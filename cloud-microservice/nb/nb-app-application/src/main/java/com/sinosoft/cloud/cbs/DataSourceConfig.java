package com.sinosoft.cloud.cbs;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.jdbc.DataSourceBuilder;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.sql.DataSource;

/**
 * Project abc-cloud
 * Created by sundongbo on 2017/9/21.
 * <pre>
 * 林花谢了春红，太匆匆，无奈朝来寒雨晚来风。
 * 胭脂泪，相留醉，几时重，自是人生长恨水长东。
 *
 * --翼羽水淼起辰月，秋山红日望紫枫--
 * </pre>
 */
@Configuration
public class DataSourceConfig {

    @Bean(name = "dataSource")
    @Qualifier("dataSource")
    @Primary
    @ConfigurationProperties(prefix = "spring.datasource.nb")
    public DataSource DataSource() {
        return DataSourceBuilder.create().build();
    }

    @Bean(name = "jdbcTemplate")
    public JdbcTemplate JdbcTemplate(
            @Qualifier("dataSource") DataSource dataSource) {
        return new JdbcTemplate(dataSource);
    }

    @Bean(name = "basedataSource")
    @Qualifier("basedataSource")
    @ConfigurationProperties(prefix = "spring.datasource.base")
    public DataSource BaseDataSource() {
        return DataSourceBuilder.create().build();
    }

    @Bean(name = "basejdbcTemplate")
    public JdbcTemplate BaseJdbcTemplate(
            @Qualifier("basedataSource") DataSource dataSource) {
        return new JdbcTemplate(dataSource);
    }

    @Bean(name = "groupdataSource")
    @Qualifier("groupdataSource")
    @ConfigurationProperties(prefix = "spring.datasource.group")
    public DataSource GroupDataSource() {
        return DataSourceBuilder.create().build();
    }

    @Bean(name = "groupjdbcTemplate")
    public JdbcTemplate GroupJdbcTemplate(
            @Qualifier("groupdataSource") DataSource dataSource) {
        return new JdbcTemplate(dataSource);
    }


}
