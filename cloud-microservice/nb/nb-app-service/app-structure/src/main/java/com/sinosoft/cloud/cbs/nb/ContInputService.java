package com.sinosoft.cloud.cbs.nb;

import com.sinosoft.cloud.cbs.nb.demoBL.CheckPreRuleBL;
import com.sinosoft.cloud.cbs.nb.demoBL.NBDataStoreBL;
import com.sinosoft.cloud.cbs.nb.demoBL.PolicyDataBL;
import com.sinosoft.cloud.cbs.uw.UWCheckService;
import com.sinosoft.cloud.microservice.AbstractMicroService;
import com.sinosoft.cloud.rest.TradeInfo;
import com.sinosoft.lis.entity.*;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.utility.ExceptionUtils;
import feign.RetryableException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component("ContInputService")
public class ContInputService extends AbstractMicroService {
    private final Log logger = LogFactory.getLog(getClass());


    @Autowired
    private UWCheckService uwCheckService;

    /**
     * 投保结构
     * 将蚂蚁平台码值转换为中台码值
     *
     * @param requestInfo
     * @return
     */
    @Override
    public boolean checkData(TradeInfo requestInfo) {
        return true;
    }

    @Override
    public TradeInfo dealData(TradeInfo requestInfo) {

        List<LogLktranStracksPojo> tLogList = new ArrayList<>();
        long startTime = 0L;
        long endTime = 0L;
        String infoStr = "";

        //1、保单数据校验
        try {
            logger.info("开始保单数据校验");
            startTime = System.currentTimeMillis();
            CheckPreRuleBL tCheckPreRule = new CheckPreRuleBL();
            requestInfo = tCheckPreRule.dealData(requestInfo);
            endTime = System.currentTimeMillis();
            infoStr = "保单数据校验完成，耗时"+String.valueOf(endTime - startTime);
            logger.info(infoStr);
            if(requestInfo.getErrorList().size() > 0){
                infoStr = requestInfo.getErrorList().get(0);
                logger.info("保单数据校验失败，失败原因为："+infoStr);
                return requestInfo;
            }
        }catch (Exception e) {
            endTime = System.currentTimeMillis();
            infoStr = "保单数据校验异常，耗时"+String.valueOf(endTime - startTime);
            requestInfo.addError(ExceptionUtils.exceptionToString(e));
        }finally {
            LogLktranStracksPojo tLog = dealInfo(startTime, endTime, "CheckPreRule", "A0021",requestInfo,infoStr);
            tLogList.add(tLog);
        }


        //2、保单数据补充
        try {
            logger.info("开始保单数据补充");
            startTime = System.currentTimeMillis();
            PolicyDataBL tPolicyDataBL = new PolicyDataBL();
            requestInfo = tPolicyDataBL.dealData(requestInfo);
            endTime = System.currentTimeMillis();
            infoStr = "保单数据补充完成，耗时"+String.valueOf(endTime - startTime);
            logger.info(infoStr);
            if(requestInfo.getErrorList().size() > 0){
                infoStr = requestInfo.getErrorList().get(0);
                logger.info("保单数据补充失败，失败原因为："+infoStr);
                return requestInfo;
            }
        }catch (Exception e) {
            endTime = System.currentTimeMillis();
            infoStr = "保单数据补充异常，耗时"+String.valueOf(endTime - startTime);
            requestInfo.addError(ExceptionUtils.exceptionToString(e));
        }finally {
            LogLktranStracksPojo tLog = dealInfo(startTime, endTime, "PolicyData", "A0021",requestInfo,infoStr);
            tLogList.add(tLog);
        }
        requestInfo.addData(LogLktranStracksPojo.class.getName(),tLogList);

        //3、规则校验
        try {
            logger.info("8.调用核保校验微服务");
            startTime = System.currentTimeMillis();
            requestInfo = uwCheckService.service(requestInfo);
            endTime = System.currentTimeMillis();
            StringBuffer stringBuffer = new StringBuffer(1024);
            infoStr = stringBuffer.append("access性能监控：")
                    .append("[核保校验]用时")
                    .append(String.valueOf(endTime - startTime)).append("毫秒").toString();
            logger.info(infoStr);
        } catch (Exception e) {
            TradeInfo tradeInfo = new TradeInfo();
            tradeInfo.addError(e.toString());
            return tradeInfo;
        }

        //核保失败返回规则编码和失败信息
        List<LCResultInfoPojo> LCResultInfoList = (List<LCResultInfoPojo>) requestInfo.getData(LCResultInfoPojo.class.getName());
        String errorLog = "";
        if (null != LCResultInfoList && LCResultInfoList.size() > 0) {
            infoStr = new StringBuffer(1024).append("[").append(LCResultInfoList.get(0).getResultNo())
                    .append("]:").append(LCResultInfoList.get(0).getResultContent()).toString();
            errorLog = "LCResultInfoList信息不为空，错误信息是：" + LCResultInfoList.toString();
            return getCheckRuleFailInfo(infoStr, errorLog);
        }
        if (requestInfo.getErrorList().size() == 0) {
            logger.info("调用核保校验微服务成功。返回结果是：" + requestInfo.toString());
        } else {
            infoStr = requestInfo.getErrorList().get(0);
            errorLog = new StringBuffer(1024).append("调用核保校验微服务内部发生错误，")
                    .append(requestInfo.getErrorList().toString()).toString();
            return getCheckRuleFailInfo(infoStr, errorLog);
        }

        //4、数据落地
        try {
            logger.info("开始保单数据落地");
            startTime = System.currentTimeMillis();
            NBDataStoreBL tNBDataStoreBL = new NBDataStoreBL();
            requestInfo = tNBDataStoreBL.dealData(requestInfo);
            endTime = System.currentTimeMillis();
            infoStr = "保单数据落地完成，耗时"+String.valueOf(endTime - startTime);
            logger.info(infoStr);
            if(requestInfo.getErrorList().size() > 0){
                infoStr = requestInfo.getErrorList().get(0);
                logger.info("保单数据落地失败，失败原因为："+infoStr);
                return requestInfo;
            }
        }catch (Exception e) {
            endTime = System.currentTimeMillis();
            infoStr = "保单数据落地异常，耗时"+String.valueOf(endTime - startTime);
            requestInfo.addError(ExceptionUtils.exceptionToString(e));
        }



        return requestInfo;
    }

    /**
     * 返回投保或者核保的异常信息，将异常信息返回给xxx
     * 如果存在异常信息，则返回内部错误，交易失败
     *
     * @param errorStr 错误信息
     * @param errorLog 错误日志
     * @return
     */
    public TradeInfo getCheckRuleFailInfo(String errorStr, String errorLog) {

        //若微服务返回信息含有Exception，则直接返回xxx内部错误，交易失败
        if (errorStr.contains("Exception") || errorStr.contains("EXCEPTION") || errorStr.contains("exception")) {
            TradeInfo tradeInfo = new TradeInfo();
            tradeInfo.addError(errorStr);
            return tradeInfo;
        }

        TradeInfo tradeInfo = new TradeInfo();
        tradeInfo.addData("return", errorStr);
        tradeInfo.addData("log", errorLog);
        return tradeInfo;
    }

    private LogLktranStracksPojo dealInfo(Long startTime, Long endTime, String serviceName, String AccessChnl, TradeInfo requestInfo,String infoStr) {
        GlobalPojo globalPojo = (GlobalPojo)requestInfo.getData(GlobalPojo.class.getName());
        LCContPojo lcContPojo = (LCContPojo)requestInfo.getData(LCContPojo.class.getName());
        String serialNo=globalPojo.getSerialNo();
        String prtNo=lcContPojo.getPrtNo();
        String contNo=lcContPojo.getContNo();


        Long timeOut = endTime - startTime;
        //重新生成物理主键
        LogLktranStracksPojo logLktranStracksPojo = new LogLktranStracksPojo();
        logLktranStracksPojo.setTemp1(String.valueOf(startTime));
        logLktranStracksPojo.setTemp2(String.valueOf(endTime));
        logLktranStracksPojo.setTemp3(String.valueOf(Math.abs(timeOut)));
        if (infoStr.indexOf("。")!=-1) {
            String info = infoStr.split("。")[0];
            logLktranStracksPojo.setPHConclusion(info);
        }else{
            logLktranStracksPojo.setPHConclusion(infoStr);
        }

        logLktranStracksPojo.setTransCode(serviceName);
        logLktranStracksPojo.setTransDate(PubFun.getCurrentDate());
        logLktranStracksPojo.setTransTime(PubFun.getCurrentTime());
        logLktranStracksPojo.setContType("1");//个险
        if(serialNo!=null){
            logLktranStracksPojo.setTransNo(serialNo);
        }
        if(prtNo!=null){
            logLktranStracksPojo.setProposalNo(prtNo);
        }
        if(contNo!=null){
            logLktranStracksPojo.setContNo(contNo);
        }

        logLktranStracksPojo.setAccessChnl(AccessChnl);

        //TODO  准备数据落地

        return logLktranStracksPojo;

    }

}



