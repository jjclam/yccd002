package com.sinosoft.cloud.cbs.nb.aip;

/**
 * Created by zhangfei on 2018/7/26.
 */
public enum DocNoTypeEnum  {
    ABCSELF("ABC_SST","9000071"),ABCSUPOTC("ABC_SUPOTC","9000081"),
    ABCMOBILE("ABC_MOBILE","9000091"),ABCEBANK("ABC_EBANK","9000101"),
    WECHAT("WECHAT","9000061");
    private String EntrustWay;
    private String CardTypeCode;

    DocNoTypeEnum(String entrustWay, String cardTypeCode) {
        EntrustWay = entrustWay;
        CardTypeCode = cardTypeCode;
    }

    public String getEntrustWay() {
        return EntrustWay;
    }

    public void setEntrustWay(String entrustWay) {
        EntrustWay = entrustWay;
    }

    public String getCardTypeCode() {
        return CardTypeCode;
    }

    public void setCardTypeCode(String cardTypeCode) {
        CardTypeCode = cardTypeCode;
    }
    public static  DocNoTypeEnum getDocNoType(String entrustWay){
        DocNoTypeEnum docNoTypeEnum1=null;
        entrustWay=entrustWay.toUpperCase();
       for(DocNoTypeEnum docNoTypeEnum:DocNoTypeEnum.values()){
           if(docNoTypeEnum.getEntrustWay().equals(entrustWay)){
               docNoTypeEnum1= docNoTypeEnum;
           }
       }
        return docNoTypeEnum1;

    }

}
