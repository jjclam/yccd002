package com.sinosoft.cloud.cbs.nb.bl;

import com.sinosoft.cloud.bl.AbstractBL;
import com.sinosoft.cloud.rest.TradeInfo;
import com.sinosoft.lis.entity.LCAppntPojo;
import com.sinosoft.lis.entity.LCContPojo;
import com.sinosoft.lis.entity.LDPersonPojo;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.utility.ExceptionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

public class AppntBL{

    private final Log logger = LogFactory.getLog(getClass());
    private static final String OPERATOR = "ABC-CLOUD";

    @Value("${cloud.nb.barrier.control}")
    private String barrier;

    private static NewCustomerCreate idWorker=new NewCustomerCreate(0,0);

    public TradeInfo dealData(TradeInfo requestInfo) {
        try {
            //获取数据
            LCContPojo lcContPojo = (LCContPojo) requestInfo.getData(LCContPojo.class.getName());
            LCAppntPojo lcAppntPojo = (LCAppntPojo) requestInfo.getData(LCAppntPojo.class.getName());

            String proposalNo = "3" + PubFun1.CreateMaxNo("WXPRTNO", 11);
            logger.info("生成的proposalNo=" + proposalNo);

            String customerNo = "";
            logger.info("==========AppntBL挡板开启=============");
            customerNo = String.valueOf(idWorker.nextId()) + NewCustomerCreate.getUUID(6);//后续调用数据交互提供生成客户号的服务

            //处理投保人信息
            this.dealAppnt(lcAppntPojo, customerNo, lcContPojo);

            //投保人客户化    type=new   old
            this.createNewPerson(requestInfo, lcAppntPojo);

            if (requestInfo.hasError()) {
                return requestInfo;
            }
        } catch (Exception e) {
            e.printStackTrace();
            logger.error(ExceptionUtils.exceptionToString(e));
            requestInfo.addError(ExceptionUtils.exceptionToString(e));
        }
        return requestInfo;
    }


    private TradeInfo createNewPerson(TradeInfo tradeInfo, LCAppntPojo lcAppntPojo) {

        List<LDPersonPojo> ldPersonPojos = (List<LDPersonPojo>) tradeInfo.getData(LDPersonPojo.class.getName());
        LCContPojo lcContPojo = (LCContPojo) tradeInfo.getData(LCContPojo.class.getName());
        if (ldPersonPojos == null) {
            ldPersonPojos = new ArrayList<LDPersonPojo>();
        }
        LDPersonPojo ldPersonPojo = new LDPersonPojo();
        String customerNo = lcAppntPojo.getAppntNo();
        //设置lcappnt 的主外键
        ldPersonPojo.setPersonID(PubFun1.CreateMaxNo(PubFun1.PhysicNo, 22));
        lcAppntPojo.setPersonID(ldPersonPojo.getPersonID());

        ldPersonPojo.setTINFlag("0");
        ldPersonPojo.setCustomerNo(customerNo);
        ldPersonPojo.setPassword(customerNo.substring(customerNo.length() - 6, customerNo.length()));
        ldPersonPojo.setName(lcAppntPojo.getAppntName());
        ldPersonPojo.setFirstName(lcAppntPojo.getFirstName());
        ldPersonPojo.setLastName(lcAppntPojo.getLastName());
        ldPersonPojo.setSex(lcAppntPojo.getAppntSex());
        ldPersonPojo.setBirthday(lcAppntPojo.getAppntBirthday());
        ldPersonPojo.setIDType(lcAppntPojo.getIDType());
        ldPersonPojo.setIDNo(lcAppntPojo.getIDNo());
        ldPersonPojo.setNativePlace(lcAppntPojo.getNativePlace());
        ldPersonPojo.setNationality(lcAppntPojo.getNationality());
        ldPersonPojo.setRgtAddress(lcAppntPojo.getRgtAddress());
        ldPersonPojo.setMarriage(lcAppntPojo.getMarriage());
        ldPersonPojo.setDegree(lcAppntPojo.getDegree());
        ldPersonPojo.setOccupationType(lcAppntPojo.getOccupationType());
        ldPersonPojo.setOccupationCode(lcAppntPojo.getOccupationCode());
        ldPersonPojo.setWorkType(lcAppntPojo.getWorkType());
        ldPersonPojo.setPosition(lcAppntPojo.getPosition());
        ldPersonPojo.setPluralityType(lcAppntPojo.getPluralityType());
        ldPersonPojo.setHaveMotorcycleLicence(lcAppntPojo.getHaveMotorcycleLicence());
        ldPersonPojo.setPartTimeJob(lcAppntPojo.getPartTimeJob());
        ldPersonPojo.setSmokeFlag(lcAppntPojo.getSmokeFlag());
        ldPersonPojo.setVIPValue("0");
        ldPersonPojo.setMakeDate(PubFun.getCurrentDate());
        ldPersonPojo.setMakeTime(PubFun.getCurrentTime());
        ldPersonPojo.setModifyDate(PubFun.getCurrentDate());
        ldPersonPojo.setModifyTime(PubFun.getCurrentTime());
        ldPersonPojo.setOperator(OPERATOR);
        ldPersonPojo.setLicenseType(lcAppntPojo.getLicenseType());
        ldPersonPojo.setIdValiDate(lcAppntPojo.getIdValiDate());
        ldPersonPojo.setCUSLevel(lcAppntPojo.getCUSLevel());
        ldPersonPojo.setRgtTpye(lcAppntPojo.getAppRgtTpye());
        ldPersonPojo.setTINFlag(lcAppntPojo.getTINFlag());
        ldPersonPojo.setTINNO(lcAppntPojo.getTINNO());
        ldPersonPojo.setNewCustomerFlag("NEW");
        ldPersonPojos.add(ldPersonPojo);
        ldPersonPojo.setShardingID(lcContPojo.getPrtNo());
        tradeInfo.addData(LDPersonPojo.class.getName(), ldPersonPojos);
        return tradeInfo;


    }


    /*
     * 处理投保人信息
     * */
    private LCAppntPojo dealAppnt(LCAppntPojo lcAppntPojo, String customerNo, LCContPojo lcContPojo) {

        lcAppntPojo.setAppntID(PubFun1.CreateMaxNo(PubFun1.PhysicNo, 22));
        lcAppntPojo.setAppntNo(customerNo);
        lcAppntPojo.setMakeDate(PubFun.getCurrentDate());
        lcAppntPojo.setMakeTime(PubFun.getCurrentTime());
        lcAppntPojo.setModifyDate(PubFun.getCurrentDate());
        lcAppntPojo.setModifyTime(PubFun.getCurrentTime());
        lcAppntPojo.setOperator(OPERATOR);
        lcAppntPojo.setShardingID(lcContPojo.getPrtNo());
        lcAppntPojo.setPrtNo(lcContPojo.getPrtNo());
        return lcAppntPojo;
    }


    public String getBarrier() {
        return barrier;
    }

    public void setBarrier(String barrier) {
        this.barrier = barrier;
    }
}