package com.sinosoft.cloud.cbs.nb.bl;

import com.sinosoft.cloud.bl.AbstractBL;
import com.sinosoft.cloud.rest.TradeInfo;
import com.sinosoft.lis.db.LDPlanDutyParamDB;
import com.sinosoft.lis.db.LDPlanRiskDB;
import com.sinosoft.lis.db.LMRiskDutyDB;
import com.sinosoft.lis.entity.*;
import com.sinosoft.lis.pubfun.Arith;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.schema.LCDutySchema;
import com.sinosoft.lis.schema.LDPlanDutyParamSchema;
import com.sinosoft.lis.schema.LDPlanRiskSchema;
import com.sinosoft.lis.vschema.LCDutySet;
import com.sinosoft.lis.vschema.LDPlanDutyParamSet;
import com.sinosoft.lis.vschema.LDPlanRiskSet;
import com.sinosoft.lis.vschema.LMRiskDutySet;
import com.sinosoft.utility.*;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Component;

import java.util.*;

@Component
public class ContBL extends AbstractBL {
    private final Log logger = LogFactory.getLog(getClass());
    private static final String OPERATOR = "ABC-CLOUD";
    private HashMap mDutyMap=new HashMap();

    @Override
    public TradeInfo dealData(TradeInfo requestInfo) {
        try {
            //lccont逻辑处理
            this.dealCont(requestInfo);
            if (requestInfo.hasError()) {
                return requestInfo;
            }

            //险种逻辑逻辑处理
            this.getRiskData(requestInfo);
            if (requestInfo.hasError()) {
                return requestInfo;
            }

            /*
             *增加lcduty数据补齐
             */
            /*this.dealDuty(requestInfo);
            if (requestInfo.hasError()) {
                return requestInfo;
            }*/
            //因为保单信息生成，所以以前用到保单新的的地方，需要在这里更新一下
            this.dealOther(requestInfo);
        } catch (Exception e) {
            e.printStackTrace();
            logger.error(ExceptionUtils.exceptionToString(e));
            requestInfo.addError(ExceptionUtils.exceptionToString(e));
            return requestInfo;
        }

        return requestInfo;
    }

    private void dealOther(TradeInfo tradeInfo) {
        LCContPojo lcContPojo = (LCContPojo) tradeInfo.getData(LCContPojo.class.getName());
        //更新投保人信息中的保单信息
        LCAppntPojo lcAppntPojo = (LCAppntPojo) tradeInfo.getData(LCAppntPojo.class.getName());
        lcAppntPojo.setContNo(lcContPojo.getContNo());
        lcAppntPojo.setGrpContNo(lcContPojo.getGrpContNo());
        lcAppntPojo.setPrtNo(lcContPojo.getPrtNo());
        lcAppntPojo.setManageCom(lcContPojo.getManageCom());
        lcAppntPojo.setShardingID(lcContPojo.getPrtNo());
    }

    private TradeInfo dealCont(TradeInfo tradeInfo) {
        try {
            LCContPojo lcContPojo = (LCContPojo) tradeInfo.getData(LCContPojo.class.getName());
            LCAppntPojo lcAppntPojo = (LCAppntPojo) tradeInfo.getData(LCAppntPojo.class.getName());
            LCInsuredPojo lcInsuredPojo = ((List<LCInsuredPojo>) tradeInfo.getData(LCInsuredPojo.class.getName())).get(0);
            GlobalPojo globalPojo = (GlobalPojo) tradeInfo.getData(GlobalPojo.class.getName());

            SSRS tLAAgent = getLAAgent(lcContPojo.getSellType());

            lcContPojo.setAgentCode(tLAAgent.GetText(1,1));
            lcContPojo.setManageCom(tLAAgent.GetText(1,2));
            tradeInfo.addData("ComCode", tLAAgent.GetText(1,2));

            //设置 lccont的主键
            lcContPojo.setContID(PubFun1.CreateMaxNo(PubFun1.PhysicNo, 22));
            lcAppntPojo.setContID(lcContPojo.getContID());
            lcContPojo.setSignCom(lcContPojo.getManageCom());

            lcContPojo.setExecuteCom(lcContPojo.getManageCom());
            lcContPojo.setApproveFlag("1");
            if(lcContPojo.getPrtNo()==null||lcContPojo.getPrtNo()==""){
                lcContPojo.setPrtNo(lcContPojo.getContNo());
            }
            lcContPojo.setShardingID(lcContPojo.getPrtNo());
            lcContPojo.setProposalContNo(lcContPojo.getPrtNo());
            lcContPojo.setPrem(0);
            lcContPojo.setAmnt(0);
            lcContPojo.setModifyDate(PubFun.getCurrentDate());
            lcContPojo.setModifyTime(PubFun.getCurrentTime());
            lcContPojo.setOperator(OPERATOR);
            lcContPojo.setGrpContNo(SysConst.ZERONO);
            lcContPojo.setAppntSex(lcAppntPojo.getAppntSex());
            lcContPojo.setAppntBirthday(lcAppntPojo.getAppntBirthday());
            lcContPojo.setAppntIDNo(lcAppntPojo.getIDNo());
            lcContPojo.setAppntIDType(lcAppntPojo.getIDType());
            lcContPojo.setAppntName(lcAppntPojo.getAppntName());
            lcContPojo.setAppntNo(lcAppntPojo.getAppntNo());
            lcContPojo.setPayLocation("0"); //续期缴费方式 默认 1
            lcContPojo.setPayMode("J");
            lcContPojo.setPayIntv("0");
            lcContPojo.setRnewFlag("-2");
            lcContPojo.setAgentGroup(lcContPojo.getAgentCode());
            lcContPojo.setMult("1");

            lcContPojo.setSettlementStatus("0");
            lcContPojo.setInsuredNo("0");
            lcContPojo.setAppFlag("1");
            lcContPojo.setState("1");
            lcContPojo.setUWFlag("9");
            lcContPojo.setForceUWFlag("0"); //强制人工核保标志 0不强制 1强制
            lcContPojo.setPolType("0"); //保单类型标记 0 --个人单：1 --无名单；如果是个单表示生日单（数据转换）2 --（团单）公共帐户
            lcContPojo.setContType("1");  //总单类型 2-集体总单,1-个人总投保单

            lcContPojo.setCValiDate(globalPojo.getTransDate());
            lcContPojo.setSignDate(globalPojo.getTransDate());
            lcContPojo.setInsuredIDNo(lcInsuredPojo.getIDNo());
            lcContPojo.setInsuredIDType(lcInsuredPojo.getIDType());
            lcContPojo.setInsuredSex(lcInsuredPojo.getSex());
            lcContPojo.setInsuredBirthday(lcInsuredPojo.getBirthday());
            lcContPojo.setInsuredName(lcInsuredPojo.getName());
            lcContPojo.setFamilyType("0");
            lcContPojo.setCardFlag("0");
            lcContPojo.setMakeDate(PubFun.getCurrentDate());
            lcContPojo.setMakeTime(PubFun.getCurrentTime());

        } catch (Exception e) {
            e.printStackTrace();
            logger.error("保单结构生成lccont异常" + ExceptionUtils.exceptionToString(e));
            tradeInfo.addError("保单结构生成lccont异常" + ExceptionUtils.exceptionToString(e));
            return tradeInfo;
        }

        return tradeInfo;
    }

    private TradeInfo dealDuty(TradeInfo tradeInfo) {
        //针对蚂蚁平台
        GlobalPojo globalPojo = (GlobalPojo) tradeInfo.getData(GlobalPojo.class.getName());
        String entrustWay = globalPojo.getEntrustWay();
        LCContPojo lcContPojo = (LCContPojo) tradeInfo.getData(LCContPojo.class.getName());
        List<LCPolPojo> LCPolPojoList = (List<LCPolPojo>) tradeInfo.getData(LCPolPojo.class.getName());
        List<LCDutyPojo> list = new ArrayList<>();

        for (int i = 0; i < LCPolPojoList.size(); i++) {
            LCPolPojo lcPolPojo = LCPolPojoList.get(i);
            if ( "Ali".equals(entrustWay)||"ShuiDi".equalsIgnoreCase(entrustWay)) {
                list.addAll(this.getLCDuty(lcPolPojo, lcContPojo, globalPojo));
            } else {
                //针对其他渠道lcduty非必传的逻辑
                List<LCDutyPojo> lcDutyPojoList = (List<LCDutyPojo>) tradeInfo.getData(LCDutyPojo.class.getName());
                for (int j = 0; j < lcDutyPojoList.size(); j++) {
                    LCDutyPojo lcDutyPojo = lcDutyPojoList.get(j);
                    if (lcDutyPojo.getDutyCode() == null || "".equals(lcDutyPojo.getDutyCode())) {
                        list.addAll(this.getLCDuty(lcPolPojo, lcContPojo, globalPojo));
                    }
                }
            }
        }
        if (list.size() > 0) {
            tradeInfo.addData(LCDutyPojo.class.getName(), list);
        }
        return tradeInfo;
    }

    private List<LCDutyPojo> getLCDuty(LCPolPojo lcPolPojo, LCContPojo lcContPojo, GlobalPojo globalPojo) {
        LCDutyPojo lcDutyPojo = new LCDutyPojo();
        List<LCDutyPojo> list = new ArrayList<>();
        LMRiskDutyDB lmRiskDutyDB = new LMRiskDutyDB();
        lmRiskDutyDB.setRiskCode(lcPolPojo.getRiskCode());
        LMRiskDutySet lmRiskset = lmRiskDutyDB.query();

        for (int j = 1; j <= lmRiskset.size(); j++) {
            String dutyCode = lmRiskset.get(j).getDutyCode();

            lcDutyPojo.setDutyID(PubFun1.CreateMaxNo(PubFun1.PhysicNo, 22));
            lcDutyPojo.setPolID(lcPolPojo.getPolID());
            lcDutyPojo.setPolNo(lcPolPojo.getPolNo());
            lcDutyPojo.setShardingID(lcPolPojo.getShardingID());
            lcDutyPojo.setDutyCode(dutyCode);
            lcDutyPojo.setContNo(lcContPojo.getContNo());
            lcDutyPojo.setMult(lcContPojo.getMult());
            lcDutyPojo.setStandPrem(lcPolPojo.getStandPrem());
            lcDutyPojo.setPrem(lcPolPojo.getPrem());
            lcDutyPojo.setSumPrem(lcPolPojo.getPrem());
            lcDutyPojo.setAmnt(lcPolPojo.getAmnt());
            lcDutyPojo.setPayIntv(lcPolPojo.getPayIntv());

            lcDutyPojo.setCValiDate(lcPolPojo.getCValiDate());
            lcDutyPojo.setOperator(lcPolPojo.getOperator());
            lcDutyPojo.setMakeDate(PubFun.getCurrentDate());
            lcDutyPojo.setMakeTime(PubFun.getCurrentTime());
            lcDutyPojo.setModifyDate(PubFun.getCurrentDate());
            lcDutyPojo.setModifyTime(PubFun.getCurrentTime());
            //补数据
            lcDutyPojo.setPayYears(lcPolPojo.getPayEndYear());
            lcDutyPojo.setYears(lcPolPojo.getInsuYear());
            lcDutyPojo.setPayEndYearFlag(lcPolPojo.getPayEndYearFlag());
            lcDutyPojo.setPayEndYear(lcPolPojo.getPayEndYear());
            lcDutyPojo.setInsuYearFlag("Y");
            lcDutyPojo.setInsuYear(lcPolPojo.getPayEndYear());
            lcDutyPojo.setEndDate(lcPolPojo.getEndDate());

            list.add(lcDutyPojo);
        }
        return list;
    }

    private String createPolNo() {
        String polLimit = "026";
        String maxNo = PubFun1.CreateMaxNo("ProposalNo", polLimit);
        maxNo =  PubFun.LCh(maxNo, "0", 19);

        String polNo = "1"+ maxNo;
        logger.debug("polLimit=" + polLimit + "CalBL生成的polno=" + polNo);
        return polNo;
    }


    private TradeInfo getRiskData(TradeInfo requestInfo) {
        LCContPojo lcContPojo = (LCContPojo) requestInfo.getData(LCContPojo.class.getName());
        GlobalPojo GlobalPojo=(com.sinosoft.lis.entity.GlobalPojo)requestInfo.getData(com.sinosoft.lis.entity.GlobalPojo.class.getName());

        /******************************* 险种信息封装开始 *********************************************/
        ExeSQL tExeSQL = new ExeSQL();
        String contPlanCode = GlobalPojo.getMainRiskCode();

        LDPlanRiskDB tLDPlanRiskDB = new LDPlanRiskDB();
        tLDPlanRiskDB.setContPlanCode(contPlanCode);

        LDPlanRiskSet tLDPlanRiskSet = tLDPlanRiskDB.query();
        if(tLDPlanRiskSet==null || tLDPlanRiskSet.size()<=0) {
            requestInfo.addError("未查询到对应的产品信息");
            return requestInfo;
        }

        List<LCPolPojo> lcPolPojoList = new ArrayList();
        List<LCDutyPojo> tLCDutyPojoList = new ArrayList();

        lcContPojo.setRemark(tLDPlanRiskSet.get(1).getContPlanName());
        lcContPojo.setProdSetCode(tLDPlanRiskSet.get(1).getContPlanCode());
        lcContPojo.setProductCode(tLDPlanRiskSet.get(1).getContPlanCode());
        //*********************************************无敌分割线*********************************************************//
        //根据套餐走不同的保额保费算法
        //1）根据算法取值
        if(false){
            String tType = "";                           //保单类型
            double strMult = lcContPojo.getMult();       //份数
            String tContPlanCode = contPlanCode;         //计划
            int insuyear = 0;                            //保险期间         （界面传值）
            String insuyearflag ="";                     //保险期间单位     （界面传值）
            int resultYear = PubFun.calInterval(lcContPojo.getInsuredBirthday(), lcContPojo.getCValiDate(), "Y"); //被保人投保年龄
            TradeInfo txTradeInfo = getPremAmnt(tType,strMult,tContPlanCode,insuyear,insuyearflag,resultYear);
            if(txTradeInfo.getErrorList().size() > 0){
                requestInfo.addError(txTradeInfo.getErrorList().get(0));
                return requestInfo;
            }
            boolean mYoung = false;
            if(resultYear<=18){
                mYoung=true;
            }
            double xprem = (double) txTradeInfo.getData("Prem");
            double xamnt = (double) txTradeInfo.getData("Amnt");
            getInputDuty(lcContPojo.getContNo(),mYoung,contPlanCode,xprem,xamnt);

            for (int t = 1; t <= tLDPlanRiskSet.size(); t++) {
                LDPlanRiskSchema tLDPlanRiskSchema = tLDPlanRiskSet.get(t);
                LCPolPojo tLCPolPojo = new LCPolPojo();
                String polId = PubFun1.CreateMaxNo(PubFun1.PhysicNo, 12);
                tLCPolPojo.setPolID(polId);

                tLCPolPojo.setSpecifyValiDate("Y");
                tLCPolPojo.setCValiDate(lcContPojo.getCValiDate()); // 生效日
                tLCPolPojo.setAgentCode(lcContPojo.getAgentCode());
                tLCPolPojo.setSignDate(lcContPojo.getSignDate());
                tLCPolPojo.setPrtNo(lcContPojo.getPrtNo());
                String mainPolNo = createPolNo();
                tLCPolPojo.setMainPolNo(mainPolNo);
                tLCPolPojo.setPolNo(mainPolNo);
                tLCPolPojo.setMainRiskFlag("1");
                tLCPolPojo.setGrpContNo(lcContPojo.getGrpContNo());
                tLCPolPojo.setGrpPolNo("00000000000000000000");
                tLCPolPojo.setProposalNo(PubFun1.CreateMaxNo(PubFun1.PhysicNo, 18));
                tLCPolPojo.setContNo(lcContPojo.getContNo());
                tLCPolPojo.setAgentCode(lcContPojo.getAgentCode());
                tLCPolPojo.setAgentGroup(lcContPojo.getAgentGroup());
                tLCPolPojo.setOperator(lcContPojo.getOperator());
                tLCPolPojo.setMakeDate(PubFun.getCurrentDate());
                tLCPolPojo.setMakeTime(PubFun.getCurrentTime());
                tLCPolPojo.setModifyDate(PubFun.getCurrentDate());
                tLCPolPojo.setModifyTime(PubFun.getCurrentTime());
                tLCPolPojo.setRiskCode(tLDPlanRiskSchema.getRiskCode()); // 险种编码
                tLCPolPojo.setInsuYear(insuyear); // 保障年期
                tLCPolPojo.setInsuYearFlag(insuyearflag); // 保险期间类型
                tLCPolPojo.setPayIntv(lcContPojo.getPayIntv()); // 缴费频率
                tLCPolPojo.setRnewFlag(!PubFun.isEmpty(lcContPojo.getRnewFlag()+"") ? Integer.parseInt(lcContPojo.getRnewFlag()+"") : -2);
                TradeInfo tLCDutyInfo = new TradeInfo();
                tLCDutyInfo.addData(LCPolPojo.class.getName(),tLCPolPojo);
                tLCDutyPojoList = getDutyInfoByRisk(tLCDutyInfo,tContPlanCode);
                if(tLCDutyPojoList.size()<=0){
                    requestInfo.addError("未查询到对应的责任信息");
                    return requestInfo;
                }
                double prem = 0.00;
                double amnt = 0.00;
                for (int s = 0; s < tLCDutyPojoList.size(); s++) {
                    prem = Arith.add(prem, tLCDutyPojoList.get(s).getPrem());
                    amnt = Arith.add(amnt, tLCDutyPojoList.get(s).getAmnt());
                }
                tLCPolPojo.setAmnt(amnt); // 保额
                tLCPolPojo.setPrem(prem); // 保费
                lcContPojo.setAmnt(amnt); // 保额
                lcContPojo.setPrem(prem); // 保费
                tLCPolPojo.setPayYears(tLCDutyPojoList.get(0).getPayEndYear()); // 缴费期间
                tLCPolPojo.setPayEndYear(tLCDutyPojoList.get(0).getPayEndYear());
                tLCPolPojo.setPayEndYearFlag(tLCDutyPojoList.get(0).getPayEndYearFlag());
                lcPolPojoList.add(tLCPolPojo);

            }
        }else{
            //2）取套餐固定保额保费
            for (int t = 1; t <= tLDPlanRiskSet.size(); t++) {
                LDPlanRiskSchema tLDPlanRiskSchema = tLDPlanRiskSet.get(t);
                LCPolPojo tLCPolPojo = new LCPolPojo();
                String polId = PubFun1.CreateMaxNo(PubFun1.PhysicNo, 12);
                tLCPolPojo.setPolID(polId);

                tLCPolPojo.setSpecifyValiDate("Y");
                tLCPolPojo.setCValiDate(lcContPojo.getCValiDate()); // 生效日
                tLCPolPojo.setAgentCode(lcContPojo.getAgentCode());
                tLCPolPojo.setSignDate(lcContPojo.getSignDate());
                tLCPolPojo.setPrtNo(lcContPojo.getPrtNo());
                String mainPolNo = createPolNo();
                tLCPolPojo.setMainPolNo(mainPolNo);
                tLCPolPojo.setPolNo(mainPolNo);
                tLCPolPojo.setMainRiskFlag("1");
                tLCPolPojo.setGrpContNo(lcContPojo.getGrpContNo());
                tLCPolPojo.setGrpPolNo("00000000000000000000");
                tLCPolPojo.setProposalNo(PubFun1.CreateMaxNo(PubFun1.PhysicNo, 18));
                tLCPolPojo.setContNo(lcContPojo.getContNo());
                tLCPolPojo.setAgentCode(lcContPojo.getAgentCode());
                tLCPolPojo.setAgentGroup(lcContPojo.getAgentGroup());
                tLCPolPojo.setOperator(lcContPojo.getOperator());
                tLCPolPojo.setMakeDate(PubFun.getCurrentDate());
                tLCPolPojo.setMakeTime(PubFun.getCurrentTime());
                tLCPolPojo.setModifyDate(PubFun.getCurrentDate());
                tLCPolPojo.setModifyTime(PubFun.getCurrentTime());
                String tSQL = "select distinct dutycode from ldplandutyparam where contplancode = '"
                        + contPlanCode
                        + "' "
                        + "and riskcode = '"
                        + tLDPlanRiskSchema.getRiskCode()
                        + "' order by dutycode";
                List<String> list= new ArrayList();
                SSRS tSSRS = tExeSQL.execSQL(tSQL);
                if (tSSRS != null && tSSRS.getMaxRow() > 0) {
                    String codeString="";
                    for (int s = 1; s <= tSSRS.getMaxRow(); s++) {
                        codeString=codeString+ tSSRS.GetText(s,1)+",";
                    }
                    codeString=codeString.substring(0,codeString.length()-1);
                    for (String s : codeString.split(",")) {
                        list.add(s);
                    }
                }
                if (list != null && list.size() > 0) {
                    for (int s = 0; s < list.size(); s++) {
                        LDPlanDutyParamDB tLDPlanDutyParamDB = new LDPlanDutyParamDB();
                        tLDPlanDutyParamDB.setContPlanCode(contPlanCode);
                        tLDPlanDutyParamDB.setRiskCode(tLDPlanRiskSchema.getRiskCode());
                        tLDPlanDutyParamDB.setDutyCode(list.get(s));

                        LDPlanDutyParamSet tLDPlanDutyParamSet = tLDPlanDutyParamDB.query();
                        if(tLDPlanDutyParamSet == null || tLDPlanDutyParamSet.size() <= 0){
                            requestInfo.addError("未查询到产品组合配置险种信息");
                        }
                        LCDutyPojo tLCDutyPojo = new LCDutyPojo();
                        tLCDutyPojo.setContNo(lcContPojo.getContNo());
                        tLCDutyPojo.setPolNo(mainPolNo);
                        tLCDutyPojo.setPolID(polId);
                        tLCDutyPojo.setDutyID(PubFun1.CreateMaxNo(PubFun1.PhysicNo, 12));
                        tLCDutyPojo.setDutyCode(list.get(s));
                        tLCDutyPojo.setGetStartDate(tLCPolPojo.getCValiDate());
                        if (!transPlanToDuty(tLCDutyPojo,tLDPlanDutyParamSet)) {
                            CError.buildErr(this,"从保险计划向责任赋值失败!");
                            return null;
                        }
                        SSRS ssrs= tExeSQL.execSQL("select payendyear,payendyearflag,insuyear from lmduty where dutycode='"+list.get(s)+"'");
                        tLCDutyPojo.setPayEndYear(ssrs.GetText(1,1));
                        tLCDutyPojo.setPayEndYearFlag(ssrs.GetText(1,2));
                        tLCDutyPojo.setPayIntv(lcContPojo.getPayIntv());
                        tLCDutyPojo.setSSFlag("0");
                        tLCDutyPojo.setOperator(lcContPojo.getOperator());
                        tLCDutyPojo.setMakeDate(PubFun.getCurrentDate());
                        tLCDutyPojo.setModifyDate(PubFun.getCurrentDate());
                        tLCDutyPojo.setMakeTime(PubFun.getCurrentTime());
                        tLCDutyPojo.setModifyTime(PubFun.getCurrentTime());
                        tLCDutyPojoList.add(tLCDutyPojo);
                    }
                }
                double prem = 0.00;
                double amnt = 0.00;
                for (int s = 0; s < tLCDutyPojoList.size(); s++) {
                    prem = Arith.add(prem, tLCDutyPojoList.get(s).getPrem());
                    amnt = Arith.add(amnt, tLCDutyPojoList.get(s).getAmnt());
                }
                tLCPolPojo.setRiskCode(tLDPlanRiskSchema.getRiskCode()); // 险种编码
                tLCPolPojo.setAmnt(amnt); // 保额
                tLCPolPojo.setPrem(prem); // 保费
                lcContPojo.setAmnt(amnt); // 保额
                lcContPojo.setPrem(prem); // 保费
                tLCPolPojo.setPayYears(tLCDutyPojoList.get(0).getPayEndYear()); // 缴费期间
                tLCPolPojo.setPayIntv(lcContPojo.getPayIntv()); // 缴费频率
                tLCPolPojo.setInsuYear(tLCDutyPojoList.get(0).getInsuYear()); // 保障年期
                tLCPolPojo.setInsuYearFlag(tLCDutyPojoList.get(0).getInsuYearFlag()); // 保险期间类型
                tLCPolPojo.setPayEndYear(tLCDutyPojoList.get(0).getPayEndYear());
                tLCPolPojo.setPayEndYearFlag(tLCDutyPojoList.get(0).getPayEndYearFlag());
                tLCPolPojo.setRnewFlag(!PubFun.isEmpty(lcContPojo.getRnewFlag()+"") ? Integer.parseInt(lcContPojo.getRnewFlag()+"") : -2);


                lcPolPojoList.add(tLCPolPojo);
            }
        }
        requestInfo.addData(LCPolPojo.class.getName(),lcPolPojoList);
//        requestInfo.addData(lcPolPojoList);
//        requestInfo.addData(tLCDutyPojoList);
        requestInfo.addData(LCDutyPojo.class.getName(),tLCDutyPojoList);
        return requestInfo;
    }

    private boolean transPlanToDuty(LCDutyPojo tLCDutyPojo, LDPlanDutyParamSet tLDPlanDutyParamSet) {
        String tDutyCode = tLCDutyPojo.getDutyCode();
        for (int i = 1; i <= tLDPlanDutyParamSet.size(); i++) {
            String CalFactor = tLDPlanDutyParamSet.get(i).getCalFactor();
            String CalFactorValue = tLDPlanDutyParamSet.get(i).getCalFactorValue();
            String tCalDutyCode = tLDPlanDutyParamSet.get(i).getDutyCode();
            if (tDutyCode.trim().equals(tCalDutyCode.trim())) {
                if (tLCDutyPojo.getFieldIndex(CalFactor) > -1) {
                    if (CalFactor.equalsIgnoreCase("Prem")
                            || CalFactor.equalsIgnoreCase("Amnt")
                            || CalFactor.equalsIgnoreCase("GetLimit")
                            || CalFactor.equalsIgnoreCase("GetRate")) {
                        tLCDutyPojo.setV(CalFactor, String.valueOf(Double.parseDouble(CalFactorValue) * 1));
                    } else {
                        tLCDutyPojo.setV(CalFactor, CalFactorValue);
                    }
                }
                tLCDutyPojo.setMult(1);
            }
        }
        return true;
    }














    public SSRS getLAAgent(String tSellType) {
        StringBuffer sql = new StringBuffer();
        sql.append("select a.codealias,a.comcode from ldcode a where a.codetype = 'rmpagent' and a.code in (?) ");
        sql.append("order by a.code desc");
        TransferData tparam = new TransferData();
        tparam.setNameAndValue("0", "string:" + tSellType);
        VData tBVData = new VData();
        tBVData.add(sql.toString());
        tBVData.add(tparam);
        ExeSQL pExeSQL = new ExeSQL();
        SSRS tSSRS = pExeSQL.execSQL(tBVData);
        return tSSRS;
    }

    public TradeInfo getPremAmnt(String tType,double strMult,String tContPlanCode,int year,String yearUnit,int resultYear ) {
        TradeInfo tTradeInfo = new TradeInfo();
        if("train".equals(tType) || "air".equals(tType)){
            //计算保费
            String tsql = "";
            tsql="select contplantype from ldplan where 1=1 and contplancode='"+tContPlanCode+"'";
            ExeSQL tExesql = new ExeSQL();
            SSRS tssrs =new SSRS();
            tssrs= tExesql.execSQL(tsql);
            if(tssrs.GetText(1, 1).equals("1")){
                //计算保费
                String tSQL = "select nvl((select a.prem from abroadbaserate a where a.intercode = '"+tContPlanCode+"' "
                        + "and a.mininsuyear <= "+year+" and a.maxinsuyear >= "+year
                        + " and a.insuyearflag = '"+yearUnit+"' and rownum = 1), 0) * nvl((select b.rate from abroadaddrate b "
                        + "where b.intercode = '"+tContPlanCode+"' and b.minage <= '"+resultYear+"' and b.maxage >= '"+resultYear+"' and rownum = 1),1) from dual ";
                ExeSQL tExeSQL = new ExeSQL();
                String strPrem = tExeSQL.getOneValue(tSQL);
                if("".equals(strPrem) || strPrem == null || "0".equals(strPrem)){
                    tTradeInfo.addError("计算保费失败，请检查产品费率表！");
                    return tTradeInfo;
                }
                tTradeInfo.addData("Prem",Double.parseDouble(strPrem)*strMult);//保费

                //按年龄计算保额
                String tSQL1 = "select sum(dutyamnt) from sldplanriskbyage where contplancode in (select contplancode from sldplan where intercode='"
                        +tContPlanCode +"') and startage<=" +resultYear +" and endage>=" +resultYear;
                ExeSQL tExeSQL1 = new ExeSQL();
                String strAmnt = tExeSQL1.getOneValue(tSQL1);
                if("".equals(strAmnt) || strAmnt == null || "0".equals(strAmnt)){
                    tTradeInfo.addError("计算保额失败，请检查款式计划包含险种责任信息定义表！");
                    return tTradeInfo;
                }
                tTradeInfo.addData("Amnt",Double.parseDouble(strAmnt)*strMult);//保额
            }else{
                String tSQL = "";
                if("train".equals(tType)){
                    tSQL = "select prem*(select nvl((select rate from abroadaddrate where intercode='"+tContPlanCode
                            +"' and '"+resultYear+"' between minage and maxage),1) rate from dual),amnt from TraRate1 where 1=1 and ("
                            +resultYear+" between minage and maxage) and InterCode='"+tContPlanCode+"' and InsuYear='"+year+"' and InsuYearFlag='"+yearUnit+"'";
                }else{
                    tSQL = "select prem*(select nvl((select rate from abroadaddrate where intercode='"+tContPlanCode
                            +"' and '"+resultYear+"' between minage and maxage),1) rate from dual),amnt from TraRate1 where 1=1 and InterCode='"+tContPlanCode+"' and InsuYear='"+year+"' and InsuYearFlag='"+yearUnit+"'";
                }
                ExeSQL tExeSQL = new ExeSQL();
                SSRS tSSRS = new SSRS();
                tSSRS = tExeSQL.execSQL(tSQL);
                if(tSSRS.getMaxRow() == 1){
                    tTradeInfo.addData("Prem",Double.parseDouble(tSSRS.GetText(1, 1))*strMult);//保费
                    tTradeInfo.addData("Amnt",Double.parseDouble(tSSRS.GetText(1, 2))*strMult);//保额
                }else{
                    if("train".equals(tType)){
                        tTradeInfo.addError("行录入的保险款式为"+tContPlanCode+"、保险期间为"+year+"、保险期间单位为"+yearUnit+"的险种，计算保费时出错，请确认费率表是否存在！\\n");
                        return tTradeInfo;
                    }else{
                        tTradeInfo.addData("Prem",0);//保费
                        tTradeInfo.addData("Amnt",0);//保额
                    }
                }
            }
        }else if("tour".equals(tType)){
            //计算保费
            String tSQL = "select nvl((select a.prem from abroadbaserate a where a.intercode = '"+tContPlanCode+"' "
                    + "and a.mininsuyear <= "+year+" and a.maxinsuyear >= "+year
                    + " and a.insuyearflag = '"+yearUnit+"' and rownum = 1), 0) * nvl((select b.rate from abroadaddrate b "
                    + "where b.intercode = '"+tContPlanCode+"' and b.minage <= '"+resultYear+"' and b.maxage >= '"+resultYear+"' and rownum = 1),1) from dual ";
            ExeSQL tExeSQL = new ExeSQL();
            String strPrem = tExeSQL.getOneValue(tSQL);
            if("".equals(strPrem) || strPrem == null || "0".equals(strPrem)){
                tTradeInfo.addError("计算保费失败，请检查产品费率表！");
                return tTradeInfo;
            }
            tTradeInfo.addData("Prem",Double.parseDouble(strPrem)*strMult);//保费
            //按年龄计算保额
            String tSQL1 = "select sum(dutyamnt) from sldplanriskbyage where contplancode in (select contplancode from sldplan where intercode='"
                    +tContPlanCode
                    +"') and startage<="
                    +resultYear
                    +" and endage>="
                    +resultYear;
            ExeSQL tExeSQL1 = new ExeSQL();
            String strAmnt = tExeSQL1.getOneValue(tSQL1);
            if("".equals(strAmnt) || strAmnt == null || "0".equals(strAmnt)){
                tTradeInfo.addError("计算保额失败，请检查款式计划包含险种责任信息定义表！");
                return tTradeInfo;
            }
            tTradeInfo.addData("Amnt",Double.parseDouble(strAmnt)*strMult);//保额
        }

        return tTradeInfo;
    }

    public boolean getInputDuty(String tContNo,boolean mYoung,String mContPlanCode,double dNewContPrem,double dNewContAmnt){
        //查询出产品组合的总保额
        String sContPlanPrem = "";
        double dContPlanPrem=0;
        String sContPlanAmnt = "";
        double dContPlanAmnt=0;

        ExeSQL tExeSQL = new ExeSQL();

        //查询险种的保额之和
        String tContPlanSql=" select nvl(sum(to_number(calfactorvalue)),0) "
                + " from ldplandutyparam "
                + " where contplancode = '"+mContPlanCode+"' "
                + " and lower(trim(calfactor)) = 'amnt' "
                ;
        sContPlanAmnt = tExeSQL.getOneValue(tContPlanSql);

        if(sContPlanAmnt==null || "".equals(sContPlanAmnt)){
            System.out.println("产品套餐:"+mContPlanCode+" 的保额为空!");
            return false;
        }
        dContPlanAmnt=Double.parseDouble(sContPlanAmnt);

        tContPlanSql=" select nvl(sum(to_number(calfactorvalue)),0) "
                + " from ldplandutyparam "
                + " where contplancode = '"+mContPlanCode+"' "
                + " and lower(trim(calfactor)) = 'prem' "
        ;
        sContPlanPrem = tExeSQL.getOneValue(tContPlanSql);
        if(sContPlanPrem==null || "".equals(sContPlanPrem)){
            System.out.println("产品套餐:"+mContPlanCode+" 的保费为空!");
            return false;
        }
        dContPlanPrem=Double.parseDouble(sContPlanPrem);

        //查询套餐中是否包含共保险种
        boolean tShareAmntFlag=false;
        SSRS tPlanRiskSSRS=new ExeSQL().execSQL("select distinct riskcode from ldplanrisk where contplancode='"+mContPlanCode+"'");
        for(int m=1;m<=tPlanRiskSSRS.getMaxRow();m++){
            if("314013".equals(tPlanRiskSSRS.GetText(m, 1))){//远程-借意
                tShareAmntFlag=true;
                if(tPlanRiskSSRS.getMaxRow()>1){
                    System.out.println("产品套餐:"+mContPlanCode+" 除了【314013】包含其它险种!");
                    return false;
                }
                break;
            }
        }
        //查询是否需要处理套餐结构
        Map tPlanDutysMap=new HashMap();
        if(mYoung==true){
            SSRS tPlanDutysSSRS=new ExeSQL().execSQL("select code1 from ldcode1 where codetype='ycyamnt' and code='"+mContPlanCode+"'");
            if(tPlanDutysSSRS!=null && tPlanDutysSSRS.getMaxRow()>0){
                for(int i=1;i<=tPlanDutysSSRS.getMaxRow();i++){
                    tPlanDutysMap.put(tPlanDutysSSRS.GetText(i, 1), "mark");
                }
            }
        }

        //查出每个责任的保费、保额
        String tPlanDutySql=" select dutycode,calfactor,nvl(calfactorvalue,'0') "
                + " from ldplandutyparam "
                + " where contplancode='"+mContPlanCode+"' and lower(calfactor) in ('amnt','prem')"
                + " order by dutycode "
                ;
        SSRS tPlanDutySSRS = new ExeSQL().execSQL(tPlanDutySql);
        if(tPlanDutySSRS==null || tPlanDutySSRS.getMaxRow()<1){
            System.out.println("查询产品套餐:"+mContPlanCode+" 的责任时出错!");
            return false;
        }
        mDutyMap=new HashMap();
        String tDutyCode="";
        double[] tDutyArray=null;
        for(int i=1;i<=tPlanDutySSRS.getMaxRow();i++){
            tDutyCode=tPlanDutySSRS.GetText(i, 1);
            tDutyArray=(double[])mDutyMap.get(tDutyCode);
            if(tDutyArray==null){
                //0-保费   1-保额   2-新保费  3-新保额
                tDutyArray=new double[4];
                mDutyMap.put(tDutyCode, tDutyArray);
            }
            if("Prem".equals(StrTool.cTrim(tPlanDutySSRS.GetText(i, 2)))){
                tDutyArray[0]=Double.parseDouble(tPlanDutySSRS.GetText(i, 3));
            }else if("Amnt".equals(StrTool.cTrim(tPlanDutySSRS.GetText(i, 2)))){
                if(!"".equals(StrTool.cTrim((String)tPlanDutysMap.get(tPlanDutySSRS.GetText(i, 1))))){
                    tDutyArray[1]=Double.parseDouble(tPlanDutySSRS.GetText(i, 3));
                    System.out.println("tDutyArray[1]="+tDutyArray[1]);
                    if(tDutyArray[1]>1000){
                        dContPlanAmnt=Arith.sub(dContPlanAmnt, Arith.sub(tDutyArray[1], 1000));
                        tDutyArray[1]=1000;
                    }
                }else{
                    tDutyArray[1]=Double.parseDouble(tPlanDutySSRS.GetText(i, 3));
                }
            }
        }
        //将传入的保费、保额拆分到责任
        double dSumSplitPrem=0.00,dSumSplitAmnt=0.00;

        Iterator tPlanIt=mDutyMap.keySet().iterator();
        while(tPlanIt.hasNext()){
            tDutyCode=(String)tPlanIt.next();
            tDutyArray=(double[])mDutyMap.get(tDutyCode);

            if(dNewContPrem==0){
                tDutyArray[2]=0;
            }else{
                tDutyArray[2]=Arith.round(Arith.mul(dNewContPrem, Arith.div(tDutyArray[0], dContPlanPrem)), 2);
            }
            if(tShareAmntFlag==false){
                tDutyArray[3]=Arith.round(Arith.mul(dNewContAmnt, Arith.div(tDutyArray[1], dContPlanAmnt)), 2);
            }else{
                tDutyArray[3]=Arith.round(dNewContAmnt, 2);
            }
            dSumSplitPrem=Arith.add(dSumSplitPrem, tDutyArray[2]);
            dSumSplitAmnt=Arith.add(dSumSplitAmnt, tDutyArray[3]);
        }
        //调整拆分后的金额
        double dDifPrem=0,dDifAmnt=0;
        dDifPrem=Arith.sub(dSumSplitPrem,dNewContPrem);
        dDifAmnt=Arith.sub(dSumSplitAmnt,dNewContAmnt);
        //调整保费
        if(dDifPrem!=0){
            boolean tChange=false;
            tPlanIt=mDutyMap.keySet().iterator();
            while(tPlanIt.hasNext()){
                tDutyCode=(String)tPlanIt.next();
                tDutyArray=(double[])mDutyMap.get(tDutyCode);

                if(Arith.sub(tDutyArray[2],dDifPrem)>0){
                    tDutyArray[2]=Arith.sub(tDutyArray[2],dDifPrem);
                    tChange=true;
                    break;
                }
            }
            if(tChange==false){
                System.out.println("调整保单  "+tContNo+" 的责任保费时出错");
                return false;
            }
        }
        //调整保额
        if(dDifAmnt!=0){
            if(tShareAmntFlag==true){
                System.out.println("mContPlanCode【"+mContPlanCode+"】共享保额的发现保额之和【"+dSumSplitAmnt+"】不等传入的保额【"+dNewContAmnt+"】差额【"+dDifAmnt+"】，跳过。");

            }else{
                boolean tChange=false;
                tPlanIt=mDutyMap.keySet().iterator();
                while(tPlanIt.hasNext()){
                    tDutyCode=(String)tPlanIt.next();
                    tDutyArray=(double[])mDutyMap.get(tDutyCode);

                    if(Arith.sub(tDutyArray[3],dDifAmnt)>0){
                        tDutyArray[3]=Arith.sub(tDutyArray[3],dDifAmnt);
                        tChange=true;
                        break;
                    }
                }
                if(tChange==false){
                    System.out.println("调整保单  "+tContNo+" 的责任保额时出错");
                    return false;
                }
            }
        }
        return true;
    }
    /**
     * 通过险种编码获取保险计划下的责任信息
     */
    private List<LCDutyPojo>  getDutyInfoByRisk(TradeInfo tLCDutyInfo,String mContPlanCode){
        LCPolPojo tLCPolPojo = (LCPolPojo) tLCDutyInfo.getData(LCPolPojo.class.getName());
        String tRiskCode = tLCPolPojo.getRiskCode();             //险种
        String mCValiDate = tLCPolPojo.getCValiDate();           //生效日期
        String tInsuYear = tLCPolPojo .getInsuYear()+"";         //保险期间
        String tInsuYearFlag = tLCPolPojo.getInsuYearFlag();     //保险期间单位

        List<LCDutyPojo> tLCDutyPojoList = new ArrayList();
        ExeSQL tExeSQL = new ExeSQL();
        SSRS tSSRS = new SSRS();
        String tSQL = "select DutyCode from LDPlanDutyParam where 1=1 " +
                "and ContPlanCode='"+mContPlanCode+"' and RiskCode='"+tRiskCode+"' group by DutyCode";
        tSSRS = tExeSQL.execSQL(tSQL);
        if(tSSRS.getMaxRow()<=0){
            CError.buildErr(this,"产品组合"+mContPlanCode+"下险种"+tRiskCode+"的责任信息查询失败!");
            return null;
        }
        for(int i=1;i<=tSSRS.getMaxRow();i++){
            String tDutyCode = tSSRS.GetText(i, 1);
            LCDutyPojo tLCDutyPojo = new LCDutyPojo();
            tLCDutyPojo.setDutyCode(tDutyCode);

            LDPlanDutyParamDB tLDPlanDutyParamDB = new LDPlanDutyParamDB();
            tLDPlanDutyParamDB.setContPlanCode(mContPlanCode);
            tLDPlanDutyParamDB.setRiskCode(tRiskCode);
            tLDPlanDutyParamDB.setDutyCode(tDutyCode);
            LDPlanDutyParamSet tLDPlanDutyParamSet = tLDPlanDutyParamDB.query();
            for(int j=1;j<=tLDPlanDutyParamSet.size();j++){
                LDPlanDutyParamSchema tLDPlanDutyParamSchema = tLDPlanDutyParamSet.get(j);
                String tCalFactor = tLDPlanDutyParamSchema.getCalFactor();
                String tCalFactorValue = tLDPlanDutyParamSchema.getCalFactorValue();
                tLCDutyPojo.setV(tCalFactor, tCalFactorValue);
            }

            tLCDutyPojo.setMult("1");
            tLCDutyPojo.setCValiDate(mCValiDate);
            tLCDutyPojo.setCalRule("3");
            tLCDutyPojo.setInsuYear(tInsuYear);
            tLCDutyPojo.setInsuYearFlag(tInsuYearFlag);
            double[] tDutyArray=(double[])mDutyMap.get(tDutyCode);
            if(tDutyArray==null){
                CError.buildErr(this,"查找责任 "+tDutyCode+" 的保额时出错");
                return null;
            }
            tLCDutyPojo.setPrem(tDutyArray[2]);
            tLCDutyPojo.setAmnt(tDutyArray[3]);
            tLCDutyPojo.setContNo(tLCPolPojo.getContNo());
            tLCDutyPojo.setPolNo(tLCPolPojo.getMainPolNo());
            tLCDutyPojo.setPolID(tLCPolPojo.getPolID());
            tLCDutyPojo.setDutyID(PubFun1.CreateMaxNo(PubFun1.PhysicNo, 12));
            tLCDutyPojo.setGetStartDate(tLCPolPojo.getCValiDate());
            SSRS ssrs= tExeSQL.execSQL("select payendyear,payendyearflag,insuyear from lmduty where dutycode='"+tDutyCode+"'");
            tLCDutyPojo.setPayEndYear(ssrs.GetText(1,1));
            tLCDutyPojo.setPayEndYearFlag(ssrs.GetText(1,2));
            tLCDutyPojo.setPayIntv(tLCPolPojo.getPayIntv());
            tLCDutyPojo.setSSFlag("0");
            tLCDutyPojo.setOperator(tLCPolPojo.getOperator());
            tLCDutyPojo.setMakeDate(PubFun.getCurrentDate());
            tLCDutyPojo.setModifyDate(PubFun.getCurrentDate());
            tLCDutyPojo.setMakeTime(PubFun.getCurrentTime());
            tLCDutyPojo.setModifyTime(PubFun.getCurrentTime());
            tLCDutyPojoList.add(tLCDutyPojo);
        }
        return tLCDutyPojoList;
    }
}





