package com.sinosoft.cloud.cbs.nb.demoBL;

import com.sinosoft.cloud.rest.TradeInfo;
import com.sinosoft.lis.entity.GlobalPojo;
import com.sinosoft.lis.entity.LCContPojo;

public class CheckPreRuleBL {


    public TradeInfo dealData(TradeInfo requestInfo) {
        GlobalPojo tGlobalPojo = (GlobalPojo) requestInfo.getData(GlobalPojo.class.getName());
        LCContPojo tLCContPojo = (LCContPojo) requestInfo.getData(LCContPojo.class.getName());

        String tSerialNo = tGlobalPojo.getSerialNo();
        if(tSerialNo == null || "".equals(tSerialNo)){
            requestInfo.addError("SerialNo不可为空");
        }
        String tMainRiskCode = tGlobalPojo.getMainRiskCode();
        if(tMainRiskCode == null || "".equals(tMainRiskCode)){
            requestInfo.addError("ProductCode不可为空");
        }
        String TransDate = tGlobalPojo.getTransDate();
        if(TransDate == null || "".equals(TransDate)){
            requestInfo.addError("TransDate不可为空");
        }
        String TransTime = tGlobalPojo.getTransTime();
        if(TransTime == null || "".equals(TransTime)){
            requestInfo.addError("TransTime不可为空");
        }
        String TransNo = tGlobalPojo.getTransNo();
        if(TransNo == null || "".equals(TransNo)){
            requestInfo.addError("TransType不可为空");
        }
        String EntrustWay = tGlobalPojo.getEntrustWay();
        if(EntrustWay == null || "".equals(EntrustWay)){
            requestInfo.addError("ChannelCode不可为空");
        }
        String ThirdPartyOrderId = tLCContPojo.getThirdPartyOrderId();
        if(ThirdPartyOrderId == null || "".equals(ThirdPartyOrderId)){
            requestInfo.addError("OrderNo不可为空");
        }
        String AppntName = tLCContPojo.getAppntName();
        if(AppntName == null || "".equals(AppntName)){
            requestInfo.addError("AppntName不可为空");
        }
        String AppntSex = tLCContPojo.getAppntSex();
        if(AppntSex == null || "".equals(AppntSex)){
            requestInfo.addError("AppntSex不可为空");
        }
        String AppntBirthday = tLCContPojo.getAppntBirthday();
        if(AppntBirthday == null || "".equals(AppntBirthday)){
            requestInfo.addError("AppntBirthday不可为空");
        }
        String AppntIDType = tLCContPojo.getAppntIDType();
        if(AppntIDType == null || "".equals(AppntIDType)){
            requestInfo.addError("AppntIDType不可为空");
        }
        String AppntIDNo = tLCContPojo.getAppntIDNo();
        if(AppntIDNo == null || "".equals(AppntIDNo)){
            requestInfo.addError("AppntIDNo不可为空");
        }

        return requestInfo;
    }
}
