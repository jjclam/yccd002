package com.sinosoft.cloud.cbs.nb.demoBL;

import com.sinosoft.cloud.cbs.nb.bl.AppntBL;
import com.sinosoft.cloud.cbs.nb.bl.ContBL;
import com.sinosoft.cloud.cbs.nb.bl.InsuredBL;
import com.sinosoft.cloud.cbs.nb.bl.PolBL;
import com.sinosoft.cloud.rest.TradeInfo;
import com.sinosoft.lis.db.LDPlanDutyParamDB;
import com.sinosoft.lis.db.LDPlanRiskDB;
import com.sinosoft.lis.entity.*;
import com.sinosoft.lis.pubfun.Arith;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.schema.LDPlanRiskSchema;
import com.sinosoft.lis.vschema.LDPlanDutyParamSet;
import com.sinosoft.lis.vschema.LDPlanRiskSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.ExceptionUtils;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

public class PolicyDataBL {
    private final Log logger = LogFactory.getLog(this.getClass());
    private AppntBL appntBL = new AppntBL();
    private ContBL contBL = new ContBL();
    private InsuredBL insuredBL = new InsuredBL();
    private PolBL polBL = new PolBL();

    public TradeInfo dealData(TradeInfo requestInfo) {
        //保单号生成
        LCContPojo lcContPojo = (LCContPojo) requestInfo.getData(LCContPojo.class.getName());
        String contNo = PubFun1.CreateMaxNo("ContNo", "2020");

        if(lcContPojo.getPrtNo()==null||"".equals(lcContPojo.getPrtNo())){
            lcContPojo.setPrtNo(contNo);
        }

        lcContPojo.setContNo(contNo);
        logger.info("生成保单号生成成功"+lcContPojo.getContNo());
        requestInfo.addData(LCContPojo.class.getName(),lcContPojo);

        //补充保单结构
        requestInfo = getContData(requestInfo);
        logger.info("补充保单结构成功"+lcContPojo.getContNo());

        //计算保额保费
//        requestInfo = getPremAmnt(requestInfo);
//        logger.info("补充保单结构成功"+lcContPojo.getContNo());

        return requestInfo;
    }

    private TradeInfo getPremAmnt(TradeInfo requestInfo) {
        LCContPojo lcContPojo = (LCContPojo) requestInfo.getData(LCContPojo.class.getName());
        List<LCPolPojo> lcPolPojoList= (List<LCPolPojo>) requestInfo.getData(LCPolPojo.class.getName());
        List<LCInsuredPojo>  lcInsuredPojoList = (List<LCInsuredPojo>) requestInfo.getData(LCInsuredPojo.class.getName());
        List<LCDutyPojo>  tLCDutyPojoList = (List<LCDutyPojo>)  requestInfo.getData(LCDutyPojo.class.getName());
        GlobalPojo GlobalPojo=(com.sinosoft.lis.entity.GlobalPojo)requestInfo.getData(com.sinosoft.lis.entity.GlobalPojo.class.getName());


        /******************************* 险种信息封装开始 *********************************************/
        ExeSQL tExeSQL = new ExeSQL();
        String contPlanCode = lcInsuredPojoList.get(0).getContPlanCode();
        LDPlanRiskDB tLDPlanRiskDB = new LDPlanRiskDB();
        tLDPlanRiskDB.setContPlanCode(contPlanCode);
        LDPlanRiskSet tLDPlanRiskSet = tLDPlanRiskDB.query();
        if (tLDPlanRiskSet != null && tLDPlanRiskSet.size() > 0) {

//            lcPolPojoList.clear();
//            tLCDutyPojoList.clear();

            for (int t = 1; t <= tLDPlanRiskSet.size(); t++) {
                LDPlanRiskSchema tLDPlanRiskSchema = new LDPlanRiskSchema();
                tLDPlanRiskSchema = tLDPlanRiskSet.get(t);
                LCPolPojo tLCPolPojo = new LCPolPojo();
                tLCPolPojo.setSpecifyValiDate("Y");
                tLCPolPojo.setCValiDate(lcContPojo.getCValiDate()); // 生效日
                tLCPolPojo.setEndDate(GlobalPojo.getRiskEndDate());
                tLCPolPojo.setAgentCode(lcContPojo.getAgentCode());
                tLCPolPojo.setSignDate(PubFun.getCurrentDate());
                String tSQL = "select distinct dutycode from ldplandutyparam where contplancode = '"
                        + contPlanCode
                        + "' "
                        + "and riskcode = '"
                        + tLDPlanRiskSchema.getRiskCode()
                        + "' order by dutycode";

                SSRS tSSRS = new SSRS();
                tSSRS = tExeSQL.execSQL(tSQL);
                List<LCDutyPojo>   tLCDutyPojoSetList = new ArrayList();
                if (tSSRS != null && tSSRS.getMaxRow() > 0) {

                    for (int s = 1; s <= tSSRS.getMaxRow(); s++) {
                        LDPlanDutyParamDB tLDPlanDutyParamDB = new LDPlanDutyParamDB();
                        tLDPlanDutyParamDB.setContPlanCode(contPlanCode);
                        tLDPlanDutyParamDB.setRiskCode(tLDPlanRiskSchema.getRiskCode());
                        tLDPlanDutyParamDB.setDutyCode(tSSRS.GetText(s, 1));

                        LDPlanDutyParamSet tLDPlanDutyParamSet = tLDPlanDutyParamDB.query();
                        if (tLDPlanDutyParamSet == null
                                || tLDPlanDutyParamSet.size() <= 0) {
                            CError.buildErr(this, "未查询到产品组合配置险种信息。");
                        }

                        LCDutyPojo tLCDutyPojo = new LCDutyPojo();
                        tLCDutyPojo.setDutyCode(tSSRS.GetText(s, 1));
                        tLCDutyPojo.setGetStartDate(tLCPolPojo.getCValiDate());
                        if (!transPlanToDuty(tLCDutyPojo,tLDPlanDutyParamSet)) {
                            CError.buildErr(this,"从保险计划向责任赋值失败!");
                            return null;
                        }

                        SSRS ssrs= tExeSQL.execSQL("select payendyear,payendyearflag,insuyear from lmduty where dutycode='"+tSSRS.GetText(s, 1)+"'");


                        tLCDutyPojo.setPayEndYear(ssrs.GetText(1,1));
                        tLCDutyPojo.setPayEndYearFlag(ssrs.GetText(1,2));
                        //        tLCDutyPojo.setInsuYear(ssrs.GetText(1,3));
                        tLCDutyPojo.setPayIntv(lcContPojo.getPayIntv());
                        tLCDutyPojo.setSSFlag("0");
                        tLCDutyPojoSetList.add(tLCDutyPojo);
                        tLCDutyPojoList.add(tLCDutyPojo);
                    }
                }
                double prem = 0.00;
                double amnt = 0.00;
                for (int s = 0; s < tLCDutyPojoSetList.size(); s++) {
                    prem = Arith.add(prem, tLCDutyPojoSetList.get(s).getPrem());
                    amnt = Arith.add(amnt, tLCDutyPojoSetList.get(s).getAmnt());
                }
                tLCPolPojo.setRiskCode(tLDPlanRiskSchema.getRiskCode()); // 险种编码
                //            tLCPolPojo.setdistriflag(tExeSQL.getOneValue("select decode(RinsFlag, 'N', '0', '1' ) from lmrisk where riskcode = '" + tLCPolPojo.getRiskCode() + "'"));
                tLCPolPojo.setAmnt(amnt); // 保额
                tLCPolPojo.setPrem(prem); // 保费

                lcContPojo.setPrem(prem);
                lcContPojo.setAmnt(amnt);
                tLCPolPojo.setPayYears(tLCDutyPojoList.get(1).getPayEndYear()); // 缴费期间
                tLCPolPojo.setPayIntv(lcContPojo.getPayIntv()); // 缴费频率
                tLCPolPojo.setInsuYear(tLCDutyPojoList.get(1).getInsuYear()); // 保障年期
                tLCPolPojo.setInsuYearFlag(tLCDutyPojoList.get(1).getInsuYearFlag()); // 保险期间类型
                tLCPolPojo.setPayEndYear(tLCDutyPojoList.get(1).getPayEndYear());
                tLCPolPojo.setPayEndYearFlag(tLCDutyPojoList.get(1).getPayEndYearFlag());
                tLCPolPojo.setRnewFlag(!PubFun.isEmpty(lcContPojo.getRnewFlag()+"") ? Integer.parseInt(lcContPojo.getRnewFlag()+"") : -2);
                lcPolPojoList.add(tLCPolPojo);
            }

            GlobalPojo.setMainRiskCode(tLDPlanRiskSet.get(1).getMainRiskCode());

        }

        return requestInfo;
    }


    private boolean transPlanToDuty(LCDutyPojo tLCDutyPojo, LDPlanDutyParamSet tLDPlanDutyParamSet) {
        String tDutyCode = tLCDutyPojo.getDutyCode();
        for (int i = 1; i <= tLDPlanDutyParamSet.size(); i++) {
            String CalFactor = tLDPlanDutyParamSet.get(i).getCalFactor();
            String CalFactorValue = tLDPlanDutyParamSet.get(i).getCalFactorValue();
            String tCalDutyCode = tLDPlanDutyParamSet.get(i).getDutyCode();
            if (tDutyCode.trim().equals(tCalDutyCode.trim())) {
                if (tLCDutyPojo.getFieldIndex(CalFactor) > -1) {
                    if (CalFactor.equalsIgnoreCase("Prem")
                            || CalFactor.equalsIgnoreCase("Amnt")
                            || CalFactor.equalsIgnoreCase("GetLimit")
                            || CalFactor.equalsIgnoreCase("GetRate")) {
                        tLCDutyPojo.setV(CalFactor, String.valueOf(Double.parseDouble(CalFactorValue) * 1));
                    } else {
                        tLCDutyPojo.setV(CalFactor, CalFactorValue);
                    }
                }
                tLCDutyPojo.setMult(1);
            }
        }
        return true;
    }

    public TradeInfo getContData(TradeInfo requestInfo) {
        try {
            //业务逻辑处理    投保人——保单————被保人———— 险种———责任————给付
            /**
             * 投保人保单结构处理
             * */
            logger.info("调用生成保单微服务传入的requestInfo为：" + requestInfo.toString());

            requestInfo = appntBL.dealData(requestInfo);
            if (requestInfo.hasError()) {
                LCAppntPojo lcAppntPojo = (LCAppntPojo) requestInfo.getData(LCAppntPojo.class.getName());
                logger.info("投保人编号为" + lcAppntPojo.getAppntNo() + "保单结构处理出错!" + requestInfo.getErrorList().toString() + requestInfo.toString());
                return requestInfo;
            }

            /**
             * 保单结构处理
             * */
            requestInfo = contBL.dealData(requestInfo);
            LCContPojo lcContPojo = (LCContPojo) requestInfo.getData(LCContPojo.class.getName());
            if (requestInfo.hasError()) {
                logger.info("contno" + lcContPojo.getContNo() + "保单结构出错" + requestInfo.getErrorList().toString() + requestInfo.toString());
                return requestInfo;
            }

            /**
             * 被保人保单结构
             * */
            requestInfo = insuredBL.dealData(requestInfo);
            LCInsuredPojo lcInsuredPojo = ((List<LCInsuredPojo>) requestInfo.getData(LCInsuredPojo.class.getName())).get(0);

            if (requestInfo.hasError()) {
                logger.info("被保人" + lcInsuredPojo.getInsuredNo() + "被保人保单结构出错！" + requestInfo.getErrorList().toString() + requestInfo.toString());
                return requestInfo;
            }
            /**
             * 险种  受益人结构处理
             * */
            /*requestInfo = polBL.dealData(requestInfo);
            if (requestInfo.hasError()) {
                logger.info("受益人保单结构出错！" + requestInfo.getErrorList().toString() + requestInfo.toString());
                return requestInfo;
            }*/
        } catch (Exception e) {
            requestInfo.addError(ExceptionUtils.exceptionToString(e));
            e.printStackTrace();
            LCContPojo lcContPojo = (LCContPojo) requestInfo.getData(LCContPojo.class.getName());
            logger.error("ContInputService 中  contno" + lcContPojo.getContNo() + "保单结构出错" + ExceptionUtils.exceptionToString(e) + requestInfo.toString());
        }
        logger.info("调用生成保单微服务成功，返回的结果为：" + requestInfo.toString());
        return requestInfo;
    }


}
