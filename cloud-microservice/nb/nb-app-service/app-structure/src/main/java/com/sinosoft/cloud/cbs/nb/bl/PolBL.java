package com.sinosoft.cloud.cbs.nb.bl;

import com.sinosoft.cache.dao.impl.RedisCommonDao;
import com.sinosoft.cloud.bl.AbstractBL;
import com.sinosoft.cloud.nb.app.client.CustomerNoSynchronizeService;
import com.sinosoft.cloud.rest.TradeInfo;
import com.sinosoft.lis.entity.*;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.utility.ExceptionUtils;
import com.sinosoft.utility.SysConst;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * 此类生成保单险种层级的相关数据
 */
@Component
public class PolBL extends AbstractBL {
    private final Log logger = LogFactory.getLog(getClass());
    private static final String OPERATOR = "ABC-CLOUD";

    @Autowired
    RedisCommonDao redisCommonDao;
    @Autowired
    CustomerNoSynchronizeService customerNoSynchronizeClient;
    private final String polLimit = "026";
    @Value("${cloud.nb.barrier.control}")
    private String barrier;

    @Override
    public TradeInfo dealData(TradeInfo requestInfo) {
        try {
            //生成lcpol信息
            this.dealPols(requestInfo);
            if (requestInfo.hasError()) {
                return requestInfo;
            }
            //生成Lcbnf信息
            this.dealBnfs(requestInfo);
            if (requestInfo.hasError()) {
                return requestInfo;
            }

        } catch (Exception e) {
            e.printStackTrace();
            logger.error(ExceptionUtils.exceptionToString(e));
            requestInfo.addError(ExceptionUtils.exceptionToString(e));
            return requestInfo;
        }
        return requestInfo;
    }

    private void dealBnfs(TradeInfo tradeInfo) {
        LCInsuredPojo lcInsuredPojo = ((List<LCInsuredPojo>) tradeInfo.getData(LCInsuredPojo.class.getName())).get(0);
        List<LCPolPojo> lcpolList = (List<LCPolPojo>) tradeInfo.getData(LCPolPojo.class.getName());


        //end
        if (lcpolList == null || lcpolList.size() == 0) {
            tradeInfo.addError("lcpol数据为空");
        } else {
            List<LCBnfPojo> bnfList = new ArrayList<>();
            LCBnfPojo lcBnfPojo = new LCBnfPojo();
            lcBnfPojo.setBnfID(PubFun1.CreateMaxNo(PubFun1.PhysicNo, 22));
            lcBnfPojo.setName("法定");
            lcBnfPojo.setPolNo(lcpolList.get(0).getPolNo());
            lcBnfPojo.setContNo(lcpolList.get(0).getContNo());
            lcBnfPojo.setInsuredNo(lcInsuredPojo.getInsuredNo());
            lcBnfPojo.setCustomerNo(lcInsuredPojo.getInsuredNo());
            lcBnfPojo.setBnfType("1");
            lcBnfPojo.setBnfNo(PubFun1.CreateMaxNo(PubFun1.PhysicNo, 22));
            lcBnfPojo.setBnfGrade("1");
            lcBnfPojo.setModifyDate(PubFun.getCurrentDate());
            lcBnfPojo.setModifyTime(PubFun.getCurrentTime());
            lcBnfPojo.setOperator(OPERATOR);
            lcBnfPojo.setMakeDate(PubFun.getCurrentDate());
            lcBnfPojo.setMakeTime(PubFun.getCurrentTime());
            lcBnfPojo.setPolID(lcpolList.get(0).getPolID());
            bnfList.add(lcBnfPojo);
            tradeInfo.addData(LCBnfPojo.class.getName(), bnfList);
        }
    }

    /**
     * 处理险种信息
     */
    private void dealOnePol(LCContPojo lcContPojo, LCPolPojo lcPolPojo, TradeInfo tradeInfo) {
        //C066处理
        LMRiskAppPojo lmRiskAppPojo = redisCommonDao.getEntityRelaDB(LMRiskAppPojo.class, lcPolPojo.getRiskCode());
        if (lmRiskAppPojo != null) {
            String polNo = createPolNo();

            if ("M".equals(lmRiskAppPojo.getSubRiskFlag())) {
                lcPolPojo.setMainPolNo(polNo);
            }
            lcPolPojo.setPolID(PubFun1.CreateMaxNo(PubFun1.PhysicNo, 22));
            lcPolPojo.setContID(lcContPojo.getContID());
            lcPolPojo.setShardingID(lcContPojo.getPrtNo());
            lcPolPojo.setGrpContNo(SysConst.ZERONO);
            lcPolPojo.setGrpPolNo(SysConst.ZERONO);
            lcPolPojo.setContNo(lcContPojo.getContNo());
            lcPolPojo.setInsuredNo(lcContPojo.getInsuredNo());
            //生成Polno
            lcPolPojo.setPolNo(polNo);
            lcPolPojo.setProposalNo(polNo);
            lcPolPojo.setPrtNo(lcContPojo.getPrtNo());
            lcPolPojo.setAgentCode(lcContPojo.getAgentCode());
            lcPolPojo.setAgentGroup(lcContPojo.getAgentGroup());
            lcPolPojo.setOperator(OPERATOR);
            lcPolPojo.setMakeDate(PubFun.getCurrentDate());
            lcPolPojo.setMakeTime(PubFun.getCurrentTime());
            lcPolPojo.setModifyDate(PubFun.getCurrentDate());
            lcPolPojo.setModifyTime(PubFun.getCurrentTime());
            lcPolPojo.setGetBankCode(lcContPojo.getBankCode());
            //add by rs 20190930
            lcPolPojo.setMult("1");
            //end
            /**
             * 趸交判断 如果是趸交 ：PayEndYear 1000 PayEndYearFlag Y
             */
            if(lcPolPojo.getPayIntv()==0){
                lcPolPojo.setPayEndYear(1000);
                lcPolPojo.setPayEndYearFlag("Y");
            }
            List<LCBnfPojo> lcBnfList = (List<LCBnfPojo>) tradeInfo.getData(LCBnfPojo.class.getName());
            if (lcBnfList != null && lcBnfList.size() > 0) {
                lcPolPojo.setBnfFlag("1");
            } else {
                lcPolPojo.setBnfFlag("0");
            }

            lcPolPojo.setManageCom(lcContPojo.getManageCom());
            lcPolPojo.setSpecifyValiDate("N");
            lcPolPojo.setUWFlag("0");
            lcPolPojo.setImpartFlag("0");
            lcPolPojo.setSumPrem(lcPolPojo.getPrem());
            lcPolPojo.setAppFlag("0");
            lcPolPojo.setContType(lcContPojo.getContType());
            lcPolPojo.setPolTypeFlag("0"); //0-个人单 1--无名单
            lcPolPojo.setSaleChnl(lcContPojo.getSaleChnl());
            lcPolPojo.setInsuredSex(lcContPojo.getInsuredSex());
            lcPolPojo.setInsuredName(lcContPojo.getInsuredName());
            lcPolPojo.setInsuredNo(lcContPojo.getInsuredNo());
            lcPolPojo.setInsuredBirthday(lcContPojo.getInsuredBirthday());
            lcPolPojo.setInsuredName(lcContPojo.getInsuredName());



            //nonyin
            lcPolPojo.setLastRevDate(lcPolPojo.getCValiDate()); // 把最近复效日期置为起保日期
            lcPolPojo.setSignCom(lcContPojo.getManageCom());
            lcPolPojo.setInsurPolFlag("0");
            lcPolPojo.setInsuredAppAge(PubFun.calInterval(lcContPojo.getInsuredBirthday(), lcPolPojo.getCValiDate(), "Y"));
            lcPolPojo.setInsuredPeoples("1");
            //投保规则调整
            lcPolPojo.setProdSetCode(lcContPojo.getProdSetCode());
            lcPolPojo.setInsuredBirthday(lcContPojo.getInsuredBirthday());
            lcPolPojo.setAppFlag("0");
            lcPolPojo.setBonusMan("0"); //红利金领取人
            lcPolPojo.setAppntNo(lcContPojo.getAppntNo());
            lcPolPojo.setAppntName(lcContPojo.getAppntName());
            //增加prem
            lcPolPojo.setSumPrem(lcPolPojo.getPrem());
            lcPolPojo.setAgentGroup(lcContPojo.getAgentGroup());
            /**
             * 添加备用字段
             * add by rs 20191220
             *
             */
            LCInsuredPojo lcInsuredPojo = ((List<LCInsuredPojo>) tradeInfo.getData(LCInsuredPojo.class.getName())).get(0);
            lcPolPojo.setStandbyFlag1(lcInsuredPojo.getContPlanCode());
            /**----原来不存值，后来为了给核心的lcpol的paymode存值要求加的---**/
            lcPolPojo.setPayLocation("0");
            if (lcPolPojo.getAgentGroup() == null || "".equals(lcPolPojo.getAgentGroup())) {
                lcPolPojo.setAgentGroup(lcContPojo.getAgentCode());
            }
        } else {
            logger.error("根据riskcode" + lcPolPojo.getRiskCode() + "未从redis中查询到对应的LMRiskAppPojo");
            tradeInfo.addError("根据riskcode" + lcPolPojo.getRiskCode() + "未从redis中查询到对应的LMRiskAppPojo");
        }
    }

    private void dealPols(TradeInfo tradeInfo) {
        LCContPojo lcContPojo = (LCContPojo) tradeInfo.getData(LCContPojo.class.getName());
        List<LCPolPojo> lcpolList = (List<LCPolPojo>) tradeInfo.getData(LCPolPojo.class.getName());
        if (lcpolList == null || lcpolList.size() == 0) {
//            tradeInfo.addError("lcpol数据为空");
        } else {
            for (LCPolPojo lcpolPojo : lcpolList) {
                this.dealOnePol(lcContPojo, lcpolPojo, tradeInfo);
            }
        }
    }

    public String createPolNo() {
        String maxNo = PubFun1.CreateMaxNo("ProposalNo", polLimit);
        maxNo = PubFun.LCh(maxNo, "0", 19);
        maxNo = "1" + maxNo;
        logger.info("polLimit=" + polLimit + "PolBL生成的polno=" + maxNo);
        return maxNo;
    }

    public String getBarrier() {
        return barrier;
    }

    public void setBarrier(String barrier) {
        this.barrier = barrier;
    }
}
