package com.sinosoft.cloud.cbs.nb.demoBL;

import com.sinosoft.cloud.common.TradeToStorage;
import com.sinosoft.cloud.rest.TradeInfo;
import com.sinosoft.lis.entity.*;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.utility.ExceptionUtils;
import com.sinosoft.utility.Reflections;
import com.sinosoft.utility.VData;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.ArrayList;
import java.util.List;

public class NBDataStoreBL {

    private final Log logger = LogFactory.getLog(this.getClass());

    public TradeInfo dealData(TradeInfo requestInfo) {
        try {
            logger.debug("契约落库开始......");
            //传入统一接入的数据
            requestInfo.clearStorage();
            List<LDPersonPojo> ldPersonPojos = (List<LDPersonPojo>) requestInfo.getData(LDPersonPojo.class.getName());
            //先判断ldperson的数据是否是新客户，只有是新客户，ldperson才需要落库和同步给数据交互平台
            Reflections reflections = new Reflections();
            List<LDPersonPojo> tLDPersonPojos = new ArrayList<LDPersonPojo>();

            if (ldPersonPojos != null && ldPersonPojos.size() > 0) {
                for (int i = 0; i < ldPersonPojos.size(); i++) {
                    LDPersonPojo ldPersonPojo = ldPersonPojos.get(i);
                    if (!"NEW".equals(ldPersonPojo.getNewCustomerFlag())) {
                        ldPersonPojos.remove(i);
                        i--;
                    }
                    tLDPersonPojos.add((LDPersonPojo) reflections.transFields(new LDPersonPojo(), ldPersonPojo));
                }
                if (ldPersonPojos.size() > 0) {
                    requestInfo.doInsert(LDPersonPojo.class.getName());
                }
            }

            LCContPojo lcContPojo = (LCContPojo) requestInfo.getData(LCContPojo.class.getName());
            if (lcContPojo != null) {
                requestInfo.doInsert(LCContPojo.class.getName());
            }
            LCAppntPojo lcAppntPojo = (LCAppntPojo) requestInfo.getData(LCAppntPojo.class.getName());
            if (lcAppntPojo != null) {
                requestInfo.doInsert(LCAppntPojo.class.getName());
            }
            List<LCInsuredPojo> lcInsuredPojos = (List<LCInsuredPojo>) requestInfo.getData(LCInsuredPojo.class.getName());
            if (lcInsuredPojos != null && lcInsuredPojos.size() != 0) {
                requestInfo.doInsert(LCInsuredPojo.class.getName());
            }
            List<LCPolPojo> lcPolPojos = (List<LCPolPojo>) requestInfo.getData(LCPolPojo.class.getName());
            if (lcPolPojos != null && lcPolPojos.size() > 0) {
                requestInfo.doInsert(LCPolPojo.class.getName());
            }
            //受益人
            ArrayList<LCBnfPojo> lcBnfPojos = (ArrayList) requestInfo.getData(LCBnfPojo.class.getName());
            if (lcBnfPojos != null && lcBnfPojos.size() > 0) {
                requestInfo.doInsert(LCBnfPojo.class.getName());
            }
            List<LCCustomerImpartPojo> lcCustomerImpartPojoList =
                    (List<LCCustomerImpartPojo>) requestInfo.getData(LCCustomerImpartPojo.class.getName());
            if (lcCustomerImpartPojoList != null && lcCustomerImpartPojoList.size() > 0) {
                requestInfo.doInsert(LCCustomerImpartPojo.class.getName());
            }
            List<LCCustomerImpartParamsPojo> lcCustomerImpartParamsPojos =
                    (List<LCCustomerImpartParamsPojo>) requestInfo.getData(LCCustomerImpartParamsPojo.class.getName());
            if (lcCustomerImpartParamsPojos != null && lcCustomerImpartParamsPojos.size() > 0) {
                requestInfo.doInsert(LCCustomerImpartParamsPojo.class.getName());
            }
            List<LCDutyPojo> lcDutyPojos = (List<LCDutyPojo>) requestInfo.getData(LCDutyPojo.class.getName());
            if (lcDutyPojos != null && lcDutyPojos.size() > 0) {
                requestInfo.doInsert(LCDutyPojo.class.getName());
            }
            List<LCGetPojo> lcGetPojos = (List<LCGetPojo>) requestInfo.getData(LCGetPojo.class.getName());
            if (lcGetPojos != null && lcGetPojos.size() > 0) {
                requestInfo.doInsert(LCGetPojo.class.getName());
            }
            List<LCPremPojo> lcPremPojos = (List<LCPremPojo>) requestInfo.getData(LCPremPojo.class.getName());
            if (lcPremPojos != null && lcPremPojos.size() > 0) {
                requestInfo.doInsert(LCPremPojo.class.getName());
            }
            LCCUWMasterPojo tLCCUWMasterPojos = (LCCUWMasterPojo) requestInfo.getData(LCCUWMasterPojo.class.getName());
            if (tLCCUWMasterPojos != null) {
                requestInfo.doInsert(LCCUWMasterPojo.class.getName());
            }
            LCCUWSubPojo tLCCUWSubPojoList =
                    (LCCUWSubPojo) requestInfo.getData(LCCUWSubPojo.class.getName());
            if (tLCCUWSubPojoList != null) {
                requestInfo.doInsert(LCCUWSubPojo.class.getName());
            }
            List<LCCUWErrorPojo> tLCCUWErrorPojoList =
                    (List<LCCUWErrorPojo>) requestInfo.getData(LCCUWErrorPojo.class.getName());
            if (tLCCUWErrorPojoList != null && tLCCUWErrorPojoList.size() > 0) {
                requestInfo.doInsert(LCCUWErrorPojo.class.getName());
            }
            List<LCUWMasterPojo> lcuwMasterPojos = (List) requestInfo.getData(LCUWMasterPojo.class.getName());
            if (lcuwMasterPojos != null && lcuwMasterPojos.size() > 0) {
                requestInfo.doInsert(LCUWMasterPojo.class.getName());
            }
            List<LCUWSubPojo> lcuwSubPojos = (List) requestInfo.getData(LCUWSubPojo.class.getName());
            if (lcuwSubPojos != null && lcuwSubPojos.size() > 0) {
                requestInfo.doInsert(LCUWSubPojo.class.getName());
            }
            List<LCUWErrorPojo> lcuwErrorPojos = (List<LCUWErrorPojo>) requestInfo.getData(LCUWErrorPojo.class.getName());
            if (lcuwErrorPojos != null && lcuwErrorPojos.size() > 0) {
                requestInfo.doInsert(LCUWErrorPojo.class.getName());
            }
            List<LCAddressPojo> lcAddressPojoList = (List<LCAddressPojo>) requestInfo.getData(LCAddressPojo.class.getName());
            if (lcAddressPojoList != null && lcAddressPojoList.size() > 0) {
                requestInfo.doInsert(LCAddressPojo.class.getName());
            }
            //保单状态表
            LCContStatePojo lcContStatePojo = (LCContStatePojo) requestInfo.getData(LCContStatePojo.class.getName());
            if (lcContStatePojo != null) {
                requestInfo.doInsert(LCContStatePojo.class.getName());
            }

            //保单保费项和账户关联表
            List<LCPremToAccPojo> lcPremToAccPojos = (List<LCPremToAccPojo>) requestInfo.getData(LCPremToAccPojo.class.getName());
            if (lcPremToAccPojos != null && lcPremToAccPojos.size() > 0) {
                requestInfo.doInsert(LCPremToAccPojo.class.getName());
            }

            //保单领取项和账户关联表
            List<LCGetToAccPojo> lcGetToAccPojos = (List<LCGetToAccPojo>) requestInfo.getData(LCGetToAccPojo.class.getName());
            if (lcGetToAccPojos != null && lcGetToAccPojos.size() > 0) {
                requestInfo.doInsert(LCGetToAccPojo.class.getName());
            }

            //保单账户分类表
            List<LCInsureAccClassPojo> lcInsureAccClassPojos = (List<LCInsureAccClassPojo>) requestInfo.getData(LCInsureAccClassPojo.class.getName());
            if (lcInsureAccClassPojos != null && lcInsureAccClassPojos.size() > 0) {
                requestInfo.doInsert(LCInsureAccClassPojo.class.getName());
            }

            //保单账户管理费分类表
            List<LCInsureAccClassFeePojo> lcInsureAccClassFeePojos = (List<LCInsureAccClassFeePojo>) requestInfo.getData(LCInsureAccClassFeePojo.class.getName());
            if (lcInsureAccClassFeePojos != null && lcInsureAccClassFeePojos.size() > 0) {
                requestInfo.doInsert(LCInsureAccClassFeePojo.class.getName());
            }

            //保单账户管理费轨迹表
            List<LCInsureAccFeeTracePojo> lcInsureAccFeeTracePojos = (List<LCInsureAccFeeTracePojo>) requestInfo.getData(LCInsureAccFeeTracePojo.class.getName());
            if (lcInsureAccFeeTracePojos != null && lcInsureAccFeeTracePojos.size() > 0) {
                requestInfo.doInsert(LCInsureAccFeeTracePojo.class.getName());
            }

            List<LCInsureAccPojo> lcInsureAccPojos = (List<LCInsureAccPojo>) requestInfo.getData(LCInsureAccPojo.class.getName());
            if (lcInsureAccPojos != null && lcInsureAccPojos.size() > 0) {
                requestInfo.doInsert(LCInsureAccPojo.class.getName());
            }
            //新交易流水号存储
            List<LKTransTracksPojo> lkTransTracksPojos = (List<LKTransTracksPojo>) requestInfo.getData(LKTransTracksPojo.class.getName());
            if (lkTransTracksPojos != null && lkTransTracksPojos.size() > 0) {
                requestInfo.doInsert(LKTransTracksPojo.class.getName());
            }
            //新交易流水号存储
            List<LogLktranStracksPojo> tLogList = (List<LogLktranStracksPojo>) requestInfo.getData(LogLktranStracksPojo.class.getName());
            if (lkTransTracksPojos != null && lkTransTracksPojos.size() > 0) {
                requestInfo.doInsert(LKTransTracksPojo.class.getName());
            }

            //统一落库
            this.submitData(requestInfo);
            if (requestInfo.hasError()) {
                logger.error("1002-契约落库失败！");
                return requestInfo;
            }

            requestInfo.addData(LDPersonPojo.class.getName(), tLDPersonPojos);
        } catch (Exception e) {
            e.printStackTrace();
            logger.error(ExceptionUtils.exceptionToString(e));
            requestInfo.addError(ExceptionUtils.exceptionToString(e));
        }
        return requestInfo;
    }

    private TradeInfo submitData(TradeInfo requestInfo) {
        logger.debug("oracle.jdbc.DateZeroTime的值NBDataStoreBL="+System.getProperty("oracle.jdbc.DateZeroTime"));
        logger.debug("契约落库开始......");
        //统一落库
        try {
            TradeToStorage tradeToStorage = new TradeToStorage();
            VData vData = tradeToStorage.transfer(requestInfo);

            PubSubmit pubSubmit = new PubSubmit();
            if (!pubSubmit.submitData(vData)) {
                requestInfo.addError("契约落库失败......");
                logger.error("契约落库失败......"+requestInfo);
                logger.error(pubSubmit.mErrors.getErrContent());
                return requestInfo;
            }
            logger.debug("契约落库成功......");
        } catch (Exception e) {
            logger.error("契约落库异常......");
            logger.error(ExceptionUtils.exceptionToString(e));
            requestInfo.addError("契约落库异常......");
            e.printStackTrace();
        }
        return requestInfo;
    }
}
