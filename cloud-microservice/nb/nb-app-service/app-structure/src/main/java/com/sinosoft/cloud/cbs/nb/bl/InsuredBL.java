package com.sinosoft.cloud.cbs.nb.bl;

import com.sinosoft.cloud.bl.AbstractBL;
import com.sinosoft.cloud.rest.TradeInfo;
import com.sinosoft.lis.db.LCInsuredDB;
import com.sinosoft.lis.entity.*;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.utility.ExceptionUtils;
import com.sinosoft.utility.SysConst;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.List;


@Component
public class InsuredBL extends AbstractBL {

    private final Log logger = LogFactory.getLog(getClass());
    private static final String OPERATOR = "ABC-CLOUD";

    @Value("${cloud.nb.barrier.control}")
    private String barrier;

    private static NewCustomerCreate idWorker=new NewCustomerCreate(0,0);

    @Override
    public TradeInfo dealData(TradeInfo requestInfo) {
        logger.info("--------------保单结构被保人———————————");
        try {
            //获取数据
            LCContPojo lcContPojo = (LCContPojo) requestInfo.getData(LCContPojo.class.getName());
            List<LCInsuredPojo> lcInsuredPojos = ((List<LCInsuredPojo>) requestInfo.getData(LCInsuredPojo.class.getName()));
            LCInsuredPojo lcInsuredPojo = lcInsuredPojos.get(0);

            //处理被保人信息
            this.dealInsured(lcContPojo, lcInsuredPojo, OPERATOR, requestInfo);
            //被保人客户化
            this.dealInsuredCust(requestInfo, lcContPojo, lcInsuredPojo, OPERATOR);

            if (requestInfo.hasError()) {
                return requestInfo;
            }

        } catch (Exception e) {
            e.printStackTrace();
            logger.error(ExceptionUtils.exceptionToString(e));
            requestInfo.addError(ExceptionUtils.exceptionToString(e));
            return requestInfo;
        }
        return requestInfo;
    }

    /**
     * 被保人客户化
     */
    private TradeInfo dealInsuredCust(TradeInfo requestInfo, LCContPojo lcContPojo, LCInsuredPojo lcInsuredPojo,
                                      String operator) {
        int addressNo = 0;

        logger.debug("========InsuredBL挡板开启===========");
        String customerNo = String.valueOf(idWorker.nextId())+ NewCustomerCreate.getUUID(6);//后续调用数据交互提供生成客户号的服务
        createNewPerson(requestInfo, operator, lcInsuredPojo, customerNo);

        lcInsuredPojo.setAddressNo(String.valueOf(addressNo));
        lcInsuredPojo.setInsuredNo(customerNo);
        lcContPojo.setInsuredNo(customerNo);
        //业务校验

        if (!StringUtils.isEmpty(lcInsuredPojo.getInsuredNo())) {
            LCInsuredDB tLCInsuredDB = new LCInsuredDB();
            tLCInsuredDB.setInsuredID(lcInsuredPojo.getInsuredID());
            if (tLCInsuredDB.getInfo()) {
                logger.debug("不能重复添加同一客户");
                requestInfo.addError("不能重复添加同一客户!  ");
                return requestInfo;
            }
        }
        lcInsuredPojo.setRelationToMainInsured("00");


        return requestInfo;
    }


    /**
     * 处理被保人信息
     */
    private void dealInsured(LCContPojo lcContPojo, LCInsuredPojo lcInsuredPojo, String operator, TradeInfo tradeInfo) {
        GlobalPojo globalPojo = (GlobalPojo) tradeInfo.getData(GlobalPojo.class.getName());
        //设置主外键
        lcInsuredPojo.setInsuredID(PubFun1.CreateMaxNo(PubFun1.PhysicNo, 22));
        //
        lcContPojo.setInsuredIDNo(lcInsuredPojo.getIDNo());
        lcInsuredPojo.setContID(lcContPojo.getContID());
        lcInsuredPojo.setContNo(lcContPojo.getContNo());
        lcInsuredPojo.setPrtNo(lcContPojo.getPrtNo());
        lcInsuredPojo.setAppntNo(lcContPojo.getAppntNo());
        lcInsuredPojo.setContPlanCode(globalPojo.getMainRiskCode());
        lcInsuredPojo.setGrpContNo(SysConst.ZERONO);
        lcInsuredPojo.setManageCom(lcContPojo.getManageCom());
        lcInsuredPojo.setExecuteCom(lcContPojo.getManageCom());
        lcInsuredPojo.setSequenceNo("1");
        lcInsuredPojo.setFamilyID(lcContPojo.getFamilyID());
        lcInsuredPojo.setMakeDate(PubFun.getCurrentDate());
        lcInsuredPojo.setMakeTime(PubFun.getCurrentTime());
        lcInsuredPojo.setModifyDate(PubFun.getCurrentDate());
        lcInsuredPojo.setModifyTime(PubFun.getCurrentTime());
        lcInsuredPojo.setOperator(operator);
        lcInsuredPojo.setShardingID(lcContPojo.getPrtNo());
        lcInsuredPojo.setPrtNo(lcContPojo.getPrtNo());


    }

    private TradeInfo createNewPerson(TradeInfo tradeInfo, String operator, LCInsuredPojo lcInsuredPojo, String customerNo) {
        List<LDPersonPojo> ldPersonPojos = (List<LDPersonPojo>) tradeInfo.getData(LDPersonPojo.class.getName());
        LCContPojo lcContPojo = (LCContPojo) tradeInfo.getData(LCContPojo.class.getName());
        LCAppntPojo lcAppntPojo = (LCAppntPojo) tradeInfo.getData(LCAppntPojo.class.getName());
        for (LDPersonPojo ldPersonPojo : ldPersonPojos) {
            //被保人信息
            if (customerNo.equals(ldPersonPojo.getCustomerNo())) {
                lcInsuredPojo.setPersonID(lcAppntPojo.getPersonID());
                lcInsuredPojo.setAddressNo(lcAppntPojo.getAddressNo());
                return tradeInfo;
            }
        }
        LDPersonPojo ldPersonPojo = new LDPersonPojo();
        ldPersonPojo.setShardingID(lcContPojo.getPrtNo());
        ldPersonPojo.setPersonID(PubFun1.CreateMaxNo(PubFun1.PhysicNo, 22));
        lcInsuredPojo.setPersonID(ldPersonPojo.getPersonID());
        ldPersonPojo.setCustomerNo(customerNo);
        ldPersonPojo.setPassword(customerNo.substring(customerNo.length() - 6, customerNo.length()));
        ldPersonPojo.setName(lcInsuredPojo.getName());
        ldPersonPojo.setFirstName(lcInsuredPojo.getFirstName());
        ldPersonPojo.setLastName(lcInsuredPojo.getLastName());
        ldPersonPojo.setSex(lcInsuredPojo.getSex());
        ldPersonPojo.setBirthday(lcInsuredPojo.getBirthday());
        ldPersonPojo.setIDType(lcInsuredPojo.getIDType());
        ldPersonPojo.setIDNo(lcInsuredPojo.getIDNo());
        ldPersonPojo.setNativePlace(lcInsuredPojo.getNativePlace());
        ldPersonPojo.setNationality(lcInsuredPojo.getNationality());
        ldPersonPojo.setRgtAddress(lcInsuredPojo.getRgtAddress());
        ldPersonPojo.setMarriage(lcInsuredPojo.getMarriage());
        ldPersonPojo.setDegree(lcInsuredPojo.getDegree());
        ldPersonPojo.setOccupationType(lcInsuredPojo.getOccupationType());
        ldPersonPojo.setOccupationCode(lcInsuredPojo.getOccupationCode());
        ldPersonPojo.setWorkType(lcInsuredPojo.getWorkType());
        ldPersonPojo.setPosition(lcInsuredPojo.getPosition());
        ldPersonPojo.setPluralityType(lcInsuredPojo.getPluralityType());
        ldPersonPojo.setHaveMotorcycleLicence(lcInsuredPojo.getHaveMotorcycleLicence());
        ldPersonPojo.setPartTimeJob(lcInsuredPojo.getPartTimeJob());
        ldPersonPojo.setSmokeFlag(lcInsuredPojo.getSmokeFlag());
        ldPersonPojo.setMakeDate(PubFun.getCurrentDate());
        ldPersonPojo.setMakeTime(PubFun.getCurrentTime());
        ldPersonPojo.setModifyDate(PubFun.getCurrentDate());
        ldPersonPojo.setModifyTime(PubFun.getCurrentTime());
        ldPersonPojo.setOperator(operator);
        ldPersonPojo.setLicenseType(lcInsuredPojo.getLicenseType());
        ldPersonPojo.setIdValiDate(lcInsuredPojo.getIdValiDate());
        ldPersonPojo.setTINFlag(lcInsuredPojo.getTINFlag());
        ldPersonPojo.setTINNO(lcInsuredPojo.getTINNO());
        ldPersonPojo.setNewCustomerFlag("NEW");
        ldPersonPojo.setSSFlag(lcInsuredPojo.getSSFlag());
        ldPersonPojos.add(ldPersonPojo);
        return tradeInfo;

    }

    public String getBarrier() {
        return barrier;
    }

    public void setBarrier(String barrier) {
        this.barrier = barrier;
    }
}
