package com.sinosoft.cloud.cbs.nb.bean;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * Created by zhangfei on 2018/9/30.
 */
@Component
public class ReflashBean {

    private String barrier;

    public String getBarrier() {
        return barrier;
    }

    public void setBarrier(String barrier) {
        this.barrier = barrier;
    }

    @Override
    public String toString() {
        return "ReflashBean{" +
                "barrier='" + barrier + '\'' +
                '}';
    }
}
