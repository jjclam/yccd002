package com.sinosoft.cloud.nb.app.client;

import com.sinosoft.cloud.rest.TradeInfo;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by Administrator on 2017/10/14.
 * 保单号生成微服务接口
 */
@FeignClient(value = "nb-app-service")
public interface PolicyNoService {
    @RequestMapping(method = RequestMethod.POST, path = "/rest/PolicyNoService/PolicyNoService")
    TradeInfo service(TradeInfo tradeInfo);
}
