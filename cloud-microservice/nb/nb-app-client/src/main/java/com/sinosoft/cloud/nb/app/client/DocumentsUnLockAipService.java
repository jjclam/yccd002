package com.sinosoft.cloud.nb.app.client;

import com.sinosoft.cloud.rest.TradeInfo;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by zhangfei on 2018/7/5.
 */
@FeignClient(value = "nb-app-service")
public interface DocumentsUnLockAipService {
    @RequestMapping(method = RequestMethod.POST, path = "/rest/DocumentsUnLockAipService/DocumentsUnLockAipService")
    TradeInfo service(TradeInfo tradeInfo);
}
