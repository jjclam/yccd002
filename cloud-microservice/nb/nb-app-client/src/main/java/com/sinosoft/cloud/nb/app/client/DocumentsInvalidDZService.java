package com.sinosoft.cloud.nb.app.client;

import com.sinosoft.cloud.rest.TradeInfo;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by zhangfei on 2018/7/16.
 */
@FeignClient(value = "nb-app-service")
public interface DocumentsInvalidDZService {
    @RequestMapping(method = RequestMethod.POST, path = "/rest/DocumentsInvalidDZService/DocumentsInvalidDZService")
    TradeInfo service(TradeInfo tradeInfo);
}
