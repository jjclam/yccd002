package com.sinosoft.cloud.nb.app.client;

import com.sinosoft.cloud.rest.TradeInfo;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by grs on 2017/8/10.
 * 转保核心更新轨迹客户端
 */
@FeignClient(value = "nb-app-service")
public interface CoreDataStoreService {
    @RequestMapping(method = RequestMethod.POST, path = "/rest/CoreDataStoreService/CoreDataStoreService")
    TradeInfo service(TradeInfo tradeInfo);
}
