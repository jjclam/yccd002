package com.sinosoft.cloud.nb.app.client;

import com.sinosoft.cloud.rest.TradeInfo;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * 调用蚂蚁平台生成虚拟号
 */

@FeignClient(value = "nb-app-service")
public interface AliVirtualNumberService {
    @RequestMapping(method = RequestMethod.POST, path = "/rest/AliVirtualNumberService/AliVirtualNumberService")
    TradeInfo service(TradeInfo tradeInfo);

}
