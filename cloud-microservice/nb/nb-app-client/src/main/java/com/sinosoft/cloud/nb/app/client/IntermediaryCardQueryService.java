package com.sinosoft.cloud.nb.app.client;

import com.sinosoft.cloud.rest.TradeInfo;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * 获取中介卡单信息客户端
 */
@FeignClient(value = "nb-app-service")
public interface IntermediaryCardQueryService {
    @RequestMapping(method = RequestMethod.POST, path = "/rest/IntermediaryCardQueryService/IntermediaryCardQueryService")
    TradeInfo service(TradeInfo tradeInfo);
}
