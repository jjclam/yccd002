package com.sinosoft.cloud.nb.app.client;

import com.sinosoft.cloud.rest.TradeInfo;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by grs on 2017/9/27.
 * 保单结构生成客户端
 */
@FeignClient(value = "nb-app-service")
public interface LimitPremChkService {
    @RequestMapping(method = RequestMethod.POST, path = "/rest/LimitPremChkService/LimitPremChkService")
    TradeInfo service(TradeInfo tradeInfo);
}
