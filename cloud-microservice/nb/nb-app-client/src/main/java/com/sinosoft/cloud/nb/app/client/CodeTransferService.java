package com.sinosoft.cloud.nb.app.client;


import com.sinosoft.cloud.rest.TradeInfo;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * 复核数据编码转换客户端
 */

@FeignClient(value = "microservice-application")
public interface CodeTransferService {

    @RequestMapping(method = RequestMethod.POST, path = "/rest/CodeTransferService/CodeTransferService")
    TradeInfo service(TradeInfo tradeInfo);
}