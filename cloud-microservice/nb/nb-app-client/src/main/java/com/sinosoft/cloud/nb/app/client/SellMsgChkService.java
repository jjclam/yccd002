package com.sinosoft.cloud.nb.app.client;

import com.sinosoft.cloud.rest.TradeInfo;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


/**
 * 6807 校验销售信息
 */
@FeignClient(value = "nb-app-service")
public interface SellMsgChkService {
    @RequestMapping(method = RequestMethod.POST, path = "/rest/SellMsgChkService/SellMsgChkService")
    TradeInfo service(TradeInfo tradeInfo);
}
