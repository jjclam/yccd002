package com.sinosoft.cloud.nb.app.client;

import com.sinosoft.cloud.rest.TradeInfo;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * @Author:WangShuliang
 * @Description:
 * @Date:Created in 14:26 2017/11/13
 * @Modified by:
 */
@FeignClient(value = "nb-app-service")
public interface NbDataSynchronismService {
    @RequestMapping(method = RequestMethod.POST, path = "/rest/NbDataSynchronismService/NbDataSynchronismService")
    TradeInfo service(TradeInfo tradeInfo);
}
