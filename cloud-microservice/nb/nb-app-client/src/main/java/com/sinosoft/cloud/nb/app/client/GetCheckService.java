package com.sinosoft.cloud.nb.app.client;

import com.sinosoft.cloud.rest.TradeInfo;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
* @Author: wupengqian
* @Description: 幂等性校验.
* @Date:   2018/9/4
*
*/
@FeignClient(value = "nb-app-service")
public interface GetCheckService {
    @RequestMapping(method = RequestMethod.POST, path = "/rest/GetCheckService/GetCheckService")
    TradeInfo service(TradeInfo tradeInfo);
}
