package com.sinosoft.cloud.nb.app.client;

import com.sinosoft.cloud.rest.TradeInfo;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * @Author: zhuyiming
 * @Description:
 * @Date: Created in 14:37 2017/10/9
 * @Modified By
 */
@FeignClient(value = "nb-app-service")
public interface ContSignService {
    @RequestMapping(method = RequestMethod.POST, path = "/rest/ContSignService/ContSignService")
    TradeInfo service(TradeInfo tradeInfo);
}
