package com.sinosoft.cloud.nb.app.client;

import com.sinosoft.cloud.rest.TradeInfo;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * @project: abc-cloud-microservice
 * @author:  肖泽华
 * @date: Created in 下午 2:44 2018/3/9 0009
 * @Description: 非实时出单（新契约）微服务接口
 */
@FeignClient(value = "nb-app-service")
public interface NBNrtPolicyService {
    @RequestMapping(method = RequestMethod.POST, path = "/rest/NBNrtPolicyService/NBNrtPolicyService")
    TradeInfo service(TradeInfo tradeInfo);
}
