package com.sinosoft.cloud.nb.app.client;

import com.sinosoft.cloud.rest.TradeInfo;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by grs on 2017/8/10.
 * 数据交互转保核心更新轨迹客户端
 */
@FeignClient(value = "microservice-application")
public interface LKTransInfoUpdateService {
    @RequestMapping(method = RequestMethod.POST, path = "/rest/LKTransInfoUpdateService/LKTransInfoUpdateService")
    TradeInfo service(TradeInfo tradeInfo);
}
