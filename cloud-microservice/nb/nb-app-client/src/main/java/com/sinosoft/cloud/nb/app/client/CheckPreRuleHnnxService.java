package com.sinosoft.cloud.nb.app.client;

import com.sinosoft.cloud.rest.TradeInfo;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * @Author heshouyou
 * @Description 前置规则校验
 * @Date 2017-10-30 15:20.
 */
@FeignClient(value = "nb-app-service")
public interface CheckPreRuleHnnxService {

    @RequestMapping(method = RequestMethod.POST, path = "/rest/HnnxPojoFieldCheckService/HnnxPojoFieldCheckService")
    TradeInfo service(TradeInfo tradeInfo);
}
