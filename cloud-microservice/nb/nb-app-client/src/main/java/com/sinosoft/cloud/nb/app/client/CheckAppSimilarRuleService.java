package com.sinosoft.cloud.nb.app.client;

import com.sinosoft.cloud.rest.TradeInfo;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
/**
 * 校验相似客户.
* @Author: wupengqian
* @Description:
* @Date:   2018/9/4
*
*/
@FeignClient(value = "nb-app-service")
public interface CheckAppSimilarRuleService {
    @RequestMapping(method = RequestMethod.POST, path = "/rest/CheckAppSimilarRuleService/CheckAppSimilarRuleService")
    TradeInfo service(TradeInfo tradeInfo);
}
