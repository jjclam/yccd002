package com.sinosoft.cloud.nb.app.client;

import com.sinosoft.cloud.rest.TradeInfo;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by zhangfei on 2018/7/6.
 * 功能：回滚单证系统的单证状态为未用
 */
@FeignClient(value = "nb-app-service")
public interface DocumentsRetroversionDZService  {
    @RequestMapping(method = RequestMethod.POST, path = "/rest/DocumentsRetroversionDZService/DocumentsRetroversionDZService")
    TradeInfo service(TradeInfo tradeInfo);
}
