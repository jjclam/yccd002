package com.sinosoft.cloud.nb.app.client;

import com.sinosoft.cloud.rest.TradeInfo;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * 校验线上线下的微服务
 */
@FeignClient(value = "nb-app-service")
public interface PreparePoLicyMIPService {
    @RequestMapping(method = RequestMethod.POST, path = "/rest/PreparePoLicyMIPService/PreparePoLicyMIPService")
    TradeInfo service(TradeInfo tradeInfo);
}