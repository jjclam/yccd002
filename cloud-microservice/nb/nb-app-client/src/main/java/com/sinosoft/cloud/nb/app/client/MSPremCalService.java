package com.sinosoft.cloud.nb.app.client;


import com.sinosoft.cloud.rest.TradeInfo;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by grs on 2017/8/10.
 * 保费计算客户端
 */
@FeignClient(value = "nb-app-service")
public interface MSPremCalService {

    @RequestMapping(method = RequestMethod.POST, path = "/rest/MSPremCalServiceImpl/MSPremCalServiceImpl")
    TradeInfo service(TradeInfo tradeInfo);
}
