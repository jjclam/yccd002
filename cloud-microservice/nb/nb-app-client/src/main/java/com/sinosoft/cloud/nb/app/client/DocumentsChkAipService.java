package com.sinosoft.cloud.nb.app.client;

import com.sinosoft.cloud.rest.TradeInfo;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * 6807 校验单证服务 单证关联
 */
@FeignClient(value = "nb-app-service")
public interface DocumentsChkAipService {
    @RequestMapping(method = RequestMethod.POST, path = "/rest/DocumentsChkAipService/DocumentsChkAipService")
    TradeInfo service(TradeInfo tradeInfo);
}
