package com.sinosoft.cloud.nb.app.client;

import com.sinosoft.cloud.rest.TradeInfo;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * 移动展业复核数据客户端
 */
@FeignClient(value = "nb-app-service")
public interface ReviewService {
    @RequestMapping(method = RequestMethod.POST, path = "/rest/ReviewService/ReviewService")
    TradeInfo service(TradeInfo tradeInfo);
}