package com.sinosoft.cloud.nb.app.client;

import com.sinosoft.cloud.rest.TradeInfo;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by zhangfei on 2018/7/5.
 */
@FeignClient(name = "nb-app-service")
public interface DocumentsLockAipService {
    @RequestMapping(method = RequestMethod.POST, path = "/rest/DocumentsLockAipService/DocumentsLockAipService")
    TradeInfo service(TradeInfo tradeInfo);
}
