package com.sinosoft.cloud.nb.app.client;

import com.sinosoft.cloud.rest.TradeInfo;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * @Author: zhuyiming
 * @Description: 契约保单打印客户端
 * @Date: Created in 15:55 2017/10/11
 * @Modified By
 */
@FeignClient(value = "nb-app-service")
public interface NBPrintService {
    @RequestMapping(method = RequestMethod.POST, path = "/rest/NBPrintService/NBPrintService")
    TradeInfo service(TradeInfo tradeInfo);
}
