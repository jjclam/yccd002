package com.sinosoft.cloud.nb.app.client;

import com.sinosoft.cloud.rest.TradeInfo;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * @Author: zhuyiming
 * @Description: 契约落库客户端
 * @Date: Created in 11:26 2017/10/24
 * @Modified By
 */
@FeignClient(value = "nb-app-service")
public interface NBDataStoreService {
    @RequestMapping(method = RequestMethod.POST, path = "/rest/NBDataStoreService/NBDataStoreService")
    TradeInfo service(TradeInfo tradeInfo);
}
