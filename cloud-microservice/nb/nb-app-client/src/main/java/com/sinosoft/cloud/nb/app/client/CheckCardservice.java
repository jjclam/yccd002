package com.sinosoft.cloud.nb.app.client;

import com.sinosoft.cloud.rest.TradeInfo;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@FeignClient(value = "nb-app-service")
public interface CheckCardservice {

    @RequestMapping(method = RequestMethod.POST, path = "/rest/checkCardService/checkCardService")
    TradeInfo service(TradeInfo tradeInfo);
}