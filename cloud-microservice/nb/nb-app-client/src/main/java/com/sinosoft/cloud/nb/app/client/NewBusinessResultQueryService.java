package com.sinosoft.cloud.nb.app.client;


import com.sinosoft.cloud.rest.TradeInfo;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by grs on 2018/2/6.
 * 保全申请客户端
 */
@FeignClient(value = "nb-app-service")
public interface NewBusinessResultQueryService {

    @RequestMapping(method = RequestMethod.POST, path = "/rest/NewBusinessResultQueryService/NewBusinessResultQueryService")
    TradeInfo service(TradeInfo tradeInfo);
}
