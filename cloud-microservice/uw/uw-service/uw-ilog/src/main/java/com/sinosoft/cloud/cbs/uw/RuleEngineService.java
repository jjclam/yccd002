package com.sinosoft.cloud.cbs.uw;

import com.sinosoft.cloud.microservice.AbstractMicroService;
import com.sinosoft.cloud.rest.TradeInfo;
import org.springframework.stereotype.Service;

/**
 * @project: abc-cloud-microservice
 * @author: yangming
 * @date: 2017/9/17 下午10:17
 * To change this template use File | Settings | File and Code Templates.
 */
@Service
public class RuleEngineService extends AbstractMicroService {
    @Override
    public boolean checkData(TradeInfo requestInfo) {
        return false;
    }

    @Override
    public TradeInfo dealData(TradeInfo requestInfo) {
        return null;
    }
}
