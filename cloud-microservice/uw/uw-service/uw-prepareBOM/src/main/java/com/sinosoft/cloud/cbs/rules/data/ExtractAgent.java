package com.sinosoft.cloud.cbs.rules.data;

import com.sinosoft.cache.dao.impl.RedisCommonDao;
import com.sinosoft.cloud.cbs.function.UWFunction;
import com.sinosoft.cloud.cbs.rules.bom.Agent;
import com.sinosoft.cloud.cbs.rules.bom.Applicant;
import com.sinosoft.cloud.cbs.rules.bom.Policy;
import com.sinosoft.cloud.nb.service.redis.NBRedisCommon;
import com.sinosoft.cloud.rest.TradeInfo;
import com.sinosoft.lis.entity.LAAgentPojo;
import com.sinosoft.lis.entity.LAAgentStatePojo;
import com.sinosoft.lis.entity.LCContPojo;
import com.sinosoft.lis.entity.LCCustomerImpartParamsPojo;
import com.sinosoft.utility.SSRS;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author: zhuyiming
 * @Description: 提取代理人数据
 * @Date: Created in 16:12 2017/9/24
 */
@Service
public class ExtractAgent {
	private final Log logger = LogFactory.getLog(this.getClass());
	@Autowired
	RedisCommonDao redisCommonDao;
	@Autowired
	NBRedisCommon nbRedisCommon;
	@Autowired
	UWFunction uwFunction;

	/**
	 * 获取代理人
	 * @param policy
	 * @return
	 */
	public void getAgent(Policy policy, TradeInfo requestInfo) {
		// 代理人信息
		Agent agent = policy.getAgent();
		// 投保人信息
		Applicant applicantInfo = policy.getApplicant();
		String agentCode = agent.getAgentCode();
		// 代理人基本信息
		long beginTime = System.currentTimeMillis();
		LAAgentPojo tLAAgentPojo = redisCommonDao.getEntityRelaDB(LAAgentPojo.class, agentCode);
		List<LAAgentStatePojo> laAgentStatePojos = redisCommonDao.findByIndexKeyRelaDB(LAAgentStatePojo.class, "agentcode", agentCode);
		logger.debug("代理人基本信息提数：" + (System.currentTimeMillis() - beginTime) / 1000.0
				+ "s");
		if (null != tLAAgentPojo&&laAgentStatePojos!=null&&laAgentStatePojos.size()>0) {
			// 代理人代码
			agent.setAgentCode(agentCode);
			// 姓名
			agent.setAgentName(tLAAgentPojo.getName());
			// 联系电话
			agent.setPhones(tLAAgentPojo.getMobile());
			// 代理人是否为投保人
			if (applicantInfo.getIdentityCode() != null
					&& tLAAgentPojo.getIDNo() != null) {
				agent.setApplicant(applicantInfo.getIdentityCode().equals(
						tLAAgentPojo.getIDNo()) ? true : false);
			}
			// 营业部、营业组
			policy.setSalesDept(tLAAgentPojo.getAgentGroup());
			if(null != laAgentStatePojos || laAgentStatePojos.size()>0){
				for (int i = 0; i < laAgentStatePojos.size(); i++) {
					// 星级代理人标识
					agent.setStarAgentSign(("Y".equals(laAgentStatePojos.get(i).getStateValue()) && "25"
							.equals(laAgentStatePojos.get(i).getStateType())) ? true : false);
					if (agent.isStarAgentSign()) {
						break;
					}
				}
			}
		}
		beginTime = System.currentTimeMillis();
		List<LCCustomerImpartParamsPojo> tLCCIPList = (List<LCCustomerImpartParamsPojo>) requestInfo.getData(LCCustomerImpartParamsPojo.class.getName()) ;
		// 非自保件主动投保标识
		boolean flag = false;
		List<String> custList = new ArrayList<String>();
		for(int i=0;i<tLCCIPList.size();i++){
			if(!(19044==tLCCIPList.get(i).getPatchNo())){
				continue;
			}
			if(!"2".equals(tLCCIPList.get(i).getCustomerNoType())){
				continue;
			}
			if("DLR5".equals(tLCCIPList.get(i).getImpartVer())
					&& "2-1".equals(tLCCIPList.get(i).getImpartCode())
					&& "Number".equals(tLCCIPList.get(i).getImpartParamName())
					&& "1".equals(tLCCIPList.get(i).getImpartParamNo())
					&& "2".equals(tLCCIPList.get(i).getImpartParam())){
				custList.add(tLCCIPList.get(i).getCustomerNo());
				continue;
			}
			if(custList != null && custList.size()>0){
				for(int j=0;j<custList.size();j++){
					if("DLR1".equals(tLCCIPList.get(i).getImpartVer())
							&& "YesOrNo".equals(tLCCIPList.get(i).getImpartParamName())
							&& "2".equals(tLCCIPList.get(i).getImpartParam())
							&& custList.get(j)!=null
							&& custList.get(j).equals(tLCCIPList.get(i).getCustomerNo())){
						flag = true;
						break;
					}
					if("DLR1".equals(tLCCIPList.get(i).getImpartVer())
							&& "1-1".equals(tLCCIPList.get(i).getImpartCode())
							&& "2".equals(tLCCIPList.get(i).getImpartParamNo())
							&& "Number".equals(tLCCIPList.get(i).getImpartParamName())
							&& "5".equals(tLCCIPList.get(i).getImpartParam())
							&& custList.get(j)!=null
							&& custList.get(j).equals(tLCCIPList.get(i).getCustomerNo())){
						flag = true;
						break;
					}
				}
			}
			if(flag){
				break;
			}
		}
		agent.setAutoPolicySign(flag);
		// 业务人员黑名单标识SQL（contno：合同号）
		LCContPojo lcContPojo = (LCContPojo) requestInfo.getData(LCContPojo.class.getName());
		if("1".equals(lcContPojo.getContType())){
			String count = uwFunction.checkAgentBlackSign(agentCode);
			if("1".equals(count)){
				agent.setBlackSign(true);
			}else {
				agent.setBlackSign(false);
			}
		}else {
			agent.setBlackSign(false);
		}

		//得到业务员的机构
		String manageCom = null;
		if("".equals(tLAAgentPojo.getManageCom()) || tLAAgentPojo.getManageCom() == null){
			manageCom = "empty";
		}else{
			manageCom = tLAAgentPojo.getManageCom();
		}
		agent.setAgentManageCom(manageCom);
        //==wupengqian===代理人监控状态====//
		logger.debug("开始进行代理人监控状态！");
		String result = nbRedisCommon.getAgentMonitorState(agentCode);
		logger.debug(result);
		if("1".equals(result)){
			agent.setAgentMonitorState(true);
		}else{
			agent.setAgentMonitorState(false);
		}
		//--wpq--
		SSRS tSSRS= nbRedisCommon.getStateValueAndT5(agentCode);
		logger.debug("jsx代理人评级结果"+ tSSRS.GetText(1,1));
		String stateValue = null;
		if("".equals(tSSRS.GetText(1, 1)) || tSSRS.GetText(1, 1)==null){
			stateValue = "empty";
		}else{
			stateValue = tSSRS.GetText(1, 1);
		}
		agent.setStateValue(stateValue);
		logger.debug("jsx代理人品质违规分值"+ tSSRS.GetText(1,2));
		double t5 ;
		if("".equals(tSSRS.GetText(1,2)) || tSSRS.GetText(1,2)==null){
			t5 = 0;
		}else{
			t5 = Util.toDouble(result);
		}
		agent.setT5(t5);
		//代理人星级代理人等级
		String grade=nbRedisCommon.getStarAgentGrade(agentCode);
		if (!"".equals(grade) && grade !=null){
			agent.setStarAgentGrade(grade);
		}
		//calStarAgentGrade(agent);
		logger.debug("代理人拓展提数：" + (System.currentTimeMillis() - beginTime) / 1000.0 + "s");
	}

//	/**
//	 *  获取评级结果及品质违规分值 wpq
//	 * @param agent
//	 * @return
//	 */
//	public static Agent calStarAgentGrade(Agent agent){
//		//评级结果
//		String stateValue = agent.getStateValue();
//		//品质违规分值
//		double t5 = agent.getT5();
//
//		//不为黑名单代理人
//		if (agent.isBlackSign() == false) {
//			//评级结果不为空
//			if (stateValue != null ) {
//				if(stateValue.equals("AAA") && t5<=1){
//					agent.setStarAgentGrade("五星");
//				}
//				if (stateValue.equals("AAA" ) && t5>1 && t5<=10) {
//					agent.setStarAgentGrade("四星");
//				}
//				if (stateValue.equals("AAA" ) && t5>10) {
//					agent.setStarAgentGrade("三星");
//				}
//				if (stateValue.equals("AA") && t5<=5) {
//					agent.setStarAgentGrade("四星");
//				}
//				if (stateValue.equals("AA") && t5>5) {
//					agent.setStarAgentGrade("三星");
//				}
//				if (stateValue.equals("A") || stateValue.equals("B") || stateValue.equals("新入司")) {
//					agent.setStarAgentGrade("三星");
//				}
//			} else {
//				//评级结果为空
//				agent.setStarAgentGrade("三星");
//			}
//		}
//		return agent;
//	}
}
