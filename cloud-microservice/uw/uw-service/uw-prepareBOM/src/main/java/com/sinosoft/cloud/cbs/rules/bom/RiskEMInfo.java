package com.sinosoft.cloud.cbs.rules.bom;


/**
 * 险种EM值信息
 * 
 * @author dingfan
 * 
 */
public class RiskEMInfo{
	/**
	 * 险种代码
	 */
	private String riskCode;

	/**
	 * EM值
	 */
	private double EMValue;

	public RiskEMInfo() {
		super();
	}

	public String getRiskCode() {
		return riskCode;
	}

	public void setRiskCode(String riskCode) {
		this.riskCode = riskCode;
	}

	public double getEMValue() {
		return EMValue;
	}

	public void setEMValue(double eMValue) {
		EMValue = eMValue;
	}
}
