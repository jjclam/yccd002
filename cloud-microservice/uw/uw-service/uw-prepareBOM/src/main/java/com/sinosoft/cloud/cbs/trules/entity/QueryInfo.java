package com.sinosoft.cloud.cbs.trules.entity;

import java.util.List;


/**
 * @Author:wangshuliang
 * @Description:
 * @Date:Created in 18:10 2018/6/25
 * @Modified by:
 */
public class QueryInfo {
    private String approveTypeCode;

    private List<ApproveExtendInfo> approveExtendInfos;

    public String getApproveTypeCode() {
        return approveTypeCode;
    }

    public void setApproveTypeCode(String approveTypeCode) {
        this.approveTypeCode = approveTypeCode;
    }

    public List<ApproveExtendInfo> getApproveExtendInfos() {
        return approveExtendInfos;
    }

    public void setApproveExtendInfos(List<ApproveExtendInfo> approveExtendInfos) {
        this.approveExtendInfos = approveExtendInfos;
    }

    @Override
    public String toString() {
        return "QueryInfo{" +
                "approveTypeCode='" + approveTypeCode + '\'' +
                ", approveExtendInfos=" + approveExtendInfos +
                '}';
    }
}
