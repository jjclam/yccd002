package com.sinosoft.cloud.cbs.trules.bom;



import java.util.List;

public class TAgentInfo {
	
	//代理人编码
	private String agentCode;
	
	//代理人证件信息
	private List<AgentCert> AgentCerts;

	public String getAgentCode() {
		return agentCode;
	}

	public void setAgentCode(String agentCode) {
		this.agentCode = agentCode;
	}

	public List<AgentCert> getAgentCerts() {
		return AgentCerts;
	}

	public void setAgentCerts(List<AgentCert> agentCerts) {
		AgentCerts = agentCerts;
	}
}
