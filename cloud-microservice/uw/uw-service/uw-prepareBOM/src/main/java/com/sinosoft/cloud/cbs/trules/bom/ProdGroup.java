package com.sinosoft.cloud.cbs.trules.bom;



/**
 * 产品组合信息
 *
 */
public class ProdGroup {
	
	/**
	 * 产品组合编码
	 */
	private String prodGroupCode;
	
	/**
	 * 产品组合版本
	 */
	private String prodGroupVersion;
	
	/**
	 * 产品组合名称
	 */
	private String prodGroupName;
	
	/**
	 * 保费
	 */
	private double prem;
	
	
	/**
	 * 份数
	 */
	private int copies;
	
	/**
	 * 保险金额
	 */
	private double suminsur;
	
	/**
	 * 保险期间
	 */
	private int insurperiod;
	
	/**
	 * 乘机日期
	 */
	private String boardDate;
	
	/**
	 * 风险加费
	 */
	private double riskAddprem;
	
	/**
	 * 套餐有效期
	 */
	private String packageValidity;
	
	
	

	

	public int getCopies() {
		return copies;
	}

	public void setCopies(int copies) {
		this.copies = copies;
	}

	public double getSuminsur() {
		return suminsur;
	}

	public void setSuminsur(double suminsur) {
		this.suminsur = suminsur;
	}

	public int getInsurperiod() {
		return insurperiod;
	}

	public void setInsurperiod(int insurperiod) {
		this.insurperiod = insurperiod;
	}

	

	public String getBoardDate() {
		return boardDate;
	}

	public void setBoardDate(String boardDate) {
		this.boardDate = boardDate;
	}

	public double getRiskAddprem() {
		return riskAddprem;
	}

	public void setRiskAddprem(double riskAddprem) {
		this.riskAddprem = riskAddprem;
	}

	

	public String getPackageValidity() {
		return packageValidity;
	}

	public void setPackageValidity(String packageValidity) {
		this.packageValidity = packageValidity;
	}

	public String getProdGroupCode() {
		return prodGroupCode;
	}

	public void setProdGroupCode(String prodGroupCode) {
		this.prodGroupCode = prodGroupCode;
	}

	public String getProdGroupVersion() {
		return prodGroupVersion;
	}

	public void setProdGroupVersion(String prodGroupVersion) {
		this.prodGroupVersion = prodGroupVersion;
	}

	public String getProdGroupName() {
		return prodGroupName;
	}

	public void setProdGroupName(String prodGroupName) {
		this.prodGroupName = prodGroupName;
	}

	public double getPrem() {
		return prem;
	}

	public void setPrem(double prem) {
		this.prem = prem;
	}

	

}
