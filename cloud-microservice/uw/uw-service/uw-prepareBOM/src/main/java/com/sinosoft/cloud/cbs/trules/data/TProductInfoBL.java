package com.sinosoft.cloud.cbs.trules.data;

import com.sinosoft.cache.dao.impl.RedisCommonDao;
import com.sinosoft.cloud.cbs.trules.bom.TProductInfo;
import com.sinosoft.cloud.cbs.trules.util.SellTypeEnum;
import com.sinosoft.cloud.rest.TradeInfo;
import com.sinosoft.lis.entity.*;
import com.sinosoft.lis.pubfun.FDate;

import com.sinosoft.lis.pubfun.PubFun;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


import java.util.List;

/**
 * @Author:wangshuliang
 * @Description: 提取合同信息
 * @Date:Created in 17:38 2018/6/7
 * @Modified by:
 */
@Component
public class TProductInfoBL {
    private Log logger = LogFactory.getLog(getClass());
    @Autowired
    RedisCommonDao redisCommonDao;

    public TradeInfo getTProductInfo(TradeInfo tradeInfo) {
        logger.debug("开始提数提取合同信息");
        FDate fDate = new FDate();
        TProductInfo tProductInfo = new TProductInfo();
        //核保流向标记 01-投保
        //02-核保
        //03-投核保
        String riskCode = "";
        List<LCPolPojo>  lcPolPojos = (List<LCPolPojo>) tradeInfo.getData(LCPolPojo.class.getName());
         if (lcPolPojos!=null && lcPolPojos.size()>0){
             for (int i = 0; i < lcPolPojos.size(); i++) {
                 if (lcPolPojos.get(i).getPolNo().equals(lcPolPojos.get(i).getMainPolNo())){
                     riskCode = lcPolPojos.get(i).getRiskCode();
                     break;
                 }
             }
        }


        //套餐编码
        List<LCInsuredPojo> lcInsuredPojos = (List<LCInsuredPojo>) tradeInfo.getData(LCInsuredPojo.class.getName());
        String contPlanCode = lcInsuredPojos.get(0).getContPlanCode();
        tProductInfo.setProductNo(contPlanCode);
        LCContPojo lcContPojo = (LCContPojo) tradeInfo.getData(LCContPojo.class.getName());
        //投保单号
        tProductInfo.setAppformNo(lcContPojo.getContNo());
        //投保日期
        tProductInfo.setApplyApplyDate(lcContPojo.getPolApplyDate());
        //保险起期
        tProductInfo.setInsurStartDate(lcContPojo.getCValiDate());
        tProductInfo.setFlowFlag("02");
        //保险止期
        if ("6807".equals(riskCode)){
            tProductInfo.setInsurEndDate(PubFun.calDate(lcPolPojos.get(0).getEndDate(),-1,"D",null));
        }else{
            tProductInfo.setInsurEndDate(lcPolPojos.get(0).getEndDate());
        }
        //单证印刷号
        tProductInfo.setDocPrtNo(lcContPojo.getBillPressNo());
        //经办人
        tProductInfo.setInsertOper(lcContPojo.getHandler());
        LCPolicyInfoPojo lcPolicyInfoPojo = (LCPolicyInfoPojo) tradeInfo.getData(LCPolicyInfoPojo.class.getName());
        if (lcPolicyInfoPojo != null) {
            //经办人电话
            tProductInfo.setHandlerTel(lcPolicyInfoPojo.getHandlerPhone());
            //借款合同编号
            tProductInfo.setLoancontractnumber(lcPolicyInfoPojo.getJYContNo());
            //借款凭证编号
            tProductInfo.setLoandocumentnumber(lcPolicyInfoPojo.getJYCertNo());
            //借费发放日期
            tProductInfo.setBorrowfeeissuedate(lcPolicyInfoPojo.getJYStartDate());
            //借费结束日期
            tProductInfo.setBorrowtheenddate(lcPolicyInfoPojo.getJYEndDate());
            //借款期限
            tProductInfo.setLoandata(lcPolicyInfoPojo.getLendTerm());
            //借款金额
            if (lcPolicyInfoPojo.getLoanAmount() == null || "".equals(lcPolicyInfoPojo.getLoanAmount())) {
                tProductInfo.setLoanmoney(0);
            } else {
                tProductInfo.setLoanmoney(Double.valueOf(lcPolicyInfoPojo.getLoanAmount()));
            }
            //贷款发放机构
            tProductInfo.setLoangrantinginstitution(lcPolicyInfoPojo.getLendCom());
            //借款人姓名
            tProductInfo.setBorrowername(lcPolicyInfoPojo.getLoanerNatureName());
            //借款人性质(01 个人 02公司)
            tProductInfo.setBorrowerproperty(lcPolicyInfoPojo.getLoanerNature());
            if("1".equals(lcPolicyInfoPojo.getLoanerNature())){
                tProductInfo.setBorrowerproperty("01");
            }
            if ("2".equals(lcPolicyInfoPojo.getLoanerNature())){
                tProductInfo.setBorrowerproperty("02");
            }
            //燃气公司
            tProductInfo.setGasCompany(lcPolicyInfoPojo.getGasCompany());
            //燃气用户地址
            tProductInfo.setGasUserAddr(lcPolicyInfoPojo.getGasUserAddress());
            //投保确认书签字时间
            tProductInfo.setAffirmDate(PubFun.getCurrentDate());
            //保费收费确认时间
            tProductInfo.setChargeDate(lcPolicyInfoPojo.getChargeDate());
            //学校名称
            tProductInfo.setSchoolName(lcPolicyInfoPojo.getSchool());
            //家庭住址
            //tProductInfo.setAddr("");
            //救援卡卡号
            tProductInfo.setRescuecardno(lcPolicyInfoPojo.getRescuecardNo());
            //电子保单
            //tProductInfo.setElectron("");
            //纸质保单
            //tProductInfo.setPapery("");
        }
        //出访国家(MAp) 不需要
        //tProductInfo.setCountrys(lcPolicyInfoPojo.getTraveLcountry());
        //管理机构
        tProductInfo.setMngorgCode(lcContPojo.getManageCom().substring(0,4));
        //中介机构类别
       // LAComPojo laComPojo = redisCommonDao.getEntityRelaDB(LAComPojo.class, lcContPojo.getAgentCom());
        /*if (laComPojo != null) {*/
        //和老核心对比写死
        //tProductInfo.setAgencyType("07");
        //新销管的“中介机构类别”变更为“销售渠道”
            tProductInfo.setAgencyType("203");

            //新销管的"中介机构子渠道"取值为销管的"销售渠道子类"
            String sellType = lcContPojo.getSellType();
            // sellType.toUpperCase();
//        if ("ABCOTC".equals(sellType)){
//            tProductInfo.setAgencyChnnlType("20301");
//        }else if ("ABCEBANK".equals(sellType)){
//            tProductInfo.setAgencyChnnlType("20301");
//        }else if ("ABCSELF".equals(sellType)){
//            tProductInfo.setAgencyChnnlType("20301");
//        }else if ("ABCMOBILE".equals(sellType)){
//            tProductInfo.setAgencyChnnlType("20301");
//        }else if ("ABCESTWD".equals(sellType)){
//            tProductInfo.setAgencyChnnlType("20301");
//        }else if ("ABCSUPOTC".equals(sellType)){
//            tProductInfo.setAgencyChnnlType("20301");
//        }else if ("HNNX00001".equals(sellType)){
//            tProductInfo.setAgencyChnnlType("20302");
//        }
            if ("08".equals(sellType)) {
                tProductInfo.setAgencyChnnlType("20301");
            } else if ("22".equals(sellType)) {
                tProductInfo.setAgencyChnnlType("20301");
            } else if ("24".equals(sellType)) {
                tProductInfo.setAgencyChnnlType("20301");
            } else if ("25".equals(sellType)) {
                tProductInfo.setAgencyChnnlType("20301");
            } else if ("26".equals(sellType)) {
                tProductInfo.setAgencyChnnlType("20301");
            } else if ("27".equals(sellType)) {
                tProductInfo.setAgencyChnnlType("20301");
            } else if ("99".equals(sellType)) {
                tProductInfo.setAgencyChnnlType("20302");
            }else if ("21".equals(sellType)){
                tProductInfo.setAgencyChnnlType("20601");
            }else if ("20".equals(sellType)){
                tProductInfo.setAgencyChnnlType("20603");
            }
       /* }*/
        //银行编码
        tProductInfo.setBankCode(lcContPojo.getBankCode());
        if("99".equals(lcContPojo.getSellType())){
            //湖南农信 为农信社   15 农信社
           tProductInfo.setBankCode("15");
        }
        //企业借款人性质  01 - 持股30%及以上的大股东或法人  02 - 高级管理人员
        tProductInfo.setBorrowerType("");
        //农行标志  农业银行为03，其他银行为空，投保规则，不需要
        tProductInfo.setNonghangSign(lcContPojo.getBankCode());
        //渠道编码
        if("8888".equals(SellTypeEnum.getSellType(lcContPojo.getSellType()))){
            logger.debug(lcContPojo.getSellType()+"渠道编码未配置转换请检查"+tradeInfo.toString());
            tradeInfo.addError(lcContPojo.getSellType()+"渠道编码未配置转换请检查");
            return tradeInfo;
        }
        tProductInfo.setChnnlCode(SellTypeEnum.getSellType(lcContPojo.getSellType()));
        tProductInfo.setProductType("11");
        //套餐类型   6807借意险08
       for (int i = 0; i < lcPolPojos.size(); i++) {
            if ("6807".equals(lcPolPojos.get(i).getRiskCode())) {
                tProductInfo.setProductType("08");
                break;
            }
        }
        if ("SEE".equals(contPlanCode) || "SEN".equals(contPlanCode)) {
            tProductInfo.setProductType("12");
        }
        if("SEP1".equals(contPlanCode) || "SEP3".equals(contPlanCode) || "SEP6".equals(contPlanCode)){
            //tProductInfo.setSepNum(lcPolPojos.get(0).getMult());
            tProductInfo.setSepNum(1);
        }

        if("JSJXBC".equals(contPlanCode)){
            tProductInfo.setTheJSJXBCNum(lcPolPojos.get(0).getMult());
        }
        if ("SESXNH".equals(contPlanCode)){
            tProductInfo.setTheSESXNHNum(lcPolPojos.get(0).getMult());
        }

        tradeInfo.addData(TProductInfo.class.getName(), tProductInfo);
        logger.debug("完成提数提取合同信息");
        return tradeInfo;
    }

}
