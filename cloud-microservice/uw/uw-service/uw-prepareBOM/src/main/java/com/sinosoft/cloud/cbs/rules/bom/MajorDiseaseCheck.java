package com.sinosoft.cloud.cbs.rules.bom;

/**
 * Created by sll on 2019/3/28.
 * 中保信个人健康险核保风险返回信息（重大疾病）
 */
public class MajorDiseaseCheck {
    /**
     * 非正常核保结论：标记被保险人既往保单核保结论是否存在非“标准体"和非“优标体"的情况，判断相应保单的核保结论信息，如果核保结论不为“标准体"或“优标体"，则提示：Y，否则为：N.A.
     */
    private String abnormalCheck;
    /**
     * 非正常理赔结论：标记被保险人既往理赔史中是否存在非“正常给付"的情况，判断相应赔案的理赔结论信息，如果核赔结论不为“正常给付"，则提示：Y，否则为：N.A.
     */
    private String abnormalPayment;
    /**
     * CI：为重疾理赔史标签，标记被保险人在各家保险公司索赔重大疾病的历史，存在重疾理赔史则为：Y，不存在则为：N.A.如标签标记为Y，则永远为Y
     */
    private String majorDiseasePayment;
    /**
     * NCDS：为慢性病理赔史标签，标记被保险人既往在各家保险公司索赔慢性病的历史，理赔结论中有三种慢性病之一
     *（高血压、糖尿病、冠心病）则提示：Y,否则为：N.A.，如标签标记为Y，则永远为Y
     */
    private String chronicDiseasePayment;
    /**
     * 重疾保额提示：标记被保险人有效重疾险保单基本保额的累计情况，被保险人当前有效重疾险保单基本保额大于等于100万元，则显示：Y；否则显示：N.A.
     */
    private String majorDiseaseMoney;
    /**
     *多家公司承保提示：标记被保险人是否存在多家保险公司承保重疾险，当前有效保单的投保公司家数大于等于4家，则显示：Y，否则显示：N.A.
     */
    private String multiCompany;
    /**
     * 是否密集投保：被保险人有效重疾险保单的承保机构大于等于3家，且有3家或 3 家以上公司的保单生效时间集中在1个月内，存在则显示Y，不存在显示：N.A.
     */
    private String dense;
    /**
     * 网页查询码（25位）：20170414 + 000019 + 5690432 + 01+ 03
     *日期（年月日）+ 保险公司机构编码 + 0~9随机生成唯一的7位数的顺序码 + 数据产品大类编码 + 数据产品细类编码
     */
    private String pageQueryCode;
    /**
     * 险类
     */
    private String riskCode;
    /**
     * 是否有网页数据：根据三要素计算出来的被保险人接口标签风险提示信息，同时根据网页展示条件规则，判断该被保险人是否需
     *要展示网页信息，展示网页显示：Y，不展示网页信息展示：N.A.
     */
    private String displayPage;

    public String getAbnormalCheck() {return abnormalCheck;}

    public void setAbnormalCheck(String abnormalCheck) {this.abnormalCheck = abnormalCheck;}

    public String getAbnormalPayment() { return abnormalPayment;}

    public void setAbnormalPayment(String abnormalPayment) {this.abnormalPayment = abnormalPayment;}

    public String getMajorDiseasePayment() {return majorDiseasePayment;}

    public void setMajorDiseasePayment(String majorDiseasePayment) {this.majorDiseasePayment = majorDiseasePayment;}

    public String getChronicDiseasePayment() { return chronicDiseasePayment;}

    public void setChronicDiseasePayment(String chronicDiseasePayment) { this.chronicDiseasePayment = chronicDiseasePayment;}

    public String getMajorDiseaseMoney() { return majorDiseaseMoney;}

    public void setMajorDiseaseMoney(String majorDiseaseMoney) {this.majorDiseaseMoney = majorDiseaseMoney;}

    public String getMultiCompany() {return multiCompany;}

    public void setMultiCompany(String multiCompany) {this.multiCompany = multiCompany;}

    public String getDense() { return dense;}

    public void setDense(String dense) {this.dense = dense;}

    public String getPageQueryCode() { return pageQueryCode;}

    public void setPageQueryCode(String pageQueryCode) {this.pageQueryCode = pageQueryCode;}

    public String getRiskCode() { return riskCode;}

    public void setRiskCode(String riskCode) {this.riskCode = riskCode;}

    public String getDisplayPage() { return displayPage;}

    public void setDisplayPage(String displayPage) { this.displayPage = displayPage;}

    @Override
    public String toString() {
        return "MajorDiseaseCheck{" +
                "abnormalCheck='" + abnormalCheck + '\'' +
                ", abnormalPayment='" + abnormalPayment + '\'' +
                ", majorDiseasePayment='" + majorDiseasePayment + '\'' +
                ", chronicDiseasePayment='" + chronicDiseasePayment + '\'' +
                ", majorDiseaseMoney='" + majorDiseaseMoney + '\'' +
                ", multiCompany='" + multiCompany + '\'' +
                ", dense='" + dense + '\'' +
                ", pageQueryCode='" + pageQueryCode + '\'' +
                ", riskCode='" + riskCode + '\'' +
                ", displayPage='" + displayPage + '\'' +
                '}';
    }
}
