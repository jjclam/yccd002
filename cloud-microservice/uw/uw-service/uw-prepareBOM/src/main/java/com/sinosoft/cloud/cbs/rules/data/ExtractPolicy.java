package com.sinosoft.cloud.cbs.rules.data;

import com.sinosoft.cloud.cbs.rules.bom.Agent;
import com.sinosoft.cloud.cbs.rules.bom.Applicant;
import com.sinosoft.cloud.cbs.rules.bom.Policy;
import com.sinosoft.cloud.cbs.rules.util.ConstStatic;
import com.sinosoft.cloud.nb.service.redis.NBRedisCommon;
import com.sinosoft.cloud.rest.TradeInfo;
import com.sinosoft.lis.entity.*;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.*;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

/**
 * @Author: zhuyiming
 * @Description: 提取保单数据
 * @Date: Created in 17:26 2017/9/23
 */
@Service
public class ExtractPolicy {
	private final Log logger = LogFactory.getLog(this.getClass());

	@Autowired
	private NBRedisCommon mNBRedisCommon;

	/**
	 * 获取保单
	 * @param policy
	 * @throws Exception`
	 */
	public void getPolicy(Policy policy, TradeInfo requestInfo) throws Exception {
		LCContPojo tLCContPojo = (LCContPojo) requestInfo.getData(LCContPojo.class.getName());
		List<LCPolPojo> tLCPolPojoList = (List) requestInfo.getData(LCPolPojo.class.getName());
		String contno = tLCContPojo.getContNo();

		FDate fDate = new FDate();
		// 保单基本数据SQL
		if(null != tLCContPojo) {
			// 投保单号
			policy.setPolicyNo(contno);
			// 投保提示签字日期
			policy.setSignDate(fDate.getDate(tLCContPojo.getReceiveDate()));
			// 生效日期
			policy.setEffectiveDate(fDate.getDate(tLCPolPojoList.get(0).getCValiDate()));
			// 投保日期
			policy.setPolicyDate(fDate.getDate(tLCContPojo.getPolApplyDate()));
			// 保单申请日期
			policy.setPolApplyDate(fDate.getDate(tLCContPojo.getPolApplyDate()));
			// 销售方式
			policy.setSellType(tLCContPojo.getSellType());
			// 管理机构
			policy.setManageOrg(tLCContPojo.getManageCom());
			// 所属机构
			policy.setOwnerOrg(tLCContPojo.getManageCom());
			// 销售渠道
			policy.setSellChannel(tLCContPojo.getSaleChnl());
			// 受理人员姓名
			policy.setAccepterName(tLCContPojo.getHandler());
			// 柜员代码
			policy.setTellerCode(tLCContPojo.getBankAgent());
			// 柜员姓名
			policy.setTellerName(tLCContPojo.getBankAgentName());
			// 银行编码
			policy.setBankCode(tLCContPojo.getBankCode());

			Applicant applicantInfo = policy.getApplicant();
			// 首期支付方式
			applicantInfo.setFirstPayMethod(tLCContPojo.getNewPayMode());
			// 续期交费提示
			applicantInfo.setAfterPayPrompt(tLCContPojo.getRenewPayFlag());
			// 首期转账开户行
			applicantInfo.setFirstPayBank(tLCContPojo.getNewBankCode());
			// 首期账户姓名
			applicantInfo.setFirstPayName(tLCContPojo.getNewAccName());
			// 首期账号
			applicantInfo.setFirstPayBankNo(tLCContPojo.getNewBankAccNo());
			// 续期保险费支付方式
			applicantInfo.setAfterPayMethod(tLCContPojo.getPayLocation());
			// 续期转账开户行
			applicantInfo.setAfterPayBank(tLCContPojo.getBankCode());
			// 续期账户姓名
			applicantInfo.setAfterPayName(tLCContPojo.getAccName());
			// 续期账号
			applicantInfo.setAfterPayBankNo(tLCContPojo.getBankAccNo());

            // 出单方式
            policy.setSalesType(tLCContPojo.getTBType());
            // 电子签名
            policy.setElectronSign(tLCContPojo.getEAuto());
            // 保单形式
            policy.setPolicyType(tLCContPojo.getSlipForm());
            // 合同号
            policy.setContNo(tLCContPojo.getContNo());

            Agent agent = policy.getAgent();
            // 代理人代码
            agent.setAgentCode(tLCContPojo.getAgentCode());

            // 保单标识
            policy.setAppFlag(tLCContPojo.getAppFlag());
            // 核保状态
            policy.setUwFlag(tLCContPojo.getUWFlag());
            // 强制人工核保标识
            policy.setForceuwFlag("1".equals(tLCContPojo.getForceUWFlag()) ? true
                    : false);
            // 印刷号码
            policy.setPrtno(tLCContPojo.getPrtNo());
            //有核保师录入的核保函
            policy.setUnderWriterLetter(false);
            //操作员问题件
            policy.setOpIssueLetter(false);
            //保单存在核保师录入的核保函（未下发的状态）
            policy.setUnderWriterLetterNoSend(false);
            //有核保师录入的核保函且均没有选择返回录入或复核
            policy.setChekerLetterNotEnte(false);
            //有核保师录入的核保函且选择了返回录入，且选择人核
            policy.setChekerLetterBeEnteChek(false);
        }
        //有人核记录
        boolean flag = false;
        /**wpq 注销--没有人工核保**/
//        StringBuffer tSql = new StringBuffer();
//        tSql.append("select to_char(COUNT(1)) as field1,10 as flag  from LBCUWSub l where l.contno =? and l.autouwflag ='2'");
//        TransferData tparam = new TransferData();
//        tparam.setNameAndValue("0", "string:" + contno);
//        VData tBVData = new VData();
//        tBVData.add(tSql.toString());
//        tBVData.add(tparam);
//        Connection tConnection = null;
//        SSRS tSSRS=null;
//        try {
//            tConnection = DBConnPool.getConnection("basedataSource");
//            ExeSQL pExeSQL = new ExeSQL(tConnection);
//            tSSRS= pExeSQL.execSQL(tBVData);
//        } catch (Exception e) {
//            e.printStackTrace();
//            logger.error(ExceptionUtils.exceptionToString(e));
//           // tradeInfo.addError(ExceptionUtils.exceptionToString(e));
//            return;
//        } finally {
//            if (tConnection != null) {
//                try {
//                    tConnection.close();
//                } catch (SQLException e) {
//                    e.printStackTrace();
//                    //tradeInfo.addError(ExceptionUtils.exceptionToString(e));
//                    logger.error(ExceptionUtils.exceptionToString(e));
//                    return;
//                }
//            }
//        }

//        if (tSSRS == null) {
//            logger.info("保单号：" + contno + "，保单是否有人工核保记录访问老核心SQL发生异常！");
//            requestInfo.addError("保单号：" + contno + "，保单是否有人工核保记录访问老核心SQL发生异常！");
//            return;
//        }
//        if (!"0".equals(tSSRS.GetText(1, 1))) {
//            System.out.println("有人工核保记录");
//            flag = true;
//        }
        policy.setManCheckFlag(flag);
        // 单证细类SQL（doccode：单证号码，subType：单证细类）
//        List<ES_DOC_MAINPojo> tES_DOC_MAINPojoList = (List) requestInfo.getData(ES_DOC_MAINPojo.class.getName());
//        String subType = "";
//        if (tES_DOC_MAINPojoList != null && tES_DOC_MAINPojoList.size() > 0) {
//            for (int i = 0; i < tES_DOC_MAINPojoList.size(); i++) {
//                if (i == 0) {
//                    subType = tES_DOC_MAINPojoList.get(i).getSubType();
//                } else {
//                    subType = subType + "," + tES_DOC_MAINPojoList.get(i).getSubType();
//                }
//            }
//            policy.setSubType(subType.trim());
//        }
        //目前只有移动展业有这个单证细类
        String subType = "";
        if ("13".equals(tLCContPojo.getSellType()) || "01".equals(tLCContPojo.getSellType())) {
            List<LCPadImagePojo> lcPadImagePojos = (List<LCPadImagePojo>) requestInfo.getData(LCPadImagePojo.class.getName());
            if (lcPadImagePojos != null && lcPadImagePojos.size() > 0) {
                for (int i = 0; i < lcPadImagePojos.size(); i++) {
                    if (i == 0) {
                        subType = lcPadImagePojos.get(i).getSubType();
                    } else {
                        subType = subType + "," + lcPadImagePojos.get(i).getSubType();
                    }
                }
                policy.setSubType(subType.trim());
            }
        }else{
            policy.setSubType(subType.trim());
        }

        /** 保单问题件SQL（CONTNO：合同号，BackObjType：退回对象类型，dealoverFlag：是否处理完毕）互联网核心不需要问题件*/

        int rows = 0;
        /** 单据类型SQL（OTHERNO：对应其他号码（此处提数对应合同号），OTHERNOTYPE：其它号码类型，CODE：单据类型）*/
        List<LOPRTManagerPojo> tLOPRTManagerPojoList = (List) requestInfo.getData(LOPRTManagerPojo.class.getName());
        if (tLOPRTManagerPojoList != null && tLOPRTManagerPojoList.size() > 0) {
            rows = tLOPRTManagerPojoList.size() - 1;
            // 其它号码类型
            policy.setOtherNoType(tLOPRTManagerPojoList.get(rows).getOtherNoType());
            // 单据类型
            policy.setCode(tLOPRTManagerPojoList.get(rows).getCode());
        }
        /** 保单受理日期（APPLYDATE: 保单受理日期）*/
        LYVerifyAppPojo tLYVerifyAppPojo = (LYVerifyAppPojo) requestInfo.getData(LYVerifyAppPojo.class.getName());
        if (tLYVerifyAppPojo != null) {
            policy.setAcceptDate(fDate.getDate(tLYVerifyAppPojo.getApplyDate()));
        }
        /** 保单内豁免险数量 */
        String tDutyCode = "";
        int sumPrem = 0;
        int payIntv = 0;
        int tImmunityflagNum = 0;
        //add by wangshuliang 20180129 start
        int payYears = 0;
        //add by liuyu
        /*for(LCPolPojo lcPolPojo : tLCPolPojoList){
            if(lcPolPojo.getPolNo()!=null && lcPolPojo.getPolNo().equals(lcPolPojo.getMainPolNo())){
				payYears=lcPolPojo.getPayYears();
				payIntv=lcPolPojo.getPayIntv();
			}
		}*/
        //add by wangshuliang 20180129 end
        List<LCDutyPojo> tLCDutyPojoList = (List) requestInfo.getData(LCDutyPojo.class.getName());
        for (LCPolPojo lcPolPojo : tLCPolPojoList) {
            payYears = lcPolPojo.getPayYears();
            payIntv = lcPolPojo.getPayIntv();
            for (int i = 0; tLCDutyPojoList != null && i < tLCDutyPojoList.size(); i++) {
                if (tLCDutyPojoList.get(i).getPolNo() != null && tLCDutyPojoList.get(i).getPolNo().equals(lcPolPojo.getPolNo())) {
                    tDutyCode = tLCDutyPojoList.get(i).getDutyCode();
                    tImmunityflagNum = Integer.parseInt(mNBRedisCommon.checkDutyCode(tDutyCode)) > 0 ? tImmunityflagNum + 1 : tImmunityflagNum;
                    if (tDutyCode.length() >= 6 && "03".equals(tDutyCode.substring(4, 6))) {
                        sumPrem += tLCDutyPojoList.get(i).getPrem();
                    } else {
                        //if(lcPolPojo.getPolNo()!=null && lcPolPojo.getPolNo().equals(lcPolPojo.getMainPolNo())){
                        if (lcPolPojo.getPolNo() != null) {
                            switch (payIntv) {
                                case 0:
                                    sumPrem += tLCDutyPojoList.get(i).getPrem();
                                    break;
                                case 1:
                                    sumPrem += tLCDutyPojoList.get(i).getPrem() * 12 * payYears;
                                    break;
                                case 3:
                                    sumPrem += tLCDutyPojoList.get(i).getPrem() * 4 * payYears;
                                    break;
                                case 6:
                                    sumPrem += tLCDutyPojoList.get(i).getPrem() * 2 * payYears;
                                    break;
                                case 12:
                                    sumPrem += tLCDutyPojoList.get(i).getPrem() * payYears;
                                    break;
                            }
                        }
                    }
                }
            }
        }
        policy.setSumprem(sumPrem);
        policy.setImmunityflagNum(tImmunityflagNum);

        /** 本单被保人住院津贴(指定险种已停止售卖) */

        /** 单证细类PadSQL（doccode：单证号码，subType：单证细类）互联网核心可能用不到 */
        policy.setPolicyString1(policy.getSubType());
        /**
         *  移动展业预核保标记获取  alter by wangshuliang  20180904
         *  如果传过来的标记为1101走预核保（规则引擎有抽检规则） 1102 为正式核保（规则引擎去掉抽检规则）
         */
        policy.setMitTime(ConstStatic.funcflag1101);
        List<LKTransTracksPojo> lkTransTracksPojoList = (List<LKTransTracksPojo>) requestInfo.getData(LKTransTracksPojo.class.getName());
        if (lkTransTracksPojoList != null && lkTransTracksPojoList.size() > 0) {
            for (int i = 0; i < lkTransTracksPojoList.size(); i++) {
                if (ConstStatic.funcflag1102.equals(lkTransTracksPojoList.get(i).getTransCodeOri())) {
                    policy.setMitTime(ConstStatic.funcflag1102);
                }
                //本保单是续期保单标记
                if (lkTransTracksPojoList.get(i).getTemp1() != null && !("".equals(lkTransTracksPojoList.get(i).getTemp1())) && "1".equals(lkTransTracksPojoList.get(i).getTemp1())) {
                    policy.setRenewalFlag(true);
                } else {
                    policy.setRenewalFlag(false);
                }
                //父保单号
                if (lkTransTracksPojoList.get(i).getTemp2() != null && !("".equals(lkTransTracksPojoList.get(i).getTemp2()))) {
                    policy.setBeforeContno(lkTransTracksPojoList.get(i).getTemp2());
                    //续保父保单生效日
                    StringBuffer sql = new StringBuffer();
                    VData vData = new VData();
                    TransferData tParam = new TransferData();
                    sql.append("SELECT (SELECT TO_CHAR(A.CVALIDATE)" +
                            "                   FROM LCCONT A" +
                            "                  WHERE EXISTS (SELECT 1" +
                            "                           FROM LCWECHATRELATION B" +
                            "                          WHERE B.BEFORECONTNO = A.CONTNO" +
                            "                            AND B.CONTNO = ?)) FIELD1 FROM DUAL");
                    tParam.setNameAndValue("CONTNO", "string:" + lkTransTracksPojoList.get(i).getTemp2());
                    vData.add(sql.toString());
                    vData.add(tParam);
                    Connection tConnection = null;
                    try {
                        tConnection = DBConnPool.getConnection("basedataSource");
                        ExeSQL exeSQL = new ExeSQL(tConnection);
                        String ssrs = exeSQL.getOneValue(vData);
                        if (ssrs != null) {
                            policy.setLastPolCvalidate(Util.toDate(ssrs));
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        logger.error(ExceptionUtils.exceptionToString(e));
                        requestInfo.addError(ExceptionUtils.exceptionToString(e));
                        return;
                    } finally {
                        if (tConnection != null) {
                            try {
                                tConnection.close();
                            } catch (SQLException e) {
                                e.printStackTrace();
                                requestInfo.addError(ExceptionUtils.exceptionToString(e));
                                logger.error(ExceptionUtils.exceptionToString(e));
                                return;
                            }
                        }
                    }
                }
            }
        } else {
            logger.error("LKTransTracks数据为空" + requestInfo.toString());
            requestInfo.addError("LKTransTracks数据为空");
            return;
        }
		/*if(tLYVerifyAppPojo !=null){
			String mittime = tLYVerifyAppPojo.getStandbyFlag2();
			mittime = mittime == null ? "" : mittime.trim();
			policy.setMitTime(mittime);
		}*/
    }
}
