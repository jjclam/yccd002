package com.sinosoft.cloud.cbs.uw;

import com.sinosoft.cloud.bl.AbstractBL;
import com.sinosoft.cloud.rest.TradeInfo;
import com.sinosoft.lis.db.LDPlanDutyParamDB;
import com.sinosoft.lis.db.LDPlanRiskDB;
import com.sinosoft.lis.entity.*;
import com.sinosoft.lis.pos.entity.hardparse.child.LCBnf;
import com.sinosoft.lis.pubfun.Arith;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LDPlanRiskSchema;
import com.sinosoft.lis.vschema.LCDutySet;
import com.sinosoft.lis.vschema.LDPlanDutyParamSet;
import com.sinosoft.lis.vschema.LDPlanRiskSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class CheckCombinationProductServiceImpl extends AbstractBL {
    @Override
    public TradeInfo dealData(TradeInfo requestInfo) {
        LCContPojo lcContPojo = (LCContPojo) requestInfo.getData(LCContPojo.class.getName());
        List<LCPolPojo> lcPolPojoList= (List<LCPolPojo>) requestInfo.getData(LCPolPojo.class.getName());
        List<LCInsuredPojo>  lcInsuredPojoList = (List<LCInsuredPojo>) requestInfo.getData(LCInsuredPojo.class.getName());
        List<LCDutyPojo>  tLCDutyPojoList = (List<LCDutyPojo>)  requestInfo.getData(LCDutyPojo.class.getName());
        GlobalPojo GlobalPojo=(GlobalPojo)requestInfo.getData(GlobalPojo.class.getName());


        /******************************* 险种信息封装开始 *********************************************/
        ExeSQL tExeSQL = new ExeSQL();
        String contPlanCode = lcInsuredPojoList.get(0).getContPlanCode();
        LDPlanRiskDB tLDPlanRiskDB = new LDPlanRiskDB();
        tLDPlanRiskDB.setContPlanCode(contPlanCode);
        LDPlanRiskSet tLDPlanRiskSet = tLDPlanRiskDB.query();
        if (tLDPlanRiskSet != null && tLDPlanRiskSet.size() > 0) {

            lcPolPojoList.clear();
            tLCDutyPojoList.clear();

            for (int t = 1; t <= tLDPlanRiskSet.size(); t++) {
                LDPlanRiskSchema tLDPlanRiskSchema = new LDPlanRiskSchema();
                tLDPlanRiskSchema = tLDPlanRiskSet.get(t);
                LCPolPojo tLCPolPojo = new LCPolPojo();
                tLCPolPojo.setSpecifyValiDate("Y");
                tLCPolPojo.setCValiDate(lcContPojo.getCValiDate()); // 生效日
                tLCPolPojo.setEndDate(GlobalPojo.getRiskEndDate());
                tLCPolPojo.setAgentCode(lcContPojo.getAgentCode());
                tLCPolPojo.setSignDate("");
                String tSQL = "select distinct dutycode from ldplandutyparam where contplancode = '"
                        + contPlanCode
                        + "' "
                        + "and riskcode = '"
                        + tLDPlanRiskSchema.getRiskCode()
                        + "' order by dutycode";

                SSRS tSSRS = new SSRS();
                tSSRS = tExeSQL.execSQL(tSQL);
                List<LCDutyPojo>   tLCDutyPojoSetList = new ArrayList();
                if (tSSRS != null && tSSRS.getMaxRow() > 0) {

                    for (int s = 1; s <= tSSRS.getMaxRow(); s++) {
                        LDPlanDutyParamDB tLDPlanDutyParamDB = new LDPlanDutyParamDB();
                        tLDPlanDutyParamDB.setContPlanCode(contPlanCode);
                        tLDPlanDutyParamDB.setRiskCode(tLDPlanRiskSchema.getRiskCode());
                        tLDPlanDutyParamDB.setDutyCode(tSSRS.GetText(s, 1));

                        LDPlanDutyParamSet tLDPlanDutyParamSet = tLDPlanDutyParamDB.query();
                        if (tLDPlanDutyParamSet == null
                                || tLDPlanDutyParamSet.size() <= 0) {
                            CError.buildErr(this, "未查询到产品组合配置险种信息。");
                        }

                        LCDutyPojo tLCDutyPojo = new LCDutyPojo();
                        tLCDutyPojo.setDutyCode(tSSRS.GetText(s, 1));
                        tLCDutyPojo.setGetStartDate(tLCPolPojo.getCValiDate());
                        if (!transPlanToDuty(tLCDutyPojo,tLDPlanDutyParamSet)) {
                            CError.buildErr(this,"从保险计划向责任赋值失败!");
                            return null;
                        }

                        SSRS ssrs= tExeSQL.execSQL("select payendyear,payendyearflag,insuyear from lmduty where dutycode='"+tSSRS.GetText(s, 1)+"'");


                        tLCDutyPojo.setPayEndYear(ssrs.GetText(1,1));
                        tLCDutyPojo.setPayEndYearFlag(ssrs.GetText(1,2));
                        //        tLCDutyPojo.setInsuYear(ssrs.GetText(1,3));
                        tLCDutyPojo.setPayIntv(lcContPojo.getPayIntv());
                        tLCDutyPojo.setSSFlag("0");
                        tLCDutyPojoSetList.add(tLCDutyPojo);
                        tLCDutyPojoList.add(tLCDutyPojo);
                    }
                }
                double prem = 0.00;
                double amnt = 0.00;
                for (int s = 0; s < tLCDutyPojoSetList.size(); s++) {
                    prem = Arith.add(prem, tLCDutyPojoSetList.get(s).getPrem());
                    amnt = Arith.add(amnt, tLCDutyPojoSetList.get(s).getAmnt());
                }
                tLCPolPojo.setRiskCode(tLDPlanRiskSchema.getRiskCode()); // 险种编码
                //            tLCPolPojo.setdistriflag(tExeSQL.getOneValue("select decode(RinsFlag, 'N', '0', '1' ) from lmrisk where riskcode = '" + tLCPolPojo.getRiskCode() + "'"));
                tLCPolPojo.setAmnt(amnt); // 保额
                tLCPolPojo.setPrem(prem); // 保费
                tLCPolPojo.setPayYears(tLCDutyPojoList.get(1).getPayEndYear()); // 缴费期间
                tLCPolPojo.setPayIntv(lcContPojo.getPayIntv()); // 缴费频率
                tLCPolPojo.setInsuYear(tLCDutyPojoList.get(1).getInsuYear()); // 保障年期
                tLCPolPojo.setInsuYearFlag(tLCDutyPojoList.get(1).getInsuYearFlag()); // 保险期间类型
                tLCPolPojo.setPayEndYear(tLCDutyPojoList.get(1).getPayEndYear());
                tLCPolPojo.setPayEndYearFlag(tLCDutyPojoList.get(1).getPayEndYearFlag());
                tLCPolPojo.setRnewFlag(!PubFun.isEmpty(lcContPojo.getRnewFlag()+"") ? Integer.parseInt(lcContPojo.getRnewFlag()+"") : -2);
                lcPolPojoList.add(tLCPolPojo);
            }

            GlobalPojo.setMainRiskCode(tLDPlanRiskSet.get(1).getMainRiskCode());

        }

        if("1025001".equals(contPlanCode)&&(tLCDutyPojoList==null||tLCDutyPojoList.size()==0||tLCDutyPojoList.get(0).getDutyCode()==null||"".equals(tLCDutyPojoList.get(0).getDutyCode()))){
            String dutycode = tExeSQL.getOneValue("select dutycode  from lmriskduty where riskcode='"+contPlanCode+"'");
            LCDutyPojo lcDutyPojo = new LCDutyPojo();
            lcDutyPojo.setDutyCode(dutycode);
            lcDutyPojo.setGetStartDate(lcPolPojoList.get(0).getCValiDate());
            lcDutyPojo.setPayEndYear(lcPolPojoList.get(0).getPayEndYear());
            lcDutyPojo.setPayEndYearFlag(lcPolPojoList.get(0).getPayEndYearFlag());
            lcDutyPojo.setPayIntv(lcPolPojoList.get(0).getPayIntv());
            lcDutyPojo.setInsuYear(lcPolPojoList.get(0).getInsuYear());
            lcDutyPojo.setInsuYearFlag(lcPolPojoList.get(0).getInsuYearFlag());
            lcDutyPojo.setPrem(lcPolPojoList.get(0).getPrem());
            lcDutyPojo.setAmnt(lcPolPojoList.get(0).getAmnt());
            lcDutyPojo.setSSFlag("0");
            List<LCDutyPojo> dutyList = new ArrayList<LCDutyPojo>();
            dutyList.add(lcDutyPojo);
            requestInfo.addData(LCDutyPojo.class.getName(),dutyList);
        }

        return requestInfo;
    }

    private boolean transPlanToDuty(LCDutyPojo tLCDutyPojo, LDPlanDutyParamSet tLDPlanDutyParamSet) {
        String tDutyCode = tLCDutyPojo.getDutyCode();
        for (int i = 1; i <= tLDPlanDutyParamSet.size(); i++) {
            String CalFactor = tLDPlanDutyParamSet.get(i).getCalFactor();
            String CalFactorValue = tLDPlanDutyParamSet.get(i).getCalFactorValue();
            String tCalDutyCode = tLDPlanDutyParamSet.get(i).getDutyCode();
            if (tDutyCode.trim().equals(tCalDutyCode.trim())) {
                if (tLCDutyPojo.getFieldIndex(CalFactor) > -1) {
                    if (CalFactor.equalsIgnoreCase("Prem")
                            || CalFactor.equalsIgnoreCase("Amnt")
                            || CalFactor.equalsIgnoreCase("GetLimit")
                            || CalFactor.equalsIgnoreCase("GetRate")) {
                        tLCDutyPojo.setV(CalFactor, String.valueOf(Double.parseDouble(CalFactorValue) * 1));
                    } else {
                        tLCDutyPojo.setV(CalFactor, CalFactorValue);
                    }
                }
                tLCDutyPojo.setMult(1);
            }
        }
        return true;
    }


}
