package com.sinosoft.cloud.cbs.trules.data;

import com.sinosoft.cache.dao.impl.RedisCommonDao;
import com.sinosoft.cloud.cbs.rules.data.Util;
import com.sinosoft.cloud.cbs.trules.bom.LcInsured;
import com.sinosoft.cloud.cbs.trules.specontract.SearchHistoryInfoSrvBL;
import com.sinosoft.cloud.cbs.trules.util.DataSourceName;
import com.sinosoft.cloud.cbs.trules.util.MulDataExexSql;
import com.sinosoft.cloud.cbs.trules.util.SqlConst;
import com.sinosoft.cloud.rest.TradeInfo;
import com.sinosoft.lis.entity.*;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.utility.ExceptionUtils;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.joda.time.DateTime;
import org.joda.time.Months;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import rx.internal.operators.OnSubscribeFromArray;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import static com.sinosoft.cloud.cbs.trules.util.SqlConst.AccInsurdFlightAccidentAmntSql;

/**
 * @Author:wangshuliang
 * @Description: 被保人既往提数
 * @Date:Created in 9:07 2018/6/6
 * @Modified by:
 */
@Component
public class TLcInsuredBL {
    @Autowired
    RedisCommonDao redisCommonDao;
    @Autowired
    SearchHistoryInfoSrvBL searchHistoryInfoSrvBL;
    @Value("${cloud.uw.barrier.tcontrol}")
    private String tdataFlag;

    private Log logger = LogFactory.getLog(getClass());


    public TradeInfo getLcInsuredBL(TradeInfo tradeInfo) throws ParseException {
        String mCurrentDate = PubFun.getCurrentDate();
        //如果是本单则累计死亡险
        List<LCDutyPojo> lcDutyPojos = (List<LCDutyPojo>) tradeInfo.getData(LCDutyPojo.class.getName());
        List<LCPolPojo> lcPolPojos = (List<LCPolPojo>) tradeInfo.getData(LCPolPojo.class.getName());
        LCPolicyInfoPojo lcPolicyInfoPojo = (LCPolicyInfoPojo) tradeInfo.getData(LCPolicyInfoPojo.class.getName());
        LCContPojo lcContPojo = (LCContPojo) tradeInfo.getData(LCContPojo.class.getName());
        List<LCInsuredPojo> lcInsureds = (List<LCInsuredPojo>) tradeInfo.getData(LCInsuredPojo.class.getName());
        if (lcInsureds == null || lcInsureds.size() == 0) {
            logger.info("被保人信息为空" + tradeInfo.toString());
            tradeInfo.addError("被保人信息为空");
            return tradeInfo;
        }
        LCInsuredPojo lcInsuredPojo = lcInsureds.get(0);
        String insuredbirthday = lcInsuredPojo.getBirthday();
        String insuredno = lcInsuredPojo.getInsuredNo();
        MulDataExexSql mulDataExexSql = new MulDataExexSql();
        String cvalidate = lcPolPojos.get(0).getCValiDate();
        String enddate = lcPolPojos.get(0).getEndDate();
      /*  String enddate = lcPolPojos.get(0).getCValiDate();
        String cvalidate = lcPolPojos.get(0).getEndDate();*/
        String jyContNo = "";
        String orderType = "";
        if (lcPolicyInfoPojo != null) {
            //合同编号
            jyContNo = lcPolicyInfoPojo.getJYContNo();
            //出单方式
            orderType = lcPolicyInfoPojo.getOrderType();
        }
        String policyno = lcContPojo.getPolicyNo();
        String contno = lcContPojo.getContNo();
        //本单死亡
        double deathAmnt = 0;
        //本单同一被保人疾病身故保额
        double accDiseaseDieAmnt = 0;
        //同一被保人航班飞机意外险保额
        double accInsurdFlightAccidentAmnt = 0;
        //同一被保人火车地铁意外险保额
        double accInsurdTrainAccidentAmnt = 0;
        //同一被保人轮船摆渡意外险保额
        double accInsurdTrafficAccidentAmnt = 0;
        //同一被保人自驾车意外险保额
        double accInsurdPrivateCarAccidentAmnt = 0;
        //累计同一被保人市内公交意外险保额
        double accInsurdCommonalityBusAccidentAmnt = 0;
        //累计同一被保人长途汽车意外险保额
        double accInsurdLongBusAccidentAmnt = 0;
        //同一被保人出租车意外险保额
        double accInsurdTaxiAccidentAmnt = 0;
        //同一被保人借款人意外险保额
        double accInsurdBorrowBeyondAmnt = 0;
        //山东同一借款合同号所有借款人意外险保额
        double accShanDongBorrowRiskAmnt = 0;
        //同一被保人航班飞机意外险保额
        double accInsurdMyComAllPlanAccidentAmnt = 0;
        //全国同一借款合同号下6807险种保单保额
        double accQuanGuoBorrowRiskAmnt = 0;
        //随E行交通意外保障计划累计保费
        double accSeABCDPrem = 0;
        //公交出租车意外伤害保额
      //  double accBusTaxiAmnt = 0;
        //市内公交车意外伤害责任保额
        double accBusAmnt = 0;
        //出租车意外伤害保额
        double accTaxiAmnt = 0;
        //自驾车意外伤害累计责任保额
        double accCarAmnt = 0;
        //随E驾交通意外保费
        double accSEGAmnt = 0;
        //随E乘交通意外保费
        double accSEHAmnt = 0;
        //至尊行交通意外保费
        double accSEFAmnt = 0;
        //随意飞交通意外保费
        double accSEIAmnt = 0;
        //长途汽车意外伤害保额
        double acc681006Amnt = 0;
        //火车、地铁意外伤害保额
        double accTrainSubwayAmnt = 0;
        //轮船、摆渡意外伤害责任保额
        double accSteamerAmnt = 0;
        //航班飞机意外伤害责任保额
        double accPlaneAmnt = 0;
        //同一被保人套餐有效保单数量
        double accInsurdPackageValidNumber = 0;
        //同一被保人套餐有效保单份数
        double accInsurdPackageValidCopies = 0;
        //同一中介机构同一借款合同号的不同被保人的数量
        double accAgencyBorrowDiffInsurdNum = 0;
        //同一中介机构同一借款合同号的相同被保人的数量
        double accAgencyBorrowInsurdNum = 0;
        //SEE、SEN保险计划份数
        int pastSeeSenNum = 0;
        //SEO保险计划份数
        int pastSeoNum = 0;
        //SEL、SEM保险计划份数
        int pastSelSemNum = 0;
        //累计SESXNH份数
        int pastSESXNHNum =0;
        /***********************累计本单*************/
        //if (policyno != null && !"".equals(policyno)) {
        accInsurdPackageValidCopies = lcContPojo.getMult();
        //}
        if (!"4".equals(lcContPojo.getAppFlag()) && "04".equals(orderType)) {
            accAgencyBorrowInsurdNum = 1;
        }
        for (int i = 0; i < lcPolPojos.size(); i++) {
            String polno = lcPolPojos.get(i).getPolNo();
            String riskCode = lcPolPojos.get(i).getRiskCode();
            double amnt = lcPolPojos.get(i).getAmnt();
            String manageCom = lcPolPojos.get(i).getManageCom().substring(0, 4);
            RiskAmnt_DeathPojo riskAmnt_deathPojo = redisCommonDao.getEntityRelaDB(RiskAmnt_DeathPojo.class, riskCode + "|||" + "0");
            if (riskAmnt_deathPojo != null) {
                for (int j = 0; j < lcDutyPojos.size(); j++) {
                    if (polno.equals(lcDutyPojos.get(j).getPolNo())) {
                        deathAmnt += lcDutyPojos.get(j).getAmnt();
                    }
                }
            }


            if ("1805".equals(riskCode) || "1806".equals(riskCode)) {
                accDiseaseDieAmnt += amnt;
            }
            if ("6807".equals(riskCode)) {
                accInsurdBorrowBeyondAmnt += amnt;
            }
            if ("6807".equals(riskCode) && "8637".equals(manageCom)
                    && "04".equals(orderType)) {
                accShanDongBorrowRiskAmnt += amnt;
            }
            if ("6807".equals(riskCode) && policyno != null && !"".equals(policyno)
                    && contno.equals(lcPolicyInfoPojo.getContNo())) {
                accQuanGuoBorrowRiskAmnt += amnt;
            }
            /*String contplancode = "SEA,SEB,SEC,SED";
            if (contplancode.contains(lcInsuredPojo.getContPlanCode())) {
                accSeABCDPrem += lcPolPojos.get(i).getPrem();
            }
            if ("SEG".equals(lcInsuredPojo.getContPlanCode())) {
                accSEGAmnt += lcPolPojos.get(i).getPrem();
            }
            if ("SEH".equals(lcInsuredPojo.getContPlanCode())) {
                accSEHAmnt += lcPolPojos.get(i).getPrem();
            }
            if ("SEF".equals(lcInsuredPojo.getContPlanCode())) {
                accSEFAmnt += lcPolPojos.get(i).getPrem();
            }
            if ("SEI".equals(lcInsuredPojo.getContPlanCode())) {
                accSEIAmnt += lcPolPojos.get(i).getPrem();
            }*/
            accInsurdPackageValidNumber = lcPolPojos.size();
            if ("SEE".equals(lcInsuredPojo.getContPlanCode()) || "SEN".equals(lcInsuredPojo.getContPlanCode())) {
                pastSeeSenNum = lcPolPojos.size();
            }
            if ("SEO".equals(lcInsuredPojo.getContPlanCode())) {
                pastSeoNum = lcPolPojos.size();
            }
            if ("SESXNH".equals(lcInsuredPojo.getContPlanCode())) {
                pastSESXNHNum = lcPolPojos.size();
            }
            if ("SEL".equals(lcInsuredPojo.getContPlanCode()) || "SEM".equals(lcInsuredPojo.getContPlanCode())) {
                pastSelSemNum = lcPolPojos.size();
            }
            for (int j = 0; j < lcDutyPojos.size(); j++) {
                String str = lcDutyPojos.get(j).getDutyCode();
                String dutyCode = str.substring(str.length() - 2, str.length());
                if (("6805".equals(riskCode) || "6810".equals(riskCode))
                        && polno.equals(lcDutyPojos.get(j).getPolNo())
                        && "05".equals(dutyCode)) {
                    accInsurdFlightAccidentAmnt += lcDutyPojos.get(j).getAmnt();
                }
                if (("6805".equals(riskCode) || "6810".equals(riskCode))
                        && polno.equals(lcDutyPojos.get(j).getPolNo())
                        && "03".equals(dutyCode)) {
                    accInsurdTrainAccidentAmnt += lcDutyPojos.get(j).getAmnt();
                }
                if (("6805".equals(riskCode) || "6810".equals(riskCode))
                        && polno.equals(lcDutyPojos.get(j).getPolNo())
                        && "04".equals(dutyCode)) {
                    accInsurdTrafficAccidentAmnt += lcDutyPojos.get(j).getAmnt();
                }
                if (("6805".equals(riskCode) || "6810".equals(riskCode))
                        && polno.equals(lcDutyPojos.get(j).getPolNo())
                        && "01".equals(dutyCode)) {
                    accInsurdPrivateCarAccidentAmnt += lcDutyPojos.get(j).getAmnt();
                }
                if (("6805".equals(riskCode) || "6810".equals(riskCode))
                        && polno.equals(lcDutyPojos.get(j).getPolNo())
                        && "02".equals(dutyCode)) {
                    accInsurdCommonalityBusAccidentAmnt += lcDutyPojos.get(j).getAmnt();
                }
                if (("6805".equals(riskCode) || "6810".equals(riskCode))
                        && polno.equals(lcDutyPojos.get(j).getPolNo())
                        && "06".equals(dutyCode)) {
                    accInsurdLongBusAccidentAmnt += lcDutyPojos.get(j).getAmnt();
                }
                if (("6805".equals(riskCode) || "6810".equals(riskCode))
                        && polno.equals(lcDutyPojos.get(j).getPolNo())
                        && "07".equals(dutyCode)) {
                    accInsurdTaxiAccidentAmnt += lcDutyPojos.get(j).getAmnt();
                }
                if ("6810".equals(riskCode) && polno.equals(lcDutyPojos.get(j).getPolNo())
                        && "05".equals(dutyCode)) {
                    accInsurdMyComAllPlanAccidentAmnt += lcDutyPojos.get(j).getAmnt();
                }
             /*   if ("681007".equals(dutyCode) || "680502".equals(dutyCode)) {
                    accBusTaxiAmnt += lcDutyPojos.get(j).getAmnt();
                }
                if ("681001".equals(dutyCode) || "680501".equals(dutyCode)) {
                    accCarAmnt += lcDutyPojos.get(j).getAmnt();
                }*/
           /*     if ("681006".equals(dutyCode) || "680501".equals(dutyCode)) {
                    acc681006Amnt += lcDutyPojos.get(j).getAmnt();
                }
                if ("681003".equals(dutyCode) || "680503".equals(dutyCode)) {
                    accTrainSubwayAmnt += lcDutyPojos.get(j).getAmnt();
                }
                if ("681004".equals(dutyCode) || "680504".equals(dutyCode)) {
                    accSteamerAmnt += lcDutyPojos.get(j).getAmnt();
                }*/
              /*  if ((!"SEK".equals(lcInsuredPojo.getContPlanCode())
                        || !"SEJ".equals(lcInsuredPojo.getContPlanCode()))
                        && ("681005".equals(dutyCode) || "680505".equals(dutyCode))) {
                    accPlaneAmnt += lcDutyPojos.get(j).getAmnt();
                }*/
            }
        }
        /***********************累计历史*************/
        LcInsured lcInsured = new LcInsured();
        if ("FALSE".equals(tdataFlag.toUpperCase())) {


            //计算年龄
        /*    final DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyy-MM-dd");
            final DateTime start = formatter.parseDateTime(insuredbirthday);
            final DateTime end = formatter.parseDateTime(mCurrentDate);
            final int months = Months.monthsBetween(start, end).getMonths();
            int age = (int) Math.floor(months / 12);*/
            int age =PubFun.calInterval(insuredbirthday,mCurrentDate,"Y");
            if (age < 18) {
                String tsql = "select nvl((select  CustomerNo  from LDPerson where "
                        + "CustomerNo = ?  and rownum=1),'0000000000')  v_insuredno from dual";
                logger.info("通过五要素查询客户号:" + tsql);
                VData vData = new VData();
                TransferData transferData = new TransferData();
                transferData.setNameAndValue("insuredno", "String:" + lcInsuredPojo.getInsuredNo());
                vData.add(tsql);
                vData.add(transferData);
                SSRS mSSRS = null;
                try {
                    mSSRS = mulDataExexSql.excuteSql(vData, DataSourceName.TOLD.getSourceName());
                } catch (Exception e) {
                    logger.error(ExceptionUtils.exceptionToString(e));
                    logger.info("通过客户查询五要素失败" + tradeInfo.toString());
                    tradeInfo.addError("通过客户查询五要素失败");
                    return tradeInfo;
                }
                if (mSSRS == null || "0000000000".equals(mSSRS)) {
                    lcInsured.setAccMortalRiskAmnt(deathAmnt);
                } else {
                    String riskCode = "000000";
                    StringBuffer str = new StringBuffer();
                    str.append("select ");
                    str.append("F_AIP_DEATHAMNT(?,?,?,?) ");
                    str.append(" from dual where 1=1 ");
                    logger.info("查询未成年死亡险的执行sql:" + str);
                    VData tVData = new VData();
                    TransferData tParam = new TransferData();
                    tParam.setNameAndValue("v_insuredno", "String:" + mSSRS);
                    tParam.setNameAndValue("riskcode", "String:" + riskCode);
                    tParam.setNameAndValue("endate", "String:" + enddate);
                    tParam.setNameAndValue("cvalidate", "String:" + cvalidate);
                    tVData.add(str.toString());
                    tVData.add(tParam);

                    try {
                        SSRS deathSSRS = mulDataExexSql.excuteSql(tVData, DataSourceName.TOLD.getSourceName());
                        if (deathSSRS != null && deathSSRS.getMaxRow() > 0) {
                            //累计同一被保人死亡风险保额（未成年）
                            lcInsured.setAccMortalRiskAmnt(Util.toDouble(deathSSRS.GetText(1, 1)) + deathAmnt);
                        } else {
                            lcInsured.setAccMortalRiskAmnt(deathAmnt);
                        }
                        SSRS deathSSRSOld = mulDataExexSql.excuteSql(tVData, DataSourceName.OLD.getSourceName());
                        if (deathSSRSOld != null && deathSSRSOld.getMaxRow() > 0) {
                            //累计同一被保人死亡风险保额（未成年）
                            lcInsured.setAccMortalRiskAmnt(Util.toDouble(deathSSRSOld.GetText(1, 1)) + lcInsured.getAccMortalRiskAmnt());
                        }
                    } catch (Exception e) {
                        logger.error(ExceptionUtils.exceptionToString(e));
                        logger.info("查询未成年死亡险的执行sql" + tradeInfo.toString());
                        tradeInfo.addError("查询未成年死亡险的执行sql");
                        return tradeInfo;
                    }
                }
            }
            SSRS pastssrs = null;
            try {
                pastssrs = getInsuredPast(lcInsuredPojo, enddate, cvalidate, jyContNo, lcPolPojos);
            } catch (Exception e) {
                logger.error("团险被保人既往提数异常请检查" + ExceptionUtils.exceptionToString(e));
                logger.error("团险被保人既往提数失败请检查" + tradeInfo.toString());
                tradeInfo.addError("团险被保人既往提数失败请检查");
                return tradeInfo;
            }

            if (pastssrs == null || pastssrs.getMaxRow() == 0) {
                logger.error("团险被保人既往提数失败请检查" + tradeInfo.toString());
                tradeInfo.addError("团险被保人既往提数失败请检查");
                return tradeInfo;
            } else {
                int flag = 0;
                double money = 0.0;
                for (int i = 1; i <= pastssrs.getMaxRow(); i++) {
                    flag = Util.toInt(pastssrs.GetText(i, 1));
                    money = Util.toDouble(pastssrs.GetText(i, 2));
                    switch (flag) {
                        case 1:
                            lcInsured.setAccDiseaseDieAmnt(money + accDiseaseDieAmnt);
                            break;
                        case 2:
                            lcInsured.setAccInsurdFlightAccidentAmnt(money + accInsurdFlightAccidentAmnt);
                            break;
                        case 3:
                            lcInsured.setAccInsurdTrainAccidentAmnt(money + accInsurdTrainAccidentAmnt);
                            break;
                        case 4:
                            lcInsured.setAccInsurdTrafficAccidentAmnt(money + accInsurdTrafficAccidentAmnt);
                            break;
                        case 5:
                            lcInsured.setAccInsurdPrivateCarAccidentAmnt(money + accInsurdPrivateCarAccidentAmnt);
                            break;
                        case 6:
                            lcInsured.setAccInsurdCommonalityBusAccidentAmnt(money + accInsurdCommonalityBusAccidentAmnt);
                            break;
                        case 7:
                            lcInsured.setAccInsurdLongBusAccidentAmnt(money + accInsurdLongBusAccidentAmnt);
                            break;
                        case 8:
                            lcInsured.setAccInsurdTaxiAccidentAmnt(money + accInsurdTaxiAccidentAmnt);
                            break;
                        case 9:
                            lcInsured.setAccInsurdBorrowBeyondAmnt(money + accInsurdBorrowBeyondAmnt);
                            break;
                        case 10:
                            lcInsured.setAccShanDongBorrowRiskAmnt(money + accShanDongBorrowRiskAmnt);
                            break;
                        case 11:
                            lcInsured.setAccInsurdMyComAllPlanAccidentAmnt(money + accInsurdMyComAllPlanAccidentAmnt);
                            break;
                        case 12:
                            lcInsured.setAccQuanGuoBorrowRiskAmnt(money + accQuanGuoBorrowRiskAmnt);
                            break;
                        case 13:
                            lcInsured.setAccSeABCDPrem(money + accSeABCDPrem);
                            break;
//                        case 14:
//                            lcInsured.setAccBusTaxiAmnt(money + accBusTaxiAmnt);
//                            break;
                        case 15:
                            lcInsured.setAccCarAmnt(money + accCarAmnt);
                            break;
                        case 16:
                            lcInsured.setAccSEGAmnt(money + accSEGAmnt);
                            break;
                        case 17:
                            lcInsured.setAccSEHAmnt(money + accSEHAmnt);
                            break;
                        case 18:
                            lcInsured.setAccSEFAmnt(money + accSEFAmnt);
                            break;
                        case 19:
                            lcInsured.setAccSEIAmnt(money + accSEIAmnt);
                            break;
                        case 20:
                            lcInsured.setAcc681006Amnt(money + acc681006Amnt);
                            break;
                        case 21:
                            lcInsured.setAccTrainSubwayAmnt(money + accTrainSubwayAmnt);
                            break;
                        case 22:
                            lcInsured.setAccSteamerAmnt(money + accSteamerAmnt);
                            break;
                        case 23:
                            lcInsured.setAccPlaneAmnt(money + accPlaneAmnt);
                            break;
                        case 24:
                            lcInsured.setAccInsurdPackageValidNumber(money + accInsurdPackageValidNumber);
                            break;
                        case 25:
                            lcInsured.setAccInsurdPackageValidCopies(money + accInsurdPackageValidCopies);
                            break;
                        case 26:
                            lcInsured.setAccAgencyBorrowDiffInsurdNum(money + accAgencyBorrowDiffInsurdNum);
                            break;
                        case 27:
                            lcInsured.setPastSeeSenNum((int) (money + pastSeeSenNum));
                            break;
                        case 28:
                            lcInsured.setPastSeoNum((int) (money + pastSeoNum));
                            break;
                        case 29:
                            lcInsured.setPastSelSemNum((int) (money + pastSelSemNum));
                            break;
                        case 30:
                            lcInsured.setAccBusAmnt((int) (money + accBusAmnt));
                            break;
                        case 31:
                            lcInsured.setAccTaxiAmnt((int) (money + accTaxiAmnt));
                            break;
                        case 32:
                            lcInsured.setPastSEPNum((int) (money));
                            break;
                        case 33:
                            lcInsured.setPastJSJXBCNum((int) (money));
                            break;
                        case 34:
                            lcInsured.setPastSESXNHNum((int) (money+pastSESXNHNum));
                            break;
                    }
                }
            }


            String AccAgencyBorrowsql = "Select distinct p.insuredno " +
                    "  From lcpolicyinfo p, lcpol b, lccont lc" +
                    " Where 1 = 1" +
                    "   And p.standbyflag5 = '04'" +
                    "   And lc.appflag <> '4'" +
                    "   And p.contno = lc.contno" +
                    "   And p.contno = b.contno" +
                    "   And b.cvalidate < ? " +
                    "   And b.enddate > ? " +
                    "   And p.standbyflag1 = ? ";
            logger.info("累计同一中介机构同一借款合同号的相同被保人的数量:" + AccAgencyBorrowsql);
            VData tVData27 = new VData();
            TransferData tParam27 = new TransferData();
            tParam27.setNameAndValue("enddate", "String:" + enddate);
            tParam27.setNameAndValue("cvalidate", "String:" + cvalidate);
            tParam27.setNameAndValue("standbyflag1", "String:" + jyContNo);
            //tParam27.setNameAndValue("insuredno", "String:" + lcInsuredPojo.getInsuredNo());
            tVData27.add(AccAgencyBorrowsql);
            tVData27.add(tParam27);

            try {
                SSRS AccAgencyBorrowSSRS = mulDataExexSql.excuteSql(tVData27, DataSourceName.TOLD.getSourceName());
                if (AccAgencyBorrowSSRS != null && AccAgencyBorrowSSRS.getMaxRow() > 0) {
                    List<String> insurednoList = new ArrayList<>();
                    for (int i = 1; i <= AccAgencyBorrowSSRS.getMaxRow(); i++) {
                        insurednoList.add(AccAgencyBorrowSSRS.GetText(i, 1));
                    }
                    insurednoList.remove(insuredno);
                    //累计同一被保人套餐有效保单份数
                    lcInsured.setAccAgencyBorrowInsurdNum(insurednoList.size() + accAgencyBorrowInsurdNum);
                } else {
                    lcInsured.setAccAgencyBorrowInsurdNum(accAgencyBorrowInsurdNum);
                }
            } catch (Exception e) {
                logger.error(ExceptionUtils.exceptionToString(e));
                logger.info("累计同一中介机构同一借款合同号的相同被保人的数量:提数失败" + tradeInfo.toString());
                tradeInfo.addError("累计同一中介机构同一借款合同号的相同被保人的数量：提数失败:");
                return tradeInfo;
            }
            //查询既往理赔信息  //理赔拒保状 不存在是0 存在是1
            try {
                List<String> ClmNoList = new ArrayList<>();
                SSRS ssrs = searchHistoryInfoSrvBL.dealDataClaim(lcContPojo, lcInsuredPojo);
                if (ssrs == null || ssrs.getMaxRow() < 1) {
                    logger.info(lcInsuredPojo.getInsuredNo() + "[AIPE0013]:团险核心没有符合条件的查询数据");
                    lcInsured.setWhetherSettlementInfo("0");
                } else {
                    logger.info(lcInsuredPojo.getInsuredNo() + "此被保人存在既往理赔");
                    lcInsured.setWhetherSettlementInfo("1");
                    for (int i = 1; i <=ssrs.getMaxRow(); i++) {
                        ClmNoList.add(ssrs.GetText(i,1));
                    }
                    //如果存在既往理赔则放在tradeinfo中 为查询特殊审批信息做准备
                    tradeInfo.addData("WhetherSettlementInfo","WhetherSettlementInfo");
                }
//                if ("0".equals(lcInsured.getWhetherSettlementInfo())) {
//                    //如果团险核心库没有查到数据则去老核心数据库去查  如果能查到则不查
//                    ssrs = searchHistoryInfoSrvBL.dealDataClaimOLD(lcContPojo, lcInsuredPojo);
//                    if (ssrs == null || ssrs.getMaxRow() < 1) {
//                        logger.info(lcInsuredPojo.getInsuredNo() + "[AIPE0013]:老核心没有符合条件的查询数据");
//                        lcInsured.setWhetherSettlementInfo("0");
//                    } else {
//                        logger.info(lcInsuredPojo.getInsuredNo() + "此被保人存在既往理赔");
//                        lcInsured.setWhetherSettlementInfo("1");
//                        for (int i = 1; i <=ssrs.getMaxRow(); i++) {
//                            ClmNoList.add(ssrs.GetText(i,1));
//                        }
//                        tradeInfo.addData("WhetherSettlementInfo","WhetherSettlementInfo");
//                    }
//                }
                //存放理赔号 特殊审批的查询准备数据
                tradeInfo.addData("ClmNo",ClmNoList);
            } catch (Exception e) {
                logger.error(ExceptionUtils.exceptionToString(e));
                logger.info("查询既往理赔信息:提数失败" + tradeInfo.toString());
                tradeInfo.addError("查询既往理赔信息：提数失败:");
                return tradeInfo;
            }

            //查询既往核保信息

            try {
                SSRS uwSSRS = searchHistoryInfoSrvBL.dealDataUW(lcContPojo, lcInsuredPojo);
                if (uwSSRS == null || uwSSRS.getMaxRow() < 1) {
                   // logger.info("[AIPE0013]:团险核心库没有符合条件的查询数据");
                    logger.info("此被保人不存在既往核保");
                    lcInsured.setWhetherPostponeUnAccept("0");
                } else {
                    logger.info("此被保人存在既往核保");
                    lcInsured.setWhetherPostponeUnAccept("1");
                    tradeInfo.addData("WhetherPostponeUnAccept","WhetherPostponeUnAccept");
                }
//                if ("0".equals(lcInsured.getWhetherPostponeUnAccept())) {
//                    //如果团险核心库查不到数据则去老核心数据去查
//                    SSRS uwSSRSOLD = searchHistoryInfoSrvBL.dealDataUWOLD(lcContPojo, lcInsuredPojo);
//                    if (uwSSRSOLD == null || uwSSRSOLD.getMaxRow() < 1) {
//                        logger.info("[AIPE0013]:老核心没有符合条件的查询数据");
//                        lcInsured.setWhetherPostponeUnAccept("0");
//                    } else {
//                        logger.info("此被保人存在既往核保");
//                        lcInsured.setWhetherPostponeUnAccept("1");
//                        tradeInfo.addData("WhetherPostponeUnAccept","WhetherPostponeUnAccept");
//                    }
//                }
            } catch (Exception e) {
                logger.error(ExceptionUtils.exceptionToString(e));
                logger.info("查询既往理赔信息:提数失败" + tradeInfo.toString());
                tradeInfo.addError("查询既往理赔信息：提数失败:");
                return tradeInfo;
            }
        } else {
            logger.info("保单号：" + lcContPojo.getContNo() + "，调用核心数据库挡板开始！");
            lcInsured.setWhetherSettlementInfo("0");
            lcInsured.setWhetherPostponeUnAccept("0");
        }

        tradeInfo.addData(LcInsured.class.getName(), lcInsured);
        return tradeInfo;
    }


    public SSRS getInsuredPast(LCInsuredPojo lcInsuredPojo, String enddate, String cvalidate, String jyContNo, List<LCPolPojo> lcPolPojos) {
        //取出主险的险种编码
        String mainRiskCode = "";
        for (int i = 0; i < lcPolPojos.size(); i++) {
            if (lcPolPojos.get(i).getPolNo().equals(lcPolPojos.get(i).getMainPolNo())) {
                mainRiskCode = lcPolPojos.get(i).getRiskCode();
                break;
            }
        }
        //核保优化区分累计风险保额的优化  对应的险种提数对应的字段  不需要的则不进行提数 减少sql查询的次数
        StringBuffer pastSql = new StringBuffer();
        TransferData tParam = new TransferData();
        if ("6810".equals(mainRiskCode)) {
            //累计同一被保人航班飞机意外险保额（6810和6805）  6810 1
            pastSql.append(SqlConst.AccInsurdFlightAccidentAmntSql);
            pastSql.append(" UNION ");
            //累计同一被保人火车地铁意外险保额（6810和6805） 6810 2
            pastSql.append(SqlConst.AccInsurdTrainAccidentAmntSql);
            pastSql.append(" UNION ");
            //累计同一被保人轮船摆渡意外险保额（6810和6805） 6810 3
            pastSql.append(SqlConst.AccInsurdTrafficAccidentAmntSql);
            pastSql.append(" UNION ");
            //累计同一被保人自驾车意外险保额（6810和6805）6810 4
            pastSql.append(SqlConst.AccInsurdPrivateCarAccidentAmntSql);
            pastSql.append(" UNION ");
            //累计同一被保人市内公交意外险保额（6810和6805）6810 5
            pastSql.append(SqlConst.Sql6);
            pastSql.append(" UNION ");
            //累计同一被保人长途汽车意外险保额（6810和6805）6810 6
            pastSql.append(SqlConst.Sql7);
            pastSql.append(" UNION ");
            //累计同一被保人出租车意外险保额（6810和6805）6810 7
            pastSql.append(SqlConst.Sql8);
            pastSql.append(" UNION ");
            //累计同一被保人航班飞机意外险保额（6810） 6810 8
            pastSql.append(SqlConst.Sql11);
            pastSql.append(" UNION ");
            //随E行交通意外保障计划累计保费 6810  9
            pastSql.append(SqlConst.Sql13);
            pastSql.append(" UNION ");
            //公交出租车意外伤害保额 6810  10
//            pastSql.append(SqlConst.Sql14);
//            pastSql.append(" UNION ");
            //市内公交车意外伤害责任  10
            pastSql.append(SqlConst.Sql30);
            pastSql.append(" UNION ");
            //自驾车意外伤害累计责任保额 6810  11
            pastSql.append(SqlConst.Sql15);
            pastSql.append(" UNION  ");
            //累计随E驾交通意外保费 6810  12
            pastSql.append(SqlConst.Sql16);
            pastSql.append(" UNION ");
            //累计随E乘交通意外保费 6810  13
            pastSql.append(SqlConst.Sql17);
            pastSql.append(" UNION ");
            //累计至尊行交通意外保费 6810 14
            pastSql.append(SqlConst.Sql18);
            pastSql.append(" UNION ");
            //累计随意飞交通意外保费 6810 15
            pastSql.append(SqlConst.Sql19);
            pastSql.append(" UNION ");
            //累计长途汽车意外伤害保额 6810 16
            pastSql.append(SqlConst.Sql20);
            pastSql.append(" UNION ");
            //累计火车、地铁意外伤害保额 6810 17
            pastSql.append(SqlConst.Sql21);
            pastSql.append(" UNION ");
            //累计轮船、摆渡意外伤害责任保额: 6810 18
            pastSql.append(SqlConst.Sql22);
            pastSql.append(" UNION ");
            //SEE、SEN保险计划份数  6810 19
            pastSql.append(SqlConst.pastSeeSenNumsql);
            pastSql.append(" UNION ");
            //SEO保险计划份数 6810 20
            pastSql.append(SqlConst.pastSeoNumsql);
            pastSql.append(" UNION ");
            //SEL,SEM保险计划份数 6810 21
            pastSql.append(SqlConst.pastSelSemNumsql);
            pastSql.append(" UNION ");
            //出租车意外伤害责任 22
            pastSql.append(SqlConst.Sql31);
            pastSql.append(" UNION ");
            //累计航班飞机意外伤害责任保额 6810 23
            pastSql.append(SqlConst.Sql23);
            //累计SESXNH份数24
            pastSql.append(" UNION ");
            pastSql.append(SqlConst.pastSESXNHNumsql);
            pastSql.append(" UNION ");

            //累计同一被保人航班飞机意外险保额（6810和6805）
            //累计同一被保人火车地铁意外险保额（6810和6805）
            //累计同一被保人轮船摆渡意外险保额
            //累计同一被保人自驾车意外险保额（6810和6805）
            //累计同一被保人市内公交意外险保额（6810和6805）
            //累计同一被保人长途汽车意外险保额（6810和6805）
            //累计同一被保人出租车意外险保额（6810和6805）
            //累计同一被保人航班飞机意外险保额（6810）
            for (int i = 1; i <= 8; i++) {
                tParam.setNameAndValue("enddate" + i, "String:" + enddate);
                tParam.setNameAndValue("cvalidate" + i, "String:" + cvalidate);
                tParam.setNameAndValue("insuredno" + i, "String:" + lcInsuredPojo.getInsuredNo());
            }
            //随E行交通意外保障计划累计保费
            //公交出租车意外伤害保额
            //自驾车意外伤害累计责任保额
            //累计随E驾交通意外保费
            //累计随E乘交通意外保费
            //累计至尊行交通意外保费
            //累计随意飞交通意外保费
            //累计长途汽车意外伤害保额
            //累计火车、地铁意外伤害保额
            //累计轮船、摆渡意外伤害责任保额
            //SEE、SEN保险计划份数  6810
            // SEO保险计划份数 6810
            //SEL,SEM保险计划份数 6810
            for (int i = 9; i <= 22; i++) {
                tParam.setNameAndValue("insuredno" + i, "String:" + lcInsuredPojo.getInsuredNo());
            }
            tParam.setNameAndValue("insuredno231", "String:" + lcInsuredPojo.getInsuredNo());
            tParam.setNameAndValue("insuredno232", "String:" + lcInsuredPojo.getInsuredNo());
            tParam.setNameAndValue("insuredno241", "String:" + lcInsuredPojo.getInsuredNo());
        }
        if ("6807".equals(mainRiskCode)) {
            //累计同一被保人借款人意外险保额（6807）6807
            pastSql.append(SqlConst.Sql9);
            pastSql.append(" UNION ");
            tParam.setNameAndValue("enddate" + 1, "String:" + enddate);
            tParam.setNameAndValue("cvalidate" + 1, "String:" + cvalidate);
            tParam.setNameAndValue("insuredno" + 1, "String:" + lcInsuredPojo.getInsuredNo());
            //累计山东同一借款合同号所有借款人意外险保额（6807) 6807
            pastSql.append(SqlConst.Sql10);
            pastSql.append(" UNION ");
            //累计山东同一借款合同号所有借款人意外险保额（6807)
            tParam.setNameAndValue("enddate2", "string:" + enddate);
            tParam.setNameAndValue("cvalidate2", "string:" + cvalidate);
            tParam.setNameAndValue("standbyflag12", "string:" + jyContNo);
            //累计全国同一借款合同号下6807险种保单保额: 6807
            pastSql.append(SqlConst.Sql12);
            pastSql.append(" UNION ");
            tParam.setNameAndValue("enddate3", "String:" + enddate);
            tParam.setNameAndValue("cvalidate3", "String:" + cvalidate);
            tParam.setNameAndValue("standbyflag13", "String:" + jyContNo);
            //累计同一中介机构同一借款合同号的不同被保人的数量 6807
            pastSql.append(SqlConst.AgencyBorrowSql);
            pastSql.append(" UNION ");
            tParam.setNameAndValue("enddate4", "String:" + enddate);
            tParam.setNameAndValue("cvalidate4", "String:" + cvalidate);
            tParam.setNameAndValue("standbyflag14", "String:" + jyContNo);
            tParam.setNameAndValue("insuredno4", "String:" + lcInsuredPojo.getInsuredNo());
        }
        //累计同一被保人套餐有效保单数量
        pastSql.append(SqlConst.Sql24);
        pastSql.append(" UNION ");
        //累计同一被保人套餐有效保单数量24
        tParam.setNameAndValue("enddate30", "String:" + enddate);
        tParam.setNameAndValue("cvalidate30", "String:" + cvalidate);
        tParam.setNameAndValue("planCode30", "String:" + lcInsuredPojo.getContPlanCode());
        tParam.setNameAndValue("insuredno30", "String:" + lcInsuredPojo.getInsuredNo());
        //累计同一被保人套餐有效保单份数
        pastSql.append(SqlConst.Sql25);
        pastSql.append(" UNION ");
        tParam.setNameAndValue("enddate31", "String:" + enddate);
        tParam.setNameAndValue("cvalidate31", "String:" + cvalidate);
        tParam.setNameAndValue("insuredno31", "String:" + lcInsuredPojo.getInsuredNo());
        tParam.setNameAndValue("plancode31", "String:" + lcInsuredPojo.getContPlanCode());
        //累计同一被保人疾病身故保额（1806和1805）
        pastSql.append(SqlConst.AccDiseaseDieAmntSql);
        pastSql.append(" UNION ");
        //累计同一被保人疾病身故保额（1806和1805）
        tParam.setNameAndValue("enddate32" , "String:" + enddate);
        tParam.setNameAndValue("cvalidate32", "String:" + cvalidate);
        tParam.setNameAndValue("insuredno32" , "String:" + lcInsuredPojo.getInsuredNo());
        //SEP保险计划份数
        pastSql.append(SqlConst.pastSEPNumsql);
        tParam.setNameAndValue("insuredno33", "String:" + lcInsuredPojo.getInsuredNo());

        //累计同一被保人JSJXBC投保份数
        pastSql.append(" UNION ");
        pastSql.append(SqlConst.pastJSJXBCNumsql);
        tParam.setNameAndValue("insuredno34", "String:" + lcInsuredPojo.getInsuredNo());


        logger.debug("查询核心的sql为" + pastSql.toString());
        VData tVData1 = new VData();
        tVData1.add(pastSql.toString());
        tVData1.add(tParam);
        SSRS ssrs = null;
        try {
            MulDataExexSql mulDataExexSql = new MulDataExexSql();
            ssrs = mulDataExexSql.excuteSql(tVData1, DataSourceName.TOLD.getSourceName());
        } catch (Exception e) {
            logger.error(ExceptionUtils.exceptionToString(e));
            return null;
        }
        return ssrs;
    }

}

