package com.sinosoft.cloud.cbs.uw;

import com.sinosoft.cloud.common.SpringContextUtils;
import com.sinosoft.cloud.microservice.AbstractMicroService;
import com.sinosoft.cloud.rest.TradeInfo;
import org.springframework.stereotype.Component;

@Component("CheckCombinationProductService")
public class CheckCombinationProductService extends AbstractMicroService {
    @Override
    public boolean checkData(TradeInfo requestInfo) {
        return true;
    }

    @Override
    public TradeInfo dealData(TradeInfo requestInfo) {
        CheckCombinationProductServiceImpl tCheckCombinationProductServiceImpl=   SpringContextUtils.getBeanByClass(CheckCombinationProductServiceImpl.class);
        TradeInfo tradeInfo = tCheckCombinationProductServiceImpl.submitData(requestInfo);
        return tradeInfo;
    }
}
