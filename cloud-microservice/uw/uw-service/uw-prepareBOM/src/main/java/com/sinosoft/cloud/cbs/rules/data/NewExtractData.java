package com.sinosoft.cloud.cbs.rules.data;

import com.sinosoft.cloud.cbs.rules.bom.*;
import com.sinosoft.cloud.rest.TradeInfo;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by zkr on 2018/6/30.
 */
@Component
public class NewExtractData {
    private final Log logger = LogFactory.getLog(this.getClass());

    @Autowired
    ExtractPolicy mExtractPolicy;
    @Autowired
    NewExtractApplicantInfo mNewExtractApplicantInfo;
    @Autowired
    NewExtractInsuredInfo mNewExtractInsuredInfo;
    @Autowired
    ExtractCustInfo mExtractCustInfo;
    @Autowired
    ExtractAgent mExtractAgent;
    @Autowired
    NewExtractInsurType mNewExtractInsurType;
    @Autowired
    ExtractBnfcryInfo mExtractBnfcryInfo;
    @Autowired
    NewExtractCheck newExtractCheck;
    @Autowired
    ExtractNotifyAgent mExtractNotifyAgent;
    @Autowired
    ExtractNotifyApplicant mExtractNotifyApplicant;
    @Autowired
    ExtractNotifyInsured mExtractNotifyInsured;
    @Autowired
    NewExtractPast mNewExtractPast;
    @Autowired
    NewExtractAccum mNewExtractAccum;

    /**
     * 提数总接口
     *
     * @return
     * @throws Exception
     */
    public Policy getData(TradeInfo tradeInfo) throws Exception {
        //保单
        Policy policy  = new Policy();
        Applicant applicantInfo = new Applicant();
        List insuredInfoList = new ArrayList();
        List custInfoList = new ArrayList();
        List bnfcryInfoList = new ArrayList();
        Agent agent = new Agent();
        List insurTypeList = new ArrayList();
        List insuredNotifyList = new ArrayList();
        ApplicantNotify applicantNotify = new ApplicantNotify();
        AgentNotify agentNotify = new AgentNotify();

        // 投保人
        policy.setApplicant(applicantInfo);
        // 被保人列表
        policy.setInsuredList(insuredInfoList);
        // 客户信息列表
        policy.setClientInfoList(custInfoList);
        // 受益人列表
        policy.setBeneficiaryList(bnfcryInfoList);
        // 代理人
        policy.setAgent(agent);
        // 险种列表
        policy.setRiskList(insurTypeList);
/*		// 告知信息列表
		policy.setNotificationInfoList(discloseList);*/
        //提取投保人告知数据
        policy.setApplicantNotify(applicantNotify);
        //提取被保人告知数据
        policy.setInsuredNotifyList(insuredNotifyList);
        //提取代理人告知数据
        policy.setAgentNotify(agentNotify);
        // 提取保单数据
        long start = System.currentTimeMillis();
        logger.debug("累计风险保额提取policy-begin");
        long beginTime = System.currentTimeMillis();

        mExtractPolicy.getPolicy(policy,tradeInfo);
        if(tradeInfo.hasError()){
            logger.debug("累计风险保额提取保单数据失败！");
            //tradeInfo.addError("累计风险保额提取保单数据失败！");
            return policy;
        }
        logger.debug("累计风险保额提取保单数据成功，提数：" + (System.currentTimeMillis() - beginTime)/1000.0 + "s");

        logger.debug("累计风险保额提取appnt-begin");
        // 提取投保人数据
        beginTime = System.currentTimeMillis();
        mNewExtractApplicantInfo.getApplicantInfo(policy,tradeInfo);
        if(tradeInfo.hasError()){
            logger.debug("累计风险保额提取投保人数据失败！");
            //tradeInfo.addError("累计风险保额提取投保人数据失败！");
            return policy;
        }
        logger.debug("累计风险保额提取投保人数据成功，提数：" + (System.currentTimeMillis() - beginTime)/1000.0 + "s");

        logger.debug("累计风险保额提取insue-begin");
        // 提取被保人数据
        beginTime = System.currentTimeMillis();
        mNewExtractInsuredInfo.getInsuredInfo(policy,tradeInfo);
        if(tradeInfo.hasError()){
            logger.debug("累计风险保额提取被保人数据失败！");
           // tradeInfo.addError("累计风险保额提取被保人数据失败！");
            return policy;
        }
        logger.debug("累计风险保额提取被保人数据成功，提数：" + (System.currentTimeMillis() - beginTime)/1000.0 + "s");

        // 提取受益人数据
        // 提取受益人数据--电子银行默认法定受益人，无需提数
        String sellType =policy.getSellType();
        logger.debug("累计风险保额提取bnf-begin");
        beginTime = System.currentTimeMillis();
        if(!"22".equals(sellType)&& !"24".equals(sellType)&&
                !"25".equals(sellType)&& !"26".equals(sellType)&&
                !"27".equals(sellType)){
            mExtractBnfcryInfo.getBnfcryInfoList(policy,tradeInfo);
        }
        if(tradeInfo.hasError()){
            logger.debug("累计风险保额提取受益人数据失败！");
            tradeInfo.addError("累计风险保额提取受益人数据失败！");
            return policy;
        }
        logger.debug("累计风险保额提取受益人数据成功，提数：" + (System.currentTimeMillis() - beginTime)/1000.0 + "s");

        // 提取客户数据
        logger.debug("累计风险保额提取cust-begin");
        beginTime = System.currentTimeMillis();
        mExtractCustInfo.getCustInfo(policy,tradeInfo);
        if(tradeInfo.hasError()){
            tradeInfo.addError("累计风险保额提取客户数据失败！");
            logger.debug("累计风险保额提取客户数据失败！");
            return policy;
        }
        logger.debug("累计风险保额提取客户数据成功，提数：" + (System.currentTimeMillis() - beginTime)/1000.0 + "s");

        // 提取代理人数据
        logger.debug("累计风险保额提取agent-begin");
        beginTime = System.currentTimeMillis();
        mExtractAgent.getAgent(policy,tradeInfo);
        if(tradeInfo.hasError()){
            logger.debug("累计风险保额提取代理人数据失败！");
            tradeInfo.addError("累计风险保额提取代理人数据失败！");
            return policy;
        }
        logger.debug("累计风险保额提取代理人数据成功，提数：" + (System.currentTimeMillis() - beginTime)/1000.0 + "s");

        // 提取险种数据
        logger.debug("累计风险保额提取insurtype-begin");
        beginTime = System.currentTimeMillis();
        mNewExtractInsurType.getInsurTypeList(policy,tradeInfo);
        if(tradeInfo.hasError()){
            logger.debug("累计风险保额提取险种数据失败！");
            //tradeInfo.addError("累计风险保额提取险种数据失败！");
            return policy;
        }
        logger.debug("累计风险保额提取险种数据成功，提数：" + (System.currentTimeMillis() - beginTime)/1000.0 + "s");

        // 提取累计数据
        logger.debug("累计风险保额提取accum-begin");
        beginTime = System.currentTimeMillis();
        mNewExtractAccum.getAccum(policy,tradeInfo);
        if(tradeInfo.hasError()){
            logger.debug("累计风险保额提取累计数据失败！");
            //tradeInfo.addError("累计风险保额提取累计数据失败！");
            return policy;
        }
        logger.debug("累计风险保额提取累计数据成功，提数：" + (System.currentTimeMillis() - beginTime)/1000.0 + "s");

        // 提取既往数据
        logger.debug("累计风险保额提取past-begin");
        beginTime = System.currentTimeMillis();
        mNewExtractPast.getPast(tradeInfo,policy);
        if(tradeInfo.hasError()){
            logger.debug("累计风险保额提取既往数据失败！");
           // tradeInfo.addError("累计风险保额提取既往数据失败！");
            return policy;
        }
        logger.debug("累计风险保额提取既往数据成功，提数：" + (System.currentTimeMillis() - beginTime)/1000.0 + "s");

        // 提取查重数据
        logger.debug("累计风险保额提取check-begin");
        beginTime = System.currentTimeMillis();
        newExtractCheck.getCheck(tradeInfo,policy);
        if(tradeInfo.hasError()){
            logger.debug("累计风险保额提取查重数据失败！");
           // tradeInfo.addError("累计风险保额提取查重数据失败！");
            return policy;
        }
        logger.debug("累计风险保额提取查重数据成功，提数：" + (System.currentTimeMillis() - beginTime)/1000.0 + "s");

        // 提取告知数据
        logger.debug("累计风险保额提取disclose-info");
        beginTime = System.currentTimeMillis();
		/*mExtractDisclose.getDisclose(policy,tradeInfo);
		if(tradeInfo.hasError()){
			logger.debug("提取告知数据失败！！");
			tradeInfo.addError("提取告知数据失败！");
			return policy;
		}*/
        mExtractNotifyApplicant.getApplicantNotify(policy,tradeInfo);
        mExtractNotifyInsured.getInsuredNotify(policy,tradeInfo);
        mExtractNotifyAgent.getAgentNotify(policy,tradeInfo);
        logger.debug("累计风险保额提取告知数据成功，提数：" + (System.currentTimeMillis() - beginTime)/1000.0 + "s");

        logger.debug(policy.getContNo() + "，RulesEngine累计风险保额提数总耗时："+(System.currentTimeMillis() - start)/1000.0+"s");
        return policy;
    }
}
