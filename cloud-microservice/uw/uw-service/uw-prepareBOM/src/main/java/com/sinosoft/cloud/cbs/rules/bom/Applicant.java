package com.sinosoft.cloud.cbs.rules.bom;

import java.util.Date;
import java.util.List;

/**
 * 投保人信息
 * 
 * @author dingfan
 * 
 */
public class Applicant {
	/**
	 * 姓名
	 */
	private String applicantName;

	/**
	 * 证件类型
	 */
	private String identityType;

	/**
	 * 证件号码
	 */
	private String identityCode;

	/**
	 * 性别
	 */
	private String sex;

	/**
	 * 身高
	 */
	private double height;

	/**
	 * 体重
	 */
	private double weight;

	/**
	 * 出生日期
	 */
	private Date birthday;

	/**
	 * 年龄
	 */
	private int age;

	/**
	 * VIP标记
	 */
	private boolean vipSign;

	/**
	 * 证件有效期
	 */
	private String identityValidityTerm;

	/**
	 * 国籍
	 */
	private String nationality;

	/**
	 * 户籍
	 */
	private String householdRegister;

	/**
	 * 学历
	 */
	private String education;

	/**
	 * 婚姻状况
	 */
	private String marrageStatus;

	/**
	 * 与被保人关系
	 */
	private String relationWithInsured;

	/**
	 * 职业代码
	 */
	private String occuCode;

	/**
	 * 职业类别
	 */
	private String occuType;

	/**
	 * 职业描述
	 */
	private String occuDescribe;

	/**
	 * 兼职
	 */
	private String partTimeJobFlag;

	/**
	 * 是否有摩托车驾照
	 */
	private boolean motorLicence;

	/**
	 * 驾照类型
	 */
	private String licenceType;

	/**
	 * 地址代码
	 */
	private String addrCode;

	/**
	 * 首期支付方式
	 */
	private String firstPayMethod;

	/**
	 * 续期交费提示
	 */
	private String afterPayPrompt;

	/**
	 * 首期转账开户行
	 */
	private String firstPayBank;

	/**
	 * 首期账户姓名
	 */
	private String firstPayName;

	/**
	 * 首期账号
	 */
	private String firstPayBankNo;

	/**
	 * 续期保险费支付方式
	 */
	private String afterPayMethod;

	/**
	 * 首续支付方式一致
	 */
	private boolean samePayMethod;

	/**
	 * 续期转账开户行
	 */
	private String afterPayBank;

	/**
	 * 续期账户姓名
	 */
	private String afterPayName;

	/**
	 * 续期账号
	 */
	private String afterPayBankNo;

	/**
	 * 年收入
	 */
	private double annualIncome;

	/**
	 * 家庭年收入
	 */
	private double familyAnnualIncome;

	/**
	 * 主要收入来源
	 */
	private String incomeSource;

	/**
	 * 黑名单标记
	 */
	private boolean blackSign;

	/**
	 * BMI
	 */
	private String BMI;

	/**
	 * 累计期交保费
	 */
	private double sumPeriodPremium;

	/**
	 * 累计趸交保费
	 */
	private double sumSinglePremium;

	/**
	 * 累计住院医疗风险保额
	 */
	private double sumHospitalizeAmount;

	/**
	 * 累计意外医疗风险保额
	 */
	private double sumAccMedicalAmount;

	/**
	 * 累计津贴保额
	 */
	private double sumAcAmount;

	/**
	 * 累计寿险风险保额
	 */
	private double sumLifeAmount;

	/**
	 * 寿险风险保额
	 */
	private double lifeAmount;

	/**
	 * 累计意外险风险保额
	 */
	private double sumAccidentAmount;

	/**
	 * 意外险风险保额
	 */
	private double accidentAmount;

	/**
	 * 累计重疾风险保额
	 */
	private double sumDiseaseAmount;

	/**
	 * 重疾险风险保额
	 */
	private double diseaseAmount;

	/**
	 * 累计自驾车风险保额
	 */
	private double sumDriverAmount;

	/**
	 * 累计防癌险风险保额
	 */
	private double sumAnticancerAmount;

	/**
	 * 累计公共交通意外风险保额
	 */
	private double sumBusAccAmount;

	/**
	 * 公共交通意外风险保额
	 */
	private double busAccAmount;

	/**
	 * 累计航空意外风险保额
	 */
	private double sumAviationAmount;

	/**
	 * 航空意外风险保额
	 */
	private double aviationAmount;

	/**
	 * 累计人身险保额
	 */
	private double sumPersonAmount;

	/**
	 * 累计风险保额
	 */
	private double sumAmount;

	/**
	 * 累计寿险保单保额
	 */
	private double sumLifePyAmount;

	/**
	 * 寿险保单保额
	 */
	private double lifePyAmount;

	/**
	 * 累计健康险保单保额
	 */
	private double sumHealthPyAmount;

	/**
	 * 累计意外险保单保额
	 */
	private double sumAccPyAmount;

	/**
	 * 意外险保单保额
	 */
	private double accPyAmount;

	/**
	 * 累计重疾险保单保额
	 */
	private double sumDisPyAmount;

	/**
	 * 累计自驾车保单保额
	 */
	private double sumDriverPyAmount;

	/**
	 * 累计防癌险保单保额
	 */
	private double sumAnticPyAmount;

	/**
	 * 未成年人身故保额
	 */
	private double nonageDieAmount;

	/**
	 * 公共交通意外保单保额
	 */
	private double busAccPyAmount;

	/**
	 * 航空意外保单保额
	 */
	private double aviationPyAmount;

	/**
	 * 健康告知结论
	 */
	private String healthNotify;

	/**
	 *既往有体检记录
	 */
	//private boolean haveCheckNote;

	/**
	 * 既往有体检、加费、特约的保单
	 */
	//private boolean haveExaminePolicy;

	/**
	 * 既往有理赔记录
	 */
	//private boolean haveClaims;

	/**
	 * 既往有保全二核
	 */
	//private boolean havePreserve;

	/**
	 * 既往有理赔二核
	 */
	//private boolean havaPayFor;

	/**
	 * 既往有生调记录
	 */
	//private boolean haveAscertain;

	/**
	 * 既往有投保经历
	 */
	//private boolean haveApptHistory;

	/**
	 * 既往有次标准体记录
	 */
	//private boolean haveSubExamine;

	/**
	 * 既往有体检未完成记录
	 */
	//private boolean haveUnfiExamine;

	/**
	 * 既往告知为五、六类或拒保职业
	 */
	//private boolean haveRejectionOcc;

	/**
	 * 职业类别低于既往告知
	 */
	private boolean appntJobTypeThaBefo;

	/**
	 * 居民类型
	 */
	private String residentType;

	/**
	 * 保费预算
	 */
	private double premiumBudget;

	/**
	 * 职务
	 */
	private String duties;

	/**
	 * 美国纳税人识别号(TIN)
	 */
	private boolean tinFlag;

	/**
	 * 投保人客户号
	 */
	private String clientNo;

	/**
	 * 电销中心
	 */
	private String electricPinCenter;

	/**
	 * 风险测评结果
	 */
	private String riskAssessment;

	/**
	 * 城镇居民人均可支配收入
	 */
	private double perDisposIncome;

	/**
	 * 农村居民人均纯收入
	 */
	private double perIncome;

	/**
	 * 历史险种列表
	 */
	private List historyRiskList;

	/**
	 * 客户级别
	 */
	private String clientLevel;

	/**
	 * 是否为孕妇
	 */
	private boolean pregSign;

	/**
	 * 是否为残疾人
	 */
	/*private boolean disabledSign;*/

	/**
	 * 孕周
	 */
	private int pregWeek;

	/**
	 * 累计寿险体检风险保额
	 */
	private double sumLifePhyAmount;

	/**
	 * 累计再保风险保额
	 */
	private double sumReinsuranceAmount;

	/**
	 * 可疑电话
	 */
	private boolean questionTel;

	/**
	 * 投保人不同电话相同
	 */
	private boolean appntPhoneDiff;

	/**
	 * 3个（含3）以上不同投保人存在相同电话
	 */
	private boolean moreAppntTelDiff;

	/**
	 * 3个（含3）以上客户联系电话和首选回访电话中有相同号码
	 */
	private boolean moreAppntTelSame;

	/**
	 * 投保人不同联系电话相同
	 */
	private boolean appntTelDiff;

	/**
	 * 投保人非代理人但联系电话相同
	 */
	private boolean appntAgentDiff;

	/**
	 * 年交保费
	 */
	private double premiumPerYear;

	/**
	 * 期交保费
	 */
	private double periodPremium;

	/**
	 * 趸交保费
	 */
	private double singlePremium;

	/**
	 * 身高异常
	 */
	private boolean heightErr;

	/**
	 * 体重异常
	 */
	private boolean weightErr;

	/**
	 * BMI异常
	 */
	private boolean bmiErr;

	/**
	 * 投保人告知
	 */
	private boolean appNotify;

	/**
	 * 银保累计期交保费
	 */
	private double ybPrem;

	/**
	 * 
	 * 特殊保费1
	 */
	private double specialPrem1;

	/**
	 * 预留字段1
	 */
	private double applicantDouble1;

	/**
	 * 预留字段2
	 */
	private double applicantDouble2;

	/**
	 * 预留字段3
	 */
	private String applicantString1;

	/**
	 * 预留字段4
	 */
	private String applicantString2;


	/**
	 * 黑名单1-证件号与和黑名单相同,不分国籍,>0触发,参数:一个contno
	 */
	private boolean appBlackIdFlag;
	
	
	/**
	 * 黑名单2-中国客户名字、生日与黑名单相同,>0触发,参数:一个contno
	 */
	private boolean appBlackChnFlag;
	
	
	/**
	 * 黑名单3-投保人名1部分组成,投保人名与黑名单表四个名字对比,>0触发,参数:4个contno
	 */
	private boolean appBlackFlag1;
	
	
	/**
	 * 黑名单4-投保人名2部分组成,投保人名与黑名单表2个名字对比,>0触发,参数:2个contno
	 * 有一个name相同，且出生日期相同
	 */
	private boolean appBlackFlag2;
	
	/**
	 * 黑名单5-投保人名2部分组成,投保人名与黑名单表2个名字对比,>=2触发,参数:2个contno
	 * firstname和surnname都要相同.无生日限制
	 */
	private boolean appBlackFlag3;
	
	/**
	 * 黑名单6-投保人名4部分组成,投保人名与黑名单表3个名字对比,>=0触发,参数:3个contno
	 * firstname,middlename,surname,有生日限制
	 */
	private boolean appBlackFlag4;
	
	/**
	 * 黑名单7-投保人名4部分组成,投保人名与黑名单表3个名字对比,>=2触发,参数:3个contno
	 * firstname,middlename,surname,无生日限制
	 */
	private boolean appBlackFlag5;
	
	
//	被保人7053险种续保保费之和
	private double renewalPremSum7053;

	/**
	 * 中保信上一年度理赔额  --wpq--
	 */
	private double yearClaimLimit;

	/**
	 * 	投保人白名单标记
	 */
	private boolean whiteSign;


	public boolean isWhiteSign() {
		return whiteSign;
	}

	public void setWhiteSign(boolean whiteSign) {
		this.whiteSign = whiteSign;
	}

	public double getYearClaimLimit() {
		return yearClaimLimit;
	}

	public void setYearClaimLimit(double yearClaimLimit) {
		this.yearClaimLimit = yearClaimLimit;
	}

	public double getRenewalPremSum7053() {
		return renewalPremSum7053;
	}

	public void setRenewalPremSum7053(double renewalPremSum7053) {
		this.renewalPremSum7053 = renewalPremSum7053;
	}

	public boolean isAppBlackChnFlag() {
		return appBlackChnFlag;
	}

	public void setAppBlackChnFlag(boolean appBlackChnFlag) {
		this.appBlackChnFlag = appBlackChnFlag;
	}

	public boolean isAppBlackFlag1() {
		return appBlackFlag1;
	}

	public void setAppBlackFlag1(boolean appBlackFlag1) {
		this.appBlackFlag1 = appBlackFlag1;
	}

	public boolean isAppBlackFlag2() {
		return appBlackFlag2;
	}

	public void setAppBlackFlag2(boolean appBlackFlag2) {
		this.appBlackFlag2 = appBlackFlag2;
	}

	public boolean isAppBlackFlag3() {
		return appBlackFlag3;
	}

	public void setAppBlackFlag3(boolean appBlackFlag3) {
		this.appBlackFlag3 = appBlackFlag3;
	}

	public boolean isAppBlackFlag4() {
		return appBlackFlag4;
	}

	public void setAppBlackFlag4(boolean appBlackFlag4) {
		this.appBlackFlag4 = appBlackFlag4;
	}

	public boolean isAppBlackFlag5() {
		return appBlackFlag5;
	}

	public void setAppBlackFlag5(boolean appBlackFlag5) {
		this.appBlackFlag5 = appBlackFlag5;
	}

	public boolean isAppBlackIdFlag() {
		return appBlackIdFlag;
	}

	public void setAppBlackIdFlag(boolean appBlackIdFlag) {
		this.appBlackIdFlag = appBlackIdFlag;
	}

	public String getApplicantName() {
		return applicantName;
	}

	public void setApplicantName(String applicantName) {
		this.applicantName = applicantName;
	}

	public String getIdentityType() {
		return identityType;
	}

	public void setIdentityType(String identityType) {
		this.identityType = identityType;
	}

	public String getIdentityCode() {
		return identityCode;
	}

	public void setIdentityCode(String identityCode) {
		this.identityCode = identityCode;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public double getHeight() {
		return height;
	}

	public void setHeight(double height) {
		this.height = height;
	}

	public double getWeight() {
		return weight;
	}

	public void setWeight(double weight) {
		this.weight = weight;
	}

	public Date getBirthday() {
		return birthday;
	}

	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public boolean isVipSign() {
		return vipSign;
	}

	public void setVipSign(boolean vipSign) {
		this.vipSign = vipSign;
	}

	public String getIdentityValidityTerm() {
		return identityValidityTerm;
	}

	public void setIdentityValidityTerm(String identityValidityTerm) {
		this.identityValidityTerm = identityValidityTerm;
	}

	public String getNationality() {
		return nationality;
	}

	public void setNationality(String nationality) {
		this.nationality = nationality;
	}

	public String getHouseholdRegister() {
		return householdRegister;
	}

	public void setHouseholdRegister(String householdRegister) {
		this.householdRegister = householdRegister;
	}

	public String getEducation() {
		return education;
	}

	public void setEducation(String education) {
		this.education = education;
	}

	public String getMarrageStatus() {
		return marrageStatus;
	}

	public void setMarrageStatus(String marrageStatus) {
		this.marrageStatus = marrageStatus;
	}

	public String getRelationWithInsured() {
		return relationWithInsured;
	}

	public void setRelationWithInsured(String relationWithInsured) {
		this.relationWithInsured = relationWithInsured;
	}

	public String getOccuCode() {
		return occuCode;
	}

	public void setOccuCode(String occuCode) {
		this.occuCode = occuCode;
	}

	public String getOccuType() {
		return occuType;
	}

	public void setOccuType(String occuType) {
		this.occuType = occuType;
	}

	public String getOccuDescribe() {
		return occuDescribe;
	}

	public void setOccuDescribe(String occuDescribe) {
		this.occuDescribe = occuDescribe;
	}

	public String getPartTimeJobFlag() {
		return partTimeJobFlag;
	}

	public void setPartTimeJobFlag(String partTimeJobFlag) {
		this.partTimeJobFlag = partTimeJobFlag;
	}

	public boolean isMotorLicence() {
		return motorLicence;
	}

	public void setMotorLicence(boolean motorLicence) {
		this.motorLicence = motorLicence;
	}

	public String getLicenceType() {
		return licenceType;
	}

	public void setLicenceType(String licenceType) {
		this.licenceType = licenceType;
	}

	public String getAddrCode() {
		return addrCode;
	}

	public void setAddrCode(String addrCode) {
		this.addrCode = addrCode;
	}

	public String getFirstPayMethod() {
		return firstPayMethod;
	}

	public void setFirstPayMethod(String firstPayMethod) {
		this.firstPayMethod = firstPayMethod;
	}

	public String getAfterPayPrompt() {
		return afterPayPrompt;
	}

	public void setAfterPayPrompt(String afterPayPrompt) {
		this.afterPayPrompt = afterPayPrompt;
	}

	public String getFirstPayBank() {
		return firstPayBank;
	}

	public void setFirstPayBank(String firstPayBank) {
		this.firstPayBank = firstPayBank;
	}

	public String getFirstPayName() {
		return firstPayName;
	}

	public void setFirstPayName(String firstPayName) {
		this.firstPayName = firstPayName;
	}

	public String getFirstPayBankNo() {
		return firstPayBankNo;
	}

	public void setFirstPayBankNo(String firstPayBankNo) {
		this.firstPayBankNo = firstPayBankNo;
	}

	public String getAfterPayMethod() {
		return afterPayMethod;
	}

	public void setAfterPayMethod(String afterPayMethod) {
		this.afterPayMethod = afterPayMethod;
	}

	public boolean isSamePayMethod() {
		return samePayMethod;
	}

	public void setSamePayMethod(boolean samePayMethod) {
		this.samePayMethod = samePayMethod;
	}

	public String getAfterPayBank() {
		return afterPayBank;
	}

	public void setAfterPayBank(String afterPayBank) {
		this.afterPayBank = afterPayBank;
	}

	public String getAfterPayName() {
		return afterPayName;
	}

	public void setAfterPayName(String afterPayName) {
		this.afterPayName = afterPayName;
	}

	public String getAfterPayBankNo() {
		return afterPayBankNo;
	}

	public void setAfterPayBankNo(String afterPayBankNo) {
		this.afterPayBankNo = afterPayBankNo;
	}

	public double getAnnualIncome() {
		return annualIncome;
	}

	public void setAnnualIncome(double annualIncome) {
		this.annualIncome = annualIncome;
	}

	public double getFamilyAnnualIncome() {
		return familyAnnualIncome;
	}

	public void setFamilyAnnualIncome(double familyAnnualIncome) {
		this.familyAnnualIncome = familyAnnualIncome;
	}

	public String getIncomeSource() {
		return incomeSource;
	}

	public void setIncomeSource(String incomeSource) {
		this.incomeSource = incomeSource;
	}

	public boolean isBlackSign() {
		return blackSign;
	}

	public void setBlackSign(boolean blackSign) {
		this.blackSign = blackSign;
	}

	public String getBMI() {
		return BMI;
	}

	public void setBMI(String bMI) {
		BMI = bMI;
	}

	public double getSumPeriodPremium() {
		return sumPeriodPremium;
	}

	public void setSumPeriodPremium(double sumPeriodPremium) {
		this.sumPeriodPremium = sumPeriodPremium;
	}

	public double getSumSinglePremium() {
		return sumSinglePremium;
	}

	public void setSumSinglePremium(double sumSinglePremium) {
		this.sumSinglePremium = sumSinglePremium;
	}

	public double getSumHospitalizeAmount() {
		return sumHospitalizeAmount;
	}

	public void setSumHospitalizeAmount(double sumHospitalizeAmount) {
		this.sumHospitalizeAmount = sumHospitalizeAmount;
	}

	public double getSumAccMedicalAmount() {
		return sumAccMedicalAmount;
	}

	public void setSumAccMedicalAmount(double sumAccMedicalAmount) {
		this.sumAccMedicalAmount = sumAccMedicalAmount;
	}

	public double getSumAcAmount() {
		return sumAcAmount;
	}

	public void setSumAcAmount(double sumAcAmount) {
		this.sumAcAmount = sumAcAmount;
	}

	public double getSumLifeAmount() {
		return sumLifeAmount;
	}

	public void setSumLifeAmount(double sumLifeAmount) {
		this.sumLifeAmount = sumLifeAmount;
	}

	public double getLifeAmount() {
		return lifeAmount;
	}

	public void setLifeAmount(double lifeAmount) {
		this.lifeAmount = lifeAmount;
	}

	public double getSumAccidentAmount() {
		return sumAccidentAmount;
	}

	public void setSumAccidentAmount(double sumAccidentAmount) {
		this.sumAccidentAmount = sumAccidentAmount;
	}

	public double getAccidentAmount() {
		return accidentAmount;
	}

	public void setAccidentAmount(double accidentAmount) {
		this.accidentAmount = accidentAmount;
	}

	public double getSumDiseaseAmount() {
		return sumDiseaseAmount;
	}

	public void setSumDiseaseAmount(double sumDiseaseAmount) {
		this.sumDiseaseAmount = sumDiseaseAmount;
	}

	public double getDiseaseAmount() {
		return diseaseAmount;
	}

	public void setDiseaseAmount(double diseaseAmount) {
		this.diseaseAmount = diseaseAmount;
	}

	public double getSumDriverAmount() {
		return sumDriverAmount;
	}

	public void setSumDriverAmount(double sumDriverAmount) {
		this.sumDriverAmount = sumDriverAmount;
	}

	public double getSumAnticancerAmount() {
		return sumAnticancerAmount;
	}

	public void setSumAnticancerAmount(double sumAnticancerAmount) {
		this.sumAnticancerAmount = sumAnticancerAmount;
	}

	public double getSumBusAccAmount() {
		return sumBusAccAmount;
	}

	public void setSumBusAccAmount(double sumBusAccAmount) {
		this.sumBusAccAmount = sumBusAccAmount;
	}

	public double getBusAccAmount() {
		return busAccAmount;
	}

	public void setBusAccAmount(double busAccAmount) {
		this.busAccAmount = busAccAmount;
	}

	public double getSumAviationAmount() {
		return sumAviationAmount;
	}

	public void setSumAviationAmount(double sumAviationAmount) {
		this.sumAviationAmount = sumAviationAmount;
	}

	public double getAviationAmount() {
		return aviationAmount;
	}

	public void setAviationAmount(double aviationAmount) {
		this.aviationAmount = aviationAmount;
	}

	public double getSumAmount() {
		return sumAmount;
	}

	public void setSumAmount(double sumAmount) {
		this.sumAmount = sumAmount;
	}

	public double getSumLifePyAmount() {
		return sumLifePyAmount;
	}

	public void setSumLifePyAmount(double sumLifePyAmount) {
		this.sumLifePyAmount = sumLifePyAmount;
	}

	public double getLifePyAmount() {
		return lifePyAmount;
	}

	public void setLifePyAmount(double lifePyAmount) {
		this.lifePyAmount = lifePyAmount;
	}

	public double getSumHealthPyAmount() {
		return sumHealthPyAmount;
	}

	public void setSumHealthPyAmount(double sumHealthPyAmount) {
		this.sumHealthPyAmount = sumHealthPyAmount;
	}

	public double getSumAccPyAmount() {
		return sumAccPyAmount;
	}

	public void setSumAccPyAmount(double sumAccPyAmount) {
		this.sumAccPyAmount = sumAccPyAmount;
	}

	public double getAccPyAmount() {
		return accPyAmount;
	}

	public void setAccPyAmount(double accPyAmount) {
		this.accPyAmount = accPyAmount;
	}

	public double getSumDisPyAmount() {
		return sumDisPyAmount;
	}

	public void setSumDisPyAmount(double sumDisPyAmount) {
		this.sumDisPyAmount = sumDisPyAmount;
	}

	public double getSumDriverPyAmount() {
		return sumDriverPyAmount;
	}

	public void setSumDriverPyAmount(double sumDriverPyAmount) {
		this.sumDriverPyAmount = sumDriverPyAmount;
	}

	public double getSumAnticPyAmount() {
		return sumAnticPyAmount;
	}

	public void setSumAnticPyAmount(double sumAnticPyAmount) {
		this.sumAnticPyAmount = sumAnticPyAmount;
	}

	public double getNonageDieAmount() {
		return nonageDieAmount;
	}

	public void setNonageDieAmount(double nonageDieAmount) {
		this.nonageDieAmount = nonageDieAmount;
	}

	public double getBusAccPyAmount() {
		return busAccPyAmount;
	}

	public void setBusAccPyAmount(double busAccPyAmount) {
		this.busAccPyAmount = busAccPyAmount;
	}

	public double getAviationPyAmount() {
		return aviationPyAmount;
	}

	public void setAviationPyAmount(double aviationPyAmount) {
		this.aviationPyAmount = aviationPyAmount;
	}

	public String getHealthNotify() {
		return healthNotify;
	}

	public void setHealthNotify(String healthNotify) {
		this.healthNotify = healthNotify;
	}

/*
	public boolean isHaveCheckNote() {
		return haveCheckNote;
	}

	public void setHaveCheckNote(boolean haveCheckNote) {
		this.haveCheckNote = haveCheckNote;
	}
*/

/*
	public boolean isHaveExaminePolicy() {
		return haveExaminePolicy;
	}

	public void setHaveExaminePolicy(boolean haveExaminePolicy) {
		this.haveExaminePolicy = haveExaminePolicy;
	}
*/

/*
	public boolean isHaveClaims() {
		return haveClaims;
	}

	public void setHaveClaims(boolean haveClaims) {
		this.haveClaims = haveClaims;
	}
*/

	/*public boolean isHavePreserve() {
		return havePreserve;
	}

	public void setHavePreserve(boolean havePreserve) {
		this.havePreserve = havePreserve;
	}*/

/*
	public boolean isHavaPayFor() {
		return havaPayFor;
	}

	public void setHavaPayFor(boolean havaPayFor) {
		this.havaPayFor = havaPayFor;
	}
*/

/*
	public boolean isHaveAscertain() {
		return haveAscertain;
	}

	public void setHaveAscertain(boolean haveAscertain) {
		this.haveAscertain = haveAscertain;
	}
*/

	public String getResidentType() {
		return residentType;
	}

	public void setResidentType(String residentType) {
		this.residentType = residentType;
	}

	public double getPremiumBudget() {
		return premiumBudget;
	}

	public void setPremiumBudget(double premiumBudget) {
		this.premiumBudget = premiumBudget;
	}

	public String getDuties() {
		return duties;
	}

	public void setDuties(String duties) {
		this.duties = duties;
	}

	public boolean isTinFlag() {
		return tinFlag;
	}

	public void setTinFlag(boolean tinFlag) {
		this.tinFlag = tinFlag;
	}

	public String getClientNo() {
		return clientNo;
	}

	public void setClientNo(String clientNo) {
		this.clientNo = clientNo;
	}

	public String getElectricPinCenter() {
		return electricPinCenter;
	}

	public void setElectricPinCenter(String electricPinCenter) {
		this.electricPinCenter = electricPinCenter;
	}

	public String getRiskAssessment() {
		return riskAssessment;
	}

	public void setRiskAssessment(String riskAssessment) {
		this.riskAssessment = riskAssessment;
	}

	public double getPerDisposIncome() {
		return perDisposIncome;
	}

	public void setPerDisposIncome(double perDisposIncome) {
		this.perDisposIncome = perDisposIncome;
	}

	public double getPerIncome() {
		return perIncome;
	}

	public void setPerIncome(double perIncome) {
		this.perIncome = perIncome;
	}

	public List getHistoryRiskList() {
		return historyRiskList;
	}

	public void setHistoryRiskList(List historyRiskList) {
		this.historyRiskList = historyRiskList;
	}

	public String getClientLevel() {
		return clientLevel;
	}

	public void setClientLevel(String clientLevel) {
		this.clientLevel = clientLevel;
	}

/*	public boolean isDisabledSign() {
		return disabledSign;
	}

	public void setDisabledSign(boolean disabledSign) {
		this.disabledSign = disabledSign;
	}*/

	public boolean isPregSign() {
		return pregSign;
	}

	public void setPregSign(boolean pregSign) {
		this.pregSign = pregSign;
	}

	public int getPregWeek() {
		return pregWeek;
	}

	public void setPregWeek(int pregWeek) {
		this.pregWeek = pregWeek;
	}

	public double getSumLifePhyAmount() {
		return sumLifePhyAmount;
	}

	public void setSumLifePhyAmount(double sumLifePhyAmount) {
		this.sumLifePhyAmount = sumLifePhyAmount;
	}

	public double getSumReinsuranceAmount() {
		return sumReinsuranceAmount;
	}

	public void setSumReinsuranceAmount(double sumReinsuranceAmount) {
		this.sumReinsuranceAmount = sumReinsuranceAmount;
	}

	public boolean isAppntAgentDiff() {
		return appntAgentDiff;
	}

	public void setAppntAgentDiff(boolean appntAgentDiff) {
		this.appntAgentDiff = appntAgentDiff;
	}

	public boolean isAppntJobTypeThaBefo() {
		return appntJobTypeThaBefo;
	}

	public void setAppntJobTypeThaBefo(boolean appntJobTypeThaBefo) {
		this.appntJobTypeThaBefo = appntJobTypeThaBefo;
	}

	public boolean isAppntPhoneDiff() {
		return appntPhoneDiff;
	}

	public void setAppntPhoneDiff(boolean appntPhoneDiff) {
		this.appntPhoneDiff = appntPhoneDiff;
	}

	public boolean isMoreAppntTelDiff() {
		return moreAppntTelDiff;
	}

	public void setMoreAppntTelDiff(boolean moreAppntTelDiff) {
		this.moreAppntTelDiff = moreAppntTelDiff;
	}

	public boolean isMoreAppntTelSame() {
		return moreAppntTelSame;
	}

	public void setMoreAppntTelSame(boolean moreAppntTelSame) {
		this.moreAppntTelSame = moreAppntTelSame;
	}

	public boolean isQuestionTel() {
		return questionTel;
	}

	public void setQuestionTel(boolean questionTel) {
		this.questionTel = questionTel;
	}

	public boolean isAppntTelDiff() {
		return appntTelDiff;
	}

	public void setAppntTelDiff(boolean appntTelDiff) {
		this.appntTelDiff = appntTelDiff;
	}

	public boolean isBmiErr() {
		return bmiErr;
	}

	public void setBmiErr(boolean bmiErr) {
		this.bmiErr = bmiErr;
	}

/*
	public boolean isHaveApptHistory() {
		return haveApptHistory;
	}

	public void setHaveApptHistory(boolean haveApptHistory) {
		this.haveApptHistory = haveApptHistory;
	}
*/

/*
	public boolean isHaveRejectionOcc() {
		return haveRejectionOcc;
	}

	public void setHaveRejectionOcc(boolean haveRejectionOcc) {
		this.haveRejectionOcc = haveRejectionOcc;
	}
*/

	/*public boolean isHaveSubExamine() {
		return haveSubExamine;
	}

	public void setHaveSubExamine(boolean haveSubExamine) {
		this.haveSubExamine = haveSubExamine;
	}
*/
	/*public boolean isHaveUnfiExamine() {
		return haveUnfiExamine;
	}

	public void setHaveUnfiExamine(boolean haveUnfiExamine) {
		this.haveUnfiExamine = haveUnfiExamine;
	}
*/
	public boolean isHeightErr() {
		return heightErr;
	}

	public void setHeightErr(boolean heightErr) {
		this.heightErr = heightErr;
	}

	public double getPeriodPremium() {
		return periodPremium;
	}

	public void setPeriodPremium(double periodPremium) {
		this.periodPremium = periodPremium;
	}

	public double getPremiumPerYear() {
		return premiumPerYear;
	}

	public void setPremiumPerYear(double premiumPerYear) {
		this.premiumPerYear = premiumPerYear;
	}

	public double getSinglePremium() {
		return singlePremium;
	}

	public void setSinglePremium(double singlePremium) {
		this.singlePremium = singlePremium;
	}

	public double getSumPersonAmount() {
		return sumPersonAmount;
	}

	public void setSumPersonAmount(double sumPersonAmount) {
		this.sumPersonAmount = sumPersonAmount;
	}

	public boolean isWeightErr() {
		return weightErr;
	}

	public void setWeightErr(boolean weightErr) {
		this.weightErr = weightErr;
	}

	public boolean isAppNotify() {
		return appNotify;
	}

	public void setAppNotify(boolean appNotify) {
		this.appNotify = appNotify;
	}

	public double getYbPrem() {
		return ybPrem;
	}

	public void setYbPrem(double ybPrem) {
		this.ybPrem = ybPrem;
	}

	public double getSpecialPrem1() {
		return specialPrem1;
	}

	public void setSpecialPrem1(double specialPrem1) {
		this.specialPrem1 = specialPrem1;
	}

	public double getApplicantDouble1() {
		return applicantDouble1;
	}

	public void setApplicantDouble1(double applicantDouble1) {
		this.applicantDouble1 = applicantDouble1;
	}

	public double getApplicantDouble2() {
		return applicantDouble2;
	}

	public void setApplicantDouble2(double applicantDouble2) {
		this.applicantDouble2 = applicantDouble2;
	}

	public String getApplicantString1() {
		return applicantString1;
	}

	public void setApplicantString1(String applicantString1) {
		this.applicantString1 = applicantString1;
	}

	public String getApplicantString2() {
		return applicantString2;
	}

	public void setApplicantString2(String applicantString2) {
		this.applicantString2 = applicantString2;
	}
}
