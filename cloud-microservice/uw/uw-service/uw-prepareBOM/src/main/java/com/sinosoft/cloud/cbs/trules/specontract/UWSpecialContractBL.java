package com.sinosoft.cloud.cbs.trules.specontract;

import com.sinosoft.cache.dao.impl.RedisCommonDao;
import com.sinosoft.cloud.bl.AbstractBL;
import com.sinosoft.cloud.cbs.nb.bl.AipWsClientCommon;

import com.sinosoft.cloud.cbs.trules.bom.ProdGroup;
import com.sinosoft.cloud.cbs.trules.bom.Product;
import com.sinosoft.cloud.cbs.trules.bom.TInsurdInfo;
import com.sinosoft.cloud.cbs.trules.entity.*;
import com.sinosoft.cloud.cbs.trules.httpclient.SpecMesToXml;
import com.sinosoft.cloud.cbs.trules.util.ContPlanCodeEnum;
import com.sinosoft.cloud.cbs.trules.util.CredenTypeEnum;
import com.sinosoft.cloud.cbs.trules.util.DomainUtils;
import com.sinosoft.cloud.nb.service.redis.NBRedisCommon;
import com.sinosoft.cloud.rest.TradeInfo;
import com.sinosoft.lis.entity.*;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.utility.ExceptionUtils;

import com.sinosoft.utility.JdomUtil;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jdom.Document;
import org.jdom.Element;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;


/**
 * @Author:wangshuliang
 * @Description:
 * @Date:Created in 10:34 2018/6/25
 * @Modified by:
 */
@Component
public class UWSpecialContractBL {

    private final Log logger = LogFactory.getLog(this.getClass());
    @Value("${cloud.aip.uw.url}")
    private String url;
    @Value("${cloud.aip.uw.namespace}")
    private String namespace;
    @Value("${cloud.aip.uw.method}")
    private String method;
    @Value("${cloud.aip.uw.getCipRateReq}")
    private String reqType;
    @Value("${cloud.aip.uw.arg0}")
    private String arg0;
    @Value("${cloud.aip.uw.arg1}")
    private String arg1;
    @Autowired
    private  AipWsClientCommon aipWsClientCommon;
    @Autowired
    private RedisCommonDao redisCommonDao;
    @Autowired
    private NBRedisCommon nbRedisCommon;

    /**
     *  调用特殊审批信息接口
     * @param tradeInfo Map传值
     * @param ClmNo  既往理赔号码即赔案号
     * @param approveTypeCode 审批类型编码
     * @return
     */
    public Boolean dealData(TradeInfo tradeInfo,String ClmNo,String approveTypeCode)  throws Exception{
        //获取保单打印报文
        try {
            LCContPojo lcContPojo = (LCContPojo) tradeInfo.getData(LCContPojo.class.getName());
            String xml = getXML(tradeInfo,ClmNo,approveTypeCode);
            if (StringUtils.isEmpty(xml)){
                throw new Exception();
            }
            logger.debug(lcContPojo.getContNo() + "特约审批信息请求报文为" + xml);
            Map<String,Object> reqMap=new HashMap<>();
            reqMap.put(arg0,reqType);
            reqMap.put(arg1,xml);
            String res = aipWsClientCommon.sendService(url, namespace, method,reqMap);
            if (StringUtils.isEmpty(res)){
                logger.debug(lcContPojo.getContNo() + "特约审批信息返回报文为" + res);
                throw new Exception();
            }
            logger.debug(lcContPojo.getContNo() + "特约审批信息返回报文为" + res);
            Document reqDoc = JdomUtil.build(res, true);
            Element queryRes = reqDoc.getRootElement();
            Element head = queryRes.getChild("head");
            Element response = head.getChild("response");
            String businessRespCode = response.getChildText("businessRespCode");
            //没有审批有效的数据 01
            if ("01".equals(businessRespCode)) {
                logger.debug(lcContPojo.getContNo()+response.getChildText("businessRespText"));
                return false;
                //02 获取特殊审批信息成功
            } else if ("00".equals(businessRespCode)) {
                logger.debug(lcContPojo.getContNo()+"获取特约审批信息成功");
                return true;
            }
        } catch (Exception e) {
            logger.error("获取特约审批信息失败" + ExceptionUtils.exceptionToString(e));
            logger.debug(tradeInfo.toString());
            throw  e;

        }
        return false;
    }


    private String getXML(TradeInfo tradeInfo,String ClmNo,String approveTypeCode) {
        //准备报文头信息
        Head head = new Head();
        SysHeader sysHeader = new SysHeader();
        //消息日期
        sysHeader.setMsgDate(PubFun.getCurrentDate());
        //消息时间
        sysHeader.setMsgTime(PubFun.getCurrentTime());
        //服务消费方应用系统的编码
        sysHeader.setSystemCode("AIP");
        //注册至ESB上服务的编码
        sysHeader.setTransactionCode("1011");
        head.setSysHeader(sysHeader);
        //被保人信息
        List<LCInsuredPojo> lcInsuredPojos = (List<LCInsuredPojo>) tradeInfo.getData(LCInsuredPojo.class.getName());
        //准备报文体的信息
        Body body = new Body();
        QueryInfo queryInfo = new QueryInfo();
        //审批类型编码   01-延期拒保（既往核保），02-理赔，03-被保险人人数，04-借款人额度调整
        queryInfo.setApproveTypeCode(approveTypeCode);
        List<ApproveExtendInfo> list = new ArrayList<>();
        //既往理赔号码
        ApproveExtendInfo pastClaimNo = new ApproveExtendInfo();
        pastClaimNo.setApproveExtendInfoCode("pastClaimNo");
        pastClaimNo.setApproveExtendInfoValue(ClmNo);
        list.add(pastClaimNo);
        //借款合同号  loanContractNo
        LCPolicyInfoPojo lcPolicyInfoPojo = (LCPolicyInfoPojo) tradeInfo.getData(LCPolicyInfoPojo.class.getName());
        ApproveExtendInfo loanContractNo = new ApproveExtendInfo();
        loanContractNo.setApproveExtendInfoCode("loanContractNo");
        loanContractNo.setApproveExtendInfoValue(lcPolicyInfoPojo.getJYContNo());
        list.add(loanContractNo);
        //同一借款合同号下被保险人数  sumInsured
        ApproveExtendInfo sumInsured = new ApproveExtendInfo();
        sumInsured.setApproveExtendInfoCode("sumInsured");
        list.add(sumInsured);
        //已出单的被保险人数signedInsured
        ApproveExtendInfo signedInsured = new ApproveExtendInfo();
        signedInsured.setApproveExtendInfoCode("signedInsured");
        list.add(signedInsured);
        //被保人借款金额
        ApproveExtendInfo loanValue = new ApproveExtendInfo();
        loanValue.setApproveExtendInfoCode("loanValue");
        loanValue.setApproveExtendInfoValue(lcPolicyInfoPojo.getLoanAmount());
        list.add(loanValue);
        //保险计划 plan
        ApproveExtendInfo plan = new ApproveExtendInfo();
        plan.setApproveExtendInfoCode("plan");
        plan.setApproveExtendInfoValue(ContPlanCodeEnum.getValue(lcInsuredPojos.get(0).getContPlanCode()));
        list.add(plan);
        //险种
        List<LCPolPojo> lcPolPojos = (List<LCPolPojo>) tradeInfo.getData(LCPolPojo.class.getName());
        ApproveExtendInfo riskCode = new ApproveExtendInfo();
        riskCode.setApproveExtendInfoCode("riskCode");
        riskCode.setApproveExtendInfoValue("01");//写死写为“01”
        list.add(riskCode);
        //险种名称
        LMRiskAppPojo lmRiskAppPojo = redisCommonDao.getEntityRelaDB(LMRiskAppPojo.class, lcPolPojos.get(0).getRiskCode());
        if (lmRiskAppPojo == null) {
            logger.error(lcPolPojos.get(0).getRiskCode() + "该险种信息不存在");
            tradeInfo.addError(lcPolPojos.get(0).getRiskCode() + "该险种信息不存在");
            return null;
        }
        ApproveExtendInfo riskName = new ApproveExtendInfo();
        riskName.setApproveExtendInfoCode("riskName");
        riskName.setApproveExtendInfoValue(lmRiskAppPojo.getRiskName());
        list.add(riskName);

        //被保人姓名
        ApproveExtendInfo name = new ApproveExtendInfo();
        name.setApproveExtendInfoCode("name");
        name.setApproveExtendInfoValue(lcInsuredPojos.get(0).getName());
        list.add(name);
        //被保人性别
        ApproveExtendInfo sex = new ApproveExtendInfo();
        sex.setApproveExtendInfoCode("sex");
        sex.setApproveExtendInfoValue(DomainUtils.convertSex(lcInsuredPojos.get(0).getSex()));
        list.add(sex);
        //被保人身份证号
        ApproveExtendInfo idNo = new ApproveExtendInfo();
        idNo.setApproveExtendInfoCode("idNo");
        idNo.setApproveExtendInfoValue(lcInsuredPojos.get(0).getIDNo());
        list.add(idNo);
        //被保人年龄
        ApproveExtendInfo age = new ApproveExtendInfo();
        age.setApproveExtendInfoCode("age");
        LCInsuredPojo lcInsuredPojo = lcInsuredPojos.get(0);
        LCContPojo lcContPojo = (LCContPojo) tradeInfo.getData(LCContPojo.class.getName());
        if (lcInsuredPojo.getBirthday() != null && !"".equals(lcInsuredPojo.getBirthday()) && lcContPojo.getCValiDate() != null && !"".equals(lcContPojo.getCValiDate())) {
            age.setApproveExtendInfoValue(String.valueOf(PubFun
                    .calInterval(lcInsuredPojo.getBirthday(), lcContPojo.getCValiDate(), "Y")));
        }
        //被保人身份证类型
        ApproveExtendInfo idType = new ApproveExtendInfo();
        idType.setApproveExtendInfoCode("idType");
        idType.setApproveExtendInfoValue(CredenTypeEnum.getValue(lcInsuredPojos.get(0).getIDType()));
        list.add(idType);
        //被保人出生日期
        ApproveExtendInfo birthday = new ApproveExtendInfo();
        birthday.setApproveExtendInfoCode("birthday");
        birthday.setApproveExtendInfoValue(lcInsuredPojos.get(0).getBirthday());
        list.add(birthday);
        //occupationType职业类型
        ApproveExtendInfo occupationType = new ApproveExtendInfo();
        occupationType.setApproveExtendInfoCode("occupationType");
        occupationType.setApproveExtendInfoValue(lcInsuredPojos.get(0).getOccupationType());
        list.add(occupationType);
        //insuYearFlag 保险年龄年期标志
        ApproveExtendInfo insuYearFlag = new ApproveExtendInfo();
        insuYearFlag.setApproveExtendInfoCode("insuYearFlag");
        insuYearFlag.setApproveExtendInfoValue(lcPolPojos.get(0).getInsuYearFlag());
        list.add(insuYearFlag);
        //insuYear 保险年龄年期
        ApproveExtendInfo insuYear = new ApproveExtendInfo();
        insuYear.setApproveExtendInfoCode("insuYear");
        insuYear.setApproveExtendInfoValue(String.valueOf(lcPolPojos.get(0).getInsuYear()));
        list.add(insuYear);
        //sumFee
        ApproveExtendInfo sumFee = new ApproveExtendInfo();
        sumFee.setApproveExtendInfoCode("sumFee");
        double amnt=0;
        for (int i = 0; i < lcPolPojos.size(); i++) {
            amnt = lcPolPojos.get(i).getAmnt();
        }

        //总保费
        String tDutyCode = "";
        int sumPrem = 0;
        // double sumAmnt = 0;
        int tImmunityflagNum = 0;
        List<LCDutyPojo> tLCDutyPojoList = (List) tradeInfo.getData(LCDutyPojo.class.getName());
        List<LCPolPojo> tLCPolPojoList = (List<LCPolPojo>) tradeInfo.getData(LCPolPojo.class.getName());
        for (LCPolPojo lcPolPojo : tLCPolPojoList) {
            //sumAmnt = sumAmnt + lcPolPojo.getAmnt();
            for (int i = 0; tLCDutyPojoList != null && i < tLCDutyPojoList.size(); i++) {
                if (tLCDutyPojoList.get(i).getPolNo() != null && tLCDutyPojoList.get(i).getPolNo().equals(lcPolPojo.getPolNo())) {
                    tDutyCode = tLCDutyPojoList.get(i).getDutyCode();
                    tImmunityflagNum = Integer.parseInt(nbRedisCommon.checkDutyCode(tDutyCode)) > 0 ? tImmunityflagNum + 1 : tImmunityflagNum;
                    if (tDutyCode.length() >= 6 && "03".equals(tDutyCode.substring(4, 6))) {
                        sumPrem += tLCDutyPojoList.get(i).getPrem();
                    } else {
                        if (lcPolPojo.getPolNo() != null && lcPolPojo.getPolNo().equals(lcPolPojo.getMainPolNo())) {
                            switch (lcPolPojo.getPayIntv()) {
                                case 0:
                                    sumPrem += tLCDutyPojoList.get(i).getPrem();
                                    break;
                                case 1:
                                    sumPrem += tLCDutyPojoList.get(i).getPrem() * 12 * lcPolPojo.getPayYears();
                                    break;
                                case 3:
                                    sumPrem += tLCDutyPojoList.get(i).getPrem() * 4 * lcPolPojo.getPayYears();
                                    break;
                                case 6:
                                    sumPrem += tLCDutyPojoList.get(i).getPrem() * 2 * lcPolPojo.getPayYears();
                                    break;
                                case 12:
                                    sumPrem += tLCDutyPojoList.get(i).getPrem() * lcPolPojo.getPayYears();
                                    break;
                            }
                        }
                    }
                }
            }
        }
        sumFee.setApproveExtendInfoValue(String.valueOf(sumPrem));
        list.add(sumFee);
        //sumAmnt
        ApproveExtendInfo sumAmnt = new ApproveExtendInfo();
        sumAmnt.setApproveExtendInfoCode("sumAmnt");
        sumAmnt.setApproveExtendInfoValue(String.valueOf((long) amnt));
        list.add(sumAmnt);

        //承保机构
        ApproveExtendInfo manageCom = new ApproveExtendInfo();
        manageCom.setApproveExtendInfoCode("manageCom");
        manageCom.setApproveExtendInfoValue(lcContPojo.getManageCom());
        list.add(manageCom);
        //销售渠道
        ApproveExtendInfo saleChnl = new ApproveExtendInfo();
        saleChnl.setApproveExtendInfoCode("saleChnl");
        saleChnl.setApproveExtendInfoValue(lcContPojo.getSaleChnl());
        list.add(saleChnl);
        //中介机构
        ApproveExtendInfo agentCom = new ApproveExtendInfo();
        agentCom.setApproveExtendInfoCode("agentCom");
        agentCom.setApproveExtendInfoValue(lcContPojo.getAgentCom());
        list.add(agentCom);

        //业务员姓名salemanName
        ApproveExtendInfo salemanName = new ApproveExtendInfo();
        salemanName.setApproveExtendInfoCode("salemanName");
        LAAgentPojo laAgentPojo = redisCommonDao.getEntityRelaDB(LAAgentPojo.class,lcContPojo.getAgentCode());
        if (laAgentPojo == null) {
            logger.debug("代理信息不存在请检查"+tradeInfo.toString());
            tradeInfo.addError("代理人信息不存在请检查");
            return null;
        }
        salemanName.setApproveExtendInfoValue(laAgentPojo.getName());
        list.add(salemanName);
        //职业描述  occupationDesc
        ApproveExtendInfo occupationDesc = new ApproveExtendInfo();
        occupationDesc.setApproveExtendInfoCode("occupationDesc");
        occupationDesc.setApproveExtendInfoValue(lcInsuredPojo.getWorkType());
        list.add(occupationDesc);
        //费率
        ApproveExtendInfo rate = new ApproveExtendInfo();
        rate.setApproveExtendInfoCode("rate");
        list.add(rate);
        //净费率折扣rateReBate
        ApproveExtendInfo rateReBate = new ApproveExtendInfo();
        rateReBate.setApproveExtendInfoCode("rateReBate");
        list.add(rateReBate);
        queryInfo.setApproveExtendInfos(list);
        List<QueryInfo> queryInfoList = new ArrayList<>();
        queryInfoList.add(queryInfo);
        body.setQueryInfos(queryInfoList);

        Message message = new Message();
        message.setHead(head);
        message.setBody(body);
        //调用拼装报文的消息
        String request = SpecMesToXml.getXml(message);
        return request;
    }
}
