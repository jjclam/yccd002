package com.sinosoft.cloud.cbs.rules.data;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.sinosoft.cache.dao.impl.RedisCommonDao;
import com.sinosoft.cloud.cbs.rules.bom.*;
import com.sinosoft.cloud.rest.TradeInfo;
import com.sinosoft.lis.entity.LCCustomerImpartParamsPojo;
import com.sinosoft.utility.*;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @Author: zhuyiming
 * @Description: 提取告知信息
 * @Date: Created in 20:08 2017/9/24
 */
@Service
public class ExtractDisclose {
	private final Log logger = LogFactory.getLog(this.getClass());
/*
	public void getDisclose(Policy policy,TradeInfo requestInfo) {
		// 投保人告知
		getApplicantDisclose(policy,requestInfo);
		// 被保人告知
		getinsuredDisclose(policy,requestInfo);
		// 代理人告知
		getClientInfoDisclose(policy,requestInfo);

	}

	*//**
	 * 被保人告知信息
	 *//*
	public void getinsuredDisclose(Policy policy,TradeInfo requestInfo) {
		// 被保人列表
		List insuredInfoList = policy.getInsuredList();
		if (insuredInfoList != null && insuredInfoList.size() > 0) {
			for (Iterator iter = insuredInfoList.iterator(); iter.hasNext();) {
				Insured insured = (Insured) iter.next();
				String clientNo = insured.getClientNo();
				// 获取告知
				getDiscloseExtract(policy, clientNo, "1",requestInfo);
			}

		}

	}

	*//**
	 * 投保人告知信息
	 * 
	 * @param policy
	 *//*
	public void getApplicantDisclose(Policy policy,TradeInfo requestInfo) {
		// 投保人信息
		Applicant applicantInfo = policy.getApplicant();

		// 客户号
		String clientNo = applicantInfo.getClientNo();
		// 获取告知
		getDiscloseExtract(policy, clientNo, "0",requestInfo);

	}

	public void getClientInfoDisclose(Policy policy, TradeInfo requestInfo) {
		// 代理人列表
		List clientInfoList = policy.getClientInfoList();
		if (clientInfoList != null && clientInfoList.size() > 0) {
			ClientInfo client = (ClientInfo) clientInfoList.get(0);
			// 获取代理人告知信息
			getDiscloseExtract(policy, client.getClientNo(), "2",requestInfo);
		}
	}*/

	/**
	 * 告知类提数方法
	 * 
	 * @param policy
	 *            保单
	 * @param clientno
	 *            客户号玛
	 * @param clientType
	 *            客户类型
	 */
	public NotificationInfo getDiscloseExtract(Policy policy, String clientno,
			String clientType,TradeInfo requestInfo,boolean immunityflag) {
		Map discloseMap = new HashMap();

		/*// 告知信息列表
		List discloseList = policy.getNotificationInfoList();*/

		// 告知明细列表
		Map notifyMap = new HashMap();
		// 告知明细
		NotificationInfo discloseInfo = new NotificationInfo();
		// 告知客户号
		discloseInfo.setClientNo(clientno);
		// 告知客户类型
		discloseInfo.setClientType(clientType);

		List<LCCustomerImpartParamsPojo> tLCCustomerImpartParamsList  = (List<LCCustomerImpartParamsPojo>) requestInfo.getData(LCCustomerImpartParamsPojo.class.getName());

		if (null != tLCCustomerImpartParamsList && tLCCustomerImpartParamsList.size() > 0) {
			for (int i = 0; i < tLCCustomerImpartParamsList.size(); i++) {
				if (immunityflag!=true){
					if(!clientType.equals(tLCCustomerImpartParamsList.get(i).getCustomerNoType()) ){
						continue;
					}
				}else{
					if(!clientno.equals(tLCCustomerImpartParamsList.get(i).getCustomerNo())){
						continue;
					}
				}
				// 销售方式
				ArrayList list = new ArrayList();
				Notify notify = new Notify();
				String overcode = "";
				if(tLCCustomerImpartParamsList.get(i).getImpartVer()!=null){
					overcode += tLCCustomerImpartParamsList.get(i).getImpartVer();
				}
				if(tLCCustomerImpartParamsList.get(i).getImpartCode()!=null){
					overcode +=  "_" + tLCCustomerImpartParamsList.get(i).getImpartCode();
				}
				if(tLCCustomerImpartParamsList.get(i).getImpartParamName()!=null){
					overcode += "_" + tLCCustomerImpartParamsList.get(i).getImpartParamName();
				}
				// 告知名称
				notify.setName(tLCCustomerImpartParamsList.get(i).getImpartParamName());
				notify.setContents(tLCCustomerImpartParamsList.get(i).getImpartParam());
				String s = tLCCustomerImpartParamsList.get(i).getCustomerNo();

				// 告知代码
				notify.setCode(overcode);
				list.add(notify);

				if(clientno != null){
					if (!clientType.equals("2") && clientno.equals(s)) {
						// 将信息放入map  投保人被保人告知信息赋值
						notifyMap.put(notify.getCode(), notify);
					}else{
						// 将信息放入map  代理人告知赋值
						notifyMap.put(notify.getCode(), notify);
					}
				}

				if (clientno != null) {
					if (clientno.equals(s)) {


						discloseMap.put(overcode, tLCCustomerImpartParamsList.get(i).getImpartParam());
					}

				}
			}

			discloseInfo.setNotifyMap(notifyMap);
//			discloseList.add(discloseInfo);
//
//			policy.setNotificationInfoList(discloseList);



		}
		// 当客户类型不是代理人时
		if (!clientType.equals("2") && clientno != null) {
			// 对部分特殊告知做数据转换
			getOtherDiscose(policy, clientType, discloseMap, clientno);
		}
		return discloseInfo;
	}

	/**
	 * 对部分特殊告知做数据转换
	 * 
	 * @param policy
	 * @param clientType
	 *            客户类型
	 * @param discloseMap
	 */
	public void getOtherDiscose(Policy policy, String clientType,
			Map discloseMap, String clientno) {
		// 孕妇标识
		boolean flag = false;
		if (discloseMap.get("GX14_19A_YesOrNo") != null
				&& ("1".equals(discloseMap.get("GX14_19A_YesOrNo"))
				|| "Y".equals(discloseMap.get("GX14_19A_YesOrNo")))) {

			flag = true;
		}
		if (discloseMap.get("GX14_10_YesOrNo") != null
				&& "1".equals(discloseMap.get("GX14_10_YesOrNo"))) {

			flag = true;
		}

		// 孕周
		int pregWeek = 0;
		if (discloseMap.get("GX14_19A_Week") != null) {
			if (discloseMap.get("GX14_19A_Week").equals("无")
					|| discloseMap.get("GX14_19A_Week").equals("*")) {
				pregWeek = 17;
			} else {
				pregWeek = Util
						.toInt((String) discloseMap.get("GX14_19A_Week"));
			}
		}
		// 年收入
		double annualIncome = -1.0d;
		if (discloseMap.get("01_001_AnnualIncome") != null) {
			// 当收入不为*的时候
			if (!discloseMap.get("01_001_AnnualIncome").equals("*")) {
				annualIncome = Util.toDouble((String) discloseMap
						.get("01_001_AnnualIncome"));
			}

			if ("".equals(discloseMap.get("01_001_AnnualIncome").toString()
					.trim())
					|| discloseMap.get("01_001_AnnualIncome").equals("*")) {
				annualIncome = -1.0;
			}
		}
		// 家庭年收入
		double familyAnnualIncome = -1.0d;
		if (discloseMap.get("01_001_FamilyIncome") != null) {
			familyAnnualIncome = Util.toDouble((String) discloseMap
					.get("01_001_FamilyIncome"));
		}
		// 保费预算
		double budget = 0.0d;
		if (discloseMap.get("01_016_Budget") != null) {
			budget = Util.toDouble((String) discloseMap
					.get("01_016_Budget"));
		}
		
		// 当客户类型为0时对投保人进行赋值
		if (clientType.equals("0")) {

			Applicant applicant = policy.getApplicant();
			// 孕妇标识
			applicant.setPregSign(flag);
			// 孕周
			applicant.setPregWeek(pregWeek);
			// 年收入
			applicant.setAnnualIncome(annualIncome);
			// 家庭年收入
			applicant.setFamilyAnnualIncome(familyAnnualIncome);
			//保费预算
			applicant.setPremiumBudget(budget);
		}
		// 当客户类型为被保人的时候对被保人进行赋值
		if (clientType.equals("1")) {

			List insuredList = policy.getInsuredList();

			for (Iterator iter = insuredList.iterator(); iter.hasNext();) {
				Insured insured = (Insured) iter.next();
				if (insured.getClientNo().equals(clientno)) {
					insured.setPregSign(flag);
					insured.setPregWeek(pregWeek);
					insured.setAnnualIncome(annualIncome);
					insured.setFamilyAnnualIncome(familyAnnualIncome);

				}
			}

		}

	}

}
