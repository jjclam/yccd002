package com.sinosoft.cloud.cbs.uw;

import com.sinosoft.cloud.cbs.bl.AmntHistory6807BL;
import com.sinosoft.cloud.microservice.AbstractMicroService;
import com.sinosoft.cloud.rest.TradeInfo;
import com.sinosoft.lis.entity.LCContPojo;
import com.sinosoft.lis.entity.LCPolPojo;
import com.sinosoft.lis.entity.LCPolicyInfoPojo;
import com.sinosoft.utility.ExceptionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Author:wangshuliang
 * @Description:
 * @Date:Created in 9:51 2018/7/2
 * @Modified by:
 */
@Component("AmntHistory6807Service")
public class AmntHistory6807Service extends AbstractMicroService {
    private final Log logger = LogFactory.getLog(this.getClass());
    @Autowired
    private  AmntHistory6807BL amntHistory6807BL;
    @Override
    public boolean checkData(TradeInfo tradeInfo) {
        LCContPojo lcContPojo = (LCContPojo) tradeInfo.getData(LCContPojo.class.getName());
        if(lcContPojo==null){
            logger.debug("保单信息不存在请检查");
            return false;
        }
        List<LCPolPojo> lcPolPojo  = (List<LCPolPojo>) tradeInfo.getData(LCPolPojo.class.getName());
        if (lcContPojo==null || lcPolPojo.size()<=0){
            logger.debug("险种信息不存在");
            return false;
        }
      /*  LCPolicyInfoPojo lcPolicyInfoPojo = (LCPolicyInfoPojo) tradeInfo.getData(LCPolicyInfoPojo.class.getName());
        if(lcPolicyInfoPojo == null ){
            logger.debug("借款合同信息不存在");
            return  false;

        }*/
        return true;
    }

    @Override
    public TradeInfo dealData(TradeInfo tradeInfo) {
        try{
            tradeInfo = amntHistory6807BL.submitData(tradeInfo);
            if (tradeInfo.hasError()){
                logger.debug("提取6807保额失败"+tradeInfo.toString());
                tradeInfo.addError("提取6807保额失败");
                return tradeInfo;
            }
        }catch (Exception e){
            logger.error(ExceptionUtils.exceptionToString(e));
            logger.debug("提取6807保额失败"+tradeInfo.toString());
            tradeInfo.addError("提取6807保额失败");
            return tradeInfo;
        }

        return tradeInfo;
    }
}
