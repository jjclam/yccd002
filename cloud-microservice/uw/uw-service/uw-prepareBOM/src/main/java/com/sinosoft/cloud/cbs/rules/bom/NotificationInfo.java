package com.sinosoft.cloud.cbs.rules.bom;

import java.util.Map;

/**
 * 告知信息
 * 
 * @author dingfan
 * 
 */
public class NotificationInfo{
	/**
	 * 客户号
	 */
	private String clientNo;

	/**
	 * 客户类型
	 */
	private String clientType;
	
	/**
	 * 告知类型
	 */
	private String notifyType;

	/**
	 * 告知明细列表
	 */
	private Map notifyMap;

	public String getClientNo() {
		return clientNo;
	}

	public void setClientNo(String clientNo) {
		this.clientNo = clientNo;
	}

	public String getClientType() {
		return clientType;
	}

	public void setClientType(String clientType) {
		this.clientType = clientType;
	}

	public String getNotifyType() {
		return notifyType;
	}

	public void setNotifyType(String notifyType) {
		this.notifyType = notifyType;
	}

	public Map getNotifyMap() {
		return notifyMap;
	}

	public void setNotifyMap(Map notifyMap) {
		this.notifyMap = notifyMap;
	}
}
