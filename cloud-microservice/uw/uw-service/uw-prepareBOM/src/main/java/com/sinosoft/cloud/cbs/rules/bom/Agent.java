package com.sinosoft.cloud.cbs.rules.bom;

/**
 * 代理人信息
 * 
 * @author dingfan
 * 
 */
public class Agent {
	/**
	 * 代理人代码
	 */
	private String agentCode;

	/**
	 * 代理人姓名
	 */
	private String agentName;

	/**
	 * 代理人是否为投保人
	 */
	private boolean applicant;

	/**
	 * 联系电话
	 */
	private String phones;

	/**
	 * 业务人员黑名单标识
	 */
	private boolean blackSign;

	/**
	 * 非自保件主动投保标识
	 */
	private boolean autoPolicySign;

	/**
	 * 星级代理人标识
	 */
	private boolean starAgentSign;
	
	/**
	 * 代理人所在机构
	 */
	private String agentManageCom;

	/**
	 * 监控状态
	 */
	private boolean agentMonitorState;

	/**
	 * 评级结果  --wpq--
	 */
	private String stateValue;

	/**
	 * 品质违规分值 --wpq--
	 */
    private double t5;

	/**
	 * 星级代理人等级  --wpq--
	 */
	private String starAgentGrade;

	public double getT5() {
		return t5;
	}

	public void setT5(double t5) {
		this.t5 = t5;
	}

	public String getStarAgentGrade() {
		return starAgentGrade;
	}

	public void setStarAgentGrade(String starAgentGrade) {
		this.starAgentGrade = starAgentGrade;
	}

	public String getStateValue() {
		return stateValue;
	}

	public void setStateValue(String stateValue) {
		this.stateValue = stateValue;
	}

	public boolean isAgentMonitorState() {
		return agentMonitorState;
	}

	public void setAgentMonitorState(boolean agentMonitorState) {
		this.agentMonitorState = agentMonitorState;
	}

	public String getAgentManageCom() {
		return agentManageCom;
	}

	public void setAgentManageCom(String agentManageCom) {
		this.agentManageCom = agentManageCom;
	}

	
	

	public String getAgentCode() {
		return agentCode;
	}

	public void setAgentCode(String agentCode) {
		this.agentCode = agentCode;
	}

	public String getAgentName() {
		return agentName;
	}

	public void setAgentName(String agentName) {
		this.agentName = agentName;
	}

	public boolean isApplicant() {
		return applicant;
	}

	public void setApplicant(boolean applicant) {
		this.applicant = applicant;
	}

	public String getPhones() {
		return phones;
	}

	public void setPhones(String phones) {
		this.phones = phones;
	}

	public boolean isBlackSign() {
		return blackSign;
	}

	public void setBlackSign(boolean blackSign) {
		this.blackSign = blackSign;
	}

	public boolean isAutoPolicySign() {
		return autoPolicySign;
	}

	public void setAutoPolicySign(boolean autoPolicySign) {
		this.autoPolicySign = autoPolicySign;
	}

	public boolean isStarAgentSign() {
		return starAgentSign;
	}

	public void setStarAgentSign(boolean starAgentSign) {
		this.starAgentSign = starAgentSign;
	}
}
