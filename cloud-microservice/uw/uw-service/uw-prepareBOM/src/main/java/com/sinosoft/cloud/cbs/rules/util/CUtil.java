package com.sinosoft.cloud.cbs.rules.util;



import com.jiahe.amq.exception.AMQAppException;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class CUtil {
	/**
	 * get the Properties with the given name of the file
	 * 
	 * @param name
	 * @return
	 * @throws AMQAppException
	 */
	public static Properties getPropertiesByFileName(String name) throws AMQAppException {
		ClassLoader cl = null;
		Properties props = null;
		try {
			cl = CUtil.class.getClassLoader();
			InputStream is = cl.getResourceAsStream(name);
			props = new Properties();
			props.load(is);
		} catch (IOException e) {
			throw new AMQAppException("getPropertiesByFileName Exception : name is : " + name);
		}
		return props;
	}

	/**
	 * get the value by the given key and props
	 * 
	 * @param key
	 * @param props
	 * @return
	 */
	public static String getProperty(String key, Properties props) {
		return getProperty(key, null, props);
	}

	/**
	 * get the value by the given key and props, return defaultValue when not
	 * found the value
	 * 
	 * @param key
	 * @param defaultValue
	 * @param props
	 * @return
	 */
	public static String getProperty(String key, String defaultValue, Properties props) {
		String rs = props.getProperty(key, defaultValue);
		if (rs == null)
			return rs;
		else
			return rs.trim();
	}

	public static void main(String[] args) {
		
	}
}
