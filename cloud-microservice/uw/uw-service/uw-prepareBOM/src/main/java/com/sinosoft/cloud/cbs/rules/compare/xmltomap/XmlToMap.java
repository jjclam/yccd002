package com.sinosoft.cloud.cbs.rules.compare.xmltomap;

import com.sinosoft.cloud.cbs.rules.httpclient.HttpRequestClient;
import com.sinosoft.lis.entity.LCAppntLinkManInfoPojo;
import com.sinosoft.lis.entity.LCGetPojo;
import com.sinosoft.utility.ExceptionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import java.io.ByteArrayInputStream;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * xml转换成map集合的公共处理类.
 */
public class XmlToMap {

    private static SAXReader reader = new SAXReader();
    private static Document document = null;
    private static Log logger = LogFactory.getLog(XmlToMap.class);

    /**
     * 没有集合，，转换得到的集合
     *
     * @param xml 没有集合的xml.
     * @return
     */
    public static Map<String, String> noList(String xml) {

        final Map<String, String> map = new HashMap<String, String>();

        try {
            document = reader.read(new ByteArrayInputStream(xml.getBytes("UTF-8")));
            final Element element = document.getRootElement();
            final List<Element> listElement = element.elements();
            if (listElement == null || listElement.size() > 0) {
                for (Element element1 : listElement) {
                    if (element1.getText() == null || "".equals(element1.getText())) {
                        map.put(element1.getName(), "00");
                    } else {
                        map.put(element1.getName(), element1.getText());
                    }
                }
            } else {
                logger.debug("解析报文错误");
            }
        } catch (DocumentException e) {
            logger.debug("解析报文错误异常");
            e.printStackTrace();
            logger.error("根据报文转换map失败" + ExceptionUtils.exceptionToString(e));
        } catch (UnsupportedEncodingException e) {
            logger.debug("解析报文错误异常");
            e.printStackTrace();
            logger.error("根据报文转换map失败" + ExceptionUtils.exceptionToString(e));
        }
        return map;
    }


    /**
     * 有集合，，转换得到的集合
     *
     * @param xml 有集合的xml.
     * @return
     */
    public static Map<Integer, Map<String, String>> haveListResult(String xml) {
        final Map<Integer, Map<String, String>> mapMap = new HashMap<Integer, Map<String, String>>();
        try {
            document = reader.read(new ByteArrayInputStream(xml.getBytes("UTF-8")));

            final Element element = document.getRootElement();
            final List<Element> listElement = element.elements();
            int i = 0;
            if (listElement.size() > 0) {
                for (Element element1 : listElement) {
                    i++;
                    final Map<String, String> map = new HashMap<String, String>();
                    for (Element element2 : element1.elements()) {
                        if (element2.getText() == null || "".equals(element2.getText())) {
                            map.put(element2.getName(), "00");
                        } else {
                            map.put(element2.getName(), element2.getText());
                        }
                    }
                    mapMap.put(i, map);
                }
            } else {
                logger.debug("解析报文错误");
            }
        } catch (DocumentException e) {
            logger.debug("解析报文错误异常");
            e.printStackTrace();
            logger.error("根据报文转换map失败" + ExceptionUtils.exceptionToString(e));
        } catch (UnsupportedEncodingException e) {
            logger.debug("解析报文错误异常");
            e.printStackTrace();
            logger.error("根据报文转换map失败" + ExceptionUtils.exceptionToString(e));
        }
        return mapMap;
    }


//    public static void main(String[] args) {
//        String sql = "com.sinosoft.lis.entity.LCAppntLinkManInfoPojo=LCAppntLinkManInfoPojo [AppntLinkID=1264819, ContID=1264821, ShardingID=377567665, PrtNo=377567665, ContNo=20181300002788531027, AppntNo=0013717698, Name=九数据七, Sex=null, Birthday=null, IDType=null, IDNo=null, RelationToApp=null, Tel1=null, Tel3=null, Tel4=null, Tel5=null, Tel2=null, Mobile1=null, Mobile2=null, Address=null, ZipCode=null, Email=null, ManageCom=86130000, Operator=ABC-CLOUD, MakeDate=2018-05-29, MakeTime=10:19:40, ModifyDate=2018-05-29, ModifyTime=10:19:40]";
//        String subString = "";
//        String[] mm = null;
//        String className = sql.substring(sql.indexOf("LCAppntLinkManInfoPojo"), sql.indexOf("="));
//        Map<Integer, Map<String, String>> mapMap = new HashMap<Integer, Map<String, String>>();
//        if ("Insured".equals(className) || "Risk".equals(className) || "LCGetPojo".equals(className)) {
//            subString = sql.substring(sql.indexOf("[") + 1, sql.lastIndexOf("]"));
//            mm = subString.split("],");
//
//            for (int i = 0; i < mm.length; i++) {
//                Map<String, String> map = new HashMap<String, String>();
//                if (i != mm.length - 1) {
//                    // System.out.println("第" + i + "个" + mm[i] + "]");
//                    mm[i] = mm[i] + "]";
//                    String subMm = mm[i].substring(mm[i].indexOf("[") + 1, mm[i].lastIndexOf("]"));
//                    for (String s : subMm.split(",")) {
//                        String[] values = s.split("=");
//                        map.put(values[0], values[1]);
//                    }
//                } else {
//                    // System.out.println("第" + i + "个" + mm[i]);
//                    String subMm = mm[i].substring(mm[i].indexOf("[") + 1, mm[i].lastIndexOf("]"));
//                    for (String s : subMm.split(",")) {
//                        String[] values = s.split("=");
//                        map.put(values[0], values[1]);
//                    }
//                }
//                mapMap.put(i, map);
//            }
//        }
//        if ("LCAppntLinkManInfoPojo".equals(className)) {
//            Map<String, String> map = new HashMap<String, String>();
//            String subMm = sql.substring(sql.indexOf("[") + 1, sql.lastIndexOf("]"));
//            for (String s : subMm.split(",")) {
//                String[] values = s.split("=");
//                map.put(values[0], values[1]);
//            }
//            mapMap.put(0, map);
//        }
//
//        List<LCAppntLinkManInfoPojo> lcGetPojos = new ArrayList<LCAppntLinkManInfoPojo>();
//        Class clazz = null;
//        Object obj = null;
//        for (Integer integer : mapMap.keySet()) {
//
//            try {
//                clazz = Class.forName("com.sinosoft.lis.entity." + className);
//                obj = clazz.newInstance();
//                Map<String, String> mapmap = mapMap.get(integer);
//
//                for (String key : mapmap.keySet()) {
//                    Object value = mapmap.get(key);
//                    key = key.replace(" ", "");
//                    String setter = "set" + key;
//                    Method setterMethod = clazz.getMethod(("set" + key), String.class);
//                    setterMethod.invoke(obj, value);
//                }
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//
//
////            LCAppntLinkManInfoPojo lcGetPojo = (LCAppntLinkManInfoPojo) obj;
////            lcGetPojos.add(lcGetPojo);
//        }
//        LCAppntLinkManInfoPojo lcGetPojo = (LCAppntLinkManInfoPojo) obj;
//        System.out.println(lcGetPojo);
//
//
//    }
}