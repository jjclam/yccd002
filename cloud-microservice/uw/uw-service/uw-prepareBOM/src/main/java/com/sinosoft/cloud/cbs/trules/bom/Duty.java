package com.sinosoft.cloud.cbs.trules.bom;

import java.util.Date;

/**
 * 责任信息
 *
 */
public class Duty {

	/**
	 * 险种编码
	 */
	private String riskCode;
	
	/**
	 * 责任编码
	 */
	private String riskVersion;
	
	/**
	 * 险种编码
	 */
	private String dutyCode;
	
	/**
	 * 责任名称
	 */
	private String dutyName;
	
	/**
	 * 保额
	 */
	private double amnt;
	
	/**
	 * 保费
	 */
	private double prem;
	
	/**
	 * 浮动费率
	 */
	private double floatRate;
	
	/**
	 * 生效日期
	 */
	private String valiDate;
	
	/**
	 * 终止日期
	 */
	private String endDate;
	
	/**
	 * 交费间隔
	 */
	private String payIntv;
	
	/**
	 * 终交年龄年期标志
	 */
	private String payEndYearFlag;
	
	/**
	 * 终交年龄年期
	 */
	private String payEndYear;
	
	/**
	 * 保险年龄年期标志
	 */
	private String insuYearFlag;
	
	/**
	 * 保险年龄年期
	 */
	private String insuYear;
	
	/**
	 * 续保标志
	 */
	private String rnewFlag;
	
	/**
	 * 自动垫缴标志
	 */
	private String autoPayFlag;

	public String getRiskCode() {
		return riskCode;
	}

	public void setRiskCode(String riskCode) {
		this.riskCode = riskCode;
	}

	public String getRiskVersion() {
		return riskVersion;
	}

	public void setRiskVersion(String riskVersion) {
		this.riskVersion = riskVersion;
	}

	public String getDutyCode() {
		return dutyCode;
	}

	public void setDutyCode(String dutyCode) {
		this.dutyCode = dutyCode;
	}

	public String getDutyName() {
		return dutyName;
	}

	public void setDutyName(String dutyName) {
		this.dutyName = dutyName;
	}

	public double getAmnt() {
		return amnt;
	}

	public void setAmnt(double amnt) {
		this.amnt = amnt;
	}

	public double getPrem() {
		return prem;
	}

	public void setPrem(double prem) {
		this.prem = prem;
	}

	public double getFloatRate() {
		return floatRate;
	}

	public void setFloatRate(double floatRate) {
		this.floatRate = floatRate;
	}

	public String getValiDate() {
		return valiDate;
	}

	public void setValiDate(String valiDate) {
		this.valiDate = valiDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public String getPayIntv() {
		return payIntv;
	}

	public void setPayIntv(String payIntv) {
		this.payIntv = payIntv;
	}

	public String getPayEndYearFlag() {
		return payEndYearFlag;
	}

	public void setPayEndYearFlag(String payEndYearFlag) {
		this.payEndYearFlag = payEndYearFlag;
	}

	public String getPayEndYear() {
		return payEndYear;
	}

	public void setPayEndYear(String payEndYear) {
		this.payEndYear = payEndYear;
	}

	public String getInsuYearFlag() {
		return insuYearFlag;
	}

	public void setInsuYearFlag(String insuYearFlag) {
		this.insuYearFlag = insuYearFlag;
	}

	public String getInsuYear() {
		return insuYear;
	}

	public void setInsuYear(String insuYear) {
		this.insuYear = insuYear;
	}

	public String getRnewFlag() {
		return rnewFlag;
	}

	public void setRnewFlag(String rnewFlag) {
		this.rnewFlag = rnewFlag;
	}

	public String getAutoPayFlag() {
		return autoPayFlag;
	}

	public void setAutoPayFlag(String autoPayFlag) {
		this.autoPayFlag = autoPayFlag;
	}
}
