package com.sinosoft.cloud.cbs.function;

import com.sinosoft.cloud.cbs.uw.PrepareBOMService;
import com.sinosoft.cloud.rest.TradeInfo;
import com.sinosoft.lis.entity.LCResultInfoPojo;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by zkr on 2018/6/27.
 */
@Component
public class NewUWFunction {
    private final Log logger = LogFactory.getLog(this.getClass());
   @Autowired
   PrepareBOMService prepareBOMService;
    public void dealDate(TradeInfo tradeInfo){
        long start=System.currentTimeMillis();
        prepareBOMService.service(tradeInfo);
        logger.debug("核保总用时："+(System.currentTimeMillis()-start)/1000.0+"s");
        if(tradeInfo.hasError()){
            logger.debug("核保出错！");
            List<LCResultInfoPojo> lcResultInfoPojos = (List) tradeInfo.getData(LCResultInfoPojo.class.getName());
            if(lcResultInfoPojos != null && lcResultInfoPojos.size()>0){
                logger.debug("["+lcResultInfoPojos.get(0).getResultNo()+"]"+lcResultInfoPojos.get(0).getResultContent()+"没有通过！");
            }
            return;
        }
    }

}
