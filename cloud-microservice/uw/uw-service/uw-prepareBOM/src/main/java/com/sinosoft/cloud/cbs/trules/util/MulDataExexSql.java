package com.sinosoft.cloud.cbs.trules.util;

import com.sinosoft.utility.*;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.sql.Connection;
import java.sql.SQLException;

/**
 * @Author:wangshuliang
 * @Description:  查询多数据源的公共类
 * @Date:Created in 9:42 2018/6/28
 * @Modified by:
 */
public class MulDataExexSql {
    private final Log logger = LogFactory.getLog(this.getClass());
    /**
     * 多数据源查询数据库的方法
     * @param vData   sql和参数的封装类
     * @param dataName  数据源的名字
     * @return
     */
    public SSRS excuteSql(VData vData,String dataName) throws Exception{
        SSRS mSSRS = null;
        Connection tConnection = null;
        try {
            tConnection = DBConnPool.getConnection(dataName);
            if(tConnection==null){
                logger.error("没有获取到数据连接请检查");
                throw new SQLException();
            }
            ExeSQL tExeSQL = new ExeSQL(tConnection);
            mSSRS = tExeSQL.execSQL(vData);
        }catch (Exception e){
            e.printStackTrace();
            logger.error(ExceptionUtils.exceptionToString(e));
            throw e;
        }finally {
            if (tConnection != null) {
                try {
                    tConnection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                    logger.error(ExceptionUtils.exceptionToString(e));
                    throw e;
                }
            }
        }
        return mSSRS;
    }
}
