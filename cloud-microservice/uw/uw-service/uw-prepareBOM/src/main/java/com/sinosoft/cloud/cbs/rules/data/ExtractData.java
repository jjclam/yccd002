package com.sinosoft.cloud.cbs.rules.data;

import com.sinosoft.cloud.cbs.rules.bom.*;
import com.sinosoft.cloud.rest.TradeInfo;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * 提数程序类
 * 
 * @author dingfan
 * 
 */
@Component
public class ExtractData {
	private final Log logger = LogFactory.getLog(this.getClass());

	@Autowired
	ExtractPolicy mExtractPolicy;
	@Autowired
	ExtractApplicantInfo mExtractApplicantInfo;
	@Autowired
	ExtractInsuredInfo mExtractInsuredInfo;
	@Autowired
	ExtractCustInfo mExtractCustInfo;
	@Autowired
	ExtractAgent mExtractAgent;
	@Autowired
	ExtractInsurType mExtractInsurType;
	@Autowired
	ExtractDisclose mExtractDisclose;
	@Autowired
	ExtractBnfcryInfo mExtractBnfcryInfo;
	@Autowired
	ExtractCheck mExtractCheck;
	@Autowired
	ExtractPast mExtractPast;
	@Autowired
	ExtractNotifyAgent mExtractNotifyAgent;
	@Autowired
	ExtractNotifyApplicant mExtractNotifyApplicant;
	@Autowired
	ExtractNotifyInsured mExtractNotifyInsured;
	@Autowired
	ExtractTransCodeInfo mExtractTransCodeInfo;

	/**
	 * 提数总接口
	 * 
	 * @return
	 * @throws Exception 
	 */
	public Policy getData(TradeInfo tradeInfo) throws Exception {
		//保单
		Policy policy  = new Policy();
		Applicant applicantInfo = new Applicant();
		List insuredInfoList = new ArrayList();
		List custInfoList = new ArrayList();
		List bnfcryInfoList = new ArrayList();
		Agent agent = new Agent();
		List insurTypeList = new ArrayList();
//		List discloseList = new ArrayList();
		List insuredNotifyList = new ArrayList();
		ApplicantNotify applicantNotify = new ApplicantNotify();
		AgentNotify agentNotify = new AgentNotify();
		AccidentResponse accidentResponse=new AccidentResponse();
		MajorDiseaseCheck majorDiseaseCheck=new MajorDiseaseCheck();

		// 投保人
		policy.setApplicant(applicantInfo);
		// 被保人列表
		policy.setInsuredList(insuredInfoList);
		// 客户信息列表
		policy.setClientInfoList(custInfoList);
		// 受益人列表
		policy.setBeneficiaryList(bnfcryInfoList);
		// 代理人
		policy.setAgent(agent);
		// 险种列表
		policy.setRiskList(insurTypeList);
/*		// 告知信息列表
		policy.setNotificationInfoList(discloseList);*/
        //提取投保人告知数据
		policy.setApplicantNotify(applicantNotify);
		//提取被保人告知数据
		policy.setInsuredNotifyList(insuredNotifyList);
		//提取代理人告知数据
        policy.setAgentNotify(agentNotify);

        // 提取保单数据
		long start = System.currentTimeMillis();
		logger.info("policy-begin");
		long beginTime = System.currentTimeMillis();
		mExtractPolicy.getPolicy(policy,tradeInfo);
		if(tradeInfo.hasError()){
			logger.info("提取保单数据失败！");
			tradeInfo.addError("提取保单数据失败！");
			return policy;
		}
		logger.info("提取保单数据成功，提数：" + (System.currentTimeMillis() - beginTime)/1000.0 + "s");

		logger.info("appnt-begin");
		// 提取投保人数据
		beginTime = System.currentTimeMillis();
		mExtractApplicantInfo.getApplicantInfo(policy,tradeInfo);
		if(tradeInfo.hasError()){
			logger.info("提取投保人数据失败！");
			tradeInfo.addError("提取投保人数据失败！");
			return policy;
		}
		logger.info("提取投保人数据成功，提数：" + (System.currentTimeMillis() - beginTime)/1000.0 + "s");

		logger.info("insue-begin");

		// 提取险种数据
		logger.info("insurtype-begin");
		beginTime = System.currentTimeMillis();
		mExtractInsurType.getInsurTypeList(policy,tradeInfo);
		if(tradeInfo.hasError()){
			logger.info("提取险种数据失败！");
			tradeInfo.addError("提取险种数据失败！");
			return policy;
		}
		logger.info("提取险种数据成功，提数：" + (System.currentTimeMillis() - beginTime)/1000.0 + "s");


		// 提取被保人数据
		beginTime = System.currentTimeMillis();
		mExtractInsuredInfo.getInsuredInfo(policy,tradeInfo);
		if(tradeInfo.hasError()){
			logger.info("提取被保人数据失败！");
			tradeInfo.addError("提取被保人数据失败！");
			return policy;
		}
		logger.info("提取被保人数据成功，提数：" + (System.currentTimeMillis() - beginTime)/1000.0 + "s");

		// 提取受益人数据
		// 提取受益人数据--电子银行默认法定受益人，无需提数
		String sellType =policy.getSellType();
		logger.info("bnf-begin");
		beginTime = System.currentTimeMillis();
		if(!"22".equals(sellType)&& !"24".equals(sellType)&&
				!"25".equals(sellType)&& !"26".equals(sellType)&&
				!"27".equals(sellType)){
			mExtractBnfcryInfo.getBnfcryInfoList(policy,tradeInfo);
		}
		if(tradeInfo.hasError()){
			logger.info("提取受益人数据失败！");
			tradeInfo.addError("提取受益人数据失败！");
			return policy;
		}
		logger.info("提取受益人数据成功，提数：" + (System.currentTimeMillis() - beginTime)/1000.0 + "s");

		// 提取客户数据
		logger.info("cust-begin");
		beginTime = System.currentTimeMillis();
		mExtractCustInfo.getCustInfo(policy,tradeInfo);
		if(tradeInfo.hasError()){
			tradeInfo.addError("提取客户数据失败！");
			logger.info("提取客户数据失败！");
			return policy;
		}
		logger.info("提取客户数据成功，提数：" + (System.currentTimeMillis() - beginTime)/1000.0 + "s");

		// 提取代理人数据
		logger.info("agent-begin");
		beginTime = System.currentTimeMillis();
		mExtractAgent.getAgent(policy,tradeInfo);
		if(tradeInfo.hasError()){
			logger.info("提取代理人数据失败！");
			tradeInfo.addError("提取代理人数据失败！");
			return policy;
		}
		logger.info("提取代理人数据成功，提数：" + (System.currentTimeMillis() - beginTime)/1000.0 + "s");

	/*	// 提取险种数据
		logger.info("insurtype-begin");
		beginTime = System.currentTimeMillis();
		mExtractInsurType.getInsurTypeList(policy,tradeInfo);
		if(tradeInfo.hasError()){
			logger.info("提取险种数据失败！");
			tradeInfo.addError("提取险种数据失败！");
			return policy;
		}
		logger.info("提取险种数据成功，提数：" + (System.currentTimeMillis() - beginTime)/1000.0 + "s");
*/
		// 提取累计数据
		logger.info("accum-begin");
		beginTime = System.currentTimeMillis();
		new ExtractAccum().getAccum(policy,tradeInfo);
		if(tradeInfo.hasError()){
			logger.info("提取累计数据失败！");
			tradeInfo.addError("提取累计数据失败！");
			return policy;
		}
		logger.info("提取累计数据成功，提数：" + (System.currentTimeMillis() - beginTime)/1000.0 + "s");

		// 提取既往数据
		logger.info("past-begin");
		beginTime = System.currentTimeMillis();
		mExtractPast.getPast(tradeInfo, policy);
		if(tradeInfo.hasError()){
			logger.info("提取既往数据失败！");
			tradeInfo.addError("提取既往数据失败！");
			return policy;
		}
		logger.info("提取既往数据成功，提数：" + (System.currentTimeMillis() - beginTime)/1000.0 + "s");

		// 提取查重数据
		logger.info("check-begin");
		beginTime = System.currentTimeMillis();
		mExtractCheck.getCheck(tradeInfo, policy);
		if(tradeInfo.hasError()){
			logger.info("提取查重数据失败！");
			tradeInfo.addError("提取查重数据失败！");
			return policy;
		}
		logger.info("提取查重数据成功，提数：" + (System.currentTimeMillis() - beginTime)/1000.0 + "s");

		// 提取告知数据
		logger.debug("disclose-info");
		beginTime = System.currentTimeMillis();
		/*mExtractDisclose.getDisclose(policy,tradeInfo);
		if(tradeInfo.hasError()){
			logger.info("提取告知数据失败！！");
			tradeInfo.addError("提取告知数据失败！");
			return policy;
		}*/
		mExtractNotifyApplicant.getApplicantNotify(policy,tradeInfo);
		mExtractNotifyInsured.getInsuredNotify(policy,tradeInfo);
		mExtractNotifyAgent.getAgentNotify(policy,tradeInfo);

		mExtractTransCodeInfo.getTransCodeInfo(policy,tradeInfo);
		logger.info("提取告知数据成功，提数：" + (System.currentTimeMillis() - beginTime)/1000.0 + "s");

		logger.info(policy.getContNo() + "，RulesEngine提数总耗时："+(System.currentTimeMillis() - start)/1000.0+"s");

		return policy;
	}
}
