package com.sinosoft.cloud.cbs.trules.util;

/**
 * Created by zkr on 2018/8/16.
 */
public enum CredenTypeEnum {
    IdentityCard("0","01"), Passport("1","02"),SoldierCard("2","03"),DrivingCard("3","04"),HomeCard("4","05"),
    StudentCard("5","06"),WorkCard("6","07"),TaiWanCard("7","08"),OtherCard("8","8"),UnCard("9","09"),GoHomeCard("11","10"),
    TemporIdentityCard("12","11"),PoliceCard("13","12"),BirthCard("14","13"),GangAndAoPassCard("15","14"),
    TaiWanPassCard("16","15"),ForeignerLiveCard("17","17");

    CredenTypeEnum(String code, String code_val) {
        this.code = code;
        this.code_val = code_val;
    }

    //IdentityCard
    //转换前的证件类型编码
    private String code;
    //转换后的证件类型编码
    private String code_val;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getCode_val() {
        return code_val;
    }

    public void setCode_val(String code_val) {
        this.code_val = code_val;
    }
    /**
     * 循环遍历枚举类 取得code的转换值
     */

    public static String getValue(String code){
        for (CredenTypeEnum credenTypeEnum : CredenTypeEnum.values()){
            if (credenTypeEnum.getCode().equals(code)){
                return credenTypeEnum.getCode_val();
            }
        }
        return "";
    }
}
