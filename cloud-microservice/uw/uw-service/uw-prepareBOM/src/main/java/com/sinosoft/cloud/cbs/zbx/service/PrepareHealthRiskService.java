package com.sinosoft.cloud.cbs.zbx.service;

import com.sinosoft.cloud.cbs.zbx.data.AccidentBL;
import com.sinosoft.cloud.microservice.AbstractMicroService;
import com.sinosoft.cloud.rest.TradeInfo;
import com.sinosoft.lis.entity.LCContPojo;
import com.sinosoft.lis.entity.LCInsuredPojo;
import com.sinosoft.lis.entity.LCPolPojo;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by sll on 2019/3/28.
 */
@Component("PrepareHealthRiskService")
public class PrepareHealthRiskService extends AbstractMicroService {
    private final Log logger = LogFactory.getLog(this.getClass());

    @Autowired
    AccidentBL accidentBL;


    @Override
    public boolean checkData(TradeInfo tradeInfo) {
        LCContPojo lcContPojo = (LCContPojo) tradeInfo.getData(LCContPojo.class.getName());
        List<LCInsuredPojo> lcInsuredPojos=(List<LCInsuredPojo>)tradeInfo.getData(LCInsuredPojo.class.getName());
        List<LCPolPojo> lcPolPojos=(List<LCPolPojo>)tradeInfo.getData(LCPolPojo.class.getName());
        if(lcContPojo == null){
            logger.debug(this.getClass().getName()+",获取tLCContPojo出错！");
            return false;
        }
        if (lcInsuredPojos == null){
            logger.debug(this.getClass().getName()+",获取LCInsuredList出错！");
            return false;
        }
        if(lcPolPojos == null){
            logger.debug(this.getClass().getName()+",获取LCPolPojoList出错！");
            return false;
        }
        logger.debug("保单号：" + lcContPojo.getContNo() + "，开始核保风险校验!");
        return true;
    }

    @Override
    public TradeInfo dealData(TradeInfo tradeInfo) {
        try{
            tradeInfo=accidentBL.dealData(tradeInfo);
            if (tradeInfo.hasError()){
                logger.debug("调用中保信失败！"+tradeInfo.toString());
               // tradeInfo.addError("调用中保信失败！");
                return tradeInfo;
            }
        }catch (Exception e){
            logger.debug("调用中保信失败！"+tradeInfo.toString());
            return tradeInfo;
        }

        return tradeInfo;
    }
}
