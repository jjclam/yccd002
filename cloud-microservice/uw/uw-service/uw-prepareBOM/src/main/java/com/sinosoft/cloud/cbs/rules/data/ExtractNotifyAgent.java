package com.sinosoft.cloud.cbs.rules.data;

import com.sinosoft.cloud.cbs.rules.bom.*;
import com.sinosoft.cloud.rest.TradeInfo;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * 代理人告知赋值
 * @project: abc-cloud-microservice
 * @author: xiaozehua
 * @date: Created in 下午 3:55 2018/5/22 0022
 * @Description:
 */
@Service
public class ExtractNotifyAgent {
    private final Log logger = LogFactory.getLog(this.getClass());
    public void getAgentNotify(Policy policy, TradeInfo requestInfo){

        logger.debug("代理人告知提数开始");
        long beginTime = System.currentTimeMillis();

        getAgnNotify(policy,requestInfo);

        logger.debug("代理人基础提数：" + (System.currentTimeMillis() - beginTime) / 1000.0
                + "s");

    }
    public void getAgnNotify(Policy policy,TradeInfo requestInfo) {
        //告知提数类对象
        ExtractDisclose eDisclose = new ExtractDisclose();
        //代理人告知对象
        AgentNotify agnNotify = new AgentNotify();

        List clientInfoList = policy.getClientInfoList();

        if (clientInfoList != null && clientInfoList.size() > 0) {
            ClientInfo client = (ClientInfo) clientInfoList.get(0);
            // 获取代理人告知信息


            NotificationInfo agnInfo = eDisclose.getDiscloseExtract(policy, client.getClientNo(), "2",requestInfo,false);


            agnNotify.setClientNo(agnInfo.getClientNo());


            Map map = agnInfo.getNotifyMap();
            if (map != null) {
                Set keys = map.keySet();
                Iterator it = keys.iterator();

                while (it.hasNext()) {
                    String key = (String) it.next();

                    if (key.equals("DLR10_4_Remark")) {
                        /**
                         * 代理人有备注信息-DLR10_4_Remark
                         */
                        boolean flag = true;

                        String str = ((Notify) map.get("DLR10_4_Remark")).getContents();

                        if (str.equals("*")) {
                            flag = false;
                        } else if (str.equals("无")) {
                            flag = false;
                        } else if (str.equals("")) {
                            flag = false;
                        }
                        agnNotify.setAgentRemark(flag);
                    }

                    if (key.equals("DLR2_1-2_YesOrNo")) {
                        /**
                         * 亲眼见过被保人-DLR2_1-2_YesOrNo
                         */
                        agnNotify.setIfMeetInsured(Util.toBoolean(((Notify) map.get("DLR2_1-2_YesOrNo")).getContents()));

                    }

                    /**
                     * 被保人身体有缺陷-DLR3_1-3_YesOrNo
                     */
                    if (key.equals("DLR3_1-3_YesOrNo")) {
                        agnNotify.setInsurdViciousBody(Util.toBoolean(((Notify) map.get("DLR3_1-3_YesOrNo")).getContents()));

                    }


                    /**
                     * 被保险人有危险运动-DLR4_1-4_YesOrNo
                     */
                    if (key.equals("DLR4_1-4_YesOrNo")) {
                        agnNotify.setInsurdangerHobby(Util.toBoolean(((Notify) map.get("DLR4_1-4_YesOrNo")).getContents()));

                    }


                    /**insurdangerHobby
                     * 客户证件与原件一致-DLR6_3-1_YesOrNo
                     */
                    if (key.equals("DLR6_3-1_YesOrNo")) {
                        agnNotify.setCardDisunity(Util.toBoolean(((Notify) map.get("DLR6_3-1_YesOrNo")).getContents()));

                    }


                    /**bigPolByPoor
                     * 客户购买大额保单与经济状况不符-DLR7_3-2_YesOrNo
                     */
                    if (key.equals("DLR7_3-2_YesOrNo")) {
                        agnNotify.setBigPolByPoor(Util.toBoolean(((Notify) map.get("DLR7_3-2_YesOrNo")).getContents()));

                    }


                    /**
                     * 客户购买的保险产品与其需求不符-DLR8_3-3_YesOrNo
                     */
                    if (key.equals("DLR8_3-3_YesOrNo")) {
                        agnNotify.setGoodsNotNeed(Util.toBoolean(((Notify) map.get("DLR8_3-3_YesOrNo")).getContents()));

                    }


                    /**goodsNotNeed
                     * 客户投保目的异常-DLR9_3-4_YesOrNo
                     */
                    if (key.equals("DLR9_3-4_YesOrNo")) {
                        agnNotify.setUnusualPurpose(Util.toBoolean(((Notify) map.get("DLR9_3-4_YesOrNo")).getContents()));

                    }


                }
            }
            policy.setAgentNotify(agnNotify);
        }
    }
}