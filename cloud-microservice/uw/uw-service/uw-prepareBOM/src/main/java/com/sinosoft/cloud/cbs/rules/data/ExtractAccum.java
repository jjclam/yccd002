package com.sinosoft.cloud.cbs.rules.data;

import com.sinosoft.cloud.cbs.rules.bom.Applicant;
import com.sinosoft.cloud.cbs.rules.bom.Insured;
import com.sinosoft.cloud.cbs.rules.bom.Policy;
import com.sinosoft.cloud.rest.TradeInfo;
import com.sinosoft.lis.entity.LCAppntPojo;
import com.sinosoft.lis.entity.LCPolPojo;
import com.sinosoft.lis.entity.RiskAmntInfoPojo;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Author: zhuyiming
 * @Description: 提取累计信息
 * @Date: Created in 18:38 2017/9/24
 */
@Service
public class ExtractAccum {
	private final Log logger = LogFactory.getLog(this.getClass());

	public void getAccum(Policy policy, TradeInfo tradeInfo) {
		logger.debug("老核心提取累计信息");
		List<RiskAmntInfoPojo>  riskAmntInfos = (List<RiskAmntInfoPojo>) tradeInfo.getData(RiskAmntInfoPojo.class.getName());
		List<LCPolPojo> lcPolPojoList = (List) tradeInfo.getData(LCPolPojo.class.getName());

		LCPolPojo tLcpolPojo = lcPolPojoList.get(0);
		if(riskAmntInfos == null || riskAmntInfos.size() == 0){
			logger.debug("老核心提取累计数据失败！");
			tradeInfo.addError("老核心提取累计数据失败！");
			return;
		}
		LCAppntPojo applicant = (LCAppntPojo) tradeInfo.getData(LCAppntPojo.class.getName());
		String relatToInsu = applicant.getRelatToInsu();
		logger.debug("投被保人关系为："+relatToInsu);
		if(!"00".equals(relatToInsu)){
			logger.debug("投被保人不是同一人提取投保人累计");
			// 投保人累计
			getApplicantAccum(policy, riskAmntInfos);
		}
		// 被保人累计
		getInsuredAccum(policy, riskAmntInfos);
		if("7063".equals(tLcpolPojo.getRiskCode()) && "C071".equals(tLcpolPojo.getProdSetCode())){
			List insuredInfoList = policy.getInsuredList();
			Insured insured = (Insured) insuredInfoList.get(0);
			insured.setSumDiseaseAmount((double)tradeInfo.getData("C071H")+tLcpolPojo.getAmnt());
			insured.setDiseaseAmount((double)tradeInfo.getData("C071H")+tLcpolPojo.getAmnt());
		}
	}

	/**
	 * 投保人累计
	 * @param policy
	 * @return
	 */
	public void getApplicantAccum(Policy policy, List<RiskAmntInfoPojo>  riskAmntInfos) {

		// 投保人
		Applicant applicantInfo = policy.getApplicant();

		// 累计类提数
		// 累计寿险风险保额（L）
		applicantInfo.setSumLifeAmount(Util.toDouble((String)riskAmntInfos.get(0).getRiskTypeL()));
		// 累计重疾风险保额（H）
		applicantInfo.setSumDiseaseAmount(Util.toDouble((String)riskAmntInfos.get(0).getRiskTypeH()));

		// 累计意外险风险保额（Y）
		applicantInfo.setSumAccidentAmount(Util.toDouble((String)riskAmntInfos.get(0).getRiskTypeY()));
		// 寿险保单保额（CL）
		applicantInfo.setLifePyAmount(Util.toDouble((String)riskAmntInfos.get(0).getRiskTypeCL()));
		// 累计重疾险保单保额（CH）
		applicantInfo.setSumDisPyAmount(Util.toDouble((String)riskAmntInfos.get(0).getRiskTypeCH()));
		// 意外险保单保额（CY）
		applicantInfo.setAccPyAmount(Util.toDouble((String)riskAmntInfos.get(0).getRiskTypeCY()));
		// 寿险体检风险保额（PL）
		applicantInfo.setSumLifePhyAmount(Util.toDouble((String)riskAmntInfos.get(0).getRiskTypePL()));
		// 人身险保额（LI）
		/*applicantInfo.setPersonAmount(Util.toDouble((String)riskAmntInfos.get(0).getRiskTypeLI()));*/
		// 累计人身险保额（LI）
		applicantInfo.setSumPersonAmount(Util.toDouble((String)riskAmntInfos.get(0).getRiskTypeLI()));
		// 累计风险保额（SR）
		applicantInfo.setSumAmount(Util.toDouble((String)riskAmntInfos.get(0).getRiskTypeSR()));
		// 住院医疗风险保额（M）
		/*applicantInfo.setHospitalizeAmount(Util.toDouble((String)riskAmntInfos.get(0).getRiskTypeM()));*/
		// 意外医疗风险保额（N）
		/*applicantInfo.setAccMedicalAmount(Util.toDouble((String)riskAmntInfos.get(0).getRiskTypeN()));*/
		// 累计津贴保额（J）
		applicantInfo.setSumAcAmount(Util.toDouble((String)riskAmntInfos.get(0).getRiskTypeJ()));
		// 累计自驾车风险保额（ZJ）
		applicantInfo.setSumDriverAmount(Util.toDouble((String)riskAmntInfos.get(0).getRiskTypeZJ()));
		// 自驾车风险保额（ZJ）
		/*applicantInfo.setDriverAmount(applicantInfo.getSumDriverAmount());*/
		// 防癌险风险保额（HC）
		/*applicantInfo.setAnticancerAmount(Util.toDouble((String)riskAmntInfos.get(0).getRiskTypeHC()));*/
		// 累计再保风险保额（ZB）
		applicantInfo.setSumReinsuranceAmount(Util.toDouble((String)riskAmntInfos.get(0).getRiskTypeZB()));

		// 住院医疗风险保额（M）
		applicantInfo.setSumHospitalizeAmount(Util.toDouble((String)riskAmntInfos.get(0).getRiskTypeM()));
		// 累计住院津贴保额（J）
		/*applicantInfo.setSumHospitalizeAcAmount(applicantInfo.getSumAcAmount());*/
		// 住院津贴保额（J）
		/*applicantInfo.setHospitalizeAcAmount(applicantInfo.getSumAcAmount());*/
		// 累计意外医疗风险保额（N）
		applicantInfo.setSumAccMedicalAmount(Util.toDouble((String)riskAmntInfos.get(0).getRiskTypeN()));
		// 累计意外津贴保额（J）
		/*applicantInfo.setSumAccidentAcAmount(applicantInfo.getSumAcAmount());*/
		// 意外津贴保额（J）
		/*applicantInfo.setAccidentAcAmount(applicantInfo.getSumAcAmount());*/
		// 累计防癌险风险保额（HC）
		applicantInfo.setSumAnticancerAmount(Util.toDouble((String)riskAmntInfos.get(0).getRiskTypeHC()));
		// 再保风险保额（ZB）
		/*applicantInfo.setReinsuranceAmount(applicantInfo
				.getSumReinsuranceAmount());*/
		// 累计寿险体检风险保额（PL）
		applicantInfo.setSumLifePhyAmount(applicantInfo.getSumLifePhyAmount());

		// 保单累计类提数

		// 累计寿险保单保额（BL）
		applicantInfo.setSumLifePyAmount(Util.toDouble((String)riskAmntInfos.get(0).getContRiskTypeBL()));
		// 累计健康险保单保额（BH）
		applicantInfo.setSumHealthPyAmount(Util.toDouble((String)riskAmntInfos.get(0).getContRiskTypeBH()));
		// 健康险保单保额（BH）
		/*applicantInfo.setHealthPyAmount(applicantInfo.getSumHealthPyAmount());*/
		// 累计意外险保单保额（BY）
		applicantInfo.setSumAccPyAmount(Util.toDouble((String)riskAmntInfos.get(0).getContRiskTypeBY()));
		// 重疾险保单保额（BH）
		/*applicantInfo.setDisPyAmount(applicantInfo.getSumHealthPyAmount());*/
		// 累计自驾车保单保额（BJ）
		applicantInfo.setSumDriverPyAmount(Util.toDouble((String)riskAmntInfos.get(0).getContRiskTypeBJ()));
		// 自驾车保单保额（BJ）
		/*applicantInfo.setDriverPyAmount(applicantInfo.getSumDriverPyAmount());*/
		// 累计防癌险保单保额（BC）
		applicantInfo.setSumAnticPyAmount(Util.toDouble((String)riskAmntInfos.get(0).getContRiskTypeBC()));
		// 防癌险保单保额（BC）
		/*applicantInfo.setAnticPyAmount(applicantInfo.getSumAnticPyAmount());*/

		// 健康险累计类提数

		// 寿险风险保额（1）
		applicantInfo.setLifeAmount(Util.toDouble((String)riskAmntInfos.get(0).getHeathType1()));
		// 重疾险风险保额（2）
		applicantInfo.setDiseaseAmount(Util.toDouble((String)riskAmntInfos.get(0).getHeathType2()));
		// 意外险风险保额（4）
		applicantInfo.setAccidentAmount(Util.toDouble((String)riskAmntInfos.get(0).getHeathType4()));

		// 身故累计类提数
		// 未成年人身故保额
		applicantInfo.setNonageDieAmount(Util.toDouble((String)riskAmntInfos.get(0).getAppRiskCodeDeath()));

	}

	/**
	 * 被保人累计
	 * 
	 * @param policy
	 * @return
	 */
	public void getInsuredAccum(Policy policy, List<RiskAmntInfoPojo>  riskAmntInfos) {
		// 被保人列表
		List insuredInfoList = policy.getInsuredList();
		for (int i = 0; i < insuredInfoList.size(); i++) {
			Insured insured = (Insured) insuredInfoList.get(i);

			// 累计类提数
			// 累计寿险风险保额（L）
			insured.setSumLifeAmount(Util.toDouble((String)riskAmntInfos.get(i+1).getRiskTypeL()));
			// 累计重疾风险保额（H）
			insured.setSumDiseaseAmount(Util.toDouble((String)riskAmntInfos.get(i+1).getRiskTypeH()));

			// 累计意外险风险保额（Y）
			insured.setSumAccidentAmount(Util.toDouble((String)riskAmntInfos.get(i+1).getRiskTypeY()));
			// 寿险保单保额（CL）
			insured.setLifePyAmount(Util.toDouble((String)riskAmntInfos.get(i+1).getRiskTypeCL()));
			// 累计重疾险保单保额（CH）
			insured.setSumDisPyAmount(Util.toDouble((String)riskAmntInfos.get(i+1).getRiskTypeCH()));
			// 意外险保单保额（CY）
			insured.setAccPyAmount(Util.toDouble((String)riskAmntInfos.get(i+1).getRiskTypeCY()));
			// 寿险体检风险保额（PL）
			insured.setSumLifePhyAmount(Util.toDouble((String)riskAmntInfos.get(i+1).getRiskTypePL()));
			// 人身险保额（LI）
			/*insured.setPersonAmount(Util.toDouble((String)riskAmntInfos.get(i+1).getRiskTypeLI()));*/
			// 累计人身险保额（LI）
			insured.setSumPersonAmount(Util.toDouble((String)riskAmntInfos.get(i+1).getRiskTypeLI()));
			// 累计风险保额（SR）
			insured.setSumAmount(Util.toDouble((String)riskAmntInfos.get(i+1).getRiskTypeSR()));
			// 住院医疗风险保额（M）
			/*insured.setHospitalizeAmount(Util.toDouble((String)riskAmntInfos.get(i+1).getRiskTypeM()));*/
			// 意外医疗风险保额（N）
			/*insured.setAccMedicalAmount(Util.toDouble((String)riskAmntInfos.get(i+1).getRiskTypeN()));*/
			// 累计津贴保额（J）
			insured.setSumAcAmount(Util.toDouble((String)riskAmntInfos.get(i+1).getRiskTypeJ()));
			// 累计自驾车风险保额（ZJ）
			insured.setSumDriverAmount(Util.toDouble((String)riskAmntInfos.get(i+1).getRiskTypeZJ()));
			// 自驾车风险保额（ZJ）
			/*insured.setDriverAmount(insured.getSumDriverAmount());*/
			// 防癌险风险保额（HC）
			/*insured.setAnticancerAmount(Util.toDouble((String)riskAmntInfos.get(i+1).getRiskTypeHC()));*/
			// 累计再保风险保额（ZB）
			insured.setSumReinsuranceAmount(Util.toDouble((String)riskAmntInfos.get(i+1).getRiskTypeZB()));

			// 住院医疗风险保额（M）
			insured.setSumHospitalizeAmount(Util.toDouble((String)riskAmntInfos.get(i+1).getRiskTypeM()));
			// 累计住院津贴保额（J）
			/*insured.setSumHospitalizeAcAmount(insured.getSumAcAmount());*/
			// 住院津贴保额（J）
			/*insured.setHospitalizeAcAmount(insured.getSumAcAmount());*/
			// 累计意外医疗风险保额（N）
			insured.setSumAccMedicalAmount(Util.toDouble((String)riskAmntInfos.get(i+1).getRiskTypeN()));
			// 累计意外津贴保额（J）
			/*insured.setSumAccidentAcAmount(insured.getSumAcAmount());*/
			// 意外津贴保额（J）
			/*insured.setAccidentAcAmount(insured.getSumAcAmount());*/
			// 累计防癌险风险保额（HC）
			insured.setSumAnticancerAmount(Util.toDouble((String)riskAmntInfos.get(i+1).getRiskTypeHC()));
			// 再保风险保额（ZB）
		    /*insured.setReinsuranceAmount(insured
					.getSumReinsuranceAmount());*/
			// 累计寿险体检风险保额（PL）
			insured.setSumLifePhyAmount(insured.getSumLifePhyAmount());

			// 保单累计类提数

			// 累计寿险保单保额（BL）
			insured.setSumLifePyAmount(Util.toDouble((String)riskAmntInfos.get(i+1).getContRiskTypeBL()));
			// 累计健康险保单保额（BH）
			insured.setSumHealthPyAmount(Util.toDouble((String)riskAmntInfos.get(i+1).getContRiskTypeBH()));
			// 健康险保单保额（BH）
			/*insured.setHealthPyAmount(insured.getSumHealthPyAmount());*/
			// 累计意外险保单保额（BY）
			insured.setSumAccPyAmount(Util.toDouble((String)riskAmntInfos.get(i+1).getContRiskTypeBY()));
			// 重疾险保单保额（BH）
			/*insured.setDisPyAmount(insured.getSumHealthPyAmount());*/
			// 累计自驾车保单保额（BJ）
			insured.setSumDriverPyAmount(Util.toDouble((String)riskAmntInfos.get(i+1).getContRiskTypeBJ()));
			// 自驾车保单保额（BJ）
			/*insured.setDriverPyAmount(insured.getSumDriverPyAmount());*/
			// 累计防癌险保单保额（BC）
			insured.setSumAnticPyAmount(Util.toDouble((String)riskAmntInfos.get(i+1).getContRiskTypeBC()));
			// 防癌险保单保额（BC）
			/*insured.setAnticPyAmount(insured.getSumAnticPyAmount());*/

			// 健康险累计类提数

			// 寿险风险保额（1）
			insured.setLifeAmount(Util.toDouble((String)riskAmntInfos.get(i+1).getHeathType1()));
			// 重疾险风险保额（2）
			insured.setDiseaseAmount(Util.toDouble((String)riskAmntInfos.get(i+1).getHeathType2()));
			// 意外险风险保额（4）
			insured.setAccidentAmount(Util.toDouble((String)riskAmntInfos.get(i+1).getHeathType4()));

			// 身故累计类提数
			// 未成年人身故保额
			insured.setNonageDieAmount(Util.toDouble((String)riskAmntInfos.get(i+1).getAppRiskCodeDeath()));
		}
	}

}
