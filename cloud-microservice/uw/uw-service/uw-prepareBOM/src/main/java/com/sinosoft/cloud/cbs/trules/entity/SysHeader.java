package com.sinosoft.cloud.cbs.trules.entity;

/**
 * @Author:wangshuliang
 * @Description:
 * @Date:Created in 17:56 2018/6/25
 * @Modified by:
 */
public class SysHeader {
    private String standardVersionCode;
    private String msgDate;
    private String msgTime;
    private String systemCode;
    private String transactionCode;
    private String transRefGUID;
    private String esbRefGUID;
    private String transNo;
    private String businessCode;
    private String businessType;

    public String getStandardVersionCode() {
        return standardVersionCode;
    }

    public void setStandardVersionCode(String standardVersionCode) {
        this.standardVersionCode = standardVersionCode;
    }

    public String getMsgDate() {
        return msgDate;
    }

    public void setMsgDate(String msgDate) {
        this.msgDate = msgDate;
    }

    public String getMsgTime() {
        return msgTime;
    }

    public void setMsgTime(String msgTime) {
        this.msgTime = msgTime;
    }

    public String getSystemCode() {
        return systemCode;
    }

    public void setSystemCode(String systemCode) {
        this.systemCode = systemCode;
    }

    public String getTransactionCode() {
        return transactionCode;
    }

    public void setTransactionCode(String transactionCode) {
        this.transactionCode = transactionCode;
    }

    public String getTransRefGUID() {
        return transRefGUID;
    }

    public void setTransRefGUID(String transRefGUID) {
        this.transRefGUID = transRefGUID;
    }

    public String getEsbRefGUID() {
        return esbRefGUID;
    }

    public void setEsbRefGUID(String esbRefGUID) {
        this.esbRefGUID = esbRefGUID;
    }

    public String getTransNo() {
        return transNo;
    }

    public void setTransNo(String transNo) {
        this.transNo = transNo;
    }

    public String getBusinessCode() {
        return businessCode;
    }

    public void setBusinessCode(String businessCode) {
        this.businessCode = businessCode;
    }

    public String getBusinessType() {
        return businessType;
    }

    public void setBusinessType(String businessType) {
        this.businessType = businessType;
    }

    @Override
    public String toString() {
        return "SysHeader{" +
                "standardVersionCode='" + standardVersionCode + '\'' +
                ", msgDate='" + msgDate + '\'' +
                ", msgTime='" + msgTime + '\'' +
                ", systemCode='" + systemCode + '\'' +
                ", transactionCode='" + transactionCode + '\'' +
                ", transRefGUID='" + transRefGUID + '\'' +
                ", esbRefGUID='" + esbRefGUID + '\'' +
                ", transNo='" + transNo + '\'' +
                ", businessCode='" + businessCode + '\'' +
                ", businessType='" + businessType + '\'' +
                '}';
    }
}
