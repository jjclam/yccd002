package com.sinosoft.cloud.cbs.rules.data;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;


import com.sinosoft.cloud.cbs.rules.bom.Policy;
import com.sinosoft.cloud.cbs.rules.bom.Risk;
import com.sinosoft.cloud.cbs.rules.result.Result;
import com.sinosoft.cloud.cbs.rules.result.UwResult;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class Util {
	private static Log logger = LogFactory.getLog(Util.class);
	private static ExeSQL exeSql = new ExeSQL();
	private static SSRS tSSRS = new SSRS();
	/**
	 * 判断sql的查询结果是否存在值
	 * @param
	 * @return
	 */
	public static boolean isExist(VData tVData) {
		tSSRS = exeSql.execSQL(tVData);
		if(null != tSSRS && tSSRS.MaxRow > 0){
			return true;
		}
		return false;
	}
	
	/**
	 * 获取sql返回的值
	 * @param
	 * @return
	 */
	public static String getValue(VData tVData) {
		String value = "";
		tSSRS = exeSql.execSQL(tVData);
		if(null != tSSRS && tSSRS.MaxRow > 0){
			value = tSSRS.GetText(1, 1);
		}
		return value;
	}
	
/*	*//**
	 * 通过出生日期计算年龄
	 * 周岁＝今年-出生年（已过生日）（未过生日还要-1）
	 * @param
	 * @return
	 *//*
	public static int getAge(Date birthDate) {
		int age = 0;

		if (birthDate != null) {
			Date now = new Date();
			
			SimpleDateFormat format_y = new SimpleDateFormat("yyyy");
			SimpleDateFormat format_M = new SimpleDateFormat("MM");
			
			String birth_year = format_y.format(birthDate);
			String this_year = format_y.format(now);
			
			String birth_month = format_M.format(birthDate);
			String this_month = format_M.format(now);
			
			age = Integer.parseInt(this_year) - Integer.parseInt(birth_year);
			
			//如果未到出生月份，则 age - 1
			if(this_month.compareTo(birth_month) < 0) {
				age -= 1;
			}
			if(age < 0) {
				age = 0;
			}
		}
		return age;
	} */
	
	
	
	
	//将String转换为double
	public static double toDouble(String str) {
		try {
			return Double.parseDouble(str);
		}catch(NumberFormatException ex) {
			return 0;
		}
	}
	
	//将String转换为int
	public static int toInt(String str) {
		try {
			return Integer.parseInt(str);
		}catch(NumberFormatException ex) {
			return 0;
		}
	}

	/**
	 * 将String转换为boolean
	 *
	 * @param str
	 * @return
	 */
	public static boolean toBoolean(String str) {
		if (str.equals("*")) {
			return false;
		} else if (str.equals("无")) {
			return false;
		} else if (str.equals("")) {
			return false;
		} else if (str.equals("2")) {
			return false;
		}else if (str.equals("否")) {
			return false;
		}
		return true;
	}
	/**
	 * 把String转换为Date
	 * @param str
	 * @return
	 */
	public static Date toDate(String str){
		Date date=new Date();
		if (str ==null || "".equals(str)){
			return null;
		}
		SimpleDateFormat outDate =new SimpleDateFormat("yyyy-MM-dd");
		try {
			date=outDate.parse(str);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return date;
	}
	//&#x编码转换成汉字（java）
	public static String  unescape (String src){
		StringBuffer tmp = new StringBuffer();
		tmp.ensureCapacity(src.length());
		int  lastPos=0,pos=0;
		char ch;

		while (lastPos<src.length()){
			pos = src.indexOf("%",lastPos);
			if (pos == lastPos){
				if (src.charAt(pos+1)=='u'){
					ch = (char)Integer.parseInt(src.substring(pos+2,pos+6),16);
					tmp.append(ch);
					lastPos = pos+6;
				}else{
					ch = (char)Integer.parseInt(src.substring(pos+1,pos+3),16);
					tmp.append(ch);
					lastPos = pos+3;
				}
			} else{

				if (pos == -1){
					tmp.append(src.substring(lastPos));
					lastPos=src.length();
				} else{
					tmp.append(src.substring(lastPos,pos));
					lastPos=pos;
				}
			}
		}
		return tmp.toString();
	}

	public static boolean judgeCode(Policy policy, String type, String code){
		boolean flag = false;

		List risks = policy.getRiskList();
				//System.out.println("险种个数-------------"+risks.size());
		if(type.equals("prodSetCode")){

			for(int i = 0;i<risks.size();i++){
				Risk risk = (Risk)risks.get(i);

					if (code.equals(risk.getProdSetCode())) {
						flag = true;
					}

			}

		}else if(type.equals("riskcode")){


			for(int i = 0;i<risks.size();i++){
				Risk risk = (Risk)risks.get(i);
//					    	System.out.println("当前险种-------------"+risk.getRiskCode());
				if(risk.getRiskCode().equals(code)){
					flag = true;
				}
			}
		}
		return flag;
	}



	/**
	 * 打印规则执行结果信息
	 */
	public static void print(Result result) {
		//规则返回结果集
		List list = result.getUwresultList();
		//预处理中设置的键值对
		Map ruleDecisionMap = result.getRuleDecisionMap();
		logger.debug("》》》核保结果："+result.isFlag()+"    "+result.getMessage());
		logger.debug("=============预处理中设置的规则执行状态=================================================");
		Iterator it = ruleDecisionMap.keySet().iterator();
		while(it.hasNext()) {
			String key = it.next().toString();
			logger.debug("》规则编码："+key+"\n     是否执行规则："+ruleDecisionMap.get(key));
		}
		logger.debug("=============规则返回结果集=========================================================");
		if(list!=null) {
			for(int i=0; i<list.size(); i++) {
				UwResult vr = (UwResult) list.get(i);
				logger.debug("》规则编码："+vr.getRuleCode()+"\n     规则描述信息："+vr.getReturnInfo()+"\n     险种编码："+vr.getRiskCode()+"\n     规则校验结果："+vr.isFlag());
			}
		}
		logger.debug("================================================================================");
	}
}
