package com.sinosoft.cloud.cbs.rules.bom;

/**
 * 投保人告知类
 * @project: abc-cloud-microservice
 * @author: xiaozehua
 * @date: Created in 下午 2:43 2018/5/22 0022
 * @Description:
 */
public class ApplicantNotify {
    /**
     * 客户号
     */
    private String clientNo;

    /**
     * (银行代理保险投保书)有异常投保经历-GX2_1_YesOrNo
     */
    private boolean	ydUnusualPolicyPast   ;
    /**
     * (银行代理保险投保书)有危险运动爱好-GX3_2_YesOrNo
     */
    private boolean	ybdangerSportHobby    ;
    /**
     * 保费预算（元）	01_016_Budget
     */
    private double	premiumBudget         ;
    /**
     * 目前或曾经有阴道异常出血畸形瘤葡萄胎子宫肌瘤卵巢囊肿或乳房肿块等	GX11_19B_YesOrNo
     */
    private boolean	gynecologicDiseases   ;
    /**
     * 年收入(万元)	01_001_AnnualIncome
     */
    private String	annualIncome          ;
    /**
     * 除本投保申请外已经购买或目前正在申请除人寿外的其他保险公司的人身保险	GX1_2_YesOrNo
     */
    private boolean	hasOtherComGoods      ;
    /**
     * 目前或曾经服用过成瘾药物	GX10_12_YesOrNo
     */
    private boolean	addictedDrugs       ;
    /**
     * (个人人身保险投保书)过去一年去过医院就诊、服药、手术或其他治疗	GX10_14_YesOrNo
     */
    private boolean	goToTheHospital     ;
    /**
     * (个人人身保险投保书)过去三年有医院检查（包括健康体检）结果为异常	GX10_15_YesOrNo
     */
    private boolean	lisUnusualDiagnose3Y  ;
    /**
     * 过去五年曾住院检查或治疗	GX10_16_YesOrNo
     */
    private boolean	livedHospital5Y       ;
    /**
     * (银行代理保险投保书)目前或曾经患有告知列疾病、症状或接受任何治疗	GX10_7_YesOrNo
     */
    private boolean	ydHasThisOfDisease    ;
    /**
     * (银行代理保险投保书)备注栏有内容	GX13_10_Remark
     */
    private boolean	noteColumn            ;
    /**
     * (个人人身保险投保书)有异常投保经历	GX2_3_YesOrNo
     */
    private boolean	lisUnusualPolicyPast  ;
    /**
     * (个人人身保险投保书)有危险运动爱好	GX3_4_YesOrNo
     */
    private boolean	lisdangerSportHobby   ;
    /**
     * 1年内计划出国	GX4_5_YesOrNo
     */
    private boolean	planGoAbroad1Y        ;
    /**
     * 为职业司机或拥有摩托车驾照	GX5_6_YesOrNo
     */
    private boolean	professionalDriver    ;
    /**
     * 每天抽烟量（支）	GX6_09_HowMany
     */
    private double	smokeFew;
    /**
     * 烟龄	GX6_09_SmokeYear
     */
    private double	lengthAsSmoker        ;
    /**
     * 有吸烟史	GX6_09_YesOrNo
     */
    private boolean	historyOfSmoking    ;
    /**
     * 目前或曾经有饮白酒、洋酒等烈性酒的习惯	GX7_11_YesOrNo
     */
    private boolean	historyOfDrinking   ;
    /**
     * 有智能障碍或失明聋哑及言语咀嚼障碍	GX8_18_YesOrNo
     */
    private boolean	sufferMentalDisorders;
    /**
     * 父母子女兄弟姐妹患有癌症心脑血管疾病白血病血友病糖尿病多囊肝多囊肾肠息肉或其他遗传性疾病等	GX9_21_YesOrNo
     */
    private boolean	familyHistoryOfDisease;
    /**
     * (银行代理保险投保书)过去一年去过医院就诊、服药、手术或其他治疗	GX10_4_YesOrNo
     */
    private boolean	ydGoHospitalLastYear  ;
    /**
     * (银行代理保险投保书)过去三年有医院检查（包括健康体检）结果为异常	GX10_5_YesOrNo
     */
    private boolean	ydUnusualDiagnose3Y   ;
    /**
     * (银行代理保险投保书)曾住院检查或治疗	GX10_6_YesOrNo
     */
    private boolean	ydLivedHospitalPast   ;
    /**
     * 曾被建议重复宫颈涂片检查或乳房超声、X光、活检等	GX11_18C_YesOrNo
     */
    private boolean	gynecologialed        ;


    /**
     * 被保人患智能障碍、恶性肿瘤等或从事所列职业	GX10_0_YesOrNo
     */
    private  boolean	hasGrediseaseOrjob      ;

    /**
     * 有身体残疾或功能障碍	GX10_3_YesOrNo
     */
    private  boolean	hasBodyDis              ;
    /**
     * 客户或客户的配偶为艾滋病病毒感染者或准备接受艾滋病病毒检测	GX10_8_YesOrNo
     */
    private  boolean	haveAIDS            ;

    /**
     * 早产过期产难产或其他先天疾病	GX12_20B_YesOrNo
     */
    private  boolean	abnormalityOfChildren ;

    /**
     * (银行代理保险投保书)有备注	GX13_C_Remark
     */
    private  boolean	cRemarks              ;
    /**
     * 健康告知备注	GX13_21_Remark
     */
    private  boolean	helthRemark;




    public boolean isYdUnusualPolicyPast() {
        return ydUnusualPolicyPast;
    }
    public void setYdUnusualPolicyPast(boolean ydUnusualPolicyPast) {
        this.ydUnusualPolicyPast = ydUnusualPolicyPast;
    }
    public boolean isYbdangerSportHobby() {
        return ybdangerSportHobby;
    }
    public void setYbdangerSportHobby(boolean ybdangerSportHobby) {
        this.ybdangerSportHobby = ybdangerSportHobby;
    }
    public double getPremiumBudget() {
        return premiumBudget;
    }
    public void setPremiumBudget(double premiumBudget) {
        this.premiumBudget = premiumBudget;
    }
    public boolean isGynecologicDiseases() {
        return gynecologicDiseases;
    }
    public void setGynecologicDiseases(boolean gynecologicDiseases) {
        this.gynecologicDiseases = gynecologicDiseases;
    }

    public boolean isHasOtherComGoods() {
        return hasOtherComGoods;
    }
    public void setHasOtherComGoods(boolean hasOtherComGoods) {
        this.hasOtherComGoods = hasOtherComGoods;
    }

    public boolean isLisUnusualDiagnose3Y() {
        return lisUnusualDiagnose3Y;
    }
    public void setLisUnusualDiagnose3Y(boolean lisUnusualDiagnose3Y) {
        this.lisUnusualDiagnose3Y = lisUnusualDiagnose3Y;
    }
    public boolean isLivedHospital5Y() {
        return livedHospital5Y;
    }
    public void setLivedHospital5Y(boolean livedHospital5Y) {
        this.livedHospital5Y = livedHospital5Y;
    }
    public boolean isYdHasThisOfDisease() {
        return ydHasThisOfDisease;
    }
    public void setYdHasThisOfDisease(boolean ydHasThisOfDisease) {
        this.ydHasThisOfDisease = ydHasThisOfDisease;
    }
    public boolean isNoteColumn() {
        return noteColumn;
    }
    public void setNoteColumn(boolean noteColumn) {
        this.noteColumn = noteColumn;
    }
    public boolean isLisUnusualPolicyPast() {
        return lisUnusualPolicyPast;
    }
    public void setLisUnusualPolicyPast(boolean lisUnusualPolicyPast) {
        this.lisUnusualPolicyPast = lisUnusualPolicyPast;
    }
    public boolean isLisdangerSportHobby() {
        return lisdangerSportHobby;
    }
    public void setLisdangerSportHobby(boolean lisdangerSportHobby) {
        this.lisdangerSportHobby = lisdangerSportHobby;
    }
    public boolean isPlanGoAbroad1Y() {
        return planGoAbroad1Y;
    }
    public void setPlanGoAbroad1Y(boolean planGoAbroad1Y) {
        this.planGoAbroad1Y = planGoAbroad1Y;
    }
    public boolean isProfessionalDriver() {
        return professionalDriver;
    }
    public void setProfessionalDriver(boolean professionalDriver) {
        this.professionalDriver = professionalDriver;
    }
    public double getSmokeFew() {
        return smokeFew;
    }
    public void setSmokeFew(double smokeFew) {
        this.smokeFew = smokeFew;
    }
    public double getLengthAsSmoker() {
        return lengthAsSmoker;
    }
    public void setLengthAsSmoker(double lengthAsSmoker) {
        this.lengthAsSmoker = lengthAsSmoker;
    }

    public boolean isYdGoHospitalLastYear() {
        return ydGoHospitalLastYear;
    }
    public void setYdGoHospitalLastYear(boolean ydGoHospitalLastYear) {
        this.ydGoHospitalLastYear = ydGoHospitalLastYear;
    }
    public boolean isYdUnusualDiagnose3Y() {
        return ydUnusualDiagnose3Y;
    }
    public void setYdUnusualDiagnose3Y(boolean ydUnusualDiagnose3Y) {
        this.ydUnusualDiagnose3Y = ydUnusualDiagnose3Y;
    }
    public boolean isYdLivedHospitalPast() {
        return ydLivedHospitalPast;
    }
    public void setYdLivedHospitalPast(boolean ydLivedHospitalPast) {
        this.ydLivedHospitalPast = ydLivedHospitalPast;
    }
    public boolean isGynecologialed() {
        return gynecologialed;
    }
    public void setGynecologialed(boolean gynecologialed) {
        this.gynecologialed = gynecologialed;
    }
    public String getClientNo() {
        return clientNo;
    }
    public void setClientNo(String clientNo) {
        this.clientNo = clientNo;
    }
    public boolean isAddictedDrugs() {
        return addictedDrugs;
    }
    public void setAddictedDrugs(boolean addictedDrugs) {
        this.addictedDrugs = addictedDrugs;
    }
    public boolean isGoToTheHospital() {
        return goToTheHospital;
    }
    public void setGoToTheHospital(boolean goToTheHospital) {
        this.goToTheHospital = goToTheHospital;
    }
    public boolean isHistoryOfSmoking() {
        return historyOfSmoking;
    }
    public void setHistoryOfSmoking(boolean historyOfSmoking) {
        this.historyOfSmoking = historyOfSmoking;
    }
    public boolean isHistoryOfDrinking() {
        return historyOfDrinking;
    }
    public void setHistoryOfDrinking(boolean historyOfDrinking) {
        this.historyOfDrinking = historyOfDrinking;
    }
    public boolean isSufferMentalDisorders() {
        return sufferMentalDisorders;
    }
    public void setSufferMentalDisorders(boolean sufferMentalDisorders) {
        this.sufferMentalDisorders = sufferMentalDisorders;
    }
    public boolean isFamilyHistoryOfDisease() {
        return familyHistoryOfDisease;
    }
    public void setFamilyHistoryOfDisease(boolean familyHistoryOfDisease) {
        this.familyHistoryOfDisease = familyHistoryOfDisease;
    }
    public boolean isHasGrediseaseOrjob() {
        return hasGrediseaseOrjob;
    }
    public void setHasGrediseaseOrjob(boolean hasGrediseaseOrjob) {
        this.hasGrediseaseOrjob = hasGrediseaseOrjob;
    }
    public boolean isHasBodyDis() {
        return hasBodyDis;
    }
    public void setHasBodyDis(boolean hasBodyDis) {
        this.hasBodyDis = hasBodyDis;
    }
    public boolean isHaveAIDS() {
        return haveAIDS;
    }
    public void setHaveAIDS(boolean haveAIDS) {
        this.haveAIDS = haveAIDS;
    }
    public boolean isAbnormalityOfChildren() {
        return abnormalityOfChildren;
    }
    public void setAbnormalityOfChildren(boolean abnormalityOfChildren) {
        this.abnormalityOfChildren = abnormalityOfChildren;
    }
    public boolean isCRemarks() {
        return cRemarks;
    }
    public void setcRemarks(boolean cRemarks) {
        this.cRemarks = cRemarks;
    }
    public boolean isHelthRemark() {
        return helthRemark;
    }
    public void setHelthRemark(boolean helthRemark) {
        this.helthRemark = helthRemark;
    }
    public String getAnnualIncome() {
        return annualIncome;
    }
    public void setAnnualIncome(String annualIncome) {
        this.annualIncome = annualIncome;
    }


}