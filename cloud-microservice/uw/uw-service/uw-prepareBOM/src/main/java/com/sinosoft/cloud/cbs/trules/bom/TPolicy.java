package com.sinosoft.cloud.cbs.trules.bom;

import java.util.List;

/**
 * @Author:wangshuliang
 * @Description:
 * @Date:Created in 16:46 2018/6/8
 * @Modified by:
 */
public class TPolicy {
    //被保人既往信息
    private LcInsured LcInsured;
    //产品信息
    private Product Product;
    //代理人信息
    private TAgentInfo TAgentInfo;
    //投保人信息
    private TApplicantInfo TApplicantInfo;
    //受益人信息
    private List<TBeneficiaryInfo> TBeneficiaryInfos;
    //被保人信息
    private TInsurdInfo TInsurdInfo;
    //付款信息
    private TPayInfo TPayInfo;
    //合同信息
    private TProductInfo TProductInfo;
    //产品组合信息
    private ProdGroup prodGroup;

    public LcInsured getLcInsured() {
        return LcInsured;
    }

    public void setLcInsured(LcInsured lcInsured) {
        LcInsured = lcInsured;
    }

    public Product getProduct() {
        return Product;
    }

    public void setProduct(Product product) {
        Product = product;
    }

    public TAgentInfo getTAgentInfo() {
        return TAgentInfo;
    }

    public void setTAgentInfo(TAgentInfo TAgentInfo) {
        this.TAgentInfo = TAgentInfo;
    }

    public TApplicantInfo getTApplicantInfo() {
        return TApplicantInfo;
    }

    public void setTApplicantInfo(TApplicantInfo TApplicantInfo) {
        this.TApplicantInfo = TApplicantInfo;
    }

    public List<TBeneficiaryInfo> getTBeneficiaryInfos() {
        return TBeneficiaryInfos;
    }

    public void setTBeneficiaryInfos(List<TBeneficiaryInfo> TBeneficiaryInfos) {
        this.TBeneficiaryInfos = TBeneficiaryInfos;
    }

    public TInsurdInfo getTInsurdInfo() {
        return TInsurdInfo;
    }

    public void setTInsurdInfo(TInsurdInfo TInsurdInfo) {
        this.TInsurdInfo = TInsurdInfo;
    }

    public TPayInfo getTPayInfo() {
        return TPayInfo;
    }

    public void setTPayInfo(TPayInfo TPayInfo) {
        this.TPayInfo = TPayInfo;
    }

    public TProductInfo getTProductInfo() {
        return TProductInfo;
    }

    public void setTProductInfo(TProductInfo TProductInfo) {
        this.TProductInfo = TProductInfo;
    }

    public ProdGroup getProdGroup() {
        return prodGroup;
    }

    public void setProdGroup(ProdGroup prodGroup) {
        this.prodGroup = prodGroup;
    }
}
