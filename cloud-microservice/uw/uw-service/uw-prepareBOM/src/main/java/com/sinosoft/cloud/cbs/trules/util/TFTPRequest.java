package com.sinosoft.cloud.cbs.trules.util;

import com.sinosoft.cloud.cbs.rules.util.FTPRequest;
import com.sinosoft.cloud.cbs.rules.util.UploadFtpClient;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

/**
 * Created by zkr on 2018/7/31.
 */
@Service
@Scope("prototype")
public class TFTPRequest implements Runnable{
    private Log logger = LogFactory.getLog(FTPRequest.class);
    private SimpleDateFormat format1 = new SimpleDateFormat("yyyyMMddHHmmss");
    private SimpleDateFormat format2 = new SimpleDateFormat("yyyy-MM-dd");
    //打印号
    private String prtno;
    //请求或响应报文
    private String xml;
    //请求或响应标识
    private String flag;
    //当前时间
    private Date date;
    /** ftp链接 */
    @Value("${cloud.tengine.ftpServer}")
    private String server;
    /** ftp用户名 */
    @Value("${cloud.tengine.ftpUserName}")
    private String userName;
    /** ftp密码 */
    @Value("${cloud.tengine.ftpPassword}")
    private String userPassword;
    /** ftp请求报文地址 */
    @Value("${cloud.tengine.ftpRequestXmlPath}")
    private String reqXmlPath;
    @Value("${cloud.tengine.ftpResponseXmlPath}")
    private String respXmlPath;

    public TFTPRequest() {

    }

	/*public FTPRequest(String prtno, String xml, String flag, Date date) {
		this.prtno = prtno;
		this.xml = xml;
		this.flag = flag;
		this.date = date;
		format1.setTimeZone(TimeZone.getTimeZone("Asia/Shanghai"));
		format2.setTimeZone(TimeZone.getTimeZone("Asia/Shanghai"));
	}*/

    public void init(String prtno, String xml, String flag, Date date) {
        this.prtno = prtno;
        this.xml = xml;
        this.flag = flag;
        this.date = date;
        format1.setTimeZone(TimeZone.getTimeZone("Asia/Shanghai"));
        format2.setTimeZone(TimeZone.getTimeZone("Asia/Shanghai"));
    }

    public void run() {
        long startTime = System.currentTimeMillis();

		/*Properties props = null;
		try {
			props = CUtil.getPropertiesByFileName("Rule.properties");
		} catch (AMQAppException e) {
			e.printStackTrace();
			logger.error(ExceptionUtils.exceptionToString(e));
		}*/
		/*String server = CUtil.getProperty("ftpServer", props);
		String userName = CUtil.getProperty("ftpUserName", props);
		String userPassword = CUtil.getProperty("ftpPassword", props);*/
        String path = "";
        if("request".equals(flag)) {
            path = reqXmlPath + "/" + format2.format(date);
            //zym 2017.10.17
//			path = CUtil.getProperty("ftpRequestXmlPath", props);
        }else if("response".equals(flag)){
            path = respXmlPath + "/" + format2.format(date);
            //zym 2017.10.17
//			path = CUtil.getProperty("ftpResponseXmlPath", props);
        }
        String fileName = prtno + "_" + format1.format(date);
        //zym 2017.10.17
//		String fileName = prtno;
        //上传FTP
        UploadFtpClient.uploadFile(new StringBuffer(xml), server, userName, userPassword, path, fileName);

        logger.debug("FTP耗时："+(System.currentTimeMillis()-startTime)+"ms");
    }
}
