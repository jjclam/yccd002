package com.sinosoft.cloud.cbs.uw;

import com.sinosoft.cloud.bl.AbstractBL;
import com.sinosoft.cloud.cbs.rules.result.Result;
import com.sinosoft.cloud.cbs.rules.result.UwResult;
import com.sinosoft.cloud.cbs.trules.bom.*;
import com.sinosoft.cloud.cbs.trules.data.TEngineBL;
import com.sinosoft.cloud.rest.TradeInfo;
import com.sinosoft.lis.entity.*;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.utility.Reflections;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author:wangshuliang
 * @Description:
 * @Date:Created in 10:18 2018/6/11
 * @Modified by:
 */
@Component
public class TUWByEngineBL extends AbstractBL {
    private final Log logger = LogFactory.getLog(this.getClass());
    @Autowired
    private TEngineBL tEngineBL;

    @Override
    public boolean checkData(TradeInfo tradeInfo) {
        //校验是否存在主险
        List<LCPolPojo> tLCPolPojoList = (List) tradeInfo.getData(LCPolPojo.class.getName());
        boolean checkMainRisk = false;
        for (int i = 0; i < tLCPolPojoList.size(); i++) {
            if (tLCPolPojoList.get(i).getPolNo().equals(
                    tLCPolPojoList.get(i).getMainPolNo())) {
                checkMainRisk = true;
                break;
            }
        }

        if (!checkMainRisk) {
            logger.debug(this.getClass().getName() + ",该投保单不存在主险信息！");
            tradeInfo.addError(this.getClass().getName() + ",该投保单不存在主险信息！");
            return false;
        }
        return true;
    }

    @Override
    public TradeInfo dealData(TradeInfo tradeInfo) {
        //核保提数
        tEngineBL.dealData(tradeInfo);
        if (tradeInfo.hasError()) {
            logger.debug("核保提数失败" + tradeInfo.toString());
            tradeInfo.addError("核保提数失败");
            return  tradeInfo;
        }
        //处理获取到的核保结论
        dealODMData(tradeInfo);
        return tradeInfo;
    }
    /**
     * @Author: zhuyiming
     * @Description: 处理获取到的核保结论
     * @Date: Created in 16:03 2017/9/22
     */
    public boolean dealODMData(TradeInfo tradeInfo) {
        //执行保单级规则存储
        prepareContRules(tradeInfo);
        //险种级核保规则存储
        preparePolRules(tradeInfo);
        //将核保结论进行输出
        OutputData(tradeInfo);

        return true;
    }

    /**
     * @Author: zhuyiming
     * @Description: 执行保单级规则存储,存储LC表
     * @Date: Created in 16:03 2017/9/22
     */
    public boolean prepareContRules(TradeInfo tradeInfo) {
        String tCurrentDate = PubFun.getCurrentDate();
        String tCurrentTime = PubFun.getCurrentTime();
        Reflections tReflections = new Reflections();
        logger.debug("执行保单级规则存储开始");
        String tContPassFlag = null;
        Result tODMResult = (Result) tradeInfo.getData(Result.class.getName());
        if (tODMResult==null){
            tContPassFlag = "5";
        }else{

            if(tODMResult.isFlag()){
                tContPassFlag = "9";
            }else {
                tContPassFlag = "5";
            }
        }
        LCContPojo tLCContPojo = (LCContPojo) tradeInfo.getData(LCContPojo.class.getName());
        //ODM规则引擎改变保单表LCContPojo
        logger.debug("ODM规则引擎改变保单表LCContPojo");
        tLCContPojo.setApproveFlag("9");
        tLCContPojo.setApproveCode("ABC-CLOUD");
        tLCContPojo.setApproveDate(tCurrentDate);
        tLCContPojo.setApproveTime(tCurrentTime);
        tLCContPojo.setUWFlag(tContPassFlag);
        tLCContPojo.setUWOperator("ABC-CLOUD");
        tLCContPojo.setUWDate(tCurrentDate);
        tLCContPojo.setUWTime(tCurrentTime);
        tLCContPojo.setModifyDate(tCurrentDate);
        tLCContPojo.setModifyTime(tCurrentTime);
        //合同核保主表C表
        logger.debug("合同核保主表C表");
        LCCUWMasterPojo tLCCUWMasterPojo = (LCCUWMasterPojo) tradeInfo.getData(LCCUWMasterPojo.class.getName());

        if (tLCCUWMasterPojo == null) {
            tLCCUWMasterPojo = new LCCUWMasterPojo();
            String tCUWMasterID = PubFun1.CreateMaxNo(PubFun1.PhysicNo, 22);
            tLCCUWMasterPojo.setCUWMasterID(tCUWMasterID);
            tLCCUWMasterPojo.setShardingID(tLCContPojo.getShardingID());
            tLCCUWMasterPojo.setContID(tLCContPojo.getContID());
            tLCCUWMasterPojo.setContNo(tLCContPojo.getContNo());
            tLCCUWMasterPojo.setGrpContNo(tLCContPojo.getGrpContNo());
            tLCCUWMasterPojo.setProposalContNo(tLCContPojo.getProposalContNo());
            tLCCUWMasterPojo.setUWNo(1);
            tLCCUWMasterPojo.setInsuredNo(tLCContPojo.getInsuredNo());
            tLCCUWMasterPojo.setInsuredName(tLCContPojo.getInsuredName());
            tLCCUWMasterPojo.setAppntNo(tLCContPojo.getAppntNo());
            tLCCUWMasterPojo.setAppntName(tLCContPojo.getAppntName());
            tLCCUWMasterPojo.setAgentCode(tLCContPojo.getAgentCode());
            tLCCUWMasterPojo.setAgentGroup(tLCContPojo.getAgentGroup());
            tLCCUWMasterPojo.setUWGrade("A1");//核保级别
            tLCCUWMasterPojo.setAppGrade("A1");//申报级别
            tLCCUWMasterPojo.setPostponeDay("");
            tLCCUWMasterPojo.setPostponeDate("");
            tLCCUWMasterPojo.setAutoUWFlag("1");//1--自动核保  2--人工核保
            tLCCUWMasterPojo.setState(tContPassFlag);
            tLCCUWMasterPojo.setPassFlag(tContPassFlag);
            tLCCUWMasterPojo.setHealthFlag("0");
            tLCCUWMasterPojo.setSpecFlag("0");
            tLCCUWMasterPojo.setQuesFlag("0");
            tLCCUWMasterPojo.setReportFlag("0");
            tLCCUWMasterPojo.setChangePolFlag("0");
            tLCCUWMasterPojo.setPrintFlag("0");
            tLCCUWMasterPojo.setPrintFlag2("0");
            tLCCUWMasterPojo.setManageCom(tLCContPojo.getManageCom());
            tLCCUWMasterPojo.setUWIdea("");
            tLCCUWMasterPojo.setUpReportContent("");
            tLCCUWMasterPojo.setOperator("ABC-CLOUD");
            tLCCUWMasterPojo.setMakeDate(tCurrentDate);
            tLCCUWMasterPojo.setMakeTime(tCurrentTime);
            tLCCUWMasterPojo.setModifyDate(tCurrentDate);
            tLCCUWMasterPojo.setModifyTime(tCurrentTime);
        } else {
            tLCCUWMasterPojo.setUWNo(tLCCUWMasterPojo.getUWNo() + 1);
            tLCCUWMasterPojo.setState(tContPassFlag);
            tLCCUWMasterPojo.setPassFlag(tContPassFlag);
            tLCCUWMasterPojo.setAutoUWFlag("1"); // 1 自动核保 2 人工核保
            tLCCUWMasterPojo.setUWGrade("A1"); // 核保级别
            tLCCUWMasterPojo.setAppGrade("A1"); // 申报级别
            tLCCUWMasterPojo.setOperator("ABC-CLOUD"); // 操作员
            tLCCUWMasterPojo.setModifyDate(tCurrentDate);
            tLCCUWMasterPojo.setModifyTime(tCurrentTime);
        }
        tradeInfo.addData(tLCCUWMasterPojo);

        //合同子表C表
        logger.debug("合同子表C表");
        ArrayList<LCCUWSubPojo> tLCCUWSubPojoList = (ArrayList<LCCUWSubPojo>) tradeInfo.getData(LCCUWSubPojo.class.getName());
        LCCUWSubPojo tLCCUWSubPojo = new LCCUWSubPojo();
        int oldUwNo = 0;
        if (tLCCUWSubPojoList == null || tLCCUWSubPojoList.size() == 0) {
            tLCCUWSubPojo.setUWNo(1); // 第1次核保
        } else {
            oldUwNo = tLCCUWSubPojoList.size();
            tLCCUWSubPojo.setUWNo(++oldUwNo); // 第几次核保
        }
        String tCUWSubID = PubFun1.CreateMaxNo(PubFun1.PhysicNo, 22);
        tLCCUWSubPojo.setCUWSubID(tCUWSubID);
        tLCCUWSubPojo.setCUWMasterID(tLCCUWMasterPojo.getCUWMasterID());
        tLCCUWSubPojo.setShardingID(tLCCUWMasterPojo.getShardingID());
        tLCCUWSubPojo.setContNo(tLCCUWMasterPojo.getContNo());
        tLCCUWSubPojo.setGrpContNo(tLCCUWMasterPojo.getGrpContNo());
        tLCCUWSubPojo.setProposalContNo(tLCCUWMasterPojo
                .getProposalContNo());
        tLCCUWSubPojo.setInsuredNo(tLCCUWMasterPojo.getInsuredNo());
        tLCCUWSubPojo.setInsuredName(tLCCUWMasterPojo.getInsuredName());
        tLCCUWSubPojo.setAppntNo(tLCCUWMasterPojo.getAppntNo());
        tLCCUWSubPojo.setAppntName(tLCCUWMasterPojo.getAppntName());
        tLCCUWSubPojo.setAgentCode(tLCCUWMasterPojo.getAgentCode());
        tLCCUWSubPojo.setAgentGroup(tLCCUWMasterPojo.getAgentGroup());
        tLCCUWSubPojo.setUWGrade(tLCCUWMasterPojo.getUWGrade()); // 核保级别
        tLCCUWSubPojo.setAppGrade(tLCCUWMasterPojo.getAppGrade()); // 申请级别
        tLCCUWSubPojo.setAutoUWFlag(tLCCUWMasterPojo.getAutoUWFlag());
        tLCCUWSubPojo.setState(tLCCUWMasterPojo.getState());
        tLCCUWSubPojo.setPassFlag(tLCCUWMasterPojo.getState());
        tLCCUWSubPojo.setPostponeDay(tLCCUWMasterPojo.getPostponeDay());
        tLCCUWSubPojo.setPostponeDate(tLCCUWMasterPojo.getPostponeDate());
        tLCCUWSubPojo.setUpReportContent(tLCCUWMasterPojo
                .getUpReportContent());
        tLCCUWSubPojo.setHealthFlag(tLCCUWMasterPojo.getHealthFlag());
        tLCCUWSubPojo.setSpecFlag(tLCCUWMasterPojo.getSpecFlag());
        tLCCUWSubPojo.setSpecReason(tLCCUWMasterPojo.getSpecReason());
        tLCCUWSubPojo.setQuesFlag(tLCCUWMasterPojo.getQuesFlag());
        tLCCUWSubPojo.setReportFlag(tLCCUWMasterPojo.getReportFlag());
        tLCCUWSubPojo.setChangePolFlag(tLCCUWMasterPojo.getChangePolFlag());
        tLCCUWSubPojo.setChangePolReason(tLCCUWMasterPojo
                .getChangePolReason());
        tLCCUWSubPojo.setAddPremReason(tLCCUWMasterPojo.getAddPremReason());
        tLCCUWSubPojo.setPrintFlag(tLCCUWMasterPojo.getPrintFlag());
        tLCCUWSubPojo.setPrintFlag2(tLCCUWMasterPojo.getPrintFlag2());
        tLCCUWSubPojo.setUWIdea(tLCCUWMasterPojo.getUWIdea());
        tLCCUWSubPojo.setOperator(tLCCUWMasterPojo.getOperator()); // 操作员
        tLCCUWSubPojo.setManageCom(tLCCUWMasterPojo.getManageCom());
        tLCCUWSubPojo.setMakeDate(tCurrentDate);
        tLCCUWSubPojo.setMakeTime(tCurrentTime);
        tLCCUWSubPojo.setModifyDate(tCurrentDate);
        tLCCUWSubPojo.setModifyTime(tCurrentTime);
        tradeInfo.addData(tLCCUWSubPojo);

        //根据新规则引擎返回的结果判断保单层核保规则有哪些不通过
        if (tODMResult==null){
            return true;
        }
        List<UwResult> tUwresultList = tODMResult.getUwresultList();
        List tUWErrorList = new ArrayList();//保存所有没有核保通过的保单层规则
        for(int i=0;i<tUwresultList.size();i++){
            UwResult tUwResult= (UwResult) tUwresultList.get(i);
            if(!tUwResult.isFlag()&&"000000".equals(tUwResult.getRiskCode())){
                logger.debug("ODM规则引擎保单层规则"+tUwResult.getRuleCode()+","+tUwResult.getReturnInfo()+"核保没有通过!  " + tradeInfo);
                tradeInfo.addError("ODM规则引擎保单层规则"+tUwResult.getRuleCode()+","+tUwResult.getReturnInfo()+"核保没有通过！");
                tUWErrorList.add(tUwResult);
            }
        }
        int mErrorCount = tUWErrorList.size();
        if(mErrorCount>0){
            logger.debug("保单层共有"+mErrorCount+"条规则不通过！");
            tradeInfo.addError("保单层共有"+mErrorCount+"条规则不通过！");
        }

        // 核保错误信息c表
        logger.debug("核保错误信息c表");
        LCCUWErrorPojo tLCCUWErrorPojo = new LCCUWErrorPojo();
        tLCCUWErrorPojo.setSerialNo("0");
        if (oldUwNo > 0) {
            tLCCUWErrorPojo.setUWNo(oldUwNo);
        } else {
            tLCCUWErrorPojo.setUWNo(1);
        }
        tLCCUWErrorPojo.setContNo(tLCContPojo.getContNo());
        tLCCUWErrorPojo.setGrpContNo(tLCCUWSubPojo.getGrpContNo());
        tLCCUWErrorPojo.setProposalContNo(tLCCUWSubPojo.getProposalContNo());
        tLCCUWErrorPojo.setInsuredNo(tLCCUWSubPojo.getInsuredNo());
        tLCCUWErrorPojo.setInsuredName(tLCCUWSubPojo.getInsuredName());
        tLCCUWErrorPojo.setAppntNo(tLCCUWSubPojo.getAppntNo());
        tLCCUWErrorPojo.setAppntName(tLCCUWSubPojo.getAppntName());
        tLCCUWErrorPojo.setManageCom(tLCCUWSubPojo.getManageCom());
        tLCCUWErrorPojo.setUWRuleCode(""); // 核保规则编码
        tLCCUWErrorPojo.setUWError(""); // 核保出错信息
        tLCCUWErrorPojo.setCurrValue(""); // 当前值
        tLCCUWErrorPojo.setModifyDate(tCurrentDate);
        tLCCUWErrorPojo.setModifyTime(tCurrentTime);
        tLCCUWErrorPojo.setUWPassFlag("");//核保结论，是要根据新规则引擎的返回结果进行赋值
        // 取核保错误信息
        ArrayList<LCCUWErrorPojo> tLCCUWErrorPojoList = new ArrayList<LCCUWErrorPojo>();
        if(mErrorCount>0){
            for(int n=1;n<=mErrorCount;n++){
                //取出有报错信息的结果
                UwResult tUwResult= (UwResult) tUWErrorList.get(n-1);
                //流水号
                String tCUWErrorID = PubFun1.CreateMaxNo(PubFun1.PhysicNo, 22);
                tLCCUWErrorPojo.setCUWErrorID(tCUWErrorID);
                tLCCUWErrorPojo.setCUWMasterID(tLCCUWMasterPojo.getCUWMasterID());
                tLCCUWErrorPojo.setShardingID(tLCCUWMasterPojo.getShardingID());
                String tSerialno = ""+n;
                tLCCUWErrorPojo.setSerialNo(tSerialno);
                tLCCUWErrorPojo.setUWPassFlag("5");
                tLCCUWErrorPojo.setUWRuleCode(tUwResult.getRuleCode());
                tLCCUWErrorPojo.setUWError(tUwResult.getReturnInfo());
                tLCCUWErrorPojo.setUWGrade("A1");
//				Modify by HXXQ201705233366-规则管理平台优化需求  20170606  借用CurrValue字段存储  校验住的规则是否是   非实时规则
                tLCCUWErrorPojo.setCurrValue(tUwResult.getNotRealTimeFlag());
                tLCCUWErrorPojo.setSugPassFlag("1");//区分是ODM保存结果
                LCCUWErrorPojo ttLCCUWErrorPojo = (LCCUWErrorPojo) tReflections.transFields(new LCCUWErrorPojo(),tLCCUWErrorPojo);
                ttLCCUWErrorPojo.setModifyDate(tCurrentDate);
                tLCCUWErrorPojoList.add(ttLCCUWErrorPojo);
            }
        }

        tradeInfo.addData(LCCUWErrorPojo.class.getName(), tLCCUWErrorPojoList);
        // 计算是否需要再保. 目前不存在LMWU.UWType = 'LF'的规则，都不分保
        logger.debug("暂时不需要临分，执行保单级规则存储结束！");
        return true;
    }

    /**
     * @Author: zhuyiming
     * @Description: 对保单核保结论、核保详细信息进行记录保存
     * @Date: Created in 17:59 2017/9/22
     */

    public boolean preparePolRules(TradeInfo tradeInfo) {
        Result tODMResult = (Result) tradeInfo.getData(Result.class.getName());
        String tCurrentDate = ((LCCUWMasterPojo) tradeInfo.getData(LCCUWMasterPojo.class.getName())).getMakeDate();
        String tCurrentTime = ((LCCUWMasterPojo) tradeInfo.getData(LCCUWMasterPojo.class.getName())).getMakeTime();
        Reflections tReflections = new Reflections();
        ArrayList<LCUWMasterPojo> tLCUWMasterPojoList = new ArrayList<LCUWMasterPojo>();
        ArrayList<LCUWSubPojo> tLCUWSubPojoList = new ArrayList<LCUWSubPojo>();
        ArrayList<LCUWErrorPojo> tLCUWErrorPojoList = new ArrayList<LCUWErrorPojo>();
        List<LCPolPojo> tAllLCPolList = new ArrayList<LCPolPojo>();
        logger.debug("preparePolRules,对保单核保结论、核保详细信息进行记录保存开始!");
        List<LCPolPojo> tLCPolPojoList = (List) tradeInfo.getData(LCPolPojo.class.getName());
        LCContPojo tLCContPojo = (LCContPojo) tradeInfo.getData(LCContPojo.class.getName());
        //循环险种
        LCPolPojo tLCPolPojo = new LCPolPojo();
        if (tODMResult==null){
            return true;
        }
        List tUwResultList = tODMResult.getUwresultList();
        for(int i=0;i<tLCPolPojoList.size();i++){
            tLCPolPojo = tLCPolPojoList.get(i);
            /**
             * 根据险种代码和保单险种号以及新规则引擎的返回结果判断险种的核保标志
             * 并将该险种核保不通过的规则，放到list集合tUwErrorList中
             */
            String tPolPassFlag = "9";
            List tUwErrorList = new ArrayList();
            for(int j=0;j<tUwResultList.size();j++){
                UwResult tUwResult = (UwResult) tUwResultList.get(j);
                if(!tUwResult.isFlag() && tLCPolPojo!=null && tLCPolPojo.getRiskCode()!=null && tLCPolPojo.getRiskCode().equals(tUwResult.getRiskCode())){
                    tPolPassFlag = "5";
//					此处不适用break跳出，需要将核保不通过的记录给保存下来，以待后面使用
                    logger.debug("ODM规则引擎险种层规则"+tUwResult.getRuleCode()+","+tUwResult.getReturnInfo()+"核保没有通过！"+ tradeInfo);
                    tradeInfo.addError("ODM规则引擎险种层规则"+","+tUwResult.getReturnInfo()+tUwResult.getRuleCode()+"核保没有通过！");
                    tUwErrorList.add(tUwResult);
                }
            }

            //险种层核保主表C表
            List<LCUWMasterPojo> tLCUWMasterPojoList1 = (List) tradeInfo.getData(LCUWMasterPojo.class.getName());
            LCUWMasterPojo tLCUWMasterPojo = new LCUWMasterPojo();
            if(tLCUWMasterPojoList1 == null || tLCUWMasterPojoList1.size() == 0){
                //若该险种在表里是不存在的
                String tUWMasterID =  PubFun1.CreateMaxNo(PubFun1.PhysicNo, 22);
                tLCUWMasterPojo.setUWMasterID(tUWMasterID);
                tLCUWMasterPojo.setContID(tLCContPojo.getContID());
                tLCUWMasterPojo.setShardingID(tLCContPojo.getShardingID());
                tLCUWMasterPojo.setContNo(tLCContPojo.getContNo());
                tLCUWMasterPojo.setGrpContNo(tLCPolPojo.getGrpContNo());
                tLCUWMasterPojo.setPolNo(tLCPolPojo.getPolNo());
                tLCUWMasterPojo.setProposalContNo(tLCContPojo.getProposalContNo());
                tLCUWMasterPojo.setProposalNo(tLCPolPojo.getPolNo());
                tLCUWMasterPojo.setUWNo(1);
                tLCUWMasterPojo.setInsuredNo(tLCPolPojo.getInsuredNo());
                tLCUWMasterPojo.setInsuredName(tLCPolPojo.getInsuredName());
                tLCUWMasterPojo.setAppntNo(tLCPolPojo.getAppntNo());
                tLCUWMasterPojo.setAppntName(tLCPolPojo.getAppntName());
                tLCUWMasterPojo.setAgentCode(tLCPolPojo.getAgentCode());
                tLCUWMasterPojo.setAgentGroup(tLCPolPojo.getAgentGroup());
                tLCUWMasterPojo.setUWGrade("A1");
                tLCUWMasterPojo.setAppGrade("A1");
                tLCUWMasterPojo.setPostponeDay("");
                tLCUWMasterPojo.setPostponeDate("");
                tLCUWMasterPojo.setAutoUWFlag("1");
                tLCUWMasterPojo.setState(tPolPassFlag);
                tLCUWMasterPojo.setPassFlag(tPolPassFlag);
                tLCUWMasterPojo.setHealthFlag("0");
                tLCUWMasterPojo.setSpecFlag("0");
                tLCUWMasterPojo.setQuesFlag("0");
                tLCUWMasterPojo.setReportFlag("0");
                tLCUWMasterPojo.setChangePolFlag("0");
                tLCUWMasterPojo.setPrintFlag("0");
                tLCUWMasterPojo.setManageCom(tLCPolPojo.getManageCom());
                tLCUWMasterPojo.setUWIdea("");
                tLCUWMasterPojo.setUpReportContent("");
                tLCUWMasterPojo.setOperator("ABC-CLOUD");
                tLCUWMasterPojo.setMakeDate(tCurrentDate);
                tLCUWMasterPojo.setMakeTime(tCurrentTime);
                tLCUWMasterPojo.setModifyDate(tCurrentDate);
                tLCUWMasterPojo.setModifyTime(tCurrentTime);
            }else if(tLCUWMasterPojoList1.size()  == 1){
                int uwNo = tLCUWMasterPojo.getUWNo();
                uwNo = uwNo+1;
                tLCUWMasterPojo.setUWNo(uwNo);
                tLCUWMasterPojo.setProposalContNo(tLCContPojo.getPolicyNo());
                tLCUWMasterPojo.setPassFlag(tPolPassFlag);
                tLCUWMasterPojo.setState(tPolPassFlag);
                tLCUWMasterPojo.setAutoUWFlag("1");
                tLCUWMasterPojo.setAppGrade("A1");
                tLCUWMasterPojo.setUWGrade("A1");
                tLCUWMasterPojo.setOperator("ABC-CLOUD");
                tLCUWMasterPojo.setModifyDate(tCurrentDate);
                tLCUWMasterPojo.setModifyTime(tCurrentTime);
            }else {
                logger.debug("preparePolRules,"+tLCPolPojo.getPolNo() + "个人核保总表取数据不唯一!");
                return false;
            }
            LCUWMasterPojo ttLCUWMasterPojo = (LCUWMasterPojo) tReflections.transFields(new LCUWMasterPojo(),tLCUWMasterPojo);
            ttLCUWMasterPojo.setModifyDate(tCurrentDate);
            ttLCUWMasterPojo.setMakeDate(tCurrentDate);
            tLCUWMasterPojoList.add(ttLCUWMasterPojo);
            //险种层核保子表C表
            List<LCUWSubPojo> tLCUWSubPojoList1 = (List) tradeInfo.getData(LCUWSubPojo.class.getName());
            LCUWSubPojo tLCUWSubPojo = new LCUWSubPojo();
            int oldSubCount = 0;
            if(tLCUWSubPojoList1 == null || tLCUWSubPojoList1.size() == 0){
                tLCUWSubPojo.setUWNo(1);//第1此核保
            } else {
                oldSubCount =tLCUWSubPojoList1 .size();
                tLCUWSubPojo.setUWNo(++oldSubCount);//第几次核保
            }
            String tUWSubID = PubFun1.CreateMaxNo(PubFun1.PhysicNo, 22);
            tLCUWSubPojo.setUWSubID(tUWSubID);
            tLCUWSubPojo.setUWMasterID(tLCUWMasterPojo.getUWMasterID());
            tLCUWSubPojo.setShardingID(tLCUWMasterPojo.getShardingID());
            tLCUWSubPojo.setContNo(tLCContPojo.getContNo());
            tLCUWSubPojo.setPolNo(tLCPolPojo.getPolNo());
            tLCUWSubPojo.setGrpContNo(tLCUWMasterPojo.getGrpContNo());
            tLCUWSubPojo.setProposalContNo(tLCUWMasterPojo.getProposalContNo());
            tLCUWSubPojo.setProposalNo(tLCUWMasterPojo.getProposalNo());
            tLCUWSubPojo.setInsuredNo(tLCUWMasterPojo.getInsuredNo());
            tLCUWSubPojo.setInsuredName(tLCUWMasterPojo.getInsuredName());
            tLCUWSubPojo.setAppntNo(tLCUWMasterPojo.getAppntNo());
            tLCUWSubPojo.setAppntName(tLCUWMasterPojo.getAppntName());
            tLCUWSubPojo.setAgentCode(tLCUWMasterPojo.getAgentCode());
            tLCUWSubPojo.setAgentGroup(tLCUWMasterPojo.getAgentGroup());
            tLCUWSubPojo.setUWGrade(tLCUWMasterPojo.getUWGrade());
            tLCUWSubPojo.setAppGrade(tLCUWMasterPojo.getAppGrade());
            tLCUWSubPojo.setAutoUWFlag(tLCUWMasterPojo.getAutoUWFlag());
            tLCUWSubPojo.setState(tLCUWMasterPojo.getState());
            tLCUWSubPojo.setPassFlag(tLCUWMasterPojo.getPassFlag());
            tLCUWSubPojo.setPostponeDay(tLCUWMasterPojo.getPostponeDay());
            tLCUWSubPojo.setPostponeDate(tLCUWMasterPojo.getPostponeDate());
            tLCUWSubPojo.setUpReportContent(tLCUWMasterPojo.getUpReportContent());
            tLCUWSubPojo.setHealthFlag(tLCUWMasterPojo.getHealthFlag());
            tLCUWSubPojo.setSpecFlag(tLCUWMasterPojo.getSpecFlag());
            tLCUWSubPojo.setSpecReason(tLCUWMasterPojo.getSpecReason());
            tLCUWSubPojo.setQuesFlag(tLCUWMasterPojo.getQuesFlag());
            tLCUWSubPojo.setReportFlag(tLCUWMasterPojo.getReportFlag());
            tLCUWSubPojo.setChangePolFlag(tLCUWMasterPojo.getChangePolFlag());
            tLCUWSubPojo.setChangePolReason(tLCUWMasterPojo.getChangePolReason());
            tLCUWSubPojo.setAddPremReason(tLCUWMasterPojo.getAddPremReason());
            tLCUWSubPojo.setPrintFlag(tLCUWMasterPojo.getPrintFlag());
            tLCUWSubPojo.setPrintFlag2(tLCUWMasterPojo.getPrintFlag2());
            tLCUWSubPojo.setUWIdea(tLCUWMasterPojo.getUWIdea());
            tLCUWSubPojo.setOperator(tLCUWMasterPojo.getOperator());
            tLCUWSubPojo.setManageCom(tLCUWMasterPojo.getManageCom());
            tLCUWSubPojo.setMakeDate(tCurrentDate);
            tLCUWSubPojo.setMakeTime(tCurrentTime);
            tLCUWSubPojo.setModifyDate(tCurrentDate);
            tLCUWSubPojo.setModifyTime(tCurrentTime);
            LCUWSubPojo ttLCUWSubPojo = (LCUWSubPojo) tReflections.transFields(new LCUWSubPojo(),tLCUWSubPojo);
            ttLCUWSubPojo.setModifyDate(tCurrentDate);
            ttLCUWSubPojo.setMakeDate(tCurrentDate);
            tLCUWSubPojoList.add(ttLCUWSubPojo);
            //险种层错误信息表C表
            LCUWErrorPojo tLCUWErrorPojo = new LCUWErrorPojo();
            tLCUWErrorPojo.setSerialNo("0");
            if(oldSubCount>0){
                tLCUWErrorPojo.setUWNo(oldSubCount);//第几次核保
            } else {
                tLCUWErrorPojo.setUWNo(1);//第1此核保
            }
            String tUWErrorID = PubFun1.CreateMaxNo(PubFun1.PhysicNo, 22);
            tLCUWErrorPojo.setUWErrorID(tUWErrorID);
            tLCUWErrorPojo.setUWMasterID(tLCUWMasterPojo.getUWMasterID());
            tLCUWErrorPojo.setShardingID(tLCUWMasterPojo.getShardingID());
            tLCUWErrorPojo.setContNo(tLCContPojo.getContNo());
            tLCUWErrorPojo.setGrpContNo(tLCUWMasterPojo.getGrpContNo());
            tLCUWErrorPojo.setProposalContNo(tLCContPojo.getProposalContNo());
            tLCUWErrorPojo.setPolNo(tLCPolPojo.getPolNo());
            tLCUWErrorPojo.setProposalNo(tLCPolPojo.getProposalNo());
            tLCUWErrorPojo.setInsuredNo(tLCPolPojo.getInsuredNo());
            tLCUWErrorPojo.setInsuredName(tLCPolPojo.getInsuredName());
            tLCUWErrorPojo.setAppntNo(tLCPolPojo.getAppntNo());
            tLCUWErrorPojo.setAppntName(tLCPolPojo.getAppntName());
            tLCUWErrorPojo.setManageCom(tLCPolPojo.getManageCom());
            tLCUWErrorPojo.setUWRuleCode("");//规则编码
            tLCUWErrorPojo.setUWError("");//错误信息
            tLCUWErrorPojo.setCurrValue("");//取当前值
            tLCUWErrorPojo.setModifyDate(tCurrentDate);
            tLCUWErrorPojo.setModifyTime(tCurrentTime);
            tLCUWErrorPojo.setUWPassFlag("");

            //取错误信息
            if(tUwErrorList.size()>0){
                for(int errorCount = 1;errorCount<=tUwErrorList.size();errorCount++){
                    UwResult tUwresult = (UwResult) tUwErrorList.get(errorCount-1);
                    //生成流水号
                    String mSerialno = ""+errorCount;
                    tLCUWErrorPojo.setSerialNo(mSerialno);
                    tLCUWErrorPojo.setUWRuleCode(tUwresult.getRuleCode());
                    tLCUWErrorPojo.setUWError(tUwresult.getReturnInfo());
                    tLCUWErrorPojo.setUWGrade("A1");
//					Modify by HXXQ201705233366-规则管理平台优化需求  20170606  借用CurrValue字段存储  校验住的规则是否是   非实时规则
                    tLCUWErrorPojo.setCurrValue(tUwresult.getNotRealTimeFlag());
                    tLCUWErrorPojo.setSugPassFlag("1");//区分是ODM保存结果
                    tLCUWErrorPojo.setUWPassFlag("5");
                    LCUWErrorPojo ttLCUWErrorPojo = (LCUWErrorPojo) tReflections.transFields(new LCUWErrorPojo(),tLCUWErrorPojo);
                    ttLCUWErrorPojo.setModifyDate(tCurrentDate);
                    tLCUWErrorPojoList.add(ttLCUWErrorPojo);
                }
            }

            tLCPolPojo.setApproveFlag("9");
            tLCPolPojo.setApproveCode("ABC-CLOUD");
            tLCPolPojo.setApproveDate(tCurrentDate);
            tLCPolPojo.setApproveTime(tCurrentTime);
            tLCPolPojo.setUWFlag(tPolPassFlag);
            tLCPolPojo.setUWCode("ABC-CLOUD");
            tLCPolPojo.setUWDate(tCurrentDate);
            tLCPolPojo.setUWTime(tCurrentTime);
            tLCPolPojo.setModifyDate(tCurrentDate);
            tLCPolPojo.setModifyTime(tCurrentTime);
        }
        tradeInfo.addData(LCUWMasterPojo.class.getName(), tLCUWMasterPojoList);
        tradeInfo.addData(LCUWSubPojo.class.getName(), tLCUWSubPojoList);
        tradeInfo.addData(LCUWErrorPojo.class.getName(), tLCUWErrorPojoList);

        logger.debug("preparePolRules,对保单核保结论、核保详细信息进行记录保存结束!");
        return true;
    }


    public boolean OutputData(TradeInfo tradeInfo) {

        //核保错误结论
        Result result = (Result) tradeInfo.getData(Result.class.getName());
        if (result==null){
            return true;
        }
        List<LKTransStatusPojo>  lkTransStatusPojos = (List<LKTransStatusPojo>) tradeInfo.getData(LKTransStatusPojo.class.getName());
        String seNo = "";
        if (lkTransStatusPojos!=null && lkTransStatusPojos.size()>0){
            seNo = lkTransStatusPojos.get(0).getTransNo();
        }
        List<UwResult> tUwResultList = result.getUwresultList();
        //保单数据
        LCContPojo lcContPojo = (LCContPojo) tradeInfo.getData(LCContPojo.class.getName());
        //核保错误数据存储
        List<LCResultInfoPojo> lcResultInfoPojos = new ArrayList<>();
        Reflections reflections = new Reflections();
        if(tUwResultList != null && tUwResultList.size() >0){
            for (int i = 0; i < tUwResultList.size(); i++) {
               logger.debug("TODM"+seNo+"["+tUwResultList.get(i).getRuleCode()+"]"+tUwResultList.get(i).getReturnInfo());
            }
            UwResult uwResult = tUwResultList.get(tUwResultList.size()-1);
            LCResultInfoPojo lcResultInfoPojo = new LCResultInfoPojo();
            lcResultInfoPojo.setContNo(lcContPojo.getContNo());
            lcResultInfoPojo.setPrtNo(lcContPojo.getPrtNo());
            lcResultInfoPojo.setResultNo(uwResult.getRuleCode());
            lcResultInfoPojo.setResultContent(uwResult.getReturnInfo());
            lcResultInfoPojo.setKey1(uwResult.getRiskCode());
            lcResultInfoPojos.add((LCResultInfoPojo) reflections.transFields(new LCResultInfoPojo(), lcResultInfoPojo));
            tradeInfo.addData(LCResultInfoPojo.class.getName(), lcResultInfoPojos);
            logger.debug("团险被核保校验住的规则"+lcResultInfoPojo.toString());
        }
        tradeInfo.removeData(Result.class.getName());
        return true;
    }
}
