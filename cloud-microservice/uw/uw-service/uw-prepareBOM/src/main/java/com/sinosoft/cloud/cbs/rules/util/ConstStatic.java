package com.sinosoft.cloud.cbs.rules.util;

/**
 * @Author:wangshuliang
 * @Description: 存储核保相关的静态变量
 * @Date:Created in 10:50 2018/9/4
 * @Modified by:
 */
public class ConstStatic {
    /**
     * 移动展业1102的标记 对应的是银保通的1004交易
     */
    public static String funcflag1102 = "1102";
    /**
     * 移动展业1101的标记 对应的是银保通的1002交易
     */
    public static String funcflag1101 = "1101";
}
