package com.sinosoft.cloud.cbs.rules.compare.dealbl;

import com.sinosoft.cloud.cbs.rules.bom.Risk;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Component;

import java.lang.reflect.AccessibleObject;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;


/**
 * 判断两个xml中riskList节点下的数据是否一致.
 */


@Component
public class RiskBL {
    private final Log logger = LogFactory.getLog(this.getClass());

    /**
     * 处理方法.
     *
     * @param oldRisks 数据.
     * @param newRisks 数据.
     * @return
     */
    public boolean xmlTwoIsSame(List<Risk> oldRisks, List<Risk> newRisks) {

        logger.debug("比对老核心得到的险别信息和累计风险保额所得到的险别信息开始");
        boolean flag = true;

        if (oldRisks != null && newRisks != null) {
            if (oldRisks.size() == newRisks.size()) {
                for (int i = 0; i < oldRisks.size(); i++) {
                    final Risk oldRisk = oldRisks.get(i);
                    final Risk newRisk = newRisks.get(i);
                    final Class clazz = oldRisk.getClass();
                    final Field[] fields = clazz.getDeclaredFields();
                    AccessibleObject.setAccessible(fields, true);
                    for (Field field : fields) {
                        if (field.isAccessible()) {
                            try {
                                final Object oldValue = field.get(oldRisk);
                                final Object newValue = field.get(newRisk);
                                if (oldValue != null && newValue != null) {
                                    if (!oldValue.equals(newValue)) {
                                        flag = false;
                                        logger.debug("不一样的字段是：" + field.getName() + "老核心所获取到的值是：" + oldValue + ",累计风险保额所获取的值是：" + newValue);
                                    }
                                }
                            } catch (IllegalAccessException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
            }
        }

        if (flag) {
            logger.debug("比对老核心得到的险别信息和累计风险保额所得到的险别信息数据一致");
        }
        logger.debug("比对老核心得到的险别信息和累计风险保额所得到的险别信息开始结束");
        return flag;
    }


  /*  public static void main(String[] args) {
        final List<Risk> oldRisks = new ArrayList<Risk>();
        Risk risk = new Risk();
        risk.setAutoPayingInAdvance(true);
        risk.setAuthBankName("00");

        Risk risk2 = new Risk();
        risk2.setAutoPayingInAdvance(true);
        risk2.setAuthBankName("22");
        oldRisks.add(risk2);
        oldRisks.add(risk);

        final List<Risk> newRisks = new ArrayList<Risk>();
        Risk risk1 = new Risk();
        risk1.setAutoPayingInAdvance(false);
        risk1.setAuthBankName("wangwu");
        newRisks.add(risk1);
        Risk risk3 = new Risk();
        risk3.setAutoPayingInAdvance(false);
        risk3.setAuthBankName("33");
        newRisks.add(risk3);
        RiskBL riskBL = new RiskBL();
        riskBL.xmlTwoIsSame(oldRisks, newRisks);

    }*/


}
