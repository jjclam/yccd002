package com.sinosoft.cloud.cbs.rules.data;

import com.sinosoft.cloud.cbs.rules.bom.Applicant;
import com.sinosoft.cloud.cbs.rules.bom.Insured;
import com.sinosoft.cloud.cbs.rules.bom.Policy;
import com.sinosoft.cloud.nb.service.redis.NBRedisCommon;
import com.sinosoft.cloud.rest.TradeInfo;
import com.sinosoft.lis.entity.LCContPojo;
import com.sinosoft.lis.entity.LCCustomerImpartParamsPojo;
import com.sinosoft.lis.entity.LCInsuredPojo;
import com.sinosoft.lis.entity.LKTransTracksPojo;
import com.sinosoft.utility.*;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Author: zhuyiming
 * @Description: 提取既往数据
 * @Date: Created in 1:23 2017/9/25
 */
@Service
public class ExtractPast {
	private final Log logger = LogFactory.getLog(this.getClass());
	@Autowired
	NBRedisCommon nbRedisCommon;
	@Value("${cloud.uw.barrier.control}")
	private String barrier;

	public void getPast(TradeInfo tradeInfo, Policy policy) {
		//挡板
		if("true".equals(barrier)){
			logger.debug("保单号：" + policy.getContNo() + "，既往数据挡板启动！");
			getBarrierData(policy);
			return;
		}
		// 投保人既往
		getApplicantPast(tradeInfo, policy);
		if(tradeInfo.hasError()){
			logger.debug("投保人既往提取失败！");
			tradeInfo.addError("投保人既往提取失败！");
			return;
		}
		logger.debug("投保人既往提取成功！");
		// 被保人既往
		getInsuredPast(tradeInfo, policy);
		if(tradeInfo.hasError()){
			logger.debug("被保人既往提取失败！");
			tradeInfo.addError("被保人既往提取失败！");
			return;
		}
		logger.debug("被保人既往提取成功！");
	}

	/**
	 * 投保人既往数据
	 * @param policy
	 * @return
	 */
	public void getApplicantPast(TradeInfo tradeInfo, Policy policy) {
		LCContPojo lcContPojo = (LCContPojo) tradeInfo.getData(LCContPojo.class.getName());
		// 投保人
		Applicant applicantInfo = policy.getApplicant();
		// 客户号
		String clientNo = applicantInfo.getClientNo();
		// 合同号
		String contno = policy.getContNo();

		StringBuffer sql = new StringBuffer();

		VData tVData = new VData();
		TransferData tParam = new TransferData();

		// 既往有体检记录
		/*sql
				.append("SELECT 1 as field, 1 as flag FROM LCPENOTICE L,LCPENOTICERESULT LS WHERE L.CUSTOMERNO = ? AND L.CONTNO = LS.CONTNO AND L.PRTSEQ = LS.PRTSEQ AND LS.PRTSEQ IS NOT NULL ");
		sql.append(" UNION ");*/
		// 既往有体检、加费、特约的保单
		/*sql
				.append(" select 1 as field, 2 as flag from lcpenotice a, lcpol b where a.CUSTOMERNO = ? AND (b.APPNTNO = ? and (b.uwflag = '3' or b.uwflag = '4')) ");
		sql.append(" UNION ");*/
		// 既往有理赔记录
		/** wpq 2018-2-2**/
	/*	sql
//				.append(" SELECT 1 as field, 3 as flag FROM DUAL WHERE (SELECT count(*) FROM llregister WHERE CUSTOMERNO = ? AND clmstate>=60)<>0 OR (SELECT count(*) FROM LLSubReport WHERE CUSTOMERNO = ?)<>0 ");
				.append(" SELECT 1 as field, 3 as flag FROM DUAL WHERE (SELECT count(*) FROM llregister WHERE CUSTOMERNO = ? ) <> 0 OR (SELECT count(*) FROM LLSubReport WHERE CUSTOMERNO = ?) <> 0  ");

		sql.append(" UNION ");*/
		// 既往有保全二核
		/*sql
				//.append(" SELECT 1 as field, 4 as flag FROM LPEDORMAIN WHERE UWSTATE='5' AND contno IN (SELECT contno FROM LCAPPNT WHERE appntno=?)");
		         // .append("select 1 as field1 , 4 as flag from (SELECT ( CASE WHEN (SELECT COUNT(1) FROM lpedormain a WHERE EXISTS (SELECT 1 FROM lpcuwmaster l WHERE a.contno=l.contno AND l.contno IN (SELECT contno FROM LCINSURED WHERE INSUREDNO= ? ) AND l.autouwflag='2' )) >0 THEN 1 ELSE 0 END + CASE WHEN (SELECT COUNT(1) FROM lpedormain a WHERE a.UWSTATE='5' AND a.contno IN (SELECT contno FROM LCINSURED WHERE INSUREDNO= ? )) >0 THEN 1 ELSE 0 END) AS num FROM dual) tab where tab.num>0 ");
		.append("  SELECT 1 AS field, 4 AS flag FROM dual WHERE (SELECT COUNT(1) FROM lpuwmaster l WHERE l.insuredno = ? AND l.autouwflag = '2') <> 0 OR (SELECT COUNT(1) FROM lpcuwmaster l WHERE l.insuredno = ? AND l.autouwflag = '2') <> 0 ");
		sql.append(" UNION ");*/
		// 既往有生调记录
		/*sql
				.append(" SELECT 1 as field, 5 as flag FROM LDSYSVAR WHERE SYSVAR = 'onerow' AND (SELECT COUNT(1) FROM LCRREPORT WHERE APPNTNO = ?) > 0 ");
*/		/*sql.append(" UNION ");*/
		// 既往有投保经历
		//只有被保人才会校验
		// 既往有次标准体记录
//		sql
//				.append(" SELECT 1 as field, 7 as flag FROM DUAL WHERE (SELECT COUNT(1) FROM LCUWMASTER WHERE CONTNO <> ? AND APPNTNO = ? AND PASSFLAG IN ('1','2','3','4','d') AND (SELECT SALECHNL FROM LCCONT WHERE CONTNO = LCUWMASTER.CONTNO) <> '2') + ");
//		sql
//				.append("  (SELECT COUNT(1) FROM LCCUWMASTER WHERE CONTNO <> ? AND APPNTNO = ? AND PASSFLAG IN ('1','2','3','4','d') AND (SELECT SALECHNL FROM LCCONT WHERE CONTNO = LCCUWMASTER.CONTNO) <> '2') + ");
//		sql
//				.append("  (SELECT COUNT(1) FROM LLUWMASTER WHERE CONTNO <> ? AND APPNTNO = ? AND PASSFLAG IN ('1','2','3','4','d') AND (SELECT SALECHNL FROM LCCONT WHERE CONTNO = LLUWMASTER.CONTNO) <> '2') + ");
//		sql
//				.append("  (SELECT COUNT(1) FROM LLCUWMASTER WHERE CONTNO <> ? AND APPNTNO = ? AND PASSFLAG IN ('1','2','3','4','d') AND (SELECT SALECHNL FROM LCCONT WHERE CONTNO = LLCUWMASTER.CONTNO) <> '2') + ");
//		sql
//				.append("  (SELECT COUNT(1) FROM LPUWMASTER WHERE CONTNO <> ? AND APPNTNO = ? AND PASSFLAG IN ('1','2','3','4','d') AND (SELECT SALECHNL FROM LCCONT WHERE CONTNO = LPUWMASTER.CONTNO) <> '2') + ");
//		sql
//				.append("  (SELECT COUNT(1) FROM LPCUWMASTER WHERE CONTNO <> ? AND APPNTNO = ? AND PASSFLAG IN ('1','2','3','4','d') AND (SELECT SALECHNL FROM LCCONT WHERE CONTNO = LPCUWMASTER.CONTNO) <> '2') > 0 ");
//		sql.append(" UNION ");

		/** wpq 2018-2-2**/
		/*sql
				.append(" SELECT 1 as field, 7 as flag FROM DUAL WHERE (SELECT COUNT(1) FROM LCUWMASTER WHERE CONTNO <> ? AND APPNTNO = ? AND PASSFLAG IN ('3','4','d') AND (SELECT SALECHNL FROM LCCONT WHERE CONTNO = LCUWMASTER.CONTNO) <> '2') + ");
		sql
				.append("  (SELECT COUNT(1) FROM LCCUWMASTER WHERE CONTNO <> ? AND APPNTNO = ? AND PASSFLAG IN ('3','4','d') AND (SELECT SALECHNL FROM LCCONT WHERE CONTNO = LCCUWMASTER.CONTNO) <> '2') + ");
		sql
				.append("  (SELECT COUNT(1) FROM LLUWMASTER WHERE CONTNO <> ? AND APPNTNO = ? AND PASSFLAG IN ('3','4','d') AND (SELECT SALECHNL FROM LCCONT WHERE CONTNO = LLUWMASTER.CONTNO) <> '2') + ");
		sql
				.append("  (SELECT COUNT(1) FROM LLCUWMASTER WHERE CONTNO <> ? AND APPNTNO = ? AND PASSFLAG IN ('3','4','d') AND (SELECT SALECHNL FROM LCCONT WHERE CONTNO = LLCUWMASTER.CONTNO) <> '2') + ");
		sql
				.append("  (SELECT COUNT(1) FROM LPUWMASTER WHERE CONTNO <> ? AND APPNTNO = ? AND PASSFLAG IN ('3','4','d') AND (SELECT SALECHNL FROM LCCONT WHERE CONTNO = LPUWMASTER.CONTNO) <> '2') + ");
		sql
				.append("  (SELECT COUNT(1) FROM LPCUWMASTER WHERE CONTNO <> ? AND APPNTNO = ? AND PASSFLAG IN ('3','4','d') AND (SELECT SALECHNL FROM LCCONT WHERE CONTNO = LPCUWMASTER.CONTNO) <> '2') > 0 ");
		sql.append(" UNION ");*/
		// 既往有体检未完成记录
		/*sql
				.append(" SELECT 1 as field, 8 as flag FROM DUAL WHERE (SELECT COUNT(1) FROM LCPENOTICE L WHERE CUSTOMERNO = ? AND CONTNO <> ? AND NOT EXISTS (SELECT 1 FROM LCPENOTICERESULT LS, LOPRTMANAGER A WHERE L.CUSTOMERNO = LS.CUSTOMERNO AND L.CONTNO = LS.CONTNO AND L.PRTSEQ = A.OLDPRTSEQ AND LS.PRTSEQ = A.PRTSEQ)) > 0 ");
				//.append(" SELECT 1 as field, 8 as flag FROM LCPENOTICE L,LCPENOTICERESULT LS WHERE L.CUSTOMERNO = ? AND L.CONTNO = LS.CONTNO AND L.PRTSEQ = LS.PRTSEQ AND LS.PRTSEQ IS NOT NULL ");
*/
	/*	sql.append(" UNION ");
		// 既往告知为五、六类或拒保职业
		sql
				.append(" SELECT 1 as field, 9 as flag FROM LCCONT A,LCAPPNT B WHERE A.CONTNO = B.CONTNO AND A.SALECHNL <> '2' AND A.CONTNO <> ? AND A.APPNTNO = B.APPNTNO AND B.APPNTNO = ?  AND B.OCCUPATIONTYPE IN ('5','6','R') AND ? IN ('1','2','3','4')  and A.polapplydate <= trunc(sysdate) and A.polapplydate >  trunc(sysdate-365)");
		*/

		// 职业类别低于既往告知
		if(("1".equals(lcContPojo.getSaleChnl())
				|| "3".equals(lcContPojo.getSaleChnl())
				|| "4".equals(lcContPojo.getSaleChnl()))
				&& applicantInfo.getOccuType() != null
				&& !"".equals(applicantInfo.getOccuType().trim())
				&& Integer.parseInt(applicantInfo.getOccuType().trim()) < 5){
			/*sql.append(" UNION ");*/
			sql
					.append(" SELECT 1 as field, 10 as flag FROM DUAL WHERE (SELECT MAX(OCCUPATIONTYPE) FROM LCAPPNT WHERE APPNTNO = ? AND EXISTS (SELECT 1 FROM LCCONT WHERE CONTNO = LCAPPNT.CONTNO AND SALECHNL IN ('1', '3', '4'))) < '5' AND ? < (SELECT MIN(OCCUPATIONTYPE) FROM LCAPPNT WHERE APPNTNO =? AND CONTNO != ? AND EXISTS (SELECT 1 FROM LCCONT WHERE CONTNO = LCAPPNT.CONTNO AND SALECHNL IN ('1', '3', '4') and LCCONT.polapplydate <= trunc(sysdate) and LCCONT.polapplydate > trunc(sysdate-365)))");
		}
		/*sql.append(" UNION ");
		//投保人既往有理赔记录
		sql
				.append("  SELECT 1 as field, 11 as flag FROM DUAL WHERE (SELECT count(1) FROM llcase WHERE CUSTOMERNO = ?) <> 0  or (SELECT count(1) FROM LLREGISTER WHERE CUSTOMERNO = ?) <> 0 or (SELECT count(1) FROM LLSUBREPORT WHERE CUSTOMERNO = ?) <> 0 ");
*/
		tVData.add(sql.toString());
		tVData.add(tParam);
		// 既往有体检记录
		//tParam.setNameAndValue("APPNTNO1", "string:" + clientNo);
		// 既往有体检、加费、特约的保单
		/*tParam.setNameAndValue("CUSTOMERNO1", "string:" + clientNo);
		tParam.setNameAndValue("APPNTNO2", "string:" + clientNo);
*/		// 既往有理赔记录
		/*tParam.setNameAndValue("CUSTOMERNO2", "string:" + clientNo);
		tParam.setNameAndValue("APPNTNO3", "string:" + clientNo);*/
		// 既往有保全二核
		/*tParam.setNameAndValue("APPNTNO41", "string:" + clientNo);
		tParam.setNameAndValue("APPNTNO42", "string:" + clientNo);*/
		// 既往有生调记录
		//tParam.setNameAndValue("APPNTNO5", "string:" + clientNo);
		// 既往有投保经历

		// 既往有次标准体记录
		/*tParam.setNameAndValue("CONTNO2", "string:" + contno);
		tParam.setNameAndValue("APPNTNO6", "string:" + clientNo);
		tParam.setNameAndValue("CONTNO3", "string:" + contno);
		tParam.setNameAndValue("APPNTNO7", "string:" + clientNo);
		tParam.setNameAndValue("CONTNO4", "string:" + contno);
		tParam.setNameAndValue("APPNTNO8", "string:" + clientNo);
		tParam.setNameAndValue("CONTNO5", "string:" + contno);
		tParam.setNameAndValue("APPNTNO9", "string:" + clientNo);
		tParam.setNameAndValue("CONTNO6", "string:" + contno);
		tParam.setNameAndValue("APPNTNO10", "string:" + clientNo);
		tParam.setNameAndValue("CONTNO7", "string:" + contno);
		tParam.setNameAndValue("APPNTNO11", "string:" + clientNo);*/
		// 既往有体检未完成记录
		/*tParam.setNameAndValue("APPNTNO12", "string:" + clientNo);
		tParam.setNameAndValue("CONTNO8", "string:" + contno);*/
		// 既往告知为五、六类或拒保职业
		/*tParam.setNameAndValue("CONTNO9", "string:" + contno);
		tParam.setNameAndValue("APPNTNO13", "string:" + clientNo);
		tParam.setNameAndValue("AppntJobType", "string:"
				+ applicantInfo.getOccuType());*/
		// 职业类别低于既往告知
        if(("1".equals(lcContPojo.getSaleChnl())
                || "3".equals(lcContPojo.getSaleChnl())
                || "4".equals(lcContPojo.getSaleChnl()))
                && applicantInfo.getOccuType() != null
                && !"".equals(applicantInfo.getOccuType().trim())
                && Integer.parseInt(applicantInfo.getOccuType().trim()) < 5){
            tParam.setNameAndValue("APPNTNO14", "string:" + clientNo);
            tParam.setNameAndValue("OCCUPATIONTYPE", "string:"
                    + applicantInfo.getOccuType());
            tParam.setNameAndValue("APPNTNO15", "string:" + clientNo);
            tParam.setNameAndValue("CONTNO10", "string:" + contno);

        //投保人既往有理赔记录
		/*tParam.setNameAndValue("GroupAPPNTNO1", "string:" + clientNo);
		tParam.setNameAndValue("GroupAPPNTNO2", "string:" + clientNo);
		tParam.setNameAndValue("GroupAPPNTNO3", "string:" + clientNo);*/

		logger.info("PastApplicant：" + sql);
		long beginTime = System.currentTimeMillis();
		SSRS tSSRS = new SSRS();
		Connection tConnection = null;
		try {
			tConnection = DBConnPool.getConnection("basedataSource");
			ExeSQL exeSql = new ExeSQL(tConnection);
			tSSRS = exeSql.execSQL(tVData);
		}catch (Exception e){
			e.printStackTrace();
			logger.error(ExceptionUtils.exceptionToString(e));
			tradeInfo.addError(ExceptionUtils.exceptionToString(e));
			return;
		}finally {
			if (tConnection != null) {
				try {
					tConnection.close();
				} catch (SQLException e) {
					e.printStackTrace();
					tradeInfo.addError(ExceptionUtils.exceptionToString(e));
					logger.error(ExceptionUtils.exceptionToString(e));
					return;
				}
			}
		}
		if(tSSRS == null){
			logger.debug("保单号：" + lcContPojo.getContNo() + "，投保人既往提数访问老核心SQL发生异常！");
			tradeInfo.addError("保单号：" + lcContPojo.getContNo() + "，投保人既往提数访问老核心SQL发生异常！");
			return;
		}
		logger.debug("投保人既往提数：" + (System.currentTimeMillis() - beginTime) / 1000.0
				+ "s");
		int flag = 0;
		if (null != tSSRS && tSSRS.MaxRow > 0) {
			for (int i = 1; i <= tSSRS.MaxRow; i++) {
				flag = Util.toInt(tSSRS.GetText(i, 2));
				switch (flag) {
					/*case 1:
						// 既往有体检记录
						applicantInfo.setHaveCheckNote(true);
						break;*/
					/*case 2:
						// 既往有体检、加费、特约的保单
						applicantInfo.setHaveExaminePolicy(true);
						break;*/
					/*case 3:
						// 既往有理赔记录
						applicantInfo.setHaveClaims(true);
						break;*/
					/*case 4:
						// 既往有保全二核
						applicantInfo.setHavePreserve(true);
						break;*/
					/*case 5:
						// 既往有生调记录
						applicantInfo.setHaveAscertain(true);
						break;*/
					/*case 6:
						// 既往有投保经历
						applicantInfo.setHaveApptHistory(true);
						break;*/
					/*case 7:
						// 既往有次标准体记录
						applicantInfo.setHaveSubExamine(true);
						break;*/
					/*case 8:
						// 既往有体检未完成记录
						applicantInfo.setHaveUnfiExamine(true);
						break;*/
				/*	case 9:
						// 既往告知为五、六类或拒保职业
						applicantInfo.setHaveRejectionOcc(true);
						break;*/
					case 10:
						// 职业类别低于既往告知
						applicantInfo.setAppntJobTypeThaBefo(true);
						break;
					/*case 11:
						//投保人既往有理赔记录
						applicantInfo.setHaveClaims(true);
						break;*/
				}
			}
		}
		}
	}

	/**
	 * 被保人既往
	 * @param policy
	 * @return
	 */
	public void getInsuredPast(TradeInfo tradeInfo, Policy policy) {
		// 被保人列表
		List insuredInfoList = policy.getInsuredList();
		List<LCInsuredPojo> lcInsuredPojos = (List<LCInsuredPojo>) tradeInfo.getData(LCInsuredPojo.class.getName());
		List<LKTransTracksPojo> transTracksPojos=(List<LKTransTracksPojo>)tradeInfo.getData(LKTransTracksPojo.class.getName());
		//父保单号
		String beforContno=null;
		if (transTracksPojos !=null && transTracksPojos.size()>0){
			for (int i = 0; i < transTracksPojos.size(); i++){
				if (transTracksPojos.get(i).getTemp2()!=null && !"".equals(transTracksPojos.get(i).getTemp2())){
					beforContno=transTracksPojos.get(i).getTemp2();
				}
			}
		}
		// 合同号
		String contno = policy.getContNo();
		//告知信息
		String impartFlag = "";
		boolean haveApptHistory = false;
		List<LCCustomerImpartParamsPojo> lcCustomerImpartParamsPojos = new ArrayList<>();
		for(LCCustomerImpartParamsPojo lcCustomerImpartParamsPojo : lcCustomerImpartParamsPojos){
			if("GX10".equals(lcCustomerImpartParamsPojo.getImpartVer())
					&& "YesOrNo".equals(lcCustomerImpartParamsPojo.getImpartParamName())
					&& "1".equals(lcCustomerImpartParamsPojo.getImpartParam())){
				impartFlag = nbRedisCommon.checkImpart(lcCustomerImpartParamsPojo.getPatchNo(),
						lcCustomerImpartParamsPojo.getImpartCode(),
						lcCustomerImpartParamsPojo.getCustomerNoType());
			}
			if("1".equals(lcCustomerImpartParamsPojo.getCustomerNoType())
					&& "002".equals(lcCustomerImpartParamsPojo.getImpartCode())
					&& "01".equals(lcCustomerImpartParamsPojo.getImpartVer())
					&&  0==lcCustomerImpartParamsPojo.getPatchNo()
					&& "1".equals(lcCustomerImpartParamsPojo.getImpartParam())){
				haveApptHistory = true;
			}
		}

		for (int i = 0; i < insuredInfoList.size(); i++) {
			// 被保人
			Insured insured = (Insured) insuredInfoList.get(i);
			//投保人
			Applicant applicantInfo = policy.getApplicant();

			// 客户号
			String clientNo = insured.getClientNo();
			StringBuffer sql = new StringBuffer();


			VData tVData = new VData();
			TransferData tParam = new TransferData();

			// 既往有体检、加费、特约的保单
			/*sql
					.append("select 1 as field1, 1 as flag from lcpenotice a, lcpol b where a.CUSTOMERNO = ? AND (b.INSUREDNO = ? and (b.uwflag = '3' or b.uwflag = '4')) ");
			sql.append(" UNION ");*/
			// 既往有理赔记录
			/**wpq 2018-2-2 **/
			/*if (!clientNo.equals(applicantInfo.getClientNo())) {
				sql
						//.append(" SELECT 1 as field1, 2 as flag FROM DUAL WHERE (SELECT count(*) FROM llregister WHERE CUSTOMERNO = ? AND clmstate >= 60) <> 0 OR (SELECT count(*) FROM LLSubReport WHERE CUSTOMERNO = ?) <> 0 ");
						.append(" SELECT 1 as field1, 2 as flag FROM DUAL WHERE (SELECT count(*) FROM llregister WHERE CUSTOMERNO = ? ) <> 0 OR (SELECT count(*) FROM LLSubReport WHERE CUSTOMERNO = ?) <> 0 ");

				sql.append(" UNION ");
			}*/
			// 既往有保全二核  add  wangshuliang 由UWSTATE='0'改成UWSTATE='5'
			sql
					//.append(" SELECT 1 as field1, 3 as flag FROM LPEDORMAIN WHERE UWSTATE='5' AND contno IN (SELECT contno FROM LCINSURED WHERE INSUREDNO=?) ");
			       //  .append("select 1 as field1 , 3 as flag from (SELECT ( CASE WHEN (SELECT COUNT(1) FROM lpedormain a WHERE EXISTS (SELECT 1 FROM lpcuwmaster l WHERE a.contno=l.contno AND l.contno IN (SELECT contno FROM LCINSURED WHERE INSUREDNO= ? ) AND l.autouwflag='2' )) >0 THEN 1 ELSE 0 END + CASE WHEN (SELECT COUNT(1) FROM lpedormain a WHERE a.UWSTATE='5' AND a.contno IN (SELECT contno FROM LCINSURED WHERE INSUREDNO= ? )) >0 THEN 1 ELSE 0 END) AS num FROM dual) tab where tab.num>0 ");
			.append("  SELECT 1 AS field, 3 AS flag FROM dual WHERE (SELECT COUNT(1) FROM lpuwmaster l WHERE l.insuredno = ? AND l.autouwflag = '2') <> 0 OR (SELECT COUNT(1) FROM lpcuwmaster l WHERE l.insuredno = ? AND l.autouwflag = '2') <> 0 ");
			sql.append(" UNION ");
			// 既往有生调记录
			sql
					.append(" SELECT 1 as field1, 4 as flag FROM LDSYSVAR WHERE SYSVAR = 'onerow' AND (SELECT COUNT(1) FROM LCRREPORT WHERE CUSTOMERNO = ?) > 0 ");
			sql.append(" UNION ");
			// 既往有投保经历
			insured.setHaveApptHistory(haveApptHistory);
			// 既往有次标准体记录
//			sql
//					.append(" SELECT 1 as field1, 6 as flag FROM DUAL WHERE (SELECT COUNT(1) FROM LCUWMASTER WHERE CONTNO <> ? AND INSUREDNO = ? AND PASSFLAG IN ('1','2','3','4','d')) + ");
//			sql
//					.append("  (SELECT COUNT(1) FROM LCCUWMASTER WHERE CONTNO <> ? AND INSUREDNO = ? AND PASSFLAG IN ('1','2','3','4','d')) + ");
//			sql
//					.append("  (SELECT COUNT(1) FROM LLUWMASTER WHERE CONTNO <> ? AND INSUREDNO = ? AND PASSFLAG IN ('1','2','3','4','d')) + ");
//			sql
//					.append("  (SELECT COUNT(1) FROM LLCUWMASTER WHERE CONTNO <> ? AND INSUREDNO = ? AND PASSFLAG IN ('1','2','3','4','d')) + ");
//			sql
//					.append("  (SELECT COUNT(1) FROM LPUWMASTER WHERE CONTNO <> ? AND INSUREDNO = ? AND PASSFLAG IN ('1','2','3','4','d')) + ");
//			sql
//					.append("  (SELECT COUNT(1) FROM LPCUWMASTER WHERE CONTNO <> ? AND INSUREDNO = ? AND PASSFLAG IN ('1','2','3','4','d')) > 0 ");
//			sql.append(" UNION ");
			/** wpq 2018-2-2**/
			sql
					.append(" SELECT 1 as field1, 6 as flag FROM DUAL WHERE (SELECT COUNT(1) FROM LCUWMASTER WHERE CONTNO <> ? AND INSUREDNO = ? AND PASSFLAG IN ('3','4','d')) + ");
			sql
					.append("  (SELECT COUNT(1) FROM LCCUWMASTER WHERE CONTNO <> ? AND INSUREDNO = ? AND PASSFLAG IN ('3','4','d')) + ");
			sql
					.append("  (SELECT COUNT(1) FROM LLUWMASTER WHERE CONTNO <> ? AND INSUREDNO = ? AND PASSFLAG IN ('3','4','d')) + ");
			sql
					.append("  (SELECT COUNT(1) FROM LLCUWMASTER WHERE CONTNO <> ? AND INSUREDNO = ? AND PASSFLAG IN ('3','4','d')) + ");
			sql
					.append("  (SELECT COUNT(1) FROM LPUWMASTER WHERE CONTNO <> ? AND INSUREDNO = ? AND PASSFLAG IN ('3','4','d')) + ");
			sql
					.append("  (SELECT COUNT(1) FROM LPCUWMASTER WHERE CONTNO <> ? AND INSUREDNO = ? AND PASSFLAG IN ('3','4','d')) > 0 ");
			sql.append(" UNION ");
			// 既往有体检未完成记录

				sql
						.append(" SELECT 1 as field1, 7 as flag FROM DUAL WHERE (SELECT COUNT(1) FROM LCPENOTICE L WHERE CUSTOMERNO = ? AND CONTNO <> ? AND NOT EXISTS (SELECT 1 FROM LCPENOTICERESULT LS, LOPRTMANAGER A WHERE L.CUSTOMERNO = LS.CUSTOMERNO AND L.CONTNO = LS.CONTNO AND L.PRTSEQ = A.OLDPRTSEQ AND LS.PRTSEQ = A.PRTSEQ)) > 0 ");
						//.append(" SELECT 1 as field, 7 as flag FROM LCPENOTICE L,LCPENOTICERESULT LS WHERE L.CUSTOMERNO = ? AND L.CONTNO = LS.CONTNO AND L.PRTSEQ = LS.PRTSEQ AND LS.PRTSEQ IS NOT NULL ");
				sql.append(" UNION ");
			
			// 既往告知为五、六类或拒保职业
			sql
					.append(" SELECT 1 as field1, 8 as flag FROM LCCONT A,LCINSURED B WHERE A.CONTNO = B.CONTNO AND A.SALECHNL <> '2' AND A.CONTNO <> ? AND A.INSUREDNO = B.INSUREDNO AND B.INSUREDNO = ? AND B.OCCUPATIONTYPE IN ('5','6','R') AND ? IN ('1','2','3','4') and A.polapplydate <= trunc(sysdate) and A.polapplydate > trunc(sysdate-365) ");
			sql.append(" UNION ");
			// 职业类别低于既往告知
			sql
					.append(" SELECT 1 as field1, 9 as flag FROM DUAL WHERE (SELECT MAX(OCCUPATIONTYPE) FROM LCINSURED WHERE INSUREDNO = ? AND EXISTS (SELECT 1 FROM LCCONT WHERE CONTNO = LCINSURED.CONTNO AND SALECHNL IN ('1', '3', '4'))) < '5'    AND ? < (SELECT MAX(OCCUPATIONTYPE) FROM LCINSURED WHERE INSUREDNO = ? AND CONTNO != ? AND EXISTS (SELECT 1 FROM LCCONT WHERE CONTNO = LCINSURED.CONTNO AND SALECHNL IN ('1', '3', '4') and LCCONT.polapplydate <= trunc(sysdate) and LCCONT.polapplydate > trunc(sysdate-365))) ");
			sql.append(" UNION ");
			// 既往有延期、拒保的保单
			sql
					.append(" SELECT 1 as field1, 10 as flag FROM LDSYSVAR WHERE SYSVAR = 'onerow' AND (SELECT COUNT(1) FROM LCUWMASTER WHERE INSUREDNO = ? AND PASSFLAG IN ('1', '2')) + (SELECT COUNT(1) FROM LCCUWMASTER WHERE INSUREDNO = ? AND PASSFLAG IN ('1', '2')) + (SELECT COUNT(1) FROM LLUWMASTER WHERE INSUREDNO = ? AND PASSFLAG IN ('1', '2')) + (SELECT COUNT(1) FROM LLCUWMASTER WHERE INSUREDNO = ? AND PASSFLAG IN ('1', '2')) + (SELECT COUNT(1) FROM LPUWMASTER WHERE INSUREDNO = ? AND PASSFLAG IN ('1', '2')) + (SELECT COUNT(1) FROM LPCUWMASTER WHERE INSUREDNO = ? AND PASSFLAG IN ('1', '2')) > 0 ");
			sql.append(" UNION ");
			// 既往投保健康告知异常
			sql
					//.append("SELECT 1 as field1, 11 as flag FROM lccont m WHERE m.contno <> ? AND m.insuredno = ? AND EXISTS (SELECT 1 FROM lccustomerimpartparams a WHERE EXISTS (SELECT 1 FROM ldimpart b WHERE b.patchno    = a.patchno AND b.impartver    = a.impartver AND b.impartcode   = a.impartcode AND b.uwclaimflg   = '1' AND ( b.persontype = '3' OR ( b.persontype  = a.customernotype AND b.persontype  IN ( '0' ,'1' ) ) )) AND a.impartver  in('GX10','GX8') AND contno= m.contno AND impartparamname = 'YesOrNo' AND impartparam     = '1') AND EXISTS (SELECT 1 FROM LCCUWMASTER C WHERE C.CONTNO = M.CONTNO AND C.PASSFLAG <> '9') ");
					.append("SELECT 1 as field1, 11 as flag FROM lccont m WHERE m.contno <> ? AND m.insuredno = ? AND EXISTS (SELECT 1 FROM lccustomerimpartparams a WHERE EXISTS (SELECT 1 FROM ldimpart b WHERE b.patchno = a.patchno AND b.impartver = a.impartver AND b.impartcode = a.impartcode AND b.uwclaimflg = '1' AND (b.persontype = '3' OR (b.persontype = a.customernotype AND b.persontype IN ('0', '1')))) AND a.impartver = '20181' AND ((a.impartcode in ('12', '13', '14', '15', '16', '17', '18') and a.patchno = '19043') or (a.impartcode in ('A1-A5', 'B1-B10') and a.patchno = '19057')) AND contno = m.contno AND impartparamname = 'YesOrNo' AND impartparam = '1') AND EXISTS (SELECT 1 FROM LCCUWMASTER C WHERE C.CONTNO = M.CONTNO AND C.PASSFLAG <> '9') ");

			sql.append(" UNION ");
			// 既往保单有体检回复记录
			sql
					.append(" SELECT 1 as field1, 12 as flag FROM LCPENOTICE L,LCPENOTICERESULT LS WHERE L.CUSTOMERNO = ? AND L.CONTNO = LS.CONTNO AND L.CONTNO <> ? AND L.PRTSEQ = LS.PRTSEQ AND LS.PRTSEQ IS NOT NULL ");
			sql.append(" UNION ");
			// 既往有问题件未回复撤单,开门红修改,不校验契约岗位问题件
			sql
					//.append(" SELECT 1 as field1, 13 as flag FROM LCCONT WHERE CONTNO <> ? AND INSUREDNO = ? AND (UWFLAG = 'a' OR STATE LIKE '1002%' OR STATE LIKE '1003%' OR STATE LIKE '1005%') AND EXISTS (SELECT 1 FROM LOPRTMANAGER WHERE OTHERNO = CONTNO AND CODE IN ('81') AND STATEFLAG <> '2') ");
			        .append("SELECT 1 as field1, 13 as flag FROM LCCONT WHERE CONTNO <> ? AND INSUREDNO = ? AND (UWFLAG = 'a' OR STATE LIKE '004%' OR STATE LIKE '005%' OR STATE LIKE '006%') AND EXISTS (SELECT 1 FROM LOPRTMANAGER WHERE OTHERNO = CONTNO AND CODE IN ('TB81') AND STATEFLAG < '2')");
			sql.append(" UNION ");
			// 既往投保职业代码与本次一致
			//客户内部号码
			/*String sequenceno = "";
			for(LCInsuredPojo lcInsuredPojo:lcInsuredPojos){
				if(clientNo.equals(lcInsuredPojo.getInsuredNo()) && lcInsuredPojo.getOccupationType() != null && !"".equals(lcInsuredPojo.getOccupationType()) && Integer.parseInt(lcInsuredPojo.getOccupationType()) < 5){
					sequenceno = (lcInsuredPojo.getSequenceNo() == null)?sequenceno : lcInsuredPojo.getSequenceNo();
				}
			}
			sql
					.append(" select 1 as field1, 14 as flag from lcinsured b where '"+sequenceno+"' ='1' and b.occupationtype>'4' and b.insuredno= ?");
			sql.append(" UNION ");*/
			// 既往有防癌险或重疾险理赔记录
			sql
					.append(" SELECT 1 as field1, 15 as flag  from ljagetclaim  t where t.kindcode='S' and exists (select  1 from llregister e where e.rgtno=t.otherno and e.rgtobj='1' and e.clmstate>=20) and exists (select 1 from llsubreport s where s.subrptno=t.otherno and s.customerno= ?  ) union  select   1 as field1, 15 as flag   from ljagetclaim  t where t.kindcode='S' and exists (select  1 from llregister e where e.rgtno=t.otherno and e.rgtobj='2' and e.clmstate>=20) and exists (select 1 from llsubreport s where s.caseno=t.otherno and s.customerno=?)");
			sql.append(" UNION ");
			// 既往有意外险理赔记录
			sql
					.append(" SELECT 1 as field1, 16 as flag  from ljagetclaim  t where t.kindcode in ('A') and exists (select  1 from llregister e where e.rgtno=t.otherno and e.rgtobj='1' and e.clmstate >= 20) and exists (select 1 from llsubreport s where s.subrptno=t.otherno and s.customerno= ? ) union  select  1 as field1, 16 as flag   from ljagetclaim  t where t.kindcode in ('A') and exists (select  1 from llregister e where e.rgtno=t.otherno and e.rgtobj='2' and e.clmstate >= 20) and exists (select 1 from llsubreport s where s.caseno=t.otherno and s.customerno=?) ");
			sql.append(" UNION ");
			// 被保人既往有理赔记录
			sql
					.append("SELECT 1 as field, 17  as flag FROM DUAL WHERE (SELECT count(1) FROM llcase WHERE CUSTOMERNO = ?) <> 0 or (SELECT count(1) FROM LLREGISTER WHERE CUSTOMERNO = ?) <> 0 or (SELECT count(1) FROM LLSUBREPORT WHERE CUSTOMERNO = ?) <> 0 ");

			/*sql.append(" UNION ");*/
			/*//被保人团险有意外险理赔记录
			sql
					.append("select 1 as filed1, 18 as flag from dual where (select count(1) from llclaimdetail a where a.contno in (select lc.contno from lccont lc where lc.insuredno = ?) and a.kindcode = 'A') <> 0 or (select count(1) from llclaimdetail a where a.contno in (select lc.contno from lccont lc where lc.insuredno = ?) and a.kindcode = 'A') <> 0 ");
			sql.append(" UNION ");*/
			//被保险人团险防癌重疾险理赔记录
		/*	sql
					.append("select 1 as filed1, 19 as flag from dual where (select count(1) from llclaimdetail a where a.contno in (select lc.contno from lccont lc where lc.insuredno = ?) and a.kindcode = 'S') <> 0 or (select count(1) from llclaimdetail a where a.contno in (select lc.contno from lccont lc where lc.insuredno = ?) and a.kindcode = 'S') <> 0 ");
			sql.append(" UNION ");*/
			//被保险人团险有非健医卡和意外医疗险的理赔记录
			/*sql
					.append("SELECT 1 as field1, 20 as flag FROM DUAL WHERE (SELECT count(1) FROM LLSubReport b WHERE b.customerno = ? and not exists (SELECT 1 FROM llclaimreportdetail a WHERE a.customerno = b.customerno and (a.clmno = b.caseno or a.clmno = b.SUBRPTNO) and (a.getdutykind = '100' or a.riskcode = '7824'))) <> 0 OR (SELECT count(1) FROM LLSUBREPORT b WHERE b.customerno = ? and not exists (SELECT 1 FROM llclaimreportdetail a WHERE a.customerno = b.customerno and (a.clmno = b.caseno or a.clmno = b.SUBRPTNO) and (a.getdutykind = '100' or a.riskcode = '7824'))) <> 0 OR (SELECT count(1) FROM LLRegister b WHERE b.clmstate > '10' and b.customerno = ? and not exists (SELECT 1 FROM llclaimdetail a WHERE a.customerno = b.customerno and a.clmno = b.RGTNO and (a.getdutykind = '100' or a.riskcode = '7824'))) <> 0 OR (SELECT count(1) FROM LLREGISTER b WHERE b.clmstate > '10' and b.customerno = ? and not exists (SELECT 1 FROM llclaimdetail a WHERE a.customerno = b.customerno and a.clmno = b.RGTNO and (a.getdutykind = '100' or a.riskcode = '7824'))) <> 0 ");
			sql.append(" UNION ");*/
			//被保险人既往有未结案的意外医疗理赔记录
		/*	sql
					.append("SELECT 1 as field1, 21 as flag FROM DUAL WHERE (SELECT count(1) FROM LLSubReport b WHERE b.customerno = ? and not exists (SELECT 1 FROM LLRegister l WHERE (l.RGTNO = b.SUBRPTNO or l.RGTNO = b.caseno)) and exists (SELECT 1 FROM llclaimreportdetail a WHERE a.customerno = b.customerno and (a.clmno = b.caseno or a.clmno = b.SUBRPTNO) and a.getdutykind = '100')) <> 0 OR (SELECT count(1) FROM LLSubReport b WHERE b.customerno = ? and not exists (SELECT 1 FROM LLRegister l WHERE (l.RGTNO = b.SUBRPTNO or l.RGTNO = b.caseno)) and exists (SELECT 1 FROM llclaimreportdetail a WHERE a.customerno = b.customerno and (a.clmno = b.caseno or a.clmno = b.SUBRPTNO) and a.getdutykind = '100')) <> 0 OR (SELECT count(1) FROM LLSubReport b WHERE b.customerno = ? and exists (SELECT 1 FROM LLRegister l WHERE l.clmstate < '50' and (l.RGTNO = b.SUBRPTNO or l.RGTNO = b.caseno)) and exists (SELECT 1 FROM llclaimreportdetail a WHERE a.customerno = b.customerno and (a.clmno = b.caseno or a.clmno = b.SUBRPTNO) and a.getdutykind = '100')) <> 0 OR (SELECT count(1) FROM LLSubReport b WHERE b.customerno = ? and exists (SELECT 1 FROM LLRegister l WHERE l.clmstate < '50' and (l.RGTNO = b.SUBRPTNO or l.RGTNO = b.caseno)) and exists (SELECT 1 FROM llclaimreportdetail a WHERE a.customerno = b.customerno and (a.clmno = b.caseno or a.clmno = b.SUBRPTNO) and a.getdutykind = '100')) <> 0 OR (SELECT count(1) FROM LLRegister b WHERE b.clmstate < '50' and customerno = ? and exists (SELECT 1 FROM llclaimdetail a WHERE a.customerno = b.customerno and a.clmno = b.RGTNO and a.getdutykind = '100')) <> 0 OR (SELECT count(1) FROM LLRegister b WHERE b.clmstate < '50' and customerno = ? and exists (SELECT 1 FROM llclaimdetail a  WHERE a.customerno = b.customerno and a.clmno = b.RGTNO and a.getdutykind = '100')) <> 0");
*/
			//被保险人7060有理赔二核不自动续保结论
			sql.append(" UNION ");
			sql
					.append("select 1 as field1, 22 as flag from lluwmaster a, lcpol b where a.passflag = 'b' AND b.riskcode = '7060' and b.insuredno = ? and a.polno = b.polno");

			//被保险人7061有理赔二核不自动续保结论
			sql.append(" UNION ");
			sql
					.append("select 1 as field1, 23 as flag from lluwmaster a, lcpol b where a.passflag = 'b' AND b.riskcode = '7061' and b.insuredno = ? and a.polno = b.polno");
			if(beforContno !=null && !"".equals(beforContno)) {
				//上一保单年度有次标准体承保记录(各渠道)
				sql.append(" UNION ");
				sql.append(SQLConstant.InsuredInfo.HAVESUBEXAMINELY);
				//上一保单年度有拒保、延期记录(各渠道)
				sql.append(" UNION ");
				sql.append(SQLConstant.InsuredInfo.HAVEDEFERPOLICYLY);
				//上一保单年度有理赔记录(除建医卡\小额医疗\7060理赔二核为标准承保的理赔)
				sql.append(" UNION ");
				sql.append(SQLConstant.InsuredInfo.HAVECLAIMS7060SPECILSQL);
				//上一保单年度有理赔记录(除建医卡、小额医疗、7061理赔二核为标准承保)
				sql.append(" UNION　");
				sql.append(SQLConstant.InsuredInfo.HAVECLAIMS7061SPECILSQL);
			}
			tVData.add(sql.toString());
			tVData.add(tParam);
			// 既往有体检、加费、特约的保单
			/*tParam.setNameAndValue("CUSTOMERNO1", "string:" + clientNo);
			tParam.setNameAndValue("INSUREDNO1", "string:" + clientNo);*/
			// 既往有理赔记录
			/*if (!clientNo.equals(applicantInfo.getClientNo())) {
				tParam.setNameAndValue("CUSTOMERNO2", "string:" + clientNo);
				tParam.setNameAndValue("CUSTOMERNO3", "string:" + clientNo);
			}*/
			// 既往有保全二核
			tParam.setNameAndValue("INSUREDNO2", "string:" + clientNo);
			tParam.setNameAndValue("INSUREDNO3", "string:" + clientNo);
			// 既往有生调记录
			tParam.setNameAndValue("CUSTOMERNO4", "string:" + clientNo);
			// 既往有投保经历
			// 既往有次标准体记录
			tParam.setNameAndValue("CONTNO2", "string:" + contno);
			tParam.setNameAndValue("INSUREDNO3", "string:" + clientNo);
			tParam.setNameAndValue("CONTNO2", "string:" + contno);
			tParam.setNameAndValue("INSUREDNO4", "string:" + clientNo);
			tParam.setNameAndValue("CONTNO3", "string:" + contno);
			tParam.setNameAndValue("INSUREDNO5", "string:" + clientNo);
			tParam.setNameAndValue("CONTNO4", "string:" + contno);
			tParam.setNameAndValue("INSUREDNO6", "string:" + clientNo);
			tParam.setNameAndValue("CONTNO5", "string:" + contno);
			tParam.setNameAndValue("INSUREDNO7", "string:" + clientNo);
			tParam.setNameAndValue("CONTNO6", "string:" + contno);
			tParam.setNameAndValue("INSUREDNO8", "string:" + clientNo);
			// 既往有体检未完成记录

				tParam.setNameAndValue("INSUREDNO9", "string:" + clientNo);
				tParam.setNameAndValue("CONTNO7", "string:" + contno);

			// 既往告知为五、六类或拒保职业
			tParam.setNameAndValue("CONTNO8", "string:" + contno);
			tParam.setNameAndValue("INSUREDNO10", "string:" + clientNo);
			tParam.setNameAndValue("InsuJobType", "string:"
					+ insured.getOccuType());
			// 职业类别低于既往告知
			tParam.setNameAndValue("INSUREDNO11", "string:" + clientNo);
			tParam.setNameAndValue("OCCUPATIONTYPE", "string:"
					+ insured.getOccuType());
			tParam.setNameAndValue("INSUREDNO12", "string:" + clientNo);
			tParam.setNameAndValue("CONTNO9", "string:" + contno);
			// 既往有延期、拒保的保单
			tParam.setNameAndValue("INSUREDNO13", "string:" + clientNo);
			tParam.setNameAndValue("INSUREDNO14", "string:" + clientNo);
			tParam.setNameAndValue("INSUREDNO15", "string:" + clientNo);
			tParam.setNameAndValue("INSUREDNO16", "string:" + clientNo);
			tParam.setNameAndValue("INSUREDNO17", "string:" + clientNo);
			tParam.setNameAndValue("INSUREDNO18", "string:" + clientNo);
			// 既往投保健康告知异常
			tParam.setNameAndValue("CONTNO10", "string:" + contno);
			tParam.setNameAndValue("INSUREDNO19", "string:" + clientNo);
			// 既往保单有体检回复记录
			tParam.setNameAndValue("INSUREDNO20", "string:" + clientNo);
			tParam.setNameAndValue("CONTNO11", "string:" + contno);
			// 既往有问题件未回复撤单
			tParam.setNameAndValue("CONTNO12", "string:" + contno);
			tParam.setNameAndValue("INSUREDNO21", "string:" + clientNo);
			// 既往投保职业代码与本次一致
			//tParam.setNameAndValue("CONTNO13", "string:" + clientNo);
			//既往有防癌险或重疾险理赔记录
			tParam.setNameAndValue("INSUREDNO22", "string:" + clientNo);
			tParam.setNameAndValue("INSUREDNO23", "string:" + clientNo);
			//既往有意外险理赔记录
			tParam.setNameAndValue("INSUREDNO24", "string:" + clientNo);
			tParam.setNameAndValue("INSUREDNO25", "string:" + clientNo);
			// 被保人既往有理赔记录
			tParam.setNameAndValue("GROUPCUSTOMERNO1", "string:" + clientNo);
			tParam.setNameAndValue("GROUPCUSTOMERNO2", "string:" + clientNo);
			tParam.setNameAndValue("GROUPCUSTOMERNO3", "string:" + clientNo);
			/*//被保人团险有意外险理赔记录
			tParam.setNameAndValue("GROUPCUSTOMERNO5", "string:" + clientNo);
			tParam.setNameAndValue("GROUPCUSTOMERNO6", "string:" + clientNo);*/
			//被保险人团险防癌重疾险理赔记录
			/*tParam.setNameAndValue("GROUPCUSTOMERNO7", "string:" + clientNo);
			tParam.setNameAndValue("GROUPCUSTOMERNO8", "string:" + clientNo);*/
			//被保险人团险有非健医卡和意外医疗险的理赔记录
		/*	tParam.setNameAndValue("GROUPCUSTOMERNO9", "string:" + clientNo);
			tParam.setNameAndValue("GROUPCUSTOMERNO10", "string:" + clientNo);
			tParam.setNameAndValue("GROUPCUSTOMERNO11", "string:" + clientNo);
			tParam.setNameAndValue("GROUPCUSTOMERNO12", "string:" + clientNo);*/
			//被保险人既往有未结案的意外医疗理赔记录
			/*tParam.setNameAndValue("GROUPCUSTOMERNO13", "string:" + clientNo);
			tParam.setNameAndValue("GROUPCUSTOMERNO14", "string:" + clientNo);
			tParam.setNameAndValue("GROUPCUSTOMERNO15", "string:" + clientNo);
			tParam.setNameAndValue("GROUPCUSTOMERNO16", "string:" + clientNo);
			tParam.setNameAndValue("GROUPCUSTOMERNO17", "string:" + clientNo);
			tParam.setNameAndValue("GROUPCUSTOMERNO18", "string:" + clientNo);*/
			//被保险人7060有理赔二核不自动续保结论
			tParam.setNameAndValue("INSUREDNO27", "string:" + clientNo);
			//被保险人7061有理赔二核不自动续保结论
			tParam.setNameAndValue("INSUREDNO28", "string:" + clientNo);
			if(beforContno !=null && !"".equals(beforContno)) {
				//上一保单年度有次标准体承保记录(各渠道)24
				for (int k = 0; k < 6; k++) {
					tParam.setNameAndValue("CONTNO1_0_" + k, "string:" + beforContno);
					tParam.setNameAndValue("INSUREDNO29_" + k, "string:" + clientNo);
					tParam.setNameAndValue("CONTNO1_1_" + k, "string:" + beforContno);
					tParam.setNameAndValue("CONTNO1_2_" + k, "string:" + beforContno);
				}
				//上一保单年度有拒保、延期记录(各渠道)25
				//上一保单年度有理赔记录(除建医卡\小额医疗\7060理赔二核为标准承保的理赔)26
				//上一保单年度有理赔记录(除建医卡、小额医疗、7061理赔二核为标准承保)27
				for (int k = 0; k < 10; k++) {
					tParam.setNameAndValue("INSUREDNO" + k, "string:" + clientNo);
					tParam.setNameAndValue("CONTNO1_3_" + k, "string:" + beforContno);
					tParam.setNameAndValue("CONTNO1_4_" + k, "string:" + beforContno);
				}
			}
			logger.info("PastInsured：" + sql);
			long beginTime = System.currentTimeMillis();
			SSRS tSSRS = new SSRS();
			Connection tConnection = null;
			try {
				tConnection = DBConnPool.getConnection("basedataSource");
				ExeSQL exeSql = new ExeSQL(tConnection);
				tSSRS = exeSql.execSQL(tVData);
			}catch (Exception e){
				e.printStackTrace();
				logger.error(ExceptionUtils.exceptionToString(e));
				tradeInfo.addError(ExceptionUtils.exceptionToString(e));
				return;
			}finally {
				if (tConnection != null) {
					try {
						tConnection.close();
					} catch (SQLException e) {
						e.printStackTrace();
						tradeInfo.addError(ExceptionUtils.exceptionToString(e));
						logger.error(ExceptionUtils.exceptionToString(e));
						return;
					}
				}
			}
			if(tSSRS == null){
				logger.debug("保单号：" + lcInsuredPojos.get(0).getContNo() + "，被保人既往提数访问老核心SQL发生异常！");
				tradeInfo.addError("保单号：" + lcInsuredPojos.get(0).getContNo() + "，被保人既往提数访问老核心SQL发生异常！");
				return;
			}
			logger.debug("被保人既往提数：" + (System.currentTimeMillis() - beginTime)
					/ 1000.0 + "s");
			int flag = 0;
			if (null != tSSRS && tSSRS.MaxRow > 0) {
				for (int j = 1; j <= tSSRS.MaxRow; j++) {
					Map map =null;
					int data = Util.toInt(tSSRS.GetText(j, 1));
					flag = Util.toInt(tSSRS.GetText(j, 2));
					switch (flag) {
						case 1:
							// 既往有体检、加费、特约的保单
							insured.setHaveExaminePolicy(true);
							break;
						/*case 2:
							// 既往有理赔记录
							insured.setHaveClaims(true);
							break;*/
						case 3:
							// 既往有保全二核
							insured.setHavePreserve(true);
							break;
						case 4:
							// 既往有生调记录
							insured.setHaveAscertain(true);
							break;
						case 5:
							// 既往有投保经历
							insured.setHaveApptHistory(true);
							break;
						case 6:
							// 既往有次标准体记录
							insured.setHaveSubExamine(true);
							break;
						case 7:
							// 既往有体检未完成记录
							insured.setHaveUnfiExamine(true);
							break;
						case 8:
							// 既往告知为五、六类或拒保职业
							insured.setHaveRejectionOcc(true);
							break;
						case 9:
							// 职业类别低于既往告知
							insured.setInsJobTypeThaBefo(true);
							break;
						case 10:
							// 既往有延期、拒保的保单
							insured.setHaveDeferPolicy(true);
							break;
						case 11:
							// 既往投保健康告知异常
							insured.setHaveHealAbnormal(true);
							break;
						case 12:
							// 既往保单有体检回复记录
							insured.setHaveExamineReply(true);
							break;
						case 13:
							// 既往有问题件未回复撤单
							insured.setHaveProb(true);
							break;
						/*case 14:
							// 既往投保职业代码与本次一致
							insured.setHaveSameOccCode(true);
							break;*/
						case 15:

							if(insured.getExtendMap()==null){
								map = new HashMap();

							}else{
								map = insured.getExtendMap();
							}

							map.put("既往有防癌险或重疾险理赔记录", "true");
							// 既往有防癌险或重疾险理赔记录
							insured.setExtendMap(map);
							break;
						case 16:
							if(insured.getExtendMap()==null){
								map = new HashMap();

							}else{
								map = insured.getExtendMap();
							}
							map.put("既往有意外险理赔记录", "true");
							// 既往有意外险理赔记录
							insured.setExtendMap(map);
							break;
						case 17:
							// 既往有理赔记录
							insured.setHaveClaims(true);
							break;
						/*case 18:
							// 团险有意外险理赔记录
							insured.setAccidentClaimRecord(true);
							break;*/
						/*case 19:
							// 团险防癌重疾险理赔记录
							insured.setCancerClaimRecord(true);
							break;*/
						/*case 20:
							// 团险有非健医卡和意外医疗险的理赔记录
							insured.setClaimsNoMedic(true);
							break;
						case 21:
							// 团险既往有未结案的意外医疗理赔记录
							insured.setAcMedicClaimNotEnd(true);
							break;*/
						case 22:
							//被保险人7060有理赔二核不自动续保结论
							insured.setHavePayFor7060(true);
							break;
						case 23:
							//被保险人7061有理赔二核不自动续保结论
							insured.setHavePayFor7061(true);
							break;
						case 24:
							if (data==1) {
								//上一保单年度有次标准体承保记录(各渠道)
								insured.setHaveSubExamineLY(true);
							}
							break;
						case 25:
							if (data==1) {
								//上一保单年度有拒保、延期记录(各渠道)
								insured.setHaveDeferPolicyLY(true);
							}
							break;
						case 26:
							if (data==1) {
								//上一保单年度有理赔记录(除建医卡\小额医疗\7060理赔二核为标准承保的理赔)
								insured.setHaveClaims7060Specil(true);
							}
							break;
						case 27:
							if (data==1) {
								//上一保单年度有理赔记录(除建医卡、小额医疗、7061理赔二核为标准承保)
								insured.setHaveClaims7061Specil(true);
							}
							break;
					}
				}
			}
			//投被为同一人时，下列数据取投保人提数
			if (clientNo.equals(applicantInfo.getClientNo())) {

				// 既往有理赔记录
				//insured.setHaveClaims(applicantInfo.isHaveClaims());

				// 既往有投保经历
				//insured.setHaveApptHistory(applicantInfo.isHaveApptHistory());

				// 既往有体检未完成记录
				//insured.setHaveUnfiExamine(applicantInfo.isHaveUnfiExamine());
			}
		}
	}

	/**
	 * 获取既往挡板数据
	 * @param policy
	 */
	public void getBarrierData(Policy policy){
		Applicant applicantInfo = policy.getApplicant();
		// 既往有体检记录
		//applicantInfo.setHaveCheckNote(false);
		// 既往有体检、加费、特约的保单
		//applicantInfo.setHaveExaminePolicy(false);
		// 既往有理赔记录
		//applicantInfo.setHaveClaims(false);
		// 既往有保全二核
		//applicantInfo.setHavePreserve(false);
		// 既往有生调记录
		//applicantInfo.setHaveAscertain(false);
		// 既往有投保经历
		//applicantInfo.setHaveApptHistory(false);
		// 既往有次标准体记录
		//applicantInfo.setHaveSubExamine(false);
		// 既往有体检未完成记录
		//applicantInfo.setHaveUnfiExamine(false);
		// 既往告知为五、六类或拒保职业
		/*applicantInfo.setHaveRejectionOcc(false);*/
		// 职业类别低于既往告知
		applicantInfo.setAppntJobTypeThaBefo(false);

		List<Insured> insuredList = policy.getInsuredList();
		for(Insured insured : insuredList){
			// 既往有体检、加费、特约的保单
			insured.setHaveExaminePolicy(false);
			// 既往有理赔记录
			insured.setHaveClaims(false);
			// 既往有保全二核
			insured.setHavePreserve(false);
			// 既往有生调记录
			insured.setHaveAscertain(false);
			// 既往有投保经历
			insured.setHaveApptHistory(false);
			// 既往有次标准体记录
			insured.setHaveSubExamine(false);
			// 既往有体检未完成记录
			insured.setHaveUnfiExamine(false);
			// 既往告知为五、六类或拒保职业
			insured.setHaveRejectionOcc(false);
			// 职业类别低于既往告知
			insured.setInsJobTypeThaBefo(false);
			// 既往有延期、拒保的保单
			insured.setHaveDeferPolicy(false);
			// 既往投保健康告知异常
			insured.setHaveHealAbnormal(false);
			// 既往保单有体检回复记录
			insured.setHaveExamineReply(false);
			// 既往有问题件未回复撤单
			insured.setHaveProb(false);
			// 既往投保职业代码与本次一致
			insured.setHaveSameOccCode(false);
			Map map =null;
			if(insured.getExtendMap()==null){
				map = new HashMap();

			}else{
				map = insured.getExtendMap();
			}
			map.put("既往有防癌险或重疾险理赔记录", "false");
			// 既往有防癌险或重疾险理赔记录
			insured.setExtendMap(map);
			if(insured.getExtendMap()==null){
				map = new HashMap();
			}else{
				map = insured.getExtendMap();
			}
			map.put("既往有意外险理赔记录", "false");
			// 既往有意外险理赔记录
			insured.setExtendMap(map);
		}
	}
}
