package com.sinosoft.cloud.cbs.function;

import com.sinosoft.lis.db.LXBlackListIndvDB;
import com.sinosoft.lis.entity.LCAppntPojo;
import com.sinosoft.lis.vschema.LXBlackListIndvSet;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * @Author:WangShuliang
 * @Description:
 * @Date:Created in 13:49 2017/12/20
 * @Modified by:
 */
@Component
public class UWFunction {

    /**
     * 被保人身高异常SQL,未成年
     * @param sex
     * @param birthday
     * @return SSRS
     */
    //@Cacheable(value = "checkHeightErr1", key = "'sex'+#sex+'cValiDate'+#cValiDate+'birthday'+#birthday")
    public SSRS checkHeightErr1(String sex, String cValiDate, String birthday){
        StringBuffer tSql = new StringBuffer();
        tSql.append("SELECT S.MINSTATURE,S.MAXSTATURE,S.MINAVOIRDUPOIS,S.MAXAVOIRDUPOIS FROM LDPERSONBUILD S WHERE 'M' = S.AGEFLAG AND S.SEX=? AND MONTHS_BETWEEN(TO_DATE(?, 'YYYY-MM-DD'), TO_DATE(?, 'YYYY-MM-DD')) >= " +
                " S.MINAGE AND MONTHS_BETWEEN(TO_DATE(?, 'YYYY-MM-DD'), TO_DATE(?, 'YYYY-MM-DD')) < S.MAXAGE");
        TransferData tparam = new TransferData();
        tparam.setNameAndValue("0", "string:" + sex);
        tparam.setNameAndValue("1", "string:" + cValiDate);
        tparam.setNameAndValue("2", "string:" + birthday);
        tparam.setNameAndValue("3", "string:" + cValiDate);
        tparam.setNameAndValue("4", "string:" + birthday);
        VData tBVData = new VData();
        tBVData.add(tSql.toString());
        tBVData.add(tparam);
        ExeSQL pExeSQL = new ExeSQL();
        SSRS tSSRS = pExeSQL.execSQL(tBVData);
        return tSSRS;
    }

    /**
     * 证件号与和黑名单相同,不分国籍,>0触发
     * @param idNO
     * @return
     */
    //@Cacheable(value = "checkBlackIdFlag", key = "'idNO'+#idNO")
    public boolean checkBlackIdFlag(String idNO){
        StringBuffer tSql = new StringBuffer();
        tSql.append("select count(1) from LXBlackListIndv a, lxidtypeorno c where 1=1");
        //tSql.append(" and a.listproperty = 'Sanctions Lists' ");
        tSql.append(" and c.blacklistid = a.blacklistid ");
        tSql.append(" and c.idno = ? ");
        TransferData tparam = new TransferData();
        tparam.setNameAndValue("0", "string:" + idNO);
        VData tBVData = new VData();
        tBVData.add(tSql.toString());
        tBVData.add(tparam);
        ExeSQL pExeSQL = new ExeSQL();
        SSRS tSSRS = pExeSQL.execSQL(tBVData);
        if("0".equals(tSSRS.GetText(1,1))){
            return false;
        }
        return true;
    }


    /**
     * 中国客户名字、生日与黑名单相同,>0触发
     * @param name
     * @param birthday
     * @return
     */
    //@Cacheable(value = "checkBlackCHNFlag", key = "'name'+#name+'birthday'+#birthday")
    public boolean checkBlackCHNFlag(String name,String birthday){
        StringBuffer tSql = new StringBuffer();
       /* tSql.append("select count(distinct(count(e.blacklistid))) from LXBlackListIndv a,lxblackdate e where 1=1 ");
        //and a.listproperty = 'Sanctions Lists'
        tSql.append(" and a.blacklistid = e.blacklistid and a.name = ? ");
        tSql.append(" group by e.blacklistid, e.blackdate ");
        tSql.append(" having (select case when ");*/
        tSql.append("SELECT DECODE(COUNT(e.blacklistid),0,0,1) from LXBlackListIndv a,lxblackdate e where 1=1 " );
        tSql.append(" and a.blacklistid = e.blacklistid and a.name = ? ");
        tSql.append(" AND (select case when ");
        tSql.append("((select count(1) from dual where (select count(1) " +
                " from lxblackdate p " +
                " where p.blacklistid = e.blacklistid) >= 1 and length(e.blackdate) = 4 and e.blackdate = " +
                " substr(?, 1, 4))) > 0 then 1 " +
                " else case when ? >= (" +
                " select min(blackdate) from lxblackdate where blacklistid = e.blacklistid) and ? <= " +
                " (select max(blackdate) from lxblackdate where blacklistid = e.blacklistid) then 1 " +
                " else 0 end end from dual) > 0");
        TransferData tparam = new TransferData();
        tparam.setNameAndValue("0", "string:" + name);
        tparam.setNameAndValue("1", "string:" + birthday);
        tparam.setNameAndValue("2", "string:" + birthday);
        tparam.setNameAndValue("3", "string:" + birthday);

        VData tBVData = new VData();
        tBVData.add(tSql.toString());
        tBVData.add(tparam);
        ExeSQL pExeSQL = new ExeSQL();
        SSRS tSSRS = pExeSQL.execSQL(tBVData);
        if("0".equals(tSSRS.GetText(1,1))){
            return false;
        }
        return true;
    }


    /**
     * 客户部分组成,客户名与黑名单表四个名字对比,>0触发
     * @param name
     * @param nativePlace
     * @param birthday
     * @return
     */
    //@Cacheable(value = "checkBlackFlag1", key = "'name'+#name+'nativePlace'+#nativePlace+'birthday'+#birthday")
    public boolean checkBlackFlag1(String name, String nativePlace, String birthday){
        StringBuffer tSql = new StringBuffer();
        tSql.append("select (case when (select firsttab.num from (select count(distinct(e.blacklistid)) as num from LXBlackListIndv a, lxblackdate e  " +
  /* a.listproperty = 'Sanctions Lists' and */          " where  e.blacklistid = a.blacklistid and instr(?, ' ') = 0 and a.firstname = ? and (? is null or ? <> 'CHN')  " +
                " group by e.blacklistid, e.blackdate having (select case when ((select count(1) from dual where (select count(1) from lxblackdate p where p.blacklistid =  " +
                " e.blacklistid) >= 1 and length(e.blackdate) = 4 and e.blackdate = substr(?, 1, 4))) > 0 then 1 else case when ? >= (select min(blackdate) from lxblackdate  " +
                " where blacklistid = e.blacklistid) and ? <= (select max(blackdate) from lxblackdate where blacklistid = e.blacklistid) then 1 else 0 end end from dual) > 0) firsttab  " +
                " where rownum = 1) > 0 then 1 else 0 end   " +
                " + case when (select middletab.num from (select count(distinct(e.blacklistid)) as num from LXBlackListIndv a, lxblackdate e where   " +   //a.listproperty = 'Sanctions Lists'
 /*   and  */   "  e.blacklistid = a.blacklistid and instr(?, ' ') = 0 and a.middlename = ? and (? is null or ? <> 'CHN') group by e.blacklistid, e.blackdate having (select case  " +
                " when ((select count(1) from dual  where (select count(1) from lxblackdate p where p.blacklistid = e.blacklistid) >= 1 and length(e.blackdate) = 4 and e.blackdate =  " +
                " substr(?, 1, 4))) > 0 then 1 else case when ? >= (select min(blackdate) from lxblackdate where blacklistid = e.blacklistid) and ? <= (select max(blackdate)  " +
                " from lxblackdate where blacklistid = e.blacklistid) then 1 else 0 end end from dual) > 0) middletab where rownum = 1) > 0 then 1 else 0 end   " +
                " + case when (select nametab.num from (select count(distinct(e.blacklistid)) as num from LXBlackListIndv a, lxblackdate e where  " +       //a.listproperty = 'Sanctions Lists'  and
                "  e.blacklistid = a.blacklistid and instr(?, ' ') = 0 and a.surname = ? and (? is null or ? <> 'CHN') group by e.blacklistid, e.blackdate  " +
                " having (select case when ((select count(1) from dual where (select count(1) from lxblackdate p where p.blacklistid = e.blacklistid) >= 1 and length(e.blackdate) = 4  " +
                " and e.blackdate = substr(?, 1, 4))) > 0 then 1 else case when ? >= (select min(blackdate) from lxblackdate where blacklistid = e.blacklistid) and ? <=  " +
                " (select max(blackdate) from lxblackdate where blacklistid = e.blacklistid) then 1 else 0 end end from dual) > 0) nametab where rownum = 1) > 0 then 1 else 0 end  " +
                " + case when (select surntab.num from (select count(distinct(e.blacklistid)) as num from LXBlackListIndv a, lxblackdate e where   " +    //a.listproperty = 'Sanctions Lists' and
                "  e.blacklistid = a.blacklistid and instr(?, ' ') = 0 and a.name = ? and (? is null or ? <> 'CHN') group by e.blacklistid, e.blackdate having (select case  " +
                " when ((select count(1) from dual where (select count(1) from lxblackdate p where p.blacklistid = e.blacklistid) >= 1 and length(e.blackdate) = 4  " +
                " and e.blackdate = substr(?, 1, 4))) > 0 then 1 else case when ? >= (select min(blackdate) from lxblackdate where blacklistid = e.blacklistid) and ? <=  " +
                " (select max(blackdate) from lxblackdate where blacklistid = e.blacklistid) then 1 else 0 end end from dual) > 0) surntab where rownum = 1) > 0 then  1  " +
                " else 0 end) from dual  ");
        TransferData tparam = new TransferData();
        tparam.setNameAndValue("0", "string:" + name);
        tparam.setNameAndValue("1", "string:" + name);
        tparam.setNameAndValue("2", "string:" + nativePlace);
        tparam.setNameAndValue("3", "string:" + nativePlace);
        tparam.setNameAndValue("4", "string:" + birthday);
        tparam.setNameAndValue("5", "string:" + birthday);
        tparam.setNameAndValue("6", "string:" + birthday);
        tparam.setNameAndValue("7", "string:" + name);
        tparam.setNameAndValue("8", "string:" + name);
        tparam.setNameAndValue("9", "string:" + nativePlace);
        tparam.setNameAndValue("10", "string:" + nativePlace);
        tparam.setNameAndValue("11", "string:" + birthday);
        tparam.setNameAndValue("12", "string:" + birthday);
        tparam.setNameAndValue("13", "string:" + birthday);
        tparam.setNameAndValue("14", "string:" + name);
        tparam.setNameAndValue("15", "string:" + name);
        tparam.setNameAndValue("16", "string:" + nativePlace);
        tparam.setNameAndValue("17", "string:" + nativePlace);
        tparam.setNameAndValue("18", "string:" + birthday);
        tparam.setNameAndValue("19", "string:" + birthday);
        tparam.setNameAndValue("20", "string:" + birthday);
        tparam.setNameAndValue("21", "string:" + name);
        tparam.setNameAndValue("22", "string:" + name);
        tparam.setNameAndValue("23", "string:" + nativePlace);
        tparam.setNameAndValue("24", "string:" + nativePlace);
        tparam.setNameAndValue("25", "string:" + birthday);
        tparam.setNameAndValue("26", "string:" + birthday);
        tparam.setNameAndValue("27", "string:" + birthday);
        VData tBVData = new VData();
        tBVData.add(tSql.toString());
        tBVData.add(tparam);
        ExeSQL pExeSQL = new ExeSQL();
        SSRS tSSRS = pExeSQL.execSQL(tBVData);
        if("0".equals(tSSRS.GetText(1,1))){
            return false;
        }
        return true;
    }


    /**
     * 客户部分组成,客户名名与黑名单表2个名字对比,>0触发
     * @param name
     * @param nativePlace
     * @param birthday
     * @return
     */
    //@Cacheable(value = "checkBlackFlag2", key = "'name'+#name+'nativePlace'+#nativePlace+'birthday'+#birthday")
    public boolean checkBlackFlag2(String name, String nativePlace, String birthday){
        StringBuffer tSql = new StringBuffer();
        tSql.append("select (case when (select firsttab.num from (select count(distinct(e.blacklistid)) as num from LXBlackListIndv a, lxblackdate e where  " +    //a.listproperty = 'Sanctions Lists' and
                "  e.blacklistid = a.blacklistid and length(?) - length(replace(?, ' ')) = 1 and a.firstname = substr(?, 1, instr(?, ' ', 1) - 1) and (? is null or ? <> 'CHN') " +
                " group by e.blacklistid, e.blackdate having (select case when ((select count(1) from dual where (select count(1) from lxblackdate p where p.blacklistid = " +
                " e.blacklistid) >= 1 and length(e.blackdate) = 4 and e.blackdate = substr(?, 1, 4))) > 0 then 1 else case when ? >= (select min(blackdate) from lxblackdate " +
                " where blacklistid = e.blacklistid) and ? <= (select max(blackdate) from lxblackdate where blacklistid = e.blacklistid) then 1 else 0 end end from dual) > 0) firsttab " +
                " where rownum = 1) > 0 then 1 else 0 end " +
                " + case when (select surntab.num from (select count(distinct(e.blacklistid)) as num from LXBlackListIndv a, lxblackdate e where  " +             //a.listproperty = 'Sanctions Lists' and
                "  e.blacklistid = a.blacklistid and length(?) - length(replace(?, ' ')) = 1 and a.surname = substr(?, instr(?, ' ', 1) + 1, length(?)) " +
                " and (? is null or ? <> 'CHN') group by e.blacklistid, e.blackdate having (select case when ((select count(1) from dual where (select count(1) " +
                " from lxblackdate p where p.blacklistid = e.blacklistid) >= 1 and length(e.blackdate) = 4 and e.blackdate = substr(?, 1, 4))) > 0 then 1 else " +
                " case when ? >= (select min(blackdate) from lxblackdate where blacklistid = e.blacklistid) and ? <= (select max(blackdate) from lxblackdate " +
                " where blacklistid = e.blacklistid) then 1 else 0 end end from dual) > 0) surntab where rownum = 1) > 0 then 1 else 0 end)  from dual  ");
        TransferData tparam = new TransferData();
        tparam.setNameAndValue("0", "string:" + name);
        tparam.setNameAndValue("1", "string:" + name);
        tparam.setNameAndValue("2", "string:" + name);
        tparam.setNameAndValue("3", "string:" + name);
        tparam.setNameAndValue("4", "string:" + nativePlace);
        tparam.setNameAndValue("5", "string:" + nativePlace);
        tparam.setNameAndValue("6", "string:" + birthday);
        tparam.setNameAndValue("7", "string:" + birthday);
        tparam.setNameAndValue("8", "string:" + birthday);
        tparam.setNameAndValue("9", "string:" + name);
        tparam.setNameAndValue("10", "string:" + name);
        tparam.setNameAndValue("11", "string:" + name);
        tparam.setNameAndValue("12", "string:" + name);
        tparam.setNameAndValue("13", "string:" + name);
        tparam.setNameAndValue("14", "string:" + nativePlace);
        tparam.setNameAndValue("15", "string:" + nativePlace);
        tparam.setNameAndValue("16", "string:" + birthday);
        tparam.setNameAndValue("17", "string:" + birthday);
        tparam.setNameAndValue("18", "string:" + birthday);
        VData tBVData = new VData();
        tBVData.add(tSql.toString());
        tBVData.add(tparam);
        ExeSQL pExeSQL = new ExeSQL();
        SSRS tSSRS = pExeSQL.execSQL(tBVData);
        if("0".equals(tSSRS.GetText(1,1))){
            return false;
        }
        return true;
    }



    /**
     * 客户名>=3部分组成,客户人名与黑名单表3个名字对比,>=0触发
     * @param name
     * @param nativePlace
     * @param birthday
     * @return
     */
    //@Cacheable(value = "checkBlackFlag4", key = "'name'+#name+'nativePlace'+#nativePlace+'birthday'+#birthday")
    public boolean checkBlackFlag4(String name, String nativePlace, String birthday){
        StringBuffer tSql = new StringBuffer();
        tSql.append("SELECT (CASE WHEN (select firsttab.num from (SELECT count(distinct(e.blacklistid)) as num FROM LXBlackListIndv a, lxblackdate e WHERE  " +   //a.listproperty = 'Sanctions Lists' and
                "  e.blacklistid = a.blacklistid AND (length(?) - length(replace(?, ' '))) >= 2 AND a.firstname = substr(?, 1, instr(?, ' ') - 1) AND (? IS NULL OR ? <> 'CHN') " +
                " GROUP BY e.blacklistid, e.blackdate HAVING (SELECT CASE WHEN ((SELECT count(1) FROM dual WHERE (SELECT count(1) FROM lxblackdate p WHERE p.blacklistid = e.blacklistid) >= 1 " +
                " AND length(e.blackdate) = 4 AND e.blackdate = substr(?,1,4))) > 0 THEN 1 ELSE CASE WHEN ? >= (select min(blackdate) from lxblackdate where blacklistid = e.blacklistid) and " +
                " ? <= (select max(blackdate) from lxblackdate where blacklistid = e.blacklistid) THEN 1 ELSE 0 END END FROM dual) > 0) firsttab where rownum = 1) > 0 THEN 1 ELSE 0 END  " +
                " + CASE WHEN (select middletab.num from (SELECT count(distinct(e.blacklistid)) as num FROM LXBlackListIndv a, lxblackdate e WHERE  " +          //a.listproperty = 'Sanctions Lists'  and
                "  e.blacklistid = a.blacklistid AND (length(?) - length(replace(?, ' '))) >= 2 AND a.middlename = substr(?, instr(?, ' ') + 1, (instr(?, ' ', 1, 2)) - " +
                " (instr(?, ' ') + 1)) AND (? IS NULL OR ? <> 'CHN') GROUP BY e.blacklistid, e.blackdate HAVING (SELECT CASE WHEN ((SELECT count(1) FROM dual " +
                " WHERE (SELECT count(1) FROM lxblackdate p WHERE p.blacklistid = e.blacklistid) >= 1 AND length(e.blackdate) = 4 AND e.blackdate = substr(?, 1, 4))) > 0 THEN 1 " +
                " ELSE CASE WHEN ? >= (select min(blackdate) from lxblackdate where blacklistid = e.blacklistid) and ? <= (select max(blackdate) from lxblackdate " +
                " where blacklistid = e.blacklistid) THEN 1 ELSE 0 END END FROM dual) > 0) middletab where rownum = 1) > 0 THEN 1 ELSE 0 END  " +
                " + CASE WHEN (select surntab.num from (SELECT count(distinct(e.blacklistid)) as num FROM LXBlackListIndv a, lxblackdate e WHERE  " +             //a.listproperty = 'Sanctions Lists'  AND
                "  e.blacklistid = a.blacklistid AND (length(?) - length(replace(?, ' '))) >= 2 AND a.surname = substr(?, instr(?, ' ', 1, 2) + 1) AND ? = e.blackdate " +
                " AND (? IS NULL OR ? <> 'CHN') GROUP BY e.blacklistid, e.blackdate HAVING (SELECT CASE WHEN ((SELECT count(1) FROM dual WHERE (SELECT count(1) FROM lxblackdate p " +
                " WHERE p.blacklistid = e.blacklistid) >= 1 AND length(e.blackdate) = 4 AND e.blackdate = substr(?, 1, 4))) > 0 THEN 1 ELSE CASE WHEN ? >= (select min(blackdate) " +
                " from lxblackdate where blacklistid = e.blacklistid) and ? <= (select max(blackdate) from lxblackdate where blacklistid = e.blacklistid) THEN 1 ELSE 0 END END " +
                " FROM dual) > 0) surntab where rownum = 1) > 0 THEN 1 ELSE 0 END) FROM dual ");
        TransferData tparam = new TransferData();
        tparam.setNameAndValue("0", "string:" + name);
        tparam.setNameAndValue("1", "string:" + name);
        tparam.setNameAndValue("2", "string:" + name);
        tparam.setNameAndValue("3", "string:" + name);
        tparam.setNameAndValue("4", "string:" + nativePlace);
        tparam.setNameAndValue("5", "string:" + nativePlace);
        tparam.setNameAndValue("6", "string:" + birthday);
        tparam.setNameAndValue("7", "string:" + birthday);
        tparam.setNameAndValue("8", "string:" + birthday);
        tparam.setNameAndValue("9", "string:" + name);
        tparam.setNameAndValue("10", "string:" + name);
        tparam.setNameAndValue("11", "string:" + name);
        tparam.setNameAndValue("12", "string:" + name);
        tparam.setNameAndValue("13", "string:" + name);
        tparam.setNameAndValue("14", "string:" + name);
        tparam.setNameAndValue("16", "string:" + nativePlace);
        tparam.setNameAndValue("17", "string:" + nativePlace);
        tparam.setNameAndValue("18", "string:" + birthday);
        tparam.setNameAndValue("19", "string:" + birthday);
        tparam.setNameAndValue("20", "string:" + birthday);
        tparam.setNameAndValue("21", "string:" + name);
        tparam.setNameAndValue("22", "string:" + name);
        tparam.setNameAndValue("23", "string:" + name);
        tparam.setNameAndValue("24", "string:" + name);
        tparam.setNameAndValue("25", "string:" + birthday);
        tparam.setNameAndValue("26", "string:" + nativePlace);
        tparam.setNameAndValue("27", "string:" + nativePlace);
        tparam.setNameAndValue("28", "string:" + birthday);
        tparam.setNameAndValue("29", "string:" + birthday);
        tparam.setNameAndValue("30", "string:" + birthday);

        VData tBVData = new VData();
        tBVData.add(tSql.toString());
        tBVData.add(tparam);
        ExeSQL pExeSQL = new ExeSQL();
        SSRS tSSRS = pExeSQL.execSQL(tBVData);
        if("0".equals(tSSRS.GetText(1,1))){
            return false;
        }
        return true;
    }

    /**
     * 投保人名>=3部分组成,投保人名与黑名单表3个名字对比,>=2触发
     * @param firstName
     * @return
     */
    //@Cacheable(value = "checkBlackFlag3_1", key = "'firstName'+#firstName")
    public int checkBlackFlag3_1(String firstName){
        LXBlackListIndvDB lxBlackListIndvDB = new LXBlackListIndvDB();
        //lxBlackListIndvDB.setListProperty("Sanctions Lists");
        lxBlackListIndvDB.setFirstName(firstName);
        LXBlackListIndvSet lxBlackListIndvSet = lxBlackListIndvDB.query();
        if(lxBlackListIndvSet != null || lxBlackListIndvSet.size()>0){
            return lxBlackListIndvSet.size();
        }
        return 0;
    }


    //@Cacheable(value = "checkBlackFlag3_3", key = "'surName'+#surName")
    public int checkBlackFlag3_3(String surName){
        LXBlackListIndvDB lxBlackListIndvDB = new LXBlackListIndvDB();
        //lxBlackListIndvDB.setListProperty("Sanctions Lists");
        lxBlackListIndvDB.setSurName(surName);
        LXBlackListIndvSet lxBlackListIndvSet = lxBlackListIndvDB.query();
        if(lxBlackListIndvSet != null || lxBlackListIndvSet.size()>0){
            return lxBlackListIndvSet.size();
        }
        return 0;
    }


    public int checkBlackFlag3_2(String middleName){
        LXBlackListIndvDB lxBlackListIndvDB = new LXBlackListIndvDB();
        //lxBlackListIndvDB.setListProperty("Sanctions Lists");
        lxBlackListIndvDB.setMiddleName(middleName);
        LXBlackListIndvSet lxBlackListIndvSet = lxBlackListIndvDB.query();
        if(lxBlackListIndvSet != null || lxBlackListIndvSet.size()>0){
            return lxBlackListIndvSet.size();
        }
        return 0;
    }

    public int checkSameNameTerr(String name){
        StringBuffer tSql = new StringBuffer();
        tSql.append("SELECT count(1) FROM LXBLACKLISTINDV A WHERE A.NAME = ? OR A.USEDNAME =?");
        TransferData tparam = new TransferData();
        tparam.setNameAndValue("0", "string:" + name);
        tparam.setNameAndValue("1", "string:" + name);
        VData tBVData = new VData();
        tBVData.add(tSql.toString());
        tBVData.add(tparam);
        ExeSQL pExeSQL = new ExeSQL();
        SSRS tSSRS = pExeSQL.execSQL(tBVData);
        return Integer.parseInt(tSSRS.GetText(1,1));
    }


    /**
     * 客户证件类型和号码与恐怖分子、恐怖组织名单或武器禁运名单相同
     * @param idNo
     * @param idType
     * @return
     */
    //@Cacheable(value = "checkSameTypeTerr", key = "'idNo'+#idNo+'idType'+#idType")
    public int checkSameTypeTerr(String idNo, String idType){
        StringBuffer tSql = new StringBuffer();
        tSql.append("SELECT COUNT(1) FROM LXBLACKLISTINDV A WHERE A.IDTYPE = ? AND A.IDNO = ? ");
        TransferData tparam = new TransferData();
        tparam.setNameAndValue("0", "string:" + idType);
        tparam.setNameAndValue("1", "string:" + idNo);
        VData tBVData = new VData();
        tBVData.add(tSql.toString());
        tBVData.add(tparam);
        ExeSQL pExeSQL = new ExeSQL();
        SSRS tSSRS = pExeSQL.execSQL(tBVData);
        return Integer.parseInt(tSSRS.GetText(1,1));
    }


    /**
     * 客户白名单，白名单限额校验（投保人）
     * @param name
     * @param idType
     * @param idNo
     * @param birthday
     * @param sex
     * @return
     */
    //@Cacheable(value = "checkWhiteSign", key = "'name'+#name+'idType'+#idType+'idNo'+#idNo+'birthday'+#birthday+'sex'+#sex")
    public SSRS checkAppWhiteSign(String name, String sex ,String idType, String idNo, String birthday){
       // String sql="select  amnt from lmwhitelist l where l.name= ? and  l.sex= ? and l.idtype= ? and l.idno= ?  and l.birthday = ? ";
        String sql="SELECT amnt FROM lmwhitelist l  WHERE l.applystate = '2' AND l.userstate = '1' AND l.wfwflag = '01' AND l.name= ? and  l.sex= ? and l.idtype= ? and l.idno= ?  and l.birthday = ? ";
        TransferData tparam = new TransferData();
        tparam.setNameAndValue("0", "string:" + name);
        tparam.setNameAndValue("1", "string:" + sex);
        tparam.setNameAndValue("2", "string:" + idType);
        tparam.setNameAndValue("3", "string:" + idNo);
        tparam.setNameAndValue("4", "string:" + birthday);
        VData tBVData = new VData();
        tBVData.add(sql);
        tBVData.add(tparam);
        ExeSQL pExeSQL = new ExeSQL();
        SSRS tSSRS = pExeSQL.execSQL(tBVData);
        return tSSRS;
    }

    /**
     * 客户白名单，白名单限额校验（被保人）
     * @param name
     * @param idType
     * @param idNo
     * @param birthday
     * @param sex
     * @return
     */
    //@Cacheable(value = "checkWhiteSign", key = "'name'+#name+'idType'+#idType+'idNo'+#idNo+'birthday'+#birthday+'sex'+#sex")
    public SSRS checkInsuWhiteSign(String name, String sex ,String idType, String idNo, String birthday){
        String sql="SELECT amnt FROM lmwhitelist l  WHERE l.applystate = '2' AND l.userstate = '1' AND l.wfwflag = '01' AND l.name= ? and  l.sex= ? and l.idtype= ? and l.idno= ?  and l.birthday = ? ";
        TransferData tparam = new TransferData();
        tparam.setNameAndValue("0", "string:" + name);
        tparam.setNameAndValue("1", "string:" + sex);
        tparam.setNameAndValue("2", "string:" + idType);
        tparam.setNameAndValue("3", "string:" + idNo);
        tparam.setNameAndValue("4", "string:" + birthday);
        VData tBVData = new VData();
        tBVData.add(sql);
        tBVData.add(tparam);
        ExeSQL pExeSQL = new ExeSQL();
        SSRS tSSRS = pExeSQL.execSQL(tBVData);
        return tSSRS;
    }

//    /**
//     * 修改拼接c错误
//     * @param agentCode
//     * @param idNoType
//     * @param idNo
//     * @return
//     */
    //@Cacheable(value = "checkAgentBlackSign", key = "'agentCode'+#agentCode+'idNoType'+#idNoType+'idNo'+#idNo")
/*    public String checkAgentBlackSign(String agentCode,String idNoType, String idNo){
        if("0".equals(idNoType)){
            return "0";
        }
        StringBuffer tSql = new StringBuffer();
        tSql.append("SELECT '1' FROM DUAL WHERE ( ");
        tSql.append(" SELECT COUNT(1) FROM LAAGENTMONITOR WHERE AGENTCODE = ? AND MONITORTYPE = '01' AND MONITORSTATE = '1' AND CANCELDATE IS NULL) ");
        tSql.append(" + ( SELECT COUNT(1) FROM LAAGENTBLACKLIST A WHERE A.IDNOTYPE = '0' AND A.IDNO = ? AND B.AGENTCODE = ?) > 0");
        TransferData tparam = new TransferData();
        tparam.setNameAndValue("0", "string:" + agentCode);
        tparam.setNameAndValue("1", "string:" + idNo);
        tparam.setNameAndValue("1", "string:" + agentCode);
        VData tBVData = new VData();
        tBVData.add(tSql.toString());
        tBVData.add(tparam);
        ExeSQL pExeSQL = new ExeSQL();
        SSRS tSSRS = pExeSQL.execSQL(tBVData);
        if(tSSRS == null || tSSRS.MaxRow == 0){
            return "0";
        }
        return "1";
    }*/

    public String checkAgentBlackSign(String agentCode){
        StringBuffer tSql = new StringBuffer();
        tSql.append(" SELECT '1' as field1 FROM DUAL WHERE (SELECT COUNT(1) FROM LAAGENTMONITOR a WHERE  ");
        tSql.append(" a.AGENTCODE = ? AND a.MONITORTYPE = '01' AND a.MONITORSTATE = '1' AND a.CANCELDATE IS NULL ");
        tSql.append(" and a.monitorno = (select max(b.monitorno) from LAAGENTMONITOR b where b.agentcode =  ");
        tSql.append( " a.agentcode)) + (SELECT COUNT(1) FROM LAAGENTBLACKLIST A, LAAGENT B WHERE A.IDNOTYPE =  ");
        tSql.append(" B.IDNOTYPE AND B.IDNOTYPE = '0' AND A.IDNO = B.IDNO AND B.AGENTCODE = ? ) > 0  ");
        TransferData tparam = new TransferData();
        tparam.setNameAndValue("0", "string:" + agentCode);
        tparam.setNameAndValue("1", "string:" + agentCode);
        VData tBVData = new VData();
        tBVData.add(tSql.toString());
        tBVData.add(tparam);
        ExeSQL pExeSQL = new ExeSQL();
        SSRS tSSRS = pExeSQL.execSQL(tBVData);
        if(tSSRS == null || tSSRS.MaxRow == 0){
            return "0";
        }
        return "1";
    }
    public boolean checkBlackSign(String blackListNo, String IDNo, String IDType){
        StringBuffer tSql = new StringBuffer();
        tSql.append("select 1 from ldblacklist where BLACKLISTNO = ? or (IDNO = ? and IDTYPE = ?)");
        TransferData tparam = new TransferData();
        tparam.setNameAndValue("0", "string:" + blackListNo);
        tparam.setNameAndValue("1", "string:" + IDNo);
        tparam.setNameAndValue("2", "string:" + IDType);
        VData tBVData = new VData();
        tBVData.add(tSql.toString());
        tBVData.add(tparam);
        ExeSQL pExeSQL = new ExeSQL();
        SSRS tSSRS = pExeSQL.execSQL(tBVData);
        if(tSSRS == null || tSSRS.MaxRow == 0){
            return false;
        }
        return true;
    }

    /**
     * 5018税优健康险的BMI --wpq--
     * @param contno
     * @param insuredno
     * @return
     */
    public String getBMI (String contno,String insuredno){
        StringBuffer tSql= new StringBuffer();
        tSql.append(" select ROUND((AVOIRDUPOIS / POWER(DECODE(A.STATURE, 0, 100000, A.STATURE) / 100,2)),0) from lcinsured A ");
        tSql.append(" where A.contno=? and A.insuredno=? ");
        TransferData tparam = new TransferData();
        tparam.setNameAndValue("0","string:"+contno);
        tparam.setNameAndValue("1","string:"+insuredno);
        VData tBVData=new VData();
        tBVData.add(tSql.toString());
        tBVData.add(tparam);
        ExeSQL pExeSQL = new ExeSQL();
        String result = pExeSQL.getOneValue(tBVData);
       return result;
    }

    /**
     * 5018税优健康险投保份数 --wpq--
     * @param insuredno
     * @return
     */
    public String getCopies5018 (String insuredno){
        StringBuffer tSql= new StringBuffer();
        tSql.append(" SELECT COUNT(contno)  FROM LCCONT N WHERE N.INSUREDNO = ?  AND N.salechnl = '8' AND N.APPFLAG  ");
        tSql.append("  IN ('0', '1', '2') AND N.UWFLAG NOT IN ('1', 'a') ");
        TransferData tparam = new TransferData();
        tparam.setNameAndValue("0","string:"+insuredno);
        VData tBVData=new VData();
        tBVData.add(tSql.toString());
        tBVData.add(tparam);
        ExeSQL pExeSQL = new ExeSQL();
        String result = pExeSQL.getOneValue(tBVData);
        return result;
    }
    /**
     * 备注栏有内容 --wpq--
     */
   public String getNoteColumn(String contno){
       StringBuffer tsql= new StringBuffer();
       tsql.append(" select case when EXISTS (SELECT 1 ");
       tsql.append(" FROM LCCUSTOMERIMPARTPARAMS A, LCCONT B ");
       tsql.append("  WHERE A.CONTNO = B.CONTNO ");
       tsql.append("  AND A.CUSTOMERNO = B.INSUREDNO ");
       tsql.append("  AND A.CUSTOMERNOTYPE = '1' ");
       tsql.append("  AND A.CONTNO = ? ");
       tsql.append(" AND A.PATCHNO = '19061' ");
       tsql.append("  AND A.IMPARTVER = 'GX13' ");
       tsql.append(" AND A.IMPARTPARAMNAME = 'Remark' ");
       tsql.append(" AND A.IMPARTPARAM IS NOT NULL ");
       tsql.append("  AND A.IMPARTPARAM NOT IN ('无', '*')) then ");
       tsql.append("   1 else 0 end from dual ");
       TransferData tparm= new TransferData();
       tparm.setNameAndValue("0","string:"+contno);
       VData tBVData= new VData();
       tBVData.add(tsql.toString());
       tBVData.add(tparm);
       ExeSQL pExeSQL = new ExeSQL();
       String result =pExeSQL.getOneValue(tBVData);
       return  result;
   }

    /**
     * 投保人钻石卡效验 --sll--
     */
    public String getClientLevel(LCAppntPojo lcAppntPojo){
        StringBuffer tsql = new StringBuffer();
        tsql.append("select ditype from lmwhitelist a where  a.name = ? and a.idtype = ? and a.idno = ? and a.birthday = ? and a.sex = ? ");
        TransferData tparam =new TransferData();
        tparam.setNameAndValue("APPNTNAME","string:"+lcAppntPojo.getAppntName());
        tparam.setNameAndValue("APPNTIDTYPE","string:"+lcAppntPojo.getIDType());
        tparam.setNameAndValue("APPNTIDNO","string:"+lcAppntPojo.getIDNo());
        tparam.setNameAndValue("APPNTBIRTHDAY","string:"+lcAppntPojo.getAppntBirthday());
        tparam.setNameAndValue("APPNTSEX","string:"+lcAppntPojo.getAppntSex());
        VData tVata = new VData();
        tVata.add(tsql.toString());
        tVata.add(tparam);
        ExeSQL pExeSQL =new ExeSQL();
        String result= pExeSQL.getOneValue(tVata);
        if("0".equals(result)){
            result="";
        }
        return  result;
    }
}