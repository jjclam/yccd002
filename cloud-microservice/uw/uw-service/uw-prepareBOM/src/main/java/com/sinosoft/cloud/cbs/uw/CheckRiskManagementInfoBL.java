package com.sinosoft.cloud.cbs.uw;

import com.alibaba.fastjson.JSONObject;
import com.sinosoft.cache.dao.impl.RedisCommonDao;
import com.sinosoft.cloud.bl.AbstractBL;
import com.sinosoft.cloud.rest.TradeInfo;
import com.sinosoft.lis.entity.*;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LCRiskManagementInfoSchema;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class CheckRiskManagementInfoBL extends AbstractBL {
    private final Log logger = LogFactory.getLog(this.getClass());
    private ExeSQL exeSQL =new ExeSQL();
    @Autowired
    RedisCommonDao redisCommonDao;

    @Override
    public boolean checkData(TradeInfo requestInfo) {

        return true;
    }

    @Override
    public TradeInfo dealData(TradeInfo requestInfo) {

       String checkflag= redisCommonDao.getOnlyString("ldcodeTables_LINK_STAUTS_01");
        if (checkflag==null){
            checkflag= exeSQL.getOneValue("select codename from ldcode where codetype = 'LINK_STAUTS' and code='01'");
            redisCommonDao.saveOnlyString("ldcodeTables_LINK_STAUTS_01",checkflag);
        }
        if("1".equals(checkflag)){
        logger.info("=====进入中保信风险核保校验");
        LCContPojo lcContPojo = (LCContPojo) requestInfo.getData(LCContPojo.class.getName());
        List<LCPolPojo> lcPolPojoList= (List<LCPolPojo>) requestInfo.getData(LCPolPojo.class.getName());
        LCAppntPojo lcAppntPojo = (LCAppntPojo) requestInfo.getData(LCAppntPojo.class.getName());
        List<LCInsuredPojo> lcInsuredPojoList = (List<LCInsuredPojo>) requestInfo.getData(LCInsuredPojo.class.getName());
        String mark="";
        String str="";
        String contNo = lcContPojo.getContNo();
        String prtNo =lcContPojo.getPrtNo();
        //用户名
         String userName = redisCommonDao.getOnlyString("ldcodeTables_ZBXInfo_username");
         if(userName==null){
             userName=exeSQL.getOneValue("select codename from ldcode where codetype = 'ZBXInfo' and code='username'");
             redisCommonDao.saveOnlyString("ldcodeTables_ZBXInfo_username",userName);
         }
        String name = lcContPojo.getInsuredName();
        String idtype = lcContPojo.getInsuredIDType();
        String idno = lcContPojo.getInsuredIDNo();
        String newIDType= redisCommonDao.getOnlyString("ldcodeTables_IDTYPE_"+idtype);
        if(newIDType==null){
            newIDType=exeSQL.getOneValue("select OTHERSIGN from ldcode where CODETYPE='IDTYPE' and CODE='"+idtype+"'");
            redisCommonDao.saveOnlyString("ldcodeTables_IDTYPE_"+idtype,newIDType);
        }
        //机构代码
            String insuranceCompanyCode = redisCommonDao.getOnlyString("ldcodeTables_ZBXInfo_companyCode");
        if(insuranceCompanyCode==null){
            insuranceCompanyCode=exeSQL.getOneValue("select codename from ldcode where codetype = 'ZBXInfo' and code='companyCode'");
            redisCommonDao.saveOnlyString("ldcodeTables_ZBXInfo_companyCode",insuranceCompanyCode);
        }

        String customerAllowed="Y";
        HashSet<LCRiskManagementInfoSchema> successResultDataSet = new HashSet<LCRiskManagementInfoSchema>();
        HashSet<String> dataSignSet = new HashSet<String>();
        List<String> signList = new ArrayList<String>();

            String url = redisCommonDao.getOnlyString("ldcodeTables_RISK_LINK_LINK_URL");
            if (url==null){
                 url=exeSQL.getOneValue("select codename from ldcode where codetype = 'RISK_LINK' and code='LINK_URL'");
                redisCommonDao.saveOnlyString("ldcodeTables_RISK_LINK_LINK_URL",url);
            }

        //统计产品存在个人意外险个数
        int count=0;
       LMRiskAppPojo lmRiskAppPojo  = redisCommonDao.getEntityRelaDB(LMRiskAppPojo.class,lcPolPojoList.get(0).getRiskCode());
       // String sql1 ="select risktype,riskprop,riskcode from lmriskapp where riskcode = '"+lcPolPojoList.get(0).getRiskCode()+"' ";
      //  SSRS execSQL = exeSQL.execSQL(sql1);
            try {

                    String risktype = lmRiskAppPojo.getRiskType();
                    String riskprop = lmRiskAppPojo.getRiskProp();
                    String riskcode = lmRiskAppPojo.getRiskCode();
                    //存在个人意外险
                    if("A".equals(risktype)&&"I".equals(riskprop)){

                        String sql11 =" select multiCompany , majorDiseasePayment , disability , dense , accumulativeMoney  from lcRiskManagementInfo where business = 'HB' and makedate = to_date('"+ PubFun.getCurrentDate()+"', 'YYYY-MM-DD') and riskcode = '"+riskcode+"' and insuredname = '"+name+"' and insuredidtype = '"+idtype+"' and insuredidno = '"+idno+"' and (multiCompany='Y' or majorDiseasePayment ='Y' or disability ='Y' or dense ='Y' or accumulativeMoney ='Y')";

                        SSRS resultSQL = exeSQL.execSQL(sql11);
                        if(resultSQL!=null&&resultSQL.getMaxRow()>0){
                            logger.info("=====核心已存在的风险信息,无需调用服务,直接返回");
                            for(int x=1;x<=resultSQL.getMaxRow();x++){
                                Set<String> setData =	new HashSet<>();
                                if("Y".equals(resultSQL.GetText(x, 1))){
                                    setData.add("01");
                                }
                                if("Y".equals(resultSQL.GetText(x, 2))){
                                    setData.add("02");
                                }
                                if("Y".equals(resultSQL.GetText(x, 3))){
                                    setData.add("03");
                                }
                                if("Y".equals(resultSQL.GetText(x, 4))){
                                    setData.add("04");
                                }
                                if("Y".equals(resultSQL.GetText(x, 5))){
                                    setData.add("05");
                                }
                                List<String> setDataguanbi =new ArrayList<>();
                                String cotr = redisCommonDao.getOnlyString("ldcode_control_01");
                                String multiCompanyFlag ="";
                                String majorDiseasePaymentFlag="";
                                String disabilityFlag ="";
                                String denseFlag ="";
                                String accumulativeMoneyFlag ="";
                                if(cotr==null){
                                    SSRS control = exeSQL.execSQL("select code,codename,codealias,comcode,othersign from ldcode where codetype='control'");
                                     multiCompanyFlag =control.GetText(1, 1);
                                     majorDiseasePaymentFlag =control.GetText(1, 2);
                                     disabilityFlag =control.GetText(1, 3);
                                     denseFlag =control.GetText(1, 4);
                                     accumulativeMoneyFlag =control.GetText(1, 5);
                                    String newcor=multiCompanyFlag+","+majorDiseasePaymentFlag+","+disabilityFlag+","+denseFlag+","+accumulativeMoneyFlag;
                                    redisCommonDao.saveOnlyString("ldcode_control_01",newcor);
                                }else if(!StringUtils.isEmpty(cotr)){
                                    String[] split = cotr.split(",");
                                    multiCompanyFlag =split[0];
                                    majorDiseasePaymentFlag =split[1];
                                    disabilityFlag =split[2];
                                    denseFlag =split[3];
                                    accumulativeMoneyFlag =split[4];
                                }

                                if("0".equals(multiCompanyFlag)){
                                    setDataguanbi.add("01");
                                }
                                if("0".equals(majorDiseasePaymentFlag)){
                                    setDataguanbi.add("02");
                                }
                                if("0".equals(disabilityFlag)){
                                    setDataguanbi.add("03");
                                }
                                if("0".equals(denseFlag)){
                                    setDataguanbi.add("04");
                                }

                                if("0".equals(accumulativeMoneyFlag)){
                                    setDataguanbi.add("05");
                                }

                                for(int m=0;m<setDataguanbi.size();m++){
                                    if(setData.contains(setDataguanbi.get(m))){
                                        setData.remove(setDataguanbi.get(m));
                                    }
                                }

                                String rk="";
                                int gnCount = setData.size();
                                if(gnCount!=0){

                                    if(setData.contains("01")){
                                        rk="01";
                                    }else if(setData.contains("02")){
                                        rk="02";
                                    }else if(setData.contains("03")){
                                        rk="03";
                                    }else if(setData.contains("04")){
                                        rk="04";
                                    }else if(setData.contains("05")){
                                        rk="05";
                                    }
                                    String riskDesc = redisCommonDao.getOnlyString("ldcodeTables_RiskManagement_" + rk);
                                    if(riskDesc==null){
                                        riskDesc = exeSQL.getOneValue("select CODENAME from ldcode where CODETYPE = 'RiskManagement' and CODE ='"+rk+"'");
                                        redisCommonDao.saveOnlyString("ldcodeTables_RiskManagement_" + rk,riskDesc);
                                    }
                                    logger.info("=====风险校验成功:返回正常报文,存在'Y'数据,并返回话术:"+riskDesc);
                                    requestInfo.addError(riskDesc);
                                    return requestInfo;
                                }else{
                                    logger.info("=====风险校验成功:核心关闭某项风险指标,无'Y'数据");
                                    return requestInfo;
                                }

                            }

                        }
                        String uuid=PubFun1.CreateMaxNo("UuidSerNo",10);
                        long num =9999999999L;
                        String newuuid=num-Integer.valueOf(uuid)+"";
                        String uuidSerNo =insuranceCompanyCode + newuuid;

                        count++;
                        JSONObject tJson = new JSONObject();
                        tJson.put("userName", userName);
                        tJson.put("policyCode", contNo);
                        tJson.put("policyHolderName", name);
                        tJson.put("credentialsType", newIDType);
                        tJson.put("credentialsCode", idno);
                        tJson.put("insuranceCompanyCode", insuranceCompanyCode);
                        tJson.put("customerAllowed", customerAllowed);
                        tJson.put("productCode", riskcode);
                        tJson.put("insurerUuid", uuidSerNo);
                        logger.info("=====发送json报文:"+tJson.toString());
                        if(UrlServiceClient.ConnterServletUrl(url,tJson.toString())){
                            //前置机返回数据
                            str = UrlServiceClient.getRestl();
                        }
                        //logger.info("=====返回未解析,json报文:"+str);
                        str = URLDecoder.decode(str, "UTF-8");
                        logger.info("=====返回解析后,json报文:"+str);
                        JSONObject resultJson = JSONObject.parseObject(str);

                        String mCurrentTime = PubFun.getCurrentTime();
                        String mCurrentDate = PubFun.getCurrentDate();
                        String serNo = PubFun1.CreateMaxNo("zbxSerNo",20);
                        if(str.indexOf("retCode")!=-1){

                            String retCode = resultJson.getString("retCode");
                            if("001".equals(retCode)||"007".equals(retCode)){

                                //开始存储数据
                                LCRiskManagementInfoSchema lcRiskManagementInfoSchema = new LCRiskManagementInfoSchema();
                                lcRiskManagementInfoSchema.setSerialNo(serNo);
                                lcRiskManagementInfoSchema.setAccNo(userName);
                                lcRiskManagementInfoSchema.setContNo(contNo);
                                lcRiskManagementInfoSchema.setPrtNo(prtNo);
                                lcRiskManagementInfoSchema.setRiskCode(riskcode);
                                lcRiskManagementInfoSchema.setInsuredName(name);
                                lcRiskManagementInfoSchema.setInsuredIDType(idtype);
                                lcRiskManagementInfoSchema.setInsuredIDNo(idno);
                                lcRiskManagementInfoSchema.setCompanyCode(insuranceCompanyCode);
                                lcRiskManagementInfoSchema.setBusiness("HB");
                                lcRiskManagementInfoSchema.setRiskType("A");
                                lcRiskManagementInfoSchema.setCustomerAllowed("Y");
                                lcRiskManagementInfoSchema.setOperator("E001");
                                lcRiskManagementInfoSchema.setMakeDate(mCurrentDate);
                                lcRiskManagementInfoSchema.setMakeTime(mCurrentTime);
                                lcRiskManagementInfoSchema.setModifyDate(mCurrentDate);
                                lcRiskManagementInfoSchema.setModifyTime(mCurrentTime);
                                lcRiskManagementInfoSchema.setInsurerUuid(uuidSerNo);
                                String multiCompany="";
                                String majorDiseasePayment ="";
                                String disability ="";
                                String dense ="";
                                String accumulativeMoney ="";
                                String pageQueryCode ="";
                                String tagDate = "";
                                String displayPage ="";
                                if("001".equals(retCode)){
                                    JSONObject dataJsonObject = resultJson.getJSONObject("data");
                                    multiCompany = dataJsonObject.getString("multiCompany");
                                    majorDiseasePayment = dataJsonObject.getString("majorDiseasePayment");
                                    disability = dataJsonObject.getString("disability");
                                    dense = dataJsonObject.getString("dense");
                                    accumulativeMoney = dataJsonObject.getString("accumulativeMoney");
                                    pageQueryCode = dataJsonObject.getString("pageQueryCode");
                                    tagDate = dataJsonObject.getString("tagDate");
                                    displayPage = dataJsonObject.getString("displayPage");
                                }

                                lcRiskManagementInfoSchema.setMultiCompany(multiCompany);
                                lcRiskManagementInfoSchema.setMajorDiseasePayment(majorDiseasePayment);
                                lcRiskManagementInfoSchema.setDisability(disability);
                                lcRiskManagementInfoSchema.setDense(dense);
                                lcRiskManagementInfoSchema.setAccumulativeMoney(accumulativeMoney);
                                lcRiskManagementInfoSchema.setPageQueryCode(pageQueryCode);
                                lcRiskManagementInfoSchema.setTagDate(tagDate);
                                lcRiskManagementInfoSchema.setDisplayPage(displayPage);
                                MMap map = new MMap();
                                VData vData = new VData();
                                PubSubmit tPubSubmit = new PubSubmit();
                                map.put(lcRiskManagementInfoSchema, "INSERT");
                                vData.add(map);
                                if(!tPubSubmit.submitData(vData)){
                                    logger.info("=====数据存储失败,跳出本次循环");
                                    requestInfo.addError("处理失败");
                                    return requestInfo;
                                };

                                successResultDataSet.add(lcRiskManagementInfoSchema);
                                    if("Y".equals(multiCompany)){
                                        dataSignSet.add("01");
                                    }
                                    if("Y".equals(majorDiseasePayment)){
                                        dataSignSet.add("02");
                                    }
                                    if("Y".equals(disability)){
                                        dataSignSet.add("03");
                                    }
                                    if("Y".equals(dense)){
                                        dataSignSet.add("04");
                                    }
                                    if("Y".equals(accumulativeMoney)){
                                        dataSignSet.add("05");
                                    }


                            }else{
                                logger.info("=====返回正常报文,不符合核心正常报文要求,返回码:"+retCode);
                                requestInfo.addError("处理失败");
                                return requestInfo;
                            }
                        }else{
                            logger.info("=====返回异常报文,跳出本次循环");
                            requestInfo.addError("处理失败");
                            return requestInfo;
                        }

                    }



                if(count == 0){
                    logger.info("=====此产品非个人意外险,不做风险校验.");
                    return requestInfo;
                }

                //成功返回数据并正常存储个数
                int successResultDataCount = successResultDataSet.size();

                if(count==successResultDataCount){

                    String cotr = redisCommonDao.getOnlyString("ldcode_control_01");
                    String multiCompanyFlag ="";
                    String majorDiseasePaymentFlag="";
                    String disabilityFlag ="";
                    String denseFlag ="";
                    String accumulativeMoneyFlag ="";
                    if(cotr==null){
                        SSRS control = exeSQL.execSQL("select code,codename,codealias,comcode,othersign from ldcode where codetype='control'");
                        multiCompanyFlag =control.GetText(1, 1);
                        majorDiseasePaymentFlag =control.GetText(1, 2);
                        disabilityFlag =control.GetText(1, 3);
                        denseFlag =control.GetText(1, 4);
                        accumulativeMoneyFlag =control.GetText(1, 5);
                        String newcor=multiCompanyFlag+","+majorDiseasePaymentFlag+","+disabilityFlag+","+denseFlag+","+accumulativeMoneyFlag;
                        redisCommonDao.saveOnlyString("ldcode_control_01",newcor);
                    }else if(!StringUtils.isEmpty(cotr)){
                        String[] split = cotr.split(",");
                        multiCompanyFlag =split[0];
                        majorDiseasePaymentFlag =split[1];
                        disabilityFlag =split[2];
                        denseFlag =split[3];
                        accumulativeMoneyFlag =split[4];
                    }
                    if("0".equals(multiCompanyFlag)){
                        signList.add("01");
                    }
                    if("0".equals(majorDiseasePaymentFlag)){
                        signList.add("02");
                    }
                    if("0".equals(disabilityFlag)){
                        signList.add("03");
                    }

                    if("0".equals(denseFlag)){
                        signList.add("04");
                    }

                    if("0".equals(accumulativeMoneyFlag)){
                        signList.add("05");
                    }

                    for(int i=0;i<signList.size();i++){
                        if(dataSignSet.contains(signList.get(i))){
                            dataSignSet.remove(signList.get(i));
                        }
                    }
                    //存在风险提示个数
                    int dataSignCount = dataSignSet.size();
                    if(dataSignCount!=0){

                        if(dataSignSet.contains("01")){
                            mark="01";
                        }else if(dataSignSet.contains("02")){
                            mark="02";
                        }else if(dataSignSet.contains("03")){
                            mark="03";
                        }else if(dataSignSet.contains("04")){
                            mark="04";
                        }else if(dataSignSet.contains("05")){
                            mark="05";
                        }
                        String riskDesc = redisCommonDao.getOnlyString("ldcodeTables_RiskManagement_" + mark);
                        if(riskDesc==null){
                            riskDesc = exeSQL.getOneValue("select CODENAME from ldcode where CODETYPE = 'RiskManagement' and CODE ='"+mark+"'");
                            redisCommonDao.saveOnlyString("ldcodeTables_RiskManagement_" + mark,riskDesc);
                        }
                        logger.info("=====风险校验成功:返回正常报文,存在'Y'数据,并返回话术:"+riskDesc);
                        requestInfo.addError(riskDesc);
                        return requestInfo;
                    }else{
                        logger.info("=====风险校验成功:返回正常报文,无'Y'数据");
                        return requestInfo;
                    }
                }else{
                    logger.info("=====风险校验失败:1.返回异常报文,2.输入参数错误,返回正常报文,3.数据存储失败");
                    requestInfo.addError("风险校验异常");
                    return requestInfo;
                }

            } catch (Exception e) {
                logger.info("=====网销风险校验异常,异常信息:"+e.getMessage());
                e.printStackTrace();
                CError tError = new CError();
                tError.moduleName = "EContJDUWBL";
                tError.functionName = "getRiskManagementInfo";
                tError.errorMessage = "处理失败，原因是：" + e.getMessage();
                requestInfo.addError("风险校验异常");
                return requestInfo;
            }
        }else{
            return requestInfo;
        }
    }

}
