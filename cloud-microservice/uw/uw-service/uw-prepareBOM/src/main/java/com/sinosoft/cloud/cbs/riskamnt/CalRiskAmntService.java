package com.sinosoft.cloud.cbs.riskamnt;

import com.sinosoft.cloud.microservice.AbstractMicroService;
import com.sinosoft.cloud.rest.TradeInfo;
import com.sinosoft.lis.entity.LCAppntPojo;
import com.sinosoft.lis.entity.LCContPojo;
import com.sinosoft.lis.entity.LCInsuredPojo;
import com.sinosoft.lis.entity.LCPolPojo;
import com.sinosoft.utility.ExceptionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @Author: zhuyiming
 * @Description: 计算风险保额
 * @Date: Created in 1:49 2017/9/25
 * @Modified By
 */
@Component("CalRiskAmntService")
public class CalRiskAmntService extends AbstractMicroService{
    private final Log logger = LogFactory.getLog(this.getClass());
    @Autowired
    GetAccumBL mGetAccumBL;
    @Override
    public boolean checkData(TradeInfo requestInfo) {
        LCAppntPojo tLCAppntPojo = (LCAppntPojo) requestInfo.getData(LCAppntPojo.class.getName());
        LCContPojo tLCContPojo = (LCContPojo) requestInfo.getData(LCContPojo.class.getName());
        List<LCInsuredPojo> tLCInsuredList = (List) requestInfo.getData(LCInsuredPojo.class.getName());
        List<LCPolPojo> tLCPolPojoList = (List) requestInfo.getData(LCPolPojo.class.getName());
        if (tLCAppntPojo == null) {
            logger.debug(this.getClass().getName() + ",获取tLCAppntPojo出错！");
            return false;
        }
        if (tLCInsuredList == null) {
            logger.debug(this.getClass().getName() + ",获取tLCInsuredList出错！");
            return false;
        }
        if (tLCPolPojoList == null) {
            logger.debug(this.getClass().getName() + ",获取tLCPolPojoList出错！");
            return false;
        }
        if (tLCContPojo == null) {
            logger.debug(this.getClass().getName() + ",获取tLCContPojo出错！");
            return false;
        }
        return true;
    }

    @Override
    public TradeInfo dealData(TradeInfo tradeInfo) {
        logger.debug("进入风险保额处理类" + this.getClass() + "。");
        try {
            mGetAccumBL.submitData(tradeInfo);
        }catch (Exception e){
            LCContPojo lcContPojo = (LCContPojo) tradeInfo.getData(LCContPojo.class.getName());
            logger.error("保单号：" + lcContPojo.getContNo() + "," + ExceptionUtils.exceptionToString(e));
            logger.error("风险保额数据准备异常！" + tradeInfo);
            tradeInfo.addError("风险保额数据准备异常！");
            return  tradeInfo;
        }
        if(tradeInfo.hasError()){
            logger.debug("风险保额准备失败！" + tradeInfo);
            tradeInfo.addError("风险保额准备失败！");
            return tradeInfo;
        }
        logger.debug("风险保额准备成功！");
        return tradeInfo;
    }
}
