package com.sinosoft.cloud.cbs.rules.data;

import com.sinosoft.cloud.cbs.rules.bom.Applicant;
import com.sinosoft.cloud.cbs.rules.bom.Insured;
import com.sinosoft.cloud.cbs.rules.bom.Policy;

import com.sinosoft.cloud.rest.TradeInfo;
import com.sinosoft.lis.entity.LRRiskAmntPojo;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;


import java.util.List;

/**
 * Created by zkr on 2018/6/20.
 */
@Service
public class NewExtractAccum {
    private final Log logger = LogFactory.getLog(this.getClass());


    public void getAccum(Policy policy, TradeInfo tradeInfo){
        List<LRRiskAmntPojo> riskAmntInfos = (List<LRRiskAmntPojo>) tradeInfo.getData(LRRiskAmntPojo.class.getName());
        if(riskAmntInfos == null || riskAmntInfos.size() == 0){
            logger.debug("累计风险保额提取累计数据失败！");
            tradeInfo.addError("累计风险保额提取累计数据失败！");
            return;
        }
        if (!"00".equals(policy.getApplicant().getRelationWithInsured())){
            logger.debug("投保人与被保人不是同一人！");
            // 投保人累计
            getApplicantAccum(policy, riskAmntInfos);
            // 被保人累计
            getInsuredAccum(policy, riskAmntInfos);
        }else{
            logger.debug("投保人与被保人是同一人！");
            // 被保人累计
            getInsuredAccum(policy, riskAmntInfos);
        }
    }

    /**
     * 投保人累计
     */
     public void getApplicantAccum(Policy policy,List<LRRiskAmntPojo>  riskAmntInfos){
         // 投保人
         Applicant applicantInfo = policy.getApplicant();

         // 累计类提数
         //累计意外险风险保额Y
         applicantInfo.setSumAccidentAmount(getString(riskAmntInfos.get(0).getSumAccidentAmount()));
         //累计寿险风险保额 L
         applicantInfo.setSumLifeAmount(getString(riskAmntInfos.get(0).getSumLifeAmount()));
         //累计重疾风险保额 H.
         applicantInfo.setSumDiseaseAmount(getString(riskAmntInfos.get(0).getSumDiseaseAmount()));
         //寿险保单保额 CL.
         applicantInfo.setLifePyAmount(getString(riskAmntInfos.get(0).getLifePyAmount()));
         //累计重疾险保单保额 CH
         applicantInfo.setSumDisPyAmount(getString(riskAmntInfos.get(0).getSumDisPyAmount()));
         //意外险保单保额 CY
         applicantInfo.setAccPyAmount(getString(riskAmntInfos.get(0).getAccPyAmount()));
         //寿险体检风险保额 PL
         applicantInfo.setSumLifePhyAmount(getString(riskAmntInfos.get(0).getSumLifePhyAmount()));
         //人身险保额 LI       sumPersonAmount
         applicantInfo.setSumPersonAmount(getString(riskAmntInfos.get(0).getPersonAmount()));
         //累计风险保额 SR
         applicantInfo.setSumAmount(getString(riskAmntInfos.get(0).getSumAmount()));
         //住院医疗风险保额 M          SumHospitalizeAmount
         applicantInfo.setSumHospitalizeAmount(getString(riskAmntInfos.get(0).getHospitalizeAmount()));
         //意外医疗风险保额 N             SumAccMedicalAmount
         applicantInfo.setSumAccMedicalAmount(getString(riskAmntInfos.get(0).getAccMedicalAmount()));
         //累计津贴保额 J
         applicantInfo.setSumAcAmount(getString(riskAmntInfos.get(0).getSumAcAmount()));
         //累计自驾车风险保额 ZJ
         applicantInfo.setSumDriverAmount(getString(riskAmntInfos.get(0).getSumDriverAmount()));
         //防癌险风险保额 HC         SumAnticancerAmount
         applicantInfo.setSumAnticancerAmount(getString(riskAmntInfos.get(0).getAnticancerAmount()));
         //累计再保风险保额 ZB
         applicantInfo.setSumReinsuranceAmount(getString(riskAmntInfos.get(0).getSumReinsuranceAmount()));
         //身故累计 C.
         applicantInfo.setNonageDieAmount(getString(riskAmntInfos.get(0).getNonageDieAmount()));
         //寿险风险保额 1
         applicantInfo.setLifeAmount(getString(riskAmntInfos.get(0).getLifeAmount()));
         //重疾险风险保额 2
         applicantInfo.setDiseaseAmount(getString(riskAmntInfos.get(0).getDiseaseAmount()));
         //意外险风险保额 4
         applicantInfo.setAccidentAmount(getString(riskAmntInfos.get(0).getAccidentAmount()));
        //累计寿险保单保额 BL.
         applicantInfo.setSumLifePyAmount(0);
         //累计健康险保单保额 BH
         applicantInfo.setSumHealthPyAmount(0);
         //累计意外险保单保额 BY
         applicantInfo.setSumAccPyAmount(0);
         //累计自驾车保单保额 BJ
         applicantInfo.setSumDriverPyAmount(0);
         //累计防癌险保单保额 BC
         applicantInfo.setSumAnticPyAmount(0);
     }

    /**
     * 被保人累计
     */
    public void getInsuredAccum(Policy policy, List<LRRiskAmntPojo>  riskAmntInfos){
        // 被保人列表
        List insuredInfoList = policy.getInsuredList();
        for (int i=0;i<insuredInfoList.size();i++){
            Insured insured = (Insured) insuredInfoList.get(i);

            // 投保人累计类提数
            //累计意外险风险保额Y
            insured.setAccidentAmount(Util.toDouble((String)riskAmntInfos.get(i+1).getAccidentAmount()));
            //累计寿险风险保额 L
            insured.setSumLifeAmount(Util.toDouble((String)riskAmntInfos.get(i+1).getSumLifeAmount()));
            //累计重疾风险保额 H.
            insured.setSumDiseaseAmount(Util.toDouble((String)riskAmntInfos.get(i+1).getSumDiseaseAmount()));
            //寿险保单保额 CL.
            insured.setLifePyAmount(Util.toDouble((String)riskAmntInfos.get(i+1).getLifePyAmount()));
            //累计重疾险保单保额 CH
            insured.setSumDisPyAmount(Util.toDouble((String)riskAmntInfos.get(i+1).getSumDisPyAmount()));
            //意外险保单保额 CY
            insured.setAccPyAmount(Util.toDouble((String)riskAmntInfos.get(i+1).getAccPyAmount()));
            //寿险体检风险保额 PL
            insured.setSumLifePhyAmount(Util.toDouble((String)riskAmntInfos.get(i+1).getSumLifePhyAmount()));
            //人身险保额 LI     sumPersonAmount
            insured.setSumPersonAmount(Util.toDouble((String)riskAmntInfos.get(i+1).getPersonAmount()));
            //累计风险保额 SR
            insured.setSumAmount(Util.toDouble((String)riskAmntInfos.get(i+1).getSumAmount()));
            //住院医疗风险保额 M      SumHospitalizeAmount
            insured.setSumHospitalizeAmount(Util.toDouble((String)riskAmntInfos.get(i+1).getHospitalizeAmount()));
            //意外医疗风险保额 N      SumAccMedicalAmount
            insured.setSumAccMedicalAmount(Util.toDouble((String)riskAmntInfos.get(i+1).getAccMedicalAmount()));
            //累计津贴保额 J
            insured.setSumAcAmount(Util.toDouble((String)riskAmntInfos.get(i+1).getSumAcAmount()));
            //累计自驾车风险保额 ZJ
            insured.setSumDriverAmount(Util.toDouble((String)riskAmntInfos.get(i+1).getSumDriverAmount()));
            //防癌险风险保额 HC   SumAnticancerAmount
            insured.setSumAnticancerAmount(Util.toDouble((String)riskAmntInfos.get(i+1).getAnticancerAmount()));
            //累计再保风险保额 ZB
            insured.setSumReinsuranceAmount(Util.toDouble((String)riskAmntInfos.get(i+1).getSumReinsuranceAmount()));
            //身故累计 C.
            insured.setNonageDieAmount(Util.toDouble((String)riskAmntInfos.get(i+1).getNonageDieAmount()));
            //寿险风险保额 1
            insured.setLifeAmount(Util.toDouble((String)riskAmntInfos.get(i+1).getLifeAmount()));
            //重疾险风险保额 2
            insured.setDiseaseAmount(Util.toDouble((String)riskAmntInfos.get(i+1).getDiseaseAmount()));
            //意外险风险保额 4
            insured.setAccidentAmount(Util.toDouble((String)riskAmntInfos.get(i+1).getAccidentAmount()));
            //被保人7059累计基本保额
            insured.setBasicAmnt7059(Util.toDouble((String)riskAmntInfos.get(i+1).getBasicAmnt7059()));
            //被保人7058累计基本保额
            insured.setBasicAmnt7058(Util.toDouble((String)riskAmntInfos.get(i+1).getBasicAmnt7058()));
            //被保人7057累计基本保额
            insured.setBasicAmnt7057(Util.toDouble((String)riskAmntInfos.get(i+1).getBasicAmnt7057()));
            //被保人1030累计基本保额
            insured.setBasikAmnt1030(Util.toDouble((String)riskAmntInfos.get(i+1).getBasicAmnt1030()));
            //被保人2048累计基本保额
            insured.setBasikAmnt2048(Util.toDouble((String)riskAmntInfos.get(i+1).getBasicAmnt2048()));
            //爱永远定期寿险保费
           // insured.setInsuredString1(lrInsuredHistoryPojos.get(i+1).getInsuredString1()+"");
            //被保人的团险渠道累计重疾保额（团险）
            //insured.setInsuredSumDisAmnt(lrInsuredHistoryPojos.get(i+1).getInsuredSumDisAmnt());
            //被保人7054累计保额
            //insured.setInsured7054Amnt(lrInsuredHistoryPojos.get(i+1).getApp7056Amnt());
            //被保人2048累计保费
           // insured.setAccPrem2048(lrInsuredHistoryPojos.get(i+1).getAccPrem2048());
            //被保人6009意外险保额
           // insured.setSum6009AccidentAmount(lrInsuredHistoryPojos.get(i+1).getSum6009AccidentAmnt());
            //7056险种累计投保额
            //insured.setApp7056Amnt(lrInsuredHistoryPojos.get(i+1).getApp7056Amnt());
            //被保人c060累计保费
            //insured.setC060AccAmnt(lrInsuredHistoryPojos.get(i+1).getC060AccAmnt());
            //被保险人投保7053累计重疾险风险保额
          // insured.setAccAmnt7053(lrInsuredHistoryPojos.get(i+1).getAccAmnt7053());
            //万能险保费，5015险种已停售
           // insured.setSumStandPrem(lrInsuredHistoryPojos.get(i+1).getSumStandPrem());
            //被保险人累计c060份数
            //累计寿险保单保额 BL.
            insured.setSumLifePyAmount(Util.toDouble((String)riskAmntInfos.get(i+1).getSumLifePyAmount()));
            //累计健康险保单保额 BH
            insured.setSumHealthPyAmount(Util.toDouble((String)riskAmntInfos.get(i+1).getSumHealthPyAmount()));
            //累计意外险保单保额 BY
            insured.setSumAccPyAmount(Util.toDouble((String)riskAmntInfos.get(i+1).getSumAccPyAmount()));
            //累计自驾车保单保额 BJ
            insured.setSumDriverPyAmount(Util.toDouble((String)riskAmntInfos.get(i+1).getSumDriverPyAmount()));
            //累计防癌险保单保额 BC
            insured.setSumAnticPyAmount(Util.toDouble((String)riskAmntInfos.get(i+1).getSumAnticPyAmount()));
        }
        //投保人累计提数
        // 投保人
        Applicant applicantInfo = policy.getApplicant();

        //累计意外险风险保额Y
        applicantInfo.setAccidentAmount(0);
        //累计寿险风险保额 L
        applicantInfo.setSumLifeAmount(0);
        //累计重疾风险保额 H.
        applicantInfo.setSumDiseaseAmount(0);
        //寿险保单保额 CL.
        applicantInfo.setLifePyAmount(0);
        //累计重疾险保单保额 CH
        applicantInfo.setSumDisPyAmount(0);
        //意外险保单保额 CY
        applicantInfo.setAccPyAmount(0);
        //寿险体检风险保额 PL
        applicantInfo.setSumLifePhyAmount(0);
        //人身险保额 LI       sumPersonAmount
        applicantInfo.setSumPersonAmount(0);
        //累计风险保额 SR
        applicantInfo.setSumAmount(0);
        //住院医疗风险保额 M          SumHospitalizeAmount
        applicantInfo.setSumHospitalizeAmount(0);
        //意外医疗风险保额 N             SumAccMedicalAmount
        applicantInfo.setSumAccMedicalAmount(0);
        //累计津贴保额 J
        applicantInfo.setSumAcAmount(0);
        //累计自驾车风险保额 ZJ
        applicantInfo.setSumDriverAmount(0);
        //防癌险风险保额 HC         SumAnticancerAmount
        applicantInfo.setSumAnticancerAmount(0);
        //累计再保风险保额 ZB
        applicantInfo.setSumReinsuranceAmount(0);
        //身故累计 C.
        applicantInfo.setNonageDieAmount(0);
        //寿险风险保额 1
        applicantInfo.setLifeAmount(0);
        //重疾险风险保额 2
        applicantInfo.setDiseaseAmount(0);
        //意外险风险保额 4
        applicantInfo.setAccidentAmount(0);
        //累计寿险保单保额 BL.
        applicantInfo.setSumLifePyAmount(0);
        //累计健康险保单保额 BH
        applicantInfo.setSumHealthPyAmount(0);
        //累计意外险保单保额 BY
        applicantInfo.setSumAccPyAmount(0);
        //累计自驾车保单保额 BJ
        applicantInfo.setSumDriverPyAmount(0);
        //累计防癌险保单保额 BC
        applicantInfo.setSumAnticPyAmount(0);
    }
    //将字符串判定转化为double类型
    public double getString(String str){
          if (str==null ||str==""){
              return 0;
          }else{
              return Util.toDouble((String)str);
          }
    }
}
