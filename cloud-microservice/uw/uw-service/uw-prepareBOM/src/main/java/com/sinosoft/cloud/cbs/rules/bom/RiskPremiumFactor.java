package com.sinosoft.cloud.cbs.rules.bom;

/**
 * 险种加费系数信息
 * 
 * @author dingfan
 * 
 */
public class RiskPremiumFactor {
	/**
	 * 险种代码
	 */
	private String riskCode;

	/**
	 * 加费系数
	 */
	private double premiumFactor;

	/**
	 * 加费类型
	 */
	private String premiumType;

	/**
	 * 加费对象
	 */
	private String premiumObject;

	public RiskPremiumFactor() {
		super();
	}

	public String getRiskCode() {
		return riskCode;
	}

	public void setRiskCode(String riskCode) {
		this.riskCode = riskCode;
	}

	public double getPremiumFactor() {
		return premiumFactor;
	}

	public void setPremiumFactor(double premiumFactor) {
		this.premiumFactor = premiumFactor;
	}

	public String getPremiumType() {
		return premiumType;
	}

	public void setPremiumType(String premiumType) {
		this.premiumType = premiumType;
	}

	public String getPremiumObject() {
		return premiumObject;
	}

	public void setPremiumObject(String premiumObject) {
		this.premiumObject = premiumObject;
	}
}
