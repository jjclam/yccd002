package com.sinosoft.cloud.cbs.rules.data;

import com.sinosoft.cache.dao.impl.RedisCommonDao;
import com.sinosoft.cloud.cbs.function.UWFunction;
import com.sinosoft.cloud.cbs.rules.bom.Applicant;
import com.sinosoft.cloud.cbs.rules.bom.Policy;
import com.sinosoft.cloud.nb.service.redis.NBRedisCommon;
import com.sinosoft.cloud.rest.TradeInfo;
import com.sinosoft.lis.entity.*;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by zkr on 2018/6/28.
 */
@Component
public class NewExtractApplicantInfo {
    private final Log logger = LogFactory.getLog(this.getClass());

//    @Autowired
//    QueryRiskAmntService queryRiskAmntService;


    private static final String[] DUTYCODES = {"500101","500102","500701","500801",
            "500802","501301","500601", "500602", "500604", "500605", "500607","300301", "300302",
            "500901", "501901", "500902"};
    private  static final String[] RISKCODES = {"5001","5006","5007","5009","7020","5008","5013","3003","5019"};


    private  static final String[] DUTYCODES19 = {"500101","500102","500701","500801","500802"};

    @Autowired
    RedisCommonDao redisCommonDao;
    @Autowired
    NBRedisCommon nbRedisCommon;
    @Value("${cloud.uw.barrier.control}")
    private String barrier;
    @Autowired
    UWFunction uwFunction;


    //获取投保人信息
    public TradeInfo getApplicantInfo(Policy policy, TradeInfo requestInfo){
        LCAppntPojo tLCAppntPojo = (LCAppntPojo) requestInfo.getData(LCAppntPojo.class.getName());
        LCContPojo tLCContPojo = (LCContPojo) requestInfo.getData(LCContPojo.class.getName());
        List<LCPremPojo> tLCPremPojos = (List<LCPremPojo>) requestInfo.getData(LCPremPojo.class.getName());
        Applicant applicantInfo = policy.getApplicant();
        long beginTime = System.currentTimeMillis();
        /** 投保人基本数据SQL（contno：合同号）*/;
        if (tLCAppntPojo != null) {
            FDate fDate = new FDate();
            // 姓名
            applicantInfo.setApplicantName(tLCAppntPojo.getAppntName());
            // 证件类型
            applicantInfo.setIdentityType(tLCAppntPojo.getIDType());
            // 证件号码
            applicantInfo.setIdentityCode(tLCAppntPojo.getIDNo());
            // 性别
            applicantInfo.setSex(tLCAppntPojo.getAppntSex());
            // 身高
            applicantInfo.setHeight(tLCAppntPojo.getStature());
            // 体重
            applicantInfo.setWeight(tLCAppntPojo.getAvoirdupois());
            // 出生日期
            applicantInfo.setBirthday(fDate.getDate(tLCAppntPojo.getAppntBirthday()));
            if(applicantInfo.getBirthday()!=null&&!"".equals(applicantInfo.getBirthday())&&policy.getEffectiveDate()!=null&&!"".equals(policy.getEffectiveDate())){
                // 年龄
                applicantInfo.setAge(PubFun
                        .calInterval(applicantInfo.getBirthday(), policy
                                .getEffectiveDate(), "Y"));
            }
            // 证件有效期
            applicantInfo.setIdentityValidityTerm(tLCAppntPojo.getIdValiDate());
            // 国籍
            applicantInfo.setNationality(tLCAppntPojo.getNativePlace());
            // 户籍
            applicantInfo.setHouseholdRegister(tLCAppntPojo.getRgtAddress());
            // 学历
            applicantInfo.setEducation(tLCAppntPojo.getDegree());
            // 婚姻状况
            applicantInfo.setMarrageStatus(tLCAppntPojo.getMarriage());
            // 与被保人关系
            applicantInfo.setRelationWithInsured(tLCAppntPojo.getRelatToInsu());
            // 职业代码
            applicantInfo.setOccuCode(tLCAppntPojo.getOccupationCode());
            // 职业类别
            applicantInfo.setOccuType(tLCAppntPojo.getOccupationType());
            // 职业描述
            applicantInfo.setOccuDescribe(tLCAppntPojo.getWorkType());
            // 兼职
            applicantInfo.setPartTimeJobFlag(tLCAppntPojo.getPluralityType());
            // 是否有摩托车驾照
            applicantInfo
                    .setMotorLicence("Y".equals(tLCAppntPojo.getHaveMotorcycleLicence()) ? true
                            : false);
            // 驾照类型
            applicantInfo.setLicenceType(tLCAppntPojo.getLicenseType());
            // 地址代码
            applicantInfo.setAddrCode(tLCAppntPojo.getAddressNo());
            // BMI
            applicantInfo.setBMI(tLCAppntPojo.getBMI()+"");
            // 居民类型
            applicantInfo.setResidentType(tLCAppntPojo.getAppRgtTpye());
            // 职务
            applicantInfo.setDuties(tLCAppntPojo.getPosition());
            // 投保人客户号
            applicantInfo.setClientNo(tLCAppntPojo.getAppntNo());
            // 客户级别
            applicantInfo.setClientLevel(tLCAppntPojo.getCUSLevel());
            // 美国纳税人识别号(TIN)
            applicantInfo.setTinFlag("1".equals(tLCAppntPojo.getTINFlag()) ? true
                    : false);
        }

        String custNo = "";
        if (applicantInfo != null) {
            custNo = applicantInfo.getClientNo();
        }
        logger.debug("投保人基础-提数：" + (System.currentTimeMillis() - beginTime) / 1000.0
                + "s");
        beginTime = System.currentTimeMillis();
        List<LCPolPojo> tLCPolPojoList = (List) requestInfo.getData(LCPolPojo.class.getName());
        /** 本单累计期交保费（contno：合同号） */
        double contPerioPrem = 0;
        /** 本单累计趸交保费（contno：合同号） */
        double contSinglePrem = 0;

        if(tLCPolPojoList != null && tLCPolPojoList.size()>0){
            for(int i=0;i<tLCPolPojoList.size();i++){
                if(tLCPolPojoList.get(i).getPayIntv() != 0){
                    //本单累计期交保费
                    contPerioPrem += tLCPolPojoList.get(i).getPrem();;
                } else {
                    //本单累计趸交保费
                    contSinglePrem += tLCPolPojoList.get(i).getPrem();
                }
            }
        }

        applicantInfo.setPeriodPremium(contPerioPrem);
        applicantInfo.setSinglePremium(contSinglePrem);
        /** 风险测评SQL（contno：合同号）*/
        LYVerifyAppPojo tLYVerifyAppPojo = (LYVerifyAppPojo) requestInfo.getData(LYVerifyAppPojo.class.getName());
        if(tLYVerifyAppPojo !=null){
            applicantInfo.setRiskAssessment(tLYVerifyAppPojo.getRiskEvaluationResult());
        }
        /** 平均年收入SQL（COMCODE：机构代码，cityavg：城镇平均年收入，valligeavg：乡村平均年收入） */
        LDAvgInComPojo ldAvgInComPojo = null;
        if(policy.getManageOrg() != null && policy.getManageOrg().length() > 4) {
            ldAvgInComPojo = redisCommonDao.getEntityRelaDB(LDAvgInComPojo.class, policy.getManageOrg().substring(0,4));
        }else if(policy.getManageOrg() != null && policy.getManageOrg().length() == 4){
            ldAvgInComPojo = redisCommonDao.getEntityRelaDB(LDAvgInComPojo.class, policy.getManageOrg());
        }
        if(ldAvgInComPojo != null){
            double cityAvg = ldAvgInComPojo.getCITYINCOME();
            double valligeAvg = ldAvgInComPojo.getVILLAGEINCOME();
            applicantInfo.setPerDisposIncome(cityAvg);
            // 农村居民人均纯收入
            applicantInfo.setPerIncome(valligeAvg);
        }
        /** 投保人是否残疾SQL（customerno：客户编码，contno：合同号） */
        List<LCCustomerImpartParamsPojo> tLCCustomerImpartParamsList = (List<LCCustomerImpartParamsPojo>) requestInfo.getData(LCCustomerImpartParamsPojo.class.getName());
        /** 黑名单标识SQL（BLACKLISTNO：黑名单客户号，IDNO：证件号码，IDTYPE：证件类型） */
        boolean blackSign = uwFunction.checkBlackSign(tLCAppntPojo.getAppntNo(), tLCAppntPojo.getIDNo(), tLCAppntPojo.getIDType());
        applicantInfo.setBlackSign(blackSign);
        /** 投保人身高异常SQL （CONTNO：合同号，APPNTNO：投保人编码） */
        double tStature = tLCAppntPojo.getStature();
        SSRS tSSRS = nbRedisCommon.checkHeightErr(tLCAppntPojo.getAppntSex(), String.valueOf(new PubFun().calInterval3(tLCAppntPojo.getAppntBirthday(),tLCContPojo.getCValiDate(),"Y")));
        if(tStature <= 0 || tSSRS == null){
            applicantInfo.setHeightErr(false);
        }else if(tStature < Double.parseDouble(tSSRS.GetText(1,1)) || tStature > Double.parseDouble(tSSRS.GetText(1,2))){
            applicantInfo.setHeightErr(true);
        }else {
            applicantInfo.setHeightErr(false);
        }
        /** 投保人体重异常SQL 成年（CONTNO：合同号，APPNTNO：投保人编码） */
        double tAvoirdupois = tLCAppntPojo.getAvoirdupois();
        if(tAvoirdupois <= 0 || tSSRS == null){
            applicantInfo.setWeightErr(false);
        }else if(tAvoirdupois < Double.parseDouble(tSSRS.GetText(1,3)) || tAvoirdupois > Double.parseDouble(tSSRS.GetText(1,4))){
            applicantInfo.setWeightErr(true);
        }else {
            applicantInfo.setWeightErr(false);
        }
        long minBMI = 0;
        long maxBMI = 0;
        if(tStature == 0){
            minBMI = Math.round(tAvoirdupois/1000000);
        }else {
            minBMI = Math.round(tAvoirdupois/((tStature/100)*(tStature/100)));
        }
        if(tStature == 0){
            maxBMI = Math.round(tAvoirdupois/0.0001);
        }else {
            maxBMI = Math.round(tAvoirdupois/((tStature/100)*(tStature/100)));
        }
        if(minBMI<18 || maxBMI>28){
            applicantInfo.setBmiErr(true);
        }else {
            applicantInfo.setBmiErr(false);
        }

        // 是否填写投保人告知SQL（CONTNO：合同号，CUSTOMERNO：客户编码）
        boolean appNotify = false;
        if(tLCCustomerImpartParamsList != null && tLCCustomerImpartParamsList.size() > 0){
            for(int i=0;i<tLCCustomerImpartParamsList.size();i++){
                if("0".equals(tLCCustomerImpartParamsList.get(i).getCustomerNoType())){
                    appNotify = true;
                }
            }
        }
        applicantInfo.setAppNotify(appNotify);
        /** 黑名单1-证件号与和黑名单相同,不分国籍,>0触发,参数:一个contno */
        boolean appBackIdFlag = uwFunction.checkBlackIdFlag(tLCAppntPojo.getIDNo());
        applicantInfo.setAppBlackIdFlag(appBackIdFlag);
        /** 黑名单2-中国客户名字、生日与黑名单相同,>0触发,参数:一个contno */
        SimpleDateFormat sdf =   new SimpleDateFormat( "yyyy-MM-dd" );
        if("CHN".equals(tLCAppntPojo.getNativePlace())){
            boolean checkBlackCHNFlag = uwFunction.checkBlackCHNFlag(tLCAppntPojo.getAppntName(), tLCAppntPojo.getAppntBirthday());
            applicantInfo.setAppBlackChnFlag(checkBlackCHNFlag);
        }
        /** 黑名单3-投保人名1部分组成,投保人名与黑名单表四个名字对比,>0触发,参数:4个contno */
        boolean checkBlackFlag1 = uwFunction.checkBlackFlag1(tLCAppntPojo.getAppntName(), tLCAppntPojo.getNativePlace(), tLCAppntPojo.getAppntBirthday());
        applicantInfo.setAppBlackFlag1(checkBlackFlag1);
        /** 黑名单4-投保人名2部分组成,投保人名与黑名单表2个名字对比,>0触发,参数:2个contno
         * 有一个name相同，且出生日期相同 */
        boolean checkBlackFlag2 = uwFunction.checkBlackFlag2(tLCAppntPojo.getAppntName(), tLCAppntPojo.getNativePlace(), tLCAppntPojo.getAppntBirthday());
        applicantInfo.setAppBlackFlag2(checkBlackFlag2);
        /** 黑名单5-投保人名2部分组成,投保人名与黑名单表2个名字对比,>=2触发,参数:2个contno
         * firstname和surnname都要相同.无生日限制 */
        int count = 0;
        if(tLCAppntPojo.getAppntName().length() - tLCAppntPojo.getAppntName().replace(" ","").length() == 1
                && (tLCAppntPojo.getNativePlace() == null || !"CHN".equals(tLCAppntPojo.getNativePlace()))){
            String appntName = tLCAppntPojo.getAppntName();
            String firstName = appntName.substring(0, appntName.indexOf(" "));
            String surName = appntName.substring(appntName.indexOf(" ")+1);
            count += uwFunction.checkBlackFlag3_1(firstName);
            count += uwFunction.checkBlackFlag3_3(surName);
        }
        if(count >= 2){
            applicantInfo.setAppBlackFlag3(true);
        }
        /** 黑名单6-投保人名>=3部分组成,投保人名与黑名单表3个名字对比,>=0触发,参数:3个contno
         * firstname,middlename,surname,有生日限制 */
        boolean checkBlackFlag4 = uwFunction.checkBlackFlag4(tLCAppntPojo.getAppntName(), tLCAppntPojo.getNativePlace(), tLCAppntPojo.getAppntBirthday());
        applicantInfo.setAppBlackFlag4(checkBlackFlag4);
        /** 黑名单7-投保人名>=3部分组成,投保人名与黑名单表3个名字对比,>=2触发,参数:3个contno
         * firstname,middlename,surname,无生日限制 */
        count = 0;
        if(tLCAppntPojo.getAppntName().length() - tLCAppntPojo.getAppntName().replace(" ","").length() >= 2
                && (tLCAppntPojo.getNativePlace() == null || !"CHN".equals(tLCAppntPojo.getNativePlace()))){
            String appntName = tLCAppntPojo.getAppntName();
            String firstName = appntName.substring(0, appntName.indexOf(" "));
            String middleName = appntName.substring(appntName.indexOf(" ")+1, appntName.lastIndexOf(" "));
            String surName = appntName.substring(appntName.lastIndexOf(" ")+1);
            count += uwFunction.checkBlackFlag3_1(firstName);
            count += uwFunction.checkBlackFlag3_2(middleName);
            count += uwFunction.checkBlackFlag3_3(surName);
        }
        if(count >= 2){
            applicantInfo.setAppBlackFlag5(true);
        }

        /** 投保人7053险种各期保费之和 */
        String insuredsex = null;
        int insuredappage = 0;
        int insuyear = 0;
        int payintv = 0;
        double amnt = 0.00;

        double renewalPremSum7053 = 0.00;
        for(LCPolPojo lcPolPojo : tLCPolPojoList) {
            if("7053".equals(lcPolPojo.getRiskCode())){
                insuredsex = lcPolPojo.getInsuredSex();
                insuredappage = lcPolPojo.getInsuredAppAge();
                insuyear = lcPolPojo.getInsuYear();
                payintv = lcPolPojo.getPayIntv();
                amnt = lcPolPojo.getAmnt();
                if(insuredappage > 70){
                    break;
                }
                VData tVData = new VData();
                StringBuffer sql1 = new StringBuffer();
                sql1.append("SELECT SUM("+ amnt +"/1000 * RATE * MUTIPAYINTV("+ payintv +")) FROM RT_7053 ");
                sql1.append("WHERE SEX = ? AND APPAGE BETWEEN ? AND 70 AND PAYENDYEAR = 1 AND INSUYEAR = ? ");
                TransferData transferData = new TransferData();
                transferData.setNameAndValue("sex", "string:"+ insuredsex);
                transferData.setNameAndValue("appage", "int:" + insuredappage );
                transferData.setNameAndValue("insuyear", "int:" + insuyear);
                ExeSQL exeSql1 = new ExeSQL();
                tVData.add(sql1.toString());
                tVData.add(transferData);
                String result = exeSql1.getOneValue(tVData);
                if(!"".equals(result)){
                    renewalPremSum7053 += Double.parseDouble(result);
                }
                break;
            }
        }
        applicantInfo.setRenewalPremSum7053(renewalPremSum7053);

        //投保人老核心提数
        if("true".equals(barrier)){
            logger.debug("保单号：" + tLCContPojo.getContNo()+ "，累计风险保额投保人挡板启动！");
        }else {
            logger.debug("保单号：" + tLCContPojo.getContNo()+ "，累计风险保额投保人提数开始！");
            getOtherData(applicantInfo, requestInfo,tLCAppntPojo);
            if(requestInfo.hasError()){
                requestInfo.addError("累计风险保额投保人提数失败！");
                return requestInfo;
            }
        }
        //特殊保费3，险种5001,、5007、5008已停售
        double applicantString1 = 0.0;
        if(applicantInfo.getApplicantString1() != null
                && !"".equals(applicantInfo.getApplicantString1())){
            applicantString1 = Util.toDouble(applicantInfo.getApplicantString1());
        }
        // 本单累计年交保费去除'5013','3003'险种,新增本单累计
        double applicantDouble2 = applicantInfo.getApplicantDouble2();
        //银保渠道累计保费
        double ybPrem = applicantInfo.getYbPrem();
        List<String> polnos = new ArrayList<>();
        for(LCPolPojo lcPolPojo:tLCPolPojoList){
            if(lcPolPojo.getPayEndYear() != 1
                    && lcPolPojo.getPayEndYear() != 3){
                polnos.add(lcPolPojo.getPolNo());
            }
            if(lcPolPojo.getPayIntv() == 12
                    || lcPolPojo.getPayIntv() ==1
                    || lcPolPojo.getPayIntv() == 3
                    || lcPolPojo.getPayIntv() == 6){
                boolean riskFlag = false;
                for(String riskcode : RISKCODES){
                    if(riskcode.equals(lcPolPojo.getRiskCode())){
                        riskFlag = true;
                        break;
                    }
                }
                if(!riskFlag){
                    applicantDouble2 += (12/lcPolPojo.getPayIntv())*lcPolPojo.getPrem();
                }
            }
            if("3".equals(lcPolPojo.getSaleChnl())
                    && lcPolPojo.getPayIntv() != 0
                    && ((lcPolPojo.getPaytoDate()!=null
                    && !lcPolPojo.getPaytoDate().equals(lcPolPojo.getEndDate()))
                    || lcPolPojo.getPaytoDate() == null)){
                ybPrem += lcPolPojo.getPrem();
            }
			/*if(!(1==lcPolPojo.getPayEndYear()
					|| 3==lcPolPojo.getPayEndYear())){
				//特殊保费3，险种5001,、5007、5008已停售
				applicantString1 += lcPolPojo.getPrem();
			}*/
        }


        for(String polno : polnos){
            for(LCPremPojo lcPremPojo : tLCPremPojos){
                if(polno.equals(lcPremPojo.getPolNo())){
                    for(int j=0;j<DUTYCODES19.length;j++){
                        if(DUTYCODES19[j].equals(lcPremPojo.getDutyCode())){
                            applicantString1 += lcPremPojo.getPrem();
                            break;
                        }
                    }
                }
            }
        }
        // 特殊保费2
        double applicantDouble1 = applicantInfo.getApplicantDouble1();
        for(String polno : polnos){
            for(LCPremPojo lcPremPojo : tLCPremPojos){
                if(polno.equals(lcPremPojo.getPolNo())){
                    for(int j=0;j<DUTYCODES.length;j++){
                        if(DUTYCODES[j].equals(lcPremPojo.getDutyCode())){
                            applicantDouble1 += lcPremPojo.getPrem();
                            break;
                        }
                    }
                }
            }
        }
        int payIntv = 0;
        double premPerYear = applicantInfo.getPremiumPerYear();
        if(tLCPolPojoList != null && tLCPolPojoList.size()>0){
            for(int i=0;i<tLCPolPojoList.size();i++){
                payIntv = tLCPolPojoList.get(i).getPayIntv();
                if(payIntv != 12 & payIntv !=1 && payIntv != 3 && payIntv !=6){
                    continue;
                }
                if ("7020".equals(tLCPolPojoList.get(i).getRiskCode()) || "5019".equals(tLCPolPojoList.get(i).getRiskCode())) {
                    continue;
                }
                premPerYear += (12 / payIntv) * tLCPolPojoList.get(i).getPrem();
            }
        }
        // 特殊保费2
        applicantInfo.setApplicantDouble1(applicantDouble1);
        // 本单累计年交保费去除'5013','3003'险种
        applicantInfo.setApplicantDouble2(applicantDouble2);
        //银保渠道累计保费
        applicantInfo.setYbPrem(ybPrem);
        //本单累计年缴保费
        applicantInfo.setPremiumPerYear(premPerYear);
        // 投保人累计期交保费
        applicantInfo.setSumPeriodPremium(applicantInfo.getSumPeriodPremium() + contPerioPrem);
        // 投保人累计趸交保费
        applicantInfo.setSumSinglePremium(applicantInfo.getSumSinglePremium() + contSinglePrem);
        //特殊保费3
        applicantInfo.setApplicantString1(applicantString1 + "");

        logger.debug("累计风险保额投保人拓展-提数："+(System.currentTimeMillis() - beginTime)/1000.0+"s");
       return requestInfo;

    }

    //投保人老核心提数
    public TradeInfo getOtherData(Applicant applicantInfo, TradeInfo tradeInfo ,LCAppntPojo lcAppntPojo){
        String custNo = "";
        if (applicantInfo != null) {
            custNo = applicantInfo.getClientNo();
        }
        try{
//            long beginTime = System.currentTimeMillis();
//            logger.debug("调用累计风险保额开始："+beginTime);
//            tradeInfo= queryRiskAmntService.service(tradeInfo);
//            logger.debug("调用累计风险保额成功！提数："+(System.currentTimeMillis() - beginTime)/1000.0 + "s");
            LRApplicantHistoryPojo lrApplicantHistoryPojo=(LRApplicantHistoryPojo)tradeInfo.getData(LRApplicantHistoryPojo.class.getName());
            // 投保人累计期交保费
            applicantInfo.setSumPeriodPremium(lrApplicantHistoryPojo.getSumPeriodPremium());
            // 投保人累计趸交保费
            applicantInfo.setSumSinglePremium(lrApplicantHistoryPojo.getSumSinglePremium());
            // 本单累计年交保费
            applicantInfo.setPremiumPerYear(lrApplicantHistoryPojo.getPremiumPerYear());
            // 银保渠道保费
            applicantInfo.setYbPrem(lrApplicantHistoryPojo.getYbPrem());
            // 特殊保费1
            applicantInfo.setSpecialPrem1(lrApplicantHistoryPojo.getSpecialPrem1());
            //特殊保费2
            applicantInfo.setApplicantDouble1(lrApplicantHistoryPojo.getSpecialPrem2());
            // 本单累计年交保费去除'5013','3003'险种
            applicantInfo.setApplicantDouble2(lrApplicantHistoryPojo.getApplicantDouble2());
            // 特殊保费3
            applicantInfo.setApplicantString1(lrApplicantHistoryPojo.getSpecialPrem3()+"");
            // 中保信上一年度理赔额 wpq
            applicantInfo.setYearClaimLimit(0);
        }catch (Exception e){
            e.printStackTrace();
            logger.error("调用累计风险保额失败");
        }
        return tradeInfo;
    }
}
