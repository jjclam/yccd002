package com.sinosoft.cloud.cbs.trules.util;

/**
 * @Author:wangshuliang
 * @Description:
 * @Date:Created in 18:03 2018/7/18
 * @Modified by:
 */
public enum SellTypeEnum {
    ABCOTC("08","ABCOTC"),ABCSELF("24","ABCSELF"),ABCESTWD("26","ABCESTWD"),ABCMOBILE("25","ABCMOBILE"),ABCEBANK("22","ABCEBANK"),
    ABCSUPOTC("27","ABCSUPOTC"),HNNX0001("99","HNNX00001"),WECHAT("21","WECHAT"),WEBSITE("20","WEBSITE");
    private String sellType;
    private String resultCode;
    public String getSellType() {
        return sellType;
    }

    public void setSellType(String sellType) {
        this.sellType = sellType;
    }

    public String getResultCode() {
        return resultCode;
    }

    public void setResultCode(String resultCode) {
        this.resultCode = resultCode;
    }


    SellTypeEnum(String sellType,String code) {
        this.sellType =sellType;
        this.resultCode =code;
    }


    /**
     * 循环遍历枚举类 取得contplancode的转换值
     * @param sellType 保险计划编码
     * @return
     */
    public static String getSellType(String sellType){
        for (SellTypeEnum sellTypeEnum : SellTypeEnum.values()){
            if (sellTypeEnum.getSellType().equals(sellType)){
                return sellTypeEnum.getResultCode();
            }
        }
        return "8888";
    }
}
