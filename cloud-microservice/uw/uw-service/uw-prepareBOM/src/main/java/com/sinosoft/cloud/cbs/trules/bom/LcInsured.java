package com.sinosoft.cloud.cbs.trules.bom;

/**
 * 被保人既往信息
 * 
 */
public class LcInsured {
	/**
	 * 累计同一被保人死亡风险保额（未成年）
	 */
	private double accMortalRiskAmnt;
	/**
	 * 累计同一被保人疾病身故保额（1806和1805）
	 */
	private double accDiseaseDieAmnt;
	/**
	 * 累计同一被保人套餐有效保单数量
	 */
	private double accInsurdPackageValidNumber;
	/**
	 * 累计同一被保人套餐有效保单份数
	 */
	private double accInsurdPackageValidCopies;
	/**
	 * 累计同一被保人航班飞机意外险保额（6810和6805）
	 */
	private double accInsurdFlightAccidentAmnt;
	/**
	 * 累计同一被保人火车地铁意外险保额（6810和6805）
	 */
	private double accInsurdTrainAccidentAmnt;
	/**
	 * 累计同一被保人轮船摆渡意外险保额（6810和6805）
	 */
	private double accInsurdTrafficAccidentAmnt;
	/**
	 * 累计同一被保人自驾车意外险保额（6810和6805）
	 */
	private double accInsurdPrivateCarAccidentAmnt;
	/**
	 * 累计同一被保人市内公交意外险保额（6810和6805）
	 */
	private double accInsurdCommonalityBusAccidentAmnt;
	/**
	 * 累计同一被保人长途汽车意外险保额（6810和6805）
	 */
	private double accInsurdLongBusAccidentAmnt;
	/**
	 * 累计同一被保人出租车意外险保额（6810和6805）
	 */
	private double accInsurdTaxiAccidentAmnt;
	/**
	 * 累计同一被保人借款人意外险保额（6807）
	 */
	private double accInsurdBorrowBeyondAmnt;
	/**
	 * 累计同一中介机构同一借款合同号的不同被保人的数量
	 */
	private double accAgencyBorrowDiffInsurdNum;
	/**
	 * 累计同一中介机构同一借款合同号的相同被保人的数量
	 */
	private double accAgencyBorrowInsurdNum;
	/**
	 * 累计山东同一借款合同号所有借款人意外险保额（6807）
	 */
	private double accShanDongBorrowRiskAmnt;
	/**
	 * 累计同一被保人航班飞机意外险保额（6810）
	 */
	private double accInsurdMyComAllPlanAccidentAmnt;
	/**
	 * 是否存在理赔记录信息
	 */
	private String whetherSettlementInfo;
	/**
	 * 是否存在延期拒保信息
	 */
	private String whetherPostponeUnAccept;
	/**
	 * 累计全国同一借款合同号下6807险种保单保额
	 */
	private double accQuanGuoBorrowRiskAmnt;
	
	/**
	 * 随E行交通意外保障计划累计保费
	 */
	private double accSeABCDPrem;
	
	/**
	 * 公交出租车意外伤害保额
	 */
//	private double accBusTaxiAmnt;
	
	/**
	 * SEE、SEN保险计划份数
	 */
	private int pastSeeSenNum;

	/**
	 * SEO保险计划份数
	 */
	private int pastSeoNum;

	/**
	 * SEL、SEM保险计划份数
	 */
	private int pastSelSemNum;

	/**
	 * 自驾车意外伤害累计责任保额
	 */
	private double accCarAmnt;

	/**
	 * 累计随E驾交通意外保费
	 */
	private double accSEGAmnt;
	
	/**
	 * 累计随E乘交通意外保费
	 */
	private double accSEHAmnt;
	
	/**
	 * 累计至尊行交通意外保费
	 */
	private double accSEFAmnt;
	
	/**
	 * 累计随意飞交通意外保费
	 */
	private double accSEIAmnt;
	
	/**
	 * 累计长途汽车意外伤害保额
	 */
	private double acc681006Amnt;
	
	/**
	 * 累计火车、地铁意外伤害保额
	 */
	private double accTrainSubwayAmnt;
	
	/**
	 * 累计轮船、摆渡意外伤害责任保额
	 */
	private double accSteamerAmnt;

	/**
	 * 累计航班飞机意外伤害责任保额
	 */
	private double accPlaneAmnt;

	/**
	 *SEP保险计划份数
	 */
	private double pastSEPNum;

	/**
	 * 出租车意外伤害保额
	 */
	private double accTaxiAmnt;

	/**
	 * 公交车意外伤害保额
	 */
	private double accBusAmnt;

	/**
	 * 累计同一被保人JSJXBC投保份数
	 */
	private double pastJSJXBCNum;
	/**
	 * 累计同一被保人JSJXBB投保份数
	 */
	private double pastJSJXBBNum;
	/**
	 * 累计同一被保人6808吉祥意外伤害保险保额
	 */
	private double pastJSJXBAAmnt;
	/**
	 * 累计SESXNH份数
	 */
	private double pastSESXNHNum;

	public double getPastSESXNHNum() {return pastSESXNHNum;}

	public void setPastSESXNHNum(double pastSESXNHNum) {this.pastSESXNHNum = pastSESXNHNum;}

	public double getPastJSJXBCNum() {
		return pastJSJXBCNum;
	}

	public void setPastJSJXBCNum(double pastJSJXBCNum) {
		this.pastJSJXBCNum = pastJSJXBCNum;
	}

	public double getPastJSJXBBNum() {
		return pastJSJXBBNum;
	}

	public void setPastJSJXBBNum(double pastJSJXBBNum) {
		this.pastJSJXBBNum = pastJSJXBBNum;
	}

	public double getPastJSJXBAAmnt() {
		return pastJSJXBAAmnt;
	}

	public void setPastJSJXBAAmnt(double pastJSJXBAAmnt) {
		this.pastJSJXBAAmnt = pastJSJXBAAmnt;
	}

	public double getAccTaxiAmnt() {
		return accTaxiAmnt;
	}

	public void setAccTaxiAmnt(double accTaxiAmnt) {
		this.accTaxiAmnt = accTaxiAmnt;
	}

	public double getAccBusAmnt() {
		return accBusAmnt;
	}

	public void setAccBusAmnt(double accBusAmnt) {
		this.accBusAmnt = accBusAmnt;
	}

	public double getPastSEPNum() {
		return pastSEPNum;
	}

	public void setPastSEPNum(double pastSEPNum) {
		this.pastSEPNum = pastSEPNum;
	}

	public double getAccMortalRiskAmnt() {
		return accMortalRiskAmnt;
	}
	public void setAccMortalRiskAmnt(double accMortalRiskAmnt) {
		this.accMortalRiskAmnt = accMortalRiskAmnt;
	}
	public double getAccDiseaseDieAmnt() {
		return accDiseaseDieAmnt;
	}
	public void setAccDiseaseDieAmnt(double accDiseaseDieAmnt) {
		this.accDiseaseDieAmnt = accDiseaseDieAmnt;
	}
	public double getAccInsurdPackageValidNumber() {
		return accInsurdPackageValidNumber;
	}
	public void setAccInsurdPackageValidNumber(double accInsurdPackageValidNumber) {
		this.accInsurdPackageValidNumber = accInsurdPackageValidNumber;
	}
	public double getAccInsurdPackageValidCopies() {
		return accInsurdPackageValidCopies;
	}
	public void setAccInsurdPackageValidCopies(double accInsurdPackageValidCopies) {
		this.accInsurdPackageValidCopies = accInsurdPackageValidCopies;
	}
	public double getAccInsurdFlightAccidentAmnt() {
		return accInsurdFlightAccidentAmnt;
	}
	public void setAccInsurdFlightAccidentAmnt(double accInsurdFlightAccidentAmnt) {
		this.accInsurdFlightAccidentAmnt = accInsurdFlightAccidentAmnt;
	}
	public double getAccInsurdTrainAccidentAmnt() {
		return accInsurdTrainAccidentAmnt;
	}
	public void setAccInsurdTrainAccidentAmnt(double accInsurdTrainAccidentAmnt) {
		this.accInsurdTrainAccidentAmnt = accInsurdTrainAccidentAmnt;
	}
	public double getAccInsurdTrafficAccidentAmnt() {
		return accInsurdTrafficAccidentAmnt;
	}
	public void setAccInsurdTrafficAccidentAmnt(double accInsurdTrafficAccidentAmnt) {
		this.accInsurdTrafficAccidentAmnt = accInsurdTrafficAccidentAmnt;
	}
	public double getAccInsurdPrivateCarAccidentAmnt() {
		return accInsurdPrivateCarAccidentAmnt;
	}
	public void setAccInsurdPrivateCarAccidentAmnt(
			double accInsurdPrivateCarAccidentAmnt) {
		this.accInsurdPrivateCarAccidentAmnt = accInsurdPrivateCarAccidentAmnt;
	}
	public double getAccInsurdCommonalityBusAccidentAmnt() {
		return accInsurdCommonalityBusAccidentAmnt;
	}
	public void setAccInsurdCommonalityBusAccidentAmnt(
			double accInsurdCommonalityBusAccidentAmnt) {
		this.accInsurdCommonalityBusAccidentAmnt = accInsurdCommonalityBusAccidentAmnt;
	}
	public double getAccInsurdLongBusAccidentAmnt() {
		return accInsurdLongBusAccidentAmnt;
	}
	public void setAccInsurdLongBusAccidentAmnt(double accInsurdLongBusAccidentAmnt) {
		this.accInsurdLongBusAccidentAmnt = accInsurdLongBusAccidentAmnt;
	}
	public double getAccInsurdTaxiAccidentAmnt() {
		return accInsurdTaxiAccidentAmnt;
	}
	public void setAccInsurdTaxiAccidentAmnt(double accInsurdTaxiAccidentAmnt) {
		this.accInsurdTaxiAccidentAmnt = accInsurdTaxiAccidentAmnt;
	}
	public double getAccInsurdBorrowBeyondAmnt() {
		return accInsurdBorrowBeyondAmnt;
	}
	public void setAccInsurdBorrowBeyondAmnt(double accInsurdBorrowBeyondAmnt) {
		this.accInsurdBorrowBeyondAmnt = accInsurdBorrowBeyondAmnt;
	}
	public double getAccAgencyBorrowDiffInsurdNum() {
		return accAgencyBorrowDiffInsurdNum;
	}
	public void setAccAgencyBorrowDiffInsurdNum(double accAgencyBorrowDiffInsurdNum) {
		this.accAgencyBorrowDiffInsurdNum = accAgencyBorrowDiffInsurdNum;
	}
	public double getAccAgencyBorrowInsurdNum() {
		return accAgencyBorrowInsurdNum;
	}
	public void setAccAgencyBorrowInsurdNum(double accAgencyBorrowInsurdNum) {
		this.accAgencyBorrowInsurdNum = accAgencyBorrowInsurdNum;
	}
	public double getAccShanDongBorrowRiskAmnt() {
		return accShanDongBorrowRiskAmnt;
	}
	public void setAccShanDongBorrowRiskAmnt(double accShanDongBorrowRiskAmnt) {
		this.accShanDongBorrowRiskAmnt = accShanDongBorrowRiskAmnt;
	}
	public double getAccInsurdMyComAllPlanAccidentAmnt() {
		return accInsurdMyComAllPlanAccidentAmnt;
	}
	public void setAccInsurdMyComAllPlanAccidentAmnt(
			double accInsurdMyComAllPlanAccidentAmnt) {
		this.accInsurdMyComAllPlanAccidentAmnt = accInsurdMyComAllPlanAccidentAmnt;
	}
	public String getWhetherSettlementInfo() {
		return whetherSettlementInfo;
	}
	public void setWhetherSettlementInfo(String whetherSettlementInfo) {
		this.whetherSettlementInfo = whetherSettlementInfo;
	}
	public String getWhetherPostponeUnAccept() {
		return whetherPostponeUnAccept;
	}
	public void setWhetherPostponeUnAccept(String whetherPostponeUnAccept) {
		this.whetherPostponeUnAccept = whetherPostponeUnAccept;
	}
	public double getAccQuanGuoBorrowRiskAmnt() {
		return accQuanGuoBorrowRiskAmnt;
	}
	public void setAccQuanGuoBorrowRiskAmnt(double accQuanGuoBorrowRiskAmnt) {
		this.accQuanGuoBorrowRiskAmnt = accQuanGuoBorrowRiskAmnt;
	}
	public double getAccSeABCDPrem() {
		return accSeABCDPrem;
	}
	public void setAccSeABCDPrem(double accSeABCDPrem) {
		this.accSeABCDPrem = accSeABCDPrem;
	}
//	public double getAccBusTaxiAmnt() {
//		return accBusTaxiAmnt;
//	}
//	public void setAccBusTaxiAmnt(double accBusTaxiAmnt) {
//		this.accBusTaxiAmnt = accBusTaxiAmnt;
//	}
	public int getPastSeeSenNum() {
		return pastSeeSenNum;
	}
	public void setPastSeeSenNum(int pastSeeSenNum) {
		this.pastSeeSenNum = pastSeeSenNum;
	}
	public int getPastSeoNum() {return pastSeoNum;}
	public void setPastSeoNum(int pastSeoNum) {this.pastSeoNum = pastSeoNum;}
	public int getPastSelSemNum() {return pastSelSemNum;}
	public void setPastSelSemNum(int pastSelSemNum) {this.pastSelSemNum = pastSelSemNum;}
	public double getAccCarAmnt() {
		return accCarAmnt;
	}
	public void setAccCarAmnt(double accCarAmnt) {
		this.accCarAmnt = accCarAmnt;
	}
	public double getAccSEGAmnt() {
		return accSEGAmnt;
	}
	public void setAccSEGAmnt(double accSEGAmnt) {
		this.accSEGAmnt = accSEGAmnt;
	}
	public double getAccSEHAmnt() {
		return accSEHAmnt;
	}
	public void setAccSEHAmnt(double accSEHAmnt) {
		this.accSEHAmnt = accSEHAmnt;
	}
	public double getAccSEFAmnt() {
		return accSEFAmnt;
	}
	public void setAccSEFAmnt(double accSEFAmnt) {
		this.accSEFAmnt = accSEFAmnt;
	}
	public double getAccSEIAmnt() {
		return accSEIAmnt;
	}
	public void setAccSEIAmnt(double accSEIAmnt) {
		this.accSEIAmnt = accSEIAmnt;
	}
	public double getAcc681006Amnt() {
		return acc681006Amnt;
	}
	public void setAcc681006Amnt(double acc681006Amnt) {
		this.acc681006Amnt = acc681006Amnt;
	}
	public double getAccTrainSubwayAmnt() {
		return accTrainSubwayAmnt;
	}
	public void setAccTrainSubwayAmnt(double accTrainSubwayAmnt) {
		this.accTrainSubwayAmnt = accTrainSubwayAmnt;
	}
	public double getAccSteamerAmnt() {
		return accSteamerAmnt;
	}
	public void setAccSteamerAmnt(double accSteamerAmnt) {
		this.accSteamerAmnt = accSteamerAmnt;
	}
	public double getAccPlaneAmnt() {
		return accPlaneAmnt;
	}
	public void setAccPlaneAmnt(double accPlaneAmnt) {
		this.accPlaneAmnt = accPlaneAmnt;
	}
}