package com.sinosoft.cloud.cbs.trules.data;

import com.sinosoft.cache.dao.impl.RedisCommonDao;
import com.sinosoft.cloud.cbs.trules.bom.Duty;
import com.sinosoft.cloud.cbs.trules.bom.DutyList;
import com.sinosoft.cloud.cbs.trules.bom.Product;
import com.sinosoft.cloud.rest.TradeInfo;
import com.sinosoft.lis.entity.LCDutyPojo;
import com.sinosoft.lis.entity.LCPolPojo;
import com.sinosoft.lis.entity.LMDutyPojo;
import com.sinosoft.lis.pubfun.FDate;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author:wangshuliang
 * @Description: 获取责任信息
 * @Date:Created in 9:52 2018/6/5
 * @Modified by:
 */
@Component
public class TDutyBL {
    Log logger = LogFactory.getLog(getClass());
    @Autowired
    RedisCommonDao redisCommonDao;

    /**
     * 获取责任信息
     *
     * @param tradeInfo
     * @return
     */
    public TradeInfo getDuty(TradeInfo tradeInfo) {
        FDate fDate = new FDate();
        Product product = (Product) tradeInfo.getData(Product.class.getName());
        List<Duty> dutyListDuty=new ArrayList<>();
        List<LCDutyPojo> lcDutyPojos = (List<LCDutyPojo>) tradeInfo.getData(LCDutyPojo.class.getName());
        List<LCPolPojo> lcPolPojos = (List<LCPolPojo>) tradeInfo.getData(LCPolPojo.class.getName());
        if (lcPolPojos != null && lcDutyPojos.size() > 0) {
            for (LCPolPojo lcpojo : lcPolPojos) {
                for (int i = 0; i < lcDutyPojos.size(); i++) {
                    if (lcpojo.getPolNo().equals(lcDutyPojos.get(i).getPolNo())) {
                        Duty duty = new Duty();
                        //险种编码
                        duty.setRiskCode(lcpojo.getRiskCode());
                        //险种版本
                        duty.setRiskVersion(lcpojo.getRiskVersion());
                        //责任编码
                        duty.setDutyCode(lcDutyPojos.get(i).getDutyCode());
                        //责任名称
                        LMDutyPojo lmDutyPojo = redisCommonDao.getEntityRelaDB(LMDutyPojo.class, lcDutyPojos.get(i).getDutyCode());
                        if (lmDutyPojo != null) {
                            duty.setDutyName(lmDutyPojo.getDutyName());
                        }
                        //保额
                        duty.setAmnt(lcDutyPojos.get(i).getAmnt());
                        //保费
                        duty.setPrem(lcDutyPojos.get(i).getPrem());
                        //浮动费率
                        duty.setFloatRate(lcDutyPojos.get(i).getFloatRate());
                        //生效日期
                        duty.setValiDate(lcDutyPojos.get(i).getCValiDate());
                        //终止日期
                        duty.setEndDate(lcDutyPojos.get(i).getEndDate());
                        //交费间隔
                        duty.setPayIntv(String.valueOf(lcDutyPojos.get(i).getPayIntv()));
                        //终交年龄年期标志
                        duty.setPayEndYearFlag(lcDutyPojos.get(i).getPayEndYearFlag());
                        //终交年龄年期
                        duty.setPayEndYear(String.valueOf(lcDutyPojos.get(i).getPayEndYear()));
                        //保险年龄年期标志
                        duty.setInsuYearFlag(lcDutyPojos.get(i).getInsuYearFlag());
                        //保险年龄年期
                        duty.setInsuYear(String.valueOf(lcDutyPojos.get(i).getInsuYear()));
                        //续保标志
                        duty.setRnewFlag(String.valueOf(lcpojo.getRnewFlag()));
                        //自动垫缴标志
                        duty.setAutoPayFlag(lcpojo.getAutoPayFlag());
                        dutyListDuty.add(duty);
                    }
                }
            }
        }
        product.setDuties(dutyListDuty);
        return tradeInfo;
    }
}
