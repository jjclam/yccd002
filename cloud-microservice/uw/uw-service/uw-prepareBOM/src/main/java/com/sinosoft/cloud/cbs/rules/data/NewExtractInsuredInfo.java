package com.sinosoft.cloud.cbs.rules.data;

import com.sinosoft.cloud.cbs.function.UWFunction;
import com.sinosoft.cloud.cbs.rules.bom.Insured;
import com.sinosoft.cloud.cbs.rules.bom.Policy;
import com.sinosoft.cloud.nb.service.redis.NBRedisCommon;
import com.sinosoft.cloud.rest.TradeInfo;
import com.sinosoft.lis.entity.*;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.lis.pubfun.PubFun;
//import com.sinosoft.lis.vschema.LMWhiteListSet;
import com.sinosoft.utility.ExceptionUtils;
import com.sinosoft.utility.SSRS;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by zkr on 2018/6/28.
 */
@Service
public class NewExtractInsuredInfo {
    private final Log logger = LogFactory.getLog(this.getClass());
    @Value("${cloud.uw.barrier.control}")
    private String barrier;

    @Autowired
    NBRedisCommon nbRedisCommon;
    @Autowired
    UWFunction uwFunction;


    //获取被保人信息
    public List getInsuredInfo(Policy policy, TradeInfo requestInfo){
        LCContPojo lcContPojo = (LCContPojo) requestInfo.getData(LCContPojo.class.getName());
        List<LCInsuredPojo> lcInsuredPojoList = (List<LCInsuredPojo>) requestInfo.getData(LCInsuredPojo.class.getName());
        LCAppntPojo lcAppntPojo =  (LCAppntPojo) requestInfo.getData(LCAppntPojo.class.getName());
        FDate fDate = new FDate();
        List insuredInfoList = policy.getInsuredList();
        List<LCInsuredPojo> tLCInsuredPojoList = (List) requestInfo.getData(LCInsuredPojo.class.getName());
        List<LCPolPojo> tLCPolPojoList = (List) requestInfo.getData(LCPolPojo.class.getName());
        long beginTime = System.currentTimeMillis();
        //第一被保人
        LCInsuredPojo lcInsuredPojo = tLCInsuredPojoList.get(0);
        if (null != lcInsuredPojo) {
            Insured insured = new Insured();
            // 姓名
            insured.setInsuredName(lcInsuredPojo.getName());
            // 证件类型
            insured.setIdentityType(lcInsuredPojo.getIDType());
            // 证件号码
            insured.setIdentityCode(lcInsuredPojo.getIDNo());
            // 性别
            insured.setSex(lcInsuredPojo.getSex());
            // 出生日期
            insured.setBirthday(fDate.getDate(lcInsuredPojo.getBirthday()));
            if (insured.getBirthday() != null
                    && !"".equals(insured.getBirthday())
                    && policy.getEffectiveDate() != null
                    && !policy.getEffectiveDate().equals("")) {
                // 年龄
                insured.setAge(PubFun.calInterval(insured.getBirthday(),
                        policy.getEffectiveDate(), "Y"));
            }
            // 身高
            insured.setHeight(lcInsuredPojo.getStature());
            // 体重
            insured.setWeight(lcInsuredPojo.getAvoirdupois());
            // 证件有效期
            insured.setIdentityValidityTerm(lcInsuredPojo.getIdValiDate());
            // 国籍
            insured.setNationality(lcInsuredPojo.getNativePlace());
            // 户籍
            insured.setHouseholdRegister(lcInsuredPojo.getRgtAddress());
            // 学历
            insured.setEducation(lcInsuredPojo.getDegree());
            // 婚姻状况
            insured.setMarrageStatus(lcInsuredPojo.getMarriage());
            // 与第一被保人关系
            insured.setRelationWithFirstInsured(lcInsuredPojo.getRelationToMainInsured());
            // 职业代码
            insured.setOccuCode(lcInsuredPojo.getOccupationCode());
            // 职业类别
            insured.setOccuType(lcInsuredPojo.getOccupationType());
            // 职业描述
            insured.setOccuDescribe(lcInsuredPojo.getWorkType());
            // 兼职
            insured.setPartTimeJobFlag(lcInsuredPojo.getPluralityType());
            // 驾照类型
            insured.setLicenceType(lcInsuredPojo.getLicenseType());
            // 地址代码
            insured.setAddrCode(lcInsuredPojo.getAddressNo());
            // BMI
            insured.setBMI(lcInsuredPojo.getBMI()+"");
            // 英文姓名
            //wsl 20180125 start
            StringBuffer englishName = new StringBuffer();
            if(lcInsuredPojo.getFirstName()==null){
                englishName.append("").append(".");
            }else{
                englishName.append(lcInsuredPojo.getFirstName()).append(".");
            }
            if (lcInsuredPojo.getLastName()==null){
                englishName.append("");
            }else{
                englishName.append(lcInsuredPojo.getLastName());
            }
            insured.setEnglishName(englishName.toString());
            //wsl 20180125 end
            // 健康声明
            insured.setHealthDeclare(lcInsuredPojo.getHealth());
            // 职务
            insured.setDuties(lcInsuredPojo.getPosition());
            // 被保险人客户号
            insured.setClientNo(lcInsuredPojo.getInsuredNo());
            // 与投保人关系
            insured.setRelationWithAppnt(lcInsuredPojo.getRelationToAppnt());

            logger.debug("累计风险保额被保人基础-提数：" + (System.currentTimeMillis() - beginTime) / 1000.0
                    + "s");

            String custNo = insured.getClientNo();
            // 被保人是否残疾SQL（customerno：客户编码，contno：合同号）
            List<LCCustomerImpartParamsPojo> tLCCustomerImpartParamsList = (List)requestInfo.getData(LCCustomerImpartParamsPojo.class.getName());
			/*boolean disabledSign = false;
			if(tLCCustomerImpartParamsList != null && tLCCustomerImpartParamsList.size() > 0){
				for(int j=0;j<tLCCustomerImpartParamsList.size();j++){
					if(custNo.equals(tLCCustomerImpartParamsList.get(j).getCustomerNo())
							&& "0".equals(tLCCustomerImpartParamsList.get(j).getCustomerNoType())
							&& "1".equals(tLCCustomerImpartParamsList.get(j).getImpartParam())
							&& "YesOrNo".equals(tLCCustomerImpartParamsList.get(j).getImpartParamName())){
						String impartVer = tLCCustomerImpartParamsList.get(j).getImpartVer();
						String impartCode = tLCCustomerImpartParamsList.get(j).getImpartCode();
						if(("DLR3".equals(impartVer) && "1-3".equals(impartCode))
								|| ("GX8".equals(impartVer) && "18".equals(impartCode) || "10".equals(impartCode))
								|| ("GX10".equals(impartVer) && "3".equals(impartCode))){
							disabledSign = true;
							break;
						}
					}
				}
			}
			insured.setDisabledSign(disabledSign);*/
            // 被保人黑名单标识SQL（BLACKLISTNO：黑名单客户号，IDNO：证件号码，IDTYPE：证件类型）
            boolean blackSign = uwFunction.checkBlackSign(lcInsuredPojo.getInsuredNo(), lcInsuredPojo.getIDNo(), lcInsuredPojo.getIDType());
            insured.setBlackSign(blackSign);
            //被保人身高异常
            SimpleDateFormat sdf =   new SimpleDateFormat( "yyyy-MM-dd" );
            double tStature = lcInsuredPojo.getStature();
            //zym add at 2017/11/1 18:00 开门红需求修改
            if (insured.getAge() >= 17) {
//			if (insured.getAge() >= 18) {
                // 被保人身高异常SQL,成年人（CONTNO：合同号，INSUREDNO：被保人编码）
                SSRS tSSRS = nbRedisCommon.checkHeightErr(lcInsuredPojo.getSex(), String.valueOf(new PubFun().calInterval3(lcInsuredPojo.getBirthday(),lcContPojo.getCValiDate(),"Y")));
                if(tStature <= 0 || tSSRS == null || tSSRS.MaxRow == 0){
                    insured.setHeightErr(false);
                }else if(tStature < Double.parseDouble(tSSRS.GetText(1,1)) || tStature > Double.parseDouble(tSSRS.GetText(1,2))){
                    insured.setHeightErr(true);
                }else {
                    insured.setHeightErr(false);
                }
            } else {
                if(insured.getAge() < 7){
                    //被保人身高异常SQL,未成年,小于7岁（CONTNO：合同号，APPNTNO：投保人编码）
                    SSRS tSSRS = uwFunction.checkHeightErr1(lcInsuredPojo.getSex(), tLCPolPojoList.get(0).getCValiDate(), lcInsuredPojo.getBirthday());
                    if(tStature <= 0 || tSSRS == null || tSSRS.MaxRow == 0){
                        insured.setHeightErr(false);
                    }else if(tStature < Double.parseDouble(tSSRS.GetText(1,1)) || tStature > Double.parseDouble(tSSRS.GetText(1,2))){
                        insured.setHeightErr(true);
                    }else {
                        insured.setHeightErr(false);
                    }
                }else{
                    //被保人身高异常SQL,未成年,7到18岁（CONTNO：合同号，APPNTNO：投保人编码）
                    SSRS tSSRS = uwFunction.checkHeightErr1(lcInsuredPojo.getSex(), tLCPolPojoList.get(0).getCValiDate(), lcInsuredPojo.getBirthday());
                    if(tStature <= 0 || tSSRS == null || tSSRS.MaxRow == 0){
                        insured.setHeightErr(false);
                    }else if(tStature < Double.parseDouble(tSSRS.GetText(1,1))){
                        insured.setHeightErr(true);
                    }else {
                        insured.setHeightErr(false);
                    }

                }
            }
            //被保人体重异常
            double tAvoirdupois = lcInsuredPojo.getAvoirdupois();
            //zym add at 2017/11/1 18:00 开门红需求修改
            if (insured.getAge() >= 17) {
//			if (insured.getAge() >= 18) {
                // 被保人体重异常SQL 成年（CONTNO：合同号，INSUREDNO：被保人编码）
                SSRS tSSRS = nbRedisCommon.checkHeightErr(lcInsuredPojo.getSex(), String.valueOf(new PubFun().calInterval3(lcInsuredPojo.getBirthday(),lcContPojo.getCValiDate(),"Y")));
                if(tAvoirdupois <= 0 || tSSRS == null || tSSRS.MaxRow == 0){
                    insured.setWeightErr(false);
                }else if(tAvoirdupois < Double.parseDouble(tSSRS.GetText(1,3)) || tAvoirdupois > Double.parseDouble(tSSRS.GetText(1,4))){
                    insured.setWeightErr(true);
                }else {
                    insured.setWeightErr(false);
                }
            } else {
                if(insured.getAge() < 7){
                    //被保人体重异常SQL 未成年,小于7岁（CONTNO：合同号，APPNTNO：投保人编码）
                    SSRS tSSRS = uwFunction.checkHeightErr1(lcInsuredPojo.getSex(), tLCPolPojoList.get(0).getCValiDate(),lcInsuredPojo.getBirthday());
                    if(tAvoirdupois <= 0 || tSSRS == null || tSSRS.MaxRow == 0){
                        insured.setWeightErr(false);
                    }else if(tAvoirdupois < Double.parseDouble(tSSRS.GetText(1,3)) || tAvoirdupois > Double.parseDouble(tSSRS.GetText(1,4))){
                        insured.setWeightErr(true);
                    }else {
                        insured.setWeightErr(false);
                    }
                }else{
                    //被保人体重异常SQL 未成年,7岁到18岁（CONTNO：合同号，APPNTNO：投保人编码）
                    SSRS tSSRS = uwFunction.checkHeightErr1(lcInsuredPojo.getSex(),tLCPolPojoList.get(0).getCValiDate(), lcInsuredPojo.getBirthday());
                    if(tAvoirdupois <= 0 || tSSRS == null || tSSRS.MaxRow == 0){
                        insured.setWeightErr(false);
                    }else if(tAvoirdupois < Double.parseDouble(tSSRS.GetText(1,3))){
                        insured.setWeightErr(true);
                    }else {
                        insured.setWeightErr(false);
                    }
                }

            }
            // 被保人BMI异常 根据是否成年拼接sql
            long minBMI = 0;
            long maxBMI = 0;
            if (insured.getAge() >= 18) {
                if(tStature == 0){
                    minBMI = Math.round(tAvoirdupois/1000000);
                }else {
                    minBMI = Math.round(tAvoirdupois/((tStature/100)*(tStature/100)));
                }
                if(tStature == 0){
                    maxBMI = Math.round(tAvoirdupois/0.0001);
                }else {
                    maxBMI = Math.round(tAvoirdupois/((tStature/100)*(tStature/100)));
                }
                if(minBMI<18 || maxBMI>28){
                    insured.setBmiErr(true);
                }else {
                    insured.setBmiErr(false);
                }
            } else {
                //未成年
                String height = "0";
                String weight = "0";
                long bmi = 0;
                List<LCCustomerImpartParamsPojo> lcCustomerImpartParamsPojos = (List<LCCustomerImpartParamsPojo>) requestInfo.getData(LCCustomerImpartParamsPojo.class.getName());
                if(lcCustomerImpartParamsPojos != null && lcCustomerImpartParamsPojos.size()>0){
                    for(int j=0;j<lcCustomerImpartParamsPojos.size();j++){
                        if(!("19055".equals(lcCustomerImpartParamsPojos.get(j).getPatchNo())
                                && "GX17".equals(lcCustomerImpartParamsPojos.get(j).getImpartVer())
                                && "12A".equals(lcCustomerImpartParamsPojos.get(j).getImpartCode())
                                && "1".equals(lcCustomerImpartParamsPojos.get(j).getCustomerNoType()))){
                            continue;
                        }
                        if("1".equals(lcCustomerImpartParamsPojos.get(j).getImpartParamNo())
                                && "Height".equals(lcCustomerImpartParamsPojos.get(j).getImpartParamName())){
                            height = lcCustomerImpartParamsPojos.get(j).getImpartParam();
                        }
                        if("2".equals(lcCustomerImpartParamsPojos.get(j).getImpartParamNo())
                                && "Weight".equals(lcCustomerImpartParamsPojos.get(j).getImpartParamName())){
                            weight = lcCustomerImpartParamsPojos.get(j).getImpartParam();
                        }
                    }
                    if("0".equals(height)){
                        height = "100000";
                    }
                    bmi = Math.round(Double.parseDouble(weight)/((Double.parseDouble(height)/100)*(Double.parseDouble(height)/100)));
                }
                if(bmi<11 || bmi >17){
                    insured.setBmiErr(true);
                }else {
                    insured.setBmiErr(false);
                }
            }
            // 摩托车驾照
			/*if("1".equals(lcInsuredPojo.getHaveMotorcycleLicence())){
				insured.setMotorLicence(true);
			}else{
				insured.setMotorLicence(false);
			}*/

            //投保人在其他公司的保额
            int otherAmnt = 0;
            double amnt7058 = 0;
            if(tLCPolPojoList!=null && tLCPolPojoList.size()>0){
                for(int j=0;j<tLCPolPojoList.size();j++){
                    if(custNo.equals(tLCPolPojoList.get(j).getInsuredNo())){
                        otherAmnt += tLCPolPojoList.get(j).getOtherAmnt();
                    }
                    if("7058".equals(tLCPolPojoList.get(j).getRiskCode())){
                        amnt7058 = tLCPolPojoList.get(j).getAmnt();
                    }
                }
            }
            insured.setOtherAmnt(otherAmnt);
            // 白名单标识
            try {
                SSRS ssrs = uwFunction.checkInsuWhiteSign(lcInsuredPojo.getName(), lcInsuredPojo.getSex(), lcInsuredPojo.getIDType(), lcInsuredPojo.getIDNo(), lcInsuredPojo.getBirthday());
                insured.setWhiteSign((ssrs==null || ssrs.MaxRow==0)?false:true);
                // 白名单限额
                insured.setWhiteSignAmnt((ssrs==null || ssrs.MaxRow==0)?0:Double.valueOf(ssrs.GetText(1,1)));
            }catch (Exception e){
                logger.error("白名单取值异常"+ExceptionUtils.exceptionToString(e)+requestInfo.toString());
                requestInfo.addError("白名单取值异常"+ExceptionUtils.exceptionToString(e));
                return null;
            }
            //投保人被保人为同一人时 跳过被保人黑名单1-7 从投保人中取
            if(!lcAppntPojo.getAppntName().equals(lcInsuredPojoList.get(0).getName())) {
                //黑名单1-证件号与和黑名单相同,不分国籍,>0触发,参数:一个contno
                //{this} 的证件号与和黑名单相同
                boolean backIdFlag = uwFunction.checkBlackIdFlag(lcInsuredPojo.getIDNo());
                insured.setInsBlackIdFlag(backIdFlag);
                //黑名单2-中国客户名字、生日与黑名单相同,>0触发,参数:一个contno
                //{this}为中国客户且名字和生日与黑名单相同
                if ("CHN".equals(lcInsuredPojo.getNativePlace())) {
                    boolean checkBlackCHNFlag = uwFunction.checkBlackCHNFlag(lcInsuredPojo.getName(), lcInsuredPojo.getBirthday());
                    insured.setInsBlackChnFlag(checkBlackCHNFlag);
                }
                //黑名单3-被保人名1部分组成,被保人名与黑名单表四个名字对比,>0触发,参数:4个contno
                //this}的名字与黑名单名字和生日相同(名字为一部分)
                boolean checkBlackFlag1 = uwFunction.checkBlackFlag1(lcInsuredPojo.getName(), lcInsuredPojo.getNativePlace(), lcInsuredPojo.getBirthday());
                insured.setInsBlackFlag1(checkBlackFlag1);
                //黑名单4-被保人名2部分组成,被保人名与黑名单表2个名字对比,>0触发,参数:2个contno
                //有一个name相同，且出生日期相同
                //{this}的名字与黑名单名字和生日相同(名字为两部分)
                boolean checkBlackFlag2 = uwFunction.checkBlackFlag2(lcInsuredPojo.getName(), lcInsuredPojo.getNativePlace(), lcInsuredPojo.getBirthday());
                insured.setInsBlackFlag2(checkBlackFlag2);
                //黑名单5-被保人名2部分组成,被保人名与黑名单表2个名字对比,>=2触发,参数:2个contno
                //firstname和surnname都要相同.无生日限制
                //{this} 的名字与黑名单名字和生日相同(名字为两部分,无生日限制)
                int count = 0;
                if (lcInsuredPojo.getName().length() - lcInsuredPojo.getName().replace(" ", "").length() == 1
                        && (lcInsuredPojo.getNativePlace() == null || !"CHN".equals(lcInsuredPojo.getNativePlace()))) {
                    String insuName = lcInsuredPojo.getName();
                    String firstName = insuName.substring(0, insuName.indexOf(" "));
                    String surName = insuName.substring(insuName.indexOf(" ") + 1);
                    count += uwFunction.checkBlackFlag3_1(firstName);
                    count += uwFunction.checkBlackFlag3_3(surName);
                }
                if (count >= 2) {
                    insured.setInsBlackFlag3(false);
                }
                //黑名单6-被保人名>=3部分组成,被保人名与黑名单表>=3个名字对比,>=0触发,参数:3个contno
                //firstname,middlename,surname,有生日限制
                //{this} 的名字与黑名单名字两个以上相同(名字为四部分)
                boolean checkBlackFlag4 = uwFunction.checkBlackFlag4(lcInsuredPojo.getName(), lcInsuredPojo.getNativePlace(), lcInsuredPojo.getBirthday());
                insured.setInsBlackFlag4(checkBlackFlag4);
                //黑名单7-被保人名>=3部分组成,被保人名与黑名单表>=3个名字对比,>=2触发,参数:3个contno
                //firstname,middlename,surname,无生日限制
                //{this} 的名字与黑名单相同(被保人名三部分构成无生日限制)
                count = 0;
                if (lcInsuredPojo.getName().length() - lcInsuredPojo.getName().replace(" ", "").length() >= 2
                        && (lcInsuredPojo.getNativePlace() == null || !"CHN".equals(lcInsuredPojo.getNativePlace()))) {
                    String insuName = lcInsuredPojo.getName();
                    String firstName = insuName.substring(0, insuName.indexOf(" "));
                    String middleName = insuName.substring(insuName.indexOf(" ") + 1, insuName.lastIndexOf(" "));
                    String surName = insuName.substring(insuName.lastIndexOf(" ") + 1);
                    count += uwFunction.checkBlackFlag3_1(firstName);
                    count += uwFunction.checkBlackFlag3_2(middleName);
                    count += uwFunction.checkBlackFlag3_3(surName);
                }
                if (count >= 2) {
                    insured.setInsBlackFlag5(true);
                }
            }
            Map map = new HashMap();
            if("true".equals(barrier)){
                logger.debug("保单号：" + lcContPojo.getContNo() + "，累计风险保额被保人挡板启动！");
            }else {
                logger.debug("保单号：" + lcContPojo.getContNo() + "，累计风险保额被保人提数开始！");
                getOtherData(insured, requestInfo, map, amnt7058);
                if(requestInfo.hasError()){
                    requestInfo.addError("累计风险保额被保人提数失败！");
                    return insuredInfoList;
                }
            }
            insured.setExtendMap(map);
            if (requestInfo.getData("amnt7058")==null){
                insured.setBasicAmnt7058(0);
            }else{
                amnt7058 = (double) requestInfo.getData("amnt7058");
                insured.setBasicAmnt7058(amnt7058);
            }
            //5018税优健康险的BMI --wpq--
            String result = uwFunction.getBMI(lcInsuredPojo.getContNo(),lcInsuredPojo.getInsuredNo());
            insured.setBmiValue(Util.toDouble(result));
            //5018税优健康险投保份数  --wpq--
            String copies=uwFunction.getCopies5018(lcInsuredPojo.getInsuredNo());
            insured.setCopies5018(Util.toInt(copies));
            insuredInfoList.add(insured);
        }
        return insuredInfoList;
    }

    //被保人提数
    public void getOtherData(Insured insured, TradeInfo tradeInfo, Map map, double amnt7058){
        String custNo = insured.getClientNo();
        LCContPojo lcContPojo = (LCContPojo) tradeInfo.getData(LCContPojo.class.getName());
        //保单号
        List<LRInsuredDataPojo> lrInsuredDataPojos=(List<LRInsuredDataPojo>)tradeInfo.getData(LRInsuredDataPojo.class.getName());
        LRInsuredDataPojo lrInsuredDataPojo=lrInsuredDataPojos.get(0);
        List<LRRiskAmntPojo> lrRiskAmntPojos=(List<LRRiskAmntPojo>)tradeInfo.getData(LRRiskAmntPojo.class.getName());
        LRRiskAmntPojo lrRiskAmntPojo=lrRiskAmntPojos.get(0);
        // for (int i=0;i<lrInsuredDataPojos.size();i++){
            // 黑名单标记
           // insured.setBlackSign(true);
            // 身高异常
           // insured.setHeightErr(true);
            // 体重异常
            //insured.setWeightErr(true);
            // BMI异常
          //  insured.setBmiErr(true);
            // 6009险意外险风险保额
            insured.setSum6009AccidentAmount(lrInsuredDataPojo.getSum6009AccidentAmnt());
            // 万能险保额
            insured.setSumStandPrem(lrInsuredDataPojo.getSumStandPrem());
            // 爱永远定期寿险保费
            insured.setInsuredString1(lrInsuredDataPojo.getInsuredString1()+"");
            // 被保人为白名单客户
           // insured.setWhiteSign(true);
            // 被保人为白名单限额
           // insured.setWhiteSignAmnt(lrInsuredDataPojo.get);
        // 被保人团险累计重疾保额
           map.put("团险累计重疾保额",lrInsuredDataPojo.getInsuredSumDisAmnt());
        // 7054险种累计保额
        map.put("7054险种累计保额",lrInsuredDataPojo.getInsured7054Amnt());
        //团险渠道长期重疾险(7829)有拒保、延期记录
        boolean flag18 = "1".equals(lrInsuredDataPojo.getRefuseRecord7829()) ? true : false;
        insured.setRefuseRecord7829(flag18);
        //被保人的团险渠道长期重疾险(7829)有次标准体记录
        boolean flag19 = "1".equals(lrInsuredDataPojo.getSubStandardRecode7829()) ? true : false;
        insured.setSubStandardRecode7829(flag19);
        //增加7056险种累计投保额
        if("".equals(lrInsuredDataPojo.getApp7056Amnt()) ){ // || lrInsuredDataPojo.getApp7056Amnt()==null
            map.put("7056险种累计投保额", "0");
        }else{
            map.put("7056险种累计投保额", lrInsuredDataPojo.getApp7056Amnt());
        }

//        //投保人在其他公司保额
//        if("".equals(lrRiskAmntPojo.get) || tSSRS.GetText(j, 1)==null ){
//            insured.setOtherAmnt(0);
//        }else{
//            insured.setOtherAmnt(Util.toDouble(tSSRS.GetText(j, 1)));
//        }
        //同业拒保标记赋值
       int field;
        if("".equals(lrInsuredDataPojo.getDeclineFlag()) || lrInsuredDataPojo.getDeclineFlag()==null || lrInsuredDataPojo.getDeclineFlag().equals("null") ){
            field = 0;
        }else{

            field = Integer.parseInt(lrInsuredDataPojo.getDeclineFlag());
        }
        logger.debug("拒保:"+field);
       boolean edge = field>0 ? true : false;
        insured.setDeclineFlag(edge);

        //同业投保超2次标记赋值

        if("".equals(lrInsuredDataPojo.getApplicationFlag()) || lrInsuredDataPojo.getApplicationFlag()==null || lrInsuredDataPojo.getApplicationFlag().equals("null") ){
            field = 0;
        }else{

            field = Integer.parseInt(lrInsuredDataPojo.getApplicationFlag());
        }
        logger.debug("投保:"+field);
        edge = field>2 ? true : false;
        insured.setApplicationFlag(edge);

        //被保人 7059险种累计基本保额

        if("".equals(lrRiskAmntPojo.getBasicAmnt7059()) || lrRiskAmntPojo.getBasicAmnt7059()==null || lrRiskAmntPojo.getBasicAmnt7059().equals("null") ){
            field = 0;
            insured.setBasicAmnt7059(field);
        }else{

            insured.setBasicAmnt7059(Util.toDouble(lrRiskAmntPojo.getBasicAmnt7059()));
        }
        //被保人有防癌重疾保全记录
        logger.debug("cancer_recodex:"+lrInsuredDataPojo.getCancerClaimRecord());
        if("".equals(lrInsuredDataPojo.getCancerClaimRecord()) || lrInsuredDataPojo.getCancerClaimRecord()==null || lrInsuredDataPojo.getCancerClaimRecord().equals("null") ){

            insured.setCancerClaimRecord(false);

        }else{
            field = Integer.parseInt(lrInsuredDataPojo.getCancerClaimRecord());
            edge = field > 0 ? true: false;
            insured.setCancerClaimRecord(edge);
        }

        //被保人有意外险理赔记录
        logger.debug("accident_recodex:"+lrInsuredDataPojo.getAccidentClaimRecord());
        if("".equals(lrInsuredDataPojo.getAccidentClaimRecord()) || lrInsuredDataPojo.getAccidentClaimRecord()==null || lrInsuredDataPojo.getAccidentClaimRecord().equals("null") ){

            insured.setAccidentClaimRecord(false);

        }else{
            field = Integer.parseInt(lrInsuredDataPojo.getAccidentClaimRecord());
            edge = field > 0 ? true: false;
            insured.setAccidentClaimRecord(edge);
        }

        //7057累计基本保额
        logger.debug("7057_amnt:"+lrRiskAmntPojo.getBasicAmnt7057());
        if("".equals(lrRiskAmntPojo.getBasicAmnt7057()) || lrRiskAmntPojo.getBasicAmnt7057()==null || lrRiskAmntPojo.getBasicAmnt7057().equals("null") ){

            insured.setBasicAmnt7057(0);

        }else{
            insured.setBasicAmnt7057(Util.toDouble(lrRiskAmntPojo.getBasicAmnt7057()));
        }
        //7058累计基本保额

        if("".equals(lrRiskAmntPojo.getBasicAmnt7058()) || lrRiskAmntPojo.getBasicAmnt7058()==null || lrRiskAmntPojo.getBasicAmnt7058().equals("null") ){
           // insured.setBasicAmnt7058(0);
        }else{
            amnt7058 += Util.toDouble(lrRiskAmntPojo.getBasicAmnt7058());
            tradeInfo.addData("amnt7058",amnt7058);
        }
        //被保人1030累计基础保费之和

        if("".equals(lrRiskAmntPojo.getBasicAmnt1030()) || lrRiskAmntPojo.getBasicAmnt1030()==null || lrRiskAmntPojo.getBasicAmnt1030().equals("null") ){
            insured.setBasikAmnt1030(0);
        }else{
            insured.setBasikAmnt1030(Util.toDouble(lrRiskAmntPojo.getBasicAmnt1030()));
        }

        //被保人2048累计保费
        logger.debug("被保人2048险种累计保费:"+lrInsuredDataPojo.getAccPrem2048());
        if("".equals(lrInsuredDataPojo.getAccPrem2048()) || lrInsuredDataPojo.getAccPrem2048()==0){
            insured.setAccPrem2048(0);

        }else{
            insured.setAccPrem2048(lrInsuredDataPojo.getAccPrem2048());
        }

        //被保人2048累计基础保费之和
        logger.debug("被保人2048险种累计保费:"+lrRiskAmntPojo.getBasicAmnt2048());
        if("".equals(lrRiskAmntPojo.getBasicAmnt2048()) || lrRiskAmntPojo.getBasicAmnt2048()==null || lrRiskAmntPojo.getBasicAmnt2048().equals("null") ){
            insured.setAccPrem2048(0);

        }else{
            insured.setAccPrem2048(Util.toDouble(lrRiskAmntPojo.getBasicAmnt2048()));
        }
        //被保人c060累计保费  --wpq--
        logger.debug("被保人c060县中计划累计保额:"+lrInsuredDataPojo.getC060AccAmnt());
        if("".equals(lrInsuredDataPojo.getC060AccAmnt()) || lrInsuredDataPojo.getC060AccAmnt()==0 ){
            insured.setC060AccAmnt(0);

        }else{
            insured.setC060AccAmnt(lrInsuredDataPojo.getC060AccAmnt());
        }

        //被保险人累计C060份数  --wpq--
        logger.debug("被保险人累计C060份数"+lrInsuredDataPojo.getC060Copies());
        insured.setC060Copies(lrInsuredDataPojo.getC060Copies());

        //被保险人投保7053累计重疾险风险保额  --wpq--
        logger.debug("被保险人投保7053累计重疾险风险保额:"+lrInsuredDataPojo.getAccAmnt7053());
        if("".equals(lrInsuredDataPojo.getAccAmnt7053()) || lrInsuredDataPojo.getAccAmnt7053()==0 ){
            insured.setAccAmnt7053(0);
        }else{
            insured.setAccAmnt7053(lrInsuredDataPojo.getAccAmnt7053());
        }

        //非工作单位组织的个人投保标记 --wpq--
//        if("1".equals(lrInsuredDataPojo.getPersonalInsurance())){
//            insured.setPersonalInsurance(true);
//        }else{
            insured.setPersonalInsurance(false);
       // }

        //工作单位首次或再次组织的投保标记  --wpq--
      //  if("".equals(lrInsuredDataPojo.getOrganizationInsurance()) || lrInsuredDataPojo.getOrganizationInsurance()==null || lrInsuredDataPojo.getOrganizationInsurance().equals("null") ){
            insured.setOrganizationInsurance(" ");
//        }if("1".equals(lrInsuredDataPojo.getOrganizationInsurance())){
//            insured.setOrganizationInsurance("一年内");
//        }if("2".equals(lrInsuredDataPojo.getOrganizationInsurance())) {
//            insured.setOrganizationInsurance("新入职员工");
//        }
//        if("3".equals(lrInsuredDataPojo.getOrganizationInsurance())) {
//            insured.setOrganizationInsurance("一年后");
//        }

        //被保险人是否已拥有7060或7061的有效保单 2018/3/27 zfx
        if("".equals(lrInsuredDataPojo.getHave7060Or7061()) || lrInsuredDataPojo.getHave7060Or7061()==null || lrInsuredDataPojo.getHave7060Or7061().equals("null") ){
            insured.setHave7060Or7061(false);
        }else{
            field = Integer.parseInt(lrInsuredDataPojo.getHave7060Or7061());
            edge = field > 0 ? true: false;
            insured.setHave7060Or7061(edge);
        }

        //C066 险种各期保费之和

        if("".equals(lrInsuredDataPojo.getC060AccAmnt()) || lrInsuredDataPojo.getC060AccAmnt()==0 ){

            insured.setRenewalPremSumC066(0);

        }else{

            insured.setRenewalPremSumC066(lrInsuredDataPojo.getC060AccAmnt());
        }

        // C070组合规则 保险计划累计保额

//        if ("".equals(lrRiskAmntPojo.get070)
//                || tSSRS.GetText(j, 1) == null
//                || tSSRS.GetText(j, 1).equals("null")) {
//
//            insured.setMaximumGrant(0);
//
//        } else {
//
//            insured.setMaximumGrant(Util
//                    .toDouble(tSSRS.GetText(j, 1)));
//        }
        // C070组合规则 被保险人累计C070份数

//        if ("".equals(tSSRS.GetText(j, 1))
//                || tSSRS.GetText(j, 1) == null
//                || tSSRS.GetText(j, 1).equals("null")) {
//
//            insured.setC070Copies(0);
//
//        } else {
//
//            insured.setC070Copies(Util.toInt(tSSRS
//                    .GetText(j, 1)));
//        }
    }
}
