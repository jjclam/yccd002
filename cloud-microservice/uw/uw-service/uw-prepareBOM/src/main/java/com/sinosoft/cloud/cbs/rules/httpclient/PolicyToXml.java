package com.sinosoft.cloud.cbs.rules.httpclient;

import com.sinosoft.cloud.cbs.rules.bom.*;
import com.sinosoft.utility.ExceptionUtils;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.io.PrintStream;

/**
 * 将Policy对象转换为xml，封装成请求报文
 * @author dingfan
 *
 */
public class PolicyToXml {
	private static Log logger = LogFactory.getLog(HttpRequestClient.class);
	private static XStream xstream = null;
	public static String getXml(Policy policy) {
		if(xstream == null) {
			//报文格式
			xstream = new XStream(new DomDriver());
			//处理对象属性为null、Map类型的情况
			xstream.registerConverter(new NullConverter());
			
			xstream.alias("Policy", Policy.class);
			xstream.alias("Agent", Agent.class);
			xstream.alias("Applicant", Applicant.class);
			xstream.alias("BankSourceInfo", BankSourceInfo.class);
			xstream.alias("Beneficiary", Beneficiary.class);
			xstream.alias("ClientInfo", ClientInfo.class);
			xstream.alias("HistoryRisk", HistoryRisk.class);
			xstream.alias("Insured", Insured.class);
			xstream.alias("InvestAccounts", InvestAccounts.class);
			xstream.alias("NotificationInfo", NotificationInfo.class);
			xstream.alias("Notify", Notify.class);
			xstream.alias("Risk", Risk.class);
			xstream.alias("RiskEMInfo", RiskEMInfo.class);
			xstream.alias("RiskPremiumFactor", RiskPremiumFactor.class);
			xstream.alias("ApplicantNotify", ApplicantNotify.class);
			xstream.alias("InsuredNotify", InsuredNotify.class);
			xstream.alias("AgentNotify", AgentNotify.class);
			xstream.alias("AccidentResponse", AccidentResponse.class);
			xstream.alias("MajorDiseaseCheck", MajorDiseaseCheck.class);
			xstream.addImplicitCollection(Policy.class, "insuredList");
			xstream.addImplicitCollection(Policy.class, "beneficiaryList");
			xstream.addImplicitCollection(Policy.class, "insuredNotifyList");
			xstream.addImplicitCollection(Policy.class, "riskList");
			xstream.addImplicitCollection(Policy.class, "clientInfoList");
		}
		
		//javabean转xml
		String error = null;
		String requestXml = xstream.toXML(policy);
		if(requestXml.indexOf("<Policy>")==-1){
			error = "ODM请求报文转换失败";
			logger.debug("ODM请求报文初始化:"+requestXml);
		}
		if(policy==null){
			error += ",policy对象为null";
			logger.debug("policy对象实例化失败,policy=null");
		}
		//封装报文
		try {
			requestXml = requestXml.substring(requestXml.indexOf("<Policy>"), requestXml.indexOf("</Policy>")+9);
//			requestXml = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:abc=\"http://www.ibm.com/rules/decisionservice/xxxxRuleApp/xxxx_rules\" xmlns:par=\"http://www.ibm.com/rules/decisionservice/xxxxRuleApp/xxxx_rules/param\">"
//					+"<soapenv:Header/><soapenv:Body><abc:xxxx_rulesRequest><abc:DecisionID>"+policy.getPrtno()+"_"+System.currentTimeMillis()+"</abc:DecisionID><par:Policy>"
//					+ requestXml + "</par:Policy></abc:xxxx_rulesRequest></soapenv:Body></soapenv:Envelope>";
			requestXml ="<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:lis=\"http://www.ibm.com/rules/decisionservice/NewLisRuleApp/LisUwRuleSet\" xmlns:par=\"http://www.ibm.com/rules/decisionservice/NewLisRuleApp/LisUwRuleSet/param\">" +
					"<soapenv:Header/><soapenv:Body><lis:LisUwRuleSetRequest><lis:DecisionID>"+policy.getPrtno()+"_"+System.currentTimeMillis()+"</lis:DecisionID><par:Policy>"
                    +requestXml+"</par:Policy></lis:LisUwRuleSetRequest></soapenv:Body></soapenv:Envelope>";

		} catch (Exception e) {
			logger.error(ExceptionUtils.exceptionToString(e));
			e.printStackTrace();
			OutputStream out = new ByteArrayOutputStream();
			PrintStream ps = new PrintStream(out);
			e.printStackTrace(ps);
			requestXml="<error>"+error+"</error><message>"+out.toString()+"</message>";
		}
		return requestXml;
	}
}
