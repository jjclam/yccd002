package com.sinosoft.cloud.cbs.trules.data;

import com.sinosoft.cache.dao.impl.RedisCommonDao;
import com.sinosoft.cloud.cbs.trules.bom.AgentCert;
import com.sinosoft.cloud.cbs.trules.bom.TAgentInfo;
import com.sinosoft.cloud.rest.TradeInfo;
import com.sinosoft.lis.entity.LAQualificationPojo;
import com.sinosoft.lis.entity.LCContPojo;
import com.sinosoft.lis.pubfun.FDate;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * @Author:wangshuliang
 * @Description:
 * @Date:Created in 15:20 2018/6/5
 * @Modified by:
 */
@Component
public class TAgentInfoBL {
    private Log logger = LogFactory.getLog(getClass());
    @Autowired
    RedisCommonDao redisCommonDao;
    /**
     * 准备代理人的信息
     * @param tradeInfo
     * @return
     */
    public TradeInfo getTAgentInfo(TradeInfo tradeInfo){
        FDate fDate = new FDate();
        DateFormat format  = new SimpleDateFormat("yyyy-MM-dd");
        LCContPojo lcContPojo = (LCContPojo) tradeInfo.getData(LCContPojo.class.getName());
        TAgentInfo  tAgentInfo = new TAgentInfo();
        tAgentInfo.setAgentCode(lcContPojo.getAgentCode());
        List<AgentCert> agentCerts = new ArrayList<>();
        List<LAQualificationPojo> laQualificationPojos = redisCommonDao.findByIndexKeyRelaDB(LAQualificationPojo.class,"AgentCode",lcContPojo.getAgentCode());
        if (laQualificationPojos!=null && laQualificationPojos.size()>0){
            for (LAQualificationPojo laQualificationPojo:laQualificationPojos){
                AgentCert agentCert = new AgentCert();
                //证件类型
                agentCert.setCertType(String.valueOf(laQualificationPojo.getIdx()));
                //证件启期
                if (!StringUtils.isEmpty(laQualificationPojo.getValidStart())){
                    agentCert.setCertValidStart(format.format(fDate.getDate(laQualificationPojo.getValidStart())));
                }
                //证件止期
                if (!StringUtils.isEmpty(laQualificationPojo.getValidEnd())){
                    agentCert.setCertValidEnd(format.format(fDate.getDate(laQualificationPojo.getValidEnd())));
                }
                //证件状态
                agentCert.setCertStatus(laQualificationPojo.getState());
                //证件签发日期
                if (!StringUtils.isEmpty(laQualificationPojo.getGrantDate())){
                    agentCert.setCertMakeDate(format.format(fDate.getDate(laQualificationPojo.getGrantDate())));
                }
                //证件年期标志    取laQualification的state1  证件年期标志
                agentCert.setCertYeartermFlg(laQualificationPojo.getState1());
                agentCerts.add(agentCert);
            }
        }
        tAgentInfo.setAgentCerts(agentCerts);
        tradeInfo.addData(TAgentInfo.class.getName(),tAgentInfo);
        return tradeInfo;
    }
}
