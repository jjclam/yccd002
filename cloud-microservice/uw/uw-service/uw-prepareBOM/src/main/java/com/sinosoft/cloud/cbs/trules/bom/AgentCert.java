package com.sinosoft.cloud.cbs.trules.bom;



public class AgentCert {
	//证件类型
	private String certType;
	
	//证件启期
	private String certValidStart;
	
	//证件止期
	private String certValidEnd;
	
	//证件状态
	private String certStatus;
	
	//证件签发日期
	private String certMakeDate;
	
	//证件年期标志
	private String certYeartermFlg;

	public String getCertType() {
		return certType;
	}

	public void setCertType(String certType) {
		this.certType = certType;
	}

	public String getCertValidStart() {
		return certValidStart;
	}

	public void setCertValidStart(String certValidStart) {
		this.certValidStart = certValidStart;
	}

	public String getCertValidEnd() {
		return certValidEnd;
	}

	public void setCertValidEnd(String certValidEnd) {
		this.certValidEnd = certValidEnd;
	}

	public String getCertStatus() {
		return certStatus;
	}

	public void setCertStatus(String certStatus) {
		this.certStatus = certStatus;
	}

	public String getCertMakeDate() {
		return certMakeDate;
	}

	public void setCertMakeDate(String certMakeDate) {
		this.certMakeDate = certMakeDate;
	}

	public String getCertYeartermFlg() {
		return certYeartermFlg;
	}

	public void setCertYeartermFlg(String certYeartermFlg) {
		this.certYeartermFlg = certYeartermFlg;
	}
}
