package com.sinosoft.cloud.cbs.rules.bom;

import java.util.Date;
import java.util.List;

/**
 * 投保单
 * 
 * @author dingfan
 * 
 */
public class Policy {
	/**
	 * 投保单号
	 */
	private String policyNo;

	/**
	 * 投保日期
	 */
	private Date policyDate;

	/**
	 * 投保提示签字日期
	 */
	private Date signDate;

	/**
	 * 生效日期
	 */
	private Date effectiveDate;

	/**
	 * 受理日期
	 */
	private Date acceptDate;

	/**
	 * 销售方式
	 */
	private String sellType;

	/**
	 * 管理机构
	 */
	private String manageOrg;

	/**
	 * 高额件标识
	 */
	private boolean highCostSign;

	/**
	 * 所属机构
	 */
	private String ownerOrg;

	/**
	 * 营业部、营业组
	 */
	private String salesDept;

	/**
	 * 出单方式
	 */
	private String salesType;

	/**
	 * 电子签名
	 */
	private String electronSign;

	/**
	 * 保单形式
	 */
	private String policyType;

	/**
	 * 投保人
	 */
	private Applicant applicant;

	/**
	 * 被保人列表
	 */
	private List insuredList;

	/**
	 * 受益人列表
	 */
	private List beneficiaryList;

	/**
	 * 代理人
	 */
	private Agent agent;

	/**
	 * 险种列表
	 */
	private List riskList;

	/**
	 * 银保通交易信息
	 */
	private BankSourceInfo bankSourceInfo;

	/**
	 * 投连账号
	 */
	private InvestAccounts investAccounts;

	/**
	 * 险种EM值信息
	 */
	private RiskEMInfo riskEMInfo;

	/**
	 * 险种加费系数信息
	 */
	private RiskPremiumFactor riskPremiumFactor;

	/**
	 * 客户信息列表
	 */
	private List clientInfoList;

	/**
	 * 销售渠道
	 */
	private String sellChannel;

	/**
	 * 扫描日期
	 */
	private Date scanDate;

	/**
	 * 扫描员姓名
	 */
	private String scanerName;

	/**
	 * 受理人员姓名
	 */
	private String accepterName;

	/**
	 * 银行网点名称
	 */
	private String bankPointName;

	/**
	 * 银行网点代码
	 */
	private String bankPointCode;

	/**
	 * 柜员代码
	 */
	private String tellerCode;

	/**
	 * 柜员姓名
	 */
	private String tellerName;

	/**
	 * 客户经理姓名
	 */
	private String accManagerName;

	/**
	 * 客户经理代码
	 */
	private String accManagerCode;

	/**
	 * 银行编码
	 */
	private String bankCode;

	/**
	 * 合同号
	 */
	private String contNo;

	/**
	 * 单证细类
	 */
	private String subType;

	/**
	 * 保单申请日期
	 */
	private Date polApplyDate;

	/**
	 * 保单标识
	 */
	private String appFlag;

	/**
	 * 核保状态
	 */
	private String uwFlag;

	/**
	 * 退回对象类型
	 */
	private String backobjType;

	/**
	 * 是否处理完毕
	 */
	private boolean dealoverFlag;

	/**
	 * 单据类型
	 */
	private String code;

	/**
	 * 其它号码类型
	 */
	private String otherNoType;

	/**
	 * 强制人工核保标识
	 */
	private boolean forceuwFlag;

	/**
	 * 问题件类型
	 */
	private String issuetype;

	/**
	 * 豁免险数量
	 */
	private int immunityflagNum;

	/**
	 * 保单印刷号码
	 */
	private String prtno;

	/**
	 * 累计本单应交保费
	 */
	private double sumprem;

	/**
	 * 本单被保人住院津贴
	 */
	private double policyDouble1;

	/**
	 * 预留字段2
	 */
	private double policyDouble2;

	/**
	 * 单证细类Pad
	 */
	private String policyString1;

	/**
	 * 预留字段4
	 */
	private String policyString2;

	/**
	 * 移动展业核保次数
	 * 1101表示预核保
	 * 1102表示正是核保
	 */
	private String mitTime;

	/**
	 * 投保人告知
	 */
	private ApplicantNotify applicantNotify;

	/**
	 * 被保人告知列表
	 */
	private List insuredNotifyList;

	/**
	 * 代理人告知
	 * @return
	 */
	private AgentNotify agentNotify;


	/**
	 * 有人核记录
	 * @return
	 */
	private boolean manCheckFlag;

	/**
	 *有核保师录入的核保函
	 * @return
	 */
	private boolean underWriterLetter;

	/**
	 * 操作员问题件
	 */
	private boolean opIssueLetter;

	/**
	 * 保单存在核保师录入的核保函（未下发的状态）
	 */
	private boolean underWriterLetterNoSend;

	/**
	 * 有核保师录入的核保函且均没有选择返回录入或复核
	 */
	private boolean chekerLetterNotEnte;

	/**
	 * 有核保师录入的核保函且选择了返回录入，且选择人核
	 */
	private boolean chekerLetterBeEnteChek;

	/**
	 * 本保单是续期保单
	 */
	private boolean renewalFlag;

	/**
	 * 父保单号
	 */
	private String beforeContno;

	/**
	 * 父保单生效日
	 */
	private  Date lastPolCvalidate;


	public boolean isManCheckFlag() {
		return manCheckFlag;
	}

	public void setManCheckFlag(boolean manCheckFlag) {
		this.manCheckFlag = manCheckFlag;
	}

	public ApplicantNotify getApplicantNotify() {
		return applicantNotify;
	}

	public void setApplicantNotify(ApplicantNotify applicantNotify) {
		this.applicantNotify = applicantNotify;
	}

	public List getInsuredNotifyList() {
		return insuredNotifyList;
	}

	public void setInsuredNotifyList(List insuredNotifyList) {
		this.insuredNotifyList = insuredNotifyList;
	}

	public AgentNotify getAgentNotify() {
		return agentNotify;
	}

	public void setAgentNotify(AgentNotify agentNotify) {
		this.agentNotify = agentNotify;
	}

	public String getMitTime() {
		return mitTime;
	}

	public void setMitTime(String mitTime) {
		this.mitTime = mitTime;
	}

	public Policy() {
		super();
	}

	public String getPolicyNo() {
		return policyNo;
	}

	public void setPolicyNo(String policyNo) {
		this.policyNo = policyNo;
	}

	public Date getPolicyDate() {
		return policyDate;
	}

	public void setPolicyDate(Date policyDate) {
		this.policyDate = policyDate;
	}

	public Date getSignDate() {
		return signDate;
	}

	public void setSignDate(Date signDate) {
		this.signDate = signDate;
	}

	public Date getEffectiveDate() {
		return effectiveDate;
	}

	public void setEffectiveDate(Date effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

	public Date getAcceptDate() {
		return acceptDate;
	}

	public void setAcceptDate(Date acceptDate) {
		this.acceptDate = acceptDate;
	}

	public String getSellType() {
		return sellType;
	}

	public void setSellType(String sellType) {
		this.sellType = sellType;
	}

	public String getManageOrg() {
		return manageOrg;
	}

	public void setManageOrg(String manageOrg) {
		this.manageOrg = manageOrg;
	}

	public boolean isHighCostSign() {
		return highCostSign;
	}

	public void setHighCostSign(boolean highCostSign) {
		this.highCostSign = highCostSign;
	}

	public String getOwnerOrg() {
		return ownerOrg;
	}

	public void setOwnerOrg(String ownerOrg) {
		this.ownerOrg = ownerOrg;
	}

	public String getSalesDept() {
		return salesDept;
	}

	public void setSalesDept(String salesDept) {
		this.salesDept = salesDept;
	}

	public String getSalesType() {
		return salesType;
	}

	public void setSalesType(String salesType) {
		this.salesType = salesType;
	}

	public String getElectronSign() {
		return electronSign;
	}

	public void setElectronSign(String electronSign) {
		this.electronSign = electronSign;
	}

	public String getPolicyType() {
		return policyType;
	}

	public void setPolicyType(String policyType) {
		this.policyType = policyType;
	}

	public Applicant getApplicant() {
		return applicant;
	}

	public void setApplicant(Applicant applicant) {
		this.applicant = applicant;
	}

	public List getInsuredList() {
		return insuredList;
	}

	public void setInsuredList(List insuredList) {
		this.insuredList = insuredList;
	}

	public List getBeneficiaryList() {
		return beneficiaryList;
	}

	public void setBeneficiaryList(List beneficiaryList) {
		this.beneficiaryList = beneficiaryList;
	}

	public Agent getAgent() {
		return agent;
	}

	public void setAgent(Agent agent) {
		this.agent = agent;
	}

	public List getRiskList() {
		return riskList;
	}

	public void setRiskList(List riskList) {
		this.riskList = riskList;
	}

	public BankSourceInfo getBankSourceInfo() {
		return bankSourceInfo;
	}

	public void setBankSourceInfo(BankSourceInfo bankSourceInfo) {
		this.bankSourceInfo = bankSourceInfo;
	}

	public InvestAccounts getInvestAccounts() {
		return investAccounts;
	}

	public void setInvestAccounts(InvestAccounts investAccounts) {
		this.investAccounts = investAccounts;
	}

	public RiskEMInfo getRiskEMInfo() {
		return riskEMInfo;
	}

	public void setRiskEMInfo(RiskEMInfo riskEMInfo) {
		this.riskEMInfo = riskEMInfo;
	}

	public RiskPremiumFactor getRiskPremiumFactor() {
		return riskPremiumFactor;
	}

	public void setRiskPremiumFactor(RiskPremiumFactor riskPremiumFactor) {
		this.riskPremiumFactor = riskPremiumFactor;
	}

	public List getClientInfoList() {
		return clientInfoList;
	}

	public void setClientInfoList(List clientInfoList) {
		this.clientInfoList = clientInfoList;
	}

	public String getSellChannel() {
		return sellChannel;
	}

	public void setSellChannel(String sellChannel) {
		this.sellChannel = sellChannel;
	}

	public Date getScanDate() {
		return scanDate;
	}

	public void setScanDate(Date scanDate) {
		this.scanDate = scanDate;
	}

	public String getScanerName() {
		return scanerName;
	}

	public void setScanerName(String scanerName) {
		this.scanerName = scanerName;
	}

	public String getAccepterName() {
		return accepterName;
	}

	public void setAccepterName(String accepterName) {
		this.accepterName = accepterName;
	}

	public String getBankPointName() {
		return bankPointName;
	}

	public void setBankPointName(String bankPointName) {
		this.bankPointName = bankPointName;
	}

	public String getBankPointCode() {
		return bankPointCode;
	}

	public void setBankPointCode(String bankPointCode) {
		this.bankPointCode = bankPointCode;
	}

	public String getTellerCode() {
		return tellerCode;
	}

	public void setTellerCode(String tellerCode) {
		this.tellerCode = tellerCode;
	}

	public String getTellerName() {
		return tellerName;
	}

	public void setTellerName(String tellerName) {
		this.tellerName = tellerName;
	}

	public String getAccManagerName() {
		return accManagerName;
	}

	public void setAccManagerName(String accManagerName) {
		this.accManagerName = accManagerName;
	}

	public String getAccManagerCode() {
		return accManagerCode;
	}

	public void setAccManagerCode(String accManagerCode) {
		this.accManagerCode = accManagerCode;
	}

	public String getBankCode() {
		return bankCode;
	}

	public void setBankCode(String bankCode) {
		this.bankCode = bankCode;
	}

	public String getContNo() {
		return contNo;
	}

	public void setContNo(String contNo) {
		this.contNo = contNo;
	}

	public String getAppFlag() {
		return appFlag;
	}

	public void setAppFlag(String appFlag) {
		this.appFlag = appFlag;
	}

	public String getBackobjType() {
		return backobjType;
	}

	public void setBackobjType(String backobjType) {
		this.backobjType = backobjType;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public boolean isDealoverFlag() {
		return dealoverFlag;
	}

	public void setDealoverFlag(boolean dealoverFlag) {
		this.dealoverFlag = dealoverFlag;
	}

	public String getOtherNoType() {
		return otherNoType;
	}

	public void setOtherNoType(String otherNoType) {
		this.otherNoType = otherNoType;
	}

	public String getSubType() {
		return subType;
	}

	public void setSubType(String subType) {
		this.subType = subType;
	}

	public String getUwFlag() {
		return uwFlag;
	}

	public void setUwFlag(String uwFlag) {
		this.uwFlag = uwFlag;
	}

	public boolean isForceuwFlag() {
		return forceuwFlag;
	}

	public void setForceuwFlag(boolean forceuwFlag) {
		this.forceuwFlag = forceuwFlag;
	}

	public Date getPolApplyDate() {
		return polApplyDate;
	}

	public void setPolApplyDate(Date polApplyDate) {
		this.polApplyDate = polApplyDate;
	}

	public String getIssuetype() {
		return issuetype;
	}

	public void setIssuetype(String issuetype) {
		this.issuetype = issuetype;
	}

	public int getImmunityflagNum() {
		return immunityflagNum;
	}

	public void setImmunityflagNum(int immunityflagNum) {
		this.immunityflagNum = immunityflagNum;
	}

	public String getPrtno() {
		return prtno;
	}

	public void setPrtno(String prtno) {
		this.prtno = prtno;
	}

	public double getSumprem() {
		return sumprem;
	}

	public void setSumprem(double sumprem) {
		this.sumprem = sumprem;
	}

	public double getPolicyDouble1() {
		return policyDouble1;
	}

	public void setPolicyDouble1(double policyDouble1) {
		this.policyDouble1 = policyDouble1;
	}

	public double getPolicyDouble2() {
		return policyDouble2;
	}

	public void setPolicyDouble2(double policyDouble2) {
		this.policyDouble2 = policyDouble2;
	}

	public String getPolicyString1() {
		return policyString1;
	}

	public void setPolicyString1(String policyString1) {
		this.policyString1 = policyString1;
	}

	public String getPolicyString2() {
		return policyString2;
	}

	public void setPolicyString2(String policyString2) {
		this.policyString2 = policyString2;
	}

	public boolean isUnderWriterLetter() {
		return underWriterLetter;
	}

	public void setUnderWriterLetter(boolean underWriterLetter) {
		this.underWriterLetter = underWriterLetter;
	}

	public boolean isOpIssueLetter() {
		return opIssueLetter;
	}

	public void setOpIssueLetter(boolean opIssueLetter) {
		this.opIssueLetter = opIssueLetter;
	}

	public boolean isUnderWriterLetterNoSend() {
		return underWriterLetterNoSend;
	}

	public void setUnderWriterLetterNoSend(boolean underWriterLetterNoSend) {
		this.underWriterLetterNoSend = underWriterLetterNoSend;
	}

	public boolean isChekerLetterNotEnte() {
		return chekerLetterNotEnte;
	}

	public void setChekerLetterNotEnte(boolean chekerLetterNotEnte) {
		this.chekerLetterNotEnte = chekerLetterNotEnte;
	}

	public boolean isChekerLetterBeEnteChek() {
		return chekerLetterBeEnteChek;
	}

	public void setChekerLetterBeEnteChek(boolean chekerLetterBeEnteChek) {
		this.chekerLetterBeEnteChek = chekerLetterBeEnteChek;
	}

	public boolean isRenewalFlag() {
		return renewalFlag;
	}

	public void setRenewalFlag(boolean renewalFlag) {
		this.renewalFlag = renewalFlag;
	}

	public String getBeforeContno() {
		return beforeContno;
	}

	public void setBeforeContno(String beforeContno) {
		this.beforeContno = beforeContno;
	}

	public Date getLastPolCvalidate() {
		return lastPolCvalidate;
	}

	public void setLastPolCvalidate(Date lastPolCvalidate) {
		this.lastPolCvalidate = lastPolCvalidate;
	}

	}
