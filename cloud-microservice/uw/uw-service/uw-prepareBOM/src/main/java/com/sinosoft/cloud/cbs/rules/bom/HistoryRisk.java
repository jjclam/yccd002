package com.sinosoft.cloud.cbs.rules.bom;


/**
 * 历史险种信息
 * 
 * @author dingfan
 * 
 */
public class HistoryRisk{
	/**
	 * 客户号
	 */
	private String clientNo;

	/**
	 * 本单客户类型
	 */
	private String clientType;

	/**
	 * 客户险种关系类型
	 */
	private String relRiskType;

	/**
	 * 基本保额
	 */
	private double basicAmount;

	/**
	 * 期交保费
	 */
	private double firstPremium;
	
	/**
	 * 趸交保费
	 */
	private double singlePremium;

	/**
	 * 险种代码
	 */
	private String riskCode;
	
	/**
	 * 险种名称
	 */
	private String riskName;

	/**
	 * 份数
	 */
	private double quantity;

	/**
	 * 风险保额
	 */
	private double riskAmount;

	/**
	 * 保单号（历史保单号）
	 */
	private String policyNo;

	public HistoryRisk() {
		super();
	}

	public String getClientNo() {
		return clientNo;
	}

	public void setClientNo(String clientNo) {
		this.clientNo = clientNo;
	}

	public String getClientType() {
		return clientType;
	}

	public void setClientType(String clientType) {
		this.clientType = clientType;
	}

	public String getRelRiskType() {
		return relRiskType;
	}

	public void setRelRiskType(String relRiskType) {
		this.relRiskType = relRiskType;
	}

	public double getBasicAmount() {
		return basicAmount;
	}

	public void setBasicAmount(double basicAmount) {
		this.basicAmount = basicAmount;
	}

	public double getFirstPremium() {
		return firstPremium;
	}

	public void setFirstPremium(double firstPremium) {
		this.firstPremium = firstPremium;
	}

	public double getSinglePremium() {
		return singlePremium;
	}

	public void setSinglePremium(double singlePremium) {
		this.singlePremium = singlePremium;
	}

	public String getRiskCode() {
		return riskCode;
	}

	public void setRiskCode(String riskCode) {
		this.riskCode = riskCode;
	}

	public String getRiskName() {
		return riskName;
	}

	public void setRiskName(String riskName) {
		this.riskName = riskName;
	}

	public double getQuantity() {
		return quantity;
	}

	public void setQuantity(double quantity) {
		this.quantity = quantity;
	}

	public double getRiskAmount() {
		return riskAmount;
	}

	public void setRiskAmount(double riskAmount) {
		this.riskAmount = riskAmount;
	}

	public String getPolicyNo() {
		return policyNo;
	}

	public void setPolicyNo(String policyNo) {
		this.policyNo = policyNo;
	}
}
