package com.sinosoft.cloud.cbs.trules.data;

import org.springframework.stereotype.Component;

import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.io.PrintStream;

/**
 * @Author:wangshuliang
 * @Description:
 * @Date:Created in 9:54 2018/6/12
 * @Modified by:
 */
@Component
public class TErrorCatch {

    /**
     * 原始响应报文
     *
     * @return
     */
    private String getResponseXml() {
        StringBuffer responseXml = new StringBuffer();
        responseXml.append("<Result xmlns=\"\"><contno>");
        responseXml.append("contnotag");
        responseXml.append("</contno><flag>false</flag><message>");
        responseXml.append("errorMessagetag");
        responseXml.append("</message>");
        responseXml.append("<uwresultList><uwresult><flag>false</flag><returnInfo>returnInfotab</returnInfo><riskCode>000000</riskCode><ruleCode>ruleCodetag</ruleCode></uwresult></uwresultList></Result>");
        return responseXml.toString();
    }

    /**
     * 原始请求报文
     * @return
     */
    private String getRequestXml() {
        String requestXml = "<Policy><contno>contnotab</contno><message>errorMessagetab</message></Policy>";
        return requestXml;
    }

    /**
     * 获得错误响应报文2
     *
     * @param contno
     * @param errorMessage
     * @param ruleCode
     * @param returnInfo
     * @return
     */
    public String getErrorResponse2(String contno, String errorMessage, String ruleCode, String returnInfo) {
        String responseXml = getResponseXml();
        responseXml = responseXml.replace("contnotag", contno);
        responseXml = responseXml.replace("errorMessagetag", errorMessage);
        responseXml = responseXml.replace("returnInfotab", returnInfo);
        responseXml = responseXml.replace("ruleCodetag", ruleCode);
        return responseXml;
    }

    /**
     * 获得错误请求报文2
     * @param contno
     * @param message
     * @param error
     * @return
     */
    public String getErrorRequest2(String contno, String message, String error) {
        String errorMessage = message + " " + error;
        String requestXml = getRequestXml();
        requestXml = requestXml.replace("contnotag", contno);
        requestXml = requestXml.replace("errorMessagetab", errorMessage);
        return requestXml;
    }

    /**
     * 获得错误响应报文
     * @param contno
     * @param ruleCode
     * @param returnInfo
     * @return
     */
    public String getErrorResponse(String contno,Exception e ,String ruleCode,String returnInfo) {
        String errorMessage = getErrorInfo(e);
        String responseXml = getResponseXml();
        responseXml = responseXml.replace("contnotag", contno);
        responseXml = responseXml.replace("errorMessagetag",errorMessage);
        responseXml = responseXml.replace("returnInfotab",returnInfo);
        responseXml = responseXml.replace("ruleCodetag",ruleCode);
        return responseXml;
    }

    /**
     * 获得报错信息String
     * @param e
     * @return
     */
    private String getErrorInfo(Exception e) {
        e.printStackTrace();
        OutputStream out = new ByteArrayOutputStream();
        PrintStream ps = new PrintStream(out);
        e.printStackTrace(ps);
        return out.toString();
    }

    /**
     * 获得错误请求报文
     * @param contno
     * @return
     */
    public String getErrorRequest(String contno ,Exception e) {
        String errorMessage = getErrorInfo(e);
        String requestXml = getRequestXml();
        requestXml = requestXml.replace("contnotag", contno);
        requestXml = requestXml.replace("errorMessagetab", errorMessage);
        return requestXml;
    }

}
