package com.sinosoft.cloud.cbs.rules.data;

import com.sinosoft.cache.dao.impl.RedisCommonDao;
import com.sinosoft.cloud.cbs.rules.bom.*;
import com.sinosoft.cloud.rest.TradeInfo;
import com.sinosoft.lis.entity.LMRiskParamsDefPojo;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * 被保人告知赋值
 *
 * @project: abc-cloud-microservice
 * @author: xiaozehua
 * @date: Created in 下午 4:02 2018/5/22 0022
 * @Description:
 */
@Service
public class ExtractNotifyInsured {
    private final Log logger = LogFactory.getLog(this.getClass());
    @Autowired
    RedisCommonDao redisCommonDao;

    public void getInsuredNotify(Policy policy, TradeInfo requestInfo) {

        logger.debug("被保人告知提数开始");
        long beginTime = System.currentTimeMillis();

        getInsurNotify(policy, requestInfo);

        logger.debug("被保人告知提数：" + (System.currentTimeMillis() - beginTime) / 1000.0
                + "s");
    }


    public void getInsurNotify(Policy policy, TradeInfo requestInfo) {

        //告知提数对象
        ExtractDisclose eDisclose = new ExtractDisclose();

//		被保人告知列表
        List insurNotifylist = policy.getInsuredNotifyList();

        // 被保人列表
        List insuredInfoList = policy.getInsuredList();
        //险种列表
        List<Risk> risks = policy.getRiskList();
        //豁免险标记
        boolean immunityflag = false;
        for (int i = 0; i < risks.size(); i++) {
            LMRiskParamsDefPojo lmRiskParamsDefPojo = redisCommonDao.getEntityRelaDB(LMRiskParamsDefPojo.class, risks.get(i).getRiskCode() + "|||" + "immunityflag");
            if (lmRiskParamsDefPojo != null && "immunityflag".equals(lmRiskParamsDefPojo.getParamsType())) {
                immunityflag = true;
                break;
            }
        }
        if (insuredInfoList != null && insuredInfoList.size() > 0) {
//            for (Iterator iter = insuredInfoList.iterator(); iter.hasNext();) {
            for (int i = 0; i < insuredInfoList.size(); i++) {
                //被保人告知对象
                InsuredNotify insurNotify = new InsuredNotify();
                //               Insured insured = (Insured) iter.next();
                Insured insured = (Insured) insuredInfoList.get(i);
                String clientNo = insured.getClientNo();

                NotificationInfo insurNotiInfo = new NotificationInfo();

                insurNotiInfo = eDisclose.getDiscloseExtract(policy, clientNo, "1", requestInfo, immunityflag);
                //被保人告知对应客户号
                insurNotify.setClientNo(clientNo);
                Map map = insurNotiInfo.getNotifyMap();
                if (map != null) {
                    Set keys = map.keySet();
                    Iterator it = keys.iterator();

                    while (it.hasNext()) {
                        String key = (String) it.next();

                        /**
                         * 负债超过200万元	GX09_7_YesOrNo
                         */
                        if (key.equals("GX09_7_YesOrNo")) {
                            insurNotify.setDebtsMorthan200(Util.toBoolean(((Notify) map.get("GX09_7_YesOrNo")).getContents()));
                        }
                        /**
                         * 被保人患智能障碍、恶性肿瘤等或从事所列职业	GX10_0_YesOrNo
                         */
                        if (key.equals("GX10_0_YesOrNo")) {
                            insurNotify.setHasGrediseaseOrjob(Util.toBoolean(((Notify) map.get("GX10_0_YesOrNo")).getContents()));
                        }

                        /**
                         * 患有告知书所列出的病症	GX10_23_YesOrNo
                         */
                        if (key.equals("GX10_23_YesOrNo")) {
                            insurNotify.setHasListDisease(Util.toBoolean(((Notify) map.get("GX10_23_YesOrNo")).getContents()));
                        }

                        /**
                         * 过去两年出现告知症状或未诊断明确	GX10_24_YesOrNo
                         */
                        if (key.equals("GX10_24_YesOrNo")) {
                            insurNotify.setHasLisDiseLastTwoYear(Util.toBoolean(((Notify) map.get("hasLisDiseLastTwoYear")).getContents()));
                        }

                        /**
                         * 过去一年因所列疾病持续病休时间超过14天	GX10_25_YesOrNo
                         */
                        if (key.equals("GX10_25_YesOrNo")) {
                            insurNotify.setLeaveJobByDise14D(Util.toBoolean(((Notify) map.get("GX10_25_YesOrNo")).getContents()));
                        }

                        /**
                         * 出生身长	GX12_20A_Height
                         */
                        if (key.equals("GX12_20A_Height")) {
                            insurNotify.setBornHeight(Util.toDouble(((Notify) map.get("GX12_20A_Height")).getContents()));
                        }

                        /**
                         * 出生孕周	GX12_20A_Week
                         */
                        if (key.equals("GX12_20A_Week")) {
                            insurNotify.setGestationalWeeks(Util.toDouble(((Notify) map.get("GX12_20A_Week")).getContents()));
                        }
                        /**
                         * 出生体重	GX12_20A_Weight
                         */
                        if (key.equals("GX12_20A_Weight")) {
                            insurNotify.setBornWeight(Util.toDouble(((Notify) map.get("GX12_20A_Weight")).getContents()));
                        }
                        /**
                         * 未成年人除本投保申请外已经购买或正在申请人寿或其他保险公司的人身保险	01_002_YesOrNo
                         */
                        if (key.equals("01_002_YesOrNo")) {
                            insurNotify.setChildHasOtherComGoods(Util.toBoolean(((Notify) map.get("01_002_YesOrNo")).getContents()));
                        }
                        /**
                         * 儿童（2周岁以下）补充告知体重	02_011A_weight
                         */
                        if (key.equals("02_011A_weight")) {
                            insurNotify.setChildSupWeight(Util.toDouble(((Notify) map.get("02_011A_weight")).getContents()));
                        }
                        /**
                         * 儿童（2周岁以下）补充告知身高	02_011A_Height
                         */
                        if (key.equals("02_011A_Height")) {
                            insurNotify.setChildSupHeight(Util.toDouble(((Notify) map.get("02_011A_Height")).getContents()));
                        }
                        /**
                         * 过去一年拥有告知单所列症状	GX10_13_YesOrNo
                         */
                        if (key.equals("GX10_13_YesOrNo")) {
                            insurNotify.setHaveFollowing(Util.toBoolean(((Notify) map.get("GX10_13_YesOrNo")).getContents()));
                        }

                        /**
                         * 目前或曾经患有过经系统及精神疾病	GX10_17_YesOrNo
                         */
                        if (key.equals("GX10_17_YesOrNo")) {
                            insurNotify.setHasSystemDisease(Util.toBoolean(((Notify) map.get("GX10_17_YesOrNo")).getContents()));
                        }
                        /**
                         * 正在住院或在过去两年内因疾病住院治疗10天以上或因意外住院治疗30天以上	GX10_26_YesOrNo
                         */
                        if (key.equals("GX10_26_YesOrNo")) {
                            insurNotify.setLivedHospital2Y(Util.toBoolean(((Notify) map.get("GX10_26_YesOrNo")).getContents()));
                        }
                        /**
                         * 在其他公司投保人身险时曾被拒保延期或申请过重大疾病或意外伤残保险金理赔	GX10_27_YesOrNo
                         */
                        if (key.equals("GX10_27_YesOrNo")) {
                            insurNotify.setDisInsureInOtherCom(Util.toBoolean(((Notify) map.get("GX10_27_YesOrNo")).getContents()));
                        }
                        /**
                         * 有身体残疾或功能障碍	GX10_3_YesOrNo
                         */
                        if (key.equals("GX10_3_YesOrNo")) {
                            insurNotify.setHasBodyDis(Util.toBoolean(((Notify) map.get("GX10_3_YesOrNo")).getContents()));
                        }
                        /**
                         * 客户或客户的配偶为艾滋病病毒感染者或准备接受艾滋病病毒检测	GX10_8_YesOrNo
                         */
                        if (key.equals("GX10_8_YesOrNo")) {
                            insurNotify.setHaveAIDS(Util.toBoolean(((Notify) map.get("GX10_8_YesOrNo")).getContents()));
                        }
                        /**
                         * A1智能障碍失明聋且哑一肢以上肢体缺失或功能丧失	GX10_A_YesOrNo
                         */
                        if (key.equals("GX10_A_YesOrNo")) {
                            insurNotify.setMoreThanOneDisability(Util.toBoolean(((Notify) map.get("GX10_A_YesOrNo")).getContents()));
                        }
                        /**
                         * 是B1类告知	GX10_B_YesOrNo
                         */
                        if (key.equals("GX10_B_YesOrNo")) {
                            insurNotify.setB1Inform(Util.toBoolean(((Notify) map.get("GX10_B_YesOrNo")).getContents()));
                        }
                        /**
                         * 早产过期产难产或其他先天疾病	GX12_20B_YesOrNo
                         */
                        if (key.equals("GX12_20B_YesOrNo")) {
                            insurNotify.setAbnormalityOfChildren(Util.toBoolean(((Notify) map.get("GX12_20B_YesOrNo")).getContents()));
                        }
                        /**
                         * (个人人身保险投保书)有备注	GX13_22_Remark
                         */
                        if (key.equals("GX13_22_Remark")) {
//				    	  insurNotify.setBe22Remarks(Util.toBoolean(((Notify)map.get("GX13_22_Remark")).getContents()));

                            boolean flag = true;
                            String str = ((Notify) map.get("GX13_22_Remark")).getContents();
                            if (str.equals("*")) {
                                flag = false;
                            } else if (str.equals("无")) {
                                flag = false;
                            } else if (str.equals("")) {
                                flag = false;
                            }
                            insurNotify.setBe22Remarks(flag);

                        }
                        /**
                         * (银行代理保险投保书)有备注	GX13_C_Remark
                         */
                        if (key.equals("GX13_C_Remark")) {
//				    	  insurNotify.setcRemarks(Util.toBoolean(((Notify)map.get("GX13_C_Remark")).getContents()));
                            boolean flag2 = true;
                            String str2 = ((Notify) map.get("GX13_C_Remark")).getContents();
                            if (str2.equals("*")) {
                                flag2 = false;
                            } else if (str2.equals("无")) {
                                flag2 = false;
                            } else if (str2.equals("")) {
                                flag2 = false;
                            }
                            insurNotify.setcRemarks(flag2);
                        }


                        if (key.equals("01_001_AnnualIncome")) {
                            //年收入(万元)可转换为数值
                            insurNotify.setAnnualIncome((String) (((Notify) map.get("01_001_AnnualIncome")).getContents()));
                        }
                        if (key.equals("GX1_2_YesOrNo")) {
                            //除本投保申请外已经购买或目前正在申请除人寿外的其他保险公司的人身保险
                            insurNotify.setHasOtherComGoods(Util.toBoolean(((Notify) map.get("GX1_2_YesOrNo")).getContents()));
                        }
                        if (key.equals("GX10_12_YesOrNo")) {
                            //目前或曾经服用过成瘾药物
                            insurNotify.setAddictedDrugs(Util.toBoolean(((Notify) map.get("GX10_12_YesOrNo")).getContents()));
                        }
                        if (key.equals("GX10_14_YesOrNo")) {
                            //(个人人身保险投保书)过去一年去过医院就诊服药手术或其他治疗
                            insurNotify.setGoToTheHospital(Util.toBoolean(((Notify) map.get("GX10_14_YesOrNo")).getContents()));
                        }
                        if (key.equals("GX10_15_YesOrNo")) {
                            //(个人人身保险投保书)过去三年有医院检查（包括健康体检）结果为异常
                            insurNotify.setLisUnusualDiagnose3Y(Util.toBoolean(((Notify) map.get("GX10_15_YesOrNo")).getContents()));
                        }
                        if (key.equals("GX10_16_YesOrNo")) {
                            //过去五年曾住院检查或治疗
                            insurNotify.setLivedHospital5Y(Util.toBoolean(((Notify) map.get("GX10_16_YesOrNo")).getContents()));
                        }
                        if (key.equals("GX10_7_YesOrNo")) {
                            //(银行代理保险投保书)目前或曾经患有告知列疾病症状或接受任何治疗
                            insurNotify.setYdHasThisOfDisease(Util.toBoolean(((Notify) map.get("GX10_7_YesOrNo")).getContents()));
                        }
                        if (key.equals("GX13_10_Remark")) {
                            //(银行代理保险投保书)备注栏有内容
                            boolean flag3 = true;
                            String str3 = ((Notify) map.get("GX13_10_Remark")).getContents();
                            if (str3.equals("*")) {
                                flag3 = false;
                            } else if (str3.equals("无")) {
                                flag3 = false;
                            } else if (str3.equals("")) {
                                flag3 = false;
                            }
                            insurNotify.setNoteColumn(flag3);
                        }
                        if (key.equals("GX2_3_YesOrNo")) {
                            //(个人人身保险投保书)有异常投保经历
                            insurNotify.setLisUnusualPolicyPast(Util.toBoolean(((Notify) map.get("GX2_3_YesOrNo")).getContents()));
                        }
                        if (key.equals("GX3_4_YesOrNo")) {
                            //(个人人身保险投保书)有危险运动爱好
                            insurNotify.setLisdangerSportHobby(Util.toBoolean(((Notify) map.get("GX3_4_YesOrNo")).getContents()));
                        }
                        if (key.equals("GX4_5_YesOrNo")) {
                            //1年内计划出国
                            insurNotify.setPlanGoAbroad1Y(Util.toBoolean(((Notify) map.get("GX4_5_YesOrNo")).getContents()));
                        }
                        if (key.equals("GX5_6_YesOrNo")) {
                            //为职业司机或拥有摩托车驾照
                            insurNotify.setProfessionalDriver(Util.toBoolean(((Notify) map.get("GX5_6_YesOrNo")).getContents()));
                        }
                        if (key.equals("GX6_09_HowMany")) {
                            //每天抽烟量（支）
                            insurNotify.setSmokeFew(Util.toDouble(((Notify) map.get("GX6_09_HowMany")).getContents()));
                        }
                        if (key.equals("GX6_10_HowMany")) {
                            //每天抽烟量（支）
                            insurNotify.setSmokeFew(Util.toDouble(((Notify) map.get("GX6_10_HowMany")).getContents()));
                        }
                        if (key.equals("GX6_09_SmokeYear")) {
                            //烟龄
                            insurNotify.setLengthAsSmoker(Util.toDouble(((Notify) map.get("GX6_09_SmokeYear")).getContents()));
                        }
                        if (key.equals("GX6_10_SmokeYear")) {
                            //烟龄
                            insurNotify.setLengthAsSmoker(Util.toDouble(((Notify) map.get("GX6_10_SmokeYear")).getContents()));
                        }
                        if (key.equals("GX6_09_YesOrNo")) {
                            //有吸烟史
                            insurNotify.setHistoryOfSmoking(Util.toBoolean(((Notify) map.get("GX6_09_YesOrNo")).getContents()));
                        }
                        if (key.equals("GX6_10_YesOrNo")) {
                            //有吸烟史
                            insurNotify.setHistoryOfSmoking(Util.toBoolean(((Notify) map.get("GX6_10_YesOrNo")).getContents()));
                        }
                        if (key.equals("GX7_11_YesOrNo")) {
                            //目前或曾经有饮白酒、洋酒等烈性酒的习惯
                            insurNotify.setHistoryOfDrinking(Util.toBoolean(((Notify) map.get("GX7_11_YesOrNo")).getContents()));
                        }
                        if (key.equals("GX8_18_YesOrNo")) {
                            //有智能障碍或失明聋哑及言语咀嚼障碍
                            insurNotify.setSufferMentalDisorders(Util.toBoolean(((Notify) map.get("GX8_18_YesOrNo")).getContents()));
                        }
                        if (key.equals("GX9_21_YesOrNo")) {
                            //父母子女兄弟姐妹患有癌症心脑血管疾病白血病血友病糖尿病多囊肝多囊肾肠息肉或其他遗传性疾病等
                            insurNotify.setFamilyHistoryOfDisease(Util.toBoolean(((Notify) map.get("GX9_21_YesOrNo")).getContents()));
                        }
                        if (key.equals("GX10_4_YesOrNo")) {
                            //(银行代理保险投保书)过去一年去过医院就诊、服药、手术或其他治疗
                            insurNotify.setYdGoHospitalLastYear(Util.toBoolean(((Notify) map.get("GX10_4_YesOrNo")).getContents()));
                        }
                        if (key.equals("GX10_5_YesOrNo")) {
                            //(银行代理保险投保书)过去三年有医院检查（包括健康体检）结果为异常
                            insurNotify.setYdUnusualDiagnose3Y(Util.toBoolean(((Notify) map.get("GX10_5_YesOrNo")).getContents()));
                        }
                        if (key.equals("GX10_6_YesOrNo")) {
                            //(银行代理保险投保书)曾住院检查或治疗
                            insurNotify.setYdLivedHospitalPast(Util.toBoolean(((Notify) map.get("GX10_6_YesOrNo")).getContents()));
                        }
                        if (key.equals("GX11_18C_YesOrNo")) {
                            //曾被建议重复宫颈涂片检查或乳房超声、X光、活检等
                            insurNotify.setGynecologialed(Util.toBoolean(((Notify) map.get("GX11_18C_YesOrNo")).getContents()));
                        }
                        if (key.equals("GX11_19B_YesOrNo")) {
                            //目前或曾经有阴道异常出血畸形瘤葡萄胎子宫肌瘤卵巢囊肿或乳房肿块等	GX11_19B_YesOrNo
                            insurNotify.setGynecologicDiseases(Util.toBoolean(((Notify) map.get("GX11_19B_YesOrNo")).getContents()));
                        }
                        if (key.equals("GX13_21_Remark")) {
                            //健康告知备注
                            boolean flag4 = true;
                            String str4 = ((Notify) map.get("GX13_21_Remark")).getContents();
                            if (str4.equals("*")) {
                                flag4 = false;
                            } else if (str4.equals("无")) {
                                flag4 = false;
                            } else if (str4.equals("")) {
                                flag4 = false;
                            }
                            insurNotify.setHelthRemark(flag4);

                        }
                        if (key.equals("GX2_1_YesOrNo")) {
                            //(银行代理保险投保书)有异常投保经历
                            insurNotify.setYdUnusualPolicyPast(Util.toBoolean(((Notify) map.get("GX2_1_YesOrNo")).getContents()));
                        }
                        if (key.equals("GX3_2_YesOrNo")) {
                            /**
                             * (银行代理保险投保书)有危险运动爱好-GX3_2_YesOrNo
                             */

                            insurNotify.setYbdangerSportHobby(Util.toBoolean(((Notify) map.get("GX3_2_YesOrNo")).getContents()));
                        }

                        if (key.equals("GX13_28_Remark ")) {
                            //其他单证（19061）-备注栏有内容
                            boolean flag5 = true;
                            String str4 = ((Notify) map.get("GX13_28_Remark ")).getContents();
                            if (str4.equals("*")) {
                                flag5 = false;
                            } else if (str4.equals("无")) {
                                flag5 = false;
                            } else if (str4.equals("")) {
                                flag5 = false;
                            }
                            insurNotify.setOthCardRemark(flag5);

                        }


                    }
                }
                insurNotifylist.add(insurNotify);
            }
        }

    }

}