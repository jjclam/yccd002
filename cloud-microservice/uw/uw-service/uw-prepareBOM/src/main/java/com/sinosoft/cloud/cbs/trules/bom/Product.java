package com.sinosoft.cloud.cbs.trules.bom;


import java.util.List;

/**
 * 产品信息
 *
 */
public class Product {
	
	/**
	 * 责任信息
	 */
	private List<Duty> duties;
	
	/**
	 * 险种信息
	 */
	private List<Risk> risks;
	
	
	/**
	 * 折扣前总保费
	 */
	private double saleSumPrem;
	
	/**
	 * 总保费
	 */
	private double sumPrem;
	
	/**
	 * 总保费大写
	 */
	private String sumPremCapital;

	/**
	 * 保险计划编码
	 */
	private String contPlanCode;
	
	/**
	 * 保障计划存在出租车意外伤害责任
	 */
	private boolean ex681007DutyFlag;
	
	/**
	 * 出租车意外伤害责任保额
	 */
	private double plan681007Amnt;
	
	/**
	 * 存在自驾车意外伤害责任
	 */
	private boolean ex681001DutyFlag;
	
	/**
	 * 自驾车意外伤害责任保额(计划)
	 */
	private double planCarAmnt;
	
	/**
	 * 存在公交车、出租车、长途汽车责任
	 */
	private boolean exTrafficDutyFlag;
	
	/**
	 * 长途汽车意外伤害责任保额
	 */
	private double plan681006Amnt;
	
	/**
	 * 市内公交车意外伤害责任保额
	 */
	private double plan681002Amnt;
	
	/**
	 * 存在火车、地铁、轮船、飞机意外风险责任
	 */
	private boolean planTraficDutyFlag;
	
	/**
	 * 火车、地铁意外伤害保额
	 */
	private double plan681003Amnt;
	
	/**
	 * 轮船、摆渡意外伤害责任保额
	 */
	private double plan681004Amnt;
	
	/**
	 * 航班飞机意外伤害责任保额
	 */
	private double plan681005Amnt;


	/**
	 * 金穗吉祥6808意外伤害保险保额
	 */
	private double plan6808Amnt;


	public double getPlan6808Amnt() {
		return plan6808Amnt;
	}

	public void setPlan6808Amnt(double plan6808Amnt) {
		this.plan6808Amnt = plan6808Amnt;
	}

	public List<Duty> getDuties() {
		return duties;
	}

	public void setDuties(List<Duty> duties) {
		this.duties = duties;
	}

	public List<Risk> getRisks() {
		return risks;
	}

	public void setRisks(List<Risk> risks) {
		this.risks = risks;
	}

	public boolean isEx681001DutyFlag() {
		return ex681001DutyFlag;
	}

	public double getSaleSumPrem() {
		return saleSumPrem;
	}

	public void setSaleSumPrem(double saleSumPrem) {
		this.saleSumPrem = saleSumPrem;
	}

	public double getSumPrem() {
		return sumPrem;
	}

	public void setSumPrem(double sumPrem) {
		this.sumPrem = sumPrem;
	}

	public String getSumPremCapital() {
		return sumPremCapital;
	}

	public void setSumPremCapital(String sumPremCapital) {
		this.sumPremCapital = sumPremCapital;
	}
	public String getContPlanCode() {
		return contPlanCode;
	}
	public void setContPlanCode(String contPlanCode) {
		this.contPlanCode = contPlanCode;
	}
	public boolean isEx681007DutyFlag() {
		return ex681007DutyFlag;
	}
	public void setEx681007DutyFlag(boolean ex681007DutyFlag) {
		this.ex681007DutyFlag = ex681007DutyFlag;
	}
	public double getPlan681007Amnt() {
		return plan681007Amnt;
	}
	public void setPlan681007Amnt(double plan681007Amnt) {
		this.plan681007Amnt = plan681007Amnt;
	}
	public boolean getEx681001DutyFlag() {
		return ex681001DutyFlag;
	}
	public void setEx681001DutyFlag(boolean ex681001DutyFlag) {
		this.ex681001DutyFlag = ex681001DutyFlag;
	}
	public double getPlanCarAmnt() {
		return planCarAmnt;
	}
	public void setPlanCarAmnt(double planCarAmnt) {
		this.planCarAmnt = planCarAmnt;
	}
	public boolean isExTrafficDutyFlag() {
		return exTrafficDutyFlag;
	}
	public void setExTrafficDutyFlag(boolean exTrafficDutyFlag) {
		this.exTrafficDutyFlag = exTrafficDutyFlag;
	}
	public double getPlan681006Amnt() {
		return plan681006Amnt;
	}
	public void setPlan681006Amnt(double plan681006Amnt) {
		this.plan681006Amnt = plan681006Amnt;
	}
	public double getPlan681002Amnt() {
		return plan681002Amnt;
	}
	public void setPlan681002Amnt(double plan681002Amnt) {
		this.plan681002Amnt = plan681002Amnt;
	}
	public boolean isPlanTraficDutyFlag() {
		return planTraficDutyFlag;
	}
	public void setPlanTraficDutyFlag(boolean planTraficDutyFlag) {
		this.planTraficDutyFlag = planTraficDutyFlag;
	}
	public double getPlan681003Amnt() {
		return plan681003Amnt;
	}
	public void setPlan681003Amnt(double plan681003Amnt) {
		this.plan681003Amnt = plan681003Amnt;
	}
	public double getPlan681004Amnt() {
		return plan681004Amnt;
	}
	public void setPlan681004Amnt(double plan681004Amnt) {
		this.plan681004Amnt = plan681004Amnt;
	}
	public double getPlan681005Amnt() {
		return plan681005Amnt;
	}
	public void setPlan681005Amnt(double plan681005Amnt) {
		this.plan681005Amnt = plan681005Amnt;
	}
}
