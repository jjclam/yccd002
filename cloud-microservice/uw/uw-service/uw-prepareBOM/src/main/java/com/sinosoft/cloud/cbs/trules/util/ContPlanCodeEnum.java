package com.sinosoft.cloud.cbs.trules.util;

/**
 * @Author:wangshuliang
 * @Description:
 * @Date:Created in 17:07 2018/7/4
 * @Modified by:
 */
public enum ContPlanCodeEnum {
    YE("YE","1"),Y6("Y6","2"),YA("YA","3"),YB("YB","4"),
    YC("YC","5"),YD("YD","6"),YG("YG","7"),XJYXA("XJYXA","8"),
    XJYXB("XJYXB","9");


    ContPlanCodeEnum(String contPlanCode, String plan) {
        this.contPlanCode = contPlanCode;
        this.plan = plan;
    }
    //保险计划编码
    private String contPlanCode;
    //转换后的码值
    private String plan;

    public String getContPlanCode() {
        return contPlanCode;
    }

    public void setContPlanCode(String contPlanCode) {
        this.contPlanCode = contPlanCode;
    }

    public String getPlan() {
        return plan;
    }

    public void setPlan(String plan) {
        this.plan = plan;
    }

    /**
     * 循环遍历枚举类 取得contplancode的转换值
     * @param contPlanCode 保险计划编码
     * @return
     */
    public static String getValue(String contPlanCode){
        for (ContPlanCodeEnum contPlanCodeEnum : ContPlanCodeEnum.values()){
            if (contPlanCodeEnum.getContPlanCode().equals(contPlanCode)){
                return contPlanCodeEnum.getPlan();
            }
        }
        return "";
    }
}
