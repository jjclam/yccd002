package com.sinosoft.cloud.cbs.rules.data;

import com.sinosoft.cloud.cbs.function.UWFunction;
import com.sinosoft.cloud.cbs.rules.bom.Applicant;
import com.sinosoft.cloud.cbs.rules.bom.ClientInfo;
import com.sinosoft.cloud.cbs.rules.bom.Insured;
import com.sinosoft.cloud.cbs.rules.bom.Policy;
import com.sinosoft.cloud.nb.service.redis.NBRedisCommon;
import com.sinosoft.cloud.rest.TradeInfo;
import com.sinosoft.lis.entity.LCAddressPojo;
import com.sinosoft.lis.entity.LCAppntPojo;
import com.sinosoft.lis.entity.LCBnfPojo;
import com.sinosoft.lis.entity.LCInsuredPojo;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Author: zhuyiming
 * @Description: 提取客户信息数据
 * @Date: Created in 15:45 2017/9/24
 */
@Service
public class ExtractCustInfo {
	private final Log logger = LogFactory.getLog(this.getClass());

	@Autowired
	NBRedisCommon nbRedisCommon;

	@Autowired
	UWFunction uwFunction;
	/**
	 * 获取客户信息
	 * @param policy
	 */
	public void getCustInfo(Policy policy, TradeInfo requestInfo) {
		// 被保人信息列表
		List insuredInfoList = policy.getInsuredList();
		// 投保人信息
		Applicant applicantInfo = policy.getApplicant();
		// 客户信息列表
		List custInfoList = policy.getClientInfoList();
		// 客户号（投保人、被保人）
		String[] custNoList = new String[insuredInfoList.size() + 1];
		custNoList[0] = applicantInfo.getClientNo();
		for (int i = 0; i < insuredInfoList.size(); i++) {
			Insured insured = (Insured) insuredInfoList.get(i);
			custNoList[i + 1] = insured.getClientNo();
		}
		long beginTime = System.currentTimeMillis();
		int flag = 0;
		//客户姓名与恐怖分子、恐怖组织名单或武器禁运名单相同SQL（CONTNO：合同号）
		boolean sameNameTerrorist = false;
		//客户证件类型和号码与恐怖分子、恐怖组织名单或武器禁运名单相同SQL（CONTNO：合同号）
		boolean sameTypeTerrorist = false;

		LCAppntPojo lcAppntPojo = (LCAppntPojo) requestInfo.getData(LCAppntPojo.class.getName());
		List<LCInsuredPojo> lcInsuredPojos = (List) requestInfo.getData(LCInsuredPojo.class.getName());
		List<LCBnfPojo> lcBnfPojos = (List) requestInfo.getData(LCBnfPojo.class.getName());
//		int count = uwFunction.checkSameNameTerr(lcAppntPojo.getAppntName());
//		int count1 = uwFunction.checkSameTypeTerr(lcAppntPojo.getIDNo(), lcAppntPojo.getIDType());
//		for(int i=0;i<lcInsuredPojos.size();i++){
//			count += uwFunction.checkSameNameTerr(lcInsuredPojos.get(i).getName());
//			count1 += uwFunction.checkSameTypeTerr(lcInsuredPojos.get(i).getIDNo(), lcInsuredPojos.get(i).getIDType());
//		}
//		if(lcBnfPojos != null && lcBnfPojos.size() > 0){
//			for(int i=0;i<lcBnfPojos.size();i++){
//				count += uwFunction.checkSameNameTerr(lcBnfPojos.get(i).getName());
//				count1 += uwFunction.checkSameTypeTerr(lcBnfPojos.get(i).getIDNo(), lcBnfPojos.get(i).getIDType());
//			}
//		}
//		if(count>0){
//			sameNameTerrorist = true;
//		}
//		if(count1>0){
//			sameTypeTerrorist = true;
//		}

		List<LCAddressPojo> tLCAddressPojoList = (List<LCAddressPojo>) requestInfo.getData(LCAddressPojo.class.getName());
		beginTime = System.currentTimeMillis();
		if (tLCAddressPojoList!= null && tLCAddressPojoList.size()>0 && custNoList != null && custNoList.length > 0) {
			for (int i = 0; i < custNoList.length; i++) {
				ClientInfo custInfo = new ClientInfo();
				for(int j=0;j<tLCAddressPojoList.size();j++){
					if(tLCAddressPojoList.get(j).getCustomerNo().equals(custNoList[i])){
						// 客户号
						custInfo.setClientNo(tLCAddressPojoList.get(j).getCustomerNo());
						// 邮箱地址
						custInfo.setEmail(tLCAddressPojoList.get(j).getEMail());
						// 常住地址
						custInfo.setHomeAddr(tLCAddressPojoList.get(j).getHomeAddress());
						// 常住地址所在市
						custInfo.setHomeCity(tLCAddressPojoList.get(j).getCity());
						// 常住地址邮编
						custInfo.setHomePostcode(tLCAddressPojoList.get(j).getHomeZipCode());
						// 常住地址所在省
						custInfo.setHomeProv(tLCAddressPojoList.get(j).getProvince());
						// 手机号码
						custInfo.setMobilePhone(tLCAddressPojoList.get(j).getMobile());
						// 通讯地址
						custInfo.setPostalAddr(tLCAddressPojoList.get(j).getPostalAddress());
						// 通讯邮编
						custInfo.setPostalPostcode(tLCAddressPojoList.get(j).getZipCode());
						// 单位名称
						custInfo.setOrgName(tLCAddressPojoList.get(j).getGrpName());
						// 联系电话
						custInfo.setPhones(tLCAddressPojoList.get(j).getPhone());
						// 固定电话
						custInfo.setTelPhone(tLCAddressPojoList.get(j).getHomePhone());
						// 常住地址所在区/县
						custInfo.setHomeCounty(tLCAddressPojoList.get(j).getCounty());
						// 常住地址所在村/社区（楼、号）
						custInfo.setHomeCommunity(tLCAddressPojoList.get(j).getStoreNo());
						// 通讯地址所在村/社区（楼、号）
						custInfo.setPostalCommunity(tLCAddressPojoList.get(j).getStoreNo2());
						// 固定电话区号
						custInfo.setZoneCode(tLCAddressPojoList.get(j).getZoneCode());
						// 客户姓名与恐怖分子、恐怖组织名单或武器禁运名单相同
						custInfo.setSameNameTerrorist(sameNameTerrorist);
						// 客户证件类型和号码与恐怖分子、恐怖组织名单或武器禁运名单相同
						custInfo.setSameTypeTerrorist(sameTypeTerrorist);
						custInfoList.add(custInfo);
						break;
					}
				}
			}
		}
		logger.debug("客户信息基础提数：" + (System.currentTimeMillis() - beginTime)
				/ 1000.0 + "s");
	}
}
