package com.sinosoft.cloud.cbs.trules.data;

import com.sinosoft.cloud.cbs.function.UWFunction;
import com.sinosoft.cloud.cbs.trules.bom.TBeneficiaryInfo;
import com.sinosoft.cloud.cbs.trules.bom.TBeneficiaryInfos;
import com.sinosoft.cloud.cbs.trules.util.DomainUtils;
import com.sinosoft.cloud.rest.TradeInfo;
import com.sinosoft.lis.entity.LCBnfPojo;
import com.sinosoft.lis.entity.LDPersonPojo;
import com.sinosoft.lis.pubfun.FDate;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @Author:wangshuliang
 * @Description: 提取受益人信息
 * @Date:Created in 17:29 2018/6/6
 * @Modified by:
 */
@Component
public class TBeneficiaryInfoBL {
    private Log logger = LogFactory.getLog(getClass());
    @Autowired
    UWFunction uwFunction;

    /**
     * 提取受益人信息
     *
     * @param tradeInfo
     * @return
     */
    public TradeInfo getTBeneficiaryInfo(TradeInfo tradeInfo) {
        logger.debug("开始提数受益人信息。。");
        FDate fDate = new FDate();
        List<TBeneficiaryInfo> tBeneficiaryInfoList = new ArrayList<>();
        List<LCBnfPojo> lcBnfPojoList = (List<LCBnfPojo>) tradeInfo.getData(LCBnfPojo.class.getName());
        if (lcBnfPojoList != null && lcBnfPojoList.size() > 0) {
            for (LCBnfPojo lcBnfpojo : lcBnfPojoList) {
                TBeneficiaryInfo tBeneficiaryInfo = new TBeneficiaryInfo();
                //姓名
                tBeneficiaryInfo.setBeneficiaryName(lcBnfpojo.getName());
                //证件类型
                tBeneficiaryInfo.setBeneficiaryCerttype(DomainUtils.convertIDType(lcBnfpojo.getIDType()));
                //证件号
                tBeneficiaryInfo.setBeneficiaryCertno(lcBnfpojo.getIDNo());
                //与被保险人的关系
                tBeneficiaryInfo.setRelaToInsurd(lcBnfpojo.getRelationToInsured());
                //电话
                tBeneficiaryInfo.setContactTel(lcBnfpojo.getTel());
                //受益顺序
                tBeneficiaryInfo.setBeneficiarySn(String.valueOf(lcBnfpojo.getBnfNo()));
                //受益份额
                tBeneficiaryInfo.setBeneficiaryPropor(lcBnfpojo.getBnfLot());
                //受益人地址
                tBeneficiaryInfo.setContactAddr(lcBnfpojo.getAccName());
                //受益类型
                tBeneficiaryInfo.setBeneficiaryType(lcBnfpojo.getBnfType());
                //证件有效期
                tBeneficiaryInfo.setIdValidDate(lcBnfpojo.getIdValiDate());
                //性别
                tBeneficiaryInfo.setBnfSex(DomainUtils.convertSex(lcBnfpojo.getSex()));
                //出生日期
                tBeneficiaryInfo.setBnfBirthday(lcBnfpojo.getBirthday());
                List<LDPersonPojo> ldPersonPojoList = (List<LDPersonPojo>) tradeInfo.getData(LDPersonPojo.class.getName());
                for (LDPersonPojo lDPersonPojo : ldPersonPojoList) {
                    if (lcBnfpojo.getCustomerNo().equals(lDPersonPojo.getCustomerNo())) {
                        //国籍
                        tBeneficiaryInfo.setNationality(lDPersonPojo.getNativePlace());
                        break;
                    }
                }
                //黑名单1-受益人的证件号与黑名单相同，黑名单1-证件号与和黑名单相同,不分国籍,>0触发
                boolean bnfBackIdFlag = uwFunction.checkBlackIdFlag(lcBnfpojo.getIDNo());
                tBeneficiaryInfo.setBnfBlackIdFlag(bnfBackIdFlag);
                if (!bnfBackIdFlag){
                    //黑名单2-中国客户名字、生日与黑名单相同,>0触发,
                    if("CHN".equals(tBeneficiaryInfo.getNationality())){
                        boolean checkBnfBlackFullFlag = uwFunction.checkBlackCHNFlag(lcBnfpojo.getName(), lcBnfpojo.getBirthday());
                        tBeneficiaryInfo.setBnfBlackFullFlag(checkBnfBlackFullFlag);
                    }else {
                        String[] strName=lcBnfpojo.getName().split(" ");
                        int count=0;
                        if (strName.length==1){
                            // 黑名单3-受益人名1部分组成,受益人名与黑名单表四个名字对比,>0触发
                            boolean checkBnfBlackFlag1 = uwFunction.checkBlackFlag1(lcBnfpojo.getName(), null, lcBnfpojo.getBirthday());
                            tBeneficiaryInfo.setBnfBlackFlag1(checkBnfBlackFlag1);
                        }
                        if (strName.length==2){
                            /**黑名单4-受益人名2部分组成,受益人名与黑名单表2个名字对比,>0触发
                             * 有一个name相同，且出生日期相同
                             */
                            boolean checkBnfBlackFlag2 = uwFunction.checkBlackFlag2(lcBnfpojo.getName(), null, lcBnfpojo.getBirthday());
                            tBeneficiaryInfo.setBnfBlackFlag2(checkBnfBlackFlag2);
                            if (!checkBnfBlackFlag2){
                                /**
                                 * 黑名单5-受益人名2部分组成,受益人名与黑名单表2个名字对比,>=2触发
                                 * firstname和surnname都要相同.无生日限制
                                 */
                                count=0;
                                count += uwFunction.checkBlackFlag3_1(strName[0]);
                                count += uwFunction.checkBlackFlag3_3(strName[1]);
                                if(count >= 2){
                                    tBeneficiaryInfo.setBnfBlackFlag3(true);
                                }
                            }

                        }
                        if (strName.length>=3){
                            /** 黑名单6-受益人名>=3部分组成,受益人名与黑名单表3个名字对比,>=0触发,参数:3个contno
                             * firstname,middlename,surname,有生日限制 */
                            boolean checkBnfBlackFlag4 = uwFunction.checkBlackFlag4(lcBnfpojo.getName(), null, lcBnfpojo.getBirthday());
                            tBeneficiaryInfo.setBnfBlackFlag4(checkBnfBlackFlag4);
                            if (!checkBnfBlackFlag4){
                                /** 黑名单7-受益人名>=3部分组成,受益人名与黑名单表3个名字对比,>=2触发,参数:3个contno
                                 * firstname,middlename,surname,无生日限制 */
                                count = 0;
                                String appntName = lcBnfpojo.getName();
                                String surName = appntName.substring(appntName.indexOf(strName[1])+strName[1].length()+1,appntName.length());
                                count += uwFunction.checkBlackFlag3_1(strName[0]);
                                count += uwFunction.checkBlackFlag3_2(strName[1]);
                                count += uwFunction.checkBlackFlag3_3(surName);
                                if(count >= 2){
                                    tBeneficiaryInfo.setBnfBlackFlag5(true);
                                }
                            }
                        }

                    }
                }


                tBeneficiaryInfoList.add(tBeneficiaryInfo);
            }
        }

      /*
        tBeneficiaryInfo.setBnfBirthday(new Date());
        tBeneficiaryInfoList.add(tBeneficiaryInfo);*/
   /*   if (tBeneficiaryInfoList.size()==0){
          TBeneficiaryInfo tBeneficiaryInfo = new TBeneficiaryInfo();
          tBeneficiaryInfo.setBeneficiaryName("借意险");
          tBeneficiaryInfoList.add(tBeneficiaryInfo);
      }*/
        TBeneficiaryInfos tBeneficiaryInfos = new TBeneficiaryInfos();
        tBeneficiaryInfos.settBeneficiaryInfo(tBeneficiaryInfoList);
        tradeInfo.addData(TBeneficiaryInfos.class.getName(), tBeneficiaryInfos);
        logger.debug("完成提数受益人信息。。");
        return tradeInfo;
    }
}
