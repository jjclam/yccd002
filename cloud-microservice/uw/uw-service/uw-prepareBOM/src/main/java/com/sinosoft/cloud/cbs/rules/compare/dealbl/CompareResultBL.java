package com.sinosoft.cloud.cbs.rules.compare.dealbl;


import com.sinosoft.cloud.cbs.rules.bom.Applicant;
import com.sinosoft.cloud.cbs.rules.bom.Insured;
import com.sinosoft.cloud.cbs.rules.bom.Policy;
import com.sinosoft.cloud.cbs.rules.bom.Risk;
import com.sinosoft.cloud.cbs.rules.httpclient.PolicyToXml;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 返回比对后的最终结果的处理类.
 */

@Component
public class CompareResultBL {
    private final Log logger = LogFactory.getLog(this.getClass());
    @Autowired
    ApplicantBL applicantBL;
    @Autowired
    InsuredBL insuredBL;
    @Autowired
    RiskBL riskBL;

    /**
     * 最终结果的放方法.
     *
     * @param oldPolicy 老核心的数据.
     * @param newPolicy 累计风险保到的数据.
     * @return
     */
    public String getXmlCompareResult(Policy oldPolicy, Policy newPolicy) {
        final long start = System.currentTimeMillis();
        logger.debug("比对xml开始");
        String oldXml = "";
        String newXml = "";
        //Applicant的老核心得到的数据
        final Applicant oldApplicant = oldPolicy.getApplicant();
        //Applicant的累计风险保额得到的数据
        final Applicant newApplicant = newPolicy.getApplicant();
//        if (!applicantBL.xmlTwoIsSame(oldApplicant, newApplicant)) {
//            logger.debug("返回的是原来流程所拼装的报文");
//            return PolicyToXml.getXml(oldPolicy);
//        } else {
//            logger.debug("返回的是调用累计风险保额所拼装的报文");
//            xml = PolicyToXml.getXml(newPolicy);
//        }
//
        final List<Insured> oldInsureds = oldPolicy.getInsuredList();
        final List<Insured> newInsureds = newPolicy.getInsuredList();
//        if (!insuredBL.xmlTwoIsSame(oldInsureds, newInsureds)) {
//            logger.debug("返回的是原来流程所拼装的报文");
//            return PolicyToXml.getXml(oldPolicy);
//        } else {
//            xml = PolicyToXml.getXml(newPolicy);
//            logger.debug("返回的是调用累计风险保额所拼装的报文");
//        }
//
        final List<Risk> oldRisks = oldPolicy.getRiskList();
        final List<Risk> newRisks = newPolicy.getRiskList();
//        if (!riskBL.xmlTwoIsSame(oldRisks, newRisks)) {
//            logger.debug("返回的是原来流程所拼装的报文");
//            return PolicyToXml.getXml(oldPolicy);
//        } else {
//            logger.debug("返回的是调用累计风险保额所拼装的报文");
//            xml = PolicyToXml.getXml(newPolicy);
//        }


        String isPass = "false";
        if (applicantBL.xmlTwoIsSame(oldApplicant, newApplicant)) {
            isPass = "true";
        }
        if (insuredBL.xmlTwoIsSame(oldInsureds, newInsureds)) {
            isPass = "true";
        } else {
            isPass = "false";
        }
        if (riskBL.xmlTwoIsSame(oldRisks, newRisks)) {
            isPass = "true";
        } else {
            isPass = "false";
        }
        logger.debug("比对xml结束，总共耗时为：" + (System.currentTimeMillis() - start)/ 1000.0+"s");

        newXml=PolicyToXml.getXml(newPolicy);
        logger.debug("调用累计风险保额所拼装的报文:"+newXml);

        oldXml=PolicyToXml.getXml(oldPolicy);
        logger.debug("调用老核心所拼装的报文:"+oldXml);

        if ("true".equals(isPass)) {
            logger.debug("返回的是调用累计风险保额所拼装的报文");
            return newXml;
        } else {
            logger.debug("返回的是调用老核心所拼装的报文");
            return oldXml;
        }

    }
}