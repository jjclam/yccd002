package com.sinosoft.cloud.cbs.rules.data;
import com.sinosoft.cloud.cbs.function.UWFunction;
import com.sinosoft.cloud.cbs.rules.bom.Beneficiary;
import com.sinosoft.cloud.cbs.rules.bom.Policy;
import com.sinosoft.cloud.nb.service.redis.NBRedisCommon;
import com.sinosoft.cloud.rest.TradeInfo;
import com.sinosoft.lis.entity.LCBnfPojo;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.lis.pubfun.PubFun;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.List;

/**
 * @Author: zhuyiming
 * @Description: 提取受益人数据
 * @Date: Created in 15:23 2017/9/24
 */
@Service
public class ExtractBnfcryInfo {
	private final Log logger = LogFactory.getLog(this.getClass());
	@Autowired
	NBRedisCommon nbRedisCommon;
	@Autowired
	UWFunction uwFunction;
	/**
	 * 获取受益人列表
	 * @param policy
	 * @throws Exception 
	 */
	public void getBnfcryInfoList(Policy policy, TradeInfo requestInfo) throws Exception {
		List bnfcryInfoList = policy.getBeneficiaryList();
		// 受益人基本信息
		List<LCBnfPojo> tLCBnfPojoList = (List) requestInfo.getData(LCBnfPojo.class.getName());

		long beginTime = System.currentTimeMillis();
		FDate fDate = new FDate();
		if (null != tLCBnfPojoList && tLCBnfPojoList.size() > 0) {
			for (int i = 0; i < tLCBnfPojoList.size(); i++) {
				Beneficiary benneficy = new Beneficiary();

				// 受益类别
				benneficy.setBeneficialType(tLCBnfPojoList.get(i).getBnfType());
				// 姓名
				benneficy.setBeneficiaryName(tLCBnfPojoList.get(i).getName());
				// 受益顺序
				benneficy.setBeneficialOrder(Util.toInt(tLCBnfPojoList.get(i).getBnfGrade()));
				// 出生日期
				benneficy.setBirthday(fDate.getDate(tLCBnfPojoList.get(i).getBirthday()));
				// 性别
				benneficy.setSex(tLCBnfPojoList.get(i).getSex());
				// 证件号码
				benneficy.setIdentityCode(tLCBnfPojoList.get(i).getIDNo());
				// 证件类型
				benneficy.setIdentityType(tLCBnfPojoList.get(i).getIDType());
				// 证件有效期
				benneficy.setIdentityValidityTerm(tLCBnfPojoList.get(i).getIdValiDate());
				// 与被保险人关系
				String realtionWithInsured = tLCBnfPojoList.get(i).getRelationToInsured();
				benneficy.setRealtionWithInsured(realtionWithInsured);
				// 联系地址
				benneficy.setHomeAddr(tLCBnfPojoList.get(i).getAddress());
				// 联系电话
				benneficy.setPhone(tLCBnfPojoList.get(i).getTel());
				// 邮编	
				benneficy.setPostalPostcode(tLCBnfPojoList.get(i).getZipCode());
				// 受益人客户号
				benneficy.setClientNo(tLCBnfPojoList.get(i).getCustomerNo());
				// 被保人客户号
				benneficy.setInsuredClientNo(tLCBnfPojoList.get(i).getInsuredNo());
				if (benneficy.getBirthday() != null && !"".equals(benneficy.getBirthday()) && policy.getEffectiveDate() != null && !policy.getEffectiveDate().equals("")) {// 年龄
					benneficy.setAge(PubFun.calInterval(benneficy.getBirthday(),
							policy.getEffectiveDate(), "Y"));
				}
				//受益份额
				benneficy.setBeneficialPercent(tLCBnfPojoList.get(i).getBnfLot());
				benneficy.setPolno(tLCBnfPojoList.get(i).getPolNo());
				logger.debug("受益人基础-提数：" + (System.currentTimeMillis() - beginTime) / 1000.0 + "s");

				beginTime = System.currentTimeMillis();
				//添加其他受益人提数sql
				String custNo = benneficy.getClientNo();
				int field = 0;
				//黑名单1-证件号与和黑名单相同,不分国籍,>0触发,参数:一个contno
				//{this} 的证件号与和黑名单相同
				boolean backIdFlag = uwFunction.checkBlackIdFlag(tLCBnfPojoList.get(i).getIDNo());
				benneficy.setBnfBlackIdFlag(backIdFlag);
				//黑名单2-受益人名1部分组成,受益人名与黑名单表四个名字对比,>0触发,参数:4个contno
				//{this}的名字与黑名单名字和生日相同(名字为一部分)
				SimpleDateFormat sdf =   new SimpleDateFormat( "yyyy-MM-dd" );
				boolean checkBlackFlag1 = uwFunction.checkBlackFlag1(tLCBnfPojoList.get(i).getName(), null,tLCBnfPojoList.get(i).getBirthday());
				benneficy.setBnfBlackFlag1(checkBlackFlag1);
				//黑名单3-受益人名2部分组成,受益人名与黑名单表2个名字对比,>0触发,参数:2个contno
				//有一个name相同,只比较firstname和surname，且出生日期相同
				//{this}的名字与黑名单名字和生日相同(名字为两部分)
				boolean checkBlackFlag2 = uwFunction.checkBlackFlag2(tLCBnfPojoList.get(i).getName(), null, tLCBnfPojoList.get(i).getBirthday());
				benneficy.setBnfBlackFlag2(checkBlackFlag2);
				//黑名单4-受益人名2部分组成,受益人名与黑名单表2个名字对比,>=2触发,参数:2个contno
				//firstname和surnname都要相同.无生日限制
				//的名字与黑名单名字和生日相同(名字为两部分,无生日限制)
				int count = 0;
				if(tLCBnfPojoList.get(i).getName().length() - tLCBnfPojoList.get(i).getName().replace(" ","").length() == 1){
					String appntName = tLCBnfPojoList.get(i).getName();
					String firstName = appntName.substring(0, appntName.indexOf(" "));
					String surName = appntName.substring(appntName.indexOf(" ")+1);
					count += uwFunction.checkBlackFlag3_1(firstName);
					count += uwFunction.checkBlackFlag3_3(surName);
				}
				if(count >= 2){
					benneficy.setBnfBlackFlag3(true);
				}
				//黑名单5-受益人名4部分组成,受益人名与黑名单表3个名字对比,>0触发,参数:3个contno
				//firstname,middlename,surname,有生日限制
				//{this} 的名字与黑名单名字两个以上相同(名字为四部分)
				boolean checkBlackFlag4 = uwFunction.checkBlackFlag4(tLCBnfPojoList.get(i).getName(), null, tLCBnfPojoList.get(i).getBirthday());
				benneficy.setBnfBlackFlag4(checkBlackFlag4);
				//黑名单6-受益人名4部分组成,受益人名与黑名单表3个名字对比,>=2触发,参数:3个contno
				//firstname,middlename,surname,无生日限制
				//{this} 的名字与黑名单相同(受益人名四部分构成无生日限制)
				count = 0;
				if(tLCBnfPojoList.get(i).getName().length() - tLCBnfPojoList.get(i).getName().replace(" ","").length() >=2){
					String appntName = tLCBnfPojoList.get(i).getName();
					String name[] = appntName.split(" ");
					String firstName = name[0];
					String middleName = name[1];
					String surName = appntName.substring(appntName.indexOf(middleName)+middleName.length()+1,appntName.length());
					count += uwFunction.checkBlackFlag3_1(firstName);
					count += uwFunction.checkBlackFlag3_2(middleName);
					count += uwFunction.checkBlackFlag3_3(surName);

				}
				if(count >= 2){
					benneficy.setBnfBlackFlag5(true);
				}
				logger.debug("受益人拓展提数：" + (System.currentTimeMillis() - beginTime)
						/ 1000.0 + "s");
				bnfcryInfoList.add(benneficy);
			}
		}
	}
}
