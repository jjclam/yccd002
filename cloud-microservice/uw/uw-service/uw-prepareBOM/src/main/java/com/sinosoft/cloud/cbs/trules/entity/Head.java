package com.sinosoft.cloud.cbs.trules.entity;

/**
 * @Author:wangshuliang
 * @Description:
 * @Date:Created in 17:34 2018/6/25
 * @Modified by:
 */
public class Head {
    private SysHeader sysHeader;
    private  String domHeader;
    private String bizHeader;

    public SysHeader getSysHeader() {
        return sysHeader;
    }

    public void setSysHeader(SysHeader sysHeader) {
        this.sysHeader = sysHeader;
    }

    public String getDomHeader() {
        return domHeader;
    }

    public void setDomHeader(String domHeader) {
        this.domHeader = domHeader;
    }

    public String getBizHeader() {
        return bizHeader;
    }

    public void setBizHeader(String bizHeader) {
        this.bizHeader = bizHeader;
    }
}
