package com.sinosoft.cloud.cbs.rules.bom;

/**
 * 代理人告知类
 * @project: abc-cloud-microservice
 * @author: xiaozehua
 * @date: Created in 下午 2:48 2018/5/22 0022
 * @Description:
 */
public class AgentNotify {
    /**
     * 客户号
     */
    private String clientNo;
    /**
     * 代理人有备注信息-DLR10_4_Remark
     */
    private boolean	agentRemark         ;
    /**
     * 亲眼见过被保人-DLR2_1-2_YesOrNo
     */
    private boolean	ifMeetInsured  = true;
    /**
     * 被保人身体有缺陷-DLR3_1-3_YesOrNo
     */
    private boolean	insurdViciousBody   ;
    /**
     * 被保险人有危险运动-DLR4_1-4_YesOrNo
     */
    private boolean	insurdangerHobby    ;
    /**
     * 客户证件与原件一致-DLR6_3-1_YesOrNo
     */
    private boolean	cardDisunity= true  ;
    /**
     * 客户购买大额保单与经济状况不符-DLR7_3-2_YesOrNo
     */
    private boolean	bigPolByPoor        ;
    /**
     * 客户购买的保险产品与其需求不符-DLR8_3-3_YesOrNo
     */
    private boolean	goodsNotNeed        ;
    /**
     * 客户投保目的异常-DLR9_3-4_YesOrNo
     */
    private boolean	unusualPurpose      ;



    public boolean isAgentRemark() {
        return agentRemark;
    }
    public void setAgentRemark(boolean agentRemark) {
        this.agentRemark = agentRemark;
    }
    public boolean isIfMeetInsured() {
        return ifMeetInsured;
    }
    public void setIfMeetInsured(boolean ifMeetInsured) {
        this.ifMeetInsured = ifMeetInsured;
    }
    public boolean isInsurdViciousBody() {
        return insurdViciousBody;
    }
    public void setInsurdViciousBody(boolean insurdViciousBody) {
        this.insurdViciousBody = insurdViciousBody;
    }
    public boolean isInsurdangerHobby() {
        return insurdangerHobby;
    }
    public void setInsurdangerHobby(boolean insurdangerHobby) {
        this.insurdangerHobby = insurdangerHobby;
    }
    public boolean isCardDisunity() {
        return cardDisunity;
    }
    public void setCardDisunity(boolean cardDisunity) {
        this.cardDisunity = cardDisunity;
    }
    public boolean isBigPolByPoor() {
        return bigPolByPoor;
    }
    public void setBigPolByPoor(boolean bigPolByPoor) {
        this.bigPolByPoor = bigPolByPoor;
    }
    public boolean isGoodsNotNeed() {
        return goodsNotNeed;
    }
    public void setGoodsNotNeed(boolean goodsNotNeed) {
        this.goodsNotNeed = goodsNotNeed;
    }
    public boolean isUnusualPurpose() {
        return unusualPurpose;
    }
    public void setUnusualPurpose(boolean unusualPurpose) {
        this.unusualPurpose = unusualPurpose;
    }
    public String getClientNo() {
        return clientNo;
    }
    public void setClientNo(String clientNo) {
        this.clientNo = clientNo;
    }


}