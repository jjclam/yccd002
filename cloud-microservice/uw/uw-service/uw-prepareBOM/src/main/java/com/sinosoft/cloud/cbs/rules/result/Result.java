package com.sinosoft.cloud.cbs.rules.result;

import java.util.List;
import java.util.Map;

/**
 * 返回结果
 * 
 * @author dingfan
 * 
 */
public class Result{
	/**
	 * 保单号
	 */
	private String contno;
	
	/**
	 * 核保通过标识
	 */
	private boolean flag;

	/**
	 * 返回信息
	 */
	private String message;

	/**
	 * 规则违反信息列表
	 */
	private List uwresultList;
	
	/**
	 * 预处理中规则对应其执行状态
	 */
	private Map ruleDecisionMap;

	public boolean isFlag() {
		return flag;
	}

	public void setFlag(boolean flag) {
		this.flag = flag;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
	public List getUwresultList() {
		return uwresultList;
	}

	public void setUwresultList(List uwresultList) {
		this.uwresultList = uwresultList;
	}

	public Map getRuleDecisionMap() {
		return ruleDecisionMap;
	}

	public void setRuleDecisionMap(Map ruleDecisionMap) {
		this.ruleDecisionMap = ruleDecisionMap;
	}

	public String getContno() {
		return contno;
	}

	public void setContno(String contno) {
		this.contno = contno;
	}
}
