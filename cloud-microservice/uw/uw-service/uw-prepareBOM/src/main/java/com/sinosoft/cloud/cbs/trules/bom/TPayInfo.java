package com.sinosoft.cloud.cbs.trules.bom;
/**
 * 收费信息
 *
 */
public class TPayInfo {
	/**
	 * 交费方式
	 */
	private String payWay;
	/**
	 * 开户银行
	 */
	private String bankAccname;
	/**
	 * 账号
	 */
	private String bankAccno;
	/**
	 * 是否自动续保
	 */
	private String isAutoInsurRenew;
	
	/**
	 * 是否预交保险费
	 */
	private String isPrepayPrem;
	
	/**
	 * 总保费
	 */
	private double sumPrem;
	

    /**
     * 交费账号与我公司其他客户重复
     * @return
     */
   private String repeatInPaymentNo;
   
   
	public double getSumPrem() {
		return sumPrem;
	}

	public void setSumPrem(double sumPrem) {
		this.sumPrem = sumPrem;
	}

	public String getPayWay() {
		return payWay;
	}

	public void setPayWay(String payWay) {
		this.payWay = payWay;
	}

	public String getBankAccname() {
		return bankAccname;
	}

	public void setBankAccname(String bankAccname) {
		this.bankAccname = bankAccname;
	}

	public String getBankAccno() {
		return bankAccno;
	}

	public void setBankAccno(String bankAccno) {
		this.bankAccno = bankAccno;
	}

	public String getIsAutoInsurRenew() {
		return isAutoInsurRenew;
	}

	public void setIsAutoInsurRenew(String isAutoInsurRenew) {
		this.isAutoInsurRenew = isAutoInsurRenew;
	}

	public String getIsPrepayPrem() {
		return isPrepayPrem;
	}

	public void setIsPrepayPrem(String isPrepayPrem) {
		this.isPrepayPrem = isPrepayPrem;
	}

	public String getRepeatInPaymentNo() {
		return repeatInPaymentNo;
	}

	public void setRepeatInPaymentNo(String repeatInPaymentNo) {
		this.repeatInPaymentNo = repeatInPaymentNo;
	}

	

}
