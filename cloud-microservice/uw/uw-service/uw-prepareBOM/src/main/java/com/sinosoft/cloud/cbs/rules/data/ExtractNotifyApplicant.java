package com.sinosoft.cloud.cbs.rules.data;

import com.sinosoft.cloud.cbs.rules.bom.*;
import com.sinosoft.cloud.rest.TradeInfo;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/**
 *
 * 投保人告知赋值
 * @project: abc-cloud-microservice
 * @author: xiaozehua
 * @date: Created in 下午 3:45 2018/5/22 0022
 * @Description:
 */
@Service
public class ExtractNotifyApplicant {
    private final Log logger = LogFactory.getLog(this.getClass());
    public void getApplicantNotify(Policy policy, TradeInfo requestInfo){

        logger.debug("投保人告知提数开始" );
        long beginTime = System.currentTimeMillis();

        getAppNotify(policy,requestInfo);

        logger.debug("投保人告知提数：" + (System.currentTimeMillis() - beginTime) / 1000.0
                + "s");
    }


    public void getAppNotify(Policy policy,TradeInfo requestInfo){

        ExtractDisclose eDisclose = new ExtractDisclose();
        // 投保人信息
        Applicant applicantInfo = policy.getApplicant();

        ApplicantNotify appNotify = new ApplicantNotify();

        // 客户号
        String clientNo = applicantInfo.getClientNo();

        //只有一个投保人
        NotificationInfo appnInfo = eDisclose.getDiscloseExtract(policy, clientNo, "0",requestInfo,false);

        appNotify.setClientNo(appnInfo.getClientNo());

        Map map = appnInfo.getNotifyMap();
        if(map != null){
            Set keys = map.keySet();
            Iterator it = keys.iterator();

            while(it.hasNext()){
                String key = (String) it.next();



                //(银行代理保险投保书)有异常投保经历-您过去是否被保险公司解除保险合同，投保、复效时被加费、特约、延期、拒保，提出或已经得到理赔？①是②否
                if(key.equals("GX2_1_YesOrNo")){
                    appNotify.setYdUnusualPolicyPast(Util.toBoolean(((Notify)map.get("GX2_1_YesOrNo")).getContents()));
                }

                if(key.equals("GX3_2_YesOrNo")){
                    //(银行代理保险投保书)有危险运动爱好
                    appNotify.setYbdangerSportHobby(Util.toBoolean(((Notify)map.get("GX3_2_YesOrNo")).getContents()));
                }
                if(key.equals("01_016_Budget")){
                    //保费预算（元）
                    appNotify.setPremiumBudget(Util.toDouble(((Notify)map.get("01_016_Budget")).getContents()));
                }
                if(key.equals("GX11_19B_YesOrNo")){
                    //目前或曾经有阴道异常出血畸形瘤葡萄胎子宫肌瘤卵巢囊肿或乳房肿块等
                    appNotify.setGynecologicDiseases(Util.toBoolean(((Notify)map.get("GX11_19B_YesOrNo")).getContents()));
                }
                if(key.equals("01_001_AnnualIncome")){
                    //年收入(万元)可转换为数值

//				Notify noti = (Notify)map.get("01_001_AnnualIncome");
//				String s = noti.getContents();
//				appNotify.setAnnualIncome(s);



                    appNotify.setAnnualIncome((((Notify)map.get("01_001_AnnualIncome")).getContents()));



                }
                if(key.equals("GX1_2_YesOrNo")){
                    //除本投保申请外已经购买或目前正在申请除人寿外的其他保险公司的人身保险
                    appNotify.setHasOtherComGoods(Util.toBoolean(((Notify)map.get("GX1_2_YesOrNo")).getContents()));
                }
                if(key.equals("GX10_12_YesOrNo")){
                    //目前或曾经服用过成瘾药物
                    appNotify.setAddictedDrugs(Util.toBoolean(((Notify)map.get("GX10_12_YesOrNo")).getContents()));
                }
                if(key.equals("GX10_14_YesOrNo")){
                    //(个人人身保险投保书)过去一年去过医院就诊服药手术或其他治疗
                    appNotify.setGoToTheHospital(Util.toBoolean(((Notify)map.get("GX10_14_YesOrNo")).getContents()));
                }
                if(key.equals("GX10_15_YesOrNo")){
                    //(个人人身保险投保书)过去三年有医院检查（包括健康体检）结果为异常
                    appNotify.setLisUnusualDiagnose3Y(Util.toBoolean(((Notify)map.get("GX10_15_YesOrNo")).getContents()));
                }
                if(key.equals("GX10_16_YesOrNo")){
                    //过去五年曾住院检查或治疗
                    appNotify.setLivedHospital5Y(Util.toBoolean(((Notify)map.get("GX10_16_YesOrNo")).getContents()));
                }
                if(key.equals("GX10_7_YesOrNo")){
                    //(银行代理保险投保书)目前或曾经患有告知列疾病症状或接受任何治疗
                    appNotify.setYdHasThisOfDisease(Util.toBoolean(((Notify)map.get("GX10_7_YesOrNo")).getContents()));
                }
                if(key.equals("GX13_10_Remark")){
                    //(银行代理保险投保书)备注栏有内容
                    boolean flag = true;
                    String str = ((Notify)map.get("GX13_10_Remark")).getContents();
                    if (str.equals("*")) {
                        flag = false;
                    } else if (str.equals("无")) {
                        flag = false;
                    } else if (str.equals("")) {
                        flag = false;
                    }
                    appNotify.setNoteColumn(flag);
                }
                if(key.equals("GX2_3_YesOrNo")){
                    //(个人人身保险投保书)有异常投保经历
                    appNotify.setLisUnusualPolicyPast(Util.toBoolean(((Notify)map.get("GX2_3_YesOrNo")).getContents()));
                }
                if(key.equals("GX3_4_YesOrNo")){
                    //(个人人身保险投保书)有危险运动爱好
                    appNotify.setLisdangerSportHobby(Util.toBoolean(((Notify)map.get("GX3_4_YesOrNo")).getContents()));
                }
                if(key.equals("GX4_5_YesOrNo")){
                    //1年内计划出国
                    appNotify.setPlanGoAbroad1Y(Util.toBoolean(((Notify)map.get("GX4_5_YesOrNo")).getContents()));
                }
                if(key.equals("GX5_6_YesOrNo")){
                    //为职业司机或拥有摩托车驾照
                    appNotify.setProfessionalDriver(Util.toBoolean(((Notify)map.get("GX5_6_YesOrNo")).getContents()));
                }
                if(key.equals("GX6_09_HowMany")){
                    //每天抽烟量（支）
                    appNotify.setSmokeFew(Util.toDouble(((Notify)map.get("GX6_09_HowMany")).getContents()));
                }
                if(key.equals("GX6_09_SmokeYear")){
                    //烟龄
                    appNotify.setLengthAsSmoker(Util.toDouble(((Notify)map.get("GX6_09_SmokeYear")).getContents()));
                }
                if(key.equals("GX6_09_YesOrNo")){
                    //有吸烟史
                    appNotify.setHistoryOfSmoking(Util.toBoolean(((Notify)map.get("GX6_09_YesOrNo")).getContents()));
                }
                if(key.equals("GX7_11_YesOrNo")){
                    //目前或曾经有饮白酒、洋酒等烈性酒的习惯
                    appNotify.setHistoryOfDrinking(Util.toBoolean(((Notify)map.get("GX7_11_YesOrNo")).getContents()));
                }
                if(key.equals("GX8_18_YesOrNo")){
                    //有智能障碍或失明聋哑及言语咀嚼障碍
                    appNotify.setSufferMentalDisorders(Util.toBoolean(((Notify)map.get("GX8_18_YesOrNo")).getContents()));
                }
                if(key.equals("GX9_21_YesOrNo")){
                    //父母子女兄弟姐妹患有癌症心脑血管疾病白血病血友病糖尿病多囊肝多囊肾肠息肉或其他遗传性疾病等
                    appNotify.setFamilyHistoryOfDisease(Util.toBoolean(((Notify)map.get("GX9_21_YesOrNo")).getContents()));
                }
                if(key.equals("GX10_4_YesOrNo")){
                    //(银行代理保险投保书)过去一年去过医院就诊、服药、手术或其他治疗
                    appNotify.setYdGoHospitalLastYear(Util.toBoolean(((Notify)map.get("GX10_4_YesOrNo")).getContents()));
                }
                if(key.equals("GX10_5_YesOrNo")){
                    //(银行代理保险投保书)过去三年有医院检查（包括健康体检）结果为异常
                    appNotify.setYdUnusualDiagnose3Y(Util.toBoolean(((Notify)map.get("GX10_5_YesOrNo")).getContents()));
                }
                if(key.equals("GX10_6_YesOrNo")){
                    //(银行代理保险投保书)曾住院检查或治疗
                    appNotify.setYdLivedHospitalPast(Util.toBoolean(((Notify)map.get("GX10_6_YesOrNo")).getContents()));
                }
                if(key.equals("GX11_18C_YesOrNo")){
                    //曾被建议重复宫颈涂片检查或乳房超声、X光、活检等
                    appNotify.setGynecologialed(Util.toBoolean(((Notify)map.get("GX11_18C_YesOrNo")).getContents()));
                }
                if(key.equals("GX10_0_YesOrNo")){
                    //被保人患智能障碍、恶性肿瘤等或从事所列职业	GX10_0_YesOrNo
                    appNotify.setHasGrediseaseOrjob(Util.toBoolean(((Notify)map.get("GX10_0_YesOrNo")).getContents()));
                }
                if(key.equals("GX10_3_YesOrNo")){
                    //有身体残疾或功能障碍	GX10_3_YesOrNo
                    appNotify.setHasBodyDis(Util.toBoolean(((Notify)map.get("GX10_3_YesOrNo")).getContents()));
                }
                if(key.equals("GX10_8_YesOrNo")){
                    //客户或客户的配偶为艾滋病病毒感染者或准备接受艾滋病病毒检测	GX10_8_YesOrNo
                    appNotify.setHaveAIDS(Util.toBoolean(((Notify)map.get("GX10_8_YesOrNo")).getContents()));
                }
                if(key.equals("GX12_20B_YesOrNo")){
                    //早产过期产难产或其他先天疾病	GX12_20B_YesOrNo
                    appNotify.setAbnormalityOfChildren(Util.toBoolean(((Notify)map.get("GX12_20B_YesOrNo")).getContents()));
                }
                if(key.equals("GX13_21_Remark")){
                    //健康告知备注
                    boolean flag4 = true;
                    String str4 = ((Notify)map.get("GX13_21_Remark")).getContents();
                    if (str4.equals("*")) {
                        flag4 = false;
                    } else if (str4.equals("无")) {
                        flag4 = false;
                    } else if (str4.equals("")) {
                        flag4 = false;
                    }
                    appNotify.setHelthRemark(flag4);

                }
                if(key.equals("GX13_C_Remark")){
                    //(银行代理保险投保书)有备注
                    //insurNotify.setcRemarks(Util.toBoolean(map.get("GX13_C_Remark").getContents()));
                    boolean flag2 = true;
                    String str2 = ((Notify)map.get("GX13_C_Remark")).getContents();
                    if (str2.equals("*")) {
                        flag2 = false;
                    } else if (str2.equals("无")) {
                        flag2 = false;
                    } else if (str2.equals("")) {
                        flag2 = false;
                    }
                    appNotify.setcRemarks(flag2);
                }

            }
            policy.setApplicantNotify(appNotify);
        }
    }
}