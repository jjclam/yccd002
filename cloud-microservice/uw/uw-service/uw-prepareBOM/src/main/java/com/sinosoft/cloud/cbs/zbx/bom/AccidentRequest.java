package com.sinosoft.cloud.cbs.zbx.bom;

/**
 * Created by sll on 2019/3/28.
 * 中保信个人意外险核保风险请求信息
 */
public class AccidentRequest {
    /**
     * 账户号
     */
    private String userName;
    /**
     * 投保单号
     */
    private String policyCode;
    /**
     * 被保人姓名
     */
    private String policyHolderName;
    /**
     * 被保人证件类型
     */
    private String credentialsType;
    /**
     * 被保人证件号码
     */
    private String credentialsCode;
    /**
     * 保险公司机构编码
     */
    private String insuranceCompanyCode;
    /**
     * 是否已获得查询系统的客户授权是：Y，否：N
     */
    private String customerAllowed;
    /**
     * 产品编码
     */
    private String productCode;
    private String insurerUuid;

    public String getUserName() {return userName;}

    public void setUserName(String userName) {this.userName = userName;}

    public String getPolicyCode() {return policyCode;}

    public void setPolicyCode(String policyCode) {this.policyCode = policyCode;}

    public String getPolicyHolderName() {return policyHolderName;}

    public void setPolicyHolderName(String policyHolderName) {this.policyHolderName = policyHolderName;}

    public String getCredentialsType() {return credentialsType;}

    public void setCredentialsType(String credentialsType) {this.credentialsType = credentialsType;}

    public String getCredentialsCode() {return credentialsCode;}

    public void setCredentialsCode(String credentialsCode) {this.credentialsCode = credentialsCode;}

    public String getInsuranceCompanyCode() {return insuranceCompanyCode;}

    public void setInsuranceCompanyCode(String insuranceCompanyCode) {this.insuranceCompanyCode = insuranceCompanyCode;}

    public String getCustomerAllowed() {
        return customerAllowed;
    }

    public void setCustomerAllowed(String customerAllowed) {
        this.customerAllowed = customerAllowed;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public String getInsurerUuid() {
        return insurerUuid;
    }

    public void setInsurerUuid(String insurerUuid) {
        this.insurerUuid = insurerUuid;
    }
}
