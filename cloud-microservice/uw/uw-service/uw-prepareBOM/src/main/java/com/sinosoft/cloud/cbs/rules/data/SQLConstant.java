package com.sinosoft.cloud.cbs.rules.data;

/**
 * 提数SQL常量配置类
 *
 */
public class SQLConstant {
	/**
	 * 保单
	 */
	public static class Policy {
		/**
		 * 保单基本数据SQL（contno：合同号);
		 */
		public static final String POLICYBASICSQL = "select distinct A.RECEIVEDATE,B.CVALIDATE,A.POLAPPLYDATE,A.SELLTYPE,A.MANAGECOM,A.SALECHNL,A.HANDLER,A.BANKAGENT,A.BANKAGENTNAME,A.BANKCODE,A.NEWPAYMODE,A.RenewPayFlag,A.NewBankCode,A.NewAccName,A.NewBankAccNo,A.PayLocation,A.BankCode,A.AccName,A.BankAccNo,A.TBType,A.EAuto,A.SlipForm,A.CONTNO,A.AGENTCODE,A.APPFLAG,A.UWFLAG,A.FORCEUWFLAG,A.prtno from lccont A, lcpol B  where   A.contno=? and A.contno=B.contno ";

		/**
		 * 单证细类SQL（doccode：单证号码，subType：单证细类）
		 */
		public static final String SUBTYPESQL = "select '' as field1 , subType as field2, '' as field3, 1 as flag ,1 as field4 from ES_DOC_MAIN where doccode = ? ";

		/**
		 * 保单问题件SQL（CONTNO：合同号，BackObjType：退回对象类型，dealoverFlag：是否处理完毕）
		 */
		public static final String ISSUEPOLSQL = "select BackObjType as field1, dealoverFlag as field2, ISSUETYPE as field3, 2 as flag ,1 as field4 from lcissuepol where CONTNO = ? ";

		/**
		 * 单据类型SQL（OTHERNO：对应其他号码（此处提数对应合同号），OTHERNOTYPE：其它号码类型，CODE：单据类型）
		 */
		public static final String LOPRTMANAGERSQL = "select OTHERNOTYPE as field1, CODE as field2, ''as field3, 3 as flag ,1 as field4 from LOPRTMANAGER where OTHERNO = ? ";
		/**
		 * 保单受理日期（APPLYDATE: 保单受理日期）
		 */
		public static final String APPLYDATESQL ="select to_char(APPLYDATE) as field1, '' as field2, ''as field3,4 as flag ,1 as field4 from LYVERIFYAPP B where B.PRTNO= ?";

		/**
		 * 豁免险数量
		 */
		public static final String IMMUNITYFLAGNUMSQL="SELECT to_char(COUNT(1)) as field1, '' as field2, ''as field3, 5 as flag ,1 as field4 FROM LCDUTY WHERE CONTNO = ? AND DUTYCODE IN (SELECT DUTYCODE FROM LMRISKPARAMSDEF WHERE PARAMSTYPE = 'immunityflag')";

		/**
		 * 累计本单需交保费
		 */
		public static final String SUMPREMSQL = "SELECT to_char(SUM(A.PREM * (CASE WHEN SUBSTR(A.DUTYCODE,5,2) = '03' THEN 1 ELSE (SELECT DECODE(B.PAYINTV,0,1,1,12 * B.PAYYEARS,3,4*B.PAYYEARS,6,2*B.PAYYEARS,12,B.PAYYEARS) PAYYEARS FROM LCPOL B WHERE B.PRTNO = ? AND B.MAINPOLNO = B.POLNO) END))) as field1, '' as field2, ''as field3, 6 as flag ,1 as field4 FROM LCDUTY A, LCPOL C WHERE C.PRTNO = ? AND C.POLNO = A.POLNO";
		/**
		 * 被保人住院津贴合计
		 */
		public static final String SUMANMTSQL =" select '' as field1, '' as field2, ''as field3, 7 as flag , NVL((SELECT SUM(D.AMNT) FROM LCDUTY D, LCPOL P WHERE D.POLNO = P.POLNO AND D.DUTYCODE = '703203' AND P.INSUREDNO IN (SELECT INSUREDNO FROM LCCONT WHERE CONTNO = ? ) AND P.RISKCODE = '7032' AND P.APPFLAG IN ('0', '1', '2','3') AND P.UWFLAG NOT IN ('1', '2', 'a') AND NOT EXISTS (SELECT CONTNO FROM LCCONT WHERE CONTNO = P.CONTNO AND (STATE LIKE '004%' OR STATE LIKE '005%' OR STATE LIKE '006%'))), 0) + NVL((SELECT SUM(AMNT * 10 / 1000) FROM LCPOL WHERE INSUREDNO IN (SELECT INSUREDNO FROM LCCONT WHERE CONTNO =  ? ) AND APPFLAG IN ('0', '1', '2','3') AND RISKCODE IN ('7030', '7031') AND UWFLAG NOT IN ('1', '2', 'a') AND NOT EXISTS (SELECT CONTNO FROM LCCONT WHERE (STATE LIKE '004%' OR STATE LIKE '005%' OR STATE LIKE '006%') AND CONTNO = LCPOL.CONTNO)), 0) as field4 from dual";
		/**
		 * 单证细类PadSQL（doccode：单证号码，subType：单证细类）
		 */
		public static final String SUBTYPEPADSQL = "select '' as field1 , '' as field2, subType as field3, 8 as flag ,1 as field4 from LCPadImage where prtno = ? ";

		/**
		 * 移动展业预核保标记
		 */
		public static final String MITSQL = "select '' as field1 , '' as field2, standbyflag2 as field3, 9 as flag ,1 as field4 from lyverifyapp where prtno = ? ";
			}

	/**
	 * 投保人
	 *
	 */
	public static class Applicant {
		/**
		 * 投保人基本数据SQL（contno：合同号）
		 */
		public static final String APPTBASICSQL = "select distinct AppntName,IDType,IDNo,AppntSex,Stature,Avoirdupois,to_char(AppntBirthday,'YYYY-MM-DD'),IdValiDate,NativePlace,RgtAddress,Degree,Marriage,RelatToInsu,OccupationCode,OccupationType,WorkType,PluralityType,HaveMotorcycleLicence,LicenseType,AddressNo,BMI,APPRGTTPYE,POSITION,APPNTNO,CUSLevel,TINFlag from lcappnt where contno=? ";

		/**
		 * 投保人累计期交保费SQL（appntno：投保人编码）
		 */
//		public static final String SUMPERIODPREMSQL = "select SUM(prem) as field1, '' as field2, '' as field3, 1 as flag from lcpol where appntno = ? and payintv != 0 and ((paytodate != payenddate and paytodate is not null) or (paytodate is null)) and appflag in ('0', '1', '2') and uwflag not in ('a', '2', '1') GROUP BY APPNTNO ";
		public static final String SUMPERIODPREMSQL = "select SUM(prem) as field1, '' as field2, '' as field3, 1 as flag from lcpol where appntno = ? and payintv != 0 and ((paytodate != payenddate and paytodate is not null) or (paytodate is null)) and appflag in ('0', '1', '2','3') and uwflag not in ('a', '2', '1') ";

		/**
		 * 投保人累计趸交保费SQL（appntno：投保人编码）
		 */
//		public static final String SUMSINGLEPREMSQL = "select SUM(prem) as field1, '' as field2, '' as field3, 2 as flag from lcpol where appntno = ? and payintv = 0 and appflag in ('0', '1', '2') and uwflag not in ('a', '2', '1') GROUP BY APPNTNO ";
		public static final String SUMSINGLEPREMSQL = "select SUM(prem) as field1, '' as field2, '' as field3, 2 as flag from lcpol where appntno = ? and payintv = 0 and appflag in ('0', '1', '2','3') and uwflag not in ('a', '2', '1') ";

		/**
		 * 本单累计年交保费SQL（CONTNO：合同号）
		 */
		public static final String PREMPERYEARSQL = "SELECT NVL(SUM((12 / TO_NUMBER(PAYINTV)) * PREM), 0) as field1, '' as field2, '' as field3, 3 as flag FROM LCPOL WHERE  PAYINTV    IN ('12', '1', '3', '6') AND APPNTNO  = ? AND RISKCODE NOT IN ('5001', '5006', '5007', '5009', '7020', '5008','5019')  AND APPFLAG      IN ('0', '1', '2','3')  AND UWFLAG NOT   IN ('1', '2', 'a') AND NOT EXISTS (SELECT CONTNO FROM LCCONT WHERE (STATE LIKE '004%' OR STATE LIKE '005%' OR STATE LIKE '006%') AND CONTNO = LCPOL.CONTNO) "	;

		/**
		 * 本单累计年交保费去除'5013','3003'险种SQL
		 */
		public static final String PREMPERYEAR2SQL = "SELECT NVL(SUM((12 / TO_NUMBER(PAYINTV)) * PREM), 0) as field1, '' as field2, '' as field3, 18 as flag FROM LCPOL WHERE PAYINTV    IN ('12', '1', '3', '6') AND APPNTNO = ? AND RISKCODE NOT IN ('5001', '5006', '5007', '5009', '7020', '5008','5013','3003','5019') AND APPFLAG IN ('0', '1', '2','3') AND UWFLAG NOT   IN ('1', '2', 'a') AND NOT EXISTS (SELECT CONTNO FROM LCCONT WHERE (STATE LIKE '004%' OR STATE LIKE '005%' OR STATE LIKE '006%') AND CONTNO = LCPOL.CONTNO)"	;

		/**
		 * 本单累计期交保费SQL（contno：合同号）
		 */
		public static final String CONTPERIODPREMSQL = "select SUM(prem) as field1, '' as field2, '' as field3, 4 as flag from lcpol a where contno=? and payintv<>0 GROUP BY CONTNO ";

		/**
		 * 本单累计趸交保费SQL（contno：合同号）
		 */
		public static final String CONTSINGLEPREMSQL = "select SUM(prem) as field1, '' as field2, '' as field3, 5 as flag from lcpol a where contno=? and payintv=0 GROUP BY CONTNO ";

		/**
		 * 风险测评SQL（contno：合同号）
		 */
		public static final String LYVERIFYAPPSQL = "SELECT 0 as field1, RISKEVALUATIONRESULT as field2, '' as field3, 6 as flag FROM LYVERIFYAPP WHERE prtno = ? ";

		/**
		 * 平均年收入SQL（COMCODE：机构代码，cityavg：城镇平均年收入，valligeavg：乡村平均年收入）
		 */
		public static final String PERDISPOSINCOMESQL = "select 0 as field1, cityavg as field2, valligeavg as field3, 7 as flag from LDAvgInCom where COMCODE=substr(?,0,4) ";

		/**
		 * 投保人是否残疾SQL（customerno：客户编码，contno：合同号）
		 */
		//public static final String DISABLEDSIGNSQL = "select 1 as field1, '' as field2, '' as field3, 8 as flag from lccustomerimpartparams l WHERE ((l.impartver = 'DLR3' and l.impartcode = '1-3') or (l.impartver = 'GX8' and l.impartcode in ('17','10')) or (l.impartver = 'GX10' and l.impartcode = '3')) and l.impartparamname = 'YesOrNo' and l.impartparam = '1' and l.customerno = ? and l.customernotype = '0' and l.contno = ? ";
	/*	public static final String DISABLEDSIGNSQL = "select 1 as field1, '' as field2, '' as field3, 8 as flag from lccustomerimpartparams l WHERE ((l.impartver = 'DLR3' and l.impartcode = '1-3') or (l.impartver = 'GX8' and l.impartcode in ('18','10')) or (l.impartver = 'GX10' and l.impartcode = '3')) and l.impartparamname = 'YesOrNo' and l.impartparam = '1' and l.customerno = ? and l.customernotype = '0' and l.contno = ? ";
*/
		/**
		 * 黑名单标识SQL（BLACKLISTNO：黑名单客户号，IDNO：证件号码，IDTYPE：证件类型）
		 */
		public static final String BLACKSIGNSQL = "select 1 as field1, '' as field2, '' as field3, 9 as flag from ldblacklist where BLACKLISTNO = ? or (IDNO = ? and IDTYPE = ?) ";

		/**
		 * 投保人身高异常SQL （CONTNO：合同号，APPNTNO：投保人编码）
		 */
		public static final String HEIGHTERRSQL = "SELECT 1 as field1, '' as field2, '' as field3, 10 as flag FROM LCAPPNT A WHERE CONTNO = ? AND APPNTNO = ? AND STATURE>0 AND (STATURE < (SELECT S.MINSTATURE FROM LDPERSONBUILD S WHERE 'A' = S.AGEFLAG AND ? = S.SEX AND ? >= S.MINAGE AND ? < S.MAXAGE) OR STATURE > (SELECT S.MAXSTATURE FROM LDPERSONBUILD S WHERE 'A' = S.AGEFLAG AND ? = S.SEX AND ? >= S.MINAGE AND ? < S.MAXAGE)) ";


		/**
		 * 投保人体重异常SQL 成年（CONTNO：合同号，APPNTNO：投保人编码）
		 */
		public static final String WEIGHTERRSQL = "SELECT 1 as field1, '' as field2, '' as field3, 11 as flag FROM LCAPPNT A WHERE CONTNO = ? AND APPNTNO = ? AND AVOIRDUPOIS >0 AND (AVOIRDUPOIS < (SELECT S.MINAVOIRDUPOIS FROM LDPERSONBUILD S WHERE 'A' = S.AGEFLAG AND ? = S.SEX AND ? >= S.MINAGE AND ? < S.MAXAGE) OR AVOIRDUPOIS > (SELECT S.MAXAVOIRDUPOIS FROM LDPERSONBUILD S WHERE 'A' = S.AGEFLAG AND ? = S.SEX AND ? >= S.MINAGE AND ? < S.MAXAGE)) ";

		/**
		 * 投保人BMI异常SQL（CONTNO：合同号，APPNTNO：投保人编码）
		 */
		public static final String BMIERRSQL = "SELECT 1 as field1, '' as field2, '' as field3, 12 as flag FROM LCAPPNT A WHERE CONTNO = ? AND APPNTNO = ? AND (ROUND((AVOIRDUPOIS / POWER(DECODE(A.STATURE, 0, 100000, A.STATURE) / 100, 2)),0) < 18 OR ROUND((AVOIRDUPOIS / POWER((DECODE(A.STATURE, 0, 1, A.STATURE) / 100), 2)),0) > 28) ";

		/**
		 * 是否填写投保人告知SQL（CONTNO：合同号，CUSTOMERNO：客户编码）
		 */
		public static final String APPNOTIFYSQL = "SELECT 1 as field1, '' as field2, '' as field3, 13 as flag FROM DUAL WHERE (SELECT COUNT(1) FROM LCCUSTOMERIMPARTPARAMS A WHERE A.CONTNO = ? AND A.CUSTOMERNO = ? AND A.CUSTOMERNOTYPE = '0') != 0 ";
		/**
		 * 银保渠道累交保费
		 */
		public static final String YBPREMSQL = " SELECT SUM(prem)as field1, '' as field2, '' as field3, 14 as flag FROM lcpol WHERE appntno   = ? AND salechnl    ='3' AND payintv!    =0 AND ((paytodate!=payenddate AND paytodate  IS NOT NULL) OR (paytodate  IS NULL)) AND appflag    IN ('0','1','2','3') AND uwflag NOT IN ('a','2','1')";
	/*	*//**
		 * 累计本单需交保费
		 *//*
		public static final String SUMPREMSQL = "SELECT SUM(A.PREM * ( CASE WHEN SUBSTR(A.DUTYCODE,5,2) = '03'  THEN 1 ELSE (SELECT DECODE(B.PAYINTV,0,1,1,12 * B.PAYYEARS,3,4*B.PAYYEARS,6,2*B.PAYYEARS,12,B.PAYYEARS) PAYYEARS  FROM LCPOL B WHERE B.PRTNO   = ? AND B.MAINPOLNO = B.POLNO) END) as field1, '' as field2, '' as field3, 15 as flag FROM LCDUTY A,LCPOL C WHERE C.PRTNO = ? AND C.POLNO   = A.POLNO";

		*/
		/**
		 * 累计投保人特殊保费1
		 */
		public static final String SUMSPECIALPREMSQL1="SELECT SUM(C.PREM / MUTIPAYINTV(C.PAYINTV)) as field1, '' as field2, '' as field3, 16 as flag FROM LCPOL P, LCDUTY C WHERE P.APPNTNO = ? AND P.SALECHNL IN ('1', '4') AND P.CONTNO = C.CONTNO AND P.POLNO = C.POLNO AND P.PAYINTV > 0 AND SUBSTR(C.DUTYCODE, 5, 2) <> '03' AND C.DUTYCODE not in ('500606','500903','501903') AND APPFLAG IN ('0', '1', '2','3') AND UWFLAG NOT IN ('1', '2', 'a') AND NOT EXISTS (SELECT CONTNO FROM LCCONT WHERE (STATE LIKE '004%' OR STATE LIKE '005%' OR STATE LIKE '006%') AND CONTNO = P.CONTNO)";
		/**
		 * 累计投保人特殊保费2
		 */
		public static final String SUMSPECIALPREMSQL2="SELECT NVL(SUM(M.PREM), 0) as field1, '' as field2, '' as field3, 17 as flag FROM LCPREM M, LCPOL N WHERE M.POLNO = N.POLNO AND N.APPNTNO = ? AND (M.DUTYCODE = '500101' OR M.DUTYCODE = '500102' OR M.DUTYCODE = '500701' OR M.DUTYCODE = '500801' OR M.DUTYCODE = '500802' OR M.DUTYCODE = '501301' OR M.DUTYCODE IN ('500601','500602','500604','500605','500607') OR M.DUTYCODE IN ('300301','300302') OR M.DUTYCODE IN ('500901','501901','500902')) AND N.PAYENDYEAR NOT IN ('1', '3') AND N.APPFLAG IN ('0', '1', '2','3') AND N.UWFLAG NOT IN ('1', '2', 'a') AND NOT EXISTS (SELECT CONTNO FROM LCCONT WHERE (STATE LIKE '004%' OR STATE LIKE '005%' OR STATE LIKE '006%') AND CONTNO = N.CONTNO)";

		/**
		 * 累计投保人特殊保费3
		 */
		public static final String SUMSPECIALPREMSQL3="SELECT NVL(SUM(M.PREM), 0)as field1, '' as field2, '' as field3, 19 as flag  FROM LCPREM M, LCPOL N WHERE M.POLNO = N.POLNO AND N.APPNTNO = ?  AND (M.DUTYCODE = '500101' OR M.DUTYCODE = '500102' OR M.DUTYCODE = '500701' OR M.DUTYCODE = '500801' OR  M.DUTYCODE = '500802') AND N.PAYENDYEAR NOT IN ('1', '3') AND N.APPFLAG IN ('0', '1', '2','3') AND N.UWFLAG NOT IN ('1', '2', 'a') AND NOT EXISTS (SELECT CONTNO FROM LCCONT WHERE (STATE LIKE '004%' OR STATE LIKE '005%' OR STATE LIKE '006%') AND CONTNO = N.CONTNO)";


		/**
		 * 黑名单1-证件号与和黑名单相同,不分国籍,>0触发,参数:一个contno
		 */
		public static final String APPBLACKIDFLAG="select  count(1)  as field1,'' as field2, '' as field3, 20 as flag from LXBlackListIndv a, lcappnt b, lxidtypeorno c where  b.contno = ? and c.blacklistid = a.blacklistid and b.idno = c.idno";

		/**
		 *  xxx黑名单2-中国客户名字、生日与黑名单相同,>0触发,参数:一个contno
		 */
		public static final String APPBLACKCHNFLAG = "select field1,'' as field2,'' as field3, 21 as flag from (select count(distinct(count(e.blacklistid))) as field1 from LXBlackListIndv a, lcappnt b, lxblackdate e where  b.contno = ? and a.blacklistid = e.blacklistid and a.name = b.appntname and b.nativeplace = 'CHN' group by e.blacklistid, e.blackdate, b.appntbirthday having (select case when ((select count(1) from dual where (select count(1) from lxblackdate p where p.blacklistid = e.blacklistid) >= 1 and length(e.blackdate) = 4 and e.blackdate = substr(to_char(b.appntbirthday, 'yyyy-mm-dd'), 1, 4))) > 0 then 1 else case when to_char(b.appntbirthday,'yyyy-mm-dd') >= (select min(blackdate) from lxblackdate where blacklistid = e.blacklistid) and to_char(b.appntbirthday,'yyyy-mm-dd') <= (select max(blackdate) from lxblackdate where blacklistid = e.blacklistid) then 1 else 0 end end from dual) > 0) where rownum = 1";

		/**
		 * xxx黑名单3-投保人名1部分组成,投保人名与黑名单表四个名字对比,>0触发,参数:4个contno
		 */
		public static final String APPBLACKFLAG1 = "select (case when (select firsttab.num from (select count(distinct(e.blacklistid)) as num from LXBlackListIndv a, lcappnt b, lxblackdate e where  b.contno = ? and e.blacklistid = a.blacklistid and instr(b.appntname, ' ') = 0 and a.firstname = b.appntname and (b.nativeplace is null or b.nativeplace <> 'CHN') group by e.blacklistid, e.blackdate, b.appntbirthday having (select case when ((select count(1) from dual where (select count(1) from lxblackdate p where p.blacklistid = e.blacklistid) >= 1 and length(e.blackdate) = 4 and e.blackdate = substr(to_char(b.appntbirthday, 'yyyy-mm-dd'), 1, 4))) > 0 then 1 else case when to_char(appntbirthday,'yyyy-mm-dd') >= (select min(blackdate) from lxblackdate where blacklistid = e.blacklistid) and to_char(appntbirthday,'yyyy-mm-dd') <= (select max(blackdate) from lxblackdate where blacklistid = e.blacklistid) then 1 else 0 end end from dual) > 0) firsttab where rownum = 1 ) > 0 then 1 else 0 end + case when (select middletab.num from (select count(distinct(e.blacklistid)) as num from LXBlackListIndv a, lcappnt b, lxblackdate e where  b.contno = ? and e.blacklistid = a.blacklistid and instr(b.appntname, ' ') = 0 and a.middlename = b.appntname and (b.nativeplace is null or b.nativeplace <> 'CHN') group by e.blacklistid, e.blackdate, b.appntbirthday having (select case when ((select count(1) from dual where (select count(1) from lxblackdate p where p.blacklistid = e.blacklistid) >= 1 and length(e.blackdate) = 4 and e.blackdate = substr(to_char(b.appntbirthday, 'yyyy-mm-dd'), 1, 4))) > 0 then 1 else case when to_char(appntbirthday,'yyyy-mm-dd') >= (select min(blackdate) from lxblackdate where blacklistid = e.blacklistid) and to_char(appntbirthday,'yyyy-mm-dd') <= (select max(blackdate) from lxblackdate where blacklistid = e.blacklistid) then 1 else 0 end end from dual) > 0) middletab where rownum = 1) > 0 then 1 else 0 end + case when (select nametab.num from (select count(distinct(e.blacklistid)) as num from LXBlackListIndv a, lcappnt b, lxblackdate e where  b.contno = ? and e.blacklistid = a.blacklistid and instr(b.appntname, ' ') = 0 and a.surname = b.appntname and (b.nativeplace is null or b.nativeplace <> 'CHN') group by e.blacklistid, e.blackdate, b.appntbirthday having (select case when ((select count(1) from dual where (select count(1) from lxblackdate p where p.blacklistid = e.blacklistid) >= 1 and length(e.blackdate) = 4 and e.blackdate = substr(to_char(b.appntbirthday, 'yyyy-mm-dd'), 1, 4))) > 0 then 1 else case when to_char(appntbirthday,'yyyy-mm-dd') >= (select min(blackdate) from lxblackdate where blacklistid = e.blacklistid) and to_char(appntbirthday,'yyyy-mm-dd') <= (select max(blackdate) from lxblackdate where blacklistid = e.blacklistid) then 1 else 0 end end from dual) > 0) nametab where rownum = 1) > 0 then 1 else 0 end + case when (select surntab.num from (select count(distinct(e.blacklistid)) as num from LXBlackListIndv a, lcappnt b, lxblackdate e where  b.contno = ? and e.blacklistid = a.blacklistid and instr(b.appntname, ' ') = 0 and a.name = b.appntname and (b.nativeplace is null or b.nativeplace <> 'CHN') group by e.blacklistid, e.blackdate, b.appntbirthday having (select case when ((select count(1) from dual where (select count(1) from lxblackdate p where p.blacklistid = e.blacklistid) >= 1 and length(e.blackdate) = 4 and e.blackdate = substr(to_char(b.appntbirthday, 'yyyy-mm-dd'), 1, 4))) > 0 then 1 else case when to_char(appntbirthday,'yyyy-mm-dd') >= (select min(blackdate) from lxblackdate where blacklistid = e.blacklistid) and to_char(appntbirthday,'yyyy-mm-dd') <= (select max(blackdate) from lxblackdate where blacklistid = e.blacklistid) then 1 else 0 end end from dual) > 0) surntab where rownum = 1) > 0 then 1 else 0 end) as field1, '' as field2, '' as field3, 22 as flag from dual ";

		/**
		 * xxx黑名单4-投保人名2部分组成,投保人名与黑名单表2个名字对比,>0触发,参数:2个contno
		 * 有一个name相同，且出生日期相同
		 */
		public static final String APPBLACKFLAG2 = "select (case when (select firsttab.num from (select count(distinct(e.blacklistid)) as num from LXBlackListIndv a, lcappnt b, lxblackdate e where  b.contno = ? and e.blacklistid = a.blacklistid and length(b.appntname) - length(replace(b.appntname, ' ')) = 1 and a.firstname = substr(b.appntname, 1, instr(b.appntname, ' ', 1) - 1) and (b.nativeplace is null or b.nativeplace <> 'CHN') group by e.blacklistid, e.blackdate, b.appntbirthday having (select case when ((select count(1) from dual where (select count(1) from lxblackdate p where p.blacklistid = e.blacklistid) >= 1 and length(e.blackdate) = 4 and e.blackdate = substr(to_char(b.appntbirthday, 'yyyy-mm-dd'), 1, 4))) > 0 then 1 else case when to_char(b.appntbirthday,'yyyy-mm-dd') >= (select min(blackdate) from lxblackdate where blacklistid = e.blacklistid) and to_char(b.appntbirthday,'yyyy-mm-dd') <= (select max(blackdate) from lxblackdate where blacklistid = e.blacklistid) then 1 else 0 end end from dual) > 0) firsttab where rownum = 1) > 0 then 1 else 0 end + case when (select surntab.num from (select count(distinct(e.blacklistid)) as num from LXBlackListIndv a, lcappnt b, lxblackdate e where  b.contno = ? and e.blacklistid = a.blacklistid and length(b.appntname) - length(replace(b.appntname, ' ')) = 1 and a.surname = substr(b.appntname, instr(b.appntname, ' ', 1) + 1, length(b.appntname)) and (b.nativeplace is null or b.nativeplace <> 'CHN') group by e.blacklistid, e.blackdate, b.appntbirthday having (select case when ((select count(1) from dual where (select count(1) from lxblackdate p where p.blacklistid = e.blacklistid) >= 1 and length(e.blackdate) = 4 and e.blackdate = substr(b.appntbirthday, 1, 4))) > 0 then 1 else case when to_char(b.appntbirthday,'yyyy-mm-dd') >= (select min(blackdate) from lxblackdate where blacklistid = e.blacklistid) and to_char(b.appntbirthday,'yyyy-mm-dd') <= (select max(blackdate) from lxblackdate where blacklistid = e.blacklistid) then 1 else 0 end end from dual) > 0) surntab where rownum = 1) > 0 then 1 else 0 end) as field1, '' as field2, '' as field3, 23 as flag from dual ";

		/**
		 * 黑名单5-投保人名2部分组成,投保人名与黑名单表2个名字对比,>=2触发,参数:2个contno
		 * firstname和surnname都要相同.无生日限制
		 */
		public static final String APPBLACKFLAG3 = " select ((select count(1)  from LXBlackListIndv a, lcappnt b   where  b.contno = ? and length(b.appntname) - length(replace(b.appntname, ' ')) = 1  and a.firstname = substr(b.appntname ,1, instr(b.appntname, ' ', 1)-1)  and (b.nativeplace is null or b.nativeplace <> 'CHN')) +  (select count(1)  from LXBlackListIndv a, lcappnt b  where  b.contno = ?  and length(b.appntname) - length(replace(b.appntname, ' ')) = 1 and a.surname = substr(b.appntname ,instr(b.appntname, ' ', 1)+1,length(b.appntname))   and (b.nativeplace is null or b.nativeplace <> 'CHN'))) as field1,'' as field2, '' as field3, 24 as flag from dual  ";

		/**
		 * xxx黑名单6-投保人名>=3部分组成,投保人名与黑名单表3个名字对比,>=0触发,参数:3个contno
		 * firstname,middlename,surname,有生日限制
		 */
		public static final String APPBLACKFLAG4 = "SELECT (CASE WHEN (select firsttab.num from (SELECT count(distinct(e.blacklistid)) as num FROM LXBlackListIndv a, lcappnt b, lxblackdate e WHERE  b.contno = ? AND e.blacklistid = a.blacklistid AND (length(b.appntname) - length(replace(b.appntname, ' '))) >= 2 AND a.firstname = substr(b.appntname, 1, instr(b.appntname, ' ') - 1) AND (b.nativeplace IS NULL OR b.nativeplace <> 'CHN') GROUP BY e.blacklistid, e.blackdate, b.appntbirthday HAVING (SELECT CASE WHEN ((SELECT count(1) FROM dual WHERE (SELECT count(1) FROM lxblackdate p WHERE p.blacklistid = e.blacklistid) >= 1 AND length(e.blackdate) = 4 AND e.blackdate = substr(to_char(b.appntbirthday, 'yyyy-mm-dd'), 1, 4))) > 0 THEN 1 ELSE CASE WHEN to_char(b.appntbirthday,'yyyy-mm-dd') >= (select min(blackdate) from lxblackdate where blacklistid = e.blacklistid) and to_char(b.appntbirthday,'yyyy-mm-dd') <= (select max(blackdate) from lxblackdate where blacklistid = e.blacklistid) THEN 1 ELSE 0 END END FROM dual) > 0) firsttab where rownum = 1) > 0 THEN 1 ELSE 0 END + CASE WHEN (select middletab.num from (SELECT count(distinct(e.blacklistid)) as num FROM LXBlackListIndv a, lcappnt b, lxblackdate e WHERE  b.contno = ? AND e.blacklistid = a.blacklistid AND (length(b.appntname) - length(replace(b.appntname, ' '))) >= 2 AND a.middlename = substr(b.appntname, instr(b.appntname, ' ') + 1, (instr(b.appntname, ' ', 1, 2)) - (instr(b.appntname, ' ') + 1)) AND (b.nativeplace IS NULL OR b.nativeplace <> 'CHN') GROUP BY e.blacklistid, e.blackdate, b.appntbirthday HAVING (SELECT CASE WHEN ((SELECT count(1) FROM dual WHERE (SELECT count(1) FROM lxblackdate p WHERE p.blacklistid = e.blacklistid) >= 1 AND length(e.blackdate) = 4 AND e.blackdate = substr(to_char(b.appntbirthday, 'yyyy-mm-dd'), 1, 4))) > 0 THEN 1 ELSE CASE WHEN to_char(b.appntbirthday,'yyyy-mm-dd') >= (select min(blackdate) from lxblackdate where blacklistid = e.blacklistid) and to_char(b.appntbirthday,'yyyy-mm-dd') <= (select max(blackdate) from lxblackdate where blacklistid = e.blacklistid) THEN 1 ELSE 0 END END FROM dual) > 0) middletab where rownum = 1) > 0 THEN 1 ELSE 0 END + CASE WHEN (select surntab.num from ( SELECT count(distinct(e.blacklistid)) as num FROM LXBlackListIndv a, lcappnt b, lxblackdate e WHERE  b.contno = ? AND e.blacklistid = a.blacklistid AND (length(b.appntname) - length(replace(b.appntname, ' '))) >= 2 AND a.surname = substr(b.appntname, instr(b.appntname, ' ', 1, 2) + 1) AND to_char(b.appntbirthday, 'yyyy-mm-dd') = e.blackdate AND (b.nativeplace IS NULL OR b.nativeplace <> 'CHN') GROUP BY e.blacklistid, e.blackdate, b.appntbirthday HAVING (SELECT CASE WHEN ((SELECT count(1) FROM dual WHERE (SELECT count(1) FROM lxblackdate p WHERE p.blacklistid = e.blacklistid) >= 1 AND length(e.blackdate) = 4 AND e.blackdate = substr(to_char(b.appntbirthday, 'yyyy-mm-dd'), 1, 4))) > 0 THEN 1 ELSE CASE WHEN to_char(b.appntbirthday,'yyyy-mm-dd') >= (select min(blackdate) from lxblackdate where blacklistid = e.blacklistid) and to_char(b.appntbirthday,'yyyy-mm-dd') <= (select max(blackdate) from lxblackdate where blacklistid = e.blacklistid) THEN 1 ELSE 0 END END FROM dual) > 0) surntab where rownum = 1) > 0 THEN 1 ELSE 0 END) AS field1, '' AS field2, '' AS field3, 25 AS flag FROM dual ";
		/**
		 * XXXXXX黑名单7-投保人名>=3部分组成,投保人名与黑名单表3个名字对比,>=2触发,参数:3个contno
		 * firstname,middlename,surname,无生日限制
		 */
		public static final String APPBLACKFLAG5 = "SELECT (select case when (SELECT count(1) FROM LXBlackListIndv a, lcappnt b WHERE  b.contno = ? AND (length(b.appntname) - length(replace(b.appntname, ' '))) >= 2 AND a.firstname = substr(b.appntname, 1, instr(b.appntname, ' ') - 1) AND (b.nativeplace IS NULL OR b.nativeplace <> 'CHN')) > 0 then 1 else 0 end from dual) + (select case when (SELECT count(1) FROM LXBlackListIndv a, lcappnt b WHERE  b.contno = ? AND (length(b.appntname) - length(replace(b.appntname, ' '))) >= 2 AND a.middlename = substr(b.appntname, instr(b.appntname, ' ') + 1, (instr(b.appntname, ' ', 1, 2)) - (instr(b.appntname, ' ') + 1)) AND (b.nativeplace IS NULL OR b.nativeplace <> 'CHN')) > 0 then 1 else 0 end from dual) + (select case when (SELECT count(1) FROM LXBlackListIndv a, lcappnt b WHERE  b.contno = ? AND (length(b.appntname) - length(replace(b.appntname, ' '))) >= 2 AND a.surname = substr(b.appntname, instr(b.appntname, ' ', 1, 2) + 1) AND (b.nativeplace IS NULL OR b.nativeplace <> 'CHN')) > 0 then 1 else 0 end from dual) AS field1, '' AS field2, '' AS field3, 26 AS flag FROM dual";

		//投保人7053险种各期保费之和
		public static final String RENEWALPREMSUM7053SQL = "select ACCRENEWALPREM(?) as field1,'' as field2,'' as field3, 27 as flag from dual";
		/**
		 * 中保信上一年度理赔额
		 */
		public static final String YEARCLAIMLIMITSQL="select AnnualClaimSa as field1,'' as field2,'' as field3, 28 as flag from lctaxcont where CONTNO = ? ";
		/**
		 * 钻石卡校验---xzh
		 */
		//public static final String LCAPPNTCLIENTLEVEL = "select TO_NUMBER(ditype) as field1,'' as field2,'' as field3, 29 as flag from lmwhitelist a where  a.name = ? and a.idtype = ? and a.idno = ? and a.birthday = ? and a.sex = ?";


		public static final String WHITESIGNSQL="SELECT 1 as field1,'' as field2,'' as field3,30 as flag FROM lmwhitelist a , lcinsured b WHERE a.applystate = '2' AND a.userstate = '1' AND a.wfwflag = '01' AND a.name = b.name AND a.idtype = b.idtype AND a.idno = b.idno AND a.birthday =b.birthday AND a.sex = b.sex AND b.appntno = ? AND b.contno = ?";
	}

	/**
	 * 被保人
	 *
	 */
	public static class InsuredInfo {
		/**
		 * 被保人基本数据SQL（contno：合同号）
		 */
		public static final String INSURBASICSQL = "select distinct A.Name,A.IDType,A.IDNo,A.Sex,to_char(A.Birthday,'YYYY-MM-DD'),A.Stature,A.Avoirdupois,A.IdValiDate,A.NativePlace,A.RgtAddress,A.Degree,A.Marriage,A.RelationToMainInsured,A.OccupationCode,A.OccupationType,A.WorkType,A.PluralityType,A.LicenseType,A.AddressNo,A.BMI,A.firstname,A.lastname,A.HEALTH,A.POSITION,B.INSUREDNO,A.RELATIONTOAPPNT from lcinsured A, LCCONT B where A.contno=b.contno and B.INSUREDNO =A.INSUREDNO And A.contno =?";
		//public static final String INSURBASICSQL = "select distinct A.Name,A.IDType,A.IDNo,A.Sex,to_char(A.Birthday,'YYYY-MM-DD'),A.Stature,A.Avoirdupois,A.IdValiDate,A.NativePlace,A.RgtAddress,A.Degree,A.Marriage,A.RelationToMainInsured,A.OccupationCode,A.OccupationType,A.WorkType,A.PluralityType,A.LicenseType,A.AddressNo,A.BMI,A.firstname,A.lastname,A.HEALTH,A.POSITION,A.INSUREDNO,A.RELATIONTOAPPNT from lcinsured A where A.contno =?";

		/**
		 * x被保人是否残疾SQL（customerno：客户编码，contno：合同号）
		 */
		//public static final String DISABLEDSIGNSQL = "select 1 as field1,1 as flag from lccustomerimpartparams l WHERE ((l.impartver = 'DLR3' and l.impartcode = '1-3') or (l.impartver = 'GX8' and l.impartcode in ('17','10')) or (l.impartver = 'GX10' and l.impartcode = '3')) and l.impartparamname = 'YesOrNo' and l.impartparam = '1' and l.customerno = ?  and l.contno = ? ";
		/*public static final String DISABLEDSIGNSQL = "select 1 as field1,1 as flag from lccustomerimpartparams l WHERE ((l.impartver = 'DLR3' and l.impartcode = '1-3') or (l.impartver = 'GX8' and l.impartcode in ('18','10')) or (l.impartver = 'GX10' and l.impartcode = '3')) and l.impartparamname = 'YesOrNo' and l.impartparam = '1' and l.customerno = ?  and l.contno = ? ";
*/
		/**
		 * 被保人黑名单标识SQL（BLACKLISTNO：黑名单客户号，IDNO：证件号码，IDTYPE：证件类型）
		 */
		public static final String BLACKSIGNSQL = "select 1 as field1,2 as flag from ldblacklist where BLACKLISTNO = ? or (IDNO = ? and IDTYPE = ?) ";

		/**
		 * 被保人身高异常SQL,成年人（CONTNO：合同号，INSUREDNO：被保人编码）
		 */
		public static final String HEIGHTERRSQL1 = "SELECT 1 as field1,3 as flag from LCINSURED A WHERE CONTNO = ? AND INSUREDNO = ? AND STATURE>0 AND (STATURE < (SELECT S.MINSTATURE FROM LDPERSONBUILD S WHERE 'A' = S.AGEFLAG AND A.SEX = S.SEX AND ? >= S.MINAGE AND ? < S.MAXAGE) OR STATURE > (SELECT S.MAXSTATURE FROM LDPERSONBUILD S WHERE 'A'= S.AGEFLAG AND A.SEX = S.SEX AND ? >= S.MINAGE AND ? < S.MAXAGE)) ";

		/**
		 * 被保人身高异常SQL,未成年,小于7岁（CONTNO：合同号，APPNTNO：投保人编码）
		 */
		public static final String HEIGHTERRSQL2 ="SELECT 1 as field1, 3 as flag  FROM LCINSURED A, (SELECT S.MINSTATURE, S.MAXSTATURE FROM LDPERSONBUILD S,  LCINSURED C WHERE CONTNO  = ?  AND INSUREDNO    = ?  AND 'M'   = S.AGEFLAG  AND C.SEX   = S.SEX AND MONTHS_BETWEEN(TO_DATE( ? , 'YYYY-MM-DD'), C.BIRTHDAY) >= S.MINAGE  AND MONTHS_BETWEEN(TO_DATE( ?, 'YYYY-MM-DD'), C.BIRTHDAY)  < S.MAXAGE ) B  WHERE CONTNO = ?  AND INSUREDNO   = ?  AND ISNUMERIC(STATURE)    =1  AND STATURE  >0  AND (STATURE  < B.MINSTATURE  OR STATURE  > B.MAXSTATURE) ";


		/**
		 * 被保人身高异常SQL,未成年,7到18岁（CONTNO：合同号，APPNTNO：投保人编码）
		 */
		public static final String HEIGHTERRSQL3 = "SELECT 1 as field1, 3 as flag  FROM LCINSURED A, (SELECT S.MINSTATURE, S.MAXSTATURE FROM LDPERSONBUILD S,  LCINSURED C WHERE CONTNO  = ?  AND INSUREDNO    = ?  AND 'M'   = S.AGEFLAG  AND C.SEX   = S.SEX AND MONTHS_BETWEEN(TO_DATE( ? , 'YYYY-MM-DD'), C.BIRTHDAY) >= S.MINAGE  AND MONTHS_BETWEEN(TO_DATE( ?, 'YYYY-MM-DD'), C.BIRTHDAY)  < S.MAXAGE ) B  WHERE CONTNO = ?  AND INSUREDNO   = ?  AND ISNUMERIC(STATURE)    =1  AND STATURE  >0  AND (STATURE  < B.MINSTATURE) ";


		/**
		 * 被保人体重异常SQL 成年（CONTNO：合同号，INSUREDNO：被保人编码）
		 */
		public static final String WEIGHTERRSQL1 = "SELECT 1 as field1,4 as flag from LCINSURED A WHERE CONTNO = ? AND INSUREDNO = ? AND AVOIRDUPOIS >0 AND (AVOIRDUPOIS < (SELECT S.MINAVOIRDUPOIS FROM LDPERSONBUILD S WHERE  'A'= S.AGEFLAG AND A.SEX = S.SEX AND ? >= S.MINAGE AND ? < S.MAXAGE) OR AVOIRDUPOIS > (SELECT S.MAXAVOIRDUPOIS FROM LDPERSONBUILD S WHERE 'A' = S.AGEFLAG AND A.SEX = S.SEX AND ? >= S.MINAGE AND ? < S.MAXAGE)) ";

		/**
		 * 被保人体重异常SQL 未成年,小于7岁（CONTNO：合同号，APPNTNO：投保人编码）
		 */
		public static final String WEIGHTERRSQL2 = "SELECT 1 as field1,4 as flag FROM LCINSURED A, (SELECT  S.MINAVOIRDUPOIS,  S.MAXAVOIRDUPOIS FROM LDPERSONBUILD S,LCINSURED C  WHERE CONTNO  = ?  AND INSUREDNO  = ?   AND 'M' = S.AGEFLAG AND C.SEX  = S.SEX AND MONTHS_BETWEEN(TO_DATE(? , 'YYYY-MM-DD'), C.BIRTHDAY) >= S.MINAGE  AND MONTHS_BETWEEN(TO_DATE( ? , 'YYYY-MM-DD'), C.BIRTHDAY)  < S.MAXAGE ) B WHERE CONTNO  = ?  AND INSUREDNO  = ?   AND ISNUMERIC(AVOIRDUPOIS)=1  AND AVOIRDUPOIS  >0 AND ( AVOIRDUPOIS  < B.MINAVOIRDUPOIS  OR AVOIRDUPOIS  > B.MAXAVOIRDUPOIS) ";

		/**
		 * 被保人体重异常SQL 未成年,7岁到18岁（CONTNO：合同号，APPNTNO：投保人编码）
		 */
		public static final String WEIGHTERRSQL3 = "SELECT 1 as field1,4 as flag FROM LCINSURED A, (SELECT  S.MINAVOIRDUPOIS,  S.MAXAVOIRDUPOIS FROM LDPERSONBUILD S,LCINSURED C  WHERE CONTNO  = ?  AND INSUREDNO  = ?   AND 'M' = S.AGEFLAG AND C.SEX  = S.SEX AND MONTHS_BETWEEN(TO_DATE(? , 'YYYY-MM-DD'), C.BIRTHDAY) >= S.MINAGE  AND MONTHS_BETWEEN(TO_DATE( ? , 'YYYY-MM-DD'), C.BIRTHDAY)  < S.MAXAGE ) B WHERE CONTNO  = ?  AND INSUREDNO  = ?   AND ISNUMERIC(AVOIRDUPOIS)=1  AND AVOIRDUPOIS  >0 AND ( AVOIRDUPOIS  < B.MINAVOIRDUPOIS ) ";


		/**
		 * 成年人,开门红需求下限有18改为17,bmi值保留整数,被保人BMI异常SQL （CONTNO：合同号，INSUREDNO：被保人编码）
		 */
		public static final String BMIERRSQL1 = "SELECT 1 as field1,5 as flag from LCINSURED A WHERE CONTNO = ? AND INSUREDNO = ? AND (ROUND((AVOIRDUPOIS / POWER(DECODE(A.STATURE, 0, 100000, A.STATURE) / 100, 2)),0) < 17 OR ROUND((AVOIRDUPOIS / POWER((DECODE(A.STATURE, 0, 1, A.STATURE) / 100), 2)),0) > 28) ";



		/**
		 * 被保人BMI异常SQL 未成年（CONTNO：合同号，INSUREDNO：被保人编码）
		 */
		public static final String BMIERRSQL2 = "SELECT 1 as field1,5 as flag from (SELECT (SELECT A.IMPARTPARAM FROM LCCUSTOMERIMPARTPARAMS A WHERE A.PATCHNO = '19055' AND A.IMPARTVER = 'GX17' AND A.IMPARTCODE = '12A' AND A.CUSTOMERNOTYPE = '1' AND A.IMPARTPARAMNO = '1' AND A.IMPARTPARAMNAME = 'Height' AND A.CONTNO = ?) HEIGHT, (SELECT A.IMPARTPARAM FROM LCCUSTOMERIMPARTPARAMS A WHERE A.PATCHNO = '19055' AND A.IMPARTVER = 'GX17' AND A.IMPARTCODE = '12A' AND A.CUSTOMERNOTYPE = '1' AND A.IMPARTPARAMNO = '2' AND A.IMPARTPARAMNAME = 'Weight' AND A.CONTNO = ?) WEIGHT, (SELECT A.IMPARTPARAM FROM LCCUSTOMERIMPARTPARAMS A WHERE A.PATCHNO = '19055' AND A.IMPARTVER = 'GX17' AND A.IMPARTCODE = '12A' AND A.CUSTOMERNOTYPE = '1' AND A.IMPARTPARAMNO = '3' AND A.IMPARTPARAMNAME = 'Week' AND A.CONTNO = ?) WEEK FROM DUAL) LL WHERE (ROUND(LL.WEIGHT / POWER(DECODE(LL.HEIGHT,0,100000, LL.HEIGHT) / 100,2),0) < 11 OR ROUND(LL.WEIGHT / POWER(DECODE(LL.HEIGHT,0,100000, LL.HEIGHT) / 100,2),0) > 17) ";
		/**
		 * 6009意外险风险保额
		 */
		public static final String SUMACCIDENTAMOUNTSQL = "SELECT PRODRISKAMNT( ?, '6009', 'Y') as field1,6 as flag from DUAL ";

		/**
		 *万能险保费
		 */
		public static final String SUMStandPremSQL = "select nvl(sum(1000),0) as field1,7 as flag from lcpol where InsuredNo= ?  and AppntNo= ? and riskcode in ('5015') and Appflag in ('0', '1', '2','3') and (LCPol.Uwflag <> '1' and LCPol.Uwflag <> '2' and LCPol.Uwflag <> 'a')";
		/**
		 * 电销重疾保额
		 */
		//	public static final String HealthByElectricPinSql="select  HEALTHAMNT_SALECHNL( ?, '000000', '2', '5') as field1,8 as flag FROM DUAL";
		/**
		 * 电销渠道下非C012组合险份数
		 */
		//public static final String NUMOFNOTC012SQL = "SELECT COUNT(1) as field1,9 as flag  FROM LCPOL WHERE INSUREDNO = ? AND SALECHNL = '5' AND PRODSETCODE <> 'C012' AND APPFLAG IN ('0', '1', '2') AND UWFLAG NOT IN ('1', '2', 'a') AND NOT EXISTS (SELECT CONTNO FROM LCCONT WHERE (STATE LIKE '1002%' OR STATE LIKE '1003%' OR STATE LIKE '1005%') AND CONTNO = LCPOL.CONTNO)";

		/**
		 * 电销渠道下非C009组合险份数
		 */
		//public static final String NUMOFNOTC009SQL = "SELECT COUNT(1) as field1,10 as flag  FROM LCPOL WHERE INSUREDNO = ? AND SALECHNL = '5' AND PRODSETCODE <> 'C009' AND APPFLAG IN ('0', '1', '2') AND UWFLAG NOT IN ('1', '2', 'a') AND NOT EXISTS (SELECT CONTNO FROM LCCONT WHERE (STATE LIKE '1002%' OR STATE LIKE '1003%' OR STATE LIKE '1005%') AND CONTNO = LCPOL.CONTNO)";
		/**
		 * 爱永远定期寿险保费
		 */
		public static final String AMNTFor1012SQL="SELECT SUM(AMNT) as field1,11 as flag  FROM LCPOL WHERE InsuredNo = ? AND RISKCODE = '1012' AND APPFLAG IN ('0', '1', '2','3') AND UWFLAG NOT IN ('1', '2', 'a') and not exists (select contno from lccont where (state LIKE '004%' OR STATE LIKE '005%' OR STATE LIKE '006%') and contno=lcpol.contno)";
		/**
		 * 电销渠道下非C014组合险份数
		 */
		//public static final String NUMOFNOTC014SQL = "SELECT COUNT(1) as field1,12 as flag  FROM LCPOL WHERE INSUREDNO = ? AND SALECHNL = '5' AND PRODSETCODE <> 'C014' AND APPFLAG IN ('0', '1', '2') AND UWFLAG NOT IN ('1', '2', 'a') AND NOT EXISTS (SELECT CONTNO FROM LCCONT WHERE (STATE LIKE '1002%' OR STATE LIKE '1003%' OR STATE LIKE '1005%') AND CONTNO = LCPOL.CONTNO)";

		/**
		 * 摩托车驾照
		 */
		//public static final String HAVEMOTORCYCLELIENCE ="SELECT 1 as field1,13 as flag  FROM LCINSURED WHERE CONTNO = ? AND havemotorcyclelicence = '1' ";

		/**
		 * 被保人白名单标识SQL（insuredno：客户号，contno：投保单号）
		 */
		public static final String WHITESIGNSQL ="SELECT 1 as field1,14 as flag FROM lmwhitelist a , lcinsured b WHERE a.applystate = '2' AND a.wfwflag = '01' AND a.name = b.name AND a.idtype = b.idtype AND a.idno = b.idno AND a.birthday =b.birthday AND a.sex = b.sex AND b.insuredno = ? AND b.contno = ? ";

		/**
		 * 被保人白名单限额SQL（insuredno：客户号，contno：投保单号）
		 */
		public static final String WHITESIGNAMNTSQL ="SELECT amnt as field1,15 as flag FROM lmwhitelist a , lcinsured b WHERE a.applystate = '2' AND a.wfwflag = '01' AND a.name = b.name AND a.idtype = b.idtype AND a.idno = b.idno AND a.birthday =b.birthday AND a.sex = b.sex AND b.insuredno = ? AND b.contno = ? ";

		/**
		 * 被保人的团险渠道累计重疾保额
		 */
		public static final String HPRMOFSELLCHANL2 ="select CalGrpRiskAmnt( ? ,'000000','CQH',? )  as field1 ,  16 as flag  from dual ";

		/**
		 * 被保人的7054险种累计保额
		 */
		public static final String  PRM7054="select sum(amnt)  as field1 ,  17 as flag  FROM LCPOL  WHERE riskcode= '7054' AND insuredno = ? ";

		/**
		 * 7056险种累计投保额
		 */
		public static final String  PRM7056="select PRODRISKAMNT(?, '7056', 'M')  as field1 ,  20 as flag FROM LDSYSVAR WHERE SYSVAR = 'onerow' ";


		/**
		 *  团险渠道长期重疾险(7829)有拒保、延期记录
		 */
		public static final String REFUSERECODE7829="SELECT 1 as field1,18 as flag FROM LDSYSVAR WHERE SYSVAR = 'onerow'  AND (SELECT COUNT(1) FROM LCUWMASTER a,lcpol b  WHERE b.INSUREDNO = ? and a.contno = b.contno and b.riskcode = '7829' AND a.PASSFLAG IN ('1', '2')) + (SELECT COUNT(1) FROM LCCUWMASTER a,lcpol b WHERE b.INSUREDNO = ? and a.contno = b.contno and b.riskcode = '7829' AND a.PASSFLAG IN ('1', '2')) + (SELECT COUNT(1) FROM LLUWMASTER a,lcpol b WHERE b.INSUREDNO = ? and a.contno = b.contno and b.riskcode = '7829' AND a.PASSFLAG IN ('1', '2')) + (SELECT COUNT(1) FROM LLCUWMASTER a,lcpol b WHERE b.INSUREDNO = ? and a.contno = b.contno and b.riskcode = '7829' AND a.PASSFLAG IN ('1', '2')) + (SELECT COUNT(1) FROM LPUWMASTER a,lcpol b WHERE b.INSUREDNO = ? and a.contno = b.contno and b.riskcode = '7829' AND a.PASSFLAG IN ('1', '2')) + (SELECT COUNT(1) FROM LPCUWMASTER a,lcpol b  WHERE b.INSUREDNO = ? and a.contno = b.contno AND b.riskcode = '7829' AND a.PASSFLAG IN ('1', '2')) > 0";


		/**
		 * 被保人的团险渠道长期重疾险(7829)有次标准体记录
		 */
		public static final String SUBSTANDARY7829="SELECT 1 as field1,19 as flag FROM LDSYSVAR WHERE SYSVAR = 'onerow'  AND (SELECT COUNT(1) FROM LCUWMASTER a,lcpol b  WHERE b.INSUREDNO = ? and a.contno = b.contno and b.riskcode = '7829' AND a.PASSFLAG IN ('4')) + (SELECT COUNT(1) FROM LCCUWMASTER a,lcpol b WHERE b.INSUREDNO = ? and a.contno = b.contno and b.riskcode = '7829' AND a.PASSFLAG IN ('4')) + (SELECT COUNT(1) FROM LLUWMASTER a,lcpol b WHERE b.INSUREDNO = ? and a.contno = b.contno and b.riskcode = '7829' AND a.PASSFLAG IN ('4')) + (SELECT COUNT(1) FROM LLCUWMASTER a,lcpol b WHERE b.INSUREDNO = ? and a.contno = b.contno and b.riskcode = '7829' AND a.PASSFLAG IN ('4')) + (SELECT COUNT(1) FROM LPUWMASTER a,lcpol b WHERE b.INSUREDNO = ? and a.contno = b.contno and b.riskcode = '7829' AND a.PASSFLAG IN ('4')) + (SELECT COUNT(1) FROM LPCUWMASTER a,lcpol b  WHERE b.INSUREDNO = ? and a.contno = b.contno AND b.riskcode = '7829' AND a.PASSFLAG IN ('4')) > 0";


		/**
		 * 7054,被保人其他保险公司保额
		 */
		public static final String OTHERAMNTSQL="SELECT MAX(otheramnt) as field1,21 as flag FROM lcpol WHERE insuredno = ? AND contno =? ";

		/**
		 * 黑名单1-证件号与和黑名单相同,不分国籍,>0触发,参数:一个contno
		 * {this} 的证件号与和黑名单相同
		 */
		public static final String INSBLACKIDFLAG = "select count(1) as field1,22 as flag from LXBlackListIndv a, LCINSURED b, lxidtypeorno c where  b.contno = ? and c.blacklistid = a.blacklistid and b.idno = c.idno";

		/**
		 * xxx黑名单2-中国客户名字、生日与黑名单相同,>0触发,参数:一个contno
		 * {this}为中国客户且名字和生日与黑名单相同
		 */
		public static final String INSBLACKCHNFLAG = "select field1, 23 as flag from (select count(distinct(count(e.blacklistid))) as field1 from LXBlackListIndv a, LCINSURED b, lxblackdate e where  b.contno = ? and a.blacklistid = e.blacklistid and a.name = b.name and b.nativeplace = 'CHN' group by e.blacklistid, e.blackdate, b.birthday having (select case when ((select count(1) from dual where (select count(1) from lxblackdate p where p.blacklistid = e.blacklistid) >= 1 and length(e.blackdate) = 4 and e.blackdate = substr(to_char(b.birthday, 'yyyy-mm-dd'), 1, 4))) > 0 then 1 else case when to_char(b.birthday,'yyyy-mm-dd') >= (select min(blackdate) from lxblackdate where blacklistid = e.blacklistid) and to_char(b.birthday,'yyyy-mm-dd') <= (select max(blackdate) from lxblackdate where blacklistid = e.blacklistid) then 1 else 0 end end from dual) > 0) where rownum = 1 ";

		/**
		 * XXX黑名单3-被保人名1部分组成,被保人名与黑名单表四个名字对比,>0触发,参数:4个contno
		 * {this}的名字与黑名单名字和生日相同(名字为一部分)
		 */
		public static final String INSBLACKFLAG1 = "SELECT (case when (select firsttab.num from (select count(distinct(e.blacklistid)) as num from LXBlackListIndv a, LCINSURED b, lxblackdate e where  b.contno = ? and e.blacklistid = a.blacklistid and instr(b.name, ' ') = 0 and a.firstname = b.name and (b.nativeplace is null or b.nativeplace <> 'CHN') group by e.blacklistid, e.blackdate, b.birthday having (select case when ((select count(1) from dual where (select count(1) from lxblackdate p where p.blacklistid = e.blacklistid) >= 1 and length(e.blackdate) = 4 and e.blackdate = substr(to_char(b.birthday, 'yyyy-mm-dd'), 1, 4))) > 0 then 1 else case when to_char(b.birthday, 'yyyy-mm-dd') >= (select min(blackdate) from lxblackdate where blacklistid = e.blacklistid) and to_char(b.birthday, 'yyyy-mm-dd') <= (select max(blackdate) from lxblackdate where blacklistid = e.blacklistid) then 1 else 0 end end from dual) > 0) firsttab where rownum = 1) > 0 then 1 else 0 end + case when (select middletab.num from (select count(distinct(e.blacklistid)) as num from LXBlackListIndv a, LCINSURED b, lxblackdate e where  b.contno = ? and e.blacklistid = a.blacklistid and instr(b.name, ' ') = 0 and a.middlename = b.name and (b.nativeplace is null or b.nativeplace <> 'CHN') group by e.blacklistid, e.blackdate, b.birthday having (select case when ((select count(1) from dual where (select count(1) from lxblackdate p where p.blacklistid = e.blacklistid) >= 1 and length(e.blackdate) = 4 and e.blackdate = substr(to_char(b.birthday, 'yyyy-mm-dd'), 1, 4))) > 0 then 1 else case when to_char(b.birthday, 'yyyy-mm-dd') >= (select min(blackdate) from lxblackdate where blacklistid = e.blacklistid) and to_char(b.birthday, 'yyyy-mm-dd') <= (select max(blackdate) from lxblackdate where blacklistid = e.blacklistid) then 1 else 0 end end from dual) > 0) middletab where rownum = 1) > 0 then 1 else 0 end + case when (select surntab.num from (select count(distinct(e.blacklistid)) as num from LXBlackListIndv a, LCINSURED b, lxblackdate e where  b.contno = ? and e.blacklistid = a.blacklistid and instr(b.name, ' ') = 0 and a.surname = b.name and (b.nativeplace is null or b.nativeplace <> 'CHN') group by e.blacklistid, e.blackdate, b.birthday having (select case when ((select count(1) from dual where (select count(1) from lxblackdate p where p.blacklistid = e.blacklistid) >= 1 and length(e.blackdate) = 4 and e.blackdate = substr(to_char(b.birthday, 'yyyy-mm-dd'), 1, 4))) > 0 then 1 else case when to_char(b.birthday, 'yyyy-mm-dd') >= (select min(blackdate) from lxblackdate where blacklistid = e.blacklistid) and to_char(b.birthday, 'yyyy-mm-dd') <= (select max(blackdate) from lxblackdate where blacklistid = e.blacklistid) then 1 else 0 end end from dual) > 0) surntab where rownum = 1) > 0 then 1 else 0 end + case when (select nametab.num from (select count(distinct(e.blacklistid)) as num from LXBlackListIndv a, LCINSURED b, lxblackdate e where  b.contno = ? and e.blacklistid = a.blacklistid and instr(b.name, ' ') = 0 and a.name = b.name and (b.nativeplace is null or b.nativeplace <> 'CHN') group by e.blacklistid, e.blackdate, b.birthday having (select case when ((select count(1) from dual where (select count(1) from lxblackdate p where p.blacklistid = e.blacklistid) >= 1 and length(e.blackdate) = 4 and e.blackdate = substr(to_char(b.birthday, 'yyyy-mm-dd'), 1, 4))) > 0 then 1 else case when to_char(b.birthday, 'yyyy-mm-dd') >= (select min(blackdate) from lxblackdate where blacklistid = e.blacklistid) and to_char(b.birthday, 'yyyy-mm-dd') <= (select max(blackdate) from lxblackdate where blacklistid = e.blacklistid) then 1 else 0 end end from dual) > 0) nametab where rownum = 1) > 0 then 1 else 0 end) as field1, 24 as flag from dual ";

		/**
		 * xxx黑名单4-被保人名2部分组成,被保人名与黑名单表2个名字对比,>0触发,参数:2个contno
		 * 有一个name相同，且出生日期相同
		 * {this}的名字与黑名单名字和生日相同(名字为两部分)
		 */
		public static final String INSBLACKFLAG2 = "select (case when (select firsttab.num from (select count(distinct(e.blacklistid)) as num from LXBlackListIndv a, LCINSURED b, lxblackdate e where  b.contno = ? and e.blacklistid = a.blacklistid and length(b.name) - length(replace(b.name, ' ')) = 1 and a.firstname = substr(b.name, 1, instr(b.name, ' ', 1) - 1) and (b.nativeplace is null or b.nativeplace <> 'CHN') group by e.blacklistid, e.blackdate, b.birthday having (select case when ((select count(1) from dual where (select count(1) from lxblackdate p where p.blacklistid = e.blacklistid) >= 1 and length(e.blackdate) = 4 and e.blackdate = substr(to_char(b.birthday, 'yyyy-mm-dd'), 1, 4))) > 0 then 1 else case when to_char(b.birthday, 'yyyy-mm-dd') >= (select min(blackdate) from lxblackdate where blacklistid = e.blacklistid) and to_char(b.birthday, 'yyyy-mm-dd') <= (select max(blackdate) from lxblackdate where blacklistid = e.blacklistid) then 1 else 0 end end from dual) > 0) firsttab where rownum = 1) > 0 then 1 else 0 end + case when (select surntab.num from (select count(distinct(e.blacklistid)) as num from LXBlackListIndv a, LCINSURED b, lxblackdate e where  b.contno = ? and e.blacklistid = a.blacklistid and length(b.name) - length(replace(b.name, ' ')) = 1 and a.surname = substr(b.name, instr(b.name, ' ', 1) + 1, length(b.name)) and (b.nativeplace is null or b.nativeplace <> 'CHN') group by e.blacklistid, e.blackdate, b.birthday having (select case when ((select count(1) from dual where (select count(1) from lxblackdate p where p.blacklistid = e.blacklistid) >= 1 and length(e.blackdate) = 4 and e.blackdate = substr(to_char(b.birthday, 'yyyy-mm-dd'), 1, 4))) > 0 then 1 else case when to_char(b.birthday, 'yyyy-mm-dd') >= (select min(blackdate) from lxblackdate where blacklistid = e.blacklistid) and to_char(b.birthday, 'yyyy-mm-dd') <= (select max(blackdate) from lxblackdate where blacklistid = e.blacklistid) then 1 else 0 end end from dual) > 0) surntab where rownum = 1) > 0 then 1 else 0 end) as field1, 25 as flag from dual ";

		/**
		 * 黑名单5-被保人名2部分组成,被保人名与黑名单表2个名字对比,>=2触发,参数:2个contno
		 * firstname和surnname都要相同.无生日限制
		 * {this} 的名字与黑名单名字和生日相同(名字为两部分,无生日限制)
		 */
		public static final String INSBLACKFLAG3 = "select ((select count(1) from LXBlackListIndv a, LCINSURED b where  b.contno = ? and length(b.name) - length(replace(b.name, ' ')) = 1 and a.firstname =substr(b.name ,1, instr(b.name, ' ', 1)-1) and (b.nativeplace is null or b.nativeplace <> 'CHN')) + (select count(1) from LXBlackListIndv a, LCINSURED b where  b.contno = ? and length(b.name) - length(replace(b.name, ' ')) = 1 and a.surname = substr(b.name ,instr(b.name, ' ', 1)+1,length(b.name)) and (b.nativeplace is null or b.nativeplace <> 'CHN'))) as field1,26 as flag from dual";

		/**
		 * xxx黑名单6-被保人名>=3部分组成,被保人名与黑名单表>=3个名字对比,>=0触发,参数:3个contno
		 * firstname,middlename,surname,有生日限制
		 * {this} 的名字与黑名单名字两个以上相同(名字为四部分)
		 */
		public static final String INSBLACKFLAG4 = "SELECT (CASE WHEN (select firsttab.num from (SELECT count(distinct(e.blacklistid)) as num FROM LXBlackListIndv a, LCINSURED b, lxblackdate e WHERE  b.contno = ? AND e.blacklistid = a.blacklistid AND (length(b.name) - length(replace(b.name, ' '))) >= 2 AND a.firstname = substr(b.name, 1, instr(b.name, ' ') - 1) AND (b.nativeplace IS NULL OR b.nativeplace <> 'CHN') GROUP BY e.blacklistid, e.blackdate, b.birthday HAVING (SELECT CASE WHEN ((SELECT count(1) FROM dual WHERE (SELECT count(1) FROM lxblackdate p WHERE p.blacklistid = e.blacklistid) >= 1 AND length(e.blackdate) = 4 AND e.blackdate = substr(to_char(b.birthday, 'yyyy-mm-dd'), 1, 4))) > 0 THEN 1 ELSE CASE WHEN to_char(b.birthday, 'yyyy-mm-dd') >= (select min(blackdate) from lxblackdate where blacklistid = e.blacklistid) and to_char(b.birthday, 'yyyy-mm-dd') <= (select max(blackdate) from lxblackdate where blacklistid = e.blacklistid) THEN 1 ELSE 0 END END FROM dual) > 0) firsttab where rownum = 1) > 0 THEN 1 ELSE 0 END + CASE WHEN (select middletab.num from (SELECT count(distinct(e.blacklistid)) as num FROM LXBlackListIndv a, LCINSURED b, lxblackdate e WHERE  b.contno = ? AND e.blacklistid = a.blacklistid AND (length(b.name) - length(replace(b.name, ' '))) >= 2 AND a.middlename = substr(b.name, instr(b.name, ' ') + 1, (instr(b.name, ' ', 1, 2)) - (instr(b.name, ' ') + 1)) AND (b.nativeplace IS NULL OR b.nativeplace <> 'CHN') GROUP BY e.blacklistid, e.blackdate, b.birthday HAVING (SELECT CASE WHEN ((SELECT count(1) FROM dual WHERE (SELECT count(1) FROM lxblackdate p WHERE p.blacklistid = e.blacklistid) >= 1 AND length(e.blackdate) = 4 AND e.blackdate = substr(to_char(b.birthday, 'yyyy-mm-dd'), 1, 4))) > 0 THEN 1 ELSE CASE WHEN to_char(b.birthday, 'yyyy-mm-dd') >= (select min(blackdate) from lxblackdate where blacklistid = e.blacklistid) and to_char(b.birthday, 'yyyy-mm-dd') <= (select max(blackdate) from lxblackdate where blacklistid = e.blacklistid) THEN 1 ELSE 0 END END FROM dual) > 0) middletab where rownum = 1) > 0 THEN 1 ELSE 0 END + CASE WHEN (select surntab.num from (SELECT count(distinct(e.blacklistid)) as num FROM LXBlackListIndv a, LCINSURED b, lxblackdate e WHERE  b.contno = ? AND e.blacklistid = a.blacklistid AND (length(b.name) - length(replace(b.name, ' '))) >= 2 AND a.surname = substr(b.name, instr(b.name, ' ', 1, 2) + 1) AND (b.nativeplace IS NULL OR b.nativeplace <> 'CHN') GROUP BY e.blacklistid, e.blackdate, b.birthday HAVING (SELECT CASE WHEN ((SELECT count(1) FROM dual WHERE (SELECT count(1) FROM lxblackdate p WHERE p.blacklistid = e.blacklistid) >= 1 AND length(e.blackdate) = 4 AND e.blackdate = substr(to_char(b.birthday, 'yyyy-mm-dd'), 1, 4))) > 0 THEN 1 ELSE CASE WHEN to_char(b.birthday, 'yyyy-mm-dd') >= (select min(blackdate) from lxblackdate where blacklistid = e.blacklistid) and to_char(b.birthday, 'yyyy-mm-dd') <= (select max(blackdate) from lxblackdate where blacklistid = e.blacklistid) THEN 1 ELSE 0 END END FROM dual) > 0) surntab where rownum = 1)> 0 THEN 1 ELSE 0 END) AS field1, 27 AS flag FROM dual ";

		/**
		 * xxxxxx黑名单7-被保人名>=3部分组成,被保人名与黑名单表>=3个名字对比,>=2触发,参数:3个contno
		 * firstname,middlename,surname,无生日限制
		 * {this} 的名字与黑名单相同(被保人名三部分构成无生日限制)
		 */
		public static final String INSBLACKFLAG5="SELECT ( (select case when (SELECT count(1) FROM LXBlackListIndv a, LCINSURED b WHERE  b.contno = ? AND (length(b.name) - length(replace(b.name, ' '))) >= 2 AND a.firstname = substr(b.name, 1, instr(b.name, ' ') - 1) AND (b.nativeplace IS NULL OR b.nativeplace <> 'CHN')) > 0 then 1 else 0 end from dual ) + (select case when (SELECT count(1) FROM LXBlackListIndv a, LCINSURED b WHERE  b.contno = ? AND (length(b.name) - length(replace(b.name, ' '))) >= 2 AND a.middlename = substr(b.name, instr(b.name, ' ') + 1, (instr(b.name, ' ', 1, 2)) - (instr(b.name, ' ') + 1)) AND (b.nativeplace IS NULL OR b.nativeplace <> 'CHN')) > 0 then 1 else 0 end from dual ) + (select case when (SELECT count(1) FROM LXBlackListIndv a, LCINSURED b WHERE  b.contno = ? AND (length(b.name) - length(replace(b.name, ' '))) >= 2 AND a.surname = substr(b.name, instr(b.name, ' ', 1, 2) + 1) AND (b.nativeplace IS NULL OR b.nativeplace <> 'CHN')) > 0 then 1 else 0 end from dual ) ) AS field1, 28 AS flag FROM dual ";

		/**
		 * 被保人-同业拒保次数
		 */
		public static final String DECLINEFLAG = "select sum(refusetimes) as field1,29 as flag  from lcsdriskquery where busstype='QY' AND customerno=? group by customerno";

		/**
		 * 被保人-同业投保次数
		 */
		public static final String APPLICATIONEFLAG = "select sum(CompanyQuantity) as field1,30 as flag from lcsdriskquery where busstype='QY' AND customerno=? group by customerno";


		/**
		 * 被保人针对7059险种累计基本保额
		 */
		public static final String BASICAMNT7059SQL = "select BASICRISKAMNT(?,'7059') as field1 ,31 as flag from dual";


		/**
		 * 被保险人防癌、重疾理赔记录，filed1>0，flag设置true
		 */
		//public static final String CANCERCLAIMRECORDSQL = "select count(tag) as filed1 , 32 as flag from (SELECT 1 as tag FROM ljagetclaim t WHERE t.kindcode='S' AND EXISTS (SELECT 1 FROM llsubreport s WHERE s.subrptno = t.otherno AND s.customerno = ? ) UNION ALL SELECT 1 as tag FROM ljagetclaim t WHERE t.kindcode='S' AND EXISTS (SELECT 1 FROM llsubreport s WHERE s.caseno = t.otherno AND s.customerno = ? )) tab ";
		//public static final String CANCERCLAIMRECORDSQL = "select 1 as filed1, 32 as flag from dual where (select count(1) from llclaimdetail a where a.contno in (select lc.contno from lccont lc where lc.insuredno = ?) and a.kindcode = 'S') <> 0 or (select count(1) from llclaimdetail b where b.contno in (select lp.contno from lccont lp where lp.insuredno = ?) and b.kindcode = 'S') <> 0 ";
		public static final String CANCERCLAIMRECORDSQL = "select 1 as filed1, 32 as flag from dual where (select count(1) from llclaimreportdetail a where a.contno in  (select lc.contno from lccont lc where lc.insuredno = ?) and a.kindcode = 'S') <> 0 or (select count(1) from llclaimdetail b where b.contno in  (select lp.contno from lccont lp where lp.insuredno = ?) and b.kindcode = 'S') <> 0 ";

		/**
		 * 被保人意外险理赔记录,，filed1>0，flag设置true
		 */
		//public static final String ACCIDENTCLAIMRECORDSQL = "SELECT COUNT(tag) AS field1, 33 AS flag FROM (SELECT 1 AS tag FROM ljagetclaim t WHERE t.kindcode='A' AND EXISTS (SELECT 1 FROM llsubreport s WHERE s.subrptno = t.otherno AND s.customerno = ? ) UNION ALL SELECT 1 AS tag FROM ljagetclaim t WHERE t.kindcode='A' AND EXISTS (SELECT 1 FROM llsubreport s WHERE s.caseno = t.otherno AND s.customerno = ? ) ) tab ";
		//public static final String ACCIDENTCLAIMRECORDSQL = "select 1 as filed1, 33 as flag from dual where (select count(1) from llclaimdetail a where a.contno in (select lc.contno from lccont lc where lc.insuredno = ?) and a.kindcode = 'A') <> 0 or (select count(1) from llclaimdetail b where b.contno in (select lt.contno from lccont lt where lt.insuredno = ?) and b.kindcode = 'A') <> 0 ";
		public static final String ACCIDENTCLAIMRECORDSQL = "SELECT 1 AS filed1, 33  AS flag FROM dual WHERE (SELECT COUNT(1)  FROM llsubreport a,  llreport b WHERE a.subrptno  = b.rptno AND a.customerno   = ? AND b.AccidentReason='1') <> 0 OR (SELECT COUNT(1) FROM llclaimdetail c WHERE c.contno IN (SELECT lt.contno FROM lccont lt WHERE lt.insuredno = ? ) AND c.kindcode = 'A') <> 0 ";

		/**
		 * 被保人7057险种累计基本保额
		 */
		public static final String BASICAMNT7057SQL ="select BASICRISKAMNT(?,'7057') as field1 ,34 as flag from dual";

		/**
		 * 被保人7058险种累计基本保额
		 */
		public static final String BASICAMNT7058SQL ="select BASICRISKAMNT(?,'7058') as field1 ,35 as flag from dual";


		/**
		 * 被保人1030险种累计基本保额
		 */
		public static final String BASICAMNT1030SQL ="select BASICRISKAMNT(?,'1030') as field1 ,36 as flag from dual";

		/**
		 * 被保人2048险种累计基本保额
		 */
		public static final String BASICAMNT2048SQL ="select BASICRISKAMNT(?,'2048') as field1 ,37 as flag from dual";

		/**
		 * 被保人2048累计保费
		 */
		public static final String ACCPREM2048SQL = "select (select sum(prem) from lcpol where insuredno = ? and riskcode = '2048' and appflag in ('0', '1', '2','3') and uwflag not in ('1', '2', 'a') and not exists(select contno from lccont where (state LIKE '004%' OR STATE LIKE '005%' OR STATE LIKE '006%') and contno = lcpol.contno)) as filed1,38 as flag from dual";

		/**
		 *被保险人C060保险计划累计保额
		 */
		public static final String C060ACCAMNTSQL="select sum(amnt) as field1 ,39 as flag  from lcpol where insuredno = ?  and riskcode = '7053' and appflag in ('0', '1', '2','3')  and uwflag not in ('1', '2', 'a')  and prodsetcode = 'C060' and not exists (select contno   from lccont  where (state LIKE '004%' OR STATE LIKE '005%' OR STATE LIKE '006%')   and contno = lcpol.contno)";
		/**
		 * 被保险人累计C060份数
		 */
		public static final String C060COPIESSQL="select count(1) as field1, 40 as flag from lcpol where insuredno = ? and riskcode = '7053' and appflag in ('0', '1', '2','3') and uwflag not in ('1', '2', 'a') and prodsetcode = 'C060' and not exists (select contno from lccont where (state LIKE '004%' OR STATE LIKE '005%' OR STATE LIKE '006%') and contno = lcpol.contno)";
		/**
		 * 被保险人投保7053累计重疾险风险保额
		 */
		public static final String ACCAMNT7053SQL="SELECT PRODRISKAMNT(?, '7053', 'H') as field1 ,41 as flag  FROM LDSYSVAR WHERE SYSVAR = 'onerow'";
		/**
		 *  非工作单位组织的个人投保标记
		 */
		public static  final  String PERSONALINSURANCESQL ="SELECT to_number(nwuinsurance) as field1,44 as flag FROM lctaxcont where contno = ? ";
		/**
		 *
		 */
		public static final String ORGANIZATIONINSURANCESQL="select to_number(wuinsurance)as field1,45 as flag FROM  lctaxcont where contno = ? ";
		/**
		 * 被保险人是否已拥有7060或7061的有效保单 2018/3/27 zfx（非续期）
		 */
		//public static final String EFFECTIVE7060POLICYSQL = "SELECT 1 as field1, 46 as flag FROM dual WHERE EXISTS (SELECT 1 FROM lcpol a WHERE a.insuredno = ? AND a.riskcode IN ('7060', '7061') AND a.appflag IN ('0', '1') AND a.contno <> ? AND a.uwflag NOT IN ('1', '2', 'a') AND NOT EXISTS (SELECT 1 FROM lccont b WHERE (b.state LIKE '1002%' OR b.state LIKE '1003%' OR b.state LIKE '1005%') AND a.contno = b.CONTNO))";
		//public static final String EFFECTIVE7060POLICYSQL ="SELECT 1 as field1, 46 as flag FROM dual WHERE EXISTS (SELECT 1 FROM lcpol WHERE insuredno = ? AND riskcode IN ('7060', '7061') AND appflag in ('0', '1', '2') and contno <> ? and uwflag not in ('1', '2', 'a') union SELECT 1 FROM lcpol WHERE insuredno = ? AND riskcode IN ('7060', '7061') AND appflag in ('0', '1', '2') and contno <> ? and uwflag not in ('1', '2', 'a') ) ";
		public static final String EFFECTIVE7060POLICYSQL2 = "SELECT 1 as field1, 46 as flag FROM dual WHERE EXISTS (SELECT 1 FROM lcpol a WHERE a.insuredno = ? AND a.riskcode IN ('7060', '7061') AND a.appflag IN ('0', '1','2','3') AND  a.uwflag NOT IN ('1', '2', 'a') AND NOT EXISTS (SELECT 1 FROM lccont b WHERE (b.state LIKE '004%' OR b.state LIKE '005%' OR b.state LIKE '006%') AND a.contno = b.CONTNO))";
		/**
		 * 被保险人是否已拥有7060或7061的有效保单 2018/3/27 zfx（续期）
		 */
		public static final String EFFECTIVE7060POLICYSQL1 ="SELECT 1 as field1, 46 as flag FROM dual WHERE EXISTS (SELECT 1 FROM lcpol a WHERE a.insuredno = ? AND a.riskcode IN ('7060', '7061') AND a.appflag IN ('0', '1','2','3') AND a.contno <> ? AND a.uwflag NOT IN ('1', '2', 'a') AND NOT EXISTS (SELECT 1 FROM lccont b WHERE (b.state LIKE '004%' OR b.state LIKE '005%' OR b.state LIKE '006%') AND a.contno = b.CONTNO))";
		/**
		 * 被保险人既往有非团险账户的医疗险种的理赔记录 2018/3/27 zfx
		 */
		public static final String HAVECLAIMSNOTMEDICSQL = "SELECT 1 as field1, 47 as flag FROM DUAL WHERE ( SELECT COUNT (*) FROM llregister A WHERE CUSTOMERNO = ? AND clmstate >= 60 AND NOT EXISTS (SELECT 1 FROM ltclaimfee WHERE rgtno = A .rgtno)) <> 0 OR (SELECT COUNT (*)FROM LLSubReport b WHERE CUSTOMERNO = ?) <> 0 ";
		/**
		 * C066组合规则 险种各期保费之和 2018/5/31 xzh
		 */
		public static final String RENEWALPREMSUMC066SQL = "select  sum(case  when payintv = 0 then prem else prem * payendyear end)as field1,48 as flag  from lcpol where contno=? and prodsetcode='C066'";

		/**
		 * C070保险计划累计保额 2018/5/31 xzh
		 */
		public static final String MAXIMUMGRANTSQL = "select sum(amnt)as field1,49 as flag from lcpol where insuredno= ? and prodsetcode='C070' and appflag in ('2','1','1','3') and uwflag not in ('1','2','a')";
		/**
		 * 被保险人累计C070份数 2018/5/31 xzh
		 */
		public static final String C070COPIESSQL = "select count(1)as field1,50 as flag from lcpol where insuredno= ? and prodsetcode='C070' ";
		/**
		 * 被保人意外医疗险累计理赔结案给付金额    2018/6/19 xzh
		 */
		//public static final String ACCIDENTMEDICPAYSQL = "select (sum(c.pay) + sum(d.pay)) as field1, 51 as flag from (SELECT a.pay FROM ljagetclaim a WHERE a.getdutykind = '100' and a.otherno in (SELECT lg.rgtno FROM llregister lg WHERE lg.clmstate = '60' and lg.customerno = ?)) c, (SELECT b.pay FROM ljagetclaim b WHERE b.getdutykind = '100' and b.otherno in (SELECT lgr.rgtno FROM llregister lgr WHERE lgr.clmstate > '50' and lgr.customerno = ?)) d ";
		public static final String ACCIDENTMEDICPAYSQL =" SELECT SUM(c.pay) AS field1, 51 AS flag FROM (SELECT a.pay FROM ljagetclaim a, llregister b WHERE a.getdutykind = '100' AND a.otherno  =b.rgtno AND b.clmstate  IN('50','60') AND otherno  IN (SELECT caseno FROM llcase b WHERE customerno = ? ) ) c ";
		/**
		 * 被保人健医卡理赔给付金额     2018/6/19 xzh
		 */
		public static final String HELTHMEDICPAYSQL = "select sum(pay) as field1, 52 as flag from LJAGetClaim l where l.riskcode = '7824' and l.contno in (select contno from lccont where insuredno = ?)";

		/**
		 * 被保人非健医卡和意外医疗险的理赔记录   2018/6/19 xzh
		 */
		//public static final String CLAIMSNOMEDICSQL = " SELECT 1 as field1, 53 as flag FROM DUAL WHERE (SELECT count(*) FROM LLSubReport b WHERE b.customerno = ? and not exists (SELECT 1 FROM llclaimreportdetail a WHERE a.customerno = b.customerno and (a.clmno = b.caseno or a.clmno = b.SUBRPTNO) and (a.getdutykind = '100' or a.riskcode = '7824'))) <> 0 OR (SELECT count(*) FROM llcase b WHERE b.customerno = ? and not exists (SELECT 1 FROM llclaimreportdetail a WHERE a.clmno = b.caseno and (a.getdutykind = '100' or a.riskcode = '7824'))) <> 0 OR (SELECT count(*) FROM LLSubReport b WHERE b.customerno = ? AND b.subrptno NOT LIKE 'BA%' AND b.subrptno NOT LIKE 'LP%' and not exists (SELECT 1 FROM llestimate a WHERE a.customerno = b.customerno and (a.rptno = b.caseno or a.rptno = b.SUBRPTNO) AND (EXISTS (SELECT 1 from lmdutygetclm m WHERE m.getdutycode = a.getdutycode AND m.getdutykind = '100') or a.riskcode = '7824')))<>0 OR (SELECT count(*) FROM LLRegister b WHERE b.clmstate > '10' and b.customerno = ? AND b.RGTNO NOT LIKE 'LP%' and not exists (SELECT 1 FROM llclaimdetail a WHERE a.customerno = b.customerno and a.clmno = b.RGTNO and (a.getdutykind = '100' or a.riskcode = '7824')))<>0  ";
		public static final String CLAIMSNOMEDICSQL =" SELECT 1 as field1, 53 as flag FROM DUAL WHERE \n" +
				"                 (SELECT count(*) FROM LLSubReport b WHERE b.customerno = ? and not exists (SELECT 1 FROM llclaimreportdetail a WHERE a.customerno = b.customerno and (a.clmno = b.caseno or a.clmno = b.SUBRPTNO) and (a.getdutykind = '100' or a.riskcode = '7824'))) <> 0\n" +
				"                 OR (SELECT count(*) FROM llcase b WHERE b.customerno = ? and not exists (SELECT 1 FROM llclaimreportdetail a WHERE a.clmno = b.caseno and (a.getdutykind = '100' or a.riskcode = '7824'))) <> 0 \n" +
				"                 OR (SELECT count(*) FROM LLSubReport b WHERE b.customerno = ? AND b.subrptno NOT LIKE 'BA%' AND b.subrptno NOT LIKE 'LP%' and not exists (SELECT 1 FROM llestimate a WHERE a.customerno = b.customerno and (a.rptno = b.caseno or a.rptno = b.SUBRPTNO) AND (EXISTS (SELECT 1 from lmdutygetclm m WHERE m.getdutycode = a.getdutycode AND m.getdutykind = '100') or a.riskcode = '7824')))<>0\n" +
				"                 OR (SELECT count(*) FROM LLRegister b WHERE b.clmstate > '10' and b.customerno = ? AND b.RGTNO NOT LIKE 'LP%' and not exists (SELECT 1 FROM llclaimdetail a WHERE a.customerno = b.customerno and a.clmno = b.RGTNO and (a.getdutykind = '100' or a.riskcode = '7824')))<>0  ";
		/**
		 * 财富世嘉终身寿险保险累计基本保险金额  2018/7/20 xzh
		 */
		public static final String SUMBASICINSURANCEAMOUNTSQL="select nvl(sum(Amnt),0) as field1,54 as flag from lcpol where insuredno= ? and riskcode='1040' and appflag in ('0','1','2','3') and uwflag not in ('1','2','a') and not exists (select contno from lccont where (state LIKE '004%' OR STATE LIKE '005%' OR STATE LIKE '006%') and contno=lcpol.contno)";
		/**
		 * 1040 险种各期保费之和    2018/7/20 xzh
		 */
		public static final String RENEWALPREMSUM1040SQL = "select sum(case  when payintv = 0 then prem else prem * payendyear end) as field1,55 as flag   from lcpol where contno=? and prodsetcode='1040'";
		/**
		 * C071 累计重疾保额   2018/7/20 xzh
		 */
		public static final String C071LEIJIZHONGJI = "select sum(amnt) as field1 ,56 as flag1 from lcpol where riskcode = '7063' and prodsetcode = 'C071' and insuredno = ?";

		/**
		 * 既往有未结案的意外医疗理赔记录   2018/8/29 xzh
		 */
		public static final String ACMEDICCLAIMNOTEND ="SELECT 1 AS field1, 57 AS flag FROM DUAL WHERE (SELECT COUNT(*) FROM llreport a , LLReportReason b WHERE a.rptno =b.rpno AND b.reasoncode='100' AND a.RgtFlag <>'15' AND a.rptno IN (SELECT subrptno FROM llsubreport WHERE customerno=? ) AND (NOT EXISTS (SELECT 1 FROM llregister WHERE rgtobjno=a.rptno ) OR EXISTS (SELECT 1 FROM llregister WHERE rgtobjno =a.rptno AND clmstate NOT IN('50','60','70') ))) <> 0 OR (SELECT COUNT(*) FROM LLSubReport b WHERE b.customerno = ? AND NOT EXISTS (SELECT 1 FROM LLRegister l WHERE (l.RGTNO = b.SUBRPTNO OR l.RGTNO = b.caseno) ) AND EXISTS (SELECT 1 FROM llestimate a WHERE a.customerno = b.customerno AND (a.rptno = b.caseno OR a.rptno = b.SUBRPTNO) AND EXISTS (SELECT 1 FROM lmdutygetclm m WHERE m.getdutycode = a.getdutycode AND m.getdutykind = '100' ) ))<>0 OR (SELECT COUNT(*) FROM LLSubReport b WHERE b.customerno = ? AND EXISTS (SELECT 1 FROM LLRegister l WHERE l.clmstate < '50' AND (l.RGTNO = b.SUBRPTNO OR l.RGTNO = b.caseno) ) AND EXISTS (SELECT 1 FROM llestimate a WHERE a.customerno = b.customerno AND (a.rptno = b.caseno OR a.rptno = b.SUBRPTNO) AND EXISTS (SELECT 1 FROM lmdutygetclm m WHERE m.getdutycode = a.getdutycode AND m.getdutykind = '100' ) ))<>0 OR (SELECT COUNT(*) FROM LLRegister b WHERE b.clmstate < '50' AND customerno = ? AND EXISTS (SELECT 1 FROM llclaimdetail a WHERE a.customerno = b.customerno AND a.clmno = b.RGTNO AND a.getdutykind = '100' )) <>0 ";

		/**
		 * 被保险人既往有"被'达标体检、抽样体检'自核规则拦截且该保单状态为待人工核保"的保单  2018/9/13 xzh
		 */
		//public static final String HAVEHELTHRULESQL="SELECT 1 AS field1, 58 AS flag FROM dual WHERE(  select count(1) from lbcuwerror a where a.insuredno = ? and a.contno <> ? and  ((a.uwerror like '%体检%' and a.uwrulecode not in ('V12GXU263', 'V12YBT174', 'V12CFT194', 'V12GXU079', 'V12GXU206','V1200U200','V1200U104','V12IMC010','V12SY0011', 'V12YDU105','V1200U101','V12SY0013', 'V12GXU255','V12HMU072', 'V12HMU073', 'V12HMU067','V12GXU256', 'V12YBT002', 'V175018031','V126009042','V187061033','V187060027')) or a.uwrulecode = 'V12RC0001') and exists  (select t.missionprop1  from lwmission t, lccont c where 1 = 1 and a.contno = c.contno and t.activityid in ('0000001100') and c.conttype = '1'  and (c.prtno = t.missionprop1 or c.contno = t.missionprop1) and t.activitystatus in ('1', '6', '7') and t.MissionProp10 like '86%' and (t.MissionProp11 = '0' or (t.MissionProp12 <= 'D' and t.MissionProp11 in ('1', '2', '3', '4'))) AND NOT EXISTS (SELECT 1 FROM lccont d WHERE (d.state LIKE '1002%' OR d.state LIKE '1003%' OR d.state LIKE '1005%') AND d.contno = c.CONTNO))) <>0   or (select count(1) from lcuwerror a where a.insuredno = ? and a.contno <> ? and  ((a.uwerror like '%体检%' and a.uwrulecode not in ('V12GXU263','V12YBT174','V12CFT194','V12GXU079','V12GXU206', 'V1200U200','V1200U104', 'V12IMC010', 'V12SY0011','V12YDU105', 'V1200U101','V12SY0013','V12GXU255','V12HMU072','V12HMU073','V12HMU067','V12GXU256','V12YBT002','V175018031','V126009042','V187061033','V187060027')) or a.uwrulecode = 'V12RC0001') and exists (select t.missionprop1 from lwmission t, lccont c where 1 = 1 and a.contno = c.contno and t.activityid in ('0000001100') and c.conttype = '1' and (c.prtno = t.missionprop1 or c.contno = t.missionprop1) and t.activitystatus in ('1', '6', '7') and t.MissionProp10 like '86%' and (t.MissionProp11 = '0' or (t.MissionProp12 <= 'D' and t.MissionProp11 in ('1', '2', '3', '4'))) AND NOT EXISTS (SELECT 1 FROM lccont d WHERE (d.state LIKE '1002%' OR d.state LIKE '1003%' OR d.state LIKE '1005%') AND d.contno = c.CONTNO)) ) <>0";
		public static final String HAVEHELTHRULESQL=  "SELECT 1 AS field1, 58 AS flag FROM dual WHERE (select count(1) from lccuwerror a where a.insuredno = ? and a.contno <> ? and ((a.uwerror like '%体检%' and a.uwrulecode not in ('V12GXU263', 'V12YBT174', 'V12CFT194', 'V12GXU079', 'V12GXU206', 'V1200U200', 'V1200U104', 'V12IMC010', 'V12SY0011', 'V12YDU105', 'V1200U101', 'V12SY0013', 'V12GXU255', 'V12HMU072', 'V12HMU073', 'V12HMU067', 'V12GXU256', 'V12YBT002', 'V175018031', 'V126009042', 'V187061033', 'V187060027')) or a.uwrulecode = 'V12RC0001') and exists (select t.missionprop1 from lwmission t, lccont c where 1 = 1 and a.contno = c.contno and t.activityid in ('0000001100') and c.conttype = '1' and (c.prtno = t.missionprop1 or c.contno = t.missionprop1) and t.activitystatus in ('1', '6', '7') and t.MissionProp2 like '86%' and (t.MissionProp11 = '0' or (t.MissionProp6 <= 'D' and t.MissionProp11 in ('1', '2', '3', '4'))) AND NOT EXISTS (SELECT 1 FROM lccont d WHERE (d.state LIKE '004%' OR d.state LIKE '005%' OR d.state LIKE '006%') AND d.contno = c.CONTNO))) <> 0 or (select count(1) from lcuwerror a where a.insuredno = ? and a.contno <> ? and ((a.uwerror like '%体检%' and a.uwrulecode not in ('V12GXU263', 'V12YBT174', 'V12CFT194', 'V12GXU079', 'V12GXU206', 'V1200U200', 'V1200U104', 'V12IMC010', 'V12SY0011', 'V12YDU105', 'V1200U101', 'V12SY0013', 'V12GXU255', 'V12HMU072', 'V12HMU073', 'V12HMU067', 'V12GXU256', 'V12YBT002', 'V175018031', 'V126009042', 'V187061033', 'V187060027')) or a.uwrulecode = 'V12RC0001') and exists (select t.missionprop1 from lwmission t, lccont c where 1 = 1 and a.contno = c.contno and t.activityid in ('0000001100') and c.conttype = '1' and (c.prtno = t.missionprop1 or c.contno = t.missionprop1) and t.activitystatus in ('1', '6', '7') and t.MissionProp2 like '86%' and (t.MissionProp11 = '0' or (t.MissionProp6 <= 'D' and t.MissionProp11 in ('1', '2', '3', '4'))) AND NOT EXISTS (SELECT 1 FROM lccont d WHERE (d.state LIKE '004%' OR d.state LIKE '005%' OR d.state LIKE '006%') AND d.contno = c.CONTNO))) <> 0 ";
		/**
		 *  被保险人累计“稳得福C款两全保险（分红型）”保费  2018/10/09 xzh
		 */
		public static final String ACCPREM2052SQL ="select sum(case when a.payintv = 0 then  a.prem else a.prem * a.payendyear end) as field1, 59 as flag from lcpol a where a.insuredno = ? and a.riskcode = '2052' and a.appflag in ('0', '1','2','3') and a.uwflag not in ('1', '2', 'a') and not exists (select 1 from lccont b where (b.state like '004%' or b.state like '005%' or b.state like '006%') and b.contno = a.contno)";
		/**
		 *5018税优健康险投保份数
		 */
		public static final String COPIES5018SQL="SELECT COUNT(contno) as field1 ,43 as flag FROM LCCONT N WHERE N.INSUREDNO = ? AND N.salechnl = '8' AND N.APPFLAG    IN ('0', '1', '2','3') AND N.UWFLAG NOT IN ('1', 'a') ";

		/**
		 *  被保险人2052险种累计期交保费”保费  2018/10/09 xzh
		 */
		public static final String SUMSTANDPREM2052SQL = " select sum(a.prem) as field1, 60 as flag from lcpol a  where a.insuredno = ?  and a.riskcode = '2052'  and a.appflag in ('0', '1','2','3')  and a.uwflag not in ('1', '2', 'a')  AND NOT EXISTS (SELECT 1 FROM LCCONT b WHERE (b.STATE LIKE '004%' OR b.STATE LIKE '005%' OR  b.STATE LIKE '006%') AND b.CONTNO = a.CONTNO)";

		/**
		 *  被保险人2053险种累计期交保费”保费  2018/12/12 xzh
		 */
		public static final String SUMSTANDPREM2053SQL="select sum(a.prem) as field1, 61 as flag from lcpol a where a.insuredno = ? and a.riskcode = '2053' and a.appflag in ('0', '1','2','3') and a.uwflag not in ('1', '2', 'a') AND NOT EXISTS (SELECT 1 FROM LCCONT b WHERE (b.STATE LIKE '004%' OR b.STATE LIKE '005%' OR b.STATE LIKE '006%') AND b.CONTNO = a.CONTNO) ";


		/**
		 *  累计被保人当日所有有效保单的保险费（首期保费+续期保费）
		 */
	    public static final String SUMEFFECTIVEPOLICYSQL="SELECT SUM(b.prem * DECODE(b.payintv,0,1,1,12 * b.payyears,3,4 * b.payyears,6,2 * b.payyears,12,b.payyears)) AS field1,62 AS flag FROM lcpol b WHERE b.insuredno = ? AND b.polapplydate = to_date(TO_CHAR(SYSDATE,'YYYY-MM-DD'),'YYYY-MM-DD') AND b.appflag IN ('0', '1','2','3') AND b.uwflag NOT IN ('1', '2', 'a') AND NOT EXISTS (SELECT CONTNO FROM LCCONT WHERE (STATE LIKE '004%' OR STATE LIKE '005%' OR STATE LIKE '006%') AND CONTNO = b.CONTNO)";
		/**
		 *判断当日有没有现金缴费方式，存在现金交费 保费上限20000，否则上限200000；
		 */
		//public static final String ISPAYMENTCASHSQL="SELECT COUNT(1) AS field1, 63 AS flag FROM LCCONT a WHERE a.NEWPAYMODE = '1' AND a.POLAPPLYDATE = TO_DATE(TO_CHAR(SYSDATE, 'YYYY-MM-DD'), 'YYYY-MM-DD') AND a.INSUREDNO = ? AND a.APPFLAG in ('0', '1') AND a.UWFLAG NOT IN ('1', '2', 'a') AND NOT EXISTS (SELECT CONTNO FROM LCCONT WHERE (STATE LIKE '1002%' OR STATE LIKE '1003%' OR STATE LIKE '1005%') AND CONTNO = a.CONTNO)";

//上一保单年度有拒保、延期记录(各渠道)
		public static final String HAVEDEFERPOLICYLY="select (SELECT 1 " +
				"   FROM LDSYSVAR" +
				"  WHERE SYSVAR = 'onerow'" +
				"    AND (SELECT COUNT(1)" +
				"           FROM LCUWMASTER" +
				"          WHERE INSUREDNO = ?" +
				"            AND PASSFLAG IN ('1', '2')" +
				"            and modifydate >= TO_DATE((select to_char(a.cvalidate) from lccont a where exists(select 1 from lcwechatrelation b where b.beforecontno = a.contno and b.contno = ? )), 'YYYY-MM-DD')   " +
				"            and modifydate < TO_DATE((select to_char(a.PolApplyDate) from lccont a where a.contno = ?), 'YYYY-MM-DD')) +  " +
				"        (SELECT COUNT(1)" +
				"           FROM LCCUWMASTER" +
				"          WHERE INSUREDNO = ?" +
				"            AND PASSFLAG IN ('1', '2')" +
				"            and modifydate >= TO_DATE((select to_char(a.cvalidate) from lccont a where exists(select 1 from lcwechatrelation b where b.beforecontno = a.contno and b.contno = ? )), 'YYYY-MM-DD')" +
				"            and modifydate < TO_DATE((select to_char(a.PolApplyDate) from lccont a where a.contno = ?), 'YYYY-MM-DD')) +" +
				"        (SELECT COUNT(1)" +
				"           FROM LLUWMASTER" +
				"          WHERE INSUREDNO = ?" +
				"            AND PASSFLAG IN ('1', '2')" +
				"            and modifydate >= TO_DATE((select to_char(a.cvalidate) from lccont a where exists(select 1 from lcwechatrelation b where b.beforecontno = a.contno and b.contno = ? )), 'YYYY-MM-DD')" +
				"            and modifydate < TO_DATE((select to_char(a.PolApplyDate) from lccont a where a.contno = ?), 'YYYY-MM-DD')) +" +
				"        (SELECT COUNT(1)" +
				"           FROM LLCUWMASTER" +
				"          WHERE INSUREDNO = ?" +
				"            AND PASSFLAG IN ('1', '2')" +
				"            and modifydate >= TO_DATE((select to_char(a.cvalidate) from lccont a where exists(select 1 from lcwechatrelation b where b.beforecontno = a.contno and b.contno = ? )), 'YYYY-MM-DD')" +
				"            and modifydate < TO_DATE((select to_char(a.PolApplyDate) from lccont a where a.contno = ?), 'YYYY-MM-DD')) +" +
				"        (SELECT COUNT(1)" +
				"           FROM LPUWMASTER" +
				"          WHERE INSUREDNO = ?" +
				"            AND PASSFLAG IN ('1', '2')" +
				"            and modifydate >= TO_DATE((select to_char(a.cvalidate) from lccont a where exists(select 1 from lcwechatrelation b where b.beforecontno = a.contno and b.contno = ? )), 'YYYY-MM-DD')" +
				"            and modifydate < TO_DATE((select to_char(a.PolApplyDate) from lccont a where a.contno = ?), 'YYYY-MM-DD')) +" +
				"        (SELECT COUNT(1)" +
				"           FROM LPCUWMASTER" +
				"          WHERE INSUREDNO = ?" +
				"            AND PASSFLAG IN ('1', '2')" +
				"            and modifydate >= TO_DATE((select to_char(a.cvalidate) from lccont a where exists(select 1 from lcwechatrelation b where b.beforecontno = a.contno and b.contno = ? )), 'YYYY-MM-DD')" +
				"            and modifydate < TO_DATE((select to_char(a.PolApplyDate) from lccont a where a.contno = ?), 'YYYY-MM-DD')) > 0)   as field1, 25 as flag from dual   ";
		//上一保单年度有次标准体承保记录(各渠道)
		public static final String HAVESUBEXAMINELY = "select (SELECT 1 " +
				"         FROM DUAL" +
				"        WHERE (SELECT COUNT(1)" +
				"                 FROM LCUWMASTER" +
				"                WHERE CONTNO <> ?" +
				"                  AND INSUREDNO = ?" +
				"                  AND PASSFLAG IN ('3', '4', 'd')" +
				"                  and modifydate >= TO_DATE((select to_char(a.cvalidate) from lccont a where exists(select 1 from lcwechatrelation b where b.beforecontno = a.contno and b.contno = ? )), 'YYYY-MM-DD')     " +
				"                  and modifydate < TO_DATE((select to_char(a.PolApplyDate) from lccont a where a.contno = ?), 'YYYY-MM-DD')) +     " +
				"              (SELECT COUNT(1)" +
				"                 FROM LCCUWMASTER" +
				"                WHERE CONTNO <> ?" +
				"                  AND INSUREDNO = ?" +
				"                  AND PASSFLAG IN ('3', '4', 'd')" +
				"                  and modifydate >= TO_DATE((select to_char(a.cvalidate) from lccont a where exists(select 1 from lcwechatrelation b where b.beforecontno = a.contno and b.contno = ? )), 'YYYY-MM-DD')" +
				"                  and modifydate < TO_DATE((select to_char(a.PolApplyDate) from lccont a where a.contno = ?), 'YYYY-MM-DD')) +" +
				"              (SELECT COUNT(1)" +
				"                 FROM LLUWMASTER" +
				"                WHERE CONTNO <> ?" +
				"                  AND INSUREDNO = ?" +
				"                  AND PASSFLAG IN ('3', '4', 'd')" +
				"                  and modifydate >= TO_DATE((select to_char(a.cvalidate) from lccont a where exists(select 1 from lcwechatrelation b where b.beforecontno = a.contno and b.contno = ? )), 'YYYY-MM-DD')" +
				"                  and modifydate < TO_DATE((select to_char(a.PolApplyDate) from lccont a where a.contno = ?), 'YYYY-MM-DD')) +" +
				"              (SELECT COUNT(1)" +
				"                 FROM LLCUWMASTER" +
				"                WHERE CONTNO <> ?" +
				"                  AND INSUREDNO = ?" +
				"                  AND PASSFLAG IN ('3', '4', 'd')" +
				"                  and modifydate >= TO_DATE((select to_char(a.cvalidate) from lccont a where exists(select 1 from lcwechatrelation b where b.beforecontno = a.contno and b.contno = ? )), 'YYYY-MM-DD')" +
				"                  and modifydate < TO_DATE((select to_char(a.PolApplyDate) from lccont a where a.contno = ?), 'YYYY-MM-DD')) +" +
				"              (SELECT COUNT(1)" +
				"                 FROM LPUWMASTER" +
				"                WHERE CONTNO <> ?" +
				"                  AND INSUREDNO = ?" +
				"                  AND PASSFLAG IN ('3', '4', 'd')" +
				"                  and modifydate >= TO_DATE((select to_char(a.cvalidate) from lccont a where exists(select 1 from lcwechatrelation b where b.beforecontno = a.contno and b.contno = ? )), 'YYYY-MM-DD')" +
				"                  and modifydate < TO_DATE((select to_char(a.PolApplyDate) from lccont a where a.contno = ?), 'YYYY-MM-DD')) +" +
				"              (SELECT COUNT(1)" +
				"                 FROM LPCUWMASTER" +
				"                WHERE CONTNO <> ?" +
				"                  AND INSUREDNO = ?" +
				"                  AND PASSFLAG IN ('3', '4', 'd')" +
				"                  and modifydate >= TO_DATE((select to_char(a.cvalidate) from lccont a where exists(select 1 from lcwechatrelation b where b.beforecontno = a.contno and b.contno = ? )), 'YYYY-MM-DD')" +
				"                  and modifydate < TO_DATE((select to_char(a.PolApplyDate) from lccont a where a.contno = ?), 'YYYY-MM-DD')) > 0) as field1, 24 as flag from dual ";
		//上一保单年度有理赔记录(除建医卡\小额医疗\7060理赔二核为标准承保的理赔)
		public static final String HAVECLAIMS7060SPECILSQL = "select (SELECT 1 " +
				"  FROM DUAL" +
				" WHERE (SELECT count(*)" +
				"          FROM LLSubReport b" +
				"         WHERE b.customerno = ?" +
				"           and b.makedate >= TO_DATE((select to_char(a.cvalidate) from lccont a where exists(select 1 from lcwechatrelation b where b.beforecontno = a.contno and b.contno = ? )), 'YYYY-MM-DD')" +
				"           and b.makedate < TO_DATE((select to_char(a.PolApplyDate) from lccont a where a.contno = ?), 'YYYY-MM-DD')" +
				"           and not exists" +
				"         (SELECT 1" +
				"                  FROM llclaimreportdetail a" +
				"                 WHERE a.customerno = b.customerno" +
				"                   and (a.clmno = b.caseno or a.clmno = b.SUBRPTNO)" +
				"                   and a.riskcode = '7824')" +
				"           and not" +
				"                exists(select 1 from DUAL where (SELECT sum(c.pay)" +
				"                          FROM ljagetclaim c" +
				"                         WHERE c.getdutykind = '100'" +
				"                           and (c.otherno = b.caseno or c.otherno = b.SUBRPTNO)" +
				"                           and c.otherno in" +
				"                               (SELECT lg.rgtno" +
				"                                  FROM llregister lg" +
				"                                 WHERE lg.clmstate >= '60'" +
				"                                   and lg.customerno = b.customerno)) < 3000)" +
				"              " +
				"           and not exists" +
				"         (select 1" +
				"                  from lluwmaster d, lcpol e" +
				"                 where e.riskcode = '7060'" +
				"                   and e.insuredno = b.customerno" +
				"                   and d.passflag = '9'" +
				"                   and d.polno = e.polno" +
				"                   and (d.caseno = b.SUBRPTNO or d.caseno = b.caseno))) <> 0" +
				"      " +
				"    OR (SELECT count(*)" +
				"          FROM LLRegister b" +
				"         WHERE b.clmstate > '10'" +
				"           and b.customerno = ?" +
				"           and b.rgtdate >= TO_DATE((select to_char(a.cvalidate) from lccont a where exists(select 1 from lcwechatrelation b where b.beforecontno = a.contno and b.contno = ? )), 'YYYY-MM-DD')" +
				"           and b.rgtdate < TO_DATE((select to_char(a.PolApplyDate) from lccont a where a.contno = ?), 'YYYY-MM-DD')" +
				"           and not exists" +
				"         (SELECT 1" +
				"                  FROM llclaimdetail a" +
				"                 WHERE a.customerno = b.customerno" +
				"                   and a.clmno = b.RGTNO" +
				"                   and a.riskcode = '7824')" +
				"           and not" +
				"                exists(select 1 from DUAL where (SELECT sum(c.pay)" +
				"                          FROM ljagetclaim c" +
				"                         WHERE c.getdutykind = '100'" +
				"                           and (c.otherno = b.RGTNO)" +
				"                           and c.otherno in" +
				"                               (SELECT lg.rgtno" +
				"                                  FROM llregister lg" +
				"                                 WHERE lg.clmstate >= '60'" +
				"                                   and lg.customerno = b.customerno)) < 3000)" +
				"              " +
				"           and not exists (select 1" +
				"                  from lluwmaster d, lcpol e" +
				"                 where e.riskcode = '7060'" +
				"                   and e.insuredno = b.customerno" +
				"                   and d.passflag = '9'" +
				"                   and d.polno = e.polno" +
				"                   and d.caseno = b.RGTNO)) <> 0" +
				"         ) as field1, 26 as flag from dual　";
		//上一保单年度有理赔记录(除建医卡、小额医疗、7061理赔二核为标准承保)
		public static final String HAVECLAIMS7061SPECILSQL ="select (SELECT 1 " +
				"          FROM DUAL" +
				"         WHERE (SELECT count(*)" +
				"                  FROM LLSubReport b" +
				"                 WHERE b.customerno = ?" +
				"                   and b.makedate >= TO_DATE((select to_char(a.cvalidate) from lccont a where exists(select 1 from lcwechatrelation b where b.beforecontno = a.contno and b.contno = ? )), 'YYYY-MM-DD')" +
				"                   and b.makedate < TO_DATE((select to_char(a.PolApplyDate) from lccont a where a.contno = ?), 'YYYY-MM-DD')" +
				"                   and not exists" +
				"                 (SELECT 1" +
				"                          FROM llclaimreportdetail a" +
				"                         WHERE a.customerno = b.customerno" +
				"                           and (a.clmno = b.caseno or a.clmno = b.SUBRPTNO)" +
				"                           and a.riskcode = '7824')" +
				"                   and not" +
				"                        exists(select 1 from DUAL where (SELECT sum(c.pay)" +
				"                                  FROM ljagetclaim c" +
				"                                 WHERE c.getdutykind = '100'" +
				"                                   and (c.otherno = b.caseno or c.otherno = b.SUBRPTNO)" +
				"                                   and c.otherno in" +
				"                                       (SELECT lg.rgtno" +
				"                                          FROM llregister lg" +
				"                                         WHERE lg.clmstate >= '60'" +
				"                                           and lg.customerno = b.customerno)) < 3000)" +
				"                      " +
				"                   and not exists" +
				"                 (select 1" +
				"                          from lluwmaster d, lcpol e" +
				"                         where e.riskcode = '7061'" +
				"                           and e.insuredno = b.customerno" +
				"                           and d.passflag = '9'" +
				"                           and d.polno = e.polno" +
				"                           and (d.caseno = b.SUBRPTNO or d.caseno = b.caseno))) <> 0" +
				"              " +
				"            OR (SELECT count(*)" +
				"                  FROM LLRegister b" +
				"                 WHERE b.clmstate > '10'" +
				"                   and b.customerno = ?" +
				"                   and b.rgtdate >= TO_DATE((select to_char(a.cvalidate) from lccont a where exists(select 1 from lcwechatrelation b where b.beforecontno = a.contno and b.contno = ? )), 'YYYY-MM-DD')" +
				"                   and b.rgtdate < TO_DATE((select to_char(a.PolApplyDate) from lccont a where a.contno = ?), 'YYYY-MM-DD')" +
				"                   and not exists" +
				"                 (SELECT 1" +
				"                          FROM llclaimdetail a" +
				"                         WHERE a.customerno = b.customerno" +
				"                           and a.clmno = b.RGTNO" +
				"                           and a.riskcode = '7824')" +
				"                   and not" +
				"                        exists(select 1 from DUAL where (SELECT sum(c.pay)" +
				"                                  FROM ljagetclaim c" +
				"                                 WHERE c.getdutykind = '100'" +
				"                           and (c.otherno = b.RGTNO)" +
				"                           and c.otherno in" +
				"                               (SELECT lg.rgtno" +
				"                                  FROM llregister lg" +
				"                                 WHERE lg.clmstate >= '60'" +
				"                                   and lg.customerno = b.customerno)) < 3000)" +
				"              " +
				"           and not exists (select 1" +
				"                  from lluwmaster d, lcpol e" +
				"                 where e.riskcode = '7061'" +
				"                   and e.insuredno = b.customerno" +
				"                   and d.passflag = '9'" +
				"                   and d.polno = e.polno" +
				"                   and d.caseno = b.RGTNO)) <> 0) AS field1,27 as flag from dual";

		/**
		 *1043 险种各期保费之和；
		 */
		//public static final String RENEWALPREMSUM1043SQL=" select sum(case  when payintv = 0 then  prem  else  prem * payendyear  end) as field1,  63 as flag  from lcpol  where insuredno = ?  and riskcode = '1043'  and exists (select 1  from lccont a  where a.contno = contno  and a.appflag in ('0', '1','2')  and a.uwflag not in ('1', '2', 'a')  and a.STATE not LIKE '004%'  and a.STATE not LIKE '005%'  and a.STATE not LIKE '006%') ";
		public static final String RENEWALPREMSUM1043SQL=" select sum(case  when payintv = 0 then  prem  else  prem * payendyear  end) as field1, 63 as flag from lcpol where insuredno = ? and riskcode = '1043' and appflag in ('0', '1','2','3') and uwflag not in ('1', '2', 'a') and exists (select 1 from lccont a  where a.contno = contno and a.STATE not LIKE '004%' and a.STATE not LIKE '005%' and a.STATE not LIKE '006%') ";

	}

	/**
	 * 客户信息
	 *
	 */
	public static class CustInfo {
		/**
		 * 客户基本信息SQL（CustomerNo：客户编码）
		 */
		//public static final String CUSTBASICSQL = "select distinct CustomerNo,EMail,HomeAddress,City,HomeZipCode,Province,Mobile,PostalAddress,ZipCode,GrpName,HomePhone,Phone,COUNTY,STORENO,STORENO2,ZONECODE from lcaddress where CustomerNo=? ";
		public static final String CUSTBASICSQL = "select distinct CustomerNo,EMail,HomeAddress,City,HomeZipCode,Province,Mobile,PostalAddress,ZipCode,GrpName,HomePhone,Phone,COUNTY,STORENO,STORENO2,ZONECODE from lcaddress where (CustomerNo=? and addressno = (select max(addressno) from lcaddress where customerno=? )) ";


		/**
		 * 客户姓名与恐怖分子、恐怖组织名单或武器禁运名单相同SQL（CONTNO：合同号）
		 */
		public static final String SAMENAMETERRSQL = "SELECT 1 as field1, 1 as flag FROM LDSYSVAR WHERE SYSVAR = 'onerow' AND (SELECT COUNT(1) FROM LXBLACKLISTINDV A, LCAPPNT B WHERE B.CONTNO = ? AND (B.APPNTNAME = A.NAME OR B.APPNTNAME=A.USEDNAME)) + (SELECT COUNT(1) FROM LXBLACKLISTINDV A, LCINSURED B WHERE B.CONTNO = ? AND (B.NAME = A.NAME OR B.NAME=A.USEDNAME)) + (SELECT COUNT(1) FROM LXBLACKLISTINDV A, LCBNF B WHERE B.CONTNO = ? AND (B.NAME = A.NAME OR B.NAME=A.USEDNAME)) > 0 ";

		/**
		 * 客户证件类型和号码与恐怖分子、恐怖组织名单或武器禁运名单相同SQL（CONTNO：合同号）
		 */
		public static final String SAMETYPETERRSQL = "SELECT 1 as field1, 2 as flag FROM LDSYSVAR WHERE SYSVAR = 'onerow' AND (SELECT COUNT(1) FROM LXBLACKLISTINDV A, LCAPPNT B WHERE B.CONTNO = ? AND B.IDTYPE = A.IDTYPE AND B.IDNO = A.IDNO) + (SELECT COUNT(1) FROM LXBLACKLISTINDV A, LCINSURED B WHERE B.CONTNO = ? AND B.IDTYPE = A.IDTYPE AND B.IDNO = A.IDNO) + (SELECT COUNT(1) FROM LXBLACKLISTINDV A, LCBNF B WHERE B.CONTNO = ? AND B.IDTYPE = A.IDTYPE AND B.IDNO = A.IDNO) > 0 ";
	}

	/**
	 * 受益人信息
	 *
	 */
	public static class BnfcryInfo {
		/**
		 * 受益人基本信息SQL（CONTNO：合同号）
		 */
		/*public static final String BNFCRYBASICSQL = "SELECT DISTINCT (SELECT NVL(SUM(BNFLOT), 0) FROM LCBNF M WHERE M.contno = ? AND M.BNFTYPE  = 0 ) / (select DECODE((SELECT COUNT(DISTINCT polno ) FROM LCBNF M WHERE M.contno = ? AND M.BNFTYPE = 0),0,1,(SELECT COUNT(DISTINCT polno ) FROM LCBNF M WHERE M.contno = ? AND M.BNFTYPE = 0) ) from dual ),"
			+"BnfType,Name,BnfGrade,TO_CHAR(Birthday,'YYYY-MM-DD'),Sex,IDNo,IDType,IdValiDate,RelationToInsured,ADDRESS,TEL,ZIPCODE,CUSTOMERNO, INSUREDNO ,(SELECT NVL(SUM(BNFLOT), 0)FROM LCBNF M WHERE M.contno = ?  AND M.BNFTYPE  = 1)/"
			+"(select DECODE((SELECT COUNT(DISTINCT polno ) FROM LCBNF M WHERE M.contno = ? AND M.BNFTYPE = 1),0,1,(SELECT COUNT(DISTINCT polno ) FROM LCBNF M WHERE M.contno = ? AND M.BNFTYPE = 1) ) from dual ) FROM lcbnf WHERE contno = ? ";*/

		public static final String BNFCRYBASICSQL ="SELECT  BNFLOT, BnfType,Name, BnfGrade,TO_CHAR(Birthday,'YYYY-MM-DD'),Sex,IDNo,IDType,IdValiDate,RelationToInsured,ADDRESS,TEL,ZIPCODE,CUSTOMERNO,INSUREDNO ,polno FROM lcbnf WHERE contno = ? ";

		/**
		 * 黑名单1-证件号与和黑名单相同,不分国籍,>0触发,参数:一个contno
		 * {this} 的证件号与和黑名单相同
		 */
		public static final String BNFBLACKIDFLAG = "select count(1) as field1 ,1 as flag from LXBlackListIndv a, lcbnf b, lxidtypeorno c where  b.contno = ? and c.blacklistid = a.blacklistid and b.idno = c.idno";

		/**
		 * XXX黑名单2-受益人名1部分组成,受益人名与黑名单表四个名字对比,>0触发,参数:4个contno
		 * {this}的名字与黑名单名字和生日相同(名字为一部分)
		 */
		public static final String BNFBLACKFLAG1 = "select (case when (select firsttab.num from (select count(distinct(e.blacklistid)) as num from LXBlackListIndv a, lcbnf b, lxblackdate e where  b.contno = ? and e.blacklistid = a.blacklistid and instr(b.name, ' ') = 0 and a.firstname = b.name group by e.blacklistid, e.blackdate, b.birthday having (select case when ((select count(1) from dual where (select count(1) from lxblackdate p where p.blacklistid = e.blacklistid) >= 1 and length(e.blackdate) = 4 and e.blackdate = substr(b.birthday, 1, 4))) > 0 then 1 else case when to_char(b.birthday, 'yyyy-mm-dd') >= (select min(blackdate) from lxblackdate where blacklistid = e.blacklistid) and to_char(b.birthday, 'yyyy-mm-dd') <= (select max(blackdate) from lxblackdate where blacklistid = e.blacklistid) then 1 else 0 end end from dual) > 0) firsttab where rownum = 1) > 0 then 1 else 0 end + case when (select middletab.num from (select count(distinct(e.blacklistid)) as num from LXBlackListIndv a, lcbnf b, lxblackdate e where  b.contno = ? and e.blacklistid = a.blacklistid and instr(b.name, ' ') = 0 and a.middlename = b.name group by e.blacklistid, e.blackdate, b.birthday having (select case when ((select count(1) from dual where (select count(1) from lxblackdate p where p.blacklistid = e.blacklistid) >= 1 and length(e.blackdate) = 4 and e.blackdate = substr(to_char(b.birthday, 'yyyy-mm-dd'), 1, 4))) > 0 then 1 else case when to_char(b.birthday, 'yyyy-mm-dd') >= (select min(blackdate) from lxblackdate where blacklistid = e.blacklistid) and to_char(b.birthday, 'yyyy-mm-dd') <= (select max(blackdate) from lxblackdate where blacklistid = e.blacklistid) then 1 else 0 end end from dual) > 0) middletab where rownum = 1) > 0 then 1 else 0 end + case when (select surntab.num from (select count(distinct(e.blacklistid)) as num from LXBlackListIndv a, lcbnf b, lxblackdate e where  b.contno = ? and e.blacklistid = a.blacklistid and instr(b.name, ' ') = 0 and a.surname = b.name group by e.blacklistid, e.blackdate, b.birthday having (select case when ((select count(1) from dual where (select count(1) from lxblackdate p where p.blacklistid = e.blacklistid) >= 1 and length(e.blackdate) = 4 and e.blackdate = substr(b.birthday, 1, 4))) > 0 then 1 else case when to_char(b.birthday, 'yyyy-mm-dd') >= (select min(blackdate) from lxblackdate where blacklistid = e.blacklistid) and to_char(b.birthday, 'yyyy-mm-dd') <= (select max(blackdate) from lxblackdate where blacklistid = e.blacklistid) then 1 else 0 end end from dual) > 0) surntab where rownum = 1) > 0 then 1 else 0 end + case when (select nametab.num from (select count(distinct(e.blacklistid)) as num from LXBlackListIndv a, lcbnf b, lxblackdate e where  b.contno = ? and e.blacklistid = a.blacklistid and instr(b.name, ' ') = 0 and a.name = b.name group by e.blacklistid, e.blackdate, b.birthday having (select case when ((select count(1) from dual where (select count(1) from lxblackdate p where p.blacklistid = e.blacklistid) >= 1 and length(e.blackdate) = 4 and e.blackdate = substr(b.birthday, 1, 4))) > 0 then 1 else case when to_char(b.birthday, 'yyyy-mm-dd') >= (select min(blackdate) from lxblackdate where blacklistid = e.blacklistid) and to_char(b.birthday, 'yyyy-mm-dd') <= (select max(blackdate) from lxblackdate where blacklistid = e.blacklistid) then 1 else 0 end end from dual) > 0) nametab where rownum = 1) > 0 then 1 else 0 end) as field, 2 as flag from dual ";

		/**
		 * xxx黑名单3-受益人名2部分组成,受益人名与黑名单表2个名字对比,>0触发,参数:2个contno
		 * 有一个name相同,只比较firstname和surname，且出生日期相同
		 * {this}的名字与黑名单名字和生日相同(名字为两部分)
		 */
		public static final String BNFBLACKFLAG2 = "select (case when (select firsttab.num from (select count(distinct(e.blacklistid)) as num from LXBlackListIndv a, lcbnf b, lxblackdate e where  b.contno = ? and e.blacklistid = a.blacklistid and length(b.name) - length(replace(b.name, ' ')) = 1 and a.firstname = substr(b.name, 1, instr(b.name, ' ', 1) - 1) group by e.blacklistid, e.blackdate, b.birthday having (select case when ((select count(1) from dual where (select count(1) from lxblackdate p where p.blacklistid = e.blacklistid) >= 1 and length(e.blackdate) = 4 and e.blackdate = substr(to_char(b.birthday, 'yyyy-mm-dd'), 1, 4))) > 0 then 1 else case when to_char(b.birthday, 'yyyy-mm-dd') >= (select min(blackdate) from lxblackdate where blacklistid = e.blacklistid) and to_char(b.birthday, 'yyyy-mm-dd') <= (select max(blackdate) from lxblackdate where blacklistid = e.blacklistid) then 1 else 0 end end from dual) > 0) firsttab where rownum = 1) > 0 then 1 else 0 end + case when (select surntab.num from (select count(distinct(e.blacklistid)) as num from LXBlackListIndv a, lcbnf b, lxblackdate e where  b.contno = ? and e.blacklistid = a.blacklistid and length(b.name) - length(replace(b.name, ' ')) = 1 and a.surname = substr(b.name, instr(b.name, ' ', 1) + 1, length(b.name)) group by e.blacklistid, e.blackdate, b.birthday having (select case when ((select count(1) from dual where (select count(1) from lxblackdate p where p.blacklistid = e.blacklistid) >= 1 and length(e.blackdate) = 4 and e.blackdate = substr(to_char(b.birthday, 'yyyy-mm-dd'), 1, 4))) > 0 then 1 else case when to_char(b.birthday, 'yyyy-mm-dd') >= (select min(blackdate) from lxblackdate where blacklistid = e.blacklistid) and to_char(b.birthday, 'yyyy-mm-dd') <= (select max(blackdate) from lxblackdate where blacklistid = e.blacklistid) then 1 else 0 end end from dual) > 0) surntab where rownum = 1) > 0 then 1 else 0 end) as field, 3 as flag from dual ";

		/**
		 * 黑名单4-受益人名2部分组成,受益人名与黑名单表2个名字对比,>=2触发,参数:2个contno
		 * firstname和surnname都要相同.无生日限制
		 * {this} 的名字与黑名单名字和生日相同(名字为两部分,无生日限制)
		 */
		public static final String BNFBLACKFLAG3 = "select ((select count(1) from LXBlackListIndv a, lcbnf b where  and b.contno = ? and length(b.name) - length(replace(b.name, ' ')) = 1 and a.firstname = substr(b.name ,1, instr(b.name, ' ', 1)-1) ) + (select count(1) from LXBlackListIndv a, lcbnf b where  b.contno = ? and length(b.name) - length(replace(b.name, ' ')) = 1 and a.surname = substr(b.name ,instr(b.name, ' ', 1)+1,length(b.name)) )) as field,4 as flag from dual";


		/**
		 * XXX黑名单5-受益人名4部分组成,受益人名与黑名单表3个名字对比,>0触发,参数:3个contno
		 * firstname,middlename,surname,有生日限制
		 * {this} 的名字与黑名单名字两个以上相同(名字为四部分)
		 */
		public static final String BNFBLACKFLAG4 = "SELECT (CASE WHEN (select firsttab.num from (SELECT count(distinct(e.blacklistid)) as num FROM LXBlackListIndv a, lcbnf b, lxblackdate e WHERE  b.contno = ? AND e.blacklistid = a.blacklistid AND (length(b.name) - length(replace(b.name, ' '))) >= 2 AND a.firstname = substr(b.name, 1, instr(b.name, ' ') - 1) GROUP BY e.blacklistid, e.blackdate, b.birthday HAVING (SELECT CASE WHEN ((SELECT count(1) FROM dual WHERE (SELECT count(1) FROM lxblackdate p WHERE p.blacklistid = e.blacklistid) >= 1 AND length(e.blackdate) = 4 AND e.blackdate = substr(to_char(b.birthday, 'yyyy-mm-dd'), 1, 4))) > 0 THEN 1 ELSE CASE WHEN to_char(b.birthday, 'yyyy-mm-dd') >= (select min(blackdate) from lxblackdate where blacklistid = e.blacklistid) and to_char(b.birthday, 'yyyy-mm-dd') <= (select max(blackdate) from lxblackdate where blacklistid = e.blacklistid) THEN 1 ELSE 0 END END FROM dual) > 0) firsttab where rownum = 1) > 0 THEN 1 ELSE 0 END + CASE WHEN (select middletab.num from (SELECT count(distinct(e.blacklistid)) as num FROM LXBlackListIndv a, lcbnf b, lxblackdate e WHERE  b.contno = ? AND e.blacklistid = a.blacklistid AND (length(b.name) - length(replace(b.name, ' '))) >= 2 AND a.middlename = substr(b.name, instr(b.name, ' ') + 1, (instr(b.name, ' ', 1, 2)) - (instr(b.name, ' ') + 1)) GROUP BY e.blacklistid, e.blackdate, b.birthday HAVING (SELECT CASE WHEN ((SELECT count(1) FROM dual WHERE (SELECT count(1) FROM lxblackdate p WHERE p.blacklistid = e.blacklistid) >= 1 AND length(e.blackdate) = 4 AND e.blackdate = substr(to_char(b.birthday, 'yyyy-mm-dd'), 1, 4))) > 0 THEN 1 ELSE CASE WHEN to_char(b.birthday, 'yyyy-mm-dd') >= (select min(blackdate) from lxblackdate where blacklistid = e.blacklistid) and to_char(b.birthday, 'yyyy-mm-dd') <= (select max(blackdate) from lxblackdate where blacklistid = e.blacklistid) THEN 1 ELSE 0 END END FROM dual) > 0) middletab where rownum = 1) > 0 THEN 1 ELSE 0 END + CASE WHEN (select surntab.num from (SELECT count(distinct(e.blacklistid)) as num FROM LXBlackListIndv a, lcbnf b, lxblackdate e WHERE  b.contno = ? AND e.blacklistid = a.blacklistid AND (length(b.name) - length(replace(b.name, ' '))) >= 2 AND a.surname = substr(b.name, instr(b.name, ' ', 1, 2) + 1) GROUP BY e.blacklistid, e.blackdate, b.birthday HAVING (SELECT CASE WHEN ((SELECT count(1) FROM dual WHERE (SELECT count(1) FROM lxblackdate p WHERE p.blacklistid = e.blacklistid) >= 1 AND length(e.blackdate) = 4 AND e.blackdate = substr(to_char(b.birthday, 'yyyy-mm-dd'), 1, 4))) > 0 THEN 1 ELSE CASE WHEN to_char(b.birthday, 'yyyy-mm-dd') >= (select min(blackdate) from lxblackdate where blacklistid = e.blacklistid) and to_char(b.birthday, 'yyyy-mm-dd') <= (select max(blackdate) from lxblackdate where blacklistid = e.blacklistid) THEN 1 ELSE 0 END END FROM dual) > 0) surntab where rownum = 1 ) > 0 THEN 1 ELSE 0 END) AS field, 5 AS flag FROM dual ";

		/**
		 * xxxxxx黑名单6-受益人名4部分组成,受益人名与黑名单表3个名字对比,>=2触发,参数:3个contno
		 * firstname,middlename,surname,无生日限制
		 * {this} 的名字与黑名单相同(受益人名四部分构成无生日限制)
		 */
		public static final String BNFBLACKFLAG5 = "SELECT ( (select case when (SELECT count(1) FROM LXBlackListIndv a, lcbnf b WHERE  b.contno = ? AND (length(b.name) - length(replace(b.name, ' '))) >= 2 AND a.firstname = substr(b.name, 1, instr(b.name, ' ') - 1))>0 then 1 else 0 end from dual) + (select case when (SELECT count(1) FROM LXBlackListIndv a, lcbnf b WHERE  b.contno = ? AND (length(b.name) - length(replace(b.name, ' '))) >= 2 AND a.middlename = substr(b.name, instr(b.name, ' ') + 1, (instr(b.name, ' ', 1, 2)) - (instr(b.name, ' ') + 1))) > 0 then 1 else 0 end from dual) + (select case when (SELECT count(1) FROM LXBlackListIndv a, lcbnf b WHERE  b.contno = ? AND (length(b.name) - length(replace(b.name, ' '))) >= 2 AND a.surname = substr(b.name, instr(b.name, ' ', 1, 2) + 1)) > 0 then 1 else 0 end from dual) ) AS field, 6 AS flag FROM dual ";




	}

	/**
	 * 代理人
	 *
	 */
	public static class Agent {
		/**
		 * 代理人基本信息SQL（agentCode：代理人编码）
		 */
		public static final String AGENTBASICSQL = "select distinct a.AgentCode,a.Name,a.Mobile,a.IDNO,a.agentgroup,b.statevalue,b.statetype,a.managecom from laagent a, laagentstate b where a.agentCode = b.agentCode and a.agentCode=? ";


		/**
		 * 非自保件主动投保标识SQL（contno：合同号）
		 */
		public static final String AUTOPOLICYSIGNSQL = "select '1' as field1, 1 as flag from dual where 1=1 and (1=(SELECT 1 FROM LCCUSTOMERIMPARTPARAMS A WHERE A.PATCHNO = '19044' AND A.IMPARTVER = 'DLR5' AND A.IMPARTCODE = '2-1' AND A.CONTNO = ? AND A.CUSTOMERNOTYPE = '2' AND A.IMPARTPARAMNAME = 'Number' AND A.IMPARTPARAMNO = '1' AND A.IMPARTPARAM = '2' AND EXISTS (SELECT 1 FROM LCCUSTOMERIMPARTPARAMS C WHERE C.GRPCONTNO = A.GRPCONTNO AND C.CONTNO = A.CONTNO AND C.PATCHNO = A.PATCHNO AND C.CUSTOMERNO = A.CUSTOMERNO AND C.CUSTOMERNOTYPE = A.CUSTOMERNOTYPE AND C.IMPARTVER = 'DLR1' AND C.IMPARTPARAMNAME = 'YesOrNo' AND C.IMPARTPARAM = '2')) OR 1=(select 1 FROM LCCUSTOMERIMPARTPARAMS A WHERE A.PATCHNO = '19044' AND A.IMPARTVER = 'DLR5' AND A.IMPARTCODE = '2-1' AND A.CONTNO = ? AND A.CUSTOMERNOTYPE = '2' AND A.IMPARTPARAMNO = '1' AND A.IMPARTPARAMNAME = 'Number' AND A.IMPARTPARAM = '2' AND EXISTS (SELECT 1 FROM LCCUSTOMERIMPARTPARAMS C WHERE C.GRPCONTNO = A.GRPCONTNO AND C.CONTNO = A.CONTNO AND C.PATCHNO = A.PATCHNO AND C.CUSTOMERNO = A.CUSTOMERNO AND C.CUSTOMERNOTYPE = A.CUSTOMERNOTYPE AND C.IMPARTVER = 'DLR1' AND C.IMPARTCODE = '1-1' AND C.IMPARTPARAMNO = '1' AND C.IMPARTPARAMNAME = 'YesOrNo' AND C.IMPARTPARAM = '1') AND EXISTS (SELECT 1 FROM LCCUSTOMERIMPARTPARAMS C WHERE C.GRPCONTNO = A.GRPCONTNO AND C.CONTNO = A.CONTNO AND C.PATCHNO = A.PATCHNO AND C.CUSTOMERNO = A.CUSTOMERNO AND C.CUSTOMERNOTYPE = A.CUSTOMERNOTYPE AND C.IMPARTVER = 'DLR1' AND C.IMPARTCODE = '1-1' AND C.IMPARTPARAMNO = '2' AND C.IMPARTPARAMNAME = 'Number' AND C.IMPARTPARAM = '5'))) ";

		/**
		 * 业务人员黑名单标识SQL（contno：合同号）
		 */
		public static final String BLACKSIGNSQL = "SELECT '1' as field1, 2 as flag FROM DUAL WHERE (SELECT COUNT(1) FROM LAAGENTMONITOR WHERE AGENTCODE = (SELECT AGENTCODE FROM LCCONT WHERE CONTNO = ? AND CONTTYPE = '1') AND MONITORTYPE = '01' AND  MONITORSTATE = '1' AND CANCELDATE IS NULL) + (SELECT COUNT(1) FROM LAAGENTBLACKLIST A, LAAGENT B WHERE A.IDNOTYPE = B.IDNOTYPE AND B.IDNOTYPE = '0' AND A.IDNO = B.IDNO AND B.AGENTCODE = (SELECT AGENTCODE FROM LCCONT WHERE CONTNO = ? AND CONTTYPE = '1')) > 0 ";

		/**
		 * 代理人的所属机构
		 */

		public static final String AGENTBASICSQL2 = "select  managecom,3 as flag from laagent where agentcode = ? ";

		/**
		 * 监控状态 wpq 2018-02-02
		 */
		public static final String AGENTBASICSQL5 ="SELECT monitorstate AS filed1, 6 AS flag FROM laagentmonitor WHERE AGENTCODE = ? and monitorno = (select MAX(monitorno) from laagentmonitor where agentCode = ?)";



	}

	/**
	 * 险种信息
	 *
	 */
	public static class InsurType {
		/**
		 * 险种基本信息SQL（contno：合同号）
		 */
		public static final String INSURTYPEBASICSQL = "select distinct a.StandPrem,b.AutoPayFlag,b.BonusMode,a.InsuYear,b.SubRiskFlag,b.RiskCode,b.KindCode,b.RiskName,b.RiskType,b.RiskPeriod,b.RiskType1,b.RiskType2,b.RiskType3,b.RiskType4,b.RiskType5,b.RiskTypeDetail,a.Mult,b.EndDate,b.RiskVer,a.Amnt,a.payYears,a.payendyearflag,a.payendyear,a.prem,a.fqgetmode,a.paymode,a.paylocation,a.GetYear,a.livegetmode,a.liveaccflag,a.insuredno,a.PayIntv,a.BonusMan,a.InsuYearFlag,a.payYears,a.PRODSETCODE,d.dutycode,b.RiskProp ,a.payendyear, a.agenttype,a.rnewflag  from lcpol a,lmriskapp b,lccont c, LCDUTY d where a.contno = ? and a.riskcode = b.riskcode and c.contno=a.contno and d.contno=a.contno and d.polno = a.polno";

		/**
		 * 保单的保额、保费SQL（contno：合同号）
		 */
		public static final String SUMPREMANDAMNTSQL = "select sum (PREM) as field1,0 as field2, 1 as flag from lcpol where contno = ? ";

	/*	*//**
		 * 累计追加保费SQL（insuredno：被保人编码）
		 *//*
		public static final String SUMADDPRESQL = "select nvl(sum(p.prem),0) as field1, 0 as field2, 2 as flag FROM lcprem p where polno in(SELECT polno FROM lcpol where insuredno=?) and dutycode = ? ";
		*/
		/**
		 * 期交基本保险费之和SQL（insuredno：被保人编码，appntno：投保人编码）
		 */
		public static final String SUMBASICPREMIUMSQL = "select SUM(M.STANDPREM) as field1, 0 as field2, 3 as flag FROM LCDUTY M, LCPOL P WHERE M.CONTNO = P.CONTNO AND M.POLNO = P.POLNO AND P.INSUREDNO = ? AND P.APPNTNO=? AND P.RISKCODE = '5013' AND M.DUTYCODE = '501301' AND P.APPFLAG IN ('0', '1','2') AND P.UWFLAG NOT IN ('1', '2', 'a') AND NOT EXISTS (SELECT 1 FROM LCCONT WHERE CONTNO = P.CONTNO AND (STATE LIKE '004%' OR STATE LIKE '005%' OR STATE LIKE '006%')) ";

		/**
		 * 六个月内投保次数SQL（appntno：投保人编码）
		 */
//		public static final String INSUREDNUMSQL = "select COUNT(1) as fiedl1, 0 as field2, 4 as flag FROM lccont WHERE cvalidate IS NOT NULL AND cvalidate   <=to_date(TO_CHAR(sysdate,'YYYY-MM-DD'),'YYYY-MM-DD') AND cvalidate >= (SELECT ADD_MONTHS(to_date(TO_CHAR(sysdate,'YYYY-MM-DD'),'YYYY-MM-DD'),-6) FROM dual) and  appntno=? ";
		public static final String INSUREDNUMSQL = "select COUNT(1) as fiedl1, 0 as field2, 4 as flag FROM lccont WHERE cvalidate IS NOT NULL AND cvalidate <= trunc(sysdate) AND cvalidate >= trunc(add_months(sysdate, -6)) and  appntno=? ";
		/*	*//**
		 * 险种份数
		 *//*
		public static final String RISKMULTSQL ="SELECT SUM(mult) ,0 as field2, 5 as flag FROM lcpol WHERE insuredno = ? AND riskcode   = ? AND appflag    IN ('0', '1', '2')  AND uwflag NOT IN ('1', '2', 'a') AND NOT EXISTS (SELECT contno FROM lccont  WHERE (state LIKE '1002%' OR state LIKE '1003%' OR state LIKE '1005%') AND contno = lcpol.contno )";
		*/
		/**
		 * 险种审核标识
		 *//*
		public static final String RISKAMNT_DEATH_FLAGSQL ="SELECT  cast(A.ADULTFLAG as INT) ,0 as field2, 5 as flag  FROM RISKAMNT_DEATH A, LCPOL B WHERE A.RISKCODE = B.RISKCODE AND B.CONTNO   = ?" ;
		*/

	}

	/**
	 * 告知信息
	 *
	 */
	public static class disclose {
		/**
		 * 告知SQL
		 */
		public static final String DISCLOSESQL = "SELECT A.IMPARTCODE,a.IMPARTVER,a.IMPARTPARAMNAME,a.IMPARTPARAM , a.CUSTOMERNO FROM LCCUSTOMERIMPARTPARAMS A WHERE A.CONTNO   = ? and A.CUSTOMERNOTYPE =?  ";
	}
}
