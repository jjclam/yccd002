package com.sinosoft.cloud.cbs.zbx.util;

/**
 * Created by sll on 2019/4/9.
 */
public enum CardTypeEnum {
    IdentityCard("0","111"),Passport("1","414"),GoHomeCard("11","516"),TemporIdentityCard("12","112"),PoliceCard("13","123"),BirthCard("14","117"),GangAndAoPassCard("15","513"),
    TaiWanPassCard("16","511"),SoldierCard("2","114"),DrivingCard("3","335"),HomeCard("4","113"),StudentCard("5","133"),WorkCard("6","131"),TaiWanCard("7","990"),OtherCard("8","990"),
    UnCard("9","990"),ForeignerLiveCard("17","553");

    CardTypeEnum(String code, String code_af) {
        this.code = code;
        this.code_af = code_af;
    };

    //IdentityCard
    //转换前的证件类型编码
    private String code;
    //转换后的证件类型编码
    private String code_af;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getCode_af() {
        return code_af;
    }

    public void setCode_af(String code_af) {
        this.code_af = code_af;
    }
    /**
     * 循环遍历枚举类 取得code的转换值
     */
    public static String getValue(String code){
        for (CardTypeEnum cardTypeEnum:CardTypeEnum.values()){
            if (cardTypeEnum.getCode().equals(code)){
                return cardTypeEnum.getCode_af();
            }
        }
        return null;
    }
}
