package com.sinosoft.cloud.cbs.rules.bom;


/**
 * 投连账号
 * 
 * @author dingfan
 * 
 */
public class InvestAccounts{
	/**
	 * 投资账户代码
	 */
	private String accountsCode;

	/**
	 * 投资分配比例
	 */
	private double investPercent;

	public InvestAccounts() {
		super();
	}

	public String getAccountsCode() {
		return accountsCode;
	}

	public void setAccountsCode(String accountsCode) {
		this.accountsCode = accountsCode;
	}

	public double getInvestPercent() {
		return investPercent;
	}

	public void setInvestPercent(double investPercent) {
		this.investPercent = investPercent;
	}
}
