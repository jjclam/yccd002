package com.sinosoft.cloud.cbs.trules.util;

/**
 * @Author:wangshuliang
 * @Description:
 * @Date:Created in 9:51 2018/8/16
 * @Modified by:
 */
public class SqlConst {
    /**
     * 累计同一被保人疾病身故保额（1806和1805）
     */
    public static String AccDiseaseDieAmntSql = "Select 1 as flag, Nvl(Sum(b.Amnt),'0') as field " +
            "        From LCInsured a, LCPol b " +
            "        Where 1 = 1 " +
            "        And a.ContNo = b.ContNo " +
            "        And b.appflag in ('1' , '0','2') " +
            "        And b.cvalidate < ? " +
            "        And b.enddate > ? " +
            "        And b.RiskCode In ('1805', '1806') " +
            "        And a.insuredno = ? ";

    /**
     * 累计同一被保人航班飞机意外险保额（6810和6805）
     */
    public static String AccInsurdFlightAccidentAmntSql = " Select 2 as flag, Nvl(Sum(c.Amnt),'0') as field " +
            "        From LCInsured a, LCPol b, LCDuty c " +
            "       Where 1 = 1 " +
            "         And a.ContNo = b.ContNo " +
            "         And a.ContNo = c.ContNo " +
            "         And b.PolNo = c.PolNo " +
            "         And b.cvalidate < ? " +
            "         And b.enddate > ? " +
            "         And b.Appflag in ('1' , '0','2') " +
            "         And c.DutyCode like '%05' " +
            "         And b.RiskCode in ('6805', '6810') " +
            "         And a.insuredno = ? ";
    /**
     * 累计同一被保人火车地铁意外险保额（6810和6805）
     */
    public static String AccInsurdTrainAccidentAmntSql = " Select 3 as flag, Nvl(Sum(c.Amnt),'0') as field " +
            "        From LCInsured a, LCPol b, LCDuty c " +
            "       Where 1 = 1 " +
            "         And a.ContNo = b.ContNo " +
            "         And a.ContNo = c.ContNo " +
            "         And b.PolNo = c.PolNo " +
            "         And b.cvalidate < ? " +
            "         And b.enddate > ? " +
            "         And b.Appflag in ('1' , '0','2') " +
            "         And c.DutyCode like '%03' " +
            "         And b.RiskCode in ('6805', '6810') " +
            "         And a.insuredno = ? ";
    /**
     * 累计同一被保人轮船摆渡意外险保额（6810和6805）
     */
    public static String AccInsurdTrafficAccidentAmntSql = " Select 4 as flag, Nvl(Sum(c.Amnt),'0') as field " +
            "        From LCInsured a, LCPol b, LCDuty c " +
            "       Where 1 = 1 " +
            "         And a.ContNo = b.ContNo " +
            "         And a.ContNo = c.ContNo " +
            "         And b.PolNo = c.PolNo " +
            "         And b.cvalidate < ? " +
            "         And b.enddate > ? " +
            "         And b.Appflag in ('1' , '0','2') " +
            "         And c.DutyCode like '%04' " +
            "         And b.RiskCode in ('6805', '6810') " +
            "         And a.insuredno = ? ";
    //累计同一被保人自驾车意外险保额（6810和6805）
    public static String AccInsurdPrivateCarAccidentAmntSql = " Select 5 as flag, Nvl(Sum(c.Amnt),'0') as field " +
            "        From LCInsured a, LCPol b, LCDuty c " +
            "       Where 1 = 1 " +
            "         And a.ContNo = b.ContNo " +
            "         And a.ContNo = c.ContNo " +
            "         And b.PolNo = c.PolNo " +
            "         And b.cvalidate < ? " +
            "         And b.enddate > ? " +
            "         And b.Appflag in ('1' , '0','2') " +
            "         And c.DutyCode like '%01' " +
            "         And b.RiskCode in ('6805', '6810') " +
            "         And a.insuredno = ? ";
    //累计同一被保人市内公交意外险保额（6810和6805）
    public static String Sql6 = " Select 6 as flag , Nvl(Sum(c.Amnt),'0') as field " +
            "        From LCInsured a, LCPol b, LCDuty c " +
            "       Where 1 = 1 " +
            "         And a.ContNo = b.ContNo " +
            "         And a.ContNo = c.ContNo " +
            "         And b.PolNo = c.PolNo " +
            "         And b.cvalidate < ? " +
            "         And b.enddate > ? " +
            "         And b.Appflag in ('1' , '0','2') " +
            "         And c.DutyCode like '%02' " +
            "         And b.RiskCode in ('6805', '6810') " +
            "         And a.insuredno = ? ";
    //累计同一被保人长途汽车意外险保额（6810和6805）
    public static String Sql7 = " Select 7 as  flag, Nvl(Sum(c.Amnt),'0') as field " +
            "        From LCInsured a, LCPol b, LCDuty c " +
            "       Where 1 = 1 " +
            "         And a.ContNo = b.ContNo " +
            "         And a.ContNo = c.ContNo " +
            "         And b.PolNo = c.PolNo " +
            "         And b.cvalidate < ? " +
            "         And b.enddate > ? " +
            "         And b.Appflag in ('1' , '0','2') " +
            "         And c.DutyCode like '%06' " +
            "         And b.RiskCode in ('6805', '6810') " +
            "         And a.insuredno = ? ";
    //累计同一被保人出租车意外险保额（6810和6805）
    public static String Sql8 = " Select 8 as flag, Nvl(Sum(c.Amnt),'0')  as  field" +
            "        From LCInsured a, LCPol b, LCDuty c " +
            "       Where 1 = 1 " +
            "         And a.ContNo = b.ContNo " +
            "         And a.ContNo = c.ContNo " +
            "         And b.PolNo = c.PolNo " +
            "         And b.cvalidate < ? " +
            "         And b.enddate > ? " +
            "         And b.Appflag in ('1' , '0','2') " +
            "         And c.DutyCode like '%07' " +
            "         And b.RiskCode in ('6805', '6810') " +
            "         And a.insuredno = ? ";
    //累计同一被保人借款人意外险保额（6807）
    public static String Sql9 = "  Select 9 as flag, Nvl(Sum(b.Amnt),'0') as field" +
            "        From LCInsured a, LCPol b" +
            "       Where 1 = 1" +
            "         And a.ContNo = b.ContNo" +
            "         And b.cvalidate < ?" +
            "         And b.enddate > ?" +
            "         And b.appflag in ('1' , '0','2')" +
            "         And b.RiskCode = '6807'" +
            "         And a.insuredno = ?";
    //累计山东同一借款合同号所有借款人意外险保额（6807)
    public static String Sql10 = "   Select 10 as  flag, Nvl(sum(b.Amnt),'0') as field" +
            "        From LCPolicyinfo l, LCPol b" +
            "       Where l.Contno = b.Contno" +
            "         And b.ManageCom Like '8637%'" +
            "         And b.riskcode = '6807'" +
            "         And l.standbyflag5 = '04'" +
            "         And b.cvalidate < ? " +
            "         And b.enddate > ? " +
            "         And l.standbyflag1 = ? " +
            "         And Exists (Select 1" +
            "                From LCCont" +
            "               Where Contno = l.Contno" +
            "                 And Appflag in ('1' , '0','2')" +
            "                 ) ";
    //累计同一被保人航班飞机意外险保额（6810）
    public static String Sql11 = " Select 11 as flag,Nvl(Sum(c.Amnt),'0') as field" +
            "        From LCInsured a, LCPol b, LCDuty c" +
            "       Where 1 = 1" +
            "         And a.ContNo = b.ContNo" +
            "         And a.ContNo = c.ContNo" +
            "         And b.PolNo = c.PolNo" +
            "         And b.cvalidate < ?" +
            "         And b.enddate > ?" +
            "         And b.appflag in ('1' , '0','2')" +
            "         And c.DutyCode like '%05'" +
            "         And b.RiskCode = '6810'" +
            "         And a.insuredno = ?";
    //累计全国同一借款合同号下6807险种保单保额:
    public static String Sql12 = " Select 12 as flag, Nvl(Sum(b.Amnt),'0') as field" +
            "        From LCcont a, LCPol b" +
            "       Where 1 = 1" +
            "         And a.ContNo = b.ContNo" +
            "         And a.Appflag in ('1' , '0','2')" +
            "         And a.policyno is not null" +
            "         And a.policyno is not null" +
            "         And b.cvalidate < ?" +
            "         And b.enddate > ?" +
            "         And b.RiskCode in ('6807')" +
            "         And a.contno in" +
            "             (select f.contno" +
            "                from lcpolicyinfo f" +
            "               where f.contno = a.contno" +
            "                 and f.standbyflag1 = ?) ";
    //随E行交通意外保障计划累计保费
    public static String Sql13 = "SELECT 13 as flag, Nvl(SUM(a.prem),'0')  as field" +
            "        FROM lcpol a," +
            "          lcinsured b" +
            "        WHERE a.insuredno   = b.insuredno" +
            "        AND a.prtno         =b.prtno" +
            "        AND a.contno        =b.contno" +
            "        AND b.plancode IN ('SEA', 'SEB', 'SEC', 'SED')" +
            "        AND a.insuredno     = ?" +
            "        AND a.appflag      IN ('1', '2', '0')" +
            "        AND a.uwflag NOT   IN ('a', '2', '1')" +
            "        AND NOT EXISTS" +
            "          (SELECT CONTNO" +
            "          FROM LCCONT" +
            "          WHERE (STATE LIKE '1002%'" +
            "          OR STATE LIKE '1003%'" +
            "          OR STATE LIKE '1005%')" +
            "          AND CONTNO = a.CONTNO" +
            "          )";
    //公交出租车意外伤害保额
//    public static String Sql14 = "SELECT 14 as flag, Nvl(SUM(b.amnt),'0') as field" +
//            "              FROM lcpol a," +
//            "                lcduty b" +
//            "              WHERE a.polno     = b.polno" +
//            "              AND a.insuredno   = ?" +
//            "              AND b.dutycode   IN ('681007','680502')" +
//            "              AND a.appflag    IN ('1','2','0')" +
//            "              AND a.uwflag NOT IN ('a','2','1')" +
//            "              AND NOT EXISTS" +
//            "                (SELECT CONTNO" +
//            "                FROM LCCONT" +
//            "                WHERE (STATE LIKE '1002%'" +
//            "                OR STATE LIKE '1003%'" +
//            "                OR STATE LIKE '1005%')" +
//            "                AND CONTNO = a.CONTNO" +
//            "                )";
    //自驾车意外伤害累计责任保额
    public static String Sql15 = "SELECT 15 as flag, Nvl(SUM(b.amnt),'0') as field" +
            "              FROM lcpol a," +
            "                lcduty b" +
            "              WHERE a.polno     = b.polno" +
            "              AND a.insuredno   = ? " +
            "              AND b.dutycode   IN ('681001','680501')     " +
            "              AND a.appflag    IN ('1','2','0')" +
            "              AND a.uwflag NOT IN ('a','2','1')" +
            "              AND NOT EXISTS" +
            "                (SELECT CONTNO" +
            "                FROM LCCONT" +
            "                WHERE (STATE LIKE '1002%'" +
            "                OR STATE LIKE '1003%'" +
            "                OR STATE LIKE '1005%')" +
            "                AND CONTNO = a.CONTNO" +
            "                )";
    //累计随E驾交通意外保费
    public static String Sql16 = "SELECT 16 as flag, Nvl(SUM(a.prem),'0')  as field" +
            "        FROM lcpol a," +
            "          lcinsured b" +
            "        WHERE a.insuredno   = b.insuredno" +
            "        AND a.prtno         =b.prtno" +
            "        AND a.contno        =b.contno" +
            "        AND b.plancode IN ('SEG')" +
            "        AND a.insuredno     = ?" +
            "        AND a.appflag      IN ('1', '2', '0')" +
            "        AND a.uwflag NOT   IN ('a', '2', '1')" +
            "        AND NOT EXISTS" +
            "          (SELECT CONTNO" +
            "          FROM LCCONT" +
            "          WHERE (STATE LIKE '1002%'" +
            "          OR STATE LIKE '1003%'" +
            "          OR STATE LIKE '1005%')" +
            "          AND CONTNO = a.CONTNO" +
            "          )";
    //累计随E乘交通意外保费
    public static String Sql17 = "SELECT 17 as flag ,Nvl(SUM(a.prem),'0') as field" +
            "        FROM lcpol a," +
            "          lcinsured b" +
            "        WHERE a.insuredno   = b.insuredno" +
            "        AND a.prtno         =b.prtno" +
            "        AND a.contno        =b.contno" +
            "        AND b.plancode IN ('SEH')" +
            "        AND a.insuredno     = ?" +
            "        AND a.appflag      IN ('1', '2', '0')" +
            "        AND a.uwflag NOT   IN ('a', '2', '1')" +
            "        AND NOT EXISTS" +
            "          (SELECT CONTNO" +
            "          FROM LCCONT" +
            "          WHERE (STATE LIKE '1002%'" +
            "          OR STATE LIKE '1003%'" +
            "          OR STATE LIKE '1005%')" +
            "          AND CONTNO = a.CONTNO" +
            "          )";
    //累计至尊行交通意外保费
    public static String Sql18 = "SELECT 18 as flag, Nvl(SUM(a.prem),'0') as field" +
            "        FROM lcpol a," +
            "          lcinsured b" +
            "        WHERE a.insuredno   = b.insuredno" +
            "        AND a.prtno         =b.prtno" +
            "        AND a.contno        =b.contno" +
            "        AND b.plancode IN ('SEF')" +
            "        AND a.insuredno     = ? " +
            "        AND a.appflag      IN ('1', '2', '0')" +
            "        AND a.uwflag NOT   IN ('a', '2', '1')" +
            "        AND NOT EXISTS" +
            "          (SELECT CONTNO" +
            "          FROM LCCONT" +
            "          WHERE (STATE LIKE '1002%'" +
            "          OR STATE LIKE '1003%'" +
            "          OR STATE LIKE '1005%')" +
            "          AND CONTNO = a.CONTNO" +
            "          )";

    //累计随意飞交通意外保费
    public static String Sql19 = "SELECT 19 as flag, Nvl(SUM(a.prem),'0')  as field" +
            "        FROM lcpol a," +
            "          lcinsured b" +
            "        WHERE a.insuredno   = b.insuredno" +
            "        AND a.prtno         =b.prtno" +
            "        AND a.contno        =b.contno" +
            "        AND b.plancode IN ('SEI')" +
            "        AND a.insuredno     = ?" +
            "        AND a.appflag      IN ('1', '2', '0')" +
            "        AND a.uwflag NOT   IN ('a', '2', '1')" +
            "        AND NOT EXISTS" +
            "          (SELECT CONTNO" +
            "          FROM LCCONT" +
            "          WHERE (STATE LIKE '1002%'" +
            "          OR STATE LIKE '1003%'" +
            "          OR STATE LIKE '1005%')" +
            "          AND CONTNO = a.CONTNO" +
            "          )";
    //累计长途汽车意外伤害保额
    public static String Sql20 = "SELECT 20 as flag ,Nvl(SUM(b.amnt),'0') as field" +
            "              FROM lcpol a," +
            "                lcduty b        " +
            "              WHERE a.polno     = b.polno" +
            "              AND a.insuredno   = ? " +
            "              AND b.dutycode    = '681006'" +
            "              AND a.appflag    IN ('1','2','0')" +
            "              AND a.uwflag NOT IN ('a','2','1')" +
            "              AND NOT EXISTS" +
            "                (SELECT CONTNO" +
            "                FROM LCCONT" +
            "                WHERE (STATE LIKE '1002%'" +
            "                OR STATE LIKE '1003%'" +
            "                OR STATE LIKE '1005%')" +
            "                AND CONTNO = a.CONTNO" +
            "                )";
    //累计火车、地铁意外伤害保额
    public static String Sql21 = "SELECT 21 as flag, Nvl(SUM(b.amnt),'0')  as field" +
            "              FROM lcpol a," +
            "                lcduty b                                     " +
            "              WHERE a.polno     = b.polno" +
            "              AND a.insuredno   = ?" +
            "              AND b.dutycode   IN ('681003', '680503')" +
            "              AND a.appflag    IN ('1', '2', '0')" +
            "              AND a.uwflag NOT IN ('a', '2', '1')" +
            "              AND NOT EXISTS" +
            "                (SELECT CONTNO" +
            "                FROM LCCONT" +
            "                WHERE (STATE LIKE '1002%'" +
            "                OR STATE LIKE '1003%'" +
            "                OR STATE LIKE '1005%')" +
            "                AND CONTNO = a.CONTNO" +
            "                )";
    //累计轮船、摆渡意外伤害责任保额:
    public static String Sql22 = "SELECT 22 as flag, Nvl(SUM(b.amnt),'0')  as field" +
            "              FROM lcpol a," +
            "                lcduty b" +
            "              WHERE a.polno     = b.polno" +
            "              AND a.insuredno   = ?" +
            "              AND b.dutycode   IN ('681004', '680504')" +
            "              AND a.appflag    IN ('1', '2', '0')" +
            "              AND a.uwflag NOT IN ('a', '2', '1')" +
            "              AND NOT EXISTS" +
            "                (SELECT CONTNO" +
            "                FROM LCCONT" +
            "                WHERE (STATE LIKE '1002%'" +
            "                OR STATE LIKE '1003%'" +
            "                OR STATE LIKE '1005%')" +
            "                AND CONTNO = a.CONTNO" +
            "                )";
    //累计航班飞机意外伤害责任保额
    public static String Sql23 = "SELECT 23 as flag, Nvl(SUM(b.amnt),'0') as field" +
            "              FROM lcpol a," +
            "                lcduty b" +
            "              WHERE a.polno     = b.polno" +
            "              AND a.insuredno   = ?" +
            "              AND EXISTS (SELECT 1" +
            "          FROM LCINSURED D" +
            "         WHERE D.CONTNO = A.CONTNO" +
            "           AND D.INSUREDNO  = ?" +
            "           AND D.PLANCODE NOT IN ('SEK', 'SEJ'))" +
            "              AND b.dutycode   IN ('681005', '680505')" +
            "              AND a.appflag    IN ('1', '2', '0')" +
            "              AND a.uwflag NOT IN ('a', '2', '1')" +
            "              AND NOT EXISTS" +
            "                (SELECT CONTNO" +
            "                FROM LCCONT" +
            "                WHERE (STATE LIKE '1002%'" +
            "                OR STATE LIKE '1003%'" +
            "                OR STATE LIKE '1005%')" +
            "                AND CONTNO = a.CONTNO" +
            "                ) ";
    //累计同一被保人套餐有效保单数量
    public static String Sql24 = "   Select 24 as flag, Nvl(Count(0),'0') as field" +
            "        From LCInsured a, LCPol b" +
            "       Where 1 = 1" +
            "         And a.ContNo = b.ContNo" +
            "         And b.appflag in ('1' , '0','2')" +
            "         And b.cvalidate < ?" +
            "         And b.enddate > ?" +
            "         And exists (select '1'" +
            "                from lccont c" +
            "               where c.contno = a.contno" +
            "                 and c.appflag in ('1' , '0','2'))" +
            "         And a.planCode = ?" +
            "         And a.insuredno = ? ";
    //累计同一被保人套餐有效保单份数
    public static String Sql25 = "  Select 25 as flag, Nvl(Sum(c.mult),'0')  as field" +
            "        From LCCont c" +
            "       Where c.policyno is not null" +
            "        And c.ContNo in (Select b.ContNo" +
            "                            From LCInsured a, LCPol b" +
            "                           Where a.ContNo = b.ContNo" +
            "                             And b.appflag in ('1' , '0','2')" +
            "                             And b.cvalidate < ?" +
            "                             And b.enddate > ?" +
            "                             And a.insuredno = ?" +
            "                             And a.plancode = ?) ";
    //累计同一中介机构同一借款合同号的不同被保人的数量
    public static String AgencyBorrowSql = " Select 26 as flag, Nvl(Count(distinct p.insuredno), '0')  as field" +
            "   From lcpolicyinfo p, lcpol b, lccont ll" +
            "  where 1 = 1" +
            "    And ll.contno = p.contno" +
            "    And ll.appflag <> '4'" +
            "    And p.contno = b.contno" +
            "    And p.standbyflag5 = '04'" +
            "    And b.cvalidate < ? " +
            "    And b.enddate > ? " +
            "    And p.standbyflag1 = ? " +
            "    And not exists (Select '1'" +
            "           From LCCont c, LCInsured d" +
            "          Where p.contno = c.contno" +
            "            And c.contno = d.contno" +
            "            And p.insuredno = c.insuredno" +
            "            And c.policyno is not null" +
            "            AND d.insuredno = ? )";
    //累计同一中介机构同一借款合同号的相同被保人的数量
/*    public static String AccAgencyBorrowsql = "Select distinct p.insuredno " +
            "  From lcpolicyinfo p, lcpol b, lccont lc" +
            " Where 1 = 1" +
            "   And p.standbyflag5 = '04'" +
            "   And lc.appflag <> '4'" +
            "   And p.contno = lc.contno" +
            "   And p.contno = b.contno" +
            "   And b.cvalidate < ? " +
            "   And b.enddate > ? " +
            "   And p.standbyflag1 = ? ";*/

    //SEE、SEN保险计划份数
    public static String pastSeeSenNumsql = "SELECT 27 as flag , nvl(COUNT(1),'0') as field  FROM lcpol a,lcinsured b WHERE " +
            "a.insuredno   = b.insuredno  AND a.prtno  = b.prtno   " +
            "AND a.contno = b.contno  AND b.plancode IN ('SEE','SEN')  " +
            "AND a.insuredno = ?  AND a.appflag IN ('1', '2', '0') " +
            "AND a.uwflag NOT IN ('a', '2', '1')  AND NOT EXISTS " +
            "    (SELECT CONTNO " +
            "    FROM LCCONT" +
            "    WHERE (STATE LIKE '1002%'" +
            "    OR STATE LIKE '1003%'" +
            "    OR STATE LIKE '1005%')" +
            "    AND CONTNO = a.CONTNO) ";

    //SEO保险计划份数
    public static String pastSeoNumsql = "select 28 as flag, nvl(count(1),'0')  as field"  +
            "          from lcpol a, lcinsured b" +
            "         where a.insuredno = b.insuredno" +
            "           and a.prtno = b.prtno" +
            "           and a.contno = b.contno" +
            "           and b.plancode in ('SEO')" +
            "           and a.insuredno = ?" +
            "           and a.appflag in ('1', '2', '0')" +
            "           and a.uwflag not in ('a', '2', '1')" +
            "           AND NOT EXISTS" +
            "         (SELECT CONTNO" +
            "                  FROM LCCONT" +
            "                 WHERE (STATE LIKE '1002%' OR STATE LIKE '1003%' OR" +
            "                       STATE LIKE '1005%')" +
            "                   AND CONTNO = a.CONTNO)";
    //SEL,SEM保险计划份数
    public static String pastSelSemNumsql = "select 29 as flag, nvl(count(1),'0') as field" +
            "          from lcpol a, lcinsured b" +
            "         where a.insuredno = b.insuredno" +
            "           and a.prtno = b.prtno" +
            "           and a.contno = b.contno" +
            "           and b.plancode in ('SEL','SEM')" +
            "           and a.insuredno = ?" +
            "           and a.appflag in ('1', '2', '0')" +
            "           and a.uwflag not in ('a', '2', '1')" +
            "           AND NOT EXISTS" +
            "         (SELECT CONTNO" +
            "                  FROM LCCONT" +
            "                 WHERE (STATE LIKE '1002%' OR STATE LIKE '1003%' OR" +
            "                       STATE LIKE '1005%')" +
            "                   AND CONTNO = a.CONTNO)";



    //市内公交车意外伤害责任
    public static String Sql30 = "SELECT 30 as flag, Nvl(SUM(b.amnt),'0') as field" +
            "              FROM lcpol a," +
            "                lcduty b" +
            "              WHERE a.polno     = b.polno" +
            "              AND a.insuredno   = ?" +
            "              AND b.dutycode  = '681002'" +
            "              AND a.appflag    IN ('1','2','0')" +
            "              AND a.uwflag NOT IN ('a','2','1')" +
            "              AND NOT EXISTS" +
            "                (SELECT CONTNO" +
            "                FROM LCCONT" +
            "                WHERE (STATE LIKE '1002%'" +
            "                OR STATE LIKE '1003%'" +
            "                OR STATE LIKE '1005%')" +
            "                AND CONTNO = a.CONTNO" +
            "                )";
    //出租车意外伤害责任
    public static String Sql31 = "SELECT 31 as flag, Nvl(SUM(b.amnt),'0') as field" +
            "              FROM lcpol a," +
            "                lcduty b" +
            "              WHERE a.polno     = b.polno" +
            "              AND a.insuredno   = ?" +
            "              AND b.dutycode  = '681007'" +
            "              AND a.appflag    IN ('1','2','0')" +
            "              AND a.uwflag NOT IN ('a','2','1')" +
            "              AND NOT EXISTS" +
            "                (SELECT CONTNO" +
            "                FROM LCCONT" +
            "                WHERE (STATE LIKE '1002%'" +
            "                OR STATE LIKE '1003%'" +
            "                OR STATE LIKE '1005%')" +
            "                AND CONTNO = a.CONTNO" +
            "                )";
    //SEP保险计划份数
    public static String pastSEPNumsql = "select 32 as flag, nvl(count(1),'0')  as field"  +
            "          from lcpol a, lcinsured b" +
            "         where a.insuredno = b.insuredno" +
            "           and a.prtno = b.prtno" +
            "           and a.contno = b.contno" +
            "           and b.plancode in ('SEP1','SEP3','SEP6')" +
            "           and a.insuredno = ?" +
            "           and a.appflag in ('1', '2', '0')" +
            "           and a.uwflag not in ('a', '2', '1')" +
            "           AND NOT EXISTS" +
            "         (SELECT CONTNO" +
            "                  FROM LCCONT" +
            "                 WHERE (STATE LIKE '1002%' OR STATE LIKE '1003%' OR" +
            "                       STATE LIKE '1005%')" +
            "                   AND CONTNO = a.CONTNO)";

    //累计同一被保人JSJXBC投保份数
    public static String pastJSJXBCNumsql = "select 33 as flag, nvl(Sum(a.mult),'0')  as field"  +
            "          from lccont a, lcinsured b" +
            "         where a.insuredno = b.insuredno" +
            "           and a.prtno = b.prtno" +
            "           and a.contno = b.contno" +
            "           and b.plancode in ('JSJXBC')" +
            "           and a.insuredno = ?" +
            "           and a.appflag in ('1', '2', '0')" +
            "           and a.uwflag not in ('a', '2', '1')" +
            "           AND NOT EXISTS" +
            "         (SELECT CONTNO" +
            "                  FROM LCCONT" +
            "                 WHERE (STATE LIKE '1002%' OR STATE LIKE '1003%' OR" +
            "                       STATE LIKE '1005%')" +
            "                   AND CONTNO = a.CONTNO)";
    //累计SESXNH份数
    public static String pastSESXNHNumsql = "select 34 as flag, nvl(count(1),'0')  as field"  +
            "          from lcpol a, lcinsured b" +
            "         where a.insuredno = b.insuredno" +
            "           and a.prtno = b.prtno" +
            "           and a.contno = b.contno" +
            "           and b.plancode in ('SESXNH')" +
            "           and a.insuredno = ?" +
            "           and a.appflag in ('1', '2', '0')" +
            "           and a.uwflag not in ('a', '2', '1')" +
            "           AND NOT EXISTS" +
            "         (SELECT CONTNO" +
            "                  FROM LCCONT" +
            "                 WHERE (STATE LIKE '1002%' OR STATE LIKE '1003%' OR" +
            "                       STATE LIKE '1005%')" +
            "                   AND CONTNO = a.CONTNO)";

}
