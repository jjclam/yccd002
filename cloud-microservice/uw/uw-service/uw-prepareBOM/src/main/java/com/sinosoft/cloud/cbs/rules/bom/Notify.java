package com.sinosoft.cloud.cbs.rules.bom;


/**
 * 告知
 * @author dingfan
 *
 */
public class Notify{
	/**
	 * 告知代码
	 */
	private String code;
	/**
	 * 告知名称
	 */
	private String name;
	/**
	 * 告知内容
	 */
	private String contents;
	
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getContents() {
		return contents;
	}
	public void setContents(String contents) {
		this.contents = contents;
	}
}
