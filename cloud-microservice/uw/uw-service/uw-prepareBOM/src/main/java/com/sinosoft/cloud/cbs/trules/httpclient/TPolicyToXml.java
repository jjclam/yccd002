package com.sinosoft.cloud.cbs.trules.httpclient;



import com.sinosoft.cloud.cbs.trules.bom.*;
import com.sinosoft.cloud.cbs.trules.bom.Risk;
import com.sinosoft.utility.ExceptionUtils;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.lang.reflect.Field;


/**
 * 将Policy对象转换为xml，封装成请求报文
 *
 * @author dingfan
 */
public class TPolicyToXml {
    private static Log logger = LogFactory.getLog(TPolicyToXml.class);
    private static XStream xstream = null;

    public static String getXml(TPolicy tPolicy) {
        if (xstream == null) {
            //报文格式
            xstream = new XStream(new DomDriver());
            //处理对象属性为null、Map类型的情况
           // xstream.registerConverter(new TNullConverter());
            //xstream.registerConverter(new DateConverter());
            xstream.alias("AgentCert", AgentCert.class);
            xstream.alias("duty", Duty.class);
            xstream.alias("LcInsured", LcInsured.class);
            xstream.alias("ProdGroup", ProdGroup.class);
            xstream.alias("Product", Product.class);
            xstream.alias("risk", Risk.class);
            xstream.alias("TAgentInfo", TAgentInfo.class);
            xstream.alias("TApplicantInfo", TApplicantInfo.class);
            xstream.alias("TBeneficiaryInfos", TBeneficiaryInfos.class);
            xstream.alias("tBeneficiaryInfo", TBeneficiaryInfo.class);
            xstream.alias("TInsurdInfo", TInsurdInfo.class);
            xstream.alias("TPayInfo", TPayInfo.class);
            xstream.alias("TPolicy", TPolicy.class);
            xstream.alias("TProductInfo", TProductInfo.class);

            //删除部分节点
			/*xstream.addImplicitCollection(Policy.class, "insuredList");
			xstream.addImplicitCollection(Policy.class, "beneficiaryList");
			xstream.addImplicitCollection(Policy.class, "riskList");
			xstream.addImplicitCollection(Policy.class, "clientInfoList");*/
        }

        //javabean转xml
        String error = null;
        String requestXml = xstream.toXML(tPolicy);
        if (requestXml.indexOf("<Policy>") == -1) {
            error = "ODM请求报文转换失败";
            logger.debug("ODM请求报文初始化:" + requestXml);
        }
        if (tPolicy == null) {
            error += ",policy对象为null";
            logger.debug("policy对象实例化失败,policy=null");
        }
        //封装报文
        try {
            Field field[] = TPolicy.class.getDeclaredFields();
            for (int i = 0; i < field.length; i++) {
                String filedStr = field[i].getName();
                StringBuffer strStart = new StringBuffer("<" + filedStr + ">");
                StringBuffer strXmlStart = new StringBuffer("<par:" + filedStr + "><" + filedStr + ">");
                requestXml = requestXml.replaceAll(strStart.toString(), strXmlStart.toString());
                StringBuffer strEnd = new StringBuffer("</" + filedStr + ">");
                StringBuffer strXmlEnd = new StringBuffer("</" + filedStr + "></par:" + filedStr + ">");
                requestXml = requestXml.replaceAll(strEnd.toString(), strXmlEnd.toString());
            }
            String xmlStart = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:web=\"http://www.ibm.com/rules/decisionservice/WebluarRuleApp/WebluarRule\" xmlns:par=\"http://www.ibm.com/rules/decisionservice/WebluarRuleApp/WebluarRule/param\">" +
                    "<soapenv:Header/>" +
                    "<soapenv:Body>" +
                    "<web:WebluarRuleRequest>" +
                    "<web:DecisionID>" + System.currentTimeMillis() + "</web:DecisionID>";
            String xmlEnd = "</web:WebluarRuleRequest>" +
                    "</soapenv:Body>" +
                    "</soapenv:Envelope>";
            String bnf ="<par:TBeneficiaryInfos>" +
                    "<tBeneficiaryInfos>" +
                    "<tBeneficiaryInfo>" +
                    "<beneficiaryName>借意险</beneficiaryName>" +
                    "<beneficiaryCerttype/>" +
                    "<beneficiaryCertno/>" +
                    "<relaToInsurd/>" +
                    "<beneficiarySn/>" +
                    "<beneficiaryPropor/>" +
                    "<beneficiaryType/>" +
                    "<bnfSex/>" +
                    "<bnfBirthday/>" +
                    "<bnfBlackIdFlag/>" +
                    "<bnfBlackFullFlag/>" +
                    "<bnfBlackFlag1/>" +
                    "<bnfBlackFlag2/>" +
                    "<bnfBlackFlag3/>" +
                    "<bnfBlackFlag4/>" +
                    "<bnfBlackFlag5/>" +
                    "</tBeneficiaryInfo>" +
                    "</tBeneficiaryInfos>" +
                    "</par:TBeneficiaryInfos>";
            requestXml = requestXml.replaceAll("<TPolicy>", xmlStart);
            if(!requestXml.contains("TBeneficiaryInfos")){
                logger.debug("法定受益人拼装受益人");
                xmlEnd=bnf+xmlEnd;
            }
            requestXml = requestXml.replaceAll("</TPolicy>", xmlEnd);
        } catch (Exception e) {
            logger.error(ExceptionUtils.exceptionToString(e));
            e.printStackTrace();
            OutputStream out = new ByteArrayOutputStream();
            PrintStream ps = new PrintStream(out);
            e.printStackTrace(ps);
            requestXml = "<error>" + error + "</error><message>" + out.toString() + "</message>";
        }
        return requestXml;
    }
}
