package com.sinosoft.cloud.cbs.rules.bom;

import java.util.Date;

/**
 * 受益人信息
 * 
 * @author dingfan
 * 
 */
public class Beneficiary{
	/**
	 * 年龄
	 */
	private int age;

	/**
	 * 受益份额
	 */
	private double beneficialPercent;

	/**
	 * 受益类别
	 */
	private String beneficialType;

	/**
	 * 受益人姓名
	 */
	private String beneficiaryName;

	/**
	 * 受益顺序
	 */
	private int beneficialOrder;

	/**
	 * 出生日期
	 */
	private Date birthday;

	/**
	 * 性别
	 */
	private String sex;

	/**
	 * 证件号
	 */
	private String identityCode;

	/**
	 * 证件类型
	 */
	private String identityType;

	/**
	 * 证件有效期
	 */
	private String identityValidityTerm;

	/**
	 * 与被保险人关系
	 */
	private String realtionWithInsured;

	/**
	 * 联系地址
	 */
	private String homeAddr;

	/**
	 * 邮编
	 */
	private String postalPostcode;

	/**
	 * 联系电话
	 */
	private String phone;
	
	/**
	 * 受益人客户号
	 */
	private String clientNo;
	
	/**
	 * 被保人客户号
	 */
	private String insuredClientNo;
	
	/**
	 * 保单险种号
	 */
	private String polno;
	
	
	/**
	 * 黑名单1-证件号与和黑名单相同,不分国籍,>0触发,参数:一个contno
	 * {this} 的证件号与和黑名单相同
	 */
	private boolean bnfBlackIdFlag;

	/**
	 * 黑名单2-受益人名1部分组成,受益人名与黑名单表四个名字对比,>0触发,参数:4个contno
	 * {this}的名字与黑名单名字和生日相同(名字为一部分)
	 */
	private boolean bnfBlackFlag1;
	
	
	/**
	 * 黑名单3-受益人名2部分组成,受益人名与黑名单表2个名字对比,>0触发,参数:2个contno
	 * 有一个name相同,只比较firstname和surname，且出生日期相同
	 * {this}的名字与黑名单名字和生日相同(名字为两部分)
	 */
	private boolean bnfBlackFlag2;
	
	/**
	 * 黑名单4-受益人名2部分组成,受益人名与黑名单表2个名字对比,>=2触发,参数:2个contno
	 * firstname和surnname都要相同.无生日限制
	 * {this} 的名字与黑名单名字和生日相同(名字为两部分,无生日限制)
	 */
	private boolean bnfBlackFlag3;
	
	/**
	 * 黑名单5-受益人名4部分组成,受益人名与黑名单表3个名字对比,>=0触发,参数:3个contno
	 * firstname,middlename,surname,有生日限制
	 * {this} 的名字与黑名单名字两个以上相同(名字为四部分)
	 */
	private boolean bnfBlackFlag4;
	
	/**
	 * 黑名单6-受益人名4部分组成,受益人名与黑名单表3个名字对比,>=2触发,参数:3个contno
	 * firstname,middlename,surname,无生日限制
	 * {this} 的名字与黑名单相同(受益人名四部分构成无生日限制)
	 */
	private boolean bnfBlackFlag5;
	
	
	
	
	public Beneficiary() {
		super();
	}
	
	
	

	public boolean isBnfBlackFlag1() {
		return bnfBlackFlag1;
	}




	public void setBnfBlackFlag1(boolean bnfBlackFlag1) {
		this.bnfBlackFlag1 = bnfBlackFlag1;
	}




	public boolean isBnfBlackFlag2() {
		return bnfBlackFlag2;
	}




	public void setBnfBlackFlag2(boolean bnfBlackFlag2) {
		this.bnfBlackFlag2 = bnfBlackFlag2;
	}




	public boolean isBnfBlackFlag3() {
		return bnfBlackFlag3;
	}




	public void setBnfBlackFlag3(boolean bnfBlackFlag3) {
		this.bnfBlackFlag3 = bnfBlackFlag3;
	}




	public boolean isBnfBlackFlag4() {
		return bnfBlackFlag4;
	}




	public void setBnfBlackFlag4(boolean bnfBlackFlag4) {
		this.bnfBlackFlag4 = bnfBlackFlag4;
	}




	public boolean isBnfBlackFlag5() {
		return bnfBlackFlag5;
	}




	public void setBnfBlackFlag5(boolean bnfBlackFlag5) {
		this.bnfBlackFlag5 = bnfBlackFlag5;
	}




	public boolean isBnfBlackIdFlag() {
		return bnfBlackIdFlag;
	}




	public void setBnfBlackIdFlag(boolean bnfBlackIdFlag) {
		this.bnfBlackIdFlag = bnfBlackIdFlag;
	}




	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public int getBeneficialOrder() {
		return beneficialOrder;
	}

	public void setBeneficialOrder(int beneficialOrder) {
		this.beneficialOrder = beneficialOrder;
	}

	public double getBeneficialPercent() {
		return beneficialPercent;
	}

	public void setBeneficialPercent(double beneficialPercent) {
		this.beneficialPercent = beneficialPercent;
	}

	public String getBeneficialType() {
		return beneficialType;
	}

	public void setBeneficialType(String beneficialType) {
		this.beneficialType = beneficialType;
	}

	public String getBeneficiaryName() {
		return beneficiaryName;
	}

	public void setBeneficiaryName(String beneficiaryName) {
		this.beneficiaryName = beneficiaryName;
	}

	public Date getBirthday() {
		return birthday;
	}

	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}

	public String getClientNo() {
		return clientNo;
	}

	public void setClientNo(String clientNo) {
		this.clientNo = clientNo;
	}

	public String getHomeAddr() {
		return homeAddr;
	}

	public void setHomeAddr(String homeAddr) {
		this.homeAddr = homeAddr;
	}

	public String getIdentityCode() {
		return identityCode;
	}

	public void setIdentityCode(String identityCode) {
		this.identityCode = identityCode;
	}

	public String getIdentityType() {
		return identityType;
	}

	public void setIdentityType(String identityType) {
		this.identityType = identityType;
	}
	public String getIdentityValidityTerm() {
		return identityValidityTerm;
	}

	public void setIdentityValidityTerm(String identityValidityTerm) {
		this.identityValidityTerm = identityValidityTerm;
	}

	public String getInsuredClientNo() {
		return insuredClientNo;
	}

	public void setInsuredClientNo(String insuredClientNo) {
		this.insuredClientNo = insuredClientNo;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getPostalPostcode() {
		return postalPostcode;
	}

	public void setPostalPostcode(String postalPostcode) {
		this.postalPostcode = postalPostcode;
	}

	public String getRealtionWithInsured() {
		return realtionWithInsured;
	}

	public void setRealtionWithInsured(String realtionWithInsured) {
		this.realtionWithInsured = realtionWithInsured;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public String getPolno() {
		return polno;
	}

	public void setPolno(String polno) {
		this.polno = polno;
	}
	
}
