package com.sinosoft.cloud.cbs.bl;

import com.sinosoft.cache.dao.impl.RedisCommonDao;
import com.sinosoft.cloud.bl.AbstractBL;
import com.sinosoft.cloud.cbs.trules.util.DataSourceName;
import com.sinosoft.cloud.cbs.trules.util.MulDataExexSql;
import com.sinosoft.cloud.rest.TradeInfo;
import com.sinosoft.lis.entity.*;
import com.sinosoft.utility.ExceptionUtils;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @Author:wangshuliang
 * @Description: 累计6807附加险保额
 * @Date:Created in 9:53 2018/7/2
 * @Modified by:
 */
@Component
public class AmntHistory6807BL extends AbstractBL {
    private final Log logger = LogFactory.getLog(this.getClass());
    @Autowired
    RedisCommonDao redisCommonDao;

    @Override
    public TradeInfo dealData(TradeInfo tradeInfo) {
        List<LCPolPojo> lcPolPojoList = (List<LCPolPojo>) tradeInfo.getData(LCPolPojo.class.getName());
        LCPolPojo mainPol=null;
        for(LCPolPojo lcPolPojo:lcPolPojoList){
            if(lcPolPojo.getPolNo().equals(lcPolPojo.getMainPolNo())){
                mainPol=lcPolPojo;
            }
        }
        LMRiskProcessPojo lmRiskProcessPojo = (LMRiskProcessPojo) tradeInfo.getData(LMRiskProcessPojo.class.getName());
        if (lmRiskProcessPojo == null) {
            lmRiskProcessPojo = redisCommonDao.getEntityRelaDB(LMRiskProcessPojo.class, mainPol.getRiskCode());
        }
        if(lmRiskProcessPojo!=null&&"6807".equals(lmRiskProcessPojo.getRiskCode())) {
            LCContPojo lcContPojo = (LCContPojo) tradeInfo.getData(LCContPojo.class.getName());
            List<LCInsuredPojo> lcInsuredPojos = (List<LCInsuredPojo>) tradeInfo.getData(LCInsuredPojo.class.getName());
            LCInsuredPojo lcInsuredPojo = lcInsuredPojos.get(0);
//            LCPolicyInfoPojo lcPolicyInfoPojo = (LCPolicyInfoPojo) tradeInfo.getData(LCPolicyInfoPojo.class.getName());
            LCPolPojo lcPolPojoAdd = null;

            String mainRiskcode = null;
            //判断是否存在附加险 如果存在附加险则计算附加险累计保额 如果不存在直接返回0
            List<LDPlanRiskPojo> LDPlanRiskPojos = redisCommonDao.findByIndexKeyRelaDB(LDPlanRiskPojo.class, "ContPlanCode", lcInsuredPojo.getContPlanCode());

            for (LDPlanRiskPojo ldPlanRiskPojo : LDPlanRiskPojos) {
                if (!ldPlanRiskPojo.getMainRiskCode().equals(ldPlanRiskPojo.getRiskCode())) {
                    lcPolPojoAdd = new LCPolPojo();
                    lcPolPojoAdd.setRiskCode(ldPlanRiskPojo.getRiskCode());
                }
            }
            if (lcPolPojoAdd == null) {
                logger.debug("套餐编码为："+lcInsuredPojo.getContPlanCode()+",不存在附加险保额那么返回：pastAmnt=0");
                tradeInfo.addData("pastAmnt", "0.0");
                return tradeInfo;
            }
            //如果存附加险
            String sql = "Select Nvl(Sum(b.Amnt),'0') " +
                    "        From LCcont a, LCPol b" +
                    "       Where 1 = 1" +
                    "         And a.ContNo = b.ContNo" +
                    "         And b.cvalidate < ? " +
                    "         And b.enddate > ? " +
                    "         And a.Appflag in ('1' , '0')" +
                    "         And a.policyno is not null" +
                    "         And b.RiskCode = ? " +
                    "         and a.insuredname = ?" +
                    "         and a.insuredsex = ?" +
                    "         and a.insuredbirthday = ?" +
                    "         and a.insuredidtype = ?" +
                    "         and a.insuredidno = ? ";
            logger.debug(lcContPojo.getContNo() + "6807附加险查询sql为：" + sql);
            VData tVData14 = new VData();
            TransferData tParam14 = new TransferData();
            tParam14.setNameAndValue("cvalidate", "string:" + mainPol.getEndDate());
            tParam14.setNameAndValue("enddate", "string:" + mainPol.getCValiDate());
            tParam14.setNameAndValue("riskcode", "string:" + lcPolPojoAdd.getRiskCode());//附加险
            tParam14.setNameAndValue("insuredname", "string:" + lcContPojo.getInsuredName());
            tParam14.setNameAndValue("insuredsex", "string:" + lcContPojo.getInsuredSex());
            tParam14.setNameAndValue("insuredbirthday", "string:" + lcContPojo.getInsuredBirthday());
            tParam14.setNameAndValue("insuredidtype", "string:" + lcContPojo.getInsuredIDType());
            tParam14.setNameAndValue("insuredidno", "string:" + lcContPojo.getInsuredIDNo());
            tVData14.add(sql);
            tVData14.add(tParam14);
            MulDataExexSql mulDataExexSql = new MulDataExexSql();
            SSRS SSRS14 = null;
            try {
                SSRS14 = mulDataExexSql.excuteSql(tVData14, DataSourceName.OLD.getSourceName());
            } catch (Exception e) {
                logger.error(ExceptionUtils.exceptionToString(e));
                logger.debug("1002计算附加险保额出错" + tradeInfo.toString());
                tradeInfo.addError("1002计算附加险保额出错");
                return tradeInfo;
            }
            if (SSRS14 != null && SSRS14.getMaxRow() > 0) {
                //公交出租车意外伤害保额
                tradeInfo.addData("pastAmnt", SSRS14.GetText(1, 1));
                logger.debug(lcContPojo.getContNo() + "的附加险信息为:" + lcPolPojoAdd.getRiskCode() + "并且累计历史保额为：" + SSRS14.GetText(1, 1));
            }
        }
        return tradeInfo;
    }
}
