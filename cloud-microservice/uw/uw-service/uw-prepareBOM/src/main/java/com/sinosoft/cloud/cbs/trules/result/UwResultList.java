package com.sinosoft.cloud.cbs.trules.result;

import java.util.List;




public class UwResultList {
	private List<UwResult> uwresult;

	public List<UwResult> getUwresult() {
		return uwresult;
	}

	public void setUwresult(List<UwResult> uwresult) {
		this.uwresult = uwresult;
	}
}
