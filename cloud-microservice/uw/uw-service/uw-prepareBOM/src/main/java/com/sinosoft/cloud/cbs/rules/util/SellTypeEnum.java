package com.sinosoft.cloud.cbs.rules.util;

/**
 * 销售类型枚举
 */
public enum SellTypeEnum {
    GetSelf("ABCSELF","04"),GetMoblfe("ABCMOBILE","05"),GetSupotc("ABCSUPOTC","10"),GetCotc("ABCOTC","07"),
    GetEbank("ABCEBANK","08"),GetEstwd("ABCESTWD","09"),GetWechat("WECHAT","12"),GetMip("MIP","MIP");

    private String sellType;
    private String z_sellType;

    SellTypeEnum( String sellType,String z_sellType){
        this.sellType=sellType;
        this.z_sellType=z_sellType;

    }

    public String getSellType() {
        return sellType;
    }

    public void setSellType(String sellType) {
        this.sellType = sellType;
    }

    public String getZ_sellType() {
        return z_sellType;
    }

    public void setZ_sellType(String z_sellType) {
        this.z_sellType = z_sellType;
    }

    public static String getValue(String sellType){
        for(SellTypeEnum sellTypeEnum:SellTypeEnum.values()){
            if(sellTypeEnum.getSellType().equals(sellType)){
                return sellTypeEnum.getZ_sellType();
            }

        }
        return "";
    }
}

