package com.sinosoft.cloud.cbs.riskamnt;


import com.sinosoft.cache.dao.impl.RedisCommonDao;
import com.sinosoft.cloud.bl.AbstractBL;
import com.sinosoft.cloud.nb.service.redis.NBRedisCommon;
import com.sinosoft.cloud.rest.TradeInfo;
import com.sinosoft.lis.entity.*;
import com.sinosoft.utility.*;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.*;

/**
 * @Author: zhuyiming
 * @Description:
 * @Date: Created in 2:09 2017/9/25
 * @Modified By
 */
@Service
public class GetAccumBL extends AbstractBL {
    private final Log logger = LogFactory.getLog(this.getClass());

    @Autowired
    NBRedisCommon nbRedisCommon;

    @Autowired
    RedisCommonDao redisCommonDao;

    @Value("${cloud.uw.barrier.control}")
    private String barrier;

    /**
     * L：累计寿险风险保额 ，H：累计重疾风险保额 ，Y：累计意外险风险保额 CL:寿险保单保额 ，CH:累计重疾保单保额 ，CY:意外险保单保额
     * PL：寿险体检风险保额 ，LI：人身险保额 ，SR：累计风险保额 M：住院医疗风险保额 ，N：意外医疗风险保额 ，J：津贴险风险保额
     * ZJ:累计自驾车风险保额 ，HC:防癌险风险保额 ，ZB:累计再保风险保额
     */
    private static final String[] RISKTYPE = {"L", "H", "Y", "CL", "CH", "CY",
            "PL", "LI", "SR", "M", "N", "J", "ZJ", "HC", "ZB"};

    /**
     * BL：累计寿险保单保额 ， BH：累计健康险保单保额 ， BY：累计意外险保单保额 ， BJ：累计自驾车保单保额 ， BC：累计防癌险保单保额
     */
    private static final String[] CONTRISKTYPE = {"BL", "BH", "BY", "BJ", "BC"};

    /**
     * 1：寿险风险保额 ， 2：重疾险风险保额 ， 4：意外险风险保额
     */
    private static final String[] HEALTHTYPE = {"1", "2", "4"};

    @Override
    public TradeInfo dealData(TradeInfo tradeInfo) {
        logger.debug("开始查询风险保额！");
        List<RiskAmntInfoPojo> riskAmntInfos = new ArrayList<>();
        if("true".equals(barrier)){
            //挡板提数
            getBarrierData(tradeInfo, riskAmntInfos);
        }else {
            //累计类提数
            getAccumExtract(tradeInfo, riskAmntInfos);
        }
        if(tradeInfo.hasError()){
            logger.debug("提取累计风险保额失败！");
            tradeInfo.addError("提取累计风险保额失败！");
            return tradeInfo;
        }
        logger.debug("提取累计风险保额成功！");
        //本单风险保额提数
        getContExtract(tradeInfo, riskAmntInfos);
        if(tradeInfo.hasError()){
            logger.debug("提取本单风险保额失败！");
            tradeInfo.addError("提取本单风险保额失败！");
            return tradeInfo;
        }
        logger.debug("提取本单风险保额成功！");
        tradeInfo.addData(RiskAmntInfoPojo.class.getName(), riskAmntInfos);
        return tradeInfo;
    }

    public void getBarrierData(TradeInfo tradeInfo, List<RiskAmntInfoPojo> riskAmntInfos){
        List<LCInsuredPojo> insuredList = (List) tradeInfo.getData(LCInsuredPojo.class.getName());
        logger.debug("保单号：" + insuredList.get(0).getContNo() + "，风险保额挡板启动！");
        Reflections reflections = new Reflections();
        //投被保人风险保额
        for (int i = 0; i <= insuredList.size(); i++) {
            RiskAmntInfoPojo riskAmntInfo = new RiskAmntInfoPojo();
            riskAmntInfo.setRiskTypeL("0");
            riskAmntInfo.setRiskTypeH("0");
            riskAmntInfo.setRiskTypeY("0");
            riskAmntInfo.setRiskTypeCL("0");
            riskAmntInfo.setRiskTypeCH("0");
            riskAmntInfo.setRiskTypeCY("0");
            riskAmntInfo.setRiskTypePL("0");
            riskAmntInfo.setRiskTypeLI("0");
            riskAmntInfo.setRiskTypeSR("0");
            riskAmntInfo.setRiskTypeM("0");
            riskAmntInfo.setRiskTypeN("0");
            riskAmntInfo.setRiskTypeJ("0");
            riskAmntInfo.setRiskTypeZJ("0");
            riskAmntInfo.setRiskTypeHC("0");
            riskAmntInfo.setRiskTypeZB("0");
            riskAmntInfo.setContRiskTypeBL("0");
            riskAmntInfo.setContRiskTypeBH("0");
            riskAmntInfo.setContRiskTypeBY("0");
            riskAmntInfo.setContRiskTypeBJ("0");
            riskAmntInfo.setContRiskTypeBC("0");
            riskAmntInfo.setHeathType1("0");
            riskAmntInfo.setHeathType2("0");
            riskAmntInfo.setHeathType4("0");
            /** 身故累计*/
            riskAmntInfo.setAppRiskCodeDeath("0");
            riskAmntInfos.add((RiskAmntInfoPojo) reflections.transFields(new RiskAmntInfoPojo(), riskAmntInfo));
        }
    }

    /**
     * 累计提数 riskCode等于null时，按照险类来累计； riskCode等于险种代码时，按险种来累计。
     * @param tradeInfo 包含所有业务数据
     * @param riskAmntInfos 风险保额存储容器
     * @return
     */
    public void getAccumExtract(TradeInfo tradeInfo, List<RiskAmntInfoPojo> riskAmntInfos) {
        // 被保人列表
        List<LCInsuredPojo> insuredList = (List) tradeInfo.getData(LCInsuredPojo.class.getName());
        // 投保人
        LCAppntPojo applicant = (LCAppntPojo) tradeInfo.getData(LCAppntPojo.class.getName());

        long beginTime = System.currentTimeMillis();
        // 拼接投保人累计
        logger.debug("保单号：" + applicant.getContNo() + "，开始提取投保人累计！");
        getAppntInfo(tradeInfo, applicant, riskAmntInfos);
        if(tradeInfo.hasError()){
            logger.debug("提取投保人累计失败！");
            tradeInfo.addError("提取投保人累计失败！");
            return;
        }
        logger.debug("提取投保人累计成功！");
        //拼接被保人累計
        logger.debug("保单号：" + applicant.getContNo() + "，开始提取被保人累计！");
        getInsuredInfo(tradeInfo, insuredList, riskAmntInfos);
        if(tradeInfo.hasError()){
            logger.debug("提取被保人累计失败！");
            tradeInfo.addError("提取被保人累计失败！");
            return;
        }
        logger.debug("提取被保人累计成功");
        logger.debug("累计提数：" + (System.currentTimeMillis() - beginTime) / 1000.0
                + "s");
    }

    /**
     * @Author: zhuyiming
     * @Description: 拼接被保人累計
     * @param insuredList 被保人集合
     * @param riskAmntInfos 风险保额存储集合
     * @Date: Created in 15:26 2017/10/10
     */
    public void getInsuredInfo(TradeInfo tradeInfo, List<LCInsuredPojo> insuredList, List<RiskAmntInfoPojo> riskAmntInfos) {
        String riskCode = "000000";
//        StringBuffer sql = new StringBuffer();
//        sql.append("select ");
//        VData tVData = new VData();
//        TransferData tParam = new TransferData();
       Reflections reflections = new Reflections();
        for (int i = 0; i < insuredList.size(); i++) {
            LCInsuredPojo insured = insuredList.get(i);
            StringBuffer sql = new StringBuffer();
            VData tVData = new VData();
            TransferData tParam = new TransferData();
            sql.append("select ");
            // 拼接PRODRISKAMNT函数（累计类）
            for (int j = 0; j < RISKTYPE.length; j++) {
                sql.append("PRODRISKAMNT(?,?,?),");
                tParam.setNameAndValue("indClientNoAmnt" + i + "_" + j,
                        "string:" + insured.getInsuredNo());
                tParam.setNameAndValue("indRiskCodeAmnt" + i + "_" + j,
                        "string:" + riskCode);
                tParam.setNameAndValue("indRisktypeAmnt" + i + "_" + j,
                        "string:" + RISKTYPE[j]);
            }
            // 拼接PRODRISKAMNTCONT函数（保单累计）
            /*for (int j = 0; j < CONTRISKTYPE.length; j++) {
                sql.append("PRODRISKAMNTCONT(?,?,?,?),");
                tParam.setNameAndValue("indClientNoCont" + i + "_" + j,
                        "string:" + insured.getInsuredNo());
                tParam.setNameAndValue("indContno" + i + "_" + j, "string:"
                        + lcContPojo.getContNo());
                tParam.setNameAndValue("indRiskCodeCont" + i + "_" + j,
                        "string:" + riskCode);
                tParam.setNameAndValue("indRisktypeCont" + i + "_" + j,
                        "string:" + CONTRISKTYPE[j]);
            }*/
            // 拼接HEALTHAMNT_JH函数（健康累计）
            for (int j = 0; j < HEALTHTYPE.length; j++) {
                sql.append("HEALTHAMNT_JH(?,?,?),");
                tParam.setNameAndValue("indClientNoHeal" + i + "_" + j,
                        "string:" + insured.getInsuredNo());
                tParam.setNameAndValue("indRiskCodeHeal" + i + "_" + j,
                        "string:" + riskCode);
                tParam.setNameAndValue("indRisktypeHeal" + i + "_" + j,
                        "string:" + HEALTHTYPE[j]);
            }
            // 拼接RISKDEATHAMNT_Y函数（身故累计）
            sql.append("RISKDEATHAMNT_Y(?,?)");
            tParam.setNameAndValue("indClientNoDeath" + i, "string:"
                    + insured.getInsuredNo());
            tParam
                    .setNameAndValue("indRiskCodeDeath" + i, "string:"
                            + riskCode);
//            sql.append("GRPRISKDEATHAMNT_Y(?,?,?)");
//            tParam.setNameAndValue("grClientNoDeath" + i, "string:"
//                    + insured.getInsuredNo());
//            tParam
//                    .setNameAndValue("grRiskCodeDeath" + i, "string:"
//                            + riskCode);
//            tParam
//                    .setNameAndValue("grContNoDeath" + i, "string:"
//                            + insured.getContNo());
            sql.append(" from dual where 1=1");
            logger.debug("RiskAmntInsuredSql:" + sql);
            tVData.clear();
            tVData.add(sql.toString());
            tVData.add(tParam);
            SSRS tSSRS = new SSRS();
            Connection tConnection = null;
            try {
                tConnection = DBConnPool.getConnection("basedataSource");
                ExeSQL exeSql = new ExeSQL(tConnection);
                tSSRS = exeSql.execSQL(tVData);
            }catch (Exception e){
                logger.error("保单号：" + insured.getContNo() + "," + ExceptionUtils.exceptionToString(e));
                logger.error("累计风险保额被保人累计异常！" + tradeInfo);
                tradeInfo.addError("累计风险保额被保人累计异常！");
                return;
            }finally {
                if (tConnection != null) {
                    try {
                        tConnection.close();
                    } catch (SQLException e) {
                        tradeInfo.addError(ExceptionUtils.exceptionToString(e));
                        logger.error(ExceptionUtils.exceptionToString(e));
                        return;
                    }
                }
            }

            if(tSSRS == null){
                logger.debug("保单号：" + insured.getContNo() + "，累计风险保额被保人累计SQL发生异常！");
                tradeInfo.addError("保单号：" + insured.getContNo() + "，累计风险保额被保人累计SQL发生异常！");
                return;
            }

            if (null != tSSRS && tSSRS.MaxRow > 0) {
                // 计算总共查询的函数个数（被保人累计+投保人累计）
                RiskAmntInfoPojo riskAmntInfo = new RiskAmntInfoPojo();
                /** L：累计寿险风险保额 ，H：累计重疾风险保额 ，Y：累计意外险风险保额 CL:寿险保单保额 ，CH:累计重疾保单保额 ，CY:意外险保单保额
                 * PL：寿险体检风险保额 ，LI：人身险保额 ，SR：累计风险保额 M：住院医疗风险保额 ，N：意外医疗风险保额 ，J：津贴险风险保额
                 * ZJ:累计自驾车风险保额 ，HC:防癌险风险保额 ，ZB:累计再保风险保额*/
                riskAmntInfo.setRiskTypeL(tSSRS.GetText(1, 1));
                riskAmntInfo.setRiskTypeH(tSSRS.GetText(1, 2));
                riskAmntInfo.setRiskTypeY(tSSRS.GetText(1, 3));
                riskAmntInfo.setRiskTypeCL(tSSRS.GetText(1, 4));
                riskAmntInfo.setRiskTypeCH(tSSRS.GetText(1, 5));
                riskAmntInfo.setRiskTypeCY(tSSRS.GetText(1, 6));
                riskAmntInfo.setRiskTypePL(tSSRS.GetText(1, 7));
                riskAmntInfo.setRiskTypeLI(tSSRS.GetText(1, 8));
                riskAmntInfo.setRiskTypeSR(tSSRS.GetText(1, 9));
                riskAmntInfo.setRiskTypeM(tSSRS.GetText(1, 10));
                riskAmntInfo.setRiskTypeN(tSSRS.GetText(1, 11));
                riskAmntInfo.setRiskTypeJ(tSSRS.GetText(1, 12));
                riskAmntInfo.setRiskTypeZJ(tSSRS.GetText(1, 13));
                riskAmntInfo.setRiskTypeHC(tSSRS.GetText(1, 14));
                riskAmntInfo.setRiskTypeZB(tSSRS.GetText(1, 15));
                /** BL：累计寿险保单保额 ， BH：累计健康险保单保额 ， BY：累计意外险保单保额 ， BJ：累计自驾车保单保额 ， BC：累计防癌险保单保额*/
                /*riskAmntInfo.setContRiskTypeBL(tSSRS.GetText(1, 16));
                riskAmntInfo.setContRiskTypeBH(tSSRS.GetText(1, 17));
                riskAmntInfo.setContRiskTypeBY(tSSRS.GetText(1, 18));
                riskAmntInfo.setContRiskTypeBJ(tSSRS.GetText(1, 19));
                riskAmntInfo.setContRiskTypeBC(tSSRS.GetText(1, 20));*/
                riskAmntInfo.setContRiskTypeBL("0");
                riskAmntInfo.setContRiskTypeBH("0");
                riskAmntInfo.setContRiskTypeBY("0");
                riskAmntInfo.setContRiskTypeBJ("0");
                riskAmntInfo.setContRiskTypeBC("0");
                /** 1：寿险风险保额 ， 2：重疾险风险保额 ， 4：意外险风险保额*/
                /*riskAmntInfo.setHeathType1(tSSRS.GetText(1, 21));
                riskAmntInfo.setHeathType2(tSSRS.GetText(1, 22));
                riskAmntInfo.setHeathType4(tSSRS.GetText(1, 23));*/
                riskAmntInfo.setHeathType1(tSSRS.GetText(1, 16));
                riskAmntInfo.setHeathType2(tSSRS.GetText(1, 17));
                riskAmntInfo.setHeathType4(tSSRS.GetText(1, 18));
                /** 身故累计*/
                //riskAmntInfo.setAppRiskCodeDeath(tSSRS.GetText(1, 19));
                ////新团险上线 新增团险的身故累计 被保人
               // String indAmnt = tSSRS.GetText(1, 19);
                riskAmntInfo.setAppRiskCodeDeath(tSSRS.GetText(1, 19));
//                String grpAmnt = tSSRS.GetText(1, 20);
//                if (!StringUtils.isEmpty(indAmnt)&&!StringUtils.isEmpty(grpAmnt)){
//                    double indAmntD = Double.valueOf(indAmnt);
//                    double grpAmntD = Double.valueOf(grpAmnt);
//                    riskAmntInfo.setAppRiskCodeDeath(String.valueOf(indAmntD+grpAmntD));
//                }else{
//                    if (!StringUtils.isEmpty(indAmnt)){
//                        riskAmntInfo.setAppRiskCodeDeath(indAmnt);
//                    }else{
//                        riskAmntInfo.setAppRiskCodeDeath(grpAmnt);
//                    }
//                }
                riskAmntInfos.add((RiskAmntInfoPojo) reflections.transFields(new RiskAmntInfoPojo(), riskAmntInfo));
            }
        }
    }

    /**
     * @Author: zhuyiming
     * @Description: 拼接投保人累計類
     * @param applicant 投保人信息
     * @param riskAmntInfos 风险保额存储容器
     * @Date: Created in 15:36 2017/10/10
     */
    public void getAppntInfo(TradeInfo tradeInfo, LCAppntPojo applicant, List<RiskAmntInfoPojo> riskAmntInfos) {
        Reflections reflections = new Reflections();
        String relatToInsu = applicant.getRelatToInsu();
        logger.debug("投被保人关系为："+relatToInsu);

        if("00".equals(relatToInsu)){
            //投保人被保人为同一人时，不提取投保人累计
            logger.debug("投保人被保人为同一人，不提取投保人累计");
            RiskAmntInfoPojo riskAmntInfo = new RiskAmntInfoPojo();
            riskAmntInfo.setRiskTypeL("0");
            riskAmntInfo.setRiskTypeH("0");
            riskAmntInfo.setRiskTypeY("0");
            riskAmntInfo.setRiskTypeCL("0");
            riskAmntInfo.setRiskTypeCH("0");
            riskAmntInfo.setRiskTypeCY("0");
            riskAmntInfo.setRiskTypePL("0");
            riskAmntInfo.setRiskTypeLI("0");
            riskAmntInfo.setRiskTypeSR("0");
            riskAmntInfo.setRiskTypeM("0");
            riskAmntInfo.setRiskTypeN("0");
            riskAmntInfo.setRiskTypeJ("0");
            riskAmntInfo.setRiskTypeZJ("0");
            riskAmntInfo.setRiskTypeHC("0");
            riskAmntInfo.setRiskTypeZB("0");
            riskAmntInfo.setContRiskTypeBL("0");
            riskAmntInfo.setContRiskTypeBH("0");
            riskAmntInfo.setContRiskTypeBY("0");
            riskAmntInfo.setContRiskTypeBJ("0");
            riskAmntInfo.setContRiskTypeBC("0");
            riskAmntInfo.setHeathType1("0");
            riskAmntInfo.setHeathType2("0");
            riskAmntInfo.setHeathType4("0");
            /** 身故累计*/
            riskAmntInfo.setAppRiskCodeDeath("0");
            riskAmntInfos.add((RiskAmntInfoPojo) reflections.transFields(new RiskAmntInfoPojo(), riskAmntInfo));

        }else {
            String riskCode = "000000";
            StringBuffer sql = new StringBuffer();
            sql.append("select ");
            VData tVData = new VData();
            TransferData tParam = new TransferData();
            // 拼接PRODRISKAMNT函数（累计类）
            StringBuffer sql1 = new StringBuffer();
            sql1.append("select ");
            for (int j = 0; j < RISKTYPE.length; j++) {
                sql1.append("PRODRISKAMNT(?,?,?),");
                tParam.setNameAndValue("appClientNoAmnt" + j, "string:"
                        + applicant.getAppntNo());
                tParam.setNameAndValue("appRiskCodeAmnt" + j, "string:" + riskCode);
                tParam.setNameAndValue("appRisktypeAmnt" + j, "string:"
                        + RISKTYPE[j]);
            }
            // 拼接PRODRISKAMNTCONT函数（保单累计）
        /*for (int j = 0; j < CONTRISKTYPE.length; j++) {
            sql1.append("PRODRISKAMNTCONT(?,?,?,?),");
            tParam.setNameAndValue("appClientNoCont" + j, "string:"
                    + applicant.getAppntNo());
            tParam.setNameAndValue("appContno" + j, "string:" + lcContPojo.getContNo());
            tParam.setNameAndValue("appRiskCodeCont" + j, "string:" + riskCode);
            tParam.setNameAndValue("appRisktypeCont" + j, "string:"
                    + CONTRISKTYPE[j]);
        }*/
            // 拼接HEALTHAMNT_JH函数（健康累计）
            for (int j = 0; j < HEALTHTYPE.length; j++) {
                sql1.append("HEALTHAMNT_JH(?,?,?),");
                tParam.setNameAndValue("appClientNoHeal" + j, "string:"
                        + applicant.getAppntNo());
                tParam.setNameAndValue("appRiskCodeHeal" + j, "string:" + riskCode);
                tParam.setNameAndValue("appRisktypeHeal" + j, "string:"
                        + HEALTHTYPE[j]);
            }
            // 拼接RISKDEATHAMNT_Y函数（身故累计）
            sql1.append("RISKDEATHAMNT_Y(?,?),");
            tParam.setNameAndValue("appClientNoDeath", "string:"
                    + applicant.getAppntNo());
            tParam.setNameAndValue("appRiskCodeDeath", "string:" + riskCode);
            sql1.append("GRPRISKDEATHAMNT_Y(?,?,?)");
            tParam.setNameAndValue("grClientNoDeath", "string:"
                    + applicant.getAppntNo());
            tParam
                    .setNameAndValue("grRiskCodeDeath", "string:"
                            + riskCode);
            tParam
                    .setNameAndValue("grContNoDeath", "string:"
                            + applicant.getContNo());
            sql1.append(" from dual where 1=1");
            logger.debug("RiskAmntAppntSql:" + sql1);
            tVData.clear();
            tVData.add(sql1.toString());
            tVData.add(tParam);
            SSRS tSSRS = new SSRS();
            Connection tConnection = null;
            try {
                tConnection = DBConnPool.getConnection("basedataSource");
                ExeSQL exeSql = new ExeSQL(tConnection);
                tSSRS = exeSql.execSQL(tVData);
            } catch (Exception e) {
                logger.error("保单号：" + applicant.getContNo() + "," + ExceptionUtils.exceptionToString(e));
                logger.error("累计风险保额投保人累计异常！" + tradeInfo);
                tradeInfo.addError("累计风险保额投保人累计异常！");
                return;
            } finally {
                if (tConnection != null) {
                    try {
                        tConnection.close();
                    } catch (SQLException e) {
                        e.printStackTrace();
                        tradeInfo.addError(ExceptionUtils.exceptionToString(e));
                        logger.error(ExceptionUtils.exceptionToString(e));
                        return;
                    }
                }
            }
            if (tSSRS == null) {
                logger.debug("保单号：" + applicant.getContNo() + "，累计风险保额投保人累计SQL发生异常！");
                tradeInfo.addError("保单号：" + applicant.getContNo() + "，累计风险保额投保人累计SQL发生异常！");
                return;
            }

            if (null != tSSRS && tSSRS.MaxRow > 0) {
                // 计算总共查询的函数个数（被保人累计+投保人累计）
                RiskAmntInfoPojo riskAmntInfo = new RiskAmntInfoPojo();
                /**L：累计寿险风险保额 ，H：累计重疾风险保额 ，Y：累计意外险风险保额 CL:寿险保单保额 ，CH:累计重疾保单保额 ，CY:意外险保单保额
                 * PL：寿险体检风险保额 ，LI：人身险保额 ，SR：累计风险保额 M：住院医疗风险保额 ，N：意外医疗风险保额 ，J：津贴险风险保额
                 * ZJ:累计自驾车风险保额 ，HC:防癌险风险保额 ，ZB:累计再保风险保额*/
                riskAmntInfo.setRiskTypeL(tSSRS.GetText(1, 1));
                riskAmntInfo.setRiskTypeH(tSSRS.GetText(1, 2));
                riskAmntInfo.setRiskTypeY(tSSRS.GetText(1, 3));
                riskAmntInfo.setRiskTypeCL(tSSRS.GetText(1, 4));
                riskAmntInfo.setRiskTypeCH(tSSRS.GetText(1, 5));
                riskAmntInfo.setRiskTypeCY(tSSRS.GetText(1, 6));
                riskAmntInfo.setRiskTypePL(tSSRS.GetText(1, 7));
                riskAmntInfo.setRiskTypeLI(tSSRS.GetText(1, 8));
                riskAmntInfo.setRiskTypeSR(tSSRS.GetText(1, 9));
                riskAmntInfo.setRiskTypeM(tSSRS.GetText(1, 10));
                riskAmntInfo.setRiskTypeN(tSSRS.GetText(1, 11));
                riskAmntInfo.setRiskTypeJ(tSSRS.GetText(1, 12));
                riskAmntInfo.setRiskTypeZJ(tSSRS.GetText(1, 13));
                riskAmntInfo.setRiskTypeHC(tSSRS.GetText(1, 14));
                riskAmntInfo.setRiskTypeZB(tSSRS.GetText(1, 15));
                /** BL：累计寿险保单保额 ， BH：累计健康险保单保额 ， BY：累计意外险保单保额 ， BJ：累计自驾车保单保额 ， BC：累计防癌险保单保额*/
            /*riskAmntInfo.setContRiskTypeBL(tSSRS.GetText(1, 16));
            riskAmntInfo.setContRiskTypeBH(tSSRS.GetText(1, 17));
            riskAmntInfo.setContRiskTypeBY(tSSRS.GetText(1, 18));
            riskAmntInfo.setContRiskTypeBJ(tSSRS.GetText(1, 19));
            riskAmntInfo.setContRiskTypeBC(tSSRS.GetText(1, 20));*/
                riskAmntInfo.setContRiskTypeBL("0");
                riskAmntInfo.setContRiskTypeBH("0");
                riskAmntInfo.setContRiskTypeBY("0");
                riskAmntInfo.setContRiskTypeBJ("0");
                riskAmntInfo.setContRiskTypeBC("0");
                /** 1：寿险风险保额 ， 2：重疾险风险保额 ， 4：意外险风险保额*/
                riskAmntInfo.setHeathType1(tSSRS.GetText(1, 16));
                riskAmntInfo.setHeathType2(tSSRS.GetText(1, 17));
                riskAmntInfo.setHeathType4(tSSRS.GetText(1, 18));
                /** 身故累计*/
                //riskAmntInfo.setAppRiskCodeDeath(tSSRS.GetText(1, 19));
                //新团险上线 新增团险的身故累计 投保人
                String indAmnt = tSSRS.GetText(1, 19);
                String grpAmnt = tSSRS.GetText(1, 20);
                if (!StringUtils.isEmpty(indAmnt)&&!StringUtils.isEmpty(grpAmnt)){
                    double indAmntD = Double.valueOf(indAmnt);
                    double grpAmntD = Double.valueOf(grpAmnt);
                    riskAmntInfo.setAppRiskCodeDeath(String.valueOf(indAmntD+grpAmntD));
                }else{
                    if (!StringUtils.isEmpty(indAmnt)){
                        riskAmntInfo.setAppRiskCodeDeath(indAmnt);
                    }else{
                        riskAmntInfo.setAppRiskCodeDeath(grpAmnt);
                    }
                }

                riskAmntInfos.add((RiskAmntInfoPojo) reflections.transFields(new RiskAmntInfoPojo(), riskAmntInfo));
            }
        }
    }

    /**
     * 该保单下提数，被保人风险保额累计。
     * @param riskAmntInfos 风险保额存储容器
     * @param tradeInfo 业务数据
     * @return
     */
    public void getContExtract(TradeInfo tradeInfo, List<RiskAmntInfoPojo> riskAmntInfos) {
        // 被保人列表
        List<LCInsuredPojo> insuredList = (List) tradeInfo.getData(LCInsuredPojo.class.getName());
        //保单信息
        List<LCPolPojo> lcPolPojoList = (List) tradeInfo.getData(LCPolPojo.class.getName());
        //责任信息
        List<LCDutyPojo> lcDutyPojoList = (List) tradeInfo.getData(LCDutyPojo.class.getName());
        LCContPojo lcContPojo=(LCContPojo)tradeInfo.getData(LCContPojo.class.getName());
        Map<String,List> typeMaps = new HashMap<>();
        int count = 0;
        double nonAge1 = 0.0;
        logger.debug("保单号：" + insuredList.get(0).getContNo() + "，开始提取本单风险保额！");
        for(int i=0;i<lcPolPojoList.size();i++){
            //获取险种编码
            String riskcode = lcPolPojoList.get(i).getRiskCode();
            if("1081".equals(riskcode) || "6081".equals(riskcode) || "6019".equals(riskcode)){
                for(int x=0;x<lcDutyPojoList.size();x++){
                    LCDutyPojo lcDutyPojo = lcDutyPojoList.get(x);
                    String insuredPolno = lcPolPojoList.get(i).getPolNo();
                    String dutyPolno = lcDutyPojo.getPolNo();
                    if(dutyPolno.equals(insuredPolno)){
                        nonAge1 = lcDutyPojo.getAmnt();
                    }
                }
            }
            SSRS tSSRS = nbRedisCommon.getRiskAmntCalMode(riskcode);
            for(int j=1;j<=tSSRS.MaxRow;j++){
                if(tSSRS.GetText(j,1)!=null && !"".equals(tSSRS.GetText(j,1))){
                    //第一列为险种编码，第二列为计算编码,第三列为险类
                    List<String> results = new ArrayList<>();
                    results.add(tSSRS.GetText(j, 2).substring(0,1));
                    results.add(tSSRS.GetText(j,3));
                    //2017-12-21 修改7058风险保额以1.2倍计入重疾险风险保额
                    if("7058".equals(riskcode)){
                        results.add(String.valueOf(lcPolPojoList.get(i).getAmnt() * 1.2));
                    }else{
                        results.add(String.valueOf(lcPolPojoList.get(i).getAmnt()));
                    }
                    typeMaps.put(tSSRS.GetText(j,1)+"_" + count++, results);
                }
            }
        }
        String immunityflag="";
        String immunityRiskcode="";
        for(int i=0;i<insuredList.size();i++){
            RiskAmntInfoPojo riskAmntInfoPojo = riskAmntInfos.get(i+1);
            RiskAmntInfoPojo appRiskAmntInfoPojo = riskAmntInfos.get(0);
            String nonAge = riskAmntInfoPojo.getAppRiskCodeDeath();
            String nonAgeApp = appRiskAmntInfoPojo.getAppRiskCodeDeath();
            for(String key : typeMaps.keySet()) {
              String riskcode = key.substring(0,key.indexOf("_"));
                 logger.info(riskcode+"-------"+insuredList.get(i).getRelationToAppnt());
                LMRiskParamsDefPojo lmRiskParamsDefPojo = redisCommonDao.getEntityRelaDB(LMRiskParamsDefPojo.class, riskcode+ "|||" + "immunityflag");
                if(lmRiskParamsDefPojo != null){
                    immunityflag="1";
                    immunityRiskcode=lmRiskParamsDefPojo.getRiskCode();
                 }
                 //多被保人--不是豁免下的被保人不能累计豁免险的本单风险保额
                if ("1".equals(immunityflag)
                        && !"00".equals(insuredList.get(i).getRelationToAppnt()) ) {
                   break;
                }
                //多被保人--是豁免下的被保人不能累计主被保人下的险种的本单风险保额
                if ("1".equals(immunityflag) &&"00".equals(insuredList.get(i).getRelationToAppnt())
                        &&  !riskcode.equals(immunityRiskcode)) {
                    continue;
                }
                List<String> results = typeMaps.get(key);
                try {
                    //第一个参数写的是方法名,第二个\第三个\...写的是方法参数列表中参数的类型
                    Method setMethod = null;
                    Method getMethod = null;
                    if("E".equals(results.get(0))){
                        setMethod = RiskAmntInfoPojo.class.getMethod("setRiskType"+results.get(1), String.class);
                        getMethod = RiskAmntInfoPojo.class.getMethod("getRiskType"+results.get(1));
                    }else{
                        setMethod = RiskAmntInfoPojo.class.getMethod("setContRiskType"+results.get(1), String.class);
                        getMethod = RiskAmntInfoPojo.class.getMethod("getContRiskType"+results.get(1));
                    }
                    double newAmnt = Double.parseDouble(results.get(2));
                    double oldAmnt = Double.parseDouble((String) getMethod.invoke(riskAmntInfoPojo));
                    //invoke是执行该方法,并携带参数值
                    setMethod.invoke(riskAmntInfoPojo, new Object[]{String.valueOf(newAmnt + oldAmnt)});
                    if (lcPolPojoList.get(0).getAppntNo() != null
                            && lcPolPojoList.get(0).getAppntNo().equals(insuredList.get(i).getInsuredNo())) {
                        double AppOldAmnt = Double.parseDouble((String) getMethod.invoke(appRiskAmntInfoPojo));
                        setMethod.invoke(appRiskAmntInfoPojo, new Object[]{String.valueOf(newAmnt + AppOldAmnt)});
                        if("6081".equals(riskcode) || "1081".equals(riskcode)){
                            double ad1= Double.parseDouble(nonAge);
                            double newAmntNonAge=ad1+nonAge1;
                            appRiskAmntInfoPojo.setAppRiskCodeDeath(String.valueOf(newAmntNonAge));
                        }

                        if("2048".equals(riskcode) && "L".equals(results.get(1))){
                            if(lcPolPojoList.get(i).getInsuredAppAge()<18 ){
                                appRiskAmntInfoPojo.setRiskTypeL(String.valueOf(lcPolPojoList.get(i).getPrem()*0+AppOldAmnt));
                            }
                            if(lcPolPojoList.get(i).getInsuredAppAge()>=18 && lcPolPojoList.get(i).getInsuredAppAge()<=40){
                                appRiskAmntInfoPojo.setRiskTypeL(String.valueOf(lcPolPojoList.get(i).getPrem()*0.6+AppOldAmnt));
                            }
                            if(lcPolPojoList.get(i).getInsuredAppAge()>=41 && lcPolPojoList.get(i).getInsuredAppAge()<=60){
                                appRiskAmntInfoPojo.setRiskTypeL(String.valueOf(lcPolPojoList.get(i).getPrem()*0.4+AppOldAmnt));
                            }
                            if(lcPolPojoList.get(i).getInsuredAppAge()>=61){
                                appRiskAmntInfoPojo.setRiskTypeL(String.valueOf(lcPolPojoList.get(i).getPrem()*0.2+AppOldAmnt));
                            }
                        }
                        if("2048".equals(riskcode) && "LI".equals(results.get(1))){
                            if(lcPolPojoList.get(i).getInsuredAppAge()<18 ){
                                appRiskAmntInfoPojo.setRiskTypeLI(String.valueOf(lcPolPojoList.get(i).getPrem()*0+AppOldAmnt));
                            }
                            if(lcPolPojoList.get(i).getInsuredAppAge()>=18 && lcPolPojoList.get(i).getInsuredAppAge()<=40){
                                appRiskAmntInfoPojo.setRiskTypeLI(String.valueOf(lcPolPojoList.get(i).getPrem()*0.6 +AppOldAmnt));
                            }
                            if(lcPolPojoList.get(i).getInsuredAppAge()>=41 && lcPolPojoList.get(i).getInsuredAppAge()<=60){
                                appRiskAmntInfoPojo.setRiskTypeLI(String.valueOf(lcPolPojoList.get(i).getPrem()*0.4 +AppOldAmnt));
                            }
                            if(lcPolPojoList.get(i).getInsuredAppAge()>=61){
                                appRiskAmntInfoPojo.setRiskTypeLI(String.valueOf(lcPolPojoList.get(i).getPrem()*0.2 +AppOldAmnt));
                            }
                        }

                        if("1040".equals(riskcode) && "L".equals(results.get(1))){
                            if(lcPolPojoList.get(i).getInsuredAppAge()>=18 && lcPolPojoList.get(i).getPayIntv() == 0){
                                appRiskAmntInfoPojo.setRiskTypeL(String.valueOf(newAmnt*0.6 +AppOldAmnt));
                            }
                        }
                        if("1040".equals(riskcode) && "LI".equals(results.get(1))){
                            if(lcPolPojoList.get(i).getInsuredAppAge()>=18 && lcPolPojoList.get(i).getPayIntv() == 0){
                                appRiskAmntInfoPojo.setRiskTypeLI(String.valueOf(newAmnt*0.6 +AppOldAmnt));
                            }
                        }

                        /** 5020 累计寿险 ---wpq---*/
                        if("5020".equals(riskcode) && "L".equals(results.get(1))){
                            if(lcPolPojoList.get(i).getInsuredAppAge()>=18 && lcPolPojoList.get(i).getInsuredAppAge()<41){
                                appRiskAmntInfoPojo.setRiskTypeL(String.valueOf(lcPolPojoList.get(i).getPrem()*0.6 + AppOldAmnt));
                            }
                            if(lcPolPojoList.get(i).getInsuredAppAge()>=41 && lcPolPojoList.get(i).getInsuredAppAge()<61){
                                appRiskAmntInfoPojo.setRiskTypeL(String.valueOf(lcPolPojoList.get(i).getPrem()*0.4 + AppOldAmnt));
                            }
                            if(lcPolPojoList.get(i).getInsuredAppAge()>=61 ){
                                appRiskAmntInfoPojo.setRiskTypeL(String.valueOf(lcPolPojoList.get(i).getPrem()*0.2 + AppOldAmnt));

                            }
                        }
                        /** 5020 累计人身险 ---xzh---*/
                        if("5020".equals(riskcode) && "LI".equals(results.get(1))){
                            if(lcPolPojoList.get(i).getInsuredAppAge()>=18 && lcPolPojoList.get(i).getInsuredAppAge()<41){
                                appRiskAmntInfoPojo.setRiskTypeLI(String.valueOf(lcPolPojoList.get(i).getPrem()*0.6 + AppOldAmnt));
                            }
                            if(lcPolPojoList.get(i).getInsuredAppAge()>=41 && lcPolPojoList.get(i).getInsuredAppAge()<61){
                                appRiskAmntInfoPojo.setRiskTypeLI(String.valueOf(lcPolPojoList.get(i).getPrem()*0.4 + AppOldAmnt));
                            }
                            if(lcPolPojoList.get(i).getInsuredAppAge()>=61 ){
                                appRiskAmntInfoPojo.setRiskTypeLI(String.valueOf(lcPolPojoList.get(i).getPrem()*0.2 + AppOldAmnt));

                            }
                        }
                        //1018累计再保风险保额需要乘以100 重新计算
                        if ("1018".equals(riskcode)&&"ZB".equals(results.get(1))) {
                            appRiskAmntInfoPojo.setRiskTypeZB(String.valueOf(newAmnt * 100 + AppOldAmnt));
                        }
                        if ("1028".equals(riskcode)&&"Y".equals(results.get(1))) {
                            appRiskAmntInfoPojo.setRiskTypeY(String.valueOf(newAmnt * 2 + AppOldAmnt));
                        }
                        if ("1028".equals(riskcode)&&"ZJ".equals(results.get(1))) {
                            appRiskAmntInfoPojo.setRiskTypeZJ(String.valueOf(newAmnt * 20 + AppOldAmnt));
                        }
                        if ("1028".equals(riskcode)&&"LI".equals(results.get(1))) {
                            appRiskAmntInfoPojo.setRiskTypeLI(String.valueOf(newAmnt * 2 + AppOldAmnt));
                        }
                        if ("1028".equals(riskcode)&&"SR".equals(results.get(1))) {
                            appRiskAmntInfoPojo.setRiskTypeSR(String.valueOf(newAmnt * 2 + AppOldAmnt));
                        }
                        if ("1028".equals(riskcode)&&"BY".equals(results.get(1))) {
                            appRiskAmntInfoPojo.setContRiskTypeBY(String.valueOf(newAmnt * 2 + AppOldAmnt));
                        }
                        if ("1028".equals(riskcode)&&"BJ".equals(results.get(1))) {
                            appRiskAmntInfoPojo.setContRiskTypeBJ(String.valueOf(newAmnt * 20 + AppOldAmnt));
                        }
                        if ("7041".equals(riskcode)&&"HC".equals(results.get(1))) {
                            appRiskAmntInfoPojo.setRiskTypeHC(String.valueOf(newAmnt * 2 + AppOldAmnt));
                        }
                        if ("7041".equals(riskcode)&&"BC".equals(results.get(1))) {
                            appRiskAmntInfoPojo.setContRiskTypeBC(String.valueOf(newAmnt * 2 + AppOldAmnt));
                        }

                        if("1041".equals(riskcode)&&"L".equals(results.get(1))){
                            if(lcPolPojoList.get(i).getInsuredAppAge()<=17){
                                appRiskAmntInfoPojo.setRiskTypeL(String.valueOf(lcPolPojoList.get(i).getPrem()*0 + AppOldAmnt));
                            }
                            if(lcPolPojoList.get(i).getInsuredAppAge()>=18 && lcPolPojoList.get(i).getInsuredAppAge()<=40 && lcPolPojoList.get(i).getPayIntv() == 12){
                                appRiskAmntInfoPojo.setRiskTypeL(String.valueOf((lcPolPojoList.get(i).getPrem()*3)*0.6 + AppOldAmnt));
                            }
                            if(lcPolPojoList.get(i).getInsuredAppAge()>=18 && lcPolPojoList.get(i).getInsuredAppAge()<=40 && lcPolPojoList.get(i).getPayIntv() == 0){
                                appRiskAmntInfoPojo.setRiskTypeL(String.valueOf(lcPolPojoList.get(i).getPrem()*0.6 + AppOldAmnt));
                            }
                            if(lcPolPojoList.get(i).getInsuredAppAge()>=41 && lcPolPojoList.get(i).getInsuredAppAge()<=55 && lcPolPojoList.get(i).getPayIntv() == 12){
                                appRiskAmntInfoPojo.setRiskTypeL(String.valueOf((lcPolPojoList.get(i).getPrem()*3)*0.4 + AppOldAmnt));
                            }
                            if(lcPolPojoList.get(i).getInsuredAppAge()>=41 && lcPolPojoList.get(i).getInsuredAppAge()<=55 && lcPolPojoList.get(i).getPayIntv() == 0){
                                appRiskAmntInfoPojo.setRiskTypeL(String.valueOf(lcPolPojoList.get(i).getPrem()*0.4 + AppOldAmnt));
                            }
                        }

                        if("1041".equals(riskcode)&&"LI".equals(results.get(1))){
                            if(lcPolPojoList.get(i).getInsuredAppAge()<=17){
                                appRiskAmntInfoPojo.setRiskTypeLI(String.valueOf(lcPolPojoList.get(i).getPrem()*0 + AppOldAmnt));
                            }
                            if(lcPolPojoList.get(i).getInsuredAppAge()>=18 && lcPolPojoList.get(i).getInsuredAppAge()<=40 && lcPolPojoList.get(i).getPayIntv() == 12){
                                appRiskAmntInfoPojo.setRiskTypeLI(String.valueOf((lcPolPojoList.get(i).getPrem()*3)*0.6 + AppOldAmnt));
                            }
                            if(lcPolPojoList.get(i).getInsuredAppAge()>=18 && lcPolPojoList.get(i).getInsuredAppAge()<=40 && lcPolPojoList.get(i).getPayIntv() == 0){
                                appRiskAmntInfoPojo.setRiskTypeLI(String.valueOf(lcPolPojoList.get(i).getPrem()*0.6 + AppOldAmnt));
                            }
                            if(lcPolPojoList.get(i).getInsuredAppAge()>=41 && lcPolPojoList.get(i).getInsuredAppAge()<=55 && lcPolPojoList.get(i).getPayIntv() == 12){
                                appRiskAmntInfoPojo.setRiskTypeLI(String.valueOf((lcPolPojoList.get(i).getPrem()*3)*0.4 + AppOldAmnt));
                            }
                            if(lcPolPojoList.get(i).getInsuredAppAge()>=41 && lcPolPojoList.get(i).getInsuredAppAge()<=55 && lcPolPojoList.get(i).getPayIntv() == 0){
                                appRiskAmntInfoPojo.setRiskTypeLI(String.valueOf(lcPolPojoList.get(i).getPrem()*0.4 + AppOldAmnt));
                            }
                        }

                        if("7065".equals(riskcode)&&"LI".equals(results.get(1))){
                            if(lcPolPojoList.get(i).getInsuredAppAge()<=17){
                                appRiskAmntInfoPojo.setRiskTypeLI(String.valueOf(lcPolPojoList.get(i).getPrem()*0 + AppOldAmnt));
                            }
                        }
                        if("7065".equals(riskcode)&&"L".equals(results.get(1))){
                            if(lcPolPojoList.get(i).getInsuredAppAge()<=17){
                                appRiskAmntInfoPojo.setRiskTypeL(String.valueOf(lcPolPojoList.get(i).getPrem()*0 + AppOldAmnt));
                            }
                        }

                        if("7065".equals(riskcode)&&"H".equals(results.get(1))){
                            if(lcPolPojoList.get(i).getInsuredAppAge()<=17){
                                appRiskAmntInfoPojo.setRiskTypeH(String.valueOf(newAmnt * 2 + AppOldAmnt));
                            }
                            if(lcPolPojoList.get(i).getInsuredAppAge()>=18 && lcPolPojoList.get(i).getInsuredAppAge()<=55){
                                appRiskAmntInfoPojo.setRiskTypeH(String.valueOf(newAmnt * 2 + AppOldAmnt));
                            }
                        }

                        if("2052".equals(riskcode) && "L".equals(results.get(1))){
                            if(lcPolPojoList.get(i).getInsuredAppAge()<18 ){
                                appRiskAmntInfoPojo.setRiskTypeL(String.valueOf(lcPolPojoList.get(i).getPrem()*0 +oldAmnt));
                            }
                            if(lcPolPojoList.get(i).getInsuredAppAge()>=18 && lcPolPojoList.get(i).getInsuredAppAge()<41){
                                appRiskAmntInfoPojo.setRiskTypeL(String.valueOf((lcPolPojoList.get(i).getPrem()*2)*0.6 +oldAmnt));
                            }
                            if(lcPolPojoList.get(i).getInsuredAppAge()>=41 && lcPolPojoList.get(i).getInsuredAppAge()<61){
                                appRiskAmntInfoPojo.setRiskTypeL(String.valueOf((lcPolPojoList.get(i).getPrem()*2)*0.4 + oldAmnt));
                            }
                            if(lcPolPojoList.get(i).getInsuredAppAge()>=61 ){
                                appRiskAmntInfoPojo.setRiskTypeL(String.valueOf((lcPolPojoList.get(i).getPrem()*2)*0.2 + oldAmnt));
                            }
                        }

                        if("2052".equals(riskcode) && "LI".equals(results.get(1))){
                            if(lcPolPojoList.get(i).getInsuredAppAge()<18){
                                appRiskAmntInfoPojo.setRiskTypeLI(String.valueOf(lcPolPojoList.get(i).getPrem()*0 +oldAmnt));
                            }
                            if(lcPolPojoList.get(i).getInsuredAppAge()>=18 && lcPolPojoList.get(i).getInsuredAppAge()<41){
                                appRiskAmntInfoPojo.setRiskTypeLI(String.valueOf((lcPolPojoList.get(i).getPrem()*2)*0.6 +oldAmnt));
                            }
                            if(lcPolPojoList.get(i).getInsuredAppAge()>=41 && lcPolPojoList.get(i).getInsuredAppAge()<61){
                                appRiskAmntInfoPojo.setRiskTypeLI(String.valueOf((lcPolPojoList.get(i).getPrem()*2)*0.4 + oldAmnt));
                            }
                            if(lcPolPojoList.get(i).getInsuredAppAge()>=61 ){
                                appRiskAmntInfoPojo.setRiskTypeLI(String.valueOf((lcPolPojoList.get(i).getPrem()*2)*0.2 + oldAmnt));
                            }
                        }

                        if("2053".equals(riskcode) && "L".equals(results.get(1))){
                            if(lcPolPojoList.get(i).getInsuredAppAge()<18 ){
                                appRiskAmntInfoPojo.setRiskTypeL(String.valueOf(lcPolPojoList.get(i).getPrem()*0 +oldAmnt));
                            }
                            if(lcPolPojoList.get(i).getInsuredAppAge()>=18 && lcPolPojoList.get(i).getInsuredAppAge()<41){
                                appRiskAmntInfoPojo.setRiskTypeL(String.valueOf((lcPolPojoList.get(i).getPrem()*2)*0.6 +oldAmnt));
                            }
                            if(lcPolPojoList.get(i).getInsuredAppAge()>=41 && lcPolPojoList.get(i).getInsuredAppAge()<61){
                                appRiskAmntInfoPojo.setRiskTypeL(String.valueOf((lcPolPojoList.get(i).getPrem()*2)*0.4 + oldAmnt));
                            }
                            if(lcPolPojoList.get(i).getInsuredAppAge()>=61 ){
                                appRiskAmntInfoPojo.setRiskTypeL(String.valueOf((lcPolPojoList.get(i).getPrem()*2)*0.2 + oldAmnt));
                            }
                        }

                        if("2053".equals(riskcode) && "LI".equals(results.get(1))){
                            if(lcPolPojoList.get(i).getInsuredAppAge()<18){
                                appRiskAmntInfoPojo.setRiskTypeLI(String.valueOf(lcPolPojoList.get(i).getPrem()*0 +oldAmnt));
                            }
                            if(lcPolPojoList.get(i).getInsuredAppAge()>=18 && lcPolPojoList.get(i).getInsuredAppAge()<41){
                                appRiskAmntInfoPojo.setRiskTypeLI(String.valueOf((lcPolPojoList.get(i).getPrem()*2)*0.6 +oldAmnt));
                            }
                            if(lcPolPojoList.get(i).getInsuredAppAge()>=41 && lcPolPojoList.get(i).getInsuredAppAge()<61){
                                appRiskAmntInfoPojo.setRiskTypeLI(String.valueOf((lcPolPojoList.get(i).getPrem()*2)*0.4 + oldAmnt));
                            }
                            if(lcPolPojoList.get(i).getInsuredAppAge()>=61 ){
                                appRiskAmntInfoPojo.setRiskTypeLI(String.valueOf((lcPolPojoList.get(i).getPrem()*2)*0.2 + oldAmnt));
                            }
                        }


                        if("1043".equals(riskcode)&&"L".equals(results.get(1))){
                            if(lcPolPojoList.get(i).getInsuredAppAge()<=17){
                                appRiskAmntInfoPojo.setRiskTypeL(String.valueOf(lcPolPojoList.get(i).getPrem()*0 + AppOldAmnt));
                            }
                            if(lcPolPojoList.get(i).getInsuredAppAge()>=18 && lcPolPojoList.get(i).getInsuredAppAge()<=60 ){
                                if(lcPolPojoList.get(i).getPayYears()==3 || lcPolPojoList.get(i).getPayYears()==5){
                                    appRiskAmntInfoPojo.setRiskTypeL(String.valueOf(lcPolPojoList.get(i).getPrem()*lcPolPojoList.get(i).getPayYears()*0.6 + AppOldAmnt));
                                }
                                if(lcPolPojoList.get(i).getPayYears()==10 || lcPolPojoList.get(i).getPayYears()==20){
                                    appRiskAmntInfoPojo.setRiskTypeL(String.valueOf(lcPolPojoList.get(i).getPrem()*lcPolPojoList.get(i).getPayYears()*0.3 + AppOldAmnt));
                                }

                            }
                            if(lcPolPojoList.get(i).getInsuredAppAge()>=61){
                                if(lcPolPojoList.get(i).getPayYears()==3 || lcPolPojoList.get(i).getPayYears()==5){
                                    appRiskAmntInfoPojo.setRiskTypeL(String.valueOf(lcPolPojoList.get(i).getPrem()*lcPolPojoList.get(i).getPayYears()*0.2 + AppOldAmnt));
                                }
                                if(lcPolPojoList.get(i).getPayYears()==10 || lcPolPojoList.get(i).getPayYears()==20){
                                    appRiskAmntInfoPojo.setRiskTypeL(String.valueOf(lcPolPojoList.get(i).getPrem()*lcPolPojoList.get(i).getPayYears()*0.1 + AppOldAmnt));
                                }

                            }

                        }
                        if("1043".equals(riskcode)&&"LI".equals(results.get(1))){
                            if(lcPolPojoList.get(i).getInsuredAppAge()<=17){
                                appRiskAmntInfoPojo.setRiskTypeLI(String.valueOf(lcPolPojoList.get(i).getPrem()*0 + AppOldAmnt));
                            }
                            if(lcPolPojoList.get(i).getInsuredAppAge()>=18 && lcPolPojoList.get(i).getInsuredAppAge()<=60 ){
                                if(lcPolPojoList.get(i).getPayYears()==3 || lcPolPojoList.get(i).getPayYears()==5){
                                    appRiskAmntInfoPojo.setRiskTypeLI(String.valueOf(lcPolPojoList.get(i).getPrem()*lcPolPojoList.get(i).getPayYears()*0.6 + AppOldAmnt));
                                }
                                if(lcPolPojoList.get(i).getPayYears()==10 || lcPolPojoList.get(i).getPayYears()==20){
                                    appRiskAmntInfoPojo.setRiskTypeLI(String.valueOf(lcPolPojoList.get(i).getPrem()*lcPolPojoList.get(i).getPayYears()*0.3 + AppOldAmnt));
                                }

                            }
                            if(lcPolPojoList.get(i).getInsuredAppAge()>=61){
                                if(lcPolPojoList.get(i).getPayYears()==3 || lcPolPojoList.get(i).getPayYears()==5){
                                    appRiskAmntInfoPojo.setRiskTypeLI(String.valueOf(lcPolPojoList.get(i).getPrem()*lcPolPojoList.get(i).getPayYears()*0.2 + AppOldAmnt));
                                }
                                if(lcPolPojoList.get(i).getPayYears()==10 || lcPolPojoList.get(i).getPayYears()==20){
                                    appRiskAmntInfoPojo.setRiskTypeLI(String.valueOf(lcPolPojoList.get(i).getPrem()*lcPolPojoList.get(i).getPayYears()*0.1 + AppOldAmnt));
                                }

                            }

                        }

                        //1035
                        if("1035".equals(riskcode)&&"L".equals(results.get(1))){
                            if (lcPolPojoList.get(i).getPayYears()<10){
                                appRiskAmntInfoPojo.setRiskTypeL(String.valueOf(lcPolPojoList.get(i).getAmnt()*lcPolPojoList.get(i).getPayYears()+AppOldAmnt));
                            }
                            if (lcPolPojoList.get(i).getPayYears()>=10){
                                appRiskAmntInfoPojo.setRiskTypeL(String.valueOf(lcPolPojoList.get(i).getAmnt()*10+AppOldAmnt));
                            }

                        }
                        if("1035".equals(riskcode)&&"LI".equals(results.get(1))){
                            if (lcPolPojoList.get(i).getPayYears()<10){
                                appRiskAmntInfoPojo.setRiskTypeLI(String.valueOf(lcPolPojoList.get(i).getAmnt()*lcPolPojoList.get(i).getPayYears()+AppOldAmnt));
                            }
                            if (lcPolPojoList.get(i).getPayYears()>=10){
                                appRiskAmntInfoPojo.setRiskTypeLI(String.valueOf(lcPolPojoList.get(i).getAmnt()*10+AppOldAmnt));
                            }
                        }

                        //7063累计防癌，C070是1.5倍，C071是0
                        if ("C070".equals(lcContPojo.getProdSetCode()) && "7063".equals(riskcode) && "HC".equals(results.get(1))){
                            appRiskAmntInfoPojo.setRiskTypeHC(String.valueOf(lcPolPojoList.get(i).getAmnt()*1.5+AppOldAmnt));
                        }
                        //7063累计防癌，C070是1.5倍，C071是0
                        if ("C071".equals(lcContPojo.getProdSetCode()) && "7063".equals(riskcode) && "HC".equals(results.get(1))){
                            appRiskAmntInfoPojo.setRiskTypeHC(String.valueOf(lcPolPojoList.get(i).getAmnt()*0+AppOldAmnt));
                        }

                    }


                    if("7065".equals(riskcode)&&"LI".equals(results.get(1))){
                        if(lcPolPojoList.get(i).getInsuredAppAge()<=17){
                            riskAmntInfoPojo.setRiskTypeLI(String.valueOf(lcPolPojoList.get(i).getPrem()*0 + oldAmnt));
                        }
                    }
                    if("7065".equals(riskcode)&&"L".equals(results.get(1))){
                        if(lcPolPojoList.get(i).getInsuredAppAge()<=17){
                            riskAmntInfoPojo.setRiskTypeL(String.valueOf(lcPolPojoList.get(i).getPrem()*0 + oldAmnt));
                        }
                    }

                    if("7065".equals(riskcode)&&"H".equals(results.get(1))){
                        if(lcPolPojoList.get(i).getInsuredAppAge()<=17){
                            riskAmntInfoPojo.setRiskTypeH(String.valueOf(newAmnt * 2 + oldAmnt));
                        }
                        if(lcPolPojoList.get(i).getInsuredAppAge()>=18 && lcPolPojoList.get(i).getInsuredAppAge()<=55){
                            riskAmntInfoPojo.setRiskTypeH(String.valueOf(newAmnt * 2 + oldAmnt));
                        }
                    }


                    if("2048".equals(riskcode) && "L".equals(results.get(1))){
                        if(lcPolPojoList.get(i).getInsuredAppAge()<18){
                            riskAmntInfoPojo.setRiskTypeL(String.valueOf(lcPolPojoList.get(i).getPrem()*0+oldAmnt));
                        }
                        if(lcPolPojoList.get(i).getInsuredAppAge()>=18 && lcPolPojoList.get(i).getInsuredAppAge()<=40){
                            riskAmntInfoPojo.setRiskTypeL(String.valueOf(lcPolPojoList.get(i).getPrem()*0.6+oldAmnt));
                        }
                        if(lcPolPojoList.get(i).getInsuredAppAge()>=41 && lcPolPojoList.get(i).getInsuredAppAge()<=60){
                            riskAmntInfoPojo.setRiskTypeL(String.valueOf(lcPolPojoList.get(i).getPrem()*0.4+oldAmnt));
                        }
                        if(lcPolPojoList.get(i).getInsuredAppAge()>=61){
                            riskAmntInfoPojo.setRiskTypeL(String.valueOf(lcPolPojoList.get(i).getPrem()*0.2+oldAmnt));
                        }
                    }
                    if("2048".equals(riskcode) && "LI".equals(results.get(1))){
                        if(lcPolPojoList.get(i).getInsuredAppAge()<18){
                            riskAmntInfoPojo.setRiskTypeLI(String.valueOf(lcPolPojoList.get(i).getPrem()*0+oldAmnt));
                        }
                        if(lcPolPojoList.get(i).getInsuredAppAge()>=18 && lcPolPojoList.get(i).getInsuredAppAge()<=40){
                            riskAmntInfoPojo.setRiskTypeLI(String.valueOf(lcPolPojoList.get(i).getPrem()*0.6 +oldAmnt));
                        }
                        if(lcPolPojoList.get(i).getInsuredAppAge()>=41 && lcPolPojoList.get(i).getInsuredAppAge()<=60){
                            riskAmntInfoPojo.setRiskTypeLI(String.valueOf(lcPolPojoList.get(i).getPrem()*0.4 +oldAmnt));
                        }
                        if(lcPolPojoList.get(i).getInsuredAppAge()>=61){
                            riskAmntInfoPojo.setRiskTypeLI(String.valueOf(lcPolPojoList.get(i).getPrem()*0.2 +oldAmnt));
                        }
                    }

                    if("1040".equals(riskcode) && "L".equals(results.get(1))){
                        if(lcPolPojoList.get(i).getInsuredAppAge()>=18 && lcPolPojoList.get(i).getPayIntv() == 0){
                            riskAmntInfoPojo.setRiskTypeL(String.valueOf(newAmnt*0.6 +oldAmnt));
                        }
                        if(lcPolPojoList.get(i).getInsuredAppAge()>=18 && lcPolPojoList.get(i).getPayIntv() == 12){
                            riskAmntInfoPojo.setRiskTypeL(String.valueOf(newAmnt +oldAmnt));
                        }
                    }
                    if("1040".equals(riskcode) && "LI".equals(results.get(1))){
                        if(lcPolPojoList.get(i).getInsuredAppAge()>=18 && lcPolPojoList.get(i).getPayIntv() == 0){
                            riskAmntInfoPojo.setRiskTypeLI(String.valueOf(newAmnt*0.6 +oldAmnt));
                        }
                        if(lcPolPojoList.get(i).getInsuredAppAge()>=18 && lcPolPojoList.get(i).getPayIntv() == 12){
                            riskAmntInfoPojo.setRiskTypeLI(String.valueOf(newAmnt +oldAmnt));
                        }
                    }
                    if("5020".equals(riskcode) && "L".equals(results.get(1))){
                        if(lcPolPojoList.get(i).getInsuredAppAge()>=18 && lcPolPojoList.get(i).getInsuredAppAge()<41){
                            riskAmntInfoPojo.setRiskTypeL(String.valueOf(lcPolPojoList.get(i).getPrem()*0.6 +oldAmnt));
                        }
                        if(lcPolPojoList.get(i).getInsuredAppAge()>=41 && lcPolPojoList.get(i).getInsuredAppAge()<61){
                            riskAmntInfoPojo.setRiskTypeL(String.valueOf(lcPolPojoList.get(i).getPrem()*0.4 + oldAmnt));
                        }
                        if(lcPolPojoList.get(i).getInsuredAppAge()>=61 ){
                            riskAmntInfoPojo.setRiskTypeL(String.valueOf(lcPolPojoList.get(i).getPrem()*0.2 + oldAmnt));
                        }
                    }
                    /** 5020 累计人身险 ---xzh---*/
                    if("5020".equals(riskcode) && "LI".equals(results.get(1))){
                        if(lcPolPojoList.get(i).getInsuredAppAge()>=18 && lcPolPojoList.get(i).getInsuredAppAge()<41){
                            riskAmntInfoPojo.setRiskTypeLI(String.valueOf(lcPolPojoList.get(i).getPrem()*0.6 +oldAmnt));
                        }
                        if(lcPolPojoList.get(i).getInsuredAppAge()>=41 && lcPolPojoList.get(i).getInsuredAppAge()<61){
                            riskAmntInfoPojo.setRiskTypeLI(String.valueOf(lcPolPojoList.get(i).getPrem()*0.4 + oldAmnt));
                        }
                        if(lcPolPojoList.get(i).getInsuredAppAge()>=61 ){
                            riskAmntInfoPojo.setRiskTypeLI(String.valueOf(lcPolPojoList.get(i).getPrem()*0.2 + oldAmnt));
                        }
                    }

                    if("2052".equals(riskcode) && "L".equals(results.get(1))){
                        if(lcPolPojoList.get(i).getInsuredAppAge()<18 ){
                            riskAmntInfoPojo.setRiskTypeL(String.valueOf(lcPolPojoList.get(i).getPrem()*0 +oldAmnt));
                        }
                        if(lcPolPojoList.get(i).getInsuredAppAge()>=18 && lcPolPojoList.get(i).getInsuredAppAge()<41){
                            riskAmntInfoPojo.setRiskTypeL(String.valueOf((lcPolPojoList.get(i).getPrem()*2)*0.6 +oldAmnt));
                        }
                        if(lcPolPojoList.get(i).getInsuredAppAge()>=41 && lcPolPojoList.get(i).getInsuredAppAge()<61){
                            riskAmntInfoPojo.setRiskTypeL(String.valueOf((lcPolPojoList.get(i).getPrem()*2)*0.4 + oldAmnt));
                        }
                        if(lcPolPojoList.get(i).getInsuredAppAge()>=61 ){
                            riskAmntInfoPojo.setRiskTypeL(String.valueOf((lcPolPojoList.get(i).getPrem()*2)*0.2 + oldAmnt));
                        }
                    }

                    if("2052".equals(riskcode) && "LI".equals(results.get(1))){
                        if(lcPolPojoList.get(i).getInsuredAppAge()<18){
                            riskAmntInfoPojo.setRiskTypeLI(String.valueOf(lcPolPojoList.get(i).getPrem()*0 +oldAmnt));
                        }
                        if(lcPolPojoList.get(i).getInsuredAppAge()>=18 && lcPolPojoList.get(i).getInsuredAppAge()<41){
                            riskAmntInfoPojo.setRiskTypeLI(String.valueOf((lcPolPojoList.get(i).getPrem()*2)*0.6 +oldAmnt));
                        }
                        if(lcPolPojoList.get(i).getInsuredAppAge()>=41 && lcPolPojoList.get(i).getInsuredAppAge()<61){
                            riskAmntInfoPojo.setRiskTypeLI(String.valueOf((lcPolPojoList.get(i).getPrem()*2)*0.4 + oldAmnt));
                        }
                        if(lcPolPojoList.get(i).getInsuredAppAge()>=61 ){
                            riskAmntInfoPojo.setRiskTypeLI(String.valueOf((lcPolPojoList.get(i).getPrem()*2)*0.2 + oldAmnt));
                        }
                    }

                    if("2053".equals(riskcode) && "L".equals(results.get(1))){
                        if(lcPolPojoList.get(i).getInsuredAppAge()<18 ){
                            riskAmntInfoPojo.setRiskTypeL(String.valueOf(lcPolPojoList.get(i).getPrem()*0 +oldAmnt));
                        }
                        if(lcPolPojoList.get(i).getInsuredAppAge()>=18 && lcPolPojoList.get(i).getInsuredAppAge()<41){
                            riskAmntInfoPojo.setRiskTypeL(String.valueOf((lcPolPojoList.get(i).getPrem()*2)*0.6 +oldAmnt));
                        }
                        if(lcPolPojoList.get(i).getInsuredAppAge()>=41 && lcPolPojoList.get(i).getInsuredAppAge()<61){
                            riskAmntInfoPojo.setRiskTypeL(String.valueOf((lcPolPojoList.get(i).getPrem()*2)*0.4 + oldAmnt));
                        }
                        if(lcPolPojoList.get(i).getInsuredAppAge()>=61 ){
                            riskAmntInfoPojo.setRiskTypeL(String.valueOf((lcPolPojoList.get(i).getPrem()*2)*0.2 + oldAmnt));
                        }
                    }

                    if("2053".equals(riskcode) && "LI".equals(results.get(1))){
                        if(lcPolPojoList.get(i).getInsuredAppAge()<18){
                            riskAmntInfoPojo.setRiskTypeLI(String.valueOf(lcPolPojoList.get(i).getPrem()*0 +oldAmnt));
                        }
                        if(lcPolPojoList.get(i).getInsuredAppAge()>=18 && lcPolPojoList.get(i).getInsuredAppAge()<41){
                            riskAmntInfoPojo.setRiskTypeLI(String.valueOf((lcPolPojoList.get(i).getPrem()*2)*0.6 +oldAmnt));
                        }
                        if(lcPolPojoList.get(i).getInsuredAppAge()>=41 && lcPolPojoList.get(i).getInsuredAppAge()<61){
                            riskAmntInfoPojo.setRiskTypeLI(String.valueOf((lcPolPojoList.get(i).getPrem()*2)*0.4 + oldAmnt));
                        }
                        if(lcPolPojoList.get(i).getInsuredAppAge()>=61 ){
                            riskAmntInfoPojo.setRiskTypeLI(String.valueOf((lcPolPojoList.get(i).getPrem()*2)*0.2 + oldAmnt));
                        }
                    }
                    if("6081".equals(riskcode) || "1081".equals(riskcode) || "6019".equals(riskcode)){
                        double ad= Double.parseDouble(nonAge);
                        double newAmntNonAge=ad+nonAge1;
                        logger.info(ad+"----");
                        logger.info(nonAge1+"-------nonAge1");
                        riskAmntInfoPojo.setAppRiskCodeDeath(String.valueOf(newAmntNonAge));
                    }
                    //1018累计再保风险保额需要乘以100 重新计算
                    if ("1018".equals(riskcode)) {
                        riskAmntInfoPojo.setRiskTypeZB(String.valueOf(newAmnt * 100 + oldAmnt));
                    }
                    //1028
                    if ("1028".equals(riskcode)&&"Y".equals(results.get(1))) {
                        riskAmntInfoPojo.setRiskTypeY(String.valueOf(newAmnt * 2 + oldAmnt));
                    }
                    if ("1028".equals(riskcode)&&"ZJ".equals(results.get(1))) {
                        riskAmntInfoPojo.setRiskTypeZJ(String.valueOf(newAmnt * 20 + oldAmnt));
                    }
                    if ("1028".equals(riskcode)&&"LI".equals(results.get(1))) {
                        riskAmntInfoPojo.setRiskTypeLI(String.valueOf(newAmnt * 2 + oldAmnt));
                    }
                    if ("1028".equals(riskcode)&&"SR".equals(results.get(1))) {
                        riskAmntInfoPojo.setRiskTypeSR(String.valueOf(newAmnt * 2 + oldAmnt));
                    }
                    if ("1028".equals(riskcode)&&"BY".equals(results.get(1))) {
                        riskAmntInfoPojo.setContRiskTypeBY(String.valueOf(newAmnt * 2 + oldAmnt));
                    }
                    if ("1028".equals(riskcode)&&"BJ".equals(results.get(1))) {
                        riskAmntInfoPojo.setContRiskTypeBJ(String.valueOf(newAmnt * 20 + oldAmnt));
                    }
                    //1028意外保额,人身险保额按2倍计算，意外保单保额1倍，自驾车风险保额20倍
                    if ("1028".equals(riskcode)&&"CY".equals(results.get(1))) {
                        riskAmntInfoPojo.setRiskTypeCY(String.valueOf(newAmnt + oldAmnt));
                    }
                    if ("7041".equals(riskcode)&&"HC".equals(results.get(1))) {
                        riskAmntInfoPojo.setRiskTypeHC(String.valueOf(newAmnt * 2 + oldAmnt));
                    }
                    if ("7041".equals(riskcode)&&"BC".equals(results.get(1))) {
                        riskAmntInfoPojo.setContRiskTypeBC(String.valueOf(newAmnt * 2 + oldAmnt));
                    }
                    if (("7025".equals(riskcode) || "7026".equals(riskcode) ||"7027".equals(riskcode) ||"7028".equals(riskcode))&&"N".equals(results.get(1))) {
                        riskAmntInfoPojo.setRiskTypeN(String.valueOf(newAmnt  + oldAmnt));
                    }
                    //7057的按1.2倍基本保额累计重疾险风险保额
                    if("7057".equals(riskcode) && "H".equals(results.get(1))){
                        riskAmntInfoPojo.setRiskTypeH(String.valueOf(newAmnt*1.2+oldAmnt));
                    }

                    //1035
                    if("1035".equals(riskcode)&&"L".equals(results.get(1))){
                        if (lcPolPojoList.get(i).getPayYears()<10){
                            riskAmntInfoPojo.setRiskTypeL(String.valueOf(lcPolPojoList.get(i).getAmnt()*lcPolPojoList.get(i).getPayYears()+oldAmnt));
                        }
                        if (lcPolPojoList.get(i).getPayYears()>=10){
                            riskAmntInfoPojo.setRiskTypeL(String.valueOf(lcPolPojoList.get(i).getAmnt()*10+oldAmnt));
                        }

                    }
                    if("1035".equals(riskcode)&&"LI".equals(results.get(1))){
                        if (lcPolPojoList.get(i).getPayYears()<10){
                            riskAmntInfoPojo.setRiskTypeLI(String.valueOf(lcPolPojoList.get(i).getAmnt()*lcPolPojoList.get(i).getPayYears()+oldAmnt));
                        }
                        if (lcPolPojoList.get(i).getPayYears()>=10){
                            riskAmntInfoPojo.setRiskTypeLI(String.valueOf(lcPolPojoList.get(i).getAmnt()*10+oldAmnt));
                        }
                    }

                    if("1041".equals(riskcode)&&"L".equals(results.get(1))){
                        if(lcPolPojoList.get(i).getInsuredAppAge()<=17){
                            riskAmntInfoPojo.setRiskTypeL(String.valueOf(lcPolPojoList.get(i).getPrem()*0 + oldAmnt));
                        }
                        if(lcPolPojoList.get(i).getInsuredAppAge()>=18 && lcPolPojoList.get(i).getInsuredAppAge()<=40 && lcPolPojoList.get(i).getPayIntv() == 12){
                            riskAmntInfoPojo.setRiskTypeL(String.valueOf((lcPolPojoList.get(i).getPrem()*3)*0.6 + oldAmnt));
                        }
                        if(lcPolPojoList.get(i).getInsuredAppAge()>=18 && lcPolPojoList.get(i).getInsuredAppAge()<=40 && lcPolPojoList.get(i).getPayIntv() == 0){
                            riskAmntInfoPojo.setRiskTypeL(String.valueOf(lcPolPojoList.get(i).getPrem()*0.6 + oldAmnt));
                        }
                        if(lcPolPojoList.get(i).getInsuredAppAge()>=41 && lcPolPojoList.get(i).getInsuredAppAge()<=55 && lcPolPojoList.get(i).getPayIntv() == 12){
                            riskAmntInfoPojo.setRiskTypeL(String.valueOf((lcPolPojoList.get(i).getPrem()*3)*0.4 + oldAmnt));
                        }
                        if(lcPolPojoList.get(i).getInsuredAppAge()>=41 && lcPolPojoList.get(i).getInsuredAppAge()<=55 && lcPolPojoList.get(i).getPayIntv() == 0){
                            riskAmntInfoPojo.setRiskTypeL(String.valueOf(lcPolPojoList.get(i).getPrem()*0.4 + oldAmnt));
                        }
                    }

                    if("1041".equals(riskcode)&&"LI".equals(results.get(1))){
                        if(lcPolPojoList.get(i).getInsuredAppAge()<=17){
                            riskAmntInfoPojo.setRiskTypeLI(String.valueOf(lcPolPojoList.get(i).getPrem()*0 + oldAmnt));
                        }
                        if(lcPolPojoList.get(i).getInsuredAppAge()>=18 && lcPolPojoList.get(i).getInsuredAppAge()<=40 && lcPolPojoList.get(i).getPayIntv() == 12){
                            riskAmntInfoPojo.setRiskTypeLI(String.valueOf((lcPolPojoList.get(i).getPrem()*3)*0.6 + oldAmnt));
                        }
                        if(lcPolPojoList.get(i).getInsuredAppAge()>=18 && lcPolPojoList.get(i).getInsuredAppAge()<=40 && lcPolPojoList.get(i).getPayIntv() == 0){
                            riskAmntInfoPojo.setRiskTypeLI(String.valueOf(lcPolPojoList.get(i).getPrem()*0.6 + oldAmnt));
                        }
                        if(lcPolPojoList.get(i).getInsuredAppAge()>=41 && lcPolPojoList.get(i).getInsuredAppAge()<=55 && lcPolPojoList.get(i).getPayIntv() == 12){
                            riskAmntInfoPojo.setRiskTypeLI(String.valueOf((lcPolPojoList.get(i).getPrem()*3)*0.4 + oldAmnt));
                        }
                        if(lcPolPojoList.get(i).getInsuredAppAge()>=41 && lcPolPojoList.get(i).getInsuredAppAge()<=55 && lcPolPojoList.get(i).getPayIntv() == 0){
                            riskAmntInfoPojo.setRiskTypeLI(String.valueOf(lcPolPojoList.get(i).getPrem()*0.4 + oldAmnt));
                        }
                    }

                    if("7065".equals(riskcode)&&"LI".equals(results.get(1))){
                        if(lcPolPojoList.get(i).getInsuredAppAge()<=17){
                            riskAmntInfoPojo.setRiskTypeLI(String.valueOf(lcPolPojoList.get(i).getPrem()*0 + oldAmnt));
                        }
                    }
                    if("7065".equals(riskcode)&&"L".equals(results.get(1))){
                        if(lcPolPojoList.get(i).getInsuredAppAge()<=17){
                            riskAmntInfoPojo.setRiskTypeL(String.valueOf(lcPolPojoList.get(i).getPrem()*0 + oldAmnt));
                        }
                    }

                    if("7065".equals(riskcode)&&"H".equals(results.get(1))){
                        if(lcPolPojoList.get(i).getInsuredAppAge()<=17){
                            riskAmntInfoPojo.setRiskTypeH(String.valueOf(newAmnt * 2 + oldAmnt));
                        }
                        if(lcPolPojoList.get(i).getInsuredAppAge()>=18 && lcPolPojoList.get(i).getInsuredAppAge()<=55){
                            riskAmntInfoPojo.setRiskTypeH(String.valueOf(newAmnt * 2 + oldAmnt));
                        }
                    }

                    if("1043".equals(riskcode)&&"L".equals(results.get(1))){
                        if(lcPolPojoList.get(i).getInsuredAppAge()<=17){
                            riskAmntInfoPojo.setRiskTypeL(String.valueOf(lcPolPojoList.get(i).getPrem()*0 + oldAmnt));
                        }
                        if(lcPolPojoList.get(i).getInsuredAppAge()>=18 && lcPolPojoList.get(i).getInsuredAppAge()<=60 ){
                            if(lcPolPojoList.get(i).getPayYears()==3 || lcPolPojoList.get(i).getPayYears()==5){
                                riskAmntInfoPojo.setRiskTypeL(String.valueOf(lcPolPojoList.get(i).getPrem()*lcPolPojoList.get(i).getPayYears()*0.6 + oldAmnt));
                            }
                            if(lcPolPojoList.get(i).getPayYears()==10 || lcPolPojoList.get(i).getPayYears()==20){
                                riskAmntInfoPojo.setRiskTypeL(String.valueOf(lcPolPojoList.get(i).getPrem()*lcPolPojoList.get(i).getPayYears()*0.3 + oldAmnt));
                            }

                        }
                        if(lcPolPojoList.get(i).getInsuredAppAge()>=61){
                            if(lcPolPojoList.get(i).getPayYears()==3 || lcPolPojoList.get(i).getPayYears()==5){
                                riskAmntInfoPojo.setRiskTypeL(String.valueOf(lcPolPojoList.get(i).getPrem()*lcPolPojoList.get(i).getPayYears()*0.2 + oldAmnt));
                            }
                            if(lcPolPojoList.get(i).getPayYears()==10 || lcPolPojoList.get(i).getPayYears()==20){
                                riskAmntInfoPojo.setRiskTypeL(String.valueOf(lcPolPojoList.get(i).getPrem()*lcPolPojoList.get(i).getPayYears()*0.1 + oldAmnt));
                            }

                        }

                    }
                    if("1043".equals(riskcode)&&"LI".equals(results.get(1))){
                        if(lcPolPojoList.get(i).getInsuredAppAge()<=17){
                            riskAmntInfoPojo.setRiskTypeLI(String.valueOf(lcPolPojoList.get(i).getPrem()*0 + oldAmnt));
                        }
                        if(lcPolPojoList.get(i).getInsuredAppAge()>=18 && lcPolPojoList.get(i).getInsuredAppAge()<=60 ){
                            if(lcPolPojoList.get(i).getPayYears()==3 || lcPolPojoList.get(i).getPayYears()==5){
                                riskAmntInfoPojo.setRiskTypeLI(String.valueOf(lcPolPojoList.get(i).getPrem()*lcPolPojoList.get(i).getPayYears()*0.6 + oldAmnt));
                            }
                            if(lcPolPojoList.get(i).getPayYears()==10 || lcPolPojoList.get(i).getPayYears()==20){
                                riskAmntInfoPojo.setRiskTypeLI(String.valueOf(lcPolPojoList.get(i).getPrem()*lcPolPojoList.get(i).getPayYears()*0.3 + oldAmnt));
                            }

                        }
                        if(lcPolPojoList.get(i).getInsuredAppAge()>=61){
                            if(lcPolPojoList.get(i).getPayYears()==3 || lcPolPojoList.get(i).getPayYears()==5){
                                riskAmntInfoPojo.setRiskTypeLI(String.valueOf(lcPolPojoList.get(i).getPrem()*lcPolPojoList.get(i).getPayYears()*0.2 + oldAmnt));
                            }
                            if(lcPolPojoList.get(i).getPayYears()==10 || lcPolPojoList.get(i).getPayYears()==20){
                                riskAmntInfoPojo.setRiskTypeLI(String.valueOf(lcPolPojoList.get(i).getPrem()*lcPolPojoList.get(i).getPayYears()*0.1 + oldAmnt));
                            }

                        }

                    }

                    //7063累计防癌，C070是1.5倍，C071是0
                    if ("C070".equals(lcContPojo.getProdSetCode()) && "7063".equals(riskcode) && "HC".equals(results.get(1))){
                        riskAmntInfoPojo.setRiskTypeHC(String.valueOf(lcPolPojoList.get(i).getAmnt()*1.5+oldAmnt));
                    }
                    //7063累计防癌，C070是1.5倍，C071是0
                    if ("C071".equals(lcContPojo.getProdSetCode()) && "7063".equals(riskcode) && "HC".equals(results.get(1))){
                        riskAmntInfoPojo.setRiskTypeHC(String.valueOf(lcPolPojoList.get(i).getAmnt()*0+oldAmnt));
                    }
                    // add by wangshuliang 20180130  start 1：寿险风险保额 ， 2：重疾险风险保额 ， 4：意外险风险保额 未累计本单
                    // heathType1
                    if ("L".equals(results.get(1))) {
                        appRiskAmntInfoPojo.setHeathType1(appRiskAmntInfoPojo.getRiskTypeL());//投保人
                        riskAmntInfoPojo.setHeathType1(riskAmntInfoPojo.getRiskTypeL());//被保人
                    }
                    // heathType2
                    if ("H".equals(results.get(1))) {
                        appRiskAmntInfoPojo.setHeathType2(appRiskAmntInfoPojo.getRiskTypeH());
                        riskAmntInfoPojo.setHeathType2(riskAmntInfoPojo.getRiskTypeH());
                    }
                    // heathType4
                    if ("Y".equals(results.get(1))) {
                        appRiskAmntInfoPojo.setHeathType4(appRiskAmntInfoPojo.getRiskTypeY());
                        riskAmntInfoPojo.setHeathType4(riskAmntInfoPojo.getRiskTypeY());
                    }
                    //add by wangshuliang end 20180130
                } catch (IllegalAccessException e) {
                    logger.error("保单号：" + lcPolPojoList.get(0).getContNo() + "," + ExceptionUtils.exceptionToString(e));
                    tradeInfo.addError("查询风险保额累计算法异常！");
                    return;
                } catch (InvocationTargetException e) {
                    logger.error("保单号：" + lcPolPojoList.get(0).getContNo() + "," + ExceptionUtils.exceptionToString(e));
                    tradeInfo.addError("查询风险保额累计算法异常！");
                    return;
                } catch (NoSuchMethodException e){
                    logger.error("保单号：" + lcPolPojoList.get(0).getContNo() + "," + ExceptionUtils.exceptionToString(e));
                    tradeInfo.addError("未找到类RiskAmntInfoPojo"+e);
                    e.printStackTrace();
                    return;
                }
            }
        }
    }
}
