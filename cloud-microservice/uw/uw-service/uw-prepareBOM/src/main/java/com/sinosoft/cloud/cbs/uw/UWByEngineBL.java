package com.sinosoft.cloud.cbs.uw;



import com.sinosoft.cache.dao.impl.RedisCommonDao;
import com.sinosoft.cloud.bl.AbstractBL;
import com.sinosoft.cloud.cbs.rules.data.Engine;
import com.sinosoft.cloud.cbs.rules.result.Result;
import com.sinosoft.cloud.cbs.rules.result.UwResult;
import com.sinosoft.cloud.common.CalBase;
import com.sinosoft.cloud.common.Calculator;
import com.sinosoft.cloud.common.SpringContextUtils;
import com.sinosoft.cloud.rest.TradeInfo;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.entity.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import static java.lang.Float.parseFloat;

/**
 * @Author: 朱一鸣
 * @Description:
 * @Date: Created in 11:37 2017/9/22
 * @Modified By
 */
@Service
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class UWByEngineBL extends AbstractBL {
    private final Log logger = LogFactory.getLog(this.getClass());
    @Value("${cloud.engine.barrier.control}")
    private String barrier;

    @Value("${cloud.engine.barrier.check}")
    private String uwCheckFalg;

    @Autowired
    RedisCommonDao redisCommonDao;


    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    //private static GlobalCheckSpot mGlobalCheckSpot = GlobalCheckSpot.getInstance();
    /** 往界面传输数据的容器 */
    MMap mMap = new MMap();
    private VData mResult = new VData();

    private ExeSQL exeSQL =new ExeSQL();

    /** 数据操作字符串 */
    private String mOperator;
    private String mManageCom;
    private String mAllPolPassFlag = "9";
    /** 保单表 */
    private LCContSchema mLCContSchema = new LCContSchema();
    private String mPolPassFlag = "0"; // 险种通过标记 初始为未核保
    private String mContPassFlag = "0"; // 合同通过标记 初始为未核保
    private String mInsPassFlag = "0";
    private String mUWGrade = "";//规则引擎接口返回的核保级别
    private String mContNo = "";
    private String mPContNo = "";
    private String mOldPolNo = "";//获得的保单号码
    private String ProductSaleFlag = "0";//产品上市停售规则标记，如果不符合规则则置为1

    private LCContSet mAllLCContSet = new LCContSet();
    private LCPolSet mAllLCPolSet = new LCPolSet();
    //   private LCInsuredSet mAllInsuredSet = new LCInsuredSet();
    //   private LCAppntSchema mLCAppntSchema = new LCAppntSchema();
    private LCAppntPojo lcAppntPojo = new LCAppntPojo();
    private List<LCInsuredPojo> lcInsuredPojoList = new ArrayList<>();
    private List<LCAddressPojo> lcLCAddressPojoList =new ArrayList<>();
    private List<LCBnfPojo> lcBnfPojoList = new ArrayList<>();

    /** 合同核保主表 */
    private LCCUWMasterSet mLCCUWMasterSet = new LCCUWMasterSet();
    private LCCUWMasterSet mAllLCCUWMasterSet = new LCCUWMasterSet();

    /** 合同核保子表 */
    private LCCUWSubSet mLCCUWSubSet = new LCCUWSubSet();
    private LCCUWSubSet mAllLCCUWSubSet = new LCCUWSubSet();

    /** 合同核保错误信息表 */
    private LCCUWErrorSet mLCCUWErrorSet = new LCCUWErrorSet();
    private LCCUWErrorSet mAllLCCUWErrorSet = new LCCUWErrorSet();

    /** 各险种核保主表 */
    private LCUWMasterSet mLCUWMasterSet = new LCUWMasterSet();
    private LCUWMasterSet mAllLCUWMasterSet = new LCUWMasterSet();

    /** 各险种核保子表 */
    private LCUWSubSet mLCUWSubSet = new LCUWSubSet();
    private LCUWSubSet mAllLCUWSubSet = new LCUWSubSet();

    /** 各险种核保错误信息表 */
    private LCUWErrorSet mLCUWErrorSet = new LCUWErrorSet();
    private LCUWErrorSet mAllLCErrSet = new LCUWErrorSet();


    /** 被保人核保主表*/
    private LCIndUWMasterSet mLCIndUWMasterSet = new LCIndUWMasterSet();

    /** 被保人核保子表*/
    private LCIndUWSubSet mLCIndUWSubSet = new LCIndUWSubSet();

    /** 被保人核保错误信息表*/
    private LCIndUWErrorSet mLCIndUWErrorSet = new LCIndUWErrorSet();

    private CalBase mCalBase = new CalBase();
    private FDate fDate = new FDate();
    private String mCalCode = "";
    private String cHeckFlag ="";


    private String reDistribute = ""; // 分保标志 add by yaory

    //风险保额
    //1 寿险
    //2 重疾
    //4  意外
    //12 人身
    //41 交通意外
    //42 航空意外
    private double LSumDangerAmnt = 0; // 同一被保险人下寿险类的累计危险保额add by yaory
    private double DSumDangerAmnt = 0; // 同一被保险人下重大疾病类的累计危险保额add by yaory
    private double MSumDangerAmnt = 0; // 同一被保险人下人身意外医疗类的累计危险保额add by yaory
    private double SSumDangerAmnt = 0; // 同一被保险人下住院医疗类的累计危险保额add by yaory
    private double SSumDieAmnt = 0; // 同一被保险人下累计身故风险保额
    private double AllSumAmnt = 0;

    private double AppntLSumDangerAmnt = 0;//投保人累计寿险风险保额
    private double AppntDSumDangerAmnt = 0;//投保人累计重疾风险保额
    private double AppntMSumDangerAmnt = 0;//投保人累计意外风险保额
    private double AppntSSumDieAmnt = 0;//投保人累计身故风险保额
    private double AppntSSumDangerAmnt = 0; // 投保人住院医疗类的累计危险保额
    private double AppntAllSumAmnt = 0;





    //中台计算本单风险保额,险种层
    private double Fxml_1=0;
    private double Fxml_2=0;
    private double Fxml_4=0;
    private double Fxml_12=0;
    private double Fxml_41=0;
    private double Fxml_43=0;

    //中台计算本单风险保额,保单层
    private double SumContFxml_1=0;
    private double SumContFxml_2=0;
    private double SumContFxml_4=0;
    private double SumContFxml_12=0;
    private double SumContFxml_41=0;
    private double SumContFxml_43=0;




    private MMap map = new MMap();

    //add by likai 临分风险保额
    private double mLFSumDangerAmnt1 = 0;//被保人临分累计寿险风险保额
    private double mLFSumDangerAmnt2 = 0;//被保人临分累计重疾风险保额
    private double mLFSumDangerAmnt4 = 0;//被保人临分累计意外风险保额
    private double mLFSumDangerAmnt12 = 0;//被保人临分累计人身风险保额
    private double mLFSumDangerAmnt41 = 0;//被保人临分累计交通意外风险保额
    private double mLFSumDangerAmnt42 = 0;//被保人临分累计航空意外风险保额
    private double mLFSumDangerAmntBZ1 = 0;//被保人累计寿险风险保额(保障类)
    private double mLFSumDangerAmntBZ2 = 0;//被保人累计重疾风险保额(保障类)
    private double mLFSumDangerAmntBZ4 = 0;//被保人累计意外风险保额(保障类)
    private double mLFSumDangerAmntBZ12 = 0;//被保人累计人身风险保额(保障类)
    private double mLFSumDangerAmntBZ41 = 0;//被保人累计交通意外风险保额(保障类)
    private double mLFSumDangerAmntBZ42 = 0;//被保人累计航空意外风险保额(保障类)
    private double mLFSumDangerAmntCX1 = 0;//被保人临分累计寿险风险保额(储蓄类)
    private double mLFSumDangerAmntCX2 = 0;//被保人临分累计重疾风险保额(储蓄类)
    private double mLFSumDangerAmntCX4 = 0;//被保人临分累计意外风险保额(储蓄类)
    private double mLFSumDangerAmntCX12 = 0;//被保人临分累计人身风险保额(储蓄类)
    private double mLFSumDangerAmntCX41 = 0;//被保人临分累计交通意外风险保额(储蓄类)
    private double mLFSumDangerAmntCX42 = 0;//被保人临分累计航空意外风险保额(储蓄类)

    private String tUWStyle = "00";//添加规则分类
    private String ImpartVer;
    private String ImpartCode;
    private String Impartparammodle;
    private int errorSqlCount=0;
    private String flag="";
    private String dutyCodeString="";
    @Override
    public boolean checkData(TradeInfo requestInfo) {
        //校验是否存在主险
        List<LCPolPojo> tLCPolPojoList = (List) requestInfo.getData(LCPolPojo.class.getName());
        boolean checkMainRisk = false;
        for(int i=0;i<tLCPolPojoList.size();i++){
            if(tLCPolPojoList.get(i).getPolNo().equals(
                    tLCPolPojoList.get(i).getMainPolNo())){
                checkMainRisk = true;
                break;
            }
        }

        if (!checkMainRisk) {
            logger.debug(this.getClass().getName()+",该投保单不存在主险信息！");
            requestInfo.addError(this.getClass().getName()+",该投保单不存在主险信息！");
            return false;
        }
        return true;
    }

    /**
     * @Author: zhuyiming
     * @Description: 业务处理
     * @Date: Created in 14:12 2017/9/22
     */
    @Override
    public TradeInfo dealData(TradeInfo requestInfo) {
        /**
         * 新老规则引擎线程的启用，分两大部分、三个阶段
         * 互联网核心只启用新规则引擎，结论也只参考ODM新规则引擎
         */
        flag= exeSQL.getOneValue("select code from ldcode where CODETYPE = 'baffletype'");

        LCContPojo lcContPojo = (LCContPojo) requestInfo.getData(LCContPojo.class.getName());
        List<LCPolPojo> lcPolPojoList= (List<LCPolPojo>) requestInfo.getData(LCPolPojo.class.getName());
        lcAppntPojo = (LCAppntPojo) requestInfo.getData(LCAppntPojo.class.getName());
        lcInsuredPojoList = (List<LCInsuredPojo>) requestInfo.getData(LCInsuredPojo.class.getName());
        lcLCAddressPojoList  =   (List<LCAddressPojo>) requestInfo.getData(LCAddressPojo.class.getName());
        lcBnfPojoList = (List<LCBnfPojo>)requestInfo.getData(LCBnfPojo.class.getName());
        List<LCDutyPojo> lcDutyPojoList = (List<LCDutyPojo>)requestInfo.getData(LCDutyPojo.class.getName());
        List<LCCustomerImpartPojo> lcCustomerImpartPojoList= (List<LCCustomerImpartPojo>)requestInfo.getData(LCCustomerImpartPojo.class.getName());

        //dutycode拼接字符串
        for(int i=0;i<lcDutyPojoList.size();i++){
            String dutyCode= lcDutyPojoList.get(i).getDutyCode();
            dutyCodeString=dutyCodeString+"'"+dutyCode+"',";
        }
        if(dutyCodeString.length()>1) {
            dutyCodeString.substring(0, dutyCodeString.length() - 1);
        }

        //添加告知信息判断1015001
        //添加告知信息判断1015001
        if(lcCustomerImpartPojoList!=null&&lcCustomerImpartPojoList.size()>0){
            for(int i=0;i<lcCustomerImpartPojoList.size();i++){
                LCCustomerImpartPojo lcCustomerImpartPojo = lcCustomerImpartPojoList.get(0);
                if("E02".equals(lcCustomerImpartPojo.getImpartVer())){
                    ImpartVer=lcCustomerImpartPojo.getImpartVer();
                }

                if("E03".equals(lcCustomerImpartPojo.getImpartVer())){
                    ImpartVer=lcCustomerImpartPojo.getImpartVer();
                    if("41301".equals(lcCustomerImpartPojo.getImpartCode())){
                        ImpartCode=lcCustomerImpartPojo.getImpartCode();
                        String impartParamModle = lcCustomerImpartPojo.getImpartParamModle();
                        if(!impartParamModle.isEmpty()) {
                            if (impartParamModle.startsWith("北京市")||impartParamModle.startsWith("上海市")||impartParamModle.indexOf("广州市")!=-1||impartParamModle.indexOf("深圳市")!=-1) {
                                Impartparammodle = impartParamModle;
                            }
                        }
                    }
                }

            }
        }

        mOperator= lcContPojo.getOperator();

        LCPolPojo lcPolPojo=null;


        Result tODMResult = new Result();
        if("true".equals(barrier)){
            logger.debug("保单号：" + lcContPojo.getContNo() + "，规则引擎挡板开启！");
            //模拟规则引擎核保结论
            tODMResult.setUwresultList(new ArrayList());
            tODMResult.setFlag(true);
            requestInfo.addData(Result.class.getName(), tODMResult);
            return requestInfo;
        }else if ("true".equals(uwCheckFalg)){

            logger.debug("保单号：" + lcContPojo.getContNo() + "，规则校验开启！");
            int polCount=  lcPolPojoList.size();
            mContNo = lcContPojo.getContNo(); // 获得保单号
            mPContNo = lcContPojo.getProposalContNo();
            //    LCAppntDB tLCAppntDB = new LCAppntDB();
            //    tLCAppntDB.setContNo(mContNo);
            if(lcAppntPojo==null) {
                requestInfo.addError("查询投保人信息失败！");
                logger.debug("查询投保人信息失败！");
                return requestInfo;
            }

            //  mLCAppntSchema = tLCAppntDB.getSchema();
            //   LCInsuredDB tLCInsuredDB = new LCInsuredDB();
            //   tLCInsuredDB.setContNo(mContNo);
            //   mAllInsuredSet = tLCInsuredDB.query();
            LMUWSet tLMUWSetUnpass = new LMUWSet(); // 未通过的核保规则
            LMUWSet tLMUWSetAll = null; // 所有核保规则
            LMUWSchema tLMUWSchema = null;
            Connection tConnection = null;
            try {

                tConnection = DBConnPool.getConnection("lbasedataSource");
                tLMUWSetUnpass.clear();
                // 对险种保单进行循环
                for (int nPolIndex = 0; nPolIndex < polCount; nPolIndex++) {
                    lcPolPojo = lcPolPojoList.get(nPolIndex);
                    LMRiskAppPojo LMRiskAppPojo = redisCommonDao.getEntityRelaDB(LMRiskAppPojo.class,lcPolPojo.getRiskCode());



                    tUWStyle = LMRiskAppPojo.getRISKSTYLE();//规则分类
                    mOldPolNo = lcPolPojo.getPolNo(); // 获得保单险种号

                    // 准备算法，获取某险种的所有核保规则的集合

                    if (tLMUWSetAll != null) {
                        tLMUWSetAll.clear();
                    }

                    //1. 获取该险种的所有核保规则的集合
                    tLMUWSetAll = CheckKinds(lcContPojo, lcPolPojo,lcInsuredPojoList.get(0).getContPlanCode());
                    if (tLMUWSetAll == null) {

                    }

                    //2. 准备数据，从险种信息中获取各项计算信息
                    CheckPolInit(lcPolPojo, lcContPojo,lcInsuredPojoList.get(0),lcDutyPojoList);

                    // 个人单核保
//	        mPolPassFlag = "0"; // 核保通过标志，初始为未核保
                    int n = tLMUWSetAll.size(); // 核保规则数量
                    // 先判断是否需要进行自核校验，如果不用直接跳过
                    LMRiskDB aLMRiskDB = new LMRiskDB();
                    aLMRiskDB.setRiskCode(lcPolPojo.getRiskCode());
                    aLMRiskDB.getInfo();

                    //当主险险种规则核保不通过时，视为核保不通过
                    if ((n == 0 || "N".equals(aLMRiskDB.getUWFlag())) && !mPolPassFlag.equals("5")) {
                        mPolPassFlag = "9"; // 无核保规则则置标志为通过
                        mContPassFlag = "9";
                    } else { // 目前目前所有的险种均有一些公共的核保规则,所以必定走该分枝
                        for (int i = 1; i <= n; i++) { // 对每个核保规则逐个校验。
                            if (aLMRiskDB.getUWFlag() != null && aLMRiskDB.getUWFlag().equals("Y")) {
                                // 取计算编码
                                tLMUWSchema = tLMUWSetAll.get(i);
                                mCalCode = tLMUWSchema.getCalCode();
                                cHeckFlag = tLMUWSchema.getCheckFlag();
                                //3. 执行规则
                                long mStartMillis = System.currentTimeMillis();
                                int aCheckPol = CheckPol(lcPolPojo.getInsuredNo(), lcPolPojo.getRiskCode(), cHeckFlag, tConnection,lcPolPojoList);
                                logger.info(mCalCode + "------" + tLMUWSchema.getRemark());
                                if (aCheckPol == 0) {
                                    long tUseTime = System.currentTimeMillis() - mStartMillis;
                                    logger.info(mCalCode + "------" + tLMUWSchema.getRemark() + "------核保通过，语句耗时" + tUseTime / 1000.0 + "秒");
                                } else {
                                    long tUseTime = System.currentTimeMillis() - mStartMillis;
                                    logger.info(mCalCode + "------" + tLMUWSchema.getRemark() + "------核保不通过，语句耗时" + tUseTime / 1000.0 + "秒");
                                    //对自核提示信息进行解析
                                    parsePolUWResult(tLMUWSchema, lcPolPojo.getRiskCode());
                                    tLMUWSetUnpass.add(tLMUWSchema);

                                    mPolPassFlag = "5"; // 待人工核保
                                    mContPassFlag = "5";
                                    mAllPolPassFlag = "5";
                                }
                            }
                        } // end of for(对每个核保规则逐个校验)

                        // 需要人工核保时候，校验核保返回核保员核保级别
              /*   if (tLMUWSetUnpass.size() > 0) {
                                       mUWGrade = getUWGrade(lcPolPojo.getInsuredNo());
                 }*/

                        if (mPolPassFlag.equals("0")) {
                            mPolPassFlag = "9";
                        }
                        logger.info("匹配数:" + tLMUWSetAll.size() + "级别:" + mUWGrade);
                    }

                    if (dealOnePol(lcPolPojo, tLMUWSetUnpass, lcContPojo, requestInfo) == false) {

                    }
                }


                /* 合同核保 */
                LMUWSet tLMUWSetContUnpass = new LMUWSet(); // 未通过的合同核保规则
                //所有合同核保规则
                LMUWSet tLMUWSetContAll = CheckKinds4(lcContPojo, lcPolPojoList.get(0),lcInsuredPojoList.get(0).getContPlanCode());

                // 准备数据，从险种信息中获取各项计算信息
                CheckContInit(lcContPojo, lcPolPojoList,lcDutyPojoList); // 设置mCalBase的一些值。
                // 个人合同核保
                int tCount = tLMUWSetContAll.size(); // 核保规则数量

                if (tCount == 0) {
                    if (!mContPassFlag.equals("1")) {
                        mContPassFlag = "9";
                    } // 无核保规则则置标志为通过
                } else {
                    for (int index = 1; index <= tCount; index++) {
                        tLMUWSchema = new LMUWSchema();
                        tLMUWSchema = tLMUWSetContAll.get(index);
                        mCalCode = tLMUWSchema.getCalCode();
                        cHeckFlag = tLMUWSchema.getCheckFlag();
                        long mStartMillis = System.currentTimeMillis();
                        logger.info(mCalCode + "------" + tLMUWSchema.getRemark());
                        if (CheckPol(lcContPojo.getInsuredNo(), "000000", cHeckFlag, tConnection,lcPolPojoList) == 0) {
                            long tUseTime = System.currentTimeMillis() - mStartMillis;
                            logger.info(mCalCode + "------" + tLMUWSchema.getRemark() + "------核保通过，语句耗时" + tUseTime / 1000.0 + "秒");
                        } else {
                            long tUseTime = System.currentTimeMillis() - mStartMillis;
                            logger.info(mCalCode + "------" + tLMUWSchema.getRemark() + "------核保不通过，语句耗时" + tUseTime / 1000.0 + "秒");
                            //对自核提示信息进行解析
                            parseContUWResult(tLMUWSchema);
                            tLMUWSetContUnpass.add(tLMUWSchema);
                            mContPassFlag = "5"; // 核保不通过，待人工核保
                        }
                    }
                    if (mUWGrade == null || mUWGrade.equals("")) {
                        mUWGrade = "A";
                    }
                    if (mContPassFlag.equals("0")) {
                        mContPassFlag = "9";
                    }

                    logger.info("合同核保匹配数:" + tLMUWSetContAll.size() + "合同核保未通过数:" + tLMUWSetContUnpass.size() + "级别:" + mUWGrade);


                }
                if ((tLMUWSetUnpass != null && tLMUWSetUnpass.size() > 0)
                        || (tLMUWSetContUnpass != null && tLMUWSetContUnpass.size() > 0)) {
                    logger.info("核保不通过规则汇总显示：");
                    for (int i = 1; i <= tLMUWSetUnpass.size(); i++) {
                        logger.info(tLMUWSetUnpass.get(i).getUWCode() + "------" + tLMUWSetUnpass.get(i).getRemark() + "------核保不通过");
                    }
                    for (int i = 1; i <= tLMUWSetContUnpass.size(); i++) {
                        logger.info(tLMUWSetContUnpass.get(i).getUWCode() + "------" + tLMUWSetContUnpass.get(i).getRemark() + "------核保不通过");
                    }
                }

                dealOneCont(lcContPojo, tLMUWSetContUnpass);
                prepareInsured(lcContPojo, lcInsuredPojoList);

                if(errorSqlCount>0){
                    requestInfo.addError("执行Sql语句失败!");
                    return requestInfo;
                }

                prepareOutputData();
                PubSubmit tSubmit = new PubSubmit();
                if (!tSubmit.submitData(mResult, "")) {
                    // @@错误处理
                    this.mErrors.copyAllErrors(tSubmit.mErrors);
                    CError tError = new CError();
                    tError.moduleName = "LCInsuredUWBL";
                    tError.functionName = "submitData";
                    tError.errorMessage = "数据提交失败!";
                    this.mErrors.addOneError(tError);
                    requestInfo.addError("数据提交失败!");
                }
                if (tLMUWSetUnpass != null && tLMUWSetUnpass.size() > 0) {
                    requestInfo.addError(tLMUWSetUnpass.get(1).getRemark());
                } else if (tLMUWSetContUnpass != null && tLMUWSetContUnpass.size() > 0) {
                    requestInfo.addError(tLMUWSetContUnpass.get(1).getRemark());
                }
            }catch (Exception e){
                requestInfo.addError("核保数据处理异常");
                logger.info("核保数据处理异常:"+e.getMessage());
            }finally {
                if(tConnection!=null){
                    try {
                        tConnection.close();
                    }catch (SQLException m){
                        requestInfo.addError("核保数据连接关闭异常");
                        logger.info("核保数据连接关闭异常:"+m.getMessage());
                    }
                }
                requestInfo.addData(Result.class.getName(), tODMResult);
                return requestInfo;
            }
        } else {
            //互联网核心修改，不另起线程
            /*startNewThread(requestInfo);*/
            Engine mEngine = SpringContextUtils.getBeanByClass(Engine.class);
            long beginTime = System.currentTimeMillis();
            logger.debug("保单号：" + lcContPojo.getContNo() + "，开始准备规则引擎数据！");
            tODMResult = mEngine.execute(requestInfo);
            logger.debug("保单号：" + lcContPojo.getContNo() + "，准备数据 + 规则引擎执行完毕！时间：" + (System.currentTimeMillis() - beginTime)/1000.0 + "s");
            requestInfo.addData(Result.class.getName(), tODMResult);
            return requestInfo;
        }
//        if(requestInfo.hasError()){
//            requestInfo.addError("保单号：" + lcContPojo.getContNo() + "调用规则失败！");
//            logger.debug("保单号：" + lcContPojo.getContNo() + "调用规则失败！");
//            return requestInfo;
//        }
//        //处理获取到的核保结论
//        dealODMData(requestInfo);
//
//        return requestInfo;
    }

    /**
     * @Author: zhuyiming
     * @Description: 处理获取到的核保结论
     * @Date: Created in 16:03 2017/9/22
     */
    public boolean dealODMData(TradeInfo requestInfo) {
        //执行保单级规则存储
        prepareContRules(requestInfo);
        //险种级核保规则存储
        preparePolRules(requestInfo);

        return true;
    }

    /**
     * @Author: zhuyiming
     * @Description: 执行保单级规则存储,存储LC表
     * @Date: Created in 16:03 2017/9/22
     */
    public boolean prepareContRules(TradeInfo requestInfo) {
        String tCurrentDate = PubFun.getCurrentDate();
        String tCurrentTime = PubFun.getCurrentTime();
        Reflections tReflections = new Reflections();
        logger.debug("执行保单级规则存储开始");
        String tContPassFlag = null;
        Result tODMResult = (Result) requestInfo.getData(Result.class.getName());
        if(tODMResult.isFlag()){
            tContPassFlag = "9";
        }else {
            tContPassFlag = "5";
        }
        LCContPojo tLCContPojo = (LCContPojo) requestInfo.getData(LCContPojo.class.getName());
        //ODM规则引擎改变保单表LCContPojo
        logger.debug("ODM规则引擎改变保单表LCContPojo");
        tLCContPojo.setApproveFlag("9");
        tLCContPojo.setApproveCode("ABC-CLOUD");
        tLCContPojo.setApproveDate(tCurrentDate);
        tLCContPojo.setApproveTime(tCurrentTime);
        tLCContPojo.setUWFlag(tContPassFlag);
        tLCContPojo.setUWOperator("ABC-CLOUD");
        tLCContPojo.setUWDate(tCurrentDate);
        tLCContPojo.setUWTime(tCurrentTime);
        tLCContPojo.setModifyDate(tCurrentDate);
        tLCContPojo.setModifyTime(tCurrentTime);
        //合同核保主表C表
        logger.debug("合同核保主表C表");
        LCCUWMasterPojo tLCCUWMasterPojo = (LCCUWMasterPojo) requestInfo.getData(LCCUWMasterPojo.class.getName());

        if (tLCCUWMasterPojo == null) {
            tLCCUWMasterPojo = new LCCUWMasterPojo();
            String tCUWMasterID = PubFun1.CreateMaxNo(PubFun1.PhysicNo, 22);
            tLCCUWMasterPojo.setCUWMasterID(tCUWMasterID);
            tLCCUWMasterPojo.setShardingID(tLCContPojo.getShardingID());
            tLCCUWMasterPojo.setContID(tLCContPojo.getContID());
            tLCCUWMasterPojo.setContNo(tLCContPojo.getContNo());
            tLCCUWMasterPojo.setGrpContNo(tLCContPojo.getGrpContNo());
            tLCCUWMasterPojo.setProposalContNo(tLCContPojo.getProposalContNo());
            tLCCUWMasterPojo.setUWNo(1);
            tLCCUWMasterPojo.setInsuredNo(tLCContPojo.getInsuredNo());
            tLCCUWMasterPojo.setInsuredName(tLCContPojo.getInsuredName());
            tLCCUWMasterPojo.setAppntNo(tLCContPojo.getAppntNo());
            tLCCUWMasterPojo.setAppntName(tLCContPojo.getAppntName());
            tLCCUWMasterPojo.setAgentCode(tLCContPojo.getAgentCode());
            tLCCUWMasterPojo.setAgentGroup(tLCContPojo.getAgentGroup());
            tLCCUWMasterPojo.setUWGrade("A1");//核保级别
            tLCCUWMasterPojo.setAppGrade("A1");//申报级别
            tLCCUWMasterPojo.setPostponeDay("");
            tLCCUWMasterPojo.setPostponeDate("");
            tLCCUWMasterPojo.setAutoUWFlag("1");//1--自动核保  2--人工核保
            tLCCUWMasterPojo.setState(tContPassFlag);
            tLCCUWMasterPojo.setPassFlag(tContPassFlag);
            tLCCUWMasterPojo.setHealthFlag("0");
            tLCCUWMasterPojo.setSpecFlag("0");
            tLCCUWMasterPojo.setQuesFlag("0");
            tLCCUWMasterPojo.setReportFlag("0");
            tLCCUWMasterPojo.setChangePolFlag("0");
            tLCCUWMasterPojo.setPrintFlag("0");
            tLCCUWMasterPojo.setPrintFlag2("0");
            tLCCUWMasterPojo.setManageCom(tLCContPojo.getManageCom());
            tLCCUWMasterPojo.setUWIdea("");
            tLCCUWMasterPojo.setUpReportContent("");
            tLCCUWMasterPojo.setOperator("ABC-CLOUD");
            tLCCUWMasterPojo.setMakeDate(tCurrentDate);
            tLCCUWMasterPojo.setMakeTime(tCurrentTime);
            tLCCUWMasterPojo.setModifyDate(tCurrentDate);
            tLCCUWMasterPojo.setModifyTime(tCurrentTime);
        } else {
            tLCCUWMasterPojo.setUWNo(tLCCUWMasterPojo.getUWNo() + 1);
            tLCCUWMasterPojo.setState(tContPassFlag);
            tLCCUWMasterPojo.setPassFlag(tContPassFlag);
            tLCCUWMasterPojo.setAutoUWFlag("1"); // 1 自动核保 2 人工核保
            tLCCUWMasterPojo.setUWGrade("A1"); // 核保级别
            tLCCUWMasterPojo.setAppGrade("A1"); // 申报级别
            tLCCUWMasterPojo.setOperator("ABC-CLOUD"); // 操作员
            tLCCUWMasterPojo.setModifyDate(tCurrentDate);
            tLCCUWMasterPojo.setModifyTime(tCurrentTime);
        }
        requestInfo.addData(tLCCUWMasterPojo);

        //合同子表C表
        logger.debug("合同子表C表");
        ArrayList<LCCUWSubPojo> tLCCUWSubPojoList = (ArrayList<LCCUWSubPojo>) requestInfo.getData(LCCUWSubPojo.class.getName());
        LCCUWSubPojo tLCCUWSubPojo = new LCCUWSubPojo();
        int oldUwNo = 0;
        if (tLCCUWSubPojoList == null || tLCCUWSubPojoList.size() == 0) {
            tLCCUWSubPojo.setUWNo(1); // 第1次核保
        } else {
            oldUwNo = tLCCUWSubPojoList.size();
            tLCCUWSubPojo.setUWNo(++oldUwNo); // 第几次核保
        }
        String tCUWSubID = PubFun1.CreateMaxNo(PubFun1.PhysicNo, 22);
        tLCCUWSubPojo.setCUWSubID(tCUWSubID);
        tLCCUWSubPojo.setCUWMasterID(tLCCUWMasterPojo.getCUWMasterID());
        tLCCUWSubPojo.setShardingID(tLCCUWMasterPojo.getShardingID());
        tLCCUWSubPojo.setContNo(tLCCUWMasterPojo.getContNo());
        tLCCUWSubPojo.setGrpContNo(tLCCUWMasterPojo.getGrpContNo());
        tLCCUWSubPojo.setProposalContNo(tLCCUWMasterPojo
                .getProposalContNo());
        tLCCUWSubPojo.setInsuredNo(tLCCUWMasterPojo.getInsuredNo());
        tLCCUWSubPojo.setInsuredName(tLCCUWMasterPojo.getInsuredName());
        tLCCUWSubPojo.setAppntNo(tLCCUWMasterPojo.getAppntNo());
        tLCCUWSubPojo.setAppntName(tLCCUWMasterPojo.getAppntName());
        tLCCUWSubPojo.setAgentCode(tLCCUWMasterPojo.getAgentCode());
        tLCCUWSubPojo.setAgentGroup(tLCCUWMasterPojo.getAgentGroup());
        tLCCUWSubPojo.setUWGrade(tLCCUWMasterPojo.getUWGrade()); // 核保级别
        tLCCUWSubPojo.setAppGrade(tLCCUWMasterPojo.getAppGrade()); // 申请级别
        tLCCUWSubPojo.setAutoUWFlag(tLCCUWMasterPojo.getAutoUWFlag());
        tLCCUWSubPojo.setState(tLCCUWMasterPojo.getState());
        tLCCUWSubPojo.setPassFlag(tLCCUWMasterPojo.getState());
        tLCCUWSubPojo.setPostponeDay(tLCCUWMasterPojo.getPostponeDay());
        tLCCUWSubPojo.setPostponeDate(tLCCUWMasterPojo.getPostponeDate());
        tLCCUWSubPojo.setUpReportContent(tLCCUWMasterPojo
                .getUpReportContent());
        tLCCUWSubPojo.setHealthFlag(tLCCUWMasterPojo.getHealthFlag());
        tLCCUWSubPojo.setSpecFlag(tLCCUWMasterPojo.getSpecFlag());
        tLCCUWSubPojo.setSpecReason(tLCCUWMasterPojo.getSpecReason());
        tLCCUWSubPojo.setQuesFlag(tLCCUWMasterPojo.getQuesFlag());
        tLCCUWSubPojo.setReportFlag(tLCCUWMasterPojo.getReportFlag());
        tLCCUWSubPojo.setChangePolFlag(tLCCUWMasterPojo.getChangePolFlag());
        tLCCUWSubPojo.setChangePolReason(tLCCUWMasterPojo
                .getChangePolReason());
        tLCCUWSubPojo.setAddPremReason(tLCCUWMasterPojo.getAddPremReason());
        tLCCUWSubPojo.setPrintFlag(tLCCUWMasterPojo.getPrintFlag());
        tLCCUWSubPojo.setPrintFlag2(tLCCUWMasterPojo.getPrintFlag2());
        tLCCUWSubPojo.setUWIdea(tLCCUWMasterPojo.getUWIdea());
        tLCCUWSubPojo.setOperator(tLCCUWMasterPojo.getOperator()); // 操作员
        tLCCUWSubPojo.setManageCom(tLCCUWMasterPojo.getManageCom());
        tLCCUWSubPojo.setMakeDate(tCurrentDate);
        tLCCUWSubPojo.setMakeTime(tCurrentTime);
        tLCCUWSubPojo.setModifyDate(tCurrentDate);
        tLCCUWSubPojo.setModifyTime(tCurrentTime);
        requestInfo.addData(tLCCUWSubPojo);

        //根据新规则引擎返回的结果判断保单层核保规则有哪些不通过
        List<UwResult> tUwresultList = tODMResult.getUwresultList();
        List tUWErrorList = new ArrayList();//保存所有没有核保通过的保单层规则
        for(int i=0;i<tUwresultList.size();i++){
            UwResult tUwResult= (UwResult) tUwresultList.get(i);
            if(!tUwResult.isFlag()&&"000000".equals(tUwResult.getRiskCode())){
                logger.info("ODM规则引擎保单层规则"+tUwResult.getRuleCode()+","+tUwResult.getReturnInfo()+"核保没有通过!  " + requestInfo);
                //requestInfo.addError("ODM规则引擎保单层规则"+tUwResult.getRuleCode()+","+tUwResult.getReturnInfo()+"核保没有通过！");
                tUWErrorList.add(tUwResult);
            }
        }
        int mErrorCount = tUWErrorList.size();
        if(mErrorCount>0){
            logger.info("保单层共有"+mErrorCount+"条规则不通过！");
            //requestInfo.addError("保单层共有"+mErrorCount+"条规则不通过！");
        }

        // 核保错误信息c表
        logger.info("核保错误信息c表");
        LCCUWErrorPojo tLCCUWErrorPojo = new LCCUWErrorPojo();
        tLCCUWErrorPojo.setSerialNo("0");
        if (oldUwNo > 0) {
            tLCCUWErrorPojo.setUWNo(oldUwNo);
        } else {
            tLCCUWErrorPojo.setUWNo(1);
        }
        tLCCUWErrorPojo.setContNo(tLCContPojo.getContNo());
        tLCCUWErrorPojo.setGrpContNo(tLCCUWSubPojo.getGrpContNo());
        tLCCUWErrorPojo.setProposalContNo(tLCCUWSubPojo.getProposalContNo());
        tLCCUWErrorPojo.setAppntNo(tLCCUWSubPojo.getAppntNo());
        tLCCUWErrorPojo.setAppntName(tLCCUWSubPojo.getAppntName());
        tLCCUWErrorPojo.setManageCom(tLCCUWSubPojo.getManageCom());
        tLCCUWErrorPojo.setUWRuleCode(""); // 核保规则编码
        tLCCUWErrorPojo.setUWError(""); // 核保出错信息
        tLCCUWErrorPojo.setCurrValue(""); // 当前值
        tLCCUWErrorPojo.setModifyDate(tCurrentDate);
        tLCCUWErrorPojo.setModifyTime(tCurrentTime);
        tLCCUWErrorPojo.setUWPassFlag("");//核保结论，是要根据新规则引擎的返回结果进行赋值
        // 取核保错误信息
        ArrayList<LCCUWErrorPojo> tLCCUWErrorPojoList = new ArrayList<LCCUWErrorPojo>();
        if(mErrorCount>0){
            for(int n=1;n<=mErrorCount;n++){
                //取出有报错信息的结果
                UwResult tUwResult= (UwResult) tUWErrorList.get(n-1);
                //流水号
                String tCUWErrorID = PubFun1.CreateMaxNo(PubFun1.PhysicNo, 22);
                tLCCUWErrorPojo.setCUWErrorID(tCUWErrorID);
                tLCCUWErrorPojo.setCUWMasterID(tLCCUWMasterPojo.getCUWMasterID());
                tLCCUWErrorPojo.setShardingID(tLCCUWMasterPojo.getShardingID());
                String tSerialno = ""+n;
                tLCCUWErrorPojo.setSerialNo(tSerialno);
                tLCCUWErrorPojo.setUWPassFlag("5");
                tLCCUWErrorPojo.setUWRuleCode(tUwResult.getRuleCode());
                tLCCUWErrorPojo.setUWError(tUwResult.getReturnInfo());
                tLCCUWErrorPojo.setUWGrade("A1");
//				Modify by HXXQ201705233366-规则管理平台优化需求  20170606  借用CurrValue字段存储  校验住的规则是否是   非实时规则
                tLCCUWErrorPojo.setCurrValue(tUwResult.getNotRealTimeFlag());
                tLCCUWErrorPojo.setSugPassFlag("1");//区分是ODM保存结果
                tLCCUWErrorPojo.setInsuredNo(tUwResult.getInsuredNo());
                tLCCUWErrorPojo.setInsuredName(tUwResult.getInsuredName());
                LCCUWErrorPojo ttLCCUWErrorPojo = (LCCUWErrorPojo) tReflections.transFields(new LCCUWErrorPojo(),tLCCUWErrorPojo);
                ttLCCUWErrorPojo.setModifyDate(tCurrentDate);
                tLCCUWErrorPojoList.add(ttLCCUWErrorPojo);
            }
        }

        requestInfo.addData(LCCUWErrorPojo.class.getName(), tLCCUWErrorPojoList);
        // 计算是否需要再保. 目前不存在LMWU.UWType = 'LF'的规则，都不分保
        logger.info("暂时不需要临分，执行保单级规则存储结束！");
        return true;
    }

    /**
     * @Author: zhuyiming
     * @Description: 对保单核保结论、核保详细信息进行记录保存
     * @Date: Created in 17:59 2017/9/22
     */

    public boolean preparePolRules(TradeInfo requestInfo) {
        Result tODMResult = (Result) requestInfo.getData(Result.class.getName());
        String tCurrentDate = ((LCCUWMasterPojo) requestInfo.getData(LCCUWMasterPojo.class.getName())).getMakeDate();
        String tCurrentTime = ((LCCUWMasterPojo) requestInfo.getData(LCCUWMasterPojo.class.getName())).getMakeTime();
        Reflections tReflections = new Reflections();
        ArrayList<LCUWMasterPojo> tLCUWMasterPojoList = new ArrayList<LCUWMasterPojo>();
        ArrayList<LCUWSubPojo> tLCUWSubPojoList = new ArrayList<LCUWSubPojo>();
        ArrayList<LCUWErrorPojo> tLCUWErrorPojoList = new ArrayList<LCUWErrorPojo>();
        List<LCPolPojo> tAllLCPolList = new ArrayList<LCPolPojo>();
        logger.info("preparePolRules,对保单核保结论、核保详细信息进行记录保存开始!");
        List<LCPolPojo> tLCPolPojoList = (List) requestInfo.getData(LCPolPojo.class.getName());
        LCContPojo tLCContPojo = (LCContPojo) requestInfo.getData(LCContPojo.class.getName());
        //循环险种
        LCPolPojo tLCPolPojo = new LCPolPojo();
        List tUwResultList = tODMResult.getUwresultList();
        for(int i=0;i<tLCPolPojoList.size();i++){
            tLCPolPojo = tLCPolPojoList.get(i);
            /**
             * 根据险种代码和保单险种号以及新规则引擎的返回结果判断险种的核保标志
             * 并将该险种核保不通过的规则，放到list集合tUwErrorList中
             */
            String tPolPassFlag = "9";
            List tUwErrorList = new ArrayList();
            for(int j=0;j<tUwResultList.size();j++){
                UwResult tUwResult = (UwResult) tUwResultList.get(j);
                if(!tUwResult.isFlag() && tLCPolPojo!=null && tLCPolPojo.getRiskCode()!=null && tLCPolPojo.getRiskCode().equals(tUwResult.getRiskCode())){
                    tPolPassFlag = "5";
//					此处不适用break跳出，需要将核保不通过的记录给保存下来，以待后面使用
                    logger.info("ODM规则引擎险种层规则"+tUwResult.getRuleCode()+","+tUwResult.getReturnInfo()+"核保没有通过！"+ requestInfo);
                    //requestInfo.addError("ODM规则引擎险种层规则"+","+tUwResult.getReturnInfo()+tUwResult.getRuleCode()+"核保没有通过！");
                    tUwErrorList.add(tUwResult);
                }
            }

            //险种层核保主表C表
            List<LCUWMasterPojo> tLCUWMasterPojoList1 = (List) requestInfo.getData(LCUWMasterPojo.class.getName());
            LCUWMasterPojo tLCUWMasterPojo = new LCUWMasterPojo();
            if(tLCUWMasterPojoList1 == null || tLCUWMasterPojoList1.size() == 0){
                //若该险种在表里是不存在的
                String tUWMasterID =  PubFun1.CreateMaxNo(PubFun1.PhysicNo, 22);
                tLCUWMasterPojo.setUWMasterID(tUWMasterID);
                tLCUWMasterPojo.setContID(tLCContPojo.getContID());
                tLCUWMasterPojo.setShardingID(tLCContPojo.getShardingID());
                tLCUWMasterPojo.setContNo(tLCContPojo.getContNo());
                tLCUWMasterPojo.setGrpContNo(tLCPolPojo.getGrpContNo());
                tLCUWMasterPojo.setPolNo(tLCPolPojo.getPolNo());
                tLCUWMasterPojo.setProposalContNo(tLCContPojo.getProposalContNo());
                tLCUWMasterPojo.setProposalNo(tLCPolPojo.getPolNo());
                tLCUWMasterPojo.setUWNo(1);
                tLCUWMasterPojo.setInsuredNo(tLCPolPojo.getInsuredNo());
                tLCUWMasterPojo.setInsuredName(tLCPolPojo.getInsuredName());
                tLCUWMasterPojo.setAppntNo(tLCPolPojo.getAppntNo());
                tLCUWMasterPojo.setAppntName(tLCPolPojo.getAppntName());
                tLCUWMasterPojo.setAgentCode(tLCPolPojo.getAgentCode());
                tLCUWMasterPojo.setAgentGroup(tLCPolPojo.getAgentGroup());
                tLCUWMasterPojo.setUWGrade("A1");
                tLCUWMasterPojo.setAppGrade("A1");
                tLCUWMasterPojo.setPostponeDay("");
                tLCUWMasterPojo.setPostponeDate("");
                tLCUWMasterPojo.setAutoUWFlag("1");
                tLCUWMasterPojo.setState(tPolPassFlag);
                tLCUWMasterPojo.setPassFlag(tPolPassFlag);
                tLCUWMasterPojo.setHealthFlag("0");
                tLCUWMasterPojo.setSpecFlag("0");
                tLCUWMasterPojo.setQuesFlag("0");
                tLCUWMasterPojo.setReportFlag("0");
                tLCUWMasterPojo.setChangePolFlag("0");
                tLCUWMasterPojo.setPrintFlag("0");
                tLCUWMasterPojo.setManageCom(tLCPolPojo.getManageCom());
                tLCUWMasterPojo.setUWIdea("");
                tLCUWMasterPojo.setUpReportContent("");
                tLCUWMasterPojo.setOperator("ABC-CLOUD");
                tLCUWMasterPojo.setMakeDate(tCurrentDate);
                tLCUWMasterPojo.setMakeTime(tCurrentTime);
                tLCUWMasterPojo.setModifyDate(tCurrentDate);
                tLCUWMasterPojo.setModifyTime(tCurrentTime);
            }else if(tLCUWMasterPojoList1.size()  == 1){
                int uwNo = tLCUWMasterPojo.getUWNo();
                uwNo = uwNo+1;
                tLCUWMasterPojo.setUWNo(uwNo);
                tLCUWMasterPojo.setProposalContNo(tLCContPojo.getPolicyNo());
                tLCUWMasterPojo.setPassFlag(tPolPassFlag);
                tLCUWMasterPojo.setState(tPolPassFlag);
                tLCUWMasterPojo.setAutoUWFlag("1");
                tLCUWMasterPojo.setAppGrade("A1");
                tLCUWMasterPojo.setUWGrade("A1");
                tLCUWMasterPojo.setOperator("ABC-CLOUD");
                tLCUWMasterPojo.setModifyDate(tCurrentDate);
                tLCUWMasterPojo.setModifyTime(tCurrentTime);
            }else {
                logger.info("preparePolRules,"+tLCPolPojo.getPolNo() + "个人核保总表取数据不唯一!");
                return false;
            }
            LCUWMasterPojo ttLCUWMasterPojo = (LCUWMasterPojo) tReflections.transFields(new LCUWMasterPojo(),tLCUWMasterPojo);
            ttLCUWMasterPojo.setModifyDate(tCurrentDate);
            ttLCUWMasterPojo.setMakeDate(tCurrentDate);
            tLCUWMasterPojoList.add(ttLCUWMasterPojo);
            //险种层核保子表C表
            List<LCUWSubPojo> tLCUWSubPojoList1 = (List) requestInfo.getData(LCUWSubPojo.class.getName());
            LCUWSubPojo tLCUWSubPojo = new LCUWSubPojo();
            int oldSubCount = 0;
            if(tLCUWSubPojoList1 == null || tLCUWSubPojoList1.size() == 0){
                tLCUWSubPojo.setUWNo(1);//第1此核保
            } else {
                oldSubCount =tLCUWSubPojoList1 .size();
                tLCUWSubPojo.setUWNo(++oldSubCount);//第几次核保
            }
            String tUWSubID = PubFun1.CreateMaxNo(PubFun1.PhysicNo, 22);
            tLCUWSubPojo.setUWSubID(tUWSubID);
            tLCUWSubPojo.setUWMasterID(tLCUWMasterPojo.getUWMasterID());
            tLCUWSubPojo.setShardingID(tLCUWMasterPojo.getShardingID());
            tLCUWSubPojo.setContNo(tLCContPojo.getContNo());
            tLCUWSubPojo.setPolNo(tLCPolPojo.getPolNo());
            tLCUWSubPojo.setGrpContNo(tLCUWMasterPojo.getGrpContNo());
            tLCUWSubPojo.setProposalContNo(tLCUWMasterPojo.getProposalContNo());
            tLCUWSubPojo.setProposalNo(tLCUWMasterPojo.getProposalNo());
            tLCUWSubPojo.setInsuredNo(tLCUWMasterPojo.getInsuredNo());
            tLCUWSubPojo.setInsuredName(tLCUWMasterPojo.getInsuredName());
            tLCUWSubPojo.setAppntNo(tLCUWMasterPojo.getAppntNo());
            tLCUWSubPojo.setAppntName(tLCUWMasterPojo.getAppntName());
            tLCUWSubPojo.setAgentCode(tLCUWMasterPojo.getAgentCode());
            tLCUWSubPojo.setAgentGroup(tLCUWMasterPojo.getAgentGroup());
            tLCUWSubPojo.setUWGrade(tLCUWMasterPojo.getUWGrade());
            tLCUWSubPojo.setAppGrade(tLCUWMasterPojo.getAppGrade());
            tLCUWSubPojo.setAutoUWFlag(tLCUWMasterPojo.getAutoUWFlag());
            tLCUWSubPojo.setState(tLCUWMasterPojo.getState());
            tLCUWSubPojo.setPassFlag(tLCUWMasterPojo.getPassFlag());
            tLCUWSubPojo.setPostponeDay(tLCUWMasterPojo.getPostponeDay());
            tLCUWSubPojo.setPostponeDate(tLCUWMasterPojo.getPostponeDate());
            tLCUWSubPojo.setUpReportContent(tLCUWMasterPojo.getUpReportContent());
            tLCUWSubPojo.setHealthFlag(tLCUWMasterPojo.getHealthFlag());
            tLCUWSubPojo.setSpecFlag(tLCUWMasterPojo.getSpecFlag());
            tLCUWSubPojo.setSpecReason(tLCUWMasterPojo.getSpecReason());
            tLCUWSubPojo.setQuesFlag(tLCUWMasterPojo.getQuesFlag());
            tLCUWSubPojo.setReportFlag(tLCUWMasterPojo.getReportFlag());
            tLCUWSubPojo.setChangePolFlag(tLCUWMasterPojo.getChangePolFlag());
            tLCUWSubPojo.setChangePolReason(tLCUWMasterPojo.getChangePolReason());
            tLCUWSubPojo.setAddPremReason(tLCUWMasterPojo.getAddPremReason());
            tLCUWSubPojo.setPrintFlag(tLCUWMasterPojo.getPrintFlag());
            tLCUWSubPojo.setPrintFlag2(tLCUWMasterPojo.getPrintFlag2());
            tLCUWSubPojo.setUWIdea(tLCUWMasterPojo.getUWIdea());
            tLCUWSubPojo.setOperator(tLCUWMasterPojo.getOperator());
            tLCUWSubPojo.setManageCom(tLCUWMasterPojo.getManageCom());
            tLCUWSubPojo.setMakeDate(tCurrentDate);
            tLCUWSubPojo.setMakeTime(tCurrentTime);
            tLCUWSubPojo.setModifyDate(tCurrentDate);
            tLCUWSubPojo.setModifyTime(tCurrentTime);
            LCUWSubPojo ttLCUWSubPojo = (LCUWSubPojo) tReflections.transFields(new LCUWSubPojo(),tLCUWSubPojo);
            ttLCUWSubPojo.setModifyDate(tCurrentDate);
            ttLCUWSubPojo.setMakeDate(tCurrentDate);
            tLCUWSubPojoList.add(ttLCUWSubPojo);
            //险种层错误信息表C表
            LCUWErrorPojo tLCUWErrorPojo = new LCUWErrorPojo();
            tLCUWErrorPojo.setSerialNo("0");
            if(oldSubCount>0){
                tLCUWErrorPojo.setUWNo(oldSubCount);//第几次核保
            } else {
                tLCUWErrorPojo.setUWNo(1);//第1此核保
            }
            tLCUWErrorPojo.setUWMasterID(tLCUWMasterPojo.getUWMasterID());
            tLCUWErrorPojo.setShardingID(tLCUWMasterPojo.getShardingID());
            tLCUWErrorPojo.setContNo(tLCContPojo.getContNo());
            tLCUWErrorPojo.setGrpContNo(tLCUWMasterPojo.getGrpContNo());
            tLCUWErrorPojo.setProposalContNo(tLCContPojo.getProposalContNo());
            tLCUWErrorPojo.setPolNo(tLCPolPojo.getPolNo());
            tLCUWErrorPojo.setProposalNo(tLCPolPojo.getProposalNo());
            tLCUWErrorPojo.setAppntNo(tLCPolPojo.getAppntNo());
            tLCUWErrorPojo.setAppntName(tLCPolPojo.getAppntName());
            tLCUWErrorPojo.setManageCom(tLCPolPojo.getManageCom());
            tLCUWErrorPojo.setUWRuleCode("");//规则编码
            tLCUWErrorPojo.setUWError("");//错误信息
            tLCUWErrorPojo.setCurrValue("");//取当前值
            tLCUWErrorPojo.setModifyDate(tCurrentDate);
            tLCUWErrorPojo.setModifyTime(tCurrentTime);
            tLCUWErrorPojo.setUWPassFlag("");

            //取错误信息
            if(tUwErrorList.size()>0){
                for(int errorCount = 1;errorCount<=tUwErrorList.size();errorCount++){
                    UwResult tUwresult = (UwResult) tUwErrorList.get(errorCount-1);
                    String tUWErrorID = PubFun1.CreateMaxNo(PubFun1.PhysicNo, 22);
                    tLCUWErrorPojo.setUWErrorID(tUWErrorID);
                    //生成流水号
                    String mSerialno = ""+errorCount;
                    tLCUWErrorPojo.setSerialNo(mSerialno);
                    tLCUWErrorPojo.setUWRuleCode(tUwresult.getRuleCode());
                    tLCUWErrorPojo.setUWError(tUwresult.getReturnInfo());
                    tLCUWErrorPojo.setUWGrade("A1");
//					Modify by HXXQ201705233366-规则管理平台优化需求  20170606  借用CurrValue字段存储  校验住的规则是否是   非实时规则
                    tLCUWErrorPojo.setCurrValue(tUwresult.getNotRealTimeFlag());
                    tLCUWErrorPojo.setSugPassFlag("1");//区分是ODM保存结果
                    tLCUWErrorPojo.setUWPassFlag("5");
                    tLCUWErrorPojo.setInsuredNo(tUwresult.getInsuredNo());
                    tLCUWErrorPojo.setInsuredName(tUwresult.getInsuredName());
                    LCUWErrorPojo ttLCUWErrorPojo = (LCUWErrorPojo) tReflections.transFields(new LCUWErrorPojo(),tLCUWErrorPojo);
                    ttLCUWErrorPojo.setModifyDate(tCurrentDate);
                    tLCUWErrorPojoList.add(ttLCUWErrorPojo);
                }
            }

            tLCPolPojo.setApproveFlag("9");
            tLCPolPojo.setApproveCode("ABC-CLOUD");
            tLCPolPojo.setApproveDate(tCurrentDate);
            tLCPolPojo.setApproveTime(tCurrentTime);
            tLCPolPojo.setUWFlag(tPolPassFlag);
            tLCPolPojo.setUWCode("ABC-CLOUD");
            tLCPolPojo.setUWDate(tCurrentDate);
            tLCPolPojo.setUWTime(tCurrentTime);
            tLCPolPojo.setModifyDate(tCurrentDate);
            tLCPolPojo.setModifyTime(tCurrentTime);
        }
        requestInfo.addData(LCUWMasterPojo.class.getName(), tLCUWMasterPojoList);
        requestInfo.addData(LCUWSubPojo.class.getName(), tLCUWSubPojoList);
        requestInfo.addData(LCUWErrorPojo.class.getName(), tLCUWErrorPojoList);

        logger.info("preparePolRules,对保单核保结论、核保详细信息进行记录保存结束!");
        return true;
    }

    @Override
    public boolean prepareOutputData(TradeInfo requestInfo) {
        //核保错误结论
        Result result = (Result) requestInfo.getData(Result.class.getName());
        List<UwResult> tUwResultList = result.getUwresultList();
        //保单数据
        LCContPojo lcContPojo = (LCContPojo) requestInfo.getData(LCContPojo.class.getName());
        //核保错误数据存储
        List<LCResultInfoPojo> lcResultInfoPojos = new ArrayList<>();
        Reflections reflections = new Reflections();
        if(tUwResultList != null && tUwResultList.size() >0){
            for (int i = 0; i <tUwResultList.size() ; i++) {
                UwResult uwResult = tUwResultList.get(i);
                logger.info("ODM核保规则["+uwResult.getRuleCode()+"]"+uwResult.getReturnInfo());
            }
            UwResult uwResult = tUwResultList.get(tUwResultList.size()-1);
            LCResultInfoPojo lcResultInfoPojo = new LCResultInfoPojo();
            lcResultInfoPojo.setContNo(lcContPojo.getContNo());
            lcResultInfoPojo.setPrtNo(lcContPojo.getPrtNo());
            lcResultInfoPojo.setResultNo(uwResult.getRuleCode());
            lcResultInfoPojo.setResultContent(uwResult.getReturnInfo());
            lcResultInfoPojo.setKey1(uwResult.getRiskCode());
            lcResultInfoPojos.add((LCResultInfoPojo) reflections.transFields(new LCResultInfoPojo(), lcResultInfoPojo));
            requestInfo.addData(LCResultInfoPojo.class.getName(), lcResultInfoPojos);
        }
        requestInfo.removeData(Result.class.getName());
        return true;
    }

    private LMUWSet CheckKinds4(LCContPojo tLCContSchema,LCPolPojo tLCPolSchema,String contplanno) {
        //modify by zhangyfsh req-793  核保规则取值sql调整   2018/01/16  start
        String tSelltype = "'"+tLCContSchema.getSellType()+"'";
        if(!"07".equals(tLCContSchema.getSellType())){
            tSelltype = "'00', '"+tLCContSchema.getSellType()+"'";
        }
        //modify by zhangyfsh req-793  核保规则取值sql调整   2018/01/16  end

        String tsql = "";
        //modify by gq req-652   核保规则取值sql调整   2017/07/18  start
        tsql = "select * from lmuw where riskcode = '000000' and relapoltype = 'BI' and uwtype = '01' and uwstyle in ('00', '" + tUWStyle + "') "//modify by req-1103 微信上线优选终寿 huangxin 20180917
                + "and uwcode not in (select value1 from ldwxconfig where codetype = 'unuwcode' and code1='"+tLCContSchema.getSellType()+"' and (code2='"+tLCPolSchema.getRiskCode().trim()+"' or code2 = '"+contplanno+"')) "//add by wong 2018/4/12 REQ-888 i云保升级
                + "and salechnl = '" + tLCContSchema.getSaleChnl() + "' and selltype in ("+ tSelltype+")  order by uworder,uwcode ";
        //modify by gq req-652   核保规则取值sql调整   2017/07/18  end
        LMUWDB tLMUWDB = new LMUWDB();
        LMUWSet tLMUWSet = tLMUWDB.executeQuery(tsql);
        if (tLMUWDB.mErrors.needDealError() == true) {
            CError.buildErr(this, "合同险种核保信息查询失败!");
            tLMUWSet.clear();
            return null;
        }
        return tLMUWSet;
    }

    /**
     * 个人单核保数据准备
     * 输出：如果发生错误则返回false,否则返回true
     * @param tLCContSchema
     */
    private void CheckContInit(LCContPojo tLCContSchema,List<LCPolPojo> lcPolPojoList,List<LCDutyPojo> lcDutyPojoList ) {
        LCPolPojo tLCPolSchema = lcPolPojoList.get(0);
        int AppntAge = PubFun.calInterval(fDate.getDate(tLCContSchema.getAppntBirthday()), fDate.getDate(tLCPolSchema.getCValiDate()), "Y");
        String AppntSex = tLCContSchema.getAppntSex();
        String AppntJob = lcAppntPojo.getOccupationType();
        String AppAge =  tLCPolSchema.getInsuredAppAge()+"";
        String Job = tLCPolSchema.getOccupationType();

        mCalBase.setPrem(tLCContSchema.getPrem());
        mCalBase.setPayIntv(tLCContSchema.getPayIntv());//add by wangzy req-434 传入payintv参数 20170126
        mCalBase.setGet(tLCContSchema.getAmnt());
        mCalBase.setMult(tLCContSchema.getMult());
        mCalBase.setInsuredNo(tLCContSchema.getInsuredNo());
        mCalBase.setAppntSex(AppntSex);
        mCalBase.setAppntAge(AppntAge);
        mCalBase.setAppntJob(AppntJob);
        mCalBase.setAppAge(Integer.valueOf(AppAge));
        mCalBase.setSex(tLCContSchema.getInsuredSex());
        mCalBase.setJob(Job);
        mCalBase.setCValiDate(tLCContSchema.getCValiDate());
        mCalBase.setSellType(tLCContSchema.getSellType());
        mCalBase.setInsuYearFlag(tLCPolSchema.getInsuYearFlag());

        mCalBase.setSumPrem(tLCContSchema.getSumPrem());
        mCalBase.setAppntIDexpDate(lcAppntPojo.getIdValiDate());
        mCalBase.setAppntNativePlace(lcAppntPojo.getNativePlace());


//		GetAllSumAmnt(tLCContSchema.getInsuredNo());
        GetAllSumAmntAppnt(lcPolPojoList, lcDutyPojoList);
        mCalBase.setSumContFxml_1(PubFun.setPrecisionString(SumContFxml_1,"0.00"));
        mCalBase.setSumContFxml_2(PubFun.setPrecisionString(SumContFxml_2,"0.00"));
        mCalBase.setSumContFxml_4(PubFun.setPrecisionString(SumContFxml_4,"0.00"));
        mCalBase.setSumContFxml_12(PubFun.setPrecisionString(SumContFxml_12,"0.00"));
        mCalBase.setSumContFxml_41(PubFun.setPrecisionString(SumContFxml_41,"0.00"));
        mCalBase.setSumContFxml_43(PubFun.setPrecisionString(SumContFxml_43,"0.00"));


        mCalBase.setLSumDangerAmnt(String.valueOf(LSumDangerAmnt)); //同一被保险人下寿险类的累计危险保额
        mCalBase.setDSumDangerAmnt(String.valueOf(DSumDangerAmnt)); //同一被保险人下重大疾病类的累计危险保额
        mCalBase.setMSumDangerAmnt(String.valueOf(MSumDangerAmnt)); //同一被保险人下人身意外类的累计危险保额
        mCalBase.setSSumDangerAmnt(String.valueOf(SSumDangerAmnt)); //同一被保险人下人身医疗类的累计危险保额
        mCalBase.setSSumDieAmnt(String.valueOf(SSumDieAmnt)); //同一被保险人身故的累计危险保额

        mCalBase.setAppntLSumDangerAmnt(String.valueOf(AppntLSumDangerAmnt));
        mCalBase.setAppntDSumDangerAmnt(String.valueOf(AppntDSumDangerAmnt));
        mCalBase.setAppntMSumDangerAmnt(String.valueOf(AppntMSumDangerAmnt));
        mCalBase.setAppntSSumDangerAmnt(String.valueOf(AppntSSumDangerAmnt));
        mCalBase.setAppntSSumDieAmnt(String.valueOf(AppntSSumDieAmnt));
        mCalBase.setAppntAllSumAmnt(String.valueOf(AppntAllSumAmnt));
        mCalBase.setImpartAmnt("0.00"); //add by gq req-543 告知信息身故保险金额参数添加

        //add by likai 2013-09-24  被保人临分风险保额
        mCalBase.setmLFSumDangerAmnt1(PubFun.setPrecisionString(mLFSumDangerAmnt1,"0.00"));
        mCalBase.setmLFSumDangerAmnt2(PubFun.setPrecisionString(mLFSumDangerAmnt2,"0.00"));
        mCalBase.setmLFSumDangerAmnt4(PubFun.setPrecisionString(mLFSumDangerAmnt4,"0.00"));
        mCalBase.setmLFSumDangerAmnt12(PubFun.setPrecisionString(mLFSumDangerAmnt12,"0.00"));
        mCalBase.setmLFSumDangerAmnt41(PubFun.setPrecisionString(mLFSumDangerAmnt41,"0.00"));
        mCalBase.setmLFSumDangerAmnt42(PubFun.setPrecisionString(mLFSumDangerAmnt42,"0.00"));
        //add by shaozq REQ-673 风险保额区分储蓄和保障类 20170906 start
        mCalBase.setmLFSumDangerAmntBZ1(PubFun.setPrecisionString(mLFSumDangerAmntBZ1,"0.00"));
        mCalBase.setmLFSumDangerAmntBZ2(PubFun.setPrecisionString(mLFSumDangerAmntBZ2,"0.00"));
        mCalBase.setmLFSumDangerAmntBZ4(PubFun.setPrecisionString(mLFSumDangerAmntBZ4,"0.00"));
        mCalBase.setmLFSumDangerAmntBZ12(PubFun.setPrecisionString(mLFSumDangerAmntBZ12,"0.00"));
        mCalBase.setmLFSumDangerAmntBZ41(PubFun.setPrecisionString(mLFSumDangerAmntBZ41,"0.00"));
        mCalBase.setmLFSumDangerAmntBZ42(PubFun.setPrecisionString(mLFSumDangerAmntBZ42,"0.00"));
        mCalBase.setmLFSumDangerAmntCX1(PubFun.setPrecisionString(mLFSumDangerAmntCX1,"0.00"));
        mCalBase.setmLFSumDangerAmntCX2(PubFun.setPrecisionString(mLFSumDangerAmntCX2,"0.00"));
        mCalBase.setmLFSumDangerAmntCX4(PubFun.setPrecisionString(mLFSumDangerAmntCX4,"0.00"));
        mCalBase.setmLFSumDangerAmntCX12(PubFun.setPrecisionString(mLFSumDangerAmntCX12,"0.00"));
        mCalBase.setmLFSumDangerAmntCX41(PubFun.setPrecisionString(mLFSumDangerAmntCX41,"0.00"));
        mCalBase.setmLFSumDangerAmntCX42(PubFun.setPrecisionString(mLFSumDangerAmntCX42,"0.00"));
        //add by shaozq REQ-673 风险保额区分储蓄和保障类 20170906 end
        //add by 20171113 shaozq REQ-693 微信渠道添加是否存在风险保额校验 start
        ExeSQL ExeSql = new ExeSQL();
        String tFlag1 = ExeSql.getOneValue("select case when (select 1 from dual where exists (select 1 from lmcalmode  where riskcode in (select riskcode from lcpol where contno = '"+tLCContSchema.getContNo()+"') and type = '1')) = 1 then 1 else 0 end from dual");
        String tFlag2 = ExeSql.getOneValue("select case when (select 1 from dual where exists (select 1 from lmcalmode  where riskcode in (select riskcode from lcpol where contno = '"+tLCContSchema.getContNo()+"') and type = '2')) = 1 then 2 else 0 end from dual");
        String tFlag4 = ExeSql.getOneValue("select case when (select 1 from dual where exists (select 1 from lmcalmode  where riskcode in (select riskcode from lcpol where contno = '"+tLCContSchema.getContNo()+"') and type = '4')) = 1 then 4 else 0 end from dual");
        String tFlag12 = ExeSql.getOneValue("select case when (select 1 from dual where exists (select 1 from lmcalmode  where riskcode in (select riskcode from lcpol where contno = '"+tLCContSchema.getContNo()+"') and type = '12')) = 1 then 12 else 0 end from dual");
        mCalBase.setmFlag1(tFlag1);
        mCalBase.setmFlag2(tFlag2);
        mCalBase.setmFlag4(tFlag4);
        mCalBase.setmFlag12(tFlag12);
        //add by 20171113 shaozq REQ-693 微信渠道添加是否存在风险保额校验  end
        mCalBase.setContNo(mContNo);
        mCalBase.setInsuredBirthday(tLCContSchema.getInsuredBirthday());

        ExeSQL tExeSQL = new ExeSQL();
        String tSQL = "select decode(preferredphonenum,1,homephone,2,companyphone,mobile) " +
                "from lcaddress where customerno = '"+lcAppntPojo.getAppntNo()+"' and addressno = "+lcAppntPojo.getAddressNo();
        mCalBase.setAppntPreferredPhone(tExeSQL.getOneValue(tSQL));
        tSQL = "select decode(preferredaddress, 1, homeaddress, companyaddress) " +
                "from lcaddress where customerno = '"+lcAppntPojo.getAppntNo()+"' and addressno = "+lcAppntPojo.getAddressNo();
        mCalBase.setAppntPreferredAddress(tExeSQL.getOneValue(tSQL));

        mCalBase.setAppntName(tLCContSchema.getAppntName());
        mCalBase.setInsuredName(tLCContSchema.getInsuredName());

        //抽查比例设置参数
        mCalBase.setInsuredSex(tLCContSchema.getInsuredSex());
        mCalBase.setSaleChnl(tLCContSchema.getSaleChnl());
        mCalBase.setAgentCode(tLCContSchema.getAgentCode());
        String polApplyDate = tLCContSchema.getPolApplyDate();
        if(polApplyDate!=null&&!"".equals(polApplyDate)){
            polApplyDate=polApplyDate.substring(0,10);
        }
        mCalBase.setPolApplyDate(polApplyDate);
        mCalBase.setManageCom(tLCContSchema.getManageCom());
        //add by yinyy REQ-1308 针对身份证证件号码一致客户增加相关校验 2019-03-19 start
        mCalBase.setAppntIdtype(tLCContSchema.getAppntIDType());
        mCalBase.setAppntIdno(tLCContSchema.getAppntIDNo());
        mCalBase.setAppntBirthday(tLCContSchema.getAppntBirthday());
        mCalBase.setInsuredIdtype(tLCContSchema.getInsuredIDType());
        mCalBase.setInsuredIdno(tLCContSchema.getInsuredIDNo());

    }

    /**
     * 个人单核保
     * 输出：如果发生错误则返回false,否则返回true
     */
    private int CheckPol(String tInsuredNo, String tRiskCode,String cHeckFlag,Connection   tConnection, List<LCPolPojo> lcPolPojoList) { //LCPolSchema tLCPolSchema)
        // 计算
        //   Calculator mCalculator = new Calculator();
        //险种层前置规则校验



        Calculator mCalculator = SpringContextUtils.getBeanByClass(Calculator.class);
        mCalculator.setCalCode(mCalCode);
        //增加基本要素
        mCalculator.addBasicFactor("CurrentDate", PubFun.getCurrentDate());
        mCalculator.addBasicFactor("CurrentTime", PubFun.getCurrentTime());
        mCalculator.addBasicFactor("Get", mCalBase.getGet());
        mCalculator.addBasicFactor("Mult", mCalBase.getMult());
        mCalculator.addBasicFactor("Prem", mCalBase.getPrem());
        mCalculator.addBasicFactor("AppAge", mCalBase.getAppAge());
        mCalculator.addBasicFactor("AppntAge", String.valueOf(mCalBase.getAppntAge()));
        mCalculator.addBasicFactor("AppntSex", mCalBase.getAppntSex());
        mCalculator.addBasicFactor("Sex", mCalBase.getSex());
        mCalculator.addBasicFactor("Job", mCalBase.getJob());
        mCalculator.addBasicFactor("PayEndYear", mCalBase.getPayEndYear());
        mCalculator.addBasicFactor("GetStartDate", "");
        mCalculator.addBasicFactor("Years", mCalBase.getYears());
        mCalculator.addBasicFactor("Grp", "");
        mCalculator.addBasicFactor("GetFlag", "");
        mCalculator.addBasicFactor("CValiDate", mCalBase.getCValiDate());
        mCalculator.addBasicFactor("Count", mCalBase.getCount());
        mCalculator.addBasicFactor("FirstPayDate", "");
        mCalculator.addBasicFactor("ContNo", mCalBase.getContNo());
        mCalculator.addBasicFactor("LoanContNo", mCalBase.getLoanContNo());
        mCalculator.addBasicFactor("PolNo", mCalBase.getPolNo());
        mCalculator.addBasicFactor("InsuredNo", mCalBase.getInsuredNo());
        mCalculator.addBasicFactor("SecondInsuredNo", mCalBase.getSecondInsuredNo());
        mCalculator.addBasicFactor("RiskCode", mCalBase.getRiskCode());
        mCalculator.addBasicFactor("Job", mCalBase.getJob());
        mCalculator.addBasicFactor("ImpartAmnt",mCalBase.getImpartAmnt()); //add by gq req-543 告知信息身故保险金额参数添加
        mCalculator.addBasicFactor("LSumDangerAmnt", mCalBase.getLSumDangerAmnt());
        mCalculator.addBasicFactor("DSumDangerAmnt", mCalBase.getDSumDangerAmnt());
        mCalculator.addBasicFactor("ASumDangerAmnt", mCalBase.getASumDangerAmnt());
        mCalculator.addBasicFactor("MSumDangerAmnt", mCalBase.getMSumDangerAmnt());
        mCalculator.addBasicFactor("SSumDieAmnt", mCalBase.getSSumDieAmnt());
        mCalculator.addBasicFactor("ManageCom", mCalBase.getManageCom());
        mCalculator.addBasicFactor("AppntJob", mCalBase.getAppntJob());
        mCalculator.addBasicFactor("MainRiskGet", mCalBase.getMainRiskGet());
        mCalculator.addBasicFactor("RiskSort", mCalBase.getRiskSort());
        mCalculator.addBasicFactor("CustomerNo", mCalBase.getCustomerNo());
        mCalculator.addBasicFactor("Occupation", mCalBase.getOccupation());
        mCalculator.addBasicFactor("MainPolNo", mCalBase.getMainPolNo());
        mCalculator.addBasicFactor("AppAg2", mCalBase.getAppAg2());
        mCalculator.addBasicFactor("AppAge2", mCalBase.getAppAge2());
        mCalculator.addBasicFactor("A2IRelationCode", mCalBase.getA2IRelationCode());
        mCalculator.addBasicFactor("AllYearPrem", mCalBase.getAllYearPrem());
        mCalculator.addBasicFactor("InsuredBirthday", mCalBase.getInsuredBirthday());
        mCalculator.addBasicFactor("RelationToInsured",mCalBase.getRelationToInsured());
        mCalculator.addBasicFactor("InsuYear", mCalBase.getInsuYear());
        mCalculator.addBasicFactor("OccupationType",  mCalBase.getOccupationType());
        mCalculator.addBasicFactor("ContPlanCode",  mCalBase.getContPlanCode());
        mCalculator.addBasicFactor("OccupationCode",  mCalBase.getOccupationCode());
        mCalculator.addBasicFactor("Stature",  String.valueOf(mCalBase.getStature()));
        mCalculator.addBasicFactor("Avoirdupois", String.valueOf(mCalBase.getAvoirdupois()));
        mCalculator.addBasicFactor("SellType",mCalBase.getSellType());
        mCalculator.addBasicFactor("AppntAddress",mCalBase.getAppntAddress());
        mCalculator.addBasicFactor("InsuYearFlag",mCalBase.getInsuYearFlag());
        mCalculator.addBasicFactor("RnewFlag",mCalBase.getRnewFlag());
        mCalculator.addBasicFactor("SumPrem",String.valueOf(mCalBase.getSumPrem()));
        mCalculator.addBasicFactor("AppntIDexpDate",mCalBase.getAppntIDexpDate());
        mCalculator.addBasicFactor("InsureIDexpDate",mCalBase.getInsureIDexpDate());
        mCalculator.addBasicFactor("AppMobile",mCalBase.getAppMobile());
        mCalculator.addBasicFactor("InsureAddress",mCalBase.getInsureAddress());
        mCalculator.addBasicFactor("InsureMobile",mCalBase.getInsureMobile());
        mCalculator.addBasicFactor("AppntHomePhone",mCalBase.getAppHomePhone());
        mCalculator.addBasicFactor("InsureHomePhone",mCalBase.getInsureHomePhone());
        mCalculator.addBasicFactor("InsuredSalary",String.valueOf(mCalBase.getInsuredSalary()));
        mCalculator.addBasicFactor("InsuredOccupationCode",mCalBase.getInsuredOccupationCode());
        mCalculator.addBasicFactor("InsureCity",mCalBase.getInsureCity());
        mCalculator.addBasicFactor("InsureProvince",mCalBase.getInsureProvince());
        mCalculator.addBasicFactor("InsureSSFlag",mCalBase.getInsureSSFlag());
        mCalculator.addBasicFactor("AppntNativePlace", mCalBase.getAppntNativePlace());
        mCalculator.addBasicFactor("InsureNativePlace",mCalBase.getInsureNativePlace());
        mCalculator.addBasicFactor("TUWFlag",mCalBase.getTUWFlag());
        mCalculator.addBasicFactor("TAPPFlag",mCalBase.getTAPPFlag());
        mCalculator.addBasicFactor("PayYears",String.valueOf(mCalBase.getPayYears()));
        mCalculator.addBasicFactor("ImpartCode",mCalBase.getImpartCode());
        mCalculator.addBasicFactor("Impartparammodle",mCalBase.getImpartparammodle());
        mCalculator.addBasicFactor("AppntProvince",mCalBase.getAppntProvince());
        mCalculator.addBasicFactor("AppntCity",mCalBase.getAppntCity());
        mCalculator.addBasicFactor("DutyCodeString",mCalBase.getDutyCodeString());

        mCalculator.addBasicFactor("Fxml_1",mCalBase.getFxml_1());
        mCalculator.addBasicFactor("Fxml_2",mCalBase.getFxml_2());
        mCalculator.addBasicFactor("Fxml_4",mCalBase.getFxml_4());
        mCalculator.addBasicFactor("Fxml_12",mCalBase.getFxml_12());
        mCalculator.addBasicFactor("Fxml_41",mCalBase.getFxml_41());
        mCalculator.addBasicFactor("Fxml_43",mCalBase.getFxml_43());
        mCalculator.addBasicFactor("SumContFxml_1",mCalBase.getSumContFxml_1());
        mCalculator.addBasicFactor("SumContFxml_2",mCalBase.getSumContFxml_2());
        mCalculator.addBasicFactor("SumContFxml_4",mCalBase.getSumContFxml_4());
        mCalculator.addBasicFactor("SumContFxml_12",mCalBase.getSumContFxml_12());
        mCalculator.addBasicFactor("SumContFxml_41",mCalBase.getSumContFxml_41());
        mCalculator.addBasicFactor("SumContFxml_43",mCalBase.getSumContFxml_43());
        mCalculator.addBasicFactor("YWshengu",mCalBase.getYWshengu());
        mCalculator.addBasicFactor("YWyiliao",mCalBase.getYWyiliao());
        mCalculator.addBasicFactor("YWjingtie",mCalBase.getYWjingtie());
        mCalculator.addBasicFactor("DutyCode",mCalBase.getDutyCode());

        mCalculator.addBasicFactor("AppntPreferredPhone", mCalBase.getAppntPreferredPhone());
        mCalculator.addBasicFactor("AppntPreferredAddress", mCalBase.getAppntPreferredAddress());
        mCalculator.addBasicFactor("InsuredPreferredPhone", mCalBase.getInsuredPreferredPhone());
        mCalculator.addBasicFactor("InsuredPreferredAddress", mCalBase.getInsuredPreferredAddress());
        mCalculator.addBasicFactor("AppntName", mCalBase.getAppntName());
        mCalculator.addBasicFactor("InsuredName", mCalBase.getInsuredName());
        mCalculator.addBasicFactor("ImpartVer", ImpartVer);
        mCalculator.addBasicFactor("HaveJiaShi", mCalBase.getHaveJiaShi());
        mCalculator.addBasicFactor("NotHaveJiaShi", mCalBase.getNotHaveJiaShi());

        mCalculator.addBasicFactor("AppntLSumDangerAmnt", mCalBase.getAppntLSumDangerAmnt());
        mCalculator.addBasicFactor("AppntDSumDangerAmnt", mCalBase.getAppntDSumDangerAmnt());
        mCalculator.addBasicFactor("AppntSSumDangerAmnt", mCalBase.getAppntSSumDangerAmnt());
        mCalculator.addBasicFactor("AppntMSumDangerAmnt", mCalBase.getAppntMSumDangerAmnt());
        mCalculator.addBasicFactor("AppntSSumDieAmnt", mCalBase.getAppntSSumDieAmnt());
        mCalculator.addBasicFactor("AppntAllSumAmnt", mCalBase.getAppntAllSumAmnt());
        mCalculator.addBasicFactor("AppntNo", mCalBase.getAppntNo());
        mCalculator.addBasicFactor("ANativePlace", mCalBase.getANativePlace());
        mCalculator.addBasicFactor("payintv", mCalBase.getPayIntv());//add by wangzy req-434 传入payintv参数 20170126


        //抽查比例参数配置
        mCalculator.addBasicFactor("InsuredSex", mCalBase.getInsuredSex());
        mCalculator.addBasicFactor("SSumDangerAmnt", mCalBase.getSSumDangerAmnt());
        mCalculator.addBasicFactor("SaleChnl", mCalBase.getSaleChnl());
        mCalculator.addBasicFactor("AgentGroup", mCalBase.getAgentGroup());
        mCalculator.addBasicFactor("AgentCode", mCalBase.getAgentCode());
        mCalculator.addBasicFactor("AppntYearSumPrem", mCalBase.getAppntYearSumPrem());
        mCalculator.addBasicFactor("PolApplyDate", mCalBase.getPolApplyDate());

        //add by likai 20131011
        mCalculator.addBasicFactor("LFSumDangerAmnt1", mCalBase.getmLFSumDangerAmnt1());
        mCalculator.addBasicFactor("LFSumDangerAmnt2", mCalBase.getmLFSumDangerAmnt2());
        mCalculator.addBasicFactor("LFSumDangerAmnt4", mCalBase.getmLFSumDangerAmnt4());
        mCalculator.addBasicFactor("LFSumDangerAmnt12", mCalBase.getmLFSumDangerAmnt12());
        mCalculator.addBasicFactor("LFSumDangerAmnt41", mCalBase.getmLFSumDangerAmnt41());
        mCalculator.addBasicFactor("LFSumDangerAmnt42", mCalBase.getmLFSumDangerAmnt42());
        //add by shaozq 20170906 REQ-673 校验是否存在风险保额 start
        mCalculator.addBasicFactor("Flag1", mCalBase.getmFlag1());
        mCalculator.addBasicFactor("Flag2", mCalBase.getmFlag2());
        mCalculator.addBasicFactor("Flag4", mCalBase.getmFlag4());
        mCalculator.addBasicFactor("Flag12", mCalBase.getmFlag12());
        mCalculator.addBasicFactor("LFSumDangerAmntBZ1", mCalBase.getmLFSumDangerAmntBZ1());
        mCalculator.addBasicFactor("LFSumDangerAmntBZ2", mCalBase.getmLFSumDangerAmntBZ2());
        mCalculator.addBasicFactor("LFSumDangerAmntBZ4", mCalBase.getmLFSumDangerAmntBZ4());
        mCalculator.addBasicFactor("LFSumDangerAmntBZ12", mCalBase.getmLFSumDangerAmntBZ12());
        mCalculator.addBasicFactor("LFSumDangerAmntBZ41", mCalBase.getmLFSumDangerAmntBZ41());
        mCalculator.addBasicFactor("LFSumDangerAmntBZ42", mCalBase.getmLFSumDangerAmntBZ42());
        mCalculator.addBasicFactor("LFSumDangerAmntCX1", mCalBase.getmLFSumDangerAmntCX1());
        mCalculator.addBasicFactor("LFSumDangerAmntCX2", mCalBase.getmLFSumDangerAmntCX2());
        mCalculator.addBasicFactor("LFSumDangerAmntCX4", mCalBase.getmLFSumDangerAmntCX4());
        mCalculator.addBasicFactor("LFSumDangerAmntCX12", mCalBase.getmLFSumDangerAmntCX12());
        mCalculator.addBasicFactor("LFSumDangerAmntCX41", mCalBase.getmLFSumDangerAmntCX41());
        mCalculator.addBasicFactor("LFSumDangerAmntCX42", mCalBase.getmLFSumDangerAmntCX42());
        //add by 20171113 shaozq REQ-693 微信渠道添加是否存在风险保额校验 start
        mCalculator.addBasicFactor("Flag1", mCalBase.getmFlag1());
        mCalculator.addBasicFactor("Flag2", mCalBase.getmFlag2());
        mCalculator.addBasicFactor("Flag4", mCalBase.getmFlag4());
        mCalculator.addBasicFactor("Flag12", mCalBase.getmFlag12());
        //add by 20171113 shaozq REQ-693 微信渠道添加是否存在风险保额校验 end

        //add by yinyy REQ-1308 针对身份证证件号码一致客户增加相关校验 2019-03-19 start
        mCalculator.addBasicFactor("AppntIdtype", mCalBase.getAppntIdtype());
        mCalculator.addBasicFactor("AppntIdno", mCalBase.getAppntIdno());
        mCalculator.addBasicFactor("AppntBirthday", mCalBase.getAppntBirthday());
        mCalculator.addBasicFactor("InsuredIdtype", mCalBase.getInsuredIdtype());
        mCalculator.addBasicFactor("InsuredIdno", mCalBase.getInsuredIdno());

        String tStr = "";
        //多个受益人校验
        if(lcBnfPojoList!=null&& lcBnfPojoList.size()>1){
            if ("C00039".equals(mCalCode) || "C00041".equals(mCalCode) || "C00046".equals(mCalCode) || "UW8535".equals(mCalCode)
                    || "UW8461".equals(mCalCode) || "UW8540".equals(mCalCode) || "UW8554".equals(mCalCode)
                    || "UW0513".equals(mCalCode)|| "P00034".equals(mCalCode)|| "UW0480".equals(mCalCode)|| "UW0445".equals(mCalCode)) {
                for (int i = 0; i < lcBnfPojoList.size(); i++) {
                    Calculator mCalculator1 = SpringContextUtils.getBeanByClass(Calculator.class);
                    mCalculator1.setCalCode(mCalCode);
                    mCalculator1.addBasicFactor("BnfName", lcBnfPojoList.get(i).getName());
                    mCalculator1.addBasicFactor("BnfRelationToInsured", lcBnfPojoList.get(i).getRelationToInsured());
                    mCalculator1.addBasicFactor("BeneficiarySex", lcBnfPojoList.get(i).getSex());
                    mCalculator1.addBasicFactor("BnfBirthday", lcBnfPojoList.get(i).getBirthday());
                    mCalculator1.addBasicFactor("BnfIDType", lcBnfPojoList.get(i).getIDType());
                    mCalculator1.addBasicFactor("BnfIDNo", lcBnfPojoList.get(i).getIDNo());
                    mCalculator1.addBasicFactor("BnfAddress", lcBnfPojoList.get(i).getPostalAddress());
                    mCalculator1.addBasicFactor("BnfNativePlace", lcBnfPojoList.get(i).getNativePlace());
                    mCalculator1.addBasicFactor("BnfType", lcBnfPojoList.get(i).getBnfType());
                    mCalculator1.addBasicFactor("InsuYear", String.valueOf(lcPolPojoList.get(0).getInsuYear()));
                    mCalculator1.addBasicFactor("InsuYearFlag", lcPolPojoList.get(0).getInsuYearFlag());
                    mCalculator1.addBasicFactor("AppAge", String.valueOf(lcPolPojoList.get(0).getInsuredAppAge()));
                    mCalculator1.addBasicFactor("Sex", lcPolPojoList.get(0).getInsuredSex());


                    if ("0".equals(cHeckFlag)) {
                        tStr = mCalculator1.calculate();
                    } else if ("1".equals(cHeckFlag)) {
                        //   Connection   tConnection = DBConnPool.getConnection("lbasedataSource");

                        if ("1".equals(flag)) {
                            tStr = mCalculator1.calculate(tConnection);
                        }
                    }
                    if (tStr != null && !tStr.trim().equals("") && !tStr.trim().equals("0")) {
                        break;
                    }
                }

            }else {
                if ("0".equals(cHeckFlag)) {
                    tStr = mCalculator.calculate();
                } else if ("1".equals(cHeckFlag)) {
                    //   Connection   tConnection = DBConnPool.getConnection("lbasedataSource");

                    if ("1".equals(flag)) {
                        tStr = mCalculator.calculate(tConnection);
                    }

                }
            }
        }
        else {
            mCalculator.addBasicFactor("BnfName", lcBnfPojoList.get(0).getName());
            mCalculator.addBasicFactor("BnfRelationToInsured", lcBnfPojoList.get(0).getRelationToInsured());
            mCalculator.addBasicFactor("BeneficiarySex", lcBnfPojoList.get(0).getSex());
            mCalculator.addBasicFactor("BnfBirthday", lcBnfPojoList.get(0).getBirthday());
            mCalculator.addBasicFactor("BnfIDType", lcBnfPojoList.get(0).getIDType());
            mCalculator.addBasicFactor("BnfIDNo", lcBnfPojoList.get(0).getIDNo());
            mCalculator.addBasicFactor("BnfAddress", lcBnfPojoList.get(0).getPostalAddress());
            mCalculator.addBasicFactor("BnfNativePlace", lcBnfPojoList.get(0).getNativePlace());
            mCalculator.addBasicFactor("BnfType", lcBnfPojoList.get(0).getBnfType());

            //add by yinyy REQ-1308 针对身份证证件号码一致客户增加相关校验 2019-03-19 end

            if ("0".equals(cHeckFlag)) {
                tStr = mCalculator.calculate();
            } else if ("1".equals(cHeckFlag)) {
                //   Connection   tConnection = DBConnPool.getConnection("lbasedataSource");

                if ("1".equals(flag)) {
                    tStr = mCalculator.calculate(tConnection);
                }
            }
        }
        int mValue;
        if (tStr == null || tStr.trim().equals("") || tStr.trim().equals("0")) {
            mValue = 0;
        } else {
            mValue = 1;
        }
        int count  =mCalculator.mErrors.getErrorCount();
        if(count>0){
            errorSqlCount++;
        }


        logger.debug("核保规则执行结果--" + mValue);
        return mValue;
    }

    /**
     * 对合同级自核提示信息进行解析，将描述的参数替换为具体的值
     * 所用参数和算法中的一致
     * */
    private void parseContUWResult(LMUWSchema cLMUWSchema){
        String uwResult = cLMUWSchema.getRemark();
        uwResult = uwResult.replaceAll("#Get#", mCalBase.getGet());
        uwResult = uwResult.replaceAll("#Mult#", mCalBase.getMult());
        uwResult = uwResult.replaceAll("#Prem#", mCalBase.getPrem());
        uwResult = uwResult.replaceAll("#AppAge#", mCalBase.getAppAge());
        uwResult = uwResult.replaceAll("#AppntAge#", String.valueOf(mCalBase.getAppntAge()));
        uwResult = uwResult.replaceAll("#AppntSex#", mCalBase.getAppntSex());
        uwResult = uwResult.replaceAll("#Sex#", mCalBase.getSex());
        uwResult = uwResult.replaceAll("#Job#", mCalBase.getJob());
        uwResult = uwResult.replaceAll("#CValiDate#", mCalBase.getCValiDate());
        uwResult = uwResult.replaceAll("#ContNo#", mCalBase.getContNo());
        uwResult = uwResult.replaceAll("#InsuredNo#", mCalBase.getInsuredNo());
        uwResult = uwResult.replaceAll("#LSumDangerAmnt#", mCalBase.getLSumDangerAmnt());
        uwResult = uwResult.replaceAll("#DSumDangerAmnt#", mCalBase.getDSumDangerAmnt());
        uwResult = uwResult.replaceAll("#ASumDangerAmnt#", mCalBase.getASumDangerAmnt());
        uwResult = uwResult.replaceAll("#MSumDangerAmnt#", mCalBase.getMSumDangerAmnt());
        uwResult = uwResult.replaceAll("#SSumDieAmnt#", mCalBase.getSSumDieAmnt());

        uwResult = uwResult.replaceAll("#AppntLSumDangerAmnt#", mCalBase.getAppntLSumDangerAmnt());
        uwResult = uwResult.replaceAll("#AppntDSumDangerAmnt#", mCalBase.getAppntDSumDangerAmnt());
        uwResult = uwResult.replaceAll("#AppntSSumDangerAmnt#", mCalBase.getAppntSSumDangerAmnt());
        uwResult = uwResult.replaceAll("#AppntMSumDangerAmnt#", mCalBase.getAppntMSumDangerAmnt());
        uwResult = uwResult.replaceAll("#AppntSSumDieAmnt#", mCalBase.getAppntSSumDieAmnt());
        uwResult = uwResult.replaceAll("#AppntAllSumAmnt#", mCalBase.getAppntAllSumAmnt());

        uwResult = uwResult.replaceAll("#A2IRelationName#", mCalBase.getA2IRelationName());
        uwResult = uwResult.replaceAll("#INativePlace#", mCalBase.getINativeplace());

        uwResult = uwResult.replaceAll("#AppntName#", mCalBase.getAppntName());
        uwResult = uwResult.replaceAll("#InsuredName#", mCalBase.getInsuredName());

        //add by likai 20131011
        uwResult = uwResult.replaceAll("#LFSumDangerAmnt1#", mCalBase.getmLFSumDangerAmnt1());
        uwResult = uwResult.replaceAll("#LFSumDangerAmnt2#", mCalBase.getmLFSumDangerAmnt2());
        uwResult = uwResult.replaceAll("#LFSumDangerAmnt4#", mCalBase.getmLFSumDangerAmnt4());
        uwResult = uwResult.replaceAll("#LFSumDangerAmnt12#", mCalBase.getmLFSumDangerAmnt12());
        uwResult = uwResult.replaceAll("#LFSumDangerAmnt41#", mCalBase.getmLFSumDangerAmnt41());
        uwResult = uwResult.replaceAll("#LFSumDangerAmnt42#", mCalBase.getmLFSumDangerAmnt42());
        //add by shaozq REQ-673 风险保额区分储蓄和保障类 20170906 start
        uwResult = uwResult.replaceAll("#LFSumDangerAmntBZ1#", mCalBase.getmLFSumDangerAmntBZ1());
        uwResult = uwResult.replaceAll("#LFSumDangerAmntBZ2#", mCalBase.getmLFSumDangerAmntBZ2());
        uwResult = uwResult.replaceAll("#LFSumDangerAmntBZ4#", mCalBase.getmLFSumDangerAmntBZ4());
        uwResult = uwResult.replaceAll("#LFSumDangerAmntBZ12#", mCalBase.getmLFSumDangerAmntBZ12());
        uwResult = uwResult.replaceAll("#LFSumDangerAmntBZ41#", mCalBase.getmLFSumDangerAmntBZ41());
        uwResult = uwResult.replaceAll("#LFSumDangerAmntBZ42#", mCalBase.getmLFSumDangerAmntBZ42());
        uwResult = uwResult.replaceAll("#LFSumDangerAmntCX1#", mCalBase.getmLFSumDangerAmntCX1());
        uwResult = uwResult.replaceAll("#LFSumDangerAmntCX2#", mCalBase.getmLFSumDangerAmntCX2());
        uwResult = uwResult.replaceAll("#LFSumDangerAmntCX4#", mCalBase.getmLFSumDangerAmntCX4());
        uwResult = uwResult.replaceAll("#LFSumDangerAmntCX12#", mCalBase.getmLFSumDangerAmntCX12());
        uwResult = uwResult.replaceAll("#LFSumDangerAmntCX41#", mCalBase.getmLFSumDangerAmntCX41());
        uwResult = uwResult.replaceAll("#LFSumDangerAmntCX42#", mCalBase.getmLFSumDangerAmntCX42());
        //add by shaozq REQ-673 风险保额区分储蓄和保障类 20170906 end

        //add by shaozq REQ-693 添加核保规则算法提示参数   20171117 start
        ExeSQL ExeSql = new ExeSQL();
        String sql = "select comcode from ldcode1 where codetype = 'DangerAmnt1' and '"+ mCalBase.getAppAge()+"' between code and code1";
        String tsql = ExeSql.getOneValue(sql);
        uwResult = uwResult.replaceAll("#DangerAmntRemark1#", tsql);
        String sql2 = "select comcode from ldcode1 where codetype = 'DangerAmnt2' and '"+ mCalBase.getAppAge()+"' between code and code1";
        String tsql2 = ExeSql.getOneValue(sql2);
        uwResult = uwResult.replaceAll("#DangerAmntRemark2#", tsql2);
        String sql3 = "select comcode from ldcode1 where codetype = 'DangerAmnt3' and '"+ mCalBase.getAppAge()+"' between code and code1";
        String tsql3 = ExeSql.getOneValue(sql3);
        uwResult = uwResult.replaceAll("#DangerAmntRemark3#", tsql3);
        String sql4 = "select comcode from ldcode1 where codetype = 'DangerAmnt4' and '"+ mCalBase.getAppAge()+"' between code and code1";
        String tsql4 = ExeSql.getOneValue(sql4);
        uwResult = uwResult.replaceAll("#DangerAmntRemark4#", tsql4);
        SSRS tSSRS = new SSRS();
        String sql5 = "select comcode,othersign from ldcode1 where codetype = 'DangerAmnt5' and '"+ mCalBase.getAppAge()+"' between code and code1";
        String sql6 = "select 1 from ldcode1 where codetype = 'DangerAmnt5' and to_date('"+mCalBase.getPolApplyDate()+"','yyyy/mm/dd') between to_date(CODENAME, 'yyyy/mm/dd') and to_date(CODEALIAS, 'yyyy/mm/dd')";
        String tsql6 = ExeSql.getOneValue(sql6);
        tSSRS = ExeSql.execSQL(sql5);
        if (tSSRS.getMaxRow() > 0) {
            if("1".equals(tsql6)){
                uwResult = uwResult.replaceAll("#DangerAmntRemark5#", tSSRS.GetText(1, 2));
            }else{
                uwResult = uwResult.replaceAll("#DangerAmntRemark5#", tSSRS.GetText(1, 1));
            }
        }
        //add by shaozq REQ-693 添加核保规则算法提示参数   20171117 end
        //add by yinyy REQ-1308 针对身份证证件号码一致客户增加相关校验 2019-03-19 start

      /*  String customerSql = " select b.QuestionType,b.name,b.gender,b.birthday from LCPastContToCustomer b where b.prtno = '"+mCalBase.getContNo()+"'";
        tSSRS= ExeSql.execSQL(customerSql);

        String iContent = "";
        String iName = "";
        String iSex = "";
        String iBirthDay = "";
        if (tSSRS.getMaxRow() > 0) {
            //当投保人和被保险人为同一人或
            //        String tIdentity = "";
            //        String Content = "";
            //        String tName = "";
            //        String tSex = "";
            //        String tBirthDay = "";
            //        String iIdentity = "";投保人和被保险人非同一人且同时被校验，只显示为投保人
            for(int i=1;i<=tSSRS.getMaxRow();i++){
                if("1999999".equals(tSSRS.GetText(i, 1))){
                    tIdentity = "投保人";
                    if((mCalBase.getAppntName() !=tSSRS.GetText(i, 2)) && tName !="姓名、"){
                        tName = "姓名、";
                    }
                    if((mCalBase.getAppntSex() !=tSSRS.GetText(i, 3)) && tSex != "性别、"){
                        tSex = "性别、";
                    }
                    if((mCalBase.getAppntBirthday() !=tSSRS.GetText(i, 4)) && tBirthDay != "出生日期、"){
                        tBirthDay = "出生日期、";
                    }
                }else{
                    if("".equals(tIdentity)){//投保人和被保险人非同一人且同时被校验，只显示为“投保人”
                        iIdentity = "被保险人";
                        if((mCalBase.getInsuredName() !=tSSRS.GetText(i, 2)) && iName !="姓名、"){
                            iName = "姓名、";
                        }
                        if((mCalBase.getInsuredSex() !=tSSRS.GetText(i, 3)) && iSex != "性别、"){
                            iSex = "性别、";
                        }
                        if((mCalBase.getInsuredBirthday() !=tSSRS.GetText(i, 4)) && iBirthDay != "出生日期、"){
                            iBirthDay = "出生日期、";
                        }
                    }
                }

            }
        }
        if(!"".equals(tIdentity)){
            Content = tName+tSex+tBirthDay;
            if(Content!=null&&Content!=""){
                uwResult = uwResult.replaceAll("#Identity#",tIdentity);
                uwResult = uwResult.replaceAll("#Content#",Content);
            }
        }
        if("".equals(tIdentity) && !"".equals(iIdentity)){
            iContent = iName+iSex+iBirthDay;
            if(iContent!=null&&iContent!=""){
                uwResult = uwResult.replaceAll("#Identity#",iIdentity);
                uwResult = uwResult.replaceAll("#Content#",iContent);		}
        }*/
        //add by yinyy REQ-1308 针对身份证证件号码一致客户增加相关校验 2019-03-19 end
        cLMUWSchema.setRemark(uwResult);
    }

    /**
     * 操作一张保单的业务处理
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     * @param tLCContSchema LCContSchema
     * @param tLMUWSetContUnpass LMUWSetContUnpass
     * @return boolean
     */
    private boolean dealOneCont(LCContPojo tLCContSchema,
                                LMUWSet tLMUWSetContUnpass) {
        prepareContUW(tLCContSchema, tLMUWSetContUnpass);

        LCContSchema tLCContSchemaDup = new LCContSchema();
        LCContSchema lcContSchema  =getSchema(tLCContSchema);
        tLCContSchemaDup.setSchema(lcContSchema);
        mAllLCContSet.add(tLCContSchemaDup);

        LCCUWMasterSet tLCCUWMasterSet = new LCCUWMasterSet();
        tLCCUWMasterSet.set(mLCCUWMasterSet);
        mAllLCCUWMasterSet.add(tLCCUWMasterSet);

        LCCUWSubSet tLCCUWSubSet = new LCCUWSubSet();
        tLCCUWSubSet.set(mLCCUWSubSet);
        mAllLCCUWSubSet.add(tLCCUWSubSet);

        LCCUWErrorSet tLCCUWErrorSet = new LCCUWErrorSet();
        tLCCUWErrorSet.set(mLCCUWErrorSet);
        mAllLCCUWErrorSet.add(tLCCUWErrorSet);

        return true;
    }
    /**
     * 准备合同核保信息
     * 输出：如果发生错误则返回false,否则返回true
     */
    private boolean prepareContUW(LCContPojo tLCContSchema,
                                  LMUWSet tLMUWSetContUnpass) {
        int batchNo=0;
        /*-----------自核与复核作为同一个节点时需要置复核状态-----------*/
        tLCContSchema.setApproveFlag("9");
        tLCContSchema.setApproveCode(mOperator);
        tLCContSchema.setApproveDate(PubFun.getCurrentDate());
        tLCContSchema.setApproveTime(PubFun.getCurrentTime());
        /*--------------------------------------------------------*/
        tLCContSchema.setUWFlag(mContPassFlag);
        tLCContSchema.setUWOperator(mOperator);
        logger.debug("uwdate:"+tLCContSchema.getUWDate());
        //logger.debug("uwdate:"+mLCContSchema.getAppUWDate());
        tLCContSchema.setUWDate(PubFun.getCurrentDate());
        //保存保险合同成立日
        //tLCContSchema.setAppUWDate(PubFun.getCurrentDate());
        //logger.debug("uwdate:"+mLCContSchema.getAppUWDate());
        logger.debug("uwdate:"+tLCContSchema.getUWDate());
        tLCContSchema.setUWTime(PubFun.getCurrentTime());
        tLCContSchema.setModifyDate(PubFun.getCurrentDate());
        tLCContSchema.setModifyTime(PubFun.getCurrentTime());

        //合同核保主表
        LCCUWMasterSchema tLCCUWMasterSchema = new LCCUWMasterSchema();
        LCCUWMasterDB tLCCUWMasterDB = new LCCUWMasterDB();
        tLCCUWMasterDB.setContNo(mContNo);
        LCCUWMasterSet tLCCUWMasterSet = new LCCUWMasterSet();
        tLCCUWMasterSet = tLCCUWMasterDB.query();
        if (tLCCUWMasterDB.mErrors.needDealError()) {
            this.mErrors.copyAllErrors(tLCCUWMasterDB.mErrors);
            CError.buildErr(this, mContNo + "合同核保总表取数失败!");
            return false;
        }

        if (tLCCUWMasterSet.size() == 0) {
            String tCUWMasterID = PubFun1.CreateMaxNo(PubFun1.PhysicNo, 22);
            tLCCUWMasterSchema.setCUWMasterID(tCUWMasterID);
            tLCCUWMasterSchema.setShardingID(tLCContSchema.getShardingID());
            tLCCUWMasterSchema.setContID(tLCContSchema.getContID());
            tLCCUWMasterSchema.setContNo(tLCContSchema.getContNo());
            tLCCUWMasterSchema.setContNo(mContNo);
            tLCCUWMasterSchema.setGrpContNo(tLCContSchema.getGrpContNo());
            tLCCUWMasterSchema.setProposalContNo(tLCContSchema.
                    getProposalContNo());
            tLCCUWMasterSchema.setUWNo(1);
            tLCCUWMasterSchema.setInsuredNo(tLCContSchema.getInsuredNo());
            tLCCUWMasterSchema.setInsuredName(tLCContSchema.getInsuredName());
            tLCCUWMasterSchema.setAppntNo(tLCContSchema.getAppntNo());
            tLCCUWMasterSchema.setAppntName(tLCContSchema.getAppntName());
            tLCCUWMasterSchema.setAgentCode(tLCContSchema.getAgentCode());
            tLCCUWMasterSchema.setAgentGroup(tLCContSchema.getAgentGroup());
            tLCCUWMasterSchema.setUWGrade(mUWGrade); //核保级别
            tLCCUWMasterSchema.setAppGrade(mUWGrade); //申报级别
            tLCCUWMasterSchema.setPostponeDay("");
            tLCCUWMasterSchema.setPostponeDate("");
            tLCCUWMasterSchema.setAutoUWFlag("1"); // 1 自动核保 2 人工核保
            tLCCUWMasterSchema.setState(mContPassFlag);
            tLCCUWMasterSchema.setPassFlag(mContPassFlag);
            tLCCUWMasterSchema.setHealthFlag("0");
            tLCCUWMasterSchema.setSpecFlag("0");
            tLCCUWMasterSchema.setQuesFlag("0");
            tLCCUWMasterSchema.setReportFlag("0");
            tLCCUWMasterSchema.setChangePolFlag("0");
            tLCCUWMasterSchema.setPrintFlag("0");
            tLCCUWMasterSchema.setPrintFlag2("0");
            tLCCUWMasterSchema.setManageCom(tLCContSchema.getManageCom());
            tLCCUWMasterSchema.setUWIdea("");
            tLCCUWMasterSchema.setUpReportContent("");
            tLCCUWMasterSchema.setOperator(mOperator); //操作员
            tLCCUWMasterSchema.setMakeDate(PubFun.getCurrentDate());
            tLCCUWMasterSchema.setMakeTime(PubFun.getCurrentTime());
            tLCCUWMasterSchema.setModifyDate(PubFun.getCurrentDate());
            tLCCUWMasterSchema.setModifyTime(PubFun.getCurrentTime());
        } else {
            tLCCUWMasterSchema = tLCCUWMasterSet.get(1);
            tLCCUWMasterSchema.setUWNo(tLCCUWMasterSchema.getUWNo() + 1);
            //add by zhaoshanshan 20160510 BUG-321 更新客户信息 start
            tLCCUWMasterSchema.setInsuredNo(tLCContSchema.getInsuredNo());
            tLCCUWMasterSchema.setInsuredName(tLCContSchema.getInsuredName());
            tLCCUWMasterSchema.setAppntNo(tLCContSchema.getAppntNo());
            tLCCUWMasterSchema.setAppntName(tLCContSchema.getAppntName());
            tLCCUWMasterSchema.setAgentCode(tLCContSchema.getAgentCode());
            tLCCUWMasterSchema.setAgentGroup(tLCContSchema.getAgentGroup());
            //add by zhaoshanshan 20160510 BUG-321 更新客户信息 end
            tLCCUWMasterSchema.setState(mContPassFlag);
            tLCCUWMasterSchema.setPassFlag(mContPassFlag);
            tLCCUWMasterSchema.setAutoUWFlag("1"); // 1 自动核保 2 人工核保
            tLCCUWMasterSchema.setUWGrade(mUWGrade); //核保级别
            tLCCUWMasterSchema.setAppGrade(mUWGrade); //申报级别
            tLCCUWMasterSchema.setOperator(mOperator); //操作员
            tLCCUWMasterSchema.setModifyDate(PubFun.getCurrentDate());
            tLCCUWMasterSchema.setModifyTime(PubFun.getCurrentTime());
        }

        // 合同核保轨迹表
        LCCUWSubSchema tLCCUWSubSchema = new LCCUWSubSchema();
        LCCUWSubDB tLCCUWSubDB = new LCCUWSubDB();

        tLCCUWSubDB.setContNo(mContNo);
        LCCUWSubSet tLCCUWSubSet = new LCCUWSubSet();
        tLCCUWSubSet = tLCCUWSubDB.query();
        if (tLCCUWSubDB.mErrors.needDealError()) {
            CError.buildErr(this, mContNo + "合同核保轨迹表查失败!");
            return false;
        }

        int m = tLCCUWSubSet.size();
        int nUWNo = 1;
        batchNo=nUWNo;
        if (m > 0) {
            nUWNo = tLCCUWSubSet.get(m).getUWNo()+1;
            batchNo=nUWNo;
            tLCCUWSubSchema.setUWNo(nUWNo); //第几次核保
        } else {
            tLCCUWSubSchema.setUWNo(1); //第1次核保
        }
        String tCUWSubID = PubFun1.CreateMaxNo(PubFun1.PhysicNo, 22);
        tLCCUWSubSchema.setCUWSubID(tCUWSubID);
        tLCCUWSubSchema.setCUWMasterID(tLCCUWMasterSchema.getCUWMasterID());
        tLCCUWSubSchema.setShardingID(tLCCUWMasterSchema.getShardingID());
        tLCCUWSubSchema.setContNo(tLCCUWMasterSchema.getContNo());
        tLCCUWSubSchema.setGrpContNo(tLCCUWMasterSchema.getGrpContNo());
        tLCCUWSubSchema.setProposalContNo(tLCCUWMasterSchema.getProposalContNo());
        tLCCUWSubSchema.setInsuredNo(tLCCUWMasterSchema.getInsuredNo());
        tLCCUWSubSchema.setInsuredName(tLCCUWMasterSchema.getInsuredName());
        tLCCUWSubSchema.setAppntNo(tLCCUWMasterSchema.getAppntNo());
        tLCCUWSubSchema.setAppntName(tLCCUWMasterSchema.getAppntName());
        tLCCUWSubSchema.setAgentCode(tLCCUWMasterSchema.getAgentCode());
        tLCCUWSubSchema.setAgentGroup(tLCCUWMasterSchema.getAgentGroup());
        tLCCUWSubSchema.setUWGrade(tLCCUWMasterSchema.getUWGrade()); //核保级别
        tLCCUWSubSchema.setAppGrade(tLCCUWMasterSchema.getAppGrade()); //申请级别
        tLCCUWSubSchema.setAutoUWFlag(tLCCUWMasterSchema.getAutoUWFlag());
        tLCCUWSubSchema.setState(tLCCUWMasterSchema.getState());
        tLCCUWSubSchema.setPassFlag(tLCCUWMasterSchema.getState());
        tLCCUWSubSchema.setPostponeDay(tLCCUWMasterSchema.getPostponeDay());
        tLCCUWSubSchema.setPostponeDate(tLCCUWMasterSchema.getPostponeDate());
        tLCCUWSubSchema.setUpReportContent(tLCCUWMasterSchema.
                getUpReportContent());
        tLCCUWSubSchema.setHealthFlag(tLCCUWMasterSchema.getHealthFlag());
        tLCCUWSubSchema.setSpecFlag(tLCCUWMasterSchema.getSpecFlag());
        tLCCUWSubSchema.setSpecReason(tLCCUWMasterSchema.getSpecReason());
        tLCCUWSubSchema.setQuesFlag(tLCCUWMasterSchema.getQuesFlag());
        tLCCUWSubSchema.setReportFlag(tLCCUWMasterSchema.getReportFlag());
        tLCCUWSubSchema.setChangePolFlag(tLCCUWMasterSchema.getChangePolFlag());
        tLCCUWSubSchema.setChangePolReason(tLCCUWMasterSchema.
                getChangePolReason());
        tLCCUWSubSchema.setAddPremReason(tLCCUWMasterSchema.getAddPremReason());
        tLCCUWSubSchema.setPrintFlag(tLCCUWMasterSchema.getPrintFlag());
        tLCCUWSubSchema.setPrintFlag2(tLCCUWMasterSchema.getPrintFlag2());
        tLCCUWSubSchema.setUWIdea(tLCCUWMasterSchema.getUWIdea());
        tLCCUWSubSchema.setOperator(tLCCUWMasterSchema.getOperator()); //操作员
        tLCCUWSubSchema.setManageCom(tLCCUWMasterSchema.getManageCom());
        tLCCUWSubSchema.setMakeDate(PubFun.getCurrentDate());
        tLCCUWSubSchema.setMakeTime(PubFun.getCurrentTime());
        tLCCUWSubSchema.setModifyDate(PubFun.getCurrentDate());
        tLCCUWSubSchema.setModifyTime(PubFun.getCurrentTime());

        mLCCUWSubSet.clear();
        mLCCUWSubSet.add(tLCCUWSubSchema);

        // 核保错误信息表
        LCCUWErrorSchema tLCCUWErrorSchema = new LCCUWErrorSchema();
        tLCCUWErrorSchema.setSerialNo("0");
        tLCCUWErrorSchema.setUWNo(nUWNo);
        tLCCUWErrorSchema.setContNo(mContNo);
        tLCCUWErrorSchema.setGrpContNo(tLCCUWSubSchema.getGrpContNo());
        tLCCUWErrorSchema.setProposalContNo(tLCContSchema.getThirdPartyOrderId());
        tLCCUWErrorSchema.setInsuredNo(tLCCUWSubSchema.getInsuredNo());
        tLCCUWErrorSchema.setInsuredName(tLCCUWSubSchema.getInsuredName());
        tLCCUWErrorSchema.setAppntNo(tLCCUWSubSchema.getAppntNo());
        tLCCUWErrorSchema.setAppntName(tLCCUWSubSchema.getAppntName());
        tLCCUWErrorSchema.setManageCom(tLCCUWSubSchema.getManageCom());
        tLCCUWErrorSchema.setUWRuleCode(""); //核保规则编码
        tLCCUWErrorSchema.setUWError(""); //核保出错信息
        tLCCUWErrorSchema.setCurrValue(""); //当前值
        tLCCUWErrorSchema.setModifyDate(PubFun.getCurrentDate());
        tLCCUWErrorSchema.setModifyTime(PubFun.getCurrentTime());
        tLCCUWErrorSchema.setUWPassFlag(mPolPassFlag);

        //取核保错误信息
        mLCCUWErrorSet.clear();
        int merrcount = tLMUWSetContUnpass.size();
        if (merrcount > 0) {
            for (int i = 1; i <= merrcount; i++) {
                //取出错信息
                LMUWSchema tLMUWSchema = new LMUWSchema();
                tLMUWSchema = tLMUWSetContUnpass.get(i);
                //生成流水号
                String tserialno = "" + i;
                String tCUWErrorID = PubFun1.CreateMaxNo(PubFun1.PhysicNo, 22);
                tLCCUWErrorSchema.setCUWErrorID(tCUWErrorID);
                tLCCUWErrorSchema.setCUWMasterID(tLCCUWMasterSchema.getCUWMasterID());
                tLCCUWErrorSchema.setShardingID(tLCCUWMasterSchema.getShardingID());
                tLCCUWErrorSchema.setSerialNo(tserialno);
                tLCCUWErrorSchema.setUWRuleCode(tLMUWSchema.getUWCode()); //核保规则编码
                tLCCUWErrorSchema.setUWError(tLMUWSchema.getRemark().trim()); //核保出错信息，即核保规则的文字描述内容
                tLCCUWErrorSchema.setUWGrade(tLMUWSchema.getUWGrade());
                tLCCUWErrorSchema.setCurrValue(""); //当前值

                LCCUWErrorSchema ttLCCUWErrorSchema = new LCCUWErrorSchema();
                ttLCCUWErrorSchema.setSchema(tLCCUWErrorSchema);
                mLCCUWErrorSet.add(ttLCCUWErrorSchema);
            }
        }
        mLCCUWMasterSet.clear();
        // tLCCUWMasterSchema.setBatchNo(batchNo);
        mLCCUWMasterSet.add(tLCCUWMasterSchema);

        return true;
    }

    /**
     * 为个单被保人核保错误信息表准备数据
     *

     * @return boolean
     */
    private boolean prepareInsured(LCContPojo tLCContSchema,List<LCInsuredPojo> tLCInsuredSet){
        int tLoop = tLCInsuredSet.size();
        for(int i=0;i<tLoop;i++){
            int batchNo = 0;//核保批次号
            LCInsuredPojo LCInsuredPojo = new LCInsuredPojo();
            LCInsuredPojo=tLCInsuredSet.get(i);
            int tuwno = 0;
            LCIndUWMasterSchema tLCIndUWMasterSchema = new LCIndUWMasterSchema();
            LCIndUWMasterDB tLCIndUWMasterDB = new LCIndUWMasterDB();
            tLCIndUWMasterDB.setInsuredNo(LCInsuredPojo.getInsuredNo());
            tLCIndUWMasterDB.setContNo(mContNo);
            LCIndUWMasterSet tLCIndUWMasterSet = new LCIndUWMasterSet();
            tLCIndUWMasterSet = tLCIndUWMasterDB.query();
            if (tLCIndUWMasterDB.mErrors.needDealError()) {
                this.mErrors.copyAllErrors(tLCIndUWMasterDB.mErrors);
                CError.buildErr(this, mOldPolNo + "个人核保总表取数失败!");
                return false;
            }

            int n = tLCIndUWMasterSet.size();
            if (n == 0) {
                tLCIndUWMasterSchema.setContNo(mContNo);
                tLCIndUWMasterSchema.setGrpContNo(LCInsuredPojo.getGrpContNo());
                tLCIndUWMasterSchema.setProposalContNo(mPContNo);
                tLCIndUWMasterSchema.setUWNo(1);
                tLCIndUWMasterSchema.setInsuredNo(LCInsuredPojo.getInsuredNo());
                tLCIndUWMasterSchema.setInsuredName(LCInsuredPojo.getName());
                tLCIndUWMasterSchema.setAppntNo(LCInsuredPojo.getAppntNo());
                tLCIndUWMasterSchema.setAppntName("");
                tLCIndUWMasterSchema.setAgentCode(tLCContSchema.getAgentCode());
                tLCIndUWMasterSchema.setAgentGroup(tLCContSchema.getAgentGroup());
                tLCIndUWMasterSchema.setUWGrade(mUWGrade); // 核保级别
                tLCIndUWMasterSchema.setAppGrade(mUWGrade); // 申报级别
                tLCIndUWMasterSchema.setPostponeDay("");
                tLCIndUWMasterSchema.setPostponeDate("");
                tLCIndUWMasterSchema.setAutoUWFlag("1"); // 1 自动核保 2 人工核保
                tLCIndUWMasterSchema.setState(mPolPassFlag);
                tLCIndUWMasterSchema.setPassFlag(mPolPassFlag);
                tLCIndUWMasterSchema.setHealthFlag("0");
                tLCIndUWMasterSchema.setSpecFlag("0");
                tLCIndUWMasterSchema.setQuesFlag("0");
                tLCIndUWMasterSchema.setReportFlag("0");
                tLCIndUWMasterSchema.setChangePolFlag("0");
                tLCIndUWMasterSchema.setPrintFlag("0");
                tLCIndUWMasterSchema.setManageCom(tLCContSchema.getManageCom());
                tLCIndUWMasterSchema.setUWIdea("");
                tLCIndUWMasterSchema.setUpReportContent("");
                tLCIndUWMasterSchema.setOperator(mOperator); // 操作员
                tLCIndUWMasterSchema.setMakeDate(PubFun.getCurrentDate());
                tLCIndUWMasterSchema.setMakeTime(PubFun.getCurrentTime());
                tLCIndUWMasterSchema.setModifyDate(PubFun.getCurrentDate());
                tLCIndUWMasterSchema.setModifyTime(PubFun.getCurrentTime());
            } else if (n == 1) {
                tLCIndUWMasterSchema = tLCIndUWMasterSet.get(1);

                tuwno = tLCIndUWMasterSchema.getUWNo();
                tuwno = tuwno + 1;

                tLCIndUWMasterSchema.setUWNo(tuwno);
                tLCIndUWMasterSchema.setProposalContNo(mPContNo);
                tLCIndUWMasterSchema.setState(mPolPassFlag);
                tLCIndUWMasterSchema.setPassFlag(mPolPassFlag);
                tLCIndUWMasterSchema.setAutoUWFlag("1"); // 1 自动核保 2 人工核保
                tLCIndUWMasterSchema.setUWGrade(mUWGrade); // 核保级别
                tLCIndUWMasterSchema.setAppGrade(mUWGrade); // 申报级别
                tLCIndUWMasterSchema.setOperator(mOperator); // 操作员
                tLCIndUWMasterSchema.setModifyDate(PubFun.getCurrentDate());
                tLCIndUWMasterSchema.setModifyTime(PubFun.getCurrentTime());
            } else {
                this.mErrors.copyAllErrors(tLCIndUWMasterDB.mErrors);
                CError.buildErr(this, mOldPolNo + "个人核保总表取数据不唯一!");
                return false;
            }

            // 核保轨迹表 ln 2008-10-21 modify uwno计算方法修改
            LCIndUWSubSchema tLCIndUWSubSchema = new LCIndUWSubSchema();
            LCIndUWSubDB tLCIndUWSubDB = new LCIndUWSubDB();
            //tLCUWSubDB.setPolNo(mOldPolNo);
            String sqlUwno = "select * from LCIndUWSub where contno ='"+ mContNo +"' and insuredno='"
                    +LCInsuredPojo.getInsuredNo()+"' order by uwno desc ";
            LCIndUWSubSet tLCIndUWSubSet = new LCIndUWSubSet();
            tLCIndUWSubSet = tLCIndUWSubDB.executeQuery(sqlUwno);
            if (tLCIndUWSubSet.mErrors.needDealError()) {
                this.mErrors.copyAllErrors(tLCIndUWSubSet.mErrors);
                CError.buildErr(this, mOldPolNo + "个人核保轨迹表查失败!");
                return false;
            }

            int m = tLCIndUWSubSet.size();
            tLCIndUWSubSchema.setUWNo(m+1); // 第几次核保
            batchNo=m+1;

            tLCIndUWSubSchema.setContNo(mContNo);
            tLCIndUWSubSchema.setGrpContNo(tLCIndUWMasterSchema.getGrpContNo());
            tLCIndUWSubSchema.setProposalContNo(tLCIndUWMasterSchema.getProposalContNo());
            tLCIndUWSubSchema.setInsuredNo(tLCIndUWMasterSchema.getInsuredNo());
            tLCIndUWSubSchema.setInsuredName(tLCIndUWMasterSchema.getInsuredName());
            tLCIndUWSubSchema.setAppntNo(tLCIndUWMasterSchema.getAppntNo());
            tLCIndUWSubSchema.setAppntName(tLCIndUWMasterSchema.getAppntName());
            tLCIndUWSubSchema.setAgentCode(tLCIndUWMasterSchema.getAgentCode());
            tLCIndUWSubSchema.setAgentGroup(tLCIndUWMasterSchema.getAgentGroup());
            //     tLCIndUWSubSchema.setUWGrade(tLCIndUWMasterSchema.getUWGrade()); // 核保级别
            tLCIndUWSubSchema.setAppGrade(tLCIndUWMasterSchema.getAppGrade()); // 申请级别
            tLCIndUWSubSchema.setAutoUWFlag(tLCIndUWMasterSchema.getAutoUWFlag());
            tLCIndUWSubSchema.setState(tLCIndUWMasterSchema.getState());
            tLCIndUWSubSchema.setPassFlag(tLCIndUWMasterSchema.getState());
            tLCIndUWSubSchema.setPostponeDay(tLCIndUWMasterSchema.getPostponeDay());
            tLCIndUWSubSchema.setPostponeDate(tLCIndUWMasterSchema.getPostponeDate());
            tLCIndUWSubSchema.setUpReportContent(tLCIndUWMasterSchema
                    .getUpReportContent());
            tLCIndUWSubSchema.setHealthFlag(tLCIndUWMasterSchema.getHealthFlag());
            tLCIndUWSubSchema.setSpecFlag(tLCIndUWMasterSchema.getSpecFlag());
            tLCIndUWSubSchema.setSpecReason(tLCIndUWMasterSchema.getSpecReason());
            tLCIndUWSubSchema.setQuesFlag(tLCIndUWMasterSchema.getQuesFlag());
            tLCIndUWSubSchema.setReportFlag(tLCIndUWMasterSchema.getReportFlag());
            tLCIndUWSubSchema.setChangePolFlag(tLCIndUWMasterSchema.getChangePolFlag());
            tLCIndUWSubSchema.setChangePolReason(tLCIndUWMasterSchema
                    .getChangePolReason());
            tLCIndUWSubSchema.setAddPremReason(tLCIndUWMasterSchema.getAddPremReason());
            tLCIndUWSubSchema.setPrintFlag(tLCIndUWMasterSchema.getPrintFlag());
            tLCIndUWSubSchema.setPrintFlag2(tLCIndUWMasterSchema.getPrintFlag2());
            tLCIndUWSubSchema.setUWIdea(tLCIndUWMasterSchema.getUWIdea());
            tLCIndUWSubSchema.setOperator(tLCIndUWMasterSchema.getOperator()); // 操作员
            tLCIndUWSubSchema.setManageCom(tLCIndUWMasterSchema.getManageCom());
            tLCIndUWSubSchema.setMakeDate(PubFun.getCurrentDate());
            tLCIndUWSubSchema.setMakeTime(PubFun.getCurrentTime());
            tLCIndUWSubSchema.setModifyDate(PubFun.getCurrentDate());
            tLCIndUWSubSchema.setModifyTime(PubFun.getCurrentTime());

            mLCIndUWSubSet.add(tLCIndUWSubSchema);
            if("5".equals(mInsPassFlag)){
                // 核保错误信息表
                LCIndUWErrorSchema tLCIndUWErrorSchema = new LCIndUWErrorSchema();
                LCIndUWErrorDB tLCIndUWErrorDB = new LCIndUWErrorDB();
                String UWNoSql="select * from lcinduwerror where insuredno='" +tLCIndUWMasterSchema.getInsuredNo()+
                        "' and contno='"+mContNo+"' order by uwno desc";//);
                LCIndUWErrorSet tLCIndUWErrorSet = new LCIndUWErrorSet();
                tLCIndUWErrorSet = tLCIndUWErrorDB.executeQuery(UWNoSql);
                if (tLCIndUWErrorDB.mErrors.needDealError()) {
                    this.mErrors.copyAllErrors(tLCIndUWErrorDB.mErrors);
                    CError.buildErr(this, mOldPolNo + "个人错误信息表查询失败!");
                    return false;
                }
                if(tLCIndUWErrorSet.size()==0){
                    m=0;
                }else{
                    m=tLCIndUWErrorSet.get(1).getUWNo();
                }
                tLCIndUWErrorSchema.setUWNo(m+1);
                tLCIndUWErrorSchema.setContNo(mContNo);
                tLCIndUWErrorSchema.setGrpContNo(tLCContSchema.getGrpContNo());
                tLCIndUWErrorSchema.setProposalContNo(mPContNo);
                tLCIndUWErrorSchema.setInsuredNo(tLCContSchema.getInsuredNo());
                tLCIndUWErrorSchema.setInsuredName(tLCContSchema.getInsuredName());
                tLCIndUWErrorSchema.setAppntNo(tLCContSchema.getAppntNo());
                tLCIndUWErrorSchema.setAppntName(tLCContSchema.getAppntName());
                tLCIndUWErrorSchema.setManageCom(tLCContSchema.getManageCom());
                tLCIndUWErrorSchema.setModifyDate(PubFun.getCurrentDate());
                tLCIndUWErrorSchema.setModifyTime(PubFun.getCurrentTime());
                tLCIndUWErrorSchema.setUWPassFlag(mPolPassFlag);

                String tserialno = PubFun1.CreateMaxNo("LCIndUWError", 20);
                tLCIndUWErrorSchema.setSerialNo(tserialno);
                tLCIndUWErrorSchema.setUWRuleCode(""); //转储规则引擎接口返回的模板的Ruleid
                tLCIndUWErrorSchema.setUWError(""); // 核保出错信息，即核保规则的文字描述内容
                tLCIndUWErrorSchema.setUWGrade(mUWGrade);
                tLCIndUWErrorSchema.setCurrValue(""); //  转储规则引擎接口返回的模板号

                LCIndUWErrorSchema ttLCIndUWErrorSchema = new LCIndUWErrorSchema();
                ttLCIndUWErrorSchema.setSchema(tLCIndUWErrorSchema);
                mLCIndUWErrorSet.add(ttLCIndUWErrorSchema);
            }
            tLCIndUWMasterSchema.setBatchNo(batchNo);
            mLCIndUWMasterSet.add(tLCIndUWMasterSchema);
        }
        return true;
    }

    /**
     * 准备返回前台统一存储数据 输出：如果发生错误则返回false,否则返回true
     */
    private boolean prepareOutputData() {
        mResult.clear();
//        map.put(mLCContSchema, "UPDATE");
        map.put(mLCCUWMasterSet.get(1), "DELETE&INSERT");
        map.put(mLCCUWSubSet, "DELETE&INSERT");
        map.put(mLCCUWErrorSet, "DELETE&INSERT");

        //   map.put(mAllLCPolSet, "UPDATE");
        // map.put(mAllInsuredSet, "UPDATE");
        int n = mAllLCUWMasterSet.size();
        for (int i = 1; i <= n; i++) {
            LCUWMasterSchema tLCUWMasterSchema = mAllLCUWMasterSet.get(i);
            map.put(tLCUWMasterSchema, "DELETE&INSERT");
        }
        map.put(mAllLCUWSubSet, "DELETE&INSERT");
        for(int i=1;i<=mAllLCUWSubSet.size();i++){
            LCUWSubSchema lcuwSubSchema = mAllLCUWSubSet.get(i);
            logger.info("险种子表结论UWSubID:"+lcuwSubSchema.getUWSubID()+"");
        }
        map.put(mAllLCErrSet, "DELETE&INSERT");

        for (int i = 1; i <= mLCIndUWMasterSet.size(); i++) {
            LCIndUWMasterSchema tLCIndUWMasterSchema = mLCIndUWMasterSet.get(i);
            map.put(tLCIndUWMasterSchema, "DELETE&INSERT");
        }
        //map.put(mLCIndUWMasterSet.get(1), "DELETE&INSERT");
        map.put(mLCIndUWSubSet, "DELETE&INSERT");

        for (int i = 1; i <= mLCIndUWSubSet.size(); i++) {
            LCIndUWSubSchema lcIndUWSubSchema = mLCIndUWSubSet.get(i);
            logger.info("个人险种子表结论,UWNo:"+lcIndUWSubSchema.getUWNo()+",ContNo:"+lcIndUWSubSchema.getContNo()+",InsuredNo:"+lcIndUWSubSchema.getInsuredNo());
        }

        map.put(mLCIndUWErrorSet, "DELETE&INSERT");
        /** 发送拒保通知书 */
        if (mContPassFlag.equals("1")) {
            //       map.put(mLOPRTManagerSchema, "DELETE&INSERT");
        }
        //modify  yangl REQ-401 核心业务系统人工核保改造需求优化（第三期） 20160920 end
        mResult.add(map);
        logger.debug("this.mPolPassFlag==" + this.mPolPassFlag);
        logger.debug("this.mAllPolPassFlag==" + this.mAllPolPassFlag);
        logger.debug("this.mContPassFlag==" + this.mContPassFlag);
        TransferData aTransferData = new TransferData();
        aTransferData.setNameAndValue("PolPassFlag", this.mPolPassFlag);
        aTransferData.setNameAndValue("ContPassFlag", this.mContPassFlag);
        aTransferData.setNameAndValue("ProductSaleFlag", ProductSaleFlag);
        aTransferData.setNameAndValue("AllPolPassFlag", this.mAllPolPassFlag);
        mResult.add(aTransferData);
        return true;
    }

    private LCContSchema getSchema(LCContPojo tLCContSchema){
        LCContSchema lcContSchema = new LCContSchema();
        lcContSchema.setGrpContNo(tLCContSchema.getGrpContNo());
        lcContSchema.setContNo(tLCContSchema.getContNo());
        lcContSchema.setProposalContNo(tLCContSchema.getProposalContNo());
        lcContSchema.setPrtNo(tLCContSchema.getPrtNo());
        lcContSchema.setContType(tLCContSchema.getContType());
        lcContSchema.setFamilyType(tLCContSchema.getFamilyType());
        lcContSchema.setFamilyID(tLCContSchema.getFamilyID());
        lcContSchema.setPolType(tLCContSchema.getPolType());
        lcContSchema.setCardFlag(tLCContSchema.getCardFlag());
        lcContSchema.setManageCom(tLCContSchema.getManageCom());
        lcContSchema.setExecuteCom(tLCContSchema.getExecuteCom());
        lcContSchema.setAgentCom(tLCContSchema.getAgentCom());
        lcContSchema.setAgentCode(tLCContSchema.getAgentCode());
        lcContSchema.setAgentGroup(tLCContSchema.getAgentGroup());
        lcContSchema.setAgentCode1(tLCContSchema.getAgentCode1());
        lcContSchema.setAgentType(tLCContSchema.getAgentType());
        lcContSchema.setSaleChnl(tLCContSchema.getSaleChnl());
        lcContSchema.setHandler(tLCContSchema.getHandler());
        lcContSchema.setPassword(tLCContSchema.getPassword());
        lcContSchema.setAppntNo(tLCContSchema.getAppntNo());
        lcContSchema.setAppntName(tLCContSchema.getAppntName());
        lcContSchema.setAppntSex(tLCContSchema.getAppntSex());
        lcContSchema.setAppntBirthday(fDate.getDate(tLCContSchema.getAppntBirthday()));
        lcContSchema.setAppntIDType(tLCContSchema.getAppntIDType());
        lcContSchema.setAppntIDNo(tLCContSchema.getAppntIDNo());
        lcContSchema.setInsuredNo(tLCContSchema.getInsuredNo());
        lcContSchema.setInsuredName(tLCContSchema.getInsuredName());
        lcContSchema.setInsuredSex(tLCContSchema.getInsuredSex());
        lcContSchema.setInsuredBirthday(fDate.getDate(tLCContSchema.getInsuredBirthday()));
        lcContSchema.setInsuredIDType(tLCContSchema.getInsuredIDType());
        lcContSchema.setInsuredIDNo(tLCContSchema.getInsuredIDNo());
        lcContSchema.setPayIntv(tLCContSchema.getPayIntv());
        lcContSchema.setPayMode(tLCContSchema.getPayMode());
        lcContSchema.setPayLocation(tLCContSchema.getPayLocation());
        lcContSchema.setDisputedFlag(tLCContSchema.getDisputedFlag());
        lcContSchema.setOutPayFlag(tLCContSchema.getOutPayFlag());
        lcContSchema.setGetPolMode(tLCContSchema.getGetPolMode());
        lcContSchema.setSignCom(tLCContSchema.getSignCom());
        lcContSchema.setSignDate(fDate.getDate(tLCContSchema.getSignDate()));
        lcContSchema.setSignTime(tLCContSchema.getSignTime());
        lcContSchema.setConsignNo(tLCContSchema.getConsignNo());
        lcContSchema.setBankCode(tLCContSchema.getBankCode());
        lcContSchema.setBankAccNo(tLCContSchema.getBankAccNo());
        lcContSchema.setAccName(tLCContSchema.getAccName());
        lcContSchema.setPrintCount(tLCContSchema.getPrintCount());
        lcContSchema.setLostTimes(tLCContSchema.getLostTimes());
        lcContSchema.setLang(tLCContSchema.getLang());
        lcContSchema.setCurrency(tLCContSchema.getCurrency());
        lcContSchema.setRemark(tLCContSchema.getRemark());
        lcContSchema.setPeoples(tLCContSchema.getPeoples());
        lcContSchema.setMult(tLCContSchema.getMult());
        lcContSchema.setPrem(tLCContSchema.getPrem());
        lcContSchema.setAmnt(tLCContSchema.getAmnt());
        lcContSchema.setSumPrem(tLCContSchema.getSumPrem());
        lcContSchema.setDif(tLCContSchema.getDif());
        lcContSchema.setPaytoDate(fDate.getDate(tLCContSchema.getPaytoDate()));
        lcContSchema.setFirstPayDate(fDate.getDate(tLCContSchema.getFirstPayDate()));
        lcContSchema.setCValiDate(fDate.getDate(tLCContSchema.getCValiDate()));
        lcContSchema.setInputOperator(tLCContSchema.getInputOperator());
        lcContSchema.setInputDate(fDate.getDate(tLCContSchema.getInputDate()));
        lcContSchema.setInputTime(tLCContSchema.getInputTime());
        lcContSchema.setApproveFlag(tLCContSchema.getApproveFlag());
        lcContSchema.setApproveCode(tLCContSchema.getApproveCode());
        lcContSchema.setApproveDate(fDate.getDate(tLCContSchema.getApproveDate()));
        lcContSchema.setApproveTime(tLCContSchema.getApproveTime());
        lcContSchema.setUWFlag(tLCContSchema.getUWFlag());
        lcContSchema.setUWOperator(tLCContSchema.getUWOperator());
        lcContSchema.setUWDate(fDate.getDate(tLCContSchema.getUWDate()));
        lcContSchema.setUWTime(tLCContSchema.getUWTime());
        lcContSchema.setAppFlag(tLCContSchema.getAppFlag());
        lcContSchema.setPolApplyDate(fDate.getDate(tLCContSchema.getPolApplyDate()));
        lcContSchema.setGetPolDate(fDate.getDate(tLCContSchema.getGetPolDate()));
        lcContSchema.setGetPolTime(tLCContSchema.getGetPolTime());
        lcContSchema.setCustomGetPolDate(fDate.getDate(tLCContSchema.getCustomGetPolDate()));
        lcContSchema.setState(tLCContSchema.getState());
        lcContSchema.setOperator(tLCContSchema.getOperator());
        lcContSchema.setMakeDate(fDate.getDate(tLCContSchema.getMakeDate()));
        lcContSchema.setMakeTime(tLCContSchema.getMakeTime());
        lcContSchema.setModifyDate(fDate.getDate(tLCContSchema.getModifyDate()));
        lcContSchema.setModifyTime(tLCContSchema.getModifyTime());
        lcContSchema.setFirstTrialOperator(tLCContSchema.getFirstTrialOperator());
        lcContSchema.setFirstTrialDate(fDate.getDate(tLCContSchema.getFirstTrialDate()));
        lcContSchema.setFirstTrialTime(tLCContSchema.getFirstTrialTime());
        lcContSchema.setReceiveOperator(tLCContSchema.getReceiveOperator());
        lcContSchema.setReceiveDate(fDate.getDate(tLCContSchema.getReceiveDate()));
        lcContSchema.setReceiveTime(tLCContSchema.getReceiveTime());
        lcContSchema.setTempFeeNo(tLCContSchema.getTempFeeNo());
        lcContSchema.setSellType(tLCContSchema.getSellType());
        lcContSchema.setForceUWFlag(tLCContSchema.getForceUWFlag());
        lcContSchema.setForceUWReason(tLCContSchema.getForceUWReason());
        lcContSchema.setNewBankCode(tLCContSchema.getNewBankCode());
        lcContSchema.setNewBankAccNo(tLCContSchema.getNewBankAccNo());
        lcContSchema.setNewAccName(tLCContSchema.getNewAccName());
        lcContSchema.setNewPayMode(tLCContSchema.getNewPayMode());
        lcContSchema.setAgentBankCode(tLCContSchema.getAgentBankCode());
        lcContSchema.setBankAgent(tLCContSchema.getBankAgent());
      /*  lcContSchema.setAutoPayFlag(tLCContSchema.getAutoPayFlag());
        lcContSchema.setRnewFlag(tLCContSchema.getRnewFlag());
        lcContSchema.setFamilyContNo(tLCContSchema.getFamilyContNo());
        lcContSchema.setBussFlag(tLCContSchema.getBussFlag());
        lcContSchema.setSignName(tLCContSchema.getSignName());
        lcContSchema.setOrganizeDate(fDate.getDate(tLCContSchema.getOrganizeDate()));
        lcContSchema.setOrganizeTime(tLCContSchema.getOrganizeTime());
        lcContSchema.setNewAutoSendBankFlag(tLCContSchema.getNewAutoSendBankFlag());
        lcContSchema.setAgentCodeOper(tLCContSchema.getAgentCodeOper());
        lcContSchema.setAgentCodeAssi(tLCContSchema.getAgentCodeAssi());
        lcContSchema.setDelayReasonCode(tLCContSchema.getDelayReasonCode());
        lcContSchema.setDelayReasonDesc(tLCContSchema.getDelayReasonDesc());
        lcContSchema.setXQremindflag(tLCContSchema.getXQremindflag());
        lcContSchema.setOrganComCode(tLCContSchema.getOrganComCode());
        lcContSchema.setBankProvince(tLCContSchema.getBankProvince());
        lcContSchema.setNewBankProvince(tLCContSchema.getNewBankProvince());
        lcContSchema.setArbitrationCom(tLCContSchema.getArbitrationCom());
        lcContSchema.setOtherPrtno(tLCContSchema.getOtherPrtno());
        lcContSchema.setDestAccountDate(fDate.getDate(tLCContSchema.getDestAccountDate()));*/
        lcContSchema.setBankAgentName(tLCContSchema.getBankAgentName());
     /*   lcContSchema.setPrePayCount(tLCContSchema.getPrePayCount());
        lcContSchema.setNewBankCity(tLCContSchema.getNewBankCity());
        lcContSchema.setInsuredBankCode(tLCContSchema.getInsuredBankCode());
        lcContSchema.setInsuredBankAccNo(tLCContSchema.getInsuredBankAccNo());
        lcContSchema.setInsuredAccName(tLCContSchema.getInsuredAccName());
        lcContSchema.setInsuredBankProvince(tLCContSchema.getInsuredBankProvince());
        lcContSchema.setInsuredBankCity(tLCContSchema.getInsuredBankCity());
        lcContSchema.setNewAccType(tLCContSchema.getNewAccType());
        lcContSchema.setAccType(tLCContSchema.getAccType());
        lcContSchema.setBankCity(tLCContSchema.getBankCity());
        lcContSchema.setEstimateMoney(tLCContSchema.getEstimateMoney());
        lcContSchema.setInsuredAccType(tLCContSchema.getInsuredAccType());
        lcContSchema.setIsAllowedCharges(tLCContSchema.getIsAllowedCharges());
        lcContSchema.setWTEdorAcceptNo(tLCContSchema.getWTEdorAcceptNo());
        lcContSchema.setRecording(tLCContSchema.getRecording());*/
        lcContSchema.setSaleCom(tLCContSchema.getSaleCom());
      /*  lcContSchema.setSaleChannels(tLCContSchema.getSaleChannels());
        lcContSchema.setZJAgentCom(tLCContSchema.getZJAgentCom());
        lcContSchema.setZJAgentComName(tLCContSchema.getZJAgentComName());
        lcContSchema.setRecordFlag(tLCContSchema.getRecordFlag());
        lcContSchema.setPlanCode(tLCContSchema.getPlanCode());
        lcContSchema.setReceiptNo(tLCContSchema.getReceiptNo());
        lcContSchema.setReissueCont(tLCContSchema.getReissueCont());
        lcContSchema.setDomainCode(tLCContSchema.getDomainCode());
        lcContSchema.setDomainName(tLCContSchema.getDomainName());
        lcContSchema.setReferralNo(tLCContSchema.getReferralNo());*/

        return lcContSchema;
    };

    /**
     * 核保险种信息校验,准备核保算法
     * 输出：如果发生错误则返回false,否则返回true
     */
    private LMUWSet CheckKinds(LCContPojo tLCContSchema,LCPolPojo tLCPolSchema ,String contplanno) {
        String tsql = "";
        //modify by gq req-652   核保规则取值sql调整   2017/07/18  start
        tsql = "select * from lmuw where ((riskcode = '000000' and relapoltype ='B' and uwtype = '01') "
                + "or (riskcode = '" + tLCPolSchema.getRiskCode().trim() + "' and relapoltype = 'BI' "
                + "and uwcode not in (select value1 from ldwxconfig where codetype = 'unuwcode' and code1='"+tLCContSchema.getSellType()+"' and (code2='"+tLCPolSchema.getRiskCode().trim()+"' or code2 = '"+contplanno+"')) "//add by wong 2018/4/12 REQ-888 i云保升级
                + "and uwtype = '01')) and uwstyle in ('00', '" + tUWStyle + "') and salechnl = '" + tLCContSchema.getSaleChnl() + "' and selltype in ('00', '"+tLCContSchema.getSellType()+"') "
                + "and uwcode not in (select value1 from ldwxconfig where codetype = 'xbunuwcode' and code1='"+tLCContSchema.getSellType()+"' and (code2='"+tLCPolSchema.getRiskCode().trim()+"' or code2 ='"+contplanno+"' ) and '"+tLCPolSchema.getRnewFlag()+"' = '0' ) "//add by req-1271 向日葵续保排除规则 huangxin 20190123
                + "order by riskcode,uworder ";///modify by req-1021 微信暑期赠险需求 赠险放开职业校验huangxin 20180724
        //modify by gq req-652   核保规则取值sql调整   2017/07/18  end

        //   tsql ="select * from lmuw where riskcode='"+tLCPolSchema.getRiskCode().trim() +"'  and relapoltype = 'BI' and uwtype = '01' and salechnl = '" + tLCContSchema.getSaleChnl() + "' and selltype='00'";

        //对规则进行Redis操作





        LMUWDB tLMUWDB = new LMUWDB();
        LMUWSet tLMUWSet = tLMUWDB.executeQuery(tsql);
        if (tLMUWDB.mErrors.needDealError() == true) {
            CError.buildErr(this, tLCPolSchema.getRiskCode().trim() + "险种核保信息查询失败!");
            tLMUWSet.clear();
            return null;
        }
        return tLMUWSet;
    }

    /**
     * 个人单核保数据准备
     * 输出：如果发生错误则返回false,否则返回true
     */
    private void CheckPolInit(LCPolPojo tLCPolSchema,LCContPojo lcContPojo,LCInsuredPojo lcInsuredPojo,List<LCDutyPojo> lcDutyPojoList) {
        mCalBase.setDutyCodeString(dutyCodeString);
        mCalBase.setDutyCode(lcDutyPojoList.get(0).getDutyCode());
        mCalBase.setPrem(tLCPolSchema.getPrem());
        mCalBase.setGet(tLCPolSchema.getAmnt());
        mCalBase.setMult(tLCPolSchema.getMult());
        mCalBase.setPayYears(tLCPolSchema.getPayYears());
        mCalBase.setPayIntv(tLCPolSchema.getPayIntv());//add by wangzy req-434 传入payintv参数 20170126
        mCalBase.setTAPPFlag(tLCPolSchema.getAppFlag());
        mCalBase.setTUWFlag(tLCPolSchema.getUWFlag());
        mCalBase.setInsuredIdno(lcContPojo.getInsuredIDNo());
        mCalBase.setAppntAge(PubFun.calInterval(fDate.getDate(lcContPojo.getAppntBirthday()), fDate.getDate(tLCPolSchema.getCValiDate()), "Y"));
        mCalBase.setAppntSex(lcContPojo.getAppntSex());
        mCalBase.setAppAge(tLCPolSchema.getInsuredAppAge());
        mCalBase.setSex(tLCPolSchema.getInsuredSex());
        mCalBase.setJob(tLCPolSchema.getOccupationType());
        mCalBase.setCount(tLCPolSchema.getInsuredPeoples());
        mCalBase.setPolNo(tLCPolSchema.getPolNo());
        mCalBase.setRnewFlag(tLCPolSchema.getRnewFlag());
        mCalBase.setContNo(mContNo);
        //      mCalBase.setLoanContNo(tLCPolSchema.getLoanContNo());
        mCalBase.setCValiDate(tLCPolSchema.getCValiDate());
        mCalBase.setInsuYear(tLCPolSchema.getInsuYear());
        mCalBase.setPayEndYear(tLCPolSchema.getPayEndYear());
        mCalBase.setInsuredNo(tLCPolSchema.getInsuredNo());
        mCalBase.setRelationToInsured(lcAppntPojo.getRelatToInsu());
        mCalBase.setOccupationType(lcInsuredPojo.getOccupationType());
        mCalBase.setOccupationCode(lcInsuredPojo.getOccupationCode());

        mCalBase.setContPlanCode(lcInsuredPojo.getContPlanCode());
        mCalBase.setAvoirdupois(lcInsuredPojo.getAvoirdupois());
        mCalBase.setStature(lcInsuredPojo.getStature());
        mCalBase.setInsureIDexpDate(lcInsuredPojo.getIdValiDate());
        mCalBase.setInsuredSalary(lcInsuredPojo.getSalary());
        mCalBase.setInsureSSFlag(lcInsuredPojo.getSSFlag());
        mCalBase.setInsuredBirthday(lcInsuredPojo.getBirthday());
        mCalBase.setInsureNativePlace(lcInsuredPojo.getNativePlace());
        mCalBase.setInsuredOccupationCode(lcInsuredPojo.getOccupationCode());
        mCalBase.setAppntBirthday(lcContPojo.getAppntBirthday());
        String polApplyDate = lcContPojo.getPolApplyDate();
        if(polApplyDate!=null&&!"".equals(polApplyDate)){
            polApplyDate=polApplyDate.substring(0,10);
        }
        mCalBase.setPolApplyDate(polApplyDate);
        mCalBase.setSaleChnl(lcContPojo.getSaleChnl());
        mCalBase.setSellType(lcContPojo.getSellType());
        mCalBase.setImpartVer(ImpartVer);
        mCalBase.setImpartCode(ImpartCode);
        mCalBase.setImpartparammodle(Impartparammodle);
        String address="";
        String appntPhone="";
        String appntHomePhone="";
        String appntProvince="";
        String appntCity="";

        String appntNo = lcContPojo.getAppntNo();
        String  addressNo=lcAppntPojo.getAddressNo();
        mCalBase.setMainPolNo(tLCPolSchema.getMainPolNo());

        GetAllSumAmnt(tLCPolSchema,lcDutyPojoList); //得到以下各种累计风险保额。

        mCalBase.setFxml_1(PubFun.setPrecisionString(Fxml_1,"0.00"));
        mCalBase.setFxml_2(PubFun.setPrecisionString(Fxml_2,"0.00"));
        mCalBase.setFxml_4(PubFun.setPrecisionString(Fxml_4,"0.00"));
        mCalBase.setFxml_12(PubFun.setPrecisionString(Fxml_12,"0.00"));
        mCalBase.setFxml_41(PubFun.setPrecisionString(Fxml_41,"0.00"));
        mCalBase.setFxml_43(PubFun.setPrecisionString(Fxml_43,"0.00"));

        mCalBase.setLSumDangerAmnt(String.valueOf(LSumDangerAmnt)); //同一被保险人下寿险类的累计危险保额
        mCalBase.setDSumDangerAmnt(String.valueOf(DSumDangerAmnt)); //同一被保险人下重大疾病类的累计危险保额
        mCalBase.setMSumDangerAmnt(String.valueOf(MSumDangerAmnt)); //同一被保险人下人身意外类的累计危险保额
        mCalBase.setSSumDangerAmnt(String.valueOf(SSumDangerAmnt)); //同一被保险人下人身医疗类的累计危险保额
        mCalBase.setSSumDieAmnt(String.valueOf(SSumDieAmnt)); //同一被保险人身故的累计危险保额
        mCalBase.setAllSumAmnt(String.valueOf(AllSumAmnt)); //同一被保险人人身险的累计危险保额
        mCalBase.setImpartAmnt("0.00");//add by gq req-543 告知信息身故保险金额参数添加

        mCalBase.setAppntLSumDangerAmnt(String.valueOf(AppntLSumDangerAmnt));
        mCalBase.setAppntDSumDangerAmnt(String.valueOf(AppntDSumDangerAmnt));
        mCalBase.setAppntMSumDangerAmnt(String.valueOf(AppntMSumDangerAmnt));
        mCalBase.setAppntSSumDangerAmnt(String.valueOf(AppntSSumDangerAmnt));
        mCalBase.setAppntSSumDieAmnt(String.valueOf(AppntSSumDieAmnt));
        mCalBase.setAppntAllSumAmnt(String.valueOf(AppntAllSumAmnt));


        //add by likai 2013-09-24  被保人临分风险保额
        mCalBase.setmLFSumDangerAmnt1(PubFun.setPrecisionString(mLFSumDangerAmnt1,"0.00"));
        mCalBase.setmLFSumDangerAmnt2(PubFun.setPrecisionString(mLFSumDangerAmnt2,"0.00"));
        mCalBase.setmLFSumDangerAmnt4(PubFun.setPrecisionString(mLFSumDangerAmnt4,"0.00"));
        mCalBase.setmLFSumDangerAmnt12(PubFun.setPrecisionString(mLFSumDangerAmnt12,"0.00"));
        mCalBase.setmLFSumDangerAmnt41(PubFun.setPrecisionString(mLFSumDangerAmnt41,"0.00"));
        mCalBase.setmLFSumDangerAmnt42(PubFun.setPrecisionString(mLFSumDangerAmnt42,"0.00"));
        mCalBase.setAppntNo(tLCPolSchema.getAppntNo());

        mCalBase.setManageCom(tLCPolSchema.getManageCom());
        mCalBase.setAppntJob(lcAppntPojo.getOccupationType()); //投保人职业类别
        mCalBase.setRiskCode(tLCPolSchema.getRiskCode());
        //add by shaozq REQ-673 风险保额区分保障和储蓄类 20170906 shaozq start
        mCalBase.setmLFSumDangerAmntBZ1(PubFun.setPrecisionString(mLFSumDangerAmntBZ1,"0.00"));
        mCalBase.setmLFSumDangerAmntBZ2(PubFun.setPrecisionString(mLFSumDangerAmntBZ2,"0.00"));
        mCalBase.setmLFSumDangerAmntBZ4(PubFun.setPrecisionString(mLFSumDangerAmntBZ4,"0.00"));
        mCalBase.setmLFSumDangerAmntBZ12(PubFun.setPrecisionString(mLFSumDangerAmntBZ12,"0.00"));
        mCalBase.setmLFSumDangerAmntBZ41(PubFun.setPrecisionString(mLFSumDangerAmntBZ41,"0.00"));
        mCalBase.setmLFSumDangerAmntBZ42(PubFun.setPrecisionString(mLFSumDangerAmntBZ42,"0.00"));
        mCalBase.setmLFSumDangerAmntCX1(PubFun.setPrecisionString(mLFSumDangerAmntCX1,"0.00"));
        mCalBase.setmLFSumDangerAmntCX2(PubFun.setPrecisionString(mLFSumDangerAmntCX2,"0.00"));
        mCalBase.setmLFSumDangerAmntCX4(PubFun.setPrecisionString(mLFSumDangerAmntCX4,"0.00"));
        mCalBase.setmLFSumDangerAmntCX12(PubFun.setPrecisionString(mLFSumDangerAmntCX12,"0.00"));
        mCalBase.setmLFSumDangerAmntCX41(PubFun.setPrecisionString(mLFSumDangerAmntCX41,"0.00"));
        mCalBase.setmLFSumDangerAmntCX42(PubFun.setPrecisionString(mLFSumDangerAmntCX42,"0.00"));
        ExeSQL ExeSql = new ExeSQL();
        String tFlag1 = ExeSql.getOneValue("select case when (select 1 from dual where exists (select 1 from lmcalmode  where riskcode in (select riskcode from lcpol where contno = '"+tLCPolSchema.getContNo()+"') and type = '1')) = 1 then 1 else 0 end from dual");
        String tFlag2 = ExeSql.getOneValue("select case when (select 1 from dual where exists (select 1 from lmcalmode  where riskcode in (select riskcode from lcpol where contno = '"+tLCPolSchema.getContNo()+"') and type = '2')) = 1 then 2 else 0 end from dual");
        String tFlag4 = ExeSql.getOneValue("select case when (select 1 from dual where exists (select 1 from lmcalmode  where riskcode in (select riskcode from lcpol where contno = '"+tLCPolSchema.getContNo()+"') and type = '4')) = 1 then 4 else 0 end from dual");
        String tFlag12 = ExeSql.getOneValue("select case when (select 1 from dual where exists (select 1 from lmcalmode  where riskcode in (select riskcode from lcpol where contno = '"+tLCPolSchema.getContNo()+"') and type = '12')) = 1 then 12 else 0 end from dual");
        mCalBase.setmFlag1(tFlag1);
        mCalBase.setmFlag2(tFlag2);
        mCalBase.setmFlag4(tFlag4);
        mCalBase.setmFlag12(tFlag12);
        //add by shaozq 20170825 REQ-673 校验是否存在风险保额 end
        //add by shaozq REQ-673 风险保额区分保障和储蓄类 20170906 shaozq end
        String polNo = tLCPolSchema.getPolNo();
        for(int i=0;i<lcDutyPojoList.size();i++){
            LCDutyPojo lcDutyPojo = lcDutyPojoList.get(i);
            String polNoDuty = lcDutyPojo.getPolNo();
            String dutyCode = lcDutyPojo.getDutyCode();
            //意外身故保额
            if(polNo.equals(polNoDuty)&&"ID0072".equals(dutyCode)){
                mCalBase.setYWshengu(PubFun.setPrecisionString(lcDutyPojo.getAmnt(),"0.00"));
            }
            //意外医疗保额
            if(polNo.equals(polNoDuty)&&"ID0073".equals(dutyCode)){
                mCalBase.setYWyiliao(PubFun.setPrecisionString(lcDutyPojo.getAmnt(),"0.00"));
            }
            //意外津贴
            if(polNo.equals(polNoDuty)&&"ID0074".equals(dutyCode)){
                mCalBase.setYWjingtie(PubFun.setPrecisionString(lcDutyPojo.getAmnt(),"0.00"));
            }

            //含驾驶
            if(polNo.equals(polNoDuty)&&"ID0131".equals(dutyCode)){
                mCalBase.setHaveJiaShi(PubFun.setPrecisionString(lcDutyPojo.getAmnt(),"0.00"));
            }

            //不含驾驶
            if(polNo.equals(polNoDuty)&&"ID0130".equals(dutyCode)){
                mCalBase.setNotHaveJiaShi(PubFun.setPrecisionString(lcDutyPojo.getAmnt(),"0.00"));
            }


        }


    }

    private void GetAllSumAmnt(LCPolPojo tLCPolSchema,List<LCDutyPojo> lcDutyPojoList) {
        // //为自核服务 write by yaory///////
        String riskcode = tLCPolSchema.getRiskCode();
        String tsql="";
        String calcode="";
        try {
            logger.debug(mContNo);
            ExeSQL riskSql = new ExeSQL();
            tsql = "select calcode from  lmcalmode where riskcode='"+riskcode+"' and type='1'";
            calcode = riskSql.getOneValue(tsql);
            if(!StringUtils.isEmpty(calcode)){
                String calculate= getpolFX(calcode,tLCPolSchema,lcDutyPojoList);
                if(!StringUtils.isEmpty(calculate)){
                    Fxml_1=parseFloat(calculate);
                }else{
                    Fxml_1=parseFloat(0+"");
                }
            }else{
                Fxml_1=parseFloat(0+"");
            }

            tsql = "select calcode  from  lmcalmode where riskcode='"+riskcode+"' and type='2'";
            calcode = riskSql.getOneValue(tsql);
            if(!StringUtils.isEmpty(calcode)){
                String calculate= getpolFX(calcode,tLCPolSchema,lcDutyPojoList);
                if(!StringUtils.isEmpty(calculate)){
                    Fxml_2=parseFloat(calculate);
                }else{
                    Fxml_2=parseFloat(0+"");
                }
            }else{
                Fxml_2=parseFloat(0+"");
            }


            tsql = "select calcode from  lmcalmode where riskcode='"+riskcode+"' and type='4'";
            calcode = riskSql.getOneValue(tsql);
            if(!StringUtils.isEmpty(calcode)){
                String calculate= getpolFX(calcode,tLCPolSchema,lcDutyPojoList);
                if(!StringUtils.isEmpty(calculate)){
                    Fxml_4=parseFloat(calculate);
                }else{
                    Fxml_4=parseFloat(0+"");
                }
            }else{
                Fxml_4=parseFloat(0+"");
            }

            tsql = "select calcode from  lmcalmode where riskcode='"+riskcode+"' and type='12'";

            calcode = riskSql.getOneValue(tsql);
            if(!StringUtils.isEmpty(calcode)){
                String calculate= getpolFX(calcode,tLCPolSchema,lcDutyPojoList);
                if(!StringUtils.isEmpty(calculate)){
                    Fxml_12=parseFloat(calculate);
                }else{
                    Fxml_12=parseFloat(0+"");
                }
            }else{
                Fxml_12=parseFloat(0+"");
            }

            tsql = "select calcode from  lmcalmode where riskcode='"+riskcode+"' and type='41'";

            calcode = riskSql.getOneValue(tsql);
            if(!StringUtils.isEmpty(calcode)){
                String calculate= getpolFX(calcode,tLCPolSchema,lcDutyPojoList);
                if(!StringUtils.isEmpty(calculate)){
                    Fxml_41=parseFloat(calculate);
                }else{
                    Fxml_41=parseFloat(0+"");
                }
            }else{
                Fxml_41=parseFloat(0+"");
            }


            tsql = "select calcode from  lmcalmode where riskcode='"+riskcode+"' and type='43'";

            calcode = riskSql.getOneValue(tsql);
            if(!StringUtils.isEmpty(calcode)){
                String calculate= getpolFX(calcode,tLCPolSchema,lcDutyPojoList);
                if(!StringUtils.isEmpty(calculate)){
                    Fxml_43=parseFloat(calculate);
                }else{
                    Fxml_43=parseFloat(0+"");
                }
            }else{
                Fxml_43=parseFloat(0+"");
            }

            //tongmeng 2009-03-12 modify
            //使用新的自核风险保额计算规则

            //寿险风险保额
			/*
			   -- tRiskType = 1 寿险风险保额
			   -- tRiskType = 2 重疾险风险保额
               -- tRiskType = 3 医疗险风险保额
               -- tRiskType = 4 意外险风险保额
               -- tRiskType = 12 人身险风险保额
               -- tRiskType = 13 寿险体检额度
               -- tRiskType = 14 重疾体检额度
               -- tRiskType = 15 医疗体检额度

			 */
         /*   tsql = "select healthyamnt2('" + tInsuredNo
                    + "','1','1') from dual";
            String tempAmnt = riskSql.getOneValue(tsql);
            LSumDangerAmnt = parseFloat(tempAmnt);//寿险

            tsql = "select healthyamnt2('" + tInsuredNo
                    + "','2','1') from dual";
            tempAmnt = riskSql.getOneValue(tsql);
            DSumDangerAmnt = parseFloat(tempAmnt); //重疾

            tsql = "select healthyamnt2('" + tInsuredNo
                    + "','3','1') from dual";
            tempAmnt = riskSql.getOneValue(tsql);
            SSumDangerAmnt = parseFloat(tempAmnt); //医疗

            tsql = "select healthyamnt2('" + tInsuredNo
                    + "','4','1') from dual";
            tempAmnt = riskSql.getOneValue(tsql);
            MSumDangerAmnt = parseFloat(tempAmnt); //意外
            tsql = "select healthyamnt2('" + tInsuredNo
                    + "','12','1') from dual";
            tempAmnt = riskSql.getOneValue(tsql);
            SSumDieAmnt = parseFloat(tempAmnt); //身故(人身险风险保额)
            AllSumAmnt=LSumDangerAmnt+DSumDangerAmnt+MSumDangerAmnt;//2009-3-5 modify累计风险保额=累计寿险风险保额+累计重疾风险保额+累计意外伤害风险保额

            //add by shaozq REQ-673 风险保额区分储蓄和保障类 20170906 start
            // 寿险临分风险保额(保障类)
            tsql = "select healthyamnt6('" + tInsuredNo
                    + "','1','3') from dual";
            tempAmnt = riskSql.getOneValue(tsql);
            mLFSumDangerAmntBZ1 = parseFloat(tempAmnt);
            // 重疾临分风险保额(保障类)
            tsql = "select healthyamnt6('" + tInsuredNo
                    + "','2','3') from dual";
            tempAmnt = riskSql.getOneValue(tsql);
            mLFSumDangerAmntBZ2 = parseFloat(tempAmnt);
            // 意外临分风险保额(保障类)
            tsql = "select healthyamnt6('" + tInsuredNo
                    + "','4','3') from dual";
            tempAmnt = riskSql.getOneValue(tsql);
            mLFSumDangerAmntBZ4 = parseFloat(tempAmnt);
            //人身临分风险保额(保障类)
            tsql = "select healthyamnt6('" + tInsuredNo
                    + "','12','3') from dual";
            tempAmnt = riskSql.getOneValue(tsql);
            mLFSumDangerAmntBZ12 = parseFloat(tempAmnt);
            //交通意外临分风险保额(保障类)
            tsql = "select healthyamnt6('" + tInsuredNo
                    + "','41','3') from dual";
            tempAmnt = riskSql.getOneValue(tsql);
            mLFSumDangerAmntBZ41 = parseFloat(tempAmnt);
            // 航空意外临分风险保额(保障类)
            tsql = "select healthyamnt6('" + tInsuredNo
                    + "','42','3') from dual";
            tempAmnt = riskSql.getOneValue(tsql);
            mLFSumDangerAmntBZ42 = parseFloat(tempAmnt);

            // 寿险临分风险保额(储蓄类)
            tsql = "select healthyamnt7('" + tInsuredNo
                    + "','1','3') from dual";
            tempAmnt = riskSql.getOneValue(tsql);
            mLFSumDangerAmntCX1 = parseFloat(tempAmnt);
            // 重疾临分风险保额(储蓄类)
            tsql = "select healthyamnt7('" + tInsuredNo
                    + "','2','3') from dual";
            tempAmnt = riskSql.getOneValue(tsql);
            mLFSumDangerAmntCX2 = parseFloat(tempAmnt);
            // 意外临分风险保额(储蓄类)
            tsql = "select healthyamnt7('" + tInsuredNo
                    + "','4','3') from dual";
            tempAmnt = riskSql.getOneValue(tsql);
            mLFSumDangerAmntCX4 = parseFloat(tempAmnt);
            //人身临分风险保额(储蓄类)
            tsql = "select healthyamnt7('" + tInsuredNo
                    + "','12','3') from dual";
            tempAmnt = riskSql.getOneValue(tsql);
            mLFSumDangerAmntCX12 = parseFloat(tempAmnt);
            //交通意外临分风险保额(储蓄类)
            tsql = "select healthyamnt7('" + tInsuredNo
                    + "','41','3') from dual";
            tempAmnt = riskSql.getOneValue(tsql);
            mLFSumDangerAmntCX41 = parseFloat(tempAmnt);
            // 航空意外临分风险保额(储蓄类)
            tsql = "select healthyamnt7('" + tInsuredNo
                    + "','42','3') from dual";
            tempAmnt = riskSql.getOneValue(tsql);
            mLFSumDangerAmntCX42 = parseFloat(tempAmnt);*/

            //add by shaozq REQ-673 风险保额区分储蓄和保障类 201709061 end


        } catch (Exception ex) {
        }
    }


    private String getpolFX(String calCode,LCPolPojo tLCPolSchema,List<LCDutyPojo> lcDutyPojoList){
        String polNo = tLCPolSchema.getPolNo();
        double dutyAmnt=0;
        double dutyAmnt2=0;
        for (int i=0;i<lcDutyPojoList.size();i++){
            LCDutyPojo lcDutyPojo = lcDutyPojoList.get(i);
            if(polNo.equals(lcDutyPojo.getPolNo())&&"ID0072".equals(lcDutyPojo.getDutyCode())){
                dutyAmnt= lcDutyPojo.getAmnt();
            }
            if(polNo.equals(lcDutyPojo.getPolNo())&&"ID0077".equals(lcDutyPojo.getDutyCode())){
                dutyAmnt2= lcDutyPojo.getAmnt();
            }
        }
        Calculator mCalculator = SpringContextUtils.getBeanByClass(Calculator.class);
        mCalculator.setCalCode(calCode);
        mCalculator.addBasicFactor("riskcode",tLCPolSchema.getRiskCode() );
        mCalculator.addBasicFactor("uwFlag",tLCPolSchema.getUWFlag() );
        mCalculator.addBasicFactor("appFlag",tLCPolSchema.getAppFlag() );
        mCalculator.addBasicFactor("amnt",String.valueOf(tLCPolSchema.getAmnt()));
        mCalculator.addBasicFactor("dutyAmnt",String.valueOf(dutyAmnt));
        mCalculator.addBasicFactor("dutyAmnt2",String.valueOf(dutyAmnt2));
        mCalculator.addBasicFactor("payEndYear",String.valueOf(tLCPolSchema.getPayEndYear()));
        mCalculator.addBasicFactor("insuredAppAge",String.valueOf(tLCPolSchema.getInsuredAppAge()));
        mCalculator.addBasicFactor("prem",String.valueOf(tLCPolSchema.getPrem()));



        return mCalculator.calculate();
    }



    private void GetAllSumAmntAppnt(List<LCPolPojo> lcPolPojoList,List <LCDutyPojo> lcDutyPojoList) {
        try {

            ExeSQL riskSql = new ExeSQL();
            String tsql="";
            String calcode="";
            logger.info("============保单层本单累计风险保额计算,险种总个数:"+lcPolPojoList.size());
            for(int i=0;i<lcPolPojoList.size();i++){
                LCPolPojo tLCPolSchema = lcPolPojoList.get(i);
                String riskcode = tLCPolSchema.getRiskCode();
                tsql = "select calcode from  lmcalmode where riskcode='"+riskcode+"' and type='1'";
                calcode = riskSql.getOneValue(tsql);
                if(!StringUtils.isEmpty(calcode)){
                    String calculate= getpolFX(calcode,tLCPolSchema,lcDutyPojoList);
                    if(!StringUtils.isEmpty(calculate)){
                        Fxml_1=parseFloat(calculate);
                    }else{
                        Fxml_1=parseFloat(0+"");
                    }
                }else{
                    Fxml_1=parseFloat(0+"");
                }
                SumContFxml_1=SumContFxml_1+ Fxml_1;

                tsql = "select calcode from  lmcalmode where riskcode='"+riskcode+"' and type='2'";
                calcode = riskSql.getOneValue(tsql);
                if(!StringUtils.isEmpty(calcode)){
                    String calculate= getpolFX(calcode,tLCPolSchema,lcDutyPojoList);
                    if(!StringUtils.isEmpty(calculate)){
                        Fxml_2=parseFloat(calculate);
                    }else{
                        Fxml_2=parseFloat(0+"");
                    }
                }else{
                    Fxml_2=parseFloat(0+"");
                }
                SumContFxml_2=SumContFxml_2+ Fxml_2;

                tsql = "select calcode from  lmcalmode where riskcode='"+riskcode+"' and type='4'";
                calcode = riskSql.getOneValue(tsql);
                if(!StringUtils.isEmpty(calcode)){
                    String calculate= getpolFX(calcode,tLCPolSchema,lcDutyPojoList);
                    if(!StringUtils.isEmpty(calculate)){
                        Fxml_4=parseFloat(calculate);
                    }else{
                        Fxml_4=parseFloat(0+"");
                    }
                }else{
                    Fxml_4=parseFloat(0+"");
                }
                SumContFxml_4=SumContFxml_4+ Fxml_4;

                tsql = "select calcode from  lmcalmode where riskcode='"+riskcode+"' and type='12'";
                calcode = riskSql.getOneValue(tsql);
                if(!StringUtils.isEmpty(calcode)){
                    String calculate= getpolFX(calcode,tLCPolSchema,lcDutyPojoList);
                    if(!StringUtils.isEmpty(calculate)){
                        Fxml_12=parseFloat(calculate);
                    }else{
                        Fxml_12=parseFloat(0+"");
                    }
                }else{
                    Fxml_12=parseFloat(0+"");
                }
                SumContFxml_12=SumContFxml_12+ Fxml_12;

                tsql = "select calcode from  lmcalmode where riskcode='"+riskcode+"' and type='41'";
                calcode = riskSql.getOneValue(tsql);
                if(!StringUtils.isEmpty(calcode)){
                    String calculate= getpolFX(calcode,tLCPolSchema,lcDutyPojoList);
                    if(!StringUtils.isEmpty(calculate)){
                        Fxml_41=parseFloat(calculate);
                    }else{
                        Fxml_41=parseFloat(0+"");
                    }
                }else{
                    Fxml_41=parseFloat(0+"");
                }
                SumContFxml_41=SumContFxml_41+ Fxml_41;



                tsql = "select calcode from  lmcalmode where riskcode='"+riskcode+"' and type='43'";
                calcode = riskSql.getOneValue(tsql);
                if(!StringUtils.isEmpty(calcode)){
                    String calculate= getpolFX(calcode,tLCPolSchema,lcDutyPojoList);
                    if(!StringUtils.isEmpty(calculate)){
                        Fxml_43=parseFloat(calculate);
                    }else{
                        Fxml_43=parseFloat(0+"");
                    }
                }else{
                    Fxml_43=parseFloat(0+"");
                }
                SumContFxml_43=SumContFxml_43+ Fxml_43;
            }

            //tongmeng 2009-03-12 modify
            //使用新的自核风险保额计算规则
            //   String tsql = "";
            //寿险风险保额
			/*
			   -- tRiskType = 1 寿险风险保额
			   -- tRiskType = 2 重疾险风险保额
             -- tRiskType = 3 医疗险风险保额
             -- tRiskType = 4 意外险风险保额
             -- tRiskType = 12 人身险风险保额
             -- tRiskType = 13 寿险体检额度
             -- tRiskType = 14 重疾体检额度
             -- tRiskType = 15 医疗体检额度

			 */
           /* tsql = "select healthyamnt2('" + tAppntNo
                    + "','1','1') from dual";
            String tempAmnt = riskSql.getOneValue(tsql);
            AppntLSumDangerAmnt = parseFloat(tempAmnt);//寿险

            tsql = "select healthyamnt2('" + tAppntNo
                    + "','2','1') from dual";
            tempAmnt = riskSql.getOneValue(tsql);
            AppntDSumDangerAmnt = parseFloat(tempAmnt); //重疾

            tsql = "select healthyamnt2('" + tAppntNo
                    + "','3','1') from dual";
            tempAmnt = riskSql.getOneValue(tsql);
            AppntSSumDangerAmnt = parseFloat(tempAmnt); //医疗

            tsql = "select healthyamnt2('" + tAppntNo
                    + "','4','1') from dual";
            tempAmnt = riskSql.getOneValue(tsql);
            AppntMSumDangerAmnt = parseFloat(tempAmnt); //意外
            tsql = "select healthyamnt2('" + tAppntNo
                    + "','12','1') from dual";
            tempAmnt = riskSql.getOneValue(tsql);
            AppntSSumDieAmnt = parseFloat(tempAmnt); //身故(人身险风险保额)
            AppntAllSumAmnt=AppntLSumDangerAmnt+AppntDSumDangerAmnt+AppntMSumDangerAmnt;//2009-3-5 modify累计风险保额=累计寿险风险保额+累计重疾风险保额+累计意外伤害风险保额
*/
        } catch (Exception ex) {
        }
    }

    /**
     *
     * @comment: 被保人临分风险保额计算
     * @author likai
     * @history: 2013-9-24 likai
     *
     */
    private void GetAllSumAmntLF(String tInsuredNo) {
        try {
            logger.debug("CONTNO:" + mContNo + "，InsuredNo：" + tInsuredNo);
            ExeSQL riskSql = new ExeSQL();
            String tsql = "";
            // 寿险临分风险保额
            tsql = "select healthyamnt2('" + tInsuredNo
                    + "','1','3') from dual";
            String tempAmnt = riskSql.getOneValue(tsql);
            mLFSumDangerAmnt1 = parseFloat(tempAmnt);
            // 重疾临分风险保额
            tsql = "select healthyamnt2('" + tInsuredNo
                    + "','2','3') from dual";
            tempAmnt = riskSql.getOneValue(tsql);
            mLFSumDangerAmnt2 = parseFloat(tempAmnt);
            // 意外临分风险保额
            tsql = "select healthyamnt2('" + tInsuredNo
                    + "','4','3') from dual";
            tempAmnt = riskSql.getOneValue(tsql);
            mLFSumDangerAmnt4 = parseFloat(tempAmnt);
            //人身临分风险保额
            tsql = "select healthyamnt2('" + tInsuredNo
                    + "','12','3') from dual";
            tempAmnt = riskSql.getOneValue(tsql);
            mLFSumDangerAmnt12 = parseFloat(tempAmnt);
            //交通意外临分风险保额
            tsql = "select healthyamnt2('" + tInsuredNo
                    + "','41','3') from dual";
            tempAmnt = riskSql.getOneValue(tsql);
            mLFSumDangerAmnt41 = parseFloat(tempAmnt);
            // 航空意外临分风险保额
            tsql = "select healthyamnt2('" + tInsuredNo
                    + "','42','3') from dual";
            tempAmnt = riskSql.getOneValue(tsql);
            mLFSumDangerAmnt42 = parseFloat(tempAmnt);



            //add by shaozq REQ-673 风险保额区分储蓄和保障类 20170906 start
            // 寿险临分风险保额(保障类)
            tsql = "select healthyamnt6('" + tInsuredNo
                    + "','1','3') from dual";
            tempAmnt = riskSql.getOneValue(tsql);
            mLFSumDangerAmntBZ1 = parseFloat(tempAmnt);
            // 重疾临分风险保额(保障类)
            tsql = "select healthyamnt6('" + tInsuredNo
                    + "','2','3') from dual";
            tempAmnt = riskSql.getOneValue(tsql);
            mLFSumDangerAmntBZ2 = parseFloat(tempAmnt);
            // 意外临分风险保额(保障类)
            tsql = "select healthyamnt6('" + tInsuredNo
                    + "','4','3') from dual";
            tempAmnt = riskSql.getOneValue(tsql);
            mLFSumDangerAmntBZ4 = parseFloat(tempAmnt);
            //人身临分风险保额(保障类)
            tsql = "select healthyamnt6('" + tInsuredNo
                    + "','12','3') from dual";
            tempAmnt = riskSql.getOneValue(tsql);
            mLFSumDangerAmntBZ12 = parseFloat(tempAmnt);
            //交通意外临分风险保额(保障类)
            tsql = "select healthyamnt6('" + tInsuredNo
                    + "','41','3') from dual";
            tempAmnt = riskSql.getOneValue(tsql);
            mLFSumDangerAmntBZ41 = parseFloat(tempAmnt);
            // 航空意外临分风险保额(保障类)
            tsql = "select healthyamnt6('" + tInsuredNo
                    + "','42','3') from dual";
            tempAmnt = riskSql.getOneValue(tsql);
            mLFSumDangerAmntBZ42 = parseFloat(tempAmnt);

            // 寿险临分风险保额(储蓄类)
            tsql = "select healthyamnt7('" + tInsuredNo
                    + "','1','3') from dual";
            tempAmnt = riskSql.getOneValue(tsql);
            mLFSumDangerAmntCX1 = parseFloat(tempAmnt);
            // 重疾临分风险保额(储蓄类)
            tsql = "select healthyamnt7('" + tInsuredNo
                    + "','2','3') from dual";
            tempAmnt = riskSql.getOneValue(tsql);
            mLFSumDangerAmntCX2 = parseFloat(tempAmnt);
            // 意外临分风险保额(储蓄类)
            tsql = "select healthyamnt7('" + tInsuredNo
                    + "','4','3') from dual";
            tempAmnt = riskSql.getOneValue(tsql);
            mLFSumDangerAmntCX4 = parseFloat(tempAmnt);
            //人身临分风险保额(储蓄类)
            tsql = "select healthyamnt7('" + tInsuredNo
                    + "','12','3') from dual";
            tempAmnt = riskSql.getOneValue(tsql);
            mLFSumDangerAmntCX12 = parseFloat(tempAmnt);
            //交通意外临分风险保额(储蓄类)
            tsql = "select healthyamnt7('" + tInsuredNo
                    + "','41','3') from dual";
            tempAmnt = riskSql.getOneValue(tsql);
            mLFSumDangerAmntCX41 = parseFloat(tempAmnt);
            // 航空意外临分风险保额(储蓄类)
            tsql = "select healthyamnt7('" + tInsuredNo
                    + "','42','3') from dual";
            tempAmnt = riskSql.getOneValue(tsql);
            mLFSumDangerAmntCX42 = parseFloat(tempAmnt);

            //add by shaozq REQ-673 风险保额区分储蓄和保障类 20170906 end



        } catch (Exception ex) {
        }
    }

    /**
     * 对险种级自核提示信息进行解析，将描述的参数替换为具体的值
     * 所用参数和算法中的一致
     * */
    private void parsePolUWResult(LMUWSchema cLMUWSchema,String cRiskCode){
        String uwResult = cLMUWSchema.getRemark();
        uwResult = uwResult.replaceAll("#Get#", mCalBase.getGet());
        uwResult = uwResult.replaceAll("#Mult#", mCalBase.getMult());
        uwResult = uwResult.replaceAll("#Prem#", mCalBase.getPrem());
        uwResult = uwResult.replaceAll("#AppAge#", mCalBase.getAppAge());
        uwResult = uwResult.replaceAll("#AppntAge#", String.valueOf(mCalBase.getAppntAge()));
        uwResult = uwResult.replaceAll("#AppntSex#", mCalBase.getAppntSex());
        uwResult = uwResult.replaceAll("#Sex#", mCalBase.getSex());
        uwResult = uwResult.replaceAll("#Job#", mCalBase.getJob());
        uwResult = uwResult.replaceAll("#PayEndYear#", mCalBase.getPayEndYear());
        uwResult = uwResult.replaceAll("#GetStartDate#", "");
        uwResult = uwResult.replaceAll("#Years#", mCalBase.getYears());
        //uwResult = uwResult.replaceAll("#Grp#", "");
        //uwResult = uwResult.replaceAll("#GetFlag#", "");
        uwResult = uwResult.replaceAll("#CValiDate#", mCalBase.getCValiDate());
        uwResult = uwResult.replaceAll("#Count#", mCalBase.getCount());
        uwResult = uwResult.replaceAll("#FirstPayDate#", "");
        uwResult = uwResult.replaceAll("#ContNo#", mCalBase.getContNo());
        uwResult = uwResult.replaceAll("#PolNo#", mCalBase.getPolNo());
        uwResult = uwResult.replaceAll("#InsuredNo#", mCalBase.getInsuredNo());
        uwResult = uwResult.replaceAll("#SecondInsuredNo#",mCalBase.getSecondInsuredNo());
        uwResult = uwResult.replaceAll("#RiskCode#", cRiskCode); //tLCPolSchema.getRiskCode());;
        uwResult = uwResult.replaceAll("#Job#", mCalBase.getJob());
        uwResult = uwResult.replaceAll("#LSumDangerAmnt#", mCalBase.getLSumDangerAmnt());
        uwResult = uwResult.replaceAll("#DSumDangerAmnt#", mCalBase.getDSumDangerAmnt());
        uwResult = uwResult.replaceAll("#ASumDangerAmnt#", mCalBase.getASumDangerAmnt());
        uwResult = uwResult.replaceAll("#MSumDangerAmnt#", mCalBase.getMSumDangerAmnt());
        uwResult = uwResult.replaceAll("#SSumDieAmnt#", mCalBase.getSSumDieAmnt());
        uwResult = uwResult.replaceAll("#ManageCom#", mCalBase.getManageCom());
        uwResult = uwResult.replaceAll("#AppntJob#", mCalBase.getAppntJob());
        uwResult = uwResult.replaceAll("#MainRiskGet#", mCalBase.getMainRiskGet());
        uwResult = uwResult.replaceAll("#RiskSort#", mCalBase.getRiskSort());
        uwResult = uwResult.replaceAll("#CustomerNo#", mCalBase.getCustomerNo());
        uwResult = uwResult.replaceAll("#Occupation#", mCalBase.getOccupation());
        uwResult = uwResult.replaceAll("#MainPolNo#", mCalBase.getMainPolNo());
        uwResult = uwResult.replaceAll("#AppAg2#", mCalBase.getAppAg2());
        uwResult = uwResult.replaceAll("#AppAge2#", mCalBase.getAppAge2());

        uwResult = uwResult.replaceAll("#AppntLSumDangerAmnt#", mCalBase.getAppntLSumDangerAmnt());
        uwResult = uwResult.replaceAll("#AppntDSumDangerAmnt#", mCalBase.getAppntDSumDangerAmnt());
        uwResult = uwResult.replaceAll("#AppntSSumDangerAmnt#", mCalBase.getAppntSSumDangerAmnt());
        uwResult = uwResult.replaceAll("#AppntMSumDangerAmnt#", mCalBase.getAppntMSumDangerAmnt());
        uwResult = uwResult.replaceAll("#AppntSSumDieAmnt#", mCalBase.getAppntSSumDieAmnt());
        uwResult = uwResult.replaceAll("#AppntAllSumAmnt#", mCalBase.getAppntAllSumAmnt());
        uwResult = uwResult.replaceAll("#ANativePlace#", mCalBase.getANativePlace());
        uwResult = uwResult.replaceAll("#AppntNo#", mCalBase.getAppntNo());

        //add by likai 20131011
        uwResult = uwResult.replaceAll("#LFSumDangerAmnt1#", mCalBase.getmLFSumDangerAmnt1());
        uwResult = uwResult.replaceAll("#LFSumDangerAmnt2#", mCalBase.getmLFSumDangerAmnt2());
        uwResult = uwResult.replaceAll("#LFSumDangerAmnt4#", mCalBase.getmLFSumDangerAmnt4());
        uwResult = uwResult.replaceAll("#LFSumDangerAmnt12#", mCalBase.getmLFSumDangerAmnt12());
        uwResult = uwResult.replaceAll("#LFSumDangerAmnt41#", mCalBase.getmLFSumDangerAmnt41());
        uwResult = uwResult.replaceAll("#LFSumDangerAmnt42#", mCalBase.getmLFSumDangerAmnt42());
        //add by shaozq REQ-673 风险保额区分储蓄和保障类 20170906 start
        uwResult = uwResult.replaceAll("#LFSumDangerAmntBZ1#", mCalBase.getmLFSumDangerAmntBZ1());
        uwResult = uwResult.replaceAll("#LFSumDangerAmntBZ2#", mCalBase.getmLFSumDangerAmntBZ2());
        uwResult = uwResult.replaceAll("#LFSumDangerAmntBZ4#", mCalBase.getmLFSumDangerAmntBZ4());
        uwResult = uwResult.replaceAll("#LFSumDangerAmntBZ12#", mCalBase.getmLFSumDangerAmntBZ12());
        uwResult = uwResult.replaceAll("#LFSumDangerAmntBZ41#", mCalBase.getmLFSumDangerAmntBZ41());
        uwResult = uwResult.replaceAll("#LFSumDangerAmntBZ42#", mCalBase.getmLFSumDangerAmntBZ42());
        uwResult = uwResult.replaceAll("#LFSumDangerAmntCX1#", mCalBase.getmLFSumDangerAmntCX1());
        uwResult = uwResult.replaceAll("#LFSumDangerAmntCX2#", mCalBase.getmLFSumDangerAmntCX2());
        uwResult = uwResult.replaceAll("#LFSumDangerAmntCX4#", mCalBase.getmLFSumDangerAmntCX4());
        uwResult = uwResult.replaceAll("#LFSumDangerAmntCX12#", mCalBase.getmLFSumDangerAmntCX12());
        uwResult = uwResult.replaceAll("#LFSumDangerAmntCX41#", mCalBase.getmLFSumDangerAmntCX41());
        uwResult = uwResult.replaceAll("#LFSumDangerAmntCX42#", mCalBase.getmLFSumDangerAmntCX42());

        ExeSQL ExeSql = new ExeSQL();
        String sql = "select comcode from ldcode1 where codetype = 'DangerAmnt1' and '"+ mCalBase.getAppAge()+"' between code and code1";
        String tsql = ExeSql.getOneValue(sql);
        uwResult = uwResult.replaceAll("#DangerAmntRemark1#", tsql);
        String sql2 = "select comcode from ldcode1 where codetype = 'DangerAmnt2' and '"+ mCalBase.getAppAge()+"' between code and code1";
        String tsql2 = ExeSql.getOneValue(sql2);
        uwResult = uwResult.replaceAll("#DangerAmntRemark2#", tsql2);
        //add by shaozq REQ-673 风险保额区分储蓄和保障类 20170906 end

        //add by shaozq REQ-693 新增智选核保检验参数  20171023 start
     /*   String sql3 = "select comcode from ldcode1 where codetype = 'DangerAmnt3' and '"+ mCalBase.getAppAge()+"' between code and code1";
        String tsql3 = ExeSql.getOneValue(sql3);
        uwResult = uwResult.replaceAll("#DangerAmntRemark3#", tsql3);
        String sql4 = "select comcode from ldcode1 where codetype = 'DangerAmnt4' and '"+ mCalBase.getAppAge()+"' between code and code1";
        String tsql4 = ExeSql.getOneValue(sql4);
        uwResult = uwResult.replaceAll("#DangerAmntRemark4#", tsql4);

        SSRS tSSRS = new SSRS();
        String sql5 = "select comcode,othersign from ldcode1 where codetype = 'DangerAmnt5' and '"+ mCalBase.getAppAge()+"' between code and code1";
        String sql6 = "select 1 from ldcode1 where codetype = 'DangerAmnt5' and "+mCalBase.getPolApplyDate()+" between to_date(CODENAME, 'yyyy/mm/dd') and to_date(CODEALIAS, 'yyyy/mm/dd')";
        String tsql6 = ExeSql.getOneValue(sql6);
        tSSRS = ExeSql.execSQL(sql5);
        if (tSSRS.getMaxRow() > 0) {
            if("1".equals(tsql6)){
                uwResult = uwResult.replaceAll("#DangerAmntRemark5#", tSSRS.GetText(1, 2));
            }else{
                uwResult = uwResult.replaceAll("#DangerAmntRemark5#", tSSRS.GetText(1, 1));
            }
        }*/
        //add by shaozq REQ-693 新增智选核保检验参数  20171023 end
        cLMUWSchema.setRemark(uwResult);
    }
    /**
     * @param
     * */
    public String getUWGrade(String tInsuredNo){
        String tUWGrade="";
        String tempUWGrade = "";
        String tempUWGrade_n = "";
        try {
            logger.debug(mContNo);
            ExeSQL riskSql = new ExeSQL();
            double RiskAmnt1=0;//累计寿险风险保额
            double RiskAmnt2=0;//累积重疾风险保额
            double RiskAmnt4=0;//累积意外风险保额
            double RiskAmnt6=0;
            //duanyh 2009-03-14 modify
            //使用新的自核风险保额计算规则
            String tsql = "";
            //寿险风险保额
			/*
			   -- tRiskType = 1 寿险风险保额
			   -- tRiskType = 2 重疾险风险保额
               -- tRiskType = 3 医疗险风险保额
               -- tRiskType = 4 意外险风险保额
               -- tRiskType = 12 身故风险保额
               -- tRiskType = 13 寿险体检额度
               -- tRiskType = 14 重疾体检额度
               -- tRiskType = 15 医疗体检额度

			 */
            tsql = "select healthyamnt2('" + tInsuredNo
                    + "','1','1') from dual";
            String tempAmnt = riskSql.getOneValue(tsql);
            RiskAmnt1 = parseFloat(tempAmnt);//寿险

            tsql = "select healthyamnt2('" + tInsuredNo
                    + "','2','1') from dual";
            tempAmnt = riskSql.getOneValue(tsql);
            RiskAmnt2 = parseFloat(tempAmnt); //重疾

            tsql = "select healthyamnt2('" + tInsuredNo
                    + "','4','1') from dual";
            tempAmnt = riskSql.getOneValue(tsql);
            RiskAmnt4 = parseFloat(tempAmnt); //意外

            RiskAmnt6 = RiskAmnt1+RiskAmnt2+RiskAmnt4;
            logger.debug("RiskAmnt1:"+RiskAmnt1+"  RiskAmnt2:"+RiskAmnt2+" " +
                    " RiskAmnt4:"+RiskAmnt4+"  RiskAmnt6:"+RiskAmnt6);
            //执行getUWGrade函数，返回核保级别
            String UWGradeSql = "select trim(nvl(getUWGrade('1','"+RiskAmnt1+"','2','"+RiskAmnt2+"'," +
                    "'4','"+RiskAmnt4+"','6','"+RiskAmnt6+"'),1)) from dual";
            tempUWGrade = riskSql.getOneValue(UWGradeSql);


            //REQ-325 STA
            String tsql_n = "select healthyamnt66('" + tInsuredNo
                    + "','66','4') from dual";
            String tempAmnt_n = riskSql.getOneValue(tsql_n);
            double RiskAmnt66 = parseFloat(tempAmnt_n); //年金
            //执行getUWGrade1函数，返回核保级别
            String UWGradeSql_n = "select trim(nvl(getUWGrade1('6','"+RiskAmnt66+"'),1)) from dual";
            tempUWGrade_n = riskSql.getOneValue(UWGradeSql_n);
            if(tempUWGrade_n.compareTo(tempUWGrade) < 0){
                tUWGrade=tempUWGrade;
            }
            if(tempUWGrade_n.compareTo(tempUWGrade) > 0){
                tUWGrade=tempUWGrade_n;
            }
            if(tempUWGrade_n.compareTo(tempUWGrade) == 0){
                if((tempUWGrade_n!=null||tempUWGrade_n!="")&&(tempUWGrade!=null||tempUWGrade!="")){
                    tUWGrade=tempUWGrade_n;
                }
            }
            //add by shaozq REQ-673 风险保额区分储蓄和保障类 20170906 start
            // 寿险临分风险保额(保障类)
            tsql = "select healthyamnt6('" + tInsuredNo
                    + "','1','3') from dual";
            tempAmnt = riskSql.getOneValue(tsql);
            mLFSumDangerAmntBZ1 = parseFloat(tempAmnt);
            // 重疾临分风险保额(保障类)
            tsql = "select healthyamnt6('" + tInsuredNo
                    + "','2','3') from dual";
            tempAmnt = riskSql.getOneValue(tsql);
            mLFSumDangerAmntBZ2 = parseFloat(tempAmnt);
            // 意外临分风险保额(保障类)
            tsql = "select healthyamnt6('" + tInsuredNo
                    + "','4','3') from dual";
            tempAmnt = riskSql.getOneValue(tsql);
            mLFSumDangerAmntBZ4 = parseFloat(tempAmnt);
            //人身临分风险保额(保障类)
            tsql = "select healthyamnt6('" + tInsuredNo
                    + "','12','3') from dual";
            tempAmnt = riskSql.getOneValue(tsql);
            mLFSumDangerAmntBZ12 = parseFloat(tempAmnt);
            //交通意外临分风险保额(保障类)
            tsql = "select healthyamnt6('" + tInsuredNo
                    + "','41','3') from dual";
            tempAmnt = riskSql.getOneValue(tsql);
            mLFSumDangerAmntBZ41 = parseFloat(tempAmnt);
            // 航空意外临分风险保额(保障类)
            tsql = "select healthyamnt6('" + tInsuredNo
                    + "','42','3') from dual";
            tempAmnt = riskSql.getOneValue(tsql);
            mLFSumDangerAmntBZ42 = parseFloat(tempAmnt);

            // 寿险临分风险保额(储蓄类)
            tsql = "select healthyamnt7('" + tInsuredNo
                    + "','1','3') from dual";
            tempAmnt = riskSql.getOneValue(tsql);
            mLFSumDangerAmntCX1 = parseFloat(tempAmnt);
            // 重疾临分风险保额(储蓄类)
            tsql = "select healthyamnt7('" + tInsuredNo
                    + "','2','3') from dual";
            tempAmnt = riskSql.getOneValue(tsql);
            mLFSumDangerAmntCX2 = parseFloat(tempAmnt);
            // 意外临分风险保额(储蓄类)
            tsql = "select healthyamnt7('" + tInsuredNo
                    + "','4','3') from dual";
            tempAmnt = riskSql.getOneValue(tsql);
            mLFSumDangerAmntCX4 = parseFloat(tempAmnt);
            //人身临分风险保额(储蓄类)
            tsql = "select healthyamnt7('" + tInsuredNo
                    + "','12','3') from dual";
            tempAmnt = riskSql.getOneValue(tsql);
            mLFSumDangerAmntCX12 = parseFloat(tempAmnt);
            //交通意外临分风险保额(储蓄类)
            tsql = "select healthyamnt7('" + tInsuredNo
                    + "','41','3') from dual";
            tempAmnt = riskSql.getOneValue(tsql);
            mLFSumDangerAmntCX41 = parseFloat(tempAmnt);
            // 航空意外临分风险保额(储蓄类)
            tsql = "select healthyamnt7('" + tInsuredNo
                    + "','42','3') from dual";
            tempAmnt = riskSql.getOneValue(tsql);
            mLFSumDangerAmntCX42 = parseFloat(tempAmnt);

            //add by shaozq REQ-673 风险保额区分储蓄和保障类 20170906 end


//			if(tUWGrade.compareTo(tempUWGrade) < 0){
//				tUWGrade=tempUWGrade;
//			}
            //END
        } catch (Exception ex) {
        }
        return tUWGrade;
    }
    /**
     * 操作一张保单的业务处理
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     * @param tLCPolSchema LCPolSchema
     * @param tLMUWSetUnpass LMUWSetUnpass
     * @return boolean
     */
    private boolean dealOnePol(LCPolPojo tLCPolSchema, LMUWSet tLMUWSetUnpass,LCContPojo tLCContPojo,TradeInfo requestInfo) {
        // 保单
        if (preparePol(tLCPolSchema) == false) { //设置tLCPolSchema数据。
            return false;
        }
        // 核保信息
        if (preparePolUW(tLCPolSchema, tLMUWSetUnpass,tLCContPojo,requestInfo) == false) { //准备险种核保主表和核保子表的信息。
            return false;
        }

        LCPolSchema PolSchema =getSchema(tLCPolSchema);
        LCPolSchema tLCPolSchemaDup = new LCPolSchema();
        tLCPolSchemaDup.setSchema(PolSchema);
        mAllLCPolSet.add(tLCPolSchemaDup);

        LCUWMasterSet tLCUWMasterSet = new LCUWMasterSet();
        tLCUWMasterSet.set(mLCUWMasterSet);
        mAllLCUWMasterSet.add(tLCUWMasterSet);

        LCUWSubSet tLCUWSubSet = new LCUWSubSet();
        tLCUWSubSet.set(mLCUWSubSet);

        mAllLCUWSubSet.add(tLCUWSubSet);

        LCUWErrorSet tLCUWErrorSet = new LCUWErrorSet();
        tLCUWErrorSet.set(mLCUWErrorSet);
        mAllLCErrSet.add(tLCUWErrorSet);

        return true;
    }

    private LCPolSchema getSchema(LCPolPojo lcPolPojo) {
        LCPolSchema lcPolSchema =  new LCPolSchema();
        lcPolSchema.setGrpContNo(lcPolPojo.getGrpContNo());
        lcPolSchema.setGrpPolNo(lcPolPojo.getGrpPolNo());
        lcPolSchema.setContNo(lcPolPojo.getContNo());
        lcPolSchema.setPolNo(lcPolPojo.getPolNo());
        lcPolSchema.setProposalNo(lcPolPojo.getProposalNo());
        lcPolSchema.setPrtNo(lcPolPojo.getPrtNo());
        lcPolSchema.setContType(lcPolPojo.getContType());
        lcPolSchema.setPolTypeFlag(lcPolPojo.getPolTypeFlag());
        lcPolSchema.setMainPolNo(lcPolPojo.getMainPolNo());
        lcPolSchema.setMasterPolNo(lcPolPojo.getMasterPolNo());
        lcPolSchema.setKindCode(lcPolPojo.getKindCode());
        lcPolSchema.setRiskCode(lcPolPojo.getRiskCode());
        lcPolSchema.setRiskVersion(lcPolPojo.getRiskVersion());
        lcPolSchema.setManageCom(lcPolPojo.getManageCom());
        lcPolSchema.setAgentCom(lcPolPojo.getAgentCom());
        lcPolSchema.setAgentType(lcPolPojo.getAgentType());
        lcPolSchema.setAgentCode(lcPolPojo.getAgentCode());
        lcPolSchema.setAgentGroup(lcPolPojo.getAgentGroup());
        lcPolSchema.setAgentCode1(lcPolPojo.getAgentCode1());
        lcPolSchema.setSaleChnl(lcPolPojo.getSaleChnl());
        lcPolSchema.setHandler(lcPolPojo.getHandler());
        lcPolSchema.setInsuredNo(lcPolPojo.getInsuredNo());
        lcPolSchema.setInsuredName(lcPolPojo.getInsuredName());
        lcPolSchema.setInsuredSex(lcPolPojo.getInsuredSex());
        lcPolSchema.setInsuredBirthday(fDate.getDate(lcPolPojo.getInsuredBirthday()));
        lcPolSchema.setInsuredAppAge(lcPolPojo.getInsuredAppAge());
        lcPolSchema.setInsuredPeoples(lcPolPojo.getInsuredPeoples());
        lcPolSchema.setOccupationType(lcPolPojo.getOccupationType());
        lcPolSchema.setAppntNo(lcPolPojo.getAppntNo());
        lcPolSchema.setAppntName(lcPolPojo.getAppntName());
        lcPolSchema.setCValiDate(fDate.getDate(lcPolPojo.getCValiDate()));
        lcPolSchema.setSignCom(lcPolPojo.getSignCom());
        lcPolSchema.setSignDate(fDate.getDate(lcPolPojo.getSignDate()));
        lcPolSchema.setSignTime(lcPolPojo.getSignTime());
        lcPolSchema.setFirstPayDate(fDate.getDate(lcPolPojo.getFirstPayDate()));
        lcPolSchema.setPayEndDate(fDate.getDate(lcPolPojo.getPayEndDate()));
        lcPolSchema.setPaytoDate(fDate.getDate(lcPolPojo.getPaytoDate()));
        lcPolSchema.setGetStartDate(fDate.getDate(lcPolPojo.getGetStartDate()));
        lcPolSchema.setEndDate(fDate.getDate(lcPolPojo.getEndDate()));
        lcPolSchema.setAcciEndDate(fDate.getDate(lcPolPojo.getAcciEndDate()));
        lcPolSchema.setGetYearFlag(lcPolPojo.getGetYearFlag());
        lcPolSchema.setGetYear(lcPolPojo.getGetYear());
        lcPolSchema.setPayEndYearFlag(lcPolPojo.getPayEndYearFlag());
        lcPolSchema.setPayEndYear(lcPolPojo.getPayEndYear());
        lcPolSchema.setInsuYearFlag(lcPolPojo.getInsuYearFlag());
        lcPolSchema.setInsuYear(lcPolPojo.getInsuYear());
        lcPolSchema.setAcciYearFlag(lcPolPojo.getAcciYearFlag());
        lcPolSchema.setAcciYear(lcPolPojo.getAcciYear());
        lcPolSchema.setGetStartType(lcPolPojo.getGetStartType());
        lcPolSchema.setSpecifyValiDate(lcPolPojo.getSpecifyValiDate());
        lcPolSchema.setPayMode(lcPolPojo.getPayMode());
        lcPolSchema.setPayLocation(lcPolPojo.getPayLocation());
        lcPolSchema.setPayIntv(lcPolPojo.getPayIntv());
        lcPolSchema.setPayYears(lcPolPojo.getPayYears());
        lcPolSchema.setYears(lcPolPojo.getYears());
        lcPolSchema.setManageFeeRate(lcPolPojo.getManageFeeRate());
        lcPolSchema.setFloatRate(lcPolPojo.getFloatRate());
        lcPolSchema.setPremToAmnt(lcPolPojo.getPremToAmnt());
        lcPolSchema.setMult(lcPolPojo.getMult());
        lcPolSchema.setStandPrem(lcPolPojo.getStandPrem());
        lcPolSchema.setPrem(lcPolPojo.getPrem());
        lcPolSchema.setSumPrem(lcPolPojo.getSumPrem());
        lcPolSchema.setAmnt(lcPolPojo.getAmnt());
        lcPolSchema.setRiskAmnt(lcPolPojo.getRiskAmnt());
        lcPolSchema.setLeavingMoney(lcPolPojo.getLeavingMoney());
        lcPolSchema.setEndorseTimes(lcPolPojo.getEndorseTimes());
        lcPolSchema.setClaimTimes(lcPolPojo.getClaimTimes());
        lcPolSchema.setLiveTimes(lcPolPojo.getLiveTimes());
        lcPolSchema.setRenewCount(lcPolPojo.getRenewCount());
        lcPolSchema.setLastGetDate(fDate.getDate(lcPolPojo.getLastGetDate()));
        lcPolSchema.setLastLoanDate(fDate.getDate(lcPolPojo.getLastLoanDate()));
        lcPolSchema.setLastRegetDate(fDate.getDate(lcPolPojo.getLastRegetDate()));
        lcPolSchema.setLastEdorDate(fDate.getDate(lcPolPojo.getLastEdorDate()));
        lcPolSchema.setLastRevDate(fDate.getDate(lcPolPojo.getLastRevDate()));
        lcPolSchema.setRnewFlag(lcPolPojo.getRnewFlag());
        lcPolSchema.setStopFlag(lcPolPojo.getStopFlag());
        lcPolSchema.setExpiryFlag(lcPolPojo.getExpiryFlag());
        lcPolSchema.setAutoPayFlag(lcPolPojo.getAutoPayFlag());
        lcPolSchema.setInterestDifFlag(lcPolPojo.getInterestDifFlag());
        lcPolSchema.setSubFlag(lcPolPojo.getSubFlag());
        lcPolSchema.setBnfFlag(lcPolPojo.getBnfFlag());
        lcPolSchema.setHealthCheckFlag(lcPolPojo.getHealthCheckFlag());
        lcPolSchema.setImpartFlag(lcPolPojo.getImpartFlag());
        lcPolSchema.setReinsureFlag(lcPolPojo.getReinsureFlag());
        lcPolSchema.setAgentPayFlag(lcPolPojo.getAgentPayFlag());
        lcPolSchema.setAgentGetFlag(lcPolPojo.getAgentGetFlag());
        lcPolSchema.setLiveGetMode(lcPolPojo.getLiveGetMode());
        lcPolSchema.setDeadGetMode(lcPolPojo.getDeadGetMode());
        lcPolSchema.setBonusGetMode(lcPolPojo.getBonusGetMode());
        lcPolSchema.setBonusMan(lcPolPojo.getBonusMan());
        lcPolSchema.setDeadFlag(lcPolPojo.getDeadFlag());
        lcPolSchema.setSmokeFlag(lcPolPojo.getSmokeFlag());
        lcPolSchema.setRemark(lcPolPojo.getRemark());
        lcPolSchema.setApproveFlag(lcPolPojo.getApproveFlag());
        lcPolSchema.setApproveCode(lcPolPojo.getApproveCode());
        lcPolSchema.setApproveDate(fDate.getDate(lcPolPojo.getApproveDate()));
        lcPolSchema.setApproveTime(lcPolPojo.getApproveTime());
        lcPolSchema.setUWFlag(lcPolPojo.getUWFlag());
        lcPolSchema.setUWCode(lcPolPojo.getUWCode());
        lcPolSchema.setUWDate(fDate.getDate(lcPolPojo.getUWDate()));
        lcPolSchema.setUWTime(lcPolPojo.getUWTime());
        lcPolSchema.setPolApplyDate(fDate.getDate(lcPolPojo.getPolApplyDate()));
        lcPolSchema.setAppFlag(lcPolPojo.getAppFlag());
        lcPolSchema.setPolState(lcPolPojo.getPolState());
        lcPolSchema.setStandbyFlag1(lcPolPojo.getStandbyFlag1());
        lcPolSchema.setStandbyFlag2(lcPolPojo.getStandbyFlag2());
        lcPolSchema.setStandbyFlag3(lcPolPojo.getStandbyFlag3());
        lcPolSchema.setOperator(lcPolPojo.getOperator());
        lcPolSchema.setMakeDate(fDate.getDate(lcPolPojo.getMakeDate()));
        lcPolSchema.setMakeTime(lcPolPojo.getMakeTime());
        lcPolSchema.setModifyDate(fDate.getDate(lcPolPojo.getModifyDate()));
        lcPolSchema.setModifyTime(lcPolPojo.getModifyTime());
        lcPolSchema.setWaitPeriod(lcPolPojo.getWaitPeriod());
        lcPolSchema.setGetForm(lcPolPojo.getGetForm());
        lcPolSchema.setGetBankCode(lcPolPojo.getGetBankCode());
        lcPolSchema.setGetBankAccNo(lcPolPojo.getGetBankAccNo());
        lcPolSchema.setGetAccName(lcPolPojo.getGetAccName());
        lcPolSchema.setKeepValueOpt(lcPolPojo.getKeepValueOpt());
        lcPolSchema.setPayRuleCode(lcPolPojo.getPayRuleCode());
        lcPolSchema.setAscriptionRuleCode(lcPolPojo.getAscriptionRuleCode());
        lcPolSchema.setAutoPubAccFlag(lcPolPojo.getAutoPubAccFlag());
        lcPolSchema.setAscriptionFlag(lcPolPojo.getAscriptionFlag());
      /*  lcPolSchema.setPCValiDate(fDate.getDate(lcPolPojo.getPCValiDate()));
        lcPolSchema.setRiskSequence(lcPolPojo.getRiskSequence());
        lcPolSchema.setCancleForegetSpecFlag(lcPolPojo.getCancleForegetSpecFlag());
        lcPolSchema.setTakeDate(fDate.getDate(lcPolPojo.getTakeDate()));
        lcPolSchema.setTakeTime(lcPolPojo.getTakeTime());
        lcPolSchema.setAirNo(lcPolPojo.getAirNo());
        lcPolSchema.setTicketNo(lcPolPojo.getTicketNo());
        lcPolSchema.setSeatNo(lcPolPojo.getSeatNo());
        lcPolSchema.setInputPrem(lcPolPojo.getInputPrem());
        lcPolSchema.setCurrency(lcPolPojo.getCurrency());*/
        lcPolSchema.setInvestRuleCode(lcPolPojo.getInvestRuleCode());
        lcPolSchema.setUintLinkValiFlag(lcPolPojo.getUintLinkValiFlag());
      /*  lcPolSchema.setLoanType(lcPolPojo.getLoanType());
        lcPolSchema.setLoanOrg(lcPolPojo.getLoanOrg());
        lcPolSchema.setLoanContNo(lcPolPojo.getLoanContNo());
        lcPolSchema.setLoanMoney(lcPolPojo.getLoanMoney());
        lcPolSchema.setRiskStyle(lcPolPojo.getRiskStyle());
        lcPolSchema.setPaymentPro(lcPolPojo.getPaymentPro());
        lcPolSchema.setContPlanCode(lcPolPojo.getContPlanCode());
        lcPolSchema.setContPlanName(lcPolPojo.getContPlanName());
        lcPolSchema.setOpGrade(lcPolPojo.getOpGrade());
        lcPolSchema.setOpDate(fDate.getDate(lcPolPojo.getOpDate()));*/

        return lcPolSchema;
    }

    /**
     * 准备保单信息
     * 输出：如果发生错误则返回false,否则返回true
     */
    private boolean preparePol(LCPolPojo tLCPolSchema) {
        /*-----------自核与复核作为同一个节点时需要置复核状态-----------*/
        tLCPolSchema.setApproveFlag("9");
        tLCPolSchema.setApproveCode(mOperator);
        tLCPolSchema.setApproveDate(PubFun.getCurrentDate());
        tLCPolSchema.setApproveTime(PubFun.getCurrentTime());
        /*--------------------------------------------------------*/

        logger.debug("险种核保标志" + mPolPassFlag);
        tLCPolSchema.setUWFlag(mPolPassFlag);
        tLCPolSchema.setUWCode(mOperator);
        tLCPolSchema.setUWDate(PubFun.getCurrentDate());
        tLCPolSchema.setUWTime(PubFun.getCurrentTime());
        tLCPolSchema.setModifyDate(PubFun.getCurrentDate());
        tLCPolSchema.setModifyTime(PubFun.getCurrentTime());
        return true;
    }

    /**
     * 准备险种核保信息
     * 输出：如果发生错误则返回false,否则返回true
     */
    private boolean preparePolUW(LCPolPojo tLCPolSchema,
                                 LMUWSet tLMUWSetUnpass,LCContPojo tLCContPojo,TradeInfo requestInfo) {
        int tuwno = 0;
        int batchNo=0;
        LCUWMasterPojo tLCUWMasterPojo = new LCUWMasterPojo();
        LCUWMasterSchema tLCUWMasterSchema = new LCUWMasterSchema();
        LCUWMasterDB tLCUWMasterDB = new LCUWMasterDB();
        tLCUWMasterDB.setProposalNo(tLCPolSchema.getProposalNo());
        LCUWMasterSet tLCUWMasterSet = new LCUWMasterSet();
        tLCUWMasterSet = tLCUWMasterDB.query();
        if (tLCUWMasterDB.mErrors.needDealError()) {
            CError.buildErr(this, mOldPolNo + "个人核保总表取数失败!");
            return false;
        }

        int n = tLCUWMasterSet.size();
        if (n == 0) {
            String tUWMasterID =  PubFun1.CreateMaxNo(PubFun1.PhysicNo, 22);
            tLCUWMasterSchema.setUWMasterID(tUWMasterID);
            tLCUWMasterSchema.setContID(tLCContPojo.getContID());
            tLCUWMasterSchema.setShardingID(tLCContPojo.getShardingID());
            tLCUWMasterSchema.setContNo(mContNo);
            tLCUWMasterSchema.setGrpContNo(tLCPolSchema.getGrpContNo());
            tLCUWMasterSchema.setPolNo(mOldPolNo);
            tLCUWMasterSchema.setProposalContNo(mPContNo);
            tLCUWMasterSchema.setProposalNo(tLCPolSchema.getProposalNo());
            tLCUWMasterSchema.setUWNo(1);
            tLCUWMasterSchema.setInsuredNo(tLCPolSchema.getInsuredNo());
            tLCUWMasterSchema.setInsuredName(tLCPolSchema.getInsuredName());
            tLCUWMasterSchema.setAppntNo(tLCPolSchema.getAppntNo());
            tLCUWMasterSchema.setAppntName(tLCPolSchema.getAppntName());
            tLCUWMasterSchema.setAgentCode(tLCPolSchema.getAgentCode());
            tLCUWMasterSchema.setAgentGroup(tLCPolSchema.getAgentGroup());
            tLCUWMasterSchema.setUWGrade(mUWGrade); //核保级别
            tLCUWMasterSchema.setAppGrade(mUWGrade); //申报级别
            tLCUWMasterSchema.setPostponeDay("");
            tLCUWMasterSchema.setPostponeDate("");
            tLCUWMasterSchema.setAutoUWFlag("1"); // 1 自动核保 2 人工核保
            tLCUWMasterSchema.setState(mPolPassFlag);
            tLCUWMasterSchema.setPassFlag(mPolPassFlag);
            tLCUWMasterSchema.setHealthFlag("0");
            tLCUWMasterSchema.setSpecFlag("0");
            tLCUWMasterSchema.setQuesFlag("0");
            tLCUWMasterSchema.setReportFlag("0");
            tLCUWMasterSchema.setChangePolFlag("0");
            tLCUWMasterSchema.setPrintFlag("0");
            tLCUWMasterSchema.setManageCom(tLCPolSchema.getManageCom());
            tLCUWMasterSchema.setUWIdea("");
            tLCUWMasterSchema.setUpReportContent("");
            tLCUWMasterSchema.setOperator(mOperator); //操作员
            tLCUWMasterSchema.setMakeDate(PubFun.getCurrentDate());
            tLCUWMasterSchema.setMakeTime(PubFun.getCurrentTime());
            tLCUWMasterSchema.setModifyDate(PubFun.getCurrentDate());
            tLCUWMasterSchema.setModifyTime(PubFun.getCurrentTime());
        } else if (n == 1) {
            tLCUWMasterSchema = tLCUWMasterSet.get(1);

            tuwno = tLCUWMasterSchema.getUWNo();
            tuwno = tuwno + 1;

            tLCUWMasterSchema.setUWNo(tuwno);
            tLCUWMasterSchema.setProposalContNo(mPContNo);
            tLCUWMasterSchema.setState(mPolPassFlag);
            tLCUWMasterSchema.setPassFlag(mPolPassFlag);
            tLCUWMasterSchema.setAutoUWFlag("1"); // 1 自动核保 2 人工核保
            tLCUWMasterSchema.setUWGrade(mUWGrade); //核保级别
            tLCUWMasterSchema.setAppGrade(mUWGrade); //申报级别
            tLCUWMasterSchema.setOperator(mOperator); //操作员
            tLCUWMasterSchema.setModifyDate(PubFun.getCurrentDate());
            tLCUWMasterSchema.setModifyTime(PubFun.getCurrentTime());
        } else {
            CError.buildErr(this, mOldPolNo + "个人核保总表取数据不唯一!");
            return false;
        }

        // 核保轨迹表
        LCUWSubSchema tLCUWSubSchema = new LCUWSubSchema();
        LCUWSubDB tLCUWSubDB = new LCUWSubDB();
        tLCUWSubDB.setProposalNo(tLCUWMasterSchema.getProposalNo());
        LCUWSubSet tLCUWSubSet = new LCUWSubSet();
        tLCUWSubSet = tLCUWSubDB.query();
        if (tLCUWSubDB.mErrors.needDealError()) {
            CError.buildErr(this, mOldPolNo + "个人核保轨迹表查失败!");
            return false;
        }

        int m = tLCUWSubSet.size();
        int uwno = 1;
        batchNo = uwno;
        if (m > 0) {
            uwno = tLCUWSubSet.get(m).getUWNo()+1;
            batchNo = uwno;
            tLCUWSubSchema.setUWNo(uwno); //第几次核保
        } else {
            tLCUWSubSchema.setUWNo(1); //第1次核保
        }

        String tUWSubID = PubFun1.CreateMaxNo(PubFun1.PhysicNo, 22);
        tLCUWSubSchema.setUWSubID(tUWSubID);
        tLCUWSubSchema.setUWMasterID(tLCUWMasterSchema.getUWMasterID());
        tLCUWSubSchema.setShardingID(tLCUWMasterSchema.getShardingID());
        tLCUWSubSchema.setContNo(mContNo);
        tLCUWSubSchema.setPolNo(mOldPolNo);
        tLCUWSubSchema.setGrpContNo(tLCUWMasterSchema.getGrpContNo());
        tLCUWSubSchema.setProposalContNo(tLCUWMasterSchema.getProposalContNo());
        tLCUWSubSchema.setProposalNo(tLCUWMasterSchema.getProposalNo());
        tLCUWSubSchema.setInsuredNo(tLCUWMasterSchema.getInsuredNo());
        tLCUWSubSchema.setInsuredName(tLCUWMasterSchema.getInsuredName());
        tLCUWSubSchema.setAppntNo(tLCUWMasterSchema.getAppntNo());
        tLCUWSubSchema.setAppntName(tLCUWMasterSchema.getAppntName());
        tLCUWSubSchema.setAgentCode(tLCUWMasterSchema.getAgentCode());
        tLCUWSubSchema.setAgentGroup(tLCUWMasterSchema.getAgentGroup());
        tLCUWSubSchema.setUWGrade(tLCUWMasterSchema.getUWGrade()); //核保级别
        tLCUWSubSchema.setAppGrade(tLCUWMasterSchema.getAppGrade()); //申请级别
        tLCUWSubSchema.setAutoUWFlag(tLCUWMasterSchema.getAutoUWFlag());
        tLCUWSubSchema.setState(tLCUWMasterSchema.getState());
        tLCUWSubSchema.setPassFlag(tLCUWMasterSchema.getState());
        tLCUWSubSchema.setPostponeDay(tLCUWMasterSchema.getPostponeDay());
        tLCUWSubSchema.setPostponeDate(tLCUWMasterSchema.getPostponeDate());
        tLCUWSubSchema.setUpReportContent(tLCUWMasterSchema.getUpReportContent());
        tLCUWSubSchema.setHealthFlag(tLCUWMasterSchema.getHealthFlag());
        tLCUWSubSchema.setSpecFlag(tLCUWMasterSchema.getSpecFlag());
        tLCUWSubSchema.setSpecReason(tLCUWMasterSchema.getSpecReason());
        tLCUWSubSchema.setQuesFlag(tLCUWMasterSchema.getQuesFlag());
        tLCUWSubSchema.setReportFlag(tLCUWMasterSchema.getReportFlag());
        tLCUWSubSchema.setChangePolFlag(tLCUWMasterSchema.getChangePolFlag());
        tLCUWSubSchema.setChangePolReason(tLCUWMasterSchema.getChangePolReason());
        tLCUWSubSchema.setAddPremReason(tLCUWMasterSchema.getAddPremReason());
        tLCUWSubSchema.setPrintFlag(tLCUWMasterSchema.getPrintFlag());
        tLCUWSubSchema.setPrintFlag2(tLCUWMasterSchema.getPrintFlag2());
        tLCUWSubSchema.setUWIdea(tLCUWMasterSchema.getUWIdea());
        tLCUWSubSchema.setOperator(tLCUWMasterSchema.getOperator()); //操作员
        tLCUWSubSchema.setManageCom(tLCUWMasterSchema.getManageCom());
        tLCUWSubSchema.setMakeDate(PubFun.getCurrentDate());
        tLCUWSubSchema.setMakeTime(PubFun.getCurrentTime());
        tLCUWSubSchema.setModifyDate(PubFun.getCurrentDate());
        tLCUWSubSchema.setModifyTime(PubFun.getCurrentTime());

        mLCUWSubSet.clear();
        mLCUWSubSet.add(tLCUWSubSchema);

        //      tLCUWMasterSchema.setBatchNo(batchNo);
        mLCUWMasterSet.clear();
        mLCUWMasterSet.add(tLCUWMasterSchema);

        // 核保错误信息表
        LCUWErrorSchema tLCUWErrorSchema = new LCUWErrorSchema();
        tLCUWErrorSchema.setUWMasterID(tLCUWMasterSchema.getUWMasterID());
        tLCUWErrorSchema.setShardingID(tLCUWMasterSchema.getShardingID());
        tLCUWErrorSchema.setSerialNo("0");
        tLCUWErrorSchema.setUWNo(uwno);
        tLCUWErrorSchema.setContNo(mContNo);
        tLCUWErrorSchema.setGrpContNo(tLCUWMasterSchema.getGrpContNo());
        tLCUWErrorSchema.setProposalContNo(tLCContPojo.getThirdPartyOrderId());
        tLCUWErrorSchema.setPolNo(mOldPolNo);
        tLCUWErrorSchema.setProposalNo(tLCPolSchema.getProposalNo());
        tLCUWErrorSchema.setInsuredNo(tLCPolSchema.getInsuredNo());
        tLCUWErrorSchema.setInsuredName(tLCPolSchema.getInsuredName());
        tLCUWErrorSchema.setAppntNo(tLCPolSchema.getAppntNo());
        tLCUWErrorSchema.setAppntName(tLCPolSchema.getAppntName());
        tLCUWErrorSchema.setManageCom(tLCPolSchema.getManageCom());
        tLCUWErrorSchema.setUWRuleCode(""); //核保规则编码
        tLCUWErrorSchema.setUWError(""); //核保出错信息
        tLCUWErrorSchema.setCurrValue(""); //当前值
        tLCUWErrorSchema.setModifyDate(PubFun.getCurrentDate());
        tLCUWErrorSchema.setModifyTime(PubFun.getCurrentTime());
        tLCUWErrorSchema.setUWPassFlag(mPolPassFlag);

        //取核保错误信息
        mLCUWErrorSet.clear();
        int merrcount = tLMUWSetUnpass.size();
        if (merrcount > 0) {
            for (int i = 1; i <= merrcount; i++) {
                //取出错信息
                LMUWSchema tLMUWSchema = new LMUWSchema();
                tLMUWSchema = tLMUWSetUnpass.get(i);
                //生成流水号
                String tserialno = "" + i;
                String tUWErrorID = PubFun1.CreateMaxNo(PubFun1.PhysicNo, 22);
                tLCUWErrorSchema.setUWErrorID(tUWErrorID);
                tLCUWErrorSchema.setSerialNo(tserialno);
                tLCUWErrorSchema.setUWRuleCode(tLMUWSchema.getUWCode()); //核保规则编码
                if(tLMUWSchema.getRiskCode().equals("125503")){
                    tLCUWErrorSchema.setInsuredNo(tLCPolSchema.getAppntNo());
                    tLCUWErrorSchema.setInsuredName(tLCPolSchema.getAppntName());
                }
                tLCUWErrorSchema.setUWError(tLMUWSchema.getRemark().trim()); //核保出错信息，即核保规则的文字描述内容
                tLCUWErrorSchema.setUWGrade(tLMUWSchema.getUWGrade());
                tLCUWErrorSchema.setCurrValue(""); //当前值
                tLCUWErrorSchema.setSugPassFlag(tLMUWSchema.getPassFlag());

                LCUWErrorSchema ttLCUWErrorSchema = new LCUWErrorSchema();
                ttLCUWErrorSchema.setSchema(tLCUWErrorSchema);
                mLCUWErrorSet.add(ttLCUWErrorSchema);
            }
        }

        return true;
    }

}
