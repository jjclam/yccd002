package com.sinosoft.cloud.cbs.uw;


import com.alibaba.fastjson.JSON;
import com.sinosoft.cloud.bl.AbstractBL;
import com.sinosoft.cloud.common.CalBase1;
import com.sinosoft.cloud.common.Calculator;
import com.sinosoft.cloud.rest.TradeInfo;
import com.sinosoft.lis.db.LMUWDB;
import com.sinosoft.lis.entity.LCResultInfoPojo;
import com.sinosoft.lis.entity.SLCXmlSchema;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LMUWSchema;
import com.sinosoft.lis.vschema.LMUWSet;
import com.sinosoft.utility.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * @Author: 朱一鸣
 * @Description:
 * @Date: Created in 11:37 2017/9/22
 * @Modified By
 */
@Service
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
@Slf4j
public class PlanUWByEngineBL extends AbstractBL {



    private CalBase1 mCalBase1 = new CalBase1();

    private  SLCXmlSchema mSLCXmlSchema= new SLCXmlSchema ();


    private String theCurrentDate = PubFun.getCurrentDate();

    private String theCurrentTime = PubFun.getCurrentTime();

    /** 数字格式化 */
    private DecimalFormat mDecimalFormat = new DecimalFormat("##.###");
    private String mCalCode; //计算编码

    private double mValue; //自动核保通过标志

    //核保信息
    private LMUWSet lmuwSet = new LMUWSet();

    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    @Override
    public TradeInfo dealData(TradeInfo requestInfo) {

        mErrors.clearErrors ();
        String jsonSlcxml = null;
        try {
            jsonSlcxml = (String) requestInfo.getData ("SLCXmlSchema");
            log.info ("jsonSlcxml的对象"+jsonSlcxml);
            mSLCXmlSchema = JSON.parseObject (jsonSlcxml, SLCXmlSchema.class);
            if (!CheckKinds(mSLCXmlSchema.getContPlanCode ())) {
                requestInfo.addError (mErrors.getFirstError ());
                return requestInfo;
            }
            log.info ("lmuwSet.size ()"+lmuwSet.size ());
            if (lmuwSet.size ()==0){
                return requestInfo ;
            }
            CheckPolInit(mSLCXmlSchema);
            for (int i = 1; i <= lmuwSet.size (); i++) {
                LMUWSchema tSLMUWSchema = new LMUWSchema();
                tSLMUWSchema = lmuwSet.get(i);
                mCalCode = tSLMUWSchema.getCalCode();
                if (!CheckPol()) {
                    List<LCResultInfoPojo> lcResultInfoPojos = new ArrayList<>();
                    LCResultInfoPojo lcResultInfoPojo = new LCResultInfoPojo();
                    lcResultInfoPojo.setResultNo (tSLMUWSchema.getCalCode ());
                    lcResultInfoPojo.setResultContent(mErrors.getFirstError ());
                    log.info ("mErrors.getFirstError ()++++++"+mErrors.getFirstError ());
                    Reflections reflections = new Reflections();
                    lcResultInfoPojos.add((LCResultInfoPojo) reflections.transFields(new LCResultInfoPojo(), lcResultInfoPojo));
                    requestInfo.addData(LCResultInfoPojo.class.getName(), lcResultInfoPojos);
                    return requestInfo;
                }
            }
        } catch (Exception e) {
            e.printStackTrace ( );
            requestInfo.addError (e.getMessage ());
            return requestInfo;
        }


    return requestInfo ;

    }

    /**
     * 准备核保算法
     * 输出：如果发生错误则返回false,否则返回true
     */
    private boolean  CheckKinds( String contplanno) {


        String tsql = "";
        //modify by gq req-652   核保规则取值sql调整   2017/07/18  start
        tsql = "select * from lmuw where uwcode='030'";
        //modify by gq req-652   核保规则取值sql调整   2017/07/18  end
        LMUWDB tLMUWDB = new LMUWDB();
        lmuwSet = tLMUWDB.executeQuery(tsql);

        if (tLMUWDB.mErrors.needDealError() == true) {
            CError.buildErr(this, "合同险种核保信息查询失败!");
            lmuwSet.clear();
            return false;
        }
        return true;
    }

    /**
     * 核保数据准备
     * 输出：如果发生错误则返回false,否则返回true
     */
    private void CheckPolInit(SLCXmlSchema mSLCXmlSchema) {
        //1份数
        mCalBase1.setMult(mSLCXmlSchema.getMult());
        //2被保人姓名
        mCalBase1.setName(mSLCXmlSchema.getInsuredName());
        //3被保人证件类型
        mCalBase1.setIDType(mSLCXmlSchema.getInsuredIDType());
        //4被保人证件号
        mCalBase1.setIDNo(mSLCXmlSchema.getInsuredIDNo());
        //5被保人出生日期
        String birthday = PubFun.getDatatimeFormatString (mSLCXmlSchema.getInsuredBirthday())  ;
        mCalBase1.setBirthday(birthday);
        //6被保人性别
        mCalBase1.setSex(mSLCXmlSchema.getInsuredSex());
        //7年龄
        int appage;
        if ((birthday.substring(0, 4).equals("8888"))) {
            //被保人年龄为默认时，将年龄默认为20，方便核保
            appage = 20;
        } else {
            appage = PubFun.calInterval(birthday,theCurrentDate, "Y");

        }
        mCalBase1.setAppAge(appage);
        //投保人年龄
        mCalBase1.setAge(PubFun.calInterval(PubFun.getDatatimeFormatString(mSLCXmlSchema.getAppBirthday()),
                theCurrentDate, "Y"));
        //8保险期间
        mCalBase1.setInsuYear(mSLCXmlSchema.getInsuYear());
        //9保险期间单位
        mCalBase1.setInsuYearFlag(mSLCXmlSchema.getInsuYearFlag());
        //10生效日期
        mCalBase1.setCValiDate(PubFun.getDatatimeFormatString(mSLCXmlSchema.getCValiDate()));
        //11航班号
        mCalBase1.setFlightNO(mSLCXmlSchema.getFlightNo());
        //12当前日期
        mCalBase1.setMakeDate(theCurrentDate);
        //13当前时间
        mCalBase1.setMakeTime(theCurrentTime);

        //14 被保人职业类别
        mCalBase1.setOccupationType(mSLCXmlSchema.getOccupationType());
        //15 保额  
        mCalBase1.setGet(mSLCXmlSchema.getAmnt());
        //16 投保人与被保险人的关系:是被保人的
        mCalBase1.setRelationToAppnt(mSLCXmlSchema.getIsAppPerson());
        //17 投保日期
        mCalBase1.setPolApplyDate(mSLCXmlSchema.getPolApplyDate());

        mCalBase1.setAddInsured("0");
        mCalBase1.setMateAge("null");
        mCalBase1.setChildAge("null");
        mCalBase1.setChildBirthday("null");

        //保额
        mCalBase1.setLoanMoney(mSLCXmlSchema.getLoanMoney());
        mCalBase1.setAmnt(mSLCXmlSchema.getAmnt());
        mCalBase1.setInterCode(mSLCXmlSchema.getInterCode());
        System.out.println("保险款式为："+mSLCXmlSchema.getInterCode());
    }

    /**
     * 核保
     * 输出：如果发生错误则返回false,否则返回true
     */
    /**
     * 核保
     * 输出：如果发生错误则返回false,否则返回true
     */
    private boolean CheckPol() {
        // 计算
        Calculator mCalculator = new Calculator();
        mCalculator.setCalCode(mCalCode);
        //增加基本要素
        mCalculator.addBasicFactor("Mult", mCalBase1.getMult());
        mCalculator.addBasicFactor("Name", mCalBase1.getName());
        mCalculator.addBasicFactor("IDType", mCalBase1.getIDType());
        mCalculator.addBasicFactor("IDNo", mCalBase1.getIDNo());
        mCalculator.addBasicFactor("Birthday", mCalBase1.getBirthday());
        mCalculator.addBasicFactor("InterCode", mCalBase1.getInterCode());
        //add by fengzq 20071125 增加出生日期不能大于当前日期的校验
        if (!mCalBase1.getBirthday().substring(0, 4).equals("8888")) {
            if (PubFun.calInterval(mCalBase1.getBirthday(), theCurrentDate, "D") < 0) {
                System.out.println("自动核保未通过:被保人出生日期大于当前日期!");
                CError tError = new CError();
                tError.moduleName = "ContPolBL";
                tError.functionName = "CheckPol";
                tError.errorMessage = "自动核保未通过：被保人出生日期大于当前日期!";
                this.mErrors.addOneError(tError);
            }
        }
        //add by fengzq 20071125  end
        mCalculator.addBasicFactor("AddInsured", mCalBase1.getAddInsured());
        mCalculator.addBasicFactor("Amnt", mCalBase1.getAmnt());

        mCalculator.addBasicFactor("MateAge", mCalBase1.getMateAge());
        mCalculator.addBasicFactor("AddRate", mCalBase1.getAddRate());
        mCalculator.addBasicFactor("CalType", mCalBase1.getCalType());
        mCalculator.addBasicFactor("ChildAge", mCalBase1.getChildAge());
        mCalculator
                .addBasicFactor("ChildBirthday", mCalBase1.getChildBirthday());
        mCalculator.addBasicFactor("ChildAge", mCalBase1.getChildAge());
        mCalculator.addBasicFactor("RelationToAppnt", mCalBase1
                .getRelationToAppnt());
        mCalculator.addBasicFactor("Get", mCalBase1.getGet());
        mCalculator.addBasicFactor("OccupationType", mCalBase1
                .getOccupationType());
        mCalculator.addBasicFactor("PolApplyDate", mCalBase1.getPolApplyDate());

        mCalculator.addBasicFactor("Sex", mCalBase1.getSex());
        mCalculator.addBasicFactor("Age", mCalBase1.getAge());
        mCalculator.addBasicFactor("AppAge", mCalBase1.getAppAge());
        mCalculator.addBasicFactor("InsuYear", mCalBase1.getInsuYear());
        mCalculator.addBasicFactor("InsuYearFlag", mCalBase1.getInsuYearFlag());
        mCalculator.addBasicFactor("CValiDate", mCalBase1.getCValiDate());
        mCalculator.addBasicFactor("FlightNO", mCalBase1.getFlightNO());
        mCalculator.addBasicFactor("MakeDate", mCalBase1.getMakeDate());
        mCalculator.addBasicFactor("MakeTime", mCalBase1.getMakeTime());
        mCalculator.addBasicFactor("LoanMoney", mDecimalFormat.format(mCalBase1.getLoanMoney()));
        mCalculator.addBasicFactor("NextMakeDate", PubFun.calDateIntev(mCalBase1.getMakeDate(), 1, "D"));//add by chenliang 20120321 NextMakeDate 当前日期的次日 
        System.out.println("NextMakeDate===>"+PubFun.calDateIntev(mCalBase1.getMakeDate(), 1, "D"));
        System.out.println("Cvalidate===>"+mCalBase1.getCValiDate());

        String tStr = "";
        tStr = mCalculator.calculate();
        System.out.println("=================" + tStr);
        if (tStr.trim().equals("")) {
            mValue = 0;
        } else {
            mValue = Double.parseDouble(tStr);
        }
        if (mValue > 0) {
            String info = "";
            try {
                ExeSQL nExeSQL = new ExeSQL();
                SSRS nSSRS = new SSRS();
                String sql = "select remark from lmcalmode where calcode ='"
                        + mCalCode + "'";
                nSSRS = nExeSQL.execSQL(sql);
                if (nSSRS.MaxRow > 0) {
                    info = nSSRS.GetText(1, 1);
                }
            } catch (Exception e) {
                info = "未知错误";
            }
            System.out.println("" + info);
            CError tError = new CError();
            tError.moduleName = "ContPolBL";
            tError.functionName = "CheckPol";
            tError.errorMessage = "" + info;
            this.mErrors.addOneError(tError);
            return false;
        }

        else {
            return true;
        }
    }

}
