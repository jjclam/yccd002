package com.sinosoft.cloud.cbs.trules.bom;


import java.util.Map;

/**
 * 合同信息
 *
 */
public class TProductInfo {
    /**
     * AIP
     * CIP
     * WEBULAR
     */
    private String platform;

    /**
     * 核保流向标记
     */
    private String flowFlag;

    /**
     * 套餐编码
     */
    private String productNo;
    /**
     * 投保单号
     */
    private String appformNo;
    /**
     * 投保日期
     */
    private String applyApplyDate;
    /**
     * 保险起期
     */
    private String insurStartDate;
    /**
     * 保险止期
     */
    private String insurEndDate;
    /**
     * 单证印刷号
     */
    private String docPrtNo;
    /**
     * 经办人
     */
    private String insertOper;
    /**
     * 经办人电话
     */
    private String handlerTel;
    /**
     * 借款合同编号
     */
    private String loancontractnumber;
    /**
     * 借款凭证编号
     */
    private String loandocumentnumber;
    /**
     * 借费发放日期
     */
    private String borrowfeeissuedate;
    /**
     * 借费结束日期
     */
    private String borrowtheenddate;
    /**
     * 借款期限
     */
    private String loandata;
    /**
     * 贷款金额
     */
    private double loanmoney;
    /**
     * 贷款发放机构
     */
    private String loangrantinginstitution;
    /**
     * 借款人名称
     */
    private String borrowername;
    /**
     * 借款人性质
     */
    private String borrowerproperty;
    /**
     * 燃气公司
     */
    private String gasCompany;
    /**
     * 燃气用户地址
     */
    private String gasUserAddr;
    /**
     * 投保确认书签字时间
     */
    private String affirmDate;
    /**
     * 保费收费确认时间
     */
    private String chargeDate;
    /**
     * 学校名称
     */
    private String schoolName;
    /**
     * 家庭住址
     */
    private String addr;
    /**
     * 救援卡卡号
     */
    private String rescuecardno;
    /**
     * 电子保单
     */
    private String electron;
    /**
     * 纸质保单
     */
    private String papery;
    /**
     * 出访国家
     */
    private Map<String,String> countrys;
    /**
     * 管理机构
     */
    private String mngorgCode;
    /**
     * 中介机构类别
     */
    private String agencyType;
    /**
     * 中介机构子渠道
     */
    private String agencyChnnlType;
    /**
     * 银行编码
     */
    private String bankCode;
    /**
     * 企业借款人性质
     */
    private String borrowerType;
    /**
     * 农行标志
     */
    private String nonghangSign;

    /**
     * 渠道编码
     * @return
     */
    private String chnnlCode;

    /**
     * 套餐类型
     */
    private String productType;

    /**
     * SEP保险计划份数
     */
    private double sepNum;


    /**
     * 本次JSJXBC投保份数
     */
    private double theJSJXBCNum;

    /**
     * 本次JSJXBB投保份数
     */
    private double theJSJXBBNum;

    /**
     * 本次SESXNH份数
     */
    private double theSESXNHNum;

    public double getTheSESXNHNum() {return theSESXNHNum;}

    public void setTheSESXNHNum(double theSESXNHNum) {this.theSESXNHNum = theSESXNHNum;}

    public double getTheJSJXBBNum() {
        return theJSJXBBNum;
    }

    public void setTheJSJXBBNum(double theJSJXBBNum) {
        this.theJSJXBBNum = theJSJXBBNum;
    }

    public double getTheJSJXBCNum() {
        return theJSJXBCNum;
    }

    public void setTheJSJXBCNum(double theJSJXBCNum) {
        this.theJSJXBCNum = theJSJXBCNum;
    }

    public double getSepNum() {
        return sepNum;
    }

    public void setSepNum(double sepNum) {
        this.sepNum = sepNum;
    }

    public String getPlatform() {
        return platform;
    }
    public void setPlatform(String platform) {
        this.platform = platform;
    }
    public String getFlowFlag() {
        return flowFlag;
    }
    public void setFlowFlag(String flowFlag) {
        this.flowFlag = flowFlag;
    }
    public String getProductType() {
        return productType;
    }
    public void setProductType(String productType) {
        this.productType = productType;
    }
    public String getChnnlCode() {
        return chnnlCode;
    }
    public void setChnnlCode(String chnnlCode) {
        this.chnnlCode = chnnlCode;
    }
    public String getProductNo() {
        return productNo;
    }
    public void setProductNo(String productNo) {
        this.productNo = productNo;
    }
    public String getApplyApplyDate() {
        return applyApplyDate;
    }
    public void setApplyApplyDate(String applyApplyDate) {
        this.applyApplyDate = applyApplyDate;
    }
    public String getAppformNo() {
        return appformNo;
    }
    public void setAppformNo(String appformNo) {
        this.appformNo = appformNo;
    }
    public String getInsurStartDate() {
        return insurStartDate;
    }
    public void setInsurStartDate(String insurStartDate) {
        this.insurStartDate = insurStartDate;
    }
    public String getInsurEndDate() {
        return insurEndDate;
    }
    public void setInsurEndDate(String insurEndDate) {
        this.insurEndDate = insurEndDate;
    }
    public String getDocPrtNo() {
        return docPrtNo;
    }
    public void setDocPrtNo(String docPrtNo) {
        this.docPrtNo = docPrtNo;
    }
    public String getInsertOper() {
        return insertOper;
    }
    public void setInsertOper(String insertOper) {
        this.insertOper = insertOper;
    }
    public String getHandlerTel() {
        return handlerTel;
    }
    public void setHandlerTel(String handlerTel) {
        this.handlerTel = handlerTel;
    }
    public String getLoancontractnumber() {
        return loancontractnumber;
    }
    public void setLoancontractnumber(String loancontractnumber) {
        this.loancontractnumber = loancontractnumber;
    }
    public String getLoandocumentnumber() {
        return loandocumentnumber;
    }
    public void setLoandocumentnumber(String loandocumentnumber) {
        this.loandocumentnumber = loandocumentnumber;
    }
    public String getBorrowfeeissuedate() {
        return borrowfeeissuedate;
    }
    public void setBorrowfeeissuedate(String borrowfeeissuedate) {
        this.borrowfeeissuedate = borrowfeeissuedate;
    }
    public String getBorrowtheenddate() {
        return borrowtheenddate;
    }
    public void setBorrowtheenddate(String borrowtheenddate) {
        this.borrowtheenddate = borrowtheenddate;
    }
    public String getLoandata() {
        return loandata;
    }
    public void setLoandata(String loandata) {
        this.loandata = loandata;
    }
    public double getLoanmoney() {
        return loanmoney;
    }
    public void setLoanmoney(double loanmoney) {
        this.loanmoney = loanmoney;
    }
    public String getLoangrantinginstitution() {
        return loangrantinginstitution;
    }
    public void setLoangrantinginstitution(String loangrantinginstitution) {
        this.loangrantinginstitution = loangrantinginstitution;
    }
    public String getBorrowername() {
        return borrowername;
    }
    public void setBorrowername(String borrowername) {
        this.borrowername = borrowername;
    }
    public String getBorrowerproperty() {
        return borrowerproperty;
    }
    public void setBorrowerproperty(String borrowerproperty) {
        this.borrowerproperty = borrowerproperty;
    }
    public String getGasCompany() {
        return gasCompany;
    }
    public void setGasCompany(String gasCompany) {
        this.gasCompany = gasCompany;
    }
    public String getGasUserAddr() {
        return gasUserAddr;
    }
    public void setGasUserAddr(String gasUserAddr) {
        this.gasUserAddr = gasUserAddr;
    }
    public String getAffirmDate() {
        return affirmDate;
    }
    public void setAffirmDate(String affirmDate) {
        this.affirmDate = affirmDate;
    }
    public String getChargeDate() {
        return chargeDate;
    }
    public void setChargeDate(String chargeDate) {
        this.chargeDate = chargeDate;
    }
    public String getSchoolName() {
        return schoolName;
    }
    public void setSchoolName(String schoolName) {
        this.schoolName = schoolName;
    }
    public String getAddr() {
        return addr;
    }
    public void setAddr(String addr) {
        this.addr = addr;
    }
    public String getRescuecardno() {
        return rescuecardno;
    }
    public void setRescuecardno(String rescuecardno) {
        this.rescuecardno = rescuecardno;
    }
    public String getElectron() {
        return electron;
    }
    public void setElectron(String electron) {
        this.electron = electron;
    }
    public String getPapery() {
        return papery;
    }
    public void setPapery(String papery) {
        this.papery = papery;
    }
    public Map<String, String> getCountrys() {
        return countrys;
    }
    public void setCountrys(Map<String, String> countrys) {
        this.countrys = countrys;
    }
    public String getMngorgCode() {
        return mngorgCode;
    }
    public void setMngorgCode(String mngorgCode) {
        this.mngorgCode = mngorgCode;
    }
    public String getAgencyType() {
        return agencyType;
    }
    public void setAgencyType(String agencyType) {
        this.agencyType = agencyType;
    }
    public String getAgencyChnnlType() {return agencyChnnlType;}
    public void setAgencyChnnlType(String agencyChnnlType) {this.agencyChnnlType = agencyChnnlType;}
    public String getBankCode() {
        return bankCode;
    }
    public void setBankCode(String bankCode) {
        this.bankCode = bankCode;
    }
    public String getBorrowerType() {
        return borrowerType;
    }
    public void setBorrowerType(String borrowerType) {
        this.borrowerType = borrowerType;
    }
    public String getNonghangSign() {
        return nonghangSign;
    }
    public void setNonghangSign(String nonghangSign) {
        this.nonghangSign = nonghangSign;
    }
}