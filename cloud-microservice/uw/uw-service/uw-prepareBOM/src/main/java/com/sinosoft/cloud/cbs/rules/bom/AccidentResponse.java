package com.sinosoft.cloud.cbs.rules.bom;

/**
 * Created by sll on 2019/3/28.
 * 中保信个人意外险返回信息
 */
public class AccidentResponse {
    /**
     * 多家公司承保提示：标记被保险人是否存在多家保险公司承保意外险，符合提示阈值>=4家，则显示：Y，不符合则显示：N.A.
     */
    private String multiCompany;
    /**
     * CI：为重疾理赔史标签，是标记被保险人在各家保险公司索赔重大疾病的历史，存在重疾理赔史则为：Y，不存在则为：N.A.如标签标记为Y，则永远为Y。
     */
    private String majorDiseasePayment;
    /**
     * PD：为伤残理赔史，标记被保险人在各家保险公司索赔伤残的历史，存在则显示：Y，不存在则显示：N.A. 如标签标记为Y，则永远为Y
     */
    private String disability;
    /**
     * 是否密集投保：标记被保险人是否在1月内投保有效意外险保单存在3家及以上保险公司的情况，存在则显示：Y，不存在显示：N.A.
     */
    private String dense;
    /**
     * 累计保额提示：标记被保险人有效意外险保单累计基本保额之和是否超过300万，达到300万及以上则显示：Y，否则显示：N.A.
     */
    private String accumulativeMoney;
    /**
     * 网页查询码（25位）：20170414 + 000019 + 5690432 + 01 + 03 日期（年月日）+ 保险公司机构编码 +0~9随机生成唯一的7位数的顺序码 + 数据产品大类编码 + 数据产品细类编码
     */
    private String pageQueryCode;
    /**
     * 险类
     */
    private String riskCode;
    /**
     * 是否有网页数据：根据三要素计算出来的被保险人接口标签风险提示信息，同时根据网页展示条件规则，判断该被保险人是否需要展示网页信
     * 息，展示网页显示：Y，不展示网页信息展示：N.A.
     */
    private String displayPage;

    public String getMultiCompany() {return multiCompany;}

    public void setMultiCompany(String multiCompany) {this.multiCompany = multiCompany;}

    public String getMajorDiseasePayment() {return majorDiseasePayment; }

    public void setMajorDiseasePayment(String majorDiseasePayment) {this.majorDiseasePayment = majorDiseasePayment;}

    public String getDisability() {return disability;}

    public void setDisability(String disability) {this.disability = disability;}

    public String getDense() { return dense;}

    public void setDense(String dense) { this.dense = dense;}

    public String getAccumulativeMoney() {return accumulativeMoney;}

    public void setAccumulativeMoney(String accumulativeMoney) {this.accumulativeMoney = accumulativeMoney;}

    public String getPageQueryCode() {return pageQueryCode;}

    public void setPageQueryCode(String pageQueryCode) {this.pageQueryCode = pageQueryCode;}

    public String getRiskCode() {return riskCode;}

    public void setRiskCode(String riskCode) {this.riskCode = riskCode; }

    public String getDisplayPage() {return displayPage;}

    public void setDisplayPage(String displayPage) {this.displayPage = displayPage;}

    @Override
    public String toString() {
        return "AccidentResponse{" +
                "multiCompany='" + multiCompany + '\'' +
                ", majorDiseasePayment='" + majorDiseasePayment + '\'' +
                ", disability='" + disability + '\'' +
                ", dense='" + dense + '\'' +
                ", accumulativeMoney='" + accumulativeMoney + '\'' +
                ", pageQueryCode='" + pageQueryCode + '\'' +
                ", riskCode='" + riskCode + '\'' +
                ", displayPage='" + displayPage + '\'' +
                '}';
    }
}
