package com.sinosoft.cloud.cbs.rules.bom;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 被保人信息
 * 
 * @author dingfan
 * 
 */
public class Insured {
	/**
	 * 姓名
	 */
	private String insuredName;

	/**
	 * 证件类型
	 */
	private String identityType;

	/**
	 * 证件号码
	 */
	private String identityCode;

	/**
	 * 性别
	 */
	private String sex;

	/**
	 * 出生日期
	 */
	private Date birthday;

	/**
	 * 年龄
	 */
	private int age;

	/**
	 * 身高
	 */
	private double height;

	/**
	 * 体重
	 */
	private double weight;

	/**
	 * VIP标记
	 */
	private boolean vipSign;

	/**
	 * 证件有效期
	 */
	private String identityValidityTerm;

	/**
	 * 国籍
	 */
	private String nationality;

	/**
	 * 户籍
	 */
	private String householdRegister;

	/**
	 * 学历
	 */
	private String education;

	/**
	 * 婚姻状况
	 */
	private String marrageStatus;

	/**
	 * 与第一被保人关系
	 */
	private String relationWithFirstInsured;

	/**
	 * 职业代码
	 */
	private String occuCode;

	/**
	 * 职业类别
	 */
	private String occuType;

	/**
	 * 职业描述
	 */
	private String occuDescribe;

	/**
	 * 兼职
	 */
	private String partTimeJobFlag;

	/**
	 * 驾照类型
	 */
	private String licenceType;

	/**
	 * 地址代码
	 */
	private String addrCode;

	/**
	 * 年收入
	 */
	private double annualIncome;

	/**
	 * 家庭年收入
	 */
	private double familyAnnualIncome;

	/**
	 * 主要收入来源
	 */
	private String incomeSource;

	/**
	 * BMI
	 */
	private String BMI;

	/**
	 * 黑名单标记
	 */
	private boolean blackSign;

	/**
	 * 累计住院医疗风险保额
	 */
	private double sumHospitalizeAmount;

	/**
	 * 累计意外医疗风险保额
	 */
	private double sumAccMedicalAmount;

	/**
	 * 累计津贴保额
	 */
	private double sumAcAmount;

	/**
	 * 累计寿险风险保额
	 */
	private double sumLifeAmount;

	/**
	 * 寿险风险保额
	 */
	private double lifeAmount;

	/**
	 * 累计意外险风险保额
	 */
	private double sumAccidentAmount;

	/**
	 * 意外险风险保额
	 */
	private double accidentAmount;

	/**
	 * 累计重疾风险保额
	 */
	private double sumDiseaseAmount;

	/**
	 * 重疾险风险保额1
	 */
	private double diseaseAmount;

	/**
	 * 累计自驾车风险保额
	 */
	private double sumDriverAmount;

	/**
	 * 累计防癌险风险保额
	 */
	private double sumAnticancerAmount;

	/**
	 * 累计公共交通意外风险保额
	 */
	private double sumBusAccAmount;

	/**
	 * 公共交通意外风险保额
	 */
	private double busAccAmount;

	/**
	 * 累计航空意外风险保额
	 */
	private double sumAviationAmount;

	/**
	 * 航空意外风险保额
	 */
	private double aviationAmount;

	/**
	 * 累计人身险保额
	 */
	private double sumPersonAmount;

	/**
	 * 累计风险保额
	 */
	private double sumAmount;

	/**
	 * 累计寿险保单保额
	 */
	private double sumLifePyAmount;

	/**
	 * 寿险保单保额
	 */
	private double lifePyAmount;

	/**
	 * 累计健康险保单保额
	 */
	private double sumHealthPyAmount;

	/**
	 * 累计意外险保单保额
	 */
	private double sumAccPyAmount;

	/**
	 * 意外险保单保额
	 */
	private double accPyAmount;

	/**
	 * 累计重疾险保单保额
	 */
	private double sumDisPyAmount;

	/**
	 * 累计自驾车保单保额
	 */
	private double sumDriverPyAmount;

	/**
	 * 累计防癌险保单保额
	 */
	private double sumAnticPyAmount;

	/**
	 * 未成年人身故保额
	 */
	private double nonageDieAmount;

	/**
	 * 公共交通意外保单保额
	 */
	private double busAccPyAmount;

	/**
	 * 航空意外保单保额
	 */
	private double aviationPyAmount;

	/**
	 * 英文姓名
	 */
	private String englishName;

	/**
	 * 既往有体检、加费、特约的保单
	 */
	private boolean haveExaminePolicy;

	/**
	 * 既往有理赔记录
	 */
	private boolean haveClaims;

	/**
	 * 既往有保全二核
	 */
	private boolean havePreserve;

	/**
	 * 既往有理赔二核
	 */
	private boolean havaPayFor;

	/**
	 * 既往有生调记录
	 */
	private boolean haveAscertain;

	/**
	 * 既往有延期、拒保的保单
	 */
	private boolean haveDeferPolicy;

	/**
	 * 既往有投保经历
	 */
	private boolean haveApptHistory;

	/**
	 * 既往有次标准体记录
	 */
	private boolean haveSubExamine;

	/**
	 * 既往有体检未完成记录
	 */
	private boolean haveUnfiExamine;

	/**
	 * 既往投保健康告知异常
	 */
	private boolean haveHealAbnormal;

	/**
	 * 既往保单有体检回复记录
	 */
	private boolean haveExamineReply;

	/**
	 * 既往有问题件未回复撤单
	 */
	private boolean haveProb;

	/**
	 * 既往投保职业代码与本次一致
	 */
	private boolean haveSameOccCode;

	/**
	 * 既往告知为五、六类或拒保职业
	 */
	private boolean haveRejectionOcc;

	/**
	 * 职业类别低于既往告知
	 */
	private boolean insJobTypeThaBefo;

	/**
	 * 健康声明
	 */
	private String healthDeclare;

	/**
	 * 常住地址是否异地
	 */
	private boolean remoteHome;

	/**
	 * 通讯地址是否异地
	 */
	private boolean remotePostal;

	/**
	 * 职务
	 */
	private String duties;

	/**
	 * 被保险人客户号
	 */
	private String clientNo;

	/**
	 * 历史险种列表
	 */
	private List historyRiskList;

	/**
	 * 与投保人关系
	 */
	private String relationWithAppnt;

	/**
	 * 是否为孕妇
	 */
	private boolean pregSign;

	/**
	 * 孕周
	 */
	private int pregWeek;

	/**
	 * 是否为残疾人
	 */
	/*private boolean disabledSign;*/

	/**
	 * 累计寿险体检风险保额
	 */
	private double sumLifePhyAmount;

	/**
	 * 是否有摩托车驾照
	 */
/*	private boolean motorLicence;*/

	/**
	 * 累计再保风险保额
	 */
	private double sumReinsuranceAmount;

	/**
	 * 身高异常
	 */
	private boolean heightErr;

	/**
	 * 体重异常
	 */
	private boolean weightErr;

	/**
	 * BMI异常
	 */
	private boolean bmiErr;

	/**
	 * 孕周异常
	 */
	private boolean pregWeekErr;

	/**
	 *
	 * 累计6009险种意外风险保额
	 */
	private double sum6009AccidentAmount;

	/**
	 *
	 * 万能险期交保费之和
	 */

	private double sumStandPrem;

	/**
	 * 电销重疾保额
	 */
/*	private double healthByElectricPin;*/

	/**
	 * 电销非c012组合险份数
	 */
/*	private double insuredDouble1;*/

	/**
	 * 电销非c009组合险份数
	 */
/*	private double insuredDouble2;*/
	/**
	 * 电销非c014组合险份数
	 */
/*	private double insuredDouble3;*/

	/**
	 * 爱永远定期寿险保费
	 */
	private String insuredString1;

	/**
	 * 预留字段4
	 */
	private String insuredString2;

	/**
	 * 白名单标记
	 */
	private boolean whiteSign;

	/**
	 * 白名单限额
	 */
	private double whiteSignAmnt;

	/**
	 * 被保人在团险被保险人团险渠道长期重疾险(7829)有拒保、延期记录的标识
	 */
	private boolean refuseRecord7829;

	/**
	 * 被保险人团险渠道长期重疾险(7829)有次标准体记录
	 */
	private boolean subStandardRecode7829;

	/**
	 * 被保人其他保险公司保额
	 */
	private double otherAmnt;


	/**
	 * 黑名单1-证件号与和黑名单相同,不分国籍,>0触发,参数:一个contno
	 * {this} 的证件号与和黑名单相同
	 */
	private boolean insBlackIdFlag;


	/**
	 * 黑名单2-中国客户名字、生日与黑名单相同,>0触发,参数:一个contno
	 * {this}为中国客户且名字和生日与黑名单相同
	 */
	private boolean insBlackChnFlag;

	/**
	 * 黑名单3-被保人名1部分组成,被保人名与黑名单表四个名字对比,>0触发,参数:4个contno
	 * {this}的名字名与黑名单名字和生日相同(名字为一部分)
	 */
	private boolean insBlackFlag1;


	/**
	 * 黑名单4-被保人名2部分组成,被保人名与黑名单表2个名字对比,>0触发,参数:2个contno
	 * 有一个name相同，且出生日期相同
	 * {this}的名字与黑名单名字和生日相同(名字为两部分)
	 */
	private boolean insBlackFlag2;

	/**
	 * 黑名单5-被保人名2部分组成,被保人名与黑名单表2个名字对比,>=2触发,参数:2个contno
	 * firstname和surnname都要相同.无生日限制
	 * {this} 的名字与黑名单名字和生日相同(名字为两部分,无生日限制)
	 */
	private boolean insBlackFlag3;

	/**
	 * 黑名单6-被保人名4部分组成,被保人名与黑名单表3个名字对比,>=0触发,参数:3个contno
	 * firstname,middlename,surname,有生日限制
	 * {this} 的名字与黑名单名字两个以上相同(名字为四部分)
	 */
	private boolean insBlackFlag4;

	/**
	 * 黑名单7-被保人名4部分组成,被保人名与黑名单表3个名字对比,>=2触发,参数:3个contno
	 * firstname,middlename,surname,无生日限制
	 * {this} 的名字与黑名单相同(被保人名三部分构成无生日限制)
	 */
	private boolean insBlackFlag5;


//	同业拒保次数
	private boolean declineFlag;

	//同业投保次数
	private boolean applicationFlag;

//	防癌或重疾险理赔记录
	private boolean cancerClaimRecord;

	//被保险人累计投保7059险种保额
	private double basicAmnt7059;

//	 被保人意外险理赔记录
	private boolean accidentClaimRecord;

//	被保人7057险种累计基本保额
	private double basicAmnt7057;


	//被保人7058险种累计基本保额
	private double basicAmnt7058;


//	被保人1030险种累计基本保额
	private double basikAmnt1030;

//	被保人2048险种累计基本保额
	private double basikAmnt2048;


//	被保人2048险种累计保费
	private double accPrem2048;

//	被保险人是否已拥有7060或7061的有效保单 2018/3/27 zfx
	private boolean have7060Or7061;

//	被保险人既往有非团险账户的医疗险种的理赔记录 2018/3/27 zfx
	//private boolean haveClaimsNotMedic;


	private Map extendMap;

	/**
	 * 被保险人C060保险计划累计保额   --wpq--
	 */
    private double c060AccAmnt;

	/**
	 * 被保险人累计C060份数  --wpq--
	 */
	private double c060Copies;

	/**
	 * 被保险人投保7053累计重疾险风险保额 --wpq--
	 */
	private double accAmnt7053;

	/**
	 *BMI  --wpq--
	 */
	private double bmiValue;
	/**
	 * 5018税优健康险投保份数  --wpq--
	 */
	private int copies5018;

	/**
	 * 非工作单位组织的个人投保标记  --wpq--
	 */
    private  boolean personalInsurance;

	/**
	 * 工作单位首次或再次组织的投保标记  --wpq--
	 */
	private String organizationInsurance;

	/**
	 * 备注栏有内容  --wpq--
	 */
	/*private boolean noteColumn;*/

	/**
	 * 各期保费（首期+各续期）之和  05/31--xzh--
	 */
	private double renewalPremSumC066;

	/**
	 * C070保险计划累计保额  05/31--xzh--
	 */
	private double maximumGrant;
	/**
	 * 被保险人累计C070份数  05/31--xzh--
	 */
	private int c070Copies;

	/**
	 * 被保人意外医疗险累计理赔结案给付金额   06/19--xzh--
	 */
	private double accidentMedicPay;
	/**
	 * 被保人健医卡理赔给付金额    06/19--xzh--
	 */
	private double helthMedicPay;
	/**
	 * 被保人非健医卡和意外医疗险的理赔记录  06/19--xzh--
	 */
	private boolean claimsNoMedic;
	/**
	 * 财富世嘉终身寿险保险累计基本保险金额  07/20--xzh--
	 */
	private double sumBasicInsuranceAmount1040;

	/**
	 * 1040 险种各期保费之和  07/20--xzh--
	 */
	private double renewalPremSum1040;

	/**
	 * 2048 险种各期保费之和  07/30--xzh--
	 */
	private double renewalPremSum2048;

	/**
	 * C067首期+续期保费之和  08/03--xzh--
	 */
	private double renewalPremSumC067;

	/**
	 * C068首期+续期保费之和  08/03--xzh--
	 */
	private double renewalPremSumC068;

	/**
	 * 既往有未结案的意外医疗理赔记录  08/29--xzh--
	 */
	private boolean acMedicClaimNotEnd;

	/**
	 * 被保险人既往有"被'达标体检、抽样体检'自核规则拦截且该保单状态为待人工核保"的保单  08/29--xzh--
	 */
	private boolean haveHelthRule;

	/**
	 * 被保险人累计“2052险种”保费  10/09--xzh--
	 */
	private double accPrem2052;

	/**
	 *被保险人2052险种累计期交保费 11/05--xzh--
	 */
	private double sumStandPrem2052;
	/**
	 *被保险人7060有理赔二核不自动续保结论 12/24--sll--
	 */
	private boolean havePayFor7060;
	/**
	 *被保险人7061有理赔二核不自动续保结论 12/24--sll--
	 */
	private boolean	havePayFor7061;


	/**
	 *被保险人2053险种累计期交保费 12/12--xzh--
	 */
	private double sumStandPrem2053;

	/**
	 *累计当日投保保单保险费（首期+续期）---xzh---
	 */
	private double sumEffectivePolicy;

	/**
	 *当日投保保单有无现金支付---xzh---
	 */
	//private boolean havePaymentCash;
	/**
	 *上一保单年度有拒保、延期记录(各渠道)
	 */
	private boolean haveDeferPolicyLY;

	/**
	 *上一保单年度有次标准体承保记录(各渠道)
	 */
	private boolean haveSubExamineLY;

	/**
	 *上一保单年度有理赔记录(除建医卡、小额医疗、7061理赔二核为标准承保)
	 */
	private boolean haveClaims7061Specil;

	/**
	 *上一保单年度有理赔记录(除建医卡\小额医疗\7060理赔二核为标准承保的理赔)
	 */
	private  boolean haveClaims7060Specil;



	/**
	 *1043 险种各期保费之和 ---xzh---
	 */
	private double renewalPremSum1043;

	/**
	 * 中保信意外险返回结果
	 * @return
	 */
	private AccidentResponse accidentResponse;

	/**
	 * 中保信重疾险返回结果
	 * @return
	 */
	private MajorDiseaseCheck majorDiseaseCheck;



	public AccidentResponse getAccidentResponse() {return accidentResponse;}

	public void setAccidentResponse(AccidentResponse accidentResponse) {this.accidentResponse = accidentResponse;}

	public MajorDiseaseCheck getMajorDiseaseCheck() {return majorDiseaseCheck;}

	public void setMajorDiseaseCheck(MajorDiseaseCheck majorDiseaseCheck) {this.majorDiseaseCheck = majorDiseaseCheck;}

	public double getRenewalPremSum1043() {
		return renewalPremSum1043;
	}

	public void setRenewalPremSum1043(double renewalPremSum1043) {
		this.renewalPremSum1043 = renewalPremSum1043;
	}

	public double getSumEffectivePolicy() {
		return sumEffectivePolicy;
	}

	public void setSumEffectivePolicy(double sumEffectivePolicy) {
		this.sumEffectivePolicy = sumEffectivePolicy;
	}

/*
	public boolean isHavePaymentCash() {
		return havePaymentCash;
	}

	public void setHavePaymentCash(boolean havePaymentCash) {
		this.havePaymentCash = havePaymentCash;
	}
*/

	public double getSumStandPrem2053() {
		return sumStandPrem2053;
	}

	public void setSumStandPrem2053(double sumStandPrem2053) {
		this.sumStandPrem2053 = sumStandPrem2053;
	}

	public double getSumStandPrem2052() {
		return sumStandPrem2052;
	}

	public void setSumStandPrem2052(double sumStandPrem2052) {
		this.sumStandPrem2052 = sumStandPrem2052;
	}


	public double getAccPrem2052() {
		return accPrem2052;
	}

	public void setAccPrem2052(double accPrem2052) {
		this.accPrem2052 = accPrem2052;
	}

	public boolean isHaveHelthRule() {
		return haveHelthRule;
	}

	public void setHaveHelthRule(boolean haveHelthRule) {
		this.haveHelthRule = haveHelthRule;
	}


	public boolean isAcMedicClaimNotEnd() {
		return acMedicClaimNotEnd;
	}

	public void setAcMedicClaimNotEnd(boolean acMedicClaimNotEnd) {
		this.acMedicClaimNotEnd = acMedicClaimNotEnd;
	}

	public double getRenewalPremSumC068() {
		return renewalPremSumC068;
	}

	public void setRenewalPremSumC068(double renewalPremSumC068) {
		this.renewalPremSumC068 = renewalPremSumC068;
	}

	public double getRenewalPremSumC067() {
		return renewalPremSumC067;
	}

	public void setRenewalPremSumC067(double renewalPremSumC067) {
		this.renewalPremSumC067 = renewalPremSumC067;
	}

	public double getRenewalPremSum2048() {
		return renewalPremSum2048;
	}

	public void setRenewalPremSum2048(double renewalPremSum2048) {
		this.renewalPremSum2048 = renewalPremSum2048;
	}

	public double getSumBasicInsuranceAmount1040() {
		return sumBasicInsuranceAmount1040;
	}

	public void setSumBasicInsuranceAmount1040(double sumBasicInsuranceAmount1040) {
		this.sumBasicInsuranceAmount1040 = sumBasicInsuranceAmount1040;
	}

	public double getRenewalPremSum1040() {
		return renewalPremSum1040;
	}

	public void setRenewalPremSum1040(double renewalPremSum1040) {
		this.renewalPremSum1040 = renewalPremSum1040;
	}
	public double getAccidentMedicPay() {
		return accidentMedicPay;
	}

	public void setAccidentMedicPay(double accidentMedicPay) {
		this.accidentMedicPay = accidentMedicPay;
	}

	public double getHelthMedicPay() {
		return helthMedicPay;
	}

	public void setHelthMedicPay(double helthMedicPay) {
		this.helthMedicPay = helthMedicPay;
	}

	public boolean isClaimsNoMedic() {
		return claimsNoMedic;
	}

	public void setClaimsNoMedic(boolean claimsNoMedic) {
		this.claimsNoMedic = claimsNoMedic;
	}


	public double getMaximumGrant() {
		return maximumGrant;
	}

	public void setMaximumGrant(double maximumGrant) {
		this.maximumGrant = maximumGrant;
	}

	public int getC070Copies() {
		return c070Copies;
	}

	public void setC070Copies(int c070Copies) {
		this.c070Copies = c070Copies;
	}

	public double getRenewalPremSumC066() {
		return renewalPremSumC066;
	}

	public void setRenewalPremSumC066(double renewalPremSumC066) {
		this.renewalPremSumC066 = renewalPremSumC066;
	}

	public boolean isHave7060Or7061() {
		return have7060Or7061;
	}

	public void setHave7060Or7061(boolean have7060Or7061) {
		this.have7060Or7061 = have7060Or7061;
	}

	public int getCopies5018() {
		return copies5018;
	}

	public void setCopies5018(int copies5018) {
		this.copies5018 = copies5018;
	}

	public boolean isPersonalInsurance() {
		return personalInsurance;
	}

	public void setPersonalInsurance(boolean personalInsurance) {
		this.personalInsurance = personalInsurance;
	}

	public String getOrganizationInsurance() {
		return organizationInsurance;
	}

	public void setOrganizationInsurance(String organizationInsurance) {
		this.organizationInsurance = organizationInsurance;
	}

/*	public boolean isNoteColumn() {
		return noteColumn;
	}

	public void setNoteColumn(boolean noteColumn) {
		this.noteColumn = noteColumn;
	}*/

	public double getBmiValue() {
		return bmiValue;
	}

	public void setBmiValue(double bmiValue) {
		this.bmiValue = bmiValue;
	}

	public double getAccAmnt7053() {
		return accAmnt7053;
	}

	public void setAccAmnt7053(double accAmnt7053) {
		this.accAmnt7053 = accAmnt7053;
	}

	public double getC060AccAmnt() {
		return c060AccAmnt;
	}

	public void setC060AccAmnt(double c060AccAmnt) {
		c060AccAmnt = c060AccAmnt;
	}

	public double getC060Copies() {
		return c060Copies;
	}

	public void setC060Copies(double c060Copies) {
		c060Copies = c060Copies;
	}

	public Insured() {
		super();
	}

	/*public boolean isHaveClaimsNotMedic() {
		return haveClaimsNotMedic;
	}

	public void setHaveClaimsNotMedic(boolean haveClaimsNotMedic) {
		this.haveClaimsNotMedic = haveClaimsNotMedic;
	}

*/

	public double getAccPrem2048() {
		return accPrem2048;
	}

	public void setAccPrem2048(double accPrem2048) {
		this.accPrem2048 = accPrem2048;
	}

	public double getBasikAmnt2048() {
		return basikAmnt2048;
	}

	public void setBasikAmnt2048(double basikAmnt2048) {
		this.basikAmnt2048 = basikAmnt2048;
	}

	public double getBasikAmnt1030() {
		return basikAmnt1030;
	}

	public void setBasikAmnt1030(double basikAmnt1030) {
		this.basikAmnt1030 = basikAmnt1030;
	}








	public double getBasicAmnt7057() {
		return basicAmnt7057;
	}





	public void setBasicAmnt7057(double basicAmnt7057) {
		this.basicAmnt7057 = basicAmnt7057;
	}





	public double getBasicAmnt7058() {
		return basicAmnt7058;
	}





	public void setBasicAmnt7058(double basicAmnt7058) {
		this.basicAmnt7058 = basicAmnt7058;
	}





	public boolean isAccidentClaimRecord() {
		return accidentClaimRecord;
	}





	public void setAccidentClaimRecord(boolean accidentClaimRecord) {
		this.accidentClaimRecord = accidentClaimRecord;
	}





	public double getBasicAmnt7059() {
		return basicAmnt7059;
	}





	public void setBasicAmnt7059(double basicAmnt7059) {
		this.basicAmnt7059 = basicAmnt7059;
	}





	public boolean isCancerClaimRecord() {
		return cancerClaimRecord;
	}





	public void setCancerClaimRecord(boolean cancerClaimRecord) {
		this.cancerClaimRecord = cancerClaimRecord;
	}





	public boolean isApplicationFlag() {
		return applicationFlag;
	}




	public void setApplicationFlag(boolean applicationFlag) {
		this.applicationFlag = applicationFlag;
	}




	public boolean isDeclineFlag() {
		return declineFlag;
	}




	public void setDeclineFlag(boolean declineFlag) {
		this.declineFlag = declineFlag;
	}




	public boolean isInsBlackChnFlag() {
		return insBlackChnFlag;
	}




	public void setInsBlackChnFlag(boolean insBlackChnFlag) {
		this.insBlackChnFlag = insBlackChnFlag;
	}




	public boolean isInsBlackFlag1() {
		return insBlackFlag1;
	}




	public void setInsBlackFlag1(boolean insBlackFlag1) {
		this.insBlackFlag1 = insBlackFlag1;
	}




	public boolean isInsBlackFlag2() {
		return insBlackFlag2;
	}




	public void setInsBlackFlag2(boolean insBlackFlag2) {
		this.insBlackFlag2 = insBlackFlag2;
	}




	public boolean isInsBlackFlag3() {
		return insBlackFlag3;
	}




	public void setInsBlackFlag3(boolean insBlackFlag3) {
		this.insBlackFlag3 = insBlackFlag3;
	}




	public boolean isInsBlackFlag4() {
		return insBlackFlag4;
	}




	public void setInsBlackFlag4(boolean insBlackFlag4) {
		this.insBlackFlag4 = insBlackFlag4;
	}




	public boolean isInsBlackFlag5() {
		return insBlackFlag5;
	}




	public void setInsBlackFlag5(boolean insBlackFlag5) {
		this.insBlackFlag5 = insBlackFlag5;
	}




	public boolean isInsBlackIdFlag() {
		return insBlackIdFlag;
	}




	public void setInsBlackIdFlag(boolean insBlackIdFlag) {
		this.insBlackIdFlag = insBlackIdFlag;
	}




	public double getOtherAmnt() {
		return otherAmnt;
	}



	public void setOtherAmnt(double otherAmnt) {
		this.otherAmnt = otherAmnt;
	}



	public boolean isRefuseRecord7829() {
		return refuseRecord7829;
	}


	public void setRefuseRecord7829(boolean refuseRecord7829) {
		this.refuseRecord7829 = refuseRecord7829;
	}




	public boolean isSubStandardRecode7829() {
		return subStandardRecode7829;
	}


	public void setSubStandardRecode7829(boolean subStandardRecode7829) {
		this.subStandardRecode7829 = subStandardRecode7829;
	}



	public String getInsuredName() {
		return insuredName;
	}

	public void setInsuredName(String insuredName) {
		this.insuredName = insuredName;
	}

	public String getIdentityType() {
		return identityType;
	}

	public void setIdentityType(String identityType) {
		this.identityType = identityType;
	}

	public String getIdentityCode() {
		return identityCode;
	}

	public void setIdentityCode(String identityCode) {
		this.identityCode = identityCode;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public Date getBirthday() {
		return birthday;
	}

	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public double getHeight() {
		return height;
	}

	public void setHeight(double height) {
		this.height = height;
	}

	public double getWeight() {
		return weight;
	}

	public void setWeight(double weight) {
		this.weight = weight;
	}

	public boolean isVipSign() {
		return vipSign;
	}

	public void setVipSign(boolean vipSign) {
		this.vipSign = vipSign;
	}

	public String getIdentityValidityTerm() {
		return identityValidityTerm;
	}

	public void setIdentityValidityTerm(String identityValidityTerm) {
		this.identityValidityTerm = identityValidityTerm;
	}

	public String getNationality() {
		return nationality;
	}

	public void setNationality(String nationality) {
		this.nationality = nationality;
	}

	public String getHouseholdRegister() {
		return householdRegister;
	}

	public void setHouseholdRegister(String householdRegister) {
		this.householdRegister = householdRegister;
	}

	public String getEducation() {
		return education;
	}

	public void setEducation(String education) {
		this.education = education;
	}

	public String getMarrageStatus() {
		return marrageStatus;
	}

	public void setMarrageStatus(String marrageStatus) {
		this.marrageStatus = marrageStatus;
	}

	public String getRelationWithFirstInsured() {
		return relationWithFirstInsured;
	}

	public void setRelationWithFirstInsured(String relationWithFirstInsured) {
		this.relationWithFirstInsured = relationWithFirstInsured;
	}

	public String getOccuCode() {
		return occuCode;
	}

	public void setOccuCode(String occuCode) {
		this.occuCode = occuCode;
	}

	public String getOccuType() {
		return occuType;
	}

	public void setOccuType(String occuType) {
		this.occuType = occuType;
	}

	public String getOccuDescribe() {
		return occuDescribe;
	}

	public void setOccuDescribe(String occuDescribe) {
		this.occuDescribe = occuDescribe;
	}

	public String getPartTimeJobFlag() {
		return partTimeJobFlag;
	}

	public void setPartTimeJobFlag(String partTimeJobFlag) {
		this.partTimeJobFlag = partTimeJobFlag;
	}

	public String getLicenceType() {
		return licenceType;
	}

	public void setLicenceType(String licenceType) {
		this.licenceType = licenceType;
	}

	public String getAddrCode() {
		return addrCode;
	}

	public void setAddrCode(String addrCode) {
		this.addrCode = addrCode;
	}

	public double getAnnualIncome() {
		return annualIncome;
	}

	public void setAnnualIncome(double annualIncome) {
		this.annualIncome = annualIncome;
	}

	public double getFamilyAnnualIncome() {
		return familyAnnualIncome;
	}

	public void setFamilyAnnualIncome(double familyAnnualIncome) {
		this.familyAnnualIncome = familyAnnualIncome;
	}

	public String getIncomeSource() {
		return incomeSource;
	}

	public void setIncomeSource(String incomeSource) {
		this.incomeSource = incomeSource;
	}

	public String getBMI() {
		return BMI;
	}

	public void setBMI(String bMI) {
		BMI = bMI;
	}

	public boolean isBlackSign() {
		return blackSign;
	}

	public void setBlackSign(boolean blackSign) {
		this.blackSign = blackSign;
	}

	public double getSumHospitalizeAmount() {
		return sumHospitalizeAmount;
	}

	public void setSumHospitalizeAmount(double sumHospitalizeAmount) {
		this.sumHospitalizeAmount = sumHospitalizeAmount;
	}

	public double getSumAccMedicalAmount() {
		return sumAccMedicalAmount;
	}

	public void setSumAccMedicalAmount(double sumAccMedicalAmount) {
		this.sumAccMedicalAmount = sumAccMedicalAmount;
	}

	public double getSumAcAmount() {
		return sumAcAmount;
	}

	public void setSumAcAmount(double sumAcAmount) {
		this.sumAcAmount = sumAcAmount;
	}

	public double getSumLifeAmount() {
		return sumLifeAmount;
	}

	public void setSumLifeAmount(double sumLifeAmount) {
		this.sumLifeAmount = sumLifeAmount;
	}

	public double getLifeAmount() {
		return lifeAmount;
	}

	public void setLifeAmount(double lifeAmount) {
		this.lifeAmount = lifeAmount;
	}

	public double getSumAccidentAmount() {
		return sumAccidentAmount;
	}

	public void setSumAccidentAmount(double sumAccidentAmount) {
		this.sumAccidentAmount = sumAccidentAmount;
	}

	public double getAccidentAmount() {
		return accidentAmount;
	}

	public void setAccidentAmount(double accidentAmount) {
		this.accidentAmount = accidentAmount;
	}

	public double getSumDiseaseAmount() {
		return sumDiseaseAmount;
	}

	public void setSumDiseaseAmount(double sumDiseaseAmount) {
		this.sumDiseaseAmount = sumDiseaseAmount;
	}

	public double getDiseaseAmount() {
		return diseaseAmount;
	}

	public void setDiseaseAmount(double diseaseAmount) {
		this.diseaseAmount = diseaseAmount;
	}

	public double getSumDriverAmount() {
		return sumDriverAmount;
	}

	public void setSumDriverAmount(double sumDriverAmount) {
		this.sumDriverAmount = sumDriverAmount;
	}

	public double getSumAnticancerAmount() {
		return sumAnticancerAmount;
	}

	public void setSumAnticancerAmount(double sumAnticancerAmount) {
		this.sumAnticancerAmount = sumAnticancerAmount;
	}

	public double getSumBusAccAmount() {
		return sumBusAccAmount;
	}

	public void setSumBusAccAmount(double sumBusAccAmount) {
		this.sumBusAccAmount = sumBusAccAmount;
	}

	public double getBusAccAmount() {
		return busAccAmount;
	}

	public void setBusAccAmount(double busAccAmount) {
		this.busAccAmount = busAccAmount;
	}

	public double getSumAviationAmount() {
		return sumAviationAmount;
	}

	public void setSumAviationAmount(double sumAviationAmount) {
		this.sumAviationAmount = sumAviationAmount;
	}

	public double getAviationAmount() {
		return aviationAmount;
	}

	public void setAviationAmount(double aviationAmount) {
		this.aviationAmount = aviationAmount;
	}

	public double getSumAmount() {
		return sumAmount;
	}

	public void setSumAmount(double sumAmount) {
		this.sumAmount = sumAmount;
	}

	public double getSumLifePyAmount() {
		return sumLifePyAmount;
	}

	public void setSumLifePyAmount(double sumLifePyAmount) {
		this.sumLifePyAmount = sumLifePyAmount;
	}

	public double getLifePyAmount() {
		return lifePyAmount;
	}

	public void setLifePyAmount(double lifePyAmount) {
		this.lifePyAmount = lifePyAmount;
	}

	public double getSumHealthPyAmount() {
		return sumHealthPyAmount;
	}

	public void setSumHealthPyAmount(double sumHealthPyAmount) {
		this.sumHealthPyAmount = sumHealthPyAmount;
	}

	public double getSumAccPyAmount() {
		return sumAccPyAmount;
	}

	public void setSumAccPyAmount(double sumAccPyAmount) {
		this.sumAccPyAmount = sumAccPyAmount;
	}

	public double getAccPyAmount() {
		return accPyAmount;
	}

	public void setAccPyAmount(double accPyAmount) {
		this.accPyAmount = accPyAmount;
	}

	public double getSumDisPyAmount() {
		return sumDisPyAmount;
	}

	public void setSumDisPyAmount(double sumDisPyAmount) {
		this.sumDisPyAmount = sumDisPyAmount;
	}

	public double getSumDriverPyAmount() {
		return sumDriverPyAmount;
	}

	public void setSumDriverPyAmount(double sumDriverPyAmount) {
		this.sumDriverPyAmount = sumDriverPyAmount;
	}

	public double getSumAnticPyAmount() {
		return sumAnticPyAmount;
	}

	public void setSumAnticPyAmount(double sumAnticPyAmount) {
		this.sumAnticPyAmount = sumAnticPyAmount;
	}

	public double getNonageDieAmount() {
		return nonageDieAmount;
	}

	public void setNonageDieAmount(double nonageDieAmount) {
		this.nonageDieAmount = nonageDieAmount;
	}

	public double getBusAccPyAmount() {
		return busAccPyAmount;
	}

	public void setBusAccPyAmount(double busAccPyAmount) {
		this.busAccPyAmount = busAccPyAmount;
	}

	public double getAviationPyAmount() {
		return aviationPyAmount;
	}

	public void setAviationPyAmount(double aviationPyAmount) {
		this.aviationPyAmount = aviationPyAmount;
	}

	public String getEnglishName() {
		return englishName;
	}

	public void setEnglishName(String englishName) {
		this.englishName = englishName;
	}

	public boolean isHaveExaminePolicy() {
		return haveExaminePolicy;
	}

	public void setHaveExaminePolicy(boolean haveExaminePolicy) {
		this.haveExaminePolicy = haveExaminePolicy;
	}

	public boolean isHaveClaims() {
		return haveClaims;
	}

	public void setHaveClaims(boolean haveClaims) {
		this.haveClaims = haveClaims;
	}

	public boolean isHavePreserve() {
		return havePreserve;
	}

	public void setHavePreserve(boolean havePreserve) {
		this.havePreserve = havePreserve;
	}

	public boolean isHavaPayFor() {
		return havaPayFor;
	}

	public void setHavaPayFor(boolean havaPayFor) {
		this.havaPayFor = havaPayFor;
	}

	public boolean isHaveAscertain() {
		return haveAscertain;
	}

	public void setHaveAscertain(boolean haveAscertain) {
		this.haveAscertain = haveAscertain;
	}

	public boolean isHaveDeferPolicy() {
		return haveDeferPolicy;
	}

	public void setHaveDeferPolicy(boolean haveDeferPolicy) {
		this.haveDeferPolicy = haveDeferPolicy;
	}

	public String getHealthDeclare() {
		return healthDeclare;
	}

	public void setHealthDeclare(String healthDeclare) {
		this.healthDeclare = healthDeclare;
	}

	public boolean isRemoteHome() {
		return remoteHome;
	}

	public void setRemoteHome(boolean remoteHome) {
		this.remoteHome = remoteHome;
	}

	public boolean isRemotePostal() {
		return remotePostal;
	}

	public void setRemotePostal(boolean remotePostal) {
		this.remotePostal = remotePostal;
	}

	public String getDuties() {
		return duties;
	}

	public void setDuties(String duties) {
		this.duties = duties;
	}

	public String getClientNo() {
		return clientNo;
	}

	public void setClientNo(String clientNo) {
		this.clientNo = clientNo;
	}

	public List getHistoryRiskList() {
		return historyRiskList;
	}

	public void setHistoryRiskList(List historyRiskList) {
		this.historyRiskList = historyRiskList;
	}

	public String getRelationWithAppnt() {
		return relationWithAppnt;
	}

	public void setRelationWithAppnt(String relationWithAppnt) {
		this.relationWithAppnt = relationWithAppnt;
	}

/*	public boolean isDisabledSign() {
		return disabledSign;
	}

	public void setDisabledSign(boolean disabledSign) {
		this.disabledSign = disabledSign;
	}*/

	public boolean isPregSign() {
		return pregSign;
	}

	public void setPregSign(boolean pregSign) {
		this.pregSign = pregSign;
	}

	public int getPregWeek() {
		return pregWeek;
	}

	public void setPregWeek(int pregWeek) {
		this.pregWeek = pregWeek;
	}

	public double getSumLifePhyAmount() {
		return sumLifePhyAmount;
	}

	public void setSumLifePhyAmount(double sumLifePhyAmount) {
		this.sumLifePhyAmount = sumLifePhyAmount;
	}

/*	public boolean isMotorLicence() {
		return motorLicence;
	}

	public void setMotorLicence(boolean motorLicence) {
		this.motorLicence = motorLicence;
	}*/

	public double getSumReinsuranceAmount() {
		return sumReinsuranceAmount;
	}

	public void setSumReinsuranceAmount(double sumReinsuranceAmount) {
		this.sumReinsuranceAmount = sumReinsuranceAmount;
	}

	public boolean isBmiErr() {
		return bmiErr;
	}

	public void setBmiErr(boolean bmiErr) {
		this.bmiErr = bmiErr;
	}

	public boolean isHaveApptHistory() {
		return haveApptHistory;
	}

	public void setHaveApptHistory(boolean haveApptHistory) {
		this.haveApptHistory = haveApptHistory;
	}

	public boolean isHaveExamineReply() {
		return haveExamineReply;
	}

	public void setHaveExamineReply(boolean haveExamineReply) {
		this.haveExamineReply = haveExamineReply;
	}

	public boolean isHaveHealAbnormal() {
		return haveHealAbnormal;
	}

	public void setHaveHealAbnormal(boolean haveHealAbnormal) {
		this.haveHealAbnormal = haveHealAbnormal;
	}

	public boolean isHaveProb() {
		return haveProb;
	}

	public void setHaveProb(boolean haveProb) {
		this.haveProb = haveProb;
	}

	public boolean isHaveRejectionOcc() {
		return haveRejectionOcc;
	}

	public void setHaveRejectionOcc(boolean haveRejectionOcc) {
		this.haveRejectionOcc = haveRejectionOcc;
	}

	public boolean isHaveSameOccCode() {
		return haveSameOccCode;
	}

	public void setHaveSameOccCode(boolean haveSameOccCode) {
		this.haveSameOccCode = haveSameOccCode;
	}

	public boolean isHaveSubExamine() {
		return haveSubExamine;
	}

	public void setHaveSubExamine(boolean haveSubExamine) {
		this.haveSubExamine = haveSubExamine;
	}

	public boolean isHaveUnfiExamine() {
		return haveUnfiExamine;
	}

	public void setHaveUnfiExamine(boolean haveUnfiExamine) {
		this.haveUnfiExamine = haveUnfiExamine;
	}

	public boolean isHeightErr() {
		return heightErr;
	}

	public void setHeightErr(boolean heightErr) {
		this.heightErr = heightErr;
	}

	public boolean isInsJobTypeThaBefo() {
		return insJobTypeThaBefo;
	}

	public void setInsJobTypeThaBefo(boolean insJobTypeThaBefo) {
		this.insJobTypeThaBefo = insJobTypeThaBefo;
	}

	public boolean isPregWeekErr() {
		return pregWeekErr;
	}

	public void setPregWeekErr(boolean pregWeekErr) {
		this.pregWeekErr = pregWeekErr;
	}

	public double getSumPersonAmount() {
		return sumPersonAmount;
	}

	public void setSumPersonAmount(double sumPersonAmount) {
		this.sumPersonAmount = sumPersonAmount;
	}

	public boolean isWeightErr() {
		return weightErr;
	}

	public void setWeightErr(boolean weightErr) {
		this.weightErr = weightErr;
	}

	public double getSum6009AccidentAmount() {
		return sum6009AccidentAmount;
	}

	public void setSum6009AccidentAmount(double sum6009AccidentAmount) {
		this.sum6009AccidentAmount = sum6009AccidentAmount;
	}

	public double getSumStandPrem() {
		return sumStandPrem;
	}

	public void setSumStandPrem(double sumStandPrem) {
		this.sumStandPrem = sumStandPrem;
	}

/*	public double getHealthByElectricPin() {
		return healthByElectricPin;
	}

	public void setHealthByElectricPin(double healthByElectricPin) {
		this.healthByElectricPin = healthByElectricPin;
	}*/

/*	public double getInsuredDouble1() {
		return insuredDouble1;
	}

	public void setInsuredDouble1(double insuredDouble1) {
		this.insuredDouble1 = insuredDouble1;
	}*/

/*	public double getInsuredDouble2() {
		return insuredDouble2;
	}

	public void setInsuredDouble2(double insuredDouble2) {
		this.insuredDouble2 = insuredDouble2;
	}*/

	public String getInsuredString1() {
		return insuredString1;
	}

	public void setInsuredString1(String insuredString1) {
		this.insuredString1 = insuredString1;
	}

	public String getInsuredString2() {
		return insuredString2;
	}

	public void setInsuredString2(String insuredString2) {
		this.insuredString2 = insuredString2;
	}

	public boolean isHavePayFor7060() {
		return havePayFor7060;
	}

	public void setHavePayFor7060(boolean havePayFor7060) {
		this.havePayFor7060 = havePayFor7060;
	}

	public boolean isHavePayFor7061() {
		return havePayFor7061;
	}

	public void setHavePayFor7061(boolean havePayFor7061) {
		this.havePayFor7061 = havePayFor7061;
	}
/*	public double getInsuredDouble3() {
		return insuredDouble3;
	}

	public void setInsuredDouble3(double insuredDouble3) {
		this.insuredDouble3 = insuredDouble3;
	}*/

	public boolean isWhiteSign() {
		return whiteSign;
	}

	public void setWhiteSign(boolean whiteSign) {
		this.whiteSign = whiteSign;
	}

	public double getWhiteSignAmnt() {
		return whiteSignAmnt;
	}

	public void setWhiteSignAmnt(double whiteSignAmnt) {
		this.whiteSignAmnt = whiteSignAmnt;
	}

	public Map getExtendMap() {
		return extendMap;
	}

	public void setExtendMap(Map extendMap) {
		this.extendMap = extendMap;
	}

	public boolean isHaveDeferPolicyLY() {
		return haveDeferPolicyLY;
	}

	public void setHaveDeferPolicyLY(boolean haveDeferPolicyLY) {
		this.haveDeferPolicyLY = haveDeferPolicyLY;
	}

	public boolean isHaveSubExamineLY() {
		return haveSubExamineLY;
	}

	public void setHaveSubExamineLY(boolean haveSubExamineLY) {
		this.haveSubExamineLY = haveSubExamineLY;
	}

	public boolean isHaveClaims7061Specil() {
		return haveClaims7061Specil;
	}

	public void setHaveClaims7061Specil(boolean haveClaims7061Specil) {
		this.haveClaims7061Specil = haveClaims7061Specil;
	}

	public boolean isHaveClaims7060Specil() {
		return haveClaims7060Specil;
	}

	public void setHaveClaims7060Specil(boolean haveClaims7060Specil) {
		this.haveClaims7060Specil = haveClaims7060Specil;
	}
}
