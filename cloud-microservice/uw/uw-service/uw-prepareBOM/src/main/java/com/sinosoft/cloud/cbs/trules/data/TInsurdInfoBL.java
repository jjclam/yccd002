package com.sinosoft.cloud.cbs.trules.data;

import com.sinosoft.cloud.cbs.function.UWFunction;
import com.sinosoft.cloud.cbs.trules.bom.TInsurdInfo;
import com.sinosoft.cloud.cbs.trules.util.DomainUtils;
import com.sinosoft.cloud.rest.TradeInfo;
import com.sinosoft.lis.entity.LCAddressPojo;
import com.sinosoft.lis.entity.LCContPojo;
import com.sinosoft.lis.entity.LCInsuredPojo;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.lis.pubfun.PubFun;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @Author:wangshuliang
 * @Description: 提取被保人信息
 * @Date:Created in 10:20 2018/6/7
 * @Modified by:
 */
@Component
public class TInsurdInfoBL {
    private Log logger = LogFactory.getLog(getClass());
    @Autowired
    UWFunction uwFunction;

    /**
     * 提取被保人信息
     *
     * @param tradeInfo
     * @return
     */
    public TradeInfo getTInsurdInfoBL(TradeInfo tradeInfo) {
        logger.debug("开始提数被保人信息");
        FDate fDate = new FDate();
        List<LCInsuredPojo> tLCInsuredPojoList = (List) tradeInfo.getData(LCInsuredPojo.class.getName());
        LCContPojo lcContPojo = (LCContPojo) tradeInfo.getData(LCContPojo.class.getName());
        if (tLCInsuredPojoList == null || tLCInsuredPojoList.size() == 0) {
            logger.debug("被保人信息不存在。。。");
            tradeInfo.addError("被保人信息不存在。。");
            return tradeInfo;
        }
        LCInsuredPojo lcInsuredPojo = tLCInsuredPojoList.get(0);
        TInsurdInfo tInsurdInfo = new TInsurdInfo();
        //被保人姓名
        tInsurdInfo.setInsurdName(lcInsuredPojo.getName());
        //被保人英文姓名
        StringBuffer englishName = new StringBuffer();
        if (lcInsuredPojo.getFirstName() == null) {
            englishName.append("").append(".");
        } else {
            englishName.append(lcInsuredPojo.getFirstName()).append(".");
        }
        if (".".equals(englishName.toString())){
            tInsurdInfo.setInsurdEngName("ABCjieyixian");
        }else{
            tInsurdInfo.setInsurdEngName(String.valueOf(englishName));
        }
        //证件类型
        tInsurdInfo.setInsurdCerttype(DomainUtils.convertIDType(lcInsuredPojo.getIDType()));
        //证件号
        tInsurdInfo.setInsurdCertno(lcInsuredPojo.getIDNo());
        //性别
        tInsurdInfo.setInsurdGender(DomainUtils.convertSex(lcInsuredPojo.getSex()));
        //年龄
        //被保人年龄
        if (lcInsuredPojo.getBirthday() != null && !"".equals(lcInsuredPojo.getBirthday()) && lcContPojo.getCValiDate() != null && !"".equals(lcContPojo.getCValiDate())) {
            tInsurdInfo.setInsurdAge(PubFun
                    .calInterval(lcInsuredPojo.getBirthday(), lcContPojo.getCValiDate(), "Y"));
        }
        //出生日期
        tInsurdInfo.setInsurdBirthDate(lcInsuredPojo.getBirthday());
        List<LCAddressPojo> lcAddressPojos = (List<LCAddressPojo>) tradeInfo.getData(LCAddressPojo.class.getName());
        for (LCAddressPojo lcAddressPojo : lcAddressPojos) {
            if (lcAddressPojo.getCustomerNo().equals(lcInsuredPojo.getInsuredNo())){
                //联系电话
            /* tInsurdInfo.setContactTel(lcAddressPojo.getMobile());
                //联系电话*/
                tInsurdInfo.setContactTel(lcAddressPojo.getMobile());
                //联系地址
                tInsurdInfo.setContactAddr(lcAddressPojo.getHomeAddress());
                //联系地址编码
                tInsurdInfo.setContactAddrno(lcAddressPojo.getZipCode());
                //固定电话
                tInsurdInfo.setPhone(lcAddressPojo.getHomePhone());
            }
        }
        //职业代码
        tInsurdInfo.setOccupationCode(lcInsuredPojo.getOccupationCode());
        //证件有效期
        tInsurdInfo.setInsurdCertValidperiod(lcInsuredPojo.getIdValiDate());
        //国籍
        tInsurdInfo.setNationality(lcInsuredPojo.getNativePlace());
        //职业类别
        tInsurdInfo.setOccupationCat(lcInsuredPojo.getOccupationType());
        //是否已有本保障计划标志  溜冰险套餐专属，不需要
       //tInsurdInfo.setSafeguardFlag("");
        //国家等级
        //tInsurdInfo.setCountryCat("");
        //证件类型是否为长期
        tInsurdInfo.setLongPeriod(lcInsuredPojo.getIsLongValid());
        //健康告知  没有为0 有为 1
        String healthNotice= (String) tradeInfo.getData("HealthNotice");
        if (StringUtils.isEmpty(healthNotice)){
            tInsurdInfo.setHealthNotice("0");
        }else{
            if ("Y".equals(healthNotice)){
                tInsurdInfo.setHealthNotice("1");
            }
        }
        //黑名单1-被保人的证件号与黑名单相同，黑名单1-证件号与和黑名单相同,不分国籍,>0触发
        boolean insBackIdFlag = uwFunction.checkBlackIdFlag(lcInsuredPojo.getIDNo());
        tInsurdInfo.setInsBlackIdFlag(insBackIdFlag);
        if (!insBackIdFlag){
            //黑名单2-中国客户名字、生日与黑名单相同,>0触发,
            if("CHN".equals(lcInsuredPojo.getNativePlace())){
                boolean checkInsBlackFullFlag = uwFunction.checkBlackCHNFlag(lcInsuredPojo.getName(), lcInsuredPojo.getBirthday());
                tInsurdInfo.setInsBlackFullFlag(checkInsBlackFullFlag);
            }else {
                String[] strName=lcInsuredPojo.getName().split(" ");
                int count=0;
                if (strName.length==1){
                    // 黑名单3-被保人名1部分组成,被保人名与黑名单表四个名字对比,>0触发
                    boolean checkInsBlackFlag1 = uwFunction.checkBlackFlag1(lcInsuredPojo.getName(), lcInsuredPojo.getNativePlace(), lcInsuredPojo.getBirthday());
                    tInsurdInfo.setInsBlackFlag1(checkInsBlackFlag1);
                }
                if (strName.length==2){
                    /**黑名单4-被保人名2部分组成,被保人名与黑名单表2个名字对比,>0触发
                     * 有一个name相同，且出生日期相同
                     */
                    boolean checkInsBlackFlag2 = uwFunction.checkBlackFlag2(lcInsuredPojo.getName(), lcInsuredPojo.getNativePlace(), lcInsuredPojo.getBirthday());
                    tInsurdInfo.setInsBlackFlag2(checkInsBlackFlag2);
                    if (!checkInsBlackFlag2){
                        /**
                         * 黑名单5-被保人名2部分组成,被保人名与黑名单表2个名字对比,>=2触发
                         * firstname和surnname都要相同.无生日限制
                         */
                        count=0;
                        count += uwFunction.checkBlackFlag3_1(strName[0]);
                        count += uwFunction.checkBlackFlag3_3(strName[1]);
                        if(count >= 2){
                            tInsurdInfo.setInsBlackFlag3(true);
                        }
                    }

                }
                if (strName.length>=3){
                    /** 黑名单6-被保人名>=3部分组成,被保人名与黑名单表3个名字对比,>=0触发,参数:3个contno
                     * firstname,middlename,surname,有生日限制 */
                    boolean checkInsBlackFlag4 = uwFunction.checkBlackFlag4(lcInsuredPojo.getName(), lcInsuredPojo.getNativePlace(), lcInsuredPojo.getBirthday());
                    tInsurdInfo.setInsBlackFlag4(checkInsBlackFlag4);
                    if (!checkInsBlackFlag4){
                        /** 黑名单7-被保人名>=3部分组成,被保人名与黑名单表3个名字对比,>=2触发,参数:3个contno
                         * firstname,middlename,surname,无生日限制 */
                        count = 0;
                        String appntName = lcInsuredPojo.getName();
                        String surName = appntName.substring(appntName.indexOf(strName[1])+strName[1].length()+1,appntName.length());
                        count += uwFunction.checkBlackFlag3_1(strName[0]);
                        count += uwFunction.checkBlackFlag3_2(strName[1]);
                        count += uwFunction.checkBlackFlag3_3(surName);
                        if(count >= 2){
                            tInsurdInfo.setInsBlackFlag5(true);
                        }
                    }
                }

            }
        }

        tradeInfo.addData(TInsurdInfo.class.getName(),tInsurdInfo);
        logger.debug("完成提数被保人信息");
        return tradeInfo;
    }
}
