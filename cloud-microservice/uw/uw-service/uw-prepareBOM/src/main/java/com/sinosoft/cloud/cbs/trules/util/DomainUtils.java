package com.sinosoft.cloud.cbs.trules.util;



/**
 * @Author:wangshuliang
 * @Description:
 * @Date:Created in 16:19 2018/6/12
 * @Modified by:
 */
public class  DomainUtils {
    //计划版本 默认V1
    public static String PRODGROUPVERSION="V1";
    //农业银行为03，其他银行为空，投保规则，不需要
    public static String  NONGHANGSIGN="03";


      /**
     * 证件类型转换  目前出生证没有进行配置需要检查
     * @param IDType
     * @return
     */
    public static String convertIDType(String IDType){
        if (IDType==null || "".equals(IDType)){
            return "";
        }
        //身份证
        if ("0".equals(IDType)){
            return "01";
        }
        //护照
        if("1".equals(IDType)){
            return "02";
        }
        //军人证
        if("2".equals(IDType)){
            return "03";
        }
        //驾驶证
        if("3".equals(IDType)){
            return "04";
        }
        //户口簿
        if("4".equals(IDType)){
            return "05";
        }
        //学生证
        if("5".equals(IDType)){
            return "06";
        }
        //工作证
        if("6".equals(IDType)){
            return "07";
        }
        //工作证
        if("7".equals(IDType)){
            return "08";
        }
        //其他
        if("8".equals(IDType)){
            return "99";
        }
        //其他
        if("9".equals(IDType)){
            return "09";
        }
        //回乡证
        if("11".equals(IDType)){
            return "10";
        }
        //临时身份证
        if ("12".equals(IDType)){
            return "11";
        }
        //警官证
        if ("13".equals(IDType)){
            return "12";
        }
        //港澳居民来往内地通行证
        if ("15".equals(IDType)){
            return "14";
        }
        //台湾居民来往内地通行证
        if ("16".equals(IDType)){
            return "15";
        }
        if ("17".equals(IDType)){
            return "17";
        }
        return IDType;
    }

    /**
     * 性别转换
     * @param sex
     * @return
     */
    public static String convertSex(String sex){
        if (sex == null || "".equals(sex)){
            return sex;
        }
        //男
        if ("0".equals(sex)){
            return "M";
        }
        if ("1".equals(sex)){
            return "F";
        }
        return sex;
    }

    /**
     * 付费方式转码
     * @param payMode
     * @return
     */
    public static String converPayMode(String payMode){
        return payMode;
    }
}
