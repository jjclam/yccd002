package com.sinosoft.cloud.cbs.trules.data;

import com.sinosoft.cache.dao.impl.RedisCommonDao;
import com.sinosoft.cloud.cbs.trules.bom.ProdGroup;
import com.sinosoft.cloud.cbs.trules.bom.Product;
import com.sinosoft.cloud.cbs.trules.util.DomainUtils;
import com.sinosoft.cloud.nb.service.redis.NBRedisCommon;
import com.sinosoft.cloud.rest.TradeInfo;
import com.sinosoft.lis.entity.*;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @Author:wangshuliang
 * @Description:获取产品组合信息
 * @Date:Created in 9:14 2018/6/6
 * @Modified by:
 */
@Component
public class TProdGroupBL {
    private Log logger = LogFactory.getLog(getClass());
    @Autowired
    private NBRedisCommon mNBRedisCommon;


    /**
     * 产品组合信息提数
     * @param tradeInfo
     * @return
     */
    public TradeInfo getProdGroupBL(TradeInfo tradeInfo){
        ProdGroup prodGroup = new ProdGroup();
        List<LCPolPojo> lcPolPojoList = (List<LCPolPojo>) tradeInfo.getData(LCPolPojo.class.getName());
        List<LCInsuredPojo> lcInsuredPojos = (List<LCInsuredPojo>) tradeInfo.getData(LCInsuredPojo.class.getName());
        String contPlanCode = lcInsuredPojos.get(0).getContPlanCode();
        String mainRiskcode="";
        for (LCPolPojo lcPolPojo : lcPolPojoList) {
            if (lcPolPojo.getPolNo().equals(lcPolPojo.getMainPolNo())) {
                mainRiskcode = lcPolPojo.getRiskCode();
            }
        }
        String contPlanName = mNBRedisCommon.getContPlanName(mainRiskcode,contPlanCode);
        //产品组合编码
        prodGroup.setProdGroupCode(contPlanCode);
        //产品组合名字
        prodGroup.setProdGroupName(contPlanName);
        //份数
        prodGroup.setCopies((int)lcPolPojoList.get(0).getMult());
        //产品组合版本  不需要
        //prodGroup.setProdGroupVersion(DomainUtils.PRODGROUPVERSION);

        double sumAmnt = 0;
        List<LCPolPojo>  tLCPolPojoList = (List<LCPolPojo>) tradeInfo.getData(LCPolPojo.class.getName());
        //保费
        Product product = (Product) tradeInfo.getData(Product.class.getName());
        if (product != null) {
            prodGroup.setPrem(product.getSumPrem());
        }
        //保险金额
        for (LCPolPojo lcPolPojo: tLCPolPojoList){
            sumAmnt = sumAmnt+ lcPolPojo.getAmnt();
        }
        prodGroup.setSuminsur(sumAmnt);
        //保险期间
        prodGroup.setInsurperiod(tLCPolPojoList.get(0).getInsuYear());
        //乘机日期  不需要
        //prodGroup.setBoardDate("");
        GlobalPojo  globalPojo = (GlobalPojo) tradeInfo.getData(GlobalPojo.class.getName());
        //风险加费
        if (globalPojo==null){
            logger.debug("globalPojo的信息为空 请检查"+tradeInfo.toString());
            tradeInfo.addError("globalPojo的信息为空 请检查");
            return tradeInfo;
        }
        prodGroup.setRiskAddprem(globalPojo.getAddPrem());
        //套餐有效期  不需要
        prodGroup.setPackageValidity("3000-01-01");
        tradeInfo.addData(ProdGroup.class.getName(),prodGroup);
        return tradeInfo;
    }

}
