package com.sinosoft.cloud.cbs.rules.data;

import com.sinosoft.cloud.cbs.function.UWFunction;
import com.sinosoft.cloud.cbs.rules.bom.*;
import com.sinosoft.cloud.nb.service.redis.NBRedisCommon;
import com.sinosoft.cloud.rest.TradeInfo;
import com.sinosoft.lis.entity.*;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.lis.pubfun.PubFun;
//import com.sinosoft.lis.vschema.LMWhiteListSet;
import com.sinosoft.utility.*;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Author: zhuyiming
 * @Description: 提取被保人数据
 * @Date: Created in 13:04 2017/9/24
 */
@Service
public class ExtractInsuredInfo {

	private final Log logger = LogFactory.getLog(this.getClass());
	@Value("${cloud.uw.barrier.control}")
	private String barrier;

	@Autowired
	NBRedisCommon nbRedisCommon;
	@Autowired
	UWFunction uwFunction;

	/**
	 * 获取被保人信息
	 * @param policy
	 * @return List
	 * @throws Exception
	 */
	public List getInsuredInfo(Policy policy,TradeInfo requestInfo) throws Exception {
		LCContPojo lcContPojo = (LCContPojo) requestInfo.getData(LCContPojo.class.getName());
		List<LCInsuredPojo> lcInsuredPojoList = (List<LCInsuredPojo>) requestInfo.getData(LCInsuredPojo.class.getName());
		LCAppntPojo lcAppntPojo =  (LCAppntPojo) requestInfo.getData(LCAppntPojo.class.getName());
		Applicant applicant = policy.getApplicant();
		FDate fDate = new FDate();
		List insuredInfoList = policy.getInsuredList();
		List<LCInsuredPojo> tLCInsuredPojoList = (List) requestInfo.getData(LCInsuredPojo.class.getName());
		List<LCPolPojo> tLCPolPojoList = (List) requestInfo.getData(LCPolPojo.class.getName());
		long beginTime = System.currentTimeMillis();
		//第一被保人
		//LCInsuredPojo lcInsuredPojo = tLCInsuredPojoList.get(0);
		//if (null != lcInsuredPojo) {
			for (int k=0;k<tLCInsuredPojoList.size();k++) {
			LCInsuredPojo lcInsuredPojo = tLCInsuredPojoList.get(k);
			Insured insured = new Insured();
			// 姓名
			insured.setInsuredName(lcInsuredPojo.getName());
			// 证件类型
			insured.setIdentityType(lcInsuredPojo.getIDType());
			// 证件号码
			insured.setIdentityCode(lcInsuredPojo.getIDNo());
			// 性别
			insured.setSex(lcInsuredPojo.getSex());
			// 出生日期
			insured.setBirthday(fDate.getDate(lcInsuredPojo.getBirthday()));
			if (insured.getBirthday() != null
					&& !"".equals(insured.getBirthday())
					&& policy.getEffectiveDate() != null
					&& !policy.getEffectiveDate().equals("")) {
				// 年龄
				insured.setAge(PubFun.calInterval(insured.getBirthday(),
						policy.getEffectiveDate(), "Y"));
			}
			// 身高
			insured.setHeight(lcInsuredPojo.getStature());
			// 体重
			insured.setWeight(lcInsuredPojo.getAvoirdupois());
			// 证件有效期
			insured.setIdentityValidityTerm(lcInsuredPojo.getIdValiDate());
			// 国籍
			insured.setNationality(lcInsuredPojo.getNativePlace());
			// 户籍
			insured.setHouseholdRegister(lcInsuredPojo.getRgtAddress());
			// 学历
			insured.setEducation(lcInsuredPojo.getDegree());
			// 婚姻状况
			insured.setMarrageStatus(lcInsuredPojo.getMarriage());
			// 与第一被保人关系
			insured.setRelationWithFirstInsured(lcInsuredPojo.getRelationToMainInsured());
			// 职业代码
			insured.setOccuCode(lcInsuredPojo.getOccupationCode());
			// 职业类别
			insured.setOccuType(lcInsuredPojo.getOccupationType());
			// 职业描述
			insured.setOccuDescribe(lcInsuredPojo.getWorkType());
			// 兼职
			insured.setPartTimeJobFlag(lcInsuredPojo.getPluralityType());
			// 驾照类型
			insured.setLicenceType(lcInsuredPojo.getLicenseType());
			// 地址代码
			insured.setAddrCode(lcInsuredPojo.getAddressNo());
			// BMI
			insured.setBMI(lcInsuredPojo.getBMI()+"");
			// 英文姓名
			//wsl 20180125 start
			StringBuffer englishName = new StringBuffer();
			if(lcInsuredPojo.getFirstName()==null){
				englishName.append("").append(".");
			}else{
				englishName.append(lcInsuredPojo.getFirstName()).append(".");
			}
			if (lcInsuredPojo.getLastName()==null){
				englishName.append("");
			}else{
				englishName.append(lcInsuredPojo.getLastName());
			}
			insured.setEnglishName(englishName.toString());
			//wsl 20180125 end
			// 健康声明
			insured.setHealthDeclare(lcInsuredPojo.getHealth());
			// 职务
			insured.setDuties(lcInsuredPojo.getPosition());
			// 被保险人客户号
			insured.setClientNo(lcInsuredPojo.getInsuredNo());
			// 与投保人关系 ---投被关系新核心取得是被保人里的 “00"解决豁免险
			if("00".equals(lcInsuredPojo.getRelationToAppnt())){
				insured.setRelationWithAppnt(lcInsuredPojo.getRelationToAppnt());
			}else {
				insured.setRelationWithAppnt(lcAppntPojo.getRelatToInsu());
			}
			logger.debug("被保人基础-提数：" + (System.currentTimeMillis() - beginTime) / 1000.0
					+ "s");

			String custNo = insured.getClientNo();
			// 被保人是否残疾SQL（customerno：客户编码，contno：合同号）
			List<LCCustomerImpartParamsPojo> tLCCustomerImpartParamsList = (List)requestInfo.getData(LCCustomerImpartParamsPojo.class.getName());
			/*boolean disabledSign = false;
			if(tLCCustomerImpartParamsList != null && tLCCustomerImpartParamsList.size() > 0){
				for(int j=0;j<tLCCustomerImpartParamsList.size();j++){
					if(custNo.equals(tLCCustomerImpartParamsList.get(j).getCustomerNo())
							&& "0".equals(tLCCustomerImpartParamsList.get(j).getCustomerNoType())
							&& "1".equals(tLCCustomerImpartParamsList.get(j).getImpartParam())
							&& "YesOrNo".equals(tLCCustomerImpartParamsList.get(j).getImpartParamName())){
						String impartVer = tLCCustomerImpartParamsList.get(j).getImpartVer();
						String impartCode = tLCCustomerImpartParamsList.get(j).getImpartCode();
						if(("DLR3".equals(impartVer) && "1-3".equals(impartCode))
								|| ("GX8".equals(impartVer) && "18".equals(impartCode) || "10".equals(impartCode))
								|| ("GX10".equals(impartVer) && "3".equals(impartCode))){
							disabledSign = true;
							break;
						}
					}
				}
			}
			insured.setDisabledSign(disabledSign);*/

			//被保人身高异常
			SimpleDateFormat sdf =   new SimpleDateFormat( "yyyy-MM-dd" );
			double tStature = lcInsuredPojo.getStature();
			//zym add at 2017/11/1 18:00 开门红需求修改
			if (insured.getAge() >= 17) {
//			if (insured.getAge() >= 18) {
				// 被保人身高异常SQL,成年人（CONTNO：合同号，INSUREDNO：被保人编码）
				SSRS tSSRS = nbRedisCommon.checkHeightErr(lcInsuredPojo.getSex(), String.valueOf(new PubFun().calInterval3(lcInsuredPojo.getBirthday(),lcContPojo.getCValiDate(),"Y")));
				if(tStature <= 0 || tSSRS == null || tSSRS.MaxRow == 0){
					insured.setHeightErr(false);
				}else if(tStature < Double.parseDouble(tSSRS.GetText(1,1)) || tStature > Double.parseDouble(tSSRS.GetText(1,2))){
					insured.setHeightErr(true);
				}else {
					insured.setHeightErr(false);
				}
			} else {
				if(insured.getAge() < 7){
					//被保人身高异常SQL,未成年,小于7岁（CONTNO：合同号，APPNTNO：投保人编码）
					SSRS tSSRS = uwFunction.checkHeightErr1(lcInsuredPojo.getSex(), tLCPolPojoList.get(0).getCValiDate(), lcInsuredPojo.getBirthday());
					if(tStature <= 0 || tSSRS == null || tSSRS.MaxRow == 0){
						insured.setHeightErr(false);
					}else if(tStature < Double.parseDouble(tSSRS.GetText(1,1)) || tStature > Double.parseDouble(tSSRS.GetText(1,2))){
						insured.setHeightErr(true);
					}else {
						insured.setHeightErr(false);
					}
				}else{
					//被保人身高异常SQL,未成年,7到18岁（CONTNO：合同号，APPNTNO：投保人编码）
					SSRS tSSRS = uwFunction.checkHeightErr1(lcInsuredPojo.getSex(), tLCPolPojoList.get(0).getCValiDate(), lcInsuredPojo.getBirthday());
					if(tStature <= 0 || tSSRS == null || tSSRS.MaxRow == 0){
						insured.setHeightErr(false);
					}else if(tStature < Double.parseDouble(tSSRS.GetText(1,1))){
						insured.setHeightErr(true);
					}else {
						insured.setHeightErr(false);
					}

				}
			}
			//被保人体重异常
			double tAvoirdupois = lcInsuredPojo.getAvoirdupois();
			//zym add at 2017/11/1 18:00 开门红需求修改
			if (insured.getAge() >= 17) {
//			if (insured.getAge() >= 18) {
				// 被保人体重异常SQL 成年（CONTNO：合同号，INSUREDNO：被保人编码）
				SSRS tSSRS = nbRedisCommon.checkHeightErr(lcInsuredPojo.getSex(), String.valueOf(new PubFun().calInterval3(lcInsuredPojo.getBirthday(),lcContPojo.getCValiDate(),"Y")));
				if(tAvoirdupois <= 0 || tSSRS == null || tSSRS.MaxRow == 0){
					insured.setWeightErr(false);
				}else if(tAvoirdupois < Double.parseDouble(tSSRS.GetText(1,3)) || tAvoirdupois > Double.parseDouble(tSSRS.GetText(1,4))){
					insured.setWeightErr(true);
				}else {
					insured.setWeightErr(false);
				}
			} else {
				if(insured.getAge() < 7){
					//被保人体重异常SQL 未成年,小于7岁（CONTNO：合同号，APPNTNO：投保人编码）
					SSRS tSSRS = uwFunction.checkHeightErr1(lcInsuredPojo.getSex(), tLCPolPojoList.get(0).getCValiDate(),lcInsuredPojo.getBirthday());
					if(tAvoirdupois <= 0 || tSSRS == null || tSSRS.MaxRow == 0){
						insured.setWeightErr(false);
					}else if(tAvoirdupois < Double.parseDouble(tSSRS.GetText(1,3)) || tAvoirdupois > Double.parseDouble(tSSRS.GetText(1,4))){
						insured.setWeightErr(true);
					}else {
						insured.setWeightErr(false);
					}
				}else{
					//被保人体重异常SQL 未成年,7岁到18岁（CONTNO：合同号，APPNTNO：投保人编码）
					SSRS tSSRS = uwFunction.checkHeightErr1(lcInsuredPojo.getSex(),tLCPolPojoList.get(0).getCValiDate(), lcInsuredPojo.getBirthday());
					if(tAvoirdupois <= 0 || tSSRS == null || tSSRS.MaxRow == 0){
						insured.setWeightErr(false);
					}else if(tAvoirdupois < Double.parseDouble(tSSRS.GetText(1,3))){
						insured.setWeightErr(true);
					}else {
						insured.setWeightErr(false);
					}
				}

			}
			// 被保人BMI异常 根据是否成年拼接sql
			long minBMI = 0;
			long maxBMI = 0;
			if (insured.getAge() >= 18) {
				if(tStature == 0){
					minBMI = Math.round(tAvoirdupois/1000000);
				}else {
					minBMI = Math.round(tAvoirdupois/((tStature/100)*(tStature/100)));
				}
				if(tStature == 0){
					maxBMI = Math.round(tAvoirdupois/0.0001);
				}else {
					maxBMI = Math.round(tAvoirdupois/((tStature/100)*(tStature/100)));
				}
				if(minBMI<18 || maxBMI>28){
					insured.setBmiErr(true);
				}else {
					insured.setBmiErr(false);
				}
			} else {
				//未成年
				String height = "0";
				String weight = "0";
				long bmi = 0;
				List<LCCustomerImpartParamsPojo> lcCustomerImpartParamsPojos = (List<LCCustomerImpartParamsPojo>) requestInfo.getData(LCCustomerImpartParamsPojo.class.getName());
				if(lcCustomerImpartParamsPojos != null && lcCustomerImpartParamsPojos.size()>0){
					for(int j=0;j<lcCustomerImpartParamsPojos.size();j++){
						if(!("19055".equals(lcCustomerImpartParamsPojos.get(j).getPatchNo())
								&& "GX17".equals(lcCustomerImpartParamsPojos.get(j).getImpartVer())
								&& "12A".equals(lcCustomerImpartParamsPojos.get(j).getImpartCode())
								&& "1".equals(lcCustomerImpartParamsPojos.get(j).getCustomerNoType()))){
							continue;
						}
						if("1".equals(lcCustomerImpartParamsPojos.get(j).getImpartParamNo())
								&& "Height".equals(lcCustomerImpartParamsPojos.get(j).getImpartParamName())){
							height = lcCustomerImpartParamsPojos.get(j).getImpartParam();
						}
						if("2".equals(lcCustomerImpartParamsPojos.get(j).getImpartParamNo())
								&& "Weight".equals(lcCustomerImpartParamsPojos.get(j).getImpartParamName())){
							weight = lcCustomerImpartParamsPojos.get(j).getImpartParam();
						}
					}
					if("0".equals(height)){
						height = "100000";
					}
					bmi = Math.round(Double.parseDouble(weight)/((Double.parseDouble(height)/100)*(Double.parseDouble(height)/100)));
				}
				if(bmi<11 || bmi >17){
					insured.setBmiErr(true);
				}else {
					insured.setBmiErr(false);
				}
			}
			// 摩托车驾照
			/*if("1".equals(lcInsuredPojo.getHaveMotorcycleLicence())){
				insured.setMotorLicence(true);
			}else{
				insured.setMotorLicence(false);
			}*/

			//投保人在其他公司的保额
			int otherAmnt = 0;
			double amnt7058 = 0;
			if(tLCPolPojoList!=null && tLCPolPojoList.size()>0){
				for(int j=0;j<tLCPolPojoList.size();j++){
					if(custNo.equals(tLCPolPojoList.get(j).getInsuredNo())){
						otherAmnt += tLCPolPojoList.get(j).getOtherAmnt();
					}
					if("7058".equals(tLCPolPojoList.get(j).getRiskCode())){
						amnt7058 = tLCPolPojoList.get(j).getAmnt();
					}
				}
			}
			insured.setOtherAmnt(otherAmnt);
			// 白名单标识
			try {
				SSRS ssrs = uwFunction.checkInsuWhiteSign(lcInsuredPojo.getName(), lcInsuredPojo.getSex(), lcInsuredPojo.getIDType(), lcInsuredPojo.getIDNo(), lcInsuredPojo.getBirthday());
				insured.setWhiteSign((ssrs==null || ssrs.MaxRow==0)?false:true);
				// 白名单限额
				insured.setWhiteSignAmnt((ssrs==null || ssrs.MaxRow==0)?0:Double.valueOf(ssrs.GetText(1,1)));
			}catch (Exception e){
				logger.error("白名单取值异常"+ExceptionUtils.exceptionToString(e)+requestInfo.toString());
				requestInfo.addError("白名单取值异常"+ExceptionUtils.exceptionToString(e));
				return null;
			}
            //投保人被保人为同一人时 跳过被保人黑名单1-7 从投保人中取
			if(!lcAppntPojo.getAppntName().equals(lcInsuredPojoList.get(0).getName())) {

				// 被保人黑名单标识SQL（BLACKLISTNO：黑名单客户号，IDNO：证件号码，IDTYPE：证件类型）
				boolean blackSign = uwFunction.checkBlackSign(lcInsuredPojo.getInsuredNo(), lcInsuredPojo.getIDNo(), lcInsuredPojo.getIDType());
				insured.setBlackSign(blackSign);

				//黑名单1-证件号与和黑名单相同,不分国籍,>0触发,参数:一个contno
				//{this} 的证件号与和黑名单相同
				boolean backIdFlag = uwFunction.checkBlackIdFlag(lcInsuredPojo.getIDNo());
				insured.setInsBlackIdFlag(backIdFlag);
				//黑名单2-中国客户名字、生日与黑名单相同,>0触发,参数:一个contno
				//{this}为中国客户且名字和生日与黑名单相同
				if ("CHN".equals(lcInsuredPojo.getNativePlace())) {
					boolean checkBlackCHNFlag = uwFunction.checkBlackCHNFlag(lcInsuredPojo.getName(), lcInsuredPojo.getBirthday());
					insured.setInsBlackChnFlag(checkBlackCHNFlag);
				}
				//黑名单3-被保人名1部分组成,被保人名与黑名单表四个名字对比,>0触发,参数:4个contno
				//this}的名字与黑名单名字和生日相同(名字为一部分)
				boolean checkBlackFlag1 = uwFunction.checkBlackFlag1(lcInsuredPojo.getName(), lcInsuredPojo.getNativePlace(), lcInsuredPojo.getBirthday());
				insured.setInsBlackFlag1(checkBlackFlag1);
				//黑名单4-被保人名2部分组成,被保人名与黑名单表2个名字对比,>0触发,参数:2个contno
				//有一个name相同，且出生日期相同
				//{this}的名字与黑名单名字和生日相同(名字为两部分)
				boolean checkBlackFlag2 = uwFunction.checkBlackFlag2(lcInsuredPojo.getName(), lcInsuredPojo.getNativePlace(), lcInsuredPojo.getBirthday());
				insured.setInsBlackFlag2(checkBlackFlag2);
				//黑名单5-被保人名2部分组成,被保人名与黑名单表2个名字对比,>=2触发,参数:2个contno
				//firstname和surnname都要相同.无生日限制
				//{this} 的名字与黑名单名字和生日相同(名字为两部分,无生日限制)
				int count = 0;
				if (lcInsuredPojo.getName().length() - lcInsuredPojo.getName().replace(" ", "").length() == 1
						&& (lcInsuredPojo.getNativePlace() == null || !"CHN".equals(lcInsuredPojo.getNativePlace()))) {
					String insuName = lcInsuredPojo.getName();
					String firstName = insuName.substring(0, insuName.indexOf(" "));
					String surName = insuName.substring(insuName.indexOf(" ") + 1);
					count += uwFunction.checkBlackFlag3_1(firstName);
					count += uwFunction.checkBlackFlag3_3(surName);
				}
				if (count >= 2) {
					insured.setInsBlackFlag3(true);
				}
				//黑名单6-被保人名>=3部分组成,被保人名与黑名单表>=3个名字对比,>=0触发,参数:3个contno
				//firstname,middlename,surname,有生日限制
				//{this} 的名字与黑名单名字两个以上相同(名字为四部分)
				boolean checkBlackFlag4 = uwFunction.checkBlackFlag4(lcInsuredPojo.getName(), lcInsuredPojo.getNativePlace(), lcInsuredPojo.getBirthday());
				insured.setInsBlackFlag4(checkBlackFlag4);
				//黑名单7-被保人名>=3部分组成,被保人名与黑名单表>=3个名字对比,>=2触发,参数:3个contno
				//firstname,middlename,surname,无生日限制
				//{this} 的名字与黑名单相同(被保人名三部分构成无生日限制)
				count = 0;
				if (lcInsuredPojo.getName().length() - lcInsuredPojo.getName().replace(" ", "").length() >= 2
						&& (lcInsuredPojo.getNativePlace() == null || !"CHN".equals(lcInsuredPojo.getNativePlace()))) {
					String appntName = lcInsuredPojo.getName();
					String name[] = appntName.split(" ");
					String firstName = name[0];
					String middleName = name[1];
					String surName = appntName.substring(appntName.indexOf(middleName)+middleName.length()+1,appntName.length());
					count += uwFunction.checkBlackFlag3_1(firstName);
					count += uwFunction.checkBlackFlag3_2(middleName);
					count += uwFunction.checkBlackFlag3_3(surName);
				}
				if (count >= 2) {
					insured.setInsBlackFlag5(true);
				}
			}else{

				// 被保人黑名单标识
				insured.setBlackSign(applicant.isBlackSign());
				// 黑名单1
				insured.setInsBlackIdFlag(applicant.isAppBlackIdFlag());
				// 黑名单2
				insured.setInsBlackChnFlag(applicant.isAppBlackChnFlag());
				// 黑名单3
				insured.setInsBlackFlag1(applicant.isAppBlackFlag1());
				// 黑名单4
				insured.setInsBlackFlag2(applicant.isAppBlackFlag2());
				// 黑名单5
				insured.setInsBlackFlag3(applicant.isAppBlackFlag3());
				// 黑名单6
				insured.setInsBlackFlag4(applicant.isAppBlackFlag4());
				// 黑名单7
				insured.setInsBlackFlag5(applicant.isAppBlackFlag5());



			}
			Map map = new HashMap();
			if("true".equals(barrier)){
				logger.debug("保单号：" + lcContPojo.getContNo() + "，被保人挡板启动！");
			}else {
				getOtherData(policy,insured, requestInfo, map, amnt7058);
				if(requestInfo.hasError()){
					requestInfo.addError("被保人提数失败！");
					return insuredInfoList;
				}
			}
			if("C066".equals(tLCPolPojoList.get(0).getProdSetCode())){
				insured.setRenewalPremSumC066(0);
				for(int i=0;i<tLCPolPojoList.size();i++){
					if(tLCPolPojoList.get(i).getPayIntv() == 0){
						insured.setRenewalPremSumC066(insured.getRenewalPremSumC066()+tLCPolPojoList.get(i).getPrem());
					}
					if(tLCPolPojoList.get(i).getPayIntv() == 12){
						insured.setRenewalPremSumC066(insured.getRenewalPremSumC066()+(tLCPolPojoList.get(i).getPrem()*tLCPolPojoList.get(i).getPayEndYear()));
					}
				}
			}
			if("C067".equals(tLCPolPojoList.get(0).getProdSetCode())){
				insured.setRenewalPremSumC067(0);
				for(int i=0;i<tLCPolPojoList.size();i++){
					if(tLCPolPojoList.get(i).getPayIntv() == 0){
						insured.setRenewalPremSumC067(insured.getRenewalPremSumC067()+tLCPolPojoList.get(i).getPrem());
					}
					if(tLCPolPojoList.get(i).getPayIntv() == 12){
						insured.setRenewalPremSumC067(insured.getRenewalPremSumC067()+(tLCPolPojoList.get(i).getPrem()*tLCPolPojoList.get(i).getPayEndYear()));
					}
				}
			}

			if("C068".equals(tLCPolPojoList.get(0).getProdSetCode())){
				insured.setRenewalPremSumC068(0);
				for(int i=0;i<tLCPolPojoList.size();i++){
					if(tLCPolPojoList.get(i).getPayIntv() == 0){
						insured.setRenewalPremSumC068(insured.getRenewalPremSumC068()+tLCPolPojoList.get(i).getPrem());
					}
					if(tLCPolPojoList.get(i).getPayIntv() == 12){
						insured.setRenewalPremSumC068(insured.getRenewalPremSumC068()+(tLCPolPojoList.get(i).getPrem()*tLCPolPojoList.get(i).getPayEndYear()));
					}
				}
			}
			if("2048".equals(tLCPolPojoList.get(0).getRiskCode())){
				insured.setRenewalPremSum2048(0);
				if(tLCPolPojoList.get(0).getPayIntv() == 0){
					insured.setRenewalPremSum2048(insured.getRenewalPremSum2048()+tLCPolPojoList.get(0).getPrem());
				}else{
					insured.setRenewalPremSum2048(insured.getRenewalPremSum2048()+(tLCPolPojoList.get(0).getPrem()*tLCPolPojoList.get(0).getPayEndYear()));
				}
				insured.setAccPrem2048(insured.getAccPrem2048()+insured.getRenewalPremSum2048());
			}
			if("1040".equals(tLCPolPojoList.get(0).getRiskCode())){
				insured.setSumBasicInsuranceAmount1040(insured.getSumBasicInsuranceAmount1040()+tLCPolPojoList.get(0).getAmnt());
			}
			if("1040".equals(tLCPolPojoList.get(0).getRiskCode())){
				if(tLCPolPojoList.get(0).getPayIntv() == 0){
					insured.setRenewalPremSum1040(tLCPolPojoList.get(0).getPrem());
				}
				if(tLCPolPojoList.get(0).getPayIntv() == 12){
					insured.setRenewalPremSum1040(tLCPolPojoList.get(0).getPrem()*tLCPolPojoList.get(0).getPayEndYear());
				}
			}
			if("2052".equals(tLCPolPojoList.get(0).getRiskCode())){
				if(tLCPolPojoList.get(0).getPayIntv() == 0){
					insured.setAccPrem2052(tLCPolPojoList.get(0).getPrem()+insured.getAccPrem2052());
				}
				if(tLCPolPojoList.get(0).getPayIntv() == 12){
					insured.setAccPrem2052(tLCPolPojoList.get(0).getPrem()*tLCPolPojoList.get(0).getPayEndYear()+insured.getAccPrem2052());
				}
				insured.setSumStandPrem2052(insured.getSumStandPrem2052()+tLCPolPojoList.get(0).getPrem());
			}
			if("2053".equals(tLCPolPojoList.get(0).getRiskCode())){
				insured.setSumStandPrem2053(insured.getSumStandPrem2053()+tLCPolPojoList.get(0).getPrem());
			}
			if("1043".equals(tLCPolPojoList.get(0).getRiskCode())){
					insured.setRenewalPremSum1043(insured.getRenewalPremSum1043()+policy.getSumprem());
			}
			insured.setSumEffectivePolicy(insured.getSumEffectivePolicy()+policy.getSumprem());
			insured.setExtendMap(map);
			amnt7058 = (double) requestInfo.getData("amnt7058");
			insured.setBasicAmnt7058(amnt7058);
			//5018税优健康险的BMI
			double tbmivalue=0.0;
			BigDecimal bmivalue=new BigDecimal(0);
		if(lcInsuredPojo.getStature()==0.0){
				tbmivalue=lcInsuredPojo.getAvoirdupois()/Math.sqrt(100000/100);
				bmivalue= new BigDecimal(tbmivalue).setScale(0, BigDecimal.ROUND_HALF_UP);
			}else{
				tbmivalue=lcInsuredPojo.getAvoirdupois()/Math.sqrt(lcInsuredPojo.getStature()/100);
				bmivalue= new BigDecimal(tbmivalue).setScale(0, BigDecimal.ROUND_HALF_UP);
				logger.debug("5018税优健康险的BMI"+bmivalue.doubleValue());
			}
			insured.setBmiValue(bmivalue.doubleValue());
			//备注栏有内容  --wpq--
			/*String noteColumn=uwFunction.getNoteColumn(lcInsuredPojo.getContNo());
			if("1".equals(noteColumn)){
				insured.setNoteColumn(true);
			}else{
				insured.setNoteColumn(false);
			}*/
			//提取中保信返回信息
				getAccidInsured(requestInfo,insured);
			insuredInfoList.add(insured);
		}

		return insuredInfoList;
	}

	/**
	 * 被保人老核心提数
	 * @param insured
	 * @param tradeInfo
	 */
	public void getOtherData(Policy policy,Insured insured, TradeInfo tradeInfo, Map map, double amnt7058){
		String custNo = insured.getClientNo();
		LCContPojo lcContPojo = (LCContPojo) tradeInfo.getData(LCContPojo.class.getName());
		List<LKTransTracksPojo> transTracksPojos=(List<LKTransTracksPojo>)tradeInfo.getData(LKTransTracksPojo.class.getName());
		//保单号
		String contno = lcContPojo.getContNo();
		//父保单号
		String beforContno=null;
		if (transTracksPojos !=null && transTracksPojos.size()>0){
			for (int i = 0; i < transTracksPojos.size(); i++){
				if (transTracksPojos.get(i).getTemp2()!=null && !"".equals(transTracksPojos.get(i).getTemp2())){
					beforContno=transTracksPojos.get(i).getTemp2();
				}
			}
		}
		StringBuffer sql = new StringBuffer();
		VData tVData = new VData();
		TransferData tParam = new TransferData();
		tradeInfo.addData("amnt7058",amnt7058);
		// 电销渠道非c012险种份数
		/*sql.append(SQLConstant.InsuredInfo.NUMOFNOTC012SQL);
		sql.append(" UNION ");*/
		// 电销渠道非c009险种份数
		/*sql.append(SQLConstant.InsuredInfo.NUMOFNOTC009SQL);*/
		/*sql.append(" UNION ");*/
		// 爱永远定期寿险保费
		if(Util.judgeCode(policy,"riskcode","1012")){
			sql.append(SQLConstant.InsuredInfo.AMNTFor1012SQL);
			tParam.setNameAndValue("INSUREDNO13", "string:" + custNo);
			sql.append(" UNION ");
		}


		//电销渠道下非C014组合险份数
		/*sql.append(SQLConstant.InsuredInfo.NUMOFNOTC014SQL);*/
		/*sql.append(" UNION ");*/
		//被保人的团险渠道累计重疾保额

		sql.append(SQLConstant.InsuredInfo.HPRMOFSELLCHANL2);
		tParam.setNameAndValue("INSUREDNO18", "string:" + custNo);
		tParam.setNameAndValue("CONTNO8", "string:" + contno);

		// 被保人7054累计保额
		if(Util.judgeCode(policy,"riskcode","7054")) {
			sql.append(" UNION ");
			sql.append(SQLConstant.InsuredInfo.PRM7054);
			tParam.setNameAndValue("INSUREDNO19", "string:" + custNo);
		}
		//团险渠道长期重疾险(7829)有拒保、延期记录
		sql.append(" UNION ");
		sql.append(SQLConstant.InsuredInfo.REFUSERECODE7829);
		tParam.setNameAndValue("INSUREDNO20", "string:" + custNo);
		tParam.setNameAndValue("INSUREDNO21", "string:" + custNo);
		tParam.setNameAndValue("INSUREDNO22", "string:" + custNo);
		tParam.setNameAndValue("INSUREDNO23", "string:" + custNo);
		tParam.setNameAndValue("INSUREDNO24", "string:" + custNo);
		tParam.setNameAndValue("INSUREDNO25", "string:" + custNo);

		//被保人的团险渠道长期重疾险(7829)有次标准体记录
		sql.append(" UNION ");
		sql.append(SQLConstant.InsuredInfo.SUBSTANDARY7829);
		tParam.setNameAndValue("INSUREDNO26", "string:" + custNo);
		tParam.setNameAndValue("INSUREDNO27", "string:" + custNo);
		tParam.setNameAndValue("INSUREDNO28", "string:" + custNo);
		tParam.setNameAndValue("INSUREDNO29", "string:" + custNo);
		tParam.setNameAndValue("INSUREDNO30", "string:" + custNo);
		tParam.setNameAndValue("INSUREDNO31", "string:" + custNo);

		//7056险种累计投保额
		if(Util.judgeCode(policy,"riskcode","7056")) {
			sql.append(" UNION ");
			sql.append(SQLConstant.InsuredInfo.PRM7056);
			tParam.setNameAndValue("INSUREDNO32", "string:" + custNo);
		}
		//被保人-同业拒保标记
		//同业拒保次数
		sql.append(" UNION ");
		sql.append(SQLConstant.InsuredInfo.DECLINEFLAG);
		tParam.setNameAndValue("INSUREDNO35","string:" + custNo);

		//同业投保超2次标记赋值
		sql.append(" UNION ");
		sql.append(SQLConstant.InsuredInfo.APPLICATIONEFLAG);
		tParam.setNameAndValue("INSUREDNO36","string:" + custNo);

		//被保人7059累计基本保额
		if(Util.judgeCode(policy,"riskcode","7059")) {
			sql.append(" UNION ");
			sql.append(SQLConstant.InsuredInfo.BASICAMNT7059SQL);
			tParam.setNameAndValue("INSUREDNO37", "string:" + custNo);
		}
		//被保人重疾或防癌理赔记录
		sql.append(" UNION ");
		sql.append(SQLConstant.InsuredInfo.CANCERCLAIMRECORDSQL);
		tParam.setNameAndValue("INSUREDNO381","string:" + custNo);
		tParam.setNameAndValue("INSUREDNO382", "string:" + custNo);
		//被保人意外险理赔记录
		sql.append(" UNION ");
		sql.append(SQLConstant.InsuredInfo.ACCIDENTCLAIMRECORDSQL);
		tParam.setNameAndValue("INSUREDNO390","string:" + custNo);
		tParam.setNameAndValue("INSUREDNO391","string:" + custNo);
		//被保人7057险种累计基本保额
		if(Util.judgeCode(policy,"riskcode","7057")) {
			sql.append(" UNION ");
			sql.append(SQLConstant.InsuredInfo.BASICAMNT7057SQL);
			tParam.setNameAndValue("INSUREDNO40", "string:" + custNo);
		}
		//被保人7058险种累计基本保额
		if(Util.judgeCode(policy,"riskcode","7058")) {
			sql.append(" UNION ");
			sql.append(SQLConstant.InsuredInfo.BASICAMNT7058SQL);
			tParam.setNameAndValue("INSUREDNO41","string:" + custNo);
		}

		//被保人1030险种累计基本保额
		if(Util.judgeCode(policy,"riskcode","1030")) {
			sql.append(" UNION ");
			sql.append(SQLConstant.InsuredInfo.BASICAMNT1030SQL);
			tParam.setNameAndValue("INSUREDNO42", "string:" + custNo);
		}
		//被保人2048险种累计基本保额
		if(Util.judgeCode(policy,"riskcode","2048")) {
			sql.append(" UNION ");
			sql.append(SQLConstant.InsuredInfo.BASICAMNT2048SQL);
			tParam.setNameAndValue("INSUREDNO43", "string:" + custNo);
		}
		//被保人2048险种累计保费 accPrem2048
		if(Util.judgeCode(policy,"riskcode","2048")) {
			sql.append(" UNION ");
			sql.append(SQLConstant.InsuredInfo.ACCPREM2048SQL);
			tParam.setNameAndValue("INSUREDNO44", "string:" + custNo);
		}
		// 被保人6009意外险保额
		if(Util.judgeCode(policy,"riskcode","6009")){
			sql.append(" UNION ");
			sql.append(SQLConstant.InsuredInfo.SUMACCIDENTAMOUNTSQL);
			tParam.setNameAndValue("INSUREDNO10","string:" + custNo);
		}
		// 万能险保费，5015险种已停售
		if(Util.judgeCode(policy,"riskcode","5015")){
		sql.append(" UNION ");
		sql.append(SQLConstant.InsuredInfo.SUMStandPremSQL);
		tParam.setNameAndValue("INSUREDNO9", "string:" + custNo);
		tParam.setNameAndValue("appno01", "string:" + lcContPojo.getAppntNo());
		}
		// 电销重疾保额
		/*sql.append(" UNION ");
		sql.append(SQLConstant.InsuredInfo.HealthByElectricPinSql);*/
		// 被保人c060累计保费  --wpq--
		if(Util.judgeCode(policy,"prodSetCode","C060")) {
			sql.append(" UNION ");
			sql.append(SQLConstant.InsuredInfo.C060ACCAMNTSQL);
			tParam.setNameAndValue("INSUREDNO45", "string:" + custNo);
			//被保险人累计C060份数  --wpq--
			sql.append(" UNION ");
			sql.append(SQLConstant.InsuredInfo.C060COPIESSQL);
			tParam.setNameAndValue("INSUREDNO46", "string:" + custNo);
		}
		//被保险人投保7053累计重疾险风险保额 --wpq--
		if(Util.judgeCode(policy,"prodSetCode","C061")) {
			sql.append(" UNION ");
			sql.append(SQLConstant.InsuredInfo.ACCAMNT7053SQL);
			tParam.setNameAndValue("INSUREDNO47", "string:" + custNo);
		}
		// 非工作单位组织的个人投保标记  --wpq--
//		sql.append(" UNION ");
//		sql.append(SQLConstant.InsuredInfo.PERSONALINSURANCESQL);
//		tParam.setNameAndValue("INSUREDNO48","string:"+ contno);
		//工作单位首次或再次组织的投保标记 --wpq--
//		sql.append(" UNION ");
//		sql.append(SQLConstant.InsuredInfo.ORGANIZATIONINSURANCESQL);
//		tParam.setNameAndValue("INSUREDNO49","string:"+ contno);


		if(Util.judgeCode(policy,"riskcode","7061") ||
				Util.judgeCode(policy,"riskcode","7060"))  {
			if(beforContno !=null && !"".equals(beforContno)){
				//被保险人是否已拥有7060或7061的有效保单 (续期)
				sql.append(" UNION ");
				sql.append(SQLConstant.InsuredInfo.EFFECTIVE7060POLICYSQL1);
				tParam.setNameAndValue("INSUREDNO50_1", "string:" + custNo);
				tParam.setNameAndValue("BEFORCONTNO34","string:" + beforContno);
			}else {
				//被保险人是否已拥有7060或7061的有效保单 2018/3/27 zfx(非续期)
				sql.append(" UNION ");
				sql.append(SQLConstant.InsuredInfo.EFFECTIVE7060POLICYSQL2);
				tParam.setNameAndValue("INSUREDNO50_2", "string:" + custNo);
				//tParam.setNameAndValue("CONTNO33", "string:" + contno);
				//tParam.setNameAndValue("INSUREDNO50", "string:" + custNo);
				//tParam.setNameAndValue("CONTNO34","string:" + beforContno);
			}
		}


		//被保险人既往有非团险账户的医疗险种的理赔记录 2018/3/27 zfx
		/*sql.append(" UNION ");
		sql.append(SQLConstant.InsuredInfo.HAVECLAIMSNOTMEDICSQL);*/
		// C066 险种各期保费之和
		/*if(Util.judgeCode(policy,"prodSetCode","C066")){
			sql.append(" UNION ");
			sql.append(SQLConstant.InsuredInfo.RENEWALPREMSUMC066SQL);
			tParam.setNameAndValue("CONTNO32", "string:" + contno);
		}*/
		if(Util.judgeCode(policy,"prodSetCode","C070")) {
			// C070组合规则 保险计划累计保额
			sql.append(" UNION ");
			sql.append(SQLConstant.InsuredInfo.MAXIMUMGRANTSQL);
			tParam.setNameAndValue("INSUREDNO53", "string:" + custNo);
			// C070组合规则 被保险人累计C070份数
			sql.append(" UNION ");
			sql.append(SQLConstant.InsuredInfo.C070COPIESSQL);
			tParam.setNameAndValue("INSUREDNO54", "string:" + custNo);
		}
		//被保人意外医疗险累计理赔给付金额
		sql.append(" UNION ");
		sql.append(SQLConstant.InsuredInfo.ACCIDENTMEDICPAYSQL);
		tParam.setNameAndValue("INSUREDNO55", "string:" + custNo);
		//tParam.setNameAndValue("INSUREDNO55", "string:" + custNo);
		//被保人健医卡理赔给付金额
		sql.append(" UNION ");
		sql.append(SQLConstant.InsuredInfo.HELTHMEDICPAYSQL);
		tParam.setNameAndValue("INSUREDNO56", "string:" + custNo);

		if(Util.judgeCode(policy,"riskcode","7061") ||
				Util.judgeCode(policy,"riskcode","7060")) {
			//被保人非健医卡和意外医疗险的理赔记录
			sql.append(" UNION ");
			sql.append(SQLConstant.InsuredInfo.CLAIMSNOMEDICSQL);
			tParam.setNameAndValue("INSUREDNO58_0", "string:" + custNo);
			tParam.setNameAndValue("INSUREDNO58_1", "string:" + custNo);
			tParam.setNameAndValue("INSUREDNO58_2", "string:" + custNo);
			tParam.setNameAndValue("INSUREDNO58_3", "string:" + custNo);
		}
		//财富世嘉终身寿险保险累计基本保险金额
        sql.append(" UNION ");
        sql.append(SQLConstant.InsuredInfo.SUMBASICINSURANCEAMOUNTSQL);
		tParam.setNameAndValue("INSUREDNO59","string:"+custNo);
        //C071累计重疾风险保额
		sql.append(" UNION ");
		sql.append(SQLConstant.InsuredInfo.C071LEIJIZHONGJI);
		tParam.setNameAndValue("INSUREDNO60","string:"+custNo);
        //既往有未结案的意外医疗理赔记录
		sql.append(" UNION ");
		sql.append(SQLConstant.InsuredInfo.ACMEDICCLAIMNOTEND);
		tParam.setNameAndValue("INSUREDNO61_0","string:"+custNo);
		tParam.setNameAndValue("INSUREDNO61_1","string:"+custNo);
		tParam.setNameAndValue("INSUREDNO61_2","string:"+custNo);
		tParam.setNameAndValue("INSUREDNO61_3","string:"+custNo);
		//tParam.setNameAndValue("INSUREDNO61","string:"+custNo);
		//tParam.setNameAndValue("INSUREDNO61","string:"+custNo);
        //被保险人既往有"被'达标体检、抽样体检'自核规则拦截且该保单状态为待人工核保"的保单
		sql.append(" UNION ");
		sql.append(SQLConstant.InsuredInfo.HAVEHELTHRULESQL);
		tParam.setNameAndValue("INSUREDNO88","string:"+custNo);
		tParam.setNameAndValue("CONTNO88","string:"+contno);
		tParam.setNameAndValue("INSUREDNO88","string:"+custNo);
		tParam.setNameAndValue("CONTNO88","string:"+contno);
		//被保险人累计“稳得福C款两全保险（分红型）”保费
		sql.append(" UNION ");
		sql.append(SQLConstant.InsuredInfo.ACCPREM2052SQL);
		tParam.setNameAndValue("INSUREDNO66","string:"+custNo);
		//5018税优健康险投保份数
		sql.append(" UNION ");
		sql.append(SQLConstant.InsuredInfo.COPIES5018SQL);
		tParam.setNameAndValue("INSUREDNO65","string:"+custNo);

		//2052累计期交保费
		sql.append(" UNION ");
		sql.append(SQLConstant.InsuredInfo.SUMSTANDPREM2052SQL);
		tParam.setNameAndValue("INSUREDNO67","string:"+custNo);

		//2052累计期交保费
		sql.append(" UNION ");
		sql.append(SQLConstant.InsuredInfo.SUMSTANDPREM2053SQL);
		tParam.setNameAndValue("INSUREDNO68","string:"+custNo);

		//累计被保人当日所有有效保单的保险费（首期保费+续期保费）
		sql.append(" UNION ");
		sql.append(SQLConstant.InsuredInfo.SUMEFFECTIVEPOLICYSQL);
		tParam.setNameAndValue("INSUREDNO69","string:"+custNo);

		//1043 险种各期保费之和
		sql.append(" UNION ");
		sql.append(SQLConstant.InsuredInfo.RENEWALPREMSUM1043SQL);
		tParam.setNameAndValue("INSUREDNO70","string:"+custNo);

		//判断当日有没有现金缴费方式，存在现金交费 保费上限20000，否则上限200000；
		/*sql.append(" UNION ");
		sql.append(SQLConstant.InsuredInfo.ISPAYMENTCASHSQL);
		tParam.setNameAndValue("INSUREDNO70","string:"+custNo);*/




		// 电销非C012 组合险份数
		/*tParam.setNameAndValue("INSUREDNO11", "string:" + custNo);*/
		// 电销非C009 组合险份数
		/*tParam.setNameAndValue("INSUREDNO12", "string:" + custNo);*/
		// 爱永远定期寿险保费

		// 电销非C014 组合险份数
		/*tParam.setNameAndValue("INSUREDNO14", "string:" + custNo);*/
		//团险重疾累计保额
		// 7054累计保额


		//被保人的团险渠道长期重疾险(7829)有次标准体记录


		//团险渠道长期重疾险(7829)有拒保、延期记录

		//7056险种累计投保额

		//同业拒保次数

		//同业投保次数

		//被保人7059累计基本保额

		//被保人防癌理赔保全记录

		//被保人意外险理赔记录

		//被保人7057险种累计基本保额

		//被保人7058险种累计基本保额

		//被保人1030险种累计基本保额

		//被保人2048险种累计基本保额

		//被保人2048险种累计保费

		// 被保人6009意外险保额

		// 万能险保费，5015险种已停售

		// 电销重疾保额
		/*tParam.setNameAndValue("INSUREDNO15","string:" + custNo);*/
		//被保人c060险种累计保费  --wpq--

        //被保险人累计C060份数 --wpq--

		//被保险人投保7053累计重疾险风险保额 --wpq--

		//非工作单位组织的个人投保标记  --wpq--

		//工作单位首次或再次组织的投保标记  --wpq--

		//被保险人是否已拥有7060或7061的有效保单 2018/3/27 zfx

		//被保险人既往有非团险账户的医疗险种的理赔记录 2018/3/27 zfx
		/*tParam.setNameAndValue("INSUREDNO51","string:"+ custNo);
		tParam.setNameAndValue("INSUREDNO52","string:"+ custNo);*/
		//C066 险种各期保费之和 2018/5/31 xzh

		// C070组合规则 保险计划累计保额

		// C070组合规则 被保险人累计C070份数

		//被保人意外医疗险累计理赔给付金额

		//被保人健医卡理赔给付金额

		//被保人非健医卡和意外医疗险的理赔记录

		//财富世嘉终身寿险保险累计基本保险金额

		//C071累计重疾风险保额



		logger.info("InsuredInfo：" + sql);
		tVData.add(sql.toString());
		tVData.add(tParam);
		long beginTime = System.currentTimeMillis();
		SSRS tSSRS = new SSRS();
		Connection tConnection = null;
		try {
			tConnection = DBConnPool.getConnection("basedataSource");
			ExeSQL exeSql = new ExeSQL(tConnection);
			tSSRS = exeSql.execSQL(tVData);
		}catch (Exception e){
			e.printStackTrace();
			logger.error(ExceptionUtils.exceptionToString(e));
			tradeInfo.addError(ExceptionUtils.exceptionToString(e));
			return;
		}finally {
			if (tConnection != null) {
				try {
					tConnection.close();
				} catch (SQLException e) {
					e.printStackTrace();
					tradeInfo.addError(ExceptionUtils.exceptionToString(e));
					logger.error(ExceptionUtils.exceptionToString(e));
					return;
				}
			}
		}
		if(tSSRS == null){
			logger.debug("保单号：" + lcContPojo.getContNo() + "，被保人访问老核心SQL发生异常！");
			tradeInfo.addError("保单号：" + lcContPojo.getContNo() + "，被保人访问老核心SQL发生异常！");
			return;
		}
		logger.debug("被保人拓展-提数：" + (System.currentTimeMillis() - beginTime) / 1000.0 + "s");
		int flag = 0;
		boolean edge = false;
		int field = 0;
		if (null != tSSRS && tSSRS.MaxRow > 0) {
			for (int j = 1; j <= tSSRS.MaxRow; j++) {
				// 取flag标记，（1:是否为残疾人 2:黑名单标记 3:身高异常 4:体重异常 5:BMI异常）
				flag = Util.toInt(tSSRS.GetText(j, 2));
				switch (flag) {
					/*case 1:
						// 是否为残疾人
						insured.setDisabledSign(true);
						break;*/
					case 2:
						// 黑名单标记
						insured.setBlackSign(true);
						break;
					case 3:
						// 身高异常
						insured.setHeightErr(true);
						break;
					case 4:
						// 体重异常
						insured.setWeightErr(true);
						break;
					case 5:
						// BMI异常
						insured.setBmiErr(true);
						break;
					case 6:
						// 6009险意外险风险保额
						insured.setSum6009AccidentAmount(Util
								.toDouble(tSSRS.GetText(j, 1)));
						break;
					case 7:
						// 万能险保额
						insured.setSumStandPrem(Util.toDouble(tSSRS
								.GetText(j, 1)));
						break;
				/*	case 8:
						// 电销重疾险保额
						insured.setHealthByElectricPin(Util.toDouble(tSSRS
								.GetText(j, 1)));
						break;
					case 9:
						// 电销非C01组合险份数
						insured.setInsuredDouble1(Util.toDouble(tSSRS
								.GetText(j, 1)));
						break;
					case 10:
						// 电销非c009组合险份数
						insured.setInsuredDouble2(Util.toDouble(tSSRS
								.GetText(j, 1)));
						break;*/
					case 11:
						// 爱永远定期寿险保费
						insured.setInsuredString1(tSSRS.GetText(j, 1));
						break;
					/*case 12:
						// 爱永远定期寿险保费
						insured.setInsuredDouble3(Util.toDouble(tSSRS
								.GetText(j, 1)));
						break;*/
					/*case 13:
						insured.setMotorLicence(true);
						break;*/
					case 14:
						// 被保人为白名单客户
						insured.setWhiteSign(true);
						break;
					case 15:
						// 被保人为白名单限额
						insured.setWhiteSignAmnt(Util.toDouble(tSSRS
								.GetText(j, 1)));
						break;

					case 16:
						// 被保人团险累计重疾保额
						map.put("团险累计重疾保额", tSSRS.GetText(j, 1));
						break;

					case 17:
						// 7054险种累计保额
						map.put("7054险种累计保额", tSSRS.GetText(j, 1));
						break;

					case 18:
						//团险渠道长期重疾险(7829)有拒保、延期记录
						boolean flag18 = "1".equals(tSSRS.GetText(j, 1)) ? true : false;
						insured.setRefuseRecord7829(flag18);

						break;

					case 19:
						//被保人的团险渠道长期重疾险(7829)有次标准体记录
						boolean flag19 = "1".equals(tSSRS.GetText(j, 1)) ? true : false;
						insured.setSubStandardRecode7829(flag19);

						break;

					case 20:
						//增加7056险种累计投保额
						if("".equals(tSSRS.GetText(j, 1)) || tSSRS.GetText(j, 1)==null ){
							map.put("7056险种累计投保额", "0");
						}else{
							map.put("7056险种累计投保额", tSSRS.GetText(j, 1));
						}
						break;
					case 21:
						//投保人在其他公司保额
						if("".equals(tSSRS.GetText(j, 1)) || tSSRS.GetText(j, 1)==null ){
							insured.setOtherAmnt(0);
						}else{
							insured.setOtherAmnt(Util.toDouble(tSSRS.GetText(j, 1)));
						}
						break;

					case 22:
						//黑名单1
						field = Integer.parseInt(tSSRS.GetText(j, 1));
						edge = field>0 ? true : false;
						insured.setInsBlackIdFlag(edge);
						break;

					case 23:
						//黑名单2
						field = Integer.parseInt(tSSRS.GetText(j, 1));
						edge = field>0 ? true : false;
						insured.setInsBlackChnFlag(edge);
						break;

					case 24:
						//黑名单3
						field = Integer.parseInt(tSSRS.GetText(j, 1));
						edge = field>0 ? true : false;
						insured.setInsBlackFlag1(edge);
						break;

					case 25:
						//黑名单4
						field = Integer.parseInt(tSSRS.GetText(j, 1));
						edge = field>0 ? true : false;
						insured.setInsBlackFlag2(edge);
						break;

					case 26:
						//黑名单5
						field = Integer.parseInt(tSSRS.GetText(j, 1));
						edge = field>=2 ? true : false;
						insured.setInsBlackFlag3(edge);
						break;

					case 27:
						//黑名单6
						field = Integer.parseInt(tSSRS.GetText(j, 1));
						edge = field>0 ? true : false;
						insured.setInsBlackFlag4(edge);
						break;

					case 28:
						//黑名单7
						field = Integer.parseInt(tSSRS.GetText(j, 1));
						edge = field>=2 ? true : false;
						insured.setInsBlackFlag5(edge);
						break;

					case 29:
						//同业拒保标记赋值

						if("".equals(tSSRS.GetText(j, 1)) || tSSRS.GetText(j, 1)==null || tSSRS.GetText(j, 1).equals("null") ){
							field = 0;
						}else{

							field = Integer.parseInt(tSSRS.GetText(j, 1));
						}
						logger.debug("拒保:"+field);
						edge = field>0 ? true : false;
						insured.setDeclineFlag(edge);
						break;

					case 30:
						//同业投保超2次标记赋值

						if("".equals(tSSRS.GetText(j, 1)) || tSSRS.GetText(j, 1)==null || tSSRS.GetText(j, 1).equals("null") ){
							field = 0;
						}else{

							field = Integer.parseInt(tSSRS.GetText(j, 1));
						}
						logger.debug("投保:"+field);
						edge = field>2 ? true : false;
						insured.setApplicationFlag(edge);
						break;

					case 31:
						//被保人 7059险种累计基本保额

						if("".equals(tSSRS.GetText(j, 1)) || tSSRS.GetText(j, 1)==null || tSSRS.GetText(j, 1).equals("null") ){
							field = 0;
							insured.setBasicAmnt7059(field);
						}else{

							insured.setBasicAmnt7059(Util.toDouble(tSSRS.GetText(j, 1)));
						}
						break;

					case 32:
						//被保人有防癌重疾保全记录
						logger.debug("cancer_recodex:"+tSSRS.GetText(j, 1));
						if("".equals(tSSRS.GetText(j, 1)) || tSSRS.GetText(j, 1)==null || tSSRS.GetText(j, 1).equals("null") ){

							insured.setCancerClaimRecord(false);

						}else{
							field = Integer.parseInt(tSSRS.GetText(j, 1));
							edge = field > 0 ? true: false;
							insured.setCancerClaimRecord(edge);
						}
						break;

					case 33:
//							被保人有意外险理赔记录
						logger.debug("accident_recodex:"+tSSRS.GetText(j, 1));
						if("".equals(tSSRS.GetText(j, 1)) || tSSRS.GetText(j, 1)==null || tSSRS.GetText(j, 1).equals("null") ){

							insured.setAccidentClaimRecord(false);

						}else{
							field = Integer.parseInt(tSSRS.GetText(j, 1));
							edge = field > 0 ? true: false;
							insured.setAccidentClaimRecord(edge);
						}
						break;

					case 34:
						//7057累计基本保额
						//logger.debug("7057_amnt:"+tSSRS.GetText(j, 1));
						if("".equals(tSSRS.GetText(j, 1)) || tSSRS.GetText(j, 1)==null || tSSRS.GetText(j, 1).equals("null") ){

							insured.setBasicAmnt7057(0);

						}else{
							insured.setBasicAmnt7057(Util.toDouble(tSSRS.GetText(j, 1)));
						}
						break;

					case 35:
						//7058累计基本保额

						if("".equals(tSSRS.GetText(j, 1)) || tSSRS.GetText(j, 1)==null || tSSRS.GetText(j, 1).equals("null") ){
//								insured.setBasicAmnt7058(0);

						}else{
							amnt7058 += Util.toDouble(tSSRS.GetText(j, 1));
							tradeInfo.addData("amnt7058",amnt7058);
						}
						break;

					case 36:
						//被保人1030累计基础保费之和

						if("".equals(tSSRS.GetText(j, 1)) || tSSRS.GetText(j, 1)==null || tSSRS.GetText(j, 1).equals("null") ){
							insured.setBasikAmnt1030(0);

						}else{
							insured.setBasikAmnt1030(Util.toDouble(tSSRS.GetText(j, 1)));
						}
						break;

					case 37:
						//被保人2048累计基础保费之和

						if("".equals(tSSRS.GetText(j, 1)) || tSSRS.GetText(j, 1)==null || tSSRS.GetText(j, 1).equals("null") ){
							insured.setBasikAmnt2048(0);

						}else{
							insured.setBasikAmnt2048(Util.toDouble(tSSRS.GetText(j, 1)));
						}
						break;

					case 38:
						//被保人2048累计保费
						logger.debug("被保人2048险种累计保费:"+tSSRS.GetText(j, 1));
						if("".equals(tSSRS.GetText(j, 1)) || tSSRS.GetText(j, 1)==null || tSSRS.GetText(j, 1).equals("null") ){
							insured.setAccPrem2048(0);

						}else{
							insured.setAccPrem2048(Util.toDouble(tSSRS.GetText(j, 1)));
						}
						break;
					case 39:
						//被保人c060累计保费  --wpq--
						logger.debug("被保人c060县中计划累计保额:"+tSSRS.GetText(j, 1));
						if("".equals(tSSRS.GetText(j, 1)) || tSSRS.GetText(j, 1)==null || tSSRS.GetText(j, 1).equals("null") ){
							insured.setC060AccAmnt(0);

						}else{
							insured.setC060AccAmnt(Util.toDouble(tSSRS.GetText(j, 1)));
						}
						break;
					case 40:
						//被保险人累计C060份数  --wpq--
						logger.debug("被保险人累计C060份数"+tSSRS.GetText(j, 1));
						insured.setC060Copies(Util.toDouble(tSSRS.GetText(j, 1)));
						break;
					case 41:
						//被保险人投保7053累计重疾险风险保额  --wpq--
						logger.debug("被保险人投保7053累计重疾险风险保额:"+tSSRS.GetText(j, 1));
						if("".equals(tSSRS.GetText(j, 1)) || tSSRS.GetText(j, 1)==null || tSSRS.GetText(j, 1).equals("null") ){
							insured.setAccAmnt7053(0);
						}else{
							insured.setAccAmnt7053(Util.toDouble(tSSRS.GetText(j, 1)));
						}
						break;
					case 43:
						//5018税优健康险投保份数  --wpq--
						logger.debug("5018税优健康险投保份数:"+tSSRS.GetText(j, 1));
						if("".equals(tSSRS.GetText(j, 1)) || tSSRS.GetText(j, 1)==null || tSSRS.GetText(j, 1).equals("null") ){
							insured.setCopies5018(0);
						}else{
							insured.setCopies5018(Util.toInt(tSSRS.GetText(j, 1)));
						}
						break;
					case 44:
						//非工作单位组织的个人投保标记 --wpq--
						if("1".equals(tSSRS.GetText(j,1))){
							insured.setPersonalInsurance(true);
						}else{
							insured.setPersonalInsurance(false);
						}
						break;
					case 45:
						//工作单位首次或再次组织的投保标记  --wpq--
						if("".equals(tSSRS.GetText(j, 1)) || tSSRS.GetText(j, 1)==null || tSSRS.GetText(j, 1).equals("null") ){
							insured.setOrganizationInsurance(" ");
						}if("1".equals(tSSRS.GetText(j, 1))){
						    insured.setOrganizationInsurance("一年内");
					    }if("2".equals(tSSRS.GetText(j, 1))) {
						    insured.setOrganizationInsurance("新入职员工");
					    }
						if("3".equals(tSSRS.GetText(j, 1))) {
							insured.setOrganizationInsurance("一年后");
						}
						break;
					case 46:
						//被保险人是否已拥有7060或7061的有效保单 2018/3/27 zfx
						if("".equals(tSSRS.GetText(j, 1)) || tSSRS.GetText(j, 1)==null || tSSRS.GetText(j, 1).equals("null") ){

							insured.setHave7060Or7061(false);

						}else{
							field = Integer.parseInt(tSSRS.GetText(j, 1));
							edge = field > 0 ? true: false;
							insured.setHave7060Or7061(edge);
						}
						break;
					/*case 47:
						//被保险人既往有非团险账户的医疗险种的理赔记录 2018/3/27 zfx
						if("".equals(tSSRS.GetText(j, 1)) || tSSRS.GetText(j, 1)==null || tSSRS.GetText(j, 1).equals("null") ){

							insured.setHaveClaimsNotMedic(false);

						}else{
							field = Integer.parseInt(tSSRS.GetText(j, 1));
							edge = field > 0 ? true: false;
							insured.setHaveClaimsNotMedic(edge);
						}
						break;*/
					/*case 48:
						//C066 险种各期保费之和

						if("".equals(tSSRS.GetText(j, 1)) || tSSRS.GetText(j, 1)==null || tSSRS.GetText(j, 1).equals("null") ){

							insured.setRenewalPremSumC066(0);

						}else{

							insured.setRenewalPremSumC066(Util.toDouble(tSSRS.GetText(j, 1)));
						}
						break;*/
					case 49:
						// C070组合规则 保险计划累计保额

						if ("".equals(tSSRS.GetText(j, 1))
								|| tSSRS.GetText(j, 1) == null
								|| tSSRS.GetText(j, 1).equals("null")) {

							insured.setMaximumGrant(0);

						} else {

							insured.setMaximumGrant(Util
									.toDouble(tSSRS.GetText(j, 1)));
						}
						break;

					case 50:
						// C070组合规则 被保险人累计C070份数

						if ("".equals(tSSRS.GetText(j, 1))
								|| tSSRS.GetText(j, 1) == null
								|| tSSRS.GetText(j, 1).equals("null")) {

							insured.setC070Copies(0);

						} else {

							insured.setC070Copies(Util.toInt(tSSRS
									.GetText(j, 1)));
						}
						break;
					case 51:
						// C070组合规则 被保险人累计C070份数

						if ("".equals(tSSRS.GetText(j, 1))
								|| tSSRS.GetText(j, 1) == null
								|| tSSRS.GetText(j, 1).equals("null")) {

							insured.setAccidentMedicPay(0);

						} else {

							insured.setAccidentMedicPay(Util.toDouble(tSSRS
									.GetText(j, 1)));
						}
						break;
					case 52:
						// C070组合规则 被保险人累计C070份数

						if ("".equals(tSSRS.GetText(j, 1))
								|| tSSRS.GetText(j, 1) == null
								|| tSSRS.GetText(j, 1).equals("null")) {

							insured.setHelthMedicPay(0);

						} else {

							insured.setHelthMedicPay(Util.toDouble(tSSRS
									.GetText(j, 1)));
						}
						break;
					case 53:
						// C070组合规则 被保险人累计C070份数

						if ("".equals(tSSRS.GetText(j, 1))
								|| tSSRS.GetText(j, 1) == null
								|| tSSRS.GetText(j, 1).equals("null")) {

							insured.setClaimsNoMedic(false);

						} else {
							field = Integer.parseInt(tSSRS.GetText(j, 1));
							edge = field > 0 ? true: false;
							insured.setClaimsNoMedic(edge);
						}
						break;
					case 54:
						// 财富世嘉终身寿险保险累计基本保险金额

						if("".equals(tSSRS.GetText(j, 1)) || tSSRS.GetText(j, 1)==null || tSSRS.GetText(j, 1).equals("null") ){

							insured.setSumBasicInsuranceAmount1040(0);

						}else{

							insured.setSumBasicInsuranceAmount1040(Util.toDouble(tSSRS.GetText(j, 1)));
						}
						break;
					case 56:
						// C071累计重疾保额
                          double C071ZHONGJI = 0.0;
						if("".equals(tSSRS.GetText(j, 1)) || tSSRS.GetText(j, 1)==null || tSSRS.GetText(j, 1).equals("null") ){

							tradeInfo.addData("C071H",C071ZHONGJI);

						}else{
							C071ZHONGJI= Util.toDouble(tSSRS.GetText(j, 1));
							tradeInfo.addData("C071H",C071ZHONGJI);
						}
						break;
					case 57:
						//既往有未结案的意外医疗理赔记录
						if("".equals(tSSRS.GetText(j, 1)) || tSSRS.GetText(j, 1)==null || tSSRS.GetText(j, 1).equals("null") ){

							insured.setAcMedicClaimNotEnd(false);

						}else{
							field = Integer.parseInt(tSSRS.GetText(j, 1));
							edge = field > 0 ? true: false;
							insured.setAcMedicClaimNotEnd(edge);
						}
						break;
					case 58:
						//被保险人既往有"被'达标体检、抽样体检'自核规则拦截且该保单状态为待人工核保"的保单
						if("".equals(tSSRS.GetText(j, 1)) || tSSRS.GetText(j, 1)==null || tSSRS.GetText(j, 1).equals("null") ){

							insured.setHaveHelthRule(false);

						}else{
							field = Integer.parseInt(tSSRS.GetText(j, 1));
							edge = field > 0 ? true: false;
							insured.setHaveHelthRule(edge);
						}
						break;
					case 59:
						//被保险人累计“2052险种”保费
						if("".equals(tSSRS.GetText(j, 1)) || tSSRS.GetText(j, 1)==null || tSSRS.GetText(j, 1).equals("null") ){

							insured.setAccPrem2052(0);

						}else{

							insured.setAccPrem2052(Util.toDouble(tSSRS.GetText(j, 1)));
						}
						break;
					case 60:
						//被保险人2052险种累计期交保费”保费
						if("".equals(tSSRS.GetText(j, 1)) || tSSRS.GetText(j, 1)==null || tSSRS.GetText(j, 1).equals("null") ){

							insured.setSumStandPrem2052(0);

						}else{

							insured.setSumStandPrem2052(Util.toDouble(tSSRS.GetText(j, 1)));
						}
						break;
					case 61:
						//被保险人2052险种累计期交保费”保费
						if("".equals(tSSRS.GetText(j, 1)) || tSSRS.GetText(j, 1)==null || tSSRS.GetText(j, 1).equals("null") ){

							insured.setSumStandPrem2053(0);

						}else{

							insured.setSumStandPrem2053(Util.toDouble(tSSRS.GetText(j, 1)));
						}
						break;
					case 62:
						//累计被保人当日所有有效保单的保险费（首期保费+续期保费）
						if("".equals(tSSRS.GetText(j, 1)) || tSSRS.GetText(j, 1)==null || tSSRS.GetText(j, 1).equals("null") ){

							insured.setSumEffectivePolicy(0);

						}else{

							insured.setSumEffectivePolicy(Util.toDouble(tSSRS.GetText(j, 1)));
						}
						break;
					case 63:
						//1043 险种各期保费之和
						if("".equals(tSSRS.GetText(j, 1)) || tSSRS.GetText(j, 1)==null || tSSRS.GetText(j, 1).equals("null") ){

							insured.setRenewalPremSum1043(0);

						}else{

							insured.setRenewalPremSum1043(Util.toDouble(tSSRS.GetText(j, 1)));
						}
						break;
/*					case 63:

                        //判断当日有没有现金缴费方式，存在现金交费 保费上限20000，否则上限200000；
						if("".equals(tSSRS.GetText(j, 1)) || tSSRS.GetText(j, 1)==null || tSSRS.GetText(j, 1).equals("null") ){

							insured.setHavePaymentCash(false);

						}else{
							field = Integer.parseInt(tSSRS.GetText(j, 1));
							edge = field > 0 ? true: false;
							insured.setHavePaymentCash(edge);
						}

						
						break;*/

				}
			}

		}
	}

	public void getAccidInsured(TradeInfo requestInfo,Insured insured){
		List<LCCIITCCheckPojo> lcciitcCheckPojos=(List<LCCIITCCheckPojo>)requestInfo.getData(LCCIITCCheckPojo.class.getName());
		if (lcciitcCheckPojos!=null && lcciitcCheckPojos.size()!=0){
			if( lcciitcCheckPojos.get(0).getAccidentFlag().equals("1")){
				List<LCCIITCAccidentPojo> lcciitcAccidentPojos=( List<LCCIITCAccidentPojo>)requestInfo.getData(LCCIITCAccidentPojo.class.getName());
				if (lcciitcAccidentPojos.get(0).getRetCode().equals("001")){
					AccidentResponse accidentResponse=new AccidentResponse();
					accidentResponse.setAccumulativeMoney(lcciitcAccidentPojos.get(0).getAccumulativeMoney());
					accidentResponse.setDisability(lcciitcAccidentPojos.get(0).getDisability());
					accidentResponse.setDense(lcciitcAccidentPojos.get(0).getDense());
					accidentResponse.setDisplayPage(lcciitcAccidentPojos.get(0).getDisplayPage());
					accidentResponse.setMajorDiseasePayment(lcciitcAccidentPojos.get(0).getMajorDiseasePayment());
					accidentResponse.setMultiCompany(lcciitcAccidentPojos.get(0).getMultiCompany());
					accidentResponse.setPageQueryCode(lcciitcAccidentPojos.get(0).getPageQueryCode());
					accidentResponse.setRiskCode(lcciitcAccidentPojos.get(0).getProductCode());
					insured.setAccidentResponse(accidentResponse);
				}else {
					getToAccMsg(insured,lcciitcCheckPojos.get(0).getAccidentProductCode());
				}
			}else {
				getToAccMsg(insured,lcciitcCheckPojos.get(0).getAccidentProductCode());
			}
			if (lcciitcCheckPojos.get(0).getMDFlag().equals("1")){
				List<LCCIITCMajorDiseaseCheckPojo> lcciitcMajorDiseaseCheckPojos=( List<LCCIITCMajorDiseaseCheckPojo>)requestInfo.getData(LCCIITCMajorDiseaseCheckPojo.class.getName());
				if (lcciitcMajorDiseaseCheckPojos.get(0).getRetCode().equals("001")){
					MajorDiseaseCheck majorDiseaseCheck=new MajorDiseaseCheck();
					majorDiseaseCheck.setMajorDiseasePayment(lcciitcMajorDiseaseCheckPojos.get(0).getMajorDiseasePayment());
					majorDiseaseCheck.setDisplayPage(lcciitcMajorDiseaseCheckPojos.get(0).getDisplayPage());
					majorDiseaseCheck.setRiskCode(lcciitcMajorDiseaseCheckPojos.get(0).getProductCode());
					majorDiseaseCheck.setMultiCompany(lcciitcMajorDiseaseCheckPojos.get(0).getMultiCompany());
					majorDiseaseCheck.setPageQueryCode(lcciitcMajorDiseaseCheckPojos.get(0).getPageQueryCode());
					majorDiseaseCheck.setMajorDiseaseMoney(lcciitcMajorDiseaseCheckPojos.get(0).getMajorDiseaseMoney());
					majorDiseaseCheck.setDense(lcciitcMajorDiseaseCheckPojos.get(0).getDense());
					majorDiseaseCheck.setChronicDiseasePayment(lcciitcMajorDiseaseCheckPojos.get(0).getChronicDiseasePayment());
					majorDiseaseCheck.setAbnormalPayment(lcciitcMajorDiseaseCheckPojos.get(0).getAbnormalPayment());
					majorDiseaseCheck.setAbnormalCheck(lcciitcMajorDiseaseCheckPojos.get(0).getAbnormalCheck());
					insured.setMajorDiseaseCheck(majorDiseaseCheck);
				}else {
					getToMajCheck(insured,lcciitcCheckPojos.get(0).getMDProductCode());
				}
			}else {
				getToMajCheck(insured,lcciitcCheckPojos.get(0).getMDProductCode());
			}
		}else {
			getToAccMsg(insured,null);
			getToMajCheck(insured,null);
		}
	}

	/**
	 * 组装中保信意外险异常或返回信息异常
	 * @param insured
	 * @param riskCode
	 */
	private void getToAccMsg(Insured insured,String riskCode){
		AccidentResponse accidentResponse=new AccidentResponse();
		accidentResponse.setAccumulativeMoney("N");
		accidentResponse.setDisability("N");
		accidentResponse.setDense("N");
		accidentResponse.setDisplayPage("N");
		accidentResponse.setMajorDiseasePayment("N");
		accidentResponse.setMultiCompany("N");
		accidentResponse.setPageQueryCode("N");
		accidentResponse.setRiskCode(riskCode);
		insured.setAccidentResponse(accidentResponse);
	}
	/**
	 * 组装中保信重疾险异常或返回信息异常
	 */
	private void getToMajCheck(Insured insured,String riskCode){
		MajorDiseaseCheck majorDiseaseCheck=new MajorDiseaseCheck();
		majorDiseaseCheck.setMajorDiseasePayment("N");
		majorDiseaseCheck.setDisplayPage("N");
		majorDiseaseCheck.setRiskCode(riskCode);
		majorDiseaseCheck.setMultiCompany("N");
		majorDiseaseCheck.setPageQueryCode("N");
		majorDiseaseCheck.setMajorDiseaseMoney("N");
		majorDiseaseCheck.setDense("N");
		majorDiseaseCheck.setChronicDiseasePayment("N");
		majorDiseaseCheck.setAbnormalPayment("N");
		majorDiseaseCheck.setAbnormalCheck("N");
		insured.setMajorDiseaseCheck(majorDiseaseCheck);
	}
}
