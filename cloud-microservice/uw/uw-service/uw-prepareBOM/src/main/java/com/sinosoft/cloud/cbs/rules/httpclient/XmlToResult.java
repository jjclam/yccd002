package com.sinosoft.cloud.cbs.rules.httpclient;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import com.sinosoft.cloud.cbs.rules.result.Result;
import com.sinosoft.cloud.cbs.rules.result.UwResult;
import com.sinosoft.utility.ExceptionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;


/**
 * 从响应xml报文中获取Result对象
 *
 * @author dingfan
 */
public class XmlToResult {
    private static final Log logger = LogFactory.getLog(XmlToResult.class);

    public static Result getResult(String responseXml) {
        //截取报文中的返回数据
        responseXml = responseXml.substring(responseXml.indexOf("<Result xmlns=\"\">"), responseXml.indexOf("</Result>") + 9);
        responseXml = responseXml.replaceFirst("<Result xmlns=\"\">", "<Result>");

        Result result = new Result();
        //预定义设置值
        Map ruleDecisionMap = new HashMap();
        //规则返回结果集
        List uwresultList = new ArrayList();

        Document doc = null;
        try {
            doc = DocumentHelper.parseText(responseXml);
        } catch (DocumentException e) {
            e.printStackTrace();
            logger.error(ExceptionUtils.exceptionToString(e));
        }
        Element root = doc.getRootElement();
        String contno = root.elementText("contno");
        String flag = root.elementText("flag");
        String message = root.elementText("message");
        Element map = root.element("ruleDecisionMap");
        Element uw = root.element("uwresultList");
        if (map != null) {
            for (Iterator it = map.elementIterator(); it.hasNext(); ) {
                Element element = (Element) it.next();
                String key = element.elementText("key");
                String value = element.elementText("value");
                ruleDecisionMap.put(key, value);
            }
        }
        if (uw != null) {
            for (Iterator it = uw.elementIterator(); it.hasNext(); ) {
                Element element = (Element) it.next();
                UwResult uwresult = new UwResult();
                String flg = element.elementText("flag");
                String ruleCode = element.elementText("ruleCode");
                String returnInfo = element.elementText("returnInfo");
                String riskCode = element.elementText("riskCode");
                String notRealTimeFlag = element.elementText("notRealTimeFlag");
                String insuredNo = element.elementText("insuredNo");
                String insuredName = element.elementText("insuredName");

                uwresult.setFlag("true".equals(flg));
                uwresult.setRuleCode(ruleCode);
                uwresult.setReturnInfo(returnInfo);
                uwresult.setRiskCode(riskCode);
                uwresult.setNotRealTimeFlag(notRealTimeFlag);
                uwresult.setInsuredNo(insuredNo);
                uwresult.setInsuredName(insuredName);
                uwresultList.add(uwresult);
            }
        }

        result.setContno(contno);    //保单号
        result.setFlag("true".equals(flag));    //保单核保是否通过标志
        result.setMessage(message);        //保单返回信息
        result.setRuleDecisionMap(ruleDecisionMap);        //预定义设置值
        result.setUwresultList(uwresultList);        //规则返回结果集

        return result;
    }
}
