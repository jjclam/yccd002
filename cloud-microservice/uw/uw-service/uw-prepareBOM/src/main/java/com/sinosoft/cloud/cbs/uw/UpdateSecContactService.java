package com.sinosoft.cloud.cbs.uw;

import com.sinosoft.cloud.cbs.bl.UpdateSecContactBL;
import com.sinosoft.cloud.microservice.AbstractMicroService;
import com.sinosoft.cloud.rest.TradeInfo;
import com.sinosoft.lis.entity.LCContPojo;
import com.sinosoft.lis.entity.LCInsuredPojo;
import com.sinosoft.lis.entity.LCPolicyInfoPojo;
import com.sinosoft.utility.ExceptionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @Author:wangshuliang
 * @Description:
 * @Date:Created in 11:04 2018/6/30
 * @Modified by:
 */
@Component("UpdateSecContactService")
public class UpdateSecContactService extends AbstractMicroService {
    private final Log logger = LogFactory.getLog(this.getClass());
    @Autowired
    UpdateSecContactBL updateSecContactBL;

    @Override
    public boolean checkData(TradeInfo tradeInfo) {
        LCContPojo lcContPojo = (LCContPojo) tradeInfo.getData(LCContPojo.class.getName());
        if (lcContPojo == null){
            logger.debug("保单信息为空"+tradeInfo.toString());
            return false;
        }
        List<LCInsuredPojo>  lcInsuredPojos = (List<LCInsuredPojo>) tradeInfo.getData(LCInsuredPojo.class.getName());
        if (lcInsuredPojos==null || lcInsuredPojos.size()<=0){
            logger.debug("被保人信息为空"+tradeInfo.toString());
            return false;
        }
        return true;
    }

    @Override
    public TradeInfo dealData(TradeInfo tradeInfo) {
        logger.debug("开始调用更新特约审批信息"+tradeInfo.toString());
        try{
            tradeInfo = updateSecContactBL.dealData(tradeInfo);
            if (tradeInfo.hasError()){
                logger.debug("更新特约审批信息失败"+tradeInfo.toString());
                tradeInfo.addError("更新特约审批信息失败");
                return  tradeInfo;
            }
        }catch (Exception e){
                logger.error("更新特约审批信息异常"+ExceptionUtils.exceptionToString(e));
                logger.error("更新特约审批信息异常"+tradeInfo.toString());
                tradeInfo.addError("更新特约审批信息异常");
                return tradeInfo;
        }
        return tradeInfo;
    }
}
