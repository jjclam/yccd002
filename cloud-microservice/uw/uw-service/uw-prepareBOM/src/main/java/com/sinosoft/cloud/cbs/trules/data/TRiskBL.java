package com.sinosoft.cloud.cbs.trules.data;

import com.sinosoft.cache.dao.impl.RedisCommonDao;
import com.sinosoft.cloud.cbs.trules.bom.Product;
import com.sinosoft.cloud.cbs.trules.bom.Risk;
import com.sinosoft.cloud.cbs.trules.util.DomainUtils;
import com.sinosoft.cloud.rest.TradeInfo;
import com.sinosoft.lis.entity.LCInsuredPojo;
import com.sinosoft.lis.entity.LCPolPojo;
import com.sinosoft.lis.entity.LMRiskAppPojo;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Author:wangshuliang
 * @Description: 准备险种的信息
 * @Date:Created in 11:06 2018/6/5
 * @Modified by:
 */
@Component
public class TRiskBL {
    private Log logger = LogFactory.getLog(getClass());
    @Autowired
    private RedisCommonDao redisCommonDao;

    /**
     * 获取险种信息
     * @param tradeInfo
     * @return
     */
    public TradeInfo getRisk(TradeInfo tradeInfo) {
        Product product = (Product) tradeInfo.getData(Product.class.getName());
      /*  RiskList risks = product.getRisks();
        List<Risk> riskList = risks.getRisk();*/
        List<Risk> riskList = product.getRisks();
        List<LCInsuredPojo> lcInsuredPojos = (List<LCInsuredPojo>) tradeInfo.getData(LCInsuredPojo.class.getName());
        String contPlanCode = lcInsuredPojos.get(0).getContPlanCode();
        List<LCPolPojo> lcPolPojos = (List<LCPolPojo>)tradeInfo.getData(LCPolPojo.class.getName());
        if (lcPolPojos!=null && lcPolPojos.size()>0){
            String mainRiskCode ="";
            for (LCPolPojo lcpolPojo: lcPolPojos){
                if (lcpolPojo.getPolNo().equals(lcpolPojo.getMainPolNo())){
                    mainRiskCode= lcpolPojo.getRiskCode();
                    break;
                }
            }
            for (LCPolPojo lcpolPojo: lcPolPojos) {
                Risk risk = new Risk();
                //产品组合编码
                risk.setProdGroupCode(contPlanCode);
                //产品组合版本
                risk.setProdGroupVersion(DomainUtils.PRODGROUPVERSION);//报文传入默认V1
                //险种编码
                risk.setRiskCode(lcpolPojo.getRiskCode());
                //险种名称
                LMRiskAppPojo lmRiskAppPojo = redisCommonDao.getEntityRelaDB(LMRiskAppPojo.class,lcpolPojo.getRiskCode());
                if (lmRiskAppPojo==null){
                    logger.debug(lcpolPojo.getRiskCode()+"该险种信息不存在请检查");
                    tradeInfo.addError("该险种信息不存在请检查");
                    return tradeInfo;
                }
                risk.setRiskName(lmRiskAppPojo.getRiskName());
                //险种版本
                risk.setRiskVersion(lcpolPojo.getRiskVersion());
                //保费
                risk.setPrem(lcpolPojo.getPrem());
                //总份数
                risk.setMult((int)lcpolPojo.getMult());
                //主附险标识
                risk.setMainPolFlag(lmRiskAppPojo.getSubRiskFlag());
                //主险险种编码
                risk.setMainRiskCode(mainRiskCode);
                // 险种扩展信息  目前没有详细的需求说 写什么
                riskList.add(risk);
            }
        }

        return tradeInfo;

    }

}
