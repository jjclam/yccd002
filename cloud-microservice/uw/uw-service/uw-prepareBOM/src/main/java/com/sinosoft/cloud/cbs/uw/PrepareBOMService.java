package com.sinosoft.cloud.cbs.uw;

import com.sinosoft.cloud.common.SpringContextUtils;
import com.sinosoft.cloud.microservice.AbstractMicroService;
import com.sinosoft.cloud.rest.TradeInfo;
import com.sinosoft.lis.entity.LCContPojo;
import com.sinosoft.utility.ExceptionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * @project: abc-cloud-microservice
 * @author: yangming
 * @date: 2017/9/17 下午10:14
 * To change this template use File | Settings | File and Code Templates.
 */
@Component("PrepareBOMService")
public class PrepareBOMService extends AbstractMicroService {
    private final Log logger = LogFactory.getLog(this.getClass());
    @Autowired
    GreenChanelBL greenChanelBL;
    //    @Autowired
//    CalRiskAmntService calRiskAmntService;
    @Autowired
    CheckRiskManagementInfoBL checkRiskManagementInfoBL;

    /**获取老核心开启标记*/
    @Value("${cloud.engine.oldCoreFlag}")
    private String oldCoreFlag;

    @Override
    public boolean checkData(TradeInfo requestInfo) {
//        LCAppntPojo tLCAppntPojo = (LCAppntPojo) requestInfo.getData(LCAppntPojo.class.getName());
        LCContPojo tLCContPojo = (LCContPojo) requestInfo.getData(LCContPojo.class.getName());
        /*List<LCInsuredPojo> tLCInsuredList = (List) requestInfo.getData(LCInsuredPojo.class.getName());
        List<LCPolPojo> tLCPolPojoList = (List) requestInfo.getData(LCPolPojo.class.getName());
        List<LCDutyPojo> tLCDutyPojoList = (List) requestInfo.getData(LCDutyPojo.class.getName());
        List<LCAddressPojo> tLCAddressPojoList = (List) requestInfo.getData(LCAddressPojo.class.getName());
        if(tLCAppntPojo == null){
            logger.debug(this.getClass().getName()+",获取tLCAppntPojo出错！");
            return false;
        }
        if(tLCInsuredList == null){
            logger.debug(this.getClass().getName()+",获取tLCInsuredList出错！");
            return false;
        }
        if(tLCPolPojoList == null){
            logger.debug(this.getClass().getName()+",获取tLCPolPojoList出错！");
            return false;
        }
        if(tLCContPojo == null){
            logger.debug(this.getClass().getName()+",获取tLCContPojo出错！");
            return false;
        }
        if(tLCDutyPojoList == null){
            logger.debug(this.getClass().getName()+",获取tLCDutyPojoList出错！");
            return false;
        }
        if(tLCAddressPojoList == null){
            logger.debug(this.getClass().getName()+",获取tLCAddressPojoList出错！");
            return false;
        }*/
        logger.debug("保单号：" + tLCContPojo.getContNo() + "，开始核保！");
        return true;
    }

    @Override
    public TradeInfo dealData(TradeInfo requestInfo) {
        LCContPojo lcContPojo = (LCContPojo) requestInfo.getData(LCContPojo.class.getName());

//        UWByEngineBL tUWByEngineBL = SpringContextUtils.getBeanByClass(UWByEngineBL.class);
        PlanUWByEngineBL planUWByEngineBL = SpringContextUtils.getBeanByClass(PlanUWByEngineBL.class);
        try {
            /**
             * 调合同保单层规则先注释掉lian目前跑套餐层规则
             */
//            tUWByEngineBL.submitData(requestInfo);
            planUWByEngineBL.submitData (requestInfo);
        }catch (Exception e){
            logger.error("保单号：" + lcContPojo.getContNo() + "," + ExceptionUtils.exceptionToString(e));
            logger.error("核保异常！" + requestInfo);
            requestInfo.addError("核保异常！");
            return requestInfo;
        }
        if(requestInfo.hasError()){
            logger.info("核保失败！");
            requestInfo.addError("核保失败！");
            return requestInfo;
        }

        return requestInfo;
    }
}
