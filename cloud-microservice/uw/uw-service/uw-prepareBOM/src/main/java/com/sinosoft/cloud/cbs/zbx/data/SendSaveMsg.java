package com.sinosoft.cloud.cbs.zbx.data;

import com.sinosoft.cloud.bl.AbstractBL;
import com.sinosoft.cloud.common.TradeToStorage;
import com.sinosoft.cloud.mq.MessageQueueProducer;
import com.sinosoft.cloud.rest.TradeInfo;
import com.sinosoft.lis.entity.LCCIITCAccidentPojo;
import com.sinosoft.lis.entity.LCCIITCCheckPojo;
import com.sinosoft.lis.entity.LCCIITCMajorDiseaseCheckPojo;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.utility.ExceptionUtils;
import com.sinosoft.utility.VData;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by sll on 2019/4/24.
 */
@Service
public class SendSaveMsg extends AbstractBL {
    private final Log logger = LogFactory.getLog(getClass());

    @Autowired
    MessageQueueProducer producer;

    @Override
    public TradeInfo dealData(TradeInfo tradeInfo) {
        TradeInfo newTradeInfo=new TradeInfo();

        if (tradeInfo.getData(LCCIITCAccidentPojo.class.getName()) !=null){
            newTradeInfo.addData(LCCIITCAccidentPojo.class.getName(), tradeInfo.getData(LCCIITCAccidentPojo.class.getName()));
            newTradeInfo.doInsert(LCCIITCAccidentPojo.class.getName());
        }
        if (tradeInfo.getData(LCCIITCMajorDiseaseCheckPojo.class.getName()) !=null){
            newTradeInfo.addData(LCCIITCMajorDiseaseCheckPojo.class.getName(), tradeInfo.getData(LCCIITCMajorDiseaseCheckPojo.class.getName()));
            newTradeInfo.doInsert(LCCIITCMajorDiseaseCheckPojo.class.getName());
        }
        if (tradeInfo.getData(LCCIITCCheckPojo.class.getName()) !=null ) {
            newTradeInfo.addData(LCCIITCCheckPojo.class.getName(), tradeInfo.getData(LCCIITCCheckPojo.class.getName()));
            newTradeInfo.doInsert(LCCIITCCheckPojo.class.getName());
        }

        //发送中保信的返回数据给核心
         producer.send("ZBX-MSG", newTradeInfo);

        try {
            TradeToStorage tradeToStorage=new TradeToStorage();
            VData vData=tradeToStorage.transfer(newTradeInfo);
            PubSubmit pubSubmit=new PubSubmit();
            if (!pubSubmit.submitData(vData)){
                //异常
               // tradeInfo.addError("中保信返回数据落库失败,"+tradeInfo.toString());
                logger.error("中保信返回数据落库失败！"+tradeInfo);
                logger.error(pubSubmit.mErrors.getErrContent());
            }else {
                logger.debug("中保信返回数据落库成功！");
            }
        } catch (Exception e) {
            logger.error("中保信返回数据落库异常"+ ExceptionUtils.exceptionToString(e));
            //tradeInfo.addError("中保信返回数据落库异常,"+tradeInfo.toString());
            e.printStackTrace();
        }
        return tradeInfo;
    }

    @Override
    public boolean checkData(TradeInfo requestInfo) {
        if (requestInfo == null) {
            logger.debug("没有传入数据！");
           // requestInfo.addError("没有传入数据！");
            return false;
        }
        return true;
    }
}
