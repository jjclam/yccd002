package com.sinosoft.cloud.cbs.trules.util;

/**
 * 数据源配置枚举类
 * @Author:wangshuliang
 * @Description:   NB 契约库 OLD 个险核心库  TOLD 团险核心库
 * @Date:Created in 9:51 2018/6/28
 * @Modified by:
 */
public enum DataSourceName {
    NB("nb","dataSource"), OLD("old","basedataSource"),TOLD("told","tbasedataSource");
    //源数据库
    private String dataSource;
    //源数据库名称
    private String sourceName;

    public String getDataSource() {
        return dataSource;
    }

    public void setDataSource(String dataSource) {
        this.dataSource = dataSource;
    }

    public String getSourceName() {
        return sourceName;
    }

    public void setSourceName(String sourceName) {
        this.sourceName = sourceName;
    }

    DataSourceName(String dataSource, String sourceName) {
        this.dataSource = dataSource;
        this.sourceName = sourceName;
    }
}
