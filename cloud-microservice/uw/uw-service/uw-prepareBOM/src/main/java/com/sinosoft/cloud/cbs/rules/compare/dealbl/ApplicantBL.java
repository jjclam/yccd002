package com.sinosoft.cloud.cbs.rules.compare.dealbl;

import com.sinosoft.cloud.cbs.rules.bom.Applicant;
import com.sinosoft.cloud.cbs.rules.bom.HistoryRisk;
import com.sinosoft.cloud.cbs.rules.compare.beantoxml.CommonToXml;
import com.sinosoft.cloud.cbs.rules.compare.xmltomap.XmlToMap;
import com.sinosoft.lis.entity.LCContPojo;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Component;

import java.lang.reflect.AccessibleObject;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


/**
 * 判断两个xml中Applicant节点下的数据是否一致.
 */

@Component
public class ApplicantBL {

    private final Log logger = LogFactory.getLog(this.getClass());

    /**
     * 处理方法
     *
     * @param oldApplicant 老核心数据
     * @param newApplicant 累计风险保额数据
     * @return
     */
    public boolean xmlTwoIsSame(Applicant oldApplicant, Applicant newApplicant) {
        logger.debug("比对老核心得到的投保人既往数据和累计风险保额所得到的投保人既往数据开始");
        boolean flag = true;

        if (newApplicant != null && oldApplicant != null) {
            if (newApplicant.getHistoryRiskList() != null && oldApplicant.getHistoryRiskList() != null) {
                if (newApplicant.getHistoryRiskList().size() == oldApplicant.getHistoryRiskList().size()) {
                    for (int j = 0; j < oldApplicant.getHistoryRiskList().size(); j++) {
                        final HistoryRisk oldHistoryRisk = (HistoryRisk) oldApplicant.getHistoryRiskList().get(j);
                        final HistoryRisk newHistoryRisk = (HistoryRisk) newApplicant.getHistoryRiskList().get(j);
                        final Class clazz = oldHistoryRisk.getClass();
                        final Field[] fields = clazz.getDeclaredFields();
                        AccessibleObject.setAccessible(fields, true);
                        for (Field field : fields) {
                            if (field.isAccessible()) {
                                try {
                                    final Object oldValue = field.get(oldHistoryRisk);
                                    final Object newValue = field.get(newHistoryRisk);
                                    if (oldValue != null && newValue != null) {
                                        if (!oldValue.equals(newValue)) {
                                            flag = false;
                                            logger.debug("不一样的字段是：" + field.getName() + ",老核心所获取到的值是：" + oldValue + ",累计风险保额所获取的值是：" + newValue);
                                        }
                                    }
                                } catch (IllegalAccessException e) {
                                    logger.debug("反射时异常");
                                }
                            }
                        }

                    }
                }
            }
        }


        if (newApplicant != null && oldApplicant != null) {
            final Class clazz = oldApplicant.getClass();
            final Field[] fields = clazz.getDeclaredFields();
            AccessibleObject.setAccessible(fields, true);
            for (Field field : fields) {
                if (field.isAccessible()) {
                    if (!"historyRiskList".equals(field.getName())) {
                        try {
                            final Object oldValue = field.get(newApplicant);
                            final Object newValue = field.get(oldApplicant);

                            if (oldValue != null && newValue != null) {
                                if (!oldValue.equals(newValue)) {
                                    flag = false;
                                    logger.debug("不一样的字段是：" + field.getName() + "老核心所获取到的值是：" + oldValue + ",累计风险保额所获取的值是：" + newValue);
                                }
                            }
                        } catch (IllegalAccessException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }

        if (flag) {
            logger.debug("比对老核心得到的投保人既往数据和累计风险保额所得到的投保人既往数据数据一致");
        }

        logger.debug("比对老核心得到的投保人既往数据和累计风险保额所得到的投保人既往数据结束");
        return flag;
    }

/*
    public static void main(String[] args) {
        Applicant oldApplicant = new Applicant();
        oldApplicant.setAccPyAmount(100);
        oldApplicant.setAddrCode("77");
        HistoryRisk oldHistoryRisk = new HistoryRisk();
        oldHistoryRisk.setClientType("mm");
        List<HistoryRisk> oldhistoryRiskList = new ArrayList<HistoryRisk>();
        oldhistoryRiskList.add(oldHistoryRisk);
        oldApplicant.setHistoryRiskList(oldhistoryRiskList);


        Applicant newApplicant = new Applicant();
        newApplicant.setAccPyAmount(200);
        newApplicant.setAddrCode("777");
        HistoryRisk newHistoryRisk = new HistoryRisk();
        newHistoryRisk.setClientType("vv");
        List<HistoryRisk> newhistoryRiskList = new ArrayList<HistoryRisk>();
        newhistoryRiskList.add(newHistoryRisk);
        newApplicant.setHistoryRiskList(newhistoryRiskList);
        ApplicantBL applicantBL = new ApplicantBL();
        applicantBL.xmlTwoIsSame(newApplicant, oldApplicant);
    }*/
}