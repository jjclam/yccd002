package com.sinosoft.cloud.cbs.trules.httpclient;



import com.sinosoft.cloud.cbs.trules.bom.*;
import com.sinosoft.utility.ExceptionUtils;
import com.thoughtworks.xstream.converters.Converter;
import com.thoughtworks.xstream.converters.MarshallingContext;
import com.thoughtworks.xstream.converters.UnmarshallingContext;
import com.thoughtworks.xstream.io.ExtendedHierarchicalStreamWriterHelper;
import com.thoughtworks.xstream.io.HierarchicalStreamReader;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.*;
import java.util.Map.Entry;

/**
 * 处理含有null值对象和Map类型的xstream转换器
 * @author dingfan
 *
 */
public class TNullConverter implements Converter {
	private final Log logger = LogFactory.getLog(this.getClass());
	private Class currentType;
	private final String clazzNames[] = { "AgentCert", "Duty","Risk","TBeneficiaryInfo"};// 定义所要转换的对象及所包含的对象名称
	private final String clazzNodeName[] = {"agentCerts","duties","risks","tBeneficiaryInfo"};
	private final Class clazzClass[] = { AgentCert.class, Duty.class, Risk.class, TBeneficiaryInfo.class};
	private List clazzNamesList;
	private Map clazzClassMap;

	public boolean canConvert(Class type) {
		currentType = type;
		clazzNamesList = Arrays.asList(clazzNames);
		clazzClassMap = new HashMap();
		String name = currentType.getName().substring(currentType.getName().lastIndexOf(".")+1);
		if (clazzNamesList.contains(name)) {
			for(int i=0; i<clazzNodeName.length; i++) {
				clazzClassMap.put(clazzNodeName[i], clazzClass[i]);
			}
			return true;
		} else {
			return false;
		}
	}

	public void marshal(Object source, HierarchicalStreamWriter writer,
			MarshallingContext context) {
		try {
			marshalSuper(source, writer, context, currentType);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(ExceptionUtils.exceptionToString(e));
		}
	}

	private Object getObj(Class clazz, String nodeName, Object source)
			throws Exception {
		Method method = null;
		if(clazz.getDeclaredField(nodeName).getType() == boolean.class) {
			if(nodeName.startsWith("is")) {
				method = clazz.getMethod(nodeName, null);
			}else {
				method = clazz.getMethod("is"
						+ Character.toUpperCase(nodeName.substring(0, 1).toCharArray()[0])
						+ nodeName.substring(1), null);
			}
		}else {
			method = clazz.getMethod("get"
						+ Character.toUpperCase(nodeName.substring(0, 1).toCharArray()[0])
						+ nodeName.substring(1), null);
		}
		Object obj = null;
		if(source==null) {
			source = clazz.newInstance();
		}
		obj = method.invoke(source, new Object[0]);
		return obj;
	}

	private void objConverter(Object source, HierarchicalStreamWriter writer,
			MarshallingContext context, Class clazz, String nodeName,
			Class fieldClazz) throws Exception {
		Object obj = getObj(clazz, nodeName, source);
		writer.startNode(nodeName);
		marshalSuper(obj, writer, context, fieldClazz);
		writer.endNode();
	}

	private void collectionConverter(Object source,
			HierarchicalStreamWriter writer, MarshallingContext context,
			Class clazz, String nodeName, Field field) throws Exception {
		Object obj = getObj(clazz, nodeName, source);
		Collection collection = null;
		if (field.getType().equals(List.class)) {
			collection = (List) obj;
		} else if (field.getType().equals(Set.class)) {
			collection = (Set) obj;
		}
		writer.startNode(nodeName);
		if(collection != null) {
			Iterator it = collection.iterator();
			while(it.hasNext()) {
				Object object = it.next();
				String clazzName = ((Class) clazzClassMap.get(nodeName)).getName();
				clazzName = clazzName.substring(clazzName.lastIndexOf(".")+1);
				writer.startNode(Character.toLowerCase(clazzName.substring(0, 1)
						.toCharArray()[0]) + clazzName.substring(1));
				marshalSuper(object, writer, context, (Class)clazzClassMap.get(nodeName));
				writer.endNode();
			}
		}
		writer.endNode();
	}

	private void basicTypeConverter(Object source,
			HierarchicalStreamWriter writer, MarshallingContext context,
			Class clazz, String nodeName, Class fieldClazz) throws Exception {
		Object obj = getObj(clazz, nodeName, source);
		writer.startNode(nodeName);
		if(fieldClazz.equals(Date.class)) {
			XMLGregorianCalendar gc = null;
			if(obj!=null){
				GregorianCalendar cal = new GregorianCalendar();
				cal.setTime((Date) obj);
				gc = DatatypeFactory.newInstance().newXMLGregorianCalendar(cal);
			}
			writer.setValue(obj == null ? "" : gc.toString());
		}else {
			writer.setValue(obj == null ? "" : obj.toString());
		}
		writer.endNode();
	}

	//Map类型数据转xml
	private void mapConverter(Object source,
			HierarchicalStreamWriter writer, MarshallingContext context,
			Class clazz, String nodeName, Field field) throws Exception {
		Map map = (Map) getObj(clazz, nodeName, source);
		if(map == null)
			return;
		// 对告知类中的Map特别处理，该Map的value是对象
		if("riskExtendInfos".equals(nodeName)) {
			writer.startNode(nodeName);
			for (Iterator iterator = map.entrySet().iterator(); iterator.hasNext();) {
				Entry entry = (Entry) iterator.next();
				ExtendedHierarchicalStreamWriterHelper.startNode(writer,
						"entry", Entry.class);
				writer.startNode("key");
				writer.setValue(entry.getKey().toString());
				writer.endNode();
				writer.startNode("value");
				writer.setValue(entry.getValue().toString());
				writer.endNode();
                writer.endNode();
			}
			writer.endNode();
		}else {
			writer.startNode(nodeName);
			for (Iterator iterator = map.entrySet().iterator(); iterator.hasNext();) {
				Entry entry = (Entry) iterator.next();
				if(entry == null)
					continue;
				ExtendedHierarchicalStreamWriterHelper.startNode(writer,
						"entry", Entry.class);
				writer.startNode("key");
				if(entry.getKey() != null) {
					writer.setValue(entry.getKey().toString());
				}else {
					writer.setValue("");
				}
				writer.endNode();
				writer.startNode("value");
				if(entry.getValue() != null) {
					writer.setValue(entry.getValue().toString());
				}else {
					writer.setValue("");
				}
				writer.endNode();
				writer.endNode();
			}
			writer.endNode();
		}
	}


	private void marshalSuper(Object source, HierarchicalStreamWriter writer,
			MarshallingContext context, Class clazz) throws Exception {
		Field fields[] = clazz.getDeclaredFields();//获取当前类的字段值
		for (int i=0; i<fields.length; i++) {
			Field field = fields[i];
			String nodeName = field.getName().substring(field.getName().lastIndexOf(".")+1);
			Class fieldClazz = field.getType();
			String name = fieldClazz.getName().substring(fieldClazz.getName().lastIndexOf(".")+1);
			if (clazzNamesList.contains(name)) {
				objConverter(source, writer, context, clazz, nodeName,
						fieldClazz);
			} else if(fieldClazz.equals(Map.class)) {
				mapConverter(source, writer, context, clazz, nodeName,
						field);
			} else if (fieldClazz.equals(List.class)) {
				collectionConverter(source, writer, context, clazz, nodeName,
						field);
			} else {
				basicTypeConverter(source, writer, context, clazz, nodeName, fieldClazz);
			}
		}
	}

	public Object unmarshal(HierarchicalStreamReader reader,
			UnmarshallingContext context) {
		return null;
	}
}
