package com.sinosoft.cloud.cbs.trules.entity;

/**
 * @Author:wangshuliang
 * @Description:
 * @Date:Created in 15:37 2018/6/26
 * @Modified by:
 */
public class ApproveExtendInfo {
    private String  approveExtendInfoCode;
    private String approveExtendInfoValue;

    public String getApproveExtendInfoCode() {
        return approveExtendInfoCode;
    }

    public void setApproveExtendInfoCode(String approveExtendInfoCode) {
        this.approveExtendInfoCode = approveExtendInfoCode;
    }

    public String getApproveExtendInfoValue() {
        return approveExtendInfoValue;
    }

    public void setApproveExtendInfoValue(String approveExtendInfoValue) {
        this.approveExtendInfoValue = approveExtendInfoValue;
    }

    @Override
    public String toString() {
        return "ApproveExtendInfo{" +
                "approveExtendInfoCode='" + approveExtendInfoCode + '\'' +
                ", approveExtendInfoValue='" + approveExtendInfoValue + '\'' +
                '}';
    }
}
