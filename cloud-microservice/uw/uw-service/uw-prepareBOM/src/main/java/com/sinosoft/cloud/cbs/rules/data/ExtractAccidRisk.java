package com.sinosoft.cloud.cbs.rules.data;

import com.sinosoft.cloud.cbs.rules.bom.AccidentResponse;
import com.sinosoft.cloud.cbs.rules.bom.MajorDiseaseCheck;
import com.sinosoft.cloud.cbs.rules.bom.Policy;
import com.sinosoft.cloud.rest.TradeInfo;
import com.sinosoft.lis.entity.LCCIITCAccidentPojo;
import com.sinosoft.lis.entity.LCCIITCCheckPojo;
import com.sinosoft.lis.entity.LCCIITCMajorDiseaseCheckPojo;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 提取中保信返回信息
 * Created by zkr on 2019/4/26.
 */
@Service
public class ExtractAccidRisk {
    private final Log logger = LogFactory.getLog(this.getClass());

    public void getAccidRisk(Policy policy, TradeInfo requestInfo){
      List<LCCIITCCheckPojo> lcciitcCheckPojos=(List<LCCIITCCheckPojo>)requestInfo.getData(LCCIITCCheckPojo.class.getName());
      if (lcciitcCheckPojos!=null && lcciitcCheckPojos.size()!=0){
         if( lcciitcCheckPojos.get(0).getAccidentFlag().equals("1")){
            List<LCCIITCAccidentPojo> lcciitcAccidentPojos=( List<LCCIITCAccidentPojo>)requestInfo.getData(LCCIITCAccidentPojo.class.getName());
            if (lcciitcAccidentPojos.get(0).getRetCode().equals("001")){
                AccidentResponse accidentResponse= new AccidentResponse();
                accidentResponse.setAccumulativeMoney(lcciitcAccidentPojos.get(0).getAccumulativeMoney());
                accidentResponse.setDisability(lcciitcAccidentPojos.get(0).getDisability());
                accidentResponse.setDense(lcciitcAccidentPojos.get(0).getDense());
                accidentResponse.setDisplayPage(lcciitcAccidentPojos.get(0).getDisplayPage());
                accidentResponse.setMajorDiseasePayment(lcciitcAccidentPojos.get(0).getMajorDiseasePayment());
                accidentResponse.setMultiCompany(lcciitcAccidentPojos.get(0).getMultiCompany());
                accidentResponse.setPageQueryCode(lcciitcAccidentPojos.get(0).getPageQueryCode());
                accidentResponse.setRiskCode(lcciitcAccidentPojos.get(0).getProductCode());
            }else {
                getToAccMsg(policy,lcciitcCheckPojos.get(0).getAccidentProductCode());
            }
         }else {
             getToAccMsg(policy,lcciitcCheckPojos.get(0).getAccidentProductCode());
         }
         if (lcciitcCheckPojos.get(0).getMDFlag().equals("1")){
             List<LCCIITCMajorDiseaseCheckPojo> lcciitcMajorDiseaseCheckPojos=( List<LCCIITCMajorDiseaseCheckPojo>)requestInfo.getData(LCCIITCMajorDiseaseCheckPojo.class.getName());
             if (lcciitcMajorDiseaseCheckPojos.get(0).getRetCode().equals("001")){
                 MajorDiseaseCheck majorDiseaseCheck=new MajorDiseaseCheck();
                 majorDiseaseCheck.setMajorDiseasePayment(lcciitcMajorDiseaseCheckPojos.get(0).getMajorDiseasePayment());
                 majorDiseaseCheck.setDisplayPage(lcciitcMajorDiseaseCheckPojos.get(0).getDisplayPage());
                 majorDiseaseCheck.setRiskCode(lcciitcMajorDiseaseCheckPojos.get(0).getProductCode());
                 majorDiseaseCheck.setMultiCompany(lcciitcMajorDiseaseCheckPojos.get(0).getMultiCompany());
                 majorDiseaseCheck.setPageQueryCode(lcciitcMajorDiseaseCheckPojos.get(0).getPageQueryCode());
                 majorDiseaseCheck.setMajorDiseaseMoney(lcciitcMajorDiseaseCheckPojos.get(0).getMajorDiseaseMoney());
                 majorDiseaseCheck.setDense(lcciitcMajorDiseaseCheckPojos.get(0).getDense());
                 majorDiseaseCheck.setChronicDiseasePayment(lcciitcMajorDiseaseCheckPojos.get(0).getChronicDiseasePayment());
                 majorDiseaseCheck.setAbnormalPayment(lcciitcMajorDiseaseCheckPojos.get(0).getAbnormalPayment());
                 majorDiseaseCheck.setAbnormalCheck(lcciitcMajorDiseaseCheckPojos.get(0).getAbnormalCheck());
             }else {
                 getToMajCheck(policy,lcciitcCheckPojos.get(0).getMDProductCode());
             }
         }else {
             getToMajCheck(policy,lcciitcCheckPojos.get(0).getMDProductCode());
         }
      }else {
          getToAccMsg(policy,null);
          getToMajCheck(policy,null);
      }
    }

    /**
     * 组装中保信意外险异常或返回信息异常
     * @param policy
     * @param riskCode
     */
    private void getToAccMsg(Policy policy,String riskCode){
        AccidentResponse accidentResponse=new AccidentResponse();
        accidentResponse.setAccumulativeMoney("N");
        accidentResponse.setDisability("N");
        accidentResponse.setDense("N");
        accidentResponse.setDisplayPage("N");
        accidentResponse.setMajorDiseasePayment("N");
        accidentResponse.setMultiCompany("N");
        accidentResponse.setPageQueryCode("N");
        accidentResponse.setRiskCode(riskCode);
    }
    /**
     * 组装中保信重疾险异常或返回信息异常
     */
    private void getToMajCheck(Policy policy,String riskCode){
        MajorDiseaseCheck majorDiseaseCheck=new MajorDiseaseCheck();
        majorDiseaseCheck.setMajorDiseasePayment("N");
        majorDiseaseCheck.setDisplayPage("N");
        majorDiseaseCheck.setRiskCode(riskCode);
        majorDiseaseCheck.setMultiCompany("N");
        majorDiseaseCheck.setPageQueryCode("N");
        majorDiseaseCheck.setMajorDiseaseMoney("N");
        majorDiseaseCheck.setDense("N");
        majorDiseaseCheck.setChronicDiseasePayment("N");
        majorDiseaseCheck.setAbnormalPayment("N");
        majorDiseaseCheck.setAbnormalCheck("N");
    }
}
