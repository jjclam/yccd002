package com.sinosoft.cloud.cbs.trules.entity;

/**
 * @Author:wangshuliang
 * @Description:
 * @Date:Created in 17:34 2018/6/25
 * @Modified by:
 */
public class Message {
    private Head head;

    private Body body;

    public Head getHead() {
        return head;
    }

    public void setHead(Head head) {
        this.head = head;
    }

    public Body getBody() {
        return body;
    }

    public void setBody(Body body) {
        this.body = body;
    }
}
