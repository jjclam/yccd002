package com.sinosoft.cloud.cbs.zbx.jsonclient;

import com.alibaba.fastjson.JSON;
import com.sinosoft.cloud.cbs.rules.data.ErrorCatch;
import com.sinosoft.cloud.cbs.zbx.bom.AccidentRequest;
import com.sinosoft.utility.ExceptionUtils;
import com.sun.jndi.toolkit.url.Uri;
import org.apache.commons.httpclient.*;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.RequestEntity;
import org.apache.commons.httpclient.methods.StringRequestEntity;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Value;

import java.io.*;
import java.net.*;
import java.net.URI;
import java.util.concurrent.TimeUnit;

/**
 * Created by sll on 2019/3/29.
 * 传入请求报文，发送请求，获得响应报文
 */
public class JsonRequestClient {
    private  Log logger = LogFactory.getLog(JsonRequestClient.class);

    public  String doPost(String requestJson, String url,int time){
        String responseJson= "";
        HttpClient httpClient = new HttpClient();
//        //连接超时
        httpClient.getHttpConnectionManager().getParams().setConnectionTimeout(time);
//        //读取时间
       httpClient.getHttpConnectionManager().getParams().setSoTimeout(time);
        PostMethod post = new PostMethod(url);
        int code=0;
        try {
            RequestEntity requestEntity=new StringRequestEntity(requestJson,"application/json","UTF-8");
            post.setRequestEntity(requestEntity);
            long beginTime = System.currentTimeMillis();
            httpClient.executeMethod(post);
            logger.info("调用中保信接口响应时间，用时：" + (System.currentTimeMillis() - beginTime) / 1000.0 + "s");

             code = post.getStatusCode();
           // logger.info("响应编码："+code);
            if(code == HttpStatus.SC_OK ) {
               responseJson = post.getResponseBodyAsString();
                return responseJson;
            }
        }catch(SocketTimeoutException e) {
            //1 响应超时
            logger.warn("中保信客户端程序异常-响应超时"+ExceptionUtils.exceptionToString(e));
        }catch (ConnectTimeoutException e) {
            logger.warn("中保信客户端程序异常-请求超时"+ExceptionUtils.exceptionToString(e));
        }catch (NoRouteToHostException e) {
            logger.warn("中保信客户端程序异常-路由错误连接不到服务端"+ExceptionUtils.exceptionToString(e));
        }catch (ConnectException e) {
            //服务端服务可能未启动
            logger.warn("中保信客户端程序异常-服务端无响应"+ExceptionUtils.exceptionToString(e));
        } catch (Exception  e) {
           logger.warn("中保信客户端程序异常 "+ ExceptionUtils.exceptionToString(e));
        }
        return responseJson=code+"";
    }

}
