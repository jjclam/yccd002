package com.sinosoft.cloud.cbs.trules.data;

import com.sinosoft.cache.dao.impl.RedisCommonDao;
import com.sinosoft.cloud.cbs.function.UWFunction;
import com.sinosoft.cloud.cbs.rules.data.Util;
import com.sinosoft.cloud.cbs.trules.bom.TApplicantInfo;
import com.sinosoft.cloud.cbs.trules.util.DataSourceName;
import com.sinosoft.cloud.cbs.trules.util.DomainUtils;
import com.sinosoft.cloud.cbs.trules.util.MulDataExexSql;
import com.sinosoft.cloud.rest.TradeInfo;
import com.sinosoft.lis.entity.LCAddressPojo;
import com.sinosoft.lis.entity.LCAppntPojo;
import com.sinosoft.lis.entity.LCContPojo;
import com.sinosoft.lis.entity.LCGrpAppntPojo;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.utility.*;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @Author:wangshuliang
 * @Description:提数提取投保人信息
 * @Date:Created in 11:50 2018/6/6
 * @Modified by:
 */
@Component
public class TApplicantInfoBL {
    private Log logger = LogFactory.getLog(getClass());
    @Autowired
    private RedisCommonDao redisCommonDao;
    @Autowired
    UWFunction uwFunction;

    /**
     * 提取投保人信息
     *
     * @param tradeInfo
     * @return
     */
    public TradeInfo getTApplicantInfo(TradeInfo tradeInfo) {
        logger.debug("团险开始提取投保人信息");
        FDate fDate = new FDate();
        LCAppntPojo tLCAppntPojo = (LCAppntPojo) tradeInfo.getData(LCAppntPojo.class.getName());
        LCContPojo lcContPojo = (LCContPojo) tradeInfo.getData(LCContPojo.class.getName());
        if (tLCAppntPojo == null) {
            logger.debug("投保人信息为空" + tradeInfo.toString());
            tradeInfo.addError("投保人信息为空");
            return tradeInfo;
        }
        TApplicantInfo tApplicantInfo = new TApplicantInfo();
        //投保人姓名
        tApplicantInfo.setApplicantName(tLCAppntPojo.getAppntName());
        //投保人英文姓名
        StringBuffer englishName = new StringBuffer();
        if (tLCAppntPojo.getFirstName() == null) {
            englishName.append("").append(".");
        } else {
            englishName.append(tLCAppntPojo.getFirstName()).append(".");
        }
        if (tLCAppntPojo.getLastName() == null) {
            englishName.append("");
        } else {
            englishName.append(tLCAppntPojo.getLastName());
        }
        if (".".equals(englishName.toString())) {
            tApplicantInfo.setApplicantEngName("ABCjieyixian");
        } else {
            tApplicantInfo.setApplicantEngName(String.valueOf(englishName));
        }
        //证件类型
        tApplicantInfo.setApplicantCerttype(DomainUtils.convertIDType(tLCAppntPojo.getIDType()));
        //证件号码
        tApplicantInfo.setApplicantCertno(tLCAppntPojo.getIDNo());
        //性别
        tApplicantInfo.setApplicantGender(DomainUtils.convertSex(tLCAppntPojo.getAppntSex()));
        //出生日期
        tApplicantInfo.setApplicantBirthDate(tLCAppntPojo.getAppntBirthday());
        //投保人年龄
        if (tLCAppntPojo.getAppntBirthday() != null && !"".equals(tLCAppntPojo.getAppntBirthday()) && lcContPojo.getCValiDate() != null && !"".equals(lcContPojo.getCValiDate())) {
            // 年龄
            tApplicantInfo.setApplicantAge(PubFun
                    .calInterval(tLCAppntPojo.getAppntBirthday(), lcContPojo.getCValiDate(), "Y"));
        }
        //投保人国籍
        tApplicantInfo.setNationality(tLCAppntPojo.getNationality());
        //证件有效期
        tApplicantInfo.setIdValidDate(tLCAppntPojo.getIdValiDate());
        //与被保人的关系
        tApplicantInfo.setRelaToInsurd(tLCAppntPojo.getRelatToInsu());
        //职业类别
        tApplicantInfo.setOccupationCat(tLCAppntPojo.getOccupationType());
        List<LCAddressPojo> lcAddressPojos = (List<LCAddressPojo>) tradeInfo.getData(LCAddressPojo.class.getName());
        LCAddressPojo lcAddressPojoAppnt = new LCAddressPojo();
        for (LCAddressPojo lcAddressPojo : lcAddressPojos) {
            if (lcAddressPojo.getCustomerNo().equals(tLCAppntPojo.getAppntNo())) {
                lcAddressPojoAppnt = lcAddressPojo;
                //联系电话
                tApplicantInfo.setContactTel(lcAddressPojo.getMobile());
                //联系地址
                tApplicantInfo.setContactAddr(lcAddressPojo.getHomeAddress());
                //联系地址编码
                tApplicantInfo.setContactAddrno(lcAddressPojo.getZipCode());
                //固定电话
                tApplicantInfo.setPhone(lcAddressPojo.getHomePhone());
                //countryCat 国家等级   境外险需要 目前不需要
                //tApplicantInfo.setCountryCat("");
                //单位名称
                tApplicantInfo.setCompanyName(lcAddressPojo.getGrpName());
                //单位地址
                tApplicantInfo.setCompanyAddr(lcAddressPojo.getCompanyAddress());
                //单位电话
                tApplicantInfo.setCompanyTel(lcAddressPojo.getCompanyPhone());
                //投保人邮箱
                tApplicantInfo.setApplMailBox(lcAddressPojo.getEMail());
                //省
                tApplicantInfo.setProv(lcAddressPojo.getProvince());
                //市
                tApplicantInfo.setCity(lcAddressPojo.getCity());
                //区
                tApplicantInfo.setZone(lcAddressPojo.getCounty());
            }
        }

        LCGrpAppntPojo lcGrpAppntPojo = (LCGrpAppntPojo) tradeInfo.getData(LCGrpAppntPojo.class.getName());
        if (lcGrpAppntPojo != null) {
            //增值税一般纳税人资质日期
            tApplicantInfo.setVatGenerTaxpayerQualifDate(lcGrpAppntPojo.getAddTaxDate());
            //统一社会信用代码  中介确认是页面录入,（投保规则），不需要
            //tApplicantInfo.setUniSocialCreditCode("");
            //开户银行全称
            tApplicantInfo.setOpenbankFullName(lcGrpAppntPojo.getBankTaxName());
            //银行账号
            tApplicantInfo.setBankAccno(lcGrpAppntPojo.getBankAccTaxNo());
            //税务登记证号
            tApplicantInfo.setTaxRegCertNo(lcGrpAppntPojo.getTaxCode());
        }
        //3个（含3）不同投保人存在相同电话号码

     /*   String companyPhone = redisCommonDao.getAgentPhone(lcAddressPojoAppnt.getCompanyPhone());
        boolean comFlag = checkPhone(tLCAppntPojo.getAppntName(), companyPhone);
        String homePhone = redisCommonDao.getAppntPhone(lcAddressPojoAppnt.getHomePhone());
        boolean homeFlag = checkPhone(tLCAppntPojo.getAppntName(), homePhone);
        String mobile = redisCommonDao.getAppntPhone(lcAddressPojoAppnt.getMobile());
        boolean mobileFlag = checkPhone(tLCAppntPojo.getAppntName(), mobile);
        String phone = redisCommonDao.getAppntPhone(lcAddressPojoAppnt.getPhone());
        boolean phoneFlag = checkPhone(tLCAppntPojo.getAppntName(), phone);
        if(comFlag || homeFlag || mobileFlag ||phoneFlag){
            tApplicantInfo.setRepeatPhoneByThreeName("1"); // 重复
        }else{
            tApplicantInfo.setRepeatPhoneByThreeName("0"); //不重复
        }*/
        tApplicantInfo.setRepeatPhoneByThreeName("0"); // 重复
        //投保人地址信息
        LCAddressPojo lcAddressPojo = ((List<LCAddressPojo>) tradeInfo.getData(LCAddressPojo.class.getName())).get(0);
        //电话集合
        List<String> phones = new ArrayList<>();

        // 投保人ID
        String appntno = lcContPojo.getAppntNo();
        //投保人姓名
        // String appntname = applicantInfo.getApplicantName();
        //投保人通讯电话
        String phone = lcAddressPojo.getPhone();
        if (phone != null && !"".equals(phone)) {
            phones.add(phone);
        }
        //投保人家庭电话
        String homePhone = lcAddressPojo.getHomePhone();
        if (homePhone != null && !"".equals(homePhone)) {
            phones.add(homePhone);
        }
        //投保人单位电话
        String companyPhone = lcAddressPojo.getCompanyPhone();
        if (companyPhone != null && !"".equals(companyPhone)) {
            phones.add(companyPhone);
        }
        //投保人手机号
        String mobile = lcAddressPojo.getMobile();
        if (mobile != null && !"".equals(mobile)) {
            phones.add(mobile);
        }

        VData tVData = new VData();
        TransferData tParam = new TransferData();
        StringBuffer sql = new StringBuffer();
        if (phones.size() > 0) {
            sql.append("select  customerno from lcaddress where exists (select 'x' from lcappnt where appntno = lcaddress.customerno and addressno=lcaddress.addressno) and customerno != ? and (mobile in (?, ?, ?, ?) or companyphone in (?, ?, ?, ?) or homephone in (?, ?, ?, ?) or phone in (?, ?, ?, ?))");
            tVData.add(sql.toString());
            tVData.add(tParam);
            tParam.setNameAndValue("CustomerNo", "string:" + appntno);
            for (int j = 0; j < 4; j++) {
                for (int i = 0; i < 4; i++) {
                    if (i < phones.size()) {
                        tParam.setNameAndValue("phone", "string:" + phones.get(i));
                    } else {
                        tParam.setNameAndValue("phone", "string:" + phones.get(0));
                    }
                }
            }
            logger.debug("提取电话相同投保人姓名不同的数据：" + sql);
            SSRS tSSRS = new SSRS();
            try {
                MulDataExexSql mulDataExexSql = new MulDataExexSql();
                tSSRS = mulDataExexSql.excuteSql(tVData, DataSourceName.TOLD.getSourceName());
            } catch (Exception e) {
                logger.error(ExceptionUtils.exceptionToString(e)+tradeInfo.toString());
                tradeInfo.addError("团险提取投保人电话失败"+ExceptionUtils.exceptionToString(e));
                return tradeInfo;
            }
            Set<String> customerNoSet  = new HashSet<String>();
            if (null != tSSRS && tSSRS.MaxRow > 0) {
                for (int i = 1; i < tSSRS.getMaxRow(); i++) {
                    customerNoSet.add(tSSRS.GetText(1, 1));
                }
            }
//            try {
//                MulDataExexSql mulDataExexSql = new MulDataExexSql();
//                tSSRS = mulDataExexSql.excuteSql(tVData, DataSourceName.OLD.getSourceName());
//            } catch (Exception e) {
//                logger.error(ExceptionUtils.exceptionToString(e)+tradeInfo.toString());
//                tradeInfo.addError("提取投保人电话失败"+ExceptionUtils.exceptionToString(e));
//                return tradeInfo;
//            }
//            if (null != tSSRS && tSSRS.MaxRow > 0) {
//                for (int i = 1; i < tSSRS.getMaxRow(); i++) {
//                    customerNoSet.add(tSSRS.GetText(1, 1));
//                }
//            }
            if (customerNoSet.size()>=2){
                tApplicantInfo.setRepeatPhoneByThreeName("1");
                tApplicantInfo.setRepeatPhoneByFourName(true);
            }
        }
        //黑名单1-投保人的证件号与黑名单相同，黑名单1-证件号与和黑名单相同,不分国籍,>0触发
        boolean appBackIdFlag = uwFunction.checkBlackIdFlag(tLCAppntPojo.getIDNo());
        tApplicantInfo.setAppBlackIdFlag(appBackIdFlag);
        if (!appBackIdFlag){
            //黑名单2-中国客户名字、生日与黑名单相同,>0触发,
            if("CHN".equals(tLCAppntPojo.getNativePlace())){
                boolean checkAppBlackFullFlag = uwFunction.checkBlackCHNFlag(tLCAppntPojo.getAppntName(), tLCAppntPojo.getAppntBirthday());
                tApplicantInfo.setAppBlackFullFlag(checkAppBlackFullFlag);
            }else {
                String[] strName=tLCAppntPojo.getAppntName().split(" ");
                int count=0;
                if (strName.length==1){
                    // 黑名单3-投保人名1部分组成,投保人名与黑名单表四个名字对比,>0触发
                    boolean checkAppBlackFlag1 = uwFunction.checkBlackFlag1(tLCAppntPojo.getAppntName(), tLCAppntPojo.getNativePlace(), tLCAppntPojo.getAppntBirthday());
                    tApplicantInfo.setAppBlackFlag1(checkAppBlackFlag1);
                }
                if (strName.length==2){
                    /**黑名单4-投保人名2部分组成,投保人名与黑名单表2个名字对比,>0触发
                     * 有一个name相同，且出生日期相同
                     */
                    boolean checkAppBlackFlag2 = uwFunction.checkBlackFlag2(tLCAppntPojo.getAppntName(), tLCAppntPojo.getNativePlace(), tLCAppntPojo.getAppntBirthday());
                    tApplicantInfo.setAppBlackFlag2(checkAppBlackFlag2);
                    if (!checkAppBlackFlag2){
                        /**
                         * 黑名单5-投保人名2部分组成,投保人名与黑名单表2个名字对比,>=2触发
                         * firstname和surnname都要相同.无生日限制
                         */
                        count=0;
                        count += uwFunction.checkBlackFlag3_1(strName[0]);
                        count += uwFunction.checkBlackFlag3_3(strName[1]);
                        if(count >= 2){
                            tApplicantInfo.setAppBlackFlag3(true);
                        }
                    }

                }
                if (strName.length>=3){
                    /** 黑名单6-投保人名>=3部分组成,投保人名与黑名单表3个名字对比,>=0触发,参数:3个contno
                     * firstname,middlename,surname,有生日限制 */
                    boolean checkAppBlackFlag4 = uwFunction.checkBlackFlag4(tLCAppntPojo.getAppntName(), tLCAppntPojo.getNativePlace(), tLCAppntPojo.getAppntBirthday());
                    tApplicantInfo.setAppBlackFlag4(checkAppBlackFlag4);
                    if (!checkAppBlackFlag4){
                        /** 黑名单7-投保人名>=3部分组成,投保人名与黑名单表3个名字对比,>=2触发,参数:3个contno
                         * firstname,middlename,surname,无生日限制 */
                        count = 0;
                        String appntName = tLCAppntPojo.getAppntName();
                        String surName = appntName.substring(appntName.indexOf(strName[1])+strName[1].length()+1,appntName.length());
                        count += uwFunction.checkBlackFlag3_1(strName[0]);
                        count += uwFunction.checkBlackFlag3_2(strName[1]);
                        count += uwFunction.checkBlackFlag3_3(surName);
                        if(count >= 2){
                            tApplicantInfo.setAppBlackFlag5(true);
                        }
                    }
                }

            }
        }

        tradeInfo.addData(TApplicantInfo.class.getName(), tApplicantInfo);
        logger.debug("团险开始提取投保人信息完成");
        return tradeInfo;

    }

}
