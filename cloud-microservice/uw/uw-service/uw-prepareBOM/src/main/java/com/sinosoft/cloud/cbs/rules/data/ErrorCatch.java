package com.sinosoft.cloud.cbs.rules.data;

import org.springframework.stereotype.Component;

import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.io.PrintStream;

/**
 * @Auther: pengfang
 * @Date: Created on 2018/01/25 14:40
 * @Description:
 * @Modify by:
 * @Version: 1.0
 */
@Component
public class ErrorCatch {

    /**
     * 原始响应报文
     *
     * @return
     */
    private String getResponseXml() {
        StringBuffer responseXml = new StringBuffer();
        responseXml.append("<Result xmlns=\"\"><contno>");
        responseXml.append("contnotag");
        responseXml.append("</contno><flag>false</flag><message>");
        responseXml.append("errorMessagetag");
        responseXml.append("</message>");
        responseXml.append("<uwresultList><uwresult><flag>false</flag><returnInfo>returnInfotab</returnInfo><riskCode>000000</riskCode><ruleCode>ruleCodetag</ruleCode></uwresult></uwresultList></Result>");
        return responseXml.toString();
    }

    /**
     * 原始请求报文
     * @return
     */
    private String getRequestXml() {
        String requestXml = "<Policy><contno>contnotab</contno><message>errorMessagetab</message></Policy>";
        return requestXml;
    }

    /**
     * 获得错误响应报文2
     *
     * @param contno
     * @param errorMessage
     * @param ruleCode
     * @param returnInfo
     * @return
     */
    public String getErrorResponse2(String contno, String errorMessage, String ruleCode, String returnInfo) {
        String responseXml = getResponseXml();
        responseXml = responseXml.replace("contnotag", contno);
        responseXml = responseXml.replace("errorMessagetag", errorMessage);
        responseXml = responseXml.replace("returnInfotab", returnInfo);
        responseXml = responseXml.replace("ruleCodetag", ruleCode);
        return responseXml;
    }

    /**
     * 获得错误响应报文3
     *
     * @param contno
     * @param errorMessage
     * @param ruleCode
     * @param returnInfo
     * @return
     */
    public String getErrorResponse3(String contno,String errorMessage,String ruleCode,String returnInfo,String errorMessageInfo){
        String responseXml = getResponseXml();
        responseXml = responseXml.replaceAll("contnotag", contno);
        responseXml = responseXml.replaceAll("errorMessagetag",errorMessage);
        responseXml = responseXml.replaceAll("returnInfotab",returnInfo);
        responseXml = responseXml.replaceAll("ruleCodetag",ruleCode);
        responseXml = responseXml.replaceAll("errorInfo",errorMessageInfo);


        return responseXml;
    }

    /**
     * 获得错误请求报文2
     * @param contno
     * @param message
     * @param error
     * @return
     */
    public String getErrorRequest2(String contno, String message, String error) {
        String errorMessage = message + " " + error;
        String requestXml = getRequestXml();
        requestXml = requestXml.replace("contnotag", contno);
        requestXml = requestXml.replace("errorMessagetab", errorMessage);
        return requestXml;
    }

    /**
     * 获得错误响应报文
     * @param contno
     * @param ruleCode
     * @param returnInfo
     * @return
     */
    public String getErrorResponse(String contno,Exception e ,String ruleCode,String returnInfo) {
        String errorMessage = getErrorInfo(e);
        String responseXml = getResponseXml();
        responseXml = responseXml.replace("contnotag", contno);
        responseXml = responseXml.replace("errorMessagetag",errorMessage);
        responseXml = responseXml.replace("returnInfotab",returnInfo);
        responseXml = responseXml.replace("ruleCodetag",ruleCode);
        return responseXml;
    }

    /**
     * 获得报错信息String
     * @param e
     * @return
     */
    private String getErrorInfo(Exception e) {
        e.printStackTrace();
        OutputStream out = new ByteArrayOutputStream();
        PrintStream ps = new PrintStream(out);
        e.printStackTrace(ps);
        return out.toString();
    }

    /**
     * 获得错误请求报文
     * @param contno
     * @return
     */
    public String getErrorRequest(String contno ,Exception e) {
        String errorMessage = getErrorInfo(e);
        String requestXml = getRequestXml();
        requestXml = requestXml.replace("contnotag", contno);
        requestXml = requestXml.replace("errorMessagetab", errorMessage);
        return requestXml;
    }
}
