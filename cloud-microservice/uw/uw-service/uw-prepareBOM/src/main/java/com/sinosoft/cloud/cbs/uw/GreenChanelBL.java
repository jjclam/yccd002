package com.sinosoft.cloud.cbs.uw;

import com.sinosoft.cache.dao.impl.RedisCommonDao;
import com.sinosoft.cloud.bl.AbstractBL;
import com.sinosoft.cloud.rest.TradeInfo;
import com.sinosoft.lis.entity.*;
import com.sinosoft.utility.*;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

/**
 * @Author: zhuyiming
 * @Description: 绿色通道业务类
 * @Date: Created in 13:38 2017/10/21
 * @Modified By
 */
@Service
public class GreenChanelBL extends AbstractBL {
    private final Log logger = LogFactory.getLog(this.getClass());
    @Autowired
    RedisCommonDao redisCommonDao;
    @Value("${cloud.uw.barrier.control}")
    private String barrier;

    @Override
    public TradeInfo dealData(TradeInfo tradeInfo) {
        logger.debug("开始计算绿色通道！");
        LCContPojo lcContPojo = (LCContPojo) tradeInfo.getData(LCContPojo.class.getName());
        int greenFlag = 0;
        //绿色通道
        String saleChnl = lcContPojo.getSaleChnl();
        String insuredNo = lcContPojo.getInsuredNo();
        String agentCode = lcContPojo.getAgentCode();
        String stateType = "25";

        //开始校验是否是星级代理人
        LAAgentStatePojo laAgentStatePojo = redisCommonDao.getEntityRelaDB(LAAgentStatePojo.class, agentCode+"|||"+stateType);
        if(laAgentStatePojo != null && "Y".equals(laAgentStatePojo.getStateValue())){
            lcContPojo.setGreenChnl("3");
            greenFlag = 1;
            logger.debug("代理人是星级代理人！");
        }

        //保单的被保险人在我公司所有保单累计风险保额200万元（含）以上
        if(insuredNo != null && !"".equals(insuredNo)){
            double theAmnt = 0.00;
            List<RiskAmntInfoPojo> riskAmntInfoPojos = (List) tradeInfo.getData(RiskAmntInfoPojo.class.getName());
            String insuredAmnt = "0.00";
            if(riskAmntInfoPojos != null && riskAmntInfoPojos.size()>0){
                insuredAmnt = riskAmntInfoPojos.get(1).getRiskTypeSR();
            }
            if(insuredAmnt != null && !"".equals(insuredAmnt)){
                theAmnt = Double.parseDouble(insuredAmnt);
            }
            if(theAmnt >= 2000000.00){
                logger.debug("保单的被保险人在我公司所有保单累计风险保额200万元（含）以上！");
                lcContPojo.setGreenChnl("2");
                greenFlag = 1;
            }
        }

        //个险
        if("1".equals(saleChnl)){
            if(onePrem(100000, 50000, "1", tradeInfo)){
                lcContPojo.setGreenChnl("1");
                return tradeInfo;
            }

            if(sumPrem(500000,200000,"1", tradeInfo)){
                lcContPojo.setGreenChnl("1");
                return tradeInfo;
            }
        }

        //电销
        if("5".equals(saleChnl)){
            if(onePrem(100000,50000,"5", tradeInfo)){
                lcContPojo.setGreenChnl("1");
                return tradeInfo;
            }

            if(sumPrem(500000,200000,"5", tradeInfo)){
                lcContPojo.setGreenChnl("1");
                return tradeInfo;
            }
        }

        //银代
        if("3".equals(saleChnl)){
            if(onePrem(1000000,100000,"3", tradeInfo)){
                lcContPojo.setGreenChnl("1");
                return tradeInfo;
            }
            if(sumPrem(5000000,400000,"3", tradeInfo)){
                lcContPojo.setGreenChnl("1");
                return tradeInfo;
            }
        }

        //财富
        if("7".equals(saleChnl)){
            if(onePrem(1000000,100000,"7", tradeInfo)){
                lcContPojo.setGreenChnl("1");
                return tradeInfo;
            }
            if(sumPrem(5000000,400000,"7", tradeInfo)){
                lcContPojo.setGreenChnl("1");
                return tradeInfo;
            }
        }

        if(greenFlag == 1){
            return tradeInfo;
        }
        else{
            lcContPojo.setGreenChnl("99");
            return tradeInfo;
        }
    }

    /**
     * 单次期缴
     * Number1 趸交
     * Number2 期缴
     */
    public boolean onePrem(int number1, int number2, String saleChnl, TradeInfo tradeInfo) {
        if("true".equals(barrier)){
            return true;
        }
        LCContPojo lcContPojo = (LCContPojo) tradeInfo.getData(LCContPojo.class.getName());

        StringBuffer sql = new StringBuffer();
        TransferData tTransferData = new TransferData();
        VData tVData = new VData();
        sql.append("SELECT DISTINCT 1 FROM ( SELECT A.CONTNO,SUM(PREM) DSUM ");
        sql.append("FROM LCPOL A WHERE A.APPNTNO=? ");
        sql.append("AND A.APPFLAG IN ('0', '1', '2') ");
        sql.append("AND A.UWFLAG NOT IN ('1', '2', 'a') ");
        sql.append(" AND A.saleChnl = ? ");
        sql.append(" AND A.PAYINTV <> 0 ");
        sql.append(" GROUP BY CONTNO ,A.APPNTNO ) C ");
        sql.append(" WHERE  DSUM >= ? ");
        sql.append(" UNION ");
        sql.append(" SELECT DISTINCT 1 FROM ( SELECT A.CONTNO,SUM(PREM) DSUM  FROM LCPOL A WHERE A.APPNTNO=? ");
        sql.append(" AND A.APPFLAG IN ('0', '1', '2') ");
        sql.append(" AND A.UWFLAG NOT IN ('1', '2', 'a') ");
        sql.append(" AND A.saleChnl = ? ");
        sql.append(" AND A.PAYINTV = 0 ");
        sql.append(" GROUP BY CONTNO ,A.APPNTNO ) C ");
        sql.append(" WHERE  DSUM >= ? ");
        tTransferData.setNameAndValue("0", "String:"+lcContPojo.getAppntNo());
        tTransferData.setNameAndValue("1", "String:"+saleChnl);
        tTransferData.setNameAndValue("2", "String:"+number2);
        tTransferData.setNameAndValue("3", "String:"+lcContPojo.getAppntNo());
        tTransferData.setNameAndValue("4", "String:"+saleChnl);
        tTransferData.setNameAndValue("5", "String:"+number1);
        logger.debug("GreenChanelOnePrem：" + sql);
        tVData.add(sql.toString());
        tVData.add(tTransferData);
        String gOneFlag = null;
        Connection tConnection = null;
        try {
            tConnection = DBConnPool.getConnection("basedataSource");
            ExeSQL exeSql = new ExeSQL(tConnection);
            gOneFlag = exeSql.getOneValue(tVData);
        }catch (Exception e){
            e.printStackTrace();
            logger.error(ExceptionUtils.exceptionToString(e));
            tradeInfo.addError(ExceptionUtils.exceptionToString(e));
            return false;
        }finally {
            if (tConnection != null) {
                try {
                    tConnection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                    tradeInfo.addError(ExceptionUtils.exceptionToString(e));
                    logger.error(ExceptionUtils.exceptionToString(e));
                    return false;
                }
            }
        }

        if ("1".equals(gOneFlag)) {
            return true;
        }
        //绿色通道不算本单 zym
//        else{
//            List<LCPolPojo> lcPolPojos = (List) tradeInfo.getData(LCPolPojo.class.getName());
//            //期缴保费
//            double premPerYear= 0.00;
//            //趸缴保费
//            double premIntv = 0.00;
//            for(LCPolPojo lcPolPojo : lcPolPojos){
//                if(lcPolPojo != null && lcPolPojo.getPayIntv() == 0){
//                    premIntv += lcPolPojo.getPrem();
//                }else{
//                    premPerYear += lcPolPojo.getPrem();
//                }
//            }
//            if(premIntv >= number1 || premPerYear >= number2){
//                return  true;
//            }
//        }
        return false;
    }

    /**
     * 累计期缴
     */
    public boolean sumPrem(int number1, int number2, String saleChnl, TradeInfo tradeInfo) {
        if("true".equals(barrier)){
            return true;
        }
        LCContPojo lcContPojo = (LCContPojo) tradeInfo.getData(LCContPojo.class.getName());

        StringBuffer sql = new StringBuffer();
        TransferData tTransferData = new TransferData();
        VData tVData = new VData();
        sql.append(" SELECT SUM(A.PREM) FROM LCPOL A ");
        sql.append(" WHERE A.APPNTNO = ? ");
        sql.append(" AND A.APPFLAG IN ('0', '1', '2')");
        sql.append(" AND A.UWFLAG NOT IN ('1', '2', 'a') ");
        sql.append(" AND A.saleChnl = ? ");
        sql.append(" AND A.PAYINTV = 0 ");
        tTransferData.setNameAndValue("0", "String:"+lcContPojo.getAppntNo());
        tTransferData.setNameAndValue("1", "String:"+saleChnl);
        tVData.add(sql.toString());
        tVData.add(tTransferData);

        StringBuffer sql1 = new StringBuffer();
        TransferData tTransferData1 = new TransferData();
        VData tVData1 = new VData();
        sql1.append(" SELECT SUM(A.PREM) FROM LCPOL A ");
        sql1.append(" WHERE A.APPNTNO = ? ");
        sql1.append(" and A.APPFLAG IN ('0', '1', '2')");
        sql1.append(" AND A.UWFLAG NOT IN ('1', '2', 'a')");
        sql1.append(" and a.saleChnl = ? ");
        sql1.append(" AND A.PAYINTV <> 0 ");
        tTransferData1.setNameAndValue("0", "String:"+lcContPojo.getAppntNo());
        tTransferData1.setNameAndValue("1", "String:"+saleChnl);
        tVData1.add(sql1.toString());
        tVData1.add(tTransferData1);

        //趸缴保费
        double sumPremIntv = 0.00;
        //期缴保费之和
        double sumPremPerYear = 0.00;
        Connection tConnection = null;
        logger.debug("GreenChanelSumPrem：" + sql);
        try {
            tConnection = DBConnPool.getConnection("basedataSource");
            ExeSQL exeSql = new ExeSQL(tConnection);
            //趸缴保费
            sumPremIntv = "".equals(exeSql.getOneValue(tVData))?0.00:Double.parseDouble(exeSql.getOneValue(tVData));
            //期缴保费之和
            sumPremPerYear = "".equals(exeSql.getOneValue(tVData1))?0.00:Double.parseDouble(exeSql.getOneValue(tVData1));
        }catch (Exception e){
            e.printStackTrace();
            logger.error(ExceptionUtils.exceptionToString(e));
            tradeInfo.addError(ExceptionUtils.exceptionToString(e));
            return false;
        }finally {
            if (tConnection != null) {
                try {
                    tConnection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                    tradeInfo.addError(ExceptionUtils.exceptionToString(e));
                    logger.error(ExceptionUtils.exceptionToString(e));
                    return false;
                }
            }
        }

        //绿色通道不算本单 zym
//        List<LCPolPojo> lcPolPojos = (List) tradeInfo.getData(LCPolPojo.class.getName());
//        for(LCPolPojo lcPolPojo : lcPolPojos){
//            if(lcPolPojo != null && lcPolPojo.getPayIntv() == 0){
//                sumPremIntv += lcPolPojo.getPrem();
//            }else{
//                sumPremPerYear += lcPolPojo.getPrem();
//            }
//        }
        if(sumPremIntv >= number1 || sumPremPerYear >= number2){
            return  true;
        }
        return false;
    }

}
