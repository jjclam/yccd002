package com.sinosoft.cloud.cbs.rules.compare.beantoxml;

import com.sinosoft.cloud.cbs.rules.bom.Applicant;
import com.sinosoft.cloud.cbs.rules.bom.BankSourceInfo;
import com.sinosoft.cloud.cbs.rules.bom.Insured;
import com.sinosoft.cloud.cbs.rules.bom.Risk;
import com.sinosoft.cloud.cbs.rules.httpclient.HttpRequestClient;
import com.sinosoft.cloud.cbs.rules.httpclient.NullConverter;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.List;

/**
 * 转换xml的公共处理类.
 */
public class CommonToXml {

    private static Log logger = LogFactory.getLog(CommonToXml.class);
    private static XStream xstream = null;

    /**
     * 转换xml的公共处理方法
     *
     * @param type  标识
     * @param objct 哪个类
     * @return
     */
    public static String getXml(String type, Object objct) {

        if (xstream == null) {
            //报文格式
            xstream = new XStream(new DomDriver());
            //处理对象属性为null、Map类型的情况
            xstream.registerConverter(new NullConverter());
            if ("Applicant".equals(type)) {
                xstream.alias("Applicant", Applicant.class);
            }
            if ("Insured".equals(type)) {
                xstream.alias("Insured", Insured.class);
                xstream.alias("insuredList", List.class);
            }
            if ("Risk".equals(type)) {
                xstream.alias("Risk", Risk.class);
                xstream.alias("riskList", List.class);
            }
        }

        final String requestXml = xstream.toXML(objct);
        if (requestXml.indexOf("<error>") != -1) {
            logger.debug("ODM请求报文转换失败:");
        }
        if (objct == null) {
            logger.debug("objct对象实例化失败,objct=null");
        }
        return requestXml;
    }
}