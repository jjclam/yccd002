package com.sinosoft.cloud.cbs.trules.entity;

import java.util.List;

/**
 * @Author:wangshuliang
 * @Description:
 * @Date:Created in 17:34 2018/6/25
 * @Modified by:
 */
public class Body {
    private List<QueryInfo> queryInfos;

    public List<QueryInfo> getQueryInfos() {
        return queryInfos;
    }

    public void setQueryInfos(List<QueryInfo> queryInfos) {
        this.queryInfos = queryInfos;
    }
}
