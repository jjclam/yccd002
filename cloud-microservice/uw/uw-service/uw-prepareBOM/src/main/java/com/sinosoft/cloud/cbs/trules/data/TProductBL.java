package com.sinosoft.cloud.cbs.trules.data;

import com.sinosoft.cache.dao.impl.RedisCommonDao;
import com.sinosoft.cloud.cbs.trules.bom.*;
import com.sinosoft.cloud.cbs.trules.util.NumberToCN;
import com.sinosoft.cloud.nb.service.redis.NBRedisCommon;
import com.sinosoft.cloud.rest.TradeInfo;
import com.sinosoft.lis.entity.LCDutyPojo;
import com.sinosoft.lis.entity.LCInsuredPojo;
import com.sinosoft.lis.entity.LCPolPojo;
import com.sinosoft.lis.entity.LDPlanDutyParamPojo;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * @Author:wangshuliang
 * @Description: 准备产品信息
 * @Date:Created in 9:30 2018/6/5
 * @Modified by:
 */
@Component
public class TProductBL {
    private Log logger = LogFactory.getLog(getClass());
    @Autowired
    private TDutyBL tDutyBL;
    @Autowired
    private TRiskBL tRiskBL;
    @Autowired
    private RedisCommonDao redisCommonDao;
    @Autowired
    private NBRedisCommon nbRedisCommon;

    /**
     * 产品信息
     *
     * @param tradeInfo
     * @return
     */
    public TradeInfo getProduct(TradeInfo tradeInfo) {
        logger.debug("开始准备产品信息");
        Product product = new Product();
        //获取责任信息
        tradeInfo.addData(Product.class.getName(), product);
        tDutyBL.getDuty(tradeInfo);
        if (tradeInfo.hasError()) {
            logger.debug("责任信息准备失败" + tradeInfo.toString());
            tradeInfo.addError("责任信息准备失败");
            return tradeInfo;
        }
        //获取险种信息
        List<Risk> risks = new ArrayList<>();
        product.setRisks(risks);
        tRiskBL.getRisk(tradeInfo);
        if (tradeInfo.hasError()) {
            logger.debug("险种信息准备失败" + tradeInfo.toString());
            tradeInfo.addError("险种信息准备失败");
            return tradeInfo;
        }

        //总保费
        String tDutyCode = "";
        int sumPrem = 0;
       // double sumAmnt = 0;
        int tImmunityflagNum = 0;
        List<LCDutyPojo> tLCDutyPojoList = (List) tradeInfo.getData(LCDutyPojo.class.getName());
        List<LCPolPojo> tLCPolPojoList = (List<LCPolPojo>) tradeInfo.getData(LCPolPojo.class.getName());
        for (LCPolPojo lcPolPojo : tLCPolPojoList) {
            //sumAmnt = sumAmnt + lcPolPojo.getAmnt();
            for (int i = 0; tLCDutyPojoList != null && i < tLCDutyPojoList.size(); i++) {
                if (tLCDutyPojoList.get(i).getPolNo() != null && tLCDutyPojoList.get(i).getPolNo().equals(lcPolPojo.getPolNo())) {
                    tDutyCode = tLCDutyPojoList.get(i).getDutyCode();
                    tImmunityflagNum = Integer.parseInt(nbRedisCommon.checkDutyCode(tDutyCode)) > 0 ? tImmunityflagNum + 1 : tImmunityflagNum;
                    if (tDutyCode.length() >= 6 && "03".equals(tDutyCode.substring(4, 6))) {
                        sumPrem += tLCDutyPojoList.get(i).getPrem();
                    } else {
                        if (lcPolPojo.getPolNo() != null && lcPolPojo.getPolNo().equals(lcPolPojo.getMainPolNo())) {
                            switch (lcPolPojo.getPayIntv()) {
                                case 0:
                                    sumPrem += tLCDutyPojoList.get(i).getPrem();
                                    break;
                                case 1:
                                    sumPrem += tLCDutyPojoList.get(i).getPrem() * 12 * lcPolPojo.getPayYears();
                                    break;
                                case 3:
                                    sumPrem += tLCDutyPojoList.get(i).getPrem() * 4 * lcPolPojo.getPayYears();
                                    break;
                                case 6:
                                    sumPrem += tLCDutyPojoList.get(i).getPrem() * 2 * lcPolPojo.getPayYears();
                                    break;
                                case 12:
                                    sumPrem += tLCDutyPojoList.get(i).getPrem() * lcPolPojo.getPayYears();
                                    break;
                            }
                        }
                    }
                }
            }
        }

        //折扣前总保费
        product.setSaleSumPrem(sumPrem);
        product.setSumPrem(sumPrem);
        //总保费大写
        product.setSumPremCapital(NumberToCN.number2CNMontrayUnit(new BigDecimal(product.getSumPrem())));
        List<LCInsuredPojo> lcInsuredPojos = (List<LCInsuredPojo>) tradeInfo.getData(LCInsuredPojo.class.getName());
        String contPlanCode = lcInsuredPojos.get(0).getContPlanCode();
        product.setContPlanCode(contPlanCode);
        List<LDPlanDutyParamPojo> ldPLans =
                redisCommonDao.findByIndexKeyRelaDB(LDPlanDutyParamPojo.class, "ContPlanCode", lcInsuredPojos.get(0).getContPlanCode());
        boolean flag681001 = false;
        boolean flag68100206 = false;
        boolean flag6810030405 = false;
        boolean flag681007 = false;
        String amnt68001 = "0";
        String amnt68002 = "0";
        String amnt68003 = "0";
        String amnt68004 = "0";
        String amnt68005 = "0";
        String amnt68006 = "0";
        String amnt68007 = "0";
        if (ldPLans != null && ldPLans.size() > 0) {
            for (LDPlanDutyParamPojo ldPlanDutyParamPojo : ldPLans) {
                if ("681001".equals(ldPlanDutyParamPojo.getDutyCode())) {
                    flag681001 = true;
                    if ("Amnt".equals(ldPlanDutyParamPojo.getCalFactor())) {
                        amnt68001 = ldPlanDutyParamPojo.getCalFactorValue();
                    }
                    continue;
                }
                if ("681002".equals(ldPlanDutyParamPojo.getDutyCode())) {
                    flag68100206 = true;
                    if ("Amnt".equals(ldPlanDutyParamPojo.getCalFactor())) {
                        amnt68002 = ldPlanDutyParamPojo.getCalFactorValue();
                    }
                    continue;
                }
                if ("681003".equals(ldPlanDutyParamPojo.getDutyCode())) {
                    if ("Amnt".equals(ldPlanDutyParamPojo.getCalFactor())) {
                        amnt68003 = ldPlanDutyParamPojo.getCalFactorValue();
                    }
                    flag6810030405 = true;
                    continue;
                }
                if ("681004".equals(ldPlanDutyParamPojo.getDutyCode())) {
                    flag6810030405 = true;
                    if ("Amnt".equals(ldPlanDutyParamPojo.getCalFactor())) {
                        amnt68004 = ldPlanDutyParamPojo.getCalFactorValue();
                    }
                    continue;
                }
                if ("681005".equals(ldPlanDutyParamPojo.getDutyCode())) {
                    flag6810030405 = true;
                    if ("Amnt".equals(ldPlanDutyParamPojo.getCalFactor())) {
                        amnt68005 = ldPlanDutyParamPojo.getCalFactorValue();
                    }
                    continue;
                }
                if ("681006".equals(ldPlanDutyParamPojo.getDutyCode())) {
                    flag68100206 = true;
                    if ("Amnt".equals(ldPlanDutyParamPojo.getCalFactor())) {
                        amnt68006 = ldPlanDutyParamPojo.getCalFactorValue();
                    }
                    continue;
                }
                if ("681007".equals(ldPlanDutyParamPojo.getDutyCode())) {
                    flag681007 = true;
                    if ("Amnt".equals(ldPlanDutyParamPojo.getCalFactor())) {
                        amnt68007 = ldPlanDutyParamPojo.getCalFactorValue();
                    }
                    continue;
                }
            }
        }
        //保障计划存在出租车意外伤害责任
        product.setEx681007DutyFlag(flag681007);
        //出租车意外伤害责任保额
        product.setPlan681007Amnt(Double.valueOf(amnt68007));
        //存在自驾车意外伤害责任
        product.setEx681001DutyFlag(flag681001);
        //自驾车意外伤害责任保额(计划）
        product.setPlanCarAmnt(Double.valueOf(amnt68001));
        //存在公交车、出租车、长途汽车责任
        product.setExTrafficDutyFlag(flag68100206);
        //长途汽车意外伤害责任保额
        product.setPlan681006Amnt(Double.valueOf(amnt68006));
        //市内公交车意外伤害责任保额
        product.setPlan681002Amnt(Double.valueOf(amnt68002));
        //存在火车、地铁、轮船、飞机意外风险责任
        product.setPlanTraficDutyFlag(flag6810030405);
        //火车、地铁意外伤害保额
        product.setPlan681003Amnt(Double.valueOf(amnt68003));
        //轮船、摆渡意外伤害责任保额
        product.setPlan681004Amnt(Double.valueOf(amnt68004));
        //航班飞机意外伤害责任保额
        product.setPlan681005Amnt(Double.valueOf(amnt68005));
        tradeInfo.addData(Product.class.getName(), product);
        logger.debug("产品信息提数成功。。");
        return tradeInfo;
    }

}
