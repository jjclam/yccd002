package com.sinosoft.cloud.cbs.rules.util;

/**
 * 红利领取方式
 */
public enum  BonusModeEnum {
    SumInterest("1","2"),GetCashMode("2","4"),PayPrem("3","1"), ClearIncrement("4","3"),Other("5","9");
  //转换前，老核心码值
  private String bonusMode;
  //转换后，新核心码值
  private String z_bonusMode;

    BonusModeEnum(String bonusMode, String z_bonusMode) {
        this.bonusMode = bonusMode;
        this.z_bonusMode = z_bonusMode;
    }

    public String getBonusMode() {
        return bonusMode;
    }

    public void setBonusMode(String bonusMode) {
        this.bonusMode = bonusMode;
    }

    public String getZ_bonusMode() {
        return z_bonusMode;
    }

    public void setZ_bonusMode(String z_bonusMode) {
        this.z_bonusMode = z_bonusMode;
    }

    public static String getValue(String bonusMode){
        for (BonusModeEnum bonusModeEnum:BonusModeEnum.values()){
            if (bonusModeEnum.getBonusMode().equals(bonusMode)){
                return bonusModeEnum.getZ_bonusMode();
            }
        }
        return "";
    }
}
