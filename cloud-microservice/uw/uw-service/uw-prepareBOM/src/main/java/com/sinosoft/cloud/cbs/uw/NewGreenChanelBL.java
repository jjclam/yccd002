package com.sinosoft.cloud.cbs.uw;

import com.sinosoft.cache.dao.impl.RedisCommonDao;
import com.sinosoft.cloud.bl.AbstractBL;
import com.sinosoft.cloud.clicent.QueryRiskAmntService;
import com.sinosoft.cloud.rest.TradeInfo;
import com.sinosoft.lis.entity.LAAgentStatePojo;
import com.sinosoft.lis.entity.LCContPojo;
import com.sinosoft.lis.entity.RiskAmntInfoPojo;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by zkr on 2018/6/30.
 */
@Service
public class NewGreenChanelBL extends AbstractBL {
    private final Log logger = LogFactory.getLog(this.getClass());
    @Autowired
    RedisCommonDao redisCommonDao;
    @Value("${cloud.uw.barrier.control}")
    private String barrier;
//    @Autowired
//    QueryRiskAmntService queryRiskAmntService;
    @Override
    public TradeInfo dealData(TradeInfo tradeInfo) {
        long begin=System.currentTimeMillis();
//        logger.debug("开始计算绿色通道调用累计风险保额！"+begin);
//        tradeInfo=queryRiskAmntService.service(tradeInfo);
//        logger.debug("计算绿色通道调用累计风险保额结束！"+(System.currentTimeMillis()-begin)/1000.0+"s");
        LCContPojo lcContPojo = (LCContPojo) tradeInfo.getData(LCContPojo.class.getName());
        int greenFlag = 0;
        //绿色通道
        String saleChnl = lcContPojo.getSaleChnl();
        String insuredNo = lcContPojo.getInsuredNo();
        String agentCode = lcContPojo.getAgentCode();
        String stateType = "25";

        //开始校验是否是星级代理人
        LAAgentStatePojo laAgentStatePojo = redisCommonDao.getEntityRelaDB(LAAgentStatePojo.class, agentCode+"|||"+stateType);
        if(laAgentStatePojo != null && "Y".equals(laAgentStatePojo.getStateValue())){
            lcContPojo.setGreenChnl("3");
            greenFlag = 1;
            logger.debug("代理人是星级代理人！");
        }

        //保单的被保险人在我公司所有保单累计风险保额200万元（含）以上
        if(insuredNo != null && !"".equals(insuredNo)){
            double theAmnt = 0.00;
            List<RiskAmntInfoPojo> riskAmntInfoPojos = (List) tradeInfo.getData(RiskAmntInfoPojo.class.getName());
            String insuredAmnt = "0.00";
            if(riskAmntInfoPojos != null && riskAmntInfoPojos.size()>0){
                insuredAmnt = riskAmntInfoPojos.get(1).getRiskTypeSR();
            }
            if(insuredAmnt != null && !"".equals(insuredAmnt)){
                theAmnt = Double.parseDouble(insuredAmnt);
            }
            if(theAmnt >= 2000000.00){
                logger.debug("保单的被保险人在我公司所有保单累计风险保额200万元（含）以上！");
                lcContPojo.setGreenChnl("2");
                greenFlag = 1;
            }
        }

        //个险
        if("1".equals(saleChnl)){
            if(onePrem(100000, 50000, "1", tradeInfo)){
                lcContPojo.setGreenChnl("1");
                return tradeInfo;
            }

            if(sumPrem(500000,200000,"1", tradeInfo)){
                lcContPojo.setGreenChnl("1");
                return tradeInfo;
            }
        }

        //电销
        if("5".equals(saleChnl)){
            if(onePrem(100000,50000,"5", tradeInfo)){
                lcContPojo.setGreenChnl("1");
                return tradeInfo;
            }

            if(sumPrem(500000,200000,"5", tradeInfo)){
                lcContPojo.setGreenChnl("1");
                return tradeInfo;
            }
        }

        //银代
        if("3".equals(saleChnl)){
            if(onePrem(1000000,100000,"3", tradeInfo)){
                lcContPojo.setGreenChnl("1");
                return tradeInfo;
            }
            if(sumPrem(5000000,400000,"3", tradeInfo)){
                lcContPojo.setGreenChnl("1");
                return tradeInfo;
            }
        }

        //财富
        if("7".equals(saleChnl)){
            if(onePrem(1000000,100000,"7", tradeInfo)){
                lcContPojo.setGreenChnl("1");
                return tradeInfo;
            }
            if(sumPrem(5000000,400000,"7", tradeInfo)){
                lcContPojo.setGreenChnl("1");
                return tradeInfo;
            }
        }

        if(greenFlag == 1){
            return tradeInfo;
        }
        else{
            lcContPojo.setGreenChnl("99");
            return tradeInfo;
        }
    }

    /**
     * 单次期缴
     * Number1 趸交
     * Number2 期缴
     */
    public boolean onePrem(int number1, int number2, String saleChnl, TradeInfo tradeInfo){
        if("true".equals(barrier)){
            return true;
        }
         double dsum1=(double)tradeInfo.getData("dsum1");
         double dsum2=(double)tradeInfo.getData("dsum2");
        if (dsum1>=number1 || dsum2>=number2){
            return true;
        }
        return false;
    }

    /**
     * 累计期缴
     */
    public boolean sumPrem(int number1, int number2, String saleChnl, TradeInfo tradeInfo){
        if("true".equals(barrier)){
            return true;
        }
        //趸缴保费
        double sumPremIntv = (double)tradeInfo.getData("sumPremIntv");
        //期缴保费之和
        double sumPremPerYear = (double)tradeInfo.getData("sumPremPerYear");
        if(sumPremIntv >= number1 || sumPremPerYear >= number2){
            return  true;
        }
        return false;
    }
}
