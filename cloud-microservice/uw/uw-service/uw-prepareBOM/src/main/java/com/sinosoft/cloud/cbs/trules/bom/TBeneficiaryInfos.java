package com.sinosoft.cloud.cbs.trules.bom;

import java.util.List;

/**
 * 受益人信息列表
 *
 */
public class TBeneficiaryInfos {
	
	/**
	 * 受益人信息列表
	 */
	private List<TBeneficiaryInfo> tBeneficiaryInfo;

	public List<TBeneficiaryInfo> gettBeneficiaryInfo() {
		return tBeneficiaryInfo;
	}

	public void settBeneficiaryInfo(List<TBeneficiaryInfo> tBeneficiaryInfo) {
		this.tBeneficiaryInfo = tBeneficiaryInfo;
	}

	

}
