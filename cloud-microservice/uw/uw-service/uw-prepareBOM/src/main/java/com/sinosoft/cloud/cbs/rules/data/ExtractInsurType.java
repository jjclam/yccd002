package com.sinosoft.cloud.cbs.rules.data;

import com.sinosoft.cache.dao.impl.RedisCommonDao;
import com.sinosoft.cloud.cbs.rules.bom.Insured;
import com.sinosoft.cloud.cbs.rules.bom.Policy;
import com.sinosoft.cloud.cbs.rules.bom.Risk;
import com.sinosoft.cloud.cbs.rules.util.BonusModeEnum;
import com.sinosoft.cloud.nb.service.redis.NBRedisCommon;
import com.sinosoft.cloud.rest.TradeInfo;
import com.sinosoft.lis.entity.*;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.utility.*;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * @Author: zhuyiming
 * @Description:
 * @Date: Created in 17:18 2017/9/24
 */
@Service
public class ExtractInsurType {
	private final Log logger = LogFactory.getLog(this.getClass());
	@Autowired
	NBRedisCommon nbRedisCommon;
	@Autowired
	RedisCommonDao redisCommonDao;
	@Value("${cloud.uw.barrier.control}")
	private String barrier;
	private static final String[] riskType={"HC","ZJ","L","H","CQH","J","M","N","PL","Y","ZB","LI"};
	/**
	 * 获取险种列表
	 *
	 * @param policy
	 * @return
	 * @throws Exception
	 */
	public List getInsurTypeList(Policy policy, TradeInfo requestInfo) throws Exception {
		// 险种列表
		List insurTypeList = policy.getRiskList();
		// 险种基本数据
		long beginTime = System.currentTimeMillis();
		List<LCPolPojo> tLCPolPojoList = (List) requestInfo.getData(LCPolPojo.class.getName());
		List<LCPremPojo> lcPremPojoList = (List) requestInfo.getData(LCPremPojo.class.getName());
		// 保险费合计
		double sumPremium = 0;
		// 累计保额
		double sumCoverag = 0;
		// 累计追加保费
		double sumAddPre = 0;
		// 期交基本保险费之和,5013险种已停售
		double sumBasicPremium = 0;

		VData tVData = new VData();
		TransferData tParam = new TransferData();
		StringBuffer sql = new StringBuffer();
		// 六个月内投保次数
		sql.append(SQLConstant.InsurType.INSUREDNUMSQL);
		tVData.add(sql.toString());
		tParam.setNameAndValue("appntno2", "string:"
				+ policy.getApplicant().getClientNo());
		tVData.add(tParam);
		// 保单的保额、保费
		for (int i = 0; i < tLCPolPojoList.size(); i++) {
			sumPremium += tLCPolPojoList.get(i).getPrem();
		}
		logger.debug("InsurType1：" + sql);
		beginTime = System.currentTimeMillis();
		SSRS tSSRS = new SSRS();
		if (!"true".equals(barrier)) {
			Connection tConnection = null;
			try {
				tConnection = DBConnPool.getConnection("basedataSource");
				ExeSQL exeSql = new ExeSQL(tConnection);
				tSSRS = exeSql.execSQL(tVData);
			} catch (Exception e) {
				e.printStackTrace();
				logger.error(ExceptionUtils.exceptionToString(e));
				requestInfo.addError(ExceptionUtils.exceptionToString(e));
				return insurTypeList;
			} finally {
				if (tConnection != null) {
					try {
						tConnection.close();
					} catch (SQLException e) {
						e.printStackTrace();
						requestInfo.addError(ExceptionUtils.exceptionToString(e));
						logger.error(ExceptionUtils.exceptionToString(e));
						return insurTypeList;
					}
				}
			}
		}
		if (tSSRS == null) {
			logger.debug("保单号：" + tLCPolPojoList.get(0).getContNo() + "，查询六个月内投保次数SQL发生异常！");
			requestInfo.addError("保单号：" + tLCPolPojoList.get(0).getContNo() + "，查询六个月内投保次数SQL发生异常！");
			return insurTypeList;
		}
		// 六个月内投保次数
		int insuredNum = 0;
		int flag = 0;
		if (null != tSSRS && tSSRS.MaxRow > 0) {
			for (int i = 1; i <= tSSRS.MaxRow; i++) {
				// 取flag标记，（1:累计保额、保险费合计 2:累计追加保费 3:期交基本保险费之和 4:六个月内投保次数）
				flag = Util.toInt(tSSRS.GetText(i, 3));
				if (flag == 4) {
					// 六个月内投保次数
					insuredNum = Util.toInt(tSSRS.GetText(i, 1));
					break;
				}
			}
		}
		FDate fDate = new FDate();
		if (tLCPolPojoList != null && tLCPolPojoList.size() > 0) {
			for (int i = 0; i < tLCPolPojoList.size(); i++) {
				StringBuffer typeList=new StringBuffer();;
				LMRiskAppPojo tLMRiskAppPojo = redisCommonDao.getEntityRelaDB(LMRiskAppPojo.class, tLCPolPojoList.get(i).getRiskCode());
				 for(int j=0;j<riskType.length;j++){
					 List<LMCalModePojo> lmCalModePojos=nbRedisCommon.getLmCalMode(tLCPolPojoList.get(i).getRiskCode(),riskType[j]);
					 if (lmCalModePojos!=null){
					 	typeList.append(riskType[j]);
					 	typeList.append(",");
					 }
				 }
				 //如果累计风险保额类型列表不为空，需删除最后的逗号
				if (typeList !=null && "".equals(typeList)){
					typeList.deleteCharAt(typeList.length()-1);
				}
				List<LCDutyPojo> tLCDutyPojoList = (List) requestInfo.getData(LCDutyPojo.class.getName());
				if (tLCDutyPojoList != null && tLCDutyPojoList.size() > 0) {
					for (int j = 0; j < tLCDutyPojoList.size(); j++) {
						if (tLCDutyPojoList.get(j) != null && tLCDutyPojoList.get(j).getPolNo().equals(tLCPolPojoList.get(i).getPolNo())) {
							Risk risk = new Risk();
							// 基本保险费
							risk.setBasicPremium(tLCPolPojoList.get(i).getStandPrem());
							// 是否自动垫交
							risk.setAutoPayingInAdvance("1".equals(tLMRiskAppPojo.getAutoPayFlag()) ? true
									: false);
							// 红利领取方式
							risk.setBonusDrawType(tLMRiskAppPojo.getBonusMode());
							// 保险期间
							risk.setCoveragePeriod(tLCPolPojoList.get(i).getInsuYear());
							// 是否主险
							risk.setBasicRisk("S".equals(tLMRiskAppPojo.getSubRiskFlag()) ? false
									: true);
							// 险种代码
							risk.setRiskCode(tLMRiskAppPojo.getRiskCode());
							risk.setRiskNo(tLMRiskAppPojo.getRiskCode());
							risk.setDutyTypeList("H L A");
							risk.setPrem(tLCPolPojoList.get(i).getPrem());
							// 险种类型（L--人寿保险 、 R--年金保险 、 H--健康险 、 A--意外伤害保险 、 S--重疾保险 、
							// U--万能保险）
							risk.setKindCode(tLMRiskAppPojo.getKindCode());
							// 险种名称
							risk.setRiskName(tLMRiskAppPojo.getRiskName());
							// 险种分类（L--寿险(Life)、A--意外险(Accident)、H--健康险(Health)）
							risk.setRiskType(tLMRiskAppPojo.getRiskType());
							// 险种类别（L--长险(Long)、M--一年期险(Middle)、S--极短期险(Short)）
							risk.setRiskPeriod(tLMRiskAppPojo.getRiskPeriod());
							// 险种分类1（默认为空 、 1--定额给付型 、 2--费用报销型 、 3--住院津贴型 、 4--委托管理型 、
							// 5--社保补充型 、 6--意外保险）
							risk.setRiskType1(tLMRiskAppPojo.getRiskType1());
							// 险种分类2（Y--打印年金领取年龄 、 N--不打印年金领取年龄）
							risk.setRiskType2(tLMRiskAppPojo.getRiskType2());
							// 险种分类3（1 传统险 、 2 分红 、 3 投连 、 4 万能 、 5 其他）
							risk.setRiskType3(tLMRiskAppPojo.getRiskType3());
							// 险种分类4（1 终身 、 2 两全及生存 、 3 定期 、 4 年金 、 5 重大疾病 、 6 意外 、 7 健康
							// 、 8 短期综合（含意外和医疗） 、 9 其他）
							risk.setRiskType4(tLMRiskAppPojo.getRiskType4());
							// 险种分类5（1 表示一年期及一年期以内 、 2 表示一年期以上）
							risk.setRiskType5(tLMRiskAppPojo.getRiskType5());
							// 险种细分（A -意外伤害保险（普通意外险） 、 L -人寿保险（含定期） 、 M -医疗保险（指门诊/住院医疗险）
							// 、 P -年金类保险 、 Y-交通意外险 、 N-意外医疗险 、 J-津贴险（住院） 、 H-重疾险 、
							// YJ--意外津贴 、 D -定期疾病保险）
							risk.setRiskTypeDetail(tLMRiskAppPojo.getRiskTypeDetail());
							// 份数
							risk.setQuantity(tLCPolPojoList.get(i).getMult());
							// 停售日
							risk.setStopSellDate(fDate.getDate(tLMRiskAppPojo.getEndDate()));
							// 险种版本号
							risk.setVersion(tLMRiskAppPojo.getRiskVer());
							// 基本保险金额
							risk.setBasicCoverage(tLCPolPojoList.get(i).getAmnt());
							// 缴费年限
							risk.setPayYears(tLCPolPojoList.get(i).getPayYears());
							// 缴费期间单位
							risk.setFeePeriodUnit(tLCPolPojoList.get(i).getPayEndYearFlag());
							// 缴费期间
							risk.setFeePeriod(tLCPolPojoList.get(i).getPayEndYear());
							// 养老金领取频率（1-按年 ， 2-按月）
							risk.setAnnuityFrequency(tLCPolPojoList.get(i).getFQGetMode());
							// 养老金领取期间（1-按年 ， 2-按月）
							risk.setAnnuityPeriod(Util.toInt(tLCPolPojoList.get(i).getFQGetMode()));
							// 首期保险费支付方式
							risk.setFirstPrePayMethod(tLCPolPojoList.get(i).getPayMode());
							// 续期保险费支付方式
							risk.setAfterPrePayMethod(tLCPolPojoList.get(i).getPayLocation());
							// 养老金领取年龄
							risk.setDrawEndownentAge(tLCPolPojoList.get(i).getGetYear());
							// 养老金领取方式
							risk.setEndowmentGetType(tLCPolPojoList.get(i).getLiveGetMode());
							// 养老金给付方式
							risk.setEndowmentPayType(tLCPolPojoList.get(i).getLiveAccFlag());
							// 第一被保人ID
							risk.setInsuredFirstId(tLCPolPojoList.get(i).getInsuredNo());
							// 交费频率
							risk.setPaymentFreqType(tLCPolPojoList.get(i).getPayIntv() + "");
							// 年交保费
							if ("1".equals(risk.getPaymentFreqType())) {
								// 年缴保费（按月交）
								risk.setPremiumPerYear(tLCPolPojoList.get(i).getPrem() * 12);
								//年缴保费2（按月交）
								risk.setRiskDouble1(tLCPolPojoList.get(i).getPrem() * 12);
							} else if ("3".equals(risk.getPaymentFreqType())) {
								// 年缴保费（按季交）
								risk.setPremiumPerYear(tLCPolPojoList.get(i).getPrem() * 4);
								//年缴保费2（按季交）
								risk.setRiskDouble1(tLCPolPojoList.get(i).getPrem() * 4);
							} else if ("12".equals(risk.getPaymentFreqType())) {
								// 年缴保费（按年交）
								risk.setPremiumPerYear(tLCPolPojoList.get(i).getPrem());
								//年缴保费2（按年交）
								risk.setRiskDouble1(tLCPolPojoList.get(i).getPrem());
							} else {
								//	年缴保费（非年交、季交、月交）
								risk.setPremiumPerYear(tLCPolPojoList.get(i).getPrem());
								//年缴保费2（非年交、季交、月交）
								risk.setRiskDouble1(0.0d);
							}
							// 红利领取人
							//risk.setBonusReceiver(tLCPolPojoList.get(i).getBonusMan());
							risk.setBonusReceiver(BonusModeEnum.getValue(tLCPolPojoList.get(i).getBonusMan()));
							// 保险期间单位
							risk.setCoveragePeriodUnit(tLCPolPojoList.get(i).getInsuYearFlag());
							// 保险期间单位
							risk.setPayYears(tLCPolPojoList.get(i).getPayYears());
							// 产品组合编码
							risk.setProdSetCode(tLCPolPojoList.get(i).getProdSetCode());
							// 责任编码
							risk.setDutyCode(tLCDutyPojoList.get(j).getDutyCode());
							// 险种性质
							risk.setRiskProp(tLMRiskAppPojo.getRiskProp());
							// 终交年龄年期
							risk.setPayendyear(tLCPolPojoList.get(i).getPayEndYear());
							// 代理机构内部分类
							risk.setRiskString1(tLCPolPojoList.get(i).getAgentType());
							//险种续保标记
							risk.setRnewFlag(tLCPolPojoList.get(i).getRnewFlag() + "");
							// 保险费合计
							risk.setSumPremium(sumPremium);
							// 累计保额
							risk.setSumCoverage(sumCoverag);
							// add by wangshuliang start 20180130 累计追加保费
							for (int s = 0; s < lcPremPojoList.size(); s++) {
								if (tLCDutyPojoList.get(j).getDutyCode().equals(lcPremPojoList.get(s).getDutyCode())) {
									sumAddPre = lcPremPojoList.get(s).getPrem();
									break;
								}
							}
							// add by wangshuliang end 20180130
							// 累计追加保费
							risk.setSumAddPre(sumAddPre);

							// 期交基本保险费之和
							risk.setSumBasicPremium(sumBasicPremium);

							// 六个月内投保次数
							risk.setInsuredNum(insuredNum);
                            //累计风险保额类型列表
							risk.setRebargainAccTypeList(typeList.toString());

							insurTypeList.add(risk);
						}
					}
				}
			}
		}
		logger.info("险种基础提数：" + (System.currentTimeMillis() - beginTime) / 1000.0 + "s");
		if ("true".equals(barrier)) {
			logger.info("保单号：" + policy.getContNo() + "，险种挡板启动！");
		} else {
			getOtherData(policy, requestInfo);
		}
		return insurTypeList;
	}

	public void getOtherData(Policy policy, TradeInfo tradeInfo) {
		List<LCPolPojo> tLCPolPojoList = (List<LCPolPojo>) tradeInfo.getData(LCPolPojo.class.getName());
		List insurTypeList = policy.getRiskList();
		//Insured insured = (Insured) policy.getInsuredList().get(0);
		List<LCInsuredPojo> lcInsuredPojos = (List<LCInsuredPojo>) tradeInfo.getData(LCInsuredPojo.class.getName());
		LCInsuredPojo lcInsuredPojo = lcInsuredPojos.get(0);
		StringBuffer sql2 = new StringBuffer();
		TransferData tParam1 = new TransferData();
		List<Double> mults = new ArrayList();
		for (int j = 0; j < insurTypeList.size(); j++) {
			Risk risk = (Risk) insurTypeList.get(j);
			// 单数(该险种是第几单)
//			sql2
//					.append(" select (select case  when (SELECT COUNT(1) fROM lcpol WHERE riskcode = ? and SALECHNL = ? ) >500 then  500  else(SELECT COUNT(1) fROM lcpol WHERE riskcode = ? and SALECHNL = ?  ) end from dual) as field1, "
//							+ j + " as cursor, 1 as flag from dual ");
//			sql2.append(" UNION ");
			double mult = 0.0;
			for (int i = 0; i < tLCPolPojoList.size(); i++) {
				if (risk.getRiskCode().equals(tLCPolPojoList.get(i).getRiskCode())) {
					if (tLCPolPojoList.get(i).getPayIntv() == 0) {
						// 该险种趸交保费
						risk.setSinglePremium(tLCPolPojoList.get(i).getPrem());
					}
					if (tLCPolPojoList.get(i).getPayIntv() == 12) {
						// 该险种期交保费
						risk.setPeriodPremium(tLCPolPojoList.get(i).getPrem());
					}
					//本单险种份数
					mult += tLCPolPojoList.get(i).getMult();
					mults.add(mult);
				}
			}
			// 险种份数
/*			sql2
					.append(" select sum(mult) as fiedl1, "
							+ j
							+ " as cursor, 4 as flag  from lcpol where insuredno = ? and riskcode = ? and appflag in ('0', '1', '2')  and uwflag not in ('1', '2', 'a')  and not exists (select contno from lccont where (state like '1002%' or state like '1003%' or state like '1005%') and contno = lcpol.contno)");
			sql2.append(" UNION ");*/
			// 审核标识
			sql2
					.append(" SELECT  cast(A.ADULTFLAG as INT) ,"
							+ j
							+ " as cursor, 5 as flag  FROM RISKAMNT_DEATH A WHERE A.RISKCODE = ? and A.ADULTFLAG  = '0'");

/*			sql2.append(" UNION ");
			// 累计追加保费
			sql2
					.append(" select sum(p.prem) as field1, "
							+ j
							+ " as cursor, 6 as flag FROM lcprem p where polno in(SELECT polno FROM lcpol where insuredno=?) and dutycode = ? ");*/

			for (int i = 0; i < tLCPolPojoList.size(); i++) {
				if (risk.getRiskCode().equals(tLCPolPojoList.get(i).getRiskCode())) {
					String payenddate = tLCPolPojoList.get(i).getPayEndDate();
					String cvalidate = tLCPolPojoList.get(i).getCValiDate();
					String enddate = tLCPolPojoList.get(i).getEndDate();
					//交费期间
					risk.setFeePeriod(PubFun.calInterval3(cvalidate, payenddate, "D"));
					//保险期间
					risk.setDurationOfInsurance(PubFun.calInterval3(cvalidate, enddate, "D"));
					//险种保额
					risk.setBasicCoverage(tLCPolPojoList.get(i).getAmnt());
				}
			}
			// 险种组合份数
			//开门红规则性能优化
		/*	sql2.append(" UNION ");
			sql2
					.append(" select SUM(MULT)  as field1, "
							+ j
							+ " as cursor, 8 as flag    FROM LCPOL M  WHERE PRODSETCODE = ? AND MAINPOLNO     = POLNO  AND INSUREDNO     = ? AND APPFLAG      IN ('0','1','2') AND UWFLAG NOT   IN ('1','2','a')  AND NOT EXISTS  (SELECT CONTNO FROM LCCONT  WHERE (STATE LIKE '1002%'  OR STATE LIKE '1003%'  OR STATE LIKE '1005%')  AND CONTNO = M.CONTNO ) ");
            */
             //再保协议标记
			sql2.append(" UNION ");
			sql2.append("select distinct 1 as field1,"+ j +" as cursor,11 as flag from  lcriskreportinfo where riskcode= ? ");

			if (j != insurTypeList.size() - 1) {
				sql2.append(" UNION ");
			}
			// 单数(该险种是第几单)
//			tParam1.setNameAndValue("riskcode0_" + j, "string:"
//					+ risk.getRiskCode());
//			tParam1.setNameAndValue("salechn0_" + j, "string:"
//					+ policy.getSellChannel());
//			tParam1.setNameAndValue("riskcode1_" + j, "string:"
//					+ risk.getRiskCode());
//			tParam1.setNameAndValue("salechn1_" + j, "string:"
//					+ policy.getSellChannel());
			// 险种份数
			/*tParam1.setNameAndValue("insured1_" + j, "string:"
					+ lcInsuredPojo.getInsuredNo());
			tParam1.setNameAndValue("riskcode_4" + j, "string:"
					+ risk.getRiskCode());*/
			// 审核标识
			tParam1.setNameAndValue("riskcode8_" + j, "string:" + risk.getRiskCode());
			// 累计追加保费
		/*	tParam1.setNameAndValue("insured2_" + j, "string:"
					+ lcInsuredPojo.getInsuredNo());
			tParam1.setNameAndValue("dutycode" + j, "string:"
					+ risk.getDutyCode());*/
			// 险种组合份数
			/*tParam1.setNameAndValue("ProdSetCode1_" + j, "string:"
					+ risk.getProdSetCode());
			tParam1.setNameAndValue("insured3_" + j, "string:"
					+ lcInsuredPojo.getInsuredNo());*/
			//再保协议标记
			tParam1.setNameAndValue("riskcode11_"+j,"string:"+risk.getRiskCode());
		}
		logger.info("InsurType2：" + sql2);
		VData tVData = new VData();
		tVData.add(sql2.toString());
		tVData.add(tParam1);
		SSRS tSSRS = new SSRS();
		Connection tConnection = null;
		long beginTime = System.currentTimeMillis();
		try {
			tConnection = DBConnPool.getConnection("basedataSource");
			ExeSQL exeSql = new ExeSQL(tConnection);
			tSSRS = exeSql.execSQL(tVData);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(ExceptionUtils.exceptionToString(e));
			tradeInfo.addError(ExceptionUtils.exceptionToString(e));
			return;
		} finally {
			if (tConnection != null) {
				try {
					tConnection.close();
				} catch (SQLException e) {
					e.printStackTrace();
					tradeInfo.addError(ExceptionUtils.exceptionToString(e));
					logger.error(ExceptionUtils.exceptionToString(e));
					return;
				}
			}
		}
		if (tSSRS == null) {
			logger.debug("保单号：" + tLCPolPojoList.get(0).getContNo() + "，险种拓展访问老核心SQL发生异常！");
			tradeInfo.addError("保单号：" + tLCPolPojoList.get(0).getContNo() + "，险种拓展访问老核心SQL发生异常！");
			return;
		}
		logger.debug("险种拓展提数：" + (System.currentTimeMillis() - beginTime)
				/ 1000.0 + "s");
		int cursor = 0;
		int flag = 0;
		if (null != tSSRS && tSSRS.MaxRow > 0) {
			for (int i = 1; i <= tSSRS.MaxRow; i++) {
				cursor = Util.toInt(tSSRS.GetText(i, 2));
				flag = Util.toInt(tSSRS.GetText(i, 3));
				Risk risk = (Risk) insurTypeList.get(cursor);
				switch (flag) {
//					case 1:
//						// 单数(该险种是第几单)
//						int count = Util.toInt(tSSRS.GetText(i, 1));
//						risk.setNum(count == 500 ? 500 : (count + 1));
//						break;
					case 2:
						// 该险种趸交保费
						risk.setSinglePremium(Util.toDouble(tSSRS
								.GetText(i, 1)));
						break;
					case 3:
						// 该险种期交保费
						risk.setPeriodPremium(Util.toDouble(tSSRS
								.GetText(i, 1)));
						break;
				/*	case 4:
						risk.setQuantity(Util.toDouble(tSSRS.GetText(i, 1)) + mults.get(cursor));

						break;*/
					case 5:
						// 险种审核标记
						risk.setAdultflag(tSSRS.GetText(i, 1) == null ? "" : tSSRS.GetText(i, 1));
						break;
			/*		case 6:
						// 累计追加保费

						risk.setSumAddPre(Util.toDouble(tSSRS.GetText(i, 1)) + risk.getSumAddPre());
						break;*/
					case 7:
						// 交费期间
						risk.setFeePeriod(Util.toInt(tSSRS.GetText(i, 1)));
						break;
					//组合险份数
					/*case 8:
						risk.setSumMultByProdsetcode(Util.toInt(tSSRS.GetText(
								i, 1)));
                    *//*	if(risk.getProdSetCode() != null && !"".equals(risk.getProdSetCode())){
							risk.setSumMultByProdsetcode(Util.toInt(tSSRS.GetText(
									i, 1)) + 1);
						}*//*
						// alter by wangshuliang 修改组合险份数累计本单为一单 应该为实际单数
						if (risk.getProdSetCode() != null && !"".equals(risk.getProdSetCode())) {
							risk.setSumMultByProdsetcode(Util.toInt(tSSRS.GetText(
									i, 1)) + (int) tLCPolPojoList.get(0).getMult());
						}
						break;*/
					//保险期间
					case 9:
						risk.setDurationOfInsurance(Util.toInt(tSSRS.GetText(
								i, 1)));
						break;
					// 险种保额
					case 10:
						risk.setBasicCoverage(Util.toDouble(tSSRS.GetText(
								i, 1)));
						break;
					// 再保协议标记
					case 11:
						if ("1".equals(tSSRS.GetText(
								i, 1))) {
							risk.setReNewPolFlag(true);
						}else {
							risk.setReNewPolFlag(false);
						}
						break;
				}
			}
		}
	}
}
