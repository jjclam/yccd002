package com.sinosoft.cloud.cbs.rules.bom;


/**
 * 客户信息
 * 
 * @author dingfan
 * 
 */
public class ClientInfo{
	/**
	 * 客户姓名
	 */
	private String clientName;

	/**
	 * 客户号
	 */
	private String clientNo;

	/**
	 * 客户类型
	 */
	private String clientType;

	/**
	 * 邮箱地址
	 */
	private String email;

	/**
	 * 常住地址
	 */
	private String homeAddr;

	/**
	 * 常住地址所在市
	 */
	private String homeCity;

	/**
	 * 常住地址邮编
	 */
	private String homePostcode;

	/**
	 * 常住地址所在省
	 */
	private String homeProv;

	/**
	 * 手机号码
	 */
	private String mobilePhone;

	/**
	 * 通讯地址
	 */
	private String postalAddr;

	/**
	 * 通讯地址所在市
	 */
	private String postalCity;

	/**
	 * 通讯邮编
	 */
	private String postalPostcode;

	/**
	 * 通讯地址所在省
	 */
	private String postalProv;

	/**
	 * 单位名称
	 */
	private String orgName;

	/**
	 * 联系电话
	 */
	private String phones;

	/**
	 * 固定电话
	 */
	private String telPhone;

	/**
	 * 首选回访电话
	 */
	private String firstOptionPhone;
	
	/**
	 * 客户姓名与恐怖分子、恐怖组织名单或武器禁运名单相同
	 */
	private boolean sameNameTerrorist;
	
	/**
	 * 客户证件类型和号码与恐怖分子、恐怖组织名单或武器禁运名单相同
	 */
	private boolean sameTypeTerrorist;
	
	/**
	 * 常住地址所在区/县
	 */
	private String homeCounty;
	
	/**
	 * 常住地址所在镇（乡）/街道
	 */
	private String homeTown;
	
	/**
	 * 常住地址所在村/社区（楼、号）
	 */
	private String homeCommunity;
	
	/**
	 * 通讯地址所在村/社区（楼、号）
	 */
	private String postalCommunity;
	
	/**
	 * 固定电话区号
	 */
	private String zoneCode;

	public ClientInfo() {
		super();
	}

	public String getClientName() {
		return clientName;
	}

	public void setClientName(String clientName) {
		this.clientName = clientName;
	}

	public String getClientNo() {
		return clientNo;
	}

	public void setClientNo(String clientNo) {
		this.clientNo = clientNo;
	}

	public String getClientType() {
		return clientType;
	}

	public void setClientType(String clientType) {
		this.clientType = clientType;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFirstOptionPhone() {
		return firstOptionPhone;
	}

	public void setFirstOptionPhone(String firstOptionPhone) {
		this.firstOptionPhone = firstOptionPhone;
	}

	public String getHomeAddr() {
		return homeAddr;
	}

	public void setHomeAddr(String homeAddr) {
		this.homeAddr = homeAddr;
	}

	public String getHomeCity() {
		return homeCity;
	}

	public void setHomeCity(String homeCity) {
		this.homeCity = homeCity;
	}

	public String getHomeCommunity() {
		return homeCommunity;
	}

	public void setHomeCommunity(String homeCommunity) {
		this.homeCommunity = homeCommunity;
	}

	public String getHomeCounty() {
		return homeCounty;
	}

	public void setHomeCounty(String homeCounty) {
		this.homeCounty = homeCounty;
	}

	public String getHomePostcode() {
		return homePostcode;
	}

	public void setHomePostcode(String homePostcode) {
		this.homePostcode = homePostcode;
	}

	public String getHomeProv() {
		return homeProv;
	}

	public void setHomeProv(String homeProv) {
		this.homeProv = homeProv;
	}

	public String getHomeTown() {
		return homeTown;
	}

	public void setHomeTown(String homeTown) {
		this.homeTown = homeTown;
	}

	public String getMobilePhone() {
		return mobilePhone;
	}

	public void setMobilePhone(String mobilePhone) {
		this.mobilePhone = mobilePhone;
	}

	public String getOrgName() {
		return orgName;
	}

	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}

	public String getPhones() {
		return phones;
	}

	public void setPhones(String phones) {
		this.phones = phones;
	}

	public String getPostalAddr() {
		return postalAddr;
	}

	public void setPostalAddr(String postalAddr) {
		this.postalAddr = postalAddr;
	}

	public String getPostalCity() {
		return postalCity;
	}

	public void setPostalCity(String postalCity) {
		this.postalCity = postalCity;
	}

	public String getPostalCommunity() {
		return postalCommunity;
	}

	public void setPostalCommunity(String postalCommunity) {
		this.postalCommunity = postalCommunity;
	}

	public String getPostalPostcode() {
		return postalPostcode;
	}

	public void setPostalPostcode(String postalPostcode) {
		this.postalPostcode = postalPostcode;
	}

	public String getPostalProv() {
		return postalProv;
	}

	public void setPostalProv(String postalProv) {
		this.postalProv = postalProv;
	}

	public boolean isSameNameTerrorist() {
		return sameNameTerrorist;
	}

	public void setSameNameTerrorist(boolean sameNameTerrorist) {
		this.sameNameTerrorist = sameNameTerrorist;
	}

	public boolean isSameTypeTerrorist() {
		return sameTypeTerrorist;
	}

	public void setSameTypeTerrorist(boolean sameTypeTerrorist) {
		this.sameTypeTerrorist = sameTypeTerrorist;
	}

	public String getTelPhone() {
		return telPhone;
	}

	public void setTelPhone(String telPhone) {
		this.telPhone = telPhone;
	}

	public String getZoneCode() {
		return zoneCode;
	}

	public void setZoneCode(String zoneCode) {
		this.zoneCode = zoneCode;
	}
}
