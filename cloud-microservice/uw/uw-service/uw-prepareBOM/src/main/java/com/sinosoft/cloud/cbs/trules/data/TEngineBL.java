package com.sinosoft.cloud.cbs.trules.data;

import com.sinosoft.cloud.bl.AbstractBL;


import com.sinosoft.cloud.cbs.rules.result.Result;
import com.sinosoft.cloud.cbs.rules.result.UwResult;
import com.sinosoft.cloud.cbs.trules.bom.*;
import com.sinosoft.cloud.cbs.trules.httpclient.THttpRequestClient;
import com.sinosoft.cloud.cbs.trules.httpclient.TPolicyToXml;

import com.sinosoft.cloud.cbs.trules.httpclient.TXmlToResult;
import com.sinosoft.cloud.cbs.trules.specontract.SearchHistoryInfoSrvBL;
import com.sinosoft.cloud.cbs.trules.specontract.UWSpecialContractBL;
import com.sinosoft.cloud.cbs.trules.util.TFTPRequest;
import com.sinosoft.cloud.common.SpringContextUtils;
import com.sinosoft.cloud.rest.TradeInfo;
import com.sinosoft.lis.entity.*;
import com.sinosoft.utility.ExceptionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @Author:wangshuliang
 * @Description:
 * @Date:Created in 10:24 2018/6/11
 * @Modified by:
 */
@Component
public class TEngineBL extends AbstractBL {
    private final Log logger = LogFactory.getLog(this.getClass());
    @Autowired
    private TProductBL tProductBL;
    @Autowired
    private TAgentInfoBL tAgentInfoBL;
    @Autowired
    private TLcInsuredBL tLcInsuredBL;
    @Autowired
    private TProdGroupBL tProdGroupBL;
    @Autowired
    private TApplicantInfoBL tApplicantInfoBL;
    @Autowired
    private TBeneficiaryInfoBL tBeneficiaryInfoBL;
    @Autowired
    private TInsurdInfoBL tInsurdInfoBL;
    @Autowired
    private TPayInfoBL tPayInfoBL;
    @Autowired
    private TProductInfoBL tProductInfoBL;
    @Autowired
    private TPolicyBL tPolicyBL;
    @Autowired
    private TErrorCatch tErrorCatch;

    /**
     * 获取团险规则引擎地址
     */
    @Value("${cloud.tengine.ruleUrl}")
    private String url;
    /**
     * 获取ftp标记 1为报文落地，0为不落地
     */
    @Value("${cloud.engine.ftpFlag}")
    private String ftpFlag;
    @Value("${cloud.engine.barrier.tcontrol}")
    private String engineFlag;

    @Override
    public TradeInfo dealData(TradeInfo tradeInfo) {
        try {
            long beginTime = System.currentTimeMillis();
            long productbeginTime = System.currentTimeMillis();
            tProductBL.getProduct(tradeInfo);
            if (tradeInfo.hasError()) {
                logger.debug("产品信息提数失败" + tradeInfo.toString());
                tradeInfo.addError("产品信息提数失败");
                return tradeInfo;
            }
            logger.debug("团险性能监控,产品信息提数时间:" + (System.currentTimeMillis() - productbeginTime)/1000.0 + "s");
            long TAgentbeginTime = System.currentTimeMillis();
            tAgentInfoBL.getTAgentInfo(tradeInfo);
            if (tradeInfo.hasError()) {
                logger.debug("代理人信息提数失败" + tradeInfo.toString());
                tradeInfo.addError("代理人信息提数失败");
                return tradeInfo;
            }
            logger.debug("团险性能监控,代理信息提数时间:" + (System.currentTimeMillis() - TAgentbeginTime)/1000.0 + "s");
            long LcInsuredbeginTime = System.currentTimeMillis();
            tLcInsuredBL.getLcInsuredBL(tradeInfo);
            if (tradeInfo.hasError()) {
                logger.debug("被保人信息提数失败" + tradeInfo.toString());
                tradeInfo.addError("被保人信息提数失败");
                return tradeInfo;
            }
            logger.debug("团险性能监控,被保人信息提数时间:" + (System.currentTimeMillis() - LcInsuredbeginTime)/1000.0 + "s");
            long ProdGroupbeginTime = System.currentTimeMillis();
            tProdGroupBL.getProdGroupBL(tradeInfo);
            if (tradeInfo.hasError()) {
                logger.debug("产品组合信息提数失败" + tradeInfo.toString());
                tradeInfo.addError("产品组合信息提数失败");
                return tradeInfo;
            }
            logger.debug("团险性能监控,产品组合信息提数时间:" + (System.currentTimeMillis() - ProdGroupbeginTime)/1000.0 + "s");
            long TApplicantInfobeginTime = System.currentTimeMillis();
            tApplicantInfoBL.getTApplicantInfo(tradeInfo);
            if (tradeInfo.hasError()) {
                logger.debug("投保人信息提数失败" + tradeInfo.toString());
                tradeInfo.addError("投保人信息提数失败");
                return tradeInfo;
            }
            logger.debug("团险性能监控,投保人信息提数时间:" + (System.currentTimeMillis() - TApplicantInfobeginTime)/1000.0 + "s");
            long TBeneficiaryInfoTime = System.currentTimeMillis();
            tBeneficiaryInfoBL.getTBeneficiaryInfo(tradeInfo);
            if (tradeInfo.hasError()) {
                logger.debug("受益人信息提数失败" + tradeInfo.toString());
                tradeInfo.addError("受益人信息提数失败");
                return tradeInfo;
            }
            logger.debug("团险性能监控,受益人信息提数时间:" + (System.currentTimeMillis() - TBeneficiaryInfoTime)/1000.0 + "s");
            long TInsurdInfoTime = System.currentTimeMillis();
            tInsurdInfoBL.getTInsurdInfoBL(tradeInfo);
            if (tradeInfo.hasError()) {
                logger.debug("被保人信息提数失败" + tradeInfo.toString());
                tradeInfo.addError("被保人信息提数失败");
                return tradeInfo;
            }
            logger.debug("团险性能监控,被保人信息提数时间:" + (System.currentTimeMillis() - TInsurdInfoTime)/1000.0 + "s");
            long TPayInfoTime = System.currentTimeMillis();
            tPayInfoBL.getTPayInfo(tradeInfo);
            if (tradeInfo.hasError()) {
                logger.debug("收费信息提数失败" + tradeInfo.toString());
                tradeInfo.addError("收费信息提数失败");
                return tradeInfo;
            }
            logger.debug("团险性能监控,收费信息提数时间:" + (System.currentTimeMillis() - TPayInfoTime)/1000.0 + "s");
            long TProductInfoTime = System.currentTimeMillis();
            tProductInfoBL.getTProductInfo(tradeInfo);
            if (tradeInfo.hasError()) {
                logger.debug("合同信息提数失败" + tradeInfo.toString());
                tradeInfo.addError("合同信息提数失败");
                return tradeInfo;
            }
            logger.debug("团险性能监控,合同信息信息提数时间:" + (System.currentTimeMillis() - TProductInfoTime)/1000.0 + "s");
            tPolicyBL.dealData(tradeInfo);
            if (tradeInfo.hasError()) {
                logger.debug("组装信息失败" + tradeInfo.toString());
                tradeInfo.addError("组装信息失败");
                return tradeInfo;
            }
            logger.debug("团险性能监控,团险提数时间为" + (System.currentTimeMillis() - beginTime)/1000.0 + "s");
            if ("FALSE".equals(engineFlag.toUpperCase())) {
                long EngineTime = System.currentTimeMillis();
                tradeInfo = getEngine(tradeInfo);
                if (tradeInfo.hasError()) {
                    logger.debug("调用团险规则引擎失败" + tradeInfo.toString());
                    tradeInfo.addError("调用团险的规则引擎失败");
                    return tradeInfo;
                }
                logger.debug("团险性能监控,调用规则引擎的时间:" + (System.currentTimeMillis() - EngineTime)/1000.0 + "s");
                long SpecTime = System.currentTimeMillis();
                List<LCPolPojo> lcPolPojos = (List<LCPolPojo>) tradeInfo.getData(LCPolPojo.class.getName());
                LCPolPojo lcPolPojoMain = null;
                for (int i = 0; i < lcPolPojos.size(); i++) {
                    if (lcPolPojos.get(i).getPolNo().equals(lcPolPojos.get(i).getMainPolNo())) {
                        lcPolPojoMain = lcPolPojos.get(i);
                        break;
                    }
                }
                if ("6807".equals(lcPolPojoMain.getRiskCode())) {
                    boolean passUWFlag = false;
                    boolean passClaimFlag = false;
                    //设置默认值为1
                    tradeInfo.addData("CHECKRULE", "01");
                    tradeInfo.addData("CHECKRULENUM", "01");
                    //核保错误结论
                    Result result = (Result) tradeInfo.getData(Result.class.getName());
                    List uwResult = new ArrayList();
                    if (result != null) {
                        List<UwResult> tUwResultList = result.getUwresultList();
                        if (tUwResultList != null && tUwResultList.size() > 0) {
                            for (int i = 0; i < tUwResultList.size(); i++) {
                                UwResult vr = (UwResult) tUwResultList.get(i);
                                //500万校验
                                if ("U18BOW013".equals(vr.getRuleCode())) {//U18BOW013
                                    LCPolicyInfoPojo lcPolicyInfoPojo = (LCPolicyInfoPojo) tradeInfo.getData(LCPolicyInfoPojo.class.getName());
                                    UWSpecialContractBL uwSpecialContractBL = SpringContextUtils.getBeanByClass(UWSpecialContractBL.class);
                                    boolean uwflag = uwSpecialContractBL.dealData(tradeInfo, lcPolicyInfoPojo.getJYContNo(), "04");
                                    if (uwflag) {
                                        //为1004更新特殊审批信息使用
                                        tradeInfo.addData("CHECKRULE", "00");
                                        continue;
                                    }
                                }
                                if ("U18BOW007".equals(vr.getRuleCode())){
                                    passUWFlag=true;
                                    continue;
                                }
                                if ("U18BOW008".equals(vr.getRuleCode())){
                                    passClaimFlag = true;
                                    continue;
                                }
                                //如果不属于以上两个规则那么返回核保被校验住
                                uwResult.add(vr);
                            }
                        }
                    }
                    //如果规则直接返回不进行特殊审批信息校验
                    result.setUwresultList(uwResult);
                    if (uwResult.size() > 0) {
                        logger.debug("核保被校验住了");
                        return tradeInfo;
                    }
                    if (passClaimFlag || passUWFlag|| 1==1){
                        SearchHistoryInfoSrvBL searchHistoryInfoSrvBL = SpringContextUtils.getBeanByClass(SearchHistoryInfoSrvBL.class);
                        try {
                            tradeInfo = searchHistoryInfoSrvBL.dealData(tradeInfo);
                        } catch (Exception e) {
                            logger.error(ExceptionUtils.exceptionToString(e));
                            logger.error("特殊审批信息处理失败" + tradeInfo.toString());
                            tradeInfo.addError("特殊审批信息处理失败");
                            return tradeInfo;
                        }
                    }
                }
                logger.debug("团险性能监控,处理特殊审批的时间:" + (System.currentTimeMillis() - SpecTime)/1000.0 + "s");
            } else {
                Result tODMResult = new Result();
                LCContPojo lcContPojo = (LCContPojo) tradeInfo.getData(LCContPojo.class.getName());
                logger.debug("保单号：" + lcContPojo.getContNo() + "，规则引擎挡板开启！");
                //模拟规则引擎核保结论
                tODMResult.setUwresultList(new ArrayList());
                tODMResult.setFlag(true);
                tradeInfo.addData(Result.class.getName(), tODMResult);
            }
        } catch (Exception e) {
            logger.debug("团险核保提数失败" + ExceptionUtils.exceptionToString(e));
            logger.debug("团险核保提数失败" + tradeInfo.toString());
            tradeInfo.addError("团险核保提数失败");
            return tradeInfo;
        }
        return tradeInfo;
    }


    /**
     * 调用规则引擎
     *
     * @param tradeInfo
     * @return
     */
    public TradeInfo getEngine(TradeInfo tradeInfo) {
        Date ftpDate = new Date();
        LCContPojo tLCContPojo = (LCContPojo) tradeInfo.getData(LCContPojo.class.getName());
        GlobalPojo globalPojo = (GlobalPojo) tradeInfo.getData(GlobalPojo.class.getName());
        //保单号
        String contno = tLCContPojo.getContNo();
        // 响应报文
        String responseXml = "";
        // 请求报文
        String requestXml = "";

        TPolicy tPolicy = (TPolicy) tradeInfo.getData(TPolicy.class.getName());
        requestXml = TPolicyToXml.getXml(tPolicy);
        logger.debug("团险调用规则引擎的报文为" + requestXml);
        //如果Policy对象转String正常，调用规则接口
        try {
            if (requestXml.indexOf("<error>") == -1) {
                long beginTime = System.currentTimeMillis();
                logger.debug("保单号：" + tLCContPojo.getContNo() + "，RULES*调用团险规则引擎开始");
                logger.debug("here,调用规则引擎的请求报文," + globalPojo.getSerialNo() + "：requestXml=" + requestXml + ",utl=" + url);
                // 发送请求，获得响应报文
                responseXml = THttpRequestClient.execute(requestXml, url, tLCContPojo.getContNo());
                logger.debug("团险性能监控,保单号：" + tLCContPojo.getContNo() + "，RULES*调用团险规则引擎结束，用时：" + (System.currentTimeMillis() - beginTime) / 1000.0 + "s");
                logger.debug(responseXml);
            } else {
                String message = requestXml.substring(requestXml.indexOf("<message>") + 9, requestXml.indexOf("</message>"));
                String error = requestXml.substring(requestXml.indexOf("<error>") + 7, requestXml.indexOf("</error>"));
                responseXml = tErrorCatch.getErrorResponse2(contno, message, "ODM0012", "团险提数程序异常-请求报文组装失败");
                requestXml = tErrorCatch.getErrorRequest2(contno, message, error);
                contno += "_ERROR";
            }
        } catch (IndexOutOfBoundsException e) {
            logger.error("团险提数程序异常-未提取到险种信息" + ExceptionUtils.exceptionToString(e));
            responseXml = tErrorCatch.getErrorResponse(contno, e, "ODM0011", "团险提数程序异常-未提取到险种信息");
            requestXml = tErrorCatch.getErrorRequest(contno, e);
            contno += "_ERROR";
        } catch (NullPointerException e) {
            logger.error("团险提数程序异常-对象空指针异常" + ExceptionUtils.exceptionToString(e));
            responseXml = tErrorCatch.getErrorResponse(contno, e, "ODM0013", "核保提数程序异常-保单信息提取失败");
            requestXml = tErrorCatch.getErrorRequest(contno, e);
            contno += "_ERROR";
        } catch (Exception exception) {
            exception.printStackTrace();
            logger.error("团险提数程序异常" + ExceptionUtils.exceptionToString(exception));
            responseXml = tErrorCatch.getErrorResponse(contno, exception, "ODM0010", "团险提数程序异常");
            requestXml = tErrorCatch.getErrorRequest(contno, exception);
            contno += "_ERROR";
        }

        //规则执行环境（RES)报错
        if (responseXml == null || "".equals(responseXml.trim())) {
            responseXml = tErrorCatch.getErrorResponse2(contno, "RES无执行结果", "ODM0020", "RES无执行结果");
            contno += "_ERROR";
        }


        logger.debug(responseXml);

        Result mResult = new Result();
        // 将响应报文转换为Result对象
        mResult = TXmlToResult.getResult(responseXml, contno);
        tradeInfo.addData(Result.class.getName(), mResult);
        try {
            logger.debug("团险RULES*请求报文为：" + requestXml);
            logger.debug("团险RULES*返回报文为：" + responseXml);
            logger.debug("团险RULES*规则总数为：" + mResult.getUwresultList().size());
            logger.debug("团险RULES*提数程序结束");
            // ftpFlag=1启用ftp, ftpFlag=0不启用ftp
            if ("1".equals(ftpFlag)) {
                if ((responseXml.indexOf("连接超时") != -1
                        || responseXml.indexOf("HTTP客户端程序异常") != -1 || responseXml
                        .indexOf("程序异常") != -1)
                        && contno.indexOf("ERROR") == -1) {
                    contno += "_ERROR";
                }
                // 另起线程将请求报文上传FTP服务器
                TFTPRequest ftpRequest = SpringContextUtils.getBeanByClass(TFTPRequest.class);
                ftpRequest.init(contno, requestXml, "request", ftpDate);
                Thread thread = new Thread(ftpRequest);
                thread.start();

                // 另起线程将响应报文上传FTP服务器
                TFTPRequest ftpResponse = SpringContextUtils.getBeanByClass(TFTPRequest.class);
                ftpResponse.init(contno, responseXml, "response", ftpDate);
                Thread thread1 = new Thread(ftpResponse);
                thread1.start();
            }
        } catch (Exception e) {
            logger.debug("团险RULESEXCEPTION*FTP异常");
            e.printStackTrace();
            logger.error(ExceptionUtils.exceptionToString(e));
        }
        tradeInfo.removeData(TPolicy.class.getName());
        tradeInfo.removeData(TProductInfo.class.getName());
        tradeInfo.removeData(TPayInfo.class.getName());
        tradeInfo.removeData(TInsurdInfo.class.getName());
        tradeInfo.removeData(TBeneficiaryInfos.class.getName());
        tradeInfo.removeData(TBeneficiaryInfo.class.getName());
        tradeInfo.removeData(TApplicantInfo.class.getName());
        tradeInfo.removeData(TAgentInfo.class.getName());
        tradeInfo.removeData(Product.class.getName());
        tradeInfo.removeData(ProdGroup.class.getName());
        tradeInfo.removeData(LcInsured.class.getName());
        //核保错误结论
        if (mResult == null) {
            return tradeInfo;
        }
        List<LKTransStatusPojo> lkTransStatusPojos = (List<LKTransStatusPojo>) tradeInfo.getData(LKTransStatusPojo.class.getName());
        String seNo = "";
        if (lkTransStatusPojos != null && lkTransStatusPojos.size() > 0) {
            seNo = lkTransStatusPojos.get(0).getTransNo();
        }
        List<UwResult> tUwResultList = mResult.getUwresultList();
        if (tUwResultList != null && tUwResultList.size() > 0) {
            for (int i = 0; i < tUwResultList.size(); i++) {
                logger.debug("TODM" + seNo + "[" + tUwResultList.get(i).getRuleCode() + "]" + tUwResultList.get(i).getReturnInfo());
            }
        }
        return tradeInfo;
    }
}
