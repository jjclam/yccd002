package com.sinosoft.cloud.cbs.trules.httpclient;


import com.sinosoft.cloud.cbs.trules.entity.*;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;



/**
 * @Author:wangshuliang
 * @Description:
 * @Date:Created in 19:35 2018/6/25
 * @Modified by:
 */
public class SpecMesToXml {
    private static Log logger = LogFactory.getLog(TPolicyToXml.class);
    private static XStream xstream = null;

    public static String getXml(Message message) {
        if (xstream == null) {
            //报文格式
            xstream = new XStream(new DomDriver());
            xstream.alias("message", Message.class);
            xstream.alias("head", Head.class);
            xstream.alias("sysHeader", SysHeader.class);
            xstream.alias("body", Body.class);
            xstream.alias("queryInfo", QueryInfo.class);
            xstream.alias("approveExtendInfo", ApproveExtendInfo.class);
        }
        //javabean转xml
        String error = null;
        String requestXml = xstream.toXML(message);

        return requestXml;
    }
}
