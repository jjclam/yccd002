package com.sinosoft.cloud.cbs.rules.data;

import com.sinosoft.cloud.cbs.riskamnt.CalRiskAmntService;
import com.sinosoft.cloud.cbs.riskamnt.GetAccumBL;
import com.sinosoft.cloud.cbs.rules.bom.Policy;
import com.sinosoft.cloud.cbs.rules.compare.dealbl.CompareResultBL;
import com.sinosoft.cloud.cbs.rules.httpclient.HttpRequestClient;
import com.sinosoft.cloud.cbs.rules.httpclient.PolicyToXml;
import com.sinosoft.cloud.cbs.rules.httpclient.XmlToResult;
import com.sinosoft.cloud.cbs.rules.result.Result;
import com.sinosoft.cloud.cbs.rules.util.FTPRequest;
import com.sinosoft.cloud.common.SpringContextUtils;
import com.sinosoft.cloud.rest.TradeInfo;
import com.sinosoft.lis.entity.GlobalPojo;
import com.sinosoft.lis.entity.LCContPojo;
import com.sinosoft.utility.ExceptionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.Map;
import java.util.concurrent.CountDownLatch;


/**
 * @Author: zhuyiming
 * @Description: ODM调用执行类
 * @Date: Created in 9:40 2017/9/25
 */
@Service
public class Engine {
	private final Log logger = LogFactory.getLog(this.getClass());
	/** 返回结果 */
	/*private Result mResult = new Result();*/
	/** 传入数据 */
	/*private TradeInfo mTradeInfo;*/

	/** CountDownLatch对象 */
	private CountDownLatch mThreadsSignal;
	/** 获取规则引擎地址*/
	@Value("${cloud.engine.ruleUrl}")
	private String url;
	/** 获取ftp标记 1为报文落地，0为不落地*/
	@Value("${cloud.engine.ftpFlag}")
	private String ftpFlag;
	@Value("${cloud.engine.oldCoreFlag}")
	private String oldCoreFlag;

	@Autowired
	ErrorCatch eCatch;
//    @Autowired
//	GetAccumBL getAccumBL;
	@Autowired
    CalRiskAmntService calRiskAmntService;
    @Autowired
	CompareResultBL compareResultBL;
	public Engine() {
	}

	/*public Engine(CountDownLatch tThreadsSignal, TradeInfo requestInfo) {
		this.mThreadsSignal = tThreadsSignal;
		this.mTradeInfo = requestInfo;
	}

	public void init(CountDownLatch tThreadsSignal, TradeInfo requestInfo) {
		this.mThreadsSignal = tThreadsSignal;
		this.mTradeInfo = requestInfo;
	}

	public void run() {
		logger.debug("----   ODM线程执行       ---Start---");
		execute(mTradeInfo);
		logger.debug("----   ODM线程执行       ---End---");
		// 新规则线程运行结束后，计数器减1
		mThreadsSignal.countDown();
		logger.debug(Thread.currentThread().getName() + "结束. 还有"
				+ mThreadsSignal.getCount() + " 个线程");
	}*/

	public Result execute(TradeInfo requestInfo) {
		logger.debug("RULES*提数程序开始");
		Result mResult = new Result();
		// 响应报文
		String responseXml = "";
		// 请求报文
		String requestXml = "";
		// 保单号
		String contno = "";
		Policy policy = null;
		Policy riskPolicy=null;
		// webservice地址
		/*Properties props = null;*/
		Date ftpDate = new Date();
		//交易流水号
		GlobalPojo globalPojo = (GlobalPojo) requestInfo.getData(GlobalPojo.class.getName());
		try {
			ExtractData extractData = SpringContextUtils.getBeanByClass(ExtractData.class);
			NewExtractData newExtractData=SpringContextUtils.getBeanByClass(NewExtractData.class);
			/*try {
				props = CUtil.getPropertiesByFileName("Rule.properties");
			} catch (AMQAppException e) {
				e.printStackTrace();
				logger.error(ExceptionUtils.exceptionToString(e));
			}*/

			LCContPojo tLCContPojo = (LCContPojo) requestInfo.getData(LCContPojo.class.getName());
			if(requestInfo.hasError()){
				return mResult;
			}
			contno = tLCContPojo.getContNo();

			try {

				if ("true".equals(oldCoreFlag.trim())) {
					// 老核心提数
//					logger.info("老核心提数开始时间：" + System.currentTimeMillis());
//					long startTime = System.currentTimeMillis();
					//requestInfo=getAccumBL.dealData(requestInfo);
//					requestInfo = calRiskAmntService.service(requestInfo);
					policy = extractData.getData(requestInfo);
//					logger.info("老核心提数所用时间：" + (System.currentTimeMillis() - startTime) / 1000.0 + "s");

					// 将Policy对象转换为请求报文
					requestXml = PolicyToXml.getXml(policy);
				}else {
					logger.info("累计提数失败！未打开提数开关");
					 requestXml = PolicyToXml.getXml(policy);
				 }


				/*String url = CUtil.getProperty("ruleUrl", props);*/
				//如果Policy对象转String正常，调用规则接口
				if(requestXml.indexOf("<error>") == -1) {
					long beginTime = System.currentTimeMillis();
					logger.info("保单号：" + tLCContPojo.getContNo() + "，RULES*调用规则引擎开始");
					logger.info("here,调用规则引擎的请求报文," + globalPojo.getSerialNo() + "：requestXml=" + requestXml + ",utl=" + url);
					// 发送请求，获得响应报文
					responseXml = HttpRequestClient.execute(requestXml, url,requestInfo);
					logger.info("保单号：" + tLCContPojo.getContNo() + "，RULES*调用规则引擎结束，用时：" + (System.currentTimeMillis() - beginTime) / 1000.0 + "s");
				}else{
					String message = requestXml.substring(requestXml.indexOf("<message>")+9,requestXml.indexOf("</message>"));
					String error = requestXml.substring(requestXml.indexOf("<error>")+7,requestXml.indexOf("</error>"));
					responseXml = eCatch.getErrorResponse2(contno, message, "ODM0012", "提数程序异常-请求报文组装失败");
					requestXml = eCatch.getErrorRequest2(contno, message,error);
					contno += "_ERROR";
					requestInfo.addError("提数程序异常-请求报文组装失败");
				}
			} catch(IndexOutOfBoundsException e) {
				logger.error("提数程序异常-未提取到险种信息"+ExceptionUtils.exceptionToString(e));
				responseXml = eCatch.getErrorResponse(contno,e, "ODM0011", "提数程序异常-未提取到险种信息");
				requestXml = eCatch.getErrorRequest(contno,e);
				contno += "_ERROR";
				requestInfo.addError("提数程序异常-未提取到险种信息");
			}catch (NullPointerException e) {
				logger.error("提数程序异常-对象空指针异常"+ExceptionUtils.exceptionToString(e));
				responseXml = eCatch.getErrorResponse(contno, e, "ODM0013", "核保提数程序异常-保单信息提取失败");
				requestXml = eCatch.getErrorRequest(contno, e);
				contno += "_ERROR";
				requestInfo.addError("提数程序异常-保单信息提取失败");
			}catch(Exception exception) {
				exception.printStackTrace();
				logger.error("提数程序异常"+ExceptionUtils.exceptionToString(exception));
				responseXml = eCatch.getErrorResponse(contno,exception,  "ODM0010", "提数程序异常");
				requestXml = eCatch.getErrorRequest(contno ,exception);
				contno += "_ERROR";
				requestInfo.addError("提数程序异常");
			}
			//规则执行环境（RES)报错
			if (responseXml == null || "".equals(responseXml.trim())) {
				responseXml = eCatch.getErrorResponse2(contno, "Exception", "ODM0020", "RES无执行结果");
				contno += "_ERROR";
				requestInfo.addError("RES无执行结果");
			}
			if(responseXml.indexOf("ilog.rules.res.decisionservice.IlrDecisionServiceException")>-1){

				String messageInfo = responseXml.substring(responseXml.indexOf("<ds:exception>")+14,responseXml.indexOf("</ds:exception>"));
				messageInfo = messageInfo.replaceAll("\\$", "-Dollar-");
				messageInfo = messageInfo.replaceAll("&#x","%u").replaceAll(";","");
				messageInfo = Util.unescape(messageInfo);

				responseXml = eCatch.getErrorResponse3(contno, "Exception", "ODM0021", "RES内部执行异常",messageInfo);

				contno += "_ERROR";
				requestInfo.addError("RES内部执行异常");
			}
			// 将响应报文转换为Result对象
			mResult = XmlToResult.getResult(responseXml);

		} catch (Exception e) {
			logger.info("RULESEXCEPTION*程序异常");
			logger.error("核保程序异常"+ExceptionUtils.exceptionToString(e));
			e.printStackTrace();
			//最大范围异常捕捉
			responseXml = eCatch.getErrorResponse(contno, e, "ODM0000", "程序异常");
			// 将响应报文转换为Result对象
			mResult = XmlToResult.getResult(responseXml);
			requestInfo.addError("程序异常");
		}

		try {
			logger.info("RULES*请求报文为：" + requestXml);
			logger.info("RULES*返回报文为：" + responseXml);

			logger.info("RULES*规则总数为：" + mResult.getUwresultList().size());

			logger.info("RULES*提数程序结束");
			// ftpFlag=1启用ftp, ftpFlag=0不启用ftp
			/*String ftpFlag = CUtil.getProperty("ftpFlag", props);*/
			if ("1".equals(ftpFlag)) {
				if ((responseXml.indexOf("连接超时") != -1
						|| responseXml.indexOf("HTTP客户端程序异常") != -1 || responseXml
						.indexOf("程序异常") != -1)
						&& contno.indexOf("ERROR") == -1) {
					responseXml = eCatch.getErrorResponse2(contno, "Exception", "NET001", "连接异常");
					contno += "_ERROR";
					requestInfo.addError("连接异常");
				}
				// 另起线程将请求报文上传FTP服务器
				FTPRequest ftpRequest = SpringContextUtils.getBeanByClass(FTPRequest.class);
				ftpRequest.init(contno, requestXml, "request", ftpDate);
				Thread thread = new Thread(ftpRequest);
				thread.start();

				// 另起线程将响应报文上传FTP服务器
				FTPRequest ftpResponse = SpringContextUtils.getBeanByClass(FTPRequest.class);
				ftpResponse.init(contno, responseXml, "response", ftpDate);
				Thread thread1 = new Thread(ftpResponse);
				thread1.start();
			}
		}catch (Exception e) {
			logger.info("RULESEXCEPTION*FTP异常");
			e.printStackTrace();
			logger.error(ExceptionUtils.exceptionToString(e));
			responseXml = eCatch.getErrorResponse(contno, e, "NET002", "RULESEXCEPTION*FTP异常");
			// 将响应报文转换为Result对象
			mResult = XmlToResult.getResult(responseXml);
			requestInfo.addError("RULESEXCEPTION*FTP异常");
		}
		return mResult;
	}

	// Add by ligz 2016/3/9 新增获取结果的方法
	/*public Result getResult() {
		return this.mResult;
	}*/

}
