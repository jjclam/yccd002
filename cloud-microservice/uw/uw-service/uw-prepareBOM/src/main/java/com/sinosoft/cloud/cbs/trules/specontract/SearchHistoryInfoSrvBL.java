package com.sinosoft.cloud.cbs.trules.specontract;


import com.sinosoft.cloud.bl.AbstractBL;
import com.sinosoft.cloud.cbs.trules.bom.LcInsured;
import com.sinosoft.cloud.cbs.trules.util.DataSourceName;
import com.sinosoft.cloud.cbs.trules.util.MulDataExexSql;
import com.sinosoft.cloud.rest.TradeInfo;
import com.sinosoft.lis.entity.LCContPojo;
import com.sinosoft.lis.entity.LCInsuredPojo;
import com.sinosoft.utility.*;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author:wangshuliang
 * @Description: 查询既往核保理赔信息 并且调用特殊审批信息的接口
 * @Date:Created in 9:18 2018/6/28
 * @Modified by:
 */
@Service
public class SearchHistoryInfoSrvBL extends AbstractBL {
    private final Log logger = LogFactory.getLog(this.getClass());
    @Autowired
    private UWSpecialContractBL uwSpecialContractBL;

    @Override
    public TradeInfo dealData(TradeInfo tradeInfo) {
        LCContPojo lcContPojo = (LCContPojo) tradeInfo.getData(LCContPojo.class.getName());
        logger.debug(lcContPojo.getContNo() + "调用查询既往核保开始");
        List<LCInsuredPojo> lcInsureds = (List<LCInsuredPojo>) tradeInfo.getData(LCInsuredPojo.class.getName());
        if (lcInsureds == null || lcInsureds.size() == 0) {
            logger.debug("被保人信息为空" + tradeInfo.toString());
            tradeInfo.addError("被保人信息为空");
            return tradeInfo;
        }
        LCInsuredPojo lcInsuredPojo = lcInsureds.get(0);
        //既往核保信息查询
        boolean uwflag = false;
        boolean claimFlag = false;
        boolean resUWFlag = true;
        boolean resclaimFlag = true;
        LcInsured lcInsured = (LcInsured) tradeInfo.getData(LcInsured.class.getName());
        //既往有理赔
        List<String> ClmNoList = new ArrayList<>();
        String WhetherSettlementInfo = (String)tradeInfo.getData("WhetherSettlementInfo");
        if (!StringUtils.isEmpty(WhetherSettlementInfo)){
            claimFlag =true;
            ClmNoList = (List<String>) tradeInfo.getData("ClmNo");
        }

        //既往有延期拒保
        String WhetherPostponeUnAccept = (String) tradeInfo.getData("WhetherPostponeUnAccept");
        if (!StringUtils.isEmpty(WhetherPostponeUnAccept)){
            uwflag = true;

        }
        try {
            if (uwflag || claimFlag) {
                String productCode = lcInsuredPojo.getContPlanCode();
                if ("YA".equals(productCode) || "Y6".equals(productCode) || "YB".equals(productCode) || "YE".equals(productCode)) {
                    if (uwflag) {
                        resUWFlag = uwSpecialContractBL.dealData(tradeInfo, "000000", "01");
                    }
                    if (claimFlag) {
                        for (int i = 0; i < ClmNoList.size(); i++) {
                            String ClmNo = ClmNoList.get(i);
                            resclaimFlag = uwSpecialContractBL.dealData(tradeInfo, ClmNo, "02");
                            if (!resclaimFlag) {
                                break;
                            }
                        }
                    }
                }
            }
            if (!resUWFlag) {
                logger.debug("被保险人有延期、拒保记录,需转保险公司审核" + tradeInfo.toString());
                tradeInfo.addError("被保险人有延期、拒保记录,需转保险公司审核");
                return tradeInfo;
            }
            if (!resclaimFlag) {
                logger.debug("被保险人有理赔记录,需转保险公司审核" + tradeInfo.toString());
                tradeInfo.addError("被保险人有理赔记录,需转保险公司审核");
                return tradeInfo;
            }
        } catch (Exception e) {
            logger.error("调用特殊审批信息服务异常" + tradeInfo.toString());
            logger.debug("调用特殊审批信息服务异常" + ExceptionUtils.exceptionToString(e));
            tradeInfo.addError("调用特殊审批信息服务异常");
            return tradeInfo;
        }

        return tradeInfo;
    }

    /**
     * 查询既往核保信息
     *
     * @param lcContPojo
     * @param lcInsuredPojo
     * @return
     */
    public SSRS dealDataUW(LCContPojo lcContPojo, LCInsuredPojo lcInsuredPojo) throws Exception {
        String sql = "select 1"
                + " from lccont t,lcpol p,lcinsured d,lcuwmaster u "
                + " where t.contno = p.contno "
                + " and t.contno = d.contno "
                + " and t.insuredno = d.insuredno "
                + " and p.polno = u.proposalno "
                + " and u.passflag in ('1','2') "
                + " and d.insuredno = ?  ";
        logger.debug(lcContPojo.getContNo() + "执行sql:" + sql);
        TransferData transferData = new TransferData();
        transferData.setNameAndValue("insuredno", "string:" + lcInsuredPojo.getInsuredNo());
        VData vData = new VData();
        vData.add(sql);
        vData.add(transferData);
        MulDataExexSql mulDataExexSql = new MulDataExexSql();
        SSRS mSSRS = null;
        try {
            mSSRS = mulDataExexSql.excuteSql(vData, DataSourceName.TOLD.getSourceName());
        } catch (Exception e) {
            throw e;
        }
        return mSSRS;
    }

    /**
     * 查询既往核保信息核心库
     *
     * @param lcContPojo
     * @param lcInsuredPojo
     * @return
     */
    public SSRS dealDataUWOLD(LCContPojo lcContPojo, LCInsuredPojo lcInsuredPojo) throws Exception {
        String sql = "select 1 "
                + " from lccont t,lcpol p,lcinsured d,lcuwmaster u "
                + " where t.contno = p.contno "
                + " and t.contno = d.contno "
                + " and t.insuredno = d.insuredno "
                + " and p.proposalno = u.proposalno "
                + " and u.passflag in ('1','2') "
                + " and d.insuredno = ?  ";
        logger.debug(lcContPojo.getContNo() + "执行sql:" + sql);
        TransferData transferData = new TransferData();
        transferData.setNameAndValue("insuredno", "string:" + lcInsuredPojo.getInsuredNo());
        VData vData = new VData();
        vData.add(sql);
        vData.add(transferData);
        MulDataExexSql mulDataExexSql = new MulDataExexSql();
        SSRS mSSRS = null;
        try {
            mSSRS = mulDataExexSql.excuteSql(vData, DataSourceName.OLD.getSourceName());
        } catch (Exception e) {
            throw e;
        }
        return mSSRS;
    }

    /**
     * 查询既往理赔信息团险库
     *
     * @param lcContPojo
     * @param lcInsuredPojo
     * @return
     */
    public SSRS dealDataClaim(LCContPojo lcContPojo, LCInsuredPojo lcInsuredPojo) throws Exception {
        String clmRecordSQL = " Select h.CLMNo                                                 "
                + "            From LLCase e                                                     "
                + "            left join (Select a.caseno,                                       "
                + "                              a.RiskCode,                                     "
                + "                              a.DutyCode,                                     "
                + "                              a.CLMNo,                                        "
                + "                              a.realpay,                                      "
                + "                              b.RiskVer                                       "
                + "                         From LLClaimDetail a, LMRiskApp b                    "
                + "                        Where a.RiskCode = b.RiskCode) h                      "
                + "              on h.CaseNo = e.CaseNo, LLRegister c, LLAppClaimReason f,       "
                + "           LDCode g, LCInsured l                                              "
                + "           Where 1 = 1                                                        "
                + "             And l.InsuredNo = e.CustomerNo                                   "
                + "             And e.RGTNo = c.RGTNo                                            "
                + "             And e.CaseNo = f.CaseNo                                          "
                + "             And e.CLMSTATE = g.code                                          "
                + "             And g.CodeType = 'llrgtstate'                                    "
                + "             And l.insuredno = ?                                         "
                + "          union                                                               "
                + "          Select  h.CLMNo                                                  "
                + "            From LLCase e                                                     "
                + "            left join (Select a.caseno,                                       "
                + "                              a.RiskCode,                                     "
                + "                              a.DutyCode,                                     "
                + "                              a.CLMNo,                                        "
                + "                              a.realpay,                                      "
                + "                              b.RiskVer                                       "
                + "                         From LLClaimDetail a, LMRiskApp b                    "
                + "                        Where a.RiskCode = b.RiskCode) h                      "
                + "              on h.clmno = e.CaseNo, LLRegister c, LLAppClaimReason f,        "
                + "           LDCode g, LCInsured l                                              "
                + "           Where 1 = 1                                                        "
                + "             And l.InsuredNo = e.CustomerNo                                   "
                + "             And e.RGTNo = c.RGTNo                                            "
                + "             And e.CaseNo = f.CaseNo                                          "
                + "             And e.CLMSTATE = g.code                                          "
                + "             And g.CodeType = 'clmstate'                                  "
                + "             And l.insuredno = ?                                         ";
        logger.debug(lcContPojo.getContNo() + "执行sql:" + clmRecordSQL);
        VData vData = new VData();
        TransferData transferData = new TransferData();
        transferData.setNameAndValue("insuredno", "string:" + lcInsuredPojo.getInsuredNo());
        transferData.setNameAndValue("insuredno1", "string:" + lcInsuredPojo.getInsuredNo());
        vData.add(clmRecordSQL);
        vData.add(transferData);
        MulDataExexSql mulDataExexSql = new MulDataExexSql();
        SSRS ssrs = null;
        try {
            ssrs = mulDataExexSql.excuteSql(vData, DataSourceName.TOLD.getSourceName());
        } catch (Exception e) {
            throw e;
        }
        return ssrs;
    }

    /**
     * 查询既往理赔信息核心库
     *
     * @param lcContPojo
     * @param lcInsuredPojo
     * @return
     */
    public SSRS dealDataClaimOLD(LCContPojo lcContPojo, LCInsuredPojo lcInsuredPojo) throws Exception {
        String clmRecordSQL = " Select h.CLMNo                                                    "
                + "            From LLCase e                                                     "
                + "            left join (Select a.caseno,                                       "
                + "                              a.RiskCode,                                     "
                + "                              a.DutyCode,                                     "
                + "                              a.CLMNo,                                        "
                + "                              a.realpay,                                      "
                + "                              b.RiskVer                                       "
                + "                         From LLClaimDetail a, LMRiskApp b                    "
                + "                        Where a.RiskCode = b.RiskCode) h                      "
                + "              on h.CaseNo = e.CaseNo, LLRegister c, LLAppClaimReason f,       "
                + "           LDCode g, LCInsured l                                              "
                + "           Where 1 = 1                                                        "
                + "             And l.InsuredNo = e.CustomerNo                                   "
                + "             And e.RGTNo = c.RGTNo                                            "
                + "             And e.CaseNo = f.CaseNo                                          "
                + "             And e.rgtstate = g.code                                          "
                + "             And g.CodeType = 'llrgtstate'                                    "
                + "             And l.insuredno = ?                                         "
                + "          union                                                               "
                + "          Select h.CLMNo                                            "
                + "            From LLCase e                                                     "
                + "            left join (Select a.caseno,                                       "
                + "                              a.RiskCode,                                     "
                + "                              a.DutyCode,                                     "
                + "                              a.CLMNo,                                        "
                + "                              a.realpay,                                      "
                + "                              b.RiskVer                                       "
                + "                         From LLClaimDetail a, LMRiskApp b                    "
                + "                        Where a.RiskCode = b.RiskCode) h                      "
                + "              on h.clmno = e.CaseNo, LLRegister c, LLAppClaimReason f,        "
                + "           LDCode g, LCInsured l                                              "
                + "           Where 1 = 1                                                        "
                + "             And l.InsuredNo = e.CustomerNo                                   "
                + "             And e.RGTNo = c.RGTNo                                            "
                + "             And e.CaseNo = f.CaseNo                                          "
                + "             And e.rgtstate = g.code                                          "
                + "             And g.CodeType = 'clmstate'                                  "
                + "             And l.insuredno = ?                                         ";
        logger.debug(lcContPojo.getContNo() + "执行sql:" + clmRecordSQL);
        VData vData = new VData();
        TransferData transferData = new TransferData();
        transferData.setNameAndValue("insuredno", "string:" + lcInsuredPojo.getInsuredNo());
        transferData.setNameAndValue("insuredno1", "string:" + lcInsuredPojo.getInsuredNo());
        vData.add(clmRecordSQL);
        vData.add(transferData);
        MulDataExexSql mulDataExexSql = new MulDataExexSql();
        SSRS ssrs = null;
        try {
            ssrs = mulDataExexSql.excuteSql(vData, DataSourceName.OLD.getSourceName());
        } catch (Exception e) {
            throw e;
        }
        return ssrs;
    }


}
