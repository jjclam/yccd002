package com.sinosoft.cloud.cbs.rules.result;



/**
 * 规则返回信息
 * 
 * @author dingfan
 * 
 */
public class UwResult{
	/**
	 * 规则违反标志
	 */
	private boolean flag;
	
	/**
	 * 规则编码
	 */
	private String ruleCode;

	/**
	 * 提示信息
	 */
	private String returnInfo;
	
	/**
	 * 险种编码（险种级的返回险种编码、保单级的返回为'000000'）
	 */
	private String riskCode;
	
	/**
	 * 非实时标记
	 * @return
	 */
	private String notRealTimeFlag;

	/**
	 * 客户号
	 */
	private String insuredNo;
	private String insuredName;
	
	public String getNotRealTimeFlag() {
		return notRealTimeFlag;
	}

	public void setNotRealTimeFlag(String notRealTimeFlag) {
		this.notRealTimeFlag = notRealTimeFlag;
	}

	public boolean isFlag() {
		return flag;
	}

	public void setFlag(boolean flag) {
		this.flag = flag;
	}

	public String getRuleCode() {
		return ruleCode;
	}

	public void setRuleCode(String ruleCode) {
		this.ruleCode = ruleCode;
	}

	public String getReturnInfo() {
		return returnInfo;
	}

	public void setReturnInfo(String returnInfo) {
		this.returnInfo = returnInfo;
	}

	public String getRiskCode() {
		return riskCode;
	}

	public void setRiskCode(String riskCode) {
		this.riskCode = riskCode;
	}

	public String getInsuredNo() {
		return insuredNo;
	}

	public void setInsuredNo(String insuredNo) {
		this.insuredNo = insuredNo;
	}


	public String getInsuredName() {
		return insuredName;
	}

	public void setInsuredName(String insuredName) {
		this.insuredName = insuredName;
	}

	@Override
	public String toString() {
		return "UwResult{" +
				"flag=" + flag +
				", ruleCode='" + ruleCode + '\'' +
				", returnInfo='" + returnInfo + '\'' +
				", riskCode='" + riskCode + '\'' +
				", notRealTimeFlag='" + notRealTimeFlag + '\'' +
				", insuredNo='" + insuredNo + '\'' +
				", insuredName='" + insuredName + '\'' +
				'}';
	}
}
