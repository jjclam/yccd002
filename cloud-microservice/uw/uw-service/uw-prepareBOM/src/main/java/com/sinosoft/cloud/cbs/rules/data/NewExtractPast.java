package com.sinosoft.cloud.cbs.rules.data;

import com.sinosoft.cloud.cbs.rules.bom.Applicant;
import com.sinosoft.cloud.cbs.rules.bom.Insured;
import com.sinosoft.cloud.cbs.rules.bom.Policy;

import com.sinosoft.cloud.rest.TradeInfo;
import com.sinosoft.lis.entity.LRApplicantHistoryPojo;
import com.sinosoft.lis.entity.LRInsuredDataPojo;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by zkr on 2018/6/20.
 */
@Service
public class NewExtractPast {

    private final Log logger = LogFactory.getLog(this.getClass());

    @Value("${cloud.uw.barrier.control}")
    private String barrier;
    public void getPast(TradeInfo tradeInfo, Policy policy){
        //挡板
        if("true".equals(barrier)){
            logger.debug("保单号：" + policy.getContNo() + "，累计风险保额既往数据挡板启动！");
            getBarrierData(policy);
            return;
        }
        // 投保人既往
        getApplicantPast(tradeInfo, policy);
        // 被保人既往
        getInsuredPast(tradeInfo, policy);
    }

    /**
     * 投保人既往
     * @param tradeInfo
     * @param policy
     */
    public void getApplicantPast(TradeInfo tradeInfo,Policy policy){
        LRApplicantHistoryPojo lrApplicantHistoryPojo = (LRApplicantHistoryPojo) tradeInfo.getData(LRApplicantHistoryPojo.class.getName());
        // 投保人
        Applicant applicantInfo = policy.getApplicant();
        // 客户号
       // String clientNo = applicantInfo.getClientNo();
        // 合同号
       // String contno = policy.getContNo();
      /*  if (lrApplicantHistoryPojo.getHaveCheckNote()!=null && lrApplicantHistoryPojo.getHaveCheckNote()!=""){
            // HaveCheckNote:既往有体检记录         HaveExamineReply:既往保单有体检回复记录
            applicantInfo.setHaveCheckNote(true);
        }*/
       /*if (lrApplicantHistoryPojo.getHaveExaminePolicy()!=null && lrApplicantHistoryPojo.getHaveExaminePolicy()!=""){
           // 既往有体检、加费、特约的保单HaveExaminePolicy
           applicantInfo.setHaveExaminePolicy(true);
       }*/
        /*if (lrApplicantHistoryPojo.getHaveClaims()!=null && lrApplicantHistoryPojo.getHaveClaims()!=""){
            // 既往有理赔记录HaveClaims
            applicantInfo.setHaveClaims(true);
        }*/
       /* if (lrApplicantHistoryPojo.getHavePreserve()!=null && lrApplicantHistoryPojo.getHavePreserve()!=""){
            // 既往有保全二核
            applicantInfo.setHavePreserve(true);
        }*/
        /*if (lrApplicantHistoryPojo.getHaveAscertain()!=null && lrApplicantHistoryPojo.getHaveAscertain()!=""){
            // 既往有生调记录
            applicantInfo.setHaveAscertain(true);
        }*/
        /*if (lrApplicantHistoryPojo.getHaveClaims()=="1"){
            // HaveApptHistory:既往有投保经历          HaveClaims:既往有理赔记录
            applicantInfo.setHaveApptHistory(true);
        }else {
            applicantInfo.setHaveApptHistory(false);
        }*/
        /*if (lrApplicantHistoryPojo.getHaveSubExamine()!=null && lrApplicantHistoryPojo.getHaveSubExamine()!=""){
            // 既往有次标准体记录
            applicantInfo.setHaveSubExamine(true);
        }*/
        /*if (lrApplicantHistoryPojo.getHaveUnfiExamine()!=null && lrApplicantHistoryPojo.getHaveUnfiExamine()!=""){
            // 既往有体检未完成记录
            applicantInfo.setHaveUnfiExamine(true);
        }*/
       /* if (lrApplicantHistoryPojo.getHaveRejectionOcc()=="1"){
            // 既往告知为五、六类或拒保职业
            applicantInfo.setHaveRejectionOcc(true);
        }else{
            applicantInfo.setHaveRejectionOcc(false);
        }*/
        if (lrApplicantHistoryPojo.getAppntJobTypeThaBefo()=="1"){
            // 职业类别低于既往告知
            applicantInfo.setAppntJobTypeThaBefo(true);
        }else{
            applicantInfo.setAppntJobTypeThaBefo(false);
        }


    }

    /**
     * 被保人既往
     */
 public void getInsuredPast(TradeInfo tradeInfo, Policy policy){
     // 被保人列表
     List insuredInfoList = policy.getInsuredList();
     List<LRInsuredDataPojo> lrInsuredDataPojos = (List<LRInsuredDataPojo>) tradeInfo.getData(LRInsuredDataPojo.class.getName());
     // 合同号
    // String contno = policy.getContNo();
     for (int i=0;i<insuredInfoList.size();i++){
         // 被保人
         Insured insured = (Insured) insuredInfoList.get(i);
         LRInsuredDataPojo lrInsuredDataPojo=null;
         for (int j=0;j<lrInsuredDataPojos.size();j++){
             Map map =null;
             lrInsuredDataPojo=lrInsuredDataPojos.get(j);
             if (insured.getClientNo()==lrInsuredDataPojo.getCustomerNo()){
                 if (lrInsuredDataPojo.getHaveExaminePolicy()!=null && lrInsuredDataPojo.getHaveExaminePolicy()!=""){
                     // 既往有体检、加费、特约的保单
                     insured.setHaveExaminePolicy(true);
                 }
                 if (lrInsuredDataPojo.getHaveClaims()!=null && lrInsuredDataPojo.getHaveClaims()!=""){
                     // 既往有理赔记录
                     insured.setHaveClaims(true);
                 }
                 if (lrInsuredDataPojo.getHavePreserve()!=null && lrInsuredDataPojo.getHavePreserve()!=""){
                     // 既往有保全二核
                     insured.setHavePreserve(true);
                 }
                 if (lrInsuredDataPojo.getHaveAscertain()!=null && lrInsuredDataPojo.getHaveAscertain()!=""){
                     // 既往有生调记录
                     insured.setHaveAscertain(true);
                 }
                 if (lrInsuredDataPojo.getHaveApptHistory()=="1"){
                     // 既往有投保经历
                     insured.setHaveApptHistory(true);
                 }else{
                     insured.setHaveApptHistory(false);
                 }
                 if (lrInsuredDataPojo.getHaveSubExamine()!=null && lrInsuredDataPojo.getHaveSubExamine()!=""){
                     // 既往有次标准体记录
                     insured.setHaveSubExamine(true);
                 }
                 if (lrInsuredDataPojo.getHaveUnfiExamine()!=null && lrInsuredDataPojo.getHaveUnfiExamine()!=""){
                     // 既往有体检未完成记录
                     insured.setHaveUnfiExamine(true);
                 }
                 if (lrInsuredDataPojo.getHaveRejectionOcc()=="1"){
                     // 既往告知为五、六类或拒保职业
                     insured.setHaveRejectionOcc(true);
                 }else{
                     insured.setHaveRejectionOcc(false);
                 }
                 if (lrInsuredDataPojo.getInsJobTypeThaBefo()=="1"){
                     // 职业类别低于既往告知
                     insured.setInsJobTypeThaBefo(true);
                 }else{
                     insured.setInsJobTypeThaBefo(false);
                 }
                 if (lrInsuredDataPojo.getHaveDeferPolicy()!=null && lrInsuredDataPojo.getHaveDeferPolicy()!=""){
                     // 既往有延期、拒保的保单
                     insured.setHaveDeferPolicy(true);
                 }
                 if (lrInsuredDataPojo.getHaveHealAbnormal()!=null && lrInsuredDataPojo.getHaveHealAbnormal()!=""){
                     // 既往投保健康告知异常
                     insured.setHaveHealAbnormal(true);
                 }
                 if (lrInsuredDataPojo.getHaveExamineReply()!=null && lrInsuredDataPojo.getHaveExamineReply()!=""){
                     // 既往保单有体检回复记录
                     insured.setHaveExamineReply(true);
                 }
                 if (lrInsuredDataPojo.getHaveProb()!=null && lrInsuredDataPojo.getHaveProb()!=""){
                     // 既往有问题件未回复撤单
                     insured.setHaveProb(true);
                 }
                 if (lrInsuredDataPojo.getHaveSameOccCode()!=null && lrInsuredDataPojo.getHaveSameOccCode()!=""){
                     // 既往投保职业代码与本次一致
                     insured.setHaveSameOccCode(true);
                 }
                 if (lrInsuredDataPojo.getHaveDIsAndCancer()!=null && lrInsuredDataPojo.getHaveDIsAndCancer()!=""){
                     if(insured.getExtendMap()==null){
                         map = new HashMap();

                     }else{
                         map = insured.getExtendMap();
                     }

                     map.put("既往有防癌险或重疾险理赔记录", "true");
                     // 既往有防癌险或重疾险理赔记录
                     insured.setExtendMap(map);
                     //insured.setCancerClaimRecord(true);
                 }
                 if (lrInsuredDataPojo.getHaveAccidentInsurance()!=null && lrInsuredDataPojo.getHaveAccidentInsurance()!=""){
                     if(insured.getExtendMap()==null){
                         map = new HashMap();

                     }else{
                         map = insured.getExtendMap();
                     }
                     map.put("既往有意外险理赔记录", "true");
                     // 既往有意外险理赔记录
                     insured.setExtendMap(map);
                     //insured.setAccidentClaimRecord(true);
                 }
                 //爱永远定期寿险保费
                 insured.setInsuredString1(lrInsuredDataPojo.getInsuredString1()+"");
                 //被保人7054累计保额      核保没有7054
                // insured.set70
                 //被保人2048累计保费
                 insured.setAccPrem2048(lrInsuredDataPojo.getAccPrem2048());
                 //被保人6009意外险保额
                 insured.setSum6009AccidentAmount(lrInsuredDataPojo.getSum6009AccidentAmnt());
                 //7056险种累计投保额     核保没有7056
                 // insured.set7
                 //被保人c060累计保费
                 insured.setC060AccAmnt(lrInsuredDataPojo.getC060AccAmnt());
                 //被保险人投保7053累计重疾险风险保额
                 insured.setAccAmnt7053(lrInsuredDataPojo.getAccAmnt7053());
                 //万能险保费，5015险种已停售
                 insured.setSumStandPrem(lrInsuredDataPojo.getSumStandPrem());
             }
         }
     }
 }

    /**
     * 获取既往挡板数据
     * @param policy
     */
    public void getBarrierData(Policy policy){
        Applicant applicantInfo = policy.getApplicant();
        // 既往有体检记录
        //applicantInfo.setHaveCheckNote(false);
        // 既往有体检、加费、特约的保单
        //applicantInfo.setHaveExaminePolicy(false);
        // 既往有理赔记录
        //applicantInfo.setHaveClaims(false);
        // 既往有保全二核
        //applicantInfo.setHavePreserve(false);
        // 既往有生调记录
        //applicantInfo.setHaveAscertain(false);
        // 既往有投保经历
        //applicantInfo.setHaveApptHistory(false);
        // 既往有次标准体记录
        //applicantInfo.setHaveSubExamine(false);
        // 既往有体检未完成记录
        //applicantInfo.setHaveUnfiExamine(false);
        // 既往告知为五、六类或拒保职业
        /*applicantInfo.setHaveRejectionOcc(false);*/
        // 职业类别低于既往告知
        applicantInfo.setAppntJobTypeThaBefo(false);

        List<Insured> insuredList = policy.getInsuredList();
        for(Insured insured : insuredList){
            // 既往有体检、加费、特约的保单
            insured.setHaveExaminePolicy(false);
            // 既往有理赔记录
            insured.setHaveClaims(false);
            // 既往有保全二核
            insured.setHavePreserve(false);
            // 既往有生调记录
            insured.setHaveAscertain(false);
            // 既往有投保经历
            insured.setHaveApptHistory(false);
            // 既往有次标准体记录
            insured.setHaveSubExamine(false);
            // 既往有体检未完成记录
            insured.setHaveUnfiExamine(false);
            // 既往告知为五、六类或拒保职业
            insured.setHaveRejectionOcc(false);
            // 职业类别低于既往告知
            insured.setInsJobTypeThaBefo(false);
            // 既往有延期、拒保的保单
            insured.setHaveDeferPolicy(false);
            // 既往投保健康告知异常
            insured.setHaveHealAbnormal(false);
            // 既往保单有体检回复记录
            insured.setHaveExamineReply(false);
            // 既往有问题件未回复撤单
            insured.setHaveProb(false);
            // 既往投保职业代码与本次一致
            insured.setHaveSameOccCode(false);
            Map map =null;
            if(insured.getExtendMap()==null){
                map = new HashMap();

            }else{
                map = insured.getExtendMap();
            }
            map.put("既往有防癌险或重疾险理赔记录", "false");
            // 既往有防癌险或重疾险理赔记录
            insured.setExtendMap(map);
            if(insured.getExtendMap()==null){
                map = new HashMap();
            }else{
                map = insured.getExtendMap();
            }
            map.put("既往有意外险理赔记录", "false");
            // 既往有意外险理赔记录
            insured.setExtendMap(map);
        }
    }
}
