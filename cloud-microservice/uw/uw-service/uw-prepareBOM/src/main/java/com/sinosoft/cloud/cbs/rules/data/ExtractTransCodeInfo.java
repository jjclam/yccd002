package com.sinosoft.cloud.cbs.rules.data;

import com.sinosoft.cloud.cbs.rules.bom.*;
import com.sinosoft.cloud.cbs.rules.util.SellTypeEnum;
import com.sinosoft.cloud.cbs.uw.CodeMappingCacheInitService;
import com.sinosoft.cloud.rest.TradeInfo;
import com.sinosoft.lis.entity.*;
import com.sinosoft.utility.ExceptionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class ExtractTransCodeInfo {
    @Autowired
    CodeMappingCacheInitService codeMappingCacheInitService;

    private final Log logger = LogFactory.getLog(this.getClass());

    public void getTransCodeInfo(Policy policy, TradeInfo tradeInfo) {
        //数据交互平台转码微服务
        TradeInfo requestinfo;
        try {
            tradeInfo.addData("cacheMappingName", "ruleMappingName");
            requestinfo = codeMappingCacheInitService.service(tradeInfo);
            if (requestinfo.hasError()) {
                logger.info("核保调用数据转码微服务失败" + requestinfo.getErrorList().toString());
                tradeInfo.addError("核保调用数据转码微服务失败");
                return;
            }
            logger.info("核保调用数据转码：" + requestinfo.toString());
        } catch (Exception e) {
            logger.error("核保调用数据转码微服务异常信息" + ExceptionUtils.exceptionToString(e).toString());
            tradeInfo.addError("核保调用数据转码微服务异常信息" + ExceptionUtils.exceptionToString(e).toString());
            return;
        }
        List<Map<String, Object>> lcPolPojos = (List<Map<String, Object>>) requestinfo.getData(LCPolPojo.class.getSimpleName());
        Map<String, Object> lcContPojo = (Map<String, Object>) requestinfo.getData(LCContPojo.class.getSimpleName());
        Map<String, Object> lcAppntPojo = (Map<String, Object>) requestinfo.getData(LCAppntPojo.class.getSimpleName());
        List<Map<String, Object>> lcInsuredPojos = (List<Map<String, Object>>) requestinfo.getData(LCInsuredPojo.class.getSimpleName());
        List<Map<String, Object>> lcBnfPojos = (List<Map<String, Object>>) requestinfo.getData(LCBnfPojo.class.getSimpleName());

        LCContPojo lcContPojo1 = (LCContPojo) requestinfo.getData(LCContPojo.class.getName());
        logger.info("数据交互传入的sellType：" + lcContPojo.get("sellType") + "");
        if ("".equals(SellTypeEnum.getValue(lcContPojo.get("sellType") + ""))) {
            logger.info(lcContPojo.get("sellType") + "" + "渠道编码未配置转换请检查" + tradeInfo.toString());
            tradeInfo.addError(lcContPojo.get("sellType") + "" + "渠道编码未配置转换请检查");
            return;
        }
        String salechnl = lcContPojo1.getSaleChnl();
        String sellType = SellTypeEnum.getValue(lcContPojo.get("sellType") + "");
        if ("MIP".equals(sellType) && "3".equals(salechnl)) {
            sellType = "02";
        } else if ("MIP".equals(sellType) && "1".equals(salechnl)) {
            sellType = "01";
        }
        // 销售方式
        policy.setSellType(sellType);

        /**********#投保人#*******/
        Applicant applicantInfo = policy.getApplicant();
        if (lcAppntPojo != null) {
            // 婚姻状况
            applicantInfo.setMarrageStatus(lcAppntPojo.get("marriage") + "");
            // 首期支付方式
            applicantInfo.setFirstPayMethod(lcContPojo.get("newPayMode") + "");
            // 续期保险费支付方式
            applicantInfo.setAfterPayMethod(lcContPojo.get("payLocation") + "");
            // 性别
            applicantInfo.setSex(lcAppntPojo.get("appntSex") + "");
            // 证件类型
            applicantInfo.setIdentityType(lcAppntPojo.get("idtype") + "");
            // 职业类别
            applicantInfo.setOccuType(lcAppntPojo.get("occupationType")+"");
            // 职业代码
            //applicantInfo.setOccuCode(lcAppntPojo.getOccupationCode());
            // 首期转账开户行
            applicantInfo.setFirstPayBank(lcContPojo.get("newBankCode") + "");
            // 学历
            applicantInfo.setEducation(lcAppntPojo.get("degree")+"");
            // 地址代码a
            //applicantInfo.setAddrCode(lcAppntPojo.getAddressNo());
            // 与被保人关系
            //applicantInfo.setRelationWithInsured(lcAppntPojo.getRelatToInsu());
        }

        /**********#险种#*******/
        List insurTypeList = policy.getRiskList();
        if (lcPolPojos != null && lcPolPojos.size() > 0) {
            for (int i = 0; i < lcPolPojos.size(); i++) {
                Risk risk = (Risk) insurTypeList.get(i);
                //险种续保标记
                risk.setRnewFlag(lcPolPojos.get(i).get("rnewFlag") + "");
                // 交费频率
                risk.setPaymentFreqType(lcPolPojos.get(i).get("payIntv") + "");
                // 养老金领取频率（1-按年 ， 2-按月） 备注:老核心没有
                // risk.setAnnuityFrequency(lcPolPojo.getFQGetMode());
                // 首期保险费支付方式
                // risk.setFirstPrePayMethod(lcContPojo.get("newPayMode")+"");
                risk.setFirstPrePayMethod(lcPolPojos.get(i).get("payMode") + "");
                // 续期保险费支付方式
                // risk.setAfterPrePayMethod(lcContPojo.get("payLocation")+"");
                risk.setAfterPrePayMethod(lcPolPojos.get(i).get("payLocation") + "");
                //insurTypeList.add(risk);

            }
        }
        /**********#被保人#*******/
        List insuredInfoList = policy.getInsuredList();
        if (lcInsuredPojos != null && lcInsuredPojos.size() > 0) {
            for (int i = 0; i < lcInsuredPojos.size(); i++) {
                Insured insured = (Insured) insuredInfoList.get(i);
                // 婚姻状况
                insured.setMarrageStatus(lcInsuredPojos.get(i).get("marriage") + "");
                // 与第一被保人关系
                //insured.setRelationWithFirstInsured(lcInsuredPojos.get(0).getRelationToMainInsured());
                // 与投保人关系
                // insured.setRelationWithAppnt(lcInsuredPojos.get(0).getRelationToAppnt());
                // 性别
                insured.setSex(lcInsuredPojos.get(i).get("sex") + "");
                // 证件类型
                insured.setIdentityType(lcInsuredPojos.get(i).get("idtype") + "");
                // 职业类别
                 insured.setOccuType(lcInsuredPojos.get(0).get("occupationType")+"");
                // 职业代码
                // insured.setOccuCode(lcInsuredPojos.get(0).getOccupationCode());
                // 学历
                insured.setEducation(lcInsuredPojos.get(0).get("degree")+"");
                // 地址代码
                //  insured.setAddrCode(lcInsuredPojos.get(0).getAddressNo());

                //  insuredInfoList.add(insured);
            }
        }
        /**********#受益人#*******/

        if (!"22".equals(lcContPojo1.getSellType()) && !"24".equals(lcContPojo1.getSellType()) &&
                !"25".equals(lcContPojo1.getSellType()) && !"26".equals(lcContPojo1.getSellType()) &&
                !"27".equals(lcContPojo1.getSellType())) {
            List bnfcryInfoList = policy.getBeneficiaryList();
            if (null != lcBnfPojos && lcBnfPojos.size() > 0) {
                for (int i = 0; i < lcBnfPojos.size(); i++) {
                    Beneficiary benneficy = (Beneficiary) bnfcryInfoList.get(i);
                    // 受益类别
                    benneficy.setBeneficialType(lcBnfPojos.get(i).get("bnfType") + "");
                    // 受益顺序
                    //benneficy.setBeneficialOrder(Util.toInt(lcBnfPojos.get(i).getBnfGrade()));
                    /** 与被保人关系 */
                    // String realtionWithInsured = lcBnfPojos.get(i).getRelationToInsured();
                    //benneficy.(realtionWithInsured);
                    // 证件类型
                    benneficy.setIdentityType(lcBnfPojos.get(i).get("idtype") + "");
                    //xingb
                    benneficy.setSex(lcBnfPojos.get(i).get("sex") + "");
                }
            }

        }
    }

}
