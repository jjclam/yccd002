package com.sinosoft.cloud.cbs.bl;

import com.sinosoft.cache.dao.impl.RedisCommonDao;
import com.sinosoft.cloud.bl.AbstractBL;
import com.sinosoft.cloud.cbs.nb.bl.AipWsClientCommon;
import com.sinosoft.cloud.cbs.trules.specontract.SearchHistoryInfoSrvBL;
import com.sinosoft.cloud.cbs.trules.specontract.UWSpecialContractBL;
import com.sinosoft.cloud.cbs.trules.util.CredenTypeEnum;
import com.sinosoft.cloud.cbs.trules.util.DomainUtils;
import com.sinosoft.cloud.rest.TradeInfo;
import com.sinosoft.lis.entity.*;
import com.sinosoft.utility.ExceptionUtils;
import com.sinosoft.utility.JdomUtil;
import com.sinosoft.utility.SSRS;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jdom.Document;
import org.jdom.Element;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Author:wangshuliang
 * @Description:
 * @Date:Created in 11:08 2018/6/30
 * @Modified by:
 */
@Component
public class UpdateSecContactBL extends AbstractBL {
    private final Log logger = LogFactory.getLog(this.getClass());
    @Value("${cloud.aip.uw.url}")
    private String url;
    @Value("${cloud.aip.uw.namespace}")
    private String namespace;
    @Value("${cloud.aip.uw.method}")
    private String method;
    @Value("${cloud.aip.uw.update}")
    private String reqType;
    @Value("${cloud.aip.uw.arg0}")
    private String arg0;
    @Value("${cloud.aip.uw.arg1}")
    private String arg1;
    @Autowired
    private AipWsClientCommon aipWsClientCommon;
    @Autowired
    private RedisCommonDao redisCommonDao;
    @Autowired
    private SearchHistoryInfoSrvBL searchHistoryInfoSrvBL;
    @Autowired
    private UWSpecialContractBL uwSpecialContractBL;

    @Override
    public TradeInfo dealData(TradeInfo tradeInfo) {
        List<LCPolPojo> lcPolPojos = (List<LCPolPojo>) tradeInfo.getData(LCPolPojo.class.getName());
        if (lcPolPojos == null || lcPolPojos.size() <= 0) {
            logger.debug("险种信息为空" + tradeInfo.toString());
            tradeInfo.addData("险种信息为空");
            return tradeInfo;
        }
        String mainRiskCode = "000000";
        for (int i = 0; i < lcPolPojos.size(); i++) {
            if (lcPolPojos.get(i).getPolNo().equals(lcPolPojos.get(i).getMainPolNo())) {
                mainRiskCode = lcPolPojos.get(i).getRiskCode();
            }
        }
        if ("000000".equals(mainRiskCode)) {
            logger.debug("主险信息为空" + tradeInfo.toString());
            tradeInfo.addData("主险信息为空");
            return tradeInfo;
        } else {
            LMRiskProcessPojo lmRiskProcessPojo = (LMRiskProcessPojo) tradeInfo.getData(LMRiskProcessPojo.class.getName());
            if (lmRiskProcessPojo == null) {
                lmRiskProcessPojo = redisCommonDao.getEntityRelaDB(LMRiskProcessPojo.class, mainRiskCode);
            }
            if (lmRiskProcessPojo != null && !"Y".equals(lmRiskProcessPojo.getSpecialApproveFlag())) {
                logger.debug("此险种不需要走更新特殊审批信息" + mainRiskCode + tradeInfo.toString());
                return tradeInfo;
            }
        }

        LCPolicyInfoPojo lcPolicyInfoPojo = (LCPolicyInfoPojo) tradeInfo.getData(LCPolicyInfoPojo.class.getName());
        if (lcPolicyInfoPojo==null){
            logger.debug("中介信息表信息为空"+tradeInfo.toString());
            tradeInfo.addError("中介信息表为空");
            return tradeInfo;

        }


        LCContPojo lcContPojo = (LCContPojo) tradeInfo.getData(LCContPojo.class.getName());
        if (lcContPojo == null) {
            logger.debug("保单信息为空" + tradeInfo.toString());
            tradeInfo.addError("保单信息为空");
            return tradeInfo;
        }
        List<LCInsuredPojo> lcInsuredPojos = (List<LCInsuredPojo>) tradeInfo.getData(LCInsuredPojo.class.getName());
        if (lcInsuredPojos == null || lcInsuredPojos.size() <= 0) {
            logger.debug("被保人信息为空" + tradeInfo.toString());
            tradeInfo.addError("被保人信息为空");
            return tradeInfo;
        }
        LCInsuredPojo lcInsuredPojo = lcInsuredPojos.get(0);
        boolean uwflag = false;
        boolean claimFlag = false;
        boolean resUWFlag = true;
        boolean resclaimFlag = true;
        /**
         *  是否使用额度调整接口01-未使用   00-使用
         *  既往核保 02
         *  既往理赔 03
         *  人数         04
         */
        String CHECKRULE = (String) tradeInfo.getData("CHECKRULE");
        if (StringUtils.isEmpty(CHECKRULE)) {
            //如果checkrule为空则默认未使用
            CHECKRULE = "01";
        }
        /**
         * 判断核保审批是否成功
         * 核保审批成功01
         * 核保审批失败02
         */
        String CHECKFAILRULE = "00";
        SSRS uwSSRS = null;
        try {
            uwSSRS = searchHistoryInfoSrvBL.dealDataUW(lcContPojo, lcInsuredPojo);
            if (uwSSRS == null || uwSSRS.getMaxRow() < 1) {
                logger.debug(lcInsuredPojo.getInsuredNo() + "[AIPE0013]:团险核心没有符合条件的查询数据");
            } else {
                logger.debug(lcInsuredPojo.getInsuredNo() + "团险核心此被保人存在既往核保");
                uwflag = true;
            }
            if (!uwflag) {
                uwSSRS = searchHistoryInfoSrvBL.dealDataUWOLD(lcContPojo, lcInsuredPojo);
                if (uwSSRS == null || uwSSRS.getMaxRow() < 1) {
                    logger.debug(lcInsuredPojo.getInsuredNo() + "[AIPE0013]:老核心没有符合条件的查询数据");
                } else {
                    logger.debug(lcInsuredPojo.getInsuredNo() + "老核心此被保人存在既往核保");
                    uwflag = true;
                }
            }
        } catch (Exception e) {
            logger.error(ExceptionUtils.exceptionToString(e));
            logger.debug("6807更新既往核保信息查询:提数失败" + tradeInfo.toString());
            tradeInfo.addError("6807更新既往核保信息查询：提数失败:");
            return tradeInfo;
        }

        //既往理赔信息查询
        SSRS claimSSRS = null;
        try {
            claimSSRS = searchHistoryInfoSrvBL.dealDataClaim(lcContPojo, lcInsuredPojo);
            if (claimSSRS == null || claimSSRS.getMaxRow() < 1) {
                logger.debug(lcInsuredPojo.getInsuredNo() + "[AIPE0013]:没有符合条件的查询数据");
            } else {
                logger.debug(lcInsuredPojo.getInsuredNo() + "此被保人存在既往理赔");
                claimFlag = true;
            }
//            if (!claimFlag) {
//                claimSSRS = searchHistoryInfoSrvBL.dealDataClaimOLD(lcContPojo, lcInsuredPojo);
//                if (claimSSRS == null || claimSSRS.getMaxRow() < 1) {
//                    logger.debug(lcInsuredPojo.getInsuredNo() + "[AIPE0013]:没有符合条件的查询数据");
//                } else {
//                    logger.debug(lcInsuredPojo.getInsuredNo() + "此被保人存在既往理赔");
//                    claimFlag = true;
//                }
//            }

        } catch (Exception e) {
            logger.error(ExceptionUtils.exceptionToString(e));
            logger.debug("6807更新既往核保信息查询:提数失败" + tradeInfo.toString());
            tradeInfo.addError("6807更新既往核保信息查询：提数失败:");
            return tradeInfo;
        }

        String productCode = lcInsuredPojo.getContPlanCode();
        try {
            if (uwflag) {
                //既往核保
                if (claimFlag) {
                    //既往理赔
                    if ("YA".equals(productCode) || "Y6".equals(productCode) || "YB".equals(productCode) || "YE".equals(productCode) || "XJYXA".equals(productCode) || "XJYXB".equals(productCode)) {
                        //调用查询既往审批接口
                        resUWFlag = uwSpecialContractBL.dealData(tradeInfo, lcPolicyInfoPojo.getJYContNo(), "01");
                        if (resUWFlag) {
                            CHECKRULE = "02";//既往核保
                            CHECKFAILRULE = "01";//核保审批成功
                        } else {
                            CHECKFAILRULE = "02";// 核保审批失败
                        }
                        //调用查询既往审批信息接口
                        resclaimFlag = uwSpecialContractBL.dealData(tradeInfo, lcPolicyInfoPojo.getJYContNo(), "02");
                        if (resclaimFlag) {
                            CHECKRULE = "03";// 既往理赔
                        }
                    }
                } else {
                    if ("YA".equals(productCode) || "Y6".equals(productCode) || "YB".equals(productCode) || "YE".equals(productCode) || "XJYXA".equals(productCode) || "XJYXB".equals(productCode)) {
                        //调用查询既往审批接口
                        resUWFlag = uwSpecialContractBL.dealData(tradeInfo, lcPolicyInfoPojo.getJYContNo(), "01");
                        if (resUWFlag) {
                            CHECKRULE = "02";//既往核保
                            CHECKFAILRULE = "01";//核保审批成功
                        } else {
                            CHECKFAILRULE = "02";// 核保审批失败
                        }
                    }
                }
            } else {
                if (claimFlag) {
                    if ("YA".equals(productCode) || "Y6".equals(productCode) || "YB".equals(productCode) || "YE".equals(productCode) || "XJYXA".equals(productCode) || "XJYXB".equals(productCode)) {
                        //调用查询既往审批信息接口
                        resclaimFlag = uwSpecialContractBL.dealData(tradeInfo, lcPolicyInfoPojo.getJYContNo(), "02");
                        if (resclaimFlag) {
                            CHECKRULE = "03";// 既往理赔
                        }
                    }
                }
            }
        } catch (Exception e) {
            logger.error("1004跟新特殊审批信息服务异常" + tradeInfo.toString());
            logger.error("1004跟新特殊审批信息服务异常" + ExceptionUtils.exceptionToString(e));
            tradeInfo.addError("调用更新特殊审批信息服务异常");
            return tradeInfo;
        }


        /**
         * 判断是否调用了特殊功能审批
         * 如果使用调用使用接口将状态改为无效
         */
        String xml = "";
        String code = "";
        if (CHECKRULE.equals("00")) {
            logger.debug(lcInsuredPojo.getContNo() + "AIP101102_9 使用特殊业务 额度 Service Start");//04
            //xml=getXML(tradeInfo,"04");
            code = "04";
        }
        if (CHECKRULE.equals("02")) {
            logger.debug(lcInsuredPojo.getContNo() + "AIP101102_9 使用特殊业务  既往核保 Service Start");//01
            //xml=getXML(tradeInfo,"01");
            code = "01";
        }
        if (CHECKRULE.equals("03")) {//01

            if ("01".equals(CHECKFAILRULE)) {//
                logger.debug(lcInsuredPojo.getContNo() + "AIP101102_9 使用特殊业务  既往核保 Service Start");
                //xml=getXML(tradeInfo,"01");
                code = "01";
            }
            //如果都使用必须保证小的在前面并且用英文逗号隔开
            if ("01".equals(code)) {
                code = "01,02";
            } else {
                code = "02";
            }
            logger.debug(lcInsuredPojo.getContNo() + "AIP101102_9 使用特殊业务 既往理赔 Service Start");
            // xml=getXML(tradeInfo,code);
        }
        if (StringUtils.isEmpty(code)) {
            logger.debug("不需要调用更新特殊审批信息" + lcContPojo.getContNo());
            return tradeInfo;
        }
   /*     if (CHECKRULENUM.equals("00")) {//03  暂时不需要
            logger.debug("AIP101102_9  使用特殊业务  人数 Service Start");
            xml=getXML(tradeInfo,"03");
        }*/
        xml = getXML(tradeInfo, code);
        logger.debug(lcContPojo.getContNo() + "特约审批信息请求报文为");
        Map<String, Object> reqMap = new HashMap<>();
        reqMap.put(arg0, reqType);
        reqMap.put(arg1, xml);
        try {
            String res = aipWsClientCommon.sendService(url, namespace, method, reqMap);
            if (StringUtils.isEmpty(res)) {
                logger.debug("更新特殊审批信息返回报文信息为空" + tradeInfo.toString());
                tradeInfo.addError("更新特殊审批信息返回报文为空");
                return tradeInfo;
            }
            logger.debug(lcContPojo.getContNo() + "更新特殊审批信息成功返回报文为" + res);
            Document reqDoc = JdomUtil.build(res, true);
            Element queryRes = reqDoc.getRootElement();
            Element head = queryRes.getChild("head");
            Element response = head.getChild("response");
            String businessRespCode = response.getChildText("businessRespCode");
            //没有审批有效的数据 01
            if ("01".equals(businessRespCode)) {
                logger.debug(lcContPojo.getContNo() + "更新特殊审批信息失败" + response.getChildText("businessRespText"));
                tradeInfo.addError("更新特殊审批信息失败");
                return tradeInfo;
                //02 获取特殊审批信息成功
            } else if ("00".equals(businessRespCode)) {
                logger.debug(lcContPojo.getContNo() + "获取特约审批信息成功");
                return tradeInfo;
            }
        } catch (Exception e) {
            logger.debug("获取特约审批信息失败" + ExceptionUtils.exceptionToString(e));
            logger.debug("获取特约审批信息失败" + tradeInfo.toString());
            tradeInfo.addError("获取特约审批信息失败");
            return tradeInfo;
        }
        return tradeInfo;
    }

    /**
     * 获取更新特殊审批信息的报文
     *
     * @param tradeInfo       保单信息
     * @param approveTypeCode 01延期拒保 02 理赔 03 被保险人数 04借款人保险额度调整
     * @return
     */
    public String getXML(TradeInfo tradeInfo, String approveTypeCode) {
        LCContPojo lcContPojo = (LCContPojo) tradeInfo.getData(LCContPojo.class.getName());
        List<LCInsuredPojo> lcInsuredPojos = (List<LCInsuredPojo>) tradeInfo.getData(LCInsuredPojo.class.getName());
        LCPolicyInfoPojo lcPolicyInfoPojo = (LCPolicyInfoPojo) tradeInfo.getData(LCPolicyInfoPojo.class.getName());
        LCInsuredPojo lcInsuredPojo = lcInsuredPojos.get(0);
        String request = "<?xml version=\"1.0\" encoding=\"utf-8\"?>" +
                "<message>" +
                "<head>" +
                "<sysHeader>" +
                "<standardVersionCode/>" +
                "<msgDate>" + lcContPojo.getPolApplyDate() + "</msgDate>" +
                "<msgTime>" + lcContPojo.getMakeTime() + "</msgTime>" +
                "<systemCode>AIP</systemCode>" +
                "<transactionCode>1011</transactionCode>" +
                "<transRefGUID/>" +
                "<esbRefGUID/>" +
                "<transNo/>" +
                "<businessCode/>" +
                "<businessType/>" +
                "</sysHeader>" +
                "<domHeader/>" +
                "<bizHeader/>" +
                "</head>" +
                "<body>" +
                "<lcInsuredInfo>" +
                "<name>" + lcInsuredPojo.getName() + "</name>" +
                "<sex>" + DomainUtils.convertSex(lcInsuredPojo.getSex()) + "</sex>" +
                "<idType>" + CredenTypeEnum.getValue(lcInsuredPojo.getIDType()) + "</idType>" +
                "<idNo>" + lcInsuredPojo.getIDNo() + "</idNo>" +
                "<birthday>" + lcInsuredPojo.getBirthday() + "</birthday>" +
                "</lcInsuredInfo>" +
                "<loanContractNo>" + lcPolicyInfoPojo.getJYContNo() + "</loanContractNo>" +
                "<approveInfos>" +
                "<approveInfo>" +
                "<approveTypeCode>" + approveTypeCode + "</approveTypeCode>" +// 01延期拒保 02 理赔 03 被保险人数 04借款人保险额度调整
                "</approveInfo>" +
                "</approveInfos>" +
                "<updateInfo>" +
                "<contNo>" + lcContPojo.getContNo() + "</contNo>" +
                "<useState>02</useState>" +//01未使用 02 已使用
                "</updateInfo>" +
                "</body>" +
                "</message>";
        return request;
    }
}
