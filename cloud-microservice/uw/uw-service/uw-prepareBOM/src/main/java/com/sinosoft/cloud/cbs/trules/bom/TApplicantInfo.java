package com.sinosoft.cloud.cbs.trules.bom;

import java.util.Date;

/**
 * 投保人信息
 *
 */
public class TApplicantInfo {
	/**
	 * 投保人姓名
	 */
	private String applicantName;
	/**
	 * 投保人英文名
	 */
	private String applicantEngName;
	/**
	 * 证件类型
	 */
	private String applicantCerttype;
	/**
	 * 证件号
	 */
	private String applicantCertno;
	/**
	 * 性别
	 */
	private String applicantGender;
	/**
	 * 出生日期
	 */
	private String applicantBirthDate;
	/**
	 * 投保人年龄
	 */
	private int applicantAge;
	/**
	 * 与被保人关系
	 */
	private String relaToInsurd;
	/**
	 * 联系电话
	 */
	private String contactTel;
	/**
	 * 联系地址
	 */
	private String contactAddr;
	/**
	 * 联系地址（编码）
	 */
	private String contactAddrno;
	
	
	/**
	 * 国籍
	 */
	private String nationality;
	
	/**
	 * 固定电话
	 */
	private String phone;
	
	
	/**
	 * 证件有效期
	 */
	private String idValidDate;
	
	/**
	 * 国家登记
	 */
	private String countryCat;
	
	
	/**
	 * 单位名称
	 */
	private String companyName;
	
	/**
	 * 单位地址
	 */
	private String companyAddr;
	
	/**
	 * 单位电话
	 */
	private String companyTel;
	
	/**
	 * 增值税一般纳税人资质日期
	 */
	private String vatGenerTaxpayerQualifDate;
	
	/**
	 * 统一社会信用代码
	 */
	private String uniSocialCreditCode;
	
	/**
	 * 税务登记证号
	 */
	private String taxRegCertNo;
	
	/**
	 * 开户银行全称
	 */
	private String openbankFullName;
	
	/**
	 * 银行账号
	 */
	private String bankAccno;
	
	/**
	 * 3个（含3）不同投保人存在相同电话号码
	 */
	private String repeatPhoneByThreeName;
	/**
	 * 投保人邮箱
	 * @return
	 */
	private String applMailBox;
	
	
	/**
	 * 省(直辖市)	 
	 */
	private String prov;
	/**
	 * 市	 
	 */
	private String city;
	/**
	 * 区	 
	 */
	private String zone;
	
	
	/**
	 * 职业类别
	 */
	private String occupationCat;

	/**
	 * 3个（含3）以上不同投保人存在相同电话
	 */
	private boolean repeatPhoneByFourName;

	/**
	 * 投保人的证件号与黑名单相同
	 */
	private boolean appBlackIdFlag;

	/**
	 * 投保人的全名与黑名单相同且生日相同
	 */
	private boolean appBlackFullFlag;

	/**
	 * 非中国籍投保人姓名与黑名单相同且生日相同（人名为1部分）
	 */
	private boolean appBlackFlag1;

	/**
	 * 非中国籍投保人姓名与黑名单相同且生日相同（人名为2部分）
	 */
	private boolean appBlackFlag2;

	/**
	 * 非中国籍投保人姓名与黑名单两个或以上名字相同（人名为2部分）
	 */
	private boolean appBlackFlag3;

	/**
	 * 非中国籍投保人姓名与黑名单相同且生日相同（人名大于3部分）
	 */
	private boolean appBlackFlag4;

	/**
	 * 非中国籍投保人姓名与黑名单两个或以上名字相同（人名大于3部分）
	 */
	private boolean appBlackFlag5;

	public String getApplicantBirthDate() {return applicantBirthDate;}
	public String getIdValidDate() {return idValidDate;}
	public boolean isRepeatPhoneByFourName() {return repeatPhoneByFourName;}
	public void setRepeatPhoneByFourName(boolean repeatPhoneByFourName) {this.repeatPhoneByFourName = repeatPhoneByFourName;}
	public String getOccupationCat() {
		return occupationCat;
	}
	public void setOccupationCat(String occupationCat) {
		this.occupationCat = occupationCat;
	}
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public String getCompanyAddr() {
		return companyAddr;
	}
	public void setCompanyAddr(String companyAddr) {
		this.companyAddr = companyAddr;
	}
	public String getCompanyTel() {
		return companyTel;
	}
	public void setCompanyTel(String companyTel) {
		this.companyTel = companyTel;
	}

	public void setApplicantBirthDate(String applicantBirthDate) {
		this.applicantBirthDate = applicantBirthDate;
	}

	public void setIdValidDate(String idValidDate) {
		this.idValidDate = idValidDate;
	}

	public String getVatGenerTaxpayerQualifDate() {
		return vatGenerTaxpayerQualifDate;
	}
	public void setVatGenerTaxpayerQualifDate(String vatGenerTaxpayerQualifDate) {
		this.vatGenerTaxpayerQualifDate = vatGenerTaxpayerQualifDate;
	}

	public String getUniSocialCreditCode() {
		return uniSocialCreditCode;
	}
	public void setUniSocialCreditCode(String uniSocialCreditCode) {
		this.uniSocialCreditCode = uniSocialCreditCode;
	}
	public String getTaxRegCertNo() {
		return taxRegCertNo;
	}
	public void setTaxRegCertNo(String taxRegCertNo) {
		this.taxRegCertNo = taxRegCertNo;
	}
	public String getOpenbankFullName() {
		return openbankFullName;
	}
	public void setOpenbankFullName(String openbankFullName) {
		this.openbankFullName = openbankFullName;
	}
	public String getBankAccno() {
		return bankAccno;
	}
	public void setBankAccno(String bankAccno) {
		this.bankAccno = bankAccno;
	}


	public String getCountryCat() {
		return countryCat;
	}
	public void setCountryCat(String countryCat) {
		this.countryCat = countryCat;
	}
	public String getApplicantName() {
		return applicantName;
	}
	public void setApplicantName(String applicantName) {
		this.applicantName = applicantName;
	}
	public String getApplicantEngName() {
		return applicantEngName;
	}
	public void setApplicantEngName(String applicantEngName) {
		this.applicantEngName = applicantEngName;
	}
	public String getApplicantCerttype() {
		return applicantCerttype;
	}
	public void setApplicantCerttype(String applicantCerttype) {
		this.applicantCerttype = applicantCerttype;
	}
	public String getApplicantCertno() {
		return applicantCertno;
	}
	public void setApplicantCertno(String applicantCertno) {
		this.applicantCertno = applicantCertno;
	}
	public String getApplicantGender() {
		return applicantGender;
	}
	public void setApplicantGender(String applicantGender) {
		this.applicantGender = applicantGender;
	}
	public String getRelaToInsurd() {
		return relaToInsurd;
	}
	public void setRelaToInsurd(String relaToInsurd) {
		this.relaToInsurd = relaToInsurd;
	}
	public String getContactTel() {
		return contactTel;
	}
	public void setContactTel(String contactTel) {
		this.contactTel = contactTel;
	}
	public String getContactAddr() {
		return contactAddr;
	}
	public void setContactAddr(String contactAddr) {
		this.contactAddr = contactAddr;
	}
	public String getContactAddrno() {
		return contactAddrno;
	}
	public void setContactAddrno(String contactAddrno) {
		this.contactAddrno = contactAddrno;
	}
	
	public String getNationality() {
		return nationality;
	}
	public void setNationality(String nationality) {
		this.nationality = nationality;
	}
	public int getApplicantAge() {
		return applicantAge;
	}
	public void setApplicantAge(int applicantAge) {
		this.applicantAge = applicantAge;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}

	
	public String getApplMailBox() {
		return applMailBox;
	}	
	public void setApplMailBox(String applMailBox) {
		this.applMailBox = applMailBox;
	}
	public String getProv() {
		return prov;
	}
	public void setProv(String prov) {
		this.prov = prov;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getZone() {
		return zone;
	}
	public void setZone(String zone) {
		this.zone = zone;
	}
	public String getRepeatPhoneByThreeName() {
		return repeatPhoneByThreeName;
	}
	public void setRepeatPhoneByThreeName(String repeatPhoneByThreeName) {
		this.repeatPhoneByThreeName = repeatPhoneByThreeName;
	}

	public boolean isAppBlackIdFlag() {return appBlackIdFlag;}
	public void setAppBlackIdFlag(boolean appBlackIdFlag) {this.appBlackIdFlag = appBlackIdFlag;}
	public boolean isAppBlackFullFlag() {return appBlackFullFlag;}
	public void setAppBlackFullFlag(boolean appBlackFullFlag) {this.appBlackFullFlag = appBlackFullFlag;}
	public boolean isAppBlackFlag1() {return appBlackFlag1;}
	public void setAppBlackFlag1(boolean appBlackFlag1) {this.appBlackFlag1 = appBlackFlag1;}
	public boolean isAppBlackFlag2() {return appBlackFlag2;}
	public void setAppBlackFlag2(boolean appBlackFlag2) {this.appBlackFlag2 = appBlackFlag2;}
	public boolean isAppBlackFlag3() {return appBlackFlag3;}
	public void setAppBlackFlag3(boolean appBlackFlag3) {this.appBlackFlag3 = appBlackFlag3;}
	public boolean isAppBlackFlag4() {return appBlackFlag4;}
	public void setAppBlackFlag4(boolean appBlackFlag4) {this.appBlackFlag4 = appBlackFlag4;}
	public boolean isAppBlackFlag5() {return appBlackFlag5;}
	public void setAppBlackFlag5(boolean appBlackFlag5) {this.appBlackFlag5 = appBlackFlag5;}
}
