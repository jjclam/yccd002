package com.sinosoft.cloud.cbs.trules.bom;

import java.util.Map;

/**
 * 险种信息
 *
 */
public class Risk {
	
	/**
	 * 产品组合编码
	 */
	private String prodGroupCode;
	
	/**
	 * 产品组合版本
	 */
	private String prodGroupVersion;
	
	/**
	 * 险种编码
	 */
	private String riskCode;
	
	/**
	 * 险种名称
	 */
	private String riskName;
	
	/**
	 * 险种版本
	 */
	private String riskVersion;
	
	/**
	 * 保费
	 */
	private double prem;
	
	/**
	 * 总份数
	 */
	private int mult;
	
	/**
	 * 主附险标识
	 */
	private String mainPolFlag;
	
	/**
	 * 主险险种编码
	 */
	private String mainRiskCode;
	
	/**
	 * 险种扩展信息
	 */
	private Map<String,String> riskExtendInfos;

	public Map<String, String> getRiskExtendInfos() {
		return riskExtendInfos;
	}

	public void setRiskExtendInfos(Map<String, String> riskExtendInfos) {
		this.riskExtendInfos = riskExtendInfos;
	}

	public String getProdGroupCode() {
		return prodGroupCode;
	}

	public void setProdGroupCode(String prodGroupCode) {
		this.prodGroupCode = prodGroupCode;
	}

	public String getProdGroupVersion() {
		return prodGroupVersion;
	}

	public void setProdGroupVersion(String prodGroupVersion) {
		this.prodGroupVersion = prodGroupVersion;
	}

	public String getRiskCode() {
		return riskCode;
	}

	public void setRiskCode(String riskCode) {
		this.riskCode = riskCode;
	}

	public String getRiskName() {
		return riskName;
	}

	public void setRiskName(String riskName) {
		this.riskName = riskName;
	}

	public String getRiskVersion() {
		return riskVersion;
	}

	public void setRiskVersion(String riskVersion) {
		this.riskVersion = riskVersion;
	}

	public double getPrem() {
		return prem;
	}

	public void setPrem(double prem) {
		this.prem = prem;
	}

	public int getMult() {
		return mult;
	}

	public void setMult(int mult) {
		this.mult = mult;
	}

	public String getMainPolFlag() {
		return mainPolFlag;
	}

	public void setMainPolFlag(String mainPolFlag) {
		this.mainPolFlag = mainPolFlag;
	}

	public String getMainRiskCode() {
		return mainRiskCode;
	}

	public void setMainRiskCode(String mainRiskCode) {
		this.mainRiskCode = mainRiskCode;
	}


}
