package com.sinosoft.cloud.cbs.rules.httpclient;

import com.sinosoft.cloud.cbs.rules.data.ErrorCatch;
import com.sinosoft.cloud.rest.TradeInfo;
import com.sinosoft.utility.ExceptionUtils;
import org.apache.commons.httpclient.ConnectTimeoutException;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.RequestEntity;
import org.apache.commons.httpclient.methods.StringRequestEntity;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.net.ConnectException;
import java.net.NoRouteToHostException;
import java.net.SocketTimeoutException;

/**
 * 传入请求报文，发送请求，获得响应报文
 * @author dingfan
 *
 */
public class HttpRequestClient {
	private static Log logger = LogFactory.getLog(HttpRequestClient.class);
	public static String execute(String requestXml, String url, TradeInfo tradeInfo) {
		String responseXml = "";
		HttpClient httpclient = new HttpClient();
		//链接超时
		httpclient.getHttpConnectionManager().getParams().setConnectionTimeout(5000);
		//读取超时
		httpclient.getHttpConnectionManager().getParams().setSoTimeout(5000);
		//保单号
		String contno = requestXml.substring(requestXml.indexOf("<policyNo>")+10, requestXml.indexOf("</policyNo>"));
		//异常捕捉类
		ErrorCatch eCatch = new ErrorCatch();
		PostMethod post = new PostMethod(url);
		try {
			RequestEntity entity = new StringRequestEntity(requestXml, "text/xml", "UTF-8");
			post.setRequestEntity(entity);
			httpclient.executeMethod(post);
			int code = post.getStatusCode();
			if(code == HttpStatus.SC_OK || code == 500) {
				responseXml = post.getResponseBodyAsString();
			}
		} catch(SocketTimeoutException e) {
			//1 响应超时
			logger.error("HTTP客户端程序异常-响应超时"+ExceptionUtils.exceptionToString(e));
			//超时，可能规则集未加载到内存
			responseXml = eCatch.getErrorResponse(contno,e,  "ODM0031", "核保客户端程序-响应超时");
			tradeInfo.addError("核保客户端程序响应超时");
		}catch (ConnectTimeoutException e) {
			logger.error("HTTP客户端程序异常-请求超时"+ExceptionUtils.exceptionToString(e));
			responseXml = eCatch.getErrorResponse(contno,e, "ODM0032", "核保客户端程序-请求超时");
			tradeInfo.addError("核保客户端程序请求超时");
		}catch (NoRouteToHostException e) {
			logger.error("HTTP客户端程序异常-路由错误连接不到服务端"+ExceptionUtils.exceptionToString(e));
			responseXml = eCatch.getErrorResponse(contno,e, "ODM0033", "核保客户端程序-通讯异常");
			tradeInfo.addError("核保客户端程序通讯异常");
		}catch (ConnectException e) {
			//服务端服务可能未启动
			logger.error("HTTP客户端程序异常-服务端无响应"+ExceptionUtils.exceptionToString(e));
			responseXml = eCatch.getErrorResponse(contno,e, "ODM0034", "核保客户端程序-服务端无响应");
			tradeInfo.addError("核保客户端程序服务端无响应");
		}catch (Exception e) {
			//捕捉所有异常
			logger.error("HTTP客户端程序异常"+ExceptionUtils.exceptionToString(e));
			responseXml = eCatch.getErrorResponse(contno,e, "ODM0030", "核保客户端程序");
			tradeInfo.addError("核保客户端程序");
		}
		return responseXml;
	}
}
