package com.sinosoft.cloud.cbs.rules.data;

import com.sinosoft.cache.dao.impl.RedisCommonDao;
import com.sinosoft.cloud.cbs.rules.bom.Applicant;
import com.sinosoft.cloud.cbs.rules.bom.Policy;
import com.sinosoft.cloud.rest.TradeInfo;
import com.sinosoft.lis.entity.LCAddressPojo;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by zkr on 2018/6/21.
 */
@Service
public class NewExtractCheck {
    private final Log logger = LogFactory.getLog(this.getClass());
    @Value("${cloud.uw.barrier.control}")
    private String barrier;
    @Autowired
    RedisCommonDao redisCommonDao;
    public void getCheck(TradeInfo tradeInfo, Policy policy) {
        // 投保人信息
        Applicant applicantInfo = policy.getApplicant();
        if ("true".equals(barrier)) {
            logger.debug("保单号：" + policy.getContNo() + "，累计风险保额查重挡板启动！");
            getOtherData(applicantInfo);
            return;
        }
        //投保人地址信息
        LCAddressPojo lcAddressPojo = ((List<LCAddressPojo>) tradeInfo.getData(LCAddressPojo.class.getName())).get(0);
        //电话集合
        List<String> phones = new ArrayList<>();

        // 投保人ID
        String appntno = applicantInfo.getClientNo();
        //投保人姓名
        String appntname = applicantInfo.getApplicantName();
        //投保人通讯电话
        String phone = lcAddressPojo.getPhone();
        if (phone != null && !"".equals(phone)) {
            phones.add(phone);
        }
        //投保人家庭电话
        String homePhone = lcAddressPojo.getHomePhone();
        if (homePhone != null && !"".equals(homePhone)) {
            phones.add(homePhone);
        }
        //投保人单位电话
        String companyPhone = lcAddressPojo.getCompanyPhone();
        if (companyPhone != null && !"".equals(companyPhone)) {
            phones.add(companyPhone);
        }
        //投保人手机号
        String mobile = lcAddressPojo.getMobile();
        if (mobile != null && !"".equals(mobile)) {
            phones.add(mobile);
        }
        Set<String> sets = new HashSet<String>();
        Set<String> customnoSet = new HashSet<String>();
        String phone1 = redisCommonDao.getAppntPhone(phone);
        if (phone1!=null){
            sets.add(phone1);
        }
        String homePhone1 = redisCommonDao.getAppntPhone(homePhone);
        if (homePhone1!=null){
            sets.add(homePhone1);
        }
        String companyPhone1 = redisCommonDao.getAppntPhone(companyPhone);
        if (companyPhone1!=null){
            sets.add(companyPhone1);
        }
        String mobile1 = redisCommonDao.getAppntPhone(mobile);
        if (mobile1!=null){
            sets.add(mobile1);
        }
        if (sets!=null && sets.size()>0){
            for (String str : sets) {
                if (str.contains(",")){
                    String[] customnoStr = str.split(",");
                    for (int i = 0; i < customnoStr.length; i++) {
                        customnoSet.add(customnoStr[i]);
                    }
                }else{
                    customnoSet.add(str);
                }
            }
        }else{
            logger.debug("缓存中不存在此查重数据");
            //tradeInfo.addError("缓存中不存在此查重数据");
           // return;
        }

        if (customnoSet.size() >0) {
            // 可疑电话
            applicantInfo.setQuestionTel(true);
            // 投保人不同电话相同
            applicantInfo.setAppntPhoneDiff(true);
            applicantInfo.setAppntAgentDiff(true);
        }
        if (customnoSet.size() >=2) {
            // 3个（含3）以上不同投保人存在相同电话
            applicantInfo.setMoreAppntTelDiff(true);

            // 3个（含3）以上客户联系电话和首选回访电话中有相同号码
            applicantInfo.setMoreAppntTelSame(true);
            // 投保人不同联系电话相同
            applicantInfo.setAppntTelDiff(true);
        }
        if (customnoSet==null ||customnoSet.size()==0){
            return;
        }
        // 投保人非代理人但联系电话相同
        //applicantInfo.setAppntAgentDiff(true);
    }

    public void getOtherData(Applicant applicantInfo) {
        // 可疑电话
        applicantInfo.setQuestionTel(false);
        // 投保人不同电话相同
        applicantInfo.setAppntPhoneDiff(false);
        // 3个（含3）以上不同投保人存在相同电话
        applicantInfo.setMoreAppntTelDiff(false);
        // 3个（含3）以上客户联系电话和首选回访电话中有相同号码
        applicantInfo.setMoreAppntTelSame(false);
        // 投保人不同联系电话相同
        applicantInfo.setAppntTelDiff(false);
        // 投保人非代理人但联系电话相同
        applicantInfo.setAppntAgentDiff(false);
    }

}
