package com.sinosoft.cloud.cbs.trules.bom;

import java.util.Date;

/**
 * 受益人信息
 *
 */
public class TBeneficiaryInfo {
	/**
	 * 姓名
	 */
	private String beneficiaryName;
	/**
	 * 证件类型
	 */
	private String beneficiaryCerttype;
	/**
	 * 证件号
	 */
	private String beneficiaryCertno;
	/**
	 * 与被保险人关系
	 */
	private String relaToInsurd;
	/**
	 * 电话
	 */
	private String contactTel;
	/**
	 * 受益顺序
	 */
	private String beneficiarySn;
	/**
	 * 受益份额
	 */
	private double beneficiaryPropor;
	/**
	 * 地址
	 */
	private String contactAddr;
	
	/**
	 * 受益类型
	 */
	private String beneficiaryType;
	
	
	/**
	 * 国籍
	 */
	private String nationality;
	
	/**
	 * 证件有效期
	 */
	private String idValidDate;
	/**
	 * 性别
	 */
	private String bnfSex;
	
	/**
	 * 
	 * 出生日期
	 */
	private String bnfBirthday;

	/**
	 * 受益人的证件号与黑名单相同
	 */
	private boolean bnfBlackIdFlag;

	/**
	 * 受益人的全名与黑名单相同且生日相同
	 */
	private boolean bnfBlackFullFlag;

	/**
	 * 非中国籍受益人姓名与黑名单相同且生日相同（人名为1部分）
	 */
	private boolean bnfBlackFlag1;

	/**
	 * 非中国籍受益人姓名与黑名单相同且生日相同（人名为2部分）
	 */
	private boolean bnfBlackFlag2;

	/**
	 * 非中国籍受益人姓名与黑名单两个或以上名字相同（人名为2部分）
	 */
	private boolean bnfBlackFlag3;

	/**
	 * 非中国籍受益人姓名与黑名单相同且生日相同（人名大于3部分）
	 */
	private boolean bnfBlackFlag4;

	/**
	 * 非中国籍受益人姓名与黑名单两个或以上名字相同（人名大于3部分）
	 */
	private boolean bnfBlackFlag5;


	public String getBeneficiaryName() {
		return beneficiaryName;
	}

	public void setBeneficiaryName(String beneficiaryName) {
		this.beneficiaryName = beneficiaryName;
	}

	public String getBeneficiaryCerttype() {
		return beneficiaryCerttype;
	}

	public void setBeneficiaryCerttype(String beneficiaryCerttype) {
		this.beneficiaryCerttype = beneficiaryCerttype;
	}

	public String getBeneficiaryCertno() {
		return beneficiaryCertno;
	}

	public void setBeneficiaryCertno(String beneficiaryCertno) {
		this.beneficiaryCertno = beneficiaryCertno;
	}

	public String getRelaToInsurd() {
		return relaToInsurd;
	}

	public void setRelaToInsurd(String relaToInsurd) {
		this.relaToInsurd = relaToInsurd;
	}

	public String getContactTel() {
		return contactTel;
	}

	public void setContactTel(String contactTel) {
		this.contactTel = contactTel;
	}

	public String getBeneficiarySn() {
		return beneficiarySn;
	}

	public void setBeneficiarySn(String beneficiarySn) {
		this.beneficiarySn = beneficiarySn;
	}

	public double getBeneficiaryPropor() {
		return beneficiaryPropor;
	}

	public void setBeneficiaryPropor(double beneficiaryPropor) {
		this.beneficiaryPropor = beneficiaryPropor;
	}

	public String getContactAddr() {
		return contactAddr;
	}

	public void setContactAddr(String contactAddr) {
		this.contactAddr = contactAddr;
	}

	public String getBeneficiaryType() {
		return beneficiaryType;
	}

	public void setBeneficiaryType(String beneficiaryType) {
		this.beneficiaryType = beneficiaryType;
	}

	public String getNationality() {
		return nationality;
	}

	public void setNationality(String nationality) {
		this.nationality = nationality;
	}

	public String getIdValidDate() {
		return idValidDate;
	}

	public void setIdValidDate(String idValidDate) {
		this.idValidDate = idValidDate;
	}

	public String getBnfSex() {
		return bnfSex;
	}

	public void setBnfSex(String bnfSex) {
		this.bnfSex = bnfSex;
	}

	public String getBnfBirthday() {
		return bnfBirthday;
	}

	public void setBnfBirthday(String bnfBirthday) {
		this.bnfBirthday = bnfBirthday;
	}

	public boolean isBnfBlackIdFlag() {return bnfBlackIdFlag;}

	public void setBnfBlackIdFlag(boolean bnfBlackIdFlag) {this.bnfBlackIdFlag = bnfBlackIdFlag;}

	public boolean isBnfBlackFullFlag() {return bnfBlackFullFlag;}

	public void setBnfBlackFullFlag(boolean bnfBlackFullFlag) {this.bnfBlackFullFlag = bnfBlackFullFlag;}

	public boolean isBnfBlackFlag1() {return bnfBlackFlag1;}

	public void setBnfBlackFlag1(boolean bnfBlackFlag1) {this.bnfBlackFlag1 = bnfBlackFlag1;}

	public boolean isBnfBlackFlag2() {return bnfBlackFlag2;}

	public void setBnfBlackFlag2(boolean bnfBlackFlag2) {this.bnfBlackFlag2 = bnfBlackFlag2;}

	public boolean isBnfBlackFlag3() {return bnfBlackFlag3;}

	public void setBnfBlackFlag3(boolean bnfBlackFlag3) {this.bnfBlackFlag3 = bnfBlackFlag3;}

	public boolean isBnfBlackFlag4() {return bnfBlackFlag4;}

	public void setBnfBlackFlag4(boolean bnfBlackFlag4) {this.bnfBlackFlag4 = bnfBlackFlag4;}

	public boolean isBnfBlackFlag5() {return bnfBlackFlag5;}

	public void setBnfBlackFlag5(boolean bnfBlackFlag5) {this.bnfBlackFlag5 = bnfBlackFlag5;}
}
