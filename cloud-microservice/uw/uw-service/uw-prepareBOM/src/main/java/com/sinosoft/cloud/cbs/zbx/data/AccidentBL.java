package com.sinosoft.cloud.cbs.zbx.data;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.sinosoft.cloud.bl.AbstractBL;
import com.sinosoft.cloud.cbs.zbx.bom.AccidentRequest;
import com.sinosoft.cloud.cbs.zbx.jsonclient.JsonRequestClient;
import com.sinosoft.cloud.cbs.zbx.util.CardTypeEnum;
import com.sinosoft.cloud.nb.service.redis.NBRedisCommon;
import com.sinosoft.cloud.rest.TradeInfo;
import com.sinosoft.lis.entity.*;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by zkr on 2019/3/28.
 */
@Component
public class AccidentBL extends AbstractBL {
    private final Log logger = LogFactory.getLog(this.getClass());

    @Autowired
    NBRedisCommon nbRedisCommon;
    @Autowired
    SendSaveMsg sendSaveMsg;

    /** 获取中保信地址*/
    @Value("${cloud.uw.zbx.url}")
    private String url;
    /** 获取中保信版本号*/
    @Value("${cloud.uw.zbx.version}")
    private String version;
    /**获取超时时间*/
    @Value("${cloud.uw.zbx.time}")
    private  int time;



    @Override
    public TradeInfo dealData(TradeInfo tradeInfo) {

        LCContPojo lcContPojo=(LCContPojo)tradeInfo.getData(LCContPojo.class.getName());
        List<LCPolPojo> lcPolPojos=(List<LCPolPojo>)tradeInfo.getData(LCPolPojo.class.getName());
        List<LCInsuredPojo> lcInsuredPojos =(List<LCInsuredPojo>)tradeInfo.getData(LCInsuredPojo.class.getName());
         //交易流水号
        GlobalPojo globalPojo = (GlobalPojo) tradeInfo.getData(GlobalPojo.class.getName());

        boolean yFlag=false;
        String yRiskcode="";
        boolean hFlag=false;
        String  hRiskcode="";
        int num=0;
        JsonRequestClient jsonRequestClient=new JsonRequestClient();
        //险类标记
        for (int i=0;i<lcPolPojos.size();i++) {
            List<LMCalModePojo> lmCalModePojosY = nbRedisCommon.getLmCalMode(lcPolPojos.get(i).getRiskCode(), "Y");
            if (lmCalModePojosY != null && lmCalModePojosY.size() > 0) {
                yFlag = true;
                yRiskcode=lcPolPojos.get(i).getRiskCode();
                continue;
            }
            List<LMCalModePojo> lmCalModePojosH = nbRedisCommon.getLmCalMode(lcPolPojos.get(i).getRiskCode(), "H");
            if (lmCalModePojosH != null && lmCalModePojosH.size() > 0) {
                hFlag = true;
                hRiskcode=lcPolPojos.get(i).getRiskCode();
                continue;
            }
        }
        if (yFlag || hFlag){
           List<LCCIITCAccidentPojo> lcciitcAccidentPojos=new ArrayList<>();
           List<LCCIITCMajorDiseaseCheckPojo> lcciitcMajorDiseaseCheckPojos=new ArrayList<>();
           tradeInfo.addData(LCCIITCAccidentPojo.class.getName(),lcciitcAccidentPojos);
           tradeInfo.addData(LCCIITCMajorDiseaseCheckPojo.class.getName(),lcciitcMajorDiseaseCheckPojos);
        }

        for (int i=0;i<lcInsuredPojos.size();i++){
            String reqAccidentJson="";
            String reqMajCheckJson="";
            AccidentRequest accidentRequest=null;
            //意外险
            if (yFlag){
                accidentRequest=getAccident(lcContPojo,lcInsuredPojos.get(i),yRiskcode);
                String requeatJson=JSON.toJSONString(accidentRequest);

                long beginTime = System.currentTimeMillis();
                logger.info("调用中保信接口意外险请求报文," + globalPojo.getSerialNo() + "：requestJSON=" + requeatJson.toString());
                logger.info("保单号：" + lcContPojo.getContNo() + "，调用中保信意外险接口开始");
                reqAccidentJson=jsonRequestClient.doPost(requeatJson,url+"/accident/"+version,time);
                logger.info("保单号：" + lcContPojo.getContNo() +"，调用中保信意外险接口结束,用时：" + (System.currentTimeMillis() - beginTime) / 1000.0 + "s");
                logger.info("调用中保信接口意外险返回报文," + globalPojo.getSerialNo() + "：responseJson=" + reqAccidentJson.toString());
                num++;
            }

            //重疾险
            if (hFlag){
                accidentRequest=getAccident(lcContPojo,lcInsuredPojos.get(i),hRiskcode);
                String requeatJson=JSON.toJSONString(accidentRequest);

                long beginTime = System.currentTimeMillis();
                logger.info("调用中保信接口重疾险请求报文," + globalPojo.getSerialNo() + "：requestJSON=" + requeatJson.toString());
                logger.info("保单号：" + lcContPojo.getContNo() + "，调用中保信重疾险接口开始");
                reqMajCheckJson=jsonRequestClient.doPost(requeatJson,url+"/majorDiseaseCheck/"+version,time);
                logger.info("保单号：" + lcContPojo.getContNo() +"，调用中保信重疾险接口结束,用时：" + (System.currentTimeMillis() - beginTime) / 1000.0 + "s");
                logger.info("调用中保信接口重疾险返回报文," + globalPojo.getSerialNo() + "：responseJson=" + reqMajCheckJson.toString());
                num++;
            }

            if (yFlag || hFlag) {
                tradeInfo=getResult(tradeInfo, reqAccidentJson, reqMajCheckJson, lcInsuredPojos.get(i),hRiskcode,yRiskcode,num);//将返回报文处理成落库数据
            }
        }

        if (yFlag || hFlag) {
            tradeInfo=sendSaveMsg.submitData(tradeInfo);
            if (tradeInfo.hasError()){
                //tradeInfo.addError("中保信返回数据落库失败！");
                logger.error("中保信返回数据落库失败！"+tradeInfo);
            }
        }

        return tradeInfo;
    }


    /**
     * 组装Json报文体
     */
    private AccidentRequest getAccident(LCContPojo lcContPojo, LCInsuredPojo lcInsuredPojo,String riskcode){
        AccidentRequest accidentRequest =new AccidentRequest();
        accidentRequest.setUserName("liuhuanling");//账号
        accidentRequest.setPolicyCode(lcContPojo.getShardingID());
        accidentRequest.setPolicyHolderName(lcInsuredPojo.getName());
        accidentRequest.setCredentialsType(CardTypeEnum.getValue(lcInsuredPojo.getIDType()));
        accidentRequest.setCredentialsCode(lcInsuredPojo.getIDNo());
        accidentRequest.setInsuranceCompanyCode("000092");//人寿公司编码
        accidentRequest.setProductCode(riskcode);
        accidentRequest.setCustomerAllowed("Y");//已经被授权
       // accidentRequest.setInsurerUuid("00000092"+ UUID.randomUUID());
        return accidentRequest;
    }

    /**
     * 将调用中保信的结果进行处理
     */
    private TradeInfo getResult(TradeInfo tradeInfo,String respAccJson,String respMajJson ,
                                LCInsuredPojo lcInsuredPojo,String hRiskCode,String yRiskCode,int num){
      List<LCCIITCCheckPojo> lcciitcCheckPojos=( List<LCCIITCCheckPojo>) tradeInfo.getData(LCCIITCCheckPojo.class.getName());

      LCAppntPojo lcAppntPojo=(LCAppntPojo)tradeInfo.getData(LCAppntPojo.class.getName());
      LCContPojo lcContPojo=(LCContPojo)tradeInfo.getData(LCContPojo.class.getName());
      boolean accFlag=false;
      boolean majFlag=false;
      JSONObject accJson=null;
      JSONObject majJson=null;

        if (respAccJson !=null &&  !"".equals(respAccJson) && respAccJson.length()!=3){
            accFlag=true;
            accJson=JSON.parseObject(respAccJson);
        }
        if (respMajJson !=null &&  !"".equals(respMajJson) && respMajJson.length()!=3){
            majFlag=true;
            majJson=JSON.parseObject(respMajJson);
        }

      if (lcciitcCheckPojos ==null || lcciitcCheckPojos.size()==0){
          List<LCCIITCCheckPojo> lcciitcCheckPojoList=new ArrayList<>();
          tradeInfo.addData(LCCIITCCheckPojo.class.getName(),lcciitcCheckPojoList);
          //效验总表
          LCCIITCCheckPojo lcciitcCheckPojo=new LCCIITCCheckPojo();
          lcciitcCheckPojo.setSerialNo(PubFun1.CreateMaxNo("ciitccheck", 20));
          lcciitcCheckPojo.setPrtNo(lcContPojo.getShardingID());
          lcciitcCheckPojo.setAppntName(lcAppntPojo.getAppntName());
          lcciitcCheckPojo.setAppntNo(lcAppntPojo.getAppntNo());
          lcciitcCheckPojo.setAccidentFlag(accFlag?"1":"0");
          lcciitcCheckPojo.setAccidentProductCode(yRiskCode);
          lcciitcCheckPojo.setMDFlag(majFlag?"1":"0");
          lcciitcCheckPojo.setMDProductCode(hRiskCode);
          lcciitcCheckPojo.setCheckNum(num);
          lcciitcCheckPojo.setMakeDate(PubFun.getCurrentDate());
          lcciitcCheckPojo.setMakeTime(PubFun.getCurrentTime());
          lcciitcCheckPojo.setManageCom(lcContPojo.getManageCom());
          lcciitcCheckPojo.setModifyDate(PubFun.getCurrentDate());
          lcciitcCheckPojo.setModifyTime(PubFun.getCurrentTime());
          lcciitcCheckPojo.setOperator("ABC-Cloud");
          ((List<LCCIITCCheckPojo>) tradeInfo.getData(LCCIITCCheckPojo.class.getName())).add(lcciitcCheckPojo);
      }else {
          lcciitcCheckPojos.get(0).setCheckNum(num);
      }


         //意外险
        if (accFlag){
            List<LCCIITCAccidentPojo> lcciitcAccidentPojoList=( List<LCCIITCAccidentPojo>) tradeInfo.getData(LCCIITCAccidentPojo.class.getName());
            LCCIITCAccidentPojo lcciitcAccidentPojo=new LCCIITCAccidentPojo();
            lcciitcAccidentPojo.setPrtNo(lcContPojo.getShardingID());
            lcciitcAccidentPojo.setCheckNum(num);
            lcciitcAccidentPojo.setHttpCode("200");
            lcciitcAccidentPojo.setRetCode(accJson.get("retCode")+"");
            lcciitcAccidentPojo.setInsuredName(lcInsuredPojo.getName());
            lcciitcAccidentPojo.setInsuredNo(lcInsuredPojo.getInsuredNo());
            lcciitcAccidentPojo.setInsuredIDType(lcInsuredPojo.getIDType());
            lcciitcAccidentPojo.setInsuredIDNO(lcInsuredPojo.getIDNo());
            lcciitcAccidentPojo.setProductCode(yRiskCode);
            lcciitcAccidentPojo.setMakeDate(PubFun.getCurrentDate());
            lcciitcAccidentPojo.setMakeTime(PubFun.getCurrentTime());
            lcciitcAccidentPojo.setModifyDate(PubFun.getCurrentDate());
            lcciitcAccidentPojo.setModifyTime(PubFun.getCurrentTime());
            lcciitcAccidentPojo.setOperator("ABC-Cloud");
            if (accJson.get("data") !=null && !"".equals(accJson.get("data").toString())){
                JSONObject object=JSON.parseObject(accJson.get("data").toString());
                lcciitcAccidentPojo.setMultiCompany(object.get("multiCompany").toString().substring(0,1));
                lcciitcAccidentPojo.setMajorDiseasePayment(object.get("majorDiseasePayment").toString().substring(0,1));
                lcciitcAccidentPojo.setDisability(object.get("disability").toString().substring(0,1));
                lcciitcAccidentPojo.setDense(object.get("dense").toString().substring(0,1));
                lcciitcAccidentPojo.setAccumulativeMoney(object.get("accumulativeMoney").toString().substring(0,1));
                lcciitcAccidentPojo.setPageQueryCode(object.get("pageQueryCode")+"");
                lcciitcAccidentPojo.setDisplayPage(object.get("displayPage").toString().substring(0,1));
                lcciitcAccidentPojo.setTagDate(object.get("tagDate")+"");
            }
            lcciitcAccidentPojoList.add(lcciitcAccidentPojo);
            tradeInfo.addData(LCCIITCAccidentPojo.class.getName(),lcciitcAccidentPojoList);
        }
        //重疾险
        if (majFlag){
            List<LCCIITCMajorDiseaseCheckPojo> lcciitcMajorDiseaseCheckPojoList =( List<LCCIITCMajorDiseaseCheckPojo>) tradeInfo.getData(LCCIITCMajorDiseaseCheckPojo.class.getName());
            LCCIITCMajorDiseaseCheckPojo lcciitcMajorDiseaseCheckPojo=new LCCIITCMajorDiseaseCheckPojo();
            lcciitcMajorDiseaseCheckPojo.setPrtNo(lcContPojo.getShardingID());
            lcciitcMajorDiseaseCheckPojo.setCheckNum(num);
            lcciitcMajorDiseaseCheckPojo.setHttpCode("200");
            lcciitcMajorDiseaseCheckPojo.setRetCode(majJson.get("retCode")+"");
            lcciitcMajorDiseaseCheckPojo.setInsuredName(lcInsuredPojo.getName());
            lcciitcMajorDiseaseCheckPojo.setInsuredNo(lcInsuredPojo.getInsuredNo());
            lcciitcMajorDiseaseCheckPojo.setInsuredIDType(lcInsuredPojo.getIDType());
            lcciitcMajorDiseaseCheckPojo.setInsuredIDNO(lcInsuredPojo.getIDNo());
            lcciitcMajorDiseaseCheckPojo.setProductCode(hRiskCode);
            lcciitcMajorDiseaseCheckPojo.setMakeDate(PubFun.getCurrentDate());
            lcciitcMajorDiseaseCheckPojo.setMakeTime(PubFun.getCurrentTime());
            lcciitcMajorDiseaseCheckPojo.setModifyDate(PubFun.getCurrentDate());
            lcciitcMajorDiseaseCheckPojo.setModifyTime(PubFun.getCurrentTime());
            lcciitcMajorDiseaseCheckPojo.setOperator("ABC-Cloud");
            if (majJson.get("data") !=null && !"".equals(majJson.get("data").toString())){
                JSONObject object=JSON.parseObject(majJson.get("data").toString());
                lcciitcMajorDiseaseCheckPojo.setAbnormalCheck(object.get("abnormalCheck").toString().substring(0,1));
                lcciitcMajorDiseaseCheckPojo.setAbnormalPayment(object.get("abnormalPayment").toString().substring(0,1));
                lcciitcMajorDiseaseCheckPojo.setChronicDiseasePayment(object.get("chronicDiseasePayment").toString().substring(0,1));
                lcciitcMajorDiseaseCheckPojo.setDense(object.get("dense").toString().substring(0,1));
                lcciitcMajorDiseaseCheckPojo.setDisplayPage(object.get("displayPage").toString().substring(0,1));
                lcciitcMajorDiseaseCheckPojo.setMajorDiseaseMoney(object.get("majorDiseaseMoney").toString().substring(0,1));
                lcciitcMajorDiseaseCheckPojo.setMajorDiseasePayment(object.get("majorDiseasePayment").toString().substring(0,1));
                lcciitcMajorDiseaseCheckPojo.setPageQueryCode(object.get("pageQueryCode")+"");
                lcciitcMajorDiseaseCheckPojo.setTagDate(object.get("tagDate")+"");
                lcciitcMajorDiseaseCheckPojo.setMultiCompany(object.get("multiCompany").toString().substring(0,1));
            }
            lcciitcMajorDiseaseCheckPojoList.add(lcciitcMajorDiseaseCheckPojo);
            tradeInfo.addData(LCCIITCMajorDiseaseCheckPojo.class.getName(),lcciitcMajorDiseaseCheckPojoList);
        }
        return tradeInfo;
    }


}
