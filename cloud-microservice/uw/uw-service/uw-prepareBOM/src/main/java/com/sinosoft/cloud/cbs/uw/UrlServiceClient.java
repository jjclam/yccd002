package com.sinosoft.cloud.cbs.uw;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

public class UrlServiceClient {


	private static String mMessages = null;
	/**
	 *
	 * @param path
	 * @param val
	 * @return
	 * @throws Exception
	 */
	public static boolean ConnterServletUrl(String path, String val){
		HttpURLConnection conn = null;
		InputStream inStream = null;
		StringBuffer buffer = new StringBuffer("");
		BufferedReader reader=null;
		try{
			URL url = new URL(path);
			conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod("POST");// 提交模式
			conn.setRequestProperty("content-type", "application/json;charset=utf-8");
			//conn.setRequestProperty("charset", "utf-8");
			conn.setRequestProperty("accept-encoding", "gzip");
			conn.setRequestProperty("accept-language", "zh-CN,zh;q=0.8");
			conn.setRequestProperty("connection", "Keep-Alive");
//			conn.setConnectTimeout(1000);// 连接超时 单位毫秒
//			conn.setReadTimeout(5000);// 读取超时 单位毫秒
			conn.setDoOutput(true);// 是否输入参数
			// 通过conn.getOutputStream().write 将json信息写入，在另一端系统，get出来再解析
			conn.getOutputStream().write(URLEncoder.encode(val, "UTF-8").getBytes());

			System.out.println("请求地址:"+conn.getURL());
			inStream = conn.getInputStream();
			reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
			String line;

			while ((line = reader.readLine()) != null) {
				buffer.append(line);
			}

			mMessages = buffer.toString();
		}catch(Exception e){
			e.printStackTrace();
			return false;
		}finally {
			try {


				if(null!=inStream){
					inStream.close();
				}

				if(null!=conn){
					conn.disconnect();
				}


			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

		return true;
	}


	public static String getRestl(){
		return mMessages;
	}

}
