package com.sinosoft.cloud.cbs.rules.bom;

import java.util.Date;

/**
 * 银保通交易信息
 * 
 * @author dingfan
 * 
 */
public class BankSourceInfo{
	/**
	 * 投保单号
	 */
	private String policyNo;

	/**
	 * 成功总金额
	 */
	private double balAccAmount;

	/**
	 * 明细合计笔数
	 */
	private int balAccCount;

	/**
	 * 对账日期
	 */
	private Date balAccDate;

	/**
	 * 成功总笔数
	 */
	private int balAccScale;

	/**
	 * 明细交易金额合计
	 */
	private double balAccSum;

	/**
	 * 银行代码
	 */
	private String bankCode;

	/**
	 * 分行代码
	 */
	private String branchBankCode;

	/**
	 * 交易金额
	 */
	private double dealAmount;

	/**
	 * 交易日期
	 */
	private Date dealDate;

	/**
	 * 交易流水号码
	 */
	private String dealSerialNum;

	/**
	 * 交易时间
	 */
	private Date dealTime;

	/**
	 * 交易类型
	 */
	private String dealType;

	/**
	 * 保险公司代码
	 */
	private String inusuranceCode;

	/**
	 * 网点代码
	 */
	private String networkCode;

	/**
	 * 保单印刷号
	 */
	private String policyPrintNo;

	/**
	 * 发票印刷号
	 */
	private String receiptPrintNo;

	/**
	 * 柜员编号
	 */
	private String tellerNo;

	public BankSourceInfo() {
		super();
	}

	public String getPolicyNo() {
		return policyNo;
	}

	public void setPolicyNo(String policyNo) {
		this.policyNo = policyNo;
	}

	public double getBalAccAmount() {
		return balAccAmount;
	}

	public void setBalAccAmount(double balAccAmount) {
		this.balAccAmount = balAccAmount;
	}

	public int getBalAccCount() {
		return balAccCount;
	}

	public void setBalAccCount(int balAccCount) {
		this.balAccCount = balAccCount;
	}

	public Date getBalAccDate() {
		return balAccDate;
	}

	public void setBalAccDate(Date balAccDate) {
		this.balAccDate = balAccDate;
	}

	public int getBalAccScale() {
		return balAccScale;
	}

	public void setBalAccScale(int balAccScale) {
		this.balAccScale = balAccScale;
	}

	public double getBalAccSum() {
		return balAccSum;
	}

	public void setBalAccSum(double balAccSum) {
		this.balAccSum = balAccSum;
	}

	public String getBankCode() {
		return bankCode;
	}

	public void setBankCode(String bankCode) {
		this.bankCode = bankCode;
	}

	public String getBranchBankCode() {
		return branchBankCode;
	}

	public void setBranchBankCode(String branchBankCode) {
		this.branchBankCode = branchBankCode;
	}

	public double getDealAmount() {
		return dealAmount;
	}

	public void setDealAmount(double dealAmount) {
		this.dealAmount = dealAmount;
	}

	public Date getDealDate() {
		return dealDate;
	}

	public void setDealDate(Date dealDate) {
		this.dealDate = dealDate;
	}

	public String getDealSerialNum() {
		return dealSerialNum;
	}

	public void setDealSerialNum(String dealSerialNum) {
		this.dealSerialNum = dealSerialNum;
	}

	public Date getDealTime() {
		return dealTime;
	}

	public void setDealTime(Date dealTime) {
		this.dealTime = dealTime;
	}

	public String getDealType() {
		return dealType;
	}

	public void setDealType(String dealType) {
		this.dealType = dealType;
	}

	public String getInusuranceCode() {
		return inusuranceCode;
	}

	public void setInusuranceCode(String inusuranceCode) {
		this.inusuranceCode = inusuranceCode;
	}

	public String getNetworkCode() {
		return networkCode;
	}

	public void setNetworkCode(String networkCode) {
		this.networkCode = networkCode;
	}

	public String getPolicyPrintNo() {
		return policyPrintNo;
	}

	public void setPolicyPrintNo(String policyPrintNo) {
		this.policyPrintNo = policyPrintNo;
	}

	public String getReceiptPrintNo() {
		return receiptPrintNo;
	}

	public void setReceiptPrintNo(String receiptPrintNo) {
		this.receiptPrintNo = receiptPrintNo;
	}

	public String getTellerNo() {
		return tellerNo;
	}

	public void setTellerNo(String tellerNo) {
		this.tellerNo = tellerNo;
	}
}
