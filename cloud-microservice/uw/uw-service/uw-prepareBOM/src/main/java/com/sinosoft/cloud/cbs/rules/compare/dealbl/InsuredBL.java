package com.sinosoft.cloud.cbs.rules.compare.dealbl;

import com.sinosoft.cloud.cbs.rules.bom.HistoryRisk;
import com.sinosoft.cloud.cbs.rules.bom.Insured;
import com.sinosoft.cloud.cbs.rules.compare.beantoxml.CommonToXml;
import com.sinosoft.cloud.cbs.rules.compare.xmltomap.XmlToMap;
import com.sinosoft.lis.entity.LCContPojo;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Component;

import java.lang.reflect.AccessibleObject;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static javafx.scene.input.KeyCode.H;


/**
 * 判断两个xml中insuredList节点下的数据是否一致.
 */
@Component
public class InsuredBL {
    private final Log logger = LogFactory.getLog(InsuredBL.class);

    /**
     * 处理方法.
     *
     * @param oldInsureds 数据.
     * @param newInsureds 数据.
     * @return
     */
    public boolean xmlTwoIsSame(List<Insured> oldInsureds, List<Insured> newInsureds) {
        logger.debug("比对老核心得到的被保人既往数据和累计风险保额所得到的被保人既往数据开始");
        boolean flag = true;
    /*    Map<String, String> oldAndNoListMap = null;
        Map<Integer, Map<String, String>> oldAndHaveHistory = null;
        Map<Integer, Map<String, String>> oldAndExtendMap = null;

        Map<String, String> newAndNoListMap = null;
        Map<Integer, Map<String, String>> newAndHaveHistory = null;
        Map<Integer, Map<String, String>> newAndExtendMap = null;
        final String oldXml = CommonToXml.getXml("Insured", oldInsureds);
        final String newXml = CommonToXml.getXml("Insured", newInsureds);

        String countOldHaveHistory = "0";
        String countOldExtendMap = "0";
        for (Insured insured : oldInsureds) {
            if (insured.getHistoryRiskList() != null) {
                countOldHaveHistory = "1";
            } else if (insured.getExtendMap() != null) {
                countOldExtendMap = "1";
            }
        }
        if ("1".equals(countOldHaveHistory)) {
            final String xmlHaveHistory = oldXml.substring(oldXml.lastIndexOf("<historyRiskList>"), oldXml.lastIndexOf("</historyRiskList>") + 18);
            oldAndHaveHistory = XmlToMap.haveListResult(xmlHaveHistory);
        } else if ("1".equals(countOldExtendMap)) {
            final String xmlHaveExtendMap = oldXml.substring(oldXml.lastIndexOf("<extendMap>"), oldXml.lastIndexOf("</extendMap>") + 12);
            oldAndExtendMap = XmlToMap.haveListResult(xmlHaveExtendMap);
        }

        if ("1".equals(countOldHaveHistory) && "1".equals(countOldExtendMap)) {
            String replace = newXml.replace(oldXml.substring(newXml.lastIndexOf("<historyRiskList>"), newXml.lastIndexOf("</historyRiskList>") + 18), "");
            replace = replace.replace(replace.substring(replace.lastIndexOf("<extendMap>"), replace.lastIndexOf("</extendMap>") + 12), "");
            oldAndNoListMap = XmlToMap.noList(replace);
        } else if ("1".equals(countOldHaveHistory)) {
            final String replace = oldXml.replace(oldXml.substring(oldXml.lastIndexOf("<historyRiskList>"), oldXml.lastIndexOf("</historyRiskList>") + 18), "");
            oldAndNoListMap = XmlToMap.noList(replace);
        } else if ("1".equals(countOldExtendMap)) {
            final String replace = oldXml.replace(oldXml.substring(oldXml.lastIndexOf("<extendMap>"), oldXml.lastIndexOf("</extendMap>") + 12), "");
            oldAndNoListMap = XmlToMap.noList(replace);
        } else {
            oldAndNoListMap = XmlToMap.noList(oldXml);
        }


        String countNewHaveHistory = "0";
        String countNewExtendMap = "1";
        for (int i = 0; i < newInsureds.size(); i++) {
            if (newInsureds.get(i).getHistoryRiskList() != null) {
                countNewHaveHistory = "1";
            } else if (newInsureds.get(i).getExtendMap() != null) {
                countNewExtendMap = "1";
            }
        }

        if ("1".equals(countNewHaveHistory)) {
            final String xmlHaveHistory = oldXml.substring(oldXml.lastIndexOf("<historyRiskList>"), oldXml.lastIndexOf("</historyRiskList>") + 18);
            newAndHaveHistory = XmlToMap.haveListResult(xmlHaveHistory);
        } else if ("1".equals(countNewExtendMap)) {
            final String xmlHaveExtendMap = oldXml.substring(oldXml.lastIndexOf("<extendMap>"), oldXml.lastIndexOf("</extendMap>") + 12);
            newAndExtendMap = XmlToMap.haveListResult(xmlHaveExtendMap);
        }


        if ("1".equals(countNewHaveHistory) && "1".equals(countNewExtendMap)) {
            String replace = oldXml.replace(oldXml.substring(oldXml.lastIndexOf("<historyRiskList>"), oldXml.lastIndexOf("</historyRiskList>") + 18), "");
            replace = replace.replace(replace.substring(replace.lastIndexOf("<extendMap>"), replace.lastIndexOf("</extendMap>") + 12), "");
            newAndNoListMap = XmlToMap.noList(replace);
        } else if ("1".equals(countNewHaveHistory)) {
            final String replace = oldXml.replace(oldXml.substring(oldXml.lastIndexOf("<historyRiskList>"), oldXml.lastIndexOf("</historyRiskList>") + 18), "");
            newAndNoListMap = XmlToMap.noList(replace);
        } else if ("1".equals(countNewExtendMap)) {
            final String replace = oldXml.replace(oldXml.substring(oldXml.lastIndexOf("<extendMap>"), oldXml.lastIndexOf("</extendMap>") + 12), "");
            newAndNoListMap = XmlToMap.noList(replace);
        } else {
            newAndNoListMap = XmlToMap.noList(newXml);
        }

       // logger.debug("比对老核心和累计风险保额无历史险种列表开始");
        String isPass = "yes";
        //判断历史险种列表为空  两个集合中的数据是否一样
        if (oldAndNoListMap != null && newAndNoListMap != null) {
            if (oldAndNoListMap.size() == newAndNoListMap.size()) {
                for (String key : oldAndNoListMap.keySet()) {
                    final String value = oldAndNoListMap.get(key);
                    if (!value.equals(newAndNoListMap.get(key))) {
                        isPass = "no";
                        logger.debug("不一样的字段是：" + key + ",老核心所获取到的值是：" + value + ",累计风险保额所获取的值是：" + newAndNoListMap.get(key));
                    }
                }
            } else {
                isPass = "no";
                logger.debug("老核心得到的数据和累计风险保额所得到的数据不一致");
            }
        } else {
            logger.debug("老核心和累计风险保额中的数据为空，无须比较");
        }

//        if ("yes".equals(isPass)) {
//            logger.debug("比对老核心和累计风险保额无历史险种列表结束，数剧一致");
//            flag = true;
//        } else {
//            logger.debug("比对老核心和累计风险保额无历史险种列表结束，数据不一致");
//            return false;
//        }
//        logger.debug("比对老核心和累计风险保额无历史险种列表开始");
//        if ("0".equals(haveHaveHistory(oldAndExtendMap, newAndExtendMap))) {
//            flag = true;
//        } else {
//            logger.debug("比对老核心和累计风险保额有集合的数据结束，数据不一致");
//            return false;
//        }
//
//        if ("0".equals(haveHaveHistory(oldAndHaveHistory, newAndHaveHistory))) {
//            flag = true;
//        } else {
//            logger.debug("比对老核心和累计风险保额有集合的数据结束，数据不一致");
//            return false;
//        }

        if ("yes".equals(isPass) && "0".equals(haveHaveHistory(oldAndHaveHistory, newAndHaveHistory)) && "0".equals(haveHaveHistory(oldAndExtendMap, newAndExtendMap))) {
            flag = true;
        }*/

        if (oldInsureds != null && newInsureds != null) {
            if (oldInsureds.size() == newInsureds.size()) {
                for (int i = 0; i < oldInsureds.size(); i++) {
                    if (oldInsureds.get(i).getHistoryRiskList() != null && newInsureds.get(i).getHistoryRiskList() != null) {
                        if (oldInsureds.get(i).getHistoryRiskList().size() == newInsureds.get(i).getHistoryRiskList().size()) {
                            final List oldHository = oldInsureds.get(i).getHistoryRiskList();
                            final List newHository = newInsureds.get(i).getHistoryRiskList();
                            for (int j = 0; j < oldHository.size(); j++) {
                                final HistoryRisk oldHistoryRisk = (HistoryRisk) oldHository.get(j);
                                final HistoryRisk newHistoryRisk = (HistoryRisk) newHository.get(j);
                                final Class clazz = oldHistoryRisk.getClass();
                                final Field[] fields = clazz.getDeclaredFields();
                                AccessibleObject.setAccessible(fields, true);
                                for (Field field : fields) {
                                    if (field.isAccessible()) {
                                        try {
                                            final Object oldValue = field.get(oldHistoryRisk);
                                            final Object newValue = field.get(newHistoryRisk);
                                            if (oldValue != null && newValue != null) {
                                                if (!oldValue.equals(newValue)) {
                                                    flag = false;
                                                    logger.debug("不一样的字段是：" + field.getName() + ",老核心所获取到的值是：" + oldValue + ",累计风险保额所获取的值是：" + newValue);
                                                }
                                            }
                                        } catch (IllegalAccessException e) {
                                            logger.debug("反射时异常");
                                        }
                                    }
                                }

                            }
                        }
                    }
                }
            }
        }


        if (oldInsureds != null && newInsureds != null) {
            if (oldInsureds.size() == newInsureds.size()) {
                for (int i = 0; i < oldInsureds.size(); i++) {
                    if (oldInsureds.get(i).getExtendMap() != null && newInsureds.get(i).getExtendMap() != null) {
                        if (oldInsureds.get(i).getExtendMap().size() == newInsureds.get(i).getExtendMap().size()) {
                            final Map oldExtendMap = oldInsureds.get(i).getExtendMap();
                            final Map newExtendMap = newInsureds.get(i).getExtendMap();
                            for (Object key : oldExtendMap.keySet()) {
                                final Object oldValue = oldExtendMap.get(key);
                                final Object newValue = newExtendMap.get(key);
                                if (oldValue != null && newValue != null) {
                                    if (!oldValue.equals(newValue)) {
                                        flag = false;
                                        logger.debug("不一样的字段是：" + key + ",老核心所获取到的值是：" + oldValue + ",累计风险保额所获取的值是：" + newValue);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }


        if (oldInsureds != null && newInsureds != null) {
            if (oldInsureds.size() == newInsureds.size()) {
                for (int i = 0; i < oldInsureds.size(); i++) {
                    final Insured oldInsured = oldInsureds.get(i);
                    final Insured newInsured = newInsureds.get(i);
                    final Class clazz = oldInsured.getClass();
                    final Field[] fields = clazz.getDeclaredFields();
                    AccessibleObject.setAccessible(fields, true);
                    for (Field field : fields) {
                        if (field.isAccessible()) {
                            if (!"extendMap".equals(field.getName()) && !"historyRiskList".equals(field.getName())) {
                                try {
                                    final Object oldValue = field.get(oldInsured);
                                    final Object newValue = field.get(newInsured);

                                    if (oldValue != null && newValue != null) {
                                        if (!oldValue.equals(newValue)) {
                                            flag = false;
                                            logger.debug("不一样的字段是：" + field.getName() + "老核心所获取到的值是：" + oldValue + ",累计风险保额所获取的值是：" + newValue);
                                        }
                                    } /*else {
                                        logger.debug("空的字段是：" + field.getName() + "老核心所获取到的值是：" + oldValue + ",累计风险保额所获取的值是：" + newValue);
                                    }*/
                                } catch (IllegalAccessException e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    }
                }
            }
        }

        if (flag) {
            logger.debug("比对老核心得到的被保人既往数据和累计风险保额所得到的被保人既往数据一致");
        }
        logger.debug("比对老核心得到的被保人既往数据和累计风险保额所得到的被保人既往数据结束");
        return flag;
    }



    /*//测试被保人是否正确
    public static void main(String[] args) {

        final List<Insured> insuredList = new ArrayList<Insured>();
        final List<Insured> ninsuredList = new ArrayList<Insured>();
        final List<HistoryRisk> nint = new ArrayList<HistoryRisk>();
        final List<HistoryRisk> nintbb = new ArrayList<HistoryRisk>();

        final Insured oldInsured = new Insured();
        HistoryRisk historyRisk = new HistoryRisk();
        historyRisk.setClientType("11");
        nint.add(historyRisk);
        oldInsured.setHistoryRiskList(nint);
        oldInsured.setAccPyAmount(0);
        Map map = new HashMap();
        map.put("1033风险保额", "100");
        oldInsured.setExtendMap(map);

        insuredList.add(oldInsured);


        final Insured newInsured = new Insured();


        HistoryRisk historyRisk1 = new HistoryRisk();
        historyRisk1.setClientType("22");
        nintbb.add(historyRisk1);
        newInsured.setHistoryRiskList(nintbb);
        newInsured.setAccPyAmount(0.0);
        Map map1 = new HashMap();
        map1.put("1033风险保额", "200");
        newInsured.setExtendMap(map1);
        ninsuredList.add(newInsured);
        InsuredBL insuredBL = new InsuredBL();
        insuredBL.xmlTwoIsSame(insuredList, ninsuredList);
    }*/
}