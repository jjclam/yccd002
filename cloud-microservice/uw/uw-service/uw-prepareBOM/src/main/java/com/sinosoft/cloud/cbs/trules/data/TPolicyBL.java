package com.sinosoft.cloud.cbs.trules.data;

import com.sinosoft.cloud.bl.AbstractBL;
import com.sinosoft.cloud.cbs.trules.bom.*;
import com.sinosoft.cloud.rest.TradeInfo;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Component;

/**
 * @Author:wangshuliang
 * @Description:
 * @Date:Created in 10:57 2018/6/11
 * @Modified by:
 */
@Component
public class TPolicyBL  extends AbstractBL {
    private final Log logger = LogFactory.getLog(this.getClass());
    @Override
    public boolean checkData(TradeInfo requestInfo) {

        return true;
    }
    @Override
    public TradeInfo dealData(TradeInfo tradeInfo) {
        TPolicy tPolicy = new TPolicy();
        Product product = (Product) tradeInfo.getData(Product.class.getName());
        if (product==null){
            logger.debug("产品信息为空"+tradeInfo.toString());
            tradeInfo.addError("产品信息为空");
            return tradeInfo;
        }
        tPolicy.setProduct(product);

        TAgentInfo tAgentInfo = (TAgentInfo)tradeInfo.getData(TAgentInfo.class.getName());
        if (tAgentInfo==null){
            logger.debug("代理人信息为空"+tradeInfo.toString());
            tradeInfo.addError("代理人信息为空");
            return tradeInfo;
        }
        tPolicy.setTAgentInfo(tAgentInfo);

        LcInsured lcInsured = (LcInsured)tradeInfo.getData(LcInsured.class.getName());
        if (lcInsured==null){
            logger.debug("被保人既往信息为空"+tradeInfo.toString());
            tradeInfo.addError("被保人既往信息为空");
            return tradeInfo;
        }
        tPolicy.setLcInsured(lcInsured);

        ProdGroup prodGroup = (ProdGroup) tradeInfo.getData(ProdGroup.class.getName());
        if (prodGroup==null){
            logger.debug("产品组合信息为空"+tradeInfo.toString());
            tradeInfo.addError("产品组合信息为空");
            return tradeInfo;
        }
        tPolicy.setProdGroup(prodGroup);

        TApplicantInfo tApplicantInfo =(TApplicantInfo)tradeInfo.getData(TApplicantInfo.class.getName());
        if (tApplicantInfo==null){
            logger.debug("投保人信息为空"+tradeInfo.toString());
            tradeInfo.addError("投保人信息为空");
            return tradeInfo;
        }
        tPolicy.setTApplicantInfo(tApplicantInfo);


        TBeneficiaryInfos tBeneficiaryInfos =(TBeneficiaryInfos) tradeInfo.getData(TBeneficiaryInfos.class.getName());
        if (tBeneficiaryInfos==null||tBeneficiaryInfos.gettBeneficiaryInfo()==null ||tBeneficiaryInfos.gettBeneficiaryInfo().size()<=0 ){
            logger.debug("受益人信息为空");
        }else{
            tPolicy.setTBeneficiaryInfos(tBeneficiaryInfos.gettBeneficiaryInfo());
        }

        TInsurdInfo tInsurdInfo = (TInsurdInfo)tradeInfo.getData(TInsurdInfo.class.getName());
        if (tBeneficiaryInfos==null){
            logger.debug("被保人信息为空"+tradeInfo.toString());
            tradeInfo.addError("被保人信息为空");
            return tradeInfo;
        }
        tPolicy.setTInsurdInfo(tInsurdInfo);
        TPayInfo tPayInfo = (TPayInfo)tradeInfo.getData(TPayInfo.class.getName());
        if (tPayInfo==null){
            logger.debug("收费信息为空"+tradeInfo.toString());
            tradeInfo.addError("收费信息为空");
            return tradeInfo;
        }
        tPolicy.setTPayInfo(tPayInfo);
        TProductInfo tProductInfo = (TProductInfo)tradeInfo.getData(TProductInfo.class.getName());
        if (tProductInfo == null){
            logger.debug("合同信息为空"+tradeInfo.toString());
            tradeInfo.addError("合同信息为空");
            return tradeInfo;
        }
        tPolicy.setTProductInfo(tProductInfo);

        tradeInfo.addData(TPolicy.class.getName(),tPolicy);

        logger.debug("团险累计风险保额提数完成");
        return tradeInfo;
    }
}
