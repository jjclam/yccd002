package com.sinosoft.cloud.cbs.trules.bom;

import java.util.Date;

/**
 * 被保人信息
 *
 */
public class TInsurdInfo {
	/**
	 * 被保人姓名
	 */
	private String insurdName;
	/**
	 * 被保人英文名
	 */
	private String insurdEngName;
	/**
	 * 证件类型
	 */
	private String insurdCerttype;
	/**
	 * 证件号
	 */
	private String insurdCertno;
	/**
	 * 性别
	 */
	private String insurdGender;
	/**
	 * 年龄
	 */
	private int insurdAge;
	/**
	 * 出生日期
	 */
	private String insurdBirthDate;
	/**
	 * 联系电话
	 */
	private String contactTel;
	/**
	 * 联系地址
	 */
	private String contactAddr;
	/**
	 * 联系地址（编码）
	 */
	private String contactAddrno;
	/**
	 * 职业代码
	 */
	private String occupationCode;
	/**
	 * 证件有效期
	 */
	private String insurdCertValidperiod;

	
	/**
	 * 国籍
	 */
	private String nationality;
	
	/**
	 * 职业类别
	 */
	private String occupationCat;
	
	/**
	 * 是否已有本保障计划标志
	 */
	private String safeguardFlag;
	
	/**
	 * 固定电话
	 */
	private String phone;
	
	/**
	 * 国家等级
	 */
	private String countryCat;
	
	/**
	 * 证件类型是否为长期
	 * @return
	 */
	private String longPeriod;
	
	/**
	 * 健康告知
	 */	
	private String healthNotice;

	/**
	 * 被保人的证件号与黑名单相同
	 */
	private boolean insBlackIdFlag;

	/**
	 * 被保人的全名与黑名单相同且生日相同
	 */
	private boolean insBlackFullFlag;

	/**
	 * 非中国籍被保人姓名与黑名单相同且生日相同（人名为1部分）
	 */
	private boolean insBlackFlag1;

	/**
	 * 非中国籍被保人姓名与黑名单相同且生日相同（人名为2部分）
	 */
	private boolean insBlackFlag2;

	/**
	 * 非中国籍被保人姓名与黑名单两个或以上名字相同（人名为2部分）
	 */
	private boolean insBlackFlag3;

	/**
	 * 非中国籍被保人姓名与黑名单相同且生日相同（人名大于3部分）
	 */
	private boolean insBlackFlag4;

	/**
	 * 非中国籍被保人姓名与黑名单两个或以上名字相同（人名大于3部分）
	 */
	private boolean insBlackFlag5;


	public String getInsurdName() {
		return insurdName;
	}

	public void setInsurdName(String insurdName) {
		this.insurdName = insurdName;
	}

	public String getInsurdEngName() {
		return insurdEngName;
	}

	public void setInsurdEngName(String insurdEngName) {
		this.insurdEngName = insurdEngName;
	}

	public String getInsurdCerttype() {
		return insurdCerttype;
	}

	public void setInsurdCerttype(String insurdCerttype) {
		this.insurdCerttype = insurdCerttype;
	}

	public String getInsurdCertno() {
		return insurdCertno;
	}

	public void setInsurdCertno(String insurdCertno) {
		this.insurdCertno = insurdCertno;
	}

	public String getInsurdGender() {
		return insurdGender;
	}

	public void setInsurdGender(String insurdGender) {
		this.insurdGender = insurdGender;
	}

	public int getInsurdAge() {
		return insurdAge;
	}

	public void setInsurdAge(int insurdAge) {
		this.insurdAge = insurdAge;
	}

	public String getInsurdBirthDate() {
		return insurdBirthDate;
	}

	public void setInsurdBirthDate(String insurdBirthDate) {
		this.insurdBirthDate = insurdBirthDate;
	}

	public String getContactTel() {
		return contactTel;
	}

	public void setContactTel(String contactTel) {
		this.contactTel = contactTel;
	}

	public String getContactAddr() {
		return contactAddr;
	}

	public void setContactAddr(String contactAddr) {
		this.contactAddr = contactAddr;
	}

	public String getContactAddrno() {
		return contactAddrno;
	}

	public void setContactAddrno(String contactAddrno) {
		this.contactAddrno = contactAddrno;
	}

	public String getOccupationCode() {
		return occupationCode;
	}

	public void setOccupationCode(String occupationCode) {
		this.occupationCode = occupationCode;
	}

	public String getInsurdCertValidperiod() {
		return insurdCertValidperiod;
	}

	public void setInsurdCertValidperiod(String insurdCertValidperiod) {
		this.insurdCertValidperiod = insurdCertValidperiod;
	}

	public String getNationality() {
		return nationality;
	}

	public void setNationality(String nationality) {
		this.nationality = nationality;
	}

	public String getOccupationCat() {
		return occupationCat;
	}

	public void setOccupationCat(String occupationCat) {
		this.occupationCat = occupationCat;
	}

	public String getSafeguardFlag() {
		return safeguardFlag;
	}

	public void setSafeguardFlag(String safeguardFlag) {
		this.safeguardFlag = safeguardFlag;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getCountryCat() {
		return countryCat;
	}

	public void setCountryCat(String countryCat) {
		this.countryCat = countryCat;
	}

	public String getLongPeriod() {
		return longPeriod;
	}

	public void setLongPeriod(String longPeriod) {
		this.longPeriod = longPeriod;
	}

	public String getHealthNotice() {
		return healthNotice;
	}

	public void setHealthNotice(String healthNotice) {
		this.healthNotice = healthNotice;
	}

	public boolean isInsBlackIdFlag() {return insBlackIdFlag;}

	public void setInsBlackIdFlag(boolean insBlackIdFlag) {this.insBlackIdFlag = insBlackIdFlag;}

	public boolean isInsBlackFullFlag() {return insBlackFullFlag;}

	public void setInsBlackFullFlag(boolean insBlackFullFlag) {this.insBlackFullFlag = insBlackFullFlag;}

	public boolean isInsBlackFlag1() {return insBlackFlag1;}

	public void setInsBlackFlag1(boolean insBlackFlag1) {this.insBlackFlag1 = insBlackFlag1;}

	public boolean isInsBlackFlag2() {return insBlackFlag2;}

	public void setInsBlackFlag2(boolean insBlackFlag2) {this.insBlackFlag2 = insBlackFlag2;}

	public boolean isInsBlackFlag3() {return insBlackFlag3;}

	public void setInsBlackFlag3(boolean insBlackFlag3) {this.insBlackFlag3 = insBlackFlag3;}

	public boolean isInsBlackFlag4() {return insBlackFlag4;}

	public void setInsBlackFlag4(boolean insBlackFlag4) {this.insBlackFlag4 = insBlackFlag4;}

	public boolean isInsBlackFlag5() {return insBlackFlag5;}

	public void setInsBlackFlag5(boolean insBlackFlag5) {this.insBlackFlag5 = insBlackFlag5;}
}
