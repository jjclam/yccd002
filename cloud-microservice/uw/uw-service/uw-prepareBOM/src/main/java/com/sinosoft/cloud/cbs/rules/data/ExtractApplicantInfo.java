package com.sinosoft.cloud.cbs.rules.data;

import com.sinosoft.cache.dao.impl.RedisCommonDao;
import com.sinosoft.cloud.cbs.function.UWFunction;
import com.sinosoft.cloud.cbs.rules.bom.Applicant;
import com.sinosoft.cloud.cbs.rules.bom.Policy;
import com.sinosoft.cloud.nb.service.redis.NBRedisCommon;
import com.sinosoft.cloud.rest.TradeInfo;
import com.sinosoft.lis.entity.*;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.utility.*;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.sql.Connection;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * @Author: zhuyiming
 * @Description: 提取投保人数据
 * @Date: Created in 17:26 2017/9/23
 */
@Service
public class ExtractApplicantInfo {
	private final Log logger = LogFactory.getLog(this.getClass());

	private static final String[] DUTYCODES = {"500101","500102","500701","500801",
			"500802","501301","500601", "500602", "500604", "500605", "500607","300301", "300302",
			"500901", "501901", "500902"};
	private  static final String[] RISKCODES = {"5001","5006","5007","5009","7020","5008","5013","3003","5019"};


	private  static final String[] DUTYCODES19 = {"500101","500102","500701","500801","500802"};

	@Autowired
	RedisCommonDao redisCommonDao;
	@Autowired
	NBRedisCommon nbRedisCommon;
	@Value("${cloud.uw.barrier.control}")
	private String barrier;
	@Autowired
	UWFunction uwFunction;
	/**
	 * 获取投保人信息
	 * @param policy
	 * @throws Exception
	 */
	public void getApplicantInfo(Policy policy, TradeInfo requestInfo) throws Exception {
		LCAppntPojo tLCAppntPojo = (LCAppntPojo) requestInfo.getData(LCAppntPojo.class.getName());
		LCContPojo tLCContPojo = (LCContPojo) requestInfo.getData(LCContPojo.class.getName());
		List<LCPremPojo> tLCPremPojos = (List<LCPremPojo>) requestInfo.getData(LCPremPojo.class.getName());
		Applicant applicantInfo = policy.getApplicant();
		long beginTime = System.currentTimeMillis();
		/** 投保人基本数据SQL（contno：合同号）*/;
		if (tLCAppntPojo != null) {
			FDate fDate = new FDate();
			// 姓名
			applicantInfo.setApplicantName(tLCAppntPojo.getAppntName());
			// 证件类型
			applicantInfo.setIdentityType(tLCAppntPojo.getIDType());
			// 证件号码
			applicantInfo.setIdentityCode(tLCAppntPojo.getIDNo());
			// 性别
			applicantInfo.setSex(tLCAppntPojo.getAppntSex());
			// 身高
			applicantInfo.setHeight(tLCAppntPojo.getStature());
			// 体重
			applicantInfo.setWeight(tLCAppntPojo.getAvoirdupois());
			// 出生日期
			applicantInfo.setBirthday(fDate.getDate(tLCAppntPojo.getAppntBirthday()));
			if(applicantInfo.getBirthday()!=null&&!"".equals(applicantInfo.getBirthday())&&policy.getEffectiveDate()!=null&&!"".equals(policy.getEffectiveDate())){
				// 年龄
				applicantInfo.setAge(PubFun
						.calInterval(applicantInfo.getBirthday(), policy
								.getEffectiveDate(), "Y"));
			}
			// 证件有效期
			applicantInfo.setIdentityValidityTerm(tLCAppntPojo.getIdValiDate());
			// 国籍
			applicantInfo.setNationality(tLCAppntPojo.getNativePlace());
			// 户籍
			applicantInfo.setHouseholdRegister(tLCAppntPojo.getRgtAddress());
			// 学历
			applicantInfo.setEducation(tLCAppntPojo.getDegree());
			// 婚姻状况
			applicantInfo.setMarrageStatus(tLCAppntPojo.getMarriage());
			// 与被保人关系
			applicantInfo.setRelationWithInsured(tLCAppntPojo.getRelatToInsu());
			// 职业代码
			applicantInfo.setOccuCode(tLCAppntPojo.getOccupationCode());
			// 职业类别
			applicantInfo.setOccuType(tLCAppntPojo.getOccupationType());
			// 职业描述
			applicantInfo.setOccuDescribe(tLCAppntPojo.getWorkType());
			// 兼职
			applicantInfo.setPartTimeJobFlag(tLCAppntPojo.getPluralityType());
			// 是否有摩托车驾照
			applicantInfo
					.setMotorLicence("Y".equals(tLCAppntPojo.getHaveMotorcycleLicence()) ? true
							: false);
			// 驾照类型
			applicantInfo.setLicenceType(tLCAppntPojo.getLicenseType());
			// 地址代码
			applicantInfo.setAddrCode(tLCAppntPojo.getAddressNo());
			// BMI
			applicantInfo.setBMI(tLCAppntPojo.getBMI()+"");
			// 居民类型
			applicantInfo.setResidentType(tLCAppntPojo.getAppRgtTpye());
			// 职务
			applicantInfo.setDuties(tLCAppntPojo.getPosition());
			// 投保人客户号
			applicantInfo.setClientNo(tLCAppntPojo.getAppntNo());
			// 客户级别
			applicantInfo.setClientLevel(tLCAppntPojo.getCUSLevel());
			// 美国纳税人识别号(TIN)
			applicantInfo.setTinFlag("1".equals(tLCAppntPojo.getTINFlag()) ? true
					: false);
		}
		String custNo = "";
		if (applicantInfo != null) {
			custNo = applicantInfo.getClientNo();
		}
		logger.debug("投保人基础-提数：" + (System.currentTimeMillis() - beginTime) / 1000.0
				+ "s");
		beginTime = System.currentTimeMillis();
		List<LCPolPojo> tLCPolPojoList = (List) requestInfo.getData(LCPolPojo.class.getName());
		/** 本单累计期交保费（contno：合同号） */
		double contPerioPrem = 0;
		/** 本单累计趸交保费（contno：合同号） */
		double contSinglePrem = 0;
		if(tLCPolPojoList != null && tLCPolPojoList.size()>0){
			for(int i=0;i<tLCPolPojoList.size();i++){
				if(tLCPolPojoList.get(i).getPayIntv() != 0){
					//本单累计期交保费
					contPerioPrem += tLCPolPojoList.get(i).getPrem();;
				} else {
					//本单累计趸交保费
					contSinglePrem += tLCPolPojoList.get(i).getPrem();
				}
			}
		}
		applicantInfo.setPeriodPremium(contPerioPrem);
		applicantInfo.setSinglePremium(contSinglePrem);
		try {
			SSRS ssrs = uwFunction.checkAppWhiteSign(tLCAppntPojo.getAppntName(), tLCAppntPojo.getAppntSex(), tLCAppntPojo.getIDType(), tLCAppntPojo.getIDNo(), tLCAppntPojo.getAppntBirthday());
			applicantInfo.setWhiteSign((ssrs==null || ssrs.MaxRow==0)?false:true);
		}catch (Exception e){
			logger.error("白名单取值异常"+ExceptionUtils.exceptionToString(e)+requestInfo.toString());
			requestInfo.addError("白名单取值异常"+ExceptionUtils.exceptionToString(e));
			return;
		}
		/** 风险测评SQL（contno：合同号）*/
		LYVerifyAppPojo tLYVerifyAppPojo = (LYVerifyAppPojo) requestInfo.getData(LYVerifyAppPojo.class.getName());
		if(tLYVerifyAppPojo !=null){
			applicantInfo.setRiskAssessment(tLYVerifyAppPojo.getRiskEvaluationResult());
		}
		/** 平均年收入SQL（COMCODE：机构代码，cityavg：城镇平均年收入，valligeavg：乡村平均年收入） */
		LDAvgInComPojo ldAvgInComPojo = null;
		if(policy.getManageOrg() != null && policy.getManageOrg().length() > 4) {
			ldAvgInComPojo = redisCommonDao.getEntityRelaDB(LDAvgInComPojo.class, policy.getManageOrg().substring(0,4));
		}else if(policy.getManageOrg() != null && policy.getManageOrg().length() == 4){
			ldAvgInComPojo = redisCommonDao.getEntityRelaDB(LDAvgInComPojo.class, policy.getManageOrg());
		}
		if(ldAvgInComPojo != null){
			double cityAvg = ldAvgInComPojo.getCITYINCOME();//CityAvg修改为cityincome
			double valligeAvg = ldAvgInComPojo.getVILLAGEINCOME();//ValligeAvg修改为Valligeincome
			//applicantInfo.setPerDisposIncome(cityAvg != null && !"".equals(cityAvg)?Double.parseDouble(cityAvg):0);
			applicantInfo.setPerDisposIncome(cityAvg);
			// 农村居民人均纯收入
			//applicantInfo.setPerIncome(valligeAvg != null && !"".equals(valligeAvg)?Double.parseDouble(valligeAvg):0);
			applicantInfo.setPerIncome(valligeAvg);
		}
		/** 投保人是否残疾SQL（customerno：客户编码，contno：合同号） */
		List<LCCustomerImpartParamsPojo> tLCCustomerImpartParamsList = (List<LCCustomerImpartParamsPojo>) requestInfo.getData(LCCustomerImpartParamsPojo.class.getName());
		/*boolean disabledSign = false;
		if(tLCCustomerImpartParamsList != null && tLCCustomerImpartParamsList.size() > 0){
			for(int i=0;i<tLCCustomerImpartParamsList.size();i++){
				if(custNo.equals(tLCCustomerImpartParamsList.get(i).getCustomerNo())
						&& "0".equals(tLCCustomerImpartParamsList.get(i).getCustomerNoType())
						&& "1".equals(tLCCustomerImpartParamsList.get(i).getImpartParam())
						&& "YesOrNo".equals(tLCCustomerImpartParamsList.get(i).getImpartParamName())){
					String impartVer = tLCCustomerImpartParamsList.get(i).getImpartVer();
					String impartCode = tLCCustomerImpartParamsList.get(i).getImpartCode();
					if(("DLR3".equals(impartVer) && "1-3".equals(impartCode))
							|| ("GX8".equals(impartVer) && "18".equals(impartCode) || "10".equals(impartCode))
							|| ("GX10".equals(impartVer) && "3".equals(impartCode))){
						disabledSign = true;
						break;
					}
				}
			}
		}
		applicantInfo.setDisabledSign(disabledSign);*/
		/** 黑名单标识SQL（BLACKLISTNO：黑名单客户号，IDNO：证件号码，IDTYPE：证件类型） */
		boolean blackSign = uwFunction.checkBlackSign(tLCAppntPojo.getAppntNo(), tLCAppntPojo.getIDNo(), tLCAppntPojo.getIDType());
		applicantInfo.setBlackSign(blackSign);
		/**钻石卡效验*/
		String clientLlevel=uwFunction.getClientLevel(tLCAppntPojo);
		applicantInfo.setClientLevel(clientLlevel);
		/** 投保人身高异常SQL （CONTNO：合同号，APPNTNO：投保人编码） */
		double tStature = tLCAppntPojo.getStature();
		SSRS tSSRS = nbRedisCommon.checkHeightErr(tLCAppntPojo.getAppntSex(), String.valueOf(new PubFun().calInterval3(tLCAppntPojo.getAppntBirthday(),tLCContPojo.getCValiDate(),"Y")));
		if(tStature <= 0 || tSSRS == null || tSSRS.MaxRow == 0){
			applicantInfo.setHeightErr(false);
		}else if(tStature < Double.parseDouble(tSSRS.GetText(1,1)) || tStature > Double.parseDouble(tSSRS.GetText(1,2))){
			applicantInfo.setHeightErr(true);
		}else {
			applicantInfo.setHeightErr(false);
		}
		/** 投保人体重异常SQL 成年（CONTNO：合同号，APPNTNO：投保人编码） */
		double tAvoirdupois = tLCAppntPojo.getAvoirdupois();
		if(tAvoirdupois <= 0 || tSSRS == null || tSSRS.MaxRow == 0){
			applicantInfo.setWeightErr(false);
		}else if(tAvoirdupois < Double.parseDouble(tSSRS.GetText(1,3)) || tAvoirdupois > Double.parseDouble(tSSRS.GetText(1,4))){
			applicantInfo.setWeightErr(true);
		}else {
			applicantInfo.setWeightErr(false);
		}
		long minBMI = 0;
		long maxBMI = 0;
		if(tStature == 0){
			minBMI = Math.round(tAvoirdupois/1000000);
		}else {
			minBMI = Math.round(tAvoirdupois/((tStature/100)*(tStature/100)));
		}
		if(tStature == 0){
			maxBMI = Math.round(tAvoirdupois/0.0001);
		}else {
			maxBMI = Math.round(tAvoirdupois/((tStature/100)*(tStature/100)));
		}
		if(minBMI<18 || maxBMI>28){
			applicantInfo.setBmiErr(true);
		}else {
			applicantInfo.setBmiErr(false);
		}
		// 是否填写投保人告知SQL（CONTNO：合同号，CUSTOMERNO：客户编码）
		boolean appNotify = false;
		if(tLCCustomerImpartParamsList != null && tLCCustomerImpartParamsList.size() > 0){
			for(int i=0;i<tLCCustomerImpartParamsList.size();i++){
				if("0".equals(tLCCustomerImpartParamsList.get(i).getCustomerNoType())){
					appNotify = true;
				}
			}
		}
		applicantInfo.setAppNotify(appNotify);
		/** 黑名单1-证件号与和黑名单相同,不分国籍,>0触发,参数:一个contno */
		boolean appBackIdFlag = uwFunction.checkBlackIdFlag(tLCAppntPojo.getIDNo());
		applicantInfo.setAppBlackIdFlag(appBackIdFlag);
		/** 黑名单2-中国客户名字、生日与黑名单相同,>0触发,参数:一个contno */
		SimpleDateFormat sdf =   new SimpleDateFormat( "yyyy-MM-dd" );
		if("CHN".equals(tLCAppntPojo.getNativePlace())){
			boolean checkBlackCHNFlag = uwFunction.checkBlackCHNFlag(tLCAppntPojo.getAppntName(), tLCAppntPojo.getAppntBirthday());
			applicantInfo.setAppBlackChnFlag(checkBlackCHNFlag);
		}
		/** 黑名单3-投保人名1部分组成,投保人名与黑名单表四个名字对比,>0触发,参数:4个contno */
		boolean checkBlackFlag1 = uwFunction.checkBlackFlag1(tLCAppntPojo.getAppntName(), tLCAppntPojo.getNativePlace(), tLCAppntPojo.getAppntBirthday());
		applicantInfo.setAppBlackFlag1(checkBlackFlag1);
		/** 黑名单4-投保人名2部分组成,投保人名与黑名单表2个名字对比,>0触发,参数:2个contno
		 * 有一个name相同，且出生日期相同 */
		boolean checkBlackFlag2 = uwFunction.checkBlackFlag2(tLCAppntPojo.getAppntName(), tLCAppntPojo.getNativePlace(), tLCAppntPojo.getAppntBirthday());
		applicantInfo.setAppBlackFlag2(checkBlackFlag2);
		/** 黑名单5-投保人名2部分组成,投保人名与黑名单表2个名字对比,>=2触发,参数:2个contno
		 * firstname和surnname都要相同.无生日限制 */
		int count = 0;
		if(tLCAppntPojo.getAppntName().length() - tLCAppntPojo.getAppntName().replace(" ","").length() == 1
				&& (tLCAppntPojo.getNativePlace() == null || !"CHN".equals(tLCAppntPojo.getNativePlace()))){
			String appntName = tLCAppntPojo.getAppntName();
			String firstName = appntName.substring(0, appntName.indexOf(" "));
			String surName = appntName.substring(appntName.indexOf(" ")+1);
			count += uwFunction.checkBlackFlag3_1(firstName);
			count += uwFunction.checkBlackFlag3_3(surName);
		}
		if(count >= 2){
			applicantInfo.setAppBlackFlag3(true);
		}
		/** 黑名单6-投保人名>=3部分组成,投保人名与黑名单表3个名字对比,>=0触发,参数:3个contno
		 * firstname,middlename,surname,有生日限制 */
		boolean checkBlackFlag4 = uwFunction.checkBlackFlag4(tLCAppntPojo.getAppntName(), tLCAppntPojo.getNativePlace(), tLCAppntPojo.getAppntBirthday());
		applicantInfo.setAppBlackFlag4(checkBlackFlag4);
		/** 黑名单7-投保人名>=3部分组成,投保人名与黑名单表3个名字对比,>=2触发,参数:3个contno
		 * firstname,middlename,surname,无生日限制 */
		count = 0;
		if(tLCAppntPojo.getAppntName().length() - tLCAppntPojo.getAppntName().replace(" ","").length() >= 2
				&& (tLCAppntPojo.getNativePlace() == null || !"CHN".equals(tLCAppntPojo.getNativePlace()))){
			String appntName = tLCAppntPojo.getAppntName();
			String name[] = appntName.split(" ");
			String firstName = name[0];
			String middleName = name[1];
			String surName = appntName.substring(appntName.indexOf(middleName)+middleName.length()+1,appntName.length());
			count += uwFunction.checkBlackFlag3_1(firstName);
			count += uwFunction.checkBlackFlag3_2(middleName);
			count += uwFunction.checkBlackFlag3_3(surName);
		}
		if(count >= 2){
			applicantInfo.setAppBlackFlag5(true);
		}
		/** 投保人7053险种各期保费之和 */
		String insuredsex = null;
		int insuredappage = 0;
		int insuyear = 0;
		int payintv = 0;
		double amnt = 0.00;

		double renewalPremSum7053 = 0.00;
		for(LCPolPojo lcPolPojo : tLCPolPojoList) {
			if("7053".equals(lcPolPojo.getRiskCode())){
				insuredsex = lcPolPojo.getInsuredSex();
				insuredappage = lcPolPojo.getInsuredAppAge();
				insuyear = lcPolPojo.getInsuYear();
				payintv = lcPolPojo.getPayIntv();
				amnt = lcPolPojo.getAmnt();
				if(insuredappage > 70){
					break;
				}
				VData tVData = new VData();
				StringBuffer sql1 = new StringBuffer();
				sql1.append("SELECT SUM("+ amnt +"/1000 * RATE * MUTIPAYINTV("+ payintv +")) FROM RT_7053 ");
				sql1.append("WHERE SEX = ? AND APPAGE BETWEEN ? AND 70 AND PAYENDYEAR = 1 AND INSUYEAR = ? ");
				TransferData transferData = new TransferData();
				transferData.setNameAndValue("sex", "string:"+ insuredsex);
				transferData.setNameAndValue("appage", "int:" + insuredappage );
				transferData.setNameAndValue("insuyear", "int:" + insuyear);
				ExeSQL exeSql1 = new ExeSQL();
				tVData.add(sql1.toString());
				tVData.add(transferData);
				String result = exeSql1.getOneValue(tVData);
				if(!"".equals(result)){
					renewalPremSum7053 += Double.parseDouble(result);
				}
				break;
			}
		}
		applicantInfo.setRenewalPremSum7053(renewalPremSum7053);

		//投保人老核心提数
		if("true".equals(barrier)){
			logger.debug("保单号：" + tLCContPojo.getContNo()+ "，投保人挡板启动！");
		}else {
			getOtherData(applicantInfo, requestInfo,tLCAppntPojo);
			if(requestInfo.hasError()){
				requestInfo.addError("投保人提数失败！");
				return;
			}
		}

		//特殊保费3，险种5001,、5007、5008已停售
		double applicantString1 = 0.0;
		if(applicantInfo.getApplicantString1() != null
				&& !"".equals(applicantInfo.getApplicantString1())){
			applicantString1 = Util.toDouble(applicantInfo.getApplicantString1());
		}
		// 本单累计年交保费去除'5013','3003'险种,新增本单累计
		double applicantDouble2 = applicantInfo.getApplicantDouble2();
		//银保渠道累计保费
		double ybPrem = applicantInfo.getYbPrem();
		List<String> polnos = new ArrayList<>();
		for(LCPolPojo lcPolPojo:tLCPolPojoList){
			if(lcPolPojo.getPayEndYear() != 1
					&& lcPolPojo.getPayEndYear() != 3){
				polnos.add(lcPolPojo.getPolNo());
			}
			if(lcPolPojo.getPayIntv() == 12
					|| lcPolPojo.getPayIntv() ==1
					|| lcPolPojo.getPayIntv() == 3
					|| lcPolPojo.getPayIntv() == 6){
				boolean riskFlag = false;
				for(String riskcode : RISKCODES){
					if(riskcode.equals(lcPolPojo.getRiskCode())){
						riskFlag = true;
						break;
					}
				}
				if(!riskFlag){
					applicantDouble2 += (12/lcPolPojo.getPayIntv())*lcPolPojo.getPrem();
				}
			}
			if("3".equals(lcPolPojo.getSaleChnl())
					&& lcPolPojo.getPayIntv() != 0
					&& ((lcPolPojo.getPaytoDate()!=null
					&& !lcPolPojo.getPaytoDate().equals(lcPolPojo.getEndDate()))
					|| lcPolPojo.getPaytoDate() == null)){
				ybPrem += lcPolPojo.getPrem();
			}
			/*if(!(1==lcPolPojo.getPayEndYear()
					|| 3==lcPolPojo.getPayEndYear())){
				//特殊保费3，险种5001,、5007、5008已停售
				applicantString1 += lcPolPojo.getPrem();
			}*/
		}


		for(String polno : polnos){
			for(LCPremPojo lcPremPojo : tLCPremPojos){
				if(polno.equals(lcPremPojo.getPolNo())){
					for(int j=0;j<DUTYCODES19.length;j++){
						if(DUTYCODES19[j].equals(lcPremPojo.getDutyCode())){
							applicantString1 += lcPremPojo.getPrem();
							break;
						}
					}
				}
			}
		}
		// 特殊保费2
		double applicantDouble1 = applicantInfo.getApplicantDouble1();
		for(String polno : polnos){
			for(LCPremPojo lcPremPojo : tLCPremPojos){
				if(polno.equals(lcPremPojo.getPolNo())){
					for(int j=0;j<DUTYCODES.length;j++){
						if(DUTYCODES[j].equals(lcPremPojo.getDutyCode())){
							applicantDouble1 += lcPremPojo.getPrem();
							break;
						}
					}
				}
			}
		}
		int payIntv = 0;
		double premPerYear = applicantInfo.getPremiumPerYear();
		if(tLCPolPojoList != null && tLCPolPojoList.size()>0){
			for(int i=0;i<tLCPolPojoList.size();i++){
				payIntv = tLCPolPojoList.get(i).getPayIntv();
				if(payIntv != 12 & payIntv !=1 && payIntv != 3 && payIntv !=6){
					continue;
				}
				if ("7020".equals(tLCPolPojoList.get(i).getRiskCode()) || "5019".equals(tLCPolPojoList.get(i).getRiskCode())) {
					continue;
				}
				premPerYear += (12 / payIntv) * tLCPolPojoList.get(i).getPrem();
			}
		}
		// 特殊保费2
		applicantInfo.setApplicantDouble1(applicantDouble1);
		// 本单累计年交保费去除'5013','3003'险种
		applicantInfo.setApplicantDouble2(applicantDouble2);
		//银保渠道累计保费
		applicantInfo.setYbPrem(ybPrem);
		//本单累计年缴保费
		applicantInfo.setPremiumPerYear(premPerYear);
		// 投保人累计期交保费
		applicantInfo.setSumPeriodPremium(applicantInfo.getSumPeriodPremium() + contPerioPrem);
		// 投保人累计趸交保费
		applicantInfo.setSumSinglePremium(applicantInfo.getSumSinglePremium() + contSinglePrem);
		//特殊保费3
		applicantInfo.setApplicantString1(applicantString1 + "");

		logger.debug("投保人拓展-提数："+(System.currentTimeMillis() - beginTime)/1000.0+"s");
	}

	/**
	 * 投保人老核心提数
	 * @param applicantInfo
	 */
	public void getOtherData(Applicant applicantInfo, TradeInfo tradeInfo ,LCAppntPojo lcAppntPojo){
		String custNo = "";
		String custName= "";
		String custIdType = "";
		String custIdNo = "";
		String custBirthday = "";
		String custSex = "";
		if (applicantInfo != null) {
			custNo = applicantInfo.getClientNo();
			custName = applicantInfo.getApplicantName();
			custIdType = applicantInfo.getIdentityType();
			custIdNo = applicantInfo.getIdentityCode();
			SimpleDateFormat sdf =   new SimpleDateFormat( "yyyy-MM-dd" );
			custBirthday = sdf.format(applicantInfo.getBirthday());
			custSex = applicantInfo.getSex();
		}
		StringBuffer sql = new StringBuffer();
		/** 投保人累计期交保费 */
		sql.append(SQLConstant.Applicant.SUMPERIODPREMSQL);
		sql.append(" UNION ");
		/** 投保人累计趸交保费 */
		sql.append(SQLConstant.Applicant.SUMSINGLEPREMSQL);
		sql.append(" UNION ");
		/** 本单累计年交保费（CONTNO：合同号） */
		sql.append(SQLConstant.Applicant.PREMPERYEARSQL);
		sql.append(" UNION ");
		/** 银保渠道累交保费 */
		sql.append(SQLConstant.Applicant.YBPREMSQL);
		sql.append(" UNION ");
		/** 投保人特殊保费1 */
		sql.append(SQLConstant.Applicant.SUMSPECIALPREMSQL1);
		sql.append(" UNION ");
		/** 投保人特殊保费2 */
		sql.append(SQLConstant.Applicant.SUMSPECIALPREMSQL2);
		sql.append(" UNION ");
		/** 本单累计年交保费去除'5019'险种 */
		sql.append(SQLConstant.Applicant.PREMPERYEAR2SQL);
		sql.append(" UNION ");
		/** 投保人特殊保费3 */
		sql.append(SQLConstant.Applicant.SUMSPECIALPREMSQL3);
		/**中保信上一年度理赔额 --wpq--*/
//		sql.append(" UNION ");
//		sql.append(SQLConstant.Applicant.YEARCLAIMLIMITSQL);
		/**钻石卡级别 --xzh--*/
//		sql.append(" UNION ");
//		sql.append(SQLConstant.Applicant.LCAPPNTCLIENTLEVEL);

		VData tVData = new VData();
		TransferData tParam = new TransferData();
		// 累计期交保费
		tParam.setNameAndValue("APPNTNO1", "string:" + custNo);
		// 累计趸交保费
		tParam.setNameAndValue("APPNTNO2", "string:" + custNo);
		//本单累计年交保费
		tParam.setNameAndValue("APPNTNO3", "string:" + custNo);
		// 银保渠道的累计期交保费
		tParam.setNameAndValue("APPNTNO6", "string:" + custNo);
		// 特殊保费1
		tParam.setNameAndValue("APPNTNO7", "string:" + custNo);
		//	特殊保费2
		tParam.setNameAndValue("APPNTNO8", "string:" + custNo);
		// 本单累计年交保费去除'5013','3003'险种
		tParam.setNameAndValue("APPNTNO9", "string:" + custNo);
		//	特殊保费3
		tParam.setNameAndValue("APPNTNO10", "string:" + custNo);
		//中保信上一年度理赔额 --wpq--
		//tParam.setNameAndValue("APPNTNO11","string:"+lcAppntPojo.getContNo());
		//**钻石卡级别 --xzh--
//		tParam.setNameAndValue("APPNTNAME","string:"+custName);
//		tParam.setNameAndValue("APPNTIDTYPE","string:"+custIdType);
//		tParam.setNameAndValue("APPNTIDNO","string:"+custIdNo);
//		tParam.setNameAndValue("APPNTBIRTHDAY","string:"+custBirthday);
//		tParam.setNameAndValue("APPNTSEX","string:"+custSex);

		logger.debug("ApplicantInfo：" + sql);
		tVData.add(sql.toString());
		tVData.add(tParam);
		SSRS tSSRS = new SSRS();
		Connection tConnection = null;
		try {
			tConnection = DBConnPool.getConnection("basedataSource");
			ExeSQL exeSql = new ExeSQL(tConnection);
			tSSRS = exeSql.execSQL(tVData);
		}catch (Exception e){
			e.printStackTrace();
			logger.error(ExceptionUtils.exceptionToString(e));
			tradeInfo.addError(ExceptionUtils.exceptionToString(e));
			return;
		}finally {
			if (tConnection != null) {
				try {
					tConnection.close();
				} catch (SQLException e) {
					e.printStackTrace();
					tradeInfo.addError(ExceptionUtils.exceptionToString(e));
					logger.error(ExceptionUtils.exceptionToString(e));
					return;
				}
			}
		}

		LCContPojo lcContPojo = (LCContPojo) tradeInfo.getData(LCContPojo.class.getName());
		if(tSSRS == null){
			logger.debug("保单号：" + lcContPojo.getContNo() + "，投保人访问老核心SQL发生异常！");
			tradeInfo.addError("保单号：" + lcContPojo.getContNo() + "，投保人访问老核心SQL发生异常！");
			return;
		}

		int flag = 0;
		if (null != tSSRS && tSSRS.MaxRow > 0) {
			for (int i = 1; i <= tSSRS.MaxRow; i++) {
				/*
				 * 取flag标记，（1：投保人累计期交保费 2：投保人累计趸交保费 3：本单累计年交保费 4:本单累计期交保费
				 * 5:本单累计趸交保费 6:风险测评结果 7:城镇居民人均可支配收入、农村居民人均纯收入 8:是否为残疾人 9:黑名单标记
				 * 10:身高异常 11:体重异常 12:BMI异常 13:是否填写投保人告知）
				 */
				flag = Util.toInt(tSSRS.GetText(i, 4));
				switch (flag) {
					case 1:
						// 投保人累计期交保费
						applicantInfo.setSumPeriodPremium(Util.toDouble(tSSRS.GetText(i, 1)));
						break;
					case 2:
						// 投保人累计趸交保费
						applicantInfo.setSumSinglePremium(Util.toDouble(tSSRS.GetText(i, 1)));
						break;
					case 3:
						// 本单累计年交保费
						applicantInfo.setPremiumPerYear(Util.toDouble(tSSRS.GetText(i, 1)));
					case 14:
						// 银保渠道保费
						applicantInfo.setYbPrem(Util.toDouble(tSSRS.GetText(i, 1)));
						break;
					case 16:
						// 特殊保费1
						applicantInfo.setSpecialPrem1(Util.toDouble(tSSRS.GetText(i, 1)));
						break;
					case 17:
						//特殊保费2
						applicantInfo.setApplicantDouble1(Util.toDouble(tSSRS.GetText(i, 1)));
						break;
					case 18:
						// 本单累计年交保费去除'5013','3003'险种
						applicantInfo.setApplicantDouble2(Util.toDouble(tSSRS.GetText(i, 1)));
						break;
					case 19:
						// 特殊保费3
						applicantInfo.setApplicantString1(tSSRS.GetText(i, 1));
						break;
					case 28:
						// 中保信上一年度理赔额 wpq
						applicantInfo.setYearClaimLimit(Util.toDouble(" "));
						break;
//					case 29:
//						// 钻石卡级别 wpq
//						if("0".equals((String)(tSSRS.GetText(i, 1)))){
//							applicantInfo.setClientLevel("");
//						}else{
//							applicantInfo.setClientLevel((String)(tSSRS.GetText(i, 1)));
//						}
//
//						break;
				}
			}
		}

	}
}
