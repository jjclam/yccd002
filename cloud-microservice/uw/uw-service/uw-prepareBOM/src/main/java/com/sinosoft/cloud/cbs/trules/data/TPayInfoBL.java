package com.sinosoft.cloud.cbs.trules.data;

import com.sinosoft.cache.dao.impl.RedisCommonDao;
import com.sinosoft.cloud.cbs.trules.bom.Product;
import com.sinosoft.cloud.cbs.trules.bom.TPayInfo;
import com.sinosoft.cloud.cbs.trules.util.DomainUtils;
import com.sinosoft.cloud.rest.TradeInfo;
import com.sinosoft.lis.entity.*;
import com.sinosoft.utility.ExceptionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @Author:wangshuliang
 * @Description: 收费信息
 * @Date:Created in 9:36 2018/6/8
 * @Modified by:
 */
@Component
public class TPayInfoBL {
    private Log logger = LogFactory.getLog(getClass());

    @Autowired
    private RedisCommonDao redisCommonDao;

    /**
     * 提取收费信息
     *
     * @param tradeInfo
     * @return
     */
    public TradeInfo getTPayInfo(TradeInfo tradeInfo) {

        logger.debug("提取收费信息开始");
        TPayInfo tPayInfo = new TPayInfo();
        //交费方式
        LCContPojo lcContPojo = (LCContPojo) tradeInfo.getData(LCContPojo.class.getName());
        tPayInfo.setPayWay(DomainUtils.converPayMode(lcContPojo.getPayMode()));
        //开户银行
        LCAppntPojo lcAppntPojo = (LCAppntPojo)tradeInfo.getData(LCAppntPojo.class.getName());
        GlobalPojo globalPojo= (GlobalPojo) tradeInfo.getData(GlobalPojo.class.getName());
        if(!"1032".equals(globalPojo.getTransNo()) && !("2".equals(lcContPojo.getSaleChnl()) && "20,21".contains(lcContPojo.getSellType()))) {
            LDCodePojo ldCodePojo = null;
            try {
                ldCodePojo = redisCommonDao.getEntityRelaDB(LDCodePojo.class, "bank" + "|||" + lcContPojo.getBankCode());
            } catch (Exception e) {
                e.printStackTrace();
                logger.error(ExceptionUtils.exceptionToString(e));
                return tradeInfo;
            }
            if (ldCodePojo == null) {
                logger.debug("lcContPojo.getBankCode()=" + lcContPojo.getBankCode() + " 没有查到对应的LDCodePojo数据");
                tradeInfo.addError("没有查到对应的LDCodePojo数据");
                return tradeInfo;
            }
            tPayInfo.setBankAccname(ldCodePojo.getCodeName());
        }//账号
        tPayInfo.setBankAccno(lcAppntPojo.getBankAccNo());
        //是否自动续保
        List<LCPolPojo> lcPolPojos = (List<LCPolPojo>) tradeInfo.getData(LCPolPojo.class.getName());
        tPayInfo.setIsAutoInsurRenew(String.valueOf(lcPolPojos.get(0).getRnewFlag()));
        //是否预交保险费   不需要
         // tPayInfo.setIsPrepayPrem("");
        //总保费
        Product product = (Product) tradeInfo.getData(Product.class.getName());
        if (product != null) {
            tPayInfo.setSumPrem(product.getSumPrem());
        }
        //交费账号与我公司其他客户重复   具体的存什么值  默认设置不重复 0 1
        tPayInfo.setRepeatInPaymentNo("");
        //SELECT * FROM lmcalmode WHERE calcode='V12APP008';   投保规则校验
     /*   String bankaccNo = redisCommonDao.getCheckBankAcc(lcAppntPojo.getBankAccNo());
        if (bankaccNo == null || "".equals(bankaccNo)) {

        } else {
            boolean flag = false;
            String strAcc[] = bankaccNo.split(",");
            for (int i = 0; i < strAcc.length; i++) {
                if (!lcContPojo.getAppntName().equals(strAcc[i])) {
                    flag = true;
                    break;
                }
            }
            if (flag) {
                tPayInfo.setRepeatInPaymentNo("");
            }
        }*/
        tradeInfo.addData(TPayInfo.class.getName(), tPayInfo);
        logger.debug("提取收费信息完成");
        return tradeInfo;
    }
}
