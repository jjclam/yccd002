package com.sinosoft.cloud.cbs.rules.data;

import com.sinosoft.cache.dao.impl.RedisCommonDao;
import com.sinosoft.cloud.cbs.rules.bom.Insured;
import com.sinosoft.cloud.cbs.rules.bom.Policy;
import com.sinosoft.cloud.cbs.rules.bom.Risk;
import com.sinosoft.cloud.rest.TradeInfo;
import com.sinosoft.lis.entity.LCDutyPojo;
import com.sinosoft.lis.entity.LCPolPojo;
import com.sinosoft.lis.entity.LMRiskAppPojo;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.lis.pubfun.PubFun;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by zkr on 2018/6/28.
 */
@Service
public class NewExtractInsurType {
    private final Log logger = LogFactory.getLog(this.getClass());
    @Autowired
    RedisCommonDao redisCommonDao;


    @Value("${cloud.uw.barrier.control}")
    private String barrier;

    /**
     *  获取险种列表
     */
    public List getInsurTypeList(Policy policy, TradeInfo requestInfo){
        // 险种列表
        List insurTypeList = policy.getRiskList();
        // 险种基本数据
        long beginTime = System.currentTimeMillis();
        List<LCPolPojo> tLCPolPojoList = (List) requestInfo.getData(LCPolPojo.class.getName());
        // 保险费合计
        double sumPremium = 0;
        // 累计保额
        double sumCoverag = 0;
        // 期交基本保险费之和,5013险种已停售
        double sumBasicPremium = 0;

        // 累计追加保费
       // double sumAddPre = (double)requestInfo.getData("sumAddPre");
        // 六个月内投保次数
        int insuredNum=(int)requestInfo.getData("insuredNum");
        //单数(该险种是第几单).
       // int num=(int)requestInfo.getData("num");
        //审核标识
       // String adultflag=(String)requestInfo.getData("adultflag");
        // 保单的保额、保费
        for (int i = 0; i < tLCPolPojoList.size(); i++) {
            sumPremium += tLCPolPojoList.get(i).getPrem();
        }

        FDate fDate = new FDate();
        if(tLCPolPojoList != null && tLCPolPojoList.size()>0){
            for(int i=0;i<tLCPolPojoList.size();i++){
                LMRiskAppPojo tLMRiskAppPojo = redisCommonDao.getEntityRelaDB(LMRiskAppPojo.class, tLCPolPojoList.get(i).getRiskCode());
                List<LCDutyPojo> tLCDutyPojoList = (List) requestInfo.getData(LCDutyPojo.class.getName());
                if (tLCDutyPojoList != null && tLCDutyPojoList.size() > 0) {
                    for (int j = 0; j < tLCDutyPojoList.size(); j++) {
                        if (tLCDutyPojoList.get(j) != null && tLCDutyPojoList.get(j).getPolNo().equals(tLCPolPojoList.get(i).getPolNo())) {
                            Risk risk = new Risk();
                            // 基本保险费
                            risk.setBasicPremium(tLCPolPojoList.get(i).getStandPrem());
                            // 是否自动垫交
                            risk.setAutoPayingInAdvance("1".equals(tLMRiskAppPojo.getAutoPayFlag()) ? true
                                    : false);
                            // 红利领取方式
                            risk.setBonusDrawType(tLMRiskAppPojo.getBonusMode());
                            // 保险期间
                            risk.setCoveragePeriod(tLCPolPojoList.get(i).getInsuYear());
                            // 是否主险
                            risk.setBasicRisk("S".equals(tLMRiskAppPojo.getSubRiskFlag()) ? false
                                    : true);
                            // 险种代码
                            risk.setRiskCode(tLMRiskAppPojo.getRiskCode());
                            // 险种类型（L--人寿保险 、 R--年金保险 、 H--健康险 、 A--意外伤害保险 、 S--重疾保险 、
                            // U--万能保险）
                            risk.setKindCode(tLMRiskAppPojo.getKindCode());
                            // 险种名称
                            risk.setRiskName(tLMRiskAppPojo.getRiskName());
                            // 险种分类（L--寿险(Life)、A--意外险(Accident)、H--健康险(Health)）
                            risk.setRiskType(tLMRiskAppPojo.getRiskType());
                            // 险种类别（L--长险(Long)、M--一年期险(Middle)、S--极短期险(Short)）
                            risk.setRiskPeriod(tLMRiskAppPojo.getRiskPeriod());
                            // 险种分类1（默认为空 、 1--定额给付型 、 2--费用报销型 、 3--住院津贴型 、 4--委托管理型 、
                            // 5--社保补充型 、 6--意外保险）
                            risk.setRiskType1(tLMRiskAppPojo.getRiskType1());
                            // 险种分类2（Y--打印年金领取年龄 、 N--不打印年金领取年龄）
                            risk.setRiskType2(tLMRiskAppPojo.getRiskType2());
                            // 险种分类3（1 传统险 、 2 分红 、 3 投连 、 4 万能 、 5 其他）
                            risk.setRiskType3(tLMRiskAppPojo.getRiskType3());
                            // 险种分类4（1 终身 、 2 两全及生存 、 3 定期 、 4 年金 、 5 重大疾病 、 6 意外 、 7 健康
                            // 、 8 短期综合（含意外和医疗） 、 9 其他）
                            risk.setRiskType4(tLMRiskAppPojo.getRiskType4());
                            // 险种分类5（1 表示一年期及一年期以内 、 2 表示一年期以上）
                            risk.setRiskType5(tLMRiskAppPojo.getRiskType5());
                            // 险种细分（A -意外伤害保险（普通意外险） 、 L -人寿保险（含定期） 、 M -医疗保险（指门诊/住院医疗险）
                            // 、 P -年金类保险 、 Y-交通意外险 、 N-意外医疗险 、 J-津贴险（住院） 、 H-重疾险 、
                            // YJ--意外津贴 、 D -定期疾病保险）
                            risk.setRiskTypeDetail(tLMRiskAppPojo.getRiskTypeDetail());
                            // 份数
                            risk.setQuantity(tLCPolPojoList.get(i).getMult());
                            // 停售日
                            risk.setStopSellDate(fDate.getDate(tLMRiskAppPojo.getEndDate()));
                            // 险种版本号
                            risk.setVersion(tLMRiskAppPojo.getRiskVer());
                            // 基本保险金额
                            risk.setBasicCoverage(tLCPolPojoList.get(i).getAmnt());
                            // 缴费年限
                            risk.setPayYears(tLCPolPojoList.get(i).getPayYears());
                            // 缴费期间单位
                            risk.setFeePeriodUnit(tLCPolPojoList.get(i).getPayEndYearFlag());
                            // 缴费期间
                            risk.setFeePeriod(tLCPolPojoList.get(i).getPayEndYear());
                            // 养老金领取频率（1-按年 ， 2-按月）
                            risk.setAnnuityFrequency(tLCPolPojoList.get(i).getFQGetMode());
                            // 养老金领取期间（1-按年 ， 2-按月）
                            risk.setAnnuityPeriod(Util.toInt(tLCPolPojoList.get(i).getFQGetMode()));
                            // 首期保险费支付方式
                            risk.setFirstPrePayMethod(tLCPolPojoList.get(i).getPayMode());
                            // 续期保险费支付方式
                            risk.setAfterPrePayMethod(tLCPolPojoList.get(i).getPayLocation());
                            // 养老金领取年龄
                            risk.setDrawEndownentAge(tLCPolPojoList.get(i).getGetYear());
                            // 养老金领取方式
                            risk.setEndowmentGetType(tLCPolPojoList.get(i).getLiveGetMode());
                            // 养老金给付方式
                            risk.setEndowmentPayType(tLCPolPojoList.get(i).getLiveAccFlag());
                            // 第一被保人ID
                            risk.setInsuredFirstId(tLCPolPojoList.get(i).getInsuredNo());
                            // 交费频率
                            risk.setPaymentFreqType(tLCPolPojoList.get(i).getPayIntv() + "");
                            // 年交保费
                            if ("1".equals(risk.getPaymentFreqType())) {
                                // 年缴保费（按月交）
                                risk.setPremiumPerYear(tLCPolPojoList.get(i).getPrem() * 12);
                                //年缴保费2（按月交）
                                risk.setRiskDouble1(tLCPolPojoList.get(i).getPrem() * 12);
                            } else if ("3".equals(risk.getPaymentFreqType())) {
                                // 年缴保费（按季交）
                                risk.setPremiumPerYear(tLCPolPojoList.get(i).getPrem() * 4);
                                //年缴保费2（按季交）
                                risk.setRiskDouble1(tLCPolPojoList.get(i).getPrem()*4);
                            }else if("12".equals(risk.getPaymentFreqType())) {
                                // 年缴保费（按年交）
                                risk.setPremiumPerYear(tLCPolPojoList.get(i).getPrem());
                                //年缴保费2（按年交）
                                risk.setRiskDouble1(tLCPolPojoList.get(i).getPrem());
                            } else {
                                //	年缴保费（非年交、季交、月交）
                                risk.setPremiumPerYear(tLCPolPojoList.get(i).getPrem());
                                //年缴保费2（非年交、季交、月交）
                                risk.setRiskDouble1(0.0d);
                            }
                            // 红利领取人
                            risk.setBonusReceiver(tLCPolPojoList.get(i).getBonusMan());
                            // 保险期间单位
                            risk.setCoveragePeriodUnit(tLCPolPojoList.get(i).getInsuYearFlag());
                            // 保险期间单位
                            risk.setPayYears(tLCPolPojoList.get(i).getPayYears());
                            // 产品组合编码
                            risk.setProdSetCode(tLCPolPojoList.get(i).getProdSetCode());
                            // 责任编码
                            risk.setDutyCode(tLCDutyPojoList.get(j).getDutyCode());
                            // 险种性质
                            risk.setRiskProp(tLMRiskAppPojo.getRiskProp());
                            // 终交年龄年期
                            risk.setPayendyear(tLCPolPojoList.get(i).getPayEndYear());
                            // 代理机构内部分类
                            risk.setRiskString1(tLCPolPojoList.get(i).getAgentType());
                            //险种续保标记
                            risk.setRnewFlag(tLCPolPojoList.get(i).getRnewFlag() + "");
                            // 保险费合计
                            risk.setSumPremium(sumPremium);
                            // 累计保额
                            risk.setSumCoverage(sumCoverag);
                            // add by wangshuliang start 20180130 累计追加保费
//                            for (int s = 0; s < lcPremPojoList.size(); s++) {
//                                if (tLCDutyPojoList.get(j).getDutyCode().equals(lcPremPojoList.get(s).getDutyCode())) {
//                                    sumAddPre = lcPremPojoList.get(s).getPrem();
//                                    break;
//                                }
//                            }
                            // add by wangshuliang end 20180130
                            // 累计追加保费
                          //  risk.setSumAddPre(sumAddPre);

                            // 期交基本保险费之和
                            risk.setSumBasicPremium(sumBasicPremium);

                            // 六个月内投保次数
                            risk.setInsuredNum(insuredNum);

                            insurTypeList.add(risk);
                        }
                    }
                }
            }
        }
        logger.debug("累计风险保额险种基础提数：" + (System.currentTimeMillis() - beginTime) / 1000.0 + "s");
        if ("true".equals(barrier)) {
            logger.debug("保单号：" + policy.getContNo() + "，累计风险保额险种挡板启动！");
        } else {
            getOtherData(policy, requestInfo);
        }
        return insurTypeList;
    }

    public void getOtherData(Policy policy, TradeInfo tradeInfo){
        List<LCPolPojo> tLCPolPojoList = (List<LCPolPojo>) tradeInfo.getData(LCPolPojo.class.getName());
        List insurTypeList = policy.getRiskList();
        Insured insured = (Insured) policy.getInsuredList().get(0);
        List<Double> mults = new ArrayList();
        for (int j = 0; j < insurTypeList.size(); j++) {
            Risk risk = (Risk) insurTypeList.get(j);

            double mult = 0.0;
            for (int i = 0; i < tLCPolPojoList.size(); i++) {
                if (risk.getRiskCode().equals(tLCPolPojoList.get(i).getRiskCode())) {
                    if (tLCPolPojoList.get(i).getPayIntv() == 0) {
                        // 该险种趸交保费
                        risk.setSinglePremium(tLCPolPojoList.get(i).getPrem());
                    }
                    if (tLCPolPojoList.get(i).getPayIntv() == 12) {
                        // 该险种期交保费
                        risk.setPeriodPremium(tLCPolPojoList.get(i).getPrem());
                    }
                    //本单险种份数
                    mult += tLCPolPojoList.get(i).getMult();
                    mults.add(mult);
                }
            }

            for (int i = 0; i < tLCPolPojoList.size(); i++) {
                if (risk.getRiskCode().equals(tLCPolPojoList.get(i).getRiskCode())) {
                    String payenddate = tLCPolPojoList.get(i).getPayEndDate();
                    String cvalidate = tLCPolPojoList.get(i).getCValiDate();
                    String enddate = tLCPolPojoList.get(i).getEndDate();
                    //交费期间
                    risk.setFeePeriod(PubFun.calInterval3(cvalidate, payenddate, "D"));
                    //保险期间
                    risk.setDurationOfInsurance(PubFun.calInterval3(cvalidate, enddate, "D"));
                    //险种保额
                    risk.setBasicCoverage(tLCPolPojoList.get(i).getAmnt());
                }
            }
        }
        // 单数(该险种是第几单)
        int num=(int)tradeInfo.getData("num");
        //险种份数
        double quantity;
        if (tradeInfo.getData("quantity")==null){
            quantity=0;
        }else{
            quantity=(double)tradeInfo.getData("quantity");
        }
        // 审核标识
        String adultflag=(String)tradeInfo.getData("adultflag");
        // 险种组合份数
        int sumMultByProdsetcode;
         if (tradeInfo.getData("sumMultByProdsetcode")==null){
             sumMultByProdsetcode=0;
         }else {
             sumMultByProdsetcode=(int)tradeInfo.getData("sumMultByProdsetcode");
         }
        long beginTime = System.currentTimeMillis();
        logger.debug("累计风险保额险种拓展提数：" + (System.currentTimeMillis() - beginTime)
                / 1000.0 + "s");
        for (int i=0;i<insurTypeList.size();i++){
            Risk risk = (Risk) insurTypeList.get(i);
            // 单数(该险种是第几单)
            //risk.setNum( num == 500?500:(num + 1));
            // 该险种趸交保费
            risk.setSinglePremium(risk.getSinglePremium());
            // 该险种期交保费
            risk.setPeriodPremium(risk.getPeriodPremium());
            //份数
            risk.setQuantity(quantity+mults.get(i));
            // 险种审核标记
            risk.setAdultflag(adultflag);
            // 交费期间
            risk.setFeePeriod(risk.getFeePeriod());
            //组合险份数
            risk.setSumMultByProdsetcode(sumMultByProdsetcode);
            if (risk.getProdSetCode() != null && !"".equals(risk.getProdSetCode())) {
                risk.setSumMultByProdsetcode(sumMultByProdsetcode + (int) tLCPolPojoList.get(0).getMult());
            }
            //保险期间
            risk.setDurationOfInsurance(risk.getDurationOfInsurance());
            // 险种保额
            risk.setBasicCoverage(risk.getBasicCoverage());
        }

    }
}
