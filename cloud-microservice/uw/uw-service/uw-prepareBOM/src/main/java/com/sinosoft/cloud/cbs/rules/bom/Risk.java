package com.sinosoft.cloud.cbs.rules.bom;

import java.util.Date;

/**
 * 险别信息
 * 
 * @author dingfan
 * 
 */
public class Risk {
	/**
	 * 基本保费
	 */
	private double basicPremium;

	/**
	 * EM值
	 */
	private double EMValue;

	/**
	 * 是否自动垫交
	 */
	private boolean autoPayingInAdvance;

	/**
	 * 红利领取方式
	 */
	private String bonusDrawType;

	/**
	 * 保险期间
	 */
	private int coveragePeriod;

	/**
	 * 领取养老金的年龄
	 */
	private int drawEndownentAge;

	/**
	 * 养老金领取方式
	 */
	private String endowmentGetType;

	/**
	 * 养老金给付方式
	 */
	private String endowmentPayType;

	/**
	 * 第一被保人ID
	 */
	private String insuredFirstId;

	/**
	 * 第二被保人ID
	 */
	private String insuredSecondId;

	/**
	 * 是否主险
	 */
	private boolean basicRisk;

	/**
	 * 交费频率
	 */
	private String paymentFreqType;

	/**
	 * 险种代码
	 */
	private String riskCode;

	/**
	 * 险种类型（L--人寿保险 、 R--年金保险 、 H--健康险 、 A--意外伤害保险 、 S--重疾保险 、 U--万能保险）
	 */
	private String kindCode;

	/**
	 * 险种名称
	 */
	private String riskName;

	/**
	 * 险种分类（L--寿险(Life)、A--意外险(Accident)、H--健康险(Health)）
	 */
	private String riskType;

	/**
	 * 险种类别（L--长险(Long)、M--一年期险(Middle)、S--极短期险(Short)）
	 */
	private String riskPeriod;

	/**
	 * 险种分类1（默认为空 、 1--定额给付型 、 2--费用报销型 、 3--住院津贴型 、 4--委托管理型 、 5--社保补充型 、
	 * 6--意外保险）
	 */
	private String riskType1;

	/**
	 * 险种分类2（Y--打印年金领取年龄 、 N--不打印年金领取年龄）
	 */
	private String riskType2;

	/**
	 * 险种分类3（1 传统险 、 2 分红 、 3 投连 、 4 万能 、 5 其他）
	 */
	private String riskType3;

	/**
	 * 险种分类4（1 终身 、 2 两全及生存 、 3 定期 、 4 年金 、 5 重大疾病 、 6 意外 、 7 健康 、 8
	 * 短期综合（含意外和医疗） 、 9 其他）
	 */
	private String riskType4;

	/**
	 * 险种分类5（1 表示一年期及一年期以内 、 2 表示一年期以上）
	 */
	private String riskType5;

	/**
	 * 险种细分（A -意外伤害保险（普通意外险） 、 L -人寿保险（含定期） 、 M -医疗保险（指门诊/住院医疗险） 、 P -年金类保险 、
	 * Y-交通意外险 、 N-意外医疗险 、 J-津贴险（住院） 、 H-重疾险 、 YJ--意外津贴 、 D -定期疾病保险）
	 */
	private String riskTypeDetail;

	/**
	 * 加费系数
	 */
	private double premiumFactor;

	/**
	 * 份数
	 */
	private double quantity;

	/**
	 * 续期保费
	 */
	private double renewPremium;

	/**
	 * 险种是否在售
	 */
	private String saleStatus;

	/**
	 * 服务提醒方式
	 */
	private String serveiceWarnMethod;

	/**
	 * 停售日
	 */
	private Date stopSellDate;

	/**
	 * 险种版本号
	 */
	private String version;

	/**
	 * 单数(该险种是第几单)
	 */
	//private int num;

	/**
	 * 起售日
	 */
	private Date startSellDate;

	/**
	 * 基本保险金额
	 */
	private double basicCoverage;

	/**
	 * 交费期间
	 */
	private int feePeriod;

	/**
	 * 万能险首期追加保险费
	 */
	private double universalLifeAddPre;

	/**
	 * 万能险额外保险费
	 */
	private double universalLifeExtraPre;

	/**
	 * 万能险额外保险金额
	 */
	private double universalLifeExtraCov;

	/**
	 * 保险费合计
	 */
	private double sumPremium;

	/**
	 * 首期保险费支付方式
	 */
	private String firstPrePayMethod;

	/**
	 * 续期保险费支付方式
	 */
	private String afterPrePayMethod;

	/**
	 * 授权开户银行名称
	 */
	private String authBankName;

	/**
	 * 授权账户号
	 */
	private String authBankAccount;

	/**
	 * 养老金领取频率
	 */
	private String annuityFrequency;

	/**
	 * 养老金领取期间
	 */
	private int annuityPeriod;

	/**
	 * 是否自动续保（长期险）
	 */
	private boolean autoRenewedLong;

	/**
	 * 是否自动续保（短期险）
	 */
	private boolean autoRenewedShort;

	/**
	 * 续期缴费是否需要提示
	 */
	private boolean renewalFeePrompt;

	/**
	 * 保险费转账授权账户所有人
	 */
	private String premiumAuthAll;

	/**
	 * 续期转帐开户行
	 */
	private String renewalBankName;

	/**
	 * 续期帐户姓名
	 */
	private String renewalAccountName;

	/**
	 * 续期账号
	 */
	private String renewalBankAccount;

	/**
	 * 保险期间单位
	 */
	private String coveragePeriodUnit;

	/**
	 * 缴费期间单位
	 */
	private String feePeriodUnit;

	/**
	 * 红利领取人
	 */
	private String bonusReceiver;

	/**
	 * 年缴保费
	 */
	private double premiumPerYear;

	/**
	 * 缴费年限
	 */
	private int payYears;

	/**
	 * 累计保额
	 */
	private double sumCoverage;

	/**
	 * 累计追加保费
	 */
	private double sumAddPre;

	/**
	 * 趸交保费
	 */
	private double singlePremium;

	/**
	 * 期交保费
	 */
	private double periodPremium;

	/**
	 * 期交基本保险费之和
	 */
	private double sumBasicPremium;

	/**
	 * 产品组合编码
	 */
	private String prodSetCode;

	/**
	 * 六个月内投保次数
	 */
	private int insuredNum;

	/**
	 * 责任编码
	 */
	private String dutyCode;

	/**
	 * 险种性质
	 */
	private String riskProp;

	/**
	 * 险种审核标记
	 */
	private String adultflag;

	/**
	 * 终交年龄年期
	 */
	private int payendyear;

	/**
	 * 组合险种份数
	 * 
	 */

	private int sumMultByProdsetcode;

	/**
	 * 保险期间
	 */
	private int durationOfInsurance;

	/**
	 * 年缴保费2（非年交、季交、月交）
	 */
	private double riskDouble1;

	/**
	 * 预留字段2
	 */
	private double riskDouble2;

	/**
	 * 预留字段3
	 */
	private String riskString1;

	/**
	 * 预留字段4
	 */
	private String riskString2;
	
	/**
	 * 险种续保方式
	 */
	private String rnewFlag;
	/**
	 * 5021期交基本保险费之和
	 */
	private double sumBasicPremium5021;
	/**
	 * 险种编码
	 * @return
	 */
	private String riskNo;

	/**
	 * 测试字段：责任类别列表(H-重疾险,L-寿险,Y-意外险)
	 */
	private String dutyTypeList;

	/**
	 * 单期保费
	 */
	private double prem;

	/**
	 * 再保协议标记
	 */
	private boolean reNewPolFlag;

	/**
	 * 累计风险保额类型列表
	 */
	private String rebargainAccTypeList;


	public Risk() {
		super();
	}
	
	
	

	public String getRnewFlag() {
		return rnewFlag;
	}




	public void setRnewFlag(String rnewFlag) {
		this.rnewFlag = rnewFlag;
	}


	public boolean isReNewPolFlag() {return reNewPolFlag;}

	public void setReNewPolFlag(boolean reNewPolFlag) {this.reNewPolFlag = reNewPolFlag;}

	public String getRebargainAccTypeList() {return rebargainAccTypeList;}

	public void setRebargainAccTypeList(String rebargainAccTypeList) {this.rebargainAccTypeList = rebargainAccTypeList;}

	public double getBasicPremium() {
		return basicPremium;
	}

	public void setBasicPremium(double basicPremium) {
		this.basicPremium = basicPremium;
	}

	public double getEMValue() {
		return EMValue;
	}

	public void setEMValue(double eMValue) {
		EMValue = eMValue;
	}

	public boolean isAutoPayingInAdvance() {
		return autoPayingInAdvance;
	}

	public void setAutoPayingInAdvance(boolean autoPayingInAdvance) {
		this.autoPayingInAdvance = autoPayingInAdvance;
	}

	public String getBonusDrawType() {
		return bonusDrawType;
	}

	public void setBonusDrawType(String bonusDrawType) {
		this.bonusDrawType = bonusDrawType;
	}

	public int getCoveragePeriod() {
		return coveragePeriod;
	}

	public void setCoveragePeriod(int coveragePeriod) {
		this.coveragePeriod = coveragePeriod;
	}

	public int getDrawEndownentAge() {
		return drawEndownentAge;
	}

	public void setDrawEndownentAge(int drawEndownentAge) {
		this.drawEndownentAge = drawEndownentAge;
	}

	public String getEndowmentGetType() {
		return endowmentGetType;
	}

	public void setEndowmentGetType(String endowmentGetType) {
		this.endowmentGetType = endowmentGetType;
	}

	public String getEndowmentPayType() {
		return endowmentPayType;
	}

	public void setEndowmentPayType(String endowmentPayType) {
		this.endowmentPayType = endowmentPayType;
	}

	public String getInsuredFirstId() {
		return insuredFirstId;
	}

	public void setInsuredFirstId(String insuredFirstId) {
		this.insuredFirstId = insuredFirstId;
	}

	public String getInsuredSecondId() {
		return insuredSecondId;
	}

	public void setInsuredSecondId(String insuredSecondId) {
		this.insuredSecondId = insuredSecondId;
	}

	public boolean isBasicRisk() {
		return basicRisk;
	}

	public void setBasicRisk(boolean basicRisk) {
		this.basicRisk = basicRisk;
	}

	public String getPaymentFreqType() {
		return paymentFreqType;
	}

	public void setPaymentFreqType(String paymentFreqType) {
		this.paymentFreqType = paymentFreqType;
	}

	public String getRiskCode() {
		return riskCode;
	}

	public void setRiskCode(String riskCode) {
		this.riskCode = riskCode;
	}

	public String getRiskPeriod() {
		return riskPeriod;
	}

	public void setRiskPeriod(String riskPeriod) {
		this.riskPeriod = riskPeriod;
	}

	public String getRiskName() {
		return riskName;
	}

	public void setRiskName(String riskName) {
		this.riskName = riskName;
	}

	public String getRiskType() {
		return riskType;
	}

	public void setRiskType(String riskType) {
		this.riskType = riskType;
	}

	public double getPremiumFactor() {
		return premiumFactor;
	}

	public void setPremiumFactor(double premiumFactor) {
		this.premiumFactor = premiumFactor;
	}

	public double getQuantity() {
		return quantity;
	}

	public void setQuantity(double quantity) {
		this.quantity = quantity;
	}

	public double getRenewPremium() {
		return renewPremium;
	}

	public void setRenewPremium(double renewPremium) {
		this.renewPremium = renewPremium;
	}

	public String getSaleStatus() {
		return saleStatus;
	}

	public void setSaleStatus(String saleStatus) {
		this.saleStatus = saleStatus;
	}

	public String getServeiceWarnMethod() {
		return serveiceWarnMethod;
	}

	public void setServeiceWarnMethod(String serveiceWarnMethod) {
		this.serveiceWarnMethod = serveiceWarnMethod;
	}

	public Date getStopSellDate() {
		return stopSellDate;
	}

	public void setStopSellDate(Date stopSellDate) {
		this.stopSellDate = stopSellDate;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

//	public int getNum() {
//		return num;
//	}
//
//	public void setNum(int num) {
//		this.num = num;
//	}

	public Date getStartSellDate() {
		return startSellDate;
	}

	public void setStartSellDate(Date startSellDate) {
		this.startSellDate = startSellDate;
	}

	public double getBasicCoverage() {
		return basicCoverage;
	}

	public void setBasicCoverage(double basicCoverage) {
		this.basicCoverage = basicCoverage;
	}

	public int getFeePeriod() {
		return feePeriod;
	}

	public void setFeePeriod(int feePeriod) {
		this.feePeriod = feePeriod;
	}

	public double getUniversalLifeAddPre() {
		return universalLifeAddPre;
	}

	public void setUniversalLifeAddPre(double universalLifeAddPre) {
		this.universalLifeAddPre = universalLifeAddPre;
	}

	public double getUniversalLifeExtraPre() {
		return universalLifeExtraPre;
	}

	public void setUniversalLifeExtraPre(double universalLifeExtraPre) {
		this.universalLifeExtraPre = universalLifeExtraPre;
	}

	public double getUniversalLifeExtraCov() {
		return universalLifeExtraCov;
	}

	public void setUniversalLifeExtraCov(double universalLifeExtraCov) {
		this.universalLifeExtraCov = universalLifeExtraCov;
	}

	public double getSumPremium() {
		return sumPremium;
	}

	public void setSumPremium(double sumPremium) {
		this.sumPremium = sumPremium;
	}

	public String getFirstPrePayMethod() {
		return firstPrePayMethod;
	}

	public void setFirstPrePayMethod(String firstPrePayMethod) {
		this.firstPrePayMethod = firstPrePayMethod;
	}

	public String getAfterPrePayMethod() {
		return afterPrePayMethod;
	}

	public void setAfterPrePayMethod(String afterPrePayMethod) {
		this.afterPrePayMethod = afterPrePayMethod;
	}

	public String getAuthBankName() {
		return authBankName;
	}

	public void setAuthBankName(String authBankName) {
		this.authBankName = authBankName;
	}

	public String getAuthBankAccount() {
		return authBankAccount;
	}

	public void setAuthBankAccount(String authBankAccount) {
		this.authBankAccount = authBankAccount;
	}

	public String getAnnuityFrequency() {
		return annuityFrequency;
	}

	public void setAnnuityFrequency(String annuityFrequency) {
		this.annuityFrequency = annuityFrequency;
	}

	public double getAnnuityPeriod() {
		return annuityPeriod;
	}

	public void setAnnuityPeriod(int annuityPeriod) {
		this.annuityPeriod = annuityPeriod;
	}

	public boolean isAutoRenewedLong() {
		return autoRenewedLong;
	}

	public void setAutoRenewedLong(boolean autoRenewedLong) {
		this.autoRenewedLong = autoRenewedLong;
	}

	public boolean isAutoRenewedShort() {
		return autoRenewedShort;
	}

	public void setAutoRenewedShort(boolean autoRenewedShort) {
		this.autoRenewedShort = autoRenewedShort;
	}

	public boolean isRenewalFeePrompt() {
		return renewalFeePrompt;
	}

	public void setRenewalFeePrompt(boolean renewalFeePrompt) {
		this.renewalFeePrompt = renewalFeePrompt;
	}

	public String getPremiumAuthAll() {
		return premiumAuthAll;
	}

	public void setPremiumAuthAll(String premiumAuthAll) {
		this.premiumAuthAll = premiumAuthAll;
	}

	public String getRenewalBankName() {
		return renewalBankName;
	}

	public void setRenewalBankName(String renewalBankName) {
		this.renewalBankName = renewalBankName;
	}

	public String getRenewalAccountName() {
		return renewalAccountName;
	}

	public void setRenewalAccountName(String renewalAccountName) {
		this.renewalAccountName = renewalAccountName;
	}

	public String getRenewalBankAccount() {
		return renewalBankAccount;
	}

	public void setRenewalBankAccount(String renewalBankAccount) {
		this.renewalBankAccount = renewalBankAccount;
	}

	public String getCoveragePeriodUnit() {
		return coveragePeriodUnit;
	}

	public void setCoveragePeriodUnit(String coveragePeriodUnit) {
		this.coveragePeriodUnit = coveragePeriodUnit;
	}

	public String getFeePeriodUnit() {
		return feePeriodUnit;
	}

	public void setFeePeriodUnit(String feePeriodUnit) {
		this.feePeriodUnit = feePeriodUnit;
	}

	public String getBonusReceiver() {
		return bonusReceiver;
	}

	public void setBonusReceiver(String bonusReceiver) {
		this.bonusReceiver = bonusReceiver;
	}

	public double getPremiumPerYear() {
		return premiumPerYear;
	}

	public void setPremiumPerYear(double premiumPerYear) {
		this.premiumPerYear = premiumPerYear;
	}

	public int getPayYears() {
		return payYears;
	}

	public void setPayYears(int payYears) {
		this.payYears = payYears;
	}

	public double getSumCoverage() {
		return sumCoverage;
	}

	public void setSumCoverage(double sumCoverage) {
		this.sumCoverage = sumCoverage;
	}

	public double getSumAddPre() {
		return sumAddPre;
	}

	public void setSumAddPre(double sumAddPre) {
		this.sumAddPre = sumAddPre;
	}

	public String getRiskType1() {
		return riskType1;
	}

	public void setRiskType1(String riskType1) {
		this.riskType1 = riskType1;
	}

	public String getRiskType2() {
		return riskType2;
	}

	public void setRiskType2(String riskType2) {
		this.riskType2 = riskType2;
	}

	public String getRiskType3() {
		return riskType3;
	}

	public void setRiskType3(String riskType3) {
		this.riskType3 = riskType3;
	}

	public String getRiskType4() {
		return riskType4;
	}

	public void setRiskType4(String riskType4) {
		this.riskType4 = riskType4;
	}

	public String getRiskType5() {
		return riskType5;
	}

	public void setRiskType5(String riskType5) {
		this.riskType5 = riskType5;
	}

	public String getKindCode() {
		return kindCode;
	}

	public void setKindCode(String kindCode) {
		this.kindCode = kindCode;
	}

	public String getRiskTypeDetail() {
		return riskTypeDetail;
	}

	public void setRiskTypeDetail(String riskTypeDetail) {
		this.riskTypeDetail = riskTypeDetail;
	}

	public int getInsuredNum() {
		return insuredNum;
	}

	public void setInsuredNum(int insuredNum) {
		this.insuredNum = insuredNum;
	}

	public String getProdSetCode() {
		return prodSetCode;
	}

	public void setProdSetCode(String prodSetCode) {
		this.prodSetCode = prodSetCode;
	}

	public double getSumBasicPremium() {
		return sumBasicPremium;
	}

	public void setSumBasicPremium(double sumBasicPremium) {
		this.sumBasicPremium = sumBasicPremium;
	}

	public String getDutyCode() {
		return dutyCode;
	}

	public void setDutyCode(String dutyCode) {
		this.dutyCode = dutyCode;
	}

	public double getPeriodPremium() {
		return periodPremium;
	}

	public void setPeriodPremium(double periodPremium) {
		this.periodPremium = periodPremium;
	}

	public double getSinglePremium() {
		return singlePremium;
	}

	public void setSinglePremium(double singlePremium) {
		this.singlePremium = singlePremium;
	}

	public String getRiskProp() {
		return riskProp;
	}

	public void setRiskProp(String riskProp) {
		this.riskProp = riskProp;
	}

	public String getAdultflag() {
		return adultflag;
	}

	public void setAdultflag(String adultflag) {
		this.adultflag = adultflag;
	}

	public int getPayendyear() {
		return payendyear;
	}

	public void setPayendyear(int payendyear) {
		this.payendyear = payendyear;
	}

	public int getSumMultByProdsetcode() {
		return sumMultByProdsetcode;
	}

	public void setSumMultByProdsetcode(int sumMultByProdsetcode) {
		this.sumMultByProdsetcode = sumMultByProdsetcode;
	}

	public int getDurationOfInsurance() {
		return durationOfInsurance;
	}

	public void setDurationOfInsurance(int durationOfInsurance) {
		this.durationOfInsurance = durationOfInsurance;
	}

	public double getRiskDouble1() {
		return riskDouble1;
	}

	public void setRiskDouble1(double riskDouble1) {
		this.riskDouble1 = riskDouble1;
	}

	public double getRiskDouble2() {
		return riskDouble2;
	}

	public void setRiskDouble2(double riskDouble2) {
		this.riskDouble2 = riskDouble2;
	}

	public String getRiskString1() {
		return riskString1;
	}

	public void setRiskString1(String riskString1) {
		this.riskString1 = riskString1;
	}

	public String getRiskString2() {
		return riskString2;
	}

	public void setRiskString2(String riskString2) {
		this.riskString2 = riskString2;
	}

	public double getSumBasicPremium5021() {
		return sumBasicPremium5021;
	}

	public void setSumBasicPremium5021(double sumBasicPremium5021) {
		this.sumBasicPremium5021 = sumBasicPremium5021;
	}

	public String getRiskNo() {
		return riskNo;
	}

	public void setRiskNo(String riskNo) {
		this.riskNo = riskNo;
	}

	public String getDutyTypeList() {
		return dutyTypeList;
	}

	public void setDutyTypeList(String dutyTypeList) {
		this.dutyTypeList = dutyTypeList;
	}

	public double getPrem() {
		return prem;
	}

	public void setPrem(double prem) {
		this.prem = prem;
	}
}
