package com.sinosoft.cloud.cbs.zbx.bom;

import com.sinosoft.cloud.cbs.rules.bom.AccidentResponse;
import com.sinosoft.cloud.cbs.rules.bom.MajorDiseaseCheck;
import org.apache.catalina.LifecycleState;

import java.util.List;

/**
 * Created by zkr on 2019/4/18.
 */
public class AccidentData {
    private List<AccidentResponse> accidentResponseList;
    private List<MajorDiseaseCheck> majorDiseaseCheckList;

    public List<AccidentResponse> getAccidentResponseList() {
        return accidentResponseList;
    }

    public void setAccidentResponseList(List<AccidentResponse> accidentResponseList) {
        this.accidentResponseList = accidentResponseList;
    }

    public List<MajorDiseaseCheck> getMajorDiseaseCheckList() {
        return majorDiseaseCheckList;
    }

    public void setMajorDiseaseCheckList(List<MajorDiseaseCheck> majorDiseaseCheckList) {
        this.majorDiseaseCheckList = majorDiseaseCheckList;
    }

    @Override
    public String toString() {
        return "AccidentData{" +
                "accidentResponseList=" + accidentResponseList +
                ", majorDiseaseCheckList=" + majorDiseaseCheckList +
                '}';
    }
}
