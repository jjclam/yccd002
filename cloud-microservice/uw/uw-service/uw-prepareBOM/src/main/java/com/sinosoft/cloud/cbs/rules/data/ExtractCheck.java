package com.sinosoft.cloud.cbs.rules.data;
;
import com.sinosoft.cloud.cbs.rules.bom.Applicant;
import com.sinosoft.cloud.cbs.rules.bom.Policy;
import com.sinosoft.cloud.cbs.rules.result.Result;
import com.sinosoft.cloud.rest.TradeInfo;
import com.sinosoft.lis.entity.LCAddressPojo;
import com.sinosoft.lis.entity.LCContPojo;
import com.sinosoft.utility.*;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * @Author: zhuyiming
 * @Description: 提取查重数据
 * @Date: Created in 20:03 2017/9/24
 */
@Service
public class ExtractCheck {
	private final Log logger = LogFactory.getLog(this.getClass());
	@Value("${cloud.uw.barrier.control}")
	private String barrier;

	public void getCheck(TradeInfo tradeInfo, Policy policy) {
		// 投保人信息
		Applicant applicantInfo = policy.getApplicant();
		//if("true".equals(barrier)){
			getOtherData(applicantInfo);
//			logger.debug("保单号：" + policy.getContNo() + "，查重挡板启动！");
//			return;
//		}
//		//投保人地址信息
//		LCAddressPojo lcAddressPojo = ((List<LCAddressPojo>) tradeInfo.getData(LCAddressPojo.class.getName())).get(0);
//		//电话集合
//		List<String> phones = new ArrayList<>();
//
//		// 投保人ID
//		String appntno = applicantInfo.getClientNo();
//		//投保人姓名
//        String appntname = applicantInfo.getApplicantName();
//		//投保人通讯电话
//		String phone = lcAddressPojo.getPhone();
//		if(phone != null && !"".equals(phone)){
//			phones.add(phone);
//		}
//		//投保人家庭电话
//		String homePhone = lcAddressPojo.getHomePhone();
//		if(homePhone != null && !"".equals(homePhone)){
//			phones.add(homePhone);
//		}
//		//投保人单位电话
//		String companyPhone = lcAddressPojo.getCompanyPhone();
//		if(companyPhone != null && !"".equals(companyPhone)){
//			phones.add(companyPhone);
//		}
//		//投保人手机号
//		String mobile = lcAddressPojo.getMobile();
//		if(mobile != null && !"".equals(mobile)){
//			phones.add(mobile);
//		}
//
//		VData tVData = new VData();
//		TransferData tParam = new TransferData();
//		StringBuffer sql = new StringBuffer();
//		if(phones.size()>0){
//			sql
//					.append("select count(distinct customerno) as field1, 1 as flag from lcaddress where exists (select 'x' from lcappnt where appntno = lcaddress.customerno and addressno=lcaddress.addressno) and customerno != ? and (mobile in (?, ?, ?, ?) or companyphone in (?, ?, ?, ?) or homephone in (?, ?, ?, ?) or phone in (?, ?, ?, ?))");
/*			sql.append(" UNION ");
			sql
					.append("SELECT COUNT(1) as field1, 4 as flag FROM LAAGENT WHERE OUTWORKDATE IS NULL AND NAME <> ? AND (MOBILE IN (?, ?, ?, ?) OR PHONE IN (?, ?, ?, ?))");*/
/*
		sql
				.append(" SELECT 1 as field1, 1 as flag FROM LDSYSVAR WHERE SYSVAR = 'onerow' AND PHOREGONIZE2((SELECT APPNTNO FROM LCAPPNT WHERE PRTNO = ?),?)>1);
		sql
				.append(" SELECT 1 as field1, 2 as flag FROM DUAL WHERE PHOREGONIZE4(?)>=3 ");
		sql.append(" UNION ");
		sql
				.append(" select 1 as field1, 3 as flag from ldsysvar where sysvar='onerow' AND phoregonize(?,?)>0");
		sql.append(" UNION ");
		sql
				.append(" SELECT 1 as field1, 4 as flag FROM LDSYSVAR WHERE SYSVAR = 'onerow' AND PHOREGONIZE3((SELECT APPNTNO FROM LCAPPNT WHERE PRTNO =  ?),?) > 0");
*/

//			tVData.add(sql.toString());
//			tVData.add(tParam);
//			tParam.setNameAndValue("CustomerNo", "string:" + appntno);
//
//			for(int j=0;j<4;j++) {
//				for (int i = 0; i < 4; i++) {
//					if (i < phones.size()) {
//						tParam.setNameAndValue("phone", "string:" + phones.get(i));
//					} else {
//						tParam.setNameAndValue("phone", "string:" + phones.get(0));
//					}
//				}
//			}
			/*tParam.setNameAndValue("appntname", "string:" + appntname);

			for(int j=0;j<2;j++) {
				for (int i = 0; i < 4; i++) {
					if (i < phones.size()) {
						tParam.setNameAndValue("phone", "string:" + phones.get(i));
					} else {
						tParam.setNameAndValue("phone", "string:" + phones.get(0));
					}
				}
			}*/
//			logger.debug("CheckInfo：" + sql);
//			long beginTime = System.currentTimeMillis();
//			SSRS tSSRS = new SSRS();
//			Connection tConnection = null;
//			try {
//				tConnection = DBConnPool.getConnection("basedataSource");
//				ExeSQL exeSql = new ExeSQL(tConnection);
//				tSSRS = exeSql.execSQL(tVData);
//			}catch (Exception e){
//				e.printStackTrace();
//				logger.error(ExceptionUtils.exceptionToString(e));
//				tradeInfo.addError(ExceptionUtils.exceptionToString(e));
//				return;
//			}finally {
//				if (tConnection != null) {
//					try {
//						tConnection.close();
//					} catch (SQLException e) {
//						e.printStackTrace();
//						tradeInfo.addError(ExceptionUtils.exceptionToString(e));
//						logger.error(ExceptionUtils.exceptionToString(e));
//						return;
//					}
//				}
//			}
//
//			LCContPojo lcContPojo = (LCContPojo) tradeInfo.getData(LCContPojo.class.getName());
//			if(tSSRS == null){
//				logger.debug("保单号：" + lcContPojo.getContNo() + "，查重访问老核心SQL发生异常！");
//				tradeInfo.addError("保单号：" + lcContPojo.getContNo() + "，查重访问老核心SQL发生异常！");
//				return;
//			}
//			logger.debug("查重提数：" + (System.currentTimeMillis() - beginTime) / 1000.0
//					+ "s");
//			int flag = 0;
//			if (null != tSSRS && tSSRS.MaxRow > 0) {
//				for (int i = 1; i <= tSSRS.MaxRow; i++) {
//					flag = Util.toInt(tSSRS.GetText(i, 2));
//					int result = Util.toInt(tSSRS.GetText(i, 1));
//					switch (flag) {
//						case 1:
//							if (result > 0) {
//								// 可疑电话
//								applicantInfo.setQuestionTel(true);
//								// 投保人不同电话相同
//								applicantInfo.setAppntPhoneDiff(true);
//							}
//							if (result >= 2) {
//								// 3个（含3）以上不同投保人存在相同电话
//								applicantInfo.setMoreAppntTelDiff(true);
//
//								// 3个（含3）以上客户联系电话和首选回访电话中有相同号码
//								applicantInfo.setMoreAppntTelSame(true);
//								// 投保人不同联系电话相同
//								applicantInfo.setAppntTelDiff(true);
//							}
//							break;
//						/*case 4:
//							if (result > 0) {
//								// 投保人非代理人但联系电话相同
//								applicantInfo.setAppntAgentDiff(true);
//							}
//							break;*/
//					}
//				}
//			}
//		}
	}

	public void getOtherData(Applicant applicantInfo) {
		// 可疑电话
		applicantInfo.setQuestionTel(false);
		// 投保人不同电话相同
		applicantInfo.setAppntPhoneDiff(false);
		// 3个（含3）以上不同投保人存在相同电话
		applicantInfo.setMoreAppntTelDiff(false);
		// 3个（含3）以上客户联系电话和首选回访电话中有相同号码
		applicantInfo.setMoreAppntTelSame(false);
		// 投保人不同联系电话相同
		applicantInfo.setAppntTelDiff(false);
		// 投保人非代理人但联系电话相同
		applicantInfo.setAppntAgentDiff(false);
	}
}
