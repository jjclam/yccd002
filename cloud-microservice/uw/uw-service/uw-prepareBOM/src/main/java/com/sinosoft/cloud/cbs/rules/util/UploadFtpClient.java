package com.sinosoft.cloud.cbs.rules.util;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

import com.sinosoft.utility.ExceptionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.net.ftp.FTPClient;

/**
 * FTP客户端
 *
 */
public class UploadFtpClient {
	private static Log logger = LogFactory.getLog(UploadFtpClient.class);
	/**
	 * 
	 * @param fileContent 文件内容
	 * @param server 服务器IP
	 * @param userName 用户名
	 * @param userPassword 密码
	 * @param path 上传路径
	 * @param fileName 文件名
	 * @return
	 */
	public static boolean uploadFile(StringBuffer fileContent, String server, String userName, String userPassword, String path, String fileName) {
		FTPClient ftpClient = new FTPClient();
		try {
			ftpClient.connect(server);
			boolean flag = ftpClient.login(userName, userPassword);
			if(flag) {
				//一级一级目录进入，如果没有，则创建该目录
				flag = cdDirAndMakeDir(ftpClient, path);
				if(flag) {
					InputStream is = new ByteArrayInputStream(fileContent.toString().getBytes("UTF-8"));
					ftpClient.setFileType(FTPClient.BINARY_FILE_TYPE);
					fileName += ".xml";
					//创建文件
					flag = ftpClient.storeFile(new String(fileName.getBytes("UTF-8"),"ISO-8859-1"), is);
					if(flag) {
						logger.debug("RULES*FTP在"+ path + "目录下生成文件：" + fileName);
					}else {
						throw new Exception("RULES*FTP生成文件失败");
					}
					is.close();
				}else {
					logger.error("RULES*FTP生成目录，进入目录失败，FTP可能没有相应权限");
					throw new Exception("RULES*FTP生成目录，进入目录失败，FTP可能没有相应权限");
				}
			} else {
				logger.error("RULES*FTP连接错误");

				throw new Exception("RULES*FTP连接错误");
			}
			if(ftpClient.isConnected()) {
				try {
					ftpClient.disconnect();
				} catch (Exception e) {
					e.printStackTrace();
					logger.error(ExceptionUtils.exceptionToString(e));
				}
			}
		}catch(Exception e) {
			e.printStackTrace();
			logger.error(ExceptionUtils.exceptionToString(e));
			return false;
		}
		return true;
	}
	
	public static boolean cdDirAndMakeDir(FTPClient ftpClient, String paths) {
		String[] pathArray = paths.split("/");
		for(int i=0; i<pathArray.length; i++) {
			String path = pathArray[i];
			if(path.equals("")) {
				continue;
			}
			boolean flag = false;
			try {
				flag = ftpClient.changeWorkingDirectory(path);
				if(!flag) {
					ftpClient.makeDirectory(path);
					flag = ftpClient.changeWorkingDirectory(path);
					//创建了目录后，切换目录，如果还是返回false，则说明没有权限
					if(!flag) {
						return false;
					}
				}
			} catch (IOException e) {
				e.printStackTrace();
				logger.error(ExceptionUtils.exceptionToString(e));
			}
		}
		return true;
	}
}
