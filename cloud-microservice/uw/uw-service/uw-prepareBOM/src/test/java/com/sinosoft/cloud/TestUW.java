package com.sinosoft.cloud;

import com.sinosoft.cloud.cbs.function.NewUWFunction;
import com.sinosoft.cloud.cbs.uw.PrepareBOMService;
import com.sinosoft.cloud.rest.TradeInfo;
import com.sinosoft.lis.entity.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by zkr on 2018/6/26.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes= UWApplication.class)
public class TestUW {
//    @Autowired
//    PrepareBOMService prepareBOMService;
    @Autowired
    NewUWFunction  newUWFunction;
//    @Test
//    public void testA(){
//      prepareBOMService.dealData(prepare());
//    }
    @Test
    public void testB(){
      newUWFunction.dealDate(prepare());
        System.out.println("11111111");

    }


    private TradeInfo prepare(){
        TradeInfo tradeInfo=new TradeInfo();
        LCPolPojo lcPolPojo=new LCPolPojo();
        List<LCPolPojo> lcPolPojos=new ArrayList<LCPolPojo>();
        lcPolPojo.setPolNo("000554367522026");
        lcPolPojo.setContNo("200943060020161540");
        lcPolPojo.setAmnt("100");
        lcPolPojo.setPrem("100");
        lcPolPojo.setInsuredNo("0001671716");
        lcPolPojo.setUWFlag("5");
        lcPolPojo.setAppFlag("2");
        lcPolPojo.setAppntNo("0001671716");
        lcPolPojo.setMainPolNo("000554367522026");
        lcPolPojo.setRiskCode("2012");
        lcPolPojo.setContType("L");
        lcPolPojo.setInsuredBirthday("1990-02-02");
        lcPolPojos.add(lcPolPojo);
        tradeInfo.addData(LCPolPojo.class.getName(),lcPolPojos);

        LCContPojo lcContPojo=new LCContPojo();
        lcContPojo.setContNo("200943060020161540");
        lcContPojo.setInsuredNo("0001671716");
        lcContPojo.setState("1004");
        lcContPojo.setAmnt("100");
        lcContPojo.setPrem("100");
        lcContPojo.setAgentCode("0001671716");
        lcContPojo.setManageCom("34322233");
        lcContPojo.setAgentBankCode("988988");
        lcContPojo.setBankAgent("33434333");
        lcContPojo.setSaleChnl("5");
        tradeInfo.addData(LCContPojo.class.getName(),lcContPojo);

        LCPremPojo lcPremPojo=new LCPremPojo();
        List<LCPremPojo> lcPremPojos=new ArrayList<LCPremPojo>();
        lcPremPojo.setContNo("200943060020161540");
        lcPremPojo.setPolNo("000554367522026");
        lcPremPojo.setPrem("100");
        lcPremPojos.add(lcPremPojo);
        tradeInfo.addData(LCPremPojo.class.getName(),lcPremPojos);

        LCDutyPojo lcDutyPojo=new LCDutyPojo();
        List<LCDutyPojo> lcDutyPojos=new ArrayList<LCDutyPojo>();
        lcDutyPojo.setContNo("200943060020161540");
        lcDutyPojo.setPolNo("000554367522026");
        lcDutyPojo.setDutyCode("202401");
        lcDutyPojo.setAmnt("100");
        lcDutyPojo.setPrem("100");
        lcDutyPojos.add(lcDutyPojo);
        tradeInfo.addData(LCDutyPojo.class.getName(),lcDutyPojos);

        LCAppntPojo lcAppntPojo=new LCAppntPojo();
        // List<LCAppntPojo> lcAppntPojos=new ArrayList<LCAppntPojo>();
        lcAppntPojo.setAppntNo("0001671716");
        lcAppntPojo.setContNo("200943060020161540");
        lcAppntPojo.setSalary("100");
        lcAppntPojo.setAppntName("测试项");
        lcAppntPojo.setAppntSex("0");
        lcAppntPojo.setPersonID("0");
        lcAppntPojo.setIDType("0");
        lcAppntPojo.setAppntBirthday("1990-02-02");
        lcAppntPojo.setIDNo("412725199002021152");
        //lcAppntPojos.add(lcAppntPojo);
        tradeInfo.addData(LCAppntPojo.class.getName(),lcAppntPojo);

        //LCInsuredPojo
        LCInsuredPojo lcInsuredPojo=new LCInsuredPojo();
        lcInsuredPojo.setAppntNo("0001671716");
        lcInsuredPojo.setContNo("200943060020161540");
        lcInsuredPojo.setAccName("测试项");
        lcInsuredPojo.setName("测试");
        lcInsuredPojo.setInsuredNo("0001671716");
        lcInsuredPojo.setIDType("0");
        lcInsuredPojo.setIDNo("412725199002021152");
        lcInsuredPojo.setSex("0");
        lcInsuredPojo.setBirthday("1990-02-02");
        List<LCInsuredPojo> lcInsuredPojos=new ArrayList<LCInsuredPojo>();
        lcInsuredPojos.add(lcInsuredPojo);
        tradeInfo.addData(LCInsuredPojo.class.getName(),lcInsuredPojos);

        //lcAddressPojo
        LCAddressPojo lcAddressPojo=new LCAddressPojo();
        lcAddressPojo.setPersonID("0001671716");
        lcAddressPojo.setCustomerNo("0001671716");
        lcAddressPojo.setAddressNo("232");
        lcAddressPojo.setCity("北京");
        lcAddressPojo.setProvince("北京");
        lcAddressPojo.setHomePhone("111111");
        lcAddressPojo.setCompanyPhone("2222222");
        lcAddressPojo.setMobile("33333333");
        lcAddressPojo.setPhone("44444444");
        List<LCAddressPojo> lcAddressPojos=new ArrayList<LCAddressPojo>();
        lcAddressPojos.add(lcAddressPojo);
        tradeInfo.addData(LCAddressPojo.class.getName(),lcAddressPojos);

        //LCInsureAccPojo
        LCInsureAccPojo lcInsureAccPojo=new LCInsureAccPojo();
        List<LCInsureAccPojo> lcInsureAccPojos=new ArrayList<LCInsureAccPojo>();
        lcInsureAccPojo.setContNo("200943060020161540");
        lcInsureAccPojo.setAppntNo("0001671716");
        lcInsureAccPojo.setInsuredNo("0001671716");
        lcInsureAccPojo.setGrpPolNo("00000000000000000000");
        lcInsureAccPojo.setRiskCode("2012");
        lcInsureAccPojo.setAccType("L");
        lcInsureAccPojos.add(lcInsureAccPojo);
        tradeInfo.addData(LCInsureAccPojo.class.getName(),lcInsureAccPojos);
        tradeInfo.addData("ContNo","200943060020161540");
       // tradeInfo.addData("CustomerNo","0001671716");
        //tradeInfo.addData("PojoName",LRAppntPojo.class.getName());

        //LCCustomerImpartParamsPojo
        LCCustomerImpartParamsPojo lcCustomerImpartParamsPojo=new LCCustomerImpartParamsPojo();
        lcCustomerImpartParamsPojo.setContNo("200943060020161540");
        lcCustomerImpartParamsPojo.setCustomerImpartDetailID("200943060020161540");
        lcCustomerImpartParamsPojo.setCustomerImpartParamsID("200943060020161540");
        lcCustomerImpartParamsPojo.setCustomerNo("0001671716");
        lcCustomerImpartParamsPojo.setImpartCode("0001671716");
        lcCustomerImpartParamsPojo.setImpartParam("0001671716");
        lcCustomerImpartParamsPojo.setImpartParamNo("0001671716");
        lcCustomerImpartParamsPojo.setMakeDate("2012-05-06");
        lcCustomerImpartParamsPojo.setPrtNo("200943060020161540");
        lcCustomerImpartParamsPojo.setShardingID("0001671716");
        List<LCCustomerImpartParamsPojo> lcCustomerImpartParamsPojos=new ArrayList<LCCustomerImpartParamsPojo>();
        lcCustomerImpartParamsPojos.add(lcCustomerImpartParamsPojo);
        tradeInfo.addData(LCCustomerImpartParamsPojo.class.getName(),lcCustomerImpartParamsPojos);

        //GlobalPojo
        GlobalPojo globalPojo=new GlobalPojo();
        globalPojo.setSerialNo("200943060020161540");
        globalPojo.setBrNo("200943060020161540");
        globalPojo.setBankCode("1111");
        globalPojo.setZoneNo("1110");
        globalPojo.setOldPolicy("200943060020161540");
        tradeInfo.addData(GlobalPojo.class.getName(),globalPojo);
        return tradeInfo;
    }
}
