package com.sinosoft.cloud;

import com.sinosoft.cloud.cbs.uw.PrepareBOMService;
import com.sinosoft.cloud.cbs.uw.TUWByEngineBL;
import com.sinosoft.cloud.cbs.uw.UpdateSecContactService;
import com.sinosoft.cloud.rest.TradeInfo;
import com.sinosoft.lis.entity.LCBnfPojo;
import com.sinosoft.lis.entity.LCInsuredPojo;
import com.sinosoft.lis.entity.LCResultInfoPojo;
import com.sinosoft.utility.Reflections;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.lang.reflect.Method;
import java.util.*;

/**
 * @Author:wangshuliang
 * @Description:
 * @Date:Created in 13:44 2018/6/11
 * @Modified by:
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = UWApplication.class)
public class TAutoTest {
    @Autowired
    private PrepareBOMService prepareBOMService;
    private final Log logger = LogFactory.getLog(this.getClass());


//    @Autowired
//    private UpdateSecContactService updateSecContactService;

    public static void main(String args[]) {
        String xml = "<LcInsured>";
        xml = xml.replaceFirst("<LcInsured>", "<par:LcInsured><LcInsured>");
        System.out.print(xml);
    }

    @Test
    public void getXml() {
        TradeInfo tradeInfo = new TradeInfo();
        prepareData(tradeInfo);
//        List<LCInsuredPojo> data = (List<LCInsuredPojo>) tradeInfo.getData(LCInsuredPojo.class.getName());
//        LCInsuredPojo lcInsuredPojo = data.get(0);
//        LCBnfPojo lcBnfPojo = new LCBnfPojo();
//        lcBnfPojo.setInsuredNo("1111");
//        lcBnfPojo.setBnfType("1");
//        lcBnfPojo.setBnfNo("1");
//        lcBnfPojo.setBnfGrade("1");
//        lcBnfPojo.setRelationToInsured("00");
//        lcBnfPojo.setCustomerNo("11111");
//        lcBnfPojo.setName(lcInsuredPojo.getName());
//        lcBnfPojo.setSex(lcInsuredPojo.getSex());
//        lcBnfPojo.setIDNo(lcInsuredPojo.getIDNo());
//        lcBnfPojo.setIDType(lcInsuredPojo.getIDType());
//        lcBnfPojo.setBirthday(lcInsuredPojo.getBirthday());
//        List<LCBnfPojo> objects = new ArrayList<>();
//        objects.add(lcBnfPojo);
        //tradeInfo.addData(LCBnfPojo.class.getName(),objects);
        prepareBOMService.service(tradeInfo);
        logger.debug(tradeInfo);
        List<LCResultInfoPojo> lcResultInfoPojo = (List<LCResultInfoPojo>) tradeInfo.getData(LCResultInfoPojo.class.getName());
        if (lcResultInfoPojo != null && lcResultInfoPojo.size() > 0) {
            logger.debug(lcResultInfoPojo.toString());
            for (LCResultInfoPojo lc : lcResultInfoPojo) {
                logger.debug("核保被校验住规则有 规则编码为:"+lc.getResultNo());
                logger.debug("核保被校验住规则有:"+lc.getResultContent());
            }
            logger.debug("核保被校验住。。。");
        } else {
            logger.debug("核保成功。。 。。。");
        }

    }
    @Test
    public void getXmlQQ() {
        TradeInfo tradeInfo = new TradeInfo();
        prepareData(tradeInfo);
     //   updateSecContactService.service(tradeInfo);


    }
    public void prepareData(TradeInfo tradeInfo) {
        String str = prepareStr();
        Reflections reflections = new Reflections();
        //截取trademap数据
        str = str.substring(str.indexOf("tradeMap={") + 10, str.indexOf("}"));
        String[] objs = str.split("com.sinosoft.lis.entity.");
        List<String> arrs = Arrays.asList(objs);
        int start = -1;
        int end = -1;
        int eq = -1;
        Map<String, String> map = new HashMap();
        //解析tradeinfo
        for (int i = 0; i < arrs.size(); i++) {
            String arr = arrs.get(i);
            start = arr.indexOf("[");
            end = arr.lastIndexOf("]");
            eq = arr.indexOf("=");
            if (start != -1 && end != -1 && eq != -1) {
                map.put(arr.substring(0, eq).trim(), arr.substring(start + 1, end));
            }
            start = arr.indexOf("{");
            end = arr.indexOf("}");
            if (start != -1 && end != -1 && end == arr.lastIndexOf("}")) {

            }
        }
        //解析类
        String value = null;
        Method setMethod = null;
        Class className = null;
        /*Field field = null;*/
        Object obj = null;
        for (String key : map.keySet()) {
            value = map.get(key);
            if ("".equals(value)) {
                continue;
            }
            //指定类
            try {
                className = Class.forName("com.sinosoft.lis.entity." + key);
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }

            start = value.indexOf("[");
            end = value.indexOf("]");
            //解析value中有几个[]，判断是集合对象还是类对象
            if (start == -1) {
                obj = analyseData(value, className, obj, setMethod);
                tradeInfo.addData(className.getName(), obj);
            } else {
                //判断是否存在字符串格式的数据
                if (value.lastIndexOf("]") != value.length() - 1) {
                    String otherVal = value.substring(value.lastIndexOf("]") + 2);
                    for (String val : otherVal.split(",")) {
                        tradeInfo.addData(val.split("=")[0].trim(), val.split("=")[1].trim());
                    }
                }
                //集合对象遍历
                List classList = new ArrayList();
                while (start != -1) {
                    classList.add(analyseData(value.substring(start + 1, end), className, obj, setMethod));
                    value = value.substring(end + 1);
                    start = value.indexOf("[");
                    end = value.indexOf("]");
                }
                if (classList.size() != 0) {
                    tradeInfo.addData(className.getName(), classList);
                }
            }
        }
        System.out.println("--------------------------------------------------------------");
        System.out.println("結果為：" + tradeInfo);
    }

    public Object analyseData(String value, Class className, Object obj, Method setMethod) {
        String[] vals = value.split(",");
        try {
            obj = className.newInstance();
            //遍历属性
            for (String var : vals) {
                if (var.split("=").length < 2) {
                    continue;
                }
                //变量名
                String na = var.split("=")[0].trim();
                //变量值
                String va = var.split("=")[1].trim();
                //指定类的成员变量
                //获取指定成员变量的set方法
                setMethod = className.getMethod("set" + na.substring(0, 1).toUpperCase() + na.substring(1), String.class);
                if (va != null && !"null".equals(va)) {
                    setMethod.invoke(obj, new Object[]{va});
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return obj;
    }

    public String prepareStr() {
        return "TradeInfo{serviceNode='', tradeMap={com.sinosoft.lis.entity.LCAppntLinkManInfoPojo=LCAppntLinkManInfoPojo [AppntLinkID=2161667, ContID=2161669, ShardingID=300000002041, PrtNo=300000002041, ContNo=20194300001010013814, AppntNo=000002912959, Name=张一摇, Sex=null, Birthday=null, IDType=null, IDNo=null, RelationToApp=null, Tel1=, Tel3=null, Tel4=null, Tel5=null, Tel2=null, Mobile1=null, Mobile2=null, Address=null, ZipCode=null, Email=lishanshan0313@rayootech.com, ManageCom=86430000, Operator=ABC-CLOUD, MakeDate=2019-04-01, MakeTime=10:33:38, ModifyDate=2019-04-01, ModifyTime=10:33:38, LetterSendMode=], healthNotice=N, com.sinosoft.lis.entity.LCInsuredPojo=[LCInsuredPojo [InsuredID=2161671, ContID=2161669, PersonID=2161668, ShardingID=300000002041, GrpContNo=00000000000000000000, ContNo=20194300001010013814, InsuredNo=000002912959, PrtNo=300000002041, AppntNo=000002912959, ManageCom=86430000, ExecuteCom=86430000, FamilyID=null, RelationToMainInsured=00, RelationToAppnt=00, AddressNo=2161664, SequenceNo=1, Name=张一摇, Sex=0, Birthday=1980-01-01, IDType=0, IDNo=11010119800101007X, NativePlace=CHN, Nationality=null, RgtAddress=null, Marriage=null, MarriageDate=null, Health=null, Stature=160.0, Avoirdupois=50.0, Degree=null, CreditGrade=null, BankCode=null, BankAccNo=null, AccName=null, JoinCompanyDate=null, StartWorkDate=null, Position=null, Salary=0.0, OccupationType=3, OccupationCode=0501008, WorkType=null, PluralityType=null, SmokeFlag=null, ContPlanCode=JSJXBC, Operator=ABC-CLOUD, InsuredStat=null, MakeDate=2019-04-01, MakeTime=10:33:38, ModifyDate=2019-04-01, ModifyTime=10:33:38, UWFlag=null, UWCode=null, UWDate=null, UWTime=null, BMI=0.0, InsuredPeoples=0, License=null, LicenseType=null, CustomerSeqNo=0, WorkNo=null, SocialInsuNo=null, OccupationDesb=null, IdValiDate=2020-01-01, HaveMotorcycleLicence=null, PartTimeJob=null, HealthFlag=null, ServiceMark=null, FirstName=null, LastName=null, SSFlag=1, TINNO=null, TINFlag=null, InsuredType=null, IsLongValid=null, IDStartDate=null]], com.sinosoft.lis.entity.LCGetPojo=[], com.sinosoft.lis.entity.LCCustomerImpartDetailPojo=[], com.sinosoft.lis.entity.LCPolPojo=[LCPolPojo [PolID=2161683, ContID=2161669, ShardingID=300000002041, GrpContNo=00000000000000000000, GrpPolNo=00000000000000000000, ContNo=20194300001010013814, PolNo=10000000000223123892, ProposalNo=10000000000223123892, PrtNo=300000002041, ContType=2, PolTypeFlag=0, MainPolNo=10000000000223123892, MasterPolNo=null, KindCode=null, RiskCode=6810, RiskVersion=null, ManageCom=86430000, AgentCom=null, AgentType=08, AgentCode=0000065689, AgentGroup=000000020971, AgentCode1=null, SaleChnl=2, Handler=null, InsuredNo=000002912959, InsuredName=张一摇, InsuredSex=0, InsuredBirthday=1980-01-01, InsuredAppAge=39, InsuredPeoples=1, OccupationType=3, AppntNo=000002912959, AppntName=张一摇, CValiDate=2019-04-02, SignCom=86430000, SignDate=null, SignTime=null, FirstPayDate=null, PayEndDate=null, PaytoDate=null, GetStartDate=null, EndDate=, AcciEndDate=null, GetYearFlag=, GetYear=0, PayEndYearFlag=Y, PayEndYear=0, InsuYearFlag=Y, InsuYear=1, AcciYearFlag=null, AcciYear=0, GetStartType=null, SpecifyValiDate=N, PayMode=C, PayLocation=null, PayIntv=-1, PayYears=0, Years=0, ManageFeeRate=0.0, FloatRate=0.0, PremToAmnt=null, Mult=1.0, StandPrem=0.0, Prem=100.0, SumPrem=100.0, Amnt=500000.0, RiskAmnt=0.0, LeavingMoney=0.0, EndorseTimes=0, ClaimTimes=0, LiveTimes=0, RenewCount=0, LastGetDate=null, LastLoanDate=null, LastRegetDate=null, LastEdorDate=null, LastRevDate=2019-04-02, RnewFlag=0, StopFlag=null, ExpiryFlag=null, AutoPayFlag=0, InterestDifFlag=null, SubFlag=null, BnfFlag=0, HealthCheckFlag=null, ImpartFlag=0, ReinsureFlag=null, AgentPayFlag=null, AgentGetFlag=null, LiveGetMode=4, DeadGetMode=null, BonusGetMode=, BonusMan=0, DeadFlag=null, SmokeFlag=null, Remark=null, ApproveFlag=null, ApproveCode=null, ApproveDate=null, ApproveTime=null, UWFlag=0, UWCode=null, UWDate=null, UWTime=null, PolApplyDate=2019-04-01, AppFlag=0, PolState=null, StandbyFlag1=null, StandbyFlag2=null, StandbyFlag3=null, Operator=ABC-CLOUD, MakeDate=2019-04-01, MakeTime=10:33:38, ModifyDate=2019-04-01, ModifyTime=10:33:38, WaitPeriod=0, GetForm=, GetBankCode=03, GetBankAccNo=7201432000082850157, GetAccName=张一摇, KeepValueOpt=null, AscriptionRuleCode=null, PayRuleCode=null, AscriptionFlag=null, AutoPubAccFlag=null, CombiFlag=null, InvestRuleCode=null, UintLinkValiFlag=null, ProdSetCode=null, InsurPolFlag=0, NewReinsureFlag=null, LiveAccFlag=1, StandRatePrem=0.0, AppntFirstName=null, AppntLastName=null, InsuredFirstName=null, InsuredLastName=null, FQGetMode=null, GetPeriod=null, GetPeriodFlag=null, DelayPeriod=null, OtherAmnt=0.0, Relation=null, ReCusNo=null, AutoRnewAge=17, AutoRnewYear=null]], com.sinosoft.lis.entity.LDPersonPojo=[LDPersonPojo [PersonID=2161668, ShardingID=300000002041, CustomerNo=000002912959, Name=张一摇, Sex=0, Birthday=1980-01-01, IDType=0, IDNo=11010119800101007X, Password=912959, NativePlace=CHN, Nationality=null, RgtAddress=null, Marriage=null, MarriageDate=null, Health=null, Stature=0.0, Avoirdupois=0.0, Degree=null, CreditGrade=null, OthIDType=null, OthIDNo=null, ICNo=null, GrpNo=null, JoinCompanyDate=null, StartWorkDate=null, Position=null, Salary=0.0, OccupationType=3, OccupationCode=0501008, WorkType=null, PluralityType=null, DeathDate=null, SmokeFlag=null, BlacklistFlag=null, Proterty=null, Remark=null, State=null, VIPValue=0, Operator=ABC-CLOUD, MakeDate=2019-04-01, MakeTime=10:33:38, ModifyDate=2019-04-01, ModifyTime=10:33:38, GrpName=null, License=null, LicenseType=null, SocialInsuNo=null, IdValiDate=2020-01-01, HaveMotorcycleLicence=null, PartTimeJob=null, HealthFlag=null, ServiceMark=null, FirstName=null, LastName=null, CUSLevel=null, SSFlag=1, RgtTpye=2, TINNO=null, TINFlag=null, NewCustomerFlag=NEW]], autoRenew=-9, com.sinosoft.lis.entity.LCDutyPojo=[], com.sinosoft.lis.entity.LCAccountPojo=LCAccountPojo [AccountID=2161684, PersonID=2161668, ShardingID=300000002041, CustomerNo=000002912959, AccKind=1, BankCode=03, BankAccNo=7201432000082850157, AccName=张一摇, Operator=ABC-CLOUD, MakeDate=2019-04-01, MakeTime=10:33:38, ModifyDate=2019-04-01, ModifyTime=10:33:38], otherNo=null, com.sinosoft.lis.entity.LCPremPojo=[], com.sinosoft.lis.entity.GlobalPojo=GlobalPojo[BankCode='0501', ZoneNo='18', BrNo='2558', TransNo='1002', MainRiskCode='6810', AutoRenew='null', SerialNo='3854345665439634434878', TransDate='2019-04-01', TransTime='10:50:15', EntrustWay='ABC_Supotc', ContPrtNo='', AgentPersonCode='00200901371300004041', TransferFlag='0', OldPolicy='', OldPolicyPwd='', OldVchNo='', TellerNo='22jq', CorpNo='1118', FunctionFlag='null', InsuSerial='null', InterActNode='1', addPrem=0.0, PoliValidDate='', RiskBeginDate='', RiskEndDate=''], ComCode=86430000, com.sinosoft.lis.entity.LCAppntPojo=LCAppntPojo [AppntID=2161666, ContID=2161669, PersonID=2161668, ShardingID=300000002041, GrpContNo=00000000000000000000, ContNo=20194300001010013814, PrtNo=300000002041, AppntNo=000002912959, AppntGrade=null, AppntName=张一摇, AppntSex=0, AppntBirthday=1980-01-01, AppntType=null, AddressNo=2161664, IDType=0, IDNo=11010119800101007X, NativePlace=CHN, Nationality=null, RgtAddress=null, Marriage=null, MarriageDate=null, Health=null, Stature=0.0, Avoirdupois=0.0, Degree=null, CreditGrade=null, BankCode=null, BankAccNo=null, AccName=null, JoinCompanyDate=null, StartWorkDate=null, Position=null, Salary=0.0, OccupationType=3, OccupationCode=0501008, WorkType=null, PluralityType=null, SmokeFlag=null, Operator=ABC-CLOUD, ManageCom=86430000, MakeDate=2019-04-01, MakeTime=10:33:38, ModifyDate=2019-04-01, ModifyTime=10:33:38, BMI=0.0, License=null, LicenseType=null, RelatToInsu=00, OccupationDesb=null, IdValiDate=2020-01-01, HaveMotorcycleLicence=null, PartTimeJob=null, FirstName=null, LastName=null, CUSLevel=null, AppRgtTpye=2, TINNO=null, TINFlag=null], com.sinosoft.lis.entity.LKTransStatusPojo=[LKTransStatusPojo [TransStatusID=2161670, ShardingID=300000002041, TransCode=3854345665439634434878, ReportNo=null, BankCode=0501, BankBranch=18, BankNode=2558, BankOperator=00200901371300004041, TransNo=3854345665439634434878, FuncFlag=1002, TransDate=2019-04-01, TransTime=10:50:15, ManageCom=null, RiskCode=null, ProposalNo=300000002041, PrtNo=, PolNo=20194300001010013814, EdorNo=null, TempFeeNo=null, TransAmnt=0.0, BankAcc=null, RCode=null, TransStatus=null, Status=null, Descr=null, Temp=null, MakeDate=2019-04-01, MakeTime=10:33:38, ModifyDate=2019-04-01, ModifyTime=10:33:38, State_Code=null, RequestId=null, OutServiceCode=null, ClientIP=null, ClientPort=null, IssueWay=null, ServiceStartTime=null, ServiceEndTime=null, RBankVSMP=null, DesBankVSMP=null, RMPVSKernel=null, DesMPVSKernel=null, ResultBalance=null, DesBalance=null, bak1=2, bak2=null, bak3=null, bak4=ABC_Supotc]], com.sinosoft.lis.entity.LCSpecPojo=[], com.sinosoft.lis.entity.LCAddressPojo=[LCAddressPojo [AddressID=2161665, PersonID=2161668, ShardingID=300000002041, CustomerNo=000002912959, AddressNo=2161664, PostalAddress=河北省石家庄市桥西区红旗大街144号, ZipCode=251082, Phone=, Fax=null, HomeAddress=河北省石家庄市桥西区红旗大街144号, HomeZipCode=251082, HomePhone=, HomeFax=null, CompanyAddress=null, CompanyZipCode=null, CompanyPhone=, CompanyFax=null, Mobile=13918233146, MobileChs=null, EMail=lishanshan0313@rayootech.com, BP=null, Mobile2=null, MobileChs2=null, EMail2=null, BP2=null, Operator=ABC-CLOUD, MakeDate=2019-04-01, MakeTime=10:33:38, ModifyDate=2019-04-01, ModifyTime=10:33:38, GrpName=, Province=13, City=1301, County=130133, ZoneCode=null, StoreNo=null, StoreNo2=null]], GroupFlag=grp, com.sinosoft.lis.entity.VMS_OUTPUT_CUSTOMERPojo=VMS_OUTPUT_CUSTOMERPojo [VIC_INDATE=2019-04-01, VIC_CUSTOMER_ID=000002912959, VIC_CUSTOMER_CNAME=张一摇, VIC_CUSTOMER_TAXNO=, VIC_CUSTOMER_ACCOUNT=null, VIC_CUSTOMER_CBANK=中国农业银行, VIC_CUSTOMER_PHONE=13918233146, VIC_CUSTOMER_EMAIL=lishanshan0313@rayootech.com, VIC_CUSTOMER_ADDRESS=河北石家庄赵县河北省石家庄市桥西区红旗大街144号, VIC_TAXPAYER_TYPE=O, VIC_FAPIAO_TYPE=0, VIC_CUSTOMER_TYPE=I, VIC_CUSTOMER_FAPIAO_FLAG=M, VIC_CUSTOMER_NATIONALITY=CHN, VIC_DATA_SOURCE=1, VIC_LINK_NAME=, VIC_LINK_PHONE=, VIC_LINK_ADDRESS=, VIC_CUSTOMER_ZIP_CODE=251082, VIC_CUSTOMER_UPLOAD_FLAG=0], RiskCodeWr=JSJXBC, serialNo=3854345665439634434878, applySerial=null, com.sinosoft.lis.entity.LCContPojo=LCContPojo [ContID=2161669, ShardingID=300000002041, GrpContNo=00000000000000000000, ContNo=20194300001010013814, ProposalContNo=20194300001010013814, PrtNo=300000002041, ContType=2, FamilyType=0, FamilyID=null, PolType=0, CardFlag=0, ManageCom=86430000, ExecuteCom=86430000, AgentCom=0506010109, AgentCode=0000065689, AgentGroup=000000020971, AgentCode1=null, AgentType=null, SaleChnl=2, Handler=null, Password=null, AppntNo=000002912959, AppntName=张一摇, AppntSex=0, AppntBirthday=1980-01-01, AppntIDType=0, AppntIDNo=11010119800101007X, InsuredNo=000002912959, InsuredName=张一摇, InsuredSex=0, InsuredBirthday=1980-01-01, InsuredIDType=0, InsuredIDNo=11010119800101007X, PayIntv=-1, PayMode=C, PayLocation=0, DisputedFlag=null, OutPayFlag=null, GetPolMode=null, SignCom=86430000, SignDate=null, SignTime=null, ConsignNo=null, BankCode=03, BankAccNo=7201432000082850157, AccName=张一摇, PrintCount=0, LostTimes=0, Lang=null, Currency=null, Remark=null, Peoples=1, Mult=0.0, Prem=0.0, Amnt=0.0, SumPrem=0.0, Dif=0.0, PaytoDate=null, FirstPayDate=2019-04-01, CValiDate=2019-04-02, InputOperator=null, InputDate=null, InputTime=null, ApproveFlag=0, ApproveCode=null, ApproveDate=null, ApproveTime=null, UWFlag=0, UWOperator=null, UWDate=null, UWTime=null, AppFlag=0, PolApplyDate=2019-04-01, GetPolDate=2019-04-01, GetPolTime=10:33:38, CustomGetPolDate=2019-04-01, State=0, Operator=ABC-CLOUD, MakeDate=2019-04-01, MakeTime=10:33:38, ModifyDate=2019-04-01, ModifyTime=10:33:38, FirstTrialOperator=null, FirstTrialDate=null, FirstTrialTime=null, ReceiveOperator=null, ReceiveDate=null, ReceiveTime=null, TempFeeNo=null, SellType=27, ForceUWFlag=0, ForceUWReason=null, NewBankCode=03, NewBankAccNo=7201432000082850157, NewAccName=张一摇, NewPayMode=4, AgentBankCode=05, BankAgent=00200901371300004041, BankAgentName=林静, BankAgentTel=null, ProdSetCode=null, PolicyNo=null, BillPressNo=null, CardTypeCode=null, VisitDate=null, VisitTime=null, SaleCom=null, PrintFlag=null, InvoicePrtFlag=null, NewReinsureFlag=null, RenewPayFlag=null, AppntFirstName=null, AppntLastName=null, InsuredFirstName=null, InsuredLastName=null, AuthorFlag=null, GreenChnl=0, TBType=01, EAuto=0, SlipForm=1],  com.sinosoft.lis.entity.LCCustomerImpartParamsPojo=[LCCustomerImpartParamsPojo [CustomerImpartParamsID=2161675, CustomerImpartDetailID=0, ShardingID=300000002041, GrpContNo=00000000000000000000, ContNo=20194300001010013814, ProposalContNo=20194300001010013814, PrtNo=300000002041, ImpartCode=001, ImpartVer=01, CustomerNo=000002912959, CustomerNoType=0, ImpartParamNo=1, ImpartParamName=AnnualIncome, ImpartParam=900.0, Operator=ABC-CLOUD, MakeDate=2019-04-01, MakeTime=10:33:38, ModifyDate=2019-04-01, ModifyTime=10:33:38, PatchNo=0], LCCustomerImpartParamsPojo [CustomerImpartParamsID=2161676, CustomerImpartDetailID=0, ShardingID=300000002041, GrpContNo=00000000000000000000, ContNo=20194300001010013814, ProposalContNo=20194300001010013814, PrtNo=300000002041, ImpartCode=001, ImpartVer=01, CustomerNo=000002912959, CustomerNoType=0, ImpartParamNo=2, ImpartParamName=Origin, ImpartParam=1, Operator=ABC-CLOUD, MakeDate=2019-04-01, MakeTime=10:33:38, ModifyDate=2019-04-01, ModifyTime=10:33:38, PatchNo=0], LCCustomerImpartParamsPojo [CustomerImpartParamsID=2161677, CustomerImpartDetailID=0, ShardingID=300000002041, GrpContNo=00000000000000000000, ContNo=20194300001010013814, ProposalContNo=20194300001010013814, PrtNo=300000002041, ImpartCode=001, ImpartVer=01, CustomerNo=000002912959, CustomerNoType=0, ImpartParamNo=3, ImpartParamName=FamilyIncome, ImpartParam=2.0, Operator=ABC-CLOUD, MakeDate=2019-04-01, MakeTime=10:33:38, ModifyDate=2019-04-01, ModifyTime=10:33:38, PatchNo=0], LCCustomerImpartParamsPojo [CustomerImpartParamsID=2161678, CustomerImpartDetailID=0, ShardingID=300000002041, GrpContNo=00000000000000000000, ContNo=20194300001010013814, ProposalContNo=20194300001010013814, PrtNo=300000002041, ImpartCode=001, ImpartVer=01, CustomerNo=000002912959, CustomerNoType=1, ImpartParamNo=1, ImpartParamName=AnnualIncome, ImpartParam=900.0, Operator=ABC-CLOUD, MakeDate=2019-04-01, MakeTime=10:33:38, ModifyDate=2019-04-01, ModifyTime=10:33:38, PatchNo=0], LCCustomerImpartParamsPojo [CustomerImpartParamsID=2161679, CustomerImpartDetailID=0, ShardingID=300000002041, GrpContNo=00000000000000000000, ContNo=20194300001010013814, ProposalContNo=20194300001010013814, PrtNo=300000002041, ImpartCode=001, ImpartVer=01, CustomerNo=000002912959, CustomerNoType=1, ImpartParamNo=2, ImpartParamName=Origin, ImpartParam=1, Operator=ABC-CLOUD, MakeDate=2019-04-01, MakeTime=10:33:38, ModifyDate=2019-04-01, ModifyTime=10:33:38, PatchNo=0], LCCustomerImpartParamsPojo [CustomerImpartParamsID=2161680, CustomerImpartDetailID=0, ShardingID=300000002041, GrpContNo=00000000000000000000, ContNo=20194300001010013814, ProposalContNo=20194300001010013814, PrtNo=300000002041, ImpartCode=001, ImpartVer=01, CustomerNo=000002912959, CustomerNoType=1, ImpartParamNo=3, ImpartParamName=FamilyIncome, ImpartParam=2.0, Operator=ABC-CLOUD, MakeDate=2019-04-01, MakeTime=10:33:38, ModifyDate=2019-04-01, ModifyTime=10:33:38, PatchNo=0], LCCustomerImpartParamsPojo [CustomerImpartParamsID=2161681, CustomerImpartDetailID=0, ShardingID=300000002041, GrpContNo=00000000000000000000, ContNo=20194300001010013814, ProposalContNo=20194300001010013814, PrtNo=300000002041, ImpartCode=000, ImpartVer=02, CustomerNo=000002912959, CustomerNoType=1, ImpartParamNo=1, ImpartParamName=Height, ImpartParam=160, Operator=ABC-CLOUD, MakeDate=2019-04-01, MakeTime=10:33:38, ModifyDate=2019-04-01, ModifyTime=10:33:38, PatchNo=0], LCCustomerImpartParamsPojo [CustomerImpartParamsID=2161682, CustomerImpartDetailID=0, ShardingID=300000002041, GrpContNo=00000000000000000000, ContNo=20194300001010013814, ProposalContNo=20194300001010013814, PrtNo=300000002041, ImpartCode=000, ImpartVer=02, CustomerNo=000002912959, CustomerNoType=1, ImpartParamNo=2, ImpartParamName=weight, ImpartParam=50, Operator=ABC-CLOUD, MakeDate=2019-04-01, MakeTime=10:33:38, ModifyDate=2019-04-01, ModifyTime=10:33:38, PatchNo=0]], otherCompanyDieAmnt=, payPrem=0.0, com.sinosoft.lis.entity.LCBnfPojo=[]}, storageMap={}, exception=null, errorList=[], shardFlag=false, shardKey='null', action='null'}";
    }
}
