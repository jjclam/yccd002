package com.sinosoft.cloud;

import com.sinosoft.cloud.cbs.uw.AmntHistory6807Service;
import com.sinosoft.cloud.rest.TradeInfo;
import com.sinosoft.lis.entity.LCContPojo;
import com.sinosoft.lis.entity.LCPolPojo;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.ArrayList;

/**
 * @Author:wangshuliang
 * @Description:
 * @Date:Created in 13:36 2018/7/2
 * @Modified by:
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = UWApplication.class)
public class AmntHistory6807ServiceTest {
    @Autowired
    private AmntHistory6807Service amntHistory6807Service;

    @Test
    public void getAddAmnt(){
        LCContPojo lcContPojo = new LCContPojo();
        lcContPojo.setContNo("007103685857020");
        lcContPojo.setInsuredName("egwegwegw");
        lcContPojo.setInsuredIDNo("37837837637");
        lcContPojo.setInsuredIDType("1");
        lcContPojo.setInsuredBirthday("1989-12-25");
        lcContPojo.setInsuredSex("0");
        TradeInfo tradeInfo = new TradeInfo();
        ArrayList<LCPolPojo> objects = new ArrayList<>();
        LCPolPojo lcPolPojo = new LCPolPojo();
        lcPolPojo.setEndDate("2015-01-01");
        lcPolPojo.setRiskCode("1806");
        lcPolPojo.setCValiDate("2014-01-12");
        lcPolPojo.setPolNo("1806");
        lcPolPojo.setMainPolNo("6807");
        LCPolPojo lcPolPojo1 = new LCPolPojo();
        lcPolPojo1.setEndDate("2015-01-01");
        lcPolPojo1.setRiskCode("6807");
        lcPolPojo1.setCValiDate("2014-01-12");
        lcPolPojo1.setPolNo("6807");
        lcPolPojo1.setMainPolNo("6807");
        objects.add(lcPolPojo);
        objects.add(lcPolPojo1);
        tradeInfo.addData(LCContPojo.class.getName(),lcContPojo);
        tradeInfo.addData(LCPolPojo.class.getName(),objects);
        amntHistory6807Service.service(tradeInfo);
        System.out.println(tradeInfo.getData("pastAmnt"));
    }
}
