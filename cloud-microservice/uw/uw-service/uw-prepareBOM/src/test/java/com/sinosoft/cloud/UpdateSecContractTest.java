package com.sinosoft.cloud;

import com.sinosoft.cloud.cbs.bl.UpdateSecContactBL;
import com.sinosoft.cloud.cbs.nb.bl.AipWsClientCommon;
import com.sinosoft.cloud.cbs.uw.UpdateSecContactService;
import com.sinosoft.cloud.rest.TradeInfo;
import com.sinosoft.lis.entity.LCContPojo;
import com.sinosoft.lis.entity.LCInsuredPojo;
import com.sinosoft.lis.entity.LCPolPojo;
import com.sinosoft.lis.entity.LCPolicyInfoPojo;
import com.sinosoft.utility.ExceptionUtils;
import com.sinosoft.utility.Reflections;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.validation.constraints.AssertTrue;
import java.lang.reflect.Method;
import java.util.*;

import static junit.framework.TestCase.assertTrue;

/**
 * @Author:wangshuliang
 * @Description:
 * @Date:Created in 11:33 2018/6/30
 * @Modified by:
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = UWApplication.class)
public class UpdateSecContractTest {

    private final Log logger = LogFactory.getLog(this.getClass());
    @Value("${cloud.aip.uw.url}")
    private String url;
    @Value("${cloud.aip.uw.namespace}")
    private String namespace;
    @Value("${cloud.aip.uw.method}")
    private String method;
    @Value("${cloud.aip.uw.update}")
    private String reqType;
    @Value("${cloud.aip.uw.arg0}")
    private String arg0;
    @Value("${cloud.aip.uw.arg1}")
    private String arg1;
    @Value("${cloud.aip.uw.getCipRateReq}")
    private String getCipRateReq;
    @Autowired
    private AipWsClientCommon aipWsClientCommon;

    @Autowired
    UpdateSecContactBL updateSecContactBL;
    @Autowired
    private UpdateSecContactService updateSecContactService;

    @Test
    public void update(){
        TradeInfo tradeInfo = new TradeInfo();
        prepareData(tradeInfo);
        updateSecContactService.service(tradeInfo);
        assertTrue(!tradeInfo.hasError());
    }

    /**
     * 更新借款人的特殊审批信息
     */
    @Test
    public void dataSource() {
        String xml = getXMLAmnt();
        Map<String, Object> reqMap = new HashMap<>();
        reqMap.put(arg0, reqType);
        reqMap.put(arg1, xml);
        try {
            String res = aipWsClientCommon.sendService(url, namespace, method, reqMap);
            logger.debug("更新特殊审批信息成功" + res);
        } catch (Exception e) {
            logger.debug("获取特约审批信息失败" + ExceptionUtils.exceptionToString(e));
        }
    }
    /**
     * 更新借款人的特殊审批信息
     */
    @Test
    public void dataSourceUW() {
        String xml = getXMLAmnt();
        Map<String, Object> reqMap = new HashMap<>();
        reqMap.put(arg0, reqType);
        reqMap.put(arg1, xml);
        try {
            String res = aipWsClientCommon.sendService(url, namespace, method, reqMap);
            logger.debug("更新特殊审批信息成功" + res);
        } catch (Exception e) {
            logger.debug("获取特约审批信息失败" + ExceptionUtils.exceptionToString(e));
        }
    }


    public String getXMLAmnt() {
        return "<?xml version=\"1.0\" encoding=\"utf-8\"?>" +
                "<message>" +
                "<head>" +
                "<sysHeader>" +
                "<standardVersionCode/>" +
                "<msgDate>2018-06-11</msgDate>" +
                "<msgTime>11:20:30</msgTime>" +
                "<systemCode>AIP</systemCode>" +
                "<transactionCode>1011</transactionCode>" +
                "<transRefGUID/>" +
                "<esbRefGUID/>" +
                "<transNo/>" +
                "<businessCode/>" +
                "<businessType/>" +
                "</sysHeader>" +
                "<domHeader/>" +
                "<bizHeader/>" +
                "</head>" +
                "<body>" +
                "<lcInsuredInfo>" +
                "<name>092608有效期已过</name>" +
                "<sex>F</sex>" +
                "<idType>14</idType>" +
                "<idNo>1546115</idNo>" +
                "<birthday>1992-06-11</birthday>" +
                "</lcInsuredInfo>" +
                "<loanContractNo>YUI123123412312321</loanContractNo>" +
                "<approveInfos>" +
                "<approveInfo>" +
                "<approveTypeCode>04</approveTypeCode>" +
                "</approveInfo>" +
                "</approveInfos>" +
                "<updateInfo>" +
                "<contNo>007679851205020</contNo>" +
                "<useState>02</useState>" +
                "</updateInfo>" +
                "</body>" +
                "</message>";
    }
    public void  prepareData(TradeInfo tradeInfo){
        LCPolPojo lcPolPojo = new LCPolPojo();
        lcPolPojo.setRiskCode("6807");
        lcPolPojo.setPolNo("6807");
        lcPolPojo.setMainPolNo("6807");
        List<LCPolPojo> objects = new ArrayList<>();
        objects.add(lcPolPojo);
        LCInsuredPojo lcInsuredPojo = new LCInsuredPojo();
        lcInsuredPojo.setIDNo("420582197901074990");
        lcInsuredPojo.setIDType("01");
        lcInsuredPojo.setBirthday("1979-01-07");
        lcInsuredPojo.setSex("0");
        lcInsuredPojo.setName("朱波");
        lcInsuredPojo.setInsuredNo("0000682061");
        lcInsuredPojo.setContPlanCode("YA");
        List<LCInsuredPojo> lcInsuredPojos = new ArrayList<>();
        lcInsuredPojos.add(lcInsuredPojo);

        LCContPojo lcContPojo = new LCContPojo();
        lcContPojo.setContNo("1111111111");
        lcContPojo.setAgentCode("0000000147");

        LCPolicyInfoPojo lcPolicyInfoPojo = new LCPolicyInfoPojo();
        //lcPolicyInfoPojo.setJYContNo("222222222");
        tradeInfo.addData(lcContPojo);
        tradeInfo.addData(LCInsuredPojo.class.getName(),lcInsuredPojos);
        tradeInfo.addData(LCPolicyInfoPojo.class.getName(),lcPolicyInfoPojo);
        tradeInfo.addData(LCPolPojo.class.getName(),objects);
    }

}