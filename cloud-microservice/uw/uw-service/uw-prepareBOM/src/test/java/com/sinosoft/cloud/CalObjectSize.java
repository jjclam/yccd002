package com.sinosoft.cloud;

import com.sinosoft.cloud.rest.TradeInfo;
import com.sinosoft.utility.Reflections;


import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.lang.reflect.Method;
import java.util.*;

/**
 * @Author:wangshuliang
 * @Description:
 * @Date:Created in 20:06 2018/11/7
 * @Modified by:
 */
public class CalObjectSize {
    public static void main(String[] args) {
        TradeInfo tradeInfo = new TradeInfo();
        CalObjectSize calObjectSize = new CalObjectSize();
        calObjectSize.prepareData(tradeInfo);
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        ObjectOutput out = null;
        try {
            out = new ObjectOutputStream(bos);
            out.writeObject(tradeInfo);
            out.flush();
            byte[] toByteArray = bos.toByteArray();
            System.out.println(toByteArray.length / 1024);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                bos.close();
            } catch (IOException ex) {
                // ignore close exception
            }
        }
    }

    public static void prepareData(TradeInfo tradeInfo) {
        String str = prepareStr();
        Reflections reflections = new Reflections();
        //截取trademap数据
        str = str.substring(str.indexOf("tradeMap={") + 10, str.indexOf("}"));
        String[] objs = str.split("com.sinosoft.lis.entity.");
        List<String> arrs = Arrays.asList(objs);
        int start = -1;
        int end = -1;
        int eq = -1;
        Map<String, String> map = new HashMap();
        //解析tradeinfo
        for (int i = 0; i < arrs.size(); i++) {
            String arr = arrs.get(i);
            start = arr.indexOf("[");
            end = arr.lastIndexOf("]");
            eq = arr.indexOf("=");
            if (start != -1 && end != -1 && eq != -1) {
                map.put(arr.substring(0, eq).trim(), arr.substring(start + 1, end));
            }
            start = arr.indexOf("{");
            end = arr.indexOf("}");
            if (start != -1 && end != -1 && end == arr.lastIndexOf("}")) {

            }
        }
        //解析类
        String value = null;
        Method setMethod = null;
        Class className = null;
        /*Field field = null;*/
        Object obj = null;
        for (String key : map.keySet()) {
            value = map.get(key);
            if ("".equals(value)) {
                continue;
            }
            //指定类
            try {
                className = Class.forName("com.sinosoft.lis.entity." + key);
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }

            start = value.indexOf("[");
            end = value.indexOf("]");
            //解析value中有几个[]，判断是集合对象还是类对象
            if (start == -1) {
                obj = analyseData(value, className, obj, setMethod);
                tradeInfo.addData(className.getName(), obj);
            } else {
                //判断是否存在字符串格式的数据
                if (value.lastIndexOf("]") != value.length() - 1) {
                    String otherVal = value.substring(value.lastIndexOf("]") + 2);
                    for (String val : otherVal.split(",")) {
                        tradeInfo.addData(val.split("=")[0].trim(), val.split("=")[1].trim());
                    }
                }
                //集合对象遍历
                List classList = new ArrayList();
                while (start != -1) {
                    classList.add(analyseData(value.substring(start + 1, end), className, obj, setMethod));
                    value = value.substring(end + 1);
                    start = value.indexOf("[");
                    end = value.indexOf("]");
                }
                if (classList.size() != 0) {
                    tradeInfo.addData(className.getName(), classList);
                }
            }
        }
        System.out.println("--------------------------------------------------------------");
        System.out.println("結果為：" + tradeInfo);
    }

    public static Object analyseData(String value, Class className, Object obj, Method setMethod) {
        String[] vals = value.split(",");
        try {
            obj = className.newInstance();
            //遍历属性
            for (String var : vals) {
                if (var.split("=").length < 2) {
                    continue;
                }
                //变量名
                String na = var.split("=")[0].trim();
                //变量值
                String va = var.split("=")[1].trim();
                //指定类的成员变量
                //获取指定成员变量的set方法
                setMethod = className.getMethod("set" + na.substring(0, 1).toUpperCase() + na.substring(1), String.class);
                if (va != null && !"null".equals(va)) {
                    setMethod.invoke(obj, new Object[]{va});
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return obj;
    }


    public static String prepareStr() {
        return "TradeInfo{serviceNode='null', tradeMap={com.sinosoft.lis.entity.LKTransStatusPojo=[LKTransStatusPojo [TransStatusID=18646893, ShardingID=33180085, TransCode=706034072748, ReportNo=null, BankCode=0501, BankBranch=15, BankNode=7737, BankOperator=null, TransNo=706034072748, FuncFlag=1002, TransDate=2018-11-09, TransTime=12:30:25, ManageCom=null, RiskCode=null, ProposalNo=33180085, PrtNo=null, PolNo=201837140020060086, EdorNo=null, TempFeeNo=null, TransAmnt=0.0, BankAcc=null, RCode=null, TransStatus=null, Status=null, Descr=null, Temp=null, MakeDate=2018-11-09, MakeTime=12:30:25, ModifyDate=2018-11-09, ModifyTime=12:30:25, State_Code=null, RequestId=null, OutServiceCode=null, ClientIP=null, ClientPort=null, IssueWay=null, ServiceStartTime=null, ServiceEndTime=null, RBankVSMP=null, DesBankVSMP=null, RMPVSKernel=null, DesMPVSKernel=null, ResultBalance=null, DesBalance=null, bak1=1, bak2=null, bak3=null, bak4=ABC_SST]], com.sinosoft.lis.entity.LJSPayPersonPojo=[LJSPayPersonPojo [SPayPersonID=435182, SPayID=435181, ShardingID=33180085, PolNo=005711615129026, PayCount=1, GrpContNo=00000000000000000000, GrpPolNo=00000000000000000000, ContNo=201837140020060086, ManageCom=86371400, AgentCom=0510160906, AgentType=08, RiskCode=5022, AgentCode=0000187824, AgentGroup=000000042590, PayTypeFlag=null, AppntNo=0014088066, GetNoticeNo=007233880945096, PayAimClass=1, DutyCode=502201, PayPlanCode=502221, SumDuePayMoney=20.0, SumActuPayMoney=0.0, PayIntv=0, PayDate=2018-11-09, PayType=ZC, LastPayToDate=1899-12-31, CurPayToDate=null, InInsuAccState=null, BankCode=03, BankAccNo=null, BankOnTheWayFlag=null, BankSuccFlag=null, ApproveCode=null, ApproveDate=null, ApproveTime=null, SerialNo=null, InputFlag=null, Operator=ABC-CLOUD, MakeDate=2018-11-09, MakeTime=14:58:16, ModifyDate=2018-11-09, ModifyTime=14:58:16, EdorNo=null, MainPolYear=69, OtherNo=33180085, OtherNoType=07, PrtNo=33180085, SaleChnl=3, ChargeCom=8637, APPntName=杨颖超, KindCode=U, PaytoDate=null, PayEndDate=3018-11-10, SignDate=null, CValiDate=2018-11-10, PayEndYear=1000, PayMoney=20.0], LJSPayPersonPojo [SPayPersonID=435183, SPayID=435181, ShardingID=33180085, PolNo=005711615343026, PayCount=1, GrpContNo=00000000000000000000, GrpPolNo=00000000000000000000, ContNo=201837140020060086, ManageCom=86371400, AgentCom=0510160906, AgentType=08, RiskCode=1037, AgentCode=0000187824, AgentGroup=000000042590, PayTypeFlag=null, AppntNo=0014088066, GetNoticeNo=007233880945096, PayAimClass=1, DutyCode=103701, PayPlanCode=103721, SumDuePayMoney=2000.0, SumActuPayMoney=0.0, PayIntv=12, PayDate=2018-11-09, PayType=ZC, LastPayToDate=1899-12-31, CurPayToDate=null, InInsuAccState=null, BankCode=03, BankAccNo=null, BankOnTheWayFlag=null, BankSuccFlag=null, ApproveCode=null, ApproveDate=null, ApproveTime=null, SerialNo=null, InputFlag=null, Operator=ABC-CLOUD, MakeDate=2018-11-09, MakeTime=14:58:16, ModifyDate=2018-11-09, ModifyTime=14:58:16, EdorNo=null, MainPolYear=10, OtherNo=33180085, OtherNoType=07, PrtNo=33180085, SaleChnl=3, ChargeCom=8637, APPntName=杨颖超, KindCode=R, PaytoDate=null, PayEndDate=2021-11-10, SignDate=null, CValiDate=2018-11-10, PayEndYear=3, PayMoney=2000.0]], com.sinosoft.lis.entity.LJSPayPojo=[LJSPayPojo [SPayID=435181, ShardingID=33180085, GetNoticeNo=007233880945096, OtherNo=33180085, OtherNoType=07, AppntNo=0014088066, SumDuePayMoney=2020.0, PayDate=2018-11-09, BankOnTheWayFlag=0, BankSuccFlag=0, SendBankCount=0, ApproveCode=, ApproveDate=, SerialNo=0073405447988637, Operator=ABC-CLOUD, MakeDate=2018-11-09, MakeTime=14:58:16, ModifyDate=2018-11-09, ModifyTime=14:58:16, ManageCom=86371400, AgentCom=0510160906, AgentType=null, BankCode=03, BankAccNo=null, RiskCode=000000, AgentCode=0000187824, AgentGroup=000000042590, AccName=null, StartPayDate=2018-11-09, PayTypeFlag=null, BalanceOnTime=null, PayMode=C, APPntName=杨颖超, ChargeCom=8637]]}, storageMap={}, exception=null, errorList=[], shardFlag=false, shardKey='null', action='null'}";
    }
}
