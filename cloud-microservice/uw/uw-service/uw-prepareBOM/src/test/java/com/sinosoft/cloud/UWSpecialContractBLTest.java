package com.sinosoft.cloud;

import com.sinosoft.cloud.cbs.nb.bl.AipWsClientCommon;
import com.sinosoft.cloud.cbs.trules.specontract.SearchHistoryInfoSrvBL;
import com.sinosoft.cloud.cbs.trules.specontract.UWSpecialContractBL;
import com.sinosoft.cloud.rest.TradeInfo;
import com.sinosoft.lis.entity.LCContPojo;
import com.sinosoft.lis.entity.LCInsuredPojo;
import com.sinosoft.lis.entity.LCPolicyInfoPojo;
import com.sinosoft.utility.*;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.lang.reflect.Method;
import java.util.*;

/**
 * @Author:wangshuliang
 * @Description:
 * @Date:Created in 14:38 2018/6/25
 * @Modified by:
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = UWApplication.class)
public class UWSpecialContractBLTest {
    private final Log logger = LogFactory.getLog(this.getClass());
    @Value("${cloud.aip.uw.url}")
    private String url;
    @Value("${cloud.aip.uw.namespace}")
    private String namespace;
    @Value("${cloud.aip.uw.method}")
    private String method;
    @Value("${cloud.aip.uw.update}")
    private String reqType;
    @Value("${cloud.aip.uw.arg0}")
    private String arg0;
    @Value("${cloud.aip.uw.arg1}")
    private String arg1;
    @Value("${cloud.aip.uw.getCipRateReq}")
    private String getCipRateReq;
    @Autowired
    private AipWsClientCommon aipWsClientCommon;
    @Autowired
    private SearchHistoryInfoSrvBL searchHistoryInfoSrvBL;
    @Autowired
    private UWSpecialContractBL uwSpecialContractBL;

    @Autowired
    SearchHistoryInfoSrvBL SearchHistoryUWInfoSrvBL;

    /**
     * 更新借款人的特殊审批信息
     */
    @Test
    public void dataSource() {
        String xml = getXML();
        Map<String, Object> reqMap = new HashMap<>();
        reqMap.put(arg0, reqType);
        reqMap.put(arg1, xml);
        try {
            String res = aipWsClientCommon.sendService(url, namespace, method, reqMap);
            logger.debug("更新特殊审批信息成功" + res);
        } catch (Exception e) {
            logger.debug("获取特约审批信息失败" + ExceptionUtils.exceptionToString(e));
        }
    }

    /**
     * 查询延期拒保的信息
     */
    @Test
    public void dataSourceUW() {
        String xml = getXMLUW();
        Map<String, Object> reqMap = new HashMap<>();
        reqMap.put(arg0, getCipRateReq);
        reqMap.put(arg1, xml);
        try {
            String res = aipWsClientCommon.sendService(url, namespace, method, reqMap);
            logger.debug("查询特殊审批延期拒保殊审批信息成功" + res);
        } catch (Exception e) {
            logger.debug("查询特殊审批延期拒保审批信息失败" + ExceptionUtils.exceptionToString(e));
        }
    }
    /**
     * 查询延期拒保的信息
     */
    @Test
    public void dataSourceAmnt() {
        String xml = getXMLAmnt();
        Map<String, Object> reqMap = new HashMap<>();
        reqMap.put(arg0, getCipRateReq);
        reqMap.put(arg1, xml);
        try {
            String res = aipWsClientCommon.sendService(url, namespace, method, reqMap);
            logger.debug("查询特殊审批延期拒保殊审批信息成功" + res);
        } catch (Exception e) {
            logger.debug("查询特殊审批延期拒保审批信息失败" + ExceptionUtils.exceptionToString(e));
        }
    }



    /**
     * 查询既往理赔的信息
     */
    @Test
    public void dataSourceClaim() {
        String xml = getXMLClaim();
        Map<String, Object> reqMap = new HashMap<>();
        reqMap.put(arg0, getCipRateReq);
        reqMap.put(arg1, xml);
        try {
            String res = aipWsClientCommon.sendService(url, namespace, method, reqMap);
            logger.debug("查询特殊审批理赔殊审批信息成功" + res);
        } catch (Exception e) {
            logger.debug("查询特殊审批理赔审批信息失败" + ExceptionUtils.exceptionToString(e));
        }
    }
    public String  getXMLAmnt(){
        return "<?xml version=\"1.0\" encoding=\"utf-8\"?>" +
                "<message>" +
                "<head>" +
                "<sysHeader>" +
                "<standardVersionCode/>" +
                "<msgDate>2018-06-11</msgDate>" +
                "<msgTime>10:40:56</msgTime>" +
                "<systemCode>AIP</systemCode>" +
                "<transactionCode>1011</transactionCode>" +
                "<transRefGUID/>" +
                "<esbRefGUID/>" +
                "<transNo/>" +
                "<businessCode/>" +
                "<businessType/>" +
                "</sysHeader>" +
                "<domHeader/>" +
                "<bizHeader/>" +
                "</head>" +
                "<body>" +
                "<queryInfos>" +
                "<queryInfo>" +
                "<approveTypeCode>04</approveTypeCode><!--审批类型编码  04为借款人保险额度调整-->" +
                "<approveExtendInfos>" +
                "<approveExtendInfo>" +
                "<approveExtendInfoCode>pastClaimNo</approveExtendInfoCode>" +
                "<approveExtendInfoValue/>" +
                "</approveExtendInfo>" +
                "<approveExtendInfo>" +
                "<approveExtendInfoCode>loanContractNo</approveExtendInfoCode><!--借款合同号-->" +
                "<approveExtendInfoValue></approveExtendInfoValue>" +
                "</approveExtendInfo>" +
                "<approveExtendInfo>" +
                "<approveExtendInfoCode>sumInsured</approveExtendInfoCode>" +
                "<approveExtendInfoValue/>" +
                "</approveExtendInfo>" +
                "<approveExtendInfo>" +
                "<approveExtendInfoCode>signedInsured</approveExtendInfoCode>" +
                "<approveExtendInfoValue/>" +
                "</approveExtendInfo>" +
                "<approveExtendInfo>" +
                "<approveExtendInfoCode>riskCode</approveExtendInfoCode><!--险种编码-->" +
                "<approveExtendInfoValue>01</approveExtendInfoValue>" +
                "</approveExtendInfo>" +
                "<approveExtendInfo>" +
                "<approveExtendInfoCode>riskName</approveExtendInfoCode><!--险种名称-->" +
                "<approveExtendInfoValue>借款人意外伤害保险</approveExtendInfoValue>" +
                "</approveExtendInfo>" +
                "<approveExtendInfo>" +
                "<approveExtendInfoCode>name</approveExtendInfoCode><!--被保人姓名-->" +
                "<approveExtendInfoValue>状态测试</approveExtendInfoValue>" +
                "</approveExtendInfo>" +
                "<approveExtendInfo>" +
                "<approveExtendInfoCode>sex</approveExtendInfoCode><!--被保人性别-->" +
                "<approveExtendInfoValue>M</approveExtendInfoValue>" +
                "</approveExtendInfo>" +
                "<approveExtendInfo>" +
                "<approveExtendInfoCode>age</approveExtendInfoCode><!--被保人年龄-->" +
                "<approveExtendInfoValue>26</approveExtendInfoValue>" +
                "</approveExtendInfo>" +
                "<approveExtendInfo>" +
                "<approveExtendInfoCode>birthday</approveExtendInfoCode><!--被保人生日-->" +
                "<approveExtendInfoValue>1992-06-11</approveExtendInfoValue>" +
                "</approveExtendInfo>" +
                "<approveExtendInfo>" +
                "<approveExtendInfoCode>idType</approveExtendInfoCode><!--被保人身份证类别-->" +
                "<approveExtendInfoValue>02</approveExtendInfoValue>" +
                "</approveExtendInfo>" +
                "<approveExtendInfo>" +
                "<approveExtendInfoCode>idNo</approveExtendInfoCode><!--被保人身份证号-->" +
                "<approveExtendInfoValue>13234816</approveExtendInfoValue>" +
                "</approveExtendInfo>" +
                "<approveExtendInfo>" +
                "<approveExtendInfoCode>manageCom</approveExtendInfoCode><!--管理机构-->" +
                "<approveExtendInfoValue/>" +
                "</approveExtendInfo>" +
                "<approveExtendInfo>" +
                "<approveExtendInfoCode>saleChnl</approveExtendInfoCode><!--销售渠道-->" +
                "<approveExtendInfoValue/>" +
                "</approveExtendInfo>" +
                "<approveExtendInfo>" +
                "<approveExtendInfoCode>agentCom</approveExtendInfoCode><!--销售渠道-->" +
                "<approveExtendInfoValue/>" +
                "</approveExtendInfo>" +
                "<approveExtendInfo>" +
                "<approveExtendInfoCode>salemanName</approveExtendInfoCode>" +
                "<approveExtendInfoValue/>" +
                "</approveExtendInfo>" +
                "<approveExtendInfo>" +
                "<approveExtendInfoCode>occupationDesc</approveExtendInfoCode>" +
                "<approveExtendInfoValue/>" +
                "</approveExtendInfo>" +
                "<approveExtendInfo>" +
                "<approveExtendInfoCode>occupationType</approveExtendInfoCode><!--职业类别-->" +
                "<approveExtendInfoValue>1</approveExtendInfoValue>" +
                "</approveExtendInfo>" +
                "<approveExtendInfo>" +
                "<approveExtendInfoCode>loanValue</approveExtendInfoCode><!--借款金额-->" +
                "<approveExtendInfoValue>10000000</approveExtendInfoValue>" +
                "</approveExtendInfo>" +
                "<approveExtendInfo>" +
                "<approveExtendInfoCode>plan</approveExtendInfoCode>" +
                "<approveExtendInfoValue>1</approveExtendInfoValue>" +
                "</approveExtendInfo>" +
                "<approveExtendInfo>" +
                "<approveExtendInfoCode>rate</approveExtendInfoCode>" +
                "<approveExtendInfoValue/>" +
                "</approveExtendInfo>" +
                "<approveExtendInfo>" +
                "<approveExtendInfoCode>insuYearFlag</approveExtendInfoCode><!--保险年龄年期标志-->" +
                "<approveExtendInfoValue>M</approveExtendInfoValue>" +
                "</approveExtendInfo>" +
                "<approveExtendInfo>" +
                "<approveExtendInfoCode>insuYear</approveExtendInfoCode><!--保险年龄年期-->" +
                "<approveExtendInfoValue>12</approveExtendInfoValue>" +
                "</approveExtendInfo>" +
                "<approveExtendInfo>" +
                "<approveExtendInfoCode>sumFee</approveExtendInfoCode><!--总保费-->" +
                "<approveExtendInfoValue>20000</approveExtendInfoValue>" +
                "</approveExtendInfo>" +
                "<approveExtendInfo>" +
                "<approveExtendInfoCode>sumAmnt</approveExtendInfoCode><!--总保额-->" +
                "<approveExtendInfoValue>100000000000000</approveExtendInfoValue>" +
                "</approveExtendInfo>" +
                "<approveExtendInfo>" +
                "<approveExtendInfoCode>rateRebate</approveExtendInfoCode>" +
                "<approveExtendInfoValue/>" +
                "</approveExtendInfo>" +
                "</approveExtendInfos>" +
                "</queryInfo>" +
                "</queryInfos>" +
                "</body>" +
                "</message>";
    }
    public String getXMLClaim(){
        return "<?xml version=\"1.0\" encoding=\"utf-8\"?>" +
                "<message>" +
                "<head>" +
                "<sysHeader>" +
                "<standardVersionCode/>" +
                "<msgDate>2018-06-11</msgDate>" +
                "<msgTime>10:40:56</msgTime>" +
                "<systemCode>AIP</systemCode>" +
                "<transactionCode>1011</transactionCode>" +
                "<transRefGUID/>" +
                "<esbRefGUID/>" +
                "<transNo/>" +
                "<businessCode/>" +
                "<businessType/>" +
                "</sysHeader>" +
                "<domHeader/>" +
                "<bizHeader/>" +
                "</head>" +
                "<body>" +
                "<queryInfos>" +
                "<queryInfo>" +
                "<approveTypeCode>02</approveTypeCode><!--审批类型编码  04为借款人保险额度调整-->" +
                "<approveExtendInfos>" +
                "<approveExtendInfo>" +
                "<approveExtendInfoCode>pastClaimNo</approveExtendInfoCode>" +
                "<approveExtendInfoValue/>" +
                "</approveExtendInfo>" +
                "<approveExtendInfo>" +
                "<approveExtendInfoCode>loanContractNo</approveExtendInfoCode><!--借款合同号-->" +
                "<approveExtendInfoValue></approveExtendInfoValue>" +
                "</approveExtendInfo>" +
                "<approveExtendInfo>" +
                "<approveExtendInfoCode>sumInsured</approveExtendInfoCode>" +
                "<approveExtendInfoValue/>" +
                "</approveExtendInfo>" +
                "<approveExtendInfo>" +
                "<approveExtendInfoCode>signedInsured</approveExtendInfoCode>" +
                "<approveExtendInfoValue/>" +
                "</approveExtendInfo>" +
                "<approveExtendInfo>" +
                "<approveExtendInfoCode>riskCode</approveExtendInfoCode><!--险种编码-->" +
                "<approveExtendInfoValue>01</approveExtendInfoValue>" +
                "</approveExtendInfo>" +
                "<approveExtendInfo>" +
                "<approveExtendInfoCode>riskName</approveExtendInfoCode><!--险种名称-->" +
                "<approveExtendInfoValue>借款人意外伤害保险</approveExtendInfoValue>" +
                "</approveExtendInfo>" +
                "<approveExtendInfo>" +
                "<approveExtendInfoCode>name</approveExtendInfoCode><!--被保人姓名-->" +
                "<approveExtendInfoValue>马长威</approveExtendInfoValue>" +
                "</approveExtendInfo>" +
                "<approveExtendInfo>" +
                "<approveExtendInfoCode>sex</approveExtendInfoCode><!--被保人性别-->" +
                "<approveExtendInfoValue>M</approveExtendInfoValue>" +
                "</approveExtendInfo>" +
                "<approveExtendInfo>" +
                "<approveExtendInfoCode>age</approveExtendInfoCode><!--被保人年龄-->" +
                "<approveExtendInfoValue>26</approveExtendInfoValue>" +
                "</approveExtendInfo>" +
                "<approveExtendInfo>" +
                "<approveExtendInfoCode>birthday</approveExtendInfoCode><!--被保人生日-->" +
                "<approveExtendInfoValue>1992-06-11</approveExtendInfoValue>" +
                "</approveExtendInfo>" +
                "<approveExtendInfo>" +
                "<approveExtendInfoCode>idType</approveExtendInfoCode><!--被保人身份证类别-->" +
                "<approveExtendInfoValue>01</approveExtendInfoValue>" +
                "</approveExtendInfo>" +
                "<approveExtendInfo>" +
                "<approveExtendInfoCode>idNo</approveExtendInfoCode><!--被保人身份证号-->" +
                "<approveExtendInfoValue>432503198201192198</approveExtendInfoValue>" +
                "</approveExtendInfo>" +
                "<approveExtendInfo>" +
                "<approveExtendInfoCode>manageCom</approveExtendInfoCode><!--管理机构-->" +
                "<approveExtendInfoValue/>" +
                "</approveExtendInfo>" +
                "<approveExtendInfo>" +
                "<approveExtendInfoCode>saleChnl</approveExtendInfoCode><!--销售渠道-->" +
                "<approveExtendInfoValue/>" +
                "</approveExtendInfo>" +
                "<approveExtendInfo>" +
                "<approveExtendInfoCode>agentCom</approveExtendInfoCode><!--销售渠道-->" +
                "<approveExtendInfoValue/>" +
                "</approveExtendInfo>" +
                "<approveExtendInfo>" +
                "<approveExtendInfoCode>salemanName</approveExtendInfoCode>" +
                "<approveExtendInfoValue/>" +
                "</approveExtendInfo>" +
                "<approveExtendInfo>" +
                "<approveExtendInfoCode>occupationDesc</approveExtendInfoCode>" +
                "<approveExtendInfoValue/>" +
                "</approveExtendInfo>" +
                "<approveExtendInfo>" +
                "<approveExtendInfoCode>occupationType</approveExtendInfoCode><!--职业类别-->" +
                "<approveExtendInfoValue>1</approveExtendInfoValue>" +
                "</approveExtendInfo>" +
                "<approveExtendInfo>" +
                "<approveExtendInfoCode>loanValue</approveExtendInfoCode><!--借款金额-->" +
                "<approveExtendInfoValue>10000000</approveExtendInfoValue>" +
                "</approveExtendInfo>" +
                "<approveExtendInfo>" +
                "<approveExtendInfoCode>plan</approveExtendInfoCode>" +
                "<approveExtendInfoValue>3</approveExtendInfoValue>" +
                "</approveExtendInfo>" +
                "<approveExtendInfo>" +
                "<approveExtendInfoCode>rate</approveExtendInfoCode>" +
                "<approveExtendInfoValue/>" +
                "</approveExtendInfo>" +
                "<approveExtendInfo>" +
                "<approveExtendInfoCode>insuYearFlag</approveExtendInfoCode><!--保险年龄年期标志-->" +
                "<approveExtendInfoValue>M</approveExtendInfoValue>" +
                "</approveExtendInfo>" +
                "<approveExtendInfo>" +
                "<approveExtendInfoCode>insuYear</approveExtendInfoCode><!--保险年龄年期-->" +
                "<approveExtendInfoValue>12</approveExtendInfoValue>" +
                "</approveExtendInfo>" +
                "<approveExtendInfo>" +
                "<approveExtendInfoCode>sumFee</approveExtendInfoCode><!--总保费-->" +
                "<approveExtendInfoValue>20000</approveExtendInfoValue>" +
                "</approveExtendInfo>" +
                "<approveExtendInfo>" +
                "<approveExtendInfoCode>sumAmnt</approveExtendInfoCode><!--总保额-->" +
                "<approveExtendInfoValue>10000000</approveExtendInfoValue>" +
                "</approveExtendInfo>" +
                "<approveExtendInfo>" +
                "<approveExtendInfoCode>rateRebate</approveExtendInfoCode>" +
                "<approveExtendInfoValue/>" +
                "</approveExtendInfo>" +
                "</approveExtendInfos>" +
                "</queryInfo>" +
                "</queryInfos>" +
                "</body>" +
                "</message>";
    }

    public String getXML(){
        return "<?xml version=\"1.0\" encoding=\"utf-8\"?>" +
                "<message>" +
                "<head>" +
                "<sysHeader>" +
                "<standardVersionCode/>" +
                "<msgDate>2018-06-11</msgDate>" +
                "<msgTime>11:20:30</msgTime>" +
                "<systemCode>AIP</systemCode>" +
                "<transactionCode>1011</transactionCode>" +
                "<transRefGUID/>" +
                "<esbRefGUID/>" +
                "<transNo/>" +
                "<businessCode/>" +
                "<businessType/>" +
                "</sysHeader>" +
                "<domHeader/>" +
                "<bizHeader/>" +
                "</head>" +
                "<body>" +
                "<lcInsuredInfo>" +
                "<name>中国人</name>" +
                "<sex>M</sex>" +
                "<idType>02</idType>" +
                "<idNo>ZXCVBN</idNo>" +
                "<birthday>1992-06-11</birthday>" +
                "</lcInsuredInfo>" +
                "<loanContractNo>YUI123123412312321</loanContractNo>" +
                "<approveInfos>" +
                "<approveInfo>" +
                "<approveTypeCode>01,02,03,04</approveTypeCode>" +
                "</approveInfo>" +
                "</approveInfos>" +
                "<updateInfo>" +
                "<contNo>007679851205020</contNo>" +
                "<useState>01</useState>" +
                "</updateInfo>" +
                "</body>" +
                "</message>";
    }


    public String getXMLUW(){
        return "<?xml version=\"1.0\" encoding=\"utf-8\"?>" +
                "<message>" +
                "<head>" +
                "<sysHeader>" +
                "<standardVersionCode/>" +
                "<msgDate>2018-06-11</msgDate>" +
                "<msgTime>10:40:56</msgTime>" +
                "<systemCode>AIP</systemCode>" +
                "<transactionCode>1011</transactionCode>" +
                "<transRefGUID/>" +
                "<esbRefGUID/>" +
                "<transNo/>" +
                "<businessCode/>" +
                "<businessType/>" +
                "</sysHeader>" +
                "<domHeader/>" +
                "<bizHeader/>" +
                "</head>" +
                "<body>" +
                "<queryInfos>" +
                "<queryInfo>" +
                "<approveTypeCode>01</approveTypeCode><!--审批类型编码  04为借款人保险额度调整-->" +
                "<approveExtendInfos>" +
                "<approveExtendInfo>" +
                "<approveExtendInfoCode>pastClaimNo</approveExtendInfoCode>" +
                "<approveExtendInfoValue/>" +
                "</approveExtendInfo>" +
                "<approveExtendInfo>" +
                "<approveExtendInfoCode>loanContractNo</approveExtendInfoCode><!--借款合同号-->" +
                "<approveExtendInfoValue></approveExtendInfoValue>" +
                "</approveExtendInfo>" +
                "<approveExtendInfo>" +
                "<approveExtendInfoCode>sumInsured</approveExtendInfoCode>" +
                "<approveExtendInfoValue/>" +
                "</approveExtendInfo>" +
                "<approveExtendInfo>" +
                "<approveExtendInfoCode>signedInsured</approveExtendInfoCode>" +
                "<approveExtendInfoValue/>" +
                "</approveExtendInfo>" +
                "<approveExtendInfo>" +
                "<approveExtendInfoCode>riskCode</approveExtendInfoCode><!--险种编码-->" +
                "<approveExtendInfoValue>01</approveExtendInfoValue>" +
                "</approveExtendInfo>" +
                "<approveExtendInfo>" +
                "<approveExtendInfoCode>riskName</approveExtendInfoCode><!--险种名称-->" +
                "<approveExtendInfoValue>借款人意外伤害保险</approveExtendInfoValue>" +
                "</approveExtendInfo>" +
                "<approveExtendInfo>" +
                "<approveExtendInfoCode>name</approveExtendInfoCode><!--被保人姓名-->" +
                "<approveExtendInfoValue>马长威</approveExtendInfoValue>" +
                "</approveExtendInfo>" +
                "<approveExtendInfo>" +
                "<approveExtendInfoCode>sex</approveExtendInfoCode><!--被保人性别-->" +
                "<approveExtendInfoValue>M</approveExtendInfoValue>" +
                "</approveExtendInfo>" +
                "<approveExtendInfo>" +
                "<approveExtendInfoCode>age</approveExtendInfoCode><!--被保人年龄-->" +
                "<approveExtendInfoValue>26</approveExtendInfoValue>" +
                "</approveExtendInfo>" +
                "<approveExtendInfo>" +
                "<approveExtendInfoCode>birthday</approveExtendInfoCode><!--被保人生日-->" +
                "<approveExtendInfoValue>1992-06-11</approveExtendInfoValue>" +
                "</approveExtendInfo>" +
                "<approveExtendInfo>" +
                "<approveExtendInfoCode>idType</approveExtendInfoCode><!--被保人身份证类别-->" +
                "<approveExtendInfoValue>02</approveExtendInfoValue>" +
                "</approveExtendInfo>" +
                "<approveExtendInfo>" +
                "<approveExtendInfoCode>idNo</approveExtendInfoCode><!--被保人身份证号-->" +
                "<approveExtendInfoValue>F123444</approveExtendInfoValue>" +
                "</approveExtendInfo>" +
                "<approveExtendInfo>" +
                "<approveExtendInfoCode>manageCom</approveExtendInfoCode><!--管理机构-->" +
                "<approveExtendInfoValue/>" +
                "</approveExtendInfo>" +
                "<approveExtendInfo>" +
                "<approveExtendInfoCode>saleChnl</approveExtendInfoCode><!--销售渠道-->" +
                "<approveExtendInfoValue/>" +
                "</approveExtendInfo>" +
                "<approveExtendInfo>" +
                "<approveExtendInfoCode>agentCom</approveExtendInfoCode><!--销售渠道-->" +
                "<approveExtendInfoValue/>" +
                "</approveExtendInfo>" +
                "<approveExtendInfo>" +
                "<approveExtendInfoCode>salemanName</approveExtendInfoCode>" +
                "<approveExtendInfoValue/>" +
                "</approveExtendInfo>" +
                "<approveExtendInfo>" +
                "<approveExtendInfoCode>occupationDesc</approveExtendInfoCode>" +
                "<approveExtendInfoValue/>" +
                "</approveExtendInfo>" +
                "<approveExtendInfo>" +
                "<approveExtendInfoCode>occupationType</approveExtendInfoCode><!--职业类别-->" +
                "<approveExtendInfoValue>1</approveExtendInfoValue>" +
                "</approveExtendInfo>" +
                "<approveExtendInfo>" +
                "<approveExtendInfoCode>loanValue</approveExtendInfoCode><!--借款金额-->" +
                "<approveExtendInfoValue>10000000</approveExtendInfoValue>" +
                "</approveExtendInfo>" +
                "<approveExtendInfo>" +
                "<approveExtendInfoCode>plan</approveExtendInfoCode>" +
                "<approveExtendInfoValue>3</approveExtendInfoValue>" +
                "</approveExtendInfo>" +
                "<approveExtendInfo>" +
                "<approveExtendInfoCode>rate</approveExtendInfoCode>" +
                "<approveExtendInfoValue/>" +
                "</approveExtendInfo>" +
                "<approveExtendInfo>" +
                "<approveExtendInfoCode>insuYearFlag</approveExtendInfoCode><!--保险年龄年期标志-->" +
                "<approveExtendInfoValue>M</approveExtendInfoValue>" +
                "</approveExtendInfo>" +
                "<approveExtendInfo>" +
                "<approveExtendInfoCode>insuYear</approveExtendInfoCode><!--保险年龄年期-->" +
                "<approveExtendInfoValue>12</approveExtendInfoValue>" +
                "</approveExtendInfo>" +
                "<approveExtendInfo>" +
                "<approveExtendInfoCode>sumFee</approveExtendInfoCode><!--总保费-->" +
                "<approveExtendInfoValue>20000</approveExtendInfoValue>" +
                "</approveExtendInfo>" +
                "<approveExtendInfo>" +
                "<approveExtendInfoCode>sumAmnt</approveExtendInfoCode><!--总保额-->" +
                "<approveExtendInfoValue>10000000</approveExtendInfoValue>" +
                "</approveExtendInfo>" +
                "<approveExtendInfo>" +
                "<approveExtendInfoCode>rateRebate</approveExtendInfoCode>" +
                "<approveExtendInfoValue/>" +
                "</approveExtendInfo>" +
                "</approveExtendInfos>" +
                "</queryInfo>" +
                "</queryInfos>" +
                "</body>" +
                "</message>";
    }


    @Test
    public void getResponseXml() {
        TradeInfo tradeInfo = new TradeInfo();
        prepareData(tradeInfo);
        List<LCInsuredPojo> lcInsuredPojo = (List<LCInsuredPojo>) tradeInfo.getData(LCInsuredPojo.class.getName());
        lcInsuredPojo.get(0).setName("马长威");
        lcInsuredPojo.get(0).setSex("M");
        lcInsuredPojo.get(0).setBirthday("M");
        lcInsuredPojo.get(0).setIDType("02");
        lcInsuredPojo.get(0).setIDNo("ZXCVBN");
        LCPolicyInfoPojo lcPolicyInfoPojo = new LCPolicyInfoPojo();
        lcPolicyInfoPojo.setJYContNo("YUI123123412312321");
        lcPolicyInfoPojo.setLoanAmount("10000");
        tradeInfo.addData(LCPolicyInfoPojo.class.getName(), lcPolicyInfoPojo);
    }


    public void prepareData(TradeInfo tradeInfo) {
        String str = prepareStr();
        Reflections reflections = new Reflections();
        //截取trademap数据
        str = str.substring(str.indexOf("tradeMap={") + 10, str.indexOf("}"));
        String[] objs = str.split("com.sinosoft.lis.entity.");
        List<String> arrs = Arrays.asList(objs);
        int start = -1;
        int end = -1;
        int eq = -1;
        Map<String, String> map = new HashMap();
        //解析tradeinfo
        for (int i = 0; i < arrs.size(); i++) {
            String arr = arrs.get(i);
            start = arr.indexOf("[");
            end = arr.lastIndexOf("]");
            eq = arr.indexOf("=");
            if (start != -1 && end != -1 && eq != -1) {
                map.put(arr.substring(0, eq).trim(), arr.substring(start + 1, end));
            }
            start = arr.indexOf("{");
            end = arr.indexOf("}");
            if (start != -1 && end != -1 && end == arr.lastIndexOf("}")) {

            }
        }
        //解析类
        String value = null;
        Method setMethod = null;
        Class className = null;
        /*Field field = null;*/
        Object obj = null;
        for (String key : map.keySet()) {
            value = map.get(key);
            if ("".equals(value)) {
                continue;
            }
            //指定类
            try {
                className = Class.forName("com.sinosoft.lis.entity." + key);
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }

            start = value.indexOf("[");
            end = value.indexOf("]");
            //解析value中有几个[]，判断是集合对象还是类对象
            if (start == -1) {
                obj = analyseData(value, className, obj, setMethod);
                tradeInfo.addData(className.getName(), obj);
            } else {
                //判断是否存在字符串格式的数据
                if (value.lastIndexOf("]") != value.length() - 1) {
                    String otherVal = value.substring(value.lastIndexOf("]") + 2);
                    for (String val : otherVal.split(",")) {
                        tradeInfo.addData(val.split("=")[0].trim(), val.split("=")[1].trim());
                    }
                }
                //集合对象遍历
                List classList = new ArrayList();
                while (start != -1) {
                    classList.add(analyseData(value.substring(start + 1, end), className, obj, setMethod));
                    value = value.substring(end + 1);
                    start = value.indexOf("[");
                    end = value.indexOf("]");
                }
                if (classList.size() != 0) {
                    tradeInfo.addData(className.getName(), classList);
                }
            }
        }
        System.out.println("--------------------------------------------------------------");
        System.out.println("結果為：" + tradeInfo);
    }

    public Object analyseData(String value, Class className, Object obj, Method setMethod) {
        String[] vals = value.split(",");
        try {
            obj = className.newInstance();
            //遍历属性
            for (String var : vals) {
                if (var.split("=").length < 2) {
                    continue;
                }
                //变量名
                String na = var.split("=")[0].trim();
                //变量值
                String va = var.split("=")[1].trim();
                //指定类的成员变量
                /*field = className.getDeclaredField(na);*/
                //获取指定成员变量的set方法
                setMethod = className.getMethod("set" + na.substring(0, 1).toUpperCase() + na.substring(1), String.class);
                if (va != null && !"null".equals(va)) {
                    setMethod.invoke(obj, new Object[]{va});
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return obj;
    }

    public String prepareStr() {
        return "TradeInfo{serviceNode='', tradeMap={com.sinosoft.lis.entity.LCAppntLinkManInfoPojo=LCAppntLinkManInfoPojo [AppntLinkID=707316, ContID=707317, ShardingID=310083664, PrtNo=310083664, ContNo=201837050020010135, AppntNo=0013473459, Name=更好, Sex=null, Birthday=null, IDType=null, IDNo=null, RelationToApp=null, Tel1=null, Tel3=null, Tel4=null, Tel5=null, Tel2=null, Mobile1=null, Mobile2=null, Address=null, ZipCode=null, Email=null, ManageCom=86370500, Operator=ABC-CLOUD, MakeDate=2018-06-11, MakeTime=14:13:50, ModifyDate=2018-06-11, ModifyTime=14:13:50], healthNotice=N, com.sinosoft.lis.entity.LCInsuredPojo=[LCInsuredPojo [InsuredID=707319, ContID=707317, PersonID=693514, ShardingID=310083664, GrpContNo=00000000000000000000, ContNo=201837050020010135, InsuredNo=0013473459, PrtNo=310083664, AppntNo=0013473459, ManageCom=86370500, ExecuteCom=86370500, FamilyID=null, RelationToMainInsured=00, RelationToAppnt=00, AddressNo=707313, SequenceNo=1, Name=更好, Sex=0, Birthday=1987-09-24, IDType=0, IDNo=371401198709240036, NativePlace=CHN, Nationality=null, RgtAddress=null, Marriage=null, MarriageDate=null, Health=null, Stature=0.0, Avoirdupois=0.0, Degree=null, CreditGrade=null, BankCode=null, BankAccNo=null, AccName=null, JoinCompanyDate=null, StartWorkDate=null, Position=null, Salary=0.0, OccupationType=2, OccupationCode=1402010, WorkType=null, PluralityType=null, SmokeFlag=null, ContPlanCode=SEH, Operator=ABC-CLOUD, InsuredStat=null, MakeDate=2018-06-11, MakeTime=14:13:50, ModifyDate=2018-06-11, ModifyTime=14:13:50, UWFlag=null, UWCode=null, UWDate=null, UWTime=null, BMI=0.0, InsuredPeoples=0, License=null, LicenseType=null, CustomerSeqNo=0, WorkNo=null, SocialInsuNo=null, OccupationDesb=null, IdValiDate=2025-01-02, HaveMotorcycleLicence=null, PartTimeJob=null, HealthFlag=null, ServiceMark=null, FirstName=null, LastName=null, SSFlag=, TINNO=null, TINFlag=null, InsuredType=null, IsLongValid=null, IDStartDate=null]], com.sinosoft.lis.entity.LCGetPojo=[LCGetPojo [GetID=707338, DutyID=707333, ShardingID=310083664, GrpContNo=00000000000000000000, ContNo=201837050020010135, PolNo=024621136458026, DutyCode=681002, GetDutyCode=681042, GetDutyKind=null, InsuredNo=0013473459, GetMode=4, GetLimit=0.0, GetRate=0.0, UrgeGetFlag=N, LiveGetType=1, AddRate=0.0, CanGet=1, NeedAcc=0, NeedCancelAcc=0, StandMoney=1000000.0, ActuGet=1000000.0, SumMoney=0.0, GetIntv=0, GettoDate=2018-06-12, GetStartDate=2018-06-12, GetEndDate=2019-06-12, BalaDate=null, State=Y, ManageCom=86370500, Operator=ABC-CLOUD, MakeDate=2018-06-11, MakeTime=14:13:50, ModifyTime=14:13:50, ModifyDate=2018-06-11, GetEndState=null], LCGetPojo [GetID=707340, DutyID=707334, ShardingID=310083664, GrpContNo=00000000000000000000, ContNo=201837050020010135, PolNo=024621136458026, DutyCode=681003, GetDutyCode=681043, GetDutyKind=null, InsuredNo=0013473459, GetMode=4, GetLimit=0.0, GetRate=0.0, UrgeGetFlag=N, LiveGetType=1, AddRate=0.0, CanGet=1, NeedAcc=0, NeedCancelAcc=0, StandMoney=2000000.0, ActuGet=2000000.0, SumMoney=0.0, GetIntv=0, GettoDate=2018-06-12, GetStartDate=2018-06-12, GetEndDate=2019-06-12, BalaDate=null, State=Y, ManageCom=86370500, Operator=ABC-CLOUD, MakeDate=2018-06-11, MakeTime=14:13:50, ModifyTime=14:13:50, ModifyDate=2018-06-11, GetEndState=null], LCGetPojo [GetID=707342, DutyID=707335, ShardingID=310083664, GrpContNo=00000000000000000000, ContNo=201837050020010135, PolNo=024621136458026, DutyCode=681006, GetDutyCode=681046, GetDutyKind=null, InsuredNo=0013473459, GetMode=4, GetLimit=0.0, GetRate=0.0, UrgeGetFlag=N, LiveGetType=1, AddRate=0.0, CanGet=1, NeedAcc=0, NeedCancelAcc=0, StandMoney=1000000.0, ActuGet=1000000.0, SumMoney=0.0, GetIntv=0, GettoDate=2018-06-12, GetStartDate=2018-06-12, GetEndDate=2019-06-12, BalaDate=null, State=Y, ManageCom=86370500, Operator=ABC-CLOUD, MakeDate=2018-06-11, MakeTime=14:13:50, ModifyTime=14:13:50, ModifyDate=2018-06-11, GetEndState=null], LCGetPojo [GetID=707344, DutyID=707336, ShardingID=310083664, GrpContNo=00000000000000000000, ContNo=201837050020010135, PolNo=024621136458026, DutyCode=681007, GetDutyCode=681047, GetDutyKind=null, InsuredNo=0013473459, GetMode=4, GetLimit=0.0, GetRate=0.0, UrgeGetFlag=N, LiveGetType=1, AddRate=0.0, CanGet=1, NeedAcc=0, NeedCancelAcc=0, StandMoney=500000.0, ActuGet=500000.0, SumMoney=0.0, GetIntv=0, GettoDate=2018-06-12, GetStartDate=2018-06-12, GetEndDate=2019-06-12, BalaDate=null, State=Y, ManageCom=86370500, Operator=ABC-CLOUD, MakeDate=2018-06-11, MakeTime=14:13:50, ModifyTime=14:13:50, ModifyDate=2018-06-11, GetEndState=null]], com.sinosoft.lis.entity.LCCustomerImpartDetailPojo=[], com.sinosoft.lis.entity.LCPolPojo=[LCPolPojo [PolID=707331, ContID=707317, ShardingID=310083664, GrpContNo=00000000000000000000, GrpPolNo=00000000000000000000, ContNo=201837050020010135, PolNo=024621136458026, ProposalNo=024621136458026, PrtNo=310083664, ContType=2, PolTypeFlag=0, MainPolNo=024621136458026, MasterPolNo=null, KindCode=A, RiskCode=6810, RiskVersion=2002, ManageCom=86370500, AgentCom=0510020520, AgentType=08, AgentCode=0000068812, AgentGroup=000000023963, AgentCode1=null, SaleChnl=2, Handler=null, InsuredNo=0013473459, InsuredName=更好, InsuredSex=0, InsuredBirthday=1987-09-24, InsuredAppAge=30, InsuredPeoples=1, OccupationType=2, AppntNo=0013473459, AppntName=更好, CValiDate=2018-06-12, SignCom=86370500, SignDate=null, SignTime=null, FirstPayDate=null, PayEndDate=2019-06-12, PaytoDate=null, GetStartDate=2018-06-12, EndDate=2019-06-12, AcciEndDate=null, GetYearFlag=Y, GetYear=0, PayEndYearFlag=Y, PayEndYear=1000, InsuYearFlag=M, InsuYear=12, AcciYearFlag=null, AcciYear=0, GetStartType=null, SpecifyValiDate=N, PayMode=C, PayLocation=null, PayIntv=0, PayYears=1, Years=1, ManageFeeRate=0.0, FloatRate=0.0, PremToAmnt=null, Mult=0.0, StandPrem=98.0, Prem=98.0, SumPrem=0.0, Amnt=4500000.0, RiskAmnt=4500000.0, LeavingMoney=0.0, EndorseTimes=0, ClaimTimes=0, LiveTimes=0, RenewCount=0, LastGetDate=null, LastLoanDate=null, LastRegetDate=null, LastEdorDate=null, LastRevDate=2018-06-12, RnewFlag=-9, StopFlag=null, ExpiryFlag=null, AutoPayFlag=0, InterestDifFlag=null, SubFlag=null, BnfFlag=0, HealthCheckFlag=null, ImpartFlag=0, ReinsureFlag=null, AgentPayFlag=null, AgentGetFlag=null, LiveGetMode=4, DeadGetMode=null, BonusGetMode=null, BonusMan=0, DeadFlag=null, SmokeFlag=null, Remark=null, ApproveFlag=0, ApproveCode=null, ApproveDate=null, ApproveTime=null, UWFlag=0, UWCode=null, UWDate=null, UWTime=null, PolApplyDate=2018-06-11, AppFlag=0, PolState=0, StandbyFlag1=null, StandbyFlag2=null, StandbyFlag3=null, Operator=ABC-CLOUD, MakeDate=2018-06-11, MakeTime=14:13:50, ModifyDate=2018-06-11, ModifyTime=14:13:50, WaitPeriod=0, GetForm=null, GetBankCode=03, GetBankAccNo=63201366469950256561, GetAccName=更好, KeepValueOpt=null, AscriptionRuleCode=null, PayRuleCode=null, AscriptionFlag=null, AutoPubAccFlag=null, CombiFlag=null, InvestRuleCode=null, UintLinkValiFlag=null, ProdSetCode=null, InsurPolFlag=0, NewReinsureFlag=null, LiveAccFlag=1, StandRatePrem=0.0, AppntFirstName=null, AppntLastName=null, InsuredFirstName=null, InsuredLastName=null, FQGetMode=null, GetPeriod=null, GetPeriodFlag=null, DelayPeriod=null, OtherAmnt=0.0, Relation=null, ReCusNo=null, AutoRnewAge=null, AutoRnewYear=null]], com.sinosoft.lis.entity.LDPersonPojo=[LDPersonPojo [PersonID=693514, ShardingID=30021414, CustomerNo=0013473459, Name=更好, Sex=0, Birthday=1987-09-24, IDType=0, IDNo=371401198709240036, Password=473459, NativePlace=CHN, Nationality=null, RgtAddress=null, Marriage=null, MarriageDate=null, Health=null, Stature=0.0, Avoirdupois=0.0, Degree=null, CreditGrade=null, OthIDType=null, OthIDNo=null, ICNo=null, GrpNo=null, JoinCompanyDate=null, StartWorkDate=null, Position=null, Salary=0.0, OccupationType=2, OccupationCode=1402010, WorkType=null, PluralityType=null, DeathDate=null, SmokeFlag=null, BlacklistFlag=null, Proterty=null, Remark=null, State=null, VIPValue=0, Operator=ABC-CLOUD, MakeDate=2018-05-28, MakeTime=18:06:58, ModifyDate=2018-05-28, ModifyTime=18:06:58, GrpName=null, License=null, LicenseType=null, SocialInsuNo=null, IdValiDate=2025-01-02, HaveMotorcycleLicence=null, PartTimeJob=null, HealthFlag=null, ServiceMark=null, FirstName=null, LastName=null, CUSLevel=null, SSFlag=, RgtTpye=1, TINNO=null, TINFlag=null, NewCustomerFlag=OLD]], dealGroupFlag=G, com.sinosoft.lis.entity.LCPolAttachPojo=[], autoRenew=-9, com.sinosoft.lis.entity.LCDutyPojo=[LCDutyPojo [DutyID=707333, PolID=707331, ShardingID=310083664, PolNo=024621136458026, DutyCode=681002, ContNo=201837050020010135, Mult=0.0, StandPrem=15.0, Prem=15.0, SumPrem=15.0, Amnt=1000000.0, RiskAmnt=1000000.0, PayIntv=0, PayYears=1, Years=1, FloatRate=1.0, FirstPayDate=2018-06-12, FirstMonth=0, PaytoDate=null, PayEndDate=2019-06-12, PayEndYearFlag=Y, PayEndYear=1, GetYearFlag=Y, GetYear=0, InsuYearFlag=M, InsuYear=12, AcciYearFlag=null, AcciYear=0, EndDate=2019-06-12, AcciEndDate=null, FreeFlag=null, FreeRate=0.0, FreeStartDate=null, FreeEndDate=null, GetStartDate=2018-06-12, GetStartType=null, LiveGetMode=4, DeadGetMode=null, BonusGetMode=null, SSFlag=, PeakLine=0.0, GetLimit=0.0, GetRate=0.0, CalRule=3, PremToAmnt=G, StandbyFlag1=null, StandbyFlag2=null, StandbyFlag3=null, Operator=ABC-CLOUD, MakeDate=2018-06-11, MakeTime=14:13:50, ModifyDate=2018-06-11, ModifyTime=14:13:50, CValiDate=2018-06-12, GetIntv=0, AscriptionRuleCode=null, PayRuleCode=null, StandRatePrem=0.0, PeriodPrem=0.0], LCDutyPojo [DutyID=707334, PolID=707331, ShardingID=310083664, PolNo=024621136458026, DutyCode=681003, ContNo=201837050020010135, Mult=0.0, StandPrem=30.0, Prem=30.0, SumPrem=30.0, Amnt=2000000.0, RiskAmnt=2000000.0, PayIntv=0, PayYears=1, Years=1, FloatRate=1.0, FirstPayDate=2018-06-12, FirstMonth=0, PaytoDate=null, PayEndDate=2019-06-12, PayEndYearFlag=Y, PayEndYear=1, GetYearFlag=Y, GetYear=0, InsuYearFlag=M, InsuYear=12, AcciYearFlag=null, AcciYear=0, EndDate=2019-06-12, AcciEndDate=null, FreeFlag=null, FreeRate=0.0, FreeStartDate=null, FreeEndDate=null, GetStartDate=2018-06-12, GetStartType=null, LiveGetMode=4, DeadGetMode=null, BonusGetMode=null, SSFlag=, PeakLine=0.0, GetLimit=0.0, GetRate=0.0, CalRule=3, PremToAmnt=G, StandbyFlag1=null, StandbyFlag2=null, StandbyFlag3=null, Operator=ABC-CLOUD, MakeDate=2018-06-11, MakeTime=14:13:50, ModifyDate=2018-06-11, ModifyTime=14:13:50, CValiDate=2018-06-12, GetIntv=0, AscriptionRuleCode=null, PayRuleCode=null, StandRatePrem=0.0, PeriodPrem=0.0], LCDutyPojo [DutyID=707335, PolID=707331, ShardingID=310083664, PolNo=024621136458026, DutyCode=681006, ContNo=201837050020010135, Mult=0.0, StandPrem=15.0, Prem=15.0, SumPrem=15.0, Amnt=1000000.0, RiskAmnt=1000000.0, PayIntv=0, PayYears=1, Years=1, FloatRate=1.0, FirstPayDate=2018-06-12, FirstMonth=0, PaytoDate=null, PayEndDate=2019-06-12, PayEndYearFlag=Y, PayEndYear=1, GetYearFlag=Y, GetYear=0, InsuYearFlag=M, InsuYear=12, AcciYearFlag=null, AcciYear=0, EndDate=2019-06-12, AcciEndDate=null, FreeFlag=null, FreeRate=0.0, FreeStartDate=null, FreeEndDate=null, GetStartDate=2018-06-12, GetStartType=null, LiveGetMode=4, DeadGetMode=null, BonusGetMode=null, SSFlag=, PeakLine=0.0, GetLimit=0.0, GetRate=0.0, CalRule=3, PremToAmnt=G, StandbyFlag1=null, StandbyFlag2=null, StandbyFlag3=null, Operator=ABC-CLOUD, MakeDate=2018-06-11, MakeTime=14:13:50, ModifyDate=2018-06-11, ModifyTime=14:13:50, CValiDate=2018-06-12, GetIntv=0, AscriptionRuleCode=null, PayRuleCode=null, StandRatePrem=0.0, PeriodPrem=0.0], LCDutyPojo [DutyID=707336, PolID=707331, ShardingID=310083664, PolNo=024621136458026, DutyCode=681007, ContNo=201837050020010135, Mult=0.0, StandPrem=38.0, Prem=38.0, SumPrem=38.0, Amnt=500000.0, RiskAmnt=500000.0, PayIntv=0, PayYears=1, Years=1, FloatRate=1.0, FirstPayDate=2018-06-12, FirstMonth=0, PaytoDate=null, PayEndDate=2019-06-12, PayEndYearFlag=Y, PayEndYear=1, GetYearFlag=Y, GetYear=0, InsuYearFlag=M, InsuYear=12, AcciYearFlag=null, AcciYear=0, EndDate=2019-06-12, AcciEndDate=null, FreeFlag=null, FreeRate=0.0, FreeStartDate=null, FreeEndDate=null, GetStartDate=2018-06-12, GetStartType=null, LiveGetMode=4, DeadGetMode=null, BonusGetMode=null, SSFlag=, PeakLine=0.0, GetLimit=0.0, GetRate=0.0, CalRule=3, PremToAmnt=G, StandbyFlag1=null, StandbyFlag2=null, StandbyFlag3=null, Operator=ABC-CLOUD, MakeDate=2018-06-11, MakeTime=14:13:50, ModifyDate=2018-06-11, ModifyTime=14:13:50, CValiDate=2018-06-12, GetIntv=0, AscriptionRuleCode=null, PayRuleCode=null, StandRatePrem=0.0, PeriodPrem=0.0]], com.sinosoft.lis.entity.LCAccountPojo=LCAccountPojo [AccountID=707332, PersonID=693514, ShardingID=310083664, CustomerNo=0013473459, AccKind=1, BankCode=03, BankAccNo=63201366469950256561, AccName=更好, Operator=ABC-CLOUD, MakeDate=2018-06-11, MakeTime=14:13:50, ModifyDate=2018-06-11, ModifyTime=14:13:50], otherNo=null, com.sinosoft.lis.entity.LCPremPojo=[LCPremPojo [PremID=707337, DutyID=707333, ShardingID=310083664, GrpContNo=00000000000000000000, ContNo=201837050020010135, PolNo=024621136458026, DutyCode=681002, PayPlanCode=681022, PayPlanType=0, AppntType=2, AppntNo=0013473459, UrgePayFlag=N, NeedAcc=0, PayTimes=0, Rate=0.0, PayStartDate=2018-06-12, PayEndDate=2019-06-12, PaytoDate=null, PayIntv=0, StandPrem=15.0, Prem=15.0, SumPrem=15.0, SuppRiskScore=0.0, FreeFlag=null, FreeRate=0.0, FreeStartDate=null, FreeEndDate=null, State=0, ManageCom=86370500, Operator=ABC-CLOUD, MakeDate=2018-06-11, MakeTime=14:13:50, ModifyDate=2018-06-11, ModifyTime=14:13:50, AddFeeDirect=null, SecInsuAddPoint=0.0, AddForm=null, PeriodPrem=0.0, PayType=null], LCPremPojo [PremID=707339, DutyID=707334, ShardingID=310083664, GrpContNo=00000000000000000000, ContNo=201837050020010135, PolNo=024621136458026, DutyCode=681003, PayPlanCode=681023, PayPlanType=0, AppntType=2, AppntNo=0013473459, UrgePayFlag=N, NeedAcc=0, PayTimes=0, Rate=0.0, PayStartDate=2018-06-12, PayEndDate=2019-06-12, PaytoDate=null, PayIntv=0, StandPrem=30.0, Prem=30.0, SumPrem=30.0, SuppRiskScore=0.0, FreeFlag=null, FreeRate=0.0, FreeStartDate=null, FreeEndDate=null, State=0, ManageCom=86370500, Operator=ABC-CLOUD, MakeDate=2018-06-11, MakeTime=14:13:50, ModifyDate=2018-06-11, ModifyTime=14:13:50, AddFeeDirect=null, SecInsuAddPoint=0.0, AddForm=null, PeriodPrem=0.0, PayType=null], LCPremPojo [PremID=707341, DutyID=707335, ShardingID=310083664, GrpContNo=00000000000000000000, ContNo=201837050020010135, PolNo=024621136458026, DutyCode=681006, PayPlanCode=681026, PayPlanType=0, AppntType=2, AppntNo=0013473459, UrgePayFlag=N, NeedAcc=0, PayTimes=0, Rate=0.0, PayStartDate=2018-06-12, PayEndDate=2019-06-12, PaytoDate=null, PayIntv=0, StandPrem=15.0, Prem=15.0, SumPrem=15.0, SuppRiskScore=0.0, FreeFlag=null, FreeRate=0.0, FreeStartDate=null, FreeEndDate=null, State=0, ManageCom=86370500, Operator=ABC-CLOUD, MakeDate=2018-06-11, MakeTime=14:13:50, ModifyDate=2018-06-11, ModifyTime=14:13:50, AddFeeDirect=null, SecInsuAddPoint=0.0, AddForm=null, PeriodPrem=0.0, PayType=null], LCPremPojo [PremID=707343, DutyID=707336, ShardingID=310083664, GrpContNo=00000000000000000000, ContNo=201837050020010135, PolNo=024621136458026, DutyCode=681007, PayPlanCode=681027, PayPlanType=0, AppntType=2, AppntNo=0013473459, UrgePayFlag=N, NeedAcc=0, PayTimes=0, Rate=0.0, PayStartDate=2018-06-12, PayEndDate=2019-06-12, PaytoDate=null, PayIntv=0, StandPrem=38.0, Prem=38.0, SumPrem=38.0, SuppRiskScore=0.0, FreeFlag=null, FreeRate=0.0, FreeStartDate=null, FreeEndDate=null, State=0, ManageCom=86370500, Operator=ABC-CLOUD, MakeDate=2018-06-11, MakeTime=14:13:50, ModifyDate=2018-06-11, ModifyTime=14:13:50, AddFeeDirect=null, SecInsuAddPoint=0.0, AddForm=null, PeriodPrem=0.0, PayType=null]], com.sinosoft.lis.entity.GlobalPojo=GlobalPojo[BankCode='0501', ZoneNo='37', BrNo='3705001', TransNo='1002', MainRiskCode='6810', AutoRenew='null', SerialNo='2018060819120102323', TransDate='2018-06-11', TransTime='18:07:28', EntrustWay='ABC_EBank', ContPrtNo='', AgentPersonCode='', TransferFlag='', OldPolicy='', OldPolicyPwd='', OldVchNo='', TellerNo='', CorpNo='0510020520', FunctionFlag='null', InsuSerial='null', InterActNode='1', addPrem=0.0], ComCode=86370500, com.sinosoft.lis.entity.LCDutyAttachPojo=[], com.sinosoft.lis.entity.LCAppntPojo=LCAppntPojo [AppntID=707315, ContID=707317, PersonID=693514, ShardingID=310083664, GrpContNo=00000000000000000000, ContNo=201837050020010135, PrtNo=310083664, AppntNo=0013473459, AppntGrade=null, AppntName=更好, AppntSex=0, AppntBirthday=1987-09-24, AppntType=null, AddressNo=707313, IDType=0, IDNo=371401198709240036, NativePlace=CHN, Nationality=null, RgtAddress=null, Marriage=null, MarriageDate=null, Health=null, Stature=0.0, Avoirdupois=0.0, Degree=null, CreditGrade=null, BankCode=null, BankAccNo=null, AccName=null, JoinCompanyDate=null, StartWorkDate=null, Position=null, Salary=0.0, OccupationType=2, OccupationCode=1402010, WorkType=null, PluralityType=null, SmokeFlag=null, Operator=ABC-CLOUD, ManageCom=86370500, MakeDate=2018-06-11, MakeTime=14:13:50, ModifyDate=2018-06-11, ModifyTime=14:13:50, BMI=0.0, License=null, LicenseType=null, RelatToInsu=00, OccupationDesb=null, IdValiDate=2025-01-02, HaveMotorcycleLicence=null, PartTimeJob=null, FirstName=null, LastName=null, CUSLevel=null, AppRgtTpye=1, TINNO=null, TINFlag=null], com.sinosoft.lis.entity.LKTransStatusPojo=[LKTransStatusPojo [TransStatusID=707318, ShardingID=310083664, TransCode=2018060819120102323, ReportNo=null, BankCode=0501, BankBranch=37, BankNode=3705001, BankOperator=, TransNo=2018060819120102323, FuncFlag=1002, TransDate=2018-06-11, TransTime=18:07:28, ManageCom=null, RiskCode=null, ProposalNo=310083664, PrtNo=, PolNo=201837050020010135, EdorNo=null, TempFeeNo=null, TransAmnt=0.0, BankAcc=null, RCode=null, TransStatus=null, Status=null, Descr=null, Temp=null, MakeDate=2018-06-11, MakeTime=14:13:50, ModifyDate=2018-06-11, ModifyTime=14:13:50, State_Code=null, RequestId=null, OutServiceCode=null, ClientIP=null, ClientPort=null, IssueWay=null, ServiceStartTime=null, ServiceEndTime=null, RBankVSMP=null, DesBankVSMP=null, RMPVSKernel=null, DesMPVSKernel=null, ResultBalance=null, DesBalance=null, bak1=null, bak2=null, bak3=null, bak4=ABC_EBank]], com.sinosoft.lis.entity.LCSpecPojo=[], com.sinosoft.lis.entity.LCAddressPojo=[LCAddressPojo [AddressID=707314, PersonID=693514, ShardingID=310083664, CustomerNo=0013473459, AddressNo=707313, PostalAddress=山东省德州市德城区118号, ZipCode=061800, Phone=, Fax=null, HomeAddress=山东省德州市德城区118号, HomeZipCode=061800, HomePhone=, HomeFax=null, CompanyAddress=null, CompanyZipCode=null, CompanyPhone=, CompanyFax=null, Mobile=13741949841, MobileChs=null, EMail=zkrchenly@xxxx.cn, BP=null, Mobile2=null, MobileChs2=null, EMail2=null, BP2=null, Operator=ABC-CLOUD, MakeDate=2018-06-11, MakeTime=14:13:50, ModifyDate=2018-06-11, ModifyTime=14:13:50, GrpName=, Province=37, City=, County=, ZoneCode=null, StoreNo=null, StoreNo2=null]], com.sinosoft.lis.entity.VMS_OUTPUT_CUSTOMERPojo=VMS_OUTPUT_CUSTOMERPojo [VIC_INDATE=2018-06-11, VIC_CUSTOMER_ID=0013473459, VIC_CUSTOMER_CNAME=更好, VIC_CUSTOMER_TAXNO=, VIC_CUSTOMER_ACCOUNT=null, VIC_CUSTOMER_CBANK=中国农业银行, VIC_CUSTOMER_PHONE=13741949841, VIC_CUSTOMER_EMAIL=zkrchenly@xxxx.cn, VIC_CUSTOMER_ADDRESS=山东省德州市德城区118号, VIC_TAXPAYER_TYPE=O, VIC_FAPIAO_TYPE=0, VIC_CUSTOMER_TYPE=I, VIC_CUSTOMER_FAPIAO_FLAG=M, VIC_CUSTOMER_NATIONALITY=CHN, VIC_DATA_SOURCE=1, VIC_LINK_NAME=, VIC_LINK_PHONE=, VIC_LINK_ADDRESS=, VIC_CUSTOMER_ZIP_CODE=061800, VIC_CUSTOMER_UPLOAD_FLAG=0], RiskCodeWr=6810a, applySerial=null, com.sinosoft.lis.entity.LCContPojo=LCContPojo [ContID=707317, ShardingID=310083664, GrpContNo=00000000000000000000, ContNo=201837050020010135, ProposalContNo=310083664, PrtNo=310083664, ContType=2, FamilyType=0, FamilyID=null, PolType=0, CardFlag=0, ManageCom=86370500, ExecuteCom=86370500, AgentCom=0510020520, AgentCode=0000068812, AgentGroup=000000023963, AgentCode1=null, AgentType=null, SaleChnl=2, Handler=null, Password=null, AppntNo=0013473459, AppntName=更好, AppntSex=0, AppntBirthday=1987-09-24, AppntIDType=0, AppntIDNo=371401198709240036, InsuredNo=0013473459, InsuredName=更好, InsuredSex=0, InsuredBirthday=1987-09-24, InsuredIDType=0, InsuredIDNo=371401198709240036, PayIntv=0, PayMode=C, PayLocation=0, DisputedFlag=null, OutPayFlag=null, GetPolMode=null, SignCom=86370500, SignDate=null, SignTime=null, ConsignNo=null, BankCode=03, BankAccNo=63201366469950256561, AccName=更好, PrintCount=0, LostTimes=0, Lang=null, Currency=null, Remark=null, Peoples=1, Mult=0.0, Prem=98.0, Amnt=4500000.0, SumPrem=0.0, Dif=0.0, PaytoDate=null, FirstPayDate=2018-06-11, CValiDate=2018-06-12, InputOperator=null, InputDate=null, InputTime=null, ApproveFlag=0, ApproveCode=null, ApproveDate=null, ApproveTime=null, UWFlag=0, UWOperator=null, UWDate=null, UWTime=null, AppFlag=0, PolApplyDate=2018-06-11, GetPolDate=2018-06-11, GetPolTime=14:13:50, CustomGetPolDate=2018-06-11, State=0, Operator=ABC-CLOUD, MakeDate=2018-06-11, MakeTime=14:13:50, ModifyDate=2018-06-11, ModifyTime=14:13:50, FirstTrialOperator=null, FirstTrialDate=null, FirstTrialTime=null, ReceiveOperator=null, ReceiveDate=null, ReceiveTime=null, TempFeeNo=null, SellType=22, ForceUWFlag=0, ForceUWReason=null, NewBankCode=03, NewBankAccNo=63201366469950256561, NewAccName=更好, NewPayMode=4, AgentBankCode=05, BankAgent=, BankAgentName=, BankAgentTel=null, ProdSetCode=null, PolicyNo=null, BillPressNo=null, CardTypeCode=null, VisitDate=null, VisitTime=null, SaleCom=null, PrintFlag=null, InvoicePrtFlag=null, NewReinsureFlag=null, RenewPayFlag=null, AppntFirstName=null, AppntLastName=null, InsuredFirstName=null, InsuredLastName=null, AuthorFlag=null, GreenChnl=0, TBType=02, EAuto=0, SlipForm=2], com.sinosoft.lis.entity.LCCustomerImpartPojo=[LCCustomerImpartPojo [CustomerImpartID=707320, ContID=707317, ShardingID=310083664, GrpContNo=00000000000000000000, ContNo=201837050020010135, ProposalContNo=310083664, PrtNo=310083664, ImpartCode=001, ImpartVer=01, ImpartContent=您每年固定收入 万元 主要收入来源： （序号）被选项：①工薪②个体③私营④房屋出租⑤证券投资⑥银行利息⑦其他 家庭年收入：, ImpartParamModle=10000000;1;*;, CustomerNo=0013473459, CustomerNoType=0, UWClaimFlg=null, PrtFlag=null, Operator=ABC-CLOUD, MakeDate=2018-06-11, MakeTime=14:13:50, ModifyDate=2018-06-11, ModifyTime=14:13:50, PatchNo=0], LCCustomerImpartPojo [CustomerImpartID=707321, ContID=707317, ShardingID=310083664, GrpContNo=00000000000000000000, ContNo=201837050020010135, ProposalContNo=310083664, PrtNo=310083664, ImpartCode=001, ImpartVer=01, ImpartContent=您每年固定收入 万元 主要收入来源： （序号）被选项：①工薪②个体③私营④房屋出租⑤证券投资⑥银行利息⑦其他 家庭年收入：, ImpartParamModle=100000;1;*;, CustomerNo=0013473459, CustomerNoType=1, UWClaimFlg=null, PrtFlag=null, Operator=ABC-CLOUD, MakeDate=2018-06-11, MakeTime=14:13:50, ModifyDate=2018-06-11, ModifyTime=14:13:50, PatchNo=0], LCCustomerImpartPojo [CustomerImpartID=707322, ContID=707317, ShardingID=310083664, GrpContNo=00000000000000000000, ContNo=201837050020010135, ProposalContNo=310083664, PrtNo=310083664, ImpartCode=000, ImpartVer=02, ImpartContent=身高________cm（厘米） 体重________Kg （公斤）, ImpartParamModle=*;*, CustomerNo=0013473459, CustomerNoType=1, UWClaimFlg=null, PrtFlag=null, Operator=ABC-CLOUD, MakeDate=2018-06-11, MakeTime=14:13:50, ModifyDate=2018-06-11, ModifyTime=14:13:50, PatchNo=0]], com.sinosoft.lis.entity.LCCustomerImpartParamsPojo=[LCCustomerImpartParamsPojo [CustomerImpartParamsID=707323, CustomerImpartDetailID=0, ShardingID=310083664, GrpContNo=00000000000000000000, ContNo=201837050020010135, ProposalContNo=310083664, PrtNo=310083664, ImpartCode=001, ImpartVer=01, CustomerNo=0013473459, CustomerNoType=0, ImpartParamNo=1, ImpartParamName=AnnualIncome, ImpartParam=10000000, Operator=ABC-CLOUD, MakeDate=2018-06-11, MakeTime=14:13:50, ModifyDate=2018-06-11, ModifyTime=14:13:50, PatchNo=0], LCCustomerImpartParamsPojo [CustomerImpartParamsID=707324, CustomerImpartDetailID=0, ShardingID=310083664, GrpContNo=00000000000000000000, ContNo=201837050020010135, ProposalContNo=310083664, PrtNo=310083664, ImpartCode=001, ImpartVer=01, CustomerNo=0013473459, CustomerNoType=0, ImpartParamNo=2, ImpartParamName=Origin, ImpartParam=1, Operator=ABC-CLOUD, MakeDate=2018-06-11, MakeTime=14:13:50, ModifyDate=2018-06-11, ModifyTime=14:13:50, PatchNo=0], LCCustomerImpartParamsPojo [CustomerImpartParamsID=707325, CustomerImpartDetailID=0, ShardingID=310083664, GrpContNo=00000000000000000000, ContNo=201837050020010135, ProposalContNo=310083664, PrtNo=310083664, ImpartCode=001, ImpartVer=01, CustomerNo=0013473459, CustomerNoType=0, ImpartParamNo=3, ImpartParamName=FamilyIncome, ImpartParam=*, Operator=ABC-CLOUD, MakeDate=2018-06-11, MakeTime=14:13:50, ModifyDate=2018-06-11, ModifyTime=14:13:50, PatchNo=0], LCCustomerImpartParamsPojo [CustomerImpartParamsID=707326, CustomerImpartDetailID=0, ShardingID=310083664, GrpContNo=00000000000000000000, ContNo=201837050020010135, ProposalContNo=310083664, PrtNo=310083664, ImpartCode=001, ImpartVer=01, CustomerNo=0013473459, CustomerNoType=1, ImpartParamNo=1, ImpartParamName=AnnualIncome, ImpartParam=100000, Operator=ABC-CLOUD, MakeDate=2018-06-11, MakeTime=14:13:50, ModifyDate=2018-06-11, ModifyTime=14:13:50, PatchNo=0], LCCustomerImpartParamsPojo [CustomerImpartParamsID=707327, CustomerImpartDetailID=0, ShardingID=310083664, GrpContNo=00000000000000000000, ContNo=201837050020010135, ProposalContNo=310083664, PrtNo=310083664, ImpartCode=001, ImpartVer=01, CustomerNo=0013473459, CustomerNoType=1, ImpartParamNo=2, ImpartParamName=Origin, ImpartParam=1, Operator=ABC-CLOUD, MakeDate=2018-06-11, MakeTime=14:13:50, ModifyDate=2018-06-11, ModifyTime=14:13:50, PatchNo=0], LCCustomerImpartParamsPojo [CustomerImpartParamsID=707328, CustomerImpartDetailID=0, ShardingID=310083664, GrpContNo=00000000000000000000, ContNo=201837050020010135, ProposalContNo=310083664, PrtNo=310083664, ImpartCode=001, ImpartVer=01, CustomerNo=0013473459, CustomerNoType=1, ImpartParamNo=3, ImpartParamName=FamilyIncome, ImpartParam=*, Operator=ABC-CLOUD, MakeDate=2018-06-11, MakeTime=14:13:50, ModifyDate=2018-06-11, ModifyTime=14:13:50, PatchNo=0], LCCustomerImpartParamsPojo [CustomerImpartParamsID=707329, CustomerImpartDetailID=0, ShardingID=310083664, GrpContNo=00000000000000000000, ContNo=201837050020010135, ProposalContNo=310083664, PrtNo=310083664, ImpartCode=000, ImpartVer=02, CustomerNo=0013473459, CustomerNoType=1, ImpartParamNo=1, ImpartParamName=Height, ImpartParam=*, Operator=ABC-CLOUD, MakeDate=2018-06-11, MakeTime=14:13:50, ModifyDate=2018-06-11, ModifyTime=14:13:50, PatchNo=0], LCCustomerImpartParamsPojo [CustomerImpartParamsID=707330, CustomerImpartDetailID=0, ShardingID=310083664, GrpContNo=00000000000000000000, ContNo=201837050020010135, ProposalContNo=310083664, PrtNo=310083664, ImpartCode=000, ImpartVer=02, CustomerNo=0013473459, CustomerNoType=1, ImpartParamNo=2, ImpartParamName=weight, ImpartParam=*, Operator=ABC-CLOUD, MakeDate=2018-06-11, MakeTime=14:13:50, ModifyDate=2018-06-11, ModifyTime=14:13:50, PatchNo=0]], otherCompanyDieAmnt=, payPrem=0.0, com.sinosoft.lis.entity.LCBnfPojo=[]}, storageMap={}, exception=null, errorList=[], shardFlag=false, shardKey='null', action='null'}";
    }
}
