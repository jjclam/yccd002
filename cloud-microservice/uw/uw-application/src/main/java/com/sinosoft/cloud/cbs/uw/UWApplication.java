package com.sinosoft.cloud.cbs.uw;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.feign.EnableFeignClients;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;

/**
 * @project: abc-cloud-microservice
 * @author: yangming
 * @date: 2017/9/17 下午1:58
 * To change this template use File | Settings | File and Code Templates.
 */
@EnableDiscoveryClient
@SpringBootApplication
@ComponentScan({"com.sinosoft"})
@EnableFeignClients({"com.sinosoft.cloud"})
public class UWApplication {
    public static void main(String[] args) {
        SpringApplication.run(UWApplication.class, args);
    }

//    @Bean
//    public static PropertySourcesPlaceholderConfigurer placeholderConfigurer() {
//        PropertySourcesPlaceholderConfigurer c = new PropertySourcesPlaceholderConfigurer();
//        c.setIgnoreUnresolvablePlaceholders(true);
//        return c;
//    }

}
