package com.sinosoft.cloud.cbs.uw;

import com.sinosoft.cloud.rest.TradeInfo;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * @Author:wangshuliang
 * @Description:
 * @Date:Created in 10:29 2018/7/6
 * @Modified by:
 */
@FeignClient(value = "uw-app-service")
public interface UpdateSecContactService {
    @RequestMapping(method = RequestMethod.POST, path = "/rest/UpdateSecContactService/UpdateSecContactService")
    TradeInfo service(TradeInfo tradeInfo);
}
