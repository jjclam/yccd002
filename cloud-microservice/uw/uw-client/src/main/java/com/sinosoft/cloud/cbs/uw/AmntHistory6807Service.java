package com.sinosoft.cloud.cbs.uw;

import com.sinosoft.cloud.rest.TradeInfo;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * @Author:wangshuliang
 * @Description:
 * @Date:Created in 9:56 2018/7/2
 * @Modified by:
 */
@FeignClient(value = "uw-app-service")
public interface AmntHistory6807Service {
    @RequestMapping(method = RequestMethod.POST, path = "/rest/AmntHistory6807Service/AmntHistory6807Service")
    TradeInfo service(TradeInfo tradeInfo);
}
