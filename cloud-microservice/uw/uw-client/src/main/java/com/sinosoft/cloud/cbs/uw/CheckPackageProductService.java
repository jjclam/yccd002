package com.sinosoft.cloud.cbs.uw;

import com.sinosoft.cloud.rest.TradeInfo;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@FeignClient(value = "uw-app-service")
public interface CheckPackageProductService {
    @RequestMapping(method = RequestMethod.POST, path = "/rest/CheckCombinationProductService/CheckCombinationProductService")
    TradeInfo service(TradeInfo tradeInfo);
}
