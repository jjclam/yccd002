package com.sinosoft.cloud.cbs.uw;


import com.sinosoft.cloud.rest.TradeInfo;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * 数据编码转换客户端
 */

//@FeignClient(value = "microservice-application")
@FeignClient(value = "microservice-application")
public interface CodeMappingCacheInitService{

    @RequestMapping(method = RequestMethod.POST, path = "/rest/CodeMappingService/CodeMappingService")
    TradeInfo service(TradeInfo tradeInfo);
}
