package com.sinosoft.cloud.cbs.uw;

import com.sinosoft.cloud.rest.TradeInfo;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by sll on 2019/4/10.
 * 中保意健险客户端
 */
@FeignClient(value = "uw-app-service")
public interface UWRiskService {
    @RequestMapping(method = RequestMethod.POST,path="/rest/PrepareHealthRiskService/PrepareHealthRiskService")
    TradeInfo service(TradeInfo tradeInfo);
}
