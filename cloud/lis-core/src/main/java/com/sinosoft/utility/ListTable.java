/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */
package com.sinosoft.utility;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.Vector;

public class ListTable {
	private static Log logger = LogFactory.getLog(ListTable.class);
	private Vector myVector = new Vector();
	private int col; // 表的行
	private String name; // 表名
	//记录数限制数量
	private int mLimitNum = 9999999;
	//是否限制记录数标志
    private boolean mLimitFlag = false;
    //记录数是否超限
    private boolean mOutOfRecFlag = false;

	// @Constructor
	public ListTable() {
	    this.mLimitFlag=false;
	    this.mOutOfRecFlag=false;
	}
    /**
     * 判断记录数的实例化方法
     * @param pLimitType VTS:限制VTS展现的记录数 EXCEL:限制导出EXCEL的记录数
     */
    public ListTable(String pLimitType)
    {
        if(pLimitType==null)
        {
            this.mLimitFlag=false;
            this.mOutOfRecFlag=false;
        }
        if("VTS".equalsIgnoreCase(pLimitType))
        {
            String tValue = new ExeSQL().getOneValue("select sysvarvalue from ldsysvar where sysvar='LIMITNUMVTS'");
            if(tValue!=null&&!"".equals(tValue))
            {
                try
                {
                    this.mLimitNum = Integer.parseInt(tValue);
                    this.mLimitFlag = true;
                }
                catch(Exception ex)
                {
                    logger.debug("未获取到限制大小，不进行记录数限制");
                    this.mLimitNum = 9999999;
                }
            }
        }
        else if("EXCEL".equalsIgnoreCase(pLimitType))
        {
            String tValue = new ExeSQL().getOneValue("select sysvarvalue from ldsysvar where sysvar='LIMITNUMEXCEL'");
            if(tValue!=null&&!"".equals(tValue))
            {
                try
                {
                    this.mLimitNum = Integer.parseInt(tValue);
                    this.mLimitFlag = true;
                }
                catch(Exception ex)
                {
                    logger.debug("未获取到限制大小，不进行记录数限制");
                    this.mLimitNum = 9999999;
                }
            }
        }
        else
        {
            this.mLimitFlag=false;
            this.mOutOfRecFlag=false;
        }
    }

	// @Method
	// 新建一个ListTable，参数为一个字符串数组，包含所需的值
	public void add(String[] temparray) {
	    if(mLimitFlag)
	    {
	        if(mOutOfRecFlag)
	        {
	            return;
	        }
	        if (myVector.size()>=mLimitNum)
	        {
	            mOutOfRecFlag=true;
	            return;
	        }
	        myVector.addElement(temparray);
	    }
	    else
	    {
	        myVector.addElement(temparray);
	    }
	}

	// 得到指定行列的值
	public String getValue(int column, int row) {
		String[] temparray = new String[column];
		temparray = (String[]) myVector.get(row);
		return temparray[column];
	}

	// 取得第i行的列表的所有值
	public String[] get(int i) {
		return (String[]) myVector.get(i);
	}

	// 得到整个列表的大小
	public int size() {
		return myVector.size();
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getName() {
		return this.name;
	}

	public int getCol() {
		return this.col;
	}
	public boolean getOutOfRec()
	{
	    return this.mOutOfRecFlag;
	}
}

