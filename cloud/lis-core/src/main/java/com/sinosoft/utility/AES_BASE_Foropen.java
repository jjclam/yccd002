package com.sinosoft.utility;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang.StringUtils;
import sun.security.provider.Sun;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import java.security.SecureRandom;
import java.security.Security;

public class AES_BASE_Foropen {
    public static String doAESForopen(String data, String key, int mode ){
        try {
            if (StringUtils.isBlank(data) || StringUtils.isBlank(key)) {
                return null;
            }
            boolean encrypt = mode == Cipher.ENCRYPT_MODE;
            byte[] content;
            if (encrypt) {
                content = data.getBytes("UTF-8");
            } else {
                content = Base64.decodeBase64(data.getBytes("UTF-8"));
            }
            SecretKey secretKey = getsecretKey(key);
            SecretKeySpec keySpec = new SecretKeySpec(secretKey.getEncoded(), "AES");
            Cipher cipher = Cipher.getInstance("AES");
            cipher.init(mode,keySpec);
            byte[] result = cipher.doFinal(content);

            if (encrypt) {
                return new String(Base64.encodeBase64(result));
            } else {
                return new String(result, "UTF-8");
            }
        }catch (Exception e){
            System.err.println("AEs密文处理异常"+e);
            e.printStackTrace();
        }
        return null;
    }

    public static SecretKey getsecretKey (String key ){
        try{
            KeyGenerator gen = KeyGenerator.getInstance("AES");
            if(null== Security.getProvider("SUN")) {
                Security.addProvider(new Sun());
            }
            SecureRandom secureRandom =SecureRandom. getInstance("SHAIPRNG","SUN");
            secureRandom.setSeed(key.getBytes());
            gen.init(128, secureRandom);

            return gen.generateKey();
        }catch(Exception e){

            throw new RuntimeException("初始化秘钥出现异常");
        }
    }
}
