/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */
package com.sinosoft.utility;

import com.sinosoft.lis.pubfun.FDate;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.lang.reflect.AccessibleObject;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class Reflections {
	private static Log logger = LogFactory.getLog(Reflections.class);

	Vector mVResult = new Vector();
	/**
	 * 构造函数显示
	 * 
	 * @param c1
	 *            Class
	 */
	public static void printConstructors(Class c1) {
		Constructor[] constructors = c1.getDeclaredConstructors();
		// logger.debug("------------------print
		// Constructors-----------------");

		for (int i = 0; i < constructors.length; i++) {
			Constructor c = constructors[i];
			String name = c.getName();
			// logger.debug(" " + name+ "(");

			Class[] paramTypes = c.getParameterTypes();
			// for (int j = 0 ; j<paramTypes.length; j++)
			// {
			// if (j > 0)
			// logger.debug("Par, ");
			// }
			// logger.debug(");");
		}
	}

	/**
	 * 方法显示
	 * 
	 * @param c1
	 *            Class
	 */
	public static void printMethods(Class c1) {
		Method[] methods = c1.getDeclaredMethods();
		// logger.debug("------------------print methods
		// ----------------");

		for (int i = 0; i < methods.length; i++) {
			Method m = methods[i];
			// Class retType = m.getReturnType();
			// String name = m.getName();

			// logger.debug(Modifier.toString(m.getModifiers()));
			// logger.debug(" | " + retType.getName() + " |" + name +
			// "(");

			// Class[] paramTypes = m.getParameterTypes();
			// for ( int j = 0 ; j < paramTypes.length; j++)
			// {
			// if(j > 0)
			// logger.debug(", ");
			// logger.debug(paramTypes[j].getName());
			// }
			// logger.debug("):");
		}
	}

	/**
	 * 变量参数显示
	 * 
	 * @param c1
	 *            Class
	 */
	public static void printFields(Class c1) {
		Field[] fields = c1.getDeclaredFields();
		AccessibleObject.setAccessible(fields, true);

		// logger.debug("------------------print Fields
		// ----------------");
		for (int i = 0; i < fields.length; i++) {
			Field f = fields[i];
			// Class type = f.getType();
			// String name = f.getName();
			// logger.debug(Modifier.toString(f.getModifiers()));
			// logger.debug(" | " + type.getName() + "| " + name + ";");
		}
	}

	/**
	 * SchemaSet的转换
	 * 
	 * @param a
	 *            SchemaSet
	 * @param b
	 *            SchemaSet
	 * @return boolean
	 */
	public boolean transFields(SchemaSet a, SchemaSet b) {
		if (a != null) {
			int n = b.size();
			try {
				Class c1 = a.getObj(1).getClass();
				a.clear();
				// logger.debug("====in"+n);
				for (int i = 1; i <= n; i++) {
					// logger.debug("---i:"+i);
					Object c = c1.newInstance();
					transFields((Schema) c, (Schema) b.getObj(i));
					a.add((Schema) c);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else {
			return false;
		}

		return true;
	}


	/**
	 * 把
	 * 对象b的值赋给对象a
	 *
	 * @param a Schema
	 * @param b Schema
	 * @return Object
	 */
	public Schema transFields(Schema a, Schema b) {
		if (a == null || b == null) {
			return null;
		}
		for (int i = 0; i < b.getFieldCount(); i++) {
			if (b.getV(i) != null && !b.getV(i).equals("null")){
				a.setV(b.getFieldName(i), b.getV(i));
			}
		}
		return a;
	}

    /**
     * 把
     * 对象b的值赋给对象a
     *
     * @param a Schema
     * @param b Schema
     * @return Object
     */
    public Pojo transFields(Pojo a, Pojo b) {
        if (a == null || b == null) {
            return null;
        }
        for (int i = 0; i < b.getFieldCount(); i++) {
            if (b.getV(i) != null && !b.getV(i).equals("null") && !b.getV(i).equals("")) {
                a.setV(b.getFieldName(i), b.getV(i));
            }
        }
        return a;
    }

	private String changeDateFormat(String dateStr) {
		SimpleDateFormat cstFormater = new SimpleDateFormat("EEE MMM dd HH:mm:ss zzz yyyy", Locale.US);
		FDate fDate = new FDate();
		Date date = null;
		try {
			date = cstFormater.parse(dateStr);
		} catch (ParseException e) {
		}
		if(date==null){
			try {
				cstFormater = new SimpleDateFormat("yyyy-MM-dd");
				date = cstFormater.parse(dateStr);
			} catch (ParseException e) {
			}
		}

		if(date==null){
			try {
				cstFormater = new SimpleDateFormat("yyyyMMdd");
				date = cstFormater.parse(dateStr);
			} catch (ParseException e) {
				logger.error("转成日期格式错误", e);
				return null;
			}
		}

		return fDate.getString(date);
	}

	/**
	 * 把
	 * 对象b的值赋给对象a
	 * @param a
	 *            Schema
	 * @param b
	 *            Schema
	 * @return Object
	 */
	public Object transFields(Object a, Object b) {

		if (b instanceof Pojo && a instanceof Schema) {
			Pojo pojo = (Pojo) b;
			Schema schema = (Schema) a;

			for (int i = 0; i < pojo.getFieldCount(); i++) {
				/**
				 * 由于Pojo的日期类型是Date，不是String，与Schema中getV SetV方法不兼容，因此将其转化成yyyy-mm-dd的字符串传入Schema中
				 */
				if (pojo.getFieldType(i) == Schema.TYPE_DATE && pojo.getV(i) != null && !pojo.getV(i).equals("null")) {
					String dateStr = pojo.getV(i);
					schema.setV(pojo.getFieldName(i), changeDateFormat(dateStr));
				} else {
					if (pojo.getV(i) != null && !pojo.getV(i).equals("null") && !pojo.getV(i).equals("")) {
						schema.setV(pojo.getFieldName(i), pojo.getV(i));
					}
					//schema.setV(pojo.getFieldName(i), pojo.getV(i));
				}
			}
			return schema;
		} else if (b instanceof Schema && a instanceof Pojo) {
			Schema schema = (Schema) b;
			Pojo pojo = (Pojo) a;
			for (int i = 0; i < schema.getFieldCount(); i++) {
				if (schema.getV(i) != null && !schema.getV(i).equals("null") && !schema.getV(i).equals("")) {
					pojo.setV(schema.getFieldName(i), schema.getV(i));
				}
			}

			return pojo;
		} else if (a instanceof Pojo && b instanceof Pojo) {
			transFields((Pojo) a, (Pojo) b);
		}
		Class c1 = a.getClass();
		Class c2 = b.getClass();
		//logger.info("此处是致命错误！一定要改Pojo c1=" + c1.getSimpleName() + " c2=" + c2.getSimpleName());
		//添加父类字段转换 fixed by yowakic 2017-1-18
		Field[] fieldsDest = (Field[]) ArrayUtils.addAll(c1.getSuperclass().getDeclaredFields(),c1.getDeclaredFields());
		Field[] fieldsOrg = (Field[])ArrayUtils.addAll(c2.getSuperclass().getDeclaredFields(),c2.getDeclaredFields());

		AccessibleObject.setAccessible(fieldsDest, true);
		AccessibleObject.setAccessible(fieldsOrg, true);

		// logger.debug("----fieldDest.length:"+fieldsDest.length);
		for (int i = 0; i < fieldsDest.length; i++) {
			Field f = fieldsDest[i];
			Class type = f.getType();
			String name = f.getName();
			String typeName = type.getName();
			// logger.debug("[Time]::"+i+"[colname]:"+name+"[Typename]:"+typeName);
			if (name.equals("FIELDNUM") || name.equals("PK")
					|| name.equals("mErrors") || name.equals("fDate")) {
				continue;
			}
			for (int j = 0; j < fieldsOrg.length; j++) {
				// 得到数据源的数据

				Field f1 = fieldsOrg[j];
				Class type1 = f1.getType();
				String typeName2 = type1.getName();
				String name1 = f1.getName();
				String typeName1 = type.getName();
				// logger.debug("[times]:"+j+"[colname1]:"+name1+"[Typename1]:"+typeName1);
				// 取出冗余变量

				if (name.equals("FIELDNUM") || name.equals("PK")
						|| name.equals("mErrors") || name.equals("fDate")) {
					continue;
				}
				// 赋值转换
				if ((typeName.equalsIgnoreCase(typeName1)) && (name1.equalsIgnoreCase(name))) {

					switch (transType(typeName)) {
					case 3:
						try {
							f.setDouble(a, f1.getDouble(b));
						} catch (Exception e) {
							e.printStackTrace();
						}
						break;
					case 5:
						try {
							f.setInt(a, f1.getInt(b));
						} catch (Exception e) {
							e.printStackTrace();
						}
						break;
					case 93:
						try {
							if("java.util.Date".equals(typeName2)||"java.sql.Date".equals(typeName2)){
								FDate fDate = new FDate();
								if(!"".equals(f1.get(b))&&f1.get(b)!=null){
									String value= fDate.getString((Date) f1.get(b));
									f.set(a, value);
								}
							}else{
								f.set(a, f1.get(b));
							}
						} catch (Exception e) {
							e.printStackTrace();
						}
						break;
					default:
						try {
							if("java.util.Date".equals(typeName)||"java.sql.Date".equals(typeName)){
								if("java.lang.String".equals(typeName2)){

									Date date = new FDate().getDate((String)f1.get(b));
									f.set(a, date);
								}else{
									f.set(a, f1.get(b));
								}
							}else{
								f.set(a, f1.get(b));
							}
							;
							// logger.debug("------Default:"+f1.get(b));
						} catch (Exception e) {
							e.printStackTrace();
						}
						break;
					}
				}
			}
		}
		return a;
	}

	public  <T>List transFields(Class<T> pClass, SchemaSet pSchemaSet){
		List<T> resultList = new ArrayList<>() ;
		if(pClass != null && pSchemaSet != null && pSchemaSet.size()>0){
			try {
				for (int i=1; i<=pSchemaSet.size(); i++){
					Object obj =  pClass.newInstance();
					this.transFields(obj,pSchemaSet.getObj(i));
					resultList.add((T)obj);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return resultList;
	}

	/**
	 * 比较两个对象是否内容相同
	 * 
	 * @param a
	 *            Schema
	 * @param b
	 *            Schema
	 * @return boolean
	 */
	public boolean compareFields(Schema a, Schema b) {
		boolean aFlag = true;

		mVResult.clear();

		Class c1 = a.getClass();
		Class c2 = b.getClass();

		Field[] fieldsDest = c1.getDeclaredFields();
		Field[] fieldsOrg = c2.getDeclaredFields();

		AccessibleObject.setAccessible(fieldsDest, true);
		AccessibleObject.setAccessible(fieldsOrg, true);

		// logger.debug("------------------comp print Fields
		// ----------------");
		for (int i = 0; i < fieldsDest.length; i++) {
			Field f = fieldsDest[i];
			Class type = f.getType();
			String name = f.getName();
			String typeName = type.getName();

			if (name.equals("FIELDNUM") || name.equals("PK")
					|| name.equals("mErrors") || name.equals("fDate")) {
				continue;
			}

			for (int j = 0; j < fieldsOrg.length; j++) {
				// 得到数据源的数据
				Field f1 = fieldsOrg[j];
				// Class type1 = f1.getType();
				String name1 = f1.getName();
				String typeName1 = type.getName();
				// 取出冗余变量

				if (name.equals("FIELDNUM") || name.equals("PK")
						|| name.equals("mErrors") || name.equals("fDate")) {
					continue;
				}
				// 赋值转换
				if ((typeName.equals(typeName1)) && (name1.equals(name))) {

					switch (transType(typeName)) {
					case 3:
						try {
							if (f.getDouble(a) != f1.getDouble(b)) {
								String tStr = name + "^" + f.getDouble(a) + "|"
										+ f1.getDouble(b);
								mVResult.addElement(tStr);
								aFlag = false;
							}
						} catch (Exception e) {
							e.printStackTrace();
						}
						break;
					case 5:
						try {
							if (f.getInt(a) != f1.getInt(b)) {
								String tStr = name + "^" + f.getInt(a) + "|"
										+ f1.getInt(b);
								mVResult.addElement(tStr);
								aFlag = false;
							}
						} catch (Exception e) {
							e.printStackTrace();
						}
						break;
					case 93:
						try {
							if (f.get(a) != f1.get(b)) {
								String tStr = name + "^" + f.get(a) + "|"
										+ f1.get(b);
								mVResult.addElement(tStr);
								aFlag = false;
							}
						} catch (Exception e) {
							e.printStackTrace();
						}
						break;
					default:
						try {
							if (f.get(a) != f1.get(b)) {
								String tStr = name + "^" + f.get(a) + "|"
										+ f1.get(b);
								mVResult.addElement(tStr);
								aFlag = false;
							}
						} catch (Exception e) {
							e.printStackTrace();
						}
						break;
					}
				}
			}

		}
		return aFlag;
	}

	/**
	 * 同步两个对象
	 * 
	 * @param a
	 *            Schema
	 * @param b
	 *            Schema
	 * @return Object
	 */
	public Object synchronizeFields(Schema a, Schema b) {
		// boolean aFlag = true;

		mVResult.clear();

		Class c1 = a.getClass();
		Class c2 = b.getClass();

		Field[] fieldsDest = c1.getDeclaredFields();
		Field[] fieldsOrg = c2.getDeclaredFields();

		AccessibleObject.setAccessible(fieldsDest, true);
		AccessibleObject.setAccessible(fieldsOrg, true);

		// logger.debug("------------------comp print Fields
		// ----------------");
		for (int i = 0; i < fieldsDest.length; i++) {
			Field f = fieldsDest[i];
			Class type = f.getType();
			String name = f.getName();
			String typeName = type.getName();

			if (name.equals("FIELDNUM") || name.equals("PK")
					|| name.equals("mErrors") || name.equals("fDate")) {
				continue;
			}

			for (int j = 0; j < fieldsOrg.length; j++) {
				// 得到数据源的数据
				Field f1 = fieldsOrg[j];
				// Class type1 = f1.getType();
				String name1 = f1.getName();
				String typeName1 = type.getName();
				// 取出冗余变量

				if (name.equals("FIELDNUM") || name.equals("PK")
						|| name.equals("mErrors") || name.equals("fDate")) {
					continue;
				}
				// 赋值转换
				if ((typeName.equals(typeName1)) && (name1.equals(name))) {

					switch (transType(typeName)) {
					case 3:
						try {
							if (f.getDouble(a) != f1.getDouble(b)) {
								f.setDouble(a, f1.getDouble(b));
							}
						} catch (Exception e) {
							e.printStackTrace();
						}
						break;
					case 5:
						try {
							if (f.getInt(a) != f1.getInt(b)) {
								f.setInt(a, f1.getInt(b));
							}
						} catch (Exception e) {
							e.printStackTrace();
						}
						break;
					case 93:
						try {
							if (f.get(a) != f1.get(b)) {
								f.set(a, f1.get(b));
							}
						} catch (Exception e) {
							e.printStackTrace();
						}
						break;
					default:
						try {
							if (f.get(a) != f1.get(b)) {
								f.set(a, f1.get(b));
							}
						} catch (Exception e) {
							e.printStackTrace();
						}
						break;
					}
				}
			}
		}
		// logger.debug("---------------------------- compare
		// end-----------------------");
		return a;
	}

	/**
	 * 显示对象内容
	 * 
	 * @param a
	 *            Object
	 */
	public void printFields(Object a) {
		Class c1 = a.getClass();

		Field[] fieldsDest = c1.getDeclaredFields();

		AccessibleObject.setAccessible(fieldsDest, true);

		// logger.debug("------------------trans print Fields
		// ----------------");
		// logger.debug("----fieldDest.length:"+fieldsDest.length);
		for (int i = 0; i < fieldsDest.length; i++) {
			Field f = fieldsDest[i];
			Class type = f.getType();
			String name = f.getName();
			String typeName = type.getName();
			// logger.debug("[Time]::"+i+"[colname]:"+name+"[Typename]:"+typeName);
			if (name.equals("FIELDNUM") || name.equals("PK")
					|| name.equals("mErrors") || name.equals("fDate")) {
				continue;
			}
			// logger.debug("----fieldOrg.length:"+fieldsOrg.length);

			switch (transType(typeName)) {
			case 3:

				// try
				// {
				// logger.debug("***************double::"+name+"---"+f.getDouble(a));
				// }
				// catch(Exception e)
				// {
				// e.printStackTrace();
				// }
				break;
			case 5:

				// try
				// {
				// logger.debug("*************************Int::"+name+"---"+f.getInt(a));
				// }
				// catch(Exception e)
				// {
				// e.printStackTrace();
				// }
				break;
			case 93:

				// try
				// {
				// logger.debug("*******************String::"+name+"---"+
				// f.get(a));
				// }
				// catch(Exception e)
				// {
				// e.printStackTrace();
				// }
				break;
			default:

				// try
				// {
				// logger.debug("------Default:"+f.get(a));
				// }
				// catch(Exception e)
				// {
				// e.printStackTrace();
				break;
			// }
			}

		}

		// logger.debug("-------- print end-------");

	}

	/**
	 * 类型转换
	 * 
	 * @param type
	 *            Object
	 * @return int
	 */
	private static int transType(Object type) {
		int typecode;
		typecode = 93;
		if (type.equals("java.lang.String")) {
			typecode = 93;
		}else if (type.equals("double")) {
			typecode = 3;
		}else if (type.equals("int")) {
			typecode = 5;
		}else {
			typecode = 999;
		}

		return typecode;
	}

	public boolean equals(Object otherobject) {
		if (this == otherobject) {
			return true;
		}

		if (otherobject == null) {
			return false;
		}

		if (getClass() != otherobject.getClass()) {
			return false;
		}

		Reflections other = (Reflections) otherobject;

		return true;
	}

	public String toString() {
		Class ref = this.getClass();
		// logger.debug("ref:"+ref);
		// logger.debug("--------------------------");
		return getClass().getName();
	}

	public Vector getVResult() {
		return mVResult;
	}

	public static void main(String[] args) {

	}
}
