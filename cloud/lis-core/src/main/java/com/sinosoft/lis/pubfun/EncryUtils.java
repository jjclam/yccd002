package com.sinosoft.lis.pubfun;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Security;
import java.security.Signature;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.HashMap;
import java.util.Map;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESedeKeySpec;


import org.apache.commons.codec.binary.Base64;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
//import org.bouncycastle.jce.provider.BouncyCastleProvider;

/**
 * 标准RSA256withRSA 加解密demo
 * @author zuchong
 * @version $Id: Demo.java, v 0.1 2017-12-12 15:11 zuchong Exp $$
 */
public class EncryUtils {
    private static Logger logger = LoggerFactory.getLogger(EncryUtils.class);

    /** the algorithm for signature */
    public static String SIGNATURE_ALGO = "SHA256withRSA";

    /** the algorithm for signature key */
    public static String SIGNATURE_KEY_ALGO = "RSA";

    /** DESede algo name */
    private static String SYMM_KEY_ALGO_NAME = "DESede";

    /** the algo for symmetric encryption and decryption */
    public static String SYMMETRIC_ENCRYPTION_ALGO = "DESede/ECB/PKCS5Padding";

    /** publicKey name */
    private static String PUBLICK_KEY   = "PUBLIC_KEY";

    /** privateKey name */
    private static String PRIVATE_KEY   = "PRIVATE_KEY";

    /**  add BC provider */
    static {
        if (Security.getProvider("BC") == null) {
//            Security.addProvider(new BouncyCastleProvider());
        }
    }

    /**
     * do sign operation to message
     * @param data  待签名数据
     * @param key   秘钥
     * @param algorithm 算法名称
     * @return  签名结果
     */
    public static byte[] sign(byte[] data, byte[] key, String algorithm) throws Exception{

        try {

            PrivateKey privateKey =  recoverPrivateKey(key);
            Signature signature = Signature.getInstance(algorithm);
            signature.initSign(privateKey);
            signature.update(data);
            return signature.sign();

        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }


    /**
     * veriry method
     * @param unSignedData 待验签数据
     * @param signedData 验签数据
     * @param key   秘钥
     * @param algorithm 算法
     * @return  验签结果
     */
    public static boolean verify(byte[] unSignedData, byte[] signedData, byte[] key, String algorithm) {
        try {

            PublicKey publicKey = recoverPublicKey(key);
            Signature signature = Signature.getInstance(algorithm);
            signature.initVerify(publicKey);

            signature.update(unSignedData);
            return signature.verify(signedData);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }


    /**
     * 通用加密处理
     * @param data  待加密数
     * @param key   秘钥
     * @param algorithm 算法, 形如 DES/ECB/NoPadding 等
     * @return  加密后数据
     */
    public static byte[] encrypt(byte[] data, byte[] key, String algorithm) {

        try {

            Cipher cipher = Cipher.getInstance(algorithm);
            cipher.init(Cipher.ENCRYPT_MODE, recovertKey(key));
            return cipher.doFinal(data);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return  null;
    }

    /**
     * 通用解密处理
     * @param data 待解密数据 byte数组形式
     * @param key 秘钥 byte 数组格式
     * @param algorithm 算法
     * @return
     */
    public static byte[] decrypt(byte[] data, byte[] key, String algorithm) {

        try {
            Cipher cipher = Cipher.getInstance(algorithm);
            cipher.init(Cipher.DECRYPT_MODE, recovertKey(key));
            return cipher.doFinal(data);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }



    /***
     * get the message to be sended, return fixed message
     * @return
     */
    private static String getSendMessage(){
        return "MerchantID=RPM0000001&TxnModel=01&TxnType=01&RefNo=BILLNO1001";
    }

    /**
     * get message
     * @param encryptedMessage
     * @return
     */
    public static String getMessage(String encryptedMessage){
        return encryptedMessage.trim().split("&Signature=")[0];
    }

    /**
     * get message signature
     * @param encryptedMessage
     * @return
     */
    public static String getSignature(String encryptedMessage){
        return encryptedMessage.trim().split("&Signature=")[1];
    }


    /**
     * generate publicKey and priateKey
     * @param keySize the size of key : 1024, 2048
     * @return
     */
    public static Map<String, Object> generateKeyPair(int keySize) {

        Map<String, Object> rtn = new HashMap<String, Object>();

        try {
            KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance(SIGNATURE_KEY_ALGO);
            keyPairGenerator.initialize(keySize);
            KeyPair keyPair = keyPairGenerator.generateKeyPair();
            byte[] publicKey = keyPair.getPublic().getEncoded();
            byte[] privateKey = keyPair.getPrivate().getEncoded();
            rtn.put(PUBLICK_KEY, publicKey);
            rtn.put(PRIVATE_KEY, privateKey);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return rtn;
    }

    /**
     * recovery privateKey
     * @param data
     * @return
     */
    public static PrivateKey recoverPrivateKey(byte[] data) {

        try {

            PKCS8EncodedKeySpec pkcs8EncodedKeySpec = new PKCS8EncodedKeySpec(data);
            KeyFactory keyFactory = KeyFactory.getInstance(SIGNATURE_KEY_ALGO);
            return keyFactory.generatePrivate(pkcs8EncodedKeySpec);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * recovery publicKey
     * @param data
     * @return
     */
    public static PublicKey recoverPublicKey(byte[] data) {

        try {

            X509EncodedKeySpec keySpec = new X509EncodedKeySpec(data);
            KeyFactory keyFactory = KeyFactory.getInstance(SIGNATURE_KEY_ALGO);
            return keyFactory.generatePublic(keySpec);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }



    /**
     *
     * 生成DESede算法秘钥的二进制数组
     * @param keyLen 秘钥长度，支持：56、64两种长度
     * @return 秘钥数组
     */
    public static byte[] generateKey(int keyLen) {
        try {

            KeyGenerator keyGenerator = KeyGenerator.getInstance(SYMM_KEY_ALGO_NAME);

            keyGenerator.init(keyLen);

            SecretKey secretKey = keyGenerator.generateKey();

            return secretKey.getEncoded();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 基于秘钥 byte数组生成秘钥
     * @param key
     * @return
     */
    public static SecretKey recovertKey(byte[] key) {

        try {

            DESedeKeySpec deSedeKeySpec = new DESedeKeySpec(key);

            SecretKeyFactory keyFactory = SecretKeyFactory.getInstance(SYMM_KEY_ALGO_NAME);

            return keyFactory.generateSecret(deSedeKeySpec);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }




    public static String base64Encode(byte[] srcBytes){
        return new String(org.apache.commons.codec.binary.Base64.encodeBase64(srcBytes));
    }

    public static byte[] base64Deocde(String srcStr){
        return org.apache.commons.codec.binary.Base64.decodeBase64(srcStr.getBytes());
    }

    public static String urlBase64encode(byte[] data){
        return new String(Base64.encodeBase64URLSafe(data));
    }





    /**
     * demo for sign and verify
     */
    public static void demo(String message)throws Exception{


        /** keyPair for RSA signature*/
        Map<String, Object> generatedKeys = generateKeyPair(2048);

        /** key for DESede algo */
        byte[] symmKey = generateKey(112);

        System.out.println("--------------------------------sign and encrypt----------------------------------------------");

//        、String message = getSendMessage();

        System.out.println("send message:" + message);

        String signature = base64Encode(sign(message.getBytes(), (byte[]) generatedKeys.get(PRIVATE_KEY), SIGNATURE_ALGO));

        System.out.println("message signature:" + signature);

        String signedMessage = message + "&Signature=" + signature;

        System.out.println("signed message:" + signedMessage);

        byte[] encryptedBytes = encrypt(signedMessage.getBytes(), symmKey, SYMMETRIC_ENCRYPTION_ALGO);

        String encyptedMessage = urlBase64encode(encryptedBytes);

        System.out.println("encrypted message:" + encyptedMessage);


        System.out.println("--------------------------------decrypt and verify----------------------------------------------");

        byte[] deBase64Message = base64Deocde(encyptedMessage);

        System.out.println("decrypt message length:" + deBase64Message.length);

        String decryptedMessage = new String(decrypt(deBase64Message, symmKey, SYMMETRIC_ENCRYPTION_ALGO));

        System.out.println(decryptedMessage);
        System.out.println(encyptedMessage);
        System.out.println("same to encrypted message:" + decryptedMessage.equals(encyptedMessage));

        String inSignature = getSignature(decryptedMessage);

        System.out.println("messageSignature:" + inSignature);
        System.out.println(signature);
        System.out.println("Signature equal :" + signature.equals(inSignature));

        String inMessage = getMessage(decryptedMessage);

        System.out.println("in message:" + inMessage);
        System.out.println(message);
        System.out.println("message equal: " + inMessage.equals(message));

        boolean verifyResult = verify(message.getBytes(), base64Deocde(signature), (byte[])generatedKeys.get(PUBLICK_KEY), SIGNATURE_ALGO);
        System.out.println("messageSignature:" + inSignature);
        System.out.println(signature);
        System.out.println("verify result:" + verifyResult);

        System.out.println("-------------------------------- end ----------------------------------------------");

    }


    /**
     * 获取验签的签
     */
    public static String   caden(String message)throws Exception{
        Map<String, Object> generatedKeys = generateKeyPair(2048);

        System.out.println("--------------------------------sign and encrypt----------------------------------------------");

        System.out.println("send message:" + message);

        String   signature = base64Encode(sign(message.getBytes(), (byte[]) generatedKeys.get(PRIVATE_KEY), SIGNATURE_ALGO));

        System.out.println("message signature:" + signature);


        return   signature;
    }






    /**
     * 验签
     */
    public static boolean CheckSign(String msg)throws Exception{
        JSONObject jsondata = JSON.parseObject(msg);
        JSONObject document=jsondata.getJSONObject("request");
        String  header=document.getJSONObject("header").toString();
        String  body=document.getJSONObject("body").toString();
        String signature = document.getString("signature");
        // String  signature=document.getJSONObject("signature").toString();
        String  request=header+body;
        Map<String, Object> generatedKeys = generateKeyPair(2048);
        //String requet=getRequestBodyStr(msg);
        //测试需要放开
        //String signature= getSignatureXml(msg);
        //本地专用
        //String signature = base64Encode(sign(requet.getBytes(), (byte[]) generatedKeys.get(PRIVATE_KEY), SIGNATURE_ALGO));
        logger.info("验签"+signature);
        System.out.println("message signature:" + signature);

        boolean verifyResult = verify(request.getBytes(), base64Deocde(signature), (byte[])generatedKeys.get(PUBLICK_KEY), SIGNATURE_ALGO);

        System.out.println("verify result:" + verifyResult);
        return verifyResult;

    }
    /**
     * 获得签名
     * @param requestXml
     * @return
     */
    public static String getSignatureXml(String requestXml) {
        String result = "";
        if ( requestXml != null && ! "".equals(requestXml)){
            int start = requestXml.indexOf("<signature>") + "<signature>".length();
            int end = requestXml.lastIndexOf("</signature>");
            result = requestXml.substring(start,end);
        }
        return result ;
    }
    /**
     * 获得request里的字符串
     * @param requestXml
     * @return
     */
    public static String getRequestBodyStr(String requestXml) {
        String result = "";
        if ( requestXml != null && ! "".equals(requestXml)){
            int start = requestXml.indexOf("<request>") + "<request>".length();
            int end = requestXml.lastIndexOf("</request>");
            result = requestXml.substring(start,end);
        }
        return result ;
    }
    /**
     * 加签
     */
    public static String addSignered(String message) throws Exception{

        /** keyPair for RSA signature*/
        Map<String, Object> generatedKeys = generateKeyPair(2048);

        /** key for DESede algo */
        byte[] symmKey = generateKey(112);

        String signature = base64Encode(sign(message.getBytes(), (byte[]) generatedKeys.get(PRIVATE_KEY), SIGNATURE_ALGO));

       /* System.out.println("message signature:" + signature);

        String signedMessage = message + "&Signature=" + signature;

        System.out.println("signed message:" + signedMessage);

        byte[] encryptedBytes = encrypt(signedMessage.getBytes(), symmKey, SYMMETRIC_ENCRYPTION_ALGO);

        String encyptedMessage = urlBase64encode(encryptedBytes);

        System.out.println("encrypted message:" + encyptedMessage);*/

        return signature;


    }

    /**
     *
     * @param args
     */

    public static void main(String[] args) {
        try {
            demo("");
        }catch (Exception e){
            e.printStackTrace();
        }
    }

}
