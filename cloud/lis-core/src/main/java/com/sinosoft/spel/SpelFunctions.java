package com.sinosoft.spel;

import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.utility.StrTool;

/**
 * Created by mali on 2016/8/30.
 */
public class SpelFunctions {
    /**
     * 获取多个数中的最大值
     *
     * @param param
     * @return 返回数值
     * @throws Exception
     */

    /**
     *
     * @param param 多个参数
     * @return 返回数值
     * @throws Exception 异常信息
     */
    public static double max(String... param) throws Exception {
        double result = 0;
        result = Double.parseDouble(param[0]);
        if (param.length >= 1) {
            for (int i = 0; i < param.length; i++) {
                result = Math.max(result, Double.parseDouble(param[i]));
            }
        }
        return result;
    }

    /**
     * 获取多个数中的最小值
     *
     * @param param 多个参数
     * @return 数值
     * @throws Exception 异常信息
     */
    public static double min(String... param) throws Exception {
        double result = 0;
        result = Double.parseDouble(param[0]);
        if (param.length >= 1) {
            for (int i = 0; i < param.length; i++) {
                result = Math.min(result, Double.parseDouble(param[i]));
            }
        }
        return result;
    }

    /**
     * 判断一个字符串是否在一个字符串集合中，类似sql中的in算法
     * 集合字符串中，字符串间需要用英文逗号分隔
     *
     * @param str 字符串
     * @param list 字符串
     * @return boolean
     */
    public static boolean in(String str, String list) {
        if (str == null && list == null) {
            return true;
        } else if (list == null) {
            return false;
        } else if (str == null) {
            return false;
        }
        list = list.trim();
        String[] strArr = list.split(",");
        for (int i = 0; i < strArr.length; i++) {
            if (str.equals(strArr[i].trim())) {
                return true;
            }
        }
        return false;
    }

    /**
     * 判断一个字符串是否在一个字符串集合中，类似sql重的not in算法
     * 集合字符串中，字符串间需要用英文逗号分隔
     *
     * @param str 字符串
     * @param list 字符串集合
     * @return boolean
     */
    public static boolean notIn(String str, String list) {
        return !in(str, list);
    }

    /**
     * 判断一个字符串是否和一个字符串集合中的任一字符串相似，类似sql中的like算法
     * 集合字符串中，字符串间需要用英文逗号分隔
     *
     * @param str 字符串
     * @param list 字符串
     * @return boolean
     */
    public static boolean like(String str, String list) {
        if (str == null && list == null) {
            return true;
        } else if (list == null) {
            return false;
        } else if (str == null) {
            return false;
        }
        list = list.trim();
        String[] strArr = list.split(",");
        for (int i = 0; i < strArr.length; i++) {
            if (StrTool.like(str, strArr[i].trim())) {
                return true;
            }
        }
        return false;
    }

    /**
     * 判断一个字符串是否和一个字符串集合中的任一字符串相似，类似sql中的not like算法
     * 集合字符串中，字符串间需要用英文逗号分隔
     *
     * @param str1 字符串
     * @param str2 字符串
     * @return boolean
     */
    public static boolean notLike(String str1, String str2) {
        return !like(str1, str2);
    }


    /**
     * 获取两个日期之间相隔年，向上取整
     *
     * @param startDate 开始时间
     * @param endDate 结束时间
     * @return 数值
     * @throws Exception 异常
     */
    public static double intervalByDay(String startDate, String endDate) throws Exception {
        return Math.ceil(PubFun.calInterval2(PubFun.newCalDate(startDate, "D", 1), endDate, "D") / 365.00);
    }
}
