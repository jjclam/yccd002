package com.sinosoft.spel;

import org.springframework.expression.Expression;
import org.springframework.expression.ExpressionParser;
import org.springframework.expression.spel.standard.SpelExpressionParser;
import org.springframework.expression.spel.support.StandardEvaluationContext;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Iterator;

/**
 * Created by mali on 2016/8/31.
 */
public class SpelUtils {
    /**
     * 可以在Map中放入参数名（key）和参数值（value）；
     * 也可以把自定义函数放到Map中：key为公式中引用的函数名，value为具体的一个方法Method
     *
     * @param expression key
     * @param paramMap map
     * @return Object 返回对象
     * @throws Exception 异常
     */
    public static Object spelExcute(String expression, HashMap<String, Object> paramMap) throws Exception {
        Object result = null;
        if (expression == null || paramMap == null || "".equals(expression)) {
            throw new Exception("入参为空，或者空串");
        }
        ExpressionParser parser = new SpelExpressionParser();
        StandardEvaluationContext context = new StandardEvaluationContext();
        context.setVariables(paramMap);
        Expression exp = parser.parseExpression(expression);
        result = exp.getValue(context);
        return result;
    }

    /**
     * 只需要把参数放到Map中传递过来，自定义函数可以不绑定
     * 如计算过公式：#a+#min(#a,#b,#c)，只需要传递a、b、c三个值，而min函数不要传递Method方法。
     * 程序自动从自定义函数类spelFunctions中获取公式中使用的函数。
     * 如果用到自定义函数，需要自己在spelFunctions类中新增方法
     *
     * @param expression key
     * @param paramMap map
     * @return Object 返回值
     * @throws Exception 异常
     */
    public static Object spelExcuteExp(String expression, HashMap<String, Object> paramMap) throws Exception {
        Object result = null;
        if (expression == null || paramMap == null || "".equals(expression)) {
            throw new Exception("入参为空，或者空串");
        }
        ExpressionParser parser = new SpelExpressionParser();
        StandardEvaluationContext context = new StandardEvaluationContext();
        context.setVariables(paramMap);
        initFuctions(expression, context);
        Expression exp = parser.parseExpression(expression);
        result = exp.getValue(context);
        return result;
    }

    /**
     * 支持在计算公式中使用自定义函数的昵称，这时候需要把昵称和自定义函数名分别作为Map的Key、
     * Value传递进来。其它用法和其同构方法一样。
     *
     * @param expression key
     * @param paramMap map
     * @param methodMap map
     * @return 返回值
     * @throws Exception 异常
     */
    public static Object spelExcuteExp(String expression, HashMap<String, Object> paramMap, HashMap<String, String> methodMap) throws Exception {
        Object result = null;
        if (expression == null || paramMap == null || "".equals(expression)) {
            throw new Exception("入参为空，或者空串");
        }
        ExpressionParser parser = new SpelExpressionParser();
        StandardEvaluationContext context = new StandardEvaluationContext();
        context.setVariables(paramMap);
        initFuctions(expression, methodMap, context);
        Expression exp = parser.parseExpression(expression);
        result = exp.getValue(context);
        return result;
    }

    /**
     * 根据计算公式使用的自定义函数名称，把spelFunctions中自定义函数加载到StandardEvaluationContext
     *
     * @param expression key
     * @param context 自定义函数
     */
    private static void initFuctions(String expression, StandardEvaluationContext context) {
        //获取spel自定义的所有函数
        Method[] methods = SpelFunctions.class.getDeclaredMethods();
        for (int i = 0; i < methods.length; i++) {
            if (expression.contains(methods[i].getName())) {
                context.setVariable(methods[i].getName(), methods[i]);
            }
        }
    }

    /**
     * 根据公式中自定义函数名称的昵称，找到对应的自定义函数。
     *
     * @param expression key
     * @param methodMap map
     * @param context 自定义函数
     */
    private static void initFuctions(String expression, HashMap<String, String> methodMap, StandardEvaluationContext context) {
        //获取spel自定义的所有函数
        Method[] methods = SpelFunctions.class.getDeclaredMethods();
        Iterator<String> keyIterator = methodMap.keySet().iterator();
        while (keyIterator.hasNext()) {
            String funcNickname = keyIterator.next();
            String funcName = methodMap.get(funcNickname);
            if (expression.contains(funcNickname)) {
                for (int i = 0; i < methods.length; i++) {
                    if (funcName.equalsIgnoreCase(methods[i].getName())) {
                        context.setVariable(funcNickname, methods[i]);
                        break;
                    }
                }
            }

        }

    }
}
