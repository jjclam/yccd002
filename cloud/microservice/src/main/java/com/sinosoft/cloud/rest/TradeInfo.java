package com.sinosoft.cloud.rest;



import org.apache.commons.lang.exception.ExceptionUtils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashMap;

/**
 * 微服务 - 通用请求响应实体类
 * Created by kaine on 2017/1/1.
 */
public class TradeInfo implements Serializable {
    private static final long serialVersionUID = -7677969583641452256L;
    //服务节点
    private String serviceNode = null;
    //业务数据封装对象
    private HashMap<String, Object> tradeMap = new HashMap<String, Object>();
    //数据库操作对象，和tradeMap连用：storageMap中的key是tradeMap，value是操作符
    private LinkedHashMap<String, String> storageMap = new LinkedHashMap<String, String>();
    //异常
    private String exception = null;
    //异常信息
    private ArrayList<String> errorList = new ArrayList<String>();
    //是否需要分库标记
    private boolean shardFlag = false;
    //分库主键
    private String shardKey = null;
    //数据库操作符：插入
    private static final String INSERT = "INSERT";
    //数据库操作符：插入
    private static final String UPDATE = "UPDATE";
    //数据库操作符：插入
    private static final String DELETE = "DELETE";
    //数据库操作符：插入
    private static final String DELETE_AND_INSERT = "DELETE&INSERT";
    private String action;

    public TradeInfo() {

    }

    public TradeInfo(HashMap<String, Object> pMap) {
        this.tradeMap.putAll(pMap);
    }

    public TradeInfo addError(String error) {
        this.errorList.add(error);
        return this;
    }

    public TradeInfo addErrors(Collection<String> errors) {
        this.errorList.addAll(errors);
        return this;
    }

    public TradeInfo addData(String key, Object value) {
        this.tradeMap.put(key, value);
        return this;
    }

    public TradeInfo addData(Object value) {
        this.tradeMap.put(value.getClass().getName(), value);
        return this;
    }

    public TradeInfo addData(HashMap<String, Object> pMap) {
        this.tradeMap.putAll(pMap);
        return this;
    }

    public Object getData(String key) {
        return this.tradeMap.get(key);
    }

    private boolean hasKey(String key) {
        return this.tradeMap.containsKey(key) && tradeMap.get(key) != null;
    }

    /**
     *
     * @param key  key
     * @return 是否put成功
     */
    public boolean doInsert(String key) {
        if (this.hasKey(key)) {
            this.storageMap.put(key, INSERT);
            return true;
        } else {
            return false;
        }
    }

    /**
     *
     * @param key key
     *
     * @return 是否删除成功
     */
    public boolean doDelete(String key) {
        if (this.hasKey(key)) {
            this.storageMap.put(key, DELETE);
            return true;
        } else {
            return false;
        }
    }

    /**
     *
     * @param key key
     * @return 是否更新成功
     */
    public boolean doUpdate(String key) {
        if (this.hasKey(key)) {
            this.storageMap.put(key, UPDATE);
            return true;
        } else {
            return false;
        }
    }

    /**
     *
     * @param key key
     * @return 删除插入
     */
    public boolean doDelAndIns(String key) {
        if (this.hasKey(key)) {
            this.storageMap.put(key, DELETE_AND_INSERT);
            return true;
        } else {
            return false;
        }
    }

    public void removeData(String key) {
        this.tradeMap.remove(key);
        this.storageMap.remove(key);
    }

    public void removeStorage(String key) {
        this.storageMap.remove(key);
    }

    public void clearData() {
        this.tradeMap.clear();
    }

    public void clearStorage() {
        this.storageMap.clear();
    }

    /**
     * 清空TradeInfo
     */
    public void clearTradeInfo() {
        this.tradeMap.clear();
        this.storageMap.clear();
        this.errorList.clear();
        this.exception = null;
        this.serviceNode = null;
        this.shardFlag = false;
        this.shardKey = null;
    }

    public boolean hasError() {
        return (this.exception != null || this.errorList.size() > 0);
    }

    public String getServiceNode() {
        return serviceNode;
    }

    public void setServiceNode(String serviceNode) {
        this.serviceNode = serviceNode;
    }

    public HashMap<String, Object> getTradeMap() {
        return tradeMap;
    }

    public void setTradeMap(HashMap<String, Object> tradeMap) {
        this.tradeMap = tradeMap;
    }

    public LinkedHashMap<String, String> getStorageMap() {
        return storageMap;
    }

    public ArrayList<String> getErrorList() {
        return errorList;
    }

    public void setErrorList(ArrayList<String> errorList) {
        this.errorList = errorList;
    }

    public String getException() {
        return exception;
    }

    public void setException(Throwable exception) {
        this.exception = ExceptionUtils.getFullStackTrace(exception);
    }

    public void setException(String exception) {
        this.exception = exception;
    }

    public String getShardKey() {
        return shardKey;
    }

    public void setShardKey(String shardKey) {
        this.shardKey = shardKey;
    }

    public boolean getShardFlag() {
        return shardFlag;
    }

    public void setShardFlag(boolean shardFlag) {
        this.shardFlag = shardFlag;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }


    @Override
    public String toString() {
        return "TradeInfo{" +
                "serviceNode='" + serviceNode + '\'' +
                ", tradeMap=" + tradeMap +
                ", storageMap=" + storageMap +
                ", exception=" + exception +
                ", errorList=" + errorList +
                ", shardFlag=" + shardFlag +
                ", shardKey='" + shardKey + '\'' +
                ", action='" + action + '\'' +
                '}';
    }
}
