package com.sinosoft.cloud.microservice;


import com.sinosoft.cloud.rest.TradeInfo;

/**
 * 微服务 - 基础接口
 * 用于提供统一的外部接口定义
 * Created by kaine on 2016/12/30.
 */
public interface IMicroService {
    /**
     * 提交服务请求
     *
     * @return
     */
    TradeInfo service(TradeInfo requestInfo);

    /**
     * 获取结果
     *
     * @return
     */
    Object getResult();

    /**
     * 获取错误
     *
     * @return
     */
    Object getError();
}
