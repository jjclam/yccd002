package com.sinosoft.cloud.bom;

/**
 * Created by sinosoft1 on 2017/2/10.
 */
public class BOMInsurant {

    //险种保单号
    private String PolNo;
    //险种名称
    private String RiskCode;
    //被保人客户号
    private String InsuredNo;
    //医疗职业加费标准
    private String MedicalOccAddFeeStandard;
    //投保年龄
    private int AppAge;
    //累计身故风险保额
    private Double SumDieRiskAmnt;
    //累积人身风险保额
    private Double SumPersonalRiskAmnt;
    //存在续期核保非标准体核保决定记录标示
    private String HaveRNNonstandardUWConclusionFlag;
    //职业代码
    private String OccupationCode;
    //职业类型
    private String OccupationType;
    //存在与反洗钱系统黑名单姓名相同标示
    private String HaveBlacklistNameSameFlag;
    //出生体重
    private Double BirthWeight;
    //既往理赔记录存在标示
    private String HaveClaimMadeFlag;
    //异常告知存在标示
    private String HaveAbnormalToldFlag;
    //非标准体承保记录存在标示
    private String HaveNonstandardUWConclusionFlag;
    //系投保人关系
    private String RelationToAppnt;
    //BMI指数
    private Double BMIIndex;
    //投保年龄天数
    private int AppAgeDays;


    //被保人职业类别
    private String job;
    //证件类型
    private String IDtype;
    //被保人国籍
    private String NativePlace;
    //被保人既往查询
    private String InsuredCount;
    //被保人证件类型
    private String Result;
    //被保人既往查询微信
    private String InsuredSum;
    //吸烟指数
    private String SmokeSum;
    //饮酒指数
    private String AlcoholSum;
    //出生周期
    private String week;

    //出生情况
    private String BirthStatus;

    public String getBirthStatus() {
        return BirthStatus;
    }

    public void setBirthStatus(String birthStatus) {
        BirthStatus = birthStatus;
    }

    public String getAlcoholSum() {
        return AlcoholSum;
    }

    public void setAlcoholSum(String alcoholSum) {
        AlcoholSum = alcoholSum;
    }

    public String getIDtype() {
        return IDtype;
    }

    public void setIDtype(String IDtype) {
        this.IDtype = IDtype;
    }

    public String getInsuredCount() {
        return InsuredCount;
    }

    public void setInsuredCount(String insuredCount) {
        InsuredCount = insuredCount;
    }

    public String getInsuredSum() {
        return InsuredSum;
    }

    public void setInsuredSum(String insuredSum) {
        InsuredSum = insuredSum;
    }

    public String getJob() {
        return job;
    }

    public void setJob(String job) {
        this.job = job;
    }

    public String getNativePlace() {
        return NativePlace;
    }

    public void setNativePlace(String nativePlace) {
        NativePlace = nativePlace;
    }

    public String getResult() {
        return Result;
    }

    public void setResult(String result) {
        Result = result;
    }

    public String getSmokeSum() {
        return SmokeSum;
    }

    public void setSmokeSum(String smokeSum) {
        SmokeSum = smokeSum;
    }

    public String getWeek() {
        return week;
    }

    public void setWeek(String week) {
        this.week = week;
    }

    public String getInsuredNo() {
        return InsuredNo;
    }

    public void setInsuredNo(String insuredNo) {
        InsuredNo = insuredNo;
    }

    public String getPolNo() {
        return PolNo;
    }

    public void setPolNo(String polNo) {
        PolNo = polNo;
    }

    public String getRiskCode() {
        return RiskCode;
    }

    public void setRiskCode(String riskCode) {
        RiskCode = riskCode;
    }

    public String getMedicalOccAddFeeStandard() {
        return MedicalOccAddFeeStandard;
    }

    public void setMedicalOccAddFeeStandard(String medicalOccAddFeeStandard) {
        MedicalOccAddFeeStandard = medicalOccAddFeeStandard;
    }

    public int getAppAge() {
        return AppAge;
    }

    public void setAppAge(int appAge) {
        AppAge = appAge;
    }

    public Double getSumDieRiskAmnt() {
        return SumDieRiskAmnt;
    }

    public void setSumDieRiskAmnt(Double sumDieRiskAmnt) {
        SumDieRiskAmnt = sumDieRiskAmnt;
    }

    public Double getSumPersonalRiskAmnt() {
        return SumPersonalRiskAmnt;
    }

    public void setSumPersonalRiskAmnt(Double sumPersonalRiskAmnt) {
        SumPersonalRiskAmnt = sumPersonalRiskAmnt;
    }

    public String getHaveRNNonstandardUWConclusionFlag() {
        return HaveRNNonstandardUWConclusionFlag;
    }

    public void setHaveRNNonstandardUWConclusionFlag(String haveRNNonstandardUWConclusionFlag) {
        HaveRNNonstandardUWConclusionFlag = haveRNNonstandardUWConclusionFlag;
    }

    public String getOccupationCode() {
        return OccupationCode;
    }

    public void setOccupationCode(String occupationCode) {
        OccupationCode = occupationCode;
    }

    public String getOccupationType() {
        return OccupationType;
    }

    public void setOccupationType(String occupationType) {
        OccupationType = occupationType;
    }

    public String getHaveBlacklistNameSameFlag() {
        return HaveBlacklistNameSameFlag;
    }

    public void setHaveBlacklistNameSameFlag(String haveBlacklistNameSameFlag) {
        HaveBlacklistNameSameFlag = haveBlacklistNameSameFlag;
    }

    public Double getBirthWeight() {
        return BirthWeight;
    }

    public void setBirthWeight(Double birthWeight) {
        BirthWeight = birthWeight;
    }

    public String getHaveClaimMadeFlag() {
        return HaveClaimMadeFlag;
    }

    public void setHaveClaimMadeFlag(String haveClaimMadeFlag) {
        HaveClaimMadeFlag = haveClaimMadeFlag;
    }

    public String getHaveAbnormalToldFlag() {
        return HaveAbnormalToldFlag;
    }

    public void setHaveAbnormalToldFlag(String haveAbnormalToldFlag) {
        HaveAbnormalToldFlag = haveAbnormalToldFlag;
    }

    public String getHaveNonstandardUWConclusionFlag() {
        return HaveNonstandardUWConclusionFlag;
    }

    public void setHaveNonstandardUWConclusionFlag(String haveNonstandardUWConclusionFlag) {
        HaveNonstandardUWConclusionFlag = haveNonstandardUWConclusionFlag;
    }

    public String getRelationToAppnt() {
        return RelationToAppnt;
    }

    public void setRelationToAppnt(String relationToAppnt) {
        RelationToAppnt = relationToAppnt;
    }

    public Double getBMIIndex() {
        return BMIIndex;
    }

    public void setBMIIndex(Double BMIIndex) {
        this.BMIIndex = BMIIndex;
    }

    public int getAppAgeDays() {
        return AppAgeDays;
    }

    public void setAppAgeDays(int appAgeDays) {
        AppAgeDays = appAgeDays;
    }
}
