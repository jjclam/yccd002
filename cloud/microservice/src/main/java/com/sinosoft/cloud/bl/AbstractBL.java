package com.sinosoft.cloud.bl;

import com.sinosoft.cloud.rest.TradeInfo;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExceptionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * @project: abc-cloud
 * @author: yangming
 * @date: 2017/9/17 下午4:25
 * To change this template use File | Settings | File and Code Templates.
 */
public abstract class AbstractBL implements BL {

    private final Log logger = LogFactory.getLog(getClass());

    private CErrors cErrors;

    @Override
    public TradeInfo submitData(TradeInfo requestInfo) {
        try {
            if (!checkData(requestInfo)) {
                requestInfo.addError(this.getClass().getName() + ",数据校验不合法。");
                return requestInfo;
            }

            requestInfo = dealData(requestInfo);


            if (!prepareOutputData(requestInfo)) {
                requestInfo.addError(this.getClass().getName() + ",准备数据出错。");
                return requestInfo;
            }
        } catch (Throwable e) {
            logger.error(ExceptionUtils.exceptionToString(e));
            e.printStackTrace();
            error("发生异常错误：" + e.getMessage());
        }


        return requestInfo;
    }


    /**
     * 报错信息处理
     *
     * @param msg
     */
    protected void error(String msg) {
        logger.error(msg);
        cErrors.addOneError(msg);
    }

//    protected void pojoToSchema(TradeInfo tradeInfo) {
//
//        HashMap<String, Object> hashMap = tradeInfo.getTradeMap();
//
//        for (String key : hashMap.keySet()) {
//            Object o = hashMap.get(key);
//
//            if (o instanceof Pojo) {
//                Pojo pojo = (Pojo) o;
//
//
//            }
//        }
//    }


    @Override
    public CErrors getErrors() {
        return cErrors;
    }

    @Override
    public abstract TradeInfo dealData(TradeInfo requestInfo);

    /**
     * 可以自行覆盖
     *
     * @param requestInfo
     * @return
     */
    @Override
    public boolean checkData(TradeInfo requestInfo) {
        return true;
    }

    /**
     * 可以自行覆盖
     *
     * @param requestInfo
     * @return
     */
    @Override
    public boolean prepareOutputData(TradeInfo requestInfo) {
        return true;
    }


}