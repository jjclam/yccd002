package com.sinosoft.cloud.bom;

import java.util.Date;
import java.util.List;

/**
 * Created by sinosoft1 on 2017/2/10.
 */
public class BOMPolicy {
    //保单号
    private String ContNo;
    //管理机构
    private String ManageCom;
    //主险编码
    private String MainRiskCode;
    //销售渠道
    private String SaleChannel;
    //销售方式
    private String SellType;
    //生效日期
    private Date CValidate;
    //保费
    private Double Prem;
    //投保机构
    private String AppCom;
    //投保单申请日期
    private Date PolApplyDate;
    //核保规则固定时间
    private Date UWDate;
    //主险保额
    private Double MainRiskAmnt;
    //核保结论
    private String SpecialSum;
    //既往保单状态
    private String RiskSum;

    public String getRiskSum() {
        return RiskSum;
    }

    public void setRiskSum(String riskSum) {
        RiskSum = riskSum;
    }

    private BOMApplicant bomApplicant;

    private List<BOMProduct> bomProductList;

    private List<BOMInsurant> bomInsurantList;


    public Date getPolApplyDate() {
        return PolApplyDate;
    }

    public void setPolApplyDate(Date polApplyDate) {
        PolApplyDate = polApplyDate;
    }

    public Double getMainRiskAmnt() {
        return MainRiskAmnt;
    }

    public void setMainRiskAmnt(Double mainRiskAmnt) {
        MainRiskAmnt = mainRiskAmnt;
    }

    public Date getUWDate() {
        return UWDate;
    }

    public void setUWDate(Date UWDate) {
        this.UWDate = UWDate;
    }

    public String getSpecialSum() {
        return SpecialSum;
    }

    public void setSpecialSum(String specialSum) {
        SpecialSum = specialSum;
    }

    public String getContNo() {
        return ContNo;
    }

    public void setContNo(String contNo) {
        ContNo = contNo;
    }

    public String getManageCom() {
        return ManageCom;
    }

    public void setManageCom(String manageCom) {
        ManageCom = manageCom;
    }

    public String getMainRiskCode() {
        return MainRiskCode;
    }

    public void setMainRiskCode(String mainRiskCode) {
        MainRiskCode = mainRiskCode;
    }

    public String getSaleChannel() {
        return SaleChannel;
    }

    public void setSaleChannel(String saleChannel) {
        SaleChannel = saleChannel;
    }

    public String getSellType() {
        return SellType;
    }

    public void setSellType(String sellType) {
        SellType = sellType;
    }

    public Date getCValidate() {
        return CValidate;
    }

    public void setCValidate(Date CValidate) {
        this.CValidate = CValidate;
    }

    public Double getPrem() {
        return Prem;
    }

    public void setPrem(Double prem) {
        Prem = prem;
    }

    public String getAppCom() {
        return AppCom;
    }

    public void setAppCom(String appCom) {
        AppCom = appCom;
    }

    public BOMApplicant getBomApplicant() {
        return bomApplicant;
    }

    public void setBomApplicant(BOMApplicant bomApplicant) {
        this.bomApplicant = bomApplicant;
    }

    public List<BOMProduct> getBomProductList() {
        return bomProductList;
    }

    public void setBomProductList(List<BOMProduct> bomProductList) {
        this.bomProductList = bomProductList;
    }

    public List<BOMInsurant> getBomInsurantList() {
        return bomInsurantList;
    }

    public void setBomInsurantList(List<BOMInsurant> bomInsurantList) {
        this.bomInsurantList = bomInsurantList;
    }
}
