package com.sinosoft.cloud.microservice;


import com.sinosoft.cloud.common.SpringContextUtils;
import com.sinosoft.cloud.dataroute.HashShardServiceImpl;
import com.sinosoft.cloud.dataroute.IHashShardService;
import com.sinosoft.cloud.rest.TradeInfo;
import com.sinosoft.utility.ExceptionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.log4j.MDC;
import org.springframework.beans.factory.BeanNameAware;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.sleuth.Span;
import org.springframework.cloud.sleuth.Tracer;

import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * 微服务框架 - 模板实现类
 * 用于控制公共处理流程
 * Created by kaine on 2016/12/31.
 */
public abstract class AbstractMicroService implements IMicroService, BeanNameAware {
    private final Log logger = LogFactory.getLog(getClass());

    // 分库组件，用于完成分库操作
//    private IHashShardService iHashShardService;


    @Value("${spring.application.name}")
    private String applicationName;

    @Value("${server.port}")
    private String serverPort;

    @Autowired
    private Tracer tracer;


    /**
     * 提交服务请求
     *
     * @return
     */
    @Override
    public final TradeInfo service(TradeInfo requestInfo) {
        String serialNo = (String) requestInfo.getData("serialNo");
        MDC.put("transNo", serialNo);
        Span span = tracer.createSpan(getClass().getSimpleName());
        long startTime = System.currentTimeMillis();
        if (logger.isDebugEnabled()) {
            logger.debug(this.getClass().getName() + ",inflowNode:" + requestInfo.getServiceNode() + ",Inparam toString:" + requestInfo.toString());
        }
        try {
            if (null != requestInfo.getShardKey() && "".equals(requestInfo.getShardKey())) {
                // 监控系统核心监控日志，勿动！
                logger.info("{orderkey:" + requestInfo.getShardKey() + "}");
            }
            //置分库标记`
            needSharding(requestInfo);

            //数据校验
            span.logEvent("start checkData");
            if (checkData(requestInfo)) {
//                span.logEvent("end checkData");
                if (requestInfo.getShardFlag() && (requestInfo.getShardKey() == null || "".equals(requestInfo.getShardKey()))) {
                    requestInfo.addError(this.getClass().getName() + ",此服务需要分库，但分库主键为空。");
                    if (logger.isErrorEnabled()) {
                        logger.error(this.getClass().getName() + ",此服务需要分库，但分库主键为空。");
                    }
                    return requestInfo;
                }
            } else {
                requestInfo.addError(this.getClass().getName() + ",数据校验不合法。");
                if (logger.isErrorEnabled()) {
                    logger.error(this.getClass().getName() + ",此服务需要分库，但分库主键为空。");
                }
                return requestInfo;
            }

            //执行分库逻辑
            /*if (requestInfo.getShardFlag()) {
                try {
                    setShardKey(requestInfo.getShardKey());
                } catch (Exception e) {
                    requestInfo.setException(e);
                    requestInfo.addError("分库失败：" + this.getClass().getName());
                    if (logger.isErrorEnabled()) {
                        logger.error(ExceptionUtils.exceptionToString(e));
                    }
                    return requestInfo;
                }
            }*/

            //业务处理
            span.logEvent("start dealData");
            requestInfo = dealData(requestInfo);
//            span.logEvent("end dealData");
            if (logger.isDebugEnabled()) {
                long endTime = System.currentTimeMillis();
                logger.debug(this.getClass().getName() + ",deal time:" + endTime + "-" + startTime + "=" + (endTime - startTime));
            }
            requestInfo.setServiceNode("");
            requestInfo.setShardFlag(false);
        } catch (Exception e) {
            span.logEvent("exception");
            requestInfo.setException(e);
            requestInfo.addError(this.getClass().getName() + ",微服务处理异常。");
            if (logger.isErrorEnabled()) {
                logger.error(ExceptionUtils.exceptionToString(e));
            }
        }
        if (logger.isDebugEnabled()) {
            logger.debug(this.getClass().getName() + ",Outparam toString：" + requestInfo.toString());
        }
        tracer.close(span);
        MDC.remove("transNo");
        return requestInfo;
    }

    /**
     * @param shardKey shardKey
     * @throws Exception 异常
     */
    /*public void setShardKey(String shardKey) throws Exception {
        if (shardKey == null || ("").equals(shardKey)) {
            throw new Exception("需要分库操作，但分库主键为空！");
        }
        if (iHashShardService == null) {
            iHashShardService = SpringContextUtils.getBeanByClass(HashShardServiceImpl.class);
        }
        logger.info("hashShardingKey:" + shardKey);
        if (!iHashShardService.setCurrentDataSource("PERCONTNO", shardKey)) {
            logger.error("分库主键：" + shardKey + "在分库获取数据库失败.....");
        }
    }*/

    /**
     * 获取结果
     *
     * @return
     */
    @Override
    public Object getResult() {
        return null;
    }

    /**
     * 获取错误
     *
     * @return
     */
    @Override
    public Object getError() {
        return null;
    }

    /***
     * 数据校验
     * @param requestInfo
     * @return
     */
    public abstract boolean checkData(TradeInfo requestInfo);

    /**
     * 添加默认不分库分表
     *
     * @param requestInfo
     */
    public void needSharding(TradeInfo requestInfo) {
        requestInfo.setShardFlag(false);
    }

    /**
     * 实现具体业务
     *
     * @return
     */
    public abstract TradeInfo dealData(TradeInfo requestInfo);


    /**
     * 此方法会在Bean初始化的时候 执行。在此处，需要debug出当前Service的服务地址，便于调试。
     *
     * @param name beanName
     */
    @Override
    public void setBeanName(String name) {
        String serverAddress = null;
        try {
            serverAddress = InetAddress.getLocalHost().getHostName();
        } catch (UnknownHostException e) {
            e.printStackTrace();
            if (logger.isErrorEnabled()) {
                logger.error(ExceptionUtils.exceptionToString(e));
            }
        }
        logger.info("http://" + serverAddress + ":" + serverPort + "/rest/" + applicationName + "/" + name);
    }

    @Override
    public String toString() {
        return "AbstractMicroService{"
                + " applicationName='" + applicationName + '\''
                + ", serverPort='" + serverPort + '\''
                + '}';
    }
}
