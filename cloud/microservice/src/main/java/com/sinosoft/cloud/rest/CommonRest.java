package com.sinosoft.cloud.rest;

import com.sinosoft.cloud.microservice.IMicroService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 */
@RestController
public class CommonRest {

    @Autowired
    private BeanFactory beanFactory;
    private Log logger = LogFactory.getLog(getClass());
    /**
     *
     * @param moduleName moudelname
     * @param serviceName serviceName
     * @param tradeInfo 接收参数
     * @return 返回TradeInfo
     */
    @PostMapping(path = "/rest/{moduleName}/{serviceName}")
    @ResponseBody
    public TradeInfo service(@PathVariable("moduleName") String moduleName, @PathVariable("serviceName") String serviceName, @RequestBody TradeInfo tradeInfo,HttpServletRequest request) {
        IMicroService microService = getMicroService(serviceName);
        logger.info("===<call /rest/"+moduleName+"/"+serviceName+",TraceID="+request.getHeader("X-B3-TraceId")+",SpanId="+request.getHeader("X-B3-SpanId")+">===");
        TradeInfo result = microService.service(tradeInfo);
        return result;
    }

    /**
     *
     * @param moduleName moudleName
     * @param serviceName serviceName
     * @param action action
     * @param tradeInfo 参数
     * @return 返回TradeInfo
     */
    @PostMapping(path = "/rest/{moduleName}/{serviceName}/{action}")
    @ResponseBody
    public TradeInfo serviceAction(
            @PathVariable("moduleName") String moduleName,
            @PathVariable("serviceName") String serviceName,
            @PathVariable("action") String action,
            @RequestBody TradeInfo tradeInfo) {
        IMicroService microService = getMicroService(serviceName);
        if (tradeInfo != null) {
            tradeInfo.setAction(action);
        }
        TradeInfo result = microService.service(tradeInfo);
        return result;
    }

    /**
     *
     * @param moduleName moudlename
     * @param serviceName serviceName
     * @param response response
     * @return 回声测试
     */
    @GetMapping(path = "/rest/{moduleName}/{serviceName}")
    @ResponseBody
    public String echo(@PathVariable("moduleName") String moduleName, @PathVariable("serviceName") String serviceName, HttpServletResponse response) {
        IMicroService microService = getMicroService(serviceName);
        String testStr = "回声测试    moduleName:" + moduleName + " serverName:" + serviceName + " ";
        if (microService != null) {
            testStr += microService.toString();
        } else {
            testStr += "服务未启动~！ 请检查";
            response.setStatus(HttpServletResponse.SC_METHOD_NOT_ALLOWED);
        }
        return testStr;
    }


    /**
     * 处理获取Bean的过程。
     *
     * @param name
     * @return
     */
    private IMicroService getMicroService(String name) {
        return (IMicroService) beanFactory.getBean(name);
    }
}
