package com.sinosoft.cloud.bom;

/**
 * Created by LZC on 2017/6/7.
 */
public class BOMBeneficiary {

    //受益人类别
    private String BnfType;
    //与被保人关系
    private String RelationToInsured;
    //证件类型
    private String IDtype;
    //受益人证件类型
    private String CheckRes;

    public String getBnfType() {
        return BnfType;
    }

    public void setBnfType(String bnfType) {
        BnfType = bnfType;
    }

    public String getCheckRes() {
        return CheckRes;
    }

    public void setCheckRes(String checkRes) {
        CheckRes = checkRes;
    }

    public String getIDtype() {
        return IDtype;
    }

    public void setIDtype(String IDtype) {
        this.IDtype = IDtype;
    }

    public String getRelationToInsured() {
        return RelationToInsured;
    }

    public void setRelationToInsured(String relationToInsured) {
        RelationToInsured = relationToInsured;
    }
}
