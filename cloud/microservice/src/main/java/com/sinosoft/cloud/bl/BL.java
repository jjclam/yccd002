package com.sinosoft.cloud.bl;

import com.sinosoft.cloud.rest.TradeInfo;
import com.sinosoft.utility.CErrors;

/**
 * BL接口
 */
public interface BL {
    TradeInfo submitData(TradeInfo requestInfo);
    TradeInfo dealData(TradeInfo requestInfo);
    boolean checkData(TradeInfo requestInfo);
    boolean prepareOutputData(TradeInfo requestInfo);
    CErrors getErrors();
}