package com.sinosoft.cloud.bom;

/**
 * Created by sinosoft1 on 2017/2/10.
 */
public class BOMProduct {
    //险种编码
    private String PolNo;
    //被保人客户号
    private String InsuredNo;
    //险种类别3
    private String RiskType3;
    //红利领取方式
    private String BonusGetMode;
    //险种类别4
    private String RiskType4;
    //险种类别5
    private String RiskType5;
    //险种编码
    private String RiskCode;
    //保费
    private Double Prem;
    //交费频次
    private String Payintv;
    //保险期间
    private int InsuYear;
    //保险期间单位
    private String InsuYearFlag;
    //交费期间
    private int PayEndYear;
    //交费期间单位
    private String PayEndYearFlag;
    //责任符合产品定义标示
    private String DutyAccordWithProdDefFlag;
    //受益人存在与反洗钱系统黑名单姓名相同标示
    private String BnfHaveBlacklistNameSameFlag;
    //累计同一被保人险种保费
    private Double RiskTotalPrem;
    //被保人累计重疾险保额
    private Double DSumDangerAmnt;
    //被保人累计重大疾病保额
    private Double TotalBigDAmnt;
    //被保人累积人身风险保额
    private Double MSumDangerAmnt;
    //未成年人累计身故风险保额
    private Double SumMinorAmnt;
    //累计投资理财型产品的寿险风险保额
    private Double SumInvestFinLifeRiskAmnt;
    //投资理财型产品标示
    private String InvestFinanceFlag;
    //投保人累计所交保费
    private Double AppntSumPrem;
    //投保风险保障型产品标示
    private String InsuRiskProtectionProdFlag;
    //被保人累计寿险风险保额、 健康险-疾病身故风险保额
    private Double LSumDangerAmnt;
    //被保人一般意外风险保额
    private Double ASumDangerAmnt;
    //被保人累计公共交通意外风险保额
    private Double SumPublicTransitAmnt;
    //被保人累计航空意外风险保额
    private Double SumAviationAmnt;
    //保额
    private Double Amnt;
    //被保人累计风险保障型产品的寿险风险保额
    private Double RPLSumDangerAmnt;
    //被保人累计风险保障型产品的一般意外风险保额
    private Double RPASumDangerAmnt;
    //被保人累计风险保障型产品的公共交通意外风险保额
    private Double RPSumPublicTransitAmnt;
    //被保人累计风险保障型产品的航空意外风险保额
    private Double RPSumAviationAmnt;
    //被保人累计风险保障型产品的重疾险风险保额
    private Double RPDSumDangerAmnt;
    //主险保险年龄年期
    private int InsuYear1;
    //主险保险年龄年期标志
    private String InsuYearFlag1;
    //主险终交年龄年期
    private int PayEndYear1;
    //主险终交年龄年期标志
    private String PayEndYearFlag1;
    //主险缴费方式
    private String Payintv1;
    //主险责任缴费方式
    private String MPayintv;
    //未成年人身故风险保额
    private Double MinorDangerAmnt;
    //份数
    private Double Mult;
    //主附险捆绑标记
    private int MainRiskFlag;
    //主附险捆绑标记
    private int MainRiskFlag1;
    //附加险捆绑标记
    private int AddRiskFlag;
    //责任编码
    private String DutyCode;


    public void setMPayintv(String MPayintv) {
        this.MPayintv = MPayintv;
    }

    public String getDutyCode1() {
        return DutyCode1;
    }

    public void setDutyCode1(String dutyCode1) {
        DutyCode1 = dutyCode1;
    }

    //责任编码
    private String DutyCode1;
    //被保人累计风险保障型产品的自驾车意外风险保额
    private Double RPSelfDrivingAmnt;
    //被保人累计自驾车意外风险保额
    private Double SumSelfDrivingAmnt;


    private BOMInsurant bomInsurant;
    private BOMBeneficiary bomBeneficiary;

    public int getAddRiskFlag() {
        return AddRiskFlag;
    }

    public void setAddRiskFlag(int addRiskFlag) {
        AddRiskFlag = addRiskFlag;
    }

    public String getMPayintv() {
        return MPayintv;
    }

    public String getDutyCode() {
        return DutyCode;
    }

    public void setDutyCode(String dutyCode) {
        DutyCode = dutyCode;
    }

    public int getMainRiskFlag1() {
        return MainRiskFlag1;
    }

    public void setMainRiskFlag1(int mainRiskFlag1) {
        MainRiskFlag1 = mainRiskFlag1;
    }

    public int getMainRiskFlag() {
        return MainRiskFlag;
    }

    public void setMainRiskFlag(int mainRiskFlag) {
        MainRiskFlag = mainRiskFlag;
    }


    public String getPolNo() {
        return PolNo;
    }

    public void setPolNo(String polNo) {
        PolNo = polNo;
    }

    public String getInsuredNo() {
        return InsuredNo;
    }

    public void setInsuredNo(String insuredNo) {
        InsuredNo = insuredNo;
    }

    public String getRiskType3() {
        return RiskType3;
    }

    public void setRiskType3(String riskType3) {
        RiskType3 = riskType3;
    }

    public String getBonusGetMode() {
        return BonusGetMode;
    }

    public void setBonusGetMode(String bonusGetMode) {
        BonusGetMode = bonusGetMode;
    }

    public String getRiskType4() {
        return RiskType4;
    }

    public void setRiskType4(String riskType4) {
        RiskType4 = riskType4;
    }

    public String getRiskType5() {
        return RiskType5;
    }

    public void setRiskType5(String riskType5) {
        RiskType5 = riskType5;
    }

    public String getRiskCode() {
        return RiskCode;
    }

    public void setRiskCode(String riskCode) {
        RiskCode = riskCode;
    }

    public Double getPrem() {
        return Prem;
    }

    public void setPrem(Double prem) {
        Prem = prem;
    }

    public String getPayintv() {
        return Payintv;
    }

    public void setPayintv(String payintv) {
        Payintv = payintv;
    }

    public int getInsuYear() {
        return InsuYear;
    }

    public void setInsuYear(int insuYear) {
        InsuYear = insuYear;
    }

    public String getInsuYearFlag() {
        return InsuYearFlag;
    }

    public void setInsuYearFlag(String insuYearFlag) {
        InsuYearFlag = insuYearFlag;
    }

    public int getPayEndYear() {
        return PayEndYear;
    }

    public void setPayEndYear(int payEndYear) {
        PayEndYear = payEndYear;
    }

    public String getPayEndYearFlag() {
        return PayEndYearFlag;
    }

    public void setPayEndYearFlag(String payEndYearFlag) {
        PayEndYearFlag = payEndYearFlag;
    }

    public String getDutyAccordWithProdDefFlag() {
        return DutyAccordWithProdDefFlag;
    }

    public void setDutyAccordWithProdDefFlag(String dutyAccordWithProdDefFlag) {
        DutyAccordWithProdDefFlag = dutyAccordWithProdDefFlag;
    }

    public String getBnfHaveBlacklistNameSameFlag() {
        return BnfHaveBlacklistNameSameFlag;
    }

    public void setBnfHaveBlacklistNameSameFlag(String bnfHaveBlacklistNameSameFlag) {
        BnfHaveBlacklistNameSameFlag = bnfHaveBlacklistNameSameFlag;
    }

    public Double getRiskTotalPrem() {
        return RiskTotalPrem;
    }

    public void setRiskTotalPrem(Double riskTotalPrem) {
        RiskTotalPrem = riskTotalPrem;
    }

    public Double getDSumDangerAmnt() {
        return DSumDangerAmnt;
    }

    public void setDSumDangerAmnt(Double DSumDangerAmnt) {
        this.DSumDangerAmnt = DSumDangerAmnt;
    }

    public Double getTotalBigDAmnt() {
        return TotalBigDAmnt;
    }

    public void setTotalBigDAmnt(Double totalBigDAmnt) {
        TotalBigDAmnt = totalBigDAmnt;
    }

    public Double getMSumDangerAmnt() {
        return MSumDangerAmnt;
    }

    public void setMSumDangerAmnt(Double MSumDangerAmnt) {
        this.MSumDangerAmnt = MSumDangerAmnt;
    }

    public Double getSumMinorAmnt() {
        return SumMinorAmnt;
    }

    public void setSumMinorAmnt(Double sumMinorAmnt) {
        SumMinorAmnt = sumMinorAmnt;
    }

    public Double getSumInvestFinLifeRiskAmnt() {
        return SumInvestFinLifeRiskAmnt;
    }

    public void setSumInvestFinLifeRiskAmnt(Double sumInvestFinLifeRiskAmnt) {
        SumInvestFinLifeRiskAmnt = sumInvestFinLifeRiskAmnt;
    }

    public String getInvestFinanceFlag() {
        return InvestFinanceFlag;
    }

    public void setInvestFinanceFlag(String investFinanceFlag) {
        InvestFinanceFlag = investFinanceFlag;
    }

    public Double getAppntSumPrem() {
        return AppntSumPrem;
    }

    public void setAppntSumPrem(Double appntSumPrem) {
        AppntSumPrem = appntSumPrem;
    }

    public String getInsuRiskProtectionProdFlag() {
        return InsuRiskProtectionProdFlag;
    }

    public void setInsuRiskProtectionProdFlag(String insuRiskProtectionProdFlag) {
        InsuRiskProtectionProdFlag = insuRiskProtectionProdFlag;
    }

    public Double getLSumDangerAmnt() {
        return LSumDangerAmnt;
    }

    public void setLSumDangerAmnt(Double LSumDangerAmnt) {
        this.LSumDangerAmnt = LSumDangerAmnt;
    }

    public Double getASumDangerAmnt() {
        return ASumDangerAmnt;
    }

    public void setASumDangerAmnt(Double ASumDangerAmnt) {
        this.ASumDangerAmnt = ASumDangerAmnt;
    }

    public Double getSumPublicTransitAmnt() {
        return SumPublicTransitAmnt;
    }

    public void setSumPublicTransitAmnt(Double sumPublicTransitAmnt) {
        SumPublicTransitAmnt = sumPublicTransitAmnt;
    }

    public Double getSumAviationAmnt() {
        return SumAviationAmnt;
    }

    public void setSumAviationAmnt(Double sumAviationAmnt) {
        SumAviationAmnt = sumAviationAmnt;
    }

    public Double getAmnt() {
        return Amnt;
    }

    public void setAmnt(Double amnt) {
        Amnt = amnt;
    }

    public Double getRPLSumDangerAmnt() {
        return RPLSumDangerAmnt;
    }

    public void setRPLSumDangerAmnt(Double RPLSumDangerAmnt) {
        this.RPLSumDangerAmnt = RPLSumDangerAmnt;
    }

    public Double getRPASumDangerAmnt() {
        return RPASumDangerAmnt;
    }

    public void setRPASumDangerAmnt(Double RPASumDangerAmnt) {
        this.RPASumDangerAmnt = RPASumDangerAmnt;
    }

    public Double getRPSumPublicTransitAmnt() {
        return RPSumPublicTransitAmnt;
    }

    public void setRPSumPublicTransitAmnt(Double RPSumPublicTransitAmnt) {
        this.RPSumPublicTransitAmnt = RPSumPublicTransitAmnt;
    }

    public Double getRPSumAviationAmnt() {
        return RPSumAviationAmnt;
    }

    public void setRPSumAviationAmnt(Double RPSumAviationAmnt) {
        this.RPSumAviationAmnt = RPSumAviationAmnt;
    }

    public Double getRPDSumDangerAmnt() {
        return RPDSumDangerAmnt;
    }

    public void setRPDSumDangerAmnt(Double RPDSumDangerAmnt) {
        this.RPDSumDangerAmnt = RPDSumDangerAmnt;
    }

    public int getInsuYear1() {
        return InsuYear1;
    }

    public Double getRPSelfDrivingAmnt() {
        return RPSelfDrivingAmnt;
    }

    public void setRPSelfDrivingAmnt(Double RPSelfDrivingAmnt) {
        this.RPSelfDrivingAmnt = RPSelfDrivingAmnt;
    }

    public Double getSumSelfDrivingAmnt() {
        return SumSelfDrivingAmnt;
    }

    public void setSumSelfDrivingAmnt(Double sumSelfDrivingAmnt) {
        SumSelfDrivingAmnt = sumSelfDrivingAmnt;
    }

    public void setInsuYear1(int insuYear1) {
        InsuYear1 = insuYear1;
    }

    public String getInsuYearFlag1() {
        return InsuYearFlag1;
    }

    public void setInsuYearFlag1(String insuYearFlag1) {
        InsuYearFlag1 = insuYearFlag1;
    }

    public int getPayEndYear1() {
        return PayEndYear1;
    }

    public void setPayEndYear1(int payEndYear1) {
        PayEndYear1 = payEndYear1;
    }

    public String getPayEndYearFlag1() {
        return PayEndYearFlag1;
    }

    public void setPayEndYearFlag1(String payEndYearFlag1) {
        PayEndYearFlag1 = payEndYearFlag1;
    }

    public String getPayintv1() {
        return Payintv1;
    }

    public void setPayintv1(String payintv1) {
        Payintv1 = payintv1;
    }

    public Double getMinorDangerAmnt() {
        return MinorDangerAmnt;
    }

    public void setMinorDangerAmnt(Double minorDangerAmnt) {
        MinorDangerAmnt = minorDangerAmnt;
    }

    public Double getMult() {
        return Mult;
    }

    public void setMult(Double mult) {
        Mult = mult;
    }

    public BOMInsurant getBomInsurant() {
        return bomInsurant;
    }

    public void setBomInsurant(BOMInsurant bomInsurant) {
        this.bomInsurant = bomInsurant;
    }

    public BOMBeneficiary getBomBeneficiary() {
        return bomBeneficiary;
    }

    public void setBomBeneficiary(BOMBeneficiary bomBeneficiary) {
        this.bomBeneficiary = bomBeneficiary;
    }
}