/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */
package com.sinosoft.cloud.common;

import com.sinosoft.cache.dao.impl.RedisCommonDao;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.pojo.LMCalFactorPojo;
import com.sinosoft.pojo.LMCalModePojo;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.StrTool;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * <p>Title: 保费计算类 </p>
 * <p>Description: 通过传入的保单信息和责任信息构建出保费信息和领取信息 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft</p>
 * @author HST
 * @version 1.0
 * @date 2002-07-01
 */
@Component
@Scope("prototype")
public class Calculator {
    private final Log logger = LogFactory.getLog(getClass());

    /**
     * 错误处理类，每个需要错误处理的类中都放置该类
     */
    public  CErrors mErrors = new CErrors();

    /**
     * 计算需要用到的保单号码
     */
    public String polNo;

   // @Autowired
    private RedisCommonDao redisCommonDao = SpringContextUtils.getBeanByClass(RedisCommonDao.class);
    /**
     * 各种要素存放的琏表
     * 1--基本要素、和常量要素相同，但是优先级最低
     * 2--扩展要素，根据SQL语句从新计算
     * 3--常量要素（只取默认值）
     */
    private ArrayList<LMCalFactorPojo> mCalFactors1 = new ArrayList<LMCalFactorPojo>(); // 存放基本要素

    public List<LMCalFactorPojo> mCalFactors = new ArrayList<LMCalFactorPojo>();

    // @Field
    //计算编码
    private String calCode = "";
    //算法对应SQL语句所在表结构
    private LMCalModePojo mLMCalMode = new LMCalModePojo();


    /**
     * 增加基本要素
     *
     * @param cFactorCode  要素的编码
     * @param cFactorValue 要素的数据值
     */
    public void addBasicFactor(String cFactorCode, String cFactorValue) {
        LMCalFactorPojo tS = new LMCalFactorPojo();
        tS.setFactorCode(cFactorCode);
        tS.setFactorDefault(cFactorValue);
        tS.setFactorType("1");
        mCalFactors1.add(tS);
    }

    /**
     *
     * @param cFactorCode  cFactorCode
     * @param cFactorValue cFactorValue
     * @param cFactorClass cFactorValue
     */
    public void addBasicFactor(String cFactorCode, String cFactorValue, Class cFactorClass) {
        LMCalFactorPojo tS = new LMCalFactorPojo();
        tS.setFactorCode(cFactorCode);
        tS.setFactorDefault(cFactorValue);
        tS.setFactorClass(cFactorClass.toString());
        tS.setFactorType("1");
        mCalFactors1.add(tS);
    }

    // @Method
    public void setCalCode(String tCalCode) {
        calCode = tCalCode;
    }

    /**
     * 公式计算函数
     *
     * @return: String 计算的结果，只能是单值的数据（数字型的转换成字符型）
     * @author: YT
     **/
    public String calculate() {

        if (!checkCalculate()) {
            return "0";
        }
        // 取得数据库中计算要素
        List<LMCalFactorPojo> tLMCalFactorPojo = redisCommonDao.findByIndexKeyRelaDB(LMCalFactorPojo.class, "CalCode", calCode);
        logger.info("calCode :" + calCode);

        if (tLMCalFactorPojo == null) {
            return "0";
        }

        // 增加基本要素
        for (int i = 0; i < tLMCalFactorPojo.size(); i++) {
            mCalFactors.add(tLMCalFactorPojo.get(i));

        }
        // 增加基本要素
        for (int i = 0; i < mCalFactors1.size(); i++) {
            mCalFactors.add(mCalFactors1.get(i));

        }
        //解释计算要素
        if (!interpretFactors()) {
            return "0";
        }

        //读取SQL语句
        if (!getSQL()) {
            return "0";
        }

        //解释SQL语句中的变量
        if (!interpretFactorInSQL()) {
            return "0";
        }
        //执行SQL语句
        return executeSQL();
    }

    /**
     * 执行SQL语句
     *
     * @return String
     */
    private String executeSQL() {
        String tReturn = "0";
        ExeSQL tExeSQL = new ExeSQL();
        tReturn = tExeSQL.getOneValue(mLMCalMode.getCalSQL());
        if (tExeSQL.mErrors.needDealError()) {
            // @@错误处理
            this.mErrors.copyAllErrors(tExeSQL.mErrors);
            CError tError = new CError();
            tError.moduleName = "Calculator";
            tError.functionName = "executeSQL";
            tError.errorMessage = "执行SQL语句：" + mLMCalMode.getCalCode() + "失败!" + mLMCalMode.getCalSQL();
            this.mErrors.addOneError(tError);
            return "0";
        }
        return tReturn;
    }


    /**
     *
     * @param connection connection
     * @return 返回值
     */
    public String calculate(Connection connection) {

        if (!checkCalculate()) {
            return "0";
        }
        // 取得数据库中计算要素
        List<LMCalFactorPojo> tLMCalFactorPojo = redisCommonDao.findByIndexKeyRelaDB(LMCalFactorPojo.class, "CalCode", calCode);
        logger.info("calCode :" + calCode);

        if (tLMCalFactorPojo == null) {
            return "0";
        }

        // 增加基本要素
        for (int i = 0; i < tLMCalFactorPojo.size(); i++) {
            mCalFactors.add(tLMCalFactorPojo.get(i));

        }
        // 增加基本要素
        for (int i = 0; i < mCalFactors1.size(); i++) {
            mCalFactors.add(mCalFactors1.get(i));

        }
        //解释计算要素
        if (!interpretFactors()) {
            return "0";
        }

        //读取SQL语句
        if (!getSQL()) {
            return "0";
        }

        //解释SQL语句中的变量
        if (!interpretFactorInSQL()) {
            return "0";
        }
        //执行SQL语句
        return executeSQL(connection);
    }

    /**
     * 执行SQL语句
     *
     * @return String
     */
    private String executeSQL(Connection connection) {
        String tReturn = "0";
        Date startDate = new Date();
        logger.info("executeSQL(Connection connection)执行sql开始startDate="+startDate);
        ExeSQL tExeSQL = new ExeSQL(connection);
        tReturn = tExeSQL.getOneValue(mLMCalMode.getCalSQL());
        Date endDate = new Date();
        logger.info("executeSQL(Connection connection)执行sql结束endDate="+endDate);
        logger.info("executeSQL(Connection connection)执行sql花费的时间="+(endDate.getTime()-startDate.getTime())+"毫秒");

        if (tExeSQL.mErrors.needDealError()) {
            // @@错误处理
            this.mErrors.copyAllErrors(tExeSQL.mErrors);
            CError tError = new CError();
            tError.moduleName = "Calculator";
            tError.functionName = "executeSQL";
            tError.errorMessage = "执行SQL语句：" + mLMCalMode.getCalCode() + "失败!" + mLMCalMode.getCalSQL();
            this.mErrors.addOneError(tError);
            return "0";
        }
        return tReturn;
    }

    /**
     * 解释SQL语句中的变量
     *
     * @return boolean
     */
    private boolean interpretFactorInSQL() {
        String tSql, tStr = "", tStr1 = "";
        tSql = mLMCalMode.getCalSQL();
        logger.info(tSql);
//    tSql=tSql.toLowerCase() ;
        try {
            while (true) {
                tStr = PubFun.getStr(tSql, 2, "?");
                if (tStr.equals("")) {
                    break;
                }
                tStr1 = "?" + tStr.trim() + "?";
                //替换变量
                tSql = StrTool.replaceEx(tSql, tStr1, getValueByName(tStr));
//        tSql=tSql.replace(tStr,getValueByName(tStr));
            }
        } catch (Exception ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "Calculator";
            tError.functionName = "interpretFactorInSQL";
            tError.errorMessage = "解释" + tSql + "的变量:" + tStr + "时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        logger.info(tSql);
        mLMCalMode.setCalSQL(tSql);
        return true;
    }

    /**
     * 计算子要素的值
     *
     * @param cF LMCalFactorSchema
     * @return String
     */
    private String calSubFactors(LMCalFactorPojo cF) {
        int i, iMax;
        String tReturn = "";
        LMCalFactorPojo tC = new LMCalFactorPojo();
        Calculator tNewC = SpringContextUtils.getBeanByClass(Calculator.class);
        iMax = mCalFactors.size();
        for (i = 0; i < iMax; i++) {
            tC = mCalFactors.get(i);
            //如果是基本要素或常量要素，则传入下一个要素中
            if (tC.getFactorType().toUpperCase().equals("1") ||
                    tC.getFactorType().toUpperCase().equals("3")) {
                tNewC.mCalFactors.add(tC);
            }
        }
        tNewC.setCalCode(cF.getFactorCalCode());
        logger.info("----SubFactor---calcode = " + cF.getFactorCalCode());
        tReturn = String.valueOf(tNewC.calculate());
        logger.info("----SubFactor = " + tReturn);
        //如果有错误，则将错误拷贝到上一要素,并且返回"0"
        if (tNewC.mErrors.needDealError()) {
            this.mErrors.copyAllErrors(tNewC.mErrors);
            tReturn = "0";
        }
        return tReturn;
    }

    /**
     * 读取SQL语句
     *
     * @return boolean
     */
    private boolean getSQL() {
        mLMCalMode = redisCommonDao.getEntityRelaDB(LMCalModePojo.class, calCode);

        if (mLMCalMode == null) {
            CError tError = new CError();
            tError.moduleName = "Calculator";
            tError.functionName = "getSql";
            tError.errorMessage = "得到" + calCode + "的SQL语句时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }


        return true;
    }

    /**
     * 解释要素连表中的非变量要素
     *
     * @return boolean
     */
    private boolean interpretFactors() {
        int i, iMax;
        LMCalFactorPojo tC = new LMCalFactorPojo();
        iMax = mCalFactors.size();
        for (i = 0; i < iMax; i++) {
            tC = mCalFactors.get(i);
            //如果是扩展要素，则解释该扩展要素
            if (tC.getFactorType().toUpperCase().equals("2")) {
                tC.setFactorDefault(calSubFactors(tC));
                //如果在计算子要素的时候发生错误，则返回false
                if (this.mErrors.needDealError()) {
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * 校验计算的输入是否足够
     *
     * @return boolean 如果不正确返回false
     */
    private boolean checkCalculate() {
        if (calCode == null || calCode.equals("")) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "Calculator";
            tError.functionName = "checkCalculate";
            tError.errorMessage = "计算时必须有计算编码。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    /**
     * 根据变量名得到变量的值
     *
     * @param cVarName String
     * @return String 如果不正确返回"",否则返回变量值
     */
    private String getValueByName(String cVarName) {
        cVarName = cVarName.toLowerCase();
        int i, iMax;
        String tReturn = "";
        LMCalFactorPojo tC = new LMCalFactorPojo();
        iMax = mCalFactors.size();
        for (i = 0; i < iMax; i++) {
            tC = mCalFactors.get(i);
            if (tC.getFactorCode().toLowerCase().equals(cVarName)) {
                tReturn = tC.getFactorDefault();
                break;
            }
        }
        return tReturn;
    }

    /**
     * Kevin 2003-08-20
     * 得到解析过的SQL语句，而不是SQL语句执行后的值。
     *
     * @return String
     */
    public String getCalSQL() {
        if (!checkCalculate()) {
            return "0";
        }

        mCalFactors = redisCommonDao.findByIndexKeyRelaDB(LMCalFactorPojo.class, "CalCode", calCode);
        logger.info("calCode :" + calCode);

        if (mCalFactors == null) {
            return "0";
        }

        // 增加基本要素
        for (int i = 0; i < mCalFactors1.size(); i++) {
            mCalFactors.add(mCalFactors1.get(i));

        }
        //解释计算要素
        if (!interpretFactors()) {
            return "0";
        }

        //读取SQL语句
        if (!getSQL()) {
            return "0";
        }

        //解释SQL语句中的变量
        if (!interpretFactorInSQL()) {
            return "0";
        }

        // 返回解析过的SQL语句
        return mLMCalMode.getCalSQL();
    }


}
