/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */
package com.sinosoft.cloud.common;

import com.sinosoft.utility.TransferData;

/**
 * <p>
 * ClassName: CalBase
 * </p>
 * <p>
 * Description: 计算基础要素类文件
 * </p>
 * <p>
 * Description: 所有方法均是为内部成员变量存取值，set开头为存，get开头为取
 * </p>
 * <p>
 * Copyright: Copyright (c) 2002
 * </p>
 * <p>
 * Company: sinosoft
 * </p>
 *
 * @Database: PJ
 * @CreateDate：2019-09-11
 */
public class CalBase {

    /**
     * 续保标记（再次投保标记）
     */
    private String ReNewalFlag;
    // @Field
    /**
     * 保费
     */
    private double Prem;

    /**
     * 保额
     */
    private double Get;

    /**
     * 份数
     */
    private double Mult;

    /**
     * 缴费间隔
     */
    private int PayIntv;

    /**
     * 领取间隔
     */
    private int GetIntv;

    /**
     * 缴费终止年期或年龄
     */
    private int PayEndYear;

    /**
     * 缴费终止年期或年龄标记
     */
    private String PayEndYearFlag;

    /**
     * 领取开始年期或年龄
     */
    private int GetYear;

    /**
     * 领取开始年期或年龄标记
     */
    private String GetYearFlag;

    /**
     * 责任领取类型
     */
    private String GetDutyKind;

    /**
     * 起领日期计算参照
     */
    private String StartDateCalRef;

    /**
     * 保险期间
     */
    private int Years;

    /**
     * 保险期间
     */
    private int InsuYear;

    /**
     * 保险期间标记
     */
    private String InsuYearFlag;

    /**
     * 被保人投保年龄
     */
    private int AppAge;

    /**
     * 被保人性别
     */
    private String Sex;

    /**
     * 被保人工种
     */
    private String Job;

    /**
     * 责任给付编码
     */
    private String GDuty;

    /**
     * 投保人数
     */
    private int Count;

    /**
     * 续保次数
     */
    private int RnewFlag;

    /**
     * 递增率
     */
    private double AddRate;

    /**
     * 个单合同号
     */
    private String ContNo;

    /**
     * 保单号
     */
    private String PolNo;

    /**
     * 原保额
     */
    private double Amnt;

    /**
     * 浮动费率
     */
    private double FloatRate;

    /**
     * 险种编码
     */
    private String RiskCode;

    /**
     * 个人保单表中用来做传递计算变量的公用元素--团体商业补充保险中传递责任选择的组合代码
     */
    private String StandbyFlag1;

    /**
     * 待用
     */
    private String StandbyFlag2;

    /**
     * 团体商业补充保险中传递在职或退休字段
     */
    private String StandbyFlag3;

    /**
     * 集体合同号
     */
    private String GrpContNo;

    /**
     * 集体保单号
     */
    private String GrpPolNo;

    private String EdorNo;

    /**
     * 计算类型
     */
    private String CalType;

    /**
     * 起付线*
     */
    private double GetLimit;

    /**
     * 赔付比例*
     */
    private double GetRate;

    /**
     * 社保标记*
     */
    private String SSFlag;

    /**
     * 封顶线*
     */
    private double PeakLine;

    /**
     * 保单生效日*
     */
    private String CValiDate;

    /**
     * 额外风险评分*
     */
    private double SuppRiskScore;

    /**
     * 第一被保险人加费评点 *
     */
    private double FirstScore;

    /**
     * 第二被保险人加费评点 *
     */
    private double SecondScore;

    /**
     * 责任编码*
     */
    private String DutyCode;

    /**
     * 单位保额*
     */
    private String VPU;

    /**
     * 连带被保人号*
     */
    private String SecondInsuredNo;

    /**
     * 被保人号*
     */
    private String InsuredNo;

    /**
     * 主险保单号*
     */
    private String MainPolNo;

    /**
     * 第二被保险人年龄
     */
    private String AppAg2;

    /**
     * 针对险种144丈夫的加费评点
     */
    private double HusbandScore;

    /**
     * 针对险种144妻子的加费评点
     */
    private double WifeScore;

    /**
     * 销售渠道
     */
    private String SaleChnl;

    /**
     * 销售方式
     */
    private String SellType;

    /**
     * 首期交费方式
     */
    private String PayMode;

    /**
     * 整单各期保费之和
     */
    private String TotalPrem;

    /**
     * 产品组合代码
     */
    private String ProdsetCode;


    public String getPayMode() {
        return PayMode;
    }

    public void setPayMode(String payMode) {
        PayMode = payMode;
    }

    public String getTotalPrem() {
        return TotalPrem;
    }

    public void setTotalPrem(String totalPrem) {
        TotalPrem = totalPrem;
    }

    public String getSaleChnl() {
        return SaleChnl;
    }

    public void setSaleChnl(String saleChnl) {
        SaleChnl = saleChnl;
    }

    public String getSellType() {
        return SellType;
    }

    public void setSellType(String sellType) {
        SellType = sellType;
    }

    /**
     * 自核使用的变量其中mainpolno and job have already definine before*
     */
    private String LSumDangerAmnt; // 同一被保险人下寿险类的累计危险保额

    private String DSumDangerAmnt; // 同一被保险人下重大疾病类的累计危险保额

    private String ASumDangerAmnt; // 同一被保险人下人身意外伤害类的累计危险保额

    private String MSumDangerAmnt; // 同一被保险人下人身意外医疗类的累计危险保额

//	private String SSumDangerAmnt; // 同一被保险人下人身意外医疗类的累计危险保额

    private String ManageCom; // 管理机构代码

    private String AppntJob; // 投保人职业类别

    private String AppntNo; // 投保人客户号

    private String BonusGetMode; // 红利领取方式

    private String MainRiskGet; // 主险保额

    private String RiskSort; // 险种类别

    private String CustomerNo; // 客户号码

    private String Occupation; // 被保险人职业

    private String MAmnt; // 主险保额

    private String AppAge2; // 连生险的第2个被保险人的投保时的年龄。

    private double MainPrem; // 组合险时赋值为主险的保费。

    private String MainPayEndDate; //组合险时主险的PayEndDate。

    private String ProdFlag; //组合险时组合险标记

    // HXYW200910231417 2009-10-23 zhangdx Insert Start
    private String PrtNo; //印刷号码
    // HXYW200910231417 2009-10-23 zhangdx Insert End

    private String CombiFlag;

    //2019-08-29 新增属性

    private String AppntSex;
    private int AppntAge;


    private String SSumDieAmnt;
    private String AppntLSumDangerAmnt;
    private String AppntDSumDangerAmnt;
    private String AppntMSumDangerAmnt;
    private String AppntSSumDangerAmnt;
    private String AppntSSumDieAmnt;
    private String AppntAllSumAmnt;
    private String AllSumAmnt;
    private String mLFSumDangerAmnt1;
    private String mLFSumDangerAmnt2;
    private String mLFSumDangerAmnt4;
    private String mLFSumDangerAmnt12;
    private String mLFSumDangerAmnt41;
    private String mLFSumDangerAmnt42;
    private String mLFSumDangerAmntBZ1;
    private String mLFSumDangerAmntBZ2;
    private String mLFSumDangerAmntBZ4;
    private String mLFSumDangerAmntBZ12;
    private String mLFSumDangerAmntBZ41;
    private String mLFSumDangerAmntBZ42;
    private String mLFSumDangerAmntCX1;
    private String mLFSumDangerAmntCX2;
    private String mLFSumDangerAmntCX4;
    private String mLFSumDangerAmntCX12;
    private String mLFSumDangerAmntCX41;
    private String mLFSumDangerAmntCX42;
    private String mFlag1;
    private String mFlag2;
    private String mFlag4;
    private String mFlag12;

    private String InsuredBirthday;
    private String AppntPreferredPhone;
    private String AppntPreferredAddress;

    private String AppntName;
    private String InsuredName;
    private String BnfType;

    private String InsuredSex;

    private String AgentCode;
    private String PolApplyDate;
    private String OccupationCode;

    private String AppntIdtype;
    private String AppntIdno;
    private String AppntBirthday;
    private String InsuredIdtype;
    private String InsuredIdno;
    private String ImpartAmnt;//告知人身险保额

    private String LoanContNo;
    private String A2IRelationCode;
    private String AllYearPrem;
    private String A2IRelationName; // 投保人与被保人关系名称
    private String INativeplace; // 被保人国籍
    /** 与被保人关系 */
    private String RelationToInsured;
    /** 投保人职业类别 */
    private String OccupationType;

    /** 身高 */
    private double Stature;
    /** 体重 */
    private double Avoirdupois;
    /**投保人地址*/
    private String AppntAddress;
    /**受益人姓名*/
    private String BnfName;
    /**受益人与被保人关系*/
    private String BnfRelationToInsured;
    /**受益人性别*/
    private String BeneficiarySex;
    /**受益人生日*/
    private String BnfBirthday;
    /**受益人证件类型*/
    private String BnfIDType;
    /**受益人证件号*/
    private String BnfIDNo;
    /**累计保费*/
    private double SumPrem;
    /**投保人证件有效期*/
    private String AppntIDexpDate;
    /**被保人证件有效期*/
    private String InsureIDexpDate;
    /**投保人手机号*/
    private String AppMobile;
    /**被保人地址*/
    private String InsureAddress;
    /**被保人手机号*/
    private String InsureMobile;
    /**投保人家庭电话*/
    private String AppHomePhone;
    /**被保人家庭电话*/
    private String InsureHomePhone;
    /**被保人工资*/
    private  double InsuredSalary;
    /**被保人职业代码*/
    private String InsuredOccupationCode;

    private String InsureCity;

    private String InsureProvince;

    private String InsureSSFlag;

    private String AppntNativePlace;

    private String InsureNativePlace;
    private String YWyiliao;
    private String YWshengu;
    private String YWjingtie;


    private String Fxml_1;
    private String Fxml_2;
    private String Fxml_4;
    private String Fxml_12;
    private String Fxml_41;
    private String Fxml_43;

    private String SumContFxml_1;
    private String SumContFxml_2;
    private String SumContFxml_4;
    private String SumContFxml_12;
    private String SumContFxml_41;
    private String SumContFxml_43;

    private String TUWFlag;
    private String TAPPFlag;

    private String ContPlanCode;

    private int PayYears;
    private String ImpartVer;
    private String HaveJiaShi;
    private String NotHaveJiaShi;
    private String ImpartCode;
    private String Impartparammodle;
    private String AppntProvince;
    private String AppntCity;
    private String DutyCodeString;


    public String getDutyCodeString() {
        return DutyCodeString;
    }

    public void setDutyCodeString(String dutyCodeString) {
        DutyCodeString = dutyCodeString;
    }

    public String getAppntProvince() {
        return AppntProvince;
    }

    public void setAppntProvince(String appntProvince) {
        AppntProvince = appntProvince;
    }

    public String getAppntCity() {
        return AppntCity;
    }

    public void setAppntCity(String appntCity) {
        AppntCity = appntCity;
    }

    public String getImpartCode() {
        return ImpartCode;
    }

    public void setImpartCode(String impartCode) {
        ImpartCode = impartCode;
    }

    public String getImpartparammodle() {
        return Impartparammodle;
    }

    public void setImpartparammodle(String impartparammodle) {
        Impartparammodle = impartparammodle;
    }

    public String getHaveJiaShi() {
        return HaveJiaShi;
    }

    public void setHaveJiaShi(String haveJiaShi) {
        HaveJiaShi = haveJiaShi;
    }

    public String getNotHaveJiaShi() {
        return NotHaveJiaShi;
    }

    public String getBnfType() {
        return BnfType;
    }

    public void setBnfType(String bnfType) {
        BnfType = bnfType;
    }

    public void setNotHaveJiaShi(String notHaveJiaShi) {
        NotHaveJiaShi = notHaveJiaShi;
    }

    public String getImpartVer() {
        return ImpartVer;
    }

    public void setImpartVer(String impartVer) {
        ImpartVer = impartVer;
    }

    public int getPayYears() {
        return PayYears;
    }

    public void setPayYears(int payYears) {
        PayYears = payYears;
    }

    public String getYWyiliao() {
        return YWyiliao;
    }

    public String getOccupationCode() {
        return OccupationCode;
    }

    public void setOccupationCode(String occupationCode) {
        OccupationCode = occupationCode;
    }

    public void setYWyiliao(String YWyiliao) {
        this.YWyiliao = YWyiliao;
    }

    public String getYWshengu() {
        return YWshengu;
    }

    public void setYWshengu(String YWshengu) {
        this.YWshengu = YWshengu;
    }

    public String getYWjingtie() {
        return YWjingtie;
    }

    public void setYWjingtie(String YWjingtie) {
        this.YWjingtie = YWjingtie;
    }

    public String getContPlanCode() {
        return ContPlanCode;
    }

    public void setContPlanCode(String contPlanCode) {
        ContPlanCode = contPlanCode;
    }

    public String getInsureProvince() {
        return InsureProvince;
    }

    public void setInsureProvince(String insureProvince) {
        InsureProvince = insureProvince;
    }

    public String getTUWFlag() {
        return TUWFlag;
    }

    public void setTUWFlag(String TUWFlag) {
        this.TUWFlag = TUWFlag;
    }

    public String getTAPPFlag() {
        return TAPPFlag;
    }

    public void setTAPPFlag(String TAPPFlag) {
        this.TAPPFlag = TAPPFlag;
    }

    public String getSumContFxml_1() {
        return SumContFxml_1;
    }

    public void setSumContFxml_1(String sumContFxml_1) {
        SumContFxml_1 = sumContFxml_1;
    }

    public String getSumContFxml_2() {
        return SumContFxml_2;
    }

    public void setSumContFxml_2(String sumContFxml_2) {
        SumContFxml_2 = sumContFxml_2;
    }

    public String getSumContFxml_4() {
        return SumContFxml_4;
    }

    public void setSumContFxml_4(String sumContFxml_4) {
        SumContFxml_4 = sumContFxml_4;
    }

    public String getSumContFxml_12() {
        return SumContFxml_12;
    }

    public void setSumContFxml_12(String sumContFxml_12) {
        SumContFxml_12 = sumContFxml_12;
    }

    public String getSumContFxml_41() {
        return SumContFxml_41;
    }

    public void setSumContFxml_41(String sumContFxml_41) {
        SumContFxml_41 = sumContFxml_41;
    }

    public String getSumContFxml_43() {
        return SumContFxml_43;
    }

    public void setSumContFxml_43(String sumContFxml_43) {
        SumContFxml_43 = sumContFxml_43;
    }

    public String getFxml_1() {
        return Fxml_1;
    }

    public void setFxml_1(String fxml_1) {
        Fxml_1 = fxml_1;
    }

    public String getFxml_2() {
        return Fxml_2;
    }

    public void setFxml_2(String fxml_2) {
        Fxml_2 = fxml_2;
    }

    public String getFxml_4() {
        return Fxml_4;
    }

    public void setFxml_4(String fxml_4) {
        Fxml_4 = fxml_4;
    }

    public String getFxml_12() {
        return Fxml_12;
    }

    public void setFxml_12(String fxml_12) {
        Fxml_12 = fxml_12;
    }

    public String getFxml_41() {
        return Fxml_41;
    }

    public void setFxml_41(String fxml_41) {
        Fxml_41 = fxml_41;
    }

    public String getFxml_43() {
        return Fxml_43;
    }

    public void setFxml_43(String fxml_43) {
        Fxml_43 = fxml_43;
    }

    public String getAppntNativePlace() {
        return AppntNativePlace;
    }

    public void setAppntNativePlace(String appntNativePlace) {
        AppntNativePlace = appntNativePlace;
    }

    public String getInsureNativePlace() {
        return InsureNativePlace;
    }

    public void setInsureNativePlace(String insureNativePlace) {
        InsureNativePlace = insureNativePlace;
    }

    public String getInsureSSFlag() {
        return InsureSSFlag;
    }

    public void setInsureSSFlag(String insureSSFlag) {
        InsureSSFlag = insureSSFlag;
    }

    public String getInsureCity() {
        return InsureCity;
    }

    public void setInsureCity(String insureCity) {
        InsureCity = insureCity;
    }

    public String getInsuredOccupationCode() {
        return InsuredOccupationCode;
    }

    public void setInsuredOccupationCode(String insuredOccupationCode) {
        InsuredOccupationCode = insuredOccupationCode;
    }

    public double getInsuredSalary() {
        return InsuredSalary;
    }

    public void setInsuredSalary(double insuredSalary) {
        InsuredSalary = insuredSalary;
    }

    public String getAppHomePhone() {
        return AppHomePhone;
    }

    public void setAppHomePhone(String appHomePhone) {
        AppHomePhone = appHomePhone;
    }

    public String getInsureHomePhone() {
        return InsureHomePhone;
    }

    public void setInsureHomePhone(String insureHomePhone) {
        InsureHomePhone = insureHomePhone;
    }

    public String getInsureAddress() {
        return InsureAddress;
    }

    public void setInsureAddress(String insureAddress) {
        InsureAddress = insureAddress;
    }

    public String getInsureMobile() {
        return InsureMobile;
    }

    public void setInsureMobile(String insureMobile) {
        InsureMobile = insureMobile;
    }

    public String getAppMobile() {
        return AppMobile;
    }

    public void setAppMobile(String appMobile) {
        AppMobile = appMobile;
    }

    public String getInsureIDexpDate() {
        return InsureIDexpDate;
    }

    public void setInsureIDexpDate(String insureIDexpDate) {
        InsureIDexpDate = insureIDexpDate;
    }

    public double getSumPrem() {
        return SumPrem;
    }

    public void setSumPrem(double sumPrem) {
        SumPrem = sumPrem;
    }

    public String getAppntIDexpDate() {
        return AppntIDexpDate;
    }

    public void setAppntIDexpDate(String appntIDexpDate) {
        AppntIDexpDate = appntIDexpDate;
    }

    public String getBnfIDType() {
        return BnfIDType;
    }

    public void setBnfIDType(String bnfIDType) {
        BnfIDType = bnfIDType;
    }

    public String getBnfIDNo() {
        return BnfIDNo;
    }

    public void setBnfIDNo(String bnfIDNo) {
        BnfIDNo = bnfIDNo;
    }

    public String getBnfBirthday() {
        return BnfBirthday;
    }

    public void setBnfBirthday(String bnfBirthday) {
        BnfBirthday = bnfBirthday;
    }

    public String getBeneficiarySex() {
        return BeneficiarySex;
    }

    public void setBeneficiarySex(String beneficiarySex) {
        BeneficiarySex = beneficiarySex;
    }

    public String getBnfRelationToInsured() {
        return BnfRelationToInsured;
    }

    public void setBnfRelationToInsured(String bnfRelationToInsured) {
        BnfRelationToInsured = bnfRelationToInsured;
    }

    public String getBnfName() {
        return BnfName;
    }

    public void setBnfName(String bnfName) {
        BnfName = bnfName;
    }

    public String getAppntAddress() {
        return AppntAddress;
    }

    public void setAppntAddress(String appntAddress) {
        AppntAddress = appntAddress;
    }

    public double getStature() {
        return Stature;
    }

    public void setStature(double stature) {
        Stature = stature;
    }

    public double getAvoirdupois() {
        return Avoirdupois;
    }

    public void setAvoirdupois(double avoirdupois) {
        Avoirdupois = avoirdupois;
    }

    public String getOccupationType() {
        return OccupationType;
    }

    public void setOccupationType(String occupationType) {
        OccupationType = occupationType;
    }

    public String getRelationToInsured() {
        return RelationToInsured;
    }

    public void setRelationToInsured(String relationToInsured) {
        RelationToInsured = relationToInsured;
    }

    public String getA2IRelationName() {
        return A2IRelationName;
    }

    public void setA2IRelationName(String a2IRelationName) {
        A2IRelationName = a2IRelationName;
    }

    public String getINativeplace() {
        return INativeplace;
    }

    public void setINativeplace(String INativeplace) {
        this.INativeplace = INativeplace;
    }

    public String getAllSumAmnt() {
        return AllSumAmnt;
    }

    public void setAllSumAmnt(String allSumAmnt) {
        AllSumAmnt = allSumAmnt;
    }

    public String getAllYearPrem() {
        return AllYearPrem;
    }

    public void setAllYearPrem(String allYearPrem) {
        AllYearPrem = allYearPrem;
    }

    public String getLoanContNo() {
        return LoanContNo;
    }

    public void setLoanContNo(String loanContNo) {
        LoanContNo = loanContNo;
    }

    public String getA2IRelationCode() {
        return A2IRelationCode;
    }

    public void setA2IRelationCode(String a2IRelationCode) {
        A2IRelationCode = a2IRelationCode;
    }

    public String getInsuredPreferredPhone() {
        return InsuredPreferredPhone;
    }

    public void setInsuredPreferredPhone(String insuredPreferredPhone) {
        InsuredPreferredPhone = insuredPreferredPhone;
    }

    public String getInsuredPreferredAddress() {
        return InsuredPreferredAddress;
    }

    public void setInsuredPreferredAddress(String insuredPreferredAddress) {
        InsuredPreferredAddress = insuredPreferredAddress;
    }

    public String getANativePlace() {
        return ANativePlace;
    }

    public void setANativePlace(String ANativePlace) {
        this.ANativePlace = ANativePlace;
    }

    public String getAgentGroup() {
        return AgentGroup;
    }

    public void setAgentGroup(String agentGroup) {
        AgentGroup = agentGroup;
    }

    public String getAppntYearSumPrem() {
        return AppntYearSumPrem;
    }

    public void setAppntYearSumPrem(String appntYearSumPrem) {
        AppntYearSumPrem = appntYearSumPrem;
    }

    private String InsuredPreferredPhone;
    private String InsuredPreferredAddress;
    private String ANativePlace;
    private String AgentGroup;
    private String AppntYearSumPrem;



    public String getImpartAmnt() {
        return ImpartAmnt;
    }

    public void setImpartAmnt(String impartAmnt) {
        ImpartAmnt = impartAmnt;
    }

    public String getAppntSex() {
        return AppntSex;
    }

    public void setAppntSex(String appntSex) {
        AppntSex = appntSex;
    }

    public int getAppntAge() {
        return AppntAge;
    }

    public void setAppntAge(int appntAge) {
        AppntAge = appntAge;
    }

    public String getSSumDieAmnt() {
        return SSumDieAmnt;
    }

    public void setSSumDieAmnt(String SSumDieAmnt) {
        this.SSumDieAmnt = SSumDieAmnt;
    }

    public String getAppntLSumDangerAmnt() {
        return AppntLSumDangerAmnt;
    }

    public void setAppntLSumDangerAmnt(String appntLSumDangerAmnt) {
        AppntLSumDangerAmnt = appntLSumDangerAmnt;
    }

    public String getAppntDSumDangerAmnt() {
        return AppntDSumDangerAmnt;
    }

    public void setAppntDSumDangerAmnt(String appntDSumDangerAmnt) {
        AppntDSumDangerAmnt = appntDSumDangerAmnt;
    }

    public String getAppntMSumDangerAmnt() {
        return AppntMSumDangerAmnt;
    }

    public void setAppntMSumDangerAmnt(String appntMSumDangerAmnt) {
        AppntMSumDangerAmnt = appntMSumDangerAmnt;
    }

    public String getAppntSSumDangerAmnt() {
        return AppntSSumDangerAmnt;
    }

    public void setAppntSSumDangerAmnt(String appntSSumDangerAmnt) {
        AppntSSumDangerAmnt = appntSSumDangerAmnt;
    }

    public String getAppntSSumDieAmnt() {
        return AppntSSumDieAmnt;
    }

    public void setAppntSSumDieAmnt(String appntSSumDieAmnt) {
        AppntSSumDieAmnt = appntSSumDieAmnt;
    }

    public String getAppntAllSumAmnt() {
        return AppntAllSumAmnt;
    }

    public void setAppntAllSumAmnt(String appntAllSumAmnt) {
        AppntAllSumAmnt = appntAllSumAmnt;
    }

    public String getmLFSumDangerAmnt1() {
        return mLFSumDangerAmnt1;
    }

    public void setmLFSumDangerAmnt1(String mLFSumDangerAmnt1) {
        this.mLFSumDangerAmnt1 = mLFSumDangerAmnt1;
    }

    public String getmLFSumDangerAmnt2() {
        return mLFSumDangerAmnt2;
    }

    public void setmLFSumDangerAmnt2(String mLFSumDangerAmnt2) {
        this.mLFSumDangerAmnt2 = mLFSumDangerAmnt2;
    }

    public String getmLFSumDangerAmnt4() {
        return mLFSumDangerAmnt4;
    }

    public void setmLFSumDangerAmnt4(String mLFSumDangerAmnt4) {
        this.mLFSumDangerAmnt4 = mLFSumDangerAmnt4;
    }

    public String getmLFSumDangerAmnt12() {
        return mLFSumDangerAmnt12;
    }

    public void setmLFSumDangerAmnt12(String mLFSumDangerAmnt12) {
        this.mLFSumDangerAmnt12 = mLFSumDangerAmnt12;
    }

    public String getmLFSumDangerAmnt41() {
        return mLFSumDangerAmnt41;
    }

    public void setmLFSumDangerAmnt41(String mLFSumDangerAmnt41) {
        this.mLFSumDangerAmnt41 = mLFSumDangerAmnt41;
    }

    public String getmLFSumDangerAmnt42() {
        return mLFSumDangerAmnt42;
    }

    public void setmLFSumDangerAmnt42(String mLFSumDangerAmnt42) {
        this.mLFSumDangerAmnt42 = mLFSumDangerAmnt42;
    }

    public String getmLFSumDangerAmntBZ1() {
        return mLFSumDangerAmntBZ1;
    }

    public void setmLFSumDangerAmntBZ1(String mLFSumDangerAmntBZ1) {
        this.mLFSumDangerAmntBZ1 = mLFSumDangerAmntBZ1;
    }

    public String getmLFSumDangerAmntBZ2() {
        return mLFSumDangerAmntBZ2;
    }

    public void setmLFSumDangerAmntBZ2(String mLFSumDangerAmntBZ2) {
        this.mLFSumDangerAmntBZ2 = mLFSumDangerAmntBZ2;
    }

    public String getmLFSumDangerAmntBZ4() {
        return mLFSumDangerAmntBZ4;
    }

    public void setmLFSumDangerAmntBZ4(String mLFSumDangerAmntBZ4) {
        this.mLFSumDangerAmntBZ4 = mLFSumDangerAmntBZ4;
    }

    public String getmLFSumDangerAmntBZ12() {
        return mLFSumDangerAmntBZ12;
    }

    public void setmLFSumDangerAmntBZ12(String mLFSumDangerAmntBZ12) {
        this.mLFSumDangerAmntBZ12 = mLFSumDangerAmntBZ12;
    }

    public String getmLFSumDangerAmntBZ41() {
        return mLFSumDangerAmntBZ41;
    }

    public void setmLFSumDangerAmntBZ41(String mLFSumDangerAmntBZ41) {
        this.mLFSumDangerAmntBZ41 = mLFSumDangerAmntBZ41;
    }

    public String getmLFSumDangerAmntBZ42() {
        return mLFSumDangerAmntBZ42;
    }

    public void setmLFSumDangerAmntBZ42(String mLFSumDangerAmntBZ42) {
        this.mLFSumDangerAmntBZ42 = mLFSumDangerAmntBZ42;
    }

    public String getmLFSumDangerAmntCX1() {
        return mLFSumDangerAmntCX1;
    }

    public void setmLFSumDangerAmntCX1(String mLFSumDangerAmntCX1) {
        this.mLFSumDangerAmntCX1 = mLFSumDangerAmntCX1;
    }

    public String getmLFSumDangerAmntCX2() {
        return mLFSumDangerAmntCX2;
    }

    public void setmLFSumDangerAmntCX2(String mLFSumDangerAmntCX2) {
        this.mLFSumDangerAmntCX2 = mLFSumDangerAmntCX2;
    }

    public String getmLFSumDangerAmntCX4() {
        return mLFSumDangerAmntCX4;
    }

    public void setmLFSumDangerAmntCX4(String mLFSumDangerAmntCX4) {
        this.mLFSumDangerAmntCX4 = mLFSumDangerAmntCX4;
    }

    public String getmLFSumDangerAmntCX12() {
        return mLFSumDangerAmntCX12;
    }

    public void setmLFSumDangerAmntCX12(String mLFSumDangerAmntCX12) {
        this.mLFSumDangerAmntCX12 = mLFSumDangerAmntCX12;
    }

    public String getmLFSumDangerAmntCX41() {
        return mLFSumDangerAmntCX41;
    }

    public void setmLFSumDangerAmntCX41(String mLFSumDangerAmntCX41) {
        this.mLFSumDangerAmntCX41 = mLFSumDangerAmntCX41;
    }

    public String getmLFSumDangerAmntCX42() {
        return mLFSumDangerAmntCX42;
    }

    public void setmLFSumDangerAmntCX42(String mLFSumDangerAmntCX42) {
        this.mLFSumDangerAmntCX42 = mLFSumDangerAmntCX42;
    }

    public String getmFlag1() {
        return mFlag1;
    }

    public void setmFlag1(String mFlag1) {
        this.mFlag1 = mFlag1;
    }

    public String getmFlag2() {
        return mFlag2;
    }

    public void setmFlag2(String mFlag2) {
        this.mFlag2 = mFlag2;
    }

    public String getmFlag4() {
        return mFlag4;
    }

    public void setmFlag4(String mFlag4) {
        this.mFlag4 = mFlag4;
    }

    public String getmFlag12() {
        return mFlag12;
    }

    public void setmFlag12(String mFlag12) {
        this.mFlag12 = mFlag12;
    }

    public String getInsuredBirthday() {
        return InsuredBirthday;
    }

    public void setInsuredBirthday(String insuredBirthday) {
        InsuredBirthday = insuredBirthday;
    }

    public String getAppntPreferredPhone() {
        return AppntPreferredPhone;
    }

    public void setAppntPreferredPhone(String appntPreferredPhone) {
        AppntPreferredPhone = appntPreferredPhone;
    }

    public String getAppntPreferredAddress() {
        return AppntPreferredAddress;
    }

    public void setAppntPreferredAddress(String appntPreferredAddress) {
        AppntPreferredAddress = appntPreferredAddress;
    }

    public String getAppntName() {
        return AppntName;
    }

    public void setAppntName(String appntName) {
        AppntName = appntName;
    }

    public String getInsuredName() {
        return InsuredName;
    }

    public void setInsuredName(String insuredName) {
        InsuredName = insuredName;
    }

    public String getInsuredSex() {
        return InsuredSex;
    }

    public void setInsuredSex(String insuredSex) {
        InsuredSex = insuredSex;
    }

    public String getAgentCode() {
        return AgentCode;
    }

    public void setAgentCode(String agentCode) {
        AgentCode = agentCode;
    }

    public String getPolApplyDate() {
        return PolApplyDate;
    }

    public void setPolApplyDate(String polApplyDate) {
        PolApplyDate = polApplyDate;
    }

    public String getAppntIdtype() {
        return AppntIdtype;
    }

    public void setAppntIdtype(String appntIdtype) {
        AppntIdtype = appntIdtype;
    }

    public String getAppntIdno() {
        return AppntIdno;
    }

    public void setAppntIdno(String appntIdno) {
        AppntIdno = appntIdno;
    }

    public String getAppntBirthday() {
        return AppntBirthday;
    }

    public void setAppntBirthday(String appntBirthday) {
        AppntBirthday = appntBirthday;
    }

    public String getInsuredIdtype() {
        return InsuredIdtype;
    }

    public void setInsuredIdtype(String insuredIdtype) {
        InsuredIdtype = insuredIdtype;
    }

    public String getInsuredIdno() {
        return InsuredIdno;
    }

    public void setInsuredIdno(String insuredIdno) {
        InsuredIdno = insuredIdno;
    }

    public String getCombiFlag() {
        return CombiFlag;
    }

    public void setCombiFlag(String combiFlag) {
        CombiFlag = combiFlag;
    }

    private TransferData mTransferData = new TransferData(); // 用于保存传入的其他信息

    // @Constructor
    public CalBase() {
    }

    // @Method，新增方法，用于处理传入的其他要素信息
    public void setOtherParm(String ParmName, String ParmVlaue) {
        mTransferData.setNameAndValue(ParmName, ParmVlaue);
    }

    public String getOtherParmVlaueByName(String ParmName) {
        return (String) mTransferData.getValueByName(ParmName);
    }

    public String getOtherParmName(int Index) {
        return (String) mTransferData.getValueNames().get(Index);
    }

    public int getAllOtherParmCount() {
        return mTransferData.getValueNames().size();
    }

    // @Method
    public void setBonusGetMode(String tBonusGetMode) {
        BonusGetMode = tBonusGetMode;
    }

    public String getBonusGetMode() {
        return String.valueOf(BonusGetMode);
    }

    public void setAppntNo(String tAppntNo) {
        AppntNo = tAppntNo;
    }

    public String getAppntNo() {
        return String.valueOf(AppntNo);
    }

    public void setAppAge2(String tAppAge2) {
        AppAge2 = tAppAge2;
    }

    public String getAppAge2() {
        return String.valueOf(AppAge2);
    }

    // add method for autocheck write by yaory

    public void setContNo(String tContNo) {
        ContNo = tContNo;
    }

    // add method for autocheck write by yaory
    public void setLSumDangerAmnt(String tLSumDangerAmnt) {
        LSumDangerAmnt = tLSumDangerAmnt;
    }

    public String getLSumDangerAmnt() {
        return String.valueOf(LSumDangerAmnt);
    }

    public void setSSumDangerAmnt(String tLSumDangerAmnt) {
        LSumDangerAmnt = tLSumDangerAmnt;
    }

    public String getSSumDangerAmnt() {
        return String.valueOf(LSumDangerAmnt);
    }

    public void setMAmnt(String tMAmnt) {
        MAmnt = tMAmnt;
    }

    public String getMAmnt() {
        return String.valueOf(MAmnt);
    }

    public void setDSumDangerAmnt(String tDSumDangerAmnt) {
        DSumDangerAmnt = tDSumDangerAmnt;
    }

    public String getDSumDangerAmnt() {
        return String.valueOf(DSumDangerAmnt);
    }

    public void setASumDangerAmnt(String tASumDangerAmnt) {
        ASumDangerAmnt = tASumDangerAmnt;
    }

    public String getASumDangerAmnt() {
        return String.valueOf(ASumDangerAmnt);
    }

    public void setMSumDangerAmnt(String tMSumDangerAmnt) {
        MSumDangerAmnt = tMSumDangerAmnt;
    }

    public String getMSumDangerAmnt() {
        return String.valueOf(MSumDangerAmnt);
    }

    public void setManageCom(String tManageCom) {
        ManageCom = tManageCom;
    }

    public String getManageCom() {
        return String.valueOf(ManageCom);
    }

    public void setAppntJob(String tAppntJob) {
        AppntJob = tAppntJob;
    }

    public String getAppntJob() {
        return String.valueOf(AppntJob);
    }

    public void setMainRiskGet(String tMainRiskGet) {
        MainRiskGet = tMainRiskGet;
    }

    public String getMainRiskGet() {
        return String.valueOf(MainRiskGet);
    }

    public void setRiskSort(String tRiskSort) {
        RiskSort = tRiskSort;
    }

    public String getRiskSort() {
        return String.valueOf(RiskSort);
    }

    public void setCustomerNo(String tCustomerNo) {
        CustomerNo = tCustomerNo;
    }

    public String getCustomerNo() {
        return String.valueOf(CustomerNo);
    }

    public void setOccupation(String tOccupation) {
        Occupation = tOccupation;
    }

    public String getOccupation() {
        return String.valueOf(Occupation);
    }

    // end add
    public void setGrpContNo(String tGrpContNo) {
        GrpContNo = tGrpContNo;
    }

    public void setPrem(double tPrem) {
        Prem = tPrem;
    }

    public String getPrem() {
        return String.valueOf(Prem);
    }

    public void setGet(double tGet) {
        Get = tGet;
    }

    public String getGet() {
        return String.valueOf(Get);
    }

    public void setAmnt(double tAmnt) {
        Amnt = tAmnt;
    }

    public String getAmnt() {
        return String.valueOf(Amnt);
    }

    public void setMult(double tMult) {
        Mult = tMult;
    }

    public String getMult() {
        return String.valueOf(Mult);
    }

    public void setFloatRate(double tFloatRate) {
        FloatRate = tFloatRate;
    }

    public String getFloatRate() {
        return String.valueOf(FloatRate);
    }

    public void setAddRate(double tAddRate) {
        AddRate = tAddRate;
    }

    public String getContNo() {
        return ContNo;
    }

    public String getGrpContNo() {
        return GrpContNo;
    }

    public String getAddRate() {
        return String.valueOf(AddRate);
    }

    public void setPayIntv(int tPayIntv) {
        PayIntv = tPayIntv;
    }

    public String getPayIntv() {
        return String.valueOf(PayIntv);
    }

    public void setGetIntv(int tGetIntv) {
        GetIntv = tGetIntv;
    }

    public String getGetIntv() {
        return String.valueOf(GetIntv);
    }

    public void setPayEndYear(int tPayEndYear) {
        PayEndYear = tPayEndYear;
    }

    public String getPayEndYear() {
        return String.valueOf(PayEndYear);
    }

    public void setGetYear(int tGetYear) {
        GetYear = tGetYear;
    }

    public String getGetYear() {
        return String.valueOf(GetYear);
    }

    public void setYears(int tYears) {
        Years = tYears;
    }

    public String getYears() {
        return String.valueOf(Years);
    }

    public void setInsuYear(int tInsuYear) {
        InsuYear = tInsuYear;
    }

    public String getInsuYear() {
        return String.valueOf(InsuYear);
    }

    public void setAppAge(int tAppAge) {
        AppAge = tAppAge;
    }

    public String getAppAge() {
        return String.valueOf(AppAge);
    }

    public void setCount(int tCount) {
        Count = tCount;
    }

    public String getCount() {
        return String.valueOf(Count);
    }

    public void setRnewFlag(int tRnewFlag) {
        RnewFlag = tRnewFlag;
    }

    public String getRnewFlag() {
        return String.valueOf(RnewFlag);
    }

    public void setSex(String tSex) {
        Sex = tSex;
    }

    public String getSex() {
        return Sex;
    }

    public void setInsuYearFlag(String tInsuYearFlag) {
        InsuYearFlag = tInsuYearFlag;
    }

    public String getInsuYearFlag() {
        return InsuYearFlag;
    }

    public void setPayEndYearFlag(String tPayEndYearFlag) {
        PayEndYearFlag = tPayEndYearFlag;
    }

    public String getPayEndYearFlag() {
        return PayEndYearFlag;
    }

    public void setGetYearFlag(String tGetYearFlag) {
        GetYearFlag = tGetYearFlag;
    }

    public String getGetYearFlag() {
        return GetYearFlag;
    }

    public void setGetDutyKind(String tGetDutyKind) {
        GetDutyKind = tGetDutyKind;
    }

    public String getGetDutyKind() {
        return GetDutyKind;
    }

    public void setStartDateCalRef(String tStartDateCalRef) {
        StartDateCalRef = tStartDateCalRef;
    }

    public String getStartDateCalRef() {
        return StartDateCalRef;
    }

    public void setJob(String tJob) {
        Job = tJob;
    }

    public String getJob() {
        return Job;
    }

    public void setGDuty(String tGDuty) {
        GDuty = tGDuty;
    }

    public String getGDuty() {
        return GDuty;
    }

    public void setPolNo(String tPolNo) {
        PolNo = tPolNo;
    }

    public String getPolNo() {
        return PolNo;
    }

    public void setRiskCode(String tRiskCode) {
        RiskCode = tRiskCode;
    }

    public String getRiskCode() {
        return RiskCode;
    }

    public void setStandbyFlag1(String tStandbyFlag1) {
        StandbyFlag1 = tStandbyFlag1;
    }

    public String getStandbyFlag1() {
        return StandbyFlag1;
    }

    public void setStandbyFlag2(String tStandbyFlag2) {
        StandbyFlag2 = tStandbyFlag2;
    }

    public String getStandbyFlag2() {
        return StandbyFlag2;
    }

    public void setStandbyFlag3(String tStandbyFlag3) {
        StandbyFlag3 = tStandbyFlag3;
    }

    public String getStandbyFlag3() {
        return StandbyFlag3;
    }

    public void setGrpPolNo(String tGrpPolNo) {
        GrpPolNo = tGrpPolNo;
    }

    public String getGrpPolNo() {
        return GrpPolNo;
    }

    public void setEdorNo(String tEdorNo) {
        EdorNo = tEdorNo;
    }

    public String getEdorNo() {
        return EdorNo;
    }

    public void setCalType(String tCalType) {
        CalType = tCalType;
    }

    public String getCalType() {
        return CalType;
    }

    public void setGetLimit(double tGetLimit) {
        GetLimit = tGetLimit;
    }

    public String getGetLimit() {
        return String.valueOf(GetLimit);
    }

    public void setGetRate(double tGetRate) {
        GetRate = tGetRate;
    }

    public String getGetRate() {
        return String.valueOf(GetRate);
    }

    public void setSSFlag(String tSSFlag) {
        SSFlag = tSSFlag;
    }

    public String getSSFlag() {
        return SSFlag;
    }

    public void setPeakLine(double tPeakLine) {
        PeakLine = tPeakLine;
    }

    public String getPeakLine() {
        return String.valueOf(PeakLine);
    }

    public void setCValiDate(String tCValiDate) {
        CValiDate = tCValiDate;
    }

    public String getCValiDate() {
        return CValiDate;
    }

    public void setSuppRiskScore(double tSuppRiskScore) {
        SuppRiskScore = tSuppRiskScore;
    }

    public String getSuppRiskScore() {
        return String.valueOf(SuppRiskScore);
    }

    public String getDutyCode() {
        return DutyCode;
    }

    public void setDutyCode(String tDutyCode) {
        DutyCode = tDutyCode;
    }

    public void setFirstScore(double tFirstScore) {
        FirstScore = tFirstScore;
    }

    public String getFirstScore() {

        return String.valueOf(FirstScore);
    }

    public void setSecondScore(double tSecondScore) {
        SecondScore = tSecondScore;
    }

    public String getSecondScore() {
        return String.valueOf(SecondScore);
    }

    public void setVPU(String tVPU) {
        VPU = tVPU;
    }

    public void setSecondInsuredNo(String tSecondInsuredNo) {
        SecondInsuredNo = tSecondInsuredNo;
    }

    public String getSecondInsuredNo() {
        return (SecondInsuredNo);
    }

    public void setInsuredNo(String tInsuredNo) {
        InsuredNo = tInsuredNo;
    }

    public String getInsuredNo() {
        return (InsuredNo);
    }

    public String getVPU() {
        return (VPU);
    }

    public void setMainPolNo(String tMainPolNo) {
        MainPolNo = tMainPolNo;
    }

    public String getMainPolNo() {
        return (MainPolNo);
    }

    public void setAppAg2(String tAppAg2) {
        AppAg2 = tAppAg2;
    }

    public String getAppAg2() {
        return (AppAg2);
    }

    public void setHusbandScore(double tHusbandScore) {
        HusbandScore = tHusbandScore;
    }

    public String getHusbandScore() {

        return String.valueOf(HusbandScore);
    }

    public void setWifeScore(double tWifeScore) {
        WifeScore = tWifeScore;
    }

    public String getWifeScore() {

        return String.valueOf(WifeScore);
    }

    public void setMainPrem(double tMainPrem) {
        MainPrem = tMainPrem;
    }

    public String getMainPrem() {

        return String.valueOf(MainPrem);
    }

    public void setMainPayEndDate(String tMainPayEndDate) {
        MainPayEndDate = tMainPayEndDate;
    }

    public String getMainPayEndDate() {

        return String.valueOf(MainPayEndDate);
    }

    public void setProdFlag(String tProdFlag) {
        ProdFlag = tProdFlag;
    }

    public String getProdFlag() {

        return String.valueOf(ProdFlag);
    }

    // HXYW200910231417 2009-10-23 zhangdx Insert Start
    public void setPrtNo(String tPrtNo) {
        PrtNo = tPrtNo;
    }

    public String getPrtNo() {
        return String.valueOf(PrtNo);
    }
    // HXYW200910231417 2009-10-23 zhangdx Insert End

    public String getProdsetCode() {
        return ProdsetCode;
    }

    public void setProdsetCode(String prodsetCode) {
        ProdsetCode = prodsetCode;
    }

    public String getReNewalFlag() {
        return ReNewalFlag;
    }

    public void setReNewalFlag(String reNewalFlag) {
        ReNewalFlag = reNewalFlag;
    }

    public TransferData getmTransferData() {
        return mTransferData;
    }

    public void setmTransferData(TransferData mTransferData) {
        this.mTransferData = mTransferData;
    }
}