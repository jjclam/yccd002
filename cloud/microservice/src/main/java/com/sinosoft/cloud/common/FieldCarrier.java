package com.sinosoft.cloud.common;

import static com.sun.org.apache.xalan.internal.xsltc.compiler.util.Type.Int;

/**
 * <p>
 * ClassName: CalBase
 * </p>
 * <p>
 * Description: 计算基础要素类文件
 * </p>
 * <p>
 * Description: 所有方法均是为内部成员变量存取值，set开头为存，get开头为取
 * </p>
 * <p>
 * Copyright: Copyright (c) 2002
 * </p>
 * <p>
 * Company: sinosoft
 * </p>
 *
 * @Database:
 * @CreateDate：2002-07-30
 */
public class FieldCarrier {
	// @Field
	/**投保人白名单标记*/
	private String AppntWhiteFlag;
	/**被保人白名单标记*/
	private String InsuredWhiteFlag;
	/**长期主险*/
	private String LMRiskCode;

	public String getLMRiskCode() {
		return LMRiskCode;
	}

	public void setLMRiskCode(String LMRiskCode) {
		this.LMRiskCode = LMRiskCode;
	}

	public String getAppntWhiteFlag() {
		return AppntWhiteFlag;
	}

	public void setAppntWhiteFlag(String appntWhiteFlag) {
		AppntWhiteFlag = appntWhiteFlag;
	}

	public String getInsuredWhiteFlag() {
		return InsuredWhiteFlag;
	}

	public void setInsuredWhiteFlag(String insuredWhiteFlag) {
		InsuredWhiteFlag = insuredWhiteFlag;
	}
	/*---保单信息--*/
	/**团个标记*/
	private String ContType;

	public String getContType() {
		return ContType;
	}

	public void setContType(String contType) {
		ContType = contType;
	}

	/** 主险的交费期间 */
	private int MPayEndYear;

	public int getMPayEndYear() {
		return MPayEndYear;
	}

	public void setMPayEndYear(int MPayEndYear) {
		this.MPayEndYear = MPayEndYear;
	}

	/** 保费 */
	private double Prem;

	/** 保额 */
	private double Get;

	/** 份数 */
	private double Mult;

	/** 被保人投保年龄 */
	private int AppAge;

	// 河北新需求新加字段 起始
	/** 国籍 */
	private String NativePlace;

	/** 同主被保人的关系 */
	private String RelationToMainInsured;

	// 结束

	/** 被保人性别 */
	private String Sex;

	/** 被保人出生日期 */
	private String InsuredBirthday;

	/** 被保人工种 */
	private String Job;

	/** 责任给付编码 */
	private String GDuty;

	/** 责任编码 */

	private String DutyCode;

	/** 投保人数 */
	private int Count;

	/** 续保次数 */
	private int RnewFlag;

	/** 递增率 */
	private double AddRate;

	/** 合同号 */
	private String ContNo;

	/** 保单号 */
	private String PolNo;

	/** 原保额 */
	private double Amnt;

	/** 浮动费率 */
	private double FloatRate;

	/** 险种编码 */
	private String RiskCode;

	/** 主险号码 */
	private String MainPolNo;

	/** 被保险人职业类别 */
	private String OccupationType;

	/** 补充字段1 */
	private String StandbyFlag1;

	/** 补充字段2 */
	private String StandbyFlag2;

	/** 补充字段3 */
	private String StandbyFlag3;

	/** 补充字段4 */
	private String StandbyFlag4;

	/** 终止日期 */
	private String EndDate;

	/*--责任信息--*/

	/** 缴费间隔 */
	private int PayIntv;

	/** 领取间隔 */
	private int GetIntv;

	/** 养老金领取方式 */
	private String GetMode;

	/** 缴费终止年期或年龄 */
	private int PayEndYear;

	/** 缴费终止年期或年龄标记 */
	private String PayEndYearFlag;

	/** 缴费终止日期 */
	private String PayEndDate;

	/** 领取开始年期或年龄 */
	private int GetYear;

	/** 领取开始年期或年龄标记 */
	private String GetYearFlag;

	/** 责任领取类型 */
	private String GetDutyKind;

	/** 起领日期计算参照 */
	private String StartDateCalRef;

	/** 保险期间 */
	private int Years;

	/** 保险期间 */
	private int InsuYear;

	/** 保险期间标记 */
	private String InsuYearFlag;

	/** 交费年期 */
	private int PayYears;

	/* 保单类型 */
	private String PolTypeFlag;

	/* 管理费比例 */
	private double ManageFeeRate;
	/*---保全信息--*/

	/** 集体保单号 */
	private String GrpPolNo;

	/** 时间间隔 */
	private int Interval;

	/** 保全申请号 */
	private String EdorNo;

	/** 保全类型 */
	private String EdorType;

	/** 批改生效日期 */
	private String EdorValiDate;

	/** 交退费金额 */
	private double GetMoney;

	/** 领取开始年龄年期标志 */
	private String GetStartFlag;

	/** 起领日期 */
	private String GetStartDate;

	/** 起保日期 */
	private String CValiDate;

	/** 帐户转年金金额 */
	private double GetBalance;

	/*--被保人信息--*/

	/** 姓名 */
	private String InsuredName;

	/** 证件号码 */
	private String IDNo;

	/** 证件类型 */
	private String IDType;

	/**摩托车驾驶证 被保人校验测试 20160121 WJL*/
	private String HaveMotorcycleLicence;

	private  String RiskPeriod;

	//主险保费
	private  double MPrem;


	public String getHaveMotorcycleLicence() {
		return HaveMotorcycleLicence;
	}

	public void setHaveMotorcycleLicence(String haveMotorcycleLicence) {
		HaveMotorcycleLicence = haveMotorcycleLicence;
	}

	/** 职业工种 */
	private String WorkType;

	/** 单位名称 */
	private String GrpName;
	/** 被保人号 */
	private String InsuredNo;

	/** 起付标准 */
	private double GetLimit;

	/** 被保人职业代码 */
	private String OccupationCode;

	/** 被保人职业代码 */
	private String ManageCom;

	/** 产品组合代码 */
	private String ContPlanCode;
	/** IPA产品组合代码 */
	private String CombiFlag;
	private String BonusGetMode;
	private String BonusMan;
	/*** 社保标记 **/
	private String SSFlag;
	/* 组合产品编码 */
	private String ProdsetCode;
	/* 销售渠道 */
	private String SaleChnl;

	/* 受理日期 */
	private String VerifyApplyDate;

	/** 银代险的银行代码 */
	private String BankCode; // add by lvzg

	/** 中介机构代码 */
	private String AgentCom;

	/** 代理人代码 */
	private String AgentCode;

	/** 万能险基础保费 */
	private String WTPrem;

	/** 万能额外保费 */
	private String ExtPrem;

	/** 追加保费 */
	private String AppendPrem;

	/** 投保人客户号 */
	private String AppntNo;

	/**
	 * 销售方式 selltype 3301
	 */
	private String SellType;

	/** 投保人同被保人关系 */
	private String AppRelatToInsu;

	/** 投保人证件类型 */
	private String AppIDNo;

	/** 投保人生日 */
	private String AppntBirthday;

	/** 投保人性别 */
	private String AppntSex;

	/** 投保人姓名 */
	private String AppntName;

	/** 银代/中介人员代码 */
	private String OutAgentCode;

	/** 银代/柜员姓名 */
	private String OutAgentName;
	/** 学历 */
	private String Degree;
	/** 婚姻状况 */
	private String Marriage;
	/** 证件有效日期 */
	private String IdValiEndDate;
	/** 投保日期 */
	private String PolApplyDate;
	/** 通讯地址-->省 */
	private String Province;
	/** 通讯地址-->邮政编码 */
	private String ZipCode;
	/** 通讯地址-->邮寄地址 */
	private String AppntPostalAddress;
	/** 常住地址-->省 */
	private String AppntHomeProvince;
	/** 常住地址-->邮政编码*/
	private String AppntHomeZipCode;
	/** 市 */
	private String City;
	/** 区 */
	private String District;
	/** 手机号 */
	private String Mobile;
	/** 固定电话 */
	private String HomePhone;
	/** 代理人姓名 */
	private String AgentName;
	/** 银行代码 */
	private String AccBankCode;
	/** 与被保人关系 */
	private String RelaToInsured;
	/** 受益人类型 */
	private String BnfType;
	/** 受益人姓名 */
	private String LCBnfName;
	/** 受益人性别 */
	private String LCBnfSex;
	/** 受益人生日 */
	private String LCBnfBirthday;
	/** 受益人证件号码 */
	private String LCBnfIDNo;
	/** 受益人电话 */
	private String LCBnfPhone;
	/** 险种名称 */
	private String RiskName;
	/** 首期交费方式 */
	private String NewPayMode;
	/** NewBankAccNo*/
	private String NewBankAccNo;
	/** PayLocation*/
	private String PayLocation;
	/** 续期银行账号*/
	private String BankAccNo;
	/** 投保人证件类型*/
	private String AppntIDType;
	/** 被保人证件类型*/
	private String InsuredIDType;
	/** 外包标记 **/
	private String BPOFlag;
	/** 财富渠道客户类型 **/
	private String BankCustomerType;
	/** 财富渠道出单方式 **/
	private String YBTModeFlag;
	/**城镇 农村居民类型 **/
	private String AppRgtType;

	/**常驻地址楼号 **/
	private String HomeStoreNo;
	/**t通讯地址楼号 **/
	private String PostalStoreNo;
	/**电话区号 **/
	private String ZoneCode;
	/**保费预算 **/
	private String Budget;

	/**投保单号 **/
	private String ProposalContNo;
	/**单证类型 **/
	private String SubType;
	/**
	 * 常住地址
	 */
	private String HomeAddress;

	private String TinFlag;

	private String TinNo;

	private String PrtNo;

	private String InsuredSex;

	// 被保人工作类型 -->
	private String InsuJobType;
	// 被保人工作编码 -->
	private String InsuJobCode;
	// 投保人投保时年龄 -->
	private String AppAge2;
	// 投保人工作类型 -->
	private String AppntJobType;
	// 投保人工作编码 -->
	private String AppntJobCode;
	// 首期交费方式 -->
	private String PayMode;
	// 第二被保人 -->
	private String SecondInsuredNo;
	// 投保人工作类型 -->
	private String AppntJob;

	//投保人年收入
	private String AppntIncome;


	private String BankAgentName;
	private String AccName;
	private String BankAccno;
	private String NewBankAccno;
	private String AccName1;
	private String BankAccno1;
	private String NewBankAccno1;
	private String NewAccName;
	private String NewAccName1;
	private String Apprgttpye;
	private String Phome;
	private String CompanyPhone;
	private String ImpartParam;
	private String MaxBnfGrade;
	private String BnfGrades;
	private String CvaliDate;
	private String SumBnfLot;
	private String TMOBILE;
	private String TCOMPANYPHONE;
	private String Insuredidno;
	private String riskcode1;
	private String riskcode2;

	private int ImpartNum1;
	private int ImpartNum2;
	private int transCount;
	private String newbankcode;
	private double dSumPrem;
	private double qSumPrem;
	private String Appflag;
	private String MainRiskCode;
	private int MPayyears;
	private double SumPrem;
	private String  MEndDate;
	private int RiskNum;//当前保单总险种数
	private String sequenceno;
	private String  AppntOccupationCode;
	/** 自动续保标记1 */
	private String AutoRnewAge;
	/**借款起期*/
	private String JYStartDate;
	private int nowYears;
	/**多年期标记*/
	private String multiYearMark;
	/**系统当前时间*/
	private String currentDate;
	/**借款止期*/
	private String JYEndDate;
	/**借款人性质*/
	private String LoanerNature;
	/** 借意险贷款金额 */
	private String LoanAmount;
	/** 借意险贷款发放机构 */
	private String LendCom;
	/** 借款合同编号 */
	private String JYContNo;
	/** 中介(02)与核心系统(01)区别码 */
	private String system;
	/**卡单中介渠道类型*/
	private String channelType2;
	/**预留儿童其它公司身故保额*/
	private String MinorAmnt;
	/**当前保单的险种编码集*/
	private String currentRiskcodes;
	/**补充告知*/
	private String SupplementImpartParam;
	/**残疾告知项次*/
	private String ImpartParamCount;
	/**孕妇告知1*/
	private String PregnancyInform1;
	/**孕妇告知2*/
	private String PregnancyInform2;

	/**主险保额*/
	private double MAmnt;
	/**受益人客户号*/
	private String BnfNo;

	/**紧急联系人信函发送形式*/
	private String LetterSendMode;
	/**投保人E-mail*/
	private String AppntEmail;
	/**累计当前单长期险的保费*/
	private String LSumPrem;
	/**豁免险标记**/
	private  String ImmunityFlag;
	/**微信告知信息标记**/
	private String ImpartInfo;
	/***微信续保标记**/
	private String RenewFlag;

	/**累计当前单长期主险的保费*/
	private String lMsumprem;
	//1041缴费期间
	private int PayEndYear_1041 ;
	//7065缴费期间
	private int PayEndYear_7065;
	//1041保险期间
	private int InsuYear_1041;
	//7065保险期间
	private int InsuYear_7065;

	public int getPayEndYear_1041() {
		return PayEndYear_1041;
	}

	public void setPayEndYear_1041(int payEndYear_1041) {
		PayEndYear_1041 = payEndYear_1041;
	}

	public int getPayEndYear_7065() {
		return PayEndYear_7065;
	}

	public void setPayEndYear_7065(int payEndYear_7065) {
		PayEndYear_7065 = payEndYear_7065;
	}

	public int getInsuYear_1041() {
		return InsuYear_1041;
	}

	public void setInsuYear_1041(int insuYear_1041) {
		InsuYear_1041 = insuYear_1041;
	}

	public int getInsuYear_7065() {
		return InsuYear_7065;
	}

	public void setInsuYear_7065(int insuYear_7065) {
		InsuYear_7065 = insuYear_7065;
	}

	public String getlMsumprem() {
		return lMsumprem;
	}

	public void setlMsumprem(String lMsumprem) {
		this.lMsumprem = lMsumprem;
	}

	public String getRenewFlag() {
		return RenewFlag;
	}

	public void setRenewFlag(String renewFlag) {
		RenewFlag = renewFlag;
	}

	public String getImpartInfo() {
		return ImpartInfo;
	}

	public void setImpartInfo(String impartInfo) {
		ImpartInfo = impartInfo;
	}

	public String getImmunityFlag() {
		return ImmunityFlag;
	}

	public void setImmunityFlag(String immunityFlag) {
		ImmunityFlag = immunityFlag;
	}

	public int getRiskNum() {
		return RiskNum;
	}

	public void setRiskNum(int riskNum) {
		RiskNum = riskNum;
	}

	public String getMEndDate() {
		return MEndDate;
	}

	public void setMEndDate(String MEndDate) {
		this.MEndDate = MEndDate;
	}

	public int getMPayyears() {
		return MPayyears;
	}

	public void setMPayyears(int MPayyears) {
		this.MPayyears = MPayyears;
	}

	public double getSumPrem() {
		return SumPrem;
	}

	public void setSumPrem(double sumPrem) {
		SumPrem = sumPrem;
	}

	public String getMainRiskCode() {
		return MainRiskCode;
	}

	public void setMainRiskCode(String mainRiskCode) {
		MainRiskCode = mainRiskCode;
	}

	public String getAppflag() {
		return Appflag;
	}

	public void setAppflag(String appflag) {
		Appflag = appflag;
	}

	public double getdSumPrem() {
		return dSumPrem;
	}

	public void setdSumPrem(double dSumPrem) {
		this.dSumPrem = dSumPrem;
	}

	public double getqSumPrem() {
		return qSumPrem;
	}

	public void setqSumPrem(double qSumPrem) {
		this.qSumPrem = qSumPrem;
	}

	public String getFamilyImpartParam() {
		return familyImpartParam;
	}

	public void setFamilyImpartParam(String familyImpartParam) {
		this.familyImpartParam = familyImpartParam;
	}

	public String getAnnualImpartParam() {
		return annualImpartParam;
	}

	public void setAnnualImpartParam(String annualImpartParam) {
		this.annualImpartParam = annualImpartParam;
	}

	private String familyImpartParam;
	private String annualImpartParam;

	public String getInsuredidno() {
		return Insuredidno;
	}

	public void setInsuredidno(String insuredidno) {
		Insuredidno = insuredidno;
	}

	public String getRiskcode1() {
		return riskcode1;
	}

	public void setRiskcode1(String riskcode1) {
		this.riskcode1 = riskcode1;
	}

	public String getRiskcode2() {
		return riskcode2;
	}

	public void setRiskcode2(String riskcode2) {
		this.riskcode2 = riskcode2;
	}

	public int getImpartNum1() {
		return ImpartNum1;
	}

	public void setImpartNum1(int impartNum1) {
		ImpartNum1 = impartNum1;
	}

	public int getImpartNum2() {
		return ImpartNum2;
	}

	public void setImpartNum2(int impartNum2) {
		ImpartNum2 = impartNum2;
	}

	public int getTransCount() {
		return transCount;
	}

	public void setTransCount(int transCount) {
		this.transCount = transCount;
	}

	public String getNewbankcode() {
		return newbankcode;
	}

	public void setNewbankcode(String newbankcode) {
		this.newbankcode = newbankcode;
	}

	public String getBankAgentName() {
		return BankAgentName;
	}

	public void setBankAgentName(String bankAgentName) {
		BankAgentName = bankAgentName;
	}

	public String getAccName() {
		return AccName;
	}

	public void setAccName(String accName) {
		AccName = accName;
	}

	public String getBankAccno() {
		return BankAccno;
	}

	public void setBankAccno(String bankAccno) {
		BankAccno = bankAccno;
	}

	public String getNewBankAccno() {
		return NewBankAccno;
	}

	public void setNewBankAccno(String newBankAccno) {
		NewBankAccno = newBankAccno;
	}

	public String getAccName1() {
		return AccName1;
	}

	public void setAccName1(String accName1) {
		AccName1 = accName1;
	}

	public String getBankAccno1() {
		return BankAccno1;
	}

	public void setBankAccno1(String bankAccno1) {
		BankAccno1 = bankAccno1;
	}

	public String getNewBankAccno1() {
		return NewBankAccno1;
	}

	public void setNewBankAccno1(String newBankAccno1) {
		NewBankAccno1 = newBankAccno1;
	}

	public String getNewAccName() {
		return NewAccName;
	}

	public void setNewAccName(String newAccName) {
		NewAccName = newAccName;
	}

	public String getNewAccName1() {
		return NewAccName1;
	}

	public void setNewAccName1(String newAccName1) {
		NewAccName1 = newAccName1;
	}

	public String getApprgttpye() {
		return Apprgttpye;
	}

	public void setApprgttpye(String apprgttpye) {
		Apprgttpye = apprgttpye;
	}

	public String getPhome() {
		return Phome;
	}

	public void setPhome(String phome) {
		Phome = phome;
	}

	public String getCompanyPhone() {
		return CompanyPhone;
	}

	public void setCompanyPhone(String companyPhone) {
		CompanyPhone = companyPhone;
	}

	public String getImpartParam() {
		return ImpartParam;
	}

	public void setImpartParam(String impartParam) {
		ImpartParam = impartParam;
	}

	public String getMaxBnfGrade() {
		return MaxBnfGrade;
	}

	public void setMaxBnfGrade(String maxBnfGrade) {
		MaxBnfGrade = maxBnfGrade;
	}

	public String getBnfGrades() {
		return BnfGrades;
	}

	public void setBnfGrades(String bnfGrades) {
		BnfGrades = bnfGrades;
	}

	public String getCvaliDate() {
		return CvaliDate;
	}

	public void setCvaliDate(String cvaliDate) {
		CvaliDate = cvaliDate;
	}

	public String getSumBnfLot() {
		return SumBnfLot;
	}

	public void setSumBnfLot(String sumBnfLot) {
		SumBnfLot = sumBnfLot;
	}

	public String getTMOBILE() {
		return TMOBILE;
	}

	public void setTMOBILE(String TMOBILE) {
		this.TMOBILE = TMOBILE;
	}

	public String getTCOMPANYPHONE() {
		return TCOMPANYPHONE;
	}

	public void setTCOMPANYPHONE(String TCOMPANYPHONE) {
		this.TCOMPANYPHONE = TCOMPANYPHONE;
	}

	public String getTHOMEPHONE() {
		return THOMEPHONE;
	}

	public void setTHOMEPHONE(String THOMEPHONE) {
		this.THOMEPHONE = THOMEPHONE;
	}

	public String getTPHONE() {
		return TPHONE;
	}

	public void setTPHONE(String TPHONE) {
		this.TPHONE = TPHONE;
	}

	private String THOMEPHONE;
	private String TPHONE;

	private String  PoliValidDate;


	public String getAppntIncome() {
		return AppntIncome;
	}

	public void setAppntIncome(String appntIncome) {
		AppntIncome = appntIncome;
	}

	public String getProposalContNo() {
		return ProposalContNo;
	}

	public void setProposalContNo(String proposalContNo) {
		ProposalContNo = proposalContNo;
	}

	public String getSubType() {
		return SubType;
	}

	public void setSubType(String subType) {
		SubType = subType;
	}

	public String getBudget() {
		return Budget;
	}

	public void setBudget(String budget) {
		Budget = budget;
	}

	public String getHomeStoreNo() {
		return HomeStoreNo;
	}

	public void setHomeStoreNo(String homeStoreNo) {
		HomeStoreNo = homeStoreNo;
	}

	public String getPostalStoreNo() {
		return PostalStoreNo;
	}

	public void setPostalStoreNo(String postalStoreNo) {
		PostalStoreNo = postalStoreNo;
	}

	public String getZoneCode() {
		return ZoneCode;
	}

	public void setZoneCode(String zoneCode) {
		ZoneCode = zoneCode;
	}

	public String getAppntIDType() {
		return AppntIDType;
	}

	public void setAppntIDType(String appntIDType) {
		AppntIDType = appntIDType;
	}

	public String getInsuredIDType() {
		return InsuredIDType;
	}

	public void setInsuredIDType(String insuredIDType) {
		InsuredIDType = insuredIDType;
	}

	public String getDegree() {
		return Degree;
	}

	public void setDegree(String degree) {
		Degree = degree;
	}

	public String getMarriage() {
		return Marriage;
	}

	public void setMarriage(String marriage) {
		Marriage = marriage;
	}

	public String getIdValiEndDate() {
		return IdValiEndDate;
	}

	public void setIdValiEndDate(String idValiEndDate) {
		IdValiEndDate = idValiEndDate;
	}

	public String getPolApplyDate() {
		return PolApplyDate;
	}

	public void setPolApplyDate(String polApplyDate) {
		PolApplyDate = polApplyDate;
	}

	public String getProvince() {
		return Province;
	}

	public void setProvince(String province) {
		Province = province;
	}

	public String getZipCode() {
		return ZipCode;
	}

	public void setZipCode(String zipCode) {
		ZipCode = zipCode;
	}

	public String getAppntPostalAddress() {
		return AppntPostalAddress;
	}

	public void setAppntPostalAddress(String appntPostalAddress) {
		AppntPostalAddress = appntPostalAddress;
	}

	public String getAppntHomeProvince() {
		return AppntHomeProvince;
	}

	public void setAppntHomeProvince(String appntHomeProvince) {
		AppntHomeProvince = appntHomeProvince;
	}


	public String getAppntHomeZipCode() {
		return AppntHomeZipCode;
	}

	public void setAppntHomeZipCode(String appntHomeZipCode) {
		AppntHomeZipCode = appntHomeZipCode;
	}

	public String getCity() {
		return City;
	}

	public void setCity(String city) {
		City = city;
	}

	public String getDistrict() {
		return District;
	}

	public void setDistrict(String district) {
		District = district;
	}

	public String getMobile() {
		return Mobile;
	}

	public void setMobile(String mobile) {
		Mobile = mobile;
	}

	public String getHomePhone() {
		return HomePhone;
	}

	public void setHomePhone(String homePhone) {
		HomePhone = homePhone;
	}

	public String getAgentName() {
		return AgentName;
	}

	public void setAgentName(String agentName) {
		AgentName = agentName;
	}

	public String getAccBankCode() {
		return AccBankCode;
	}

	public void setAccBankCode(String accBankCode) {
		AccBankCode = accBankCode;
	}

	public String getRelaToInsured() {
		return RelaToInsured;
	}

	public void setRelaToInsured(String relaToInsured) {
		RelaToInsured = relaToInsured;
	}

	public String getBnfType() {
		return BnfType;
	}

	public void setBnfType(String bnfType) {
		BnfType = bnfType;
	}

	public String getLCBnfName() {
		return LCBnfName;
	}

	public void setLCBnfName(String lCBnfName) {
		LCBnfName = lCBnfName;
	}

	public String getLCBnfSex() {
		return LCBnfSex;
	}

	public void setLCBnfSex(String lCBnfSex) {
		LCBnfSex = lCBnfSex;
	}

	public String getLCBnfBirthday() {
		return LCBnfBirthday;
	}

	public void setLCBnfBirthday(String lCBnfBirthday) {
		LCBnfBirthday = lCBnfBirthday;
	}

	public String getLCBnfIDNo() {
		return LCBnfIDNo;
	}

	public void setLCBnfIDNo(String lCBnfIDNo) {
		LCBnfIDNo = lCBnfIDNo;
	}

	public String getLCBnfPhone() {
		return LCBnfPhone;
	}

	public void setLCBnfPhone(String lCBnfPhone) {
		LCBnfPhone = lCBnfPhone;
	}

	public String getRiskName() {
		return RiskName;
	}

	public void setRiskName(String riskName) {
		RiskName = riskName;
	}

	public String getAutoPayFlag() {
		return AutoPayFlag;
	}

	public void setAutoPayFlag(String autoPayFlag) {
		AutoPayFlag = autoPayFlag;
	}

	/** 自动垫交标记 */
	private String AutoPayFlag;

	public String getAgentCode() {
		return AgentCode;
	}

	public void setAgentCode(String agentCode) {
		AgentCode = agentCode;
	}

	public String getAgentCom() {
		return AgentCom;
	}

	public void setAgentCom(String agentCom) {
		AgentCom = agentCom;
	}

	/** 万能的初始账户价值（保费扣除初始费用后的部分） */
	private double AccountValue;

	public double getAccountValue() {
		return AccountValue;
	}

	public void setAccountValue(double accountValue) {
		AccountValue = accountValue;
	}

	// @Constructor
	public FieldCarrier() {
	}

	// @Method
	public void setBonusMan(String tBonusMan) {
		BonusMan = tBonusMan;
	}

	public String getBonusMan() {
		return String.valueOf(BonusMan);
	}

	public void setSSFlag(String tSSFlag) {
		SSFlag = tSSFlag;
	}

	public String getSSFlag() {
		return String.valueOf(SSFlag);
	}

	public void setBonusGetMode(String tBonusGetMode) {
		BonusGetMode = tBonusGetMode;
	}

	public String getBonusGetMode() {
		return String.valueOf(BonusGetMode);
	}

	public void setCombiFlag(String tCombiFlag) {
		CombiFlag = tCombiFlag;
	}

	public String getCombiFlag() {
		return String.valueOf(CombiFlag);
	}

	public void setInsuredNo(String tInsuredNo) {
		InsuredNo = tInsuredNo;
	}

	public String getInsuredNo() {
		return String.valueOf(InsuredNo);
	}

	public void setPrem(double tPrem) {
		Prem = tPrem;
	}

	public String getPrem() {
		return String.valueOf(Prem);
	}

	public void setGet(double tGet) {
		Get = tGet;
	}

	public String getGet() {
		return String.valueOf(Get);
	}

	public void setAmnt(double tAmnt) {
		Amnt = tAmnt;
	}

	public String getAmnt() {
		return String.valueOf(Amnt);
	}

	public void setMult(double tMult) {
		Mult = tMult;
	}

	public String getMult() {
		return String.valueOf(Mult);
	}

	public void setFloatRate(double tFloatRate) {
		FloatRate = tFloatRate;
	}

	public String getFloatRate() {
		return String.valueOf(FloatRate);
	}

	public void setAddRate(double tAddRate) {
		AddRate = tAddRate;
	}

	public String getAddRate() {
		return String.valueOf(AddRate);
	}

	public void setPayIntv(int tPayIntv) {
		PayIntv = tPayIntv;
	}

	public void setPolTypeFlag(String tPolTypeFlag) {
		PolTypeFlag = tPolTypeFlag;
	}

	public void setManageFeeRate(double tManageFeeRate) {
		ManageFeeRate = tManageFeeRate;
	}

	public String getPayIntv() {
		return String.valueOf(PayIntv);
	}

	public void setGetIntv(int tGetIntv) {
		GetIntv = tGetIntv;
	}

	public String getGetIntv() {
		return String.valueOf(GetIntv);
	}

	public void setPayEndYear(int tPayEndYear) {
		PayEndYear = tPayEndYear;
	}

	public String getPayEndYear() {
		return String.valueOf(PayEndYear);
	}

	public void setGetYear(int tGetYear) {
		GetYear = tGetYear;
	}

	public String getGetYear() {
		return String.valueOf(GetYear);
	}

	public void setYears(int tYears) {
		Years = tYears;
	}

	public String getYears() {
		return String.valueOf(Years);
	}

	public void setInsuYear(int tInsuYear) {
		InsuYear = tInsuYear;
	}

	public String getInsuYear() {
		return String.valueOf(InsuYear);
	}

	public void setAppAge(int tAppAge) {
		AppAge = tAppAge;
	}

	public String getAppAge() {
		return String.valueOf(AppAge);
	}

	public void setCount(int tCount) {
		Count = tCount;
	}

	public String getCount() {
		return String.valueOf(Count);
	}

	public void setRnewFlag(int tRnewFlag) {
		RnewFlag = tRnewFlag;
	}

	public String getRnewFlag() {
		return String.valueOf(RnewFlag);
	}

	public void setSex(String tSex) {
		Sex = tSex;
	}

	public String getSex() {
		return Sex;
	}

	public void setInsuredBirthday(String tInsuredBirthday) {
		InsuredBirthday = tInsuredBirthday;
	}

	public String getInsuredBirthday() {
		return InsuredBirthday;
	}

	public void setInsuYearFlag(String tInsuYearFlag) {
		InsuYearFlag = tInsuYearFlag;
	}

	public String getInsuYearFlag() {
		return InsuYearFlag;
	}

	public void setPayYears(int tPayYears) {
		PayYears = tPayYears;
	}

	public int getPayYears() {
		return PayYears;
	}

	public void setPayEndYearFlag(String tPayEndYearFlag) {
		PayEndYearFlag = tPayEndYearFlag;
	}

	public String getPayEndYearFlag() {
		return PayEndYearFlag;
	}

	public String getPayEndDate() {
		return PayEndDate;
	}

	public void setPayEndDate(String payEndDate) {
		PayEndDate = payEndDate;
	}

	public void setGetYearFlag(String tGetYearFlag) {
		GetYearFlag = tGetYearFlag;
	}

	public String getGetYearFlag() {
		return GetYearFlag;
	}

	public void setGetDutyKind(String tGetDutyKind) {
		GetDutyKind = tGetDutyKind;
	}

	public String getGetDutyKind() {
		return GetDutyKind;
	}

	public void setStartDateCalRef(String tStartDateCalRef) {
		StartDateCalRef = tStartDateCalRef;
	}

	public String getStartDateCalRef() {
		return StartDateCalRef;
	}

	public void setJob(String tJob) {
		Job = tJob;
	}

	public String getJob() {
		return Job;
	}

	public void setOccupationCode(String tOccupationCode) {
		OccupationCode = tOccupationCode;
	}

	public String getOccupationCode() {
		return OccupationCode;
	}

	public void setGDuty(String tGDuty) {
		GDuty = tGDuty;
	}

	public void setDutyCode(String tDutyCode) {
		DutyCode = tDutyCode;
	}

	public String getGDuty() {
		return GDuty;
	}

	public void setContNo(String tContNo) {
		ContNo = tContNo;
	}

	public void setPolNo(String tPolNo) {
		PolNo = tPolNo;
	}

	public String getContNo() {
		return ContNo;
	}

	public String getPolNo() {
		return PolNo;
	}

	public void setRiskCode(String tRiskCode) {
		RiskCode = tRiskCode;
	}

	public String getRiskCode() {
		return RiskCode;
	}

	public void setMainPolNo(String tMainPolNo) {
		MainPolNo = tMainPolNo;
	}

	public String getMainPolNo() {
		return MainPolNo;
	}

	public void setOccupationType(String tOccupationType) {
		OccupationType = tOccupationType;
	}

	public String getOccupationType() {
		return OccupationType;
	}

	public void setStandbyFlag1(String tStandbyFlag1) {
		StandbyFlag1 = tStandbyFlag1;
	}

	public String getStandbyFlag1() {
		return StandbyFlag1;
	}

	public void setStandbyFlag2(String tStandbyFlag2) {
		StandbyFlag2 = tStandbyFlag2;
	}

	public String getStandbyFlag2() {
		return StandbyFlag2;
	}

	public void setStandbyFlag3(String tStandbyFlag3) {
		StandbyFlag3 = tStandbyFlag3;
	}

	public String getStandbyFlag3() {
		return StandbyFlag3;
	}

	public void setStandbyFlag4(String tStandbyFlag4) {
		StandbyFlag4 = tStandbyFlag4;
	}

	public String getStandbyFlag4() {
		return StandbyFlag4;
	}

	public void setEndDate(String tEndDate) {
		EndDate = tEndDate;
	}

	public String getEndDate() {
		return EndDate;
	}

	public void setGrpPolNo(String tGrpPolNo) {
		GrpPolNo = tGrpPolNo;
	}

	public String getGrpPolNo() {
		return GrpPolNo;
	}

	public void setInterval(int tInterval) {
		Interval = tInterval;
	}

	public String getInterval() {
		return String.valueOf(Interval);
	}

	public void setEdorNo(String tEdorNo) {
		EdorNo = tEdorNo;
	}

	public String getEdorNo() {
		return EdorNo;
	}

	public void setEdorType(String tEdorType) {
		EdorType = tEdorType;
	}

	public String getEdorType() {
		return EdorType;
	}

	public void setGetMoney(double tGetMoney) {
		GetMoney = tGetMoney;
	}

	public String getGetMoney() {
		return String.valueOf(GetMoney);
	}

	public void setCValiDate(String tCValiDate) {
		CValiDate = tCValiDate;
	}

	public String getCValiDate() {
		return CValiDate;
	}

	public void setEdorValiDate(String tEdorValiDate) {
		EdorValiDate = tEdorValiDate;
	}

	public String getEdorValiDate() {
		return EdorValiDate;
	}

	public void setGetStartDate(String tGetStartDate) {
		GetStartDate = tGetStartDate;
	}

	public String getGetStartDate() {
		return GetStartDate;
	}

	public void setGetBalance(double tGetBalance) {
		GetBalance = tGetBalance;
	}

	public String getGetBalance() {
		return String.valueOf(GetBalance);
	}

	public void setGetStartFlag(String tGetStartFlag) {
		GetStartFlag = tGetStartFlag;
	}

	public String getGetStartFlag() {
		return GetStartFlag;
	}

	public void setIDNo(String tIDNo) {
		IDNo = tIDNo;
	}

	public String getIDNo() {
		return IDNo;
	}

	public void setIDType(String tIDType) {
		IDType = tIDType;
	}

	public String getIDType() {
		return IDType;
	}

	public void setWorkType(String tWorkType) {
		WorkType = tWorkType;
	}

	public String getWorkType() {
		return WorkType;
	}

	public void setGrpName(String tGrpName) {
		GrpName = tGrpName;
	}

	public String getGrpName() {
		return GrpName;
	}

	public void setInsuredName(String tInsuredName) {
		InsuredName = tInsuredName;
	}

	public String getInsuredName() {
		return InsuredName;
	}

	public void setManageCom(String tManageCom) {
		ManageCom = tManageCom;
	}

	public String getManageCom() {
		return ManageCom;
	}

	public String getPolTypeFlag() {
		return PolTypeFlag;
	}

	public double getManageFeeRate() {
		return ManageFeeRate;
	}

	public String getDutyCode() {
		return DutyCode;
	}

	public void setGetLimit(double tGetLimit) {
		GetLimit = tGetLimit;
	}

	public double getGetLimit() {
		return GetLimit;
	}

	public void setContPlanCode(String tContPlanCode) {
		ContPlanCode = tContPlanCode;
	}

	public String getContPlanCode() {
		return ContPlanCode;
	}

	public String getProdsetCode() {
		return ProdsetCode;
	}

	public void setProdsetCode(String prodsetCode) {
		ProdsetCode = prodsetCode;
	}

	public String getSaleChnl() {
		return SaleChnl;
	}

	public void setSaleChnl(String tSaleChnl) {
		SaleChnl = tSaleChnl;
	}

	public String getVerifyApplyDate() {
		return VerifyApplyDate;
	}

	public void setVerifyApplyDate(String tVerifyApplyDate) {
		VerifyApplyDate = tVerifyApplyDate;
	}

	public String getBankCode() {
		return BankCode;
	}

	public void setBankCode(String bankCode) {
		BankCode = bankCode;
	}

	public String getWTPrem() {
		return WTPrem;
	}

	public void setWTPrem(String prem) {
		WTPrem = prem;
	}

	public String getAppntNo() {
		return AppntNo;
	}

	public void setAppntNo(String appntNo) {
		AppntNo = appntNo;
	}

	public String getNativePlace() {
		return NativePlace;
	}

	public void setNativePlace(String nativePlace) {
		NativePlace = nativePlace;
	}

	public String getRelationToMainInsured() {
		return RelationToMainInsured;
	}

	public void setRelationToMainInsured(String relationToMainInsured) {
		RelationToMainInsured = relationToMainInsured;
	}

	public String getSellType() {
		return SellType;
	}

	public void setSellType(String sellType) {
		SellType = sellType;
	}

	public String getAppRelatToInsu() {
		return AppRelatToInsu;
	}

	public void setAppRelatToInsu(String appRelatToInsu) {
		AppRelatToInsu = appRelatToInsu;
	}

	public String getAppIDNo() {
		return AppIDNo;
	}

	public void setAppIDNo(String appIDNo) {
		AppIDNo = appIDNo;
	}

	public String getAppntBirthday() {
		return AppntBirthday;
	}

	public void setAppntBirthday(String appntBirthday) {
		AppntBirthday = appntBirthday;
	}

	public String getAppntSex() {
		return AppntSex;
	}

	public void setAppntSex(String appntSex) {
		AppntSex = appntSex;
	}

	public String getAppntName() {
		return AppntName;
	}

	public void setAppntName(String appntName) {
		AppntName = appntName;
	}

	public String getOutAgentCode() {
		return OutAgentCode;
	}

	public void setOutAgentCode(String outAgentCode) {
		OutAgentCode = outAgentCode;
	}

	public String getOutAgentName() {
		return OutAgentName;
	}

	public void setOutAgentName(String outAgentName) {
		OutAgentName = outAgentName;
	}


	public String getNewPayMode() {
		return NewPayMode;
	}

	public void setNewPayMode(String newPayMode) {
		NewPayMode = newPayMode;
	}

	public String getNewBankAccNo() {
		return NewBankAccNo;
	}

	public void setNewBankAccNo(String newBankAccNo) {
		NewBankAccNo = newBankAccNo;
	}

	public String getPayLocation() {
		return PayLocation;
	}

	public void setPayLocation(String payLocation) {
		PayLocation = payLocation;
	}

	public String getBankAccNo() {
		return BankAccNo;
	}

	public void setBankAccNo(String bankAccNo) {
		BankAccNo = bankAccNo;
	}

	public String getAppendPrem() {
		return AppendPrem;
	}

	public void setAppendPrem(String appendPrem) {
		AppendPrem = appendPrem;
	}

	public String getExtPrem() {
		return ExtPrem;
	}

	public void setExtPrem(String extPrem) {
		ExtPrem = extPrem;
	}

	public String getGetMode() {
		return GetMode;
	}

	public void setGetMode(String getMode) {
		GetMode = getMode;
	}

	public String getBPOFlag() {
		return BPOFlag;
	}

	public void setBPOFlag(String flag) {
		BPOFlag = flag;
	}

	public String getBankCustomerType() {
		return BankCustomerType;
	}

	public void setBankCustomerType(String bankCustomerType) {
		BankCustomerType = bankCustomerType;
	}

	public String getYBTModeFlag() {
		return YBTModeFlag;
	}

	public void setYBTModeFlag(String modeFlag) {
		YBTModeFlag = modeFlag;
	}

	public String getAppRgtType()
	{
		return AppRgtType;
	}
	public void setAppRgtType(String apprgttype)
	{
		AppRgtType=apprgttype;
	}

	public String getHomeAddress() {
		return HomeAddress;
	}

	public void setHomeAddress(String homeAddress) {
		HomeAddress = homeAddress;
	}

	public String getTinFlag() {
		return TinFlag;
	}

	public void setTinFlag(String tinFlag) {
		TinFlag = tinFlag;
	}

	public String getTinNo() {
		return TinNo;
	}

	public void setTinNo(String tinNo) {
		TinNo = tinNo;
	}

	public String getAppAge2() {
		return AppAge2;
	}

	public void setAppAge2(String appAge2) {
		AppAge2 = appAge2;
	}

	public String getAppntJob() {
		return AppntJob;
	}

	public void setAppntJob(String appntJob) {
		AppntJob = appntJob;
	}

	public String getAppntJobCode() {
		return AppntJobCode;
	}

	public void setAppntJobCode(String appntJobCode) {
		AppntJobCode = appntJobCode;
	}

	public String getAppntJobType() {
		return AppntJobType;
	}

	public void setAppntJobType(String appntJobType) {
		AppntJobType = appntJobType;
	}

	public String getInsuJobCode() {
		return InsuJobCode;
	}

	public void setInsuJobCode(String insuJobCode) {
		InsuJobCode = insuJobCode;
	}

	public String getInsuJobType() {
		return InsuJobType;
	}

	public void setInsuJobType(String insuJobType) {
		InsuJobType = insuJobType;
	}

	public String getInsuredSex() {
		return InsuredSex;
	}

	public void setInsuredSex(String insuredSex) {
		InsuredSex = insuredSex;
	}

	public String getPayMode() {
		return PayMode;
	}

	public void setPayMode(String payMode) {
		PayMode = payMode;
	}

	public String getPrtNo() {
		return PrtNo;
	}

	public void setPrtNo(String prtNo) {
		PrtNo = prtNo;
	}

	public String getSecondInsuredNo() {
		return SecondInsuredNo;
	}

	public void setSecondInsuredNo(String secondInsuredNo) {
		SecondInsuredNo = secondInsuredNo;
	}

	public double getMPrem() {
		return MPrem;
	}

	public void setMPrem(double MPrem) {
		this.MPrem = MPrem;
	}

	public String getRiskPeriod() {
		return RiskPeriod;
	}

	public void setRiskPeriod(String riskPeriod) {
		RiskPeriod = riskPeriod;
	}

	public String getSequenceno() {
		return sequenceno;
	}

	public void setSequenceno(String sequenceno) {
		this.sequenceno = sequenceno;
	}

	public String getAppntOccupationCode() {
		return AppntOccupationCode;
	}

	public void setAppntOccupationCode(String appntOccupationCode) {
		AppntOccupationCode = appntOccupationCode;
	}

	public String getAutoRnewAge() {
		return AutoRnewAge;
	}

	public void setAutoRnewAge(String autoRnewAge) {
		AutoRnewAge = autoRnewAge;
	}

	public String getJYStartDate() {
		return JYStartDate;
	}

	public void setJYStartDate(String JYStartDate) {
		this.JYStartDate = JYStartDate;
	}

	public int getNowYears() {
		return nowYears;
	}

	public void setNowYears(int nowYears) {
		this.nowYears = nowYears;
	}

	public String getPoliValidDate() {
		return PoliValidDate;
	}

	public void setPoliValidDate(String poliValidDate) {
		PoliValidDate = poliValidDate;
	}

	public String getMultiYearMark() {
		return multiYearMark;
	}

	public void setMultiYearMark(String multiYearMark) {
		this.multiYearMark = multiYearMark;
	}

	public String getCurrentDate() {
		return currentDate;
	}

	public void setCurrentDate(String currentDate) {
		this.currentDate = currentDate;
	}

	public String getJYEndDate() {
		return JYEndDate;
	}

	public void setJYEndDate(String JYEndDate) {
		this.JYEndDate = JYEndDate;
	}

	public String getLoanerNature() {
		return LoanerNature;
	}

	public void setLoanerNature(String loanerNature) {
		LoanerNature = loanerNature;
	}

	public String getLoanAmount() {
		return LoanAmount;
	}

	public void setLoanAmount(String loanAmount) {
		LoanAmount = loanAmount;
	}

	public String getLendCom() {
		return LendCom;
	}

	public void setLendCom(String lendCom) {
		LendCom = lendCom;
	}

	public String getJYContNo() {
		return JYContNo;
	}

	public void setJYContNo(String JYContNo) {
		this.JYContNo = JYContNo;
	}

	public String getSystem() {
		return system;
	}

	public void setSystem(String system) {
		this.system = system;
	}

	public String getChannelType2() {
		return channelType2;
	}

	public void setChannelType2(String channelType2) {
		this.channelType2 = channelType2;
	}

	public String getMinorAmnt() {
		return MinorAmnt;
	}

	public void setMinorAmnt(String minorAmnt) {
		MinorAmnt = minorAmnt;
	}

	public String getCurrentRiskcodes() {
		return currentRiskcodes;
	}

	public void setCurrentRiskcodes(String currentRiskcodes) {
		this.currentRiskcodes = currentRiskcodes;
	}

	public String getSupplementImpartParam() {
		return SupplementImpartParam;
	}

	public void setSupplementImpartParam(String supplementImpartParam) {
		SupplementImpartParam = supplementImpartParam;
	}

	public String getImpartParamCount() {
		return ImpartParamCount;
	}

	public void setImpartParamCount(String impartParamCount) {
		ImpartParamCount = impartParamCount;
	}

	public String getPregnancyInform1() {
		return PregnancyInform1;
	}

	public void setPregnancyInform1(String pregnancyInform1) {
		PregnancyInform1 = pregnancyInform1;
	}

	public String getPregnancyInform2() {
		return PregnancyInform2;
	}

	public void setPregnancyInform2(String pregnancyInform2) {
		PregnancyInform2 = pregnancyInform2;
	}

	public double getMAmnt() {
		return MAmnt;
	}

	public void setMAmnt(double MAmnt) {
		this.MAmnt = MAmnt;
	}

	public String getBnfNo() {
		return BnfNo;
	}

	public void setBnfNo(String bnfNo) {
		BnfNo = bnfNo;
	}

	public String getLetterSendMode() {
		return LetterSendMode;
	}

	public void setLetterSendMode(String letterSendMode) {
		LetterSendMode = letterSendMode;
	}

	public String getAppntEmail() {
		return AppntEmail;
	}

	public void setAppntEmail(String appntEmail) {
		AppntEmail = appntEmail;
	}

	public String getLSumPrem() {
		return LSumPrem;
	}

	public void setLSumPrem(String LSumPrem) {
		this.LSumPrem = LSumPrem;
	}
}