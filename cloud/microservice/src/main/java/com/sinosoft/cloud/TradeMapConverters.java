package com.sinosoft.cloud;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sinosoft.cloud.rest.TradeInfo;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpInputMessage;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;

import java.io.IOException;
import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Project abc-cloud
 * Created by sundongbo on 2017/9/14.
 * <pre>
 * 林花谢了春红，太匆匆，无奈朝来寒雨晚来风。
 * 胭脂泪，相留醉，几时重，自是人生长恨水长东。
 *
 * --翼羽水淼起辰月，秋山红日望紫枫--
 * </pre>
 */
@SpringBootConfiguration
public class TradeMapConverters {


    /**
     * 重写HttpMessageConverter的JSON转换
     * @return
     */
    @Bean
    public MappingJackson2HttpMessageConverter mappingJackson2HttpMessageConverter() {
        TradeMappingConverter jsonConverter = new TradeMappingConverter();
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        objectMapper.setDateFormat(dateFormat);
        jsonConverter.setObjectMapper(objectMapper);
        return jsonConverter;
    }


    /**
     * 重写read方法，重新序列化反序列化TradeInfo中TradeMap中POJO
     */
    class TradeMappingConverter extends MappingJackson2HttpMessageConverter {
        @Override
        public Object read(Type type, Class<?> contextClass, HttpInputMessage inputMessage) throws IOException, HttpMessageNotReadableException {
            Object o = super.read(type, contextClass, inputMessage);
            if ("TradeInfo".equals(o.getClass().getSimpleName())) {
                TradeInfo tradeInfo = (TradeInfo)o;
                HashMap<String, Object> pojoMap = tradeInfo.getTradeMap();
                Set<Map.Entry<String, Object>> entrySet = pojoMap.entrySet();

                Iterator<Map.Entry<String, Object>> it2 = entrySet.iterator();
                //objectMapper在Bean中已经设置了属性，有可能无用
                this.objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
                this.objectMapper.setDateFormat(new SimpleDateFormat("yyyy-MM-dd"));
                while (it2.hasNext()) {
                    Map.Entry<String, Object> me = it2.next();//获取Map.Entry关系对象me
                    String key2 = me.getKey();//通过关系对象获取key
                    if (me !=null && me.getValue() !=null && "java.util.LinkedHashMap".equals(me.getValue().getClass().getTypeName())) {
                        try {
                            pojoMap.put(me.getKey(), this.objectMapper.readValue(this.objectMapper.writeValueAsString(me.getValue()), Class.forName(me.getKey())));
                        } catch (ClassNotFoundException | IOException e) {
                            e.printStackTrace();
                        }

                    }
                    if (me !=null && me.getValue() !=null &&"java.util.ArrayList".equals(me.getValue().getClass().getTypeName())) {
                        try {
                            pojoMap.put(me.getKey(), this.objectMapper.readValue(this.objectMapper.writeValueAsString(me.getValue()), this.objectMapper.getTypeFactory().constructParametricType(ArrayList.class, Class.forName(me.getKey()))));
                        } catch (IOException | ClassNotFoundException e) {
                            e.printStackTrace();
                        }

                    }
                }

            }
            return o;
        }
    }

    //@Override
    //public void configureMessageConverters(
    //        List<HttpMessageConverter<?>> converters) {
    //    converters.add(mappingJackson2HttpMessageConverter());
    //}

    /**
     * 重写ObjectMapper方法，留存
     */
    //@Bean
    //public ObjectMapper jsonMapper() {
    //    ObjectMapper objectMapper = new OOO();
    //null输出空字符串
    //objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
    //SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
    //objectMapper.setDateFormat(dateFormat);
    //
    //SimpleModule deserializeModule = new SimpleModule("DeserializeModule", new Version(1, 0, 0, null));
    //SimpleModule deserializeModule = new SimpleModule("DeserializeModule");
    //deserializeModule.addDeserializer(TradeInfo.class,new CustomizeJsonDeserializer());
    //objectMapper.registerModule(deserializeModule);
    //SimpleModule module = new SimpleModule();
    //module.addDeserializer(TradeInfo.class, new TradeInfoDeserializer());
    //objectMapper.registerModule(module);
    //objectMapper.getSerializerProvider().
    //objectMapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS,false);
    //objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES,false);
    //objectMapper.getSerializerProvider().setNullValueSerializer(new JsonSerializer<Object>() {
    //    @Override
    //    public void serialize(Object value, JsonGenerator jgen, SerializerProvider provider) throws IOException {
    //        System.out.println("value = " + value);
    //        jgen.writeString("");
    //
    //    }
    //});
    //    return objectMapper;
    //}

    //class OOO extends ObjectMapper {
    //    public OOO() {
    //        this.setSerializationInclusion(JsonInclude.Include.NON_NULL);
    //        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
    //        this.setDateFormat(dateFormat);
    //    }

    /**
     * 保留方法，也可用，为以后性能有问题时使用
     */
    //@Override
    //public <T> T readValue(InputStream src, JavaType valueType) throws IOException {
    //
    //        if("TradeInfo".equals(valueType.getRawClass().getSimpleName()))
    //        {
    //            T t= super.readValue(src, valueType);
    //            Class<T> tradeInfo = (Class<T>) t.getClass().getDeclaredClasses()[0];
    //        HashMap<String, Object> pojoMap = tradeInfo..getTradeMap();
    //        Set<Map.Entry<String, Object>> entrySet = pojoMap.entrySet();
    //
    //        Iterator<Map.Entry<String, Object>> it2 = entrySet.iterator();
    //        ObjectMapper mapper = new ObjectMapper();
    //        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    //        mapper.setDateFormat(new SimpleDateFormat("yyyy-MM-dd"));
    //        while (it2.hasNext()) {
    //            Map.Entry<String, Object> me = it2.next();//获取Map.Entry关系对象me
    //            String key2 = me.getKey();//通过关系对象获取key
    //            if ("java.util.LinkedHashMap".equals(me.getValue().getClass().getTypeName())) {
    //                try {
    //                    pojoMap.put(me.getKey(), mapper.readValue(mapper.writeValueAsString(me.getValue()), Class.forName(me.getKey())));
    //                } catch (ClassNotFoundException | IOException e) {
    //                    e.printStackTrace();
    //                }
    //
    //            }
    //            if ("java.util.ArrayList".equals(me.getValue().getClass().getTypeName())) {
    //                try {
    //                    pojoMap.put(me.getKey(), mapper.readValue(mapper.writeValueAsString(me.getValue()), mapper.getTypeFactory().constructParametricType(ArrayList.class, Class.forName(me.getKey()))));
    //                } catch (IOException | ClassNotFoundException e) {
    //                    e.printStackTrace();
    //                }
    //
    //            }
    //        }
    //        return (T)tradeInfo;
    //    }else {
    //        return super.readValue(src, valueType);
    //    }
    //}



    //
    //class CustomizeJsonDeserializer extends BeanDeserializerBase<TradeInfo> {
    //
    //    public CustomizeJsonDeserializer(BeanDeserializerBuilder builder, BeanDescription beanDesc, BeanPropertyMap properties, Map<String, SettableBeanProperty> backRefs, HashSet<String> ignorableProps, boolean ignoreAllUnknown, boolean hasViews) {
    //        super(builder, beanDesc, properties, backRefs, ignorableProps, ignoreAllUnknown, hasViews);
    //    }
    //
    //    @Override
    //    public TradeInfo deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException, JsonProcessingException {
    //
    //        System.out.println("jp = " + jp);
    //        String value = jp.getText();
    //        //Map<String, String> properities = Splitter.on(",").trimResults().withKeyValueSeparator("=").split(StringUtils.substringBetween(value, "{", "}"));
    //        TradeInfo typeB = new TradeInfo();
    //
    //        return typeB;
    //    }
    //
    //
    //}

}