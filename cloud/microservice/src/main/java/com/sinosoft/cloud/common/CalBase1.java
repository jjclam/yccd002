package com.sinosoft.cloud.common;

import java.text.DecimalFormat;
import java.util.Date;

/**
 * 用于存储计划层投保规则
 */
public class CalBase1 {


        // @Field
        /** 保费 */
        private double Prem;

        /** 保额 */
        private double Get;

        /** 份数 */
        private int Mult;

        /** 缴费间隔 */
        private int PayIntv;

        /** 领取间隔 */
        private int GetIntv;

        /** 缴费终止年期或年龄 */
        private int PayEndYear;

        /** 缴费终止年期或年龄标记 */
        private String PayEndYearFlag;

        /** 领取开始年期或年龄 */
        private int GetYear;

        /** 领取开始年期或年龄标记 */
        private String GetYearFlag;

        /** 责任领取类型 */
        private String GetDutyKind;

        /** 起领日期计算参照 */
        private String StartDateCalRef;

        /** 保险期间 */
        private int Years;

        /** 保险期间 */
        private int InsuYear;

        /** 保险期间标记 */
        private String InsuYearFlag;

        /** 被保人投保年龄 */
        private int AppAge;

        //add by jiangly 20100127
        /** 投保人年龄 */
        private int Age;

        /** 保单款式 */
        private String InterCode;

        /** 保单机构 */
        private String ManageCom;

        /** 被保人与投保人的关系 */
        private String RelationToAppnt;

        //add end jiangly

        //add by jiangly 20100827
        /** 被保险人职业类别 */
        private String OccupationType;

        /** 主被保险人和附属被保险人的关系 */
        private String RelationToMainInsured;

        /** 附属被保人个数 */
        private String AddInsured;

        /** 投保日期 */
        private String PolApplyDate;

        /** 配偶的年龄 */
        private String MateAge;

        /** 子女的年龄 */
        private String ChildAge;

        /** 子女的出生日期 */
        private String ChildBirthday;

        //add end jiangly

        /** 被保人性别 */
        private String Sex;

        //add by fengzq  增加被保人信息
        //被保人姓名
        private String Name;
        //被保人证件类型
        private String IDType;
        //被保人证件号
        private String IDNo;
        //被保人出生日期
        private String Birthday;
        //航班号
        private String FlightNO;
        //当前日期
        private String MakeDate;
        //当前时间
        private String MakeTime;
        //add by fengzq end

        /** 被保人工种 */
        private String Job;

        /** 责任给付编码 */
        private String GDuty;

        /** 投保人数 */
        private int Count;

        /** 续保次数 */
        private int RnewFlag;

        /** 递增率 */
        private double AddRate;

        /** 个单印刷号 */
        private String PrtNo;

        /** 个单合同号 */
        private String ContNo;

        /** 保单号 */
        private String PolNo;

        /** 原保额 */
        private double Amnt;

        /** 浮动费率 */
        private double FloatRate;

        /**险种编码*/
        private String RiskCode;

        /**个人保单表中用来做传递计算变量的公用元素--团体商业补充保险中传递责任选择的组合代码*/
        private String StandbyFlag1;

        /**待用*/
        private String StandbyFlag2;

        /**团体商业补充保险中传递在职或退休字段*/
        private String StandbyFlag3;

        /**集体合同号*/
        private String GrpContNo;

        /**集体保单号*/
        private String GrpPolNo;

        private String EdorNo;

        /**计算类型*/
        private String CalType;

        /**起付线**/
        private double GetLimit;

        /**赔付比例**/
        private double GetRate;

        /**社保标记**/
        private String SSFlag;

        /**封顶线**/
        private double PeakLine;

        /**保单生效日**/
        private String CValiDate;
        // add by wuchao
        /** 借款金额 */
        private double LoanMoney;
        /** 借款合同编码 */
        private String LoanCont;
        /** 借款凭证编码 */
        private String LoanVouch;
        /** 借款起期 */
        private String LoanStartDate;
        /** 借款止期 */
        private String LoanEndDate;

        private DecimalFormat mDecFormat = new DecimalFormat("##.###");
        // @Constructor
        public CalBase1()
        {}

        // @Method
        public void setPrtNo(String tPrtNo)
        {
            PrtNo = tPrtNo;
        }

        // @Method
        public void setContNo(String tContNo)
        {
            ContNo = tContNo;
        }

        public void setGrpContNo(String tGrpContNo)
        {
            GrpContNo = tGrpContNo;
        }

        public void setPrem(double tPrem)
        {
            Prem = tPrem;
        }

        public String getPrem()
        {
            return String.valueOf(Prem);
        }

        public void setGet(double tGet)
        {
            Get = tGet;
        }

        public String getGet()
        {
            return String.valueOf(Get);
        }

        public void setAmnt(double tAmnt)
        {
            Amnt = tAmnt;
        }

        public String getAmnt()
        {
            return mDecFormat.format(Amnt);
        }

        public void setMult(int tMult)
        {
            Mult = tMult;
        }

        public String getMult()
        {
            return String.valueOf(Mult);
        }

        public void setFloatRate(double tFloatRate)
        {
            FloatRate = tFloatRate;
        }

        public String getFloatRate()
        {
            return String.valueOf(FloatRate);
        }

        public void setAddRate(double tAddRate)
        {
            AddRate = tAddRate;
        }

        public String getPrtNo()
        {
            return PrtNo;
        }

        public String getContNo()
        {
            return ContNo;
        }

        public String getGrpContNo()
        {
            return GrpContNo;
        }

        public String getAddRate()
        {
            return String.valueOf(AddRate);
        }

        public void setPayIntv(int tPayIntv)
        {
            PayIntv = tPayIntv;
        }

        public String getPayIntv()
        {
            return String.valueOf(PayIntv);
        }

        public void setGetIntv(int tGetIntv)
        {
            GetIntv = tGetIntv;
        }

        public String getGetIntv()
        {
            return String.valueOf(GetIntv);
        }

        public void setPayEndYear(int tPayEndYear)
        {
            PayEndYear = tPayEndYear;
        }

        public String getPayEndYear()
        {
            return String.valueOf(PayEndYear);
        }

        public void setGetYear(int tGetYear)
        {
            GetYear = tGetYear;
        }

        public String getGetYear()
        {
            return String.valueOf(GetYear);
        }

        public void setYears(int tYears)
        {
            Years = tYears;
        }

        public String getYears()
        {
            return String.valueOf(Years);
        }

        public void setInsuYear(int tInsuYear)
        {
            InsuYear = tInsuYear;
        }

        public String getInsuYear()
        {
            return String.valueOf(InsuYear);
        }

        public void setAppAge(int tAppAge)
        {
            AppAge = tAppAge;
        }

        public String getAppAge()
        {
            return String.valueOf(AppAge);
        }

        public void setCount(int tCount)
        {
            Count = tCount;
        }

        public String getCount()
        {
            return String.valueOf(Count);
        }

        public void setRnewFlag(int tRnewFlag)
        {
            RnewFlag = tRnewFlag;
        }

        public String getRnewFlag()
        {
            return String.valueOf(RnewFlag);
        }

        public void setSex(String tSex)
        {
            Sex = tSex;
        }

        public String getSex()
        {
            return Sex;
        }

        public void setInsuYearFlag(String tInsuYearFlag)
        {
            InsuYearFlag = tInsuYearFlag;
        }

        public String getInsuYearFlag()
        {
            return InsuYearFlag;
        }

        public void setPayEndYearFlag(String tPayEndYearFlag)
        {
            PayEndYearFlag = tPayEndYearFlag;
        }

        public String getPayEndYearFlag()
        {
            return PayEndYearFlag;
        }

        public void setGetYearFlag(String tGetYearFlag)
        {
            GetYearFlag = tGetYearFlag;
        }

        public String getGetYearFlag()
        {
            return GetYearFlag;
        }

        public void setGetDutyKind(String tGetDutyKind)
        {
            GetDutyKind = tGetDutyKind;
        }

        public String getGetDutyKind()
        {
            return GetDutyKind;
        }

        public void setStartDateCalRef(String tStartDateCalRef)
        {
            StartDateCalRef = tStartDateCalRef;
        }

        public String getStartDateCalRef()
        {
            return StartDateCalRef;
        }

        public void setJob(String tJob)
        {
            Job = tJob;
        }

        public String getJob()
        {
            return Job;
        }

        public void setGDuty(String tGDuty)
        {
            GDuty = tGDuty;
        }

        public String getGDuty()
        {
            return GDuty;
        }

        public void setPolNo(String tPolNo)
        {
            PolNo = tPolNo;
        }

        public String getPolNo()
        {
            return PolNo;
        }

        public void setRiskCode(String tRiskCode)
        {
            RiskCode = tRiskCode;
        }

        public String getRiskCode()
        {
            return RiskCode;
        }

        public void setStandbyFlag1(String tStandbyFlag1)
        {
            StandbyFlag1 = tStandbyFlag1;
        }

        public String getStandbyFlag1()
        {
            return StandbyFlag1;
        }

        public void setStandbyFlag2(String tStandbyFlag2)
        {
            StandbyFlag2 = tStandbyFlag2;
        }

        public String getStandbyFlag2()
        {
            return StandbyFlag2;
        }

        public void setStandbyFlag3(String tStandbyFlag3)
        {
            StandbyFlag3 = tStandbyFlag3;
        }

        public String getStandbyFlag3()
        {
            return StandbyFlag3;
        }

        public void setGrpPolNo(String tGrpPolNo)
        {
            GrpPolNo = tGrpPolNo;
        }

        public String getGrpPolNo()
        {
            return GrpPolNo;
        }

        public void setEdorNo(String tEdorNo)
        {
            EdorNo = tEdorNo;
        }

        public String getEdorNo()
        {
            return EdorNo;
        }

        public void setCalType(String tCalType)
        {
            CalType = tCalType;
        }

        public String getCalType()
        {
            return CalType;
        }

        public void setGetLimit(double tGetLimit)
        {
            GetLimit = tGetLimit;
        }

        public String getGetLimit()
        {
            return String.valueOf(GetLimit);
        }

        public void setGetRate(double tGetRate)
        {
            GetRate = tGetRate;
        }

        public String getGetRate()
        {
            return String.valueOf(GetRate);
        }

        public void setSSFlag(String tSSFlag)
        {
            SSFlag = tSSFlag;
        }

        public String getSSFlag()
        {
            return SSFlag;
        }

        public void setPeakLine(double tPeakLine)
        {
            PeakLine = tPeakLine;
        }

        public String getPeakLine()
        {
            return String.valueOf(PeakLine);
        }

        public void setCValiDate(String tCValiDate)
        {
            CValiDate = tCValiDate;
        }

        public String getCValiDate()
        {
            return CValiDate;
        }

        //add by fengzq 被保人信息
        //  被保人姓名
        public void setName(String tName)
        {
            Name = tName;
        }

        public String getName()
        {
            return Name;
        }
        //被保人证件类型
        public void setIDType(String tIDType)
        {
            IDType = tIDType;
        }

        public String getIDType()
        {
            return IDType;
        }
        //被保人证件号
        public void setIDNo(String tIDNo)
        {
            IDNo = tIDNo;
        }

        public String getIDNo()
        {
            return IDNo;
        }
        //被保人出生日期
        public void setBirthday(String tBirthday)
        {
            Birthday = tBirthday;
        }

        public String getBirthday()
        {
            return Birthday;
        }
        //航班号
        public void setFlightNO(String aFlightNO)
        {
            FlightNO = aFlightNO ;
        }
        public String getFlightNO()
        {
            return FlightNO;
        }
        //当前日期
        public void setMakeDate(String aMakeDate)
        {
            MakeDate = aMakeDate ;
        }
        public String getMakeDate()
        {
            return MakeDate;
        }
        //当前时间
        public void setMakeTime(String aMakeTime)
        {
            MakeTime = aMakeTime ;
        }
        public String getMakeTime()
        {
            return MakeTime;
        }



        //add by fengzq end

        //add by jiangly 20100127
        public void setAge(int tAge)
        {
            Age = tAge;
        }

        public String getAge()
        {
            return String.valueOf(Age);
        }

        public void setInterCode(String tInterCode)
        {
            InterCode = tInterCode;
        }

        public String getInterCode()
        {
            return InterCode;
        }

        public void setManageCom(String tManageCom)
        {
            ManageCom = tManageCom;
        }

        public String getManageCom()
        {
            return ManageCom;
        }

        public void setRelationToAppnt(String tRelationToAppnt)
        {
            RelationToAppnt = tRelationToAppnt;
        }

        public String getRelationToAppnt()
        {
            return RelationToAppnt;
        }
        //add end jiangly

        //add by jiangly 20100827
        public void setOccupationType(String tOccupationType){
            OccupationType = tOccupationType;
        }

        public String getOccupationType(){
            return OccupationType;
        }

        public void setRelationToMainInsured(String tRelationToMainInsured){
            RelationToMainInsured = tRelationToMainInsured;
        }

        public String getRelationToMainInsured(){
            return RelationToMainInsured;
        }

        public void setMateAge(String tMateAge){
            MateAge = tMateAge;
        }

        public String getMateAge(){
            return MateAge;
        }

        public void setChildAge(String tChildAge){
            ChildAge = tChildAge;
        }

        public String getChildAge(){
            return ChildAge;
        }

        public void setChildBirthday(Date tChildBirthday){
            ChildBirthday = tChildBirthday.toString();
        }

        public void setChildBirthday(String tChildBirthday){
            ChildBirthday = tChildBirthday;
        }

        public String getChildBirthday(){
            return ChildBirthday;
        }

        public void setPolApplyDate(Date tPolApplyDate){
            PolApplyDate = tPolApplyDate.toString();
        }

        public void setPolApplyDate(String tPolApplyDate){
            PolApplyDate = tPolApplyDate;
        }

        public String getPolApplyDate(){
            return PolApplyDate;
        }

        public void setAddInsured(String tAddInsured){
            AddInsured = tAddInsured;
        }

        public String getAddInsured(){
            return AddInsured;
        }

        public String getLoanCont() {
            return LoanCont;
        }

        public void setLoanCont(String loanCont) {
            LoanCont = loanCont;
        }


        public double getLoanMoney() {
            return LoanMoney;
        }

        public void setLoanMoney(double loanMoney) {
            LoanMoney = loanMoney;
        }


        public String getLoanVouch() {
            return LoanVouch;
        }

        public void setLoanVouch(String loanVouch) {
            LoanVouch = loanVouch;
        }

        public String getLoanEndDate() {
            return LoanEndDate;
        }

        public void setLoanEndDate(String loanEndDate) {
            LoanEndDate = loanEndDate;
        }

        public String getLoanStartDate() {
            return LoanStartDate;
        }

        public void setLoanStartDate(String loanStartDate) {
            LoanStartDate = loanStartDate;
        }



}
