package com.sinosoft.cloud.util;

import com.sinosoft.cloud.rest.TradeInfo;
import com.xiaoleilu.hutool.io.FileUtil;
import com.xiaoleilu.hutool.util.BeanUtil;
import com.xiaoleilu.hutool.util.ClassUtil;
import com.xiaoleilu.hutool.util.StrUtil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

/**
 * @author 崔广东
 * @date 2019-05-15
 * @Desc 用于将TradeInfo.toString字符串转换为TradeInfo对象
 */
public class TradeInfoUtil {


    /**
     * 需要依赖以下jar包
     * <dependency>
     * <groupId>com.xiaoleilu</groupId>
     * <artifactId>hutool-core</artifactId>
     * <version>3.1.0</version>
     * </dependency>
     */
    public static TradeInfo strToTradeInfo(String toString) throws Exception {

        String str = toString;
        TradeInfo tradeInfo = new TradeInfo();
        //pojo对应的类集合
        Set<Class<?>> pojoSet = ClassUtil.scanPackage("com.sinosoft.lis.entity");
        ArrayList<String> pojoNames = new ArrayList<>();

        //特殊的对象toString集合，主要用于存储自动生成的toString对象
        ArrayList<String> speList = new ArrayList<>();
        initSpeList(speList);


        for (Class<?> aClass : pojoSet) {
            String name = aClass.getName();
            pojoNames.add(name);
        }

        for (String pojoName : pojoNames) {
            int i = str.indexOf(pojoName);
            if (i == -1) {
                continue;
            }


            if (speList.contains(pojoName)) {
                int startIndex = str.indexOf("{", i);
                int endIndex = str.indexOf("}", i);
                String fieldValueStr = str.substring(startIndex + 1, endIndex);
                String fieldValueStrSub = fieldValueStr.replace("\'", "");
                Object pojo = TradeInfoUtil.strToPojo(fieldValueStrSub, pojoName);
                String replacedStr = str.replace(fieldValueStr, "");
                str = replacedStr;
                tradeInfo.addData(pojoName, pojo);
                continue;
            }

            char c = str.charAt(i + pojoName.length() + 1);
            if ("[".equals(String.valueOf(c))) {

                List list = new ArrayList();

                int startIndex = str.indexOf("[", i);
                //为了判断是否为空对象
                int emptyIndex = str.indexOf("]", i);
                //如果是空对象，两个索引相减=1，将空集合放入TradeInfo
                if ((emptyIndex - startIndex) == 1) {
                    tradeInfo.addData(pojoName, list);
                    continue;
                }
                int endIndex = str.indexOf("]]", i);
                //获得所有集合的字符串
                String allListStr = str.substring(startIndex + 1, endIndex + 1);
                //将所有集合的字符串全集先保存，后续用于拆分字符串
                String temp = allListStr;
                //获取集合字符串中对象的个数
                int count = StrUtil.count(allListStr, "[");
                for (int j = 0; j < count; j++) {
                    int cStartIndex = temp.indexOf("[");
                    int cEndIndex = temp.indexOf("]");
                    String fieldValueStr = StrUtil.sub(temp, cStartIndex + 1, cEndIndex);
                    Object pojo = TradeInfoUtil.strToPojo(fieldValueStr, pojoName);
                    list.add(pojo);
                    //删除每个pojo前后多余的[]
                    String fieldValueStrSub = StrUtil.sub(temp, cStartIndex, cEndIndex + 1);
                    String replacedTemp = temp.replace(fieldValueStrSub, "");
                    temp = replacedTemp;
                }
                String replacedStr = str.replace(allListStr, "");
                str = replacedStr;
                tradeInfo.addData(pojoName, list);
            } else {
                //说明是对象
                int nullIndex = str.indexOf(pojoName) + pojoName.length() + 1;
                int nullEnd = str.indexOf(pojoName) + pojoName.length() + 5;
                String nullValue = StrUtil.sub(str, nullIndex, nullEnd);
                if ("null".equals(nullValue)) {
                    tradeInfo.addData(pojoName, null);
                    continue;
                }
                //获取对象中间的索引
                int startIndex = str.indexOf("[", i);
                int endIndex = str.indexOf("]", i);
                String fieldValueStr = str.substring(startIndex + 1, endIndex);
                //获取对象的所有属性值
                Object pojo = TradeInfoUtil.strToPojo(fieldValueStr, pojoName);
                String replacedStr = str.replace(fieldValueStr, "");
                str = replacedStr;
                tradeInfo.addData(pojoName, pojo);

            }
        }
        System.out.println(tradeInfo.toString());
        return tradeInfo;
    }

    public static Object strToPojo(String fieldValueStr, String pojoName) throws Exception {
        //获取对象的所有属性值
        String[] fieldKeyValueArry = fieldValueStr.split(",");
        //创建需要转换为Bean的Map
        HashMap<String, String> beanMap = new HashMap<>();
        HashMap<Object, Object> objectObjectHashMap = new HashMap<>();

        //属性值全部内容 例如：ContNo=0
        for (String keyValue : fieldKeyValueArry) {

            String[] keyValueArray = keyValue.trim().split("=");
            //循环切下的字符串数组，拼装到map中
            String key = "";
            String value = "";
            for (int j = 0; j < keyValueArray.length; j++) {
                if (j == 0) {
                    key = keyValueArray[j];
                } else if (j == 1) {
                    if ("null".equals(keyValueArray[j])) {
                        value = null;
                    } else {
                        value = keyValueArray[j];
                    }
                }
            }
            beanMap.put(key, value);
        }
        //将map转成bean
        Object pojo = BeanUtil.mapToBeanIgnoreCase(beanMap, Class.forName(pojoName), true);
        return pojo;
    }

    /**
     *
     * @param filePath 文件全路径
     * @return
     * @throws Exception
     */
    public static TradeInfo strToTradeInfoByFilePath(String filePath) throws Exception {
        String str = FileUtil.readUtf8String(filePath);
        return strToTradeInfo(str);
    }

    public static void initSpeList(List speList) {
        speList.add("com.sinosoft.lis.entity.GlobalPojo");
    }


}
