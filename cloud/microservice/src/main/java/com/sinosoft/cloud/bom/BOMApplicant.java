package com.sinosoft.cloud.bom;

/**
 * Created by sinosoft1 on 2017/2/10.
 */
public class    BOMApplicant {
    //投保人客户号
    private String AppntNo;
    //存在与反洗钱系统黑名单姓名相同标示
    private String HaveBlacklistNameSameFlag;
    //投保年龄
    private int AppAge;
    //手机电话
    private String MobilePhone;
    //联系地址
    private String PostalAddress;
    //办公电话
    private String OfficePhone;
    //家庭住宅电话
    private String HomePhone;
    //手机号码相同的投保人人数
    private int MobilePhoneSameAppntNum;
    //家庭住宅电话相同的投保人人数
    private int HomePhoneSameAppntNum;
    //办公电话相同的投保人人数
    private int  OfficePhoneSameAppntNum;
    //名下存在贷款的相同险种保单标示
    private String HaveRiskLoanFlag;


    //证件类型
    private String IDtype;
    //国籍
    private String NativePlace;
    //投保人职业类别
    private String job;
    //投保人既往查询
    private String AppntCount;
    //投保人证件类型
    private String Results;
    //投保人告知收入
    private double AppntIncome;
    //客户住宅电话出现次数
    private String HomePhoneSum;
    //客户办公电话出现次数
    private String CompanyPhoneSum;
    //客户移动电话出现次数
    private String MobileSum;
    //住宅电话相同人数
    private String Hcount;
    //移动电话相同人数
    private String Mcount;
    //办公电话相同人数
    private String Ccount;

    public String getCcount() {
        return Ccount;
    }

    public void setCcount(String ccount) {
        Ccount = ccount;
    }

    public String getHcount() {
        return Hcount;
    }

    public void setHcount(String hcount) {
        Hcount = hcount;
    }

    public String getMcount() {
        return Mcount;
    }

    public void setMcount(String mcount) {
        Mcount = mcount;
    }

    public String getAppntCount() {
        return AppntCount;
    }

    public void setAppntCount(String appntCount) {
        AppntCount = appntCount;
    }

    public double getAppntIncome() {
        return AppntIncome;
    }

    public void setAppntIncome(double appntIncome) {
        AppntIncome = appntIncome;
    }

    public String getCompanyPhoneSum() {
        return CompanyPhoneSum;
    }

    public void setCompanyPhoneSum(String companyPhoneSum) {
        CompanyPhoneSum = companyPhoneSum;
    }

    public String getHomePhoneSum() {
        return HomePhoneSum;
    }

    public void setHomePhoneSum(String homePhoneSum) {
        HomePhoneSum = homePhoneSum;
    }

    public String getIDtype() {
        return IDtype;
    }

    public void setIDtype(String IDtype) {
        this.IDtype = IDtype;
    }

    public String getJob() {
        return job;
    }

    public void setJob(String job) {
        this.job = job;
    }

    public String getMobileSum() {
        return MobileSum;
    }

    public void setMobileSum(String mobileSum) {
        MobileSum = mobileSum;
    }

    public String getNativePlace() {
        return NativePlace;
    }

    public void setNativePlace(String nativePlace) {
        NativePlace = nativePlace;
    }

    public String getResults() {
        return Results;
    }

    public void setResults(String results) {
        Results = results;
    }


    public String getAppntNo() {
        return AppntNo;
    }

    public void setAppntNo(String appntNo) {
        AppntNo = appntNo;
    }

    public String getHaveBlacklistNameSameFlag() {
        return HaveBlacklistNameSameFlag;
    }

    public void setHaveBlacklistNameSameFlag(String haveBlacklistNameSameFlag) {
        HaveBlacklistNameSameFlag = haveBlacklistNameSameFlag;
    }

    public int getAppAge() {
        return AppAge;
    }

    public void setAppAge(int appAge) {
        AppAge = appAge;
    }

    public String getMobilePhone() {
        return MobilePhone;
    }

    public void setMobilePhone(String mobilePhone) {
        MobilePhone = mobilePhone;
    }

    public String getPostalAddress() {
        return PostalAddress;
    }

    public void setPostalAddress(String postalAddress) {
        PostalAddress = postalAddress;
    }

    public String getOfficePhone() {
        return OfficePhone;
    }

    public void setOfficePhone(String officePhone) {
        OfficePhone = officePhone;
    }

    public String getHomePhone() {
        return HomePhone;
    }

    public void setHomePhone(String homePhone) {
        HomePhone = homePhone;
    }

    public int getMobilePhoneSameAppntNum() {
        return MobilePhoneSameAppntNum;
    }

    public void setMobilePhoneSameAppntNum(int mobilePhoneSameAppntNum) {
        MobilePhoneSameAppntNum = mobilePhoneSameAppntNum;
    }

    public int getHomePhoneSameAppntNum() {
        return HomePhoneSameAppntNum;
    }

    public void setHomePhoneSameAppntNum(int homePhoneSameAppntNum) {
        HomePhoneSameAppntNum = homePhoneSameAppntNum;
    }

    public int getOfficePhoneSameAppntNum() {
        return OfficePhoneSameAppntNum;
    }

    public void setOfficePhoneSameAppntNum(int officePhoneSameAppntNum) {
        OfficePhoneSameAppntNum = officePhoneSameAppntNum;
    }

    public String getHaveRiskLoanFlag() {
        return HaveRiskLoanFlag;
    }

    public void setHaveRiskLoanFlag(String haveRiskLoanFlag) {
        HaveRiskLoanFlag = haveRiskLoanFlag;
    }
}
