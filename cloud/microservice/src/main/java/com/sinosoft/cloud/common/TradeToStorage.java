package com.sinosoft.cloud.common;
/**
 * Created by mali on 2017/1/10.
 */

import com.sinosoft.cloud.rest.TradeInfo;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.utility.*;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * @description： 把交易数据转换成数据库可操作对象
 * @author： mali
 * @create： 2017-01-10 23:40
 **/

public class TradeToStorage {
    private final Log logger = LogFactory.getLog(getClass());

    /**
     *
     *
     * @param tradeInfo tradeInfo
     * @return 返回值
     * @throws Exception 异常
     */

    public VData transfer(TradeInfo tradeInfo) throws Exception {
        HashMap<String, Object> tradeMap = tradeInfo.getTradeMap();
        LinkedHashMap<String, String> storageMap = tradeInfo.getStorageMap();
        VData vData = new VData();
        MMap mMap = new MMap();
        if (tradeMap != null && tradeMap.size() > 0 && storageMap != null && storageMap.size() > 0) {
            Iterator<String> iterator = storageMap.keySet().iterator();
            while (iterator.hasNext()) {
                String key = iterator.next();
                if (tradeMap.containsKey(key)) {
                    Object objSrc = tradeMap.get(key);
                    Object objDes = null;
                    if (objSrc instanceof Pojo) {
                        objDes = this.transferSchema(objSrc);
                    } else if (objSrc instanceof List) {
                        List listSrc = (List) objSrc;
                        if (listSrc != null && listSrc.size() > 0) {
                            String className = null;
                            String tableName = null;
                            className = listSrc.get(0).getClass().getName();
                            tableName = className.substring(className
                                    .lastIndexOf(".") + 1, className
                                    .lastIndexOf("P"));
                            className = "com.sinosoft.lis.vschema." + tableName + "Set";
                            Class schemaClass = Class.forName(className);
                            SchemaSet schemaSet = (SchemaSet) schemaClass.newInstance();
                            for (int i = 0; i < listSrc.size(); i++) {
                                schemaSet.add(this.transferSchema(listSrc.get(i)));
                            }
                            objDes = schemaSet;
                        }
                    }
                    mMap.put(objDes, storageMap.get(key));
                }
            }
            if (mMap != null && mMap.size() > 0) {
                vData.add(mMap);
            }
        }
        return vData;
    }

    /**
     *
     * @param obj obj
     * @return 返回值
     * @throws Exception 异常
     */
    private Schema transferSchema(Object obj) throws Exception {
        String className = null;
        String tableName = null;
        Schema schema = null;
        Reflections reflections = new Reflections();
        if (obj instanceof Pojo) {
            className = obj.getClass().getName();
            tableName = className.substring(className
                    .lastIndexOf(".") + 1, className
                    .lastIndexOf("P"));
            className = "com.sinosoft.lis.schema." + tableName + "Schema";
            Class schemaClass = Class.forName(className);
            schema = (Schema) schemaClass.newInstance();
            reflections.transFields(schema, obj);
        }
        return schema;
    }
}
