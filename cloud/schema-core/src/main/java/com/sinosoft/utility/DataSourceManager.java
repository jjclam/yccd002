package com.sinosoft.utility;

import com.mchange.v2.c3p0.ComboPooledDataSource;
import com.sinosoft.cloud.common.SpringContextUtils;
import org.springframework.stereotype.Component;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import javax.sql.DataSource;
import java.util.List;

/**
 * 数据源管理功能
 * Created by yangming on 16/9/11.
 * modify by Kaine WOO on 16/12/29
 */
@Component
public class DataSourceManager {
    private static final Log logger = LogFactory.getLog(DataSourceManager.class.getName());

    private List<DataSource> dataSourceList;
    private ThreadLocal<DataSource> dataSourceThreadLocal = new ThreadLocal<>();


    /**
     * 获取可用数据源列表
     *
     * @return
     */
    public List<DataSource> getDataSourceList() {
        return dataSourceList;
    }


    /**
     * 设置当前使用的数据源
     *
     * @param num 数据源列表下标
     */
    public void setCurrentThreadDataSource(int num) {
        DataSource dataSource = get(num);
        setCurrentThreadDataSource(dataSource);
    }


    /**
     * 设置当前使用的数据源
     *
     * @param dataSourceName 数据源名称
     */
    public void setCurrentThreadDataSource(String dataSourceName) {
        for (DataSource dsComboPooled : dataSourceList) {
            if (((ComboPooledDataSource) dsComboPooled).getDescription().equalsIgnoreCase(dataSourceName)) {
                setCurrentThreadDataSource(dsComboPooled);

                logger.debug("设置成功！数据源名称：" + dataSourceName);
                return;
            }
        }

        logger.error("设置失败！数据源名称：" + dataSourceName);
    }

    /**
     * 返回受管理的数据源数量
     *
     * @return
     */
    public int getDataSourceCount() {
        if (dataSourceList == null) {
            logger.error("未找到数据源管理对象，请检查！");
            return 0;
        }
        return dataSourceList.size();
    }

    /**
     * 查询数据源对应Url
     *
     * @param idxIndex 数据源索引序号
     * @return "ERROR"表示没有该索引对应的数据源
     */
    public String getUrlByDSIndex(int idxIndex) {
        if (dataSourceList == null) {
            logger.error("未找到数据源管理对象，请检查！");
            return "ERROR";
        }
        if (idxIndex < 0 || idxIndex >= dataSourceList.size()) {
            logger.debug("未找到该索引序号对应的数据源：" + idxIndex);
            return "ERROR";
        }
        ComboPooledDataSource ds = (ComboPooledDataSource) dataSourceList.get(idxIndex);
        return ds.getJdbcUrl();
    }

    /**
     * 返回当前使用的数据源
     *
     * @return
     */
    public DataSource getCurrentThreadDataSource() {
        DataSource dataSource = getDataSource();
        if (dataSource == null) {
            dataSource = SpringContextUtils.getBeanByClass(DataSource.class);
        }
        return dataSource;
    }

    /**
     * 设置当前使用的数据源
     *
     * @param dataSource 数据源
     */
    public void setCurrentThreadDataSource(DataSource dataSource) {
        dataSourceThreadLocal.set(dataSource);
    }

    /**
     * 关闭正在使用的所有数据源
     */
    public void destroy() {
        for (DataSource dataSource : dataSourceList) {
            if (dataSource instanceof ComboPooledDataSource) {
                ((ComboPooledDataSource) dataSource).close();
            }
        }
    }

    /**
     * 获取当前正在使用的数据源
     *
     * @return
     */
    private DataSource getDataSource() {
        return dataSourceThreadLocal.get();
    }

    /**
     * 查找指定的数据源
     *
     * @param num 数据源列表下标
     * @return
     */
    private DataSource get(int num) {
        if (dataSourceList == null || dataSourceList.size() < num + 1) {
            return null;
        }
        return dataSourceList.get(num);
    }
}
