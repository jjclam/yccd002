/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */
package com.sinosoft.utility;

import com.sinosoft.cloud.common.SpringContextUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;

/*
 * <p>ClassName: dbConnsPool </p> <p>Description: 数据库连接池 </p> <p>Copyright:
 * Copyright (c) 2002</p> <p>Company: sinosoft </p> @Database: LIS
 * @CreateDate：2002-10-04
 */

public class DBConnPool {
    private static Log logger = LogFactory.getLog(DBConnPool.class);


    private static DataSourceManager dataSourceManager;


    // 构建函数
    private DBConnPool() {
    }

    /**
     * 获取连接
     *
     * @return DBConn
     */
    static public DBConn getConnection() {
        DataSource dataSource;
        Date startDate = new Date();
        logger.info("获取数据库连接开始startDate="+startDate);
        if (dataSourceManager == null) {
            dataSourceManager = SpringContextUtils.getBeanByClass(DataSourceManager.class);
        }
        dataSource = dataSourceManager.getCurrentThreadDataSource();
        try {
            DBConn tDBConn = new DBConn(dataSource.getConnection());
            //添加判断数据库类型，若为oracle，则需要设置当前会话的时间格式
            setDBConnParam(tDBConn);
            Date endDate = new Date();
            logger.info("获取数据库连接开始endDate="+endDate);
            logger.info("获取数据库花费的时间="+(endDate.getTime()-startDate.getTime())+"毫秒");
            return tDBConn;
        } catch (SQLException e) {
            e.printStackTrace();
            Date endDate = new Date();
            logger.info("获取数据库连接开始endDate="+endDate);
            logger.info("获取数据库花费的时间="+(endDate.getTime()-startDate.getTime())+"毫秒");
            logger.error("数据库连接失败", e);
        }
        return null;
    }
    static public DBConn getConnection(String c3poBeanId) {
        DataSource dataSource;
        Date startDate = new Date();
        logger.info("获取数据库连接开始startDate="+startDate);
        dataSource = (DataSource) SpringContextUtils.getBeanById(c3poBeanId);
        try {
            DBConn tDBConn = new DBConn(dataSource.getConnection());
            //添加判断数据库类型，若为oracle，则需要设置当前会话的时间格式
            setDBConnParam(tDBConn);
            Date endDate = new Date();
            logger.info("获取数据库连接开始endDate="+endDate);
            logger.info("获取数据库花费的时间="+(endDate.getTime()-startDate.getTime())+"毫秒");
            return tDBConn;
        } catch (SQLException e) {
            Date endDate = new Date();
            logger.info("获取数据库连接开始endDate="+endDate);
            logger.info("获取数据库花费的时间="+(endDate.getTime()-startDate.getTime())+"毫秒");
            e.printStackTrace();
            return null;
        }
    }
    /**
     * 设置数据库连接参数
     * 如oracle数据库需设置时间格式
     */
    private static void setDBConnParam(DBConn tDBConn) {
        Statement stmt = null;
        try {
            if (tDBConn.getMetaData().getDriverName().toUpperCase().indexOf("ORACLE") != -1) {
                stmt = tDBConn.createStatement(
                        ResultSet.TYPE_SCROLL_SENSITIVE,
                        ResultSet.CONCUR_UPDATABLE);
                stmt.execute("alter session set nls_date_format = 'YYYY-MM-DD HH24:MI:SS'");
                stmt.close();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
