package com.sinosoft.utility;

/**
 * Created by yangming on 2016/11/29.
 */
public interface Pojo {
    // 按名字或索引返回值
    String getV(String FCode);

    String getV(int nIndex);

    // 按名字或索引返回字段的类型，如Schema.TYPE_STRING
    int getFieldType(String strFieldName);

    int getFieldType(int nFieldIndex);

    // 得到字段数
    int getFieldCount();

    // 名字和索引互查
    int getFieldIndex(String strFieldName);

    String getFieldName(int nFieldIndex);

    // 按照名字设置
    boolean setV(String strFieldName, String strFieldValue);
}
