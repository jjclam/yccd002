package com.sinosoft.utility;



import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * 异常日志输出处理
 * Created by kaine on 2016/8/2.
 */
public class ExceptionUtils {
    private static final Log log4j2 = LogFactory.getLog(ExceptionUtils.class);

    /**
     * 用于处理日志输出中的异常堆栈输出问题
     *
     * @param exception 异常对象
     * @return 异常堆栈字符串
     */
    public static String exceptionToString(Throwable exception) {
        ByteArrayOutputStream buf = new ByteArrayOutputStream();
        exception.printStackTrace(new PrintWriter(buf, true));
        String exceptionMessage = buf.toString();
        if (buf != null) {
            try {
                buf.close();
            } catch (IOException ex) {
            }
        }
        return exceptionMessage;
    }


    /**
     * 用于处理日志输出中的异常堆栈输出问题
     *
     * @param exception 异常对象
     * @return 异常堆栈字符串
     */
    public static String exceptionToString(Exception exception) {
        ByteArrayOutputStream buf = new ByteArrayOutputStream();
        exception.printStackTrace(new PrintWriter(buf, true));
        String exceptionMessage = buf.toString();
        if (buf != null) {
            try {
                buf.close();
            } catch (IOException ex) {
            }
        }
        return exceptionMessage;
    }

    /**
     * 使用error等级一次性输出异常堆栈
     *
     * @param logger    日志记录器，由调用类提供
     * @param throwable 异常对象
     */
    public static void loggerExcaption(Log logger, Throwable throwable) {
        ByteArrayOutputStream buf = new ByteArrayOutputStream();
        throwable.printStackTrace(new PrintWriter(buf, true));
        logger.error(buf.toString());
        if (buf != null) {
            try {
                buf.close();
            } catch (IOException ex) {
                log4j2.error("输出throwable堆栈发生异常！原throwable对象：{}", throwable);
            }
        }
    }

    /**
     * 使用error等级一次性输出异常堆栈（为支持Log4j 1.x版本）
     *
     * @param logger    日志记录器，由调用类提供
     * @param throwable 异常对象
     */
    public static void loggerExcaption(org.apache.log4j.Logger logger, Throwable throwable) {
        ByteArrayOutputStream buf = new ByteArrayOutputStream();
        throwable.printStackTrace(new PrintWriter(buf, true));
        logger.error(buf.toString());
        if (buf != null) {
            try {
                buf.close();
            } catch (IOException ex) {
                log4j2.error("输出throwable堆栈发生异常！原throwable对象：{}", throwable);
            }
        }
    }
}