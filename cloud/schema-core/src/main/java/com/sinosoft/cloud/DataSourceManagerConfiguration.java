package com.sinosoft.cloud;

import com.sinosoft.utility.DataSourceManager;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.context.annotation.Bean;

/**
 * DataSourceg初始化管理
 */
@SpringBootConfiguration
public class DataSourceManagerConfiguration {

    /**
     * 重新获取datasouce
     * @return DataSouceManager
     */
    @Bean
    public DataSourceManager dataSourceManager() {
        DataSourceManager dataSourceManager = new DataSourceManager();
        return dataSourceManager;
    }
}
