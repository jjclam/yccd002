/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.vdb;

import com.sinosoft.lis.vschema.LCContSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.DBConnPool;
import com.sinosoft.utility.DBOper;
import com.sinosoft.utility.SQLString;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;

/*
 * <p>ClassName: LCContDBSet </p>
 * <p>Description: DB层多记录数据库操作类文件 </p>
 * <p>Copyright: Copyright (c) 2007</p>
 * <p>Company: sinosoft </p>
 * @Database: Physical Data _1
 */
public class LCContDBSet extends LCContSet
{
	// @Field
	private Connection con;
	private DBOper db;
	/**
	* flag = true: 传入Connection
	* flag = false: 不传入Connection
	**/
	private boolean mflag = false;


	// @Constructor
	public LCContDBSet(Connection tConnection)
	{
		con = tConnection;
		db = new DBOper(con,"LCCont");
		mflag = true;
	}

	public LCContDBSet()
	{
		db = new DBOper( "LCCont" );
	}
	// @Method
	public boolean deleteSQL()
	{
		if (db.deleteSQL(this))
		{
		        return true;
		}
		else
		{
			// @@错误处理
			this.mErrors.copyAllErrors(db.mErrors);
			CError tError = new CError();
			tError.moduleName = "LCContDBSet";
			tError.functionName = "deleteSQL";
			tError.errorMessage = "操作失败!";
			this.mErrors .addOneError(tError);
			return false;
		}
	}

    /**
     * 删除操作
     * 删除条件：主键
     * @return boolean
     */
	public boolean delete()
	{
		PreparedStatement pstmt = null;

		if( !mflag ) {
			con = DBConnPool.getConnection();
		}

		try
		{
            int tCount = this.size();
			pstmt = con.prepareStatement("DELETE FROM LCCont WHERE  ContNo = ?");
            for (int i = 1; i <= tCount; i++)
            {
			if(this.get(i).getContNo() == null || this.get(i).getContNo().equals("")) {
				pstmt.setString(1,null);
			} else {
				pstmt.setString(1, this.get(i).getContNo());
			}

		// only for debug purpose
		SQLString sqlObj = new SQLString("LCCont");
		sqlObj.setSQL(4, this.get(i));
		sqlObj.getSQL();

                pstmt.addBatch();
            }
            pstmt.executeBatch();
			pstmt.close();
		} catch (Exception ex) {
			// @@错误处理
ex.printStackTrace();
			this.mErrors.copyAllErrors(db.mErrors);
			CError tError = new CError();
			tError.moduleName = "LCContDBSet";
			tError.functionName = "delete()";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			try {
				pstmt.close();
			} catch (Exception e){e.printStackTrace();}

			if( !mflag ) {
				try {
					con.close();
				} catch (Exception e){e.printStackTrace();}
			}

			return false;
		}

		if( !mflag ) {
			try {
				con.close();
			} catch (Exception e){e.printStackTrace();}
		}

		return true;
	}

    /**
     * 更新操作
     * 更新条件：主键
     * @return boolean
     */
	public boolean update()
	{
		PreparedStatement pstmt = null;

		if( !mflag ) {
			con = DBConnPool.getConnection();
		}

		try
		{
            int tCount = this.size();
			pstmt = con.prepareStatement("UPDATE LCCont SET  GrpContNo = ? , ContNo = ? , ProposalContNo = ? , PrtNo = ? , ContType = ? , FamilyType = ? , FamilyID = ? , PolType = ? , CardFlag = ? , ManageCom = ? , ExecuteCom = ? , AgentCom = ? , AgentCode = ? , AgentGroup = ? , AgentCode1 = ? , AgentType = ? , SaleChnl = ? , Handler = ? , Password = ? , AppntNo = ? , AppntName = ? , AppntSex = ? , AppntBirthday = ? , AppntIDType = ? , AppntIDNo = ? , InsuredNo = ? , InsuredName = ? , InsuredSex = ? , InsuredBirthday = ? , InsuredIDType = ? , InsuredIDNo = ? , PayIntv = ? , PayMode = ? , PayLocation = ? , DisputedFlag = ? , OutPayFlag = ? , GetPolMode = ? , SignCom = ? , SignDate = ? , SignTime = ? , ConsignNo = ? , BankCode = ? , BankAccNo = ? , AccName = ? , PrintCount = ? , LostTimes = ? , Lang = ? , Currency = ? , Remark = ? , Peoples = ? , Mult = ? , Prem = ? , Amnt = ? , SumPrem = ? , Dif = ? , PaytoDate = ? , FirstPayDate = ? , CValiDate = ? , InputOperator = ? , InputDate = ? , InputTime = ? , ApproveFlag = ? , ApproveCode = ? , ApproveDate = ? , ApproveTime = ? , UWFlag = ? , UWOperator = ? , UWDate = ? , UWTime = ? , AppFlag = ? , PolApplyDate = ? , GetPolDate = ? , GetPolTime = ? , CustomGetPolDate = ? , State = ? , Operator = ? , MakeDate = ? , MakeTime = ? , ModifyDate = ? , ModifyTime = ? , FirstTrialOperator = ? , FirstTrialDate = ? , FirstTrialTime = ? , ReceiveOperator = ? , ReceiveDate = ? , ReceiveTime = ? , TempFeeNo = ? , SellType = ? , ForceUWFlag = ? , ForceUWReason = ? , NewBankCode = ? , NewBankAccNo = ? , NewAccName = ? , NewPayMode = ? , AgentBankCode = ? , BankAgent = ? , AutoPayFlag = ? , RnewFlag = ? , FamilyContNo = ? , BussFlag = ? , SignName = ? , OrganizeDate = ? , OrganizeTime = ? , NewAutoSendBankFlag = ? , AgentCodeOper = ? , AgentCodeAssi = ? , DelayReasonCode = ? , DelayReasonDesc = ? , XQremindflag = ? , OrganComCode = ? , ContFlag = ? , SaleDepart = ? , ChnlType = ? , RenewCount = ? , RenewContNo = ? , InitNumPeople = ? , InitMult = ? , InitAmnt = ? , InitRiskAmnt = ? , InitPrem = ? , InitStandPrem = ? , RiskAmnt = ? , StandPrem = ? , SumNumPeople = ? , SumPay = ? , EndDate = ? , PayEndDate = ? , AccKind = ? , SignOperator = ? , LoanStartDate = ? , LoanEndDate = ? , AgentBranchesCode = ? , NeedBillFlag = ? , ComCode = ? , ModifyOperator = ? , ContPrintFlag = ? WHERE  ContNo = ?");
            for (int i = 1; i <= tCount; i++)
            {
			if(this.get(i).getGrpContNo() == null || this.get(i).getGrpContNo().equals("")) {
				pstmt.setString(1,null);
			} else {
				pstmt.setString(1, this.get(i).getGrpContNo());
			}
			if(this.get(i).getContNo() == null || this.get(i).getContNo().equals("")) {
				pstmt.setString(2,null);
			} else {
				pstmt.setString(2, this.get(i).getContNo());
			}
			if(this.get(i).getProposalContNo() == null || this.get(i).getProposalContNo().equals("")) {
				pstmt.setString(3,null);
			} else {
				pstmt.setString(3, this.get(i).getProposalContNo());
			}
			if(this.get(i).getPrtNo() == null || this.get(i).getPrtNo().equals("")) {
				pstmt.setString(4,null);
			} else {
				pstmt.setString(4, this.get(i).getPrtNo());
			}
			if(this.get(i).getContType() == null || this.get(i).getContType().equals("")) {
				pstmt.setString(5,null);
			} else {
				pstmt.setString(5, this.get(i).getContType());
			}
			if(this.get(i).getFamilyType() == null || this.get(i).getFamilyType().equals("")) {
				pstmt.setString(6,null);
			} else {
				pstmt.setString(6, this.get(i).getFamilyType());
			}
			if(this.get(i).getFamilyID() == null || this.get(i).getFamilyID().equals("")) {
				pstmt.setString(7,null);
			} else {
				pstmt.setString(7, this.get(i).getFamilyID());
			}
			if(this.get(i).getPolType() == null || this.get(i).getPolType().equals("")) {
				pstmt.setString(8,null);
			} else {
				pstmt.setString(8, this.get(i).getPolType());
			}
			if(this.get(i).getCardFlag() == null || this.get(i).getCardFlag().equals("")) {
				pstmt.setString(9,null);
			} else {
				pstmt.setString(9, this.get(i).getCardFlag());
			}
			if(this.get(i).getManageCom() == null || this.get(i).getManageCom().equals("")) {
				pstmt.setString(10,null);
			} else {
				pstmt.setString(10, this.get(i).getManageCom());
			}
			if(this.get(i).getExecuteCom() == null || this.get(i).getExecuteCom().equals("")) {
				pstmt.setString(11,null);
			} else {
				pstmt.setString(11, this.get(i).getExecuteCom());
			}
			if(this.get(i).getAgentCom() == null || this.get(i).getAgentCom().equals("")) {
				pstmt.setString(12,null);
			} else {
				pstmt.setString(12, this.get(i).getAgentCom());
			}
			if(this.get(i).getAgentCode() == null || this.get(i).getAgentCode().equals("")) {
				pstmt.setString(13,null);
			} else {
				pstmt.setString(13, this.get(i).getAgentCode());
			}
			if(this.get(i).getAgentGroup() == null || this.get(i).getAgentGroup().equals("")) {
				pstmt.setString(14,null);
			} else {
				pstmt.setString(14, this.get(i).getAgentGroup());
			}
			if(this.get(i).getAgentCode1() == null || this.get(i).getAgentCode1().equals("")) {
				pstmt.setString(15,null);
			} else {
				pstmt.setString(15, this.get(i).getAgentCode1());
			}
			if(this.get(i).getAgentType() == null || this.get(i).getAgentType().equals("")) {
				pstmt.setString(16,null);
			} else {
				pstmt.setString(16, this.get(i).getAgentType());
			}
			if(this.get(i).getSaleChnl() == null || this.get(i).getSaleChnl().equals("")) {
				pstmt.setString(17,null);
			} else {
				pstmt.setString(17, this.get(i).getSaleChnl());
			}
			if(this.get(i).getHandler() == null || this.get(i).getHandler().equals("")) {
				pstmt.setString(18,null);
			} else {
				pstmt.setString(18, this.get(i).getHandler());
			}
			if(this.get(i).getPassword() == null || this.get(i).getPassword().equals("")) {
				pstmt.setString(19,null);
			} else {
				pstmt.setString(19, this.get(i).getPassword());
			}
			if(this.get(i).getAppntNo() == null || this.get(i).getAppntNo().equals("")) {
				pstmt.setString(20,null);
			} else {
				pstmt.setString(20, this.get(i).getAppntNo());
			}
			if(this.get(i).getAppntName() == null || this.get(i).getAppntName().equals("")) {
				pstmt.setString(21,null);
			} else {
				pstmt.setString(21, this.get(i).getAppntName());
			}
			if(this.get(i).getAppntSex() == null || this.get(i).getAppntSex().equals("")) {
				pstmt.setString(22,null);
			} else {
				pstmt.setString(22, this.get(i).getAppntSex());
			}
			if(this.get(i).getAppntBirthday() == null || this.get(i).getAppntBirthday().equals("")) {
				pstmt.setDate(23,null);
			} else {
				pstmt.setDate(23, Date.valueOf(this.get(i).getAppntBirthday()));
			}
			if(this.get(i).getAppntIDType() == null || this.get(i).getAppntIDType().equals("")) {
				pstmt.setString(24,null);
			} else {
				pstmt.setString(24, this.get(i).getAppntIDType());
			}
			if(this.get(i).getAppntIDNo() == null || this.get(i).getAppntIDNo().equals("")) {
				pstmt.setString(25,null);
			} else {
				pstmt.setString(25, this.get(i).getAppntIDNo());
			}
			if(this.get(i).getInsuredNo() == null || this.get(i).getInsuredNo().equals("")) {
				pstmt.setString(26,null);
			} else {
				pstmt.setString(26, this.get(i).getInsuredNo());
			}
			if(this.get(i).getInsuredName() == null || this.get(i).getInsuredName().equals("")) {
				pstmt.setString(27,null);
			} else {
				pstmt.setString(27, this.get(i).getInsuredName());
			}
			if(this.get(i).getInsuredSex() == null || this.get(i).getInsuredSex().equals("")) {
				pstmt.setString(28,null);
			} else {
				pstmt.setString(28, this.get(i).getInsuredSex());
			}
			if(this.get(i).getInsuredBirthday() == null || this.get(i).getInsuredBirthday().equals("")) {
				pstmt.setDate(29,null);
			} else {
				pstmt.setDate(29, Date.valueOf(this.get(i).getInsuredBirthday()));
			}
			if(this.get(i).getInsuredIDType() == null || this.get(i).getInsuredIDType().equals("")) {
				pstmt.setString(30,null);
			} else {
				pstmt.setString(30, this.get(i).getInsuredIDType());
			}
			if(this.get(i).getInsuredIDNo() == null || this.get(i).getInsuredIDNo().equals("")) {
				pstmt.setString(31,null);
			} else {
				pstmt.setString(31, this.get(i).getInsuredIDNo());
			}
			pstmt.setInt(32, this.get(i).getPayIntv());
			if(this.get(i).getPayMode() == null || this.get(i).getPayMode().equals("")) {
				pstmt.setString(33,null);
			} else {
				pstmt.setString(33, this.get(i).getPayMode());
			}
			if(this.get(i).getPayLocation() == null || this.get(i).getPayLocation().equals("")) {
				pstmt.setString(34,null);
			} else {
				pstmt.setString(34, this.get(i).getPayLocation());
			}
			if(this.get(i).getDisputedFlag() == null || this.get(i).getDisputedFlag().equals("")) {
				pstmt.setString(35,null);
			} else {
				pstmt.setString(35, this.get(i).getDisputedFlag());
			}
			if(this.get(i).getOutPayFlag() == null || this.get(i).getOutPayFlag().equals("")) {
				pstmt.setString(36,null);
			} else {
				pstmt.setString(36, this.get(i).getOutPayFlag());
			}
			if(this.get(i).getGetPolMode() == null || this.get(i).getGetPolMode().equals("")) {
				pstmt.setString(37,null);
			} else {
				pstmt.setString(37, this.get(i).getGetPolMode());
			}
			if(this.get(i).getSignCom() == null || this.get(i).getSignCom().equals("")) {
				pstmt.setString(38,null);
			} else {
				pstmt.setString(38, this.get(i).getSignCom());
			}
			if(this.get(i).getSignDate() == null || this.get(i).getSignDate().equals("")) {
				pstmt.setDate(39,null);
			} else {
				pstmt.setDate(39, Date.valueOf(this.get(i).getSignDate()));
			}
			if(this.get(i).getSignTime() == null || this.get(i).getSignTime().equals("")) {
				pstmt.setString(40,null);
			} else {
				pstmt.setString(40, this.get(i).getSignTime());
			}
			if(this.get(i).getConsignNo() == null || this.get(i).getConsignNo().equals("")) {
				pstmt.setString(41,null);
			} else {
				pstmt.setString(41, this.get(i).getConsignNo());
			}
			if(this.get(i).getBankCode() == null || this.get(i).getBankCode().equals("")) {
				pstmt.setString(42,null);
			} else {
				pstmt.setString(42, this.get(i).getBankCode());
			}
			if(this.get(i).getBankAccNo() == null || this.get(i).getBankAccNo().equals("")) {
				pstmt.setString(43,null);
			} else {
				pstmt.setString(43, this.get(i).getBankAccNo());
			}
			if(this.get(i).getAccName() == null || this.get(i).getAccName().equals("")) {
				pstmt.setString(44,null);
			} else {
				pstmt.setString(44, this.get(i).getAccName());
			}
			pstmt.setInt(45, this.get(i).getPrintCount());
			pstmt.setInt(46, this.get(i).getLostTimes());
			if(this.get(i).getLang() == null || this.get(i).getLang().equals("")) {
				pstmt.setString(47,null);
			} else {
				pstmt.setString(47, this.get(i).getLang());
			}
			if(this.get(i).getCurrency() == null || this.get(i).getCurrency().equals("")) {
				pstmt.setString(48,null);
			} else {
				pstmt.setString(48, this.get(i).getCurrency());
			}
			if(this.get(i).getRemark() == null || this.get(i).getRemark().equals("")) {
				pstmt.setString(49,null);
			} else {
				pstmt.setString(49, this.get(i).getRemark());
			}
			pstmt.setInt(50, this.get(i).getPeoples());
			pstmt.setDouble(51, this.get(i).getMult());
			pstmt.setDouble(52, this.get(i).getPrem());
			pstmt.setDouble(53, this.get(i).getAmnt());
			pstmt.setDouble(54, this.get(i).getSumPrem());
			pstmt.setDouble(55, this.get(i).getDif());
			if(this.get(i).getPaytoDate() == null || this.get(i).getPaytoDate().equals("")) {
				pstmt.setDate(56,null);
			} else {
				pstmt.setDate(56, Date.valueOf(this.get(i).getPaytoDate()));
			}
			if(this.get(i).getFirstPayDate() == null || this.get(i).getFirstPayDate().equals("")) {
				pstmt.setDate(57,null);
			} else {
				pstmt.setDate(57, Date.valueOf(this.get(i).getFirstPayDate()));
			}
			if(this.get(i).getCValiDate() == null || this.get(i).getCValiDate().equals("")) {
				pstmt.setDate(58,null);
			} else {
				pstmt.setDate(58, Date.valueOf(this.get(i).getCValiDate()));
			}
			if(this.get(i).getInputOperator() == null || this.get(i).getInputOperator().equals("")) {
				pstmt.setString(59,null);
			} else {
				pstmt.setString(59, this.get(i).getInputOperator());
			}
			if(this.get(i).getInputDate() == null || this.get(i).getInputDate().equals("")) {
				pstmt.setDate(60,null);
			} else {
				pstmt.setDate(60, Date.valueOf(this.get(i).getInputDate()));
			}
			if(this.get(i).getInputTime() == null || this.get(i).getInputTime().equals("")) {
				pstmt.setString(61,null);
			} else {
				pstmt.setString(61, this.get(i).getInputTime());
			}
			if(this.get(i).getApproveFlag() == null || this.get(i).getApproveFlag().equals("")) {
				pstmt.setString(62,null);
			} else {
				pstmt.setString(62, this.get(i).getApproveFlag());
			}
			if(this.get(i).getApproveCode() == null || this.get(i).getApproveCode().equals("")) {
				pstmt.setString(63,null);
			} else {
				pstmt.setString(63, this.get(i).getApproveCode());
			}
			if(this.get(i).getApproveDate() == null || this.get(i).getApproveDate().equals("")) {
				pstmt.setDate(64,null);
			} else {
				pstmt.setDate(64, Date.valueOf(this.get(i).getApproveDate()));
			}
			if(this.get(i).getApproveTime() == null || this.get(i).getApproveTime().equals("")) {
				pstmt.setString(65,null);
			} else {
				pstmt.setString(65, this.get(i).getApproveTime());
			}
			if(this.get(i).getUWFlag() == null || this.get(i).getUWFlag().equals("")) {
				pstmt.setString(66,null);
			} else {
				pstmt.setString(66, this.get(i).getUWFlag());
			}
			if(this.get(i).getUWOperator() == null || this.get(i).getUWOperator().equals("")) {
				pstmt.setString(67,null);
			} else {
				pstmt.setString(67, this.get(i).getUWOperator());
			}
			if(this.get(i).getUWDate() == null || this.get(i).getUWDate().equals("")) {
				pstmt.setDate(68,null);
			} else {
				pstmt.setDate(68, Date.valueOf(this.get(i).getUWDate()));
			}
			if(this.get(i).getUWTime() == null || this.get(i).getUWTime().equals("")) {
				pstmt.setString(69,null);
			} else {
				pstmt.setString(69, this.get(i).getUWTime());
			}
			if(this.get(i).getAppFlag() == null || this.get(i).getAppFlag().equals("")) {
				pstmt.setString(70,null);
			} else {
				pstmt.setString(70, this.get(i).getAppFlag());
			}
			if(this.get(i).getPolApplyDate() == null || this.get(i).getPolApplyDate().equals("")) {
				pstmt.setDate(71,null);
			} else {
				pstmt.setDate(71, Date.valueOf(this.get(i).getPolApplyDate()));
			}
			if(this.get(i).getGetPolDate() == null || this.get(i).getGetPolDate().equals("")) {
				pstmt.setDate(72,null);
			} else {
				pstmt.setDate(72, Date.valueOf(this.get(i).getGetPolDate()));
			}
			if(this.get(i).getGetPolTime() == null || this.get(i).getGetPolTime().equals("")) {
				pstmt.setString(73,null);
			} else {
				pstmt.setString(73, this.get(i).getGetPolTime());
			}
			if(this.get(i).getCustomGetPolDate() == null || this.get(i).getCustomGetPolDate().equals("")) {
				pstmt.setDate(74,null);
			} else {
				pstmt.setDate(74, Date.valueOf(this.get(i).getCustomGetPolDate()));
			}
			if(this.get(i).getState() == null || this.get(i).getState().equals("")) {
				pstmt.setString(75,null);
			} else {
				pstmt.setString(75, this.get(i).getState());
			}
			if(this.get(i).getOperator() == null || this.get(i).getOperator().equals("")) {
				pstmt.setString(76,null);
			} else {
				pstmt.setString(76, this.get(i).getOperator());
			}
			if(this.get(i).getMakeDate() == null || this.get(i).getMakeDate().equals("")) {
				pstmt.setDate(77,null);
			} else {
				pstmt.setDate(77, Date.valueOf(this.get(i).getMakeDate()));
			}
			if(this.get(i).getMakeTime() == null || this.get(i).getMakeTime().equals("")) {
				pstmt.setString(78,null);
			} else {
				pstmt.setString(78, this.get(i).getMakeTime());
			}
			if(this.get(i).getModifyDate() == null || this.get(i).getModifyDate().equals("")) {
				pstmt.setDate(79,null);
			} else {
				pstmt.setDate(79, Date.valueOf(this.get(i).getModifyDate()));
			}
			if(this.get(i).getModifyTime() == null || this.get(i).getModifyTime().equals("")) {
				pstmt.setString(80,null);
			} else {
				pstmt.setString(80, this.get(i).getModifyTime());
			}
			if(this.get(i).getFirstTrialOperator() == null || this.get(i).getFirstTrialOperator().equals("")) {
				pstmt.setString(81,null);
			} else {
				pstmt.setString(81, this.get(i).getFirstTrialOperator());
			}
			if(this.get(i).getFirstTrialDate() == null || this.get(i).getFirstTrialDate().equals("")) {
				pstmt.setDate(82,null);
			} else {
				pstmt.setDate(82, Date.valueOf(this.get(i).getFirstTrialDate()));
			}
			if(this.get(i).getFirstTrialTime() == null || this.get(i).getFirstTrialTime().equals("")) {
				pstmt.setString(83,null);
			} else {
				pstmt.setString(83, this.get(i).getFirstTrialTime());
			}
			if(this.get(i).getReceiveOperator() == null || this.get(i).getReceiveOperator().equals("")) {
				pstmt.setString(84,null);
			} else {
				pstmt.setString(84, this.get(i).getReceiveOperator());
			}
			if(this.get(i).getReceiveDate() == null || this.get(i).getReceiveDate().equals("")) {
				pstmt.setDate(85,null);
			} else {
				pstmt.setDate(85, Date.valueOf(this.get(i).getReceiveDate()));
			}
			if(this.get(i).getReceiveTime() == null || this.get(i).getReceiveTime().equals("")) {
				pstmt.setString(86,null);
			} else {
				pstmt.setString(86, this.get(i).getReceiveTime());
			}
			if(this.get(i).getTempFeeNo() == null || this.get(i).getTempFeeNo().equals("")) {
				pstmt.setString(87,null);
			} else {
				pstmt.setString(87, this.get(i).getTempFeeNo());
			}
			if(this.get(i).getSellType() == null || this.get(i).getSellType().equals("")) {
				pstmt.setString(88,null);
			} else {
				pstmt.setString(88, this.get(i).getSellType());
			}
			if(this.get(i).getForceUWFlag() == null || this.get(i).getForceUWFlag().equals("")) {
				pstmt.setString(89,null);
			} else {
				pstmt.setString(89, this.get(i).getForceUWFlag());
			}
			if(this.get(i).getForceUWReason() == null || this.get(i).getForceUWReason().equals("")) {
				pstmt.setString(90,null);
			} else {
				pstmt.setString(90, this.get(i).getForceUWReason());
			}
			if(this.get(i).getNewBankCode() == null || this.get(i).getNewBankCode().equals("")) {
				pstmt.setString(91,null);
			} else {
				pstmt.setString(91, this.get(i).getNewBankCode());
			}
			if(this.get(i).getNewBankAccNo() == null || this.get(i).getNewBankAccNo().equals("")) {
				pstmt.setString(92,null);
			} else {
				pstmt.setString(92, this.get(i).getNewBankAccNo());
			}
			if(this.get(i).getNewAccName() == null || this.get(i).getNewAccName().equals("")) {
				pstmt.setString(93,null);
			} else {
				pstmt.setString(93, this.get(i).getNewAccName());
			}
			if(this.get(i).getNewPayMode() == null || this.get(i).getNewPayMode().equals("")) {
				pstmt.setString(94,null);
			} else {
				pstmt.setString(94, this.get(i).getNewPayMode());
			}
			if(this.get(i).getAgentBankCode() == null || this.get(i).getAgentBankCode().equals("")) {
				pstmt.setString(95,null);
			} else {
				pstmt.setString(95, this.get(i).getAgentBankCode());
			}
			if(this.get(i).getBankAgent() == null || this.get(i).getBankAgent().equals("")) {
				pstmt.setString(96,null);
			} else {
				pstmt.setString(96, this.get(i).getBankAgent());
			}
			if(this.get(i).getAutoPayFlag() == null || this.get(i).getAutoPayFlag().equals("")) {
				pstmt.setString(97,null);
			} else {
				pstmt.setString(97, this.get(i).getAutoPayFlag());
			}
			pstmt.setInt(98, this.get(i).getRnewFlag());
			if(this.get(i).getFamilyContNo() == null || this.get(i).getFamilyContNo().equals("")) {
				pstmt.setString(99,null);
			} else {
				pstmt.setString(99, this.get(i).getFamilyContNo());
			}
			if(this.get(i).getBussFlag() == null || this.get(i).getBussFlag().equals("")) {
				pstmt.setString(100,null);
			} else {
				pstmt.setString(100, this.get(i).getBussFlag());
			}
			if(this.get(i).getSignName() == null || this.get(i).getSignName().equals("")) {
				pstmt.setString(101,null);
			} else {
				pstmt.setString(101, this.get(i).getSignName());
			}
			if(this.get(i).getOrganizeDate() == null || this.get(i).getOrganizeDate().equals("")) {
				pstmt.setDate(102,null);
			} else {
				pstmt.setDate(102, Date.valueOf(this.get(i).getOrganizeDate()));
			}
			if(this.get(i).getOrganizeTime() == null || this.get(i).getOrganizeTime().equals("")) {
				pstmt.setString(103,null);
			} else {
				pstmt.setString(103, this.get(i).getOrganizeTime());
			}
			if(this.get(i).getNewAutoSendBankFlag() == null || this.get(i).getNewAutoSendBankFlag().equals("")) {
				pstmt.setString(104,null);
			} else {
				pstmt.setString(104, this.get(i).getNewAutoSendBankFlag());
			}
			if(this.get(i).getAgentCodeOper() == null || this.get(i).getAgentCodeOper().equals("")) {
				pstmt.setString(105,null);
			} else {
				pstmt.setString(105, this.get(i).getAgentCodeOper());
			}
			if(this.get(i).getAgentCodeAssi() == null || this.get(i).getAgentCodeAssi().equals("")) {
				pstmt.setString(106,null);
			} else {
				pstmt.setString(106, this.get(i).getAgentCodeAssi());
			}
			if(this.get(i).getDelayReasonCode() == null || this.get(i).getDelayReasonCode().equals("")) {
				pstmt.setString(107,null);
			} else {
				pstmt.setString(107, this.get(i).getDelayReasonCode());
			}
			if(this.get(i).getDelayReasonDesc() == null || this.get(i).getDelayReasonDesc().equals("")) {
				pstmt.setString(108,null);
			} else {
				pstmt.setString(108, this.get(i).getDelayReasonDesc());
			}
			if(this.get(i).getXQremindflag() == null || this.get(i).getXQremindflag().equals("")) {
				pstmt.setString(109,null);
			} else {
				pstmt.setString(109, this.get(i).getXQremindflag());
			}
			if(this.get(i).getOrganComCode() == null || this.get(i).getOrganComCode().equals("")) {
				pstmt.setString(110,null);
			} else {
				pstmt.setString(110, this.get(i).getOrganComCode());
			}
			if(this.get(i).getContFlag() == null || this.get(i).getContFlag().equals("")) {
				pstmt.setString(111,null);
			} else {
				pstmt.setString(111, this.get(i).getContFlag());
			}
			if(this.get(i).getSaleDepart() == null || this.get(i).getSaleDepart().equals("")) {
				pstmt.setString(112,null);
			} else {
				pstmt.setString(112, this.get(i).getSaleDepart());
			}
			if(this.get(i).getChnlType() == null || this.get(i).getChnlType().equals("")) {
				pstmt.setString(113,null);
			} else {
				pstmt.setString(113, this.get(i).getChnlType());
			}
			pstmt.setInt(114, this.get(i).getRenewCount());
			if(this.get(i).getRenewContNo() == null || this.get(i).getRenewContNo().equals("")) {
				pstmt.setString(115,null);
			} else {
				pstmt.setString(115, this.get(i).getRenewContNo());
			}
			pstmt.setInt(116, this.get(i).getInitNumPeople());
			pstmt.setInt(117, this.get(i).getInitMult());
			pstmt.setDouble(118, this.get(i).getInitAmnt());
			pstmt.setDouble(119, this.get(i).getInitRiskAmnt());
			pstmt.setDouble(120, this.get(i).getInitPrem());
			pstmt.setDouble(121, this.get(i).getInitStandPrem());
			pstmt.setDouble(122, this.get(i).getRiskAmnt());
			pstmt.setDouble(123, this.get(i).getStandPrem());
			pstmt.setInt(124, this.get(i).getSumNumPeople());
			pstmt.setDouble(125, this.get(i).getSumPay());
			if(this.get(i).getEndDate() == null || this.get(i).getEndDate().equals("")) {
				pstmt.setDate(126,null);
			} else {
				pstmt.setDate(126, Date.valueOf(this.get(i).getEndDate()));
			}
			if(this.get(i).getPayEndDate() == null || this.get(i).getPayEndDate().equals("")) {
				pstmt.setDate(127,null);
			} else {
				pstmt.setDate(127, Date.valueOf(this.get(i).getPayEndDate()));
			}
			if(this.get(i).getAccKind() == null || this.get(i).getAccKind().equals("")) {
				pstmt.setString(128,null);
			} else {
				pstmt.setString(128, this.get(i).getAccKind());
			}
			if(this.get(i).getSignOperator() == null || this.get(i).getSignOperator().equals("")) {
				pstmt.setString(129,null);
			} else {
				pstmt.setString(129, this.get(i).getSignOperator());
			}
			if(this.get(i).getLoanStartDate() == null || this.get(i).getLoanStartDate().equals("")) {
				pstmt.setDate(130,null);
			} else {
				pstmt.setDate(130, Date.valueOf(this.get(i).getLoanStartDate()));
			}
			if(this.get(i).getLoanEndDate() == null || this.get(i).getLoanEndDate().equals("")) {
				pstmt.setDate(131,null);
			} else {
				pstmt.setDate(131, Date.valueOf(this.get(i).getLoanEndDate()));
			}
			if(this.get(i).getAgentBranchesCode() == null || this.get(i).getAgentBranchesCode().equals("")) {
				pstmt.setString(132,null);
			} else {
				pstmt.setString(132, this.get(i).getAgentBranchesCode());
			}
			if(this.get(i).getNeedBillFlag() == null || this.get(i).getNeedBillFlag().equals("")) {
				pstmt.setString(133,null);
			} else {
				pstmt.setString(133, this.get(i).getNeedBillFlag());
			}
			if(this.get(i).getComCode() == null || this.get(i).getComCode().equals("")) {
				pstmt.setString(134,null);
			} else {
				pstmt.setString(134, this.get(i).getComCode());
			}
			if(this.get(i).getModifyOperator() == null || this.get(i).getModifyOperator().equals("")) {
				pstmt.setString(135,null);
			} else {
				pstmt.setString(135, this.get(i).getModifyOperator());
			}
			if(this.get(i).getContPrintFlag() == null || this.get(i).getContPrintFlag().equals("")) {
				pstmt.setString(136,null);
			} else {
				pstmt.setString(136, this.get(i).getContPrintFlag());
			}
			// set where condition
			if(this.get(i).getContNo() == null || this.get(i).getContNo().equals("")) {
				pstmt.setString(137,null);
			} else {
				pstmt.setString(137, this.get(i).getContNo());
			}

		// only for debug purpose
		SQLString sqlObj = new SQLString("LCCont");
		sqlObj.setSQL(2, this.get(i));
		sqlObj.getSQL();

                pstmt.addBatch();
            }
            pstmt.executeBatch();
			pstmt.close();
		} catch (Exception ex) {
			// @@错误处理
ex.printStackTrace();
			this.mErrors.copyAllErrors(db.mErrors);
			CError tError = new CError();
			tError.moduleName = "LCContDBSet";
			tError.functionName = "update()";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			try {
				pstmt.close();
			} catch (Exception e){e.printStackTrace();}

			if( !mflag ) {
				try {
					con.close();
				} catch (Exception e){e.printStackTrace();}
			}

			return false;
		}

		if( !mflag ) {
			try {
				con.close();
			} catch (Exception e){e.printStackTrace();}
		}

		return true;
	}

    /**
     * 新增操作
     * @return boolean
     */
	public boolean insert()
	{
		PreparedStatement pstmt = null;

		if( !mflag ) {
			con = DBConnPool.getConnection();
		}

		try
		{
            int tCount = this.size();
			pstmt = con.prepareStatement("INSERT INTO LCCont(GrpContNo,ContNo,ProposalContNo,PrtNo,ContType,FamilyType,FamilyID,PolType,CardFlag,ManageCom,ExecuteCom,AgentCom,AgentCode,AgentGroup,AgentCode1,AgentType,SaleChnl,Handler,Password,AppntNo,AppntName,AppntSex,AppntBirthday,AppntIDType,AppntIDNo,InsuredNo,InsuredName,InsuredSex,InsuredBirthday,InsuredIDType,InsuredIDNo,PayIntv,PayMode,PayLocation,DisputedFlag,OutPayFlag,GetPolMode,SignCom,SignDate,SignTime,ConsignNo,BankCode,BankAccNo,AccName,PrintCount,LostTimes,Lang,Currency,Remark,Peoples,Mult,Prem,Amnt,SumPrem,Dif,PaytoDate,FirstPayDate,CValiDate,InputOperator,InputDate,InputTime,ApproveFlag,ApproveCode,ApproveDate,ApproveTime,UWFlag,UWOperator,UWDate,UWTime,AppFlag,PolApplyDate,GetPolDate,GetPolTime,CustomGetPolDate,State,Operator,MakeDate,MakeTime,ModifyDate,ModifyTime,FirstTrialOperator,FirstTrialDate,FirstTrialTime,ReceiveOperator,ReceiveDate,ReceiveTime,TempFeeNo,SellType,ForceUWFlag,ForceUWReason,NewBankCode,NewBankAccNo,NewAccName,NewPayMode,AgentBankCode,BankAgent,AutoPayFlag,RnewFlag,FamilyContNo,BussFlag,SignName,OrganizeDate,OrganizeTime,NewAutoSendBankFlag,AgentCodeOper,AgentCodeAssi,DelayReasonCode,DelayReasonDesc,XQremindflag,OrganComCode,ContFlag,SaleDepart,ChnlType,RenewCount,RenewContNo,InitNumPeople,InitMult,InitAmnt,InitRiskAmnt,InitPrem,InitStandPrem,RiskAmnt,StandPrem,SumNumPeople,SumPay,EndDate,PayEndDate,AccKind,SignOperator,LoanStartDate,LoanEndDate,AgentBranchesCode,NeedBillFlag,ComCode,ModifyOperator,ContPrintFlag) VALUES( ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ?)");
            for (int i = 1; i <= tCount; i++)
            {
			if(this.get(i).getGrpContNo() == null || this.get(i).getGrpContNo().equals("")) {
				pstmt.setString(1,null);
			} else {
				pstmt.setString(1, this.get(i).getGrpContNo());
			}
			if(this.get(i).getContNo() == null || this.get(i).getContNo().equals("")) {
				pstmt.setString(2,null);
			} else {
				pstmt.setString(2, this.get(i).getContNo());
			}
			if(this.get(i).getProposalContNo() == null || this.get(i).getProposalContNo().equals("")) {
				pstmt.setString(3,null);
			} else {
				pstmt.setString(3, this.get(i).getProposalContNo());
			}
			if(this.get(i).getPrtNo() == null || this.get(i).getPrtNo().equals("")) {
				pstmt.setString(4,null);
			} else {
				pstmt.setString(4, this.get(i).getPrtNo());
			}
			if(this.get(i).getContType() == null || this.get(i).getContType().equals("")) {
				pstmt.setString(5,null);
			} else {
				pstmt.setString(5, this.get(i).getContType());
			}
			if(this.get(i).getFamilyType() == null || this.get(i).getFamilyType().equals("")) {
				pstmt.setString(6,null);
			} else {
				pstmt.setString(6, this.get(i).getFamilyType());
			}
			if(this.get(i).getFamilyID() == null || this.get(i).getFamilyID().equals("")) {
				pstmt.setString(7,null);
			} else {
				pstmt.setString(7, this.get(i).getFamilyID());
			}
			if(this.get(i).getPolType() == null || this.get(i).getPolType().equals("")) {
				pstmt.setString(8,null);
			} else {
				pstmt.setString(8, this.get(i).getPolType());
			}
			if(this.get(i).getCardFlag() == null || this.get(i).getCardFlag().equals("")) {
				pstmt.setString(9,null);
			} else {
				pstmt.setString(9, this.get(i).getCardFlag());
			}
			if(this.get(i).getManageCom() == null || this.get(i).getManageCom().equals("")) {
				pstmt.setString(10,null);
			} else {
				pstmt.setString(10, this.get(i).getManageCom());
			}
			if(this.get(i).getExecuteCom() == null || this.get(i).getExecuteCom().equals("")) {
				pstmt.setString(11,null);
			} else {
				pstmt.setString(11, this.get(i).getExecuteCom());
			}
			if(this.get(i).getAgentCom() == null || this.get(i).getAgentCom().equals("")) {
				pstmt.setString(12,null);
			} else {
				pstmt.setString(12, this.get(i).getAgentCom());
			}
			if(this.get(i).getAgentCode() == null || this.get(i).getAgentCode().equals("")) {
				pstmt.setString(13,null);
			} else {
				pstmt.setString(13, this.get(i).getAgentCode());
			}
			if(this.get(i).getAgentGroup() == null || this.get(i).getAgentGroup().equals("")) {
				pstmt.setString(14,null);
			} else {
				pstmt.setString(14, this.get(i).getAgentGroup());
			}
			if(this.get(i).getAgentCode1() == null || this.get(i).getAgentCode1().equals("")) {
				pstmt.setString(15,null);
			} else {
				pstmt.setString(15, this.get(i).getAgentCode1());
			}
			if(this.get(i).getAgentType() == null || this.get(i).getAgentType().equals("")) {
				pstmt.setString(16,null);
			} else {
				pstmt.setString(16, this.get(i).getAgentType());
			}
			if(this.get(i).getSaleChnl() == null || this.get(i).getSaleChnl().equals("")) {
				pstmt.setString(17,null);
			} else {
				pstmt.setString(17, this.get(i).getSaleChnl());
			}
			if(this.get(i).getHandler() == null || this.get(i).getHandler().equals("")) {
				pstmt.setString(18,null);
			} else {
				pstmt.setString(18, this.get(i).getHandler());
			}
			if(this.get(i).getPassword() == null || this.get(i).getPassword().equals("")) {
				pstmt.setString(19,null);
			} else {
				pstmt.setString(19, this.get(i).getPassword());
			}
			if(this.get(i).getAppntNo() == null || this.get(i).getAppntNo().equals("")) {
				pstmt.setString(20,null);
			} else {
				pstmt.setString(20, this.get(i).getAppntNo());
			}
			if(this.get(i).getAppntName() == null || this.get(i).getAppntName().equals("")) {
				pstmt.setString(21,null);
			} else {
				pstmt.setString(21, this.get(i).getAppntName());
			}
			if(this.get(i).getAppntSex() == null || this.get(i).getAppntSex().equals("")) {
				pstmt.setString(22,null);
			} else {
				pstmt.setString(22, this.get(i).getAppntSex());
			}
			if(this.get(i).getAppntBirthday() == null || this.get(i).getAppntBirthday().equals("")) {
				pstmt.setDate(23,null);
			} else {
				pstmt.setDate(23, Date.valueOf(this.get(i).getAppntBirthday()));
			}
			if(this.get(i).getAppntIDType() == null || this.get(i).getAppntIDType().equals("")) {
				pstmt.setString(24,null);
			} else {
				pstmt.setString(24, this.get(i).getAppntIDType());
			}
			if(this.get(i).getAppntIDNo() == null || this.get(i).getAppntIDNo().equals("")) {
				pstmt.setString(25,null);
			} else {
				pstmt.setString(25, this.get(i).getAppntIDNo());
			}
			if(this.get(i).getInsuredNo() == null || this.get(i).getInsuredNo().equals("")) {
				pstmt.setString(26,null);
			} else {
				pstmt.setString(26, this.get(i).getInsuredNo());
			}
			if(this.get(i).getInsuredName() == null || this.get(i).getInsuredName().equals("")) {
				pstmt.setString(27,null);
			} else {
				pstmt.setString(27, this.get(i).getInsuredName());
			}
			if(this.get(i).getInsuredSex() == null || this.get(i).getInsuredSex().equals("")) {
				pstmt.setString(28,null);
			} else {
				pstmt.setString(28, this.get(i).getInsuredSex());
			}
			if(this.get(i).getInsuredBirthday() == null || this.get(i).getInsuredBirthday().equals("")) {
				pstmt.setDate(29,null);
			} else {
				pstmt.setDate(29, Date.valueOf(this.get(i).getInsuredBirthday()));
			}
			if(this.get(i).getInsuredIDType() == null || this.get(i).getInsuredIDType().equals("")) {
				pstmt.setString(30,null);
			} else {
				pstmt.setString(30, this.get(i).getInsuredIDType());
			}
			if(this.get(i).getInsuredIDNo() == null || this.get(i).getInsuredIDNo().equals("")) {
				pstmt.setString(31,null);
			} else {
				pstmt.setString(31, this.get(i).getInsuredIDNo());
			}
			pstmt.setInt(32, this.get(i).getPayIntv());
			if(this.get(i).getPayMode() == null || this.get(i).getPayMode().equals("")) {
				pstmt.setString(33,null);
			} else {
				pstmt.setString(33, this.get(i).getPayMode());
			}
			if(this.get(i).getPayLocation() == null || this.get(i).getPayLocation().equals("")) {
				pstmt.setString(34,null);
			} else {
				pstmt.setString(34, this.get(i).getPayLocation());
			}
			if(this.get(i).getDisputedFlag() == null || this.get(i).getDisputedFlag().equals("")) {
				pstmt.setString(35,null);
			} else {
				pstmt.setString(35, this.get(i).getDisputedFlag());
			}
			if(this.get(i).getOutPayFlag() == null || this.get(i).getOutPayFlag().equals("")) {
				pstmt.setString(36,null);
			} else {
				pstmt.setString(36, this.get(i).getOutPayFlag());
			}
			if(this.get(i).getGetPolMode() == null || this.get(i).getGetPolMode().equals("")) {
				pstmt.setString(37,null);
			} else {
				pstmt.setString(37, this.get(i).getGetPolMode());
			}
			if(this.get(i).getSignCom() == null || this.get(i).getSignCom().equals("")) {
				pstmt.setString(38,null);
			} else {
				pstmt.setString(38, this.get(i).getSignCom());
			}
			if(this.get(i).getSignDate() == null || this.get(i).getSignDate().equals("")) {
				pstmt.setDate(39,null);
			} else {
				pstmt.setDate(39, Date.valueOf(this.get(i).getSignDate()));
			}
			if(this.get(i).getSignTime() == null || this.get(i).getSignTime().equals("")) {
				pstmt.setString(40,null);
			} else {
				pstmt.setString(40, this.get(i).getSignTime());
			}
			if(this.get(i).getConsignNo() == null || this.get(i).getConsignNo().equals("")) {
				pstmt.setString(41,null);
			} else {
				pstmt.setString(41, this.get(i).getConsignNo());
			}
			if(this.get(i).getBankCode() == null || this.get(i).getBankCode().equals("")) {
				pstmt.setString(42,null);
			} else {
				pstmt.setString(42, this.get(i).getBankCode());
			}
			if(this.get(i).getBankAccNo() == null || this.get(i).getBankAccNo().equals("")) {
				pstmt.setString(43,null);
			} else {
				pstmt.setString(43, this.get(i).getBankAccNo());
			}
			if(this.get(i).getAccName() == null || this.get(i).getAccName().equals("")) {
				pstmt.setString(44,null);
			} else {
				pstmt.setString(44, this.get(i).getAccName());
			}
			pstmt.setInt(45, this.get(i).getPrintCount());
			pstmt.setInt(46, this.get(i).getLostTimes());
			if(this.get(i).getLang() == null || this.get(i).getLang().equals("")) {
				pstmt.setString(47,null);
			} else {
				pstmt.setString(47, this.get(i).getLang());
			}
			if(this.get(i).getCurrency() == null || this.get(i).getCurrency().equals("")) {
				pstmt.setString(48,null);
			} else {
				pstmt.setString(48, this.get(i).getCurrency());
			}
			if(this.get(i).getRemark() == null || this.get(i).getRemark().equals("")) {
				pstmt.setString(49,null);
			} else {
				pstmt.setString(49, this.get(i).getRemark());
			}
			pstmt.setInt(50, this.get(i).getPeoples());
			pstmt.setDouble(51, this.get(i).getMult());
			pstmt.setDouble(52, this.get(i).getPrem());
			pstmt.setDouble(53, this.get(i).getAmnt());
			pstmt.setDouble(54, this.get(i).getSumPrem());
			pstmt.setDouble(55, this.get(i).getDif());
			if(this.get(i).getPaytoDate() == null || this.get(i).getPaytoDate().equals("")) {
				pstmt.setDate(56,null);
			} else {
				pstmt.setDate(56, Date.valueOf(this.get(i).getPaytoDate()));
			}
			if(this.get(i).getFirstPayDate() == null || this.get(i).getFirstPayDate().equals("")) {
				pstmt.setDate(57,null);
			} else {
				pstmt.setDate(57, Date.valueOf(this.get(i).getFirstPayDate()));
			}
			if(this.get(i).getCValiDate() == null || this.get(i).getCValiDate().equals("")) {
				pstmt.setDate(58,null);
			} else {
				pstmt.setDate(58, Date.valueOf(this.get(i).getCValiDate()));
			}
			if(this.get(i).getInputOperator() == null || this.get(i).getInputOperator().equals("")) {
				pstmt.setString(59,null);
			} else {
				pstmt.setString(59, this.get(i).getInputOperator());
			}
			if(this.get(i).getInputDate() == null || this.get(i).getInputDate().equals("")) {
				pstmt.setDate(60,null);
			} else {
				pstmt.setDate(60, Date.valueOf(this.get(i).getInputDate()));
			}
			if(this.get(i).getInputTime() == null || this.get(i).getInputTime().equals("")) {
				pstmt.setString(61,null);
			} else {
				pstmt.setString(61, this.get(i).getInputTime());
			}
			if(this.get(i).getApproveFlag() == null || this.get(i).getApproveFlag().equals("")) {
				pstmt.setString(62,null);
			} else {
				pstmt.setString(62, this.get(i).getApproveFlag());
			}
			if(this.get(i).getApproveCode() == null || this.get(i).getApproveCode().equals("")) {
				pstmt.setString(63,null);
			} else {
				pstmt.setString(63, this.get(i).getApproveCode());
			}
			if(this.get(i).getApproveDate() == null || this.get(i).getApproveDate().equals("")) {
				pstmt.setDate(64,null);
			} else {
				pstmt.setDate(64, Date.valueOf(this.get(i).getApproveDate()));
			}
			if(this.get(i).getApproveTime() == null || this.get(i).getApproveTime().equals("")) {
				pstmt.setString(65,null);
			} else {
				pstmt.setString(65, this.get(i).getApproveTime());
			}
			if(this.get(i).getUWFlag() == null || this.get(i).getUWFlag().equals("")) {
				pstmt.setString(66,null);
			} else {
				pstmt.setString(66, this.get(i).getUWFlag());
			}
			if(this.get(i).getUWOperator() == null || this.get(i).getUWOperator().equals("")) {
				pstmt.setString(67,null);
			} else {
				pstmt.setString(67, this.get(i).getUWOperator());
			}
			if(this.get(i).getUWDate() == null || this.get(i).getUWDate().equals("")) {
				pstmt.setDate(68,null);
			} else {
				pstmt.setDate(68, Date.valueOf(this.get(i).getUWDate()));
			}
			if(this.get(i).getUWTime() == null || this.get(i).getUWTime().equals("")) {
				pstmt.setString(69,null);
			} else {
				pstmt.setString(69, this.get(i).getUWTime());
			}
			if(this.get(i).getAppFlag() == null || this.get(i).getAppFlag().equals("")) {
				pstmt.setString(70,null);
			} else {
				pstmt.setString(70, this.get(i).getAppFlag());
			}
			if(this.get(i).getPolApplyDate() == null || this.get(i).getPolApplyDate().equals("")) {
				pstmt.setDate(71,null);
			} else {
				pstmt.setDate(71, Date.valueOf(this.get(i).getPolApplyDate()));
			}
			if(this.get(i).getGetPolDate() == null || this.get(i).getGetPolDate().equals("")) {
				pstmt.setDate(72,null);
			} else {
				pstmt.setDate(72, Date.valueOf(this.get(i).getGetPolDate()));
			}
			if(this.get(i).getGetPolTime() == null || this.get(i).getGetPolTime().equals("")) {
				pstmt.setString(73,null);
			} else {
				pstmt.setString(73, this.get(i).getGetPolTime());
			}
			if(this.get(i).getCustomGetPolDate() == null || this.get(i).getCustomGetPolDate().equals("")) {
				pstmt.setDate(74,null);
			} else {
				pstmt.setDate(74, Date.valueOf(this.get(i).getCustomGetPolDate()));
			}
			if(this.get(i).getState() == null || this.get(i).getState().equals("")) {
				pstmt.setString(75,null);
			} else {
				pstmt.setString(75, this.get(i).getState());
			}
			if(this.get(i).getOperator() == null || this.get(i).getOperator().equals("")) {
				pstmt.setString(76,null);
			} else {
				pstmt.setString(76, this.get(i).getOperator());
			}
			if(this.get(i).getMakeDate() == null || this.get(i).getMakeDate().equals("")) {
				pstmt.setDate(77,null);
			} else {
				pstmt.setDate(77, Date.valueOf(this.get(i).getMakeDate()));
			}
			if(this.get(i).getMakeTime() == null || this.get(i).getMakeTime().equals("")) {
				pstmt.setString(78,null);
			} else {
				pstmt.setString(78, this.get(i).getMakeTime());
			}
			if(this.get(i).getModifyDate() == null || this.get(i).getModifyDate().equals("")) {
				pstmt.setDate(79,null);
			} else {
				pstmt.setDate(79, Date.valueOf(this.get(i).getModifyDate()));
			}
			if(this.get(i).getModifyTime() == null || this.get(i).getModifyTime().equals("")) {
				pstmt.setString(80,null);
			} else {
				pstmt.setString(80, this.get(i).getModifyTime());
			}
			if(this.get(i).getFirstTrialOperator() == null || this.get(i).getFirstTrialOperator().equals("")) {
				pstmt.setString(81,null);
			} else {
				pstmt.setString(81, this.get(i).getFirstTrialOperator());
			}
			if(this.get(i).getFirstTrialDate() == null || this.get(i).getFirstTrialDate().equals("")) {
				pstmt.setDate(82,null);
			} else {
				pstmt.setDate(82, Date.valueOf(this.get(i).getFirstTrialDate()));
			}
			if(this.get(i).getFirstTrialTime() == null || this.get(i).getFirstTrialTime().equals("")) {
				pstmt.setString(83,null);
			} else {
				pstmt.setString(83, this.get(i).getFirstTrialTime());
			}
			if(this.get(i).getReceiveOperator() == null || this.get(i).getReceiveOperator().equals("")) {
				pstmt.setString(84,null);
			} else {
				pstmt.setString(84, this.get(i).getReceiveOperator());
			}
			if(this.get(i).getReceiveDate() == null || this.get(i).getReceiveDate().equals("")) {
				pstmt.setDate(85,null);
			} else {
				pstmt.setDate(85, Date.valueOf(this.get(i).getReceiveDate()));
			}
			if(this.get(i).getReceiveTime() == null || this.get(i).getReceiveTime().equals("")) {
				pstmt.setString(86,null);
			} else {
				pstmt.setString(86, this.get(i).getReceiveTime());
			}
			if(this.get(i).getTempFeeNo() == null || this.get(i).getTempFeeNo().equals("")) {
				pstmt.setString(87,null);
			} else {
				pstmt.setString(87, this.get(i).getTempFeeNo());
			}
			if(this.get(i).getSellType() == null || this.get(i).getSellType().equals("")) {
				pstmt.setString(88,null);
			} else {
				pstmt.setString(88, this.get(i).getSellType());
			}
			if(this.get(i).getForceUWFlag() == null || this.get(i).getForceUWFlag().equals("")) {
				pstmt.setString(89,null);
			} else {
				pstmt.setString(89, this.get(i).getForceUWFlag());
			}
			if(this.get(i).getForceUWReason() == null || this.get(i).getForceUWReason().equals("")) {
				pstmt.setString(90,null);
			} else {
				pstmt.setString(90, this.get(i).getForceUWReason());
			}
			if(this.get(i).getNewBankCode() == null || this.get(i).getNewBankCode().equals("")) {
				pstmt.setString(91,null);
			} else {
				pstmt.setString(91, this.get(i).getNewBankCode());
			}
			if(this.get(i).getNewBankAccNo() == null || this.get(i).getNewBankAccNo().equals("")) {
				pstmt.setString(92,null);
			} else {
				pstmt.setString(92, this.get(i).getNewBankAccNo());
			}
			if(this.get(i).getNewAccName() == null || this.get(i).getNewAccName().equals("")) {
				pstmt.setString(93,null);
			} else {
				pstmt.setString(93, this.get(i).getNewAccName());
			}
			if(this.get(i).getNewPayMode() == null || this.get(i).getNewPayMode().equals("")) {
				pstmt.setString(94,null);
			} else {
				pstmt.setString(94, this.get(i).getNewPayMode());
			}
			if(this.get(i).getAgentBankCode() == null || this.get(i).getAgentBankCode().equals("")) {
				pstmt.setString(95,null);
			} else {
				pstmt.setString(95, this.get(i).getAgentBankCode());
			}
			if(this.get(i).getBankAgent() == null || this.get(i).getBankAgent().equals("")) {
				pstmt.setString(96,null);
			} else {
				pstmt.setString(96, this.get(i).getBankAgent());
			}
			if(this.get(i).getAutoPayFlag() == null || this.get(i).getAutoPayFlag().equals("")) {
				pstmt.setString(97,null);
			} else {
				pstmt.setString(97, this.get(i).getAutoPayFlag());
			}
			pstmt.setInt(98, this.get(i).getRnewFlag());
			if(this.get(i).getFamilyContNo() == null || this.get(i).getFamilyContNo().equals("")) {
				pstmt.setString(99,null);
			} else {
				pstmt.setString(99, this.get(i).getFamilyContNo());
			}
			if(this.get(i).getBussFlag() == null || this.get(i).getBussFlag().equals("")) {
				pstmt.setString(100,null);
			} else {
				pstmt.setString(100, this.get(i).getBussFlag());
			}
			if(this.get(i).getSignName() == null || this.get(i).getSignName().equals("")) {
				pstmt.setString(101,null);
			} else {
				pstmt.setString(101, this.get(i).getSignName());
			}
			if(this.get(i).getOrganizeDate() == null || this.get(i).getOrganizeDate().equals("")) {
				pstmt.setDate(102,null);
			} else {
				pstmt.setDate(102, Date.valueOf(this.get(i).getOrganizeDate()));
			}
			if(this.get(i).getOrganizeTime() == null || this.get(i).getOrganizeTime().equals("")) {
				pstmt.setString(103,null);
			} else {
				pstmt.setString(103, this.get(i).getOrganizeTime());
			}
			if(this.get(i).getNewAutoSendBankFlag() == null || this.get(i).getNewAutoSendBankFlag().equals("")) {
				pstmt.setString(104,null);
			} else {
				pstmt.setString(104, this.get(i).getNewAutoSendBankFlag());
			}
			if(this.get(i).getAgentCodeOper() == null || this.get(i).getAgentCodeOper().equals("")) {
				pstmt.setString(105,null);
			} else {
				pstmt.setString(105, this.get(i).getAgentCodeOper());
			}
			if(this.get(i).getAgentCodeAssi() == null || this.get(i).getAgentCodeAssi().equals("")) {
				pstmt.setString(106,null);
			} else {
				pstmt.setString(106, this.get(i).getAgentCodeAssi());
			}
			if(this.get(i).getDelayReasonCode() == null || this.get(i).getDelayReasonCode().equals("")) {
				pstmt.setString(107,null);
			} else {
				pstmt.setString(107, this.get(i).getDelayReasonCode());
			}
			if(this.get(i).getDelayReasonDesc() == null || this.get(i).getDelayReasonDesc().equals("")) {
				pstmt.setString(108,null);
			} else {
				pstmt.setString(108, this.get(i).getDelayReasonDesc());
			}
			if(this.get(i).getXQremindflag() == null || this.get(i).getXQremindflag().equals("")) {
				pstmt.setString(109,null);
			} else {
				pstmt.setString(109, this.get(i).getXQremindflag());
			}
			if(this.get(i).getOrganComCode() == null || this.get(i).getOrganComCode().equals("")) {
				pstmt.setString(110,null);
			} else {
				pstmt.setString(110, this.get(i).getOrganComCode());
			}
			if(this.get(i).getContFlag() == null || this.get(i).getContFlag().equals("")) {
				pstmt.setString(111,null);
			} else {
				pstmt.setString(111, this.get(i).getContFlag());
			}
			if(this.get(i).getSaleDepart() == null || this.get(i).getSaleDepart().equals("")) {
				pstmt.setString(112,null);
			} else {
				pstmt.setString(112, this.get(i).getSaleDepart());
			}
			if(this.get(i).getChnlType() == null || this.get(i).getChnlType().equals("")) {
				pstmt.setString(113,null);
			} else {
				pstmt.setString(113, this.get(i).getChnlType());
			}
			pstmt.setInt(114, this.get(i).getRenewCount());
			if(this.get(i).getRenewContNo() == null || this.get(i).getRenewContNo().equals("")) {
				pstmt.setString(115,null);
			} else {
				pstmt.setString(115, this.get(i).getRenewContNo());
			}
			pstmt.setInt(116, this.get(i).getInitNumPeople());
			pstmt.setInt(117, this.get(i).getInitMult());
			pstmt.setDouble(118, this.get(i).getInitAmnt());
			pstmt.setDouble(119, this.get(i).getInitRiskAmnt());
			pstmt.setDouble(120, this.get(i).getInitPrem());
			pstmt.setDouble(121, this.get(i).getInitStandPrem());
			pstmt.setDouble(122, this.get(i).getRiskAmnt());
			pstmt.setDouble(123, this.get(i).getStandPrem());
			pstmt.setInt(124, this.get(i).getSumNumPeople());
			pstmt.setDouble(125, this.get(i).getSumPay());
			if(this.get(i).getEndDate() == null || this.get(i).getEndDate().equals("")) {
				pstmt.setDate(126,null);
			} else {
				pstmt.setDate(126, Date.valueOf(this.get(i).getEndDate()));
			}
			if(this.get(i).getPayEndDate() == null || this.get(i).getPayEndDate().equals("")) {
				pstmt.setDate(127,null);
			} else {
				pstmt.setDate(127, Date.valueOf(this.get(i).getPayEndDate()));
			}
			if(this.get(i).getAccKind() == null || this.get(i).getAccKind().equals("")) {
				pstmt.setString(128,null);
			} else {
				pstmt.setString(128, this.get(i).getAccKind());
			}
			if(this.get(i).getSignOperator() == null || this.get(i).getSignOperator().equals("")) {
				pstmt.setString(129,null);
			} else {
				pstmt.setString(129, this.get(i).getSignOperator());
			}
			if(this.get(i).getLoanStartDate() == null || this.get(i).getLoanStartDate().equals("")) {
				pstmt.setDate(130,null);
			} else {
				pstmt.setDate(130, Date.valueOf(this.get(i).getLoanStartDate()));
			}
			if(this.get(i).getLoanEndDate() == null || this.get(i).getLoanEndDate().equals("")) {
				pstmt.setDate(131,null);
			} else {
				pstmt.setDate(131, Date.valueOf(this.get(i).getLoanEndDate()));
			}
			if(this.get(i).getAgentBranchesCode() == null || this.get(i).getAgentBranchesCode().equals("")) {
				pstmt.setString(132,null);
			} else {
				pstmt.setString(132, this.get(i).getAgentBranchesCode());
			}
			if(this.get(i).getNeedBillFlag() == null || this.get(i).getNeedBillFlag().equals("")) {
				pstmt.setString(133,null);
			} else {
				pstmt.setString(133, this.get(i).getNeedBillFlag());
			}
			if(this.get(i).getComCode() == null || this.get(i).getComCode().equals("")) {
				pstmt.setString(134,null);
			} else {
				pstmt.setString(134, this.get(i).getComCode());
			}
			if(this.get(i).getModifyOperator() == null || this.get(i).getModifyOperator().equals("")) {
				pstmt.setString(135,null);
			} else {
				pstmt.setString(135, this.get(i).getModifyOperator());
			}
			if(this.get(i).getContPrintFlag() == null || this.get(i).getContPrintFlag().equals("")) {
				pstmt.setString(136,null);
			} else {
				pstmt.setString(136, this.get(i).getContPrintFlag());
			}

		// only for debug purpose
		SQLString sqlObj = new SQLString("LCCont");
		sqlObj.setSQL(1, this.get(i));
		sqlObj.getSQL();

                pstmt.addBatch();
            }
            pstmt.executeBatch();
			pstmt.close();
		} catch (Exception ex) {
			// @@错误处理
ex.printStackTrace();
			this.mErrors.copyAllErrors(db.mErrors);
			CError tError = new CError();
			tError.moduleName = "LCContDBSet";
			tError.functionName = "insert()";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			try {
				pstmt.close();
			} catch (Exception e){e.printStackTrace();}

			if( !mflag ) {
				try {
					con.close();
				} catch (Exception e){e.printStackTrace();}
			}

			return false;
		}

		if( !mflag ) {
			try {
				con.close();
			} catch (Exception e){e.printStackTrace();}
		}

		return true;
	}

}
