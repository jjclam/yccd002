package com.sinosoft.cloud;

import com.sinosoft.lis.db.LCContDB;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = SimpleApplication.class)
@EnableScheduling
public class SimpleTestApplication {


    @Test
    public void testDB(){
        LCContDB contDB = new LCContDB();
        contDB.setContNo("1");
        contDB.getInfo();
    }
}
