package com.sinosoft.cloud.mq;


import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.MessageTransformer;
import org.apache.activemq.command.ActiveMQObjectMessage;
import org.apache.activemq.command.ActiveMQQueue;
import org.apache.activemq.command.ActiveMQTextMessage;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.jms.core.JmsMessagingTemplate;
import org.springframework.stereotype.Component;

import javax.jms.*;

/**
 * 操作MQ相关方法
 */
@Component
public class MessageQueueProducer {

    private Log log = LogFactory.getLog(getClass());
    @Autowired
    private RedisTemplate redisTemplate;


    public MessageQueueProducer(ActiveMQConnectionFactory factory) {
        factory.setTransformer(
                new MessageTransformer() {

                    @Override
                    public Message producerTransform(Session session, MessageProducer messageProducer, Message message) throws JMSException {
                        ActiveMQQueue activeMQQueue = (ActiveMQQueue) messageProducer.getDestination();
                        StringBuffer stringBuffer = new StringBuffer(messageProducer.toString().substring(32, messageProducer.toString().length() - 2));
                        stringBuffer.append(":1").replace(0, 2, activeMQQueue.getQueueName());
                        if (message instanceof ActiveMQTextMessage) {
                            ActiveMQTextMessage messagespring = (ActiveMQTextMessage) message;
                            redisTemplate.opsForHash().put("MQ", stringBuffer.toString(), messagespring.getText());
                            if (log.isDebugEnabled()) {
                                log.debug("redis缓存消息成功:{topic:" + activeMQQueue.getQueueName() + ";messageid:" + stringBuffer.toString() + ";messge:" + messagespring.getText() + "}");
                            }
                        }
                        if (message instanceof ActiveMQObjectMessage) {
                            ActiveMQObjectMessage messagespring = (ActiveMQObjectMessage) message;
                            redisTemplate.opsForHash().put("MQ", stringBuffer.toString(), messagespring.getObject());
                            if (log.isDebugEnabled()) {
                                log.debug("redis缓存消息成功:{topic:" + activeMQQueue.getQueueName() + ";messageid:" + stringBuffer.toString() + ";messge:" + messagespring.getObject() + "}");
                            }
                        }

                        return message;
                    }

                    @Override
                    public Message consumerTransform(Session session, MessageConsumer messageConsumer, Message message) throws JMSException {
                        return message;
                    }
                }
        );
    }


    @Autowired
    private JmsMessagingTemplate jmsMessagingTemplate;

    /**
     * 发送消息
     *
     * @param queue 队列
     * @param message 信息
     */
    public void send(Queue queue, Object message) {
        this.jmsMessagingTemplate.convertAndSend(queue, message);
    }

    /**
     * @param topic 主题
     * @param message 信息
     */
    public void send(String topic, Object message) {
        Queue queue = new ActiveMQQueue(topic);
        send(queue, message);
    }


}
