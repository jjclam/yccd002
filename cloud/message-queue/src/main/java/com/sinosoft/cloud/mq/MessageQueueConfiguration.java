package com.sinosoft.cloud.mq;

import org.springframework.boot.SpringBootConfiguration;
import org.springframework.context.annotation.Description;
import org.springframework.jms.annotation.EnableJms;

/**
 * MQ spring初始化配置
 */
@SpringBootConfiguration
@EnableJms
@Description(value = "ActiveMQ Configuration")
public class MessageQueueConfiguration {

}
