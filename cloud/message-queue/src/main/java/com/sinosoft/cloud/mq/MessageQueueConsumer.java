package com.sinosoft.cloud.mq;

import org.apache.activemq.command.ActiveMQObjectMessage;
import org.apache.activemq.command.ActiveMQQueue;
import org.apache.activemq.command.ActiveMQTextMessage;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import javax.jms.JMSException;
import javax.jms.Message;

/**
 * Project abc-cloud
 * Created by sundongbo on 2017/10/24.
 * <pre>
 * 林花谢了春红，太匆匆，无奈朝来寒雨晚来风。
 * 胭脂泪，相留醉，几时重，自是人生长恨水长东。
 *
 * --翼羽水淼起辰月，秋山红日望紫枫--
 * </pre>
 *
 * @author jiangzif
 */
@Component
public class MessageQueueConsumer {
    private Log log = LogFactory.getLog(getClass());
    @Autowired
    private RedisTemplate redisTemplate;

    /**
     * 核销功能，默认将mq消息对象使用redis的hash存储，key="MQ",field="topic:messageid" value=messageobject
     *
     * @param b 业务处理返回值
     * @param message message消息对象
     */
    public void verified(Boolean b, Message message) {
        try {
            ActiveMQQueue activeMQQueue = (ActiveMQQueue) message.getJMSDestination();
            if (b) {
                redisTemplate.opsForHash().delete("MQ", message.getJMSMessageID().replaceFirst("ID", activeMQQueue.getQueueName()));
                if (log.isDebugEnabled()) {
                    if (message instanceof ActiveMQTextMessage) {
                        ActiveMQTextMessage messagespring = (ActiveMQTextMessage) message;
                        log.debug("Redis核销消息成功:{topic:" + activeMQQueue.getQueueName() + ";messageid:" + message.getJMSMessageID().replaceFirst("ID", activeMQQueue.getQueueName()) + ";messge:" + messagespring.getText() + "}");

                    }
                    if (message instanceof ActiveMQObjectMessage) {
                        ActiveMQObjectMessage messagespring = (ActiveMQObjectMessage) message;
                        log.debug("Redis核销消息成功:{topic:" + activeMQQueue.getQueueName() + ";messageid:" + message.getJMSMessageID().replaceFirst("ID", activeMQQueue.getQueueName()) + ";messge:" + messagespring.getObject() + "}");

                    }
                }
            } else {
                log.warn("业务处理失败，不进行核销：{topic:" + activeMQQueue.getQueueName() + ";messageid:" + message.getJMSMessageID().replaceFirst("ID", activeMQQueue.getQueueName()) + "}");
            }
        } catch (JMSException e) {
            log.error(ExceptionUtils.getFullStackTrace(e.fillInStackTrace()));
        }
    }
}
