package com.sinosoft.cloud.mq;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

import javax.jms.JMSException;
import javax.jms.Message;

@Component
public class RevicerTest {


    @Autowired
    private MessageQueueConsumer messageQueueProducer;

    @Autowired
    private RedisTemplate redisTemplate;

    /**
     * destination 是队列名称，
     * concurrency 是线程数量 min-max  1-100 标示最小线程数1，最大线程数100.
     * @param text
     * @throws InterruptedException
     */
    @JmsListener(destination = "yangming",concurrency = "1-100")
    public void receiveQueue2(LAAgentPojo text, Message message) throws InterruptedException, JMSException {
        Thread.sleep(1000);
        System.out.println(" 订阅2 text.getAgentGroup()"+text.getAgentGroup());
        java.util.Random random=new java.util.Random();// 定义随机类
        int result=random.nextInt(10);
        boolean b = result%2==0;
        System.out.println("b = " + b);
        messageQueueProducer.verified(b,message);
        System.out.println("redisTemplate.opsForHash()..keys(\"MQ\") = " + redisTemplate.opsForHash().keys("MQ").size());
    }
}
