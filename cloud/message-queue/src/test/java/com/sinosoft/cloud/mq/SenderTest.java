package com.sinosoft.cloud.mq;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
@EnableScheduling
public class SenderTest {

    @Autowired
    MessageQueueProducer producer;

    /**
     * @Scheduled 是定时运行的意思，每300毫秒运行一次。
     */
    @Scheduled(fixedDelay = 1000  )
    public void sender(){
        LAAgentPojo laAgentPojo = new LAAgentPojo();
        laAgentPojo.setAgentGroup(String.valueOf(System.nanoTime()));
        producer.send("yangming",laAgentPojo);

    }



}
