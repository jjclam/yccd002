/**
 * Copyright (c) 2017 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.cloud.mq;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>ClassName: LAAgentPojo </p>
 * <p>Copyright: Copyright (c) 2017</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2017-03-03
 */

public class LAAgentPojo implements Serializable {
    // @Field
    /** 代理人编码 */
    private String AgentCode;
    /** 代理人展业机构代码 */
    private String AgentGroup;
    /** 管理机构 */
    private String ManageCom;
    /** 密码 */
    private String Password;
    /** 推荐报名编号 */
    private String EntryNo;
    /** 姓名 */
    private String Name;
    /** 性别 */
    private String Sex;
    /** 出生日期 */
    private Date Birthday;
    /** 籍贯 */
    private String NativePlace;
    /** 民族 */
    private String Nationality;
    /** 婚姻状况 */
    private String Marriage;
    /** 信用等级 */
    private String CreditGrade;
    /** 家庭地址编码 */
    private String HomeAddressCode;
    /** 家庭地址 */
    private String HomeAddress;
    /** 通讯地址 */
    private String PostalAddress;
    /** 邮政编码 */
    private String ZipCode;
    /** 电话 */
    private String Phone;
    /** 传呼 */
    private String BP;
    /** 手机 */
    private String Mobile;
    /** E_mail */
    private String EMail;
    /** 结婚日期 */
    private Date MarriageDate;
    /** 身份证号码 */
    private String IDNo;
    /** 来源地 */
    private String Source;
    /** 血型 */
    private String BloodType;
    /** 政治面貌 */
    private String PolityVisage;
    /** 学历 */
    private String Degree;
    /** 毕业院校 */
    private String GraduateSchool;
    /** 专业 */
    private String Speciality;
    /** 职称 */
    private String PostTitle;
    /** 外语水平 */
    private String ForeignLevel;
    /** 从业年限 */
    private int WorkAge;
    /** 原工作单位 */
    private String OldCom;
    /** 原职业 */
    private String OldOccupation;
    /** 工作职务（体制） */
    private String HeadShip;
    /** 推荐代理人 */
    private String RecommendAgent;
    /** 工种/行业 */
    private String Business;
    /** 销售资格 */
    private String SaleQuaf;

    public String getAgentCode() {
        return AgentCode;
    }

    public void setAgentCode(String agentCode) {
        AgentCode = agentCode;
    }

    public String getAgentGroup() {
        return AgentGroup;
    }

    public void setAgentGroup(String agentGroup) {
        AgentGroup = agentGroup;
    }

    public String getManageCom() {
        return ManageCom;
    }

    public void setManageCom(String manageCom) {
        ManageCom = manageCom;
    }

    public String getPassword() {
        return Password;
    }

    public void setPassword(String password) {
        Password = password;
    }

    public String getEntryNo() {
        return EntryNo;
    }

    public void setEntryNo(String entryNo) {
        EntryNo = entryNo;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getSex() {
        return Sex;
    }

    public void setSex(String sex) {
        Sex = sex;
    }

    public Date getBirthday() {
        return Birthday;
    }

    public void setBirthday(Date birthday) {
        Birthday = birthday;
    }

    public String getNativePlace() {
        return NativePlace;
    }

    public void setNativePlace(String nativePlace) {
        NativePlace = nativePlace;
    }

    public String getNationality() {
        return Nationality;
    }

    public void setNationality(String nationality) {
        Nationality = nationality;
    }

    public String getMarriage() {
        return Marriage;
    }

    public void setMarriage(String marriage) {
        Marriage = marriage;
    }

    public String getCreditGrade() {
        return CreditGrade;
    }

    public void setCreditGrade(String creditGrade) {
        CreditGrade = creditGrade;
    }

    public String getHomeAddressCode() {
        return HomeAddressCode;
    }

    public void setHomeAddressCode(String homeAddressCode) {
        HomeAddressCode = homeAddressCode;
    }

    public String getHomeAddress() {
        return HomeAddress;
    }

    public void setHomeAddress(String homeAddress) {
        HomeAddress = homeAddress;
    }

    public String getPostalAddress() {
        return PostalAddress;
    }

    public void setPostalAddress(String postalAddress) {
        PostalAddress = postalAddress;
    }

    public String getZipCode() {
        return ZipCode;
    }

    public void setZipCode(String zipCode) {
        ZipCode = zipCode;
    }

    public String getPhone() {
        return Phone;
    }

    public void setPhone(String phone) {
        Phone = phone;
    }

    public String getBP() {
        return BP;
    }

    public void setBP(String BP) {
        this.BP = BP;
    }

    public String getMobile() {
        return Mobile;
    }

    public void setMobile(String mobile) {
        Mobile = mobile;
    }

    public String getEMail() {
        return EMail;
    }

    public void setEMail(String EMail) {
        this.EMail = EMail;
    }

    public Date getMarriageDate() {
        return MarriageDate;
    }

    public void setMarriageDate(Date marriageDate) {
        MarriageDate = marriageDate;
    }

    public String getIDNo() {
        return IDNo;
    }

    public void setIDNo(String IDNo) {
        this.IDNo = IDNo;
    }

    public String getSource() {
        return Source;
    }

    public void setSource(String source) {
        Source = source;
    }

    public String getBloodType() {
        return BloodType;
    }

    public void setBloodType(String bloodType) {
        BloodType = bloodType;
    }

    public String getPolityVisage() {
        return PolityVisage;
    }

    public void setPolityVisage(String polityVisage) {
        PolityVisage = polityVisage;
    }

    public String getDegree() {
        return Degree;
    }

    public void setDegree(String degree) {
        Degree = degree;
    }

    public String getGraduateSchool() {
        return GraduateSchool;
    }

    public void setGraduateSchool(String graduateSchool) {
        GraduateSchool = graduateSchool;
    }

    public String getSpeciality() {
        return Speciality;
    }

    public void setSpeciality(String speciality) {
        Speciality = speciality;
    }

    public String getPostTitle() {
        return PostTitle;
    }

    public void setPostTitle(String postTitle) {
        PostTitle = postTitle;
    }

    public String getForeignLevel() {
        return ForeignLevel;
    }

    public void setForeignLevel(String foreignLevel) {
        ForeignLevel = foreignLevel;
    }

    public int getWorkAge() {
        return WorkAge;
    }

    public void setWorkAge(int workAge) {
        WorkAge = workAge;
    }

    public String getOldCom() {
        return OldCom;
    }

    public void setOldCom(String oldCom) {
        OldCom = oldCom;
    }

    public String getOldOccupation() {
        return OldOccupation;
    }

    public void setOldOccupation(String oldOccupation) {
        OldOccupation = oldOccupation;
    }

    public String getHeadShip() {
        return HeadShip;
    }

    public void setHeadShip(String headShip) {
        HeadShip = headShip;
    }

    public String getRecommendAgent() {
        return RecommendAgent;
    }

    public void setRecommendAgent(String recommendAgent) {
        RecommendAgent = recommendAgent;
    }

    public String getBusiness() {
        return Business;
    }

    public void setBusiness(String business) {
        Business = business;
    }

    public String getSaleQuaf() {
        return SaleQuaf;
    }

    public void setSaleQuaf(String saleQuaf) {
        SaleQuaf = saleQuaf;
    }
}
