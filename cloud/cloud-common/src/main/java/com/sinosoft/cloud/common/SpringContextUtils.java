package com.sinosoft.cloud.common;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * Created by yangming on 15/10/24.
 */
@Component
public class SpringContextUtils implements ApplicationContextAware {

    private static ApplicationContext applicationContext;
    private static Log logger = LogFactory.getLog(SpringContextUtils.class);

    /**
     * 获取applicationContext对象
     * @return ApplicationContext对象
     */
    public static ApplicationContext getApplicationContext() {
        return applicationContext;
    }

    /**
     * 根据bean的id来查找对象
     * @param id beanid
     * @return bean对象
     */
    public static Object getBeanById(String id) {
        if (applicationContext == null) {
            logger.error("未加载SpringContextUtils,如果是本地Main函数中Test,请在Main函数的Spring文件加载上,添加classpath*:/spring/root-context.xml,具体sample,可参看MemPubSubmitTest");
            return null;
        }
        return applicationContext.getBean(id);
    }

    /**
     * 根据bean的class来查找对象
     * @param c 类类型
     * @param <T> 返回值类型
     * @return 返回
     */
    public static <T> T getBeanByClass(Class<T> c) {
        if (applicationContext == null) {
            logger.error("未加载SpringContextUtils,如果是本地Main函数中Test,请在Main函数的Spring文件加载上,添加classpath*:/spring/root-context.xml,具体sample,可参看MemPubSubmitTest");
            return null;
        }
        return (T) applicationContext.getBean(c);
    }

    /**
     * 根据bean的class来查找所有的对象(包括子类)
     *
     * @param c 类类型
     * @return bean集合
     */
    public static Map getBeansByClass(Class c) {
        if (applicationContext == null) {
            logger.error("未加载SpringContextUtils,如果是本地Main函数中Test,请在Main函数的Spring文件加载上,添加classpath*:/spring/root-context.xml,具体sample,可参看MemPubSubmitTest");
            return null;
        }
        return applicationContext.getBeansOfType(c);
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }
}

