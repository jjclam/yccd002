package com.sinosoft.lis.maxno;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * 保单号生成程序 测试类
 * Created by kaine on 2016/8/17.
 */
public class TestPolicySNGenerator {
    static Log logger = LogFactory.getLog(TestPolicySNGenerator.class);

    public static void main(String[] args) {
        PolicySNGenerator policySNGenerator = new PolicySNGenerator();
        // 传入机构编码
        policySNGenerator.setManageCom("860101");
        String SNNo = policySNGenerator.generate();
        logger.info("生成的保单号：" + SNNo);
    }
}
