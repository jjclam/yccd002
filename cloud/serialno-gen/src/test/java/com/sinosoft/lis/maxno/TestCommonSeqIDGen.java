package com.sinosoft.lis.maxno;

import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Created by filon51 on 2016/7/27.
 */
public class TestCommonSeqIDGen {
    /**
     * 测试MAIN
     * @param args
     */
    public static void main(String[] args){
//        String paths[] = {"classpath*:/spring/root-context.xml", "classpath*:/spring/database-context.xml", "classpath*:/spring/spring-cache-context.xml"};
//        ApplicationContext ctx = new ClassPathXmlApplicationContext(paths);

//        CommonSeqIDGen tSNGen = new CommonSeqIDGen();
//        tSNGen.setNoType("PolicyID");
//        tSNGen.setNoLimit("SN");
//        for (int i =1; i<=10 ;i ++) {
//            System.out.println("生成流水号为：" + tSNGen.generate());
//        }

//        CommonSeqStrGen tSNGen = new CommonSeqStrGen();
//        tSNGen.setNoType("ContractID1");
//        tSNGen.setNoLength(10);
////        tSNGen.setNoLimit("SN");
//            System.out.println("生成流水号为：" + tSNGen.generate());



        ExeSQL tExeSQL = new ExeSQL();
//        String  BankAccNosql1 = "select prtno from lcappnt where contno = '1' ";
//        String qq = tExeSQL.getOneValue(BankAccNosql1);
        VData vData = new VData();
        TransferData tTransferData = new TransferData();
        String  BankAccNosql = "select prtno from lcappnt where contno = ? ";
        tTransferData.setNameAndValue("0","String:"+1);
        vData.add(BankAccNosql);
        vData.add(tTransferData);
        SSRS dSSRS = tExeSQL.execSQL(vData);
        System.out.println(dSSRS.GetText(1, 1));

        VData vData1 = new VData();
        TransferData tTransferData1 = new TransferData();
        String  SumDuePayMoneysql = "select appntbirthday from lcappnt where appntid = ? ";
        tTransferData1.setNameAndValue("0","String:"+1);
        vData1.add(SumDuePayMoneysql);
        vData1.add(tTransferData1);
        SSRS tSSRS = tExeSQL.execSQL(vData1);
        System.out.println(tSSRS.GetText(1,1));
    }
}
