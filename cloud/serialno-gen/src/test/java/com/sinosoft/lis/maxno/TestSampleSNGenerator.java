package com.sinosoft.lis.maxno;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Created by filon51 on 2016/7/27.
 */
public class TestSampleSNGenerator {

    /**
     * 测试MAIN
     * @param args
     */
    public static void main(String[] args){
        String paths[] = {"classpath*:/spring/root-context.xml", "classpath*:/spring/database-context.xml", "classpath*:/spring/spring-cache-context.xml"};
        ApplicationContext ctx = new ClassPathXmlApplicationContext(paths);

        SampleSNGenerator tSNGen = new SampleSNGenerator();
        for (int i =1; i<=10 ;i ++) {
            System.out.println("生成流水号为：" + tSNGen.generate());
        }
    }
}
