package com.sinosoft.lis.maxno;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;


/**
 * 电子保单号生成程序 测试类
 * Created by kaine on 2016/8/17.
 */
public class TestEPolicySNGenerator {
    private static final Log logger = LogFactory.getLog(TestPolicySNGenerator.class.getName());

    public static void main(String[] args) {
        EPolicySNGenerator ePolicySNGenerator = new EPolicySNGenerator();
        String SNNo = ePolicySNGenerator.generate();
        logger.info("生成的电子保单号：" + SNNo);
    }
}
