package com.sinosoft;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

/**
 * @project: abc-cloud-microservice
 * @author: yangming
 * @date: 2017/9/17 下午1:58
 * To change this template use File | Settings | File and Code Templates.
 */
@SpringBootApplication
@ComponentScan("com.sinosoft")
public class NBApplication {
    public static void main(String[] args) {

        SpringApplication.run(NBApplication.class, args);
    }

}
//