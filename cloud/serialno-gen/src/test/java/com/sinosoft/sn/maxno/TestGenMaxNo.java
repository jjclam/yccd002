package com.sinosoft.sn.maxno;

import com.sinosoft.cloud.cache.RedisService;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * 需要测试多个子线程同时操作缓存数据的问题。
 * Created by filon51 on 2016/8/1.
 */
public class TestGenMaxNo {
    public static void main(String[] args){
        String paths[] = {"classpath*:/spring/root-context.xml", "classpath*:/spring/database-context.xml", "classpath*:/spring/spring-cache-context.xml"};
        ApplicationContext ctx = new ClassPathXmlApplicationContext(paths);
        RedisService<String, MaxNoCache> redisService = ctx.getBean(RedisService.class);

        MaxNoCache tMaxNoCache = new MaxNoCache();
        tMaxNoCache.setNoType("PolicyID");
        tMaxNoCache.setNoLimit("SN");

//        initMaxNoCache(redisService, tMaxNoCache);


        if(redisService.get(tMaxNoCache.getCachedKey()) != null){
            System.out.println("生成流水号前：" + redisService.get(tMaxNoCache.getCachedKey()).toString());
        }else{
            System.out.println("缓存中不存在！");
        }
        int iGenNoCount = 33;
        for (int i=0; i<iGenNoCount; i++){
            System.out.println("本次产生号码：" + GenMaxNo.getMaxNo("PolicyID", null));
            if(redisService.get(tMaxNoCache.getCachedKey()) != null){
                System.out.println("生成完流水号后：" + redisService.get(tMaxNoCache.getCachedKey()).toString());
            }else{
                //System.out.println("缓存中不存在！");
            }
        }
//        System.out.println("生成完流水号后：" + redisService.get(tMaxNoCache.getCachedKey()).toString());

//       GenMaxNo.getCurNoFromRDB("PolicyID", "SN");
    }

    private static void initMaxNoCache(RedisService<String, MaxNoCache> redisService, MaxNoCache tMaxNoCache) {
        tMaxNoCache.setCurNo(50);
        tMaxNoCache.setMaxNo(100);
        tMaxNoCache.setNextNo(51);
        redisService.set(tMaxNoCache.getCachedKey(), tMaxNoCache);

        System.out.println("初始化缓存：" + redisService.get(tMaxNoCache.getCachedKey()).toString());
    }


}
