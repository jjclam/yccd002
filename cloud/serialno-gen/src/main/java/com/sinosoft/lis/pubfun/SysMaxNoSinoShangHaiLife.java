package com.sinosoft.lis.pubfun;

/**
 * <p>
 * Title: Web业务系统
 * </p>
 * <p>
 * Description:系统号码管理生成系统号码
 * </p>
 * <p>
 * Copyright: Copyright (c) 2010
 * </p>
 * <p>
 * Company: Sinosoft
 * </p>
 *
 * @author
 * @version 1.0
 */

import org.apache.log4j.Logger;

import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.VData;

public class SysMaxNoSinoShangHaiLife implements com.sinosoft.lis.pubfun.SysMaxNo {
	private static Logger logger = Logger.getLogger(SysMaxNoSinoShangHaiLife.class);

	public SysMaxNoSinoShangHaiLife() {

	}

	/**
	 * <p>
	 * 生成流水号的函数
	 * <p>
	 * <p>
	 * 号码规则：机构编码 日期年 校验位 类型 流水号
	 * <p>
	 * <p>
	 * 1-6 7-10 11 12-13 14-20
	 * <p>
	 *
	 * @param cNoType
	 *          为需要生成号码的类型
	 * @param cNoLimit
	 *          为需要生成号码的限制条件（要么是SN，要么是机构编码）
	 * @param cVData
	 *          为需要生成号码的业务相关限制条件
	 * @return 生成的符合条件的流水号，如果生成失败，返回空字符串""
	 */
	public String CreateMaxNo(String cNoType, String cNoLimit, VData cVData) {
		// 传入的参数不能为空，如果为空，则直接返回
		if((cNoType == null) || (cNoType.trim().length() <= 0) || (cNoLimit == null)) {
			logger.debug("NoType长度错误或者NoLimit为空");
			return null;
		}

		// 默认流水号位数
		int serialLen = 10;

		String tReturn = null;
		cNoType = cNoType.toUpperCase();

		// cNoLimit如果是SN类型，则cNoType只能是下面类型中的一个
		if("SN".equals(cNoLimit.trim().toUpperCase())) {
			if(cNoType.equals("GRPNO") || cNoType.equals("CUSTOMERNO") || cNoType.equals("SUGDATAITEMCODE")
					|| cNoType.equals("SUGITEMCODE") || cNoType.equals("SUGMODELCODE") || cNoType.equals("SUGCODE")
					|| cNoType.equals("ACCNO") || cNoType.equals("RPTONLYNO")) {
				serialLen = 10;
			} else if(cNoType.equals("COMMISIONSN")) {
				serialLen = 10;
			} else if(cNoType.equals("GRPPROPOSALNO")) {
				tReturn = tReturn + "12";
			} else if(cNoType.equals("YBTBATDAYNO")) {
				serialLen = 3;
			} else if(cNoType.equals("YBTSURRENDERDATA")) {
				serialLen = 9;
			}else if(cNoType.equals("LCWARNLOG")) {
				serialLen = 9;
			} else {
				logger.debug("错误的NoLimit");
				return null;
			}
		}

		if(cNoType.equals("YBTBATDAYNO")) {
			tReturn = (String) cVData.get(0);
		}

		// 扫描批次号,Added by niuzj 2006-04-27,每个分公司每天一个批次号
		// 前8位为机构代码,再8位为日期,再1位是业务类型,后3位为大流水号
		if(cNoType.equals("SCANNO")) {
			// cNoType为“SCANNO”
			// cNoLimit为登陆机构代码
			// cVData为业务类型
			String tStr = PubFun.getCurrentDate().substring(0,4);
			String tMng = cNoLimit.trim(); // 登录机构代码
			String tBusstype = ""; // 业务类型

			if(cVData.get(0).equals(null)) {
				logger.debug("传入的业务类型cVData为空");
				return null;
			} else {
				tBusstype = (String)cVData.get(0);
			}

			// 4位管理机构
			if(tMng.length() >= 4) {
				cNoLimit= tBusstype + tStr + tMng.substring(0, 4);
			} else {
				logger.debug("传入的机构代码cNoLimit错误");
				return null;
			}
			tReturn = cNoLimit;
			serialLen = 4; // 4位为大流水号
		}

		String tValue = getSEQNo(cNoType, tReturn);
		if(tValue != null && !"".equals(tValue)){
			tValue = PubFun.LCh(tValue, "0", serialLen);
		}

		if(tReturn.equals("SN") || cNoType.equals("YBTBATDAYNO")) {
			tReturn = tValue.trim();
		} else {
			tReturn = tReturn.trim() + tValue.trim();
		}
		logger.debug("------tReturn:" + tReturn);
		return tReturn;
	}

	/**
	 * <p>
	 * 生成流水号的函数
	 * <p>
	 * <p>
	 * 号码规则：机构编码 日期年 校验位 类型 流水号
	 * <p>
	 * <p>
	 * 1-6 7-10 11 12-13 14-20
	 * <p>
	 *
	 * @param cNoType
	 *          为需要生成号码的类型
	 * @param cNoLimit
	 *          为需要生成号码的限制条件（要么是SN，要么是机构编码）
	 * @return 生成的符合条件的流水号，如果生成失败，返回空字符串""
	 */
	public String CreateMaxNo(String cNoType, String cNoLimit) {
		// 传入的参数不能为空，如果为空，则直接返回
		if((cNoType == null) || (cNoType.trim().length() <= 0) || (cNoLimit == null)) {
			logger.debug("NoType长度错误或者NoLimit为空");
			return null;
		}

		// 默认流水号位数
		int serialLen = 10;
		if(cNoType.equals("OTOF")) {
			serialLen = 7;
		}

		String tReturn = null;
		cNoType = cNoType.toUpperCase();

		// cNoLimit如果是SN类型，则cNoType只能是下面类型中的一个  流水号
		if("SN".equals(cNoLimit.trim().toUpperCase())) {
			if(cNoType.equals("GRPNO") || cNoType.equals("CUSTOMERNO") || cNoType.equals("SUGDATAITEMCODE")
					|| cNoType.equals("SUGITEMCODE") || cNoType.equals("SUGMODELCODE") || cNoType.equals("SUGCODE")
					|| cNoType.equals("ACCNO") || cNoType.equals("RPTONLYNO") || cNoType.equals("ORGANCODE")
					|| cNoType.equals("SERIALNO2")) {
				serialLen = 10;
			}
			// add by renhl 修改为相当与个位加1 2008-10-14
			else if(cNoType.equals("COMMISIONSN")) {
				serialLen = 10;
			}
			else if(cNoType.equals("DIRPOLNO")) {
				serialLen = 15;
			}else if(cNoType.equals("YBTSURRENDERDATA")) {
				serialLen = 9;
			}else if(cNoType.equals("LCWARNLOG")) {
				serialLen = 9;
			}else {
				logger.debug("错误的NoLimit");
				return null;
			}
		}

		// 航意险6位流水号
		if(cNoType.equals("AIRPOLNO")) { // modify by yt 2002-11-04
			serialLen = 6;
		}
		// 特殊险种流水号长度处理
		if(cNoType.equals("AGENTCODE") || cNoType.equals("MANAGECOM")) {
			serialLen = 4;
		}
		if(cNoType.equals("ORGANCODE")){
			serialLen = 6;
		}
		//add for 团单打印回执单证号16位，流水号8位  by lifang start
		if(cNoType.equals("GRPRECEIPTNO")){
			serialLen = 8;
		}
		//add for 团单打印回执单证号16位，流水号8位  by lifang end

		tReturn = cNoLimit.trim();//对应前缀

		String tCom = ""; // 两位机构
		String tCenterCom = ""; // 中支公司

		if(tReturn.length() >= 4
				&& !(cNoType.toUpperCase().equals("CONTNO") || cNoType.toUpperCase().equals("PAYNO")
				|| cNoType.equals("PRTSEQNO") || cNoType.equals("GETNOTICENO") || cNoType.equals("GETNO")
				|| cNoType.equals("PAYNOTICENO") || cNoType.equals("GRPPROPOSALNO")|| cNoType.equals("GRPRGTNO") || cNoType.equals("GRPRPTNO")
				|| cNoType.equals("GRPRGTNO02") || cNoType.equals("GRPRGTNO11") || cNoType.equals("RPTNO")
				|| cNoType.equals("RGTNO") || cNoType.equals("GRPRGTNO03") || cNoType.equals("GRPCONTNO")
				|| cNoType.equals("SPECNO") || cNoType.equals("UWPRTSEQ")|| cNoType.equals("GRPRECEIPTNO"))) {
			tCom = tReturn.substring(2, 4);
			tCom = "0" + tCom; // 加一位较验位
			if(tReturn.length() >= 6) {
				tCenterCom = tReturn.substring(4, 6);
			} else {
				tCenterCom = "00";
			}
			tReturn = tReturn.substring(0, 4);
		}

		if(cNoType.equals("PROCESSID")) {
			tReturn = "6" + tReturn;
			serialLen = 10 - tReturn.length();
		}

		// 生成各种号码
		// 投保单险种号码
		if(cNoType.equals("PROPOSALNO")) {
			tReturn = "11" + tCom;
		}

		// 集体投保单险种号码
		else if(cNoType.equals("GRPPROPOSALNO")) {
			tReturn = tReturn + "12";
			serialLen = 7;
		}

		// 总单投保单号码
		else if(cNoType.equals("PROPOSALCONTNO")) {
			tReturn = "13" + tCom;
		}

		// 集体投保单号码ProposalGrpContNo最大长度为15，所以用ProGrpContNo代替
		else if(cNoType.equals("PROGRPCONTNO")) {
			tReturn = "14" + tCom;
		}

		// 保单险种号码
		else if(cNoType.equals("POLNO")) {
			tReturn = "21" + tCom;
		}

		// 集体保单险种号码
		else if(cNoType.equals("GRPPOLNO")) {
			tReturn = "22" + tCom;
		}

		// 合同号码
		else if(cNoType.equals("CONTNO")) {
			serialLen = 6;
		} else if(cNoType.equals("PRTSEQNO") || cNoType.equals("CCPRTSEQNO") || cNoType.equals("CCPRTSEQNO2")
				|| cNoType.equals("CCPRTSEQNO3") || cNoType.equals("CCPRTSEQNO4") || cNoType.equals("CCPRTSEQNO5")) {
			tReturn = cNoLimit;
			serialLen = 8;
		}
		// 集体合同号码 修改成5.3 GRPPOLNO的生成逻辑 modify on 2009-01-09
		else if(cNoType.equals("GRPCONTNO")) {
			serialLen = 6;
		}
		else if(cNoType.equals("PRTSEQNOLP5")){
			tReturn = cNoLimit;
			serialLen = 6;
		}
		// 交费通知书号码
		else if(cNoType.equals("PAYNOTICENO")) {
			tReturn = tReturn + "31";
			serialLen = 7;
		}

		// 交费通知书号码
		else if(cNoType.equals("PAYNO")) {
			tReturn = tReturn + "32";
			serialLen = 7; // 参考5.3 modify on 2009-01-09
		}
		// 交费收据号
		else if(cNoType.equals("GETNOTICENO")) {
			tReturn = tReturn + "36";
			serialLen = 7;
		}

		// 给付通知书号码
		else if(cNoType.equals("GETNO")) {
			tReturn = tReturn + "37";
			serialLen = 7; // 参考5.3
		}

		// 保全受理号码 //6+4+1+2+1+7（机构+年份 +校验位 + 特殊字符 +防充字符 +流水号）
		else if(cNoType.equals("EDORACCEPTNO")) { // changed by pst for
			tReturn = cNoLimit;
			serialLen = 8;
		}
		// 申请归档号，用于民生保全项目连续归档，在保全确认时生成
		else if(cNoType.equals("EDORCONFNO")) { // 连续
			tReturn = cNoLimit;
			serialLen = 8;
		}
		// 批改申请号码
		else if(cNoType.equals("EDORAPPNO")) { // 新华申请批单号和批单号合一，批单号可以不连续
			tReturn = cNoLimit;
			serialLen = 8;
		}
		// 批单号码
		else if(cNoType.equals("EDORNO")) {
			String tStr = PubFun.getCurrentDate();
			tReturn = cNoLimit.substring(0, 6) + tStr.substring(0, 4) + "46";
			serialLen = 7;
		}
		// 集体批单申请号码
		else if(cNoType.equals("EDORGRPAPPNO")) {
			tReturn = cNoLimit;
			serialLen = 8;
		}
		// 集体批单号码
		else if(cNoType.equals("EDORGRPNO")) {
			String tStr = PubFun.getCurrentDate();
			tReturn = cNoLimit.substring(0, 6) + tStr.substring(0, 4) + "44";
			serialLen = 7;
		}
		// 保全试算流水号
		else if(cNoType.equals("EDORTESTNO")) {
			tReturn = cNoLimit;
			serialLen = 8;
		}
		// 核保测算号码
		else if(cNoType.equals("ASKPRTNO")) {
			String tStr = PubFun.getCurrentDate();
			// 登录机构为86，则补两位！
			if(tReturn.length() == 2) {
				tReturn = "00" + tReturn;
			}
			tReturn = tReturn + tStr.substring(2, 4) + tStr.substring(5, 7) + tStr.substring(8, 10);
			cNoLimit = tReturn;
			serialLen = 3;
		}
		// 套餐编码
		else if(cNoType.equals("CONTPLANCODE")) {
			// 登录机构为86，则补两位！
			if(tReturn.length() == 2) {
				tReturn = tReturn + "00";
			} else {
				tReturn = tCom.substring(1, 3) + tCenterCom;
			}
			cNoLimit = tReturn;
			serialLen = 4;
		}

		// 单证扫描批次号,Added by niuzj 2005-10-22,每个分公司每天一个批次号
		// 前8位为机构代码,再8位为日期,后4位为大流水号
		else if(cNoType.equals("SCANNO")) {
			String tStr = PubFun.getCurrentDate();
			String tMng = cNoLimit.trim(); // 登录机构代码
			if(tMng.length() == 2) // 2位管理机构,前面补6个0
			{
				tReturn = "000000" + tMng + tStr.substring(0, 4) + tStr.substring(5, 7) + tStr.substring(8, 10);
			} else if(tMng.length() == 4) // 4位管理机构,前面补4个0
			{
				tReturn = "0000" + tMng + tStr.substring(0, 4) + tStr.substring(5, 7) + tStr.substring(8, 10);
			} else if(tMng.length() == 6) // 6位管理机构,前面补2个0
			{
				tReturn = "00" + tMng + tStr.substring(0, 4) + tStr.substring(5, 7) + tStr.substring(8, 10);
			} else // 8位管理机构,前面不补0
			{
				tReturn = tMng + tStr.substring(0, 4) + tStr.substring(5, 7) + tStr.substring(8, 10);
			}
			serialLen = 4; // 4位为大流水号
		}

		// 报案编号
		else if(cNoType.equals("RPTNO")) {
			serialLen = 6;
		}

		// 立案编号
		else if(cNoType.equals("RGTNO")) {
			serialLen = 6;
		}

		// 团体报案编号
		else if(cNoType.equals("GRPRPTNO")) {
			serialLen = 5;
		}

		// 账户案件团体立案编号
		else if(cNoType.equals("GRPRGTNO")) {
			serialLen = 6;
		}
		// 账户案件团体立案编号
		else if(cNoType.equals("GRPRGTNO02")) {
			serialLen = 6;
		}
		// 批量案件 团体立案编号
		else if(cNoType.equals("GRPRGTNO03")) {
			serialLen = 6;
		}
		// 普通案件团体立案编号
		else if(cNoType.equals("GRPRGTNO11")) {
			serialLen = 6;
		}
		// 赔案编号
		else if(cNoType.equals("CLMNO")) {
			tReturn = "52" + tCom;
		}
		// 拒案编号
		else if(cNoType.equals("DECLINENO")) {
			tReturn = "53" + tCom;
		}
		// 报案分案编号
		else if(cNoType.equals("SUBRPTNO")) {
			tReturn = "54" + tCom;
		}
		// 立案分案编号
		else if(cNoType.equals("CASENO")) {
			tReturn = "55" + tCom;
		}
		// 合同号
		else if(cNoType.equals("PROTOCOLNO")) {
			tReturn = "71" + tCom;
		}
		// 单证印刷号码
		else if(cNoType.equals("PRTNO")) {
			tReturn = "80" + tCom;
		}

		// 打印管理流水号
		else if(cNoType.equals("PRTSEQ2NO")) {
			tReturn = "82" + tCom;
		}
		// 保单打印批次号
		else if(cNoType.equals("CONTPRTSEQ")) {
			tCom = tCom.substring(1, 3);
			tReturn = "83" + tCom;
			serialLen = 7;
		}
		// 银保通对帐批次号
		else if(cNoType.equals("YBTNO")) {
			tReturn = "10";
			serialLen = 7;
		}
		// 回收清算单号
		else if(cNoType.equals("TAKEBACKNO")) {
			tReturn = "61" + tCom;
		}
		// 银行代扣代付批次号
		else if(cNoType.equals("BATCHNO")) {
			tReturn = "62" + tCom;
		}
		// 接口凭证id号
		else if(cNoType.equals("VOUCHERIDNO")) {
			tReturn = "63" + tCom;
		}
		// 佣金号码
		else if(cNoType.equals("WAGENO")) {
			tReturn = "90" + tCom;
		}
		// 卡单结算号
		else if(cNoType.equals("BALANCENO")) {
			String tStr = PubFun.getCurrentDate();
			tReturn = tReturn + tStr.substring(2, 4) + tStr.substring(5, 7) + tStr.substring(8, 10);
			serialLen = 4;
		}
		// add by likai 外网接口流水号
		else if(cNoType.equals("WEBSITE")) {
			String tStr = PubFun.getCurrentDate();
			tReturn = tReturn + tStr.substring(0, 4) + tStr.substring(5, 7) + tStr.substring(8, 10);
			serialLen = 10;
		}
		// 流水号
		else if(cNoType.equals("SERIALNO")) {
			// 万能险用的是99，5.3用的是98，目前6.5保持和5.3一致，用98 //modify by weikai
			tReturn = "98" + tCom;
		} else if(cNoType.equals("SPECNO")) {
			tReturn = tReturn + "99";
			serialLen = 7;
		}

		int tMaxNo = 0;
		cNoLimit = tReturn;
		if(cNoType.equals("UWPRTSEQ")) {
			serialLen = 8;
			tReturn = cNoLimit;
		}

		// 使用序列方式
		if(cNoType.equals("DOCID") || cNoType.equals("PAGEID") || cNoType.equals("UWPRTSEQ")) {
			tMaxNo = Integer.parseInt(getSEQNo(cNoType));
		}else if (cNoType.equals("CUSTOMERNO") || cNoType.equals("PROPOSALNO")) {
			tMaxNo = Integer.parseInt(getSEQNo(cNoType, cNoLimit));
		}else {
			//add yangheng 赔案号生成机制 start
			//modify by muyl 2016-02-18  start  赔案号按机构排序
			if( cNoType.equals("GRPRGTNO") || cNoLimit.equals("GRPRGTNO02") ||
					cNoLimit.equals("GRPRGTNO03") || cNoLimit.equals("GRPRGTNO11") || cNoLimit.equals("PRTSEQNOLP5")){
				cNoLimit = cNoLimit.substring(0, cNoLimit.length()-2);
			}
			//add yangheng 赔案号生成机制 end

			tMaxNo = Integer.parseInt(getSEQNo(cNoType, cNoLimit));
		}

		// 按照5.3的模式修改
		if(tReturn.length() >= 12 && serialLen < 8) {
			tReturn = tReturn.substring(0, 10) + "0" + tReturn.substring(10, 12);
		}

		String tStr = String.valueOf(tMaxNo);
		tStr = PubFun.LCh(tStr, "0", serialLen);
		if(cNoType.equals("CONTNO") && tReturn.equals("0")) {
			tReturn = "88";
		}

		if(tReturn.equals("SN")) {
			if(cNoType.toUpperCase().equals("ORGANCODE")) {
				tReturn = tStr.trim();
			}else if(cNoType.toUpperCase().equals("CUSTOMERNO")) {
				//modify by 王庆龙 2015-02-11 把客户号码变成以1开头的十位数字
				tReturn = tStr.trim().replaceFirst("0", "1");
			}
			else if(cNoType.equals("COMMISIONSN")||cNoType.equals("SERIALNO2")) {
				tReturn = tStr.trim();
			} else {
				tReturn = tStr.trim() + "0";
			}
		} else if(tReturn.equals("RECO")) {
			tReturn = tStr.trim();
		} else {
			tReturn = tReturn.trim() + tStr.trim();
		}
		logger.debug("------tReturn:" + tReturn);
		return tReturn;
	}

	/**
	 * 功能：产生指定长度的流水号，一个号码类型一个流水
	 */
	public String CreateMaxNo(String cNoType, int cNoLength) {
		if((cNoType == null) || (cNoType.trim().length() <= 0) || (cNoLength <= 0)) {
			logger.debug("NoType长度错误或NoLength错误");
			return null;
		}

		cNoType = cNoType.toUpperCase();
		String tValue = "";

		if(cNoType.equals("DOCID") || cNoType.equals("PAGEID") || cNoType.equals("MISSIONID")
				|| cNoType.equals("MISSIONSERIELNO") || cNoType.equals("LOPRTMANAGERBSERIALNO")
				|| cNoType.equals("LBISSUEPOLSERIALNOB")) {
			tValue = getSEQNo(cNoType);
			if(tValue != null && !"".equals(tValue)){
				tValue = PubFun.LCh(tValue, "0", cNoLength);
			}
		}else{
			if("CFAGENTCODE".equals(cNoType)){
				tValue = getSEQNo1(cNoType, "SN");
				if(tValue != null && !"".equals(tValue)){
					tValue = PubFun.LCh(tValue, "0", cNoLength);
				}
			}else{
				tValue = getSEQNo(cNoType, "SN");
				if(tValue != null && !"".equals(tValue)){
					tValue = PubFun.LCh(tValue, "0", cNoLength);
				}
			}
		}
		return tValue;
	}

	/**
	 * 没有后缀的序列  REQ-1183  财富人员编码  递减
	 * @param cNoType
	 * @return
	 */
	private String getSEQNo1(String cNoType, String noLimit){
		//使用序列方式
		try {
			String tSQL_SEQ = " select " + "SEQ_" + cNoType + "_" + noLimit + ".nextval from dual ";
			ExeSQL tExeSQL = new ExeSQL();
			String tValue = "";
			tValue = tExeSQL.getOneValue(tSQL_SEQ);
//	        logger.debug("sequence value------" + tValue);

			// 如果返回信息为空，表示序列不存在
			if ("".equals(tValue)){
				StringBuffer tSBDDLSql = new StringBuffer(128);
				tSBDDLSql.append("create sequence SEQ_");
				tSBDDLSql.append(cNoType);
				tSBDDLSql.append("_");
				tSBDDLSql.append(noLimit);
				tSBDDLSql.append(" minvalue 1 maxvalue 99999 start with 99999 increment by -1 nocache");

				boolean tFlag = tExeSQL.execUpdateSQL(tSBDDLSql.toString());
				// 判断序列创建是否成功
				if (tFlag){
					tValue = tExeSQL.getOneValue(tSQL_SEQ.toString());
				}else{
					// 如果创建sequence失败，则返回null，表示生成最大号失败
					logger.debug(tExeSQL.mErrors.getFirstError());
					return null;
				}
			}
			return tValue.toString();
		} catch(Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * 没有后缀的序列
	 * @param cNoType
	 * @return
	 */
	private String getSEQNo(String cNoType){
		//使用序列方式
		try {
			String tSQL_SEQ = " select " + "SEQ_" + cNoType + ".nextval from dual ";
			ExeSQL tExeSQL = new ExeSQL();
			String tValue = "";
			tValue = tExeSQL.getOneValue(tSQL_SEQ);
//	        logger.debug("sequence value------" + tValue);

			// 如果返回信息为空，表示序列不存在
			if ("".equals(tValue)){
				StringBuffer tSBDDLSql = new StringBuffer(128);
				tSBDDLSql.append("create sequence SEQ_");
				tSBDDLSql.append(cNoType);
				tSBDDLSql.append(" minvalue 1 maxvalue 999999999999 start with 1 increment by 1 nocache");

				boolean tFlag = tExeSQL.execUpdateSQL(tSBDDLSql.toString());
				// 判断序列创建是否成功
				if (tFlag){
					tValue = tExeSQL.getOneValue(tSQL_SEQ.toString());
				}else{
					// 如果创建sequence失败，则返回null，表示生成最大号失败
					logger.debug(tExeSQL.mErrors.getFirstError());
					return null;
				}
			}
			return tValue.toString();
		} catch(Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * 没有后缀的序列
	 * @param cNoType
	 * @return
	 */
	private String getSEQNo(String cNoType, String noLimit){
		//使用序列方式
		try {
			String tSQL_SEQ = " select " + "SEQ_" + cNoType + "_" + noLimit + ".nextval from dual ";
			ExeSQL tExeSQL = new ExeSQL();
			String tValue = "";
			tValue = tExeSQL.getOneValue(tSQL_SEQ);
//	        logger.debug("sequence value------" + tValue);

			// 如果返回信息为空，表示序列不存在
			if ("".equals(tValue)){
				StringBuffer tSBDDLSql = new StringBuffer(128);
				tSBDDLSql.append("create sequence SEQ_");
				tSBDDLSql.append(cNoType);
				tSBDDLSql.append("_");
				tSBDDLSql.append(noLimit);
				tSBDDLSql.append(" minvalue 1 maxvalue 999999999999 start with 1 increment by 1 nocache");

				boolean tFlag = tExeSQL.execCreateSeq(tSBDDLSql.toString());
				// 判断序列创建是否成功
				if (tFlag){
					tValue = tExeSQL.getOneValue(tSQL_SEQ.toString());
				}else{
					// 如果创建sequence失败，则返回null，表示生成最大号失败
					logger.debug(tExeSQL.mErrors.getFirstError());
					return null;
				}
			}
			return tValue.toString();
		} catch(Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public static void main(String[] args) {
		String tLimit = PubFun.getNoLimit("86010101");
		String tGetNoticeNo = PubFun1.CreateMaxNo("PAYNOTICENO", tLimit);
		System.out.println("获得新的保单号码：" + tGetNoticeNo);
	}

	/*@Override
	public String CreateMaxNo(String cNoType, String cNoLimit, int stepLength) {
		SysMaxNoStep tSysMaxNoStep = new SysMaxNoStep();
		String tValue = tSysMaxNoStep.CreateMaxNo(cNoType, cNoLimit, stepLength);
		return tValue;
	}*/
}
