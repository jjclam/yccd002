package com.sinosoft.lis.maxno;

import com.sinosoft.sn.AbstractSNGenerator;
import com.sinosoft.sn.generator.GeneratorTypeSet;
import com.sinosoft.sn.generator.ParamConsts;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by filon51 on 2016/7/27.
 */
@Service
public class SampleSNGenerator extends AbstractSNGenerator {
    @Override
    protected Map prepareMap() {
        Map parameterMap = new HashMap(); //设定参数
        parameterMap.put(ParamConsts.SERIAL_NO_TYPE, "SampleID");
        parameterMap.put(ParamConsts.SERIAL_NO_LIMIT, "SN");
        return parameterMap;
    }

    @Override
    protected String prepareFormatStr() {
        String snFormatStr
                = GeneratorTypeSet.FIXED_STRING + ParamConsts.SPLIT_SUB_GENERATOR_INNER + " 中科软〔"
                + ParamConsts.SPLIT_SUB_GENERATOR + GeneratorTypeSet.DATE_TIME + ParamConsts.SPLIT_SUB_GENERATOR_INNER + "yyyy-mm-dd hh:mm:ss"
                + ParamConsts.SPLIT_SUB_GENERATOR + GeneratorTypeSet.FIXED_STRING + ParamConsts.SPLIT_SUB_GENERATOR_INNER + "〕"
                + ParamConsts.SPLIT_SUB_GENERATOR + GeneratorTypeSet.NUMBER_SEQUENCE + ParamConsts.SPLIT_SUB_GENERATOR_INNER + "8" + ParamConsts.SPLIT_C + "0"
                + ParamConsts.SPLIT_SUB_GENERATOR + GeneratorTypeSet.FIXED_STRING + ParamConsts.SPLIT_SUB_GENERATOR_INNER + " 号";
        return snFormatStr;
    }


}
