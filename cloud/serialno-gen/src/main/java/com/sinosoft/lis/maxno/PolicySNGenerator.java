package com.sinosoft.lis.maxno;

import com.sinosoft.sn.AbstractSNGenerator;
import com.sinosoft.sn.generator.GeneratorTypeSet;
import com.sinosoft.sn.generator.ParamConsts;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.HashMap;
import java.util.Map;

/**
 * 保单号自动生成程序
 * Created by kaine on 2016/8/10.
 */
public class PolicySNGenerator extends AbstractSNGenerator {
    Log logger = LogFactory.getLog(PolicySNGenerator.class);

    // 默认机构编码
    private String manageCom = "0000";

    /**
     * 准备流水号生成需要的参数MAP
     *
     * @return
     */
    @Override
    protected Map prepareMap() {
        Map parameterMap = new HashMap();
        parameterMap.put(ParamConsts.SERIAL_NO_TYPE, "PolicyCode");
        parameterMap.put(ParamConsts.SERIAL_NO_LIMIT, "SN");

        return parameterMap;
    }

    /**
     * 保单号生成规则
     *
     * @return
     */
    @Override
    protected String prepareFormatStr() {
        String snFormatStr
                // 4位网销固定开头：1098
                = GeneratorTypeSet.FIXED_STRING + ParamConsts.SPLIT_SUB_GENERATOR_INNER + "1098"
                + ParamConsts.SPLIT_SUB_GENERATOR
                // 4位二、三级机构代码
                + GeneratorTypeSet.FIXED_STRING + ParamConsts.SPLIT_SUB_GENERATOR_INNER + manageCom
                + ParamConsts.SPLIT_SUB_GENERATOR
                // 2位年份
                + GeneratorTypeSet.DATE_TIME + ParamConsts.SPLIT_SUB_GENERATOR_INNER + "yy"
                + ParamConsts.SPLIT_SUB_GENERATOR
                // 9位流水号
                + GeneratorTypeSet.NUMBER_SEQUENCE + ParamConsts.SPLIT_SUB_GENERATOR_INNER + "9" + ParamConsts.SPLIT_C + "0"
                + ParamConsts.SPLIT_SUB_GENERATOR
                // 1位固定结尾：6
                + GeneratorTypeSet.FIXED_STRING + ParamConsts.SPLIT_SUB_GENERATOR_INNER + "6";
        if (logger.isDebugEnabled()) {
            logger.debug(snFormatStr);
        }
        return snFormatStr;
    }


    /**
     * 设置管理机构
     * @param manageCom manageCom
     */
    public void setManageCom(String manageCom) {
        if (manageCom.length() >= 6) {
            this.manageCom = manageCom.substring(2, 6);
        } else {
            StringBuffer sb = new StringBuffer();
            sb.append(manageCom);
            if (manageCom == null || manageCom.length() < 6) {
                sb.append("000000");
            }
            this.manageCom = sb.substring(2, 6);
        }
        logger.debug(this.manageCom);
    }
}

