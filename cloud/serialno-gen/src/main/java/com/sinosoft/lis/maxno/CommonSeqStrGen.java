package com.sinosoft.lis.maxno;

import com.sinosoft.sn.AbstractSNGenerator;
import com.sinosoft.sn.generator.GeneratorTypeSet;
import com.sinosoft.sn.generator.ParamConsts;

import java.util.HashMap;
import java.util.Map;

/**
 * 通用生成器，生成左补0的流水号串
 * Created by filon51 on 2016/8/2.
 */
public class CommonSeqStrGen extends AbstractSNGenerator {
    private String noType = "";
    private String noLimit = "SN"; //走流水号
    private int noLength = 0;

    @Override
    protected Map prepareMap() {
        Map parameterMap = new HashMap(); //设定参数
        parameterMap.put(ParamConsts.SERIAL_NO_TYPE, this.getNoType());
        parameterMap.put(ParamConsts.SERIAL_NO_LIMIT, this.getNoLimit());
        return parameterMap;
    }

    @Override
    protected String prepareFormatStr() {
        String snFormatStr
                = GeneratorTypeSet.NUMBER_SEQUENCE + ParamConsts.SPLIT_SUB_GENERATOR_INNER + this.getNoLength() + ParamConsts.SPLIT_C + "0";
        return snFormatStr;
    }

    public String getNoType() {
        return noType;
    }

    public void setNoType(String noType) {
        this.noType = noType;
    }

    public String getNoLimit() {
        return noLimit;
    }

    public int getNoLength() {
        return noLength;
    }

    /**
     *
     * @param noLength 长度
     */
    public void setNoLength(int noLength) {
        if (noLength < 2) {
            noLength = 8;
        }
        this.noLength = noLength;
    }
//
//    public void setNoLimit(String noLimit) {
//        this.noLimit = noLimit;
//    }
}
