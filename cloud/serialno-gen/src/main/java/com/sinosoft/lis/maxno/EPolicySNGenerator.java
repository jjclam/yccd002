package com.sinosoft.lis.maxno;

import com.sinosoft.sn.AbstractSNGenerator;
import com.sinosoft.sn.generator.GeneratorTypeSet;
import com.sinosoft.sn.generator.ParamConsts;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.HashMap;
import java.util.Map;

/**
 * 电子保单号自动生成程序
 * Created by kaine on 2016/8/17.
 */
public class EPolicySNGenerator extends AbstractSNGenerator {
    private static final Log logger = LogFactory.getLog(EPolicySNGenerator.class);

    /**
     * 准备流水号生成需要的参数MAP
     *
     * @return
     */
    @Override
    protected Map prepareMap() {
        Map parameterMap = new HashMap();
        parameterMap.put(ParamConsts.SERIAL_NO_TYPE, "EPolicyCode");
        parameterMap.put(ParamConsts.SERIAL_NO_LIMIT, "SN");

        return parameterMap;
    }

    /**
     * 准备流水号生成规则
     *
     * @return
     */
    @Override
    protected String prepareFormatStr() {
        String snFormatStr
                // 10位网销固定开头：1110889800
                = GeneratorTypeSet.FIXED_STRING + ParamConsts.SPLIT_SUB_GENERATOR_INNER + "1110889800"
                + ParamConsts.SPLIT_SUB_GENERATOR
                // 2位年份
                + GeneratorTypeSet.DATE_TIME + ParamConsts.SPLIT_SUB_GENERATOR_INNER + "yy"
                + ParamConsts.SPLIT_SUB_GENERATOR
                // 1位网销固定开头：0
                + GeneratorTypeSet.FIXED_STRING + ParamConsts.SPLIT_SUB_GENERATOR_INNER + "0"
                + ParamConsts.SPLIT_SUB_GENERATOR
                // 7位流水号
                + GeneratorTypeSet.NUMBER_SEQUENCE + ParamConsts.SPLIT_SUB_GENERATOR_INNER + "7" + ParamConsts.SPLIT_C + "0";
        if (logger.isDebugEnabled()) {
            logger.debug(snFormatStr);
        }
        return snFormatStr;
    }
}
