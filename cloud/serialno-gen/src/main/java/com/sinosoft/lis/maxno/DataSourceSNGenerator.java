package com.sinosoft.lis.maxno;

import com.sinosoft.sn.AbstractSNGenerator;
import com.sinosoft.sn.generator.GeneratorTypeSet;
import com.sinosoft.sn.generator.ParamConsts;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by kaine on 2016/12/15.
 */
public class DataSourceSNGenerator extends AbstractSNGenerator {
    private static final Log logger = LogFactory.getLog(DataSourceSNGenerator.class.getName());

    /**
     * 准备流水号生成需要的参数MAP
     *
     * @return
     */
    @Override
    protected Map prepareMap() {
        Map parameterMap = new HashMap();
        parameterMap.put(ParamConsts.SERIAL_NO_TYPE, "DataSource");
        parameterMap.put(ParamConsts.SERIAL_NO_LIMIT, "SN");

        return parameterMap;
    }

    /**
     * 保单号生成规则
     *
     * @return
     */
    @Override
    protected String prepareFormatStr() {
        String snFormatStr
                // 自动生成的
                = GeneratorTypeSet.FIXED_STRING + ParamConsts.SPLIT_SUB_GENERATOR_INNER + "AutoCreate"
                + ParamConsts.SPLIT_SUB_GENERATOR
                // 3位流水号
                + GeneratorTypeSet.NUMBER_SEQUENCE + ParamConsts.SPLIT_SUB_GENERATOR_INNER + "3" + ParamConsts.SPLIT_C + "0"
                + ParamConsts.SPLIT_SUB_GENERATOR;
        if (logger.isDebugEnabled()) {
            logger.debug(snFormatStr);
        }
        return snFormatStr;
    }
}