/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 
 */
package com.sinosoft.lis.pubfun;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * <p>
 * Title: Web业务系统
 * </p>
 * <p>
 * Description:系统号码管理（新华人寿核心业务系统）生成系统号码
 * </p>
 * <p>
 * Copyright: Copyright (c) 2005
 * </p>
 * <p>
 * Company: Sinosoft
 * </p>
 * @author Liuqiang
 * @version 1.0
 */

import java.sql.Connection;

import com.sinosoft.utility.DBConnPool;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.*;
//import com.sinosoft.lis.f1print.PrintManagerBL;
import java.math.BigInteger;

public class SysMaxNoAB implements SysMaxNo
{
	private final static Log log4j = LogFactory.getLog(SysMaxNoAB.class);

	private String ENDORSEMENTCODE = "3013";// 批单单证的类型编码 生成保全批单流水号需用 Added By
											// Qisl At 2007.12.28

	public SysMaxNoAB()
	{}
	private String tNoLimit;
	/**
	 * <p>
	 * 生成流水号的函数
	 * <p>
	 * <p>
	 * 号码规则：机构编码 日期年 校验位 类型 流水号
	 * <p>
	 * <p>
	 * 1-6 7-10 11 12-13 14-20
	 * <p>
	 * @param cNoType 为需要生成号码的类型
	 * @param cNoLimit 为需要生成号码的限制条件（要么是SN，要么是机构编码）
	 * @param cVData 为需要生成号码的业务相关限制条件
	 * @return 生成的符合条件的流水号，如果生成失败，返回空字符串""
	 */
	public String CreateMaxNo(String cNoType, String cNoLimit, VData cVData)
	{
		// 传入的参数不能为空，如果为空，则直接返回
		if ((cNoType == null) || (cNoType.trim().length() <= 0) || (cNoLimit == null))
		{
			log4j.info("NoType长度错误或者NoLimit为空");
			return null;
		}

		// 默认流水号位数
		int serialLen = 10;
		String tReturn = null;
		cNoType = cNoType.toUpperCase();
		// log4j.info("-----------cNoType:"+cNoType+"
		// cNoLimit:"+cNoLimit);

		// cNoLimit如果是SN类型，则cNoType只能是下面类型中的一个
		if (cNoLimit.trim().toUpperCase().equals("SN"))
		{
			if (cNoType.equals("COMMISIONSN") || cNoType.equals("GRPNO") || cNoType.equals("CUSTOMERNO")
					|| cNoType.equals("SUGDATAITEMCODE") || cNoType.equals("SUGITEMCODE")
					|| cNoType.equals("SUGMODELCODE") || cNoType.equals("SUGCODE") || cNoType.equals("ACCNO")
					|| cNoType.equals("RPTONLYNO")  || cNoType.equals("TASKRUNLOGSN"))
			{
				serialLen = 9;
			}
			else
			{
				log4j.info("错误的NoLimit");
				return null;
			}
		}

		// 扫描批次号,Added by niuzj 2006-04-27,每个分公司每天一个批次号
		// 前8位为机构代码,再8位为日期,再1位是业务类型,后3位为大流水号
		if (cNoType.equals("SCANNO"))
		{
			// cNoType为“SCANNO”
			// cNoLimit为登陆机构代码
			// cVData为业务类型
			String tStr = PubFun.getCurrentDate();
			String tMng = cNoLimit.trim(); // 登录机构代码
			String tBusstype = ""; // 业务类型

			if (cVData.get(0).equals(null))
			{
				log4j.info("传入的业务类型cVData为空");
				return null;
			}
			// else if (cVData.get(0).equals("TB")) { //新契约
			// tBusstype = "1";
			// }
			// else if (cVData.get(0).equals("BQ")) { //保全
			// tBusstype = "2";
			// }
			// else if (cVData.get(0).equals("LP")) { //理赔
			// tBusstype = "3";
			// }
			// else if (cVData.get(0).equals("XQ")) { //续期
			// tBusstype = "4";
			// }
			else if (cVData.get(0).equals("TB"))
			{ // 新契约
				tBusstype = "1";
			}
			else if (cVData.get(0).equals("IC"))
			{ // 个险契约
				tBusstype = "2";
			}
			else if (cVData.get(0).equals("GC"))
			{ // 团险契约
				tBusstype = "3";
			}
			else if (cVData.get(0).equals("BQ"))
			{ // 保全
				tBusstype = "4";
			}
			else if (cVData.get(0).equals("LP"))
			{ // 理赔
				tBusstype = "5";
			}
			else if (cVData.get(0).equals("XQ"))
			{ // 续期
				tBusstype = "6";
			}
			else
			{
				log4j.info("传入的业务类型cVData错误");
				return null;
			}

			if (tMng.length() == 2)
			{ // 2位管理机构,前面补6个0
				tReturn = "000000" + tMng + tStr.substring(0, 4) + tStr.substring(5, 7) + tStr.substring(8, 10)
						+ tBusstype;
			}
			else if (tMng.length() == 4)
			{ // 4位管理机构,前面补4个0
				tReturn = "0000" + tMng + tStr.substring(0, 4) + tStr.substring(5, 7) + tStr.substring(8, 10)
						+ tBusstype;
			}
			else if (tMng.length() == 6)
			{ // 6位管理机构,前面补2个0
				tReturn = "00" + tMng + tStr.substring(0, 4) + tStr.substring(5, 7) + tStr.substring(8, 10) + tBusstype;
			}
			else if (tMng.length() == 8)
			{ // 8位管理机构,前面不补0
				tReturn = tMng + tStr.substring(0, 4) + tStr.substring(5, 7) + tStr.substring(8, 10) + tBusstype;
			}
			else
			{
				log4j.info("传入的机构代码cNoLimit错误");
				return null;
			}
			serialLen = 3; // 3位为大流水号
		}

		Connection conn = DBConnPool.getConnection();

		if (conn == null)
		{
			log4j.info("CreateMaxNo : fail to get db connection");
			return tReturn;
		}

		BigInteger tMaxNo = new BigInteger("0");
		cNoLimit = tReturn;
		// log4j.info("cNoLimit:"+cNoLimit);

		try
		{
			// 开始事务
			// 查询结果有3个： -- added by Fanym
			// 全部采用直接执行SQL语句，只要有其他事务锁定了本行，立即返回NULL
			// 如果没有锁定，则本事务锁定，查询得到结果则UPDATE，没有则INSERT
			conn.setAutoCommit(false);
	         String tSQL = " select createmaxno('" + cNoType + "','" + cNoLimit +
             "') from dual ";
			ExeSQL exeSQL = new ExeSQL(conn);
			String result = null;
			result = exeSQL.getOneValue(tSQL);
			tMaxNo = new BigInteger(result);
			conn.commit();
			conn.close();
//-------------------2011-2-22 废弃  start--------------------------
//			String strSQL = "select MaxNo from LDMaxNo where notype='" + cNoType + "' and nolimit='" + cNoLimit
//					+ "' for update";
//
//			ExeSQL exeSQL = new ExeSQL(conn);
//			String result = null;
//			result = exeSQL.getOneValue(strSQL);
//
//			// 测试返回bull时
//			if (exeSQL.mErrors.needDealError())
//			{
//				log4j.info("查询LDMaxNo出错，请稍后!");
//				conn.rollback();
//				conn.close();
//
//				return null;
//			}
//
//			if ((result == null) || result.equals(""))
//			{
//				strSQL = "insert into ldmaxno(notype, nolimit, maxno) values('" + cNoType + "', '" + cNoLimit + "', 1)";
//				exeSQL = new ExeSQL(conn);
//
//				if (!exeSQL.execUpdateSQL(strSQL))
//				{
//					log4j.info("CreateMaxNo 插入失败，请重试!");
//					conn.rollback();
//					conn.close();
//
//					return null;
//				}
//				else
//				{
//					tMaxNo = 1;
//				}
//			}
//			else
//			{
//				strSQL = "update ldmaxno set maxno = maxno + 1" + " where notype = '" + cNoType + "' and nolimit = '"
//						+ cNoLimit + "'";
//				exeSQL = new ExeSQL(conn);
//
//				if (!exeSQL.execUpdateSQL(strSQL))
//				{
//					log4j.info("CreateMaxNo 更新失败，请重试!");
//					conn.rollback();
//					conn.close();
//
//					return null;
//				}
//				else
//				{
//					tMaxNo = Integer.parseInt(result) + 1;
//				}
//			}
//
//			conn.commit();
//			conn.close();
//			-------------------2011-2-22 废弃  end--------------------------
		}
		catch (Exception Ex)
		{
			try
			{
				conn.rollback();
				conn.close();

				return null;
			}
			catch (Exception e1)
			{
				e1.printStackTrace();

				return null;
			}
		}

		String tStr = String.valueOf(tMaxNo);
		tStr = PubFun.LCh(tStr, "0", serialLen);
		if (tReturn.equals("SN"))
		{
			tReturn = tStr.trim();
			// tReturn = tStr.trim() + "0";
		}
		else
		{
			tReturn = tReturn.trim() + tStr.trim();
		}
		log4j.info("------tReturn:" + tReturn);
		return tReturn;
	}

	/**
	 * <p>
	 * 生成流水号的函数
	 * <p>
	 * <p>
	 * 号码规则：机构编码 日期年 校验位 类型 流水号
	 * <p>
	 * <p>
	 * 1-6 7-10 11 12-13 14-20
	 * <p>
	 * @param cNoType 为需要生成号码的类型
	 * @param cNoLimit 为需要生成号码的限制条件（要么是SN，要么是机构编码）
	 * @return 生成的符合条件的流水号，如果生成失败，返回空字符串""
	 */
	public String CreateMaxNo(String cNoType, String cNoLimit)
	{

		// 传入的参数不能为空，如果为空，则直接返回
		if ((cNoType == null) || (cNoType.trim().length() <= 0) || (cNoLimit == null))
		{
			log4j.info("NoType长度错误或者NoLimit为空");

			return null;
		}

		// 默认流水号位数
		int serialLen = 10;
		String tReturn = null;
		cNoType = cNoType.toUpperCase();
		// log4j.info("-----------cNoType:"+cNoType+"
		// cNoLimit:"+cNoLimit);

		// cNoLimit如果是SN类型，则cNoType只能是下面类型中的一个
		if (cNoLimit.trim().toUpperCase().equals("SN"))
		{ // modify by yt 2002-11-04
			// if (cNoType.equals("GRPNO") || cNoType.equals("CUSTOMERNO") ||
			// cNoType.equals("SugDataItemCode") ||
			// cNoType.equals("SugItemCode") || cNoType.equals("SugModelCode")
			// || cNoType.equals("SugCode"))
			if (cNoType.equals("COMMISIONSN") || cNoType.equals("GRPNO") || cNoType.equals("CUSTOMERNO")
					|| cNoType.equals("SUGDATAITEMCODE") || cNoType.equals("SUGITEMCODE")
					|| cNoType.equals("SUGMODELCODE") || cNoType.equals("SUGCODE") || cNoType.equals("ACCNO")
					|| cNoType.equals("RPTONLYNO") || cNoType.equals("TASKRUNLOGSN"))
			{
				serialLen = 9;
			}
			else
			{
				log4j.info("错误的NoLimit");

				return null;
			}
		}

		// 航意险6位流水号
		if (cNoType.equals("AIRPOLNO"))
		{ // modify by yt 2002-11-04
			serialLen = 6;
		}
		// 特殊险种流水号长度处理
		if (cNoType.equals("MANAGECOM"))
		{
			serialLen = 4;
		}

		tReturn = cNoLimit.trim();
		// log4j.info("tReturn:"+tReturn);

		String tCom = ""; // 两位机构
		String tCenterCom = ""; // 中支公司

		if (tReturn.length() >= 4)
		{
			tCom = tReturn.substring(2, 4);
			tCom = "0" + tCom; // 加一位较验位
			if (tReturn.length() >= 6)
			{
				tCenterCom = tReturn.substring(4, 6);
			}
			else
			{
				tCenterCom = "00";
			}
			tReturn = tReturn.substring(0, 4);
		}

		// 生成各种号码
		// 投保单险种号码
		if (cNoType.equals("PROPOSALNO"))
		{
			tReturn = "11" + tCom;
		}

		// 集体投保单险种号码
		else if (cNoType.equals("GRPPROPOSALNO"))
		{
			tReturn = "12" + tCom;
		}

		// 总单投保单号码
		else if (cNoType.equals("PROPOSALCONTNO"))
		{
			tReturn = "13" + tCom;
		}

		// 集体投保单号码ProposalGrpContNo,LDMaxNo里最大长度为15，所以用ProGrpContNo代替
		else if (cNoType.equals("PROGRPCONTNO"))
		{
			tReturn = "14" + tCom;
		}

		// 保单险种号码
		else if (cNoType.equals("POLNO"))
		{
			tReturn = "21" + tCom;
		}

		// 集体保单险种号码
		else if (cNoType.equals("GRPPOLNO"))
		{
			tReturn = "22" + tCom;
		}

		// 合同号码
		else if (cNoType.equals("CONTNO"))
		{
			//cNoLimit为 6为保单管理机构+4为年度+个团标志
			//tNoLimit=cNoLimit;
			//tReturn = cNoLimit.substring(6,10);
			//serialLen = 7;

			tNoLimit=cNoLimit;
			tReturn = cNoLimit.substring(8,10);
			serialLen = 9;
		}

		// 集体合同号码
		else if (cNoType.equals("GRPCONTNO"))
		{
			tReturn = "24" + tCom;
		}

		// 交费通知书号码
		else if (cNoType.equals("PAYNOTICENO"))
		{
			tReturn = "31" + tCom;
			// tReturn = tCom.substring(1);
			serialLen = 8;
		}

		// 交费收据号码
		else if (cNoType.equals("PAYNO"))
		{
			tReturn = "32" + tCom;
		}

		// 给付通知书号码
		else if (cNoType.equals("GETNOTICENO"))
		{
			tReturn = "36" + tCom;
			serialLen = 8;
		}

		// 给付通知书号码
		else if (cNoType.equals("GETNO"))
		{
			tReturn = "37" + tCom;
		}

		// 保全受理号码
		else if (cNoType.equals("EDORACCEPTNO"))
		{
			// String tStr = PubFun.getCurrentDate();
			// tReturn = "61" + tStr.substring(0, 4) + tStr.substring(5, 7) +
			// tStr.substring(8, 10);
			// serialLen = 6;
			tReturn = "3" + cNoLimit.substring(6, cNoLimit.length()) + cNoLimit.substring(2, 4);
			serialLen = 7;
		}
		// 保全试算流水号
		else if (cNoType.equals("EDORTESTNO"))
		{
			String tStr = PubFun.getCurrentDate();
			tReturn = "64" + tStr.substring(0, 4) + tStr.substring(5, 7) + tStr.substring(8, 10);
			serialLen = 6;
		}
		// 批改申请号码
//		else if (cNoType.equals("EDORAPPNO"))
//		{ // 新华申请批单号和批单号合一，批单号可以不连续
//		// String tStr = PubFun.getCurrentDate();
//		// tReturn = "60" + tStr.substring(0, 4) + tStr.substring(5, 7) +
//		// tStr.substring(8, 10);
//		// serialLen = 6;
//		//tReturn = ENDORSEMENTCODE + cNoLimit.substring(6, cNoLimit.length()) + cNoLimit.substring(2, 4);
//		//serialLen = 7;
//		//保全申请书号  保全批单号 :单证编码（4位）+ 年份（4位）+版本号（2位）+流水号（8位）
//			tReturn = "3003" + cNoLimit.substring(6, cNoLimit.length()) + PrintManagerBL.CODE_VERSION;
//			serialLen = 8;
//		}

		// 批单号码
		else if (cNoType.equals("EDORNO"))
		{
			// String tStr = PubFun.getCurrentDate();
			// tReturn = "60" + tStr.substring(0, 4) + tStr.substring(5, 7) +
			// tStr.substring(8, 10);
			// serialLen = 6;
			tReturn = ENDORSEMENTCODE + cNoLimit.substring(6, cNoLimit.length()) + cNoLimit.substring(2, 4);
			serialLen = 7;
		}

		// 集体批单申请号码
		else if (cNoType.equals("EDORGRPAPPNO"))
		{
			// String tStr = PubFun.getCurrentDate();
			// tReturn = "63" + tStr.substring(0, 4) + tStr.substring(5, 7) +
			// tStr.substring(8, 10);
			// serialLen = 6;
			tReturn = ENDORSEMENTCODE + cNoLimit.substring(6, cNoLimit.length()) + cNoLimit.substring(2, 4);
			serialLen = 7;
		}

		// 集体批单号码
		else if (cNoType.equals("EDORGRPNO"))
		{
			// String tStr = PubFun.getCurrentDate();
			// tReturn = "63" + tStr.substring(0, 4) + tStr.substring(5, 7) +
			// tStr.substring(8, 10);
			// serialLen = 6;
			tReturn = ENDORSEMENTCODE + cNoLimit.substring(6, cNoLimit.length()) + cNoLimit.substring(2, 4);
			serialLen = 7;
		}

		// 核保测算号码
		else if (cNoType.equals("ASKPRTNO"))
		{
			String tStr = PubFun.getCurrentDate();
			// 登录机构为86，则补两位！
			if (tReturn.length() == 2)
			{
				tReturn = "00" + tReturn;
			}
			tReturn = tReturn + tStr.substring(2, 4) + tStr.substring(5, 7) + tStr.substring(8, 10);
			cNoLimit = tReturn;
			serialLen = 3;
		}
		// 套餐编码
		else if (cNoType.equals("CONTPLANCODE"))
		{
			// 登录机构为86，则补两位！
			if (tReturn.length() == 2)
			{
				tReturn = tReturn + "00";
			}
			else
			{
				tReturn = tCom.substring(1, 3) + tCenterCom;
			}
			cNoLimit = tReturn;
			serialLen = 4;
		}
		//
		else if(cNoType.equals("RECEIPTNO"))//个险保单收据号码
		{
			String tStr = PubFun.getCurrentDate();

			tReturn ="5101"+tStr.substring(0, 4);
			serialLen = 7; // 7位为大流水号
		}
		// 单证扫描批次号,Added by niuzj 2005-10-22,每个分公司每天一个批次号
		// 前8位为机构代码,再8位为日期,后4位为大流水号
		else if (cNoType.equals("SCANNO"))
		{
			String tStr = PubFun.getCurrentDate();
			String tMng = cNoLimit.trim(); // 登录机构代码
			if (tMng.length() == 2) // 2位管理机构,前面补6个0
			{
				tReturn = "000000" + tMng + tStr.substring(0, 4) + tStr.substring(5, 7) + tStr.substring(8, 10);
			}
			else if (tMng.length() == 4) // 4位管理机构,前面补4个0
			{
				tReturn = "0000" + tMng + tStr.substring(0, 4) + tStr.substring(5, 7) + tStr.substring(8, 10);
			}
			else if (tMng.length() == 6) // 6位管理机构,前面补2个0
			{
				tReturn = "00" + tMng + tStr.substring(0, 4) + tStr.substring(5, 7) + tStr.substring(8, 10);
			}
			else
			// 8位管理机构,前面不补0
			{
				tReturn = tMng + tStr.substring(0, 4) + tStr.substring(5, 7) + tStr.substring(8, 10);
			}
			serialLen = 4; // 4位为大流水号
		}

		// //事件编号(新华)
		// else if (cNoType.equals("ACCNO"))
		// {
		// tReturn = "8" + tCom;
		// }
		//
		// //赔案编号(新华)
		// else if (cNoType.equals("RPTONLYNO"))
		// {
		// tReturn = "9" + tCom;
		// }

		// 报案编号
		else if (cNoType.equals("RPTNO"))
		{
			tReturn = "50" + tCom;
		}

		// 立案编号
		else if (cNoType.equals("RGTNO"))
		{
			tReturn = "51" + tCom;
		}

		// 赔案编号
		/*
		 * Modify by chenlao 20080102 赔案号生成规则为 "4" + 4位年度号 + 4位机构（到分公司） + 7 位流水 +
		 * "8" 按照安度生成流水
		 */
//		else if (cNoType.equals("CLMNO"))
//		{
////			tReturn = "6" + cNoLimit.substring(0, 4) + cNoLimit.substring(4, 8);
////			cNoLimit = cNoLimit.substring(0, 4);// 按照安度流水
////			serialLen = 7;
////			单证编码（4位）+ 年份（4位）+版本号（2位）+流水号（8位）
//			tReturn = "6066" + cNoLimit.substring(0, 4)+PrintManagerBL.CODE_VERSION;
//			cNoLimit = cNoLimit.substring(0, 4)+PrintManagerBL.CODE_VERSION;
//			serialLen = 8;
//		}
		/*
		 * Modify by chenlao 20080102 团体赔案号生成规则为 "T" + 4位年度号 + 11 位流水 按照安度生成流水
		 */
		else if (cNoType.equals("GRPRPTONLYNO"))
		{
			tReturn = "T" + cNoLimit.substring(0, 4);
			cNoLimit = cNoLimit.substring(0, 4);// 按照安度流水
			serialLen = 11;
		}
		// 拒案编号
		else if (cNoType.equals("DECLINENO"))
		{
			tReturn = "53" + tCom;
		}

		// 报案分案编号
		else if (cNoType.equals("SUBRPTNO"))
		{
			tReturn = "54" + tCom;
		}

		// 立案分案编号
		else if (cNoType.equals("CASENO"))
		{
			tReturn = "55" + tCom;
		}

		// 合同号
		else if (cNoType.equals("PROTOCOLNO"))
		{
			tReturn = "71" + tCom;
		}

		// 单证印刷号码
		else if (cNoType.equals("PRTNO"))
		{
			tReturn = "80" + tCom;
		}

		// 打印管理流水号
//		else if (cNoType.equals("PRTSEQNO"))
//		{
//			//	tNoLimit 6位管理机构+4为年度+4为单证类型
//			//tNoLimit=cNoLimit;
//			//tReturn = cNoLimit.substring(6,10)+cNoLimit.substring(0,4);
//			//serialLen = 6;
//			//单证编码（4位）+ 年份（4位）+版本号（2位）+流水号（8位）
//			tNoLimit=cNoLimit;
//			tReturn = cNoLimit.substring(6,10)+PrintManagerBL.CODE_VERSION;
//			serialLen = 8;
//		}
		//		 理赔单证打印管理流水号
//		else if (cNoType.equals("LCPRTSEQNO"))
//		{
//			//	tNoLimit 6位管理机构+4为年度+4为单证类型
//			//tNoLimit=cNoLimit;
//			//tReturn = cNoLimit.substring(6,10)+cNoLimit.substring(0,4);
//			//serialLen = 6;
//			//单证编码（4位）+ 年份（4位）+版本号（2位）+流水号（8位）
//			tNoLimit=cNoLimit;
//			tReturn = cNoLimit.substring(6,10)+PrintManagerBL.CODE_VERSION;
//			serialLen = 8;
//		}

		// 打印管理流水号
		else if (cNoType.equals("PRTSEQ2NO"))
		{
			tReturn = "82" + tCom;
		}
		// 保单打印批次号
		else if (cNoType.equals("CONTPRTSEQ"))
		{
			tCom = tCom.substring(1, 3);
			tReturn = "83" + tCom;
			serialLen = 7;

		}
		// 银保通对帐批次号
		else if (cNoType.equals("YBTNO"))
		{
			tReturn = "0";
			serialLen = 9;
		}

		// 回收清算单号
		else if (cNoType.equals("TAKEBACKNO"))
		{
			tReturn = "61" + tCom;
		}

		// 银行代扣代付批次号
		else if (cNoType.equals("BATCHNO"))
		{
			tReturn = "62" + tCom;
		}

		// 接口凭证id号
		else if (cNoType.equals("VOUCHERIDNO"))
		{
			tReturn = "63" + tCom;
		}

		// 佣金号码
		else if (cNoType.equals("WAGENO"))
		{
			tReturn = "90" + tCom;
		}

		// 卡单结算号
		else if (cNoType.equals("BALANCENO"))
		{
			String tStr = PubFun.getCurrentDate();
			tReturn = tReturn + tStr.substring(2, 4) + tStr.substring(5, 7) + tStr.substring(8, 10);
			serialLen = 4;
		}
		// 流水号
		else if (cNoType.equals("SERIALNO"))
		{
			tReturn = "98" + tCom;
		}
		// 团险呈报号码 S+二级机构代码+年（四位）+四位流水号
		else if (cNoType.equals("UPREPORT"))
		{
			String com = "";
			if (cNoLimit != null)
			{
				if (cNoLimit.length() >= 4)
				{
					com = cNoLimit.substring(0, 4);
				}
				if (cNoLimit.length() < 4)
				{
					com = cNoLimit + "00";
				}
			}
			String date = PubFun.getCurrentDate();
			if (!com.equals(""))
			{
				// +CreateMaxNo(cNoType,4)
				tReturn = "S" + com + date.substring(0, 4);
			}
			serialLen = 4;
		}
		else if(cNoType.equals("AGENTCODE"))
		{
			String com = "";
			if (cNoLimit != null)
			{
				if (cNoLimit.length() >= 4)
				{
					com = cNoLimit.substring(2, 4);
				}
				if (cNoLimit.length() < 4)
				{
					com ="00";
				}
			}
			if (!com.equals(""))
			{
				// +CreateMaxNo(cNoType,4)
				tReturn =com;
			}
			serialLen = 6;
		}
		//单证抵押金退费通知书号：4位机构代码+11流水号
		else if(cNoType.equals("REMORTGAGENO" ))
		{
			String com = "";
			if (cNoLimit != null)
			{
				if (cNoLimit.length() >= 4)
				{
					com = cNoLimit.substring(2, 6);
				}
			}
			if (!com.equals(""))
			{
				tReturn =com;
			}
			serialLen = 11;

		}
		else if(cNoType.equals("STUDENTCODE"))
		{
			String com = "";
			if (cNoLimit != null)
			{
				if (cNoLimit.length() >= 4)
				{
					com = cNoLimit.substring(2, 4);
				}
				if (cNoLimit.length() < 4)
				{
					com ="00";
				}
			}
			if (!com.equals(""))
			{
				// +CreateMaxNo(cNoType,4)
				tReturn ="S"+com;
			}
			serialLen = 6;
		}
		else if (cNoType.equals("ERP_PAYMENT_ID")) //cbs付款接口表 流水号
		{
			  // modify by zy 2010-02-20
			  serialLen = 6;
			  tReturn = cNoLimit;
		}
        /*ABLREQUEST-1930-准备金导入 liaozc 2013-08-07 add begin*/
		/*准备金批号*/
		else if (cNoType.equals("RESERVEBATCHNO")) 
		{
			// add by liaozc 2013-08-01
			  serialLen = 6;
			  tReturn = cNoLimit;
		}
		/*准备金流水号*/
		else if (cNoType.equals("RESERVENO")) 
		{
			  // add by liaozc 2013-08-01
			  serialLen = 10;
			  tReturn = cNoLimit;
		}
        /*ABLREQUEST-1930-准备金导入 liaozc 2013-08-07 add end*/
		//前四位：银行代码前四位;第五，六位标识为代收标准位 01;14位流水号
		else if (cNoType.equals("GET"))
		{
			tNoLimit=cNoLimit;
            tReturn = cNoLimit.substring(0,3)+"S";
            //ABLREQUEST-2679 寿险批量代扣优化需求 maiwei
            serialLen = 11;
		}
		//前四位：银行代码前四位;第五，六位标识为代付标准位 02;14位流水号
		else if (cNoType.equals("PAY"))
		{
			tNoLimit=cNoLimit;
            tReturn = cNoLimit.substring(0,3)+"F";
            serialLen = 16;
		}
		//		add by yyh 2010-8-6 外包录入号码
		else if (cNoType.equals("BPOCUSTOMERNO"))
		{
			serialLen = 10;
		}
		else if (cNoType.equals("BPOPROPOSALNO"))
		{
			tReturn = "BPO11" + tCom;
		}
		else if (cNoType.equals("BPOSPECNo"))
		{
			tReturn = "BPO" + tCom;
			serialLen = 10;
		}
		else if (cNoType.equals("LOCKCONFIGNO"))//add by zhaojw 20151204 增加并发控制组配置流水号10位
		{
			tReturn = cNoLimit;
		}
		else if (cNoType.equals("LOCKLOGNO"))//add by zhaojw 20151204 增加并发解锁备份日志流水号 年份+16位流水号
		{
			serialLen = 16;
			tReturn = cNoLimit;
		}
		else if (cNoType.equals("THREADLOGNO"))//add by zhaojw 20151209  增加多线程日志流水号 年份+16位流水号
		{
			serialLen = 16;
			tReturn = cNoLimit;
		}		
		// else
		// {
		// tReturn += "99";
		// }

		// 其他
		Connection conn = DBConnPool.getConnection();

		if (conn == null)
		{
			log4j.info("CreateMaxNo : fail to get db connection");

			return tReturn;
		}

		//int tMaxNo = 0;
		BigInteger tMaxNo = new BigInteger("0");
		cNoLimit = tReturn;
		try
		{
            //开始事务
            conn.setAutoCommit(false);
            String tSQL = " select createmaxno('" + cNoType + "','" + cNoLimit +
                          "') from dual ";
            ExeSQL exeSQL = new ExeSQL(conn);
            String result = null;
            result = exeSQL.getOneValue(tSQL);
            tMaxNo = new BigInteger(result);
            conn.commit();
            conn.close();
//---------------------modify at 2011-2-22 最大号生成优化，如下程序被废弃--start-------------			
//			// 开始事务
//			// 查询结果有3个： -- added by Fanym
//			// 全部采用直接执行SQL语句，只要有其他事务锁定了本行，立即返回NULL
//			// 如果没有锁定，则本事务锁定，查询得到结果则UPDATE，没有则INSERT
//			conn.setAutoCommit(false);
//
//			// String strSQL = "select MaxNo from LDMaxNo where notype='" +
//			// cNoType + "' and nolimit='" + cNoLimit +
//			// "' for update";
//			// //如果数据库类型是ORACLE的话，需要添加nowait属性，以防止锁等待
//			// if (SysConst.DBTYPE.compareTo("ORACLE") == 0)
//			// {
//			// strSQL = strSQL + " nowait";
//			// }
//			StringBuffer tSBql = new StringBuffer(256);
//			tSBql.append("select MaxNo from LDMaxNo where notype='");
//			tSBql.append(cNoType);
//			tSBql.append("' and nolimit='");
//			tSBql.append(cNoLimit);
//			tSBql.append("' for update");
//			// 如果数据库类型是ORACLE的话，需要添加nowait属性，以防止锁等待
//			if (SysConst.DBTYPE.compareTo("ORACLE") == 0)
//			{
//				// 去掉nowait的限制
//				// tSBql.append(" nowait");
//			}
//
//			ExeSQL exeSQL = new ExeSQL(conn);
//			String result = null;
//			result = exeSQL.getOneValue(tSBql.toString());
//
//			// 测试返回bull时
//			if (exeSQL.mErrors.needDealError())
//			{
//				log4j.info("查询LDMaxNo出错，请稍后!");
//				conn.rollback();
//				conn.close();
//
//				return null;
//			}
//
//			if ((result == null) || result.equals(""))
//			{
//				// strSQL = "insert into ldmaxno(notype, nolimit, maxno)
//				// values('" +
//				// cNoType + "', '" + cNoLimit + "', 1)";
//				tSBql = new StringBuffer(256);
//				tSBql.append("insert into ldmaxno(notype, nolimit, maxno) values('");
//				tSBql.append(cNoType);
//				tSBql.append("', '");
//				tSBql.append(cNoLimit);
//				tSBql.append("', 1)");
//
//				exeSQL = new ExeSQL(conn);
//				if (!exeSQL.execUpdateSQL(tSBql.toString()))
//				{
//					log4j.info("CreateMaxNo 插入失败，请重试!");
//					conn.rollback();
//					conn.close();
//
//					return null;
//				}
//				else
//				{
//					tMaxNo = 1;
//				}
//			}
//			else
//			{
//				// strSQL = "update ldmaxno set maxno = maxno + 1 where notype =
//				// '" + cNoType
//				// + "' and nolimit = '" + cNoLimit + "'";
//				tSBql = new StringBuffer(256);
//				tSBql.append("update ldmaxno set maxno = maxno + 1 where notype = '");
//				tSBql.append(cNoType);
//				tSBql.append("' and nolimit = '");
//				tSBql.append(cNoLimit);
//				tSBql.append("'");
//
//				exeSQL = new ExeSQL(conn);
//				if (!exeSQL.execUpdateSQL(tSBql.toString()))
//				{
//					log4j.info("CreateMaxNo 更新失败，请重试!");
//					conn.rollback();
//					conn.close();
//
//					return null;
//				}
//				else
//				{
//					tMaxNo = Integer.parseInt(result) + 1;
//				}
//			}
//
//			conn.commit();
//			conn.close();
//			---------------------modify at 2011-2-22 最大号生成优化，如上程序被废弃--start-------------	
		}
		catch (Exception Ex)
		{
			try
			{
				conn.rollback();
				conn.close();

				return null;
			}
			catch (Exception e1)
			{
				e1.printStackTrace();

				return null;
			}
		}

		/*
		 * if (tReturn.length() >= 12) { tReturn = tReturn.substring(0, 10) +
		 * "0" + tReturn.substring(10, 12); }
		 */
		String tStr = String.valueOf(tMaxNo);
		tStr = PubFun.LCh(tStr, "0", serialLen);
		// log4j.info("tStr:"+tStr+" serialLen:"+serialLen);
		// log4j.info("---------tStr:"+tStr);
		if (cNoType.equals("CONTNO"))
		{
			//个险保单号格式（共18位）= 1位标识+4位年度+4位机构代码+7位流水号+1位校验码+末尾标识“8”，如：1  2007 8621 0000001* 8
        	// 校验码规则：{3，5，7，11，13，17，19，23，29，31，37，41，43，47，53，59}乘以相应的位，
        	// 个险 相加的结果除以61的余数取个位数。 相加的结果除以61的余数取个位数
        	// 1为标志 1个险 2团险 3保全 4理赔 8银保
        	// cNoLimit 86210020081
			//int Sum=0;
			//tReturn =tNoLimit.substring(10,11)+tNoLimit.substring(6,10)+ tNoLimit.substring(0,4)+tStr;
			//int arr[]={3,5,7,11,13,17,19,23,29,31,37,41,43,47,53,59};
			//for(int i=0;i<tReturn.length();i++)
			//{
			//	Sum=Sum+arr[i]*Integer.parseInt(tReturn.substring(i,i+1));
			//}
			//int ModValue=Sum%61;
			//String tMod=String.valueOf(ModValue);
			//log4j.info("Mod="+tMod);
			//tReturn=tReturn+tMod.substring(tMod.length()-1,tMod.length())+"8";

			//个险保单号格式（共20位）= 单证编码（4位）+机构编码（4位, 为二、三类机构编码）+年份（2位）+流水号（9位）+ 末尾标识“8”
			//cNoLimit 86210120101088
			tReturn =tNoLimit.substring(10,14)+tNoLimit.substring(2,6)+ tNoLimit.substring(8,10)+tStr+"8";
		}
		if(cNoType.equals("PRTSEQNO"))
		{
			//tNoLimit 6位管理机构+4为年度+4为单证类型
			//单证编码（4位）+ 年份（4位）+版本号（2位）+流水号（8位）
			if(tNoLimit.length() >= 14)
			{
				tReturn=tNoLimit.substring(10,14)+tReturn+tStr;
			}
			else
			{
				tReturn=tReturn+tStr;
			}
		}
		if (tReturn.equals("SN"))
		{
			tReturn = tStr.trim() + "0";
		}
		else if (!cNoType.equals("CONTNO")&&!cNoType.equals("PRTSEQNO"))
		{
			tReturn = tReturn.trim() + tStr.trim();
		}

		// 保全受理号、批单号码、批单申请号码统一在最后一位置"8"
		//if (cNoType.equals("EDORACCEPTNO") || cNoType.equals("EDORAPPNO") || cNoType.equals("EDORNO")
//		if (cNoType.equals("EDORACCEPTNO") || cNoType.equals("EDORNO")
//				|| cNoType.equals("EDORGRPAPPNO") || cNoType.equals("EDORGRPNO") || cNoType.equals("CLMNO"))
		if (cNoType.equals("EDORACCEPTNO") || cNoType.equals("EDORNO")
				|| cNoType.equals("EDORGRPAPPNO") || cNoType.equals("EDORGRPNO"))
		{
			tReturn = tReturn.trim() + "8";
		}

		// log4j.info("------tReturn:"+tReturn);

		return tReturn;
	}

	/**
	 * 功能：产生指定长度的流水号，一个号码类型一个流水
	 * @param cNoType String 流水号的类型
	 * @param cNoLength int 流水号的长度
	 * @return String 返回产生的流水号码
	 */
	public String CreateMaxNo(String cNoType, int cNoLength)
	{
		if ((cNoType == null) || (cNoType.trim().length() <= 0) || (cNoLength <= 0))
		{
			log4j.info("NoType长度错误或NoLength错误");

			return null;
		}

		cNoType = cNoType.toUpperCase();

		// log4j.info("type:"+cNoType+" length:"+cNoLength);
		Connection conn = DBConnPool.getConnection();

		if (conn == null)
		{
			log4j.info("CreateMaxNo : fail to get db connection");

			return null;
		}

		String tReturn = "";
		String cNoLimit = "SN";
		// 对上面那种cNoLimit为SN的数据做一个校验，否则会导致数据干扰
		if (cNoType.equals("COMMISIONSN") || cNoType.equals("GRPNO") || cNoType.equals("CUSTOMERNO")
				|| cNoType.equals("SUGDATAITEMCODE") || cNoType.equals("SUGITEMCODE") || cNoType.equals("SUGMODELCODE")
				|| cNoType.equals("SUGCODE"))
		{

			log4j.info("该类型流水号，请采用CreateMaxNo('" + cNoType + "','SN')方式生成");
			return null;
		}
		BigInteger tMaxNo = new BigInteger("0");
		tReturn = cNoLimit;

		try
		{
			// 开始事务
			conn.setAutoCommit(false);
	         String tSQL = " select createmaxno('" + cNoType + "','" + cNoLimit +
             "') from dual ";
			ExeSQL exeSQL = new ExeSQL(conn);
			String result = null;
			result = exeSQL.getOneValue(tSQL);
			tMaxNo = new BigInteger(result);
			conn.commit();
			conn.close();
//----------------------2011-2-22 废弃 start---------------------------
//			// String strSQL = "select MaxNo from LDMaxNo where notype='" +
//			// cNoType + "' and nolimit='" + cNoLimit +
//			// "' for update";
//			// //如果数据库类型是ORACLE的话，需要添加nowait属性，以防止锁等待
//			// if (SysConst.DBTYPE.compareTo("ORACLE") == 0)
//			// {
//			// strSQL = strSQL + " nowait";
//			// }
//			StringBuffer tSBql = new StringBuffer(256);
//			tSBql.append("select MaxNo from LDMaxNo where notype='");
//			tSBql.append(cNoType);
//			tSBql.append("' and nolimit='");
//			tSBql.append(cNoLimit);
//			tSBql.append("' for update");
//			// 如果数据库类型是ORACLE的话，需要添加nowait属性，以防止锁等待
//			if (SysConst.DBTYPE.compareTo("ORACLE") == 0)
//			{
//				// tSBql.append(" nowait");
//			}
//
//			ExeSQL exeSQL = new ExeSQL(conn);
//			String result = null;
//			result = exeSQL.getOneValue(tSBql.toString());
//
//			if ((result == null) || exeSQL.mErrors.needDealError())
//			{
//				log4j.info("CreateMaxNo 资源忙，请稍后!");
//				conn.rollback();
//				conn.close();
//
//				return null;
//			}
//
//			if (result.equals(""))
//			{
//				// strSQL = "insert into ldmaxno(notype, nolimit, maxno)
//				// values('" +
//				// cNoType + "', '" + cNoLimit + "', 1)";
//				tSBql = new StringBuffer(256);
//				tSBql.append("insert into ldmaxno(notype, nolimit, maxno) values('");
//				tSBql.append(cNoType);
//				tSBql.append("', '");
//				tSBql.append(cNoLimit);
//				tSBql.append("', 1)");
//
//				exeSQL = new ExeSQL(conn);
//				if (!exeSQL.execUpdateSQL(tSBql.toString()))
//				{
//					log4j.info("CreateMaxNo 插入失败，请重试!");
//					conn.rollback();
//					conn.close();
//
//					return null;
//				}
//				else
//				{
//					tMaxNo = new BigInteger("1");
//				}
//			}
//			else
//			{
//				// strSQL = "update ldmaxno set maxno = maxno + 1 where notype =
//				// '" + cNoType
//				// + "' and nolimit = '" + cNoLimit + "'";
//				tSBql = new StringBuffer(256);
//				tSBql.append("update ldmaxno set maxno = maxno + 1 where notype = '");
//				tSBql.append(cNoType);
//				tSBql.append("' and nolimit = '");
//				tSBql.append(cNoLimit);
//				tSBql.append("'");
//
//				exeSQL = new ExeSQL(conn);
//				if (!exeSQL.execUpdateSQL(tSBql.toString()))
//				{
//					log4j.info("CreateMaxNo 更新失败，请重试!");
//					conn.rollback();
//					conn.close();
//
//					return null;
//				}
//				else
//				{
//					tMaxNo = (new BigInteger(result)).add(new BigInteger("1"));
//				}
//			}
//
//			conn.commit();
//			conn.close();
//			----------------------2011-2-22 废弃 end---------------------------
		}
		catch (Exception Ex)
		{
			try
			{
				conn.rollback();
				conn.close();

				return null;
			}
			catch (Exception e1)
			{
				e1.printStackTrace();

				return null;
			}
		}

		String tStr = tMaxNo.toString();
		tStr = PubFun.LCh(tStr, "0", cNoLength);
		tReturn = tStr.trim();

		return tReturn;
	}

	public static void main(String[] args)
	{
		// SysMaxNoNCL sysMaxNoZhongYing1 = new SysMaxNoNCL();
		// log4j.info(sysMaxNoZhongYing1.CreateMaxNo("EdorNo", "SN"));

		// String strLimit = PubFun.getNoLimit("86");
		//
		//
		// String newContNo = PubFun1.CreateMaxNo("CONTNO", strLimit);//
		// String tReturn = PubFun1.creatVerifyDigit(newContNo);
		// log4j.info("EDORACCEPTNO : " + newContNo);
		// log4j.info("EDORACCEPTNO : " + tReturn);
		// log4j.info("EDORACCEPTNO : " +
		// newContNo.substring(6,newContNo.length())+newContNo.substring(5,6));

//		String tSCANNO = PubFun1.CreateMaxNo("UPREPORT", "86");
		log4j.info("PRTNO:" + PubFun1.CreateMaxNo("PRTSEQNO", "86310020071026"));
//		log4j.info("SCANNO:" + PubFun1.CreateMaxNo("UPREPORT", "86"));
//		log4j.info("SCANNO:" + PubFun1.CreateMaxNo("UPREPORT", "8621"));
//		log4j.info("SCANNO:" + PubFun1.CreateMaxNo("UPREPORT", "86"));
//		log4j.info("SCANNO:" + PubFun1.CreateMaxNo("UPREPORT", "8621"));
//		log4j.info("SCANNO:" + PubFun1.CreateMaxNo("UPREPORT", "86"));
//		log4j.info("SCANNO:" + PubFun1.CreateMaxNo("UPREPORT", "8621"));
//		log4j.info("SCANNO:" + PubFun1.CreateMaxNo("UPREPORT", "86"));
//		log4j.info("SCANNO:" + PubFun1.CreateMaxNo("UPREPORT", "8631"));
//		log4j.info("SCANNO:" + PubFun1.CreateMaxNo("UPREPORT", "8631"));
//		log4j.info("SCANNO:" + PubFun1.CreateMaxNo("UPREPORT", "8631"));
//		log4j.info("SCANNO:" + PubFun1.CreateMaxNo("UPREPORT", "86"));
//		log4j.info("SCANNO:" + PubFun1.CreateMaxNo("UPREPORT", "86"));
//		log4j.info("SCANNO:" + PubFun1.CreateMaxNo("UPREPORT", "86"));
//		log4j.info("SCANNO:" + PubFun1.CreateMaxNo("UPREPORT", "86"));
//		log4j.info("SCANNO:" + PubFun1.CreateMaxNo("UPREPORT", "86"));
//		log4j.info("SCANNO:" + PubFun1.CreateMaxNo("UPREPORT", "86"));
	}
}
