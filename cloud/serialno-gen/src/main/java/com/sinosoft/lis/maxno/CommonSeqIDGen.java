package com.sinosoft.lis.maxno;

import com.sinosoft.sn.AbstractSNGenerator;
import com.sinosoft.sn.generator.GeneratorTypeSet;
import com.sinosoft.sn.generator.ParamConsts;

import java.util.HashMap;
import java.util.Map;

/**
 * 主键自增ID通用生成类
 * Created by filon51 on 2016/7/27.
 */
public class CommonSeqIDGen extends AbstractSNGenerator {
    private String noType = "";
    private String noLimit = ""; //默认是流水号

    @Override
    protected Map prepareMap() {
        Map parameterMap = new HashMap(); //设定参数
        parameterMap.put(ParamConsts.SERIAL_NO_TYPE, this.getNoType());
        parameterMap.put(ParamConsts.SERIAL_NO_LIMIT, this.getNoLimit());
        return parameterMap;
    }

    @Override
    protected String prepareFormatStr() {
        String snFormatStr
                = GeneratorTypeSet.ID_KEY + ParamConsts.SPLIT_SUB_GENERATOR_INNER + "ID";
        return snFormatStr;
    }

    public String getNoType() {
        return noType;
    }

    public void setNoType(String noType) {
        this.noType = noType;
    }

    public String getNoLimit() {
        return noLimit;
    }

    public void setNoLimit(String noLimit) {
        this.noLimit = noLimit;
    }
}
