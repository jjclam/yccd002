package com.sinosoft.lis.maxno;

import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.sn.AbstractSNGenerator;
import com.sinosoft.sn.generator.GeneratorTypeSet;
import com.sinosoft.sn.generator.ParamConsts;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

/**
 * 保全子系统 生成云核心 保全受理号 实现类
 * Created by kaine on 2016/11/11.
 */
public class EdorSNGenerator extends AbstractSNGenerator {
    private static final Log logger = LogFactory.getLog(EdorSNGenerator.class.getName());


    // 默认机构编码
    private String manageCom = "860100";
    // 默认年份
    private String year = Integer.toString(Calendar.getInstance().get(Calendar.YEAR));

    /**
     * 准备流水号生成需要的参数MAP
     *
     * @return
     */
    @Override
    protected Map prepareMap() {
        Map parameterMap = new HashMap();
        parameterMap.put(ParamConsts.SERIAL_NO_TYPE, "EdorID");
        parameterMap.put(ParamConsts.SERIAL_NO_LIMIT, "SN");

        return parameterMap;
    }

    /**
     * 准备流水号生成规则
     *
     * @return
     */
    @Override
    protected String prepareFormatStr() {
        String snFormatStr
                // 6位机构代码
                = GeneratorTypeSet.FIXED_STRING + ParamConsts.SPLIT_SUB_GENERATOR_INNER + manageCom
                + ParamConsts.SPLIT_SUB_GENERATOR
                // 4位年份
                + GeneratorTypeSet.DATE_TIME + ParamConsts.SPLIT_SUB_GENERATOR_INNER + "yyyy"
                + ParamConsts.SPLIT_SUB_GENERATOR
                // 9位流水号
                + GeneratorTypeSet.NUMBER_SEQUENCE + ParamConsts.SPLIT_SUB_GENERATOR_INNER + "9" + ParamConsts.SPLIT_C + "0"
                + ParamConsts.SPLIT_SUB_GENERATOR
                // 1位固定结尾：6
                + GeneratorTypeSet.FIXED_STRING + ParamConsts.SPLIT_SUB_GENERATOR_INNER + "6";
        if (logger.isDebugEnabled()) {
            logger.debug(snFormatStr);
        }
        return snFormatStr;
    }

    // 设置管理机构
    public void setManageCom(String manageCom) {
        this.manageCom = PubFun.RCh(manageCom, "0", 6).substring(0, 6);
        logger.debug(this.manageCom);
    }
}
