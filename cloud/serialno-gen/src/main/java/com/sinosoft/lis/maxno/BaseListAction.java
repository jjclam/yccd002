package com.sinosoft.lis.maxno;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Peidong on 2016/8/10.
 */
public class BaseListAction {
    /**
     * 生成业务主键（在插入数据时需要，由时间戳生成）
     *
     * @return String
     */
    private static String timeStamp = "";

    /**
     *
     * @return 返回业务ID
     */
    public String createBusinessId() { //12bt
        String temp = BaseListAction.serverCurrentTimeStamp();
        long random = Math.round(Math.random() * 110104 + 100000);
        if (!timeStamp.equals(temp)) {
            timeStamp = temp;
            return timeStamp
                    + NumberUtils.format(random, NumberUtils.DEFAULT_SEQNUMBER);
        } else {
            return temp
                    + NumberUtils.format(random, NumberUtils.DEFAULT_SEQNUMBER);
        }
    }

    /**
     *
     * @param tabFlag tabFlag
     * @return 返回业务ID
     */
    public String createBusinessId(String tabFlag) { //26bt
        if (tabFlag == null) {
            tabFlag = "";
        }
        String temp = BaseListAction.serverCurrentTimeStamp2();
        long random = Math.round(Math.random() * 110104 + 100000);
        if (!timeStamp.equals(temp)) {
            timeStamp = temp;
            return tabFlag + timeStamp
                    + NumberUtils.format(random, NumberUtils.DEFAULT_SEQNUMBER);
        } else {
            return tabFlag + temp
                    + NumberUtils.format(random, NumberUtils.DEFAULT_SEQNUMBER);
        }
    }

    /**
     * 获取系统时间
     * @return 系统时间
     */
    public static String serverCurrentTimeStamp() {
        Date date = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("HHmmss");
        String str = formatter.format(date);
        return str;
    }

    /**
     *
     * @return 日期和时间
     */
    public static String serverCurrentTimeStamp2() {
        Date date = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMddHHmmssSSSS");
        String str2 = formatter.format(date);
        return str2;
    }




}
