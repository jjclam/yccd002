/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */
package com.sinosoft.lis.pubfun;
import com.sinosoft.lis.maxno.CommonSeqIDGen;
import com.sinosoft.lis.maxno.CommonSeqStrGen;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * <p>
 * Title: Web业务系统
 * </p>
 * <p>
 * Description:
 * </p>
 * <p>
 * Copyright: Copyright (c) 2002
 * </p>
 * <p>
 * Company: Sinosoft
 * </p>
 *
 * @author YT
 * @version 1.0
 */
public class PubFun1 {
    private static Log logger = LogFactory.getLog(PubFun1.class);

    public static final String PhysicNo ="PhysicNo";
    public PubFun1() {
    }

    private VData rResult = new VData();

    /**
     * <p>
     * 生成流水号的函数
     * <p>
     * <p>
     * 号码规则：机构编码 日期年 校验位 类型 流水号
     * <p>
     * <p>
     * 1-6 7-10 11 12-13 14-20
     * <p>
     *
     * @param cNoType  为需要生成号码的类型
     * @param cNoLimit 为需要生成号码的限制条件
     * @return 生成的符合条件的流水号，如果生成失败，返回空字符串""
     */
    public static String CreateMaxNo(String cNoType, String cNoLimit) {
        try {
                // 动态载入类
                logger.debug("sysmaxnotype:"
                        + com.sinosoft.utility.SysConst.MAXNOTYPE);
                String className = "com.sinosoft.lis.pubfun.SysMaxNo"
                        + com.sinosoft.utility.SysConst.MAXNOTYPE;
                Class cc = Class.forName(className);
                com.sinosoft.lis.pubfun.SysMaxNo tSysMaxNo = (com.sinosoft.lis.pubfun.SysMaxNo) (cc
                        .newInstance());
                return tSysMaxNo.CreateMaxNo(cNoType, cNoLimit);

        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }

       /* CommonSeqIDGen tSNGen = new CommonSeqIDGen();
        tSNGen.setNoType(cNoType);
        tSNGen.setNoLimit(cNoLimit);
        return tSNGen.generate();*/
    }
    /**
     * 获取校验位 98-(sequenceNo*1000+cardCode)*100%97
     * @param sequenceNo//序列号
     * @param cardCode//单证类经
     * @return
     */
    public static int getCheckTwo(long sequenceNo, int cardCode)

    {

        long l1 = sequenceNo;

        l1 = l1 * 1000L + (long) cardCode;

        l1 *= 100L;

        l1 %= 97L;

        l1 = 98L - l1;

        return (int) l1;

    }

    //一位校验位
    public static char getCheckOne(String id)

    {

        char pszSrc[]=id.toCharArray();;
        int iS = 0;
        int iW[]={7, 9, 10, 5, 8, 4, 2, 1, 6, 3, 7, 9, 10, 5, 8, 4, 2};
        char szVerCode[] = new char[]{'1','0', '0', '9', '8', '7', '6', '5', '4', '3', '2'};
        //int i;
        for(int j=0;j<17;j++)
        {
            iS += (int)(pszSrc[j]-'0') * iW[j];
        }
        int iY = iS%11;
        return szVerCode[iY];

    }
//    /**
//     * <p>
//     * MOD-10保单号校验规则的函数
//     * <p>
//     * <p>
//     * 校验规则：Number: 0 0 0 0 0 0 1 2 3 4 7
//     * <p>
//     * <p>
//     * (x2 alternate digits): 0 0 0 2 6 14
//     * <p>
//     * <p>
//     * (Add when double digits) 0 0 2 6 5
//     * <p>
//     * <p>
//     * Add all digits 0 0 0 0 0 0 2 2 6 4 5
//     * <p>
//     * <p>
//     * Sum of all digits = 2+2+6+4+5 = 19
//     * <p>
//     * <p>
//     * Last digit = 9
//     * <p>
//     * <p>
//     * Check digit = 9-9 = 0
//     * <p>
//     * <p>
//     * Number is 000000123470
//     * <p>
//     *
//     * @param cNoType  为需要生成号码的类型
//     * @param cNoLimit 为需要生成号码的限制条件
//     * @return 生成的符合条件的流水号，如果生成失败，返回空字符串""
//     * @author quyang 2005.06.29
//     * @version 1.0
//     */
//    public static String creatVerifyDigit(String originalDigit) {
//        try {
//            String finalDigit = null;
//            String[] alterDigOdd = null;
//            String[] alterDigEven = null;
//            logger.debug("originalDigit" + originalDigit
//                    + "originalDigit.length()" + originalDigit.length());
//            if (!"".equalsIgnoreCase(originalDigit)) {
//                alterDigOdd = new String[originalDigit.length()];
//                alterDigEven = new String[originalDigit.length()];
//                for (int i = 0; i < originalDigit.length(); i++) {
//                    if (i == 0) {
//                        alterDigOdd[i] = originalDigit.substring(i, i + 1);
//                        logger.debug("alterDigOdd[" + i + "]"
//                                + alterDigOdd[i].toString());
//                    }
//                    if (i == 1) {
//                        alterDigEven[i] = originalDigit.substring(i, i + 1);
//                        logger.debug("alterDigEven[" + i + "]"
//                                + alterDigEven[i].toString());
//                    }
//                    if (i > 1 && i % 2 == 1) {
//                        alterDigEven[i] = originalDigit.substring(i, i + 1);
//                        logger.debug("alterDigEven[" + i + "]"
//                                + alterDigEven[i].toString());
//                    }
//                    if (i > 1 && i % 2 == 0) {
//                        alterDigOdd[i] = originalDigit.substring(i, i + 1);
//                        logger.debug("alterDigOdd[" + i + "]"
//                                + alterDigOdd[i].toString());
//                    }
//                }
//            }
//            for (int i = alterDigOdd.length - 1; i >= 0; i--) {
//                logger.debug("alterDigOdd数组的元素[" + i + "]"
//                        + alterDigOdd[i]);
//            }
//            for (int i = alterDigEven.length - 1; i >= 0; i--) {
//                logger.debug("alterDigEven数组的元素[" + i + "]"
//                        + alterDigEven[i]);
//            }
//            logger.debug("alterDigOdd" + alterDigOdd.toString()
//                    + "alterDigOdd.length()" + alterDigOdd.length);
//            logger.debug("alterDigEven" + alterDigEven.toString()
//                    + "alterDigEven.length()" + alterDigEven.length);
//            // x2 alternate digits
//            if (!"".equals(alterDigOdd)) {
//
//                for (int i = 0; i < alterDigOdd.length; i++) {
//                    if ((String) alterDigOdd[i] != null
//                            && (String) alterDigOdd[i] != "null"
//                            && (String) alterDigOdd[i] != "0") {
//                        logger.debug("(String)alterDigOdd[i]"
//                                + (String) alterDigOdd[i]);
//                        String tempString = null;
//                        logger.debug("alterDigOdd before double[" + i
//                                + "]:" + alterDigOdd[i]);
//                        tempString = String.valueOf(2 * Integer
//                                .parseInt(alterDigOdd[i].toString()));
//                        logger.debug("alterDigOdd after double[" + i
//                                + "]:" + alterDigOdd[i]);
//                        alterDigOdd[i] = "";
//                        alterDigOdd[i] = tempString;
//                        // Add when double digits
//                        if ((!"".equals(alterDigOdd[i]))
//                                && alterDigOdd[i].toString().length() > 1) {
//                            int tempInt = 0;
//                            for (int j = 0; j < alterDigOdd[i].length(); j++) {
//                                String temp = (String) alterDigOdd[i];
//                                tempInt += Integer.parseInt(String.valueOf(temp
//                                        .charAt(j)));
//                            }
//                            alterDigOdd[i] = "";
//                            alterDigOdd[i] = String.valueOf(tempInt);
//                        }
//                        logger.debug("alterDigOdd after Add double"
//                                + alterDigOdd[i].toString());
//                    }
//                }
//                for (int i = alterDigOdd.length - 1; i >= 0; i--) {
//                    logger.debug("alterDigOdd数组的元素 after Add double ["
//                            + i + "]" + alterDigOdd[i]);
//                }
//                // Add all digits
//                // Sum of all digits
//                int allDig = 0;
//                for (int i = 0; i < alterDigOdd.length; i++) {
//                    if (i == 0) {
//                        allDig += Integer.parseInt(alterDigOdd[i]);
//                        logger.debug("alterDigOdd[" + i + "]"
//                                + alterDigOdd[i].toString());
//                    }
//                    if (i == 1) {
//                        allDig += Integer.parseInt(alterDigEven[i]);
//                        logger.debug("alterDigEven[" + i + "]"
//                                + alterDigEven[i].toString());
//                    }
//                    if (i > 1 && i % 2 == 1) {
//                        allDig += Integer.parseInt(alterDigEven[i]);
//                        logger.debug("alterDigEven[" + i + "]"
//                                + alterDigEven[i].toString());
//                    }
//                    if (i > 1 && i % 2 == 0) {
//                        allDig += Integer.parseInt(alterDigOdd[i]);
//                        logger.debug("alterDigOdd[" + i + "]"
//                                + alterDigOdd[i].toString());
//                    }
//
//                }
//                logger.debug("allDig" + String.valueOf(allDig));
//                // Last digit
//                int lastDig = 0;
//                String allDigString = String.valueOf(allDig);
//                if (allDig != 0) {
//                    lastDig = Integer.parseInt(allDigString.substring(
//                            allDigString.length() - 1, allDigString.length()));
//                }
//                logger.debug("lastDig" + String.valueOf(lastDig));
//                // Check digit
//                int checkDig = 0;
//                checkDig = 9 - lastDig;
//
//                if (checkDig == 4) {
//                    checkDig = 8;
//                }
//                finalDigit = originalDigit + String.valueOf(checkDig);
//                logger.debug("finalDigit" + finalDigit);
//            }
//            return finalDigit;
//        } catch (Exception ex) {
//            ex.printStackTrace();
//            return null;
//        }
//    }
//
//    /**
//     * 生成流水号的函数 号码规则：机构编码 日期年 校验位 类型 流水号 1-6 7-10 11 12-13 14-20
//     *
//     * @param cNoType  String 为需要生成号码的类型
//     * @param cNoLimit String 为需要生成号码的限制条件
//     * @param tVData   VData
//     * @return String 生成的符合条件的流水号，如果生成失败，返回空字符串""
//     */
//    public static String CreateMaxNo(String cNoType, String cNoLimit,
//                                     VData tVData) {
//        try {
//
//                // 没有查到结果,使用原有方式生成
//                logger.debug("sysmaxnotype:"
//                        + com.sinosoft.utility.SysConst.MAXNOTYPE);
//                String className = "com.sinosoft.lis.pubfun.SysMaxNo"
//                        + com.sinosoft.utility.SysConst.MAXNOTYPE;
//                Class cc = Class.forName(className);
//                com.sinosoft.lis.pubfun.SysMaxNo tSysMaxNo = (com.sinosoft.lis.pubfun.SysMaxNo) (cc
//                        .newInstance());
//                return tSysMaxNo.CreateMaxNo(cNoType, cNoLimit, tVData);
//        } catch (Exception ex) {
//            ex.printStackTrace();
//            return null;
//        }
//    }
//
    /**
     * 功能：产生指定长度的流水号，一个号码类型一个流水
     *
     * @param cNoType   String 流水号的类型
     * @param cNoLength int 流水号的长度
     * @return String 返回产生的流水号码
     */
    public static String CreateMaxNo(String cNoType, int cNoLength) {
//        try {
//                String className = "com.sinosoft.lis.pubfun.SysMaxNo"
//                        + com.sinosoft.utility.SysConst.MAXNOTYPE;
//                Class cc = Class.forName(className);
//                com.sinosoft.lis.pubfun.SysMaxNo tSysMaxNo = (com.sinosoft.lis.pubfun.SysMaxNo) (cc
//                        .newInstance());
//                return tSysMaxNo.CreateMaxNo(cNoType, cNoLength);
//        } catch (Exception ex) {
//            ex.printStackTrace();
//            return null;
//        }

        CommonSeqStrGen tSNGen = new CommonSeqStrGen();
        tSNGen.setNoType(cNoType);
        tSNGen.setNoLength(cNoLength);
        return tSNGen.generate();
    }
//
//    /**
//     * tongmeng 2011-05-17 add
//     * 规则引擎生成编码的程序
//     * 前三位为分类码,用于区分规则的所属业务功能.
//     * 1,2位为算法识别位,统一由RU开头.
//     * 第3,4位为功能编码
//     *
//     * @param tModulCode
//     * @return
//     */
//    public static String CreateRuleCalCode(String tModulCode) {
//        String tNo = "";
//        tNo = tNo + "RU" + tModulCode.toUpperCase();
//        String tID = CreateMaxNo(tNo, 6);
//        tNo = tNo + tID;
//        return tNo;
//    }
//
//    /**
//     * tongmeng 2011-07-13 add
//     * 算法引擎生成编码的程序
//     * 前三位为分类码,用于区分规则的所属业务功能.
//     * 1,2位为算法识别位,统一由CL开头.
//     * 第3,4位为功能编码
//     *
//     * @param tModulCode
//     * @return
//     */
//    public static String CreateRuleCalCode(String tModulCode, String tFlag) {
//        String tNo = "";
//        if (tFlag.equals("Y")) {
//            tNo = tNo + "RU" + tModulCode.toUpperCase();
//        } else {
//            tNo = tNo + "CL" + tModulCode.toUpperCase();
//        }
//        String tID = CreateMaxNo(tNo, 6);
//        tNo = tNo + tID;
//        return tNo;
//    }


    /**
     * 调试函数
     *
     * @param args String[]
     */
    public static void main(String[] args) {
        PubFun1 tPF1 = new PubFun1();
//        logger.debug(PubFun1.CreateRuleCalCode("A"));
    }

    public CErrors getErrors() {
        // TODO Auto-generated method stub
        return null;
    }

    public VData getResult() {
        // TODO Auto-generated method stub
        return rResult;
    }
}
