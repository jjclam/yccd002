package com.sinosoft.lis.pubfun;

import com.sinosoft.utility.*;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Random;

public class YwxptMaxNo implements com.sinosoft.lis.pubfun.SysMaxNo {
    private final static Log log4j = LogFactory.getLog(YwxptMaxNo.class);

    public YwxptMaxNo() {
    }

    /**
     * 生成流水号的函数
     * 号码规则：机构编码 日期年 校验位 类型 流水号
     * 1-6 7-10 11 12-13 14-20
     *
     * @param cNoType  为需要生成号码的类型
     * @param cNoLimit 为需要生成号码的限制条件（要么是SN，要么是机构编码）
     * @return 生成的符合条件的流水号，如果生成失败，返回空字符串""
     */
    public String CreateMaxNo(String cNoType, String cNoLimit) {
        VData tvdata = new VData();
        tvdata.add(0, "1");
        tvdata.add(1, "");
        tvdata.add(2, "MAX");
        return CreateMaxNo(cNoType, cNoLimit, tvdata);
    }

    /**
     * 功能：产生指定长度的流水号，一个号码类型一个流水
     *
     * @param cNoType：流水号的类型
     * @param cNoLength：流水号的长度
     * @return 返回产生的流水号码
     */
    public String CreateMaxNo(String cNoType, int cNoLength) {
        return CreateMaxNo(cNoType, "SN", cNoLength);
    }

    public String CreateMaxNo(String cNoType, String cNoLimit, int cNoLength) {
        return CreateMaxNo(cNoType, cNoLimit, cNoLength, 1, "", "MAX");
    }

    public String CreateMaxNo(String cNoType, String cNoLimit, int cNoLength, int n, String cOtherNo, String cOperate) {
        // log4j.info("++++++ no synchronized ++++++");
        // log4j.info(cNoLimit);
        if ((cNoType == null) || (cNoType.trim().length() <= 0) || (cNoLength <= 0)) {
            log4j.info("NoType长度错误或NoLength错误");

            return null;
        }

        cNoType = cNoType.toUpperCase();

        // log4j.info("type:"+cNoType+" length:"+cNoLength);
        Connection conn = DBConnPool.getConnection();

        if (conn == null) {
            log4j.info("CreateMaxNo : fail to get db connection");

            return null;
        }

        String tReturn = "";
        int tMaxNo = 0;
        tReturn = cNoLimit;

        try {
            if (cOperate.equals("STORE")) {

                // log4j.info(cNoType);
                // log4j.info(cNoLimit);
                // log4j.info(cOtherNo);
                String tsql = "select NoType,NoLimit from ldstoreno where " + " otherno = '" + cOtherNo + "'";
                ExeSQL tesql = new ExeSQL();
                SSRS tss = tesql.execSQL(tsql);
                if (tss.getMaxRow() > 0) {
                    cNoType = tss.GetText(1, 1);
                    cNoLimit = tss.GetText(1, 2);
                }
                // log4j.info(cNoType);
                // log4j.info(cNoLimit);

                tMaxNo = getStoreno(conn, cNoType, cNoLimit, cOtherNo);
            } else
                tMaxNo = getMaxno(conn, cNoType, cNoLimit, n, cOtherNo);

            conn.close();
        } catch (Exception Ex) {
            try {
                conn.rollback();
                conn.close();

                return null;
            } catch (Exception e1) {
                e1.printStackTrace();

                return null;
            }
        }
        if (tMaxNo == 0)
            return null;
        String tStr = String.valueOf(tMaxNo);
        tStr = PubFun.LCh(tStr, "0", cNoLength);
        tReturn = tStr.trim();
        // log4j.info(tReturn);
        return tReturn;
    }

    /**
     * <p>
     * 生成流水号的函数,生成特定规则的流水号
     * <p>
     *
     * @param cNoType 为需要生成号码的类型
     * @param cInput  为生成号码需要传入的参数
     * @return 生成的符合条件的流水号，如果生成失败，返回空null
     */
    public String CreateMaxNo(String cNoType, String cInput, VData cVData) {
        // 传入的参数不能为空，如果为空，则直接返回
        if ((cNoType == null) || (cNoType.trim().length() <= 0)) {
            log4j.info("NoType长度错误");
            return null;
        }
        String cOtherNo = "";
        int n = 1;
        String cOperate = "MAX";
        try {
            String no = (String) cVData.getObject(0);
            cOtherNo = (String) cVData.getObject(1);
            cOperate = (String) cVData.getObject(2);
            n = Integer.parseInt(no);
        } catch (Exception ex) {

        }

        String tReturn = null;
        cNoType = cNoType.toUpperCase();
        String strSQL;
        SSRS tSSRS = new SSRS();
        ExeSQL tExeSQL = new ExeSQL();
        // 得到当前日期
        String tDate = PubFun.getCurrentDate();
        String tDateCode = tDate.substring(2, 4) + tDate.substring(5, 7) + tDate.substring(8, 10);

        // 个险单家庭号
        if (cNoType.equals("FamilyNo")) {
            tReturn = "F" + CreateMaxNo(cNoType, 9);
        }

        // 个险客户号
        else if (cNoType.equals("CUSTOMERNO")) {
            tReturn = CreateMaxNo(cNoType, 9);
        }

        // 团险境外救援 印刷号K
        else if (cNoType.equals("GRPPRTNO")) {
            tReturn = "12" + CreateMaxNo(cNoType, 9);
        }

        // 个险投保单号
        else if (cNoType.equals("PROPOSALCONTNO")) {
            String seqNo = CreateMaxNo(cNoType, 9);
            if (seqNo == null) {
                log4j.info("生成号错误！");
                return null;
            }
            tReturn = "13" + seqNo;
            /** 在248上使用 大批量生成号规则 */
            // String seqNo = CreateMaxNo(cNoType, 15);
            // if (seqNo == null) {
            // log4j.info("生成号错误！");
            // return null;
            // }
            // tReturn = "B13" + seqNo;
        }

        // 团险投保单号
        else if (cNoType.equals("PROGRPCONTNO")) {
            String seqNo = CreateMaxNo(cNoType, 8);
            if (seqNo == null) {
                return null;
            }
            tReturn = "14" + seqNo;
            /** 在248服务器上使用大批量生成号 */
            // String seqNo = CreateMaxNo(cNoType, 15);
            // if (seqNo == null) {
            // log4j.info("生成号错误！");
            // return null;
            // }
            // tReturn = "B14" + seqNo;
        }

        // 个单保单号为9位投保人客户号+2位投保次数
        else if (cNoType.equals("CONTNO")) {
            if (cInput == null) {
                log4j.info("请输入个险客户号!");
                return null;
            }
            // if (cInput.length() == 9) {
            // strSQL = "Select count(distinct ContNo) From LCCont " +
            // "Where AppntNo = '" + cInput + "' " +
            // "And AppFlag = '1'";
            // String cMaxNo = tExeSQL.getOneValue(strSQL);
            // strSQL = "Select count(distinct ContNo) From LBCont " +
            // "Where AppntNo = '" + cInput + "' " +
            // "And AppFlag = '1'";
            // String bMaxNo = tExeSQL.getOneValue(strSQL);
            //
            // int addNo = Integer.parseInt(cMaxNo) + Integer.parseInt(bMaxNo) +
            // 1;
            // String tMaxNo = String.valueOf(addNo);
            // if (tMaxNo == null) {
            // log4j.info("查询出错!");
            // return null;
            // }
            // if (tMaxNo.length() == 1) {
            // tMaxNo = "0" + tMaxNo; //补齐两位
            // }
            // tReturn = cInput + tMaxNo;
            // } else {
            // tReturn = "13" + CreateMaxNo(cNoType, 9);
            // }
            String appntNo = cInput;
            String sql = "select AppntNum+1 from LDPerson where CustomerNo='" + appntNo + "'";
            String tMaxNo = tExeSQL.getOneValue(sql);
            if (StrTool.cTrim(tMaxNo).equals("") || StrTool.cTrim(tMaxNo).equals("null")) {
                /** @author:Yangming 初期客户可能没有客户号码暂时兼容此问题 */
                tMaxNo = "1";
            }

            int length = 6;
            if (tMaxNo.length() < length) {
                // tMaxNo = "0" + tMaxNo; //补齐两位

                // 已修改为六位长度
                tMaxNo = PubFun.LCh(tMaxNo, "0", length);
            }
            tReturn = cInput + tMaxNo;
        }

        // 团险保单号
        else if (cNoType.equals("GRPCONTNO")) {
            String seqNo = CreateMaxNo(cNoType, 12);
            if (seqNo == null) {
                return null;
            }
            tReturn = seqNo + "088";

//			if ((cInput == null) || (cInput.length() != 8))
//			{
//				log4j.info("请输入8位团险客户号!");
//				return null;
//			}
//			String appntNo = cInput;
//			String sql = "select nvl(GrpAppntNum,0)+1 from LDGrp where CustomerNo='" + appntNo + "'";
//			String tMaxNo = tExeSQL.getOneValue(sql);
//			if (tMaxNo.indexOf(".") > 0)
//			{
//				tMaxNo = tMaxNo.substring(0, tMaxNo.indexOf("."));
//			}
//
//			int length = 6;
//			if (tMaxNo.length() < length)
//			{
//				// tMaxNo = "0" + tMaxNo; //补齐两位
//
//				// 已修改为六位长度
//				tMaxNo = PubFun.LCh(tMaxNo, "0", length);
//			}
//			tReturn = cInput + tMaxNo;
        }

        // 向春 2005-05-08
        // 保费收据号(个单)
        else if (cNoType.equals("CONTPREMFEENO")) {
            tReturn = "61" + CreateMaxNo(cNoType, 9);
        }
        // 保费收据号(团单)
        else if (cNoType.equals("GContPremFeeNo")) {
            tReturn = "81 " + CreateMaxNo(cNoType, 9);
        }
        // 送达回执号
        else if (cNoType.equals("CUSRECEIPTNO")) {
            if (cInput == null) {
                log4j.info("请输入个单合同号!");
                return null;
            }
            tReturn = "13" + cInput;
        }

        // 个险缴费通知书号 : 个险印刷号+"8"+2位流水号
        else if (cNoType.equals("PAYNOTICENO")) {
            // 这条语句不能加，否者保全会出问题。qulq
            // log4j.info("cInput.length()"+cInput.length());
            if ((cInput == null) || (cInput.length() != 11) && (cInput.length() != 12)) {
                log4j.info("请输入印刷号!");
                tReturn = "31" + CreateMaxNo(cNoType, 9);
                return tReturn;
            }
            strSQL = "Select count(distinct ContNo) From LCCont " + "Where PrtNo = '" + cInput + "' ";
            tSSRS = tExeSQL.execSQL(strSQL);
            String tMaxNo = tSSRS.GetText(1, 1);
            if (tMaxNo == null) {
                log4j.info("查询出错!");
                return null;
            }
            if (tMaxNo.length() == 1) {
                tMaxNo = "0" + tMaxNo; // 补齐两位
            }

            tReturn = cInput + "8" + tMaxNo;
        }
        // 团险缴费通知书号 : 团险印刷号+"8"+2位流水号
        else if (cNoType.equals("GPAYNOTICENO")) {
            String seqNo = CreateMaxNo(cNoType, 12);
            if (seqNo == null) {
                return null;
            }
            tReturn = seqNo + "303";
//			if ((cInput == null) || (cInput.length() != 11))
//			{
//				log4j.info("请输入印刷号!");
//				return null;
//			}
//			strSQL = "Select count(distinct a.otherno)+1 From LOPrtManager a "
//					+ "Where a.OtherNo = (select grpcontno from lcgrpcont where prtno='" + cInput
//					+ "') and a.code='57' ";
//			tSSRS = tExeSQL.execSQL(strSQL);
//			String tMaxNo = tSSRS.GetText(1, 1);
//			// String sql = "select prtno from lcgrpcont where grpcontno='" +
//			// cInput + "'";
//			// cInput = tExeSQL.getOneValue(sql);
//			if (tMaxNo == null)
//			{
//				log4j.info("查询出错!");
//				return null;
//			}
//			if (tMaxNo.length() == 1)
//			{
//				tMaxNo = "0" + tMaxNo; // 补齐两位
//			}
//
//			tReturn = cInput + "8" + tMaxNo;
        }

        // 个险客户通知书号(问题件) 个险印刷号+"1"+2位流水号
        else if (cNoType.equals("IPAYNOTICENO")) {
            if ((cInput == null) || (cInput.length() != 11 && cInput.length() != 12)) {
                log4j.info("请输入客户通知书号!");
                return null;
            }
            strSQL = "select count(1)+1 from loprtmanager where otherno in " + "(Select ContNo From LCCont "
                    + "Where PrtNo = '" + cInput + "' and conttype='1') " + " and code='85'";
            tSSRS = tExeSQL.execSQL(strSQL);
            String tMaxNo = tSSRS.GetText(1, 1);
            if (tMaxNo == null) {
                log4j.info("查询出错!");
                return null;
            }
            if (tMaxNo.length() == 1) {
                tMaxNo = "0" + tMaxNo; // 补齐两位
            }
            tReturn = cInput + "1" + tMaxNo;
        }

        // 保全个险客户通知书号(问题件) 个险印刷号+"1"+2位流水号
        else if (cNoType.equals("BQIPAYNOTICENO")) {
            if ((cInput == null) || (cInput.length() != 11 && cInput.length() != 12)) {
                log4j.info("请输入保单印刷号!");
                return null;
            }
            strSQL = "select substr(max(PrtSeq), 13) from loprtmanager " + "where PrtSeq like '" + cInput + "1%'";
            tSSRS = tExeSQL.execSQL(strSQL);
            String tMaxNo = tSSRS.GetText(1, 1);
            if ((tMaxNo == null) || (tMaxNo.equals(""))) {
                tMaxNo = "0";
            }
            tMaxNo = String.valueOf(Integer.parseInt(tMaxNo) + 1);
            if (tMaxNo == null) {
                log4j.info("查询出错!");
                return null;
            }
            if (tMaxNo.length() == 1) {
                tMaxNo = "0" + tMaxNo; // 补齐两位
            }
            tReturn = cInput + "1" + tMaxNo;
        }

        // 团险客户通知书号(问题件) 团险印刷号+"1"+2位流水号
        else if (cNoType.equals("GIPAYNOTICENO")) {
            if (cInput == null || "".equals(cInput)) {
                log4j.info("参数传入错误，缺少参数!");
                return null;
            }
            strSQL = "Select count(1) From loprtmanager "
                    + "Where otherNo = (select grpcontno from lcgrpcont where prtno='" + cInput + "' ) and code='54'";
            tSSRS = tExeSQL.execSQL(strSQL);
            String tMaxNo = String.valueOf(Integer.parseInt(tSSRS.GetText(1, 1)) + 1);
            if (tMaxNo == null) {
                log4j.info("查询出错!");
                return null;
            }
            if (tMaxNo.length() == 1) {
                tMaxNo = "0" + tMaxNo; // 补齐两位
            }
            tReturn = cInput + "1" + tMaxNo;
        }

        // 个险客户通知书号(体检件) 个险印刷号+"2"+2位流水号
        else if (cNoType.equals("PPAYNOTICENO")) {
            if ((cInput == null) || (cInput.length() != 11 && cInput.length() != 12)) {
                log4j.info("请输入客户通知书号!");
                return null;
            }
            strSQL = "select count(1) from loprtmanager where otherno=" + "(Select ContNo From LCCont "
                    + "Where PrtNo = '" + cInput + "') " + " and code='03'";
            tSSRS = tExeSQL.execSQL(strSQL);
            String tMaxNo = tSSRS.GetText(1, 1);
            tMaxNo = String.valueOf(Integer.parseInt(tMaxNo) + 1);
            if (tMaxNo == null) {
                log4j.info("查询出错!");
                return null;
            }
            if (tMaxNo.length() == 1) {
                tMaxNo = "0" + tMaxNo; // 补齐两位
            }
            tReturn = cInput + "2" + tMaxNo;
        }

        // 保全个险客户通知书号(体检件) 个险印刷号+"2"+2位流水号
        else if (cNoType.equals("EDORPPAYNOTICENO")) {
            if ((cInput == null) || (cInput.length() != 11 && cInput.length() != 12)) {
                log4j.info("请输入保单印刷号!");
                return null;
            }
            strSQL = "select substr(max(PrtSeq), 13) from loprtmanager " + "where PrtSeq like '" + cInput + "2%'";
            tSSRS = tExeSQL.execSQL(strSQL);
            String tMaxNo = tSSRS.GetText(1, 1);
            if ((tMaxNo == null) || (tMaxNo.equals(""))) {
                tMaxNo = "0";
            }
            tMaxNo = String.valueOf(Integer.parseInt(tMaxNo) + 1);
            if (tMaxNo == null) {
                log4j.info("查询出错!");
                return null;
            }
            if (tMaxNo.length() == 1) {
                tMaxNo = "0" + tMaxNo; // 补齐两位
            }
            tReturn = cInput + "2" + tMaxNo;
        }

        // 团险客户通知书号(体检件) 团险印刷号+"2"+2位流水号
        else if (cNoType.equals("GPPAYNOTICENO")) {
            if ((cInput == null) || (cInput.length() != 11)) {
                log4j.info("请输入客户通知书号!");
                return null;
            }
            strSQL = "Select count(distinct GrpContNo) From LCGrpCont " + "Where PrtNo = '" + cInput + "' ";
            tSSRS = tExeSQL.execSQL(strSQL);
            String tMaxNo = tSSRS.GetText(1, 1);
            if (tMaxNo == null) {
                log4j.info("查询出错!");
                return null;
            }
            if (tMaxNo.length() == 1) {
                tMaxNo = "0" + tMaxNo; // 补齐两位
            }
            tReturn = cInput + "2" + tMaxNo;
        }

        // 个险催办通知书号(催办体检件) 个险印刷号+"0"+2位流水号
        else if (cNoType.equals("URGEPENOTICENO")) {
            if ((cInput == null) || (cInput.length() != 11 && cInput.length() != 12)) {
                log4j.info("请输入客户通知书号!");
                return null;
            }
            strSQL = "select count(1) from loprtmanager where otherno=" + "(Select ContNo From LCCont "
                    + "Where PrtNo = '" + cInput + "') " + " and code='11'";
            tSSRS = tExeSQL.execSQL(strSQL);
            String tMaxNo = tSSRS.GetText(1, 1);
            tMaxNo = String.valueOf(Integer.parseInt(tMaxNo) + 1);
            if (tMaxNo == null) {
                log4j.info("查询出错!");
                return null;
            }
            if (tMaxNo.length() == 1) {
                tMaxNo = "0" + tMaxNo; // 补齐两位
            }
            tReturn = cInput + "02" + tMaxNo;

        }
        // 个险催办通知书号(催办问题见件) 个险印刷号+"0"+2位流水号
        else if (cNoType.equals("URGEWTNOTICENO")) {
            if ((cInput == null) || (cInput.length() != 11 && cInput.length() != 12)) {
                log4j.info("请输入客户通知书号!");
                return null;
            }
            strSQL = "select count(1) from loprtmanager where otherno=" + "(Select ContNo From LCCont "
                    + "Where PrtNo = '" + cInput + "') " + " and code='18'";
            tSSRS = tExeSQL.execSQL(strSQL);
            String tMaxNo = tSSRS.GetText(1, 1);
            tMaxNo = String.valueOf(Integer.parseInt(tMaxNo) + 1);
            if (tMaxNo == null) {
                log4j.info("查询出错!");
                return null;
            }
            if (tMaxNo.length() == 1) {
                tMaxNo = "0" + tMaxNo; // 补齐两位
            }
            tReturn = cInput + "01" + tMaxNo;

        }
        // 个险催办通知书号(催办承保计划变更) 个险印刷号+"0"+2位流水号
        else if (cNoType.equals("URGEJHBGNOTICENO")) {
            if ((cInput == null) || (cInput.length() != 11 && cInput.length() != 12)) {
                log4j.info("请输入客户通知书号!");
                return null;
            }
            strSQL = "select count(1) from loprtmanager where otherno=" + "(Select ContNo From LCCont "
                    + "Where PrtNo = '" + cInput + "') " + " and code='19'";
            tSSRS = tExeSQL.execSQL(strSQL);
            String tMaxNo = tSSRS.GetText(1, 1);
            tMaxNo = String.valueOf(Integer.parseInt(tMaxNo) + 1);
            if (tMaxNo == null) {
                log4j.info("查询出错!");
                return null;
            }
            if (tMaxNo.length() == 1) {
                tMaxNo = "0" + tMaxNo; // 补齐两位
            }
            tReturn = cInput + "09" + tMaxNo;

        }

        // 个险催办通知书号(催办首期交费) 个险印刷号+"0"+2位流水号
        else if (cNoType.equals("URGEJFNOTICENO")) {
            if ((cInput == null) || (cInput.length() != 11 && cInput.length() != 12)) {
                log4j.info("请输入客户通知书号!");
                return null;
            }
            strSQL = "select count(1) from loprtmanager where otherno=" + "(Select ContNo From LCCont "
                    + "Where PrtNo = '" + cInput + "') " + " and code='17'";
            tSSRS = tExeSQL.execSQL(strSQL);
            String tMaxNo = tSSRS.GetText(1, 1);
            tMaxNo = String.valueOf(Integer.parseInt(tMaxNo) + 1);
            if (tMaxNo == null) {
                log4j.info("查询出错!");
                return null;
            }
            if (tMaxNo.length() == 1) {
                tMaxNo = "0" + tMaxNo; // 补齐两位
            }
            tReturn = cInput + "08" + tMaxNo;

        }

        // 个险或团险客户通知书号(契调件) 个险印刷号+"3"+2位流水号
        else if (cNoType.equals("RPAYNOTICENO")) {
            if ((cInput == null) || (cInput.length() != 11 && cInput.length() != 12)) {
                log4j.info("请输入客户通知书号!");
                return null;
            }
            strSQL = "select count(1) from loprtmanager where otherno=" + "(Select ProposalContNo From LCCont "
                    + "Where PrtNo = '" + cInput + "') " + " and code='04'";
            tSSRS = tExeSQL.execSQL(strSQL);
            String tMaxNo = tSSRS.GetText(1, 1);
            tMaxNo = String.valueOf(Integer.parseInt(tMaxNo) + 1);
            if (tMaxNo == null) {
                log4j.info("查询出错!");
                return null;
            }
            if (tMaxNo.length() == 1) {
                tMaxNo = "0" + tMaxNo; // 补齐两位
            }
            tReturn = cInput + "3" + tMaxNo;

        }
        // 个险或团险客户通知书号(契调件) 团险印刷号+"3"+2位流水号
        else if (cNoType.equals("GRPAYNOTICENO")) {
            if ((cInput == null) || (cInput.length() != 11 && cInput.length() != 12)) {
                log4j.info("请输入客户通知书号!");
                return null;
            }
            strSQL = "Select count(distinct GrpContNo) From LCGrpCont " + "Where PrtNo = '" + cInput + "' ";
            tSSRS = tExeSQL.execSQL(strSQL);
            String tMaxNo = tSSRS.GetText(1, 1);
            if (tMaxNo == null) {
                log4j.info("查询出错!");
                return null;
            }
            if (tMaxNo.length() == 1) {
                tMaxNo = "0" + tMaxNo; // 补齐两位
            }

            tReturn = cInput + "3" + tMaxNo;
        }
        // 个单核保通知书
        else if (cNoType.equals("UWRESULTNOTICE")) {
            if ((cInput == null) || (cInput.length() != 11 && cInput.length() != 12)) {
                log4j.info("请输入客户通知书号!");
                return null;
            }
            strSQL = "select count(1)+1 from loprtmanager where otherno=" + "(Select ProposalContNo From LCCont "
                    + "Where PrtNo = '" + cInput + "') " + " and code in ('05','06','00','09')";
            tSSRS = tExeSQL.execSQL(strSQL);
            String tMaxNo = tSSRS.GetText(1, 1);
            if (tMaxNo == null) {
                log4j.info("查询出错!");
                return null;
            }
            if (tMaxNo.length() == 1) {
                tMaxNo = "0" + tMaxNo; // 补齐两位
            }
            tReturn = cInput + "9" + tMaxNo;
        }

        // //////////////////////////////////////////////////////////////////////////////////

        // 团险中的个单保单号
        else if (cNoType.equals("GRPPERSONCONTNO")) {
            //modify on 2008-8-26 团单号+五位流水号
            if (cInput == null || "".equals(cInput)) {
                log4j.info("团体保单号未传入，请检查!");
                return null;
            }
            tReturn = cInput + CreateMaxNo(cInput, 5);

            //tReturn = "23" + CreateMaxNo(cNoType, 8);
            /** 大批量生成号,在248上使用 */
            // String seqNo = CreateMaxNo(cNoType, 15);
            // if (seqNo == null) {
            // log4j.info("生成号错误！");
            // return null;
            // }
            // tReturn = "B23" + seqNo;
        }

        // 保单险种号码
        else if (cNoType.equals("POLNO")) {

            tReturn = "21" + CreateMaxNo(cNoType, 9);

            /** 大批量生成号 */
            // String seqNo = CreateMaxNo(cNoType, 15);
            // if (seqNo == null) {
            // log4j.info("生成号错误！");
            // return null;
            // }
            // tReturn = "B21" + seqNo;
        }

        // 集体保单险种号码
        else if (cNoType.equals("GRPPOLNO")) {

            tReturn = "22" + CreateMaxNo(cNoType, 8);

            /** 大批量生成号 */
            // String seqNo = CreateMaxNo(cNoType, 15);
            // if (seqNo == null) {
            // log4j.info("生成号错误！");
            // return null;
            // }
            // tReturn = "B22" + seqNo;
        }

        // 个险投保书印刷号
        else if (cNoType.equals("PRINTNO")) {
            tReturn = "16" + CreateMaxNo(cNoType, 9);
        }

        // 团险客户号
        else if (cNoType.equals("GRPNO")) {
            tReturn = CreateMaxNo(cNoType, 8);
        }

        // 团险被保险人客户号
        else if (cNoType.equals("GRPINSUREDNO")) {
            if ((cInput == null) || (cInput.length() != 9)) {
                log4j.info("请输入9位客户号!");
                return null;
            }
            tReturn = cInput + CreateMaxNo(cNoType, 2);
        }

        // 团险询价号码为1位英文字母R+8位客户号+3位代表询价次数
        else if (cNoType.equals("GRPQUERYNO")) {
            if ((cInput == null) || (cInput.length() != 8)) {
                log4j.info("请输入8位客户号!");
                return null;
            }
            tReturn = "R" + cInput + CreateMaxNo(cNoType, 3);
        } else if (cNoType.equals("PROPOSALNO")) {
            String seqNo = CreateMaxNo(cNoType, 9);
            if (seqNo == null) {
                log4j.info("生成号码错误");
                return null;
            }
            tReturn = "11" + seqNo;
        }

        // 集体投保单险种号码
        else if (cNoType.equals("GRPPROPOSALNO")) {
            tReturn = "12" + CreateMaxNo(cNoType, 9);
        }

        // 团险确认型投保书印刷号
        else if (cNoType.equals("GRPCOMFIRMPRTNO")) {
            tReturn = "18" + CreateMaxNo(cNoType, 9);
        }

        // 团险询价型投保书印刷号
        else if (cNoType.equals("GRPQUERYPRTNO")) {
            tReturn = "19" + CreateMaxNo(cNoType, 9);
        }

        // 单证条形码为类型码＋9位客户号＋2位随机号 --类型码?
        else if (cNoType.equals("BARCODENO")) {
            if (cInput == null) {
                log4j.info("请输入9位客户号!");
                return null;
            }
            Random random = new Random();
            tReturn = cInput + Math.abs(random.nextInt()) % 100;
        }

        // 理赔号为理赔号分类编码+二级机构编码+三级机构编码+日期编码+当日流水号
        // 理赔通知号
        else if (cNoType.equals("NOTICENO")) {
            // if ((cInput == null) || (cInput.length() != 4))
            // {
            // log4j.info("理赔号参数输入不正确!");
            // return null;
            // }
            cInput = getClaimLimit(cInput);
            tReturn = "T" + cInput + tDateCode + CreateMaxNo("T" + cInput + tDateCode, 6);
        }
        // 理赔咨询号

        else if (cNoType.equals("COSULTNO")) {
            // if ((cInput == null) || (cInput.length() != 4))
            // {
            // log4j.info("理赔号参数输入不正确!");
            // return null;
            // }
            cInput = getClaimLimit(cInput);
            tReturn = "Z" + cInput + tDateCode + CreateMaxNo("Z" + cInput + tDateCode, 6);
        }

        // 理赔通知咨询号
        else if (cNoType.equals("CNNO")) {
            // if ((cInput == null) || (cInput.length() != 4))
            // {
            // log4j.info("理赔号参数输入不正确!");
            // return null;
            // }
            cInput = getClaimLimit(cInput);
            tReturn = "H" + cInput + tDateCode + CreateMaxNo(cNoType + tDateCode, 6);
        }

        // 理赔申请号
        else if (cNoType.equals("CASENO")) {
            // if ((cInput == null) || (cInput.length() != 4))
            // {
            // log4j.info("理赔号参数输入不正确!");
            // return null;
            // }
            cInput = getClaimLimit(cInput);
            if (cOperate.equals("STORE")) {
                // log4j.info(cInput);
                // log4j.info(tDateCode);
                String tsql = "select NoLimit from ldstoreno where " + " otherno = '" + cOtherNo + "'";
                ExeSQL tesql = new ExeSQL();
                SSRS tss = tesql.execSQL(tsql);
                if (tss.getMaxRow() > 0) {
                    tDateCode = tss.GetText(1, 1);
                }
            }
            tReturn = "C" + cInput + tDateCode + CreateMaxNo("C" + cInput, tDateCode, 6, n, cOtherNo, cOperate);
        }

        // 理赔申诉号
        else if (cNoType.equals("APPEALNO")) {
            // if ((cInput == null) || (cInput.length() != 4))
            // {
            // log4j.info("理赔号参数输入不正确!");
            // return null;
            // }
            cInput = getClaimLimit(cInput);
            tReturn = "S" + cInput + tDateCode + CreateMaxNo("S" + cInput + tDateCode, 6);
        }
        // 帐户轨迹表的SerialNo
        else if (cNoType.equals("LLACCTRACENO")) {
            tReturn = cInput + CreateMaxNo(cInput, 12);
        }

        // 理赔错误处理号
        else if (cNoType.equals("LLERRORNO")) {
            // if ((cInput == null) || (cInput.length() != 4))
            // {
            // log4j.info("理赔号参数输入不正确!");
            // return null;
            // }
            cInput = getClaimLimit(cInput);
            tReturn = "R" + cInput + tDateCode + CreateMaxNo("R" + cInput + tDateCode, 6);
        }

        // 理赔批次号
        else if (cNoType.equals("RGTNO")) {
            // if ((cInput == null) || (cInput.length() != 4))
            // {
            // log4j.info("理赔号参数输入不正确!");
            // return null;
            // }
            cInput = getClaimLimit(cInput);
            tReturn = "P" + cInput + tDateCode + CreateMaxNo("P" + cInput, tDateCode, 6);
        }

        // 理赔事件号
        else if (cNoType.equals("SUBRPTNO")) {
            // if ((cInput == null) || (cInput.length() != 4))
            // {
            // log4j.info("理赔号参数输入不正确!");
            // return null;
            // }
            cInput = getClaimLimit(cInput);
            tReturn = "A" + cInput + tDateCode + CreateMaxNo("A" + cInput + tDateCode, 6);
        }

        // 交费通知书号码
        else if (cNoType.equals("PAYNOTICENO")) {
            tReturn = CreateMaxNo(cNoType, 8) + "305";
        }

        // 交费收据号码
        else if (cNoType.equals("PAYNO")) {
            tReturn = "32" + CreateMaxNo(cNoType, 9);
            /** 大批量生成号码 */
            // String seqNo = CreateMaxNo(cNoType, 15);
            // if (seqNo == null) {
            // log4j.info("生成号错误！");
            // return null;
            // }
            // tReturn = "B32" + seqNo;
        }

        // 给付通知书号码
        else if (cNoType.equals("GETNOTICENO")) {
            tReturn = CreateMaxNo(cNoType, 8) + "306";
            //tReturn = "36" + CreateMaxNo(cNoType, 9);
        }

        // 给付通知书号码
        else if (cNoType.equals("GETNO")) {
            tReturn = "37" + CreateMaxNo(cNoType, 9);
        }

        // 批改申请号码
        else if (cNoType.equals("EDORAPPNO")) {
            tReturn = "41" + CreateMaxNo(cNoType, 9);
        }

        // 批单号码
        else if (cNoType.equals("EDORNO")) {
            tReturn = "42" + CreateMaxNo(cNoType, 9);
        }

        // 集体批单申请号码
        else if (cNoType.equals("EDORGRPAPPNO")) {
            tReturn = "43" + CreateMaxNo(cNoType, 9);
        }

        // 集体批单号码
        else if (cNoType.equals("EDORGRPNO")) {
            tReturn = "44" + CreateMaxNo(cNoType, 9);
        }

        // 报案编号
        else if (cNoType.equals("RPTNO")) {
            tReturn = "50" + CreateMaxNo(cNoType, 9);
        }

        // 立案编号
        else if (cNoType.equals("RGTNO")) {
            tReturn = "51" + CreateMaxNo(cNoType, 9);
        }

        // 赔案编号
        else if (cNoType.equals("CLMNO")) {
            tReturn = "52" + CreateMaxNo(cNoType, 9);
        }

        // 拒案编号
        else if (cNoType.equals("DECLINENO")) {
            tReturn = "53" + CreateMaxNo(cNoType, 9);
        }

        // 报案分案编号
        else if (cNoType.equals("SUBRPTNO")) {
            tReturn = "54" + CreateMaxNo(cNoType, 9);
        }

        // 立案分案编号
        else if (cNoType.equals("CASENO")) {
            tReturn = "55" + CreateMaxNo(cNoType, 9);
        }

        // 合同号
        else if (cNoType.equals("PROTOCOLNO")) {
            tReturn = "71" + CreateMaxNo(cNoType, 9);
        }

        // 单证印刷号码
        else if (cNoType.equals("PRTNO")) {
            tReturn = "80" + CreateMaxNo(cNoType, 9);
        }

        // 打印管理流水号
        else if (cNoType.equals("PRTSEQNO")) {
            tReturn = "81" + CreateMaxNo(cNoType, 9);
        }

        // 打印管理流水号
        else if (cNoType.equals("PRTSEQ2NO")) {
            tReturn = "82" + CreateMaxNo(cNoType, 9);
        }

        // 回收清算单号
        else if (cNoType.equals("TAKEBACKNO")) {
            tReturn = "61" + CreateMaxNo(cNoType, 9);
        }

        // 银行代扣代付批次号
        else if (cNoType.equals("BATCHNO")) {
            tReturn = "62" + CreateMaxNo(cNoType, 9);
        }

        // 接口凭证id号
        else if (cNoType.equals("VOUCHERIDNO")) {
            tReturn = "63" + CreateMaxNo(cNoType, 9);
        }

        // 佣金号码
        else if (cNoType.equals("WAGENO")) {
            tReturn = "90" + CreateMaxNo(cNoType, 9);
        }

        // 流水号
        else if (cNoType.equals("SERIALNO")) {
            tReturn = "98" + CreateMaxNo(cNoType, 9);
        }
        // 团单个人服务流水号
        // else if (cNoType.equals("LHGRPSERVNO")) {
        // tReturn = "G" + CreateMaxNo(cNoType, 15);
        // }
        // 健管短信模版LHMSGTP
        else if (cNoType.equals("LHMSGTC")) {
            tReturn = "MC" + CreateMaxNo(cNoType, 8);
        } else if (cNoType.equals("LHMSGTP")) {
            tReturn = "MT" + CreateMaxNo(cNoType, 8);
        }
        // LJTempFeeClassB的seqno
        else if (cNoType.equals("TEMPFEESEQNO")) {
            tReturn = CreateMaxNo(cNoType, 20);
        } else if (cNoType.equals("ASKGRPCONTNO")) {
            String strSql = "select count(1) from lcgrpcont where AppntNo='" + cInput + "' and appflag='8' ";
            String MaxNo = tExeSQL.getOneValue(strSql);
            MaxNo = String.valueOf(Integer.parseInt(MaxNo) + 1);
            if (MaxNo == null) {
                log4j.info("查询出错!");
                return null;
            }
            if (MaxNo.length() == 1) {
                MaxNo = "00" + MaxNo; // 补齐两位
            }
            if (MaxNo.length() == 2) {
                MaxNo = "0" + MaxNo; // 补齐两位
            }
            tReturn = cInput + MaxNo;
        } else {
            // modified by Xx 不用进zhongyi的最大号生成文件
            if (!cInput.equals("SN")) {
                if (cInput.length() >= 4) {
                    cInput = cInput.substring(0, 4);
                } else {
                    cInput = PubFun.RCh(cInput, "0", 4);
                }
                tReturn = cInput + CreateMaxNo(cNoType, cInput, 10);
            } else {
                int serialLen = 10;
                if (cNoType.equals("COMMISIONSN") || cNoType.equals("GRPNO") || cNoType.equals("CUSTOMERNO")
                        || cNoType.equals("SUGDATAITEMCODE") || cNoType.equals("SUGITEMCODE")
                        || cNoType.equals("SUGMODELCODE") || cNoType.equals("SUGCODE")) {
                    serialLen = 9;
                    tReturn = CreateMaxNo(cNoType, cInput, serialLen);
                    tReturn = tReturn + "0";
                } else {
                    tReturn = CreateMaxNo(cNoType, cInput, serialLen);
                }
            }
        }

        return tReturn;
    }

    private String getClaimLimit(String MngComCode) {
        return MngComCode.substring(2, 6);
    }

    public static void main(String[] args) {

    }

    static int positionedUpdateWithoutSubqueries(Connection con, String cNoType, String cNoLimit) {

        int dept = 0;
        try {
            con.setAutoCommit(false);
            String name = null;
            String curName = null;

            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery("select MaxNo from LDMaxNo where notype='" + cNoType + "' and nolimit='"
                    + cNoLimit + "'  FOR UPDATE OF maxno");

            curName = rs.getCursorName();
            Statement stmt1 = con.createStatement();
            while (rs.next()) {
                dept = rs.getInt(1);
                log4j.info("nomx = " + dept);
                stmt1.executeUpdate("UPDATE ldmaxno " + "  SET maxno = maxno + 1 " + "  WHERE CURRENT OF " + curName);
            }
            stmt1.close();
            rs.close();
            stmt.close();

            con.commit();

            log4j.info("new maxno function");
        } catch (Exception e) {
            // JdbcException jdbcExc = new JdbcException(e, con);
            // jdbcExc.handle();
            try {
                con.rollback();
            } catch (SQLException ex) {
            }
            e.printStackTrace();
        }
        return dept;
    } // positionedUpdateWithoutSubqueries

    /**
     * 流水号生成
     *
     * @param con      Connection
     * @param cNoType  String
     * @param cNoLimit String
     * @param n        int (Added by Xx,如果需要预留号或跳号，传入大于1的值，默认值为1)
     * @return int
     */
    static int getMaxno(Connection con, String cNoType, String cNoLimit, int n, String cOtherNo) {
        n = (n < 1) ? 1 : n;
        int dept = 0;
        try {
            con.setAutoCommit(false);
            // log4j.info(cNoType);
            // log4j.info(cNoLimit);
            Statement stmt = con.createStatement();

            ResultSet rs1 = stmt.executeQuery("update LDMaxNo set MaxNo=MaxNo+" + n + " where notype='" + cNoType
                    + "' and nolimit='" + cNoLimit + "'");

            ResultSet rs = stmt.executeQuery("select maxno from  ldmaxno where notype='" + cNoType + "' and nolimit='"
                    + cNoLimit + "'");

            // ResultSet rs = stmt.executeQuery("select MaxNo from final table
            // (update LDMaxNo set MaxNo=MaxNo+" + n
            // + " where notype='" + cNoType + "' and nolimit='" + cNoLimit +
            // "')");
            while (rs.next()) {
                dept = rs.getInt(1);
                log4j.info("nomx = " + dept);
            }
            rs.close();
            stmt.close();
            if (dept == 0) {
                String strSQL = "insert into ldmaxno(notype, nolimit, maxno) values('" + cNoType + "', '" + cNoLimit
                        + "', " + n + ")";
                ExeSQL exeSQL = new ExeSQL(con);

                if (!exeSQL.execUpdateSQL(strSQL)) {
                    log4j.info("CreateMaxNo 插入失败，请重试!");
                    con.rollback();
                } else {
                    dept = 1;
                }
            }
            if (n >= 1) {
                /*
                 * Modify by chenlao 20080626
				 * Desc:由于团体理赔申请的时候如果团体申请为1个人的话在申请人确认的时候报预留号码出错，针对此问题进行了修改。
				 */
                if (!"".equals(cOtherNo)) {
                    String strSQL = "insert into ldstoreno(notype, nolimit,otherno,currentno, maxno,count) values('"
                            + cNoType + "', '" + cNoLimit + "','" + cOtherNo + "',";
                    if (dept > 1) {
                        strSQL += (dept - n) + "," + (dept) + "," + n + ")";
                    } else {
                        strSQL += 0 + "," + n + "," + n + ")";
                    }
                    ExeSQL exeSQL = new ExeSQL(con);

                    if (!exeSQL.execUpdateSQL(strSQL)) {
                        log4j.info("CreateMaxNo 插入失败，请重试!");
                        con.rollback();
                    }

                }
                con.commit();

            }

            // log4j.info("new maxno function");
        } catch (Exception e) {
            // JdbcException jdbcExc = new JdbcException(e, con);
            // jdbcExc.handle();
            try {
                con.rollback();
            } catch (SQLException ex) {
            }
            e.printStackTrace();
        }
        return dept;
    } // positionedUpdateWithoutSubqueries

    /**
     * 使用预留号
     *
     * @param con      Connection
     * @param cNoType  String
     * @param cNoLimit String
     * @return int
     */
    static int getStoreno(Connection con, String cNoType, String cNoLimit, String cOtherNo) {
        int dept = 0;
        int max = 0;
        try {
            con.setAutoCommit(false);

            Statement stmt = con.createStatement();
            // ResultSet rsupdate = stmt.executeQuery("select currentno,maxno
            // from final table (update LDStoreNo set currentno=currentno+1"
            // + " where notype='" + cNoType + "' and nolimit='" + cNoLimit + "'
            // and otherno='" + cOtherNo + "')");

            String updateSQL = "update LDStoreNo set currentno=currentno+1 where" + " notype='" + cNoType
                    + "' and nolimit='" + cNoLimit + "' and otherno='" + cOtherNo + "'";
            String querySQL = "select currentno,maxno from LDStoreNo where " + " notype='" + cNoType
                    + "' and nolimit='" + cNoLimit + "' and otherno='" + cOtherNo + "'";
            // ResultSet rsupdate = stmt.executeQuery("update LDStoreNo set
            // currentno=currentno+1 "
            // + "where notype='" + cNoType + "' and nolimit='" + cNoLimit + "'
            // and otherno='" + cOtherNo + "')");
            // ResultSet rsquery = stmt.executeQuery("select currentno,maxno
            // from LDStoreNo"
            // +"where notype='" + cNoType + "' and nolimit='" + cNoLimit + "'
            // and otherno='" + cOtherNo + "')");
            // log4j.info("select currentno,maxno from final table
            // (update LDStoreNo set currentno=currentno+1"
            // + " where notype='" + cNoType + "' and nolimit='" + cNoLimit + "'
            // and otherno='" + cOtherNo + "')");
            log4j.info("UPDATESQL:" + updateSQL);
            log4j.info("QuerySQL:" + querySQL);
            ResultSet rsupdate = stmt.executeQuery(updateSQL);
            ResultSet rs = stmt.executeQuery(querySQL);
            //
            // ResultSet rs1 = stmt.executeQuery("update LDMaxNo set
            // MaxNo=MaxNo+" + n
            // + " where notype='" + cNoType + "' and nolimit='" + cNoLimit +
            // "'");

            // ResultSet rs=stmt.executeQuery("select maxno from ldmaxno where
            // notype='" + cNoType + "' and nolimit='" + cNoLimit + "'");
            while (rs.next()) {
                dept = rs.getInt(1);
                max = rs.getInt(2);
                // log4j.info("cNoType = " + cNoType);
                // log4j.info("cNoLimit = " + cNoLimit);
                // log4j.info("nomx = " + dept);
            }
            rs.close();
            stmt.close();
            if (dept == 0 || dept > max) {
                log4j.info("CreateMaxNo 插入失败，请重试!");
                dept = 0;
                con.rollback();
            }
            con.commit();

            // log4j.info("new maxno function");
        } catch (Exception e) {
            try {
                con.rollback();
            } catch (SQLException ex) {
            }
            e.printStackTrace();
        }
        return dept;
    }

}
