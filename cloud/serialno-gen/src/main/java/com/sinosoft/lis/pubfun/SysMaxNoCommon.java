/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */
package com.sinosoft.lis.pubfun;

import com.sinosoft.lis.maxno.CommonSeqStrGen;
import com.sinosoft.utility.DBConnPool;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.VData;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.math.BigInteger;
import java.sql.Connection;

/**
 * <p>
 * Title: Web业务系统
 * </p>
 * <p>
 * Description:系统号码管理（MSRS核心业务系统）生成系统号码
 * </p>
 * <p>
 * Copyright: Copyright (c) 2007
 * </p>
 * <p>
 * Company: Sinosoft
 * </p>
 *
 * @author tongmeng
 * @version 1.0
 */

public class SysMaxNoCommon implements com.sinosoft.lis.pubfun.SysMaxNo {
    private  static Log logger = LogFactory.getLog(SysMaxNoCommon.class);

    public SysMaxNoCommon() {
    }

    /**
     * <p>
     * 生成流水号的函数
     * <p>
     * <p>
     * 号码规则：机构编码 日期年 校验位 类型 流水号
     * <p>
     * <p>
     * 1-6 7-10 11 12-13 14-20
     * <p>
     *
     * @param cNoType  为需要生成号码的类型
     * @param cNoLimit 为需要生成号码的限制条件（要么是SN，要么是机构编码）
     * @param cVData   为需要生成号码的业务相关限制条件
     * @return 生成的符合条件的流水号，如果生成失败，返回空字符串""
     */
    public String CreateMaxNo(String cNoType, String cNoLimit, VData cVData) {
        // 传入的参数不能为空，如果为空，则直接返回
        if ((cNoType == null) || (cNoType.trim().length() <= 0)
                || (cNoLimit == null)) {
            logger.debug("NoType长度错误或者NoLimit为空");
            return null;
        }

        // 默认流水号位数
        int serialLen = 10;

        String tReturn = null;
        cNoType = cNoType.toUpperCase();
        // logger.debug("-----------cNoType:"+cNoType+"
        // cNoLimit:"+cNoLimit);

        Connection conn = DBConnPool.getConnection();

        if (conn == null) {
            logger.debug("CreateMaxNo : fail to get db connection");
            return tReturn;
        }

        int tMaxNo = 0;
        cNoLimit = tReturn;
        // logger.debug("cNoLimit:"+cNoLimit);

        try {
            // 开始事务
            // 查询结果有3个： -- added by Fanym
            // 全部采用直接执行SQL语句，只要有其他事务锁定了本行，立即返回NULL
            // 如果没有锁定，则本事务锁定，查询得到结果则UPDATE，没有则INSERT
            conn.setAutoCommit(false);

            String strSQL = "select MaxNo from LDMaxNo where notype='"
                    + cNoType + "' and nolimit='" + cNoLimit + "' for update";

            ExeSQL exeSQL = new ExeSQL(conn);
            String result = null;
            result = exeSQL.getOneValue(strSQL);

            // 测试返回bull时
            if (exeSQL.mErrors.needDealError()) {
                logger.debug("查询LDMaxNo出错，请稍后!");
                conn.rollback();
                conn.close();

                return null;
            }

            if ((result == null) || result.equals("")) {
                strSQL = "insert into ldmaxno(notype, nolimit, maxno) values('"
                        + cNoType + "', '" + cNoLimit + "', 1)";
                exeSQL = new ExeSQL(conn);

                if (!exeSQL.execUpdateSQL(strSQL)) {
                    logger.debug("CreateMaxNo 插入失败，请重试!");
                    conn.rollback();
                    conn.close();

                    return null;
                } else {
                    tMaxNo = 1;
                }
            } else {
                strSQL = "update ldmaxno set maxno = maxno + 1"
                        + " where notype = '" + cNoType + "' and nolimit = '"
                        + cNoLimit + "'";
                exeSQL = new ExeSQL(conn);

                if (!exeSQL.execUpdateSQL(strSQL)) {
                    logger.debug("CreateMaxNo 更新失败，请重试!");
                    conn.rollback();
                    conn.close();

                    return null;
                } else {
                    tMaxNo = Integer.parseInt(result) + 1;
                }
            }

            conn.commit();
            conn.close();
        } catch (Exception Ex) {
            try {
                conn.rollback();
                conn.close();

                return null;
            } catch (Exception e1) {
                e1.printStackTrace();

                return null;
            }
        }

        String tStr = String.valueOf(tMaxNo);
        tStr = PubFun.LCh(tStr, "0", serialLen);
        if (tReturn.equals("SN") || cNoType.equals("YBTBATDAYNO")) {
            tReturn = tStr.trim();
            // tReturn = tStr.trim() + "0";
        } else {
            tReturn = tReturn.trim() + tStr.trim();
        }
        logger.debug("------tReturn:" + tReturn);
        return tReturn;
    }

    /**
     * <p>
     * 生成流水号的函数
     * <p>
     * <p>
     * 号码规则：机构编码 日期年 校验位 类型 流水号
     * <p>
     * <p>
     * 1-6 7-10 11 12-13 14-20
     * <p>
     *
     * @param cNoType  为需要生成号码的类型
     * @param cNoLimit 为需要生成号码的限制条件（要么是SN，要么是机构编码）
     * @return 生成的符合条件的流水号，如果生成失败，返回空字符串""
     */
    public String CreateMaxNo(String cNoType, String cNoLimit) {
        // 传入的参数不能为空，如果为空，则直接返回
        if ((cNoType == null) || (cNoType.trim().length() <= 0)
                || (cNoLimit == null)) {
            logger.debug("NoType长度错误或者NoLimit为空");

            return null;
        }

        // 默认流水号位数
        int serialLen = 10;
        String tReturn = null;
        cNoType = cNoType.toUpperCase();
        // logger.debug("-----------cNoType:"+cNoType+"
        // cNoLimit:"+cNoLimit);

        int tMaxNo = 0;
//        cNoLimit = tReturn;
//        // logger.debug("cNoLimit:"+cNoLimit);
//        //tongmeng 2009-02-25 modify
//        //增加核保通知书流水号生成，并且使用SEQRENCE。
//        if (cNoType.equals("UWPRTSEQ")) {
//            serialLen = 8;
//            tReturn = cNoLimit;
//        }
//        //tongmeng 2008-03-26 modify
//        //对于DOCID 和PAGEID 使用序列方式


        // 其他
        Connection conn = DBConnPool.getConnection();

        if (conn == null) {
            logger.debug("CreateMaxNo : fail to get db connection");

            return tReturn;
        }
        try {
            // 开始事务
            // 查询结果有3个： -- added by Fanym
            // 全部采用直接执行SQL语句，只要有其他事务锁定了本行，立即返回NULL
            // 如果没有锁定，则本事务锁定，查询得到结果则UPDATE，没有则INSERT
            conn.setAutoCommit(false);

            // String strSQL = "select MaxNo from LDMaxNo where notype='" +
            // cNoType + "' and nolimit='" + cNoLimit +
            // "' for update";
            // //如果数据库类型是ORACLE的话，需要添加nowait属性，以防止锁等待
            // if (SysConst.DBTYPE.compareTo("ORACLE") == 0)
            // {
            // strSQL = strSQL + " nowait";
            // }
            StringBuffer tSBql = new StringBuffer(256);
            tSBql.append("select MaxNo from LDMaxNo where notype='");
            tSBql.append(cNoType);
            tSBql.append("' and nolimit='");

            tSBql.append(cNoLimit);
            tSBql.append("' for update");
            // 如果数据库类型是ORACLE的话，需要添加nowait属性，以防止锁等待
            if (SysConst.DBTYPE.compareTo("ORACLE") == 0) {
                // 去掉nowait的限制
                // tSBql.append(" nowait");
            }

            ExeSQL exeSQL = new ExeSQL(conn);
            String result = null;
            result = exeSQL.getOneValue(tSBql.toString());

            // 测试返回bull时
            if (exeSQL.mErrors.needDealError()) {
                logger.debug("查询LDMaxNo出错，请稍后!");
                conn.rollback();
                conn.close();

                return null;
            }

            if ((result == null) || result.equals("")) {
                // strSQL = "insert into ldmaxno(notype, nolimit, maxno)
                // values('" +
                // cNoType + "', '" + cNoLimit + "', 1)";
                tSBql = new StringBuffer(256);
                tSBql
                        .append("insert into ldmaxno(notype, nolimit, maxno) values('");
                tSBql.append(cNoType);
                tSBql.append("', '");

                tSBql.append(cNoLimit);

                tSBql.append("', 1)");

                exeSQL = new ExeSQL(conn);
                if (!exeSQL.execUpdateSQL(tSBql.toString())) {
                    logger.debug("CreateMaxNo 插入失败，请重试!");
                    conn.rollback();
                    conn.close();

                    return null;
                } else {
                    tMaxNo = 1;
                }
            } else {
                // strSQL = "update ldmaxno set maxno = maxno + 1 where notype =
                // '" + cNoType
                // + "' and nolimit = '" + cNoLimit + "'";
                tSBql = new StringBuffer(256);
                tSBql
                        .append("update ldmaxno set maxno = maxno + 1 where notype = '");
                tSBql.append(cNoType);
                tSBql.append("' and nolimit = '");

                tSBql.append(cNoLimit);
                tSBql.append("'");

                exeSQL = new ExeSQL(conn);
                if (!exeSQL.execUpdateSQL(tSBql.toString())) {
                    logger.debug("CreateMaxNo 更新失败，请重试!");
                    conn.rollback();
                    conn.close();

                    return null;
                } else {
                    tMaxNo = Integer.parseInt(result) + 1;
                }
            }

            conn.commit();
            conn.close();
        } catch (Exception Ex) {
            try {
                conn.rollback();
                conn.close();

                return null;
            } catch (Exception e1) {
                e1.printStackTrace();

                return null;
            }
        }


        //按照5.3的模式修改
        if (tReturn.length() >= 12) {
            tReturn = tReturn.substring(0, 10) + "0" + tReturn.substring(10, 12);
        }


        String tStr = String.valueOf(tMaxNo);
        tStr = PubFun.LCh(tStr, "0", serialLen);
        // logger.debug("tStr:"+tStr+" serialLen:"+serialLen);
        // logger.debug("---------tStr:"+tStr);
        if (cNoType.equals("CONTNO") && tReturn.equals("0")) {
            tReturn = "88";
        }


        if (tReturn.equals("SN")) {
            if (cNoType.toUpperCase().equals("CUSTOMERNO")) {
//				tReturn = "9" + tStr.trim();
                tReturn = tStr.trim();
            }

            //add by renhl 修改为相当与个位加1 2008-10-14
            else if (cNoType.equals("COMMISIONSN")) {
                tReturn = tStr.trim();
            } else {
                tReturn = tStr.trim() + "0";
            }
        } else if (tReturn.equals("RECO")) {
            tReturn = tStr.trim();
        } else {
            tReturn = tReturn.trim() + tStr.trim();
        }
        // logger.debug("------tReturn:"+tReturn);
        return tReturn;
    }

    /**
     * 功能：产生指定长度的流水号，一个号码类型一个流水
     *
     * @param cNoType   String 流水号的类型
     * @param cNoLength int 流水号的长度
     * @return String 返回产生的流水号码
     */
    public String CreateMaxNo(String cNoType, int cNoLength) {
        if ((cNoType == null) || (cNoType.trim().length() <= 0)
                || (cNoLength <= 0)) {
            logger.debug("NoType长度错误或NoLength错误");

            return null;
        }

        cNoType = cNoType.toUpperCase();



        // logger.debug("type:"+cNoType+" length:"+cNoLength);
        Connection conn = DBConnPool.getConnection();

        if (conn == null) {
            logger.debug("CreateMaxNo : fail to get db connection");

            return null;
        }
        String tReturn = "";
        String cNoLimit = "SN";
        // 对上面那种cNoLimit为SN的数据做一个校验，否则会导致数据干扰
        BigInteger tMaxNo = new BigInteger("0");
        tReturn = cNoLimit;

        try {
            // 开始事务
            conn.setAutoCommit(false);

            // String strSQL = "select MaxNo from LDMaxNo where notype='" +
            // cNoType + "' and nolimit='" + cNoLimit +
            // "' for update";
            // //如果数据库类型是ORACLE的话，需要添加nowait属性，以防止锁等待
            // if (SysConst.DBTYPE.compareTo("ORACLE") == 0)
            // {
            // strSQL = strSQL + " nowait";
            // }
            StringBuffer tSBql = new StringBuffer(256);
            tSBql.append("select MaxNo from LDMaxNo where notype='");
            tSBql.append(cNoType);
            tSBql.append("' and nolimit='");
            tSBql.append(cNoLimit);
            tSBql.append("' for update");
            // 如果数据库类型是ORACLE的话，需要添加nowait属性，以防止锁等待
            if (SysConst.DBTYPE.compareTo("ORACLE") == 0) {
                // tSBql.append(" nowait");
            }

            ExeSQL exeSQL = new ExeSQL(conn);
            String result = null;
            result = exeSQL.getOneValue(tSBql.toString());

            if ((result == null) || exeSQL.mErrors.needDealError()) {
                logger.debug("CreateMaxNo 资源忙，请稍后!");
                conn.rollback();
                conn.close();

                return null;
            }

            if (result.equals("")) {
                // strSQL = "insert into ldmaxno(notype, nolimit, maxno)
                // values('" +
                // cNoType + "', '" + cNoLimit + "', 1)";
                tSBql = new StringBuffer(256);
                tSBql
                        .append("insert into ldmaxno(notype, nolimit, maxno) values('");
                tSBql.append(cNoType);
                tSBql.append("', '");
                tSBql.append(cNoLimit);
                tSBql.append("', 1)");

                exeSQL = new ExeSQL(conn);
                if (!exeSQL.execUpdateSQL(tSBql.toString())) {
                    logger.debug("CreateMaxNo 插入失败，请重试!");
                    conn.rollback();
                    conn.close();

                    return null;
                } else {
                    tMaxNo = new BigInteger("1");
                }
            } else {
                // strSQL = "update ldmaxno set maxno = maxno + 1 where notype =
                // '" + cNoType
                // + "' and nolimit = '" + cNoLimit + "'";
                tSBql = new StringBuffer(256);
                tSBql
                        .append("update ldmaxno set maxno = maxno + 1 where notype = '");
                tSBql.append(cNoType);
                tSBql.append("' and nolimit = '");
                tSBql.append(cNoLimit);
                tSBql.append("'");

                exeSQL = new ExeSQL(conn);
                if (!exeSQL.execUpdateSQL(tSBql.toString())) {
                    logger.debug("CreateMaxNo 更新失败，请重试!");
                    conn.rollback();
                    conn.close();

                    return null;
                } else {
                    tMaxNo = (new BigInteger(result)).add(new BigInteger("1"));
                }
            }

            conn.commit();
            conn.close();
        } catch (Exception Ex) {
            try {
                conn.rollback();
                conn.close();

                return null;
            } catch (Exception e1) {
                e1.printStackTrace();

                return null;
            }
        }

        String tStr = tMaxNo.toString();
        tStr = PubFun.LCh(tStr, "0", cNoLength);
        tReturn = tStr.trim();

        return tReturn;
    }

    public static void main(String[] args) {

        CommonSeqStrGen tSNGen = new CommonSeqStrGen();
        tSNGen.setNoType("ContractID1");
        tSNGen.setNoLength(10);
//        tSNGen.setNoLimit("SN");
        for (int i =1; i<=10 ;i ++) {
            System.out.println("生成流水号为：" + tSNGen.generate());
        }
    }
}
