package com.sinosoft.sn.generator.impl;

import com.sinosoft.sn.generator.GeneratorTypeSet;
import com.sinosoft.sn.generator.IGenerator;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

/**
 * 日期串序列生成器
 * Created by filon51 on 10/22/14.
 */
@Service
public class DateTimeGenerator implements IGenerator {
    private static final String type = GeneratorTypeSet.DATE_TIME;

    @Override
    public String getGeneratorType() {
        return type;
    }

    @Override
    public String generate(String formatStr, Map paraMap) {
        SimpleDateFormat sdf = new SimpleDateFormat();
        sdf.applyPattern(formatStr);
        return sdf.format(new Date());
    }
}
