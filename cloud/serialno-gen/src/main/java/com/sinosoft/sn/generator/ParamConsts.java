package com.sinosoft.sn.generator;

/**
 * 参数常量
 * Created by filon51 on 10/22/14.
 */
public class ParamConsts {
    /** 子序列间分隔符 */
    public static final String SPLIT_SUB_GENERATOR = "#";
    /** 子序列内部分隔符 */
    public static final String SPLIT_SUB_GENERATOR_INNER = "@";

    /** 为需要生成号码的类型 */
    public static final String SERIAL_NO_TYPE = "serial_no_type";
    /** 为需要生成号码的限制条件（要么是SN，要么是日期，要么是机构编码） */
    public static final String SERIAL_NO_LIMIT = "serial_no_limit";
    /**
     * 对于数字序列，需要保持的数字位数，比如00000001；00012345 ……
     * 例子：
     * 0C0:不补
     * 8C0:用0补足8位
     */
    public static final String SPLIT_C = "C";
}
