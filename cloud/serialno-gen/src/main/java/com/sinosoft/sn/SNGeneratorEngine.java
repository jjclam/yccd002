package com.sinosoft.sn;

import com.sinosoft.sn.generator.IGenerator;
import com.sinosoft.sn.generator.ParamConsts;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

/**
 * 流水号生成引擎
 */
@Service
public  final class SNGeneratorEngine {
    private static SNGeneratorEngine snGeneratorEngine = new SNGeneratorEngine();
    private static Map<String, IGenerator> generatorMap = new HashMap<String, IGenerator>();

    /**
     * 流水号生成规则
     * 典型的例子，比如：
     * FixedString@ 国办发〔#DateTime@yyyy#Str@〕#NumberSequence@0C0#FixedString@ 号
     * FixedString@ 沪 ICP 备 #NumberSequence@8C0#FixedString@ 号
     */
//    private String formatStr;

    /**
     * 子序列间分隔符
     */
    private String splitChar = ParamConsts.SPLIT_SUB_GENERATOR;

    /**
     * 子序列内部分隔符
     */
    private String innerSplitChar = ParamConsts.SPLIT_SUB_GENERATOR_INNER;

    /**
     * 流水号子序列生成规则
     */
//    private String[] subFormatStr;


    /**
     * 按照类型和 Generator 实例存放
     *
     * @param generator
     */
    public void addGenerator(IGenerator generator) {
        generatorMap.put(generator.getGeneratorType(), generator);
    }

    private SNGeneratorEngine() {
    }

    public static SNGeneratorEngine getInstance() {
        return snGeneratorEngine;
    }

    /**
     * 接收流水号格式字符串，并分割成子序列
     *
     * @param formatStr
     */
//    public void setFormatStr(String formatStr) {
//        this.formatStr = formatStr;
//        subFormatStr = this.formatStr.split(splitChar);
//    }


//    public String generate(Map parameterMap) {
//        StringBuffer serNumber = new StringBuffer();
//        for (String format : subFormatStr) {
//            serNumber.append(generateSubSN(format, parameterMap));
//        }
//        return serNumber.toString();
//    }

    /**
     * 生成流水号：分发给各个子序列 Generator 生成
     * @param formatStr formatStr
     * @param parameterMap parameterMap
     * @return 返回
     */
    public String generate(String formatStr, Map parameterMap) {
        String[] subFormatStr = formatStr.split(splitChar);
        StringBuffer serNumber = new StringBuffer();
        for (String format : subFormatStr) {
            serNumber.append(generateSubSN(format, parameterMap));
        }
        return serNumber.toString();
    }

    /**
     * 根据类型调用子序列 Generator 生成
     *
     * @param subFormatStr
     * @param parameterMap
     * @return
     */
    private String generateSubSN(String subFormatStr, Map parameterMap) {
        String[] innerSubStr = subFormatStr.split(innerSplitChar);
        IGenerator generator = this.getGenerator(innerSubStr[0]);
        return generator.generate(innerSubStr[1], parameterMap);
    }

    /**
     * 根据 GeneratorType 获取 Generator 实例
     */
    public IGenerator getGenerator(String generatorType) {
        return generatorMap.get(generatorType);
    }
}
