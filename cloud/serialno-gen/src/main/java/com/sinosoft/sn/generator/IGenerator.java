package com.sinosoft.sn.generator;

import java.util.Map;

/**
 * 子序列生成器接口
 */
public interface IGenerator {

    /**
     * 获取子序列类型
     *
     * @return 序列类型
     */
    String getGeneratorType();

    /**
     * 生成子序列串
     *
     * @param formatStr 序列格式
     * @param paraMap   参数map
     * @return 返回值
     */
    String generate(String formatStr, Map paraMap);
}
