package com.sinosoft.sn.generator.impl;

import com.sinosoft.sn.generator.GeneratorTypeSet;
import com.sinosoft.sn.generator.IGenerator;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * 固定字符串序列生成器
 * Created by filon51 on 10/22/14.
 */
@Service
public class FixedStringGenerator implements IGenerator {
    private static final String type = GeneratorTypeSet.FIXED_STRING;

    @Override
    public String getGeneratorType() {
        return type;
    }

    @Override
    public String generate(String formatStr, Map paraMap) {
        return formatStr;
    }
}
