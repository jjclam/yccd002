package com.sinosoft.sn.maxno;

import org.springframework.stereotype.Service;

import java.io.Serializable;

/**
 * 流水号生成引擎，用于缓存的号段
 * Created by filon51 on 2016/8/1.
 */
@Service
public class MaxNoCache implements Serializable {
    private String NoType;
    private String NoLimit;
    private int MaxNo;
    private int CurNo;
    private int NextNo;
    private String CachedKey;
    private String CachedKey_CurNo;

    /**
     * 号码类型
     */
    public String getNoType() {
        return NoType;
    }

    public void setNoType(String noType) {
        NoType = noType;
    }

    /**
     * 号码限制条件
     */
    public String getNoLimit() {
        return NoLimit;
    }

    public void setNoLimit(String noLimit) {
        NoLimit = noLimit;
    }

    /**
     * 当前最大值
     */
    public int getMaxNo() {
        return MaxNo;
    }

    public void setMaxNo(int maxNo) {
        MaxNo = maxNo;
    }

    /**
     * 当前值
     */
    public int getCurNo() {
        return CurNo;
    }

    public void setCurNo(int curNo) {
        CurNo = curNo;
    }

    /**
     * 下一个值
     */
    public int getNextNo() {
        return NextNo;
    }

    public void setNextNo(int nextNo) {
        NextNo = nextNo;
    }

    /**
     * 缓存KEY
     */
    public String getCachedKey() {
        CachedKey = "MaxNo_" + NoType + "@" + NoLimit;
        return CachedKey;
    }

    private void setCachedKey(String cachedKey) {
        CachedKey = cachedKey;
    }

    @Override
    public String toString() {
        return "MaxNoCache{" +
                "NoType='" + NoType + '\'' +
                ", NoLimit='" + NoLimit + '\'' +
                ", MaxNo=" + MaxNo +
                ", CurNo=" + CurNo +
                ", NextNo=" + NextNo +
                ", CachedKey='" + CachedKey + '\'' +
                ", CachedKey_CurNo='" + CachedKey_CurNo + '\'' +
                '}';
    }

    /**
     * 缓存当前号码的KEY
     */
    public String getCachedKey_CurNo() {
        CachedKey_CurNo = "CurNo_" + NoType + "@" + NoLimit;
        return CachedKey_CurNo;
    }

    public void setCachedKey_CurNo(String cachedKey_CurNo) {
        CachedKey_CurNo = cachedKey_CurNo;
    }
}
