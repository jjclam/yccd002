package com.sinosoft.sn.generator.impl;

import com.sinosoft.sn.maxno.GenMaxNo;
import com.sinosoft.sn.generator.GeneratorTypeSet;
import com.sinosoft.sn.generator.IGenerator;
import com.sinosoft.sn.generator.ParamConsts;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * 自增主键的生成器
 * Created by filon51 on 2016/7/27.
 */
@Service
public class IDKeyGenerator implements IGenerator {
    private static final String type = GeneratorTypeSet.ID_KEY;

    @Override
    public String getGeneratorType() {
        return type;
    }

    @Override
    public String generate(String formatStr, Map paraMap) {
        // 1 从MAP中解析获取NOTYPE NOLIMIT
        String sNoType = (String) paraMap.get(ParamConsts.SERIAL_NO_TYPE);
        String sNoLimit = (String) paraMap.get(ParamConsts.SERIAL_NO_LIMIT);

        if (sNoType == null || sNoType.equals("")) {
            return null;
        }
        // 2 得到当前ID
        int keyID = GenMaxNo.getMaxNo(sNoType, sNoLimit); //调用common中的方法实现maxNo自增并获取出来
        if (keyID == -1) {
            return null;
        }
        return String.valueOf(keyID);
    }
}
