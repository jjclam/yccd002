package com.sinosoft.sn.generator.impl;

import com.sinosoft.sn.maxno.GenMaxNo;
import com.sinosoft.sn.generator.GeneratorTypeSet;
import com.sinosoft.sn.generator.IGenerator;
import com.sinosoft.sn.generator.ParamConsts;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * 流水号（左填充 默认串）生成器
 * Created by filon51 on 10/22/14.
 */
@Service
public class NumberSequenceGenerator implements IGenerator {
    private static final String type = GeneratorTypeSet.NUMBER_SEQUENCE;
    private String splitC = ParamConsts.SPLIT_C;
    private Log logger = LogFactory.getLog(NumberSequenceGenerator.class);

    @Override
    public String getGeneratorType() {
        return type;
    }


    /**
     * @param formatStr 0C0
     * @param paraMap   存放自增流水{KEY:VALUE},比如：
     *                  {serial_no_type: reportdata}
     *                  {serial_no_limit：SN}
     * @return
     */
    @Override
    public String generate(String formatStr, Map paraMap) {
        String sNoType = (String) paraMap.get(ParamConsts.SERIAL_NO_TYPE);
        String sNoLimit = (String) paraMap.get(ParamConsts.SERIAL_NO_LIMIT);
        int seqNum = GenMaxNo.getMaxNo(sNoType, sNoLimit); //调用common中的方法实现maxNo自增并获取出来
        logger.info("sNoType:" + sNoType + ",sNoLimit:" + sNoLimit + " 生成seqNum为：" + seqNum);

        if (seqNum == -1) {
            return null;
        }

        String[] charArray = formatStr.split(splitC);
        int seqNumLength = Integer.parseInt(charArray[0]);
        char prefixChar = charArray[1].charAt(0);
        if (seqNumLength == 0) {
            return String.valueOf(seqNum);
        } else {
            return appendPrefixChar(seqNum, seqNumLength, prefixChar);
        }
    }

    /**
     * 补足前缀以保证序列定长，如 0001, 保持 4 位，不足 4 位用'0'补齐
     *
     * @param seqNum       当前数值
     * @param seqNumLength 需要返回的字符串长度
     * @param prefixChar   用于补齐的前置字符串
     * @return
     */
    private String appendPrefixChar(int seqNum, int seqNumLength, char prefixChar) {
        String seqNumStr = String.valueOf(seqNum);
        for (int i = seqNumStr.length(); i < seqNumLength; i++) {
            seqNumStr = prefixChar + seqNumStr;
        }
        return seqNumStr;
    }
}
