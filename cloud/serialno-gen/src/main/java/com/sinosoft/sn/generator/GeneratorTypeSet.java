package com.sinosoft.sn.generator;

/**
 * 序列生成器分类代码集
 * <p/>
 * Created by filon51 on 10/22/14.
 */
public class GeneratorTypeSet {
    /** 固定字符串序列生成器 */
    public static final String FIXED_STRING = "FixedString";
    /** 日期串序列生成器 */
    public static final String DATE_TIME = "DateTime";
    /** 数字串序列生成器 */
    public static final String NUMBER_SEQUENCE = "NumberSequence";
    /** ID 自增键生成器 */
    public static final String ID_KEY = "IDKey";
}
