package com.sinosoft.sn.maxno;

import com.sinosoft.cloud.cache.RedisService;
import com.sinosoft.cloud.common.SpringContextUtils;
import com.sinosoft.utility.*;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import javax.sql.DataSource;
import java.sql.Connection;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Random;

/**
 * 获取当前最大号
 * Created by filon51 on 2016/7/27.
 */
@Service
public class GenMaxNo {
    static Log logger = LogFactory.getLog(GenMaxNo.class);

    /**
     * 是否使用缓存标记
     */
    private static final boolean USE_CACHE_FLAG = true;
    /**
     * 本地缓存
     */
    private static final Hashtable<String, MaxNoCache> MAX_NO_LOCAL_CACHE = new Hashtable();
    /**
     * 申请号段的步长
     */
    private static final int MAX_NO_STEP_TO_RDMS = 100; //健康险改动。


    private static final Object LOCK = new Object();

    /**
     * 获取最大号(要求使用内存数据库，批次获取号段以提高流水号的生成效率)
     *
     * @param cNoType  为需要生成号码的类型
     * @param cNoLimit 为需要生成号码的限制条件（要么是SN，要么是日期，要么是机构编码）
     * @return
     */
    public static synchronized int getMaxNo(String cNoType, String cNoLimit) {
        int iCurNo = -1;

        if (cNoType == null || cNoType.trim().equals("")) {
            return iCurNo;
        }
        if (cNoLimit == null || cNoLimit.trim().equals("")) { //不传，默认是SN
            cNoLimit = "SN";
        }

        /**
         * @author: yangming
         * 要使用两级缓存来解决高并发生成号问题。
         * 首先险查询本地缓存，如果本地缓存存在，自增返回。
         * 如果本地缓存不存在，从数据库中获取，并存入本地缓存。
         */
        if (USE_CACHE_FLAG) {
            MaxNoCache tMaxNoCache = new MaxNoCache();
            tMaxNoCache.setNoType(cNoType);
            tMaxNoCache.setNoLimit(cNoLimit);

            Integer maxNo = getMaxNoFromLocalCache(tMaxNoCache);

            if (maxNo == null) {
                maxNo = applyNOFromRDB(cNoType, cNoLimit, MAX_NO_STEP_TO_RDMS);
                if (maxNo == -1 || maxNo == -2) {
                    return maxNo;
                }
                iCurNo = maxNo - MAX_NO_STEP_TO_RDMS;
                if (iCurNo < 0) {
                    iCurNo = 0;
                } else if (iCurNo == 0) {
                    iCurNo = 1;
                }
                synchronized (LOCK) {
                    tMaxNoCache.setMaxNo(maxNo);
                    tMaxNoCache.setCurNo(iCurNo);
                    tMaxNoCache.setNextNo(iCurNo + 1);
                    MAX_NO_LOCAL_CACHE.put(tMaxNoCache.getCachedKey(), tMaxNoCache);
                }
                return iCurNo;
            }
            return maxNo;
        } else { //不使用缓存
            int applyMaxNo = applyNOFromRDB(cNoType, cNoLimit, 1);
            if (applyMaxNo == -1) {
                return -1;
            }
            return applyMaxNo;
        }
    }

    /**
     * 本地缓存不存在，就去Redis上申请
     * <p>
     * 暂时废弃
     *
     * @param tMaxNoCache
     * @return
     */
    @Deprecated
    private static Integer getMaxNoFromRedisCache(MaxNoCache tMaxNoCache) {
        Integer iCurNo = null;
        RedisTemplate redisTemplateTrans = (RedisTemplate) SpringContextUtils.getBeanById("redisTemplateTrans");
        RedisService<String, MaxNoCache> tRedisService = new RedisService(redisTemplateTrans);
        String theCachedKey = tMaxNoCache.getCachedKey();
        MaxNoCache theCache = tRedisService.get(theCachedKey);
        if (theCache != null) { //缓存中存在
            if (theCache.getCurNo() < theCache.getMaxNo()) { //当前号小于最大号
                HashMap<String, MaxNoCache> kvMap = new HashMap<String, MaxNoCache>();
                kvMap.put(theCachedKey, theCache);
                tRedisService.watch(kvMap);
                tRedisService.beginTx();
                //当前号累加
                /**
                 * @author：yangming
                 * @date:2016-11-11
                 * 此处逻辑修改为去Redis中按步长申请，避免Redis中并发过大。
                 */
                iCurNo = theCache.getCurNo() + 1;
                theCache.setCurNo(iCurNo + 1);
                theCache.setNextNo(iCurNo + 1);
                tRedisService.batchSet(kvMap);
                /**
                 * 重试三次
                 */
                int redo = 3;
                while (redo-- > 0) {
                    if (tRedisService.endTx() == null) {
                        logger.error("流水号更新失败！");
                        Random r = new Random(System.nanoTime());
                        try {
                            int timeout = r.nextInt(100);
                            logger.info("暂停" + timeout + "ms后再重试Redis");
                            Thread.sleep(timeout);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    } else {
                        break;
                    }
                }
            } else { //当前号等于最大号
                //1 从数据库中申请一个号段
                int applyMaxNo = applyNOFromRDB(tMaxNoCache.getNoType(), tMaxNoCache.getNoLimit(), MAX_NO_STEP_TO_RDMS);
                if (applyMaxNo == -1) {
                    return null;
                }
                //2 更新缓存
                HashMap<String, MaxNoCache> kvMap = new HashMap<String, MaxNoCache>();
                kvMap.put(theCachedKey, theCache);
                tRedisService.watch(kvMap);
                tRedisService.beginTx();
                theCache.setMaxNo(applyMaxNo);
                //当前号累加
                iCurNo = theCache.getCurNo() + 1;
                theCache.setCurNo(iCurNo);
                theCache.setNextNo(iCurNo + 1);
                tRedisService.batchSet(kvMap);
                if (tRedisService.endTx() == null) {
                    logger.error("流水号更新失败！");
                    return null;
                }
            }
        } else { //缓存中不存在
            //1 查找数据库当前最大号
//                iCurNo = getCurNoFromRDB(cNoType, cNoLimit);
//                if (iCurNo == -1) {
//                    return -1;
//                }

            //2 从数据库中申请一个号段
            int applyMaxNo = applyNOFromRDB(tMaxNoCache.getNoType(), tMaxNoCache.getNoLimit(), MAX_NO_STEP_TO_RDMS);
            if (applyMaxNo == -1) {
                return null;
            }
            iCurNo = applyMaxNo - MAX_NO_STEP_TO_RDMS;
            if (iCurNo < 0) {
                iCurNo = 0;
            }
            //3 初始化缓存
            HashMap<String, MaxNoCache> kvMap = new HashMap<String, MaxNoCache>();
            kvMap.put(theCachedKey, tMaxNoCache);
            tRedisService.watch(kvMap);
            tRedisService.beginTx();
            tMaxNoCache.setMaxNo(applyMaxNo);
            //当前号累加
            iCurNo = iCurNo + 1;
            tMaxNoCache.setCurNo(iCurNo);
            tMaxNoCache.setNextNo(iCurNo + 1);
            tRedisService.batchSet(kvMap);
            if (tRedisService.endTx() == null) {
                logger.error("流水号更新失败！");
                return null;
            }
        }
        return iCurNo;
    }

    /**
     * 先获取本地缓存
     *
     * @param tMaxNoCache
     * @return
     */
    private static Integer getMaxNoFromLocalCache(MaxNoCache tMaxNoCache) {
        MaxNoCache localCache = MAX_NO_LOCAL_CACHE.get(tMaxNoCache.getCachedKey());
        Integer maxNo = null;
        if (localCache != null) {
            synchronized (localCache) {
                if (localCache.getNextNo() >= localCache.getMaxNo()) {
                    return null;
                }
                maxNo = localCache.getCurNo() + 1;
                localCache.setCurNo(maxNo);
                localCache.setNextNo(maxNo + 1);
                MAX_NO_LOCAL_CACHE.remove(localCache.getCachedKey());
                MAX_NO_LOCAL_CACHE.put(localCache.getCachedKey(), localCache);
            }
        }

        return maxNo;
    }

    /**
     * 从关系数据库中获取当前号码，如果数据库中不存在该号码，需要插入一条数据。
     *
     * @param cNoType
     * @param cNoLimit
     * @return
     */
    private static int getCurNoFromRDB(String cNoType, String cNoLimit) {
        int tMaxNo = 0;
        // 其他
        Connection conn = DBConnPool.getConnection();
        if (conn == null) {
            logger.debug("getCurNoFromRDB : fail to get db connection");
            return -1;
        }
        try {
            // 开始事务
            conn.setAutoCommit(false);
            StringBuffer tSBql = new StringBuffer(256);
            SpringPropertyBean springPropertyBean = SpringContextUtils.getBeanByClass(SpringPropertyBean.class);
            String jdbcType = springPropertyBean.getProperty("jdbc.driverClassName");
            // 如果数据库类型是ORACLE
            if (jdbcType.indexOf("oracle") > 0) {
                tSBql.append("select MaxNo from LDMaxNo where notype='");
                tSBql.append(cNoType);
                tSBql.append("' and nolimit='");
                tSBql.append(cNoLimit);
                tSBql.append("' for update");
            } else if (jdbcType.indexOf("mysql") > 0) {
                logger.debug("MySQL数据库!");
                tSBql.append("select MaxNo from LDMaxNo where notype='");
                tSBql.append(cNoType);
                tSBql.append("' and nolimit='");
                tSBql.append(cNoLimit);
                tSBql.append("' LOCK IN SHARE MODE");
            }

            ExeSQL exeSQL = new ExeSQL(conn);
            String result = null;
            result = exeSQL.getOneValue(tSBql.toString());
            // 测试返回bull时
            if (exeSQL.mErrors.needDealError()) {
                logger.debug("查询LDMaxNo出错，请稍后!");
                conn.rollback();
                conn.close();
                return -1;
            }
            if ((result == null) || result.equals("")) {
                tSBql = new StringBuffer(256);
                tSBql.append("insert into ldmaxno(notype, nolimit, maxno) values('");
                tSBql.append(cNoType);
                tSBql.append("', '");
                tSBql.append(cNoLimit);
                tSBql.append("', 0)");
                exeSQL = new ExeSQL(conn);
                if (!exeSQL.execUpdateSQL(tSBql.toString())) {
                    logger.debug("getCurNoFromRDB 插入失败，请重试!");
                    conn.rollback();
                    conn.close();
                    return -1;
                } else {
                    tMaxNo = 0;
                }
            } else {
                tMaxNo = Integer.parseInt(result);
            }
            conn.commit();
            conn.close();
        } catch (Exception ex) {
            try {
                conn.rollback();
                conn.close();
                return -1;
            } catch (Exception e1) {
                e1.printStackTrace();
                return -1;
            }
        } finally {
            try {
                conn.close();
            } catch (Exception e1) {
            }
        }
        return tMaxNo;
    }

    /**
     * 从关系数据库中申请某一个号段的号码。
     *
     * @param cNoType
     * @param cNoLimit
     * @return
     */
    private static int applyNOFromRDB(String cNoType, String cNoLimit, int iStep) {
        int tMaxNo = 0;
//        DataSourceManager dataSourceManager = SpringContextUtils.getBeanByClass(DataSourceManager.class);
//        DataSource dataSource = dataSourceManager.getCurrentThreadDataSource(); //获取当前
//        dataSourceManager.setCurrentThreadDataSource(0);    //设置获取最大号应用取第一个数据源

        // 其他
        Connection conn = DBConnPool.getConnection();
        if (conn == null) {
            logger.debug("applyNOFromRDB : fail to get db connection");
            return -1;
        }
        try {
            // 开始事务
            conn.setAutoCommit(false);
            StringBuffer tSBql = new StringBuffer(256);
            // 如果数据库类型是ORACLE
            if (SysConst.DBTYPE.compareTo("ORACLE") == 0) {
                tSBql.append("select MaxNo,UPLIMITEMAXNO from LDMaxNo where notype='");
                tSBql.append(cNoType);
                tSBql.append("' and nolimit='");
                tSBql.append(cNoLimit);
                tSBql.append("' for update");
            } else if (SysConst.DBTYPE.compareTo("MYSQL") == 0) {
                logger.debug("MySQL数据库!");
                tSBql.append("select MaxNo,UPLIMITEMAXNO from LDMaxNo where notype='");
                tSBql.append(cNoType);
                tSBql.append("' and nolimit='");
                tSBql.append(cNoLimit);
                tSBql.append("' for update");
            }

            ExeSQL exeSQL = new ExeSQL(conn);
            SSRS result = null;
            result = exeSQL.execSQL(tSBql.toString());
            // 测试返回bull时
            if (exeSQL.mErrors.needDealError()) {
                logger.debug("查询LDMaxNo出错，请稍后!");
                conn.rollback();
                conn.close();
                return -1;
            }
            if ((result == null) || result.getMaxRow() == 0) {
                tSBql = new StringBuffer(256);
                tSBql.append("insert into ldmaxno(notype, nolimit, maxno) values('");
                tSBql.append(cNoType);
                tSBql.append("', '");
                tSBql.append(cNoLimit);
                if (iStep == 1) { //不使用缓存，则从1开始
                    tSBql.append("', 1)");
                } else { //使用缓存，则从0开始
                    tSBql.append("', " + iStep + ")");
                }
                exeSQL = new ExeSQL(conn);
                if (!exeSQL.execUpdateSQL(tSBql.toString())) {
                    logger.debug("applyNOFromRDB 插入失败，请重试!");
                    conn.rollback();
                    conn.close();
                    return -1;
                } else {
                    tMaxNo = 1 + iStep;
                    conn.commit();
                }
            } else {
                String maxNo = result.GetText(1, 1);
                String upLimitMaxNo = result.GetText(1, 2);
                if (!"0".equals(upLimitMaxNo) && (Integer.parseInt(upLimitMaxNo) - Integer.parseInt(maxNo) < iStep)) {
                    logger.error("cNoType=" + cNoType + "  cNoLimit=" + cNoLimit + "对应的号段不足，请检查！");
                    conn.rollback();
                    conn.close();
                    return -2;
                } else {
                    String strSQL = "update ldmaxno set maxno = maxno + " + iStep
                            + " where notype = '" + cNoType + "' and nolimit = '"
                            + cNoLimit + "'";
                    exeSQL = new ExeSQL(conn);

                    if (!exeSQL.execUpdateSQL(strSQL)) {
                        logger.debug("applyNOFromRDB 更新失败，请重试!");
                        conn.rollback();
                        conn.close();
                        return -1;
                    } else {
                        tMaxNo = Integer.parseInt(maxNo) + iStep + 1;
                    }
                }
                conn.commit();
                conn.close();
            }

        } catch (Exception ex) {
            try {
                conn.rollback();
                conn.close();
                return -1;
            } catch (Exception e1) {
                e1.printStackTrace();
                return -1;
            }
        } finally {
            try {
//                dataSourceManager.setCurrentThreadDataSource(dataSource);
            } catch (Exception e2) {
            }
            try {
                conn.close();
            } catch (Exception e1) {
            }
        }
        return tMaxNo;
    }
}
