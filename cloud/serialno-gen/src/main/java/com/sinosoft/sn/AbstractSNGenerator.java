package com.sinosoft.sn;

import com.sinosoft.sn.generator.GeneratorTypeSet;
import com.sinosoft.sn.generator.IGenerator;
import com.sinosoft.sn.generator.impl.DateTimeGenerator;
import com.sinosoft.sn.generator.impl.FixedStringGenerator;
import com.sinosoft.sn.generator.impl.IDKeyGenerator;
import com.sinosoft.sn.generator.impl.NumberSequenceGenerator;

import java.util.Map;

/**
 * Created by filon51 on 10/22/14.
 */
public abstract class AbstractSNGenerator {
    private static SNGeneratorEngine snGenEngine = SNGeneratorEngine.getInstance();

//    @Autowired
//    private NumberSequenceGenerator numSeqGenerator;

    /**
     * 设置内置的生成器
     */
    private void addBuildInGenerator() {
        if ((snGenEngine.getGenerator(GeneratorTypeSet.DATE_TIME)) == null) {
            snGenEngine.addGenerator(new DateTimeGenerator());
        }

        if (snGenEngine.getGenerator(GeneratorTypeSet.NUMBER_SEQUENCE) == null) {
            snGenEngine.addGenerator(new NumberSequenceGenerator());
//            snGenEngine.addGenerator(numSeqGenerator);
        }

        if (snGenEngine.getGenerator(GeneratorTypeSet.FIXED_STRING) == null) {
            snGenEngine.addGenerator(new FixedStringGenerator());
        }

        if (snGenEngine.getGenerator(GeneratorTypeSet.ID_KEY) == null) {
            snGenEngine.addGenerator(new IDKeyGenerator());
        }
    }

    /**
     * 添加 Generator，提供扩展功能
     *
     * @param generator
     */
    private void addGenerator(IGenerator generator) {
        snGenEngine.addGenerator(generator);
    }

    /**
     * 生成序列号
     *
     * @param snFormatStr  流水号格式字符串
     * @param parameterMap 参数列表
     * @return
     */
    private String generateSN(String snFormatStr, Map parameterMap) {
        addBuildInGenerator();
//        snGenEngine.setFormatStr(snFormatStr);
//        return snGenEngine.generate(parameterMap);
        return snGenEngine.generate(snFormatStr, parameterMap);
    }

    /**
     * 生成序列号
     *
     * @return
     */
    public String generate() {
        Map parameterMap = prepareMap();
        String sFormatStr = prepareFormatStr();
        return generateSN(sFormatStr, parameterMap);
    }

    /**
     * 准备流水号生成需要的参数MAP
     *
     * @return
     */
    protected abstract Map prepareMap();

    /**
     * 准备流水号生成规则
     *
     * @return
     */
    protected abstract String prepareFormatStr();
}
