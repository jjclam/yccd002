package com.sinosoft.cloud.dataroute;


import com.sinosoft.cloud.common.SpringContextUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Created by malong on 2016-09-23.
 */
public abstract class ADataSetSource {
    private final Log logger = LogFactory.getLog(getClass());

    @Autowired
    protected IHashShardService iHashShardService;

    /**
     * 设置sharingkey
     * @param shardKey sharkey
     */
    public void setShardKey(String shardKey) {
        if (shardKey == null || ("").equals(shardKey)) {
            logger.error("传入分库主键为NULL或者是空串！");
            return;
        }
        logger.info("分库--->begin");

        if (iHashShardService == null) {
            logger.info("iHashShardService is null");
            iHashShardService = SpringContextUtils.getBeanByClass(HashShardServiceImpl.class);
        }
        logger.info("注入IHashShardService：" + (iHashShardService == null));
        logger.info("分库主键：" + shardKey);
        //分库操作
        if (!iHashShardService.setCurrentDataSource("PERCONTNO", shardKey)) {
            logger.error("分库主键：" + shardKey + "在分库获取数据库失败.....");
        }

    }

}
