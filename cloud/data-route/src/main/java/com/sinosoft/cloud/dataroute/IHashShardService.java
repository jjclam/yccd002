package com.sinosoft.cloud.dataroute;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

/**
 * 数据路由处理方案 接口
 * Created by kaine on 2016/7/13.
 */
@WebService
public interface IHashShardService {
    /**
     * 判断是否需要分库操作
     *
     * @return 是否需要分库
     */
    boolean needSharding();

    /**
     * 设置使用的数据源
     *
     * @param shardMark 系统隔离字符
     * @param shardCode 保单合同号
     * @return 设置数据源结果：true：成功；false：失败
     */
    boolean setCurrentDataSource(String shardMark, String shardCode);

    /**
     * 增加一个新的数据源供使用
     *
     * @param domain   域名，包括端口号
     * @param dbName   数据库名称
     * @param userName 用户名
     * @param password 密码
     * @return 添加数据源
     */
    @WebMethod
    boolean addDataSource(@WebParam String domain, @WebParam String dbName, @WebParam String userName, @WebParam String password);

    /**
     * 停用指定数据源 不接收新业务
     *
     * @param dataSourceName 数据源名称
     * @return 关闭数据源
     */
    @WebMethod
    boolean disableDataSource(@WebParam String dataSourceName);

    /**
     * 重新装载受管理的数据源列表
     *
     * @return 重新装载数据源
     */
    @WebMethod
    boolean reloadDataSource();

    /**
     * 装载分库hash环
     *
     * @return 获取hash
     */
    @WebMethod
    boolean reloadHashRing();

    /**
     * 刷新指定应用的数据源列表
     *
     * @param ltApplicationWSInfo 需要刷新数据源的应用程序信息
     * @return 刷新数据源列表
     */
    @WebMethod
    boolean reloadAllDataSource(@WebParam List ltApplicationWSInfo);

    /**
     * 刷新指定应用的hash环信息
     *
     * @param ltApplicationWSInfo 需要刷新数据源的应用程序信息
     * @return 指定hash
     */
    @WebMethod
    boolean reloadAllHashRing(@WebParam List ltApplicationWSInfo);
}