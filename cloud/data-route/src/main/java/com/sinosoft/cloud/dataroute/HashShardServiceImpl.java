package com.sinosoft.cloud.dataroute;

import com.sinosoft.cloud.cache.RedisService;
import com.sinosoft.utility.DataSourceManager;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.SortedMap;
import java.util.TreeMap;


/**
 * 数据路由处理方案 实现类
 * Created by kaine on 2016/7/13.
 */
@Service
public class HashShardServiceImpl implements IHashShardService {
    private final Log logger = LogFactory.getLog(getClass());

    // 形成环的节点数据
    private static TreeMap<Long, String> nodes = new TreeMap<>();
    // 每个真实节点关联的虚拟节点个数
    private static int NODE_NUM = 100;
    @Autowired(required = false)
    private RedisService redisService;
    // 为初始化数据源节点，使用set函数注入
    @Autowired(required = false)
    private DataSourceManager dataSourceManager;

    /**
     * 设置datasource
     * @param dataSourceManager 数据库连接
     */
    public void setDataSourceManager(DataSourceManager dataSourceManager) {
        this.dataSourceManager = dataSourceManager;
        reloadHashRing();
    }

    /**
     * 判断是否需要分库操作
     *
     * @return
     */
    @Override
    public boolean needSharding() {
        return false;
    }

    /**
     * 设置使用的数据源
     *
     * @param shardMark 系统隔离字符
     * @param shardCode 保单合同号
     * @return 设置数据源结果：true：成功；false：失败
     */
    @Override
    public boolean setCurrentDataSource(String shardMark, String shardCode) {


        if (shardCode == null || "".equals(shardCode)) {
            logger.error("传入分库主键为NULL或者是空串！");
            return false;
        }

        logger.info("分库主键值：" + shardCode);
        // 查询落库数据信息
        String dataSourceName = locateDBInfo(shardMark, shardCode);
        // 查询到数据落库地址
        if (dataSourceName != null) {
            logger.info("查询到落库信息,已落库: " + dataSourceName);
            dataSourceManager.setCurrentThreadDataSource(dataSourceName);

        } else {
            // 未查询到数据落库地址，使用Hash环算法路由出落库地址
            dataSourceName = routeDBInfo(shardMark, shardCode, nodes);
            if (dataSourceName != null) {
                logger.info("未查询到落库信息，路由落库，数据源名称：" + dataSourceName);
                dataSourceManager.setCurrentThreadDataSource(dataSourceName);

            } else {
                logger.error("指定数据源失败，请重试！");

                return false;
            }
        }
        return true;
    }

    /**
     * 增加一个新的数据源供使用
     *
     * @param domain   域名，包括端口号
     * @param dbName   数据库名称
     * @param userName 用户名
     * @param password 密码
     * @return
     */
    @Override
    public boolean addDataSource(String domain, String dbName, String userName, String password) {
//        LDDataSourceInfoDB ldDataSourceInfoDB = new LDDataSourceInfoDB();
//        ldDataSourceInfoDB.setIDNO(new DataSourceSNGenerator().generate());
//        ldDataSourceInfoDB.setIPDesc(domain);
//        ldDataSourceInfoDB.setDBName(dbName);
//        ldDataSourceInfoDB.setUserName(userName);
//        ldDataSourceInfoDB.setPass(password);
//        ldDataSourceInfoDB.setBackUp1("?useUnicode=true&characterEncoding=utf8");
//        ldDataSourceInfoDB.setBackUp2("1");
//        return dataSourceManager.addDataSource(ldDataSourceInfoDB);
        return false;

    }

    /**
     * 停用指定数据源 不接收新业务
     *
     * @param dataSourceName 数据源名称
     * @return
     */
    @Override
    public boolean disableDataSource(String dataSourceName) {
//        return dataSourceManager.disableDataSource(dataSourceName);
        return false;
    }

    /**
     * 重新装载受管理的数据源列表
     *
     * @return
     */
    @Override
    public boolean reloadDataSource() {
        // dataSourceManager受管理的数据源列表
//        return dataSourceManager.loadEnableDataSource();
        return false;
    }

    /**
     * 装载分库hash环
     *
     * @return
     */
    @Override
    public boolean reloadHashRing() {


//        LDDataSourceInfoSet ldDataSourceInfoSet = dataSourceManager.queryHashingUsedDataSource();
//        // 每个数据源节点都需要关联虚拟节点
//        TreeMap<Long, String> tempNodes = new TreeMap<>();
//        if (ldDataSourceInfoSet != null && ldDataSourceInfoSet.size() != 0) {
//            for (int i = 1; i <= ldDataSourceInfoSet.size(); i++) {
//                String dataSourceName = ldDataSourceInfoSet.get(i).getIDNO();
//                for (int n = 0; n < NODE_NUM; n++) {
//                    // 一个真实机器节点关联NODE_NUM个虚拟节点
//                    tempNodes.put(HashRingAlgorithm.hash("SHARD-" + dataSourceName + "-NODE-" + n), dataSourceName);
//                }
//            }
//        }
//        nodes = tempNodes;


        return true;
    }

    /**
     * 刷新指定应用的数据源列表
     *
     * @param ltApplicationWSInfo 需要刷新数据源的应用程序信息
     * @return
     */
    @Override
    public boolean reloadAllDataSource(List ltApplicationWSInfo) {
        return false;
    }

    /**
     * 刷新指定应用的hash环信息
     *
     * @param ltApplicationWSInfo 需要刷新数据源的应用程序信息
     * @return
     */
    @Override
    public boolean reloadAllHashRing(List ltApplicationWSInfo) {
        return false;
    }


    /**
     * 查询：已落数据库信息
     *
     * @param shardMark 系统隔离字符
     * @param shardCode 分库主键
     * @return 已落数据库信息
     */
    private String locateDBInfo(String shardMark, String shardCode) {


        if (redisService == null) {
            logger.error("未找到redis管理对象，请检查！");

            return null;
        }
        Object obj = redisService.hget(shardMark, shardCode);
        // 没有主键对应的分库记录
        if (obj == null) {
            logger.info("未找到分库记录，分库主键：" + shardCode);

            return null;
        }
        if (logger.isInfoEnabled()) {
            logger.info("分库主键：" + shardCode + "；数据源名称：" + obj.toString());
        }


        return obj.toString();
    }

    /**
     * 使用分库主键路由出目标数据库
     *
     * @param shardMark 系统隔离字符
     * @param shardCode 分库主键
     * @param nodes node
     * @return 目标数据库信息
     */
    private String routeDBInfo(String shardMark, String shardCode, TreeMap<Long, String> nodes) {


        if (nodes.size() == 0) {
            logger.error("未找到配置的数据源信息，请检查！");

            return null;
        }
        // 获取节点后面的数据库列表
        SortedMap<Long, String> tail = nodes.tailMap(HashRingAlgorithm.hash(shardCode));
        String dataSourceName;
        if (tail.size() == 0) {
            // 无列表时取第一个节点（形成环）
            dataSourceName = nodes.get(nodes.firstKey());
        } else {
            // 返回第一个数据库信息
            dataSourceName = tail.get(tail.firstKey());
        }
        // 保存至redis
        if (saveRouteInfo(shardMark, shardCode, dataSourceName)) {
            if (logger.isInfoEnabled()) {
                logger.info("分库主键：" + shardCode + "；数据源名称：" + dataSourceName);
            }
            return dataSourceName;
        } else {
            logger.error("保存路由信息失败，请检查redis服务状态！");

            return null;
        }
    }

    /**
     * 保存落库路由信息
     *
     * @param shardMark      系统隔离字符
     * @param shardCode      分库主键
     * @param dataSourceName 数据源名称
     * @return 返回成功失败
     */
    private boolean saveRouteInfo(String shardMark, String shardCode, String dataSourceName) {

        if (redisService == null) {
            logger.error("未找到redis管理对象，请检查！");

            return false;
        }
        try {
            redisService.hset(shardMark, shardCode, dataSourceName);
            logger.debug("写入redis信息：" + shardMark + "-" + dataSourceName + "-" + shardCode);


            return true;
        } catch (Exception ex) {
            logger.error("保存路由信息失败,请检查redis服务状态！");
//            logger.error(ExceptionUtils.exceptionToString(ex));


            return false;
        }
    }
}
