package com.sinosoft.cloud.dataroute;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

/**
 * MurMurHash算法，非加密算法，性能高，比传统的CRC32,MD5，SHA-1等快很多，
 * 而且据说这个算法的碰撞率很低. http://murmurhash.googlepages.com/
 * Created by kaine on 2016/9/26.
 */
public class HashRingAlgorithm {
    /**
     * 使用一致性hash环算法计算输入参数
     *
     * @param key 输入参数
     * @return 输入参数在环中位置
     */
    public static final Long hash(String key) {
        ByteBuffer buf = ByteBuffer.wrap(key.getBytes());
        int seed = 0x1234ABCD;

        ByteOrder byteOrder = buf.order();
        buf.order(ByteOrder.LITTLE_ENDIAN);

        long m = 0xc6a4a7935bd1e995L;
        int r = 47;
        long h = seed ^ (buf.remaining() * m);
        long k;

        while (buf.remaining() >= 8) {
            k = buf.getLong();

            k *= m;
            k ^= k >>> r;
            k *= m;

            h ^= k;
            h *= m;
        }

        if (buf.remaining() > 0) {
            ByteBuffer finish = ByteBuffer.allocate(8).order(ByteOrder.LITTLE_ENDIAN);
            // for big-endian version, do this first:
            // finish.position(8-buf.remaining());
            finish.put(buf).rewind();
            h ^= finish.getLong();
            h *= m;
            h *= m;
        }

        h ^= h >>> r;
        h *= m;
        h ^= h >>> r;

        buf.order(byteOrder);
        return h;
    }
}
