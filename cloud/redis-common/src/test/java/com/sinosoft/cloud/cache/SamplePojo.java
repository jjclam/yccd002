package com.sinosoft.cloud.cache;

import java.io.Serializable;

public class SamplePojo implements Serializable {

    private String id;
    private String contNo;
    private String prtNo;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getContNo() {
        return contNo;
    }

    public void setContNo(String contNo) {
        this.contNo = contNo;
    }

    public String getPrtNo() {
        return prtNo;
    }

    public void setPrtNo(String prtNo) {
        this.prtNo = prtNo;
    }
}
