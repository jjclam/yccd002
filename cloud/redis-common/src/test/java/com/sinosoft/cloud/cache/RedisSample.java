package com.sinosoft.cloud.cache;


import com.sinosoft.cloud.cache.annotation.RedisListPut;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;

@Component
//@CacheConfig(cacheNames = "ContService")
public class RedisSample {

    Log logger = LogFactory.getLog(getClass());


    /**
     * get缓存
     *
     * @param id
     * @return
     */
    @Cacheable(value = "cacheName", key = "'id_'+#id")
    public SamplePojo getSamplePojo(String id) {
        logger.info("id: " + id);
        SamplePojo pojo = new SamplePojo();
        pojo.setContNo("2123123");
        pojo.setId(id);
        return pojo;
    }


    @CachePut(value = "cacheName", key = "#pojo.getId()")
    public void insertSample(SamplePojo pojo) {

    }

    @CacheEvict(value = "cacheName", key = "'id_'+#id")
    public void deleteSample(String id) {

    }
}
