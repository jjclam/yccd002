package com.sinosoft.cloud.cache;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = SimpleApplication.class)
public class SimpleTestApplication {
    @Autowired
    RedisSample redisSample;

    @Autowired
    RedisService redisService;


    @Test
    public void testInsertRedis(){
        SamplePojo samplePojo = new SamplePojo();
        samplePojo.setId("12345");
        redisSample.insertSample(samplePojo);
    }

    //
    //@Test
    //public void testGetRedis(){
    //    redisSample.getSamplePojo("12345");
    //}
    //
    //
    //@Test
    //public void testDelRedis(){
    //    redisSample.deleteSample("12345");
    //}
    //
    //
    //@Test
    //public void testRedisService(){
    //    redisService.set("yangming","test");
    //    System.out.println( redisService.get("yangming"));
    //}

}
