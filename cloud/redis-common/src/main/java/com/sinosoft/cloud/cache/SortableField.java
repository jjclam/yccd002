package com.sinosoft.cloud.cache;

import com.sinosoft.cloud.cache.annotation.RedisPrimaryHKey;

import java.lang.reflect.Field;

/**
 * Project abc-cloud
 * Created by sundongbo on 2017/9/19.
 * <pre>
 * 林花谢了春红，太匆匆，无奈朝来寒雨晚来风。
 * 胭脂泪，相留醉，几时重，自是人生长恨水长东。
 *
 * --翼羽水淼起辰月，秋山红日望紫枫--
 * </pre>
 */
public class SortableField {
    private RedisPrimaryHKey primaryHKey;
    private Field field;
    private String name;
    private Class<?> type;

    public SortableField() {
    }

    public SortableField(RedisPrimaryHKey primaryHKey, Field field) {
        super();
        this.primaryHKey = primaryHKey;
        this.field = field;
        this.name = field.getName();
        this.type = field.getType();
    }

    public RedisPrimaryHKey getPrimaryHKey() {
        return primaryHKey;
    }

    public void setPrimaryHKey(RedisPrimaryHKey primaryHKey) {
        this.primaryHKey = primaryHKey;
    }

    public Field getField() {
        return field;
    }

    public void setField(Field field) {
        this.field = field;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Class<?> getType() {
        return type;
    }

    public void setType(Class<?> type) {
        this.type = type;
    }
}
