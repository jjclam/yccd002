package com.sinosoft.cloud.cache.annotation;

import java.lang.annotation.*;

/**
 * Project abc-cloud
 * Created by sundongbo on 2017/10/13.
 * <pre>
 * 林花谢了春红，太匆匆，无奈朝来寒雨晚来风。
 * 胭脂泪，相留醉，几时重，自是人生长恨水长东。
 *
 * --翼羽水淼起辰月，秋山红日望紫枫--
 * </pre>
 */
@Target({ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface RedisListPut {
    /**
     *
      * @return value
     */
    String value() default "";

    /**
     *
     * @return key
     */
    String key() default "mqlist";

}
