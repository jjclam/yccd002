package com.sinosoft.cloud.cache;


import org.springframework.boot.SpringBootConfiguration;
import org.springframework.cache.annotation.EnableCaching;

/**
 * 应用扫描时redis控制类
 */
@SpringBootConfiguration
@EnableCaching
public class RedisConfiguration {

//    @Bean
//    public RedisService redisService() {
//        RedisService redisService = new RedisService();
//        return redisService;
//    }

}
