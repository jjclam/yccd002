package com.sinosoft.cloud.cache.annotation;

import java.lang.annotation.*;

/**
 * Redis表名注解
 * 防止出现ldcodepojo和ldcodeschema在redis中存储不同的两份数据
 * 且为了查询数据库，需要表名
 * @author  yangqm
 *
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.TYPE })
@Documented
public @interface RedisTable {
	String name() default "";
}
