package com.sinosoft.cloud.cache.annotation;


import java.lang.annotation.*;

/**
 * Redis用于存放hash类型数据的hash键注解
 * 注解实体类索引属性，可注解多个索引属性
 * 如果对象属性标明RedisIndexHKey，则存储时会将该索引字段和主键字段作为hash对象存储起来
 * @author  杨庆民
 *
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.FIELD })
@Documented
public @interface RedisIndexHKey {
	String value() default "";
}
