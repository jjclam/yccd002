package com.sinosoft.cloud.cache;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.data.redis.connection.RedisConnection;

import org.springframework.data.redis.core.*;
import org.springframework.data.redis.serializer.RedisSerializer;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.*;

/**
 * @param <K> String类型的key值
 * @param <V> value的类型
 */
@Component
public class RedisService<K extends String, V> {

    private static String REDISCODE = "utf-8";

    @Autowired
    private RedisTemplate<K, V> redisTemplate;

    public RedisService() {
//        if (this.getRedisTemplate() == null) {
//            RedisTemplate<K, V> redisTemplate = (RedisTemplate) SpringContextUtils.getBeanByClass(RedisTemplate.class);
//            this.setRedisTemplate(redisTemplate);
//        }
    }

    public RedisService(RedisTemplate redisTemplate) {
        this.setRedisTemplate(redisTemplate);
    }

    /**
     * @param kvMap key-value 集合
     */
    public void watch(final Map<K, V> kvMap) {
        Iterator<K> iterator = kvMap.keySet().iterator();
        K key = null;
        V value = null;
        while (iterator.hasNext()) {
            key = iterator.next();
            redisTemplate.watch(key);
        }
    }

    /**
     * @param keys keys集合
     */
    public void watch(final Collection<K> keys) {
        redisTemplate.watch(keys);
    }

    /**
     * @param key key
     */
    public void watch(final K key) {
        redisTemplate.watch(key);
    }

    /**
     * 开启事务
     */
    public void beginTx() {
        redisTemplate.multi();
    }

    /**
     * 关闭事务
     *
     * @return 返回值集合
     */
    public List<Object> endTx() {
        try {
            return redisTemplate.exec();
        } catch (Exception e) {
            e.printStackTrace();
            redisTemplate.discard();
        }
        return null;
    }

    /**
     * 删除key
     *
     * @param keys key集合
     */
    public void delete(final Collection<K> keys) {
        redisTemplate.delete(keys);
    }

    /**
     * 管道机制批量提交
     *
     * @param kvMap key-value的map集合
     */
    public void batchSet(final Map<K, V> kvMap) {
        redisTemplate.execute(new RedisCallback<V>() {
            @Override
            public V doInRedis(RedisConnection connection) throws DataAccessException {
                Set<K> keySets = kvMap.keySet();
                Iterator<K> iterator = keySets.iterator();
                K key = null;
                V value = null;
                RedisSerializer keySerializer = redisTemplate.getKeySerializer();
                RedisSerializer valueSerializer = redisTemplate.getValueSerializer();
                while (iterator.hasNext()) {
                    key = iterator.next();
                    value = kvMap.get(key);
                    connection.set(keySerializer.serialize(key), valueSerializer.serialize(value));
                }
                return value;
            }
        });


//        redisTemplate.executePipelined(new RedisCallback<Object>() {
//            /**
//             * 在使用管道机制之前。首先将kvMap中的所有的k进行锁定数据。
//             * 然后开启事务进行操作。
//             * @param connection
//             * @return
//             * @throws DataAccessException
//             */
//            @Override
//            public Object doInRedis(RedisConnection connection) throws DataAccessException {
//
////                if (!batchLock(kvMap)) {
////                    throw new RedisPipelineException("无法lock对应的key值!", null);
////                }
//
////                connection.multi();
//                Set<K> keySets = kvMap.keySet();
//                Iterator<K> iterator = keySets.iterator();
//                RedisSerializer keySerializer = redisTemplate.getKeySerializer();
//                RedisSerializer valueSerializer = redisTemplate.getValueSerializer();
//                while (iterator.hasNext()) {
//                    final K key = iterator.next();
//                    final V value = kvMap.get(key);
//                    connection.set(keySerializer.serialize(key), valueSerializer.serialize(value));
//                }
////                connection.exec();
//                return null;
//            }
//        });
    }


    /**
     * 管道机制批量删除
     *
     * @param kvMap key的map集合
     */
    public void batchDel(final Map<K, V> kvMap) {
        redisTemplate.execute(new RedisCallback<V>() {
            @Override
            public V doInRedis(RedisConnection connection) throws DataAccessException {
                Set<K> keySets = kvMap.keySet();
                Iterator<K> iterator = keySets.iterator();
                K key = null;
                V value = null;
                RedisSerializer keySerializer = redisTemplate.getKeySerializer();
                RedisSerializer valueSerializer = redisTemplate.getValueSerializer();
                while (iterator.hasNext()) {
                    key = iterator.next();
                    value = kvMap.get(key);
                    connection.del(keySerializer.serialize(key), valueSerializer.serialize(value));
                }
                return value;
            }
        });
//        redisTemplate.executePipelined(new RedisCallback<Object>() {
//            @Override
//            public Object doInRedis(RedisConnection connection) throws DataAccessException {
//
//                Set<K> keySets = kvMap.keySet();
//                Iterator<K> iterator = keySets.iterator();
//                RedisSerializer keySerializer = redisTemplate.getKeySerializer();
//                RedisSerializer valueSerializer = redisTemplate.getValueSerializer();
//
//                while (iterator.hasNext()) {
//                    final K key = iterator.next();
//                    final V value = kvMap.get(key);
//                    connection.del(keySerializer.serialize(key), valueSerializer.serialize(value));
//                }
//
//                return null;
//            }
//        });
//        redisTemplate.executePipelined(new RedisCallback<Object>() {
//            @Override
//            public Object doInRedis(RedisConnection connection) throws DataAccessException {
//                connection.openPipeline();
//                RedisSerializer keySerializer = redisTemplate.getStringSerializer();
//                Iterator<K> iterator = kvMap.keySet().iterator();
//                while (iterator.hasNext()) {
//                    K key = iterator.next();
//                    connection.del(keySerializer.serialize(key));
//                }
//                connection.closePipeline();
//                return null;
//            }
//        });
    }


    /**
     * 管道机制批量删除
     *
     * @param keys key集合
     */
    public void batchDel(final Collection<K> keys) {
        redisTemplate.executePipelined(new RedisCallback<Object>() {
            @Override
            public Object doInRedis(RedisConnection connection) throws DataAccessException {

                Iterator<K> iterator = keys.iterator();
                RedisSerializer keySerializer = redisTemplate.getKeySerializer();
                RedisSerializer valueSerializer = redisTemplate.getValueSerializer();

                while (iterator.hasNext()) {
                    final K key = iterator.next();
                    connection.del(keySerializer.serialize(key));
                }

                return null;
            }
        });
    }

    /**
     * @param keys 删除多个key
     */
    public void delete(final K... keys) {
        delete(Arrays.asList(keys));
    }

    /**
     * @param key   key
     * @param value value
     */
    public void set(final K key, final V value) {
        redisTemplate.execute(new RedisCallback<V>() {
            @Override
            public V doInRedis(RedisConnection connection) throws DataAccessException {
                RedisSerializer keySerializer = redisTemplate.getKeySerializer();
                RedisSerializer valueSerializer = redisTemplate.getValueSerializer();
                connection.set(keySerializer.serialize(key), valueSerializer.serialize(value));
                return value;
            }
        });
    }

    public void pSetEx(final K key, final V value,long times) {
        redisTemplate.execute(new RedisCallback<V>() {
            @Override
            public V doInRedis(RedisConnection connection) throws DataAccessException {
                RedisSerializer keySerializer = redisTemplate.getKeySerializer();
                RedisSerializer valueSerializer = redisTemplate.getValueSerializer();
                connection.pSetEx(keySerializer.serialize(key), times,valueSerializer.serialize(value));
                return value;
            }
        });
    }
    /**
     * @param key key
     * @return 返回值
     */
    public V get(final String key) {
        return redisTemplate.execute(new RedisCallback<V>() {
            @Override
            public V doInRedis(RedisConnection connection) throws DataAccessException {
                RedisSerializer keySerializer = redisTemplate.getKeySerializer();
                RedisSerializer valueSerializer = redisTemplate.getValueSerializer();
                return (V) valueSerializer.deserialize(connection.get(keySerializer.serialize(key)));
            }
        });
    }

    /**
     * 获取redis 中的集合
     *
     * @param keys key的集合
     * @return 返回值集合
     */
    public List<V> mget(final List<K> keys) {
        if (keys == null || keys.size() <= 0) {
            return null;
        }

        ValueOperations valOper = redisTemplate.opsForValue();
        return valOper.multiGet(keys);
    }

    /**
     * 存储hash结构的数据
     *
     * @param redisKey redis键
     * @param hashKey  存储hash对象的键
     * @param hashVal  存储hash对象的值
     */
    public void hset(final K redisKey, final K hashKey, final V hashVal) {
        if (StringUtils.isEmpty(redisKey) || StringUtils.isEmpty(hashKey) || hashVal == null) {
            return;
        }

        HashOperations hashOper = redisTemplate.opsForHash();
        hashOper.put(redisKey, hashKey, hashVal);
    }

    /**
     * 获取hash结构的数据
     *
     * @param redisKey redis键
     * @param hashKey  hash对象的键
     * @return 返回值
     */
    public V hget(final String redisKey, final String hashKey) {
        if (StringUtils.isEmpty(redisKey) || StringUtils.isEmpty(hashKey)) {
            return null;
        }

        HashOperations hashOper = redisTemplate.opsForHash();
        return (V) hashOper.get(redisKey, hashKey);
    }

    /**
     * 批量存储hash结构的数据
     *
     * @param redisKey redis键
     * @param map      hash对象的键
     */
    public void hmset(final K redisKey, final Map<K, V> map) {
        if (StringUtils.isEmpty(redisKey) || map == null) {
            return;
        }

        HashOperations hashOper = redisTemplate.opsForHash();
        hashOper.putAll(redisKey, map);
    }

    /**
     * 批量获取hash结构的数据
     *
     * @param redisKey redis键
     * @param hashKeys hash结构的键
     * @return 返回值集合
     */
    public List<V> hmget(final String redisKey, final List<String> hashKeys) {
        if (StringUtils.isEmpty(redisKey) || hashKeys == null || hashKeys.size() <= 0) {
            return null;
        }

        HashOperations hashOper = redisTemplate.opsForHash();
        return hashOper.multiGet(redisKey, hashKeys);
    }

    /**
     * 查看哈希表的指定字段是否存在
     *
     * @param redisKey redis键
     * @param hashKey  hash对象的键
     * @return boolean
     */
    public boolean hexists(final String redisKey, final String hashKey) {
        if (StringUtils.isEmpty(redisKey) || StringUtils.isEmpty(hashKey)) {
            return false;
        }

        HashOperations hashOper = redisTemplate.opsForHash();
        return hashOper.hasKey(redisKey, hashKey);
    }

    /**
     * 删除哈希表 key 中的一个或多个指定值，不存在的值将被忽略
     *
     * @param redisKey redis键
     * @param hashKeys hash对象的键
     * @return 影响的行数
     */
    public Long hdel(final String redisKey, final Object... hashKeys) {
        if (StringUtils.isEmpty(redisKey) || hashKeys == null) {
            return Long.getLong("0");
        }

        HashOperations hashOper = redisTemplate.opsForHash();
        return hashOper.delete(redisKey, hashKeys);
    }

    /**
     * 根据rediskey获取所有哈希表中的key字段
     *
     * @param redisKey redis键
     * @return 影响的行数
     */
    public Set<K> hkeys(final String redisKey) {
        if (StringUtils.isEmpty(redisKey)) {
            return null;
        }

        HashOperations hashOper = redisTemplate.opsForHash();
        return hashOper.keys(redisKey);
    }


    /**
     * 存储set结构的数据
     *
     * @param redisKey redis键
     * @param vals     一个或多个值
     * @return 返回值
     */
    public Long sset(final K redisKey, V... vals) {
        if (StringUtils.isEmpty(redisKey) || vals == null) {
            return new Long(0);
        }

        SetOperations setOper = redisTemplate.opsForSet();
        return setOper.add(redisKey, vals);
    }

    /**
     * 获取set结构的集合数据
     *
     * @param redisKey redis键
     * @return 返回值集合
     */
    public Set<V> sget(final String redisKey) {
        if (StringUtils.isEmpty(redisKey)) {
            return null;
        }

        SetOperations setOper = redisTemplate.opsForSet();
        return setOper.members(redisKey);
    }

    /**
     * 设置多个值
     *
     * @param redisKey key
     * @param vals     多个值
     * @return 返回值
     */
    public Long sdel(final K redisKey, V... vals) {
        if (StringUtils.isEmpty(redisKey) || vals == null) {
            return new Long(0);
        }

        SetOperations setOper = redisTemplate.opsForSet();
        return setOper.remove(redisKey, vals);
    }


    /**
     * @param pattern 正则
     * @return 结果集集合
     */
    public Set<K> keys(K pattern) {
        return redisTemplate.keys(pattern);

    }


    /**
     * scan命令迭代获取结果
     *
     * @param pattern 通配符表达式
     * @param count   每次遍历的结果数量，当数据量较大时，可能需要设置的比较大，比如1万，否则时间可能会比较长
     * @return 结果集集合
     */
    public List<String> scan(final String pattern, final int count) {
        if (StringUtils.isEmpty(pattern)) {
            return new ArrayList<>();
        }
        return redisTemplate.execute(new RedisCallback<List<String>>() {
            public List<String> doInRedis(RedisConnection connection) throws DataAccessException {
                ScanOptions options = ScanOptions.scanOptions().match(pattern).count(count).build();
                Cursor<byte[]> cursor = connection.scan(options);
                List<String> result = new ArrayList<String>();
                if (cursor != null) {
                    while (cursor.hasNext()) {
                        result.add(new String(cursor.next()));
                    }
                }
                return result;
            }
        });
    }

    /**
     * 判断key是否存在
     *
     * @param key key
     * @return boolean
     */
    public boolean exists(final K key) {
        return redisTemplate.execute(new RedisCallback<Boolean>() {
            public Boolean doInRedis(RedisConnection connection) throws DataAccessException {
                RedisSerializer keySerializer = redisTemplate.getKeySerializer();
                return connection.exists(keySerializer.serialize(key));
            }
        });
    }

    /**
     * @return redis大小
     */
    public Long dbSize() {
        return redisTemplate.execute(new RedisCallback<Long>() {
            public Long doInRedis(RedisConnection connection) throws DataAccessException {
                return connection.dbSize();
            }
        });
    }

    /**
     * @return 延迟数值
     */
    public String ping() {
        return redisTemplate.execute(new RedisCallback<String>() {
            public String doInRedis(RedisConnection connection) throws DataAccessException {
                return connection.ping();
            }
        });
    }

    public void setRedisTemplate(RedisTemplate redisTemplate) {
        this.redisTemplate = redisTemplate;
    }

    public RedisTemplate<K, V> getRedisTemplate() {
        return redisTemplate;
    }

    /**
     * @param key   key
     * @param value value
     * @return boolean
     */
    public boolean setNX(final K key, final V value) {
        return redisTemplate.execute(new RedisCallback<Boolean>() {
            public Boolean doInRedis(RedisConnection connection) throws DataAccessException {
                RedisSerializer keySerializer = redisTemplate.getKeySerializer();
                RedisSerializer valueSerializer = redisTemplate.getValueSerializer();
                return connection.setNX(keySerializer.serialize(key), valueSerializer.serialize(value));
            }
        });
    }

    /**
     * @param kvMap keymap
     * @return boolean
     */
    public boolean setNX(final HashMap<K, V> kvMap) {
        return redisTemplate.execute(new RedisCallback<Boolean>() {
            public Boolean doInRedis(RedisConnection connection) throws DataAccessException {
//                connection.multi();
                Set<K> keySets = kvMap.keySet();
                Iterator<K> iterator = keySets.iterator();
                RedisSerializer keySerializer = redisTemplate.getKeySerializer();
                RedisSerializer valueSerializer = redisTemplate.getValueSerializer();

                while (iterator.hasNext()) {
                    final K key = iterator.next();
                    final V value = kvMap.get(key);
                    connection.setNX(keySerializer.serialize(key), valueSerializer.serialize(value));
                }
                List<Object> list = connection.exec();
                /**
                 * 只有有一个是false 就返回false
                 */
                for (Object obj : list) {
                    if (!(Boolean) obj) {
                        return false;
                    }
                }
                return true;
            }
        });
    }

    /**
     * 返回redis数据类型
     *
     * @param key key值
     * @return value值
     */
    public String type(final K key) {
        if (key == null) {
            return null;
        }
        ValueOperations valOper = redisTemplate.opsForValue();

        return valOper.getOperations().type(key).toString();
    }


    /**
     * 获取队列集合
     *
     * @param key key值
     * @return 返回值
     */
    public V listget(final K key) {
        if (key == null) {
            return null;
        }
        return redisTemplate.opsForList().index(key, -1);
    }


    /**
     * 移除并去除列表第一个元素
     *
     * @param key list集合key
     * @return 返回值
     */
    public V listLpop(final K key) {
        if (key == null) {
            return null;
        }
        return redisTemplate.opsForList().leftPop(key);
    }


    /**
     * 向List集合最后添加值
     *
     * @param key   list集合的名称
     * @param value 具体内容
     * @return 返回值
     */
    public Long listRpush(final K key, final V value) {
        if (key == null) {
            return null;
        }
        return redisTemplate.opsForList().rightPush(key, value);
    }

    /**
     * 向List集合添加集合
     *
     * @param key   list集合的名称
     * @param value 具体内容
     * @return 返回值
     */
    public Long listRpushAll(final K key, final List<V> value) {
        if (key == null) {
            return null;
        }
        return redisTemplate.opsForList().rightPushAll(key, value);
    }


}