package com.sinosoft.cloud.cache.annotation;

import java.lang.annotation.*;

/**
 * Redis用于存放hash类型数据的hash键注解
 * 注解实体类业务主键，可注解多个主键
 * @author  杨庆民
 *
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.FIELD })
@Documented
public @interface RedisPrimaryHKey {
	/**
	 * 主键顺序，多主键情况，需添加不同主键的先后顺序（必须要添加），从1开始
	 * 存放redis hash对象的key按先后顺序将主键连接起来存放
	 * @author
	 */
	int order() default 0;
}
