package com.sinosoft.cache;

import com.sinosoft.cloud.cache.RedisService;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.SchemaSet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;


/**
 * @param <Record>    Record
 * @param <RecordSet> RecordSet
 */
@Service
public class MemDBOper<Record extends Schema, RecordSet extends SchemaSet> extends Object {
    private static final String MDB = "mdb";
    private static final String SPLIT = "_";
    private static final String PKSPLIT = "@";
    private static final String SCHEMA_SUFFIX = "Schema";
    private static final String SCHEMASET_SUFFIX = "Set";


    @Autowired
    RedisService<String, Record> redisService;

    /**
     * 根据SCHEMA得到内存数据库的KEY
     *
     * @param aRecord aRecord
     * @return 返回值
     */
    public String getMemDBKey(Record aRecord) {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(MDB);
        strReturn.append(SPLIT);

        String tableName = aRecord.getClass().getSimpleName().replaceAll(SCHEMA_SUFFIX, "");
        strReturn.append(tableName);
        strReturn.append(SPLIT);

        String[] pks = aRecord.getPK();
        int pkSize = pks.length;
        for (int i = 1; i <= pkSize; i++) {
            String pkStr = aRecord.getV(pks[i - 1]);
            if (pkStr == null || pkStr.equals("")) {
                //PK KEY字段不能有空值
                return null;
            }
            strReturn.append(pkStr);
            if (i < pkSize) {
                strReturn.append(PKSPLIT);
            }
        }

        return strReturn.toString();
    }

    /**
     * @param aRecord aRecord
     * @return 返回值
     */
    public boolean delete(Record aRecord) {

        redisService.delete(getMemDBKey(aRecord));
        return true;
    }

    /**
     * @param aRecord aRecord
     * @return 返回值
     */
    public boolean update(Record aRecord) {

        String memDBKey = getMemDBKey(aRecord);
        if (redisService.get(memDBKey) == null) {
            return false;
        }
        redisService.set(memDBKey, aRecord);
        return true;
    }

    /**
     * @param aRecord aRecord
     * @return 返回值
     */
    public boolean insert(Record aRecord) {

        String memDBKey = getMemDBKey(aRecord);
        if (redisService.get(memDBKey) != null) {
            return false;
        }
        redisService.set(memDBKey, aRecord);
        return true;
    }

    /**
     * @param aRecord aRecord
     * @return 返回值
     */
    public Record query(Record aRecord) {

        return redisService.get(getMemDBKey(aRecord));
    }

    /**
     * @param aRecordSet aRecordSet
     * @return 返回值
     */
    public boolean delete(RecordSet aRecordSet) {
        HashMap<String, Record> kvMap = new HashMap<String, Record>();
        for (int i = 1; i <= aRecordSet.size(); i++) {
            Record aRecord = (Record) aRecordSet.getObj(i);
            kvMap.put(getMemDBKey(aRecord), aRecord);
        }


        redisService.batchDel(kvMap);
        return true;
    }

    /**
     * @param aRecordSet aRecordSet
     * @return 返回值
     */
    public boolean update(RecordSet aRecordSet) {
        HashMap<String, Record> kvMap = new HashMap<String, Record>();
        for (int i = 1; i <= aRecordSet.size(); i++) {
            Record aRecord = (Record) aRecordSet.getObj(i);
            kvMap.put(getMemDBKey(aRecord), aRecord);
        }


//        String memDBKey = getMemDBKey(aRecord);
//        if(redisService.get(memDBKey) == null) return false;
//        redisService.set(memDBKey, aRecord);
        redisService.batchSet(kvMap);
        return true;
    }

    /**
     * @param aRecordSet aRecordSet
     * @return 返回值
     */
    public boolean insert(RecordSet aRecordSet) {
        HashMap<String, Record> kvMap = new HashMap<String, Record>();
        for (int i = 1; i <= aRecordSet.size(); i++) {
            Record aRecord = (Record) aRecordSet.getObj(i);
            kvMap.put(getMemDBKey(aRecord), aRecord);
        }


//        String memDBKey = getMemDBKey(aRecord);
//        if(redisService.get(memDBKey) != null) return false;
//        redisService.set(memDBKey, aRecord);
//        System.out.println(kvMap.toString());
        redisService.batchSet(kvMap);
        return true;
    }

    /**
     * @param aRecordSet aRecordSet
     * @return 返回值
     */
    public RecordSet query(RecordSet aRecordSet) {

        RecordSet returnSet = null;
        try {
            returnSet = (RecordSet) getEntityClass(aRecordSet).newInstance();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        for (int i = 1; i <= aRecordSet.size(); i++) {
            Record aRecord = (Record) aRecordSet.getObj(i);
//            System.out.println(getMemDBKey(aRecord));
//            System.out.println(redisService.get(getMemDBKey(aRecord)));
            Record tRecord = redisService.get(getMemDBKey(aRecord));
            if (tRecord != null) {
                returnSet.add(tRecord);
            }
        }
        if (returnSet.size() == 0) {
            return null;
        }
        return returnSet;
    }

    /**
     * @param aRecordSet aRecordSet
     * @return
     */
    protected Class getEntityClass(RecordSet aRecordSet) {
        return aRecordSet.getClass();
    }


}
