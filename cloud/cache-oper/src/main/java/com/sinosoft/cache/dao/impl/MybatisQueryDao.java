package com.sinosoft.cache.dao.impl;


import com.sinosoft.cloud.cache.SortableField;
import com.sinosoft.mybatis.dao.GeneralCrudDao;
import com.sinosoft.mybatis.utility.GeneralQueryParam;
import com.sinosoft.mybatis.utility.PersistentUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Project abc-cloud
 * Created by sundongbo on 2017/9/19.
 * <pre>
 * 林花谢了春红，太匆匆，无奈朝来寒雨晚来风。
 * 胭脂泪，相留醉，几时重，自是人生长恨水长东。
 *
 * --翼羽水淼起辰月，秋山红日望紫枫--
 * </pre>
 */

@Service
public class MybatisQueryDao {

    private static Log logger = LogFactory.getLog(MybatisQueryDao.class);

    @Autowired
    private GeneralCrudDao generalCrudDao;


    /**
     * 使用指定的检索标准检索数据并分页返回数据
     *
     * @param tableName 表名
     * @param map       map
     * @param colname   列名
     * @return 结果集
     */
    public List<Map<String, Object>> findForJdbc(String tableName, Map<String, String> map, String colname) {
        GeneralQueryParam generalQueryParam = new GeneralQueryParam();
        List<String> conditionExp = new ArrayList<>();
        Map<String, Object> conditionParam = new HashMap<String, Object>();
        for (String key : map.keySet()) {
            conditionExp.add(key + " = #{conditionParam." + key + "}");
        }
        List<String> column = new ArrayList<>();
        column.add(colname);
        generalQueryParam.setTableName(tableName);
        generalQueryParam.setConditionParam(conditionParam);
        generalQueryParam.setConditionExp(conditionExp);

        generalQueryParam.setQueryColumn(column);
        return generalCrudDao.selectAdvancedByColumn(generalQueryParam);
    }


    /**
     * 使用指定的检索标准检索数据并分页返回数据
     *
     * @param clazz        clazz
     * @param indexKeyName indexKeyName
     * @param indexKeyType indexKeyType
     * @param indexKeyVal  indexKeyVal
     * @param <T>          类型
     * @return 返回结果集
     */
    public <T> List<T> findObjForJdbc(Class<T> clazz, String indexKeyName, String indexKeyType, String indexKeyVal) {
        GeneralQueryParam generalQueryParam = new GeneralQueryParam();
        List<String> conditionExp = new ArrayList<>();
        Map<String, Object> conditionParam = new HashMap<String, Object>();

        if ("double".equalsIgnoreCase(indexKeyType)) {
            conditionParam.put(indexKeyName, Double.valueOf(indexKeyVal));
        }
        if ("int".equalsIgnoreCase(indexKeyType)) {
            conditionParam.put(indexKeyName, Integer.valueOf(indexKeyVal));
        }
        if ("java.lang.String".equalsIgnoreCase(indexKeyType)) {
            conditionParam.put(indexKeyName, indexKeyVal);
        }
        conditionExp.add(indexKeyName + " = #{conditionParam." + indexKeyName + "}");

        generalQueryParam.setConditionParam(conditionParam);
        generalQueryParam.setConditionExp(conditionExp);

        return generalCrudDao.selectAdvanced(clazz, generalQueryParam);
    }


    /**
     * 使用指定的检索标准检索数据并分页返回数据
     *
     * @param clazz          clazz
     * @param sortableFields sortableFields
     * @param pKeys          pKeys
     * @param <T>            类型
     * @return 返回结果集
     */
    public <T> List<T> findObjForJdbc(Class<T> clazz, List<SortableField> sortableFields, String[] pKeys) {
        GeneralQueryParam generalQueryParam = new GeneralQueryParam();
        List<String> conditionExp = new ArrayList<>();
        Map<String, Object> conditionParam = new HashMap<String, Object>();
        for (int i = 0; i < sortableFields.size(); i++) {
            conditionExp.add(sortableFields.get(i).getName() + " = #{conditionParam." + sortableFields.get(i).getName() + "}");

            if ("double".equalsIgnoreCase(sortableFields.get(i).getType().getName())) {
                conditionParam.put(sortableFields.get(i).getName(), Double.valueOf(pKeys[i]));
            }
            if ("int".equalsIgnoreCase(sortableFields.get(i).getType().getName())) {
                conditionParam.put(sortableFields.get(i).getName(), Integer.valueOf(pKeys[i]));
            }
            if ("java.lang.String".equalsIgnoreCase(sortableFields.get(i).getType().getName())) {
                conditionParam.put(sortableFields.get(i).getName(), pKeys[i]);
            }
        }
        generalQueryParam.setConditionParam(conditionParam);
        generalQueryParam.setConditionExp(conditionExp);

        return generalCrudDao.selectAdvanced(clazz, generalQueryParam);
    }


    /**
     * @param sql  sql
     * @param objs objs
     * @return 返回结果集
     */
    public List<Map<String, Object>> findForJdbc(String sql, Object... objs) {
        return generalCrudDao.queryForList(sql, objs);
    }

    /**
     * @param sql   sql
     * @param param param
     * @return 返回结果
     */
    public Integer executeSql(String sql, List<Object> param) {
        return generalCrudDao.update(sql, param);
    }

    /**
     * @param sql   sql
     * @param param param
     * @return 返回结果集
     */
    public Integer executeSql(String sql, Object... param) {
        return generalCrudDao.update(sql, param);
    }

    /**
     *
     * @param sql sql
     * @param objs objs
     * @return 返回结果
     */
    public Map<String, Object> findOneForJdbc(String sql, Object... objs) {
        try {
            return generalCrudDao.queryForMap(sql, objs);
        } catch (EmptyResultDataAccessException e) {
            return null;
        }
    }

    /**
     * 绑定变量查询
     *
     * @param sql   sql
     * @param clazz clazz
     * @param objs  objs
     * @param <T> 类型
     * @return 返回结果
     */
    public <T> List<T> findObjForJdbcByParams(String sql, Class<T> clazz, Object... objs) {
        logger.info("sql:" + sql);
        List<Map<String, Object>> mapList = findForJdbc(sql, objs);
        return PersistentUtil.parseToBean(mapList, clazz);
    }

    /**
     * @param sql  sql
     * @param objs objs
     * @return 返回结果
     */
    public List<String> findObjForJdbcForString(String sql, Object... objs) {
        List<String> rsList = new ArrayList<String>();
        logger.info("sql:" + sql);
        List<Map<String, Object>> mapList = findForJdbc(sql, objs);

        for (Map<String, Object> m : mapList) {
            try {
                String contno = (String) m.get("CONTNO");
                rsList.add(contno);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return rsList;
    }
}
