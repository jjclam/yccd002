package com.sinosoft.cache.dao.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONException;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SerializerFeature;


import com.sinosoft.cloud.cache.RedisService;

import com.sinosoft.cloud.cache.SortableField;
import com.sinosoft.cloud.cache.annotation.RedisIndexHKey;
import com.sinosoft.cloud.cache.annotation.RedisPrimaryHKey;

import com.sinosoft.cloud.common.SpringContextUtils;

import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.lis.pubfun.MMap;

import com.sinosoft.mybatis.utility.PersistentUtil;
import com.sinosoft.utility.DateUtil;
import com.sinosoft.utility.ReflectUtil;
import com.sinosoft.utility.VData;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.lang.reflect.Field;
import java.util.*;

/**
 * 类描述： 操作redis库DAO层泛型基类
 *
 * @author yangqm
 * @version 1.0
 * @date： 日期：2016-8-10 时间：上午10:16:48
 */
@Service
public class RedisCommonDao {

    private static final String VMAP = "Map";
    private static final String ENTITY = "Ety";
    private static final String SPLIT = "_";
    private static final String PKSPLIT = "|||";
    private static final String ATSPLIT = "@";
    private static final String INDEX = "Idx";
    private static final String RIAKAMNT = "RA"; //风险保额标识
    private static final String REFERENCE = "REF"; //风险保额映射关系标识
    private static final String RESULT = "RES"; //风险保额结果标识
    private static final String POLMSG = "POLMESSAGE"; //保单数据标识
    private static final String UW_CHECK_LIST="UWCK_LIST";
    private static final String UW_CHECK_OBJ="UWCK_OBJ";
    private static final String UW_CHECK_STRING="UWCK_STRING";
    //银行账号提重key的前缀
    private static final String DC_BANKACC_PREFIX = "DC_BANKACC_PREFIX_";
    //投保人电话提重key的前缀
    private static final String DC_APPNT_PHONE_PREFIX = "DC_APPNT_PHONE_PREFIX_";
    //代理人电话提重key的前缀
    private static final String DC_LAAGENT_PHONE_PREFIX = "DC_LAAGENT_PHONE_PREFIX_";


    /**
     * 初始化Log4j的一个实例
     */

    private static Log logger = LogFactory.getLog(RedisCommonDao.class);

    private static SerializerFeature[] features =
            {SerializerFeature.WriteDateUseDateFormat, SerializerFeature.DisableCircularReferenceDetect};

    @Autowired
    private RedisService redisService;


    @Autowired
    @Qualifier("redisTemplate")
    private RedisTemplate redisTemplateTrans;

    /**
     * 根据主键查找redis实体
     *
     * @param <T>    类型
     * @param tClass 获取的实体类类型
     * @param id     实体RedisPrimaryHKey注解对应的值
     *               如果是多主键，则不同值之间加|||分隔
     * @return 返回值
     */
    public <T> T getEntity(Class<T> tClass, String id) {
        Assert.notNull(tClass, "class不能为空");
        Assert.hasText(id, "主键不能为空");
        String redisEntityKey = getRedisEntityKey(tClass, id);
        String valStr = (String) redisService.get(redisEntityKey);
        return JSON.parseObject(valStr, tClass);
    }

    /**
     * 根据主键查找redis实体，如果redis中没有数据，继续查找数据库
     * 如果数据库中存在，则返回的同时加载到redis中
     * 仅限于配置或者定义数据
     *
     * @param <T>    类型
     * @param tClass 获取的实体类类型
     * @param id     实体RedisPrimaryHKey注解对应的值
     *               如果是多主键，则不同值之间加|||分隔
     * @return 返回值
     */
    public <T> T getEntityRelaDB(Class<T> tClass, String id) {
        //redis中未查询到值，则去数据库中查询
        T entity = null;
        if (entity == null) {
            logger.info("redis中未查到值，查询数据库");

            List<SortableField> sortableFields = getRedisPKeySortFields(tClass);
            String[] pKeys = id.split("\\|\\|\\|");
            if (pKeys.length != sortableFields.size()) {
                logger.error("传入主键参数与实体类中注解属性数目不一致");
                return null;
            }

            MybatisQueryDao commonDao = (MybatisQueryDao) SpringContextUtils.getBeanByClass(MybatisQueryDao.class);
            List<T> entities = commonDao.findObjForJdbc(tClass, sortableFields, pKeys);
            entity = (entities != null && entities.size() > 0) ? entities.get(0) : null;

//            if (entity != null) {
//                //保存redis，只保存主键不保存索引
//                saveOnlyEntity(entity);
//            }
        }
        return entity;
    }

    /**
     * 根据rediskey查找redis实体
     *
     * @param <T>      类型
     * @param redisKey redis键
     * @return 返回值
     */
    private <T> T getEntity(String redisKey) {
        return (T) redisService.get(redisKey);
    }

    /**
     * 根据主键List查找redis实体
     *
     * @param <T>    类型
     * @param tClass 获取的实体类类型
     * @param idList 主键List，实体RedisPrimaryHKey注解对应的值list
     * @return
     */
    public <T> List getEntitys(Class<T> tClass, List<String> idList) {
        Assert.notNull(tClass, "class不能为空");
        Assert.notEmpty(idList, "主键list不能为空");

        //将主键转换为redis实体键
        List<String> redisEntityKeys = getRedisEntityKeys(tClass, idList);
        try {
            List<String> valStrList = redisService.mget(redisEntityKeys);
            List<T> valList = new ArrayList<>();
            for (int i = 0; i < valStrList.size(); i++) {
                String val = valStrList.get(i);
                valList.add((T) JSON.parseObject(val, tClass));
            }
            return valList;
        } catch (JSONException e) {
            e.printStackTrace();
            return redisService.mget(redisEntityKeys);
        }
    }

    /**
     * 根据主键List查找redis实体，如果redis中没有数据，继续查找数据库
     * 如果数据库中存在，则返回的同时加载到redis中
     * 仅限于配置或者定义数据
     *
     * @param <T>    类型
     * @param tClass 获取的实体类类型
     * @param idList 主键List，实体RedisPrimaryHKey注解对应的值list
     * @return
     */
    public <T> List<T> getEntitysRelaDB(Class<T> tClass, List<String> idList) {
        Assert.notNull(tClass, "class不能为空");
        Assert.notEmpty(idList, "主键list不能为空");

        //一条一条检查
        List<T> entities = new ArrayList<>();
        for (int i = 0; i < idList.size(); i++) {
            entities.add(getEntityRelaDB(tClass, idList.get(i)));
        }

        return entities;
    }

    /**
     * 此方法只供数据交互平台使用，存储风险保额请求业务号与被保人五要素的映射关系
     *
     * @param redisKey   key
     * @param primaryVal primaryVal
     * @return 返回值
     */
    public boolean save(String redisKey, String primaryVal) {


        redisKey = RIAKAMNT + SPLIT + REFERENCE + SPLIT + redisKey;
        redisService.set(redisKey, primaryVal);

        return true;
    }

    //

    /**
     * 此方法只供数据交互平台使用,删除风险保额请求业务号与被保人五要素的映射关系
     *
     * @param redisKey rediskey
     * @return 返回值
     */
    public boolean delete(String redisKey) {

        redisKey = RIAKAMNT + SPLIT + REFERENCE + SPLIT + redisKey;
        redisService.delete(redisKey);

        return true;
    }


    /**
     * 此方法只供数据交互平台使用
     *
     * @param redisKey redisKey
     * @return 返回值
     */
    public String getValue(String redisKey) {

        redisKey = RIAKAMNT + SPLIT + REFERENCE + SPLIT + redisKey;
        String value = (String) redisService.get(redisKey);
        return value;
    }


    /**
     * 此方法只供数据交互平台使用，存储被保人风险保额数据
     *
     * @param redisKey redisKey
     * @param entities entities
     * @return 返回值
     */
    public boolean saveEntity(String redisKey, List entities) {


        redisKey = RIAKAMNT + SPLIT + RESULT + SPLIT + redisKey;
        List<String> entityStrList = new ArrayList<>();
        for (int i = 0; i < entities.size(); i++) {
            Object entity = entities.get(i);
            entityStrList.add(JSON.toJSONString(entity, features));
        }
        redisService.set(redisKey, entityStrList);
        return true;
    }

    //

    /**
     * 此方法只供数据交互平台使用，根据被保人五要素获取被保人风险保额信息
     *
     * @param tClass   tClass
     * @param redisKey redisKey
     * @param <T>      类型
     * @return 返回值
     */
    public <T> List<T> getEntityRiskAmnt(Class<T> tClass, String redisKey) {

        List list = new ArrayList();
        redisKey = RIAKAMNT + SPLIT + RESULT + SPLIT + redisKey;
        list = (List) redisService.get(redisKey);

        List<T> valList = new ArrayList<>();
        if (list != null) {
            for (int i = 0; i < list.size(); i++) {
                String val = (String) list.get(i);
                valList.add(JSON.parseObject(val, tClass));
            }
        }

        return valList;
    }

    //

    /**
     * 此方法只供数据交互平台使用,根据五要素删除对应风险保额
     *
     * @param redisKey redisKey
     */
    public void deleteEntitys(String redisKey) {

        List list = new ArrayList();
        redisKey = RIAKAMNT + SPLIT + RESULT + SPLIT + redisKey;
        redisService.delete(redisKey);
    }

    /**
     * @param redisKey redisKey
     * @return 返回值
     */
    public String getCheckBankAcc(String redisKey) {
        redisKey = DC_BANKACC_PREFIX + redisKey;
        String valStr = (String) redisService.get(redisKey);
        return valStr;
    }
    /**
     * @param redisKey redisKey
     * @return 返回值
     */
    public String getAppntPhone(String redisKey) {
        redisKey = DC_APPNT_PHONE_PREFIX + redisKey;
        String valStr = (String) redisService.get(redisKey);
        return valStr;
    }
    /**
     * @param redisKey redisKey
     * @return 返回值
     */
    public String getAgentPhone(String redisKey) {
        redisKey = DC_LAAGENT_PHONE_PREFIX + redisKey;
        String valStr = (String) redisService.get(redisKey);
        return valStr;
    }


    /**
     * @param tClass   tClass
     * @param redisKey redisKey
     * @param <T>      类型
     * @return 返回值
     */
    public <T> T getEntityPolMsg(Class<T> tClass, String redisKey) {

        List list = new ArrayList();
        redisKey = POLMSG + redisKey;
        String valStr = (String) redisService.get(redisKey);
        return JSON.parseObject(valStr, tClass);

    }

    /**
     * @param redisKey redisKey
     */
    public void deletePolMsg(String redisKey) {

        List list = new ArrayList();
        redisKey = POLMSG + redisKey;
        redisService.delete(redisKey);
    }


    /**
     * @param redisKey redisKey
     * @param entity   entity
     * @param <T>      类型
     * @return 返回值
     */
    public <T> boolean saveEntityPolMsg(String redisKey, T entity,long times) {


        redisKey = POLMSG + redisKey;
        redisService.pSetEx(redisKey, JSON.toJSONString(entity, features),times);
        return true;
    }

    /**
     * 保存实体对象，只保存主键，不保存索引
     *
     * @param entity 实体对象
     * @return 返回值
     */
    private <T> boolean saveOnlyEntity(T entity) {
        Assert.notNull(entity, "save对象不能为空");
        String redisEntityKey = getRedisEntityKey(entity);
        //转换为json串
        redisService.set(redisEntityKey, JSON.toJSONString(entity, features));
        return true;
    }

    /**
     * 保存实体对象并创建索引
     *
     * @param <T>    类型
     * @param entity 实体对象
     * @return 返回值
     */
    public <T> boolean save(T entity) {
        Assert.notNull(entity, "save对象不能为空");
        Map<String, Class> indexKeyMap = getRedisIKeyFNameAndType(entity.getClass());
        if (indexKeyMap.size() > 0) {
            return saveByIndex(entity, indexKeyMap);
        } else {
            return saveOnlyEntity(entity);
        }
    }

    /**
     * 保存实体对象并创建指定的索引
     *
     * @param entity      实体对象
     * @param indexKeyMap 索引属性名称和类型列表
     * @return 返回值
     */
    private <T> boolean saveByIndex(T entity, Map<String, Class> indexKeyMap) {
        Assert.notNull(entity, "save对象不能为空");
        Assert.notEmpty(indexKeyMap, "indexKeys不能为空");

        //得到主键值
        String primaryVal = getPrimaryVal(entity);
        String tableName = PersistentUtil.getTableName(entity.getClass());
        String redisEndityKey = tableName + SPLIT + ENTITY + ATSPLIT + primaryVal;

        //保存索引  优化
        List<String> redisIndexKeys = getRedisIndexKeyList(entity, indexKeyMap);

        //统一提交主键和索引
        redisService.set(redisEndityKey, JSON.toJSONString(entity, features));    //改为键值对结构存储
        for (int i = 0; i < redisIndexKeys.size(); i++) {
            redisService.sset(redisIndexKeys.get(i), primaryVal);
        }
        return true;
    }

    /**
     * 批量保存数据
     *
     * @param entitys 要存储的临时实体对象集合
     */
    /*public <T> boolean batchSave(List<T> entitys) {
        if (entitys == null || entitys.size() <= 0){
			return false;
		}

		for (int i = 0; i < entitys.size(); i++) {
			save(entitys.get(i));
		}
		return true;
	}*/

    /**
     * 批量保存数据
     *
     * @param entities 要存储的实体对象集合
     */
    public boolean batchSave(List entities) {
        return batchSave(entities, redisService);
    }


    /**
     * 批量保存数据
     *
     * @param entities     要存储的实体对象集合
     * @param redisService redisService
     * @return 返回值
     */
    public boolean batchSave(List entities, RedisService redisService) {
        Assert.notEmpty(entities, "实体集合不能为空");

        //List<String> indexKeyList = getRedisIKeyFName(entities.get(0).getClass());	//索引属性名称list
        batchSaveBySpecIndex(entities, null, true, redisService);
        return true;
    }

    /**
     * 批量保存数据
     *
     * @param entities    要存储的临时实体对象集合
     * @param indexKeyMap 索引属性名称和类型列表
     * @param isDiffObj   是否保存不同类型的对象
     */
    private boolean batchSaveBySpecIndex(List<Object> entities, Map<String, Class> indexKeyMap, boolean isDiffObj, RedisService redisService) {
        Assert.notEmpty(entities, "实体集合不能为空");
        if (!isDiffObj) {
            Assert.notEmpty(indexKeyMap, "indexKeys不能为空");
        }

        Map<String, String> primaryEntityMap = new HashMap();        //主键实体Map
        Map<String, List<String>> indexEntityMap = new HashMap();        //索引set集合Map

        //List<String> indexKeyList = getRedisIKeyFName(entities.get(0).getClass());	//索引属性名称list
        for (int i = 0; i < entities.size(); i++) {
            Object entity = entities.get(i);

            //组装主键实体Map
            String tableName = PersistentUtil.getTableName(entity.getClass());    //表名
            String primaryVal = getPrimaryVal(entity);
            String redisEntityKey = tableName + SPLIT + ENTITY + ATSPLIT + primaryVal;
            primaryEntityMap.put(redisEntityKey, JSON.toJSONString(entity, features));

            if (isDiffObj) {
                indexKeyMap = getRedisIKeyFNameAndType(entity.getClass());    //索引属性名称list
            }
            //组装索引List集合Map
            Iterator iter = indexKeyMap.entrySet().iterator();
            while (iter.hasNext()) {
                Map.Entry<String, Class> entry = (Map.Entry) iter.next();
                String indexKeyName = entry.getKey();
                Class indexKeyType = entry.getValue();

                Object val = ReflectUtil.invokeGetterMethod(entity, indexKeyName);
                if (val == null) {
                    continue;
                }
                if ("java.util.Date".equals(indexKeyType.getName())) {
                    if (val instanceof String) {
                        val = DateUtil.get10Date(new FDate().getDate((String) val));
                    } else {
                        val = DateUtil.get10Date((Date) val);
                    }
                }
                String fIValue = val + "";
                //获取索引的redis键
                String redisIndexKey = getRedisIndexKey(entity.getClass(), indexKeyName, fIValue);
                if (indexEntityMap.containsKey(redisIndexKey)) {
                    ((ArrayList) indexEntityMap.get(redisIndexKey)).add(primaryVal);
                } else {
                    List<String> primaryValList = new ArrayList<>();
                    primaryValList.add(primaryVal);
                    indexEntityMap.put(redisIndexKey, primaryValList);
                }
                /*String redisIndexKey = getRedisIndexKey(entity.getClass(),indexKeyName,fIValue);
                indexEntityMap.put(redisIndexKey,primaryVal);*/
            }
        }

        //统一提交redis
        redisService.batchSet(primaryEntityMap);
        Iterator<String> iterator = indexEntityMap.keySet().iterator();
        String key = null;
        List<String> value = null;
        while (iterator.hasNext()) {
            key = iterator.next();
            value = indexEntityMap.get(key);
            redisService.sset(key, value.toArray());
        }
//        redisService.batchSet(indexEntityMap);
        return true;
    }

    /**
     * 根据索引键值对，indexKeyName和indexKeyVal查找实体对象
     *
     * @param <T>          类型
     * @param entityClass  要查询的实体类型
     * @param indexKeyName 实体RedisIndexKey对应的名
     * @param indexKeyVal  实体RedisIndexKey对应的值
     */
    public <T> List<T> findByIndexKey(Class<T> entityClass, String indexKeyName, String indexKeyVal) {
        Assert.notNull(entityClass, "class不能为空");
        Assert.hasText(indexKeyName, "indexKeyName不能为空");
        Assert.hasText(indexKeyVal, "indexKeyVal不能为空");

        //传入的indexKeyName不区分大小写，转换为真实的属性名
        List<String> indexKeyList = getRedisIKeyFName(entityClass);
        String indexRealKeyName = "";
        for (int i = 0; i < indexKeyList.size(); i++) {
            if (indexKeyName.equalsIgnoreCase(indexKeyList.get(i))) {
                indexRealKeyName = indexKeyList.get(i);
                break;
            }
        }

        if ("".equals(indexRealKeyName)) {
            return null;
        }

        String redisKey = PersistentUtil.getTableName(entityClass) + SPLIT + indexRealKeyName + SPLIT + INDEX + ATSPLIT + indexKeyVal;

        /*Set<String> redisEntitySetKeys = redisService.keys(redisKey+"@*");
        if (redisEntitySetKeys == null || redisEntitySetKeys.size() <= 0){
            logger.error("未查到索引对应的索引键列表");
            return null;
        }

        List<String> primaryList = redisService.mget(redisEntityListKeys);
        if(primaryList == null || primaryList.size() <= 0){
            logger.error("未查到索引对应的主键列表");
            return null;
        }*/
        Set<String> redisEntitySetKeys = redisService.sget(redisKey);
        if (redisEntitySetKeys == null || redisEntitySetKeys.size() <= 0) {
//            logger.error("未查到索引对应的主键列表");
            return null;
        }

        List<String> redisEntityListKeys = new ArrayList<>();
        redisEntityListKeys.addAll(redisEntitySetKeys);
        return getEntitys(entityClass, redisEntityListKeys);
    }

    /**
     * 根据索引键值对redisKey和indexKey查找实体对象，如果redis中没有数据，继续查找数据库
     * 如果数据库中存在，则返回的同时加载到redis中
     * 仅限于配置或者定义数据
     *
     * @param <T>          类型
     * @param entityClass  entityClass
     * @param indexKeyName indexKeyName
     * @param indexKeyVal  indexKeyVal
     */
    public <T> List<T> findByIndexKeyRelaDB(Class<T> entityClass, String indexKeyName, String indexKeyVal) {
        List entities = findByIndexKey(entityClass, indexKeyName, indexKeyVal);

        if (entities == null || entities.size() <= 0) {
            //redis中未查到，去数据库中查询
            String tableName = PersistentUtil.getTableName(entityClass);
            StringBuffer whereSql = new StringBuffer();

            //查询索引的属性名称和类型
            Map<String, Class> indexKeyMap = getRedisIKeyFNameAndType(entityClass);
            String indexKeyType = "";
            Map<String, Class> indexRealKey = new HashMap<>();
            Iterator iter = indexKeyMap.entrySet().iterator();
            while (iter.hasNext()) {
                Map.Entry<String, Class> entry = (Map.Entry) iter.next();
                if (indexKeyName.equalsIgnoreCase(entry.getKey())) {
                    indexKeyType = entry.getValue().getName();
                    indexRealKey.put(entry.getKey(), entry.getValue());
                    break;
                }
            }


            MybatisQueryDao commonDao = (MybatisQueryDao) SpringContextUtils.getBeanByClass(MybatisQueryDao.class);
            entities = commonDao.findObjForJdbc(entityClass, indexKeyName, indexKeyType, indexKeyVal);

            if (entities != null && entities.size() > 0 && indexRealKey.size() > 0) {
                //保存redis
                batchSaveBySpecIndex(entities, indexRealKey, false, redisService);
            }
        }

        return entities;
    }

    /**
     * 根据主键删除指定的实体(不建议使用的方法，因为该方法需要先从redis中查询对象之后再删除，建议使用delete方法删除)
     * 删除实体同时需删除相关联的索引，否则根据索引查找数据时会报错
     *
     * @param <T>    Type
     * @param tClass tClass
     * @param id     实体RedisPrimaryHKey注解对应的值
     */
    @Deprecated
    public <T> void deleteEntityById(Class<T> tClass, String id) {
        Assert.notNull(tClass, "class不能为空");
        Assert.hasText(id, "主键不能为空");

        //先查询出当前实体
        T entity = getEntity(tClass, id);
        if (entity == null) {
            logger.error("未查到对应实体");
            return;
        }

        String redisEntityKey = getRedisEntityKey(tClass, id);

        //删除索引
        List<String> indexKeyList = getRedisIKeyFName(tClass);
        List<String> indexRedisKeyList = getRedisIndexKeyList(entity);

        //统一删除主键和索引
        redisService.delete(redisEntityKey);
        for (int i = 0; i < indexRedisKeyList.size(); i++) {
            redisService.sdel(indexRedisKeyList.get(i), id);
        }
//		redisService.delete(indexRedisKeyList);
    }


    /**
     * 根据传入的实体删除对象
     *
     * @param entity entity
     * @param <T>    Type
     */
    public <T> void delete(T entity) {
        Assert.notNull(entity, "delete对象不能为空");

        //得到主键值
        String primaryVal = getPrimaryVal(entity);
        String tableName = PersistentUtil.getTableName(entity.getClass());
        String redisEntityKey = tableName + SPLIT + ENTITY + ATSPLIT + primaryVal;

        //获取索引键列表
        Map<String, Class> indexKeyClass = getRedisIKeyFNameAndType(entity.getClass());
        List<String> indexRedisKeyList = getRedisIndexKeyList(entity, indexKeyClass);

        //统一删除主键和索引
        redisService.delete(redisEntityKey);
        for (int i = 0; i < indexRedisKeyList.size(); i++) {
            redisService.sdel(indexRedisKeyList.get(i), primaryVal);
        }
//        redisService.delete(indexRedisKeyList);
    }

    /**
     * 删除全部的实体(不建议使用的方法，因为该方法需要先从redis中查询对象之后再删除，建议使用deleteEntities方法删除)
     *
     * @param <T>    Type
     * @param tClass tClass
     * @param ids    实体RedisPrimaryHKey注解对应的值list
     */
    @Deprecated
    public <T> void deleteEntities(Class<T> tClass, List<String> ids) {
        Assert.notNull(tClass, "class不能为空");
        Assert.notEmpty(ids, "主键list不能为空");

        for (String id : ids) {
            deleteEntityById(tClass, id);
        }
    }


    /**
     * 删除全部的实体
     *
     * @param entities     要删除的实体对象集合
     * @param redisService redisService
     */
    public void deleteEntities(List entities, RedisService redisService) {
        Assert.notEmpty(entities, "实体集合不能为空");

        List<String> redisEntityKeyList = new ArrayList<String>();        //redis实体键List
        Map<String, List<String>> indexEntityMap = new HashMap();        //索引set集合Map
        List<String> redisIndexKeyList = new ArrayList();        //索引set集合Map
        Map<String, Class> indexKeyMap = new HashMap();        //索引set集合Map

        for (int i = 0; i < entities.size(); i++) {
            Object entity = entities.get(i);

            //组装主键实体Map
            String tableName = PersistentUtil.getTableName(entities.get(0).getClass());    //表名
            String primaryVal = getPrimaryVal(entity);
            String redisEntityKey = tableName + SPLIT + ENTITY + ATSPLIT + primaryVal;
            redisEntityKeyList.add(redisEntityKey);

            //组装索引List集合Map
            indexKeyMap = getRedisIKeyFNameAndType(entity.getClass());    //索引属性名称list
            Iterator iter = indexKeyMap.entrySet().iterator();
            while (iter.hasNext()) {
                Map.Entry<String, Class> entry = (Map.Entry) iter.next();
                String indexKeyName = entry.getKey();
                Class indexKeyType = entry.getValue();
                Object val = ReflectUtil.invokeGetterMethod(entity, indexKeyName);

                if (val == null) {
                    continue;
                }
                if ("java.util.Date".equals(indexKeyType.getName())) {
                    if (val instanceof String) {
                        val = DateUtil.get10Date(new FDate().getDate((String) val));
                    } else {
                        val = DateUtil.get10Date((Date) val);
                    }
                }
                String fIValue = val + "";

                //获取索引的redis键
                String redisIndexKey = getRedisIndexKey(entity.getClass(), indexKeyName, fIValue);
                if (indexEntityMap.containsKey(redisIndexKey)) {
                    ((ArrayList) indexEntityMap.get(redisIndexKey)).add(primaryVal);
                } else {
                    List<String> primaryValList2 = new ArrayList<>();
                    primaryValList2.add(primaryVal);
                    indexEntityMap.put(redisIndexKey, primaryValList2);
                }
            }
        }

        //统一提交redis
        redisService.delete(redisEntityKeyList);
        Iterator<String> iterator = indexEntityMap.keySet().iterator();
        String key = null;
        List<String> value = null;
        while (iterator.hasNext()) {
            key = iterator.next();
            value = indexEntityMap.get(key);
            redisService.sdel(key, value.toArray());
        }
//        redisService.delete(redisIndexKeyList);
    }

    /**
     * 删除全部的实体
     *
     * @param entities 要删除的实体对象集合
     */
    public void deleteEntities(List entities) {
        deleteEntities(entities, redisService);
    }

    /**
     * 类似传统核心pubsubmit的方式，统一管理事务，统一提交数据
     *
     * @param cInputData 传入的POJO对象集合，如果单条则传入POJO对象，如果多条传入ArrayList<POJO>列表对象
     * @return
     */
    public boolean submitData(VData cInputData) {
        return submitData(cInputData, true);
    }

    /**
     * 类似传统核心pubsubmit的方式，统一管理事务，统一提交数据（处理过程中不做watch，即不做事务隔离）
     *
     * @param cInputData 传入的POJO对象集合，如果单条则传入POJO对象，如果多条传入ArrayList<POJO>列表对象
     * @return
     */
    public boolean submitDataNoWatch(VData cInputData) {
        return submitData(cInputData, false);
    }

    /**
     * 统一提交数据方法
     *
     * @param cInputData
     * @param isNeedWatch 是否需要开启事务的watch，true-开启watch，false-不开启watch
     * @return 返回值
     */
    private boolean submitData(VData cInputData, boolean isNeedWatch) {
        String action = ""; // 操作方式，INSERT\UPDATE\DELETE
        Object o = null; // Schema或Set对象
        String className = ""; // 类名
        try {
            VData mInputData = (VData) cInputData.clone();
            // 通过MMap来传递每个Schema或Set的数据库操作方式，约定使用
            MMap map = (MMap) mInputData.getObjectByObjectName("MMap", 0);
            List<String> redisEntityKeyList = new ArrayList<>();
            Set<String> redisIndexKeySet = new HashSet<>();
            List<Object> setEntities = new ArrayList<>();
            List<Object> delEntities = new ArrayList<>();
            if (map != null && map.keySet().size() != 0) {
                Set set = map.keySet();
                for (int i = 0; i < set.size(); i++) {
                    // 获取操作对象Schema或Set或SQL
                    o = map.getOrder().get(String.valueOf(i + 1));
                    // 获取操作方式
                    action = (String) map.get(o);
                    if (action == null) {
                        continue;
                    }
                    // 构造相应的DB类名
                    className = o.getClass().getName();
                    String sDelOrSetFlag = "NotSupport";
                    if (action.equals("INSERT") || action.equals("UPDATE") || action.equals("DELETE&INSERT")) {
                        sDelOrSetFlag = "BatchSet";
                    } else if (action.equals("DELETE")) {
                        sDelOrSetFlag = "BatchDel";
                    }
                    //获取所有需要watch的键
                    if (isNeedWatch) {
                        if (o instanceof List) {
                            //集合类型
                            List pojos = (List) o;
                            for (int j = 0; j < pojos.size(); j++) {
                                Object object = pojos.get(j);
                                //获取实体键
                                if (object == null) {
                                    logger.error("object is null,第" + j + "次循环");
                                }
                                String redisEntityKey = getRedisEntityKey(object);
                                redisEntityKeyList.add(redisEntityKey);

                                //获取索引键list
                                redisIndexKeySet.addAll(getRedisIndexKeyList(object));
                            }
                        } else {
                            //pojo类型
                            String redisEntityKey = getRedisEntityKey(o);
                            redisEntityKeyList.add(redisEntityKey);

                            redisIndexKeySet.addAll(getRedisIndexKeyList(o));
                        }
                    }
                    //获取实体
                    if (o instanceof List) {
                        //集合类型
                        List pojos = (List) o;
                        if (sDelOrSetFlag.equals("BatchSet")) {
                            setEntities.addAll(pojos);
                        } else if (sDelOrSetFlag.equals("BatchDel")) {
                            delEntities.addAll(pojos);
                        }
                    } else {
                        //pojo类型
                        if (sDelOrSetFlag.equals("BatchSet")) {
                            setEntities.add(o);
                        } else if (sDelOrSetFlag.equals("BatchDel")) {
                            delEntities.add(o);
                        }
                    }
                }
            }
//            disposeTransaction(setKVMap, delKVMap);
            //事务处理单独创建新的redisservice，否则可能会有事务冲突，并且需要获取单独的redisTemplate
            //RedisTemplate redisTemplateTrans = (RedisTemplate)SpringContextUtils.getBeanById("redisTemplateTrans");
            RedisService redisServiceTrans = new RedisService(redisTemplateTrans);
            if (isNeedWatch) {
                if (redisEntityKeyList.size() > 0) {
                    redisServiceTrans.watch(redisEntityKeyList);
                }
                if (redisIndexKeySet.size() > 0) {
                    redisServiceTrans.watch(redisIndexKeySet);
                }
            }
            redisServiceTrans.beginTx();
            if (setEntities.size() > 0) {
                this.batchSave(setEntities, redisServiceTrans);
            }
            if (delEntities.size() > 0) {
                this.deleteEntities(delEntities, redisServiceTrans);
            }
            if (redisServiceTrans.endTx() == null) {
                logger.error("事务提交失败！");
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
            // @@错误处理
            return false;
        }
        return true;
    }

    /**
     * 按表名删除redis中数据，同时删除实体部分及索引部分
     * 该方法只针对定义或配置表信息，也即在mysql数据库中有实体表存在
     * 当mysql数据库中表更 新之后，需将当前表的数据从redis中移除掉
     *
     * @param tableNames tableNames
     * @return 返回值
     */


    public boolean deleteByTableName(String... tableNames) {
        Assert.notEmpty(tableNames, "表名不能为空");
        List<String> redisKeyList = new ArrayList<>();
        Set<String> redisKeySet = new HashSet<>();

        //事务处理单独创建新的redisservice，否则可能会有事务冲突，并且需要获取单独的redisTemplate
        //RedisTemplate redisTemplateTrans = (RedisTemplate)SpringContextUtils.getBeanById("redisTemplateTrans");
        RedisService redisServiceTrans = new RedisService(redisTemplateTrans);

        for (int i = 0; i < tableNames.length; i++) {
            String tableName = tableNames[i];
            if (StringUtils.isBlank(tableName)) {
                continue;
            }
            //转换为小写
            tableName = tableName.toLowerCase();

            //实体键
            List<String> redisEntityListKeys = redisServiceTrans.scan(tableName + SPLIT + ENTITY + ATSPLIT + "*", 10000);
            if (redisEntityListKeys != null && redisEntityListKeys.size() > 0) {
                redisKeyList.addAll(redisEntityListKeys);
            }

            //索引键
            List<String> redisIndexListKeys = redisServiceTrans.scan(tableName + SPLIT + "*" + SPLIT + INDEX + ATSPLIT + "*", 10000);
            if (redisIndexListKeys != null && redisIndexListKeys.size() > 0) {
                redisKeyList.addAll(redisIndexListKeys);
            }
        }

//        if(redisKeySet.size() > 0){
//            redisKeyList.addAll(redisKeySet);
//        }

        try {
            redisServiceTrans.watch(redisKeyList);
            redisServiceTrans.beginTx();

            redisServiceTrans.delete(redisKeyList);

            if (redisServiceTrans.endTx() == null) {
                logger.error("事务提交失败！");
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }


    /**
     * 获取redis中索引数据的键
     *
     * @param eClass       eClass
     * @param indexKeyName indexKeyName
     * @param indexKeyVal  indexKeyVal
     * @return 返回值
     */
    private String getRedisIndexKey(Class eClass, String indexKeyName, String indexKeyVal) {
        String tableName = PersistentUtil.getTableName(eClass);

        /*List<SortableField> sortableFields = getRedisPKeySortFields(eClass);
        //创建索引
        //获得拼接的主键名
        String fPName = "";
        for (int j = 0; j < sortableFields.size(); j++) {
            fPName += sortableFields.get(j).getName()+SPLIT;
        }
        fPName = fPName.substring(0,fPName.lastIndexOf(SPLIT));*/
        return tableName + SPLIT + indexKeyName + SPLIT + INDEX + ATSPLIT + indexKeyVal;
    }


    /**
     * 重载方法，获取redis索引结构数据键List
     *
     * @param entity      entity
     * @param indexKeyMap indexKeyMap
     * @return 返回值
     */
    private List<String> getRedisIndexKeyList(Object entity, Map<String, Class> indexKeyMap) {
        //List<String> indexKeyList = getRedisIKeyFName(entity.getClass());
        List<String> redisIndexKeyList = new ArrayList<>();
        Iterator iter = indexKeyMap.entrySet().iterator();
        while (iter.hasNext()) {
            Map.Entry<String, Class> entry = (Map.Entry) iter.next();
            String indexKeyName = entry.getKey();
            Object val = ReflectUtil.invokeGetterMethod(entity, indexKeyName);
            if (val == null) {
                continue;
            }
            if ("java.util.Date".equals(entry.getValue().getName())) {
                if (val instanceof String) {
                    val = DateUtil.get10Date(new FDate().getDate((String) val));
                } else {
                    val = DateUtil.get10Date((Date) val);
                }
            }
            //全部转换成字符类型
            String fIValue = val + "";

            String indexRedisKey = getRedisIndexKey(entity.getClass(), indexKeyName, fIValue);
            redisIndexKeyList.add(indexRedisKey);
        }

        return redisIndexKeyList;
    }

    //获取redis索引结构数据键List
    /*private List<String> getRedisIndexKeyList(Object entity, Map<String,Class> indexKeyMap){
        //List<String> indexKeyList = getRedisIKeyFName(entity.getClass());
        String primaryVal = getPrimaryVal(entity);
		return getRedisIndexKeyList(entity,indexKeyMap,primaryVal);
	}*/

    //

    /**
     * 重载方法，获取redis索引结构数据键List
     *
     * @param entity entity
     * @return 返回值
     */
    private List<String> getRedisIndexKeyList(Object entity) {
        Map<String, Class> indexKeyMap = getRedisIKeyFNameAndType(entity.getClass());
        return getRedisIndexKeyList(entity, indexKeyMap);
    }

    /**
     * @param eClass eClass
     * @return 返回值
     */
    private List<SortableField> getRedisPKeySortFields(Class eClass) {
        List<SortableField> sortableFields = new ArrayList<SortableField>();
        logger.debug("eClass.getName():" + eClass.getName());
        Field[] fields = eClass.getDeclaredFields();
        for (Field field : fields) {
            if (field.isAnnotationPresent(RedisPrimaryHKey.class)) {
                RedisPrimaryHKey primaryHKey = field.getAnnotation(RedisPrimaryHKey.class);
                SortableField sf = new SortableField(primaryHKey, field);
                sortableFields.add(sf);
            }
        }

        if (sortableFields.size() <= 0) {
            throw new IllegalArgumentException("未设置主键");
        }

        //大于一个主键属性才进行比较
        if (sortableFields.size() > 1) {
            //将主键字段按顺序排序
            Collections.sort(sortableFields, new Comparator<SortableField>() {
                @Override
                public int compare(SortableField o1, SortableField o2) {
                    return o1.getPrimaryHKey().order() - o2.getPrimaryHKey().order();
                }
            });
        }

        return sortableFields;
    }

    /**
     * 获得redis实体类索引注解的属性类型
     *
     * @param eClass       eClass
     * @param indexKeyName indexKeyName
     * @return 返回值
     */
    private String getRedisIKeyFType(Class eClass, String indexKeyName) {
        if (!"".equals(indexKeyName)) {
            List<String> indexTypeList = new ArrayList<String>();
            Field[] fields = eClass.getDeclaredFields();
            for (Field field : fields) {
                if (field.isAnnotationPresent(RedisIndexHKey.class)) {
                    if (indexKeyName.equalsIgnoreCase(field.getName())) {
                        return field.getType().getName();
                    }
                }
            }
        }
        return "";
    }

    /**
     * 查找实体中带索引注解的属性名
     *
     * @param eClass eClass
     * @return 返回值
     */
    private List<String> getRedisIKeyFName(Class eClass) {
        List<String> indexKeyList = new ArrayList<String>();
        Field[] fields = eClass.getDeclaredFields();
        for (Field field : fields) {
            if (field.isAnnotationPresent(RedisIndexHKey.class)) {
                indexKeyList.add(field.getName());
            }
        }
        return indexKeyList;
    }

    /**
     * 查找实体中带索引注解的属性名和类型
     *
     * @param eClass eClass
     * @return 返回值
     */
    private Map<String, Class> getRedisIKeyFNameAndType(Class eClass) {
        Map<String, Class> indexKeyMap = new HashMap<>();
        Field[] fields = eClass.getDeclaredFields();
        for (Field field : fields) {
            if (field.isAnnotationPresent(RedisIndexHKey.class)) {
                indexKeyMap.put(field.getName(), field.getType());
            }
        }
        return indexKeyMap;
    }


    //

    /**
     * 获取实体主键名称
     *
     * @param sortableFields sortableFields
     * @return 返回值
     */
    private String getPrimaryName(List<SortableField> sortableFields) {
        String primaryName = "";
        for (int i = 0; i < sortableFields.size(); i++) {
            String fName = sortableFields.get(i).getName();
            primaryName += primaryName + PKSPLIT;
        }
        return primaryName.substring(0, primaryName.lastIndexOf(PKSPLIT));
    }

    //

    /**
     * 重载方法，获取实体主键名称
     *
     * @param entityClass entityClass
     * @return 返回值
     */
    private String getPrimaryName(Class entityClass) {
        List<SortableField> sortableFields = getRedisPKeySortFields(entityClass);
        String primaryName = "";
        for (int i = 0; i < sortableFields.size(); i++) {
            String fName = sortableFields.get(i).getName();
            primaryName += primaryName + PKSPLIT;
        }
        return primaryName.substring(0, primaryName.lastIndexOf(PKSPLIT));
    }

    //

    /**
     * 获取实体主键名称和值
     *
     * @param sortableFields sortableFields
     * @param entity         entity
     * @return 返回值
     */
    private String getPrimaryVal(List<SortableField> sortableFields, Object entity) {
        String primaryVal = "";
        for (int i = 0; i < sortableFields.size(); i++) {
            String fName = sortableFields.get(i).getName();
            String fType = sortableFields.get(i).getType().getName();
            //是否需要做类型转换？
            Object val = ReflectUtil.invokeGetterMethod(entity, fName);
            if (val == null) {
                throw new IllegalArgumentException("未获得主键值");
            }
            if ("java.util.Date".equals(fType)) {
                if (val instanceof String) {
                    val = DateUtil.get10Date(new FDate().getDate((String) val));
                } else {
                    val = DateUtil.get10Date((Date) val);
                }
            }
            String fValue = val + "";
            if (StringUtils.isBlank(fValue)) {
                throw new IllegalArgumentException("未获得主键值");
            }
            primaryVal += fValue + PKSPLIT;
        }
        primaryVal = primaryVal.substring(0, primaryVal.lastIndexOf(PKSPLIT));
        return primaryVal;
    }


    /**
     * 获取实体主键名称和值
     *
     * @param entity entity
     * @return 返回值
     */
    public String getPrimaryValByRedisPrimaryHKeyAnnotation(Object entity) {
        String primaryVal = "";
        List<SortableField> sortableFields = getRedisPKeySortFields(entity.getClass());
        return getPrimaryVal(sortableFields, entity);
    }


    /**
     * 重载方法，获取实体主键名称和值
     *
     * @param entity entity
     * @return 返回值
     */
    private String getPrimaryVal(Object entity) {
        String primaryVal = "";
        List<SortableField> sortableFields = getRedisPKeySortFields(entity.getClass());
        return getPrimaryVal(sortableFields, entity);
    }


    /**
     * 获取实体主键名称和值
     *
     * @param sortableFields sortableFields
     * @param entity         entity
     * @return 返回值
     */
    private String[] getPrimaryNameAndVals(List<SortableField> sortableFields, Object entity) {
        String primaryVal = "";
        String primaryName = "";
        for (int i = 0; i < sortableFields.size(); i++) {
            String fName = sortableFields.get(i).getName();
            //是否需要做类型转换？
            String fValue = ReflectUtil.invokeGetterMethod(entity, fName) + "";
            if (StringUtils.isBlank(fValue)) {
                throw new IllegalArgumentException("未获得主键值");
            }
            primaryName += primaryName + PKSPLIT;
            primaryVal += fValue + PKSPLIT;
        }
        primaryName = primaryName.substring(0, primaryName.lastIndexOf(PKSPLIT));
        primaryVal = primaryVal.substring(0, primaryVal.lastIndexOf(PKSPLIT));
        return new String[]{primaryName, primaryVal};
    }


    /**
     * 重载方法，获取实体主键名称和值
     *
     * @param entity entity
     * @return 返回值
     */
    private String[] getPrimaryNameAndVals(Object entity) {
        List<SortableField> sortableFields = getRedisPKeySortFields(entity.getClass());
        return getPrimaryNameAndVals(sortableFields, entity);
    }

    /**
     * 获取redis实体对象的key，也即主键对应的key
     *
     * @param tClass     tClass
     * @param primaryVal primaryVal
     * @return 返回值
     */
    private String getRedisEntityKey(Class tClass, String primaryVal) {
        //String primaryName = getPrimaryName(tClass);
        String tableName = PersistentUtil.getTableName(tClass);
        return tableName + SPLIT + ENTITY + ATSPLIT + primaryVal;
    }


    /**
     * 重载方法，获取redis实体对象的key，也即主键对应的key
     *
     * @param entity entity
     * @return 返回值
     */
    private String getRedisEntityKey(Object entity) {
        //得到主键名称和值
        List<SortableField> sortableFields = getRedisPKeySortFields(entity.getClass());
//		String[] primaryNameAndVal = getPrimaryNameAndVals(sortableFields,entity);
//		String primaryName = primaryNameAndVal[0];
//		String primaryVal = primaryNameAndVal[1];
        String primaryVal = getPrimaryVal(sortableFields, entity);

        String tableName = PersistentUtil.getTableName(entity.getClass());
        return tableName + SPLIT + ENTITY + ATSPLIT + primaryVal;
    }


    /**
     * 获取redis实体对象的key列表，也即主键对应的key列表
     *
     * @param tClass      tClass
     * @param primaryVals primaryVals
     * @return 返回值
     */
    private List<String> getRedisEntityKeys(Class tClass, List<String> primaryVals) {
        List<String> redisEntityKeys = new ArrayList<>();
        //String primaryName = getPrimaryName(tClass);
        String tableName = PersistentUtil.getTableName(tClass);
        for (int i = 0; i < primaryVals.size(); i++) {
            redisEntityKeys.add(tableName + SPLIT + ENTITY + ATSPLIT + primaryVals.get(i));
        }
        return redisEntityKeys;
    }

    /**
     * 根据表名、联合主键查询单个字段值
     *
     * @param tableName 费率表名
     * @param map       限制条件
     * @param cName     cName
     * @return 返回值
     */
    public double getEntitySingleField(String tableName, Map<String, String> map, String cName) {
        StringBuffer whereSql = new StringBuffer();
        String redisKey = "";
        String redisEntityKey = tableName + SPLIT + ENTITY + ATSPLIT;
        for (String key : map.keySet()) {
            redisKey = redisKey + map.get(key) + PKSPLIT;
        }
        redisEntityKey = redisEntityKey + redisKey.substring(0, redisKey.length() - 3);
        String value = (String) redisService.get(redisEntityKey);
        logger.info("查询redis得到的值：" + value);
        if (value == null) {
            logger.info("redis中未查到值，查询数据库");
            MybatisQueryDao commonDao = (MybatisQueryDao) SpringContextUtils.getBeanByClass(MybatisQueryDao.class);
            List<Map<String, Object>> listMaps = commonDao.findForJdbc(tableName, map, cName);
            if (listMaps != null && listMaps.size() > 0) {
                value = listMaps.get(0).get(cName).toString();
                if (value != null) {
                    //保存redis，只保存主键不保存索引
                    redisService.set(redisEntityKey, value);
                }
            }
        }
        if (value == null) {
            value = "0.0";
        }
        double val = Double.parseDouble(value);
        logger.info("返回的值：" + val);
        return val;
    }


    //

    /**
     * 此方法只供健康险风险保额查询
     *
     * @param tClass   tClass
     * @param redisKey redisKey
     * @param hashKey  hashKey
     * @param <T>      Type
     * @return 返回值
     */
    public <T> List<T> getHealthRiskAmntEntity(Class<T> tClass, String redisKey, String hashKey) {

        List list = new ArrayList();
        list = (List) redisService.hget(redisKey, hashKey);

        List<T> valList = new ArrayList<>();
        if (list != null) {
            for (int i = 0; i < list.size(); i++) {
                String val = (String) list.get(i);
                valList.add(JSON.parseObject(val, tClass));
            }
        }

        return valList;
    }

    //

    /**
     * 此方法只供健康险风险保额保存
     *
     * @param redisKey redisKey
     * @param hashKey  hashKey
     * @param entities entities
     * @return 返回值
     */
    public boolean saveHealthRiskAmntEntity(String redisKey, String hashKey, List entities) {
        List<String> entityStrList = new ArrayList<>();
        for (int i = 0; i < entities.size(); i++) {
            Object entity = entities.get(i);
            entityStrList.add(JSON.toJSONString(entity, features));
        }
        redisService.hset(redisKey, hashKey, entityStrList);
        return true;
    }

    /**
     * 自定义存储POJO集合对象
     *
     * @param redisKey redisKey
     * @param entities entities
     * @return 返回值
     */
    public boolean savePojoList(String redisKey, List entities) {

        redisKey = UW_CHECK_LIST+ redisKey;
        List<Object> entityStrList = new ArrayList<>();
        for (int i = 0; i < entities.size(); i++) {
            Object entity = entities.get(i);
            entityStrList.add(entity);
        }
        redisService.set(redisKey, JSONObject.toJSONString(entityStrList));
        return true;
    }

    //

    /**
     *
     * 自定义取值POJO集合对象
     * @param tClass   tClass
     * @param redisKey redisKey
     * @param <T>      类型
     * @return 返回值
     */
    public <T> List<T> getPojoList(Class<T> tClass, String redisKey) {
        List list = new ArrayList();
        redisKey = UW_CHECK_LIST + redisKey;
        String str = (String) redisService.get(redisKey);
        if(str==null){
            list= null;
        } else if(str!=null&&str.length()==0){

        }  else if(!StringUtils.isEmpty(str)){
            list= JSONObject.parseArray(str,tClass);
        }
        return list;
    }

    /**
     * 自定义存储单个POJO对象
     *
     * @param redisKey redisKey
     * @param object object
     * @return 返回值
     */
    public boolean savePojo(String redisKey, Object object) {
        redisKey = UW_CHECK_OBJ+ redisKey;
        redisService.set(redisKey, JSONObject.toJSONString(object));
        return true;
    }


    /**
     *
     * 自定义取值单个POJO对象
     * @param tClass   tClass
     * @param redisKey redisKey
     * @param
     * @return 返回值
     */
    public <T>Object getPojo(Class<T> tClass, String redisKey) {
        Object object= new Object();
        redisKey = UW_CHECK_OBJ + redisKey;
        String str = (String) redisService.get(redisKey);
        if(str==null){
            object= null;
        } else if(str!=null&&str.length()==0){

        }  else if(!StringUtils.isEmpty(str)){
            object=JSONObject.parseObject(str,tClass);
        }
        return object;
    }

    //自定义存储单个字符串
    public boolean saveOnlyString(String redisKey, String primaryVal) {


        redisKey = UW_CHECK_STRING+ redisKey;
        redisService.set(redisKey, primaryVal);

        return true;
    }

    public String getOnlyString(String redisKey) {

        redisKey = UW_CHECK_STRING + redisKey;
        String value = (String) redisService.get(redisKey);
        return value;
    }
}
