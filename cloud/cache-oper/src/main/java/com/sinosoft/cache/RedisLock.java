package com.sinosoft.cache;

import com.sinosoft.cloud.cache.RedisService;
import com.sinosoft.cloud.common.SpringContextUtils;


import java.util.Random;
import java.util.concurrent.TimeUnit;

/**
 * Created by yangming on 16/7/27.
 */
public class RedisLock {


    /**
     * 加锁标志
     */
    private static final String LOCKED = "TRUE";
    private static final String LOCK_POSTFIX = "_LOCK";
    /**
     * 毫秒与毫微秒的换算单位 1毫秒 = 1000000毫微秒
     */
    private static final long MILLI_NANO_CONVERSION = 1000 * 1000L;
    /**
     * 默认超时时间（毫秒）
     */
    private static final long DEFAULT_TIME_OUT = 1000;
    /**
     * 锁的超时时间（秒），过期删除
     */
    private static final int EXPIRE = 3 * 60;
    private final Random random = new Random();


    private String name;
    private RedisService redisService;

    public RedisLock(String name) {
        this.name = name + LOCK_POSTFIX;
        redisService = SpringContextUtils.getBeanByClass(RedisService.class);
    }

    /**
     * 加锁
     * 应该以：
     * lock();
     * try {
     * doSomething();
     * } finally {
     * unlock()；
     * }
     * 的方式调用
     *
     * @return 成功或失败标志
     */
    public boolean lock() {
        return lock(DEFAULT_TIME_OUT);
    }

    /**
     * 加锁
     * 应该以：
     * lock();
     * try {
     * doSomething();
     * } finally {
     * unlock()；
     * }
     * 的方式调用
     *
     * @param timeout 超时时间
     * @return 成功或失败标志
     */
    public boolean lock(long timeout) {
        long nano = System.nanoTime();
        timeout *= MILLI_NANO_CONVERSION;
        try {
            //默认1000毫秒,超时
            int x = 0;
            while ((System.nanoTime() - nano) < timeout) {
//                System.out.println("看看循环多少次?"+x++);

                if (this.redisService.setNX(this.name, LOCKED)) {
                    //默认180秒内没有手动释放,将会自动释放;
                    this.redisService.getRedisTemplate().expire(this.name, EXPIRE, TimeUnit.SECONDS);
                    return true;
                }
                // 短暂休眠，避免出现活锁
                Thread.sleep(3, random.nextInt(500));
            }
        } catch (Exception e) {
            throw new RuntimeException("Locking error", e);
        }
        return false;
    }


    /**
     * 解锁
     * 无论是否加锁成功，都需要调用unlock
     * 应该以：
     * lock();
     * try {
     * doSomething();
     * } finally {
     * unlock()；
     * }
     * 的方式调用
     */
    public void unlock() {
        this.redisService.getRedisTemplate().delete(this.name);
    }
}
