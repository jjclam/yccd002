package com.sinosoft.cache;


import com.sinosoft.cloud.cache.RedisService;
import com.sinosoft.cloud.common.SpringContextUtils;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.utility.*;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.HashMap;
import java.util.Set;

/**
 * 内存数据库事务批量提交公共处理类(仅支持提交SCHEMA或者SCHEMASET)
 * Created by filon51 on 2016/8/1.
 */
public class PubMemDBSubmit {

    private static Log logger = LogFactory.getLog(PubMemDBSubmit.class);
    // 传输数据类
    private VData mInputData;
    /**
     * 错误处理类，每个需要错误处理的类中都放置该类
     */
    private CErrors mErrors = new CErrors();

    private RedisService mRedisService;

    public PubMemDBSubmit() {
        mRedisService = SpringContextUtils.getBeanByClass(RedisService.class);
    }

    /**
     * 传输数据的公共方法 传入数据
     *
     * @param cInputData VData 数据操作标示
     * @param cOperate   String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate) {
        // 首先将数据在本类中做一个备份
        mInputData = (VData) cInputData.clone();

        if (!this.saveData()) {
            return false;
        }
        mInputData = null;
        return true;
    }

    public boolean submitData(VData cInputData) {
        return submitData(cInputData, null);
    }

    /**
     * 数据库操作
     *
     * @return: boolean
     */
    private boolean saveData() {

        String action = ""; // 操作方式，INSERT\UPDATE\DELETE
        Object o = null; // Schema或Set对象
        String className = ""; // 类名

        try {
            HashMap<String, Schema> setKVMap = new HashMap<String, Schema>();
            HashMap<String, Schema> delKVMap = new HashMap<String, Schema>();
            MemDBOper tDBOper = new MemDBOper();

            // 通过MMap来传递每个Schema或Set的数据库操作方式，约定使用
            MMap map = (MMap) mInputData.getObjectByObjectName("MMap", 0);
            if (map != null && map.keySet().size() != 0) {
                Set set = map.keySet();
                for (int i = 0; i < set.size(); i++) {
                    // 获取操作对象Schema或Set或SQL
                    // o = iterator.next();
                    o = map.getOrder().get(String.valueOf(i + 1));
                    // 获取操作方式
                    action = (String) map.get(o);
                    if (action == null) {
                        continue;
                    }
                    // 构造相应的DB类名
                    className = o.getClass().getName();
//                    logger.info("对象为：" + o);
//                    logger.info("类名为：" + className);
//                    logger.info("操作方式为：" + action);
                    String sDelOrSetFlag = "NotSupport";
                    if (action.equals("INSERT") || action.equals("UPDATE") || action.equals("DELETE&INSERT")) {
                        sDelOrSetFlag = "BatchSet";
                    } else if (action.equals("DELETE")) {
                        sDelOrSetFlag = "BatchDel";
                    }

                    if (className.endsWith("Schema")) {
                        if (sDelOrSetFlag.equals("BatchSet")) {
                            setKVMap.put(tDBOper.getMemDBKey((Schema) o), (Schema) o);
                        } else if (sDelOrSetFlag.equals("BatchDel")) {
                            delKVMap.put(tDBOper.getMemDBKey((Schema) o), (Schema) o);
                        }
                    } else if (className.endsWith("Set")) {
                        SchemaSet tSchemaSet = (SchemaSet) o;
                        if (sDelOrSetFlag.equals("BatchSet")) {
                            for (int j = 1; j <= tSchemaSet.size(); j++) {
                                Schema aSchema = (Schema) tSchemaSet.getObj(j);
                                setKVMap.put(tDBOper.getMemDBKey(aSchema), aSchema);
                            }
                        } else if (sDelOrSetFlag.equals("BatchDel")) {
                            for (int j = 1; j <= tSchemaSet.size(); j++) {
                                Schema aSchema = (Schema) tSchemaSet.getObj(j);
                                delKVMap.put(tDBOper.getMemDBKey(aSchema), aSchema);
                            }
                        }
                    }
                }
            }
//            disposeTransaction(setKVMap, delKVMap);

            mRedisService = new RedisService();
            if (setKVMap.size() != 0) {
                mRedisService.watch(setKVMap);
            }
            if (delKVMap.size() != 0) {
                mRedisService.watch(delKVMap);
            }
            mRedisService.beginTx();
            if (setKVMap.size() != 0) {
                mRedisService.batchSet(setKVMap);
            }
            if (delKVMap.size() != 0) {
                mRedisService.batchDel(delKVMap);
            }
            if (mRedisService.endTx() == null) {
                logger.error("事务提交失败！");
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
            // @@错误处理
            CError.buildErr(this, e.toString());
            return false;
        }
        return true;
    }

//    private void disposeTransaction(HashMap<String, Schema> setKVMap, HashMap<String, Schema> delKVMap) {

//        mRedisService = new RedisService();
//        if(setKVMap.size() != 0) {
//            mRedisService.batchSet(setKVMap);
//        }
//        if(delKVMap.size() != 0) {
//            mRedisService.batchDel(delKVMap);
//        }
//    }
}
