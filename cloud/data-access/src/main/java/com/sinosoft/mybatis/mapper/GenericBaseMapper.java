package com.sinosoft.mybatis.mapper;


import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * 类描述：DAO层泛型基类接口
 *
 * @version 1.0
 * @date： 日期：2016-8-10 时间：下午05:37:33
 */
@Mapper
public interface GenericBaseMapper {
    /**
     * 通用sql单表查询
     *
     * @param param 参数
     * @return 返回值集合
     */
    List<Map<String, Object>> selectAdvanced(Map<String, Object> param);
}
