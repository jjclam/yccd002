package com.sinosoft.mybatis.utility;

import com.sinosoft.cloud.common.SpringContextUtils;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.HashMap;
import java.util.List;


/**
 * Created by yangming on 16/8/10.
 * @param <T> 类型
 */
public class BatisSQL<T> {

    /**
     * 获取sqlSession
     */
    public BatisSQL() {
        if (sqlSession == null) {
            sqlSession = SpringContextUtils.getBeanByClass(SqlSession.class);
        }
    }

    /**
     * 开启事务
     */
    public void beginTx() {
        if (sqlSessionFactory == null) {
            sqlSessionFactory = SpringContextUtils.getBeanByClass(SqlSessionFactory.class);
        }
        sqlSession = sqlSessionFactory.openSession(false);
    }

    /**
     * 关闭事务
     */
    public void endTx() {
        sqlSession.commit();
    }

    @Autowired
    private SqlSession sqlSession;

    @Autowired
    private SqlSessionFactory sqlSessionFactory;


    /**
     * sql id 是对应sqlinfo 目录下 xml 中描述的  namespace.id  对应的值
     *
     * @param sqlId sqlid
     * @param params param
     *
     * @return 返回值
     */
    public T getOneValue(String sqlId, HashMap<String, String> params) {
        return sqlSession.selectOne(sqlId, params);
    }

    /**
     * 用法同getOneValue,只是返回多条
     *
     * @param sqlId sqlid
     * @param params param
     * @return 返回值
     */
    public List<T> execSQL(String sqlId, HashMap<String, String> params) {
        return sqlSession.selectList(sqlId, params);
    }

    /**
     * 执行update语句,返回值是执行update的行数。
     *
     * @param sqlId sqlId
     * @param params 参数
     * @return 返回值
     */
    public int execUpdateSQL(String sqlId, HashMap<String, String> params) {
        return sqlSession.update(sqlId, params);
    }

    ///**
    // * 执行insert语句,返回值是执行insert的行数。
    // *
    // * @param sqlId
    // * @param params
    // * @return
    // */
    //public int execInsertSQL(String sqlId, Schema params) {
    //    return sqlSession.insert(sqlId, params);
    //}

    /**
     * 执行insert语句,返回值是执行insert的行数。
     *
     * @param sqlId sqldio
     * @param params 参数
     * @return 返回值
     */
    public int execInsertSQL(String sqlId, HashMap<String, String> params) {
        return sqlSession.insert(sqlId, params);
    }

    /**
     * 执行delete语句,返回值是执行delete的行数。
     *
     * @param sqlId sqlid
     * @param params param
     * @return 返回值
     */
    public int execDeleteSQL(String sqlId, HashMap<String, String> params) {
        return sqlSession.delete(sqlId, params);
    }

    /**
     *
     * @param sqlId sqlId
     */
    public void execSQL(String sqlId) {
        sqlSession.update(sqlId);
    }

}
