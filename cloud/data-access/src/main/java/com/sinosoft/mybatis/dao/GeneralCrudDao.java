package com.sinosoft.mybatis.dao;

import com.sinosoft.mybatis.utility.GeneralQueryParam;

import java.util.List;
import java.util.Map;

/**
 * Project abc-cloud
 * Created by sundongbo on 2017/9/18.
 * <pre>
 * 林花谢了春红，太匆匆，无奈朝来寒雨晚来风。
 * 胭脂泪，相留醉，几时重，自是人生长恨水长东。
 *
 * --翼羽水淼起辰月，秋山红日望紫枫--
 * </pre>
 */
public interface GeneralCrudDao {
    ///**
    // * 根据主键查询
    // *
    // * @param <T>          pojo类
    // * @param clazz        pojo类-class对象
    // * @param primaryValue 主键值
    // * @return pojo对象
    // * @throws Exception
    // */
    //<T> T selectByPrimaryKey(Class<T> clazz, Integer primaryValue) throws Exception;
    //
    ///**
    // * 插入数据
    // *
    // * @param <T> pojo类
    // * @param t   pojo对象
    // * @return 数据条数
    // * @throws Exception
    // */
    //<T> int insert(T t) throws Exception;
    //
    ///**
    // * 插入数据
    // *
    // * @param <T> pojo类
    // * @param t   pojo对象
    // * @return 数据条数
    // * @throws Exception
    // */
    //<T> int insertSelective(T t) throws Exception;
    //
    ///**
    // * 删除
    // * <p>
    // * 根据主键删除
    // * </p>
    // *
    // * @param <T>          pojo类
    // * @param clazz        pojo类-class对象
    // * @param primaryValue 主键值
    // * @return 数据条数
    // */
    //<T> int deleteByPrimaryKey(Class<T> clazz, String primaryValue);
    //
    ///**
    // * 更新
    // * <p>
    // * 根据主键更新
    // * </p>
    // * <p>
    // * 更新pojo的所有字段，包括空值(null值)字段
    // * </p>
    // *
    // * @param <T> pojo类
    // * @param t   pojo对象
    // * @return 数据条数
    // * @throws Exception
    // */
    //<T> int updateByPrimaryKey(T t) throws Exception;
    //
    ///**
    // * 更新
    // * <p>
    // * 根据主键更新
    // * </p>
    // * <p>
    // * 更新pojo的非空字段
    // * </p>
    // *
    // * @param <T> pojo类
    // * @param t   pojo对象
    // * @return 数据条数
    // * @throws Exception
    // */
    //<T> int updateByPrimaryKeySelective(T t) throws Exception;

    /**
     * 查询表
     *
     * @param sql  sql语句
     * @param objs sql查询参数
     * @return 结果集集合
     */
    List<Map<String, Object>> queryForList(String sql, Object... objs);


    /**
     * @param sql   sql 语句
     * @param param 参数
     * @return 成功失败
     */
    Integer update(String sql, List<Object> param);

    /**
     * @param sql   sql语句
     * @param param 参数
     * @return 返回值
     */
    Integer update(String sql, Object... param);

    /**
     * @param sql  sql语句
     * @param objs 参数
     * @return 返回值
     */
    Map<String, Object> queryForMap(String sql, Object... objs);

    /**
     * 高级查询
     *
     * @param clazz      pojo类-class对象
     * @param queryParam 查询参数
     * @param <T>        返回类型
     * @return 返回值集合
     */
    <T> List<T> selectAdvanced(Class<T> clazz, GeneralQueryParam queryParam);

    /**
     * 高级查询,指定返回列
     *
     * @param clazz      pojo类-class对象
     * @param queryParam 查询参数
     * @param <T>        返回类型
     * @return 返回值集合
     */
    <T> List<Map<String, Object>> selectAdvancedByColumn(Class<T> clazz, GeneralQueryParam queryParam);

    /**
     * 高级查询,指定返回列
     *
     * @param queryParam 查询参数
     * @param <T>        返回类型
     * @return 返回值
     */
    <T> List<Map<String, Object>> selectAdvancedByColumn(GeneralQueryParam queryParam);
}