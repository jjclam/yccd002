package com.sinosoft.mybatis.dao.impl;

import com.sinosoft.mybatis.mapper.GenericBaseMapper;
import com.sinosoft.mybatis.utility.GeneralQueryParam;
import com.sinosoft.mybatis.utility.PersistentUtil;
import com.sinosoft.mybatis.dao.GeneralCrudDao;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Service;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 类描述： DAO层泛型基类
 * 张代浩
 *
 * @param <T>
 * @param <PK>
 * @version 1.0
 * yangqm 修改20160905，目前仅支持查询数据向redis中加载，待完善
 * sundongbo 修改20170919,改为mybatis绑定变量方式，同时封装一个公共方法，待完善
 * @date： 日期：2012-12-7 时间：上午10:16:48
 */
@Service
public class GeneralCrudDaoImpl<T, PK extends Serializable>
        implements GeneralCrudDao {
    /**
     * 初始化Log4j的一个实例
     */
    private static final Log logger = LogFactory
            .getLog(GeneralCrudDaoImpl.class);

    @Autowired
    @Qualifier("jdbcTemplate")
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private GenericBaseMapper genericBaseMapper;

    @Autowired
    @Qualifier("namedParameterJdbcTemplate")
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    public JdbcTemplate getJdbcTemplate() {
        return jdbcTemplate;
    }

    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public NamedParameterJdbcTemplate getNamedParameterJdbcTemplate() {
        return namedParameterJdbcTemplate;
    }

    public void setNamedParameterJdbcTemplate(NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
        this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
    }

    @Override
    public List<Map<String, Object>> queryForList(String sql, Object... objs) {
        return this.jdbcTemplate.queryForList(sql, objs);
    }

    @Override
    public Integer update(String sql, List<Object> param) {
        return this.jdbcTemplate.update(sql, param);
    }
    @Override
    public Integer update(String sql, Object... param) {
        return this.jdbcTemplate.update(sql, param);
    }
    @Override
    public Map<String, Object> queryForMap(String sql, Object... objs) {
        return this.jdbcTemplate.queryForMap(sql, objs);

    }


    @Override
    public <T> List<T> selectAdvanced(Class<T> clazz, GeneralQueryParam queryParam) {
        List<Map<String, Object>> list = selectAdvancedByColumn(clazz, queryParam);
        return PersistentUtil.parseToBean(list, clazz);

    }


    @Override
    public <T> List<Map<String, Object>> selectAdvancedByColumn(Class<T> clazz, GeneralQueryParam queryParam) {
        Map<String, Object> param = new HashMap<String, Object>();

        String tableName = PersistentUtil.getTableName(clazz);

        param.put("tableName", tableName);
        param.put("conditionExp", queryParam.getConditionExp());
        param.put("conditionParam", queryParam.getConditionParam());


        return genericBaseMapper.selectAdvanced(param);
    }

    @Override
    public <T> List<Map<String, Object>> selectAdvancedByColumn(GeneralQueryParam queryParam) {
        Map<String, Object> param = new HashMap<String, Object>();


        param.put("tableName", queryParam.getTableName());
        param.put("conditionExp", queryParam.getConditionExp());
        param.put("conditionParam", queryParam.getConditionParam());

        return genericBaseMapper.selectAdvanced(param);
    }
}
