package com.sinosoft.mybatis.utility;

import java.util.List;
import java.util.Map;

/**
 * 子呢的工艺参数对象
 */
public class GeneralQueryParam {

    /**
     * 表对应的pojo类
     */
    private String tableName;


    /**
     * 查询条件 where 表达式
     * <p>不要写where</p>
     * <p>传入的参数格式为:#{conditionParam.paramName}</p>
     */
    private List<String> conditionExp;

    /**
     * 查询条件 where 表达式中的参数集
     * <p>key:paramName</p>
     */
    private Map<String, Object> conditionParam;


    /**
     * 查询列
     */
    private List<String> queryColumn;


    public GeneralQueryParam() {
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public List<String> getConditionExp() {
        return conditionExp;
    }

    public void setConditionExp(List<String> conditionExp) {
        this.conditionExp = conditionExp;
    }

    public Map<String, Object> getConditionParam() {
        return conditionParam;
    }

    public void setConditionParam(Map<String, Object> conditionParam) {
        this.conditionParam = conditionParam;
    }

    public List<String> getQueryColumn() {
        return queryColumn;
    }

    public void setQueryColumn(List<String> queryColumn) {
        this.queryColumn = queryColumn;
    }
}
