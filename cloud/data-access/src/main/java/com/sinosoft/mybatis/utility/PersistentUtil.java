package com.sinosoft.mybatis.utility;

import com.sinosoft.cloud.cache.annotation.RedisTable;
import org.apache.commons.lang.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Project abc-cloud
 * Created by sundongbo on 2017/9/19.
 * <pre>
 * 林花谢了春红，太匆匆，无奈朝来寒雨晚来风。
 * 胭脂泪，相留醉，几时重，自是人生长恨水长东。
 *
 * --翼羽水淼起辰月，秋山红日望紫枫--
 * </pre>
 */
public class PersistentUtil {


    /**
     * 根据class获取表名
     * 如果有注解获取注解的表名
     * 如果没有注解则可能为pojo或schema，则截取pojo或schema字符
     * 否则直接返回simplename
     *
     * @param eClass Calss类型
     * @param <T> 类型
     * @return 转换为小写表名
     */
    public static <T> String getTableName(Class<T> eClass) {
        if (eClass.isAnnotationPresent(RedisTable.class)) {
            RedisTable redisTable = (RedisTable) eClass.getAnnotation(RedisTable.class);
            if (StringUtils.isNotBlank(redisTable.name())) {
                return redisTable.name().toLowerCase();
            }
        }
        String simpleName = eClass.getSimpleName().toLowerCase();
        if (simpleName.endsWith("pojo")) {
            simpleName = simpleName.substring(0, simpleName.lastIndexOf("pojo"));
        } else if (simpleName.endsWith("schema")) {
            simpleName = simpleName.substring(0, simpleName.lastIndexOf("schema"));
        }

        return simpleName;
    }

    /**
     *
     * @param mapList map 集合
     * @param clazz 类型
     * @param <T> 类型
     * @return 返回bean类型
     */
    public static <T> List<T> parseToBean(List<Map<String, Object>> mapList, Class<T> clazz) {
        List<T> rsList = new ArrayList();
        T po = null;
        for (Map<String, Object> m : mapList) {
            try {
                po = clazz.newInstance();
                MyBeanUtils.copyMap2Bean_Nobig(po, m);
                rsList.add(po);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return rsList;
    }
}
