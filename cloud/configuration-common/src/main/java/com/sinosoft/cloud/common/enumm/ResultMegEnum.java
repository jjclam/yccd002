package com.sinosoft.cloud.common.enumm;

/**
 * 结果信息返回码
 * @author J
 * @date 2019-11-05
 * @version V1.0
 * 
 * */
public enum ResultMegEnum {
    // 0000: 成功
    SUCCESS("0000", ""),


    // 失败(00开始标示通讯层相关错误码)
    Chl_REMOTE_UNUSABLE("0001", "远程服务不可用"),
    Chl_REMOTE_INVALID("0002", "客户端非法调用"),
    Chl_NO_BIZ_SEQUENCE_NO("0003", "远程服务调用业务流水号不存在"),
    Chl_REMOTE_CHECK_SIGN_FAIL("0004", "远程服务调用签名验证失败"),
    Chl_REMOTE_RPC_SEQ_NO_REPEATED("0005", "随机通讯码在指定时间内重复"),
    Chl_REMOTE_SIGN_INVALID("0006", "远程服务调用签名计算方式错误"),
    Chl_REMOTE_DEAL_EXCEPTION("0007", "远程服务调用处理异常"),
    Chl_REMOTE_PROTOCOL_INVALID("0008", "客户端调用协议非法"),
    Chl_REMOTE_HTTP_METHOD_INVALID("0009", "客户端请求方式非法"),

    // 失败(01开始标示参数校验相关错误码)
    PARAM_NOT_FOUND("0101", "参数不存在"),
    Chl_PARAM_INVALID("0102", "无效的参数"),
    Chl_PARAM_TOO_LARGE_LIST("0103", "列表超长"),
    Chl_PARAM_TYPE_INVALID("0104", "参数类型错误"),
    Chl_CURRENT_PAGE_INVALID("0105", "当前页码非法"),
    Chl_VIEW_NUMBER_INVALID("0106", "分页显示数目非法"),
    Chl_VIEW_LIMIT_INVALID("0107", "数据排列显示数目非法"),
    Chl_NESPARAM_NOT_FOUND("0108", "必填信息不能为空"),
    STANDARD_NOT_FOUND("0109", "是否标准未选择"),

    //  失败(02开始标示DB操作相关错误码)
    DB_FAIL("0201", "数据库操作失败"),
    DB_NOT_FOUND("0202", "无满足查询条件的数据！"),
    DB_USER_EXIST("0203", "该用户已存在！"),

    // 业务相关
    Chl_BIZ_DATA_NOT_EXISTS("1001", "数据不存在"),
    Chl_SALETIME_NOT_FOUND("1002", "产品起售时间/产品止售时间不能为空"),
    Chl_SALESTARTTIME_OVER_END("1003", "产品起售时间不能大于产品止售时间"),
    Chl_SALESTARTTIME_ERROR("1004", "渠道产品起售日期和时间不在合理范围内"),//渠道产品起售时间不能小于产品起售时间
    Chl_SALEENDTIME_ERROR("1005", "渠道产品止售日期和时间不在合理范围内"),//渠道产品止售时间不能大于产品起售时间
//    Chl_PUTDOWN_EXIST("1006", "产品已下架，渠道上架失败"),
    Chl_PUTDOWN_EXIST("1006", "产品中心没有为该产品配置上架状态"),//业务描述更改
    Chl_SALESTARTTIME_UPERROR("1007", "产品起售时间必须在产品上架时间之后"),
//    Chl_PUTAWAY_EXIST("1008", "产品已上架，请重新选择!"),
    Chl_PUTAWAY_EXIST("1008", "该产品在该渠道已经上架，请重新选择!"),//业务描述更改
    Chl_STATUS_NOT_EXIST("1009", "产品中心没有为该产品配置上架状态!"),
    Chl_PRO_UPSTATUS("1010", "该产品已在渠道上架，请先进行渠道下架操作!"),
    Chl_PUTDOWN_ALL_EXIST("1011", "所选渠道中存在该产品已经下架，请重新选择!"),
    Chl_PUTAWAY_ALL_EXIST("1012", "所选渠道中存在该产品已经上架，请重新选择!"),
    Chl_PUTAWAY_NOT_EXIST("1013", "所选渠道存在没有配置过的产品，请重新选择!"),
    Chl_LINKPRO_NOT_EXIST("1014", "没有该险种的产品，请重新选择!"),
    Chl_DATE_NOT_EXIST("1015", "请补充完整授权止期或授权起期"),

    // 代码维护功能相关
    CM_LINKCODEVALUE_NOT_EXIST("2001", "没有该代码类型的信息，请重新选择!"),
    CM_LINKCHLCHILDTYPE_NOT_EXIST("2002", "没有该渠道的子渠道的信息，请重新选择!"),
    CM_CHLAGENCYLINKED_EXIST("2003", "所选渠道和中介机构关联已存在，请重新选择!"),
    CM_LINKCODETYPE_NOT_EXIST("2004", "所选渠道没有配置过代码类型，请重新选择!"),
    CM_CHLCODELINKED_EXIST("2005", "已存在渠道代码值!"),
    CM_CHLCODE_EXIST("2006", "该渠道代码已存在!"),
    CM_SMSPARAM_NOT_EXIST("2007", "短信模板参数没有配置!"),
    CM_CODETYPE_EXIST("2008", "该代码值已经维护!"),
    CM_CODETYPENAME_EXIST("2009", "该代码值名称已经维护!"),
    CM_CHLCODELINKEDNAME_EXIST("2010", "已存在渠道代码值映射!"),
    CM_SMSPARAM_EXIST("2011", "短信模板已存在!"),

    // 未知错误
    XML_WRITE_ERROR("3001", "XML写入异常"),

    // 未知错误
    Chl_UNKNOWN_ERROR("9999", "未知错误");

    private String code;
    private String message;

    private ResultMegEnum(String code, String message) { this.code = code;
        this.message = message; }

    public String getCode()
    {
        return this.code;
    }

    public String getMessage() {
        return this.message;
    }

    public static ResultMegEnum getResultMegEnum(String code) {
        if (code == null) {
            return null;
        }

        ResultMegEnum[] values = ResultMegEnum.values();
        for (ResultMegEnum e : values) {
            if (e.getCode().equals(code)) {
                return e;
            }
        }
        return null;
    }
}
