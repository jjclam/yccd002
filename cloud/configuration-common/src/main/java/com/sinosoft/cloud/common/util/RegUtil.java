package com.sinosoft.cloud.common.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 核心产品和套餐区分
 *
 * */
public class RegUtil {
    private static final Pattern pattern = Pattern.compile("^-?\\d+(\\.\\d+)?$");

    public static boolean regMatch(String adobe) {
        Matcher reg = pattern.matcher(adobe);
        if (reg.matches()) {
            return true;
        } else {
            return false;
        }
    }
}
