package com.sinosof.test;

import com.sinosoft.PersistApplication;
import com.sinosoft.cloud.mq.MessageQueueProducer;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * Created by Administrator on 2017-10-25.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = PersistApplication.class)
public class TestProducer {

    @Autowired
    MessageQueueProducer producer;

    @Test
    public void test(){
        String str = "I'm producer";
        producer.send("TestListener",str);
    }
}
