package com.sinosoft;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Created by Administrator on 2017-10-25.
 */
@SpringBootApplication
public class PersistApplication {
    public static void main(String[] args) {
        SpringApplication.run(PersistApplication.class, args);
        System.out.println("启动成功");
    }
}
