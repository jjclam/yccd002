package com.sinosoft.cloud.access.persist;


import com.sinosoft.cloud.common.SpringContextUtils;
import com.sinosoft.cloud.mq.MessageQueueConsumer;
import com.sinosoft.cloud.mq.MessageQueueProducer;
import com.sinosoft.cloud.rest.TradeInfo;
import com.sinosoft.lis.entity.LogLktranStracksPojo;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LogLktranStracksSchema;
import com.sinosoft.utility.DBConnPool;
import com.sinosoft.utility.VData;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

import javax.jms.Message;
import java.io.*;
import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * Created by Administrator on 2017-10-25.
 */
@Component
public class TextPersist {

    /**
     * 日志管理
     */
    private final Log cLogger = LogFactory.getLog(this.getClass());

    @Value("${text.path}")
    private String textPath;
    @Autowired
    MessageQueueConsumer consumer;
    @Autowired
    MessageQueueProducer producer;

    @JmsListener(destination = "CipherText", concurrency = "1-20")
    public void persistCipherText(String msg) {

        cLogger.debug("接收到密文的消息。开始持久化密文。");
        /**
         * 进行密文持久化
         */
        OutputStream os = null;

        //获取渠道代码
        int index = msg.indexOf(",");
        String bankAlias = msg.substring(0, index);

        //获取全部报文信息
        String text = msg.substring(index + 1);

        //获取当前日期字符串
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        String currentDate = sdf.format(new Date());

        //拼接文件夹名称
        String filePath = new StringBuffer(1024).append(textPath)
                .append(bankAlias).append(File.separator).append("cipherFile").append(File.separator).toString();
        File tempFile = new File(filePath);
        if (!tempFile.exists()) {
            tempFile.mkdirs();
        }

        //获取当前时间
        SimpleDateFormat sdf2 = new SimpleDateFormat("HHmmss");
        String currentTime = sdf2.format(new Date());

        //保存文件
        try {
            os = new FileOutputStream(filePath + "H" + currentTime + "-" + UUID.randomUUID() + ".xml");
            os.write(text.getBytes("UTF-8"));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            cLogger.error("密文持久化出错");
            cLogger.error(ExceptionUtils.getFullStackTrace(e.fillInStackTrace()));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            cLogger.error("密文持久化出错");
            cLogger.error(ExceptionUtils.getFullStackTrace(e.fillInStackTrace()));
        } catch (IOException e) {
            e.printStackTrace();
            cLogger.error("密文持久化出错");
            cLogger.error(ExceptionUtils.getFullStackTrace(e.fillInStackTrace()));
        } finally {
            try {
                os.flush();
                os.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        cLogger.debug("密文持久化完毕。");
    }

    @JmsListener(destination = "ClearText", concurrency = "1-20")
    public void persistClearText(List<String> msg) {

        cLogger.debug("接收到明文的消息。开始持久化明文。");
        /**
         * 进行密文持久化
         */
        OutputStream os = null;

        //获取xxx信息
        String bankAlias = msg.get(0);
        //获取渠道信息
        String sourceType = msg.get(1);
        //获取交易编码
        String transCode = msg.get(2);
        //获取原始报文信息
        String clearText = msg.get(3);
        //获取交易流水号
        String transNo = msg.get(4);
        //获取当前日期字符串
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        String currentDate = sdf.format(new Date());

        //拼接文件夹名称
        String filePath = new StringBuffer(1024).append(textPath)
                .append(bankAlias).append(File.separator).append(currentDate).append(File.separator)
                .append(sourceType).append(File.separator).append(transCode).append(File.separator).toString();
        File tempFile = new File(filePath);
        if (!tempFile.exists()) {
            tempFile.mkdirs();
        }

        //获取当前时分秒字符串
        SimpleDateFormat sdf2 = new SimpleDateFormat("HHmmss");
        String currentTime = sdf2.format(new Date());

        //保存文件
        try {
            String fileName = new StringBuffer(1024).append(filePath).append(transNo)
                    .append("-In-").append(currentTime).append(".xml").toString();
            os = new FileOutputStream(fileName);
            os.write(clearText.getBytes("UTF-8"));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            cLogger.error("明文持久化出错");
            cLogger.error(ExceptionUtils.getFullStackTrace(e.fillInStackTrace()));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            cLogger.error("明文持久化出错");
            cLogger.error(ExceptionUtils.getFullStackTrace(e.fillInStackTrace()));
        } catch (IOException e) {
            e.printStackTrace();
            cLogger.error("明文持久化出错");
            cLogger.error(ExceptionUtils.getFullStackTrace(e.fillInStackTrace()));
        } finally {
            try {
                os.flush();
                os.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        cLogger.debug("明文持久化完毕。"+msg.get(4));
    }

    @JmsListener(destination = "OutClearText", concurrency = "1-20")
    public void persistOutClearText(List<String> msg) {

        cLogger.debug("接收到返回明文的消息。开始持久化返回明文。");
        /**
         * 进行密文持久化
         */
        OutputStream os = null;

        //获取xxx信息
        String bankAlias = msg.get(0);
        //获取渠道信息
        String sourceType = msg.get(1);
        //获取交易编码
        String transCode = msg.get(2);
        //获取原始报文信息
        String clearText = msg.get(3);
        //获取交易流水号
        String transNo = msg.get(4);
        //获取当前日期字符串
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        String currentDate = sdf.format(new Date());

        //拼接文件夹名称
        String filePath = new StringBuffer(1024).append(textPath)
                .append(bankAlias).append(File.separator).append(currentDate).append(File.separator)
                .append(sourceType).append(File.separator).append(transCode).append(File.separator).toString();
        File tempFile = new File(filePath);
        if (!tempFile.exists()) {
            tempFile.mkdirs();
        }

        //获取当前时分秒字符串
        SimpleDateFormat sdf2 = new SimpleDateFormat("HHmmss");
        String currentTime = sdf2.format(new Date());

        //保存文件
        try {
            String fileName = new StringBuffer(1024).append(filePath).append(transNo)
                    .append("-Out-").append(currentTime).append(".xml").toString();
            os = new FileOutputStream(fileName);
            os.write(clearText.getBytes("UTF-8"));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            cLogger.error("返回明文持久化出错");
            cLogger.error(ExceptionUtils.getFullStackTrace(e.fillInStackTrace()));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            cLogger.error("返回明文持久化出错");
            cLogger.error(ExceptionUtils.getFullStackTrace(e.fillInStackTrace()));
        } catch (IOException e) {
            e.printStackTrace();
            cLogger.error("返回明文持久化出错");
            cLogger.error(ExceptionUtils.getFullStackTrace(e.fillInStackTrace()));
        } finally {
            try {
                os.flush();
                os.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        cLogger.debug("返回明文持久化完毕。"+msg.get(4));
    }


    @JmsListener(destination = "logTransTracks", concurrency = "1-20")
    public void logTransTracks(TradeInfo tradeInfo, Message message)throws InterruptedException  {
        cLogger.info("交易轨迹--logTransTracks--");
        cLogger.info("=交易轨迹数据处理=");
        LogLktranStracksPojo logLktranStracksPojo=(LogLktranStracksPojo)tradeInfo.getData(LogLktranStracksPojo.class.getName());
        LogLktranStracksSchema logLktranStracksSchema=new LogLktranStracksSchema();
        String transId= PubFun1.CreateMaxNo(PubFun1.PhysicNo, 22);
        logLktranStracksSchema.setTransID(transId);
        logLktranStracksSchema.setTemp1(logLktranStracksPojo.getTemp1());
        logLktranStracksSchema.setTemp2(logLktranStracksPojo.getTemp2());
        logLktranStracksSchema.setTemp3(logLktranStracksPojo.getTemp3());
        logLktranStracksSchema.setPHConclusion(logLktranStracksPojo.getPHConclusion().substring(100));
        logLktranStracksSchema.setTransCode(logLktranStracksPojo.getTransCode());
        logLktranStracksSchema.setTransDate(logLktranStracksPojo.getTransDate());
        logLktranStracksSchema.setTransTime(logLktranStracksPojo.getTransTime());
        logLktranStracksSchema.setContType(logLktranStracksPojo.getContType());//个险
        logLktranStracksSchema.setTransNo(logLktranStracksPojo.getTransNo());
        logLktranStracksSchema.setProposalNo(logLktranStracksPojo.getProposalNo());
        logLktranStracksSchema.setContNo(logLktranStracksPojo.getContNo());
        logLktranStracksSchema.setAccessChnl(logLktranStracksPojo.getAccessChnl());
        VData vData = new VData();
        MMap mMap = new MMap();
        mMap.put(logLktranStracksSchema, "INSERT");
        vData.add(mMap);
        PubSubmit pubSubmit = new PubSubmit();
        boolean b = pubSubmit.submitData(vData);
        if(b=true){
            cLogger.info("轨迹信息落库成功！");
        }
        consumer.verified(!tradeInfo.hasError(),message);


    }

}
