package com.sinosoft.cloud.access.persist.conf;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.jdbc.DataSourceBuilder;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

import javax.sql.DataSource;

/**
 * Project cloud
 * Created by sundongbo on 2017/9/21.
 * <pre>
 * 林花谢了春红，太匆匆，无奈朝来寒雨晚来风。
 * 胭脂泪，相留醉，几时重，自是人生长恨水长东。
 *
 * --翼羽水淼起辰月，秋山红日望紫枫--
 * </pre>
 */
@Configuration
public class DataSourceConfig {
    @Bean(name = "logdataSource")
    @Qualifier("logdataSource")
    @Primary
    @ConfigurationProperties(prefix = "spring.datasource.log")
    public DataSource DataSource() {
        return DataSourceBuilder.create().build();
    }

}
