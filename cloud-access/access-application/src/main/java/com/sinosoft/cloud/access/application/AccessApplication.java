package com.sinosoft.cloud.access.application;


import com.sinosoft.cloud.access.abc.config.BankServerProperties;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.feign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@ComponentScan({"com.sinosoft.cloud"})
@EnableFeignClients("com.sinosoft.cloud")
@EnableDiscoveryClient
@SpringBootApplication
@Configuration
@PropertySource(value = {"classpath:router.properties", "classpath:bankServer.properties"})
@EnableConfigurationProperties({BankServerProperties.class})
public class AccessApplication {

    /**
     * 项目启动类
     *
     * @param args 参数
     */
    public static void main(String[] args) {
        SpringApplication.run(AccessApplication.class, args);

//        try {
//            test();
//        } catch (ParserConfigurationException e) {
//            e.printStackTrace();
//        } catch (IOException e) {
//            e.printStackTrace();
//        } catch (SAXException e) {
//            e.printStackTrace();
//        } catch (TransformerException e) {
//            e.printStackTrace();
//        }
    }

//    public static void test() throws ParserConfigurationException, IOException, SAXException, TransformerException {
//
//
//        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
//        DocumentBuilder db = dbf.newDocumentBuilder();
//
//        ClassPathResource xml = new ClassPathResource("data.xml");
//        ClassPathResource xsl = new ClassPathResource("data.xsl");
//
//
//        String strXml = "";
//        try {
//            StringBuffer str = new StringBuffer(1024);
//            FileInputStream fs = new FileInputStream(xml.getFile());
//            InputStreamReader isr;
//            isr = new InputStreamReader(fs);
//            BufferedReader br = new BufferedReader(isr);
//            try {
//                String data = "";
//                while ((data = br.readLine()) != null) {
//                    str.append(data + " ");
//                }
//                strXml = str.toString();
//            } catch (Exception e) {
//                str.append(e.toString());
//            }
//        } catch (IOException es) {
//        }
//
//        while (true) {
//            XsltTrans xsltTrans = new XsltTrans();
//            xsltTrans.setAccessName("abc");
//
//            long a = System.currentTimeMillis();
//            String result = xsltTrans.transform(strXml);
//            System.out.println("这是返回xxx的报文" + result);
//            System.out.println("" + (System.currentTimeMillis() - a));
//        }
//    }
}
