package com.sinosoft.cloud.protocol.socket;

import com.sinosoft.cloud.protocol.socket.annotations.EnableNettyServers;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

//@EnableCaching
//@EnableScheduling
@SpringBootApplication
@EnableNettyServers
public class SimpleApplication {

    public static void main(String[] args) {
        SpringApplication.run(SimpleApplication.class, args);
    }


}
