package com.sinosoft.cloud.protocol.socket.configuration;

public class TcpServerProperties {


    private String name;

    private String host;

    private Integer port;

    private Integer bossThreads;
    private Integer workerThreads;
    private ChannelOptions options;
    private ChannelOptions childOptions;

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public Integer getBossThreads() {
        return bossThreads;
    }

    public void setBossThreads(final Integer bossThreads) {
        this.bossThreads = bossThreads;
    }

    public Integer getWorkerThreads() {
        return workerThreads;
    }

    public void setWorkerThreads(final Integer workerThreads) {
        this.workerThreads = workerThreads;
    }

    public String getHost() {
        return host;
    }

    public void setHost(final String host) {
        this.host = host;
    }

    public Integer getPort() {
        return port;
    }

    public void setPort(final Integer port) {
        this.port = port;
    }

    public ChannelOptions getOptions() {
        return options;
    }

    public void setOptions(final ChannelOptions options) {
        this.options = options;
    }

    public ChannelOptions getChildOptions() {
        return childOptions;
    }

    public void setChildOptions(final ChannelOptions childOptions) {
        this.childOptions = childOptions;
    }
}
