package com.sinosoft.cloud.protocol.socket.handlers;

import com.sinosoft.cloud.protocol.socket.events.TcpEvent;
import com.sinosoft.cloud.protocol.socket.events.TcpEventHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;

import java.util.List;

/**
 * Handler that notifies all {@code TcpEventHandler} instances about client connection. This is part of
 * internal API.
 */
public final class OnConnectEventHandler extends ChannelInboundHandlerAdapter {
    private final List<TcpEventHandler<Void>> handlerList;

    /**
     * Create new handler instance
     *
     * @param handlerList List of underlying handlers
     */
    public OnConnectEventHandler(final List<TcpEventHandler<Void>> handlerList) {
        this.handlerList = handlerList;
    }

    @Override
    public void channelActive(final ChannelHandlerContext ctx) throws Exception {
        ctx.pipeline().remove(this);

        final TcpEvent<Void> event = new TcpEvent<>(ctx);
        handlerList.forEach(h -> h.handle(event));

        super.channelActive(ctx);
    }
}