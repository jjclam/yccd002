package com.sinosoft.cloud.protocol.socket.configuration;


import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.validation.annotation.Validated;

import java.util.List;

@ConfigurationProperties("netty")
@Validated
public class SpringNettyConfigurationProperties {

    private List<TcpServerProperties> servers;

    public List<TcpServerProperties> getServers() {
        return servers;
    }

    public void setServers(final List<TcpServerProperties> servers) {
        this.servers = servers;
    }
}
