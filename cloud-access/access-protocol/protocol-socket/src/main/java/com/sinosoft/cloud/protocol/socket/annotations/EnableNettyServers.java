package com.sinosoft.cloud.protocol.socket.annotations;

import com.sinosoft.cloud.protocol.socket.configuration.SpringNettyConfiguration;
import com.sinosoft.cloud.protocol.socket.server.TcpServerLifeCycle;
import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

/**
 * Annotation to enable Netty TCP servers.
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
@Import({SpringNettyConfiguration.class, TcpServerLifeCycle.class})
public @interface EnableNettyServers {
}