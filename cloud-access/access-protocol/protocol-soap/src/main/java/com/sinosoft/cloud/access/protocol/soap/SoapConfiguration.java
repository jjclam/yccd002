package com.sinosoft.cloud.access.protocol.soap;

import org.apache.cxf.jaxws.spring.JaxWsWebServicePublisherBeanPostProcessor;
import org.apache.cxf.spring.boot.autoconfigure.CxfProperties;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.context.annotation.Bean;

@SpringBootConfiguration
public class SoapConfiguration {

    @Bean
    public JaxWsWebServicePublisherBeanPostProcessor postProcessor(CxfProperties properties) throws NoSuchMethodException, ClassNotFoundException {
        JaxWsWebServicePublisherBeanPostProcessor postProcessor = new JaxWsWebServicePublisherBeanPostProcessor();
        postProcessor.setUrlPrefix("/");
        return postProcessor;
    }
}
