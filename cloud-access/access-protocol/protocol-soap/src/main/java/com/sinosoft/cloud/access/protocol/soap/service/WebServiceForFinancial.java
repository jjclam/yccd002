package com.sinosoft.cloud.access.protocol.soap.service;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import javax.jws.WebService;


/**
 * 电商接入平台-安邦金融统一入口
 * <p>
 * <p>
 * Created by ChenJin on 16/7/22.
 */
@WebService(name = "WebServiceForFinancial", serviceName = "WebServiceForFinancial",
        endpointInterface = "com.sinosoft.cloud.access.protocol.soap.service.WebServiceForFinancialInterface")
@Service("WebServiceForFinancialInterface")
public class WebServiceForFinancial {
    protected final Logger cLogger = Logger.getLogger(getClass());

    /**
     * 模块入口方法
     * @param pInNoStdStr 传入的非标准xml字符串报文
     * @return
     */
    public String service(String pInNoStdStr) {

     //   System.out.println(pInNoStdStr);

        return pInNoStdStr;
    }
}
