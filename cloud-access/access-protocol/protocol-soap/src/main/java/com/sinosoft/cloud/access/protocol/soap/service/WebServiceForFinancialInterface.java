package com.sinosoft.cloud.access.protocol.soap.service;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

/**
 * 安邦金融接入接口类
 * Created by ChenJin on 16/7/25.
 */
@WebService
public interface WebServiceForFinancialInterface {
    @WebMethod
    String service(@WebParam(name = "pInNoStdStr") String pInNoStdStr);
}
