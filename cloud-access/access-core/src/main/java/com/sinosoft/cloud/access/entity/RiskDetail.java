package com.sinosoft.cloud.access.entity;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("RiskDetail")
public class RiskDetail {

	@XStreamAlias("RiskCode")
    private String riskCode;
	@XStreamAlias("RiskName")
	private String riskName;
	@XStreamAlias("Appflag")
	private String appflag;
	@XStreamAlias("Mult")
	private String mult;
	@XStreamAlias("Prem")
	private String prem;
	@XStreamAlias("Amnt")
	private String amnt;
	@XStreamAlias("InsuYear")
	private String insuYear;
	@XStreamAlias("InsuYearFlag")
	private String insuYearFlag;
	@XStreamAlias("PayIntv")
	private String payIntv;
	@XStreamAlias("PayEndYear")
	private String payEndYear;
	@XStreamAlias("PayEndYearFlag")
	private String payEndYearFlag;
	@XStreamAlias("Years")
	private String years;
	@XStreamAlias("YearFlag")
	private String yearFlag;
	@XStreamAlias("PayYears")
	private String payYears;
	@XStreamAlias("ProdCode")
	private String prodCode;

	public String getRiskCode() {
		return riskCode;
	}

	public void setRiskCode(String riskCode) {
		this.riskCode = riskCode;
	}

	public String getRiskName() {
		return riskName;
	}

	public void setRiskName(String riskName) {
		this.riskName = riskName;
	}

	public String getAppflag() {
		return appflag;
	}

	public void setAppflag(String appflag) {
		this.appflag = appflag;
	}

	public String getMult() {
		return mult;
	}

	public void setMult(String mult) {
		this.mult = mult;
	}

	public String getPrem() {
		return prem;
	}

	public void setPrem(String prem) {
		this.prem = prem;
	}

	public String getAmnt() {
		return amnt;
	}

	public void setAmnt(String amnt) {
		this.amnt = amnt;
	}

	public String getInsuYear() {
		return insuYear;
	}

	public void setInsuYear(String insuYear) {
		this.insuYear = insuYear;
	}

	public String getInsuYearFlag() {
		return insuYearFlag;
	}

	public void setInsuYearFlag(String insuYearFlag) {
		this.insuYearFlag = insuYearFlag;
	}

	public String getPayIntv() {
		return payIntv;
	}

	public void setPayIntv(String payIntv) {
		this.payIntv = payIntv;
	}

	public String getPayEndYear() {
		return payEndYear;
	}

	public void setPayEndYear(String payEndYear) {
		this.payEndYear = payEndYear;
	}

	public String getPayEndYearFlag() {
		return payEndYearFlag;
	}

	public void setPayEndYearFlag(String payEndYearFlag) {
		this.payEndYearFlag = payEndYearFlag;
	}

	public String getYears() {
		return years;
	}

	public void setYears(String years) {
		this.years = years;
	}

	public String getYearFlag() {
		return yearFlag;
	}

	public void setYearFlag(String yearFlag) {
		this.yearFlag = yearFlag;
	}

	public String getPayYears() {
		return payYears;
	}

	public void setPayYears(String payYears) {
		this.payYears = payYears;
	}

	public String getProdCode() {
		return prodCode;
	}

	public void setProdCode(String prodCode) {
		this.prodCode = prodCode;
	}

	@Override
	public String toString() {
		return "RiskDetail{" +
				"riskCode='" + riskCode + '\'' +
				", riskName='" + riskName + '\'' +
				", appflag='" + appflag + '\'' +
				", mult='" + mult + '\'' +
				", prem='" + prem + '\'' +
				", amnt='" + amnt + '\'' +
				", insuYear='" + insuYear + '\'' +
				", insuYearFlag='" + insuYearFlag + '\'' +
				", payIntv='" + payIntv + '\'' +
				", payEndYear='" + payEndYear + '\'' +
				", payEndYearFlag='" + payEndYearFlag + '\'' +
				", years='" + years + '\'' +
				", yearFlag='" + yearFlag + '\'' +
				", payYears='" + payYears + '\'' +
				", prodCode='" + prodCode + '\'' +
				'}';
	}
}
