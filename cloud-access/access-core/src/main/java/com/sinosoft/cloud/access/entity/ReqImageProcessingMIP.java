package com.sinosoft.cloud.access.entity;

import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.List;

/**
 * @Author:lizhen
 * @Description:
 * @Date:Created in 16:38 2018/11/29
 * @Modified By:
 */
/*
* PC006 移动展业影像
* */
public class ReqImageProcessingMIP {
    @XStreamImplicit(itemFieldName="Docs")
    private List<Docs> Docs;

    public List<com.sinosoft.cloud.access.entity.Docs> getDocs() {
        return Docs;
    }

    public void setDocs(List<com.sinosoft.cloud.access.entity.Docs> docs) {
        Docs = docs;
    }

    @Override
    public String toString() {
        return "ReqImageProcessingMIP{" +
                "Docs=" + Docs +
                '}';
    }
}