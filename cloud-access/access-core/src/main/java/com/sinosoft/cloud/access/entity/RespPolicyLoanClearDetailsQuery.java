package com.sinosoft.cloud.access.entity;

/**
 * @Author:lizhen
 * @Description:
 * @Date:Created in 22:12 2018/11/12
 * @Modified By:
 */
/**
 * 4003保单贷款清偿明细查询POS005,POS118.  返回报文
 */
public class RespPolicyLoanClearDetailsQuery {
    //保全项目编码
    private String EdorType;
    //保单号
    private String ContNo;
    //投保人姓名
    private String AppntName;
    //主险名称
    private String MainRiskName;
    //贷款本金
    private String LoanMoney;
    //逾期期间利率
    private String OverRate;
    //逾期期间利息(不含税)
    private String Overinterest;
    //逾期期间贷款利息(含税)
    private String OverInterestTax;
    //本金
    private String Principal;
    //利息
    private String Interest;
    //本息和
    private String Princinterest;
    //手机号码
    private String MobilePhone;
    //是否还息不还本
    private String Isinterest;
    //贷款日期
    private String LoanDate;
    //贷款起期
    private String LoanStartDate;
    //贷款利率
    private String LoanRate;
    //初始利息
    private String InitialInterest;
    //初始利率(不含税)
    private String InitialRate;
    //初始利率(含税)
    private String InitialRateTax;
    //清偿金额合计(不含税)
    private String PayOffMoney;
    //清偿金额合计(含税)
    private String PayOffMoneyTax;
    //收付费方式
    private String PayMode;
    //xxx账户
    private String BankAccNo;
    //户名
    private String BankAccName;
    //开户行
    private String BankCode;
    //补退费领取人
    private String RefundPeople;
    //身份证号
    private String IDNo;
    //是否为美国纳税义务的个人
    private String TinFlag;
    //美国纳税人识别号
    private String TinNo;
    private String AllPayOffMoney;
    private String AllTaxMoneyCNY;
    private String LoanEndDate;
    //清偿利息合计（含税）
    private String PayOffInterestTax;
    //清偿利息合计（不含税）
    private String AllPayOffInterest;
    //清偿日期
    private String PayOffDate;

    public String getPayOffDate() {
        return PayOffDate;
    }

    public void setPayOffDate(String payOffDate) {
        PayOffDate = payOffDate;
    }

    public String getEdorType() {
        return EdorType;
    }

    public void setEdorType(String edorType) {
        EdorType = edorType;
    }

    public String getContNo() {
        return ContNo;
    }

    public void setContNo(String contNo) {
        ContNo = contNo;
    }

    public String getAppntName() {
        return AppntName;
    }

    public void setAppntName(String appntName) {
        AppntName = appntName;
    }

    public String getMainRiskName() {
        return MainRiskName;
    }

    public void setMainRiskName(String mainRiskName) {
        MainRiskName = mainRiskName;
    }

    public String getLoanMoney() {
        return LoanMoney;
    }

    public void setLoanMoney(String loanMoney) {
        LoanMoney = loanMoney;
    }

    public String getOverRate() {
        return OverRate;
    }

    public void setOverRate(String overRate) {
        OverRate = overRate;
    }

    public String getOverinterest() {
        return Overinterest;
    }

    public void setOverinterest(String overinterest) {
        Overinterest = overinterest;
    }

    public String getOverInterestTax() {
        return OverInterestTax;
    }

    public void setOverInterestTax(String overInterestTax) {
        OverInterestTax = overInterestTax;
    }

    public String getPrincipal() {
        return Principal;
    }

    public void setPrincipal(String principal) {
        Principal = principal;
    }

    public String getInterest() {
        return Interest;
    }

    public void setInterest(String interest) {
        Interest = interest;
    }

    public String getPrincinterest() {
        return Princinterest;
    }

    public void setPrincinterest(String princinterest) {
        Princinterest = princinterest;
    }

    public String getMobilePhone() {
        return MobilePhone;
    }

    public void setMobilePhone(String mobilePhone) {
        MobilePhone = mobilePhone;
    }

    public String getIsinterest() {
        return Isinterest;
    }

    public void setIsinterest(String isinterest) {
        Isinterest = isinterest;
    }

    public String getLoanDate() {
        return LoanDate;
    }

    public void setLoanDate(String loanDate) {
        LoanDate = loanDate;
    }

    public String getLoanStartDate() {
        return LoanStartDate;
    }

    public void setLoanStartDate(String loanStartDate) {
        LoanStartDate = loanStartDate;
    }

    public String getLoanRate() {
        return LoanRate;
    }

    public void setLoanRate(String loanRate) {
        LoanRate = loanRate;
    }

    public String getInitialInterest() {
        return InitialInterest;
    }

    public void setInitialInterest(String initialInterest) {
        InitialInterest = initialInterest;
    }

    public String getInitialRate() {
        return InitialRate;
    }

    public void setInitialRate(String initialRate) {
        InitialRate = initialRate;
    }

    public String getInitialRateTax() {
        return InitialRateTax;
    }

    public void setInitialRateTax(String initialRateTax) {
        InitialRateTax = initialRateTax;
    }

    public String getPayOffMoney() {
        return PayOffMoney;
    }

    public void setPayOffMoney(String payOffMoney) {
        PayOffMoney = payOffMoney;
    }

    public String getPayOffMoneyTax() {
        return PayOffMoneyTax;
    }

    public void setPayOffMoneyTax(String payOffMoneyTax) {
        PayOffMoneyTax = payOffMoneyTax;
    }

    public String getPayMode() {
        return PayMode;
    }

    public void setPayMode(String payMode) {
        PayMode = payMode;
    }

    public String getBankAccNo() {
        return BankAccNo;
    }

    public void setBankAccNo(String bankAccNo) {
        BankAccNo = bankAccNo;
    }

    public String getBankAccName() {
        return BankAccName;
    }

    public void setBankAccName(String bankAccName) {
        BankAccName = bankAccName;
    }

    public String getBankCode() {
        return BankCode;
    }

    public void setBankCode(String bankCode) {
        BankCode = bankCode;
    }

    public String getRefundPeople() {
        return RefundPeople;
    }

    public void setRefundPeople(String refundPeople) {
        RefundPeople = refundPeople;
    }

    public String getIDNo() {
        return IDNo;
    }

    public void setIDNo(String IDNo) {
        this.IDNo = IDNo;
    }

    public String getTinFlag() {
        return TinFlag;
    }

    public void setTinFlag(String tinFlag) {
        TinFlag = tinFlag;
    }

    public String getTinNo() {
        return TinNo;
    }

    public void setTinNo(String tinNo) {
        TinNo = tinNo;
    }

    public String getAllPayOffMoney() {
        return AllPayOffMoney;
    }

    public void setAllPayOffMoney(String allPayOffMoney) {
        AllPayOffMoney = allPayOffMoney;
    }

    public String getAllTaxMoneyCNY() {
        return AllTaxMoneyCNY;
    }

    public void setAllTaxMoneyCNY(String allTaxMoneyCNY) {
        AllTaxMoneyCNY = allTaxMoneyCNY;
    }

    public String getLoanEndDate() {
        return LoanEndDate;
    }

    public void setLoanEndDate(String loanEndDate) {
        LoanEndDate = loanEndDate;
    }

    public String getPayOffInterestTax() {
        return PayOffInterestTax;
    }

    public void setPayOffInterestTax(String payOffInterestTax) {
        PayOffInterestTax = payOffInterestTax;
    }

    public String getAllPayOffInterest() {
        return AllPayOffInterest;
    }

    public void setAllPayOffInterest(String allPayOffInterest) {
        AllPayOffInterest = allPayOffInterest;
    }

    @Override
    public String toString() {
        return "RespPolicyLoanClearDetailsQuery{" +
                "EdorType='" + EdorType + '\'' +
                ", ContNo='" + ContNo + '\'' +
                ", AppntName='" + AppntName + '\'' +
                ", MainRiskName='" + MainRiskName + '\'' +
                ", LoanMoney='" + LoanMoney + '\'' +
                ", OverRate='" + OverRate + '\'' +
                ", Overinterest='" + Overinterest + '\'' +
                ", OverInterestTax='" + OverInterestTax + '\'' +
                ", Principal='" + Principal + '\'' +
                ", Interest='" + Interest + '\'' +
                ", Princinterest='" + Princinterest + '\'' +
                ", MobilePhone='" + MobilePhone + '\'' +
                ", Isinterest='" + Isinterest + '\'' +
                ", LoanDate='" + LoanDate + '\'' +
                ", LoanStartDate='" + LoanStartDate + '\'' +
                ", LoanRate='" + LoanRate + '\'' +
                ", InitialInterest='" + InitialInterest + '\'' +
                ", InitialRate='" + InitialRate + '\'' +
                ", InitialRateTax='" + InitialRateTax + '\'' +
                ", PayOffMoney='" + PayOffMoney + '\'' +
                ", PayOffMoneyTax='" + PayOffMoneyTax + '\'' +
                ", PayMode='" + PayMode + '\'' +
                ", BankAccNo='" + BankAccNo + '\'' +
                ", BankAccName='" + BankAccName + '\'' +
                ", BankCode='" + BankCode + '\'' +
                ", RefundPeople='" + RefundPeople + '\'' +
                ", IDNo='" + IDNo + '\'' +
                ", TinFlag='" + TinFlag + '\'' +
                ", TinNo='" + TinNo + '\'' +
                ", AllPayOffMoney='" + AllPayOffMoney + '\'' +
                ", AllTaxMoneyCNY='" + AllTaxMoneyCNY + '\'' +
                ", LoanEndDate='" + LoanEndDate + '\'' +
                ", PayOffInterestTax='" + PayOffInterestTax + '\'' +
                ", AllPayOffInterest='" + AllPayOffInterest + '\'' +
                ", PayOffDate='" + PayOffDate + '\'' +
                '}';
    }
}