package com.sinosoft.cloud.access.entity;

public class LCContState {
    // @Field
    /** Id */
    private long ContStateID;
    /** Fk_lccont */
    private long ContID;
    /** Shardingid */
    private String ShardingID;
    /** 保单号 */
    private String ContNo;
    /** 被保险人号码 */
    private String InsuredNo;
    /** 保单险种号 */
    private String PolNo;
    /** 状态类型 */
    private String StateType;
    /** 状态 */
    private String State;
    /** 状态原因 */
    private String StateReason;
    /** 状态生效时间 */
    private String  StartDate;
    /** 状态结束时间 */
    private String  EndDate;
    /** 备注 */
    private String Remark;
    /** 操作员 */
    private String Operator;
    /** 入机日期 */
    private String  MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 最后一次修改日期 */
    private String  ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime;

    public long getContStateID() {
        return ContStateID;
    }

    public void setContStateID(long contStateID) {
        ContStateID = contStateID;
    }

    public long getContID() {
        return ContID;
    }

    public void setContID(long contID) {
        ContID = contID;
    }

    public String getShardingID() {
        return ShardingID;
    }

    public void setShardingID(String shardingID) {
        ShardingID = shardingID;
    }

    public String getContNo() {
        return ContNo;
    }

    public void setContNo(String contNo) {
        ContNo = contNo;
    }

    public String getInsuredNo() {
        return InsuredNo;
    }

    public void setInsuredNo(String insuredNo) {
        InsuredNo = insuredNo;
    }

    public String getPolNo() {
        return PolNo;
    }

    public void setPolNo(String polNo) {
        PolNo = polNo;
    }

    public String getStateType() {
        return StateType;
    }

    public void setStateType(String stateType) {
        StateType = stateType;
    }

    public String getState() {
        return State;
    }

    public void setState(String state) {
        State = state;
    }

    public String getStateReason() {
        return StateReason;
    }

    public void setStateReason(String stateReason) {
        StateReason = stateReason;
    }

    public String getStartDate() {
        return StartDate;
    }

    public void setStartDate(String startDate) {
        StartDate = startDate;
    }

    public String getEndDate() {
        return EndDate;
    }

    public void setEndDate(String endDate) {
        EndDate = endDate;
    }

    public String getRemark() {
        return Remark;
    }

    public void setRemark(String remark) {
        Remark = remark;
    }

    public String getOperator() {
        return Operator;
    }

    public void setOperator(String operator) {
        Operator = operator;
    }

    public String getMakeDate() {
        return MakeDate;
    }

    public void setMakeDate(String makeDate) {
        MakeDate = makeDate;
    }

    public String getMakeTime() {
        return MakeTime;
    }

    public void setMakeTime(String makeTime) {
        MakeTime = makeTime;
    }

    public String getModifyDate() {
        return ModifyDate;
    }

    public void setModifyDate(String modifyDate) {
        ModifyDate = modifyDate;
    }

    public String getModifyTime() {
        return ModifyTime;
    }

    public void setModifyTime(String modifyTime) {
        ModifyTime = modifyTime;
    }
}
