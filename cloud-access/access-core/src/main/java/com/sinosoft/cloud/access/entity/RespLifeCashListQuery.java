package com.sinosoft.cloud.access.entity;

import com.sinosoft.lis.pos.entity.PolicyQueryInfosPojo;

/**
 * @Author:lizhen
 * @Description:
 * @Date:Created in 15:14 2018/11/16
 * @Modified By:
 */
/**
 * 生存金列表查询POS046. 返回
 */
public class RespLifeCashListQuery {
    //保全项目编码
    private String EdorType;
    private PolicyQueryInfos PolicyQueryInfos;

    public String getEdorType() {
        return EdorType;
    }

    public void setEdorType(String edorType) {
        EdorType = edorType;
    }

    public com.sinosoft.cloud.access.entity.PolicyQueryInfos getPolicyQueryInfos() {
        return PolicyQueryInfos;
    }

    public void setPolicyQueryInfos(com.sinosoft.cloud.access.entity.PolicyQueryInfos policyQueryInfos) {
        PolicyQueryInfos = policyQueryInfos;
    }

    @Override
    public String toString() {
        return "RespLifeCashListQuery{" +
                "EdorType='" + EdorType + '\'' +
                ", PolicyQueryInfos=" + PolicyQueryInfos +
                '}';
    }
}