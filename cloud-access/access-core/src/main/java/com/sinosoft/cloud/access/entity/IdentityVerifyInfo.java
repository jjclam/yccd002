package com.sinosoft.cloud.access.entity;

/**
 * @Author:lizhen
 * @Description:
 * @Date:Created in 15:40 2018/11/15
 * @Modified By:
 */
public class IdentityVerifyInfo {
    //被保人号
    private String InsuredNo;
    //被保人姓名
    private String InsuredName;
    private String PolicyAppName;
    private String PolicyCom;

    public String getInsuredNo() {
        return InsuredNo;
    }

    public void setInsuredNo(String insuredNo) {
        InsuredNo = insuredNo;
    }

    public String getInsuredName() {
        return InsuredName;
    }

    public void setInsuredName(String insuredName) {
        InsuredName = insuredName;
    }

    public String getPolicyAppName() {
        return PolicyAppName;
    }

    public void setPolicyAppName(String policyAppName) {
        PolicyAppName = policyAppName;
    }

    public String getPolicyCom() {
        return PolicyCom;
    }

    public void setPolicyCom(String policyCom) {
        PolicyCom = policyCom;
    }

    @Override
    public String toString() {
        return "IdentityVerifyInfo{" +
                "InsuredNo='" + InsuredNo + '\'' +
                ", InsuredName='" + InsuredName + '\'' +
                ", PolicyAppName='" + PolicyAppName + '\'' +
                ", PolicyCom='" + PolicyCom + '\'' +
                '}';
    }
}