package com.sinosoft.cloud.access.entity;

/**
 * @Author:lizhen
 * @Description:
 * @Date:Created in 15:29 2018/11/15
 * @Modified By:
 */
/**
 * 缴费成功通知书查询XQ004.
 */
public class ReqRenewalAdviseQuery{
    //续期业务类型
    private String BusiType;
    //客户号
    private String CustomerNo;
    //客户身份证号
    private String CustomerIDNo;
    //客户姓名
    private String CustomerName;

    public String getBusiType() {
        return BusiType;
    }

    public void setBusiType(String busiType) {
        BusiType = busiType;
    }

    public String getCustomerNo() {
        return CustomerNo;
    }

    public void setCustomerNo(String customerNo) {
        CustomerNo = customerNo;
    }

    public String getCustomerIDNo() {
        return CustomerIDNo;
    }

    public void setCustomerIDNo(String customerIDNo) {
        CustomerIDNo = customerIDNo;
    }

    public String getCustomerName() {
        return CustomerName;
    }

    public void setCustomerName(String customerName) {
        CustomerName = customerName;
    }

    @Override
    public String toString() {
        return "ReqRenewalAdviseQuery{" +
                "BusiType='" + BusiType + '\'' +
                ", CustomerNo='" + CustomerNo + '\'' +
                ", CustomerIDNo='" + CustomerIDNo + '\'' +
                ", CustomerName='" + CustomerName + '\'' +
                '}';
    }
}