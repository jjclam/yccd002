package com.sinosoft.cloud.access.entity;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import java.util.List;

/**
 * @Description
 * @Author:zhaoshulei
 * @Date:2017/11/8
 */
@XStreamAlias("CashValue")
public class CashValue {

    private String riskCode;
    private List<String> cashValue;

    public String getRiskCode() {
        return riskCode;
    }

    public void setRiskCode(String riskCode) {
        this.riskCode = riskCode;
    }

    public List<String> getCashValue() {
        return cashValue;
    }

    public void setCashValue(List<String> cashValue) {
        this.cashValue = cashValue;
    }

    @Override
    public String toString() {
        return "CashValue{" +
                "riskCode='" + riskCode + '\'' +
                ", cashValue=" + cashValue +
                '}';
    }
}
