package com.sinosoft.cloud.access.entity;

import com.sinosoft.cloud.access.annotations.BeanCopy;

/**
 * @Author:lizhen
 * @Description:
 * @Date:Created in 17:49 2018/11/9
 * @Modified By:
 */
/*
* 4004保单犹豫期退保申请 POS018请求
* */
public class ReqWT {
    //保全项目编码
    private String EdorType;
    //保单号
    private String ContNo;
    //保单印刷号
    private String PrintCode;
    //领取金额
    private String GetAmt;
    //地区代码
    private String ZoneNo;
    //网点编码
    private String BranchNo;
    //柜员代码
    private String TellerNo;
    private EdorAppInfo EdorAppInfo;
    private PersonInfo PersonInfo;
    private BankInfo BankInfo;
    //收付费方式
    private String PayMode;
    //退保原因
    private String Reason	;

    public String getEdorType() {
        return EdorType;
    }

    public void setEdorType(String edorType) {
        EdorType = edorType;
    }

    public String getContNo() {
        return ContNo;
    }

    public void setContNo(String contNo) {
        ContNo = contNo;
    }

    public String getPrintCode() {
        return PrintCode;
    }

    public void setPrintCode(String printCode) {
        PrintCode = printCode;
    }

    public String getGetAmt() {
        return GetAmt;
    }

    public void setGetAmt(String getAmt) {
        GetAmt = getAmt;
    }

    public String getZoneNo() {
        return ZoneNo;
    }

    public void setZoneNo(String zoneNo) {
        ZoneNo = zoneNo;
    }

    public String getBranchNo() {
        return BranchNo;
    }

    public void setBranchNo(String branchNo) {
        BranchNo = branchNo;
    }

    public String getTellerNo() {
        return TellerNo;
    }

    public void setTellerNo(String tellerNo) {
        TellerNo = tellerNo;
    }

    public EdorAppInfo getEdorAppInfo() {
        return EdorAppInfo;
    }

    public void setEdorAppInfo(EdorAppInfo edorAppInfo) {
        EdorAppInfo = edorAppInfo;
    }

    public PersonInfo getPersonInfo() {
        return PersonInfo;
    }

    public void setPersonInfo(PersonInfo personInfo) {
        PersonInfo = personInfo;
    }

    public BankInfo getBankInfo() {
        return BankInfo;
    }

    public void setBankInfo(BankInfo bankInfo) {
        BankInfo = bankInfo;
    }

    public String getPayMode() {
        return PayMode;
    }

    public void setPayMode(String payMode) {
        PayMode = payMode;
    }

    public String getReason() {
        return Reason;
    }

    public void setReason(String reason) {
        Reason = reason;
    }

    @Override
    public String toString() {
        return "ReqWT{" +
                "EdorType='" + EdorType + '\'' +
                ", ContNo='" + ContNo + '\'' +
                ", PrintCode='" + PrintCode + '\'' +
                ", GetAmt='" + GetAmt + '\'' +
                ", ZoneNo='" + ZoneNo + '\'' +
                ", BranchNo='" + BranchNo + '\'' +
                ", TellerNo='" + TellerNo + '\'' +
                ", EdorAppInfo=" + EdorAppInfo +
                ", PersonInfo=" + PersonInfo +
                ", BankInfo=" + BankInfo +
                ", PayMode='" + PayMode + '\'' +
                ", Reason='" + Reason + '\'' +
                '}';
    }
}
