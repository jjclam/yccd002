package com.sinosoft.cloud.access.entity;

import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.List;

public class Node {
    private String DocType;
    private String Remark;
    private String PageNum;
    @XStreamImplicit(itemFieldName="Image")
    private List<Image> Image;

    public String getDocType() {
        return DocType;
    }

    public void setDocType(String docType) {
        DocType = docType;
    }

    public String getRemark() {
        return Remark;
    }

    public void setRemark(String remark) {
        Remark = remark;
    }

    public String getPageNum() {
        return PageNum;
    }

    public void setPageNum(String pageNum) {
        PageNum = pageNum;
    }

    public List<com.sinosoft.cloud.access.entity.Image> getImage() {
        return Image;
    }

    public void setImage(List<com.sinosoft.cloud.access.entity.Image> image) {
        Image = image;
    }

    @Override
    public String toString() {
        return "Node{" +
                "DocType='" + DocType + '\'' +
                ", Remark='" + Remark + '\'' +
                ", PageNum='" + PageNum + '\'' +
                ", Image=" + Image +
                '}';
    }
}
