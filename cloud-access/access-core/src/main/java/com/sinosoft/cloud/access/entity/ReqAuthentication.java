package com.sinosoft.cloud.access.entity;

/**
 * @Author:lizhen
 * @Description:
 * @Date:Created in 15:37 2018/11/15
 * @Modified By:
 */
/**
 * 身份验证环节CLM001.
 */
public class ReqAuthentication {
    //客户名称
    private String CustomerName;
    //客户性别
    private String CustomerSex;
    //证件类型
    private String CustomerIDType;
    //证件号码
    private String CustomerIDNo;
    //出生日期
    private String CustomerBirthday;
    //审核类型
    private String CheckType;
    //业务类型
    private String BussType;

    public String getCustomerName() {
        return CustomerName;
    }

    public void setCustomerName(String customerName) {
        CustomerName = customerName;
    }

    public String getCustomerSex() {
        return CustomerSex;
    }

    public void setCustomerSex(String customerSex) {
        CustomerSex = customerSex;
    }

    public String getCustomerIDType() {
        return CustomerIDType;
    }

    public void setCustomerIDType(String customerIDType) {
        CustomerIDType = customerIDType;
    }

    public String getCustomerIDNo() {
        return CustomerIDNo;
    }

    public void setCustomerIDNo(String customerIDNo) {
        CustomerIDNo = customerIDNo;
    }

    public String getCustomerBirthday() {
        return CustomerBirthday;
    }

    public void setCustomerBirthday(String customerBirthday) {
        CustomerBirthday = customerBirthday;
    }

    public String getCheckType() {
        return CheckType;
    }

    public void setCheckType(String checkType) {
        CheckType = checkType;
    }

    public String getBussType() {
        return BussType;
    }

    public void setBussType(String bussType) {
        BussType = bussType;
    }

    @Override
    public String toString() {
        return "ReqAuthentication{" +
                "CustomerName='" + CustomerName + '\'' +
                ", CustomerSex='" + CustomerSex + '\'' +
                ", CustomerIDType='" + CustomerIDType + '\'' +
                ", CustomerIDNo='" + CustomerIDNo + '\'' +
                ", CustomerBirthday='" + CustomerBirthday + '\'' +
                ", CheckType='" + CheckType + '\'' +
                ", BussType='" + BussType + '\'' +
                '}';
    }
}