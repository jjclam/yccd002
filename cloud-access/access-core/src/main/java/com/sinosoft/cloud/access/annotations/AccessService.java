package com.sinosoft.cloud.access.annotations;


import org.springframework.stereotype.Component;

import java.lang.annotation.*;

@Component
@Inherited
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface AccessService {

    /**
     * 接口名称
     *
     * @return
     */
    String name();


    /**
     * 交易编码
     */
    String transCode();
}
