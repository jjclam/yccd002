package com.sinosoft.cloud.access.entity;

import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.List;

/**
 * @Description
 * @Author:zhaoshulei
 * @Date:2018/4/2
 */
public class LCPols {

    @XStreamImplicit(itemFieldName="LCPol")
    private List<LCPol> LCPols;

    public List<LCPol> getLCPols() {
        return LCPols;
    }

    public void setLCPols(List<LCPol> LCPols) {
        this.LCPols = LCPols;
    }

    @Override
    public String toString() {
        return "LCPols{" +
                "LCPols=" + LCPols +
                '}';
    }
}
