package com.sinosoft.cloud.access.entity;

import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.List;

/**
 * @Description
 * @Author:zhaoshulei
 * @Date:2018/4/24
 */
public class LKTransStatuses {
    @XStreamImplicit(itemFieldName="LKTransStatus")
    private List<LKTransStatus> LKTransStatuses;

    public List<LKTransStatus> getLKTransStatuses() {
        return LKTransStatuses;
    }

    public void setLKTransStatuses(List<LKTransStatus> LKTransStatuses) {
        this.LKTransStatuses = LKTransStatuses;
    }

    @Override
    public String toString() {
        return "LKTransStatuses{" +
                "LKTransStatuses=" + LKTransStatuses +
                '}';
    }
}
