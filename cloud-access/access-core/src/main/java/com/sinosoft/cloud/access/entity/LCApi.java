package com.sinosoft.cloud.access.entity;

/**
 * @program: abc-cloud-access
 * @description:
 * @author: BaoYongmeng
 * @create: 2018-06-15 11:06
 **/
public class LCApi {
    private String MultiYearMark;
    private String YearMark;
    private String OutLetsCode;
    private String ChargeMode;
    private String ChargeType;
    private String ChannelCode;

    public void setMultiYearMark(String multiYearMark) {
        MultiYearMark = multiYearMark;
    }

    public void setYearMark(String yearMark) {
        YearMark = yearMark;
    }

    public void setOutLetsCode(String outLetsCode) {
        OutLetsCode = outLetsCode;
    }

    public void setChargeMode(String chargeMode) {
        ChargeMode = chargeMode;
    }

    public void setChargeType(String chargeType) {
        ChargeType = chargeType;
    }

    public void setChannelCode(String channelCode) {
        ChannelCode = channelCode;
    }

    public String getMultiYearMark() {
        return MultiYearMark;
    }

    public String getYearMark() {
        return YearMark;
    }

    public String getOutLetsCode() {
        return OutLetsCode;
    }

    public String getChargeMode() {
        return ChargeMode;
    }

    public String getChargeType() {
        return ChargeType;
    }

    public String getChannelCode() {
        return ChannelCode;
    }

    @Override
    public String toString() {
        return "LCApi{" +
                "MultiYearMark='" + MultiYearMark + '\'' +
                ", YearMark='" + YearMark + '\'' +
                ", OutLetsCode='" + OutLetsCode + '\'' +
                ", ChargeMode='" + ChargeMode + '\'' +
                ", ChargeType='" + ChargeType + '\'' +
                ", ChannelCode='" + ChannelCode + '\'' +
                '}';
    }
}
