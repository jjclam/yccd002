package com.sinosoft.cloud.access.entity;
/**
 * 生存给付续领账户变更详情查询POS038.
 */
public class RespPayAccountDetailsQuery {

    //保全项目编码
    private String EdorType;
    //保单号
    private String ContNo;
    //主险名称
    private String MainRiskName;
    //投保人姓名
    private String AppntName;
    //户名
    private String NewBankAccName;
    //领取方式
    private String GetMode;
    //开户行
    private String BankCode;
    //xxx账户名
    private String BankAccName;
    //xxx账号
    private String BankAccNo;

    public String getEdorType() {
        return EdorType;
    }

    public void setEdorType(String edorType) {
        EdorType = edorType;
    }

    public String getContNo() {
        return ContNo;
    }

    public void setContNo(String contNo) {
        ContNo = contNo;
    }

    public String getMainRiskName() {
        return MainRiskName;
    }

    public void setMainRiskName(String mainRiskName) {
        MainRiskName = mainRiskName;
    }

    public String getAppntName() {
        return AppntName;
    }

    public void setAppntName(String appntName) {
        AppntName = appntName;
    }

    public String getNewBankAccName() {
        return NewBankAccName;
    }

    public void setNewBankAccName(String newBankAccName) {
        NewBankAccName = newBankAccName;
    }

    public String getGetMode() {
        return GetMode;
    }

    public void setGetMode(String getMode) {
        GetMode = getMode;
    }

    public String getBankCode() {
        return BankCode;
    }

    public void setBankCode(String bankCode) {
        BankCode = bankCode;
    }

    public String getBankAccName() {
        return BankAccName;
    }

    public void setBankAccName(String bankAccName) {
        BankAccName = bankAccName;
    }

    public String getBankAccNo() {
        return BankAccNo;
    }

    public void setBankAccNo(String bankAccNo) {
        BankAccNo = bankAccNo;
    }

    @Override
    public String toString() {
        return "RespPayAccountDetailsQuery{" +
                "EdorType='" + EdorType + '\'' +
                ", ContNo='" + ContNo + '\'' +
                ", MainRiskName='" + MainRiskName + '\'' +
                ", AppntName='" + AppntName + '\'' +
                ", NewBankAccName='" + NewBankAccName + '\'' +
                ", GetMode='" + GetMode + '\'' +
                ", BankCode='" + BankCode + '\'' +
                ", BankAccName='" + BankAccName + '\'' +
                ", BankAccNo='" + BankAccNo + '\'' +
                '}';
    }
}
