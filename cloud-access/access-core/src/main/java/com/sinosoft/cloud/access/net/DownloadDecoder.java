package com.sinosoft.cloud.access.net;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ByteToMessageDecoder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.ByteOrder;
import java.nio.charset.Charset;
import java.util.List;

/**
 * @Author: 崔广东
 * @Date: 2018-3-13 17:13
 * @Description:
 */
public class DownloadDecoder extends ByteToMessageDecoder {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    protected String accessName;
    private DecoderFactory decoderFactory = DecoderFactory.getInstance();

    @Override
    protected void decode(ChannelHandlerContext ctx, ByteBuf in, List<Object> out) throws Exception {
        Object decode = decode(ctx, in);
        out.add(decode);
    }

    private Object decode(ChannelHandlerContext ctx, ByteBuf in) {
        if (in.readableBytes() < 1) {
            return null;
        }

        String headMsg = in.toString(Charset.defaultCharset());
        // 如果返回的是交易应答信息，则按照正常处理
        if ("X".startsWith(headMsg) || "x".startsWith(headMsg)) {
            Coder coder = decoderFactory.getDecoder(getAccessName());
            if (coder == null) {
                return null;
            }

            in.order(ByteOrder.BIG_ENDIAN);
            int frameLengthInt = coder.getBodySizeByHead(headMsg);
            int currentMsgLength = in.readableBytes() - coder.getHeadSize();
            if (currentMsgLength < frameLengthInt) {
                return null;
            }
            int readerIndex = in.readerIndex();
            in.readerIndex(readerIndex + frameLengthInt);
            return headMsg;
        } else if ("e".startsWith(headMsg) || "E".startsWith(headMsg)) {
            // 说明目前xxx暂无对账文件
            // 返回确认信息
            return "0000";
        } else {
            // 说明返回的是文件大小或文件大小+文件内容
            // 第一次返回的是即将返回的文件大小
            if (in.readableBytes() < 12) {
                return null;
            }
            int fileLen = Integer.parseInt(in.toString(Charset.defaultCharset()).substring(12));
            logger.debug("文件大小为：" + fileLen);

            int currentLen = in.readableBytes() - fileLen;
            if (currentLen < fileLen) {
                return null;
            }
            int readerIndex = in.readerIndex();
            in.readerIndex(readerIndex + fileLen);
            return headMsg;
        }
    }

    public String getAccessName() {
        return accessName;
    }

    public void setAccessName(String accessName) {
        this.accessName = accessName;
    }
}
