package com.sinosoft.cloud.access.entity;

/**
 * @program: abc-cloud-access
 * @description: 换发纸质保单查询POS094.
 * @author: BaoYongmeng
 * @create: 2018-12-24 16:55
 **/
public class RespBarterpaperBumfQuery {

    private WXContInfoRe WXContInfoRe;

    public com.sinosoft.cloud.access.entity.WXContInfoRe getWXContInfoRe() {
        return WXContInfoRe;
    }

    public void setWXContInfoRe(com.sinosoft.cloud.access.entity.WXContInfoRe WXContInfoRe) {
        this.WXContInfoRe = WXContInfoRe;
    }

    @Override
    public String toString() {
        return "RespBarterpaperBumfQuery{" +
                "WXContInfoRe=" + WXContInfoRe +
                '}';
    }
}
