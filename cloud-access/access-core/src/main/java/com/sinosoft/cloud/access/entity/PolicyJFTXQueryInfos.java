package com.sinosoft.cloud.access.entity;

import com.sinosoft.lis.pos.entity.PolicyJFTXQueryInfoPojo;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.List;

/**
 * @Author:lizhen
 * @Description:
 * @Date:Created in 15:21 2018/11/15
 * @Modified By:
 */
public class PolicyJFTXQueryInfos {
    @XStreamImplicit(itemFieldName="PolicyJFTXQueryInfo")
    private List<PolicyJFTXQueryInfo> PolicyJFTXQueryInfo;

    public List<com.sinosoft.cloud.access.entity.PolicyJFTXQueryInfo> getPolicyJFTXQueryInfo() {
        return PolicyJFTXQueryInfo;
    }

    public void setPolicyJFTXQueryInfo(List<com.sinosoft.cloud.access.entity.PolicyJFTXQueryInfo> policyJFTXQueryInfo) {
        PolicyJFTXQueryInfo = policyJFTXQueryInfo;
    }

    @Override
    public String toString() {
        return "PolicyJFTXQueryInfos{" +
                "PolicyJFTXQueryInfo=" + PolicyJFTXQueryInfo +
                '}';
    }
}