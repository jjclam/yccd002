package com.sinosoft.cloud.access.entity;

/**
 * 保单收费信息查询XQ006.
 */
public class RespPolicyChargeInfoQuery {
    //保单号
    private String ContNo;
    //是否可以收续期保费
    private String XqFeeFlag;
    //保费金额
    private String Prem;
    //应缴期数
    private String DuePeriod;
    //应缴日期
    private String DueDate;
    //证件类型
    private String IdType;
    //证件号码
    private String IDNo;
    //手机号
    private String Mobile;
    //申请组织
    private String ComCode;
    //交易类型
    private String BussType;
    //xxx编码
    private String BankCode;
    //xxx账号
    private String BankAccNo;
    //户名
    private String BankAccName;

    public String getContNo() {
        return ContNo;
    }

    public void setContNo(String contNo) {
        ContNo = contNo;
    }

    public String getXqFeeFlag() {
        return XqFeeFlag;
    }

    public void setXqFeeFlag(String xqFeeFlag) {
        XqFeeFlag = xqFeeFlag;
    }

    public String getPrem() {
        return Prem;
    }

    public void setPrem(String prem) {
        Prem = prem;
    }

    public String getDuePeriod() {
        return DuePeriod;
    }

    public void setDuePeriod(String duePeriod) {
        DuePeriod = duePeriod;
    }

    public String getDueDate() {
        return DueDate;
    }

    public void setDueDate(String dueDate) {
        DueDate = dueDate;
    }

    public String getIdType() {
        return IdType;
    }

    public void setIdType(String idType) {
        IdType = idType;
    }

    public String getIDNo() {
        return IDNo;
    }

    public void setIDNo(String IDNo) {
        this.IDNo = IDNo;
    }

    public String getMobile() {
        return Mobile;
    }

    public void setMobile(String mobile) {
        Mobile = mobile;
    }

    public String getComCode() {
        return ComCode;
    }

    public void setComCode(String comCode) {
        ComCode = comCode;
    }

    public String getBussType() {
        return BussType;
    }

    public void setBussType(String bussType) {
        BussType = bussType;
    }

    public String getBankCode() {
        return BankCode;
    }

    public void setBankCode(String bankCode) {
        BankCode = bankCode;
    }

    public String getBankAccNo() {
        return BankAccNo;
    }

    public void setBankAccNo(String bankAccNo) {
        BankAccNo = bankAccNo;
    }

    public String getBankAccName() {
        return BankAccName;
    }

    public void setBankAccName(String bankAccName) {
        BankAccName = bankAccName;
    }

    @Override
    public String toString() {
        return "RespPolicyChargeInfoQuery{" +
                "ContNo='" + ContNo + '\'' +
                ", XqFeeFlag='" + XqFeeFlag + '\'' +
                ", Prem='" + Prem + '\'' +
                ", DuePeriod='" + DuePeriod + '\'' +
                ", DueDate='" + DueDate + '\'' +
                ", IdType='" + IdType + '\'' +
                ", IDNo='" + IDNo + '\'' +
                ", Mobile='" + Mobile + '\'' +
                ", ComCode='" + ComCode + '\'' +
                ", BussType='" + BussType + '\'' +
                ", BankCode='" + BankCode + '\'' +
                ", BankAccNo='" + BankAccNo + '\'' +
                ", BankAccName='" + BankAccName + '\'' +
                '}';
    }
}
