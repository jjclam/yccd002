package com.sinosoft.cloud.access.entity;

/**
 * abc-cloud-access
 *
 * @author lizhen
 * @create 2019/3/25 PC008
 */
public class RespAgentCharge {
    //成功失败标志
    private String RetCode;
    //提示信息
    private String RetMsg;

    public String getRetCode() {
        return RetCode;
    }

    public void setRetCode(String retCode) {
        RetCode = retCode;
    }

    public String getRetMsg() {
        return RetMsg;
    }

    public void setRetMsg(String retMsg) {
        RetMsg = retMsg;
    }

    @Override
    public String toString() {
        return "RespAgentCharge{" +
                "RetCode='" + RetCode + '\'' +
                ", RetMsg='" + RetMsg + '\'' +
                '}';
    }
}