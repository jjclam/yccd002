package com.sinosoft.cloud.access.net;

import com.sinosoft.cloud.access.transformer.XsltTrans;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @Author:lizhen
 * @Description:
 * @Date:Created in 14:18 2018/4/27
 * @Modified By:
 */
@Controller
public class ControlAccess {
    public static String controlFlag = "on";
    private static final Log logger = LogFactory.getLog(XsltTrans.class);

    @GetMapping("/control")
    @ResponseBody
    public String control(@RequestParam String name) {
        controlFlag = name;
        logger.debug("统一接入控制开关状态：" + controlFlag);
        return controlFlag;
    }

}