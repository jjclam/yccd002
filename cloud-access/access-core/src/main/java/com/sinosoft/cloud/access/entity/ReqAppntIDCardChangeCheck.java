package com.sinosoft.cloud.access.entity;

/**
 * 投保人证件有效期校验POS058
 */
public class ReqAppntIDCardChangeCheck {
    //身份证件号码
    private String CustomerIDNo;
    //姓名
    private String CustomerName;

    public String getCustomerIDNo() {
        return CustomerIDNo;
    }

    public void setCustomerIDNo(String customerIDNo) {
        CustomerIDNo = customerIDNo;
    }

    public String getCustomerName() {
        return CustomerName;
    }

    public void setCustomerName(String customerName) {
        CustomerName = customerName;
    }

    @Override
    public String toString() {
        return "ReqAppntIDCardChangeCheck{" +
                "CustomerIDNo='" + CustomerIDNo + '\'' +
                ", CustomerName='" + CustomerName + '\'' +
                '}';
    }
}
