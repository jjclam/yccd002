package com.sinosoft.cloud.access.transformer.function;

import com.sinosoft.cache.dao.impl.RedisCommonDao;
import com.sinosoft.cloud.access.util.MoenyUtil;
import com.sinosoft.cloud.access.util.TimeUtil;
import com.sinosoft.lis.entity.LDPlanPojo;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import io.netty.util.internal.StringUtil;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;

import java.util.Date;
import java.util.List;

/**
 * Created by zg on 2016/8/12.
 * 用于XSL转换非标准报文到标准报文时的填充内容。传入相关节点内容作为参数，返回核心需要的标准报文的填充节点值
 */
public class XSLTransfromFunction {
    @Autowired
    RedisCommonDao redisCommonDao;
    /**
     * 日志管理
     */
    private static final Log cLogger = LogFactory.getLog(TimeUtil.class);
    private static List<String[]> ldcoms;

  // public static RedisCommonDao mRedisCommonDao = SpringContextUtils.getBeanByClass(RedisCommonDao.class);
      //最新需求拼接移动展业受益人地址信息表 HomeAddres
      public static String getMIPAddress(String address) {
          String[] homeAddress = address.split("~");
          String returnAddress = null;
          if (homeAddress.length >= 7) {
              returnAddress = homeAddress[1] + "&" + homeAddress[2] + "&" + homeAddress[3] + "&" + homeAddress[4] + "&" + homeAddress[5];
              return returnAddress;
          } else {
              return "";
          }
      }
    //获取常住地址村/社区（楼、号）StoreNo
    public static String getMIPStoreNo(String address) {
        String[] homeAddress = address.split("~");
        String returnStoreNo = null;
        if (homeAddress.length >= 7) {
            returnStoreNo = homeAddress[6];
            return returnStoreNo;
        } else {
            return "";
        }
    }
    public static String getBNFAddress(String address) {
        String[] homeAddress = address.split("~");
        String returnAddress = null;
        if (homeAddress.length >= 7) {
            returnAddress = homeAddress[0] + "~" + homeAddress[4];
            return returnAddress;
        } else {
            return "";
        }
    }
        //拼接受益人地址信息 HomeAddres
      public static String getbnfAddress(String address,String Province,String City, String Country,String HomeZipCode) {
          String[] homeAddress = address.split("~");
          String  returnAddress = Province + "&" + City + "&" + Country + "&" + homeAddress[1] + "&" + HomeZipCode;
          return returnAddress;
      }
     //获取投保人地址信息 HomeAddres
    public static String getAddress(String address) {
        String[] homeAddress = address.split("~");
        String returnAddress = null;
        if (homeAddress.length >= 6) {
            returnAddress = homeAddress[0] + "&" + homeAddress[1] + "&" + homeAddress[2] + "&" + homeAddress[3] + "&" + homeAddress[4];
            return returnAddress;
        } else {
            return "";
        }
    }

    //获取常住地址村/社区（楼、号）StoreNo
    public static String getStoreNo(String address) {
        String[] homeAddress = address.split("~");
        String returnStoreNo = null;
        if (homeAddress.length >= 6) {
            returnStoreNo = homeAddress[5];
            return returnStoreNo;
        } else {
            return "";
        }
    }


    //获取电话区号 ZoneCode
    public static String getZoneCode(String homePhone) {
        String returnHmonePhone = null;
        if (homePhone.indexOf("-") > -1) {
            String[] split = homePhone.split("-");
            returnHmonePhone = split[0];
            return returnHmonePhone;
        } else {
            return "";
        }
    }


    /**
     * @Description: 转换湖南xxx的bodyOut的交易编码
     * @Param: [resp]
     * @return: com.sinosoft.cloud.rest.TradeInfo
     * @Author: BaoYongmeng
     * @Date: 2018/8/3
     */
    public static String transOutHeadTransNo(String transNo) {

        switch (transNo) {
            case "1002": //新单试算
                transNo = "6000112";
                break;
            case "1004": //新单缴费
                transNo = "6000113";
                break;
            case "1009":  //取消交易
                transNo = "6001001";
                break;
            case "A0026":   //当日撤单
                transNo = "6000901";
                break;
            case "1021":    //保单查询
                transNo = "6000211";
                break;
            case "1018":    //保单重打
                transNo = "6000811";
                break;
        }
        return transNo;
    }

    /**
     * @Description: 计算保险期间的天数
     * @Param: [stardDate, EndDate, Flag]
     * @return: java.lang.String
     * @Author: BaoYongmeng
     * @Date: 2018/7/17
     */
    public static String calculationDays(String stardDate, String EndDate, String Flag) {
        return String.valueOf(PubFun.calInterval(stardDate, EndDate, Flag) + 1);
    }

    /**
     * @Description: 校验险种代码是否是SE开头的 所有SE开头的都转换为6810
     * @Param: [riskCode]
     * @return: java.lang.String
     * @Author: BaoYongmeng
     * @Date: 2018/6/27
     */
    public static String checkRiskCode(String riskCode) {
        /*String[] riskCodes = {"SEL","SEM","SEO"};
        for (int i = 0;i <riskCodes.length;i++){
            if (riskCode.equals(riskCodes[i])){
                break;
            }
            riskCode = "6810";
        }*/
        if (riskCode.indexOf("SE") != -1) {
            riskCode = "6810";
        }
       /* if (riskCode.indexOf("JSJXBA") != -1) {
            riskCode = "6810";
        }
        if (riskCode.indexOf("JSJXBB") != -1) {
            riskCode = "6810";
        }*/
        if (riskCode.indexOf("JSJXBC") != -1) {
            riskCode = "6810";
        }
        return riskCode;
    }

    //获取保单核保时的投保人的婚姻状况
    public static String getMarriage(String saleChnl) {
        String marriage = "";

        return marriage;
    }

    //将日期和时间通过空格分开
    //add by rs 20100819
    public static String  getDate(String cTranDate){
        String tranDate = "";
        if (cTranDate != null && !"".equals(cTranDate)) {
            tranDate = cTranDate.split(" ")[0];
        }
        return tranDate;
    }
    //将日期和时间通过空格分开
    public static String  getTime(String cTranDate){
        String tranDate = "";
        if (cTranDate != null && !"".equals(cTranDate)) {
            tranDate =cTranDate.split(" ")[1];
        }
        return tranDate;
    }

    /**
     *获取年月日  传入20190102112201 返回 20190102
     * @param cTranDate
     * @return
     */
    public static String getExactDate(String cTranDate){
        String tranDate="";
        if(cTranDate !=null&&!"".equals(cTranDate)){
            String s=cTranDate.substring(0,8);
            tranDate=s.substring(0, 4) + "-" + s.substring(4, 6) + "-" + s.substring(6, 8);
        }
        return tranDate;
    }
    /**
     *获取时间  传入20190102112201 返回 112201
     * @param cTranDate
     * @return
     */
    public static String getExactTime(String cTranDate){
        String tranDate="";
        if(cTranDate !=null&&!"".equals(cTranDate)){
            String s =cTranDate.substring(cTranDate.length()-6);
            tranDate=s.substring(0, 2) + ":" + s.substring(2, 4) + ":" + s.substring(4, 6);
        }
        return tranDate;
    }

    /**
     *将数字和年分开， 10Y 返回10
     * @param numYear
     * @return
     */
    public static String getPaymentNum(String numYear){
        String number="";
        if(numYear !=null&&!"".equals(numYear)){
            number=numYear.substring(0,numYear.length()-1);
        }
        return number;
    }
    /**
     *将数字和年分开， 10Y 返回Y
     * @param numYear
     * @return
     */
    public static String getPaymentYear(String numYear){
        String number="";
        if(numYear !=null&&!"".equals(numYear)){
            number=numYear.substring(numYear.length()-1);
        }
        return number;
    }

    /**
     * string 转 long
     * @param msg
     * @return
     */
    //end by rs
    public static Long getLongByString(String msg){
        return Long.parseLong(msg);

    }



    //获取yyyyMMdd格式的日期 ，如20160805
    public static String getFormatDate(String cTranDate) {
        String tranDate = "";
        if (cTranDate != null && !"".equals(cTranDate)) {
            tranDate = cTranDate.replaceAll("-", "").replaceAll("/", "").trim();
        }
        return tranDate;
    }

    //获取时间格式为hhmmss ，如152030
    public static String getFormatTime(String cTranTime) {
        String tranTime = "";
        if (cTranTime != null && !"".equals(cTranTime)) {
            tranTime = cTranTime.replaceAll(":", "").trim();
        }
        return tranTime;
    }

    /**
     * 获取日期时间类型字段，格式为yyyyMMdd hhmmss 如 20161012 152030
     *
     * @param cTranDate 2016-10-12 或 20161012
     * @param cTranTime 15:20:30 或 152030
     * @return 格式为yyyyMMdd hhmmss
     */
    public static String getFormatDateTime(String cTranDate, String cTranTime) {
        String tranDateTime = "";
        String tranDate = "";
        String tranTime = "";
        if (cTranDate != null && !"".equals(cTranDate)) {
            tranDate = cTranDate.replaceAll("-", "").replaceAll("/", "").trim();
        }
        if (cTranTime != null && !"".equals(cTranTime)) {
            tranTime = cTranTime.replaceAll(":", "").trim();
        }
        tranDateTime = tranDate + " " + tranTime;
        return tranDateTime;
    }

    /**
     * 获取日期时间类型字段，格式为yyyy-MM-dd HH:mm:ss 如 2016-10-12 15:20:30
     *
     * @param cTranDate 2016-10-12 或 20161012
     * @param cTranTime 15:20:30 或 152030
     * @return 格式为yyyy-MM-dd HH:mm:ss
     */
    public static String getFormatDateTime2(String cTranDate, String cTranTime) {
        String tranDateTime = "";
        String tranDate = "";
        String tranTime = "";
        String tempTranDate = "";
        String tempTranTime = "";

        if (cTranDate != null && !"".equals(cTranDate.trim()) && cTranDate.trim().length() <= 10) {
            tempTranDate = cTranDate.replace("-", "").replace("/", "").trim();
            tranDate = tempTranDate.substring(0, 4) + "-" + tempTranDate.substring(4, 6) + "-" + tempTranDate.substring(6, 8);
        } else {
            tranDate = cTranDate;
        }
        if (cTranTime != null && !"".equals(cTranTime.trim()) && cTranTime.trim().length() <= 8) {
            tempTranTime = cTranTime.replace(":", "").trim();
            tranTime = tempTranTime.substring(0, 2) + ":" + tempTranTime.substring(2, 4) + ":" + tempTranTime.substring(4, 6);
        } else {
            tranTime = cTranTime;
        }
        tranDateTime = tranDate + " " + tranTime;
        return tranDateTime;
    }

    /**
     * 传入的日期和时间格式为yyyy-MM-dd HH:mm:ss 或 yyyyMMdd HH:mm:ss 或 yyyy/MM/dd HH:mm:ss 或 yyyy-MM-dd HHmmss 或 yyyyMMdd HHmmss 或 yyyy/MM/dd HHmmss
     * 返回yyyyMMdd格式的日期 ，如：传入 2016-10-11 13:13:13 则返回 20161011
     *
     * @param cTranDateTime
     * @return yyyyMMdd格式的日期
     */
    public static String getFormatDateFromDateTime(String cTranDateTime) {
        String tranDate = "";
        if (cTranDateTime != null && !"".equals(cTranDateTime.trim())) {
            tranDate = cTranDateTime.trim().replace("-", "").replace("/", "").replace(":", "").substring(0, 8).trim();
        }
        return tranDate;
    }

    /**
     * 传入的日期和时间格式为yyyy-MM-dd HH:mm:ss 或 yyyyMMdd HH:mm:ss 或 yyyy/MM/dd HH:mm:ss 或 yyyy-MM-dd HHmmss 或 yyyyMMdd HHmmss 或 yyyy/MM/dd HHmmss
     * 返回时间格式为hhmmss ，如传入 2016-10-11 13:13:13 则返回 131313
     *
     * @param cTranDateTime
     * @return 时间格式为hhmmss的时间
     */
    public static String getFormatTimeFromDateTime(String cTranDateTime) {
        String tranTime = "";
        if (cTranDateTime != null && !"".equals(cTranDateTime.trim())) {
            String temp = cTranDateTime.trim().replace("-", "").replace("/", "").replace(":", "");
            tranTime = temp.substring(8, temp.length()).trim();
        }
        return tranTime;
    }

    /**
     * 功能描述：去除字符串首部为"0"字符
     *
     * @param str 传入需要转换的字符串
     * @return 转换后的字符串
     */
    public static String removeZero(String str) {
        char ch;
        String result = "";
        if (str != null && str.trim().length() > 0 && !str.trim().equalsIgnoreCase("null")) {
            try {
                for (int i = 0; i < str.length(); i++) {
                    ch = str.charAt(i);
                    if (ch != '0') {
                        result = str.substring(i);
                        break;
                    }
                }
            } catch (Exception e) {
                result = "";
            }
        } else {
            result = "";
        }
        return result;
    }

    /**
     * 获取以元为单位的金额 字符串类型
     *
     * @param fenMoney 传入以分为单位的金额
     * @return 转换后的金额字符串 以元为单位
     */
    public static String getYuanMoney(String fenMoney) {
        if (fenMoney == null)
            return "0.00";
        String s = fenMoney;
        int len = -1;
        StringBuilder sb = new StringBuilder();
        if (s != null && s.trim().length() > 0 && !s.equalsIgnoreCase("null")) {
            s = removeZero(s);
            if (s != null && s.trim().length() > 0 && !s.equalsIgnoreCase("null")) {
                len = s.length();
                int tmp = s.indexOf("-");
                if (tmp >= 0) {
                    if (len == 2) {
                        sb.append("-0.0").append(s.substring(1));
                    } else if (len == 3) {
                        sb.append("-0.").append(s.substring(1));
                    } else {
                        sb.append(s.substring(0, len - 2)).append(".").append(s.substring(len - 2));
                    }
                } else {
                    if (len == 1) {
                        sb.append("0.0").append(s);
                    } else if (len == 2) {
                        sb.append("0.").append(s);
                    } else {
                        sb.append(s.substring(0, len - 2)).append(".").append(s.substring(len - 2));
                    }
                }
            } else {
                sb.append("0.00");
            }
        } else {
            sb.append("0.00");
        }
        return sb.toString();
    }

    /**
     * 功能描述：金额字符串转换：单位元转成单位分
     *
     * @param yuanMoney 传入需要转换的金额字符串 单位元
     * @return 转换后的金额字符串 单位分
     */
    public static String getFenMoney(String yuanMoney) {
        if (yuanMoney == null)
            return "0";
        String s = yuanMoney;
        int posIndex = -1;
        String str = "";
        StringBuilder sb = new StringBuilder();
        if (s != null && s.trim().length() > 0 && !s.equalsIgnoreCase("null")) {
            posIndex = s.indexOf(".");
            if (posIndex > 0) {
                int len = s.length();
                if (len == posIndex + 1) {
                    str = s.substring(0, posIndex);
                    if (str == "0") {
                        str = "";
                    }
                    sb.append(str).append("00");
                } else if (len == posIndex + 2) {
                    str = s.substring(0, posIndex);
                    if (str == "0") {
                        str = "";
                    }
                    sb.append(str).append(s.substring(posIndex + 1, posIndex + 2)).append("0");
                } else if (len == posIndex + 3) {
                    str = s.substring(0, posIndex);
                    if (str == "0") {
                        str = "";
                    }
                    sb.append(str).append(s.substring(posIndex + 1, posIndex + 3));
                } else {
                    str = s.substring(0, posIndex);
                    if (str == "0") {
                        str = "";
                    }
                    sb.append(str).append(s.substring(posIndex + 1, posIndex + 3));
                }
            } else {
                sb.append(s).append("00");
            }
        } else {
            sb.append("0");
        }
        str = removeZero(sb.toString());
        if (str != null && str.trim().length() > 0 && !str.trim().equalsIgnoreCase("null")) {
            return str;
        } else {
            return "0";
        }
    }

    /**
     * 保险期限单位获取
     *
     * @param InsPeriod 带符号的保险期限
     * @return 最后一位保险期限单位若没有默认单位A(年龄)
     */
    public static String getInsuYearFlag(String InsPeriod) {
        if (InsPeriod != null && !"".equals(InsPeriod)) {
            return InsPeriod.substring(InsPeriod.length() - 1);
        }
        return "Y";
    }

    /**
     * 保险期限年期获取
     *
     * @param InsPeriod 带符号的保险期限
     * @return 去除保险期限最后一位的符号的保险年期
     */
    public static String getInsuYear(String InsPeriod) {
        if (InsPeriod != null && !"".equals(InsPeriod)) {
            return InsPeriod.substring(0, InsPeriod.length() - 1);
        }
        return "10";
    }

    /**
     * 受益人与被保人关系枚举值转换
     *
     * @param RelationToInsured 带符号的保险期限
     * @return 去除保险期限最后一位的符号的保险年期
     */
    public static String getRelationToInsured(String RelationToInsured) {
        if (RelationToInsured == null || "".equals(RelationToInsured)) return "";
        if ("01".equals(RelationToInsured) || "02".equals(RelationToInsured)) return "02";
        if ("03".equals(RelationToInsured) || "04".equals(RelationToInsured)) return "01";
        if ("05".equals(RelationToInsured) || "06".equals(RelationToInsured)) return "03";
        return RelationToInsured;
    }

    /**
     * 获取当前日期后一天
     *
     * @return
     */
    public static String getCurrDateAfterDay(String abFlag, String afterDay) {
        SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
        Date date = new Date();
        if ("A".equals(abFlag)) date.setTime(date.getTime() + (1000 * 60 * 60 * 24) * new Integer(afterDay));
        if ("B".equals(abFlag)) date.setTime(date.getTime() - (1000 * 60 * 60 * 24) * new Integer(afterDay));
        return format.format(date);
    }

    /**
     * 获取当前日期前五年
     *
     * @return
     */
    public static String getCurrDateBeforeyear(String abFlag, String beforeYear) {
        String currentDate = PubFun.getCurrentDate();
        String[] split = currentDate.split("-");
        Integer curryear = new Integer(split[0]);
        if ("A".equals(abFlag)) return (curryear + new Integer(beforeYear)) + split[1] + split[2];
        if ("B".equals(abFlag)) return (curryear - new Integer(beforeYear)) + split[1] + split[2];
        return "";
    }

    /**
     * 获取指定日期前后N天后的日期
     *
     * @param date     指定日期
     * @param abFlag   前后标记
     * @param afterDay 天数
     * @return
     */
    public static String getThisDateAfterDay(String date, String abFlag, String afterDay) {
        try {
            if (StringUtil.isNullOrEmpty(date)) return "";
            SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
            Date newdate = format.parse(date.replaceAll("-", "").replaceAll("/", ""));
            if ("A".equals(abFlag)) newdate.setTime(newdate.getTime() + (1000 * 60 * 60 * 24) * new Integer(afterDay));
            if ("B".equals(abFlag)) newdate.setTime(newdate.getTime() - (1000 * 60 * 60 * 24) * new Integer(afterDay));
            SimpleDateFormat tformat = new SimpleDateFormat("yyyyMMdd");
            return tformat.format(newdate);
        } catch (ParseException e) {
            e.printStackTrace();
            return "";
        }
    }

    /**
     * 通过告知类型,告知编码获取对应告知的详细内容
     *
     * @param ImpartVer
     * @param ImpartCode
     * @return
     */
    public static String getImpartContent(String ImpartVer, String ImpartCode) {

        if ("29".equals(ImpartVer) && "7a".equals(ImpartCode)) {
            return "被保险人是否曾被任何保险公司拒保、延期?";
        }
        if ("29".equals(ImpartVer) && "7b".equals(ImpartCode)) {
            return "被保险人是否目前患有或曾经患有下列疾病或症状? 先天性心脏病、心力衰竭；脑出血、脑栓塞、脑动静脉血管瘤及畸形；肝硬化、肝衰竭；尿毒症、肾衰竭；恶性肿瘤或脑部良性肿瘤；先天性疾病、遗传性疾病、职业病（如尘肺、矽肺）；是否有身体残障，酒精或药物滥用成瘾?";
        }
        if ("29".equals(ImpartVer) && "7c".equals(ImpartCode)) {
            return "您是否曾于过去三年内被要求接受手术、病理检查、放化疗?或因病计划在近期进行手术?";
        }
        if ("Agent".equals(ImpartVer) && "Name".equals(ImpartCode)) {
            return "网销业务员";
        }
        return "";
    }


    public static String getCodeName(String CodeType, String CodeValue) {
        if ("EdorReasonCode".equals(CodeType) && "02".equals(CodeValue)) {
            return "急需用钱";
        }
        return "";
    }


    public static String getCurrValueAddOne(String value) {
        try {
            return (new Integer(value) + 1) + "";
        } catch (NumberFormatException e) {
            e.printStackTrace();
            return "";
        }
    }


    /**
     * 将金额从元转换为分，参数是string类型
     *
     * @param pYuan
     * @return
     */
    public static long yuanToFen(String pYuan) {
        return pYuan != null && !"".equals(pYuan) ? Math.round(Double.parseDouble(pYuan) * 100.0D) : 0L;
    }

    /**
     * 将金额从元转换为分，参数是double类型
     *
     * @param pYuan
     * @return
     */
    public static long yuanToFen(double pYuan) {
        return Math.round(pYuan * 100.0D);
    }

    /**
     * 将金额从分转换为元，参数是string类型
     *
     * @return
     */
    public static String fenToYuan(String pFen) {
        return pFen != null && !"".equals(pFen) ? (new DecimalFormat("0.00")).format((double) Long.parseLong(pFen) / 100.0D) : pFen;
    }

    /**
     * 将金额从分转换为元，参数是long类型
     *
     * @return
     */
    public static String fenToYuan(long pFen) {
        return (new DecimalFormat("0.00")).format((double) pFen / 100.0D);
    }

    /**
     * 在返回结果后面加上N个空格。N代表第二个参数。
     * 两个参数的方法：第一个参数是字符串，第二个参数是多少个空格
     *
     * @param pSrcStr
     * @param pLength
     * @return
     */
    public static String fillStrWith_2Args(String pSrcStr, int pLength) {
        return fillStrWith_(pSrcStr, pLength, false);
    }

    /**
     * 在返回结果后面加上N个空格,N代表第二个参数，第三个参数代表
     * 三个参数的方法
     *
     * @param pSrcStr
     * @param pLength
     * @return
     */
    public static String fillStrWith_(String pSrcStr, int pLength, boolean pLeftAdd) {
        return fillStr(pSrcStr, pLength, ' ', pLeftAdd);
    }

    public static String fillStr(String pSrcStr, int pLength, char pFillChar, boolean pLeftAdd) {
        return fillStr(pSrcStr, pLength, pFillChar, pLeftAdd, "GBK");
    }

    public static String fillStr(String pSrcStr, int pLength, char pFillChar, boolean pLeftAdd, String pCharset) {
        if (pSrcStr == null) {
            return null;
        } else {
            int mSrcBytesLen = 0;

            try {
                mSrcBytesLen = pSrcStr.getBytes(pCharset).length;
            } catch (UnsupportedEncodingException var8) {
                var8.printStackTrace();
            }

            StringBuilder mStrBuilder = new StringBuilder();
            int i;
            if (pLeftAdd) {
                for (i = mSrcBytesLen; i < pLength; ++i) {
                    mStrBuilder.append(pFillChar);
                }

                mStrBuilder.append(pSrcStr);
            } else {
                mStrBuilder.append(pSrcStr);

                for (i = mSrcBytesLen; i < pLength; ++i) {
                    mStrBuilder.append(pFillChar);
                }
            }

            return mStrBuilder.toString();
        }
    }

    /**
     * 将八位日期字符串转换为十位日期字符串。例如yyyyMMdd转换为yyyy-MM-dd
     *
     * @param pDate
     * @return
     */
    public static String date8to10(String pDate) {
        if (pDate != null && !"".equals(pDate)) {
            char[] mChars = pDate.toCharArray();
            return (new StringBuilder()).append(mChars, 0, 4).append('-').append(mChars, 4, 2).append('-').append(mChars, 6, 2).toString();
        } else {
            return pDate;
        }
    }

    /**
     * 将十位日期字符串转换为八位日期字符串。例如yyyy-MM-dd转换为yyyyMMdd
     *
     * @param pDate
     * @return
     */
    public static String date10to8(String pDate) {
        return pDate != null && !"".equals(pDate) ? pDate.substring(0, 4) + pDate.substring(5, 7) + pDate.substring(8) : pDate;
    }

    /**
     * 将字符串转换为boolean
     *
     * @param booleanStr
     * @return
     */
    public static Boolean parseBoolean(String booleanStr) {
        return Boolean.parseBoolean(booleanStr);
    }

    /**
     * 投保人家庭年收入处理
     *
     * @param Flag
     * @param appntIncome
     * @return
     */
    public static String appntIncomeFormat(String Flag, String appntIncome) {

        if (!("").equals(Flag) && (Flag != null && ("01").equals(Flag) || Flag != null && ("02").equals(Flag))) {
//            appntIncome=appntIncome.indexOf("-")>-1 ? appntIncome.substring(0,appntIncome.indexOf("-")):appntIncome;
//            if(("5").equals(appntIncome)){//投保人年收入为5-10万
//                return appntIncome;
//            }else{
//                String regEX="[^0-9]";
//                Pattern p = Pattern.compile(regEX);
//                Matcher m = p.matcher(appntIncome);
//                appntIncome=m.replaceAll("");//将匹配到的matcher数字转化为String类型数字可用m.replaceAll("")
//                if(!("").equals(appntIncome)&&appntIncome!=null&&Double.parseDouble(appntIncome)<=5){//投保人年收入为5万以下
//                    appntIncome="0";
//                }
//            }
            appntIncome = appntIncome.indexOf("-") > -1 ? appntIncome.substring(0, appntIncome.indexOf("-")) : appntIncome;
            if (!appntIncome.contains("万")) {
                return appntIncome;
            } else {
                return appntIncome.split("万")[0];
            }
        } else if (!("").equals(Flag) && Flag != null && ("04").equals(Flag)) {
            if (!("").equals(appntIncome) && appntIncome != null) {
                double numberIncome = Double.parseDouble(appntIncome) / 10000;
                appntIncome = Double.toString(numberIncome);
            }
        } else {
            if (!("").equals(appntIncome) && appntIncome != null) {
                double numberIncome = Double.parseDouble(appntIncome) / 10000;
                appntIncome = Double.toString(numberIncome);
            }
        }
        return appntIncome;
    }

    /**
     * 投保人家庭年收入处理
     *
     * @param Flag
     * @param appntIncome
     * @return
     */
    public static String insuredIncomeFormat(String Flag, String appntIncome) {
        if (!("").equals(Flag) && (Flag != null && ("01").equals(Flag) || Flag != null && ("02").equals(Flag))) {
//            appntIncome=appntIncome.indexOf("-")>-1 ? appntIncome.substring(0,appntIncome.indexOf("-")):appntIncome;
//            if(("5").equals(appntIncome)){//投保人年收入为5-10万
//                return appntIncome;
//            }else{
//                String regEX="[^0-9]";
//                Pattern p = Pattern.compile(regEX);
//                Matcher m = p.matcher(appntIncome);
//                appntIncome=m.replaceAll("");//将匹配到的matcher数字转化为String类型数字可用m.replaceAll("")
//                if(!("").equals(appntIncome)&&appntIncome!=null&&Double.parseDouble(appntIncome)<=5){//投保人年收入为5万以下
//                    appntIncome="0";
//                }
//            }
            appntIncome = appntIncome.indexOf("-") > -1 ? appntIncome.substring(0, appntIncome.indexOf("-")) : appntIncome;
            if (!appntIncome.contains("万")) {
                return appntIncome;
            } else {
                return appntIncome.split("万")[0];
            }
        } else if (!("").equals(Flag) && Flag != null && ("04").equals(Flag)) {
            if (!("").equals(appntIncome) && appntIncome != null) {
                double numberIncome = Double.parseDouble(appntIncome) / 10000;
                appntIncome = Double.toString(numberIncome);
            }
        } else {
            if (!("").equals(appntIncome) && appntIncome != null) {
                double numberIncome = Double.parseDouble(appntIncome) / 10000;
                appntIncome = Double.toString(numberIncome);
            }
        }
        return appntIncome;
    }

    /**
     * 投保人家庭年收入处理
     *
     * @return
     */
    public static String appntFamilyIncomeFormat(String familyMoney) {
        double d;
        try {
            d = Double.parseDouble(familyMoney);
        } catch (NumberFormatException e) {
            return familyMoney;
        }

        String wanYuan = String.valueOf(d / 10000);

        if (wanYuan.contains("e") || wanYuan.contains("E")) {
            BigDecimal bigDecimal = new BigDecimal(wanYuan);
            wanYuan = bigDecimal.toString();
        }
        return wanYuan;
    }

    /**
     * 按给定的长度，输出字符串
     *
     * @param srcStr 源串，使用“,”分隔不同串
     * @param lenStr 长度定义，使用“,”分隔不同串
     * @return 拼好的结果
     */
    public static String fixedLengthString(String srcStr, String lenStr) {
        if (srcStr == null || lenStr == null || srcStr.length() == 0
                || lenStr.length() == 0)
            return "";
        String[] srcArr = srcStr.split(",", -1);
        String[] lenArr = lenStr.split(",", -1);
        if (srcArr.length != lenArr.length) {
            // logger.error(""+srcArr.length+","+lenArr.length);
            return "长度非法";
        }
        StringBuffer resultBuffer = new StringBuffer();
        try {
            for (int i = 0; i < srcArr.length; i++) {
                int tLength = Integer.parseInt(lenArr[i]);
                resultBuffer.append(cutStringByByteLength(srcArr[i], tLength));
            }
        } catch (Exception e) {
            e.printStackTrace();
            return e.getMessage();
        }
        return resultBuffer.toString();
    }

    private static String cutStringByByteLength(String src, int length) {

        int srcLength = src.length();
        int sumLength = 0;
        for (int i = 0; i < srcLength; i++) {
            if (src.charAt(i) > 255) {
                sumLength += 2;
            } else {
                sumLength += 1;
            }
            if (sumLength == length)
                return src.substring(0, i + 1);
            else if (sumLength - length == 1) {
                if (src.charAt(i - 1) > 255)
                    return src.substring(0, i) + " ";
                else {
                    return src.substring(0, i);
                }
            }
        }
        StringBuffer sb = new StringBuffer(src);
        for (int i = sumLength; i < length; i++) {
            sb.append(' ');
        }
        return sb.toString();
    }

    /**
     * 时间格式转换 eg:000000 to 00:00:00
     *
     * @param src
     * @return
     */
    public static String timeFormat(String src) {

        if (src == null || "".equals(src)) {
            return "";
        }
        if (8 == src.length()) {
            return src;
        }
        if (6 != src.length()) {
            return "长度非法";
        }

        StringBuffer sb = new StringBuffer();
        sb.append(src.substring(0, 2)).append(":").append(src.substring(2, 4)).append(":").append(src.substring(4));
        return sb.toString();
    }

    /**
     * 转化阿拉伯金额为中国大写金额格式
     *
     * @param moeny
     * @return
     */
    public static String getChnMoney(String moeny) {
        return MoenyUtil.getChnMoney(moeny);
    }

    /**
     * 计算日期
     *
     * @param baseDate
     * @return
     */
    public static String getCalDate(String baseDate) {
        return date10to8(PubFun.calDate(baseDate, -1, "D", ""));
    }

    /**
     * 将yyyy-MM-dd转换为yyyy年MM月dd日
     *
     * @param dateStr
     * @return
     */
    public static String dateFormat(String dateStr) {
        return TimeUtil.dateFormat(dateStr);
    }

    /**
     * 将yyyyMMdd转换为yyyy-MM-dd
     *
     * @param dateStr yyyyMMdd格式的日期字符串
     * @return
     */
    public static String dateFormat8To10(String dateStr) {
        return TimeUtil.dateString8To10(dateStr);
    }

    /**
     * 将hh:ss:mm转换为hhssmm
     *
     * @param timeStr hhssmm格式的时间字符串
     * @return
     */
    public static String timeString10To8(String timeStr) {
        return TimeUtil.timeString10To8(timeStr);
    }

    /**
     * 将保单生效日改为次日
     *
     * @param cValiDate-投保日期（String类型） 当日投保日期
     * @return 保单生效日期, yyyy-MM-dd（String类型）
     */
    public static String cValiDate(String cValiDate) {
        return TimeUtil.getCValiDate(cValiDate);
    }

    /**
     * 将保单生效日改为次日
     *
     * @param cValiDate-投保日期（String类型） 当日投保日期，传入日期格式为2018-10-15
     * @return 保单生效日期, yyyy-MM-dd（String类型）
     */
    public static String cValiDateOther(String cValiDate) {
        return TimeUtil.getCValiDateOther(cValiDate);
    }

    /**
     * 架构重组后xxx客户收入告知计算.
     *
     * @param flag         xxx子渠道
     * @param income       原始报文中的投保人年收入
     * @param familyIncome 原始报文中的投保人家庭年收入
     * @return
     */
    public static String incomeFormat(String flag, String income, String familyIncome) {

        String customerIncome = "";
        String customerFamilyIncome = "";

        if (StringUtils.isEmpty(income)) {
            customerIncome = "*";
        } else if (!TimeUtil.regExp(income,"^([1-9]\\d+|\\d{1})(\\.\\d+)?$") &&(flag != null && ("11").equals(flag) || flag != null && ("22").equals(flag))){
            customerIncome = income;
        }else {
            if (!("").equals(flag) && (flag != null && ("01").equals(flag) || flag != null && ("02").equals(flag) || flag != null && ("21").equals(flag))) {
                income = income.indexOf("-") > -1 ? income.substring(0, income.indexOf("-")) : income;
                if (!income.contains("万")) {
                    customerIncome = income;
                } else {
                    customerIncome = income.split("万")[0];
                }
            } else if (!("").equals(flag) && flag != null && ("04").equals(flag)) {
                if (!("").equals(income) && income != null) {
                    double numberIncome = Double.parseDouble(income) / 10000;
                    income = Double.toString(numberIncome);
                    customerIncome = income;
                }
            } else {
                if (!("").equals(income) && income != null) {
                    double numberIncome = Double.parseDouble(income) / 10000;
                    income = Double.toString(numberIncome);
                    customerIncome = income;
                }
            }
        }

        if (customerIncome.contains("e") || customerIncome.contains("E")) {
            BigDecimal bigDecimal = new BigDecimal(customerIncome);
            customerIncome = bigDecimal.toString();
        }


        if (StringUtils.isEmpty(familyIncome)) {
            customerFamilyIncome = "*";
        } else {
            double d;
            try {
                d = Double.parseDouble(familyIncome);
            } catch (NumberFormatException e) {
                return familyIncome;
            }

            String wanYuan = String.valueOf(d / 10000);

            if (wanYuan.contains("e") || wanYuan.contains("E")) {
                BigDecimal bigDecimal = new BigDecimal(wanYuan);
                wanYuan = bigDecimal.toString();
            }
            customerFamilyIncome = wanYuan;
        }
        String result = customerIncome + ";" + "1" + ";" + customerFamilyIncome + ";";

        return result;
    }

    /**
     * 架构重组后xxx被保人身高体重计算
     *
     * @param stature     身高
     * @param avoirdupois 体重
     * @return
     */
    public static String calStatureAndAvoirdupois(String stature, String avoirdupois) {

        String insuredHeight = "";
        String insuredWeight = "";
        insuredHeight = stature;
        insuredWeight = avoirdupois;
        if (org.apache.commons.lang.StringUtils.isEmpty(stature)) {
            insuredHeight = "*";
        }
        if (org.apache.commons.lang.StringUtils.isEmpty(avoirdupois)) {
            insuredWeight = "*";
        }

        String result = insuredHeight + ";" + insuredWeight;

        return result;
    }

    /**
     * 将保单生效日改为次日
     *
     * @param cValiDate-投保日期（String类型） 当日投保日期
     * @return 保单生效日期, yyyy-MM-dd（String类型）
     */
    public static String getCurrentDate(String cValiDate) {
        return TimeUtil.getCurrentDate();
    }

    /**
     * getProductCode
     *
     * @param ProductCode
     * @return
     * add by rs
     * 20190824
     */
    public  String getProductCode(String ProductCode){

        LDPlanPojo ldPlanPojo=redisCommonDao.getEntityRelaDB(LDPlanPojo.class, ProductCode);
        if(ldPlanPojo!=null){
            return ProductCode;
        }
        return "";

    }
    //end
    public static String getContPlanCode(String Amnt, String RiskCode, String RiskCodeWr, String Prem, String RiskCode1) {
        // 险种计划
        String ContPlanCode = "";
        //判断险种是否是6807
        if (!("").equals(RiskCode) && null != RiskCode && ("6807").equals(RiskCode)) {
            if (("").equals(RiskCode1)) {
                ContPlanCode = "YA";
            } else {
                ContPlanCode = "1001";//不能有附加险
            }
            if (null == ContPlanCode || ("").equals(ContPlanCode)) {
                ContPlanCode = "0000";
            }
            return ContPlanCode;
        }
        // 6810的riskcode的传值有以下几种（6810a\6810b\...\6810\SEA\SEL\...）等，
        //如果是SEA、SEL这样的，就直接返回，如果是6810a这类的需要下面的判断，如果是6810也需要下面的判断
        String[] riskCodes = RiskCode.split("6810");
        //如果传入的RiskCode不是6810 则进行RiskCode转换
        if (!("6810").equals(RiskCode)) {
            //判断RiskCode是否 是以6810开头（例：6810a）
            if (("").equals(riskCodes[0])) {
                //将Riskcode置为6810
                RiskCode = "6810";
            } else {

                //除了SEO、SEL、SEM这三个意外其余的不是一6810或6810x开头的全部返回错误信息
                //RiskCode.equals("JSJXBA")||RiskCode.equals("JSJXBB")||
                if (RiskCode.equals("SEO") || RiskCode.equals("SEL") || RiskCode.equals("SEM")||RiskCode.equals("JSJXBC")||RiskCode.equals("SEP")) {
                    if (RiskCode.equals("SEP")){
                        switch (Prem){
                            case "6":
                                return "SEP1";
                            case "18":
                                return "SEP3";
                            case "30":
                                return "SEP6";
                        }
                    } else {
                        return RiskCode;
                    }
                } else {
                    ContPlanCode = "0000";
                    return ContPlanCode;
                }
            }
        }
        String SEFLAG = "DEFUALT";

        if (RiskCode.equals("6810") && RiskCodeWr != null && !"".equals(RiskCodeWr) && RiskCodeWr.equals("6810a")) {
            SEFLAG = "SEH";
        } else if (RiskCode.equals("6810") && RiskCodeWr != null && !"".equals(RiskCodeWr) && RiskCodeWr.equals("6810b")) {
            SEFLAG = "SEI";
        } else if (RiskCode.equals("6810") && RiskCodeWr != null && !"".equals(RiskCodeWr) && RiskCodeWr.equals("6810c")) {
            SEFLAG = "SEG";
        } else if (RiskCode.equals("6810") && RiskCodeWr != null && !"".equals(RiskCodeWr) && RiskCodeWr.equals("6810d")) {
            SEFLAG = "SEF";
        }
        if ((null == Prem || ("").equals(Prem)) || (null != SEFLAG && !("").equals(SEFLAG))) {
            if (Amnt.equals("500000") && SEFLAG.equals("DEFUALT")) {
                ContPlanCode = "SEA";
            } else if (Amnt.equals("1000000") && SEFLAG.equals("DEFUALT")) {
                ContPlanCode = "SEB";
            } else if (Amnt.equals("1500000") && SEFLAG.equals("DEFUALT")) {
                ContPlanCode = "SEC";
            } else if (Amnt.equals("2000000") && SEFLAG.equals("DEFUALT")) {
                ContPlanCode = "SED";
            } /*else if (Amnt.equals("200000") && SEFLAG.equals("DEFUALT")) {
                    ContPlanCode = "SEE";
                } */ else if (Amnt.equals("1000000") && SEFLAG.equals("SEF")) { //6810d
                ContPlanCode = "SEF";
            } else if (Amnt.equals("500000") && SEFLAG.equals("SEG")) { //6810c
                ContPlanCode = "SEG";
            } else if (Amnt.equals("1000000") && SEFLAG.equals("SEH")) { //6810a
                ContPlanCode = "SEH";
            } else if (Amnt.equals("5000000") && SEFLAG.equals("SEI")) { //6810b
                ContPlanCode = "SEI";
            } else {
                SEFLAG = "DEFUALT";
            }
        }

        if (Amnt.equals("13500000") && Prem.equals("338") && SEFLAG.equals("DEFUALT")) {
            ContPlanCode = "SEF";
        } else if (Amnt.equals("4500000") && Prem.equals("158") && SEFLAG.equals("DEFUALT")) {
            ContPlanCode = "SEG";
        } else if (Amnt.equals("4500000") && Prem.equals("98") && SEFLAG.equals("DEFUALT")) {
            ContPlanCode = "SEH";
        } else if (Amnt.equals("5000000") && Prem.equals("100") && SEFLAG.equals("DEFUALT")) {
            ContPlanCode = "SEI";
        } else if ((Amnt.equals("7000000") || Amnt.equals("500000")) && Prem.equals("118") && SEFLAG.equals("DEFUALT")) {
            ContPlanCode = "SEL";
        } else if ((Amnt.equals("7500000") || Amnt.equals("500000")) && Prem.equals("168") && SEFLAG.equals("DEFUALT")) {
            ContPlanCode = "SEM";
        }/*else if((Amnt.equals("500000") || Amnt.equals("1000000")) && Prem.equals("0")&&SEFLAG.equals("DEFUALT")){
            ContPlanCode = "SEN";
        }*/
        if (null == ContPlanCode || ("").equals(ContPlanCode)) {
            ContPlanCode = "0000";
        }
        return ContPlanCode;
    }


    /**
     * 份数string转化为int
     *
     * @param src 传入的份数Mult  切割为整数类型
     * @return
     */
    public static Integer policyAmmount(String src) {
        String[] c = src.split("\\.");
        String c1 = c[0];
        return Integer.parseInt(c1);
    }

    /**
     * 补交金额转换
     *
     * @param src 传入TransferPrem  切割为整数类型
     * @return
     */
    public static String getNTransferPrem(double src) {
        String s;
        if (src < 0) {
            s = String.valueOf(Math.abs(src));
        } else if (src > 0) {
            s = "0";
        } else {
            s = "0";
        }
        return s;
    }

    /**
     * 退款金额转换，
     *
     * @param src 传入的份数TransferPrem  根据正负数进行转换
     * @return
     */
    public static String getOTransferPrem(double src) {
        String s;
        if (src > 0) {
            s = String.valueOf(Math.abs(src));
        } else if (src < 0) {
            s = "0";
        } else {
            s = "0";
        }
        return s;
    }

    /**
     * @param arg  lccont的prem
     * @param arg1 bqresult 的转保金额
     * @return 保单和转保金额的差值
     */
    public static String getZBPrem(double arg, double arg1) {
        String prem;
        BigDecimal bdprem = new BigDecimal(String.valueOf(arg));
        BigDecimal bdamt = new BigDecimal(String.valueOf(arg1));
        prem = String.valueOf(bdprem.subtract(bdamt));
        return prem;
    }

    /**
     * @Description: 返回系统当前时间
     * @Param: []
     * @return: java.lang.String
     * @Author: BaoYongmeng
     * @Date: 2018/6/20
     */
    public static String getDateTimeNow() {
        SimpleDateFormat format = new SimpleDateFormat("yyyyMMddhhmmss");
        Date date = new Date();
        return format.format(date);
    }

    /**
     * @param min  要加减的分钟数
     * @param flag 加减标记 ADD 为加 REDUCE 为减
     * @Description: 返回系统当前时间 加减指定时间后的时间
     * @Param: []
     * @return: java.lang.String
     * @Author: BaoYongmeng
     * @Date: 2018/6/20
     */
    public static String getDateTimeNowAddOrReduce(String min, String flag) {
        SimpleDateFormat format = new SimpleDateFormat("yyyyMMddHHmmss");
        Date date = new Date();
        long time = Integer.parseInt(min) * 60 * 1000;// min 分钟
        if (("ADD").equals(flag)) {
            date = new Date(date.getTime() + time);
        } else {
            date = new Date(date.getTime() - time);
        }
        return format.format(date);
    }


    /**
     * 计算客户生日
     *
     * @param birthday 生日字符串
     * @return
     */
    public static String checkReserve4ByInsuredAgeTo18(String birthday, String reserve4) {
        LocalDate now = LocalDate.now();
        LocalDate birthdayDate = LocalDate.of(Integer.parseInt(birthday.substring(0, 4)), Integer.parseInt(birthday.substring(4, 6)), Integer.parseInt(birthday.substring(6, 8)));
        int years = birthdayDate.until(now).getYears();
        if (years > 18) {
            return "true";
        }
        if (StringUtils.isEmpty(reserve4) && years < 18) {
            return "false";
        }
        return "true";
    }

    /**
     * @param birthday 生日字符串 transDate交易日期   polApplyDate 受理日期
     * @return
     */
     /*
        //1受理日期 和 交易日期  都在生日之后 或者 受理日期 和 交易日期和生日是同一天的情况，生效日是交易日期的下一天
        //2 受理日期 和 交易日期 都在生日前一天，生效日追溯到生日前一天
        //3 受理日期 和 交易日期 早于生日前一天，生效日是交易日期下一天
        //4受理日期 在 生日之前，交易日期在生日后，生效日追溯到生日的前一天（不包括 受理日期 和 交易日期是当天的情况,） （包括：受理日期在生日之前，交易日期在生日当天）
       */
    public static String modifyCValiDate(String transDate, String birthday, String polApplyDate) {
        String abirthday = birthday.substring(5);
        String yearbirthday = transDate.substring(0, 5);
        //newtransdate  交易日期下一天 截去 年份
        String newtransdate = TimeUtil.getCValiDateOther(transDate).substring(5);
        final SimpleDateFormat sf = new SimpleDateFormat("MM-dd");
        Date mbirthday = null;
        Date mnewtransdate = null;
        try {
            mbirthday = sf.parse(abirthday);
            mnewtransdate = sf.parse(newtransdate);
        } catch (ParseException e) {
            cLogger.error("日期转换异常");
            cLogger.error(e);
            e.printStackTrace();
            return "";
        }

        int polage = PubFun.calInterval(birthday, polApplyDate, "Y");
        int cvalage = PubFun.calInterval(birthday, TimeUtil.getCValiDateOther(transDate), "Y");
        if (polage < cvalage) {
            return TimeUtil.getCValiDateOther1(yearbirthday.concat(abirthday));
        } else {
            return TimeUtil.getCValiDateOther(transDate);
        }
    }

    /**
     * 计算生效日
     * 计算原则： 1受理日期和交易日期都在生日之前或者之后，生效日是下一天
     * 2受理日期在生日之前，交易日期都在生日之后，则追溯到生日的前一天
     * <p>
     * //1 受理日期 和 交易日期 都在生日之前，生效日是交易日期的下一天
     * //2受理日期 和 交易日期  都在生日之后，生效日是交易日期的下一天（包括 受理日期 和 交易日期是当天的情况）
     * //3受理日期 在 生日之前，交易日期在生日后，生效日追溯到生日的前一天（不包括 受理日期 和 交易日期是当天的情况,） （包括：受理日期在生日之前，交易日期在生日当天）
     *
     * @param birthday 生日字符串 transDate交易日期   polApplyDate 受理日期
     * @return
     */
    public static String modify1035CValiDate(String transDate, String birthday, String polApplyDate, String appbirthday) {

        String insubirthday = birthday.substring(5);
        String yearbirthday = transDate.substring(0, 5);
        String appntbirthday = appbirthday.substring(5);
        String polapplydate=polApplyDate.substring(5);
        final SimpleDateFormat sf = new SimpleDateFormat("MM-dd");
        Date minsubirthday = null;
        Date mappntbirthday = null;
        Date mpolapplydate = null;
        try {
            mappntbirthday = sf.parse(appntbirthday);
            minsubirthday = sf.parse(insubirthday);
            mpolapplydate = sf.parse(polapplydate);
        } catch (ParseException e) {
            cLogger.error("日期转换异常");
            cLogger.error(e);
            e.printStackTrace();
            return "";
        }
        //计算被保人 试算 insupolage和 签单insucvalage 年龄
        int insupolage = PubFun.calInterval(birthday, polApplyDate, "Y");
        int insucvalage = PubFun.calInterval(birthday, TimeUtil.getCValiDateOther(transDate), "Y");
        //计算投保人 试算 apppolage appcvalage 年龄
        int apppolage = PubFun.calInterval(appbirthday, polApplyDate, "Y");
        int appcvalage = PubFun.calInterval(appbirthday, TimeUtil.getCValiDateOther(transDate), "Y");
        //投保人被保人年龄都有变化
        if ((insupolage < insucvalage) && (apppolage < appcvalage)){
            if ((mpolapplydate).before(mappntbirthday)&&(mpolapplydate).before(minsubirthday)){//都变化且都符合追溯
                if (mappntbirthday.before(minsubirthday)){//投保人比被保人生日早
                    return TimeUtil.getCValiDateOther1(yearbirthday.concat(appntbirthday));
                }else {//被保人比投保人生日早
                    return TimeUtil.getCValiDateOther1(yearbirthday.concat(insubirthday));
                }
                //符合被保人追溯，不符合投保人追溯
            }else if ((mpolapplydate).before(minsubirthday)&&((mpolapplydate).after(mappntbirthday)||(mpolapplydate).equals(mappntbirthday))){
                return TimeUtil.getCValiDateOther1(yearbirthday.concat(insubirthday));
            }else {//符合投保人追溯，不符合被保人追溯
                return TimeUtil.getCValiDateOther1(yearbirthday.concat(appbirthday));
            }
        } else if (insupolage < insucvalage) {   //被保人年龄有变化
            return TimeUtil.getCValiDateOther1(yearbirthday.concat(insubirthday));
        } else if (apppolage < appcvalage) {//投保人年龄有变化
            return TimeUtil.getCValiDateOther1(yearbirthday.concat(appntbirthday));
        } else {// 正常次日生效
            return TimeUtil.getCValiDateOther(transDate);
        }
    }
    /*
     *计算保险期间年数
     * @param beginDate 20160919000000 保险起期
     * @parm  endDate  20370918235959
     */
    public static String calInterval3(String beginDate,String endDate){
        return String.valueOf(PubFun.calInterval3(getExactDate(beginDate),getExactDate(endDate),"Y"));
    }
    /**
     * 生成流水号
     */
    public static String CreateMaxNo(String no){
        return PubFun1.CreateMaxNo(PubFun1.PhysicNo, 22);
    }
}
