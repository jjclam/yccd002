package com.sinosoft.cloud.access.entity;

/**
 * @Description
 * @Author:zhaoshulei
 * @Date:2018/4/10
 */
public class LAAgent {
    private String AgentCode;
    private String AgentGroup;
    private String ManageCom;
    private String Password;
    private String EntryNo;
    private String Name;
    private String Sex;
    private String Birthday;
    private String NativePlace;
    private String Nationality;
    private String Marriage;
    private String CreditGrade;
    private String HomeAddressCode;
    private String HomeAddress;
    private String PostalAddress;
    private String ZipCode;
    private String Phone;
    private String BP;
    private String Mobile;
    private String EMail;
    private String MarriageDate;
    private String IDNo;
    private String Source;
    private String BloodType;
    private String PolityVisage;
    private String Degree;
    private String GraduateSchool;
    private String Speciality;
    private String PostTitle;
    private String ForeignLevel;
    private int WorkAge;
    private String OldCom;
    private String OldOccupation;
    private String HeadShip;
    private String RecommendAgent;
    private String Business;
    private String SaleQuaf;
    private String QuafNo;
    private String QuafStartDate;
    private String QuafEndDate;
    private String DevNo1;
    private String DevNo2;
    private String RetainContNo;
    private String AgentKind;
    private String DevGrade;
    private String InsideFlag;
    private String FullTimeFlag;
    private String NoWorkFlag;
    private String TrainDate;
    private String EmployDate;
    private String InDueFormDate;
    private String OutWorkDate;
    private String RecommendNo;
    private String CautionerName;
    private String CautionerSex;
    private String CautionerID;
    private String CautionerBirthday;
    private String Approver;
    private String ApproveDate;
    private double AssuMoney;
    private String Remark;
    private String AgentState;
    private String QualiPassFlag;
    private String SmokeFlag;
    private String RgtAddress;
    private String BankCode;
    private String BankAccNo;
    private String Operator;
    private String MakeDate;
    private String MakeTime;
    private String ModifyDate;
    private String ModifyTime;
    private String BranchType;
    private String TrainPeriods;
    private String BranchCode;
    private int Age;
    private String ChannelName;
    private String ReceiptNo;
    private String IDNoType;
    private String BranchType2;
    private String TrainPassFlag;
    private String EmergentLink;
    private String EmergentPhone;
    private String RetainStartDate;
    private String RetainEndDate;
    private String TogaeFlag;
    private String ArchieveCode;
    private String Affix;
    private String AffixName;
    private String BankAddress;
    private String DXBranchtype2;
    private String DXSaleOrg;

    public String getAgentCode() {
        return AgentCode;
    }

    public void setAgentCode(String agentCode) {
        AgentCode = agentCode;
    }

    public String getAgentGroup() {
        return AgentGroup;
    }

    public void setAgentGroup(String agentGroup) {
        AgentGroup = agentGroup;
    }

    public String getManageCom() {
        return ManageCom;
    }

    public void setManageCom(String manageCom) {
        ManageCom = manageCom;
    }

    public String getPassword() {
        return Password;
    }

    public void setPassword(String password) {
        Password = password;
    }

    public String getEntryNo() {
        return EntryNo;
    }

    public void setEntryNo(String entryNo) {
        EntryNo = entryNo;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getSex() {
        return Sex;
    }

    public void setSex(String sex) {
        Sex = sex;
    }

    public String getBirthday() {
        return Birthday;
    }

    public void setBirthday(String birthday) {
        Birthday = birthday;
    }

    public String getNativePlace() {
        return NativePlace;
    }

    public void setNativePlace(String nativePlace) {
        NativePlace = nativePlace;
    }

    public String getNationality() {
        return Nationality;
    }

    public void setNationality(String nationality) {
        Nationality = nationality;
    }

    public String getMarriage() {
        return Marriage;
    }

    public void setMarriage(String marriage) {
        Marriage = marriage;
    }

    public String getCreditGrade() {
        return CreditGrade;
    }

    public void setCreditGrade(String creditGrade) {
        CreditGrade = creditGrade;
    }

    public String getHomeAddressCode() {
        return HomeAddressCode;
    }

    public void setHomeAddressCode(String homeAddressCode) {
        HomeAddressCode = homeAddressCode;
    }

    public String getHomeAddress() {
        return HomeAddress;
    }

    public void setHomeAddress(String homeAddress) {
        HomeAddress = homeAddress;
    }

    public String getPostalAddress() {
        return PostalAddress;
    }

    public void setPostalAddress(String postalAddress) {
        PostalAddress = postalAddress;
    }

    public String getZipCode() {
        return ZipCode;
    }

    public void setZipCode(String zipCode) {
        ZipCode = zipCode;
    }

    public String getPhone() {
        return Phone;
    }

    public void setPhone(String phone) {
        Phone = phone;
    }

    public String getBP() {
        return BP;
    }

    public void setBP(String BP) {
        this.BP = BP;
    }

    public String getMobile() {
        return Mobile;
    }

    public void setMobile(String mobile) {
        Mobile = mobile;
    }

    public String getEMail() {
        return EMail;
    }

    public void setEMail(String EMail) {
        this.EMail = EMail;
    }

    public String getMarriageDate() {
        return MarriageDate;
    }

    public void setMarriageDate(String marriageDate) {
        MarriageDate = marriageDate;
    }

    public String getIDNo() {
        return IDNo;
    }

    public void setIDNo(String IDNo) {
        this.IDNo = IDNo;
    }

    public String getSource() {
        return Source;
    }

    public void setSource(String source) {
        Source = source;
    }

    public String getBloodType() {
        return BloodType;
    }

    public void setBloodType(String bloodType) {
        BloodType = bloodType;
    }

    public String getPolityVisage() {
        return PolityVisage;
    }

    public void setPolityVisage(String polityVisage) {
        PolityVisage = polityVisage;
    }

    public String getDegree() {
        return Degree;
    }

    public void setDegree(String degree) {
        Degree = degree;
    }

    public String getGraduateSchool() {
        return GraduateSchool;
    }

    public void setGraduateSchool(String graduateSchool) {
        GraduateSchool = graduateSchool;
    }

    public String getSpeciality() {
        return Speciality;
    }

    public void setSpeciality(String speciality) {
        Speciality = speciality;
    }

    public String getPostTitle() {
        return PostTitle;
    }

    public void setPostTitle(String postTitle) {
        PostTitle = postTitle;
    }

    public String getForeignLevel() {
        return ForeignLevel;
    }

    public void setForeignLevel(String foreignLevel) {
        ForeignLevel = foreignLevel;
    }

    public int getWorkAge() {
        return WorkAge;
    }

    public void setWorkAge(int workAge) {
        WorkAge = workAge;
    }

    public String getOldCom() {
        return OldCom;
    }

    public void setOldCom(String oldCom) {
        OldCom = oldCom;
    }

    public String getOldOccupation() {
        return OldOccupation;
    }

    public void setOldOccupation(String oldOccupation) {
        OldOccupation = oldOccupation;
    }

    public String getHeadShip() {
        return HeadShip;
    }

    public void setHeadShip(String headShip) {
        HeadShip = headShip;
    }

    public String getRecommendAgent() {
        return RecommendAgent;
    }

    public void setRecommendAgent(String recommendAgent) {
        RecommendAgent = recommendAgent;
    }

    public String getBusiness() {
        return Business;
    }

    public void setBusiness(String business) {
        Business = business;
    }

    public String getSaleQuaf() {
        return SaleQuaf;
    }

    public void setSaleQuaf(String saleQuaf) {
        SaleQuaf = saleQuaf;
    }

    public String getQuafNo() {
        return QuafNo;
    }

    public void setQuafNo(String quafNo) {
        QuafNo = quafNo;
    }

    public String getQuafStartDate() {
        return QuafStartDate;
    }

    public void setQuafStartDate(String quafStartDate) {
        QuafStartDate = quafStartDate;
    }

    public String getQuafEndDate() {
        return QuafEndDate;
    }

    public void setQuafEndDate(String quafEndDate) {
        QuafEndDate = quafEndDate;
    }

    public String getDevNo1() {
        return DevNo1;
    }

    public void setDevNo1(String devNo1) {
        DevNo1 = devNo1;
    }

    public String getDevNo2() {
        return DevNo2;
    }

    public void setDevNo2(String devNo2) {
        DevNo2 = devNo2;
    }

    public String getRetainContNo() {
        return RetainContNo;
    }

    public void setRetainContNo(String retainContNo) {
        RetainContNo = retainContNo;
    }

    public String getAgentKind() {
        return AgentKind;
    }

    public void setAgentKind(String agentKind) {
        AgentKind = agentKind;
    }

    public String getDevGrade() {
        return DevGrade;
    }

    public void setDevGrade(String devGrade) {
        DevGrade = devGrade;
    }

    public String getInsideFlag() {
        return InsideFlag;
    }

    public void setInsideFlag(String insideFlag) {
        InsideFlag = insideFlag;
    }

    public String getFullTimeFlag() {
        return FullTimeFlag;
    }

    public void setFullTimeFlag(String fullTimeFlag) {
        FullTimeFlag = fullTimeFlag;
    }

    public String getNoWorkFlag() {
        return NoWorkFlag;
    }

    public void setNoWorkFlag(String noWorkFlag) {
        NoWorkFlag = noWorkFlag;
    }

    public String getTrainDate() {
        return TrainDate;
    }

    public void setTrainDate(String trainDate) {
        TrainDate = trainDate;
    }

    public String getEmployDate() {
        return EmployDate;
    }

    public void setEmployDate(String employDate) {
        EmployDate = employDate;
    }

    public String getInDueFormDate() {
        return InDueFormDate;
    }

    public void setInDueFormDate(String inDueFormDate) {
        InDueFormDate = inDueFormDate;
    }

    public String getOutWorkDate() {
        return OutWorkDate;
    }

    public void setOutWorkDate(String outWorkDate) {
        OutWorkDate = outWorkDate;
    }

    public String getRecommendNo() {
        return RecommendNo;
    }

    public void setRecommendNo(String recommendNo) {
        RecommendNo = recommendNo;
    }

    public String getCautionerName() {
        return CautionerName;
    }

    public void setCautionerName(String cautionerName) {
        CautionerName = cautionerName;
    }

    public String getCautionerSex() {
        return CautionerSex;
    }

    public void setCautionerSex(String cautionerSex) {
        CautionerSex = cautionerSex;
    }

    public String getCautionerID() {
        return CautionerID;
    }

    public void setCautionerID(String cautionerID) {
        CautionerID = cautionerID;
    }

    public String getCautionerBirthday() {
        return CautionerBirthday;
    }

    public void setCautionerBirthday(String cautionerBirthday) {
        CautionerBirthday = cautionerBirthday;
    }

    public String getApprover() {
        return Approver;
    }

    public void setApprover(String approver) {
        Approver = approver;
    }

    public String getApproveDate() {
        return ApproveDate;
    }

    public void setApproveDate(String approveDate) {
        ApproveDate = approveDate;
    }

    public double getAssuMoney() {
        return AssuMoney;
    }

    public void setAssuMoney(double assuMoney) {
        AssuMoney = assuMoney;
    }

    public String getRemark() {
        return Remark;
    }

    public void setRemark(String remark) {
        Remark = remark;
    }

    public String getAgentState() {
        return AgentState;
    }

    public void setAgentState(String agentState) {
        AgentState = agentState;
    }

    public String getQualiPassFlag() {
        return QualiPassFlag;
    }

    public void setQualiPassFlag(String qualiPassFlag) {
        QualiPassFlag = qualiPassFlag;
    }

    public String getSmokeFlag() {
        return SmokeFlag;
    }

    public void setSmokeFlag(String smokeFlag) {
        SmokeFlag = smokeFlag;
    }

    public String getRgtAddress() {
        return RgtAddress;
    }

    public void setRgtAddress(String rgtAddress) {
        RgtAddress = rgtAddress;
    }

    public String getBankCode() {
        return BankCode;
    }

    public void setBankCode(String bankCode) {
        BankCode = bankCode;
    }

    public String getBankAccNo() {
        return BankAccNo;
    }

    public void setBankAccNo(String bankAccNo) {
        BankAccNo = bankAccNo;
    }

    public String getOperator() {
        return Operator;
    }

    public void setOperator(String operator) {
        Operator = operator;
    }

    public String getMakeDate() {
        return MakeDate;
    }

    public void setMakeDate(String makeDate) {
        MakeDate = makeDate;
    }

    public String getMakeTime() {
        return MakeTime;
    }

    public void setMakeTime(String makeTime) {
        MakeTime = makeTime;
    }

    public String getModifyDate() {
        return ModifyDate;
    }

    public void setModifyDate(String modifyDate) {
        ModifyDate = modifyDate;
    }

    public String getModifyTime() {
        return ModifyTime;
    }

    public void setModifyTime(String modifyTime) {
        ModifyTime = modifyTime;
    }

    public String getBranchType() {
        return BranchType;
    }

    public void setBranchType(String branchType) {
        BranchType = branchType;
    }

    public String getTrainPeriods() {
        return TrainPeriods;
    }

    public void setTrainPeriods(String trainPeriods) {
        TrainPeriods = trainPeriods;
    }

    public String getBranchCode() {
        return BranchCode;
    }

    public void setBranchCode(String branchCode) {
        BranchCode = branchCode;
    }

    public int getAge() {
        return Age;
    }

    public void setAge(int age) {
        Age = age;
    }

    public String getChannelName() {
        return ChannelName;
    }

    public void setChannelName(String channelName) {
        ChannelName = channelName;
    }

    public String getReceiptNo() {
        return ReceiptNo;
    }

    public void setReceiptNo(String receiptNo) {
        ReceiptNo = receiptNo;
    }

    public String getIDNoType() {
        return IDNoType;
    }

    public void setIDNoType(String IDNoType) {
        this.IDNoType = IDNoType;
    }

    public String getBranchType2() {
        return BranchType2;
    }

    public void setBranchType2(String branchType2) {
        BranchType2 = branchType2;
    }

    public String getTrainPassFlag() {
        return TrainPassFlag;
    }

    public void setTrainPassFlag(String trainPassFlag) {
        TrainPassFlag = trainPassFlag;
    }

    public String getEmergentLink() {
        return EmergentLink;
    }

    public void setEmergentLink(String emergentLink) {
        EmergentLink = emergentLink;
    }

    public String getEmergentPhone() {
        return EmergentPhone;
    }

    public void setEmergentPhone(String emergentPhone) {
        EmergentPhone = emergentPhone;
    }

    public String getRetainStartDate() {
        return RetainStartDate;
    }

    public void setRetainStartDate(String retainStartDate) {
        RetainStartDate = retainStartDate;
    }

    public String getRetainEndDate() {
        return RetainEndDate;
    }

    public void setRetainEndDate(String retainEndDate) {
        RetainEndDate = retainEndDate;
    }

    public String getTogaeFlag() {
        return TogaeFlag;
    }

    public void setTogaeFlag(String togaeFlag) {
        TogaeFlag = togaeFlag;
    }

    public String getArchieveCode() {
        return ArchieveCode;
    }

    public void setArchieveCode(String archieveCode) {
        ArchieveCode = archieveCode;
    }

    public String getAffix() {
        return Affix;
    }

    public void setAffix(String affix) {
        Affix = affix;
    }

    public String getAffixName() {
        return AffixName;
    }

    public void setAffixName(String affixName) {
        AffixName = affixName;
    }

    public String getBankAddress() {
        return BankAddress;
    }

    public void setBankAddress(String bankAddress) {
        BankAddress = bankAddress;
    }

    public String getDXBranchtype2() {
        return DXBranchtype2;
    }

    public void setDXBranchtype2(String DXBranchtype2) {
        this.DXBranchtype2 = DXBranchtype2;
    }

    public String getDXSaleOrg() {
        return DXSaleOrg;
    }

    public void setDXSaleOrg(String DXSaleOrg) {
        this.DXSaleOrg = DXSaleOrg;
    }

    @Override
    public String toString() {
        return "LAAgent{" +
                "AgentCode='" + AgentCode + '\'' +
                ", AgentGroup='" + AgentGroup + '\'' +
                ", ManageCom='" + ManageCom + '\'' +
                ", Password='" + Password + '\'' +
                ", EntryNo='" + EntryNo + '\'' +
                ", Name='" + Name + '\'' +
                ", Sex='" + Sex + '\'' +
                ", Birthday='" + Birthday + '\'' +
                ", NativePlace='" + NativePlace + '\'' +
                ", Nationality='" + Nationality + '\'' +
                ", Marriage='" + Marriage + '\'' +
                ", CreditGrade='" + CreditGrade + '\'' +
                ", HomeAddressCode='" + HomeAddressCode + '\'' +
                ", HomeAddress='" + HomeAddress + '\'' +
                ", PostalAddress='" + PostalAddress + '\'' +
                ", ZipCode='" + ZipCode + '\'' +
                ", Phone='" + Phone + '\'' +
                ", BP='" + BP + '\'' +
                ", Mobile='" + Mobile + '\'' +
                ", EMail='" + EMail + '\'' +
                ", MarriageDate='" + MarriageDate + '\'' +
                ", IDNo='" + IDNo + '\'' +
                ", Source='" + Source + '\'' +
                ", BloodType='" + BloodType + '\'' +
                ", PolityVisage='" + PolityVisage + '\'' +
                ", Degree='" + Degree + '\'' +
                ", GraduateSchool='" + GraduateSchool + '\'' +
                ", Speciality='" + Speciality + '\'' +
                ", PostTitle='" + PostTitle + '\'' +
                ", ForeignLevel='" + ForeignLevel + '\'' +
                ", WorkAge=" + WorkAge +
                ", OldCom='" + OldCom + '\'' +
                ", OldOccupation='" + OldOccupation + '\'' +
                ", HeadShip='" + HeadShip + '\'' +
                ", RecommendAgent='" + RecommendAgent + '\'' +
                ", Business='" + Business + '\'' +
                ", SaleQuaf='" + SaleQuaf + '\'' +
                ", QuafNo='" + QuafNo + '\'' +
                ", QuafStartDate='" + QuafStartDate + '\'' +
                ", QuafEndDate='" + QuafEndDate + '\'' +
                ", DevNo1='" + DevNo1 + '\'' +
                ", DevNo2='" + DevNo2 + '\'' +
                ", RetainContNo='" + RetainContNo + '\'' +
                ", AgentKind='" + AgentKind + '\'' +
                ", DevGrade='" + DevGrade + '\'' +
                ", InsideFlag='" + InsideFlag + '\'' +
                ", FullTimeFlag='" + FullTimeFlag + '\'' +
                ", NoWorkFlag='" + NoWorkFlag + '\'' +
                ", TrainDate='" + TrainDate + '\'' +
                ", EmployDate='" + EmployDate + '\'' +
                ", InDueFormDate='" + InDueFormDate + '\'' +
                ", OutWorkDate='" + OutWorkDate + '\'' +
                ", RecommendNo='" + RecommendNo + '\'' +
                ", CautionerName='" + CautionerName + '\'' +
                ", CautionerSex='" + CautionerSex + '\'' +
                ", CautionerID='" + CautionerID + '\'' +
                ", CautionerBirthday='" + CautionerBirthday + '\'' +
                ", Approver='" + Approver + '\'' +
                ", ApproveDate='" + ApproveDate + '\'' +
                ", AssuMoney=" + AssuMoney +
                ", Remark='" + Remark + '\'' +
                ", AgentState='" + AgentState + '\'' +
                ", QualiPassFlag='" + QualiPassFlag + '\'' +
                ", SmokeFlag='" + SmokeFlag + '\'' +
                ", RgtAddress='" + RgtAddress + '\'' +
                ", BankCode='" + BankCode + '\'' +
                ", BankAccNo='" + BankAccNo + '\'' +
                ", Operator='" + Operator + '\'' +
                ", MakeDate='" + MakeDate + '\'' +
                ", MakeTime='" + MakeTime + '\'' +
                ", ModifyDate='" + ModifyDate + '\'' +
                ", ModifyTime='" + ModifyTime + '\'' +
                ", BranchType='" + BranchType + '\'' +
                ", TrainPeriods='" + TrainPeriods + '\'' +
                ", BranchCode='" + BranchCode + '\'' +
                ", Age=" + Age +
                ", ChannelName='" + ChannelName + '\'' +
                ", ReceiptNo='" + ReceiptNo + '\'' +
                ", IDNoType='" + IDNoType + '\'' +
                ", BranchType2='" + BranchType2 + '\'' +
                ", TrainPassFlag='" + TrainPassFlag + '\'' +
                ", EmergentLink='" + EmergentLink + '\'' +
                ", EmergentPhone='" + EmergentPhone + '\'' +
                ", RetainStartDate='" + RetainStartDate + '\'' +
                ", RetainEndDate='" + RetainEndDate + '\'' +
                ", TogaeFlag='" + TogaeFlag + '\'' +
                ", ArchieveCode='" + ArchieveCode + '\'' +
                ", Affix='" + Affix + '\'' +
                ", AffixName='" + AffixName + '\'' +
                ", BankAddress='" + BankAddress + '\'' +
                ", DXBranchtype2='" + DXBranchtype2 + '\'' +
                ", DXSaleOrg='" + DXSaleOrg + '\'' +
                '}';
    }
}
