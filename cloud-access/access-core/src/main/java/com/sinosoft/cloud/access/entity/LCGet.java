package com.sinosoft.cloud.access.entity;

/**
 * Created by Administrator on 2018/4/16.
 */
public class LCGet {
    private long GetID;
    private long DutyID;
    private String ShardingID;
    private String GrpContNo;
    private String ContNo;
    private String PolNo;
    private String DutyCode;
    private String GetDutyCode;
    private String GetDutyKind;
    private String InsuredNo;
    private String GetMode;
    private double GetLimit;
    private double GetRate;
    private String UrgeGetFlag;
    private String LiveGetType;
    private double AddRate;
    private String CanGet;
    private String NeedAcc;
    private String NeedCancelAcc;
    private double StandMoney;
    private double ActuGet;
    private double SumMoney;
    private int GetIntv;
    private String GettoDate;
    private String GetStartDate;
    private String GetEndDate;
    private String BalaDate;
    private String State;
    private String ManageCom;
    private String Operator;
    private String MakeDate;
    private String MakeTime;
    private String ModifyTime;
    private String ModifyDate;
    private String GetEndState;

    public long getGetID() {
        return GetID;
    }

    public void setGetID(long getID) {
        GetID = getID;
    }

    public long getDutyID() {
        return DutyID;
    }

    public void setDutyID(long dutyID) {
        DutyID = dutyID;
    }

    public String getShardingID() {
        return ShardingID;
    }

    public void setShardingID(String shardingID) {
        ShardingID = shardingID;
    }

    public String getGrpContNo() {
        return GrpContNo;
    }

    public void setGrpContNo(String grpContNo) {
        GrpContNo = grpContNo;
    }

    public String getContNo() {
        return ContNo;
    }

    public void setContNo(String contNo) {
        ContNo = contNo;
    }

    public String getPolNo() {
        return PolNo;
    }

    public void setPolNo(String polNo) {
        PolNo = polNo;
    }

    public String getDutyCode() {
        return DutyCode;
    }

    public void setDutyCode(String dutyCode) {
        DutyCode = dutyCode;
    }

    public String getGetDutyCode() {
        return GetDutyCode;
    }

    public void setGetDutyCode(String getDutyCode) {
        GetDutyCode = getDutyCode;
    }

    public String getGetDutyKind() {
        return GetDutyKind;
    }

    public void setGetDutyKind(String getDutyKind) {
        GetDutyKind = getDutyKind;
    }

    public String getInsuredNo() {
        return InsuredNo;
    }

    public void setInsuredNo(String insuredNo) {
        InsuredNo = insuredNo;
    }

    public String getGetMode() {
        return GetMode;
    }

    public void setGetMode(String getMode) {
        GetMode = getMode;
    }

    public double getGetLimit() {
        return GetLimit;
    }

    public void setGetLimit(double getLimit) {
        GetLimit = getLimit;
    }

    public double getGetRate() {
        return GetRate;
    }

    public void setGetRate(double getRate) {
        GetRate = getRate;
    }

    public String getUrgeGetFlag() {
        return UrgeGetFlag;
    }

    public void setUrgeGetFlag(String urgeGetFlag) {
        UrgeGetFlag = urgeGetFlag;
    }

    public String getLiveGetType() {
        return LiveGetType;
    }

    public void setLiveGetType(String liveGetType) {
        LiveGetType = liveGetType;
    }

    public double getAddRate() {
        return AddRate;
    }

    public void setAddRate(double addRate) {
        AddRate = addRate;
    }

    public String getCanGet() {
        return CanGet;
    }

    public void setCanGet(String canGet) {
        CanGet = canGet;
    }

    public String getNeedAcc() {
        return NeedAcc;
    }

    public void setNeedAcc(String needAcc) {
        NeedAcc = needAcc;
    }

    public String getNeedCancelAcc() {
        return NeedCancelAcc;
    }

    public void setNeedCancelAcc(String needCancelAcc) {
        NeedCancelAcc = needCancelAcc;
    }

    public double getStandMoney() {
        return StandMoney;
    }

    public void setStandMoney(double standMoney) {
        StandMoney = standMoney;
    }

    public double getActuGet() {
        return ActuGet;
    }

    public void setActuGet(double actuGet) {
        ActuGet = actuGet;
    }

    public double getSumMoney() {
        return SumMoney;
    }

    public void setSumMoney(double sumMoney) {
        SumMoney = sumMoney;
    }

    public int getGetIntv() {
        return GetIntv;
    }

    public void setGetIntv(int getIntv) {
        GetIntv = getIntv;
    }

    public String getGettoDate() {
        return GettoDate;
    }

    public void setGettoDate(String gettoDate) {
        GettoDate = gettoDate;
    }

    public String getGetStartDate() {
        return GetStartDate;
    }

    public void setGetStartDate(String getStartDate) {
        GetStartDate = getStartDate;
    }

    public String getGetEndDate() {
        return GetEndDate;
    }

    public void setGetEndDate(String getEndDate) {
        GetEndDate = getEndDate;
    }

    public String getBalaDate() {
        return BalaDate;
    }

    public void setBalaDate(String balaDate) {
        BalaDate = balaDate;
    }

    public String getState() {
        return State;
    }

    public void setState(String state) {
        State = state;
    }

    public String getManageCom() {
        return ManageCom;
    }

    public void setManageCom(String manageCom) {
        ManageCom = manageCom;
    }

    public String getOperator() {
        return Operator;
    }

    public void setOperator(String operator) {
        Operator = operator;
    }

    public String getMakeDate() {
        return MakeDate;
    }

    public void setMakeDate(String makeDate) {
        MakeDate = makeDate;
    }

    public String getMakeTime() {
        return MakeTime;
    }

    public void setMakeTime(String makeTime) {
        MakeTime = makeTime;
    }

    public String getModifyTime() {
        return ModifyTime;
    }

    public void setModifyTime(String modifyTime) {
        ModifyTime = modifyTime;
    }

    public String getModifyDate() {
        return ModifyDate;
    }

    public void setModifyDate(String modifyDate) {
        ModifyDate = modifyDate;
    }

    public String getGetEndState() {
        return GetEndState;
    }

    public void setGetEndState(String getEndState) {
        GetEndState = getEndState;
    }
}
