package com.sinosoft.cloud.access.util;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.sun.org.apache.xml.internal.serialize.XMLSerializer;
import de.odysseus.staxon.json.JsonXMLConfig;
import de.odysseus.staxon.json.JsonXMLConfigBuilder;
import de.odysseus.staxon.json.JsonXMLInputFactory;
import de.odysseus.staxon.json.JsonXMLOutputFactory;
import de.odysseus.staxon.xml.util.PrettyXMLEventWriter;
import org.dom4j.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLEventWriter;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLOutputFactory;
import java.io.*;
import java.util.Map;



import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.LinkedList;



import static org.bouncycastle.asn1.x500.style.RFC4519Style.o;

/**
 * abc-cloud-access
 *
 * @title: abc-cloud-access
 * @package: com.sinosoft.cloud.access.util
 * @author: yangming
 * @date: 2018/3/16 下午5:22
 */
public class StaxonUtil {


    /**
     * json 转化 xml, 添加虚拟根节点, 使xml可以完整.
     *
     * @param json        原始json字符串
     * @param virtualRoot 转换成xml后的报文头节点
     * @return
     */
    public static String json2xml(String json, String virtualRoot) {
        StringReader input = new StringReader(json);
        StringWriter output = new StringWriter();
        JsonXMLConfig config = new JsonXMLConfigBuilder().virtualRoot(virtualRoot).multiplePI(false).repairingNamespaces(false).build();
        try {
            XMLEventReader reader = new JsonXMLInputFactory(config).createXMLEventReader(input);
            XMLEventWriter writer = XMLOutputFactory.newInstance().createXMLEventWriter(output);
            writer = new PrettyXMLEventWriter(writer);
            writer.add(reader);
            reader.close();
            writer.close();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                output.close();
                input.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
//        if (output.toString().length() >= 38) {//remove <?xml version="1.0" encoding="UTF-8"?>
//            return output.toString().substring(39);
//        }
        return output.toString();
    }


    /**
     * xml 转化 json, 添加虚拟根节点, 使最外层json节点为空
     *
     * @param xml         原始xml字符串
     * @param virtualRoot 转换成json后的json头节点
     * @return
     */
    public static String xml2json(String xml, String virtualRoot) {
        StringReader input = new StringReader(xml);
        StringWriter output = new StringWriter();
        JsonXMLConfig config = new JsonXMLConfigBuilder().virtualRoot(virtualRoot).autoArray(true).autoPrimitive(true).prettyPrint(true).build();
        try {
            XMLEventReader reader = XMLInputFactory.newInstance().createXMLEventReader(input);
            XMLEventWriter writer = new JsonXMLOutputFactory(config).createXMLEventWriter(output);
            writer.add(reader);
            reader.close();
            writer.close();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                output.close();
                input.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return output.toString();
    }

    /**
     * 获取移动展业发送的json中的XML报文
     *
     * @param jsonStr json字符串
     * @return
     */
    public static String getXmlByMip(String jsonStr) {
        Map<String, String> map = JSON.parseObject(jsonStr, Map.class);

        //移动展业将xml封装到json的message中，直接获取message里面的xml即可
        for (String key : map.keySet()) {
            if ("message".equals(key)){
                String orgXml = map.get(key);
                return orgXml;
            }
            break;
        }

        return null;
    }

    /**
     * 将返回移动展业的报文封装到json中
     *
     * @param xml 返回移动展业的xml
     * @return
     */
    public static String getJsonByMip(String xml) {
        JSONObject jsonin = new JSONObject();
        jsonin.put("rtnValue", xml);

        JSONObject jsonout = new JSONObject();
        jsonout.put("content", jsonin);
        jsonout.put("successFlag", "1");
        return jsonout.toString();
    }

    public static void main(String[] args) {
        String s="<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
                "<document>\n" +
                "\t<Head>\n" +
                "\t\t<version>3.0</version>\n" +
                "\t\t<function>ant.bxcloud.core.underwrite</function>\n" +
                "\t\t<transTime>20160927195708</transTime>\n" +
                "\t\t<transTimeZone>UTC+8</transTimeZone>\n" +
                "\t\t<reqMsgId>2016092719570534920695867854845</reqMsgId>\n" +
                "\t\t<format>xml</format>\n" +
                "\t\t<signType>RSA</signType>\n" +
                "\t\t<asyn>0</asyn>\n" +
                "\t\t<cid>shlife</cid>\n" +
                "\t</Head>\n" +
                "\t<body>\n" +
                "\t\t<success>1</success>\n" +
                "\t</body>\n" +
                "</document>";
        String ss="<version>3.0</version>";

       String json= StaxonUtil.xml2json(ss,"AUTO_XML_DATA");
        System.out.println("json:+++"+json);

    }
}
