package com.sinosoft.cloud.access.entity;
/**
 * 生存给付领取方式变更保全申请POS041.
 */
public class RespPreserveApply {
    //保全项目编码
    private String EdorType;
    //保全受理号
    private String EdorAcceptNo;
    //保全批单号
    private String EdorNo;
    @Override
    public String toString() {
        return "RespPreserveApply{" +
                "EdorType='" + EdorType + '\'' +
                ", EdorAcceptNo='" + EdorAcceptNo + '\'' +
                ", EdorNo='" + EdorNo + '\'' +
                '}';
    }

    public String getEdorType() {
        return EdorType;
    }

    public void setEdorType(String edorType) {
        EdorType = edorType;
    }

    public String getEdorAcceptNo() {
        return EdorAcceptNo;
    }

    public void setEdorAcceptNo(String edorAcceptNo) {
        EdorAcceptNo = edorAcceptNo;
    }

    public String getEdorNo() {
        return EdorNo;
    }

    public void setEdorNo(String edorNo) {
        EdorNo = edorNo;
    }
}
