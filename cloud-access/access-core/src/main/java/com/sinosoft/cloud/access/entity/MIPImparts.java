package com.sinosoft.cloud.access.entity;

import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.List;

/**
 * @author zhujiaxing
 * @date 2018/9/13 11:01
 * @description 移动展业告知类
 **/
public class MIPImparts {

    @XStreamImplicit(itemFieldName="MIPImpart")
    private List<MIPImpart> MIPImparts;

    public List<MIPImpart> getMIPImparts() {
        return MIPImparts;
    }

    public void setMIPImparts(List<MIPImpart> MIPImparts) {
        this.MIPImparts = MIPImparts;
    }

    @Override
    public String toString() {
        return "MIPImparts{" +
                "MIPImparts=" + MIPImparts +
                '}';
    }
}
