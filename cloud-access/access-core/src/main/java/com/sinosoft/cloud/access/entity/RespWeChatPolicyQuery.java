package com.sinosoft.cloud.access.entity;

/**
 * @program: abc-cloud-access
 * @description: 微信查询响应
 * @author: BaoYongmeng
 * @create: 2019-02-19 14:12
 **/
public class RespWeChatPolicyQuery {
    private String ContNo;
    private String RiskCode;
    private String Prem;
    private String Amnt;
    private String RiskName;
    private String PolicyStatus;
    private String PolicyPledge;
    private String AcceptDate;
    private String PolicyBgnDate;
    private String PolicyEndDate;
    private String PolicyAmmount;
    private String PolicyValue;
    private String AutoTransferAccNo;

    public void setContNo(String contNo) {
        ContNo = contNo;
    }

    public void setRiskCode(String riskCode) {
        RiskCode = riskCode;
    }

    public void setPrem(String prem) {
        Prem = prem;
    }

    public void setAmnt(String amnt) {
        Amnt = amnt;
    }

    public void setRiskName(String riskName) {
        RiskName = riskName;
    }

    public void setPolicyStatus(String policyStatus) {
        PolicyStatus = policyStatus;
    }

    public void setPolicyPledge(String policyPledge) {
        PolicyPledge = policyPledge;
    }

    public void setAcceptDate(String acceptDate) {
        AcceptDate = acceptDate;
    }

    public void setPolicyBgnDate(String policyBgnDate) {
        PolicyBgnDate = policyBgnDate;
    }

    public void setPolicyEndDate(String policyEndDate) {
        PolicyEndDate = policyEndDate;
    }

    public void setPolicyAmmount(String policyAmmount) {
        PolicyAmmount = policyAmmount;
    }

    public void setPolicyValue(String policyValue) {
        PolicyValue = policyValue;
    }

    public void setAutoTransferAccNo(String autoTransferAccNo) {
        AutoTransferAccNo = autoTransferAccNo;
    }

    public String getContNo() {
        return ContNo;
    }

    public String getRiskCode() {
        return RiskCode;
    }

    public String getPrem() {
        return Prem;
    }

    public String getAmnt() {
        return Amnt;
    }

    public String getRiskName() {
        return RiskName;
    }

    public String getPolicyStatus() {
        return PolicyStatus;
    }

    public String getPolicyPledge() {
        return PolicyPledge;
    }

    public String getAcceptDate() {
        return AcceptDate;
    }

    public String getPolicyBgnDate() {
        return PolicyBgnDate;
    }

    public String getPolicyEndDate() {
        return PolicyEndDate;
    }

    public String getPolicyAmmount() {
        return PolicyAmmount;
    }

    public String getPolicyValue() {
        return PolicyValue;
    }

    public String getAutoTransferAccNo() {
        return AutoTransferAccNo;
    }

    @Override
    public String toString() {
        return "RespWeChatPolicyQuery{" +
                "ContNo='" + ContNo + '\'' +
                ", RiskCode='" + RiskCode + '\'' +
                ", Prem='" + Prem + '\'' +
                ", Amnt='" + Amnt + '\'' +
                ", RiskName='" + RiskName + '\'' +
                ", PolicyStatus='" + PolicyStatus + '\'' +
                ", PolicyPledge='" + PolicyPledge + '\'' +
                ", AcceptDate='" + AcceptDate + '\'' +
                ", PolicyBgnDate='" + PolicyBgnDate + '\'' +
                ", PolicyEndDate='" + PolicyEndDate + '\'' +
                ", PolicyAmmount='" + PolicyAmmount + '\'' +
                ", PolicyValue='" + PolicyValue + '\'' +
                ", AutoTransferAccNo='" + AutoTransferAccNo + '\'' +
                '}';
    }
}
