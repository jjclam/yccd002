package com.sinosoft.cloud.access.entity;

import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.List;

/**
 * @Author:lizhen
 * @Description:
 * @Date:Created in 16:37 2018/11/29
 * @Modified By:
 */
public class Docs {
    private String DocCode;
    private String BussType;
    private String SubType;
    private String NumPages;
    private String UploadPages;
    private String ScanOperator;
    private String ManageCom;
    @XStreamImplicit(itemFieldName="Pages")
    private List<Pages> Pages;

    public String getDocCode() {
        return DocCode;
    }

    public void setDocCode(String docCode) {
        DocCode = docCode;
    }

    public String getBussType() {
        return BussType;
    }

    public void setBussType(String bussType) {
        BussType = bussType;
    }

    public String getSubType() {
        return SubType;
    }

    public void setSubType(String subType) {
        SubType = subType;
    }

    public String getNumPages() {
        return NumPages;
    }

    public void setNumPages(String numPages) {
        NumPages = numPages;
    }

    public String getUploadPages() {
        return UploadPages;
    }

    public void setUploadPages(String uploadPages) {
        UploadPages = uploadPages;
    }

    public String getScanOperator() {
        return ScanOperator;
    }

    public void setScanOperator(String scanOperator) {
        ScanOperator = scanOperator;
    }

    public String getManageCom() {
        return ManageCom;
    }

    public void setManageCom(String manageCom) {
        ManageCom = manageCom;
    }

    public List<com.sinosoft.cloud.access.entity.Pages> getPages() {
        return Pages;
    }

    public void setPages(List<com.sinosoft.cloud.access.entity.Pages> pages) {
        Pages = pages;
    }

    @Override
    public String toString() {
        return "Docs{" +
                "DocCode='" + DocCode + '\'' +
                ", BussType='" + BussType + '\'' +
                ", SubType='" + SubType + '\'' +
                ", NumPages='" + NumPages + '\'' +
                ", UploadPages='" + UploadPages + '\'' +
                ", ScanOperator='" + ScanOperator + '\'' +
                ", ManageCom='" + ManageCom + '\'' +
                ", Pages=" + Pages +
                '}';
    }
}