package com.sinosoft.cloud.access.entity;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("RiskList")
public class RiskList {

	@XStreamAlias("RiskDetail")
    private RiskDetail riskDetail;

	public RiskDetail getRiskDetail() {
		return riskDetail;
	}

	public void setRiskDetail(RiskDetail riskDetail) {
		this.riskDetail = riskDetail;
	}

	@Override
	public String toString() {
		return "RiskList [riskDetail=" + riskDetail + "]";
	}
}
