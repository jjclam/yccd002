package com.sinosoft.cloud.access.entity;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.List;

/**
 * @project: abc-cloud-access
 * @author: yangming
 * @date: 2017/9/19 下午7:09
 * To change this template use File | Settings | File and Code Templates.
 */
@XStreamAlias("Risks")
public class Risks {
    @XStreamImplicit(itemFieldName="Risk")
    private List<Risk> risks;

    public List<Risk> getRisks() {
        return risks;
    }

    public void setRisks(List<Risk> risks) {
        this.risks = risks;
    }

    @Override
    public String toString() {
        return "Risks{" +
                "risks=" + risks +
                '}';
    }
}
