package com.sinosoft.cloud.access.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @Description
 * @Author:zhaoshulei
 * @Date:2018/4/23
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface BeanCopy {

    /**
     * 对应Pojo包名
     * @return
     */
    String targetPackage() default "com.sinosoft.lis.entity";

    /**
     * 单数节点
     * @return
     */

    String element() default "";

    /**
     * 复数节点
     * @return
     */
    String elements() default "";

    /**
     * 是否有子类
     * @return
     */
    String complicated() default "N";
}
