package com.sinosoft.cloud.access.entity;
/**
 * 生存金详情查询POS047.
 */
public class ReqLifeCashDetailsQuery {

    //保全项目编码
    private String EdorType;
    //保单号
    private String ContNo;

    @Override
    public String toString() {
        return "ReqLifeCashDetailsQuery{" +
                "EdorType='" + EdorType + '\'' +
                ", ContNo='" + ContNo + '\'' +
                '}';
    }

    public String getEdorType() {
        return EdorType;
    }

    public void setEdorType(String edorType) {
        EdorType = edorType;
    }

    public String getContNo() {
        return ContNo;
    }

    public void setContNo(String contNo) {
        ContNo = contNo;
    }
}
