package com.sinosoft.cloud.access.entity;

/**
 * @Description
 * @Author:zhaoshulei
 * @Date:2018/4/2
 */
public class LCCont {

    private long ContID;
    private String ShardingID;
    private String GrpContNo;
    private String ContNo;
    private String ProposalContNo;
    private String PrtNo;
    private String ContType;
    private String FamilyType;
    private String FamilyID;
    private String PolType;
    private String CardFlag;
    private String ManageCom;
    private String ExecuteCom;
    private String AgentCom;
    private String AgentCode;
    private String AgentGroup;

    private String AgentCode1;
    private String AgentType;
    private String SaleChnl;
    private String Handler;
    private String Password;
    private String AppntNo;
    private String AppntName;
    private String AppntSex;
    private String AppntBirthday;
    private String AppntIDType;
    private String AppntIDNo;
    private String InsuredNo;
    private String InsuredName;
    private String InsuredSex;
    private String InsuredBirthday;
    private String InsuredIDType;
    private String InsuredIDNo;
    private int PayIntv;
    private String PayMode;
    private String PayLocation;
    private String DisputedFlag;
    private String OutPayFlag;
    private String GetPolMode;
    private String SignCom;
    private String SignDate;
    private String SignTime;
    private String ConsignNo;
    private String BankCode;
    private String BankAccNo;
    private String AccName;
    private int PrintCount;
    private int LostTimes;
    private String Lang;
    private String Currency;
    private String Remark;
    private int Peoples;
    private double Mult;
    private double Prem;
    private double Amnt;
    private double SumPrem;
    private double Dif;
    private String PaytoDate;
    private String FirstPayDate;
    private String CValiDate;
    private String InputOperator;
    private String InputDate;
    private String InputTime;
    private String ApproveFlag;
    private String ApproveCode;
    private String ApproveDate;
    private String ApproveTime;
    private String UWFlag;
    private String UWOperator;
    private String UWDate;
    private String UWTime;
    private String AppFlag;
    private String PolApplyDate;
    private String GetPolDate;
    private String GetPolTime;
    private String CustomGetPolDate;
    private String State;
    private String Operator;
    private String MakeDate;
    private String MakeTime;
    private String ModifyDate;
    private String ModifyTime;
    private String FirstTrialOperator;
    private String FirstTrialDate;
    private String FirstTrialTime;
    private String ReceiveOperator;
    private String ReceiveDate;
    private String ReceiveTime;
    private String TempFeeNo;
    private String SellType;
    private String ForceUWFlag;
    private String ForceUWReason;
    private String NewBankCode;
    private String NewBankAccNo;
    private String NewAccName;
    private String NewPayMode;
    private String AgentBankCode;
    private String BankAgent;
    private String BankAgentName;
    private String BankAgentTel;
    private String ProdSetCode;
    private String PolicyNo;
    private String BillPressNo;
    private String CardType;
    private String CardTypeCode;
    private String VisitDate;
    private String VisitTime;
    private String SaleCom;
    private String PrintFlag;
    private String InvoicePrtFlag;
    private String NewReinsureFlag;
    private String RenewPayFlag;
    private String AppntFirstName;
    private String AppntLastName;
    private String InsuredFirstName;
    private String InsuredLastName;
    private String AuthorFlag;
    private int GreenChnl;
    private String TBType;
    private String EAuto;
    private String SlipForm;
    private int RnewFlag;
    private String ThirdPartyOrderId;
    private String UserId;
    private String BankProvince;

    private String BankCity;
    private String  AccType;


    //新增开户行所在的省编码
    private String newbankproivnce;
    private String newbankcity;
    //    private String newbankaccno;
    private String newacctype;

    public String getNewbankproivnce() {
        return newbankproivnce;
    }

    public void setNewbankproivnce(String newbankproivnce) {
        this.newbankproivnce = newbankproivnce;
    }

    public String getNewbankcity() {
        return newbankcity;
    }

    public void setNewbankcity(String newbankcity) {
        this.newbankcity = newbankcity;
    }

    public String getNewacctype() {
        return newacctype;
    }

    public void setNewacctype(String newacctype) {
        this.newacctype = newacctype;
    }

    public long getContID() {
        return ContID;
    }

    public void setContID(long contID) {
        ContID = contID;
    }

    public String getShardingID() {
        return ShardingID;
    }

    public void setShardingID(String shardingID) {
        ShardingID = shardingID;
    }

    public String getGrpContNo() {
        return GrpContNo;
    }

    public void setGrpContNo(String grpContNo) {
        GrpContNo = grpContNo;
    }

    public String getContNo() {
        return ContNo;
    }

    public void setContNo(String contNo) {
        ContNo = contNo;
    }

    public String getProposalContNo() {
        return ProposalContNo;
    }

    public void setProposalContNo(String proposalContNo) {
        ProposalContNo = proposalContNo;
    }

    public String getPrtNo() {
        return PrtNo;
    }

    public void setPrtNo(String prtNo) {
        PrtNo = prtNo;
    }

    public String getContType() {
        return ContType;
    }

    public void setContType(String contType) {
        ContType = contType;
    }

    public String getFamilyType() {
        return FamilyType;
    }

    public void setFamilyType(String familyType) {
        FamilyType = familyType;
    }

    public String getFamilyID() {
        return FamilyID;
    }

    public void setFamilyID(String familyID) {
        FamilyID = familyID;
    }

    public String getPolType() {
        return PolType;
    }

    public void setPolType(String polType) {
        PolType = polType;
    }

    public String getCardFlag() {
        return CardFlag;
    }

    public void setCardFlag(String cardFlag) {
        CardFlag = cardFlag;
    }

    public String getManageCom() {
        return ManageCom;
    }

    public void setManageCom(String manageCom) {
        ManageCom = manageCom;
    }

    public String getExecuteCom() {
        return ExecuteCom;
    }

    public void setExecuteCom(String executeCom) {
        ExecuteCom = executeCom;
    }

    public String getAgentCom() {
        return AgentCom;
    }

    public void setAgentCom(String agentCom) {
        AgentCom = agentCom;
    }

    public String getAgentCode() {
        return AgentCode;
    }

    public void setAgentCode(String agentCode) {
        AgentCode = agentCode;
    }

    public String getAgentGroup() {
        return AgentGroup;
    }

    public void setAgentGroup(String agentGroup) {
        AgentGroup = agentGroup;
    }

    public String getAgentCode1() {
        return AgentCode1;
    }

    public void setAgentCode1(String agentCode1) {
        AgentCode1 = agentCode1;
    }

    public String getAgentType() {
        return AgentType;
    }

    public void setAgentType(String agentType) {
        AgentType = agentType;
    }

    public String getSaleChnl() {
        return SaleChnl;
    }

    public void setSaleChnl(String saleChnl) {
        SaleChnl = saleChnl;
    }

    public String getHandler() {
        return Handler;
    }

    public void setHandler(String handler) {
        Handler = handler;
    }

    public String getPassword() {
        return Password;
    }

    public void setPassword(String password) {
        Password = password;
    }

    public String getAppntNo() {
        return AppntNo;
    }

    public void setAppntNo(String appntNo) {
        AppntNo = appntNo;
    }

    public String getAppntName() {
        return AppntName;
    }

    public void setAppntName(String appntName) {
        AppntName = appntName;
    }

    public String getAppntSex() {
        return AppntSex;
    }

    public void setAppntSex(String appntSex) {
        AppntSex = appntSex;
    }

    public String getAppntBirthday() {
        return AppntBirthday;
    }

    public void setAppntBirthday(String appntBirthday) {
        AppntBirthday = appntBirthday;
    }

    public String getAppntIDType() {
        return AppntIDType;
    }

    public void setAppntIDType(String appntIDType) {
        AppntIDType = appntIDType;
    }

    public String getAppntIDNo() {
        return AppntIDNo;
    }

    public void setAppntIDNo(String appntIDNo) {
        AppntIDNo = appntIDNo;
    }

    public String getInsuredNo() {
        return InsuredNo;
    }

    public void setInsuredNo(String insuredNo) {
        InsuredNo = insuredNo;
    }

    public String getInsuredName() {
        return InsuredName;
    }

    public void setInsuredName(String insuredName) {
        InsuredName = insuredName;
    }

    public String getInsuredSex() {
        return InsuredSex;
    }

    public void setInsuredSex(String insuredSex) {
        InsuredSex = insuredSex;
    }

    public String getInsuredBirthday() {
        return InsuredBirthday;
    }

    public void setInsuredBirthday(String insuredBirthday) {
        InsuredBirthday = insuredBirthday;
    }

    public String getInsuredIDType() {
        return InsuredIDType;
    }

    public void setInsuredIDType(String insuredIDType) {
        InsuredIDType = insuredIDType;
    }

    public String getInsuredIDNo() {
        return InsuredIDNo;
    }

    public void setInsuredIDNo(String insuredIDNo) {
        InsuredIDNo = insuredIDNo;
    }

    public int getPayIntv() {
        return PayIntv;
    }

    public void setPayIntv(int payIntv) {
        PayIntv = payIntv;
    }

    public String getPayMode() {
        return PayMode;
    }

    public void setPayMode(String payMode) {
        PayMode = payMode;
    }

    public String getPayLocation() {
        return PayLocation;
    }

    public void setPayLocation(String payLocation) {
        PayLocation = payLocation;
    }

    public String getDisputedFlag() {
        return DisputedFlag;
    }

    public void setDisputedFlag(String disputedFlag) {
        DisputedFlag = disputedFlag;
    }

    public String getOutPayFlag() {
        return OutPayFlag;
    }

    public void setOutPayFlag(String outPayFlag) {
        OutPayFlag = outPayFlag;
    }

    public String getGetPolMode() {
        return GetPolMode;
    }

    public void setGetPolMode(String getPolMode) {
        GetPolMode = getPolMode;
    }

    public String getSignCom() {
        return SignCom;
    }

    public void setSignCom(String signCom) {
        SignCom = signCom;
    }

    public String getSignDate() {
        return SignDate;
    }

    public void setSignDate(String signDate) {
        SignDate = signDate;
    }

    public String getSignTime() {
        return SignTime;
    }

    public void setSignTime(String signTime) {
        SignTime = signTime;
    }

    public String getConsignNo() {
        return ConsignNo;
    }

    public void setConsignNo(String consignNo) {
        ConsignNo = consignNo;
    }

    public String getBankCode() {
        return BankCode;
    }

    public void setBankCode(String bankCode) {
        BankCode = bankCode;
    }

    public String getBankAccNo() {
        return BankAccNo;
    }

    public void setBankAccNo(String bankAccNo) {
        BankAccNo = bankAccNo;
    }

    public String getAccName() {
        return AccName;
    }

    public void setAccName(String accName) {
        AccName = accName;
    }

    public int getPrintCount() {
        return PrintCount;
    }

    public void setPrintCount(int printCount) {
        PrintCount = printCount;
    }

    public int getLostTimes() {
        return LostTimes;
    }

    public void setLostTimes(int lostTimes) {
        LostTimes = lostTimes;
    }

    public String getLang() {
        return Lang;
    }

    public void setLang(String lang) {
        Lang = lang;
    }

    public String getCurrency() {
        return Currency;
    }

    public void setCurrency(String currency) {
        Currency = currency;
    }

    public String getRemark() {
        return Remark;
    }

    public void setRemark(String remark) {
        Remark = remark;
    }

    public int getPeoples() {
        return Peoples;
    }

    public void setPeoples(int peoples) {
        Peoples = peoples;
    }

    public double getMult() {
        return Mult;
    }

    public void setMult(double mult) {
        Mult = mult;
    }

    public double getPrem() {
        return Prem;
    }

    public void setPrem(double prem) {
        Prem = prem;
    }

    public double getAmnt() {
        return Amnt;
    }

    public void setAmnt(double amnt) {
        Amnt = amnt;
    }

    public double getSumPrem() {
        return SumPrem;
    }

    public void setSumPrem(double sumPrem) {
        SumPrem = sumPrem;
    }

    public double getDif() {
        return Dif;
    }

    public void setDif(double dif) {
        Dif = dif;
    }

    public String getPaytoDate() {
        return PaytoDate;
    }

    public void setPaytoDate(String paytoDate) {
        PaytoDate = paytoDate;
    }

    public String getFirstPayDate() {
        return FirstPayDate;
    }

    public void setFirstPayDate(String firstPayDate) {
        FirstPayDate = firstPayDate;
    }

    public String getCValiDate() {
        return CValiDate;
    }

    public void setCValiDate(String CValiDate) {
        this.CValiDate = CValiDate;
    }

    public String getInputOperator() {
        return InputOperator;
    }

    public void setInputOperator(String inputOperator) {
        InputOperator = inputOperator;
    }

    public String getInputDate() {
        return InputDate;
    }

    public void setInputDate(String inputDate) {
        InputDate = inputDate;
    }

    public String getInputTime() {
        return InputTime;
    }

    public void setInputTime(String inputTime) {
        InputTime = inputTime;
    }

    public String getApproveFlag() {
        return ApproveFlag;
    }

    public void setApproveFlag(String approveFlag) {
        ApproveFlag = approveFlag;
    }

    public String getApproveCode() {
        return ApproveCode;
    }

    public void setApproveCode(String approveCode) {
        ApproveCode = approveCode;
    }

    public String getApproveDate() {
        return ApproveDate;
    }

    public void setApproveDate(String approveDate) {
        ApproveDate = approveDate;
    }

    public String getApproveTime() {
        return ApproveTime;
    }

    public void setApproveTime(String approveTime) {
        ApproveTime = approveTime;
    }

    public String getUWFlag() {
        return UWFlag;
    }

    public void setUWFlag(String UWFlag) {
        this.UWFlag = UWFlag;
    }

    public String getUWOperator() {
        return UWOperator;
    }

    public void setUWOperator(String UWOperator) {
        this.UWOperator = UWOperator;
    }

    public String getUWDate() {
        return UWDate;
    }

    public void setUWDate(String UWDate) {
        this.UWDate = UWDate;
    }

    public String getUWTime() {
        return UWTime;
    }

    public void setUWTime(String UWTime) {
        this.UWTime = UWTime;
    }

    public String getAppFlag() {
        return AppFlag;
    }

    public void setAppFlag(String appFlag) {
        AppFlag = appFlag;
    }

    public String getPolApplyDate() {
        return PolApplyDate;
    }

    public void setPolApplyDate(String polApplyDate) {
        PolApplyDate = polApplyDate;
    }

    public String getGetPolDate() {
        return GetPolDate;
    }

    public void setGetPolDate(String getPolDate) {
        GetPolDate = getPolDate;
    }

    public String getGetPolTime() {
        return GetPolTime;
    }

    public void setGetPolTime(String getPolTime) {
        GetPolTime = getPolTime;
    }

    public String getCustomGetPolDate() {
        return CustomGetPolDate;
    }

    public void setCustomGetPolDate(String customGetPolDate) {
        CustomGetPolDate = customGetPolDate;
    }

    public String getState() {
        return State;
    }

    public void setState(String state) {
        State = state;
    }

    public String getOperator() {
        return Operator;
    }

    public void setOperator(String operator) {
        Operator = operator;
    }

    public String getMakeDate() {
        return MakeDate;
    }

    public void setMakeDate(String makeDate) {
        MakeDate = makeDate;
    }

    public String getMakeTime() {
        return MakeTime;
    }

    public void setMakeTime(String makeTime) {
        MakeTime = makeTime;
    }

    public String getModifyDate() {
        return ModifyDate;
    }

    public void setModifyDate(String modifyDate) {
        ModifyDate = modifyDate;
    }

    public String getModifyTime() {
        return ModifyTime;
    }

    public void setModifyTime(String modifyTime) {
        ModifyTime = modifyTime;
    }

    public String getFirstTrialOperator() {
        return FirstTrialOperator;
    }

    public void setFirstTrialOperator(String firstTrialOperator) {
        FirstTrialOperator = firstTrialOperator;
    }

    public String getFirstTrialDate() {
        return FirstTrialDate;
    }

    public void setFirstTrialDate(String firstTrialDate) {
        FirstTrialDate = firstTrialDate;
    }

    public String getFirstTrialTime() {
        return FirstTrialTime;
    }

    public void setFirstTrialTime(String firstTrialTime) {
        FirstTrialTime = firstTrialTime;
    }

    public String getReceiveOperator() {
        return ReceiveOperator;
    }

    public void setReceiveOperator(String receiveOperator) {
        ReceiveOperator = receiveOperator;
    }

    public String getReceiveDate() {
        return ReceiveDate;
    }

    public void setReceiveDate(String receiveDate) {
        ReceiveDate = receiveDate;
    }

    public String getReceiveTime() {
        return ReceiveTime;
    }

    public void setReceiveTime(String receiveTime) {
        ReceiveTime = receiveTime;
    }

    public String getTempFeeNo() {
        return TempFeeNo;
    }

    public void setTempFeeNo(String tempFeeNo) {
        TempFeeNo = tempFeeNo;
    }

    public String getSellType() {
        return SellType;
    }

    public void setSellType(String sellType) {
        SellType = sellType;
    }

    public String getForceUWFlag() {
        return ForceUWFlag;
    }

    public void setForceUWFlag(String forceUWFlag) {
        ForceUWFlag = forceUWFlag;
    }

    public String getForceUWReason() {
        return ForceUWReason;
    }

    public void setForceUWReason(String forceUWReason) {
        ForceUWReason = forceUWReason;
    }

    public String getNewBankCode() {
        return NewBankCode;
    }

    public void setNewBankCode(String newBankCode) {
        NewBankCode = newBankCode;
    }

    public String getNewBankAccNo() {
        return NewBankAccNo;
    }

    public void setNewBankAccNo(String newBankAccNo) {
        NewBankAccNo = newBankAccNo;
    }

    public String getNewAccName() {
        return NewAccName;
    }

    public void setNewAccName(String newAccName) {
        NewAccName = newAccName;
    }

    public String getNewPayMode() {
        return NewPayMode;
    }

    public void setNewPayMode(String newPayMode) {
        NewPayMode = newPayMode;
    }

    public String getAgentBankCode() {
        return AgentBankCode;
    }

    public void setAgentBankCode(String agentBankCode) {
        AgentBankCode = agentBankCode;
    }

    public String getBankAgent() {
        return BankAgent;
    }

    public void setBankAgent(String bankAgent) {
        BankAgent = bankAgent;
    }

    public String getBankAgentName() {
        return BankAgentName;
    }

    public void setBankAgentName(String bankAgentName) {
        BankAgentName = bankAgentName;
    }

    public String getBankAgentTel() {
        return BankAgentTel;
    }

    public void setBankAgentTel(String bankAgentTel) {
        BankAgentTel = bankAgentTel;
    }

    public String getProdSetCode() {
        return ProdSetCode;
    }

    public void setProdSetCode(String prodSetCode) {
        ProdSetCode = prodSetCode;
    }

    public String getPolicyNo() {
        return PolicyNo;
    }

    public void setPolicyNo(String policyNo) {
        PolicyNo = policyNo;
    }

    public String getBillPressNo() {
        return BillPressNo;
    }

    public void setBillPressNo(String billPressNo) {
        BillPressNo = billPressNo;
    }

    public String getCardType() {
        return CardType;
    }

    public void setCardType(String cardType) {
        CardType = cardType;
    }

    public String getCardTypeCode() {
        return CardTypeCode;
    }

    public void setCardTypeCode(String cardTypeCode) {
        CardTypeCode = cardTypeCode;
    }

    public String getVisitDate() {
        return VisitDate;
    }

    public void setVisitDate(String visitDate) {
        VisitDate = visitDate;
    }

    public String getVisitTime() {
        return VisitTime;
    }

    public void setVisitTime(String visitTime) {
        VisitTime = visitTime;
    }

    public String getSaleCom() {
        return SaleCom;
    }

    public void setSaleCom(String saleCom) {
        SaleCom = saleCom;
    }

    public String getPrintFlag() {
        return PrintFlag;
    }

    public void setPrintFlag(String printFlag) {
        PrintFlag = printFlag;
    }

    public String getInvoicePrtFlag() {
        return InvoicePrtFlag;
    }

    public void setInvoicePrtFlag(String invoicePrtFlag) {
        InvoicePrtFlag = invoicePrtFlag;
    }

    public String getNewReinsureFlag() {
        return NewReinsureFlag;
    }

    public void setNewReinsureFlag(String newReinsureFlag) {
        NewReinsureFlag = newReinsureFlag;
    }

    public String getRenewPayFlag() {
        return RenewPayFlag;
    }

    public void setRenewPayFlag(String renewPayFlag) {
        RenewPayFlag = renewPayFlag;
    }

    public String getAppntFirstName() {
        return AppntFirstName;
    }

    public void setAppntFirstName(String appntFirstName) {
        AppntFirstName = appntFirstName;
    }

    public String getAppntLastName() {
        return AppntLastName;
    }

    public void setAppntLastName(String appntLastName) {
        AppntLastName = appntLastName;
    }

    public String getInsuredFirstName() {
        return InsuredFirstName;
    }

    public void setInsuredFirstName(String insuredFirstName) {
        InsuredFirstName = insuredFirstName;
    }

    public String getInsuredLastName() {
        return InsuredLastName;
    }

    public void setInsuredLastName(String insuredLastName) {
        InsuredLastName = insuredLastName;
    }

    public String getAuthorFlag() {
        return AuthorFlag;
    }

    public void setAuthorFlag(String authorFlag) {
        AuthorFlag = authorFlag;
    }

    public int getGreenChnl() {
        return GreenChnl;
    }

    public void setGreenChnl(int greenChnl) {
        GreenChnl = greenChnl;
    }

    public String getTBType() {
        return TBType;
    }

    public void setTBType(String TBType) {
        this.TBType = TBType;
    }

    public String getEAuto() {
        return EAuto;
    }

    public void setEAuto(String EAuto) {
        this.EAuto = EAuto;
    }

    public String getSlipForm() {
        return SlipForm;
    }

    public void setSlipForm(String slipForm) {
        SlipForm = slipForm;
    }

    public String getThirdPartyOrderId() {
        return ThirdPartyOrderId;
    }

    public void setThirdPartyOrderId(String thirdPartyOrderId) {
        ThirdPartyOrderId = thirdPartyOrderId;
    }

    public String getUserId() {
        return UserId;
    }

    public void setUserId(String userId) {
        UserId = userId;
    }

    public String getBankProvince() {
        return BankProvince;
    }

    public void setBankProvince(String bankProvince) {
        BankProvince = bankProvince;
    }

    public String getBankCity() {
        return BankCity;
    }

    public void setBankCity(String bankCity) {
        BankCity = bankCity;
    }

    public String getAccType() {
        return AccType;
    }

    public void setAccType(String accType) {
        AccType = accType;
    }

    public int getRnewFlag() {
        return RnewFlag;
    }

    public void setRnewFlag(int rnewFlag) {
        RnewFlag = rnewFlag;
    }

    @Override
    public String toString() {
        return "LCCont{" +
                "ContID=" + ContID +
                ", ShardingID='" + ShardingID + '\'' +
                ", GrpContNo='" + GrpContNo + '\'' +
                ", ContNo='" + ContNo + '\'' +
                ", ProposalContNo='" + ProposalContNo + '\'' +
                ", PrtNo='" + PrtNo + '\'' +
                ", ContType='" + ContType + '\'' +
                ", FamilyType='" + FamilyType + '\'' +
                ", FamilyID='" + FamilyID + '\'' +
                ", PolType='" + PolType + '\'' +
                ", CardFlag='" + CardFlag + '\'' +
                ", ManageCom='" + ManageCom + '\'' +
                ", ExecuteCom='" + ExecuteCom + '\'' +
                ", AgentCom='" + AgentCom + '\'' +
                ", AgentCode='" + AgentCode + '\'' +
                ", AgentGroup='" + AgentGroup + '\'' +
                ", AgentGroup='" + AgentGroup + '\'' +
                ", AgentCode1='" + AgentCode1 + '\'' +
                ", AgentType='" + AgentType + '\'' +
                ", SaleChnl='" + SaleChnl + '\'' +
                ", Handler='" + Handler + '\'' +
                ", Password='" + Password + '\'' +
                ", AppntNo='" + AppntNo + '\'' +
                ", AppntName='" + AppntName + '\'' +
                ", AppntSex='" + AppntSex + '\'' +
                ", AppntBirthday='" + AppntBirthday + '\'' +
                ", AppntIDType='" + AppntIDType + '\'' +
                ", AppntIDNo='" + AppntIDNo + '\'' +
                ", InsuredNo='" + InsuredNo + '\'' +
                ", InsuredName='" + InsuredName + '\'' +
                ", InsuredSex='" + InsuredSex + '\'' +
                ", InsuredBirthday='" + InsuredBirthday + '\'' +
                ", InsuredIDType='" + InsuredIDType + '\'' +
                ", InsuredIDNo='" + InsuredIDNo + '\'' +
                ", PayIntv=" + PayIntv +
                ", PayMode='" + PayMode + '\'' +
                ", PayLocation='" + PayLocation + '\'' +
                ", DisputedFlag='" + DisputedFlag + '\'' +
                ", OutPayFlag='" + OutPayFlag + '\'' +
                ", GetPolMode='" + GetPolMode + '\'' +
                ", SignCom='" + SignCom + '\'' +
                ", SignDate='" + SignDate + '\'' +
                ", SignTime='" + SignTime + '\'' +
                ", ConsignNo='" + ConsignNo + '\'' +
                ", BankCode='" + BankCode + '\'' +
                ", BankAccNo='" + BankAccNo + '\'' +
                ", AccName='" + AccName + '\'' +
                ", PrintCount=" + PrintCount +
                ", LostTimes=" + LostTimes +
                ", Lang='" + Lang + '\'' +
                ", Currency='" + Currency + '\'' +
                ", Remark='" + Remark + '\'' +
                ", Peoples=" + Peoples +
                ", Mult=" + Mult +
                ", Prem=" + Prem +
                ", Amnt=" + Amnt +
                ", SumPrem=" + SumPrem +
                ", Dif=" + Dif +
                ", PaytoDate='" + PaytoDate + '\'' +
                ", FirstPayDate='" + FirstPayDate + '\'' +
                ", CValiDate='" + CValiDate + '\'' +
                ", InputOperator='" + InputOperator + '\'' +
                ", InputDate='" + InputDate + '\'' +
                ", InputTime='" + InputTime + '\'' +
                ", ApproveFlag='" + ApproveFlag + '\'' +
                ", ApproveCode='" + ApproveCode + '\'' +
                ", ApproveDate='" + ApproveDate + '\'' +
                ", ApproveTime='" + ApproveTime + '\'' +
                ", UWFlag='" + UWFlag + '\'' +
                ", UWOperator='" + UWOperator + '\'' +
                ", UWDate='" + UWDate + '\'' +
                ", UWTime='" + UWTime + '\'' +
                ", AppFlag='" + AppFlag + '\'' +
                ", PolApplyDate='" + PolApplyDate + '\'' +
                ", GetPolDate='" + GetPolDate + '\'' +
                ", GetPolTime='" + GetPolTime + '\'' +
                ", CustomGetPolDate='" + CustomGetPolDate + '\'' +
                ", State='" + State + '\'' +
                ", Operator='" + Operator + '\'' +
                ", MakeDate='" + MakeDate + '\'' +
                ", MakeTime='" + MakeTime + '\'' +
                ", ModifyDate='" + ModifyDate + '\'' +
                ", ModifyTime='" + ModifyTime + '\'' +
                ", FirstTrialOperator='" + FirstTrialOperator + '\'' +
                ", FirstTrialDate='" + FirstTrialDate + '\'' +
                ", FirstTrialTime='" + FirstTrialTime + '\'' +
                ", ReceiveOperator='" + ReceiveOperator + '\'' +
                ", ReceiveDate='" + ReceiveDate + '\'' +
                ", ReceiveTime='" + ReceiveTime + '\'' +
                ", TempFeeNo='" + TempFeeNo + '\'' +
                ", SellType='" + SellType + '\'' +
                ", ForceUWFlag='" + ForceUWFlag + '\'' +
                ", ForceUWReason='" + ForceUWReason + '\'' +
                ", NewBankCode='" + NewBankCode + '\'' +
                ", NewBankAccNo='" + NewBankAccNo + '\'' +
                ", NewAccName='" + NewAccName + '\'' +
                ", NewPayMode='" + NewPayMode + '\'' +
                ", AgentBankCode='" + AgentBankCode + '\'' +
                ", BankAgent='" + BankAgent + '\'' +
                ", BankAgentName='" + BankAgentName + '\'' +
                ", BankAgentTel='" + BankAgentTel + '\'' +
                ", ProdSetCode='" + ProdSetCode + '\'' +
                ", PolicyNo='" + PolicyNo + '\'' +
                ", BillPressNo='" + BillPressNo + '\'' +
                ", CardType='" + CardType + '\'' +
                ", CardTypeCode='" + CardTypeCode + '\'' +
                ", VisitDate='" + VisitDate + '\'' +
                ", VisitTime='" + VisitTime + '\'' +
                ", SaleCom='" + SaleCom + '\'' +
                ", PrintFlag='" + PrintFlag + '\'' +
                ", InvoicePrtFlag='" + InvoicePrtFlag + '\'' +
                ", NewReinsureFlag='" + NewReinsureFlag + '\'' +
                ", RenewPayFlag='" + RenewPayFlag + '\'' +
                ", AppntFirstName='" + AppntFirstName + '\'' +
                ", AppntLastName='" + AppntLastName + '\'' +
                ", InsuredFirstName='" + InsuredFirstName + '\'' +
                ", InsuredLastName='" + InsuredLastName + '\'' +
                ", AuthorFlag='" + AuthorFlag + '\'' +
                ", GreenChnl=" + GreenChnl +
                ", TBType='" + TBType + '\'' +
                ", EAuto='" + EAuto + '\'' +
                ", SlipForm='" + SlipForm + '\'' +
                ", ThirdPartyOrderId='" + ThirdPartyOrderId + '\'' +
                ", UserId='" + UserId + '\'' +
                ", BankProvince='" + BankProvince + '\'' +
                ", BankCity='" + BankCity + '\'' +
                ", AccType='" + AccType + '\'' +
                '}';
    }
}
