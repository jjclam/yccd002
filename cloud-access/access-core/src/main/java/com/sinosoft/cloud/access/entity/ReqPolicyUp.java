package com.sinosoft.cloud.access.entity;

/**
 * 保单挂起请求PC005
 */
public class ReqPolicyUp {
    //保单号
    private String ContNo;
    //保费金额
    private String Prem;
    //应缴期数
    private String DuePeriod;
    //应缴日期
    private String DueDate;

    public String getContNo() {
        return ContNo;
    }

    public void setContNo(String contNo) {
        ContNo = contNo;
    }

    public String getPrem() {
        return Prem;
    }

    public void setPrem(String prem) {
        Prem = prem;
    }

    public String getDuePeriod() {
        return DuePeriod;
    }

    public void setDuePeriod(String duePeriod) {
        DuePeriod = duePeriod;
    }

    public String getDueDate() {
        return DueDate;
    }

    public void setDueDate(String dueDate) {
        DueDate = dueDate;
    }

   
    public String getV(String s) {
        return null;
    }

   
    public String getV(int i) {
        return null;
    }

   
    public int getFieldType(String s) {
        return 0;
    }

   
    public int getFieldType(int i) {
        return 0;
    }

   
    public int getFieldCount() {
        return 0;
    }

   
    public int getFieldIndex(String s) {
        return 0;
    }

   
    public String getFieldName(int i) {
        return null;
    }

   
    public boolean setV(String s, String s1) {
        return false;
    }

   @Override
    public String toString() {
        return "ReqPolicyUp{" +
                "ContNo='" + ContNo + '\'' +
                ", Prem='" + Prem + '\'' +
                ", DuePeriod='" + DuePeriod + '\'' +
                ", DueDate='" + DueDate + '\'' +
                '}';
    }
}
