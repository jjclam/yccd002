package com.sinosoft.cloud.access.entity;

/**
 * @program: abc-cloud-access
 * @description:
 * @author: BaoYongmeng
 * @create: 2018-12-25 11:47
 **/
public class BalanceInfo {
    //结算日期
    private String BalanceDate;
    //结算利率
    private String BalanceRate;

    public void setBalanceDate(String balanceDate) {
        BalanceDate = balanceDate;
    }

    public void setBalanceRate(String balanceRate) {
        BalanceRate = balanceRate;
    }

    public String getBalanceDate() {
        return BalanceDate;
    }

    public String getBalanceRate() {
        return BalanceRate;
    }

    @Override
    public String toString() {
        return "BalanceInfo{" +
                "BalanceDate='" + BalanceDate + '\'' +
                ", BalanceRate='" + BalanceRate + '\'' +
                '}';
    }
}
