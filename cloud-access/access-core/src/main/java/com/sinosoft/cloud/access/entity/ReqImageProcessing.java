package com.sinosoft.cloud.access.entity;

import com.sinosoft.lis.pos.entity.ImgPathsPojo;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.List;

/**
 * @Author:lizhen
 * @Description:
 * @Date:Created in 20:32 2018/11/15
 * @Modified By:
 */
/*
* 影像处理 pc003
* */
public class ReqImageProcessing {
    private String Count;
    private String ManageCom;
    private String SerialNo;
    private String Contno;
    private String ImgType;
    @XStreamImplicit(itemFieldName="ImgPaths")
    private List<ImgPaths> ImgPaths;

    public String getCount() {
        return Count;
    }

    public void setCount(String count) {
        Count = count;
    }

    public String getManageCom() {
        return ManageCom;
    }

    public void setManageCom(String manageCom) {
        ManageCom = manageCom;
    }

    public String getSerialNo() {
        return SerialNo;
    }

    public void setSerialNo(String serialNo) {
        SerialNo = serialNo;
    }

    public String getContno() {
        return Contno;
    }

    public void setContno(String contno) {
        Contno = contno;
    }

    public String getImgType() {
        return ImgType;
    }

    public void setImgType(String imgType) {
        ImgType = imgType;
    }

    public List<com.sinosoft.cloud.access.entity.ImgPaths> getImgPaths() {
        return ImgPaths;
    }

    public void setImgPaths(List<com.sinosoft.cloud.access.entity.ImgPaths> imgPaths) {
        ImgPaths = imgPaths;
    }

    @Override
    public String toString() {
        return "ReqImageProcessing{" +
                "Count='" + Count + '\'' +
                ", ManageCom='" + ManageCom + '\'' +
                ", SerialNo='" + SerialNo + '\'' +
                ", Contno='" + Contno + '\'' +
                ", ImgType='" + ImgType + '\'' +
                ", ImgPaths=" + ImgPaths +
                '}';
    }
}