package com.sinosoft.cloud.access.entity;

import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.List;

/**
 * @program: abc-cloud-access
 * @description:
 * @author: BaoYongmeng
 * @create: 2018-12-25 11:47
 **/
public class BalanceInfos {
    @XStreamImplicit(itemFieldName="BalanceInfo")
    private List<BalanceInfo> BalanceInfo;

    public List<BalanceInfo> getBalanceInfo() {
        return BalanceInfo;
    }

    public void setBalanceInfo(List<BalanceInfo> balanceInfo) {
        BalanceInfo = balanceInfo;
    }

    @Override
    public String toString() {
        return "BalanceInfos{" +
                "BalanceInfo=" + BalanceInfo +
                '}';
    }
}
