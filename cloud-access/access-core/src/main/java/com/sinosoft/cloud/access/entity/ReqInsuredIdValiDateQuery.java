package com.sinosoft.cloud.access.entity;
/**
 * 被保人身份证有效期变更查询POS053.
 */
public class ReqInsuredIdValiDateQuery {

    //保全项目编码
    private String EdorType;
    //证件号
    private String CustomerIDNo;
    //证件姓名
    private String CustomerName;

    public String getEdorType() {
        return EdorType;
    }

    public void setEdorType(String edorType) {
        EdorType = edorType;
    }

    public String getCustomerIDNo() {
        return CustomerIDNo;
    }

    public void setCustomerIDNo(String customerIDNo) {
        CustomerIDNo = customerIDNo;
    }

    public String getCustomerName() {
        return CustomerName;
    }

    public void setCustomerName(String customerName) {
        CustomerName = customerName;
    }

    @Override
    public String toString() {
        return "ReqInsuredIdValiDateQuery{" +
                "EdorType='" + EdorType + '\'' +
                ", CustomerIDNo='" + CustomerIDNo + '\'' +
                ", CustomerName='" + CustomerName + '\'' +
                '}';
    }
}
