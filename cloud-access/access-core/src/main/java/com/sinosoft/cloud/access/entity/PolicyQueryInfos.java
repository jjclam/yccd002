package com.sinosoft.cloud.access.entity;

import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.List;

/**
 * @Author:lizhen
 * @Description:
 * @Date:Created in 17:46 2018/11/9
 * @Modified By:
 */
public class PolicyQueryInfos {

    @XStreamImplicit(itemFieldName="PolicyQueryInfo")
    private List<PolicyQueryInfo> PolicyQueryInfo;

    public List<com.sinosoft.cloud.access.entity.PolicyQueryInfo> getPolicyQueryInfo() {
        return PolicyQueryInfo;
    }

    public void setPolicyQueryInfo(List<com.sinosoft.cloud.access.entity.PolicyQueryInfo> policyQueryInfo) {
        PolicyQueryInfo = policyQueryInfo;
    }

    @Override
    public String toString() {
        return "PolicyQueryInfos{" +
                "PolicyQueryInfo=" + PolicyQueryInfo +
                '}';
    }
}