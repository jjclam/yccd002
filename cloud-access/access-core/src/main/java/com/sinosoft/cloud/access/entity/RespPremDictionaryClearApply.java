package com.sinosoft.cloud.access.entity;

/**
 * @Author:lizhen
 * @Description:
 * @Date:Created in 22:23 2018/11/12
 * @Modified By:
 */
/**
 * 4004保费自垫清偿申请POS008.  返回报文
 */
public class RespPremDictionaryClearApply {
    //保全项目编码
    private String EdorType;
    //保单号
    private String ContNo;
    //保全受理号
    private String EdorAcceptNo;
    //保全批单号
    private String EdorNo;

    public String getEdorNo() {
        return EdorNo;
    }

    public void setEdorNo(String edorNo) {
        EdorNo = edorNo;
    }

    public String getEdorType() {
        return EdorType;
    }

    public void setEdorType(String edorType) {
        EdorType = edorType;
    }

    public String getContNo() {
        return ContNo;
    }

    public void setContNo(String contNo) {
        ContNo = contNo;
    }

    public String getEdorAcceptNo() {
        return EdorAcceptNo;
    }

    public void setEdorAcceptNo(String edorAcceptNo) {
        EdorAcceptNo = edorAcceptNo;
    }

    @Override
    public String toString() {
        return "RespPremDictionaryClearApply{" +
                "EdorType='" + EdorType + '\'' +
                ", ContNo='" + ContNo + '\'' +
                ", EdorAcceptNo='" + EdorAcceptNo + '\'' +
                ", EdorNo='" + EdorNo + '\'' +
                '}';
    }
}