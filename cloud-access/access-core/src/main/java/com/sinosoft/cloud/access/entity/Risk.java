package com.sinosoft.cloud.access.entity;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("Risk")
public class Risk {

    @XStreamAlias("RiskCode")
    private String riskCode;
    @XStreamAlias("MainRiskCode")
    private String mainRiskCode;
    @XStreamAlias("Amnt")
    private String amnt;
    @XStreamAlias("Prem")
    private String prem;
    @XStreamAlias("Mult")
    private String mult;
    @XStreamAlias("PayIntv")
    private String payIntv;
    @XStreamAlias("PayMode")
    private String payMode;
    @XStreamAlias("ExPayMode")
    private String exPayMode;
    @XStreamAlias("InsuYearFlag")
    private String insuYearFlag;
    @XStreamAlias("PayEndYearFlag")
    private String payEndYearFlag;
    @XStreamAlias("InsuYear")
    private String insuYear;
    @XStreamAlias("PayEndYear")
    private String payEndYear;
    @XStreamAlias("BonusGetMode")
    private String bonusGetMode;
    @XStreamAlias("GetIntv")
    private String getIntv;
    //1002获取自动续保标志
    @XStreamAlias("RnewFlag")
    private String rnewFlag;
    //1016返回
    @XStreamAlias("RiskName")
    private String riskName;
    @XStreamAlias("SignDate")
    private String signDate;
    @XStreamAlias("CValiDate")
    private String CValiDate;
    @XStreamAlias("InsuEndDate")
    private String insuEndDate;
    //1004所需字段
    /**
     * 现金价值
     */
    @XStreamAlias("CashValues")
    private CashValues cashValues; // add by zhaoshulei
    @XStreamAlias("EndDate")
    private String endDate;
    @XStreamAlias("AutoPayFlag")
    private String autoPayFlag;
    @XStreamAlias("GetYear")
    private String getYear;
    @XStreamAlias("GetYearFlag")
    private String getYearFlag;
    @XStreamAlias("LiveGetMode")
    private String liveGetMode;
    @XStreamAlias("ReducedVolumes")
    private ReducedVolumes reducedVolumes;
    @XStreamAlias("PolicyStatus")
    private String policyStatus;
    @XStreamAlias("PolicyValue")
    private String policyValue;
    @XStreamAlias("AccountValue")
    private String accountValue;

    public String getMainRiskCode() {
        return mainRiskCode;
    }

    public void setMainRiskCode(String mainRiskCode) {
        this.mainRiskCode = mainRiskCode;
    }

    public String getAmnt() {
        return amnt;
    }

    public void setAmnt(String amnt) {
        this.amnt = amnt;
    }

    public String getPrem() {
        return prem;
    }

    public void setPrem(String prem) {
        this.prem = prem;
    }

    public String getMult() {
        return mult;
    }

    public void setMult(String mult) {
        this.mult = mult;
    }

    public String getPayIntv() {
        return payIntv;
    }

    public void setPayIntv(String payIntv) {
        this.payIntv = payIntv;
    }

    public String getPayMode() {
        return payMode;
    }

    public void setPayMode(String payMode) {
        this.payMode = payMode;
    }

    public String getExPayMode() {
        return exPayMode;
    }

    public void setExPayMode(String exPayMode) {
        this.exPayMode = exPayMode;
    }

    public String getInsuYearFlag() {
        return insuYearFlag;
    }

    public void setInsuYearFlag(String insuYearFlag) {
        this.insuYearFlag = insuYearFlag;
    }

    public String getPayEndYearFlag() {
        return payEndYearFlag;
    }

    public void setPayEndYearFlag(String payEndYearFlag) {
        this.payEndYearFlag = payEndYearFlag;
    }

    public String getInsuYear() {
        return insuYear;
    }

    public void setInsuYear(String insuYear) {
        this.insuYear = insuYear;
    }

    public String getPayEndYear() {
        return payEndYear;
    }

    public void setPayEndYear(String payEndYear) {
        this.payEndYear = payEndYear;
    }

    public String getBonusGetMode() {
        return bonusGetMode;
    }

    public void setBonusGetMode(String bonusGetMode) {
        this.bonusGetMode = bonusGetMode;
    }

    public String getGetIntv() {
        return getIntv;
    }

    public void setGetIntv(String getIntv) {
        this.getIntv = getIntv;
    }

    public String getRnewFlag() {
        return rnewFlag;
    }

    public void setRnewFlag(String rnewFlag) {
        this.rnewFlag = rnewFlag;
    }

    public String getRiskName() {
        return riskName;
    }

    public void setRiskName(String riskName) {
        this.riskName = riskName;
    }

    public String getSignDate() {
        return signDate;
    }

    public void setSignDate(String signDate) {
        this.signDate = signDate;
    }

    public String getCValiDate() {
        return CValiDate;
    }

    public void setCValiDate(String CValiDate) {
        this.CValiDate = CValiDate;
    }

    public String getInsuEndDate() {
        return insuEndDate;
    }

    public void setInsuEndDate(String insuEndDate) {
        this.insuEndDate = insuEndDate;
    }

    public CashValues getCashValues() {
        return cashValues;
    }

    public void setCashValues(CashValues cashValues) {
        this.cashValues = cashValues;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getAutoPayFlag() {
        return autoPayFlag;
    }

    public void setAutoPayFlag(String autoPayFlag) {
        this.autoPayFlag = autoPayFlag;
    }

    public String getGetYear() {
        return getYear;
    }

    public void setGetYear(String getYear) {
        this.getYear = getYear;
    }

    public String getGetYearFlag() {
        return getYearFlag;
    }

    public void setGetYearFlag(String getYearFlag) {
        this.getYearFlag = getYearFlag;
    }

    public String getLiveGetMode() {
        return liveGetMode;
    }

    public void setLiveGetMode(String liveGetMode) {
        this.liveGetMode = liveGetMode;
    }

    public String getRiskCode() {
        return riskCode;
    }

    public void setRiskCode(String riskCode) {
        this.riskCode = riskCode;
    }

    public ReducedVolumes getReducedVolumes() {
        return reducedVolumes;
    }

    public void setReducedVolumes(ReducedVolumes reducedVolumes) {
        this.reducedVolumes = reducedVolumes;
    }

    public String getPolicyStatus() {
        return policyStatus;
    }

    public void setPolicyStatus(String policyStatus) {
        this.policyStatus = policyStatus;
    }

    public String getPolicyValue() {
        return policyValue;
    }

    public void setPolicyValue(String policyValue) {
        this.policyValue = policyValue;
    }

    public String getAccountValue() {
        return accountValue;
    }

    public void setAccountValue(String accountValue) {
        this.accountValue = accountValue;
    }

    @Override
    public String toString() {
        return "Risk{" +
                "riskCode='" + riskCode + '\'' +
                ", mainRiskCode='" + mainRiskCode + '\'' +
                ", amnt='" + amnt + '\'' +
                ", prem='" + prem + '\'' +
                ", mult='" + mult + '\'' +
                ", payIntv='" + payIntv + '\'' +
                ", payMode='" + payMode + '\'' +
                ", exPayMode='" + exPayMode + '\'' +
                ", insuYearFlag='" + insuYearFlag + '\'' +
                ", payEndYearFlag='" + payEndYearFlag + '\'' +
                ", insuYear='" + insuYear + '\'' +
                ", payEndYear='" + payEndYear + '\'' +
                ", bonusGetMode='" + bonusGetMode + '\'' +
                ", getIntv='" + getIntv + '\'' +
                ", rnewFlag='" + rnewFlag + '\'' +
                ", riskName='" + riskName + '\'' +
                ", signDate='" + signDate + '\'' +
                ", CValiDate='" + CValiDate + '\'' +
                ", insuEndDate='" + insuEndDate + '\'' +
                ", cashValues=" + cashValues +
                ", endDate='" + endDate + '\'' +
                ", autoPayFlag='" + autoPayFlag + '\'' +
                ", getYear='" + getYear + '\'' +
                ", getYearFlag='" + getYearFlag + '\'' +
                ", liveGetMode='" + liveGetMode + '\'' +
                ", reducedVolumes=" + reducedVolumes +
                ", policyStatus='" + policyStatus + '\'' +
                ", policyValue='" + policyValue + '\'' +
                ", accountValue='" + accountValue + '\'' +
                '}';
    }

}
