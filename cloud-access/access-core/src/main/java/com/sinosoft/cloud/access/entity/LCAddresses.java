package com.sinosoft.cloud.access.entity;

import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.List;

/**
 * Created by Administrator on 2018/4/11.
 */
public class LCAddresses {
    @XStreamImplicit(itemFieldName="LCAddress")
    private List<LCAddress> LCAddresses;

    public List<LCAddress> getLCAddresses() {
        return LCAddresses;
    }

    public void setLCAddresses(List<LCAddress> LCAddresses) {
        this.LCAddresses = LCAddresses;
    }

    @Override
    public String toString() {
        return "LCAddresses{" +
                "LCAddresses=" + LCAddresses +
                '}';
    }
}
