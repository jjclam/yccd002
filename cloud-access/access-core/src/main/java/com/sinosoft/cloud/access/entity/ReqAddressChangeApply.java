package com.sinosoft.cloud.access.entity;

/**
 * @Author:lizhen
 * @Description:
 * @Date:Created in 19:56 2018/11/15
 * @Modified By:
 */
/**
 * 地址信息变更更数据提交POS070 请求
 */
public class ReqAddressChangeApply {
    //请求类型
    private String EdorType;
    //客户号
    private String CustomerNo;
    //合同号码
    private String ContNo;
    //省
    private String Province;
    //市
    private String City;
    //通讯地址区/县
    private String County;
    //通讯地址
    private String PostalAddress;
    //常住地址村/社区（楼、号）
    private String StoreNo;
    //邮编
    private String ZipCode;
    //邮箱
    private String Email;
    //手机号码
    private String Mobile;
    //申请人信息
    private EdorAppInfo EdorAppInfo;
    //领取人
    private PersonInfo PersonInfo;
    //xxx
    private BankInfo BankInfo;
    //收付费方式
    private String PayMode;

    public String getEdorType() {
        return EdorType;
    }

    public void setEdorType(String edorType) {
        EdorType = edorType;
    }

    public String getCustomerNo() {
        return CustomerNo;
    }

    public void setCustomerNo(String customerNo) {
        CustomerNo = customerNo;
    }

    public String getContNo() {
        return ContNo;
    }

    public void setContNo(String contNo) {
        ContNo = contNo;
    }

    public String getProvince() {
        return Province;
    }

    public void setProvince(String province) {
        Province = province;
    }

    public String getCity() {
        return City;
    }

    public void setCity(String city) {
        City = city;
    }

    public String getCounty() {
        return County;
    }

    public void setCounty(String county) {
        County = county;
    }

    public String getPostalAddress() {
        return PostalAddress;
    }

    public void setPostalAddress(String postalAddress) {
        PostalAddress = postalAddress;
    }

    public String getStoreNo() {
        return StoreNo;
    }

    public void setStoreNo(String storeNo) {
        StoreNo = storeNo;
    }

    public String getZipCode() {
        return ZipCode;
    }

    public void setZipCode(String zipCode) {
        ZipCode = zipCode;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getMobile() {
        return Mobile;
    }

    public void setMobile(String mobile) {
        Mobile = mobile;
    }

    public com.sinosoft.cloud.access.entity.EdorAppInfo getEdorAppInfo() {
        return EdorAppInfo;
    }

    public void setEdorAppInfo(com.sinosoft.cloud.access.entity.EdorAppInfo edorAppInfo) {
        EdorAppInfo = edorAppInfo;
    }

    public com.sinosoft.cloud.access.entity.PersonInfo getPersonInfo() {
        return PersonInfo;
    }

    public void setPersonInfo(com.sinosoft.cloud.access.entity.PersonInfo personInfo) {
        PersonInfo = personInfo;
    }

    public com.sinosoft.cloud.access.entity.BankInfo getBankInfo() {
        return BankInfo;
    }

    public void setBankInfo(com.sinosoft.cloud.access.entity.BankInfo bankInfo) {
        BankInfo = bankInfo;
    }

    public String getPayMode() {
        return PayMode;
    }

    public void setPayMode(String payMode) {
        PayMode = payMode;
    }

    @Override
    public String toString() {
        return "ReqAddressChangeApply{" +
                "EdorType='" + EdorType + '\'' +
                ", CustomerNo='" + CustomerNo + '\'' +
                ", ContNo='" + ContNo + '\'' +
                ", Province='" + Province + '\'' +
                ", City='" + City + '\'' +
                ", County='" + County + '\'' +
                ", PostalAddress='" + PostalAddress + '\'' +
                ", StoreNo='" + StoreNo + '\'' +
                ", ZipCode='" + ZipCode + '\'' +
                ", Email='" + Email + '\'' +
                ", Mobile='" + Mobile + '\'' +
                ", EdorAppInfo=" + EdorAppInfo +
                ", PersonInfo=" + PersonInfo +
                ", BankInfo=" + BankInfo +
                ", PayMode='" + PayMode + '\'' +
                '}';
    }
}