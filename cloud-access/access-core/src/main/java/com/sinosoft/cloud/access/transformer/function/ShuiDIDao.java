package com.sinosoft.cloud.access.transformer.function;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.sinosoft.cloud.access.entity.*;

import com.sinosoft.cloud.rest.TradeInfo;
import com.sinosoft.lis.entity.*;
import com.sinosoft.lis.pubfun.PubFun1;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

@Component
public class ShuiDIDao {

    /**
     * 核保相关json转换
     *
     * @param msg
     * @return
     */
    public static TradeInfo underWriterTransPojos(String msg) {
        TradeInfo tradeInfo = new TradeInfo();
        LCContPojo lcContPojo = new LCContPojo();
        GlobalPojo globalPojo = new GlobalPojo();
        List<LKTransTracksPojo> LKTransTrackss = new ArrayList<LKTransTracksPojo>();
        LKTransTracksPojo lkTransTracksPojo = new LKTransTracksPojo();
        LKTransStatusPojo lkTransStatusPojo = new LKTransStatusPojo();
        LCAddresses lcAddresses = new LCAddresses();


        //将string转成json进行解析
        JSONObject dataJson = JSONObject.parseObject(msg);
        //headers头部
        ShuiDiHead shuiDiHead = new ShuiDiHead();
        JSONObject headers = dataJson.getJSONObject("headers");
        String supplierNo = headers.getString("supplierNo");
        String timeStamp = headers.getString("timeStamp");
        String orderNo = headers.getString("orderNo");
        shuiDiHead.setSupplierNo(supplierNo);
        shuiDiHead.setTimeStamp(timeStamp);
        shuiDiHead.setTransDate(timeStamp.split(" ")[0]);
        shuiDiHead.setTransTime(timeStamp.split(" ")[1]);
        shuiDiHead.setOrderNo(orderNo);
        /* tradeInfo.addData(ShuiDiHead.class.getName(), shuiDiHead);*/

        lkTransTracksPojo.setTransNo(headers.getString("supplierNo"));
        lkTransTracksPojo.setTransDate(timeStamp.split(" ")[0]);
        lkTransTracksPojo.setTransTime(timeStamp.split(" ")[1]);
        LKTransTrackss.add(lkTransTracksPojo);
        lkTransTracksPojo.setBankCode("05");
        lkTransTracksPojo.setBankBranch("26");
        lkTransTracksPojo.setBankNode("26");
        tradeInfo.addData(LKTransTracksPojo.class.getName(), LKTransTrackss);


        lkTransStatusPojo.setTransNo(headers.getString("supplierNo"));
        lkTransStatusPojo.setTransDate(timeStamp.split(" ")[0]);
        lkTransStatusPojo.setTransTime(timeStamp.split(" ")[1]);
        lkTransStatusPojo.setBankCode("05");
        lkTransStatusPojo.setBankBranch("26");
        lkTransStatusPojo.setBankNode("26");
        tradeInfo.addData(LKTransStatusPojo.class.getName(), lkTransStatusPojo);


        globalPojo.setEntrustWay("ShuiDi");
        globalPojo.setSerialNo(headers.getString("supplierNo"));
        globalPojo.setTransNo("proposalOrder");
        globalPojo.setTransDate(timeStamp.split(" ")[0]);
        globalPojo.setTransTime(timeStamp.split(" ")[1]);

        /**
         *  body部分解析封装
         */
        JSONObject body = dataJson.getJSONObject("body");
        //订单信息
        JSONObject orderInfo = body.getJSONObject("orderInfo");
        List<LCAddressPojo> lcAddresss = new ArrayList<LCAddressPojo>();
        //投保人信息以及地址信息封装
        LCAppntPojo lcAppntPojo = new LCAppntPojo();
        LCAddressPojo AppntAddress = new LCAddressPojo();

        JSONObject applyUser = body.getJSONObject("applyUser");

        lcAppntPojo.setAppntName(applyUser.getString("name"));//姓名
        lcAppntPojo.setAppntSex(applyUser.getString("gender"));//性别

        lcAppntPojo.setAppntBirthday(applyUser.getString("birthday"));//生日
        lcAppntPojo.setIDType(applyUser.getString("idType"));//证件类型
        lcAppntPojo.setIDNo(applyUser.getString("idCode"));//证件号码
        String idValidType = applyUser.getString("idValidType");//证件有效期类型 1.到期失效；2：永久有效
        String idValidDay = applyUser.getString("idValidDay");//证件有效期截止时间  idValidType为2时可以为空，格式为: yyyy-MM-dd

        lcAppntPojo.setIdValiDate(getIdValiDate(idValidType, idValidDay));//投保人证件有效期
        lcAppntPojo.setNativePlace("CHN");//默认CHN
        lcAppntPojo.setMarriage(applyUser.getString("maritalStatus"));//婚姻状况

        lcAppntPojo.setSalary(applyUser.getString("annualEarnings"));//年收入万元
        lcAppntPojo.setOccupationType(applyUser.getString("occupation"));//职业类别
        lcAppntPojo.setOccupationCode(applyUser.getString("occupation"));//职业代码
        lcAppntPojo.setStature(applyUser.getString("height"));//身高
        lcAppntPojo.setAvoirdupois(applyUser.getString("weight"));//体重
        lcAppntPojo.setBankCode(applyUser.getString("bankCode"));//银行编码
        lcAppntPojo.setBankAccNo(applyUser.getString("bankNo"));//银行账号
        tradeInfo.addData(LCAppntPojo.class.getName(), lcAppntPojo);

        lcContPojo.setAppntName(applyUser.getString("name"));
        lcContPojo.setAppntSex(applyUser.getString("gender"));//性别
        lcContPojo.setAppntBirthday(applyUser.getString("birthday"));
        lcContPojo.setAppntIDType(applyUser.getString("idType"));
        lcContPojo.setAppntIDNo(applyUser.getString("idCode"));
        /**
         * 投保人地址信息
         */
        AppntAddress.setPostalAddress(applyUser.getString("address"));//详细地址
        AppntAddress.setPhone(applyUser.getString("Phone"));//手机号
        AppntAddress.setEMail(applyUser.getString("mail"));//邮箱
        AppntAddress.setProvince(applyUser.getString("province"));//省
        AppntAddress.setCity(applyUser.getString("city"));//市
        AppntAddress.setCounty(applyUser.getString("district"));//区
        lcAddresss.add(AppntAddress);


        //被保人信息以及被被人地址信息封装
        JSONArray insuredUsers = body.getJSONArray("insuredUsers");
        List<LCInsuredPojo> insureds = new ArrayList<LCInsuredPojo>();
        String InsuredName = null;
        String InsuredSex = null;
        String InsuredBirthday = null;
        String InsuredIDType = null;
        String InsuredIDNo = null;
        for (int i = 0; i < insuredUsers.size(); i++) {
            JSONObject insured = insuredUsers.getJSONObject(i);
            LCInsuredPojo lcInsuredPojo = new LCInsuredPojo();
            InsuredName = insured.getString("name");
            lcInsuredPojo.setName(InsuredName);//被保人姓名
            InsuredSex = insured.getString("gender");
            lcInsuredPojo.setSex(InsuredSex);//性别
            InsuredBirthday = insured.getString("birthday");
            lcInsuredPojo.setBirthday(InsuredBirthday);//生日
            InsuredIDType = insured.getString("idType");
            lcInsuredPojo.setIDType(InsuredIDType);//证件类型
            InsuredIDNo = insured.getString("idCode");
            lcInsuredPojo.setIDNo(InsuredIDNo);//证件号码
            String idValidType1 = insured.getString("idValidType");//证件有效期类型 1.到期失效；2：永久有效
            String idValidDay1 = insured.getString("idValidDay");//证件有效期截止时间  idValidType为2时可以为空，格式为: yyyy-MM-dd
            lcInsuredPojo.setIdValiDate(getIdValiDate(idValidType1, idValidDay1));//证件有效期
            lcInsuredPojo.setNativePlace("CHN");//国籍
            lcInsuredPojo.setRelationToAppnt(insured.getString("relType"));//投被保人关系
            lcInsuredPojo.setStature(insured.getString("height"));//身高
            lcInsuredPojo.setAvoirdupois(insured.getString("weight"));//体重
            lcInsuredPojo.setSalary(insured.getString("annualEarnings"));//年收入
            lcInsuredPojo.setMarriage(insured.getString("maritalStatus"));//婚姻状况
            lcInsuredPojo.setSSFlag(insured.getString("hasSocial"));//社保标记
            lcInsuredPojo.setOccupationType(insured.getString("occupation"));//职业类型
            lcInsuredPojo.setOccupationCode(insured.getString("occupation"));//职业编码
            lcInsuredPojo.setContPlanCode(orderInfo.getString("productNo"));
            insureds.add(lcInsuredPojo);
            //收益人地址信息
            LCAddressPojo insuredAddress = new LCAddressPojo();
            insuredAddress.setPostalAddress(insured.getString("address"));//详细地址
            insuredAddress.setPhone(insured.getString("Phone"));//手机号
            insuredAddress.setEMail(insured.getString("mail"));//邮箱
            insuredAddress.setProvince(insured.getString("province"));//省
            insuredAddress.setCity(insured.getString("city"));//市
            insuredAddress.setCounty(insured.getString("district"));//区
            lcAddresss.add(insuredAddress);
        }
        lcContPojo.setInsuredName(InsuredName);
        lcContPojo.setInsuredSex(InsuredSex);
        lcContPojo.setInsuredBirthday(InsuredBirthday);
        lcContPojo.setInsuredIDType(InsuredIDType);
        lcContPojo.setInsuredIDNo(InsuredIDNo);
        lcContPojo.setSaleChnl("05");


        tradeInfo.addData(LCInsuredPojo.class.getName(), insureds);
        tradeInfo.addData(LCAddressPojo.class.getName(), lcAddresss);
        //收益人信息
        LCBnfPojo lcBnfPojo = new LCBnfPojo();
        lcBnfPojo.setName("法定");
        List<LCBnfPojo> bnfs = new ArrayList<LCBnfPojo>();
        bnfs.add(lcBnfPojo);
        tradeInfo.addData(LCBnfPojo.class.getName(), bnfs);

        List<LCPolPojo> pols = new ArrayList<LCPolPojo>();
        LCPolPojo lcPolPojo = new LCPolPojo();

        // orderNo	水滴订单号
        lcContPojo.setThirdPartyOrderId(orderInfo.getString("orderNo"));//水滴订单号
        lcContPojo.setProposalContNo(orderInfo.getString("orderNo"));//水滴订单号
        //policyType	保单类型
        lcContPojo.setPolType(orderInfo.getString("policyType"));//保单类型
        //productNo 	水滴产品编码
        globalPojo.setMainRiskCode(orderInfo.getString("productNo"));
        lcPolPojo.setRiskCode(orderInfo.getString("productNo"));

        //issueTime	投保时间
        lcContPojo.setCValiDate(orderInfo.getString("issueTime"));

        lcPolPojo.setCValiDate(orderInfo.getString("issueTime"));//
        //effectTime	保单生效时间
        // invalidTime	保单失效时间
        globalPojo.setRiskBeginDate(orderInfo.getString("effectTime"));
        globalPojo.setRiskEndDate(orderInfo.getString("invalidTime"));
        lcPolPojo.setEndDate(orderInfo.getString("invalidTime"));
        // protectPeriodType	保障期限类型
        lcPolPojo.setInsuYearFlag(orderInfo.getString("protectPeriodType"));
        // protectPeriodValue	保障期限
        lcPolPojo.setInsuYear(orderInfo.getString("protectPeriodValue"));
        //firstPeriodPremium	首期保费
        lcPolPojo.setPrem(orderInfo.getString("firstPeriodPremium"));
        lcContPojo.setPrem(orderInfo.getString("firstPeriodPremium"));
        //secondPeriodPremium	期交保费
        // totalAmount	总保额
        lcPolPojo.setAmnt(orderInfo.getString("totalAmount"));
        tradeInfo.addData("totalAmount", orderInfo.getString("totalAmount"));
        lcContPojo.setAmnt(orderInfo.getString("totalAmount"));
        // payPeriodType	交费期限类型
        lcPolPojo.setPayEndYearFlag(orderInfo.getString("payPeriodType"));
        //payPeriodValue	交费年限
        lcPolPojo.setPayEndYear(orderInfo.getString("payPeriodValue"));
        // payFrequency	交费频率
        lcContPojo.setPayIntv(orderInfo.getString("payFrequency"));
        lcPolPojo.setPayIntv(orderInfo.getString("payFrequency"));

        /*
        lcPolPojo.setPayEndDate();
        lcPolPojo.setPayYears();*/


        // isRenewPolicy	是否是续保订单
        // parentPolicyNo	原始保单号
        //familyBasePolicy	家庭基础保单号
        // productChannel	销售渠道
        lcContPojo.setSaleChnl("05");//05-网销渠道
        lcPolPojo.setSaleChnl("05");
        lcContPojo.setSellType("48");//48-水滴保子渠道
        lcContPojo.setAgentCode("0101500048");//默认值

        //saleNo	销售人编码
        lcContPojo.setUserId(orderInfo.getString("saleNo"));
        // saleManage	销售人机构代码
        lcContPojo.setSaleCom(orderInfo.getString("saleManage"));
        // collectPeriodType	领取年限类型
        //collectPeriodValue	领取年限
        //collectFrequencyType	领取频率
        pols.add(lcPolPojo);


        lcContPojo.setContType("1");//默认个险

        //险种信息
        JSONArray orderMainss = orderInfo.getJSONArray("orderMains");
        if (orderMainss != null && orderMainss.size() != 0) {
            for (int i = 0; i < orderMainss.size(); i++) {
                JSONObject pol = insuredUsers.getJSONObject(i);
                LCPolPojo lcPol = new LCPolPojo();
                lcPol.setRiskCode(pol.getString("supplierProductNo"));//险种编码
                lcPol.setMult(1);//份数默认1
                lcPol.setPrem(pol.getString("firstPremium"));//首期保费
                lcPol.setAmnt(pol.getString("amount"));//保额
                pols.add(lcPolPojo);

            }
        }


        tradeInfo.addData(LCPolPojo.class.getName(), pols);

        //globalPojo
        tradeInfo.addData(GlobalPojo.class.getName(), globalPojo);
        tradeInfo.addData(LCContPojo.class.getName(), lcContPojo);

        /**
         *                 <LDPersons/>
         *                 <!--<LCDutys/>-->
         *                 <LCGets/>
         *                 <LCPrems/>
         *                 <LCCustomerImpartParamses/>
         *                 <LCCustomerImpartDetails/>
         */
        List<LDPersonPojo> persons = new ArrayList<LDPersonPojo>();
        tradeInfo.addData(LDPersonPojo.class.getName(), persons);

        List<LCDutyPojo> dutys = new ArrayList<LCDutyPojo>();
        tradeInfo.addData(LCDutyPojo.class.getName(), dutys);

        List<LCPremPojo> prems = new ArrayList<LCPremPojo>();
        tradeInfo.addData(LCPremPojo.class.getName(), prems);

        List<LCCustomerImpartParamsPojo> LCCustomerImpartParamses = new ArrayList<LCCustomerImpartParamsPojo>();
        tradeInfo.addData(LCCustomerImpartParamsPojo.class.getName(), LCCustomerImpartParamses);

        List<LCCustomerImpartDetailPojo> LCCustomerImpartDetails = new ArrayList<LCCustomerImpartDetailPojo>();
        tradeInfo.addData(LCCustomerImpartDetailPojo.class.getName(), LCCustomerImpartDetails);



        return tradeInfo;
    }

    /**
     * 超过1000万数据进行封装成xml
     */
    public static String data2Xml(TradeInfo info) {
        LKTransStatusPojo lkTransStatusPojo=(LKTransStatusPojo)info.getData(LKTransStatusPojo.class.getName());
        //创建Document 对象，代表整个 xml文档
        Document document = DocumentHelper.createDocument();
        //创建根节点
        Element Package = document.addElement("Package");
        //创建子节点Head
        Element Head = Package.addElement("Head");
        /**
         * <TransTime>2019-10-08 09:52:28</TransTime>
         *         <ThirdPartyCode>11</ThirdPartyCode>
         *         <TransNo>HuiZe20191008095321198</TransNo>
         *         <TransType>A0021</TransType>
         *         <Coworker>01</Coworker>
         */
        Date date = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("YYYY-MM-DD mm:hh:ss");
        Element TransTime = Head.addElement("TransTime");
        TransTime.setText(sdf.format(date));
        Element ThirdPartyCode = Head.addElement("ThirdPartyCode");
        ThirdPartyCode.setText("48");
        Element TransNo = Head.addElement("TransNo");
        TransNo.setText(lkTransStatusPojo.getTransNo());
        Element TransType = Head.addElement("TransType");
        TransType.setText("A0021");
        Element Coworker = Head.addElement("Coworker");
        Coworker.setText("01");
        Element RequestNodes = Package.addElement("RequestNodes");
        Element RequestNode = RequestNodes.addElement("RequestNode");

        List<LCPolPojo> pols =(List)info.getData(LCPol.class.getName());
        LCContPojo lccont=(LCContPojo)info.getData(LCContPojo.class.getName());
        Element ProductCode = RequestNode.addElement("ProductCode");
        ProductCode.setText(pols.get(0).getRiskCode());
        Element ProductName = RequestNode.addElement("ProductName");
        Element ThirdPartyOrderId = RequestNode.addElement("ThirdPartyOrderId");
        ThirdPartyCode.setText(lccont.getThirdPartyOrderId());
        Element UserId = RequestNode.addElement("UserId");
        UserId.setText(lccont.getUserId());
        Element AssetId = RequestNode.addElement("AssetId");

        Element ContNo = RequestNode.addElement("ContNo");
        ContNo.setText(lccont.getContNo());
        Element ProposalNo = RequestNode.addElement("ProposalNo");
        ProposalNo.setText(lccont.getProposalContNo());
        Element InsuranceStartPeriod = RequestNode.addElement("InsuranceStartPeriod");
        InsuranceStartPeriod.setText(lccont.getCValiDate());
        Element InsuranceEndPeriod = RequestNode.addElement("InsuranceEndPeriod");

        Element SignedDate = RequestNode.addElement("SignedDate");
        SignedDate.setText(lccont.getPolApplyDate());
        Element InsuYear = RequestNode.addElement("InsuYear");
        Element InsuYearFlag = RequestNode.addElement("InsuYearFlag");
        Element PayIntv = RequestNode.addElement("PayIntv");
        Element PayEndYear = RequestNode.addElement("PayEndYear");
        Element Premium = RequestNode.addElement("Premium");
        Element InsuredAmount = RequestNode.addElement("InsuredAmount");
        Element GrossPremium = RequestNode.addElement("GrossPremium");
        Element FaceAmount = RequestNode.addElement("FaceAmount");
        Element FirstPremium = RequestNode.addElement("FirstPremium");
        Element InitialPremAmt = RequestNode.addElement("InitialPremAmt");
        Element PolicyDeliveryFee = RequestNode.addElement("PolicyDeliveryFee");
        Element UnitCount = RequestNode.addElement("UnitCount");
        Element AgentNo = RequestNode.addElement("AgentNo");
        Element ABCode = RequestNode.addElement("ABCode");
        Element ExpireProcessMode = RequestNode.addElement("ExpireProcessMode");
        Element ProductPeriod = RequestNode.addElement("ProductPeriod");
        Element ProductPeriodFlag = RequestNode.addElement("ProductPeriodFlag");

        List<LCAddressPojo> addres=(List)info.getData(LCAddressPojo.class.getName());
        //投保人
        LCAppntPojo lcAppnt=(LCAppntPojo)info.getData(LCAppntPojo.class.getName());
        LCAddressPojo appntaddress=addres.get(0);
        Element Applicant = RequestNode.addElement("Applicant");
        Element AppNo = Applicant.addElement("AppNo");
        AppNo.setText(lcAppnt.getAppntNo());
        Element RelationToInsured = Applicant.addElement("RelationToInsured");
        RelationToInsured.setText(lcAppnt.getRelatToInsu());
        Element AppName = Applicant.addElement("AppName");
        AppName.setText(lcAppnt.getAppntName());
        Element AppSex = Applicant.addElement("AppSex");
        AppSex.setText(lcAppnt.getAppntSex());
        Element AppBirthday = Applicant.addElement("AppBirthday");
        AppBirthday.setText(lcAppnt.getAppntBirthday());
        Element AppIDType = Applicant.addElement("AppIDType");
        AppIDType.setText(lcAppnt.getIDType());
        Element AppIDNo = Applicant.addElement("AppIDNo");
        AppIDNo.setText(lcAppnt.getIDNo());
        Element IdexpDate = Applicant.addElement("IdexpDate");
        IdexpDate.setText(lcAppnt.getIdValiDate());
        Element NativePlace = Applicant.addElement("NativePlace");
        NativePlace.setText(lcAppnt.getNativePlace());
        Element Language = Applicant.addElement("Language");
        Language.setText(lcAppnt.getLanguage());
        Element Marriage = Applicant.addElement("Marriage");
        Marriage.setText(lcAppnt.getMarriage());
        Element Stature = Applicant.addElement("Stature");
        if(lcAppnt.getStature()>0){
            Stature.setText(String.valueOf(lcAppnt.getStature()));
        }

        Element Weight = Applicant.addElement("Weight");
        if(lcAppnt.getAvoirdupois()>0){
            Weight.setText(String.valueOf(lcAppnt.getAvoirdupois()));
        }
        Element SmokeFlag = Applicant.addElement("SmokeFlag");
        SmokeFlag.setText(lcAppnt.getSmokeFlag());
        Element Health = Applicant.addElement("Health");
        Health.setText(lcAppnt.getHealth());
        Element Salary = Applicant.addElement("Salary");
        if(lcAppnt.getSalary()>0){
            Salary.setText(String.valueOf(lcAppnt.getSalary()));
        }


        Element Province = Applicant.addElement("Province");
        Element City = Applicant.addElement("City");
        Element Country = Applicant.addElement("Country");

        Element OccupationClass = Applicant.addElement("OccupationClass");
        OccupationClass.setText(lcAppnt.getOccupationType());
        Element OccupationCode = Applicant.addElement("OccupationCode");
        OccupationCode.setText(lcAppnt.getOccupationCode());
        Element SocialInsuFlag = Applicant.addElement("SocialInsuFlag");
        SocialInsuFlag.setText(lcAppnt.getSocialInsuFlag());
        Element AppEmail = Applicant.addElement("AppEmail");
        Element AppTelephone = Applicant.addElement("AppTelephone");
        Element AppMobile = Applicant.addElement("AppMobile");
        Element AppZipCode = Applicant.addElement("AppZipCode");
        Element AppAddress = Applicant.addElement("AppAddress");
        Element AppHomeAddress = Applicant.addElement("AppHomeAddress");
        if(appntaddress!=null){
            Province.setText(appntaddress.getProvince());
            City.setText(appntaddress.getCity());
            Country.setText(appntaddress.getCounty());
            AppEmail.setText(appntaddress.getEMail());
            AppTelephone.setText(appntaddress.getPhone());
            AppMobile.setText(appntaddress.getMobile());
            AppZipCode.setText(appntaddress.getZipCode());
            AppAddress.setText(appntaddress.getAddress());
            AppHomeAddress.setText(appntaddress.getHomeAddress());

        }
        List<LCInsuredPojo> insureds=(List)info.getData(LCInsuredPojo.class.getName());
        LCAddressPojo insuredAddress=addres.get(1);
        //被保人
        Element Insureds = RequestNode.addElement("Insureds");

        if(insureds.size()>0){
            for(LCInsuredPojo insured:insureds){
                Element Insured = Insureds.addElement("Insured");
                Element InsuNo = Insured.addElement("InsuNo");
                InsuNo.setText(insured.getInsuredNo());
                Element InsuRelationToApp = Insured.addElement("InsuRelationToApp");
                InsuRelationToApp.setText(insured.getRelationToAppnt());
                Element InsuRelationToMainInsu = Insured.addElement("InsuRelationToMainInsu");
                InsuRelationToMainInsu.setText(insured.getRelationToMainInsured());
                Element IsMainInsured = Insured.addElement("IsMainInsured");
                IsMainInsured.setText(insured.getIsMainInsured());
                Element InsuName = Insured.addElement("InsuName");
                InsuName.setText(insured.getName());
                Element InsuSex = Insured.addElement("InsuSex");
                InsuSex.setText(insured.getSex());
                Element InsuBirthday = Insured.addElement("InsuBirthday");
                InsuBirthday.setText(insured.getBirthday());
                Element InsuIDType = Insured.addElement("InsuIDType");
                InsuIDType.setText(insured.getIDType());
                Element InsuIDNo = Insured.addElement("InsuIDNo");
                InsuIDNo.setText(insured.getIDNo());
                Element InsuredIdexpDate = Insured.addElement("IdexpDate");
                InsuredIdexpDate.setText(insured.getIdValiDate());
                Element InsuredNativePlace = Insured.addElement("NativePlace");
                InsuredNativePlace.setText(insured.getNativePlace());
                Element InsuredLanguage = Insured.addElement("Language");
               /* InsuredLanguage.setText(insured.get);*/
                Element InsuStature = Insured.addElement("Stature");
                if(insured.getStature()>0){
                    InsuStature.setText(String.valueOf(insured.getStature()));
                }

                Element InsuWeight = Insured.addElement("Weight");
                if(insured.getAvoirdupois()>0){
                    InsuWeight.setText(String.valueOf(insured.getAvoirdupois()));
                }

                Element InsuSmokeFlag = Insured.addElement("SmokeFlag");
                InsuSmokeFlag.setText(insured.getSmokeFlag());
                Element InsuHealth = Insured.addElement("Health");
                InsuHealth.setText(insured.getHealth());
                Element InsuSalary = Insured.addElement("Salary");
                if(insured.getSalary()>0){
                    InsuSalary.setText(String.valueOf(insured.getSalary()));
                }
                Element InsuMarriage = Insured.addElement("Marriage");
                InsuMarriage.setText(insured.getMarriage());
                Element InsuSocialInsuFlag = Insured.addElement("SocialInsuFlag");
                InsuSocialInsuFlag.setText(insured.getSSFlag());

                Element InsuEmail = Insured.addElement("InsuEmail");


                Element InsuTelephone = Insured.addElement("InsuTelephone");
                Element InsuMobile = Insured.addElement("InsuMobile");
                Element InsuZipCode = Insured.addElement("InsuZipCode");
                Element InsuProvince = Insured.addElement("InsuProvince");
                Element InsuCity = Insured.addElement("InsuCity");
                Element InsuCountry = Insured.addElement("InsuCountry");
                Element InsuAddress = Insured.addElement("InsuAddress");
                Element InsuHomeAddress = Insured.addElement("InsuHomeAddress");
                Element InsuOccupationClass = Insured.addElement("OccupationClass");
                InsuOccupationClass.setText(insured.getOccupationType());
                Element InsuOccupationCode = Insured.addElement("OccupationCode");
                InsuOccupationCode.setText(insured.getOccupationCode());
                Element SameIndustryInsuredAmount = Insured.addElement("SameIndustryInsuredAmount");
                /*SameIndustryInsuredAmount.setText();*/
                Element InsuImpartInfo = Insured.addElement("InsuImpartInfo");
                Element PolicyLiabilities = Insured.addElement("PolicyLiabilities");
                if(insuredAddress!=null){
                    InsuEmail.setText(insuredAddress.getEMail());
                    InsuTelephone.setText(insuredAddress.getPhone());
                    InsuMobile.setText(insuredAddress.getMobile());
                    InsuZipCode.setText(insuredAddress.getZipCode());
                    InsuProvince.setText(insuredAddress.getProvince());
                    InsuCity.setText(insuredAddress.getCity());
                    InsuCountry.setText(insuredAddress.getCounty());
                    InsuAddress.setText(insuredAddress.getAddress());
                    InsuHomeAddress.setText(insuredAddress.getHomeAddress());
                }
            }
        }





        /**
         * 受益人信息
         */
        Element Beneficiaries = RequestNode.addElement("Beneficiaries");

        Element RenewalPaymentInfo = RequestNode.addElement("RenewalPaymentInfo");
        Element PayMode = RenewalPaymentInfo.addElement("PayMode");
        PayMode.setText(lccont.getPayMode());
        Element PayBankCode = RenewalPaymentInfo.addElement("PayBankCode");
        PayBankCode.setText(lccont.getBankCode());
        Element PayBankName = RenewalPaymentInfo.addElement("PayBankName");

        Element BankProvinceCode = RenewalPaymentInfo.addElement("BankProvinceCode");
        BankProvinceCode.setText(lccont.getBankProivnce());
        Element BankCityCode = RenewalPaymentInfo.addElement("BankCityCode");
        BankCityCode.setText(lccont.getBankCity());
        Element BankLocationCode = RenewalPaymentInfo.addElement("BankLocationCode");

        Element PayAccountName = RenewalPaymentInfo.addElement("PayAccountName");

        Element PayAcountNo = RenewalPaymentInfo.addElement("PayAcountNo");
        PayAcountNo.setText(lccont.getBankAccNo());
        Element PayVirtualAcountNo = RenewalPaymentInfo.addElement("PayVirtualAcountNo");
        Element PayAccountId = RenewalPaymentInfo.addElement("PayAccountId");


        document.setXMLEncoding("GBK");
        String text = document.asXML();
        return text;
    }

    /**
     * 核保返回结果处理
     *
     * @param tradeInfo
     * @return
     */
    public static JSONObject underWriterPojosTransJson(TradeInfo tradeInfo) {
        LCContPojo lcContPojo = (LCContPojo) tradeInfo.getData(LCContPojo.class.getName());
        GlobalPojo globalPojo = (GlobalPojo) tradeInfo.getData(GlobalPojo.class.getName());
        String errorStr = (String) tradeInfo.getData("return");
        List<String> errorList = tradeInfo.getErrorList();
        JSONObject jsonObject = new JSONObject();

        JSONObject body = new JSONObject();
        jsonObject.put("body", body);
        if (errorList.size() == 0 || errorList == null) {
            //系统正常返回数据未发生异常
            jsonObject.put("supplierNo", globalPojo.getSerialNo());
            body.put("orderNo", globalPojo.getTransNo());
            jsonObject.put("code", 0);
            jsonObject.put("errorCode", "0");
            jsonObject.put("errorMsg", "核保成功！");
            body.put("proposalNo", lcContPojo.getContNo());
            body.put("proposalTime", lcContPojo.getUWDate() + " " + lcContPojo.getUWTime());
        } else {
            jsonObject.put("code", 1);
            jsonObject.put("errorCode", "1");
            jsonObject.put("errorMsg", errorList.get(0));
        }

        //orderNo  proposalNo  proposalTime
        return jsonObject;
    }

    /**
     * 签单请求数据转换
     *
     * @param msg
     * @return
     */
    public static TradeInfo acceptTransPojos(String msg) {
        //将string转成json进行解析
        JSONObject dataJson = JSONObject.parseObject(msg);

        //headers头部
        ShuiDiHead shuiDiHead = new ShuiDiHead();
        JSONObject headers = dataJson.getJSONObject("headers");
        String supplierNo = headers.getString("supplierNo");
        String timeStamp = headers.getString("timeStamp");
        String orderNo = headers.getString("orderNo");
        shuiDiHead.setSupplierNo(supplierNo);
        shuiDiHead.setTimeStamp(timeStamp);
        shuiDiHead.setTransDate(timeStamp.split(" ")[0]);
        shuiDiHead.setTransTime(timeStamp.split(" ")[1]);
        shuiDiHead.setOrderNo(orderNo);


        TradeInfo tradeInfo = new TradeInfo();
        LCContPojo lcContPojo = new LCContPojo();
        GlobalPojo globalPojo = new GlobalPojo();
        globalPojo.setEntrustWay("ShuiDi");
        globalPojo.setTransNo("policyOrder");

        globalPojo.setSerialNo(headers.getString("supplierNo"));

        globalPojo.setTransDate(timeStamp.split(" ")[0]);
        globalPojo.setTransTime(timeStamp.split(" ")[1]);
        tradeInfo.addData(GlobalPojo.class.getName(), globalPojo);

        List<LKTransTracksPojo> lkTransTrackss = new ArrayList<>();
        LKTransTracksPojo lkTransTracks = new LKTransTracksPojo();
        lkTransTracks.setTransDate(timeStamp.split(" ")[0]);
        lkTransTracks.setTransTime(timeStamp.split(" ")[1]);
        lkTransTracks.setTransNo(orderNo);//交易流水号
        lkTransTracks.setTransCode("proposalOrder");
        lkTransTrackss.add(lkTransTracks);
        tradeInfo.addData(LKTransTracksPojo.class.getName(), lkTransTrackss);

        /**
         *  body部分解析封装
         */
        JSONObject body = dataJson.getJSONObject("body");
        // orderNo	水滴订单号
        // proposalNo	投保单号
        lcContPojo.setContNo(body.getString("proposalNo"));
        tradeInfo.addData(LCContPojo.class.getName(), lcContPojo);
        //firstPeriodPremium	首期保费

        //secondPeriodPremium	期交保费

        List<LJTempFeePojo> LJTempFeePojos = new ArrayList<LJTempFeePojo>();
        LJTempFeePojo ljTempFeePojo = new LJTempFeePojo();
        // payTime	支付时间
        ljTempFeePojo.setPayDate(body.getString("payTime"));
        // payType	支付方式

        LJTempFeePojos.add(ljTempFeePojo);
        tradeInfo.addData(LJTempFeePojo.class.getName(), LJTempFeePojos);


        // bankCode	支付时使用银行卡的银行编码  N
        //bankName	支付时使用银行卡的银行名称  N
        // bankCardNo	支付时使用银行卡的卡号  N
        // accountName	支付时使用银行卡的开户人姓名 N
        //payAccount	支付账户  N
        return tradeInfo;
    }

    /**
     * 签单返回数据转换
     *
     * @param tradeInfo
     * @return
     */
    public static JSONObject acceptPojosTransJson(TradeInfo tradeInfo) {
        LCContPojo lcContPojo = (LCContPojo) tradeInfo.getData(LCContPojo.class.getName());
        GlobalPojo globalPojo = (GlobalPojo) tradeInfo.getData(GlobalPojo.class.getName());
        String errorStr = (String) tradeInfo.getData("return");
        List<String> errorList = tradeInfo.getErrorList();
        JSONObject jsonObject = new JSONObject();

        JSONObject body = new JSONObject();
        jsonObject.put("body", body);
        if (errorList.size() == 0 || errorList == null) {
            //系统正常返回数据未发生异常
            jsonObject.put("supplierNo", globalPojo.getSerialNo());
            body.put("orderNo", globalPojo.getTransNo());
            jsonObject.put("code", 0);
            jsonObject.put("errorCode", "0");
            jsonObject.put("errorMsg", "签单成功！");
            body.put("proposalNo", lcContPojo.getContNo());
            body.put("proposalTime", lcContPojo.getUWDate() + " " + lcContPojo.getUWTime());
        } else {
            jsonObject.put("code", 1);
            jsonObject.put("errorCode", "1");
            jsonObject.put("errorMsg", errorList.get(0));
        }

        //orderNo  proposalNo  proposalTime
        return jsonObject;
    }


    /**
     * 根据证件有效期类型和截止时间确定证件有效期
     */
    private static String getIdValiDate(String idValidType, String idValidDay) {
        if ("2".equals(idValidType)) {
            return "9999-01-01";
        } else return idValidDay;

    }
}
