package com.sinosoft.cloud.access.entity;

import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.StrTool;

/**
 * @Author:lizhen
 * @Description:
 * @Date:Created in 10:21 2018/4/11
 * @Modified By:
 */
public class LKNRTBizTrans {
    private long LKNRTBizID;
    private String ShardingID;
    private String TransCode;
    private String BankCode;
    private String TranDate;
    private String ZoneNo;
    private String BankNode;
    private String TellerNo;
    private String TransNo;
    private String ProposalNo;
    private String BanksaleChnl;
    private String UwType;
    private String AppntName;
    private String AppntidType;
    private String AppntidNo;
    private String AppntaccNo;
    private String BizStatus;
    private String UwStatus;
    private String RespDate;
    private String RespTime;
    private String MakeDate;
    private String MakeTime;
    private String ModifyDate;
    private String ModifyTime;
    private String Bak1;
    private String Bak2;
    public static final int FIELDNUM = 26;
    private FDate fDate = new FDate();

    public LKNRTBizTrans() {
    }

    public long getLKNRTBizID() {
        return this.LKNRTBizID;
    }

    public void setLKNRTBizID(long aLKNRTBizID) {
        this.LKNRTBizID = aLKNRTBizID;
    }

    public void setLKNRTBizID(String aLKNRTBizID) {
        if (aLKNRTBizID != null && !aLKNRTBizID.equals("")) {
            this.LKNRTBizID = (new Long(aLKNRTBizID)).longValue();
        }

    }

    public String getShardingID() {
        return this.ShardingID;
    }

    public void setShardingID(String aShardingID) {
        this.ShardingID = aShardingID;
    }

    public String getTransCode() {
        return this.TransCode;
    }

    public void setTransCode(String aTransCode) {
        this.TransCode = aTransCode;
    }

    public String getBankCode() {
        return this.BankCode;
    }

    public void setBankCode(String aBankCode) {
        this.BankCode = aBankCode;
    }

    public String getTranDate() {
        return this.TranDate;
    }

    public void setTranDate(String aTranDate) {
        this.TranDate = aTranDate;
    }

    public String getZoneNo() {
        return this.ZoneNo;
    }

    public void setZoneNo(String aZoneNo) {
        this.ZoneNo = aZoneNo;
    }

    public String getBankNode() {
        return this.BankNode;
    }

    public void setBankNode(String aBankNode) {
        this.BankNode = aBankNode;
    }

    public String getTellerNo() {
        return this.TellerNo;
    }

    public void setTellerNo(String aTellerNo) {
        this.TellerNo = aTellerNo;
    }

    public String getTransNo() {
        return this.TransNo;
    }

    public void setTransNo(String aTransNo) {
        this.TransNo = aTransNo;
    }

    public String getProposalNo() {
        return this.ProposalNo;
    }

    public void setProposalNo(String aProposalNo) {
        this.ProposalNo = aProposalNo;
    }

    public String getBanksaleChnl() {
        return this.BanksaleChnl;
    }

    public void setBanksaleChnl(String aBanksaleChnl) {
        this.BanksaleChnl = aBanksaleChnl;
    }

    public String getUwType() {
        return this.UwType;
    }

    public void setUwType(String aUwType) {
        this.UwType = aUwType;
    }

    public String getAppntName() {
        return this.AppntName;
    }

    public void setAppntName(String aAppntName) {
        this.AppntName = aAppntName;
    }

    public String getAppntidType() {
        return this.AppntidType;
    }

    public void setAppntidType(String aAppntidType) {
        this.AppntidType = aAppntidType;
    }

    public String getAppntidNo() {
        return this.AppntidNo;
    }

    public void setAppntidNo(String aAppntidNo) {
        this.AppntidNo = aAppntidNo;
    }

    public String getAppntaccNo() {
        return this.AppntaccNo;
    }

    public void setAppntaccNo(String aAppntaccNo) {
        this.AppntaccNo = aAppntaccNo;
    }

    public String getBizStatus() {
        return this.BizStatus;
    }

    public void setBizStatus(String aBizStatus) {
        this.BizStatus = aBizStatus;
    }

    public String getUwStatus() {
        return this.UwStatus;
    }

    public void setUwStatus(String aUwStatus) {
        this.UwStatus = aUwStatus;
    }

    public String getRespDate() {
        return this.RespDate;
    }

    public void setRespDate(String aRespDate) {
        this.RespDate = aRespDate;
    }

    public String getRespTime() {
        return this.RespTime;
    }

    public void setRespTime(String aRespTime) {
        this.RespTime = aRespTime;
    }

    public String getMakeDate() {
        return this.MakeDate;
    }

    public void setMakeDate(String aMakeDate) {
        this.MakeDate = aMakeDate;
    }

    public String getMakeTime() {
        return this.MakeTime;
    }

    public void setMakeTime(String aMakeTime) {
        this.MakeTime = aMakeTime;
    }

    public String getModifyDate() {
        return this.ModifyDate;
    }

    public void setModifyDate(String aModifyDate) {
        this.ModifyDate = aModifyDate;
    }

    public String getModifyTime() {
        return this.ModifyTime;
    }

    public void setModifyTime(String aModifyTime) {
        this.ModifyTime = aModifyTime;
    }

    public String getBak1() {
        return this.Bak1;
    }

    public void setBak1(String aBak1) {
        this.Bak1 = aBak1;
    }

    public String getBak2() {
        return this.Bak2;
    }

    public void setBak2(String aBak2) {
        this.Bak2 = aBak2;
    }

    public int getFieldCount() {
        return 26;
    }

    public int getFieldIndex(String strFieldName) {
        return strFieldName.equals("LKNRTBizID") ? 0 : (strFieldName.equals("ShardingID") ? 1 : (strFieldName.equals("TransCode") ? 2 : (strFieldName.equals("BankCode") ? 3 : (strFieldName.equals("TranDate") ? 4 : (strFieldName.equals("ZoneNo") ? 5 : (strFieldName.equals("BankNode") ? 6 : (strFieldName.equals("TellerNo") ? 7 : (strFieldName.equals("TransNo") ? 8 : (strFieldName.equals("ProposalNo") ? 9 : (strFieldName.equals("BanksaleChnl") ? 10 : (strFieldName.equals("UwType") ? 11 : (strFieldName.equals("AppntName") ? 12 : (strFieldName.equals("AppntidType") ? 13 : (strFieldName.equals("AppntidNo") ? 14 : (strFieldName.equals("AppntaccNo") ? 15 : (strFieldName.equals("BizStatus") ? 16 : (strFieldName.equals("UwStatus") ? 17 : (strFieldName.equals("RespDate") ? 18 : (strFieldName.equals("RespTime") ? 19 : (strFieldName.equals("MakeDate") ? 20 : (strFieldName.equals("MakeTime") ? 21 : (strFieldName.equals("ModifyDate") ? 22 : (strFieldName.equals("ModifyTime") ? 23 : (strFieldName.equals("Bak1") ? 24 : (strFieldName.equals("Bak2") ? 25 : -1)))))))))))))))))))))))));
    }

    public String getFieldName(int nFieldIndex) {
        String strFieldName = "";
        switch (nFieldIndex) {
            case 0:
                strFieldName = "LKNRTBizID";
                break;
            case 1:
                strFieldName = "ShardingID";
                break;
            case 2:
                strFieldName = "TransCode";
                break;
            case 3:
                strFieldName = "BankCode";
                break;
            case 4:
                strFieldName = "TranDate";
                break;
            case 5:
                strFieldName = "ZoneNo";
                break;
            case 6:
                strFieldName = "BankNode";
                break;
            case 7:
                strFieldName = "TellerNo";
                break;
            case 8:
                strFieldName = "TransNo";
                break;
            case 9:
                strFieldName = "ProposalNo";
                break;
            case 10:
                strFieldName = "BanksaleChnl";
                break;
            case 11:
                strFieldName = "UwType";
                break;
            case 12:
                strFieldName = "AppntName";
                break;
            case 13:
                strFieldName = "AppntidType";
                break;
            case 14:
                strFieldName = "AppntidNo";
                break;
            case 15:
                strFieldName = "AppntaccNo";
                break;
            case 16:
                strFieldName = "BizStatus";
                break;
            case 17:
                strFieldName = "UwStatus";
                break;
            case 18:
                strFieldName = "RespDate";
                break;
            case 19:
                strFieldName = "RespTime";
                break;
            case 20:
                strFieldName = "MakeDate";
                break;
            case 21:
                strFieldName = "MakeTime";
                break;
            case 22:
                strFieldName = "ModifyDate";
                break;
            case 23:
                strFieldName = "ModifyTime";
                break;
            case 24:
                strFieldName = "Bak1";
                break;
            case 25:
                strFieldName = "Bak2";
                break;
            default:
                strFieldName = "";
        }

        return strFieldName;
    }

    public int getFieldType(String strFieldName) {
        String var2 = strFieldName.toUpperCase();
        byte var3 = -1;
        switch (var2.hashCode()) {
            case -2087255365:
                if (var2.equals("APPNTACCNO")) {
                    var3 = 15;
                }
                break;
            case -1778552612:
                if (var2.equals("UWTYPE")) {
                    var3 = 11;
                }
                break;
            case -1642993043:
                if (var2.equals("ZONENO")) {
                    var3 = 5;
                }
                break;
            case -1322448033:
                if (var2.equals("SHARDINGID")) {
                    var3 = 1;
                }
                break;
            case -1076025815:
                if (var2.equals("BANKCODE")) {
                    var3 = 3;
                }
                break;
            case -1075698114:
                if (var2.equals("BANKNODE")) {
                    var3 = 6;
                }
                break;
            case -571435425:
                if (var2.equals("TELLERNO")) {
                    var3 = 7;
                }
                break;
            case -455795595:
                if (var2.equals("TRANSCODE")) {
                    var3 = 2;
                }
                break;
            case -349076919:
                if (var2.equals("TRANSNO")) {
                    var3 = 8;
                }
                break;
            case -154308557:
                if (var2.equals("PROPOSALNO")) {
                    var3 = 9;
                }
                break;
            case -49933028:
                if (var2.equals("APPNTIDTYPE")) {
                    var3 = 13;
                }
                break;
            case 2031045:
                if (var2.equals("BAK1")) {
                    var3 = 24;
                }
                break;
            case 2031046:
                if (var2.equals("BAK2")) {
                    var3 = 25;
                }
                break;
            case 69993125:
                if (var2.equals("BIZSTATUS")) {
                    var3 = 16;
                }
                break;
            case 174247188:
                if (var2.equals("UWSTATUS")) {
                    var3 = 17;
                }
                break;
            case 441963390:
                if (var2.equals("RESPDATE")) {
                    var3 = 18;
                }
                break;
            case 442447517:
                if (var2.equals("RESPTIME")) {
                    var3 = 19;
                }
                break;
            case 558919357:
                if (var2.equals("LKNRTBIZID")) {
                    var3 = 0;
                }
                break;
            case 764192803:
                if (var2.equals("APPNTIDNO")) {
                    var3 = 14;
                }
                break;
            case 764338834:
                if (var2.equals("APPNTNAME")) {
                    var3 = 12;
                }
                break;
            case 823606940:
                if (var2.equals("MAKEDATE")) {
                    var3 = 20;
                }
                break;
            case 824091067:
                if (var2.equals("MAKETIME")) {
                    var3 = 21;
                }
                break;
            case 1247661030:
                if (var2.equals("BANKSALECHNL")) {
                    var3 = 10;
                }
                break;
            case 1696950120:
                if (var2.equals("MODIFYDATE")) {
                    var3 = 22;
                }
                break;
            case 1697434247:
                if (var2.equals("MODIFYTIME")) {
                    var3 = 23;
                }
                break;
            case 2063058265:
                if (var2.equals("TRANDATE")) {
                    var3 = 4;
                }
        }

        switch (var3) {
            case 0:
                return 7;
            case 1:
                return 0;
            case 2:
                return 0;
            case 3:
                return 0;
            case 4:
                return 0;
            case 5:
                return 0;
            case 6:
                return 0;
            case 7:
                return 0;
            case 8:
                return 0;
            case 9:
                return 0;
            case 10:
                return 0;
            case 11:
                return 0;
            case 12:
                return 0;
            case 13:
                return 0;
            case 14:
                return 0;
            case 15:
                return 0;
            case 16:
                return 0;
            case 17:
                return 0;
            case 18:
                return 0;
            case 19:
                return 0;
            case 20:
                return 0;
            case 21:
                return 0;
            case 22:
                return 0;
            case 23:
                return 0;
            case 24:
                return 0;
            case 25:
                return 0;
            default:
                return -1;
        }
    }

    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return 7;
            case 1:
                return 0;
            case 2:
                return 0;
            case 3:
                return 0;
            case 4:
                return 0;
            case 5:
                return 0;
            case 6:
                return 0;
            case 7:
                return 0;
            case 8:
                return 0;
            case 9:
                return 0;
            case 10:
                return 0;
            case 11:
                return 0;
            case 12:
                return 0;
            case 13:
                return 0;
            case 14:
                return 0;
            case 15:
                return 0;
            case 16:
                return 0;
            case 17:
                return 0;
            case 18:
                return 0;
            case 19:
                return 0;
            case 20:
                return 0;
            case 21:
                return 0;
            case 22:
                return 0;
            case 23:
                return 0;
            case 24:
                return 0;
            case 25:
                return 0;
            default:
                return -1;
        }
    }

    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("LKNRTBizID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.LKNRTBizID));
        }

        if (FCode.equalsIgnoreCase("ShardingID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.ShardingID));
        }

        if (FCode.equalsIgnoreCase("TransCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.TransCode));
        }

        if (FCode.equalsIgnoreCase("BankCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.BankCode));
        }

        if (FCode.equalsIgnoreCase("TranDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.TranDate));
        }

        if (FCode.equalsIgnoreCase("ZoneNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.ZoneNo));
        }

        if (FCode.equalsIgnoreCase("BankNode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.BankNode));
        }

        if (FCode.equalsIgnoreCase("TellerNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.TellerNo));
        }

        if (FCode.equalsIgnoreCase("TransNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.TransNo));
        }

        if (FCode.equalsIgnoreCase("ProposalNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.ProposalNo));
        }

        if (FCode.equalsIgnoreCase("BanksaleChnl")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.BanksaleChnl));
        }

        if (FCode.equalsIgnoreCase("UwType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.UwType));
        }

        if (FCode.equalsIgnoreCase("AppntName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.AppntName));
        }

        if (FCode.equalsIgnoreCase("AppntidType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.AppntidType));
        }

        if (FCode.equalsIgnoreCase("AppntidNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.AppntidNo));
        }

        if (FCode.equalsIgnoreCase("AppntaccNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.AppntaccNo));
        }

        if (FCode.equalsIgnoreCase("BizStatus")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.BizStatus));
        }

        if (FCode.equalsIgnoreCase("UwStatus")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.UwStatus));
        }

        if (FCode.equalsIgnoreCase("RespDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.RespDate));
        }

        if (FCode.equalsIgnoreCase("RespTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.RespTime));
        }

        if (FCode.equalsIgnoreCase("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.MakeDate));
        }

        if (FCode.equalsIgnoreCase("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.MakeTime));
        }

        if (FCode.equalsIgnoreCase("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.ModifyDate));
        }

        if (FCode.equalsIgnoreCase("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.ModifyTime));
        }

        if (FCode.equalsIgnoreCase("Bak1")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.Bak1));
        }

        if (FCode.equalsIgnoreCase("Bak2")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.Bak2));
        }

        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }

    public String getV(int nFieldIndex) {
        String strFieldValue = "";
        switch (nFieldIndex) {
            case 0:
                strFieldValue = String.valueOf(this.LKNRTBizID);
                break;
            case 1:
                strFieldValue = String.valueOf(this.ShardingID);
                break;
            case 2:
                strFieldValue = String.valueOf(this.TransCode);
                break;
            case 3:
                strFieldValue = String.valueOf(this.BankCode);
                break;
            case 4:
                strFieldValue = String.valueOf(this.TranDate);
                break;
            case 5:
                strFieldValue = String.valueOf(this.ZoneNo);
                break;
            case 6:
                strFieldValue = String.valueOf(this.BankNode);
                break;
            case 7:
                strFieldValue = String.valueOf(this.TellerNo);
                break;
            case 8:
                strFieldValue = String.valueOf(this.TransNo);
                break;
            case 9:
                strFieldValue = String.valueOf(this.ProposalNo);
                break;
            case 10:
                strFieldValue = String.valueOf(this.BanksaleChnl);
                break;
            case 11:
                strFieldValue = String.valueOf(this.UwType);
                break;
            case 12:
                strFieldValue = String.valueOf(this.AppntName);
                break;
            case 13:
                strFieldValue = String.valueOf(this.AppntidType);
                break;
            case 14:
                strFieldValue = String.valueOf(this.AppntidNo);
                break;
            case 15:
                strFieldValue = String.valueOf(this.AppntaccNo);
                break;
            case 16:
                strFieldValue = String.valueOf(this.BizStatus);
                break;
            case 17:
                strFieldValue = String.valueOf(this.UwStatus);
                break;
            case 18:
                strFieldValue = String.valueOf(this.RespDate);
                break;
            case 19:
                strFieldValue = String.valueOf(this.RespTime);
                break;
            case 20:
                strFieldValue = String.valueOf(this.MakeDate);
                break;
            case 21:
                strFieldValue = String.valueOf(this.MakeTime);
                break;
            case 22:
                strFieldValue = String.valueOf(this.ModifyDate);
                break;
            case 23:
                strFieldValue = String.valueOf(this.ModifyTime);
                break;
            case 24:
                strFieldValue = String.valueOf(this.Bak1);
                break;
            case 25:
                strFieldValue = String.valueOf(this.Bak2);
                break;
            default:
                strFieldValue = "";
        }

        if (strFieldValue.equals("")) {
            strFieldValue = "null";
        }

        return strFieldValue;
    }

    public boolean setV(String FCode, String FValue) {
        if (StrTool.cTrim(FCode).equals("")) {
            return false;
        } else {
            if (FCode.equalsIgnoreCase("LKNRTBizID") && FValue != null && !FValue.equals("")) {
                this.LKNRTBizID = (new Long(FValue)).longValue();
            }

            if (FCode.equalsIgnoreCase("ShardingID")) {
                if (FValue != null && !FValue.equals("")) {
                    this.ShardingID = FValue.trim();
                } else {
                    this.ShardingID = null;
                }
            }

            if (FCode.equalsIgnoreCase("TransCode")) {
                if (FValue != null && !FValue.equals("")) {
                    this.TransCode = FValue.trim();
                } else {
                    this.TransCode = null;
                }
            }

            if (FCode.equalsIgnoreCase("BankCode")) {
                if (FValue != null && !FValue.equals("")) {
                    this.BankCode = FValue.trim();
                } else {
                    this.BankCode = null;
                }
            }

            if (FCode.equalsIgnoreCase("TranDate")) {
                if (FValue != null && !FValue.equals("")) {
                    this.TranDate = FValue.trim();
                } else {
                    this.TranDate = null;
                }
            }

            if (FCode.equalsIgnoreCase("ZoneNo")) {
                if (FValue != null && !FValue.equals("")) {
                    this.ZoneNo = FValue.trim();
                } else {
                    this.ZoneNo = null;
                }
            }

            if (FCode.equalsIgnoreCase("BankNode")) {
                if (FValue != null && !FValue.equals("")) {
                    this.BankNode = FValue.trim();
                } else {
                    this.BankNode = null;
                }
            }

            if (FCode.equalsIgnoreCase("TellerNo")) {
                if (FValue != null && !FValue.equals("")) {
                    this.TellerNo = FValue.trim();
                } else {
                    this.TellerNo = null;
                }
            }

            if (FCode.equalsIgnoreCase("TransNo")) {
                if (FValue != null && !FValue.equals("")) {
                    this.TransNo = FValue.trim();
                } else {
                    this.TransNo = null;
                }
            }

            if (FCode.equalsIgnoreCase("ProposalNo")) {
                if (FValue != null && !FValue.equals("")) {
                    this.ProposalNo = FValue.trim();
                } else {
                    this.ProposalNo = null;
                }
            }

            if (FCode.equalsIgnoreCase("BanksaleChnl")) {
                if (FValue != null && !FValue.equals("")) {
                    this.BanksaleChnl = FValue.trim();
                } else {
                    this.BanksaleChnl = null;
                }
            }

            if (FCode.equalsIgnoreCase("UwType")) {
                if (FValue != null && !FValue.equals("")) {
                    this.UwType = FValue.trim();
                } else {
                    this.UwType = null;
                }
            }

            if (FCode.equalsIgnoreCase("AppntName")) {
                if (FValue != null && !FValue.equals("")) {
                    this.AppntName = FValue.trim();
                } else {
                    this.AppntName = null;
                }
            }

            if (FCode.equalsIgnoreCase("AppntidType")) {
                if (FValue != null && !FValue.equals("")) {
                    this.AppntidType = FValue.trim();
                } else {
                    this.AppntidType = null;
                }
            }

            if (FCode.equalsIgnoreCase("AppntidNo")) {
                if (FValue != null && !FValue.equals("")) {
                    this.AppntidNo = FValue.trim();
                } else {
                    this.AppntidNo = null;
                }
            }

            if (FCode.equalsIgnoreCase("AppntaccNo")) {
                if (FValue != null && !FValue.equals("")) {
                    this.AppntaccNo = FValue.trim();
                } else {
                    this.AppntaccNo = null;
                }
            }

            if (FCode.equalsIgnoreCase("BizStatus")) {
                if (FValue != null && !FValue.equals("")) {
                    this.BizStatus = FValue.trim();
                } else {
                    this.BizStatus = null;
                }
            }

            if (FCode.equalsIgnoreCase("UwStatus")) {
                if (FValue != null && !FValue.equals("")) {
                    this.UwStatus = FValue.trim();
                } else {
                    this.UwStatus = null;
                }
            }

            if (FCode.equalsIgnoreCase("RespDate")) {
                if (FValue != null && !FValue.equals("")) {
                    this.RespDate = FValue.trim();
                } else {
                    this.RespDate = null;
                }
            }

            if (FCode.equalsIgnoreCase("RespTime")) {
                if (FValue != null && !FValue.equals("")) {
                    this.RespTime = FValue.trim();
                } else {
                    this.RespTime = null;
                }
            }

            if (FCode.equalsIgnoreCase("MakeDate")) {
                if (FValue != null && !FValue.equals("")) {
                    this.MakeDate = FValue.trim();
                } else {
                    this.MakeDate = null;
                }
            }

            if (FCode.equalsIgnoreCase("MakeTime")) {
                if (FValue != null && !FValue.equals("")) {
                    this.MakeTime = FValue.trim();
                } else {
                    this.MakeTime = null;
                }
            }

            if (FCode.equalsIgnoreCase("ModifyDate")) {
                if (FValue != null && !FValue.equals("")) {
                    this.ModifyDate = FValue.trim();
                } else {
                    this.ModifyDate = null;
                }
            }

            if (FCode.equalsIgnoreCase("ModifyTime")) {
                if (FValue != null && !FValue.equals("")) {
                    this.ModifyTime = FValue.trim();
                } else {
                    this.ModifyTime = null;
                }
            }

            if (FCode.equalsIgnoreCase("Bak1")) {
                if (FValue != null && !FValue.equals("")) {
                    this.Bak1 = FValue.trim();
                } else {
                    this.Bak1 = null;
                }
            }

            if (FCode.equalsIgnoreCase("Bak2")) {
                if (FValue != null && !FValue.equals("")) {
                    this.Bak2 = FValue.trim();
                } else {
                    this.Bak2 = null;
                }
            }

            return true;
        }
    }

    @Override
    public String toString() {
        return "LKNRTBizTransPojo [LKNRTBizID=" + this.LKNRTBizID + ", ShardingID=" + this.ShardingID + ", TransCode=" + this.TransCode + ", BankCode=" + this.BankCode + ", TranDate=" + this.TranDate + ", ZoneNo=" + this.ZoneNo + ", BankNode=" + this.BankNode + ", TellerNo=" + this.TellerNo + ", TransNo=" + this.TransNo + ", ProposalNo=" + this.ProposalNo + ", BanksaleChnl=" + this.BanksaleChnl + ", UwType=" + this.UwType + ", AppntName=" + this.AppntName + ", AppntidType=" + this.AppntidType + ", AppntidNo=" + this.AppntidNo + ", AppntaccNo=" + this.AppntaccNo + ", BizStatus=" + this.BizStatus + ", UwStatus=" + this.UwStatus + ", RespDate=" + this.RespDate + ", RespTime=" + this.RespTime + ", MakeDate=" + this.MakeDate + ", MakeTime=" + this.MakeTime + ", ModifyDate=" + this.ModifyDate + ", ModifyTime=" + this.ModifyTime + ", Bak1=" + this.Bak1 + ", Bak2=" + this.Bak2 + "]";
    }

}