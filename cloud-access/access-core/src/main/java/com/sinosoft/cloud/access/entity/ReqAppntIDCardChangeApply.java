package com.sinosoft.cloud.access.entity;
/**
 * 投保人身份证有效期变更申请POS057
 */
public class ReqAppntIDCardChangeApply {
    //保全项目编码
    private String EdorType;
    //保单号
    private String ContNo;
    //投保人姓名
    private String Name;
    //性别
    private String Sex;
    //生日
    private String Birthday;
    //证件类型
    private String IdType;
    //证件号码
    private String IdNo;
    //证件有效期
    private String ValiDate;
    //申请人信息
    private EdorAppInfo EdorAppInfo;
    //领取人
    private PersonInfo PersonInfo;
    //xxx
    private BankInfo BankInfo;
    //收付费方式
    private String PayMode;

    public String getEdorType() {
        return EdorType;
    }

    public void setEdorType(String edorType) {
        EdorType = edorType;
    }

    public String getContNo() {
        return ContNo;
    }

    public void setContNo(String contNo) {
        ContNo = contNo;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getSex() {
        return Sex;
    }

    public void setSex(String sex) {
        Sex = sex;
    }

    public String getBirthday() {
        return Birthday;
    }

    public void setBirthday(String birthday) {
        Birthday = birthday;
    }

    public String getIdType() {
        return IdType;
    }

    public void setIdType(String idType) {
        IdType = idType;
    }

    public String getIdNo() {
        return IdNo;
    }

    public void setIdNo(String idNo) {
        IdNo = idNo;
    }

    public String getValiDate() {
        return ValiDate;
    }

    public void setValiDate(String valiDate) {
        ValiDate = valiDate;
    }

    public com.sinosoft.cloud.access.entity.EdorAppInfo getEdorAppInfo() {
        return EdorAppInfo;
    }

    public void setEdorAppInfo(com.sinosoft.cloud.access.entity.EdorAppInfo edorAppInfo) {
        EdorAppInfo = edorAppInfo;
    }

    public com.sinosoft.cloud.access.entity.PersonInfo getPersonInfo() {
        return PersonInfo;
    }

    public void setPersonInfo(com.sinosoft.cloud.access.entity.PersonInfo personInfo) {
        PersonInfo = personInfo;
    }

    public com.sinosoft.cloud.access.entity.BankInfo getBankInfo() {
        return BankInfo;
    }

    public void setBankInfo(com.sinosoft.cloud.access.entity.BankInfo bankInfo) {
        BankInfo = bankInfo;
    }

    public String getPayMode() {
        return PayMode;
    }

    public void setPayMode(String payMode) {
        PayMode = payMode;
    }

    @Override
    public String toString() {
        return "ReqAppntIDCardChangeApply{" +
                "EdorType='" + EdorType + '\'' +
                ", ContNo='" + ContNo + '\'' +
                ", Name='" + Name + '\'' +
                ", Sex='" + Sex + '\'' +
                ", Birthday='" + Birthday + '\'' +
                ", IdType='" + IdType + '\'' +
                ", IdNo='" + IdNo + '\'' +
                ", ValiDate='" + ValiDate + '\'' +
                ", EdorAppInfo=" + EdorAppInfo +
                ", PersonInfo=" + PersonInfo +
                ", BankInfo=" + BankInfo +
                ", PayMode='" + PayMode + '\'' +
                '}';
    }
}
