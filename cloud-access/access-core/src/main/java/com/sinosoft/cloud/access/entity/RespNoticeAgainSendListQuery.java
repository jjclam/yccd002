package com.sinosoft.cloud.access.entity;

/**
 * 4008查询保单列表POS060
 */
public class RespNoticeAgainSendListQuery {

    //文本信息
    private TextInfos TextInfos;

    public com.sinosoft.cloud.access.entity.TextInfos getTextInfos() {
        return TextInfos;
    }

    public void setTextInfos(com.sinosoft.cloud.access.entity.TextInfos textInfos) {
        TextInfos = textInfos;
    }
    @Override
    public String toString() {
        return "RespNoticeAgainSendListQuery{" +
                "TextInfos=" + TextInfos +
                '}';
    }

}
