package com.sinosoft.cloud.access.entity;

import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.List;

/**
 * @Author:lizhen
 * @Description:
 * @Date:Created in 17:00 2018/11/16
 * @Modified By:
 */
public class PhoneInfos {
    //联系电话个数
    private String Count;
    //联系电话内容节点
    @XStreamImplicit(itemFieldName="PhoneInfo")
    private List<PhoneInfo> PhoneInfo;

    public List<com.sinosoft.cloud.access.entity.PhoneInfo> getPhoneInfo() {
        return PhoneInfo;
    }

    public void setPhoneInfo(List<com.sinosoft.cloud.access.entity.PhoneInfo> phoneInfo) {
        PhoneInfo = phoneInfo;
    }

    public String getCount() {
        return Count;
    }

    public void setCount(String count) {
        Count = count;
    }

    @Override
    public String toString() {
        return "PhoneInfos{" +
                "Count='" + Count + '\'' +
                ", PhoneInfo=" + PhoneInfo +
                '}';
    }
}