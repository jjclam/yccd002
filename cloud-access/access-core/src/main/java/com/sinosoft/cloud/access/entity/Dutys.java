package com.sinosoft.cloud.access.entity;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.List;


public class Dutys {
    @XStreamImplicit(itemFieldName="Duty")
    private List<Duty> dutyList;

    public List<Duty> getDutyList() {
        return dutyList;
    }

    public void setDutyList(List<Duty> dutyList) {
        this.dutyList = dutyList;
    }

    @Override
    public String toString() {
        return "Dutys{" +
                "dutyList=" + dutyList +
                '}';
    }
}
