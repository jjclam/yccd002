package com.sinosoft.cloud.access.entity;

import com.sinosoft.lis.pos.entity.PolicyQueryInfosPojo;
/**
 * 万能账户价值列表查询POS032.
 */
public class RespAccountListQuery {
    //保全项目编码
    private String EdorType;
    private PolicyQueryInfos PolicyQueryInfos;

    public String getEdorType() {
        return EdorType;
    }

    public void setEdorType(String edorType) {
        EdorType = edorType;
    }

    public com.sinosoft.cloud.access.entity.PolicyQueryInfos getPolicyQueryInfos() {
        return PolicyQueryInfos;
    }

    public void setPolicyQueryInfos(com.sinosoft.cloud.access.entity.PolicyQueryInfos policyQueryInfos) {
        PolicyQueryInfos = policyQueryInfos;
    }
    @Override
    public String toString() {
        return "RespAccountListQuery{" +
                "EdorType='" + EdorType + '\'' +
                ", PolicyQueryInfos=" + PolicyQueryInfos +
                '}';
    }
}
