package com.sinosoft.cloud.access.net;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import okhttp3.*;


/**
 * @Author: 崔广东
 * @Date: 2018/3/28 11:50
 * @Description: http客户端
 */
public class HttpClient {

    /**
     * 传送的数据类型
     */
    public static final MediaType APPLICATION_JSON = MediaType.parse("application/json;charset=utf-8");
    public static final MediaType APPLICATION_XML = MediaType.parse("aapplication/xml;charset=utf-8");
    public static final MediaType MULTIPART_FORM_DATA = MediaType.parse("multipart/form-data;charset=utf-8");
    public static final MediaType TEXT_HTML = MediaType.parse("text/html;charset=utf-8");
    public static final MediaType TEXT_PLAIN = MediaType.parse("text/plain;charset=utf-8");
    public static final MediaType TEXT_XML = MediaType.parse("text/xml;charset=utf-8");

    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    /**
     * 默认参数
     */
    private int connectTimeOut = 2;
    private int readTimeOut = 10;
    private MediaType contentType = TEXT_PLAIN;


    /**
     * http post方式远程调用
     *
     * @param url         http请求路径
     * @param bodyContent http请求body内容
     * @return
     * @throws IOException
     */
    public String postSend(String url, String bodyContent) throws IOException {

        if (StringUtils.isEmpty(url) || StringUtils.isEmpty(bodyContent)) {
//            String error = "请求路径或者请求内容为空";
//            logger.error(error);
//            return null;
            throw new RuntimeException("请求路径或者请求内容为空");
        }

        //设置超时时间
        OkHttpClient.Builder builder = new OkHttpClient.Builder().connectTimeout(connectTimeOut, TimeUnit.SECONDS).readTimeout(readTimeOut, TimeUnit.SECONDS);
        //创建http客户端
        OkHttpClient httpClient = builder.build();
        RequestBody requestBody = RequestBody.create(contentType, bodyContent);
        Request request = new Request.Builder()
                .url(url)
                .post(requestBody)
                .build();
        Response response = httpClient.newCall(request).execute();
        return response.body().string();
    }

    /**
     * http get请求（低配版）
     * @param url 请求路径
     * @return
     * @throws IOException
     */
    public String getSend(String url) throws IOException {
        OkHttpClient httpClient = new OkHttpClient();
        Request request = new Request.Builder()
                .url(url)
                .build();
        Response response = httpClient.newCall(request).execute();
        return response.body().string();
    }

    /**
     * 设置连接超时时间
     *
     * @param connectTimeOut 连接超时时间，秒为单位
     * @return
     */
    public HttpClient setConnectTimeOut(int connectTimeOut) {
        this.connectTimeOut = connectTimeOut;
        return this;
    }

    /**
     * 设置读取超时时间
     *
     * @param readTimeOut 连接读取时间，秒为单位
     * @return
     */
    public HttpClient setReadTimeOut(int readTimeOut) {
        this.readTimeOut = readTimeOut;
        return this;
    }

    /**
     * 设置http请求的传输格式
     *
     * @param type 传输的格式
     * @return
     */
    public HttpClient setContentType(MediaType type) {
        this.contentType = type;
        return this;
    }
}
