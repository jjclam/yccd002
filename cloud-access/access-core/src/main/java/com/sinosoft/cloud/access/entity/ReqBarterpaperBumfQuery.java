package com.sinosoft.cloud.access.entity;

/**
 * @program: abc-cloud-access
 * @description: 换发纸质保单查询POS094.
 * @author: BaoYongmeng
 * @create: 2018-12-24 16:59
 **/
public class ReqBarterpaperBumfQuery {

    //保单号
    private String ContNo;
    //保单查询类型
    private String ELecType;
    //客户姓名
    private String CustomerName;
    //证件类型
    private String CustomerIdType;
    //证件号码
    private String CustomerIdNo;

    public String getContNo() {
        return ContNo;
    }

    public void setContNo(String contNo) {
        ContNo = contNo;
    }

    public String getELecType() {
        return ELecType;
    }

    public void setELecType(String ELecType) {
        this.ELecType = ELecType;
    }

    public String getCustomerName() {
        return CustomerName;
    }

    public void setCustomerName(String customerName) {
        CustomerName = customerName;
    }

    public String getCustomerIdType() {
        return CustomerIdType;
    }

    public void setCustomerIdType(String customerIdType) {
        CustomerIdType = customerIdType;
    }

    public String getCustomerIdNo() {
        return CustomerIdNo;
    }

    public void setCustomerIdNo(String customerIdNo) {
        CustomerIdNo = customerIdNo;
    }

    @Override
    public String toString() {
        return "ReqBarterpaperBumfQuery{" +
                "ContNo='" + ContNo + '\'' +
                ", ELecType='" + ELecType + '\'' +
                ", CustomerName='" + CustomerName + '\'' +
                ", CustomerIdType='" + CustomerIdType + '\'' +
                ", CustomerIdNo='" + CustomerIdNo + '\'' +
                '}';
    }
}
