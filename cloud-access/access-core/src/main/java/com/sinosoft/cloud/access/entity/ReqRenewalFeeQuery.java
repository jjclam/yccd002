package com.sinosoft.cloud.access.entity;

/**
 * 续期缴费查询XQ001
 */
public class ReqRenewalFeeQuery {
    private String RiskCode;
    //保单号
    private String ContNo;
    //xxx编码
    private String BankCode;
    //地区代码
    private String ZoneNo;
    //网点编码
    private String BranchNo;
    //柜员代码
    private String TellerNo;

    public String getRiskCode() {
        return RiskCode;
    }

    public void setRiskCode(String riskCode) {
        RiskCode = riskCode;
    }

    public String getContNo() {
        return ContNo;
    }

    public void setContNo(String contNo) {
        ContNo = contNo;
    }

    public String getBankCode() {
        return BankCode;
    }

    public void setBankCode(String bankCode) {
        BankCode = bankCode;
    }

    public String getZoneNo() {
        return ZoneNo;
    }

    public void setZoneNo(String zoneNo) {
        ZoneNo = zoneNo;
    }

    public String getBranchNo() {
        return BranchNo;
    }

    public void setBranchNo(String branchNo) {
        BranchNo = branchNo;
    }

    public String getTellerNo() {
        return TellerNo;
    }

    public void setTellerNo(String tellerNo) {
        TellerNo = tellerNo;
    }

    public String getV(String s) {
        return null;
    }

    public String getV(int i) {
        return null;
    }

    public int getFieldType(String s) {
        return 0;
    }

    public int getFieldType(int i) {
        return 0;
    }

    public int getFieldCount() {
        return 0;
    }

    public int getFieldIndex(String s) {
        return 0;
    }

    public String getFieldName(int i) {
        return null;
    }

    public boolean setV(String s, String s1) {
        return false;
    }

    @Override
    public String toString() {
        return "ReqRenewalFeeQuery{" +
                "RiskCode='" + RiskCode + '\'' +
                ", ContNo='" + ContNo + '\'' +
                ", BankCode='" + BankCode + '\'' +
                ", ZoneNo='" + ZoneNo + '\'' +
                ", BranchNo='" + BranchNo + '\'' +
                ", TellerNo='" + TellerNo + '\'' +
                '}';
    }
}
