package com.sinosoft.cloud.access.entity;

import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 * Created by zhaoshulei on 2017/9/28.
 * 特别约定
 */
@XStreamAlias("SpecialInfo")
public class SpecialInfo {
    @XStreamAlias("SpecContent")
    private String specContent;

    public String getSpecContent() {
        return specContent;
    }

    public void setSpecContent(String specContent) {
        this.specContent = specContent;
    }

    @Override
    public String toString() {
        return "SpecialInfo{" +
                "specContent='" + specContent + '\'' +
                '}';
    }
}
