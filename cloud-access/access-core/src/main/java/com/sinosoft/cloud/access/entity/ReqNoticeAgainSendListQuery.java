package com.sinosoft.cloud.access.entity;
/**
 * 4008查询保单列表POS060
 */
public class ReqNoticeAgainSendListQuery {
    //文本类型
    private String TextType;
    //客户号
    private String CustomerNo;
    //证件号
    private String CustomerIDNo;
    //证件姓名
    private String CustomerName;

    public String getTextType() {
        return TextType;
    }

    public void setTextType(String textType) {
        TextType = textType;
    }

    public String getCustomerNo() {
        return CustomerNo;
    }

    public void setCustomerNo(String customerNo) {
        CustomerNo = customerNo;
    }

    public String getCustomerIDNo() {
        return CustomerIDNo;
    }

    public void setCustomerIDNo(String customerIDNo) {
        CustomerIDNo = customerIDNo;
    }

    public String getCustomerName() {
        return CustomerName;
    }

    public void setCustomerName(String customerName) {
        CustomerName = customerName;
    }

    @Override
    public String toString() {
        return "ReqNoticeAgainSendListQuery{" +
                "TextType='" + TextType + '\'' +
                ", CustomerNo='" + CustomerNo + '\'' +
                ", CustomerIDNo='" + CustomerIDNo + '\'' +
                ", CustomerName='" + CustomerName + '\'' +
                '}';
    }
}

