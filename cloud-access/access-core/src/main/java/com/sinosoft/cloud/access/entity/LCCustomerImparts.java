package com.sinosoft.cloud.access.entity;

import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.List;

/**
 * Created by Administrator on 2018/4/13.
 */
public class LCCustomerImparts {
    @XStreamImplicit(itemFieldName="LCCustomerImpart")
    private List<LCCustomerImpart> LCCustomerImparts;

    public List<LCCustomerImpart> getLCCustomerImparts() {
        return LCCustomerImparts;
    }

    public void setLCCustomerImparts(List<LCCustomerImpart> LCCustomerImparts) {
        this.LCCustomerImparts = LCCustomerImparts;
    }

    @Override
    public String toString() {
        return "LCCustomerImparts{" +
                "LCCustomerImparts=" + LCCustomerImparts +
                '}';
    }
}
