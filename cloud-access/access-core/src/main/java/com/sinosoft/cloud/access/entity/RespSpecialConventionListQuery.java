package com.sinosoft.cloud.access.entity;

/**
 * @program: abc-cloud-access
 * @description: 特别约定（出境告知）保全项目保单列表查询POS121.
 * @author: BaoYongmeng
 * @create: 2018-12-24 17:28
 **/
public class RespSpecialConventionListQuery {
    //保全项目编码
    private String EdorType;
    private PolicyQueryInfos PolicyQueryInfos;
    private PhoneInfos PhoneInfos;

    public String getEdorType() {
        return EdorType;
    }

    public void setEdorType(String edorType) {
        EdorType = edorType;
    }

    public com.sinosoft.cloud.access.entity.PolicyQueryInfos getPolicyQueryInfos() {
        return PolicyQueryInfos;
    }

    public void setPolicyQueryInfos(com.sinosoft.cloud.access.entity.PolicyQueryInfos policyQueryInfos) {
        PolicyQueryInfos = policyQueryInfos;
    }

    public com.sinosoft.cloud.access.entity.PhoneInfos getPhoneInfos() {
        return PhoneInfos;
    }

    public void setPhoneInfos(com.sinosoft.cloud.access.entity.PhoneInfos phoneInfos) {
        PhoneInfos = phoneInfos;
    }

    @Override
    public String toString() {
        return "RespSpecialConventionListQuery{" +
                "EdorType='" + EdorType + '\'' +
                ", PolicyQueryInfos=" + PolicyQueryInfos +
                ", PhoneInfos=" + PhoneInfos +
                '}';
    }
}
