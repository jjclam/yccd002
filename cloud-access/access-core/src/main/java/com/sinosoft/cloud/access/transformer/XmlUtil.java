package com.sinosoft.cloud.access.transformer;

import com.sinosoft.cloud.access.entity.*;
import com.sinosoft.cloud.access.transformer.converters.DoubleConverter;
import com.sinosoft.cloud.access.transformer.converters.IntConverter;
import com.sinosoft.cloud.access.transformer.converters.LongConverter;
import com.thoughtworks.xstream.XStream;


public class XmlUtil {

    static final Class[] trustClass = new Class[]{
            Appnt.class,
            Bnf.class,
            BnfList.class,
            Bnfs.class,
            Body.class,
            CashValue.class,
            CashValues.class,
            ContnoDetail.class,
            ContnoList.class,
            CustomerImpart.class,
            CustomerImparts.class,
            Head.class,
            AliHead.class,
            ShuiDiHead.class,
            Insured.class,
            Insureds.class,
            PolicyBasicInfo.class,
            Risk.class,
            RiskDetail.class,
            RiskList.class,
            Risks.class,
            SpecialInfo.class,
            SpecialInfos.class,
            TranData.class,
    };

    private static XStream xStream;

    private synchronized static void init() {

        if (xStream != null) {
            return;
        }
        xStream = new XStream();
        XStream.setupDefaultSecurity(xStream);
        xStream.processAnnotations(trustClass);
        xStream.allowTypes(trustClass);
        xStream.registerConverter(new DoubleConverter());
        xStream.registerConverter(new LongConverter());
        xStream.registerConverter(new IntConverter());

    }

    public static <T> T getBean(String xml, Class<T> tClass) {
        if (xStream == null) {
            init();
        }
        return (T) xStream.fromXML(xml);
    }



    public static String getString(Object xmlObj) {
        if (xmlObj == null) {
            return null;
        }
        if (xStream == null) {
            init();
        }
        return xStream.toXML(xmlObj);
    }

}
