package com.sinosoft.cloud.access.configuration;

import com.sinosoft.cloud.access.net.DefaultHttpHandler;
import com.sinosoft.cloud.access.transformer.XsltTrans;
import com.sinosoft.cloud.access.util.StaxonUtil;
import com.sinosoft.cloud.common.SpringContextUtils;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;

import static com.sinosoft.cloud.access.configuration.RestfulEndpoint.XML_ROOT;

/**
 * @Author: cuiguangdong
 * @Date: 2018/9/4 17:26
 */
@WebService(serviceName = "WebServiceEndpoint"
        , endpointInterface = "com.sinosoft.cloud.access.configuration.WebServiceEndpoint"
        , targetNamespace = "http://com.sinosoft.cloud")
@Component
public class WebServiceEndpoint {

    private static final String JSON = "json";
    private Logger cLogger = LoggerFactory.getLogger(this.getClass());

    @Value("${spring.application.name}")
    private String accessName;

    /**
     * 接收到报文的处理方法
     *
     * @param accessName 渠道名称
     * @param dataType   报文类型
     * @param data       交易报文
     * @return
     */
    @WebMethod
    @WebResult(name = "String")
    public String service(@WebParam(name = "accessName") String accessName, @WebParam(name = "dataType") String dataType, @WebParam(name = "data") String data) {
        long startTime = System.currentTimeMillis();

        String error = "";
        if (StringUtils.isEmpty(accessName)) {
            error = "传入的第一个参数【渠道名称】有误！错误值为：" + accessName;
            cLogger.error(error);
            return StaxonUtil.getJsonByMip(getErrorXml(error));
        }

        if (dataType.equals(JSON)) {
            if ("mip".equals(accessName)) {
                data = StaxonUtil.getXmlByMip(data);
                if (null == data) {
                    error = "传输的json格式有误，没有message节点";
                    cLogger.error(error);
                    return StaxonUtil.getJsonByMip(getErrorXml(error));
                }
            } else {
                data = StaxonUtil.json2xml(data, XML_ROOT);
            }
        }

        DefaultHttpHandler handler = new DefaultHttpHandler();
        handler.setAccessName(accessName);
        String result = handler.submitData(data);
        cLogger.info(result);
//        String result = data;
        if (dataType.equals(JSON)) {
            if ("mip".equals(accessName)) {
                //StringEscapeUtils.unescapeJava()去掉json转化产生的转义字符
               /* result = StringEscapeUtils.unescapeJava(StaxonUtil.getJsonByMip(result));*/
                result = StaxonUtil.getJsonByMip(result);
            } else {
                result = StaxonUtil.xml2json(result, XML_ROOT);
            }
        }
        cLogger.info("返回" + accessName + "的报文为：" + result);
        cLogger.info(accessName + "完成交易：" + (System.currentTimeMillis() - startTime) + "ms");
        return result;

    }

    private String getErrorXml(String error) {
        XsltTrans xsltTrans = new XsltTrans();
        String applicationAccessName = accessName.split("-")[1];
        xsltTrans.setAccessName(applicationAccessName);
        return xsltTrans.returnError(error);
    }
}
