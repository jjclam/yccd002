package com.sinosoft.cloud.access.entity;

import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.List;

/**
 * @Author:lizhen
 * @Description:
 * @Date:Created in 14:11 2018/11/1
 * @Modified By:
 */
public class CaluParamInfos {
    @XStreamImplicit(itemFieldName="CaluParamInfo")
    private List<CaluParamInfo> CaluParamInfos;

    public List<CaluParamInfo> getCaluParamInfos() {
        return CaluParamInfos;
    }

    public void setCaluParamInfos(List<CaluParamInfo> CaluParamInfos) {
        this.CaluParamInfos = CaluParamInfos;
    }

    @Override
    public String toString() {
        return "CaluParamInfos{" +
                "CaluParamInfos=" + CaluParamInfos +
                '}';
    }
}