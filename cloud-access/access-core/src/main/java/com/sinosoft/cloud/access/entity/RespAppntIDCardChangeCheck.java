package com.sinosoft.cloud.access.entity;

/**
 *投保人证件有效期校验POS058
 */
public class RespAppntIDCardChangeCheck {
    //是否过期
    private String IDStatus;
    //证件有效期
    private String ValidDate;

    public String getIDStatus() {
        return IDStatus;
    }

    public void setIDStatus(String IDStatus) {
        this.IDStatus = IDStatus;
    }

    public String getValidDate() {
        return ValidDate;
    }

    public void setValidDate(String validDate) {
        ValidDate = validDate;
    }

    @Override
    public String toString() {
        return "RespAppntIDCardChangeCheck{" +
                "IDStatus='" + IDStatus + '\'' +
                ", ValidDate='" + ValidDate + '\'' +
                '}';
    }
}
