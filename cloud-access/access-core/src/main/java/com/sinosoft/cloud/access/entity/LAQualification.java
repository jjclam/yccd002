package com.sinosoft.cloud.access.entity;

/**
 * @Description
 * @Author:zhaoshulei
 * @Date:2018/4/10
 */
public class LAQualification {
    private String AgentCode;
    private String QualifNo;
    private int Idx;
    private String GrantUnit;
    private String GrantDate;
    private String InvalidDate;
    private String InvalidRsn;
    private String reissueDate;
    private String reissueRsn;
    private int ValidPeriod;
    private String State;
    private String PasExamDate;
    private String ExamYear;
    private String ExamTimes;
    private String Operator;
    private String MakeDate;
    private String MakeTime;
    private String ModifyDate;
    private String ModifyTime;
    private String ValidStart;
    private String ValidEnd;
    private String OldQualifNo;
    private String QualifName;
    private String State1;

    public String getAgentCode() {
        return AgentCode;
    }

    public void setAgentCode(String agentCode) {
        AgentCode = agentCode;
    }

    public String getQualifNo() {
        return QualifNo;
    }

    public void setQualifNo(String qualifNo) {
        QualifNo = qualifNo;
    }

    public int getIdx() {
        return Idx;
    }

    public void setIdx(int idx) {
        Idx = idx;
    }

    public String getGrantUnit() {
        return GrantUnit;
    }

    public void setGrantUnit(String grantUnit) {
        GrantUnit = grantUnit;
    }

    public String getGrantDate() {
        return GrantDate;
    }

    public void setGrantDate(String grantDate) {
        GrantDate = grantDate;
    }

    public String getInvalidDate() {
        return InvalidDate;
    }

    public void setInvalidDate(String invalidDate) {
        InvalidDate = invalidDate;
    }

    public String getInvalidRsn() {
        return InvalidRsn;
    }

    public void setInvalidRsn(String invalidRsn) {
        InvalidRsn = invalidRsn;
    }

    public String getReissueDate() {
        return reissueDate;
    }

    public void setReissueDate(String reissueDate) {
        this.reissueDate = reissueDate;
    }

    public String getReissueRsn() {
        return reissueRsn;
    }

    public void setReissueRsn(String reissueRsn) {
        this.reissueRsn = reissueRsn;
    }

    public int getValidPeriod() {
        return ValidPeriod;
    }

    public void setValidPeriod(int validPeriod) {
        ValidPeriod = validPeriod;
    }

    public String getState() {
        return State;
    }

    public void setState(String state) {
        State = state;
    }

    public String getPasExamDate() {
        return PasExamDate;
    }

    public void setPasExamDate(String pasExamDate) {
        PasExamDate = pasExamDate;
    }

    public String getExamYear() {
        return ExamYear;
    }

    public void setExamYear(String examYear) {
        ExamYear = examYear;
    }

    public String getExamTimes() {
        return ExamTimes;
    }

    public void setExamTimes(String examTimes) {
        ExamTimes = examTimes;
    }

    public String getOperator() {
        return Operator;
    }

    public void setOperator(String operator) {
        Operator = operator;
    }

    public String getMakeDate() {
        return MakeDate;
    }

    public void setMakeDate(String makeDate) {
        MakeDate = makeDate;
    }

    public String getMakeTime() {
        return MakeTime;
    }

    public void setMakeTime(String makeTime) {
        MakeTime = makeTime;
    }

    public String getModifyDate() {
        return ModifyDate;
    }

    public void setModifyDate(String modifyDate) {
        ModifyDate = modifyDate;
    }

    public String getModifyTime() {
        return ModifyTime;
    }

    public void setModifyTime(String modifyTime) {
        ModifyTime = modifyTime;
    }

    public String getValidStart() {
        return ValidStart;
    }

    public void setValidStart(String validStart) {
        ValidStart = validStart;
    }

    public String getValidEnd() {
        return ValidEnd;
    }

    public void setValidEnd(String validEnd) {
        ValidEnd = validEnd;
    }

    public String getOldQualifNo() {
        return OldQualifNo;
    }

    public void setOldQualifNo(String oldQualifNo) {
        OldQualifNo = oldQualifNo;
    }

    public String getQualifName() {
        return QualifName;
    }

    public void setQualifName(String qualifName) {
        QualifName = qualifName;
    }

    public String getState1() {
        return State1;
    }

    public void setState1(String state1) {
        State1 = state1;
    }

    @Override
    public String toString() {
        return "LAQualification{" +
                "AgentCode='" + AgentCode + '\'' +
                ", QualifNo='" + QualifNo + '\'' +
                ", Idx=" + Idx +
                ", GrantUnit='" + GrantUnit + '\'' +
                ", GrantDate='" + GrantDate + '\'' +
                ", InvalidDate='" + InvalidDate + '\'' +
                ", InvalidRsn='" + InvalidRsn + '\'' +
                ", reissueDate='" + reissueDate + '\'' +
                ", reissueRsn='" + reissueRsn + '\'' +
                ", ValidPeriod=" + ValidPeriod +
                ", State='" + State + '\'' +
                ", PasExamDate='" + PasExamDate + '\'' +
                ", ExamYear='" + ExamYear + '\'' +
                ", ExamTimes='" + ExamTimes + '\'' +
                ", Operator='" + Operator + '\'' +
                ", MakeDate='" + MakeDate + '\'' +
                ", MakeTime='" + MakeTime + '\'' +
                ", ModifyDate='" + ModifyDate + '\'' +
                ", ModifyTime='" + ModifyTime + '\'' +
                ", ValidStart='" + ValidStart + '\'' +
                ", ValidEnd='" + ValidEnd + '\'' +
                ", OldQualifNo='" + OldQualifNo + '\'' +
                ", QualifName='" + QualifName + '\'' +
                ", State1='" + State1 + '\'' +
                '}';
    }
}
