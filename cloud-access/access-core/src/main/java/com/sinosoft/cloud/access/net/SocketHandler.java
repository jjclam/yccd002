package com.sinosoft.cloud.access.net;

import com.sinosoft.cloud.access.configuration.MessageProducer;
import com.sinosoft.cloud.access.crypto.CryptoBean;
import com.sinosoft.cloud.access.crypto.CryptoFactory;
import com.sinosoft.cloud.access.router.RouterBean;
import com.sinosoft.cloud.access.router.RouterFactory;
import com.sinosoft.cloud.common.SpringContextUtils;
import com.sinosoft.utility.ExceptionUtils;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.util.ReferenceCountUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.slf4j.MDC;

import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

import static io.netty.channel.ChannelFutureListener.CLOSE;

//@ChannelHandler.Sharable
public abstract class SocketHandler extends ChannelInboundHandlerAdapter implements Handler {

    protected String accessName;
    protected static final String NEED_ROUTER = "@NEED_ROUTER@";

    protected final Log logger = LogFactory.getLog(getClass());
    protected final Log monitorLog = LogFactory.getLog("monitor");


    public static final ConcurrentHashMap<String, String> messageHashTable = new ConcurrentHashMap<>();
    private DecoderFactory decoderFactory = DecoderFactory.getInstance();

    /**
     * 创建由spring管理的MQ的消息发送者对象
     */
    MessageProducer producer = SpringContextUtils.getBeanByClass(MessageProducer.class);

    /**
     * 创建由spring管理的报文持久化开关对象
     */
    AccessControl accessControl = SpringContextUtils.getBeanByClass(AccessControl.class);

    /**
     * 接受netty Socket 消息
     *
     * @param ctx
     * @param msg
     */
    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) {
        try {
            if (msg instanceof ByteBuf) {
                ByteBuf in = (ByteBuf) msg;
                String strMsg = in.toString(Charset.defaultCharset());
                if ("hunannongxin".equals(this.getAccessName())) {
                    strMsg = in.toString(Charset.forName("GBK"));
                }
                long start = System.currentTimeMillis();
                MDC.put("start", String.valueOf(start));
                messageHashTable.put(getThreadKey(), String.valueOf(start));
                dealMessage(ctx, strMsg);
                long end = System.currentTimeMillis();
                monitorLog.info("完成交易:" + (end - start) + " ms");
            }
        } finally {
            MDC.remove("transNo");
            MDC.remove("start");
            MDC.remove("status");
            ReferenceCountUtil.release(msg);
        }
    }

    @Override
    public void channelReadComplete(ChannelHandlerContext ctx) throws Exception {
        super.channelReadComplete(ctx);
//        ctx.writeAndFlush(Unpooled.EMPTY_BUFFER).addListener(CLOSE);
//        ctx.writeAndFlush(Unpooled.EMPTY_BUFFER).addListener((ChannelFutureListener) future -> {
//            Channel channel = future.channel();
//            if (channel instanceof NioSocketChannel) {
//                NioSocketChannel socketChannel = (NioSocketChannel) channel;
//                ChannelFuture shut = socketChannel.shutdownOutput();
//            }
//        });
    }

    //    @Override
//    public void channelReadComplete(ChannelHandlerContext ctx) {
//
//        byte[] completeBytes = messageHashTable.get(getThreadKey());
//        String completeMsg = "";
//        if (null != completeBytes) {
//            completeMsg = new String(completeBytes, Charset.defaultCharset());
//        }
//
//        if (completeMsg == null || completeMsg.equals("")) {
//            logger.error("在channelReadComplete中收到消息为空:" + completeMsg);
////            throw new RuntimeException("在channelReadComplete中收到消息为空:" + completeMsg);
//            return;
//        }
//        logger.debug("channelReadComplete 报文:" + completeMsg);
//
//        /**
//         * 进行密文的消息发送
//         */
//        String CipherTextSwitch = accessControl.getCipherTextSwitch();
//        logger.debug("是否需要密文持久化？" + CipherTextSwitch);
//        if ("true".equalsIgnoreCase(CipherTextSwitch)) {
//            if (null != completeMsg) {
//                String sendText = getAccessName() + "," + completeMsg.trim();
//                producer.send("CipherText", sendText);
//                logger.debug("已经发送密文持久化消息！");
//            }
//        }
//
//        Coder coder = decoderFactory.getDecoder(getAccessName());
//        /**
//         * 如果coder为空,则不需要进行 报文长度的判断
//         */
//        if (coder == null) {
//            dealMessage(ctx, completeMsg);
//        } else {
//            if (coder.needDecoder(completeMsg) && !coder.isComplate(completeMsg)) {
//                ctx.flush();
//                return;
//            } else {
//                dealMessage(ctx, completeMsg);
//            }
//        }
//        ctx.flush();
//    }

    /**
     * 当客户端强制重发或者断开，将HashTable中当前线程所对应的key和value删除
     *
     * @param ctx
     * @throws Exception
     */
    @Override
    public void channelInactive(ChannelHandlerContext ctx) throws Exception {
        super.channelInactive(ctx);
//        if (null != messageHashTable.get(getThreadKey()) && messageHashTable.get(getThreadKey()).length() > 0) {
//            int index = getThreadKey().indexOf(";");
//            String thread = getThreadKey().substring(index + 1);
//            logger.debug("当前线程：" + thread);
////            messageHashTable.remove(getThreadKey());
//            logger.debug("客户端强制关闭了连接！清除" + thread + "线程接收的消息");
//        }
    }

    /**
     * 处理业务逻辑
     *
     * @param ctx
     * @param completeMsg
     */
    private void dealMessage(ChannelHandlerContext ctx, String completeMsg) {
        String strRecevice = null;
        if (logger.isDebugEnabled()) {
            logger.debug("接收到Scoket请求");
        }

        String strIn = new String(completeMsg);

        if (logger.isInfoEnabled()) {
            logger.debug("接收到原始消息为：" + strIn);
        }

                /*判断是否解密，如果解密调用对应的解密类*/
        if (needCrypt()) {
            strIn = decrypt(strIn.trim());
        }
        if (logger.isInfoEnabled()) {
            logger.debug("接收到解密消息为：" + strIn);
        }


        if (strIn != null) {
            strIn = strIn.trim();
        }

        strRecevice = receive(strIn);
        /*进行路由处理*/
        if (strRecevice != null && strRecevice.equals(NEED_ROUTER)) {
            RouterBean routerBean = RouterFactory.getInstance().getRouterBeanByAccess(getAccessName());
            RouterHandler routerHandler = new RouterHandler() {
                @Override
                public String receive(String msg) {
                    ByteBuf encoded = ctx.alloc().buffer(4 * msg.length());
                    encoded.writeBytes(msg.getBytes());
                    ctx.writeAndFlush(encoded).addListener(CLOSE);
                    return msg;
                }

                @Override
                public String send() {
                    return completeMsg;
                }
            };
            routerHandler.setOrgXml(strIn);
            routerHandler.setAccessName(getAccessName());
            SocketClient socketClient = new SocketClient();
            List<ChannelHandler> socketHandlers = new ArrayList<>();
            DefaultDecoder defaultDecoder = new DefaultDecoder();
            defaultDecoder.setAccessName(getAccessName());
            socketHandlers.add(defaultDecoder);
            socketHandlers.add(routerHandler);
            socketClient.setSocketHandlers(socketHandlers);
            try {
                socketClient.connect(routerBean.getTargetIP(), routerBean.getTargetPort());
            } catch (Exception e) {
                e.printStackTrace();
            }
            messageHashTable.remove(getThreadKey());
        } else {

                /*判断是否加密，如果解密调用对应的解密类*/
            if (needCrypt()) {
                strRecevice = encrypt(strRecevice.trim());
            }

            if (strRecevice != null) {

                Coder coder = decoderFactory.getDecoder(getAccessName());
                if (coder != null) {
                    strRecevice = coder.encoder(strRecevice);
                }

                logger.info("返回xxx的报文：" + strRecevice);

                byte[] gbkBytes = new byte[0];

                gbkBytes = strRecevice.getBytes();

                if ("hunannongxin".equals(this.getAccessName())){
                    gbkBytes = strRecevice.getBytes(Charset.forName("GBK"));
                }

                ByteBuf encoded = Unpooled.copiedBuffer(gbkBytes);
                ctx.writeAndFlush(encoded).addListener(CLOSE);

                //从hashTable中获取对应交易的开始时间和交易编码
                String start = messageHashTable.get(getThreadKey());
                messageHashTable.remove(getThreadKey());

//                String transCode = XsltTrans.getCurrentThreadTransCode();
//                monitorLog.info("互联网" + transCode + "交易总耗时：" + (System.currentTimeMillis() - Long.parseLong(start)) + " ms");
                long end = System.currentTimeMillis();
//                logger.info("$$$TxReturn." + "txcode={" + this.getAccessName() + XsltTrans.getCurrentThreadTransCode() + "}" + "txid={" + MDC.get("transNo") + "}start={" + MDC.get("start") + "}"
//                        + "end={" + end + "}" + "status={" + MDC.get("status") + "}time={" + (end - Long.parseLong(MDC.get("start"))) + "}ms");
//                byte[] gbkBytes = new byte[0];
//                gbkBytes = strRecevice.getBytes();
//                ByteBuf encoded = Unpooled.copiedBuffer(gbkBytes);
//                ctx.writeAndFlush(encoded).addListener((ChannelFutureListener) future -> {
//                    if (future.isSuccess()) {
//                        Channel channel = future.channel();
//                        logger.debug("channel:" + channel.getClass().getName());
//                        if (channel instanceof NioSocketChannel) {
//                            NioSocketChannel socketChannel = (NioSocketChannel) channel;
//                            ChannelFuture shut = socketChannel.shutdownOutput();
//                            shut.addListener((ChannelFutureListener) shutFuture -> {
//                                logger.debug("打开状态:" + shutFuture.channel().isOpen());
//                            });
//                            logger.debug("打开状态:" + socketChannel.isOpen());
//                        }
//                    } else {
//                        logger.error("发生异常错误, 通讯失败!");
//                    }
//                });


            }
        }
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {
        // Close the connection when an exception is raised.
        cause.printStackTrace();
        if (cause instanceof Exception) {
            logger.error("发生异常错误:" + ExceptionUtils.exceptionToString((Exception) cause));
        } else {
            logger.error("发生异常错误getMessage:" + cause.getMessage());
            logger.error("发生异常错误cause:", cause);

        }
//        messageHashTable.remove(getThreadKey());
        ctx.close();
    }


    public String getAccessName() {
        return accessName;
    }

    @Override
    public void setAccessName(String accessName) {
        this.accessName = accessName;
    }

    @Override
    public String decrypt(String msg) {
        CryptoFactory cryptoFactory = CryptoFactory.getInstance();
        CryptoBean cryptoBean = cryptoFactory.getCryptoBeanByAccess(this.accessName);

        /**
         * 由于现在使用开关，故@AccessCrypto注解会一直打开
         * 在配置文件中配置开关，根据不同渠道不同的配置
         * 如果为true，则表示使用加密，执行加密。如果为false则不进行加密操作，切掉报文头返回字符串
         */
        if (cryptoBean != null) {
            return cryptoBean.decryt(msg);
        }
        return null;
    }

    @Override
    public String encrypt(String msg) {
        CryptoFactory cryptoFactory = CryptoFactory.getInstance();
        CryptoBean cryptoBean = cryptoFactory.getCryptoBeanByAccess(this.accessName);

        /**
         * 由于现在使用开关，故@AccessCrypto注解会一直打开
         * 在配置文件中配置开关，根据不同渠道不同的配置
         * 如果为true，则表示使用解密，执行解密。如果为false则不进行加密操作，直接返回明文
         */
        if (cryptoBean != null) {
            String encryptSwitch = cryptoBean.getEnv().getProperty("cloud.access.encrypt." + this.accessName);
            if ("false".equalsIgnoreCase(encryptSwitch)) {
                return msg;
            }
            if ("true".equalsIgnoreCase(encryptSwitch)) {
                logger.info("返回xxx前未加密的报文：" + msg.trim());
                long start = System.currentTimeMillis();
                String encrytStr = cryptoBean.encryt(msg);
//                logger.debug(XsltTrans.getCurrentThreadTransCode() + "交易加密耗时：" + String.valueOf(System.currentTimeMillis() - start) + "ms");
                return encrytStr;
            }
            return null;
        }
        return null;
    }

    public boolean needCrypt() {
        CryptoFactory cryptoFactory = CryptoFactory.getInstance();
        return cryptoFactory.needCrypto(this.accessName);
    }


    protected String getThreadKey() {
        return getClass() + ";" + Thread.currentThread().getName() + ":" + Thread.currentThread().getId();
    }

    //    public static ConcurrentHashMap<String, String> getMessageHashTable() {
//        return messageHashTable;
//    }
    public static String getKey() {
        return Thread.currentThread().getName() + ":" + Thread.currentThread().getId();
    }
}
