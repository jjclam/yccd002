package com.sinosoft.cloud.access.entity;

/**
 * abc-cloud-access
 *
 * @author lizhen
 * @create 2019/3/29
 */
public class MainRisk {
    //主险编码
    private String MainRiskCode;
    //主险名称
    private String MainRiskName;
    //主险保额
    private String MainAmnt;
    //主险保费
    private String MainPrem;
    //现金价值
    private String MainCashValue;
    //主险份数
    private String MainMult;
    //主险生存金领取标志/续保标记
    private String MainXBFlag;
    //主险生存金领取方式/续保方式
    private String MainXBPattern;
    //主险被保人姓名
    private String MainInsuredName;
    //主险赔付日期/缴费对应日
    private String MainPayDate;
    //主险健康加费
    private String MainHealthPrem;
    //主险职业加费
    private String MainJobPrem;

    public String getMainRiskCode() {
        return MainRiskCode;
    }

    public void setMainRiskCode(String mainRiskCode) {
        MainRiskCode = mainRiskCode;
    }

    public String getMainRiskName() {
        return MainRiskName;
    }

    public void setMainRiskName(String mainRiskName) {
        MainRiskName = mainRiskName;
    }

    public String getMainAmnt() {
        return MainAmnt;
    }

    public void setMainAmnt(String mainAmnt) {
        MainAmnt = mainAmnt;
    }

    public String getMainPrem() {
        return MainPrem;
    }

    public void setMainPrem(String mainPrem) {
        MainPrem = mainPrem;
    }

    public String getMainCashValue() {
        return MainCashValue;
    }

    public void setMainCashValue(String mainCashValue) {
        MainCashValue = mainCashValue;
    }

    public String getMainMult() {
        return MainMult;
    }

    public void setMainMult(String mainMult) {
        MainMult = mainMult;
    }

    public String getMainXBFlag() {
        return MainXBFlag;
    }

    public void setMainXBFlag(String mainXBFlag) {
        MainXBFlag = mainXBFlag;
    }

    public String getMainXBPattern() {
        return MainXBPattern;
    }

    public void setMainXBPattern(String mainXBPattern) {
        MainXBPattern = mainXBPattern;
    }

    public String getMainInsuredName() {
        return MainInsuredName;
    }

    public void setMainInsuredName(String mainInsuredName) {
        MainInsuredName = mainInsuredName;
    }

    public String getMainPayDate() {
        return MainPayDate;
    }

    public void setMainPayDate(String mainPayDate) {
        MainPayDate = mainPayDate;
    }

    public String getMainHealthPrem() {
        return MainHealthPrem;
    }

    public void setMainHealthPrem(String mainHealthPrem) {
        MainHealthPrem = mainHealthPrem;
    }

    public String getMainJobPrem() {
        return MainJobPrem;
    }

    public void setMainJobPrem(String mainJobPrem) {
        MainJobPrem = mainJobPrem;
    }

    @Override
    public String toString() {
        return "MainRisk{" +
                "MainRiskCode='" + MainRiskCode + '\'' +
                ", MainRiskName='" + MainRiskName + '\'' +
                ", MainAmnt='" + MainAmnt + '\'' +
                ", MainPrem='" + MainPrem + '\'' +
                ", MainCashValue='" + MainCashValue + '\'' +
                ", MainMult='" + MainMult + '\'' +
                ", MainXBFlag='" + MainXBFlag + '\'' +
                ", MainXBPattern='" + MainXBPattern + '\'' +
                ", MainInsuredName='" + MainInsuredName + '\'' +
                ", MainPayDate='" + MainPayDate + '\'' +
                ", MainHealthPrem='" + MainHealthPrem + '\'' +
                ", MainJobPrem='" + MainJobPrem + '\'' +
                '}';
    }
}