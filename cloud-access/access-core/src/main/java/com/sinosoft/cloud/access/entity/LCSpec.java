package com.sinosoft.cloud.access.entity;

/**
 * @Description
 * @Author:zhaoshulei
 * @Date:2018/4/10
 */
public class LCSpec {
    private long SpecID;
    private long ContID;
    private String ShardingID;
    private String GrpContNo;
    private String ContNo;
    private String PolNo;
    private String ProposalNo;
    private String PrtSeq;
    private String SerialNo;
    private String EndorsementNo;
    private String SpecType;
    private String SpecCode;
    private String SpecContent;
    private String PrtFlag;
    private String BackupType;
    private String Operator;
    private String MakeDate;
    private String MakeTime;
    private String ModifyDate;
    private String ModifyTime;

    public long getSpecID() {
        return SpecID;
    }

    public void setSpecID(long specID) {
        SpecID = specID;
    }

    public long getContID() {
        return ContID;
    }

    public void setContID(long contID) {
        ContID = contID;
    }

    public String getShardingID() {
        return ShardingID;
    }

    public void setShardingID(String shardingID) {
        ShardingID = shardingID;
    }

    public String getGrpContNo() {
        return GrpContNo;
    }

    public void setGrpContNo(String grpContNo) {
        GrpContNo = grpContNo;
    }

    public String getContNo() {
        return ContNo;
    }

    public void setContNo(String contNo) {
        ContNo = contNo;
    }

    public String getPolNo() {
        return PolNo;
    }

    public void setPolNo(String polNo) {
        PolNo = polNo;
    }

    public String getProposalNo() {
        return ProposalNo;
    }

    public void setProposalNo(String proposalNo) {
        ProposalNo = proposalNo;
    }

    public String getPrtSeq() {
        return PrtSeq;
    }

    public void setPrtSeq(String prtSeq) {
        PrtSeq = prtSeq;
    }

    public String getSerialNo() {
        return SerialNo;
    }

    public void setSerialNo(String serialNo) {
        SerialNo = serialNo;
    }

    public String getEndorsementNo() {
        return EndorsementNo;
    }

    public void setEndorsementNo(String endorsementNo) {
        EndorsementNo = endorsementNo;
    }

    public String getSpecType() {
        return SpecType;
    }

    public void setSpecType(String specType) {
        SpecType = specType;
    }

    public String getSpecCode() {
        return SpecCode;
    }

    public void setSpecCode(String specCode) {
        SpecCode = specCode;
    }

    public String getSpecContent() {
        return SpecContent;
    }

    public void setSpecContent(String specContent) {
        SpecContent = specContent;
    }

    public String getPrtFlag() {
        return PrtFlag;
    }

    public void setPrtFlag(String prtFlag) {
        PrtFlag = prtFlag;
    }

    public String getBackupType() {
        return BackupType;
    }

    public void setBackupType(String backupType) {
        BackupType = backupType;
    }

    public String getOperator() {
        return Operator;
    }

    public void setOperator(String operator) {
        Operator = operator;
    }

    public String getMakeDate() {
        return MakeDate;
    }

    public void setMakeDate(String makeDate) {
        MakeDate = makeDate;
    }

    public String getMakeTime() {
        return MakeTime;
    }

    public void setMakeTime(String makeTime) {
        MakeTime = makeTime;
    }

    public String getModifyDate() {
        return ModifyDate;
    }

    public void setModifyDate(String modifyDate) {
        ModifyDate = modifyDate;
    }

    public String getModifyTime() {
        return ModifyTime;
    }

    public void setModifyTime(String modifyTime) {
        ModifyTime = modifyTime;
    }

    @Override
    public String toString() {
        return "LCSpec{" +
                "SpecID=" + SpecID +
                ", ContID=" + ContID +
                ", ShardingID='" + ShardingID + '\'' +
                ", GrpContNo='" + GrpContNo + '\'' +
                ", ContNo='" + ContNo + '\'' +
                ", PolNo='" + PolNo + '\'' +
                ", ProposalNo='" + ProposalNo + '\'' +
                ", PrtSeq='" + PrtSeq + '\'' +
                ", SerialNo='" + SerialNo + '\'' +
                ", EndorsementNo='" + EndorsementNo + '\'' +
                ", SpecType='" + SpecType + '\'' +
                ", SpecCode='" + SpecCode + '\'' +
                ", SpecContent='" + SpecContent + '\'' +
                ", PrtFlag='" + PrtFlag + '\'' +
                ", BackupType='" + BackupType + '\'' +
                ", Operator='" + Operator + '\'' +
                ", MakeDate='" + MakeDate + '\'' +
                ", MakeTime='" + MakeTime + '\'' +
                ", ModifyDate='" + ModifyDate + '\'' +
                ", ModifyTime='" + ModifyTime + '\'' +
                '}';
    }
}
