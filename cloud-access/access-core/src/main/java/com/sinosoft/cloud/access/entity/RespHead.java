package com.sinosoft.cloud.access.entity;

/**
 * @Author:lizhen
 * @Description:
 * @Date:Created in 17:50 2018/11/9
 * @Modified By:
 */
public class RespHead {
    private String Version;
    private String SerialNo;
    private String TransID;
    private String TransDate;
    private String TransTime;
    private String SysCode;
    private String Channel;
    private String RespFlag;
    private String RespDesc;

    @Override
    public String toString() {
        return "RespHead{" +
                "Version='" + Version + '\'' +
                ", SerialNo='" + SerialNo + '\'' +
                ", TransID='" + TransID + '\'' +
                ", TransDate='" + TransDate + '\'' +
                ", TransTime='" + TransTime + '\'' +
                ", SysCode='" + SysCode + '\'' +
                ", Channel='" + Channel + '\'' +
                ", RespFlag='" + RespFlag + '\'' +
                ", RespDesc='" + RespDesc + '\'' +
                '}';
    }

    public String getVersion() {
        return Version;
    }

    public void setVersion(String version) {
        Version = version;
    }

    public String getSerialNo() {
        return SerialNo;
    }

    public void setSerialNo(String serialNo) {
        SerialNo = serialNo;
    }

    public String getTransID() {
        return TransID;
    }

    public void setTransID(String transID) {
        TransID = transID;
    }

    public String getTransDate() {
        return TransDate;
    }

    public void setTransDate(String transDate) {
        TransDate = transDate;
    }

    public String getTransTime() {
        return TransTime;
    }

    public void setTransTime(String transTime) {
        TransTime = transTime;
    }

    public String getSysCode() {
        return SysCode;
    }

    public void setSysCode(String sysCode) {
        SysCode = sysCode;
    }

    public String getChannel() {
        return Channel;
    }

    public void setChannel(String channel) {
        Channel = channel;
    }

    public String getRespFlag() {
        return RespFlag;
    }

    public void setRespFlag(String respFlag) {
        RespFlag = respFlag;
    }

    public String getRespDesc() {
        return RespDesc;
    }

    public void setRespDesc(String respDesc) {
        RespDesc = respDesc;
    }
}