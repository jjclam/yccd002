package com.sinosoft.cloud.access.entity;

/**
 * 保单解挂XQ003.
 */
public class RespPolicyHang {
    //保单号
    private String ContNo;
    //是否挂起成功
    private String HangUpFlag;

    public String getContNo() {
        return ContNo;
    }

    public void setContNo(String contNo) {
        ContNo = contNo;
    }

    public String getHangUpFlag() {
        return HangUpFlag;
    }

    public void setHangUpFlag(String hangUpFlag) {
        HangUpFlag = hangUpFlag;
    }

    @Override
    public String toString() {
        return "RespPolicyHang{" +
                "ContNo='" + ContNo + '\'' +
                ", HangUpFlag='" + HangUpFlag + '\'' +
                '}';
    }
}
