package com.sinosoft.cloud.access.entity;

/**
 * 4009发送邮件POS059
 */
public class ReqNoticeAgainSend {
    //邮箱地址
    private String Email;
    //文本编号
    private String TextNo;

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getTextNo() {
        return TextNo;
    }

    public void setTextNo(String textNo) {
        TextNo = textNo;
    }

    @Override
    public String toString() {
        return "ReqNoticeAgainSend{" +
                "Email='" + Email + '\'' +
                ", TextNo='" + TextNo + '\'' +
                '}';
    }
}
