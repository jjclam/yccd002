package com.sinosoft.cloud.access.entity;

/**
 * abc-cloud-access
 *
 * @author lizhen
 * @create 2019/2/21
 */
public class Parameter {
    //系统代码
    private String ParameterCode;
    //系统名称
    private String ParameterName;
    //截至日期
    private String ParameterDate;

    public String getParameterCode() {
        return ParameterCode;
    }

    public void setParameterCode(String parameterCode) {
        ParameterCode = parameterCode;
    }

    public String getParameterName() {
        return ParameterName;
    }

    public void setParameterName(String parameterName) {
        ParameterName = parameterName;
    }

    public String getParameterDate() {
        return ParameterDate;
    }

    public void setParameterDate(String parameterDate) {
        ParameterDate = parameterDate;
    }

    @Override
    public String toString() {
        return "Parameter{" +
                "ParameterCode='" + ParameterCode + '\'' +
                ", ParameterName='" + ParameterName + '\'' +
                ", ParameterDate='" + ParameterDate + '\'' +
                '}';
    }
}