package com.sinosoft.cloud.access.entity;

/**
 * @Description
 * @Author:zhaoshulei
 * @Date:2018/4/10
 */
public class LCAppnt {
    private long AppntID;
    private long ContID;
    private long PersonID;
    private String ShardingID;
    private String GrpContNo;
    private String ContNo;
    private String PrtNo;
    private String AppntNo;
    private String AppntGrade;
    private String AppntName;
    private String AppntSex;
    private String AppntBirthday;
    private String AppntType;
    private String AddressNo;
    private String IDType;
    private String IDNo;
    private String NativePlace;
    private String Nationality;
    private String RgtAddress;
    private String Marriage;
    private String MarriageDate;
    private String Health;
    private double Stature;
    private double Avoirdupois;
    private String Degree;
    private String CreditGrade;
    private String BankCode;
    private String BankAccNo;
    private String AccName;
    private String JoinCompanyDate;
    private String StartWorkDate;
    private String Position;
    private double Salary;
    private String OccupationType;
    private String OccupationCode;
    private String WorkType;
    private String PluralityType;
    private String SmokeFlag;
    private String Operator;
    private String ManageCom;
    private String MakeDate;
    private String MakeTime;
    private String ModifyDate;
    private String ModifyTime;
    private double BMI;
    private String License;
    private String LicenseType;
    private String RelatToInsu;
    private String OccupationDesb;
    private String IdValiDate;
    private String HaveMotorcycleLicence;
    private String PartTimeJob;
    private String FirstName;
    private String LastName;
    private String CUSLevel;
    private String AppRgtTpye;
    private String TINNO;
    private String TINFlag;
    private String SocialInsuFlag;

    public String getSocialInsuFlag() {
        return SocialInsuFlag;
    }

    public void setSocialInsuFlag(String socialInsuFlag) {
        SocialInsuFlag = socialInsuFlag;
    }

    public long getAppntID() {
        return AppntID;
    }

    public void setAppntID(long appntID) {
        AppntID = appntID;
    }

    public long getContID() {
        return ContID;
    }

    public void setContID(long contID) {
        ContID = contID;
    }

    public long getPersonID() {
        return PersonID;
    }

    public void setPersonID(long personID) {
        PersonID = personID;
    }

    public String getShardingID() {
        return ShardingID;
    }

    public void setShardingID(String shardingID) {
        ShardingID = shardingID;
    }

    public String getGrpContNo() {
        return GrpContNo;
    }

    public void setGrpContNo(String grpContNo) {
        GrpContNo = grpContNo;
    }

    public String getContNo() {
        return ContNo;
    }

    public void setContNo(String contNo) {
        ContNo = contNo;
    }

    public String getPrtNo() {
        return PrtNo;
    }

    public void setPrtNo(String prtNo) {
        PrtNo = prtNo;
    }

    public String getAppntNo() {
        return AppntNo;
    }

    public void setAppntNo(String appntNo) {
        AppntNo = appntNo;
    }

    public String getAppntGrade() {
        return AppntGrade;
    }

    public void setAppntGrade(String appntGrade) {
        AppntGrade = appntGrade;
    }

    public String getAppntName() {
        return AppntName;
    }

    public void setAppntName(String appntName) {
        AppntName = appntName;
    }

    public String getAppntSex() {
        return AppntSex;
    }

    public void setAppntSex(String appntSex) {
        AppntSex = appntSex;
    }

    public String getAppntBirthday() {
        return AppntBirthday;
    }

    public void setAppntBirthday(String appntBirthday) {
        AppntBirthday = appntBirthday;
    }

    public String getAppntType() {
        return AppntType;
    }

    public void setAppntType(String appntType) {
        AppntType = appntType;
    }

    public String getAddressNo() {
        return AddressNo;
    }

    public void setAddressNo(String addressNo) {
        AddressNo = addressNo;
    }

    public String getIDType() {
        return IDType;
    }

    public void setIDType(String IDType) {
        this.IDType = IDType;
    }

    public String getIDNo() {
        return IDNo;
    }

    public void setIDNo(String IDNo) {
        this.IDNo = IDNo;
    }

    public String getNativePlace() {
        return NativePlace;
    }

    public void setNativePlace(String nativePlace) {
        NativePlace = nativePlace;
    }

    public String getNationality() {
        return Nationality;
    }

    public void setNationality(String nationality) {
        Nationality = nationality;
    }

    public String getRgtAddress() {
        return RgtAddress;
    }

    public void setRgtAddress(String rgtAddress) {
        RgtAddress = rgtAddress;
    }

    public String getMarriage() {
        return Marriage;
    }

    public void setMarriage(String marriage) {
        Marriage = marriage;
    }

    public String getMarriageDate() {
        return MarriageDate;
    }

    public void setMarriageDate(String marriageDate) {
        MarriageDate = marriageDate;
    }

    public String getHealth() {
        return Health;
    }

    public void setHealth(String health) {
        Health = health;
    }

    public double getStature() {
        return Stature;
    }

    public void setStature(double stature) {
        Stature = stature;
    }

    public double getAvoirdupois() {
        return Avoirdupois;
    }

    public void setAvoirdupois(double avoirdupois) {
        Avoirdupois = avoirdupois;
    }

    public String getDegree() {
        return Degree;
    }

    public void setDegree(String degree) {
        Degree = degree;
    }

    public String getCreditGrade() {
        return CreditGrade;
    }

    public void setCreditGrade(String creditGrade) {
        CreditGrade = creditGrade;
    }

    public String getBankCode() {
        return BankCode;
    }

    public void setBankCode(String bankCode) {
        BankCode = bankCode;
    }

    public String getBankAccNo() {
        return BankAccNo;
    }

    public void setBankAccNo(String bankAccNo) {
        BankAccNo = bankAccNo;
    }

    public String getAccName() {
        return AccName;
    }

    public void setAccName(String accName) {
        AccName = accName;
    }

    public String getJoinCompanyDate() {
        return JoinCompanyDate;
    }

    public void setJoinCompanyDate(String joinCompanyDate) {
        JoinCompanyDate = joinCompanyDate;
    }

    public String getStartWorkDate() {
        return StartWorkDate;
    }

    public void setStartWorkDate(String startWorkDate) {
        StartWorkDate = startWorkDate;
    }

    public String getPosition() {
        return Position;
    }

    public void setPosition(String position) {
        Position = position;
    }

    public double getSalary() {
        return Salary;
    }

    public void setSalary(double salary) {
        Salary = salary;
    }

    public String getOccupationType() {
        return OccupationType;
    }

    public void setOccupationType(String occupationType) {
        OccupationType = occupationType;
    }

    public String getOccupationCode() {
        return OccupationCode;
    }

    public void setOccupationCode(String occupationCode) {
        OccupationCode = occupationCode;
    }

    public String getWorkType() {
        return WorkType;
    }

    public void setWorkType(String workType) {
        WorkType = workType;
    }

    public String getPluralityType() {
        return PluralityType;
    }

    public void setPluralityType(String pluralityType) {
        PluralityType = pluralityType;
    }

    public String getSmokeFlag() {
        return SmokeFlag;
    }

    public void setSmokeFlag(String smokeFlag) {
        SmokeFlag = smokeFlag;
    }

    public String getOperator() {
        return Operator;
    }

    public void setOperator(String operator) {
        Operator = operator;
    }

    public String getManageCom() {
        return ManageCom;
    }

    public void setManageCom(String manageCom) {
        ManageCom = manageCom;
    }

    public String getMakeDate() {
        return MakeDate;
    }

    public void setMakeDate(String makeDate) {
        MakeDate = makeDate;
    }

    public String getMakeTime() {
        return MakeTime;
    }

    public void setMakeTime(String makeTime) {
        MakeTime = makeTime;
    }

    public String getModifyDate() {
        return ModifyDate;
    }

    public void setModifyDate(String modifyDate) {
        ModifyDate = modifyDate;
    }

    public String getModifyTime() {
        return ModifyTime;
    }

    public void setModifyTime(String modifyTime) {
        ModifyTime = modifyTime;
    }

    public double getBMI() {
        return BMI;
    }

    public void setBMI(double BMI) {
        this.BMI = BMI;
    }

    public String getLicense() {
        return License;
    }

    public void setLicense(String license) {
        License = license;
    }

    public String getLicenseType() {
        return LicenseType;
    }

    public void setLicenseType(String licenseType) {
        LicenseType = licenseType;
    }

    public String getRelatToInsu() {
        return RelatToInsu;
    }

    public void setRelatToInsu(String relatToInsu) {
        RelatToInsu = relatToInsu;
    }

    public String getOccupationDesb() {
        return OccupationDesb;
    }

    public void setOccupationDesb(String occupationDesb) {
        OccupationDesb = occupationDesb;
    }

    public String getIdValiDate() {
        return IdValiDate;
    }

    public void setIdValiDate(String idValiDate) {
        IdValiDate = idValiDate;
    }

    public String getHaveMotorcycleLicence() {
        return HaveMotorcycleLicence;
    }

    public void setHaveMotorcycleLicence(String haveMotorcycleLicence) {
        HaveMotorcycleLicence = haveMotorcycleLicence;
    }

    public String getPartTimeJob() {
        return PartTimeJob;
    }

    public void setPartTimeJob(String partTimeJob) {
        PartTimeJob = partTimeJob;
    }

    public String getFirstName() {
        return FirstName;
    }

    public void setFirstName(String firstName) {
        FirstName = firstName;
    }

    public String getLastName() {
        return LastName;
    }

    public void setLastName(String lastName) {
        LastName = lastName;
    }

    public String getCUSLevel() {
        return CUSLevel;
    }

    public void setCUSLevel(String CUSLevel) {
        this.CUSLevel = CUSLevel;
    }

    public String getAppRgtTpye() {
        return AppRgtTpye;
    }

    public void setAppRgtTpye(String appRgtTpye) {
        AppRgtTpye = appRgtTpye;
    }

    public String getTINNO() {
        return TINNO;
    }

    public void setTINNO(String TINNO) {
        this.TINNO = TINNO;
    }

    public String getTINFlag() {
        return TINFlag;
    }

    public void setTINFlag(String TINFlag) {
        this.TINFlag = TINFlag;
    }

    @Override
    public String toString() {
        return "LCAppnt{" +
                "AppntID=" + AppntID +
                ", ContID=" + ContID +
                ", PersonID=" + PersonID +
                ", ShardingID='" + ShardingID + '\'' +
                ", GrpContNo='" + GrpContNo + '\'' +
                ", ContNo='" + ContNo + '\'' +
                ", PrtNo='" + PrtNo + '\'' +
                ", AppntNo='" + AppntNo + '\'' +
                ", AppntGrade='" + AppntGrade + '\'' +
                ", AppntName='" + AppntName + '\'' +
                ", AppntSex='" + AppntSex + '\'' +
                ", AppntBirthday='" + AppntBirthday + '\'' +
                ", AppntType='" + AppntType + '\'' +
                ", AddressNo='" + AddressNo + '\'' +
                ", IDType='" + IDType + '\'' +
                ", IDNo='" + IDNo + '\'' +
                ", NativePlace='" + NativePlace + '\'' +
                ", Nationality='" + Nationality + '\'' +
                ", RgtAddress='" + RgtAddress + '\'' +
                ", Marriage='" + Marriage + '\'' +
                ", MarriageDate='" + MarriageDate + '\'' +
                ", Health='" + Health + '\'' +
                ", Stature=" + Stature +
                ", Avoirdupois=" + Avoirdupois +
                ", Degree='" + Degree + '\'' +
                ", CreditGrade='" + CreditGrade + '\'' +
                ", BankCode='" + BankCode + '\'' +
                ", BankAccNo='" + BankAccNo + '\'' +
                ", AccName='" + AccName + '\'' +
                ", JoinCompanyDate='" + JoinCompanyDate + '\'' +
                ", StartWorkDate='" + StartWorkDate + '\'' +
                ", Position='" + Position + '\'' +
                ", Salary=" + Salary +
                ", OccupationType='" + OccupationType + '\'' +
                ", OccupationCode='" + OccupationCode + '\'' +
                ", WorkType='" + WorkType + '\'' +
                ", PluralityType='" + PluralityType + '\'' +
                ", SmokeFlag='" + SmokeFlag + '\'' +
                ", Operator='" + Operator + '\'' +
                ", ManageCom='" + ManageCom + '\'' +
                ", MakeDate='" + MakeDate + '\'' +
                ", MakeTime='" + MakeTime + '\'' +
                ", ModifyDate='" + ModifyDate + '\'' +
                ", ModifyTime='" + ModifyTime + '\'' +
                ", BMI=" + BMI +
                ", License='" + License + '\'' +
                ", LicenseType='" + LicenseType + '\'' +
                ", RelatToInsu='" + RelatToInsu + '\'' +
                ", OccupationDesb='" + OccupationDesb + '\'' +
                ", IdValiDate='" + IdValiDate + '\'' +
                ", HaveMotorcycleLicence='" + HaveMotorcycleLicence + '\'' +
                ", PartTimeJob='" + PartTimeJob + '\'' +
                ", FirstName='" + FirstName + '\'' +
                ", LastName='" + LastName + '\'' +
                ", CUSLevel='" + CUSLevel + '\'' +
                ", AppRgtTpye='" + AppRgtTpye + '\'' +
                ", TINNO='" + TINNO + '\'' +
                ", TINFlag='" + TINFlag + '\'' +
                '}';
    }
}
