package com.sinosoft.cloud.access.entity;

/**
 * 非实时出单申请NB007的请求报文
 */
public class ReqNotRealNewContnoApply {

    private String BankCode;
    private String ZoneNo;
    private String BranchNo;
    //投保单号
    private String PolicyApplyNo;
    //险种代码
    private String RiskCode;
    //产品代码
    private String ProdCode;
    //贷款合同号
    private String LoanContact;
    //手续费费率
    private String SpecialRate;
    //保费
    private String Prem;
    //账号
    private String AccNo;
    //柜员代码
    private String TellerNo;
    private AppInfo AppInfo;

    public String getBankCode() {
        return BankCode;
    }

    public void setBankCode(String bankCode) {
        BankCode = bankCode;
    }

    public String getZoneNo() {
        return ZoneNo;
    }

    public void setZoneNo(String zoneNo) {
        ZoneNo = zoneNo;
    }

    public String getBranchNo() {
        return BranchNo;
    }

    public void setBranchNo(String branchNo) {
        BranchNo = branchNo;
    }

    public String getTellerNo() {
        return TellerNo;
    }

    public void setTellerNo(String tellerNo) {
        TellerNo = tellerNo;
    }

    public String getPolicyApplyNo() {
        return PolicyApplyNo;
    }

    public void setPolicyApplyNo(String policyApplyNo) {
        PolicyApplyNo = policyApplyNo;
    }

    public String getRiskCode() {
        return RiskCode;
    }

    public void setRiskCode(String riskCode) {
        RiskCode = riskCode;
    }

    public String getProdCode() {
        return ProdCode;
    }

    public void setProdCode(String prodCode) {
        ProdCode = prodCode;
    }

    public String getLoanContact() {
        return LoanContact;
    }

    public void setLoanContact(String loanContact) {
        LoanContact = loanContact;
    }

    public String getSpecialRate() {
        return SpecialRate;
    }

    public void setSpecialRate(String specialRate) {
        SpecialRate = specialRate;
    }

    public String getPrem() {
        return Prem;
    }

    public void setPrem(String prem) {
        Prem = prem;
    }

    public String getAccNo() {
        return AccNo;
    }

    public void setAccNo(String accNo) {
        AccNo = accNo;
    }

    public AppInfo getAppInfo() {
        return AppInfo;
    }

    public void setAppInfo(AppInfo appInfo) {
        AppInfo = appInfo;
    }

    @Override
    public String toString() {
        return "ReqNotRealNewContnoApply{" +
                "PolicyApplyNo='" + PolicyApplyNo + '\'' +
                ", RiskCode='" + RiskCode + '\'' +
                ", ProdCode='" + ProdCode + '\'' +
                ", LoanContact='" + LoanContact + '\'' +
                ", SpecialRate='" + SpecialRate + '\'' +
                ", Prem='" + Prem + '\'' +
                ", AccNo='" + AccNo + '\'' +
                ", TellerNo='" + TellerNo + '\'' +
                ", BankCode='" + BankCode + '\'' +
                ", ZoneNo='" + ZoneNo + '\'' +
                ", BranchNo='" + BranchNo + '\'' +
                ", AppInfo=" + AppInfo +
                '}';
    }
}