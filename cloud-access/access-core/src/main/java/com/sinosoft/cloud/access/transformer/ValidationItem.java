package com.sinosoft.cloud.access.transformer;

/**
 * cloud-access
 *
 * @title: cloud-access
 * @package: com.sinosoft.cloud.access.transformer
 * @author: yangming
 * @date: 2017/11/15 下午6:26
 */
public class ValidationItem {

    private String rule;
    private String msg;

    public String getRule() {
        return rule;
    }

    public void setRule(String rule) {
        this.rule = rule;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    @Override
    public String toString() {
        return "ValidationItem{" +
                "rule='" + rule + '\'' +
                ", msg='" + msg + '\'' +
                '}';
    }
}
