package com.sinosoft.cloud.access.entity;

/**
 * @Author:lizhen
 * @Description:
 * @Date:Created in 10:26 2018/12/20
 * @Modified By:
 */
/**
 * 4010保单贷款清偿申请POS096
 */
public class ReqPolicyLoanClearApplyWebSite {
    private String EdorType;
    private String ContNo;
    private String LoanMoney;

    public String getEdorType() {
        return EdorType;
    }

    public void setEdorType(String edorType) {
        EdorType = edorType;
    }

    public String getContNo() {
        return ContNo;
    }

    public void setContNo(String contNo) {
        ContNo = contNo;
    }

    public String getLoanMoney() {
        return LoanMoney;
    }

    public void setLoanMoney(String loanMoney) {
        LoanMoney = loanMoney;
    }

    @Override
    public String toString() {
        return "ReqPolicyLoanClearApplyWebSite{" +
                "EdorType='" + EdorType + '\'' +
                ", ContNo='" + ContNo + '\'' +
                ", LoanMoney='" + LoanMoney + '\'' +
                '}';
    }
}