package com.sinosoft.cloud.access.transformer.function;

import net.sf.saxon.s9api.*;

import static com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.*;
import static com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.checkReserve4ByInsuredAgeTo18;

/**
 * Created by Jarod on 2016/12/3.
 */
public class SaxFunction {

    public static ExtensionFunction[] getFunctions() {

        ExtensionFunction getBNFAddress = new ExtensionFunction() {
            @Override
            public QName getName() {
                return new QName("http://xml.apache.org/xslt/java", "com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.getBNFAddress");
            }

            @Override
            public SequenceType getResultType() {
                return SequenceType.makeSequenceType(ItemType.STRING, OccurrenceIndicator.ONE);
            }

            @Override
            public SequenceType[] getArgumentTypes() {
                return new SequenceType[]
                        {
                                SequenceType.makeSequenceType(ItemType.STRING, OccurrenceIndicator.ONE)
                        };
            }

            @Override
            public XdmValue call(XdmValue[] arguments) throws SaxonApiException {
                String result = "";
                if (((XdmAtomicValue) arguments[0].itemAt(0)) != null) {
                    String arg0 = ((XdmAtomicValue) arguments[0].itemAt(0)).getStringValue();
                    result = getBNFAddress(arg0);
                    return new XdmAtomicValue(result);
                } else {
                    return new XdmAtomicValue(result);
                }
            }
        };
        ExtensionFunction getMIPAddress = new ExtensionFunction() {
            @Override
            public QName getName() {
                return new QName("http://xml.apache.org/xslt/java", "com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.getMIPAddress");
            }

            @Override
            public SequenceType getResultType() {
                return SequenceType.makeSequenceType(ItemType.STRING, OccurrenceIndicator.ONE);
            }

            @Override
            public SequenceType[] getArgumentTypes() {
                return new SequenceType[]
                        {
                                SequenceType.makeSequenceType(ItemType.STRING, OccurrenceIndicator.ONE)
                        };
            }

            @Override
            public XdmValue call(XdmValue[] arguments) throws SaxonApiException {
                String result = "";
                if (((XdmAtomicValue) arguments[0].itemAt(0)) != null) {
                    String arg0 = ((XdmAtomicValue) arguments[0].itemAt(0)).getStringValue();
                    result = getMIPAddress(arg0);
                    return new XdmAtomicValue(result);
                } else {
                    return new XdmAtomicValue(result);
                }
            }
        };
        ExtensionFunction getMIPStoreNo = new ExtensionFunction() {
            @Override
            public QName getName() {
                return new QName("http://xml.apache.org/xslt/java", "com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.getMIPStoreNo");
            }

            @Override
            public SequenceType getResultType() {
                return SequenceType.makeSequenceType(ItemType.STRING, OccurrenceIndicator.ONE);
            }

            @Override
            public SequenceType[] getArgumentTypes() {
                return new SequenceType[]
                        {
                                SequenceType.makeSequenceType(ItemType.STRING, OccurrenceIndicator.ONE)
                        };
            }

            @Override
            public XdmValue call(XdmValue[] arguments) throws SaxonApiException {
                String result = "";
                if (((XdmAtomicValue) arguments[0].itemAt(0)) != null) {
                    String arg0 = ((XdmAtomicValue) arguments[0].itemAt(0)).getStringValue();
                    result = getMIPStoreNo(arg0);
                    return new XdmAtomicValue(result);
                } else {
                    return new XdmAtomicValue(result);
                }
            }
        };

        ExtensionFunction getbnfAddress = new ExtensionFunction() {
            @Override
            public QName getName() {
                return new QName("http://xml.apache.org/xslt/java", "com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.getbnfAddress");
            }

            @Override
            public SequenceType getResultType() {
                return SequenceType.makeSequenceType(ItemType.STRING, OccurrenceIndicator.ONE);
            }

            @Override
            public SequenceType[] getArgumentTypes() {
                return new SequenceType[]
                        {
                                SequenceType.makeSequenceType(ItemType.STRING, OccurrenceIndicator.ONE),
                                SequenceType.makeSequenceType(ItemType.STRING, OccurrenceIndicator.ONE),
                                SequenceType.makeSequenceType(ItemType.STRING, OccurrenceIndicator.ONE),
                                SequenceType.makeSequenceType(ItemType.STRING, OccurrenceIndicator.ONE),
                                SequenceType.makeSequenceType(ItemType.STRING, OccurrenceIndicator.ONE)

                        };
            }

            @Override
            public XdmValue call(XdmValue[] arguments) throws SaxonApiException {
                String result = "";
                if (((XdmAtomicValue) arguments[0].itemAt(0)) != null) {
                    String arg0 = ((XdmAtomicValue) arguments[0].itemAt(0)).getStringValue();
                    String arg1 = ((XdmAtomicValue) arguments[1].itemAt(0)).getStringValue();
                    String arg2 = ((XdmAtomicValue) arguments[2].itemAt(0)).getStringValue();
                    String arg3 = ((XdmAtomicValue) arguments[3].itemAt(0)).getStringValue();
                    String arg4 = ((XdmAtomicValue) arguments[4].itemAt(0)).getStringValue();
                    result = getbnfAddress(arg0, arg1, arg2, arg3, arg4);
                    return new XdmAtomicValue(result);
                } else {
                    return new XdmAtomicValue(result);
                }
            }
        };

        ExtensionFunction getZoneCode = new ExtensionFunction() {
            @Override
            public QName getName() {
                return new QName("http://xml.apache.org/xslt/java", "com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.getZoneCode");
            }

            @Override
            public SequenceType getResultType() {
                return SequenceType.makeSequenceType(ItemType.STRING, OccurrenceIndicator.ONE);
            }

            @Override
            public SequenceType[] getArgumentTypes() {
                return new SequenceType[]
                        {
                                SequenceType.makeSequenceType(ItemType.STRING, OccurrenceIndicator.ONE)
                        };
            }

            @Override
            public XdmValue call(XdmValue[] arguments) throws SaxonApiException {
                String result = "";
                if (((XdmAtomicValue) arguments[0].itemAt(0)) != null) {
                    String arg0 = ((XdmAtomicValue) arguments[0].itemAt(0)).getStringValue();
                    result = getZoneCode(arg0);
                    return new XdmAtomicValue(result);
                } else {
                    return new XdmAtomicValue(result);
                }
            }
        };

        ExtensionFunction getStoreNo = new ExtensionFunction() {
            @Override
            public QName getName() {
                return new QName("http://xml.apache.org/xslt/java", "com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.getStoreNo");
            }

            @Override
            public SequenceType getResultType() {
                return SequenceType.makeSequenceType(ItemType.STRING, OccurrenceIndicator.ONE);
            }

            @Override
            public SequenceType[] getArgumentTypes() {
                return new SequenceType[]
                        {
                                SequenceType.makeSequenceType(ItemType.STRING, OccurrenceIndicator.ONE)
                        };
            }

            @Override
            public XdmValue call(XdmValue[] arguments) throws SaxonApiException {
                String result = "";
                if (((XdmAtomicValue) arguments[0].itemAt(0)) != null) {
                    String arg0 = ((XdmAtomicValue) arguments[0].itemAt(0)).getStringValue();
                    result = getStoreNo(arg0);
                    return new XdmAtomicValue(result);
                } else {
                    return new XdmAtomicValue(result);
                }
            }
        };

        ExtensionFunction getAddress = new ExtensionFunction() {
            @Override
            public QName getName() {
                return new QName("http://xml.apache.org/xslt/java", "com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.getAddress");
            }

            @Override
            public SequenceType getResultType() {
                return SequenceType.makeSequenceType(ItemType.STRING, OccurrenceIndicator.ONE);
            }

            @Override
            public SequenceType[] getArgumentTypes() {
                return new SequenceType[]
                        {
                                SequenceType.makeSequenceType(ItemType.STRING, OccurrenceIndicator.ONE)
                        };
            }

            @Override
            public XdmValue call(XdmValue[] arguments) throws SaxonApiException {
                String result = "";
                if (((XdmAtomicValue) arguments[0].itemAt(0)) != null) {
                    String arg0 = ((XdmAtomicValue) arguments[0].itemAt(0)).getStringValue();
                    result = getAddress(arg0);
                    return new XdmAtomicValue(result);
                } else {
                    return new XdmAtomicValue(result);
                }
            }
        };


        ExtensionFunction getFenMoney = new ExtensionFunction() {
            @Override
            public QName getName() {
                return new QName("http://xml.apache.org/xslt/java", "com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.getFenMoney");
            }

            @Override
            public SequenceType getResultType() {
                return SequenceType.makeSequenceType(ItemType.STRING, OccurrenceIndicator.ONE);
            }

            @Override
            public SequenceType[] getArgumentTypes() {
                return new SequenceType[]
                        {
                                SequenceType.makeSequenceType(ItemType.STRING, OccurrenceIndicator.ONE)
                        };
            }

            @Override
            public XdmValue call(XdmValue[] arguments) throws SaxonApiException {
                String result = "";
                if (((XdmAtomicValue) arguments[0].itemAt(0)) != null) {
                    String arg0 = ((XdmAtomicValue) arguments[0].itemAt(0)).getStringValue();
                    result = getFenMoney(arg0);
                    return new XdmAtomicValue(result);
                } else {
                    return new XdmAtomicValue(result);
                }
            }
        };

        ExtensionFunction getYuanMoney = new ExtensionFunction() {
            @Override
            public QName getName() {
                return new QName("http://xml.apache.org/xslt/java", "com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.getYuanMoney");
            }

            @Override
            public SequenceType getResultType() {
                return SequenceType.makeSequenceType(ItemType.STRING, OccurrenceIndicator.ONE);
            }

            @Override
            public SequenceType[] getArgumentTypes() {
                return new SequenceType[]
                        {
                                SequenceType.makeSequenceType(ItemType.STRING, OccurrenceIndicator.ONE)
                        };
            }

            @Override
            public XdmValue call(XdmValue[] arguments) throws SaxonApiException {
                String result = "";
                if (((XdmAtomicValue) arguments[0].itemAt(0)) != null) {
                    String arg0 = ((XdmAtomicValue) arguments[0].itemAt(0)).getStringValue();
                    result = getYuanMoney(arg0);
                    return new XdmAtomicValue(result);
                } else {
                    return new XdmAtomicValue(result);
                }
            }
        };


        ExtensionFunction getDateTimeNowAddOrReduce = new ExtensionFunction() {
            @Override
            public QName getName() {
                return new QName("http://xml.apache.org/xslt/java", "com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.getDateTimeNowAddOrReduce");
            }

            @Override
            public SequenceType getResultType() {
                return SequenceType.makeSequenceType(ItemType.STRING, OccurrenceIndicator.ONE);
            }

            @Override
            public SequenceType[] getArgumentTypes() {
                return new SequenceType[]
                        {
                                SequenceType.makeSequenceType(ItemType.STRING, OccurrenceIndicator.ONE),
                                SequenceType.makeSequenceType(ItemType.STRING, OccurrenceIndicator.ONE)
                        };
            }

            @Override
            public XdmValue call(XdmValue[] arguments) throws SaxonApiException {
                String result = "";
                if (((XdmAtomicValue) arguments[0].itemAt(0)) != null && ((XdmAtomicValue) arguments[1].itemAt(0)) != null) {
                    String arg0 = ((XdmAtomicValue) arguments[0].itemAt(0)).getStringValue();
                    String arg1 = ((XdmAtomicValue) arguments[1].itemAt(0)).getStringValue();
                    result = getDateTimeNowAddOrReduce(arg0, arg1);
                    return new XdmAtomicValue(result);
                } else {
                    return new XdmAtomicValue(result);
                }
            }
        };


        ExtensionFunction transOutHeadTransNo = new ExtensionFunction() {
            @Override
            public QName getName() {
                return new QName("http://xml.apache.org/xslt/java", "com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.transOutHeadTransNo");
            }

            @Override
            public SequenceType getResultType() {
                return SequenceType.makeSequenceType(ItemType.STRING, OccurrenceIndicator.ONE);
            }

            @Override
            public SequenceType[] getArgumentTypes() {
                return new SequenceType[]
                        {
                                SequenceType.makeSequenceType(ItemType.STRING, OccurrenceIndicator.ONE)
                        };
            }

            @Override
            public XdmValue call(XdmValue[] arguments) throws SaxonApiException {
                String result = "";
                if (((XdmAtomicValue) arguments[0].itemAt(0)) != null) {
                    String arg0 = ((XdmAtomicValue) arguments[0].itemAt(0)).getStringValue();
                    result = transOutHeadTransNo(arg0);
                    return new XdmAtomicValue(result);
                } else {
                    return new XdmAtomicValue(result);
                }
            }
        };

        ExtensionFunction calculationDays = new ExtensionFunction() {
            @Override
            public QName getName() {
                return new QName("http://xml.apache.org/xslt/java", "com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.calculationDays");
            }

            @Override
            public SequenceType getResultType() {
                return SequenceType.makeSequenceType(ItemType.STRING, OccurrenceIndicator.ONE);
            }

            @Override
            public SequenceType[] getArgumentTypes() {
                return new SequenceType[]
                        {
                                SequenceType.makeSequenceType(ItemType.STRING, OccurrenceIndicator.ONE),
                                SequenceType.makeSequenceType(ItemType.STRING, OccurrenceIndicator.ONE),
                                SequenceType.makeSequenceType(ItemType.STRING, OccurrenceIndicator.ONE)
                        };
            }

            @Override
            public XdmValue call(XdmValue[] arguments) throws SaxonApiException {
                String result = "";
                if (((XdmAtomicValue) arguments[0].itemAt(0)) != null
                        && ((XdmAtomicValue) arguments[1].itemAt(0)) != null
                        && ((XdmAtomicValue) arguments[2].itemAt(0)) != null) {
                    String arg0 = ((XdmAtomicValue) arguments[0].itemAt(0)).getStringValue();
                    String arg1 = ((XdmAtomicValue) arguments[1].itemAt(0)).getStringValue();
                    String arg2 = ((XdmAtomicValue) arguments[2].itemAt(0)).getStringValue();
                    result = calculationDays(arg0, arg1, arg2);
                    return new XdmAtomicValue(result);
                } else {
                    return new XdmAtomicValue(result);
                }
            }
        };

        ExtensionFunction checkRiskCode = new ExtensionFunction() {
            @Override
            public QName getName() {
                return new QName("http://xml.apache.org/xslt/java", "com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.checkRiskCode");
            }

            @Override
            public SequenceType getResultType() {
                return SequenceType.makeSequenceType(ItemType.STRING, OccurrenceIndicator.ONE);
            }

            @Override
            public SequenceType[] getArgumentTypes() {
                return new SequenceType[]
                        {
                                SequenceType.makeSequenceType(ItemType.STRING, OccurrenceIndicator.ONE)
                        };
            }

            @Override
            public XdmValue call(XdmValue[] arguments) throws SaxonApiException {
                String result = "";
                if (((XdmAtomicValue) arguments[0].itemAt(0)) != null) {
                    String arg0 = ((XdmAtomicValue) arguments[0].itemAt(0)).getStringValue();
                    result = checkRiskCode(arg0);
                    return new XdmAtomicValue(result);
                } else {
                    return new XdmAtomicValue(result);
                }
            }
        };


        ExtensionFunction getThisDateAfterDay = new ExtensionFunction() {
            @Override
            public QName getName() {
                return new QName("http://xml.apache.org/xslt/java", "com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.getThisDateAfterDay");
            }

            @Override
            public SequenceType getResultType() {
                return SequenceType.makeSequenceType(ItemType.STRING, OccurrenceIndicator.ONE);
            }

            @Override
            public SequenceType[] getArgumentTypes() {
                return new SequenceType[]
                        {
                                SequenceType.makeSequenceType(ItemType.STRING, OccurrenceIndicator.ONE),
                                SequenceType.makeSequenceType(ItemType.STRING, OccurrenceIndicator.ONE),
                                SequenceType.makeSequenceType(ItemType.STRING, OccurrenceIndicator.ONE)
                        };
            }

            @Override
            public XdmValue call(XdmValue[] arguments) throws SaxonApiException {
                String result = "";
                if (((XdmAtomicValue) arguments[0].itemAt(0)) != null && ((XdmAtomicValue) arguments[1].itemAt(0)) != null && ((XdmAtomicValue) arguments[2].itemAt(0)) != null) {
                    String arg0 = ((XdmAtomicValue) arguments[0].itemAt(0)).getStringValue();
                    String arg1 = ((XdmAtomicValue) arguments[1].itemAt(0)).getStringValue();
                    String arg2 = ((XdmAtomicValue) arguments[2].itemAt(0)).getStringValue();
                    result = getThisDateAfterDay(arg0, arg1, arg2);
                    return new XdmAtomicValue(result);
                } else {
                    return new XdmAtomicValue(result);
                }
            }
        };


        ExtensionFunction getDateTimeNow = new ExtensionFunction() {
            @Override
            public QName getName() {
                return new QName("http://xml.apache.org/xslt/java", "com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.getDateTimeNow");
            }

            @Override
            public SequenceType getResultType() {
                return SequenceType.makeSequenceType(ItemType.STRING, OccurrenceIndicator.ONE);
            }

            @Override
            public SequenceType[] getArgumentTypes() {
                return new SequenceType[]
                        {
                        };
            }

            @Override
            public XdmValue call(XdmValue[] arguments) throws SaxonApiException {
                String result = "";
                result = String.valueOf(getDateTimeNow());
                return new XdmAtomicValue(result);
            }
        };

        ExtensionFunction yuanToFen = new ExtensionFunction() {
            @Override
            public QName getName() {
                return new QName("http://xml.apache.org/xslt/java", "com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.yuanToFen");
            }

            @Override
            public SequenceType getResultType() {
                return SequenceType.makeSequenceType(ItemType.STRING, OccurrenceIndicator.ONE);
            }

            @Override
            public SequenceType[] getArgumentTypes() {
                return new SequenceType[]
                        {
                                SequenceType.makeSequenceType(ItemType.STRING, OccurrenceIndicator.ONE)
                        };
            }

            @Override
            public XdmValue call(XdmValue[] arguments) throws SaxonApiException {
                String result = "";
                if (((XdmAtomicValue) arguments[0].itemAt(0)) != null) {
                    String arg = ((XdmAtomicValue) arguments[0].itemAt(0)).getStringValue();
                    result = String.valueOf(yuanToFen(arg));
                    return new XdmAtomicValue(result);
                } else {
                    return new XdmAtomicValue(result);
                }
            }
        };

        ExtensionFunction fenToYuan = new ExtensionFunction() {
            @Override
            public QName getName() {
                return new QName("http://xml.apache.org/xslt/java", "com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.fenToYuan");
            }

            @Override
            public SequenceType getResultType() {
                return SequenceType.makeSequenceType(ItemType.STRING, OccurrenceIndicator.ONE);
            }

            @Override
            public SequenceType[] getArgumentTypes() {
                return new SequenceType[]
                        {
                                SequenceType.makeSequenceType(ItemType.STRING, OccurrenceIndicator.ZERO_OR_ONE)
                        };
            }

            @Override
            public XdmValue call(XdmValue[] arguments) throws SaxonApiException {
                String result = "";
                if (arguments.length > 0
                        && arguments[0].size() > 0
                        && ((XdmAtomicValue) arguments[0].itemAt(0)) != null) {
                    String arg = ((XdmAtomicValue) arguments[0].itemAt(0)).getStringValue();
                    result = String.valueOf(fenToYuan(arg));
                    return new XdmAtomicValue(result);
                } else {
                    return new XdmAtomicValue(result);
                }
            }
        };

        ExtensionFunction fillStrWith_2Args = new ExtensionFunction() {
            @Override
            public QName getName() {
                return new QName("http://xml.apache.org/xslt/java", "com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.fillStrWith_2Args");
            }

            @Override
            public SequenceType getResultType() {
                return SequenceType.makeSequenceType(ItemType.STRING, OccurrenceIndicator.ONE);
            }

            @Override
            public SequenceType[] getArgumentTypes() {
                return new SequenceType[]
                        {
                                SequenceType.makeSequenceType(ItemType.STRING, OccurrenceIndicator.ONE)
                                , SequenceType.makeSequenceType(ItemType.INTEGER, OccurrenceIndicator.ONE)
                        };
            }

            @Override
            public XdmValue call(XdmValue[] arguments) throws SaxonApiException {
                String result = "";
                if (((XdmAtomicValue) arguments[0].itemAt(0)) != null && ((XdmAtomicValue) arguments[1].itemAt(0)) != null) {
                    String arg0 = ((XdmAtomicValue) arguments[0].itemAt(0)).getStringValue();
                    Integer arg1 = Integer.valueOf(((XdmAtomicValue) arguments[1].itemAt(0)).getStringValue());
                    result = fillStrWith_2Args(arg0, arg1);
                    return new XdmAtomicValue(result);
                } else {
                    return new XdmAtomicValue(result);
                }
            }
        };

        ExtensionFunction fillStrWith_3Args = new ExtensionFunction() {
            @Override
            public QName getName() {
                return new QName("http://xml.apache.org/xslt/java", "com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.fillStrWith_");
            }

            @Override
            public SequenceType getResultType() {
                return SequenceType.makeSequenceType(ItemType.STRING, OccurrenceIndicator.ONE);
            }

            @Override
            public SequenceType[] getArgumentTypes() {
                return new SequenceType[]
                        {
                                SequenceType.makeSequenceType(ItemType.STRING, OccurrenceIndicator.ONE)
                                , SequenceType.makeSequenceType(ItemType.INTEGER, OccurrenceIndicator.ONE)
                                , SequenceType.makeSequenceType(ItemType.BOOLEAN, OccurrenceIndicator.ONE)
                        };
            }

            @Override
            public XdmValue call(XdmValue[] arguments) throws SaxonApiException {
                String result = "";
                if (((XdmAtomicValue) arguments[0].itemAt(0)) != null && ((XdmAtomicValue) arguments[1].itemAt(0)) != null) {
                    String arg0 = ((XdmAtomicValue) arguments[0].itemAt(0)).getStringValue();
                    Integer arg1 = Integer.valueOf(((XdmAtomicValue) arguments[1].itemAt(0)).getStringValue());
                    Boolean arg2 = Boolean.valueOf(((XdmAtomicValue) arguments[2].itemAt(0)).getStringValue());
                    if (arg2 == null) {
                        arg2 = true;
                    }
                    result = fillStrWith_(arg0, arg1, arg2);
                    return new XdmAtomicValue(result);
                } else {
                    return new XdmAtomicValue(result);
                }
            }
        };

        ExtensionFunction date8to10 = new ExtensionFunction() {
            @Override
            public QName getName() {
                return new QName("http://xml.apache.org/xslt/java", "com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.date8to10");
            }

            @Override
            public SequenceType getResultType() {
                return SequenceType.makeSequenceType(ItemType.STRING, OccurrenceIndicator.ONE);
            }

            @Override
            public SequenceType[] getArgumentTypes() {
                return new SequenceType[]
                        {
                                SequenceType.makeSequenceType(ItemType.STRING, OccurrenceIndicator.ONE)
                        };
            }

            @Override
            public XdmValue call(XdmValue[] arguments) throws SaxonApiException {
                String result = "";
                if (((XdmAtomicValue) arguments[0].itemAt(0)) != null) {
                    String arg0 = ((XdmAtomicValue) arguments[0].itemAt(0)).getStringValue();
                    result = date8to10(arg0);
                    return new XdmAtomicValue(result);
                } else {
                    return new XdmAtomicValue(result);
                }
            }
        };

        ExtensionFunction date10to8 = new ExtensionFunction() {
            @Override
            public QName getName() {
                return new QName("http://xml.apache.org/xslt/java", "com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.date10to8");
            }

            @Override
            public SequenceType getResultType() {
                return SequenceType.makeSequenceType(ItemType.STRING, OccurrenceIndicator.ONE);
            }

            @Override
            public SequenceType[] getArgumentTypes() {
                return new SequenceType[]
                        {
                                SequenceType.makeSequenceType(ItemType.STRING, OccurrenceIndicator.ZERO_OR_ONE)
                        };
            }

            @Override
            public XdmValue call(XdmValue[] arguments) throws SaxonApiException {
                String result = "";
                if (!((arguments[0]) instanceof XdmEmptySequence) && ((XdmAtomicValue) arguments[0].itemAt(0)) != null) {
                    String arg0 = ((XdmAtomicValue) arguments[0].itemAt(0)).getStringValue();
                    result = date10to8(arg0);
                    return new XdmAtomicValue(result);
                } else {
                    return new XdmAtomicValue(result);
                }
            }
        };

        ExtensionFunction parsBoolean = new ExtensionFunction() {
            @Override
            public QName getName() {
                return new QName("http://xml.apache.org/xslt/java", "com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.parseBoolean");
            }

            @Override
            public SequenceType getResultType() {
                return SequenceType.makeSequenceType(ItemType.BOOLEAN, OccurrenceIndicator.ONE);
            }

            @Override
            public SequenceType[] getArgumentTypes() {
                return new SequenceType[]
                        {
                                SequenceType.makeSequenceType(ItemType.STRING, OccurrenceIndicator.ONE)
                        };
            }

            @Override
            public XdmValue call(XdmValue[] arguments) throws SaxonApiException {
                Boolean result = null;
                if (((XdmAtomicValue) arguments[0].itemAt(0)) != null) {
                    String arg = ((XdmAtomicValue) arguments[0].itemAt(0)).getStringValue();
                    result = parseBoolean(arg);
                    return new XdmAtomicValue(result);
                } else {
                    return new XdmAtomicValue(result);
                }
            }
        };

        ExtensionFunction appntIncome = new ExtensionFunction() {
            @Override
            public QName getName() {
                return new QName("http://xml.apache.org/xslt/java", "com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.appntIncomeFormat");
            }

            @Override
            public SequenceType getResultType() {
                return SequenceType.makeSequenceType(ItemType.STRING, OccurrenceIndicator.ONE);
            }

            @Override
            public SequenceType[] getArgumentTypes() {
                return new SequenceType[]
                        {
                                SequenceType.makeSequenceType(ItemType.STRING, OccurrenceIndicator.ONE)
                                , SequenceType.makeSequenceType(ItemType.STRING, OccurrenceIndicator.ONE)
                        };
            }

            @Override
            public XdmValue call(XdmValue[] arguments) throws SaxonApiException {
                String result = "";
                if (((XdmAtomicValue) arguments[0].itemAt(0)) != null && ((XdmAtomicValue) arguments[1].itemAt(0)) != null) {
                    String arg0 = ((XdmAtomicValue) arguments[0].itemAt(0)).getStringValue();
                    String arg1 = ((XdmAtomicValue) arguments[1].itemAt(0)).getStringValue();
                    if (null == arg1 || "".equals(arg1)) {
                        arg1 = "";
                    }
                    try {
                        result = appntIncomeFormat(arg0, arg1);
                    } catch (NumberFormatException e) {
                        result = arg1;
                        e.printStackTrace();
                    }
                    return new XdmAtomicValue(result);
                } else {
                    return new XdmAtomicValue(result);
                }
            }
        };

        ExtensionFunction insuredIncome = new ExtensionFunction() {
            @Override
            public QName getName() {
                return new QName("http://xml.apache.org/xslt/java", "com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.insuredIncomeFormat");
            }

            @Override
            public SequenceType getResultType() {
                return SequenceType.makeSequenceType(ItemType.STRING, OccurrenceIndicator.ONE);
            }

            @Override
            public SequenceType[] getArgumentTypes() {
                return new SequenceType[]
                        {
                                SequenceType.makeSequenceType(ItemType.STRING, OccurrenceIndicator.ONE)
                                , SequenceType.makeSequenceType(ItemType.STRING, OccurrenceIndicator.ONE)
                        };
            }

            @Override
            public XdmValue call(XdmValue[] arguments) throws SaxonApiException {
                String result = "";
                if (((XdmAtomicValue) arguments[0].itemAt(0)) != null && ((XdmAtomicValue) arguments[1].itemAt(0)) != null) {
                    String arg0 = ((XdmAtomicValue) arguments[0].itemAt(0)).getStringValue();
                    String arg1 = ((XdmAtomicValue) arguments[1].itemAt(0)).getStringValue();
                    if (null == arg1 || "".equals(arg1)) {
                        arg1 = "";
                    }
                    result = insuredIncomeFormat(arg0, arg1);
                    return new XdmAtomicValue(result);
                } else {
                    return new XdmAtomicValue(result);
                }
            }
        };

        ExtensionFunction appntFamilyIncome = new ExtensionFunction() {
            @Override
            public QName getName() {
                return new QName("http://xml.apache.org/xslt/java", "com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.appntFamilyIncomeFormat");
            }

            @Override
            public SequenceType getResultType() {
                return SequenceType.makeSequenceType(ItemType.STRING, OccurrenceIndicator.ONE);
            }

            @Override
            public SequenceType[] getArgumentTypes() {
                return new SequenceType[]
                        {
                                SequenceType.makeSequenceType(ItemType.STRING, OccurrenceIndicator.ONE)
                        };
            }

            @Override
            public XdmValue call(XdmValue[] arguments) throws SaxonApiException {
                String result = "";
                if (((XdmAtomicValue) arguments[0].itemAt(0)) != null) {
                    String arg0 = ((XdmAtomicValue) arguments[0].itemAt(0)).getStringValue();
                    result = appntFamilyIncomeFormat(arg0);
                    return new XdmAtomicValue(result);
                } else {
                    return new XdmAtomicValue(result);
                }
            }
        };

        ExtensionFunction fixedLengthString = new ExtensionFunction() {
            @Override
            public QName getName() {
                return new QName("http://xml.apache.org/xslt/java", "com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.fixedLengthString");
            }

            @Override
            public SequenceType getResultType() {
                return SequenceType.makeSequenceType(ItemType.STRING, OccurrenceIndicator.ONE);
            }

            @Override
            public SequenceType[] getArgumentTypes() {
                return new SequenceType[]
                        {
                                SequenceType.makeSequenceType(ItemType.STRING, OccurrenceIndicator.ONE),
                                SequenceType.makeSequenceType(ItemType.STRING, OccurrenceIndicator.ONE)
                        };
            }

            @Override
            public XdmValue call(XdmValue[] arguments) throws SaxonApiException {
                String result = "";
                if (((XdmAtomicValue) arguments[0].itemAt(0)) != null) {
                    String arg = ((XdmAtomicValue) arguments[0].itemAt(0)).getStringValue();
                    String arg2 = ((XdmAtomicValue) arguments[1].itemAt(0)).getStringValue();
                    result = String.valueOf(fixedLengthString(arg, arg2));
                    return new XdmAtomicValue(result);
                } else {
                    return new XdmAtomicValue(result);
                }
            }
        };

        ExtensionFunction timeFormat = new ExtensionFunction() {
            @Override
            public QName getName() {
                return new QName("http://xml.apache.org/xslt/java", "com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.timeFormat");
            }

            @Override
            public SequenceType getResultType() {
                return SequenceType.makeSequenceType(ItemType.STRING, OccurrenceIndicator.ONE);
            }

            @Override
            public SequenceType[] getArgumentTypes() {
                return new SequenceType[]
                        {
                                SequenceType.makeSequenceType(ItemType.STRING, OccurrenceIndicator.ONE)
                        };
            }

            @Override
            public XdmValue call(XdmValue[] arguments) throws SaxonApiException {
                String result = "";
                if (((XdmAtomicValue) arguments[0].itemAt(0)) != null) {
                    String arg = ((XdmAtomicValue) arguments[0].itemAt(0)).getStringValue();
                    result = String.valueOf(timeFormat(arg));
                    return new XdmAtomicValue(result);
                } else {
                    return new XdmAtomicValue(result);
                }
            }
        };

        ExtensionFunction getChnMoney = new ExtensionFunction() {
            @Override
            public QName getName() {
                return new QName("http://xml.apache.org/xslt/java", "com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.getChnMoney");
            }

            @Override
            public SequenceType getResultType() {
                return SequenceType.makeSequenceType(ItemType.STRING, OccurrenceIndicator.ONE);
            }

            @Override
            public SequenceType[] getArgumentTypes() {
                return new SequenceType[]
                        {
                                SequenceType.makeSequenceType(ItemType.STRING, OccurrenceIndicator.ONE)
                        };
            }

            @Override
            public XdmValue call(XdmValue[] arguments) throws SaxonApiException {
                String result = "";
                if (((XdmAtomicValue) arguments[0].itemAt(0)) != null) {
                    String arg = ((XdmAtomicValue) arguments[0].itemAt(0)).getStringValue();
                    result = String.valueOf(getChnMoney(arg));
                    return new XdmAtomicValue(result);
                } else {
                    return new XdmAtomicValue(result);
                }
            }
        };
        ExtensionFunction getCalDate = new ExtensionFunction() {
            @Override
            public QName getName() {
                return new QName("http://xml.apache.org/xslt/java", "com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.getCalDate");
            }

            @Override
            public SequenceType getResultType() {
                return SequenceType.makeSequenceType(ItemType.STRING, OccurrenceIndicator.ONE);
            }

            @Override
            public SequenceType[] getArgumentTypes() {
                return new SequenceType[]
                        {
                                SequenceType.makeSequenceType(ItemType.STRING, OccurrenceIndicator.ONE)
                        };
            }

            @Override
            public XdmValue call(XdmValue[] arguments) throws SaxonApiException {
                String result = "";
                if (((XdmAtomicValue) arguments[0].itemAt(0)) != null) {
                    String arg = ((XdmAtomicValue) arguments[0].itemAt(0)).getStringValue();
                    result = String.valueOf(getCalDate(arg));
                    return new XdmAtomicValue(result);
                } else {
                    return new XdmAtomicValue(result);
                }
            }
        };
        ExtensionFunction dateFormat = new ExtensionFunction() {
            @Override
            public QName getName() {
                return new QName("http://xml.apache.org/xslt/java", "com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.dateFormat");
            }

            @Override
            public SequenceType getResultType() {
                return SequenceType.makeSequenceType(ItemType.STRING, OccurrenceIndicator.ONE);
            }

            @Override
            public SequenceType[] getArgumentTypes() {
                return new SequenceType[]
                        {
                                SequenceType.makeSequenceType(ItemType.STRING, OccurrenceIndicator.ONE)
                        };
            }

            @Override
            public XdmValue call(XdmValue[] arguments) throws SaxonApiException {
                String result = "";
                if (((XdmAtomicValue) arguments[0].itemAt(0)) != null) {
                    String arg = ((XdmAtomicValue) arguments[0].itemAt(0)).getStringValue();
                    result = String.valueOf(dateFormat(arg));
                    return new XdmAtomicValue(result);
                } else {
                    return new XdmAtomicValue(result);
                }
            }
        };

        ExtensionFunction dateFormat8To10 = new ExtensionFunction() {
            @Override
            public QName getName() {
                return new QName("http://xml.apache.org/xslt/java", "com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.dateFormat8To10");
            }

            @Override
            public SequenceType getResultType() {
                return SequenceType.makeSequenceType(ItemType.STRING, OccurrenceIndicator.ONE);
            }

            @Override
            public SequenceType[] getArgumentTypes() {
                return new SequenceType[]
                        {
                                SequenceType.makeSequenceType(ItemType.STRING, OccurrenceIndicator.ZERO_OR_ONE)
                        };
            }

            @Override
            public XdmValue call(XdmValue[] arguments) throws SaxonApiException {
                String result = "";
                int argument = arguments[0].size();
//                XdmItem xdmItem = arguments[0].itemAt(0);
                if (arguments[0].size() == 0) {
                    return new XdmAtomicValue(result);
                }
                if (((XdmAtomicValue) arguments[0].itemAt(0)) != null) {
                    String arg = ((XdmAtomicValue) arguments[0].itemAt(0)).getStringValue();
                    result = String.valueOf(dateFormat8To10(arg));
                    return new XdmAtomicValue(result);
                } else {
                    return new XdmAtomicValue(result);
                }
            }
        };
        ExtensionFunction timeString10To8 = new ExtensionFunction() {
            @Override
            public QName getName() {
                return new QName("http://xml.apache.org/xslt/java", "com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.timeString10To8");
            }

            @Override
            public SequenceType getResultType() {
                return SequenceType.makeSequenceType(ItemType.STRING, OccurrenceIndicator.ONE);
            }

            @Override
            public SequenceType[] getArgumentTypes() {
                return new SequenceType[]
                        {
                                SequenceType.makeSequenceType(ItemType.STRING, OccurrenceIndicator.ONE)
                        };
            }

            @Override
            public XdmValue call(XdmValue[] arguments) throws SaxonApiException {
                String result = "";
                if (((XdmAtomicValue) arguments[0].itemAt(0)) != null) {
                    String arg = ((XdmAtomicValue) arguments[0].itemAt(0)).getStringValue();
                    result = String.valueOf(timeString10To8(arg));
                    return new XdmAtomicValue(result);
                } else {
                    return new XdmAtomicValue(result);
                }
            }
        };


        ExtensionFunction incomeFormat = new ExtensionFunction() {
            @Override
            public QName getName() {
                return new QName("http://xml.apache.org/xslt/java", "com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.incomeFormat");
            }

            @Override
            public SequenceType getResultType() {
                return SequenceType.makeSequenceType(ItemType.STRING, OccurrenceIndicator.ONE);
            }

            @Override
            public SequenceType[] getArgumentTypes() {
                return new SequenceType[]
                        {
                                SequenceType.makeSequenceType(ItemType.STRING, OccurrenceIndicator.ONE)
                                , SequenceType.makeSequenceType(ItemType.STRING, OccurrenceIndicator.ONE)
                                , SequenceType.makeSequenceType(ItemType.STRING, OccurrenceIndicator.ONE)
                        };
            }

            @Override
            public XdmValue call(XdmValue[] arguments) throws SaxonApiException {
                String result = "";
                if (((XdmAtomicValue) arguments[0].itemAt(0)) != null && ((XdmAtomicValue) arguments[1].itemAt(0)) != null) {
                    String arg0 = ((XdmAtomicValue) arguments[0].itemAt(0)).getStringValue();
                    String arg1 = ((XdmAtomicValue) arguments[1].itemAt(0)).getStringValue();
                    String arg2 = ((XdmAtomicValue) arguments[2].itemAt(0)).getStringValue();
                    result = incomeFormat(arg0, arg1, arg2);
                    return new XdmAtomicValue(result);
                } else {
                    return new XdmAtomicValue(result);
                }
            }
        };

        ExtensionFunction calStatureAndAvoirdupois = new ExtensionFunction() {
            @Override
            public QName getName() {
                return new QName("http://xml.apache.org/xslt/java", "com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.calStatureAndAvoirdupois");
            }

            @Override
            public SequenceType getResultType() {
                return SequenceType.makeSequenceType(ItemType.STRING, OccurrenceIndicator.ONE);
            }

            @Override
            public SequenceType[] getArgumentTypes() {
                return new SequenceType[]
                        {
                                SequenceType.makeSequenceType(ItemType.STRING, OccurrenceIndicator.ONE)
                                , SequenceType.makeSequenceType(ItemType.STRING, OccurrenceIndicator.ONE)
                        };
            }

            @Override
            public XdmValue call(XdmValue[] arguments) throws SaxonApiException {
                String result = "";
                if (((XdmAtomicValue) arguments[0].itemAt(0)) != null && ((XdmAtomicValue) arguments[1].itemAt(0)) != null) {
                    String arg0 = ((XdmAtomicValue) arguments[0].itemAt(0)).getStringValue();
                    String arg1 = ((XdmAtomicValue) arguments[1].itemAt(0)).getStringValue();
                    result = calStatureAndAvoirdupois(arg0, arg1);
                    return new XdmAtomicValue(result);
                } else {
                    return new XdmAtomicValue(result);
                }
            }
        };
        ExtensionFunction cValiDate = new ExtensionFunction() {
            @Override
            public QName getName() {
                return new QName("http://xml.apache.org/xslt/java", "com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.cValiDate");
            }

            @Override
            public SequenceType getResultType() {
                return SequenceType.makeSequenceType(ItemType.STRING, OccurrenceIndicator.ONE);
            }

            @Override
            public SequenceType[] getArgumentTypes() {
                return new SequenceType[]
                        {
                                SequenceType.makeSequenceType(ItemType.STRING, OccurrenceIndicator.ONE)
                        };
            }

            @Override
            public XdmValue call(XdmValue[] arguments) throws SaxonApiException {
                String result = "";
                if (((XdmAtomicValue) arguments[0].itemAt(0)) != null) {
                    String arg0 = ((XdmAtomicValue) arguments[0].itemAt(0)).getStringValue();
                    result = cValiDate(arg0);
                    return new XdmAtomicValue(result);
                } else {
                    return new XdmAtomicValue(result);
                }
            }
        };

        ExtensionFunction cValiDateOther = new ExtensionFunction() {
            @Override
            public QName getName() {
                return new QName("http://xml.apache.org/xslt/java", "com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.cValiDateOther");
            }

            @Override
            public SequenceType getResultType() {
                return SequenceType.makeSequenceType(ItemType.STRING, OccurrenceIndicator.ONE);
            }

            @Override
            public SequenceType[] getArgumentTypes() {
                return new SequenceType[]
                        {
                                SequenceType.makeSequenceType(ItemType.STRING, OccurrenceIndicator.ONE)
                        };
            }

            @Override
            public XdmValue call(XdmValue[] arguments) throws SaxonApiException {
                String result = "";
                if (((XdmAtomicValue) arguments[0].itemAt(0)) != null) {
                    String arg0 = ((XdmAtomicValue) arguments[0].itemAt(0)).getStringValue();
                    result = cValiDateOther(arg0);
                    return new XdmAtomicValue(result);
                } else {
                    return new XdmAtomicValue(result);
                }
            }
        };

        ExtensionFunction getCurrentDate = new ExtensionFunction() {
            @Override
            public QName getName() {
                return new QName("http://xml.apache.org/xslt/java", "com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.getCurrentDate");
            }

            @Override
            public SequenceType getResultType() {
                return SequenceType.makeSequenceType(ItemType.STRING, OccurrenceIndicator.ONE);
            }

            @Override
            public SequenceType[] getArgumentTypes() {
                return new SequenceType[]
                        {
                                SequenceType.makeSequenceType(ItemType.STRING, OccurrenceIndicator.ONE)
                        };
            }

            @Override
            public XdmValue call(XdmValue[] arguments) throws SaxonApiException {
                String result = "";
                if (((XdmAtomicValue) arguments[0].itemAt(0)) != null) {
                    String arg0 = ((XdmAtomicValue) arguments[0].itemAt(0)).getStringValue();
                    result = getCurrentDate(arg0);
                    return new XdmAtomicValue(result);
                } else {
                    return new XdmAtomicValue(result);
                }
            }
        };
        ExtensionFunction getContPlanCode = new ExtensionFunction() {
            @Override
            public QName getName() {
                return new QName("http://xml.apache.org/xslt/java", "com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.getContPlanCode");
            }

            @Override
            public SequenceType getResultType() {
                return SequenceType.makeSequenceType(ItemType.STRING, OccurrenceIndicator.ONE);
            }

            @Override
            public SequenceType[] getArgumentTypes() {
                return new SequenceType[]
                        {
                                SequenceType.makeSequenceType(ItemType.STRING, OccurrenceIndicator.ONE)
                                , SequenceType.makeSequenceType(ItemType.STRING, OccurrenceIndicator.ONE)
                                , SequenceType.makeSequenceType(ItemType.STRING, OccurrenceIndicator.ONE)
                                , SequenceType.makeSequenceType(ItemType.STRING, OccurrenceIndicator.ONE)
                                , SequenceType.makeSequenceType(ItemType.STRING, OccurrenceIndicator.ONE)

                        };
            }

            @Override
            public XdmValue call(XdmValue[] arguments) throws SaxonApiException {
                String result = "";
                if (((XdmAtomicValue) arguments[0].itemAt(0)) != null && ((XdmAtomicValue) arguments[1].itemAt(0)) != null) {
                    String arg0 = ((XdmAtomicValue) arguments[0].itemAt(0)).getStringValue();
                    String arg1 = ((XdmAtomicValue) arguments[1].itemAt(0)).getStringValue();
                    String arg2 = ((XdmAtomicValue) arguments[2].itemAt(0)).getStringValue();
                    String arg3 = ((XdmAtomicValue) arguments[3].itemAt(0)).getStringValue();
//                    XdmValue argument = arguments[4];
                    String arg4 = ((XdmAtomicValue) arguments[4].itemAt(0)).getStringValue();
//                    if (!(argument instanceof XdmEmptySequence)){
//                        arg4 = ((XdmAtomicValue) arguments[4].itemAt(0)).getStringValue();
//                    }
                    if (null == arg4 || "".equals(arg4)) {
                        arg4 = "";
                    }
                    try {
                        result = getContPlanCode(arg0, arg1, arg2, arg3, arg4);
                    } catch (NumberFormatException e) {
                        e.printStackTrace();
                    }
                    return new XdmAtomicValue(result);
                } else {
                    return new XdmAtomicValue(result);
                }
            }
        };
        ExtensionFunction policyMult = new ExtensionFunction() {
            @Override
            public QName getName() {
                return new QName("http://xml.apache.org/xslt/java", "com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.policyMult");
            }

            @Override
            public SequenceType getResultType() {
                return SequenceType.makeSequenceType(ItemType.INTEGER, OccurrenceIndicator.ONE);
            }

            @Override
            public SequenceType[] getArgumentTypes() {
                return new SequenceType[]
                        {
                                SequenceType.makeSequenceType(ItemType.STRING, OccurrenceIndicator.ONE)
                        };
            }

            @Override
            public XdmValue call(XdmValue[] arguments) throws SaxonApiException {
                Integer result = 0;
                if (((XdmAtomicValue) arguments[0].itemAt(0)) != null) {
                    String arg = ((XdmAtomicValue) arguments[0].itemAt(0)).getStringValue();
                    result = policyAmmount(arg);
                    return new XdmAtomicValue(result);
                } else {
                    return new XdmAtomicValue(result);
                }
            }
        };

        ExtensionFunction getNTransferPrem = new ExtensionFunction() {
            @Override
            public QName getName() {
                return new QName("http://xml.apache.org/xslt/java", "com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.getNTransferPrem");
            }

            @Override
            public SequenceType getResultType() {
                return SequenceType.makeSequenceType(ItemType.STRING, OccurrenceIndicator.ONE);
            }

            @Override
            public SequenceType[] getArgumentTypes() {
                return new SequenceType[]
                        {
                                SequenceType.makeSequenceType(ItemType.DOUBLE, OccurrenceIndicator.ONE)
                        };
            }

            @Override
            public XdmValue call(XdmValue[] arguments) throws SaxonApiException {
                String result = "";
                if (((XdmAtomicValue) arguments[0].itemAt(0)) != null) {
                    double arg = ((XdmAtomicValue) arguments[0].itemAt(0)).getDoubleValue();
                    result = getNTransferPrem(arg);
                    return new XdmAtomicValue(result);
                } else {
                    return new XdmAtomicValue(result);
                }
            }
        };


        ExtensionFunction getOTransferPrem = new ExtensionFunction() {
            @Override
            public QName getName() {
                return new QName("http://xml.apache.org/xslt/java", "com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.getOTransferPrem");
            }

            @Override
            public SequenceType getResultType() {
                return SequenceType.makeSequenceType(ItemType.STRING, OccurrenceIndicator.ONE);
            }

            @Override
            public SequenceType[] getArgumentTypes() {
                return new SequenceType[]
                        {
                                SequenceType.makeSequenceType(ItemType.DOUBLE, OccurrenceIndicator.ONE)
                        };
            }

            @Override
            public XdmValue call(XdmValue[] arguments) throws SaxonApiException {
                String result = "";
                if (((XdmAtomicValue) arguments[0].itemAt(0)) != null) {
                    double arg = ((XdmAtomicValue) arguments[0].itemAt(0)).getDoubleValue();
                    result = getOTransferPrem(arg);
                    return new XdmAtomicValue(result);
                } else {
                    return new XdmAtomicValue(result);
                }
            }
        };
        ExtensionFunction getZBPrem = new ExtensionFunction() {
            @Override
            public QName getName() {
                return new QName("http://xml.apache.org/xslt/java", "com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.getZBPrem");
            }

            @Override
            public SequenceType getResultType() {
                return SequenceType.makeSequenceType(ItemType.STRING, OccurrenceIndicator.ONE);
            }

            @Override
            public SequenceType[] getArgumentTypes() {
                return new SequenceType[]
                        {
                                SequenceType.makeSequenceType(ItemType.DOUBLE, OccurrenceIndicator.ONE),
                                SequenceType.makeSequenceType(ItemType.DOUBLE, OccurrenceIndicator.ONE)
                        };
            }

            @Override
            public XdmValue call(XdmValue[] arguments) throws SaxonApiException {
                String result = "";
                if (((XdmAtomicValue) arguments[0].itemAt(0)) != null) {
                    double arg = ((XdmAtomicValue) arguments[0].itemAt(0)).getDoubleValue();
                    double arg1 = ((XdmAtomicValue) arguments[1].itemAt(0)).getDoubleValue();
                    result = getZBPrem(arg, arg1);
                    return new XdmAtomicValue(result);
                } else {
                    return new XdmAtomicValue(result);
                }
            }
        };

        ExtensionFunction checkReserve4ByInsuredAgeTo18 = new ExtensionFunction() {
            @Override
            public QName getName() {
                return new QName("http://xml.apache.org/xslt/java", "com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.checkReserve4ByInsuredAgeTo18");
            }

            @Override
            public SequenceType getResultType() {
                return SequenceType.makeSequenceType(ItemType.STRING, OccurrenceIndicator.ONE);
            }

            @Override
            public SequenceType[] getArgumentTypes() {
                return new SequenceType[]
                        {
                                SequenceType.makeSequenceType(ItemType.STRING, OccurrenceIndicator.ONE),
                                SequenceType.makeSequenceType(ItemType.STRING, OccurrenceIndicator.ONE)
                        };
            }

            @Override
            public XdmValue call(XdmValue[] arguments) throws SaxonApiException {
                String result = "";

                if (((XdmAtomicValue) arguments[0].itemAt(0)) != null && ((XdmAtomicValue) arguments[1].itemAt(0)) != null) {
                    String arg0 = ((XdmAtomicValue) arguments[0].itemAt(0)).getStringValue();
                    String arg1 = ((XdmAtomicValue) arguments[1].itemAt(0)).getStringValue();
                    try {
                        result = checkReserve4ByInsuredAgeTo18(arg0, arg1);
                    } catch (NumberFormatException e) {
                        e.printStackTrace();
                    }
                    return new XdmAtomicValue(result);
                } else {
                    return new XdmAtomicValue(result);
                }
            }
        };

        ExtensionFunction modifyCValiDate = new ExtensionFunction() {
            @Override
            public QName getName() {
                return new QName("http://xml.apache.org/xslt/java", "com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.modifyCValiDate");
            }

            @Override
            public SequenceType getResultType() {
                return SequenceType.makeSequenceType(ItemType.STRING, OccurrenceIndicator.ONE);
            }

            @Override
            public SequenceType[] getArgumentTypes() {
                return new SequenceType[]
                        {
                                SequenceType.makeSequenceType(ItemType.STRING, OccurrenceIndicator.ONE),
                                SequenceType.makeSequenceType(ItemType.STRING, OccurrenceIndicator.ONE),
                                SequenceType.makeSequenceType(ItemType.STRING, OccurrenceIndicator.ONE)
                        };
            }

            @Override
            public XdmValue call(XdmValue[] arguments) throws SaxonApiException {
                String result = "";

                if (((XdmAtomicValue) arguments[0].itemAt(0)) != null && ((XdmAtomicValue) arguments[1].itemAt(0)) != null) {
                    String arg0 = ((XdmAtomicValue) arguments[0].itemAt(0)).getStringValue();
                    String arg1 = ((XdmAtomicValue) arguments[1].itemAt(0)).getStringValue();
                    String arg2 = ((XdmAtomicValue) arguments[2].itemAt(0)).getStringValue();
                    try {
                        result = modifyCValiDate(arg0, arg1, arg2);
                    } catch (NumberFormatException e) {
                        e.printStackTrace();
                    }
                    return new XdmAtomicValue(result);
                } else {
                    return new XdmAtomicValue(result);
                }
            }
        };
        ExtensionFunction modify1035CValiDate = new ExtensionFunction() {
            @Override
            public QName getName() {
                return new QName("http://xml.apache.org/xslt/java", "com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.modify1035CValiDate");
            }

            @Override
            public SequenceType getResultType() {
                return SequenceType.makeSequenceType(ItemType.STRING, OccurrenceIndicator.ONE);
            }

            @Override
            public SequenceType[] getArgumentTypes() {
                return new SequenceType[]
                        {
                                SequenceType.makeSequenceType(ItemType.STRING, OccurrenceIndicator.ONE),
                                SequenceType.makeSequenceType(ItemType.STRING, OccurrenceIndicator.ONE),
                                SequenceType.makeSequenceType(ItemType.STRING, OccurrenceIndicator.ONE),
                                SequenceType.makeSequenceType(ItemType.STRING, OccurrenceIndicator.ONE)
                        };
            }

            @Override
            public XdmValue call(XdmValue[] arguments) throws SaxonApiException {
                String result = "";

                if (((XdmAtomicValue) arguments[0].itemAt(0)) != null && ((XdmAtomicValue) arguments[1].itemAt(0)) != null) {
                    String arg0 = ((XdmAtomicValue) arguments[0].itemAt(0)).getStringValue();
                    String arg1 = ((XdmAtomicValue) arguments[1].itemAt(0)).getStringValue();
                    String arg2 = ((XdmAtomicValue) arguments[2].itemAt(0)).getStringValue();
                    String arg3 = ((XdmAtomicValue) arguments[3].itemAt(0)).getStringValue();
                    try {
                        result = modify1035CValiDate(arg0, arg1, arg2, arg3);
                    } catch (NumberFormatException e) {
                        e.printStackTrace();
                    }
                    return new XdmAtomicValue(result);
                } else {
                    return new XdmAtomicValue(result);
                }
            }
        };
        /**
         * 新增的方法进行配置
         *
         * add by rs
         * 20190819
         * getDate
         */
        ExtensionFunction getDate = new ExtensionFunction() {
            @Override
            public QName getName() {
                return new QName("http://xml.apache.org/xslt/java", "com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.getDate");
            }

            @Override
            public SequenceType getResultType() {
                return SequenceType.makeSequenceType(ItemType.STRING, OccurrenceIndicator.ONE);
            }

            @Override
            public SequenceType[] getArgumentTypes() {
                return new SequenceType[]
                        {
                                SequenceType.makeSequenceType(ItemType.STRING, OccurrenceIndicator.ONE)
                        };
            }

            @Override
            public XdmValue call(XdmValue[] arguments) throws SaxonApiException {
                String result = "";
                if (((XdmAtomicValue) arguments[0].itemAt(0)) != null) {
                    String arg0 = ((XdmAtomicValue) arguments[0].itemAt(0)).getStringValue();
                    result = getDate(arg0);
                    return new XdmAtomicValue(result);
                } else {
                    return new XdmAtomicValue(result);
                }
            }
        };
        //end
        /**
         * getTIme
         * add by rs 20190819
         */
        ExtensionFunction getTime = new ExtensionFunction() {
            @Override
            public QName getName() {
                return new QName("http://xml.apache.org/xslt/java", "com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.getTime");
            }

            @Override
            public SequenceType getResultType() {
                return SequenceType.makeSequenceType(ItemType.STRING, OccurrenceIndicator.ONE);
            }

            @Override
            public SequenceType[] getArgumentTypes() {
                return new SequenceType[]
                        {
                                SequenceType.makeSequenceType(ItemType.STRING, OccurrenceIndicator.ONE)
                        };
            }

            @Override
            public XdmValue call(XdmValue[] arguments) throws SaxonApiException {
                String result = "";
                if (((XdmAtomicValue) arguments[0].itemAt(0)) != null) {
                    String arg0 = ((XdmAtomicValue) arguments[0].itemAt(0)).getStringValue();
                    result = getTime(arg0);
                    return new XdmAtomicValue(result);
                } else {
                    return new XdmAtomicValue(result);
                }
            }
        };
        //end

        /**
         * getExactDate
         * add by rs 20191021
         */
        ExtensionFunction getExactDate = new ExtensionFunction() {
            @Override
            public QName getName() {
                return new QName("http://xml.apache.org/xslt/java", "com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.getExactDate");
            }

            @Override
            public SequenceType getResultType() {
                return SequenceType.makeSequenceType(ItemType.STRING, OccurrenceIndicator.ONE);
            }

            @Override
            public SequenceType[] getArgumentTypes() {
                return new SequenceType[]
                        {
                                SequenceType.makeSequenceType(ItemType.STRING, OccurrenceIndicator.ONE)
                        };
            }

            @Override
            public XdmValue call(XdmValue[] arguments) throws SaxonApiException {
                String result = "";
                if (((XdmAtomicValue) arguments[0].itemAt(0)) != null) {
                    String arg0 = ((XdmAtomicValue) arguments[0].itemAt(0)).getStringValue();
                    result = getExactDate(arg0);
                    return new XdmAtomicValue(result);
                } else {
                    return new XdmAtomicValue(result);
                }
            }
        };
        //end
        /**
         * getExactTime
         * add by rs 20191021
         */
        ExtensionFunction getExactTime = new ExtensionFunction() {
            @Override
            public QName getName() {
                return new QName("http://xml.apache.org/xslt/java", "com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.getExactTime");
            }

            @Override
            public SequenceType getResultType() {
                return SequenceType.makeSequenceType(ItemType.STRING, OccurrenceIndicator.ONE);
            }

            @Override
            public SequenceType[] getArgumentTypes() {
                return new SequenceType[]
                        {
                                SequenceType.makeSequenceType(ItemType.STRING, OccurrenceIndicator.ONE)
                        };
            }

            @Override
            public XdmValue call(XdmValue[] arguments) throws SaxonApiException {
                String result = "";
                if (((XdmAtomicValue) arguments[0].itemAt(0)) != null) {
                    String arg0 = ((XdmAtomicValue) arguments[0].itemAt(0)).getStringValue();
                    result = getExactTime(arg0);
                    return new XdmAtomicValue(result);
                } else {
                    return new XdmAtomicValue(result);
                }
            }
        };
        /**
         * 数字和年（或者月）分开
         */
        ExtensionFunction getPaymentNum = new ExtensionFunction() {
            @Override
            public QName getName() {
                return new QName("http://xml.apache.org/xslt/java", "com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.getPaymentNum");
            }

            @Override
            public SequenceType getResultType() {
                return SequenceType.makeSequenceType(ItemType.STRING, OccurrenceIndicator.ONE);
            }

            @Override
            public SequenceType[] getArgumentTypes() {
                return new SequenceType[]
                        {
                                SequenceType.makeSequenceType(ItemType.STRING, OccurrenceIndicator.ONE)
                        };
            }

            @Override
            public XdmValue call(XdmValue[] arguments) throws SaxonApiException {
                String result = "";
                if (((XdmAtomicValue) arguments[0].itemAt(0)) != null) {
                    String arg0 = ((XdmAtomicValue) arguments[0].itemAt(0)).getStringValue();
                    result = getPaymentNum(arg0);
                    return new XdmAtomicValue(result);
                } else {
                    return new XdmAtomicValue(result);
                }
            }
        };
        ExtensionFunction getPaymentYear = new ExtensionFunction() {
            @Override
            public QName getName() {
                return new QName("http://xml.apache.org/xslt/java", "com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.getPaymentYear");
            }

            @Override
            public SequenceType getResultType() {
                return SequenceType.makeSequenceType(ItemType.STRING, OccurrenceIndicator.ONE);
            }

            @Override
            public SequenceType[] getArgumentTypes() {
                return new SequenceType[]
                        {
                                SequenceType.makeSequenceType(ItemType.STRING, OccurrenceIndicator.ONE)
                        };
            }

            @Override
            public XdmValue call(XdmValue[] arguments) throws SaxonApiException {
                String result = "";
                if (((XdmAtomicValue) arguments[0].itemAt(0)) != null) {
                    String arg0 = ((XdmAtomicValue) arguments[0].itemAt(0)).getStringValue();
                    result = getPaymentYear(arg0);
                    return new XdmAtomicValue(result);
                } else {
                    return new XdmAtomicValue(result);
                }
            }
        };
        ExtensionFunction getLongByString = new ExtensionFunction() {
            @Override
            public QName getName() {
                return new QName("http://xml.apache.org/xslt/java", "com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.getLongByString");
            }

            @Override
            public SequenceType getResultType() {
                return SequenceType.makeSequenceType(ItemType.STRING, OccurrenceIndicator.ONE);
            }

            @Override
            public SequenceType[] getArgumentTypes() {
                return new SequenceType[]
                        {
                                SequenceType.makeSequenceType(ItemType.STRING, OccurrenceIndicator.ONE)
                        };
            }

            @Override
            public XdmValue call(XdmValue[] arguments) throws SaxonApiException {
                Long result=null;
                if (((XdmAtomicValue) arguments[0].itemAt(0)) != null) {
                    String arg0 = ((XdmAtomicValue) arguments[0].itemAt(0)).getStringValue();
                    result = getLongByString(arg0);
                    return new XdmAtomicValue(result);
                } else {
                    return new XdmAtomicValue(result);
                }
            }
        };

        ExtensionFunction calInterval3 = new ExtensionFunction() {
            @Override
            public QName getName() {
                return new QName("http://xml.apache.org/xslt/java", "com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.calInterval3");
            }

            @Override
            public SequenceType getResultType() {
                return SequenceType.makeSequenceType(ItemType.STRING, OccurrenceIndicator.ONE);
            }

            @Override
            public SequenceType[] getArgumentTypes() {
                return new SequenceType[]
                        {
                                SequenceType.makeSequenceType(ItemType.STRING, OccurrenceIndicator.ONE),
                                SequenceType.makeSequenceType(ItemType.STRING, OccurrenceIndicator.ONE)
                        };
            }

            @Override
            public XdmValue call(XdmValue[] arguments) throws SaxonApiException {
                String result=null;
                if (((XdmAtomicValue) arguments[0].itemAt(0)) != null
                        && ((XdmAtomicValue) arguments[1].itemAt(0)) != null) {
                    String arg0 = ((XdmAtomicValue) arguments[0].itemAt(0)).getStringValue();
                    String arg1 = ((XdmAtomicValue) arguments[1].itemAt(0)).getStringValue();
                    result = calInterval3(arg0,arg1);
                    return new XdmAtomicValue(result);
                } else {
                    return new XdmAtomicValue(result);
                }
            }
        };

        ExtensionFunction CreateMaxNo = new ExtensionFunction() {
            @Override
            public QName getName() {
                return new QName("http://xml.apache.org/xslt/java", "com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.CreateMaxNo");
            }

            @Override
            public SequenceType getResultType() {
                return SequenceType.makeSequenceType(ItemType.STRING, OccurrenceIndicator.ONE);
            }

            @Override
            public SequenceType[] getArgumentTypes() {
                return new SequenceType[]
                        {
                                SequenceType.makeSequenceType(ItemType.STRING, OccurrenceIndicator.ONE)
                        };
            }

            @Override
            public XdmValue call(XdmValue[] arguments) throws SaxonApiException {
                String result=null;
                if (((XdmAtomicValue) arguments[0].itemAt(0)) != null) {
                    String arg0 = ((XdmAtomicValue) arguments[0].itemAt(0)).getStringValue();
                    result = CreateMaxNo(arg0);
                    return new XdmAtomicValue(result);
                } else {
                    return new XdmAtomicValue(result);
                }
            }
        };

        //end


        return new ExtensionFunction[]{
                yuanToFen, fenToYuan, fillStrWith_2Args, fillStrWith_3Args, date8to10, parsBoolean
                , date10to8, appntIncome, insuredIncome, appntFamilyIncome, fixedLengthString
                , timeFormat, getChnMoney, getCalDate, dateFormat, dateFormat8To10, incomeFormat
                , calStatureAndAvoirdupois, timeString10To8, cValiDate, getCurrentDate, getContPlanCode
                , policyMult, getNTransferPrem, getOTransferPrem, getZBPrem, getDateTimeNow, getThisDateAfterDay, checkRiskCode
                , calculationDays, transOutHeadTransNo, getDateTimeNowAddOrReduce, getFenMoney, getYuanMoney, checkReserve4ByInsuredAgeTo18
                , cValiDateOther, getAddress, getStoreNo, modifyCValiDate, modify1035CValiDate, getbnfAddress, getMIPAddress, getMIPStoreNo, getBNFAddress
                , getDate, getTime,getExactDate,getExactTime,getPaymentNum,getPaymentYear,getLongByString,calInterval3,CreateMaxNo
        };
    }
}