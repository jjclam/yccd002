package com.sinosoft.cloud.access.util;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @Author: 崔广东
 * @Date: 2017-9-27 10:36
 * @Description: 时间相关的工具类
 */
public class TimeUtil {

    /**
     * 日志管理
     */
    private static final Log cLogger = LogFactory.getLog(TimeUtil.class);

    /**
     * 获取当前的时间，精确到秒
     * @return yyyyMMddHHmmss（String）
     */
    public static String getCurrentDateTime(){
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String currentTime = formatter.format(new Date());
        return currentTime;
    }

    /**
     * 获取系统当前日期
     * @return yyyy-MM-dd（String）
     */
    public static String getCurrentDate()
    {
        String pattern = "yyyy-MM-dd";
        SimpleDateFormat df = new SimpleDateFormat(pattern);
        Date today = new Date();
        String tString = df.format(today);
        return tString;
    }

    /**
     * 获取当前时间（时分秒）
     * @return HHmmss（String）
     */
    public static String getCurrentTime(){
        SimpleDateFormat formatter = new SimpleDateFormat("HHmmss");
        String currentTime = formatter.format(new Date());
        return currentTime;
    }

    /**
     * 将字符串类型转换为Date类型
     * @param dateStr-yyyyMMdd（String）
     * @return Date
     */
    public static Date stringToDate(String dateStr){
        String resultStr = "";
        if (dateStr.contains("-"))
        {
            StringBuffer sb = new StringBuffer(1024);
            resultStr = sb.append(dateStr.substring(0, 4)).append(dateStr.substring(5, 7)).append(dateStr.substring(8)).toString();
        }else {
            resultStr = dateStr;
        }

        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        Date result = null;
        try {
            result = sdf.parse(resultStr);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return result;
    }

    /**
     * 获取保单生效日期（目前为投保日期的第二天）
     * @param cValiDate-投保日期（String类型），传入日期格式为20181015
     * @return 保单生效日期,yyyy-MM-dd（String类型）
     */
    public static String getCValiDate(String cValiDate){

        if (StringUtils.isEmpty(cValiDate))
        {
            return "";
        }

        Calendar c = Calendar.getInstance();
        Date date=null;
        try {
            date = new SimpleDateFormat("yyyyMMdd").parse(cValiDate);
        } catch (ParseException e) {
            cLogger.error("日期转换异常");
            cLogger.error(e);
            e.printStackTrace();
            return "";
        }
        c.setTime(date);
        int day=c.get(Calendar.DATE);
        c.set(Calendar.DATE,day+1);

        String dayAfter=new SimpleDateFormat("yyyy-MM-dd").format(c.getTime());
        return dayAfter;
    }

    /**
     * 获取保单生效日期（目前为投保日期的第二天）
     * @param cValiDate-投保日期（String类型），传入日期格式为2018-10-15
     * @return 保单生效日期,yyyy-MM-dd（String类型）
     */
    public static String getCValiDateOther(String cValiDate){

        if (StringUtils.isEmpty(cValiDate))
        {
            return "";
        }

        Calendar c = Calendar.getInstance();
        Date date=null;
        try {
            date = new SimpleDateFormat("yyyy-MM-dd").parse(cValiDate);
        } catch (ParseException e) {
            cLogger.error("日期转换异常");
            cLogger.error(e);
            e.printStackTrace();
            return "";
        }
        c.setTime(date);
        int day=c.get(Calendar.DATE);
        c.set(Calendar.DATE,day+1);

        String dayAfter=new SimpleDateFormat("yyyy-MM-dd").format(c.getTime());
        return dayAfter;
    }

    /**
     * 将Date类型转换为字符串类型
     * @param date-Date
     * @return yyyyMMdd（String）
     */
    public static String dateToString(Date date){
        String result = "";
        if (date != null)
        {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
            result = sdf.format(date);
        }
        return result;
    }

    /**
     * 将Date类型转换为字符串类型
     * @param date-Date
     * @return yyyy-MM-dd（String）
     */
    public static String dateToStringFor10(Date date){
        String result = "";
        if (date != null)
        {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            result = sdf.format(date);
        }
        return result;
    }

    /**
     * 获取当前时间与特定时间之差
     * @param startDate-开始日期（String）
     * @param startTime-开始时间（String）
     * @return 毫秒值对应的字符串
     */
    public static String interval(String startDate, String startTime) {
        SimpleDateFormat dfs = new SimpleDateFormat("yyyyMMdd");
        SimpleDateFormat dfs2 = new SimpleDateFormat("HHmmss");
        String insuTime = null;

        try {
            insuTime = String.valueOf(System.currentTimeMillis() - (dfs.parse(startDate).getTime() + dfs2.parse(startTime).getTime()));
        } catch (ParseException e) {
            cLogger.error("日期转换异常");
            cLogger.error(e);
            e.printStackTrace();
        }

        return insuTime;
    }

    /**
     * 将yyyyMMdd的字符串日期格式改成yyyy-MM-dd的字符串日期格式
     * @param dateStr-yyyyMMdd
     * @return yyyy-MM-dd
     */
    public static String dateString8To10(String dateStr){
        if (null == dateStr){
            return "";
        }
        if (!"".equals(dateStr) && dateStr.length() == 8){
            StringBuffer sb = new StringBuffer(1024);
            String result = sb.append(dateStr.substring(0, 4)).append("-").append(dateStr.substring(4, 6))
                    .append("-").append(dateStr.substring(6)).toString();
            return result;
        }else {
            return dateStr;
        }
    }

    /**
     * 获取两个long值之间的毫秒值
     * @param startTime
     * @param endTime
     * @return
     */
    public static String getMillis(long startTime,long endTime,String serviceName){
        StringBuffer stringBuffer = new StringBuffer(1024);
        String result = stringBuffer.append("access性能监控：")
                .append("[").append(serviceName).append("]").append("用时")
                .append(String.valueOf(endTime - startTime)).append("毫秒").toString();
        return result;
    }

    /**
     * 将yyyy-MM-dd格式转为yyyyMMdd
     * @param dateStr
     * @return
     */
    public static String dateString10To8(String dateStr)
    {
        if (null == dateStr){
            return "";
        }

        StringBuffer sb = new StringBuffer(1024);
        if (!"".equals(dateStr) && dateStr.length() == 10)
        {
            return sb.append(dateStr.substring(0, 4)).append(dateStr.substring(5, 7)).append(dateStr.substring(8)).toString();
        }
        else
        {
            return dateStr;
        }
    }

    /**
     * 将yyyy-MM-dd转换为yyyy年MM月dd日
     * @param dateStr
     * @return
     */
    public static String dateFormat(String dateStr)
    {
        if (null == dateStr || "".equals(dateStr)){
            return "";
        }

        StringBuffer sb = new StringBuffer(1024);

        if (!"".equals(dateStr) && dateStr.length() == 10)
        {
            return sb.append(dateStr.substring(0, 4)).append("年").append(dateStr.substring(5, 7)).append("月").append(dateStr.substring(8)).append("日").toString();
        }
        else
        {
            return sb.append(dateStr.substring(0, 4)).append("年").append(dateStr.substring(4, 6)).append("月").append(dateStr.substring(6)).append("日").toString();
        }
    }

    /**
     * 将HHssmm的字符串日期格式改成HH:ss:mm的字符串日期格式
     * @param timeStr-HHssmm
     * @return HH:ss:mm
     */
    public static String timeString8To10(String timeStr){
        if (null == timeStr){
            return "";
        }
        if (!"".equals(timeStr) && timeStr.length() == 6){
            StringBuffer sb = new StringBuffer(1024);
            String result = sb.append(timeStr.substring(0, 2)).append(":").append(timeStr.substring(2, 4))
                    .append(":").append(timeStr.substring(4)).toString();
            return result;
        }else {
            return timeStr;
        }
    }
    /**
     * 将HH:ss:mm的字符串日期格式改成HHssmm的字符串日期格式
     *
     * @param timeStr-HH:ss:mm
     * @return HHssmm
     */
    public static String timeString10To8(String timeStr) {
        if (null == timeStr) {
            return "";
        }
        if (!"".equals(timeStr) && timeStr.length() == 8) {
            StringBuffer sb = new StringBuffer(1024);
            String result = sb.append(timeStr.substring(0, 2)).append(timeStr.substring(3, 5)).append(timeStr.substring(6)).toString();
            return result;
        } else {
            return timeStr;
        }
    }


    /**
     * 获取保单生效日期（目前为投保日期的第二天）
     * @param cValiDate-投保日期（String类型），传入日期格式为2018-10-15
     * @return 保单生效日期,yyyy-MM-dd（String类型）
     */
    public static String getCValiDateOther1(String cValiDate){

        if (StringUtils.isEmpty(cValiDate))
        {
            return "";
        }

        Calendar c = Calendar.getInstance();
        Date date=null;
        try {
            date = new SimpleDateFormat("yyyy-MM-dd").parse(cValiDate);
        } catch (ParseException e) {
            cLogger.error("日期转换异常");
            cLogger.error(e);
            e.printStackTrace();
            return "";
        }
        c.setTime(date);
        int day=c.get(Calendar.DATE);
        c.set(Calendar.DATE,day-1);

        String dayAfter=new SimpleDateFormat("yyyy-MM-dd").format(c.getTime());
        return dayAfter;
    }
    /**
     * @param value 要校验的字符串对象
     * @param rule  正则表达式
     * @return 匹配成功返回true，匹配失败返回false
     */
    public static boolean regExp(String value, String rule) {
        String strRule = rule == null ? "" : rule;
        String strValue = value == null ? "" : value;
        boolean flag = false;
        Pattern pattern = Pattern.compile(strRule);
        Matcher matcher = null;
        matcher = pattern.matcher(strValue);
        if (matcher != null && matcher.find()) {
            flag = true;
        }
        return flag;
    }
}
