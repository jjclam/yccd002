package com.sinosoft.cloud.access.transformer.converters;

import com.thoughtworks.xstream.converters.basic.AbstractSingleValueConverter;
import org.apache.commons.lang.StringUtils;

/**
 * @Description
 * @Author:zhaoshulei
 * @Date:2018/4/11
 */
public class DoubleConverter extends AbstractSingleValueConverter {

    public boolean canConvert(Class type) {
        return type.equals(double.class) || type.equals(Double.class);
    }

    /**
     * @param str 需要转换的字符串
     * @return
     */
    public Object fromString(String str) {
        if (StringUtils.isEmpty(str.trim())) {
            return 0.0D;
        }
        return Double.valueOf(str);
    }

}
