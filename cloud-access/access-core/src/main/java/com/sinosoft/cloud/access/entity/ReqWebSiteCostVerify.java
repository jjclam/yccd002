package com.sinosoft.cloud.access.entity;

/**
 * abc-cloud-access
 *
 * @author lizhen
 * @create 2019/3/4
 */

/**
 * 退保费用核算 微信、官网  pos127
 */
public class ReqWebSiteCostVerify {
    //xxx订单号
    private String ABCOrderId;
    //保单号
    private String ContNo;
    //用户输入的支取金额
    private String WithdrawMoney;
    //是否用户选择为退保
    private String IsCancelPolicy;
    //是否全部领取
    private String IsWithdrawAll;

    public String getABCOrderId() {
        return ABCOrderId;
    }

    public void setABCOrderId(String ABCOrderId) {
        this.ABCOrderId = ABCOrderId;
    }

    public String getContNo() {
        return ContNo;
    }

    public void setContNo(String contNo) {
        ContNo = contNo;
    }

    public String getWithdrawMoney() {
        return WithdrawMoney;
    }

    public void setWithdrawMoney(String withdrawMoney) {
        WithdrawMoney = withdrawMoney;
    }

    public String getIsCancelPolicy() {
        return IsCancelPolicy;
    }

    public void setIsCancelPolicy(String isCancelPolicy) {
        IsCancelPolicy = isCancelPolicy;
    }

    public String getIsWithdrawAll() {
        return IsWithdrawAll;
    }

    public void setIsWithdrawAll(String isWithdrawAll) {
        IsWithdrawAll = isWithdrawAll;
    }

    @Override
    public String toString() {
        return "ReqWebSiteCostVerify{" +
                "ABCOrderId='" + ABCOrderId + '\'' +
                ", ContNo='" + ContNo + '\'' +
                ", WithdrawMoney='" + WithdrawMoney + '\'' +
                ", IsCancelPolicy='" + IsCancelPolicy + '\'' +
                ", IsWithdrawAll='" + IsWithdrawAll + '\'' +
                '}';
    }
}