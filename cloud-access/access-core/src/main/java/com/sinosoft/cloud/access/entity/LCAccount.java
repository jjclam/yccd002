package com.sinosoft.cloud.access.entity;

/**
 * 账户pojo.
 * @Author: cuiguangdong
 * @Date: 2018/4/11 17:01
 * @Description: 写描述
 */
public class LCAccount {
    private long AccountID;
    private long PersonID;
    private String ShardingID;
    private String CustomerNo;
    private String AccKind;
    private String BankCode;
    private String BankAccNo;
    private String AccName;
    private String Operator;
    private String MakeDate;
    private String MakeTime;
    private String ModifyDate;
    private String ModifyTime;

    public long getAccountID() {
        return AccountID;
    }

    public void setAccountID(long accountID) {
        AccountID = accountID;
    }

    public long getPersonID() {
        return PersonID;
    }

    public void setPersonID(long personID) {
        PersonID = personID;
    }

    public String getShardingID() {
        return ShardingID;
    }

    public void setShardingID(String shardingID) {
        ShardingID = shardingID;
    }

    public String getCustomerNo() {
        return CustomerNo;
    }

    public void setCustomerNo(String customerNo) {
        CustomerNo = customerNo;
    }

    public String getAccKind() {
        return AccKind;
    }

    public void setAccKind(String accKind) {
        AccKind = accKind;
    }

    public String getBankCode() {
        return BankCode;
    }

    public void setBankCode(String bankCode) {
        BankCode = bankCode;
    }

    public String getBankAccNo() {
        return BankAccNo;
    }

    public void setBankAccNo(String bankAccNo) {
        BankAccNo = bankAccNo;
    }

    public String getAccName() {
        return AccName;
    }

    public void setAccName(String accName) {
        AccName = accName;
    }

    public String getOperator() {
        return Operator;
    }

    public void setOperator(String operator) {
        Operator = operator;
    }

    public String getMakeDate() {
        return MakeDate;
    }

    public void setMakeDate(String makeDate) {
        MakeDate = makeDate;
    }

    public String getMakeTime() {
        return MakeTime;
    }

    public void setMakeTime(String makeTime) {
        MakeTime = makeTime;
    }

    public String getModifyDate() {
        return ModifyDate;
    }

    public void setModifyDate(String modifyDate) {
        ModifyDate = modifyDate;
    }

    public String getModifyTime() {
        return ModifyTime;
    }

    public void setModifyTime(String modifyTime) {
        ModifyTime = modifyTime;
    }
}
