package com.sinosoft.cloud.access.net;

public interface Coder {
    boolean needDecoder(String msg);

    int getBodySizeByHead(String msg);

    int getBodySize(String msg);

    int getHeadSize();

    boolean isComplate(String msg);

    String encoder(String msg);
}
