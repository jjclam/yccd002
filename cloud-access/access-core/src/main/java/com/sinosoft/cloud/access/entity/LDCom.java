package com.sinosoft.cloud.access.entity;

/**
 * @Description
 * @Author:zhaoshulei
 * @Date:2018/4/10
 */
public class LDCom {
    private String ComCode;
    private String OutComCode;
    private String Name;
    private String ShortName;
    private String Address;
    private String ZipCode;
    private String Phone;
    private String Fax;
    private String EMail;
    private String WebAddress;
    private String SatrapName;
    private String InsuMonitorCode;
    private String InsureID;
    private String SignID;
    private String RegionalismCode;
    private String ComNature;
    private String ValidCode;
    private String Sign;
    private String ComCitySize;
    private String ServiceName;
    private String ServiceNo;
    private String ServicePhone;
    private String ServicePostAddress;
    private String COMGRADE;
    private String COMAREATYPE;
    private String UPCOMCODE;
    private String ISDIRUNDER;

    public String getComCode() {
        return ComCode;
    }

    public void setComCode(String comCode) {
        ComCode = comCode;
    }

    public String getOutComCode() {
        return OutComCode;
    }

    public void setOutComCode(String outComCode) {
        OutComCode = outComCode;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getShortName() {
        return ShortName;
    }

    public void setShortName(String shortName) {
        ShortName = shortName;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String address) {
        Address = address;
    }

    public String getZipCode() {
        return ZipCode;
    }

    public void setZipCode(String zipCode) {
        ZipCode = zipCode;
    }

    public String getPhone() {
        return Phone;
    }

    public void setPhone(String phone) {
        Phone = phone;
    }

    public String getFax() {
        return Fax;
    }

    public void setFax(String fax) {
        Fax = fax;
    }

    public String getEMail() {
        return EMail;
    }

    public void setEMail(String EMail) {
        this.EMail = EMail;
    }

    public String getWebAddress() {
        return WebAddress;
    }

    public void setWebAddress(String webAddress) {
        WebAddress = webAddress;
    }

    public String getSatrapName() {
        return SatrapName;
    }

    public void setSatrapName(String satrapName) {
        SatrapName = satrapName;
    }

    public String getInsuMonitorCode() {
        return InsuMonitorCode;
    }

    public void setInsuMonitorCode(String insuMonitorCode) {
        InsuMonitorCode = insuMonitorCode;
    }

    public String getInsureID() {
        return InsureID;
    }

    public void setInsureID(String insureID) {
        InsureID = insureID;
    }

    public String getSignID() {
        return SignID;
    }

    public void setSignID(String signID) {
        SignID = signID;
    }

    public String getRegionalismCode() {
        return RegionalismCode;
    }

    public void setRegionalismCode(String regionalismCode) {
        RegionalismCode = regionalismCode;
    }

    public String getComNature() {
        return ComNature;
    }

    public void setComNature(String comNature) {
        ComNature = comNature;
    }

    public String getValidCode() {
        return ValidCode;
    }

    public void setValidCode(String validCode) {
        ValidCode = validCode;
    }

    public String getSign() {
        return Sign;
    }

    public void setSign(String sign) {
        Sign = sign;
    }

    public String getComCitySize() {
        return ComCitySize;
    }

    public void setComCitySize(String comCitySize) {
        ComCitySize = comCitySize;
    }

    public String getServiceName() {
        return ServiceName;
    }

    public void setServiceName(String serviceName) {
        ServiceName = serviceName;
    }

    public String getServiceNo() {
        return ServiceNo;
    }

    public void setServiceNo(String serviceNo) {
        ServiceNo = serviceNo;
    }

    public String getServicePhone() {
        return ServicePhone;
    }

    public void setServicePhone(String servicePhone) {
        ServicePhone = servicePhone;
    }

    public String getServicePostAddress() {
        return ServicePostAddress;
    }

    public void setServicePostAddress(String servicePostAddress) {
        ServicePostAddress = servicePostAddress;
    }

    public String getCOMGRADE() {
        return COMGRADE;
    }

    public void setCOMGRADE(String COMGRADE) {
        this.COMGRADE = COMGRADE;
    }

    public String getCOMAREATYPE() {
        return COMAREATYPE;
    }

    public void setCOMAREATYPE(String COMAREATYPE) {
        this.COMAREATYPE = COMAREATYPE;
    }

    public String getUPCOMCODE() {
        return UPCOMCODE;
    }

    public void setUPCOMCODE(String UPCOMCODE) {
        this.UPCOMCODE = UPCOMCODE;
    }

    public String getISDIRUNDER() {
        return ISDIRUNDER;
    }

    public void setISDIRUNDER(String ISDIRUNDER) {
        this.ISDIRUNDER = ISDIRUNDER;
    }

    @Override
    public String toString() {
        return "LDCom{" +
                "ComCode='" + ComCode + '\'' +
                ", OutComCode='" + OutComCode + '\'' +
                ", Name='" + Name + '\'' +
                ", ShortName='" + ShortName + '\'' +
                ", Address='" + Address + '\'' +
                ", ZipCode='" + ZipCode + '\'' +
                ", Phone='" + Phone + '\'' +
                ", Fax='" + Fax + '\'' +
                ", EMail='" + EMail + '\'' +
                ", WebAddress='" + WebAddress + '\'' +
                ", SatrapName='" + SatrapName + '\'' +
                ", InsuMonitorCode='" + InsuMonitorCode + '\'' +
                ", InsureID='" + InsureID + '\'' +
                ", SignID='" + SignID + '\'' +
                ", RegionalismCode='" + RegionalismCode + '\'' +
                ", ComNature='" + ComNature + '\'' +
                ", ValidCode='" + ValidCode + '\'' +
                ", Sign='" + Sign + '\'' +
                ", ComCitySize='" + ComCitySize + '\'' +
                ", ServiceName='" + ServiceName + '\'' +
                ", ServiceNo='" + ServiceNo + '\'' +
                ", ServicePhone='" + ServicePhone + '\'' +
                ", ServicePostAddress='" + ServicePostAddress + '\'' +
                ", COMGRADE='" + COMGRADE + '\'' +
                ", COMAREATYPE='" + COMAREATYPE + '\'' +
                ", UPCOMCODE='" + UPCOMCODE + '\'' +
                ", ISDIRUNDER='" + ISDIRUNDER + '\'' +
                '}';
    }
}
