package com.sinosoft.cloud.access.entity;

/**
 * 生存调查申请POS049.
 */
public class ReqLifeApply {
    //保全项目编码
    private String EdorType;
    //身份证件号码
    private String CustomerIDNo;
    //姓名
    private String CustomerName;
    //生调日期
    private String SurvivalDate;
    //生调结果
    private String SurvivalResult;
    //生调人员姓名
    private String AppentName;
    //客户号
    private String CustomerNo;
    private EdorAppInfo EdorAppInfo;
    private PersonInfo PersonInfo;
    private BankInfo BankInfo;
    //收付费方式
    private String PayMode;

    public String getEdorType() {
        return EdorType;
    }

    public void setEdorType(String edorType) {
        EdorType = edorType;
    }

    public String getCustomerIDNo() {
        return CustomerIDNo;
    }

    public void setCustomerIDNo(String customerIDNo) {
        CustomerIDNo = customerIDNo;
    }

    public String getCustomerName() {
        return CustomerName;
    }

    public void setCustomerName(String customerName) {
        CustomerName = customerName;
    }

    public String getSurvivalDate() {
        return SurvivalDate;
    }

    public void setSurvivalDate(String survivalDate) {
        SurvivalDate = survivalDate;
    }

    public String getSurvivalResult() {
        return SurvivalResult;
    }

    public void setSurvivalResult(String survivalResult) {
        SurvivalResult = survivalResult;
    }

    public String getAppentName() {
        return AppentName;
    }

    public void setAppentName(String appentName) {
        AppentName = appentName;
    }

    public com.sinosoft.cloud.access.entity.EdorAppInfo getEdorAppInfo() {
        return EdorAppInfo;
    }

    public void setEdorAppInfo(com.sinosoft.cloud.access.entity.EdorAppInfo edorAppInfo) {
        EdorAppInfo = edorAppInfo;
    }

    public com.sinosoft.cloud.access.entity.PersonInfo getPersonInfo() {
        return PersonInfo;
    }

    public void setPersonInfo(com.sinosoft.cloud.access.entity.PersonInfo personInfo) {
        PersonInfo = personInfo;
    }

    public com.sinosoft.cloud.access.entity.BankInfo getBankInfo() {
        return BankInfo;
    }

    public void setBankInfo(com.sinosoft.cloud.access.entity.BankInfo bankInfo) {
        BankInfo = bankInfo;
    }

    public String getPayMode() {
        return PayMode;
    }

    public void setPayMode(String payMode) {
        PayMode = payMode;
    }

    public String getCustomerNo() {
        return CustomerNo;
    }

    public void setCustomerNo(String customerNo) {
        CustomerNo = customerNo;
    }

    @Override
    public String toString() {
        return "ReqLifeApply{" +
                "EdorType='" + EdorType + '\'' +
                ", CustomerIDNo='" + CustomerIDNo + '\'' +
                ", CustomerName='" + CustomerName + '\'' +
                ", SurvivalDate='" + SurvivalDate + '\'' +
                ", SurvivalResult='" + SurvivalResult + '\'' +
                ", AppentName='" + AppentName + '\'' +
                ", CustomerNo='" + CustomerNo + '\'' +
                ", EdorAppInfo=" + EdorAppInfo +
                ", PersonInfo=" + PersonInfo +
                ", BankInfo=" + BankInfo +
                ", PayMode='" + PayMode + '\'' +
                '}';
    }
}