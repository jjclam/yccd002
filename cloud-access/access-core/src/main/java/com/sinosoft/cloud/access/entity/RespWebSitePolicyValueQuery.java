package com.sinosoft.cloud.access.entity;

/**
 * abc-cloud-access
 *
 * @author lizhen
 * @create 2019/3/4
 */

/**
 * 官网，微信 保单价值查询   PC007
 */
public class RespWebSitePolicyValueQuery {
    //xxx订单号
    private String ABCOrderId;
    //是否允许部分支取
    private String IsPartWithdraw;
    //保单账户现金价值
    private String PolicyValue;
    //是否可冲正
    private String IsReversal;
    //全额情况下是否必须退保
    private String IsMustCancel;
    //保单状态
    private String PolicyStatus;
    //现金价值
    private String CashValue;
    //部分领取最大金额
    private String MaxPartValue;

    public String getABCOrderId() {
        return ABCOrderId;
    }

    public void setABCOrderId(String ABCOrderId) {
        this.ABCOrderId = ABCOrderId;
    }

    public String getIsPartWithdraw() {
        return IsPartWithdraw;
    }

    public void setIsPartWithdraw(String isPartWithdraw) {
        IsPartWithdraw = isPartWithdraw;
    }

    public String getPolicyValue() {
        return PolicyValue;
    }

    public void setPolicyValue(String policyValue) {
        PolicyValue = policyValue;
    }

    public String getIsReversal() {
        return IsReversal;
    }

    public void setIsReversal(String isReversal) {
        IsReversal = isReversal;
    }

    public String getIsMustCancel() {
        return IsMustCancel;
    }

    public void setIsMustCancel(String isMustCancel) {
        IsMustCancel = isMustCancel;
    }

    public String getPolicyStatus() {
        return PolicyStatus;
    }

    public void setPolicyStatus(String policyStatus) {
        PolicyStatus = policyStatus;
    }

    public String getCashValue() {
        return CashValue;
    }

    public void setCashValue(String cashValue) {
        CashValue = cashValue;
    }

    public String getMaxPartValue() {
        return MaxPartValue;
    }

    public void setMaxPartValue(String maxPartValue) {
        MaxPartValue = maxPartValue;
    }

    @Override
    public String toString() {
        return "RespWebSitePolicyValueQuery{" +
                "ABCOrderId='" + ABCOrderId + '\'' +
                ", IsPartWithdraw='" + IsPartWithdraw + '\'' +
                ", PolicyValue='" + PolicyValue + '\'' +
                ", IsReversal='" + IsReversal + '\'' +
                ", IsMustCancel='" + IsMustCancel + '\'' +
                ", PolicyStatus='" + PolicyStatus + '\'' +
                ", CashValue='" + CashValue + '\'' +
                ", MaxPartValue='" + MaxPartValue + '\'' +
                '}';
    }
}