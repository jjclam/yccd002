package com.sinosoft.cloud.access.entity;

import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.List;

/**
 * @Author:lizhen
 * @Description:
 * @Date:Created in 15:05 2018/4/19
 * @Modified By:
 */
public class LDPersons {
    @XStreamImplicit(itemFieldName="LDPerson")
    private List<LDPerson> LDPersons;

    public List<LDPerson> getLDPersons() {
        return LDPersons;
    }

    public void setLDPersons(List<LDPerson> LDPersons) {
        this.LDPersons = LDPersons;
    }

    @Override
    public String toString() {
        return "LDPersons{" +
                "LDPersons=" + LDPersons +
                '}';
    }
}