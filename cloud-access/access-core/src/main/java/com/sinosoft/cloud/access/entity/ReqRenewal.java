package com.sinosoft.cloud.access.entity;

/**
 * @program: abc-cloud-access
 * @description: 微信续保查询
 * @author: BaoYongmeng
 * @create: 2019-03-20 18:03
 **/
public class ReqRenewal {

    //保单号
    private String ContNo;

    public String getContNo() {
        return ContNo;
    }

    public void setContNo(String contNo) {
        ContNo = contNo;
    }

    @Override
    public String toString() {
        return "ReqRenewal{" +
                "ContNo='" + ContNo + '\'' +
                '}';
    }
}
