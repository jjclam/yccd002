package com.sinosoft.cloud.access.entity;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("Head_copy")
public class Head_copy {

    /*交易日期*/
    @XStreamAlias("TranDate")
    private String tranDate;
    /*交易时间*/
    @XStreamAlias("TranTime")
    private String tranTime;
    /*柜员代码*/
    @XStreamAlias("TellerNo")
    private String tellerNo;
    /*交易流水号*/
    @XStreamAlias("TranNo")
    private String tranNo;
    /*网点代码*/
    @XStreamAlias("NodeNo")
    private String nodeNo;
    @XStreamAlias("TranCom")
    private String tranCom;
    @XStreamAlias("BankCode")
    private String bankCode;
    @XStreamAlias("FuncFlag")

    private String funcFlag;
    @XStreamAlias("AgentCom")

    private String agentCom;
    @XStreamAlias("AgentCode")

    private String agentCode;
    /*xxx柜员姓名*/
    @XStreamAlias("BankClerk")

    private String bankClerk;
    /*渠道*/
    @XStreamAlias("SourceType")
    private String sourceType;
    @XStreamAlias("TransCode")
    private String transCode;

    //返回报文所需要的字段
    @XStreamAlias("Flag")
    private String flag;
    @XStreamAlias("Desc")
    private String desc;

    //销售渠道
    @XStreamAlias("SellType")
    private String sellType;
    //保险公司流水号
    @XStreamAlias("InsuSerial")
    private String insuSerial;
    //保险公司代码
    @XStreamAlias("CorpNo")
    private String corpNo;
    @XStreamAlias("ZoneNo")
    private String zoneNo;
    @XStreamAlias("TransSide")
    private String transSide;

    public String getTranDate() {
        return tranDate;
    }

    public void setTranDate(String tranDate) {
        this.tranDate = tranDate;
    }

    public String getTranTime() {
        return tranTime;
    }

    public void setTranTime(String tranTime) {
        this.tranTime = tranTime;
    }

    public String getTellerNo() {
        return tellerNo;
    }

    public void setTellerNo(String tellerNo) {
        this.tellerNo = tellerNo;
    }

    public String getTranNo() {
        return tranNo;
    }

    public void setTranNo(String tranNo) {
        this.tranNo = tranNo;
    }

    public String getNodeNo() {
        return nodeNo;
    }

    public void setNodeNo(String nodeNo) {
        this.nodeNo = nodeNo;
    }

    public String getTranCom() {
        return tranCom;
    }

    public void setTranCom(String tranCom) {
        this.tranCom = tranCom;
    }

    public String getBankCode() {
        return bankCode;
    }

    public void setBankCode(String bankCode) {
        this.bankCode = bankCode;
    }

    public String getFuncFlag() {
        return funcFlag;
    }

    public void setFuncFlag(String funcFlag) {
        this.funcFlag = funcFlag;
    }

    public String getAgentCom() {
        return agentCom;
    }

    public void setAgentCom(String agentCom) {
        this.agentCom = agentCom;
    }

    public String getAgentCode() {
        return agentCode;
    }

    public void setAgentCode(String agentCode) {
        this.agentCode = agentCode;
    }

    public String getBankClerk() {
        return bankClerk;
    }

    public void setBankClerk(String bankClerk) {
        this.bankClerk = bankClerk;
    }

    public String getSourceType() {
        return sourceType;
    }

    public void setSourceType(String sourceType) {
        this.sourceType = sourceType;
    }

    public String getTransCode() {
        return transCode;
    }

    public void setTransCode(String transCode) {
        this.transCode = transCode;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getSellType() {
        return sellType;
    }

    public void setSellType(String sellType) {
        this.sellType = sellType;
    }

    public String getInsuSerial() {
        return insuSerial;
    }

    public void setInsuSerial(String insuSerial) {
        this.insuSerial = insuSerial;
    }

    public String getCorpNo() {
        return corpNo;
    }

    public void setCorpNo(String corpNo) {
        this.corpNo = corpNo;
    }

    public String getZoneNo() {
        return zoneNo;
    }

    public void setZoneNo(String zoneNo) {
        this.zoneNo = zoneNo;
    }

    public String getTransSide() {
        return transSide;
    }

    public void setTransSide(String transSide) {
        this.transSide = transSide;
    }

    @Override
    public String toString() {
        return "Head{" +
                "tranDate='" + tranDate + '\'' +
                ", tranTime='" + tranTime + '\'' +
                ", tellerNo='" + tellerNo + '\'' +
                ", tranNo='" + tranNo + '\'' +
                ", nodeNo='" + nodeNo + '\'' +
                ", tranCom='" + tranCom + '\'' +
                ", bankCode='" + bankCode + '\'' +
                ", funcFlag='" + funcFlag + '\'' +
                ", agentCom='" + agentCom + '\'' +
                ", agentCode='" + agentCode + '\'' +
                ", bankClerk='" + bankClerk + '\'' +
                ", sourceType='" + sourceType + '\'' +
                ", transCode='" + transCode + '\'' +
                ", flag='" + flag + '\'' +
                ", desc='" + desc + '\'' +
                ", sellType='" + sellType + '\'' +
                ", insuSerial='" + insuSerial + '\'' +
                ", corpNo='" + corpNo + '\'' +
                ", zoneNo='" + zoneNo + '\'' +
                ", transSide='" + transSide + '\'' +
                '}';
    }
}


