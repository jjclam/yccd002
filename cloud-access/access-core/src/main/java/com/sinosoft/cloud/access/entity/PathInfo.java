package com.sinosoft.cloud.access.entity;

/**
 * abc-cloud-access
 *
 * @author lizhen
 * @create 2019/1/10
 */
public class PathInfo {
    //影像处理
    private String ImagePath;

    public String getImagePath() {
        return ImagePath;
    }

    public void setImagePath(String imagePath) {
        ImagePath = imagePath;
    }

    @Override
    public String toString() {
        return "PathInfo{" +
                "ImagePath='" + ImagePath + '\'' +
                '}';
    }
}