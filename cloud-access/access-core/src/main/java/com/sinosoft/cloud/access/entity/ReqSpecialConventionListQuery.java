package com.sinosoft.cloud.access.entity;

/**
 * @program: abc-cloud-access
 * @description: 特别约定（出境告知）保全项目保单列表查询POS121.
 * @author: BaoYongmeng
 * @create: 2018-12-24 17:27
 **/
public class ReqSpecialConventionListQuery {
    //保全项目编码
    private String EdorType;
    //客户号
    private String CustomerNo;
    //证件号
    private String CustomerIDNo;
    //证件姓名
    private String CustomerName;

    public void setEdorType(String edorType) {
        EdorType = edorType;
    }

    public void setCustomerNo(String customerNo) {
        CustomerNo = customerNo;
    }

    public void setCustomerIDNo(String customerIDNo) {
        CustomerIDNo = customerIDNo;
    }

    public void setCustomerName(String customerName) {
        CustomerName = customerName;
    }

    public String getEdorType() {
        return EdorType;
    }

    public String getCustomerNo() {
        return CustomerNo;
    }

    public String getCustomerIDNo() {
        return CustomerIDNo;
    }

    public String getCustomerName() {
        return CustomerName;
    }

    @Override
    public String toString() {
        return "ReqSpecialConventionListQuery{" +
                "EdorType='" + EdorType + '\'' +
                ", CustomerNo='" + CustomerNo + '\'' +
                ", CustomerIDNo='" + CustomerIDNo + '\'' +
                ", CustomerName='" + CustomerName + '\'' +
                '}';
    }
}
