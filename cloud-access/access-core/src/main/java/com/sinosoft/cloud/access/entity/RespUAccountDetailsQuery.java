package com.sinosoft.cloud.access.entity;
/**
 * 4003保单万能账户追加保费明细查询POS027,POS122
 */
public class RespUAccountDetailsQuery {
    private String EdorType;
    private String ContNo;
    private String AppntName;
    private String RiskName;
    private String NewBankName;
    private String NewBankAccNo;
    private String NewBankAccName;
    private String DutyName;
    private String GetMoney;
    private String Phone;

    public String getEdorType() {
        return EdorType;
    }

    public void setEdorType(String edorType) {
        EdorType = edorType;
    }

    public String getContNo() {
        return ContNo;
    }

    public void setContNo(String contNo) {
        ContNo = contNo;
    }

    public String getAppntName() {
        return AppntName;
    }

    public void setAppntName(String appntName) {
        AppntName = appntName;
    }

    public String getRiskName() {
        return RiskName;
    }

    public void setRiskName(String riskName) {
        RiskName = riskName;
    }

    public String getNewBankName() {
        return NewBankName;
    }

    public void setNewBankName(String newBankName) {
        NewBankName = newBankName;
    }

    public String getNewBankAccNo() {
        return NewBankAccNo;
    }

    public void setNewBankAccNo(String newBankAccNo) {
        NewBankAccNo = newBankAccNo;
    }

    public String getNewBankAccName() {
        return NewBankAccName;
    }

    public void setNewBankAccName(String newBankAccName) {
        NewBankAccName = newBankAccName;
    }

    public String getDutyName() {
        return DutyName;
    }

    public void setDutyName(String dutyName) {
        DutyName = dutyName;
    }

    public String getGetMoney() {
        return GetMoney;
    }

    public void setGetMoney(String getMoney) {
        GetMoney = getMoney;
    }

    public String getPhone() {
        return Phone;
    }

    public void setPhone(String phone) {
        Phone = phone;
    }

    @Override
    public String toString() {
        return "RespUAccountDetailsQuery{" +
                "EdorType='" + EdorType + '\'' +
                ", ContNo='" + ContNo + '\'' +
                ", AppntName='" + AppntName + '\'' +
                ", RiskName='" + RiskName + '\'' +
                ", NewBankName='" + NewBankName + '\'' +
                ", NewBankAccNo='" + NewBankAccNo + '\'' +
                ", NewBankAccName='" + NewBankAccName + '\'' +
                ", DutyName='" + DutyName + '\'' +
                ", GetMoney='" + GetMoney + '\'' +
                ", Phone='" + Phone + '\'' +
                '}';
    }
}
