package com.sinosoft.cloud.access.entity;

/**
 * @Author:lizhen
 * @Description:
 * @Date:Created in 16:51 2018/11/21
 * @Modified By:
 */
/**
 * 生存给付续领账户变更列表查询POS037. 请求
 */
public class ReqPayAccountListQuery {
    //保全项目编码
    private String EdorType;
    //证件号
    private String CustomerIDNo;
    //证件姓名
    private String CustomerName;
    //客户号
    private String CustomerNo;

    public String getEdorType() {
        return EdorType;
    }

    public void setEdorType(String edorType) {
        EdorType = edorType;
    }

    public String getCustomerIDNo() {
        return CustomerIDNo;
    }

    public void setCustomerIDNo(String customerIDNo) {
        CustomerIDNo = customerIDNo;
    }

    public String getCustomerName() {
        return CustomerName;
    }

    public void setCustomerName(String customerName) {
        CustomerName = customerName;
    }

    public String getCustomerNo() {
        return CustomerNo;
    }

    public void setCustomerNo(String customerNo) {
        CustomerNo = customerNo;
    }

    @Override
    public String toString() {
        return "ReqPayAccountListQuery{" +
                "EdorType='" + EdorType + '\'' +
                ", CustomerIDNo='" + CustomerIDNo + '\'' +
                ", CustomerName='" + CustomerName + '\'' +
                ", CustomerNo='" + CustomerNo + '\'' +
                '}';
    }
}