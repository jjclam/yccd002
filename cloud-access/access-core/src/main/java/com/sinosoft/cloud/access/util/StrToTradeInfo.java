package com.sinosoft.cloud.access.util;

import com.sinosoft.cloud.rest.TradeInfo;
import com.sinosoft.utility.Reflections;

import java.lang.reflect.Method;
import java.util.*;

/**
 * @Description
 * @Author:zhaoshulei
 * @Date:2018/2/28
 */
public class StrToTradeInfo {

    public TradeInfo prepareData(String string) {
        TradeInfo tradeInfo = new TradeInfo();
        String str = string;
        Reflections reflections = new Reflections();
        //截取trademap数据
        str = str.substring(str.indexOf("tradeMap={") + 10, str.indexOf("}"));
        String[] objs = str.split("com.sinosoft.lis.entity.");
        List<String> arrs = Arrays.asList(objs);
        int start = -1;
        int end = -1;
        int eq = -1;
        Map<String, String> map = new HashMap();
        //解析tradeinfo
        for (int i = 0; i < arrs.size(); i++) {
            String arr = arrs.get(i);
            start = arr.indexOf("[");
            end = arr.lastIndexOf("]");
            eq = arr.indexOf("=");
            if (start != -1 && end != -1 && eq != -1) {
                map.put(arr.substring(0, eq).trim(), arr.substring(start + 1, end));
            }
            start = arr.indexOf("{");
            end = arr.indexOf("}");
            if (start != -1 && end != -1 && end == arr.lastIndexOf("}")) {

            }
        }
        //解析类
        String value = null;
        Method setMethod = null;
        Class className = null;
        /*Field field = null;*/
        Object obj = null;
        for (String key : map.keySet()) {
            value = map.get(key);
            if ("".equals(value)) {
                continue;
            }
            //指定类
            try {
                className = Class.forName("com.sinosoft.lis.entity." + key);
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }

            start = value.indexOf("[");
            end = value.indexOf("]");
            //解析value中有几个[]，判断是集合对象还是类对象
            if (start == -1) {
                obj = analyseData(value, className, obj, setMethod);
                tradeInfo.addData(className.getName(), obj);
            } else {
                //判断是否存在字符串格式的数据
                if (value.lastIndexOf("]") != value.length() - 1) {
                    String otherVal = value.substring(value.lastIndexOf("]") + 2);
                    for (String val : otherVal.split(",")) {
                        tradeInfo.addData(val.split("=")[0].trim(), val.split("=")[1].trim());
                    }
                }
                //集合对象遍历
                List classList = new ArrayList();
                while (start != -1) {
                    classList.add(analyseData(value.substring(start + 1, end), className, obj, setMethod));
                    value = value.substring(end + 1);
                    start = value.indexOf("[");
                    end = value.indexOf("]");
                }
                if (classList.size() != 0) {
                    tradeInfo.addData(className.getName(), classList);
                }
            }
        }
        System.out.println("--------------------------------------------------------------");
        System.out.println("結果為：" + tradeInfo);
        return tradeInfo;
    }

    public Object analyseData(String value, Class className, Object obj, Method setMethod) {
        String[] vals = value.split(",");
        try {
            obj = className.newInstance();
            //遍历属性
            for (String var : vals) {
                if (var.split("=").length < 2) {
                    continue;
                }
                //变量名
                String na = var.split("=")[0].trim();
                //变量值
                String va = var.split("=")[1].trim();
                //指定类的成员变量
                        /*field = className.getDeclaredField(na);*/
                //获取指定成员变量的set方法
                setMethod = className.getMethod("set" + na.substring(0, 1).toUpperCase() + na.substring(1), String.class);
                if (va != null && !"null".equals(va)) {
                    setMethod.invoke(obj, new Object[]{va});
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return obj;
    }

}
