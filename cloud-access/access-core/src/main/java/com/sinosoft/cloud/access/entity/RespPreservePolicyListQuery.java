package com.sinosoft.cloud.access.entity;

/**
 * @program: abc-cloud-access
 * @description: 生存给付领取方式保全项目保单列表查询POS120.
 * @author: BaoYongmeng
 * @create: 2018-12-25 11:18
 **/
public class RespPreservePolicyListQuery {
    //保全项目编码
    private String EdorType;

    private PolicyQueryInfos PolicyQueryInfos;
    private PhoneInfos PhoneInfos;

    public void setEdorType(String edorType) {
        EdorType = edorType;
    }

    public void setPolicyQueryInfos(com.sinosoft.cloud.access.entity.PolicyQueryInfos policyQueryInfos) {
        PolicyQueryInfos = policyQueryInfos;
    }

    public void setPhoneInfos(com.sinosoft.cloud.access.entity.PhoneInfos phoneInfos) {
        PhoneInfos = phoneInfos;
    }

    public String getEdorType() {
        return EdorType;
    }

    public com.sinosoft.cloud.access.entity.PolicyQueryInfos getPolicyQueryInfos() {
        return PolicyQueryInfos;
    }

    public com.sinosoft.cloud.access.entity.PhoneInfos getPhoneInfos() {
        return PhoneInfos;
    }

    @Override
    public String toString() {
        return "RespPreservePolicyListQuery{" +
                "EdorType='" + EdorType + '\'' +
                ", PolicyQueryInfos=" + PolicyQueryInfos +
                ", PhoneInfos=" + PhoneInfos +
                '}';
    }
}
