package com.sinosoft.cloud.access.entity;

/**
 * @Author:lizhen
 * @Description:
 * @Date:Created in 14:01 2018/11/14
 * @Modified By:
 */
public class AddtRisk {
    //险种编码
    private String RiskCode;
    //附加险险种名称
    private String RiskName;
    //保额
    private String Amnt;
    //保费
    private String Prem;
    //份数
    private String Mult;
    //生存金领取标志/续保标记
    private String XBFlag;
    //生存金领取方式/续保方式
    private String XBPattern;
    //被保人姓名
    private String InsuredName;
    //赔付日期/缴费对应日
    private String PayDate;
    //现金价值
    private String CashValue;
    //健康加费
    private String HealthPrem;
    //职业加费
    private String JobPrem;
    //给付金额
    private String RiskgetMoney;
    //附加险编码
    private String AddtRiskCode;
    //附加险名称
    private String AddtRiskName;
    //附加险保额
    private String AddtAmnt;
    //附加险保费
    private String AddtPrem;
    //现金价值
    private String AddtCashValue;
    //附加险份数
    private String AddtMult;
    //附加险生存金领取标志/续保标记
    private String AddtXBFlag;
    //附加险生存金领取方式/续保方式
    private String AddtXBPattern;
    //附加险被保人姓名
    private String AddtInsuredName;
    //附加险赔付日期/缴费对应日
    private String AddtPayDate;
    //附加险健康加费
    private String AddtHealthPrem;
    //附加险职业加费
    private String AddtJobPrem;

    public String getRiskCode() {
        return RiskCode;
    }

    public void setRiskCode(String riskCode) {
        RiskCode = riskCode;
    }

    public String getRiskName() {
        return RiskName;
    }

    public void setRiskName(String riskName) {
        RiskName = riskName;
    }

    public String getAmnt() {
        return Amnt;
    }

    public void setAmnt(String amnt) {
        Amnt = amnt;
    }

    public String getPrem() {
        return Prem;
    }

    public void setPrem(String prem) {
        Prem = prem;
    }

    public String getMult() {
        return Mult;
    }

    public void setMult(String mult) {
        Mult = mult;
    }

    public String getXBFlag() {
        return XBFlag;
    }

    public void setXBFlag(String XBFlag) {
        this.XBFlag = XBFlag;
    }

    public String getXBPattern() {
        return XBPattern;
    }

    public void setXBPattern(String XBPattern) {
        this.XBPattern = XBPattern;
    }

    public String getInsuredName() {
        return InsuredName;
    }

    public void setInsuredName(String insuredName) {
        InsuredName = insuredName;
    }

    public String getPayDate() {
        return PayDate;
    }

    public void setPayDate(String payDate) {
        PayDate = payDate;
    }

    public String getCashValue() {
        return CashValue;
    }

    public void setCashValue(String cashValue) {
        CashValue = cashValue;
    }

    public String getHealthPrem() {
        return HealthPrem;
    }

    public void setHealthPrem(String healthPrem) {
        HealthPrem = healthPrem;
    }

    public String getJobPrem() {
        return JobPrem;
    }

    public void setJobPrem(String jobPrem) {
        JobPrem = jobPrem;
    }

    public String getRiskgetMoney() {
        return RiskgetMoney;
    }

    public void setRiskgetMoney(String riskgetMoney) {
        RiskgetMoney = riskgetMoney;
    }

    public String getAddtRiskCode() {
        return AddtRiskCode;
    }

    public void setAddtRiskCode(String addtRiskCode) {
        AddtRiskCode = addtRiskCode;
    }

    public String getAddtRiskName() {
        return AddtRiskName;
    }

    public void setAddtRiskName(String addtRiskName) {
        AddtRiskName = addtRiskName;
    }

    public String getAddtAmnt() {
        return AddtAmnt;
    }

    public void setAddtAmnt(String addtAmnt) {
        AddtAmnt = addtAmnt;
    }

    public String getAddtPrem() {
        return AddtPrem;
    }

    public void setAddtPrem(String addtPrem) {
        AddtPrem = addtPrem;
    }

    public String getAddtCashValue() {
        return AddtCashValue;
    }

    public void setAddtCashValue(String addtCashValue) {
        AddtCashValue = addtCashValue;
    }

    public String getAddtMult() {
        return AddtMult;
    }

    public void setAddtMult(String addtMult) {
        AddtMult = addtMult;
    }

    public String getAddtXBFlag() {
        return AddtXBFlag;
    }

    public void setAddtXBFlag(String addtXBFlag) {
        AddtXBFlag = addtXBFlag;
    }

    public String getAddtXBPattern() {
        return AddtXBPattern;
    }

    public void setAddtXBPattern(String addtXBPattern) {
        AddtXBPattern = addtXBPattern;
    }

    public String getAddtInsuredName() {
        return AddtInsuredName;
    }

    public void setAddtInsuredName(String addtInsuredName) {
        AddtInsuredName = addtInsuredName;
    }

    public String getAddtPayDate() {
        return AddtPayDate;
    }

    public void setAddtPayDate(String addtPayDate) {
        AddtPayDate = addtPayDate;
    }

    public String getAddtHealthPrem() {
        return AddtHealthPrem;
    }

    public void setAddtHealthPrem(String addtHealthPrem) {
        AddtHealthPrem = addtHealthPrem;
    }

    public String getAddtJobPrem() {
        return AddtJobPrem;
    }

    public void setAddtJobPrem(String addtJobPrem) {
        AddtJobPrem = addtJobPrem;
    }

    @Override
    public String toString() {
        return "AddtRisk{" +
                "RiskCode='" + RiskCode + '\'' +
                ", RiskName='" + RiskName + '\'' +
                ", Amnt='" + Amnt + '\'' +
                ", Prem='" + Prem + '\'' +
                ", Mult='" + Mult + '\'' +
                ", XBFlag='" + XBFlag + '\'' +
                ", XBPattern='" + XBPattern + '\'' +
                ", InsuredName='" + InsuredName + '\'' +
                ", PayDate='" + PayDate + '\'' +
                ", CashValue='" + CashValue + '\'' +
                ", HealthPrem='" + HealthPrem + '\'' +
                ", JobPrem='" + JobPrem + '\'' +
                ", RiskgetMoney='" + RiskgetMoney + '\'' +
                ", AddtRiskCode='" + AddtRiskCode + '\'' +
                ", AddtRiskName='" + AddtRiskName + '\'' +
                ", AddtAmnt='" + AddtAmnt + '\'' +
                ", AddtPrem='" + AddtPrem + '\'' +
                ", AddtCashValue='" + AddtCashValue + '\'' +
                ", AddtMult='" + AddtMult + '\'' +
                ", AddtXBFlag='" + AddtXBFlag + '\'' +
                ", AddtXBPattern='" + AddtXBPattern + '\'' +
                ", AddtInsuredName='" + AddtInsuredName + '\'' +
                ", AddtPayDate='" + AddtPayDate + '\'' +
                ", AddtHealthPrem='" + AddtHealthPrem + '\'' +
                ", AddtJobPrem='" + AddtJobPrem + '\'' +
                '}';
    }
}