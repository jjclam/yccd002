package com.sinosoft.cloud.access.entity;

import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.List;

/**
 * @Description
 * @Author:zhaoshulei
 * @Date:2018/4/10
 */
public class LMRiskApps {

    @XStreamImplicit(itemFieldName="LMRiskApp")
    private List<LMRiskApp> LMRiskApps;

    public List<LMRiskApp> getLMRiskApps() {
        return LMRiskApps;
    }

    public void setLMRiskApps(List<LMRiskApp> LMRiskApps) {
        this.LMRiskApps = LMRiskApps;
    }

    @Override
    public String toString() {
        return "LMRiskApps{" +
                "LMRiskApps=" + LMRiskApps +
                '}';
    }
}
