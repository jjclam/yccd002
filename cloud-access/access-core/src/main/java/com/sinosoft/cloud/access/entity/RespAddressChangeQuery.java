package com.sinosoft.cloud.access.entity;

/**
 * @Author:lizhen
 * @Description:
 * @Date:Created in 19:47 2018/11/15
 * @Modified By:
 */
/**
 * 地址信息变更获取客户地址POS069 返回
 */
public class RespAddressChangeQuery {
    //请求类型
    private String EdorType;
    //客户号
    private String CustomerNo;
    //证件号码
    private String CustomerIDNo;
    //证件姓名
    private String CustomerName;
    //保单列表
    private Policys Policys;

    public String getEdorType() {
        return EdorType;
    }

    public void setEdorType(String edorType) {
        EdorType = edorType;
    }

    public String getCustomerNo() {
        return CustomerNo;
    }

    public void setCustomerNo(String customerNo) {
        CustomerNo = customerNo;
    }

    public String getCustomerIDNo() {
        return CustomerIDNo;
    }

    public void setCustomerIDNo(String customerIDNo) {
        CustomerIDNo = customerIDNo;
    }

    public String getCustomerName() {
        return CustomerName;
    }

    public void setCustomerName(String customerName) {
        CustomerName = customerName;
    }

    public Policys getPolicys() {
        return Policys;
    }

    public void setPolicys(Policys policys) {
        Policys = policys;
    }

    @Override
    public String toString() {
        return "RespAddressChangeQuery{" +
                "EdorType='" + EdorType + '\'' +
                ", CustomerNo='" + CustomerNo + '\'' +
                ", CustomerIDNo='" + CustomerIDNo + '\'' +
                ", CustomerName='" + CustomerName + '\'' +
                ", Policys=" + Policys +
                '}';
    }
}