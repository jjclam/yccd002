package com.sinosoft.cloud.access.entity;

/**
 * @Author:lizhen
 * @Description:
 * @Date:Created in 17:47 2018/11/9
 * @Modified By:
 */
public class ProgressQueryInfo {

    private String ContNo;
    private String EdorType;
    private String EndorAppDate;
    private String BQHandleStatus;
    private String EdorNo;
    private String BQAcceptNo;

    public String getContNo() {
        return ContNo;
    }

    public void setContNo(String contNo) {
        ContNo = contNo;
    }

    public String getEdorType() {
        return EdorType;
    }

    public void setEdorType(String edorType) {
        EdorType = edorType;
    }

    public String getEndorAppDate() {
        return EndorAppDate;
    }

    public void setEndorAppDate(String endorAppDate) {
        EndorAppDate = endorAppDate;
    }

    public String getBQHandleStatus() {
        return BQHandleStatus;
    }

    public void setBQHandleStatus(String BQHandleStatus) {
        this.BQHandleStatus = BQHandleStatus;
    }

    public String getEdorNo() {
        return EdorNo;
    }

    public void setEdorNo(String edorNo) {
        EdorNo = edorNo;
    }

    public String getBQAcceptNo() {
        return BQAcceptNo;
    }

    public void setBQAcceptNo(String BQAcceptNo) {
        this.BQAcceptNo = BQAcceptNo;
    }

    @Override
    public String toString() {
        return "ProgressQueryInfo{" +
                "ContNo='" + ContNo + '\'' +
                ", EdorType='" + EdorType + '\'' +
                ", EndorAppDate='" + EndorAppDate + '\'' +
                ", BQHandleStatus='" + BQHandleStatus + '\'' +
                ", EdorNo='" + EdorNo + '\'' +
                ", BQAcceptNo='" + BQAcceptNo + '\'' +
                '}';
    }
}