package com.sinosoft.cloud.access.entity;

/**
 * @Author:lizhen
 * @Description:
 * @Date:Created in 16:46 2018/11/16
 * @Modified By:
 */
public class RespCustomerAddressEmailChangeApply {
    //保全项目编码
    private String EdorType;
    //客户号
    private String CustomerNo;
    //客户身份证
    private String CustomerIDNo;
    //客户姓名
    private String CustomerName;

    //保单查询信息
    private PolicyQueryInfos PolicyQueryInfos;

    public void setEdorType(String edorType) {
        EdorType = edorType;
    }

    public void setCustomerNo(String customerNo) {
        CustomerNo = customerNo;
    }

    public void setCustomerIDNo(String customerIDNo) {
        CustomerIDNo = customerIDNo;
    }

    public void setCustomerName(String customerName) {
        CustomerName = customerName;
    }

    public void setPolicyQueryInfos(com.sinosoft.cloud.access.entity.PolicyQueryInfos policyQueryInfos) {
        PolicyQueryInfos = policyQueryInfos;
    }

    public String getEdorType() {
        return EdorType;
    }

    public String getCustomerNo() {
        return CustomerNo;
    }

    public String getCustomerIDNo() {
        return CustomerIDNo;
    }

    public String getCustomerName() {
        return CustomerName;
    }

    public com.sinosoft.cloud.access.entity.PolicyQueryInfos getPolicyQueryInfos() {
        return PolicyQueryInfos;
    }

    @Override
    public String toString() {
        return "RespCustomerAddressEmailChangeApply{" +
                "EdorType='" + EdorType + '\'' +
                ", CustomerNo='" + CustomerNo + '\'' +
                ", CustomerIDNo='" + CustomerIDNo + '\'' +
                ", CustomerName='" + CustomerName + '\'' +
                ", PolicyQueryInfos=" + PolicyQueryInfos +
                '}';
    }
}