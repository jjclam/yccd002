package com.sinosoft.cloud.access.entity;

import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.List;

/**
 * @Author:lizhen
 * @Description:
 * @Date:Created in 14:28 2018/11/13
 * @Modified By:
 */
public class UpdateContInfos {
    @XStreamImplicit(itemFieldName="UpdateInfo")
    private List<UpdateInfo> UpdateInfo;

    public List<UpdateInfo> getUpdateInfo() {
        return UpdateInfo;
    }

    public void setUpdateInfo(List<UpdateInfo> updateInfo) {
        UpdateInfo = updateInfo;
    }

    @Override
    public String toString() {
        return "UpdateContInfos{" +
                "UpdateInfo=" + UpdateInfo +
                '}';
    }
}