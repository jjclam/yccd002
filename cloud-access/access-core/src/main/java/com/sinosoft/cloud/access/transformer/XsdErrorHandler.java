package com.sinosoft.cloud.access.transformer;

import org.dom4j.Attribute;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.xml.sax.ErrorHandler;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

import javax.xml.stream.XMLStreamReader;
import java.io.*;
import java.util.List;

import static com.sinosoft.cloud.access.configuration.AccessConfiguration.BASE_PACKAGE;
import static com.sinosoft.cloud.access.transformer.XsdValidator.XSD_VALIDATION_FILE;


/**
 * xsd校验出现错误的处理类.
 *
 * @Author: cuiguangdong
 * @Date: 2018/4/9 14:03
 * @Description: 在xsd校验出现错误的时候，抓取xsd配置文件中错误的取值
 */
public class XsdErrorHandler implements ErrorHandler {
    private String errorElement = null;
    private XMLStreamReader reader;
    private String accessName;

    public XsdErrorHandler(XMLStreamReader reader, String accessName) {
        this.reader = reader;
        this.accessName = accessName;
    }

    @Override
    public void warning(SAXParseException e) throws SAXException {
        fatalError(e);
    }

    @Override
    public void error(SAXParseException e) throws SAXException {
        fatalError(e);
    }

    @Override
    public void fatalError(SAXParseException e) throws SAXException {
        String lement = reader.getLocalName();
        String msg = e.getMessage();
        System.out.println(msg);

        if (null != this.errorElement && !"".equals(this.errorElement)) {
            return;
        }

        SAXReader saxReader = new SAXReader();
        try {

            Resource resource = new ClassPathResource(BASE_PACKAGE + accessName + XSD_VALIDATION_FILE);
//            File file = null;
            InputStream inputStream = null;
            StringBuffer str = new StringBuffer(1024);
            BufferedReader br = null;
            try {
//                file = resource.getFile();
                inputStream = resource.getInputStream();
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                br = new BufferedReader(inputStreamReader);
                String data = new String();
                while ((data = br.readLine()) != null) {
                    str.append(data + "");
                }
            } catch (IOException e1) {
                e1.printStackTrace();
            }

//            Document doc = saxReader.read(file);
            Document doc = saxReader.read(new StringReader(str.toString()));
            Element root = doc.getRootElement();
            List<Element> list = root.elements("element");
//            Iterator<Node> nodeIterator = element1.nodeIterator();
            for (Element element : list) {
                List<Attribute> attributes = element.attributes();
                for (Attribute attribute : attributes) {
                    String name = attribute.getName();
                    String value = attribute.getValue();
                    if (value.equals(lement)) {
                        String lang = element.attribute("lang").getValue();
                        this.errorElement = lang;
                        return;
                    }
                }
            }

        } catch (DocumentException e1) {
            e1.printStackTrace();
        }
//        this.errorElement=lement+":"+msg;
    }

    public String getErrorElement() {
        return errorElement;
    }

    public void setErrorElement(String errorElement) {
        this.errorElement = errorElement;
    }

    public XMLStreamReader getReader() {
        return reader;
    }

    public void setReader(XMLStreamReader reader) {
        this.reader = reader;
    }
}
