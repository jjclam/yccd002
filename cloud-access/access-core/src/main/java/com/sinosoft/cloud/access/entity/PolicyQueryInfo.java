package com.sinosoft.cloud.access.entity;

/**
 * @Author:lizhen
 * @Description:
 * @Date:Created in 17:46 2018/11/9
 * @Modified By:
 */
public class PolicyQueryInfo {
    //保单号
    private String ContNo;
    //主险名称
    private String MainRiskName;
    //生效日期
    private String ValiDate;
    //总保费
    private String TotalPrem;
    //返还时间
    private String ReturnDate;
    //保全号
    private String BQAcceptNo;

    //申请人姓名
    private String CustomerName;
    //申请人身份证
    private String CustomerIdNo;
    //申请日期
    private String ApplyDate;
    //开户xxx
    private String BankCode;
    //xxx账号
    private String BankAccNo;
    //申请人信息
    private EdorAppInfo EdorAppInfo;
    private PersonInfo PersonInfo;
    private BankInfo BankInfo;
    //收付费方式
    private String PayMode;

    //每个保单对应一个成功失败返回
    private String ResultType;
    //响应信息
    private String ResultMessage;
    //保全受理号
    private String EdorAcceptNo;
    //保全批单号
    private String EdorNo;

    //保单生效日期
    private String CValiDate;
    //保全状态
    private String EdorStatus;

    //投保人姓名
    private String AppntName;
    //邮箱地址
    private String Email;
    //通讯地址
    private String PostalAddress;
    //邮编
    private String ZipCode;
    //文本受理标志
    private String TextAcceptFlag;

    //被保人姓名
    private String InsuredName;
    //保单状态
    private String PolicyStatus;
    //证件有效期
    private String IDValidDate;
    //险种编码
    private String RiskCode;
    //组合编码
    private String ProdSetCode;
    //组合名称
    private String ProdSetName;
    //该客户是否只作为受益人参与变更的标识 1-是 0-否
    private String BnfFlag;

    //标记区分邮箱变更还是地址变更 0 邮箱 1-地址
    private String BussType;
    //省
    private String Province;
    //市
    private String City;
    //县
    private String Powiat;
    //镇（乡）/街道
    private String Street;
    //村/社区（楼号)
    private String Village;
    //手机号码
    private String MobilePhone;
    //投保人客户号
    private String CustomerNo;
    //国籍
    private String Nationality;
    //是否为美国纳税义务的个人
    private String TinFlag;
    //美国纳税人识别号
    private String TinNo;


    //贷款本金
    private String LoanMoney;
    //贷款日期
    private String LoanDate;
    //保费自垫日期
    private String AdcanceDate;

    //保全项
    private String EdorItem;
    //挂失状态
    private String LossFlag;
    //保全生效日
    private String BQValiDate;
    //状态
    private String Status;
    //通讯地址区/县
    private String County;
    //常住地址村/社区（楼、号）
    private String StoreNo;
    //主险编码
    private String MainRiskNo;
    //通知书类型
    private String XHFlag;

    public String getContNo() {
        return ContNo;
    }

    public void setContNo(String contNo) {
        ContNo = contNo;
    }

    public String getMainRiskName() {
        return MainRiskName;
    }

    public void setMainRiskName(String mainRiskName) {
        MainRiskName = mainRiskName;
    }

    public String getValiDate() {
        return ValiDate;
    }

    public void setValiDate(String valiDate) {
        ValiDate = valiDate;
    }

    public String getTotalPrem() {
        return TotalPrem;
    }

    public void setTotalPrem(String totalPrem) {
        TotalPrem = totalPrem;
    }

    public String getReturnDate() {
        return ReturnDate;
    }

    public void setReturnDate(String returnDate) {
        ReturnDate = returnDate;
    }

    public String getBQAcceptNo() {
        return BQAcceptNo;
    }

    public void setBQAcceptNo(String BQAcceptNo) {
        this.BQAcceptNo = BQAcceptNo;
    }

    public String getCustomerName() {
        return CustomerName;
    }

    public void setCustomerName(String customerName) {
        CustomerName = customerName;
    }

    public String getCustomerIdNo() {
        return CustomerIdNo;
    }

    public void setCustomerIdNo(String customerIdNo) {
        CustomerIdNo = customerIdNo;
    }

    public String getApplyDate() {
        return ApplyDate;
    }

    public void setApplyDate(String applyDate) {
        ApplyDate = applyDate;
    }

    public String getBankCode() {
        return BankCode;
    }

    public void setBankCode(String bankCode) {
        BankCode = bankCode;
    }

    public String getBankAccNo() {
        return BankAccNo;
    }

    public void setBankAccNo(String bankAccNo) {
        BankAccNo = bankAccNo;
    }

    public com.sinosoft.cloud.access.entity.EdorAppInfo getEdorAppInfo() {
        return EdorAppInfo;
    }

    public void setEdorAppInfo(com.sinosoft.cloud.access.entity.EdorAppInfo edorAppInfo) {
        EdorAppInfo = edorAppInfo;
    }

    public com.sinosoft.cloud.access.entity.PersonInfo getPersonInfo() {
        return PersonInfo;
    }

    public void setPersonInfo(com.sinosoft.cloud.access.entity.PersonInfo personInfo) {
        PersonInfo = personInfo;
    }

    public com.sinosoft.cloud.access.entity.BankInfo getBankInfo() {
        return BankInfo;
    }

    public void setBankInfo(com.sinosoft.cloud.access.entity.BankInfo bankInfo) {
        BankInfo = bankInfo;
    }

    public String getPayMode() {
        return PayMode;
    }

    public void setPayMode(String payMode) {
        PayMode = payMode;
    }

    public String getResultType() {
        return ResultType;
    }

    public void setResultType(String resultType) {
        ResultType = resultType;
    }

    public String getResultMessage() {
        return ResultMessage;
    }

    public void setResultMessage(String resultMessage) {
        ResultMessage = resultMessage;
    }

    public String getEdorAcceptNo() {
        return EdorAcceptNo;
    }

    public void setEdorAcceptNo(String edorAcceptNo) {
        EdorAcceptNo = edorAcceptNo;
    }

    public String getEdorNo() {
        return EdorNo;
    }

    public void setEdorNo(String edorNo) {
        EdorNo = edorNo;
    }

    public String getCValiDate() {
        return CValiDate;
    }

    public void setCValiDate(String CValiDate) {
        this.CValiDate = CValiDate;
    }

    public String getEdorStatus() {
        return EdorStatus;
    }

    public void setEdorStatus(String edorStatus) {
        EdorStatus = edorStatus;
    }

    public String getAppntName() {
        return AppntName;
    }

    public void setAppntName(String appntName) {
        AppntName = appntName;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getPostalAddress() {
        return PostalAddress;
    }

    public void setPostalAddress(String postalAddress) {
        PostalAddress = postalAddress;
    }

    public String getZipCode() {
        return ZipCode;
    }

    public void setZipCode(String zipCode) {
        ZipCode = zipCode;
    }

    public String getTextAcceptFlag() {
        return TextAcceptFlag;
    }

    public void setTextAcceptFlag(String textAcceptFlag) {
        TextAcceptFlag = textAcceptFlag;
    }

    public String getInsuredName() {
        return InsuredName;
    }

    public void setInsuredName(String insuredName) {
        InsuredName = insuredName;
    }

    public String getPolicyStatus() {
        return PolicyStatus;
    }

    public void setPolicyStatus(String policyStatus) {
        PolicyStatus = policyStatus;
    }

    public String getIDValidDate() {
        return IDValidDate;
    }

    public void setIDValidDate(String IDValidDate) {
        this.IDValidDate = IDValidDate;
    }

    public String getRiskCode() {
        return RiskCode;
    }

    public void setRiskCode(String riskCode) {
        RiskCode = riskCode;
    }

    public String getProdSetCode() {
        return ProdSetCode;
    }

    public void setProdSetCode(String prodSetCode) {
        ProdSetCode = prodSetCode;
    }

    public String getProdSetName() {
        return ProdSetName;
    }

    public void setProdSetName(String prodSetName) {
        ProdSetName = prodSetName;
    }

    public String getBnfFlag() {
        return BnfFlag;
    }

    public void setBnfFlag(String bnfFlag) {
        BnfFlag = bnfFlag;
    }

    public String getBussType() {
        return BussType;
    }

    public void setBussType(String bussType) {
        BussType = bussType;
    }

    public String getProvince() {
        return Province;
    }

    public void setProvince(String province) {
        Province = province;
    }

    public String getCity() {
        return City;
    }

    public void setCity(String city) {
        City = city;
    }

    public String getPowiat() {
        return Powiat;
    }

    public void setPowiat(String powiat) {
        Powiat = powiat;
    }

    public String getStreet() {
        return Street;
    }

    public void setStreet(String street) {
        Street = street;
    }

    public String getVillage() {
        return Village;
    }

    public void setVillage(String village) {
        Village = village;
    }

    public String getMobilePhone() {
        return MobilePhone;
    }

    public void setMobilePhone(String mobilePhone) {
        MobilePhone = mobilePhone;
    }

    public String getCustomerNo() {
        return CustomerNo;
    }

    public void setCustomerNo(String customerNo) {
        CustomerNo = customerNo;
    }

    public String getNationality() {
        return Nationality;
    }

    public void setNationality(String nationality) {
        Nationality = nationality;
    }

    public String getTinFlag() {
        return TinFlag;
    }

    public void setTinFlag(String tinFlag) {
        TinFlag = tinFlag;
    }

    public String getTinNo() {
        return TinNo;
    }

    public void setTinNo(String tinNo) {
        TinNo = tinNo;
    }

    public String getLoanMoney() {
        return LoanMoney;
    }

    public void setLoanMoney(String loanMoney) {
        LoanMoney = loanMoney;
    }

    public String getLoanDate() {
        return LoanDate;
    }

    public void setLoanDate(String loanDate) {
        LoanDate = loanDate;
    }

    public String getAdcanceDate() {
        return AdcanceDate;
    }

    public void setAdcanceDate(String adcanceDate) {
        AdcanceDate = adcanceDate;
    }

    public String getEdorItem() {
        return EdorItem;
    }

    public void setEdorItem(String edorItem) {
        EdorItem = edorItem;
    }

    public String getLossFlag() {
        return LossFlag;
    }

    public void setLossFlag(String lossFlag) {
        LossFlag = lossFlag;
    }

    public String getBQValiDate() {
        return BQValiDate;
    }

    public void setBQValiDate(String BQValiDate) {
        this.BQValiDate = BQValiDate;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

    public String getCounty() {
        return County;
    }

    public void setCounty(String county) {
        County = county;
    }

    public String getStoreNo() {
        return StoreNo;
    }

    public void setStoreNo(String storeNo) {
        StoreNo = storeNo;
    }

    public String getMainRiskNo() {
        return MainRiskNo;
    }

    public void setMainRiskNo(String mainRiskNo) {
        MainRiskNo = mainRiskNo;
    }

    public String getXHFlag() {
        return XHFlag;
    }

    public void setXHFlag(String XHFlag) {
        this.XHFlag = XHFlag;
    }

    @Override
    public String toString() {
        return "PolicyQueryInfo{" +
                "ContNo='" + ContNo + '\'' +
                ", MainRiskName='" + MainRiskName + '\'' +
                ", ValiDate='" + ValiDate + '\'' +
                ", TotalPrem='" + TotalPrem + '\'' +
                ", ReturnDate='" + ReturnDate + '\'' +
                ", BQAcceptNo='" + BQAcceptNo + '\'' +
                ", CustomerName='" + CustomerName + '\'' +
                ", CustomerIdNo='" + CustomerIdNo + '\'' +
                ", ApplyDate='" + ApplyDate + '\'' +
                ", BankCode='" + BankCode + '\'' +
                ", BankAccNo='" + BankAccNo + '\'' +
                ", EdorAppInfo=" + EdorAppInfo +
                ", PersonInfo=" + PersonInfo +
                ", BankInfo=" + BankInfo +
                ", PayMode='" + PayMode + '\'' +
                ", ResultType='" + ResultType + '\'' +
                ", ResultMessage='" + ResultMessage + '\'' +
                ", EdorAcceptNo='" + EdorAcceptNo + '\'' +
                ", EdorNo='" + EdorNo + '\'' +
                ", CValiDate='" + CValiDate + '\'' +
                ", EdorStatus='" + EdorStatus + '\'' +
                ", AppntName='" + AppntName + '\'' +
                ", Email='" + Email + '\'' +
                ", PostalAddress='" + PostalAddress + '\'' +
                ", ZipCode='" + ZipCode + '\'' +
                ", TextAcceptFlag='" + TextAcceptFlag + '\'' +
                ", InsuredName='" + InsuredName + '\'' +
                ", PolicyStatus='" + PolicyStatus + '\'' +
                ", IDValidDate='" + IDValidDate + '\'' +
                ", RiskCode='" + RiskCode + '\'' +
                ", ProdSetCode='" + ProdSetCode + '\'' +
                ", ProdSetName='" + ProdSetName + '\'' +
                ", BnfFlag='" + BnfFlag + '\'' +
                ", BussType='" + BussType + '\'' +
                ", Province='" + Province + '\'' +
                ", City='" + City + '\'' +
                ", Powiat='" + Powiat + '\'' +
                ", Street='" + Street + '\'' +
                ", Village='" + Village + '\'' +
                ", MobilePhone='" + MobilePhone + '\'' +
                ", CustomerNo='" + CustomerNo + '\'' +
                ", Nationality='" + Nationality + '\'' +
                ", TinFlag='" + TinFlag + '\'' +
                ", TinNo='" + TinNo + '\'' +
                ", LoanMoney='" + LoanMoney + '\'' +
                ", LoanDate='" + LoanDate + '\'' +
                ", AdcanceDate='" + AdcanceDate + '\'' +
                ", EdorItem='" + EdorItem + '\'' +
                ", LossFlag='" + LossFlag + '\'' +
                ", BQValiDate='" + BQValiDate + '\'' +
                ", Status='" + Status + '\'' +
                ", County='" + County + '\'' +
                ", StoreNo='" + StoreNo + '\'' +
                ", MainRiskNo='" + MainRiskNo + '\'' +
                ", XHFlag='" + XHFlag + '\'' +
                '}';
    }
}

