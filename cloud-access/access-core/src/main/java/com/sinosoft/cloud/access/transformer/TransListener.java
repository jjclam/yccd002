package com.sinosoft.cloud.access.transformer;

/**
 * cloud-access
 *
 * @title: cloud-access
 * @package: com.sinosoft.cloud.access.transformer
 * @author: yangming
 * @date: 2017/11/23 下午1:44
 */
public interface TransListener {

    String beforeTransEvent(String msg, String accessName);

    String afterTransEvent(String msg);

}
