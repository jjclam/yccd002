package com.sinosoft.cloud.access.entity;

import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.List;

/**
 * @Author:lizhen
 * @Description:
 * @Date:Created in 17:15 2018/9/14
 * @Modified By:
 */
public class LJTempFees {
    @XStreamImplicit(itemFieldName="LJTempFee")
    private List<LJTempFee> LJTempFees;

    public List<LJTempFee> getLJTempFees() {
        return LJTempFees;
    }

    public void setLJTempFees(List<LJTempFee> LJTempFees) {
        this.LJTempFees = LJTempFees;
    }

    @Override
    public String toString() {
        return "LJTempFees{" +
                "LJTempFees=" + LJTempFees +
                '}';
    }
}