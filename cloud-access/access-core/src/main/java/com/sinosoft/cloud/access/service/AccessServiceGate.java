package com.sinosoft.cloud.access.service;

import com.sinosoft.cloud.access.entity.AliHead;
import com.sinosoft.cloud.access.entity.Head;

import java.util.HashMap;

public class AccessServiceGate {

    private AccessServiceGate() {
    }

    private static AccessServiceGate accessServiceGate = new AccessServiceGate();

    public static AccessServiceGate getInstance() {
        return accessServiceGate;
    }

    private HashMap<String, AccessServiceBean> serviceMap = new HashMap();


    public AccessServiceBean getServiceByAccessTransCode(String accessName, String transCode) {


        AccessServiceBean accessServiceBean = serviceMap.get(accessName + transCode);
        if (accessServiceBean == null) {
            return null;
        }

        return accessServiceBean;
    }

    public void addService(String accessName, String transCode, AccessServiceBean accessServiceBean) {
        serviceMap.put(accessName + transCode, accessServiceBean);
    }

    public AccessServiceBean getServiceByTranData(String accessName, String transType) {

        AccessServiceBean accessServiceBean = getServiceByAccessTransCode(accessName, transType);

        return accessServiceBean;
    }

}



