package com.sinosoft.cloud.access.transformer;

import com.sinosoft.cloud.access.entity.Head;
import com.sinosoft.cloud.access.entity.TranData;
import org.springframework.core.io.Resource;

public interface Trans {


    Head headerTrans(String msg);

    TranData bodyTrans(String msg, Resource xslFile);

    String returnTrans(TranData msg, Resource xslFile);

    String transform(String msg);

    void setAccessName(String accessName);
//    Node

//    String receiveformBank(String msg);
//    String sendToBank(String msg);
}
