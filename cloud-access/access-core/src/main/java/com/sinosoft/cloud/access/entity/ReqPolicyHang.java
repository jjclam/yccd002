package com.sinosoft.cloud.access.entity;

/**
 * 保单解挂XQ003.
 */
public class ReqPolicyHang {
    //保单号
    private String ContNo;
    //缴费方式
    private String PayMode;
    //保费金额
    private String Prem;
    //应缴期数
    private String DuePeriod;
    //应缴日期
    private String DueDate;
    //xxx名称
    private String BankName;
    //xxx账户号
    private String BankAccNo;
    //缴费结果
    private String PayResult;
    //收款xxx账号
    private String CompanyAccNo;
    //xxx扣费日期
    private String PayDate;
    //扣费时间
    private String PayTime;
    //xxx网点代码
    private String BankNode;
    //xxx机构代码
    private String BankBranch;

    public String getContNo() {
        return ContNo;
    }

    public void setContNo(String contNo) {
        ContNo = contNo;
    }

    public String getPayMode() {
        return PayMode;
    }

    public void setPayMode(String payMode) {
        PayMode = payMode;
    }

    public String getPrem() {
        return Prem;
    }

    public void setPrem(String prem) {
        Prem = prem;
    }

    public String getDuePeriod() {
        return DuePeriod;
    }

    public void setDuePeriod(String duePeriod) {
        DuePeriod = duePeriod;
    }

    public String getDueDate() {
        return DueDate;
    }

    public void setDueDate(String dueDate) {
        DueDate = dueDate;
    }

    public String getBankName() {
        return BankName;
    }

    public void setBankName(String bankName) {
        BankName = bankName;
    }

    public String getBankAccNo() {
        return BankAccNo;
    }

    public void setBankAccNo(String bankAccNo) {
        BankAccNo = bankAccNo;
    }

    public String getPayResult() {
        return PayResult;
    }

    public void setPayResult(String payResult) {
        PayResult = payResult;
    }

    public String getCompanyAccNo() {
        return CompanyAccNo;
    }

    public void setCompanyAccNo(String companyAccNo) {
        CompanyAccNo = companyAccNo;
    }

    public String getPayDate() {
        return PayDate;
    }

    public void setPayDate(String payDate) {
        PayDate = payDate;
    }

    public String getPayTime() {
        return PayTime;
    }

    public void setPayTime(String payTime) {
        PayTime = payTime;
    }

    public String getBankNode() {
        return BankNode;
    }

    public void setBankNode(String bankNode) {
        BankNode = bankNode;
    }

    public String getBankBranch() {
        return BankBranch;
    }

    public void setBankBranch(String bankBranch) {
        BankBranch = bankBranch;
    }

    @Override
    public String toString() {
        return "ReqPolicyHang{" +
                "ContNo='" + ContNo + '\'' +
                ", PayMode='" + PayMode + '\'' +
                ", Prem='" + Prem + '\'' +
                ", DuePeriod='" + DuePeriod + '\'' +
                ", DueDate='" + DueDate + '\'' +
                ", BankName='" + BankName + '\'' +
                ", BankAccNo='" + BankAccNo + '\'' +
                ", PayResult='" + PayResult + '\'' +
                ", CompanyAccNo='" + CompanyAccNo + '\'' +
                ", PayDate='" + PayDate + '\'' +
                ", PayTime='" + PayTime + '\'' +
                ", BankNode='" + BankNode + '\'' +
                ", BankBranch='" + BankBranch + '\'' +
                '}';
    }
}
