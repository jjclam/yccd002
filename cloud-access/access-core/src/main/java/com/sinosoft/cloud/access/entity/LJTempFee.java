package com.sinosoft.cloud.access.entity;

import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.StrTool;

/**
 * @Author:lizhen
 * @Description:
 * @Date:Created in 19:59 2018/9/5
 * @Modified By:
 */
public class LJTempFee {
    private long TempFeeID;
    private long TempFeeClassID;
    private String ShardingID;
    private String TempFeeNo;
    private String TempFeeType;
    private String RiskCode;
    private int PayIntv;
    private String OtherNo;
    private String OtherNoType;
    private double PayMoney;
    private String PayDate;
    private String EnterAccDate;
    private String ConfDate;
    private String ConfMakeDate;
    private String ConfMakeTime;
    private String SaleChnl;
    private String ManageCom;
    private String PolicyCom;
    private String AgentCom;
    private String AgentType;
    private String APPntName;
    private String AgentGroup;
    private String AgentCode;
    private String ConfFlag;
    private String SerialNo;
    private String Operator;
    private String State;
    private String MakeTime;
    private String MakeDate;
    private String ModifyDate;
    private String ModifyTime;
    private String ContCom;
    private int PayEndYear;
    private String TempFeeNoType;
    private double StandPrem;
    private String Remark;
    private String Distict;
    private String Department;
    private String BranchCode;
    private String BatchNo;
    private String ContNo;
    public static final int FIELDNUM = 41;
    private FDate fDate = new FDate();

    public long getTempFeeID() {
        return this.TempFeeID;
    }

    public void setTempFeeID(long aTempFeeID) {
        this.TempFeeID = aTempFeeID;
    }

    public void setTempFeeID(String aTempFeeID) {
        if(aTempFeeID != null && !aTempFeeID.equals("")) {
            this.TempFeeID = (new Long(aTempFeeID)).longValue();
        }

    }

    public long getTempFeeClassID() {
        return this.TempFeeClassID;
    }

    public void setTempFeeClassID(long aTempFeeClassID) {
        this.TempFeeClassID = aTempFeeClassID;
    }

    public void setTempFeeClassID(String aTempFeeClassID) {
        if(aTempFeeClassID != null && !aTempFeeClassID.equals("")) {
            this.TempFeeClassID = (new Long(aTempFeeClassID)).longValue();
        }

    }

    public String getShardingID() {
        return this.ShardingID;
    }

    public void setShardingID(String aShardingID) {
        this.ShardingID = aShardingID;
    }

    public String getTempFeeNo() {
        return this.TempFeeNo;
    }

    public void setTempFeeNo(String aTempFeeNo) {
        this.TempFeeNo = aTempFeeNo;
    }

    public String getTempFeeType() {
        return this.TempFeeType;
    }

    public void setTempFeeType(String aTempFeeType) {
        this.TempFeeType = aTempFeeType;
    }

    public String getRiskCode() {
        return this.RiskCode;
    }

    public void setRiskCode(String aRiskCode) {
        this.RiskCode = aRiskCode;
    }

    public int getPayIntv() {
        return this.PayIntv;
    }

    public void setPayIntv(int aPayIntv) {
        this.PayIntv = aPayIntv;
    }

    public void setPayIntv(String aPayIntv) {
        if(aPayIntv != null && !aPayIntv.equals("")) {
            Integer tInteger = new Integer(aPayIntv);
            int i = tInteger.intValue();
            this.PayIntv = i;
        }

    }

    public String getOtherNo() {
        return this.OtherNo;
    }

    public void setOtherNo(String aOtherNo) {
        this.OtherNo = aOtherNo;
    }

    public String getOtherNoType() {
        return this.OtherNoType;
    }

    public void setOtherNoType(String aOtherNoType) {
        this.OtherNoType = aOtherNoType;
    }

    public double getPayMoney() {
        return this.PayMoney;
    }

    public void setPayMoney(double aPayMoney) {
        this.PayMoney = aPayMoney;
    }

    public void setPayMoney(String aPayMoney) {
        if(aPayMoney != null && !aPayMoney.equals("")) {
            Double tDouble = new Double(aPayMoney);
            double d = tDouble.doubleValue();
            this.PayMoney = d;
        }

    }

    public String getPayDate() {
        return this.PayDate;
    }

    public void setPayDate(String aPayDate) {
        this.PayDate = aPayDate;
    }

    public String getEnterAccDate() {
        return this.EnterAccDate;
    }

    public void setEnterAccDate(String aEnterAccDate) {
        this.EnterAccDate = aEnterAccDate;
    }

    public String getConfDate() {
        return this.ConfDate;
    }

    public void setConfDate(String aConfDate) {
        this.ConfDate = aConfDate;
    }

    public String getConfMakeDate() {
        return this.ConfMakeDate;
    }

    public void setConfMakeDate(String aConfMakeDate) {
        this.ConfMakeDate = aConfMakeDate;
    }

    public String getConfMakeTime() {
        return this.ConfMakeTime;
    }

    public void setConfMakeTime(String aConfMakeTime) {
        this.ConfMakeTime = aConfMakeTime;
    }

    public String getSaleChnl() {
        return this.SaleChnl;
    }

    public void setSaleChnl(String aSaleChnl) {
        this.SaleChnl = aSaleChnl;
    }

    public String getManageCom() {
        return this.ManageCom;
    }

    public void setManageCom(String aManageCom) {
        this.ManageCom = aManageCom;
    }

    public String getPolicyCom() {
        return this.PolicyCom;
    }

    public void setPolicyCom(String aPolicyCom) {
        this.PolicyCom = aPolicyCom;
    }

    public String getAgentCom() {
        return this.AgentCom;
    }

    public void setAgentCom(String aAgentCom) {
        this.AgentCom = aAgentCom;
    }

    public String getAgentType() {
        return this.AgentType;
    }

    public void setAgentType(String aAgentType) {
        this.AgentType = aAgentType;
    }

    public String getAPPntName() {
        return this.APPntName;
    }

    public void setAPPntName(String aAPPntName) {
        this.APPntName = aAPPntName;
    }

    public String getAgentGroup() {
        return this.AgentGroup;
    }

    public void setAgentGroup(String aAgentGroup) {
        this.AgentGroup = aAgentGroup;
    }

    public String getAgentCode() {
        return this.AgentCode;
    }

    public void setAgentCode(String aAgentCode) {
        this.AgentCode = aAgentCode;
    }

    public String getConfFlag() {
        return this.ConfFlag;
    }

    public void setConfFlag(String aConfFlag) {
        this.ConfFlag = aConfFlag;
    }

    public String getSerialNo() {
        return this.SerialNo;
    }

    public void setSerialNo(String aSerialNo) {
        this.SerialNo = aSerialNo;
    }

    public String getOperator() {
        return this.Operator;
    }

    public void setOperator(String aOperator) {
        this.Operator = aOperator;
    }

    public String getState() {
        return this.State;
    }

    public void setState(String aState) {
        this.State = aState;
    }

    public String getMakeTime() {
        return this.MakeTime;
    }

    public void setMakeTime(String aMakeTime) {
        this.MakeTime = aMakeTime;
    }

    public String getMakeDate() {
        return this.MakeDate;
    }

    public void setMakeDate(String aMakeDate) {
        this.MakeDate = aMakeDate;
    }

    public String getModifyDate() {
        return this.ModifyDate;
    }

    public void setModifyDate(String aModifyDate) {
        this.ModifyDate = aModifyDate;
    }

    public String getModifyTime() {
        return this.ModifyTime;
    }

    public void setModifyTime(String aModifyTime) {
        this.ModifyTime = aModifyTime;
    }

    public String getContCom() {
        return this.ContCom;
    }

    public void setContCom(String aContCom) {
        this.ContCom = aContCom;
    }

    public int getPayEndYear() {
        return this.PayEndYear;
    }

    public void setPayEndYear(int aPayEndYear) {
        this.PayEndYear = aPayEndYear;
    }

    public void setPayEndYear(String aPayEndYear) {
        if(aPayEndYear != null && !aPayEndYear.equals("")) {
            Integer tInteger = new Integer(aPayEndYear);
            int i = tInteger.intValue();
            this.PayEndYear = i;
        }

    }

    public String getTempFeeNoType() {
        return this.TempFeeNoType;
    }

    public void setTempFeeNoType(String aTempFeeNoType) {
        this.TempFeeNoType = aTempFeeNoType;
    }

    public double getStandPrem() {
        return this.StandPrem;
    }

    public void setStandPrem(double aStandPrem) {
        this.StandPrem = aStandPrem;
    }

    public void setStandPrem(String aStandPrem) {
        if(aStandPrem != null && !aStandPrem.equals("")) {
            Double tDouble = new Double(aStandPrem);
            double d = tDouble.doubleValue();
            this.StandPrem = d;
        }

    }

    public String getRemark() {
        return this.Remark;
    }

    public void setRemark(String aRemark) {
        this.Remark = aRemark;
    }

    public String getDistict() {
        return this.Distict;
    }

    public void setDistict(String aDistict) {
        this.Distict = aDistict;
    }

    public String getDepartment() {
        return this.Department;
    }

    public void setDepartment(String aDepartment) {
        this.Department = aDepartment;
    }

    public String getBranchCode() {
        return this.BranchCode;
    }

    public void setBranchCode(String aBranchCode) {
        this.BranchCode = aBranchCode;
    }

    public String getBatchNo() {
        return this.BatchNo;
    }

    public void setBatchNo(String aBatchNo) {
        this.BatchNo = aBatchNo;
    }

    public String getContNo() {
        return this.ContNo;
    }

    public void setContNo(String aContNo) {
        this.ContNo = aContNo;
    }

    public int getFieldCount() {
        return 41;
    }

    public int getFieldIndex(String strFieldName) {
        return strFieldName.equals("TempFeeID")?0:(strFieldName.equals("TempFeeClassID")?1:(strFieldName.equals("ShardingID")?2:(strFieldName.equals("TempFeeNo")?3:(strFieldName.equals("TempFeeType")?4:(strFieldName.equals("RiskCode")?5:(strFieldName.equals("PayIntv")?6:(strFieldName.equals("OtherNo")?7:(strFieldName.equals("OtherNoType")?8:(strFieldName.equals("PayMoney")?9:(strFieldName.equals("PayDate")?10:(strFieldName.equals("EnterAccDate")?11:(strFieldName.equals("ConfDate")?12:(strFieldName.equals("ConfMakeDate")?13:(strFieldName.equals("ConfMakeTime")?14:(strFieldName.equals("SaleChnl")?15:(strFieldName.equals("ManageCom")?16:(strFieldName.equals("PolicyCom")?17:(strFieldName.equals("AgentCom")?18:(strFieldName.equals("AgentType")?19:(strFieldName.equals("APPntName")?20:(strFieldName.equals("AgentGroup")?21:(strFieldName.equals("AgentCode")?22:(strFieldName.equals("ConfFlag")?23:(strFieldName.equals("SerialNo")?24:(strFieldName.equals("Operator")?25:(strFieldName.equals("State")?26:(strFieldName.equals("MakeTime")?27:(strFieldName.equals("MakeDate")?28:(strFieldName.equals("ModifyDate")?29:(strFieldName.equals("ModifyTime")?30:(strFieldName.equals("ContCom")?31:(strFieldName.equals("PayEndYear")?32:(strFieldName.equals("TempFeeNoType")?33:(strFieldName.equals("StandPrem")?34:(strFieldName.equals("Remark")?35:(strFieldName.equals("Distict")?36:(strFieldName.equals("Department")?37:(strFieldName.equals("BranchCode")?38:(strFieldName.equals("BatchNo")?39:(strFieldName.equals("ContNo")?40:-1))))))))))))))))))))))))))))))))))))))));
    }

    public String getFieldName(int nFieldIndex) {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "TempFeeID";
                break;
            case 1:
                strFieldName = "TempFeeClassID";
                break;
            case 2:
                strFieldName = "ShardingID";
                break;
            case 3:
                strFieldName = "TempFeeNo";
                break;
            case 4:
                strFieldName = "TempFeeType";
                break;
            case 5:
                strFieldName = "RiskCode";
                break;
            case 6:
                strFieldName = "PayIntv";
                break;
            case 7:
                strFieldName = "OtherNo";
                break;
            case 8:
                strFieldName = "OtherNoType";
                break;
            case 9:
                strFieldName = "PayMoney";
                break;
            case 10:
                strFieldName = "PayDate";
                break;
            case 11:
                strFieldName = "EnterAccDate";
                break;
            case 12:
                strFieldName = "ConfDate";
                break;
            case 13:
                strFieldName = "ConfMakeDate";
                break;
            case 14:
                strFieldName = "ConfMakeTime";
                break;
            case 15:
                strFieldName = "SaleChnl";
                break;
            case 16:
                strFieldName = "ManageCom";
                break;
            case 17:
                strFieldName = "PolicyCom";
                break;
            case 18:
                strFieldName = "AgentCom";
                break;
            case 19:
                strFieldName = "AgentType";
                break;
            case 20:
                strFieldName = "APPntName";
                break;
            case 21:
                strFieldName = "AgentGroup";
                break;
            case 22:
                strFieldName = "AgentCode";
                break;
            case 23:
                strFieldName = "ConfFlag";
                break;
            case 24:
                strFieldName = "SerialNo";
                break;
            case 25:
                strFieldName = "Operator";
                break;
            case 26:
                strFieldName = "State";
                break;
            case 27:
                strFieldName = "MakeTime";
                break;
            case 28:
                strFieldName = "MakeDate";
                break;
            case 29:
                strFieldName = "ModifyDate";
                break;
            case 30:
                strFieldName = "ModifyTime";
                break;
            case 31:
                strFieldName = "ContCom";
                break;
            case 32:
                strFieldName = "PayEndYear";
                break;
            case 33:
                strFieldName = "TempFeeNoType";
                break;
            case 34:
                strFieldName = "StandPrem";
                break;
            case 35:
                strFieldName = "Remark";
                break;
            case 36:
                strFieldName = "Distict";
                break;
            case 37:
                strFieldName = "Department";
                break;
            case 38:
                strFieldName = "BranchCode";
                break;
            case 39:
                strFieldName = "BatchNo";
                break;
            case 40:
                strFieldName = "ContNo";
                break;
            default:
                strFieldName = "";
        }

        return strFieldName;
    }

    public int getFieldType(String strFieldName) {
        String var2 = strFieldName.toUpperCase();
        byte var3 = -1;
        switch(var2.hashCode()) {
            case -2145366347:
                if(var2.equals("SERIALNO")) {
                    var3 = 24;
                }
                break;
            case -2129360616:
                if(var2.equals("PAYMONEY")) {
                    var3 = 9;
                }
                break;
            case -2043177892:
                if(var2.equals("AGENTCOM")) {
                    var3 = 18;
                }
                break;
            case -1982087716:
                if(var2.equals("MANAGECOM")) {
                    var3 = 16;
                }
                break;
            case -1905104108:
                if(var2.equals("DISTICT")) {
                    var3 = 36;
                }
                break;
            case -1881294976:
                if(var2.equals("REMARK")) {
                    var3 = 35;
                }
                break;
            case -1734048017:
                if(var2.equals("POLICYCOM")) {
                    var3 = 17;
                }
                break;
            case -1575827030:
                if(var2.equals("SALECHNL")) {
                    var3 = 15;
                }
                break;
            case -1322448033:
                if(var2.equals("SHARDINGID")) {
                    var3 = 2;
                }
                break;
            case -1301282484:
                if(var2.equals("TEMPFEETYPE")) {
                    var3 = 4;
                }
                break;
            case -1177207337:
                if(var2.equals("ENTERACCDATE")) {
                    var3 = 11;
                }
                break;
            case -877853171:
                if(var2.equals("TEMPFEENOTYPE")) {
                    var3 = 33;
                }
                break;
            case -690111846:
                if(var2.equals("AGENTGROUP")) {
                    var3 = 21;
                }
                break;
            case -458499600:
                if(var2.equals("PAYENDYEAR")) {
                    var3 = 32;
                }
                break;
            case -428174159:
                if(var2.equals("OTHERNO")) {
                    var3 = 7;
                }
                break;
            case -307623620:
                if(var2.equals("RISKCODE")) {
                    var3 = 5;
                }
                break;
            case -68970442:
                if(var2.equals("PAYDATE")) {
                    var3 = 10;
                }
                break;
            case -68808977:
                if(var2.equals("PAYINTV")) {
                    var3 = 6;
                }
                break;
            case 79219825:
                if(var2.equals("STATE")) {
                    var3 = 26;
                }
                break;
            case 202333394:
                if(var2.equals("CONFDATE")) {
                    var3 = 12;
                }
                break;
            case 202402960:
                if(var2.equals("CONFFLAG")) {
                    var3 = 23;
                }
                break;
            case 282073252:
                if(var2.equals("OPERATOR")) {
                    var3 = 25;
                }
                break;
            case 386239067:
                if(var2.equals("BATCHNO")) {
                    var3 = 39;
                }
                break;
            case 467918765:
                if(var2.equals("TEMPFEEID")) {
                    var3 = 0;
                }
                break;
            case 467918931:
                if(var2.equals("TEMPFEENO")) {
                    var3 = 3;
                }
                break;
            case 528849632:
                if(var2.equals("STANDPREM")) {
                    var3 = 34;
                }
                break;
            case 541032896:
                if(var2.equals("CONFMAKEDATE")) {
                    var3 = 13;
                }
                break;
            case 541517023:
                if(var2.equals("CONFMAKETIME")) {
                    var3 = 14;
                }
                break;
            case 764338834:
                if(var2.equals("APPNTNAME")) {
                    var3 = 20;
                }
                break;
            case 823606940:
                if(var2.equals("MAKEDATE")) {
                    var3 = 28;
                }
                break;
            case 824091067:
                if(var2.equals("MAKETIME")) {
                    var3 = 27;
                }
                break;
            case 1085994578:
                if(var2.equals("AGENTCODE")) {
                    var3 = 22;
                }
                break;
            case 1086511007:
                if(var2.equals("AGENTTYPE")) {
                    var3 = 19;
                }
                break;
            case 1224104811:
                if(var2.equals("OTHERNOTYPE")) {
                    var3 = 8;
                }
                break;
            case 1333276498:
                if(var2.equals("DEPARTMENT")) {
                    var3 = 37;
                }
                break;
            case 1575106849:
                if(var2.equals("TEMPFEECLASSID")) {
                    var3 = 1;
                }
                break;
            case 1669511407:
                if(var2.equals("CONTCOM")) {
                    var3 = 31;
                }
                break;
            case 1696950120:
                if(var2.equals("MODIFYDATE")) {
                    var3 = 29;
                }
                break;
            case 1697434247:
                if(var2.equals("MODIFYTIME")) {
                    var3 = 30;
                }
                break;
            case 1838685839:
                if(var2.equals("BRANCHCODE")) {
                    var3 = 38;
                }
                break;
            case 1993518195:
                if(var2.equals("CONTNO")) {
                    var3 = 40;
                }
        }

        switch(var3) {
            case 0:
                return 7;
            case 1:
                return 7;
            case 2:
                return 0;
            case 3:
                return 0;
            case 4:
                return 0;
            case 5:
                return 0;
            case 6:
                return 3;
            case 7:
                return 0;
            case 8:
                return 0;
            case 9:
                return 4;
            case 10:
                return 0;
            case 11:
                return 0;
            case 12:
                return 0;
            case 13:
                return 0;
            case 14:
                return 0;
            case 15:
                return 0;
            case 16:
                return 0;
            case 17:
                return 0;
            case 18:
                return 0;
            case 19:
                return 0;
            case 20:
                return 0;
            case 21:
                return 0;
            case 22:
                return 0;
            case 23:
                return 0;
            case 24:
                return 0;
            case 25:
                return 0;
            case 26:
                return 0;
            case 27:
                return 0;
            case 28:
                return 0;
            case 29:
                return 0;
            case 30:
                return 0;
            case 31:
                return 0;
            case 32:
                return 3;
            case 33:
                return 0;
            case 34:
                return 4;
            case 35:
                return 0;
            case 36:
                return 0;
            case 37:
                return 0;
            case 38:
                return 0;
            case 39:
                return 0;
            case 40:
                return 0;
            default:
                return -1;
        }
    }

    public int getFieldType(int nFieldIndex) {
        switch(nFieldIndex) {
            case 0:
                return 7;
            case 1:
                return 7;
            case 2:
                return 0;
            case 3:
                return 0;
            case 4:
                return 0;
            case 5:
                return 0;
            case 6:
                return 3;
            case 7:
                return 0;
            case 8:
                return 0;
            case 9:
                return 4;
            case 10:
                return 0;
            case 11:
                return 0;
            case 12:
                return 0;
            case 13:
                return 0;
            case 14:
                return 0;
            case 15:
                return 0;
            case 16:
                return 0;
            case 17:
                return 0;
            case 18:
                return 0;
            case 19:
                return 0;
            case 20:
                return 0;
            case 21:
                return 0;
            case 22:
                return 0;
            case 23:
                return 0;
            case 24:
                return 0;
            case 25:
                return 0;
            case 26:
                return 0;
            case 27:
                return 0;
            case 28:
                return 0;
            case 29:
                return 0;
            case 30:
                return 0;
            case 31:
                return 0;
            case 32:
                return 3;
            case 33:
                return 0;
            case 34:
                return 4;
            case 35:
                return 0;
            case 36:
                return 0;
            case 37:
                return 0;
            case 38:
                return 0;
            case 39:
                return 0;
            case 40:
                return 0;
            default:
                return -1;
        }
    }

    public String getV(String FCode) {
        String strReturn = "";
        if(FCode.equalsIgnoreCase("TempFeeID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.TempFeeID));
        }

        if(FCode.equalsIgnoreCase("TempFeeClassID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.TempFeeClassID));
        }

        if(FCode.equalsIgnoreCase("ShardingID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.ShardingID));
        }

        if(FCode.equalsIgnoreCase("TempFeeNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.TempFeeNo));
        }

        if(FCode.equalsIgnoreCase("TempFeeType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.TempFeeType));
        }

        if(FCode.equalsIgnoreCase("RiskCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.RiskCode));
        }

        if(FCode.equalsIgnoreCase("PayIntv")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.PayIntv));
        }

        if(FCode.equalsIgnoreCase("OtherNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.OtherNo));
        }

        if(FCode.equalsIgnoreCase("OtherNoType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.OtherNoType));
        }

        if(FCode.equalsIgnoreCase("PayMoney")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.PayMoney));
        }

        if(FCode.equalsIgnoreCase("PayDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.PayDate));
        }

        if(FCode.equalsIgnoreCase("EnterAccDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.EnterAccDate));
        }

        if(FCode.equalsIgnoreCase("ConfDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.ConfDate));
        }

        if(FCode.equalsIgnoreCase("ConfMakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.ConfMakeDate));
        }

        if(FCode.equalsIgnoreCase("ConfMakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.ConfMakeTime));
        }

        if(FCode.equalsIgnoreCase("SaleChnl")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.SaleChnl));
        }

        if(FCode.equalsIgnoreCase("ManageCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.ManageCom));
        }

        if(FCode.equalsIgnoreCase("PolicyCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.PolicyCom));
        }

        if(FCode.equalsIgnoreCase("AgentCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.AgentCom));
        }

        if(FCode.equalsIgnoreCase("AgentType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.AgentType));
        }

        if(FCode.equalsIgnoreCase("APPntName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.APPntName));
        }

        if(FCode.equalsIgnoreCase("AgentGroup")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.AgentGroup));
        }

        if(FCode.equalsIgnoreCase("AgentCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.AgentCode));
        }

        if(FCode.equalsIgnoreCase("ConfFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.ConfFlag));
        }

        if(FCode.equalsIgnoreCase("SerialNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.SerialNo));
        }

        if(FCode.equalsIgnoreCase("Operator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.Operator));
        }

        if(FCode.equalsIgnoreCase("State")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.State));
        }

        if(FCode.equalsIgnoreCase("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.MakeTime));
        }

        if(FCode.equalsIgnoreCase("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.MakeDate));
        }

        if(FCode.equalsIgnoreCase("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.ModifyDate));
        }

        if(FCode.equalsIgnoreCase("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.ModifyTime));
        }

        if(FCode.equalsIgnoreCase("ContCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.ContCom));
        }

        if(FCode.equalsIgnoreCase("PayEndYear")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.PayEndYear));
        }

        if(FCode.equalsIgnoreCase("TempFeeNoType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.TempFeeNoType));
        }

        if(FCode.equalsIgnoreCase("StandPrem")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.StandPrem));
        }

        if(FCode.equalsIgnoreCase("Remark")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.Remark));
        }

        if(FCode.equalsIgnoreCase("Distict")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.Distict));
        }

        if(FCode.equalsIgnoreCase("Department")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.Department));
        }

        if(FCode.equalsIgnoreCase("BranchCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.BranchCode));
        }

        if(FCode.equalsIgnoreCase("BatchNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.BatchNo));
        }

        if(FCode.equalsIgnoreCase("ContNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.ContNo));
        }

        if(strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }

    public String getV(int nFieldIndex) {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = String.valueOf(this.TempFeeID);
                break;
            case 1:
                strFieldValue = String.valueOf(this.TempFeeClassID);
                break;
            case 2:
                strFieldValue = String.valueOf(this.ShardingID);
                break;
            case 3:
                strFieldValue = String.valueOf(this.TempFeeNo);
                break;
            case 4:
                strFieldValue = String.valueOf(this.TempFeeType);
                break;
            case 5:
                strFieldValue = String.valueOf(this.RiskCode);
                break;
            case 6:
                strFieldValue = String.valueOf(this.PayIntv);
                break;
            case 7:
                strFieldValue = String.valueOf(this.OtherNo);
                break;
            case 8:
                strFieldValue = String.valueOf(this.OtherNoType);
                break;
            case 9:
                strFieldValue = String.valueOf(this.PayMoney);
                break;
            case 10:
                strFieldValue = String.valueOf(this.PayDate);
                break;
            case 11:
                strFieldValue = String.valueOf(this.EnterAccDate);
                break;
            case 12:
                strFieldValue = String.valueOf(this.ConfDate);
                break;
            case 13:
                strFieldValue = String.valueOf(this.ConfMakeDate);
                break;
            case 14:
                strFieldValue = String.valueOf(this.ConfMakeTime);
                break;
            case 15:
                strFieldValue = String.valueOf(this.SaleChnl);
                break;
            case 16:
                strFieldValue = String.valueOf(this.ManageCom);
                break;
            case 17:
                strFieldValue = String.valueOf(this.PolicyCom);
                break;
            case 18:
                strFieldValue = String.valueOf(this.AgentCom);
                break;
            case 19:
                strFieldValue = String.valueOf(this.AgentType);
                break;
            case 20:
                strFieldValue = String.valueOf(this.APPntName);
                break;
            case 21:
                strFieldValue = String.valueOf(this.AgentGroup);
                break;
            case 22:
                strFieldValue = String.valueOf(this.AgentCode);
                break;
            case 23:
                strFieldValue = String.valueOf(this.ConfFlag);
                break;
            case 24:
                strFieldValue = String.valueOf(this.SerialNo);
                break;
            case 25:
                strFieldValue = String.valueOf(this.Operator);
                break;
            case 26:
                strFieldValue = String.valueOf(this.State);
                break;
            case 27:
                strFieldValue = String.valueOf(this.MakeTime);
                break;
            case 28:
                strFieldValue = String.valueOf(this.MakeDate);
                break;
            case 29:
                strFieldValue = String.valueOf(this.ModifyDate);
                break;
            case 30:
                strFieldValue = String.valueOf(this.ModifyTime);
                break;
            case 31:
                strFieldValue = String.valueOf(this.ContCom);
                break;
            case 32:
                strFieldValue = String.valueOf(this.PayEndYear);
                break;
            case 33:
                strFieldValue = String.valueOf(this.TempFeeNoType);
                break;
            case 34:
                strFieldValue = String.valueOf(this.StandPrem);
                break;
            case 35:
                strFieldValue = String.valueOf(this.Remark);
                break;
            case 36:
                strFieldValue = String.valueOf(this.Distict);
                break;
            case 37:
                strFieldValue = String.valueOf(this.Department);
                break;
            case 38:
                strFieldValue = String.valueOf(this.BranchCode);
                break;
            case 39:
                strFieldValue = String.valueOf(this.BatchNo);
                break;
            case 40:
                strFieldValue = String.valueOf(this.ContNo);
                break;
            default:
                strFieldValue = "";
        }

        if(strFieldValue.equals("")) {
            strFieldValue = "null";
        }

        return strFieldValue;
    }

    public boolean setV(String FCode, String FValue) {
        if(StrTool.cTrim(FCode).equals("")) {
            return false;
        } else {
            if(FCode.equalsIgnoreCase("TempFeeID") && FValue != null && !FValue.equals("")) {
                this.TempFeeID = (new Long(FValue)).longValue();
            }

            if(FCode.equalsIgnoreCase("TempFeeClassID") && FValue != null && !FValue.equals("")) {
                this.TempFeeClassID = (new Long(FValue)).longValue();
            }

            if(FCode.equalsIgnoreCase("ShardingID")) {
                if(FValue != null && !FValue.equals("")) {
                    this.ShardingID = FValue.trim();
                } else {
                    this.ShardingID = null;
                }
            }

            if(FCode.equalsIgnoreCase("TempFeeNo")) {
                if(FValue != null && !FValue.equals("")) {
                    this.TempFeeNo = FValue.trim();
                } else {
                    this.TempFeeNo = null;
                }
            }

            if(FCode.equalsIgnoreCase("TempFeeType")) {
                if(FValue != null && !FValue.equals("")) {
                    this.TempFeeType = FValue.trim();
                } else {
                    this.TempFeeType = null;
                }
            }

            if(FCode.equalsIgnoreCase("RiskCode")) {
                if(FValue != null && !FValue.equals("")) {
                    this.RiskCode = FValue.trim();
                } else {
                    this.RiskCode = null;
                }
            }

            Integer tDouble;
            int d;
            if(FCode.equalsIgnoreCase("PayIntv") && FValue != null && !FValue.equals("")) {
                tDouble = new Integer(FValue);
                d = tDouble.intValue();
                this.PayIntv = d;
            }

            if(FCode.equalsIgnoreCase("OtherNo")) {
                if(FValue != null && !FValue.equals("")) {
                    this.OtherNo = FValue.trim();
                } else {
                    this.OtherNo = null;
                }
            }

            if(FCode.equalsIgnoreCase("OtherNoType")) {
                if(FValue != null && !FValue.equals("")) {
                    this.OtherNoType = FValue.trim();
                } else {
                    this.OtherNoType = null;
                }
            }

            Double tDouble1;
            double d1;
            if(FCode.equalsIgnoreCase("PayMoney") && FValue != null && !FValue.equals("")) {
                tDouble1 = new Double(FValue);
                d1 = tDouble1.doubleValue();
                this.PayMoney = d1;
            }

            if(FCode.equalsIgnoreCase("PayDate")) {
                if(FValue != null && !FValue.equals("")) {
                    this.PayDate = FValue.trim();
                } else {
                    this.PayDate = null;
                }
            }

            if(FCode.equalsIgnoreCase("EnterAccDate")) {
                if(FValue != null && !FValue.equals("")) {
                    this.EnterAccDate = FValue.trim();
                } else {
                    this.EnterAccDate = null;
                }
            }

            if(FCode.equalsIgnoreCase("ConfDate")) {
                if(FValue != null && !FValue.equals("")) {
                    this.ConfDate = FValue.trim();
                } else {
                    this.ConfDate = null;
                }
            }

            if(FCode.equalsIgnoreCase("ConfMakeDate")) {
                if(FValue != null && !FValue.equals("")) {
                    this.ConfMakeDate = FValue.trim();
                } else {
                    this.ConfMakeDate = null;
                }
            }

            if(FCode.equalsIgnoreCase("ConfMakeTime")) {
                if(FValue != null && !FValue.equals("")) {
                    this.ConfMakeTime = FValue.trim();
                } else {
                    this.ConfMakeTime = null;
                }
            }

            if(FCode.equalsIgnoreCase("SaleChnl")) {
                if(FValue != null && !FValue.equals("")) {
                    this.SaleChnl = FValue.trim();
                } else {
                    this.SaleChnl = null;
                }
            }

            if(FCode.equalsIgnoreCase("ManageCom")) {
                if(FValue != null && !FValue.equals("")) {
                    this.ManageCom = FValue.trim();
                } else {
                    this.ManageCom = null;
                }
            }

            if(FCode.equalsIgnoreCase("PolicyCom")) {
                if(FValue != null && !FValue.equals("")) {
                    this.PolicyCom = FValue.trim();
                } else {
                    this.PolicyCom = null;
                }
            }

            if(FCode.equalsIgnoreCase("AgentCom")) {
                if(FValue != null && !FValue.equals("")) {
                    this.AgentCom = FValue.trim();
                } else {
                    this.AgentCom = null;
                }
            }

            if(FCode.equalsIgnoreCase("AgentType")) {
                if(FValue != null && !FValue.equals("")) {
                    this.AgentType = FValue.trim();
                } else {
                    this.AgentType = null;
                }
            }

            if(FCode.equalsIgnoreCase("APPntName")) {
                if(FValue != null && !FValue.equals("")) {
                    this.APPntName = FValue.trim();
                } else {
                    this.APPntName = null;
                }
            }

            if(FCode.equalsIgnoreCase("AgentGroup")) {
                if(FValue != null && !FValue.equals("")) {
                    this.AgentGroup = FValue.trim();
                } else {
                    this.AgentGroup = null;
                }
            }

            if(FCode.equalsIgnoreCase("AgentCode")) {
                if(FValue != null && !FValue.equals("")) {
                    this.AgentCode = FValue.trim();
                } else {
                    this.AgentCode = null;
                }
            }

            if(FCode.equalsIgnoreCase("ConfFlag")) {
                if(FValue != null && !FValue.equals("")) {
                    this.ConfFlag = FValue.trim();
                } else {
                    this.ConfFlag = null;
                }
            }

            if(FCode.equalsIgnoreCase("SerialNo")) {
                if(FValue != null && !FValue.equals("")) {
                    this.SerialNo = FValue.trim();
                } else {
                    this.SerialNo = null;
                }
            }

            if(FCode.equalsIgnoreCase("Operator")) {
                if(FValue != null && !FValue.equals("")) {
                    this.Operator = FValue.trim();
                } else {
                    this.Operator = null;
                }
            }

            if(FCode.equalsIgnoreCase("State")) {
                if(FValue != null && !FValue.equals("")) {
                    this.State = FValue.trim();
                } else {
                    this.State = null;
                }
            }

            if(FCode.equalsIgnoreCase("MakeTime")) {
                if(FValue != null && !FValue.equals("")) {
                    this.MakeTime = FValue.trim();
                } else {
                    this.MakeTime = null;
                }
            }

            if(FCode.equalsIgnoreCase("MakeDate")) {
                if(FValue != null && !FValue.equals("")) {
                    this.MakeDate = FValue.trim();
                } else {
                    this.MakeDate = null;
                }
            }

            if(FCode.equalsIgnoreCase("ModifyDate")) {
                if(FValue != null && !FValue.equals("")) {
                    this.ModifyDate = FValue.trim();
                } else {
                    this.ModifyDate = null;
                }
            }

            if(FCode.equalsIgnoreCase("ModifyTime")) {
                if(FValue != null && !FValue.equals("")) {
                    this.ModifyTime = FValue.trim();
                } else {
                    this.ModifyTime = null;
                }
            }

            if(FCode.equalsIgnoreCase("ContCom")) {
                if(FValue != null && !FValue.equals("")) {
                    this.ContCom = FValue.trim();
                } else {
                    this.ContCom = null;
                }
            }

            if(FCode.equalsIgnoreCase("PayEndYear") && FValue != null && !FValue.equals("")) {
                tDouble = new Integer(FValue);
                d = tDouble.intValue();
                this.PayEndYear = d;
            }

            if(FCode.equalsIgnoreCase("TempFeeNoType")) {
                if(FValue != null && !FValue.equals("")) {
                    this.TempFeeNoType = FValue.trim();
                } else {
                    this.TempFeeNoType = null;
                }
            }

            if(FCode.equalsIgnoreCase("StandPrem") && FValue != null && !FValue.equals("")) {
                tDouble1 = new Double(FValue);
                d1 = tDouble1.doubleValue();
                this.StandPrem = d1;
            }

            if(FCode.equalsIgnoreCase("Remark")) {
                if(FValue != null && !FValue.equals("")) {
                    this.Remark = FValue.trim();
                } else {
                    this.Remark = null;
                }
            }

            if(FCode.equalsIgnoreCase("Distict")) {
                if(FValue != null && !FValue.equals("")) {
                    this.Distict = FValue.trim();
                } else {
                    this.Distict = null;
                }
            }

            if(FCode.equalsIgnoreCase("Department")) {
                if(FValue != null && !FValue.equals("")) {
                    this.Department = FValue.trim();
                } else {
                    this.Department = null;
                }
            }

            if(FCode.equalsIgnoreCase("BranchCode")) {
                if(FValue != null && !FValue.equals("")) {
                    this.BranchCode = FValue.trim();
                } else {
                    this.BranchCode = null;
                }
            }

            if(FCode.equalsIgnoreCase("BatchNo")) {
                if(FValue != null && !FValue.equals("")) {
                    this.BatchNo = FValue.trim();
                } else {
                    this.BatchNo = null;
                }
            }

            if(FCode.equalsIgnoreCase("ContNo")) {
                if(FValue != null && !FValue.equals("")) {
                    this.ContNo = FValue.trim();
                } else {
                    this.ContNo = null;
                }
            }

            return true;
        }
    }

    @Override
    public String toString() {
        return "LJTempFee{" +
                "TempFeeID=" + TempFeeID +
                ", TempFeeClassID=" + TempFeeClassID +
                ", ShardingID='" + ShardingID + '\'' +
                ", TempFeeNo='" + TempFeeNo + '\'' +
                ", TempFeeType='" + TempFeeType + '\'' +
                ", RiskCode='" + RiskCode + '\'' +
                ", PayIntv=" + PayIntv +
                ", OtherNo='" + OtherNo + '\'' +
                ", OtherNoType='" + OtherNoType + '\'' +
                ", PayMoney=" + PayMoney +
                ", PayDate='" + PayDate + '\'' +
                ", EnterAccDate='" + EnterAccDate + '\'' +
                ", ConfDate='" + ConfDate + '\'' +
                ", ConfMakeDate='" + ConfMakeDate + '\'' +
                ", ConfMakeTime='" + ConfMakeTime + '\'' +
                ", SaleChnl='" + SaleChnl + '\'' +
                ", ManageCom='" + ManageCom + '\'' +
                ", PolicyCom='" + PolicyCom + '\'' +
                ", AgentCom='" + AgentCom + '\'' +
                ", AgentType='" + AgentType + '\'' +
                ", APPntName='" + APPntName + '\'' +
                ", AgentGroup='" + AgentGroup + '\'' +
                ", AgentCode='" + AgentCode + '\'' +
                ", ConfFlag='" + ConfFlag + '\'' +
                ", SerialNo='" + SerialNo + '\'' +
                ", Operator='" + Operator + '\'' +
                ", State='" + State + '\'' +
                ", MakeTime='" + MakeTime + '\'' +
                ", MakeDate='" + MakeDate + '\'' +
                ", ModifyDate='" + ModifyDate + '\'' +
                ", ModifyTime='" + ModifyTime + '\'' +
                ", ContCom='" + ContCom + '\'' +
                ", PayEndYear=" + PayEndYear +
                ", TempFeeNoType='" + TempFeeNoType + '\'' +
                ", StandPrem=" + StandPrem +
                ", Remark='" + Remark + '\'' +
                ", Distict='" + Distict + '\'' +
                ", Department='" + Department + '\'' +
                ", BranchCode='" + BranchCode + '\'' +
                ", BatchNo='" + BatchNo + '\'' +
                ", ContNo='" + ContNo + '\'' +
                ", fDate=" + fDate +
                '}';
    }
}
