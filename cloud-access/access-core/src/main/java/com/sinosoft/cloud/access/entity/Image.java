package com.sinosoft.cloud.access.entity;


public class Image {
    private String ImgData;
    private String ImgName;
    private String PageCode;

    public String getImgData() {

        return ImgData;
    }

    public void setImgData(String imgData) {
        ImgData = imgData;
    }

    public String getImgName() {
        return ImgName;
    }

    public void setImgName(String imgName) {
        ImgName = imgName;
    }

    public String getPageCode() {
        return PageCode;
    }

    public void setPageCode(String pageCode) {
        PageCode = pageCode;
    }

    @Override
    public String toString() {
        return "Image{" +
                "ImgData='" + ImgData + '\'' +
                ", ImgName='" + ImgName + '\'' +
                ", PageCode='" + PageCode + '\'' +
                '}';
    }
}
