package com.sinosoft.cloud.access.entity;

/**
 * @Author:lizhen
 * @Description:
 * @Date:Created in 14:40 2018/4/11
 * @Modified By:
 */
public class XQJFQueryResult {
    private String AppntName;
    private String AppntIDKind;
    private String AppntIDCode;
    private String DueAmt;
    private String DueDate;
    private String DuePeriod;
    private String ProdCode;
    private String PayAcc;
    private String PayAmt;
    private String PolicyNo;
    private String RiskCode;

    public XQJFQueryResult() {
    }

    public String getPolicyNo() {
        return this.PolicyNo;
    }

    public void setPolicyNo(String policyNo) {
        this.PolicyNo = policyNo;
    }

    public String getRiskCode() {
        return this.RiskCode;
    }

    public void setRiskCode(String riskCode) {
        this.RiskCode = riskCode;
    }

    public String getAppntName() {
        return this.AppntName;
    }

    public void setAppntName(String appntName) {
        this.AppntName = appntName;
    }

    public String getAppntIDKind() {
        return this.AppntIDKind;
    }

    public void setAppntIDKind(String appntIDKind) {
        this.AppntIDKind = appntIDKind;
    }

    public String getAppntIDCode() {
        return this.AppntIDCode;
    }

    public void setAppntIDCode(String appntIDCode) {
        this.AppntIDCode = appntIDCode;
    }

    public String getDueAmt() {
        return this.DueAmt;
    }

    public void setDueAmt(String dueAmt) {
        this.DueAmt = dueAmt;
    }

    public String getDueDate() {
        return this.DueDate;
    }

    public void setDueDate(String dueDate) {
        this.DueDate = dueDate;
    }

    public String getDuePeriod() {
        return this.DuePeriod;
    }

    public void setDuePeriod(String duePeriod) {
        this.DuePeriod = duePeriod;
    }

    public String getProdCode() {
        return this.ProdCode;
    }

    public void setProdCode(String prodCode) {
        this.ProdCode = prodCode;
    }

    public String getPayAcc() {
        return this.PayAcc;
    }

    public void setPayAcc(String payAcc) {
        this.PayAcc = payAcc;
    }

    public String getPayAmt() {
        return this.PayAmt;
    }

    public void setPayAmt(String payAmt) {
        this.PayAmt = payAmt;
    }

    @Override
    public String toString() {
        return "XQJFQueryResultPojo{AppntName=\'" + this.AppntName + '\'' + ", AppntIDKind=\'" + this.AppntIDKind + '\'' + ", AppntIDCode=\'" + this.AppntIDCode + '\'' + ", DueAmt=\'" + this.DueAmt + '\'' + ", DueDate=\'" + this.DueDate + '\'' + ", DuePeriod=\'" + this.DuePeriod + '\'' + ", ProdCode=\'" + this.ProdCode + '\'' + ", PayAcc=\'" + this.PayAcc + '\'' + ", PayAmt=\'" + this.PayAmt + '\'' + ", PolicyNo=\'" + this.PolicyNo + '\'' + ", RiskCode=\'" + this.RiskCode + '\'' + '}';
    }

    public String getV(String s) {
        return null;
    }

    public String getV(int i) {
        return null;
    }

    public int getFieldType(String s) {
        return 0;
    }

    public int getFieldType(int i) {
        return 0;
    }

    public int getFieldCount() {
        return 0;
    }

    public int getFieldIndex(String s) {
        return 0;
    }

    public String getFieldName(int i) {
        return null;
    }

    public boolean setV(String s, String s1) {
        return false;
    }

}