package com.sinosoft.cloud.access.util;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class HttpUtil {

    private static Logger logger = LoggerFactory.getLogger(HttpUtil.class);

    private static final String DEFAULT_CHARSET = "gbk";

    /**
     * read string from request
     *
     * @param request
     * @param charset
     * @return
     * @author YANGYUCHENG890
     */
    public static String readStringFromRequest(HttpServletRequest request, String charset) {
        String string = null;

        InputStream inputStream = null;
        try {
            inputStream = request.getInputStream();
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            byte[] buffer = new byte[1024];
            int len = -1;
            while ((len = inputStream.read(buffer)) != -1) {
                outputStream.write(buffer, 0, len);
            }
            string = new String(outputStream.toByteArray(), charset);
//            string = new String(string.getBytes("utf-8"), "GBK");
            logger.info("[readStringFromRequest] string: " + string);
            return string.trim();
        } catch (Exception e) {
            logger.error("[readStringFromRequest] exception: " + e.getMessage(), e);
        }finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return string;
    }

    /**
     * read string from request
     *
     * @param request
     * @return
     * @author YANGYUCHENG890
     */
    public static String readStringFromRequest(HttpServletRequest request) {
        return readStringFromRequest(request, DEFAULT_CHARSET);
    }

    /**
     * read json from request
     *
     * @param request
     * @param charset
     * @return
     * @author YUYANGCHENG890
     */
    public static JSONObject readJsonFromRequest(HttpServletRequest request, String charset) {
        String string = readStringFromRequest(request, charset);
        if (string == null || string.trim().length() == 0) {
            logger.error("[readJsonFromRequest] fail to read parse json from string: " + string);
            return null;
        }
        JSONObject jsonObject = JSON.parseObject(string);
        if (jsonObject == null) {
            logger.error("[readJsonFromRequest] fail to read parse json from string: " + string);
            return null;
        }
        return jsonObject;
    }

    /**
     * read json from request
     *
     * @param request
     * @return
     * @author YANGYUCHENG890
     */
    public static JSONObject readJsonFromRequest(HttpServletRequest request) {
        return readJsonFromRequest(request, DEFAULT_CHARSET);
    }

    /**
     * write json into response
     *
     * @param response
     * @param jsonString
     * @param charset
     * @author YANGYUCHENG890
     */
    public static void writeJsonIntoResponse(HttpServletResponse response, String jsonString, String charset) {
        logger.info("线程"+Thread.currentThread()+"返回第三方JSON" + jsonString);
        OutputStream outputStream = null;
        try {
            response.setCharacterEncoding(charset);
            response.setContentType("application/json;charset=" + charset.toLowerCase());
            outputStream = response.getOutputStream();
            outputStream.write(jsonString.getBytes(charset));
            outputStream.flush();
        } catch (Exception e) {
            logger.error("[writeJsonIntoResponse] exception: " + e.getMessage(), e);
        } finally {
            if (outputStream != null) {
                try {
                    outputStream.close();
                } catch (Exception e2) {
                    logger.error("[writeJsonIntoResponse] exception to close outputStream: " + e2.getMessage());
                }
            }
        }
    }

    /**
     * write json into response
     *
     * @param response
     * @param jsonString
     * @author YANGYUCHENG890
     */
    public static void writeJsonIntoResponse(HttpServletResponse response, String jsonString) {
        writeJsonIntoResponse(response, jsonString, DEFAULT_CHARSET);
    }

    /**
     * write json into response
     *
     * @param response
     * @param bytes
     * @param charset
     * @return
     * @author
     */
    public static boolean writePdfIntoResponse(HttpServletResponse response, byte[] bytes, String charset) {
        return writePdfIntoResponse(response, bytes, charset, null);
    }
    public static boolean writePdfIntoResponse(HttpServletResponse response,
                                               byte[] byteArray, String charset, String polNo) {
        boolean retFlag = false;
        OutputStream outputStream = null;
        try {
            outputStream = response.getOutputStream();
            String filename = StringUtils.isBlank(polNo) ? "InsurancePolicy.pdf" : polNo + ".pdf";
            response.setCharacterEncoding("UTF-8");
            response.setHeader("Content-Disposition", "attachment; filename="+filename);
            response.setContentType("application/octet-stream; charset=" + charset.toLowerCase());
            outputStream.write(byteArray);
            outputStream.flush();
            retFlag = true;
        } catch (Exception e) {
            logger.error("[writePdfIntoResponse] exception: " + e.getMessage(), e);
        } finally {
            if (outputStream != null) {
                try {
                    outputStream.close();
                } catch (Exception e2) {
                    logger.error("[writePdfIntoResponse] exception to close outputStream: " + e2.getMessage());
                }
            }
        }
        return retFlag;
    }
    /**
     * write json into response
     *
     * @param response
     * @param bytes
     * @return
     * @author
     */
    public static boolean writePdfIntoResponse(HttpServletResponse response, byte[] bytes) {
        return writePdfIntoResponse(response, bytes, DEFAULT_CHARSET);
    }

    /**
     * write txt into response
     *
     * @param response
     * @param txt
     * @param charset
     * @author
     */
    public static void writeTxtIntoResponse(HttpServletResponse response, String txt, String charset) {
        OutputStream outputStream = null;
        try {
            logger.info("[writeTxtIntoResponse] txt: " + txt);
            response.setCharacterEncoding(charset);
            response.setContentType("text/plain;charset=" + charset.toLowerCase());
            outputStream = response.getOutputStream();
            outputStream.write(txt.getBytes(charset));
            outputStream.flush();
        } catch (Exception e) {
            logger.error("[writeTxtIntoResponse] exception: " + e.getMessage(), e);
        } finally {
            if (outputStream != null) {
                try {
                    outputStream.close();
                } catch (Exception e2) {
                    logger.error("[writeTxtIntoResponse] exception to close outputStream: " + e2.getMessage());
                }
            }
        }
    }

    /**
     * write txt into response
     *
     * @param response
     * @param txt
     * @author YANGYUCHENG890
     */
    public static void writeTxtIntoResponse(HttpServletResponse response, String txt) {
        writeTxtIntoResponse(response, txt, DEFAULT_CHARSET);
    }

    /**
     * write xml into response
     *
     * @param response
     * @param xmlString
     * @param charset
     * @author YANGYUCHENG890
     */
    public static void writeXmlIntoResponse(HttpServletResponse response, String xmlString, String charset) {
        OutputStream outputStream = null;
        try {
            logger.info("[writeXmlIntoResponse] xmlString: " + xmlString);
            response.setCharacterEncoding(charset);
            response.setContentType("text/xml;charset=" + charset.toLowerCase());
            outputStream = response.getOutputStream();
            outputStream.write(xmlString.getBytes(charset));
            outputStream.flush();
        } catch (Exception e) {
            logger.error("[writeXmlIntoResponse] exception: " + e.getMessage(), e);
        } finally {
            if (outputStream != null) {
                try {
                    outputStream.close();
                } catch (Exception e2) {
                    logger.error("[writeXmlIntoResponse] exception to close outputStream: " + e2.getMessage());
                }
            }
        }
    }

    /**
     * write xml into response
     *
     * @param response
     * @param xmlString
     * @author YANGYUCHENG890
     */
    public static void writeXmlIntoResponse(HttpServletResponse response, String xmlString) {
        writeXmlIntoResponse(response, xmlString, DEFAULT_CHARSET);
    }


}

