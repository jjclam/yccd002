package com.sinosoft.cloud.access.entity;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("ContnoList")
public class ContnoList {

	@XStreamAlias("ContnoDetail")
    private ContnoDetail contnoDetail;

	public ContnoDetail getContnoDetail() {
		return contnoDetail;
	}

	public void setContnoDetail(ContnoDetail contnoDetail) {
		this.contnoDetail = contnoDetail;
	}

	@Override
	public String toString() {
		return "ContnoList [contnoDetail=" + contnoDetail + "]";
	}
}
