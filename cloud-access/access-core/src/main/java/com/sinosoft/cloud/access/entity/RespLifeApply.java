package com.sinosoft.cloud.access.entity;

/**
 * @Author:lizhen
 * @Description:
 * @Date:Created in 17:47 2018/11/12
 * @Modified By:
 */

/**
 * 生存调查申请POS049.
 */
public class RespLifeApply {
    //保全项目编码
    private String EdorType;

    public String getEdorType() {
        return EdorType;
    }

    public void setEdorType(String edorType) {
        EdorType = edorType;
    }

    @Override
    public String toString() {
        return "RespLifeApply{" +
                "EdorType='" + EdorType + '\'' +
                '}';
    }
}