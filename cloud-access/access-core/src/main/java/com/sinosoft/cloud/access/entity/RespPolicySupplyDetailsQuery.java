package com.sinosoft.cloud.access.entity;


/**
 * 保单补发详情查询POS002. 返回
 */
public class RespPolicySupplyDetailsQuery {
    //保单号
    private String ContNo;
    //投保人名称
    private String AppntName;
    //交费金额
    private String GetMoney;
    //补发原因
    private String CauseOfReissue;
    //开户行
    private String BankName;
    //帐号
    private String BankAccNo;
    //账户名
    private String BankAccName;

    public String getContNo() {
        return ContNo;
    }

    public void setContNo(String contNo) {
        ContNo = contNo;
    }

    public String getAppntName() {
        return AppntName;
    }

    public void setAppntName(String appntName) {
        AppntName = appntName;
    }

    public String getGetMoney() {
        return GetMoney;
    }

    public void setGetMoney(String getMoney) {
        GetMoney = getMoney;
    }

    public String getCauseOfReissue() {
        return CauseOfReissue;
    }

    public void setCauseOfReissue(String causeOfReissue) {
        CauseOfReissue = causeOfReissue;
    }

    public String getBankName() {
        return BankName;
    }

    public void setBankName(String bankName) {
        BankName = bankName;
    }

    public String getBankAccNo() {
        return BankAccNo;
    }

    public void setBankAccNo(String bankAccNo) {
        BankAccNo = bankAccNo;
    }

    public String getBankAccName() {
        return BankAccName;
    }

    public void setBankAccName(String bankAccName) {
        BankAccName = bankAccName;
    }

    @Override
    public String toString() {
        return "RespPolicySupplyDetailsQuery{" +
                "ContNo='" + ContNo + '\'' +
                ", AppntName='" + AppntName + '\'' +
                ", GetMoney='" + GetMoney + '\'' +
                ", CauseOfReissue='" + CauseOfReissue + '\'' +
                ", BankName='" + BankName + '\'' +
                ", BankAccNo='" + BankAccNo + '\'' +
                ", BankAccName='" + BankAccName + '\'' +
                '}';
    }
}
