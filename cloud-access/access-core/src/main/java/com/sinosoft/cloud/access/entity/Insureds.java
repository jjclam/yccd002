package com.sinosoft.cloud.access.entity;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.List;

/**
 * Created by zhaoshulei on 2017/9/28.
 */
@XStreamAlias("Insureds")
public class Insureds {

    @XStreamImplicit(itemFieldName="Insured")
    private List<Insured> insureds;

    public List<Insured> getInsureds() {
        return insureds;
    }

    public void setInsureds(List<Insured> insureds) {
        this.insureds = insureds;
    }

    @Override
    public String toString() {
        return "Insureds{" +
                "insureds=" + insureds +
                '}';
    }
}
