package com.sinosoft.cloud.access.entity;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("Insured")
public class Insured {
    @XStreamAlias("Name")
    private String name;
    @XStreamAlias("Sex")
    private String sex;
    @XStreamAlias("Birthday")
    private String birthday;
    @XStreamAlias("IDType")
    private String iDType;
    @XStreamAlias("IDNo")
    private String iDNo;
    @XStreamAlias("InsValidityday")
    private String insValidityday;
    @XStreamAlias("JobCode")
    private String jobCode;
    @XStreamAlias("Nationality")
    private String nationality;
    @XStreamAlias("Address")
    private String address;
    @XStreamAlias("ZipCode")
    private String zipCode;
    @XStreamAlias("Mobile")
    private String mobile;
    @XStreamAlias("Phone")
    private String phone;
    @XStreamAlias("Email")
    private String email;
    @XStreamAlias("Province")
    private String province;
    @XStreamAlias("City")
    private String city;
    @XStreamAlias("County")
    private String county;
    //1004所需要的字段
    @XStreamAlias("InsuredNo")
    private String insuredNo;

    @XStreamAlias("JobType")
    private String jobType;
    @XStreamAlias("GrpName")
    private String grpName;

    //用于将pojo中赋的死值映射到mapping配置文件中
    private String unChangedValue;

    public String getUnChangedValue() {
        return unChangedValue;
    }

    public void setUnChangedValue(String unChangedValue) {
        this.unChangedValue = unChangedValue;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getiDType() {
        return iDType;
    }

    public void setiDType(String iDType) {
        this.iDType = iDType;
    }

    public String getiDNo() {
        return iDNo;
    }

    public void setiDNo(String iDNo) {
        this.iDNo = iDNo;
    }

    public String getInsValidityday() {
        return insValidityday;
    }

    public void setInsValidityday(String insValidityday) {
        this.insValidityday = insValidityday;
    }

    public String getJobCode() {
        return jobCode;
    }

    public void setJobCode(String jobCode) {
        this.jobCode = jobCode;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCounty() {
        return county;
    }

    public void setCounty(String county) {
        this.county = county;
    }

    public String getInsuredNo() {
        return insuredNo;
    }

    public void setInsuredNo(String insuredNo) {
        this.insuredNo = insuredNo;
    }

    public String getJobType() {
        return jobType;
    }

    public void setJobType(String jobType) {
        this.jobType = jobType;
    }

    public String getGrpName() {
        return grpName;
    }

    public void setGrpName(String grpName) {
        this.grpName = grpName;
    }

    @Override
    public String toString() {
        return "Insured{" +
                "name='" + name + '\'' +
                ", sex='" + sex + '\'' +
                ", birthday='" + birthday + '\'' +
                ", iDType='" + iDType + '\'' +
                ", iDNo='" + iDNo + '\'' +
                ", insValidityday='" + insValidityday + '\'' +
                ", jobCode='" + jobCode + '\'' +
                ", nationality='" + nationality + '\'' +
                ", address='" + address + '\'' +
                ", zipCode='" + zipCode + '\'' +
                ", mobile='" + mobile + '\'' +
                ", phone='" + phone + '\'' +
                ", email='" + email + '\'' +
                ", province='" + province + '\'' +
                ", city='" + city + '\'' +
                ", county='" + county + '\'' +
                ", insuredNo='" + insuredNo + '\'' +
                ", jobType='" + jobType + '\'' +
                ", grpName='" + grpName + '\'' +
                '}';
    }
}
