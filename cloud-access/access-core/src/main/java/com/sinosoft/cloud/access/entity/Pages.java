package com.sinosoft.cloud.access.entity;

/**
 * @Author:lizhen
 * @Description:
 * @Date:Created in 16:36 2018/11/29
 * @Modified By:
 */
public class Pages {
    private String PageCode;
    private String PageName;
    private String PageSuffix;
    private String PicPathFTP;
    private String Page_URL;

    public String getPageCode() {
        return PageCode;
    }

    public void setPageCode(String pageCode) {
        PageCode = pageCode;
    }

    public String getPageName() {
        return PageName;
    }

    public void setPageName(String pageName) {
        PageName = pageName;
    }

    public String getPageSuffix() {
        return PageSuffix;
    }

    public void setPageSuffix(String pageSuffix) {
        PageSuffix = pageSuffix;
    }

    public String getPicPathFTP() {
        return PicPathFTP;
    }

    public void setPicPathFTP(String picPathFTP) {
        PicPathFTP = picPathFTP;
    }

    public String getPage_URL() {
        return Page_URL;
    }

    public void setPage_URL(String page_URL) {
        Page_URL = page_URL;
    }

    @Override
    public String toString() {
        return "Pages{" +
                "PageCode='" + PageCode + '\'' +
                ", PageName='" + PageName + '\'' +
                ", PageSuffix='" + PageSuffix + '\'' +
                ", PicPathFTP='" + PicPathFTP + '\'' +
                ", Page_URL='" + Page_URL + '\'' +
                '}';
    }
}