package com.sinosoft.cloud.access.entity;

/**
 * 理赔报案CLM002
 */
public class RespClaimReport {
    //赔案号
    private String ClaimNo;

    @Override
    public String toString() {
        return "RespClaimReport{" +
                "ClaimNo='" + ClaimNo + '\'' +
                '}';
    }

    public String getClaimNo() {
        return ClaimNo;
    }

    public void setClaimNo(String claimNo) {
        ClaimNo = claimNo;
    }
    
    public String getV(String s) {
        return null;
    }
    
    public String getV(int i) {
        return null;
    }
    
    public int getFieldType(String s) {
        return 0;
    }
    
    public int getFieldType(int i) {
        return 0;
    }
    
    public int getFieldCount() {
        return 0;
    }

    
    public int getFieldIndex(String s) {
        return 0;
    }

    
    public String getFieldName(int i) {
        return null;
    }

    
    public boolean setV(String s, String s1) {
        return false;
    }
}
