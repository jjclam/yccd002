package com.sinosoft.cloud.access.entity;

import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.List;

/**
 * @Description
 * @Author:zhaoshulei
 * @Date:2018/4/10
 */
public class LCBnfs {

    @XStreamImplicit(itemFieldName="LCBnf")
    private List<LCBnf> LCBnfs;

    public List<LCBnf> getLCBnfs() {
        return LCBnfs;
    }

    public void setLCBnfs(List<LCBnf> LCBnfs) {
        this.LCBnfs = LCBnfs;
    }

    @Override
    public String toString() {
        return "LCBnfs{" +
                "LCBnfs=" + LCBnfs +
                '}';
    }
}
