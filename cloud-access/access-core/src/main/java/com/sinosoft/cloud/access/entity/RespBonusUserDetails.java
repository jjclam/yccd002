package com.sinosoft.cloud.access.entity;

/**
 * 红利领取要操作保全用户详情POS025
 */
public class RespBonusUserDetails {
    //保全项目编码
    private String EdorType;
    //保单号
    private String ContNo;
    //投保人姓名
    private String AppntName;
    //主险名称
    private String MainRiskName;
    //领取金额/万能账户余额/getMoney/账户价值/追加保费金额/退保金额
    private String GetMoney;
    //开户xxx
    private String NewBankName;
    //xxx帐号
    private String NewBankAccNo;
    //户名
    private String NewBankAccName;
    //手机电话号码
    private String MobilePhone;
    //账户余额/累积生息账户余额
    private String AccountBalance;
    //涉及险种
    private String RelationRisk;

    public String getEdorType() {
        return EdorType;
    }

    public void setEdorType(String edorType) {
        EdorType = edorType;
    }

    public String getContNo() {
        return ContNo;
    }

    public void setContNo(String contNo) {
        ContNo = contNo;
    }

    public String getAppntName() {
        return AppntName;
    }

    public void setAppntName(String appntName) {
        AppntName = appntName;
    }

    public String getMainRiskName() {
        return MainRiskName;
    }

    public void setMainRiskName(String mainRiskName) {
        MainRiskName = mainRiskName;
    }

    public String getGetMoney() {
        return GetMoney;
    }

    public void setGetMoney(String getMoney) {
        GetMoney = getMoney;
    }

    public String getNewBankName() {
        return NewBankName;
    }

    public void setNewBankName(String newBankName) {
        NewBankName = newBankName;
    }

    public String getNewBankAccNo() {
        return NewBankAccNo;
    }

    public void setNewBankAccNo(String newBankAccNo) {
        NewBankAccNo = newBankAccNo;
    }

    public String getNewBankAccName() {
        return NewBankAccName;
    }

    public void setNewBankAccName(String newBankAccName) {
        NewBankAccName = newBankAccName;
    }

    public String getMobilePhone() {
        return MobilePhone;
    }

    public void setMobilePhone(String mobilePhone) {
        MobilePhone = mobilePhone;
    }

    public String getAccountBalance() {
        return AccountBalance;
    }

    public void setAccountBalance(String accountBalance) {
        AccountBalance = accountBalance;
    }

    public String getRelationRisk() {
        return RelationRisk;
    }

    public void setRelationRisk(String relationRisk) {
        RelationRisk = relationRisk;
    }

    @Override
    public String toString() {
        return "RespBonusUserDetails{" +
                "EdorType='" + EdorType + '\'' +
                ", ContNo='" + ContNo + '\'' +
                ", AppntName='" + AppntName + '\'' +
                ", MainRiskName='" + MainRiskName + '\'' +
                ", GetMoney='" + GetMoney + '\'' +
                ", NewBankName='" + NewBankName + '\'' +
                ", NewBankAccNo='" + NewBankAccNo + '\'' +
                ", NewBankAccName='" + NewBankAccName + '\'' +
                ", MobilePhone='" + MobilePhone + '\'' +
                ", AccountBalance='" + AccountBalance + '\'' +
                ", RelationRisk='" + RelationRisk + '\'' +
                '}';
    }
}
