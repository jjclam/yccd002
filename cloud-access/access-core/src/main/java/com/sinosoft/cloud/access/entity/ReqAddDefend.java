package com.sinosoft.cloud.access.entity;

/**
 * @program: abc-cloud-access
 * @description: 4012-加保接口POS116.
 * @author: BaoYongmeng
 * @create: 2018-12-24 16:42
 **/
public class ReqAddDefend {
    private String EdorType;
    //保单号
    private String ContNo;
    //领取金额/万能账户余额/getMoney/账户价值/追加保费金额/退保金额
    private String GetMoney;
    //xxx网点
    private String BankCode;
    //开户xxx
    private String BankAccNo;
    //是否为美国纳税义务的个人
    private String TinFlag;
    //美国纳税人识别号
    private String TinNo;

    public void setEdorType(String edorType) {
        EdorType = edorType;
    }

    public void setContNo(String contNo) {
        ContNo = contNo;
    }

    public void setGetMoney(String getMoney) {
        GetMoney = getMoney;
    }

    public void setBankCode(String bankCode) {
        BankCode = bankCode;
    }

    public void setBankAccNo(String bankAccNo) {
        BankAccNo = bankAccNo;
    }

    public void setTinFlag(String tinFlag) {
        TinFlag = tinFlag;
    }

    public void setTinNo(String tinNo) {
        TinNo = tinNo;
    }

    public String getEdorType() {
        return EdorType;
    }

    public String getContNo() {
        return ContNo;
    }

    public String getGetMoney() {
        return GetMoney;
    }

    public String getBankCode() {
        return BankCode;
    }

    public String getBankAccNo() {
        return BankAccNo;
    }

    public String getTinFlag() {
        return TinFlag;
    }

    public String getTinNo() {
        return TinNo;
    }

    @Override
    public String toString() {
        return "ReqAddDefend{" +
                "EdorType='" + EdorType + '\'' +
                ", ContNo='" + ContNo + '\'' +
                ", GetMoney='" + GetMoney + '\'' +
                ", BankCode='" + BankCode + '\'' +
                ", BankAccNo='" + BankAccNo + '\'' +
                ", TinFlag='" + TinFlag + '\'' +
                ", TinNo='" + TinNo + '\'' +
                '}';
    }
}
