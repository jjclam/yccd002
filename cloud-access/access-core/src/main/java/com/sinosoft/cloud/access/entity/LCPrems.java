package com.sinosoft.cloud.access.entity;

import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.List;

/**
 * Created by Administrator on 2018/4/16.
 */
public class LCPrems {
    @XStreamImplicit(itemFieldName="LCPrem")
    private List<LCPrem> LCPrems;

    public List<LCPrem> getLCPrems() {
        return LCPrems;
    }

    public void setLCPrems(List<LCPrem> LCPrems) {
        this.LCPrems = LCPrems;
    }

    @Override
    public String toString() {
        return "LCPrems{" +
                "LCPrems=" + LCPrems +
                '}';
    }
}
