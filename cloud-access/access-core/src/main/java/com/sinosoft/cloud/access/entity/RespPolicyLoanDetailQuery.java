package com.sinosoft.cloud.access.entity;

/**
 * @Author:lizhen
 * @Description:
 * @Date:Created in 15:55 2018/11/16
 * @Modified By:
 */
/**
 * 4003保单贷款明细查询POS074 POS125返回
 */
public class RespPolicyLoanDetailQuery {
    //保全项目编码
    private String EdorType;
    //证件姓名
    private String CustomerName;
    //贷款年利率
    private String LoanAnnualRate;
    //贷款时长
    private String LoanDays;
    //最大贷款数额
    private String MaxLoan;
    //账户名称
    private String BankName;
    //开户xxx
    private String BankAccName;
    //xxx账号
    private String BankAccNo;
    //手机电话号码
    private String MobilePhone;
    //主险编码
    private String MainRiskCode;
    //主险名称
    private String MainRiskName;
    //保额
    private String Amnt;
    //保费
    private String Prem;
    //现金价值
    private String CashValue;
    //附加险
    private AddtRisks AddtRisks;
    //保单号
    private String ContNo;
    //贷款金额
    private String LoanMoney;
    private MainRisks MainRisks;
    //还款状态
    private String RepaymentStatus;
    public String getEdorType() {
        return EdorType;
    }

    public void setEdorType(String edorType) {
        EdorType = edorType;
    }

    public String getCustomerName() {
        return CustomerName;
    }

    public void setCustomerName(String customerName) {
        CustomerName = customerName;
    }

    public String getLoanAnnualRate() {
        return LoanAnnualRate;
    }

    public void setLoanAnnualRate(String loanAnnualRate) {
        LoanAnnualRate = loanAnnualRate;
    }

    public String getLoanDays() {
        return LoanDays;
    }

    public void setLoanDays(String loanDays) {
        LoanDays = loanDays;
    }

    public String getMaxLoan() {
        return MaxLoan;
    }

    public void setMaxLoan(String maxLoan) {
        MaxLoan = maxLoan;
    }

    public String getBankName() {
        return BankName;
    }

    public void setBankName(String bankName) {
        BankName = bankName;
    }

    public String getBankAccName() {
        return BankAccName;
    }

    public void setBankAccName(String bankAccName) {
        BankAccName = bankAccName;
    }

    public String getBankAccNo() {
        return BankAccNo;
    }

    public void setBankAccNo(String bankAccNo) {
        BankAccNo = bankAccNo;
    }

    public String getMobilePhone() {
        return MobilePhone;
    }

    public void setMobilePhone(String mobilePhone) {
        MobilePhone = mobilePhone;
    }

    public String getMainRiskCode() {
        return MainRiskCode;
    }

    public void setMainRiskCode(String mainRiskCode) {
        MainRiskCode = mainRiskCode;
    }

    public String getMainRiskName() {
        return MainRiskName;
    }

    public void setMainRiskName(String mainRiskName) {
        MainRiskName = mainRiskName;
    }

    public String getAmnt() {
        return Amnt;
    }

    public void setAmnt(String amnt) {
        Amnt = amnt;
    }

    public String getPrem() {
        return Prem;
    }

    public void setPrem(String prem) {
        Prem = prem;
    }

    public String getCashValue() {
        return CashValue;
    }

    public void setCashValue(String cashValue) {
        CashValue = cashValue;
    }

    public com.sinosoft.cloud.access.entity.AddtRisks getAddtRisks() {
        return AddtRisks;
    }

    public void setAddtRisks(com.sinosoft.cloud.access.entity.AddtRisks addtRisks) {
        AddtRisks = addtRisks;
    }

    public String getContNo() {
        return ContNo;
    }

    public void setContNo(String contNo) {
        ContNo = contNo;
    }

    public String getLoanMoney() {
        return LoanMoney;
    }

    public void setLoanMoney(String loanMoney) {
        LoanMoney = loanMoney;
    }

    public com.sinosoft.cloud.access.entity.MainRisks getMainRisks() {
        return MainRisks;
    }

    public void setMainRisks(com.sinosoft.cloud.access.entity.MainRisks mainRisks) {
        MainRisks = mainRisks;
    }

    public String getRepaymentStatus() {
        return RepaymentStatus;
    }

    public void setRepaymentStatus(String repaymentStatus) {
        RepaymentStatus = repaymentStatus;
    }

    @Override
    public String toString() {
        return "RespPolicyLoanDetailQuery{" +
                "EdorType='" + EdorType + '\'' +
                ", CustomerName='" + CustomerName + '\'' +
                ", LoanAnnualRate='" + LoanAnnualRate + '\'' +
                ", LoanDays='" + LoanDays + '\'' +
                ", MaxLoan='" + MaxLoan + '\'' +
                ", BankName='" + BankName + '\'' +
                ", BankAccName='" + BankAccName + '\'' +
                ", BankAccNo='" + BankAccNo + '\'' +
                ", MobilePhone='" + MobilePhone + '\'' +
                ", MainRiskCode='" + MainRiskCode + '\'' +
                ", MainRiskName='" + MainRiskName + '\'' +
                ", Amnt='" + Amnt + '\'' +
                ", Prem='" + Prem + '\'' +
                ", CashValue='" + CashValue + '\'' +
                ", AddtRisks=" + AddtRisks +
                ", ContNo='" + ContNo + '\'' +
                ", LoanMoney='" + LoanMoney + '\'' +
                ", MainRisks=" + MainRisks +
                ", RepaymentStatus='" + RepaymentStatus + '\'' +
                '}';
    }
}