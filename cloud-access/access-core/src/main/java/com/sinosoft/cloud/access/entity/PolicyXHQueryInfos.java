package com.sinosoft.cloud.access.entity;

import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.List;

/**
 * @program: abc-cloud-access
 * @description:
 * @author: BaoYongmeng
 * @create: 2018-12-24 17:20
 **/
public class PolicyXHQueryInfos {

    @XStreamImplicit(itemFieldName="PolicyXHQueryInfo")
    private List<PolicyXHQueryInfo> PolicyXHQueryInfo;

    public List<com.sinosoft.cloud.access.entity.PolicyXHQueryInfo> getPolicyXHQueryInfo() {
        return PolicyXHQueryInfo;
    }

    public void setPolicyXHQueryInfo(List<com.sinosoft.cloud.access.entity.PolicyXHQueryInfo> policyXHQueryInfo) {
        PolicyXHQueryInfo = policyXHQueryInfo;
    }

    @Override
    public String toString() {
        return "PolicyXHQueryInfos{" +
                "PolicyXHQueryInfo=" + PolicyXHQueryInfo +
                '}';
    }
}
