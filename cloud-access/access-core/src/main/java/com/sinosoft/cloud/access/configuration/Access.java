package com.sinosoft.cloud.access.configuration;

/**
 * cloud-access
 *
 * @title: cloud-access
 * @package: com.sinosoft.cloud.access.configuration
 * @author: yangming
 * @date: 2017/11/14 下午4:58
 */
public interface Access {

    String getPort();

    void setPort(String port);
}
