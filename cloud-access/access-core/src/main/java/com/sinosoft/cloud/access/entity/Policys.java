package com.sinosoft.cloud.access.entity;

import com.sinosoft.lis.pos.entity.PolicyPojo;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.List;

/**
 * @Author:lizhen
 * @Description:
 * @Date:Created in 19:49 2018/11/15
 * @Modified By:
 */
public class Policys {
    @XStreamImplicit(itemFieldName="Policy")
    private List<Policy> Policy;

    public List<Policy> getPolicy() {
        return Policy;
    }

    public void setPolicy(List<Policy> policy) {
        Policy = policy;
    }

    @Override
    public String toString() {
        return "Policys{" +
                "Policy=" + Policy +
                '}';
    }
}