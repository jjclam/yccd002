package com.sinosoft.cloud.access.entity;

/**
 * Created by Administrator on 2018/4/13.
 */
public class LCCustomerImpart {
    private long CustomerImpartID;
    private long ContID;
    private String ShardingID;
    private String GrpContNo;
    private String ContNo;
    private String ProposalContNo;
    private String PrtNo;
    private String ImpartCode;
    private String ImpartVer;
    private String ImpartContent;
    private String ImpartParamModle;
    private String CustomerNo;
    private String CustomerNoType;
    private String UWClaimFlg;
    private String PrtFlag;
    private String Operator;
    private String MakeDate;
    private String MakeTime;
    private String ModifyDate;
    private String ModifyTime;
    private int PatchNo;

    public long getCustomerImpartID() {
        return CustomerImpartID;
    }

    public void setCustomerImpartID(long customerImpartID) {
        CustomerImpartID = customerImpartID;
    }

    public long getContID() {
        return ContID;
    }

    public void setContID(long contID) {
        ContID = contID;
    }

    public String getShardingID() {
        return ShardingID;
    }

    public void setShardingID(String shardingID) {
        ShardingID = shardingID;
    }

    public String getGrpContNo() {
        return GrpContNo;
    }

    public void setGrpContNo(String grpContNo) {
        GrpContNo = grpContNo;
    }

    public String getContNo() {
        return ContNo;
    }

    public void setContNo(String contNo) {
        ContNo = contNo;
    }

    public String getProposalContNo() {
        return ProposalContNo;
    }

    public void setProposalContNo(String proposalContNo) {
        ProposalContNo = proposalContNo;
    }

    public String getPrtNo() {
        return PrtNo;
    }

    public void setPrtNo(String prtNo) {
        PrtNo = prtNo;
    }

    public String getImpartCode() {
        return ImpartCode;
    }

    public void setImpartCode(String impartCode) {
        ImpartCode = impartCode;
    }

    public String getImpartVer() {
        return ImpartVer;
    }

    public void setImpartVer(String impartVer) {
        ImpartVer = impartVer;
    }

    public String getImpartContent() {
        return ImpartContent;
    }

    public void setImpartContent(String impartContent) {
        ImpartContent = impartContent;
    }

    public String getImpartParamModle() {
        return ImpartParamModle;
    }

    public void setImpartParamModle(String impartParamModle) {
        ImpartParamModle = impartParamModle;
    }

    public String getCustomerNo() {
        return CustomerNo;
    }

    public void setCustomerNo(String customerNo) {
        CustomerNo = customerNo;
    }

    public String getCustomerNoType() {
        return CustomerNoType;
    }

    public void setCustomerNoType(String customerNoType) {
        CustomerNoType = customerNoType;
    }

    public String getUWClaimFlg() {
        return UWClaimFlg;
    }

    public void setUWClaimFlg(String UWClaimFlg) {
        this.UWClaimFlg = UWClaimFlg;
    }

    public String getPrtFlag() {
        return PrtFlag;
    }

    public void setPrtFlag(String prtFlag) {
        PrtFlag = prtFlag;
    }

    public String getOperator() {
        return Operator;
    }

    public void setOperator(String operator) {
        Operator = operator;
    }

    public String getMakeDate() {
        return MakeDate;
    }

    public void setMakeDate(String makeDate) {
        MakeDate = makeDate;
    }

    public String getMakeTime() {
        return MakeTime;
    }

    public void setMakeTime(String makeTime) {
        MakeTime = makeTime;
    }

    public String getModifyDate() {
        return ModifyDate;
    }

    public void setModifyDate(String modifyDate) {
        ModifyDate = modifyDate;
    }

    public String getModifyTime() {
        return ModifyTime;
    }

    public void setModifyTime(String modifyTime) {
        ModifyTime = modifyTime;
    }

    public int getPatchNo() {
        return PatchNo;
    }

    public void setPatchNo(int patchNo) {
        PatchNo = patchNo;
    }

    @Override
    public String toString() {
        return "LCCustomerImpart{" +
                "CustomerImpartID=" + CustomerImpartID +
                ", ContID=" + ContID +
                ", ShardingID='" + ShardingID + '\'' +
                ", GrpContNo='" + GrpContNo + '\'' +
                ", ContNo='" + ContNo + '\'' +
                ", ProposalContNo='" + ProposalContNo + '\'' +
                ", PrtNo='" + PrtNo + '\'' +
                ", ImpartCode='" + ImpartCode + '\'' +
                ", ImpartVer='" + ImpartVer + '\'' +
                ", ImpartContent='" + ImpartContent + '\'' +
                ", ImpartParamModle='" + ImpartParamModle + '\'' +
                ", CustomerNo='" + CustomerNo + '\'' +
                ", CustomerNoType='" + CustomerNoType + '\'' +
                ", UWClaimFlg='" + UWClaimFlg + '\'' +
                ", PrtFlag='" + PrtFlag + '\'' +
                ", Operator='" + Operator + '\'' +
                ", MakeDate='" + MakeDate + '\'' +
                ", MakeTime='" + MakeTime + '\'' +
                ", ModifyDate='" + ModifyDate + '\'' +
                ", ModifyTime='" + ModifyTime + '\'' +
                ", PatchNo=" + PatchNo +
                '}';
    }
}
