package com.sinosoft.cloud.access.entity;

/**
 * @Author:lizhen
 * @Description:
 * @Date:Created in 15:23 2018/4/11
 * @Modified By:
 */
public class BQQuery {
    private String AppNo;
    private String PolicyNo;
    private String IDNo;
    private String IDType;
    private String PrintCode;
    private String ClientName;
    private String PolicyPwd;
    private String PayeetName;
    private String PayeeIdKind;
    private String PayeeIdCode;
    private String PayAcc;
    private double Amt;
    private String BusiType;
    private double GetAmt;
    private String PicFileName;
    private String RiskCode;

    public BQQuery() {
    }

    public String getRiskCode() {
        return this.RiskCode;
    }

    public void setRiskCode(String riskCode) {
        this.RiskCode = riskCode;
    }

    public String getAppNo() {
        return this.AppNo;
    }

    public void setAppNo(String appNo) {
        this.AppNo = appNo;
    }

    public String getPolicyNo() {
        return this.PolicyNo;
    }

    public void setPolicyNo(String policyNo) {
        this.PolicyNo = policyNo;
    }

    public String getPolicyPwd() {
        return this.PolicyPwd;
    }

    public void setPolicyPwd(String policyPwd) {
        this.PolicyPwd = policyPwd;
    }

    public String getPayeetName() {
        return this.PayeetName;
    }

    public void setPayeetName(String payeetName) {
        this.PayeetName = payeetName;
    }

    public String getPayeeIdKind() {
        return this.PayeeIdKind;
    }

    public void setPayeeIdKind(String payeeIdKind) {
        this.PayeeIdKind = payeeIdKind;
    }

    public String getPayeeIdCode() {
        return this.PayeeIdCode;
    }

    public void setPayeeIdCode(String payeeIdCode) {
        this.PayeeIdCode = payeeIdCode;
    }

    public String getPayAcc() {
        return this.PayAcc;
    }

    public void setPayAcc(String payAcc) {
        this.PayAcc = payAcc;
    }

    public String getBusiType() {
        return this.BusiType;
    }

    public void setBusiType(String busiType) {
        this.BusiType = busiType;
    }

    public String getPicFileName() {
        return this.PicFileName;
    }

    public void setPicFileName(String picFileName) {
        this.PicFileName = picFileName;
    }

    public String getIDNo() {
        return this.IDNo;
    }

    public void setIDNo(String IDNo) {
        this.IDNo = IDNo;
    }

    public double getAmt() {
        return this.Amt;
    }

    public void setAmt(double amt) {
        this.Amt = amt;
    }

    public double getGetAmt() {
        return this.GetAmt;
    }

    public void setGetAmt(double getAmt) {
        this.GetAmt = getAmt;
    }

    public String getIDType() {
        return this.IDType;
    }

    public void setIDType(String IDType) {
        this.IDType = IDType;
    }

    public String getPrintCode() {
        return this.PrintCode;
    }

    public void setPrintCode(String printCode) {
        this.PrintCode = printCode;
    }

    public String getClientName() {
        return this.ClientName;
    }

    public void setClientName(String clientName) {
        this.ClientName = clientName;
    }

    @Override
    public String toString() {
        return "BQApplyPojo{AppNo=\'" + this.AppNo + '\'' + ", PolicyNo=\'" + this.PolicyNo + '\'' + ", IDNo=" + this.IDNo + ", IDType=" + this.IDType + ", PrintCode=" + this.PrintCode + ", ClientName=" + this.ClientName + ", PolicyPwd=\'" + this.PolicyPwd + '\'' + ", PayeetName=\'" + this.PayeetName + '\'' + ", PayeeIdKind=\'" + this.PayeeIdKind + '\'' + ", PayeeIdCode=\'" + this.PayeeIdCode + '\'' + ", PayAcc=\'" + this.PayAcc + '\'' + ", Amt=\'" + this.Amt + '\'' + ", BusiType=\'" + this.BusiType + '\'' + ", GetAmt=\'" + this.GetAmt + '\'' + ", PicFileName=\'" + this.PicFileName + '\'' + '}';
    }

    public String getV(String s) {
        return null;
    }

    public String getV(int i) {
        return null;
    }

    public int getFieldType(String s) {
        return 0;
    }

    public int getFieldType(int i) {
        return 0;
    }

    public int getFieldCount() {
        return 0;
    }

    public int getFieldIndex(String s) {
        return 0;
    }

    public String getFieldName(int i) {
        return null;
    }

    public boolean setV(String s, String s1) {
        return false;
    }

}