package com.sinosoft.cloud.access.entity;

/**
 * @Description:1016/1021返回保单价值和账户价值
 * @Author:zhaoshulei
 * @Date:2018/4/13
 */
public class Reserve {
    private String RiskCode;
    private String PolicyValue;
    private String AccountValue;

    public String getRiskCode() {
        return RiskCode;
    }

    public void setRiskCode(String riskCode) {
        RiskCode = riskCode;
    }

    public String getPolicyValue() {
        return PolicyValue;
    }

    public void setPolicyValue(String policyValue) {
        PolicyValue = policyValue;
    }

    public String getAccountValue() {
        return AccountValue;
    }

    public void setAccountValue(String accountValue) {
        AccountValue = accountValue;
    }

    @Override
    public String toString() {
        return "Reserve{" +
                "RiskCode='" + RiskCode + '\'' +
                ", PolicyValue='" + PolicyValue + '\'' +
                ", AccountValue='" + AccountValue + '\'' +
                '}';
    }
}
