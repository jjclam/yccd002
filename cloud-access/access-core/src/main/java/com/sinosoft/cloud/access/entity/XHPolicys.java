package com.sinosoft.cloud.access.entity;

import com.sinosoft.lis.pos.entity.XHPolicyPojo;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.List;

/**
 * @Author:lizhen
 * @Description:
 * @Date:Created in 18:19 2018/11/16
 * @Modified By:
 */
public class XHPolicys {
    @XStreamImplicit(itemFieldName="XHPolicy")
    private List<XHPolicy> XHPolicy;

    public List<XHPolicy> getXHPolicy() {
        return XHPolicy;
    }

    public void setXHPolicy(List<XHPolicy> XHPolicy) {
        this.XHPolicy = XHPolicy;
    }

    @Override
    public String toString() {
        return "XHPolicys{" +
                "XHPolicy=" + XHPolicy +
                '}';
    }
}