package com.sinosoft.cloud.access.entity;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.List;

/**
 * @project: abc-cloud-access
 * @author: yangming
 * @date: 2017/9/19 下午7:09
 * To change this template use File | Settings | File and Code Templates.
 */
@XStreamAlias("CustomerImparts")
public class CustomerImparts {
    @XStreamImplicit(itemFieldName="CustomerImpart")
    private List<CustomerImpart> CustomerImparts;

    public List<CustomerImpart> getCustomerImparts() {
        return CustomerImparts;
    }

    public void setCustomerImparts(List<CustomerImpart> customerImparts) {
        CustomerImparts = customerImparts;
    }

    @Override
    public String toString() {
        return "CustomerImparts{" +
                "CustomerImparts=" + CustomerImparts +
                '}';
    }
}
