package com.sinosoft.cloud.access.entity;

import com.sinosoft.cloud.access.annotations.BeanCopy;

/**
 * @Author:lizhen
 * @Description:
 * @Date:Created in 14:28 2018/11/12
 * @Modified By:
 */
/*
* NB007
* */
public class RespNotRealNewContnoApply {
    //投保单号
    private String PolicyApplyNo;
    //险种代码
    private String RiskCode;
    //产品代码
    private String ProdCode;
    //保费
    private String Prem;
    private AppInfo AppInfo;

    public String getPolicyApplyNo() {
        return PolicyApplyNo;
    }

    public void setPolicyApplyNo(String policyApplyNo) {
        PolicyApplyNo = policyApplyNo;
    }

    public String getRiskCode() {
        return RiskCode;
    }

    public void setRiskCode(String riskCode) {
        RiskCode = riskCode;
    }

    public String getProdCode() {
        return ProdCode;
    }

    public void setProdCode(String prodCode) {
        ProdCode = prodCode;
    }

    public String getPrem() {
        return Prem;
    }

    public void setPrem(String prem) {
        Prem = prem;
    }

    public com.sinosoft.cloud.access.entity.AppInfo getAppInfo() {
        return AppInfo;
    }

    public void setAppInfo(com.sinosoft.cloud.access.entity.AppInfo appInfo) {
        AppInfo = appInfo;
    }

    @Override
    public String toString() {
        return "RespNotRealNewContnoApply{" +
                "PolicyApplyNo='" + PolicyApplyNo + '\'' +
                ", RiskCode='" + RiskCode + '\'' +
                ", ProdCode='" + ProdCode + '\'' +
                ", Prem='" + Prem + '\'' +
                ", AppInfo=" + AppInfo +
                '}';
    }
}