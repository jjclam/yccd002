package com.sinosoft.cloud.access.configuration;

import com.sinosoft.cloud.access.net.DefaultHttpHandler;
import com.sinosoft.cloud.access.util.StaxonUtil;
import org.glassfish.jersey.server.internal.LocalizationMessages;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import javax.inject.Singleton;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.io.IOException;
import java.util.Map;

/**
 * cloud-access
 *
 * @title: cloud-access
 * @package: com.sinosoft.cloud.access.net
 * @author: yangming
 * @date: 2018/2/21 下午5:41
 */
@Singleton
@Path("/{accessName}/{dataType}")
@Service
public class RestfulEndpoint {

    private static final String JSON = "json";
    public static final String XML_ROOT = "AUTO_XML_DATA";


    @Autowired
    @Qualifier("accessMap")
    Map accessMap;

    /**
     * get请求
     *
     * @param accessName 渠道名称
     * @return
     * @throws IOException
     */
    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public String welcome(@PathParam("accessName") String accessName) throws IOException {

        if (accessMap == null) {
            return "没有初始化成功";
        }
        if (accessMap.get(accessName) == null) {
            throw new ForbiddenException(LocalizationMessages.USER_NOT_AUTHORIZED());
        }
        return accessName;
    }

    /**
     * post请求
     * @param accessName 渠道名称
     * @param dataType  数据类型（xml或者json）
     * @param data 数据
     * @return
     */
    @POST
    @Produces({MediaType.TEXT_PLAIN + ";" + MediaType.CHARSET_PARAMETER + "=utf-8"})
    public Object endpoint(@PathParam("accessName") String accessName, @PathParam("dataType") String dataType, String data) {

        if (accessMap == null) {
            return "没有初始化成功";
        }
        if (accessMap.get(accessName) == null) {
            throw new ForbiddenException(LocalizationMessages.USER_NOT_AUTHORIZED());
        }
        if (dataType == null || "".equals(dataType)) {
            throw new ForbiddenException("请传入数据类型,例如: http://hostname:port/picch/json 或 http://hostname:port/picch/xml ");
        }

        if (dataType.equals(JSON)) {
            data = StaxonUtil.json2xml(data, XML_ROOT);
        }

        DefaultHttpHandler handler = new DefaultHttpHandler();
        handler.setAccessName(accessName);
        String result = handler.submitData(data);
//        String result = data;
        if (dataType.equals(JSON)) {
            result = StaxonUtil.xml2json(result, XML_ROOT);
        }
        return result;
    }

}
