package com.sinosoft.cloud.access.net;

import com.sinosoft.cloud.access.annotations.AccessCoder;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.core.annotation.AnnotationUtils;

import javax.annotation.PostConstruct;

public abstract class CoderBean  implements Coder {

    protected final Log logger = LogFactory.getLog(getClass());


    private String accessName;

    public String getAccessName() {
        return accessName;
    }

    public void setAccessName(String accessName) {
        this.accessName = accessName;
    }


    @PostConstruct
    public void init() {
        AccessCoder decoder = AnnotationUtils.findAnnotation(this.getClass(), AccessCoder.class);
        setAccessName(decoder.name());
        logger.debug("新建解密器:" + decoder.name());
    }

    @Override
    public boolean isComplate(String msg) {
        int expBodySize = getBodySizeByHead(msg);
        int actuBodySize = getBodySize(msg);

        logger.debug("期望报文长度:" + expBodySize + ";实际报文长度:" + actuBodySize);
        if (expBodySize - actuBodySize == 0) {
            return true;
        } else {
            return false;
        }
    }
}
