package com.sinosoft.cloud.access.transformer;

import com.sinosoft.cloud.access.entity.Head;
import com.sinosoft.cloud.access.entity.TranData;
import com.sinosoft.cloud.access.service.AccessServiceBean;
import com.sinosoft.cloud.access.service.AccessServiceGate;
import com.sinosoft.cloud.access.transformer.function.CommonTrans;
import com.sinosoft.cloud.access.transformer.function.SaxFunction;

import com.sinosoft.cloud.rest.TradeInfo;
import com.sinosoft.utility.ExceptionUtils;
import net.sf.saxon.Configuration;
import net.sf.saxon.s9api.*;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.slf4j.MDC;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import javax.ws.rs.ForbiddenException;
import javax.xml.transform.stream.StreamSource;
import java.io.FileNotFoundException;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.HashMap;

import static com.sinosoft.cloud.access.configuration.AccessConfiguration.BASE_PACKAGE;



public class XsltTrans implements Trans {
    private static final Log logger = LogFactory.getLog(XsltTrans.class);
    private static Processor proc;
    private static final String ERROR_DESC = "内部错误，交易失败";

    private static final String ERROR_INFO = "交易失败，错误信息：";

    static {
        Configuration configuration = new Configuration();
        proc = new Processor(false);
        ExtensionFunction[] functions = SaxFunction.getFunctions();
        for (ExtensionFunction function : functions) {
            proc.registerExtensionFunction(function);
        }
        configuration.setProcessor(proc);
    }

    private static HashMap<String, XsltExecutable> xslCache = new HashMap();

    private String accessName;

    private CommonTrans mCommonTrans = new CommonTrans();

    /**
     * 进行格式转换、服务调用
     *
     */
    @Override
    public String transform(String msg) {
        TradeInfo tTradeInfo = mCommonTrans.xmlToObject(msg);
        if(tTradeInfo == null){
            throw new ForbiddenException("解析报文异常");
        }

        String returnBodyOut = msgService(tTradeInfo);

        return returnBodyOut;
    }

    /**
     * 使用服务网关进行服务调用
     *
     */
    private String msgService(TradeInfo tTradeInfo) {
        /*获取服务网关，服务网关是在系统启动时，初始化所有渠道的服务信息，存入缓存中*/
        AccessServiceGate accessServiceGate = AccessServiceGate.getInstance();
        /*根据报文头信息获取对应的服务类*/
        AccessServiceBean accessServiceBean;
        String serialNo =(String) tTradeInfo.getData("SerialNo");


        accessServiceBean= accessServiceGate.getServiceByTranData(getAccessName(), (String)tTradeInfo.getData("TransType"));

        /*执行服务类，在服务类中应 应调用微服务信息*/
        TradeInfo returnData = null;
        try {
            returnData = accessServiceBean.service(tTradeInfo);
            returnData.addData("TransType", tTradeInfo.getData("TransType"));
        } catch (Exception e) {
            e.printStackTrace();
            returnData.addData("return", "发生严重错误:" + e.getMessage());
            returnData.addData("returnFlag", "0");
            returnData.addData("log", "发生严重错误:" + e.getMessage());
            logger.error("发生严重错误:" + e.getMessage());
            MDC.put("status","error");
            return mCommonTrans.getReturnTrans(returnData,tTradeInfo);
        }
        String errorStr = (String) returnData.getData("return");
        String log = (String) returnData.getData("log");

        if (null != errorStr && !"".equals(errorStr)) {
            logger.warn("交易流水号：" + serialNo + "。" + ERROR_INFO + log);
            returnData.addData("returnFlag", "0");
            MDC.put("status", "done_deny");
            return mCommonTrans.getReturnTrans(returnData,tTradeInfo);
        }

        if (returnData.getErrorList().size() > 0) {
            logger.error("交易流水号：" + serialNo + "。" + ERROR_INFO + returnData.getErrorList().get(0));
            returnData.addData("return",ERROR_DESC);
            returnData.addData("returnFlag", "0");
            MDC.put("status", "error");
            return mCommonTrans.getReturnTrans(returnData,tTradeInfo);
        }

        /*将标准报文转化为xxx所需要的报文，在return的过程中，会将TranData先转化为xml字符串，然后使用xsl进行转化*/
        return mCommonTrans.getReturnTrans(returnData,tTradeInfo);
    }

    /**
     * 新的转化引擎
     *
     */
    public String xslTransform(String strXml, Resource xslFile) {
        Serializer out = null;
        StringWriter writer = new StringWriter(1024);
        XsltTransformer trans = null;
        StringReader xmlReader = null;
        try {
            XsltExecutable exp = xslCache.get(xslFile.getURL().getPath());
            if (exp == null) {
                StreamSource xslStream = new StreamSource(xslFile.getInputStream());
                //这个是设置导入模板的初始目录

                XsltCompiler comp = proc.newXsltCompiler();
                exp = comp.compile(xslStream);
                xslCache.put(xslFile.getURL().getPath(), exp);
            }
            xmlReader = new StringReader(strXml);
            StreamSource xmlStream = new StreamSource(xmlReader);
            if (logger.isDebugEnabled()) {
                logger.debug("解析xml:" + strXml);
            }
            out = proc.newSerializer();
            out.setOutputWriter(writer);
            trans = exp.load();
            trans.setSource(xmlStream);
            trans.setDestination(out);
            trans.transform();
            String result = writer.toString();
            return result;
        } catch (FileNotFoundException sae) {
            logger.warn("未找到转换模板，非互联网核心交易！" + sae.getMessage(), sae);
            logger.warn(ExceptionUtils.exceptionToString(sae));
            try {
                xmlReader.close();
                out.close();
                writer.close();
                trans.close();
            } catch (Exception e) {
            }
        } catch (Exception sae) {
            logger.error("XML报文转化失败！" + sae.getMessage(), sae);
            logger.error(ExceptionUtils.exceptionToString(sae));
            MDC.put("status","error");
            try {
                xmlReader.close();
                out.close();
                writer.close();
                trans.close();
            } catch (Exception e) {
            }
        } finally {
            try {
                out.close();
                writer.close();
                trans.close();
            } catch (Exception e) {
            }
        }
        return null;
    }


    /**
     * 获取渠道名称
     */
    public String getAccessName() {
        return accessName;
    }

    /**
     * 设置去打名称
     */
    @Override
    public void setAccessName(String accessName) {
        this.accessName = accessName;
    }


    /**
     * 报文头处理
     *
     */
    @Override
    public Head headerTrans(String msg) {
        if (logger.isDebugEnabled()) {
            logger.debug("开始进行报文头处理，原始报文为：" + msg);
        }
        Resource xslFile = null;
        String stdHead = xslTransform(msg, xslFile);
        Head head = XmlUtil.getBean(stdHead, Head.class);
        return head;
    }

    /**
     * 进行报文头转换
     *
     */
    @Override
    public TranData bodyTrans(String msg, Resource xslFile) {

        String stdMsg = xslTransform(msg, xslFile);
        System.out.println(stdMsg);
        TranData tranData = null;
        try {
            tranData = XmlUtil.getBean(stdMsg, TranData.class);
        } catch (Exception e) {
            tranData = new TranData();
            e.printStackTrace();
            logger.warn("标准报文转换TranData失败：" + ExceptionUtils.exceptionToString(e));
            MDC.put("status","error");
        }
        return tranData;
    }


    @Override
    public String returnTrans(TranData msg, Resource xslFile) {
        String strMsg = XmlUtil.getString(msg);
        System.out.println("返回标准报文：" + strMsg);
        String returnBankStr = xslTransform(strMsg, xslFile);
        return returnBankStr;
    }

    /**
     * 封装返回error.xsl
     *
     */
    public String returnError(String result) {
        TranData tranData = new TranData();
        Head head = new Head();
        head.setDesc(result);
        tranData.setHead(head);

        ClassPathResource resource = new ClassPathResource(BASE_PACKAGE + accessName + "/xsl/Error.xsl");

        String returnTrans = returnTrans(tranData, resource);

        return returnTrans;
    }
}
