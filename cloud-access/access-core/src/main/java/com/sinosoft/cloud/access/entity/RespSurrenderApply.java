package com.sinosoft.cloud.access.entity;

/**
 * 退保申请POS089 返回
 */
public class RespSurrenderApply {
    //险种名称
    private String RiskName;
    //保单号
    private String ContNo;
    //退保金额
    private String OccurBala;
    //保单生效日
    private String ValidDate;

    public String getRiskName() {
        return RiskName;
    }

    public void setRiskName(String riskName) {
        RiskName = riskName;
    }

    public String getContNo() {
        return ContNo;
    }

    public void setContNo(String contNo) {
        ContNo = contNo;
    }

    public String getOccurBala() {
        return OccurBala;
    }

    public void setOccurBala(String occurBala) {
        OccurBala = occurBala;
    }

    public String getValidDate() {
        return ValidDate;
    }

    public void setValidDate(String validDate) {
        ValidDate = validDate;
    }

    @Override
    public String toString() {
        return "RespSurrenderApply{" +
                "RiskName='" + RiskName + '\'' +
                ", ContNo='" + ContNo + '\'' +
                ", OccurBala='" + OccurBala + '\'' +
                ", ValidDate='" + ValidDate + '\'' +
                '}';
    }
}
