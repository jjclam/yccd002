package com.sinosoft.cloud.access.entity;

/**
 * @Description
 * @Author:zhaoshulei
 * @Date:2018/4/10
 */
public class LAAgent1 {
    private String ManageCom;
    private String AgentCom;
    private String OutAgentCode;
    private String AgentName;
    private String IDType;
    private String IDNo;
    private String QualType;
    private String QualNo;
    private String QualStartDate;
    private String QualEndDate;
    private String AgentState;
    private String BranchType;
    private String Operator;
    private String MakeDate;
    private String MakeTime;
    private String ModifyDate;
    private String ModifyTime;
    private String State1;
    private String Phone;
    private String Mobile;
    private String SerialNo;

    public String getManageCom() {
        return ManageCom;
    }

    public void setManageCom(String manageCom) {
        ManageCom = manageCom;
    }

    public String getAgentCom() {
        return AgentCom;
    }

    public void setAgentCom(String agentCom) {
        AgentCom = agentCom;
    }

    public String getOutAgentCode() {
        return OutAgentCode;
    }

    public void setOutAgentCode(String outAgentCode) {
        OutAgentCode = outAgentCode;
    }

    public String getAgentName() {
        return AgentName;
    }

    public void setAgentName(String agentName) {
        AgentName = agentName;
    }

    public String getIDType() {
        return IDType;
    }

    public void setIDType(String IDType) {
        this.IDType = IDType;
    }

    public String getIDNo() {
        return IDNo;
    }

    public void setIDNo(String IDNo) {
        this.IDNo = IDNo;
    }

    public String getQualType() {
        return QualType;
    }

    public void setQualType(String qualType) {
        QualType = qualType;
    }

    public String getQualNo() {
        return QualNo;
    }

    public void setQualNo(String qualNo) {
        QualNo = qualNo;
    }

    public String getQualStartDate() {
        return QualStartDate;
    }

    public void setQualStartDate(String qualStartDate) {
        QualStartDate = qualStartDate;
    }

    public String getQualEndDate() {
        return QualEndDate;
    }

    public void setQualEndDate(String qualEndDate) {
        QualEndDate = qualEndDate;
    }

    public String getAgentState() {
        return AgentState;
    }

    public void setAgentState(String agentState) {
        AgentState = agentState;
    }

    public String getBranchType() {
        return BranchType;
    }

    public void setBranchType(String branchType) {
        BranchType = branchType;
    }

    public String getOperator() {
        return Operator;
    }

    public void setOperator(String operator) {
        Operator = operator;
    }

    public String getMakeDate() {
        return MakeDate;
    }

    public void setMakeDate(String makeDate) {
        MakeDate = makeDate;
    }

    public String getMakeTime() {
        return MakeTime;
    }

    public void setMakeTime(String makeTime) {
        MakeTime = makeTime;
    }

    public String getModifyDate() {
        return ModifyDate;
    }

    public void setModifyDate(String modifyDate) {
        ModifyDate = modifyDate;
    }

    public String getModifyTime() {
        return ModifyTime;
    }

    public void setModifyTime(String modifyTime) {
        ModifyTime = modifyTime;
    }

    public String getState1() {
        return State1;
    }

    public void setState1(String state1) {
        State1 = state1;
    }

    public String getPhone() {
        return Phone;
    }

    public void setPhone(String phone) {
        Phone = phone;
    }

    public String getMobile() {
        return Mobile;
    }

    public void setMobile(String mobile) {
        Mobile = mobile;
    }

    public String getSerialNo() {
        return SerialNo;
    }

    public void setSerialNo(String serialNo) {
        SerialNo = serialNo;
    }

    @Override
    public String toString() {
        return "LAAgent1{" +
                "ManageCom='" + ManageCom + '\'' +
                ", AgentCom='" + AgentCom + '\'' +
                ", OutAgentCode='" + OutAgentCode + '\'' +
                ", AgentName='" + AgentName + '\'' +
                ", IDType='" + IDType + '\'' +
                ", IDNo='" + IDNo + '\'' +
                ", QualType='" + QualType + '\'' +
                ", QualNo='" + QualNo + '\'' +
                ", QualStartDate='" + QualStartDate + '\'' +
                ", QualEndDate='" + QualEndDate + '\'' +
                ", AgentState='" + AgentState + '\'' +
                ", BranchType='" + BranchType + '\'' +
                ", Operator='" + Operator + '\'' +
                ", MakeDate='" + MakeDate + '\'' +
                ", MakeTime='" + MakeTime + '\'' +
                ", ModifyDate='" + ModifyDate + '\'' +
                ", ModifyTime='" + ModifyTime + '\'' +
                ", State1='" + State1 + '\'' +
                ", Phone='" + Phone + '\'' +
                ", Mobile='" + Mobile + '\'' +
                ", SerialNo='" + SerialNo + '\'' +
                '}';
    }
}
