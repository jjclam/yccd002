package com.sinosoft.cloud.access.entity;

/**
 * abc-cloud-access
 *
 * @author lizhen
 * @create 2019/3/4
 */
/**
 * 退保申请 微信、官网  pos126
 */
public class RespWebSiteSurrenderApply {
    //xxx订单号
    private String ABCOrderId;
    //xxx退保订单号
    private String ABCRefundId;
    // 保险公司退保单号
    private String RefundProposalNo;
    //退保金额
    private String WithdrawMoney;
    //实际可支取金额
    private String AvailableMoney;
    // 是否成功
    private String IsSuccess;
    //审核失败原因
    private String FailReason;

    public String getABCOrderId() {
        return ABCOrderId;
    }

    public void setABCOrderId(String ABCOrderId) {
        this.ABCOrderId = ABCOrderId;
    }

    public String getABCRefundId() {
        return ABCRefundId;
    }

    public void setABCRefundId(String ABCRefundId) {
        this.ABCRefundId = ABCRefundId;
    }

    public String getRefundProposalNo() {
        return RefundProposalNo;
    }

    public void setRefundProposalNo(String refundProposalNo) {
        RefundProposalNo = refundProposalNo;
    }

    public String getWithdrawMoney() {
        return WithdrawMoney;
    }

    public void setWithdrawMoney(String withdrawMoney) {
        WithdrawMoney = withdrawMoney;
    }

    public String getAvailableMoney() {
        return AvailableMoney;
    }

    public void setAvailableMoney(String availableMoney) {
        AvailableMoney = availableMoney;
    }

    public String getIsSuccess() {
        return IsSuccess;
    }

    public void setIsSuccess(String isSuccess) {
        IsSuccess = isSuccess;
    }

    public String getFailReason() {
        return FailReason;
    }

    public void setFailReason(String failReason) {
        FailReason = failReason;
    }

    @Override
    public String toString() {
        return "RespWebSiteSurrenderApply{" +
                "ABCOrderId='" + ABCOrderId + '\'' +
                ", ABCRefundId='" + ABCRefundId + '\'' +
                ", RefundProposalNo='" + RefundProposalNo + '\'' +
                ", WithdrawMoney='" + WithdrawMoney + '\'' +
                ", AvailableMoney='" + AvailableMoney + '\'' +
                ", IsSuccess='" + IsSuccess + '\'' +
                ", FailReason='" + FailReason + '\'' +
                '}';
    }
}