package com.sinosoft.cloud.access.entity;

import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.List;

/**
 * @Description
 * @Author:zhaoshulei
 * @Date:2018/4/13
 */
public class Reserves {
    @XStreamImplicit(itemFieldName="Reserve")
    private List<Reserve> Reserves;

    public List<Reserve> getReserves() {
        return Reserves;
    }

    public void setReserves(List<Reserve> reserves) {
        Reserves = reserves;
    }

    @Override
    public String toString() {
        return "Reserves{" +
                "Reserves=" + Reserves +
                '}';
    }
}
