package com.sinosoft.cloud.access.entity;

import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.List;

/**
 * @Description
 * @Author:zhaoshulei
 * @Date:2018/4/10
 */
public class LCSpecs {

    @XStreamImplicit(itemFieldName="LCSpec")
    private List<LCSpec> LCSpecs;

    public List<LCSpec> getLCSpecs() {
        return LCSpecs;
    }

    public void setLCSpecs(List<LCSpec> LCSpecs) {
        this.LCSpecs = LCSpecs;
    }

    @Override
    public String toString() {
        return "LCSpecs{" +
                "LCSpecs=" + LCSpecs +
                '}';
    }
}
