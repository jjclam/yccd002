package com.sinosoft.cloud.access.net;

import com.sinosoft.cloud.common.SpringContextUtils;
import io.netty.bootstrap.Bootstrap;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.timeout.ReadTimeoutHandler;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.List;
import java.util.concurrent.TimeUnit;


/**
 * 连接Socket服务器
 */
public class SocketClient {

    protected final Log logger = LogFactory.getLog(getClass());

    AccessControl accessControl = SpringContextUtils.getBeanByClass(AccessControl.class);

    private String ip;
    private String port;
    private Channel ch;
    private Bootstrap bootstrap;
    private List<ChannelHandler> socketHandlers;

    private DecoderFactory decoderFactory = DecoderFactory.getInstance();

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getPort() {
        return port;
    }

    public void setPort(String port) {
        this.port = port;
    }


    public List<ChannelHandler> getSocketHandlers() {
        return socketHandlers;
    }

    public void setSocketHandlers(List<ChannelHandler> socketHandlers) {
        this.socketHandlers = socketHandlers;
    }

    public void connect(String host, String port) throws Exception {
        NioEventLoopGroup group = new NioEventLoopGroup();
        try {
            Bootstrap b = new Bootstrap();
            b.group(group).channel(NioSocketChannel.class)
                    .option(ChannelOption.TCP_NODELAY, true)
                    .handler(new ChannelInitializer<SocketChannel>() {
                        @Override
                        protected void initChannel(SocketChannel ch) throws Exception {
//                            ch.config().setRecvByteBufAllocator(new FixedRecvByteBufAllocator(2048000)); //set  buf size
                            if (socketHandlers != null) {
                                for (ChannelHandler socketHandler : socketHandlers) {
                                    ch.pipeline().addLast(new ReadTimeoutHandler(accessControl.getCipReturnTimeOut(), TimeUnit.MILLISECONDS));
                                    ch.pipeline().addLast(socketHandler);
                                }
                            }

                        }
                    });

            ChannelFuture f = b.connect(host, Integer.parseInt(port)).addListener(new ChannelFutureListener() {
                @Override
                public void operationComplete(ChannelFuture future) throws Exception {

                    if (!future.isSuccess()){

                        for (int i = 0; i < socketHandlers.size(); i++) {
                            if (socketHandlers.get(i) instanceof RouterHandler) {
                                RouterHandler routerHandler = (RouterHandler) socketHandlers.get(i);
                                String errorStr = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><ABCB2I><Header><RetCode>009999</RetCode><RetMsg>系统繁忙，请稍后重试</RetMsg><SerialNo></SerialNo><InsuSerial/><TransDate></TransDate><TranTime></TranTime><BankCode></BankCode><CorpNo></CorpNo><TransCode></TransCode></Header></ABCB2I>";

                                if (routerHandler.needCrypt()) {
                                    errorStr = routerHandler.encrypt(errorStr.trim());
                                }

                                Coder coder = decoderFactory.getDecoder(routerHandler.getAccessName());
                                if (coder != null) {
                                    errorStr = coder.encoder(errorStr);
                                }

                                routerHandler.receive(errorStr);
                                logger.error("连接xxx异常！无法获取连接！"+System.getProperty("line.separator")
                                        +"解密后的报文："+routerHandler.getOrgXml()+System.getProperty("line.separator")
                                        +"返回的报文："+errorStr);
                            }
                        }
                    }
                }
            }).sync();
//            ChannelFuture f = b.connect(host, Integer.parseInt(port)).sync();
            f.channel().closeFuture().sync();
        } finally {
            group.shutdownGracefully();
        }
    }
}
