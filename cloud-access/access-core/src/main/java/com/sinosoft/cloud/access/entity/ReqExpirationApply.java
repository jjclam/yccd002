package com.sinosoft.cloud.access.entity;

import com.alibaba.fastjson.annotation.JSONField;

/**
 * 4004保单生存金领取(AGSQ)申请、4004保单生存金领取(SQ)申请、满期申请POS044、POS045、POS072.
 */
public class ReqExpirationApply {
    //保全项目编码
    private String EdorType;
    //保单号
    private String ContNo;
    //保单印刷号
    private String PrintCode;
    //申请人姓名
    private String ClientName;
    //账（卡）号
    private String PayAcc;
    //领取金额/万能账户余额/getMoney/账户价值/追加保费金额/退保金额
    private String GetMoney;
    //地区代码
    private String ZoneNo;
    //网点编码
    private String BranchNo;
    //柜员代码
    private String TellerNo;
    //申请人信息
    private EdorAppInfo EdorAppInfo;
    //领取人
    private PersonInfo PersonInfo;
    //xxx
    private BankInfo BankInfo;
    //收付费方式
    private String PayMode;

    public String getEdorType() {
        return EdorType;
    }

    public void setEdorType(String edorType) {
        EdorType = edorType;
    }

    public String getContNo() {
        return ContNo;
    }

    public void setContNo(String contNo) {
        ContNo = contNo;
    }

    public String getPrintCode() {
        return PrintCode;
    }

    public void setPrintCode(String printCode) {
        PrintCode = printCode;
    }

    public String getClientName() {
        return ClientName;
    }

    public void setClientName(String clientName) {
        ClientName = clientName;
    }

    public String getPayAcc() {
        return PayAcc;
    }

    public void setPayAcc(String payAcc) {
        PayAcc = payAcc;
    }

    public String getGetMoney() {
        return GetMoney;
    }

    public void setGetMoney(String getMoney) {
        GetMoney = getMoney;
    }

    public String getZoneNo() {
        return ZoneNo;
    }

    public void setZoneNo(String zoneNo) {
        ZoneNo = zoneNo;
    }

    public String getBranchNo() {
        return BranchNo;
    }

    public void setBranchNo(String branchNo) {
        BranchNo = branchNo;
    }

    public String getTellerNo() {
        return TellerNo;
    }

    public void setTellerNo(String tellerNo) {
        TellerNo = tellerNo;
    }

    public com.sinosoft.cloud.access.entity.EdorAppInfo getEdorAppInfo() {
        return EdorAppInfo;
    }

    public void setEdorAppInfo(com.sinosoft.cloud.access.entity.EdorAppInfo edorAppInfo) {
        EdorAppInfo = edorAppInfo;
    }

    public com.sinosoft.cloud.access.entity.PersonInfo getPersonInfo() {
        return PersonInfo;
    }

    public void setPersonInfo(com.sinosoft.cloud.access.entity.PersonInfo personInfo) {
        PersonInfo = personInfo;
    }

    public com.sinosoft.cloud.access.entity.BankInfo getBankInfo() {
        return BankInfo;
    }

    public void setBankInfo(com.sinosoft.cloud.access.entity.BankInfo bankInfo) {
        BankInfo = bankInfo;
    }

    public String getPayMode() {
        return PayMode;
    }

    public void setPayMode(String payMode) {
        PayMode = payMode;
    }

    @Override
    public String toString() {
        return "ReqExpirationApply{" +
                "EdorType='" + EdorType + '\'' +
                ", ContNo='" + ContNo + '\'' +
                ", PrintCode='" + PrintCode + '\'' +
                ", ClientName='" + ClientName + '\'' +
                ", PayAcc='" + PayAcc + '\'' +
                ", GetMoney='" + GetMoney + '\'' +
                ", ZoneNo='" + ZoneNo + '\'' +
                ", BranchNo='" + BranchNo + '\'' +
                ", TellerNo='" + TellerNo + '\'' +
                ", EdorAppInfo=" + EdorAppInfo +
                ", PersonInfo=" + PersonInfo +
                ", BankInfo=" + BankInfo +
                ", PayMode='" + PayMode + '\'' +
                '}';
    }
}
