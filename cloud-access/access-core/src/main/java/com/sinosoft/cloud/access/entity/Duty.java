package com.sinosoft.cloud.access.entity;

import com.thoughtworks.xstream.annotations.XStreamAlias;


public class Duty {
    private String riskCode;

    private String riskVersion;

    private String dutyCode;

    private String dutyName;

    private String amnt;

    private String prem;

    private String floatRate;

    private String valiDate;

    private String endDate;

    private String payIntv;

    private String payEndYearFlag;

    private String payEndYear;

    private String insuYearFlag;

    private String insuYear;

    private String rnewFlag;

    private String autoPayFlag;
    private String calMode;

    public String getCalMode() {
        return calMode;
    }

    public void setCalMode(String calMode) {
        this.calMode = calMode;
    }

    public String getRiskCode() {
        return riskCode;
    }

    public void setRiskCode(String riskCode) {
        this.riskCode = riskCode;
    }

    public String getRiskVersion() {
        return riskVersion;
    }

    public void setRiskVersion(String riskVersion) {
        this.riskVersion = riskVersion;
    }

    public String getDutyCode() {
        return dutyCode;
    }

    public void setDutyCode(String dutyCode) {
        this.dutyCode = dutyCode;
    }

    public String getDutyName() {
        return dutyName;
    }

    public void setDutyName(String dutyName) {
        this.dutyName = dutyName;
    }

    public String getAmnt() {
        return amnt;
    }

    public void setAmnt(String amnt) {
        this.amnt = amnt;
    }

    public String getPrem() {
        return prem;
    }

    public void setPrem(String prem) {
        this.prem = prem;
    }

    public String getFloatRate() {
        return floatRate;
    }

    public void setFloatRate(String floatRate) {
        this.floatRate = floatRate;
    }

    public String getValiDate() {
        return valiDate;
    }

    public void setValiDate(String valiDate) {
        this.valiDate = valiDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getPayIntv() {
        return payIntv;
    }

    public void setPayIntv(String payIntv) {
        this.payIntv = payIntv;
    }

    public String getPayEndYearFlag() {
        return payEndYearFlag;
    }

    public void setPayEndYearFlag(String payEndYearFlag) {
        this.payEndYearFlag = payEndYearFlag;
    }

    public String getPayEndYear() {
        return payEndYear;
    }

    public void setPayEndYear(String payEndYear) {
        this.payEndYear = payEndYear;
    }

    public String getInsuYearFlag() {
        return insuYearFlag;
    }

    public void setInsuYearFlag(String insuYearFlag) {
        this.insuYearFlag = insuYearFlag;
    }

    public String getInsuYear() {
        return insuYear;
    }

    public void setInsuYear(String insuYear) {
        this.insuYear = insuYear;
    }

    public String getRnewFlag() {
        return rnewFlag;
    }

    public void setRnewFlag(String rnewFlag) {
        this.rnewFlag = rnewFlag;
    }

    public String getAutoPayFlag() {
        return autoPayFlag;
    }

    public void setAutoPayFlag(String autoPayFlag) {
        this.autoPayFlag = autoPayFlag;
    }

}
