package com.sinosoft.cloud.access.entity;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("TranData")
public class TranData {
    @XStreamAlias("Body")
    private Body body;
    @XStreamAlias("Head")
    private Head head;
    @XStreamAlias("AliHead")
    private AliHead AliHead;
    @XStreamAlias("ShuiDiHead")
    private ShuiDiHead shuiDiHead;

    public Body getBody() {
        return body;
    }

    public void setBody(Body body) {
        this.body = body;
    }

    public Head getHead() {
        return head;
    }

    public void setHead(Head head) {
        this.head = head;
    }

    public com.sinosoft.cloud.access.entity.AliHead getAliHead() {
        return AliHead;
    }

    public void setAliHead(com.sinosoft.cloud.access.entity.AliHead aliHead) {
        AliHead = aliHead;
    }

    public ShuiDiHead getShuiDiHead() {
        return shuiDiHead;
    }

    public void setShuiDiHead(ShuiDiHead shuiDiHead) {
        this.shuiDiHead = shuiDiHead;
    }


    @Override
    public String toString() {
        return "TranData{" +
                "body=" + body +
                ", head=" + head +
                ", AliHead=" + AliHead +
                ", shuiDiHead=" + shuiDiHead +
                '}';
    }
}
