package com.sinosoft.cloud.access.entity;

/**
 * @Author:lizhen
 * @Description:
 * @Date:Created in 11:54 2018/4/11
 * @Modified By:
 */
public class XQJFQuery {
    private String AppNo;
    private String PolicyNo;
    private String Riskcode;
    private String ProdCode;
    private String PolicyApplyNo;
    private String IDKind;
    private String IDCode;
    private String Name;
    private String DuePeriod;
    private String PayAcc;
    private String PayAmt;
    private String PolicyPwd;
    private String AccType;
    private String ClientName;
    private String OldAcc;
    private String NewAcc;

    public XQJFQuery() {
    }

    public String getPolicyPwd() {
        return this.PolicyPwd;
    }

    public void setPolicyPwd(String policyPwd) {
        this.PolicyPwd = policyPwd;
    }

    public String getAccType() {
        return this.AccType;
    }

    public void setAccType(String accType) {
        this.AccType = accType;
    }

    public String getClientName() {
        return this.ClientName;
    }

    public void setClientName(String clientName) {
        this.ClientName = clientName;
    }

    public String getOldAcc() {
        return this.OldAcc;
    }

    public void setOldAcc(String oldAcc) {
        this.OldAcc = oldAcc;
    }

    public String getNewAcc() {
        return this.NewAcc;
    }

    public void setNewAcc(String newAcc) {
        this.NewAcc = newAcc;
    }

    public String getAppNo() {
        return this.AppNo;
    }

    public void setAppNo(String appNo) {
        this.AppNo = appNo;
    }

    public String getPolicyNo() {
        return this.PolicyNo;
    }

    public void setPolicyNo(String policyNo) {
        this.PolicyNo = policyNo;
    }

    public String getRiskcode() {
        return this.Riskcode;
    }

    public void setRiskcode(String riskcode) {
        this.Riskcode = riskcode;
    }

    public String getProdCode() {
        return this.ProdCode;
    }

    public void setProdCode(String prodCode) {
        this.ProdCode = prodCode;
    }

    public String getPolicyApplyNo() {
        return this.PolicyApplyNo;
    }

    public void setPolicyApplyNo(String policyApplyNo) {
        this.PolicyApplyNo = policyApplyNo;
    }

    public String getIDKind() {
        return this.IDKind;
    }

    public void setIDKind(String IDKind) {
        this.IDKind = IDKind;
    }

    public String getIDCode() {
        return this.IDCode;
    }

    public void setIDCode(String IDCode) {
        this.IDCode = IDCode;
    }

    public String getName() {
        return this.Name;
    }

    public void setName(String name) {
        this.Name = name;
    }

    public String getDuePeriod() {
        return this.DuePeriod;
    }

    public void setDuePeriod(String duePeriod) {
        this.DuePeriod = duePeriod;
    }

    public String getPayAcc() {
        return this.PayAcc;
    }

    public void setPayAcc(String payAcc) {
        this.PayAcc = payAcc;
    }

    public String getPayAmt() {
        return this.PayAmt;
    }

    public void setPayAmt(String payAmt) {
        this.PayAmt = payAmt;
    }

    @Override
    public String toString() {
        return "XQJFQueryPojo{AppNo=\'" + this.AppNo + '\'' + ", PolicyNo=\'" + this.PolicyNo + '\'' + ", Riskcode=\'" + this.Riskcode + '\'' + ", ProdCode=\'" + this.ProdCode + '\'' + ", PolicyApplyNo=\'" + this.PolicyApplyNo + '\'' + ", IDKind=\'" + this.IDKind + '\'' + ", IDCode=\'" + this.IDCode + '\'' + ", Name=\'" + this.Name + '\'' + ", DuePeriod=\'" + this.DuePeriod + '\'' + ", PayAcc=\'" + this.PayAcc + '\'' + ", PayAmt=\'" + this.PayAmt + '\'' + '}';
    }

    public String getV(String s) {
        return null;
    }

    public String getV(int i) {
        return null;
    }

    public int getFieldType(String s) {
        return 0;
    }

    public int getFieldType(int i) {
        return 0;
    }

    public int getFieldCount() {
        return 0;
    }

    public int getFieldIndex(String s) {
        return 0;
    }

    public String getFieldName(int i) {
        return null;
    }

    public boolean setV(String s, String s1) {
        return false;
    }

}