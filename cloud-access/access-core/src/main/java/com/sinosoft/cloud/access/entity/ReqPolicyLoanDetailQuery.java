package com.sinosoft.cloud.access.entity;

/**
 * @Author:lizhen
 * @Description:
 * @Date:Created in 15:52 2018/11/16
 * @Modified By:
 */

/**
 * 4003保单贷款明细查询POS074 pos125请求
 */
public class ReqPolicyLoanDetailQuery {
    //保全项目编码
    private String EdorType;
    //保单号
    private String ContNo;
    //批单号
    private String EdorNo;

    public String getEdorType() {
        return EdorType;
    }

    public void setEdorType(String edorType) {
        EdorType = edorType;
    }

    public String getContNo() {
        return ContNo;
    }

    public void setContNo(String contNo) {
        ContNo = contNo;
    }

    public String getEdorNo() {
        return EdorNo;
    }

    public void setEdorNo(String edorNo) {
        EdorNo = edorNo;
    }

    @Override
    public String toString() {
        return "ReqPolicyLoanDetailQuery{" +
                "EdorType='" + EdorType + '\'' +
                ", ContNo='" + ContNo + '\'' +
                ", EdorNo='" + EdorNo + '\'' +
                '}';
    }
}