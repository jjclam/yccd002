package com.sinosoft.cloud.access.entity;
/**
 * 白名单提交NB005.
 */
public class RespWhilteApply {
    //姓名
    private String CustomerName;
    //证件号码
    private String CustomerIDNo;
    //客户是否已存在
    private String DealResult;


    public String getCustomerName() {
        return CustomerName;
    }

    public void setCustomerName(String customerName) {
        CustomerName = customerName;
    }

    public String getCustomerIDNo() {
        return CustomerIDNo;
    }

    public void setCustomerIDNo(String customerIDNo) {
        CustomerIDNo = customerIDNo;
    }

    public String getDealResult() {
        return DealResult;
    }

    public void setDealResult(String dealResult) {
        DealResult = dealResult;
    }

    @Override
    public String toString() {
        return "RespWhilteApply{" +
                "CustomerName='" + CustomerName + '\'' +
                ", CustomerIDNo='" + CustomerIDNo + '\'' +
                ", DealResult='" + DealResult + '\'' +
                '}';
    }


}
