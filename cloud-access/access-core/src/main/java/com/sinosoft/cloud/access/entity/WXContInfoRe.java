package com.sinosoft.cloud.access.entity;

/**
 * @program: abc-cloud-access
 * @description:
 * @author: BaoYongmeng
 * @create: 2018-12-24 17:04
 **/
public class WXContInfoRe {
    //姓名
    private String AppntName;
    //省
    private String Province;
    //市
    private String City;
    private String Powiat;
    //街道
    private String Street;
    //通讯邮编
    private String ZipCode;
    private String Village;
    //手机号
    private String MobilePhone;

    public void setAppntName(String appntName) {
        AppntName = appntName;
    }

    public void setProvince(String province) {
        Province = province;
    }

    public void setCity(String city) {
        City = city;
    }

    public void setPowiat(String powiat) {
        Powiat = powiat;
    }

    public void setStreet(String street) {
        Street = street;
    }

    public void setZipCode(String zipCode) {
        ZipCode = zipCode;
    }

    public void setVillage(String village) {
        Village = village;
    }

    public void setMobilePhone(String mobilePhone) {
        MobilePhone = mobilePhone;
    }

    public String getAppntName() {
        return AppntName;
    }

    public String getProvince() {
        return Province;
    }

    public String getCity() {
        return City;
    }

    public String getPowiat() {
        return Powiat;
    }

    public String getStreet() {
        return Street;
    }

    public String getZipCode() {
        return ZipCode;
    }

    public String getVillage() {
        return Village;
    }

    public String getMobilePhone() {
        return MobilePhone;
    }

    @Override
    public String toString() {
        return "WXContInfoRe{" +
                "AppntName='" + AppntName + '\'' +
                ", Province='" + Province + '\'' +
                ", City='" + City + '\'' +
                ", Powiat='" + Powiat + '\'' +
                ", Street='" + Street + '\'' +
                ", ZipCode='" + ZipCode + '\'' +
                ", Village='" + Village + '\'' +
                ", MobilePhone='" + MobilePhone + '\'' +
                '}';
    }
}
