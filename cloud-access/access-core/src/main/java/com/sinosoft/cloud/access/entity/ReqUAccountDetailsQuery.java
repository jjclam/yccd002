package com.sinosoft.cloud.access.entity;

/**
 * 4003保单万能账户追加保费明细查询POS027
 */
public class ReqUAccountDetailsQuery {


    //保全项目编码
    private String EdorType;
    //保单号
    private String ContNo;
    //保全受理号
    private String EdorAcceptNo;
    public String getEdorType() {
        return EdorType;
    }

    public void setEdorType(String edorType) {
        EdorType = edorType;
    }

    public String getContNo() {
        return ContNo;
    }

    public void setContNo(String contNo) {
        ContNo = contNo;
    }

    public String getEdorAcceptNo() {
        return EdorAcceptNo;
    }

    public void setEdorAcceptNo(String edorAcceptNo) {
        EdorAcceptNo = edorAcceptNo;
    }

    @Override
    public String toString() {
        return "ReqUAccountDetailsQuery{" +
                "EdorType='" + EdorType + '\'' +
                ", ContNo='" + ContNo + '\'' +
                ", EdorAcceptNo='" + EdorAcceptNo + '\'' +
                '}';
    }
}
