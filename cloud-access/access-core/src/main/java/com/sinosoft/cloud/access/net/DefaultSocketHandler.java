package com.sinosoft.cloud.access.net;

import com.sinosoft.cloud.access.transformer.XsltTrans;

public class DefaultSocketHandler extends SocketHandler {


    @Override
    public String receive(String msg) {
        if (logger.isInfoEnabled()) {
            logger.info("进入DefaultSocketHandler.receive:" + msg);
        }
        String accessName = getAccessName();

        XsltTrans xsltTrans = new XsltTrans();
        xsltTrans.setAccessName(accessName);
        String result = xsltTrans.transform(msg);
//        if (xsltTrans.getNeedRouter()) {
//            return NEED_ROUTER;
//        }

        return result;
    }

    @Override
    public String send() {
        return null;
    }


}
