package com.sinosoft.cloud.access.entity;

/**
 * @program: abc-cloud-access
 * @description: 4011-手续费与追加保费POS117.
 * @author: BaoYongmeng
 * @create: 2018-12-24 16:21
 **/
public class ReqHandChargeAddPrem {
    //保全项目编码
    private String EdorType;
    //保单号
    private String ContNo;
    //领取金额/万能账户余额/getMoney/账户价值/追加保费金额/退保金额
    private String GetMoney;

    public void setEdorType(String edorType) {
        EdorType = edorType;
    }

    public void setContNo(String contNo) {
        ContNo = contNo;
    }

    public void setGetMoney(String getMoney) {
        GetMoney = getMoney;
    }

    public String getEdorType() {
        return EdorType;
    }

    public String getContNo() {
        return ContNo;
    }

    public String getGetMoney() {
        return GetMoney;
    }

    @Override
    public String toString() {
        return "ReqHandChargeAddPrem{" +
                "EdorType='" + EdorType + '\'' +
                ", ContNo='" + ContNo + '\'' +
                ", GetMoney='" + GetMoney + '\'' +
                '}';
    }
}
