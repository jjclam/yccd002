package com.sinosoft.cloud.access.entity;

import com.sinosoft.lis.pos.entity.IdentityVerifyInfoPojo;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.List;

/**
 * @Author:lizhen
 * @Description:
 * @Date:Created in 15:39 2018/11/15
 * @Modified By:
 */
public class IdentityVerifyInfos {
    @XStreamImplicit(itemFieldName="IdentityVerifyInfo")
    private List<IdentityVerifyInfo> IdentityVerifyInfo;

    public List<IdentityVerifyInfo> getIdentityVerifyInfo() {
        return IdentityVerifyInfo;
    }

    public void setIdentityVerifyInfo(List<IdentityVerifyInfo> identityVerifyInfo) {
        IdentityVerifyInfo = identityVerifyInfo;
    }

    @Override
    public String toString() {
        return "IdentityVerifyInfos{" +
                "IdentityVerifyInfo=" + IdentityVerifyInfo +
                '}';
    }
}