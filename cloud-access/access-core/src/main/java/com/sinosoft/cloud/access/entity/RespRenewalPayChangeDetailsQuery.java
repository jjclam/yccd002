package com.sinosoft.cloud.access.entity;
/**
 *4002查询保单列表POS062,POS113
 */
public class RespRenewalPayChangeDetailsQuery {

    //保全项目编码
    private String EdorType;
    //保单号
    private String ContNo;
    //投保人姓名
    private String AppntName;
    //总保费
    private String SumPrem;
    //生效日期
    private String ValiDate;
    //缴费方式
    private String PayMode;
    //账户名称
    private String BankName;
    //开户xxx
    private String BankAccName;
    //xxx账号
    private String BankAccNo;
    //开户xxx
    private String BankCode;
    //主险名称
    private String MainRiskName;
    //缴费形式
    private String OldPayMode;
    //账户名称
    private String OldBankName;
    //开户xxx
    private String OldBankAccNo;
    //xxx账号
    private String OldBankAccName;

    public String getOldPayMode() {
        return OldPayMode;
    }

    public String getOldBankName() {
        return OldBankName;
    }

    public String getOldBankAccNo() {
        return OldBankAccNo;
    }

    public String getOldBankAccName() {
        return OldBankAccName;
    }

    public String getEdorType() {
        return EdorType;
    }

    public void setEdorType(String edorType) {
        EdorType = edorType;
    }

    public String getContNo() {
        return ContNo;
    }

    public void setContNo(String contNo) {
        ContNo = contNo;
    }

    public String getAppntName() {
        return AppntName;
    }

    public void setAppntName(String appntName) {
        AppntName = appntName;
    }

    public String getSumPrem() {
        return SumPrem;
    }

    public void setSumPrem(String sumPrem) {
        SumPrem = sumPrem;
    }

    public String getValiDate() {
        return ValiDate;
    }

    public void setValiDate(String valiDate) {
        ValiDate = valiDate;
    }

    public String getPayMode() {
        return PayMode;
    }

    public void setPayMode(String payMode) {
        PayMode = payMode;
    }

    public String getBankName() {
        return BankName;
    }

    public void setBankName(String bankName) {
        BankName = bankName;
    }

    public String getBankAccName() {
        return BankAccName;
    }

    public void setBankAccName(String bankAccName) {
        BankAccName = bankAccName;
    }

    public String getBankAccNo() {
        return BankAccNo;
    }

    public void setBankAccNo(String bankAccNo) {
        BankAccNo = bankAccNo;
    }

    public String getBankCode() {
        return BankCode;
    }

    public void setBankCode(String bankCode) {
        BankCode = bankCode;
    }

    public String getMainRiskName() {
        return MainRiskName;
    }

    public void setMainRiskName(String mainRiskName) {
        MainRiskName = mainRiskName;
    }

    public void setOldPayMode(String oldPayMode) {
        OldPayMode = oldPayMode;
    }

    public void setOldBankName(String oldBankName) {
        OldBankName = oldBankName;
    }

    public void setOldBankAccNo(String oldBankAccNo) {
        OldBankAccNo = oldBankAccNo;
    }

    public void setOldBankAccName(String oldBankAccName) {
        OldBankAccName = oldBankAccName;
    }

    @Override
    public String toString() {
        return "RespRenewalPayChangeDetailsQuery{" +
                "EdorType='" + EdorType + '\'' +
                ", ContNo='" + ContNo + '\'' +
                ", AppntName='" + AppntName + '\'' +
                ", SumPrem='" + SumPrem + '\'' +
                ", ValiDate='" + ValiDate + '\'' +
                ", PayMode='" + PayMode + '\'' +
                ", BankName='" + BankName + '\'' +
                ", BankAccName='" + BankAccName + '\'' +
                ", BankAccNo='" + BankAccNo + '\'' +
                ", BankCode='" + BankCode + '\'' +
                ", MainRiskName='" + MainRiskName + '\'' +
                ", OldPayMode='" + OldPayMode + '\'' +
                ", OldBankName='" + OldBankName + '\'' +
                ", OldBankAccNo='" + OldBankAccNo + '\'' +
                ", OldBankAccName='" + OldBankAccName + '\'' +
                '}';
    }
}
