package com.sinosoft.cloud.access.entity;
/**
 * 被保人身份证有效期变更明细查询POS054.
 */
public class ReqInsuredIdValiDateDetailsQuery {

    //保全项目编码
    private String EdorType;
    //保单号
    private String ContNo;

    public String getEdorType() {
        return EdorType;
    }

    public void setEdorType(String edorType) {
        EdorType = edorType;
    }

    public String getContNo() {
        return ContNo;
    }

    public void setContNo(String contNo) {
        ContNo = contNo;
    }

    @Override
    public String toString() {
        return "ReqInsuredIdValiDateDetailsQuery{" +
                "EdorType='" + EdorType + '\'' +
                ", ContNo='" + ContNo + '\'' +
                '}';
    }
}
