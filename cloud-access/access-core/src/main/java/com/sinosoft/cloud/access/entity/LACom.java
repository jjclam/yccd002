package com.sinosoft.cloud.access.entity;

/**
 * @Description
 * @Author:zhaoshulei
 * @Date:2018/4/10
 */
public class LACom {
    private String AgentCom;
    private String ManageCom;
    private String AreaType;
    private String ChannelType;
    private String UpAgentCom;
    private String Name;
    private String Address;
    private String ZipCode;
    private String Phone;
    private String Fax;
    private String EMail;
    private String WebAddress;
    private String LinkMan;
    private String Password;
    private String Corporation;
    private String BankCode;
    private String BankAccNo;
    private String BusinessType;
    private String GrpNature;
    private String ACType;
    private String SellFlag;
    private String Operator;
    private String MakeDate;
    private String MakeTime;
    private String ModifyDate;
    private String ModifyTime;
    private String BankType;
    private String CalFlag;
    private String BusiLicenseCode;
    private String InsureID;
    private String InsurePrincipal;
    private String ChiefBusiness;
    private String BusiAddress;
    private String SubscribeMan;
    private String SubscribeManDuty;
    private String LicenseNo;
    private String RegionalismCode;
    private String AppAgentCom;
    private String State;
    private String Noti;
    private String BusinessCode;
    private String LicenseStartDate;
    private String LicenseEndDate;
    private String BranchType;
    private String BranchType2;
    private double Assets;
    private double Income;
    private double Profits;
    private int PersonnalSum;
    private String ProtocalNo;
    private String HeadOffice;
    private String FoundDate;
    private String EndDate;
    private String Bank;
    private String AccName;
    private String DraWer;
    private String DraWerAccNo;
    private String RepManageCom;
    private String DraWerAccCode;
    private String DraWerAccName;
    private String ChannelType2;
    private String AreaType2;

    public String getAgentCom() {
        return AgentCom;
    }

    public void setAgentCom(String agentCom) {
        AgentCom = agentCom;
    }

    public String getManageCom() {
        return ManageCom;
    }

    public void setManageCom(String manageCom) {
        ManageCom = manageCom;
    }

    public String getAreaType() {
        return AreaType;
    }

    public void setAreaType(String areaType) {
        AreaType = areaType;
    }

    public String getChannelType() {
        return ChannelType;
    }

    public void setChannelType(String channelType) {
        ChannelType = channelType;
    }

    public String getUpAgentCom() {
        return UpAgentCom;
    }

    public void setUpAgentCom(String upAgentCom) {
        UpAgentCom = upAgentCom;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String address) {
        Address = address;
    }

    public String getZipCode() {
        return ZipCode;
    }

    public void setZipCode(String zipCode) {
        ZipCode = zipCode;
    }

    public String getPhone() {
        return Phone;
    }

    public void setPhone(String phone) {
        Phone = phone;
    }

    public String getFax() {
        return Fax;
    }

    public void setFax(String fax) {
        Fax = fax;
    }

    public String getEMail() {
        return EMail;
    }

    public void setEMail(String EMail) {
        this.EMail = EMail;
    }

    public String getWebAddress() {
        return WebAddress;
    }

    public void setWebAddress(String webAddress) {
        WebAddress = webAddress;
    }

    public String getLinkMan() {
        return LinkMan;
    }

    public void setLinkMan(String linkMan) {
        LinkMan = linkMan;
    }

    public String getPassword() {
        return Password;
    }

    public void setPassword(String password) {
        Password = password;
    }

    public String getCorporation() {
        return Corporation;
    }

    public void setCorporation(String corporation) {
        Corporation = corporation;
    }

    public String getBankCode() {
        return BankCode;
    }

    public void setBankCode(String bankCode) {
        BankCode = bankCode;
    }

    public String getBankAccNo() {
        return BankAccNo;
    }

    public void setBankAccNo(String bankAccNo) {
        BankAccNo = bankAccNo;
    }

    public String getBusinessType() {
        return BusinessType;
    }

    public void setBusinessType(String businessType) {
        BusinessType = businessType;
    }

    public String getGrpNature() {
        return GrpNature;
    }

    public void setGrpNature(String grpNature) {
        GrpNature = grpNature;
    }

    public String getACType() {
        return ACType;
    }

    public void setACType(String ACType) {
        this.ACType = ACType;
    }

    public String getSellFlag() {
        return SellFlag;
    }

    public void setSellFlag(String sellFlag) {
        SellFlag = sellFlag;
    }

    public String getOperator() {
        return Operator;
    }

    public void setOperator(String operator) {
        Operator = operator;
    }

    public String getMakeDate() {
        return MakeDate;
    }

    public void setMakeDate(String makeDate) {
        MakeDate = makeDate;
    }

    public String getMakeTime() {
        return MakeTime;
    }

    public void setMakeTime(String makeTime) {
        MakeTime = makeTime;
    }

    public String getModifyDate() {
        return ModifyDate;
    }

    public void setModifyDate(String modifyDate) {
        ModifyDate = modifyDate;
    }

    public String getModifyTime() {
        return ModifyTime;
    }

    public void setModifyTime(String modifyTime) {
        ModifyTime = modifyTime;
    }

    public String getBankType() {
        return BankType;
    }

    public void setBankType(String bankType) {
        BankType = bankType;
    }

    public String getCalFlag() {
        return CalFlag;
    }

    public void setCalFlag(String calFlag) {
        CalFlag = calFlag;
    }

    public String getBusiLicenseCode() {
        return BusiLicenseCode;
    }

    public void setBusiLicenseCode(String busiLicenseCode) {
        BusiLicenseCode = busiLicenseCode;
    }

    public String getInsureID() {
        return InsureID;
    }

    public void setInsureID(String insureID) {
        InsureID = insureID;
    }

    public String getInsurePrincipal() {
        return InsurePrincipal;
    }

    public void setInsurePrincipal(String insurePrincipal) {
        InsurePrincipal = insurePrincipal;
    }

    public String getChiefBusiness() {
        return ChiefBusiness;
    }

    public void setChiefBusiness(String chiefBusiness) {
        ChiefBusiness = chiefBusiness;
    }

    public String getBusiAddress() {
        return BusiAddress;
    }

    public void setBusiAddress(String busiAddress) {
        BusiAddress = busiAddress;
    }

    public String getSubscribeMan() {
        return SubscribeMan;
    }

    public void setSubscribeMan(String subscribeMan) {
        SubscribeMan = subscribeMan;
    }

    public String getSubscribeManDuty() {
        return SubscribeManDuty;
    }

    public void setSubscribeManDuty(String subscribeManDuty) {
        SubscribeManDuty = subscribeManDuty;
    }

    public String getLicenseNo() {
        return LicenseNo;
    }

    public void setLicenseNo(String licenseNo) {
        LicenseNo = licenseNo;
    }

    public String getRegionalismCode() {
        return RegionalismCode;
    }

    public void setRegionalismCode(String regionalismCode) {
        RegionalismCode = regionalismCode;
    }

    public String getAppAgentCom() {
        return AppAgentCom;
    }

    public void setAppAgentCom(String appAgentCom) {
        AppAgentCom = appAgentCom;
    }

    public String getState() {
        return State;
    }

    public void setState(String state) {
        State = state;
    }

    public String getNoti() {
        return Noti;
    }

    public void setNoti(String noti) {
        Noti = noti;
    }

    public String getBusinessCode() {
        return BusinessCode;
    }

    public void setBusinessCode(String businessCode) {
        BusinessCode = businessCode;
    }

    public String getLicenseStartDate() {
        return LicenseStartDate;
    }

    public void setLicenseStartDate(String licenseStartDate) {
        LicenseStartDate = licenseStartDate;
    }

    public String getLicenseEndDate() {
        return LicenseEndDate;
    }

    public void setLicenseEndDate(String licenseEndDate) {
        LicenseEndDate = licenseEndDate;
    }

    public String getBranchType() {
        return BranchType;
    }

    public void setBranchType(String branchType) {
        BranchType = branchType;
    }

    public String getBranchType2() {
        return BranchType2;
    }

    public void setBranchType2(String branchType2) {
        BranchType2 = branchType2;
    }

    public double getAssets() {
        return Assets;
    }

    public void setAssets(double assets) {
        Assets = assets;
    }

    public double getIncome() {
        return Income;
    }

    public void setIncome(double income) {
        Income = income;
    }

    public double getProfits() {
        return Profits;
    }

    public void setProfits(double profits) {
        Profits = profits;
    }

    public int getPersonnalSum() {
        return PersonnalSum;
    }

    public void setPersonnalSum(int personnalSum) {
        PersonnalSum = personnalSum;
    }

    public String getProtocalNo() {
        return ProtocalNo;
    }

    public void setProtocalNo(String protocalNo) {
        ProtocalNo = protocalNo;
    }

    public String getHeadOffice() {
        return HeadOffice;
    }

    public void setHeadOffice(String headOffice) {
        HeadOffice = headOffice;
    }

    public String getFoundDate() {
        return FoundDate;
    }

    public void setFoundDate(String foundDate) {
        FoundDate = foundDate;
    }

    public String getEndDate() {
        return EndDate;
    }

    public void setEndDate(String endDate) {
        EndDate = endDate;
    }

    public String getBank() {
        return Bank;
    }

    public void setBank(String bank) {
        Bank = bank;
    }

    public String getAccName() {
        return AccName;
    }

    public void setAccName(String accName) {
        AccName = accName;
    }

    public String getDraWer() {
        return DraWer;
    }

    public void setDraWer(String draWer) {
        DraWer = draWer;
    }

    public String getDraWerAccNo() {
        return DraWerAccNo;
    }

    public void setDraWerAccNo(String draWerAccNo) {
        DraWerAccNo = draWerAccNo;
    }

    public String getRepManageCom() {
        return RepManageCom;
    }

    public void setRepManageCom(String repManageCom) {
        RepManageCom = repManageCom;
    }

    public String getDraWerAccCode() {
        return DraWerAccCode;
    }

    public void setDraWerAccCode(String draWerAccCode) {
        DraWerAccCode = draWerAccCode;
    }

    public String getDraWerAccName() {
        return DraWerAccName;
    }

    public void setDraWerAccName(String draWerAccName) {
        DraWerAccName = draWerAccName;
    }

    public String getChannelType2() {
        return ChannelType2;
    }

    public void setChannelType2(String channelType2) {
        ChannelType2 = channelType2;
    }

    public String getAreaType2() {
        return AreaType2;
    }

    public void setAreaType2(String areaType2) {
        AreaType2 = areaType2;
    }

    @Override
    public String toString() {
        return "LACom{" +
                "AgentCom='" + AgentCom + '\'' +
                ", ManageCom='" + ManageCom + '\'' +
                ", AreaType='" + AreaType + '\'' +
                ", ChannelType='" + ChannelType + '\'' +
                ", UpAgentCom='" + UpAgentCom + '\'' +
                ", Name='" + Name + '\'' +
                ", Address='" + Address + '\'' +
                ", ZipCode='" + ZipCode + '\'' +
                ", Phone='" + Phone + '\'' +
                ", Fax='" + Fax + '\'' +
                ", EMail='" + EMail + '\'' +
                ", WebAddress='" + WebAddress + '\'' +
                ", LinkMan='" + LinkMan + '\'' +
                ", Password='" + Password + '\'' +
                ", Corporation='" + Corporation + '\'' +
                ", BankCode='" + BankCode + '\'' +
                ", BankAccNo='" + BankAccNo + '\'' +
                ", BusinessType='" + BusinessType + '\'' +
                ", GrpNature='" + GrpNature + '\'' +
                ", ACType='" + ACType + '\'' +
                ", SellFlag='" + SellFlag + '\'' +
                ", Operator='" + Operator + '\'' +
                ", MakeDate='" + MakeDate + '\'' +
                ", MakeTime='" + MakeTime + '\'' +
                ", ModifyDate='" + ModifyDate + '\'' +
                ", ModifyTime='" + ModifyTime + '\'' +
                ", BankType='" + BankType + '\'' +
                ", CalFlag='" + CalFlag + '\'' +
                ", BusiLicenseCode='" + BusiLicenseCode + '\'' +
                ", InsureID='" + InsureID + '\'' +
                ", InsurePrincipal='" + InsurePrincipal + '\'' +
                ", ChiefBusiness='" + ChiefBusiness + '\'' +
                ", BusiAddress='" + BusiAddress + '\'' +
                ", SubscribeMan='" + SubscribeMan + '\'' +
                ", SubscribeManDuty='" + SubscribeManDuty + '\'' +
                ", LicenseNo='" + LicenseNo + '\'' +
                ", RegionalismCode='" + RegionalismCode + '\'' +
                ", AppAgentCom='" + AppAgentCom + '\'' +
                ", State='" + State + '\'' +
                ", Noti='" + Noti + '\'' +
                ", BusinessCode='" + BusinessCode + '\'' +
                ", LicenseStartDate='" + LicenseStartDate + '\'' +
                ", LicenseEndDate='" + LicenseEndDate + '\'' +
                ", BranchType='" + BranchType + '\'' +
                ", BranchType2='" + BranchType2 + '\'' +
                ", Assets=" + Assets +
                ", Income=" + Income +
                ", Profits=" + Profits +
                ", PersonnalSum=" + PersonnalSum +
                ", ProtocalNo='" + ProtocalNo + '\'' +
                ", HeadOffice='" + HeadOffice + '\'' +
                ", FoundDate='" + FoundDate + '\'' +
                ", EndDate='" + EndDate + '\'' +
                ", Bank='" + Bank + '\'' +
                ", AccName='" + AccName + '\'' +
                ", DraWer='" + DraWer + '\'' +
                ", DraWerAccNo='" + DraWerAccNo + '\'' +
                ", RepManageCom='" + RepManageCom + '\'' +
                ", DraWerAccCode='" + DraWerAccCode + '\'' +
                ", DraWerAccName='" + DraWerAccName + '\'' +
                ", ChannelType2='" + ChannelType2 + '\'' +
                ", AreaType2='" + AreaType2 + '\'' +
                '}';
    }
}
