package com.sinosoft.cloud.access.entity;

import com.sinosoft.cloud.access.annotations.BeanCopy;

/**
 * @Author:lizhen
 * @Description:
 * @Date:Created in 17:49 2018/11/9
 * @Modified By:
 */
/**
 * 保全进度查询  pos015返回
 */
public class RespBQProgressQuery {
//    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity",complicated = "Y")
    private ProgressQueryInfos ProgressQueryInfos;

    public ProgressQueryInfos getProgressQueryInfos() {
        return ProgressQueryInfos;
    }

    public void setProgressQueryInfos(ProgressQueryInfos progressQueryInfos) {
        ProgressQueryInfos = progressQueryInfos;
    }

    @Override
    public String toString() {
        return "ReqBQProgressQuery{" +
                "ProgressQueryInfos=" + ProgressQueryInfos +
                '}';
    }

}