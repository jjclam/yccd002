package com.sinosoft.cloud.access.entity;

/**
 * @program: abc-cloud-access
 * @description: 客服信函服务POS093.
 * @author: BaoYongmeng
 * @create: 2018-12-24 17:18
 **/
public class RespCustomerService {
    //保全项目编码
    private String EdorType;

    private PolicyXHQueryInfos PolicyXHQueryInfos;

    public void setEdorType(String edorType) {
        EdorType = edorType;
    }

    public void setPolicyXHQueryInfos(PolicyXHQueryInfos policyXHQueryInfos) {
        PolicyXHQueryInfos = policyXHQueryInfos;
    }

    public String getEdorType() {
        return EdorType;
    }

    public PolicyXHQueryInfos getPolicyXHQueryInfos() {
        return PolicyXHQueryInfos;
    }

    @Override
    public String toString() {
        return "RespCustomerService{" +
                "EdorType='" + EdorType + '\'' +
                ", PolicyXHQueryInfos=" + PolicyXHQueryInfos +
                '}';
    }
}
