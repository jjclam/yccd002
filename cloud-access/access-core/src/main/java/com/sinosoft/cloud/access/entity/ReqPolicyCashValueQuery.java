package com.sinosoft.cloud.access.entity;

/**
 * @Author:lizhen
 * @Description:
 * @Date:Created in 1:56 2018/11/18
 * @Modified By:
 */
/**
 * 保单价值查询PC001.
 */
public class ReqPolicyCashValueQuery {
    //保单号
    private String ContNo;

    public String getContNo() {
        return ContNo;
    }

    public void setContNo(String contNo) {
        ContNo = contNo;
    }

    @Override
    public String toString() {
        return "ReqPolicyCashValueQuery{" +
                "ContNo='" + ContNo + '\'' +
                '}';
    }
}