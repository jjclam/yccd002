package com.sinosoft.cloud.access.entity;

/**
 * abc-cloud-access
 *
 * @author lizhen
 * @create 2019/3/4
 */
/**
 * 官网，微信 保单价值查询   PC007
 */
public class ReqWebSitePolicyValueQuery {
    //xxx订单号
    private String ABCOrderId;
    //保单号
    private String ContNo;

    public String getABCOrderId() {
        return ABCOrderId;
    }

    public void setABCOrderId(String ABCOrderId) {
        this.ABCOrderId = ABCOrderId;
    }

    public String getContNo() {
        return ContNo;
    }

    public void setContNo(String contNo) {
        ContNo = contNo;
    }

    @Override
    public String toString() {
        return "ReqWebSitePolicyValueQuery{" +
                "ABCOrderId='" + ABCOrderId + '\'' +
                ", ContNo='" + ContNo + '\'' +
                '}';
    }
}