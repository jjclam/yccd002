package com.sinosoft.cloud.access.entity;

/**
 * @Author:lizhen
 * @Description:
 * @Date:Created in 22:46 2018/11/13
 * @Modified By:
 */
/**
 * 保单补发列表查询POS001.
 */
public class ReqPolicySupplyQuery {
    //保全项目编码
    private String EdorType;
    //证件号
    private String CustomerIDNo;
    //证件姓名
    private String CustomerName;

    public String getEdorType() {
        return EdorType;
    }

    public void setEdorType(String edorType) {
        EdorType = edorType;
    }

    public String getCustomerIDNo() {
        return CustomerIDNo;
    }

    public void setCustomerIDNo(String customerIDNo) {
        CustomerIDNo = customerIDNo;
    }

    public String getCustomerName() {
        return CustomerName;
    }

    public void setCustomerName(String customerName) {
        CustomerName = customerName;
    }

    @Override
    public String toString() {
        return "ReqPolicySupplyQuery{" +
                "EdorType='" + EdorType + '\'' +
                ", CustomerIDNo='" + CustomerIDNo + '\'' +
                ", CustomerName='" + CustomerName + '\'' +
                '}';
    }
}