package com.sinosoft.cloud.access.entity;

/**
 * Created by Administrator on 2018/4/16.
 */
public class LCDuty {
    private long DutyID;
    private long PolID;
    private String ShardingID;
    private String PolNo;
    private String DutyCode;
    private String ContNo;
    private double Mult;
    private double StandPrem;
    private double Prem;
    private double SumPrem;
    private double Amnt;
    private double RiskAmnt;
    private int PayIntv;
    private int PayYears;
    private int Years;
    private double FloatRate;
    private String FirstPayDate;
    private int FirstMonth;
    private String PaytoDate;
    private String PayEndDate;
    private String PayEndYearFlag;
    private int PayEndYear;
    private String GetYearFlag;
    private int GetYear;
    private String InsuYearFlag;
    private int InsuYear;
    private String AcciYearFlag;
    private int AcciYear;
    private String EndDate;
    private String AcciEndDate;
    private String FreeFlag;
    private double FreeRate;
    private String FreeStartDate;
    private String FreeEndDate;
    private String GetStartDate;
    private String GetStartType;
    private String LiveGetMode;
    private String DeadGetMode;
    private String BonusGetMode;
    private String SSFlag;
    private double PeakLine;
    private double GetLimit;
    private double GetRate;
    private String CalRule;
    private String PremToAmnt;
    private String StandbyFlag1;
    private String StandbyFlag2;
    private String StandbyFlag3;
    private String Operator;
    private String MakeDate;
    private String MakeTime;
    private String ModifyDate;
    private String ModifyTime;
    private String CValiDate;
    private int GetIntv;
    private String AscriptionRuleCode;
    private String PayRuleCode;
    private double StandRatePrem;

    public long getDutyID() {
        return DutyID;
    }

    public void setDutyID(long dutyID) {
        DutyID = dutyID;
    }

    public long getPolID() {
        return PolID;
    }

    public void setPolID(long polID) {
        PolID = polID;
    }

    public String getShardingID() {
        return ShardingID;
    }

    public void setShardingID(String shardingID) {
        ShardingID = shardingID;
    }

    public String getPolNo() {
        return PolNo;
    }

    public void setPolNo(String polNo) {
        PolNo = polNo;
    }

    public String getDutyCode() {
        return DutyCode;
    }

    public void setDutyCode(String dutyCode) {
        DutyCode = dutyCode;
    }

    public String getContNo() {
        return ContNo;
    }

    public void setContNo(String contNo) {
        ContNo = contNo;
    }

    public double getMult() {
        return Mult;
    }

    public void setMult(double mult) {
        Mult = mult;
    }

    public double getStandPrem() {
        return StandPrem;
    }

    public void setStandPrem(double standPrem) {
        StandPrem = standPrem;
    }

    public double getPrem() {
        return Prem;
    }

    public void setPrem(double prem) {
        Prem = prem;
    }

    public double getSumPrem() {
        return SumPrem;
    }

    public void setSumPrem(double sumPrem) {
        SumPrem = sumPrem;
    }

    public double getAmnt() {
        return Amnt;
    }

    public void setAmnt(double amnt) {
        Amnt = amnt;
    }

    public double getRiskAmnt() {
        return RiskAmnt;
    }

    public void setRiskAmnt(double riskAmnt) {
        RiskAmnt = riskAmnt;
    }

    public int getPayIntv() {
        return PayIntv;
    }

    public void setPayIntv(int payIntv) {
        PayIntv = payIntv;
    }

    public int getPayYears() {
        return PayYears;
    }

    public void setPayYears(int payYears) {
        PayYears = payYears;
    }

    public int getYears() {
        return Years;
    }

    public void setYears(int years) {
        Years = years;
    }

    public double getFloatRate() {
        return FloatRate;
    }

    public void setFloatRate(double floatRate) {
        FloatRate = floatRate;
    }

    public String getFirstPayDate() {
        return FirstPayDate;
    }

    public void setFirstPayDate(String firstPayDate) {
        FirstPayDate = firstPayDate;
    }

    public int getFirstMonth() {
        return FirstMonth;
    }

    public void setFirstMonth(int firstMonth) {
        FirstMonth = firstMonth;
    }

    public String getPaytoDate() {
        return PaytoDate;
    }

    public void setPaytoDate(String paytoDate) {
        PaytoDate = paytoDate;
    }

    public String getPayEndDate() {
        return PayEndDate;
    }

    public void setPayEndDate(String payEndDate) {
        PayEndDate = payEndDate;
    }

    public String getPayEndYearFlag() {
        return PayEndYearFlag;
    }

    public void setPayEndYearFlag(String payEndYearFlag) {
        PayEndYearFlag = payEndYearFlag;
    }

    public int getPayEndYear() {
        return PayEndYear;
    }

    public void setPayEndYear(int payEndYear) {
        PayEndYear = payEndYear;
    }

    public String getGetYearFlag() {
        return GetYearFlag;
    }

    public void setGetYearFlag(String getYearFlag) {
        GetYearFlag = getYearFlag;
    }

    public int getGetYear() {
        return GetYear;
    }

    public void setGetYear(int getYear) {
        GetYear = getYear;
    }

    public String getInsuYearFlag() {
        return InsuYearFlag;
    }

    public void setInsuYearFlag(String insuYearFlag) {
        InsuYearFlag = insuYearFlag;
    }

    public int getInsuYear() {
        return InsuYear;
    }

    public void setInsuYear(int insuYear) {
        InsuYear = insuYear;
    }

    public String getAcciYearFlag() {
        return AcciYearFlag;
    }

    public void setAcciYearFlag(String acciYearFlag) {
        AcciYearFlag = acciYearFlag;
    }

    public int getAcciYear() {
        return AcciYear;
    }

    public void setAcciYear(int acciYear) {
        AcciYear = acciYear;
    }

    public String getEndDate() {
        return EndDate;
    }

    public void setEndDate(String endDate) {
        EndDate = endDate;
    }

    public String getAcciEndDate() {
        return AcciEndDate;
    }

    public void setAcciEndDate(String acciEndDate) {
        AcciEndDate = acciEndDate;
    }

    public String getFreeFlag() {
        return FreeFlag;
    }

    public void setFreeFlag(String freeFlag) {
        FreeFlag = freeFlag;
    }

    public double getFreeRate() {
        return FreeRate;
    }

    public void setFreeRate(double freeRate) {
        FreeRate = freeRate;
    }

    public String getFreeStartDate() {
        return FreeStartDate;
    }

    public void setFreeStartDate(String freeStartDate) {
        FreeStartDate = freeStartDate;
    }

    public String getFreeEndDate() {
        return FreeEndDate;
    }

    public void setFreeEndDate(String freeEndDate) {
        FreeEndDate = freeEndDate;
    }

    public String getGetStartDate() {
        return GetStartDate;
    }

    public void setGetStartDate(String getStartDate) {
        GetStartDate = getStartDate;
    }

    public String getGetStartType() {
        return GetStartType;
    }

    public void setGetStartType(String getStartType) {
        GetStartType = getStartType;
    }

    public String getLiveGetMode() {
        return LiveGetMode;
    }

    public void setLiveGetMode(String liveGetMode) {
        LiveGetMode = liveGetMode;
    }

    public String getDeadGetMode() {
        return DeadGetMode;
    }

    public void setDeadGetMode(String deadGetMode) {
        DeadGetMode = deadGetMode;
    }

    public String getBonusGetMode() {
        return BonusGetMode;
    }

    public void setBonusGetMode(String bonusGetMode) {
        BonusGetMode = bonusGetMode;
    }

    public String getSSFlag() {
        return SSFlag;
    }

    public void setSSFlag(String SSFlag) {
        this.SSFlag = SSFlag;
    }

    public double getPeakLine() {
        return PeakLine;
    }

    public void setPeakLine(double peakLine) {
        PeakLine = peakLine;
    }

    public double getGetLimit() {
        return GetLimit;
    }

    public void setGetLimit(double getLimit) {
        GetLimit = getLimit;
    }

    public double getGetRate() {
        return GetRate;
    }

    public void setGetRate(double getRate) {
        GetRate = getRate;
    }

    public String getCalRule() {
        return CalRule;
    }

    public void setCalRule(String calRule) {
        CalRule = calRule;
    }

    public String getPremToAmnt() {
        return PremToAmnt;
    }

    public void setPremToAmnt(String premToAmnt) {
        PremToAmnt = premToAmnt;
    }

    public String getStandbyFlag1() {
        return StandbyFlag1;
    }

    public void setStandbyFlag1(String standbyFlag1) {
        StandbyFlag1 = standbyFlag1;
    }

    public String getStandbyFlag2() {
        return StandbyFlag2;
    }

    public void setStandbyFlag2(String standbyFlag2) {
        StandbyFlag2 = standbyFlag2;
    }

    public String getStandbyFlag3() {
        return StandbyFlag3;
    }

    public void setStandbyFlag3(String standbyFlag3) {
        StandbyFlag3 = standbyFlag3;
    }

    public String getOperator() {
        return Operator;
    }

    public void setOperator(String operator) {
        Operator = operator;
    }

    public String getMakeDate() {
        return MakeDate;
    }

    public void setMakeDate(String makeDate) {
        MakeDate = makeDate;
    }

    public String getMakeTime() {
        return MakeTime;
    }

    public void setMakeTime(String makeTime) {
        MakeTime = makeTime;
    }

    public String getModifyDate() {
        return ModifyDate;
    }

    public void setModifyDate(String modifyDate) {
        ModifyDate = modifyDate;
    }

    public String getModifyTime() {
        return ModifyTime;
    }

    public void setModifyTime(String modifyTime) {
        ModifyTime = modifyTime;
    }

    public String getCValiDate() {
        return CValiDate;
    }

    public void setCValiDate(String CValiDate) {
        this.CValiDate = CValiDate;
    }

    public int getGetIntv() {
        return GetIntv;
    }

    public void setGetIntv(int getIntv) {
        GetIntv = getIntv;
    }

    public String getAscriptionRuleCode() {
        return AscriptionRuleCode;
    }

    public void setAscriptionRuleCode(String ascriptionRuleCode) {
        AscriptionRuleCode = ascriptionRuleCode;
    }

    public String getPayRuleCode() {
        return PayRuleCode;
    }

    public void setPayRuleCode(String payRuleCode) {
        PayRuleCode = payRuleCode;
    }

    public double getStandRatePrem() {
        return StandRatePrem;
    }

    public void setStandRatePrem(double standRatePrem) {
        StandRatePrem = standRatePrem;
    }
}
