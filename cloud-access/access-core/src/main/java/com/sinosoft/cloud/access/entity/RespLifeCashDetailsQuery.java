package com.sinosoft.cloud.access.entity;

import com.sinosoft.lis.pos.entity.SurvivalRisksPojo;

/**
 * 生存金详情查询POS047.
 */
public class RespLifeCashDetailsQuery {

    //保全项目编码
    private String EdorType;
    //保单号
    private String ContNo;
    //生存受益人
    private String SurvivalAppntNo;
    //生存金总额
    private String SumSurvivalMoney;
    private SurvivalRisks SurvivalRisks;

    public String getEdorType() {
        return EdorType;
    }

    public void setEdorType(String edorType) {
        EdorType = edorType;
    }

    public String getContNo() {
        return ContNo;
    }

    public void setContNo(String contNo) {
        ContNo = contNo;
    }

    public String getSurvivalAppntNo() {
        return SurvivalAppntNo;
    }

    public void setSurvivalAppntNo(String survivalAppntNo) {
        SurvivalAppntNo = survivalAppntNo;
    }

    public String getSumSurvivalMoney() {
        return SumSurvivalMoney;
    }

    public void setSumSurvivalMoney(String sumSurvivalMoney) {
        SumSurvivalMoney = sumSurvivalMoney;
    }

    public com.sinosoft.cloud.access.entity.SurvivalRisks getSurvivalRisks() {
        return SurvivalRisks;
    }

    public void setSurvivalRisks(com.sinosoft.cloud.access.entity.SurvivalRisks survivalRisks) {
        SurvivalRisks = survivalRisks;
    }


    @Override
    public String toString() {
        return "RespLifeCashDetailsQuery{" +
                "EdorType='" + EdorType + '\'' +
                ", ContNo='" + ContNo + '\'' +
                ", SurvivalAppntNo='" + SurvivalAppntNo + '\'' +
                ", SumSurvivalMoney='" + SumSurvivalMoney + '\'' +
                ", SurvivalRisks=" + SurvivalRisks +
                '}';
    }
}
