package com.sinosoft.cloud.access.router;


import com.sinosoft.cloud.access.entity.Body;
import com.sinosoft.cloud.access.entity.Head;
import com.sinosoft.cloud.access.entity.TranData;
import com.xiaoleilu.hutool.util.BeanUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.expression.BeanFactoryResolver;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.expression.Expression;
import org.springframework.expression.ExpressionParser;
import org.springframework.expression.spel.standard.SpelExpressionParser;
import org.springframework.expression.spel.support.StandardEvaluationContext;

import java.util.Map;
import java.util.concurrent.TimeUnit;


public abstract class RouterBean {

    protected final Log logger = LogFactory.getLog(getClass());

    protected ExpressionParser parser = new SpelExpressionParser();

    @Autowired
    BeanFactory beanFactory;

    @Autowired
    StringRedisTemplate redisTemplate;
//    private Head head;
//    private Body body;


    public Boolean isHeadRemote(Head head) {
        String el = getHeadEL();
        TranData tranData = new TranData();
        tranData.setHead(head);
        return isRemote(el,tranData);
    }

    public Boolean isBodyRemote(Body body) {
        String el = getBodyEL();
        TranData tranData = new TranData();
        tranData.setBody(body);
        return isRemote(el,tranData);
    }

    public Boolean isCrossRemote(TranData tranData) {
        String el = getCrossEL();
        return isRemote(el,tranData);
    }


    /**
     * @return
     */
    private Boolean isRemote(String el,TranData tranData) {
        Map<String, Object> map = getParams(tranData);

        if (map == null) {
            logger.error("在路由判断前应先传入 head 或 body !");
            throw new RuntimeException("在路由判断前应先传入 head 或 body");
        }

        StandardEvaluationContext headContext = new StandardEvaluationContext();
        BeanFactoryResolver beanFactoryResolver = new BeanFactoryResolver(beanFactory);
        headContext.setBeanResolver(beanFactoryResolver);
//        map.put("routerBean",this);
        headContext.setVariables(map);
//        String el = getEL();
        Expression expression = parser.parseExpression(el);
        Boolean result = expression.getValue(headContext, Boolean.class);
        return result;
    }

    private Map<String, Object> getParams(TranData tranData) {

        if (null != tranData.getHead() && null != tranData.getBody()){
            return BeanUtil.beanToMap(tranData, false, true);
        }

        if (tranData.getHead() != null) {
            return BeanUtil.beanToMap(tranData.getHead(), false, true);
        } else if (tranData.getBody() != null) {
            return BeanUtil.beanToMap(tranData.getBody(), false, true);
        }
        return null;
    }


//    public Head getHead() {
//        return head;
//    }
//
//    public void setHead(Head head) {
//        this.head = head;
//        this.body = null;
//    }
//
//    public Body getBody() {
//        return body;
//    }
//
//    public void setBody(Body body) {
//        this.body = body;
//        this.head = null;
//    }


//    @CachePut(value = "AccessRouter", key = "'AccessRouter'+#key")
    public Boolean setRouterKey(String key) {
        redisTemplate.opsForValue().set("AccessRouter" + key,"1",1,TimeUnit.DAYS);
        return true;
    }

//    @Cacheable(value = "AccessRouter", key = "'AccessRoute'+#key")
    public Boolean getRouterKey(String key) {
        return redisTemplate.hasKey("AccessRouter" + key);
    }

    protected abstract String getBodyEL();

    protected abstract String getHeadEL();

    protected abstract void setBodyEL(String bodyEL);

    protected abstract void setHeadEL(String headEL);

    protected abstract String getCrossEL();

    protected abstract void setCrossEL(String crossEL);

    public abstract String getTargetIP();

    public abstract String getTargetPort();

    public abstract String getTargetUrl();

}
