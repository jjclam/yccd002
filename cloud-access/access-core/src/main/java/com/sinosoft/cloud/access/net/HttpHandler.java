package com.sinosoft.cloud.access.net;

/**
 * @Author: 崔广东
 * @Date: 2018/3/23 9:39
 * @Description:
 */
public interface HttpHandler {
    /**
     * 业务处理方法
     *
     * @param msg 解密后的报文
     * @return
     */
    String submitData(String msg);

    /**
     * 数据转换处理
     *
     * @param msg 解密后的报文
     * @return
     */
    String receive(String msg);

    /**
     * 加入渠道名称
     *
     * @param accessName 渠道名称
     */
    void setAccessName(String accessName);

}
