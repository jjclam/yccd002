package com.sinosoft.cloud.access.entity;

/**
 * 保单查询PC002
 */
public class ReqContQuery {
    //险种代码
    private String RiskCode;
    //组合编码
    private String RiskCodeWr;
    //保单号
    private String ContNo;
    //订单号
    private String ThirdPartyOrderId;

    public String getThirdPartyOrderId() {
        return ThirdPartyOrderId;
    }

    public void setThirdPartyOrderId(String thirdPartyOrderId) {
        ThirdPartyOrderId = thirdPartyOrderId;
    }

    public String getRiskCode() {
        return RiskCode;
    }

    public void setRiskCode(String riskCode) {
        RiskCode = riskCode;
    }

    public String getRiskCodeWr() {
        return RiskCodeWr;
    }

    public void setRiskCodeWr(String riskCodeWr) {
        RiskCodeWr = riskCodeWr;
    }

    public String getContNo() {
        return ContNo;
    }

    public void setContNo(String contNo) {
        ContNo = contNo;
    }

    @Override
    public String toString() {
        return "ReqContQuery{" +
                "RiskCode='" + RiskCode + '\'' +
                ", RiskCodeWr='" + RiskCodeWr + '\'' +
                ", ContNo='" + ContNo + '\'' +
                '}';
    }
}
