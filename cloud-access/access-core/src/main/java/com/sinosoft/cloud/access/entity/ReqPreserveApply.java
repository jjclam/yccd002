package com.sinosoft.cloud.access.entity;
/**
 * 生存给付领取方式变更保全申请POS041.
 */
public class ReqPreserveApply {

    //保全项目编码
    private String EdorType;
    //保单号
    private String ContNo;
    //申请日期
    private String ApplyDate;
    //红利领取方式
    private String GetMode;
    //业务类型
    private String BussType;
    //收付费方式
    private String PayMode;
    private EdorAppInfo EdorAppInfo;
    private PersonInfo PersonInfo;
    private BankInfo BankInfo;
    //生存金派发方式（一级）
    private String LiveGetMode	;
    //生存金派发方式（二级）
    private String LiveAccFlag;
    //生存金派发方式（三级）
    private String GetForm	;
    //领取方式校验标识
    private String JudgeFlag;

    public String getEdorType() {
        return EdorType;
    }

    public void setEdorType(String edorType) {
        EdorType = edorType;
    }

    public String getContNo() {
        return ContNo;
    }

    public void setContNo(String contNo) {
        ContNo = contNo;
    }

    public String getApplyDate() {
        return ApplyDate;
    }

    public void setApplyDate(String applyDate) {
        ApplyDate = applyDate;
    }

    public String getGetMode() {
        return GetMode;
    }

    public void setGetMode(String getMode) {
        GetMode = getMode;
    }

    public String getBussType() {
        return BussType;
    }

    public void setBussType(String bussType) {
        BussType = bussType;
    }

    public String getPayMode() {
        return PayMode;
    }

    public void setPayMode(String payMode) {
        PayMode = payMode;
    }

    public com.sinosoft.cloud.access.entity.EdorAppInfo getEdorAppInfo() {
        return EdorAppInfo;
    }

    public void setEdorAppInfo(com.sinosoft.cloud.access.entity.EdorAppInfo edorAppInfo) {
        EdorAppInfo = edorAppInfo;
    }

    public com.sinosoft.cloud.access.entity.PersonInfo getPersonInfo() {
        return PersonInfo;
    }

    public void setPersonInfo(com.sinosoft.cloud.access.entity.PersonInfo personInfo) {
        PersonInfo = personInfo;
    }

    public com.sinosoft.cloud.access.entity.BankInfo getBankInfo() {
        return BankInfo;
    }

    public void setBankInfo(com.sinosoft.cloud.access.entity.BankInfo bankInfo) {
        BankInfo = bankInfo;
    }

    public String getLiveGetMode() {
        return LiveGetMode;
    }

    public void setLiveGetMode(String liveGetMode) {
        LiveGetMode = liveGetMode;
    }

    public String getLiveAccFlag() {
        return LiveAccFlag;
    }

    public void setLiveAccFlag(String liveAccFlag) {
        LiveAccFlag = liveAccFlag;
    }

    public String getGetForm() {
        return GetForm;
    }

    public void setGetForm(String getForm) {
        GetForm = getForm;
    }

    public String getJudgeFlag() {
        return JudgeFlag;
    }

    public void setJudgeFlag(String judgeFlag) {
        JudgeFlag = judgeFlag;
    }

    @Override
    public String toString() {
        return "ReqPreserveApply{" +
                "EdorType='" + EdorType + '\'' +
                ", ContNo='" + ContNo + '\'' +
                ", ApplyDate='" + ApplyDate + '\'' +
                ", GetMode='" + GetMode + '\'' +
                ", BussType='" + BussType + '\'' +
                ", PayMode='" + PayMode + '\'' +
                ", EdorAppInfo=" + EdorAppInfo +
                ", PersonInfo=" + PersonInfo +
                ", BankInfo=" + BankInfo +
                ", LiveGetMode='" + LiveGetMode + '\'' +
                ", LiveAccFlag='" + LiveAccFlag + '\'' +
                ", GetForm='" + GetForm + '\'' +
                ", JudgeFlag='" + JudgeFlag + '\'' +
                '}';
    }
}
