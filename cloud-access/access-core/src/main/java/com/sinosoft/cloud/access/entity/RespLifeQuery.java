package com.sinosoft.cloud.access.entity;
/**
 * 生存调查列表查询POS048.
 */
public class RespLifeQuery {
    //保全项目编码
    private String EdorType;
    //处理结果
    private String DealResult;

    public String getEdorType() {
        return EdorType;
    }

    public void setEdorType(String edorType) {
        EdorType = edorType;
    }

    public String getDealResult() {
        return DealResult;
    }

    public void setDealResult(String dealResult) {
        DealResult = dealResult;
    }

    @Override
    public String toString() {
        return "RespLifeQuery{" +
                "EdorType='" + EdorType + '\'' +
                ", DealResult='" + DealResult + '\'' +
                '}';
    }
}
