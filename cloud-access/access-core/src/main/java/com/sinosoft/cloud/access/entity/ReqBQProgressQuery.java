package com.sinosoft.cloud.access.entity;

/**
 * @Author:lizhen
 * @Description:
 * @Date:Created in 17:47 2018/11/9
 * @Modified By:
 */
/**
 * 保全进度查询 pos015 请求
 */
public class ReqBQProgressQuery {

    //保单号
    private String ContNo;
    //    保全项目编码
    private String EdorType;
    // 批单号

    private String EdorNo;

    //客户号
    private String CustomerNo;
    //姓名
    private String CustomerName;
    //身份证件号
    private String CustomerIDNo;
    //保全提交起期
    private String EdorStartDate;
    //保全提交止期
    private String EdorEndDate;

    public String getContNo() {
        return ContNo;
    }

    public void setContNo(String contNo) {
        ContNo = contNo;
    }

    public String getEdorType() {
        return EdorType;
    }

    public void setEdorType(String edorType) {
        EdorType = edorType;
    }

    public String getEdorNo() {
        return EdorNo;
    }

    public void setEdorNo(String edorNo) {
        EdorNo = edorNo;
    }

    public String getCustomerNo() {
        return CustomerNo;
    }

    public void setCustomerNo(String customerNo) {
        CustomerNo = customerNo;
    }

    public String getCustomerName() {
        return CustomerName;
    }

    public void setCustomerName(String customerName) {
        CustomerName = customerName;
    }

    public String getCustomerIDNo() {
        return CustomerIDNo;
    }

    public void setCustomerIDNo(String customerIDNo) {
        CustomerIDNo = customerIDNo;
    }

    public String getEdorStartDate() {
        return EdorStartDate;
    }

    public void setEdorStartDate(String edorStartDate) {
        EdorStartDate = edorStartDate;
    }

    public String getEdorEndDate() {
        return EdorEndDate;
    }

    public void setEdorEndDate(String edorEndDate) {
        EdorEndDate = edorEndDate;
    }

    @Override
    public String toString() {
        return "ReqBQProgressQuery{" +
                "ContNo='" + ContNo + '\'' +
                ", EdorType='" + EdorType + '\'' +
                ", EdorNo='" + EdorNo + '\'' +
                ", CustomerNo='" + CustomerNo + '\'' +
                ", CustomerName='" + CustomerName + '\'' +
                ", CustomerIDNo='" + CustomerIDNo + '\'' +
                ", EdorStartDate='" + EdorStartDate + '\'' +
                ", EdorEndDate='" + EdorEndDate + '\'' +
                '}';
    }
}