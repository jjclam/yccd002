package com.sinosoft.cloud.access.entity;

/**
 * @Author:lizhen
 * @Description:
 * @Date:Created in 10:51 2018/11/16
 * @Modified By:
 */

/**
 * 4003保单犹豫期退保详情查询POS017. POS100返回
 */
public class RespWTDetailsQuery {
    //退保金额
    private String OccurBala;
    //保单生效日
    private String ValidDate;
    //保单到期日
    private String ExpireDate;
    //保全项目编码
    private String EdorType;
    //保单号
    private String ContNo;
    //投保人姓名
    private String AppntName;
    //领取金额/万能账户余额/GetMoney/账户价值/追加保费金额/退保金额
    private String GetMoney;
    //开户xxx
    private String NewBankName;
    //xxx帐号
    private String NewBankAccNo;
    //户名
    private String NewBankAccName;
    //手机电话号码
    private String MobilePhone;
    //申请日期
    private String ApplyDate;
    //主险编码
    private String MainRiskCode;
    //主险名称
    private String MainRiskName;
    //被保人姓名
    private String InsuredName;
    //保额
    private String Amnt;
    //保费
    private String Prem;
    //份数
    private String Mult;
    //生存金领取标志/续保标记
    private String XBFlag;
    //生存金领取方式/续保方式
    private String XBPattern;
    //赔付日期/缴费对应日
    private String PayDate;
    //现金价值
    private String CashValue;
    //健康加费
    private String HealthPrem;
    //职业加费
    private String JobPrem;
    private AddtRisks AddtRisks;
    //主险
    private MainRisks MainRisks;
    public String getOccurBala() {
        return OccurBala;
    }

    public void setOccurBala(String occurBala) {
        OccurBala = occurBala;
    }

    public String getValidDate() {
        return ValidDate;
    }

    public void setValidDate(String validDate) {
        ValidDate = validDate;
    }

    public String getExpireDate() {
        return ExpireDate;
    }

    public void setExpireDate(String expireDate) {
        ExpireDate = expireDate;
    }

    public String getEdorType() {
        return EdorType;
    }

    public void setEdorType(String edorType) {
        EdorType = edorType;
    }

    public String getContNo() {
        return ContNo;
    }

    public void setContNo(String contNo) {
        ContNo = contNo;
    }

    public String getAppntName() {
        return AppntName;
    }

    public void setAppntName(String appntName) {
        AppntName = appntName;
    }

    public String getGetMoney() {
        return GetMoney;
    }

    public void setGetMoney(String getMoney) {
        GetMoney = getMoney;
    }

    public String getNewBankName() {
        return NewBankName;
    }

    public void setNewBankName(String newBankName) {
        NewBankName = newBankName;
    }

    public String getNewBankAccNo() {
        return NewBankAccNo;
    }

    public void setNewBankAccNo(String newBankAccNo) {
        NewBankAccNo = newBankAccNo;
    }

    public String getNewBankAccName() {
        return NewBankAccName;
    }

    public void setNewBankAccName(String newBankAccName) {
        NewBankAccName = newBankAccName;
    }

    public String getMobilePhone() {
        return MobilePhone;
    }

    public void setMobilePhone(String mobilePhone) {
        MobilePhone = mobilePhone;
    }

    public String getApplyDate() {
        return ApplyDate;
    }

    public void setApplyDate(String applyDate) {
        ApplyDate = applyDate;
    }

    public String getMainRiskCode() {
        return MainRiskCode;
    }

    public void setMainRiskCode(String mainRiskCode) {
        MainRiskCode = mainRiskCode;
    }

    public String getMainRiskName() {
        return MainRiskName;
    }

    public void setMainRiskName(String mainRiskName) {
        MainRiskName = mainRiskName;
    }

    public String getInsuredName() {
        return InsuredName;
    }

    public void setInsuredName(String insuredName) {
        InsuredName = insuredName;
    }

    public String getAmnt() {
        return Amnt;
    }

    public void setAmnt(String amnt) {
        Amnt = amnt;
    }

    public String getPrem() {
        return Prem;
    }

    public void setPrem(String prem) {
        Prem = prem;
    }

    public String getMult() {
        return Mult;
    }

    public void setMult(String mult) {
        Mult = mult;
    }

    public String getXBFlag() {
        return XBFlag;
    }

    public void setXBFlag(String XBFlag) {
        this.XBFlag = XBFlag;
    }

    public String getXBPattern() {
        return XBPattern;
    }

    public void setXBPattern(String XBPattern) {
        this.XBPattern = XBPattern;
    }

    public String getPayDate() {
        return PayDate;
    }

    public void setPayDate(String payDate) {
        PayDate = payDate;
    }

    public String getCashValue() {
        return CashValue;
    }

    public void setCashValue(String cashValue) {
        CashValue = cashValue;
    }

    public String getHealthPrem() {
        return HealthPrem;
    }

    public void setHealthPrem(String healthPrem) {
        HealthPrem = healthPrem;
    }

    public String getJobPrem() {
        return JobPrem;
    }

    public void setJobPrem(String jobPrem) {
        JobPrem = jobPrem;
    }

    public com.sinosoft.cloud.access.entity.AddtRisks getAddtRisks() {
        return AddtRisks;
    }

    public void setAddtRisks(com.sinosoft.cloud.access.entity.AddtRisks addtRisks) {
        AddtRisks = addtRisks;
    }

    public com.sinosoft.cloud.access.entity.MainRisks getMainRisks() {
        return MainRisks;
    }

    public void setMainRisks(com.sinosoft.cloud.access.entity.MainRisks mainRisks) {
        MainRisks = mainRisks;
    }

    @Override
    public String toString() {
        return "RespWTDetailsQuery{" +
                "OccurBala='" + OccurBala + '\'' +
                ", ValidDate='" + ValidDate + '\'' +
                ", ExpireDate='" + ExpireDate + '\'' +
                ", EdorType='" + EdorType + '\'' +
                ", ContNo='" + ContNo + '\'' +
                ", AppntName='" + AppntName + '\'' +
                ", GetMoney='" + GetMoney + '\'' +
                ", NewBankName='" + NewBankName + '\'' +
                ", NewBankAccNo='" + NewBankAccNo + '\'' +
                ", NewBankAccName='" + NewBankAccName + '\'' +
                ", MobilePhone='" + MobilePhone + '\'' +
                ", ApplyDate='" + ApplyDate + '\'' +
                ", MainRiskCode='" + MainRiskCode + '\'' +
                ", MainRiskName='" + MainRiskName + '\'' +
                ", InsuredName='" + InsuredName + '\'' +
                ", Amnt='" + Amnt + '\'' +
                ", Prem='" + Prem + '\'' +
                ", Mult='" + Mult + '\'' +
                ", XBFlag='" + XBFlag + '\'' +
                ", XBPattern='" + XBPattern + '\'' +
                ", PayDate='" + PayDate + '\'' +
                ", CashValue='" + CashValue + '\'' +
                ", HealthPrem='" + HealthPrem + '\'' +
                ", JobPrem='" + JobPrem + '\'' +
                ", AddtRisks=" + AddtRisks +
                ", MainRisks=" + MainRisks +
                '}';
    }
}