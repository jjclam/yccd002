package com.sinosoft.cloud.access.entity;

/**
 * @Description
 * @Author:zhaoshulei
 * @Date:2018/4/11
 */
public class LKTransStatus {
    private long TransStatusID;
    private String ShardingID;
    private String TransCode;
    private String ReportNo;
    private String BankCode;
    private String BankBranch;
    private String BankNode;
    private String BankOperator;
    private String TransNo;
    private String FuncFlag;
    private String TransDate;
    private String TransTime;
    private String ManageCom;
    private String RiskCode;
    private String ProposalNo;
    private String PrtNo;
    private String PolNo;
    private String EdorNo;
    private String TempFeeNo;
    private double TransAmnt;
    private String BankAcc;
    private String RCode;
    private String TransStatus;
    private String Status;
    private String Descr;
    private String Temp;
    private String MakeDate;
    private String MakeTime;
    private String ModifyDate;
    private String ModifyTime;
    private String State_Code;
    private String RequestId;
    private String OutServiceCode;
    private String ClientIP;
    private String ClientPort;
    private String IssueWay;
    private String ServiceStartTime;
    private String ServiceEndTime;
    private String RBankVSMP;
    private String DesBankVSMP;
    private String RMPVSKernel;
    private String DesMPVSKernel;
    private String ResultBalance;
    private String DesBalance;
    private String bak1;
    private String bak2;
    private String bak3;
    private String bak4;

    public long getTransStatusID() {
        return TransStatusID;
    }

    public void setTransStatusID(long transStatusID) {
        TransStatusID = transStatusID;
    }

    public String getShardingID() {
        return ShardingID;
    }

    public void setShardingID(String shardingID) {
        ShardingID = shardingID;
    }

    public String getTransCode() {
        return TransCode;
    }

    public void setTransCode(String transCode) {
        TransCode = transCode;
    }

    public String getReportNo() {
        return ReportNo;
    }

    public void setReportNo(String reportNo) {
        ReportNo = reportNo;
    }

    public String getBankCode() {
        return BankCode;
    }

    public void setBankCode(String bankCode) {
        BankCode = bankCode;
    }

    public String getBankBranch() {
        return BankBranch;
    }

    public void setBankBranch(String bankBranch) {
        BankBranch = bankBranch;
    }

    public String getBankNode() {
        return BankNode;
    }

    public void setBankNode(String bankNode) {
        BankNode = bankNode;
    }

    public String getBankOperator() {
        return BankOperator;
    }

    public void setBankOperator(String bankOperator) {
        BankOperator = bankOperator;
    }

    public String getTransNo() {
        return TransNo;
    }

    public void setTransNo(String transNo) {
        TransNo = transNo;
    }

    public String getFuncFlag() {
        return FuncFlag;
    }

    public void setFuncFlag(String funcFlag) {
        FuncFlag = funcFlag;
    }

    public String getTransDate() {
        return TransDate;
    }

    public void setTransDate(String transDate) {
        TransDate = transDate;
    }

    public String getTransTime() {
        return TransTime;
    }

    public void setTransTime(String transTime) {
        TransTime = transTime;
    }

    public String getManageCom() {
        return ManageCom;
    }

    public void setManageCom(String manageCom) {
        ManageCom = manageCom;
    }

    public String getRiskCode() {
        return RiskCode;
    }

    public void setRiskCode(String riskCode) {
        RiskCode = riskCode;
    }

    public String getProposalNo() {
        return ProposalNo;
    }

    public void setProposalNo(String proposalNo) {
        ProposalNo = proposalNo;
    }

    public String getPrtNo() {
        return PrtNo;
    }

    public void setPrtNo(String prtNo) {
        PrtNo = prtNo;
    }

    public String getPolNo() {
        return PolNo;
    }

    public void setPolNo(String polNo) {
        PolNo = polNo;
    }

    public String getEdorNo() {
        return EdorNo;
    }

    public void setEdorNo(String edorNo) {
        EdorNo = edorNo;
    }

    public String getTempFeeNo() {
        return TempFeeNo;
    }

    public void setTempFeeNo(String tempFeeNo) {
        TempFeeNo = tempFeeNo;
    }

    public double getTransAmnt() {
        return TransAmnt;
    }

    public void setTransAmnt(double transAmnt) {
        TransAmnt = transAmnt;
    }

    public String getBankAcc() {
        return BankAcc;
    }

    public void setBankAcc(String bankAcc) {
        BankAcc = bankAcc;
    }

    public String getRCode() {
        return RCode;
    }

    public void setRCode(String RCode) {
        this.RCode = RCode;
    }

    public String getTransStatus() {
        return TransStatus;
    }

    public void setTransStatus(String transStatus) {
        TransStatus = transStatus;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

    public String getDescr() {
        return Descr;
    }

    public void setDescr(String descr) {
        Descr = descr;
    }

    public String getTemp() {
        return Temp;
    }

    public void setTemp(String temp) {
        Temp = temp;
    }

    public String getMakeDate() {
        return MakeDate;
    }

    public void setMakeDate(String makeDate) {
        MakeDate = makeDate;
    }

    public String getMakeTime() {
        return MakeTime;
    }

    public void setMakeTime(String makeTime) {
        MakeTime = makeTime;
    }

    public String getModifyDate() {
        return ModifyDate;
    }

    public void setModifyDate(String modifyDate) {
        ModifyDate = modifyDate;
    }

    public String getModifyTime() {
        return ModifyTime;
    }

    public void setModifyTime(String modifyTime) {
        ModifyTime = modifyTime;
    }

    public String getState_Code() {
        return State_Code;
    }

    public void setState_Code(String state_Code) {
        State_Code = state_Code;
    }

    public String getRequestId() {
        return RequestId;
    }

    public void setRequestId(String requestId) {
        RequestId = requestId;
    }

    public String getOutServiceCode() {
        return OutServiceCode;
    }

    public void setOutServiceCode(String outServiceCode) {
        OutServiceCode = outServiceCode;
    }

    public String getClientIP() {
        return ClientIP;
    }

    public void setClientIP(String clientIP) {
        ClientIP = clientIP;
    }

    public String getClientPort() {
        return ClientPort;
    }

    public void setClientPort(String clientPort) {
        ClientPort = clientPort;
    }

    public String getIssueWay() {
        return IssueWay;
    }

    public void setIssueWay(String issueWay) {
        IssueWay = issueWay;
    }

    public String getServiceStartTime() {
        return ServiceStartTime;
    }

    public void setServiceStartTime(String serviceStartTime) {
        ServiceStartTime = serviceStartTime;
    }

    public String getServiceEndTime() {
        return ServiceEndTime;
    }

    public void setServiceEndTime(String serviceEndTime) {
        ServiceEndTime = serviceEndTime;
    }

    public String getRBankVSMP() {
        return RBankVSMP;
    }

    public void setRBankVSMP(String RBankVSMP) {
        this.RBankVSMP = RBankVSMP;
    }

    public String getDesBankVSMP() {
        return DesBankVSMP;
    }

    public void setDesBankVSMP(String desBankVSMP) {
        DesBankVSMP = desBankVSMP;
    }

    public String getRMPVSKernel() {
        return RMPVSKernel;
    }

    public void setRMPVSKernel(String RMPVSKernel) {
        this.RMPVSKernel = RMPVSKernel;
    }

    public String getDesMPVSKernel() {
        return DesMPVSKernel;
    }

    public void setDesMPVSKernel(String desMPVSKernel) {
        DesMPVSKernel = desMPVSKernel;
    }

    public String getResultBalance() {
        return ResultBalance;
    }

    public void setResultBalance(String resultBalance) {
        ResultBalance = resultBalance;
    }

    public String getDesBalance() {
        return DesBalance;
    }

    public void setDesBalance(String desBalance) {
        DesBalance = desBalance;
    }

    public String getBak1() {
        return bak1;
    }

    public void setBak1(String bak1) {
        this.bak1 = bak1;
    }

    public String getBak2() {
        return bak2;
    }

    public void setBak2(String bak2) {
        this.bak2 = bak2;
    }

    public String getBak3() {
        return bak3;
    }

    public void setBak3(String bak3) {
        this.bak3 = bak3;
    }

    public String getBak4() {
        return bak4;
    }

    public void setBak4(String bak4) {
        this.bak4 = bak4;
    }

    @Override
    public String toString() {
        return "LKTransStatus{" +
                "TransStatusID=" + TransStatusID +
                ", ShardingID='" + ShardingID + '\'' +
                ", TransCode='" + TransCode + '\'' +
                ", ReportNo='" + ReportNo + '\'' +
                ", BankCode='" + BankCode + '\'' +
                ", BankBranch='" + BankBranch + '\'' +
                ", BankNode='" + BankNode + '\'' +
                ", BankOperator='" + BankOperator + '\'' +
                ", TransNo='" + TransNo + '\'' +
                ", FuncFlag='" + FuncFlag + '\'' +
                ", TransDate='" + TransDate + '\'' +
                ", TransTime='" + TransTime + '\'' +
                ", ManageCom='" + ManageCom + '\'' +
                ", RiskCode='" + RiskCode + '\'' +
                ", ProposalNo='" + ProposalNo + '\'' +
                ", PrtNo='" + PrtNo + '\'' +
                ", PolNo='" + PolNo + '\'' +
                ", EdorNo='" + EdorNo + '\'' +
                ", TempFeeNo='" + TempFeeNo + '\'' +
                ", TransAmnt=" + TransAmnt +
                ", BankAcc='" + BankAcc + '\'' +
                ", RCode='" + RCode + '\'' +
                ", TransStatus='" + TransStatus + '\'' +
                ", Status='" + Status + '\'' +
                ", Descr='" + Descr + '\'' +
                ", Temp='" + Temp + '\'' +
                ", MakeDate='" + MakeDate + '\'' +
                ", MakeTime='" + MakeTime + '\'' +
                ", ModifyDate='" + ModifyDate + '\'' +
                ", ModifyTime='" + ModifyTime + '\'' +
                ", State_Code='" + State_Code + '\'' +
                ", RequestId='" + RequestId + '\'' +
                ", OutServiceCode='" + OutServiceCode + '\'' +
                ", ClientIP='" + ClientIP + '\'' +
                ", ClientPort='" + ClientPort + '\'' +
                ", IssueWay='" + IssueWay + '\'' +
                ", ServiceStartTime='" + ServiceStartTime + '\'' +
                ", ServiceEndTime='" + ServiceEndTime + '\'' +
                ", RBankVSMP='" + RBankVSMP + '\'' +
                ", DesBankVSMP='" + DesBankVSMP + '\'' +
                ", RMPVSKernel='" + RMPVSKernel + '\'' +
                ", DesMPVSKernel='" + DesMPVSKernel + '\'' +
                ", ResultBalance='" + ResultBalance + '\'' +
                ", DesBalance='" + DesBalance + '\'' +
                ", bak1='" + bak1 + '\'' +
                ", bak2='" + bak2 + '\'' +
                ", bak3='" + bak3 + '\'' +
                ", bak4='" + bak4 + '\'' +
                '}';
    }
}
