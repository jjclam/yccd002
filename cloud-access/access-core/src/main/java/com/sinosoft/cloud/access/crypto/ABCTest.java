package com.sinosoft.cloud.access.crypto;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

public class ABCTest {

    private String ivParameter = "xxx返回A..ANIHCBA";

    // private static String cacertpath;
    // static{
    // BankConfig config = BankConfig.getInstence("ABC");
    // cacertpath = config.getValueByKey("KeyFile");
    // }

    /**
     * @param args
     */
    public static void main(String[] args) {
        System.out.println("杨明");
//        ABCAESSecurity tABCAESSecurity = new ABCAESSecurity();
//        try {
//            String cSrc = "<ABCB2I><Header><TransDate>20140217</TransDate><TransTime>100418</TransTime><TransCode>1006</TransCode><SerialNo>710000003038</SerialNo><BankCode></BankCode><CorpNo>4518</CorpNo><TransSide>1</TransSide><EntrustWay>11</EntrustWay><ProvCode>11</ProvCode><BranchNo>0102</BranchNo></Header><App><Req><Appl><Name>郑新芳</Name><IDKind>110001</IDKind><IDCode>142424198612094514</IDCode><Phone>01070513942</Phone><CellPhone></CellPhone><Address>阿打发打发点发大水发第三方</Address><ZipCode>100073</ZipCode></Appl><PolicyApplyNo>1006080123000458</PolicyApplyNo><RiskCode>BELA</RiskCode><ProdCode>110001</ProdCode><LoanContact></LoanContact><SpecialRate></SpecialRate><Prem>5000000.00</Prem><AccNo>6228450018010609178</AccNo><Reserve1></Reserve1></Req></App></ABCB2I>";
//            System.out.println(cSrc.length());
////            System.out.println(new String(cSrc.getBytes("UTF-8")));
//            java.io.InputStream in = new ByteArrayInputStream(cSrc.getBytes("GBK"));
//            // tABCAESSecurity.generateKey();
//            InputStream enc = tABCAESSecurity.encrypt(in);
//
//            System.out.println("加密后：" + tABCAESSecurity.Stream2String(enc));
//
//            InputStream dec = tABCAESSecurity.decrypt(enc);
//
//            System.out.println("解密后：" + tABCAESSecurity.Stream2String(dec));
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
    }

////    @Override
//    public InputStream encrypt(InputStream in, IMessageManager iMessageManager) throws YbtException {
//        return this.encrypt(in, iMessageManager.getBaseInfo().getBankConfig());
//    }
//
//    @Override
//    public InputStream decrypt(InputStream in, IMessageManager iMessageManager)
//            throws IOException, YbtException {
//        return this.decrypt(in, iMessageManager.getBaseInfo().getBankConfig());
//    }

    /**
     * 加密
     *
     * @param data 待加密数据
     * @param key  密钥
     * @return String 加密数据
     * @throws YbtException
     * @throws NoSuchPaddingException
     * @throws NoSuchAlgorithmException
     * @throws InvalidKeyException
     * @throws BadPaddingException
     * @throws IllegalBlockSizeException
     * @throws InvalidAlgorithmParameterException
     */
    private InputStream encrypt(InputStream resp){

        int c = 0;
        ByteArrayOutputStream bous = new ByteArrayOutputStream();

        try {
            while ((c = resp.read()) != -1) {
                bous.write(c);
            }

            byte[] content = bous.toByteArray();
            int length1 = content.length;
            System.out.println("传进来的报文是：" + new String(content));
            System.out.println("传进来的报文长度是：" + length1);
            //补齐16的整数倍
            byte[] msg;
            if (length1 % 16 != 0) {
                byte[] s1 = new byte[16 - length1 % 16];
                byte arr1[] = (" ").getBytes("UTF-8");
                for (int j = 0; j < s1.length; j++) {
                    s1[j] = arr1[0];
                }
                // 将两个数组合并到一个
                msg = new byte[content.length + s1.length];
                System.arraycopy(content, 0, msg, 0, content.length);
                System.arraycopy(s1, 0, msg, content.length,
                        s1.length);
            } else {
                msg = content;
            }


            String key = "dD5ZSg1ijCwF3GWG";
//			System.out.println("密钥："+key);
            // 还原密钥
            SecretKeySpec skeySpec = new SecretKeySpec(key.getBytes(), "AES");
            // 实例化
            Cipher ci = Cipher.getInstance("AES/CBC/NoPadding");
            // 初始化,设置为加密模式
            IvParameterSpec iv = new IvParameterSpec(ivParameter.getBytes());// 使用CBC模式，需要一个向量iv，可增加加密算法的强度
            ci.init(Cipher.ENCRYPT_MODE, skeySpec, iv);
            // 执行操作
            byte[] encbyte = ci.doFinal(msg);
            System.out.println("加密后报文长度：" + encbyte.length);
            System.out.println("加密后报文：" + parseByte2HexStr(encbyte));
            byte[] enc = parseByte2HexStr(encbyte).getBytes();
            ByteArrayInputStream cipherStream = new ByteArrayInputStream(
                    enc, 0, enc.length);
            bous.close();
            return cipherStream;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * 将二进制转换成16进制
     *
     * @param buf
     * @return
     */
    public static String parseByte2HexStr(byte buf[]) {
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < buf.length; i++) {
            String hex = Integer.toHexString(buf[i] & 0xFF);
            if (hex.length() == 1) {
                hex = '0' + hex;
            }
            sb.append(hex.toUpperCase());
        }
        return sb.toString();
    }

    /**
     * 将16转换成二进制进制
     *
     * @param buf
     * @return
     */
    public static byte[] hex2byte(String strhex) {
        if (strhex == null) {
            return null;
        }
        int l = strhex.length();
        if (l % 2 == 1) {
            return null;
        }
        byte[] b = new byte[l / 2];
        for (int i = 0; i != l / 2; i++) {
            b[i] = (byte) Integer.parseInt(strhex.substring(i * 2, i * 2 + 2),
                    16);
        }
        return b;
    }

    /**
     * 解密
     *
     * @param data 待解密数据
     * @param key  密钥
     * @return String 解密数据
     * @throws YbtException
     */
    private InputStream decrypt(InputStream res) {
        try {
            int m = 0;
            ByteArrayOutputStream bous1 = new ByteArrayOutputStream();

            while ((m = res.read()) != -1) {
                bous1.write(m);
            }

            byte[] bPack = bous1.toByteArray();

            byte[] bPack1 = hex2byte(new String(bPack, "UTF-8"));

            String key = "dD5ZSg1ijCwF3GWG";

            // 还原密钥
            SecretKeySpec skeySpec = new SecretKeySpec(key.getBytes(), "AES");
            // 实例化
            Cipher c1 = Cipher.getInstance("AES/CBC/NoPadding");
            // 初始化,设置为解密模式
            IvParameterSpec iv = new IvParameterSpec(ivParameter.getBytes());// 使用CBC模式，需要一个向量iv，可增加加密算法的强度
            c1.init(Cipher.DECRYPT_MODE, skeySpec, iv);
            // 执行操作
            byte[] decbyte = c1.doFinal(bPack1);
            // 解密数据转为字符串输出
            ByteArrayInputStream cipherStream = new ByteArrayInputStream(
                    decbyte, 0, decbyte.length);
            bous1.close();
            System.out.println("解密后：" + Stream2String(cipherStream));
            return cipherStream;

        } catch (Exception e) {
            return null;
        }
    }

    private String Stream2String(InputStream in) {
        int c = 0;
        ByteArrayOutputStream bous = new ByteArrayOutputStream();

        try {
            while ((c = in.read()) != -1) {
                bous.write(c);
            }
            in.reset();
        } catch (IOException e) {
            e.printStackTrace();
        }
        byte[] bPack = bous.toByteArray();
        String ss = "";
        ss = new String(bPack);
        return ss;
    }

}
