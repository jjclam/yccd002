package com.sinosoft.cloud.access.entity;

/**
 * @Author:lizhen
 * @Description:
 * @Date:Created in 16:44 2018/11/16
 * @Modified By:
 */

/**
 * 客户联系地址或邮箱变更保全申请POS078 请求
 */
public class ReqCustomerAddressEmailChangeApply {
    //保全项目编码
    private String EdorType;
    //保单层或客户层变更标记
    private String BussFlag;
    //收付费方式
    private String PayMode;
    //合同号码
    private String ContNo;
    private EdorAppInfo EdorAppInfo;
    private PersonInfo PersonInfo;
    private BankInfo BankInfo;
    //保单查询信息
    private PolicyQueryInfos PolicyQueryInfos;

    public void setEdorType(String edorType) {
        EdorType = edorType;
    }

    public void setBussFlag(String bussFlag) {
        BussFlag = bussFlag;
    }

    public void setPayMode(String payMode) {
        PayMode = payMode;
    }

    public void setContNo(String contNo) {
        ContNo = contNo;
    }

    public String getEdorType() {
        return EdorType;
    }

    public String getBussFlag() {
        return BussFlag;
    }

    public String getPayMode() {
        return PayMode;
    }

    public String getContNo() {
        return ContNo;
    }

    public void setEdorAppInfo(com.sinosoft.cloud.access.entity.EdorAppInfo edorAppInfo) {
        EdorAppInfo = edorAppInfo;
    }

    public void setPersonInfo(com.sinosoft.cloud.access.entity.PersonInfo personInfo) {
        PersonInfo = personInfo;
    }

    public void setBankInfo(com.sinosoft.cloud.access.entity.BankInfo bankInfo) {
        BankInfo = bankInfo;
    }

    public void setPolicyQueryInfos(com.sinosoft.cloud.access.entity.PolicyQueryInfos policyQueryInfos) {
        PolicyQueryInfos = policyQueryInfos;
    }

    public com.sinosoft.cloud.access.entity.EdorAppInfo getEdorAppInfo() {
        return EdorAppInfo;
    }

    public com.sinosoft.cloud.access.entity.PersonInfo getPersonInfo() {
        return PersonInfo;
    }

    public com.sinosoft.cloud.access.entity.BankInfo getBankInfo() {
        return BankInfo;
    }

    public com.sinosoft.cloud.access.entity.PolicyQueryInfos getPolicyQueryInfos() {
        return PolicyQueryInfos;
    }

    @Override
    public String toString() {
        return "ReqCustomerAddressEmailChangeApply{" +
                "EdorType='" + EdorType + '\'' +
                ", BussFlag='" + BussFlag + '\'' +
                ", PayMode='" + PayMode + '\'' +
                ", ContNo='" + ContNo + '\'' +
                ", EdorAppInfo=" + EdorAppInfo +
                ", PersonInfo=" + PersonInfo +
                ", BankInfo=" + BankInfo +
                ", PolicyQueryInfos=" + PolicyQueryInfos +
                '}';
    }
}