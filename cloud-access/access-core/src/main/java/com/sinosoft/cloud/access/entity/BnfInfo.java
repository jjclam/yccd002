package com.sinosoft.cloud.access.entity;

/**
 * abc-cloud-access
 *
 * @author lizhen
 * @create 2019/4/12
 */
public class BnfInfo {
    //姓名
    private String Name;
    //性别
    private String Sex;
    //生日
    private String Birthday;
    //证件类型
    private String IdType;
    //证件号码
    private String IdNo;

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getSex() {
        return Sex;
    }

    public void setSex(String sex) {
        Sex = sex;
    }

    public String getBirthday() {
        return Birthday;
    }

    public void setBirthday(String birthday) {
        Birthday = birthday;
    }

    public String getIdType() {
        return IdType;
    }

    public void setIdType(String idType) {
        IdType = idType;
    }

    public String getIdNo() {
        return IdNo;
    }

    public void setIdNo(String idNo) {
        IdNo = idNo;
    }

    @Override
    public String toString() {
        return "BnfInfo{" +
                "Name='" + Name + '\'' +
                ", Sex='" + Sex + '\'' +
                ", Birthday='" + Birthday + '\'' +
                ", IdType='" + IdType + '\'' +
                ", IdNo='" + IdNo + '\'' +
                '}';
    }
}