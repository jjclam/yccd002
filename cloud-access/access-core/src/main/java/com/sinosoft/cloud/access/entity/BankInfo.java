package com.sinosoft.cloud.access.entity;

/**
 * @Author:lizhen
 * @Description:
 * @Date:Created in 17:53 2018/11/9
 * @Modified By:
 */
public class BankInfo {
    private String BankAccNo;
    private String BankCode;
    private String AccName;

    public String getBankAccNo() {
        return BankAccNo;
    }

    public void setBankAccNo(String bankAccNo) {
        BankAccNo = bankAccNo;
    }

    public String getBankCode() {
        return BankCode;
    }

    public void setBankCode(String bankCode) {
        BankCode = bankCode;
    }

    public String getAccName() {
        return AccName;
    }

    public void setAccName(String accName) {
        AccName = accName;
    }

    @Override
    public String toString() {
        return "BankInfo{" +
                "BankAccNo='" + BankAccNo + '\'' +
                ", BankCode='" + BankCode + '\'' +
                ", AccName='" + AccName + '\'' +
                '}';
    }
}