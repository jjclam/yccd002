package com.sinosoft.cloud.access.entity;

/**
 * @program: abc-cloud-access
 * @description:
 * @author: BaoYongmeng
 * @create: 2019-03-07 17:03
 **/
public class Agent{

    //代理人编码
    private String AgentCode;
    //网点代码
    private String ManageCom;
    //柜员代码
    private String TellerNo;
    //xxx代码
    private String BankCode;
    //省市代码
    private String ZoneNo;
    //网点号
    private String BrNo;

    @Override
    public String toString() {
        return "Agent{" +
                "AgentCode='" + AgentCode + '\'' +
                ", ManageCom='" + ManageCom + '\'' +
                ", TellerNo='" + TellerNo + '\'' +
                ", BankCode='" + BankCode + '\'' +
                ", ZoneNo='" + ZoneNo + '\'' +
                ", BrNo='" + BrNo + '\'' +
                '}';
    }

    public String getAgentCode() {
        return AgentCode;
    }

    public void setAgentCode(String agentCode) {
        AgentCode = agentCode;
    }

    public String getManageCom() {
        return ManageCom;
    }

    public void setManageCom(String manageCom) {
        ManageCom = manageCom;
    }

    public String getTellerNo() {
        return TellerNo;
    }

    public void setTellerNo(String tellerNo) {
        TellerNo = tellerNo;
    }

    public String getBankCode() {
        return BankCode;
    }

    public void setBankCode(String bankCode) {
        BankCode = bankCode;
    }

    public String getZoneNo() {
        return ZoneNo;
    }

    public void setZoneNo(String zoneNo) {
        ZoneNo = zoneNo;
    }

    public String getBrNo() {
        return BrNo;
    }

    public void setBrNo(String brNo) {
        BrNo = brNo;
    }
}
