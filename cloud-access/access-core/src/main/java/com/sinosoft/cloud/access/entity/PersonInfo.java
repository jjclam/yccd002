package com.sinosoft.cloud.access.entity;

/**
 * @Author:lizhen
 * @Description:
 * @Date:Created in 17:44 2018/11/9
 * @Modified By:
 */
public class PersonInfo {
    private String PersonName;
    private String PersonIDType;
    private String PersonIDNO;
    private String PersonIDValiDate;

    public String getPersonName() {
        return PersonName;
    }

    public void setPersonName(String personName) {
        PersonName = personName;
    }

    public String getPersonIDType() {
        return PersonIDType;
    }

    public void setPersonIDType(String personIDType) {
        PersonIDType = personIDType;
    }

    public String getPersonIDNO() {
        return PersonIDNO;
    }

    public void setPersonIDNO(String personIDNO) {
        PersonIDNO = personIDNO;
    }

    public String getPersonIDValiDate() {
        return PersonIDValiDate;
    }

    public void setPersonIDValiDate(String personIDValiDate) {
        PersonIDValiDate = personIDValiDate;
    }

    @Override
    public String toString() {
        return "PersonInfo{" +
                "PersonName='" + PersonName + '\'' +
                ", PersonIDType='" + PersonIDType + '\'' +
                ", PersonIDNO='" + PersonIDNO + '\'' +
                ", PersonIDValiDate='" + PersonIDValiDate + '\'' +
                '}';
    }
}