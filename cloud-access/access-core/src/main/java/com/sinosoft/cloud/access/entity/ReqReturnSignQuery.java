package com.sinosoft.cloud.access.entity;

/**
 * 回执签收查询NB002. 请求报文
 */
public class ReqReturnSignQuery {
    //保单号
    private String ContNo;
    //业务类型
    private String BussType;
    //证件姓名
    private String CustomerName;
    //证件类型
    private String CustomerIdType;
    //证件号码
    private String CustomerIdNo;

    public String getContNo() {
        return ContNo;
    }

    public void setContNo(String contNo) {
        ContNo = contNo;
    }

    public String getBussType() {
        return BussType;
    }

    public void setBussType(String bussType) {
        BussType = bussType;
    }

    public String getCustomerName() {
        return CustomerName;
    }

    public void setCustomerName(String customerName) {
        CustomerName = customerName;
    }

    public String getCustomerIdType() {
        return CustomerIdType;
    }

    public void setCustomerIdType(String customerIdType) {
        CustomerIdType = customerIdType;
    }

    public String getCustomerIdNo() {
        return CustomerIdNo;
    }

    public void setCustomerIdNo(String customerIdNo) {
        CustomerIdNo = customerIdNo;
    }

    @Override
    public String toString() {
        return "ReqReturnSignQuery{" +
                "ContNo='" + ContNo + '\'' +
                ", BussType='" + BussType + '\'' +
                ", CustomerName='" + CustomerName + '\'' +
                ", CustomerIdType='" + CustomerIdType + '\'' +
                ", CustomerIdNo='" + CustomerIdNo + '\'' +
                '}';
    }
}