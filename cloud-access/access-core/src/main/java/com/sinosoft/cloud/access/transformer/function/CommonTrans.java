package com.sinosoft.cloud.access.transformer.function;

import com.sinosoft.cloud.rest.TradeInfo;
import com.sinosoft.lis.entity.GlobalPojo;
import com.sinosoft.lis.entity.LCAppntPojo;
import com.sinosoft.lis.entity.LCContPojo;
import com.sinosoft.lis.entity.LCInsuredPojo;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;

import java.util.ArrayList;
import java.util.List;

public class CommonTrans {
    public TradeInfo xmlToObject(String msg) {
        try {
            Document tRequestDocument = DocumentHelper.parseText(msg);
            // 获取请求报文根节点Package
            Element tRootElement = tRequestDocument.getRootElement();
            Element headElement = tRootElement.element("Head");
            TradeInfo tradeInfo = getUWTrade(tRootElement,headElement);

            return tradeInfo;
        } catch (DocumentException e) {
            e.printStackTrace();
        }
        return null;
    }

    private TradeInfo getUWTrade(Element tRootElement, Element headElement)  {
        TradeInfo tradeInfo = new TradeInfo();
        LCContPojo tLCContPojo = new LCContPojo();
        GlobalPojo tGlobalPojo = new GlobalPojo();
        LCAppntPojo tLCAppntPojo = new LCAppntPojo();

        List<LCInsuredPojo> tLCInsuredList = new ArrayList();
        LCInsuredPojo tLCInsuredPojo = new LCInsuredPojo();

        tradeInfo.addData("TransDate", headElement.elementText("TransDate"));
        tradeInfo.addData("TransTime", headElement.elementText("TransTime"));
        tradeInfo.addData("ChannelCode", headElement.elementText("ChannelCode"));
        tradeInfo.addData("SerialNo", headElement.elementText("SerialNo"));
        tradeInfo.addData("TransType", headElement.elementText("TransType"));

        List<Element> requestNodes = tRootElement.element("RequestNodes").elements();
        Element requestNode = requestNodes.get(0);
        Element tApplicant = requestNode.element("Applicant");
        Element tInsured = requestNode.element("Insured");

        //Global
        tGlobalPojo.setSerialNo(headElement.elementText("SerialNo"));
        tGlobalPojo.setMainRiskCode(requestNode.elementText("ProductCode"));
        tGlobalPojo.setTransDate(headElement.elementText("TransDate"));
        tGlobalPojo.setTransTime(headElement.elementText("TransTime"));
        tGlobalPojo.setTransNo(headElement.elementText("TransType"));
        tGlobalPojo.setEntrustWay(headElement.elementText("ChannelCode"));
        tradeInfo.addData(GlobalPojo.class.getName(),tGlobalPojo);

        //LCCont
        tLCContPojo.setContType("1");
        tLCContPojo.setSellType(headElement.elementText("ChannelCode"));
        tLCContPojo.setThirdPartyOrderId(requestNode.elementText("OrderNo"));
        tLCContPojo.setAppntName(tApplicant.elementText("AppName"));
        tLCContPojo.setAppntSex(tApplicant.elementText("AppSex"));
        tLCContPojo.setAppntBirthday(tApplicant.elementText("AppBirthday"));
        tLCContPojo.setAppntIDType(tApplicant.elementText("AppIDType"));
        tLCContPojo.setAppntIDNo(tApplicant.elementText("AppIDNo"));
        tLCContPojo.setInsuredName(tApplicant.elementText("AppName"));
        tLCContPojo.setInsuredSex(tApplicant.elementText("AppSex"));
        tLCContPojo.setInsuredBirthday(tApplicant.elementText("AppBirthday"));
        tLCContPojo.setInsuredIDType(tApplicant.elementText("AppIDType"));
        tLCContPojo.setInsuredIDNo(tApplicant.elementText("AppIDNo"));
        tradeInfo.addData(LCContPojo.class.getName(),tLCContPojo);

        //LCAppnt
        tLCAppntPojo.setAppntName(tApplicant.elementText("AppName"));
        tLCAppntPojo.setAppntSex(tApplicant.elementText("AppSex"));
        tLCAppntPojo.setAppntBirthday(tApplicant.elementText("AppBirthday"));
        tLCAppntPojo.setIDType(tApplicant.elementText("AppIDType"));
        tLCAppntPojo.setIDNo(tApplicant.elementText("AppIDNo"));
        tLCAppntPojo.setIdValiDate(tApplicant.elementText("IdexpDate"));
        tLCAppntPojo.setRelatToInsu("01");
        tradeInfo.addData(LCAppntPojo.class.getName(),tLCAppntPojo);

        //LCInsured
        tLCInsuredPojo.setName(tInsured.elementText("InsuName"));
        tLCInsuredPojo.setRelationToAppnt(tInsured.elementText("Relationship"));
        tLCInsuredPojo.setSex(tInsured.elementText("InsuSex"));
        tLCInsuredPojo.setBirthday(tInsured.elementText("InsuBirthday"));
        tLCInsuredPojo.setIDType(tInsured.elementText("InsuIDType"));
        tLCInsuredPojo.setIDNo(tInsured.elementText("InsuIDNo"));
        tLCInsuredPojo.setIdValiDate(tInsured.elementText("IdexpDate"));
        tLCInsuredPojo.setIsMainInsured("1");
        tLCInsuredList.add(tLCInsuredPojo);
        tradeInfo.addData(LCInsuredPojo.class.getName(),tLCInsuredList);
        // TODO: 2020/4/24 增加获取 SLCXml节点数据

        System.out.println ("SLCXmlSchema.elementText (\"SLCXml\")"+ requestNode.elementText ("SLCXml"));
        tradeInfo.addData ("SLCXmlSchema",requestNode.elementText ("SLCXml"));
        return tradeInfo;

    }




    public String getReturnTrans(TradeInfo returnData, TradeInfo TradeInfo) {
        Document mRespDocument = DocumentHelper.createDocument();
        mRespDocument.setXMLEncoding("GBK");

        Element tPackageElement = mRespDocument.addElement("Package");
        //HEAD
        Element tResponseHeadElement = tPackageElement.addElement("Head");
        Element tResponseTransDate = tResponseHeadElement.addElement("TransDate");
        tResponseTransDate.setText((String) TradeInfo.getData("TransDate"));
        Element tResponseTransTime = tResponseHeadElement.addElement("TransTime");
        tResponseTransTime.setText((String) TradeInfo.getData("TransTime"));
        Element tResponseChannelCode = tResponseHeadElement.addElement("ChannelCode");
        tResponseChannelCode.setText((String) TradeInfo.getData("ChannelCode"));
        Element tResponseTransType = tResponseHeadElement.addElement("TransType");
        tResponseTransType.setText((String) TradeInfo.getData("TransType"));
        Element tResponseSerialNo = tResponseHeadElement.addElement("SerialNo");
        tResponseSerialNo.setText((String) TradeInfo.getData("SerialNo"));


        //BODY
        Element mResponseNodes = tPackageElement.addElement("ResponseNodes");
        Element mResponseNode = mResponseNodes.addElement("ResponseNode");
        //result
        Element mResult = mResponseNode.addElement("Result");
        Element tResultStatus = mResult.addElement("ResultStatus");
        Element tResultInfoDesc = mResult.addElement("ResultInfoDesc");
        //BusinessObject
        Element mBusinessObject = mResponseNode.addElement("BusinessObject");
//        Element policyNo = mBusinessObject.addElement("policyNo");
//        Element proposalNo = mBusinessObject.addElement("proposalNo");
//        Element signDate = mBusinessObject.addElement("signDate");
//        Element InsuranceStartPeriod = mBusinessObject.addElement("postFee");
//        Element InsuranceEndPeriod = mBusinessObject.addElement("postFee");
        Element isSuccess = mBusinessObject.addElement("isSuccess");
        Element message = mBusinessObject.addElement("message");


        String returnMsg = (String) returnData.getData("return");
        String returnFlag = (String) returnData.getData("returnFlag");
//        String returnLog = (String) tTradeInfo.getData("log");

        if(returnFlag != null && "0".equals(returnFlag)){
            tResultStatus.setText("0");
            tResultInfoDesc.setText(returnMsg);
            isSuccess.setText("0");
            message.setText("出单失败");
        }else{
            tResultStatus.setText("1");
            tResultInfoDesc.setText("交易成功");
            isSuccess.setText("1");
            message.setText("出单成功");
        }

        return mRespDocument.asXML();
    }
}
