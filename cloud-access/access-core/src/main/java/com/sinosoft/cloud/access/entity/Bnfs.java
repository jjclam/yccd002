package com.sinosoft.cloud.access.entity;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.List;

/**
 * @project: abc-cloud-access
 * @author: yangming
 * @date: 2017/9/19 下午7:09
 * To change this template use File | Settings | File and Code Templates.
 */
@XStreamAlias("Bnfs")
public class Bnfs {
    @XStreamImplicit(itemFieldName="Bnf")
    private List<Bnf> bnfs;

    public List<Bnf> getBnfs() {
        return bnfs;
    }

    public void setBnfs(List<Bnf> bnfs) {
        this.bnfs = bnfs;
    }

    @Override
    public String toString() {
        return "Bnfs{" +
                "bnfs=" + bnfs +
                '}';
    }
}
