package com.sinosoft.cloud.access.entity;

import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 * @description
 * @author:zhaoshulei
 * @date:2018/2/9
 */
@XStreamAlias("BnfDetail")
public class BnfDetail {

    @XStreamAlias("Type")
    private String type;
    @XStreamAlias("BnfName")
    private String bnfName;
    @XStreamAlias("BnfSex")
    private String bnfSex;
    @XStreamAlias("BnfBirthday")
    private String bnfBirthday;
    @XStreamAlias("BnfIDType")
    private String bnfIDType;
    @XStreamAlias("BnfIDNo")
    private String bnfIDNo;
    @XStreamAlias("BnfRelationToInsured")
    private String bnfRelationToInsured;
    @XStreamAlias("Sequence")
    private String sequence;
    @XStreamAlias("BnfLot")
    private String bnfLot;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getBnfName() {
        return bnfName;
    }

    public void setBnfName(String bnfName) {
        this.bnfName = bnfName;
    }

    public String getBnfSex() {
        return bnfSex;
    }

    public void setBnfSex(String bnfSex) {
        this.bnfSex = bnfSex;
    }

    public String getBnfBirthday() {
        return bnfBirthday;
    }

    public void setBnfBirthday(String bnfBirthday) {
        this.bnfBirthday = bnfBirthday;
    }

    public String getBnfIDType() {
        return bnfIDType;
    }

    public void setBnfIDType(String bnfIDType) {
        this.bnfIDType = bnfIDType;
    }

    public String getBnfIDNo() {
        return bnfIDNo;
    }

    public void setBnfIDNo(String bnfIDNo) {
        this.bnfIDNo = bnfIDNo;
    }

    public String getBnfRelationToInsured() {
        return bnfRelationToInsured;
    }

    public void setBnfRelationToInsured(String bnfRelationToInsured) {
        this.bnfRelationToInsured = bnfRelationToInsured;
    }

    public String getSequence() {
        return sequence;
    }

    public void setSequence(String sequence) {
        this.sequence = sequence;
    }

    public String getBnfLot() {
        return bnfLot;
    }

    public void setBnfLot(String bnfLot) {
        this.bnfLot = bnfLot;
    }

    @Override
    public String toString() {
        return "BnfDetail{" +
                "type='" + type + '\'' +
                ", bnfName='" + bnfName + '\'' +
                ", bnfSex='" + bnfSex + '\'' +
                ", bnfBirthday='" + bnfBirthday + '\'' +
                ", bnfIDType='" + bnfIDType + '\'' +
                ", bnfIDNo='" + bnfIDNo + '\'' +
                ", bnfRelationToInsured='" + bnfRelationToInsured + '\'' +
                ", sequence='" + sequence + '\'' +
                ", bnfLot='" + bnfLot + '\'' +
                '}';
    }
}
