package com.sinosoft.cloud.access.entity;

import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.StrTool;

/** 三期需要的pojo
 * @Author:lizhen
 * @Description:
 * @Date:Created in 10:03 2018/4/26
 * @Modified By:
 */
public class LKTransInfo {
    private long TransInfoID;
    private String ShardingID;
    private String SSTransCode;
    private String SuSpendTransCode;
    private String TBTransCode;
    private String NewPolicyNo;
    private String OldPolicyNo;
    private String EdorAcceptNo;
    private double OccurBala;
    private double Prem;
    private double TransferPrem;
    private String FuncFlag;
    private String TransDate;
    private String TransTime;
    private String MakeDate;
    private String MakeTime;
    private String ModifyDate;
    private String ModifyTime;
    private String Bak1;
    private String Bak2;
    private String Bak3;
    private String Bak4;
    public static final int FIELDNUM = 22;
    private FDate fDate = new FDate();

    public LKTransInfo() {
    }

    public long getTransInfoID() {
        return this.TransInfoID;
    }

    public void setTransInfoID(long aTransInfoID) {
        this.TransInfoID = aTransInfoID;
    }

    public void setTransInfoID(String aTransInfoID) {
        if(aTransInfoID != null && !aTransInfoID.equals("")) {
            this.TransInfoID = (new Long(aTransInfoID)).longValue();
        }

    }

    public String getShardingID() {
        return this.ShardingID;
    }

    public void setShardingID(String aShardingID) {
        this.ShardingID = aShardingID;
    }

    public String getSSTransCode() {
        return this.SSTransCode;
    }

    public void setSSTransCode(String aSSTransCode) {
        this.SSTransCode = aSSTransCode;
    }

    public String getSuSpendTransCode() {
        return this.SuSpendTransCode;
    }

    public void setSuSpendTransCode(String aSuSpendTransCode) {
        this.SuSpendTransCode = aSuSpendTransCode;
    }

    public String getTBTransCode() {
        return this.TBTransCode;
    }

    public void setTBTransCode(String aTBTransCode) {
        this.TBTransCode = aTBTransCode;
    }

    public String getNewPolicyNo() {
        return this.NewPolicyNo;
    }

    public void setNewPolicyNo(String aNewPolicyNo) {
        this.NewPolicyNo = aNewPolicyNo;
    }

    public String getOldPolicyNo() {
        return this.OldPolicyNo;
    }

    public void setOldPolicyNo(String aOldPolicyNo) {
        this.OldPolicyNo = aOldPolicyNo;
    }

    public String getEdorAcceptNo() {
        return this.EdorAcceptNo;
    }

    public void setEdorAcceptNo(String aEdorAcceptNo) {
        this.EdorAcceptNo = aEdorAcceptNo;
    }

    public double getOccurBala() {
        return this.OccurBala;
    }

    public void setOccurBala(double aOccurBala) {
        this.OccurBala = aOccurBala;
    }

    public void setOccurBala(String aOccurBala) {
        if(aOccurBala != null && !aOccurBala.equals("")) {
            Double tDouble = new Double(aOccurBala);
            double d = tDouble.doubleValue();
            this.OccurBala = d;
        }

    }

    public double getPrem() {
        return this.Prem;
    }

    public void setPrem(double aPrem) {
        this.Prem = aPrem;
    }

    public void setPrem(String aPrem) {
        if(aPrem != null && !aPrem.equals("")) {
            Double tDouble = new Double(aPrem);
            double d = tDouble.doubleValue();
            this.Prem = d;
        }

    }

    public double getTransferPrem() {
        return this.TransferPrem;
    }

    public void setTransferPrem(double aTransferPrem) {
        this.TransferPrem = aTransferPrem;
    }

    public void setTransferPrem(String aTransferPrem) {
        if(aTransferPrem != null && !aTransferPrem.equals("")) {
            Double tDouble = new Double(aTransferPrem);
            double d = tDouble.doubleValue();
            this.TransferPrem = d;
        }

    }

    public String getFuncFlag() {
        return this.FuncFlag;
    }

    public void setFuncFlag(String aFuncFlag) {
        this.FuncFlag = aFuncFlag;
    }

    public String getTransDate() {
        return this.TransDate;
    }

    public void setTransDate(String aTransDate) {
        this.TransDate = aTransDate;
    }

    public String getTransTime() {
        return this.TransTime;
    }

    public void setTransTime(String aTransTime) {
        this.TransTime = aTransTime;
    }

    public String getMakeDate() {
        return this.MakeDate;
    }

    public void setMakeDate(String aMakeDate) {
        this.MakeDate = aMakeDate;
    }

    public String getMakeTime() {
        return this.MakeTime;
    }

    public void setMakeTime(String aMakeTime) {
        this.MakeTime = aMakeTime;
    }

    public String getModifyDate() {
        return this.ModifyDate;
    }

    public void setModifyDate(String aModifyDate) {
        this.ModifyDate = aModifyDate;
    }

    public String getModifyTime() {
        return this.ModifyTime;
    }

    public void setModifyTime(String aModifyTime) {
        this.ModifyTime = aModifyTime;
    }

    public String getBak1() {
        return this.Bak1;
    }

    public void setBak1(String aBak1) {
        this.Bak1 = aBak1;
    }

    public String getBak2() {
        return this.Bak2;
    }

    public void setBak2(String aBak2) {
        this.Bak2 = aBak2;
    }

    public String getBak3() {
        return this.Bak3;
    }

    public void setBak3(String aBak3) {
        this.Bak3 = aBak3;
    }

    public String getBak4() {
        return this.Bak4;
    }

    public void setBak4(String aBak4) {
        this.Bak4 = aBak4;
    }

    public int getFieldCount() {
        return 22;
    }

    public int getFieldIndex(String strFieldName) {
        return strFieldName.equals("TransInfoID")?0:(strFieldName.equals("ShardingID")?1:(strFieldName.equals("SSTransCode")?2:(strFieldName.equals("SuSpendTransCode")?3:(strFieldName.equals("TBTransCode")?4:(strFieldName.equals("NewPolicyNo")?5:(strFieldName.equals("OldPolicyNo")?6:(strFieldName.equals("EdorAcceptNo")?7:(strFieldName.equals("OccurBala")?8:(strFieldName.equals("Prem")?9:(strFieldName.equals("TransferPrem")?10:(strFieldName.equals("FuncFlag")?11:(strFieldName.equals("TransDate")?12:(strFieldName.equals("TransTime")?13:(strFieldName.equals("MakeDate")?14:(strFieldName.equals("MakeTime")?15:(strFieldName.equals("ModifyDate")?16:(strFieldName.equals("ModifyTime")?17:(strFieldName.equals("Bak1")?18:(strFieldName.equals("Bak2")?19:(strFieldName.equals("Bak3")?20:(strFieldName.equals("Bak4")?21:-1)))))))))))))))))))));
    }

    public String getFieldName(int nFieldIndex) {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "TransInfoID";
                break;
            case 1:
                strFieldName = "ShardingID";
                break;
            case 2:
                strFieldName = "SSTransCode";
                break;
            case 3:
                strFieldName = "SuSpendTransCode";
                break;
            case 4:
                strFieldName = "TBTransCode";
                break;
            case 5:
                strFieldName = "NewPolicyNo";
                break;
            case 6:
                strFieldName = "OldPolicyNo";
                break;
            case 7:
                strFieldName = "EdorAcceptNo";
                break;
            case 8:
                strFieldName = "OccurBala";
                break;
            case 9:
                strFieldName = "Prem";
                break;
            case 10:
                strFieldName = "TransferPrem";
                break;
            case 11:
                strFieldName = "FuncFlag";
                break;
            case 12:
                strFieldName = "TransDate";
                break;
            case 13:
                strFieldName = "TransTime";
                break;
            case 14:
                strFieldName = "MakeDate";
                break;
            case 15:
                strFieldName = "MakeTime";
                break;
            case 16:
                strFieldName = "ModifyDate";
                break;
            case 17:
                strFieldName = "ModifyTime";
                break;
            case 18:
                strFieldName = "Bak1";
                break;
            case 19:
                strFieldName = "Bak2";
                break;
            case 20:
                strFieldName = "Bak3";
                break;
            case 21:
                strFieldName = "Bak4";
                break;
            default:
                strFieldName = "";
        }

        return strFieldName;
    }

    public int getFieldType(String strFieldName) {
        String var2 = strFieldName.toUpperCase();
        byte var3 = -1;
        switch(var2.hashCode()) {
            case -2131816400:
                if(var2.equals("FUNCFLAG")) {
                    var3 = 11;
                }
                break;
            case -1322448033:
                if(var2.equals("SHARDINGID")) {
                    var3 = 1;
                }
                break;
            case -1160843833:
                if(var2.equals("TBTRANSCODE")) {
                    var3 = 4;
                }
                break;
            case -455778762:
                if(var2.equals("TRANSDATE")) {
                    var3 = 12;
                }
                break;
            case -455294635:
                if(var2.equals("TRANSTIME")) {
                    var3 = 13;
                }
                break;
            case -402994189:
                if(var2.equals("NEWPOLICYNO")) {
                    var3 = 5;
                }
                break;
            case -186200103:
                if(var2.equals("SUSPENDTRANSCODE")) {
                    var3 = 3;
                }
                break;
            case 2031045:
                if(var2.equals("BAK1")) {
                    var3 = 18;
                }
                break;
            case 2031046:
                if(var2.equals("BAK2")) {
                    var3 = 19;
                }
                break;
            case 2031047:
                if(var2.equals("BAK3")) {
                    var3 = 20;
                }
                break;
            case 2031048:
                if(var2.equals("BAK4")) {
                    var3 = 21;
                }
                break;
            case 2464298:
                if(var2.equals("PREM")) {
                    var3 = 9;
                }
                break;
            case 174609557:
                if(var2.equals("TRANSFERPREM")) {
                    var3 = 10;
                }
                break;
            case 238020305:
                if(var2.equals("TRANSINFOID")) {
                    var3 = 0;
                }
                break;
            case 701323642:
                if(var2.equals("OLDPOLICYNO")) {
                    var3 = 6;
                }
                break;
            case 787158411:
                if(var2.equals("EDORACCEPTNO")) {
                    var3 = 7;
                }
                break;
            case 823606940:
                if(var2.equals("MAKEDATE")) {
                    var3 = 14;
                }
                break;
            case 824091067:
                if(var2.equals("MAKETIME")) {
                    var3 = 15;
                }
                break;
            case 1590345237:
                if(var2.equals("SSTRANSCODE")) {
                    var3 = 2;
                }
                break;
            case 1593635776:
                if(var2.equals("OCCURBALA")) {
                    var3 = 8;
                }
                break;
            case 1696950120:
                if(var2.equals("MODIFYDATE")) {
                    var3 = 16;
                }
                break;
            case 1697434247:
                if(var2.equals("MODIFYTIME")) {
                    var3 = 17;
                }
        }

        switch(var3) {
            case 0:
                return 7;
            case 1:
                return 0;
            case 2:
                return 0;
            case 3:
                return 0;
            case 4:
                return 0;
            case 5:
                return 0;
            case 6:
                return 0;
            case 7:
                return 0;
            case 8:
                return 4;
            case 9:
                return 4;
            case 10:
                return 4;
            case 11:
                return 0;
            case 12:
                return 0;
            case 13:
                return 0;
            case 14:
                return 0;
            case 15:
                return 0;
            case 16:
                return 0;
            case 17:
                return 0;
            case 18:
                return 0;
            case 19:
                return 0;
            case 20:
                return 0;
            case 21:
                return 0;
            default:
                return -1;
        }
    }

    public int getFieldType(int nFieldIndex) {
        switch(nFieldIndex) {
            case 0:
                return 7;
            case 1:
                return 0;
            case 2:
                return 0;
            case 3:
                return 0;
            case 4:
                return 0;
            case 5:
                return 0;
            case 6:
                return 0;
            case 7:
                return 0;
            case 8:
                return 4;
            case 9:
                return 4;
            case 10:
                return 4;
            case 11:
                return 0;
            case 12:
                return 0;
            case 13:
                return 0;
            case 14:
                return 0;
            case 15:
                return 0;
            case 16:
                return 0;
            case 17:
                return 0;
            case 18:
                return 0;
            case 19:
                return 0;
            case 20:
                return 0;
            case 21:
                return 0;
            default:
                return -1;
        }
    }

    public String getV(String FCode) {
        String strReturn = "";
        if(FCode.equalsIgnoreCase("TransInfoID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.TransInfoID));
        }

        if(FCode.equalsIgnoreCase("ShardingID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.ShardingID));
        }

        if(FCode.equalsIgnoreCase("SSTransCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.SSTransCode));
        }

        if(FCode.equalsIgnoreCase("SuSpendTransCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.SuSpendTransCode));
        }

        if(FCode.equalsIgnoreCase("TBTransCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.TBTransCode));
        }

        if(FCode.equalsIgnoreCase("NewPolicyNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.NewPolicyNo));
        }

        if(FCode.equalsIgnoreCase("OldPolicyNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.OldPolicyNo));
        }

        if(FCode.equalsIgnoreCase("EdorAcceptNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.EdorAcceptNo));
        }

        if(FCode.equalsIgnoreCase("OccurBala")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.OccurBala));
        }

        if(FCode.equalsIgnoreCase("Prem")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.Prem));
        }

        if(FCode.equalsIgnoreCase("TransferPrem")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.TransferPrem));
        }

        if(FCode.equalsIgnoreCase("FuncFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.FuncFlag));
        }

        if(FCode.equalsIgnoreCase("TransDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.TransDate));
        }

        if(FCode.equalsIgnoreCase("TransTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.TransTime));
        }

        if(FCode.equalsIgnoreCase("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.MakeDate));
        }

        if(FCode.equalsIgnoreCase("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.MakeTime));
        }

        if(FCode.equalsIgnoreCase("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.ModifyDate));
        }

        if(FCode.equalsIgnoreCase("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.ModifyTime));
        }

        if(FCode.equalsIgnoreCase("Bak1")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.Bak1));
        }

        if(FCode.equalsIgnoreCase("Bak2")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.Bak2));
        }

        if(FCode.equalsIgnoreCase("Bak3")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.Bak3));
        }

        if(FCode.equalsIgnoreCase("Bak4")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.Bak4));
        }

        if(strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }

    public String getV(int nFieldIndex) {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = String.valueOf(this.TransInfoID);
                break;
            case 1:
                strFieldValue = String.valueOf(this.ShardingID);
                break;
            case 2:
                strFieldValue = String.valueOf(this.SSTransCode);
                break;
            case 3:
                strFieldValue = String.valueOf(this.SuSpendTransCode);
                break;
            case 4:
                strFieldValue = String.valueOf(this.TBTransCode);
                break;
            case 5:
                strFieldValue = String.valueOf(this.NewPolicyNo);
                break;
            case 6:
                strFieldValue = String.valueOf(this.OldPolicyNo);
                break;
            case 7:
                strFieldValue = String.valueOf(this.EdorAcceptNo);
                break;
            case 8:
                strFieldValue = String.valueOf(this.OccurBala);
                break;
            case 9:
                strFieldValue = String.valueOf(this.Prem);
                break;
            case 10:
                strFieldValue = String.valueOf(this.TransferPrem);
                break;
            case 11:
                strFieldValue = String.valueOf(this.FuncFlag);
                break;
            case 12:
                strFieldValue = String.valueOf(this.TransDate);
                break;
            case 13:
                strFieldValue = String.valueOf(this.TransTime);
                break;
            case 14:
                strFieldValue = String.valueOf(this.MakeDate);
                break;
            case 15:
                strFieldValue = String.valueOf(this.MakeTime);
                break;
            case 16:
                strFieldValue = String.valueOf(this.ModifyDate);
                break;
            case 17:
                strFieldValue = String.valueOf(this.ModifyTime);
                break;
            case 18:
                strFieldValue = String.valueOf(this.Bak1);
                break;
            case 19:
                strFieldValue = String.valueOf(this.Bak2);
                break;
            case 20:
                strFieldValue = String.valueOf(this.Bak3);
                break;
            case 21:
                strFieldValue = String.valueOf(this.Bak4);
                break;
            default:
                strFieldValue = "";
        }

        if(strFieldValue.equals("")) {
            strFieldValue = "null";
        }

        return strFieldValue;
    }

    public boolean setV(String FCode, String FValue) {
        if(StrTool.cTrim(FCode).equals("")) {
            return false;
        } else {
            if(FCode.equalsIgnoreCase("TransInfoID") && FValue != null && !FValue.equals("")) {
                this.TransInfoID = (new Long(FValue)).longValue();
            }

            if(FCode.equalsIgnoreCase("ShardingID")) {
                if(FValue != null && !FValue.equals("")) {
                    this.ShardingID = FValue.trim();
                } else {
                    this.ShardingID = null;
                }
            }

            if(FCode.equalsIgnoreCase("SSTransCode")) {
                if(FValue != null && !FValue.equals("")) {
                    this.SSTransCode = FValue.trim();
                } else {
                    this.SSTransCode = null;
                }
            }

            if(FCode.equalsIgnoreCase("SuSpendTransCode")) {
                if(FValue != null && !FValue.equals("")) {
                    this.SuSpendTransCode = FValue.trim();
                } else {
                    this.SuSpendTransCode = null;
                }
            }

            if(FCode.equalsIgnoreCase("TBTransCode")) {
                if(FValue != null && !FValue.equals("")) {
                    this.TBTransCode = FValue.trim();
                } else {
                    this.TBTransCode = null;
                }
            }

            if(FCode.equalsIgnoreCase("NewPolicyNo")) {
                if(FValue != null && !FValue.equals("")) {
                    this.NewPolicyNo = FValue.trim();
                } else {
                    this.NewPolicyNo = null;
                }
            }

            if(FCode.equalsIgnoreCase("OldPolicyNo")) {
                if(FValue != null && !FValue.equals("")) {
                    this.OldPolicyNo = FValue.trim();
                } else {
                    this.OldPolicyNo = null;
                }
            }

            if(FCode.equalsIgnoreCase("EdorAcceptNo")) {
                if(FValue != null && !FValue.equals("")) {
                    this.EdorAcceptNo = FValue.trim();
                } else {
                    this.EdorAcceptNo = null;
                }
            }

            Double tDouble;
            double d;
            if(FCode.equalsIgnoreCase("OccurBala") && FValue != null && !FValue.equals("")) {
                tDouble = new Double(FValue);
                d = tDouble.doubleValue();
                this.OccurBala = d;
            }

            if(FCode.equalsIgnoreCase("Prem") && FValue != null && !FValue.equals("")) {
                tDouble = new Double(FValue);
                d = tDouble.doubleValue();
                this.Prem = d;
            }

            if(FCode.equalsIgnoreCase("TransferPrem") && FValue != null && !FValue.equals("")) {
                tDouble = new Double(FValue);
                d = tDouble.doubleValue();
                this.TransferPrem = d;
            }

            if(FCode.equalsIgnoreCase("FuncFlag")) {
                if(FValue != null && !FValue.equals("")) {
                    this.FuncFlag = FValue.trim();
                } else {
                    this.FuncFlag = null;
                }
            }

            if(FCode.equalsIgnoreCase("TransDate")) {
                if(FValue != null && !FValue.equals("")) {
                    this.TransDate = FValue.trim();
                } else {
                    this.TransDate = null;
                }
            }

            if(FCode.equalsIgnoreCase("TransTime")) {
                if(FValue != null && !FValue.equals("")) {
                    this.TransTime = FValue.trim();
                } else {
                    this.TransTime = null;
                }
            }

            if(FCode.equalsIgnoreCase("MakeDate")) {
                if(FValue != null && !FValue.equals("")) {
                    this.MakeDate = FValue.trim();
                } else {
                    this.MakeDate = null;
                }
            }

            if(FCode.equalsIgnoreCase("MakeTime")) {
                if(FValue != null && !FValue.equals("")) {
                    this.MakeTime = FValue.trim();
                } else {
                    this.MakeTime = null;
                }
            }

            if(FCode.equalsIgnoreCase("ModifyDate")) {
                if(FValue != null && !FValue.equals("")) {
                    this.ModifyDate = FValue.trim();
                } else {
                    this.ModifyDate = null;
                }
            }

            if(FCode.equalsIgnoreCase("ModifyTime")) {
                if(FValue != null && !FValue.equals("")) {
                    this.ModifyTime = FValue.trim();
                } else {
                    this.ModifyTime = null;
                }
            }

            if(FCode.equalsIgnoreCase("Bak1")) {
                if(FValue != null && !FValue.equals("")) {
                    this.Bak1 = FValue.trim();
                } else {
                    this.Bak1 = null;
                }
            }

            if(FCode.equalsIgnoreCase("Bak2")) {
                if(FValue != null && !FValue.equals("")) {
                    this.Bak2 = FValue.trim();
                } else {
                    this.Bak2 = null;
                }
            }

            if(FCode.equalsIgnoreCase("Bak3")) {
                if(FValue != null && !FValue.equals("")) {
                    this.Bak3 = FValue.trim();
                } else {
                    this.Bak3 = null;
                }
            }

            if(FCode.equalsIgnoreCase("Bak4")) {
                if(FValue != null && !FValue.equals("")) {
                    this.Bak4 = FValue.trim();
                } else {
                    this.Bak4 = null;
                }
            }

            return true;
        }
    }


    @Override
    public String toString() {
        return "LKTransInfoPojo [TransInfoID=" + this.TransInfoID + ", ShardingID=" + this.ShardingID + ", SSTransCode=" + this.SSTransCode + ", SuSpendTransCode=" + this.SuSpendTransCode + ", TBTransCode=" + this.TBTransCode + ", NewPolicyNo=" + this.NewPolicyNo + ", OldPolicyNo=" + this.OldPolicyNo + ", EdorAcceptNo=" + this.EdorAcceptNo + ", OccurBala=" + this.OccurBala + ", Prem=" + this.Prem + ", TransferPrem=" + this.TransferPrem + ", FuncFlag=" + this.FuncFlag + ", TransDate=" + this.TransDate + ", TransTime=" + this.TransTime + ", MakeDate=" + this.MakeDate + ", MakeTime=" + this.MakeTime + ", ModifyDate=" + this.ModifyDate + ", ModifyTime=" + this.ModifyTime + ", Bak1=" + this.Bak1 + ", Bak2=" + this.Bak2 + ", Bak3=" + this.Bak3 + ", Bak4=" + this.Bak4 + "]";
    }

}