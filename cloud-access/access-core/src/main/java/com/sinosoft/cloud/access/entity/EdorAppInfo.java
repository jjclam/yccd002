package com.sinosoft.cloud.access.entity;

/**
 * @Author:lizhen
 * @Description:
 * @Date:Created in 17:42 2018/11/9
 * @Modified By:
 */
/**
 * P0S018需要
 */
public class EdorAppInfo {

    private String EdorAppName;
    private String EdorAppIDType;
    private String EdorAppIDNO;
    private String EdorAppIdValiDate;
    private String EdorAppPhone;

    public String getEdorAppName() {
        return EdorAppName;
    }

    public void setEdorAppName(String edorAppName) {
        EdorAppName = edorAppName;
    }

    public String getEdorAppIDType() {
        return EdorAppIDType;
    }

    public void setEdorAppIDType(String edorAppIDType) {
        EdorAppIDType = edorAppIDType;
    }

    public String getEdorAppIDNO() {
        return EdorAppIDNO;
    }

    public void setEdorAppIDNO(String edorAppIDNO) {
        EdorAppIDNO = edorAppIDNO;
    }

    public String getEdorAppIdValiDate() {
        return EdorAppIdValiDate;
    }

    public void setEdorAppIdValiDate(String edorAppIdValiDate) {
        EdorAppIdValiDate = edorAppIdValiDate;
    }

    public String getEdorAppPhone() {
        return EdorAppPhone;
    }

    public void setEdorAppPhone(String edorAppPhone) {
        EdorAppPhone = edorAppPhone;
    }

    @Override
    public String toString() {
        return "EdorAppInfo{" +
                "EdorAppName='" + EdorAppName + '\'' +
                ", EdorAppIDType='" + EdorAppIDType + '\'' +
                ", EdorAppIDNO='" + EdorAppIDNO + '\'' +
                ", EdorAppIdValiDate='" + EdorAppIdValiDate + '\'' +
                ", EdorAppPhone='" + EdorAppPhone + '\'' +
                '}';
    }
}
