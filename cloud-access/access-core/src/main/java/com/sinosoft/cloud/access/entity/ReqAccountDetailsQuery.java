package com.sinosoft.cloud.access.entity;
/**
 * 万能账户价值详情查询POS033.
 */
public class ReqAccountDetailsQuery {
    //保全项目编码
    private String EdorType;
    //保单号
    private String ContNo;

    public String getEdorType() {
        return EdorType;
    }

    public void setEdorType(String edorType) {
        EdorType = edorType;
    }

    public String getContNo() {
        return ContNo;
    }

    public void setContNo(String contNo) {
        ContNo = contNo;
    }

    @Override
    public String toString() {
        return "ReqAccountDetailsQuery{" +
                "EdorType='" + EdorType + '\'' +
                ", ContNo='" + ContNo + '\'' +
                '}';
    }
}
