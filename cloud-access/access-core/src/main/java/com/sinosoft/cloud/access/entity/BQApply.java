package com.sinosoft.cloud.access.entity;

/**
 * @program: abc-cloud-access
 * @description: 转换类
 * @author: BaoYongmeng
 * @create: 2018-04-12 18:27
 **/
public class BQApply {

    private String AppNo;
    private String PolicyNo;
    private String IDNo;
    private String IDType;
    private String PrintCode;
    private String ClientName;
    private String PolicyPwd;
    private String PayeetName;
    private String PayeeIdKind;
    private String PayeeIdCode;
    private String PayAcc;
    private double Amt;
    private String BusiType;
    private double GetAmt;
    private String PicFileName;
    /**
     * 保全申请方式
     */
    private String AppType;

    public void setAppType(String appType) {
        AppType = appType;
    }

    public String getAppType() {
        return AppType;
    }

    public String getAppNo() {
        return AppNo;
    }

    public String getPolicyNo() {
        return PolicyNo;
    }

    public String getIDNo() {
        return IDNo;
    }

    public String getIDType() {
        return IDType;
    }

    public String getPrintCode() {
        return PrintCode;
    }

    public String getClientName() {
        return ClientName;
    }

    public String getPolicyPwd() {
        return PolicyPwd;
    }

    public String getPayeetName() {
        return PayeetName;
    }

    public String getPayeeIdKind() {
        return PayeeIdKind;
    }

    public String getPayeeIdCode() {
        return PayeeIdCode;
    }

    public String getPayAcc() {
        return PayAcc;
    }

    public double getAmt() {
        return Amt;
    }

    public String getBusiType() {
        return BusiType;
    }

    public double getGetAmt() {
        return GetAmt;
    }

    public String getPicFileName() {
        return PicFileName;
    }

    public void setAppNo(String appNo) {
        AppNo = appNo;
    }

    public void setPolicyNo(String policyNo) {
        PolicyNo = policyNo;
    }

    public void setIDNo(String IDNo) {
        this.IDNo = IDNo;
    }

    public void setIDType(String IDType) {
        this.IDType = IDType;
    }

    public void setPrintCode(String printCode) {
        PrintCode = printCode;
    }

    public void setClientName(String clientName) {
        ClientName = clientName;
    }

    public void setPolicyPwd(String policyPwd) {
        PolicyPwd = policyPwd;
    }

    public void setPayeetName(String payeetName) {
        PayeetName = payeetName;
    }

    public void setPayeeIdKind(String payeeIdKind) {
        PayeeIdKind = payeeIdKind;
    }

    public void setPayeeIdCode(String payeeIdCode) {
        PayeeIdCode = payeeIdCode;
    }

    public void setPayAcc(String payAcc) {
        PayAcc = payAcc;
    }

    public void setAmt(double amt) {
        Amt = amt;
    }

    public void setBusiType(String busiType) {
        BusiType = busiType;
    }

    public void setGetAmt(double getAmt) {
        GetAmt = getAmt;
    }

    public void setPicFileName(String picFileName) {
        PicFileName = picFileName;
    }

    @Override
    public String toString() {
        return "BQApply{" +
                "AppNo='" + AppNo + '\'' +
                ", PolicyNo='" + PolicyNo + '\'' +
                ", IDNo='" + IDNo + '\'' +
                ", IDType='" + IDType + '\'' +
                ", PrintCode='" + PrintCode + '\'' +
                ", ClientName='" + ClientName + '\'' +
                ", PolicyPwd='" + PolicyPwd + '\'' +
                ", PayeetName='" + PayeetName + '\'' +
                ", PayeeIdKind='" + PayeeIdKind + '\'' +
                ", PayeeIdCode='" + PayeeIdCode + '\'' +
                ", PayAcc='" + PayAcc + '\'' +
                ", Amt=" + Amt +
                ", BusiType='" + BusiType + '\'' +
                ", GetAmt=" + GetAmt +
                ", PicFileName='" + PicFileName + '\'' +
                ", AppType='" + AppType + '\'' +
                '}';
    }
}
