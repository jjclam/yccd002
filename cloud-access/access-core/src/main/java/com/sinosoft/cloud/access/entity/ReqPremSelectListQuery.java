package com.sinosoft.cloud.access.entity;

/**
 * @Author:lizhen
 * @Description:
 * @Date:Created in 23:02 2018/11/13
 * @Modified By:
 */

/**
 * 4002保费自垫选择权变更列表查询POS010,POS098. 请求
 */
public class ReqPremSelectListQuery {
    //保全项目编码
    private String EdorType;
    //客户号
    private String CustomerNo;
    //证件号
    private String CustomerIDNo;
    //证件姓名
    private String CustomerName;

    public String getEdorType() {
        return EdorType;
    }

    public void setEdorType(String edorType) {
        EdorType = edorType;
    }

    public String getCustomerNo() {
        return CustomerNo;
    }

    public void setCustomerNo(String customerNo) {
        CustomerNo = customerNo;
    }

    public String getCustomerIDNo() {
        return CustomerIDNo;
    }

    public void setCustomerIDNo(String customerIDNo) {
        CustomerIDNo = customerIDNo;
    }

    public String getCustomerName() {
        return CustomerName;
    }

    public void setCustomerName(String customerName) {
        CustomerName = customerName;
    }

    @Override
    public String toString() {
        return "ReqPremSelectListQuery{" +
                "EdorType='" + EdorType + '\'' +
                ", CustomerNo='" + CustomerNo + '\'' +
                ", CustomerIDNo='" + CustomerIDNo + '\'' +
                ", CustomerName='" + CustomerName + '\'' +
                '}';
    }
}