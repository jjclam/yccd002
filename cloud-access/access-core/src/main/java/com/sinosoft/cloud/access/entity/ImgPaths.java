package com.sinosoft.cloud.access.entity;

/**
 * @Author:lizhen
 * @Description:
 * @Date:Created in 17:44 2018/11/9
 * @Modified By:
 */
public class ImgPaths {

    //单证类型
    private String SubType;
    //影像路径
    private String ImgPath;
    //影像名称
    private String ImgName;

    public String getSubType() {
        return SubType;
    }

    public void setSubType(String subType) {
        SubType = subType;
    }

    public String getImgPath() {
        return ImgPath;
    }

    public void setImgPath(String imgPath) {
        ImgPath = imgPath;
    }

    public String getImgName() {
        return ImgName;
    }

    public void setImgName(String imgName) {
        ImgName = imgName;
    }

    @Override
    public String toString() {
        return "ImgPaths{" +
                "SubType='" + SubType + '\'' +
                ", ImgPath='" + ImgPath + '\'' +
                ", ImgName='" + ImgName + '\'' +
                '}';
    }
}