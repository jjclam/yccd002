package com.sinosoft.cloud.access.util;

/**
 * Created by Administrator on 2017-9-14.
 */

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;


/**
 * @Author: 崔广东
 * @Date: 2017-9-27 10:37
 * @Description: 代码转换的工具类
 */
public class CodeTransformUtil {

    private static final Log cLogger = LogFactory.getLog(CodeTransformUtil.class);

    /**
     * 用于投保人和被保人之间的关系转换
     * 由于xxx传来的数据是投保人与被保人关系，而微服务需要被保人与投保人关系，所以需要关系转换
     * 传入投保人与被保人关系，返回被保人与投保人关系
     * @param rela-投保人与被保人的关系
     * @param sex 被保人性别
     * @return 被保人与投保人的关系
     */
    public static String appRela2InsuRela(String rela,String sex){
        if ("0".equals(sex)){
                if("00".equals(rela)){return "00";}
				if("01".equals(rela)){return "02";}
				if("02".equals(rela)){return "01";}
				if("03".equals(rela)){return "05";}
				if("04".equals(rela)){return "05";}
				if("05".equals(rela)){return "03";}
				if("06".equals(rela)){return "03";}
				if("07".equals(rela)){return "09";}
				if("08".equals(rela)){return "09";}
				if("09".equals(rela)){return "07";}
				if("10".equals(rela)){return "07";}
				if("11".equals(rela)){return "13";}
				if("12".equals(rela)){return "13";}
				if("13".equals(rela)){return "11";}
				if("14".equals(rela)){return "11";}
				if("15".equals(rela)){return "17";}
				if("16".equals(rela)){return "17";}
				if("17".equals(rela)){return "15";}
				if("18".equals(rela)){return "15";}
				if("19".equals(rela)){return "";}
				if("20".equals(rela)){return "";}		
				if("21".equals(rela)){return "24";}
				if("22".equals(rela)){return "24";}			
				if("23".equals(rela)){return "19";}
				if("24".equals(rela)){return "21";}					
				if("25".equals(rela)){return "25";}			
				if("26".equals(rela)){return "26";}
				if("27".equals(rela)){return "27";}						
				if("28".equals(rela)){return "29";}
				if("29".equals(rela)){return "28";}						
				if("30".equals(rela)){return "30";}
        } else if ("1".equals(sex)){
                if("00".equals(rela)){return "00";}
				if("03".equals(rela)){return "06";}
				if("04".equals(rela)){return "06";}
				if("02".equals(rela)){return "01";}
				if("01".equals(rela)){return "02";}
				if("05".equals(rela)){return "04";}
				if("06".equals(rela)){return "04";}
				if("07".equals(rela)){return "10";}
				if("08".equals(rela)){return "10";}
				if("09".equals(rela)){return "08";}
				if("10".equals(rela)){return "08";}
				if("11".equals(rela)){return "14";}
				if("12".equals(rela)){return "14";}
				if("13".equals(rela)){return "12";}
				if("14".equals(rela)){return "12";}
				if("15".equals(rela)){return "18";}
				if("16".equals(rela)){return "18";}
				if("17".equals(rela)){return "16";}
				if("18".equals(rela)){return "16";}
				if("19".equals(rela)){return "23";}
				if("20".equals(rela)){return "23";}		
				if("21".equals(rela)){return "";}
				if("22".equals(rela)){return "";}			
				if("23".equals(rela)){return "20";}
				if("24".equals(rela)){return "22";}					
				if("25".equals(rela)){return "25";}			
				if("26".equals(rela)){return "26";}
				if("27".equals(rela)){return "27";}						
				if("28".equals(rela)){return "29";}
				if("29".equals(rela)){return "28";}						
				if("30".equals(rela)){return "30";}
        }else {
            cLogger.warn("传入未知的被保人性别！");
            return "";
        }
        return "";
    }

    /**
     * 组合险种Prodsetcode模板
     * @param varRiskCode
     * @param prem
     * @param riskCode1
     * @return
     */
    public static String transformProdsetcode(String varRiskCode,String prem,String riskCode1){
        String prodsetcode="";
        if(("6017").equals(varRiskCode)){
            if(("50").equals(prem)){
                prodsetcode="C015";
            }else if(("99").equals(prem)){
                prodsetcode="C016";
            }else if(("198").equals(prem)){
                prodsetcode="C017";
            }
        }else if(("1013").equals(varRiskCode)&&("5009").equals(riskCode1)){
            prodsetcode="C029";
        }else if(("2045").equals(varRiskCode)&&("7045").equals(riskCode1)){
            prodsetcode="C030";
        }else if(("1018").equals(varRiskCode)&&("7046").equals(riskCode1)){
            prodsetcode="C031";
        }else if(("5011").equals(varRiskCode)){
            prodsetcode="C034";
        }else if(("5012").equals(varRiskCode)){
            prodsetcode="C035";
        }
        return prodsetcode;
    }
}
