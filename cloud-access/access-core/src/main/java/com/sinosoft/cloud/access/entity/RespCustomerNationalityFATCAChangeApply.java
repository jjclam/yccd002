package com.sinosoft.cloud.access.entity;

/**
 * @Author:lizhen
 * @Description:
 * @Date:Created in 16:35 2018/11/16
 * @Modified By:
 */
/**
 * 客户国籍及FATCA变更保全申请 pos077 返回
 */
public class RespCustomerNationalityFATCAChangeApply {
    //保全项目编码
    private String EdorType;
    //保单查询信息
    private PolicyQueryInfos PolicyQueryInfos;

    public String getEdorType() {
        return EdorType;
    }

    public void setEdorType(String edorType) {
        EdorType = edorType;
    }

    public com.sinosoft.cloud.access.entity.PolicyQueryInfos getPolicyQueryInfos() {
        return PolicyQueryInfos;
    }

    public void setPolicyQueryInfos(com.sinosoft.cloud.access.entity.PolicyQueryInfos policyQueryInfos) {
        PolicyQueryInfos = policyQueryInfos;
    }

    @Override
    public String toString() {
        return "RespCustomerNationalityFATCAChangeApply{" +
                "EdorType='" + EdorType + '\'' +
                ", PolicyQueryInfos=" + PolicyQueryInfos +
                '}';
    }
}