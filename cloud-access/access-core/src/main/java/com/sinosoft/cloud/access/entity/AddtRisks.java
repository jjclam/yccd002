package com.sinosoft.cloud.access.entity;


import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.List;

/**
 * @Author:lizhen
 * @Description:
 * @Date:Created in 14:00 2018/11/14
 * @Modified By:
 */
public class AddtRisks {
    @XStreamImplicit(itemFieldName="AddtRisk")
    private List<AddtRisk> AddtRisk;
    private String Count;

    public List<AddtRisk> getAddtRisk() {
        return AddtRisk;
    }

    public void setAddtRisk(List<AddtRisk> addtRisk) {
        AddtRisk = addtRisk;
    }

    public String getCount() {
        return Count;
    }

    public void setCount(String count) {
        Count = count;
    }

    @Override
    public String toString() {
        return "AddtRisks{" +
                "AddtRisk=" + AddtRisk +
                ", Count='" + Count + '\'' +
                '}';
    }
}