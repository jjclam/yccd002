package com.sinosoft.cloud.access.configuration;


import com.sinosoft.cloud.access.annotations.*;
import com.sinosoft.cloud.access.crypto.CryptoBean;
import com.sinosoft.cloud.access.crypto.CryptoFactory;
import com.sinosoft.cloud.access.net.*;
import com.sinosoft.cloud.access.router.RouterBean;
import com.sinosoft.cloud.access.router.RouterFactory;
import com.sinosoft.cloud.access.service.AccessServiceBean;
import com.sinosoft.cloud.access.service.AccessServiceGate;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.codec.http.HttpObjectAggregator;
import io.netty.handler.codec.http.HttpRequestDecoder;
import io.netty.handler.codec.http.HttpResponseEncoder;
import io.netty.handler.logging.LogLevel;
import io.netty.handler.logging.LoggingHandler;
import io.netty.handler.stream.ChunkedWriteHandler;
import org.glassfish.jersey.netty.httpserver.NettyHttpContainerProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.core.env.Environment;
import org.springframework.core.task.TaskExecutor;
import org.springframework.jms.annotation.EnableJms;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import javax.annotation.Resource;
import java.net.URI;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * @Author: yangming
 * @Date: 2017/9/1 15:05
 * @Description:
 */
@SpringBootConfiguration
@ComponentScan("com.sinosoft.cloud.access")
@EnableAsync
@EnableCaching
@EnableJms
public class AccessConfiguration {


    @Autowired
    private ConfigurableListableBeanFactory beanFactory;

    public static final String BASE_PACKAGE = "com/sinosoft/cloud/access/";


    @Autowired
    RestfulConfiguration restfulConfiguration;

    /**
     * webService处理类
     */
    @Autowired
    WebServiceEndpoint webServiceEndpoint;

    /**
     * 线程池
     * 此线程池用于承载Socket、SOAP等Server的线程容器。
     *
     * @return
     */
    @Bean
    public TaskExecutor taskExecutor() {
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setCorePoolSize(5);
        executor.setMaxPoolSize(10);
        executor.setQueueCapacity(25);
        return executor;
    }


    /**
     * 线程池
     * 此线程池用于承载Socket、SOAP等Server的线程容器。
     *
     * @return
     */
    @Bean
    public ThreadPoolExecutor threadPoolExecutor() {

        ThreadPoolExecutor executorService = new ThreadPoolExecutor(0, Integer.MAX_VALUE,
                60L, TimeUnit.SECONDS,
                new SynchronousQueue<Runnable>());
        return executorService;
    }

    /**
     * 存放所有access的map
     *
     * @param taskExecutor 线程池
     * @return
     */
    @Bean(name = "accessMap")
    public Map initServer(TaskExecutor taskExecutor) {

        HashMap<String, Access> accessMap = new HashMap<>();

        Map<String, Object> accessChannelBean = beanFactory.getBeansWithAnnotation(AccessChannel.class);
        for (String beanName : accessChannelBean.keySet()) {
            AccessChannel channel = beanFactory.findAnnotationOnBean(beanName, AccessChannel.class);
            Access access = (Access) accessChannelBean.get(beanName);
            accessMap.put(channel.name(), access);
            taskExecutor.execute(new Runnable() {
                @Override
                public void run() {
                    startServer(channel, access);
                }
            });
        }

        return accessMap;
    }

    /**
     * 存放所有处理类service的map
     *
     * @return
     */
    @Bean
    public AccessServiceGate accessServiceGate() {
        Map<String, Object> accessServiceBeans = beanFactory.getBeansWithAnnotation(AccessService.class);

        AccessServiceGate accessServiceGate = AccessServiceGate.getInstance();
        for (String beanName : accessServiceBeans.keySet()) {
            AccessService accessService = beanFactory.findAnnotationOnBean(beanName, AccessService.class);
            String name = accessService.name();
            String transCode = accessService.transCode();
            accessServiceGate.addService(name, transCode, (AccessServiceBean) accessServiceBeans.get(beanName));
        }

        return accessServiceGate;
    }

    /**
     * 用于加解密处理
     *
     * @return
     * @author yangming
     * @date 2017年10月19日
     */
    @Bean
    public CryptoFactory cryptoFactory() {
        Map<String, Object> cryptoBeanMap = beanFactory.getBeansWithAnnotation(AccessCrypto.class);
        CryptoFactory cryptoFactory = CryptoFactory.getInstance();
        for (String beanName : cryptoBeanMap.keySet()) {
            AccessCrypto accessCrypto = beanFactory.findAnnotationOnBean(beanName, AccessCrypto.class);
            String name = accessCrypto.name();
            cryptoFactory.addCrypto(name, (CryptoBean) cryptoBeanMap.get(beanName));
        }

        return cryptoFactory;
    }

    /**
     * 用于路由机制处理
     * <p>
     * 在application.properties中可以配置 head body两种路由条件
     *
     * @return
     */
    @Bean
    public RouterFactory routerFactory() {
        Map<String, Object> routerBeanMap = beanFactory.getBeansWithAnnotation(AccessRouter.class);
        RouterFactory routerFactory = RouterFactory.getInstance();
        for (String beanName : routerBeanMap.keySet()) {
            AccessRouter accessRouter = beanFactory.findAnnotationOnBean(beanName, AccessRouter.class);
            String name = accessRouter.name();
            routerFactory.addRouter(name, (RouterBean) routerBeanMap.get(beanName));
        }

        return routerFactory;
    }

    /**
     * 此处主要是解决Socket收到消息的完整性的解码器.
     * 一般来说, socket 消息(xxx发送的) 都会包含一个
     * 消息Head, 这个头里面 会包含整个消息体的长度.
     * 因此需要不断校验收到消息体的完整性问题.
     *
     * @return decoderFactory
     * @author: Yangming
     * @date: 2017年10月19日
     * @see access-abc中com.sinosoft.cloud.access.abc.net.AbcDecoder
     */
    @Bean
    public DecoderFactory decoderFactory() {
        Map<String, Object> decoderBean = beanFactory.getBeansWithAnnotation(AccessCoder.class);
        DecoderFactory decoderFactory = DecoderFactory.getInstance();
        for (String beanName : decoderBean.keySet()) {
            AccessCoder accessCoder = beanFactory.findAnnotationOnBean(beanName, AccessCoder.class);
            String name = accessCoder.name();
            decoderFactory.addDecoder(name, (Coder) decoderBean.get(beanName));
        }
        return decoderFactory;
    }

    @Resource
    private Environment env;

    /**
     * socket或者http启动方法
     *
     * @param channel 渠道
     * @param access  渠道对应的配置
     */
    private void startServer(AccessChannel channel, Access access) {
        String name = channel.name();
        int port = Integer.parseInt(access.getPort());
        ProtocolType type = channel.protocol();
        switch (type) {
            case SOCKET:
                startNettyServer(name, port);
            case RESTFUL:
                startHttpServer(name, port);
            case SOAP:
                String registerPath = env.getProperty("access.soap.register.path");
                HashMap<String, Object> map = new HashMap<>();
                map.put(registerPath, webServiceEndpoint);
                System.out.println("registerPath:" + registerPath);
                System.out.println("webServiceEndpoint:"+webServiceEndpoint);
                startSoapServer(name, port, map);
        }
    }

    /**
     * 启动httpServer
     *
     * @param name
     * @param port
     */
    private void startHttpServer(String name, int port) {
        URI uri = null;
        uri = URI.create("http://0.0.0.0:" + port + "/");
        final Channel server = NettyHttpContainerProvider.createServer(uri, restfulConfiguration, false);
//        final Channel server = NettyHttpContainerProvider.createHttp2Server(uri, restfulConfiguration, null);
        Runtime.getRuntime().addShutdownHook(new Thread(() -> server.close()));
    }

    private void startNettyServer(String name, int port) {
        EventLoopGroup bossGroup = new NioEventLoopGroup();
        EventLoopGroup workerGroup = new NioEventLoopGroup(200, threadPoolExecutor());
//        EventLoopGroup workerGroup = new NioEventLoopGroup();
        try {
            ServerBootstrap b = new ServerBootstrap();
            b.option(ChannelOption.SO_BACKLOG, 128);
            b.option(ChannelOption.SO_SNDBUF, 1048576);
            b.option(ChannelOption.SO_RCVBUF, 1048576);
            b.group(bossGroup, workerGroup)
                    .channel(NioServerSocketChannel.class)
                    .handler(new LoggingHandler(LogLevel.INFO))
                    .childHandler(new ChannelInitializer<SocketChannel>() {
                        @Override
                        public void initChannel(SocketChannel ch) throws Exception {
                            ChannelPipeline p = ch.pipeline();
                            DefaultDecoder defaultDecoder = new DefaultDecoder();
                            defaultDecoder.setAccessName(name);
                            p.addLast(defaultDecoder);
                            Handler handler = new DefaultSocketHandler();
                            ChannelHandler channelHandler = (ChannelHandler) handler;
                            handler.setAccessName(name);
                            p.addLast(channelHandler);
                        }
                    });
            ChannelFuture f = b.bind(port).sync();
            f.channel().closeFuture().sync();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            bossGroup.shutdownGracefully();
            workerGroup.shutdownGracefully();
        }
    }

    /**
     * 启动SoapServer
     *
     * @param name
     * @param port
     */
    private void startSoapServer(String name, int port, Map<String, Object> mappings) {
        EventLoopGroup bossGroup = new NioEventLoopGroup();
        EventLoopGroup workerGroup = new NioEventLoopGroup(500, Executors.newCachedThreadPool());
        try {
            ServerBootstrap b = new ServerBootstrap();
            b.option(ChannelOption.SO_BACKLOG, 1024);
            b.option(ChannelOption.SO_SNDBUF, 1048576);
            b.option(ChannelOption.SO_RCVBUF, 1048576);
            b.group(bossGroup, workerGroup)
                    .channel(NioServerSocketChannel.class)
                    .handler(new LoggingHandler(LogLevel.INFO))
                    .childHandler(new ChannelInitializer<SocketChannel>() {
                        @Override
                        public void initChannel(SocketChannel ch) throws Exception {
                            ChannelPipeline p = ch.pipeline();
                            p.addLast(new HttpRequestDecoder());
                            p.addLast(new HttpObjectAggregator(65536));
                            p.addLast(new HttpResponseEncoder());
                            p.addLast(new ChunkedWriteHandler());
                            JaxWsHandler jaxWsHandler = new JaxWsHandler(mappings);
                            jaxWsHandler.setAccessName(name);
                            p.addLast(jaxWsHandler);
                        }
                    });
//            ChannelFuture f = b.bind(ip,port).sync();
//            f.channel().closeFuture().sync();
            ChannelFuture f = b.bind(port).sync();
            f.channel().closeFuture().sync();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            bossGroup.shutdownGracefully();
            workerGroup.shutdownGracefully();
        }
    }

}
