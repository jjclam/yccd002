package com.sinosoft.cloud.access.entity;

/**
 * abc-cloud-access
 *
 * @author lizhen
 * @create 2019/1/14
 */
public class WecLPSyncImagesRe {
    //理赔号
    private String ClaimNo;
    //状态
    private String Status;

    public String getClaimNo() {
        return ClaimNo;
    }

    public void setClaimNo(String claimNo) {
        ClaimNo = claimNo;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

    @Override
    public String toString() {
        return "WecLPSyncImagesRe{" +
                "ClaimNo='" + ClaimNo + '\'' +
                ", Status='" + Status + '\'' +
                '}';
    }
}