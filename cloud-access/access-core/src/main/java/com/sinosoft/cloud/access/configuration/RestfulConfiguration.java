package com.sinosoft.cloud.access.configuration;

import org.glassfish.jersey.logging.LoggingFeature;
import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import javax.ws.rs.ApplicationPath;

/**
 * cloud-access
 *
 * @title: cloud-access
 * @package: com.sinosoft.cloud.access.net
 * @author: yangming
 * @date: 2018/2/21 下午6:04
 */
@Component
@ApplicationPath("/openApi")
public class RestfulConfiguration extends ResourceConfig implements ApplicationContextAware {
    public RestfulConfiguration() {
        register(RestfulFilterEndpoint.class);
        register(RestfulEndpoint.class);
        register(LoggingFeature.class);
    }

    public RestfulConfiguration(ApplicationContext applicationContext) {
        property("contextConfig", applicationContext);
    }


    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        property("contextConfig", applicationContext);
    }
}
