package com.sinosoft.cloud.access.entity;

/**
 * @program: abc-cloud-access
 * @description: 客服信函服务POS093.
 * @author: BaoYongmeng
 * @create: 2018-12-24 17:16
 **/
public class ReqCustomerService {
    //客户号
    private String CustomerNo;
    //证件号
    private String CustomerIDNo;
    //证件姓名
    private String CustomerName;
    //保全类型
    private String EdorType;

    public void setCustomerNo(String customerNo) {
        CustomerNo = customerNo;
    }

    public void setCustomerIDNo(String customerIDNo) {
        CustomerIDNo = customerIDNo;
    }

    public void setCustomerName(String customerName) {
        CustomerName = customerName;
    }

    public String getCustomerNo() {
        return CustomerNo;
    }

    public String getCustomerIDNo() {
        return CustomerIDNo;
    }

    public String getCustomerName() {
        return CustomerName;
    }

    public String getEdorType() {
        return EdorType;
    }

    public void setEdorType(String edorType) {
        EdorType = edorType;
    }

    @Override
    public String toString() {
        return "ReqCustomerService{" +
                "CustomerNo='" + CustomerNo + '\'' +
                ", CustomerIDNo='" + CustomerIDNo + '\'' +
                ", CustomerName='" + CustomerName + '\'' +
                ", EdorType='" + EdorType + '\'' +
                '}';
    }
}
