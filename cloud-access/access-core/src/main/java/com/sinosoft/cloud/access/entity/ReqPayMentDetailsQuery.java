package com.sinosoft.cloud.access.entity;
/**
 * 4003万能补缴明细查询POS035.POS107
 */
public class ReqPayMentDetailsQuery {
    //保全项目编码
    private String EdorType;
    //保单号
    private String ContNo;
    //保全号
    private String EdorAcceptNo;
    public String getEdorAcceptNo() {
        return EdorAcceptNo;
    }

    public void setEdorAcceptNo(String edorAcceptNo) {
        EdorAcceptNo = edorAcceptNo;
    }

    public String getEdorType() {
        return EdorType;
    }

    public void setEdorType(String edorType) {
        EdorType = edorType;
    }

    public String getContNo() {
        return ContNo;
    }

    public void setContNo(String contNo) {
        ContNo = contNo;
    }

    @Override
    public String toString() {
        return "ReqPayMentDetailsQuery{" +
                "EdorType='" + EdorType + '\'' +
                ", ContNo='" + ContNo + '\'' +
                ", EdorAcceptNo='" + EdorAcceptNo + '\'' +
                '}';
    }
}
