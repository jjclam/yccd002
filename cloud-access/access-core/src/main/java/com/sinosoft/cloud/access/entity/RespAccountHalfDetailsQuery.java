package com.sinosoft.cloud.access.entity;

/**
 * 4003保单万能账户部分领取明细查询POS030 pos105
 */
public class RespAccountHalfDetailsQuery {
    //保全项目编码
    private String EdorType;
    //保单号
    private String ContNo;
    //投保人姓名
    private String AppntName;
    //附加险险种名称
    private String RiskName;
    //领取金额/万能账户余额/getMoney/账户价值/追加保费金额/退保金额
    private String GetMoney;
    //开户xxx
    private String NewBankName;
    //xxx帐号
    private String NewBankAccNo;
    //户名
    private String NewBankAccName;
    //手机电话号码
    private String MobilePhone;

    private AddtRisks AddtRisks;
    //申请日期
    private String ApplyDate;

    public com.sinosoft.cloud.access.entity.AddtRisks getAddtRisks() {
        return AddtRisks;
    }

    public void setAddtRisks(com.sinosoft.cloud.access.entity.AddtRisks addtRisks) {
        AddtRisks = addtRisks;
    }

    public String getEdorType() {
        return EdorType;
    }

    public void setEdorType(String edorType) {
        EdorType = edorType;
    }

    public String getContNo() {
        return ContNo;
    }

    public void setContNo(String contNo) {
        ContNo = contNo;
    }

    public String getAppntName() {
        return AppntName;
    }

    public void setAppntName(String appntName) {
        AppntName = appntName;
    }

    public String getRiskName() {
        return RiskName;
    }

    public void setRiskName(String riskName) {
        RiskName = riskName;
    }

    public String getGetMoney() {
        return GetMoney;
    }

    public void setGetMoney(String getMoney) {
        GetMoney = getMoney;
    }

    public String getNewBankName() {
        return NewBankName;
    }

    public void setNewBankName(String newBankName) {
        NewBankName = newBankName;
    }

    public String getNewBankAccNo() {
        return NewBankAccNo;
    }

    public void setNewBankAccNo(String newBankAccNo) {
        NewBankAccNo = newBankAccNo;
    }

    public String getNewBankAccName() {
        return NewBankAccName;
    }

    public void setNewBankAccName(String newBankAccName) {
        NewBankAccName = newBankAccName;
    }

    public String getMobilePhone() {
        return MobilePhone;
    }

    public void setMobilePhone(String mobilePhone) {
        MobilePhone = mobilePhone;
    }

    public String getApplyDate() {
        return ApplyDate;
    }

    public void setApplyDate(String applyDate) {
        ApplyDate = applyDate;
    }

    @Override
    public String toString() {
        return "RespAccountHalfDetailsQuery{" +
                "EdorType='" + EdorType + '\'' +
                ", ContNo='" + ContNo + '\'' +
                ", AppntName='" + AppntName + '\'' +
                ", RiskName='" + RiskName + '\'' +
                ", GetMoney='" + GetMoney + '\'' +
                ", NewBankName='" + NewBankName + '\'' +
                ", NewBankAccNo='" + NewBankAccNo + '\'' +
                ", NewBankAccName='" + NewBankAccName + '\'' +
                ", MobilePhone='" + MobilePhone + '\'' +
                ", AddtRisks=" + AddtRisks +
                ", ApplyDate='" + ApplyDate + '\'' +
                '}';
    }
}
