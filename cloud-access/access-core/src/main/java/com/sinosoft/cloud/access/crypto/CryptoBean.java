package com.sinosoft.cloud.access.crypto;

import com.sinosoft.cloud.access.annotations.AccessCrypto;
import com.sinosoft.cloud.access.annotations.CryptoType;
import com.xiaoleilu.hutool.io.FileUtil;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.core.env.Environment;
import org.springframework.core.io.ClassPathResource;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;

import static com.sinosoft.cloud.access.configuration.AccessConfiguration.BASE_PACKAGE;


public abstract class CryptoBean {

    private String accessName;
    private String key;
    private String pubKey;
    private String priKey;
    private CryptoType cryptoType;

    private static final String AES_KEY_FILE = "aes.key";
    private static final String DES_KEY_FILE = "des.key";
    private static final String PUB_KEY_FILE = "pub.key";
    private static final String PRI_KEY_FILE = "pri.key";

    /**
     * 读取全局配置文件的对象，主要用于读取是否启用加解密开关
     */
    @Autowired
    private Environment env;

    public Environment getEnv() {
        return env;
    }

    public void setEnv(Environment env) {
        this.env = env;
    }

    public abstract String encryt(String msg);

    public abstract String decryt(String msg);


    public byte[] getCryptoBody(byte[] msg, int start, int length) {
        return Arrays.copyOfRange(msg, start, length);
    }


    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getPubKey() {
        return pubKey;
    }

    public void setPubKey(String pubKey) {
        this.pubKey = pubKey;
    }

    public String getPriKey() {
        return priKey;
    }

    public void setPriKey(String priKey) {
        this.priKey = priKey;
    }

    public CryptoType getCryptoType() {
        return cryptoType;
    }

    public void setCryptoType(CryptoType cryptoType) {
        this.cryptoType = cryptoType;
    }

    public String getAccessName() {
        return accessName;
    }

    public void setAccessName(String accessName) {
        this.accessName = accessName;
    }

    @PostConstruct
    public void init() {
        AccessCrypto accessCrypto = AnnotationUtils.findAnnotation(this.getClass(), AccessCrypto.class);
        setAccessName(accessCrypto.name());
        setCryptoType(accessCrypto.cryptoType());
        initKey();
    }

    private void initKey() {
        switch (getCryptoType()) {
            case AES:
                getAesKey();
        }
    }

    private void getAesKey() {
        ClassPathResource classPathResource = new ClassPathResource(BASE_PACKAGE + accessName + "/" + AES_KEY_FILE);
        File f = null;
        try {
            InputStream stream = classPathResource.getInputStream();
            String targetFilePath = classPathResource.getFilename();

//            f = classPathResource.getFile();
            f = new File(targetFilePath);
            FileUtils.copyInputStreamToFile(stream, f);
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (f.exists()) {

            byte[] bytes = FileUtil.readBytes(f);

            if (bytes != null) {
                setKey(new String(bytes));
            }
        }
    }

}
