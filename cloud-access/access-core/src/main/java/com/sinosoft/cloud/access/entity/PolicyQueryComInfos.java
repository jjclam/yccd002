package com.sinosoft.cloud.access.entity;

import com.sinosoft.lis.pos.entity.PolicyQueryComInfoPojo;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.List;

/**
 * @Author:lizhen
 * @Description:
 * @Date:Created in 15:56 2018/11/15
 * @Modified By:
 */
public class PolicyQueryComInfos {
    //保单来源机构信息查询
    @XStreamImplicit(itemFieldName="PolicyQueryComInfo")
    private List<PolicyQueryComInfo> PolicyQueryComInfo;

    public List<PolicyQueryComInfo> getPolicyQueryComInfo() {
        return PolicyQueryComInfo;
    }

    public void setPolicyQueryComInfo(List<PolicyQueryComInfo> policyQueryComInfo) {
        PolicyQueryComInfo = policyQueryComInfo;
    }

    @Override
    public String toString() {
        return "PolicyQueryComInfos{" +
                "PolicyQueryComInfo=" + PolicyQueryComInfo +
                '}';
    }
}