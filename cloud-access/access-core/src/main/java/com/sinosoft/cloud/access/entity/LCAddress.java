package com.sinosoft.cloud.access.entity;

/**
 * Created by Administrator on 2018/4/11.
 */
public class LCAddress {
    private long AddressID;
    private long PersonID;
    private String ShardingID;
    private String CustomerNo;
    private int AddressNo;
    private String PostalAddress;
    private String ZipCode;
    private String Phone;
    private String Fax;
    private String HomeAddress;
    private String HomeZipCode;
    private String HomePhone;
    private String HomeFax;
    private String CompanyAddress;
    private String CompanyZipCode;
    private String CompanyPhone;
    private String CompanyFax;
    private String Mobile;
    private String MobileChs;
    private String EMail;
    private String BP;
    private String Mobile2;
    private String MobileChs2;
    private String EMail2;
    private String BP2;
    private String Operator;
    private String MakeDate;
    private String MakeTime;
    private String ModifyDate;
    private String ModifyTime;
    private String GrpName;
    private String Province;
    private String City;
    private String County;
    private String ZoneCode;
    private String StoreNo;
    private String StoreNo2;

    public long getAddressID() {
        return AddressID;
    }

    public void setAddressID(long addressID) {
        AddressID = addressID;
    }

    public long getPersonID() {
        return PersonID;
    }

    public void setPersonID(long personID) {
        PersonID = personID;
    }

    public String getShardingID() {
        return ShardingID;
    }

    public void setShardingID(String shardingID) {
        ShardingID = shardingID;
    }

    public String getCustomerNo() {
        return CustomerNo;
    }

    public void setCustomerNo(String customerNo) {
        CustomerNo = customerNo;
    }

    public int getAddressNo() {
        return AddressNo;
    }

    public void setAddressNo(int addressNo) {
        AddressNo = addressNo;
    }

    public String getPostalAddress() {
        return PostalAddress;
    }

    public void setPostalAddress(String postalAddress) {
        PostalAddress = postalAddress;
    }

    public String getZipCode() {
        return ZipCode;
    }

    public void setZipCode(String zipCode) {
        ZipCode = zipCode;
    }

    public String getPhone() {
        return Phone;
    }

    public void setPhone(String phone) {
        Phone = phone;
    }

    public String getFax() {
        return Fax;
    }

    public void setFax(String fax) {
        Fax = fax;
    }

    public String getHomeAddress() {
        return HomeAddress;
    }

    public void setHomeAddress(String homeAddress) {
        HomeAddress = homeAddress;
    }

    public String getHomeZipCode() {
        return HomeZipCode;
    }

    public void setHomeZipCode(String homeZipCode) {
        HomeZipCode = homeZipCode;
    }

    public String getHomePhone() {
        return HomePhone;
    }

    public void setHomePhone(String homePhone) {
        HomePhone = homePhone;
    }

    public String getHomeFax() {
        return HomeFax;
    }

    public void setHomeFax(String homeFax) {
        HomeFax = homeFax;
    }

    public String getCompanyAddress() {
        return CompanyAddress;
    }

    public void setCompanyAddress(String companyAddress) {
        CompanyAddress = companyAddress;
    }

    public String getCompanyZipCode() {
        return CompanyZipCode;
    }

    public void setCompanyZipCode(String companyZipCode) {
        CompanyZipCode = companyZipCode;
    }

    public String getCompanyPhone() {
        return CompanyPhone;
    }

    public void setCompanyPhone(String companyPhone) {
        CompanyPhone = companyPhone;
    }

    public String getCompanyFax() {
        return CompanyFax;
    }

    public void setCompanyFax(String companyFax) {
        CompanyFax = companyFax;
    }

    public String getMobile() {
        return Mobile;
    }

    public void setMobile(String mobile) {
        Mobile = mobile;
    }

    public String getMobileChs() {
        return MobileChs;
    }

    public void setMobileChs(String mobileChs) {
        MobileChs = mobileChs;
    }

    public String getEMail() {
        return EMail;
    }

    public void setEMail(String EMail) {
        this.EMail = EMail;
    }

    public String getBP() {
        return BP;
    }

    public void setBP(String BP) {
        this.BP = BP;
    }

    public String getMobile2() {
        return Mobile2;
    }

    public void setMobile2(String mobile2) {
        Mobile2 = mobile2;
    }

    public String getMobileChs2() {
        return MobileChs2;
    }

    public void setMobileChs2(String mobileChs2) {
        MobileChs2 = mobileChs2;
    }

    public String getEMail2() {
        return EMail2;
    }

    public void setEMail2(String EMail2) {
        this.EMail2 = EMail2;
    }

    public String getBP2() {
        return BP2;
    }

    public void setBP2(String BP2) {
        this.BP2 = BP2;
    }

    public String getOperator() {
        return Operator;
    }

    public void setOperator(String operator) {
        Operator = operator;
    }

    public String getMakeDate() {
        return MakeDate;
    }

    public void setMakeDate(String makeDate) {
        MakeDate = makeDate;
    }

    public String getMakeTime() {
        return MakeTime;
    }

    public void setMakeTime(String makeTime) {
        MakeTime = makeTime;
    }

    public String getModifyDate() {
        return ModifyDate;
    }

    public void setModifyDate(String modifyDate) {
        ModifyDate = modifyDate;
    }

    public String getModifyTime() {
        return ModifyTime;
    }

    public void setModifyTime(String modifyTime) {
        ModifyTime = modifyTime;
    }

    public String getGrpName() {
        return GrpName;
    }

    public void setGrpName(String grpName) {
        GrpName = grpName;
    }

    public String getProvince() {
        return Province;
    }

    public void setProvince(String province) {
        Province = province;
    }

    public String getCity() {
        return City;
    }

    public void setCity(String city) {
        City = city;
    }

    public String getCounty() {
        return County;
    }

    public void setCounty(String county) {
        County = county;
    }

    public String getZoneCode() {
        return ZoneCode;
    }

    public void setZoneCode(String zoneCode) {
        ZoneCode = zoneCode;
    }

    public String getStoreNo() {
        return StoreNo;
    }

    public void setStoreNo(String storeNo) {
        StoreNo = storeNo;
    }

    public String getStoreNo2() {
        return StoreNo2;
    }

    public void setStoreNo2(String storeNo2) {
        StoreNo2 = storeNo2;
    }

    @Override
    public String toString() {
        return "LCAddress{" +
                "AddressID=" + AddressID +
                ", PersonID=" + PersonID +
                ", ShardingID='" + ShardingID + '\'' +
                ", CustomerNo='" + CustomerNo + '\'' +
                ", AddressNo=" + AddressNo +
                ", PostalAddress='" + PostalAddress + '\'' +
                ", ZipCode='" + ZipCode + '\'' +
                ", Phone='" + Phone + '\'' +
                ", Fax='" + Fax + '\'' +
                ", HomeAddress='" + HomeAddress + '\'' +
                ", HomeZipCode='" + HomeZipCode + '\'' +
                ", HomePhone='" + HomePhone + '\'' +
                ", HomeFax='" + HomeFax + '\'' +
                ", CompanyAddress='" + CompanyAddress + '\'' +
                ", CompanyZipCode='" + CompanyZipCode + '\'' +
                ", CompanyPhone='" + CompanyPhone + '\'' +
                ", CompanyFax='" + CompanyFax + '\'' +
                ", Mobile='" + Mobile + '\'' +
                ", MobileChs='" + MobileChs + '\'' +
                ", EMail='" + EMail + '\'' +
                ", BP='" + BP + '\'' +
                ", Mobile2='" + Mobile2 + '\'' +
                ", MobileChs2='" + MobileChs2 + '\'' +
                ", EMail2='" + EMail2 + '\'' +
                ", BP2='" + BP2 + '\'' +
                ", Operator='" + Operator + '\'' +
                ", MakeDate='" + MakeDate + '\'' +
                ", MakeTime='" + MakeTime + '\'' +
                ", ModifyDate='" + ModifyDate + '\'' +
                ", ModifyTime='" + ModifyTime + '\'' +
                ", GrpName='" + GrpName + '\'' +
                ", Province='" + Province + '\'' +
                ", City='" + City + '\'' +
                ", County='" + County + '\'' +
                ", ZoneCode='" + ZoneCode + '\'' +
                ", StoreNo='" + StoreNo + '\'' +
                ", StoreNo2='" + StoreNo2 + '\'' +
                '}';
    }
}
