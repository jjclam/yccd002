package com.sinosoft.cloud.access.entity;

/**
 * @Author:lizhen
 * @Description:
 * @Date:Created in 18:13 2018/11/12
 * @Modified By:
 */
/**
 * 电子保单补发NB008  的请求报文 返回
 */
public class RespEleCtronPolicy {
    //保单号
    private String ContNo;
    //响应码
    private String RetCode;
    //响应消息
    private String RetMsg;

    public String getContNo() {
        return ContNo;
    }

    public void setContNo(String contNo) {
        ContNo = contNo;
    }

    public String getRetCode() {
        return RetCode;
    }

    public void setRetCode(String retCode) {
        RetCode = retCode;
    }

    public String getRetMsg() {
        return RetMsg;
    }

    public void setRetMsg(String retMsg) {
        RetMsg = retMsg;
    }

    @Override
    public String toString() {
        return "RespEleCtronPolicy{" +
                "ContNo='" + ContNo + '\'' +
                ", RetCode='" + RetCode + '\'' +
                ", RetMsg='" + RetMsg + '\'' +
                '}';
    }
}