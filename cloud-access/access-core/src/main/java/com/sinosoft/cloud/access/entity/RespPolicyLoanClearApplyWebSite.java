package com.sinosoft.cloud.access.entity;

/**
 * @Author:lizhen
 * @Description:
 * @Date:Created in 10:27 2018/12/20
 * @Modified By:
 */
/**
 * 4010保单贷款清偿申请POS096
 */
public class RespPolicyLoanClearApplyWebSite {
    private String EdorType;
    private String Principal;
    private String ContNo;
    private String PayOffMoney;
    private String LoanInterest;
    private String OverRate;
    private String OverInterest;

    public String getEdorType() {
        return EdorType;
    }

    public void setEdorType(String edorType) {
        EdorType = edorType;
    }

    public String getPrincipal() {
        return Principal;
    }

    public void setPrincipal(String principal) {
        Principal = principal;
    }

    public String getContNo() {
        return ContNo;
    }

    public void setContNo(String contNo) {
        ContNo = contNo;
    }

    public String getPayOffMoney() {
        return PayOffMoney;
    }

    public void setPayOffMoney(String payOffMoney) {
        PayOffMoney = payOffMoney;
    }

    public String getLoanInterest() {
        return LoanInterest;
    }

    public void setLoanInterest(String loanInterest) {
        LoanInterest = loanInterest;
    }

    public String getOverRate() {
        return OverRate;
    }

    public void setOverRate(String overRate) {
        OverRate = overRate;
    }

    public String getOverInterest() {
        return OverInterest;
    }

    public void setOverInterest(String overInterest) {
        OverInterest = overInterest;
    }

    @Override
    public String toString() {
        return "RespPolicyLoanClearApplyWebSite{" +
                "EdorType='" + EdorType + '\'' +
                ", Principal='" + Principal + '\'' +
                ", ContNo='" + ContNo + '\'' +
                ", PayOffMoney='" + PayOffMoney + '\'' +
                ", LoanInterest='" + LoanInterest + '\'' +
                ", OverRate='" + OverRate + '\'' +
                ", OverInterest='" + OverInterest + '\'' +
                '}';
    }
}