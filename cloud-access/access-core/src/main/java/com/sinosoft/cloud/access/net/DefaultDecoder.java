package com.sinosoft.cloud.access.net;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ByteToMessageDecoder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.ByteOrder;
import java.nio.charset.Charset;
import java.util.List;

import static org.bouncycastle.asn1.x500.style.RFC4519Style.c;

/**
 * cloud-access
 *
 * @title: cloud-access
 * @package: com.sinosoft.cloud.access.net
 * @author: yangming
 * @date: 2017/11/25 下午11:56
 */
//@ChannelHandler.Sharable
public class DefaultDecoder extends ByteToMessageDecoder {

    protected String accessName;
    private DecoderFactory decoderFactory = DecoderFactory.getInstance();
    private Logger cLogger = LoggerFactory.getLogger(this.getClass());

    @Override
    protected void decode(ChannelHandlerContext ctx, ByteBuf in, List<Object> out) throws Exception {
        Object decoded = decode(ctx, in);
        if (decoded != null) {
            out.add(decoded);
        }
    }

    private Object decode(ChannelHandlerContext ctx, ByteBuf in) {

        Coder coder = decoderFactory.getDecoder(getAccessName());
        if (coder == null) {
            return null;
        }

        in.order(ByteOrder.BIG_ENDIAN);
        String x = in.toString(Charset.defaultCharset());
        String coderName = coder.getClass().getSimpleName();
        if ("HunanNongXinCoder".equals(coderName)) {
            x = in.toString(Charset.forName("GBK"));
        }
        cLogger.debug("in.toString的内容：" + x);

        int frameLengthInt = coder.getBodySizeByHead(x);
        int currentMsgLength = in.readableBytes() - coder.getHeadSize();
        if (currentMsgLength < frameLengthInt) {
            return null;
        }
        int readerIndex = in.readerIndex();
        ByteBuf frame = extractFrame(ctx, in, readerIndex, frameLengthInt + coder.getHeadSize());
        in.readerIndex(readerIndex + frameLengthInt + coder.getHeadSize());
        return frame;
    }

    private ByteBuf extractFrame(ChannelHandlerContext ctx, ByteBuf in, int readerIndex, int frameLengthInt) {
        return in.retainedSlice(readerIndex, frameLengthInt);
    }

    public String getAccessName() {
        return accessName;
    }

    public void setAccessName(String accessName) {
        this.accessName = accessName;
    }
}
