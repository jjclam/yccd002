package com.sinosoft.cloud.access.entity;

import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 * Created by zhaoshulei on 2017/9/20.
 * 保单基本信息
 */
@XStreamAlias("PolicyBasicInfo")
public class PolicyBasicInfo {

    /** 保险单号 */
    @XStreamAlias("ContNo")
    private String contNo;
    /** 保单印刷号 */
    @XStreamAlias("PrtNo")
    private String prtNo;
    /** 签约日期 */
    @XStreamAlias("SignDate")
    private String signDate;
    /** 保险生效日期 */
    @XStreamAlias("CValiDate")
    private String cValiDate;
    /** 保险终止日期*/
    @XStreamAlias("InsuEndDate")
    private String insuEndDate;
    /** 交费期限 */
    @XStreamAlias("PayEndDate")
    private String payEndDate;
    /** 业务员代码 */
    @XStreamAlias("AgentCode")
    private String agentCode;
    /** 缴费账户 */
    @XStreamAlias("AccNo")
    private String accNo;
    /** 缴费金额 */
    @XStreamAlias("Prem")
    private String prem;
    /** 保费合计*/
    @XStreamAlias("PremText")
    private String premText;
    /** 申请日期 */
    @XStreamAlias("PolApplyDate")
    private String polApplyDate;
    /** 签单时间 */
    @XStreamAlias("SignTime")
    private String signTime;
    /** 特别约定 */
    @XStreamAlias("SpecialInfos")
    private SpecialInfos specialInfos;
    /** 代理xxx网点 */
    @XStreamAlias("BankBranchName")
    private String bankBranchName;
    /** 公司客户经理 */
    @XStreamAlias("AgentManagerName")
    private String agentManagerName;
    /** 公司客户经理工号 */
    @XStreamAlias("AgentManagerCode")
    private String agentManagerCode;
    /** 代理xxx销售人员 */
    @XStreamAlias("AgentSaleName")
    private String agentSaleName;
    /** 代理xxx销售人员工号 */
    @XStreamAlias("AgentSaleCode")
    private String agentSaleCode;
    /** 公司名称 */
    @XStreamAlias("ComName")
    private String comName;
    /** 公司地址 */
    @XStreamAlias("ComAddress")
    private String comAddress;
    /** 公司邮编 */
    @XStreamAlias("ComZipCode")
    private String comZipCode;
    /** 保险单制作日期 */
    @XStreamAlias("ContMakeDate")
    private String contMakeDate;
    /** 保险合同成立日期 */
    @XStreamAlias("ContSignDate")
    private String contSignDate;
    /** 保险合同生效日期 */
    @XStreamAlias("ContValiDate")
    private String contValiDate;
    /** 首期保险费收费确认时间 */
    @XStreamAlias("FirstPremConfirmTime")
    private String firstPremConfirmTime;
    /** 有效保单生成时间 */
    @XStreamAlias("ValiContMakeTime")
    private String valiContMakeTime;
    /** 管理机构 */
    @XStreamAlias("ManageCom")
    private String manageCom;
    /** 业务许可证编号 */
    @XStreamAlias("LicenseNo")
    private String licenseNo;
    /** xxx保险业务负责人 */
    @XStreamAlias("BankBusinessManager")
    private String bankBusinessManager;
    /** xxx保险业务负责人工号 */
    @XStreamAlias("BankBusinessManagerCode")
    private String bankBusinessManagerCode;
    /** xxx人员从业资格证编号 */
    @XStreamAlias("BankWorkCardNo")
    private String bankWorkCardNo;
    /** 公司客户经理从业资格证编号*/
    @XStreamAlias("AgentWorkCardNo")
    private String agentWorkCardNo;
    public String getContNo() {
        return contNo;
    }

    public void setContNo(String contNo) {
        this.contNo = contNo;
    }

    public String getPrtNo() {
        return prtNo;
    }

    public void setPrtNo(String prtNo) {
        this.prtNo = prtNo;
    }

    public String getSignDate() {
        return signDate;
    }

    public void setSignDate(String signDate) {
        this.signDate = signDate;
    }

    public String getcValiDate() {
        return cValiDate;
    }

    public void setcValiDate(String cValiDate) {
        this.cValiDate = cValiDate;
    }

    public String getInsuEndDate() {
        return insuEndDate;
    }

    public void setInsuEndDate(String insuEndDate) {
        this.insuEndDate = insuEndDate;
    }

    public String getPayEndDate() {
        return payEndDate;
    }

    public void setPayEndDate(String payEndDate) {
        this.payEndDate = payEndDate;
    }

    public String getAgentCode() {
        return agentCode;
    }

    public void setAgentCode(String agentCode) {
        this.agentCode = agentCode;
    }

    public String getAccNo() {
        return accNo;
    }

    public void setAccNo(String accNo) {
        this.accNo = accNo;
    }

    public String getPrem() {
        return prem;
    }

    public void setPrem(String prem) {
        this.prem = prem;
    }

    public String getPremText() {
        return premText;
    }

    public void setPremText(String premText) {
        this.premText = premText;
    }

    public String getPolApplyDate() {
        return polApplyDate;
    }

    public void setPolApplyDate(String polApplyDate) {
        this.polApplyDate = polApplyDate;
    }

    public String getSignTime() {
        return signTime;
    }

    public void setSignTime(String signTime) {
        this.signTime = signTime;
    }

    public SpecialInfos getSpecialInfos() {
        return specialInfos;
    }

    public void setSpecialInfos(SpecialInfos specialInfos) {
        this.specialInfos = specialInfos;
    }

    public String getBankBranchName() {
        return bankBranchName;
    }

    public void setBankBranchName(String bankBranchName) {
        this.bankBranchName = bankBranchName;
    }

    public String getAgentManagerName() {
        return agentManagerName;
    }

    public void setAgentManagerName(String agentManagerName) {
        this.agentManagerName = agentManagerName;
    }

    public String getAgentManagerCode() {
        return agentManagerCode;
    }

    public void setAgentManagerCode(String agentManagerCode) {
        this.agentManagerCode = agentManagerCode;
    }

    public String getAgentSaleName() {
        return agentSaleName;
    }

    public void setAgentSaleName(String agentSaleName) {
        this.agentSaleName = agentSaleName;
    }

    public String getAgentSaleCode() {
        return agentSaleCode;
    }

    public void setAgentSaleCode(String agentSaleCode) {
        this.agentSaleCode = agentSaleCode;
    }

    public String getComName() {
        return comName;
    }

    public void setComName(String comName) {
        this.comName = comName;
    }

    public String getComAddress() {
        return comAddress;
    }

    public void setComAddress(String comAddress) {
        this.comAddress = comAddress;
    }

    public String getComZipCode() {
        return comZipCode;
    }

    public void setComZipCode(String comZipCode) {
        this.comZipCode = comZipCode;
    }

    public String getContMakeDate() {
        return contMakeDate;
    }

    public void setContMakeDate(String contMakeDate) {
        this.contMakeDate = contMakeDate;
    }

    public String getContSignDate() {
        return contSignDate;
    }

    public void setContSignDate(String contSignDate) {
        this.contSignDate = contSignDate;
    }

    public String getContValiDate() {
        return contValiDate;
    }

    public void setContValiDate(String contValiDate) {
        this.contValiDate = contValiDate;
    }

    public String getFirstPremConfirmTime() {
        return firstPremConfirmTime;
    }

    public void setFirstPremConfirmTime(String firstPremConfirmTime) {
        this.firstPremConfirmTime = firstPremConfirmTime;
    }

    public String getValiContMakeTime() {
        return valiContMakeTime;
    }

    public void setValiContMakeTime(String valiContMakeTime) {
        this.valiContMakeTime = valiContMakeTime;
    }

    public String getManageCom() {
        return manageCom;
    }

    public void setManageCom(String manageCom) {
        this.manageCom = manageCom;
    }

    public String getLicenseNo() {
        return licenseNo;
    }

    public void setLicenseNo(String licenseNo) {
        this.licenseNo = licenseNo;
    }

    public String getBankBusinessManager() {
        return bankBusinessManager;
    }

    public void setBankBusinessManager(String bankBusinessManager) {
        this.bankBusinessManager = bankBusinessManager;
    }

    public String getBankBusinessManagerCode() {
        return bankBusinessManagerCode;
    }

    public void setBankBusinessManagerCode(String bankBusinessManagerCode) {
        this.bankBusinessManagerCode = bankBusinessManagerCode;
    }

    public String getBankWorkCardNo() {
        return bankWorkCardNo;
    }

    public void setBankWorkCardNo(String bankWorkCardNo) {
        this.bankWorkCardNo = bankWorkCardNo;
    }

    public String getAgentWorkCardNo() {
        return agentWorkCardNo;
    }

    public void setAgentWorkCardNo(String agentWorkCardNo) {
        this.agentWorkCardNo = agentWorkCardNo;
    }

    @Override
    public String toString() {
        return "PolicyBasicInfo{" +
                "contNo='" + contNo + '\'' +
                ", prtNo='" + prtNo + '\'' +
                ", signDate='" + signDate + '\'' +
                ", cValiDate='" + cValiDate + '\'' +
                ", insuEndDate='" + insuEndDate + '\'' +
                ", payEndDate='" + payEndDate + '\'' +
                ", agentCode='" + agentCode + '\'' +
                ", accNo='" + accNo + '\'' +
                ", prem='" + prem + '\'' +
                ", premText='" + premText + '\'' +
                ", polApplyDate='" + polApplyDate + '\'' +
                ", signTime='" + signTime + '\'' +
                ", specialInfos=" + specialInfos +
                ", bankBranchName='" + bankBranchName + '\'' +
                ", agentManagerName='" + agentManagerName + '\'' +
                ", agentManagerCode='" + agentManagerCode + '\'' +
                ", agentSaleName='" + agentSaleName + '\'' +
                ", agentSaleCode='" + agentSaleCode + '\'' +
                ", comName='" + comName + '\'' +
                ", comAddress='" + comAddress + '\'' +
                ", comZipCode='" + comZipCode + '\'' +
                ", contMakeDate='" + contMakeDate + '\'' +
                ", contSignDate='" + contSignDate + '\'' +
                ", contValiDate='" + contValiDate + '\'' +
                ", firstPremConfirmTime='" + firstPremConfirmTime + '\'' +
                ", valiContMakeTime='" + valiContMakeTime + '\'' +
                ", manageCom='" + manageCom + '\'' +
                ", licenseNo='" + licenseNo + '\'' +
                ", bankBusinessManager='" + bankBusinessManager + '\'' +
                ", bankBusinessManagerCode='" + bankBusinessManagerCode + '\'' +
                ", bankWorkCardNo='" + bankWorkCardNo + '\'' +
                ", agentWorkCardNo='" + agentWorkCardNo + '\'' +
                '}';
    }
}
