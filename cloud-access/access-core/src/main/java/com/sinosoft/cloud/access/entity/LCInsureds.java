package com.sinosoft.cloud.access.entity;

import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.List;

/**
 * @Description
 * @Author:zhaoshulei
 * @Date:2018/4/10
 */
public class LCInsureds {

    @XStreamImplicit(itemFieldName="LCInsured")
    private List<LCInsured> LCInsureds;

    public List<LCInsured> getLCInsureds() {
        return LCInsureds;
    }

    public void setLCInsureds(List<LCInsured> LCInsureds) {
        this.LCInsureds = LCInsureds;
    }

    @Override
    public String toString() {
        return "LCInsureds{" +
                "LCInsureds=" + LCInsureds +
                '}';
    }
}
