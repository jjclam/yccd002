package com.sinosoft.cloud.access.entity;
/**
 * 红利领取方式变更保全申请POS022.
 */
public class ReqBonusGetModeApply {

    //保全项目编码
    private String EdorType;
    //保单号
    private String ContNo;
    //申请日期
    private String ApplyDate;
    //红利领取方式
    private String GetMode;
    //收付费方式
    private String PayMode;
    private EdorAppInfo EdorAppInfo;
    private PersonInfo PersonInfo;
    private BankInfo BankInfo;
    //红利领取方式(一级)
    private String BonusGetMode;
    //红利领取方式(二级)
    private String BonusAccFlag;
    //红利领取方式(三级)
    private String BonusGetform;

    public String getEdorType() {
        return EdorType;
    }

    public void setEdorType(String edorType) {
        EdorType = edorType;
    }

    public String getContNo() {
        return ContNo;
    }

    public void setContNo(String contNo) {
        ContNo = contNo;
    }

    public String getApplyDate() {
        return ApplyDate;
    }

    public void setApplyDate(String applyDate) {
        ApplyDate = applyDate;
    }

    public String getGetMode() {
        return GetMode;
    }

    public void setGetMode(String getMode) {
        GetMode = getMode;
    }

    public String getPayMode() {
        return PayMode;
    }

    public void setPayMode(String payMode) {
        PayMode = payMode;
    }

    public com.sinosoft.cloud.access.entity.EdorAppInfo getEdorAppInfo() {
        return EdorAppInfo;
    }

    public void setEdorAppInfo(com.sinosoft.cloud.access.entity.EdorAppInfo edorAppInfo) {
        EdorAppInfo = edorAppInfo;
    }

    public com.sinosoft.cloud.access.entity.PersonInfo getPersonInfo() {
        return PersonInfo;
    }

    public void setPersonInfo(com.sinosoft.cloud.access.entity.PersonInfo personInfo) {
        PersonInfo = personInfo;
    }

    public com.sinosoft.cloud.access.entity.BankInfo getBankInfo() {
        return BankInfo;
    }

    public void setBankInfo(com.sinosoft.cloud.access.entity.BankInfo bankInfo) {
        BankInfo = bankInfo;
    }

    public String getBonusGetMode() {
        return BonusGetMode;
    }

    public void setBonusGetMode(String bonusGetMode) {
        BonusGetMode = bonusGetMode;
    }

    public String getBonusAccFlag() {
        return BonusAccFlag;
    }

    public void setBonusAccFlag(String bonusAccFlag) {
        BonusAccFlag = bonusAccFlag;
    }

    public String getBonusGetform() {
        return BonusGetform;
    }

    public void setBonusGetform(String bonusGetform) {
        BonusGetform = bonusGetform;
    }

    @Override
    public String toString() {
        return "ReqBonusGetModeApply{" +
                "EdorType='" + EdorType + '\'' +
                ", ContNo='" + ContNo + '\'' +
                ", ApplyDate='" + ApplyDate + '\'' +
                ", GetMode='" + GetMode + '\'' +
                ", PayMode='" + PayMode + '\'' +
                ", EdorAppInfo=" + EdorAppInfo +
                ", PersonInfo=" + PersonInfo +
                ", BankInfo=" + BankInfo +
                ", BonusGetMode='" + BonusGetMode + '\'' +
                ", BonusAccFlag='" + BonusAccFlag + '\'' +
                ", BonusGetform='" + BonusGetform + '\'' +
                '}';
    }
}
