package com.sinosoft.cloud.access.entity;

/**
 * 红利领取要操作保全用户详情POS025,POS102
 */
public class ReqBonusUserDetails {
    //保全项目编码
    private String EdorType;
    //保单号
    private String ContNo;

    private String EdorAcceptNo;

    public String getEdorAcceptNo() {
        return EdorAcceptNo;
    }

    public void setEdorAcceptNo(String edorAcceptNo) {
        EdorAcceptNo = edorAcceptNo;
    }

    public String getEdorType() {
        return EdorType;
    }

    public void setEdorType(String edorType) {
        EdorType = edorType;
    }

    public String getContNo() {
        return ContNo;
    }

    public void setContNo(String contNo) {
        ContNo = contNo;
    }

    @Override
    public String toString() {
        return "ReqBonusUserDetails{" +
                "EdorType='" + EdorType + '\'' +
                ", ContNo='" + ContNo + '\'' +
                ", EdorAcceptNo='" + EdorAcceptNo + '\'' +
                '}';
    }
}
