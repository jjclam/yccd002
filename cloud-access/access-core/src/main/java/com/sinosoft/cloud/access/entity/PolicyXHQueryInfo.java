package com.sinosoft.cloud.access.entity;

/**
 * @program: abc-cloud-access
 * @description:
 * @author: BaoYongmeng
 * @create: 2018-12-24 17:20
 **/
public class PolicyXHQueryInfo {

    //保单申请号
    private String PolicyApplyNo;
    //对账类型
    private String NoticeType;
    //保单申请号
    private String NoticeNo;

    public void setPolicyApplyNo(String policyApplyNo) {
        PolicyApplyNo = policyApplyNo;
    }

    public void setNoticeType(String noticeType) {
        NoticeType = noticeType;
    }

    public void setNoticeNo(String noticeNo) {
        NoticeNo = noticeNo;
    }

    public String getPolicyApplyNo() {
        return PolicyApplyNo;
    }

    public String getNoticeType() {
        return NoticeType;
    }

    public String getNoticeNo() {
        return NoticeNo;
    }

    @Override
    public String toString() {
        return "PolicyXHQueryInfo{" +
                "PolicyApplyNo='" + PolicyApplyNo + '\'' +
                ", NoticeType='" + NoticeType + '\'' +
                ", NoticeNo='" + NoticeNo + '\'' +
                '}';
    }
}
