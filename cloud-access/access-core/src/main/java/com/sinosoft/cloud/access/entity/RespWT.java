package com.sinosoft.cloud.access.entity;

/**
 * @Author:lizhen
 * @Description:
 * @Date:Created in 17:50 2018/11/9
 * @Modified By:
 */
/*
* 4004保单犹豫期退保申请 POS018 返回
* */
public class RespWT {
    //保全受理号
    private String EdorAcceptNo;
    //保单号
    private String ContNo;
    //保全项目编码
    private String EdorType;
    //保全批单号
    private String EdorNo;
    //保全状态
    private String EdorStatus;
    //退保金额
    private String OccurBala;
    //保全生效日
    private String BqValidDate;
    //险种名称
    private String RiskName;


    public String getRiskName() {
        return RiskName;
    }

    public void setRiskName(String riskName) {
        RiskName = riskName;
    }

    public String getEdorAcceptNo() {
        return EdorAcceptNo;
    }

    public void setEdorAcceptNo(String edorAcceptNo) {
        EdorAcceptNo = edorAcceptNo;
    }

    public String getContNo() {
        return ContNo;
    }

    public void setContNo(String contNo) {
        ContNo = contNo;
    }

    public String getEdorType() {
        return EdorType;
    }

    public void setEdorType(String edorType) {
        EdorType = edorType;
    }

    public String getEdorNo() {
        return EdorNo;
    }

    public void setEdorNo(String edorNo) {
        EdorNo = edorNo;
    }

    public String getEdorStatus() {
        return EdorStatus;
    }

    public void setEdorStatus(String edorStatus) {
        EdorStatus = edorStatus;
    }

    public String getOccurBala() {
        return OccurBala;
    }

    public void setOccurBala(String occurBala) {
        OccurBala = occurBala;
    }

    public String getBqValidDate() {
        return BqValidDate;
    }

    public void setBqValidDate(String bqValidDate) {
        BqValidDate = bqValidDate;
    }

    @Override
    public String toString() {
        return "RespWT{" +
                "EdorAcceptNo='" + EdorAcceptNo + '\'' +
                ", ContNo='" + ContNo + '\'' +
                ", EdorType='" + EdorType + '\'' +
                ", EdorNo='" + EdorNo + '\'' +
                ", EdorStatus='" + EdorStatus + '\'' +
                ", OccurBala='" + OccurBala + '\'' +
                ", BqValidDate='" + BqValidDate + '\'' +
                ", RiskName='" + RiskName + '\'' +
                '}';
    }
}