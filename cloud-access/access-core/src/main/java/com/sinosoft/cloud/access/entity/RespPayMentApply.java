package com.sinosoft.cloud.access.entity;
/**
 * 4004万能补缴申请POS036.
 */
public class RespPayMentApply {

    //保全项目编码
    private String EdorType;
    //保单号
    private String ContNo;
    //保全受理号
    private String EdorAcceptNo;
    //保全批单号
    private String EdorNo;
    @Override
    public String toString() {
        return "RespPayMentApply{" +
                "EdorType='" + EdorType + '\'' +
                ", ContNo='" + ContNo + '\'' +
                ", EdorAcceptNo='" + EdorAcceptNo + '\'' +
                ", EdorNo='" + EdorNo + '\'' +
                '}';
    }

    public String getEdorType() {
        return EdorType;
    }

    public void setEdorType(String edorType) {
        EdorType = edorType;
    }

    public String getContNo() {
        return ContNo;
    }

    public void setContNo(String contNo) {
        ContNo = contNo;
    }

    public String getEdorAcceptNo() {
        return EdorAcceptNo;
    }

    public void setEdorAcceptNo(String edorAcceptNo) {
        EdorAcceptNo = edorAcceptNo;
    }

    public String getEdorNo() {
        return EdorNo;
    }

    public void setEdorNo(String edorNo) {
        EdorNo = edorNo;
    }
}
