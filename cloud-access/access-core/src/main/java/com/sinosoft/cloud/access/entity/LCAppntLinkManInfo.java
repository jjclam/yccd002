package com.sinosoft.cloud.access.entity;

/**
 * Created by Administrator on 2018/4/16.
 */
public class LCAppntLinkManInfo {
    private long AppntLinkID;
    private long ContID;
    private String ShardingID;
    private String PrtNo;
    private String ContNo;
    private String AppntNo;
    private String Name;
    private String Sex;
    private String Birthday;
    private String IDType;
    private String IDNo;
    private String RelationToApp;
    private String Tel1;
    private String Tel3;
    private String Tel4;
    private String Tel5;
    private String Tel2;
    private String Mobile1;
    private String Mobile2;
    private String Address;
    private String ZipCode;
    private String Email;
    private String ManageCom;
    private String Operator;
    private String MakeDate;
    private String MakeTime;
    private String ModifyDate;
    private String ModifyTime;
    private String LetterSendMode;

    public long getAppntLinkID() {
        return AppntLinkID;
    }

    public void setAppntLinkID(long appntLinkID) {
        AppntLinkID = appntLinkID;
    }

    public long getContID() {
        return ContID;
    }

    public void setContID(long contID) {
        ContID = contID;
    }

    public String getShardingID() {
        return ShardingID;
    }

    public void setShardingID(String shardingID) {
        ShardingID = shardingID;
    }

    public String getPrtNo() {
        return PrtNo;
    }

    public void setPrtNo(String prtNo) {
        PrtNo = prtNo;
    }

    public String getContNo() {
        return ContNo;
    }

    public void setContNo(String contNo) {
        ContNo = contNo;
    }

    public String getAppntNo() {
        return AppntNo;
    }

    public void setAppntNo(String appntNo) {
        AppntNo = appntNo;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getSex() {
        return Sex;
    }

    public void setSex(String sex) {
        Sex = sex;
    }

    public String getBirthday() {
        return Birthday;
    }

    public void setBirthday(String birthday) {
        Birthday = birthday;
    }

    public String getIDType() {
        return IDType;
    }

    public void setIDType(String IDType) {
        this.IDType = IDType;
    }

    public String getIDNo() {
        return IDNo;
    }

    public void setIDNo(String IDNo) {
        this.IDNo = IDNo;
    }

    public String getRelationToApp() {
        return RelationToApp;
    }

    public void setRelationToApp(String relationToApp) {
        RelationToApp = relationToApp;
    }

    public String getTel1() {
        return Tel1;
    }

    public void setTel1(String tel1) {
        Tel1 = tel1;
    }

    public String getTel3() {
        return Tel3;
    }

    public void setTel3(String tel3) {
        Tel3 = tel3;
    }

    public String getTel4() {
        return Tel4;
    }

    public void setTel4(String tel4) {
        Tel4 = tel4;
    }

    public String getTel5() {
        return Tel5;
    }

    public void setTel5(String tel5) {
        Tel5 = tel5;
    }

    public String getTel2() {
        return Tel2;
    }

    public void setTel2(String tel2) {
        Tel2 = tel2;
    }

    public String getMobile1() {
        return Mobile1;
    }

    public void setMobile1(String mobile1) {
        Mobile1 = mobile1;
    }

    public String getMobile2() {
        return Mobile2;
    }

    public void setMobile2(String mobile2) {
        Mobile2 = mobile2;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String address) {
        Address = address;
    }

    public String getZipCode() {
        return ZipCode;
    }

    public void setZipCode(String zipCode) {
        ZipCode = zipCode;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getManageCom() {
        return ManageCom;
    }

    public void setManageCom(String manageCom) {
        ManageCom = manageCom;
    }

    public String getOperator() {
        return Operator;
    }

    public void setOperator(String operator) {
        Operator = operator;
    }

    public String getMakeDate() {
        return MakeDate;
    }

    public void setMakeDate(String makeDate) {
        MakeDate = makeDate;
    }

    public String getMakeTime() {
        return MakeTime;
    }

    public void setMakeTime(String makeTime) {
        MakeTime = makeTime;
    }

    public String getModifyDate() {
        return ModifyDate;
    }

    public void setModifyDate(String modifyDate) {
        ModifyDate = modifyDate;
    }

    public String getModifyTime() {
        return ModifyTime;
    }

    public void setModifyTime(String modifyTime) {
        ModifyTime = modifyTime;
    }

    public String getLetterSendMode() {
        return LetterSendMode;
    }

    public void setLetterSendMode(String letterSendMode) {
        LetterSendMode = letterSendMode;
    }

    @Override
    public String toString() {
        return "LCAppntLinkManInfo{" +
                "AppntLinkID=" + AppntLinkID +
                ", ContID=" + ContID +
                ", ShardingID='" + ShardingID + '\'' +
                ", PrtNo='" + PrtNo + '\'' +
                ", ContNo='" + ContNo + '\'' +
                ", AppntNo='" + AppntNo + '\'' +
                ", Name='" + Name + '\'' +
                ", Sex='" + Sex + '\'' +
                ", Birthday='" + Birthday + '\'' +
                ", IDType='" + IDType + '\'' +
                ", IDNo='" + IDNo + '\'' +
                ", RelationToApp='" + RelationToApp + '\'' +
                ", Tel1='" + Tel1 + '\'' +
                ", Tel3='" + Tel3 + '\'' +
                ", Tel4='" + Tel4 + '\'' +
                ", Tel5='" + Tel5 + '\'' +
                ", Tel2='" + Tel2 + '\'' +
                ", Mobile1='" + Mobile1 + '\'' +
                ", Mobile2='" + Mobile2 + '\'' +
                ", Address='" + Address + '\'' +
                ", ZipCode='" + ZipCode + '\'' +
                ", Email='" + Email + '\'' +
                ", ManageCom='" + ManageCom + '\'' +
                ", Operator='" + Operator + '\'' +
                ", MakeDate='" + MakeDate + '\'' +
                ", MakeTime='" + MakeTime + '\'' +
                ", ModifyDate='" + ModifyDate + '\'' +
                ", ModifyTime='" + ModifyTime + '\'' +
                ", LetterSendMode='" + LetterSendMode + '\'' +
                '}';
    }
}
