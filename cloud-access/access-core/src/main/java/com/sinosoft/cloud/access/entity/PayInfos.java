package com.sinosoft.cloud.access.entity;

import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 * @Author:lizhen
 * @Description:
 * @Date:Created in 9:37 2018/2/28
 * @Modified By:
 */
public class PayInfos {

    @XStreamAlias("PayMoney")
    private String payMoney;
    @XStreamAlias("PayDate")
    private String payDate;
    @XStreamAlias("PaymentOrder")
    private String paymentOrder;
    @XStreamAlias("RiskCode")
    private String riskCode;

    public String getPayMoney() {
        return payMoney;
    }

    public void setPayMoney(String payMoney) {
        this.payMoney = payMoney;
    }

    public String getPayDate() {
        return payDate;
    }

    public void setPayDate(String payDate) {
        this.payDate = payDate;
    }

    public String getPaymentOrder() {
        return paymentOrder;
    }

    public void setPaymentOrder(String paymentOrder) {
        this.paymentOrder = paymentOrder;
    }

    public String getRiskCode() {
        return riskCode;
    }

    public void setRiskCode(String riskCode) {
        this.riskCode = riskCode;
    }

    @Override
    public String toString() {
        return "PayInfos{" +
                "payMoney='" + payMoney + '\'' +
                ", payDate='" + payDate + '\'' +
                ", paymentOrder='" + paymentOrder + '\'' +
                ", riskCode='" + riskCode + '\'' +
                '}';
    }
}