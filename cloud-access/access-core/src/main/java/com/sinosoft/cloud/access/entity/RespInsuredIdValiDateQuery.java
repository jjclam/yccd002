package com.sinosoft.cloud.access.entity;

import com.sinosoft.lis.pos.entity.PolicyQueryInfosPojo;
/**
 * 被保人身份证有效期变更查询POS053.
 */
public class RespInsuredIdValiDateQuery {
    //保全项目编码
    private String EdorType;
    private PolicyQueryInfos PolicyQueryInfos;

    public String getEdorType() {
        return EdorType;
    }

    public void setEdorType(String edorType) {
        EdorType = edorType;
    }

    public com.sinosoft.cloud.access.entity.PolicyQueryInfos getPolicyQueryInfos() {
        return PolicyQueryInfos;
    }

    public void setPolicyQueryInfos(com.sinosoft.cloud.access.entity.PolicyQueryInfos policyQueryInfos) {
        PolicyQueryInfos = policyQueryInfos;
    }

    @Override
    public String toString() {
        return "RespInsuredIdValiDateQuery{" +
                "EdorType='" + EdorType + '\'' +
                ", PolicyQueryInfos=" + PolicyQueryInfos +
                '}';
    }

}
