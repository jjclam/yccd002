package com.sinosoft.cloud.access.entity;

import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 * @Author:lizhen
 * @Description:
 * @Date:Created in 10:25 2018/3/9
 * @Modified By:
 */
public class PrintNode {
    @XStreamAlias("FinActivityGrossAmt")
    private String finActivityGrossAmt;
    @XStreamAlias("RiskName")
    private String riskName;
    @XStreamAlias("PolNumber")
    private String polNumber;
    @XStreamAlias("CustomerName")
    private String customerName;
    @XStreamAlias("BankAccNo")
    private String bankAccNo;
    @XStreamAlias("EdorAppDate")
    private String edorAppDate;
    @XStreamAlias("EdorValiDate")
    private String edorValiDate;
    @XStreamAlias("BusiType")
    private String busiType;

    public String getFinActivityGrossAmt() {
        return finActivityGrossAmt;
    }

    public void setFinActivityGrossAmt(String finActivityGrossAmt) {
        this.finActivityGrossAmt = finActivityGrossAmt;
    }

    public String getRiskName() {
        return riskName;
    }

    public void setRiskName(String riskName) {
        this.riskName = riskName;
    }

    public String getPolNumber() {
        return polNumber;
    }

    public void setPolNumber(String polNumber) {
        this.polNumber = polNumber;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getBankAccNo() {
        return bankAccNo;
    }

    public void setBankAccNo(String bankAccNo) {
        this.bankAccNo = bankAccNo;
    }

    public String getEdorAppDate() {
        return edorAppDate;
    }

    public void setEdorAppDate(String edorAppDate) {
        this.edorAppDate = edorAppDate;
    }

    public String getEdorValiDate() {
        return edorValiDate;
    }

    public void setEdorValiDate(String edorValiDate) {
        this.edorValiDate = edorValiDate;
    }

    public String getBusiType() {
        return busiType;
    }

    public void setBusiType(String busiType) {
        this.busiType = busiType;
    }

    @Override
    public String toString() {
        return "PrintNode{" +
                "finActivityGrossAmt=" + finActivityGrossAmt +
                ", riskName='" + riskName + '\'' +
                ", polNumber='" + polNumber + '\'' +
                ", customerName='" + customerName + '\'' +
                ", bankAccNo='" + bankAccNo + '\'' +
                ", edorAppDate='" + edorAppDate + '\'' +
                ", edorValiDate='" + edorValiDate + '\'' +
                ", busiType='" + busiType + '\'' +
                '}';
    }
}