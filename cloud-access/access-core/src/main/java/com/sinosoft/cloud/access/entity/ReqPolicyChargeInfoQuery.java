package com.sinosoft.cloud.access.entity;

/**
 * 保单收费信息查询XQ006.
 */
public class ReqPolicyChargeInfoQuery {
    //保单号
    private String ContNo;

    public String getContNo() {
        return ContNo;
    }

    public void setContNo(String contNo) {
        ContNo = contNo;
    }

    @Override
    public String toString() {
        return "ReqPolicyChargeInfoQuery{" +
                "ContNo='" + ContNo + '\'' +
                '}';
    }
}
