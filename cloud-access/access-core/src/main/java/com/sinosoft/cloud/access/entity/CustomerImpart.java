package com.sinosoft.cloud.access.entity;

import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 * Created by Administrator on 2017-9-22.
 */
@XStreamAlias("CustomerImpart")
public class CustomerImpart {
    @XStreamAlias("ImpartObject")
    private String impartObject;
    @XStreamAlias("ImpartType")
    private String impartType;
    @XStreamAlias("ImpartCode")
    private String impartCode;
    @XStreamAlias("ImpartParamModle")
    private String impartParamModle;

    public String getImpartObject() {
        return impartObject;
    }

    public void setImpartObject(String impartObject) {
        this.impartObject = impartObject;
    }

    public String getImpartType() {
        return impartType;
    }

    public void setImpartType(String impartType) {
        this.impartType = impartType;
    }

    public String getImpartCode() {
        return impartCode;
    }

    public void setImpartCode(String impartCode) {
        this.impartCode = impartCode;
    }

    public String getImpartParamModle() {
        return impartParamModle;
    }

    public void setImpartParamModle(String impartParamModle) {
        this.impartParamModle = impartParamModle;
    }

    @Override
    public String toString() {
        return "CustomerImpart{" +
                "impartObject='" + impartObject + '\'' +
                ", impartType='" + impartType + '\'' +
                ", impartCode='" + impartCode + '\'' +
                ", impartParamModle='" + impartParamModle + '\'' +
                '}';
    }
}
