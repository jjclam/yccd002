package com.sinosoft.cloud.access.entity;

/**
 * 受益人身份证有效期变更查询POS050.
 */

public class RespBnfIdValiDateQuery {
    //保全项目编码
    private String EdorType;
    private PolicyQueryInfos PolicyQueryInfos;

    public String getEdorType() {
        return EdorType;
    }

    public void setEdorType(String edorType) {
        EdorType = edorType;
    }

    public com.sinosoft.cloud.access.entity.PolicyQueryInfos getPolicyQueryInfos() {
        return PolicyQueryInfos;
    }

    public void setPolicyQueryInfos(com.sinosoft.cloud.access.entity.PolicyQueryInfos policyQueryInfos) {
        PolicyQueryInfos = policyQueryInfos;
    }

    @Override
    public String toString() {
        return "RespBnfIdValiDateQuery{" +
                "EdorType='" + EdorType + '\'' +
                ", PolicyQueryInfos=" + PolicyQueryInfos +
                '}';
    }
}
