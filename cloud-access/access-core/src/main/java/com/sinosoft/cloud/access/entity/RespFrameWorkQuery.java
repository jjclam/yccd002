package com.sinosoft.cloud.access.entity;

import com.sinosoft.lis.pos.entity.ManageComsPojo;

/**
 * abc-cloud-access
 *
 * @author lizhen
 * @create 2019/1/14
 */
//clm003
public class RespFrameWorkQuery {
    private ManageComs ManageComs;

    public com.sinosoft.cloud.access.entity.ManageComs getManageComs() {
        return ManageComs;
    }

    public void setManageComs(com.sinosoft.cloud.access.entity.ManageComs manageComs) {
        ManageComs = manageComs;
    }

    @Override
    public String toString() {
        return "RespFrameWorkQuery{" +
                "ManageComs=" + ManageComs +
                '}';
    }
}