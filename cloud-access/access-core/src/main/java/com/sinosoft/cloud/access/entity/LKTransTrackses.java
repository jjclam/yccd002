package com.sinosoft.cloud.access.entity;

import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.List;

/**
 * @Author:lizhen
 * @Description:
 * @Date:Created in 19:48 2018/9/5
 * @Modified By:
 */
public class LKTransTrackses {
    @XStreamImplicit(itemFieldName="LKTransTracks")
    private List<LKTransTracks> LKTransTrackses;

    public List<LKTransTracks> getLKTransTrackses() {
        return LKTransTrackses;
    }

    public void setLKTransTrackses(List<LKTransTracks> LKTransTrackses) {
        this.LKTransTrackses = LKTransTrackses;
    }

    @Override
    public String toString() {
        return "LKTransTrackses{" +
                "LKTransTrackses=" + LKTransTrackses +
                '}';
    }
}
