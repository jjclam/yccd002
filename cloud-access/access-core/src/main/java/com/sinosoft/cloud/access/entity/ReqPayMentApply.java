package com.sinosoft.cloud.access.entity;
/**
 * 4004万能补缴申请POS036.
 */
public class ReqPayMentApply {
    //保全项目编码
    private String EdorType;
    //保单号
    private String ContNo;
    //领取金额/万能账户余额/getMoney/账户价值/追加保费金额/退保金额
    private String GetMoney;
    //是否为美国纳税义务的个人
    private String TinFlag;
    //美国纳税人识别号
    private String TinNo;
    //申请日期
    private String ApplyDate;
    //缴费期数
    private String Reprenumber;

    //收付费方式
    private String PayMode;
    private EdorAppInfo EdorAppInfo;
    private PersonInfo PersonInfo;
    private BankInfo BankInfo;

    public void setEdorType(String edorType) {
        EdorType = edorType;
    }

    public void setContNo(String contNo) {
        ContNo = contNo;
    }

    public void setGetMoney(String getMoney) {
        GetMoney = getMoney;
    }

    public void setTinFlag(String tinFlag) {
        TinFlag = tinFlag;
    }

    public void setTinNo(String tinNo) {
        TinNo = tinNo;
    }

    public void setApplyDate(String applyDate) {
        ApplyDate = applyDate;
    }

    public void setReprenumber(String reprenumber) {
        Reprenumber = reprenumber;
    }

    public void setPayMode(String payMode) {
        PayMode = payMode;
    }


    public String getEdorType() {
        return EdorType;
    }

    public String getContNo() {
        return ContNo;
    }

    public String getGetMoney() {
        return GetMoney;
    }

    public String getTinFlag() {
        return TinFlag;
    }

    public String getTinNo() {
        return TinNo;
    }

    public String getApplyDate() {
        return ApplyDate;
    }

    public String getReprenumber() {
        return Reprenumber;
    }

    public String getPayMode() {
        return PayMode;
    }

    public void setEdorAppInfo(com.sinosoft.cloud.access.entity.EdorAppInfo edorAppInfo) {
        EdorAppInfo = edorAppInfo;
    }

    public void setPersonInfo(com.sinosoft.cloud.access.entity.PersonInfo personInfo) {
        PersonInfo = personInfo;
    }

    public void setBankInfo(com.sinosoft.cloud.access.entity.BankInfo bankInfo) {
        BankInfo = bankInfo;
    }

    public com.sinosoft.cloud.access.entity.EdorAppInfo getEdorAppInfo() {
        return EdorAppInfo;
    }

    public com.sinosoft.cloud.access.entity.PersonInfo getPersonInfo() {
        return PersonInfo;
    }

    public com.sinosoft.cloud.access.entity.BankInfo getBankInfo() {
        return BankInfo;
    }

    @Override
    public String toString() {
        return "ReqPayMentApply{" +
                "EdorType='" + EdorType + '\'' +
                ", ContNo='" + ContNo + '\'' +
                ", GetMoney='" + GetMoney + '\'' +
                ", TinFlag='" + TinFlag + '\'' +
                ", TinNo='" + TinNo + '\'' +
                ", ApplyDate='" + ApplyDate + '\'' +
                ", Reprenumber='" + Reprenumber + '\'' +
                ", PayMode='" + PayMode + '\'' +
                ", EdorAppInfo=" + EdorAppInfo +
                ", PersonInfo=" + PersonInfo +
                ", BankInfo=" + BankInfo +
                '}';
    }
}
