package com.sinosoft.cloud.access.entity;

/**
 * @Author:lizhen
 * @Description:
 * @Date:Created in 16:59 2018/11/16
 * @Modified By:
 */
/**
 * 客户保单列表查询 pos086 返回
 */
public class RespCustomerListQuery {
    //保全项目编码
    private String EdorType;
    //保单续期查询
    private PolicyQueryInfos PolicyQueryInfos;
    //联系电话节点
    private PhoneInfos PhoneInfos;

    public String getEdorType() {
        return EdorType;
    }

    public void setEdorType(String edorType) {
        EdorType = edorType;
    }

    public com.sinosoft.cloud.access.entity.PolicyQueryInfos getPolicyQueryInfos() {
        return PolicyQueryInfos;
    }

    public void setPolicyQueryInfos(com.sinosoft.cloud.access.entity.PolicyQueryInfos policyQueryInfos) {
        PolicyQueryInfos = policyQueryInfos;
    }

    public PhoneInfos getPhoneInfos() {
        return PhoneInfos;
    }

    public void setPhoneInfos(PhoneInfos phoneInfos) {
        PhoneInfos = phoneInfos;
    }

    @Override
    public String toString() {
        return "RespCustomerListQuery{" +
                "EdorType='" + EdorType + '\'' +
                ", PolicyQueryInfos=" + PolicyQueryInfos +
                ", PhoneInfos=" + PhoneInfos +
                '}';
    }
}