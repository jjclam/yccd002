package com.sinosoft.cloud.access.annotations;

import org.springframework.stereotype.Component;

import java.lang.annotation.*;

@Component
@Inherited
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface AccessCoder {
    /**
     * 接口名称
     *
     * @return
     */
    String name();
}