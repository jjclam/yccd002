package com.sinosoft.cloud.access.entity;

/**
 * @Author:lizhen
 * @Description:
 * @Date:Created in 15:20 2018/11/15
 * @Modified By:
 */

import java.util.List;

/**
 * 缴费提醒XQ005. 返回
 */
public class RespRenewalAwoke {
    //续期业务类型
    private String BusiType;
    private List<PolicyJFTXQueryInfos> PolicyJFTXQueryInfos;

    public String getBusiType() {
        return BusiType;
    }

    public void setBusiType(String busiType) {
        BusiType = busiType;
    }

    public List<PolicyJFTXQueryInfos> getPolicyJFTXQueryInfos() {
        return PolicyJFTXQueryInfos;
    }

    public void setPolicyJFTXQueryInfos(List<PolicyJFTXQueryInfos> policyJFTXQueryInfos) {
        PolicyJFTXQueryInfos = policyJFTXQueryInfos;
    }

    @Override
    public String toString() {
        return "RespRenewalAwoke{" +
                "BusiType='" + BusiType + '\'' +
                ", PolicyJFTXQueryInfos=" + PolicyJFTXQueryInfos +
                '}';
    }
}