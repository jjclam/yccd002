package com.sinosoft.cloud.access.entity;

import com.sinosoft.lis.pos.entity.BonusRisksPojo;
/**
 * 红利详情查询POS020. 返回
 */
public class RespBonusGetDetailsQuery {

    //保全项目编码
    private String EdorType;
    //保单号
    private String ContNo;
    //红利总金额
    private String TotalBonusValue;
    private BonusRisks BonusRisks;

    public String getEdorType() {
        return EdorType;
    }

    public void setEdorType(String edorType) {
        EdorType = edorType;
    }

    public String getContNo() {
        return ContNo;
    }

    public void setContNo(String contNo) {
        ContNo = contNo;
    }

    public String getTotalBonusValue() {
        return TotalBonusValue;
    }

    public void setTotalBonusValue(String totalBonusValue) {
        TotalBonusValue = totalBonusValue;
    }

    public com.sinosoft.cloud.access.entity.BonusRisks getBonusRisks() {
        return BonusRisks;
    }

    public void setBonusRisks(com.sinosoft.cloud.access.entity.BonusRisks bonusRisks) {
        BonusRisks = bonusRisks;
    }
    @Override
    public String toString() {
        return "RespBonusGetDetailsQuery{" +
                "EdorType='" + EdorType + '\'' +
                ", ContNo='" + ContNo + '\'' +
                ", TotalBonusValue='" + TotalBonusValue + '\'' +
                ", BonusRisks=" + BonusRisks +
                '}';
    }

}
