package com.sinosoft.cloud.access.entity;

/**
 * 受益人身份证有效期变更申请POS052
 */
public class RespBnfIdValiDateApply {
    //保全项目编码
    private String EdorType;
    //保全受理号
    private String EdorAcceptNo;
    //保全批单号
    private String EdorNo;

    public String getEdorType() {
        return EdorType;
    }

    public void setEdorType(String edorType) {
        EdorType = edorType;
    }

    public String getEdorAcceptNo() {
        return EdorAcceptNo;
    }

    public void setEdorAcceptNo(String edorAcceptNo) {
        EdorAcceptNo = edorAcceptNo;
    }

    public String getEdorNo() {
        return EdorNo;
    }

    public void setEdorNo(String edorNo) {
        EdorNo = edorNo;
    }

    @Override
    public String toString() {
        return "RespBnfIdValiDateApply{" +
                "EdorType='" + EdorType + '\'' +
                ", EdorAcceptNo='" + EdorAcceptNo + '\'' +
                ", EdorNo='" + EdorNo + '\'' +
                '}';
    }
}
