package com.sinosoft.cloud.access.entity;

import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.List;

/**
 * @Description
 * @Author:zhaoshulei
 * @Date:2018/4/10
 */
public class ChnlInfoExtends {

    @XStreamImplicit(itemFieldName="ChnlInfoExtend")
    private List<ChnlInfoExtend> ChnlInfoExtends;


    public void setChnlInfoExtends(List<ChnlInfoExtend> chnlInfoExtends) {
        ChnlInfoExtends = chnlInfoExtends;
    }

    public List<ChnlInfoExtend> getChnlInfoExtends() {
        return ChnlInfoExtends;
    }

    @Override
    public String toString() {
        return "ChnlInfoExtends{" +
                "ChnlInfoExtends=" + ChnlInfoExtends +
                '}';
    }
}
