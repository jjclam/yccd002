package com.sinosoft.cloud.access.router;

import com.sinosoft.cloud.access.transformer.XsltTrans;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.core.env.MutablePropertySources;
import org.springframework.core.env.PropertiesPropertySource;
import org.springframework.core.env.PropertySource;
import org.springframework.core.env.StandardEnvironment;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;
import java.util.Properties;

/**
 * cloud-access
 *
 * @title: cloud-access
 * @package: com.sinosoft.cloud.access.router
 * @author: yangming
 * @date: 2017/11/23 下午9:55
 */
@Controller
public class RefleshProperties {

    //增加临时调用微服务开关，默认打开，打开即调用（1004更新保单号）
    public static String updateContNoFlag = "on";


    @Autowired
    private StandardEnvironment environment;

    private static final Log logger = LogFactory.getLog(XsltTrans.class);

    @Value("${cloud.access.router.config}")
    Resource resource;

    @Autowired
    AutowireCapableBeanFactory beanFactory;

    @Autowired
    private RouterBean routerBean;

    @GetMapping("/router/reflesh")
    @ResponseBody
    public String reflesh() throws IOException {
        if (!resource.exists()) {
            return "文件路径错误！错误路径为：" + resource.getURL();
        }
        String headEl = routerBean.getHeadEL();
        MutablePropertySources propertySources = environment.getPropertySources();
        Iterator<PropertySource<?>> propertySourceIterator = propertySources.iterator();
        PropertySource p = null;
        while (propertySourceIterator.hasNext()) {
            p = propertySourceIterator.next();
            if (p.getName().indexOf(resource.getFilename()) > 0) {
                break;
            }
        }
        Properties properties = new Properties();
        InputStream inputStream = resource.getInputStream();
        properties.load(inputStream);
        inputStream.close();
        propertySources.replace(p.getName(), new PropertiesPropertySource(p.getName(), properties));
        RouterBean tmp = beanFactory.createBean(routerBean.getClass());
        routerBean.setBodyEL(tmp.getBodyEL());
        routerBean.setHeadEL(tmp.getHeadEL());
        routerBean.setCrossEL(tmp.getCrossEL());

        logger.debug("动态刷新服务路由成功！" + System.getProperty("line.separator") + "HeadEL：" + routerBean.getHeadEL()
                + System.getProperty("line.separator") + "BodyEl：" + routerBean.getBodyEL()
                + System.getProperty("line.separator") + "CrossEL：" + routerBean.getCrossEL());

        return routerBean.getHeadEL() + "</br></br>" + routerBean.getBodyEL() + "</br></br>" + routerBean.getCrossEL();
    }

    /**
     * 临时增加微服务调用开关
     * @param flag
     * @return
     */
    @GetMapping("/updateContNo/reflesh/{flag}")
    @ResponseBody
    public String updateContNoFlageflesh(@PathVariable("flag") String flag){
        updateContNoFlag = flag;
        logger.debug("动态刷新调用微服务开关成功！" + "开关状态：" + updateContNoFlag);
        return updateContNoFlag;
    }
}
