package com.sinosoft.cloud.access.transformer.converters;

import com.thoughtworks.xstream.converters.basic.AbstractSingleValueConverter;
import org.apache.commons.lang.StringUtils;

/**
 * @Description
 * @Author:zhaoshulei
 * @Date:2018/4/11
 */
public class IntConverter extends AbstractSingleValueConverter {

    public boolean canConvert(Class type) {
        return type.equals(int.class) || type.equals(Integer.class);
    }

    /**
     * @param str 需要转换的字符串
     * @return
     */
    public Object fromString(String str) {
        if (StringUtils.isEmpty(str.trim())) {
            return 0;
        }
        long value = Long.decode(str).longValue();
        if (value < Integer.MIN_VALUE || value > 0xFFFFFFFFl) {
            throw new NumberFormatException("For input string: \"" + str + '"');
        }
        return new Integer((int) value);
    }

}
