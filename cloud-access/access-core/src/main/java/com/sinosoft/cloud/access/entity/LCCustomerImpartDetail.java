package com.sinosoft.cloud.access.entity;

/**
 * Created by Administrator on 2018/4/16.
 */
public class LCCustomerImpartDetail {
    private long CustomerImpartDetailID;
    private long CustomerImpartID;
    private String ShardingID;
    private String GrpContNo;
    private String ContNo;
    private String ProposalContNo;
    private String PrtNo;
    private String ImpartCode;
    private String ImpartVer;
    private String SubSerialNo;
    private String ImpartDetailContent;
    private String DiseaseContent;
    private String StartDate;
    private String EndDate;
    private String Prover;
    private String CurrCondition;
    private String IsProved;
    private String CustomerNo;
    private String CustomerNoType;
    private String UWClaimFlg;
    private String PrtFlag;
    private String Operator;
    private String MakeDate;
    private String MakeTime;
    private String ModifyDate;
    private String ModifyTime;
    private int PatchNo;

    public long getCustomerImpartDetailID() {
        return CustomerImpartDetailID;
    }

    public void setCustomerImpartDetailID(long customerImpartDetailID) {
        CustomerImpartDetailID = customerImpartDetailID;
    }

    public long getCustomerImpartID() {
        return CustomerImpartID;
    }

    public void setCustomerImpartID(long customerImpartID) {
        CustomerImpartID = customerImpartID;
    }

    public String getShardingID() {
        return ShardingID;
    }

    public void setShardingID(String shardingID) {
        ShardingID = shardingID;
    }

    public String getGrpContNo() {
        return GrpContNo;
    }

    public void setGrpContNo(String grpContNo) {
        GrpContNo = grpContNo;
    }

    public String getContNo() {
        return ContNo;
    }

    public void setContNo(String contNo) {
        ContNo = contNo;
    }

    public String getProposalContNo() {
        return ProposalContNo;
    }

    public void setProposalContNo(String proposalContNo) {
        ProposalContNo = proposalContNo;
    }

    public String getPrtNo() {
        return PrtNo;
    }

    public void setPrtNo(String prtNo) {
        PrtNo = prtNo;
    }

    public String getImpartCode() {
        return ImpartCode;
    }

    public void setImpartCode(String impartCode) {
        ImpartCode = impartCode;
    }

    public String getImpartVer() {
        return ImpartVer;
    }

    public void setImpartVer(String impartVer) {
        ImpartVer = impartVer;
    }

    public String getSubSerialNo() {
        return SubSerialNo;
    }

    public void setSubSerialNo(String subSerialNo) {
        SubSerialNo = subSerialNo;
    }

    public String getImpartDetailContent() {
        return ImpartDetailContent;
    }

    public void setImpartDetailContent(String impartDetailContent) {
        ImpartDetailContent = impartDetailContent;
    }

    public String getDiseaseContent() {
        return DiseaseContent;
    }

    public void setDiseaseContent(String diseaseContent) {
        DiseaseContent = diseaseContent;
    }

    public String getStartDate() {
        return StartDate;
    }

    public void setStartDate(String startDate) {
        StartDate = startDate;
    }

    public String getEndDate() {
        return EndDate;
    }

    public void setEndDate(String endDate) {
        EndDate = endDate;
    }

    public String getProver() {
        return Prover;
    }

    public void setProver(String prover) {
        Prover = prover;
    }

    public String getCurrCondition() {
        return CurrCondition;
    }

    public void setCurrCondition(String currCondition) {
        CurrCondition = currCondition;
    }

    public String getIsProved() {
        return IsProved;
    }

    public void setIsProved(String isProved) {
        IsProved = isProved;
    }

    public String getCustomerNo() {
        return CustomerNo;
    }

    public void setCustomerNo(String customerNo) {
        CustomerNo = customerNo;
    }

    public String getCustomerNoType() {
        return CustomerNoType;
    }

    public void setCustomerNoType(String customerNoType) {
        CustomerNoType = customerNoType;
    }

    public String getUWClaimFlg() {
        return UWClaimFlg;
    }

    public void setUWClaimFlg(String UWClaimFlg) {
        this.UWClaimFlg = UWClaimFlg;
    }

    public String getPrtFlag() {
        return PrtFlag;
    }

    public void setPrtFlag(String prtFlag) {
        PrtFlag = prtFlag;
    }

    public String getOperator() {
        return Operator;
    }

    public void setOperator(String operator) {
        Operator = operator;
    }

    public String getMakeDate() {
        return MakeDate;
    }

    public void setMakeDate(String makeDate) {
        MakeDate = makeDate;
    }

    public String getMakeTime() {
        return MakeTime;
    }

    public void setMakeTime(String makeTime) {
        MakeTime = makeTime;
    }

    public String getModifyDate() {
        return ModifyDate;
    }

    public void setModifyDate(String modifyDate) {
        ModifyDate = modifyDate;
    }

    public String getModifyTime() {
        return ModifyTime;
    }

    public void setModifyTime(String modifyTime) {
        ModifyTime = modifyTime;
    }

    public int getPatchNo() {
        return PatchNo;
    }

    public void setPatchNo(int patchNo) {
        PatchNo = patchNo;
    }
}
