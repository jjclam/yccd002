package com.sinosoft.cloud.access.annotations;


import org.springframework.stereotype.Component;

import java.lang.annotation.*;

@Component
@Inherited
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface AccessChannel {

    /**
     * 协议类型
     *
     * @return
     */
    ProtocolType protocol();

    /**
     * 接口名称
     *
     * @return
     */
    String name();

    /**
     * 备注
     *
     * @return
     */
    String remark();


//    /**
//     * 业务处理类
//     *
//     * @return
//     */
//    Class handler();


    int port() default -1;
}
