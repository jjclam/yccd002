package com.sinosoft.cloud.access.entity;

/**
 * @Author:lizhen
 * @Description:
 * @Date:Created in 15:43 2018/11/16
 * @Modified By:
 */
/*
* 4003查询保单详细-续保方式变更 pos065返回
* POS112
* */
public class RespRenewalChangeDetailsQuery {

    //保全项目编码
    private String EdorType;
    //保单号
    private String ContNo;
    //投保人姓名
    private String AppntName;
    //被保人姓名
    private String InsuredName;
    //总保费
    private String TotalPrem;
    //赔付日期/缴费对应日
    private String PayDate;
    //主险编码
    private String MainRiskCode;
    //主险名称
    private String MainRiskName;
    //保额
    private String Amnt;
    //保费
    private String Prem;
    //份数
    private String Mult;
    //生存金领取标志/续保标记
    private String XBFlag;
    //生存金领取方式/续保方式
    private String XBPattern;
    //缴费xxx账号
    private String BankAccNo;
    //缴费xxx名称
    private String BankName;
    //新增首期保费交费形式
    private String FirstPayMode;
    //续保方式
    private String RenealMode;
    //附加险
    private AddtRisks AddtRisks;
    //总保额
    private String TotalAmnt;
    //主险
    private MainRisks MainRisks;
    public String getEdorType() {
        return EdorType;
    }

    public void setEdorType(String edorType) {
        EdorType = edorType;
    }

    public String getContNo() {
        return ContNo;
    }

    public void setContNo(String contNo) {
        ContNo = contNo;
    }

    public String getAppntName() {
        return AppntName;
    }

    public void setAppntName(String appntName) {
        AppntName = appntName;
    }

    public String getInsuredName() {
        return InsuredName;
    }

    public void setInsuredName(String insuredName) {
        InsuredName = insuredName;
    }

    public String getTotalPrem() {
        return TotalPrem;
    }

    public void setTotalPrem(String totalPrem) {
        TotalPrem = totalPrem;
    }

    public String getPayDate() {
        return PayDate;
    }

    public void setPayDate(String payDate) {
        PayDate = payDate;
    }

    public String getMainRiskCode() {
        return MainRiskCode;
    }

    public void setMainRiskCode(String mainRiskCode) {
        MainRiskCode = mainRiskCode;
    }

    public String getMainRiskName() {
        return MainRiskName;
    }

    public void setMainRiskName(String mainRiskName) {
        MainRiskName = mainRiskName;
    }

    public String getAmnt() {
        return Amnt;
    }

    public void setAmnt(String amnt) {
        Amnt = amnt;
    }

    public String getPrem() {
        return Prem;
    }

    public void setPrem(String prem) {
        Prem = prem;
    }

    public String getMult() {
        return Mult;
    }

    public void setMult(String mult) {
        Mult = mult;
    }

    public String getXBFlag() {
        return XBFlag;
    }

    public void setXBFlag(String XBFlag) {
        this.XBFlag = XBFlag;
    }

    public String getXBPattern() {
        return XBPattern;
    }

    public void setXBPattern(String XBPattern) {
        this.XBPattern = XBPattern;
    }

    public String getBankAccNo() {
        return BankAccNo;
    }

    public void setBankAccNo(String bankAccNo) {
        BankAccNo = bankAccNo;
    }

    public String getBankName() {
        return BankName;
    }

    public void setBankName(String bankName) {
        BankName = bankName;
    }

    public String getFirstPayMode() {
        return FirstPayMode;
    }

    public void setFirstPayMode(String firstPayMode) {
        FirstPayMode = firstPayMode;
    }

    public String getRenealMode() {
        return RenealMode;
    }

    public void setRenealMode(String renealMode) {
        RenealMode = renealMode;
    }

    public com.sinosoft.cloud.access.entity.AddtRisks getAddtRisks() {
        return AddtRisks;
    }

    public void setAddtRisks(com.sinosoft.cloud.access.entity.AddtRisks addtRisks) {
        AddtRisks = addtRisks;
    }

    public String getTotalAmnt() {
        return TotalAmnt;
    }

    public void setTotalAmnt(String totalAmnt) {
        TotalAmnt = totalAmnt;
    }

    public com.sinosoft.cloud.access.entity.MainRisks getMainRisks() {
        return MainRisks;
    }

    public void setMainRisks(com.sinosoft.cloud.access.entity.MainRisks mainRisks) {
        MainRisks = mainRisks;
    }

    @Override
    public String toString() {
        return "RespRenewalChangeDetailsQuery{" +
                "EdorType='" + EdorType + '\'' +
                ", ContNo='" + ContNo + '\'' +
                ", AppntName='" + AppntName + '\'' +
                ", InsuredName='" + InsuredName + '\'' +
                ", TotalPrem='" + TotalPrem + '\'' +
                ", PayDate='" + PayDate + '\'' +
                ", MainRiskCode='" + MainRiskCode + '\'' +
                ", MainRiskName='" + MainRiskName + '\'' +
                ", Amnt='" + Amnt + '\'' +
                ", Prem='" + Prem + '\'' +
                ", Mult='" + Mult + '\'' +
                ", XBFlag='" + XBFlag + '\'' +
                ", XBPattern='" + XBPattern + '\'' +
                ", BankAccNo='" + BankAccNo + '\'' +
                ", BankName='" + BankName + '\'' +
                ", FirstPayMode='" + FirstPayMode + '\'' +
                ", RenealMode='" + RenealMode + '\'' +
                ", AddtRisks=" + AddtRisks +
                ", TotalAmnt='" + TotalAmnt + '\'' +
                ", MainRisks=" + MainRisks +
                '}';
    }
}