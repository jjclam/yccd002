package com.sinosoft.cloud.access.entity;

/**
 * @Author:lizhen
 * @Description:
 * @Date:Created in 22:18 2018/11/12
 * @Modified By:
 */

/**
 * 4004保单贷款清偿申请POS006. 请求报文
 */
public class ReqPolicyLoanClearApply {
    //保全项目编码
    private String EdorType;
    //保单号
    private String ContNo;
    //是否还息不还本
    private String IsInterest;
    //清偿金额
    private String PayOffMoney;
    //申请人姓名
    private String CustomerName;
    //申请人身份证号码
    private String CustomerIdNo;
    //申请日期
    private String ApplyDate;
    //是否为美国纳税义务的个人
    private String TinFlag;
    //美国纳税人识别号
    private String TinNo;
    //收付费方式
    private String PayMode;
    private EdorAppInfo EdorAppInfo;
    private PersonInfo PersonInfo;
    private BankInfo BankInfo;
    //贷款本金
    private String LoanMoney;
    //清偿方式
    private String CompenMode;
    public String getEdorType() {
        return EdorType;
    }

    public void setEdorType(String edorType) {
        EdorType = edorType;
    }

    public String getContNo() {
        return ContNo;
    }

    public void setContNo(String contNo) {
        ContNo = contNo;
    }

    public String getIsInterest() {
        return IsInterest;
    }

    public void setIsInterest(String isInterest) {
        IsInterest = isInterest;
    }

    public String getPayOffMoney() {
        return PayOffMoney;
    }

    public void setPayOffMoney(String payOffMoney) {
        PayOffMoney = payOffMoney;
    }

    public String getCustomerName() {
        return CustomerName;
    }

    public void setCustomerName(String customerName) {
        CustomerName = customerName;
    }

    public String getCustomerIdNo() {
        return CustomerIdNo;
    }

    public void setCustomerIdNo(String customerIdNo) {
        CustomerIdNo = customerIdNo;
    }

    public String getApplyDate() {
        return ApplyDate;
    }

    public void setApplyDate(String applyDate) {
        ApplyDate = applyDate;
    }

    public String getTinFlag() {
        return TinFlag;
    }

    public void setTinFlag(String tinFlag) {
        TinFlag = tinFlag;
    }

    public String getTinNo() {
        return TinNo;
    }

    public void setTinNo(String tinNo) {
        TinNo = tinNo;
    }

    public String getPayMode() {
        return PayMode;
    }

    public void setPayMode(String payMode) {
        PayMode = payMode;
    }

    public com.sinosoft.cloud.access.entity.EdorAppInfo getEdorAppInfo() {
        return EdorAppInfo;
    }

    public void setEdorAppInfo(com.sinosoft.cloud.access.entity.EdorAppInfo edorAppInfo) {
        EdorAppInfo = edorAppInfo;
    }

    public com.sinosoft.cloud.access.entity.PersonInfo getPersonInfo() {
        return PersonInfo;
    }

    public void setPersonInfo(com.sinosoft.cloud.access.entity.PersonInfo personInfo) {
        PersonInfo = personInfo;
    }

    public com.sinosoft.cloud.access.entity.BankInfo getBankInfo() {
        return BankInfo;
    }

    public void setBankInfo(com.sinosoft.cloud.access.entity.BankInfo bankInfo) {
        BankInfo = bankInfo;
    }

    public String getLoanMoney() {
        return LoanMoney;
    }

    public void setLoanMoney(String loanMoney) {
        LoanMoney = loanMoney;
    }

    public String getCompenMode() {
        return CompenMode;
    }

    public void setCompenMode(String compenMode) {
        CompenMode = compenMode;
    }

    @Override
    public String toString() {
        return "ReqPolicyLoanClearApply{" +
                "EdorType='" + EdorType + '\'' +
                ", ContNo='" + ContNo + '\'' +
                ", IsInterest='" + IsInterest + '\'' +
                ", PayOffMoney='" + PayOffMoney + '\'' +
                ", CustomerName='" + CustomerName + '\'' +
                ", CustomerIdNo='" + CustomerIdNo + '\'' +
                ", ApplyDate='" + ApplyDate + '\'' +
                ", TinFlag='" + TinFlag + '\'' +
                ", TinNo='" + TinNo + '\'' +
                ", PayMode='" + PayMode + '\'' +
                ", EdorAppInfo=" + EdorAppInfo +
                ", PersonInfo=" + PersonInfo +
                ", BankInfo=" + BankInfo +
                ", LoanMoney='" + LoanMoney + '\'' +
                ", CompenMode='" + CompenMode + '\'' +
                '}';
    }
}