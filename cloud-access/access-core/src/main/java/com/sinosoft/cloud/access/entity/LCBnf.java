package com.sinosoft.cloud.access.entity;

/**
 * @Description
 * @Author:zhaoshulei
 * @Date:2018/4/10
 */
public class LCBnf {
    private long BnfID;
    private long PolID;
    private long InsuredID;
    private String ShardingID;
    private String ContNo;
    private String PolNo;
    private String InsuredNo;
    private String BnfType;
    private int BnfNo;
    private String BnfGrade;
    private String RelationToInsured;
    private double BnfLot;
    private String CustomerNo;
    private String Name;
    private String Sex;
    private String Birthday;
    private String IDType;
    private String IDNo;
    private String Operator;
    private String MakeDate;
    private String MakeTime;
    private String ModifyDate;
    private String ModifyTime;
    private String Tel;
    private String Address;
    private String ZipCode;
    private String IdValiDate;
    private String BankCode;
    private String BankAccNo;
    private String AccName;
    private int BeneficiaryOrder;

    public int getBeneficiaryOrder() {
        return BeneficiaryOrder;
    }

    public void setBeneficiaryOrder(int beneficiaryOrder) {
        BeneficiaryOrder = beneficiaryOrder;
    }

    public long getBnfID() {
        return BnfID;
    }

    public void setBnfID(long bnfID) {
        BnfID = bnfID;
    }

    public long getPolID() {
        return PolID;
    }

    public void setPolID(long polID) {
        PolID = polID;
    }

    public long getInsuredID() {
        return InsuredID;
    }

    public void setInsuredID(long insuredID) {
        InsuredID = insuredID;
    }

    public String getShardingID() {
        return ShardingID;
    }

    public void setShardingID(String shardingID) {
        ShardingID = shardingID;
    }

    public String getContNo() {
        return ContNo;
    }

    public void setContNo(String contNo) {
        ContNo = contNo;
    }

    public String getPolNo() {
        return PolNo;
    }

    public void setPolNo(String polNo) {
        PolNo = polNo;
    }

    public String getInsuredNo() {
        return InsuredNo;
    }

    public void setInsuredNo(String insuredNo) {
        InsuredNo = insuredNo;
    }

    public String getBnfType() {
        return BnfType;
    }

    public void setBnfType(String bnfType) {
        BnfType = bnfType;
    }

    public int getBnfNo() {
        return BnfNo;
    }

    public void setBnfNo(int bnfNo) {
        BnfNo = bnfNo;
    }

    public String getBnfGrade() {
        return BnfGrade;
    }

    public void setBnfGrade(String bnfGrade) {
        BnfGrade = bnfGrade;
    }

    public String getRelationToInsured() {
        return RelationToInsured;
    }

    public void setRelationToInsured(String relationToInsured) {
        RelationToInsured = relationToInsured;
    }

    public double getBnfLot() {
        return BnfLot;
    }

    public void setBnfLot(double bnfLot) {
        BnfLot = bnfLot;
    }

    public String getCustomerNo() {
        return CustomerNo;
    }

    public void setCustomerNo(String customerNo) {
        CustomerNo = customerNo;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getSex() {
        return Sex;
    }

    public void setSex(String sex) {
        Sex = sex;
    }

    public String getBirthday() {
        return Birthday;
    }

    public void setBirthday(String birthday) {
        Birthday = birthday;
    }

    public String getIDType() {
        return IDType;
    }

    public void setIDType(String IDType) {
        this.IDType = IDType;
    }

    public String getIDNo() {
        return IDNo;
    }

    public void setIDNo(String IDNo) {
        this.IDNo = IDNo;
    }

    public String getOperator() {
        return Operator;
    }

    public void setOperator(String operator) {
        Operator = operator;
    }

    public String getMakeDate() {
        return MakeDate;
    }

    public void setMakeDate(String makeDate) {
        MakeDate = makeDate;
    }

    public String getMakeTime() {
        return MakeTime;
    }

    public void setMakeTime(String makeTime) {
        MakeTime = makeTime;
    }

    public String getModifyDate() {
        return ModifyDate;
    }

    public void setModifyDate(String modifyDate) {
        ModifyDate = modifyDate;
    }

    public String getModifyTime() {
        return ModifyTime;
    }

    public void setModifyTime(String modifyTime) {
        ModifyTime = modifyTime;
    }

    public String getTel() {
        return Tel;
    }

    public void setTel(String tel) {
        Tel = tel;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String address) {
        Address = address;
    }

    public String getZipCode() {
        return ZipCode;
    }

    public void setZipCode(String zipCode) {
        ZipCode = zipCode;
    }

    public String getIdValiDate() {
        return IdValiDate;
    }

    public void setIdValiDate(String idValiDate) {
        IdValiDate = idValiDate;
    }

    public String getBankCode() {
        return BankCode;
    }

    public void setBankCode(String bankCode) {
        BankCode = bankCode;
    }

    public String getBankAccNo() {
        return BankAccNo;
    }

    public void setBankAccNo(String bankAccNo) {
        BankAccNo = bankAccNo;
    }

    public String getAccName() {
        return AccName;
    }

    public void setAccName(String accName) {
        AccName = accName;
    }

    @Override
    public String toString() {
        return "LCBnf{" +
                "BnfID=" + BnfID +
                ", PolID=" + PolID +
                ", InsuredID=" + InsuredID +
                ", ShardingID='" + ShardingID + '\'' +
                ", ContNo='" + ContNo + '\'' +
                ", PolNo='" + PolNo + '\'' +
                ", InsuredNo='" + InsuredNo + '\'' +
                ", BnfType='" + BnfType + '\'' +
                ", BnfNo=" + BnfNo +
                ", BnfGrade='" + BnfGrade + '\'' +
                ", RelationToInsured='" + RelationToInsured + '\'' +
                ", BnfLot=" + BnfLot +
                ", CustomerNo='" + CustomerNo + '\'' +
                ", Name='" + Name + '\'' +
                ", Sex='" + Sex + '\'' +
                ", Birthday='" + Birthday + '\'' +
                ", IDType='" + IDType + '\'' +
                ", IDNo='" + IDNo + '\'' +
                ", Operator='" + Operator + '\'' +
                ", MakeDate='" + MakeDate + '\'' +
                ", MakeTime='" + MakeTime + '\'' +
                ", ModifyDate='" + ModifyDate + '\'' +
                ", ModifyTime='" + ModifyTime + '\'' +
                ", Tel='" + Tel + '\'' +
                ", Address='" + Address + '\'' +
                ", ZipCode='" + ZipCode + '\'' +
                ", IdValiDate='" + IdValiDate + '\'' +
                ", BankCode='" + BankCode + '\'' +
                ", BankAccNo='" + BankAccNo + '\'' +
                ", AccName='" + AccName + '\'' +
                '}';
    }
}
