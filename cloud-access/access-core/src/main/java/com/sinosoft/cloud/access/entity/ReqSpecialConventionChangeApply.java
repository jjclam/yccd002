package com.sinosoft.cloud.access.entity;


/**
 * 特别约定（出境告知）变更保全申请POS081
 */
public class ReqSpecialConventionChangeApply {
    //保全项目编码
    private String EdorType;
    //保单号
    private String ContNo;
    //申请日期
    private String ApplyDate;
    //特别约定信息
    private String SPMessage;
    //申请人信息
    private EdorAppInfo EdorAppInfo;
    //领取人
    private PersonInfo PersonInfo;
    //xxx
    private BankInfo BankInfo;
    //收付费方式
    private String PayMode;

    public String getEdorType() {
        return EdorType;
    }

    public void setEdorType(String edorType) {
        EdorType = edorType;
    }

    public String getContNo() {
        return ContNo;
    }

    public void setContNo(String contNo) {
        ContNo = contNo;
    }

    public String getApplyDate() {
        return ApplyDate;
    }

    public void setApplyDate(String applyDate) {
        ApplyDate = applyDate;
    }

    public String getSPMessage() {
        return SPMessage;
    }

    public void setSPMessage(String SPMessage) {
        this.SPMessage = SPMessage;
    }

    public com.sinosoft.cloud.access.entity.EdorAppInfo getEdorAppInfo() {
        return EdorAppInfo;
    }

    public void setEdorAppInfo(com.sinosoft.cloud.access.entity.EdorAppInfo edorAppInfo) {
        EdorAppInfo = edorAppInfo;
    }

    public com.sinosoft.cloud.access.entity.PersonInfo getPersonInfo() {
        return PersonInfo;
    }

    public void setPersonInfo(com.sinosoft.cloud.access.entity.PersonInfo personInfo) {
        PersonInfo = personInfo;
    }

    public com.sinosoft.cloud.access.entity.BankInfo getBankInfo() {
        return BankInfo;
    }

    public void setBankInfo(com.sinosoft.cloud.access.entity.BankInfo bankInfo) {
        BankInfo = bankInfo;
    }

    public String getPayMode() {
        return PayMode;
    }

    public void setPayMode(String payMode) {
        PayMode = payMode;
    }

    @Override
    public String toString() {
        return "ReqSpecialConventionChangeApply{" +
                "EdorType='" + EdorType + '\'' +
                ", ContNo='" + ContNo + '\'' +
                ", ApplyDate='" + ApplyDate + '\'' +
                ", SPMessage='" + SPMessage + '\'' +
                ", EdorAppInfo=" + EdorAppInfo +
                ", PersonInfo=" + PersonInfo +
                ", BankInfo=" + BankInfo +
                ", PayMode='" + PayMode + '\'' +
                '}';
    }
}
