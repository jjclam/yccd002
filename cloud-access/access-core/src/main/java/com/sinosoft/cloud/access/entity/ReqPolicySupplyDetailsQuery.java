package com.sinosoft.cloud.access.entity;

/**
 * 保单补发详情查询POS002.
 */
public class ReqPolicySupplyDetailsQuery {
    //保全项目编码
    private String EdorType;
    //保单号
    private String ContNo;

    public String getEdorType() {
        return EdorType;
    }

    public void setEdorType(String edorType) {
        EdorType = edorType;
    }

    public String getContNo() {
        return ContNo;
    }

    public void setContNo(String contNo) {
        ContNo = contNo;
    }

    @Override
    public String toString() {
        return "ReqPolicySupplyDetailsQuery{" +
                "EdorType='" + EdorType + '\'' +
                ", ContNo='" + ContNo + '\'' +
                '}';
    }

}
