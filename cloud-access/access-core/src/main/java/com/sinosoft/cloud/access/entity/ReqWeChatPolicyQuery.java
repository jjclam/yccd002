package com.sinosoft.cloud.access.entity;

/**
 * @program: abc-cloud-access
 * @description: 微信保单查询
 * @author: BaoYongmeng
 * @create: 2019-02-19 14:10
 **/
public class ReqWeChatPolicyQuery {

    private Agent Agent;
    private String RiskCode;
    private String PolicyNo;
    private String Policypwd;

    public com.sinosoft.cloud.access.entity.Agent getAgent() {
        return Agent;
    }

    public void setAgent(com.sinosoft.cloud.access.entity.Agent agent) {
        Agent = agent;
    }

    public String getRiskCode() {
        return RiskCode;
    }

    public void setRiskCode(String riskCode) {
        RiskCode = riskCode;
    }

    public String getPolicyNo() {
        return PolicyNo;
    }

    public void setPolicyNo(String policyNo) {
        PolicyNo = policyNo;
    }

    public String getPolicypwd() {
        return Policypwd;
    }

    public void setPolicypwd(String policypwd) {
        Policypwd = policypwd;
    }

    @Override
    public String toString() {
        return "ReqWeChatPolicyQuery{" +
                "Agent=" + Agent +
                ", RiskCode='" + RiskCode + '\'' +
                ", PolicyNo='" + PolicyNo + '\'' +
                ", Policypwd='" + Policypwd + '\'' +
                '}';
    }
}
