package com.sinosoft.cloud.access.entity;

/**
 * @Description
 * @Author:zhaoshulei
 * @Date:2018/4/13
 */
public class ResponseObj {
    private String PolicyStatus;
    private String PolicyPledge;
    private String AppResult;
    private String Amt;
    private String FeeAmt;

    public String getPolicyStatus() {
        return PolicyStatus;
    }

    public void setPolicyStatus(String policyStatus) {
        PolicyStatus = policyStatus;
    }

    public String getPolicyPledge() {
        return PolicyPledge;
    }

    public void setPolicyPledge(String policyPledge) {
        PolicyPledge = policyPledge;
    }

    public String getAppResult() {
        return AppResult;
    }

    public void setAppResult(String appResult) {
        AppResult = appResult;
    }

    public String getAmt() {
        return Amt;
    }

    public void setAmt(String amt) {
        Amt = amt;
    }

    public String getFeeAmt() {
        return FeeAmt;
    }

    public void setFeeAmt(String feeAmt) {
        FeeAmt = feeAmt;
    }

    @Override
    public String toString() {
        return "ResponseObj{" +
                "PolicyStatus='" + PolicyStatus + '\'' +
                ", PolicyPledge='" + PolicyPledge + '\'' +
                ", AppResult='" + AppResult + '\'' +
                ", Amt='" + Amt + '\'' +
                ", FeeAmt='" + FeeAmt + '\'' +
                '}';
    }
}
