package com.sinosoft.cloud.access.entity;



/**
 * @Author:lizhen
 * @Description:
 * @Date:Created in 22:50 2018/11/13
 * @Modified By:
 */

import com.sinosoft.lis.pos.entity.PhoneInfosPojo;

/**
 * 4002保费自垫清偿列表查询POS007,POS119. 返回报文
 */
public class RespPremDictionaryClearListQuery {
    //保全项目编码
    private String EdorType;
    private PolicyQueryInfos PolicyQueryInfos;
    private PhoneInfos PhoneInfos;

    public String getEdorType() {
        return EdorType;
    }

    public void setEdorType(String edorType) {
        EdorType = edorType;
    }

    public com.sinosoft.cloud.access.entity.PolicyQueryInfos getPolicyQueryInfos() {
        return PolicyQueryInfos;
    }

    public void setPolicyQueryInfos(com.sinosoft.cloud.access.entity.PolicyQueryInfos policyQueryInfos) {
        PolicyQueryInfos = policyQueryInfos;
    }

    public com.sinosoft.cloud.access.entity.PhoneInfos getPhoneInfos() {
        return PhoneInfos;
    }

    public void setPhoneInfos(com.sinosoft.cloud.access.entity.PhoneInfos phoneInfos) {
        PhoneInfos = phoneInfos;
    }

    @Override
    public String toString() {
        return "RespPremDictionaryClearListQuery{" +
                "EdorType='" + EdorType + '\'' +
                ", PolicyQueryInfos=" + PolicyQueryInfos +
                ", PhoneInfos=" + PhoneInfos +
                '}';
    }
}