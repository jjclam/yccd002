package com.sinosoft.cloud.access.net;

import com.sinosoft.utility.ExceptionUtils;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.util.ReferenceCountUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.nio.charset.Charset;
import java.util.concurrent.ConcurrentHashMap;

import static io.netty.channel.ChannelFutureListener.CLOSE;

/**
 * abc-cloud-access
 *
 * @title: abc-cloud-access
 * @package: com.sinosoft.cloud.access.net
 * @author: yangming
 * @date: 2018/3/12 下午2:13
 */
public abstract class AbstractNettySupportHandler extends ChannelInboundHandlerAdapter implements HttpHandler {


    protected final Log logger = LogFactory.getLog(getClass());
    protected final Log monitorLog = LogFactory.getLog("monitor");
    private static final ConcurrentHashMap<String, String> messageHashTable = new ConcurrentHashMap<>();


    @Override
    public void channelReadComplete(ChannelHandlerContext ctx) throws Exception {
        super.channelReadComplete(ctx);
    }

    /**
     * 当客户端强制重发或者断开，将HashTable中当前线程所对应的key和value删除
     *
     * @param ctx
     * @throws Exception
     */
    @Override
    public void channelInactive(ChannelHandlerContext ctx) throws Exception {
        super.channelInactive(ctx);
    }


    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {
        cause.printStackTrace();
        if (cause instanceof Exception) {
            logger.error("发生异常错误:" + ExceptionUtils.exceptionToString((Exception) cause));
        } else {
            logger.error("发生异常错误getMessage:" + cause.getMessage());
            logger.error("发生异常错误cause:", cause);
        }
        ctx.close();
    }


    /**
     * 接受netty Socket 消息
     *
     * @param ctx
     * @param msg
     */
    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) {
        try {
            if (msg instanceof ByteBuf) {
                long start = System.currentTimeMillis();
                messageHashTable.put(getThreadKey(), String.valueOf(start));
                ByteBuf in = (ByteBuf) msg;


                if (in == null) {
                    throw new RuntimeException("netty 传入消息为null");
                }
                final String strMsg = in.toString(Charset.defaultCharset());
                if (strMsg == null) {
                    throw new RuntimeException("netty 解析报文为null");
                }

                String result = submitData(strMsg);

                byte[] gbkBytes = result.getBytes();
                if (gbkBytes == null) {
                    throw new RuntimeException("获取报文byte数组为null");
                }
                ByteBuf encoded = Unpooled.copiedBuffer(gbkBytes);
                ctx.writeAndFlush(encoded).addListener(CLOSE);
                messageHashTable.remove(getThreadKey());
            }
        } finally {
            ReferenceCountUtil.release(msg);
        }
    }

    /**
     * 用于存储当前正在运行的 线程数量. 用于限流\业务负载统计等.
     *
     * @return
     */
    public static ConcurrentHashMap<String, String> getMessageHashTable() {
        return messageHashTable;
    }

    /**
     * 存储键值
     *
     * @return
     */
    protected String getThreadKey() {
        return getClass() + ";" + Thread.currentThread().getName() + ":" + Thread.currentThread().getId();
    }
}
