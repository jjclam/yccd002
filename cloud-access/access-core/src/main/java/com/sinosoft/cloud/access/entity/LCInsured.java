package com.sinosoft.cloud.access.entity;

/**
 * @Description
 * @Author:zhaoshulei
 * @Date:2018/4/10
 */
public class LCInsured {
    private long InsuredID;
    private long ContID;
    private long PersonID;
    private String ShardingID;
    private String GrpContNo;
    private String ContNo;
    private String InsuredNo;
    private String PrtNo;
    private String AppntNo;
    private String ManageCom;
    private String ExecuteCom;
    private String FamilyID;
    private String RelationToMainInsured;
    private String RelationToAppnt;
    private String AddressNo;
    private String SequenceNo;
    private String Name;
    private String Sex;
    private String Birthday;
    private String IDType;
    private String IDNo;
    private String NativePlace;
    private String Nationality;
    private String RgtAddress;
    private String Marriage;
    private String MarriageDate;
    private String Health;
    private double Stature;
    private double Avoirdupois;
    private String Degree;
    private String CreditGrade;
    private String BankCode;
    private String BankAccNo;
    private String AccName;
    private String JoinCompanyDate;
    private String StartWorkDate;
    private String Position;
    private double Salary;
    private String OccupationType;
    private String OccupationCode;
    private String WorkType;
    private String PluralityType;
    private String SmokeFlag;
    private String ContPlanCode;
    private String Operator;
    private String InsuredStat;
    private String MakeDate;
    private String MakeTime;
    private String ModifyDate;
    private String ModifyTime;
    private String UWFlag;
    private String UWCode;
    private String UWDate;
    private String UWTime;
    private double BMI;
    private int InsuredPeoples;
    private String License;
    private String LicenseType;
    private int CustomerSeqNo;
    private String WorkNo;
    private String SocialInsuNo;
    private String OccupationDesb;
    private String IdValiDate;
    private String HaveMotorcycleLicence;
    private String PartTimeJob;
    private String HealthFlag;
    private String ServiceMark;
    private String FirstName;
    private String LastName;
    private String SSFlag;
    private String TINNO;
    private String TINFlag;
    private String IsMainInsured;

    public String getIsMainInsured() {
        return IsMainInsured;
    }

    public void setIsMainInsured(String isMainInsured) {
        IsMainInsured = isMainInsured;
    }

    public long getInsuredID() {
        return InsuredID;
    }

    public void setInsuredID(long insuredID) {
        InsuredID = insuredID;
    }

    public long getContID() {
        return ContID;
    }

    public void setContID(long contID) {
        ContID = contID;
    }

    public long getPersonID() {
        return PersonID;
    }

    public void setPersonID(long personID) {
        PersonID = personID;
    }

    public String getShardingID() {
        return ShardingID;
    }

    public void setShardingID(String shardingID) {
        ShardingID = shardingID;
    }

    public String getGrpContNo() {
        return GrpContNo;
    }

    public void setGrpContNo(String grpContNo) {
        GrpContNo = grpContNo;
    }

    public String getContNo() {
        return ContNo;
    }

    public void setContNo(String contNo) {
        ContNo = contNo;
    }

    public String getInsuredNo() {
        return InsuredNo;
    }

    public void setInsuredNo(String insuredNo) {
        InsuredNo = insuredNo;
    }

    public String getPrtNo() {
        return PrtNo;
    }

    public void setPrtNo(String prtNo) {
        PrtNo = prtNo;
    }

    public String getAppntNo() {
        return AppntNo;
    }

    public void setAppntNo(String appntNo) {
        AppntNo = appntNo;
    }

    public String getManageCom() {
        return ManageCom;
    }

    public void setManageCom(String manageCom) {
        ManageCom = manageCom;
    }

    public String getExecuteCom() {
        return ExecuteCom;
    }

    public void setExecuteCom(String executeCom) {
        ExecuteCom = executeCom;
    }

    public String getFamilyID() {
        return FamilyID;
    }

    public void setFamilyID(String familyID) {
        FamilyID = familyID;
    }

    public String getRelationToMainInsured() {
        return RelationToMainInsured;
    }

    public void setRelationToMainInsured(String relationToMainInsured) {
        RelationToMainInsured = relationToMainInsured;
    }

    public String getRelationToAppnt() {
        return RelationToAppnt;
    }

    public void setRelationToAppnt(String relationToAppnt) {
        RelationToAppnt = relationToAppnt;
    }

    public String getAddressNo() {
        return AddressNo;
    }

    public void setAddressNo(String addressNo) {
        AddressNo = addressNo;
    }

    public String getSequenceNo() {
        return SequenceNo;
    }

    public void setSequenceNo(String sequenceNo) {
        SequenceNo = sequenceNo;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getSex() {
        return Sex;
    }

    public void setSex(String sex) {
        Sex = sex;
    }

    public String getBirthday() {
        return Birthday;
    }

    public void setBirthday(String birthday) {
        Birthday = birthday;
    }

    public String getIDType() {
        return IDType;
    }

    public void setIDType(String IDType) {
        this.IDType = IDType;
    }

    public String getIDNo() {
        return IDNo;
    }

    public void setIDNo(String IDNo) {
        this.IDNo = IDNo;
    }

    public String getNativePlace() {
        return NativePlace;
    }

    public void setNativePlace(String nativePlace) {
        NativePlace = nativePlace;
    }

    public String getNationality() {
        return Nationality;
    }

    public void setNationality(String nationality) {
        Nationality = nationality;
    }

    public String getRgtAddress() {
        return RgtAddress;
    }

    public void setRgtAddress(String rgtAddress) {
        RgtAddress = rgtAddress;
    }

    public String getMarriage() {
        return Marriage;
    }

    public void setMarriage(String marriage) {
        Marriage = marriage;
    }

    public String getMarriageDate() {
        return MarriageDate;
    }

    public void setMarriageDate(String marriageDate) {
        MarriageDate = marriageDate;
    }

    public String getHealth() {
        return Health;
    }

    public void setHealth(String health) {
        Health = health;
    }

    public double getStature() {
        return Stature;
    }

    public void setStature(double stature) {
        Stature = stature;
    }

    public double getAvoirdupois() {
        return Avoirdupois;
    }

    public void setAvoirdupois(double avoirdupois) {
        Avoirdupois = avoirdupois;
    }

    public String getDegree() {
        return Degree;
    }

    public void setDegree(String degree) {
        Degree = degree;
    }

    public String getCreditGrade() {
        return CreditGrade;
    }

    public void setCreditGrade(String creditGrade) {
        CreditGrade = creditGrade;
    }

    public String getBankCode() {
        return BankCode;
    }

    public void setBankCode(String bankCode) {
        BankCode = bankCode;
    }

    public String getBankAccNo() {
        return BankAccNo;
    }

    public void setBankAccNo(String bankAccNo) {
        BankAccNo = bankAccNo;
    }

    public String getAccName() {
        return AccName;
    }

    public void setAccName(String accName) {
        AccName = accName;
    }

    public String getJoinCompanyDate() {
        return JoinCompanyDate;
    }

    public void setJoinCompanyDate(String joinCompanyDate) {
        JoinCompanyDate = joinCompanyDate;
    }

    public String getStartWorkDate() {
        return StartWorkDate;
    }

    public void setStartWorkDate(String startWorkDate) {
        StartWorkDate = startWorkDate;
    }

    public String getPosition() {
        return Position;
    }

    public void setPosition(String position) {
        Position = position;
    }

    public double getSalary() {
        return Salary;
    }

    public void setSalary(double salary) {
        Salary = salary;
    }

    public String getOccupationType() {
        return OccupationType;
    }

    public void setOccupationType(String occupationType) {
        OccupationType = occupationType;
    }

    public String getOccupationCode() {
        return OccupationCode;
    }

    public void setOccupationCode(String occupationCode) {
        OccupationCode = occupationCode;
    }

    public String getWorkType() {
        return WorkType;
    }

    public void setWorkType(String workType) {
        WorkType = workType;
    }

    public String getPluralityType() {
        return PluralityType;
    }

    public void setPluralityType(String pluralityType) {
        PluralityType = pluralityType;
    }

    public String getSmokeFlag() {
        return SmokeFlag;
    }

    public void setSmokeFlag(String smokeFlag) {
        SmokeFlag = smokeFlag;
    }

    public String getContPlanCode() {
        return ContPlanCode;
    }

    public void setContPlanCode(String contPlanCode) {
        ContPlanCode = contPlanCode;
    }

    public String getOperator() {
        return Operator;
    }

    public void setOperator(String operator) {
        Operator = operator;
    }

    public String getInsuredStat() {
        return InsuredStat;
    }

    public void setInsuredStat(String insuredStat) {
        InsuredStat = insuredStat;
    }

    public String getMakeDate() {
        return MakeDate;
    }

    public void setMakeDate(String makeDate) {
        MakeDate = makeDate;
    }

    public String getMakeTime() {
        return MakeTime;
    }

    public void setMakeTime(String makeTime) {
        MakeTime = makeTime;
    }

    public String getModifyDate() {
        return ModifyDate;
    }

    public void setModifyDate(String modifyDate) {
        ModifyDate = modifyDate;
    }

    public String getModifyTime() {
        return ModifyTime;
    }

    public void setModifyTime(String modifyTime) {
        ModifyTime = modifyTime;
    }

    public String getUWFlag() {
        return UWFlag;
    }

    public void setUWFlag(String UWFlag) {
        this.UWFlag = UWFlag;
    }

    public String getUWCode() {
        return UWCode;
    }

    public void setUWCode(String UWCode) {
        this.UWCode = UWCode;
    }

    public String getUWDate() {
        return UWDate;
    }

    public void setUWDate(String UWDate) {
        this.UWDate = UWDate;
    }

    public String getUWTime() {
        return UWTime;
    }

    public void setUWTime(String UWTime) {
        this.UWTime = UWTime;
    }

    public double getBMI() {
        return BMI;
    }

    public void setBMI(double BMI) {
        this.BMI = BMI;
    }

    public int getInsuredPeoples() {
        return InsuredPeoples;
    }

    public void setInsuredPeoples(int insuredPeoples) {
        InsuredPeoples = insuredPeoples;
    }

    public String getLicense() {
        return License;
    }

    public void setLicense(String license) {
        License = license;
    }

    public String getLicenseType() {
        return LicenseType;
    }

    public void setLicenseType(String licenseType) {
        LicenseType = licenseType;
    }

    public int getCustomerSeqNo() {
        return CustomerSeqNo;
    }

    public void setCustomerSeqNo(int customerSeqNo) {
        CustomerSeqNo = customerSeqNo;
    }

    public String getWorkNo() {
        return WorkNo;
    }

    public void setWorkNo(String workNo) {
        WorkNo = workNo;
    }

    public String getSocialInsuNo() {
        return SocialInsuNo;
    }

    public void setSocialInsuNo(String socialInsuNo) {
        SocialInsuNo = socialInsuNo;
    }

    public String getOccupationDesb() {
        return OccupationDesb;
    }

    public void setOccupationDesb(String occupationDesb) {
        OccupationDesb = occupationDesb;
    }

    public String getIdValiDate() {
        return IdValiDate;
    }

    public void setIdValiDate(String idValiDate) {
        IdValiDate = idValiDate;
    }

    public String getHaveMotorcycleLicence() {
        return HaveMotorcycleLicence;
    }

    public void setHaveMotorcycleLicence(String haveMotorcycleLicence) {
        HaveMotorcycleLicence = haveMotorcycleLicence;
    }

    public String getPartTimeJob() {
        return PartTimeJob;
    }

    public void setPartTimeJob(String partTimeJob) {
        PartTimeJob = partTimeJob;
    }

    public String getHealthFlag() {
        return HealthFlag;
    }

    public void setHealthFlag(String healthFlag) {
        HealthFlag = healthFlag;
    }

    public String getServiceMark() {
        return ServiceMark;
    }

    public void setServiceMark(String serviceMark) {
        ServiceMark = serviceMark;
    }

    public String getFirstName() {
        return FirstName;
    }

    public void setFirstName(String firstName) {
        FirstName = firstName;
    }

    public String getLastName() {
        return LastName;
    }

    public void setLastName(String lastName) {
        LastName = lastName;
    }

    public String getSSFlag() {
        return SSFlag;
    }

    public void setSSFlag(String SSFlag) {
        this.SSFlag = SSFlag;
    }

    public String getTINNO() {
        return TINNO;
    }

    public void setTINNO(String TINNO) {
        this.TINNO = TINNO;
    }

    public String getTINFlag() {
        return TINFlag;
    }

    public void setTINFlag(String TINFlag) {
        this.TINFlag = TINFlag;
    }

    @Override
    public String toString() {
        return "LCInsured{" +
                "InsuredID=" + InsuredID +
                ", ContID=" + ContID +
                ", PersonID=" + PersonID +
                ", ShardingID='" + ShardingID + '\'' +
                ", GrpContNo='" + GrpContNo + '\'' +
                ", ContNo='" + ContNo + '\'' +
                ", InsuredNo='" + InsuredNo + '\'' +
                ", PrtNo='" + PrtNo + '\'' +
                ", AppntNo='" + AppntNo + '\'' +
                ", ManageCom='" + ManageCom + '\'' +
                ", ExecuteCom='" + ExecuteCom + '\'' +
                ", FamilyID='" + FamilyID + '\'' +
                ", RelationToMainInsured='" + RelationToMainInsured + '\'' +
                ", RelationToAppnt='" + RelationToAppnt + '\'' +
                ", AddressNo='" + AddressNo + '\'' +
                ", SequenceNo='" + SequenceNo + '\'' +
                ", Name='" + Name + '\'' +
                ", Sex='" + Sex + '\'' +
                ", Birthday='" + Birthday + '\'' +
                ", IDType='" + IDType + '\'' +
                ", IDNo='" + IDNo + '\'' +
                ", NativePlace='" + NativePlace + '\'' +
                ", Nationality='" + Nationality + '\'' +
                ", RgtAddress='" + RgtAddress + '\'' +
                ", Marriage='" + Marriage + '\'' +
                ", MarriageDate='" + MarriageDate + '\'' +
                ", Health='" + Health + '\'' +
                ", Stature=" + Stature +
                ", Avoirdupois=" + Avoirdupois +
                ", Degree='" + Degree + '\'' +
                ", CreditGrade='" + CreditGrade + '\'' +
                ", BankCode='" + BankCode + '\'' +
                ", BankAccNo='" + BankAccNo + '\'' +
                ", AccName='" + AccName + '\'' +
                ", JoinCompanyDate='" + JoinCompanyDate + '\'' +
                ", StartWorkDate='" + StartWorkDate + '\'' +
                ", Position='" + Position + '\'' +
                ", Salary=" + Salary +
                ", OccupationType='" + OccupationType + '\'' +
                ", OccupationCode='" + OccupationCode + '\'' +
                ", WorkType='" + WorkType + '\'' +
                ", PluralityType='" + PluralityType + '\'' +
                ", SmokeFlag='" + SmokeFlag + '\'' +
                ", ContPlanCode='" + ContPlanCode + '\'' +
                ", Operator='" + Operator + '\'' +
                ", InsuredStat='" + InsuredStat + '\'' +
                ", MakeDate='" + MakeDate + '\'' +
                ", MakeTime='" + MakeTime + '\'' +
                ", ModifyDate='" + ModifyDate + '\'' +
                ", ModifyTime='" + ModifyTime + '\'' +
                ", UWFlag='" + UWFlag + '\'' +
                ", UWCode='" + UWCode + '\'' +
                ", UWDate='" + UWDate + '\'' +
                ", UWTime='" + UWTime + '\'' +
                ", BMI=" + BMI +
                ", InsuredPeoples=" + InsuredPeoples +
                ", License='" + License + '\'' +
                ", LicenseType='" + LicenseType + '\'' +
                ", CustomerSeqNo=" + CustomerSeqNo +
                ", WorkNo='" + WorkNo + '\'' +
                ", SocialInsuNo='" + SocialInsuNo + '\'' +
                ", OccupationDesb='" + OccupationDesb + '\'' +
                ", IdValiDate='" + IdValiDate + '\'' +
                ", HaveMotorcycleLicence='" + HaveMotorcycleLicence + '\'' +
                ", PartTimeJob='" + PartTimeJob + '\'' +
                ", HealthFlag='" + HealthFlag + '\'' +
                ", ServiceMark='" + ServiceMark + '\'' +
                ", FirstName='" + FirstName + '\'' +
                ", LastName='" + LastName + '\'' +
                ", SSFlag='" + SSFlag + '\'' +
                ", TINNO='" + TINNO + '\'' +
                ", TINFlag='" + TINFlag + '\'' +
                '}';
    }
}
