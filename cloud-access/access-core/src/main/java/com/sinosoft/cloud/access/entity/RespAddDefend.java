package com.sinosoft.cloud.access.entity;

/**
 * @program: abc-cloud-access
 * @description: 4012-加保接口POS116.
 * @author: BaoYongmeng
 * @create: 2018-12-24 16:24
 **/
public class RespAddDefend {
    //保全项目编码
    private String EdorType;
    //保单号
    private String ContNo;
    //保全受理号
    private String EdorAcceptNo;
    //批单号
    private String EdorNo;

    public void setEdorType(String edorType) {
        EdorType = edorType;
    }

    public void setContNo(String contNo) {
        ContNo = contNo;
    }

    public void setEdorAcceptNo(String edorAcceptNo) {
        EdorAcceptNo = edorAcceptNo;
    }

    public void setEdorNo(String edorNo) {
        EdorNo = edorNo;
    }

    public String getEdorType() {
        return EdorType;
    }

    public String getContNo() {
        return ContNo;
    }

    public String getEdorAcceptNo() {
        return EdorAcceptNo;
    }

    public String getEdorNo() {
        return EdorNo;
    }

    @Override
    public String toString() {
        return "RespAddDefend{" +
                "EdorType='" + EdorType + '\'' +
                ", ContNo='" + ContNo + '\'' +
                ", EdorAcceptNo='" + EdorAcceptNo + '\'' +
                ", EdorNo='" + EdorNo + '\'' +
                '}';
    }
}
