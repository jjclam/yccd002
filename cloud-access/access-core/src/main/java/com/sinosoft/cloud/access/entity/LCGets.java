package com.sinosoft.cloud.access.entity;

import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.List;

/**
 * Created by Administrator on 2018/4/16.
 */
public class LCGets {
    @XStreamImplicit(itemFieldName="LCGet")
    private List<LCGet> LCGets;

    public List<LCGet> getLCGets() {
        return LCGets;
    }

    public void setLCGets(List<LCGet> LCGets) {
        this.LCGets = LCGets;
    }

    @Override
    public String toString() {
        return "LCGets{" +
                "LCGets=" + LCGets +
                '}';
    }
}
