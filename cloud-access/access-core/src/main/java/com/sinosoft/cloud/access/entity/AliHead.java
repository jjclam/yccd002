package com.sinosoft.cloud.access.entity;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("AliHead")
public class AliHead {
    /*版本号*/
    @XStreamAlias("version")
    private String version;
    /*接口代码*/
    @XStreamAlias("function")
    private String function;
    /*发送时间*/
    @XStreamAlias("transTime")
    private String transTime;
    /*时区*/
    @XStreamAlias("transTimeZone")
    private String transTimeZone;
    /*请求报文id*/
    @XStreamAlias("reqMsgId")
    private String reqMsgId;
    /*数据格式*/
    @XStreamAlias("format")
    private String format;
    /*签名类型*/
    @XStreamAlias("signType")
    private String signType;
    /*同异步*/
    @XStreamAlias("asyn")
    private String asyn;
    /*合作伙伴id*/
    @XStreamAlias("cid")
    private String cid;

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getFunction() {
        return function;
    }

    public void setFunction(String function) {
        this.function = function;
    }

    public String getTransTime() {
        return transTime;
    }

    public void setTransTime(String transTime) {
        this.transTime = transTime;
    }

    public String getTransTimeZone() {
        return transTimeZone;
    }

    public void setTransTimeZone(String transTimeZone) {
        this.transTimeZone = transTimeZone;
    }

    public String getReqMsgId() {
        return reqMsgId;
    }

    public void setReqMsgId(String reqMsgId) {
        this.reqMsgId = reqMsgId;
    }

    public String getFormat() {
        return format;
    }

    public void setFormat(String format) {
        this.format = format;
    }

    public String getSignType() {
        return signType;
    }

    public void setSignType(String signType) {
        this.signType = signType;
    }

    public String getAsyn() {
        return asyn;
    }

    public void setAsyn(String asyn) {
        this.asyn = asyn;
    }

    public String getCid() {
        return cid;
    }

    public void setCid(String cid) {
        this.cid = cid;
    }

    @Override
    public String toString() {
        return "AliHead{" +
                "version='" + version + '\'' +
                ", function='" + function + '\'' +
                ", transTime='" + transTime + '\'' +
                ", transTimeZone='" + transTimeZone + '\'' +
                ", reqMsgId='" + reqMsgId + '\'' +
                ", format='" + format + '\'' +
                ", signType='" + signType + '\'' +
                ", asyn='" + asyn + '\'' +
                ", cid='" + cid + '\'' +
                '}';
    }
}