package com.sinosoft.cloud.access.entity;

/**
 * @Description
 * @Author:zhaoshulei
 * @Date:2018/4/10
 */
public class LMRiskApp {
    private String RiskCode;
    private String RiskVer;
    private String RiskName;
    private String KindCode;
    private String RiskType;
    private String RiskType1;
    private String RiskType2;
    private String RiskProp;
    private String RiskPeriod;
    private String RiskTypeDetail;
    private String RiskFlag;
    private String PolType;
    private String InvestFlag;
    private String BonusFlag;
    private String BonusMode;
    private String ListFlag;
    private String SubRiskFlag;
    private int CalDigital;
    private String CalChoMode;
    private int RiskAmntMult;
    private String InsuPeriodFlag;
    private int MaxEndPeriod;
    private int AgeLmt;
    private int SignDateCalMode;
    private String ProtocolFlag;
    private String GetChgFlag;
    private String ProtocolPayFlag;
    private String EnsuPlanFlag;
    private String EnsuPlanAdjFlag;
    private String StartDate;
    private String EndDate;
    private int MinAppntAge;
    private int MaxAppntAge;
    private int MaxInsuredAge;
    private int MinInsuredAge;
    private double AppInterest;
    private double AppPremRate;
    private String InsuredFlag;
    private String ShareFlag;
    private String BnfFlag;
    private String TempPayFlag;
    private String InpPayPlan;
    private String ImpartFlag;
    private String InsuExpeFlag;
    private String LoanFalg;
    private String MortagageFlag;
    private String IDifReturnFlag;
    private String CutAmntStopPay;
    private double RinsRate;
    private String SaleFlag;
    private String FileAppFlag;
    private String MngCom;
    private String AutoPayFlag;
    private String NeedPrintHospital;
    private String NeedPrintGet;
    private String RiskType3;
    private String RiskType4;
    private String RiskType5;
    private String NotPrintPol;
    private String NeedGetPolDate;
    private String NeedReReadBank;
    private String SpecFlag;
    private String InterestDifFlag;

    public String getRiskCode() {
        return RiskCode;
    }

    public void setRiskCode(String riskCode) {
        RiskCode = riskCode;
    }

    public String getRiskVer() {
        return RiskVer;
    }

    public void setRiskVer(String riskVer) {
        RiskVer = riskVer;
    }

    public String getRiskName() {
        return RiskName;
    }

    public void setRiskName(String riskName) {
        RiskName = riskName;
    }

    public String getKindCode() {
        return KindCode;
    }

    public void setKindCode(String kindCode) {
        KindCode = kindCode;
    }

    public String getRiskType() {
        return RiskType;
    }

    public void setRiskType(String riskType) {
        RiskType = riskType;
    }

    public String getRiskType1() {
        return RiskType1;
    }

    public void setRiskType1(String riskType1) {
        RiskType1 = riskType1;
    }

    public String getRiskType2() {
        return RiskType2;
    }

    public void setRiskType2(String riskType2) {
        RiskType2 = riskType2;
    }

    public String getRiskProp() {
        return RiskProp;
    }

    public void setRiskProp(String riskProp) {
        RiskProp = riskProp;
    }

    public String getRiskPeriod() {
        return RiskPeriod;
    }

    public void setRiskPeriod(String riskPeriod) {
        RiskPeriod = riskPeriod;
    }

    public String getRiskTypeDetail() {
        return RiskTypeDetail;
    }

    public void setRiskTypeDetail(String riskTypeDetail) {
        RiskTypeDetail = riskTypeDetail;
    }

    public String getRiskFlag() {
        return RiskFlag;
    }

    public void setRiskFlag(String riskFlag) {
        RiskFlag = riskFlag;
    }

    public String getPolType() {
        return PolType;
    }

    public void setPolType(String polType) {
        PolType = polType;
    }

    public String getInvestFlag() {
        return InvestFlag;
    }

    public void setInvestFlag(String investFlag) {
        InvestFlag = investFlag;
    }

    public String getBonusFlag() {
        return BonusFlag;
    }

    public void setBonusFlag(String bonusFlag) {
        BonusFlag = bonusFlag;
    }

    public String getBonusMode() {
        return BonusMode;
    }

    public void setBonusMode(String bonusMode) {
        BonusMode = bonusMode;
    }

    public String getListFlag() {
        return ListFlag;
    }

    public void setListFlag(String listFlag) {
        ListFlag = listFlag;
    }

    public String getSubRiskFlag() {
        return SubRiskFlag;
    }

    public void setSubRiskFlag(String subRiskFlag) {
        SubRiskFlag = subRiskFlag;
    }

    public int getCalDigital() {
        return CalDigital;
    }

    public void setCalDigital(int calDigital) {
        CalDigital = calDigital;
    }

    public String getCalChoMode() {
        return CalChoMode;
    }

    public void setCalChoMode(String calChoMode) {
        CalChoMode = calChoMode;
    }

    public int getRiskAmntMult() {
        return RiskAmntMult;
    }

    public void setRiskAmntMult(int riskAmntMult) {
        RiskAmntMult = riskAmntMult;
    }

    public String getInsuPeriodFlag() {
        return InsuPeriodFlag;
    }

    public void setInsuPeriodFlag(String insuPeriodFlag) {
        InsuPeriodFlag = insuPeriodFlag;
    }

    public int getMaxEndPeriod() {
        return MaxEndPeriod;
    }

    public void setMaxEndPeriod(int maxEndPeriod) {
        MaxEndPeriod = maxEndPeriod;
    }

    public int getAgeLmt() {
        return AgeLmt;
    }

    public void setAgeLmt(int ageLmt) {
        AgeLmt = ageLmt;
    }

    public int getSignDateCalMode() {
        return SignDateCalMode;
    }

    public void setSignDateCalMode(int signDateCalMode) {
        SignDateCalMode = signDateCalMode;
    }

    public String getProtocolFlag() {
        return ProtocolFlag;
    }

    public void setProtocolFlag(String protocolFlag) {
        ProtocolFlag = protocolFlag;
    }

    public String getGetChgFlag() {
        return GetChgFlag;
    }

    public void setGetChgFlag(String getChgFlag) {
        GetChgFlag = getChgFlag;
    }

    public String getProtocolPayFlag() {
        return ProtocolPayFlag;
    }

    public void setProtocolPayFlag(String protocolPayFlag) {
        ProtocolPayFlag = protocolPayFlag;
    }

    public String getEnsuPlanFlag() {
        return EnsuPlanFlag;
    }

    public void setEnsuPlanFlag(String ensuPlanFlag) {
        EnsuPlanFlag = ensuPlanFlag;
    }

    public String getEnsuPlanAdjFlag() {
        return EnsuPlanAdjFlag;
    }

    public void setEnsuPlanAdjFlag(String ensuPlanAdjFlag) {
        EnsuPlanAdjFlag = ensuPlanAdjFlag;
    }

    public String getStartDate() {
        return StartDate;
    }

    public void setStartDate(String startDate) {
        StartDate = startDate;
    }

    public String getEndDate() {
        return EndDate;
    }

    public void setEndDate(String endDate) {
        EndDate = endDate;
    }

    public int getMinAppntAge() {
        return MinAppntAge;
    }

    public void setMinAppntAge(int minAppntAge) {
        MinAppntAge = minAppntAge;
    }

    public int getMaxAppntAge() {
        return MaxAppntAge;
    }

    public void setMaxAppntAge(int maxAppntAge) {
        MaxAppntAge = maxAppntAge;
    }

    public int getMaxInsuredAge() {
        return MaxInsuredAge;
    }

    public void setMaxInsuredAge(int maxInsuredAge) {
        MaxInsuredAge = maxInsuredAge;
    }

    public int getMinInsuredAge() {
        return MinInsuredAge;
    }

    public void setMinInsuredAge(int minInsuredAge) {
        MinInsuredAge = minInsuredAge;
    }

    public double getAppInterest() {
        return AppInterest;
    }

    public void setAppInterest(double appInterest) {
        AppInterest = appInterest;
    }

    public double getAppPremRate() {
        return AppPremRate;
    }

    public void setAppPremRate(double appPremRate) {
        AppPremRate = appPremRate;
    }

    public String getInsuredFlag() {
        return InsuredFlag;
    }

    public void setInsuredFlag(String insuredFlag) {
        InsuredFlag = insuredFlag;
    }

    public String getShareFlag() {
        return ShareFlag;
    }

    public void setShareFlag(String shareFlag) {
        ShareFlag = shareFlag;
    }

    public String getBnfFlag() {
        return BnfFlag;
    }

    public void setBnfFlag(String bnfFlag) {
        BnfFlag = bnfFlag;
    }

    public String getTempPayFlag() {
        return TempPayFlag;
    }

    public void setTempPayFlag(String tempPayFlag) {
        TempPayFlag = tempPayFlag;
    }

    public String getInpPayPlan() {
        return InpPayPlan;
    }

    public void setInpPayPlan(String inpPayPlan) {
        InpPayPlan = inpPayPlan;
    }

    public String getImpartFlag() {
        return ImpartFlag;
    }

    public void setImpartFlag(String impartFlag) {
        ImpartFlag = impartFlag;
    }

    public String getInsuExpeFlag() {
        return InsuExpeFlag;
    }

    public void setInsuExpeFlag(String insuExpeFlag) {
        InsuExpeFlag = insuExpeFlag;
    }

    public String getLoanFalg() {
        return LoanFalg;
    }

    public void setLoanFalg(String loanFalg) {
        LoanFalg = loanFalg;
    }

    public String getMortagageFlag() {
        return MortagageFlag;
    }

    public void setMortagageFlag(String mortagageFlag) {
        MortagageFlag = mortagageFlag;
    }

    public String getIDifReturnFlag() {
        return IDifReturnFlag;
    }

    public void setIDifReturnFlag(String IDifReturnFlag) {
        this.IDifReturnFlag = IDifReturnFlag;
    }

    public String getCutAmntStopPay() {
        return CutAmntStopPay;
    }

    public void setCutAmntStopPay(String cutAmntStopPay) {
        CutAmntStopPay = cutAmntStopPay;
    }

    public double getRinsRate() {
        return RinsRate;
    }

    public void setRinsRate(double rinsRate) {
        RinsRate = rinsRate;
    }

    public String getSaleFlag() {
        return SaleFlag;
    }

    public void setSaleFlag(String saleFlag) {
        SaleFlag = saleFlag;
    }

    public String getFileAppFlag() {
        return FileAppFlag;
    }

    public void setFileAppFlag(String fileAppFlag) {
        FileAppFlag = fileAppFlag;
    }

    public String getMngCom() {
        return MngCom;
    }

    public void setMngCom(String mngCom) {
        MngCom = mngCom;
    }

    public String getAutoPayFlag() {
        return AutoPayFlag;
    }

    public void setAutoPayFlag(String autoPayFlag) {
        AutoPayFlag = autoPayFlag;
    }

    public String getNeedPrintHospital() {
        return NeedPrintHospital;
    }

    public void setNeedPrintHospital(String needPrintHospital) {
        NeedPrintHospital = needPrintHospital;
    }

    public String getNeedPrintGet() {
        return NeedPrintGet;
    }

    public void setNeedPrintGet(String needPrintGet) {
        NeedPrintGet = needPrintGet;
    }

    public String getRiskType3() {
        return RiskType3;
    }

    public void setRiskType3(String riskType3) {
        RiskType3 = riskType3;
    }

    public String getRiskType4() {
        return RiskType4;
    }

    public void setRiskType4(String riskType4) {
        RiskType4 = riskType4;
    }

    public String getRiskType5() {
        return RiskType5;
    }

    public void setRiskType5(String riskType5) {
        RiskType5 = riskType5;
    }

    public String getNotPrintPol() {
        return NotPrintPol;
    }

    public void setNotPrintPol(String notPrintPol) {
        NotPrintPol = notPrintPol;
    }

    public String getNeedGetPolDate() {
        return NeedGetPolDate;
    }

    public void setNeedGetPolDate(String needGetPolDate) {
        NeedGetPolDate = needGetPolDate;
    }

    public String getNeedReReadBank() {
        return NeedReReadBank;
    }

    public void setNeedReReadBank(String needReReadBank) {
        NeedReReadBank = needReReadBank;
    }

    public String getSpecFlag() {
        return SpecFlag;
    }

    public void setSpecFlag(String specFlag) {
        SpecFlag = specFlag;
    }

    public String getInterestDifFlag() {
        return InterestDifFlag;
    }

    public void setInterestDifFlag(String interestDifFlag) {
        InterestDifFlag = interestDifFlag;
    }

    @Override
    public String toString() {
        return "LMRiskApp{" +
                "RiskCode='" + RiskCode + '\'' +
                ", RiskVer='" + RiskVer + '\'' +
                ", RiskName='" + RiskName + '\'' +
                ", KindCode='" + KindCode + '\'' +
                ", RiskType='" + RiskType + '\'' +
                ", RiskType1='" + RiskType1 + '\'' +
                ", RiskType2='" + RiskType2 + '\'' +
                ", RiskProp='" + RiskProp + '\'' +
                ", RiskPeriod='" + RiskPeriod + '\'' +
                ", RiskTypeDetail='" + RiskTypeDetail + '\'' +
                ", RiskFlag='" + RiskFlag + '\'' +
                ", PolType='" + PolType + '\'' +
                ", InvestFlag='" + InvestFlag + '\'' +
                ", BonusFlag='" + BonusFlag + '\'' +
                ", BonusMode='" + BonusMode + '\'' +
                ", ListFlag='" + ListFlag + '\'' +
                ", SubRiskFlag='" + SubRiskFlag + '\'' +
                ", CalDigital=" + CalDigital +
                ", CalChoMode='" + CalChoMode + '\'' +
                ", RiskAmntMult=" + RiskAmntMult +
                ", InsuPeriodFlag='" + InsuPeriodFlag + '\'' +
                ", MaxEndPeriod=" + MaxEndPeriod +
                ", AgeLmt=" + AgeLmt +
                ", SignDateCalMode=" + SignDateCalMode +
                ", ProtocolFlag='" + ProtocolFlag + '\'' +
                ", GetChgFlag='" + GetChgFlag + '\'' +
                ", ProtocolPayFlag='" + ProtocolPayFlag + '\'' +
                ", EnsuPlanFlag='" + EnsuPlanFlag + '\'' +
                ", EnsuPlanAdjFlag='" + EnsuPlanAdjFlag + '\'' +
                ", StartDate='" + StartDate + '\'' +
                ", EndDate='" + EndDate + '\'' +
                ", MinAppntAge=" + MinAppntAge +
                ", MaxAppntAge=" + MaxAppntAge +
                ", MaxInsuredAge=" + MaxInsuredAge +
                ", MinInsuredAge=" + MinInsuredAge +
                ", AppInterest=" + AppInterest +
                ", AppPremRate=" + AppPremRate +
                ", InsuredFlag='" + InsuredFlag + '\'' +
                ", ShareFlag='" + ShareFlag + '\'' +
                ", BnfFlag='" + BnfFlag + '\'' +
                ", TempPayFlag='" + TempPayFlag + '\'' +
                ", InpPayPlan='" + InpPayPlan + '\'' +
                ", ImpartFlag='" + ImpartFlag + '\'' +
                ", InsuExpeFlag='" + InsuExpeFlag + '\'' +
                ", LoanFalg='" + LoanFalg + '\'' +
                ", MortagageFlag='" + MortagageFlag + '\'' +
                ", IDifReturnFlag='" + IDifReturnFlag + '\'' +
                ", CutAmntStopPay='" + CutAmntStopPay + '\'' +
                ", RinsRate=" + RinsRate +
                ", SaleFlag='" + SaleFlag + '\'' +
                ", FileAppFlag='" + FileAppFlag + '\'' +
                ", MngCom='" + MngCom + '\'' +
                ", AutoPayFlag='" + AutoPayFlag + '\'' +
                ", NeedPrintHospital='" + NeedPrintHospital + '\'' +
                ", NeedPrintGet='" + NeedPrintGet + '\'' +
                ", RiskType3='" + RiskType3 + '\'' +
                ", RiskType4='" + RiskType4 + '\'' +
                ", RiskType5='" + RiskType5 + '\'' +
                ", NotPrintPol='" + NotPrintPol + '\'' +
                ", NeedGetPolDate='" + NeedGetPolDate + '\'' +
                ", NeedReReadBank='" + NeedReReadBank + '\'' +
                ", SpecFlag='" + SpecFlag + '\'' +
                ", InterestDifFlag='" + InterestDifFlag + '\'' +
                '}';
    }
}
