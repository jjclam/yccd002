package com.sinosoft.cloud.access.net;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.timeout.ReadTimeoutException;
import io.netty.util.ReferenceCountUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.nio.charset.Charset;

@ChannelHandler.Sharable
public abstract class RouterHandler extends SocketHandler implements Handler {

    protected final Log logger = LogFactory.getLog(getClass());

    private DecoderFactory decoderFactory = DecoderFactory.getInstance();

    private String orgXml = "";

    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        String encoderXml = send();
        ByteBuf encoded = ctx.alloc().buffer(4 * encoderXml.length());
        logger.debug("已经发送需要转保的报文 : " + encoderXml);
        encoded.writeBytes(encoderXml.getBytes());
        ctx.write(encoded);
        ctx.flush();
    }

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) {
        try {
            if (msg instanceof ByteBuf) {
                ByteBuf in = (ByteBuf) msg;
                String strMsg = in.toString(Charset.defaultCharset());
                receive(strMsg);
                logger.debug("收到XXX返回的报文：" + strMsg);
//                messageHashTable.remove(getThreadKey());
//                messageHashTable.put(getThreadKey(), strMsg);
            }
        } finally {
            ReferenceCountUtil.release(msg);
        }
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {
        if (cause instanceof ReadTimeoutException) {

            String errorStr = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><ABCB2I><Header><RetCode>009999</RetCode><RetMsg>系统繁忙，请稍后重试</RetMsg><SerialNo></SerialNo><InsuSerial/><TransDate></TransDate><TranTime></TranTime><BankCode></BankCode><CorpNo></CorpNo><TransCode></TransCode></Header></ABCB2I>";

            if (needCrypt()) {
                errorStr = encrypt(errorStr.trim());
            }

            Coder coder = decoderFactory.getDecoder(getAccessName());
            if (coder != null) {
                errorStr = coder.encoder(errorStr);
            }

            logger.warn("由于XXX在" + accessControl.getCipReturnTimeOut() + "ms内未返回，则返回错误信息。" + System.getProperty("line.separator")
                    + "解密后的报文：" + getOrgXml() + System.getProperty("line.separator")
                    + "返回报文：" + errorStr);
            receive(errorStr);
        }
    }

    public String getOrgXml() {
        return orgXml;
    }

    public void setOrgXml(String orgXml) {
        this.orgXml = orgXml;
    }

    //    @Override
//    public void channelReadComplete(ChannelHandlerContext ctx) {
//        byte[] completeBytes = messageHashTable.get(getThreadKey());
//        String completeMsg = "";
//        if (null != completeBytes){
//            completeMsg = new String(completeBytes, Charset.defaultCharset());
//        }
//        if (completeMsg == null || completeMsg.equals("")) {
//            logger.error("在channelReadComplete中收到消息为空:" + completeMsg);
////            throw new RuntimeException("在channelReadComplete中收到消息为空:" + completeMsg);
//            return;
//        }
//        logger.debug("channelReadComplete 报文:" + completeMsg);
//        receive(completeMsg);
//        messageHashTable.remove(getThreadKey());
//    }

//    protected abstract void returnMsg(String completeMsg);
//
//    protected abstract String sendMsg();

}
