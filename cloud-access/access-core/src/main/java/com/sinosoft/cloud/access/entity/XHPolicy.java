package com.sinosoft.cloud.access.entity;

/**
 * @Author:lizhen
 * @Description:
 * @Date:Created in 18:20 2018/11/16
 * @Modified By:
 */
public class XHPolicy {
    //保单号
    private String ContNo;
    //文本受理标志
    private String TextAcceptFlag;
    //信函名称
    private String  XHFlag	;
    public String getContNo() {
        return ContNo;
    }

    public void setContNo(String contNo) {
        ContNo = contNo;
    }

    public String getTextAcceptFlag() {
        return TextAcceptFlag;
    }

    public void setTextAcceptFlag(String textAcceptFlag) {
        TextAcceptFlag = textAcceptFlag;
    }

    public String getXHFlag() {
        return XHFlag;
    }

    public void setXHFlag(String XHFlag) {
        this.XHFlag = XHFlag;
    }

    @Override
    public String toString() {
        return "XHPolicy{" +
                "ContNo='" + ContNo + '\'' +
                ", TextAcceptFlag='" + TextAcceptFlag + '\'' +
                ", XHFlag='" + XHFlag + '\'' +
                '}';
    }
}