package com.sinosoft.cloud.access.configuration;

import org.apache.activemq.command.ActiveMQQueue;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsMessagingTemplate;
import org.springframework.stereotype.Component;

import javax.jms.Queue;

/**
 * Created by Administrator on 2017-10-25.
 */
@Component
public class MessageProducer {
    @Autowired
    private JmsMessagingTemplate jmsMessagingTemplate;

    public void send(Queue queue, Object message) {
        this.jmsMessagingTemplate.convertAndSend(queue, message);
    }

    public void send(String topic, Object message) {
        ActiveMQQueue queue = new ActiveMQQueue(topic);
        this.send((Queue)queue, message);
    }
}
