package com.sinosoft.cloud.access.entity;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.List;

/**
 * @Description
 * @Author:zhaoshulei
 * @Date:2017/11/8
 */
@XStreamAlias("ReducedVolumes")
public class ReducedVolumes {
    @XStreamImplicit(itemFieldName="ReducedVolume")
    private List<ReducedVolume> reducedVolumes;

    public List<ReducedVolume> getReducedVolumes() {
        return reducedVolumes;
    }

    public void setReducedVolumes(List<ReducedVolume> reducedVolumes) {
        this.reducedVolumes = reducedVolumes;
    }

    @Override
    public String toString() {
        return "ReducedVolumes{" +
                "reducedVolumes=" + reducedVolumes +
                '}';
    }
}
