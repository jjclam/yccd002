package com.sinosoft.cloud.access.entity;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import java.util.List;

/**
 * @Description
 * @Author:zhaoshulei
 * @Date:2017/11/8
 */
@XStreamAlias("ReducedVolume")
public class ReducedVolume {

    private String riskCode;
    private List<String> reducedVolume;

    public String getRiskCode() {
        return riskCode;
    }

    public void setRiskCode(String riskCode) {
        this.riskCode = riskCode;
    }

    public List<String> getReducedVolume() {
        return reducedVolume;
    }

    public void setReducedVolume(List<String> reducedVolume) {
        this.reducedVolume = reducedVolume;
    }

    @Override
    public String toString() {
        return "ReducedVolume{" +
                "riskCode='" + riskCode + '\'' +
                ", reducedVolume=" + reducedVolume +
                '}';
    }
}
