package com.sinosoft.cloud.access.service;


import com.alibaba.fastjson.JSON;
import com.alibaba.tamper.BeanMapping;
import com.alibaba.tamper.core.config.BeanMappingConfigHelper;
import com.sinosoft.cloud.access.entity.*;
import com.sinosoft.cloud.access.net.AccessControl;
import com.sinosoft.cloud.access.transformer.PojoTrans;
import com.sinosoft.cloud.access.util.TimeUtil;
import com.sinosoft.cloud.rest.TradeInfo;
import com.sinosoft.lis.entity.GlobalPojo;
import com.sinosoft.lis.entity.LCPolPojo;
import com.sinosoft.lis.entity.LCResultInfoPojo;
import com.sinosoft.lis.entity.LJTempFeePojo;
import com.sinosoft.utility.ExceptionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 公共抽象类.
 */
public abstract class AccessServiceBean {

    /**
     * 日志管理
     */
    private final Log cLogger = LogFactory.getLog(this.getClass());
    /**
     * 错误处理类，每个需要错误处理的类中都放置该类
     */
//    public CErrors mErrors = new CErrors();

    /**
     * 若处理过程中出现异常信息，Head中的Flag字段所需要设置的值
     */
    private static final String ONE = "1";
    /**
     * 若出现异常信息，Head中的Desc字段所需要设置的值
     */
    private static final String ERROR_DESC = "内部错误，交易失败";
    /**
     * 若出现异常信息，记录日志的错误前缀
     */
    private static final String ERROR_INFO = "交易失败，错误信息：";


    /**
     * 创建由spring管理的读取配置文件对象
     */
    @Autowired
    AccessControl accessControl;


    /**
     * service
     *
     * @param msg msg
     */

    public TranData service(TranData msg) {

        Global global=msg.getBody().getGlobal();
        String entrustWay=null;
        if(global!=null){
            entrustWay =global.getEntrustWay();
        }

        long startTime = System.currentTimeMillis();
        String serialNo=null;
        if (entrustWay!=null&&"Ali".equals(entrustWay)) {
            serialNo = msg.getAliHead().getReqMsgId();
            cLogger.info(msg.getAliHead().getFunction() + "交易开始！");
        }else{
            serialNo = msg.getHead().getTranNo();
            cLogger.info(msg.getHead().getTransCode() + "交易开始！");
        }

        //将TranData转换为TradeInfo
        TradeInfo tradeInfo = null;
        try {
//            if("A0039".equals(msg.getHead().getTransCode())){
//                tradeInfo=  new TradeInfo();
//                LCImageUpload lcImageUpload = msg.getBody().getLCImageUpload();
//               Map<String,String> lcImageUploadMap = new HashMap<>();
//               List<Map<String,Object>> nodelist= new ArrayList<>();
//                lcImageUploadMap.put("AppCode",lcImageUpload.getAppCode());
//                lcImageUploadMap.put("AppName",lcImageUpload.getAppName());
//                lcImageUploadMap.put("BusiNum",lcImageUpload.getBusiNum());
//                lcImageUploadMap.put("DataType",lcImageUpload.getDataType());
//
//                List<Node> nodeList = msg.getBody().getImages().getNode();
//                for (Node node:nodeList) {
//                    Map<String,Object> nodeMap = new HashMap<>();
//                    List<Map<String,String>>  list=new ArrayList<>();
//                    nodeMap.put("DocType",node.getDocType());
//                    nodeMap.put("PageNum",node.getPageNum());
//                    nodeMap.put("Remark",node.getRemark());
//                    List<Image> imageList = node.getImage();
//                    for (Image Image:imageList) {
//                        Map<String,String> ImaMap = new HashMap<>();
//                        ImaMap.put("ImgName",Image.getImgName());
//                        ImaMap.put("PageCode",Image.getPageCode());
//                        ImaMap.put("ImgData",Image.getImgData());
//                        list.add(ImaMap);
//                    }
//                    nodeMap.put("Image",list);
//                    nodelist.add(nodeMap);
//                }
//                tradeInfo.addData("sendDatalcImageUpload",lcImageUploadMap);
//                tradeInfo.addData("sendDataNode", nodelist);
//            }else {
            tradeInfo = tranTransInfo(msg);
            //  }

        } catch (Exception e) {
            cLogger.error(ExceptionUtils.exceptionToString(e));
            return getErrorTranData(msg, ERROR_DESC);
        }
        cLogger.info("TranData转换为TradeInfo成功，返回结果是：" + tradeInfo);

        String errorStr = "";
        String log = "";

        errorStr = (String) tradeInfo.getData("return");
        log = (String) tradeInfo.getData("log");
        if (null != errorStr && !"".equals(errorStr)) {
            cLogger.warn("交易流水号：" + serialNo + "。" + ERROR_INFO + log);
            return getErrorTranData(msg, errorStr);
        }

        if (tradeInfo.getErrorList().size() > 0) {
            cLogger.error("交易流水号：" + serialNo + "。" + ERROR_INFO + tradeInfo.getErrorList().get(0));
            return getErrorTranData(msg, ERROR_DESC);
        }

        TradeInfo result = new TradeInfo();
        /**
         * 若配置中挡板配置为true，则不调用任何微服务，直接将封装的TradeInfo进行返回xsl转换
         */
        Boolean barrierControl = accessControl.getBarrierControl();
        if (barrierControl) {
            result = test(tradeInfo);
        } else {
            result = service(tradeInfo);
        }

        errorStr = (String) result.getData("return");
        log = (String) result.getData("log");

        if (null != errorStr && !"".equals(errorStr)) {
            cLogger.warn("交易流水号：" + serialNo + "。" + ERROR_INFO + log);
            MDC.put("status", "done_deny");
            return getErrorTranData(msg, errorStr);
        }

        if (result.getErrorList().size() > 0) {
            cLogger.error("交易流水号：" + serialNo + "。" + ERROR_INFO + result.getErrorList().get(0));
            MDC.put("status", "error");
            return getErrorTranData(msg, ERROR_DESC);
        }

        //将TradeInfo转换为TranData
        TranData tranData = null;
        try {
            tranData = transTranData(result, msg);
        } catch (Exception e) {
            cLogger.error(ExceptionUtils.exceptionToString(e));
            errorStr = "报文转换失败！";
            MDC.put("status", "error");
            return getErrorTranData(msg, errorStr);
        }

        long endTime = System.currentTimeMillis();
        // cLogger.debug(TimeUtil.getMillis(startTime, endTime, msg.getHead().getTransCode() + "交易"));
        //cLogger.debug(msg.getHead().getTransCode() + "交易成功！交易结束。");
        MDC.put("status", "done");
        //cLogger.info(TimeUtil.getMillis(startTime, endTime, msg.getHead().getTransCode() + "交易"));
        //cLogger.info(msg.getHead().getTransCode() + "交易成功！交易结束。");
        return tranData;
    }

    /**
     * TranData转换为pojo，封装到tradeinfo
     *
     * @param msg
     * @return
     */
    private TradeInfo tranTransInfo(TranData msg) throws Exception {

        /*标准报文中报文体信息*/
        Body body = null;
        /*标准报文中报文体中险种信息*/
        Risks risks = null;
        /*标准报文中报文体中投保人信息*/
        Appnt appnt = null;
        /*标准报文中报文体中被保人信息*/
        Insured insured = null;
        /*标准报文中报文体中受益人信息*/
        Bnfs bnfs = null;
        /*标准报文中报文头的信息*/
        Head head = null;
        AliHead aliHead=null;
        /*标准报文中报文体中客户告知的信息*/
        CustomerImparts customerImparts = null;

        //获取TranData数据，将数据备份到局部变量中，若有错误，则返回交易失败
        TradeInfo inputData = getInputData(msg);
        if (inputData.getErrorList().size() > 0) {
            return inputData;
        }
        TradeInfo reqTradeInfo = tranToPojo(msg);
        return reqTradeInfo;
    }

    /**
     * TranData转换TradeInfo
     *
     * @param msg
     * @return
     */
    private TradeInfo tranToPojo(TranData msg) throws Exception {
        Global globa=msg.getBody().getGlobal();
        String entrustWay=null;
        if(globa!=null){
            entrustWay=globa.getEntrustWay();
        }
        if(!"Ali".equals(entrustWay)){
            BeanMappingConfigHelper.getInstance().registerConfig("mapping/request-mapping.xml");
        }
        Head head = msg.getHead();
        Body body = msg.getBody();
        AliHead aliHead=msg.getAliHead();
        LCCont lcCont = body.getLCCont();
        Global global = body.getGlobal();

        TradeInfo tradeInfo = PojoTrans.transPojos(body);

        if(aliHead==null&&!"A0021".equals(head.getTransCode())){
            tradeInfo.addData(GlobalPojo.class.getName(), getGlobalPojo(head, tradeInfo, body));
            tradeInfo.addData("serialNo", head.getTranNo());
        }


        if (null != global) {
            tradeInfo.addData("applySerial", global.getApplySerial()); //1004、1005需要传入试算号
        }
        if (null != lcCont) {
            tradeInfo.addData("otherCompanyDieAmnt", body.getOtherCompanyDieAmnt());
            tradeInfo.addData("payPrem", String.valueOf(lcCont.getPrem())); //1004应交保费
            tradeInfo.addData("otherNo", lcCont.getContNo()); //1004保单号
        }
        tradeInfo.addData("RiskCodeWr", body.getRiskCodeWr()); //原交易编码
        tradeInfo.addData("AgainUWFlag", body.getAgainUWFlag()); //再次核保标记
        tradeInfo.addData("WICNewPayMode", body.getWICNewPayMode()); //原交易编码



        tradeInfo = addGroupFlag(tradeInfo, body);

        return tradeInfo;
    }

    private List<LJTempFeePojo> getLJTempFeePojo(Head head, TradeInfo tradeInfo, Body body) {
        List<LCPolPojo> lcPolList = (List<LCPolPojo>) tradeInfo.getData(LCPolPojo.class.getName());
        List<LJTempFeePojo> ljTempFeeList = (List<LJTempFeePojo>) tradeInfo.getData(LJTempFeePojo.class.getName());
        LJTempFeePojo ljTempFeePojo = ljTempFeeList.get(0);
        if (0.0 == ljTempFeePojo.getPayMoney()) {
            double sumprem = 0.00;
            int riskLength = lcPolList.size();
            for (int i = 0; lcPolList != null && i < riskLength; i++) {
                sumprem += lcPolList.get(i).getPrem();
            }
            ljTempFeePojo.setPayMoney(sumprem);
        } else {
            return ljTempFeeList;
        }
        return ljTempFeeList;
    }

    private GlobalPojo getGlobalPojo(Head head, TradeInfo tradeInfo, Body body) {
        GlobalPojo globalPojo = null;
        if (null != tradeInfo.getData(GlobalPojo.class.getName())) {
            globalPojo = (GlobalPojo) tradeInfo.getData(GlobalPojo.class.getName());
            BeanMapping.create(Head.class, GlobalPojo.class).mapping(head, globalPojo);
//            return globalPojo;
        } else {
            globalPojo = new GlobalPojo();
            BeanMapping.create(Head.class, GlobalPojo.class).mapping(head, globalPojo);
//            tradeInfo.addData("healthNotice", body.getHealthNotice());
//            tradeInfo.addData("autoRenew", "-9");
        }
        if ((null != globalPojo && null != body.getLCPols() && null != body.getLCPols().getLCPols() && null != body.getLCPols().getLCPols().get(0) && null != body.getLCPols().getLCPols().get(0).getRiskCode() && body.getLCPols().getLCPols().get(0).getRiskCode().equals("6807"))) {
            if (null != body.getLCInsureds() && null != body.getLCInsureds().getLCInsureds().get(0) && !("").equals(body.getLCInsureds().getLCInsureds().get(0))
                    && body.getLCInsureds().getLCInsureds().get(0).getContPlanCode().equals("Y6")) {
                cLogger.debug("6807 套餐编码是" + body.getLCInsureds().getLCInsureds().get(0).getContPlanCode() + "；xxx渠道");
            } else {
                globalPojo.setBankCode("05");
            }
        }
        tradeInfo.addData("remark", body.getRemark());
        tradeInfo.addData("healthNotice", body.getHealthNotice());
        tradeInfo.addData("autoRenew", "-9");
        return globalPojo;
    }

    /**
     * 从tradeinfo中取pojo，转换为TranData
     *
     * @param respTradeInfo,msg
     * @return
     */
    private TranData transTranData(TradeInfo respTradeInfo, TranData msg) throws Exception {

        msg = pojoToStd(respTradeInfo, msg);

        return msg;
    }

    /**
     * 从TranData获取所有对象，备份到局部变量
     *
     * @param cInputData
     * @return
     */
    private TradeInfo getInputData(TranData cInputData) {

        TradeInfo tradeInfo = new TradeInfo();

        Body body = cInputData.getBody();
        Head head = cInputData.getHead();
        AliHead aliHead=cInputData.getAliHead();


        String errorStr = "";

        if (null == body) {
            errorStr = "接收到报文中的body数据异常";
            tradeInfo.addError(errorStr);
            return tradeInfo;
        }

        if (aliHead==null&&head==null) {
            errorStr = "接收到报文中Head数据异常";
            tradeInfo.addError(errorStr);
            return tradeInfo;
        }
        return tradeInfo;
    }


    /**
     * @param msg           msg
     * @param respTradeInfo respTradeInfo
     *                      将返回的pojo转换为TranData
     */
    public TranData pojoToStd(TradeInfo respTradeInfo, TranData msg) throws Exception {
        BeanMappingConfigHelper.getInstance().registerConfig("mapping/response-mapping.xml");
        Body body = PojoTrans.transTranData(respTradeInfo, msg.getBody());
        msg.setBody(body);
        //公共返回对象
        ResponseObj responseObj = new ResponseObj();
        //1016、1021返回保单状态
        responseObj.setPolicyStatus(String.valueOf(respTradeInfo.getData("policyStatus")));
        //1016、1021返回保单质押状态
        responseObj.setPolicyPledge(String.valueOf(respTradeInfo.getData("policyPledge")));
        //1005返回节点试算结果
        responseObj.setAppResult(String.valueOf(respTradeInfo.getData("AppResult")));
        //1009退保可返还金额
        responseObj.setAmt(String.valueOf(respTradeInfo.getData("Amt")));
        //1009退保手续费
        responseObj.setFeeAmt(String.valueOf(respTradeInfo.getData("FeeAmt")));
        msg.getBody().setOperate(String.valueOf(respTradeInfo.getData("operate")));
        msg.getBody().setOccupationName(String.valueOf(respTradeInfo.getData("OccupationName")));
        msg.getBody().setResponseObj(responseObj);
        msg.getBody().setInsuTime(String.valueOf(respTradeInfo.getData("insuTime")));
        msg.getBody().setDateFlag(String.valueOf(respTradeInfo.getData("DateFlag")));
        msg.getBody().setContType(String.valueOf(respTradeInfo.getData("contType")));
        msg.getBody().setPolicyUrl(String.valueOf(respTradeInfo.getData("policyUrl")));
        /*msg.getHead().setFlag("0");*/
        msg = transferFlag(msg, respTradeInfo);
        //1004转保
        if (null != respTradeInfo.getData("TransferFlag")) {
            msg.getBody().setTransferFlag(String.valueOf(respTradeInfo.getData("TransferFlag")));
        }

        return msg;
    }

    public TranData transferFlag(TranData msg, TradeInfo respTradeInfo) {
        if ("MIP".equals(msg.getHead().getSourceType())) {
            List<LCResultInfoPojo> LCResultInfoList = (List<LCResultInfoPojo>) respTradeInfo.getData(LCResultInfoPojo.class.getName());
            if ("NPR".equals(respTradeInfo.getData("transType"))) {
                //新单复核工作流
                if (null != LCResultInfoList && LCResultInfoList.size() > 0) {
                    String result = new StringBuffer(1024).append("[").append(LCResultInfoList.get(0).getKey1())
                            .append("]:").append(LCResultInfoList.get(0).getResultContent()).toString();
                    cLogger.info(result);
                    if ("Y".equals(LCResultInfoList.get(0).getKey4())) {
                        msg.getHead().setDesc("进入新单复核工作流原因:" + result);
                        msg.getHead().setFlag("07");
                    } else if ("1004".equals(respTradeInfo.getData("access"))) {
                        msg.getHead().setFlag("07");
                        msg.getHead().setDesc("进入新单复核工作流原因:" + result);
                    } else {
                        msg.getHead().setFlag("01");
                        msg.getHead().setDesc(result);
                    }
                }
            } else if ("UWR".equals(respTradeInfo.getData("transType"))) {
                //9人工复核工作流
                String result = new StringBuffer(1024).append("[").append(LCResultInfoList.get(0).getKey1())
                        .append("]:").append(LCResultInfoList.get(0).getResultContent()).toString();
                cLogger.info(result);
                msg.getHead().setFlag("09");
                msg.getHead().setDesc("进入人工核保工作流的原因:" + result);
            } else if ("PTD".equals(respTradeInfo.getData("transType")) && "08".equals(respTradeInfo.getData("ResponseCode"))) {
                //8待签单工作流,  非缴费不足
                msg.getHead().setFlag("08");
                msg.getHead().setDesc("进入待签单工作流的原因:线下收费,非缴费不足");
            } else if ("PTD".equals(respTradeInfo.getData("transType")) && "11".equals(respTradeInfo.getData("ResponseCode"))) {
                msg.getHead().setFlag("11");
                msg.getHead().setDesc("进入待签单工作流的原因:缴费不足");
            } else {
                msg.getHead().setFlag("10");
                if (null != msg.getBody().getRespHead()) {
                    msg.getHead().setDesc(msg.getBody().getRespHead().getRespDesc());
                }
            }
        } else {
            msg.getHead().setFlag("0");
            return msg;
        }
        return msg;
    }


    /**
     * 若在交易过程中出现问题，需要返回的错误报文信息
     *
     * @param errorDesc 错误信息
     * @param msg       msg
     * @return
     */
    public TranData getErrorTranData(TranData msg, String errorDesc) {
        //交易失败返回的头部信息
        msg.getHead().setFlag(ONE);
        msg.getHead().setDesc(errorDesc);

        //交易失败的体信息全部为空
        msg.setBody(new Body());
        msg.getBody().setRisk(new Risk());
        msg.getBody().setRisks(new Risks());
        msg.getBody().setPolicyBasicInfo(new PolicyBasicInfo());
        msg.getBody().getPolicyBasicInfo().setSpecialInfos(new SpecialInfos());
        msg.getBody().setAppnt(new Appnt());
        msg.getBody().setInsureds(new Insureds());
        msg.getBody().setBnfs(new Bnfs());

        cLogger.warn(msg.getHead().getTransCode() + "交易结束！");
        return msg;
    }

    /**
     * 返回异常信息
     *
     * @param errorStr 错误信息
     * @return
     */
    public TradeInfo getErrorTradeInfo(String errorStr) {
        TradeInfo tradeInfo = new TradeInfo();
        tradeInfo.addError(errorStr);
        return tradeInfo;
    }

    /**
     * 返回投保或者核保的异常信息，将异常信息返回给xxx
     * 如果存在异常信息，则返回内部错误，交易失败
     *
     * @param errorStr 错误信息
     * @param errorLog 错误日志
     * @return
     */
    public TradeInfo getCheckRuleFailInfo(String errorStr, String errorLog) {

        //若微服务返回信息含有Exception，则直接返回xxx内部错误，交易失败
        if (errorStr.contains("Exception") || errorStr.contains("EXCEPTION") || errorStr.contains("exception")) {
            return getErrorTradeInfo(errorLog);
        }

        TradeInfo tradeInfo = new TradeInfo();
        tradeInfo.addData("return", errorStr);
        tradeInfo.addData("log", errorLog);
        return tradeInfo;
    }

    /**
     * 应对超时异常，传入微服务开始时间，结束时间，微服务名称，异常信息，统一处理
     *
     * @param startTime   开始时间
     * @param endTime     结束时间
     * @param serviceName service名字
     * @param e           异常
     * @return
     */
    public String getExceptionStr(long startTime, long endTime, String serviceName, Exception e) {

        StringBuffer buffer = new StringBuffer(1024);
        long dealTime = endTime - startTime;

        if (dealTime > accessControl.getReadTimeout()) {
            buffer.append("调用").append(serviceName).append("微服务超时，调用时间：").append(dealTime).append("ms")
                    .append(System.getProperty("line.separator")).append(ExceptionUtils.exceptionToString(e));
        } else {
            buffer.append("调用").append(serviceName).append("微服务异常。")
                    .append(System.getProperty("line.separator")).append(ExceptionUtils.exceptionToString(e));
        }

        return buffer.toString();
    }

    /**
     * 不同的交易实现类调用不同的微服务接口
     *
     * @param msg :参数是转换为pojo后的TradeInfo
     * @return 返回TradeInfo
     */
    public abstract TradeInfo service(TradeInfo msg);

    /**
     * 开启挡板后调用该方法,用于本地测试
     *
     * @param msg msg
     */
    public abstract TradeInfo test(TradeInfo msg);

    /**
     * remove掉微服务不用的pojo,
     *
     * @param tradeInfo tradeInfo
     * @return
     */

    public TradeInfo removePos(TradeInfo tradeInfo) {
        /*tradeInfo.removeData(BQResultPojo.class.getName());
        tradeInfo.removeData(BQQueryPojo.class.getName());
        tradeInfo.removeData(BQApplyPojo.class.getName());
        tradeInfo.removeData(XQJFQueryResultPojo.class.getName());
        tradeInfo.removeData(XQJFQueryPojo.class.getName());*/
        return tradeInfo;
    }

    /**
     * @Description: 添加团单标记
     * @Param: [tradeInfo, body]
     * @return: com.sinosoft.cloud.rest.TradeInfo
     * @Author: BaoYongmeng
     * @Date: 2018/6/22
     */
    public TradeInfo addGroupFlag(TradeInfo tradeInfo, Body body) {
        LCPols lcPols = new LCPols();
        if (body.getLCPols() != null) {
            lcPols = body.getLCPols();
            List<LCPol> list = lcPols.getLCPols();
            if (null == list || list.size() <= 0) {
                return tradeInfo;
            }
            if (!"".equals(list.get(0).getRiskCode()) && null != (list.get(0).getRiskCode())) {
                if ("6807".equals(list.get(0).getRiskCode()) || "6810".equals(list.get(0).getRiskCode())) {
                    tradeInfo.addData("GroupFlag", "grp");
                    cLogger.info("成功添加团单标记");
                } else {
                    tradeInfo.addData("GroupFlag", "ind");
                }
            }
        }
        return tradeInfo;
    }
}
