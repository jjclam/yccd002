package com.sinosoft.cloud.access.entity;

/**
 * @Author:lizhen
 * @Description:
 * @Date:Created in 9:58 2018/12/25
 * @Modified By:
 */
/**
 * 4007保费自垫选择权变更POS111
 */
public class ReqPremSelectApplyWebSite {
    //保全项目编码
    private String EdorType;

    //保单号
    private String ContNo;

    //批单号
    private String EdorNo;

    public String getEdorType() {
        return EdorType;
    }

    public void setEdorType(String edorType) {
        EdorType = edorType;
    }

    public String getContNo() {
        return ContNo;
    }

    public void setContNo(String contNo) {
        ContNo = contNo;
    }

    public String getEdorNo() {
        return EdorNo;
    }

    public void setEdorNo(String edorNo) {
        EdorNo = edorNo;
    }

    @Override
    public String toString() {
        return "ReqPremSelectApplyWebSite{" +
                "EdorType='" + EdorType + '\'' +
                ", ContNo='" + ContNo + '\'' +
                ", EdorNo='" + EdorNo + '\'' +
                '}';
    }
}