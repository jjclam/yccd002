package com.sinosoft.cloud.access.entity;

import com.sinosoft.lis.pos.entity.WecLPSyncImagesRePojo;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.List;

/**
 * abc-cloud-access
 *
 * @author lizhen
 * @create 2019/1/14
 */
public class WecLPSyncImagesRes {
    @XStreamImplicit(itemFieldName="WecLPSyncImagesRe")
    private List<WecLPSyncImagesRe> WecLPSyncImagesRe;

    public List<WecLPSyncImagesRe> getWecLPSyncImagesRe() {
        return WecLPSyncImagesRe;
    }

    public void setWecLPSyncImagesRe(List<WecLPSyncImagesRe> wecLPSyncImagesRe) {
        WecLPSyncImagesRe = wecLPSyncImagesRe;
    }

    @Override
    public String toString() {
        return "WecLPSyncImagesRes{" +
                "WecLPSyncImagesRe=" + WecLPSyncImagesRe +
                '}';
    }
}