package com.sinosoft.cloud.access.entity;

/**
 * @Description
 * @Author:zhaoshulei
 * @Date:2018/4/2
 */
public class Global {

    private String BankCode;
    private String ZoneNo;
    private String BrNo;
    private String TransNo;
    private String MainRiskCode;
    private String AutoRenew;
    private String SerialNo;
    private String TransDate;
    private String TransTime;
    private String EntrustWay;
    private String ContPrtNo;
    private String AgentPersonCode;
    private String TransferFlag;
    private String OldPolicy;
    private String OldPolicyPwd;
    private String OldVchNo;
    private String TellerNo;
    private String CorpNo;
    private String FunctionFlag;
    private String InsuSerial;
    private String InterActNode;
    private String ApplySerial;
    private String PoliValidDate;
    String RiskBeginDate;
    String RiskEndDate;
    //s3 回传的xxx
    String InBankCode;
    //s3 回传的账号
    String InBankAccNo;
    //指定xxx标记
    String BankFlag;

    public String getBankFlag() {
        return BankFlag;
    }

    public void setBankFlag(String bankFlag) {
        BankFlag = bankFlag;
    }

    public String getRiskBeginDate() {
        return RiskBeginDate;
    }

    public String getRiskEndDate() {
        return RiskEndDate;
    }

    public void setRiskBeginDate(String riskBeginDate) {
        RiskBeginDate = riskBeginDate;
    }

    public void setRiskEndDate(String riskEndDate) {
        RiskEndDate = riskEndDate;
    }

    public String getPoliValidDate() {
        return PoliValidDate;
    }

    public void setPoliValidDate(String poliValidDate) {
        PoliValidDate = poliValidDate;
    }

    public String getBankCode() {
        return BankCode;
    }

    public void setBankCode(String bankCode) {
        BankCode = bankCode;
    }

    public String getZoneNo() {
        return ZoneNo;
    }

    public void setZoneNo(String zoneNo) {
        ZoneNo = zoneNo;
    }

    public String getBrNo() {
        return BrNo;
    }

    public void setBrNo(String brNo) {
        BrNo = brNo;
    }

    public String getTransNo() {
        return TransNo;
    }

    public void setTransNo(String transNo) {
        TransNo = transNo;
    }

    public String getMainRiskCode() {
        return MainRiskCode;
    }

    public void setMainRiskCode(String mainRiskCode) {
        MainRiskCode = mainRiskCode;
    }

    public String getAutoRenew() {
        return AutoRenew;
    }

    public void setAutoRenew(String autoRenew) {
        AutoRenew = autoRenew;
    }

    public String getSerialNo() {
        return SerialNo;
    }

    public void setSerialNo(String serialNo) {
        SerialNo = serialNo;
    }

    public String getTransDate() {
        return TransDate;
    }

    public void setTransDate(String transDate) {
        TransDate = transDate;
    }

    public String getTransTime() {
        return TransTime;
    }

    public void setTransTime(String transTime) {
        TransTime = transTime;
    }

    public String getEntrustWay() {
        return EntrustWay;
    }

    public void setEntrustWay(String entrustWay) {
        EntrustWay = entrustWay;
    }

    public String getContPrtNo() {
        return ContPrtNo;
    }

    public void setContPrtNo(String contPrtNo) {
        ContPrtNo = contPrtNo;
    }

    public String getAgentPersonCode() {
        return AgentPersonCode;
    }

    public void setAgentPersonCode(String agentPersonCode) {
        AgentPersonCode = agentPersonCode;
    }

    public String getTransferFlag() {
        return TransferFlag;
    }

    public void setTransferFlag(String transferFlag) {
        TransferFlag = transferFlag;
    }

    public String getOldPolicy() {
        return OldPolicy;
    }

    public void setOldPolicy(String oldPolicy) {
        OldPolicy = oldPolicy;
    }

    public String getOldPolicyPwd() {
        return OldPolicyPwd;
    }

    public void setOldPolicyPwd(String oldPolicyPwd) {
        OldPolicyPwd = oldPolicyPwd;
    }

    public String getOldVchNo() {
        return OldVchNo;
    }

    public void setOldVchNo(String oldVchNo) {
        OldVchNo = oldVchNo;
    }

    public String getTellerNo() {
        return TellerNo;
    }

    public void setTellerNo(String tellerNo) {
        TellerNo = tellerNo;
    }

    public String getCorpNo() {
        return CorpNo;
    }

    public void setCorpNo(String corpNo) {
        CorpNo = corpNo;
    }

    public String getFunctionFlag() {
        return FunctionFlag;
    }

    public void setFunctionFlag(String functionFlag) {
        FunctionFlag = functionFlag;
    }

    public String getInsuSerial() {
        return InsuSerial;
    }

    public void setInsuSerial(String insuSerial) {
        InsuSerial = insuSerial;
    }

    public String getInterActNode() {
        return InterActNode;
    }

    public void setInterActNode(String interActNode) {
        InterActNode = interActNode;
    }

    public String getApplySerial() {
        return ApplySerial;
    }

    public void setApplySerial(String applySerial) {
        ApplySerial = applySerial;
    }

    public String getInBankCode() {
        return InBankCode;
    }

    public String getInBankAccNo() {
        return InBankAccNo;
    }

    public void setInBankCode(String inBankCode) {
        InBankCode = inBankCode;
    }

    public void setInBankAccNo(String inBankAccNo) {
        InBankAccNo = inBankAccNo;
    }

    @Override
    public String toString() {
        return "Global{" +
                "BankCode='" + BankCode + '\'' +
                ", ZoneNo='" + ZoneNo + '\'' +
                ", BrNo='" + BrNo + '\'' +
                ", TransNo='" + TransNo + '\'' +
                ", MainRiskCode='" + MainRiskCode + '\'' +
                ", AutoRenew='" + AutoRenew + '\'' +
                ", SerialNo='" + SerialNo + '\'' +
                ", TransDate='" + TransDate + '\'' +
                ", TransTime='" + TransTime + '\'' +
                ", EntrustWay='" + EntrustWay + '\'' +
                ", ContPrtNo='" + ContPrtNo + '\'' +
                ", AgentPersonCode='" + AgentPersonCode + '\'' +
                ", TransferFlag='" + TransferFlag + '\'' +
                ", OldPolicy='" + OldPolicy + '\'' +
                ", OldPolicyPwd='" + OldPolicyPwd + '\'' +
                ", OldVchNo='" + OldVchNo + '\'' +
                ", TellerNo='" + TellerNo + '\'' +
                ", CorpNo='" + CorpNo + '\'' +
                ", FunctionFlag='" + FunctionFlag + '\'' +
                ", InsuSerial='" + InsuSerial + '\'' +
                ", InterActNode='" + InterActNode + '\'' +
                ", ApplySerial='" + ApplySerial + '\'' +
                ", PoliValidDate='" + PoliValidDate + '\'' +
                ", RiskBeginDate='" + RiskBeginDate + '\'' +
                ", RiskEndDate='" + RiskEndDate + '\'' +
                ", InBankCode='" + InBankCode + '\'' +
                ", InBankAccNo='" + InBankAccNo + '\'' +
                ", BankFlag='" + BankFlag + '\'' +
                '}';
    }
}
