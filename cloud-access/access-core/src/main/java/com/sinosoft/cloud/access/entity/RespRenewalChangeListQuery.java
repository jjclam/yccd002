package com.sinosoft.cloud.access.entity;

/**
 * @Author:lizhen
 * @Description:
 * @Date:Created in 19:37 2018/11/15
 * @Modified By:
 */
/**
 * 4002查询保单列表-续保方式变更POS064,pos109 返回
 */
public class RespRenewalChangeListQuery {
    //保全项目编码
    private String EdorType;
    private PolicyQueryInfos PolicyQueryInfos;
    private PhoneInfos PhoneInfos;

    public com.sinosoft.cloud.access.entity.PhoneInfos getPhoneInfos() {
        return PhoneInfos;
    }

    public void setPhoneInfos(com.sinosoft.cloud.access.entity.PhoneInfos phoneInfos) {
        PhoneInfos = phoneInfos;
    }

    public com.sinosoft.cloud.access.entity.PolicyQueryInfos getPolicyQueryInfos() {
        return PolicyQueryInfos;
    }

    public void setPolicyQueryInfos(com.sinosoft.cloud.access.entity.PolicyQueryInfos policyQueryInfos) {
        PolicyQueryInfos = policyQueryInfos;
    }

    public String getEdorType() {
        return EdorType;
    }

    public void setEdorType(String edorType) {
        EdorType = edorType;
    }

    @Override
    public String toString() {
        return "RespRenewalChangeListQuery{" +
                "EdorType='" + EdorType + '\'' +
                ", PolicyQueryInfos=" + PolicyQueryInfos +
                ", PhoneInfos=" + PhoneInfos +
                '}';
    }
}