package com.sinosoft.cloud.access.entity;
/**
 * @Description
 * @Author:xiaozehua
 * @Date:2018/11/9
 * 白名单查询NB004.
 */
public class RespWhilteQuery {
    //客户是否已存在
    private String DealResult;
    //提示信息
    private String ErrorMessage;

    public String getDealResult() {
        return DealResult;
    }

    public void setDealResult(String dealResult) {
        DealResult = dealResult;
    }

    public String getErrorMessage() {
        return ErrorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        ErrorMessage = errorMessage;
    }
    @Override
    public String toString() {
        return "RespWhilteQuery{" +
                "DealResult='" + DealResult + '\'' +
                ", ErrorMessage='" + ErrorMessage + '\'' +
                '}';
    }

}
