package com.sinosoft.cloud.access.entity;

/**
 * Created by Administrator on 2018/4/16.
 */
public class LCPrem {
    private long PremID;
    private long DutyID;
    private String ShardingID;
    private String GrpContNo;
    private String ContNo;
    private String PolNo;
    private String DutyCode;
    private String PayPlanCode;
    private String PayPlanType;
    private String AppntType;
    private String AppntNo;
    private String UrgePayFlag;
    private String NeedAcc;
    private int PayTimes;
    private double Rate;
    private String PayStartDate;
    private String PayEndDate;
    private String PaytoDate;
    private int PayIntv;
    private double StandPrem;
    private double Prem;
    private double SumPrem;
    private double SuppRiskScore;
    private String FreeFlag;
    private double FreeRate;
    private String FreeStartDate;
    private String FreeEndDate;
    private String State;
    private String ManageCom;
    private String Operator;
    private String MakeDate;
    private String MakeTime;
    private String ModifyDate;
    private String ModifyTime;
    private String AddFeeDirect;
    private double SecInsuAddPoint;

    public long getPremID() {
        return PremID;
    }

    public void setPremID(long premID) {
        PremID = premID;
    }

    public long getDutyID() {
        return DutyID;
    }

    public void setDutyID(long dutyID) {
        DutyID = dutyID;
    }

    public String getShardingID() {
        return ShardingID;
    }

    public void setShardingID(String shardingID) {
        ShardingID = shardingID;
    }

    public String getGrpContNo() {
        return GrpContNo;
    }

    public void setGrpContNo(String grpContNo) {
        GrpContNo = grpContNo;
    }

    public String getContNo() {
        return ContNo;
    }

    public void setContNo(String contNo) {
        ContNo = contNo;
    }

    public String getPolNo() {
        return PolNo;
    }

    public void setPolNo(String polNo) {
        PolNo = polNo;
    }

    public String getDutyCode() {
        return DutyCode;
    }

    public void setDutyCode(String dutyCode) {
        DutyCode = dutyCode;
    }

    public String getPayPlanCode() {
        return PayPlanCode;
    }

    public void setPayPlanCode(String payPlanCode) {
        PayPlanCode = payPlanCode;
    }

    public String getPayPlanType() {
        return PayPlanType;
    }

    public void setPayPlanType(String payPlanType) {
        PayPlanType = payPlanType;
    }

    public String getAppntType() {
        return AppntType;
    }

    public void setAppntType(String appntType) {
        AppntType = appntType;
    }

    public String getAppntNo() {
        return AppntNo;
    }

    public void setAppntNo(String appntNo) {
        AppntNo = appntNo;
    }

    public String getUrgePayFlag() {
        return UrgePayFlag;
    }

    public void setUrgePayFlag(String urgePayFlag) {
        UrgePayFlag = urgePayFlag;
    }

    public String getNeedAcc() {
        return NeedAcc;
    }

    public void setNeedAcc(String needAcc) {
        NeedAcc = needAcc;
    }

    public int getPayTimes() {
        return PayTimes;
    }

    public void setPayTimes(int payTimes) {
        PayTimes = payTimes;
    }

    public double getRate() {
        return Rate;
    }

    public void setRate(double rate) {
        Rate = rate;
    }

    public String getPayStartDate() {
        return PayStartDate;
    }

    public void setPayStartDate(String payStartDate) {
        PayStartDate = payStartDate;
    }

    public String getPayEndDate() {
        return PayEndDate;
    }

    public void setPayEndDate(String payEndDate) {
        PayEndDate = payEndDate;
    }

    public String getPaytoDate() {
        return PaytoDate;
    }

    public void setPaytoDate(String paytoDate) {
        PaytoDate = paytoDate;
    }

    public int getPayIntv() {
        return PayIntv;
    }

    public void setPayIntv(int payIntv) {
        PayIntv = payIntv;
    }

    public double getStandPrem() {
        return StandPrem;
    }

    public void setStandPrem(double standPrem) {
        StandPrem = standPrem;
    }

    public double getPrem() {
        return Prem;
    }

    public void setPrem(double prem) {
        Prem = prem;
    }

    public double getSumPrem() {
        return SumPrem;
    }

    public void setSumPrem(double sumPrem) {
        SumPrem = sumPrem;
    }

    public double getSuppRiskScore() {
        return SuppRiskScore;
    }

    public void setSuppRiskScore(double suppRiskScore) {
        SuppRiskScore = suppRiskScore;
    }

    public String getFreeFlag() {
        return FreeFlag;
    }

    public void setFreeFlag(String freeFlag) {
        FreeFlag = freeFlag;
    }

    public double getFreeRate() {
        return FreeRate;
    }

    public void setFreeRate(double freeRate) {
        FreeRate = freeRate;
    }

    public String getFreeStartDate() {
        return FreeStartDate;
    }

    public void setFreeStartDate(String freeStartDate) {
        FreeStartDate = freeStartDate;
    }

    public String getFreeEndDate() {
        return FreeEndDate;
    }

    public void setFreeEndDate(String freeEndDate) {
        FreeEndDate = freeEndDate;
    }

    public String getState() {
        return State;
    }

    public void setState(String state) {
        State = state;
    }

    public String getManageCom() {
        return ManageCom;
    }

    public void setManageCom(String manageCom) {
        ManageCom = manageCom;
    }

    public String getOperator() {
        return Operator;
    }

    public void setOperator(String operator) {
        Operator = operator;
    }

    public String getMakeDate() {
        return MakeDate;
    }

    public void setMakeDate(String makeDate) {
        MakeDate = makeDate;
    }

    public String getMakeTime() {
        return MakeTime;
    }

    public void setMakeTime(String makeTime) {
        MakeTime = makeTime;
    }

    public String getModifyDate() {
        return ModifyDate;
    }

    public void setModifyDate(String modifyDate) {
        ModifyDate = modifyDate;
    }

    public String getModifyTime() {
        return ModifyTime;
    }

    public void setModifyTime(String modifyTime) {
        ModifyTime = modifyTime;
    }

    public String getAddFeeDirect() {
        return AddFeeDirect;
    }

    public void setAddFeeDirect(String addFeeDirect) {
        AddFeeDirect = addFeeDirect;
    }

    public double getSecInsuAddPoint() {
        return SecInsuAddPoint;
    }

    public void setSecInsuAddPoint(double secInsuAddPoint) {
        SecInsuAddPoint = secInsuAddPoint;
    }
}
