package com.sinosoft.cloud.access.entity;

/**
 * abc-cloud-access
 *
 * @author lizhen
 * @create 2019/1/14
 */
public class ReqClaimStatusQuery {
    //理赔号
    private String ClaimNo;

    public String getClaimNo() {
        return ClaimNo;
    }

    public void setClaimNo(String claimNo) {
        ClaimNo = claimNo;
    }

    @Override
    public String toString() {
        return "ReqClaimStatusQuery{" +
                "ClaimNo='" + ClaimNo + '\'' +
                '}';
    }
}