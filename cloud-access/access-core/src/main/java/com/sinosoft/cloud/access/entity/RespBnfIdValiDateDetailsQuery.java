package com.sinosoft.cloud.access.entity;
/**
 * 受益人身份证有效期变更明细查询POS051.
 */
public class RespBnfIdValiDateDetailsQuery {

    //保全项目编码
    private String EdorType;
    private BnfInfos BnfInfos;

    public String getEdorType() {
        return EdorType;
    }

    public void setEdorType(String edorType) {
        EdorType = edorType;
    }

    public BnfInfos getBnfInfos() {
        return BnfInfos;
    }

    public void setBnfInfos(BnfInfos bnfInfos) {
        BnfInfos = bnfInfos;
    }

    @Override
    public String toString() {
        return "RespBnfIdValiDateDetailsQuery{" +
                "EdorType='" + EdorType + '\'' +
                ", BnfInfos=" + BnfInfos +
                '}';
    }
}
