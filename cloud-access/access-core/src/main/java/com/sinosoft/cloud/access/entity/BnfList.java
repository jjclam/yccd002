package com.sinosoft.cloud.access.entity;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.List;

@XStreamAlias("BnfList")
public class BnfList {

	@XStreamImplicit(itemFieldName = "BnfDetail")
	 private List<BnfDetail> bnfDetail;

	public List<BnfDetail> getBnfDetail() {
		return bnfDetail;
	}

	public void setBnfDetail(List<BnfDetail> bnfDetail) {
		this.bnfDetail = bnfDetail;
	}

	@Override
	public String toString() {
		return "BnfList{" +
				"bnfDetail=" + bnfDetail +
				'}';
	}
}
