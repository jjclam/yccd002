package com.sinosoft.cloud.access.entity;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("ShuiDiHead")
public class ShuiDiHead {
    /*合作商编号*/
    @XStreamAlias("supplierNo")
    private String supplierNo;


    /*时间戳*/
    @XStreamAlias("timeStamp")
    private String timeStamp;
    /*请求日期*/
    @XStreamAlias("TransDate")
    private String TransDate;
    /*请求时间*/
    @XStreamAlias("TransTime")
    private String TransTime;

    /*订单号*/
    @XStreamAlias("orderNo")
    private String orderNo;


    public String getSupplierNo() {
        return supplierNo;
    }

    public void setSupplierNo(String supplierNo) {
        this.supplierNo = supplierNo;
    }

    public String getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(String timeStamp) {
        this.timeStamp = timeStamp;
    }

    public String getTransDate() {
        return TransDate;
    }

    public void setTransDate(String transDate) {
        TransDate = transDate;
    }

    public String getTransTime() {
        return TransTime;
    }

    public void setTransTime(String transTime) {
        TransTime = transTime;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }


    @Override
    public String toString() {
        return "ShuiDiHead{" +
                "supplierNo='" + supplierNo + '\'' +
                ", timeStamp='" + timeStamp + '\'' +
                ", TransDate='" + TransDate + '\'' +
                ", TransTime='" + TransTime + '\'' +
                ", orderNo='" + orderNo + '\'' +
                '}';
    }
}