package com.sinosoft.cloud.access.entity;

/**
 * @Author:lizhen
 * @Description:
 * @Date:Created in 15:38 2018/11/15
 * @Modified By:
 */
/**
 * 身份验证环节CLM001.
 */

public class RespAuthentication {
    private IdentityVerifyInfos IdentityVerifyInfos;

    public IdentityVerifyInfos getIdentityVerifyInfos() {
        return IdentityVerifyInfos;
    }

    public void setIdentityVerifyInfos(IdentityVerifyInfos identityVerifyInfos) {
        IdentityVerifyInfos = identityVerifyInfos;
    }

    @Override
    public String toString() {
        return "RespAuthentication{" +
                "IdentityVerifyInfos=" + IdentityVerifyInfos +
                '}';
    }
}