package com.sinosoft.cloud.access.entity;

/**
 * @Description
 * @Author:zhaoshulei
 * @Date:2018/4/2
 */
public class LCPol {

    private long PolID;
    private long ContID;
    private String ShardingID;
    private String GrpContNo;
    private String GrpPolNo;
    private String ContNo;
    private String PolNo;
    private String ProposalNo;
    private String PrtNo;
    private String ContType;
    private String PolTypeFlag;
    private String MainPolNo;
    private String MasterPolNo;
    private String KindCode;
    private String RiskCode;
    private String RiskVersion;
    private String ManageCom;
    private String AgentCom;
    private String AgentType;
    private String AgentCode;
    private String AgentGroup;
    private String AgentCode1;
    private String SaleChnl;
    private String Handler;
    private String InsuredNo;
    private String InsuredName;
    private String InsuredSex;
    private String InsuredBirthday;
    private int InsuredAppAge;
    private int InsuredPeoples;
    private String OccupationType;
    private String AppntNo;
    private String AppntName;
    private String CValiDate;
    private String SignCom;
    private String SignDate;
    private String SignTime;
    private String FirstPayDate;
    private String PayEndDate;
    private String PaytoDate;
    private String GetStartDate;
    private String EndDate;
    private String AcciEndDate;
    private String GetYearFlag;
    private int GetYear;
    private String PayEndYearFlag;
    private int PayEndYear;
    private String InsuYearFlag;
    private int InsuYear;
    private String AcciYearFlag;
    private int AcciYear;
    private String GetStartType;
    private String SpecifyValiDate;
    private String PayMode;
    private String PayLocation;
    private int PayIntv;
    private int PayYears;
    private int Years;
    private double ManageFeeRate;
    private double FloatRate;
    private String PremToAmnt;
    private double Mult;
    private double StandPrem;
    private double Prem;
    private double SumPrem;
    private double Amnt;
    private double RiskAmnt;
    private double LeavingMoney;
    private int EndorseTimes;
    private int ClaimTimes;
    private int LiveTimes;
    private int RenewCount;
    private String LastGetDate;
    private String LastLoanDate;
    private String LastRegetDate;
    private String LastEdorDate;
    private String LastRevDate;
    private int RnewFlag;
    private String StopFlag;
    private String ExpiryFlag;
    private String AutoPayFlag;
    private String InterestDifFlag;
    private String SubFlag;
    private String BnfFlag;
    private String HealthCheckFlag;
    private String ImpartFlag;
    private String ReinsureFlag;
    private String AgentPayFlag;
    private String AgentGetFlag;
    private String LiveGetMode;
    private String DeadGetMode;
    private String BonusGetMode;
    private String BonusMan;
    private String DeadFlag;
    private String SmokeFlag;
    private String Remark;
    private String ApproveFlag;
    private String ApproveCode;
    private String ApproveDate;
    private String ApproveTime;
    private String UWFlag;
    private String UWCode;
    private String UWDate;
    private String UWTime;
    private String PolApplyDate;
    private String AppFlag;
    private String PolState;
    private String StandbyFlag1;
    private String StandbyFlag2;
    private String StandbyFlag3;
    private String Operator;
    private String MakeDate;
    private String MakeTime;
    private String ModifyDate;
    private String ModifyTime;
    private int WaitPeriod;
    private String GetForm;
    private String GetBankCode;
    private String GetBankAccNo;
    private String GetAccName;
    private String KeepValueOpt;
    private String AscriptionRuleCode;
    private String PayRuleCode;
    private String AscriptionFlag;
    private String AutoPubAccFlag;
    private String CombiFlag;
    private String InvestRuleCode;
    private String UintLinkValiFlag;
    private String ProdSetCode;
    private String InsurPolFlag;
    private String NewReinsureFlag;
    private String LiveAccFlag;
    private double StandRatePrem;
    private String AppntFirstName;
    private String AppntLastName;
    private String InsuredFirstName;
    private String InsuredLastName;
    private String FQGetMode;
    private String GetPeriod;
    private String GetPeriodFlag;
    private String DelayPeriod;
    private double OtherAmnt;
    private String Relation;
    private String ReCusNo;
    private String AutoRnewAge;
    private String AutoRnewYear;






    public long getPolID() {
        return PolID;
    }

    public void setPolID(long polID) {
        PolID = polID;
    }

    public long getContID() {
        return ContID;
    }

    public void setContID(long contID) {
        ContID = contID;
    }

    public String getShardingID() {
        return ShardingID;
    }

    public void setShardingID(String shardingID) {
        ShardingID = shardingID;
    }

    public String getGrpContNo() {
        return GrpContNo;
    }

    public void setGrpContNo(String grpContNo) {
        GrpContNo = grpContNo;
    }

    public String getGrpPolNo() {
        return GrpPolNo;
    }

    public void setGrpPolNo(String grpPolNo) {
        GrpPolNo = grpPolNo;
    }

    public String getContNo() {
        return ContNo;
    }

    public void setContNo(String contNo) {
        ContNo = contNo;
    }

    public String getPolNo() {
        return PolNo;
    }

    public void setPolNo(String polNo) {
        PolNo = polNo;
    }

    public String getProposalNo() {
        return ProposalNo;
    }

    public void setProposalNo(String proposalNo) {
        ProposalNo = proposalNo;
    }

    public String getPrtNo() {
        return PrtNo;
    }

    public void setPrtNo(String prtNo) {
        PrtNo = prtNo;
    }

    public String getContType() {
        return ContType;
    }

    public void setContType(String contType) {
        ContType = contType;
    }

    public String getPolTypeFlag() {
        return PolTypeFlag;
    }

    public void setPolTypeFlag(String polTypeFlag) {
        PolTypeFlag = polTypeFlag;
    }

    public String getMainPolNo() {
        return MainPolNo;
    }

    public void setMainPolNo(String mainPolNo) {
        MainPolNo = mainPolNo;
    }

    public String getMasterPolNo() {
        return MasterPolNo;
    }

    public void setMasterPolNo(String masterPolNo) {
        MasterPolNo = masterPolNo;
    }

    public String getKindCode() {
        return KindCode;
    }

    public void setKindCode(String kindCode) {
        KindCode = kindCode;
    }

    public String getRiskCode() {
        return RiskCode;
    }

    public void setRiskCode(String riskCode) {
        RiskCode = riskCode;
    }

    public String getRiskVersion() {
        return RiskVersion;
    }

    public void setRiskVersion(String riskVersion) {
        RiskVersion = riskVersion;
    }

    public String getManageCom() {
        return ManageCom;
    }

    public void setManageCom(String manageCom) {
        ManageCom = manageCom;
    }

    public String getAgentCom() {
        return AgentCom;
    }

    public void setAgentCom(String agentCom) {
        AgentCom = agentCom;
    }

    public String getAgentType() {
        return AgentType;
    }

    public void setAgentType(String agentType) {
        AgentType = agentType;
    }

    public String getAgentCode() {
        return AgentCode;
    }

    public void setAgentCode(String agentCode) {
        AgentCode = agentCode;
    }

    public String getAgentGroup() {
        return AgentGroup;
    }

    public void setAgentGroup(String agentGroup) {
        AgentGroup = agentGroup;
    }

    public String getAgentCode1() {
        return AgentCode1;
    }

    public void setAgentCode1(String agentCode1) {
        AgentCode1 = agentCode1;
    }

    public String getSaleChnl() {
        return SaleChnl;
    }

    public void setSaleChnl(String saleChnl) {
        SaleChnl = saleChnl;
    }

    public String getHandler() {
        return Handler;
    }

    public void setHandler(String handler) {
        Handler = handler;
    }

    public String getInsuredNo() {
        return InsuredNo;
    }

    public void setInsuredNo(String insuredNo) {
        InsuredNo = insuredNo;
    }

    public String getInsuredName() {
        return InsuredName;
    }

    public void setInsuredName(String insuredName) {
        InsuredName = insuredName;
    }

    public String getInsuredSex() {
        return InsuredSex;
    }

    public void setInsuredSex(String insuredSex) {
        InsuredSex = insuredSex;
    }

    public String getInsuredBirthday() {
        return InsuredBirthday;
    }

    public void setInsuredBirthday(String insuredBirthday) {
        InsuredBirthday = insuredBirthday;
    }

    public int getInsuredAppAge() {
        return InsuredAppAge;
    }

    public void setInsuredAppAge(int insuredAppAge) {
        InsuredAppAge = insuredAppAge;
    }

    public int getInsuredPeoples() {
        return InsuredPeoples;
    }

    public void setInsuredPeoples(int insuredPeoples) {
        InsuredPeoples = insuredPeoples;
    }

    public String getOccupationType() {
        return OccupationType;
    }

    public void setOccupationType(String occupationType) {
        OccupationType = occupationType;
    }

    public String getAppntNo() {
        return AppntNo;
    }

    public void setAppntNo(String appntNo) {
        AppntNo = appntNo;
    }

    public String getAppntName() {
        return AppntName;
    }

    public void setAppntName(String appntName) {
        AppntName = appntName;
    }

    public String getCValiDate() {
        return CValiDate;
    }

    public void setCValiDate(String CValiDate) {
        this.CValiDate = CValiDate;
    }

    public String getSignCom() {
        return SignCom;
    }

    public void setSignCom(String signCom) {
        SignCom = signCom;
    }

    public String getSignDate() {
        return SignDate;
    }

    public void setSignDate(String signDate) {
        SignDate = signDate;
    }

    public String getSignTime() {
        return SignTime;
    }

    public void setSignTime(String signTime) {
        SignTime = signTime;
    }

    public String getFirstPayDate() {
        return FirstPayDate;
    }

    public void setFirstPayDate(String firstPayDate) {
        FirstPayDate = firstPayDate;
    }

    public String getPayEndDate() {
        return PayEndDate;
    }

    public void setPayEndDate(String payEndDate) {
        PayEndDate = payEndDate;
    }

    public String getPaytoDate() {
        return PaytoDate;
    }

    public void setPaytoDate(String paytoDate) {
        PaytoDate = paytoDate;
    }

    public String getGetStartDate() {
        return GetStartDate;
    }

    public void setGetStartDate(String getStartDate) {
        GetStartDate = getStartDate;
    }

    public String getEndDate() {
        return EndDate;
    }

    public void setEndDate(String endDate) {
        EndDate = endDate;
    }

    public String getAcciEndDate() {
        return AcciEndDate;
    }

    public void setAcciEndDate(String acciEndDate) {
        AcciEndDate = acciEndDate;
    }

    public String getGetYearFlag() {
        return GetYearFlag;
    }

    public void setGetYearFlag(String getYearFlag) {
        GetYearFlag = getYearFlag;
    }

    public int getGetYear() {
        return GetYear;
    }

    public void setGetYear(int getYear) {
        GetYear = getYear;
    }

    public String getPayEndYearFlag() {
        return PayEndYearFlag;
    }

    public void setPayEndYearFlag(String payEndYearFlag) {
        PayEndYearFlag = payEndYearFlag;
    }

    public int getPayEndYear() {
        return PayEndYear;
    }

    public void setPayEndYear(int payEndYear) {
        PayEndYear = payEndYear;
    }

    public String getInsuYearFlag() {
        return InsuYearFlag;
    }

    public void setInsuYearFlag(String insuYearFlag) {
        InsuYearFlag = insuYearFlag;
    }

    public int getInsuYear() {
        return InsuYear;
    }

    public void setInsuYear(int insuYear) {
        InsuYear = insuYear;
    }

    public String getAcciYearFlag() {
        return AcciYearFlag;
    }

    public void setAcciYearFlag(String acciYearFlag) {
        AcciYearFlag = acciYearFlag;
    }

    public int getAcciYear() {
        return AcciYear;
    }

    public void setAcciYear(int acciYear) {
        AcciYear = acciYear;
    }

    public String getGetStartType() {
        return GetStartType;
    }

    public void setGetStartType(String getStartType) {
        GetStartType = getStartType;
    }

    public String getSpecifyValiDate() {
        return SpecifyValiDate;
    }

    public void setSpecifyValiDate(String specifyValiDate) {
        SpecifyValiDate = specifyValiDate;
    }

    public String getPayMode() {
        return PayMode;
    }

    public void setPayMode(String payMode) {
        PayMode = payMode;
    }

    public String getPayLocation() {
        return PayLocation;
    }

    public void setPayLocation(String payLocation) {
        PayLocation = payLocation;
    }

    public int getPayIntv() {
        return PayIntv;
    }

    public void setPayIntv(int payIntv) {
        PayIntv = payIntv;
    }

    public int getPayYears() {
        return PayYears;
    }

    public void setPayYears(int payYears) {
        PayYears = payYears;
    }

    public int getYears() {
        return Years;
    }

    public void setYears(int years) {
        Years = years;
    }

    public double getManageFeeRate() {
        return ManageFeeRate;
    }

    public void setManageFeeRate(double manageFeeRate) {
        ManageFeeRate = manageFeeRate;
    }

    public double getFloatRate() {
        return FloatRate;
    }

    public void setFloatRate(double floatRate) {
        FloatRate = floatRate;
    }

    public String getPremToAmnt() {
        return PremToAmnt;
    }

    public void setPremToAmnt(String premToAmnt) {
        PremToAmnt = premToAmnt;
    }

    public double getMult() {
        return Mult;
    }

    public void setMult(double mult) {
        Mult = mult;
    }

    public double getStandPrem() {
        return StandPrem;
    }

    public void setStandPrem(double standPrem) {
        StandPrem = standPrem;
    }

    public double getPrem() {
        return Prem;
    }

    public void setPrem(double prem) {
        Prem = prem;
    }

    public double getSumPrem() {
        return SumPrem;
    }

    public void setSumPrem(double sumPrem) {
        SumPrem = sumPrem;
    }

    public double getAmnt() {
        return Amnt;
    }

    public void setAmnt(double amnt) {
        Amnt = amnt;
    }

    public double getRiskAmnt() {
        return RiskAmnt;
    }

    public void setRiskAmnt(double riskAmnt) {
        RiskAmnt = riskAmnt;
    }

    public double getLeavingMoney() {
        return LeavingMoney;
    }

    public void setLeavingMoney(double leavingMoney) {
        LeavingMoney = leavingMoney;
    }

    public int getEndorseTimes() {
        return EndorseTimes;
    }

    public void setEndorseTimes(int endorseTimes) {
        EndorseTimes = endorseTimes;
    }

    public int getClaimTimes() {
        return ClaimTimes;
    }

    public void setClaimTimes(int claimTimes) {
        ClaimTimes = claimTimes;
    }

    public int getLiveTimes() {
        return LiveTimes;
    }

    public void setLiveTimes(int liveTimes) {
        LiveTimes = liveTimes;
    }

    public int getRenewCount() {
        return RenewCount;
    }

    public void setRenewCount(int renewCount) {
        RenewCount = renewCount;
    }

    public String getLastGetDate() {
        return LastGetDate;
    }

    public void setLastGetDate(String lastGetDate) {
        LastGetDate = lastGetDate;
    }

    public String getLastLoanDate() {
        return LastLoanDate;
    }

    public void setLastLoanDate(String lastLoanDate) {
        LastLoanDate = lastLoanDate;
    }

    public String getLastRegetDate() {
        return LastRegetDate;
    }

    public void setLastRegetDate(String lastRegetDate) {
        LastRegetDate = lastRegetDate;
    }

    public String getLastEdorDate() {
        return LastEdorDate;
    }

    public void setLastEdorDate(String lastEdorDate) {
        LastEdorDate = lastEdorDate;
    }

    public String getLastRevDate() {
        return LastRevDate;
    }

    public void setLastRevDate(String lastRevDate) {
        LastRevDate = lastRevDate;
    }

    public int getRnewFlag() {
        return RnewFlag;
    }

    public void setRnewFlag(int rnewFlag) {
        RnewFlag = rnewFlag;
    }

    public String getStopFlag() {
        return StopFlag;
    }

    public void setStopFlag(String stopFlag) {
        StopFlag = stopFlag;
    }

    public String getExpiryFlag() {
        return ExpiryFlag;
    }

    public void setExpiryFlag(String expiryFlag) {
        ExpiryFlag = expiryFlag;
    }

    public String getAutoPayFlag() {
        return AutoPayFlag;
    }

    public void setAutoPayFlag(String autoPayFlag) {
        AutoPayFlag = autoPayFlag;
    }

    public String getInterestDifFlag() {
        return InterestDifFlag;
    }

    public void setInterestDifFlag(String interestDifFlag) {
        InterestDifFlag = interestDifFlag;
    }

    public String getSubFlag() {
        return SubFlag;
    }

    public void setSubFlag(String subFlag) {
        SubFlag = subFlag;
    }

    public String getBnfFlag() {
        return BnfFlag;
    }

    public void setBnfFlag(String bnfFlag) {
        BnfFlag = bnfFlag;
    }

    public String getHealthCheckFlag() {
        return HealthCheckFlag;
    }

    public void setHealthCheckFlag(String healthCheckFlag) {
        HealthCheckFlag = healthCheckFlag;
    }

    public String getImpartFlag() {
        return ImpartFlag;
    }

    public void setImpartFlag(String impartFlag) {
        ImpartFlag = impartFlag;
    }

    public String getReinsureFlag() {
        return ReinsureFlag;
    }

    public void setReinsureFlag(String reinsureFlag) {
        ReinsureFlag = reinsureFlag;
    }

    public String getAgentPayFlag() {
        return AgentPayFlag;
    }

    public void setAgentPayFlag(String agentPayFlag) {
        AgentPayFlag = agentPayFlag;
    }

    public String getAgentGetFlag() {
        return AgentGetFlag;
    }

    public void setAgentGetFlag(String agentGetFlag) {
        AgentGetFlag = agentGetFlag;
    }

    public String getLiveGetMode() {
        return LiveGetMode;
    }

    public void setLiveGetMode(String liveGetMode) {
        LiveGetMode = liveGetMode;
    }

    public String getDeadGetMode() {
        return DeadGetMode;
    }

    public void setDeadGetMode(String deadGetMode) {
        DeadGetMode = deadGetMode;
    }

    public String getBonusGetMode() {
        return BonusGetMode;
    }

    public void setBonusGetMode(String bonusGetMode) {
        BonusGetMode = bonusGetMode;
    }

    public String getBonusMan() {
        return BonusMan;
    }

    public void setBonusMan(String bonusMan) {
        BonusMan = bonusMan;
    }

    public String getDeadFlag() {
        return DeadFlag;
    }

    public void setDeadFlag(String deadFlag) {
        DeadFlag = deadFlag;
    }

    public String getSmokeFlag() {
        return SmokeFlag;
    }

    public void setSmokeFlag(String smokeFlag) {
        SmokeFlag = smokeFlag;
    }

    public String getRemark() {
        return Remark;
    }

    public void setRemark(String remark) {
        Remark = remark;
    }

    public String getApproveFlag() {
        return ApproveFlag;
    }

    public void setApproveFlag(String approveFlag) {
        ApproveFlag = approveFlag;
    }

    public String getApproveCode() {
        return ApproveCode;
    }

    public void setApproveCode(String approveCode) {
        ApproveCode = approveCode;
    }

    public String getApproveDate() {
        return ApproveDate;
    }

    public void setApproveDate(String approveDate) {
        ApproveDate = approveDate;
    }

    public String getApproveTime() {
        return ApproveTime;
    }

    public void setApproveTime(String approveTime) {
        ApproveTime = approveTime;
    }

    public String getUWFlag() {
        return UWFlag;
    }

    public void setUWFlag(String UWFlag) {
        this.UWFlag = UWFlag;
    }

    public String getUWCode() {
        return UWCode;
    }

    public void setUWCode(String UWCode) {
        this.UWCode = UWCode;
    }

    public String getUWDate() {
        return UWDate;
    }

    public void setUWDate(String UWDate) {
        this.UWDate = UWDate;
    }

    public String getUWTime() {
        return UWTime;
    }

    public void setUWTime(String UWTime) {
        this.UWTime = UWTime;
    }

    public String getPolApplyDate() {
        return PolApplyDate;
    }

    public void setPolApplyDate(String polApplyDate) {
        PolApplyDate = polApplyDate;
    }

    public String getAppFlag() {
        return AppFlag;
    }

    public void setAppFlag(String appFlag) {
        AppFlag = appFlag;
    }

    public String getPolState() {
        return PolState;
    }

    public void setPolState(String polState) {
        PolState = polState;
    }

    public String getStandbyFlag1() {
        return StandbyFlag1;
    }

    public void setStandbyFlag1(String standbyFlag1) {
        StandbyFlag1 = standbyFlag1;
    }

    public String getStandbyFlag2() {
        return StandbyFlag2;
    }

    public void setStandbyFlag2(String standbyFlag2) {
        StandbyFlag2 = standbyFlag2;
    }

    public String getStandbyFlag3() {
        return StandbyFlag3;
    }

    public void setStandbyFlag3(String standbyFlag3) {
        StandbyFlag3 = standbyFlag3;
    }

    public String getOperator() {
        return Operator;
    }

    public void setOperator(String operator) {
        Operator = operator;
    }

    public String getMakeDate() {
        return MakeDate;
    }

    public void setMakeDate(String makeDate) {
        MakeDate = makeDate;
    }

    public String getMakeTime() {
        return MakeTime;
    }

    public void setMakeTime(String makeTime) {
        MakeTime = makeTime;
    }

    public String getModifyDate() {
        return ModifyDate;
    }

    public void setModifyDate(String modifyDate) {
        ModifyDate = modifyDate;
    }

    public String getModifyTime() {
        return ModifyTime;
    }

    public void setModifyTime(String modifyTime) {
        ModifyTime = modifyTime;
    }

    public int getWaitPeriod() {
        return WaitPeriod;
    }

    public void setWaitPeriod(int waitPeriod) {
        WaitPeriod = waitPeriod;
    }

    public String getGetForm() {
        return GetForm;
    }

    public void setGetForm(String getForm) {
        GetForm = getForm;
    }

    public String getGetBankCode() {
        return GetBankCode;
    }

    public void setGetBankCode(String getBankCode) {
        GetBankCode = getBankCode;
    }

    public String getGetBankAccNo() {
        return GetBankAccNo;
    }

    public void setGetBankAccNo(String getBankAccNo) {
        GetBankAccNo = getBankAccNo;
    }

    public String getGetAccName() {
        return GetAccName;
    }

    public void setGetAccName(String getAccName) {
        GetAccName = getAccName;
    }

    public String getKeepValueOpt() {
        return KeepValueOpt;
    }

    public void setKeepValueOpt(String keepValueOpt) {
        KeepValueOpt = keepValueOpt;
    }

    public String getAscriptionRuleCode() {
        return AscriptionRuleCode;
    }

    public void setAscriptionRuleCode(String ascriptionRuleCode) {
        AscriptionRuleCode = ascriptionRuleCode;
    }

    public String getPayRuleCode() {
        return PayRuleCode;
    }

    public void setPayRuleCode(String payRuleCode) {
        PayRuleCode = payRuleCode;
    }

    public String getAscriptionFlag() {
        return AscriptionFlag;
    }

    public void setAscriptionFlag(String ascriptionFlag) {
        AscriptionFlag = ascriptionFlag;
    }

    public String getAutoPubAccFlag() {
        return AutoPubAccFlag;
    }

    public void setAutoPubAccFlag(String autoPubAccFlag) {
        AutoPubAccFlag = autoPubAccFlag;
    }

    public String getCombiFlag() {
        return CombiFlag;
    }

    public void setCombiFlag(String combiFlag) {
        CombiFlag = combiFlag;
    }

    public String getInvestRuleCode() {
        return InvestRuleCode;
    }

    public void setInvestRuleCode(String investRuleCode) {
        InvestRuleCode = investRuleCode;
    }

    public String getUintLinkValiFlag() {
        return UintLinkValiFlag;
    }

    public void setUintLinkValiFlag(String uintLinkValiFlag) {
        UintLinkValiFlag = uintLinkValiFlag;
    }

    public String getProdSetCode() {
        return ProdSetCode;
    }

    public void setProdSetCode(String prodSetCode) {
        ProdSetCode = prodSetCode;
    }

    public String getInsurPolFlag() {
        return InsurPolFlag;
    }

    public void setInsurPolFlag(String insurPolFlag) {
        InsurPolFlag = insurPolFlag;
    }

    public String getNewReinsureFlag() {
        return NewReinsureFlag;
    }

    public void setNewReinsureFlag(String newReinsureFlag) {
        NewReinsureFlag = newReinsureFlag;
    }

    public String getLiveAccFlag() {
        return LiveAccFlag;
    }

    public void setLiveAccFlag(String liveAccFlag) {
        LiveAccFlag = liveAccFlag;
    }

    public double getStandRatePrem() {
        return StandRatePrem;
    }

    public void setStandRatePrem(double standRatePrem) {
        StandRatePrem = standRatePrem;
    }

    public String getAppntFirstName() {
        return AppntFirstName;
    }

    public void setAppntFirstName(String appntFirstName) {
        AppntFirstName = appntFirstName;
    }

    public String getAppntLastName() {
        return AppntLastName;
    }

    public void setAppntLastName(String appntLastName) {
        AppntLastName = appntLastName;
    }

    public String getInsuredFirstName() {
        return InsuredFirstName;
    }

    public void setInsuredFirstName(String insuredFirstName) {
        InsuredFirstName = insuredFirstName;
    }

    public String getInsuredLastName() {
        return InsuredLastName;
    }

    public void setInsuredLastName(String insuredLastName) {
        InsuredLastName = insuredLastName;
    }

    public String getFQGetMode() {
        return FQGetMode;
    }

    public void setFQGetMode(String FQGetMode) {
        this.FQGetMode = FQGetMode;
    }

    public String getGetPeriod() {
        return GetPeriod;
    }

    public void setGetPeriod(String getPeriod) {
        GetPeriod = getPeriod;
    }

    public String getGetPeriodFlag() {
        return GetPeriodFlag;
    }

    public void setGetPeriodFlag(String getPeriodFlag) {
        GetPeriodFlag = getPeriodFlag;
    }

    public String getDelayPeriod() {
        return DelayPeriod;
    }

    public void setDelayPeriod(String delayPeriod) {
        DelayPeriod = delayPeriod;
    }

    public double getOtherAmnt() {
        return OtherAmnt;
    }

    public void setOtherAmnt(double otherAmnt) {
        OtherAmnt = otherAmnt;
    }

    public String getRelation() {
        return Relation;
    }

    public void setRelation(String relation) {
        Relation = relation;
    }

    public String getReCusNo() {
        return ReCusNo;
    }

    public void setReCusNo(String reCusNo) {
        ReCusNo = reCusNo;
    }

    public String getAutoRnewAge() {
        return AutoRnewAge;
    }

    public void setAutoRnewAge(String autoRnewAge) {
        AutoRnewAge = autoRnewAge;
    }

    public String getAutoRnewYear() {
        return AutoRnewYear;
    }

    public void setAutoRnewYear(String autoRnewYear) {
        AutoRnewYear = autoRnewYear;
    }

    @Override
    public String toString() {
        return "LCPol{" +
                "PolID=" + PolID +
                ", ContID=" + ContID +
                ", ShardingID='" + ShardingID + '\'' +
                ", GrpContNo='" + GrpContNo + '\'' +
                ", GrpPolNo='" + GrpPolNo + '\'' +
                ", ContNo='" + ContNo + '\'' +
                ", PolNo='" + PolNo + '\'' +
                ", ProposalNo='" + ProposalNo + '\'' +
                ", PrtNo='" + PrtNo + '\'' +
                ", ContType='" + ContType + '\'' +
                ", PolTypeFlag='" + PolTypeFlag + '\'' +
                ", MainPolNo='" + MainPolNo + '\'' +
                ", MasterPolNo='" + MasterPolNo + '\'' +
                ", KindCode='" + KindCode + '\'' +
                ", RiskCode='" + RiskCode + '\'' +
                ", RiskVersion='" + RiskVersion + '\'' +
                ", ManageCom='" + ManageCom + '\'' +
                ", AgentCom='" + AgentCom + '\'' +
                ", AgentType='" + AgentType + '\'' +
                ", AgentCode='" + AgentCode + '\'' +
                ", AgentGroup='" + AgentGroup + '\'' +
                ", AgentCode1='" + AgentCode1 + '\'' +
                ", SaleChnl='" + SaleChnl + '\'' +
                ", Handler='" + Handler + '\'' +
                ", InsuredNo='" + InsuredNo + '\'' +
                ", InsuredName='" + InsuredName + '\'' +
                ", InsuredSex='" + InsuredSex + '\'' +
                ", InsuredBirthday='" + InsuredBirthday + '\'' +
                ", InsuredAppAge=" + InsuredAppAge +
                ", InsuredPeoples=" + InsuredPeoples +
                ", OccupationType='" + OccupationType + '\'' +
                ", AppntNo='" + AppntNo + '\'' +
                ", AppntName='" + AppntName + '\'' +
                ", CValiDate='" + CValiDate + '\'' +
                ", SignCom='" + SignCom + '\'' +
                ", SignDate='" + SignDate + '\'' +
                ", SignTime='" + SignTime + '\'' +
                ", FirstPayDate='" + FirstPayDate + '\'' +
                ", PayEndDate='" + PayEndDate + '\'' +
                ", PaytoDate='" + PaytoDate + '\'' +
                ", GetStartDate='" + GetStartDate + '\'' +
                ", EndDate='" + EndDate + '\'' +
                ", AcciEndDate='" + AcciEndDate + '\'' +
                ", GetYearFlag='" + GetYearFlag + '\'' +
                ", GetYear=" + GetYear +
                ", PayEndYearFlag='" + PayEndYearFlag + '\'' +
                ", PayEndYear=" + PayEndYear +
                ", InsuYearFlag='" + InsuYearFlag + '\'' +
                ", InsuYear=" + InsuYear +
                ", AcciYearFlag='" + AcciYearFlag + '\'' +
                ", AcciYear=" + AcciYear +
                ", GetStartType='" + GetStartType + '\'' +
                ", SpecifyValiDate='" + SpecifyValiDate + '\'' +
                ", PayMode='" + PayMode + '\'' +
                ", PayLocation='" + PayLocation + '\'' +
                ", PayIntv=" + PayIntv +
                ", PayYears=" + PayYears +
                ", Years=" + Years +
                ", ManageFeeRate=" + ManageFeeRate +
                ", FloatRate=" + FloatRate +
                ", PremToAmnt='" + PremToAmnt + '\'' +
                ", Mult=" + Mult +
                ", StandPrem=" + StandPrem +
                ", Prem=" + Prem +
                ", SumPrem=" + SumPrem +
                ", Amnt=" + Amnt +
                ", RiskAmnt=" + RiskAmnt +
                ", LeavingMoney=" + LeavingMoney +
                ", EndorseTimes=" + EndorseTimes +
                ", ClaimTimes=" + ClaimTimes +
                ", LiveTimes=" + LiveTimes +
                ", RenewCount=" + RenewCount +
                ", LastGetDate='" + LastGetDate + '\'' +
                ", LastLoanDate='" + LastLoanDate + '\'' +
                ", LastRegetDate='" + LastRegetDate + '\'' +
                ", LastEdorDate='" + LastEdorDate + '\'' +
                ", LastRevDate='" + LastRevDate + '\'' +
                ", RnewFlag=" + RnewFlag +
                ", StopFlag='" + StopFlag + '\'' +
                ", ExpiryFlag='" + ExpiryFlag + '\'' +
                ", AutoPayFlag='" + AutoPayFlag + '\'' +
                ", InterestDifFlag='" + InterestDifFlag + '\'' +
                ", SubFlag='" + SubFlag + '\'' +
                ", BnfFlag='" + BnfFlag + '\'' +
                ", HealthCheckFlag='" + HealthCheckFlag + '\'' +
                ", ImpartFlag='" + ImpartFlag + '\'' +
                ", ReinsureFlag='" + ReinsureFlag + '\'' +
                ", AgentPayFlag='" + AgentPayFlag + '\'' +
                ", AgentGetFlag='" + AgentGetFlag + '\'' +
                ", LiveGetMode='" + LiveGetMode + '\'' +
                ", DeadGetMode='" + DeadGetMode + '\'' +
                ", BonusGetMode='" + BonusGetMode + '\'' +
                ", BonusMan='" + BonusMan + '\'' +
                ", DeadFlag='" + DeadFlag + '\'' +
                ", SmokeFlag='" + SmokeFlag + '\'' +
                ", Remark='" + Remark + '\'' +
                ", ApproveFlag='" + ApproveFlag + '\'' +
                ", ApproveCode='" + ApproveCode + '\'' +
                ", ApproveDate='" + ApproveDate + '\'' +
                ", ApproveTime='" + ApproveTime + '\'' +
                ", UWFlag='" + UWFlag + '\'' +
                ", UWCode='" + UWCode + '\'' +
                ", UWDate='" + UWDate + '\'' +
                ", UWTime='" + UWTime + '\'' +
                ", PolApplyDate='" + PolApplyDate + '\'' +
                ", AppFlag='" + AppFlag + '\'' +
                ", PolState='" + PolState + '\'' +
                ", StandbyFlag1='" + StandbyFlag1 + '\'' +
                ", StandbyFlag2='" + StandbyFlag2 + '\'' +
                ", StandbyFlag3='" + StandbyFlag3 + '\'' +
                ", Operator='" + Operator + '\'' +
                ", MakeDate='" + MakeDate + '\'' +
                ", MakeTime='" + MakeTime + '\'' +
                ", ModifyDate='" + ModifyDate + '\'' +
                ", ModifyTime='" + ModifyTime + '\'' +
                ", WaitPeriod=" + WaitPeriod +
                ", GetForm='" + GetForm + '\'' +
                ", GetBankCode='" + GetBankCode + '\'' +
                ", GetBankAccNo='" + GetBankAccNo + '\'' +
                ", GetAccName='" + GetAccName + '\'' +
                ", KeepValueOpt='" + KeepValueOpt + '\'' +
                ", AscriptionRuleCode='" + AscriptionRuleCode + '\'' +
                ", PayRuleCode='" + PayRuleCode + '\'' +
                ", AscriptionFlag='" + AscriptionFlag + '\'' +
                ", AutoPubAccFlag='" + AutoPubAccFlag + '\'' +
                ", CombiFlag='" + CombiFlag + '\'' +
                ", InvestRuleCode='" + InvestRuleCode + '\'' +
                ", UintLinkValiFlag='" + UintLinkValiFlag + '\'' +
                ", ProdSetCode='" + ProdSetCode + '\'' +
                ", InsurPolFlag='" + InsurPolFlag + '\'' +
                ", NewReinsureFlag='" + NewReinsureFlag + '\'' +
                ", LiveAccFlag='" + LiveAccFlag + '\'' +
                ", StandRatePrem=" + StandRatePrem +
                ", AppntFirstName='" + AppntFirstName + '\'' +
                ", AppntLastName='" + AppntLastName + '\'' +
                ", InsuredFirstName='" + InsuredFirstName + '\'' +
                ", InsuredLastName='" + InsuredLastName + '\'' +
                ", FQGetMode='" + FQGetMode + '\'' +
                ", GetPeriod='" + GetPeriod + '\'' +
                ", GetPeriodFlag='" + GetPeriodFlag + '\'' +
                ", DelayPeriod='" + DelayPeriod + '\'' +
                ", OtherAmnt=" + OtherAmnt +
                ", Relation='" + Relation + '\'' +
                ", ReCusNo='" + ReCusNo + '\'' +
                ", AutoRnewAge='" + AutoRnewAge + '\'' +
                ", AutoRnewYear='" + AutoRnewYear + '\'' +
                '}';
    }
}
