package com.sinosoft.cloud.access.entity;

/**
 * 4004保单生存金领取(AGSQ)申请、4004保单生存金领取(SQ)申请、满期申请POS044、POS045、POS072.
 */
public class RespExpirationApply {
    //保全项目编码
    private String EdorType;
    //保单号
    private String ContNo;
    //保全受理号
    private String EdorAcceptNo;
    //保全批单号
    private String EdorNo;
    //险种名称
    private String RiskName;
    //退保金额
    private String OccurBala;
    //保全生效日
    private String BqValidDate;

    public String getEdorType() {
        return EdorType;
    }

    public void setEdorType(String edorType) {
        EdorType = edorType;
    }

    public String getContNo() {
        return ContNo;
    }

    public void setContNo(String contNo) {
        ContNo = contNo;
    }

    public String getEdorAcceptNo() {
        return EdorAcceptNo;
    }

    public void setEdorAcceptNo(String edorAcceptNo) {
        EdorAcceptNo = edorAcceptNo;
    }

    public String getEdorNo() {
        return EdorNo;
    }

    public void setEdorNo(String edorNo) {
        EdorNo = edorNo;
    }

    public String getRiskName() {
        return RiskName;
    }

    public void setRiskName(String riskName) {
        RiskName = riskName;
    }

    public String getOccurBala() {
        return OccurBala;
    }

    public void setOccurBala(String occurBala) {
        OccurBala = occurBala;
    }

    public String getBqValidDate() {
        return BqValidDate;
    }

    public void setBqValidDate(String bqValidDate) {
        BqValidDate = bqValidDate;
    }

    @Override
    public String toString() {
        return "RespExpirationApply{" +
                "EdorType='" + EdorType + '\'' +
                ", ContNo='" + ContNo + '\'' +
                ", EdorAcceptNo='" + EdorAcceptNo + '\'' +
                ", EdorNo='" + EdorNo + '\'' +
                ", RiskName='" + RiskName + '\'' +
                ", OccurBala='" + OccurBala + '\'' +
                ", BqValidDate='" + BqValidDate + '\'' +
                '}';
    }
}
