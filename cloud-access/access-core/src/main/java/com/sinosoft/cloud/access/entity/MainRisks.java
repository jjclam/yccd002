package com.sinosoft.cloud.access.entity;

import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.List;

/**
 * abc-cloud-access
 *
 * @author lizhen
 * @create 2019/3/29
 */
public class MainRisks {
    @XStreamImplicit(itemFieldName="MainRisk")
    private List<MainRisk> MainRisk;

    public List<MainRisk> getMainRisk() {
        return MainRisk;
    }

    public void setMainRisk(List<MainRisk> mainRisk) {
        MainRisk = mainRisk;
    }

    @Override
    public String toString() {
        return "MainRisks{" +
                "MainRisk=" + MainRisk +
                '}';
    }
}