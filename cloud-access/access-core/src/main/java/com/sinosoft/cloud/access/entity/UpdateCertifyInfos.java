package com.sinosoft.cloud.access.entity;



import java.util.List;

/**
 * @Author:lizhen
 * @Description:
 * @Date:Created in 14:30 2018/11/13
 * @Modified By:
 */
public class UpdateCertifyInfos {

    private List<UpdateInfo> UpdateInfo;

    public List<com.sinosoft.cloud.access.entity.UpdateInfo> getUpdateInfo() {
        return UpdateInfo;
    }

    public void setUpdateInfo(List<com.sinosoft.cloud.access.entity.UpdateInfo> updateInfo) {
        UpdateInfo = updateInfo;
    }

    @Override
    public String toString() {
        return "UpdateCertifyInfos{" +
                "UpdateInfo=" + UpdateInfo +
                '}';
    }
}