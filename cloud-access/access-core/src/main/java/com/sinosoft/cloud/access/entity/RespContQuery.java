package com.sinosoft.cloud.access.entity;

/**
 * 保单查询PC002
 */
public class RespContQuery {
    //保单号
    private String ContNo;
    //险种编码
    private String RiskCode;
    //保费
    private String Prem;
    //保额
    private String Amnt;
    //险种名称
    private String RiskName;
    //保单状态
    private String PolicyStatus;
    //保单质押状态
    private String PolicyPledge;
    //受理日期
    private String AcceptDate;
    //保单生效日
    private String PolicyBgnDate;
    //保单到期日
    private String PolicyEndDate;
    //投保份数
    private String PolicyAmmount;
    //保单价值
    private String PolicyValue;
    //自动转账授权账号
    private String AutoTransferAccNo;

    public String getContNo() {
        return ContNo;
    }

    public void setContNo(String contNo) {
        ContNo = contNo;
    }

    public String getRiskCode() {
        return RiskCode;
    }

    public void setRiskCode(String riskCode) {
        RiskCode = riskCode;
    }

    public String getPrem() {
        return Prem;
    }

    public void setPrem(String prem) {
        Prem = prem;
    }

    public String getAmnt() {
        return Amnt;
    }

    public void setAmnt(String amnt) {
        Amnt = amnt;
    }

    public String getRiskName() {
        return RiskName;
    }

    public void setRiskName(String riskName) {
        RiskName = riskName;
    }

    public String getPolicyStatus() {
        return PolicyStatus;
    }

    public void setPolicyStatus(String policyStatus) {
        PolicyStatus = policyStatus;
    }

    public String getPolicyPledge() {
        return PolicyPledge;
    }

    public void setPolicyPledge(String policyPledge) {
        PolicyPledge = policyPledge;
    }

    public String getAcceptDate() {
        return AcceptDate;
    }

    public void setAcceptDate(String acceptDate) {
        AcceptDate = acceptDate;
    }

    public String getPolicyBgnDate() {
        return PolicyBgnDate;
    }

    public void setPolicyBgnDate(String policyBgnDate) {
        PolicyBgnDate = policyBgnDate;
    }

    public String getPolicyEndDate() {
        return PolicyEndDate;
    }

    public void setPolicyEndDate(String policyEndDate) {
        PolicyEndDate = policyEndDate;
    }

    public String getPolicyAmmount() {
        return PolicyAmmount;
    }

    public void setPolicyAmmount(String policyAmmount) {
        PolicyAmmount = policyAmmount;
    }

    public String getPolicyValue() {
        return PolicyValue;
    }

    public void setPolicyValue(String policyValue) {
        PolicyValue = policyValue;
    }

    public String getAutoTransferAccNo() {
        return AutoTransferAccNo;
    }

    public void setAutoTransferAccNo(String autoTransferAccNo) {
        AutoTransferAccNo = autoTransferAccNo;
    }

    @Override
    public String toString() {
        return "RespContQuery{" +
                "ContNo='" + ContNo + '\'' +
                ", RiskCode='" + RiskCode + '\'' +
                ", Prem='" + Prem + '\'' +
                ", Amnt='" + Amnt + '\'' +
                ", RiskName='" + RiskName + '\'' +
                ", PolicyStatus='" + PolicyStatus + '\'' +
                ", PolicyPledge='" + PolicyPledge + '\'' +
                ", AcceptDate='" + AcceptDate + '\'' +
                ", PolicyBgnDate='" + PolicyBgnDate + '\'' +
                ", PolicyEndDate='" + PolicyEndDate + '\'' +
                ", PolicyAmmount='" + PolicyAmmount + '\'' +
                ", PolicyValue='" + PolicyValue + '\'' +
                ", AutoTransferAccNo='" + AutoTransferAccNo + '\'' +
                '}';
    }
}
