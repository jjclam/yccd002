package com.sinosoft.cloud.access.entity;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.List;

/**
 * Created by zhaoshulei on 2017/9/28.
 */
@XStreamAlias("CashValues")
public class CashValues {

    @XStreamImplicit(itemFieldName="CashValue")
    private List<CashValue> cashValues;

    public List<CashValue> getCashValues() {
        return cashValues;
    }

    public void setCashValues(List<CashValue> cashValues) {
        this.cashValues = cashValues;
    }

    @Override
    public String toString() {
        return "CashValues{" +
                "cashValues=" + cashValues +
                '}';
    }
}
