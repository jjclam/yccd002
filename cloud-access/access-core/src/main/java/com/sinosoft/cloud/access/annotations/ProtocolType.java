package com.sinosoft.cloud.access.annotations;

public enum ProtocolType {
    SOAP,SOCKET,RESTFUL
}
