package com.sinosoft.cloud.access.entity;

/**
 * abc-cloud-access
 *
 * @author lizhen
 * @create 2019/1/14
 */
public class RespClaimStatusQuery {
    private WecLPSyncImagesRes WecLPSyncImagesRes;

    public WecLPSyncImagesRes getWecLPSyncImagesRes() {
        return WecLPSyncImagesRes;
    }

    public void setWecLPSyncImagesRes(WecLPSyncImagesRes wecLPSyncImagesRes) {
        WecLPSyncImagesRes = wecLPSyncImagesRes;
    }

    @Override
    public String toString() {
        return "RespClaimStatusQuery{" +
                "WecLPSyncImagesRes=" + WecLPSyncImagesRes +
                '}';
    }
}