package com.sinosoft.cloud.access.entity;

/**
 * 投连万能补缴保全项目保单明细查询POS082
 */
public class RespUniversalPaymentQuery {
    //保全项目编码
    private String EdorType;
    //保单号
    private String ContNo;
    //万能账户余额
    private String AccontBalance;
    //欠缴续期保费期数
    private String Unreprenumber;
    //期缴保费
    private String Repremium;
    //最少缴费期数
    private String Leastprenumber;
    //缴费期数
    private String Reprenumber;
    //补缴保费
    private String PayPrem;
    //补缴保费利息
    private String PayPremInterest;
    //收付费方式
    private String PayMode;
    //xxx账户
    private String BankAccNo;
    //户名
    private String BankAccName;
    //开户行
    private String BankCode;
    //补退费领取人
    private String RefundPeople;
    //补退费合计
    private String RefundMoney;
    //身份证号
    private String IDNo;
    //是否为美国纳税义务的个人
    private String TinFlag;
    //美国纳税人识别号
    private String TinNo;

    @Override
    public String toString() {
        return "RespUniversalPaymentQuery{" +
                "EdorType='" + EdorType + '\'' +
                ", ContNo='" + ContNo + '\'' +
                ", AccontBalance='" + AccontBalance + '\'' +
                ", Unreprenumber='" + Unreprenumber + '\'' +
                ", Repremium='" + Repremium + '\'' +
                ", Leastprenumber='" + Leastprenumber + '\'' +
                ", Reprenumber='" + Reprenumber + '\'' +
                ", PayPrem='" + PayPrem + '\'' +
                ", PayPremInterest='" + PayPremInterest + '\'' +
                ", PayMode='" + PayMode + '\'' +
                ", BankAccNo='" + BankAccNo + '\'' +
                ", BankAccName='" + BankAccName + '\'' +
                ", BankCode='" + BankCode + '\'' +
                ", RefundPeople='" + RefundPeople + '\'' +
                ", RefundMoney='" + RefundMoney + '\'' +
                ", IDNo='" + IDNo + '\'' +
                ", TinFlag='" + TinFlag + '\'' +
                ", TinNo='" + TinNo + '\'' +
                '}';
    }

    public String getEdorType() {
        return EdorType;
    }

    public void setEdorType(String edorType) {
        EdorType = edorType;
    }

    public String getContNo() {
        return ContNo;
    }

    public void setContNo(String contNo) {
        ContNo = contNo;
    }

    public String getAccontBalance() {
        return AccontBalance;
    }

    public void setAccontBalance(String accontBalance) {
        AccontBalance = accontBalance;
    }

    public String getUnreprenumber() {
        return Unreprenumber;
    }

    public void setUnreprenumber(String unreprenumber) {
        Unreprenumber = unreprenumber;
    }

    public String getRepremium() {
        return Repremium;
    }

    public void setRepremium(String repremium) {
        Repremium = repremium;
    }

    public String getLeastprenumber() {
        return Leastprenumber;
    }

    public void setLeastprenumber(String leastprenumber) {
        Leastprenumber = leastprenumber;
    }

    public String getReprenumber() {
        return Reprenumber;
    }

    public void setReprenumber(String reprenumber) {
        Reprenumber = reprenumber;
    }

    public String getPayPrem() {
        return PayPrem;
    }

    public void setPayPrem(String payPrem) {
        PayPrem = payPrem;
    }

    public String getPayPremInterest() {
        return PayPremInterest;
    }

    public void setPayPremInterest(String payPremInterest) {
        PayPremInterest = payPremInterest;
    }

    public String getPayMode() {
        return PayMode;
    }

    public void setPayMode(String payMode) {
        PayMode = payMode;
    }

    public String getBankAccNo() {
        return BankAccNo;
    }

    public void setBankAccNo(String bankAccNo) {
        BankAccNo = bankAccNo;
    }

    public String getBankAccName() {
        return BankAccName;
    }

    public void setBankAccName(String bankAccName) {
        BankAccName = bankAccName;
    }

    public String getBankCode() {
        return BankCode;
    }

    public void setBankCode(String bankCode) {
        BankCode = bankCode;
    }

    public String getRefundPeople() {
        return RefundPeople;
    }

    public void setRefundPeople(String refundPeople) {
        RefundPeople = refundPeople;
    }

    public String getRefundMoney() {
        return RefundMoney;
    }

    public void setRefundMoney(String refundMoney) {
        RefundMoney = refundMoney;
    }

    public String getIDNo() {
        return IDNo;
    }

    public void setIDNo(String IDNo) {
        this.IDNo = IDNo;
    }

    public String getTinFlag() {
        return TinFlag;
    }

    public void setTinFlag(String tinFlag) {
        TinFlag = tinFlag;
    }

    public String getTinNo() {
        return TinNo;
    }

    public void setTinNo(String tinNo) {
        TinNo = tinNo;
    }

    public String getV(String s) {
        return null;
    }

    public String getV(int i) {
        return null;
    }

    public int getFieldType(String s) {
        return 0;
    }

    public int getFieldType(int i) {
        return 0;
    }

    public int getFieldCount() {
        return 0;
    }

    public int getFieldIndex(String s) {
        return 0;
    }

    public String getFieldName(int i) {
        return null;
    }

    public boolean setV(String s, String s1) {
        return false;
    }
}
