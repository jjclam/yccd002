package com.sinosoft.cloud.access.entity;

/**
 * Created by Administrator on 2018/4/16.
 */
public class LDPerson {
    private long PersonID;
    private String ShardingID;
    private String CustomerNo;
    private String Name;
    private String Sex;
    private String Birthday;
    private String IDType;
    private String IDNo;
    private String Password;
    private String NativePlace;
    private String Nationality;
    private String RgtAddress;
    private String Marriage;
    private String MarriageDate;
    private String Health;
    private double Stature;
    private double Avoirdupois;
    private String Degree;
    private String CreditGrade;
    private String OthIDType;
    private String OthIDNo;
    private String ICNo;
    private String GrpNo;
    private String JoinCompanyDate;
    private String StartWorkDate;
    private String Position;
    private double Salary;
    private String OccupationType;
    private String OccupationCode;
    private String WorkType;
    private String PluralityType;
    private String DeathDate;
    private String SmokeFlag;
    private String BlacklistFlag;
    private String Proterty;
    private String Remark;
    private String State;
    private String VIPValue;
    private String Operator;
    private String MakeDate;
    private String MakeTime;
    private String ModifyDate;
    private String ModifyTime;
    private String GrpName;
    private String License;
    private String LicenseType;
    private String SocialInsuNo;
    private String IdValiDate;
    private String HaveMotorcycleLicence;
    private String PartTimeJob;
    private String HealthFlag;
    private String ServiceMark;
    private String FirstName;
    private String LastName;
    private String CUSLevel;
    private String SSFlag;
    private String RgtTpye;
    private String TINNO;
    private String TINFlag;
    private String NewCustomerFlag;

    public long getPersonID() {
        return PersonID;
    }

    public void setPersonID(long personID) {
        PersonID = personID;
    }

    public String getShardingID() {
        return ShardingID;
    }

    public void setShardingID(String shardingID) {
        ShardingID = shardingID;
    }

    public String getCustomerNo() {
        return CustomerNo;
    }

    public void setCustomerNo(String customerNo) {
        CustomerNo = customerNo;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getSex() {
        return Sex;
    }

    public void setSex(String sex) {
        Sex = sex;
    }

    public String getBirthday() {
        return Birthday;
    }

    public void setBirthday(String birthday) {
        Birthday = birthday;
    }

    public String getIDType() {
        return IDType;
    }

    public void setIDType(String IDType) {
        this.IDType = IDType;
    }

    public String getIDNo() {
        return IDNo;
    }

    public void setIDNo(String IDNo) {
        this.IDNo = IDNo;
    }

    public String getPassword() {
        return Password;
    }

    public void setPassword(String password) {
        Password = password;
    }

    public String getNativePlace() {
        return NativePlace;
    }

    public void setNativePlace(String nativePlace) {
        NativePlace = nativePlace;
    }

    public String getNationality() {
        return Nationality;
    }

    public void setNationality(String nationality) {
        Nationality = nationality;
    }

    public String getRgtAddress() {
        return RgtAddress;
    }

    public void setRgtAddress(String rgtAddress) {
        RgtAddress = rgtAddress;
    }

    public String getMarriage() {
        return Marriage;
    }

    public void setMarriage(String marriage) {
        Marriage = marriage;
    }

    public String getMarriageDate() {
        return MarriageDate;
    }

    public void setMarriageDate(String marriageDate) {
        MarriageDate = marriageDate;
    }

    public String getHealth() {
        return Health;
    }

    public void setHealth(String health) {
        Health = health;
    }

    public double getStature() {
        return Stature;
    }

    public void setStature(double stature) {
        Stature = stature;
    }

    public double getAvoirdupois() {
        return Avoirdupois;
    }

    public void setAvoirdupois(double avoirdupois) {
        Avoirdupois = avoirdupois;
    }

    public String getDegree() {
        return Degree;
    }

    public void setDegree(String degree) {
        Degree = degree;
    }

    public String getCreditGrade() {
        return CreditGrade;
    }

    public void setCreditGrade(String creditGrade) {
        CreditGrade = creditGrade;
    }

    public String getOthIDType() {
        return OthIDType;
    }

    public void setOthIDType(String othIDType) {
        OthIDType = othIDType;
    }

    public String getOthIDNo() {
        return OthIDNo;
    }

    public void setOthIDNo(String othIDNo) {
        OthIDNo = othIDNo;
    }

    public String getICNo() {
        return ICNo;
    }

    public void setICNo(String ICNo) {
        this.ICNo = ICNo;
    }

    public String getGrpNo() {
        return GrpNo;
    }

    public void setGrpNo(String grpNo) {
        GrpNo = grpNo;
    }

    public String getJoinCompanyDate() {
        return JoinCompanyDate;
    }

    public void setJoinCompanyDate(String joinCompanyDate) {
        JoinCompanyDate = joinCompanyDate;
    }

    public String getStartWorkDate() {
        return StartWorkDate;
    }

    public void setStartWorkDate(String startWorkDate) {
        StartWorkDate = startWorkDate;
    }

    public String getPosition() {
        return Position;
    }

    public void setPosition(String position) {
        Position = position;
    }

    public double getSalary() {
        return Salary;
    }

    public void setSalary(double salary) {
        Salary = salary;
    }

    public String getOccupationType() {
        return OccupationType;
    }

    public void setOccupationType(String occupationType) {
        OccupationType = occupationType;
    }

    public String getOccupationCode() {
        return OccupationCode;
    }

    public void setOccupationCode(String occupationCode) {
        OccupationCode = occupationCode;
    }

    public String getWorkType() {
        return WorkType;
    }

    public void setWorkType(String workType) {
        WorkType = workType;
    }

    public String getPluralityType() {
        return PluralityType;
    }

    public void setPluralityType(String pluralityType) {
        PluralityType = pluralityType;
    }

    public String getDeathDate() {
        return DeathDate;
    }

    public void setDeathDate(String deathDate) {
        DeathDate = deathDate;
    }

    public String getSmokeFlag() {
        return SmokeFlag;
    }

    public void setSmokeFlag(String smokeFlag) {
        SmokeFlag = smokeFlag;
    }

    public String getBlacklistFlag() {
        return BlacklistFlag;
    }

    public void setBlacklistFlag(String blacklistFlag) {
        BlacklistFlag = blacklistFlag;
    }

    public String getProterty() {
        return Proterty;
    }

    public void setProterty(String proterty) {
        Proterty = proterty;
    }

    public String getRemark() {
        return Remark;
    }

    public void setRemark(String remark) {
        Remark = remark;
    }

    public String getState() {
        return State;
    }

    public void setState(String state) {
        State = state;
    }

    public String getVIPValue() {
        return VIPValue;
    }

    public void setVIPValue(String VIPValue) {
        this.VIPValue = VIPValue;
    }

    public String getOperator() {
        return Operator;
    }

    public void setOperator(String operator) {
        Operator = operator;
    }

    public String getMakeDate() {
        return MakeDate;
    }

    public void setMakeDate(String makeDate) {
        MakeDate = makeDate;
    }

    public String getMakeTime() {
        return MakeTime;
    }

    public void setMakeTime(String makeTime) {
        MakeTime = makeTime;
    }

    public String getModifyDate() {
        return ModifyDate;
    }

    public void setModifyDate(String modifyDate) {
        ModifyDate = modifyDate;
    }

    public String getModifyTime() {
        return ModifyTime;
    }

    public void setModifyTime(String modifyTime) {
        ModifyTime = modifyTime;
    }

    public String getGrpName() {
        return GrpName;
    }

    public void setGrpName(String grpName) {
        GrpName = grpName;
    }

    public String getLicense() {
        return License;
    }

    public void setLicense(String license) {
        License = license;
    }

    public String getLicenseType() {
        return LicenseType;
    }

    public void setLicenseType(String licenseType) {
        LicenseType = licenseType;
    }

    public String getSocialInsuNo() {
        return SocialInsuNo;
    }

    public void setSocialInsuNo(String socialInsuNo) {
        SocialInsuNo = socialInsuNo;
    }

    public String getIdValiDate() {
        return IdValiDate;
    }

    public void setIdValiDate(String idValiDate) {
        IdValiDate = idValiDate;
    }

    public String getHaveMotorcycleLicence() {
        return HaveMotorcycleLicence;
    }

    public void setHaveMotorcycleLicence(String haveMotorcycleLicence) {
        HaveMotorcycleLicence = haveMotorcycleLicence;
    }

    public String getPartTimeJob() {
        return PartTimeJob;
    }

    public void setPartTimeJob(String partTimeJob) {
        PartTimeJob = partTimeJob;
    }

    public String getHealthFlag() {
        return HealthFlag;
    }

    public void setHealthFlag(String healthFlag) {
        HealthFlag = healthFlag;
    }

    public String getServiceMark() {
        return ServiceMark;
    }

    public void setServiceMark(String serviceMark) {
        ServiceMark = serviceMark;
    }

    public String getFirstName() {
        return FirstName;
    }

    public void setFirstName(String firstName) {
        FirstName = firstName;
    }

    public String getLastName() {
        return LastName;
    }

    public void setLastName(String lastName) {
        LastName = lastName;
    }

    public String getCUSLevel() {
        return CUSLevel;
    }

    public void setCUSLevel(String CUSLevel) {
        this.CUSLevel = CUSLevel;
    }

    public String getSSFlag() {
        return SSFlag;
    }

    public void setSSFlag(String SSFlag) {
        this.SSFlag = SSFlag;
    }

    public String getRgtTpye() {
        return RgtTpye;
    }

    public void setRgtTpye(String rgtTpye) {
        RgtTpye = rgtTpye;
    }

    public String getTINNO() {
        return TINNO;
    }

    public void setTINNO(String TINNO) {
        this.TINNO = TINNO;
    }

    public String getTINFlag() {
        return TINFlag;
    }

    public void setTINFlag(String TINFlag) {
        this.TINFlag = TINFlag;
    }

    public String getNewCustomerFlag() {
        return NewCustomerFlag;
    }

    public void setNewCustomerFlag(String newCustomerFlag) {
        NewCustomerFlag = newCustomerFlag;
    }
}
