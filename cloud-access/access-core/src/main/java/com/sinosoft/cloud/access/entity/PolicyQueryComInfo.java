package com.sinosoft.cloud.access.entity;

/**
 * @Author:lizhen
 * @Description:
 * @Date:Created in 15:57 2018/11/15
 * @Modified By:
 */
public class PolicyQueryComInfo {
    //保单号
    private String ContNo;
    //管理机构
    private String ManageCom;

    public String getContNo() {
        return ContNo;
    }

    public void setContNo(String contNo) {
        ContNo = contNo;
    }

    public String getManageCom() {
        return ManageCom;
    }

    public void setManageCom(String manageCom) {
        ManageCom = manageCom;
    }

    @Override
    public String toString() {
        return "PolicyQueryComInfo{" +
                "ContNo='" + ContNo + '\'' +
                ", ManageCom='" + ManageCom + '\'' +
                '}';
    }
}