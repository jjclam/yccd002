package com.sinosoft.cloud.access.entity;

import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.List;

/**
 * abc-cloud-access
 *
 * @author lizhen
 * @create 2019/1/10
 */
public class PathInfos {

    @XStreamImplicit(itemFieldName="PathInfo")
    private List<PathInfo> PathInfo;

    public List<com.sinosoft.cloud.access.entity.PathInfo> getPathInfo() {
        return PathInfo;
    }

    public void setPathInfo(List<com.sinosoft.cloud.access.entity.PathInfo> pathInfo) {
        PathInfo = pathInfo;
    }

    @Override
    public String toString() {
        return "PathInfos{" +
                "PathInfo=" + PathInfo +
                '}';
    }
}