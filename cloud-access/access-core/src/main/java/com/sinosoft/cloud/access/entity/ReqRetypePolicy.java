package com.sinosoft.cloud.access.entity;

/**
 * @Author:lizhen
 * @Description:
 * @Date:Created in 17:49 2018/11/17
 * @Modified By:
 */
/**
 * 保单重打NB006 请求
 */
public class ReqRetypePolicy {
    //老印刷号码
    private String OldVchNo;
    //新印刷号码
    private String NewVchNo;
    //总单投保单号码
    private String ProposalContNo;
    //xxx编码
    private String BankCode;
    //地区代码
    private String ZoneNo;
    //网点编码
    private String BranchNo;
    //柜员代码
    private String TellerNo;

    private String RiskCode;

    public String getOldVchNo() {
        return OldVchNo;
    }

    public void setOldVchNo(String oldVchNo) {
        OldVchNo = oldVchNo;
    }

    public String getNewVchNo() {
        return NewVchNo;
    }

    public void setNewVchNo(String newVchNo) {
        NewVchNo = newVchNo;
    }

    public String getProposalContNo() {
        return ProposalContNo;
    }

    public void setProposalContNo(String proposalContNo) {
        ProposalContNo = proposalContNo;
    }

    public String getBankCode() {
        return BankCode;
    }

    public void setBankCode(String bankCode) {
        BankCode = bankCode;
    }

    public String getZoneNo() {
        return ZoneNo;
    }

    public void setZoneNo(String zoneNo) {
        ZoneNo = zoneNo;
    }

    public String getBranchNo() {
        return BranchNo;
    }

    public void setBranchNo(String branchNo) {
        BranchNo = branchNo;
    }

    public String getTellerNo() {
        return TellerNo;
    }

    public void setTellerNo(String tellerNo) {
        TellerNo = tellerNo;
    }

    public String getRiskCode() {
        return RiskCode;
    }

    public void setRiskCode(String riskCode) {
        RiskCode = riskCode;
    }

    @Override
    public String toString() {
        return "ReqRetypePolicy{" +
                "OldVchNo='" + OldVchNo + '\'' +
                ", NewVchNo='" + NewVchNo + '\'' +
                ", ProposalContNo='" + ProposalContNo + '\'' +
                ", BankCode='" + BankCode + '\'' +
                ", ZoneNo='" + ZoneNo + '\'' +
                ", BranchNo='" + BranchNo + '\'' +
                ", TellerNo='" + TellerNo + '\'' +
                ", RiskCode='" + RiskCode + '\'' +
                '}';
    }
}