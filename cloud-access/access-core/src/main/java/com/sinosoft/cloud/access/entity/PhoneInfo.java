package com.sinosoft.cloud.access.entity;

/**
 * @Author:lizhen
 * @Description:
 * @Date:Created in 17:02 2018/11/16
 * @Modified By:
 */
public class PhoneInfo {
    //手机号
    private String MobilePhone;

    public String getMobilePhone() {
        return MobilePhone;
    }

    public void setMobilePhone(String mobilePhone) {
        MobilePhone = mobilePhone;
    }

    @Override
    public String toString() {
        return "PhoneInfo{" +
                "MobilePhone='" + MobilePhone + '\'' +
                '}';
    }
}