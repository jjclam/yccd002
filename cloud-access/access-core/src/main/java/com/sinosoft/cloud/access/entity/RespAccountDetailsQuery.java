package com.sinosoft.cloud.access.entity;
/**
 * 万能账户价值详情查询POS033.
 */
public class RespAccountDetailsQuery {
    //保全项目编码
    private String EdorType;
    //保单号
    private String ContNo;
    //累积账户价值
    private String AccountBalance;
    private BalanceInfos BalanceInfos;

    public void setEdorType(String edorType) {
        EdorType = edorType;
    }

    public void setContNo(String contNo) {
        ContNo = contNo;
    }

    public void setAccountBalance(String accountBalance) {
        AccountBalance = accountBalance;
    }

    public void setBalanceInfos(BalanceInfos balanceInfos) {
        BalanceInfos = balanceInfos;
    }

    public String getEdorType() {
        return EdorType;
    }

    public String getContNo() {
        return ContNo;
    }

    public String getAccountBalance() {
        return AccountBalance;
    }

    public BalanceInfos getBalanceInfos() {
        return BalanceInfos;
    }

    @Override
    public String toString() {
        return "RespAccountDetailsQuery{" +
                "EdorType='" + EdorType + '\'' +
                ", ContNo='" + ContNo + '\'' +
                ", AccountBalance='" + AccountBalance + '\'' +
                ", BalanceInfos=" + BalanceInfos +
                '}';
    }
}
