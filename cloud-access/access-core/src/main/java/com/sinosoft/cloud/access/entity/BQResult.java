package com.sinosoft.cloud.access.entity;

/**
 * @program: abc-cloud-access
 * @description: 转换类
 * @author: BaoYongmeng
 * @create: 2018-04-13 09:53
 **/
public class BQResult {
    private String PolicyNo;
    private String RiskName;
    private Double TransferAmt;
    private Double Prem;
    private Double PolicyAmt;
    private Double PayTBFee;
    private String ValidDate;
    private String ExpireDate;
    private String BusinessType;
    private String RiskCode;
    private String PolicyStatus;
    private String BQStatus;
    private String ApplyDate;
    private String BusinType;

    public void setPolicyNo(String policyNo) {
        PolicyNo = policyNo;
    }

    public void setRiskName(String riskName) {
        RiskName = riskName;
    }

    public void setTransferAmt(Double transferAmt) {
        TransferAmt = transferAmt;
    }

    public void setPrem(Double prem) {
        Prem = prem;
    }

    public void setPolicyAmt(Double policyAmt) {
        PolicyAmt = policyAmt;
    }

    public void setPayTBFee(Double payTBFee) {
        PayTBFee = payTBFee;
    }

    public void setValidDate(String validDate) {
        ValidDate = validDate;
    }

    public void setExpireDate(String expireDate) {
        ExpireDate = expireDate;
    }

    public void setBusinessType(String businessType) {
        BusinessType = businessType;
    }

    public void setRiskCode(String riskCode) {
        RiskCode = riskCode;
    }

    public void setPolicyStatus(String policyStatus) {
        PolicyStatus = policyStatus;
    }

    public void setBQStatus(String BQStatus) {
        this.BQStatus = BQStatus;
    }

    public void setApplyDate(String applyDate) {
        ApplyDate = applyDate;
    }

    public void setBusinType(String businType) {
        BusinType = businType;
    }

    public String getPolicyNo() {
        return PolicyNo;
    }

    public String getRiskName() {
        return RiskName;
    }

    public Double getTransferAmt() {
        return TransferAmt;
    }

    public Double getPrem() {
        return Prem;
    }

    public Double getPolicyAmt() {
        return PolicyAmt;
    }

    public Double getPayTBFee() {
        return PayTBFee;
    }

    public String getValidDate() {
        return ValidDate;
    }

    public String getExpireDate() {
        return ExpireDate;
    }

    public String getBusinessType() {
        return BusinessType;
    }

    public String getRiskCode() {
        return RiskCode;
    }

    public String getPolicyStatus() {
        return PolicyStatus;
    }

    public String getBQStatus() {
        return BQStatus;
    }

    public String getApplyDate() {
        return ApplyDate;
    }

    public String getBusinType() {
        return BusinType;
    }

    @Override
    public String toString() {
        return "BQResult{" +
                "PolicyNo='" + PolicyNo + '\'' +
                ", RiskName='" + RiskName + '\'' +
                ", TransferAmt=" + TransferAmt +
                ", Prem=" + Prem +
                ", PolicyAmt=" + PolicyAmt +
                ", PayTBFee=" + PayTBFee +
                ", ValidDate='" + ValidDate + '\'' +
                ", ExpireDate='" + ExpireDate + '\'' +
                ", BusinessType='" + BusinessType + '\'' +
                ", RiskCode='" + RiskCode + '\'' +
                ", PolicyStatus='" + PolicyStatus + '\'' +
                ", BQStatus='" + BQStatus + '\'' +
                ", ApplyDate='" + ApplyDate + '\'' +
                ", BusinType='" + BusinType + '\'' +
                '}';
    }
}
