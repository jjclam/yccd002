package com.sinosoft.cloud.access.interceptor;

import com.sinosoft.cloud.access.util.HttpUtil;
import com.sinosoft.cloud.access.util.IPAddressUtil;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 拦截请求，记录请求IP，请求报文
 *
 * Create by Geny on 2019.08
 */
public class LogInterceptor implements HandlerInterceptor {
    @Override
    public boolean preHandle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o) throws Exception {
        System.out.println("请求IP\n" + IPAddressUtil.getClientIpAddress(httpServletRequest));
//        String data = HttpUtil.readStringFromRequest(httpServletRequest);
//        httpServletRequest.setAttribute("data", data);
//        System.out.println("请求报文\n" + data);
        return true;
    }

    @Override
    public void postHandle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, ModelAndView modelAndView) throws Exception {

    }

    @Override
    public void afterCompletion(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, Exception e) throws Exception {

    }
}
