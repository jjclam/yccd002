package com.sinosoft.cloud.access.entity;

/**
 * @Author:lizhen
 * @Description:
 * @Date:Created in 17:48 2018/11/9
 * @Modified By:
 */
public class ReqHead {

    private String Version;
    private String SerialNo;
    private String TransID;
    private String TransDate;
    private String TransTime;
    private String SysCode;
    private String Channel;

    public String getVersion() {
        return Version;
    }

    public void setVersion(String version) {
        Version = version;
    }

    public String getSerialNo() {
        return SerialNo;
    }

    public void setSerialNo(String serialNo) {
        SerialNo = serialNo;
    }

    public String getTransID() {
        return TransID;
    }

    public void setTransID(String transID) {
        TransID = transID;
    }

    public String getTransDate() {
        return TransDate;
    }

    public void setTransDate(String transDate) {
        TransDate = transDate;
    }

    public String getTransTime() {
        return TransTime;
    }

    public void setTransTime(String transTime) {
        TransTime = transTime;
    }

    public String getSysCode() {
        return SysCode;
    }

    public void setSysCode(String sysCode) {
        SysCode = sysCode;
    }

    public String getChannel() {
        return Channel;
    }

    public void setChannel(String channel) {
        Channel = channel;
    }

    @Override
    public String toString() {
        return "ReqHead{" +
                "Version='" + Version + '\'' +
                ", SerialNo='" + SerialNo + '\'' +
                ", TransID='" + TransID + '\'' +
                ", TransDate='" + TransDate + '\'' +
                ", TransTime='" + TransTime + '\'' +
                ", SysCode='" + SysCode + '\'' +
                ", Channel='" + Channel + '\'' +
                '}';
    }
}