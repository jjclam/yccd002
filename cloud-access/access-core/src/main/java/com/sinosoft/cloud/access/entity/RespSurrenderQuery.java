package com.sinosoft.cloud.access.entity;

/**
 * 退保查询POS088
 */
public class RespSurrenderQuery {
    //险种名称
    private String RiskName;
    //保单号
    private String ContNo;
    //退保金额
    private String OccurBala;
    //保单生效日
    private String ValidDate;
    //保单到期日
    private String ExpireDate;

    @Override
    public String toString() {
        return "RespSurrenderQuery{" +
                "RiskName='" + RiskName + '\'' +
                ", ContNo='" + ContNo + '\'' +
                ", OccurBala='" + OccurBala + '\'' +
                ", ValidDate='" + ValidDate + '\'' +
                ", ExpireDate='" + ExpireDate + '\'' +
                '}';
    }

    public String getRiskName() {
        return RiskName;
    }

    public void setRiskName(String riskName) {
        RiskName = riskName;
    }

    public String getContNo() {
        return ContNo;
    }

    public void setContNo(String contNo) {
        ContNo = contNo;
    }

    public String getOccurBala() {
        return OccurBala;
    }

    public void setOccurBala(String occurBala) {
        OccurBala = occurBala;
    }

    public String getValidDate() {
        return ValidDate;
    }

    public void setValidDate(String validDate) {
        ValidDate = validDate;
    }

    public String getExpireDate() {
        return ExpireDate;
    }

    public void setExpireDate(String expireDate) {
        ExpireDate = expireDate;
    }

    public String getV(String s) {
        return null;
    }

    public String getV(int i) {
        return null;
    }

    public int getFieldType(String s) {
        return 0;
    }

    public int getFieldType(int i) {
        return 0;
    }

    public int getFieldCount() {
        return 0;
    }

    public int getFieldIndex(String s) {
        return 0;
    }

    public String getFieldName(int i) {
        return null;
    }

    public boolean setV(String s, String s1) {
        return false;
    }
}
