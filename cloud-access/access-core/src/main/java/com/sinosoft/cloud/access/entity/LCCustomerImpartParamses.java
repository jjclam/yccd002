package com.sinosoft.cloud.access.entity;

import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.List;

/**
 * Created by Administrator on 2018/4/16.
 */
public class LCCustomerImpartParamses {
    @XStreamImplicit(itemFieldName="LCCustomerImpartParams")
    private List<LCCustomerImpartParams> LCCustomerImpartParamses;

    public List<LCCustomerImpartParams> getLCCustomerImpartParamses() {
        return LCCustomerImpartParamses;
    }

    public void setLCCustomerImpartParamses(List<LCCustomerImpartParams> LCCustomerImpartParamses) {
        this.LCCustomerImpartParamses = LCCustomerImpartParamses;
    }

    @Override
    public String toString() {
        return "LCCustomerImpartParamses{" +
                "LCCustomerImpartParamses=" + LCCustomerImpartParamses +
                '}';
    }
}
