package com.sinosoft.cloud.access.entity;

import com.sinosoft.cloud.access.annotations.BeanCopy;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.List;

/**
 * @Author:lizhen
 * @Description:
 * @Date:Created in 17:47 2018/11/9
 * @Modified By:
 */
public class ProgressQueryInfos {

//    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity",hasChild = "1",element = "ProgressQueryInfo")
    @XStreamImplicit(itemFieldName="ProgressQueryInfo")
    private List<ProgressQueryInfo> ProgressQueryInfo;

    public List<ProgressQueryInfo> getProgressQueryInfo() {
        return ProgressQueryInfo;
    }

    public void setProgressQueryInfo(List<ProgressQueryInfo> progressQueryInfo) {
        ProgressQueryInfo = progressQueryInfo;
    }

    @Override
    public String toString() {
        return "ProgressQueryInfos{" +
                "ProgressQueryInfo=" + ProgressQueryInfo +
                '}';
    }
}