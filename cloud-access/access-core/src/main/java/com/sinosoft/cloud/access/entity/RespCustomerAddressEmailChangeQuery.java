package com.sinosoft.cloud.access.entity;

/**
 * @Author:lizhen
 * @Description:
 * @Date:Created in 15:55 2018/11/15
 * @Modified By:
 */

/**
 * 客户联系地址或邮箱保全项目保单明细查询POS079
 */
public class RespCustomerAddressEmailChangeQuery {
    //保全项目编码
    private String EdorType;
    //明细信息总节点
    private PolicyQueryInfos PolicyQueryInfos;
    //保单来源机构信息查询
    private PolicyQueryComInfos PolicyQueryComInfos;
    private PhoneInfos PhoneInfos;

    public com.sinosoft.cloud.access.entity.PhoneInfos getPhoneInfos() {
        return PhoneInfos;
    }

    public void setPhoneInfos(com.sinosoft.cloud.access.entity.PhoneInfos phoneInfos) {
        PhoneInfos = phoneInfos;
    }

    public String getEdorType() {
        return EdorType;
    }

    public void setEdorType(String edorType) {
        EdorType = edorType;
    }

    public com.sinosoft.cloud.access.entity.PolicyQueryInfos getPolicyQueryInfos() {
        return PolicyQueryInfos;
    }

    public void setPolicyQueryInfos(com.sinosoft.cloud.access.entity.PolicyQueryInfos policyQueryInfos) {
        PolicyQueryInfos = policyQueryInfos;
    }

    public PolicyQueryComInfos getPolicyQueryComInfos() {
        return PolicyQueryComInfos;
    }

    public void setPolicyQueryComInfos(PolicyQueryComInfos policyQueryComInfos) {
        PolicyQueryComInfos = policyQueryComInfos;
    }

    @Override
    public String toString() {
        return "RespCustomerAddressEmailChangeQuery{" +
                "EdorType='" + EdorType + '\'' +
                ", PolicyQueryInfos=" + PolicyQueryInfos +
                ", PolicyQueryComInfos=" + PolicyQueryComInfos +
                ", PhoneInfos=" + PhoneInfos +
                '}';
    }
}