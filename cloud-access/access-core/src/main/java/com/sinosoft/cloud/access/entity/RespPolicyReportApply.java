package com.sinosoft.cloud.access.entity;

/**
 * @Author:lizhen
 * @Description:
 * @Date:Created in 23:21 2018/11/13
 * @Modified By:
 */
/**
 * 保单挂失解挂申请POS014.  返回
 */
public class RespPolicyReportApply {
    //保全项目编码
    private String EdorType;
    //保全受理号
    private String EdorAcceptNo;
    //保全批单号
    private String EdorNo;

    public String getEdorType() {
        return EdorType;
    }

    public void setEdorType(String edorType) {
        EdorType = edorType;
    }

    public String getEdorAcceptNo() {
        return EdorAcceptNo;
    }

    public void setEdorAcceptNo(String edorAcceptNo) {
        EdorAcceptNo = edorAcceptNo;
    }

    public String getEdorNo() {
        return EdorNo;
    }

    public void setEdorNo(String edorNo) {
        EdorNo = edorNo;
    }

    @Override
    public String toString() {
        return "RespPolicyReportApply{" +
                "EdorType='" + EdorType + '\'' +
                ", EdorAcceptNo='" + EdorAcceptNo + '\'' +
                ", EdorNo='" + EdorNo + '\'' +
                '}';
    }
}