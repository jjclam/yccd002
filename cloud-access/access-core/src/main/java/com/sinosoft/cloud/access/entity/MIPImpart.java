package com.sinosoft.cloud.access.entity;

/**
 * @author zhujiaxing
 * @date 2018/9/13 10:58
 * @description
 **/
public class MIPImpart {
    private String impartObject;
    private String impartVer;
    private String impartCode;
    private String impartContent;
    private String patchNo;

    public String getImpartCode() {
        return this.impartCode;
    }

    public void setImpartCode(String impartCode) {
        this.impartCode = impartCode;
    }

    public String getImpartContent() {
        return this.impartContent;
    }

    public void setImpartContent(String impartContent) {
        this.impartContent = impartContent;
    }

    public String getImpartObject() {
        return this.impartObject;
    }

    public void setImpartObject(String impartObject) {
        this.impartObject = impartObject;
    }

    public String getImpartVer() {
        return this.impartVer;
    }

    public void setImpartVer(String impartVer) {
        this.impartVer = impartVer;
    }

    public String getPatchNo() {
        return this.patchNo;
    }

    public void setPatchNo(String patchNo) {
        this.patchNo = patchNo;
    }

    public String toString() {
        return "MIPImpartPojo{impartCode='" + this.impartCode + '\'' + ", impartObject='" + this.impartObject + '\'' + ", impartVer='" + this.impartVer + '\'' + ", impartContent='" + this.impartContent + '\'' + ", patchNo='" + this.patchNo + '\'' + '}';
    }
}
