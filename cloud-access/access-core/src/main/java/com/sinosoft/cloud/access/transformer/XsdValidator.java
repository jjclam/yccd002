package com.sinosoft.cloud.access.transformer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.xml.sax.SAXException;

import javax.xml.XMLConstants;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.transform.Source;
import javax.xml.transform.stax.StAXSource;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import java.io.*;

import static com.sinosoft.cloud.access.configuration.AccessConfiguration.BASE_PACKAGE;


/**
 * xsd校验类.
 *
 * @Author: cuiguangdong
 * @Date: 2018/4/9 13:44
 * @Description: 使用xml schema进行xsd校验原始报文
 */
public class XsdValidator {

    protected static final String XSD_VALIDATION_FILE = "/validation.xsd";
    private final Logger cLogger = LoggerFactory.getLogger(this.getClass());

    /**
     * 使用xsd校验原始Xml
     *
     * @param orgXml     原始报文
     * @param accessName 渠道名称
     * @throws SAXException
     */
    public String validate(String orgXml, String accessName){

        XsdErrorHandler errorHandler = null;

        try {
            Reader xmlReader = new BufferedReader(new StringReader(orgXml));
            Resource resource = new ClassPathResource(BASE_PACKAGE + accessName + XSD_VALIDATION_FILE);
//            File file = resource.getFile();
            InputStream inputStream = resource.getInputStream();
            InputStreamReader inputStreamReader = new InputStreamReader(inputStream);

            BufferedReader br = new BufferedReader(inputStreamReader);
            StringBuffer str = new StringBuffer(1024);
            String data = new String();
            while ((data = br.readLine()) != null) {
                str.append(data + "");
            }
            Reader xsdReader = new BufferedReader(new StringReader(str.toString()));

//            Reader xsdReader = new BufferedReader(new FileReader(file));
            Source xmlSource = new StreamSource(xmlReader);
            Source xsdSource = new StreamSource(xsdReader);
            SchemaFactory factory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
            Schema schema = factory.newSchema(xsdSource);
            XMLStreamReader reader = XMLInputFactory.newFactory().createXMLStreamReader(xmlSource);
            Validator validator = schema.newValidator();
            errorHandler = new XsdErrorHandler(reader, accessName);
            validator.setErrorHandler(errorHandler);
            validator.validate(new StAXSource(reader));
        } catch (XMLStreamException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (SAXException e){
            e.printStackTrace();
        }
        return errorHandler.getErrorElement();
    }
}
