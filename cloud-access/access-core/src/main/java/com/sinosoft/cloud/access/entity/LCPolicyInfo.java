package com.sinosoft.cloud.access.entity;

/**
 * @program: abc-cloud-access
 * @description:
 * @author: BaoYongmeng
 * @create: 2018-06-13 17:15
 **/
public class LCPolicyInfo {
    private String ContNo;
    private String InsuredNo;
    private String ContplanCode;
    private String ContplanName;
    private double BasePrem;
    private double AddPrem;
    private String ActiveDate;
    private int InsuYear;
    private String InsuYearFlag;
    private int Cvaliintv;
    private String CvaliintvFlag;
    private String SisinSuredNo;
    private String TraveLcountry;
    private String AgentIp;
    private String FlightNo;
    private String RescuecardNo;
    private String HandlerName;
    private String HandlerPhone;
    private String SignDate;
    private String SignTime;
    private String agentcom;
    private String RiskType;
    private String OrderType;
    private String JYContNo;
    private String JYCertNo;
    private String JYStartDate;
    private String JYEndDate;
    private String LoanAmount;
    private String LendCom;
    private String LoanerNature;
    private String LoanerNatureName;
    private String LendTerm;
    private String CountryCode;
    private String GasCompany;
    private String GasUserAddress;
    private String PayoutPro;
    private String School;
    private String ChargeDate;
    private String Mark;
    public static final int FIELDNUM = 39;

    public LCPolicyInfo() {
    }

    public void setContNo(String contNo) {
        ContNo = contNo;
    }

    public void setInsuredNo(String insuredNo) {
        InsuredNo = insuredNo;
    }

    public void setContplanCode(String contplanCode) {
        ContplanCode = contplanCode;
    }

    public void setContplanName(String contplanName) {
        ContplanName = contplanName;
    }

    public void setBasePrem(double basePrem) {
        BasePrem = basePrem;
    }

    public void setAddPrem(double addPrem) {
        AddPrem = addPrem;
    }

    public void setActiveDate(String activeDate) {
        ActiveDate = activeDate;
    }

    public void setInsuYear(int insuYear) {
        InsuYear = insuYear;
    }

    public void setInsuYearFlag(String insuYearFlag) {
        InsuYearFlag = insuYearFlag;
    }

    public void setCvaliintv(int cvaliintv) {
        Cvaliintv = cvaliintv;
    }

    public void setCvaliintvFlag(String cvaliintvFlag) {
        CvaliintvFlag = cvaliintvFlag;
    }

    public void setSisinSuredNo(String sisinSuredNo) {
        SisinSuredNo = sisinSuredNo;
    }

    public void setTraveLcountry(String traveLcountry) {
        TraveLcountry = traveLcountry;
    }

    public void setAgentIp(String agentIp) {
        AgentIp = agentIp;
    }

    public void setFlightNo(String flightNo) {
        FlightNo = flightNo;
    }

    public void setRescuecardNo(String rescuecardNo) {
        RescuecardNo = rescuecardNo;
    }

    public void setHandlerName(String handlerName) {
        HandlerName = handlerName;
    }

    public void setHandlerPhone(String handlerPhone) {
        HandlerPhone = handlerPhone;
    }

    public void setSignDate(String signDate) {
        SignDate = signDate;
    }

    public void setSignTime(String signTime) {
        SignTime = signTime;
    }

    public void setAgentcom(String agentcom) {
        this.agentcom = agentcom;
    }

    public void setRiskType(String riskType) {
        RiskType = riskType;
    }

    public void setOrderType(String orderType) {
        OrderType = orderType;
    }

    public void setJYContNo(String JYContNo) {
        this.JYContNo = JYContNo;
    }

    public void setJYCertNo(String JYCertNo) {
        this.JYCertNo = JYCertNo;
    }

    public void setJYStartDate(String JYStartDate) {
        this.JYStartDate = JYStartDate;
    }

    public void setJYEndDate(String JYEndDate) {
        this.JYEndDate = JYEndDate;
    }

    public void setLoanAmount(String loanAmount) {
        LoanAmount = loanAmount;
    }

    public void setLendCom(String lendCom) {
        LendCom = lendCom;
    }

    public void setLoanerNature(String loanerNature) {
        LoanerNature = loanerNature;
    }

    public void setLoanerNatureName(String loanerNatureName) {
        LoanerNatureName = loanerNatureName;
    }

    public void setLendTerm(String lendTerm) {
        LendTerm = lendTerm;
    }

    public void setCountryCode(String countryCode) {
        CountryCode = countryCode;
    }

    public void setGasCompany(String gasCompany) {
        GasCompany = gasCompany;
    }

    public void setGasUserAddress(String gasUserAddress) {
        GasUserAddress = gasUserAddress;
    }

    public void setPayoutPro(String payoutPro) {
        PayoutPro = payoutPro;
    }

    public void setSchool(String school) {
        School = school;
    }

    public void setChargeDate(String chargeDate) {
        ChargeDate = chargeDate;
    }

    public void setMark(String mark) {
        Mark = mark;
    }


    public String getContNo() {
        return ContNo;
    }

    public String getInsuredNo() {
        return InsuredNo;
    }

    public String getContplanCode() {
        return ContplanCode;
    }

    public String getContplanName() {
        return ContplanName;
    }

    public double getBasePrem() {
        return BasePrem;
    }

    public double getAddPrem() {
        return AddPrem;
    }

    public String getActiveDate() {
        return ActiveDate;
    }

    public int getInsuYear() {
        return InsuYear;
    }

    public String getInsuYearFlag() {
        return InsuYearFlag;
    }

    public int getCvaliintv() {
        return Cvaliintv;
    }

    public String getCvaliintvFlag() {
        return CvaliintvFlag;
    }

    public String getSisinSuredNo() {
        return SisinSuredNo;
    }

    public String getTraveLcountry() {
        return TraveLcountry;
    }

    public String getAgentIp() {
        return AgentIp;
    }

    public String getFlightNo() {
        return FlightNo;
    }

    public String getRescuecardNo() {
        return RescuecardNo;
    }

    public String getHandlerName() {
        return HandlerName;
    }

    public String getHandlerPhone() {
        return HandlerPhone;
    }

    public String getSignDate() {
        return SignDate;
    }

    public String getSignTime() {
        return SignTime;
    }

    public String getAgentcom() {
        return agentcom;
    }

    public String getRiskType() {
        return RiskType;
    }

    public String getOrderType() {
        return OrderType;
    }

    public String getJYContNo() {
        return JYContNo;
    }

    public String getJYCertNo() {
        return JYCertNo;
    }

    public String getJYStartDate() {
        return JYStartDate;
    }

    public String getJYEndDate() {
        return JYEndDate;
    }

    public String getLoanAmount() {
        return LoanAmount;
    }

    public String getLendCom() {
        return LendCom;
    }

    public String getLoanerNature() {
        return LoanerNature;
    }

    public String getLoanerNatureName() {
        return LoanerNatureName;
    }

    public String getLendTerm() {
        return LendTerm;
    }

    public String getCountryCode() {
        return CountryCode;
    }

    public String getGasCompany() {
        return GasCompany;
    }

    public String getGasUserAddress() {
        return GasUserAddress;
    }

    public String getPayoutPro() {
        return PayoutPro;
    }

    public String getSchool() {
        return School;
    }

    public String getChargeDate() {
        return ChargeDate;
    }

    public String getMark() {
        return Mark;
    }

    public static int getFIELDNUM() {
        return FIELDNUM;
    }

    @Override
    public String toString() {
        return "LCPolicyInfo{" +
                "ContNo='" + ContNo + '\'' +
                ", InsuredNo='" + InsuredNo + '\'' +
                ", ContplanCode='" + ContplanCode + '\'' +
                ", ContplanName='" + ContplanName + '\'' +
                ", BasePrem=" + BasePrem +
                ", AddPrem=" + AddPrem +
                ", ActiveDate='" + ActiveDate + '\'' +
                ", InsuYear=" + InsuYear +
                ", InsuYearFlag='" + InsuYearFlag + '\'' +
                ", Cvaliintv=" + Cvaliintv +
                ", CvaliintvFlag='" + CvaliintvFlag + '\'' +
                ", SisinSuredNo='" + SisinSuredNo + '\'' +
                ", TraveLcountry='" + TraveLcountry + '\'' +
                ", AgentIp='" + AgentIp + '\'' +
                ", FlightNo='" + FlightNo + '\'' +
                ", RescuecardNo='" + RescuecardNo + '\'' +
                ", HandlerName='" + HandlerName + '\'' +
                ", HandlerPhone='" + HandlerPhone + '\'' +
                ", SignDate='" + SignDate + '\'' +
                ", SignTime='" + SignTime + '\'' +
                ", agentcom='" + agentcom + '\'' +
                ", RiskType='" + RiskType + '\'' +
                ", OrderType='" + OrderType + '\'' +
                ", JYContNo='" + JYContNo + '\'' +
                ", JYCertNo='" + JYCertNo + '\'' +
                ", JYStartDate='" + JYStartDate + '\'' +
                ", JYEndDate='" + JYEndDate + '\'' +
                ", LoanAmount='" + LoanAmount + '\'' +
                ", LendCom='" + LendCom + '\'' +
                ", LoanerNature='" + LoanerNature + '\'' +
                ", LoanerNatureName='" + LoanerNatureName + '\'' +
                ", LendTerm='" + LendTerm + '\'' +
                ", CountryCode='" + CountryCode + '\'' +
                ", GasCompany='" + GasCompany + '\'' +
                ", GasUserAddress='" + GasUserAddress + '\'' +
                ", PayoutPro='" + PayoutPro + '\'' +
                ", School='" + School + '\'' +
                ", ChargeDate='" + ChargeDate + '\'' +
                ", Mark='" + Mark + '\'' +
                '}';
    }
}
