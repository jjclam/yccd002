package com.sinosoft.cloud.access.entity;

/**
 * @program: abc-cloud-access
 * @description: 微信续保查询NB013
 * @author: BaoYongmeng
 * @create: 2019-03-20 18:03
 **/
public class RespRenewal {

    //可续保标志
    private String RenewFlag;
    //产品名称
    private String ProuctName;
    //投保人
    private String AppntName;
    //被保人
    private String InsuRedName;
    //投被保人关系代码
    private String RelationToAppnt;
    //被投保人关系代码
    private String RelaToInsured;
    //关系名称
    private String RelationName;
    //被保人是否有社保
    private String HealthFlag;
    //原保险合同号码
    private String ContNo;
    //原保险合同保险期间
    private String InsuYear;
    //投保人邮箱地址
    private String AppntEmail;
    //续保保费
    private String Premium;
   //续保次数
    private String RenewCount;
    //投保人职业代码
    private String AppntOccupationcode;
    //被保人职业代码
    private String InsuredOccupationcode;

    public String getRenewFlag() {
        return RenewFlag;
    }

    public void setRenewFlag(String renewFlag) {
        RenewFlag = renewFlag;
    }

    public String getProuctName() {
        return ProuctName;
    }

    public void setProuctName(String prouctName) {
        ProuctName = prouctName;
    }

    public String getAppntName() {
        return AppntName;
    }

    public void setAppntName(String appntName) {
        AppntName = appntName;
    }

    public String getInsuRedName() {
        return InsuRedName;
    }

    public void setInsuRedName(String insuRedName) {
        InsuRedName = insuRedName;
    }

    public String getRelationToAppnt() {
        return RelationToAppnt;
    }

    public void setRelationToAppnt(String relationToAppnt) {
        RelationToAppnt = relationToAppnt;
    }

    public String getRelationName() {
        return RelationName;
    }

    public void setRelationName(String relationName) {
        RelationName = relationName;
    }

    public String getHealthFlag() {
        return HealthFlag;
    }

    public void setHealthFlag(String healthFlag) {
        HealthFlag = healthFlag;
    }

    public String getContNo() {
        return ContNo;
    }

    public void setContNo(String contNo) {
        ContNo = contNo;
    }

    public String getInsuYear() {
        return InsuYear;
    }

    public void setInsuYear(String insuYear) {
        InsuYear = insuYear;
    }

    public String getAppntEmail() {
        return AppntEmail;
    }

    public void setAppntEmail(String appntEmail) {
        AppntEmail = appntEmail;
    }

    public String getPremium() {
        return Premium;
    }

    public void setPremium(String premium) {
        Premium = premium;
    }

    public String getRelaToInsured() {
        return RelaToInsured;
    }

    public void setRelaToInsured(String relaToInsured) {
        RelaToInsured = relaToInsured;
    }

    public String getRenewCount() {
        return RenewCount;
    }

    public void setRenewCount(String renewCount) {
        RenewCount = renewCount;
    }

    public String getAppntOccupationcode() {
        return AppntOccupationcode;
    }

    public void setAppntOccupationcode(String appntOccupationcode) {
        AppntOccupationcode = appntOccupationcode;
    }

    public String getInsuredOccupationcode() {
        return InsuredOccupationcode;
    }

    public void setInsuredOccupationcode(String insuredOccupationcode) {
        InsuredOccupationcode = insuredOccupationcode;
    }

    @Override
    public String toString() {
        return "RespRenewal{" +
                "RenewFlag='" + RenewFlag + '\'' +
                ", ProuctName='" + ProuctName + '\'' +
                ", AppntName='" + AppntName + '\'' +
                ", InsuRedName='" + InsuRedName + '\'' +
                ", RelationToAppnt='" + RelationToAppnt + '\'' +
                ", RelaToInsured='" + RelaToInsured + '\'' +
                ", RelationName='" + RelationName + '\'' +
                ", HealthFlag='" + HealthFlag + '\'' +
                ", ContNo='" + ContNo + '\'' +
                ", InsuYear='" + InsuYear + '\'' +
                ", AppntEmail='" + AppntEmail + '\'' +
                ", Premium='" + Premium + '\'' +
                ", RenewCount='" + RenewCount + '\'' +
                ", AppntOccupationcode='" + AppntOccupationcode + '\'' +
                ", InsuredOccupationcode='" + InsuredOccupationcode + '\'' +
                '}';
    }
}
