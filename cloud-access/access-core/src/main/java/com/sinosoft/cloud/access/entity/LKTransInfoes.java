package com.sinosoft.cloud.access.entity;

import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.List;

/**
 * @Author:lizhen
 * @Description:
 * @Date:Created in 10:05 2018/4/26
 * @Modified By:
 */
public class LKTransInfoes {
    @XStreamImplicit(itemFieldName="LKTransInfo")
    private List<LKTransInfo> LKTransInfoes;

    public List<LKTransInfo> getLKTransInfoes() {
        return LKTransInfoes;
    }

    public void setLKTransInfoes(List<LKTransInfo> LKTransInfoes) {
        this.LKTransInfoes = LKTransInfoes;
    }

    @Override
    public String toString() {
        return "LKTransInfoes{" +
                "LKTransInfoes=" + LKTransInfoes +
                '}';
    }
}