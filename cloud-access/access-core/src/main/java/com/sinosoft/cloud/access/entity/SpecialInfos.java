package com.sinosoft.cloud.access.entity;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.List;

/**
 * Created by zhaoshulei on 2017/9/28.
 */
@XStreamAlias("SpecialInfos")
public class SpecialInfos {

    @XStreamImplicit(itemFieldName="SpecialInfo")
    private List<SpecialInfo> specialInfos;

    public List<SpecialInfo> getSpecialInfos() {
        return specialInfos;
    }

    public void setSpecialInfos(List<SpecialInfo> specialInfos) {
        this.specialInfos = specialInfos;
    }

    @Override
    public String toString() {
        return "SpecialInfos{" +
                "specialInfos=" + specialInfos +
                '}';
    }
}
