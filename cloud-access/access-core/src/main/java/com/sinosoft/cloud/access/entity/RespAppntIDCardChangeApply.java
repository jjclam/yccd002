package com.sinosoft.cloud.access.entity;
/**
 * 投保人身份证有效期变更申请POS057
 */
public class RespAppntIDCardChangeApply {

    //保全项目编码
    private String EdorType;
    private PolicyQueryInfos PolicyQueryInfos;

    public com.sinosoft.cloud.access.entity.PolicyQueryInfos getPolicyQueryInfos() {
        return PolicyQueryInfos;
    }

    public void setPolicyQueryInfos(com.sinosoft.cloud.access.entity.PolicyQueryInfos policyQueryInfos) {
        PolicyQueryInfos = policyQueryInfos;
    }

    public String getEdorType() {
        return EdorType;
    }

    public void setEdorType(String edorType) {
        EdorType = edorType;
    }

    @Override
    public String toString() {
        return "RespAppntIDCardChangeApply{" +
                "EdorType='" + EdorType + '\'' +
                ", PolicyQueryInfos=" + PolicyQueryInfos +
                '}';
    }
}
