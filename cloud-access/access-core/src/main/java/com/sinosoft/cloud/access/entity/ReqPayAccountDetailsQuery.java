package com.sinosoft.cloud.access.entity;
/**
 * 生存给付续领账户变更详情查询POS038.
 */
public class ReqPayAccountDetailsQuery {

    //保全项目编码
    private String EdorType;
    //保单号
    private String ContNo;

    @Override
    public String toString() {
        return "ReqPayAccountDetailsQuery{" +
                "EdorType='" + EdorType + '\'' +
                ", ContNo='" + ContNo + '\'' +
                '}';
    }

    public String getEdorType() {
        return EdorType;
    }

    public void setEdorType(String edorType) {
        EdorType = edorType;
    }

    public String getContNo() {
        return ContNo;
    }

    public void setContNo(String contNo) {
        ContNo = contNo;
    }
}
