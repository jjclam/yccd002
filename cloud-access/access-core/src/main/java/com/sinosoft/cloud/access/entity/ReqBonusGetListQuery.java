package com.sinosoft.cloud.access.entity;

import com.sinosoft.lis.pos.entity.BankInfoPojo;
import com.sinosoft.lis.pos.entity.EdorAppInfoPojo;
import com.sinosoft.lis.pos.entity.PersonInfoPojo;

/**
 * 红利列表查询POS019. 返回
 */
public class ReqBonusGetListQuery {
    //保全项目编码
    private String EdorType;
    //证件号
    private String CustomerIDNo;
    //证件姓名
    private String CustomerName;
    //收付费方式
    private String PayMode;
    private EdorAppInfo EdorAppInfo;
    private PersonInfo PersonInfo;
    private BankInfo BankInfo;


    public String getEdorType() {
        return EdorType;
    }

    public void setEdorType(String edorType) {
        EdorType = edorType;
    }

    public String getCustomerIDNo() {
        return CustomerIDNo;
    }

    public void setCustomerIDNo(String customerIDNo) {
        CustomerIDNo = customerIDNo;
    }

    public String getCustomerName() {
        return CustomerName;
    }

    public void setCustomerName(String customerName) {
        CustomerName = customerName;
    }

    public String getPayMode() {
        return PayMode;
    }

    public void setPayMode(String payMode) {
        PayMode = payMode;
    }

    public com.sinosoft.cloud.access.entity.EdorAppInfo getEdorAppInfo() {
        return EdorAppInfo;
    }

    public void setEdorAppInfo(com.sinosoft.cloud.access.entity.EdorAppInfo edorAppInfo) {
        EdorAppInfo = edorAppInfo;
    }

    public com.sinosoft.cloud.access.entity.PersonInfo getPersonInfo() {
        return PersonInfo;
    }

    public void setPersonInfo(com.sinosoft.cloud.access.entity.PersonInfo personInfo) {
        PersonInfo = personInfo;
    }

    public com.sinosoft.cloud.access.entity.BankInfo getBankInfo() {
        return BankInfo;
    }

    public void setBankInfo(com.sinosoft.cloud.access.entity.BankInfo bankInfo) {
        BankInfo = bankInfo;
    }

    @Override
    public String toString() {
        return "ReqBonusGetListQuery{" +
                "EdorType='" + EdorType + '\'' +
                ", CustomerIDNo='" + CustomerIDNo + '\'' +
                ", CustomerName='" + CustomerName + '\'' +
                ", PayMode='" + PayMode + '\'' +
                ", EdorAppInfo=" + EdorAppInfo +
                ", PersonInfo=" + PersonInfo +
                ", BankInfo=" + BankInfo +
                '}';
    }
}
