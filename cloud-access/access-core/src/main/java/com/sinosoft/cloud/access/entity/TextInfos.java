package com.sinosoft.cloud.access.entity;

import com.sinosoft.lis.pos.entity.TextInfoPojo;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.List;

public class TextInfos {
    @XStreamImplicit(itemFieldName="TextInfo")
    private List<TextInfo> TextInfo;

    @Override
    public String toString() {
        return "TextInfos{" +
                "TextInfo=" + TextInfo +
                '}';
    }

    public List<com.sinosoft.cloud.access.entity.TextInfo> getTextInfo() {
        return TextInfo;
    }

    public void setTextInfo(List<com.sinosoft.cloud.access.entity.TextInfo> textInfo) {
        TextInfo = textInfo;
    }
}
