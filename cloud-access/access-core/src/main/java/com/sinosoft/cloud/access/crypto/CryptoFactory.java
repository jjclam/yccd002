package com.sinosoft.cloud.access.crypto;

import java.util.HashMap;

public class CryptoFactory {

    private static CryptoFactory cryptoFactory = new CryptoFactory();

    public static CryptoFactory getInstance() {
        return cryptoFactory;
    }

    private HashMap<String, CryptoBean> cryptoMap = new HashMap();

    public void addCrypto(String accessName, CryptoBean cryptoBean) {
        cryptoMap.put(accessName, cryptoBean);
    }

    public boolean needCrypto(String accessName) {
        return cryptoMap.containsKey(accessName);
    }

    public CryptoBean getCryptoBeanByAccess(String accessName) {

        return cryptoMap.get(accessName);
    }
}
