package com.sinosoft.cloud.access.transformer;

public enum XslFileType {
    HEADE, BODY_IN, BODY_OUT, ERROR_MSG
}
