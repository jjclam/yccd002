package com.sinosoft.cloud.access.entity;

import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.List;

/**
 * Created by Administrator on 2018/4/16.
 */
public class LCDutys {

    @XStreamImplicit(itemFieldName="LCDuty")
    private List<LCDuty> LCDutys;

    public List<LCDuty> getLCDutys() {
        return LCDutys;
    }

    public void setLCDutys(List<LCDuty> LCDutys) {
        this.LCDutys = LCDutys;
    }

    @Override
    public String toString() {
        return "LCDutys{" +
                "LCDutys=" + LCDutys +
                '}';
    }
}
