package com.sinosoft.cloud.access.entity;

/**
 * @Author:lizhen
 * @Description:
 * @Date:Created in 18:18 2018/11/16
 * @Modified By:
 */

/**
 * 信函接受方式变更申请POS068
 */
public class ReqLetterChangeApply {
    //保全项目编码
    private String EdorType;
    //客户号
    private String CustomerNo;
    //信函变更方式申请
    private XHPolicys XHPolicys;
    //申请人信息
    private EdorAppInfo EdorAppInfo;
    //领取人信息
    private PersonInfo PersonInfo;
    //xxx信息
    private BankInfo BankInfo;
    //收付费方式
    private String PayMode;

    public String getEdorType() {
        return EdorType;
    }

    public void setEdorType(String edorType) {
        EdorType = edorType;
    }

    public String getCustomerNo() {
        return CustomerNo;
    }

    public void setCustomerNo(String customerNo) {
        CustomerNo = customerNo;
    }

    public XHPolicys getXHPolicys() {
        return XHPolicys;
    }

    public void setXHPolicys(XHPolicys XHPolicys) {
        this.XHPolicys = XHPolicys;
    }

    public com.sinosoft.cloud.access.entity.EdorAppInfo getEdorAppInfo() {
        return EdorAppInfo;
    }

    public void setEdorAppInfo(com.sinosoft.cloud.access.entity.EdorAppInfo edorAppInfo) {
        EdorAppInfo = edorAppInfo;
    }

    public com.sinosoft.cloud.access.entity.PersonInfo getPersonInfo() {
        return PersonInfo;
    }

    public void setPersonInfo(com.sinosoft.cloud.access.entity.PersonInfo personInfo) {
        PersonInfo = personInfo;
    }

    public com.sinosoft.cloud.access.entity.BankInfo getBankInfo() {
        return BankInfo;
    }

    public void setBankInfo(com.sinosoft.cloud.access.entity.BankInfo bankInfo) {
        BankInfo = bankInfo;
    }

    public String getPayMode() {
        return PayMode;
    }

    public void setPayMode(String payMode) {
        PayMode = payMode;
    }

    @Override
    public String toString() {
        return "ReqLetterChangeApply{" +
                "EdorType='" + EdorType + '\'' +
                ", CustomerNo='" + CustomerNo + '\'' +
                ", XHPolicys=" + XHPolicys +
                ", EdorAppInfo=" + EdorAppInfo +
                ", PersonInfo=" + PersonInfo +
                ", BankInfo=" + BankInfo +
                ", PayMode='" + PayMode + '\'' +
                '}';
    }

}