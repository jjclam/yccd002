package com.sinosoft.cloud.access.net;

public interface Handler {


    String receive(String msg);

    String send();

    void setAccessName(String accessName);

    String decrypt(String msg);

    String encrypt(String msg);
}
