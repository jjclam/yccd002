package com.sinosoft.cloud.access.entity;
/**
 * 生存给付领取方式保全项目保单明细查询POS040.
 */
public class RespPreservePolicyDetailsQuery {

    //保全项目编码
    private String EdorType;
    //保单号
    private String ContNo;
    //领取方式
    private String GetMode;
    //开户行
    private String BankCode;
    //户名
    private String BankAccName;
    //xxx账户
    private String BankAccNo;
    //领取方式(一级)
    private String LiveGetMode;
    //领取方式(二级)
    private String LiveAccFlag;
    //领取方式(三级)
    private String GetForm;
    //领取方式校验标识
    private String JudgeFlag;

    public String getEdorType() {
        return EdorType;
    }

    public void setEdorType(String edorType) {
        EdorType = edorType;
    }

    public String getContNo() {
        return ContNo;
    }

    public void setContNo(String contNo) {
        ContNo = contNo;
    }

    public String getGetMode() {
        return GetMode;
    }

    public void setGetMode(String getMode) {
        GetMode = getMode;
    }

    public String getBankCode() {
        return BankCode;
    }

    public void setBankCode(String bankCode) {
        BankCode = bankCode;
    }

    public String getBankAccName() {
        return BankAccName;
    }

    public void setBankAccName(String bankAccName) {
        BankAccName = bankAccName;
    }

    public String getBankAccNo() {
        return BankAccNo;
    }

    public void setBankAccNo(String bankAccNo) {
        BankAccNo = bankAccNo;
    }

    public String getLiveGetMode() {
        return LiveGetMode;
    }

    public void setLiveGetMode(String liveGetMode) {
        LiveGetMode = liveGetMode;
    }

    public String getLiveAccFlag() {
        return LiveAccFlag;
    }

    public void setLiveAccFlag(String liveAccFlag) {
        LiveAccFlag = liveAccFlag;
    }

    public String getGetForm() {
        return GetForm;
    }

    public void setGetForm(String getForm) {
        GetForm = getForm;
    }

    public String getJudgeFlag() {
        return JudgeFlag;
    }

    public void setJudgeFlag(String judgeFlag) {
        JudgeFlag = judgeFlag;
    }

    @Override
    public String toString() {
        return "RespPreservePolicyDetailsQuery{" +
                "EdorType='" + EdorType + '\'' +
                ", ContNo='" + ContNo + '\'' +
                ", GetMode='" + GetMode + '\'' +
                ", BankCode='" + BankCode + '\'' +
                ", BankAccName='" + BankAccName + '\'' +
                ", BankAccNo='" + BankAccNo + '\'' +
                ", LiveGetMode='" + LiveGetMode + '\'' +
                ", LiveAccFlag='" + LiveAccFlag + '\'' +
                ", GetForm='" + GetForm + '\'' +
                ", JudgeFlag='" + JudgeFlag + '\'' +
                '}';
    }
}
