package com.sinosoft.cloud.access.entity;

/**
 * @program: abc-cloud-access
 * @description:
 * @author: BaoYongmeng
 * @create: 2018-12-28 14:57
 **/
public class ChnlInfoExtend {
    private String chnlInfoExtendCode;
    private String chnlInfoExtendValue;

    public void setChnlInfoExtendCode(String chnlInfoExtendCode) {
        this.chnlInfoExtendCode = chnlInfoExtendCode;
    }

    public void setChnlInfoExtendValue(String chnlInfoExtendValue) {
        this.chnlInfoExtendValue = chnlInfoExtendValue;
    }

    public String getChnlInfoExtendCode() {
        return chnlInfoExtendCode;
    }

    public String getChnlInfoExtendValue() {
        return chnlInfoExtendValue;
    }

    @Override
    public String toString() {
        return "ChnlInfoExtend{" +
                "chnlInfoExtendCode='" + chnlInfoExtendCode + '\'' +
                ", chnlInfoExtendValue='" + chnlInfoExtendValue + '\'' +
                '}';
    }
}
