package com.sinosoft.cloud.access.entity;

/**
 * @Author:lizhen
 * @Description:
 * @Date:Created in 23:16 2018/11/13
 * @Modified By:
 */


/**
 * 保单挂失解挂查询POS013. 返回
 */
public class RespPolicyReportQuery {
    //保全项目编码
    private String EdorType;
    private PolicyQueryInfos PolicyQueryInfos;

    public String getEdorType() {
        return EdorType;
    }

    public void setEdorType(String edorType) {
        EdorType = edorType;
    }

    public com.sinosoft.cloud.access.entity.PolicyQueryInfos getPolicyQueryInfos() {
        return PolicyQueryInfos;
    }

    public void setPolicyQueryInfos(com.sinosoft.cloud.access.entity.PolicyQueryInfos policyQueryInfos) {
        PolicyQueryInfos = policyQueryInfos;
    }

    @Override
    public String toString() {
        return "RespPolicyReportQuery{" +
                "EdorType='" + EdorType + '\'' +
                ", PolicyQueryInfos=" + PolicyQueryInfos +
                '}';
    }
}