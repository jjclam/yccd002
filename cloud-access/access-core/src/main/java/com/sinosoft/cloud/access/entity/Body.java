package com.sinosoft.cloud.access.entity;

import com.sinosoft.cloud.access.annotations.BeanCopy;


import com.sinosoft.lis.entity.LCContStatePojo;
import com.thoughtworks.xstream.annotations.XStreamAlias;

import javax.xml.crypto.NodeSetData;


@XStreamAlias("Body")
public class Body {
    @BeanCopy
    private LMriskchange LMriskchange;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.preserve", complicated = "Y")
    private RespPolicyReportQuery RespPolicyReportQuery;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.common", complicated = "Y")
    private ReqImageProcessingMIP ReqImageProcessingMIP;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.preserve", complicated = "Y")
    private ReqRenewalPayChangeApply ReqRenewalPayChangeApply;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.common", complicated = "Y")
    private ReqImageProcessing ReqImageProcessing;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.preserve", complicated = "Y")
    private ReqLetterChangeApply ReqLetterChangeApply;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.preserve", complicated = "Y")
    private ReqCustomerNationalityFATCAChangeApply ReqCustomerNationalityFATCAChangeApply;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.preserve", complicated = "Y")
    private ReqCustomerAddressEmailChangeApply ReqCustomerAddressEmailChangeApply;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.preserve", complicated = "Y")
    private ReqWT ReqWT;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.preserve")
    private ReqPolicySupplyDetailsQuery ReqPolicySupplyDetailsQuery;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.contract")
    private ReqReturnSignSure ReqReturnSignSure;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.preserve", complicated = "Y")
    private RespWTDetailsQuery RespWTDetailsQuery;
    @BeanCopy(element = "CaluParamInfo")
    private CaluParamInfos CaluParamInfos;
    @BeanCopy
    private Global Global;
    @BeanCopy(element = "CashValue")
    private CashValues CashValues;
    @BeanCopy(element = "ReducedVolume")
    private ReducedVolumes ReducedVolumes;
    @BeanCopy
    private LCPolicyInfo LCPolicyInfo;
    @BeanCopy
    private LCApi LCApi;

    @BeanCopy
    private LCCont LCCont;
    @BeanCopy
    private LCContState LCContState;

    public com.sinosoft.cloud.access.entity.LCContState getLCContState() {
        return LCContState;
    }

    public void setLCContState(com.sinosoft.cloud.access.entity.LCContState LCContState) {
        this.LCContState = LCContState;
    }
    /*   @BeanCopy
    private LCImageUpload LCImageUpload;

    @BeanCopy(element = "Node")
    private Images Images;

    @BeanCopy(element = "Image")
    private Node Node;*/

//  @BeanCopy
//   private Node Node;

 /*   @BeanCopy
    private Image Image;*/

 /*public com.sinosoft.cloud.access.entity.LCImageUpload getLCImageUpload() {
  return LCImageUpload;
 }

 public void setLCImageUpload(com.sinosoft.cloud.access.entity.LCImageUpload LCImageUpload) {
  this.LCImageUpload = LCImageUpload;
 }

 public com.sinosoft.cloud.access.entity.Images getImages() {
  return Images;
 }

 public void setImages(com.sinosoft.cloud.access.entity.Images images) {
  Images = images;
 }

 public com.sinosoft.cloud.access.entity.Node getNode() {
  return Node;
 }

 public void setNode(com.sinosoft.cloud.access.entity.Node node) {
  Node = node;
 }

 public com.sinosoft.cloud.access.entity.Image getImage() {
  return Image;
 }

 public void setImage(com.sinosoft.cloud.access.entity.Image image) {
  Image = image;
 }*/

 @BeanCopy(element = "LCPol")
    private LCPols LCPols;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.group")
    private BQResult BQResult;


    @BeanCopy(element = "ChnlInfoExtend")
    private ChnlInfoExtends ChnlInfoExtends;

    @BeanCopy
    private LCAppnt LCAppnt;
    @BeanCopy(element = "LCInsured")
    private LCInsureds LCInsureds;
    @BeanCopy(element = "LCBnf")
    private LCBnfs LCBnfs;
    @BeanCopy
    private LAAgent LAAgent;
    @BeanCopy
    private LAAgent1 LAAgent1;
    @BeanCopy
    private LACom LACom;
    @BeanCopy
    private LAQualification LAQualification;
    @BeanCopy(element = "LCSpec")
    private LCSpecs LCSpecs;
    @BeanCopy
    private LDCom LDCom;
    @BeanCopy(element = "LMRiskApp")
    private LMRiskApps LMRiskApps;
    @BeanCopy
    private LCAccount LCAccount;
    @BeanCopy(element = "LCAddress")
    private LCAddresses LCAddresses;
    @BeanCopy(element = "LCCustomerImpart")
    private LCCustomerImparts LCCustomerImparts;
    @BeanCopy(elements = "LKTransInfoes")
    private LKTransInfo LKTransInfo;
    @BeanCopy(element = "LKTransInfo")
    private LKTransInfoes LKTransInfoes;
    @BeanCopy
    private LKNRTBizTrans LKNRTBizTrans;
    @BeanCopy(elements = "LKTransStatuses")
    private LKTransStatus LKTransStatus;
    @BeanCopy(element = "LKTransStatus")
    private LKTransStatuses LKTransStatuses;
    @BeanCopy(element = "Reserve")
    private Reserves Reserves;
    private ResponseObj ResponseObj;
    private LDPerson LDPerson;
    @BeanCopy
    private LCAppntLinkManInfo LCAppntLinkManInfo;
    @BeanCopy(element = "LCDuty")
    private LCDutys LCDutys;

   //新增属性payBankName
   @XStreamAlias("payBankName")
   private String payBankName;
   //新增属性payVirtualAcountNo
   @XStreamAlias("payVirtualAcountNo")
   private String payVirtualAcountNo;
   //新增属性payAccountId
   @XStreamAlias("payAccountId")
   private String payAccountId;

   public String getPayBankName() {
      return payBankName;
   }

   public void setPayBankName(String payBankName) {
      this.payBankName = payBankName;
   }

   public String getPayVirtualAcountNo() {
      return payVirtualAcountNo;
   }

   public void setPayVirtualAcountNo(String payVirtualAcountNo) {
      this.payVirtualAcountNo = payVirtualAcountNo;
   }

   public String getPayAccountId() {
      return payAccountId;
   }

   public void setPayAccountId(String payAccountId) {
      this.payAccountId = payAccountId;
   }

   @BeanCopy(element = "LCGet")
    private LCGets LCGets;
    @BeanCopy(element = "LCPrem")
    private LCPrems LCPrems;
    @BeanCopy(element = "MIPImpart")
    private MIPImparts MIPImparts;
    @BeanCopy(element = "LDPerson")
    private LDPersons LDPersons;
    @BeanCopy(element = "LCCustomerImpartDetail")
    private LCCustomerImpartDetails LCCustomerImpartDetails;
    @BeanCopy(element = "LCCustomerImpartParams")
    private LCCustomerImpartParamses LCCustomerImpartParamses;
    //移动展业新开发
    @BeanCopy(element = "LKTransTracks")
    private LKTransTrackses LKTransTrackses;

    @BeanCopy(elements = "LKTransTrackses")
    private LKTransTracks LKTransTracks;
    @BeanCopy(element = "LJTempFee")
    private LJTempFees LJTempFees;
    @BeanCopy(elements = "LJTempFees")
    private LKTransTracks LJTempFee;
    //传入的String类型字段
    @XStreamAlias("RiskCodeWr")
    private String riskCodeWr;


    /*w微信承保传的交费方式*/
    @XStreamAlias("WICNewPayMode")
    private String WICNewPayMode;

    /*w微信承保返回的保单地址*/
    @XStreamAlias("PolicyUrl")
    private String policyUrl;

    @XStreamAlias("SysCode")
    private String sysCode;
    //传入的String类型字段
    @XStreamAlias("Version")
    private String version;
    //1004 再次核保标记
    @XStreamAlias("AgainUWFlag")
    private String againUWFlag;
    //操作员
    @XStreamAlias("Operate")
    private String operate;
    //职业类型名
    @XStreamAlias("OccupationName")
    private String occupationName;
    //当日往日标记    1为当日  2为往日
    @XStreamAlias("DateFlag")
    private String dateFlag;
    //团个单标记
    @XStreamAlias("ContType")
    private String contType;

    @XStreamAlias("OldTranno")
    private String oldTranno;

    @XStreamAlias("OldContPrtNo")
    private String oldContPrtNo;

    @XStreamAlias("OtherCompanyDieAmnt")
    private String otherCompanyDieAmnt;
    @XStreamAlias("AppNo")
    private String appNo;
    @XStreamAlias("Remark")
    private String remark;
    @XStreamAlias("HealthNotice")
    private String healthNotice;
    @XStreamAlias("ContPrtNo")
    private String contPrtNo;
    @XStreamAlias("ContNo")
    private String contNo;
    //用于将pojo中赋的死值映射到mapping配置文件中
    private String unChangedValue;
    //1004判断转保需要
    @XStreamAlias("TransferFlag")
    private String transferFlag;
    //XSL1000报文体中所需要的字段
    @XStreamAlias("Reserve")
    private String reserve;
    @XStreamAlias("InsuTime")
    private String insuTime;
    //1004所需字段
    @XStreamAlias("PayPrem")
    private String payPrem;
    //1021返回需要
    @XStreamAlias("AppntMSalary")
    private String appntMSalary;
    /*1017数字证书下载*/
    @XStreamAlias("InsuSerial")
    private String insuSerial;
    @XStreamAlias("FileTimeStamp")
    private String fileTimeStamp;
    //1001密钥重置
    @XStreamAlias("PriKey")
    private String priKey;
    @XStreamAlias("OrgKey")
    private String orgKey;
    @XStreamAlias("Risk")
    private Risk risk;
    @XStreamAlias("Risks")
    private Risks risks;
    @XStreamAlias("Insureds")
    private Insureds insureds;
    @XStreamAlias("PolicyBasicInfo")
    private PolicyBasicInfo policyBasicInfo;
    @XStreamAlias("Appnt")
    private Appnt appnt;
    @XStreamAlias("Insured")
    private Appnt insured;
    @XStreamAlias("Bnfs")
    private Bnfs bnfs;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.renewal")
    private ReqRenewalFeeQuery ReqRenewalFeeQuery;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.renewal")
    private RespRenewalFeeQuery RespRenewalFeeQuery;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.claims")
    private ReqClaimReport ReqClaimReport;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.claims")
    private RespClaimReport RespClaimReport;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.preserve")
    private RespSurrenderQuery RespSurrenderQuery;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.preserve")
    private ReqSurrenderQuery ReqSurrenderQuery;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.preserve")
    private ReqPreservationStateQuery ReqPreservationStateQuery;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.preserve")
    private RespPreservationStateQuery RespPreservationStateQuery;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.preserve")
    private ReqSpecialConventionChangeQuery ReqSpecialConventionChangeQuery;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.preserve")
    private RespSpecialConventionChangeQuery RespSpecialConventionChangeQuery;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.preserve")
    private RespNoticeAgainSend RespNoticeAgainSend;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.preserve")
    private ReqNoticeAgainSend ReqNoticeAgainSend;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.preserve")
    private RespAppntIDCardChangeCheck RespAppntIDCardChangeCheck;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.preserve")
    private ReqAppntIDCardChangeCheck ReqAppntIDCardChangeCheck;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.preserve")
    private ReqAppntIDCardChangeQuery ReqAppntIDCardChangeQuery;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.preserve")
    private RespAppntIDCardChangeQuery RespAppntIDCardChangeQuery;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.preserve")
    private ReqAccountHalfDetailsQuery ReqAccountHalfDetailsQuery;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.preserve", complicated = "Y")
    private RespAccountHalfDetailsQuery RespAccountHalfDetailsQuery;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.preserve")
    private ReqBonusUserDetails ReqBonusUserDetails;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.preserve")
    private RespBonusUserDetails RespBonusUserDetails;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.preserve", complicated = "Y")
    private ReqBonusCommitReturn ReqBonusCommitReturn;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.preserve")
    private RespBonusCommitReturn RespBonusCommitReturn;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.preserve")
    private RespBonusGetModeDetailsQuery RespBonusGetModeDetailsQuery;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.preserve")
    private ReqBonusGetModeDetailsQuery ReqBonusGetModeDetailsQuery;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.preserve")
    private RespPremSelectQuery RespPremSelectQuery;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.preserve")
    private ReqPremSelectQuery ReqPremSelectQuery;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.preserve")
    private ReqPremDictionaryClearDetailsQuery ReqPremDictionaryClearDetailsQuery;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.preserve")
    private RespPremDictionaryClearDetailsQuery RespPremDictionaryClearDetailsQuery;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.common")
    private ReqContQuery ReqContQuery;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.common")
    private RespContQuery RespContQuery;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.preserve")
    private RespSurrenderApply RespSurrenderApply;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.preserve")
    private ReqSurrenderApply ReqSurrenderApply;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.preserve", complicated = "Y")
    private ReqAccountChange ReqAccountChange;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.preserve")
    private RespAccountChange RespAccountChange;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.preserve", complicated = "Y")
    private ReqSpecialConventionChangeApply ReqSpecialConventionChangeApply;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.preserve")
    private RespSpecialConventionChangeApply RespSpecialConventionChangeApply;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.preserve", complicated = "Y")
    private ReqPolicyLoanApply ReqPolicyLoanApply;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.preserve")
    private RespPolicyLoanApply RespPolicyLoanApply;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.preserve", complicated = "Y")
    private ReqRenewalChangeApply ReqRenewalChangeApply;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.preserve")
    private RespRenewalChangeApply RespRenewalChangeApply;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.preserve", complicated = "Y")
    private ReqInsuredIdValiDateApply ReqInsuredIdValiDateApply;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.preserve")
    private RespInsuredIdValiDateApply RespInsuredIdValiDateApply;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.preserve", complicated = "Y")
    private ReqBnfIdValiDateApply ReqBnfIdValiDateApply;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.preserve")
    private RespBnfIdValiDateApply RespBnfIdValiDateApply;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.request")
    private ReqHead ReqHead;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.response")
    private RespHead RespHead;

    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.preserve")
    private RespWT RespWT;

    // TODO: 2018/11/14
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity", complicated = "Y")
    private PolicyQueryInfos PolicyQueryInfos;

    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.contract")
    private ReqRenewal ReqRenewal;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.contract")
    private RespRenewal RespRenewal;

    public com.sinosoft.cloud.access.entity.ReqRenewal getReqRenewal() {
        return ReqRenewal;
    }

    public void setReqRenewal(com.sinosoft.cloud.access.entity.ReqRenewal reqRenewal) {
        ReqRenewal = reqRenewal;
    }

    public com.sinosoft.cloud.access.entity.RespRenewal getRespRenewal() {
        return RespRenewal;
    }

    public void setRespRenewal(com.sinosoft.cloud.access.entity.RespRenewal respRenewal) {
        RespRenewal = respRenewal;
    }

    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.contract", complicated = "Y")
    private ReqWeChatPolicyQuery ReqWeChatPolicyQuery;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.contract")
    private RespWeChatPolicyQuery RespWeChatPolicyQuery;

    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.preserve")
    private ReqRenewalChangeListQuery ReqRenewalChangeListQuery;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.preserve", complicated = "Y")
    private RespRenewalChangeListQuery RespRenewalChangeListQuery;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.preserve", complicated = "Y")
    private RespWTListQuery RespWTListQuery;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.preserve")
    private ReqWTListQuery ReqWTListQuery;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.preserve", complicated = "Y")
    private RespBQProgressQuery RespBQProgressQuery;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.preserve")
    private ReqBQProgressQuery ReqBQProgressQuery;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.contract")
    private RespWhilteQuery RespWhilteQuery;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.contract")
    private ReqWhilteQuery ReqWhilteQuery;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.contract")
    private ReqWhilteApply ReqWhilteApply;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.contract")
    private RespWhilteApply RespWhilteApply;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.preserve")
    private RespPolicySupplyDetailsQuery RespPolicySupplyDetailsQuery;

    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.preserve")
    private RespUAccountDetailsQuery RespUAccountDetailsQuery;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.preserve")
    private ReqUAccountDetailsQuery ReqUAccountDetailsQuery;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.preserve")
    private ReqLifeQuery ReqLifeQuery;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.preserve")
    private RespLifeQuery RespLifeQuery;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.preserve")
    private ReqBnfIdValiDateDetailsQuery ReqBnfIdValiDateDetailsQuery;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.preserve", complicated = "Y")
    private RespBnfIdValiDateDetailsQuery RespBnfIdValiDateDetailsQuery;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.preserve")
    private RespInsuredIdValiDateDetailsQuery RespInsuredIdValiDateDetailsQuery;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.preserve")
    private ReqInsuredIdValiDateDetailsQuery ReqInsuredIdValiDateDetailsQuery;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.preserve")
    private ReqRenewalPayChangeDetailsQuery ReqRenewalPayChangeDetailsQuery;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.preserve")
    private RespRenewalPayChangeDetailsQuery RespRenewalPayChangeDetailsQuery;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.contract")
    private ReqAcceptanceCancel ReqAcceptanceCancel;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.contract")
    private RespAcceptanceCancel RespAcceptanceCancel;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.contract")
    private RespReturnSignQuery RespReturnSignQuery;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.contract")
    private ReqReturnSignQuery ReqReturnSignQuery;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.contract", complicated = "Y")
    private ReqNotRealNewContnoApply ReqNotRealNewContnoApply;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.contract", complicated = "Y")
    private RespNotRealNewContnoApply RespNotRealNewContnoApply;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.contract")
    private ReqEleCtronPolicy ReqEleCtronPolicy;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.contract")
    private RespEleCtronPolicy RespEleCtronPolicy;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.common")
    private RespPolicyUp RespPolicyUp;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.common")
    private ReqPolicyUp ReqPolicyUp;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.renewal")
    private ReqPolicyChargeInfoQuery ReqPolicyChargeInfoQuery;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.renewal")
    private RespPolicyChargeInfoQuery RespPolicyChargeInfoQuery;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.renewal")
    private ReqPolicyHang ReqPolicyHang;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.renewal")
    private RespPolicyHang RespPolicyHang;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.renewal")
    private ReqRenewalFee ReqRenewalFee;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.renewal")
    private RespRenewalFee RespRenewalFee;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.preserve")
    private RespLifeApply RespLifeApply;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.preserve", complicated = "Y")
    private ReqLifeApply ReqLifeApply;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.preserve", complicated = "Y")
    private ReqPolicySupplyApply ReqPolicySupplyApply;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.preserve")
    private RespPolicySupplyApply RespPolicySupplyApply;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.preserve")
    private ReqPolicyLoanClearDetailsQuery ReqPolicyLoanClearDetailsQuery;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.preserve")
    private RespPolicyLoanClearDetailsQuery RespPolicyLoanClearDetailsQuery;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.preserve", complicated = "Y")
    private ReqPolicyLoanClearApply ReqPolicyLoanClearApply;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.preserve")
    private RespPolicyLoanClearApply RespPolicyLoanClearApply;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.preserve", complicated = "Y")
    private ReqPremDictionaryClearApply ReqPremDictionaryClearApply;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.preserve")
    private RespPremDictionaryClearApply RespPremDictionaryClearApply;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.preserve", complicated = "Y")
    private RespPolicyLoanClearQuery RespPolicyLoanClearQuery;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.preserve")
    private ReqPolicyLoanClearQuery ReqPolicyLoanClearQuery;

    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.contract")
    private RespReturnSignSure RespReturnSignSure;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.preserve", complicated = "Y")
    private RespPolicySupplyQuery RespPolicySupplyQuery;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.preserve")
    private ReqPolicySupplyQuery ReqPolicySupplyQuery;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.preserve")
    private ReqPremDictionaryClearListQuery ReqPremDictionaryClearListQuery;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.preserve", complicated = "Y")
    private RespPremDictionaryClearListQuery RespPremDictionaryClearListQuery;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.preserve")
    private ReqPremSelectListQuery ReqPremSelectListQuery;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.preserve", complicated = "Y")
    private RespPremSelectListQuery RespPremSelectListQuery;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.preserve")
    private RespPremSelectApply RespPremSelectApply;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.preserve", complicated = "Y")
    private ReqPremSelectApply ReqPremSelectApply;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.preserve", complicated = "Y")
    private ReqPolicyReportApply ReqPolicyReportApply;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.preserve")
    private RespPolicyReportApply RespPolicyReportApply;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.preserve")
    private ReqPolicyReportQuery ReqPolicyReportQuery;

    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.preserve", complicated = "Y")
    private ReqBonusGetModeApply ReqBonusGetModeApply;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.preserve")
    private RespBonusGetModeApply RespBonusGetModeApply;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.preserve", complicated = "Y")
    private ReqUAccountApply ReqUAccountApply;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.preserve")
    private RespUAccountApply RespUAccountApply;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.preserve")
    private RespAccountHalfApply RespAccountHalfApply;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.preserve", complicated = "Y")
    private ReqAccountHalfApply ReqAccountHalfApply;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.preserve", complicated = "Y")
    private RespAccountDetailsQuery RespAccountDetailsQuery;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.preserve")
    private ReqAccountDetailsQuery ReqAccountDetailsQuery;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.preserve")
    private RespPayMentDetailsQuery RespPayMentDetailsQuery;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.preserve")
    private ReqPayMentDetailsQuery ReqPayMentDetailsQuery;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.preserve", complicated = "Y")
    private ReqPayMentApply ReqPayMentApply;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.preserve")
    private RespPayMentApply RespPayMentApply;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.preserve")
    private ReqPayAccountDetailsQuery ReqPayAccountDetailsQuery;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.preserve")
    private RespPayAccountDetailsQuery RespPayAccountDetailsQuery;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.preserve", complicated = "Y")
    private ReqPayAccountApply ReqPayAccountApply;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.preserve")
    private RespPayAccountApply RespPayAccountApply;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.preserve")
    private RespPreservePolicyDetailsQuery RespPreservePolicyDetailsQuery;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.preserve")
    private ReqPreservePolicyDetailsQuery ReqPreservePolicyDetailsQuery;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.preserve")
    private RespPreserveApply RespPreserveApply;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.preserve", complicated = "Y")
    private ReqPreserveApply ReqPreserveApply;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.renewal")
    private ReqRenewalAwoke ReqRenewalAwoke;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.renewal", complicated = "Y")
    private RespRenewalAwoke RespRenewalAwoke;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.renewal")
    private ReqRenewalAdviseQuery ReqRenewalAdviseQuery;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.renewal", complicated = "Y")
    private RespRenewalAdviseQuery RespRenewalAdviseQuery;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.claims")
    private ReqAuthentication ReqAuthentication;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.claims", complicated = "Y")
    private RespAuthentication RespAuthentication;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.preserve")
    private ReqCustomerAddressEmailChangeQuery ReqCustomerAddressEmailChangeQuery;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.preserve", complicated = "Y")
    private RespCustomerAddressEmailChangeQuery RespCustomerAddressEmailChangeQuery;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.preserve", complicated = "Y")
    private RespLetterChangeQuery RespLetterChangeQuery;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.preserve")
    private ReqLetterChangeQuery ReqLetterChangeQuery;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.preserve", complicated = "Y")
    private RespLifeCashListQuery RespLifeCashListQuery;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.preserve", complicated = "Y")
    private ReqLifeCashListQuery ReqLifeCashListQuery;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.preserve")
    private ReqWTDetailsQuery ReqWTDetailsQuery;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.preserve", complicated = "Y")
    private RespRenewalPayChangeApply RespRenewalPayChangeApply;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.preserve")
    private ReqRenewalChangeDetailsQuery ReqRenewalChangeDetailsQuery;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.preserve", complicated = "Y")
    private RespRenewalChangeDetailsQuery RespRenewalChangeDetailsQuery;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.preserve")
    private ReqPolicyLoanDetailQuery ReqPolicyLoanDetailQuery;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.preserve", complicated = "Y")
    private RespPolicyLoanDetailQuery RespPolicyLoanDetailQuery;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.preserve", complicated = "Y")
    private RespCustomerNationalityFATCAChangeApply RespCustomerNationalityFATCAChangeApply;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.preserve", complicated = "Y")
    private RespCustomerAddressEmailChangeApply RespCustomerAddressEmailChangeApply;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.preserve")
    private RespLetterChangeApply RespLetterChangeApply;

    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.preserve", complicated = "Y")
    private ReqBonusGetListQuery ReqBonusGetListQuery;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.preserve", complicated = "Y")
    private RespBonusGetListQuery RespBonusGetListQuery;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.preserve")
    private ReqBonusGetDetailsQuery ReqBonusGetDetailsQuery;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.preserve", complicated = "Y")
    private RespBonusGetDetailsQuery RespBonusGetDetailsQuery;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.preserve")
    private ReqBonusDrawPolicyList ReqBonusDrawPolicyList;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.preserve", complicated = "Y")
    private RespBonusDrawPolicyList RespBonusDrawPolicyList;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.preserve", complicated = "Y")
    private RespUAccountListQuery RespUAccountListQuery;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.preserve")
    private ReqUAccountListQuery ReqUAccountListQuery;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.preserve")
    private ReqAccountHalfQuery ReqAccountHalfQuery;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.preserve", complicated = "Y")
    private RespAccountHalfQuery RespAccountHalfQuery;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.preserve", complicated = "Y")
    private RespAccountListQuery RespAccountListQuery;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.preserve", complicated = "Y")
    private ReqAccountListQuery ReqAccountListQuery;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.preserve")
    private ReqPayMentQuery ReqPayMentQuery;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.preserve", complicated = "Y")
    private RespPayMentQuery RespPayMentQuery;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.preserve")
    private ReqLiveGoldGetQuery ReqLiveGoldGetQuery;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.preserve", complicated = "Y")
    private RespLiveGoldGetQuery RespLiveGoldGetQuery;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.preserve", complicated = "Y")
    private RespLifeCashDetailsQuery RespLifeCashDetailsQuery;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.preserve")
    private ReqLifeCashDetailsQuery ReqLifeCashDetailsQuery;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.preserve")
    private ReqBnfIdValiDateQuery ReqBnfIdValiDateQuery;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.preserve", complicated = "Y")
    private RespBnfIdValiDateQuery RespBnfIdValiDateQuery;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.preserve", complicated = "Y")
    private RespInsuredIdValiDateQuery RespInsuredIdValiDateQuery;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.preserve")
    private ReqInsuredIdValiDateQuery ReqInsuredIdValiDateQuery;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.preserve", complicated = "Y")
    private ReqAppntIDCardChangeApply ReqAppntIDCardChangeApply;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.preserve", complicated = "Y")
    private RespAppntIDCardChangeApply RespAppntIDCardChangeApply;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.preserve", complicated = "Y")
    private RespNoticeAgainSendListQuery RespNoticeAgainSendListQuery;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.preserve")
    private ReqNoticeAgainSendListQuery ReqNoticeAgainSendListQuery;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.preserve")
    private ReqRenewalPayChangeListQuery ReqRenewalPayChangeListQuery;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.preserve", complicated = "Y")
    private RespRenewalPayChangeListQuery RespRenewalPayChangeListQuery;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.preserve")
    private ReqCustomerNationalityFATCAChangeQuery ReqCustomerNationalityFATCAChangeQuery;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.preserve", complicated = "Y")
    private RespCustomerNationalityFATCAChangeQuery RespCustomerNationalityFATCAChangeQuery;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.preserve")
    private ReqPolicyLoanListQuery ReqPolicyLoanListQuery;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.preserve", complicated = "Y")
    private RespPolicyLoanListQuery RespPolicyLoanListQuery;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.common", complicated = "Y")
    private RespImageProcessing RespImageProcessing;
    /*  @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.common",complicated = "Y")
      private ReqImageProcessing ReqImageProcessing;*/
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.hardparse")
    private ReqRetypePolicy ReqRetypePolicy;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.common")
    private ReqPolicyCashValueQuery ReqPolicyCashValueQuery;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.preserve")
    private ReqPayAccountListQuery ReqPayAccountListQuery;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.preserve", complicated = "Y")
    private RespPayAccountListQuery RespPayAccountListQuery;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.hardparse")
    private ReqContDetailsQuery ReqContDetailsQuery;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.preserve")
    private RespPolicyLoanClearApplyWebSite RespPolicyLoanClearApplyWebSite;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.preserve")
    private ReqPolicyLoanClearApplyWebSite ReqPolicyLoanClearApplyWebSite;

    /*----------------------------*/
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.preserve")
    private RespHandChargeAddPrem RespHandChargeAddPrem;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.preserve")
    private ReqHandChargeAddPrem ReqHandChargeAddPrem;

    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.preserve")
    private ReqAddDefend ReqAddDefend;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.preserve")
    private RespAddDefend RespAddDefend;

    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.preserve", complicated = "Y")
    private RespBarterpaperBumfQuery RespBarterpaperBumfQuery;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.preserve")
    private ReqBarterpaperBumfQuery ReqBarterpaperBumfQuery;

    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.preserve")
    private ReqBarterpaperBumfApply ReqBarterpaperBumfApply;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.preserve")
    private RespBarterpaperBumfApply RespBarterpaperBumfApply;

    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.preserve", complicated = "Y")
    private RespCustomerService RespCustomerService;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.preserve")
    private ReqCustomerService ReqCustomerService;

    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.preserve", complicated = "Y")
    private RespSpecialConventionListQuery RespSpecialConventionListQuery;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.preserve")
    private ReqSpecialConventionListQuery ReqSpecialConventionListQuery;

    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.preserve")
    private RespExpirationApply RespExpirationApply;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.preserve", complicated = "Y")
    private ReqExpirationApply ReqExpirationApply;

    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.preserve")
    private ReqPremSelectApplyWebSite ReqPremSelectApplyWebSite;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.preserve")
    private RespPremSelectApplyWebSite RespPremSelectApplyWebSite;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.preserve", complicated = "Y")
    private RespExpirationQuery RespExpirationQuery;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.preserve")
    private ReqExpirationQuery ReqExpirationQuery;

    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.preserve", complicated = "Y")
    private RespPreservePolicyListQuery RespPreservePolicyListQuery;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.preserve")
    private ReqPreservePolicyListQuery ReqPreservePolicyListQuery;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.preserve")
    private ReqBonusGetModeListQuery ReqBonusGetModeListQuery;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.preserve", complicated = "Y")
    private RespBonusGetModeListQuery RespBonusGetModeListQuery;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.claims", complicated = "Y")
    private RespFrameWorkQuery RespFrameWorkQuery;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.claims")
    private ReqFrameWorkQuery ReqFrameWorkQuery;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.claims")
    private ReqClaimStatusQuery ReqClaimStatusQuery;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.claims", complicated = "Y")
    private RespClaimStatusQuery RespClaimStatusQuery;

    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.preserve")
    private ReqWebSiteSurrenderApply ReqWebSiteSurrenderApply;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.preserve")
    private RespWebSiteSurrenderApply RespWebSiteSurrenderApply;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.preserve")
    private ReqWebSiteCostVerify ReqWebSiteCostVerify;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.preserve")
    private RespWebSiteCostVerify RespWebSiteCostVerify;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.common")
    private RespWebSitePolicyValueQuery RespWebSitePolicyValueQuery;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.common")
    private ReqWebSitePolicyValueQuery ReqWebSitePolicyValueQuery;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.common")
    private ReqAgentCharge ReqAgentCharge;
    @BeanCopy(targetPackage = "com.sinosoft.lis.pos.entity.common")
    private RespAgentCharge RespAgentCharge;

    public com.sinosoft.cloud.access.entity.ChnlInfoExtends getChnlInfoExtends() {
        return ChnlInfoExtends;
    }

    public void setChnlInfoExtends(com.sinosoft.cloud.access.entity.ChnlInfoExtends chnlInfoExtends) {
        ChnlInfoExtends = chnlInfoExtends;
    }

    public void setRespExpirationQuery(com.sinosoft.cloud.access.entity.RespExpirationQuery respExpirationQuery) {
        RespExpirationQuery = respExpirationQuery;
    }

    public void setReqExpirationQuery(com.sinosoft.cloud.access.entity.ReqExpirationQuery reqExpirationQuery) {
        ReqExpirationQuery = reqExpirationQuery;
    }

    public void setRespPreservePolicyListQuery(com.sinosoft.cloud.access.entity.RespPreservePolicyListQuery respPreservePolicyListQuery) {
        RespPreservePolicyListQuery = respPreservePolicyListQuery;
    }

    public void setReqPreservePolicyListQuery(com.sinosoft.cloud.access.entity.ReqPreservePolicyListQuery reqPreservePolicyListQuery) {
        ReqPreservePolicyListQuery = reqPreservePolicyListQuery;
    }

    public com.sinosoft.cloud.access.entity.RespExpirationQuery getRespExpirationQuery() {
        return RespExpirationQuery;
    }

    public com.sinosoft.cloud.access.entity.ReqExpirationQuery getReqExpirationQuery() {
        return ReqExpirationQuery;
    }

    public com.sinosoft.cloud.access.entity.RespPreservePolicyListQuery getRespPreservePolicyListQuery() {
        return RespPreservePolicyListQuery;
    }

    public com.sinosoft.cloud.access.entity.ReqPreservePolicyListQuery getReqPreservePolicyListQuery() {
        return ReqPreservePolicyListQuery;
    }

    public com.sinosoft.cloud.access.entity.RespPolicyLoanClearApplyWebSite getRespPolicyLoanClearApplyWebSite() {
        return RespPolicyLoanClearApplyWebSite;
    }

    public void setRespPolicyLoanClearApplyWebSite(com.sinosoft.cloud.access.entity.RespPolicyLoanClearApplyWebSite respPolicyLoanClearApplyWebSite) {
        RespPolicyLoanClearApplyWebSite = respPolicyLoanClearApplyWebSite;
    }

    public com.sinosoft.cloud.access.entity.ReqPolicyLoanClearApplyWebSite getReqPolicyLoanClearApplyWebSite() {
        return ReqPolicyLoanClearApplyWebSite;
    }

    public void setReqPolicyLoanClearApplyWebSite(com.sinosoft.cloud.access.entity.ReqPolicyLoanClearApplyWebSite reqPolicyLoanClearApplyWebSite) {
        ReqPolicyLoanClearApplyWebSite = reqPolicyLoanClearApplyWebSite;
    }

    public com.sinosoft.cloud.access.entity.ReqContDetailsQuery getReqContDetailsQuery() {
        return ReqContDetailsQuery;
    }

    public void setReqContDetailsQuery(com.sinosoft.cloud.access.entity.ReqContDetailsQuery reqContDetailsQuery) {
        ReqContDetailsQuery = reqContDetailsQuery;
    }

    public com.sinosoft.cloud.access.entity.ReqRetypePolicy getReqRetypePolicy() {
        return ReqRetypePolicy;
    }

    public void setReqRetypePolicy(com.sinosoft.cloud.access.entity.ReqRetypePolicy reqRetypePolicy) {
        ReqRetypePolicy = reqRetypePolicy;
    }

    public com.sinosoft.cloud.access.entity.RespLifeCashListQuery getRespLifeCashListQuery() {
        return RespLifeCashListQuery;
    }

    public void setRespLifeCashListQuery(com.sinosoft.cloud.access.entity.RespLifeCashListQuery respLifeCashListQuery) {
        RespLifeCashListQuery = respLifeCashListQuery;
    }

    public com.sinosoft.cloud.access.entity.ReqLifeCashListQuery getReqLifeCashListQuery() {
        return ReqLifeCashListQuery;
    }

    public void setReqLifeCashListQuery(com.sinosoft.cloud.access.entity.ReqLifeCashListQuery reqLifeCashListQuery) {
        ReqLifeCashListQuery = reqLifeCashListQuery;
    }

    public com.sinosoft.cloud.access.entity.ReqWTDetailsQuery getReqWTDetailsQuery() {
        return ReqWTDetailsQuery;
    }

    public void setReqWTDetailsQuery(com.sinosoft.cloud.access.entity.ReqWTDetailsQuery reqWTDetailsQuery) {
        ReqWTDetailsQuery = reqWTDetailsQuery;
    }

    public com.sinosoft.cloud.access.entity.RespRenewalPayChangeApply getRespRenewalPayChangeApply() {
        return RespRenewalPayChangeApply;
    }

    public void setRespRenewalPayChangeApply(com.sinosoft.cloud.access.entity.RespRenewalPayChangeApply respRenewalPayChangeApply) {
        RespRenewalPayChangeApply = respRenewalPayChangeApply;
    }

    public com.sinosoft.cloud.access.entity.ReqRenewalChangeDetailsQuery getReqRenewalChangeDetailsQuery() {
        return ReqRenewalChangeDetailsQuery;
    }

    public void setReqRenewalChangeDetailsQuery(com.sinosoft.cloud.access.entity.ReqRenewalChangeDetailsQuery reqRenewalChangeDetailsQuery) {
        ReqRenewalChangeDetailsQuery = reqRenewalChangeDetailsQuery;
    }

    public com.sinosoft.cloud.access.entity.ReqPolicyLoanDetailQuery getReqPolicyLoanDetailQuery() {
        return ReqPolicyLoanDetailQuery;
    }

    public void setReqPolicyLoanDetailQuery(com.sinosoft.cloud.access.entity.ReqPolicyLoanDetailQuery reqPolicyLoanDetailQuery) {
        ReqPolicyLoanDetailQuery = reqPolicyLoanDetailQuery;
    }

    public com.sinosoft.cloud.access.entity.RespCustomerNationalityFATCAChangeApply getRespCustomerNationalityFATCAChangeApply() {
        return RespCustomerNationalityFATCAChangeApply;
    }

    public void setRespCustomerNationalityFATCAChangeApply(com.sinosoft.cloud.access.entity.RespCustomerNationalityFATCAChangeApply respCustomerNationalityFATCAChangeApply) {
        RespCustomerNationalityFATCAChangeApply = respCustomerNationalityFATCAChangeApply;
    }

    public com.sinosoft.cloud.access.entity.RespCustomerAddressEmailChangeApply getRespCustomerAddressEmailChangeApply() {
        return RespCustomerAddressEmailChangeApply;
    }

    public void setRespCustomerAddressEmailChangeApply(com.sinosoft.cloud.access.entity.RespCustomerAddressEmailChangeApply respCustomerAddressEmailChangeApply) {
        RespCustomerAddressEmailChangeApply = respCustomerAddressEmailChangeApply;
    }

    public com.sinosoft.cloud.access.entity.RespLetterChangeApply getRespLetterChangeApply() {
        return RespLetterChangeApply;
    }

    public void setRespLetterChangeApply(com.sinosoft.cloud.access.entity.RespLetterChangeApply respLetterChangeApply) {
        RespLetterChangeApply = respLetterChangeApply;
    }

    public com.sinosoft.cloud.access.entity.RespRenewalChangeListQuery getRespRenewalChangeListQuery() {
        return RespRenewalChangeListQuery;
    }

    public void setRespRenewalChangeListQuery(com.sinosoft.cloud.access.entity.RespRenewalChangeListQuery respRenewalChangeListQuery) {
        RespRenewalChangeListQuery = respRenewalChangeListQuery;
    }

    public com.sinosoft.cloud.access.entity.RespReturnSignSure getRespReturnSignSure() {
        return RespReturnSignSure;
    }

    public void setRespReturnSignSure(com.sinosoft.cloud.access.entity.RespReturnSignSure respReturnSignSure) {
        RespReturnSignSure = respReturnSignSure;
    }

    public com.sinosoft.cloud.access.entity.RespLetterChangeQuery getRespLetterChangeQuery() {
        return RespLetterChangeQuery;
    }

    public void setRespLetterChangeQuery(com.sinosoft.cloud.access.entity.RespLetterChangeQuery respLetterChangeQuery) {
        RespLetterChangeQuery = respLetterChangeQuery;
    }

    public com.sinosoft.cloud.access.entity.ReqLetterChangeQuery getReqLetterChangeQuery() {
        return ReqLetterChangeQuery;
    }

    public void setReqLetterChangeQuery(com.sinosoft.cloud.access.entity.ReqLetterChangeQuery reqLetterChangeQuery) {
        ReqLetterChangeQuery = reqLetterChangeQuery;
    }

    public com.sinosoft.cloud.access.entity.ReqCustomerNationalityFATCAChangeQuery getReqCustomerNationalityFATCAChangeQuery() {
        return ReqCustomerNationalityFATCAChangeQuery;
    }

    public void setReqCustomerNationalityFATCAChangeQuery(com.sinosoft.cloud.access.entity.ReqCustomerNationalityFATCAChangeQuery reqCustomerNationalityFATCAChangeQuery) {
        ReqCustomerNationalityFATCAChangeQuery = reqCustomerNationalityFATCAChangeQuery;
    }

    public com.sinosoft.cloud.access.entity.RespCustomerNationalityFATCAChangeQuery getRespCustomerNationalityFATCAChangeQuery() {
        return RespCustomerNationalityFATCAChangeQuery;
    }

    public void setRespCustomerNationalityFATCAChangeQuery(com.sinosoft.cloud.access.entity.RespCustomerNationalityFATCAChangeQuery respCustomerNationalityFATCAChangeQuery) {
        RespCustomerNationalityFATCAChangeQuery = respCustomerNationalityFATCAChangeQuery;
    }

    public com.sinosoft.cloud.access.entity.ReqPolicyLoanListQuery getReqPolicyLoanListQuery() {
        return ReqPolicyLoanListQuery;
    }

    public void setReqPolicyLoanListQuery(com.sinosoft.cloud.access.entity.ReqPolicyLoanListQuery reqPolicyLoanListQuery) {
        ReqPolicyLoanListQuery = reqPolicyLoanListQuery;
    }

    public com.sinosoft.cloud.access.entity.RespPolicyLoanListQuery getRespPolicyLoanListQuery() {
        return RespPolicyLoanListQuery;
    }

    public void setRespPolicyLoanListQuery(com.sinosoft.cloud.access.entity.RespPolicyLoanListQuery respPolicyLoanListQuery) {
        RespPolicyLoanListQuery = respPolicyLoanListQuery;
    }

    public com.sinosoft.cloud.access.entity.ReqRenewalAwoke getReqRenewalAwoke() {
        return ReqRenewalAwoke;
    }

    public void setReqRenewalAwoke(com.sinosoft.cloud.access.entity.ReqRenewalAwoke reqRenewalAwoke) {
        ReqRenewalAwoke = reqRenewalAwoke;
    }

    public com.sinosoft.cloud.access.entity.RespRenewalAwoke getRespRenewalAwoke() {
        return RespRenewalAwoke;
    }

    public void setRespRenewalAwoke(com.sinosoft.cloud.access.entity.RespRenewalAwoke respRenewalAwoke) {
        RespRenewalAwoke = respRenewalAwoke;
    }

    public com.sinosoft.cloud.access.entity.ReqRenewalAdviseQuery getReqRenewalAdviseQuery() {
        return ReqRenewalAdviseQuery;
    }

    public void setReqRenewalAdviseQuery(com.sinosoft.cloud.access.entity.ReqRenewalAdviseQuery reqRenewalAdviseQuery) {
        ReqRenewalAdviseQuery = reqRenewalAdviseQuery;
    }

    public com.sinosoft.cloud.access.entity.RespRenewalAdviseQuery getRespRenewalAdviseQuery() {
        return RespRenewalAdviseQuery;
    }

    public void setRespRenewalAdviseQuery(com.sinosoft.cloud.access.entity.RespRenewalAdviseQuery respRenewalAdviseQuery) {
        RespRenewalAdviseQuery = respRenewalAdviseQuery;
    }

    public com.sinosoft.cloud.access.entity.ReqAuthentication getReqAuthentication() {
        return ReqAuthentication;
    }

    public void setReqAuthentication(com.sinosoft.cloud.access.entity.ReqAuthentication reqAuthentication) {
        ReqAuthentication = reqAuthentication;
    }

    public com.sinosoft.cloud.access.entity.RespAuthentication getRespAuthentication() {
        return RespAuthentication;
    }

    public void setRespAuthentication(com.sinosoft.cloud.access.entity.RespAuthentication respAuthentication) {
        RespAuthentication = respAuthentication;
    }

    public com.sinosoft.cloud.access.entity.ReqCustomerAddressEmailChangeQuery getReqCustomerAddressEmailChangeQuery() {
        return ReqCustomerAddressEmailChangeQuery;
    }

    public void setReqCustomerAddressEmailChangeQuery(com.sinosoft.cloud.access.entity.ReqCustomerAddressEmailChangeQuery reqCustomerAddressEmailChangeQuery) {
        ReqCustomerAddressEmailChangeQuery = reqCustomerAddressEmailChangeQuery;
    }

    public com.sinosoft.cloud.access.entity.RespCustomerAddressEmailChangeQuery getRespCustomerAddressEmailChangeQuery() {
        return RespCustomerAddressEmailChangeQuery;
    }

    public void setRespCustomerAddressEmailChangeQuery(com.sinosoft.cloud.access.entity.RespCustomerAddressEmailChangeQuery respCustomerAddressEmailChangeQuery) {
        RespCustomerAddressEmailChangeQuery = respCustomerAddressEmailChangeQuery;
    }

    public com.sinosoft.cloud.access.entity.RespImageProcessing getRespImageProcessing() {
        return RespImageProcessing;
    }

    public void setRespImageProcessing(com.sinosoft.cloud.access.entity.RespImageProcessing respImageProcessing) {
        RespImageProcessing = respImageProcessing;
    }

    public com.sinosoft.cloud.access.entity.ReqBonusGetListQuery getReqBonusGetListQuery() {
        return ReqBonusGetListQuery;
    }

    public void setReqBonusGetListQuery(com.sinosoft.cloud.access.entity.ReqBonusGetListQuery reqBonusGetListQuery) {
        ReqBonusGetListQuery = reqBonusGetListQuery;
    }

    public com.sinosoft.cloud.access.entity.RespBonusGetListQuery getRespBonusGetListQuery() {
        return RespBonusGetListQuery;
    }

    public void setRespBonusGetListQuery(com.sinosoft.cloud.access.entity.RespBonusGetListQuery respBonusGetListQuery) {
        RespBonusGetListQuery = respBonusGetListQuery;
    }

    public com.sinosoft.cloud.access.entity.ReqBonusGetDetailsQuery getReqBonusGetDetailsQuery() {
        return ReqBonusGetDetailsQuery;
    }

    public void setReqBonusGetDetailsQuery(com.sinosoft.cloud.access.entity.ReqBonusGetDetailsQuery reqBonusGetDetailsQuery) {
        ReqBonusGetDetailsQuery = reqBonusGetDetailsQuery;
    }

    public com.sinosoft.cloud.access.entity.RespBonusGetDetailsQuery getRespBonusGetDetailsQuery() {
        return RespBonusGetDetailsQuery;
    }

    public void setRespBonusGetDetailsQuery(com.sinosoft.cloud.access.entity.RespBonusGetDetailsQuery respBonusGetDetailsQuery) {
        RespBonusGetDetailsQuery = respBonusGetDetailsQuery;
    }

    public com.sinosoft.cloud.access.entity.ReqBonusDrawPolicyList getReqBonusDrawPolicyList() {
        return ReqBonusDrawPolicyList;
    }

    public void setReqBonusDrawPolicyList(com.sinosoft.cloud.access.entity.ReqBonusDrawPolicyList reqBonusDrawPolicyList) {
        ReqBonusDrawPolicyList = reqBonusDrawPolicyList;
    }

    public com.sinosoft.cloud.access.entity.RespBonusDrawPolicyList getRespBonusDrawPolicyList() {
        return RespBonusDrawPolicyList;
    }

    public void setRespBonusDrawPolicyList(com.sinosoft.cloud.access.entity.RespBonusDrawPolicyList respBonusDrawPolicyList) {
        RespBonusDrawPolicyList = respBonusDrawPolicyList;
    }

    public com.sinosoft.cloud.access.entity.RespUAccountListQuery getRespUAccountListQuery() {
        return RespUAccountListQuery;
    }

    public void setRespUAccountListQuery(com.sinosoft.cloud.access.entity.RespUAccountListQuery respUAccountListQuery) {
        RespUAccountListQuery = respUAccountListQuery;
    }

    public com.sinosoft.cloud.access.entity.ReqUAccountListQuery getReqUAccountListQuery() {
        return ReqUAccountListQuery;
    }

    public void setReqUAccountListQuery(com.sinosoft.cloud.access.entity.ReqUAccountListQuery reqUAccountListQuery) {
        ReqUAccountListQuery = reqUAccountListQuery;
    }

    public com.sinosoft.cloud.access.entity.ReqAccountHalfQuery getReqAccountHalfQuery() {
        return ReqAccountHalfQuery;
    }

    public void setReqAccountHalfQuery(com.sinosoft.cloud.access.entity.ReqAccountHalfQuery reqAccountHalfQuery) {
        ReqAccountHalfQuery = reqAccountHalfQuery;
    }

    public com.sinosoft.cloud.access.entity.RespAccountHalfQuery getRespAccountHalfQuery() {
        return RespAccountHalfQuery;
    }

    public void setRespAccountHalfQuery(com.sinosoft.cloud.access.entity.RespAccountHalfQuery respAccountHalfQuery) {
        RespAccountHalfQuery = respAccountHalfQuery;
    }

    public com.sinosoft.cloud.access.entity.RespAccountListQuery getRespAccountListQuery() {
        return RespAccountListQuery;
    }

    public void setRespAccountListQuery(com.sinosoft.cloud.access.entity.RespAccountListQuery respAccountListQuery) {
        RespAccountListQuery = respAccountListQuery;
    }

    public com.sinosoft.cloud.access.entity.ReqAccountListQuery getReqAccountListQuery() {
        return ReqAccountListQuery;
    }

    public void setReqAccountListQuery(com.sinosoft.cloud.access.entity.ReqAccountListQuery reqAccountListQuery) {
        ReqAccountListQuery = reqAccountListQuery;
    }

    public com.sinosoft.cloud.access.entity.ReqPayMentQuery getReqPayMentQuery() {
        return ReqPayMentQuery;
    }

    public void setReqPayMentQuery(com.sinosoft.cloud.access.entity.ReqPayMentQuery reqPayMentQuery) {
        ReqPayMentQuery = reqPayMentQuery;
    }

    public com.sinosoft.cloud.access.entity.RespPayMentQuery getRespPayMentQuery() {
        return RespPayMentQuery;
    }

    public void setRespPayMentQuery(com.sinosoft.cloud.access.entity.RespPayMentQuery respPayMentQuery) {
        RespPayMentQuery = respPayMentQuery;
    }

    public com.sinosoft.cloud.access.entity.ReqLiveGoldGetQuery getReqLiveGoldGetQuery() {
        return ReqLiveGoldGetQuery;
    }

    public void setReqLiveGoldGetQuery(com.sinosoft.cloud.access.entity.ReqLiveGoldGetQuery reqLiveGoldGetQuery) {
        ReqLiveGoldGetQuery = reqLiveGoldGetQuery;
    }

    public com.sinosoft.cloud.access.entity.RespLiveGoldGetQuery getRespLiveGoldGetQuery() {
        return RespLiveGoldGetQuery;
    }

    public void setRespLiveGoldGetQuery(com.sinosoft.cloud.access.entity.RespLiveGoldGetQuery respLiveGoldGetQuery) {
        RespLiveGoldGetQuery = respLiveGoldGetQuery;
    }

    public com.sinosoft.cloud.access.entity.RespLifeCashDetailsQuery getRespLifeCashDetailsQuery() {
        return RespLifeCashDetailsQuery;
    }

    public void setRespLifeCashDetailsQuery(com.sinosoft.cloud.access.entity.RespLifeCashDetailsQuery respLifeCashDetailsQuery) {
        RespLifeCashDetailsQuery = respLifeCashDetailsQuery;
    }

    public com.sinosoft.cloud.access.entity.ReqLifeCashDetailsQuery getReqLifeCashDetailsQuery() {
        return ReqLifeCashDetailsQuery;
    }

    public void setReqLifeCashDetailsQuery(com.sinosoft.cloud.access.entity.ReqLifeCashDetailsQuery reqLifeCashDetailsQuery) {
        ReqLifeCashDetailsQuery = reqLifeCashDetailsQuery;
    }

    public com.sinosoft.cloud.access.entity.ReqBnfIdValiDateQuery getReqBnfIdValiDateQuery() {
        return ReqBnfIdValiDateQuery;
    }

    public void setReqBnfIdValiDateQuery(com.sinosoft.cloud.access.entity.ReqBnfIdValiDateQuery reqBnfIdValiDateQuery) {
        ReqBnfIdValiDateQuery = reqBnfIdValiDateQuery;
    }

    public com.sinosoft.cloud.access.entity.RespBnfIdValiDateQuery getRespBnfIdValiDateQuery() {
        return RespBnfIdValiDateQuery;
    }

    public void setRespBnfIdValiDateQuery(com.sinosoft.cloud.access.entity.RespBnfIdValiDateQuery respBnfIdValiDateQuery) {
        RespBnfIdValiDateQuery = respBnfIdValiDateQuery;
    }

    public com.sinosoft.cloud.access.entity.RespInsuredIdValiDateQuery getRespInsuredIdValiDateQuery() {
        return RespInsuredIdValiDateQuery;
    }

    public void setRespInsuredIdValiDateQuery(com.sinosoft.cloud.access.entity.RespInsuredIdValiDateQuery respInsuredIdValiDateQuery) {
        RespInsuredIdValiDateQuery = respInsuredIdValiDateQuery;
    }

    public com.sinosoft.cloud.access.entity.ReqInsuredIdValiDateQuery getReqInsuredIdValiDateQuery() {
        return ReqInsuredIdValiDateQuery;
    }

    public void setReqInsuredIdValiDateQuery(com.sinosoft.cloud.access.entity.ReqInsuredIdValiDateQuery reqInsuredIdValiDateQuery) {
        ReqInsuredIdValiDateQuery = reqInsuredIdValiDateQuery;
    }

    public com.sinosoft.cloud.access.entity.ReqAppntIDCardChangeApply getReqAppntIDCardChangeApply() {
        return ReqAppntIDCardChangeApply;
    }

    public void setReqAppntIDCardChangeApply(com.sinosoft.cloud.access.entity.ReqAppntIDCardChangeApply reqAppntIDCardChangeApply) {
        ReqAppntIDCardChangeApply = reqAppntIDCardChangeApply;
    }

    public com.sinosoft.cloud.access.entity.RespAppntIDCardChangeApply getRespAppntIDCardChangeApply() {
        return RespAppntIDCardChangeApply;
    }

    public void setRespAppntIDCardChangeApply(com.sinosoft.cloud.access.entity.RespAppntIDCardChangeApply respAppntIDCardChangeApply) {
        RespAppntIDCardChangeApply = respAppntIDCardChangeApply;
    }

    public com.sinosoft.cloud.access.entity.RespNoticeAgainSendListQuery getRespNoticeAgainSendListQuery() {
        return RespNoticeAgainSendListQuery;
    }

    public void setRespNoticeAgainSendListQuery(com.sinosoft.cloud.access.entity.RespNoticeAgainSendListQuery respNoticeAgainSendListQuery) {
        RespNoticeAgainSendListQuery = respNoticeAgainSendListQuery;
    }

    public com.sinosoft.cloud.access.entity.ReqNoticeAgainSendListQuery getReqNoticeAgainSendListQuery() {
        return ReqNoticeAgainSendListQuery;
    }

    public void setReqNoticeAgainSendListQuery(com.sinosoft.cloud.access.entity.ReqNoticeAgainSendListQuery reqNoticeAgainSendListQuery) {
        ReqNoticeAgainSendListQuery = reqNoticeAgainSendListQuery;
    }

    public com.sinosoft.cloud.access.entity.ReqRenewalPayChangeListQuery getReqRenewalPayChangeListQuery() {
        return ReqRenewalPayChangeListQuery;
    }

    public void setReqRenewalPayChangeListQuery(com.sinosoft.cloud.access.entity.ReqRenewalPayChangeListQuery reqRenewalPayChangeListQuery) {
        ReqRenewalPayChangeListQuery = reqRenewalPayChangeListQuery;
    }

    public com.sinosoft.cloud.access.entity.RespRenewalPayChangeListQuery getRespRenewalPayChangeListQuery() {
        return RespRenewalPayChangeListQuery;
    }

    public void setRespRenewalPayChangeListQuery(com.sinosoft.cloud.access.entity.RespRenewalPayChangeListQuery respRenewalPayChangeListQuery) {
        RespRenewalPayChangeListQuery = respRenewalPayChangeListQuery;
    }

    public com.sinosoft.cloud.access.entity.RespWTListQuery getRespWTListQuery() {
        return RespWTListQuery;
    }

    public void setRespWTListQuery(com.sinosoft.cloud.access.entity.RespWTListQuery respWTListQuery) {
        RespWTListQuery = respWTListQuery;
    }

    public com.sinosoft.cloud.access.entity.ReqWTListQuery getReqWTListQuery() {
        return ReqWTListQuery;
    }

    public void setReqWTListQuery(com.sinosoft.cloud.access.entity.ReqWTListQuery reqWTListQuery) {
        ReqWTListQuery = reqWTListQuery;
    }

    public com.sinosoft.cloud.access.entity.ReqBonusGetModeApply getReqBonusGetModeApply() {
        return ReqBonusGetModeApply;
    }

    public void setReqBonusGetModeApply(com.sinosoft.cloud.access.entity.ReqBonusGetModeApply reqBonusGetModeApply) {
        ReqBonusGetModeApply = reqBonusGetModeApply;
    }

    public com.sinosoft.cloud.access.entity.RespBonusGetModeApply getRespBonusGetModeApply() {
        return RespBonusGetModeApply;
    }

    public void setRespBonusGetModeApply(com.sinosoft.cloud.access.entity.RespBonusGetModeApply respBonusGetModeApply) {
        RespBonusGetModeApply = respBonusGetModeApply;
    }

    public com.sinosoft.cloud.access.entity.ReqUAccountApply getReqUAccountApply() {
        return ReqUAccountApply;
    }

    public void setReqUAccountApply(com.sinosoft.cloud.access.entity.ReqUAccountApply reqUAccountApply) {
        ReqUAccountApply = reqUAccountApply;
    }

    public com.sinosoft.cloud.access.entity.RespUAccountApply getRespUAccountApply() {
        return RespUAccountApply;
    }

    public void setRespUAccountApply(com.sinosoft.cloud.access.entity.RespUAccountApply respUAccountApply) {
        RespUAccountApply = respUAccountApply;
    }

    public com.sinosoft.cloud.access.entity.RespAccountHalfApply getRespAccountHalfApply() {
        return RespAccountHalfApply;
    }

    public void setRespAccountHalfApply(com.sinosoft.cloud.access.entity.RespAccountHalfApply respAccountHalfApply) {
        RespAccountHalfApply = respAccountHalfApply;
    }

    public com.sinosoft.cloud.access.entity.ReqAccountHalfApply getReqAccountHalfApply() {
        return ReqAccountHalfApply;
    }

    public void setReqAccountHalfApply(com.sinosoft.cloud.access.entity.ReqAccountHalfApply reqAccountHalfApply) {
        ReqAccountHalfApply = reqAccountHalfApply;
    }

    public com.sinosoft.cloud.access.entity.RespAccountDetailsQuery getRespAccountDetailsQuery() {
        return RespAccountDetailsQuery;
    }

    public void setRespAccountDetailsQuery(com.sinosoft.cloud.access.entity.RespAccountDetailsQuery respAccountDetailsQuery) {
        RespAccountDetailsQuery = respAccountDetailsQuery;
    }

    public com.sinosoft.cloud.access.entity.ReqAccountDetailsQuery getReqAccountDetailsQuery() {
        return ReqAccountDetailsQuery;
    }

    public void setReqAccountDetailsQuery(com.sinosoft.cloud.access.entity.ReqAccountDetailsQuery reqAccountDetailsQuery) {
        ReqAccountDetailsQuery = reqAccountDetailsQuery;
    }

    public com.sinosoft.cloud.access.entity.RespPayMentDetailsQuery getRespPayMentDetailsQuery() {
        return RespPayMentDetailsQuery;
    }

    public void setRespPayMentDetailsQuery(com.sinosoft.cloud.access.entity.RespPayMentDetailsQuery respPayMentDetailsQuery) {
        RespPayMentDetailsQuery = respPayMentDetailsQuery;
    }

    public com.sinosoft.cloud.access.entity.ReqPayMentDetailsQuery getReqPayMentDetailsQuery() {
        return ReqPayMentDetailsQuery;
    }

    public void setReqPayMentDetailsQuery(com.sinosoft.cloud.access.entity.ReqPayMentDetailsQuery reqPayMentDetailsQuery) {
        ReqPayMentDetailsQuery = reqPayMentDetailsQuery;
    }

    public com.sinosoft.cloud.access.entity.ReqPayMentApply getReqPayMentApply() {
        return ReqPayMentApply;
    }

    public void setReqPayMentApply(com.sinosoft.cloud.access.entity.ReqPayMentApply reqPayMentApply) {
        ReqPayMentApply = reqPayMentApply;
    }

    public com.sinosoft.cloud.access.entity.RespPayMentApply getRespPayMentApply() {
        return RespPayMentApply;
    }

    public void setRespPayMentApply(com.sinosoft.cloud.access.entity.RespPayMentApply respPayMentApply) {
        RespPayMentApply = respPayMentApply;
    }

    public com.sinosoft.cloud.access.entity.ReqPayAccountDetailsQuery getReqPayAccountDetailsQuery() {
        return ReqPayAccountDetailsQuery;
    }

    public void setReqPayAccountDetailsQuery(com.sinosoft.cloud.access.entity.ReqPayAccountDetailsQuery reqPayAccountDetailsQuery) {
        ReqPayAccountDetailsQuery = reqPayAccountDetailsQuery;
    }

    public com.sinosoft.cloud.access.entity.RespPayAccountDetailsQuery getRespPayAccountDetailsQuery() {
        return RespPayAccountDetailsQuery;
    }

    public void setRespPayAccountDetailsQuery(com.sinosoft.cloud.access.entity.RespPayAccountDetailsQuery respPayAccountDetailsQuery) {
        RespPayAccountDetailsQuery = respPayAccountDetailsQuery;
    }

    public com.sinosoft.cloud.access.entity.ReqPayAccountApply getReqPayAccountApply() {
        return ReqPayAccountApply;
    }

    public void setReqPayAccountApply(com.sinosoft.cloud.access.entity.ReqPayAccountApply reqPayAccountApply) {
        ReqPayAccountApply = reqPayAccountApply;
    }

    public com.sinosoft.cloud.access.entity.RespPayAccountApply getRespPayAccountApply() {
        return RespPayAccountApply;
    }

    public void setRespPayAccountApply(com.sinosoft.cloud.access.entity.RespPayAccountApply respPayAccountApply) {
        RespPayAccountApply = respPayAccountApply;
    }

    public com.sinosoft.cloud.access.entity.RespPreservePolicyDetailsQuery getRespPreservePolicyDetailsQuery() {
        return RespPreservePolicyDetailsQuery;
    }

    public void setRespPreservePolicyDetailsQuery(com.sinosoft.cloud.access.entity.RespPreservePolicyDetailsQuery respPreservePolicyDetailsQuery) {
        RespPreservePolicyDetailsQuery = respPreservePolicyDetailsQuery;
    }

    public com.sinosoft.cloud.access.entity.ReqPreservePolicyDetailsQuery getReqPreservePolicyDetailsQuery() {
        return ReqPreservePolicyDetailsQuery;
    }

    public void setReqPreservePolicyDetailsQuery(com.sinosoft.cloud.access.entity.ReqPreservePolicyDetailsQuery reqPreservePolicyDetailsQuery) {
        ReqPreservePolicyDetailsQuery = reqPreservePolicyDetailsQuery;
    }

    public com.sinosoft.cloud.access.entity.RespPreserveApply getRespPreserveApply() {
        return RespPreserveApply;
    }

    public void setRespPreserveApply(com.sinosoft.cloud.access.entity.RespPreserveApply respPreserveApply) {
        RespPreserveApply = respPreserveApply;
    }

    public com.sinosoft.cloud.access.entity.ReqPreserveApply getReqPreserveApply() {
        return ReqPreserveApply;
    }

    public void setReqPreserveApply(com.sinosoft.cloud.access.entity.ReqPreserveApply reqPreserveApply) {
        ReqPreserveApply = reqPreserveApply;
    }

    public ReqPremSelectListQuery getReqPremSelectListQuery() {
        return ReqPremSelectListQuery;
    }

    public void setReqPremSelectListQuery(ReqPremSelectListQuery reqPremSelectListQuery) {
        ReqPremSelectListQuery = reqPremSelectListQuery;
    }

    public RespPremSelectListQuery getRespPremSelectListQuery() {
        return RespPremSelectListQuery;
    }

    public void setRespPremSelectListQuery(RespPremSelectListQuery respPremSelectListQuery) {
        RespPremSelectListQuery = respPremSelectListQuery;
    }

    public RespPremSelectApply getRespPremSelectApply() {
        return RespPremSelectApply;
    }

    public void setRespPremSelectApply(RespPremSelectApply respPremSelectApply) {
        RespPremSelectApply = respPremSelectApply;
    }

    public ReqPremSelectApply getReqPremSelectApply() {
        return ReqPremSelectApply;
    }

    public void setReqPremSelectApply(ReqPremSelectApply reqPremSelectApply) {
        ReqPremSelectApply = reqPremSelectApply;
    }

    public ReqPolicyReportApply getReqPolicyReportApply() {
        return ReqPolicyReportApply;
    }

    public void setReqPolicyReportApply(ReqPolicyReportApply reqPolicyReportApply) {
        ReqPolicyReportApply = reqPolicyReportApply;
    }

    public RespPolicyReportApply getRespPolicyReportApply() {
        return RespPolicyReportApply;
    }

    public void setRespPolicyReportApply(RespPolicyReportApply respPolicyReportApply) {
        RespPolicyReportApply = respPolicyReportApply;
    }

    public ReqPolicyReportQuery getReqPolicyReportQuery() {
        return ReqPolicyReportQuery;
    }

    public void setReqPolicyReportQuery(ReqPolicyReportQuery reqPolicyReportQuery) {
        ReqPolicyReportQuery = reqPolicyReportQuery;
    }

    public RespPolicyReportQuery getRespPolicyReportQuery() {
        return RespPolicyReportQuery;
    }

    public void setRespPolicyReportQuery(RespPolicyReportQuery respPolicyReportQuery) {
        RespPolicyReportQuery = respPolicyReportQuery;
    }

    public ReqReturnSignSure getReqReturnSignSure() {
        return ReqReturnSignSure;
    }

    public void setReqReturnSignSure(ReqReturnSignSure reqReturnSignSure) {
        ReqReturnSignSure = reqReturnSignSure;
    }

    public RespPolicySupplyQuery getRespPolicySupplyQuery() {
        return RespPolicySupplyQuery;
    }

    public void setRespPolicySupplyQuery(RespPolicySupplyQuery respPolicySupplyQuery) {
        RespPolicySupplyQuery = respPolicySupplyQuery;
    }

    public ReqPolicySupplyQuery getReqPolicySupplyQuery() {
        return ReqPolicySupplyQuery;
    }

    public void setReqPolicySupplyQuery(ReqPolicySupplyQuery reqPolicySupplyQuery) {
        ReqPolicySupplyQuery = reqPolicySupplyQuery;
    }

    public ReqPremDictionaryClearListQuery getReqPremDictionaryClearListQuery() {
        return ReqPremDictionaryClearListQuery;
    }

    public void setReqPremDictionaryClearListQuery(ReqPremDictionaryClearListQuery reqPremDictionaryClearListQuery) {
        ReqPremDictionaryClearListQuery = reqPremDictionaryClearListQuery;
    }

    public RespPremDictionaryClearListQuery getRespPremDictionaryClearListQuery() {
        return RespPremDictionaryClearListQuery;
    }

    public void setRespPremDictionaryClearListQuery(RespPremDictionaryClearListQuery respPremDictionaryClearListQuery) {
        RespPremDictionaryClearListQuery = respPremDictionaryClearListQuery;
    }

    public ReqBQProgressQuery getReqBQProgressQuery() {
        return ReqBQProgressQuery;
    }

    public void setReqBQProgressQuery(ReqBQProgressQuery reqBQProgressQuery) {
        ReqBQProgressQuery = reqBQProgressQuery;
    }

    public ReqPolicyLoanClearQuery getReqPolicyLoanClearQuery() {
        return ReqPolicyLoanClearQuery;
    }

    public void setReqPolicyLoanClearQuery(ReqPolicyLoanClearQuery reqPolicyLoanClearQuery) {
        ReqPolicyLoanClearQuery = reqPolicyLoanClearQuery;
    }

    public RespPolicyLoanClearQuery getRespPolicyLoanClearQuery() {
        return RespPolicyLoanClearQuery;
    }

    public void setRespPolicyLoanClearQuery(RespPolicyLoanClearQuery respPolicyLoanClearQuery) {
        RespPolicyLoanClearQuery = respPolicyLoanClearQuery;
    }

    public ReqPolicySupplyApply getReqPolicySupplyApply() {
        return ReqPolicySupplyApply;
    }

    public void setReqPolicySupplyApply(ReqPolicySupplyApply reqPolicySupplyApply) {
        ReqPolicySupplyApply = reqPolicySupplyApply;
    }

    public RespPolicySupplyApply getRespPolicySupplyApply() {
        return RespPolicySupplyApply;
    }

    public void setRespPolicySupplyApply(RespPolicySupplyApply respPolicySupplyApply) {
        RespPolicySupplyApply = respPolicySupplyApply;
    }

    public ReqPolicyLoanClearDetailsQuery getReqPolicyLoanClearDetailsQuery() {
        return ReqPolicyLoanClearDetailsQuery;
    }

    public void setReqPolicyLoanClearDetailsQuery(ReqPolicyLoanClearDetailsQuery reqPolicyLoanClearDetailsQuery) {
        ReqPolicyLoanClearDetailsQuery = reqPolicyLoanClearDetailsQuery;
    }

    public RespPolicyLoanClearDetailsQuery getRespPolicyLoanClearDetailsQuery() {
        return RespPolicyLoanClearDetailsQuery;
    }

    public void setRespPolicyLoanClearDetailsQuery(RespPolicyLoanClearDetailsQuery respPolicyLoanClearDetailsQuery) {
        RespPolicyLoanClearDetailsQuery = respPolicyLoanClearDetailsQuery;
    }

    public ReqPolicyLoanClearApply getReqPolicyLoanClearApply() {
        return ReqPolicyLoanClearApply;
    }

    public void setReqPolicyLoanClearApply(ReqPolicyLoanClearApply reqPolicyLoanClearApply) {
        ReqPolicyLoanClearApply = reqPolicyLoanClearApply;
    }

    public RespPolicyLoanClearApply getRespPolicyLoanClearApply() {
        return RespPolicyLoanClearApply;
    }

    public void setRespPolicyLoanClearApply(RespPolicyLoanClearApply respPolicyLoanClearApply) {
        RespPolicyLoanClearApply = respPolicyLoanClearApply;
    }

    public ReqPremDictionaryClearApply getReqPremDictionaryClearApply() {
        return ReqPremDictionaryClearApply;
    }

    public void setReqPremDictionaryClearApply(ReqPremDictionaryClearApply reqPremDictionaryClearApply) {
        ReqPremDictionaryClearApply = reqPremDictionaryClearApply;
    }

    public RespPremDictionaryClearApply getRespPremDictionaryClearApply() {
        return RespPremDictionaryClearApply;
    }

    public void setRespPremDictionaryClearApply(RespPremDictionaryClearApply respPremDictionaryClearApply) {
        RespPremDictionaryClearApply = respPremDictionaryClearApply;
    }

    public RespEleCtronPolicy getRespEleCtronPolicy() {
        return RespEleCtronPolicy;
    }

    public void setRespEleCtronPolicy(RespEleCtronPolicy respEleCtronPolicy) {
        RespEleCtronPolicy = respEleCtronPolicy;
    }

    public RespAcceptanceCancel getRespAcceptanceCancel() {
        return RespAcceptanceCancel;
    }

    public void setRespAcceptanceCancel(RespAcceptanceCancel respAcceptanceCancel) {
        RespAcceptanceCancel = respAcceptanceCancel;
    }

    public RespPolicyUp getRespPolicyUp() {
        return RespPolicyUp;
    }

    public void setRespPolicyUp(RespPolicyUp respPolicyUp) {
        RespPolicyUp = respPolicyUp;
    }

    public ReqPolicyUp getReqPolicyUp() {
        return ReqPolicyUp;
    }

    public void setReqPolicyUp(ReqPolicyUp reqPolicyUp) {
        ReqPolicyUp = reqPolicyUp;
    }

    public ReqPolicyChargeInfoQuery getReqPolicyChargeInfoQuery() {
        return ReqPolicyChargeInfoQuery;
    }

    public void setReqPolicyChargeInfoQuery(ReqPolicyChargeInfoQuery reqPolicyChargeInfoQuery) {
        ReqPolicyChargeInfoQuery = reqPolicyChargeInfoQuery;
    }

    public RespPolicyChargeInfoQuery getRespPolicyChargeInfoQuery() {
        return RespPolicyChargeInfoQuery;
    }

    public void setRespPolicyChargeInfoQuery(RespPolicyChargeInfoQuery respPolicyChargeInfoQuery) {
        RespPolicyChargeInfoQuery = respPolicyChargeInfoQuery;
    }

    public ReqPolicyHang getReqPolicyHang() {
        return ReqPolicyHang;
    }

    public void setReqPolicyHang(ReqPolicyHang reqPolicyHang) {
        ReqPolicyHang = reqPolicyHang;
    }

    public RespPolicyHang getRespPolicyHang() {
        return RespPolicyHang;
    }

    public void setRespPolicyHang(RespPolicyHang respPolicyHang) {
        RespPolicyHang = respPolicyHang;
    }

    public ReqRenewalFee getReqRenewalFee() {
        return ReqRenewalFee;
    }

    public void setReqRenewalFee(ReqRenewalFee reqRenewalFee) {
        ReqRenewalFee = reqRenewalFee;
    }

    public RespRenewalFee getRespRenewalFee() {
        return RespRenewalFee;
    }

    public void setRespRenewalFee(RespRenewalFee respRenewalFee) {
        RespRenewalFee = respRenewalFee;
    }

    public RespLifeApply getRespLifeApply() {
        return RespLifeApply;
    }

    public void setRespLifeApply(RespLifeApply respLifeApply) {
        RespLifeApply = respLifeApply;
    }

    public ReqLifeApply getReqLifeApply() {
        return ReqLifeApply;
    }

    public void setReqLifeApply(ReqLifeApply reqLifeApply) {
        ReqLifeApply = reqLifeApply;
    }

    public ReqAcceptanceCancel getReqAcceptanceCancel() {
        return ReqAcceptanceCancel;
    }

    public void setReqAcceptanceCancel(ReqAcceptanceCancel reqAcceptanceCancel) {
        ReqAcceptanceCancel = reqAcceptanceCancel;
    }

    public RespReturnSignQuery getRespReturnSignQuery() {
        return RespReturnSignQuery;
    }

    public void setRespReturnSignQuery(RespReturnSignQuery respReturnSignQuery) {
        RespReturnSignQuery = respReturnSignQuery;
    }

    public ReqReturnSignQuery getReqReturnSignQuery() {
        return ReqReturnSignQuery;
    }

    public void setReqReturnSignQuery(ReqReturnSignQuery reqReturnSignQuery) {
        ReqReturnSignQuery = reqReturnSignQuery;
    }

    public ReqNotRealNewContnoApply getReqNotRealNewContnoApply() {
        return ReqNotRealNewContnoApply;
    }

    public void setReqNotRealNewContnoApply(ReqNotRealNewContnoApply reqNotRealNewContnoApply) {
        ReqNotRealNewContnoApply = reqNotRealNewContnoApply;
    }

    public RespNotRealNewContnoApply getRespNotRealNewContnoApply() {
        return RespNotRealNewContnoApply;
    }

    public void setRespNotRealNewContnoApply(RespNotRealNewContnoApply respNotRealNewContnoApply) {
        RespNotRealNewContnoApply = respNotRealNewContnoApply;
    }

    public ReqEleCtronPolicy getReqEleCtronPolicy() {
        return ReqEleCtronPolicy;
    }

    public void setReqEleCtronPolicy(ReqEleCtronPolicy reqEleCtronPolicy) {
        ReqEleCtronPolicy = reqEleCtronPolicy;
    }

    public RespWhilteQuery getRespWhilteQuery() {
        return RespWhilteQuery;
    }

    public void setRespWhilteQuery(RespWhilteQuery respWhilteQuery) {
        RespWhilteQuery = respWhilteQuery;
    }

    public ReqWhilteQuery getReqWhilteQuery() {
        return ReqWhilteQuery;
    }

    public void setReqWhilteQuery(ReqWhilteQuery reqWhilteQuery) {
        ReqWhilteQuery = reqWhilteQuery;
    }

    public ReqWhilteApply getReqWhilteApply() {
        return ReqWhilteApply;
    }

    public void setReqWhilteApply(ReqWhilteApply reqWhilteApply) {
        ReqWhilteApply = reqWhilteApply;
    }

    public RespWhilteApply getRespWhilteApply() {
        return RespWhilteApply;
    }

    public void setRespWhilteApply(RespWhilteApply respWhilteApply) {
        RespWhilteApply = respWhilteApply;
    }

    public RespPolicySupplyDetailsQuery getRespPolicySupplyDetailsQuery() {
        return RespPolicySupplyDetailsQuery;
    }

    public void setRespPolicySupplyDetailsQuery(RespPolicySupplyDetailsQuery respPolicySupplyDetailsQuery) {
        RespPolicySupplyDetailsQuery = respPolicySupplyDetailsQuery;
    }

    public ReqPolicySupplyDetailsQuery getReqPolicySupplyDetailsQuery() {
        return ReqPolicySupplyDetailsQuery;
    }

    public void setReqPolicySupplyDetailsQuery(ReqPolicySupplyDetailsQuery reqPolicySupplyDetailsQuery) {
        ReqPolicySupplyDetailsQuery = reqPolicySupplyDetailsQuery;
    }

    public RespUAccountDetailsQuery getRespUAccountDetailsQuery() {
        return RespUAccountDetailsQuery;
    }

    public void setRespUAccountDetailsQuery(RespUAccountDetailsQuery respUAccountDetailsQuery) {
        RespUAccountDetailsQuery = respUAccountDetailsQuery;
    }

    public ReqUAccountDetailsQuery getReqUAccountDetailsQuery() {
        return ReqUAccountDetailsQuery;
    }

    public void setReqUAccountDetailsQuery(ReqUAccountDetailsQuery reqUAccountDetailsQuery) {
        ReqUAccountDetailsQuery = reqUAccountDetailsQuery;
    }

    public ReqLifeQuery getReqLifeQuery() {
        return ReqLifeQuery;
    }

    public void setReqLifeQuery(ReqLifeQuery reqLifeQuery) {
        ReqLifeQuery = reqLifeQuery;
    }

    public RespLifeQuery getRespLifeQuery() {
        return RespLifeQuery;
    }

    public void setRespLifeQuery(RespLifeQuery respLifeQuery) {
        RespLifeQuery = respLifeQuery;
    }

    public ReqBnfIdValiDateDetailsQuery getReqBnfIdValiDateDetailsQuery() {
        return ReqBnfIdValiDateDetailsQuery;
    }

    public void setReqBnfIdValiDateDetailsQuery(ReqBnfIdValiDateDetailsQuery reqBnfIdValiDateDetailsQuery) {
        ReqBnfIdValiDateDetailsQuery = reqBnfIdValiDateDetailsQuery;
    }

    public RespBnfIdValiDateDetailsQuery getRespBnfIdValiDateDetailsQuery() {
        return RespBnfIdValiDateDetailsQuery;
    }

    public void setRespBnfIdValiDateDetailsQuery(RespBnfIdValiDateDetailsQuery respBnfIdValiDateDetailsQuery) {
        RespBnfIdValiDateDetailsQuery = respBnfIdValiDateDetailsQuery;
    }

    public RespInsuredIdValiDateDetailsQuery getRespInsuredIdValiDateDetailsQuery() {
        return RespInsuredIdValiDateDetailsQuery;
    }

    public void setRespInsuredIdValiDateDetailsQuery(RespInsuredIdValiDateDetailsQuery respInsuredIdValiDateDetailsQuery) {
        RespInsuredIdValiDateDetailsQuery = respInsuredIdValiDateDetailsQuery;
    }

    public ReqInsuredIdValiDateDetailsQuery getReqInsuredIdValiDateDetailsQuery() {
        return ReqInsuredIdValiDateDetailsQuery;
    }

    public void setReqInsuredIdValiDateDetailsQuery(ReqInsuredIdValiDateDetailsQuery reqInsuredIdValiDateDetailsQuery) {
        ReqInsuredIdValiDateDetailsQuery = reqInsuredIdValiDateDetailsQuery;
    }

    public ReqRenewalPayChangeDetailsQuery getReqRenewalPayChangeDetailsQuery() {
        return ReqRenewalPayChangeDetailsQuery;
    }

    public void setReqRenewalPayChangeDetailsQuery(ReqRenewalPayChangeDetailsQuery reqRenewalPayChangeDetailsQuery) {
        ReqRenewalPayChangeDetailsQuery = reqRenewalPayChangeDetailsQuery;
    }

    public RespRenewalPayChangeDetailsQuery getRespRenewalPayChangeDetailsQuery() {
        return RespRenewalPayChangeDetailsQuery;
    }

    public void setRespRenewalPayChangeDetailsQuery(RespRenewalPayChangeDetailsQuery respRenewalPayChangeDetailsQuery) {
        RespRenewalPayChangeDetailsQuery = respRenewalPayChangeDetailsQuery;
    }

//
//    public CaluParamInfo getCaluParamInfo() {
//        return CaluParamInfo;
//    }

//    public void setCaluParamInfo(CaluParamInfo caluParamInfo) {
//        CaluParamInfo = caluParamInfo;
//    }

    public String getOldContPrtNo() {
        return oldContPrtNo;
    }

    public void setOldContPrtNo(String oldContPrtNo) {
        this.oldContPrtNo = oldContPrtNo;
    }

    public String getOldTranno() {
        return oldTranno;
    }

    public void setOldTranno(String oldTranno) {
        this.oldTranno = oldTranno;
    }

    public void setContType(String contType) {
        this.contType = contType;
    }

    public String getContType() {
        return contType;
    }

    public String getDateFlag() {
        return dateFlag;
    }

    public void setDateFlag(String dateFlag) {
        this.dateFlag = dateFlag;
    }

    public void setOccupationName(String occupationName) {
        this.occupationName = occupationName;
    }

    public String getOccupationName() {
        return occupationName;
    }

    public String getOperate() {
        return operate;
    }

    public void setOperate(String operate) {
        this.operate = operate;
    }

    public void setLCApi(LCApi LCApi) {
        this.LCApi = LCApi;
    }

    public String getRiskCodeWr() {
        return riskCodeWr;
    }

    public void setRiskCodeWr(String riskCodeWr) {
        this.riskCodeWr = riskCodeWr;
    }

    public Appnt getInsured() {
        return insured;
    }

    public void setInsured(Appnt insured) {
        this.insured = insured;
    }

    public Risk getRisk() {
        return risk;
    }

    public void setRisk(Risk risk) {
        this.risk = risk;
    }

    public Risks getRisks() {
        return risks;
    }

    public void setRisks(Risks risks) {
        this.risks = risks;
    }

    public Insureds getInsureds() {
        return insureds;
    }

    public void setInsureds(Insureds insureds) {
        this.insureds = insureds;
    }

    public PolicyBasicInfo getPolicyBasicInfo() {
        return policyBasicInfo;
    }

    public void setPolicyBasicInfo(PolicyBasicInfo policyBasicInfo) {
        this.policyBasicInfo = policyBasicInfo;
    }

    public Appnt getAppnt() {
        return appnt;
    }

    public void setAppnt(Appnt appnt) {
        this.appnt = appnt;
    }

    public Bnfs getBnfs() {
        return bnfs;
    }

    public void setBnfs(Bnfs bnfs) {
        this.bnfs = bnfs;
    }

    public Global getGlobal() {
        return Global;
    }

    public void setGlobal(Global global) {
        Global = global;
    }

    public CashValues getCashValues() {
        return CashValues;
    }

    public void setCashValues(CashValues cashValues) {
        CashValues = cashValues;
    }

    public ReducedVolumes getReducedVolumes() {
        return ReducedVolumes;
    }

    public void setReducedVolumes(ReducedVolumes reducedVolumes) {
        ReducedVolumes = reducedVolumes;
    }

    public void setLCPolicyInfo(LCPolicyInfo LCPolicyInfo) {
        this.LCPolicyInfo = LCPolicyInfo;
    }

    public LCApi getLCApi() {
        return LCApi;
    }

    public LCPolicyInfo getLCPolicyInfo() {
        return LCPolicyInfo;
    }

    public LCCont getLCCont() {
        return LCCont;
    }

    public void setLCCont(LCCont LCCont) {
        this.LCCont = LCCont;
    }

    public LCPols getLCPols() {
        return LCPols;
    }

    public void setLCPols(LCPols LCPols) {
        this.LCPols = LCPols;
    }

    public LCAppnt getLCAppnt() {
        return LCAppnt;
    }

    public void setLCAppnt(LCAppnt LCAppnt) {
        this.LCAppnt = LCAppnt;
    }

    public LCInsureds getLCInsureds() {
        return LCInsureds;
    }

    public void setLCInsureds(LCInsureds LCInsureds) {
        this.LCInsureds = LCInsureds;
    }

    public LCBnfs getLCBnfs() {
        return LCBnfs;
    }

    public void setLCBnfs(LCBnfs LCBnfs) {
        this.LCBnfs = LCBnfs;
    }

    public LAAgent getLAAgent() {
        return LAAgent;
    }

    public void setLAAgent(LAAgent LAAgent) {
        this.LAAgent = LAAgent;
    }

    public LAAgent1 getLAAgent1() {
        return LAAgent1;
    }

    public void setLAAgent1(LAAgent1 LAAgent1) {
        this.LAAgent1 = LAAgent1;
    }

    public LACom getLACom() {
        return LACom;
    }

    public void setLACom(LACom LACom) {
        this.LACom = LACom;
    }

    public LAQualification getLAQualification() {
        return LAQualification;
    }

    public void setLAQualification(LAQualification LAQualification) {
        this.LAQualification = LAQualification;
    }

    public LCSpecs getLCSpecs() {
        return LCSpecs;
    }

    public void setLCSpecs(LCSpecs LCSpecs) {
        this.LCSpecs = LCSpecs;
    }

    public LDCom getLDCom() {
        return LDCom;
    }

    public void setLDCom(LDCom LDCom) {
        this.LDCom = LDCom;
    }

    public LMRiskApps getLMRiskApps() {
        return LMRiskApps;
    }

    public void setLMRiskApps(LMRiskApps LMRiskApps) {
        this.LMRiskApps = LMRiskApps;
    }

    public LCAccount getLCAccount() {
        return LCAccount;
    }

    public void setLCAccount(LCAccount LCAccount) {
        this.LCAccount = LCAccount;
    }

    public LCAddresses getLCAddresses() {
        return LCAddresses;
    }

    public void setLCAddresses(LCAddresses LCAddresses) {
        this.LCAddresses = LCAddresses;
    }

    public LCCustomerImparts getLCCustomerImparts() {
        return LCCustomerImparts;
    }

    public void setLCCustomerImparts(LCCustomerImparts LCCustomerImparts) {
        this.LCCustomerImparts = LCCustomerImparts;
    }

    public LKTransInfo getLKTransInfo() {
        return LKTransInfo;
    }

    public void setLKTransInfo(LKTransInfo LKTransInfo) {
        this.LKTransInfo = LKTransInfo;
    }

    public LKTransInfoes getLKTransInfoes() {
        return LKTransInfoes;
    }

    public void setLKTransInfoes(LKTransInfoes LKTransInfoes) {
        this.LKTransInfoes = LKTransInfoes;
    }

    public LKNRTBizTrans getLKNRTBizTrans() {
        return LKNRTBizTrans;
    }

    public void setLKNRTBizTrans(LKNRTBizTrans LKNRTBizTrans) {
        this.LKNRTBizTrans = LKNRTBizTrans;
    }

    public LKTransStatus getLKTransStatus() {
        return LKTransStatus;
    }

    public void setLKTransStatus(LKTransStatus LKTransStatus) {
        this.LKTransStatus = LKTransStatus;
    }

    public LKTransStatuses getLKTransStatuses() {
        return LKTransStatuses;
    }

    public void setLKTransStatuses(LKTransStatuses LKTransStatuses) {
        this.LKTransStatuses = LKTransStatuses;
    }

    public Reserves getReserves() {
        return Reserves;
    }

    public void setReserves(Reserves reserves) {
        Reserves = reserves;
    }

    public ResponseObj getResponseObj() {
        return ResponseObj;
    }

    public void setResponseObj(ResponseObj responseObj) {
        ResponseObj = responseObj;
    }

    public LDPerson getLDPerson() {
        return LDPerson;
    }

    public void setLDPerson(LDPerson LDPerson) {
        this.LDPerson = LDPerson;
    }

    public LCAppntLinkManInfo getLCAppntLinkManInfo() {
        return LCAppntLinkManInfo;
    }

    public void setLCAppntLinkManInfo(LCAppntLinkManInfo LCAppntLinkManInfo) {
        this.LCAppntLinkManInfo = LCAppntLinkManInfo;
    }

    public LCDutys getLCDutys() {
        return LCDutys;
    }

    public void setLCDutys(LCDutys LCDutys) {
        this.LCDutys = LCDutys;
    }



    public LCGets getLCGets() {
        return LCGets;
    }

    public void setLCGets(LCGets LCGets) {
        this.LCGets = LCGets;
    }

    public LCPrems getLCPrems() {
        return LCPrems;
    }

    public void setLCPrems(LCPrems LCPrems) {
        this.LCPrems = LCPrems;
    }

    public MIPImparts getMIPImparts() {
        return MIPImparts;
    }

    public void setMIPImparts(MIPImparts MIPImparts) {
        this.MIPImparts = MIPImparts;
    }

    public LDPersons getLDPersons() {
        return LDPersons;
    }

    public void setLDPersons(LDPersons LDPersons) {
        this.LDPersons = LDPersons;
    }

    public LCCustomerImpartDetails getLCCustomerImpartDetails() {
        return LCCustomerImpartDetails;
    }

    public void setLCCustomerImpartDetails(LCCustomerImpartDetails LCCustomerImpartDetails) {
        this.LCCustomerImpartDetails = LCCustomerImpartDetails;
    }

    public LCCustomerImpartParamses getLCCustomerImpartParamses() {
        return LCCustomerImpartParamses;
    }

    public void setLCCustomerImpartParamses(LCCustomerImpartParamses LCCustomerImpartParamses) {
        this.LCCustomerImpartParamses = LCCustomerImpartParamses;
    }

    public String getOtherCompanyDieAmnt() {
        return otherCompanyDieAmnt;
    }

    public void setOtherCompanyDieAmnt(String otherCompanyDieAmnt) {
        this.otherCompanyDieAmnt = otherCompanyDieAmnt;
    }

    public String getAppNo() {
        return appNo;
    }

    public void setAppNo(String appNo) {
        this.appNo = appNo;
    }

    public String getHealthNotice() {
        return healthNotice;
    }

    public void setHealthNotice(String healthNotice) {
        this.healthNotice = healthNotice;
    }

    public String getContPrtNo() {
        return contPrtNo;
    }

    public void setContPrtNo(String contPrtNo) {
        this.contPrtNo = contPrtNo;
    }

    public String getContNo() {
        return contNo;
    }

    public void setContNo(String contNo) {
        this.contNo = contNo;
    }

    public String getUnChangedValue() {
        return unChangedValue;
    }

    public void setUnChangedValue(String unChangedValue) {
        this.unChangedValue = unChangedValue;
    }

    public String getTransferFlag() {
        return transferFlag;
    }

    public void setTransferFlag(String transferFlag) {
        this.transferFlag = transferFlag;
    }

    public String getReserve() {
        return reserve;
    }

    public void setReserve(String reserve) {
        this.reserve = reserve;
    }

    public String getInsuTime() {
        return insuTime;
    }

    public void setInsuTime(String insuTime) {
        this.insuTime = insuTime;
    }

    public String getPayPrem() {
        return payPrem;
    }

    public void setPayPrem(String payPrem) {
        this.payPrem = payPrem;
    }

    public String getAppntMSalary() {
        return appntMSalary;
    }

    public void setAppntMSalary(String appntMSalary) {
        this.appntMSalary = appntMSalary;
    }

    public String getInsuSerial() {
        return insuSerial;
    }

    public void setInsuSerial(String insuSerial) {
        this.insuSerial = insuSerial;
    }

    public String getFileTimeStamp() {
        return fileTimeStamp;
    }

    public void setFileTimeStamp(String fileTimeStamp) {
        this.fileTimeStamp = fileTimeStamp;
    }

    public String getPriKey() {
        return priKey;
    }

    public void setPriKey(String priKey) {
        this.priKey = priKey;
    }

    public String getOrgKey() {
        return orgKey;
    }

    public void setOrgKey(String orgKey) {
        this.orgKey = orgKey;
    }

    public LKTransTrackses getLKTransTrackses() {
        return LKTransTrackses;
    }

    public void setLKTransTrackses(LKTransTrackses LKTransTrackses) {
        this.LKTransTrackses = LKTransTrackses;
    }

    public LKTransTracks getLKTransTracks() {
        return LKTransTracks;
    }

    public void setLKTransTracks(LKTransTracks LKTransTracks) {
        this.LKTransTracks = LKTransTracks;
    }

    public LJTempFees getLJTempFees() {
        return LJTempFees;
    }

    public void setLJTempFees(LJTempFees LJTempFees) {
        this.LJTempFees = LJTempFees;
    }

    public LKTransTracks getLJTempFee() {
        return LJTempFee;
    }

    public void setLJTempFee(LKTransTracks LJTempFee) {
        this.LJTempFee = LJTempFee;
    }

    public CaluParamInfos getCaluParamInfos() {
        return CaluParamInfos;
    }

    public void setCaluParamInfos(CaluParamInfos caluParamInfos) {
        CaluParamInfos = caluParamInfos;
    }

    public ReqHead getReqHead() {
        return ReqHead;
    }

    public void setReqHead(ReqHead reqHead) {
        ReqHead = reqHead;
    }

    public RespHead getRespHead() {
        return RespHead;
    }

    public void setRespHead(RespHead respHead) {
        RespHead = respHead;
    }

    public ReqWT getReqWT() {
        return ReqWT;
    }

    public void setReqWT(ReqWT reqWT) {
        ReqWT = reqWT;
    }

    public RespWT getRespWT() {
        return RespWT;
    }

    public void setRespWT(RespWT respWT) {
        RespWT = respWT;
    }

    public PolicyQueryInfos getPolicyQueryInfos() {
        return PolicyQueryInfos;
    }

    public void setPolicyQueryInfos(PolicyQueryInfos policyQueryInfos) {
        PolicyQueryInfos = policyQueryInfos;
    }

    public ReqRenewalChangeListQuery getReqRenewalChangeListQuery() {
        return ReqRenewalChangeListQuery;
    }

    public void setReqRenewalChangeListQuery(ReqRenewalChangeListQuery reqRenewalChangeListQuery) {
        ReqRenewalChangeListQuery = reqRenewalChangeListQuery;
    }

    public RespBQProgressQuery getRespBQProgressQuery() {
        return RespBQProgressQuery;
    }

    public void setRespBQProgressQuery(RespBQProgressQuery respBQProgressQuery) {
        RespBQProgressQuery = respBQProgressQuery;
    }

    public ReqRenewalFeeQuery getReqRenewalFeeQuery() {
        return ReqRenewalFeeQuery;
    }

    public void setReqRenewalFeeQuery(ReqRenewalFeeQuery reqRenewalFeeQuery) {
        ReqRenewalFeeQuery = reqRenewalFeeQuery;
    }

    public RespRenewalFeeQuery getRespRenewalFeeQuery() {
        return RespRenewalFeeQuery;
    }

    public void setRespRenewalFeeQuery(RespRenewalFeeQuery respRenewalFeeQuery) {
        RespRenewalFeeQuery = respRenewalFeeQuery;
    }

    public ReqClaimReport getReqClaimReport() {
        return ReqClaimReport;
    }

    public void setReqClaimReport(ReqClaimReport reqClaimReport) {
        ReqClaimReport = reqClaimReport;
    }

    public RespClaimReport getRespClaimReport() {
        return RespClaimReport;
    }

    public void setRespClaimReport(RespClaimReport respClaimReport) {
        RespClaimReport = respClaimReport;
    }

    public RespSurrenderQuery getRespSurrenderQuery() {
        return RespSurrenderQuery;
    }

    public void setRespSurrenderQuery(RespSurrenderQuery respSurrenderQuery) {
        RespSurrenderQuery = respSurrenderQuery;
    }

    public ReqSurrenderQuery getReqSurrenderQuery() {
        return ReqSurrenderQuery;
    }

    public void setReqSurrenderQuery(ReqSurrenderQuery reqSurrenderQuery) {
        ReqSurrenderQuery = reqSurrenderQuery;
    }

    public ReqPreservationStateQuery getReqPreservationStateQuery() {
        return ReqPreservationStateQuery;
    }

    public void setReqPreservationStateQuery(ReqPreservationStateQuery reqPreservationStateQuery) {
        ReqPreservationStateQuery = reqPreservationStateQuery;
    }

    public RespPreservationStateQuery getRespPreservationStateQuery() {
        return RespPreservationStateQuery;
    }

    public void setRespPreservationStateQuery(RespPreservationStateQuery respPreservationStateQuery) {
        RespPreservationStateQuery = respPreservationStateQuery;
    }

    public ReqSpecialConventionChangeQuery getReqSpecialConventionChangeQuery() {
        return ReqSpecialConventionChangeQuery;
    }

    public void setReqSpecialConventionChangeQuery(ReqSpecialConventionChangeQuery reqSpecialConventionChangeQuery) {
        ReqSpecialConventionChangeQuery = reqSpecialConventionChangeQuery;
    }

    public RespSpecialConventionChangeQuery getRespSpecialConventionChangeQuery() {
        return RespSpecialConventionChangeQuery;
    }

    public void setRespSpecialConventionChangeQuery(RespSpecialConventionChangeQuery respSpecialConventionChangeQuery) {
        RespSpecialConventionChangeQuery = respSpecialConventionChangeQuery;
    }

    public RespNoticeAgainSend getRespNoticeAgainSend() {
        return RespNoticeAgainSend;
    }

    public void setRespNoticeAgainSend(RespNoticeAgainSend respNoticeAgainSend) {
        RespNoticeAgainSend = respNoticeAgainSend;
    }

    public ReqNoticeAgainSend getReqNoticeAgainSend() {
        return ReqNoticeAgainSend;
    }

    public void setReqNoticeAgainSend(ReqNoticeAgainSend reqNoticeAgainSend) {
        ReqNoticeAgainSend = reqNoticeAgainSend;
    }

    public RespAppntIDCardChangeCheck getRespAppntIDCardChangeCheck() {
        return RespAppntIDCardChangeCheck;
    }

    public void setRespAppntIDCardChangeCheck(RespAppntIDCardChangeCheck respAppntIDCardChangeCheck) {
        RespAppntIDCardChangeCheck = respAppntIDCardChangeCheck;
    }

    public ReqAppntIDCardChangeCheck getReqAppntIDCardChangeCheck() {
        return ReqAppntIDCardChangeCheck;
    }

    public void setReqAppntIDCardChangeCheck(ReqAppntIDCardChangeCheck reqAppntIDCardChangeCheck) {
        ReqAppntIDCardChangeCheck = reqAppntIDCardChangeCheck;
    }

    public ReqAppntIDCardChangeQuery getReqAppntIDCardChangeQuery() {
        return ReqAppntIDCardChangeQuery;
    }

    public void setReqAppntIDCardChangeQuery(ReqAppntIDCardChangeQuery reqAppntIDCardChangeQuery) {
        ReqAppntIDCardChangeQuery = reqAppntIDCardChangeQuery;
    }

    public RespAppntIDCardChangeQuery getRespAppntIDCardChangeQuery() {
        return RespAppntIDCardChangeQuery;
    }

    public void setRespAppntIDCardChangeQuery(RespAppntIDCardChangeQuery respAppntIDCardChangeQuery) {
        RespAppntIDCardChangeQuery = respAppntIDCardChangeQuery;
    }

    public ReqAccountHalfDetailsQuery getReqAccountHalfDetailsQuery() {
        return ReqAccountHalfDetailsQuery;
    }

    public void setReqAccountHalfDetailsQuery(ReqAccountHalfDetailsQuery reqAccountHalfDetailsQuery) {
        ReqAccountHalfDetailsQuery = reqAccountHalfDetailsQuery;
    }

    public RespAccountHalfDetailsQuery getRespAccountHalfDetailsQuery() {
        return RespAccountHalfDetailsQuery;
    }

    public void setRespAccountHalfDetailsQuery(RespAccountHalfDetailsQuery respAccountHalfDetailsQuery) {
        RespAccountHalfDetailsQuery = respAccountHalfDetailsQuery;
    }

    public ReqBonusUserDetails getReqBonusUserDetails() {
        return ReqBonusUserDetails;
    }

    public void setReqBonusUserDetails(ReqBonusUserDetails reqBonusUserDetails) {
        ReqBonusUserDetails = reqBonusUserDetails;
    }

    public RespBonusUserDetails getRespBonusUserDetails() {
        return RespBonusUserDetails;
    }

    public void setRespBonusUserDetails(RespBonusUserDetails respBonusUserDetails) {
        RespBonusUserDetails = respBonusUserDetails;
    }

    public ReqBonusCommitReturn getReqBonusCommitReturn() {
        return ReqBonusCommitReturn;
    }

    public void setReqBonusCommitReturn(ReqBonusCommitReturn reqBonusCommitReturn) {
        ReqBonusCommitReturn = reqBonusCommitReturn;
    }

    public RespBonusCommitReturn getRespBonusCommitReturn() {
        return RespBonusCommitReturn;
    }

    public void setRespBonusCommitReturn(RespBonusCommitReturn respBonusCommitReturn) {
        RespBonusCommitReturn = respBonusCommitReturn;
    }

    public RespBonusGetModeDetailsQuery getRespBonusGetModeDetailsQuery() {
        return RespBonusGetModeDetailsQuery;
    }

    public void setRespBonusGetModeDetailsQuery(RespBonusGetModeDetailsQuery respBonusGetModeDetailsQuery) {
        RespBonusGetModeDetailsQuery = respBonusGetModeDetailsQuery;
    }

    public ReqBonusGetModeDetailsQuery getReqBonusGetModeDetailsQuery() {
        return ReqBonusGetModeDetailsQuery;
    }

    public void setReqBonusGetModeDetailsQuery(ReqBonusGetModeDetailsQuery reqBonusGetModeDetailsQuery) {
        ReqBonusGetModeDetailsQuery = reqBonusGetModeDetailsQuery;
    }

    public RespPremSelectQuery getRespPremSelectQuery() {
        return RespPremSelectQuery;
    }

    public void setRespPremSelectQuery(RespPremSelectQuery respPremSelectQuery) {
        RespPremSelectQuery = respPremSelectQuery;
    }

    public ReqPremSelectQuery getReqPremSelectQuery() {
        return ReqPremSelectQuery;
    }

    public void setReqPremSelectQuery(ReqPremSelectQuery reqPremSelectQuery) {
        ReqPremSelectQuery = reqPremSelectQuery;
    }

    public ReqPremDictionaryClearDetailsQuery getReqPremDictionaryClearDetailsQuery() {
        return ReqPremDictionaryClearDetailsQuery;
    }

    public void setReqPremDictionaryClearDetailsQuery(ReqPremDictionaryClearDetailsQuery reqPremDictionaryClearDetailsQuery) {
        ReqPremDictionaryClearDetailsQuery = reqPremDictionaryClearDetailsQuery;
    }

    public RespPremDictionaryClearDetailsQuery getRespPremDictionaryClearDetailsQuery() {
        return RespPremDictionaryClearDetailsQuery;
    }

    public void setRespPremDictionaryClearDetailsQuery(RespPremDictionaryClearDetailsQuery respPremDictionaryClearDetailsQuery) {
        RespPremDictionaryClearDetailsQuery = respPremDictionaryClearDetailsQuery;
    }

    public ReqContQuery getReqContQuery() {
        return ReqContQuery;
    }

    public void setReqContQuery(ReqContQuery reqContQuery) {
        ReqContQuery = reqContQuery;
    }

    public RespContQuery getRespContQuery() {
        return RespContQuery;
    }

    public void setRespContQuery(RespContQuery respContQuery) {
        RespContQuery = respContQuery;
    }

    public RespSurrenderApply getRespSurrenderApply() {
        return RespSurrenderApply;
    }

    public void setRespSurrenderApply(RespSurrenderApply respSurrenderApply) {
        RespSurrenderApply = respSurrenderApply;
    }

    public ReqSurrenderApply getReqSurrenderApply() {
        return ReqSurrenderApply;
    }

    public void setReqSurrenderApply(ReqSurrenderApply reqSurrenderApply) {
        ReqSurrenderApply = reqSurrenderApply;
    }

    public ReqAccountChange getReqAccountChange() {
        return ReqAccountChange;
    }

    public void setReqAccountChange(ReqAccountChange reqAccountChange) {
        ReqAccountChange = reqAccountChange;
    }

    public RespAccountChange getRespAccountChange() {
        return RespAccountChange;
    }

    public void setRespAccountChange(RespAccountChange respAccountChange) {
        RespAccountChange = respAccountChange;
    }

    public ReqSpecialConventionChangeApply getReqSpecialConventionChangeApply() {
        return ReqSpecialConventionChangeApply;
    }

    public void setReqSpecialConventionChangeApply(ReqSpecialConventionChangeApply reqSpecialConventionChangeApply) {
        ReqSpecialConventionChangeApply = reqSpecialConventionChangeApply;
    }

    public RespSpecialConventionChangeApply getRespSpecialConventionChangeApply() {
        return RespSpecialConventionChangeApply;
    }

    public void setRespSpecialConventionChangeApply(RespSpecialConventionChangeApply respSpecialConventionChangeApply) {
        RespSpecialConventionChangeApply = respSpecialConventionChangeApply;
    }

    public ReqPolicyLoanApply getReqPolicyLoanApply() {
        return ReqPolicyLoanApply;
    }

    public void setReqPolicyLoanApply(ReqPolicyLoanApply reqPolicyLoanApply) {
        ReqPolicyLoanApply = reqPolicyLoanApply;
    }

    public RespPolicyLoanApply getRespPolicyLoanApply() {
        return RespPolicyLoanApply;
    }

    public void setRespPolicyLoanApply(RespPolicyLoanApply respPolicyLoanApply) {
        RespPolicyLoanApply = respPolicyLoanApply;
    }

    public ReqRenewalChangeApply getReqRenewalChangeApply() {
        return ReqRenewalChangeApply;
    }

    public void setReqRenewalChangeApply(ReqRenewalChangeApply reqRenewalChangeApply) {
        ReqRenewalChangeApply = reqRenewalChangeApply;
    }

    public RespRenewalChangeApply getRespRenewalChangeApply() {
        return RespRenewalChangeApply;
    }

    public void setRespRenewalChangeApply(RespRenewalChangeApply respRenewalChangeApply) {
        RespRenewalChangeApply = respRenewalChangeApply;
    }

    public ReqInsuredIdValiDateApply getReqInsuredIdValiDateApply() {
        return ReqInsuredIdValiDateApply;
    }

    public void setReqInsuredIdValiDateApply(ReqInsuredIdValiDateApply reqInsuredIdValiDateApply) {
        ReqInsuredIdValiDateApply = reqInsuredIdValiDateApply;
    }

    public RespInsuredIdValiDateApply getRespInsuredIdValiDateApply() {
        return RespInsuredIdValiDateApply;
    }

    public void setRespInsuredIdValiDateApply(RespInsuredIdValiDateApply respInsuredIdValiDateApply) {
        RespInsuredIdValiDateApply = respInsuredIdValiDateApply;
    }

    public ReqBnfIdValiDateApply getReqBnfIdValiDateApply() {
        return ReqBnfIdValiDateApply;
    }

    public void setReqBnfIdValiDateApply(ReqBnfIdValiDateApply reqBnfIdValiDateApply) {
        ReqBnfIdValiDateApply = reqBnfIdValiDateApply;
    }

    public RespBnfIdValiDateApply getRespBnfIdValiDateApply() {
        return RespBnfIdValiDateApply;
    }

    public void setRespBnfIdValiDateApply(RespBnfIdValiDateApply respBnfIdValiDateApply) {
        RespBnfIdValiDateApply = respBnfIdValiDateApply;
    }

    public String getSysCode() {
        return sysCode;
    }

    public String getVersion() {
        return version;
    }

    public String getAgainUWFlag() {
        return againUWFlag;
    }

    public void setSysCode(String sysCode) {
        this.sysCode = sysCode;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public void setAgainUWFlag(String againUWFlag) {
        this.againUWFlag = againUWFlag;
    }

    public com.sinosoft.cloud.access.entity.ReqPayAccountListQuery getReqPayAccountListQuery() {
        return ReqPayAccountListQuery;
    }

    public void setReqPayAccountListQuery(com.sinosoft.cloud.access.entity.ReqPayAccountListQuery reqPayAccountListQuery) {
        ReqPayAccountListQuery = reqPayAccountListQuery;
    }

    public com.sinosoft.cloud.access.entity.RespPayAccountListQuery getRespPayAccountListQuery() {
        return RespPayAccountListQuery;
    }

    public void setRespPayAccountListQuery(com.sinosoft.cloud.access.entity.RespPayAccountListQuery respPayAccountListQuery) {
        RespPayAccountListQuery = respPayAccountListQuery;
    }

    public com.sinosoft.cloud.access.entity.ReqRenewalPayChangeApply getReqRenewalPayChangeApply() {
        return ReqRenewalPayChangeApply;
    }

    public void setReqRenewalPayChangeApply(com.sinosoft.cloud.access.entity.ReqRenewalPayChangeApply reqRenewalPayChangeApply) {
        ReqRenewalPayChangeApply = reqRenewalPayChangeApply;
    }

    public com.sinosoft.cloud.access.entity.ReqImageProcessing getReqImageProcessing() {
        return ReqImageProcessing;
    }

    public void setReqImageProcessing(com.sinosoft.cloud.access.entity.ReqImageProcessing reqImageProcessing) {
        ReqImageProcessing = reqImageProcessing;
    }

    public com.sinosoft.cloud.access.entity.ReqLetterChangeApply getReqLetterChangeApply() {
        return ReqLetterChangeApply;
    }

    public void setReqLetterChangeApply(com.sinosoft.cloud.access.entity.ReqLetterChangeApply reqLetterChangeApply) {
        ReqLetterChangeApply = reqLetterChangeApply;
    }

    public com.sinosoft.cloud.access.entity.ReqCustomerNationalityFATCAChangeApply getReqCustomerNationalityFATCAChangeApply() {
        return ReqCustomerNationalityFATCAChangeApply;
    }

    public void setReqCustomerNationalityFATCAChangeApply(com.sinosoft.cloud.access.entity.ReqCustomerNationalityFATCAChangeApply reqCustomerNationalityFATCAChangeApply) {
        ReqCustomerNationalityFATCAChangeApply = reqCustomerNationalityFATCAChangeApply;
    }

    public com.sinosoft.cloud.access.entity.ReqCustomerAddressEmailChangeApply getReqCustomerAddressEmailChangeApply() {
        return ReqCustomerAddressEmailChangeApply;
    }

    public void setReqCustomerAddressEmailChangeApply(com.sinosoft.cloud.access.entity.ReqCustomerAddressEmailChangeApply reqCustomerAddressEmailChangeApply) {
        ReqCustomerAddressEmailChangeApply = reqCustomerAddressEmailChangeApply;
    }


    public com.sinosoft.cloud.access.entity.ReqImageProcessingMIP getReqImageProcessingMIP() {
        return ReqImageProcessingMIP;
    }

    public void setReqImageProcessingMIP(com.sinosoft.cloud.access.entity.ReqImageProcessingMIP reqImageProcessingMIP) {
        ReqImageProcessingMIP = reqImageProcessingMIP;
    }

    public void setRespWTDetailsQuery(com.sinosoft.cloud.access.entity.RespWTDetailsQuery respWTDetailsQuery) {
        RespWTDetailsQuery = respWTDetailsQuery;
    }

    public void setRespRenewalChangeDetailsQuery(com.sinosoft.cloud.access.entity.RespRenewalChangeDetailsQuery respRenewalChangeDetailsQuery) {
        RespRenewalChangeDetailsQuery = respRenewalChangeDetailsQuery;
    }

    public void setRespPolicyLoanDetailQuery(com.sinosoft.cloud.access.entity.RespPolicyLoanDetailQuery respPolicyLoanDetailQuery) {
        RespPolicyLoanDetailQuery = respPolicyLoanDetailQuery;
    }

    public void setReqPolicyCashValueQuery(com.sinosoft.cloud.access.entity.ReqPolicyCashValueQuery reqPolicyCashValueQuery) {
        ReqPolicyCashValueQuery = reqPolicyCashValueQuery;
    }

    public com.sinosoft.cloud.access.entity.RespWTDetailsQuery getRespWTDetailsQuery() {
        return RespWTDetailsQuery;
    }

    public com.sinosoft.cloud.access.entity.RespRenewalChangeDetailsQuery getRespRenewalChangeDetailsQuery() {
        return RespRenewalChangeDetailsQuery;
    }

    public com.sinosoft.cloud.access.entity.RespPolicyLoanDetailQuery getRespPolicyLoanDetailQuery() {
        return RespPolicyLoanDetailQuery;
    }

    public com.sinosoft.cloud.access.entity.ReqPolicyCashValueQuery getReqPolicyCashValueQuery() {
        return ReqPolicyCashValueQuery;
    }

    public com.sinosoft.cloud.access.entity.RespHandChargeAddPrem getRespHandChargeAddPrem() {
        return RespHandChargeAddPrem;
    }

    public void setRespHandChargeAddPrem(com.sinosoft.cloud.access.entity.RespHandChargeAddPrem respHandChargeAddPrem) {
        RespHandChargeAddPrem = respHandChargeAddPrem;
    }

    public com.sinosoft.cloud.access.entity.ReqHandChargeAddPrem getReqHandChargeAddPrem() {
        return ReqHandChargeAddPrem;
    }

    public void setReqHandChargeAddPrem(com.sinosoft.cloud.access.entity.ReqHandChargeAddPrem reqHandChargeAddPrem) {
        ReqHandChargeAddPrem = reqHandChargeAddPrem;
    }

    public com.sinosoft.cloud.access.entity.ReqAddDefend getReqAddDefend() {
        return ReqAddDefend;
    }

    public void setReqAddDefend(com.sinosoft.cloud.access.entity.ReqAddDefend reqAddDefend) {
        ReqAddDefend = reqAddDefend;
    }

    public com.sinosoft.cloud.access.entity.RespAddDefend getRespAddDefend() {
        return RespAddDefend;
    }

    public void setRespAddDefend(com.sinosoft.cloud.access.entity.RespAddDefend respAddDefend) {
        RespAddDefend = respAddDefend;
    }

    public com.sinosoft.cloud.access.entity.RespBarterpaperBumfQuery getRespBarterpaperBumfQuery() {
        return RespBarterpaperBumfQuery;
    }

    public void setRespBarterpaperBumfQuery(com.sinosoft.cloud.access.entity.RespBarterpaperBumfQuery respBarterpaperBumfQuery) {
        RespBarterpaperBumfQuery = respBarterpaperBumfQuery;
    }

    public com.sinosoft.cloud.access.entity.ReqBarterpaperBumfQuery getReqBarterpaperBumfQuery() {
        return ReqBarterpaperBumfQuery;
    }

    public void setReqBarterpaperBumfQuery(com.sinosoft.cloud.access.entity.ReqBarterpaperBumfQuery reqBarterpaperBumfQuery) {
        ReqBarterpaperBumfQuery = reqBarterpaperBumfQuery;
    }

    public com.sinosoft.cloud.access.entity.ReqBarterpaperBumfApply getReqBarterpaperBumfApply() {
        return ReqBarterpaperBumfApply;
    }

    public void setReqBarterpaperBumfApply(com.sinosoft.cloud.access.entity.ReqBarterpaperBumfApply reqBarterpaperBumfApply) {
        ReqBarterpaperBumfApply = reqBarterpaperBumfApply;
    }

    public com.sinosoft.cloud.access.entity.RespBarterpaperBumfApply getRespBarterpaperBumfApply() {
        return RespBarterpaperBumfApply;
    }

    public void setRespBarterpaperBumfApply(com.sinosoft.cloud.access.entity.RespBarterpaperBumfApply respBarterpaperBumfApply) {
        RespBarterpaperBumfApply = respBarterpaperBumfApply;
    }

    public com.sinosoft.cloud.access.entity.RespCustomerService getRespCustomerService() {
        return RespCustomerService;
    }

    public void setRespCustomerService(com.sinosoft.cloud.access.entity.RespCustomerService respCustomerService) {
        RespCustomerService = respCustomerService;
    }

    public com.sinosoft.cloud.access.entity.ReqCustomerService getReqCustomerService() {
        return ReqCustomerService;
    }

    public void setReqCustomerService(com.sinosoft.cloud.access.entity.ReqCustomerService reqCustomerService) {
        ReqCustomerService = reqCustomerService;
    }

    public com.sinosoft.cloud.access.entity.RespSpecialConventionListQuery getRespSpecialConventionListQuery() {
        return RespSpecialConventionListQuery;
    }

    public void setRespSpecialConventionListQuery(com.sinosoft.cloud.access.entity.RespSpecialConventionListQuery respSpecialConventionListQuery) {
        RespSpecialConventionListQuery = respSpecialConventionListQuery;
    }

    public com.sinosoft.cloud.access.entity.ReqSpecialConventionListQuery getReqSpecialConventionListQuery() {
        return ReqSpecialConventionListQuery;
    }

    public void setReqSpecialConventionListQuery(com.sinosoft.cloud.access.entity.ReqSpecialConventionListQuery reqSpecialConventionListQuery) {
        ReqSpecialConventionListQuery = reqSpecialConventionListQuery;
    }

    public com.sinosoft.cloud.access.entity.RespExpirationApply getRespExpirationApply() {
        return RespExpirationApply;
    }

    public void setRespExpirationApply(com.sinosoft.cloud.access.entity.RespExpirationApply respExpirationApply) {
        RespExpirationApply = respExpirationApply;
    }

    public com.sinosoft.cloud.access.entity.ReqExpirationApply getReqExpirationApply() {
        return ReqExpirationApply;
    }

    public void setReqExpirationApply(com.sinosoft.cloud.access.entity.ReqExpirationApply reqExpirationApply) {
        ReqExpirationApply = reqExpirationApply;
    }

    public com.sinosoft.cloud.access.entity.ReqPremSelectApplyWebSite getReqPremSelectApplyWebSite() {
        return ReqPremSelectApplyWebSite;
    }

    public void setReqPremSelectApplyWebSite(com.sinosoft.cloud.access.entity.ReqPremSelectApplyWebSite reqPremSelectApplyWebSite) {
        ReqPremSelectApplyWebSite = reqPremSelectApplyWebSite;
    }

    public com.sinosoft.cloud.access.entity.RespPremSelectApplyWebSite getRespPremSelectApplyWebSite() {
        return RespPremSelectApplyWebSite;
    }

    public void setRespPremSelectApplyWebSite(com.sinosoft.cloud.access.entity.RespPremSelectApplyWebSite respPremSelectApplyWebSite) {
        RespPremSelectApplyWebSite = respPremSelectApplyWebSite;
    }

    public com.sinosoft.cloud.access.entity.ReqBonusGetModeListQuery getReqBonusGetModeListQuery() {
        return ReqBonusGetModeListQuery;
    }

    public void setReqBonusGetModeListQuery(com.sinosoft.cloud.access.entity.ReqBonusGetModeListQuery reqBonusGetModeListQuery) {
        ReqBonusGetModeListQuery = reqBonusGetModeListQuery;
    }

    public com.sinosoft.cloud.access.entity.RespBonusGetModeListQuery getRespBonusGetModeListQuery() {
        return RespBonusGetModeListQuery;
    }

    public void setRespBonusGetModeListQuery(com.sinosoft.cloud.access.entity.RespBonusGetModeListQuery respBonusGetModeListQuery) {
        RespBonusGetModeListQuery = respBonusGetModeListQuery;
    }

    public com.sinosoft.cloud.access.entity.RespFrameWorkQuery getRespFrameWorkQuery() {
        return RespFrameWorkQuery;
    }

    public void setRespFrameWorkQuery(com.sinosoft.cloud.access.entity.RespFrameWorkQuery respFrameWorkQuery) {
        RespFrameWorkQuery = respFrameWorkQuery;
    }

    public com.sinosoft.cloud.access.entity.ReqFrameWorkQuery getReqFrameWorkQuery() {
        return ReqFrameWorkQuery;
    }

    public void setReqFrameWorkQuery(com.sinosoft.cloud.access.entity.ReqFrameWorkQuery reqFrameWorkQuery) {
        ReqFrameWorkQuery = reqFrameWorkQuery;
    }

    public com.sinosoft.cloud.access.entity.ReqClaimStatusQuery getReqClaimStatusQuery() {
        return ReqClaimStatusQuery;
    }

    public void setReqClaimStatusQuery(com.sinosoft.cloud.access.entity.ReqClaimStatusQuery reqClaimStatusQuery) {
        ReqClaimStatusQuery = reqClaimStatusQuery;
    }

    public com.sinosoft.cloud.access.entity.RespClaimStatusQuery getRespClaimStatusQuery() {
        return RespClaimStatusQuery;
    }

    public void setRespClaimStatusQuery(com.sinosoft.cloud.access.entity.RespClaimStatusQuery respClaimStatusQuery) {
        RespClaimStatusQuery = respClaimStatusQuery;
    }

    public void setWICNewPayMode(String WICNewPayMode) {
        this.WICNewPayMode = WICNewPayMode;
    }

    public String getWICNewPayMode() {
        return WICNewPayMode;
    }

    public String getPolicyUrl() {
        return policyUrl;
    }

    public void setPolicyUrl(String policyUrl) {
        this.policyUrl = policyUrl;
    }

    public void setReqWeChatPolicyQuery(com.sinosoft.cloud.access.entity.ReqWeChatPolicyQuery reqWeChatPolicyQuery) {
        ReqWeChatPolicyQuery = reqWeChatPolicyQuery;
    }

    public void setRespWeChatPolicyQuery(com.sinosoft.cloud.access.entity.RespWeChatPolicyQuery respWeChatPolicyQuery) {
        RespWeChatPolicyQuery = respWeChatPolicyQuery;
    }

    public com.sinosoft.cloud.access.entity.ReqWeChatPolicyQuery getReqWeChatPolicyQuery() {
        return ReqWeChatPolicyQuery;
    }

    public com.sinosoft.cloud.access.entity.RespWeChatPolicyQuery getRespWeChatPolicyQuery() {
        return RespWeChatPolicyQuery;
    }

    public com.sinosoft.cloud.access.entity.BQResult getBQResult() {
        return BQResult;
    }

    public void setBQResult(com.sinosoft.cloud.access.entity.BQResult BQResult) {
        this.BQResult = BQResult;
    }

    public com.sinosoft.cloud.access.entity.ReqWebSiteSurrenderApply getReqWebSiteSurrenderApply() {
        return ReqWebSiteSurrenderApply;
    }

    public void setReqWebSiteSurrenderApply(com.sinosoft.cloud.access.entity.ReqWebSiteSurrenderApply reqWebSiteSurrenderApply) {
        ReqWebSiteSurrenderApply = reqWebSiteSurrenderApply;
    }

    public com.sinosoft.cloud.access.entity.RespWebSiteSurrenderApply getRespWebSiteSurrenderApply() {
        return RespWebSiteSurrenderApply;
    }

    public void setRespWebSiteSurrenderApply(com.sinosoft.cloud.access.entity.RespWebSiteSurrenderApply respWebSiteSurrenderApply) {
        RespWebSiteSurrenderApply = respWebSiteSurrenderApply;
    }

    public com.sinosoft.cloud.access.entity.ReqWebSiteCostVerify getReqWebSiteCostVerify() {
        return ReqWebSiteCostVerify;
    }

    public void setReqWebSiteCostVerify(com.sinosoft.cloud.access.entity.ReqWebSiteCostVerify reqWebSiteCostVerify) {
        ReqWebSiteCostVerify = reqWebSiteCostVerify;
    }

    public com.sinosoft.cloud.access.entity.RespWebSiteCostVerify getRespWebSiteCostVerify() {
        return RespWebSiteCostVerify;
    }

    public void setRespWebSiteCostVerify(com.sinosoft.cloud.access.entity.RespWebSiteCostVerify respWebSiteCostVerify) {
        RespWebSiteCostVerify = respWebSiteCostVerify;
    }

    public com.sinosoft.cloud.access.entity.RespWebSitePolicyValueQuery getRespWebSitePolicyValueQuery() {
        return RespWebSitePolicyValueQuery;
    }

    public void setRespWebSitePolicyValueQuery(com.sinosoft.cloud.access.entity.RespWebSitePolicyValueQuery respWebSitePolicyValueQuery) {
        RespWebSitePolicyValueQuery = respWebSitePolicyValueQuery;
    }

    public com.sinosoft.cloud.access.entity.ReqWebSitePolicyValueQuery getReqWebSitePolicyValueQuery() {
        return ReqWebSitePolicyValueQuery;
    }

    public void setReqWebSitePolicyValueQuery(com.sinosoft.cloud.access.entity.ReqWebSitePolicyValueQuery reqWebSitePolicyValueQuery) {
        ReqWebSitePolicyValueQuery = reqWebSitePolicyValueQuery;
    }

    public com.sinosoft.cloud.access.entity.LMriskchange getLMriskchange() {
        return LMriskchange;
    }

    public void setLMriskchange(com.sinosoft.cloud.access.entity.LMriskchange LMriskchange) {
        this.LMriskchange = LMriskchange;
    }

    public com.sinosoft.cloud.access.entity.RespAgentCharge getRespAgentCharge() {
        return RespAgentCharge;
    }

    public void setRespAgentCharge(com.sinosoft.cloud.access.entity.RespAgentCharge respAgentCharge) {
        RespAgentCharge = respAgentCharge;
    }

    public com.sinosoft.cloud.access.entity.ReqAgentCharge getReqAgentCharge() {
        return ReqAgentCharge;
    }

    public void setReqAgentCharge(com.sinosoft.cloud.access.entity.ReqAgentCharge reqAgentCharge) {
        ReqAgentCharge = reqAgentCharge;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

   @Override
   public String toString() {
      return "Body{" +
              "LMriskchange=" + LMriskchange +
              ", RespPolicyReportQuery=" + RespPolicyReportQuery +
              ", ReqImageProcessingMIP=" + ReqImageProcessingMIP +
              ", ReqRenewalPayChangeApply=" + ReqRenewalPayChangeApply +
              ", ReqImageProcessing=" + ReqImageProcessing +
              ", ReqLetterChangeApply=" + ReqLetterChangeApply +
              ", ReqCustomerNationalityFATCAChangeApply=" + ReqCustomerNationalityFATCAChangeApply +
              ", ReqCustomerAddressEmailChangeApply=" + ReqCustomerAddressEmailChangeApply +
              ", ReqWT=" + ReqWT +
              ", ReqPolicySupplyDetailsQuery=" + ReqPolicySupplyDetailsQuery +
              ", ReqReturnSignSure=" + ReqReturnSignSure +
              ", RespWTDetailsQuery=" + RespWTDetailsQuery +
              ", CaluParamInfos=" + CaluParamInfos +
              ", Global=" + Global +
              ", CashValues=" + CashValues +
              ", ReducedVolumes=" + ReducedVolumes +
              ", LCPolicyInfo=" + LCPolicyInfo +
              ", LCApi=" + LCApi +
              ", LCCont=" + LCCont +
              ", LCPols=" + LCPols +
              ", BQResult=" + BQResult +
              ", ChnlInfoExtends=" + ChnlInfoExtends +
              ", LCAppnt=" + LCAppnt +
              ", LCInsureds=" + LCInsureds +
              ", LCBnfs=" + LCBnfs +
              ", LAAgent=" + LAAgent +
              ", LAAgent1=" + LAAgent1 +
              ", LACom=" + LACom +
              ", LAQualification=" + LAQualification +
              ", LCSpecs=" + LCSpecs +
              ", LDCom=" + LDCom +
              ", LMRiskApps=" + LMRiskApps +
              ", LCAccount=" + LCAccount +
              ", LCAddresses=" + LCAddresses +
              ", LCCustomerImparts=" + LCCustomerImparts +
              ", LKTransInfo=" + LKTransInfo +
              ", LKTransInfoes=" + LKTransInfoes +
              ", LKNRTBizTrans=" + LKNRTBizTrans +
              ", LKTransStatus=" + LKTransStatus +
              ", LKTransStatuses=" + LKTransStatuses +
              ", Reserves=" + Reserves +
              ", ResponseObj=" + ResponseObj +
              ", LDPerson=" + LDPerson +
              ", LCAppntLinkManInfo=" + LCAppntLinkManInfo +
              ", LCDutys=" + LCDutys +
              ", LCGets=" + LCGets +
              ", LCPrems=" + LCPrems +
              ", MIPImparts=" + MIPImparts +
              ", LDPersons=" + LDPersons +
              ", LCCustomerImpartDetails=" + LCCustomerImpartDetails +
              ", LCCustomerImpartParamses=" + LCCustomerImpartParamses +
              ", LKTransTrackses=" + LKTransTrackses +
              ", LKTransTracks=" + LKTransTracks +
              ", LJTempFees=" + LJTempFees +
              ", LJTempFee=" + LJTempFee +
              ", riskCodeWr='" + riskCodeWr + '\'' +
              ", WICNewPayMode='" + WICNewPayMode + '\'' +
              ", policyUrl='" + policyUrl + '\'' +
              ", sysCode='" + sysCode + '\'' +
              ", version='" + version + '\'' +
              ", againUWFlag='" + againUWFlag + '\'' +
              ", operate='" + operate + '\'' +
              ", occupationName='" + occupationName + '\'' +
              ", dateFlag='" + dateFlag + '\'' +
              ", contType='" + contType + '\'' +
              ", oldTranno='" + oldTranno + '\'' +
              ", oldContPrtNo='" + oldContPrtNo + '\'' +
              ", otherCompanyDieAmnt='" + otherCompanyDieAmnt + '\'' +
              ", appNo='" + appNo + '\'' +
              ", remark='" + remark + '\'' +
              ", healthNotice='" + healthNotice + '\'' +
              ", contPrtNo='" + contPrtNo + '\'' +
              ", contNo='" + contNo + '\'' +
              ", unChangedValue='" + unChangedValue + '\'' +
              ", transferFlag='" + transferFlag + '\'' +
              ", reserve='" + reserve + '\'' +
              ", insuTime='" + insuTime + '\'' +
              ", payPrem='" + payPrem + '\'' +
              ", appntMSalary='" + appntMSalary + '\'' +
              ", insuSerial='" + insuSerial + '\'' +
              ", fileTimeStamp='" + fileTimeStamp + '\'' +
              ", priKey='" + priKey + '\'' +
              ", orgKey='" + orgKey + '\'' +
              ", risk=" + risk +
              ", risks=" + risks +
              ", insureds=" + insureds +
              ", policyBasicInfo=" + policyBasicInfo +
              ", appnt=" + appnt +
              ", insured=" + insured +
              ", bnfs=" + bnfs +
              ", ReqRenewalFeeQuery=" + ReqRenewalFeeQuery +
              ", RespRenewalFeeQuery=" + RespRenewalFeeQuery +
              ", ReqClaimReport=" + ReqClaimReport +
              ", RespClaimReport=" + RespClaimReport +
              ", RespSurrenderQuery=" + RespSurrenderQuery +
              ", ReqSurrenderQuery=" + ReqSurrenderQuery +
              ", ReqPreservationStateQuery=" + ReqPreservationStateQuery +
              ", RespPreservationStateQuery=" + RespPreservationStateQuery +
              ", ReqSpecialConventionChangeQuery=" + ReqSpecialConventionChangeQuery +
              ", RespSpecialConventionChangeQuery=" + RespSpecialConventionChangeQuery +
              ", RespNoticeAgainSend=" + RespNoticeAgainSend +
              ", ReqNoticeAgainSend=" + ReqNoticeAgainSend +
              ", RespAppntIDCardChangeCheck=" + RespAppntIDCardChangeCheck +
              ", ReqAppntIDCardChangeCheck=" + ReqAppntIDCardChangeCheck +
              ", ReqAppntIDCardChangeQuery=" + ReqAppntIDCardChangeQuery +
              ", RespAppntIDCardChangeQuery=" + RespAppntIDCardChangeQuery +
              ", ReqAccountHalfDetailsQuery=" + ReqAccountHalfDetailsQuery +
              ", RespAccountHalfDetailsQuery=" + RespAccountHalfDetailsQuery +
              ", ReqBonusUserDetails=" + ReqBonusUserDetails +
              ", RespBonusUserDetails=" + RespBonusUserDetails +
              ", ReqBonusCommitReturn=" + ReqBonusCommitReturn +
              ", RespBonusCommitReturn=" + RespBonusCommitReturn +
              ", RespBonusGetModeDetailsQuery=" + RespBonusGetModeDetailsQuery +
              ", ReqBonusGetModeDetailsQuery=" + ReqBonusGetModeDetailsQuery +
              ", RespPremSelectQuery=" + RespPremSelectQuery +
              ", ReqPremSelectQuery=" + ReqPremSelectQuery +
              ", ReqPremDictionaryClearDetailsQuery=" + ReqPremDictionaryClearDetailsQuery +
              ", RespPremDictionaryClearDetailsQuery=" + RespPremDictionaryClearDetailsQuery +
              ", ReqContQuery=" + ReqContQuery +
              ", RespContQuery=" + RespContQuery +
              ", RespSurrenderApply=" + RespSurrenderApply +
              ", ReqSurrenderApply=" + ReqSurrenderApply +
              ", ReqAccountChange=" + ReqAccountChange +
              ", RespAccountChange=" + RespAccountChange +
              ", ReqSpecialConventionChangeApply=" + ReqSpecialConventionChangeApply +
              ", RespSpecialConventionChangeApply=" + RespSpecialConventionChangeApply +
              ", ReqPolicyLoanApply=" + ReqPolicyLoanApply +
              ", RespPolicyLoanApply=" + RespPolicyLoanApply +
              ", ReqRenewalChangeApply=" + ReqRenewalChangeApply +
              ", RespRenewalChangeApply=" + RespRenewalChangeApply +
              ", ReqInsuredIdValiDateApply=" + ReqInsuredIdValiDateApply +
              ", RespInsuredIdValiDateApply=" + RespInsuredIdValiDateApply +
              ", ReqBnfIdValiDateApply=" + ReqBnfIdValiDateApply +
              ", RespBnfIdValiDateApply=" + RespBnfIdValiDateApply +
              ", ReqHead=" + ReqHead +
              ", RespHead=" + RespHead +
              ", RespWT=" + RespWT +
              ", PolicyQueryInfos=" + PolicyQueryInfos +
              ", ReqRenewal=" + ReqRenewal +
              ", RespRenewal=" + RespRenewal +
              ", ReqWeChatPolicyQuery=" + ReqWeChatPolicyQuery +
              ", RespWeChatPolicyQuery=" + RespWeChatPolicyQuery +
              ", ReqRenewalChangeListQuery=" + ReqRenewalChangeListQuery +
              ", RespRenewalChangeListQuery=" + RespRenewalChangeListQuery +
              ", RespWTListQuery=" + RespWTListQuery +
              ", ReqWTListQuery=" + ReqWTListQuery +
              ", RespBQProgressQuery=" + RespBQProgressQuery +
              ", ReqBQProgressQuery=" + ReqBQProgressQuery +
              ", RespWhilteQuery=" + RespWhilteQuery +
              ", ReqWhilteQuery=" + ReqWhilteQuery +
              ", ReqWhilteApply=" + ReqWhilteApply +
              ", RespWhilteApply=" + RespWhilteApply +
              ", RespPolicySupplyDetailsQuery=" + RespPolicySupplyDetailsQuery +
              ", RespUAccountDetailsQuery=" + RespUAccountDetailsQuery +
              ", ReqUAccountDetailsQuery=" + ReqUAccountDetailsQuery +
              ", ReqLifeQuery=" + ReqLifeQuery +
              ", RespLifeQuery=" + RespLifeQuery +
              ", ReqBnfIdValiDateDetailsQuery=" + ReqBnfIdValiDateDetailsQuery +
              ", RespBnfIdValiDateDetailsQuery=" + RespBnfIdValiDateDetailsQuery +
              ", RespInsuredIdValiDateDetailsQuery=" + RespInsuredIdValiDateDetailsQuery +
              ", ReqInsuredIdValiDateDetailsQuery=" + ReqInsuredIdValiDateDetailsQuery +
              ", ReqRenewalPayChangeDetailsQuery=" + ReqRenewalPayChangeDetailsQuery +
              ", RespRenewalPayChangeDetailsQuery=" + RespRenewalPayChangeDetailsQuery +
              ", ReqAcceptanceCancel=" + ReqAcceptanceCancel +
              ", RespAcceptanceCancel=" + RespAcceptanceCancel +
              ", RespReturnSignQuery=" + RespReturnSignQuery +
              ", ReqReturnSignQuery=" + ReqReturnSignQuery +
              ", ReqNotRealNewContnoApply=" + ReqNotRealNewContnoApply +
              ", RespNotRealNewContnoApply=" + RespNotRealNewContnoApply +
              ", ReqEleCtronPolicy=" + ReqEleCtronPolicy +
              ", RespEleCtronPolicy=" + RespEleCtronPolicy +
              ", RespPolicyUp=" + RespPolicyUp +
              ", ReqPolicyUp=" + ReqPolicyUp +
              ", ReqPolicyChargeInfoQuery=" + ReqPolicyChargeInfoQuery +
              ", RespPolicyChargeInfoQuery=" + RespPolicyChargeInfoQuery +
              ", ReqPolicyHang=" + ReqPolicyHang +
              ", RespPolicyHang=" + RespPolicyHang +
              ", ReqRenewalFee=" + ReqRenewalFee +
              ", RespRenewalFee=" + RespRenewalFee +
              ", RespLifeApply=" + RespLifeApply +
              ", ReqLifeApply=" + ReqLifeApply +
              ", ReqPolicySupplyApply=" + ReqPolicySupplyApply +
              ", RespPolicySupplyApply=" + RespPolicySupplyApply +
              ", ReqPolicyLoanClearDetailsQuery=" + ReqPolicyLoanClearDetailsQuery +
              ", RespPolicyLoanClearDetailsQuery=" + RespPolicyLoanClearDetailsQuery +
              ", ReqPolicyLoanClearApply=" + ReqPolicyLoanClearApply +
              ", RespPolicyLoanClearApply=" + RespPolicyLoanClearApply +
              ", ReqPremDictionaryClearApply=" + ReqPremDictionaryClearApply +
              ", RespPremDictionaryClearApply=" + RespPremDictionaryClearApply +
              ", RespPolicyLoanClearQuery=" + RespPolicyLoanClearQuery +
              ", ReqPolicyLoanClearQuery=" + ReqPolicyLoanClearQuery +
              ", RespReturnSignSure=" + RespReturnSignSure +
              ", RespPolicySupplyQuery=" + RespPolicySupplyQuery +
              ", ReqPolicySupplyQuery=" + ReqPolicySupplyQuery +
              ", ReqPremDictionaryClearListQuery=" + ReqPremDictionaryClearListQuery +
              ", RespPremDictionaryClearListQuery=" + RespPremDictionaryClearListQuery +
              ", ReqPremSelectListQuery=" + ReqPremSelectListQuery +
              ", RespPremSelectListQuery=" + RespPremSelectListQuery +
              ", RespPremSelectApply=" + RespPremSelectApply +
              ", ReqPremSelectApply=" + ReqPremSelectApply +
              ", ReqPolicyReportApply=" + ReqPolicyReportApply +
              ", RespPolicyReportApply=" + RespPolicyReportApply +
              ", ReqPolicyReportQuery=" + ReqPolicyReportQuery +
              ", ReqBonusGetModeApply=" + ReqBonusGetModeApply +
              ", RespBonusGetModeApply=" + RespBonusGetModeApply +
              ", ReqUAccountApply=" + ReqUAccountApply +
              ", RespUAccountApply=" + RespUAccountApply +
              ", RespAccountHalfApply=" + RespAccountHalfApply +
              ", ReqAccountHalfApply=" + ReqAccountHalfApply +
              ", RespAccountDetailsQuery=" + RespAccountDetailsQuery +
              ", ReqAccountDetailsQuery=" + ReqAccountDetailsQuery +
              ", RespPayMentDetailsQuery=" + RespPayMentDetailsQuery +
              ", ReqPayMentDetailsQuery=" + ReqPayMentDetailsQuery +
              ", ReqPayMentApply=" + ReqPayMentApply +
              ", RespPayMentApply=" + RespPayMentApply +
              ", ReqPayAccountDetailsQuery=" + ReqPayAccountDetailsQuery +
              ", RespPayAccountDetailsQuery=" + RespPayAccountDetailsQuery +
              ", ReqPayAccountApply=" + ReqPayAccountApply +
              ", RespPayAccountApply=" + RespPayAccountApply +
              ", RespPreservePolicyDetailsQuery=" + RespPreservePolicyDetailsQuery +
              ", ReqPreservePolicyDetailsQuery=" + ReqPreservePolicyDetailsQuery +
              ", RespPreserveApply=" + RespPreserveApply +
              ", ReqPreserveApply=" + ReqPreserveApply +
              ", ReqRenewalAwoke=" + ReqRenewalAwoke +
              ", RespRenewalAwoke=" + RespRenewalAwoke +
              ", ReqRenewalAdviseQuery=" + ReqRenewalAdviseQuery +
              ", RespRenewalAdviseQuery=" + RespRenewalAdviseQuery +
              ", ReqAuthentication=" + ReqAuthentication +
              ", RespAuthentication=" + RespAuthentication +
              ", ReqCustomerAddressEmailChangeQuery=" + ReqCustomerAddressEmailChangeQuery +
              ", RespCustomerAddressEmailChangeQuery=" + RespCustomerAddressEmailChangeQuery +
              ", RespLetterChangeQuery=" + RespLetterChangeQuery +
              ", ReqLetterChangeQuery=" + ReqLetterChangeQuery +
              ", RespLifeCashListQuery=" + RespLifeCashListQuery +
              ", ReqLifeCashListQuery=" + ReqLifeCashListQuery +
              ", ReqWTDetailsQuery=" + ReqWTDetailsQuery +
              ", RespRenewalPayChangeApply=" + RespRenewalPayChangeApply +
              ", ReqRenewalChangeDetailsQuery=" + ReqRenewalChangeDetailsQuery +
              ", RespRenewalChangeDetailsQuery=" + RespRenewalChangeDetailsQuery +
              ", ReqPolicyLoanDetailQuery=" + ReqPolicyLoanDetailQuery +
              ", RespPolicyLoanDetailQuery=" + RespPolicyLoanDetailQuery +
              ", RespCustomerNationalityFATCAChangeApply=" + RespCustomerNationalityFATCAChangeApply +
              ", RespCustomerAddressEmailChangeApply=" + RespCustomerAddressEmailChangeApply +
              ", RespLetterChangeApply=" + RespLetterChangeApply +
              ", ReqBonusGetListQuery=" + ReqBonusGetListQuery +
              ", RespBonusGetListQuery=" + RespBonusGetListQuery +
              ", ReqBonusGetDetailsQuery=" + ReqBonusGetDetailsQuery +
              ", RespBonusGetDetailsQuery=" + RespBonusGetDetailsQuery +
              ", ReqBonusDrawPolicyList=" + ReqBonusDrawPolicyList +
              ", RespBonusDrawPolicyList=" + RespBonusDrawPolicyList +
              ", RespUAccountListQuery=" + RespUAccountListQuery +
              ", ReqUAccountListQuery=" + ReqUAccountListQuery +
              ", ReqAccountHalfQuery=" + ReqAccountHalfQuery +
              ", RespAccountHalfQuery=" + RespAccountHalfQuery +
              ", RespAccountListQuery=" + RespAccountListQuery +
              ", ReqAccountListQuery=" + ReqAccountListQuery +
              ", ReqPayMentQuery=" + ReqPayMentQuery +
              ", RespPayMentQuery=" + RespPayMentQuery +
              ", ReqLiveGoldGetQuery=" + ReqLiveGoldGetQuery +
              ", RespLiveGoldGetQuery=" + RespLiveGoldGetQuery +
              ", RespLifeCashDetailsQuery=" + RespLifeCashDetailsQuery +
              ", ReqLifeCashDetailsQuery=" + ReqLifeCashDetailsQuery +
              ", ReqBnfIdValiDateQuery=" + ReqBnfIdValiDateQuery +
              ", RespBnfIdValiDateQuery=" + RespBnfIdValiDateQuery +
              ", RespInsuredIdValiDateQuery=" + RespInsuredIdValiDateQuery +
              ", ReqInsuredIdValiDateQuery=" + ReqInsuredIdValiDateQuery +
              ", ReqAppntIDCardChangeApply=" + ReqAppntIDCardChangeApply +
              ", RespAppntIDCardChangeApply=" + RespAppntIDCardChangeApply +
              ", RespNoticeAgainSendListQuery=" + RespNoticeAgainSendListQuery +
              ", ReqNoticeAgainSendListQuery=" + ReqNoticeAgainSendListQuery +
              ", ReqRenewalPayChangeListQuery=" + ReqRenewalPayChangeListQuery +
              ", RespRenewalPayChangeListQuery=" + RespRenewalPayChangeListQuery +
              ", ReqCustomerNationalityFATCAChangeQuery=" + ReqCustomerNationalityFATCAChangeQuery +
              ", RespCustomerNationalityFATCAChangeQuery=" + RespCustomerNationalityFATCAChangeQuery +
              ", ReqPolicyLoanListQuery=" + ReqPolicyLoanListQuery +
              ", RespPolicyLoanListQuery=" + RespPolicyLoanListQuery +
              ", RespImageProcessing=" + RespImageProcessing +
              ", ReqRetypePolicy=" + ReqRetypePolicy +
              ", ReqPolicyCashValueQuery=" + ReqPolicyCashValueQuery +
              ", ReqPayAccountListQuery=" + ReqPayAccountListQuery +
              ", RespPayAccountListQuery=" + RespPayAccountListQuery +
              ", ReqContDetailsQuery=" + ReqContDetailsQuery +
              ", RespPolicyLoanClearApplyWebSite=" + RespPolicyLoanClearApplyWebSite +
              ", ReqPolicyLoanClearApplyWebSite=" + ReqPolicyLoanClearApplyWebSite +
              ", RespHandChargeAddPrem=" + RespHandChargeAddPrem +
              ", ReqHandChargeAddPrem=" + ReqHandChargeAddPrem +
              ", ReqAddDefend=" + ReqAddDefend +
              ", RespAddDefend=" + RespAddDefend +
              ", RespBarterpaperBumfQuery=" + RespBarterpaperBumfQuery +
              ", ReqBarterpaperBumfQuery=" + ReqBarterpaperBumfQuery +
              ", ReqBarterpaperBumfApply=" + ReqBarterpaperBumfApply +
              ", RespBarterpaperBumfApply=" + RespBarterpaperBumfApply +
              ", RespCustomerService=" + RespCustomerService +
              ", ReqCustomerService=" + ReqCustomerService +
              ", RespSpecialConventionListQuery=" + RespSpecialConventionListQuery +
              ", ReqSpecialConventionListQuery=" + ReqSpecialConventionListQuery +
              ", RespExpirationApply=" + RespExpirationApply +
              ", ReqExpirationApply=" + ReqExpirationApply +
              ", ReqPremSelectApplyWebSite=" + ReqPremSelectApplyWebSite +
              ", RespPremSelectApplyWebSite=" + RespPremSelectApplyWebSite +
              ", RespExpirationQuery=" + RespExpirationQuery +
              ", ReqExpirationQuery=" + ReqExpirationQuery +
              ", RespPreservePolicyListQuery=" + RespPreservePolicyListQuery +
              ", ReqPreservePolicyListQuery=" + ReqPreservePolicyListQuery +
              ", ReqBonusGetModeListQuery=" + ReqBonusGetModeListQuery +
              ", RespBonusGetModeListQuery=" + RespBonusGetModeListQuery +
              ", RespFrameWorkQuery=" + RespFrameWorkQuery +
              ", ReqFrameWorkQuery=" + ReqFrameWorkQuery +
              ", ReqClaimStatusQuery=" + ReqClaimStatusQuery +
              ", RespClaimStatusQuery=" + RespClaimStatusQuery +
              ", ReqWebSiteSurrenderApply=" + ReqWebSiteSurrenderApply +
              ", RespWebSiteSurrenderApply=" + RespWebSiteSurrenderApply +
              ", ReqWebSiteCostVerify=" + ReqWebSiteCostVerify +
              ", RespWebSiteCostVerify=" + RespWebSiteCostVerify +
              ", RespWebSitePolicyValueQuery=" + RespWebSitePolicyValueQuery +
              ", ReqWebSitePolicyValueQuery=" + ReqWebSitePolicyValueQuery +
              ", ReqAgentCharge=" + ReqAgentCharge +
              ", RespAgentCharge=" + RespAgentCharge +
              '}';
   }
}