package com.sinosoft.cloud.access.entity;

/**
 * abc-cloud-access
 *
 * @author lizhen
 * @create 2019/1/14
 */
public class ManageCom {
    //机构编码
    private String ManageCom;
    //机构名称     ;
    private String ManageName;
    //保单号
    private String ContNo;

    public String getManageCom() {
        return ManageCom;
    }

    public void setManageCom(String manageCom) {
        ManageCom = manageCom;
    }

    public String getManageName() {
        return ManageName;
    }

    public void setManageName(String manageName) {
        ManageName = manageName;
    }

    public String getContNo() {
        return ContNo;
    }

    public void setContNo(String contNo) {
        ContNo = contNo;
    }

    @Override
    public String toString() {
        return "ManageCom{" +
                "ManageCom='" + ManageCom + '\'' +
                ", ManageName='" + ManageName + '\'' +
                ", ContNo='" + ContNo + '\'' +
                '}';
    }
}