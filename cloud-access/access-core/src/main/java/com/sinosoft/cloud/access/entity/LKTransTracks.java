package com.sinosoft.cloud.access.entity;

import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.StrTool;

/**
 * @Author:lizhen
 * @Description:
 * @Date:Created in 17:53 2018/9/5
 * @Modified By:
 */
public class LKTransTracks {
    private long TransID;
    private String ShadingID;
    private String TransNo;
    private String AccessChnl;
    private String AccessChnlSub;
    private String TransCode;
    private String TransCodeOri;
    private String BankCode;
    private String BankBranch;
    private String BankNode;
    private String ContNo;
    private String ProposalNo;
    private double TransMony;
    private String TransDate;
    private String TransTime;
    private String ContType;
    private String PHConclusion;
    private String UWConclusion;
    private String SellsWay;
    private String Perform;
    private String BalanceStateCode;
    private String BalanceResult;
    private String BalanceResultDesc;
    private String Temp1;
    private String Temp2;
    private String Temp3;
    private String Temp4;
    private String Temp5;
    private String PayMode;
    public static final int FIELDNUM = 29;
    private FDate fDate = new FDate();

    public long getTransID() {
        return this.TransID;
    }

    public void setTransID(long aTransID) {
        this.TransID = aTransID;
    }

    public void setTransID(String aTransID) {
        if (aTransID != null && !aTransID.equals("")) {
            this.TransID = (new Long(aTransID)).longValue();
        }

    }

    public String getShadingID() {
        return this.ShadingID;
    }

    public void setShadingID(String aShadingID) {
        this.ShadingID = aShadingID;
    }

    public String getTransNo() {
        return this.TransNo;
    }

    public void setTransNo(String aTransNo) {
        this.TransNo = aTransNo;
    }

    public String getAccessChnl() {
        return this.AccessChnl;
    }

    public void setAccessChnl(String aAccessChnl) {
        this.AccessChnl = aAccessChnl;
    }

    public String getAccessChnlSub() {
        return this.AccessChnlSub;
    }

    public void setAccessChnlSub(String aAccessChnlSub) {
        this.AccessChnlSub = aAccessChnlSub;
    }

    public String getTransCode() {
        return this.TransCode;
    }

    public void setTransCode(String aTransCode) {
        this.TransCode = aTransCode;
    }

    public String getTransCodeOri() {
        return this.TransCodeOri;
    }

    public void setTransCodeOri(String aTransCodeOri) {
        this.TransCodeOri = aTransCodeOri;
    }

    public String getBankCode() {
        return this.BankCode;
    }

    public void setBankCode(String aBankCode) {
        this.BankCode = aBankCode;
    }

    public String getBankBranch() {
        return this.BankBranch;
    }

    public void setBankBranch(String aBankBranch) {
        this.BankBranch = aBankBranch;
    }

    public String getBankNode() {
        return this.BankNode;
    }

    public void setBankNode(String aBankNode) {
        this.BankNode = aBankNode;
    }

    public String getContNo() {
        return this.ContNo;
    }

    public void setContNo(String aContNo) {
        this.ContNo = aContNo;
    }

    public String getProposalNo() {
        return this.ProposalNo;
    }

    public void setProposalNo(String aProposalNo) {
        this.ProposalNo = aProposalNo;
    }

    public double getTransMony() {
        return this.TransMony;
    }

    public void setTransMony(double aTransMony) {
        this.TransMony = aTransMony;
    }

    public void setTransMony(String aTransMony) {
        if (aTransMony != null && !aTransMony.equals("")) {
            Double tDouble = new Double(aTransMony);
            double d = tDouble.doubleValue();
            this.TransMony = d;
        }

    }

    public String getTransDate() {
        return this.TransDate;
    }

    public void setTransDate(String aTransDate) {
        this.TransDate = aTransDate;
    }

    public String getTransTime() {
        return this.TransTime;
    }

    public void setTransTime(String aTransTime) {
        this.TransTime = aTransTime;
    }

    public String getContType() {
        return this.ContType;
    }

    public void setContType(String aContType) {
        this.ContType = aContType;
    }

    public String getPHConclusion() {
        return this.PHConclusion;
    }

    public void setPHConclusion(String aPHConclusion) {
        this.PHConclusion = aPHConclusion;
    }

    public String getUWConclusion() {
        return this.UWConclusion;
    }

    public void setUWConclusion(String aUWConclusion) {
        this.UWConclusion = aUWConclusion;
    }

    public String getSellsWay() {
        return this.SellsWay;
    }

    public void setSellsWay(String aSellsWay) {
        this.SellsWay = aSellsWay;
    }

    public String getPerform() {
        return this.Perform;
    }

    public void setPerform(String aPerform) {
        this.Perform = aPerform;
    }

    public String getBalanceStateCode() {
        return this.BalanceStateCode;
    }

    public void setBalanceStateCode(String aBalanceStateCode) {
        this.BalanceStateCode = aBalanceStateCode;
    }

    public String getBalanceResult() {
        return this.BalanceResult;
    }

    public void setBalanceResult(String aBalanceResult) {
        this.BalanceResult = aBalanceResult;
    }

    public String getBalanceResultDesc() {
        return this.BalanceResultDesc;
    }

    public void setBalanceResultDesc(String aBalanceResultDesc) {
        this.BalanceResultDesc = aBalanceResultDesc;
    }

    public String getTemp1() {
        return this.Temp1;
    }

    public void setTemp1(String aTemp1) {
        this.Temp1 = aTemp1;
    }

    public String getTemp2() {
        return this.Temp2;
    }

    public void setTemp2(String aTemp2) {
        this.Temp2 = aTemp2;
    }

    public String getTemp3() {
        return this.Temp3;
    }

    public void setTemp3(String aTemp3) {
        this.Temp3 = aTemp3;
    }

    public String getTemp4() {
        return this.Temp4;
    }

    public void setTemp4(String aTemp4) {
        this.Temp4 = aTemp4;
    }

    public String getTemp5() {
        return this.Temp5;
    }

    public void setTemp5(String aTemp5) {
        this.Temp5 = aTemp5;
    }

    public String getPayMode() {
        return this.PayMode;
    }

    public void setPayMode(String aPayMode) {
        this.PayMode = aPayMode;
    }

    public int getFieldCount() {
        return 29;
    }

    public int getFieldIndex(String strFieldName) {
        return strFieldName.equals("TransID") ? 0 : (strFieldName.equals("ShadingID") ? 1 : (strFieldName.equals("TransNo") ? 2 : (strFieldName.equals("AccessChnl") ? 3 : (strFieldName.equals("AccessChnlSub") ? 4 : (strFieldName.equals("TransCode") ? 5 : (strFieldName.equals("TransCodeOri") ? 6 : (strFieldName.equals("BankCode") ? 7 : (strFieldName.equals("BankBranch") ? 8 : (strFieldName.equals("BankNode") ? 9 : (strFieldName.equals("ContNo") ? 10 : (strFieldName.equals("ProposalNo") ? 11 : (strFieldName.equals("TransMony") ? 12 : (strFieldName.equals("TransDate") ? 13 : (strFieldName.equals("TransTime") ? 14 : (strFieldName.equals("ContType") ? 15 : (strFieldName.equals("PHConclusion") ? 16 : (strFieldName.equals("UWConclusion") ? 17 : (strFieldName.equals("SellsWay") ? 18 : (strFieldName.equals("Perform") ? 19 : (strFieldName.equals("BalanceStateCode") ? 20 : (strFieldName.equals("BalanceResult") ? 21 : (strFieldName.equals("BalanceResultDesc") ? 22 : (strFieldName.equals("Temp1") ? 23 : (strFieldName.equals("Temp2") ? 24 : (strFieldName.equals("Temp3") ? 25 : (strFieldName.equals("Temp4") ? 26 : (strFieldName.equals("Temp5") ? 27 : (strFieldName.equals("PayMode") ? 28 : -1))))))))))))))))))))))))))));
    }

    public String getFieldName(int nFieldIndex) {
        String strFieldName = "";
        switch (nFieldIndex) {
            case 0:
                strFieldName = "TransID";
                break;
            case 1:
                strFieldName = "ShadingID";
                break;
            case 2:
                strFieldName = "TransNo";
                break;
            case 3:
                strFieldName = "AccessChnl";
                break;
            case 4:
                strFieldName = "AccessChnlSub";
                break;
            case 5:
                strFieldName = "TransCode";
                break;
            case 6:
                strFieldName = "TransCodeOri";
                break;
            case 7:
                strFieldName = "BankCode";
                break;
            case 8:
                strFieldName = "BankBranch";
                break;
            case 9:
                strFieldName = "BankNode";
                break;
            case 10:
                strFieldName = "ContNo";
                break;
            case 11:
                strFieldName = "ProposalNo";
                break;
            case 12:
                strFieldName = "TransMony";
                break;
            case 13:
                strFieldName = "TransDate";
                break;
            case 14:
                strFieldName = "TransTime";
                break;
            case 15:
                strFieldName = "ContType";
                break;
            case 16:
                strFieldName = "PHConclusion";
                break;
            case 17:
                strFieldName = "UWConclusion";
                break;
            case 18:
                strFieldName = "SellsWay";
                break;
            case 19:
                strFieldName = "Perform";
                break;
            case 20:
                strFieldName = "BalanceStateCode";
                break;
            case 21:
                strFieldName = "BalanceResult";
                break;
            case 22:
                strFieldName = "BalanceResultDesc";
                break;
            case 23:
                strFieldName = "Temp1";
                break;
            case 24:
                strFieldName = "Temp2";
                break;
            case 25:
                strFieldName = "Temp3";
                break;
            case 26:
                strFieldName = "Temp4";
                break;
            case 27:
                strFieldName = "Temp5";
                break;
            case 28:
                strFieldName = "PayMode";
                break;
            default:
                strFieldName = "";
        }

        return strFieldName;
    }

    public int getFieldType(String strFieldName) {
        String var2 = strFieldName.toUpperCase();
        byte var3 = -1;
        switch (var2.hashCode()) {
            case -1076025815:
                if (var2.equals("BANKCODE")) {
                    var3 = 7;
                }
                break;
            case -1075698114:
                if (var2.equals("BANKNODE")) {
                    var3 = 9;
                }
                break;
            case -656481209:
                if (var2.equals("ACCESSCHNL")) {
                    var3 = 3;
                }
                break;
            case -618350119:
                if (var2.equals("BALANCERESULT")) {
                    var3 = 21;
                }
                break;
            case -580016575:
                if (var2.equals("UWCONCLUSION")) {
                    var3 = 17;
                }
                break;
            case -466478102:
                if (var2.equals("BALANCERESULTDESC")) {
                    var3 = 22;
                }
                break;
            case -455795595:
                if (var2.equals("TRANSCODE")) {
                    var3 = 5;
                }
                break;
            case -455778762:
                if (var2.equals("TRANSDATE")) {
                    var3 = 13;
                }
                break;
            case -455497355:
                if (var2.equals("TRANSMONY")) {
                    var3 = 12;
                }
                break;
            case -455294635:
                if (var2.equals("TRANSTIME")) {
                    var3 = 14;
                }
                break;
            case -419485022:
                if (var2.equals("BALANCESTATECODE")) {
                    var3 = 20;
                }
                break;
            case -349077085:
                if (var2.equals("TRANSID")) {
                    var3 = 0;
                }
                break;
            case -349076919:
                if (var2.equals("TRANSNO")) {
                    var3 = 2;
                }
                break;
            case -154308557:
                if (var2.equals("PROPOSALNO")) {
                    var3 = 11;
                }
                break;
            case -68689365:
                if (var2.equals("PAYMODE")) {
                    var3 = 28;
                }
                break;
            case -40963561:
                if (var2.equals("PHCONCLUSION")) {
                    var3 = 16;
                }
                break;
            case 39154497:
                if (var2.equals("PERFORM")) {
                    var3 = 19;
                }
                break;
            case 79707869:
                if (var2.equals("TEMP1")) {
                    var3 = 23;
                }
                break;
            case 79707870:
                if (var2.equals("TEMP2")) {
                    var3 = 24;
                }
                break;
            case 79707871:
                if (var2.equals("TEMP3")) {
                    var3 = 25;
                }
                break;
            case 79707872:
                if (var2.equals("TEMP4")) {
                    var3 = 26;
                }
                break;
            case 79707873:
                if (var2.equals("TEMP5")) {
                    var3 = 27;
                }
                break;
            case 215762284:
                if (var2.equals("CONTTYPE")) {
                    var3 = 15;
                }
                break;
            case 808218149:
                if (var2.equals("SHADINGID")) {
                    var3 = 1;
                }
                break;
            case 1000372958:
                if (var2.equals("BANKBRANCH")) {
                    var3 = 8;
                }
                break;
            case 1981143022:
                if (var2.equals("SELLSWAY")) {
                    var3 = 18;
                }
                break;
            case 1993518195:
                if (var2.equals("CONTNO")) {
                    var3 = 10;
                }
                break;
            case 2049451129:
                if (var2.equals("ACCESSCHNLSUB")) {
                    var3 = 4;
                }
                break;
            case 2080097841:
                if (var2.equals("TRANSCODEORI")) {
                    var3 = 6;
                }
        }

        switch (var3) {
            case 0:
                return 7;
            case 1:
                return 0;
            case 2:
                return 0;
            case 3:
                return 0;
            case 4:
                return 0;
            case 5:
                return 0;
            case 6:
                return 0;
            case 7:
                return 0;
            case 8:
                return 0;
            case 9:
                return 0;
            case 10:
                return 0;
            case 11:
                return 0;
            case 12:
                return 4;
            case 13:
                return 0;
            case 14:
                return 0;
            case 15:
                return 0;
            case 16:
                return 0;
            case 17:
                return 0;
            case 18:
                return 0;
            case 19:
                return 0;
            case 20:
                return 0;
            case 21:
                return 0;
            case 22:
                return 0;
            case 23:
                return 0;
            case 24:
                return 0;
            case 25:
                return 0;
            case 26:
                return 0;
            case 27:
                return 0;
            case 28:
                return 0;
            default:
                return -1;
        }
    }

    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return 7;
            case 1:
                return 0;
            case 2:
                return 0;
            case 3:
                return 0;
            case 4:
                return 0;
            case 5:
                return 0;
            case 6:
                return 0;
            case 7:
                return 0;
            case 8:
                return 0;
            case 9:
                return 0;
            case 10:
                return 0;
            case 11:
                return 0;
            case 12:
                return 4;
            case 13:
                return 0;
            case 14:
                return 0;
            case 15:
                return 0;
            case 16:
                return 0;
            case 17:
                return 0;
            case 18:
                return 0;
            case 19:
                return 0;
            case 20:
                return 0;
            case 21:
                return 0;
            case 22:
                return 0;
            case 23:
                return 0;
            case 24:
                return 0;
            case 25:
                return 0;
            case 26:
                return 0;
            case 27:
                return 0;
            case 28:
                return 0;
            default:
                return -1;
        }
    }

    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("TransID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.TransID));
        }

        if (FCode.equalsIgnoreCase("ShadingID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.ShadingID));
        }

        if (FCode.equalsIgnoreCase("TransNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.TransNo));
        }

        if (FCode.equalsIgnoreCase("AccessChnl")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.AccessChnl));
        }

        if (FCode.equalsIgnoreCase("AccessChnlSub")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.AccessChnlSub));
        }

        if (FCode.equalsIgnoreCase("TransCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.TransCode));
        }

        if (FCode.equalsIgnoreCase("TransCodeOri")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.TransCodeOri));
        }

        if (FCode.equalsIgnoreCase("BankCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.BankCode));
        }

        if (FCode.equalsIgnoreCase("BankBranch")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.BankBranch));
        }

        if (FCode.equalsIgnoreCase("BankNode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.BankNode));
        }

        if (FCode.equalsIgnoreCase("ContNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.ContNo));
        }

        if (FCode.equalsIgnoreCase("ProposalNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.ProposalNo));
        }

        if (FCode.equalsIgnoreCase("TransMony")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.TransMony));
        }

        if (FCode.equalsIgnoreCase("TransDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.TransDate));
        }

        if (FCode.equalsIgnoreCase("TransTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.TransTime));
        }

        if (FCode.equalsIgnoreCase("ContType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.ContType));
        }

        if (FCode.equalsIgnoreCase("PHConclusion")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.PHConclusion));
        }

        if (FCode.equalsIgnoreCase("UWConclusion")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.UWConclusion));
        }

        if (FCode.equalsIgnoreCase("SellsWay")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.SellsWay));
        }

        if (FCode.equalsIgnoreCase("Perform")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.Perform));
        }

        if (FCode.equalsIgnoreCase("BalanceStateCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.BalanceStateCode));
        }

        if (FCode.equalsIgnoreCase("BalanceResult")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.BalanceResult));
        }

        if (FCode.equalsIgnoreCase("BalanceResultDesc")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.BalanceResultDesc));
        }

        if (FCode.equalsIgnoreCase("Temp1")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.Temp1));
        }

        if (FCode.equalsIgnoreCase("Temp2")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.Temp2));
        }

        if (FCode.equalsIgnoreCase("Temp3")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.Temp3));
        }

        if (FCode.equalsIgnoreCase("Temp4")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.Temp4));
        }

        if (FCode.equalsIgnoreCase("Temp5")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.Temp5));
        }

        if (FCode.equalsIgnoreCase("PayMode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.PayMode));
        }

        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }

    public String getV(int nFieldIndex) {
        String strFieldValue = "";
        switch (nFieldIndex) {
            case 0:
                strFieldValue = String.valueOf(this.TransID);
                break;
            case 1:
                strFieldValue = String.valueOf(this.ShadingID);
                break;
            case 2:
                strFieldValue = String.valueOf(this.TransNo);
                break;
            case 3:
                strFieldValue = String.valueOf(this.AccessChnl);
                break;
            case 4:
                strFieldValue = String.valueOf(this.AccessChnlSub);
                break;
            case 5:
                strFieldValue = String.valueOf(this.TransCode);
                break;
            case 6:
                strFieldValue = String.valueOf(this.TransCodeOri);
                break;
            case 7:
                strFieldValue = String.valueOf(this.BankCode);
                break;
            case 8:
                strFieldValue = String.valueOf(this.BankBranch);
                break;
            case 9:
                strFieldValue = String.valueOf(this.BankNode);
                break;
            case 10:
                strFieldValue = String.valueOf(this.ContNo);
                break;
            case 11:
                strFieldValue = String.valueOf(this.ProposalNo);
                break;
            case 12:
                strFieldValue = String.valueOf(this.TransMony);
                break;
            case 13:
                strFieldValue = String.valueOf(this.TransDate);
                break;
            case 14:
                strFieldValue = String.valueOf(this.TransTime);
                break;
            case 15:
                strFieldValue = String.valueOf(this.ContType);
                break;
            case 16:
                strFieldValue = String.valueOf(this.PHConclusion);
                break;
            case 17:
                strFieldValue = String.valueOf(this.UWConclusion);
                break;
            case 18:
                strFieldValue = String.valueOf(this.SellsWay);
                break;
            case 19:
                strFieldValue = String.valueOf(this.Perform);
                break;
            case 20:
                strFieldValue = String.valueOf(this.BalanceStateCode);
                break;
            case 21:
                strFieldValue = String.valueOf(this.BalanceResult);
                break;
            case 22:
                strFieldValue = String.valueOf(this.BalanceResultDesc);
                break;
            case 23:
                strFieldValue = String.valueOf(this.Temp1);
                break;
            case 24:
                strFieldValue = String.valueOf(this.Temp2);
                break;
            case 25:
                strFieldValue = String.valueOf(this.Temp3);
                break;
            case 26:
                strFieldValue = String.valueOf(this.Temp4);
                break;
            case 27:
                strFieldValue = String.valueOf(this.Temp5);
                break;
            case 28:
                strFieldValue = String.valueOf(this.PayMode);
                break;
            default:
                strFieldValue = "";
        }

        if (strFieldValue.equals("")) {
            strFieldValue = "null";
        }

        return strFieldValue;
    }

    public boolean setV(String FCode, String FValue) {
        if (StrTool.cTrim(FCode).equals("")) {
            return false;
        } else {
            if (FCode.equalsIgnoreCase("TransID") && FValue != null && !FValue.equals("")) {
                this.TransID = (new Long(FValue)).longValue();
            }

            if (FCode.equalsIgnoreCase("ShadingID")) {
                if (FValue != null && !FValue.equals("")) {
                    this.ShadingID = FValue.trim();
                } else {
                    this.ShadingID = null;
                }
            }

            if (FCode.equalsIgnoreCase("TransNo")) {
                if (FValue != null && !FValue.equals("")) {
                    this.TransNo = FValue.trim();
                } else {
                    this.TransNo = null;
                }
            }

            if (FCode.equalsIgnoreCase("AccessChnl")) {
                if (FValue != null && !FValue.equals("")) {
                    this.AccessChnl = FValue.trim();
                } else {
                    this.AccessChnl = null;
                }
            }

            if (FCode.equalsIgnoreCase("AccessChnlSub")) {
                if (FValue != null && !FValue.equals("")) {
                    this.AccessChnlSub = FValue.trim();
                } else {
                    this.AccessChnlSub = null;
                }
            }

            if (FCode.equalsIgnoreCase("TransCode")) {
                if (FValue != null && !FValue.equals("")) {
                    this.TransCode = FValue.trim();
                } else {
                    this.TransCode = null;
                }
            }

            if (FCode.equalsIgnoreCase("TransCodeOri")) {
                if (FValue != null && !FValue.equals("")) {
                    this.TransCodeOri = FValue.trim();
                } else {
                    this.TransCodeOri = null;
                }
            }

            if (FCode.equalsIgnoreCase("BankCode")) {
                if (FValue != null && !FValue.equals("")) {
                    this.BankCode = FValue.trim();
                } else {
                    this.BankCode = null;
                }
            }

            if (FCode.equalsIgnoreCase("BankBranch")) {
                if (FValue != null && !FValue.equals("")) {
                    this.BankBranch = FValue.trim();
                } else {
                    this.BankBranch = null;
                }
            }

            if (FCode.equalsIgnoreCase("BankNode")) {
                if (FValue != null && !FValue.equals("")) {
                    this.BankNode = FValue.trim();
                } else {
                    this.BankNode = null;
                }
            }

            if (FCode.equalsIgnoreCase("ContNo")) {
                if (FValue != null && !FValue.equals("")) {
                    this.ContNo = FValue.trim();
                } else {
                    this.ContNo = null;
                }
            }

            if (FCode.equalsIgnoreCase("ProposalNo")) {
                if (FValue != null && !FValue.equals("")) {
                    this.ProposalNo = FValue.trim();
                } else {
                    this.ProposalNo = null;
                }
            }

            if (FCode.equalsIgnoreCase("TransMony") && FValue != null && !FValue.equals("")) {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                this.TransMony = d;
            }

            if (FCode.equalsIgnoreCase("TransDate")) {
                if (FValue != null && !FValue.equals("")) {
                    this.TransDate = FValue.trim();
                } else {
                    this.TransDate = null;
                }
            }

            if (FCode.equalsIgnoreCase("TransTime")) {
                if (FValue != null && !FValue.equals("")) {
                    this.TransTime = FValue.trim();
                } else {
                    this.TransTime = null;
                }
            }

            if (FCode.equalsIgnoreCase("ContType")) {
                if (FValue != null && !FValue.equals("")) {
                    this.ContType = FValue.trim();
                } else {
                    this.ContType = null;
                }
            }

            if (FCode.equalsIgnoreCase("PHConclusion")) {
                if (FValue != null && !FValue.equals("")) {
                    this.PHConclusion = FValue.trim();
                } else {
                    this.PHConclusion = null;
                }
            }

            if (FCode.equalsIgnoreCase("UWConclusion")) {
                if (FValue != null && !FValue.equals("")) {
                    this.UWConclusion = FValue.trim();
                } else {
                    this.UWConclusion = null;
                }
            }

            if (FCode.equalsIgnoreCase("SellsWay")) {
                if (FValue != null && !FValue.equals("")) {
                    this.SellsWay = FValue.trim();
                } else {
                    this.SellsWay = null;
                }
            }

            if (FCode.equalsIgnoreCase("Perform")) {
                if (FValue != null && !FValue.equals("")) {
                    this.Perform = FValue.trim();
                } else {
                    this.Perform = null;
                }
            }

            if (FCode.equalsIgnoreCase("BalanceStateCode")) {
                if (FValue != null && !FValue.equals("")) {
                    this.BalanceStateCode = FValue.trim();
                } else {
                    this.BalanceStateCode = null;
                }
            }

            if (FCode.equalsIgnoreCase("BalanceResult")) {
                if (FValue != null && !FValue.equals("")) {
                    this.BalanceResult = FValue.trim();
                } else {
                    this.BalanceResult = null;
                }
            }

            if (FCode.equalsIgnoreCase("BalanceResultDesc")) {
                if (FValue != null && !FValue.equals("")) {
                    this.BalanceResultDesc = FValue.trim();
                } else {
                    this.BalanceResultDesc = null;
                }
            }

            if (FCode.equalsIgnoreCase("Temp1")) {
                if (FValue != null && !FValue.equals("")) {
                    this.Temp1 = FValue.trim();
                } else {
                    this.Temp1 = null;
                }
            }

            if (FCode.equalsIgnoreCase("Temp2")) {
                if (FValue != null && !FValue.equals("")) {
                    this.Temp2 = FValue.trim();
                } else {
                    this.Temp2 = null;
                }
            }

            if (FCode.equalsIgnoreCase("Temp3")) {
                if (FValue != null && !FValue.equals("")) {
                    this.Temp3 = FValue.trim();
                } else {
                    this.Temp3 = null;
                }
            }

            if (FCode.equalsIgnoreCase("Temp4")) {
                if (FValue != null && !FValue.equals("")) {
                    this.Temp4 = FValue.trim();
                } else {
                    this.Temp4 = null;
                }
            }

            if (FCode.equalsIgnoreCase("Temp5")) {
                if (FValue != null && !FValue.equals("")) {
                    this.Temp5 = FValue.trim();
                } else {
                    this.Temp5 = null;
                }
            }

            if (FCode.equalsIgnoreCase("PayMode")) {
                if (FValue != null && !FValue.equals("")) {
                    this.PayMode = FValue.trim();
                } else {
                    this.PayMode = null;
                }
            }

            return true;
        }
    }

    @Override
    public String toString() {
        return "LKTransTracks{" +
                "TransID=" + TransID +
                ", ShadingID='" + ShadingID + '\'' +
                ", TransNo='" + TransNo + '\'' +
                ", AccessChnl='" + AccessChnl + '\'' +
                ", AccessChnlSub='" + AccessChnlSub + '\'' +
                ", TransCode='" + TransCode + '\'' +
                ", TransCodeOri='" + TransCodeOri + '\'' +
                ", BankCode='" + BankCode + '\'' +
                ", BankBranch='" + BankBranch + '\'' +
                ", BankNode='" + BankNode + '\'' +
                ", ContNo='" + ContNo + '\'' +
                ", ProposalNo='" + ProposalNo + '\'' +
                ", TransMony=" + TransMony +
                ", TransDate='" + TransDate + '\'' +
                ", TransTime='" + TransTime + '\'' +
                ", ContType='" + ContType + '\'' +
                ", PHConclusion='" + PHConclusion + '\'' +
                ", UWConclusion='" + UWConclusion + '\'' +
                ", SellsWay='" + SellsWay + '\'' +
                ", Perform='" + Perform + '\'' +
                ", BalanceStateCode='" + BalanceStateCode + '\'' +
                ", BalanceResult='" + BalanceResult + '\'' +
                ", BalanceResultDesc='" + BalanceResultDesc + '\'' +
                ", Temp1='" + Temp1 + '\'' +
                ", Temp2='" + Temp2 + '\'' +
                ", Temp3='" + Temp3 + '\'' +
                ", Temp4='" + Temp4 + '\'' +
                ", Temp5='" + Temp5 + '\'' +
                ", PayMode='" + PayMode + '\'' +
                ", fDate=" + fDate +
                '}';
    }
}
