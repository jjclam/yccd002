package com.sinosoft.cloud.access.net;

import com.alibaba.fastjson.JSONObject;
import com.sinosoft.cloud.access.configuration.RestfulFilterEndpoint;
import com.sinosoft.cloud.access.router.RouterBean;
import com.sinosoft.cloud.access.router.RouterFactory;
import com.sinosoft.cloud.access.transformer.XsltTrans;
import com.sinosoft.cloud.access.util.TextXml2Json;
import com.sinosoft.cloud.common.SpringContextUtils;
import com.sinosoft.lis.pubfun.EncryUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.IOException;
import java.net.ConnectException;
import java.net.SocketTimeoutException;

/**
 * abc-cloud-access
 *
 * @title: abc-cloud-access
 * @package: com.sinosoft.cloud.access.net
 * @author: yangming
 * @date: 2018/3/12 下午2:11
 */
public abstract class AbstractHandler extends AbstractNettySupportHandler {

    protected final Log logger = LogFactory.getLog(getClass());

    protected String accessName;
    protected static final String NEED_ROUTER = "@NEED_ROUTER@";

    protected AccessControl accessControl = SpringContextUtils.getBeanByClass(AccessControl.class);

    /**
     * 进行报文业务逻辑处理
     *
     * @param msg
     * @return
     */
    @Override
    public String receive(String msg) {
        if (logger.isInfoEnabled()) {
            logger.info("进入DefaultSocketHandler.receive:" + msg);
        }
        String accessName = getAccessName();

        XsltTrans xsltTrans = new XsltTrans();
        xsltTrans.setAccessName(accessName);
        String result = xsltTrans.transform(msg);
//        if (xsltTrans.getNeedRouter()) {
//            return NEED_ROUTER;
//        }
        return result;
    }

    @Override
    public void setAccessName(String accessName) {
        this.accessName = accessName;
    }

    public String getAccessName() {
        return accessName;
    }

    @Override
    public String submitData(String decryptResult) {

        String dealResult = receive(decryptResult);
        if (dealResult == null) {
            throw new RuntimeException("业务处理报文为null");
        }

        /**
         * 路由处理
         */
        String backmessage=null;
        if (NEED_ROUTER.equals(dealResult)) {
            RouterBean routerBean = RouterFactory.getInstance().getRouterBeanByAccess(getAccessName());

            logger.debug("需要进行http的路由处理");
            String url = "http://" + routerBean.getTargetIP() + ":" + routerBean.getTargetPort() + routerBean.getTargetUrl();
//            dealResult = new QuickHttp().url(url)
//                    .post().setBodyContent(RestfulFilterEndpoint.getOrgMessage()).text();
//            logger.debug("收到XXX返回的结果是：" + dealResult);

            try {
                dealResult = new HttpClient().
                        setConnectTimeOut(1).
                        setReadTimeOut(10).
                        postSend(url, RestfulFilterEndpoint.getOrgMessage());

            } catch (ConnectException e) {
                logger.debug("连接异常");
            } catch (SocketTimeoutException e) {
               // System.out.println("返回结果超时超时");
                logger.debug("返回结果超时超时");
            } catch (IOException e) {
                e.printStackTrace();
                logger.debug("远程调用失败");
            } catch (RuntimeException e) {
                logger.debug(e.getMessage().toString());
            }

        }
        String resultxml = null;
        /**
         * 针对蚂蚁平台需要加签返回数据
         */

        if(dealResult.indexOf("function")!=-1){
            try {
                resultxml = TextXml2Json.xmlToJson(dealResult);
                String message= EncryUtils.addSignered(resultxml);
                backmessage="  \"response\": "+(resultxml.substring(0,resultxml.length()-1))+",\"signature\":\""+message+"\" }";
               /* JSONObject jsonObject=new JSONObject();
                jsonObject.put("signature",message);
                backmessage= resultxml+","+jsonObject.toJSONString();*/
            } catch (Exception e) {
                e.printStackTrace();
            }finally {
                logger.info("返回蚂蚁平台已经加签的JSON：" + backmessage);
                return backmessage;
            }

        }

        logger.info("返回xxx前端未加密的报文：" + dealResult);
        return dealResult;
    }
}
