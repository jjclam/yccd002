package com.sinosoft.cloud.access.entity;

import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.List;

public class Images {
    @XStreamImplicit(itemFieldName="Node")
    private List<Node> Node;

    public List<com.sinosoft.cloud.access.entity.Node> getNode() {
        return Node;
    }

    public void setNode(List<com.sinosoft.cloud.access.entity.Node> node) {
        Node = node;
    }

    @Override
    public String toString() {
        return "Images{" +
                "Node=" + Node +
                '}';
    }
}
