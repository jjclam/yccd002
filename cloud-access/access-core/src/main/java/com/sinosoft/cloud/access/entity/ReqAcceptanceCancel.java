package com.sinosoft.cloud.access.entity;

/**
 * @Author:lizhen
 * @Description:
 * @Date:Created in 14:14 2018/11/12
 * @Modified By:
 */


/**
 * 承保前撤单NB001.请求报文
 */
public class ReqAcceptanceCancel {
    //保单号
    private String ContNo;
    //投保单号
    private String ProposalContNo;
    //撤单系统
    private String DrawSystem;
    //撤单原因编码
    private String DreasonCode;
    //撤单原因
    private String DrawReason;
    //印刷号
    private String PrtNo;

    public String getContNo() {
        return ContNo;
    }

    public void setContNo(String contNo) {
        ContNo = contNo;
    }

    public String getProposalContNo() {
        return ProposalContNo;
    }

    public void setProposalContNo(String proposalContNo) {
        ProposalContNo = proposalContNo;
    }

    public String getDrawSystem() {
        return DrawSystem;
    }

    public void setDrawSystem(String drawSystem) {
        DrawSystem = drawSystem;
    }

    public String getDreasonCode() {
        return DreasonCode;
    }

    public void setDreasonCode(String dreasonCode) {
        DreasonCode = dreasonCode;
    }

    public String getDrawReason() {
        return DrawReason;
    }

    public void setDrawReason(String drawReason) {
        DrawReason = drawReason;
    }

    public String getPrtNo() {
        return PrtNo;
    }

    public void setPrtNo(String prtNo) {
        PrtNo = prtNo;
    }

    @Override
    public String toString() {
        return "ReqAcceptanceCancel{" +
                "ContNo='" + ContNo + '\'' +
                ", ProposalContNo='" + ProposalContNo + '\'' +
                ", DrawSystem='" + DrawSystem + '\'' +
                ", DreasonCode='" + DreasonCode + '\'' +
                ", DrawReason='" + DrawReason + '\'' +
                ", PrtNo='" + PrtNo + '\'' +
                '}';
    }
}