package com.sinosoft.cloud.access.entity;

/**
 * 4003保费自垫选择权变更查询POS011.     请求
 */
public class ReqPremSelectQuery {
    //保全项目编码
    private String EdorType;
    //保单号
    private String ContNo;

    public String getEdorType() {
        return EdorType;
    }

    public void setEdorType(String edorType) {
        EdorType = edorType;
    }

    public String getContNo() {
        return ContNo;
    }

    public void setContNo(String contNo) {
        ContNo = contNo;
    }

    @Override
    public String toString() {
        return "ReqPremSelectQuery{" +
                "EdorType='" + EdorType + '\'' +
                ", ContNo='" + ContNo + '\'' +
                '}';
    }
}
