package com.sinosoft.cloud.access.entity;

/**
 * @Author:lizhen
 * @Description:
 * @Date:Created in 14:29 2018/11/13
 * @Modified By:
 */
public class UpdateInfo {
    //保单号
    private String ContNo;
    //保单送达日期
    private String GetPolDate;
    //保单送达时间
    private String GetPolTime;
    //保单回执客户签收日期
    private String CustomGetPolDate;
    //投保单号
    private String CardNo;
    //单证编码（9995-保单号）
    private String CertifyCode;
    //D+代理人工号
    private String SendOutCom;
    //D+代理人工号
    private String ReceiveCom;
    //单证回收日期
    private String TakeBackDate;
    //保单号
    private String CertifyNo;
    //代理人签收日期
    private String AgentTakeBackDate;

    public String getContNo() {
        return ContNo;
    }

    public void setContNo(String contNo) {
        ContNo = contNo;
    }

    public String getGetPolDate() {
        return GetPolDate;
    }

    public void setGetPolDate(String getPolDate) {
        GetPolDate = getPolDate;
    }

    public String getGetPolTime() {
        return GetPolTime;
    }

    public void setGetPolTime(String getPolTime) {
        GetPolTime = getPolTime;
    }

    public String getCustomGetPolDate() {
        return CustomGetPolDate;
    }

    public void setCustomGetPolDate(String customGetPolDate) {
        CustomGetPolDate = customGetPolDate;
    }

    public String getCardNo() {
        return CardNo;
    }

    public void setCardNo(String cardNo) {
        CardNo = cardNo;
    }

    public String getCertifyCode() {
        return CertifyCode;
    }

    public void setCertifyCode(String certifyCode) {
        CertifyCode = certifyCode;
    }

    public String getSendOutCom() {
        return SendOutCom;
    }

    public void setSendOutCom(String sendOutCom) {
        SendOutCom = sendOutCom;
    }

    public String getReceiveCom() {
        return ReceiveCom;
    }

    public void setReceiveCom(String receiveCom) {
        ReceiveCom = receiveCom;
    }

    public String getTakeBackDate() {
        return TakeBackDate;
    }

    public void setTakeBackDate(String takeBackDate) {
        TakeBackDate = takeBackDate;
    }

    public String getCertifyNo() {
        return CertifyNo;
    }

    public void setCertifyNo(String certifyNo) {
        CertifyNo = certifyNo;
    }

    public String getAgentTakeBackDate() {
        return AgentTakeBackDate;
    }

    public void setAgentTakeBackDate(String agentTakeBackDate) {
        AgentTakeBackDate = agentTakeBackDate;
    }

    @Override
    public String toString() {
        return "UpdateInfo{" +
                "ContNo='" + ContNo + '\'' +
                ", GetPolDate='" + GetPolDate + '\'' +
                ", GetPolTime='" + GetPolTime + '\'' +
                ", CustomGetPolDate='" + CustomGetPolDate + '\'' +
                ", CardNo='" + CardNo + '\'' +
                ", CertifyCode='" + CertifyCode + '\'' +
                ", SendOutCom='" + SendOutCom + '\'' +
                ", ReceiveCom='" + ReceiveCom + '\'' +
                ", TakeBackDate='" + TakeBackDate + '\'' +
                ", CertifyNo='" + CertifyNo + '\'' +
                ", AgentTakeBackDate='" + AgentTakeBackDate + '\'' +
                '}';
    }
}