package com.sinosoft.cloud.access.annotations;

public enum CryptoType {
    AES,DES,RSA,DSA,NONE
}
