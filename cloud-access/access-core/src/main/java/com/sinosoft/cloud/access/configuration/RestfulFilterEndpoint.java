package com.sinosoft.cloud.access.configuration;

import com.sinosoft.cloud.access.crypto.CryptoBean;
import com.sinosoft.cloud.access.crypto.CryptoFactory;
import org.apache.commons.io.IOUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerResponseFilter;
import javax.ws.rs.ext.Provider;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;

/**
 * abc-cloud-access
 *
 * @title: abc-cloud-access
 * @package: com.sinosoft.cloud.access.configuration
 * @author: yangming
 * @date: 2018/3/19 下午2:31
 */
@Service
@Provider
public class RestfulFilterEndpoint implements ContainerRequestFilter, ContainerResponseFilter {

    private final Log logger = LogFactory.getLog(this.getClass());

    private static ThreadLocal<String> ORG_MESSAGE = new ThreadLocal<String>();

    @Autowired
    CryptoFactory cryptoFactory;

    @Override
    public void filter(ContainerRequestContext requestContext) throws IOException {
        if (!requestContext.hasEntity()) {
            return;
        }
        String path = requestContext.getUriInfo().getPath();
        String accessName = path.replaceAll("/.*", "");
        InputStream inputStream = requestContext.getEntityStream();
        if (inputStream != null) {
            String jsonStr = IOUtils.toString(inputStream, Charset.defaultCharset());
            logger.debug("接收到的原始消息为：" + jsonStr);
            ORG_MESSAGE.set(jsonStr);
            if (jsonStr.startsWith("<") || jsonStr.startsWith("{")) {
                logger.debug("无需解密处理。");
                requestContext.setEntityStream(IOUtils.toInputStream(jsonStr, Charset.defaultCharset()));
            } else {
                logger.debug("需要进行解密处理");
                CryptoBean cryptoBean = cryptoFactory.getCryptoBeanByAccess(accessName);
                if (cryptoBean != null) {
                    String result = cryptoBean.decryt(jsonStr);
                    logger.debug(accessName + " 接收到解密后的消息为:" + result);
                    requestContext.setEntityStream(IOUtils.toInputStream(result, Charset.defaultCharset()));
                } else {
                    logger.debug("无法获取渠道" + accessName + "对应解密类,请检查请求参数是否正确或者初始化未成功");
                    return;
                }
            }
        }
    }

    /**
     * 返回前判断是否需要进行 加密处理
     *
     * @param requestContext
     * @param responseContext
     * @throws IOException
     */
    @Override
    public void filter(ContainerRequestContext requestContext, ContainerResponseContext responseContext) throws IOException {
        if (!responseContext.hasEntity()) {
            return;
        }
        //判断是否需要加密
        String orgMessage = getOrgMessage();

        if (orgMessage.startsWith("<") || orgMessage.startsWith("{")) {
            logger.debug("返回报文无需加密");
            return;
        }

        String path = requestContext.getUriInfo().getPath();
        String accessName = path.replaceAll("/.*", "");
        CryptoBean cryptoBean = cryptoFactory.getCryptoBeanByAccess(accessName);
        if (cryptoBean != null) {
            String oJsonStr = responseContext.getEntity().toString();
            logger.debug(accessName + " 需要进解密处理:" + oJsonStr);
            String result = cryptoBean.encryt(oJsonStr);
            logger.debug(accessName + " 加密后数据为:" + result);
            responseContext.setEntity(result);
        } else {
            logger.debug("无需加密处理");
        }
    }

    public static String getOrgMessage() {
        return ORG_MESSAGE.get();
    }
}
