package com.sinosoft.cloud.access.entity;

/**
 * @program: abc-cloud-access
 * @description: 生存给付领取方式保全项目保单列表查询POS120.
 * @author: BaoYongmeng
 * @create: 2018-12-25 11:15
 **/
public class ReqPreservePolicyListQuery {
    //保全项目编码
    private String EdorType;
    //客户号
    private String CustomerNo;
    //证件号
    private String CustomerIDNo;
    //证件姓名
    private String CustomerName;

    public String getEdorType() {
        return EdorType;
    }

    public String getCustomerNo() {
        return CustomerNo;
    }

    public String getCustomerIDNo() {
        return CustomerIDNo;
    }

    public String getCustomerName() {
        return CustomerName;
    }

    public void setEdorType(String edorType) {
        EdorType = edorType;
    }

    public void setCustomerNo(String customerNo) {
        CustomerNo = customerNo;
    }

    public void setCustomerIDNo(String customerIDNo) {
        CustomerIDNo = customerIDNo;
    }

    public void setCustomerName(String customerName) {
        CustomerName = customerName;
    }

    @Override
    public String toString() {
        return "ReqPreservePolicyListQuery{" +
                "EdorType='" + EdorType + '\'' +
                ", CustomerNo='" + CustomerNo + '\'' +
                ", CustomerIDNo='" + CustomerIDNo + '\'' +
                ", CustomerName='" + CustomerName + '\'' +
                '}';
    }
}
