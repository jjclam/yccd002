package com.sinosoft.cloud.access.entity;

import com.sinosoft.lis.pos.entity.PolicyJFTXQueryInfosPojo;

import java.util.List;

/**
 * @Author:lizhen
 * @Description:
 * @Date:Created in 15:29 2018/11/15
 * @Modified By:
 */
/**
 * 缴费提醒XQ005. 返回
 */
public class RespRenewalAdviseQuery {
    //续期业务类型
    private String BusiType;
    private List<PolicyJFTXQueryInfos> PolicyJFTXQueryInfos;

    public String getBusiType() {
        return BusiType;
    }

    public void setBusiType(String busiType) {
        BusiType = busiType;
    }

    public List<com.sinosoft.cloud.access.entity.PolicyJFTXQueryInfos> getPolicyJFTXQueryInfos() {
        return PolicyJFTXQueryInfos;
    }

    public void setPolicyJFTXQueryInfos(List<com.sinosoft.cloud.access.entity.PolicyJFTXQueryInfos> policyJFTXQueryInfos) {
        PolicyJFTXQueryInfos = policyJFTXQueryInfos;
    }

    @Override
    public String toString() {
        return "RespRenewalAdviseQuery{" +
                "BusiType='" + BusiType + '\'' +
                ", PolicyJFTXQueryInfos=" + PolicyJFTXQueryInfos +
                '}';
    }
}