package com.sinosoft.cloud.access.entity;

/**
 * abc-cloud-access
 *
 * @author lizhen
 * @create 2019/1/23
 */
public class LMriskchange {
    //输入riskcode
    private String InputRiskCode;
    //主险riskcode
    private String RiskCode;
    //返回riskcode
    private String BackRiskCode;
    private String ProdSetCode;
    private String SellType;
    private String QFlag;
    private String Bak1;
    private String Bak2;
    private String Bak3;
    private String Bak4;
    private String Bak5;

    public String getInputRiskCode() {
        return InputRiskCode;
    }

    public void setInputRiskCode(String inputRiskCode) {
        InputRiskCode = inputRiskCode;
    }

    public String getRiskCode() {
        return RiskCode;
    }

    public void setRiskCode(String riskCode) {
        RiskCode = riskCode;
    }

    public String getBackRiskCode() {
        return BackRiskCode;
    }

    public void setBackRiskCode(String backRiskCode) {
        BackRiskCode = backRiskCode;
    }

    public String getProdSetCode() {
        return ProdSetCode;
    }

    public void setProdSetCode(String prodSetCode) {
        ProdSetCode = prodSetCode;
    }

    public String getSellType() {
        return SellType;
    }

    public void setSellType(String sellType) {
        SellType = sellType;
    }

    public String getQFlag() {
        return QFlag;
    }

    public void setQFlag(String QFlag) {
        this.QFlag = QFlag;
    }

    public String getBak1() {
        return Bak1;
    }

    public void setBak1(String bak1) {
        Bak1 = bak1;
    }

    public String getBak2() {
        return Bak2;
    }

    public void setBak2(String bak2) {
        Bak2 = bak2;
    }

    public String getBak3() {
        return Bak3;
    }

    public void setBak3(String bak3) {
        Bak3 = bak3;
    }

    public String getBak4() {
        return Bak4;
    }

    public void setBak4(String bak4) {
        Bak4 = bak4;
    }

    public String getBak5() {
        return Bak5;
    }

    public void setBak5(String bak5) {
        Bak5 = bak5;
    }

    @Override
    public String toString() {
        return "LMriskchange{" +
                "InputRiskCode='" + InputRiskCode + '\'' +
                ", RiskCode='" + RiskCode + '\'' +
                ", BackRiskCode='" + BackRiskCode + '\'' +
                ", ProdSetCode='" + ProdSetCode + '\'' +
                ", SellType='" + SellType + '\'' +
                ", QFlag='" + QFlag + '\'' +
                ", Bak1='" + Bak1 + '\'' +
                ", Bak2='" + Bak2 + '\'' +
                ", Bak3='" + Bak3 + '\'' +
                ", Bak4='" + Bak4 + '\'' +
                ", Bak5='" + Bak5 + '\'' +
                '}';
    }
}