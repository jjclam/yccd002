package com.sinosoft.cloud.access.entity;

/**
 * 4003保费自垫选择权变更查询POS011. 返回
 */
public class RespPremSelectQuery {
    //保全项目编码
    private String EdorType;
    //保单号
    private String ContNo;
    //投保人姓名
    private String AppntName;
    //涉及险种
    private String RelationRisk;
    //保费自垫标记
    private String AdcanceFlag;

    public String getEdorType() {
        return EdorType;
    }

    public void setEdorType(String edorType) {
        EdorType = edorType;
    }

    public String getContNo() {
        return ContNo;
    }

    public void setContNo(String contNo) {
        ContNo = contNo;
    }

    public String getAppntName() {
        return AppntName;
    }

    public void setAppntName(String appntName) {
        AppntName = appntName;
    }

    public String getRelationRisk() {
        return RelationRisk;
    }

    public void setRelationRisk(String relationRisk) {
        RelationRisk = relationRisk;
    }

    public String getAdcanceFlag() {
        return AdcanceFlag;
    }

    public void setAdcanceFlag(String adcanceFlag) {
        AdcanceFlag = adcanceFlag;
    }

    @Override
    public String toString() {
        return "RespPremSelectQuery{" +
                "EdorType='" + EdorType + '\'' +
                ", ContNo='" + ContNo + '\'' +
                ", AppntName='" + AppntName + '\'' +
                ", RelationRisk='" + RelationRisk + '\'' +
                ", AdcanceFlag='" + AdcanceFlag + '\'' +
                '}';
    }
}
