package com.sinosoft.cloud.access.entity;

/**
 * @Author:lizhen
 * @Description:
 * @Date:Created in 20:27 2018/11/15
 * @Modified By:
 */
/**
 * 影像处理PC003. 返回
 */
public class RespImageProcessing {
    //流水号
    private String SerialNo;
    private String ABCFlag;
    private String AppendMessage;
    private PathInfos PathInfos;
    //业务类型
    private String ImgType;
    public String getSerialNo() {
        return SerialNo;
    }

    public void setSerialNo(String serialNo) {
        SerialNo = serialNo;
    }

    public String getABCFlag() {
        return ABCFlag;
    }

    public void setABCFlag(String ABCFlag) {
        this.ABCFlag = ABCFlag;
    }

    public String getAppendMessage() {
        return AppendMessage;
    }

    public void setAppendMessage(String appendMessage) {
        AppendMessage = appendMessage;
    }

    public com.sinosoft.cloud.access.entity.PathInfos getPathInfos() {
        return PathInfos;
    }

    public void setPathInfos(com.sinosoft.cloud.access.entity.PathInfos pathInfos) {
        PathInfos = pathInfos;
    }

    public String getImgType() {
        return ImgType;
    }

    public void setImgType(String imgType) {
        ImgType = imgType;
    }

    @Override
    public String toString() {
        return "RespImageProcessing{" +
                "SerialNo='" + SerialNo + '\'' +
                ", ABCFlag='" + ABCFlag + '\'' +
                ", AppendMessage='" + AppendMessage + '\'' +
                ", PathInfos=" + PathInfos +
                ", ImgType='" + ImgType + '\'' +
                '}';
    }
}