package com.sinosoft.cloud.access.entity;

import com.sinosoft.lis.pos.entity.BonusRiskPojo;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.List;

public class BonusRisks {
    @XStreamImplicit(itemFieldName="BonusRisk")
    private List<BonusRisk> BonusRisk;

    public List<com.sinosoft.cloud.access.entity.BonusRisk> getBonusRisk() {
        return BonusRisk;
    }

    public void setBonusRisk(List<com.sinosoft.cloud.access.entity.BonusRisk> bonusRisk) {
        BonusRisk = bonusRisk;
    }

    @Override
    public String toString() {
        return "BonusRisks{" +
                "BonusRisk=" + BonusRisk +
                '}';
    }


}
