package com.sinosoft.cloud.access.net;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.HashMap;

public class DecoderFactory {

    protected final Log logger = LogFactory.getLog(getClass());


    private DecoderFactory(){
    }

    private static DecoderFactory decoderFactory= new DecoderFactory();

    public static DecoderFactory getInstance() {
        return decoderFactory;
    }

    private HashMap<String,Coder> decoderHashMap = new HashMap<>();

    /**
     *
     * @param accessName
     * @param coder
     */
    public void addDecoder(String accessName,Coder coder){
        logger.debug("添加decoder: "+accessName);
        this.decoderHashMap.put(accessName, coder);
    }

    /**
     *
     * @param accessName
     * @return
     */
    public Coder getDecoder(String accessName){
        logger.debug("获取decoder:"+accessName);
        return this.decoderHashMap.get(accessName);
    }
}
