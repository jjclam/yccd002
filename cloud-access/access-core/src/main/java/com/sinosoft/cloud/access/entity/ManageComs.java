package com.sinosoft.cloud.access.entity;


import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.List;

/**
 * abc-cloud-access
 *
 * @author lizhen
 * @create 2019/1/14
 */
public class ManageComs {
    @XStreamImplicit(itemFieldName="ManageCom")
    private List<ManageCom> ManageCom;

    public List<com.sinosoft.cloud.access.entity.ManageCom> getManageCom() {
        return ManageCom;
    }

    public void setManageCom(List<com.sinosoft.cloud.access.entity.ManageCom> manageCom) {
        ManageCom = manageCom;
    }

    @Override
    public String toString() {
        return "ManageComs{" +
                "ManageCom=" + ManageCom +
                '}';
    }
}