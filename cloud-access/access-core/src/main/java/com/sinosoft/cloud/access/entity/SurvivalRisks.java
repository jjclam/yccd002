package com.sinosoft.cloud.access.entity;

import com.sinosoft.lis.pos.entity.SurvivalRiskPojo;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.List;

public class SurvivalRisks {

    @XStreamImplicit(itemFieldName="SurvivalRisk")
    private List<SurvivalRisk> SurvivalRisk;
    @Override
    public String toString() {
        return "SurvivalRisks{" +
                "SurvivalRisk=" + SurvivalRisk +
                '}';
    }

    public List<com.sinosoft.cloud.access.entity.SurvivalRisk> getSurvivalRisk() {
        return SurvivalRisk;
    }

    public void setSurvivalRisk(List<com.sinosoft.cloud.access.entity.SurvivalRisk> survivalRisk) {
        SurvivalRisk = survivalRisk;
    }
}
