package com.sinosoft.cloud.access.entity;

/**
 * @program: abc-cloud-access
 * @description: 4011-手续费与追加保费POS117.
 * @author: BaoYongmeng
 * @create: 2018-12-24 16:16
 **/
public class RespHandChargeAddPrem {
    //保全项目编码
    private String EdorType;
    //手续费
    private String ChargeFee;
    //账户减少金额
    private String AccontReduMoney;

    public void setEdorType(String edorType) {
        EdorType = edorType;
    }

    public void setChargeFee(String chargeFee) {
        ChargeFee = chargeFee;
    }

    public String getEdorType() {
        return EdorType;
    }

    public String getChargeFee() {
        return ChargeFee;
    }

    public String getAccontReduMoney() {
        return AccontReduMoney;
    }

    public void setAccontReduMoney(String accontReduMoney) {
        AccontReduMoney = accontReduMoney;
    }

    @Override
    public String toString() {
        return "RespHandChargeAddPrem{" +
                "EdorType='" + EdorType + '\'' +
                ", ChargeFee='" + ChargeFee + '\'' +
                ", AccontReduMoney='" + AccontReduMoney + '\'' +
                '}';
    }
}
