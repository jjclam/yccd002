package com.sinosoft.cloud.access.transformer;

import com.sinosoft.cloud.access.annotations.BeanCopy;
import com.sinosoft.cloud.access.entity.Body;
import com.sinosoft.cloud.rest.TradeInfo;
import com.sinosoft.lis.pos.entity.response.ResqBody;
import com.sinosoft.utility.Pojo;
import com.sinosoft.utility.Reflections;
import com.xiaoleilu.hutool.util.BeanUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.*;
import java.util.ArrayList;
import java.util.List;

import static com.alibaba.tamper.BeanMappingUtil.copy;
import static org.bouncycastle.asn1.x500.style.RFC4519Style.c;
import static org.bouncycastle.asn1.x500.style.RFC4519Style.o;

/**
 * @Description: 提取Body中的数据, 根据body的属性实现自动识别转换；
 * 提取TradeInfo的数据,根据body的属性实现自动识别转换。
 * @Author:zhaoshulei
 * @Date:2018/4/23
 */

public class PojoTrans {

    private static final Logger cLogger = LoggerFactory.getLogger(PojoTrans.class);

    private static final String ACCESS_ENTITY_PACKAGE = "com.sinosoft.cloud.access.entity";
    private static final String POJO_SUFFIX = "Pojo";

    /**
     * 将Body中的对象复制，添加到TradeInfo中
     *
     * @param body 请求数据
     * @return
     * @throws Exception
     */
    public static TradeInfo transPojos(Body body) throws Exception {
        TradeInfo tradeInfo = new TradeInfo();
        Field[] bodyFields = Body.class.getDeclaredFields();
        for (Field bodyField : bodyFields) {
            //遍历Body中带有BeanCopy注解的属性
            if (bodyField.isAnnotationPresent(BeanCopy.class)) {
                //获取私有属性
                bodyField.setAccessible(true);
                //获取属性的值，即为复制源对象
                Object fieldObject = bodyField.get(body);
                if (fieldObject != null) {
                    //复制目标Class,通过Class.forName创建实例
                    Class targetClass = null;
                    BeanCopy beanCopy = bodyField.getDeclaredAnnotation(BeanCopy.class);

                    /**
                     * @variable elementName
                     * 1.区分复制的源对象类型（List or Object）,如果该值不为""说明为List集合
                     * 2.用于生成目标实例名
                     */
                    String elementName = beanCopy.element();
                    //该条件成立，说明不是集合，属性名即是需要复制的对象名
                    if ("".equals(elementName)) {
                        elementName = bodyField.getName();
                        targetClass = Class.forName(beanCopy.targetPackage() + "." + elementName + POJO_SUFFIX);
                        //不是List集合，该属性即为复制源对象
                        Object srcObject = fieldObject;
                        //创建复制目标对象实例
                        Object targetObject = Class.forName(targetClass.getName()).newInstance();
                        Reflections reflections = new Reflections();
                        reflections.transFields(targetObject, srcObject);

//                        copyBeanInnerBean(beanCopy, fieldObject, targetObject);
                        String complicated = beanCopy.complicated();
                        if ("Y".equals(complicated)) {
                            copyBeanInnerBean4(srcObject,targetObject);
                        }

                        tradeInfo.addData(targetClass.getName(), targetObject);
                    } else {
                        targetClass = Class.forName(beanCopy.targetPackage() + "." + elementName + POJO_SUFFIX);
                        //List集合，根据属性名获取body属性对象的属性
                        Field listField = fieldObject.getClass().getDeclaredField(bodyField.getName());
                        listField.setAccessible(true);
                        //获取复制源对象集合
                        List srcObjectList = (List) listField.get(fieldObject);
                        //存放目标对象集合
                        List targetObjectList = new ArrayList();
                        if (srcObjectList != null && srcObjectList.size() > 0) {
                            for (Object srcObject : srcObjectList) {
                                //创建复制目标对象实例
                                Object targetObject = Class.forName(targetClass.getName()).newInstance();
                                Reflections reflections = new Reflections();
                                reflections.transFields(targetObject, srcObject);

                                String complicated = beanCopy.complicated();
                                if ("Y".equals(complicated)) {
                                    copyBeanInnerBean4(srcObject,targetObject);
                                }

                                targetObjectList.add(targetObject);
                            }
                        }
                        tradeInfo.addData(targetClass.getName(), targetObjectList);
                    }
                }

            }
        }
        cLogger.debug("请求数据转换成功：" + tradeInfo.toString());
        return tradeInfo;
    }


    public static void copyBeanInnerBean4(Object srcObject,Object targetObject) throws IllegalAccessException, NoSuchMethodException, InvocationTargetException, NoSuchFieldException, InstantiationException, ClassNotFoundException {
        Field[] declaredFields = srcObject.getClass().getDeclaredFields();
        for (Field declaredField : declaredFields) {
            declaredField.setAccessible(true);
            Object secondObject = declaredField.get(srcObject);

            String pojoName = null;
            String entityName = null;
            Class<?> entityClass = null;
            Object entityObj = null;

            if (secondObject instanceof String){
                String name = declaredField.getName();
                Object fieldValue = BeanUtil.getFieldValue(srcObject, name);
                Method method = targetObject.getClass().getMethod("set" + name, String.class);
                method.invoke(targetObject, fieldValue);
            }else if (secondObject instanceof List){
                List srcFieldList = (List) secondObject;
                List targetList = new ArrayList<>();
                for (Object srcFieldListInnerObj : srcFieldList){
                    pojoName = declaredField.getName();
                    entityName = pojoName+"Pojo";
                    Field listField = targetObject.getClass().getDeclaredField(entityName);
                    ParameterizedType  listGenericType  = (ParameterizedType) listField.getGenericType();
                    Type[] listActualTypeArguments = listGenericType.getActualTypeArguments();
                    String pojoFullName = listActualTypeArguments[listActualTypeArguments.length - 1].getTypeName();
                    Object pojoObj = Class.forName(pojoFullName).newInstance();

                    copyBeanInnerBean4(srcFieldListInnerObj, pojoObj);
                    targetList.add(pojoObj);
                }
                Method method = targetObject.getClass().getMethod("set" + entityName, List.class);
                method.invoke(targetObject,targetList);
            }else if (secondObject instanceof Object){
                pojoName = declaredField.getName();
                pojoName += "Pojo";

               Object thdObject = targetObject.getClass().getDeclaredField(pojoName).getType().newInstance();
                copyBeanInnerBean4(secondObject,thdObject);
                Method method = targetObject.getClass().getMethod("set" + pojoName, thdObject.getClass());
                method.invoke(targetObject, thdObject);
            }
        }
    }


    /*public static void copyBeanInnerBean2(Object orgPojo, Object fatherObj) throws ClassNotFoundException, IllegalAccessException, InstantiationException, InvocationTargetException, NoSuchMethodException, NoSuchFieldException {
        Field[] fields = fatherObj.getClass().getDeclaredFields();
        for (Field fatherObjField : fields) {
            if (fatherObjField.isAnnotationPresent(BeanCopy.class)) {

                BeanCopy fatherObjFieldBeanCopy = fatherObjField.getDeclaredAnnotation(BeanCopy.class);
                String hasChild = fatherObjFieldBeanCopy.hasChild();
                String fatherObjFieldName = fatherObjField.getName();
                Object fieldObj = Class.forName(ACCESS_ENTITY_PACKAGE + "." + fatherObjFieldName).newInstance();

                Field orgPojoField = orgPojo.getClass().getDeclaredField(fatherObjFieldName + POJO_SUFFIX);

                orgPojoField.setAccessible(true);
                Object orgFieldObj = orgPojoField.get(orgPojo);
                if (orgFieldObj != null) {

                    Reflections reflections1 = new Reflections();
                    reflections1.transFields(fieldObj, orgFieldObj);
                    Method method = fatherObj.getClass().getMethod("set" + fatherObjFieldName, fieldObj.getClass());
                    method.invoke(fatherObj, fieldObj);
                }
                if ("1".equals(hasChild)) {
                    copyBeanInnerBean2(orgFieldObj, fieldObj);
                }
            }
        }
    }*/

    public static void copyBeanInnerBean(BeanCopy currentObjInnerBeanCopy, Object currentObj, Object fatherObj) throws ClassNotFoundException, IllegalAccessException, InstantiationException, InvocationTargetException, NoSuchMethodException {
        String complicated = currentObjInnerBeanCopy.complicated();

        if ("Y".equals(complicated)) {
            Field[] cObjFields = currentObj.getClass().getDeclaredFields();
            for (Field cObjField : cObjFields) {
                if (!cObjField.isAnnotationPresent(BeanCopy.class)) {
                    continue;
                }

                String elementName = cObjField.getName();

                Class targetClass = Class.forName(currentObjInnerBeanCopy.targetPackage() + "." + elementName + POJO_SUFFIX);
                Object targetChildObj = Class.forName(targetClass.getName()).newInstance();

                BeanCopy cObjFieldInnerBeanCopy = cObjField.getDeclaredAnnotation(BeanCopy.class);
                String childHasChild = cObjFieldInnerBeanCopy.complicated();

                cObjField.setAccessible(true);
                Object cObjInnerFieldObj = cObjField.get(currentObj);
                if (cObjInnerFieldObj != null) {
                    Reflections reflections = new Reflections();
                    reflections.transFields(targetChildObj, cObjInnerFieldObj);
                    Method method = fatherObj.getClass().getMethod("set" + targetChildObj.getClass().getSimpleName(), targetChildObj.getClass());
                    method.invoke(fatherObj, targetChildObj);
                }


                if ("1".equals(childHasChild)) {
                    cObjField.setAccessible(true);
                    Object childObj = cObjField.get(currentObj);
                    copyBeanInnerBean(cObjFieldInnerBeanCopy, childObj, targetChildObj);
                }
            }
        }
    }


    /**
     * 提取tradeInfo中的数据返回
     *
     * @param tradeInfo 返回数据
     * @param body      有些请求数据需要原样返回
     * @return
     * @throws Exception
     */
    public static Body transTranData(TradeInfo tradeInfo, Body body) throws Exception {
        Field[] bodyFields = Body.class.getDeclaredFields();
        for (Field bodyField : bodyFields) {
            //遍历Body中带有BeanCopy注解的属性
            if (bodyField.isAnnotationPresent(BeanCopy.class)) {
                BeanCopy beanCopy = bodyField.getDeclaredAnnotation(BeanCopy.class);
                //body属性名，用于调用set方法
                String bodyFieldName = bodyField.getName();
                String elementName = beanCopy.element();
                if ("".equals(elementName)) { //为空 则表示是普通的pojo不是集合
                    elementName = bodyField.getName();
                }
                //复制目标Class,通过Class.forName创建实例
                Class targetClass = Class.forName(ACCESS_ENTITY_PACKAGE + "." + elementName);
                //复制源Class,通过srcClass.getName()得到key从tradeInfo中提取数据
                Class srcClass = Class.forName(beanCopy.targetPackage() + "." + elementName + POJO_SUFFIX);
                Object tradeInfoData = tradeInfo.getData(srcClass.getName());
                if (tradeInfoData != null) {
                    if (tradeInfoData instanceof List) {
                        //复数节点，如果不为""说明返回结果为集合
                        String elementsName = beanCopy.elements();
                        if (!"".equals(elementsName)) {
                            bodyFieldName = elementsName;
                        }
                        List srcObjectList = (List) tradeInfoData;
                        if (srcObjectList != null && srcObjectList.size() > 0) {
                            //目标对象集合
                            List targetObjectList = new ArrayList();
                            //Body中的属性，创建实例，存放目标对象集合
                            Object targetObjects = Class.forName(ACCESS_ENTITY_PACKAGE + "." + bodyFieldName).newInstance();
                            for (Object srcObject : srcObjectList) {
                                //创建目标对象实例
                                Object targetObject = Class.forName(targetClass.getName()).newInstance();
                                Reflections reflections = new Reflections();
                                reflections.transFields(targetObject, srcObject);

//                                copyBeanInnerBean2(srcObject, targetObject);
                                String complicated = beanCopy.complicated();
                                if ("Y".equals(complicated)) {
                                    copyBeanInnerBean3(srcObject, targetObject);
                                }

                                targetObjectList.add(targetObject);
                            }
                            //调用Body属性对象的set方法，参数类型为List
                            Method setTargetList = targetObjects.getClass().getDeclaredMethod("set" + bodyFieldName, List.class);
                            setTargetList.invoke(targetObjects, targetObjectList);
                            //调用body的set方法，参数类型为body属性类型
                            Method setTargetObjects = body.getClass().getDeclaredMethod("set" + bodyFieldName, targetObjects.getClass());
                            setTargetObjects.invoke(body, targetObjects);
                        }
                    } else {
                        if (!"".equals(elementName)) {
                            bodyFieldName = elementName;
                        }
                        Object srcObject = tradeInfoData;
                        //创建目标对象实例
                        Object targetObject = Class.forName(targetClass.getName()).newInstance();
                        Reflections reflections = new Reflections();
                        reflections.transFields(targetObject, srcObject);

//                        copyBeanInnerBean2(srcObject, targetObject);
                        String complicated = beanCopy.complicated();
                        if ("Y".equals(complicated)) {
                            copyBeanInnerBean3(srcObject, targetObject);
                        }

                        //调用body的set方法，参数类型为body属性类型
                        Method setTargetObject = body.getClass().getDeclaredMethod("set" + bodyFieldName, targetObject.getClass());
                        setTargetObject.invoke(body, targetObject);
                    }
                }
            }
        }
        cLogger.debug("返回数据转换成功：" + body.toString());
        return body;
    }

    public static void copyBeanInnerBean3(Object srcObj, Object targetObj) throws IllegalAccessException, InstantiationException, NoSuchMethodException, InvocationTargetException, ClassNotFoundException {
        Field[] srcFields = srcObj.getClass().getDeclaredFields();
        for (Field srcField : srcFields) {
            String srcFieldName = srcField.getName();
            Object srcFieldObj = BeanUtil.getFieldValue(srcObj, srcFieldName);
            srcField.setAccessible(true);

            String pojoName = null;
            String entityName = null;
            Class<?> entityClass = null;
            Object entityObj = null;


            if (srcFieldObj instanceof String) {
                Object fieldValue = BeanUtil.getFieldValue(srcObj, srcFieldName);
                Method method = targetObj.getClass().getMethod("set" + srcFieldName, String.class);
                method.invoke(targetObj, fieldValue);
            } else if (srcFieldObj instanceof com.sinosoft.lis.pos.entity.inter.Body) {
                pojoName = srcFieldObj.getClass().getSimpleName();
                entityName = pojoName.substring(0, pojoName.length() - 4);
                entityClass = Class.forName(ACCESS_ENTITY_PACKAGE + "." + entityName);
                entityObj = entityClass.newInstance();

                copyBeanInnerBean3(srcFieldObj, entityObj);

                Method method = targetObj.getClass().getMethod("set" + entityName, entityObj.getClass());
                method.invoke(targetObj, entityObj);
            } else if (srcFieldObj instanceof List) {
                List srcFieldList = (List) srcFieldObj;

                List targetList = new ArrayList<>();

                for (Object srcFieldListInnerObj : srcFieldList) {
                    pojoName = srcFieldListInnerObj.getClass().getSimpleName();
                    entityName = pojoName.substring(0, pojoName.length() - 4);
                    entityClass = Class.forName(ACCESS_ENTITY_PACKAGE + "." + entityName);
                    entityObj = entityClass.newInstance();

                    copyBeanInnerBean3(srcFieldListInnerObj, entityObj);
                    targetList.add(entityObj);
                }

                String targetObjFieldName = null;
                String srcFieldNameSub = null;

                for (Field targetObjField : targetObj.getClass().getDeclaredFields()) {
                    targetObjFieldName = targetObjField.getName();
                    Object srcFieldObj1 = BeanUtil.getFieldValue(srcObj, targetObjFieldName);

                    if (srcFieldObj1 instanceof String){
                        Object fieldValue = BeanUtil.getFieldValue(targetObj, targetObjFieldName);
                        Method method = targetObj.getClass().getMethod("set" + targetObjFieldName, String.class);
                        method.invoke(targetObj, fieldValue);
                    }else {
                        srcFieldNameSub = srcFieldName.substring(0, srcFieldName.length() - 4);
                        if (targetObjFieldName.equals(srcFieldNameSub)) {
                            Method method = targetObj.getClass().getMethod("set" + targetObjFieldName, List.class);
                            method.invoke(targetObj, targetList);
                        } else {
                            throw new RuntimeException("创建实体对象与对应的pojo名称不符合；实体对象名称：" +
                                    targetObjFieldName + ";pojo对象名称" + srcFieldName);
                        }
                    }

                }
            }
        }
    }

/**
 * 用于测试的main方法
 *
 * @param args 参数
 */
    /*public static void main(String[] args) {

//        准备测试数据
        TranData tranData = new TranData();
        Body body = new Body();
        LCCont lcCont = new LCCont();
        lcCont.setContNo("123");
        body.setLCCont(lcCont);

        LCPrems lcPrems = new LCPrems();
        LCPrem lcPrem = new LCPrem();
        lcPrem.setAppntNo("08908");
        ArrayList<LCPrem> lcPrems1 = new ArrayList<>();
        lcPrems1.add(lcPrem);
        lcPrems.setLCPrems(lcPrems1);
        body.setLCPrems(lcPrems);

        body.setContNo("jkl");

        LCAddresses lcAddresses = new LCAddresses();
        LCAddress lcAddress = new LCAddress();
        lcAddress.setCity("addr11111");
        LCAddress lcAddress1 = new LCAddress();
        lcAddress1.setCounty("addr222222");
        List<LCAddress> lcAddresses1 = new ArrayList<>();
        lcAddresses1.add(lcAddress);
        lcAddresses1.add(lcAddress1);
        lcAddresses.setLCAddresses(lcAddresses1);
        body.setLCAddresses(lcAddresses);


        tranData.setBody(body);

        PojoTrans pojoTrans = new PojoTrans();
        try {
            pojoTrans.transPojos(tranData.getBody());
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        }
    }*/
}