package com.sinosoft.cloud.access.entity;

/**
 * 续期缴费查询XQ001.
 */
public class RespRenewalFeeQuery {
    //投保人姓名
    private String AppntName;
    //投保人证件类型
    private String IDType;
    //投保人证件类型
    private String IDNo;
    //应缴期数
    private String DuePeriod;
    //应缴日期
    private String DueDate;
    //应缴金额
    private String DueAmt;
    //险种编码
    private String RiskCode;
    //保单号
    private String Contno;

    public String getAppntName() {
        return AppntName;
    }

    public void setAppntName(String appntName) {
        AppntName = appntName;
    }

    public String getIDType() {
        return IDType;
    }

    public void setIDType(String IDType) {
        this.IDType = IDType;
    }

    public String getIDNo() {
        return IDNo;
    }

    public void setIDNo(String IDNo) {
        this.IDNo = IDNo;
    }

    public String getDuePeriod() {
        return DuePeriod;
    }

    public void setDuePeriod(String duePeriod) {
        DuePeriod = duePeriod;
    }

    public String getDueDate() {
        return DueDate;
    }

    public void setDueDate(String dueDate) {
        DueDate = dueDate;
    }

    public String getDueAmt() {
        return DueAmt;
    }

    public void setDueAmt(String dueAmt) {
        DueAmt = dueAmt;
    }

    public String getRiskCode() {
        return RiskCode;
    }

    public void setRiskCode(String riskCode) {
        RiskCode = riskCode;
    }

    public String getContno() {
        return Contno;
    }

    public void setContno(String contno) {
        Contno = contno;
    }

    public String getV(String s) {
        return null;
    }

    public String getV(int i) {
        return null;
    }

    public int getFieldType(String s) {
        return 0;
    }

    public int getFieldType(int i) {
        return 0;
    }

    public int getFieldCount() {
        return 0;
    }

    public int getFieldIndex(String s) {
        return 0;
    }

    public String getFieldName(int i) {
        return null;
    }

    public boolean setV(String s, String s1) {
        return false;
    }

    @Override
    public String toString() {
        return "RespRenewalFeeQuery{" +
                "AppntName='" + AppntName + '\'' +
                ", IDType='" + IDType + '\'' +
                ", IDNo='" + IDNo + '\'' +
                ", DuePeriod='" + DuePeriod + '\'' +
                ", DueDate='" + DueDate + '\'' +
                ", DueAmt='" + DueAmt + '\'' +
                ", RiskCode='" + RiskCode + '\'' +
                ", Contno='" + Contno + '\'' +
                '}';
    }
}
