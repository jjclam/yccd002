package com.sinosoft.cloud.access.entity;

import com.thoughtworks.xstream.annotations.XStreamImplicit;
import org.apache.catalina.LifecycleState;

import java.util.List;

public class LCImageUpload {
    private String AppCode;
    private String AppName;
    private String BusiNum;
    private String DataType;


    public String getAppCode() {
        return AppCode;
    }

    public void setAppCode(String appCode) {
        AppCode = appCode;
    }

    public String getAppName() {
        return AppName;
    }

    public void setAppName(String appName) {
        AppName = appName;
    }

    public String getBusiNum() {
        return BusiNum;
    }

    public void setBusiNum(String busiNum) {
        BusiNum = busiNum;
    }

    public String getDataType() {
        return DataType;
    }

    public void setDataType(String dataType) {
        DataType = dataType;
    }

    @Override
    public String toString() {
        return "LCImageUpload{" +
                "AppCode='" + AppCode + '\'' +
                ", AppName='" + AppName + '\'' +
                ", BusiNum='" + BusiNum + '\'' +
                ", DataType='" + DataType + '\'' +
                '}';
    }
}
