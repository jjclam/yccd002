package com.sinosoft.cloud.access.entity;
/**
 * 生存给付领取方式保全项目保单明细查询POS040.
 */
public class ReqPreservePolicyDetailsQuery {

    //保全项目编码
    private String EdorType;
    //保单号
    private String ContNo;
    //业务类型
    private String BussType;

    @Override
    public String toString() {
        return "ReqPreservePolicyDetailsQuery{" +
                "EdorType='" + EdorType + '\'' +
                ", ContNo='" + ContNo + '\'' +
                ", BussType='" + BussType + '\'' +
                '}';
    }

    public String getEdorType() {
        return EdorType;
    }

    public void setEdorType(String edorType) {
        EdorType = edorType;
    }

    public String getContNo() {
        return ContNo;
    }

    public void setContNo(String contNo) {
        ContNo = contNo;
    }

    public String getBussType() {
        return BussType;
    }

    public void setBussType(String bussType) {
        BussType = bussType;
    }
}
