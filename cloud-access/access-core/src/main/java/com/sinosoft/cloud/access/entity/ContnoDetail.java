package com.sinosoft.cloud.access.entity;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("ContnoDetail")
public class ContnoDetail {

	@XStreamAlias("ContNo")
    private String ContNo;
	@XStreamAlias("ProposalPrtNo")
	private String ProposalPrtNo;
	@XStreamAlias("RiskList")
	private RiskList riskList;
	@XStreamAlias("PolicyPledge")
	private String policyPledge;
	@XStreamAlias("AcceptDate")
	private String acceptDate;
	@XStreamAlias("PolicyBgnDate")
	private String policyBgnDate;
	@XStreamAlias("PolicyEndDate")
	private String policyEndDate;
	@XStreamAlias("PolicyValue")
	private String policyValue;
	@XStreamAlias("AccountValue")
	private String accountValue;
	@XStreamAlias("RelationToAppnt")
	private String relationToAppnt;
	@XStreamAlias("InsuredName")
	private String insuredName;
	@XStreamAlias("InsuredSex")
	private String insuredSex;
	@XStreamAlias("InsuredIDType")
	private String insuredIDType;
	@XStreamAlias("InsuredIDNo")
	private String insuredIDNo;
	@XStreamAlias("InsuredBirthday")
	private String insuredBirthday;
	@XStreamAlias("BnfNum")
	private String bnfNum;
	@XStreamAlias("BnfList")
	private BnfList bnfList;
	@XStreamAlias("PayProv")
	private String payProv;
	@XStreamAlias("PayBranch")
	private String payBranch;
	@XStreamAlias("Risks")
	private Risks risks;
	public BnfList getBnfList() {
		return bnfList;
	}
	public void setBnfList(BnfList bnfList) {
		this.bnfList = bnfList;
	}
	public String getContNo() {
		return ContNo;
	}
	public void setContNo(String contNo) {
		ContNo = contNo;
	}
	public String getProposalPrtNo() {
		return ProposalPrtNo;
	}
	public void setProposalPrtNo(String proposalPrtNo) {
		ProposalPrtNo = proposalPrtNo;
	}
	public RiskList getRiskList() {
		return riskList;
	}
	public void setRiskList(RiskList riskList) {
		this.riskList = riskList;
	}
	public String getPolicyPledge() {
		return policyPledge;
	}
	public void setPolicyPledge(String policyPledge) {
		this.policyPledge = policyPledge;
	}
	public String getAcceptDate() {
		return acceptDate;
	}
	public void setAcceptDate(String acceptDate) {
		this.acceptDate = acceptDate;
	}
	public String getPolicyBgnDate() {
		return policyBgnDate;
	}
	public void setPolicyBgnDate(String policyBgnDate) {
		this.policyBgnDate = policyBgnDate;
	}
	public String getPolicyEndDate() {
		return policyEndDate;
	}
	public void setPolicyEndDate(String policyEndDate) {
		this.policyEndDate = policyEndDate;
	}
	public String getPolicyValue() {
		return policyValue;
	}
	public void setPolicyValue(String policyValue) {
		this.policyValue = policyValue;
	}
	public String getAccountValue() {
		return accountValue;
	}
	public void setAccountValue(String accountValue) {
		this.accountValue = accountValue;
	}
	public String getRelationToAppnt() {
		return relationToAppnt;
	}
	public void setRelationToAppnt(String relationToAppnt) {
		this.relationToAppnt = relationToAppnt;
	}
	public String getInsuredName() {
		return insuredName;
	}
	public void setInsuredName(String insuredName) {
		this.insuredName = insuredName;
	}
	public String getInsuredSex() {
		return insuredSex;
	}
	public void setInsuredSex(String insuredSex) {
		this.insuredSex = insuredSex;
	}
	public String getInsuredIDType() {
		return insuredIDType;
	}
	public void setInsuredIDType(String insuredIDType) {
		this.insuredIDType = insuredIDType;
	}
	public String getInsuredIDNo() {
		return insuredIDNo;
	}
	public void setInsuredIDNo(String insuredIDNo) {
		this.insuredIDNo = insuredIDNo;
	}
	public String getInsuredBirthday() {
		return insuredBirthday;
	}
	public void setInsuredBirthday(String insuredBirthday) {
		this.insuredBirthday = insuredBirthday;
	}
	public String getBnfNum() {
		return bnfNum;
	}
	public void setBnfNum(String bnfNum) {
		this.bnfNum = bnfNum;
	}

	public String getPayProv() {
		return payProv;
	}

	public void setPayProv(String payProv) {
		this.payProv = payProv;
	}

	public String getPayBranch() {
		return payBranch;
	}

	public void setPayBranch(String payBranch) {
		this.payBranch = payBranch;
	}

	public Risks getRisks() {
		return risks;
	}

	public void setRisks(Risks risks) {
		this.risks = risks;
	}

	@Override
	public String toString() {
		return "ContnoDetail{" +
				"ContNo='" + ContNo + '\'' +
				", ProposalPrtNo='" + ProposalPrtNo + '\'' +
				", riskList=" + riskList +
				", policyPledge='" + policyPledge + '\'' +
				", acceptDate='" + acceptDate + '\'' +
				", policyBgnDate='" + policyBgnDate + '\'' +
				", policyEndDate='" + policyEndDate + '\'' +
				", policyValue='" + policyValue + '\'' +
				", accountValue='" + accountValue + '\'' +
				", relationToAppnt='" + relationToAppnt + '\'' +
				", insuredName='" + insuredName + '\'' +
				", insuredSex='" + insuredSex + '\'' +
				", insuredIDType='" + insuredIDType + '\'' +
				", insuredIDNo='" + insuredIDNo + '\'' +
				", insuredBirthday='" + insuredBirthday + '\'' +
				", bnfNum='" + bnfNum + '\'' +
				", bnfList=" + bnfList +
				", payProv='" + payProv + '\'' +
				", payBranch='" + payBranch + '\'' +
				", risks=" + risks +
				'}';
	}

}
