package com.sinosoft.cloud.access.entity;

/**
 * 账户变更POS087
 */
public class RespAccountChange {
    //险种代码
    private String RiskCode;
    //保单号
    private String ContNo;

    public String getRiskCode() {
        return RiskCode;
    }

    public void setRiskCode(String riskCode) {
        RiskCode = riskCode;
    }

    public String getContNo() {
        return ContNo;
    }

    public void setContNo(String contNo) {
        ContNo = contNo;
    }

    @Override
    public String toString() {
        return "RespAccountChange{" +
                "RiskCode='" + RiskCode + '\'' +
                ", ContNo='" + ContNo + '\'' +
                '}';
    }
}
