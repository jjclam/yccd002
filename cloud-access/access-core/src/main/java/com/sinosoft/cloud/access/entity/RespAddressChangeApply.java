package com.sinosoft.cloud.access.entity;

/**
 * @Author:lizhen
 * @Description:
 * @Date:Created in 19:57 2018/11/15
 * @Modified By:
 */
/**
 * 地址信息变更更数据提交 pos070返回
 */
public class RespAddressChangeApply {
    //客户号
    private String CustomerNo;
    //客户身份证
    private String CustomerIDNo;
    //客户姓名
    private String CustomerName;
    //请求类型
    private String EdorType;
    //保单列表
    private Policys Policys;

    public String getCustomerNo() {
        return CustomerNo;
    }

    public void setCustomerNo(String customerNo) {
        CustomerNo = customerNo;
    }

    public String getCustomerIDNo() {
        return CustomerIDNo;
    }

    public void setCustomerIDNo(String customerIDNo) {
        CustomerIDNo = customerIDNo;
    }

    public String getCustomerName() {
        return CustomerName;
    }

    public void setCustomerName(String customerName) {
        CustomerName = customerName;
    }

    public String getEdorType() {
        return EdorType;
    }

    public void setEdorType(String edorType) {
        EdorType = edorType;
    }

    public com.sinosoft.cloud.access.entity.Policys getPolicys() {
        return Policys;
    }

    public void setPolicys(com.sinosoft.cloud.access.entity.Policys policys) {
        Policys = policys;
    }

    @Override
    public String toString() {
        return "RespAddressChangeApply{" +
                "CustomerNo='" + CustomerNo + '\'' +
                ", CustomerIDNo='" + CustomerIDNo + '\'' +
                ", CustomerName='" + CustomerName + '\'' +
                ", EdorType='" + EdorType + '\'' +
                ", Policys=" + Policys +
                '}';
    }
}