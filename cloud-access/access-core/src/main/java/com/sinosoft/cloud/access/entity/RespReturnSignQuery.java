package com.sinosoft.cloud.access.entity;

/**
 * 回执签收查询NB002的返回pojo
 */
public class RespReturnSignQuery {
    //投保人姓名
    private String AppntName;

    public String getAppntName() {
        return AppntName;
    }

    public void setAppntName(String appntName) {
        AppntName = appntName;
    }

    @Override
    public String toString() {
        return "RespReturnSignQuery{" +
                "AppntName='" + AppntName + '\'' +
                '}';
    }
}