package com.sinosoft.cloud.access.entity;

import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.List;

/**
 * abc-cloud-access
 *
 * @author lizhen
 * @create 2019/4/12
 */
public class BnfInfos {
    @XStreamImplicit(itemFieldName="BnfInfo")
    private List<BnfInfo> BnfInfo;

    public List<com.sinosoft.cloud.access.entity.BnfInfo> getBnfInfo() {
        return BnfInfo;
    }

    public void setBnfInfo(List<com.sinosoft.cloud.access.entity.BnfInfo> bnfInfo) {
        BnfInfo = bnfInfo;
    }

    @Override
    public String toString() {
        return "BnfInfos{" +
                "BnfInfo=" + BnfInfo +
                '}';
    }
}