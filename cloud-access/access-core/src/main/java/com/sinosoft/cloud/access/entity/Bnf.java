package com.sinosoft.cloud.access.entity;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("Bnf")
public class Bnf {


    @XStreamAlias("Type")
    private String type;
    @XStreamAlias("Grade")
    private String grade;
    @XStreamAlias("Name")
    private String name;
    @XStreamAlias("Sex")
    private String sex;
    @XStreamAlias("Birthday")
    private String birthday;
    @XStreamAlias("IDType")
    private String iDType;
    @XStreamAlias("IDNo")
    private String iDNo;
    @XStreamAlias("BnfValidityday")
    private String bnfValidityday;
    @XStreamAlias("Lot")
    private String lot;
    @XStreamAlias("RelaToInsured")
    private String relaToInsured;
    @XStreamAlias("BeneficType")
    private String beneficType;
    @XStreamAlias("Nationality")
    private String nationality;
    @XStreamAlias("Phone")
    private String phone;
    @XStreamAlias("Address")
    private String address;
    //1004需要的字段
    /** 受益人类型 */
    @XStreamAlias("BnfType")
    private String bnfType;
    /** 受益比例 */
    @XStreamAlias("BnfRate")
    private String bnfRate;
    /** 受益顺序 */
    @XStreamAlias("BnfNo")
    private String bnfNo;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getGrade() {
        return grade;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getiDType() {
        return iDType;
    }

    public void setiDType(String iDType) {
        this.iDType = iDType;
    }

    public String getiDNo() {
        return iDNo;
    }

    public void setiDNo(String iDNo) {
        this.iDNo = iDNo;
    }

    public String getBnfValidityday() {
        return bnfValidityday;
    }

    public void setBnfValidityday(String bnfValidityday) {
        this.bnfValidityday = bnfValidityday;
    }

    public String getLot() {
        return lot;
    }

    public void setLot(String lot) {
        this.lot = lot;
    }

    public String getRelaToInsured() {
        return relaToInsured;
    }

    public void setRelaToInsured(String relaToInsured) {
        this.relaToInsured = relaToInsured;
    }

    public String getBeneficType() {
        return beneficType;
    }

    public void setBeneficType(String beneficType) {
        this.beneficType = beneficType;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getBnfType() {
        return bnfType;
    }

    public void setBnfType(String bnfType) {
        this.bnfType = bnfType;
    }

    public String getBnfRate() {
        return bnfRate;
    }

    public void setBnfRate(String bnfRate) {
        this.bnfRate = bnfRate;
    }

    public String getBnfNo() {
        return bnfNo;
    }

    public void setBnfNo(String bnfNo) {
        this.bnfNo = bnfNo;
    }

    @Override
    public String toString() {
        return "Bnf{" +
                "type='" + type + '\'' +
                ", grade='" + grade + '\'' +
                ", name='" + name + '\'' +
                ", sex='" + sex + '\'' +
                ", birthday='" + birthday + '\'' +
                ", iDType='" + iDType + '\'' +
                ", iDNo='" + iDNo + '\'' +
                ", bnfValidityday='" + bnfValidityday + '\'' +
                ", lot='" + lot + '\'' +
                ", relaToInsured='" + relaToInsured + '\'' +
                ", beneficType='" + beneficType + '\'' +
                ", nationality='" + nationality + '\'' +
                '}';
    }
}
