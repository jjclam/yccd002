package com.sinosoft.cloud.access.net;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * Created by Administrator on 2017-10-25.
 */
@Component
public class AccessControl {

    /**
     * 读取配置文件，获取发生熔断机制的阈值
     */
    @Value("${persist.max.count}")
    private String threshold;

    /**
     * 密文持久化开关
     */
    @Value("${cloud.access.abc.save.cipher.open}")
    private String cipherTextSwitch;

    /**
     * 明文持久化开关
     */
    @Value("${cloud.access.abc.save.clear.open}")
    private String clearTextSwitch;

    /**
     * 返回明文持久化开关
     */
    @Value("${cloud.access.abc.save.clearOut.open}")
    private String outClearTextSwitch;

    /**
     * xxx返回消息的时间
     */
    @Value("${ribbon.ReadTimeout}")
    private long readTimeout;

    /**
     * 挡板开关，用于测试
     */
    @Value("${cloud.access.barrier.control}")
    private String barrier;

    @Value("${CipReturn.TimeOut}")
    private long CipReturnTimeOut;

    public String getThreshold() {
        return threshold;
    }

    public void setThreshold(String threshold) {
        this.threshold = threshold;
    }

    public String getCipherTextSwitch() {
        return cipherTextSwitch;
    }

    public void setCipherTextSwitch(String cipherTextSwitch) {
        this.cipherTextSwitch = cipherTextSwitch;
    }

    public String getClearTextSwitch() {
        return clearTextSwitch;
    }

    public void setClearTextSwitch(String clearTextSwitch) {
        this.clearTextSwitch = clearTextSwitch;
    }

    public String getOutClearTextSwitch() {
        return outClearTextSwitch;
    }

    public void setOutClearTextSwitch(String outClearTextSwitch) {
        this.outClearTextSwitch = outClearTextSwitch;
    }

    public String getBarrier() {
        return barrier;
    }

    public void setBarrier(String barrier) {
        this.barrier = barrier;
    }

    public long getReadTimeout() {
        return readTimeout;
    }

    public void setReadTimeout(long readTimeout) {
        this.readTimeout = readTimeout;
    }

    public long getCipReturnTimeOut() {
        return CipReturnTimeOut;
    }

    public void setCipReturnTimeOut(long cipReturnTimeOut) {
        CipReturnTimeOut = cipReturnTimeOut;
    }

    /**
     * 若开启挡板开关，则不走服务路由，不进行报文持久化，不调用任何微服务
     * @return
     */
    public Boolean getBarrierControl(){
        if ("true".equalsIgnoreCase(this.barrier)){
            this.threshold = "999";
            this.cipherTextSwitch = "false";
            this.clearTextSwitch = "false";
            this.outClearTextSwitch = "false";
            return true;
        }
        return false;
    }
}
