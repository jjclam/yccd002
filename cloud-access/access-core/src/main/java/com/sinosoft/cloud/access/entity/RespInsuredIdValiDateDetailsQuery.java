package com.sinosoft.cloud.access.entity;
/**
 * 被保人身份证有效期变更明细查询POS054.
 */
public class RespInsuredIdValiDateDetailsQuery {

    //保全项目编码
    private String EdorType;
    //姓名
    private String Name;
    //性别
    private String Sex;
    //生日
    private String Birthday;
    //证件类型
    private String IdType;
    //证件号码
    private String IdNo;

    public String getEdorType() {
        return EdorType;
    }

    public void setEdorType(String edorType) {
        EdorType = edorType;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getSex() {
        return Sex;
    }

    public void setSex(String sex) {
        Sex = sex;
    }

    public String getBirthday() {
        return Birthday;
    }

    public void setBirthday(String birthday) {
        Birthday = birthday;
    }

    public String getIdType() {
        return IdType;
    }

    public void setIdType(String idType) {
        IdType = idType;
    }

    public String getIdNo() {
        return IdNo;
    }

    public void setIdNo(String idNo) {
        IdNo = idNo;
    }

    @Override
    public String toString() {
        return "RespInsuredIdValiDateDetailsQuery{" +
                "EdorType='" + EdorType + '\'' +
                ", Name='" + Name + '\'' +
                ", Sex='" + Sex + '\'' +
                ", Birthday='" + Birthday + '\'' +
                ", IdType='" + IdType + '\'' +
                ", IdNo='" + IdNo + '\'' +
                '}';
    }
}
