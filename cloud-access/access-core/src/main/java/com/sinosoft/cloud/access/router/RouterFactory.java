package com.sinosoft.cloud.access.router;

import java.util.HashMap;

public class RouterFactory {

    private static RouterFactory routerFactory = new RouterFactory();

    public static RouterFactory getInstance() {
        return routerFactory;
    }

    private HashMap<String, RouterBean> cryptoMap = new HashMap();

    public void addRouter(String accessName, RouterBean routerBean) {
        cryptoMap.put(accessName, routerBean);
    }

    public boolean needCrypto(String accessName) {
        return cryptoMap.containsKey(accessName);
    }

    public RouterBean getRouterBeanByAccess(String accessName) {

        return cryptoMap.get(accessName);
    }
}
