package com.sinosoft.cloud.access.entity;

import com.alibaba.fastjson.annotation.JSONField;

/**
 * 4004保单贷款申请POS075
 */
public class ReqPolicyLoanApply {
    //保全项目编码
    private String EdorType;
    //保单号
    private String ContNo;
    //贷款本金
    private String LoanMoney;
    //申请人信息
    private EdorAppInfo EdorAppInfo;
    //领取人
    private PersonInfo PersonInfo;
    //xxx
    private BankInfo BankInfo;
    //收付费方式
    private String PayMode;
    //是否预约还款
    private String RepayFlag;
    //预约日期
    private String RepayDate;
    public String getEdorType() {
        return EdorType;
    }

    public void setEdorType(String edorType) {
        EdorType = edorType;
    }

    public String getContNo() {
        return ContNo;
    }

    public void setContNo(String contNo) {
        ContNo = contNo;
    }

    public String getLoanMoney() {
        return LoanMoney;
    }

    public void setLoanMoney(String loanMoney) {
        LoanMoney = loanMoney;
    }

    public com.sinosoft.cloud.access.entity.EdorAppInfo getEdorAppInfo() {
        return EdorAppInfo;
    }

    public void setEdorAppInfo(com.sinosoft.cloud.access.entity.EdorAppInfo edorAppInfo) {
        EdorAppInfo = edorAppInfo;
    }

    public com.sinosoft.cloud.access.entity.PersonInfo getPersonInfo() {
        return PersonInfo;
    }

    public void setPersonInfo(com.sinosoft.cloud.access.entity.PersonInfo personInfo) {
        PersonInfo = personInfo;
    }

    public com.sinosoft.cloud.access.entity.BankInfo getBankInfo() {
        return BankInfo;
    }

    public void setBankInfo(com.sinosoft.cloud.access.entity.BankInfo bankInfo) {
        BankInfo = bankInfo;
    }

    public String getPayMode() {
        return PayMode;
    }

    public void setPayMode(String payMode) {
        PayMode = payMode;
    }

    public String getRepayFlag() {
        return RepayFlag;
    }

    public void setRepayFlag(String repayFlag) {
        RepayFlag = repayFlag;
    }

    public String getRepayDate() {
        return RepayDate;
    }

    public void setRepayDate(String repayDate) {
        RepayDate = repayDate;
    }
}
