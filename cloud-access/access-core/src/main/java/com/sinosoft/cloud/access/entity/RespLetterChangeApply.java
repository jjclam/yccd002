package com.sinosoft.cloud.access.entity;

/**
 * @Author:lizhen
 * @Description:
 * @Date:Created in 18:21 2018/11/16
 * @Modified By:
 */
/**
 * 信函接受方式变更申请 pos068
 */
public class RespLetterChangeApply {
    private String EdorType;

    public String getEdorType() {
        return EdorType;
    }

    public void setEdorType(String edorType) {
        EdorType = edorType;
    }

    @Override
    public String toString() {
        return "RespLetterChangeApply{" +
                "EdorType='" + EdorType + '\'' +
                '}';
    }
}