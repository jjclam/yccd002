package com.sinosoft.cloud.access.annotations;

import org.springframework.stereotype.Component;

import java.lang.annotation.*;

@Component
@Inherited
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface AccessCrypto {
    /**
     * 接口名称
     *
     * @return
     */
    String name();

    /**
     * 加密算法
     *
     * @return
     */
    CryptoType cryptoType();
}
