package com.sinosoft.cloud.access.entity;

/**
 * 保单挂起响应PC005
 */
public class RespPolicyUp {
    //保单号
    private String ContNo;
    //是否挂起成功
    private String HangUpFlag;

    public String getContNo() {
        return ContNo;
    }

    public void setContNo(String contNo) {
        ContNo = contNo;
    }

    public String getHangUpFlag() {
        return HangUpFlag;
    }

    public void setHangUpFlag(String hangUpFlag) {
        HangUpFlag = hangUpFlag;
    }

    @Override
    public String toString() {
        return "RespPolicyUp{" +
                "ContNo='" + ContNo + '\'' +
                ", HangUpFlag='" + HangUpFlag + '\'' +
                '}';
    }
}
