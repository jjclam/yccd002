package com.sinosoft.cloud.access.entity;

/**
 * @Author:lizhen
 * @Description:
 * @Date:Created in 19:50 2018/11/15
 * @Modified By:
 */
public class Policy {
    //合同号码
    private String ContNo;
    //省
    private String Province;
    //市
    private String City;
    //通讯地址区/县
    private String County;
    //通讯地址
    private String PostalAddress;
    //常住地址村/社区（楼、号）
    private String StoreNo;
    //邮编
    private String ZipCode;
    //邮箱地址
    private String Email;
    //手机号
    private String Mobile;

    //主险编号
    private String MainRiskNo;
    //主险名称
    private String MainRiskName;
    //投保人姓名
    private String AppntName;
    //被保人姓名
    private String InsuredName;
    //状态
    private String Status;

    public String getContNo() {
        return ContNo;
    }

    public void setContNo(String contNo) {
        ContNo = contNo;
    }

    public String getProvince() {
        return Province;
    }

    public void setProvince(String province) {
        Province = province;
    }

    public String getCity() {
        return City;
    }

    public void setCity(String city) {
        City = city;
    }

    public String getCounty() {
        return County;
    }

    public void setCounty(String county) {
        County = county;
    }

    public String getPostalAddress() {
        return PostalAddress;
    }

    public void setPostalAddress(String postalAddress) {
        PostalAddress = postalAddress;
    }

    public String getStoreNo() {
        return StoreNo;
    }

    public void setStoreNo(String storeNo) {
        StoreNo = storeNo;
    }

    public String getZipCode() {
        return ZipCode;
    }

    public void setZipCode(String zipCode) {
        ZipCode = zipCode;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getMobile() {
        return Mobile;
    }

    public void setMobile(String mobile) {
        Mobile = mobile;
    }

    public String getMainRiskNo() {
        return MainRiskNo;
    }

    public void setMainRiskNo(String mainRiskNo) {
        MainRiskNo = mainRiskNo;
    }

    public String getMainRiskName() {
        return MainRiskName;
    }

    public void setMainRiskName(String mainRiskName) {
        MainRiskName = mainRiskName;
    }

    public String getAppntName() {
        return AppntName;
    }

    public void setAppntName(String appntName) {
        AppntName = appntName;
    }

    public String getInsuredName() {
        return InsuredName;
    }

    public void setInsuredName(String insuredName) {
        InsuredName = insuredName;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

    @Override
    public String toString() {
        return "Policy{" +
                "ContNo='" + ContNo + '\'' +
                ", Province='" + Province + '\'' +
                ", City='" + City + '\'' +
                ", County='" + County + '\'' +
                ", PostalAddress='" + PostalAddress + '\'' +
                ", StoreNo='" + StoreNo + '\'' +
                ", ZipCode='" + ZipCode + '\'' +
                ", Email='" + Email + '\'' +
                ", Mobile='" + Mobile + '\'' +
                ", MainRiskNo='" + MainRiskNo + '\'' +
                ", MainRiskName='" + MainRiskName + '\'' +
                ", AppntName='" + AppntName + '\'' +
                ", InsuredName='" + InsuredName + '\'' +
                ", Status='" + Status + '\'' +
                '}';
    }
}