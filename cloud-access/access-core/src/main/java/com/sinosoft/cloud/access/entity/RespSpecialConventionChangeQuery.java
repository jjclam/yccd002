package com.sinosoft.cloud.access.entity;

/**
 * 特别约定（出境告知）保全项目保单明细查询POS080
 */
public class RespSpecialConventionChangeQuery {
    //保全项目编码
    private String EdorType;
    //保单号
    private String ContNo;
    //特别约定信息
    private String SPMessage;

    @Override
    public String toString() {
        return "RespSpecialConventionChangeQuery{" +
                "EdorType='" + EdorType + '\'' +
                ", ContNo='" + ContNo + '\'' +
                ", SPMessage='" + SPMessage + '\'' +
                '}';
    }

    public String getEdorType() {
        return EdorType;
    }

    public void setEdorType(String edorType) {
        EdorType = edorType;
    }

    public String getContNo() {
        return ContNo;
    }

    public void setContNo(String contNo) {
        ContNo = contNo;
    }

    public String getSPMessage() {
        return SPMessage;
    }

    public void setSPMessage(String SPMessage) {
        this.SPMessage = SPMessage;
    }

    public String getV(String s) {
        return null;
    }

    public String getV(int i) {
        return null;
    }

    public int getFieldType(String s) {
        return 0;
    }

    public int getFieldType(int i) {
        return 0;
    }

    public int getFieldCount() {
        return 0;
    }

    public int getFieldIndex(String s) {
        return 0;
    }

    public String getFieldName(int i) {
        return null;
    }

    public boolean setV(String s, String s1) {
        return false;
    }
}
