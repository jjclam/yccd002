package com.sinosoft.cloud.access.entity;

/**
 * @Author:lizhen
 * @Description:
 * @Date:Created in 14:09 2018/11/1
 * @Modified By:
 */
public class CaluParamInfo {
    private String riskCode;
    private String caluParamCode;
    private String caluParamValue;

//    public CaluParamInfo(String riskCode, String caluParamCode, String caluParamValue) {
//        this.riskCode = riskCode;
//        this.caluParamCode = caluParamCode;
//        this.caluParamValue = caluParamValue;
//    }

    public String getRiskCode() {
        return riskCode;
    }

    public void setRiskCode(String riskCode) {
        this.riskCode = riskCode;
    }

    public String getCaluParamValue() {
        return caluParamValue;
    }

    public void setCaluParamValue(String caluParamValue) {
        this.caluParamValue = caluParamValue;
    }

    public String getCaluParamCode() {
        return caluParamCode;
    }

    public void setCaluParamCode(String caluParamCode) {
        this.caluParamCode = caluParamCode;
    }

    @Override
    public String toString() {
        return "CaluParamInfo{" +
                "riskCode='" + riskCode + '\'' +
                ", caluParamCode='" + caluParamCode + '\'' +
                ", caluParamValue='" + caluParamValue + '\'' +
                '}';
    }
}