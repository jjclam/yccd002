package com.sinosoft.cloud.access.entity;

/**
 * 保全申请状态查询POS085
 */
public class RespPreservationStateQuery {
    //保单状态
    private String PolicyStatus;
    //保单号
    private String ContNo;
    //保全申请状态
    private String BqStatus;
    //业务类别
    private String EdorType;
    //申请日期
    private String ApplyDate;
    //险种名称
    private String RiskCode;
    //生效日期
    private String ValiDate;

    public String getPolicyStatus() {
        return PolicyStatus;
    }

    public void setPolicyStatus(String policyStatus) {
        PolicyStatus = policyStatus;
    }

    public String getContNo() {
        return ContNo;
    }

    public void setContNo(String contNo) {
        ContNo = contNo;
    }

    public String getBqStatus() {
        return BqStatus;
    }

    public void setBqStatus(String bqStatus) {
        BqStatus = bqStatus;
    }

    public String getEdorType() {
        return EdorType;
    }

    public void setEdorType(String edorType) {
        EdorType = edorType;
    }

    public String getApplyDate() {
        return ApplyDate;
    }

    public void setApplyDate(String applyDate) {
        ApplyDate = applyDate;
    }

    public String getRiskCode() {
        return RiskCode;
    }

    public void setRiskCode(String riskCode) {
        RiskCode = riskCode;
    }

    public String getValiDate() {
        return ValiDate;
    }

    public void setValiDate(String valiDate) {
        ValiDate = valiDate;
    }

    @Override
    public String toString() {
        return "RespPreservationStateQuery{" +
                "PolicyStatus='" + PolicyStatus + '\'' +
                ", ContNo='" + ContNo + '\'' +
                ", BqStatus='" + BqStatus + '\'' +
                ", EdorType='" + EdorType + '\'' +
                ", ApplyDate='" + ApplyDate + '\'' +
                ", RiskCode='" + RiskCode + '\'' +
                ", ValiDate='" + ValiDate + '\'' +
                '}';
    }
}
