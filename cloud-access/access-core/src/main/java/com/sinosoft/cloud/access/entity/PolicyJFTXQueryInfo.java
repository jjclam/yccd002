package com.sinosoft.cloud.access.entity;

/**
 * @Author:lizhen
 * @Description:
 * @Date:Created in 15:23 2018/11/15
 * @Modified By:
 */
public class PolicyJFTXQueryInfo {
    //保单号
    private String ContNo;
    //支付日期
    private String PayDate;
    //保费
    private String Prem;
    //xxx名称
    private String BankName;
    //xxx账户号
    private String BankAccNo;
    //代理人姓名
    private String AgentName;
    //代理人电话
    private String AgentPhone;

    public String getContNo() {
        return ContNo;
    }

    public void setContNo(String contNo) {
        ContNo = contNo;
    }

    public String getPayDate() {
        return PayDate;
    }

    public void setPayDate(String payDate) {
        PayDate = payDate;
    }

    public String getPrem() {
        return Prem;
    }

    public void setPrem(String prem) {
        Prem = prem;
    }

    public String getBankName() {
        return BankName;
    }

    public void setBankName(String bankName) {
        BankName = bankName;
    }

    public String getBankAccNo() {
        return BankAccNo;
    }

    public void setBankAccNo(String bankAccNo) {
        BankAccNo = bankAccNo;
    }

    public String getAgentName() {
        return AgentName;
    }

    public void setAgentName(String agentName) {
        AgentName = agentName;
    }

    public String getAgentPhone() {
        return AgentPhone;
    }

    public void setAgentPhone(String agentPhone) {
        AgentPhone = agentPhone;
    }

    @Override
    public String toString() {
        return "PolicyJFTXQueryInfo{" +
                "ContNo='" + ContNo + '\'' +
                ", PayDate='" + PayDate + '\'' +
                ", Prem='" + Prem + '\'' +
                ", BankName='" + BankName + '\'' +
                ", BankAccNo='" + BankAccNo + '\'' +
                ", AgentName='" + AgentName + '\'' +
                ", AgentPhone='" + AgentPhone + '\'' +
                '}';
    }
}