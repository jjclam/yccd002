package com.sinosoft.cloud.access.entity;

/**
 * 4002保单万能账户部分领取查询POS029.POS104
 */
public class RespAccountHalfQuery {

    //保全项目编码
    private String EdorType;
    private PolicyQueryInfos PolicyQueryInfos;

    public String getEdorType() {
        return EdorType;
    }

    public void setEdorType(String edorType) {
        EdorType = edorType;
    }

    public com.sinosoft.cloud.access.entity.PolicyQueryInfos getPolicyQueryInfos() {
        return PolicyQueryInfos;
    }

    public void setPolicyQueryInfos(com.sinosoft.cloud.access.entity.PolicyQueryInfos policyQueryInfos) {
        PolicyQueryInfos = policyQueryInfos;
    }

    @Override
    public String toString() {
        return "RespAccountHalfQuery{" +
                "EdorType='" + EdorType + '\'' +
                ", PolicyQueryInfos=" + PolicyQueryInfos +
                '}';
    }
}
