package com.sinosoft.cloud.access.entity;

import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.List;

/**
 * Created by Administrator on 2018/4/16.
 */
public class LCCustomerImpartDetails {
    @XStreamImplicit(itemFieldName="LCCustomerImpartDetail")
    private List<LCCustomerImpartDetail> LCCustomerImpartDetails;

    public List<LCCustomerImpartDetail> getLCCustomerImpartDetails() {
        return LCCustomerImpartDetails;
    }

    public void setLCCustomerImpartDetails(List<LCCustomerImpartDetail> LCCustomerImpartDetails) {
        this.LCCustomerImpartDetails = LCCustomerImpartDetails;
    }

    @Override
    public String toString() {
        return "LCCustomerImpartDetails{" +
                "LCCustomerImpartDetails=" + LCCustomerImpartDetails +
                '}';
    }
}
