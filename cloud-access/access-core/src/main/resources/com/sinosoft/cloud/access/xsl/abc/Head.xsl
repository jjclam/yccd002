<?xml version="1.0" encoding="GBK"?>

<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:java="http://xml.apache.org/xslt/java"
                exclude-result-prefixes="java">

    <xsl:template match="ABCB2I">
        <Head>
            <TranDate><!-- 交易日期 -->
                <xsl:value-of select="Header/TransDate"/>
            </TranDate>
            <TranTime><!-- 交易时间 -->
                <xsl:value-of select="Header/TransTime"/>
            </TranTime>
            <TellerNo><!-- 柜员代码 -->
                <xsl:value-of select="Header/Tlid"/>
            </TellerNo>
            <TransCode>
                <xsl:value-of select="Header/TransCode"/>
            </TransCode>
            <TranNo><!-- 交易流水号 -->
                <xsl:value-of select="Header/SerialNo"/>
            </TranNo>
            <NodeNo><!-- 网点代码 -->
                <xsl:value-of select="Header/ProvCode"/>
                <xsl:value-of select="Header/BranchNo"/>
            </NodeNo>
            <TranCom>2</TranCom>
            <xsl:if test="Header/EntrustWay = 11">
                <BankCode>05</BankCode>
            </xsl:if>
            <xsl:if test="Header/EntrustWay != 11">
                <BankCode>0501</BankCode>
            </xsl:if>
            <FuncFlag>201</FuncFlag>
            <AgentCom></AgentCom>
            <AgentCode>-</AgentCode>
            <BankClerk>
                <xsl:value-of select="App/Req/Base/Saler"/>
            </BankClerk><!-- xxx柜员姓名 -->
            <SourceType>
                <xsl:call-template name="tran_Type">
                    <xsl:with-param name="Type">
                        <xsl:value-of select="Header/EntrustWay"/>
                    </xsl:with-param>
                </xsl:call-template>
            </SourceType>
        </Head>
    </xsl:template>

    <!-- 渠道 -->
    <xsl:template name="tran_Type">
        <xsl:param name="Type"></xsl:param>
        <xsl:if test="$Type = 11">ABC</xsl:if>
        <xsl:if test="$Type = 01">ABC_EBank</xsl:if>
        <xsl:if test="$Type = 04">ABC_SST</xsl:if>
        <xsl:if test="$Type = 02">ABC_Mobile</xsl:if>
    </xsl:template>

</xsl:stylesheet>