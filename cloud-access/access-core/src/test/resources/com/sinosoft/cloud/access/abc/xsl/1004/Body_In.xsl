<?xml version="1.0" encoding="GBK"?>

<xsl:stylesheet version="1.0"
				xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
				xmlns:java="http://xml.apache.org/xslt/java"
				exclude-result-prefixes="java">

	<xsl:template match="ABCB2I">
		<TranData>
			<!-- 报文体 -->
			<Body>
				<!-- 试算申请顺序号 -->
				<AppNo><xsl:value-of select="App/Req/ApplySerial"/></AppNo>
				<!-- 应交保险费 -->
				<PayPrem><xsl:value-of select="App/Req/PayAmt"/></PayPrem>
				<!-- 交费账户 -->
				<AccNo><xsl:value-of select="App/Req/PayAccount"/></AccNo>
				<!-- 保险公司方险种代码 -->
				<RiskCode><xsl:value-of select="App/Req/RiskCode"/></RiskCode>
				<!-- 保单号 -->
				<ContNo><xsl:value-of select="App/Req/PolicyNo"/></ContNo>
				<!-- 交费账户户名 -->
				<XAccName><xsl:value-of select="App/Req/AccName"/></XAccName>
				<!-- 是否开户行标志 -->
				<OpenBranchFlag><xsl:value-of select="App/Req/OpenBranchFlag"/></OpenBranchFlag>
				<OldTranno>
					<xsl:value-of select="App/Req/ApplySerial"/>
				</OldTranno>
			</Body>
		</TranData>
	</xsl:template>

</xsl:stylesheet>