<?xml version="1.0" encoding="GBK"?>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:java="http://xml.apache.org/xslt/java" exclude-result-prefixes="java">
<xsl:template match="/TranData">
<ABCB2I>
		<Header>
		    <xsl:if test="Head/Flag=0">
	  	    	<RetCode>000000</RetCode>
	     		<RetMsg>交易成功</RetMsg>
	  		</xsl:if>
	   		<xsl:if test="Head/Flag!=0">
	     		<RetCode>009999</RetCode>
	    		<RetMsg><xsl:value-of select="Head/Desc"/></RetMsg>
	  		</xsl:if>
			<SerialNo>
				<xsl:value-of select="Head/TranNo"/>
			</SerialNo>
			<InsuSerial>
				<xsl:value-of select="Head/InsuSerial"/>
			</InsuSerial>
			<TransDate>
				<xsl:value-of select="Head/TranDate"/>
			</TransDate>
			<TranTime>
				<xsl:value-of select="Head/TranTime"/>
			</TranTime>
			<BankCode>
				<xsl:value-of select="Head/BankCode"/>
			</BankCode>
			<CorpNo>
				<xsl:value-of select="Head/CorpNo"/>
			</CorpNo>
			<TransCode>
				<xsl:value-of select="Head/TransCode"/>
			</TransCode>
	  	</Header>
	  	<App>
        	<Ret>
          	 	 <OrgSerialNo><xsl:value-of select="Body/OldTranno"/></OrgSerialNo><!--原交易编码-->
           		 <OrgTransDate><xsl:value-of select="Body/OrgTransDate"/></OrgTransDate><!--原交易日期-->
            	 <TransCode><xsl:value-of select="Body/OrgTransCode"/></TransCode><!-- 所冲正交易编码 -->
        	</Ret>
    </App>
</ABCB2I>
</xsl:template>
</xsl:stylesheet>