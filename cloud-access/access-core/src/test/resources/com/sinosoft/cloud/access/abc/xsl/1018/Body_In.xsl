<?xml version="1.0" encoding="GBK"?>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:java="http://xml.apache.org/xslt/java" exclude-result-prefixes="java">

<xsl:template match="ABCB2I">
<TranData>
    <Head>
		<TranDate>
			<xsl:value-of select="Header/TransDate" />
		</TranDate>
		<TranTime>
			<xsl:value-of select="Header/TransTime" />
		</TranTime>
		<TellerNo>nonghang</TellerNo>
		<TranNo>
			<xsl:value-of select="Header/SerialNo" />
		</TranNo>
		<NodeNo>
			<xsl:value-of select="Header/ProvCode" />
			<xsl:value-of select="Header/BranchNo" />
		</NodeNo>
		<TranCom>2</TranCom>
		<BankCode>02</BankCode>
		<FuncFlag>102</FuncFlag>
		<AgentCom></AgentCom>
		<AgentCode>-</AgentCode>
	</Head>
	<Body>
        <RiskCode><xsl:value-of select="App/Req/InsuCode" /></RiskCode>
        <ProposalPrtNo><xsl:value-of select="App/Req/PolicyApplyNo" /></ProposalPrtNo>
		<ContPrtNo><xsl:value-of select="App/Req/NewVchNo" /></ContPrtNo>
		<OldContPrtNo><xsl:value-of select="App/Req/OldVchNo"/></OldContPrtNo>
	</Body>
 </TranData>
 </xsl:template>
</xsl:stylesheet>
