<?xml version="1.0" encoding="GBK"?>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:java="http://xml.apache.org/xslt/java" exclude-result-prefixes="java">
    <xsl:template match="/TranData">
        <ABCB2I>
            <Header>
                <xsl:if test="Head/Flag=0">
                    <RetCode>000000</RetCode>
                    <RetMsg>交易成功</RetMsg>
                </xsl:if>
                <xsl:if test="Head/Flag!=0">
                    <RetCode>009999</RetCode>
                    <RetMsg><xsl:value-of select="Head/Desc"/></RetMsg>
                </xsl:if>
                <SerialNo></SerialNo>
                <InsuSerial></InsuSerial>
                <TransDate><xsl:value-of select="Head/TranDate"/></TransDate>
                <BankCode>01</BankCode>
                <CorpNo></CorpNo>
                <TransCode></TransCode>
            </Header>
            <App>
                <Ret>
                    <PolicyNo><xsl:value-of select="Body/ContNo"/></PolicyNo>
                    <RiskCode><xsl:value-of select="Body/Risk/MainRiskCode"/></RiskCode>
                    <RiskName><xsl:value-of select="Body/Risk/RiskName"/></RiskName>
                    <PolicyStatus><xsl:value-of select="Body/ContState"/></PolicyStatus><!-- 保单状态 -->
                    <PolicyPledge><xsl:value-of select="Body/Pledge"/></PolicyPledge><!-- 保单质押状态 -->
                    <AcceptDate><xsl:value-of select="Body/Risk/SignDate"/></AcceptDate><!-- 受理日期 -->
                    <PolicyBgnDate><xsl:value-of select="Body/Risk/CValiDate"/></PolicyBgnDate><!-- 保单生效日 -->
                    <PolicyEndDate><xsl:value-of select="Body/Risk/InsuEndDate"/></PolicyEndDate><!-- 保单到期日 -->
                    <PolicyAmmount><xsl:value-of select="Body/Risk/Mult"/></PolicyAmmount><!-- 投保份数 -->
                    <Amt><xsl:value-of select="java:com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.fenToYuan(Body/Prem)"/></Amt><!-- baofei -->
                    <Beamt><xsl:value-of select="java:com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.fenToYuan(Body/Amnt)"/></Beamt>
                    <!--<Amt><xsl:value-of select="(Body/Prem)"/></Amt>&lt;!&ndash; baofei &ndash;&gt;-->
                    <!--<Beamt><xsl:value-of select="(Body/Amnt)"/></Beamt>-->

                    <PolicyValue><xsl:value-of select="Body/ContValue"/></PolicyValue><!-- 保单价值 -->
                    <AutoTransferAccNo><xsl:value-of select="Body/XBankAccNo"/></AutoTransferAccNo><!-- 自动转账授权账号 -->
                    <prt1></prt1>
                    <prt2></prt2>
                    <prt3></prt3>
                    <prt4></prt4>
                    <prt5></prt5>
                    <prt6></prt6>
                </Ret>
            </App>
        </ABCB2I>
    </xsl:template>
</xsl:stylesheet>