<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:java="http://xml.apache.org/xslt/java" exclude-result-prefixes="java">
	<xsl:output method="xml" indent="yes"/>
	<!--自动冲正交易-->
	<xsl:template match="ABCB2I">
		<TranData>
			<Body>
			<ContNo>
				<xsl:value-of select="App/Req/PolicyNo"/>
			</ContNo>
			<!--自增字段  返回时要用-->
			<OldTranno>
				<xsl:value-of select="App/Req/OrgSerialNo"/>
			</OldTranno><!--原交易流水号-->
			<OrgTransDate>
				<xsl:value-of select="App/Req/OrgTransDate"/>
			</OrgTransDate><!--原交易编码-->
			<OrgTransCode>
				<xsl:value-of select="App/Req/TransCode"/>
			</OrgTransCode><!--原交易编码-->
			</Body>
		</TranData>
	</xsl:template>
</xsl:stylesheet>
