<?xml version="1.0" encoding="GBK"?>

<xsl:stylesheet version="3.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:java="http://xml.apache.org/xslt/java"
	exclude-result-prefixes="java">

	<xsl:template match="ABCB2I">
		<TranData>
			<xsl:variable name="Insu" select="App/Req/Insu"/>
			<xsl:variable name="Header" select="Header"/>
		<!-- 报文体 -->
			<Body>
				<AgentComName><xsl:value-of select="App/Req/Base/BranchName" /></AgentComName>
				<AgentComSellNo><xsl:value-of select="App/Req/Base/BranchCertNo" /></AgentComSellNo>
				<AgentPersonSellNo><xsl:value-of select="App/Req/Base/Saler" /></AgentPersonSellNo>
				<AgentPersonCode><xsl:value-of select="App/Req/Base/SalerCertNo" /></AgentPersonCode>
				<AppNo><xsl:value-of select="App/Req/AppNo" /></AppNo>
				<ProposalPrtNo><!-- 投保单号 -->
					<xsl:value-of select="App/Req/Base/PolicyApplySerial" />
				</ProposalPrtNo>
				<PolApplyDate><!-- 保单投保日期 -->
					<xsl:value-of select="App/Req/Base/ApplyDate" />
				</PolApplyDate>
				<ContPrtNo><xsl:value-of select="App/Req/Base/VchNo" /></ContPrtNo><!-- 单证号码 -->
				<HealthNotice>
					<xsl:call-template name="tran_Health">
							<xsl:with-param name="Health">
								<xsl:value-of select="App/Req/Insu/HealthNotice" />
							</xsl:with-param>
						</xsl:call-template>
				</HealthNotice>
				<JobNotice></JobNotice><!-- 职业告知 xxx不传 -->
				<AccName><xsl:value-of select="App/Req/Appl/Name" /></AccName><!-- 账户姓名 -->
				<!-- 未找到匹配 -->
				<AccNo><xsl:value-of select="App/Req/Base/ConAccNo"/></AccNo><!-- xxx账号  与核心曹增，张建东商量用续期缴费账号覆盖首期账号 -->
				<xsl:if test="Header/EntrustWay = 01">
					<PayMode>C</PayMode>
				</xsl:if>
				<xsl:if test="Header/EntrustWay = 04">
					<PayMode>C</PayMode>
				</xsl:if>
				<xsl:if test="Header/EntrustWay = 11">
					<PayMode>A</PayMode>
				</xsl:if>
				<xsl:if test="Header/EntrustWay = 02">
					<PayMode>A</PayMode>
				</xsl:if>
				<XBankCode>02</XBankCode>
				<XAccName><xsl:value-of select="App/Req/Base/ConAccName"/></XAccName>
				<!-- 续期缴费账号 -->
				<XBankAccNo>
					<xsl:value-of select="App/Req/Base/ConAccNo"/>
				</XBankAccNo>
				<SpecContent>
					<xsl:value-of select="App/Req/Base/SpecArranged"/>
				</SpecContent><!-- 特别约定 -->
				<TransferFlag>
					<xsl:value-of select="App/Req/Base/TransferFlag"/>
				</TransferFlag><!-- 转保标志 -->
				<OtherCompanyDieAmnt>
					<xsl:value-of select="App/Req/Reserve4"/>
				</OtherCompanyDieAmnt><!-- 其他公司身故保额 -->
				<!-- 投保人 -->
				<Appnt>
					<Name>
						<xsl:value-of select="App/Req/Appl/Name" />
					</Name>
					<Sex>
						<xsl:call-template name="tran_sex">
							<xsl:with-param name="sex">
								<xsl:value-of select="App/Req/Appl/Sex" />
							</xsl:with-param>
						</xsl:call-template>
					</Sex>
					<Birthday>
						<xsl:value-of select="App/Req/Appl/Birthday" />
					</Birthday>
					<IDType><!-- 投保人证件类型 -->
						<xsl:call-template name="tran_idtype">
							<xsl:with-param name="idtype">
								<xsl:value-of select="App/Req/Appl/IDKind" />
							</xsl:with-param>
						</xsl:call-template>
					</IDType>
					<IDNo><!-- 投保人证件号 -->
						<xsl:value-of select="App/Req/Appl/IDCode" />
					</IDNo>
					<xsl:choose>
						<xsl:when test="App/Req/Appl/InvalidDate = '99999999'">
							<AppntValidityday>长期</AppntValidityday>
						</xsl:when>
						<xsl:when test="App/Req/Appl/InvalidDate = '99991231'">
							<AppntValidityday>长期</AppntValidityday>
						</xsl:when>
						<xsl:otherwise>
							<AppntValidityday><xsl:value-of select="App/Req/Appl/InvalidDate" /></AppntValidityday><!-- 被保人证件有效期 -->
						</xsl:otherwise>
					</xsl:choose><!-- 投保人证件有效期 -->
					<!--<JobCode><xsl:value-of select="App/Req/Appl/JobCode" /></JobCode>-->
					<Nationality><!-- 投保人国籍 -->
						<xsl:call-template name="tran_nationality">
							<xsl:with-param name="Flag" select="App/Req/Header/EntrustWay"/>
							<xsl:with-param name="nationality" select="App/Req/Appl/Country"/>
						</xsl:call-template>
					</Nationality>
					<Address>
						<xsl:value-of select="App/Req/Appl/Address" />
					</Address>
					<ZipCode><!-- 邮编 -->
						<xsl:value-of select="App/Req/Appl/ZipCode" />
					</ZipCode>
					<Mobile>
						<xsl:value-of select="App/Req/Appl/Mobile" />
					</Mobile>
					<Phone>
						<xsl:value-of select="App/Req/Appl/Phone" />
					</Phone>
					<Email><xsl:value-of select="App/Req/Appl/Email" /></Email>
					<!-- YBT投保人年收入 -->
					<xsl:variable name="tType" select="Header/EntrustWay" />
					<xsl:if test = "$tType = '11' or $tType='04'">
						<xsl:variable name="tIncome" select="App/Req/Appl/AnnualIncome" />
						<xsl:if test = "$tIncome = ''">
							 <Salary></Salary>
						</xsl:if>
						<xsl:if test = "$tIncome != ''">
							 <Salary><xsl:value-of select="$tIncome div 10000" /></Salary>
						</xsl:if>
						<xsl:variable name="tFamilyIncome" select="App/Req/Appl/AnnualIncome" />
						<xsl:if test = "$tFamilyIncome != ''">
							<FamilySalary><xsl:value-of select="$tFamilyIncome  div 10000"/></FamilySalary>
						</xsl:if>
						<xsl:if test = "$tFamilyIncome = ''">
							<FamilySalary></FamilySalary>
						</xsl:if>
					</xsl:if>
					<!-- WY投保人年收入 -->
					<xsl:if test = "$tType != '11' and $tType != '04'">
						<xsl:variable name="tIncome" select="App/Req/Appl/AnnualIncome" />
						<xsl:if test = "$tIncome = ''">
							 <Salary></Salary>
						</xsl:if>
						<xsl:if test = "$tIncome = '5万以下'">
							 <Salary>49000</Salary>
						</xsl:if>
						<xsl:if test = "$tIncome = '5-10万'">
							 <Salary>99000</Salary>
						</xsl:if>
						<xsl:if test = "$tIncome = '10-30万'">
							 <Salary>299000</Salary>
						</xsl:if>
						<xsl:if test = "$tIncome = '30-50万'">
							 <Salary>499000</Salary>
						</xsl:if>
						<xsl:if test = "$tIncome = '50万以上'">
							 <Salary>5000000</Salary>
						</xsl:if>
						<xsl:variable name="tFamilyIncome" select="App/Req/Appl/AnnualIncome" />
						<xsl:if test = "$tFamilyIncome = ''">
							<FamilySalary></FamilySalary>
						</xsl:if>
						<xsl:if test = "$tFamilyIncome = '5万以下'">
							<FamilySalary>49000</FamilySalary>
						</xsl:if>
						<xsl:if test = "$tFamilyIncome = '5-10万'">
							<FamilySalary>99000</FamilySalary>
						</xsl:if>
						<xsl:if test = "$tFamilyIncome = '10-30万'">
							<FamilySalary>299000</FamilySalary>
						</xsl:if>
						<xsl:if test = "$tFamilyIncome = '30-50万'">
							<FamilySalary>499000</FamilySalary>
						</xsl:if>
						<xsl:if test = "$tFamilyIncome = '50万以上'">
							<FamilySalary>5000000</FamilySalary>
						</xsl:if>
					</xsl:if>
					<LiveZone>
						<xsl:call-template name="tran_appRgtType">
							<xsl:with-param name="appntRgtType" select="App/Req/Appl/CustSource"/>
						</xsl:call-template>
					</LiveZone>
					<RelaToInsured><!-- 投保人与被保人关系 -->
						<xsl:call-template name="tran_rela">
							<!--<xsl:with-param name="sex" select="App/Req/Appl/Sex"/>-->
							<xsl:with-param name="rela">
								<xsl:value-of select="App/Req/Appl/RelaToInsured"/>
							</xsl:with-param>
						</xsl:call-template>
					</RelaToInsured>
					<xsl:if test="Header/EntrustWay = 04 or Header/EntrustWay = 02 or Header/EntrustWay = 01">
						<JobCode>
							<xsl:call-template name="tran_occupationCode">
								<xsl:with-param name="occupationCode" select="App/Req/Appl/JobType"/>
							</xsl:call-template>
						</JobCode>
					</xsl:if>
					<xsl:if test="Header/EntrustWay = 11">
						<JobCode>
							<xsl:value-of select="App/Req/Appl/JobCode"/>
						</JobCode>
					</xsl:if>
					<Province>
						<xsl:value-of select="App/Req/Appl/Prov"/>
					</Province><!-- 省 -->
					<City>
						<xsl:value-of select="App/Req/Appl/City"/>
					</City><!-- 市 -->
					<County>
						<xsl:value-of select="App/Req/Appl/Zone"/>
					</County><!-- 区 -->
					<xsl:if test="Header/EntrustWay = 04 or Header/EntrustWay = 02 or Header/EntrustWay = 01">
						<JobType>
							<xsl:call-template name="tran_occupationType">
								<xsl:with-param name="occupationType" select="App/Req/Appl/JobType"/>
							</xsl:call-template>
						</JobType>
					</xsl:if>
					<xsl:if test="Header/EntrustWay = 11">
						<JobType>
							<xsl:value-of select="App/Req/Appl/JobType"/>
						</JobType>
					</xsl:if>
					<GrpName>
						<xsl:value-of select="App/Req/Appl/Company"/>
					</GrpName>
				</Appnt>
				<!-- 被保人 -->
				<Insured>
					<Name>
						<xsl:value-of select="App/Req/Insu/Name" />
					</Name>
					<Sex>
						<xsl:call-template name="tran_sex">
							<xsl:with-param name="sex">
								<xsl:value-of select="App/Req/Insu/Sex" />
							</xsl:with-param>
						</xsl:call-template>
					</Sex>
					<Birthday>
						<xsl:value-of select="App/Req/Insu/Birthday" />
					</Birthday>
					<IDType>
						<xsl:call-template name="tran_idtype">
							<xsl:with-param name="idtype">
								<xsl:value-of select="App/Req/Insu/IDKind" />
							</xsl:with-param>
						</xsl:call-template>
					</IDType>
					<IDNo>
						<xsl:value-of select="App/Req/Insu/IDCode" />
					</IDNo>
					<xsl:choose>
						<xsl:when test="App/Req/Insu/ValidDate = '99999999'">
							<InsValidityday>长期</InsValidityday>
						</xsl:when>
						<xsl:when test="App/Req/Insu/ValidDate = '99991231'">
							<InsValidityday>长期</InsValidityday>
						</xsl:when>
						<xsl:otherwise>
							<InsValidityday><xsl:value-of select="App/Req/Insu/ValidDate" /></InsValidityday><!-- 被保人证件有效期 -->
						</xsl:otherwise>
					</xsl:choose>
					<!--<JobCode><xsl:value-of select="App/Req/Insu/JobCode" /></JobCode>-->
					<!--<Nationality>
							<xsl:value-of select="App/Req/Insu/County" />
					</Nationality>-->
					<Address>
						<xsl:value-of select="App/Req/Insu/Address" />
					</Address>
					<ZipCode>
						<xsl:value-of select="App/Req/Insu/ZipCode" />
					</ZipCode>
					<Mobile>
						<xsl:value-of select="App/Req/Insu/Mobile" />
					</Mobile>
					<Phone>
						<xsl:value-of select="App/Req/Insu/Phone" />
					</Phone>
					<Email><xsl:value-of select="App/Req/Insu/Email" /></Email>
					<Nationality>
						<xsl:call-template name="tran_nationality">
							<xsl:with-param name="Flag" select="App/Req/Header/EntrustWay"/>
							<xsl:with-param name="nationality" select="App/Req/Insu/Country"/>
						</xsl:call-template>
					</Nationality>
					<!--<JobCode>
						<xsl:call-template name="tran_idjobCode">
							<xsl:with-param name="idjobCode">
								<xsl:value-of select="App/Req/Insu/JobType" />
							</xsl:with-param>
						</xsl:call-template>
					</JobCode>--><!-- 原中融逻辑 -->
					<xsl:if test="Header/EntrustWay = 04 or Header/EntrustWay = 02 or Header/EntrustWay = 01">
							<JobCode>
								<xsl:call-template name="tran_occupationCode">
									<xsl:with-param name="occupationCode" select="App/Req/Insu/JobType"/>
								</xsl:call-template>
							</JobCode>
					</xsl:if>
					<xsl:if test="Header/EntrustWay = 11">
							<JobCode>
								<xsl:value-of select="App/Req/Insu/JobCode"/>
							</JobCode>
					</xsl:if>
					<Province>
						<xsl:value-of select="App/Req/Insu/Prov"/>
					</Province><!-- 省 -->
					<City>
						<xsl:value-of select="App/Req/Insu/City"/>
					</City><!-- 市 -->
					<County>
						<xsl:value-of select="App/Req/Insu/Zone"/>
					</County><!-- 区 -->
					<xsl:if test="Header/EntrustWay = 04 or Header/EntrustWay = 02 or Header/EntrustWay = 01">
						<JobType>
							<xsl:call-template name="tran_occupationType">
								<xsl:with-param name="occupationType" select="App/Req/Insu/JobType"/>
							</xsl:call-template>
						</JobType>
					</xsl:if>
					<xsl:if test="Header/EntrustWay = 11">
						<JobType>
							<xsl:value-of select="App/Req/Insu/JobType"/>
						</JobType>
					</xsl:if>
					<GrpName>
						<xsl:value-of select="App/Req/Insu/Company"/>
					</GrpName>
				</Insured>
				<!-- 受益人 -->
				<Bnfs>
					<xsl:choose>
						<!-- 由于xxx受益人节点采用循环组包，依靠节点名称后面添加数字来区分，暂时先支持6个受益人 -->
						<xsl:when test="App/Req/Bnfs/Count != '0'">

						<xsl:if test="App/Req/Bnfs/Count &gt;= 1">
							<Bnf>
								<BnfType>
									<xsl:value-of select="App/Req/Bnfs/Type1" />
								</BnfType>
								<Grade><xsl:value-of select="App/Req/Bnfs/Sequence1" /></Grade>
								<Name><xsl:value-of select="App/Req/Bnfs/Name1" /></Name>
								<Sex>
									<xsl:call-template name="tran_sex">
										<xsl:with-param name="sex">
											<xsl:value-of select="App/Req/Bnfs/Sex1" />
										</xsl:with-param>
									</xsl:call-template>
								</Sex>
								<Birthday>
									<xsl:value-of select="App/Req/Bnfs/Birthday1" />
								</Birthday>
								<IDType>
									<xsl:call-template name="tran_idtype">
										<xsl:with-param name="idtype">
											<xsl:value-of select="App/Req/Bnfs/IDKind1" />
										</xsl:with-param>
									</xsl:call-template>
								</IDType>
								<IDNo><xsl:value-of select="App/Req/Bnfs/IDCode1" /></IDNo>
								<BnfValidityday>
									<xsl:call-template name="tran_idValidate">
										<xsl:with-param name="idValidate" select="App/Req/Bnfs/InvalidDate1"/>
									</xsl:call-template>
								</BnfValidityday><!-- 受益人证件有效期 -->
								<Lot>
									<xsl:value-of select="App/Req/Bnfs/Prop1" />
								</Lot>
								<RelaToInsured>
									<xsl:call-template name="tran_rela">
										<xsl:with-param name="rela">
											<xsl:value-of select="App/Req/Bnfs/RelationToInsured1" />
										</xsl:with-param>
									</xsl:call-template>
								</RelaToInsured>
								<BeneficType>N</BeneficType>
								<Nationality>
									<xsl:call-template name="tran_nationality">
										<xsl:with-param name="Flag" select="App/Req/Header/EntrustWay"/>
										<xsl:with-param name="nationality" select="App/Req/Bnfs/Country1"/>
									</xsl:call-template>
								</Nationality>
								<Phone>
									<xsl:value-of select="App/Req/Bnfs/Phone1"/>
								</Phone>
								<Address>
									<xsl:value-of select="App/Req/Bnfs/Address1"/>
								</Address>
								</Bnf>
							</xsl:if>
							<xsl:if test="App/Req/Bnfs/Count &gt;= 2">
							<Bnf>
								<BnfType>
									<xsl:value-of select="App/Req/Bnfs/Type2" />
								</BnfType>
								<Grade><xsl:value-of select="App/Req/Bnfs/Sequence2" /></Grade>
								<Name><xsl:value-of select="App/Req/Bnfs/Name2" /></Name>
								<Sex>
									<xsl:call-template name="tran_sex">
										<xsl:with-param name="sex">
											<xsl:value-of select="App/Req/Bnfs/Sex2" />
										</xsl:with-param>
									</xsl:call-template>
								</Sex>
								<Birthday>
									<xsl:value-of select="App/Req/Bnfs/Birthday2" />
								</Birthday>
								<IDType>
									<xsl:call-template name="tran_idtype">
										<xsl:with-param name="idtype">
											<xsl:value-of select="App/Req/Bnfs/IDKind2" />
										</xsl:with-param>
									</xsl:call-template>
								</IDType>
								<IDNo><xsl:value-of select="App/Req/Bnfs/IDCode2" /></IDNo>
								<BnfValidityday>
									<xsl:call-template name="tran_idValidate">
										<xsl:with-param name="idValidate" select="App/Req/Bnfs/InvalidDate2"/>
									</xsl:call-template>
								</BnfValidityday><!-- 受益人证件有效期 -->
								<Lot>
									<xsl:value-of select="App/Req/Bnfs/Prop2" />
								</Lot>
								<RelaToInsured>
									<xsl:call-template name="tran_rela">
										<xsl:with-param name="rela">
											<xsl:value-of select="App/Req/Bnfs/RelationToInsured2" />
										</xsl:with-param>
									</xsl:call-template>
								</RelaToInsured>
								<BeneficType>N</BeneficType>
								<Nationality>
									<xsl:call-template name="tran_nationality">
										<xsl:with-param name="Flag" select="App/Req/Header/EntrustWay"/>
										<xsl:with-param name="nationality" select="App/Req/Bnfs/Country2"/>
									</xsl:call-template>
								</Nationality>
								<Phone>
									<xsl:value-of select="App/Req/Bnfs/Phone2"/>
								</Phone>
								<Address>
									<xsl:value-of select="App/Req/Bnfs/Address2"/>
								</Address>
								</Bnf>
							</xsl:if>
							<xsl:if test="App/Req/Bnfs/Count &gt;= 3">
							<Bnf>
								<BnfType>
									<xsl:value-of select="App/Req/Bnfs/Type3" />
								</BnfType>
								<Grade><xsl:value-of select="App/Req/Bnfs/Sequence3" /></Grade>
								<Name><xsl:value-of select="App/Req/Bnfs/Name3" /></Name>
								<Sex>
									<xsl:call-template name="tran_sex">
										<xsl:with-param name="sex">
											<xsl:value-of select="App/Req/Bnfs/Sex3" />
										</xsl:with-param>
									</xsl:call-template>
								</Sex>
								<Birthday>
									<xsl:value-of select="App/Req/Bnfs/Birthday3" />
								</Birthday>
								<IDType>
									<xsl:call-template name="tran_idtype">
										<xsl:with-param name="idtype">
											<xsl:value-of select="App/Req/Bnfs/IDKind3" />
										</xsl:with-param>
									</xsl:call-template>
								</IDType>
								<IDNo><xsl:value-of select="App/Req/Bnfs/IDCode3" /></IDNo>
								<BnfValidityday>
									<xsl:call-template name="tran_idValidate">
										<xsl:with-param name="idValidate" select="App/Req/Bnfs/InvalidDate3"/>
									</xsl:call-template>
								</BnfValidityday><!-- 受益人证件有效期 -->
								<Lot>
									<xsl:value-of select="App/Req/Bnfs/Prop3" />
								</Lot>
								<RelaToInsured>
									<xsl:call-template name="tran_rela">
										<xsl:with-param name="rela">
											<xsl:value-of select="App/Req/Bnfs/RelationToInsured3" />
										</xsl:with-param>
									</xsl:call-template>
								</RelaToInsured>
								<BeneficType>N</BeneficType>
								<Nationality>
									<xsl:call-template name="tran_nationality">
										<xsl:with-param name="Flag" select="App/Req/Header/EntrustWay"/>
										<xsl:with-param name="nationality" select="App/Req/Bnfs/Country3"/>
									</xsl:call-template>
								</Nationality>
								<Phone>
									<xsl:value-of select="App/Req/Bnfs/Phone3"/>
								</Phone>
								<Address>
									<xsl:value-of select="App/Req/Bnfs/Address3"/>
								</Address>
								</Bnf>
							</xsl:if>
							<xsl:if test="App/Req/Bnfs/Count &gt;= 4">
							<Bnf>
								<BnfType>
									<xsl:value-of select="App/Req/Bnfs/Type4" />
								</BnfType>
								<Grade><xsl:value-of select="App/Req/Bnfs/Sequence4" /></Grade>
								<Name><xsl:value-of select="App/Req/Bnfs/Name4" /></Name>
								<Sex>
									<xsl:call-template name="tran_sex">
										<xsl:with-param name="sex">
											<xsl:value-of select="App/Req/Bnfs/Sex4" />
										</xsl:with-param>
									</xsl:call-template>
								</Sex>
								<Birthday>
									<xsl:value-of select="App/Req/Bnfs/Birthday4" />
								</Birthday>
								<IDType>
									<xsl:call-template name="tran_idtype">
										<xsl:with-param name="idtype">
											<xsl:value-of select="App/Req/Bnfs/IDKind4" />
										</xsl:with-param>
									</xsl:call-template>
								</IDType>
								<IDNo><xsl:value-of select="App/Req/Bnfs/IDCode4" /></IDNo>
								<BnfValidityday>
									<xsl:call-template name="tran_idValidate">
										<xsl:with-param name="idValidate" select="App/Req/Bnfs/InvalidDate4"/>
									</xsl:call-template>
								</BnfValidityday><!-- 受益人证件有效期 -->
								<Lot>
									<xsl:value-of select="App/Req/Bnfs/Prop4" />
								</Lot>
								<RelaToInsured>
									<xsl:call-template name="tran_rela">
										<xsl:with-param name="rela">
											<xsl:value-of select="App/Req/Bnfs/RelationToInsured4" />
										</xsl:with-param>
									</xsl:call-template>
								</RelaToInsured>
								<BeneficType>N</BeneficType>
								<Nationality>
									<xsl:call-template name="tran_nationality">
										<xsl:with-param name="Flag" select="App/Req/Header/EntrustWay"/>
										<xsl:with-param name="nationality" select="App/Req/Bnfs/Country4"/>
									</xsl:call-template>
								</Nationality>
								<Phone>
									<xsl:value-of select="App/Req/Bnfs/Phone4"/>
								</Phone>
								<Address>
									<xsl:value-of select="App/Req/Bnfs/Address4"/>
								</Address>
								</Bnf>
							</xsl:if>
							<xsl:if test="App/Req/Bnfs/Count &gt;= 5">
							<Bnf>
								<BnfType>
									<xsl:value-of select="App/Req/Bnfs/Type5" />
								</BnfType>
								<Grade><xsl:value-of select="App/Req/Bnfs/Sequence5" /></Grade>
								<Name><xsl:value-of select="App/Req/Bnfs/Name5" /></Name>
								<Sex>
									<xsl:call-template name="tran_sex">
										<xsl:with-param name="sex">
											<xsl:value-of select="App/Req/Bnfs/Sex5" />
										</xsl:with-param>
									</xsl:call-template>
								</Sex>
								<Birthday>
									<xsl:value-of select="App/Req/Bnfs/Birthday5" />
								</Birthday>
								<IDType>
									<xsl:call-template name="tran_idtype">
										<xsl:with-param name="idtype">
											<xsl:value-of select="App/Req/Bnfs/IDKind5" />
										</xsl:with-param>
									</xsl:call-template>
								</IDType>
								<IDNo><xsl:value-of select="App/Req/Bnfs/IDCode5" /></IDNo>
								<BnfValidityday>
									<xsl:call-template name="tran_idValidate">
										<xsl:with-param name="idValidate" select="App/Req/Bnfs/InvalidDate5"/>
									</xsl:call-template>
								</BnfValidityday><!-- 受益人证件有效期 -->
								<Lot>
									<xsl:value-of select="App/Req/Bnfs/Prop5" />
								</Lot>
								<RelaToInsured>
									<xsl:call-template name="tran_rela">
										<xsl:with-param name="rela">
											<xsl:value-of select="App/Req/Bnfs/RelationToInsured5" />
										</xsl:with-param>
									</xsl:call-template>
								</RelaToInsured>
								<BeneficType>N</BeneficType>
								<Nationality>
									<xsl:call-template name="tran_nationality">
										<xsl:with-param name="Flag" select="App/Req/Header/EntrustWay"/>
										<xsl:with-param name="nationality" select="App/Req/Bnfs/Country5"/>
									</xsl:call-template>
								</Nationality>
								<Phone>
									<xsl:value-of select="App/Req/Bnfs/Phone5"/>
								</Phone>
								<Address>
									<xsl:value-of select="App/Req/Bnfs/Address5"/>
								</Address>
								</Bnf>
							</xsl:if>
							<xsl:if test="App/Req/Bnfs/Count &gt;= 6">
							<Bnf>
								<BnfType>
									<xsl:value-of select="App/Req/Bnfs/Type6" />
								</BnfType>
								<Grade><xsl:value-of select="App/Req/Bnfs/Sequence6" /></Grade>
								<Name><xsl:value-of select="App/Req/Bnfs/Name6" /></Name>
								<Sex>
									<xsl:call-template name="tran_sex">
										<xsl:with-param name="sex">
											<xsl:value-of select="App/Req/Bnfs/Sex6" />
										</xsl:with-param>
									</xsl:call-template>
								</Sex>
								<Birthday>
									<xsl:value-of select="App/Req/Bnfs/Birthday6" />
								</Birthday>
								<IDType>
									<xsl:call-template name="tran_idtype">
										<xsl:with-param name="idtype">
											<xsl:value-of select="App/Req/Bnfs/IDKind6" />
										</xsl:with-param>
									</xsl:call-template>
								</IDType>
								<IDNo><xsl:value-of select="App/Req/Bnfs/IDCode6" /></IDNo>
								<BnfValidityday>
									<xsl:call-template name="tran_idValidate">
										<xsl:with-param name="idValidate" select="App/Req/Bnfs/InvalidDate6"/>
									</xsl:call-template>
								</BnfValidityday><!-- 受益人证件有效期 -->
								<Lot>
									<xsl:value-of select="App/Req/Bnfs/Prop6" />
								</Lot>
								<RelaToInsured>
									<xsl:call-template name="tran_rela">
										<xsl:with-param name="rela">
											<xsl:value-of select="App/Req/Bnfs/RelationToInsured6" />
										</xsl:with-param>
									</xsl:call-template>
								</RelaToInsured>
								<BeneficType>N</BeneficType>
								<Nationality>
									<xsl:call-template name="tran_nationality">
										<xsl:with-param name="Flag" select="App/Req/Header/EntrustWay"/>
										<xsl:with-param name="nationality" select="App/Req/Bnfs/Country6"/>
									</xsl:call-template>
								</Nationality>
								<Phone>
									<xsl:value-of select="App/Req/Bnfs/Phone6"/>
								</Phone>
								<Address>
									<xsl:value-of select="App/Req/Bnfs/Address6"/>
								</Address>
								</Bnf>
							</xsl:if>
						</xsl:when>
					</xsl:choose>
				</Bnfs>
				<!-- 险种 -->
				<Risks>
					<Risk>
						<xsl:variable name="varRiskCode">
							<xsl:call-template name="tran_riskCode">
								<xsl:with-param name="riskCode" select="App/Req/Risks/RiskCode"/>
							</xsl:call-template>
						</xsl:variable>
						<RiskCode><!-- 险种代码 -->
							<xsl:value-of select="$varRiskCode" />
						</RiskCode>
						<MainRiskCode><!-- 主险代码 -->
							<xsl:value-of select="$varRiskCode" />
						</MainRiskCode>
						<Amnt><!-- 保额 -->
							<!--<xsl:value-of select="java:com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.yuanToFen(App/Req/Risks/Amnt)" />-->
							<xsl:choose>
								<xsl:when test="Header/EntrustWay = 01">
									<xsl:call-template name="tran_amnt">
										<xsl:with-param name="riskCode" select="$varRiskCode"/>
										<xsl:with-param name="amnt" select="App/Req/Risks/Amnt"/>
									</xsl:call-template>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="App/Req/Risks/Amnt"/>
								</xsl:otherwise>
							</xsl:choose>
						</Amnt>
						<Prem><!-- 保费 -->
							<!--<xsl:value-of select="java:com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.yuanToFen(App/Req/Risks/Prem)" />-->
							<xsl:value-of select="App/Req/Risks/Prem"/>
						</Prem>
						<Mult><!-- 份数 -->
							<xsl:value-of select="App/Req/Risks/Share" />
						</Mult>
						<PayIntv><!-- 缴费间隔 -->
							<xsl:call-template name="tran_Contpayintv">
								<xsl:with-param name="payintv">
									<xsl:value-of select="App/Req/Risks/PayType" />
								</xsl:with-param>
							</xsl:call-template>
						</PayIntv>
						<xsl:if test="Header/EntrustWay = 01">
							<PayMode>C</PayMode>
						</xsl:if>
						<xsl:if test="Header/EntrustWay = 04">
							<PayMode>C</PayMode>
						</xsl:if>
						<xsl:if test="Header/EntrustWay = 11">
							<PayMode>A</PayMode>
						</xsl:if>
						<xsl:if test="Header/EntrustWay = 02">
							<PayMode>A</PayMode>
						</xsl:if>
						<ExPayMode>4</ExPayMode>
						<InsuYearFlag><!-- 保险年期类型 -->
							<xsl:call-template name="tran_insuYearFlag">
								<xsl:with-param name="yearFlag" select="App/Req/Risks/InsuDueType"/>
							</xsl:call-template>
						</InsuYearFlag>
						<InsuYear>
							<xsl:call-template name="tran_InsuYear">
								<xsl:with-param name="insuYear" select="App/Req/Risks/InsuDueDate"/>
							</xsl:call-template>
						</InsuYear>
						<PayEndYear>
							<xsl:value-of select="App/Req/Risks/PayDueDate"/>
						</PayEndYear>
						<PayEndYearFlag>
							<xsl:if test="App/Req/Risks/PayDueType != ''">
								<xsl:call-template name="tran_payendYearFlag">
									<xsl:with-param name="yearFlag" select="App/Req/Risks/PayDueType"/>
								</xsl:call-template>
							</xsl:if>
							<xsl:if test="App/Req/Risks/PayDueType = ''">Y</xsl:if>
							<!--<xsl:if test="Header/EntrustWay = 01">-->
								<!--<xsl:if test="App/Req/Risks/RiskCode = 1029">Y</xsl:if>-->
								<!--<xsl:if test="App/Req/Risks/RiskCode = 1022">Y</xsl:if>-->
								<!--<xsl:if test="App/Req/Risks/RiskCode = 2036">Y</xsl:if>-->
								<!--<xsl:if test="App/Req/Risks/RiskCode = 2044">Y</xsl:if>-->
							<!--</xsl:if>-->
						</PayEndYearFlag>
						<BonusGetMode><!-- 红利领取方式 -->
							<xsl:if test="App/Req/Risks/BonusGetMode!=''">
								<xsl:call-template name="tran_getMode">
									<xsl:with-param name="bonusGetMode" select="App/Req/Risks/BonusGetMode"/>
								</xsl:call-template>
							</xsl:if>
						</BonusGetMode>
						<GetIntv><!-- 年金领取方式 /保险金领取方式-->
							<xsl:call-template name="tran_getIntv">
								<xsl:with-param name="riskCode">
									<xsl:value-of select="$varRiskCode"/>
								</xsl:with-param>
								<xsl:with-param name="getIntv">
									<xsl:value-of select="App/Req/Risks/FullBonusGetMode"/>
								</xsl:with-param>
							</xsl:call-template>
						</GetIntv>
						<RnewFlag>
							<xsl:call-template name="tran_renew">
								<xsl:with-param name="flag" select="App/Req/Risks/AutoPayFlag" />
							</xsl:call-template>
						</RnewFlag><!-- 自动续保标志 -->
						<AutoPayFlag>
							<xsl:value-of select="App/Req/Risks/AutoPayForFlag"/>
						</AutoPayFlag><!-- 自动垫交标志 -->
						<EndDate>
							<xsl:value-of select="App/Req/Risks/RiskEndDate"/>
						</EndDate><!-- 保险止期 -->
						<GetYear>
							<xsl:value-of select="App/Req/Risks/GetYear"/>
						</GetYear><!-- 领取年期 -->
						<GetYearFlag>
							<xsl:call-template name="tran_getyearflag">
								<xsl:with-param name="getyearflag">
									<xsl:value-of select="App/Req/Risks/GetYearFlag"/>
								</xsl:with-param>
							</xsl:call-template>
						</GetYearFlag><!-- 年金/生存金领取年期类型: -->
						<LiveGetMode>
							<xsl:call-template name="tran_livegetmode">
								<xsl:with-param name="livegetmode">
									<xsl:if test="Header/EntrustWay = 01 or Header/EntrustWay = 02 or Header/EntrustWay = 04">
										<xsl:choose>
											<!-- 1累计生息，2现金领取 -->
											<xsl:when test="string($varRiskCode) = '2041'">
												1
											</xsl:when>
											<xsl:when test="string($varRiskCode) = '1029'">2</xsl:when>
											<xsl:when test="string($varRiskCode) = '2039'">
												<xsl:if test="App/Req/Risks/FullBonusGetMode ='0'">
													1
												</xsl:if>
												<xsl:if test="App/Req/Risks/FullBonusGetMode ='1'">
													2
												</xsl:if>
												<xsl:if test="App/Req/Risks/FullBonusGetMode ='4'">
													2
												</xsl:if>
											</xsl:when>
											<xsl:otherwise>
												<xsl:value-of select="''"/>
											</xsl:otherwise>
										</xsl:choose>
									</xsl:if>
									<xsl:if test="Header/EntrustWay = 11">
										<xsl:choose>
											<!-- 1累计生息，2现金领取 -->
											<xsl:when test="string($varRiskCode) = '2041'">
												1
											</xsl:when>
											<xsl:when test="string($varRiskCode) = '2039'">
												<xsl:if test="App/Req/Risks/FullBonusGetMode ='0'">
													1
												</xsl:if>
												<xsl:if test="App/Req/Risks/FullBonusGetMode ='1'">
													2
												</xsl:if>
												<xsl:if test="App/Req/Risks/FullBonusGetMode ='4'">
													2
												</xsl:if>
											</xsl:when>
											<xsl:otherwise>
												<xsl:value-of select="''"/>
											</xsl:otherwise>
										</xsl:choose>
									</xsl:if>
								</xsl:with-param>
							</xsl:call-template>
						</LiveGetMode>
					</Risk>
					<xsl:choose>
					<xsl:when test="App/Req/Addt/Count != '0'">
						<xsl:if test="App/Req/Addt/Count &gt;= 1">
							<Risk>
								<xsl:variable name="varRiskCode">
									<xsl:call-template name="tran_riskCode">
										<xsl:with-param name="riskCode" select="App/Req/Addt/RiskCode1"/>
									</xsl:call-template>
								</xsl:variable>
								<xsl:variable name="varMainRiskCode">
									<xsl:call-template name="tran_riskCode">
										<xsl:with-param name="riskCode" select="App/Req/Risks/RiskCode"/>
									</xsl:call-template>
								</xsl:variable>
								<RiskCode>
									<xsl:value-of select="$varRiskCode"/>
								</RiskCode>
								<MainRiskCode>
									<xsl:value-of select="$varMainRiskCode"/>
								</MainRiskCode>
								<Amnt>
									<!--<xsl:value-of select="java:com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.yuanToFen(App/Req/Addt/Amnt1)" />-->
									<xsl:value-of select="App/Req/Addt/Amnt1"/>
								</Amnt>
								<Prem>
									<!--<xsl:value-of select="java:com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.yuanToFen(App/Req/Addt/Prem1)" />-->
									<xsl:value-of select="App/Req/Addt/Prem1"/>
								</Prem>
								<Mult>
									<xsl:value-of select="App/Req/Addt/Share1"/>
								</Mult>
								<PayIntv>
									<xsl:call-template name="tran_Contpayintv">
										<xsl:with-param name="payintv" select="App/Req/Addt/PayType1"/>
									</xsl:call-template>
								</PayIntv>
								<xsl:if test="Header/EntrustWay = 01">
									<PayMode>C</PayMode>
								</xsl:if>
								<xsl:if test="Header/EntrustWay = 04">
									<PayMode>C</PayMode>
								</xsl:if>
								<xsl:if test="Header/EntrustWay = 11">
									<PayMode>A</PayMode>
								</xsl:if>
								<xsl:if test="Header/EntrustWay = 02">
									<PayMode>A</PayMode>
								</xsl:if>
								<ExPayMode>4</ExPayMode>
								<InsuYearFlag><!-- 保险年期类型 -->
									<xsl:call-template name="tran_insuYearFlag">
										<xsl:with-param name="yearFlag" select="App/Req/Addt/InsuDueType1"/>
									</xsl:call-template>
								</InsuYearFlag>
								<InsuYear>
									<xsl:call-template name="tran_InsuYear">
										<xsl:with-param name="insuYear" select="App/Req/Addt/InsuDueDate1"/>
									</xsl:call-template>
								</InsuYear>
								<PayEndYear>
									<xsl:value-of select="App/Req/Addt/PayDueDate1"/>
								</PayEndYear>
								<PayEndYearFlag>
									<xsl:if test="App/Req/Addt/PayDueType1 != ''">
										<xsl:call-template name="tran_payendYearFlag">
											<xsl:with-param name="yearFlag" select="App/Req/Addt/PayDueType1"/>
										</xsl:call-template>
									</xsl:if>
									<xsl:if test="App/Req/Addt/PayDueType1 = ''">Y</xsl:if>
									<xsl:if test="Header/EntrustWay = 01">
										<xsl:if test="App/Req/Addt/RiskCode1 = 1029">Y</xsl:if>
										<xsl:if test="App/Req/Addt/RiskCode1 = 1022">Y</xsl:if>
										<xsl:if test="App/Req/Addt/RiskCode1 = 2036">Y</xsl:if>
										<xsl:if test="App/Req/Addt/RiskCode1 = 2044">Y</xsl:if>
									</xsl:if>
								</PayEndYearFlag>
								<BonusGetMode><!-- 红利领取方式 -->
								</BonusGetMode>
								<GetIntv><!-- 年金领取方式 /保险金领取方式-->
									<xsl:call-template name="tran_getIntv">
										<xsl:with-param name="riskCode">
											<xsl:value-of select="$varRiskCode"/>
										</xsl:with-param>
										<xsl:with-param name="getIntv">
											<xsl:value-of select="App/Req/Risks/FullBonusGetMode"/>
										</xsl:with-param>
									</xsl:call-template>
								</GetIntv>
								<RnewFlag>
									<xsl:call-template name="tran_renew">
										<xsl:with-param name="flag" select="App/Req/Addt/AutoPayFlag1" />
									</xsl:call-template>
								</RnewFlag><!-- 自动续保标志 -->
								<AutoPayFlag>
									<xsl:value-of select="App/Req/Addt/AutoPayForFlag1"/>
								</AutoPayFlag><!-- 自动垫交标志 -->
								<EndDate>
									<xsl:value-of select="App/Req/Risks/RiskEndDate"/>
								</EndDate><!-- 保险止期 -->
								<LiveGetMode>
									<xsl:call-template name="tran_livegetmode">
										<xsl:with-param name="livegetmode">
											<xsl:if test="Header/EntrustWay = 01 or Header/EntrustWay = 02 or Header/EntrustWay = 04">
												<xsl:choose>
													<!-- 1累计生息，2现金领取 -->
													<xsl:when test="string($varRiskCode) = '2041'">
														1
													</xsl:when>
													<xsl:when test="string($varRiskCode) = '1029'">2</xsl:when>
													<xsl:when test="string($varRiskCode) = '2039'">
														<xsl:if test="App/Req/Risks/FullBonusGetMode ='0'">
															1
														</xsl:if>
														<xsl:if test="App/Req/Risks/FullBonusGetMode ='1'">
															2
														</xsl:if>
														<xsl:if test="App/Req/Risks/FullBonusGetMode ='4'">
															2
														</xsl:if>
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="''"/>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:if>
											<xsl:if test="Header/EntrustWay = 11">
												<xsl:choose>
													<!-- 1累计生息，2现金领取 -->
													<xsl:when test="string($varRiskCode) = '2041'">
														1
													</xsl:when>
													<xsl:when test="string($varRiskCode) = '2039'">
														<xsl:if test="App/Req/Risks/FullBonusGetMode ='0'">
															1
														</xsl:if>
														<xsl:if test="App/Req/Risks/FullBonusGetMode ='1'">
															2
														</xsl:if>
														<xsl:if test="App/Req/Risks/FullBonusGetMode ='4'">
															2
														</xsl:if>
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="''"/>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:if>
										</xsl:with-param>
									</xsl:call-template>
								</LiveGetMode>
							</Risk>
						</xsl:if>
						<xsl:if test="App/Req/Addt/Count &gt;= 2">
							<Risk>
								<xsl:variable name="varRiskCode">
									<xsl:call-template name="tran_riskCode">
										<xsl:with-param name="riskCode" select="App/Req/Addt/RiskCode2"/>
									</xsl:call-template>
								</xsl:variable>
								<xsl:variable name="varMainRiskCode">
									<xsl:call-template name="tran_riskCode">
										<xsl:with-param name="riskCode" select="App/Req/Risks/RiskCode"/>
									</xsl:call-template>
								</xsl:variable>
								<RiskCode>
									<xsl:value-of select="$varRiskCode"/>
								</RiskCode>
								<MainRiskCode>
									<xsl:value-of select="$varMainRiskCode"/>
								</MainRiskCode>
								<Amnt>
									<!--<xsl:value-of select="java:com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.yuanToFen(App/Req/Addt/Amnt2)" />-->
									<xsl:value-of select="App/Req/Addt/Amnt2"/>
								</Amnt>
								<Prem>
									<!--<xsl:value-of select="java:com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.yuanToFen(App/Req/Addt/Prem2)" />-->
									<xsl:value-of select="App/Req/Addt/Prem2"/>
								</Prem>
								<Mult>
									<xsl:value-of select="App/Req/Addt/Share2"/>
								</Mult>
								<PayIntv>
									<xsl:call-template name="tran_Contpayintv">
										<xsl:with-param name="payintv" select="App/Req/Addt/PayType2"/>
									</xsl:call-template>
								</PayIntv>
								<xsl:if test="Header/EntrustWay = 01">
									<PayMode>C</PayMode>
								</xsl:if>
								<xsl:if test="Header/EntrustWay = 04">
									<PayMode>C</PayMode>
								</xsl:if>
								<xsl:if test="Header/EntrustWay = 11">
									<PayMode>A</PayMode>
								</xsl:if>
								<xsl:if test="Header/EntrustWay = 02">
									<PayMode>A</PayMode>
								</xsl:if>
								<ExPayMode>4</ExPayMode>
								<InsuYearFlag><!-- 保险年期类型 -->
									<xsl:call-template name="tran_insuYearFlag">
										<xsl:with-param name="yearFlag" select="App/Req/Addt/InsuDueType2"/>
									</xsl:call-template>
								</InsuYearFlag>
								<InsuYear>
									<xsl:call-template name="tran_InsuYear">
										<xsl:with-param name="insuYear" select="App/Req/Addt/InsuDueDate2"/>
									</xsl:call-template>
								</InsuYear>
								<PayEndYear>
									<xsl:value-of select="App/Req/Addt/PayDueDate2"/>
								</PayEndYear>
								<PayEndYearFlag>
									<xsl:if test="App/Req/Addt/PayDueType2 != ''">
										<xsl:call-template name="tran_payendYearFlag">
											<xsl:with-param name="yearFlag" select="App/Req/Addt/PayDueType2"/>
										</xsl:call-template>
									</xsl:if>
									<xsl:if test="App/Req/Addt/PayDueType2 = ''">Y</xsl:if>
									<xsl:if test="Header/EntrustWay = 01">
										<xsl:if test="App/Req/Addt/RiskCode2 = 1029">Y</xsl:if>
										<xsl:if test="App/Req/Addt/RiskCode2 = 1022">Y</xsl:if>
										<xsl:if test="App/Req/Addt/RiskCode2 = 2036">Y</xsl:if>
										<xsl:if test="App/Req/Addt/RiskCode2 = 2044">Y</xsl:if>
									</xsl:if>
								</PayEndYearFlag>
								<BonusGetMode><!-- 红利领取方式 -->
								</BonusGetMode>
								<GetIntv><!-- 年金领取方式 /保险金领取方式-->
									<xsl:call-template name="tran_getIntv">
										<xsl:with-param name="riskCode">
											<xsl:value-of select="$varRiskCode"/>
										</xsl:with-param>
										<xsl:with-param name="getIntv">
											<xsl:value-of select="App/Req/Risks/FullBonusGetMode"/>
										</xsl:with-param>
									</xsl:call-template>
								</GetIntv>
								<RnewFlag>
									<xsl:call-template name="tran_renew">
										<xsl:with-param name="flag" select="App/Req/Addt/AutoPayFlag2" />
									</xsl:call-template>
								</RnewFlag><!-- 自动续保标志 -->
								<AutoPayFlag>
									<xsl:value-of select="App/Req/Addt/AutoPayForFlag2"/>
								</AutoPayFlag><!-- 自动垫交标志 -->
								<EndDate>
									<xsl:value-of select="App/Req/Risks/RiskEndDate"/>
								</EndDate><!-- 保险止期 -->
								<LiveGetMode>
									<xsl:call-template name="tran_livegetmode">
										<xsl:with-param name="livegetmode">
											<xsl:if test="Header/EntrustWay = 01 or Header/EntrustWay = 02 or Header/EntrustWay = 04">
												<xsl:choose>
													<!-- 1累计生息，2现金领取 -->
													<xsl:when test="string($varRiskCode) = '2041'">
														1
													</xsl:when>
													<xsl:when test="string($varRiskCode) = '1029'">2</xsl:when>
													<xsl:when test="string($varRiskCode) = '2039'">
														<xsl:if test="App/Req/Risks/FullBonusGetMode ='0'">
															1
														</xsl:if>
														<xsl:if test="App/Req/Risks/FullBonusGetMode ='1'">
															2
														</xsl:if>
														<xsl:if test="App/Req/Risks/FullBonusGetMode ='4'">
															2
														</xsl:if>
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="''"/>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:if>
											<xsl:if test="Header/EntrustWay = 11">
												<xsl:choose>
													<!-- 1累计生息，2现金领取 -->
													<xsl:when test="string($varRiskCode) = '2041'">
														1
													</xsl:when>
													<xsl:when test="string($varRiskCode) = '2039'">
														<xsl:if test="App/Req/Risks/FullBonusGetMode ='0'">
															1
														</xsl:if>
														<xsl:if test="App/Req/Risks/FullBonusGetMode ='1'">
															2
														</xsl:if>
														<xsl:if test="App/Req/Risks/FullBonusGetMode ='4'">
															2
														</xsl:if>
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="''"/>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:if>
										</xsl:with-param>
									</xsl:call-template>
								</LiveGetMode>
							</Risk>
						</xsl:if>
						<xsl:if test="App/Req/Addt/Count &gt;= 3">
							<Risk>
								<xsl:variable name="varRiskCode">
									<xsl:call-template name="tran_riskCode">
										<xsl:with-param name="riskCode" select="App/Req/Addt/RiskCode3"/>
									</xsl:call-template>
								</xsl:variable>
								<xsl:variable name="varMainRiskCode">
									<xsl:call-template name="tran_riskCode">
										<xsl:with-param name="riskCode" select="App/Req/Risks/RiskCode"/>
									</xsl:call-template>
								</xsl:variable>
								<RiskCode>
									<xsl:value-of select="$varRiskCode"/>
								</RiskCode>
								<MainRiskCode>
									<xsl:value-of select="$varMainRiskCode"/>
								</MainRiskCode>
								<Amnt>
									<!--<xsl:value-of select="java:com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.yuanToFen(App/Req/Addt/Amnt3)" />-->
									<xsl:value-of select="App/Req/Addt/Amnt3"/>
								</Amnt>
								<Prem>
									<!--<xsl:value-of select="java:com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.yuanToFen(App/Req/Addt/Prem3)" />-->
									<xsl:value-of select="App/Req/Addt/Prem3"/>
								</Prem>
								<Mult>
									<xsl:value-of select="App/Req/Addt/Share3"/>
								</Mult>
								<PayIntv>
									<xsl:call-template name="tran_Contpayintv">
										<xsl:with-param name="payintv" select="App/Req/Addt/PayType3"/>
									</xsl:call-template>
								</PayIntv>
								<xsl:if test="Header/EntrustWay = 01">
									<PayMode>C</PayMode>
								</xsl:if>
								<xsl:if test="Header/EntrustWay = 04">
									<PayMode>C</PayMode>
								</xsl:if>
								<xsl:if test="Header/EntrustWay = 11">
									<PayMode>A</PayMode>
								</xsl:if>
								<xsl:if test="Header/EntrustWay = 02">
									<PayMode>A</PayMode>
								</xsl:if>
								<ExPayMode>4</ExPayMode>
								<InsuYearFlag><!-- 保险年期类型 -->
									<xsl:call-template name="tran_insuYearFlag">
										<xsl:with-param name="yearFlag" select="App/Req/Addt/InsuDueType3"/>
									</xsl:call-template>
								</InsuYearFlag>
								<InsuYear>
									<xsl:call-template name="tran_InsuYear">
										<xsl:with-param name="insuYear" select="App/Req/Addt/InsuDueDate3"/>
									</xsl:call-template>
								</InsuYear>
								<PayEndYear>
									<xsl:value-of select="App/Req/Addt/PayDueDate3"/>
								</PayEndYear>
								<PayEndYearFlag>
									<xsl:if test="App/Req/Addt/PayDueType3 != ''">
										<xsl:call-template name="tran_payendYearFlag">
											<xsl:with-param name="yearFlag" select="App/Req/Addt/PayDueType3"/>
										</xsl:call-template>
									</xsl:if>
									<xsl:if test="App/Req/Addt/PayDueType3 = ''">Y</xsl:if>
									<xsl:if test="Header/EntrustWay = 01">
										<xsl:if test="App/Req/Addt/RiskCode3 = 1029">Y</xsl:if>
										<xsl:if test="App/Req/Addt/RiskCode3 = 1022">Y</xsl:if>
										<xsl:if test="App/Req/Addt/RiskCode3 = 2036">Y</xsl:if>
										<xsl:if test="App/Req/Addt/RiskCode3 = 2044">Y</xsl:if>
									</xsl:if>
								</PayEndYearFlag>
								<BonusGetMode><!-- 红利领取方式 -->
								</BonusGetMode>
								<GetIntv><!-- 年金领取方式 /保险金领取方式-->
									<xsl:call-template name="tran_getIntv">
										<xsl:with-param name="riskCode">
											<xsl:value-of select="$varRiskCode"/>
										</xsl:with-param>
										<xsl:with-param name="getIntv">
											<xsl:value-of select="App/Req/Risks/FullBonusGetMode"/>
										</xsl:with-param>
									</xsl:call-template>
								</GetIntv>
								<RnewFlag>
									<xsl:call-template name="tran_renew">
										<xsl:with-param name="flag" select="App/Req/Addt/AutoPayFlag3" />
									</xsl:call-template>
								</RnewFlag><!-- 自动续保标志 -->
								<AutoPayFlag>
									<xsl:value-of select="App/Req/Addt/AutoPayForFlag3"/>
								</AutoPayFlag><!-- 自动垫交标志 -->
								<EndDate>
									<xsl:value-of select="App/Req/Risks/RiskEndDate"/>
								</EndDate><!-- 保险止期 -->
								<LiveGetMode>
									<xsl:call-template name="tran_livegetmode">
										<xsl:with-param name="livegetmode">
											<xsl:if test="Header/EntrustWay = 01 or Header/EntrustWay = 02 or Header/EntrustWay = 04">
												<xsl:choose>
													<!-- 1累计生息，2现金领取 -->
													<xsl:when test="string($varRiskCode) = '2041'">
														1
													</xsl:when>
													<xsl:when test="string($varRiskCode) = '1029'">2</xsl:when>
													<xsl:when test="string($varRiskCode) = '2039'">
														<xsl:if test="App/Req/Risks/FullBonusGetMode ='0'">
															1
														</xsl:if>
														<xsl:if test="App/Req/Risks/FullBonusGetMode ='1'">
															2
														</xsl:if>
														<xsl:if test="App/Req/Risks/FullBonusGetMode ='4'">
															2
														</xsl:if>
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="''"/>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:if>
											<xsl:if test="Header/EntrustWay = 11">
												<xsl:choose>
													<!-- 1累计生息，2现金领取 -->
													<xsl:when test="string($varRiskCode) = '2041'">
														1
													</xsl:when>
													<xsl:when test="string($varRiskCode) = '2039'">
														<xsl:if test="App/Req/Risks/FullBonusGetMode ='0'">
															1
														</xsl:if>
														<xsl:if test="App/Req/Risks/FullBonusGetMode ='1'">
															2
														</xsl:if>
														<xsl:if test="App/Req/Risks/FullBonusGetMode ='4'">
															2
														</xsl:if>
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="''"/>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:if>
										</xsl:with-param>
									</xsl:call-template>
								</LiveGetMode>
							</Risk>
						</xsl:if>
						<xsl:if test="App/Req/Addt/Count &gt;= 4">
							<Risk>
								<xsl:variable name="varRiskCode">
									<xsl:call-template name="tran_riskCode">
										<xsl:with-param name="riskCode" select="App/Req/Addt/RiskCode4"/>
									</xsl:call-template>
								</xsl:variable>
								<xsl:variable name="varMainRiskCode">
									<xsl:call-template name="tran_riskCode">
										<xsl:with-param name="riskCode" select="App/Req/Risks/RiskCode"/>
									</xsl:call-template>
								</xsl:variable>
								<RiskCode>
									<xsl:value-of select="$varRiskCode"/>
								</RiskCode>
								<MainRiskCode>
									<xsl:value-of select="$varMainRiskCode"/>
								</MainRiskCode>
								<Amnt>
									<!--<xsl:value-of select="java:com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.yuanToFen(App/Req/Addt/Amnt4)" />-->
									<xsl:value-of select="App/Req/Addt/Amnt4"/>
								</Amnt>
								<Prem>
									<!--<xsl:value-of select="java:com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.yuanToFen(App/Req/Addt/Prem4)" />-->
									<xsl:value-of select="App/Req/Addt/Prem4"/>
								</Prem>
								<Mult>
									<xsl:value-of select="App/Req/Addt/Share4"/>
								</Mult>
								<PayIntv>
									<xsl:call-template name="tran_Contpayintv">
										<xsl:with-param name="payintv" select="App/Req/Addt/PayType4"/>
									</xsl:call-template>
								</PayIntv>
								<xsl:if test="Header/EntrustWay = 01">
									<PayMode>C</PayMode>
								</xsl:if>
								<xsl:if test="Header/EntrustWay = 04">
									<PayMode>C</PayMode>
								</xsl:if>
								<xsl:if test="Header/EntrustWay = 11">
									<PayMode>A</PayMode>
								</xsl:if>
								<xsl:if test="Header/EntrustWay = 02">
									<PayMode>A</PayMode>
								</xsl:if>
								<ExPayMode>4</ExPayMode>
								<InsuYearFlag><!-- 保险年期类型 -->
									<xsl:call-template name="tran_insuYearFlag">
										<xsl:with-param name="yearFlag" select="App/Req/Addt/InsuDueType4"/>
									</xsl:call-template>
								</InsuYearFlag>
								<InsuYear>
									<xsl:call-template name="tran_InsuYear">
										<xsl:with-param name="insuYear" select="App/Req/Addt/InsuDueDate4"/>
									</xsl:call-template>
								</InsuYear>
								<PayEndYear>
									<xsl:value-of select="App/Req/Addt/PayDueDate4"/>
								</PayEndYear>
								<PayEndYearFlag>
									<xsl:if test="App/Req/Addt/PayDueType4 != ''">
										<xsl:call-template name="tran_payendYearFlag">
											<xsl:with-param name="yearFlag" select="App/Req/Addt/PayDueType4"/>
										</xsl:call-template>
									</xsl:if>
									<xsl:if test="App/Req/Addt/PayDueType4 = ''">Y</xsl:if>
									<xsl:if test="Header/EntrustWay = 01">
										<xsl:if test="App/Req/Addt/RiskCode4 = 1029">Y</xsl:if>
										<xsl:if test="App/Req/Addt/RiskCode4 = 1022">Y</xsl:if>
										<xsl:if test="App/Req/Addt/RiskCode4 = 2036">Y</xsl:if>
										<xsl:if test="App/Req/Addt/RiskCode4 = 2044">Y</xsl:if>
									</xsl:if>
								</PayEndYearFlag>
								<BonusGetMode><!-- 红利领取方式 -->
								</BonusGetMode>
								<GetIntv><!-- 年金领取方式 /保险金领取方式-->
									<xsl:call-template name="tran_getIntv">
										<xsl:with-param name="riskCode">
											<xsl:value-of select="$varRiskCode"/>
										</xsl:with-param>
										<xsl:with-param name="getIntv">
											<xsl:value-of select="App/Req/Risks/FullBonusGetMode"/>
										</xsl:with-param>
									</xsl:call-template>
								</GetIntv>
								<RnewFlag>
									<xsl:call-template name="tran_renew">
										<xsl:with-param name="flag" select="App/Req/Addt/AutoPayFlag4" />
									</xsl:call-template>
								</RnewFlag><!-- 自动续保标志 -->
								<AutoPayFlag>
									<xsl:value-of select="App/Req/Addt/AutoPayForFlag4"/>
								</AutoPayFlag><!-- 自动垫交标志 -->
								<EndDate>
									<xsl:value-of select="App/Req/Risks/RiskEndDate"/>
								</EndDate><!-- 保险止期 -->
								<LiveGetMode>
									<xsl:call-template name="tran_livegetmode">
										<xsl:with-param name="livegetmode">
											<xsl:if test="Header/EntrustWay = 01 or Header/EntrustWay = 02 or Header/EntrustWay = 04">
												<xsl:choose>
													<!-- 1累计生息，2现金领取 -->
													<xsl:when test="string($varRiskCode) = '2041'">
														1
													</xsl:when>
													<xsl:when test="string($varRiskCode) = '1029'">2</xsl:when>
													<xsl:when test="string($varRiskCode) = '2039'">
														<xsl:if test="App/Req/Risks/FullBonusGetMode ='0'">
															1
														</xsl:if>
														<xsl:if test="App/Req/Risks/FullBonusGetMode ='1'">
															2
														</xsl:if>
														<xsl:if test="App/Req/Risks/FullBonusGetMode ='4'">
															2
														</xsl:if>
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="''"/>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:if>
											<xsl:if test="Header/EntrustWay = 11">
												<xsl:choose>
													<!-- 1累计生息，2现金领取 -->
													<xsl:when test="string($varRiskCode) = '2041'">
														1
													</xsl:when>
													<xsl:when test="string($varRiskCode) = '2039'">
														<xsl:if test="App/Req/Risks/FullBonusGetMode ='0'">
															1
														</xsl:if>
														<xsl:if test="App/Req/Risks/FullBonusGetMode ='1'">
															2
														</xsl:if>
														<xsl:if test="App/Req/Risks/FullBonusGetMode ='4'">
															2
														</xsl:if>
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="''"/>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:if>
										</xsl:with-param>
									</xsl:call-template>
								</LiveGetMode>
							</Risk>
						</xsl:if>
						<xsl:if test="App/Req/Addt/Count &gt;= 5">
							<Risk>
								<xsl:variable name="varRiskCode">
									<xsl:call-template name="tran_riskCode">
										<xsl:with-param name="riskCode" select="App/Req/Addt/RiskCode5"/>
									</xsl:call-template>
								</xsl:variable>
								<xsl:variable name="varMainRiskCode">
									<xsl:call-template name="tran_riskCode">
										<xsl:with-param name="riskCode" select="App/Req/Risks/RiskCode"/>
									</xsl:call-template>
								</xsl:variable>
								<RiskCode>
									<xsl:value-of select="$varRiskCode"/>
								</RiskCode>
								<MainRiskCode>
									<xsl:value-of select="$varMainRiskCode"/>
								</MainRiskCode>
								<Amnt>
									<!--<xsl:value-of select="java:com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.yuanToFen(App/Req/Addt/Amnt5)" />-->
									<xsl:value-of select="App/Req/Addt/Amnt5"/>
								</Amnt>
								<Prem>
									<!--<xsl:value-of select="java:com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.yuanToFen(App/Req/Addt/Prem5)" />-->
									<xsl:value-of select="App/Req/Addt/Prem5"/>
								</Prem>
								<Mult>
									<xsl:value-of select="App/Req/Addt/Share5"/>
								</Mult>
								<PayIntv>
									<xsl:call-template name="tran_Contpayintv">
										<xsl:with-param name="payintv" select="App/Req/Addt/PayType5"/>
									</xsl:call-template>
								</PayIntv>
								<xsl:if test="Header/EntrustWay = 01">
									<PayMode>C</PayMode>
								</xsl:if>
								<xsl:if test="Header/EntrustWay = 04">
									<PayMode>C</PayMode>
								</xsl:if>
								<xsl:if test="Header/EntrustWay = 11">
									<PayMode>A</PayMode>
								</xsl:if>
								<xsl:if test="Header/EntrustWay = 02">
									<PayMode>A</PayMode>
								</xsl:if>
								<ExPayMode>4</ExPayMode>
								<InsuYearFlag><!-- 保险年期类型 -->
									<xsl:call-template name="tran_insuYearFlag">
										<xsl:with-param name="yearFlag" select="App/Req/Addt/InsuDueType5"/>
									</xsl:call-template>
								</InsuYearFlag>
								<InsuYear>
									<xsl:call-template name="tran_InsuYear">
										<xsl:with-param name="insuYear" select="App/Req/Addt/InsuDueDate5"/>
									</xsl:call-template>
								</InsuYear>
								<PayEndYear>
									<xsl:value-of select="App/Req/Addt/PayDueDate5"/>
								</PayEndYear>
								<PayEndYearFlag>
									<xsl:if test="App/Req/Addt/PayDueType5 != ''">
										<xsl:call-template name="tran_payendYearFlag">
											<xsl:with-param name="yearFlag" select="App/Req/Addt/PayDueType5"/>
										</xsl:call-template>
									</xsl:if>
									<xsl:if test="App/Req/Addt/PayDueType5 = ''">Y</xsl:if>
									<xsl:if test="Header/EntrustWay = 01">
										<xsl:if test="App/Req/Addt/RiskCode5 = 1029">Y</xsl:if>
										<xsl:if test="App/Req/Addt/RiskCode5 = 1022">Y</xsl:if>
										<xsl:if test="App/Req/Addt/RiskCode5 = 2036">Y</xsl:if>
										<xsl:if test="App/Req/Addt/RiskCode5 = 2044">Y</xsl:if>
									</xsl:if>
								</PayEndYearFlag>
								<BonusGetMode><!-- 红利领取方式 -->
								</BonusGetMode>
								<GetIntv><!-- 年金领取方式 /保险金领取方式-->
									<xsl:call-template name="tran_getIntv">
										<xsl:with-param name="riskCode">
											<xsl:value-of select="$varRiskCode"/>
										</xsl:with-param>
										<xsl:with-param name="getIntv">
											<xsl:value-of select="App/Req/Risks/FullBonusGetMode"/>
										</xsl:with-param>
									</xsl:call-template>
								</GetIntv>
								<RnewFlag>
									<xsl:call-template name="tran_renew">
										<xsl:with-param name="flag" select="App/Req/Addt/AutoPayFlag5" />
									</xsl:call-template>
								</RnewFlag><!-- 自动续保标志 -->
								<AutoPayFlag>
									<xsl:value-of select="App/Req/Addt/AutoPayForFlag5"/>
								</AutoPayFlag><!-- 自动垫交标志 -->
								<EndDate>
									<xsl:value-of select="App/Req/Risks/RiskEndDate"/>
								</EndDate><!-- 保险止期 -->
								<LiveGetMode>
									<xsl:call-template name="tran_livegetmode">
										<xsl:with-param name="livegetmode">
											<xsl:if test="Header/EntrustWay = 01 or Header/EntrustWay = 02 or Header/EntrustWay = 04">
												<xsl:choose>
													<!-- 1累计生息，2现金领取 -->
													<xsl:when test="string($varRiskCode) = '2041'">
														1
													</xsl:when>
													<xsl:when test="string($varRiskCode) = '1029'">2</xsl:when>
													<xsl:when test="string($varRiskCode) = '2039'">
														<xsl:if test="App/Req/Risks/FullBonusGetMode ='0'">
															1
														</xsl:if>
														<xsl:if test="App/Req/Risks/FullBonusGetMode ='1'">
															2
														</xsl:if>
														<xsl:if test="App/Req/Risks/FullBonusGetMode ='4'">
															2
														</xsl:if>
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="''"/>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:if>
											<xsl:if test="Header/EntrustWay = 11">
												<xsl:choose>
													<!-- 1累计生息，2现金领取 -->
													<xsl:when test="string($varRiskCode) = '2041'">
														1
													</xsl:when>
													<xsl:when test="string($varRiskCode) = '2039'">
														<xsl:if test="App/Req/Risks/FullBonusGetMode ='0'">
															1
														</xsl:if>
														<xsl:if test="App/Req/Risks/FullBonusGetMode ='1'">
															2
														</xsl:if>
														<xsl:if test="App/Req/Risks/FullBonusGetMode ='4'">
															2
														</xsl:if>
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="''"/>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:if>
										</xsl:with-param>
									</xsl:call-template>
								</LiveGetMode>
							</Risk>
						</xsl:if>
						<xsl:if test="App/Req/Addt/Count &gt;= 6">
							<Risk>
								<xsl:variable name="varRiskCode">
									<xsl:call-template name="tran_riskCode">
										<xsl:with-param name="riskCode" select="App/Req/Addt/RiskCode6"/>
									</xsl:call-template>
								</xsl:variable>
								<xsl:variable name="varMainRiskCode">
									<xsl:call-template name="tran_riskCode">
										<xsl:with-param name="riskCode" select="App/Req/Risks/RiskCode"/>
									</xsl:call-template>
								</xsl:variable>
								<RiskCode>
									<xsl:value-of select="$varRiskCode"/>
								</RiskCode>
								<MainRiskCode>
									<xsl:value-of select="$varMainRiskCode"/>
								</MainRiskCode>
								<Amnt>
									<!--<xsl:value-of select="java:com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.yuanToFen(App/Req/Addt/Amnt6)" />-->
									<xsl:value-of select="App/Req/Addt/Amnt6"/>
								</Amnt>
								<Prem>
									<!--<xsl:value-of select="java:com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.yuanToFen(App/Req/Addt/Prem6)" />-->
									<xsl:value-of select="App/Req/Addt/Prem6"/>
								</Prem>
								<Mult>
									<xsl:value-of select="App/Req/Addt/Share6"/>
								</Mult>
								<PayIntv>
									<xsl:call-template name="tran_Contpayintv">
										<xsl:with-param name="payintv" select="App/Req/Addt/PayType6"/>
									</xsl:call-template>
								</PayIntv>
								<xsl:if test="Header/EntrustWay = 01">
									<PayMode>C</PayMode>
								</xsl:if>
								<xsl:if test="Header/EntrustWay = 04">
									<PayMode>C</PayMode>
								</xsl:if>
								<xsl:if test="Header/EntrustWay = 11">
									<PayMode>A</PayMode>
								</xsl:if>
								<xsl:if test="Header/EntrustWay = 02">
									<PayMode>A</PayMode>
								</xsl:if>
								<ExPayMode>4</ExPayMode>
								<InsuYearFlag><!-- 保险年期类型 -->
									<xsl:call-template name="tran_insuYearFlag">
										<xsl:with-param name="yearFlag" select="App/Req/Addt/InsuDueType6"/>
									</xsl:call-template>
								</InsuYearFlag>
								<InsuYear>
									<xsl:call-template name="tran_InsuYear">
										<xsl:with-param name="insuYear" select="App/Req/Addt/InsuDueDate6"/>
									</xsl:call-template>
								</InsuYear>
								<PayEndYear>
									<xsl:value-of select="App/Req/Addt/PayDueDate6"/>
								</PayEndYear>
								<PayEndYearFlag>
									<xsl:if test="App/Req/Addt/PayDueType6 != ''">
										<xsl:call-template name="tran_payendYearFlag">
											<xsl:with-param name="yearFlag" select="App/Req/Addt/PayDueType6"/>
										</xsl:call-template>
									</xsl:if>
									<xsl:if test="App/Req/Addt/PayDueType6 = ''">Y</xsl:if>
									<xsl:if test="Header/EntrustWay = 01">
										<xsl:if test="App/Req/Addt/RiskCode6 = 1029">Y</xsl:if>
										<xsl:if test="App/Req/Addt/RiskCode6 = 1022">Y</xsl:if>
										<xsl:if test="App/Req/Addt/RiskCode6 = 2036">Y</xsl:if>
										<xsl:if test="App/Req/Addt/RiskCode6 = 2044">Y</xsl:if>
									</xsl:if>
								</PayEndYearFlag>
								<BonusGetMode><!-- 红利领取方式 -->
								</BonusGetMode>
								<GetIntv><!-- 年金领取方式 /保险金领取方式-->
									<xsl:call-template name="tran_getIntv">
										<xsl:with-param name="riskCode">
											<xsl:value-of select="$varRiskCode"/>
										</xsl:with-param>
										<xsl:with-param name="getIntv">
											<xsl:value-of select="App/Req/Risks/FullBonusGetMode"/>
										</xsl:with-param>
									</xsl:call-template>
								</GetIntv>
								<RnewFlag>
									<xsl:call-template name="tran_renew">
										<xsl:with-param name="flag" select="App/Req/Addt/AutoPayFlag6" />
									</xsl:call-template>
								</RnewFlag><!-- 自动续保标志 -->
								<AutoPayFlag>
									<xsl:value-of select="App/Req/Addt/AutoPayForFlag6"/>
								</AutoPayFlag><!-- 自动垫交标志 -->
								<EndDate>
									<xsl:value-of select="App/Req/Risks/RiskEndDate"/>
								</EndDate><!-- 保险止期 -->
								<LiveGetMode>
									<xsl:call-template name="tran_livegetmode">
										<xsl:with-param name="livegetmode">
											<xsl:if test="Header/EntrustWay = 01 or Header/EntrustWay = 02 or Header/EntrustWay = 04">
												<xsl:choose>
													<!-- 1累计生息，2现金领取 -->
													<xsl:when test="string($varRiskCode) = '2041'">
														1
													</xsl:when>
													<xsl:when test="string($varRiskCode) = '1029'">2</xsl:when>
													<xsl:when test="string($varRiskCode) = '2039'">
														<xsl:if test="App/Req/Risks/FullBonusGetMode ='0'">
															1
														</xsl:if>
														<xsl:if test="App/Req/Risks/FullBonusGetMode ='1'">
															2
														</xsl:if>
														<xsl:if test="App/Req/Risks/FullBonusGetMode ='4'">
															2
														</xsl:if>
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="''"/>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:if>
											<xsl:if test="Header/EntrustWay = 11">
												<xsl:choose>
													<!-- 1累计生息，2现金领取 -->
													<xsl:when test="string($varRiskCode) = '2041'">
														1
													</xsl:when>
													<xsl:when test="string($varRiskCode) = '2039'">
														<xsl:if test="App/Req/Risks/FullBonusGetMode ='0'">
															1
														</xsl:if>
														<xsl:if test="App/Req/Risks/FullBonusGetMode ='1'">
															2
														</xsl:if>
														<xsl:if test="App/Req/Risks/FullBonusGetMode ='4'">
															2
														</xsl:if>
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="''"/>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:if>
										</xsl:with-param>
									</xsl:call-template>
								</LiveGetMode>
							</Risk>
						</xsl:if>
					</xsl:when>
					</xsl:choose>
				</Risks>
				<CustomerImparts>
					<CustomerImpart>
						<ImpartObject>dx0002</ImpartObject>
						<ImpartType>lx003</ImpartType>
						<ImpartCode>cw0001</ImpartCode>
						<ImpartParamModle>
							<xsl:value-of select="java:com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.appntFamilyIncomeFormat(App/Req/Reserve2)"/>
							<!--<xsl:call-template name="tran_YuanToWanYuan">
								<xsl:with-param name="number" select="App/Req/Reserve2"/>
							</xsl:call-template>-->
						</ImpartParamModle>
					</CustomerImpart>
					<CustomerImpart>
						<ImpartObject>dx0002</ImpartObject>
						<ImpartType>lx003</ImpartType>
						<ImpartCode>cw0002</ImpartCode>
						<ImpartParamModle>
							<xsl:value-of select="java:com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.appntIncomeFormat(Header/EntrustWay,App/Req/Appl/AnnualIncome)"/>
						</ImpartParamModle>
					</CustomerImpart>
					<xsl:for-each select="$Insu">
						<CustomerImpart>
							<ImpartObject>
								<xsl:value-of select="concat('dx000',2+position())" />
							</ImpartObject>
							<ImpartType>lx001</ImpartType>
							<ImpartCode>jk0002</ImpartCode>
							<ImpartParamModle>
								<xsl:call-template name="tran_healthcode">
									<xsl:with-param name="healthCode">
										<xsl:value-of select="$Insu[string-length(HealthNotice)>0]/HealthNotice"/>
									</xsl:with-param>
								</xsl:call-template>
							</ImpartParamModle>
						</CustomerImpart>
						<CustomerImpart>
							<ImpartObject>
								<xsl:value-of select="concat('dx000',2+position())" />
							</ImpartObject>
							<ImpartType>lx003</ImpartType>
							<ImpartCode>cw0002</ImpartCode>
							<ImpartParamModle>
								<xsl:value-of select="java:com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.appntIncomeFormat($Header/EntrustWay,$Insu/AnnualIncome)"/>
							</ImpartParamModle>
						</CustomerImpart>
						<CustomerImpart>
							<ImpartObject>
								<xsl:value-of select="concat('dx000',2+position())" />
							</ImpartObject>
							<ImpartType>lx001</ImpartType>
							<ImpartCode>jk0003</ImpartCode>
							<ImpartParamModle>
								<xsl:value-of select="$Insu/Tall"/>
							</ImpartParamModle>
						</CustomerImpart>
						<CustomerImpart>
							<ImpartObject>
								<xsl:value-of select="concat('dx000',2+position())" />
							</ImpartObject>
							<ImpartType>lx001</ImpartType>
							<ImpartCode>jk0004</ImpartCode>
							<ImpartParamModle>
								<xsl:value-of select="$Insu/Weight"/>
							</ImpartParamModle>
						</CustomerImpart>
					</xsl:for-each>
				</CustomerImparts>
			</Body>
		</TranData>
	</xsl:template>
		<!-- 健康告知 -->
	<xsl:template name="tran_Health">
		<xsl:param name="Health"/>
		<xsl:choose>
			<xsl:when test="$Health='1'">
				<xsl:value-of select="'Y'" />
			</xsl:when>
			<xsl:when test="$Health='0'">
				<xsl:value-of select="'N'" />
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="''" />
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<!-- 性别 -->
	<xsl:template name="tran_sex">
		<xsl:param name="sex">0</xsl:param>
		<xsl:if test="$sex = 0">0</xsl:if><!-- 男 -->
		<xsl:if test="$sex = 1">1</xsl:if><!-- 女 -->
	</xsl:template>

	<!-- 保单的缴费方式 -->
	<xsl:template name="tran_Contpayintv">
		<xsl:param name="payintv">1</xsl:param>
		<xsl:if test="$payintv = '1'">0</xsl:if>
		<xsl:if test="$payintv = '2'">1</xsl:if>
		<xsl:if test="$payintv = '5'">12</xsl:if>
		<xsl:if test="$payintv = ''">2147483647</xsl:if>
	</xsl:template>
	
	<!-- 保障年期/年龄标志 -->
	<xsl:template name="tran_insuYearFlag">
		<xsl:param name="yearFlag">4</xsl:param>
		<xsl:if test="$yearFlag = 0"></xsl:if>
		<xsl:if test="$yearFlag = 1"></xsl:if>
		<xsl:if test="$yearFlag = 2"></xsl:if>
		<xsl:if test="$yearFlag = 3"></xsl:if>
		<xsl:if test="$yearFlag = 4">Y</xsl:if>
		<xsl:if test="$yearFlag = 5">A</xsl:if>
		<xsl:if test="$yearFlag = 6">A</xsl:if>
	</xsl:template>

	<xsl:template name="tran_InsuYear">
		<xsl:param name="insuYear" />
		<xsl:choose>
			<xsl:when test="$insuYear=199">106</xsl:when>
			<xsl:otherwise><xsl:value-of select="$insuYear" /></xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<!-- 证件类型  -->
	<xsl:template name="tran_idtype">
		<xsl:param name="idtype" />
		<xsl:if test="$idtype = 1">0</xsl:if>
		<xsl:if test="$idtype = 2">1</xsl:if>
		<xsl:if test="$idtype = 3">2</xsl:if>
		<xsl:if test="$idtype = 4">8</xsl:if>
		<xsl:if test="$idtype = 5">8</xsl:if>
		<xsl:if test="$idtype = 0">9</xsl:if>
		<xsl:if test="$idtype = 110001">0</xsl:if>
		<xsl:if test="$idtype = 110002">0</xsl:if>
		<xsl:if test="$idtype = 110003">12</xsl:if>
		<xsl:if test="$idtype = 110004">12</xsl:if>
		<xsl:if test="$idtype = 110005">4</xsl:if>
		<xsl:if test="$idtype = 110006">4</xsl:if>
		<xsl:if test="$idtype = 110011">8</xsl:if>
		<xsl:if test="$idtype = 110012">8</xsl:if>
		<xsl:if test="$idtype = 110013">2</xsl:if>
		<xsl:if test="$idtype = 110014">2</xsl:if>
		<xsl:if test="$idtype = 110015">8</xsl:if>
		<xsl:if test="$idtype = 110016">8</xsl:if>
		<xsl:if test="$idtype = 110017">8</xsl:if>
		<xsl:if test="$idtype = 110018">8</xsl:if>
		<xsl:if test="$idtype = 110019">15</xsl:if>
		<xsl:if test="$idtype = 110020">15</xsl:if>
		<xsl:if test="$idtype = 110021">16</xsl:if>
		<xsl:if test="$idtype = 110022">16</xsl:if>
		<xsl:if test="$idtype = 110023">1</xsl:if>
		<xsl:if test="$idtype = 110024">1</xsl:if>
		<xsl:if test="$idtype = 110025">1</xsl:if>
		<xsl:if test="$idtype = 110026">1</xsl:if>
		<xsl:if test="$idtype = 110027">2</xsl:if>
		<xsl:if test="$idtype = 110028">2</xsl:if>
		<xsl:if test="$idtype = 110029">8</xsl:if>
		<xsl:if test="$idtype = 110030">8</xsl:if>
		<xsl:if test="$idtype = 110031">13</xsl:if>
		<xsl:if test="$idtype = 110032">13</xsl:if>
		<xsl:if test="$idtype = 110033">2</xsl:if>
		<xsl:if test="$idtype = 110034">2</xsl:if>
		<xsl:if test="$idtype = 110035">13</xsl:if>
		<xsl:if test="$idtype = 110036">13</xsl:if>
		<xsl:if test="$idtype = 119998">8</xsl:if>
		<xsl:if test="$idtype = 119999">8</xsl:if>
		<xsl:if test="$idtype = 110037">17</xsl:if>
	</xsl:template>


	<!-- 缴费年期/年龄类型    暂时没用 -->
	<xsl:template name="tran_payendYearFlag">
		<xsl:param name="yearFlag">4</xsl:param>
		<xsl:if test="$yearFlag = 0"></xsl:if>
		<xsl:if test="$yearFlag = 1">A</xsl:if>
		<xsl:if test="$yearFlag = 2"></xsl:if>
		<xsl:if test="$yearFlag = 3"></xsl:if>
		<xsl:if test="$yearFlag = 4">Y</xsl:if>
		<xsl:if test="$yearFlag = 5"></xsl:if>
		<xsl:if test="$yearFlag = 6"></xsl:if>
	</xsl:template>
	<!-- 缴费形式 -->

	<!--国籍转换-->
	<xsl:template name="tran_nationality">
		<xsl:param name="nationality">156</xsl:param>
		<xsl:param name="Flag"></xsl:param>
		<xsl:choose>
			<xsl:when test="$Flag = 01">CHN</xsl:when>
			<xsl:when test="$nationality = 	004">AFG</xsl:when>
			<xsl:when test="$nationality = 	008">ALB</xsl:when>
			<xsl:when test="$nationality = 	010">ATA</xsl:when>
			<xsl:when test="$nationality = 	012">DZA</xsl:when>
			<xsl:when test="$nationality = 	016">ASM</xsl:when>
			<xsl:when test="$nationality = 	020">AND</xsl:when>
			<xsl:when test="$nationality = 	024">AGO</xsl:when>
			<xsl:when test="$nationality = 	028">ATG</xsl:when>
			<xsl:when test="$nationality = 	031">AZE</xsl:when>
			<xsl:when test="$nationality = 	032">ARG</xsl:when>
			<xsl:when test="$nationality = 	036">AUS</xsl:when>
			<xsl:when test="$nationality = 	040">AUT</xsl:when>
			<xsl:when test="$nationality = 	044">BHS</xsl:when>
			<xsl:when test="$nationality = 	048">BHR</xsl:when>
			<xsl:when test="$nationality = 	050">BGD</xsl:when>
			<xsl:when test="$nationality = 	051">ARM</xsl:when>
			<xsl:when test="$nationality = 	052">BRB</xsl:when>
			<xsl:when test="$nationality = 	056">BEL</xsl:when>
			<xsl:when test="$nationality = 	060">BMU</xsl:when>
			<xsl:when test="$nationality = 	064">BTN</xsl:when>
			<xsl:when test="$nationality = 	068">BOL</xsl:when>
			<xsl:when test="$nationality = 	070">BIH</xsl:when>
			<xsl:when test="$nationality = 	072">BWA</xsl:when>
			<xsl:when test="$nationality = 	074">BVT</xsl:when>
			<xsl:when test="$nationality = 	076">BRA</xsl:when>
			<xsl:when test="$nationality = 	084">BLZ</xsl:when>
			<xsl:when test="$nationality = 	086">IOT</xsl:when>
			<xsl:when test="$nationality = 	090">SLB</xsl:when>
			<xsl:when test="$nationality = 	092">VGB</xsl:when>
			<xsl:when test="$nationality = 	096">BRN</xsl:when>
			<xsl:when test="$nationality = 	100">BGR</xsl:when>
			<xsl:when test="$nationality = 	104">MMR</xsl:when>
			<xsl:when test="$nationality = 	108">BDI</xsl:when>
			<xsl:when test="$nationality = 	112">BLR</xsl:when>
			<xsl:when test="$nationality = 	116">KHM</xsl:when>
			<xsl:when test="$nationality = 	120">CMR</xsl:when>
			<xsl:when test="$nationality = 	124">CAN</xsl:when>
			<xsl:when test="$nationality = 	132">CPV</xsl:when>
			<xsl:when test="$nationality = 	136">CYM</xsl:when>
			<xsl:when test="$nationality = 	140">CAF</xsl:when>
			<xsl:when test="$nationality = 	144">LKA</xsl:when>
			<xsl:when test="$nationality = 	148">TCD</xsl:when>
			<xsl:when test="$nationality = 	152">CHL</xsl:when>
			<xsl:when test="$nationality = 	156">CHN</xsl:when>
			<xsl:when test="$nationality = 	158">TWN</xsl:when>
			<xsl:when test="$nationality = 	162">CSR</xsl:when>
			<xsl:when test="$nationality = 	166">CCK</xsl:when>
			<xsl:when test="$nationality = 	170">COL</xsl:when>
			<xsl:when test="$nationality = 	174">COM</xsl:when>
			<xsl:when test="$nationality = 	175">MYT</xsl:when>
			<xsl:when test="$nationality = 	178">COG</xsl:when>
			<xsl:when test="$nationality = 	180">COD</xsl:when>
			<xsl:when test="$nationality = 	184">COK</xsl:when>
			<xsl:when test="$nationality = 	188">CR</xsl:when>
			<xsl:when test="$nationality = 	191">HRV</xsl:when>
			<xsl:when test="$nationality = 	192">CUB</xsl:when>
			<xsl:when test="$nationality = 	196">CYP</xsl:when>
			<xsl:when test="$nationality = 	203">CZE</xsl:when>
			<xsl:when test="$nationality = 	204">BEN</xsl:when>
			<xsl:when test="$nationality = 	208">DNK</xsl:when>
			<xsl:when test="$nationality = 	212">DMA</xsl:when>
			<xsl:when test="$nationality = 	214">DOM</xsl:when>
			<xsl:when test="$nationality = 	218">ECU</xsl:when>
			<xsl:when test="$nationality = 	222">SLV</xsl:when>
			<xsl:when test="$nationality = 	226">GNQ</xsl:when>
			<xsl:when test="$nationality = 	231">ETH</xsl:when>
			<xsl:when test="$nationality = 	232">ERI</xsl:when>
			<xsl:when test="$nationality = 	233">EST</xsl:when>
			<xsl:when test="$nationality = 	234">FRO</xsl:when>
			<xsl:when test="$nationality = 	238">FLK</xsl:when>
			<xsl:when test="$nationality = 	239">SGS</xsl:when>
			<xsl:when test="$nationality = 	242">FJI</xsl:when>
			<xsl:when test="$nationality = 	246">FIN</xsl:when>
			<xsl:when test="$nationality = 	248">ALA</xsl:when>
			<xsl:when test="$nationality = 	250">FRA</xsl:when>
			<xsl:when test="$nationality = 	254">GUF</xsl:when>
			<xsl:when test="$nationality = 	258">PYF</xsl:when>
			<xsl:when test="$nationality = 	260">ATF</xsl:when>
			<xsl:when test="$nationality = 	262">DJI</xsl:when>
			<xsl:when test="$nationality = 	266">GAB</xsl:when>
			<xsl:when test="$nationality = 	268">GEO</xsl:when>
			<xsl:when test="$nationality = 	270">GMB</xsl:when>
			<xsl:when test="$nationality = 	275">PST</xsl:when>
			<xsl:when test="$nationality = 	276">DEU</xsl:when>
			<xsl:when test="$nationality = 	288">GHA</xsl:when>
			<xsl:when test="$nationality = 	292">GIB</xsl:when>
			<xsl:when test="$nationality = 	296">KIR</xsl:when>
			<xsl:when test="$nationality = 	300">GRC</xsl:when>
			<xsl:when test="$nationality = 	304">GRL</xsl:when>
			<xsl:when test="$nationality = 	308">GRD</xsl:when>
			<xsl:when test="$nationality = 	312">GLP</xsl:when>
			<xsl:when test="$nationality = 	316">GUM</xsl:when>
			<xsl:when test="$nationality = 	320">GTM</xsl:when>
			<xsl:when test="$nationality = 	324">GIN</xsl:when>
			<xsl:when test="$nationality = 	328">GUY</xsl:when>
			<xsl:when test="$nationality = 	332">HTI</xsl:when>
			<xsl:when test="$nationality = 	334">HMD</xsl:when>
			<xsl:when test="$nationality = 	336">VAT</xsl:when>
			<xsl:when test="$nationality = 	340">HND</xsl:when>
			<xsl:when test="$nationality = 	344">HKG</xsl:when>
			<xsl:when test="$nationality = 	348">HUN</xsl:when>
			<xsl:when test="$nationality = 	352">ISL</xsl:when>
			<xsl:when test="$nationality = 	356">IND</xsl:when>
			<xsl:when test="$nationality = 	360">IDN</xsl:when>
			<xsl:when test="$nationality = 	364">IRN</xsl:when>
			<xsl:when test="$nationality = 	368">IRQ</xsl:when>
			<xsl:when test="$nationality = 	372">IRL</xsl:when>
			<xsl:when test="$nationality = 	376">ISR</xsl:when>
			<xsl:when test="$nationality = 	380">ITA</xsl:when>
			<xsl:when test="$nationality = 	384">CIV</xsl:when>
			<xsl:when test="$nationality = 	388">JAM</xsl:when>
			<xsl:when test="$nationality = 	392">JPN</xsl:when>
			<xsl:when test="$nationality = 	398">KAZ</xsl:when>
			<xsl:when test="$nationality = 	400">JOR</xsl:when>
			<xsl:when test="$nationality = 	404">KEN</xsl:when>
			<xsl:when test="$nationality = 	408">PRK</xsl:when>
			<xsl:when test="$nationality = 	410">KOR</xsl:when>
			<xsl:when test="$nationality = 	414">KWT</xsl:when>
			<xsl:when test="$nationality = 	417">KGZ</xsl:when>
			<xsl:when test="$nationality = 	418">LAO</xsl:when>
			<xsl:when test="$nationality = 	422">LBN</xsl:when>
			<xsl:when test="$nationality = 	426">LSO</xsl:when>
			<xsl:when test="$nationality = 	428">LVA</xsl:when>
			<xsl:when test="$nationality = 	430">LBR</xsl:when>
			<xsl:when test="$nationality = 	434">LBY</xsl:when>
			<xsl:when test="$nationality = 	438">LIE</xsl:when>
			<xsl:when test="$nationality = 	440">LTU</xsl:when>
			<xsl:when test="$nationality = 	442">LUX</xsl:when>
			<xsl:when test="$nationality = 	446">MAC</xsl:when>
			<xsl:when test="$nationality = 	450">MDG</xsl:when>
			<xsl:when test="$nationality = 	454">MWI</xsl:when>
			<xsl:when test="$nationality = 	458">MYS</xsl:when>
			<xsl:when test="$nationality = 	462">MDV</xsl:when>
			<xsl:when test="$nationality = 	466">MLI</xsl:when>
			<xsl:when test="$nationality = 	470">MLT</xsl:when>
			<xsl:when test="$nationality = 	474">MTQ</xsl:when>
			<xsl:when test="$nationality = 	478">MRT</xsl:when>
			<xsl:when test="$nationality = 	480">MUS</xsl:when>
			<xsl:when test="$nationality = 	484">MEX</xsl:when>
			<xsl:when test="$nationality = 	492">MCO</xsl:when>
			<xsl:when test="$nationality = 	496">MNG</xsl:when>
			<xsl:when test="$nationality = 	498">MDA</xsl:when>
			<xsl:when test="$nationality = 	499">MNE</xsl:when>
			<xsl:when test="$nationality = 	500">MSR</xsl:when>
			<xsl:when test="$nationality = 	504">MAR</xsl:when>
			<xsl:when test="$nationality = 	508">MOZ</xsl:when>
			<xsl:when test="$nationality = 	512">OMN</xsl:when>
			<xsl:when test="$nationality = 	516">NAM</xsl:when>
			<xsl:when test="$nationality = 	520">NRU</xsl:when>
			<xsl:when test="$nationality = 	524">NPL</xsl:when>
			<xsl:when test="$nationality = 	528">NLD</xsl:when>
			<xsl:when test="$nationality = 	530">ANT</xsl:when>
			<xsl:when test="$nationality = 	531">OTH</xsl:when>
			<xsl:when test="$nationality = 	533">ABW</xsl:when>
			<xsl:when test="$nationality = 	534">OTH</xsl:when>
			<xsl:when test="$nationality = 	535">OTH</xsl:when>
			<xsl:when test="$nationality = 	540">NCL</xsl:when>
			<xsl:when test="$nationality = 	548">VUT</xsl:when>
			<xsl:when test="$nationality = 	554">NZL</xsl:when>
			<xsl:when test="$nationality = 	558">NIC</xsl:when>
			<xsl:when test="$nationality = 	562">NER</xsl:when>
			<xsl:when test="$nationality = 	566">NGA</xsl:when>
			<xsl:when test="$nationality = 	570">NIU</xsl:when>
			<xsl:when test="$nationality = 	574">NFK</xsl:when>
			<xsl:when test="$nationality = 	578">NOR</xsl:when>
			<xsl:when test="$nationality = 	580">MNP</xsl:when>
			<xsl:when test="$nationality = 	581">UMI</xsl:when>
			<xsl:when test="$nationality = 	583">FSM</xsl:when>
			<xsl:when test="$nationality = 	584">MHL</xsl:when>
			<xsl:when test="$nationality = 	585">PLW</xsl:when>
			<xsl:when test="$nationality = 	586">PAK</xsl:when>
			<xsl:when test="$nationality = 	591">PAN</xsl:when>
			<xsl:when test="$nationality = 	598">PNG</xsl:when>
			<xsl:when test="$nationality = 	600">PRY</xsl:when>
			<xsl:when test="$nationality = 	604">PER</xsl:when>
			<xsl:when test="$nationality = 	608">PHL</xsl:when>
			<xsl:when test="$nationality = 	612">PCN</xsl:when>
			<xsl:when test="$nationality = 	616">POL</xsl:when>
			<xsl:when test="$nationality = 	620">PRT</xsl:when>
			<xsl:when test="$nationality = 	624">GNB</xsl:when>
			<xsl:when test="$nationality = 	626">TMP</xsl:when>
			<xsl:when test="$nationality = 	630">PRI</xsl:when>
			<xsl:when test="$nationality = 	634">QAT</xsl:when>
			<xsl:when test="$nationality = 	638">REU</xsl:when>
			<xsl:when test="$nationality = 	642">ROM</xsl:when>
			<xsl:when test="$nationality = 	643">RUS</xsl:when>
			<xsl:when test="$nationality = 	646">RWA</xsl:when>
			<xsl:when test="$nationality = 	652">BLM</xsl:when>
			<xsl:when test="$nationality = 	654">SHN</xsl:when>
			<xsl:when test="$nationality = 	659">KNA</xsl:when>
			<xsl:when test="$nationality = 	660">AIA</xsl:when>
			<xsl:when test="$nationality = 	662">LCA</xsl:when>
			<xsl:when test="$nationality = 	663">MAF</xsl:when>
			<xsl:when test="$nationality = 	666">SPM</xsl:when>
			<xsl:when test="$nationality = 	670">VCT</xsl:when>
			<xsl:when test="$nationality = 	674">SMR</xsl:when>
			<xsl:when test="$nationality = 	678">STp</xsl:when>
			<xsl:when test="$nationality = 	682">SAU</xsl:when>
			<xsl:when test="$nationality = 	686">SEN</xsl:when>
			<xsl:when test="$nationality = 	688">SRB</xsl:when>
			<xsl:when test="$nationality = 	690">SYC</xsl:when>
			<xsl:when test="$nationality = 	694">SLE</xsl:when>
			<xsl:when test="$nationality = 	702">SGP</xsl:when>
			<xsl:when test="$nationality = 	703">SVK</xsl:when>
			<xsl:when test="$nationality = 	704">VNM</xsl:when>
			<xsl:when test="$nationality = 	705">SVN</xsl:when>
			<xsl:when test="$nationality = 	706">SOM</xsl:when>
			<xsl:when test="$nationality = 	710">ZAF</xsl:when>
			<xsl:when test="$nationality = 	716">ZWE</xsl:when>
			<xsl:when test="$nationality = 	724">ESP</xsl:when>
			<xsl:when test="$nationality = 	728">SSD</xsl:when>
			<xsl:when test="$nationality = 	729">SDN</xsl:when>
			<xsl:when test="$nationality = 	732">ESH</xsl:when>
			<xsl:when test="$nationality = 	736">OTH</xsl:when>
			<xsl:when test="$nationality = 	740">SUR</xsl:when>
			<xsl:when test="$nationality = 	744">SJM</xsl:when>
			<xsl:when test="$nationality = 	748">SWZ</xsl:when>
			<xsl:when test="$nationality = 	752">SWE</xsl:when>
			<xsl:when test="$nationality = 	756">CHE</xsl:when>
			<xsl:when test="$nationality = 	760">SYR</xsl:when>
			<xsl:when test="$nationality = 	762">TJK</xsl:when>
			<xsl:when test="$nationality = 	764">THA</xsl:when>
			<xsl:when test="$nationality = 	768">TGO</xsl:when>
			<xsl:when test="$nationality = 	772">TKL</xsl:when>
			<xsl:when test="$nationality = 	776">TON</xsl:when>
			<xsl:when test="$nationality = 	780">TTO</xsl:when>
			<xsl:when test="$nationality = 	784">ARE</xsl:when>
			<xsl:when test="$nationality = 	788">TUN</xsl:when>
			<xsl:when test="$nationality = 	792">TUR</xsl:when>
			<xsl:when test="$nationality = 	795">TKM</xsl:when>
			<xsl:when test="$nationality = 	796">TCA</xsl:when>
			<xsl:when test="$nationality = 	798">TUV</xsl:when>
			<xsl:when test="$nationality = 	800">UGA</xsl:when>
			<xsl:when test="$nationality = 	804">UKR</xsl:when>
			<xsl:when test="$nationality = 	807">MKD</xsl:when>
			<xsl:when test="$nationality = 	818">EGY</xsl:when>
			<xsl:when test="$nationality = 	826">GBR</xsl:when>
			<xsl:when test="$nationality = 	831">GGY</xsl:when>
			<xsl:when test="$nationality = 	832">OTH</xsl:when>
			<xsl:when test="$nationality = 	833">IMN</xsl:when>
			<xsl:when test="$nationality = 	834">TZA</xsl:when>
			<xsl:when test="$nationality = 	840">USA</xsl:when>
			<xsl:when test="$nationality = 	850">VIR</xsl:when>
			<xsl:when test="$nationality = 	854">BFA</xsl:when>
			<xsl:when test="$nationality = 	858">URY</xsl:when>
			<xsl:when test="$nationality = 	860">UZB</xsl:when>
			<xsl:when test="$nationality = 	862">VEN</xsl:when>
			<xsl:when test="$nationality = 	876">WLF</xsl:when>
			<xsl:when test="$nationality = 	882">WSM</xsl:when>
			<xsl:when test="$nationality = 	887">YEM</xsl:when>
			<xsl:when test="$nationality = 	891">YUG</xsl:when>
			<xsl:when test="$nationality = 	894">ZMB</xsl:when>
			<xsl:when test="$nationality = 	998">OTH</xsl:when>
			<xsl:when test="$nationality = 	999">OTH</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="''" />
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<!-- 职业类别 -->
	<xsl:template name="tran_idjobCode">
		<xsl:param name="idjobCode"></xsl:param>
		<xsl:if test="$idjobCode = 01">0101002</xsl:if><!-- 农夫 -->
		<xsl:if test="$idjobCode = 02">0001003</xsl:if><!-- 企\事业单位负责人 -->
		<xsl:if test="$idjobCode = 03">0001001</xsl:if><!-- 机关团体公司行号内勤人员 -->
		<xsl:if test="$idjobCode = 04">0101008</xsl:if><!-- xxx技术人员 -->
		<xsl:if test="$idjobCode = 05">0501008</xsl:if><!-- 客运车司机及服务员 -->
		<xsl:if test="$idjobCode = 06">1202002</xsl:if><!-- 商业店员 -->
		<xsl:if test="$idjobCode = 07">2147004</xsl:if><!-- 无业人员 -->
	</xsl:template>

	<!-- 渠道 -->
	<xsl:template name="tran_Type">
		<xsl:param name="Type"></xsl:param>
		<xsl:if test="$Type = 11">ybt</xsl:if>
		<xsl:if test="$Type = 01">wy</xsl:if>
		<xsl:if test="$Type = 04">atm</xsl:if>
		<xsl:if test="$Type = 02">zy</xsl:if>
	</xsl:template>

	<!-- 健康告知代码转换 -->
	<xsl:template name="tran_healthcode">
		<!-- Y 有健康告知，拒保，N可以正常投保 -->
		<xsl:param name="healthCode" />
		<xsl:choose>
			<xsl:when test="$healthCode='1'">
				<xsl:value-of select="'Y'" />
			</xsl:when>
			<xsl:when test="$healthCode='0'">
				<xsl:value-of select="'N'" />
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="''" />
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<!-- 投保人年收入和家庭年收入转换 -->
	<xsl:template name="tran_YuanToWanYuan">
		<xsl:param name="number" />
		<xsl:if test="string(number($number))!='NaN'">
			<xsl:value-of
					select="$number div 10000" />
		</xsl:if>
	</xsl:template>

	<!-- 投保人与被保人关系转换 -->
	<xsl:template name="tran_rela">
		<xsl:param name="rela">00</xsl:param>
		<xsl:if test="$rela = 01">00</xsl:if>
		<xsl:if test="$rela = 02">01</xsl:if>
		<xsl:if test="$rela = 03">02</xsl:if>
		<xsl:if test="$rela = 04">03</xsl:if>
		<xsl:if test="$rela = 05">04</xsl:if>
		<xsl:if test="$rela = 06">05</xsl:if>
		<xsl:if test="$rela = 07">06</xsl:if>
		<xsl:if test="$rela = 08">07</xsl:if>
		<xsl:if test="$rela = 09">08</xsl:if>
		<xsl:if test="$rela = 10">09</xsl:if>
		<xsl:if test="$rela = 11">10</xsl:if>
		<xsl:if test="$rela = 12">11</xsl:if>
		<xsl:if test="$rela = 13">12</xsl:if>
		<xsl:if test="$rela = 14">13</xsl:if>
		<xsl:if test="$rela = 15">14</xsl:if>
		<xsl:if test="$rela = 16">15</xsl:if>
		<xsl:if test="$rela = 17">16</xsl:if>
		<xsl:if test="$rela = 18">17</xsl:if>
		<xsl:if test="$rela = 19">18</xsl:if>
		<xsl:if test="$rela = 20">19</xsl:if>
		<xsl:if test="$rela = 21">20</xsl:if>
		<xsl:if test="$rela = 22">23</xsl:if>
		<xsl:if test="$rela = 23">21</xsl:if>
		<xsl:if test="$rela = 24">22</xsl:if>
		<xsl:if test="$rela = 25">24</xsl:if>
		<xsl:if test="$rela = 26">25</xsl:if>
		<xsl:if test="$rela = 27">26</xsl:if>
		<xsl:if test="$rela = 28">27</xsl:if>
		<xsl:if test="$rela = 29">28</xsl:if>
		<xsl:if test="$rela = 30">30</xsl:if>
	</xsl:template>

	<!-- 起领年期标志转换 -->
	<xsl:template name="tran_getyearflag">
		<xsl:param name="getyearflag" />
		<xsl:choose>
			<xsl:when test="$getyearflag='1'">A</xsl:when>
			<xsl:when test="$getyearflag='0'">Y</xsl:when>
		</xsl:choose>
	</xsl:template>

	<!-- liveaccflag,livegetmode,1累计生息,2现金领取 -->
	<xsl:template name="tran_livegetmode">
		<xsl:param name="livegetmode" />
		<xsl:choose>
			<xsl:when test="$livegetmode='1'">4</xsl:when>
			<xsl:when test="$livegetmode='2'">1</xsl:when>
			<xsl:when test="$livegetmode=''">4</xsl:when>
		</xsl:choose>
	</xsl:template>

	<!-- 居民居住类型转换 -->
	<xsl:template name="tran_appRgtType">
		<xsl:param name="appntRgtType" />
		<xsl:choose>
			<xsl:when test="$appntRgtType=0">1</xsl:when>
			<xsl:when test="$appntRgtType=1">2</xsl:when>
		</xsl:choose>
	</xsl:template>

	<!-- 职业类型转 -->
	<xsl:template name="tran_occupationType">
		<xsl:param name="occupationType" />
		<xsl:choose>
			<xsl:when test="$occupationType=01">2</xsl:when>
			<xsl:when test="$occupationType=02">1</xsl:when>
			<xsl:when test="$occupationType=03">2</xsl:when>
			<xsl:when test="$occupationType=04">2</xsl:when>
			<xsl:when test="$occupationType=05">4</xsl:when>
			<xsl:when test="$occupationType=06">3</xsl:when>
			<xsl:when test="$occupationType=07">5</xsl:when>
		</xsl:choose>
	</xsl:template>

	<!-- 职业代码转换 -->
	<xsl:template name="tran_occupationCode">
		<xsl:param name="occupationCode" />
		<xsl:choose>
			<xsl:when test="$occupationCode=01">0101002</xsl:when>
			<xsl:when test="$occupationCode=02">0001003</xsl:when>
			<xsl:when test="$occupationCode=03">0501002</xsl:when>
			<xsl:when test="$occupationCode=04">1402010</xsl:when>
			<xsl:when test="$occupationCode=05">0501010</xsl:when>
			<xsl:when test="$occupationCode=06">0501008</xsl:when>
			<xsl:when test="$occupationCode=07">0816018</xsl:when>
		</xsl:choose>
	</xsl:template>

	<!-- 红利领取方式 -->
	<xsl:template name="tran_getMode">
		<xsl:param name="bonusGetMode">0</xsl:param>
		<xsl:if test="$bonusGetMode = 0">2</xsl:if>
		<xsl:if test="$bonusGetMode = 1">3</xsl:if>
		<xsl:if test="$bonusGetMode = 2">1</xsl:if>
		<xsl:if test="$bonusGetMode = 3">4</xsl:if>
	</xsl:template>

	<xsl:template name="tran_riskCode">
		<xsl:param name="riskCode" />
		<xsl:choose>
			<xsl:when test="$riskCode='2024'">2036</xsl:when>
			<xsl:when test="$riskCode='1015'">1020</xsl:when>
			<xsl:when test="$riskCode='C026'">5010</xsl:when>
			<xsl:otherwise><xsl:value-of select="$riskCode" /></xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<!-- 满期领取金的领取方式 -->
	<xsl:template name="tran_getIntv">
		<xsl:param name="riskCode" />
		<xsl:param name="getIntv"/>
		<!-- 把对应生存金领取方式字段都改为统一的“一次性领”而不去取xxx发来的领取方式字段 -->
		<!-- <xsl:variable name="getIntv" select="'0'"></xsl:variable> -->
		<xsl:choose>
			<xsl:when test="$riskCode ='2020' and $getIntv='1'">
				12
			</xsl:when>
			<xsl:when test="$riskCode ='2020' and $getIntv='12'">
				1
			</xsl:when>
			<xsl:otherwise>
				<xsl:if test="$getIntv = '4'">12</xsl:if>
				<xsl:if test="$getIntv = '1'">1</xsl:if>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<!--证件有效期长期转换-->
	<xsl:template name="tran_idValidate">
		<xsl:param name="idValidate"/>
		<xsl:choose>
			<xsl:when test="$idValidate = 99991231">长期</xsl:when>
			<xsl:when test="$idValidate = 99999999">长期</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="$idValidate" />
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<!-- 保额转换 -->
	<xsl:template name="tran_amnt">
		<xsl:param name="riskCode" />
		<xsl:param name="amnt"></xsl:param>
		<xsl:choose>
			<xsl:when test="$riskCode=1022">0</xsl:when>
			<xsl:when test="$riskCode=2036">0</xsl:when>
			<xsl:when test="$riskCode=2044">0</xsl:when>
			<xsl:otherwise><xsl:value-of select="$amnt" /></xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<!-- 自动续保标记 -->
	<xsl:template name="tran_renew">
		<xsl:param name="flag"/>
		<xsl:if test="$flag = '1'">-1</xsl:if>
		<xsl:if test="$flag = '0'">0</xsl:if>
		<xsl:if test="$flag = ''">-9</xsl:if><!-- -9代表xxx没有录入自动续保标记 -->
	</xsl:template>
</xsl:stylesheet>

