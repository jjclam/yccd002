<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:template match="/">
        <ErrorMessages>
            <xsl:for-each select="RuleSet/Rule">
                <xsl:variable name="data" select="."/>
                <xsl:variable name="regex" select="@regex"/>
                <xsl:variable name="errMsg" select="@errMsg"/>
                <xsl:analyze-string select="$data" regex="{$regex}">
                    <xsl:non-matching-substring>
                        <ErrorMessage>
                            <xsl:value-of select="$errMsg"/>
                        </ErrorMessage>
                    </xsl:non-matching-substring>
                </xsl:analyze-string>
            </xsl:for-each>
        </ErrorMessages>
    </xsl:template>
</xsl:stylesheet>

