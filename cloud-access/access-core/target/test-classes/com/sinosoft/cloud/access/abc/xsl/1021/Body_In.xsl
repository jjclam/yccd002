<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:java="http://xml.apache.org/xslt/java" exclude-result-prefixes="java">
	<xsl:output method="xml" indent="yes"/>
	<!--保单详细信息查询-->
	<xsl:template match="ABCB2I">
		<TranData>
			<Body>
				<ContNo>
					<xsl:value-of select="App/Req/PolicyNo"/>
				</ContNo>
				<RiskCode>
					<xsl:value-of select="App/Req/RiskCode"/>
				</RiskCode>
				<Policypwd>
					<xsl:value-of select="App/Req/Policypwd"/>
				</Policypwd>
			</Body>
		</TranData>
	</xsl:template>
</xsl:stylesheet>
