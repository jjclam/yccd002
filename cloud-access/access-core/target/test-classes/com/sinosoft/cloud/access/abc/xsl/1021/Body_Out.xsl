<?xml version="1.0" encoding="GBK"?>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:java="http://xml.apache.org/xslt/java" exclude-result-prefixes="java">
<xsl:template match="/TranData">
<ABCB2I>
	<Header>
		<xsl:if test="Head/Flag=0">
			<RetCode>000000</RetCode>
			<RetMsg>交易成功</RetMsg>
		</xsl:if>
		<xsl:if test="Head/Flag!=0">
			<RetCode>009999</RetCode>
			<RetMsg><xsl:value-of select="Head/Desc"/></RetMsg>
		</xsl:if>
		<SerialNo></SerialNo>
		<InsuSerial></InsuSerial>
		<TransDate></TransDate>
		<BankCode>02</BankCode>
		<CorpNo></CorpNo>
		<TransCode></TransCode>
	</Header>
	<App>
		<Ret>
			<PolicyNo><xsl:value-of select="Body/ContnoList/ContnoDetail/ContNo"/></PolicyNo>
			<VchNo><xsl:value-of select="Body/ContnoList/ContnoDetail/ProposalPrtNo"/></VchNo><!-- 保单印刷号 -->
			<RiskCode><xsl:value-of select="Body/ContnoList/ContnoDetail/RiskList/RiskDetail/RiskCode"/></RiskCode>
			<RiskName><xsl:value-of select="Body/ContnoList/ContnoDetail/RiskList/RiskDetail/RiskName"/></RiskName>
			<!-- 保单状态  00有效 01退保 02当日撤单 03犹撤 04满期给付 05状态不明确 06部分赎回 07理赔终止 08处理中 -->
			<PolicyStatus><xsl:value-of select="Body/ContnoList/ContnoDetail/RiskList/RiskDetail/Appflag"/></PolicyStatus>
			<!-- 保单质押状态  0-未质押；1-质押中 -->
			<PolicyPledge><xsl:value-of select="Body/ContnoList/ContnoDetail/PolicyPledge"/></PolicyPledge>
			<AcceptDate><xsl:value-of select="Body/ContnoList/ContnoDetail/AcceptDate"/></AcceptDate>
			<PolicyBgnDate><xsl:value-of select="Body/ContnoList/ContnoDetail/PolicyBgnDate"/></PolicyBgnDate>
			<PolicyEndDate><xsl:value-of select="Body/ContnoList/ContnoDetail/PolicyEndDate"/></PolicyEndDate>
			<PolicyAmmount><xsl:value-of select="Body/ContnoList/ContnoDetail/RiskList/RiskDetail/Mult"/></PolicyAmmount><!-- 投保份数 -->
			 <Amt><xsl:value-of select="java:com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.fenToYuan(Body/ContnoList/ContnoDetail/RiskList/RiskDetail/Prem)"/></Amt>保费
	        <Beamt><xsl:value-of select="java:com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.fenToYuan(Body/ContnoList/ContnoDetail/RiskList/RiskDetail/Amnt)"/></Beamt>保额	            
			<PolicyValue><xsl:value-of select="Body/ContnoList/ContnoDetail/PolicyValue"/></PolicyValue><!-- 保单价值 -->
			<AccountValue><xsl:value-of select="Body/ContnoList/ContnoDetail/AccountValue"/></AccountValue><!-- 当前账户价值 -->
			<InsuDueDate><xsl:value-of select="Body/ContnoList/ContnoDetail/RiskList/RiskDetail/InsuYear"/>年</InsuDueDate><!-- 保险期间 -->
			<!-- 缴费方式 -->
			<PayType>
				<xsl:call-template name="tran_Contpayintv">
					<xsl:with-param name="payintv">
						<xsl:value-of select="Body/ContnoList/ContnoDetail/RiskList/RiskDetail/PayIntv" />
					</xsl:with-param>
				</xsl:call-template>
			</PayType>
			<!-- zhangjingsheng date20170707 bug:4695：xxx保单详情查询缴费年期与缴费期间返回有误-->
			<PayDue>
			<xsl:value-of select="Body/ContnoList/ContnoDetail/RiskList/RiskDetail/PayEndYear" />年
			</PayDue>		
			<!-- zhangjingsheng end-->	
			 <Prem><xsl:value-of select="java:com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.fenToYuan(Body/ContnoList/ContnoDetail/RiskList/RiskDetail/Prem)"/></Prem>缴费金额
			 <PayAccount><xsl:value-of select="Body/AppntBankAccNo"/></PayAccount><!-- 缴费账户 -->
			<PayProv/><!-- 缴费省市代码 -->
        	<PayBranch/><!-- 缴费网点号 -->
			<!-- 投保人部分 -->
			<Appl>
				<IDKind>
			    	<xsl:call-template name="tran_idtype">
						<xsl:with-param name="idtype">
							<xsl:value-of select="Body/AppntIDType" />
						</xsl:with-param>
					</xsl:call-template>
			    </IDKind>
				<IDCode><xsl:value-of select="Body/AppntIDNo"/></IDCode>
				<Name><xsl:value-of select="Body/AppntName"/></Name>
				<Sex><xsl:value-of select="Body/AppntSex"/></Sex>
				 <Birthday><xsl:value-of select="java:com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.date10to8(Body/AppntBirthday)"/></Birthday>				
				 <Address><xsl:value-of select="Body/AppntPostalAddress"/></Address>
				<ZipCode><xsl:value-of select="Body/AppntZipCode"/></ZipCode>
				<Email><xsl:value-of select="Body/AppntEmail"/></Email>
				<Phone><xsl:value-of select="Body/AppntPhone"/></Phone>
				<Mobile><xsl:value-of select="Body/AppntMobile"/></Mobile>
				<!-- 投保人收人 -->
				<AnnualIncome>
			    	<xsl:call-template name="tran_AnnualIncome">
						<xsl:with-param name="tAnnualIncome">
							<xsl:value-of select="Body/AppntMSalary" />
						</xsl:with-param>
					</xsl:call-template>
			    </AnnualIncome>
				<!--  投保人与被保人关系 -->
				<RelaToInsured>
			    	<xsl:call-template name="tran_relation">
						<xsl:with-param name="RelaToInsured">
							<xsl:value-of select="Body/ContnoList/ContnoDetail/RelationToAppnt" />
						</xsl:with-param>
					</xsl:call-template>
			    </RelaToInsured>
			</Appl>
			<!-- 被保人信息 -->
			<Insu>
				<Name><xsl:value-of select="Body/ContnoList/ContnoDetail/InsuredName"/></Name><!-- 被保人姓名 -->
				<Sex><xsl:value-of select="Body/ContnoList/ContnoDetail/InsuredSex"/></Sex><!-- 被保人性别 -->
				<!-- 被保人证件类型 -->
				<IDKind>
			    	<xsl:call-template name="tran_idtype">
						<xsl:with-param name="idtype">
							<xsl:value-of select="Body/ContnoList/ContnoDetail/InsuredIDType" />
						</xsl:with-param>
					</xsl:call-template>
			    </IDKind>
				<IDCode><xsl:value-of select="Body/ContnoList/ContnoDetail/InsuredIDNo"/></IDCode><!-- 被保人证件号码 -->
				 <Birthday><xsl:value-of select="java:com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.date10to8(Body/ContnoList/ContnoDetail/InsuredBirthday)"/></Birthday>被保人出生日期
			  </Insu>
			<!-- 受益人信息 -->
			<Bnfs>
				<Count><xsl:value-of select="Body/ContnoList/ContnoDetail/BnfNum"/></Count><!-- 受益人个数 --> 
				<xsl:for-each select="Body/ContnoList/ContnoDetail/BnfList/BnfDetail">
					<Type1><xsl:value-of select="Type1"/></Type1><!-- 受益人类型 -->
					<Name1><xsl:value-of select="BnfName"/></Name1><!-- 受益人姓名 -->
					<Sex1><xsl:value-of select="BnfSex"/></Sex1><!-- 性别 -->
					<Birthday1><xsl:value-of select="BnfBirthday"/></Birthday1><!-- 出生日期 -->
					<!-- 证件类型 -->
					<IDKind1>
				    	<xsl:call-template name="tran_idtype">
							<xsl:with-param name="idtype">
								<xsl:value-of select="BnfIDType" />
							</xsl:with-param>
						</xsl:call-template>
				    </IDKind1>
					<IDCode1><xsl:value-of select="BnfIDNo"/></IDCode1><!-- 证件号码 -->
					<RelationToInsured1><xsl:value-of select="BnfRelationToInsured"/></RelationToInsured1><!-- 与被保人关系 -->
					<Sequence1><xsl:value-of select="Sequence1"/></Sequence1><!-- 受益人受益顺序 -->
					<Prop1><xsl:value-of select="BnfLot"/></Prop1><!-- 受益人受益比例 -->
				</xsl:for-each>
			</Bnfs>
			<PrntCount>0</PrntCount>
			<prt1></prt1>
			<prt2></prt2>
			<prt3></prt3>
			<prt4></prt4>
			<prt5></prt5>
			<prt6></prt6>
		</Ret>
	</App>
</ABCB2I>
</xsl:template>
	<!-- 关系 -->
	<xsl:template name="tran_relation">
		<xsl:param name="RelaToInsured">00</xsl:param>
		<xsl:if test="$RelaToInsured = 00">01</xsl:if>
		<xsl:if test="$RelaToInsured = 01">30</xsl:if>
		<xsl:if test="$RelaToInsured = 04">30</xsl:if>
		<xsl:if test="$RelaToInsured = 03">30</xsl:if>
		<xsl:if test="$RelaToInsured = 31">30</xsl:if>
		<xsl:if test="$RelaToInsured = 21">30</xsl:if>
		<xsl:if test="$RelaToInsured = 25">30</xsl:if>
		<xsl:if test="$RelaToInsured = 12">30</xsl:if>
		<xsl:if test="$RelaToInsured = 25">30</xsl:if>
	</xsl:template>

	<!-- 证件类型 -->
	  <xsl:template name="tran_idtype">
		<xsl:param name="idtype">0</xsl:param>
		<xsl:if test="$idtype = 0">110001</xsl:if><!-- 身份证 -->
		<xsl:if test="$idtype = 5">110003</xsl:if><!-- 临时身份证 -->
		<xsl:if test="$idtype = 6">110005</xsl:if><!-- 户口薄 -->
		<xsl:if test="$idtype = 8">119999</xsl:if>
		<xsl:if test="$idtype = 1">119999</xsl:if>
		<xsl:if test="$idtype = 2">119999</xsl:if>
		<xsl:if test="$idtype = 7">119999</xsl:if>
		<xsl:if test="$idtype = 3">119999</xsl:if>
	</xsl:template>
	
	<!-- 保单的缴费方式 -->
	<xsl:template name="tran_Contpayintv">
		<xsl:param name="payintv">0</xsl:param>
		<xsl:choose>
			<xsl:when test="$payintv = 0">1</xsl:when><!-- 趸交 -->
			<xsl:when test="$payintv = 1">0</xsl:when><!-- 月缴 -->
			<xsl:when test="$payintv = 3">0</xsl:when><!-- 季缴 -->
			<xsl:when test="$payintv = 6">0</xsl:when><!-- 半年交 -->
			<!-- zhangjingsheng date20170707 bug:4695：xxx保单详情查询缴费年期与缴费期间返回有误-->
			<xsl:when test="$payintv = 12">5</xsl:when><!-- 年缴 -->
			<!-- zhangjingsheng end-->
			<xsl:when test="$payintv = -1">0</xsl:when><!-- 不定期 -->
			<xsl:otherwise>
				<xsl:value-of select="0" />
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	
	<!-- 投保人年收入 -->
	<xsl:template name="tran_AnnualIncome">
		<xsl:param name="tAnnualIncome">0</xsl:param>
		<xsl:choose>
			<xsl:when test="$tAnnualIncome = 49000">5万以下</xsl:when>
			<xsl:when test="$tAnnualIncome = 99000">5-10万</xsl:when>
			<xsl:when test="$tAnnualIncome = 299000">10-30万</xsl:when>
			<xsl:when test="$tAnnualIncome = 499000">30-50万</xsl:when>
			<xsl:when test="$tAnnualIncome = 5000000">50万以上</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="0" />
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	
</xsl:stylesheet>