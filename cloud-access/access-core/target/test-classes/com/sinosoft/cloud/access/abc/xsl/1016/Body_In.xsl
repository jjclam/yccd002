<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:java="http://xml.apache.org/xslt/java" exclude-result-prefixes="java">
    <xsl:output method="xml" indent="yes"/>
    <!--自动冲正交易-->
    <xsl:template match="ABCB2I">
        <TranData>
            <Head>
                <TranDate>
                    <xsl:value-of select="Header/TransDate" />
                </TranDate>
                <TranTime>
                    <xsl:value-of select="Header/TransTime" />
                </TranTime>
                <TellerNo>nonghang</TellerNo>
                <TranNo>
                    <xsl:value-of select="Header/SerialNo" />
                </TranNo>
                <NodeNo>
                    <xsl:value-of select="Header/ProvCode" />
                    <xsl:value-of select="Header/BranchNo" />
                </NodeNo>
                <TranCom>2</TranCom>
                <BankCode>02</BankCode>
                <FuncFlag>103</FuncFlag>
                <AgentCom></AgentCom>
                <AgentCode>-</AgentCode>
            </Head>
            <Body>
                <ContNo>
                    <xsl:value-of select="App/Req/PolicyNo"/>
                </ContNo>
                <RiskCode>
                    <xsl:value-of select="App/Req/RiskCode"/>
                </RiskCode>
            </Body>
        </TranData>
    </xsl:template>
</xsl:stylesheet>
