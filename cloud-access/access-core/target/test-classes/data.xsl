<?xml version="1.0" encoding="GBK"?>

<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:java="http://xml.apache.org/xslt/java"
                exclude-result-prefixes="java">

    <xsl:template match="ABCB2I">
        <xsl:value-of select="java:com.sinosoft.midplat.common.NumberUtil.yuanToFen('1')"/>

        <TranData>
            <Head>
                <TranDate><!-- 交易日期 -->
                    <xsl:value-of select="Header/TransDate"/>
                </TranDate>
                <TransCode>
                    <xsl:value-of select="Header/TransCode"/>
                </TransCode>
                <TranTime><!-- 交易时间 -->
                    <xsl:value-of select="Header/TransTime"/>
                </TranTime>
                <TellerNo><!-- 柜员代码 -->
                    <xsl:value-of select="Header/Tlid"/>
                </TellerNo>
                <TranNo><!-- 交易流水号 -->
                    <xsl:value-of select="Header/SerialNo"/>
                </TranNo>
                <NodeNo><!-- 网点代码 -->
                    <xsl:value-of select="Header/ProvCode"/>
                    <xsl:value-of select="Header/BranchNo"/>
                </NodeNo>
                <TranCom>2</TranCom>
                <BankCode>02</BankCode>
                <FuncFlag>201</FuncFlag>
                <AgentCom></AgentCom>
                <AgentCode>-</AgentCode>
                <BankClerk>
                    <xsl:value-of select="App/Req/Base/Saler"/>
                </BankClerk><!-- xxx柜员姓名 -->
                <SourceType>
                    <xsl:call-template name="tran_Type">
                        <xsl:with-param name="Type">
                            <xsl:value-of select="Header/EntrustWay"/>
                        </xsl:with-param>
                    </xsl:call-template>
                </SourceType>
            </Head>


            <!-- 报文体 -->
            <Body>
                <AgentComName>
                    <xsl:value-of select="App/Req/Base/BranchName"/>
                </AgentComName>
                <AgentComSellNo>
                    <xsl:value-of select="App/Req/Base/BranchCertNo"/>
                </AgentComSellNo>
                <AgentPersonSellNo>
                    <xsl:value-of select="App/Req/Base/Saler"/>
                </AgentPersonSellNo>
                <AgentPersonCode>
                    <xsl:value-of select="App/Req/Base/SalerCertNo"/>
                </AgentPersonCode>
                <AppNo>
                    <xsl:value-of select="App/Req/AppNo"/>
                </AppNo>
                <ProposalPrtNo><!-- 投保单号 -->
                    <xsl:value-of select="App/Req/Base/PolicyApplySerial"/>
                </ProposalPrtNo>
                <PolApplyDate><!-- 保单投保日期 -->
                    <xsl:value-of select="App/Req/Base/ApplyDate"/>
                </PolApplyDate>
                <ContPrtNo>
                    <xsl:value-of select="App/Req/Base/VchNo"/>
                </ContPrtNo><!-- 单证号码 -->
                <HealthNotice>
                    <xsl:call-template name="tran_Health">
                        <xsl:with-param name="Health">
                            <xsl:value-of select="App/Req/Insu/HealthNotice"/>
                        </xsl:with-param>
                    </xsl:call-template>
                </HealthNotice>
                <JobNotice></JobNotice><!-- 职业告知 xxx不传 -->
                <AccName>
                    <xsl:value-of select="App/Req/Appl/Name"/>
                </AccName><!-- 账户姓名 -->
                <!-- 未找到匹配 -->
                <AccNo>
                    <xsl:value-of select="App/Req/Base/ConAccNo"/>
                </AccNo><!-- xxx账号  与核心曹增，张建东商量用续期缴费账号覆盖首期账号 -->
                <PayMode></PayMode>
                <XBankCode>02</XBankCode>
                <XAccName>
                    <xsl:value-of select="App/Req/Base/ConAccName"/>
                </XAccName>
                <!-- 续期缴费账号 -->
                <XBankAccNo>
                    <xsl:value-of select="App/Req/Base/ConAccNo"/>
                </XBankAccNo>
                <!-- 投保人 -->
                <Appnt>
                    <Name>
                        <xsl:value-of select="App/Req/Appl/Name"/>
                    </Name>
                    <Sex>
                        <xsl:call-template name="tran_sex">
                            <xsl:with-param name="sex">
                                <xsl:value-of select="App/Req/Appl/Sex"/>
                            </xsl:with-param>
                        </xsl:call-template>
                    </Sex>
                    <Birthday>
                        <xsl:value-of select="App/Req/Appl/Birthday"/>
                    </Birthday>
                    <IDType><!-- 投保人证件类型 -->
                        <xsl:call-template name="tran_idtype">
                            <xsl:with-param name="idtype">
                                <xsl:value-of select="App/Req/Appl/IDKind"/>
                            </xsl:with-param>
                        </xsl:call-template>
                    </IDType>
                    <IDNo><!-- 投保人证件号 -->
                        <xsl:value-of select="App/Req/Appl/IDCode"/>
                    </IDNo>
                    <AppntValidityday>
                        <xsl:value-of select="App/Req/Appl/InvalidDate"/>
                    </AppntValidityday><!-- 投保人证件有效期 -->
                    <JobCode>
                        <xsl:value-of select="App/Req/Appl/JobCode"/>
                    </JobCode>
                    <Nationality><!-- 投保人国籍 -->
                        <xsl:call-template name="tran_idnationality">
                            <xsl:with-param name="idnationality">
                                <xsl:value-of select="App/Req/Appl/Country"/>
                            </xsl:with-param>
                        </xsl:call-template>
                    </Nationality>
                    <Address>
                        <xsl:value-of select="App/Req/Appl/Prov"/><xsl:value-of select="App/Req/Appl/City"/><xsl:value-of
                            select="App/Req/Appl/Zone"/><xsl:value-of select="App/Req/Appl/Address"/>
                    </Address>
                    <ZipCode><!-- 邮编 -->
                        <xsl:value-of select="App/Req/Appl/ZipCode"/>
                    </ZipCode>
                    <Mobile>
                        <xsl:value-of select="App/Req/Appl/Mobile"/>
                    </Mobile>
                    <Phone>
                        <xsl:value-of select="App/Req/Appl/Phone"/>
                    </Phone>
                    <Email>
                        <xsl:value-of select="App/Req/Appl/Email"/>
                    </Email>
                    <!-- YBT投保人年收入 -->
                    <xsl:variable name="tType" select="Header/EntrustWay"/>
                    <xsl:if test="$tType = '11' or $tType='04'">
                        <xsl:variable name="tIncome" select="App/Req/Appl/AnnualIncome"/>
                        <xsl:if test="$tIncome = ''">
                            <Salary></Salary>
                        </xsl:if>
                        <xsl:if test="$tIncome != ''">
                            <Salary>
                                <xsl:value-of select="$tIncome div 10000"/>
                            </Salary>
                        </xsl:if>
                        <xsl:variable name="tFamilyIncome" select="App/Req/Appl/AnnualIncome"/>
                        <xsl:if test="$tFamilyIncome != ''">
                            <FamilySalary>
                                <xsl:value-of select="$tFamilyIncome  div 10000"/>
                            </FamilySalary>
                        </xsl:if>
                        <xsl:if test="$tFamilyIncome = ''">
                            <FamilySalary></FamilySalary>
                        </xsl:if>
                    </xsl:if>
                    <!-- WY投保人年收入 -->
                    <xsl:if test="$tType != '11' and $tType != '04'">
                        <xsl:variable name="tIncome" select="App/Req/Appl/AnnualIncome"/>
                        <xsl:if test="$tIncome = ''">
                            <Salary></Salary>
                        </xsl:if>
                        <xsl:if test="$tIncome = '5万以下'">
                            <Salary>49000</Salary>
                        </xsl:if>
                        <xsl:if test="$tIncome = '5-10万'">
                            <Salary>99000</Salary>
                        </xsl:if>
                        <xsl:if test="$tIncome = '10-30万'">
                            <Salary>299000</Salary>
                        </xsl:if>
                        <xsl:if test="$tIncome = '30-50万'">
                            <Salary>499000</Salary>
                        </xsl:if>
                        <xsl:if test="$tIncome = '50万以上'">
                            <Salary>5000000</Salary>
                        </xsl:if>
                        <xsl:variable name="tFamilyIncome" select="App/Req/Appl/AnnualIncome"/>
                        <xsl:if test="$tFamilyIncome = ''">
                            <FamilySalary></FamilySalary>
                        </xsl:if>
                        <xsl:if test="$tFamilyIncome = '5万以下'">
                            <FamilySalary>49000</FamilySalary>
                        </xsl:if>
                        <xsl:if test="$tFamilyIncome = '5-10万'">
                            <FamilySalary>99000</FamilySalary>
                        </xsl:if>
                        <xsl:if test="$tFamilyIncome = '10-30万'">
                            <FamilySalary>299000</FamilySalary>
                        </xsl:if>
                        <xsl:if test="$tFamilyIncome = '30-50万'">
                            <FamilySalary>499000</FamilySalary>
                        </xsl:if>
                        <xsl:if test="$tFamilyIncome = '50万以上'">
                            <FamilySalary>5000000</FamilySalary>
                        </xsl:if>
                    </xsl:if>
                    <LiveZone>
                        <xsl:value-of select="App/Req/Appl/CustSource"/>
                    </LiveZone>
                    <RelaToInsured><!-- 投保人与被保人关系 -->
                        <xsl:call-template name="tran_relation">
                            <xsl:with-param name="RelaToInsured">
                                <xsl:value-of select="App/Req/Appl/RelaToInsured"/>
                            </xsl:with-param>
                        </xsl:call-template>
                    </RelaToInsured>
                    <JobCode>
                        <xsl:call-template name="tran_idjobCode">
                            <xsl:with-param name="idjobCode">
                                <xsl:value-of select="App/Req/Appl/JobType"/>
                            </xsl:with-param>
                        </xsl:call-template>
                    </JobCode>
                </Appnt>
                <!-- 被保人 -->
                <Insured>
                    <Name>
                        <xsl:value-of select="App/Req/Insu/Name"/>
                    </Name>
                    <Sex>
                        <xsl:call-template name="tran_sex">
                            <xsl:with-param name="sex">
                                <xsl:value-of select="App/Req/Insu/Sex"/>
                            </xsl:with-param>
                        </xsl:call-template>
                    </Sex>
                    <Birthday>
                        <xsl:value-of select="App/Req/Insu/Birthday"/>
                    </Birthday>
                    <IDType>
                        <xsl:call-template name="tran_idtype">
                            <xsl:with-param name="idtype">
                                <xsl:value-of select="App/Req/Insu/IDKind"/>
                            </xsl:with-param>
                        </xsl:call-template>
                    </IDType>
                    <IDNo>
                        <xsl:value-of select="App/Req/Insu/IDCode"/>
                    </IDNo>
                    <InsValidityday>
                        <xsl:value-of select="App/Req/Insu/ValidDate"/>
                    </InsValidityday><!-- 被保人证件有效期 -->
                    <JobCode>
                        <xsl:value-of select="App/Req/Insu/JobCode"/>
                    </JobCode>
                    <Nationality>
                        <xsl:value-of select="App/Req/Insu/County"/>
                    </Nationality>
                    <Address>
                        <xsl:value-of select="App/Req/Insu/Prov"/><xsl:value-of select="App/Req/Insu/City"/><xsl:value-of
                            select="App/Req/Insu/Zone"/><xsl:value-of select="App/Req/Insu/Address"/>
                    </Address>
                    <ZipCode>
                        <xsl:value-of select="App/Req/Insu/ZipCode"/>
                    </ZipCode>
                    <Mobile>
                        <xsl:value-of select="App/Req/Insu/Mobile"/>
                    </Mobile>
                    <Phone>
                        <xsl:value-of select="App/Req/Insu/Phone"/>
                    </Phone>
                    <Email>
                        <xsl:value-of select="App/Req/Insu/Email"/>
                    </Email>
                    <Nationality>
                        <xsl:call-template name="tran_idnationality">
                            <xsl:with-param name="idnationality">
                                <xsl:value-of select="App/Req/Insu/Country"/>
                            </xsl:with-param>
                        </xsl:call-template>
                    </Nationality>
                    <JobCode>
                        <xsl:call-template name="tran_idjobCode">
                            <xsl:with-param name="idjobCode">
                                <xsl:value-of select="App/Req/Insu/JobType"/>
                            </xsl:with-param>
                        </xsl:call-template>
                    </JobCode>
                </Insured>
                <!-- 受益人 -->
                <xsl:choose>
                    <xsl:when test="App/Req/Bnfs/Count = '0'">
                        <Bnf>
                            <Type>1</Type>
                            <!-- 默认为“1-死亡受益人” -->
                            <Grade>1</Grade>
                            <Name>未指定</Name>
                            <Sex></Sex>
                            <Birthday></Birthday>
                            <IDType></IDType>
                            <IDNo></IDNo>
                            <BnfValidityday></BnfValidityday><!-- 受益人证件有效期 -->
                            <Lot>100</Lot>
                            <RelaToInsured></RelaToInsured>
                            <BeneficType>Y</BeneficType>
                            <Nationality>
                                <xsl:call-template name="tran_idnationality">
                                    <xsl:with-param name="idnationality">
                                        <xsl:value-of select="App/Req/Bnfs/Country1"/>
                                    </xsl:with-param>
                                </xsl:call-template>
                            </Nationality>
                        </Bnf>
                    </xsl:when>
                    <!-- 由于xxx受益人节点采用循环组包，依靠节点名称后面添加数字来区分，暂时先支持6个受益人 -->
                    <xsl:when test="App/Req/Bnfs/Count != '0'">

                        <xsl:if test="App/Req/Bnfs/Count &gt;= 1">
                            <Bnf>
                                <Type>
                                    <xsl:value-of select="App/Req/Bnfs/Type1"/>
                                </Type>
                                <Grade>
                                    <xsl:value-of select="App/Req/Bnfs/Sequence1"/>
                                </Grade>
                                <Name>
                                    <xsl:value-of select="App/Req/Bnfs/Name1"/>
                                </Name>
                                <Sex>
                                    <xsl:call-template name="tran_sex">
                                        <xsl:with-param name="sex">
                                            <xsl:value-of select="App/Req/Bnfs/Sex1"/>
                                        </xsl:with-param>
                                    </xsl:call-template>
                                </Sex>
                                <Birthday>
                                    <xsl:value-of select="App/Req/Bnfs/Birthday1"/>
                                </Birthday>
                                <IDType>
                                    <xsl:call-template name="tran_idtype">
                                        <xsl:with-param name="idtype">
                                            <xsl:value-of select="App/Req/Bnfs/IDKind1"/>
                                        </xsl:with-param>
                                    </xsl:call-template>
                                </IDType>
                                <IDNo>
                                    <xsl:value-of select="App/Req/Bnfs/IDCode1"/>
                                </IDNo>
                                <BnfValidityday>
                                    <xsl:value-of select="App/Req/Bnfs/InvalidDate1"/>
                                </BnfValidityday><!-- 受益人证件有效期 -->
                                <Lot>
                                    <xsl:value-of select="App/Req/Bnfs/Prop1"/>
                                </Lot>
                                <RelaToInsured>
                                    <xsl:call-template name="tran_relation">
                                        <xsl:with-param name="RelaToInsured">
                                            <xsl:value-of select="App/Req/Bnfs/RelationToInsured1"/>
                                        </xsl:with-param>
                                    </xsl:call-template>
                                </RelaToInsured>
                                <BeneficType>N</BeneficType>
                                <Nationality>
                                    <xsl:call-template name="tran_idnationality">
                                        <xsl:with-param name="idnationality">
                                            <xsl:value-of select="App/Req/Bnfs/Country1"/>
                                        </xsl:with-param>
                                    </xsl:call-template>
                                </Nationality>
                            </Bnf>
                        </xsl:if>
                        <xsl:if test="App/Req/Bnfs/Count &gt;= 2">
                            <Bnf>
                                <Type>
                                    <xsl:value-of select="App/Req/Bnfs/Type2"/>
                                </Type>
                                <Grade>
                                    <xsl:value-of select="App/Req/Bnfs/Sequence2"/>
                                </Grade>
                                <Name>
                                    <xsl:value-of select="App/Req/Bnfs/Name2"/>
                                </Name>
                                <Sex>
                                    <xsl:call-template name="tran_sex">
                                        <xsl:with-param name="sex">
                                            <xsl:value-of select="App/Req/Bnfs/Sex2"/>
                                        </xsl:with-param>
                                    </xsl:call-template>
                                </Sex>
                                <Birthday>
                                    <xsl:value-of select="App/Req/Bnfs/Birthday2"/>
                                </Birthday>
                                <IDType>
                                    <xsl:call-template name="tran_idtype">
                                        <xsl:with-param name="idtype">
                                            <xsl:value-of select="App/Req/Bnfs/IDKind2"/>
                                        </xsl:with-param>
                                    </xsl:call-template>
                                </IDType>
                                <IDNo>
                                    <xsl:value-of select="App/Req/Bnfs/IDCode2"/>
                                </IDNo>
                                <BnfValidityday>
                                    <xsl:value-of select="App/Req/Bnfs/InvalidDate2"/>
                                </BnfValidityday><!-- 受益人证件有效期 -->
                                <Lot>
                                    <xsl:value-of select="App/Req/Bnfs/Prop2"/>
                                </Lot>
                                <RelaToInsured>
                                    <xsl:call-template name="tran_relation">
                                        <xsl:with-param name="RelaToInsured">
                                            <xsl:value-of select="App/Req/Bnfs/RelationToInsured2"/>
                                        </xsl:with-param>
                                    </xsl:call-template>
                                </RelaToInsured>
                                <BeneficType>N</BeneficType>
                                <Nationality>
                                    <xsl:call-template name="tran_idnationality">
                                        <xsl:with-param name="idnationality">
                                            <xsl:value-of select="App/Req/Bnfs/Country1"/>
                                        </xsl:with-param>
                                    </xsl:call-template>
                                </Nationality>
                            </Bnf>
                        </xsl:if>
                        <xsl:if test="App/Req/Bnfs/Count &gt;= 3">
                            <Bnf>
                                <Type>
                                    <xsl:value-of select="App/Req/Bnfs/Type3"/>
                                </Type>
                                <Grade>
                                    <xsl:value-of select="App/Req/Bnfs/Sequence3"/>
                                </Grade>
                                <Name>
                                    <xsl:value-of select="App/Req/Bnfs/Name3"/>
                                </Name>
                                <Sex>
                                    <xsl:call-template name="tran_sex">
                                        <xsl:with-param name="sex">
                                            <xsl:value-of select="App/Req/Bnfs/Sex3"/>
                                        </xsl:with-param>
                                    </xsl:call-template>
                                </Sex>
                                <Birthday>
                                    <xsl:value-of select="App/Req/Bnfs/Birthday3"/>
                                </Birthday>
                                <IDType>
                                    <xsl:call-template name="tran_idtype">
                                        <xsl:with-param name="idtype">
                                            <xsl:value-of select="App/Req/Bnfs/IDKind3"/>
                                        </xsl:with-param>
                                    </xsl:call-template>
                                </IDType>
                                <IDNo>
                                    <xsl:value-of select="App/Req/Bnfs/IDCode3"/>
                                </IDNo>
                                <BnfValidityday>
                                    <xsl:value-of select="App/Req/Bnfs/InvalidDate3"/>
                                </BnfValidityday><!-- 受益人证件有效期 -->
                                <Lot>
                                    <xsl:value-of select="App/Req/Bnfs/Prop3"/>
                                </Lot>
                                <RelaToInsured>
                                    <xsl:call-template name="tran_relation">
                                        <xsl:with-param name="RelaToInsured">
                                            <xsl:value-of select="App/Req/Bnfs/RelationToInsured3"/>
                                        </xsl:with-param>
                                    </xsl:call-template>
                                </RelaToInsured>
                                <BeneficType>N</BeneficType>
                                <Nationality>
                                    <xsl:call-template name="tran_idnationality">
                                        <xsl:with-param name="idnationality">
                                            <xsl:value-of select="App/Req/Bnfs/Country1"/>
                                        </xsl:with-param>
                                    </xsl:call-template>
                                </Nationality>
                            </Bnf>
                        </xsl:if>
                        <xsl:if test="App/Req/Bnfs/Count &gt;= 4">
                            <Bnf>
                                <Type>
                                    <xsl:value-of select="App/Req/Bnfs/Type4"/>
                                </Type>
                                <Grade>
                                    <xsl:value-of select="App/Req/Bnfs/Sequence4"/>
                                </Grade>
                                <Name>
                                    <xsl:value-of select="App/Req/Bnfs/Name4"/>
                                </Name>
                                <Sex>
                                    <xsl:call-template name="tran_sex">
                                        <xsl:with-param name="sex">
                                            <xsl:value-of select="App/Req/Bnfs/Sex4"/>
                                        </xsl:with-param>
                                    </xsl:call-template>
                                </Sex>
                                <Birthday>
                                    <xsl:value-of select="App/Req/Bnfs/Birthday4"/>
                                </Birthday>
                                <IDType>
                                    <xsl:call-template name="tran_idtype">
                                        <xsl:with-param name="idtype">
                                            <xsl:value-of select="App/Req/Bnfs/IDKind4"/>
                                        </xsl:with-param>
                                    </xsl:call-template>
                                </IDType>
                                <IDNo>
                                    <xsl:value-of select="App/Req/Bnfs/IDCode4"/>
                                </IDNo>
                                <BnfValidityday>
                                    <xsl:value-of select="App/Req/Bnfs/InvalidDate4"/>
                                </BnfValidityday><!-- 受益人证件有效期 -->
                                <Lot>
                                    <xsl:value-of select="App/Req/Bnfs/Prop4"/>
                                </Lot>
                                <RelaToInsured>
                                    <xsl:call-template name="tran_relation">
                                        <xsl:with-param name="RelaToInsured">
                                            <xsl:value-of select="App/Req/Bnfs/RelationToInsured4"/>
                                        </xsl:with-param>
                                    </xsl:call-template>
                                </RelaToInsured>
                                <BeneficType>N</BeneficType>
                                <Nationality>
                                    <xsl:call-template name="tran_idnationality">
                                        <xsl:with-param name="idnationality">
                                            <xsl:value-of select="App/Req/Bnfs/Country1"/>
                                        </xsl:with-param>
                                    </xsl:call-template>
                                </Nationality>
                            </Bnf>
                        </xsl:if>
                        <xsl:if test="App/Req/Bnfs/Count &gt;= 5">
                            <Bnf>
                                <Type>
                                    <xsl:value-of select="App/Req/Bnfs/Type5"/>
                                </Type>
                                <Grade>
                                    <xsl:value-of select="App/Req/Bnfs/Sequence5"/>
                                </Grade>
                                <Name>
                                    <xsl:value-of select="App/Req/Bnfs/Name5"/>
                                </Name>
                                <Sex>
                                    <xsl:call-template name="tran_sex">
                                        <xsl:with-param name="sex">
                                            <xsl:value-of select="App/Req/Bnfs/Sex5"/>
                                        </xsl:with-param>
                                    </xsl:call-template>
                                </Sex>
                                <Birthday>
                                    <xsl:value-of select="App/Req/Bnfs/Birthday5"/>
                                </Birthday>
                                <IDType>
                                    <xsl:call-template name="tran_idtype">
                                        <xsl:with-param name="idtype">
                                            <xsl:value-of select="App/Req/Bnfs/IDKind5"/>
                                        </xsl:with-param>
                                    </xsl:call-template>
                                </IDType>
                                <IDNo>
                                    <xsl:value-of select="App/Req/Bnfs/IDCode5"/>
                                </IDNo>
                                <BnfValidityday>
                                    <xsl:value-of select="App/Req/Bnfs/InvalidDate5"/>
                                </BnfValidityday><!-- 受益人证件有效期 -->
                                <Lot>
                                    <xsl:value-of select="App/Req/Bnfs/Prop5"/>
                                </Lot>
                                <RelaToInsured>
                                    <xsl:call-template name="tran_relation">
                                        <xsl:with-param name="RelaToInsured">
                                            <xsl:value-of select="App/Req/Bnfs/RelationToInsured5"/>
                                        </xsl:with-param>
                                    </xsl:call-template>
                                </RelaToInsured>
                                <BeneficType>N</BeneficType>
                                <Nationality>
                                    <xsl:call-template name="tran_idnationality">
                                        <xsl:with-param name="idnationality">
                                            <xsl:value-of select="App/Req/Bnfs/Country1"/>
                                        </xsl:with-param>
                                    </xsl:call-template>
                                </Nationality>
                            </Bnf>
                        </xsl:if>
                        <xsl:if test="App/Req/Bnfs/Count &gt;= 6">
                            <Bnf>
                                <Type>
                                    <xsl:value-of select="App/Req/Bnfs/Type6"/>
                                </Type>
                                <Grade>
                                    <xsl:value-of select="App/Req/Bnfs/Sequence6"/>
                                </Grade>
                                <Name>
                                    <xsl:value-of select="App/Req/Bnfs/Name6"/>
                                </Name>
                                <Sex>
                                    <xsl:call-template name="tran_sex">
                                        <xsl:with-param name="sex">
                                            <xsl:value-of select="App/Req/Bnfs/Sex6"/>
                                        </xsl:with-param>
                                    </xsl:call-template>
                                </Sex>
                                <Birthday>
                                    <xsl:value-of select="App/Req/Bnfs/Birthday6"/>
                                </Birthday>
                                <IDType>
                                    <xsl:call-template name="tran_idtype">
                                        <xsl:with-param name="idtype">
                                            <xsl:value-of select="App/Req/Bnfs/IDKind6"/>
                                        </xsl:with-param>
                                    </xsl:call-template>
                                </IDType>
                                <IDNo>
                                    <xsl:value-of select="App/Req/Bnfs/IDCode6"/>
                                </IDNo>
                                <BnfValidityday>
                                    <xsl:value-of select="App/Req/Bnfs/InvalidDate6"/>
                                </BnfValidityday><!-- 受益人证件有效期 -->
                                <Lot>
                                    <xsl:value-of select="App/Req/Bnfs/Prop6"/>
                                </Lot>
                                <RelaToInsured>
                                    <xsl:call-template name="tran_relation">
                                        <xsl:with-param name="RelaToInsured">
                                            <xsl:value-of select="App/Req/Bnfs/RelationToInsured6"/>
                                        </xsl:with-param>
                                    </xsl:call-template>
                                </RelaToInsured>
                                <BeneficType>N</BeneficType>
                            </Bnf>
                        </xsl:if>
                    </xsl:when>
                </xsl:choose>

                <!-- 险种 -->
                <Risk>
                    <RiskCode><!-- 险种代码 -->
                        <xsl:value-of select="App/Req/Risks/RiskCode"/>
                    </RiskCode>
                    <MainRiskCode><!-- 主险代码 -->
                        <xsl:value-of select="App/Req/Risks/RiskCode"/>
                    </MainRiskCode>
                    <Amnt><!-- 保额 -->
                        <!--<xsl:value-of-->
                                <!--select="java:com.sinosoft.midplat.common.NumberUtil.yuanToFen(App/Req/Risks/Amnt)"/>-->
                    </Amnt>
                    <Prem><!-- 保费 -->
                        <!--<xsl:value-of-->
                                <!--select="java:com.sinosoft.midplat.common.NumberUtil.yuanToFen(App/Req/Risks/Prem)"/>-->
                    </Prem>
                    <Mult><!-- 份数 -->
                        <xsl:value-of select="App/Req/Risks/Share"/>
                    </Mult>
                    <PayIntv><!-- 缴费间隔 -->
                        <xsl:call-template name="tran_Contpayintv">
                            <xsl:with-param name="payintv">
                                <xsl:value-of select="App/Req/Risks/PayType"/>
                            </xsl:with-param>
                        </xsl:call-template>
                    </PayIntv>
                    <PayMode>4</PayMode>
                    <ExPayMode>4</ExPayMode>
                    <InsuYearFlag><!-- 保险年期类型 -->
                        <xsl:call-template name="tran_PbIYF">
                            <xsl:with-param name="PbInsuYearFlag">
                                <xsl:value-of select="App/Req/Risks/InsuDueType"/>
                            </xsl:with-param>
                        </xsl:call-template>
                    </InsuYearFlag>
                    <xsl:if test="App/Req/Risks/InsuDueType = 6">    <!-- 保终身 -->
                        <InsuYear>105</InsuYear>
                        <PayEndYear>105</PayEndYear>
                        <PayEndYearFlag>A</PayEndYearFlag>
                    </xsl:if>
                    <xsl:if test="App/Req/Risks/InsuDueType != 6">
                        <PayEndYearFlag>Y</PayEndYearFlag>
                        <InsuYear>
                            <xsl:value-of select="App/Req/Risks/InsuDueDate"/>
                        </InsuYear>
                        <xsl:if test="App/Req/Risks/PayDueDate != ''">
                            <PayEndYear>
                                <xsl:value-of select="App/Req/Risks/PayDueDate"/>
                            </PayEndYear>
                        </xsl:if>
                        <xsl:if test="App/Req/Risks/PayDueDate = ''">
                            <PayEndYear>
                                <xsl:value-of select="App/Req/Risks/InsuDueDate"/>
                            </PayEndYear>
                        </xsl:if>
                    </xsl:if>
                    <BonusGetMode><!-- 红利领取方式 -->
                        <xsl:call-template name="tran_BonusGetMode">
                            <xsl:with-param name="BonusGetMode">
                                <xsl:value-of select="App/Req/Risks/BonusGetMode"/>
                            </xsl:with-param>
                        </xsl:call-template>
                    </BonusGetMode>
                    <GetIntv><!-- 年金领取方式 /保险金领取方式-->
                        <xsl:call-template name="tran_GetIntv">
                            <xsl:with-param name="GetIntv">
                                <xsl:value-of select="App/Req/Risks/GetYearFlag"/>
                            </xsl:with-param>
                        </xsl:call-template>
                    </GetIntv>
                </Risk>
            </Body>
        </TranData>
    </xsl:template>
    <!-- 健康告知 -->
    <xsl:template name="tran_Health">
        <xsl:param name="Health">N</xsl:param>
        <xsl:if test="$Health = 1">Y</xsl:if>
        <xsl:if test="$Health = 0">N</xsl:if>
    </xsl:template>
    <!-- 性别 -->
    <xsl:template name="tran_sex">
        <xsl:param name="sex">0</xsl:param>
        <xsl:if test="$sex = 0">0</xsl:if><!-- 男 -->
        <xsl:if test="$sex = 1">1</xsl:if><!-- 女 -->
        <xsl:if test="$sex = ''">--</xsl:if><!-- 空 -->
    </xsl:template>

    <!-- 保险金领取方式 -->
    <xsl:template name="tran_GetIntv">
        <xsl:param name="GetIntv"></xsl:param>
        <xsl:if test="$GetIntv = 1">0</xsl:if>
        <xsl:if test="$GetIntv = 2"></xsl:if>
        <xsl:if test="$GetIntv = 3"></xsl:if>
    </xsl:template>

    <!-- 红利领取方式 -->
    <xsl:template name="tran_BonusGetMode">
        <xsl:param name="BonusGetMode"></xsl:param>
        <xsl:if test="$BonusGetMode = 0"></xsl:if>
        <xsl:if test="$BonusGetMode = 1">3</xsl:if>
        <xsl:if test="$BonusGetMode = 2">1</xsl:if>
        <xsl:if test="$BonusGetMode = 3">5</xsl:if>
    </xsl:template>

    <!-- 保单的缴费方式 -->
    <xsl:template name="tran_Contpayintv">
        <xsl:param name="payintv">0</xsl:param>
        <xsl:choose>
            <xsl:when test="$payintv = 1">0</xsl:when><!-- 趸交 -->
            <xsl:when test="$payintv = 2">1</xsl:when><!-- 月缴 -->
            <xsl:when test="$payintv = 3">3</xsl:when><!-- 季缴 -->
            <xsl:when test="$payintv = 4">6</xsl:when><!-- 半年交 -->
            <xsl:when test="$payintv = 5">12</xsl:when><!-- 年缴 -->
            <xsl:when test="$payintv = 6">-1</xsl:when><!-- 不定期 -->
            <xsl:otherwise>
                <xsl:value-of select="0"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <!-- 保障年期/年龄标志 -->
    <xsl:template name="tran_PbIYF">
        <xsl:param name="PbInsuYearFlag">2</xsl:param>
        <xsl:choose>
            <xsl:when test="$PbInsuYearFlag = 5">A</xsl:when>
            <xsl:when test="$PbInsuYearFlag = 2">M</xsl:when>
            <xsl:when test="$PbInsuYearFlag = 1">D</xsl:when>
            <xsl:when test="$PbInsuYearFlag = 4">Y</xsl:when>
            <xsl:when test="$PbInsuYearFlag = 6">A</xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="0"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    <!-- 证件类型
    <xsl:template name="tran_idtype">
        <xsl:param name="idtype">0</xsl:param>
        <xsl:if test="$idtype = 110001">0</xsl:if>
        <xsl:if test="$idtype = 110002">0</xsl:if>
        <xsl:if test="$idtype = 110003">5</xsl:if>
        <xsl:if test="$idtype = 110004">5</xsl:if>
        <xsl:if test="$idtype = 110005">6</xsl:if>
        <xsl:if test="$idtype = 110006">6</xsl:if>
        <xsl:if test="$idtype = 110011">8</xsl:if>
        <xsl:if test="$idtype = 110012">8</xsl:if>
        <xsl:if test="$idtype = 110013">8</xsl:if>
        <xsl:if test="$idtype = 110014">8</xsl:if>
        <xsl:if test="$idtype = 110015">8</xsl:if>
        <xsl:if test="$idtype = 110016">8</xsl:if>
        <xsl:if test="$idtype = 110017">8</xsl:if>
        <xsl:if test="$idtype = 110018">8</xsl:if>
        <xsl:if test="$idtype = 110019">8</xsl:if>
        <xsl:if test="$idtype = 110020">8</xsl:if>
        <xsl:if test="$idtype = 110021">8</xsl:if>
        <xsl:if test="$idtype = 110022">8</xsl:if>
        <xsl:if test="$idtype = 110023">1</xsl:if>
        <xsl:if test="$idtype = 110024">1</xsl:if>
        <xsl:if test="$idtype = 110025">1</xsl:if>
        <xsl:if test="$idtype = 110026">1</xsl:if>
        <xsl:if test="$idtype = 110027">2</xsl:if>
        <xsl:if test="$idtype = 110028">7</xsl:if>
        <xsl:if test="$idtype = 110029">8</xsl:if>
        <xsl:if test="$idtype = 110030">8</xsl:if>
        <xsl:if test="$idtype = 110031">7</xsl:if>
        <xsl:if test="$idtype = 110032">8</xsl:if>
        <xsl:if test="$idtype = 110033">3</xsl:if>
        <xsl:if test="$idtype = 110034">8</xsl:if>
        <xsl:if test="$idtype = 110035">3</xsl:if>
        <xsl:if test="$idtype = 110036">8</xsl:if>
        <xsl:if test="$idtype = 119998">8</xsl:if>
        <xsl:if test="$idtype = 119999">8</xsl:if>
    </xsl:template>
    -->
    <!-- 证件类型  -->
    <xsl:template name="tran_idtype">
        <xsl:param name="idtype">0</xsl:param>
        <xsl:if test="$idtype = 110001">0</xsl:if><!-- 身份证 -->
        <xsl:if test="$idtype = 110002">0</xsl:if><!-- 重号身份证 -->
        <xsl:if test="$idtype = 110003">5</xsl:if><!-- 临时身份证 -->
        <xsl:if test="$idtype = 110004">5</xsl:if><!-- 重号临时身份证 -->
        <xsl:if test="$idtype = 110005">6</xsl:if><!-- 户口薄 -->
        <xsl:if test="$idtype = 110006">6</xsl:if><!-- 重号户口薄 -->
        <xsl:if test="$idtype = 110011">8</xsl:if>
        <xsl:if test="$idtype = 110012">8</xsl:if>
        <xsl:if test="$idtype = 110013">2</xsl:if>
        <xsl:if test="$idtype = 110014">2</xsl:if>
        <xsl:if test="$idtype = 110015">2</xsl:if>
        <xsl:if test="$idtype = 110016">2</xsl:if>
        <xsl:if test="$idtype = 110017">2</xsl:if>
        <xsl:if test="$idtype = 110018">2</xsl:if>
        <xsl:if test="$idtype = 110019">4</xsl:if>
        <xsl:if test="$idtype = 110020">4</xsl:if>
        <xsl:if test="$idtype = 110021">4</xsl:if>
        <xsl:if test="$idtype = 110022">4</xsl:if>
        <xsl:if test="$idtype = 110023">1</xsl:if>
        <xsl:if test="$idtype = 110024">1</xsl:if>
        <xsl:if test="$idtype = 110025">1</xsl:if>
        <xsl:if test="$idtype = 110026">1</xsl:if>
        <xsl:if test="$idtype = 110027">2</xsl:if>
        <xsl:if test="$idtype = 110028">2</xsl:if>
        <xsl:if test="$idtype = 110029">2</xsl:if>
        <xsl:if test="$idtype = 110030">2</xsl:if>
        <xsl:if test="$idtype = 110031">7</xsl:if>
        <xsl:if test="$idtype = 110032">7</xsl:if>
        <xsl:if test="$idtype = 110033">3</xsl:if>
        <xsl:if test="$idtype = 110034">3</xsl:if>
        <xsl:if test="$idtype = 110035">3</xsl:if>
        <xsl:if test="$idtype = 110036">3</xsl:if>
        <xsl:if test="$idtype = 119998">8</xsl:if>
        <xsl:if test="$idtype = 119999">8</xsl:if>
    </xsl:template>

    <!-- 关系 -->
    <xsl:template name="tran_relation">
        <xsl:param name="RelaToInsured">00</xsl:param>
        <xsl:if test="$RelaToInsured = 01">00</xsl:if>
        <xsl:if test="$RelaToInsured = 02">01</xsl:if>
        <xsl:if test="$RelaToInsured = 03">01</xsl:if>
        <xsl:if test="$RelaToInsured = 04">04</xsl:if>
        <xsl:if test="$RelaToInsured = 05">04</xsl:if>
        <xsl:if test="$RelaToInsured = 06">03</xsl:if>
        <xsl:if test="$RelaToInsured = 07">03</xsl:if>
        <xsl:if test="$RelaToInsured = 08">31</xsl:if>
        <xsl:if test="$RelaToInsured = 09">31</xsl:if>
        <xsl:if test="$RelaToInsured = 10">21</xsl:if>
        <xsl:if test="$RelaToInsured = 11">21</xsl:if>
        <xsl:if test="$RelaToInsured = 12">25</xsl:if>
        <xsl:if test="$RelaToInsured = 13">25</xsl:if>
        <xsl:if test="$RelaToInsured = 14">25</xsl:if>
        <xsl:if test="$RelaToInsured = 15">25</xsl:if>
        <xsl:if test="$RelaToInsured = 16">12</xsl:if>
        <xsl:if test="$RelaToInsured = 17">12</xsl:if>
        <xsl:if test="$RelaToInsured = 18">12</xsl:if>
        <xsl:if test="$RelaToInsured = 19">12</xsl:if>
        <xsl:if test="$RelaToInsured = 20">25</xsl:if>
        <xsl:if test="$RelaToInsured = 21">25</xsl:if>
        <xsl:if test="$RelaToInsured = 22">25</xsl:if>
        <xsl:if test="$RelaToInsured = 23">25</xsl:if>
        <xsl:if test="$RelaToInsured = 24">25</xsl:if>
        <xsl:if test="$RelaToInsured = 25">25</xsl:if>
        <xsl:if test="$RelaToInsured = 26">25</xsl:if>
        <xsl:if test="$RelaToInsured = 27">25</xsl:if>
        <xsl:if test="$RelaToInsured = 28">25</xsl:if>
        <xsl:if test="$RelaToInsured = 29">25</xsl:if>
        <xsl:if test="$RelaToInsured = 30">25</xsl:if>

    </xsl:template>

    <!-- 缴费年期/年龄类型    暂时没用 -->
    <xsl:template name="tran_payendyearflag" match="PaymentDurationMode">
        <xsl:choose>
            <xsl:when test=".=1">A</xsl:when><!-- 缴至某确定年龄 -->
            <xsl:when test=".=2">M</xsl:when><!-- 年 -->
            <xsl:when test=".=3">D</xsl:when><!-- 月 -->
            <xsl:when test=".=4">Y</xsl:when><!-- 日 -->
            <xsl:when test=".=0"></xsl:when><!-- 终缴费 -->
            <xsl:otherwise>--</xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    <!-- 缴费形式 -->

    <!-- 国籍 -->
    <xsl:template name="tran_idnationality">
        <xsl:param name="idnationality"></xsl:param>
        <xsl:if test="$idnationality = 156">CHN</xsl:if><!-- 中国 -->
        <xsl:if test="$idnationality = 344">CHN</xsl:if>
        <xsl:if test="$idnationality = 158">CHN</xsl:if>
        <xsl:if test="$idnationality = 446">CHN</xsl:if>
        <xsl:if test="$idnationality = 392">OTH</xsl:if><!-- 其他 -->
        <xsl:if test="$idnationality = 840">OTH</xsl:if>
        <xsl:if test="$idnationality = 634">OTH</xsl:if>
        <xsl:if test="$idnationality = 826">OTH</xsl:if>
        <xsl:if test="$idnationality = 250">OTH</xsl:if>
        <xsl:if test="$idnationality = 276">OTH</xsl:if>
        <xsl:if test="$idnationality = 410">OTH</xsl:if>
        <xsl:if test="$idnationality = 702">OTH</xsl:if>
        <xsl:if test="$idnationality = 360">OTH</xsl:if>
        <xsl:if test="$idnationality = 356">OTH</xsl:if>
        <xsl:if test="$idnationality = 380">OTH</xsl:if>
        <xsl:if test="$idnationality = 458">OTH</xsl:if>
        <xsl:if test="$idnationality = 764">OTH</xsl:if>
        <xsl:if test="$idnationality = 999">OTH</xsl:if>
    </xsl:template>

    <!-- 职业类别 -->
    <xsl:template name="tran_idjobCode">
        <xsl:param name="idjobCode"></xsl:param>
        <xsl:if test="$idjobCode = 01">0101002</xsl:if><!-- 农夫 -->
        <xsl:if test="$idjobCode = 02">0001003</xsl:if><!-- 企\事业单位负责人 -->
        <xsl:if test="$idjobCode = 03">0001001</xsl:if><!-- 机关团体公司行号内勤人员 -->
        <xsl:if test="$idjobCode = 04">0101008</xsl:if><!-- xxx技术人员 -->
        <xsl:if test="$idjobCode = 05">0501008</xsl:if><!-- 客运车司机及服务员 -->
        <xsl:if test="$idjobCode = 06">1202002</xsl:if><!-- 商业店员 -->
        <xsl:if test="$idjobCode = 07">2147004</xsl:if><!-- 无业人员 -->
    </xsl:template>

    <!-- 渠道 -->
    <xsl:template name="tran_Type">
        <xsl:param name="Type"></xsl:param>
        <xsl:if test="$Type = 11">ybt</xsl:if>
        <xsl:if test="$Type = 01">wy</xsl:if>
        <xsl:if test="$Type = 04">atm</xsl:if>
        <xsl:if test="$Type = 02">zy</xsl:if>
    </xsl:template>

</xsl:stylesheet>

