<?xml version="1.0" encoding="GBK"?>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:java="http://xml.apache.org/xslt/java" exclude-result-prefixes="java">
    <xsl:template match="/TranData">
        <ABCB2I>
            <Header>
                <RetCode>009999</RetCode>
                <RetMsg><xsl:value-of select="Head/Desc"/></RetMsg>
                <SerialNo></SerialNo>
                <InsuSerial></InsuSerial>
                <TransDate></TransDate>
                <TransTime></TransTime>
                <BankCode></BankCode>
                <CorpNo></CorpNo>
                <TransCode></TransCode>
            </Header>
            <App>
                <Ret>
                </Ret>
            </App>
        </ABCB2I>
    </xsl:template>
</xsl:stylesheet>