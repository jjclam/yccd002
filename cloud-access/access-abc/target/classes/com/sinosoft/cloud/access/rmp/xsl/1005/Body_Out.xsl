<?xml version="1.0" encoding="GBK"?>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:java="http://xml.apache.org/xslt/java" exclude-result-prefixes="java">
<xsl:template match="/TranData">
<ABCB2I>
	<Header>
		<xsl:if test="Head/Flag=0">
			<RetCode>000000</RetCode>
			<RetMsg>交易成功</RetMsg>
		</xsl:if>
		<xsl:if test="Head/Flag=1">
			<RetCode>009999</RetCode>
			<RetMsg><xsl:value-of select="Head/Desc"/></RetMsg>
		</xsl:if>
		<xsl:if test="Head/Flag=2">
			<RetCode>009990</RetCode>
			<RetMsg><xsl:value-of select="Head/Desc"/></RetMsg>
		</xsl:if>
		<SerialNo>
			<xsl:value-of select="Head/TranNo"/>
		</SerialNo>
		<InsuSerial>
			<xsl:value-of select="Head/InsuSerial"/>
		</InsuSerial>
		<TransDate>
			<xsl:value-of
					select="java:com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.date10to8(Head/TranDate)"/>
		</TransDate>
		<TransTime>
			<xsl:value-of
					select="java:com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.timeString10To8(Head/TranTime)"/>
		</TransTime>
		<BankCode>
			<xsl:value-of select="Head/BankCode"/>
		</BankCode>
		<CorpNo>
			<xsl:value-of select="Head/CorpNo"/>
		</CorpNo>
		<TransCode>
			<xsl:value-of select="Head/TransCode"/>
		</TransCode>
	</Header>
	<App>
		<Ret>
			<AppResult><xsl:value-of select="Body/ResponseObj/AppResult"/></AppResult><!-- 核保结果 -->
			<RiskCode><xsl:value-of select="Body/LCPols/LCPol[1]/RiskCode"/></RiskCode><!-- 保险公司方险种代码 -->
			<xsl:variable name="appResult" select="Body/ResponseObj/AppResult"/>
			<PolicyNo><xsl:value-of select="Body/LCCont/ContNo"/></PolicyNo><!-- 保单号 -->
			<!-- Double默认值0.0,当APPResult节点值为1（失败）时Prem节点值应为空-->
			<xsl:choose>
				<xsl:when test="$appResult = '1'">
					<Prem></Prem>
				</xsl:when>
				<xsl:otherwise>
					<Prem><xsl:value-of select="Body/LCCont/Prem"/></Prem><!-- 保费 -->
				</xsl:otherwise>
			</xsl:choose>
		</Ret>
	</App>
</ABCB2I>
</xsl:template>
</xsl:stylesheet>