<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:java="http://xml.apache.org/xslt/java"
                exclude-result-prefixes="java">

    <xsl:template match="AUTO_XML_DATA">
        <xsl:variable name="head" select="request/header"/>
           <AliHead>
               <!--版本号-->
               <version>
                   <xsl:value-of select="$head/version"/>
               </version>
               <!--  接口代码  -->
               <function>
                   <xsl:value-of select="$head/function"/>
               </function>
               <!--  报文发送时间 格式：yyyyMMddHHmmss  -->
               <transTime>
                   <xsl:value-of select="$head/transTime"/>
               </transTime>
               <!--  请求发起系统所在时区 可选 transTimeZone  -->
               <transTimeZone>
                   <xsl:value-of select="$head/transTimeZone"/>
               </transTimeZone>
               <!--  唯一编码，由发起方生成，应答方原样返回  -->
               <reqMsgId>
                   <xsl:value-of select="$head/reqMsgId"/>
               </reqMsgId>
               <!--  数据格式 xml/json  -->
               <format>
                   <xsl:value-of select="$head/format"/>
               </format>
               <!--  签名类型 RSA  -->
               <signType>
                   <xsl:value-of select="$head/signType"/>
               </signType>
               <!--  保险公司同步返回还是异步返回 默认同步返回 0：同步；1：异步  -->
               <asyn>
                   <xsl:value-of select="$head/asyn"/>
               </asyn>
               <!--  合作伙伴ID  -->
               <cid>
                   <xsl:value-of select="$head/cid"/>
               </cid>
           </AliHead>
    </xsl:template>
</xsl:stylesheet>
