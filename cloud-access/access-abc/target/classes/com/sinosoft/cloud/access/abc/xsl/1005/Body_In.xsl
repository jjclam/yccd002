<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:java="http://xml.apache.org/xslt/java" exclude-result-prefixes="java">
    <xsl:output method="xml" indent="yes"/>
    <!--新单试算结果查询-->
    <xsl:template match="ABCB2I">
        <TranData>
            <Body>
                <Global>
                    <ApplySerial>
                        <xsl:value-of select="App/Req/ApplySerial"/>
                    </ApplySerial>
                </Global>
            </Body>
        </TranData>
    </xsl:template>
</xsl:stylesheet>
