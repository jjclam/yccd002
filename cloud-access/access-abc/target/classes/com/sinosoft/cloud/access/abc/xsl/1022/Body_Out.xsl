<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
				xmlns:java="http://xml.apache.org/xslt/java"
				exclude-result-prefixes="java">
	<xsl:template match="/TranData">
		<xsl:variable name="risks" select="TranData/Body/LCPols"/>
		<xsl:variable name="lccont" select="Body/LCCont"/>
		<xsl:variable name="global" select="Body/Global"/>
		<xsl:variable name="bqresult" select="Body/BQResult"/>
		<xsl:variable name="mainRisk" select="$risks/LCPol[PolNo=MainPolNo]"/>
		<xsl:variable name="addtRisk" select="$risks/LCPol[PolNo!=MainPolNo]"/>
		<ABCB2I>
			<Header>
				<xsl:if test="Head/Flag=0">
					<RetCode>000000</RetCode>
					<RetMsg>交易成功</RetMsg>
				</xsl:if>
				<xsl:if test="Head/Flag=1">
					<RetCode>009999</RetCode>
					<RetMsg>
						<xsl:value-of select="Head/Desc"/>
					</RetMsg>
				</xsl:if>
				<xsl:if test="Head/Flag=2">
					<RetCode>009990</RetCode>
					<RetMsg>
						<xsl:value-of select="Head/Desc"/>
					</RetMsg>
				</xsl:if>
				<SerialNo>
					<xsl:value-of select="Head/TranNo"/>
				</SerialNo>
				<InsuSerial>
					<xsl:value-of select="Head/InsuSerial"/>
				</InsuSerial>
				<TransDate>
					<xsl:value-of
							select="java:com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.date10to8(Head/TranDate)"/>
				</TransDate>
				<TransTime>
					<xsl:value-of
							select="java:com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.timeString10To8(Head/TranTime)"/>
				</TransTime>
				<BankCode>
					<xsl:value-of select="Head/BankCode"/>
				</BankCode>
				<CorpNo>
					<xsl:value-of select="Head/CorpNo"/>
				</CorpNo>
				<TransCode>
					<xsl:value-of select="Head/TransCode"/>
				</TransCode>
			</Header>

			<App>
				<Ret>
					<xsl:if test="Head/Flag=0">
						<xsl:choose>
							<xsl:when test="$global/TransferFlag=1">
								<TransferAmt>
									<xsl:value-of select="$bqresult/TransferAmt"/>
								</TransferAmt>
								<PolicyAmt>
									<xsl:value-of select="$lccont/Prem"/>
								</PolicyAmt>
								<PolicyNo>
									<xsl:value-of select="$lccont/ContNo"/>
								</PolicyNo>
								<Prem>
									<xsl:value-of
											select="java:com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.getZBPrem($lccont/Prem,$bqresult/TransferAmt)"/>
									<!--     <xsl:value-of select="BQResult/Prem"/>-->
								</Prem>
							</xsl:when>
							<xsl:otherwise>
								<xsl:choose>
									<xsl:when test="$mainRisk/RiskCode = '1032' or $mainRisk/RiskCode = '7058'">
										<Prem>
											<!--<xsl:value-of select="java:com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.fenToYuan(Prem)" />-->
											<xsl:value-of select="$lccont/Prem"/>
										</Prem>
										<Addt>
											<xsl:for-each select="$addtRisk">
												<xsl:call-template name="generateBTMark">
													<xsl:with-param name="value" select="Prem" />
													<xsl:with-param name="number" select="position()" />
													<xsl:with-param name="name" select="string('Prem')" />
												</xsl:call-template>
											</xsl:for-each>
										</Addt>
										<Risks>
											<Prem>
												<xsl:value-of select="format-number($mainRisk/Prem,'#0.00')"/>
											</Prem>
										</Risks>
									</xsl:when>
									<xsl:otherwise>
										<Prem>
											<xsl:value-of select="$lccont/Prem"/>
										</Prem>
										<PolicyNo>
											<xsl:value-of select="$lccont/ContNo"/>
										</PolicyNo>
									</xsl:otherwise>
								</xsl:choose>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:if>
				</Ret>
			</App>
		</ABCB2I>
	</xsl:template>

	<!-- 自定义标签 -->
	<xsl:template name="generateBTMark">
		<xsl:param name="number"/>
		<xsl:param name="name"/>
		<xsl:param name="value"/>
		<xsl:text disable-output-escaping="yes">&lt;</xsl:text><xsl:value-of select="concat($name,$number)" /><xsl:text disable-output-escaping="yes">&gt;</xsl:text>
		<xsl:value-of select="$value" />
		<xsl:text disable-output-escaping="yes">&lt;</xsl:text><xsl:value-of select="concat('/',$name,$number)" /><xsl:text disable-output-escaping="yes">&gt;</xsl:text>
	</xsl:template>
</xsl:stylesheet>