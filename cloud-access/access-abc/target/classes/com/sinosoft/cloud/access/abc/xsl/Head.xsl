<?xml version="1.0" encoding="GBK"?>

<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:java="http://xml.apache.org/xslt/java"
                exclude-result-prefixes="java">

    <xsl:template match="Package">
        <xsl:variable name="riskCode" select="App/Req/Risks/RiskCode"/>
        <Head>
            <!-- 日期-->
            <TransDate>
                <xsl:value-of
                        select="java:com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.getDate(Head/TransTime)"/>
            </TransDate>
            <!--时间-->
            <TransTime>
                <xsl:value-of
                        select="java:com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.getTime(Head/TransTime)"/>
            </TransTime>
            <TranTime>
                <xsl:value-of select="Head/TransTime"/>
            </TranTime>
            <!--<TranDate>&lt;!&ndash; 交易日期 &ndash;&gt;
                <xsl:value-of select="java:com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.dateFormat8To10(Head/TransDate)"/>
            </TranDate>
            <TranTime>&lt;!&ndash; 交易时间 &ndash;&gt;
                <xsl:value-of select="java:com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.timeFormat(Head/TransTime)"/>
            </TranTime>-->

            <ThirdPartyCode>
                <xsl:value-of select="Head/ThirdPartyCode"/>
            </ThirdPartyCode>
            <TranNo>
                <xsl:value-of select="Head/TransNo"/>
            </TranNo>
            <TransCode>
                <xsl:value-of select="Head/TransType"/>
            </TransCode>



           <!-- <ZoneNo>
                <xsl:value-of select="Head/ProvCode"/>
            </ZoneNo>
            <NodeNo>&lt;!&ndash; 网点代码 &ndash;&gt;
                <xsl:value-of select="Head/BranchNo"/>
            </NodeNo>
            <TranCom>2</TranCom>
            <xsl:if test="Head/EntrustWay = 11">
                <BankCode>05</BankCode>
            </xsl:if>
            <xsl:if test="Head/EntrustWay != 11">
                <BankCode>0501</BankCode>
            </xsl:if>
            <FuncFlag>201</FuncFlag>
            <AgentCom></AgentCom>
            <AgentCode>-</AgentCode>
            <BankClerk>
                <xsl:value-of select="App/Req/Base/Saler"/>
            </BankClerk>&lt;!&ndash; xxx柜员姓名 &ndash;&gt;
            <SourceType>
                <xsl:call-template name="tran_Type">
                    <xsl:with-param name="Type">
                        <xsl:value-of select="Head/EntrustWay"/>
                    </xsl:with-param>
                </xsl:call-template>
            </SourceType>
            <SellType>
                <xsl:call-template name="tran_sellType">
                    <xsl:with-param name="sellType">
                        <xsl:value-of select="Head/EntrustWay"/>
                    </xsl:with-param>
                </xsl:call-template>
            </SellType>&lt;!&ndash; 销售方式 &ndash;&gt;
            <InsuSerial>
                <xsl:value-of select="Head/InsuSerial"/>
            </InsuSerial>&lt;!&ndash; 保险公司流水号 &ndash;&gt;
            <CorpNo>
                <xsl:value-of select="Head/CorpNo"/>
            </CorpNo>
            <TransSide>
                <xsl:value-of select="Head/TransSide"/>
            </TransSide>-->
        </Head>
    </xsl:template>

    <!-- 渠道 -->
    <xsl:template name="tran_Type">
        <xsl:param name="Type"></xsl:param>
        <xsl:if test="$Type = 11">ABC</xsl:if><!--柜面-->
        <xsl:if test="$Type = 01">ABC_EBank</xsl:if><!--网银-->
        <xsl:if test="$Type = 04">ABC_SST</xsl:if><!--自助终端-->
        <xsl:if test="$Type = 02">ABC_Mobile</xsl:if><!--掌银-->
        <xsl:if test="$Type = 22">ABC_Supotc</xsl:if><!--超贵-->
        <xsl:if test="$Type = 08">ABC_Estwd</xsl:if><!--e管家-->
    </xsl:template>

    <!-- 销售方式 -->
    <xsl:template name="tran_sellType">
        <xsl:param name="sellType"></xsl:param>
        <xsl:if test="$sellType = 11">08</xsl:if>
        <xsl:if test="$sellType = 01">22</xsl:if>
        <xsl:if test="$sellType = 04">24</xsl:if>
        <xsl:if test="$sellType = 02">25</xsl:if>
        <xsl:if test="$sellType = 22">27</xsl:if>
        <xsl:if test="$sellType = 08">26</xsl:if>
    </xsl:template>

</xsl:stylesheet>