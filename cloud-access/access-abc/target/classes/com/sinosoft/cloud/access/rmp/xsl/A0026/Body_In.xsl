<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:java="http://xml.apache.org/xslt/java" exclude-result-prefixes="java">
	<xsl:output method="xml" indent="yes"/>
	<!--自动冲正交易-->
	<xsl:template match="Package">
		<TranData>
			<Body>

				<LCCont>
					<ContNo>
						<xsl:value-of select="RequestNodes/RequestNode/ContNo"/>
					</ContNo>
					<!--合作机构订单号 ThirdPartyOrderId-->
					<ThirdPartyOrderId>
						<xsl:value-of select="RequestNodes/RequestNode/ThirdPartyOrderId"/>
					</ThirdPartyOrderId>
				</LCCont>
				<LCContState>
					<ModifyDate>
						<xsl:value-of select="Head/TransTime"/>
					</ModifyDate>
					<!--下面三个撤单时未用到  直接返回-->
					<MakeDate>
						<xsl:value-of select="RequestNodes/RequestNode/ApplyDate"/>
					</MakeDate>
					<MakeTime>
						<xsl:value-of select="RequestNodes/RequestNode/ApplyTime"/>
					</MakeTime>
					<State>
						<xsl:value-of select="RequestNodes/RequestNode/ApplyFlag"/>
					</State>
					<Remark>
						<xsl:value-of select="Head/ThirdPartyCode"/>
					</Remark>
				</LCContState>

				<LKTransStatus>
					<BankCode>05</BankCode>
					<!--处理标志-->
					<FuncFlag>
						<xsl:value-of select="RequestNodes/RequestNode/ApplyFlag"/>
					</FuncFlag>
					<!--网点号-->
					<!--<BankNode>
						<xsl:value-of select="Head/BranchNo"/>
					</BankNode>-->
					<!--省号-->
					<!--<BankBranch>
						<xsl:value-of select="Head/ProvCode"/>
					</BankBranch>-->
					<TransDate></TransDate>
					<!--原交易流水号-->
					<TransNo>
						<xsl:value-of select="Head/TransNo"/>
					</TransNo>
					<!--原交易流水号-->
					<!--<TransCode>
						<xsl:value-of select="App/Req/OrgSerialNo"/>
					</TransCode>-->
					<!--销售柜员-->
					<BankOperator></BankOperator>
					<!--<PolNo>
						<xsl:value-of select="App/Req/PolicyNo"/>
					</PolNo>-->
				</LKTransStatus>
				<LKTransTrackses>
					<LKTransTracks>
						<!--交易流水号-->
						<TransNo>
							<xsl:value-of select="Head/TransNo"/>
						</TransNo>

						<!--交易编码-->
						<TransCode>
							<xsl:value-of select="Head/TransCode"/>
						</TransCode>

						<!--原交易编码-->
						<!--<TransCodeOri>
							<xsl:value-of select="Head/TransCode"/>
						</TransCodeOri>-->
						<!--交易子渠道-->
						<!--<AccessChnlSub>
							<xsl:value-of select="Head/EntrustWay"/>
						</AccessChnlSub>-->
						<BankCode>05</BankCode>

						<!--<BankBranch>
							<xsl:value-of select="Head/ProvCode"/>
						</BankBranch>
						<BankNode>
							<xsl:value-of select="Head/BranchNo"/>
						</BankNode>-->


						<TransDate>
							<xsl:value-of
									select="java:com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.getDate(Head/TransTime)"/>
						</TransDate>
						<!--时间-->
						<TransTime>
							<xsl:value-of
									select="java:com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.getTime(Head/TransTime)"/>
						</TransTime>
						<!--1 银保通 2 移动展业  3 微信卡单 4 微信银保通-->
						<SellsWay>1</SellsWay>
					</LKTransTracks>
				</LKTransTrackses>
			</Body>
		</TranData>
	</xsl:template>
</xsl:stylesheet>
