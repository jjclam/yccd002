<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:java="http://xml.apache.org/xslt/java"
                exclude-result-prefixes="java">

    <xsl:template match="Package">
        <TranData>
            <xsl:variable name="header" select="Head"/>
            <xsl:variable name="req" select="RequestNodes/RequestNode"/>
            <xsl:variable name="appnt" select="RequestNodes/RequestNode/Applicant"/>
            <!-- <xsl:variable name="addt" select="RequestNodes/RequestNode/Addt"/>-->
            <!--<xsl:variable name="base" select="RequestNodes/RequestNode/Base"/>-->
            <!--<xsl:variable name="risks" select="RequestNodes/RequestNode/PolicyLiabilities"/>-->

            <xsl:variable name="insureds" select="RequestNodes/RequestNode/Insureds"/>
            <!--<xsl:variable name="loan" select="RequestNodes/RequestNode/Loan"/>-->
            <xsl:variable name="bnfs" select="RequestNodes/RequestNode/Beneficiaries"/>
            <xsl:variable name="bnf" select="$bnfs/Beneficiary"/>
            <xsl:variable name="flag" select="$header/EntrustWay"/>
            <!-- <xsl:variable name="risk" select="$risks/risk"/>-->
            <xsl:variable name="Insured" select="RequestNodes/RequestNode/Insureds/Insured"/>
            <xsl:variable name="policyLiabilitys" select="RequestNodes/RequestNode/Insureds/Insured/PolicyLiabilities"/>
            <xsl:variable name="policyLiability"
                          select="RequestNodes/RequestNode/Insureds/Insured/PolicyLiabilities/PolicyLiability"/>
            <xsl:variable name="DutiesInfo"
                          select="RequestNodes/RequestNode/Insureds/Insured/PolicyLiabilities/PolicyLiability/DutiesInfo"/>
            <xsl:variable name="DutyInfo"
                          select="RequestNodes/RequestNode/Insureds/Insured/PolicyLiabilities/PolicyLiability/DutiesInfo/DutyInfo"/>
            <xsl:variable name="InsuImpartInfo"
                          select="RequestNodes/RequestNode/Insureds/Insured/InsuImpartInfo"/>
            <xsl:variable name="InsuImpart"
                          select="RequestNodes/RequestNode/Insureds/Insured/InsuImpartInfo/InsuImpart"/>


            <xsl:variable name="varRiskCodess">
                <xsl:call-template name="tran_riskCode">
                    <xsl:with-param name="riskCode"
                                    select="$policyLiability/RiskCode"/>
                    <!--<xsl:with-param name="riskCode" select="$risks/RiskCode"/>-->
                </xsl:call-template>
            </xsl:variable>
            <Body>

                <LKTransTrackses>
                    <LKTransTracks>
                        <!--交易流水号-->
                        <TransNo>
                            <xsl:value-of select="$header/TransNo"/>
                        </TransNo>
                        <!--交易编码-->
                        <TransCode>
                            <xsl:value-of select="$header/TransType"/>
                        </TransCode>

                        <ProposalNo><!-- 投保单号 -->
                            <xsl:value-of select="$req/ProposalNo"/>
                        </ProposalNo>
                        <!-- 日期-->
                        <TransDate>
                            <xsl:value-of
                                    select="java:com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.getDate($header/TransTime)"/>
                        </TransDate>
                        <!--时间-->
                        <TransTime>
                            <xsl:value-of
                                    select="java:com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.getTime($header/TransTime)"/>
                        </TransTime>
                        <!--第三方渠道编码-->
                        <!--<ThirdPartyCode>
                            <xsl:value-of select="$header/ThirdPartyCode"/>
                        </ThirdPartyCode>-->

                        <!--&lt;!&ndash;交易类型&ndash;&gt;
                        <TransType>
                            <xsl:value-of select="$header/TransType"/>
                        </TransType>-->
                        <!--交互对象-->
                        <!--<Coworker>
                            <xsl:value-of select="$header/Coworker"/>
                        </Coworker>-->
                        <!--保单号 新增-->
                        <ContNo>
                            <xsl:value-of select="$req/ContNo"/>
                        </ContNo>
                        <BankCode>05</BankCode>
                        <!--&lt;!&ndash;这是干啥的&ndash;&gt;
                        <xsl:if test="Header/EntrustWay = 11">
                            <BankCode>05</BankCode>
                        </xsl:if>
                        <xsl:if test="Header/EntrustWay != 11">
                            <BankCode>0501</BankCode>
                        </xsl:if>-->
                        <BankBranch>26</BankBranch>

                        <!-- <xsl:value-of select="$header/ProvCode"/>-->

                        <BankNode>26</BankNode>
                    </LKTransTracks>
                </LKTransTrackses>


                <LKTransStatus>
                    <TransNo>
                        <xsl:value-of select="$header/TransNo"/>
                    </TransNo>
                    <TransCode>
                        <xsl:value-of select="$header/TransType"/>
                    </TransCode>
                    <ProposalNo><!-- 投保单号 -->
                        <xsl:value-of select="$req/ProposalNo"/>
                    </ProposalNo>
                    <PrtNo><!-- 投保单号 -->
                        <xsl:value-of select="$req/ProposalNo"/>
                    </PrtNo>
                    <BankCode>05</BankCode>
                    <!-- &lt;!&ndash;what&ndash;&gt;
                     <xsl:if test="Header/EntrustWay = 11 or $varRiskCodess = '6807'">
                         <BankCode>05</BankCode>
                     </xsl:if>
                     <xsl:if test="Header/EntrustWay != 11 and $varRiskCodess != '6807'">
                         <BankCode>0501</BankCode>
                     </xsl:if>-->
                    <!-- 日期-->
                    <TransDate>
                        <xsl:value-of
                                select="java:com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.getDate($header/TransTime)"/>
                    </TransDate>
                    <!--时间-->
                    <TransTime>
                        <xsl:value-of
                                select="java:com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.getTime($header/TransTime)"/>
                    </TransTime>

                    <!--what-->
                    <BankNode>26</BankNode>

                    <!-- <xsl:value-of select="Header/BranchNo"/>-->

                    <FuncFlag>
                        <xsl:value-of select="$header/TransType"/>
                    </FuncFlag>
                    <BankOperator>
                        <xsl:value-of select="$header/Tlid"/>
                    </BankOperator>



                </LKTransStatus>

                <!--LCContPojo-->
                <LCCont>
                    <!-- 保单号ContNo-->
                    <ContNo>
                        <xsl:value-of select="$req/ContNo"/>
                    </ContNo>
                    <ContType>1</ContType>
                    <PrtNo>
                        <xsl:value-of select="$req/ProposalNo"/>
                    </PrtNo>
                    <EAuto>0</EAuto>

                    <!--缴费方式-->
                    <PayIntv>
                        <xsl:value-of select="$req/PayIntv"/>
                    </PayIntv>

                    <!--新增代理人编码-->
                    <AgentCode>
                        <xsl:value-of select="$req/AgentNo"/>
                    </AgentCode>
                    <!-- 销售方式 -->
                    <SellType>
                        <xsl:value-of select="$header/ThirdPartyCode"/>
                    </SellType>
                    <!--投保入口新增-->
                    <!--投保入口ABCode-->
                    <AgentBankCode>
                        <xsl:value-of select="$req/ABCode"/>
                    </AgentBankCode>

                    <!--<BankAgent>
                        <xsl:value-of select="$base/SalerCertNo"/>
                    </BankAgent>-->
                    <!-- 投保单号 -->
                    <ProposalContNo>
                        <xsl:value-of select="$req/ProposalNo"/>
                    </ProposalContNo>

                    <CValiDate>
                        <xsl:value-of select="$req/InsuranceStartPeriod"/>
                    </CValiDate>

                    <!-- 新增字段 PolApplyDate -->
                    <PolApplyDate>
                        <xsl:value-of select="$req/SignedDate"/>
                    </PolApplyDate>


                    <PayMode>
                        <xsl:value-of select="$req/RenewalPaymentInfo/PayMode"/>
                    </PayMode>
                    <BankCode>
                        <xsl:value-of select="$req/RenewalPaymentInfo/PayBankCode"/>
                    </BankCode>
                    <BankProvince>
                        <xsl:value-of select="$req/RenewalPaymentInfo/BankProvinceCode"/>
                    </BankProvince>
                    <BankCity>
                        <xsl:value-of select="$req/RenewalPaymentInfo/BankCityCode"/>
                    </BankCity>
                    <BankAccNo>
                        <xsl:value-of select="$req/RenewalPaymentInfo/PayAcountNo"/>
                    </BankAccNo>
                    <AccType>
                        <xsl:value-of select="$req/RenewalPaymentInfo/CardType"/>
                    </AccType>

                    <AccName>
                        <xsl:value-of select="$req/RenewalPaymentInfo/PayAccountName"/>
                    </AccName>

                    <NewBankAccNo>
                        <xsl:value-of select="$req/RenewalPaymentInfo/PayAcountNo"/>
                    </NewBankAccNo>

                    <NewAccName>
                        <xsl:value-of select="$req/RenewalPaymentInfo/PayAccountName"/>
                    </NewAccName>
                    <!-- <BranchCertNo>
                         <xsl:value-of select="$req/BranchCertNo"/>
                     </BranchCertNo>-->
                    <BankAgent>
                        <xsl:value-of select="$req/ZJAgentCode"/>
                    </BankAgent>
                    <BankAgentName>
                        <xsl:value-of select="$req/ZJAgentName"/>
                    </BankAgentName>
                    <!-- <AgentBankCode>
                         <xsl:value-of select="$req/BankCode"/>
                     </AgentBankCode>-->
                    <AgentCom>
                        <xsl:value-of select="$req/AgentCom"/>
                    </AgentCom>


                    <!--投保人姓名-->
                    <AppntName>
                        <xsl:value-of select="$appnt/AppName"/>
                    </AppntName>
                    <!-- 投保人性别-->
                    <AppntSex>
                        <xsl:value-of select="$appnt/AppSex"/>
                    </AppntSex>
                    <!--投保人出生日期-->
                    <AppntBirthday>
                        <xsl:value-of
                                select="$appnt/AppBirthday"/>
                    </AppntBirthday>
                    <!-- 投保人证件类型 -->
                    <AppntIDType>
                        <xsl:value-of select="$appnt/AppIDType"/>
                    </AppntIDType>
                    <!-- 投保人证件号 -->
                    <AppntIDNo>
                        <xsl:value-of select="$appnt/AppIDNo"/>
                    </AppntIDNo>
                    <!-- 被保人姓名-->
                    <InsuredName>
                        <xsl:value-of select="$Insured/InsuName"/>
                    </InsuredName>
                    <!--被保人性别-->
                    <InsuredSex>
                        <xsl:value-of select="$Insured/InsuSex"/>
                    </InsuredSex>
                    <!--被保人出生日期-->
                    <InsuredBirthday>
                        <xsl:value-of
                                select="$Insured/InsuBirthday"/>
                    </InsuredBirthday>
                    <!-- 被保人证件类型-->
                    <InsuredIDType>
                        <xsl:value-of select="$Insured/InsuIDType"/>
                    </InsuredIDType>
                    <!--被保人证件号-->
                    <InsuredIDNo>
                        <xsl:value-of select="$Insured/InsuIDNo"/>
                    </InsuredIDNo>
                    <!--合作机构订单号 ThirdPartyOrderId-->
                    <ThirdPartyOrderId>
                        <xsl:value-of select="$req/ThirdPartyOrderId"/>
                    </ThirdPartyOrderId>
                    <!--用户id（用户在合作机构的唯一id）UserId-->
                    <UserId>
                        <xsl:value-of select="$req/UserId"/>
                    </UserId>
                    <SaleChnl>05</SaleChnl>

                    <!-- 自动续保标志 ExpireProcessMode-->
                    <RnewFlag>
                        <xsl:call-template name="tran_renewflag">
                            <xsl:with-param name="flag" select="$req/ExpireProcessMode"/>
                        </xsl:call-template>
                        <!--<xsl:value-of
                                select="java:com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.getRenewFlag($req/ExpireProcessMode)"/>-->
                    </RnewFlag>
                    <Remark>
                        <xsl:value-of select="$req/ProductName"/>
                    </Remark>
                </LCCont>

                <!--GlobalPojo-->
                <Global>

                    <SerialNo>
                        <xsl:value-of select="$header/TransNo"/>
                    </SerialNo>
                    <MainRiskCode>
                        <xsl:value-of select="$varRiskCodess"/>
                    </MainRiskCode>
                    <!--6810 SEO用的指定生效日-->
                    <!--<PoliValidDate>
                        <xsl:value-of
                                select="java:com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.dateFormat8To10($base/PoliValidDate)"/>
                    </PoliValidDate>-->
                    <!--保险起期-->
                    <RiskBeginDate>
                        <xsl:value-of
                                select="$req/InsuranceStartPeriod"/>
                    </RiskBeginDate>
                    <!--保险止期-->
                    <RiskEndDate>
                        <xsl:value-of
                                select="$req/InsuranceEndPeriod"/>
                    </RiskEndDate>

                    <BankCode>0101</BankCode>
                    <EntrustWay>ABC</EntrustWay>
                    <!-- 日期-->
                    <TransDate>
                        <xsl:value-of
                                select="java:com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.getDate($header/TransTime)"/>
                    </TransDate>
                    <!--时间-->
                    <TransTime>
                        <xsl:value-of
                                select="java:com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.getTime($header/TransTime)"/>
                    </TransTime>
                    <TransNo>
                        <xsl:value-of select="$header/TransType"/>
                    </TransNo>
                </Global>

                <!--LCAppntPojo-->
                <LCAppnt>

                    <!-- 投保单号 -->
                    <PrtNo>
                        <xsl:value-of select="$req/proposalNo"/>
                    </PrtNo>
                    <AppntName>
                        <xsl:value-of select="$appnt/AppName"/>
                    </AppntName>
                    <AppntSex>
                        <xsl:value-of select="$appnt/AppSex"/>
                    </AppntSex>
                    <AppntBirthday>
                        <xsl:value-of
                                select="$appnt/AppBirthday"/>
                    </AppntBirthday>
                    <!-- 投保人证件类型 -->
                    <IDType>
                        <xsl:value-of select="$appnt/AppIDType"/>
                    </IDType>
                    <IDNo><!-- 投保人证件号 -->
                        <xsl:value-of select="$appnt/AppIDNo"/>
                    </IDNo>
                    <!-- 投保人证件有效期 -->
                    <IdValiDate>
                        <xsl:value-of select="$appnt/IdexpDate"/>
                    </IdValiDate>
                    <Degree>
                        <xsl:value-of select="$appnt/Degree"/>
                    </Degree>

                    <!-- 投保人国籍 -->
                    <NativePlace>
                        <xsl:value-of select="$appnt/NativePlace"/>
                    </NativePlace>

                    <!--婚姻状况-->
                    <Marriage>
                        <xsl:value-of select="$appnt/Marriage"/>
                    </Marriage>
                    <!--社保标记SSFlag-->
                    <SocialInsuFlag>
                        <xsl:value-of select="$appnt/SocialInsuFlag"/>
                    </SocialInsuFlag>
                    <!--年收入-->
                    <Salary>
                        <xsl:value-of select="$appnt/Salary"/>
                    </Salary>
                    <!--健康状况-->
                    <Health>
                        <xsl:value-of select="$appnt/Health"/>
                    </Health>
                    <SmokeFlag>
                        <xsl:value-of select="$appnt/SmokeFlag"/>
                    </SmokeFlag>
                    <Stature>
                        <xsl:value-of select="$appnt/Stature"/>
                    </Stature>
                    <Avoirdupois>
                        <xsl:value-of select="$appnt/Weight"/>
                    </Avoirdupois>
                    <RelatToInsu>
                        <xsl:value-of select="$appnt/RelationToInsured"/>
                    </RelatToInsu>
                    <!--职业类别-->
                    <OccupationType>
                        <xsl:value-of select="$appnt/OccupationClass"/>
                    </OccupationType>
                    <!--职业代码-->
                    <OccupationCode>
                        <xsl:value-of select="$appnt/OccupationCode"/>
                    </OccupationCode>
                </LCAppnt>


                <!--LCAccountPojo-->
                <LCAccount>
                    <AccName>
                        <xsl:value-of select="$req/Beneficiaries/PayAccountName"/>
                    </AccName><!-- 账户姓名 -->
                    <BankAccNo>
                        <xsl:value-of select="$req/Beneficiaries/PayAcountNo"/>
                    </BankAccNo><!-- 续期缴费账号 -->
                    <AccKind>1</AccKind>
                    <BankCode>
                        <xsl:value-of select="$req/Beneficiaries/PayBankCode"/>
                    </BankCode>

                </LCAccount>

                <!--LCSpecPojo-->
                <LCSpecs>
                    <LCSpec>
                        <!-- 特别约定 -->
                        <SpecContent>
                            <!--<xsl:if test="$base/SpecArranged != '' and $base/SpecArranged = 'N'">
                                <xsl:value-of select="$base/SpecArranged"/>
                            </xsl:if>-->
                        </SpecContent>
                    </LCSpec>
                </LCSpecs>

                <!--LCAddressPojo-->
                <LCAddresses>
                    <!--投保人地址信息-->
                    <LCAddress>
                        <PostalAddress>
                            <xsl:value-of select="$appnt/AppAddress"/>
                        </PostalAddress>
                        <HomeAddress>
                            <xsl:value-of select="$appnt/AppAddress"/>
                        </HomeAddress>
                        <!-- 邮编 -->
                        <ZipCode>
                            <xsl:value-of select="$appnt/AppZipCode"/>
                        </ZipCode>

                        <HomeZipCode>
                            <xsl:value-of select="$appnt/AppZipCode"/>
                        </HomeZipCode>
                        <Mobile>
                            <xsl:value-of select="$appnt/AppMobile"/>
                        </Mobile>
                        <HomePhone>
                            <xsl:value-of select="$appnt/AppTelephone"/>
                        </HomePhone>
                        <Phone>
                            <xsl:value-of select="$appnt/AppTelephone"/>
                        </Phone>

                        <CompanyPhone>
                            <xsl:value-of select="$appnt/CompanyPhone"/>
                        </CompanyPhone>
                        <EMail>
                            <xsl:value-of select="$appnt/AppEmail"/>
                        </EMail>
                        <!-- 省 -->
                        <Province>
                            <xsl:value-of select="$appnt/Province"/>
                        </Province>
                        <!-- 市 -->
                        <City>
                            <xsl:value-of select="$appnt/City"/>
                        </City>
                        <!-- 区 -->
                        <County>
                            <xsl:value-of select="$appnt/Country"/>
                        </County>
                        <!-- <GrpName>
                             <xsl:value-of select="$appnt/Company"/>
                         </GrpName>-->
                    </LCAddress>
                    <!--被保人地址信息-->
                    <LCAddress>
                        <PostalAddress>
                            <xsl:value-of select="$Insured/InsuAddress"/>
                        </PostalAddress>
                        <HomeAddress>
                            <xsl:value-of select="$Insured/InsuAddress"/>
                        </HomeAddress>

                        <ZipCode>
                            <xsl:value-of select="$Insured/InsuZipCode"/>
                        </ZipCode>
                        <!-- <HomeZipCode>
                             <xsl:value-of select="$Insured/ZipCode"/>
                         </HomeZipCode>-->
                        <Mobile>
                            <xsl:value-of select="$Insured/InsuMobile"/>
                        </Mobile>
                        <Phone>
                            <xsl:value-of select="$Insured/InsuTelephone"/>
                        </Phone>
                        <!--<HomePhone>
                            <xsl:value-of select="$Insured/Phone"/>
                        </HomePhone>-->
                        <!--<Phone>
                            <xsl:value-of select="$Insured/Phone"/>
                        </Phone>-->
                        <EMail>
                            <xsl:value-of select="$Insured/InsuEmail"/>
                        </EMail>
                        <!-- 省 -->
                        <Province>
                            <xsl:value-of select="$Insured/InsuProvince"/>
                        </Province>
                        <!-- 市 -->
                        <City>
                            <xsl:value-of select="$Insured/InsuCity"/>
                        </City>
                        <!-- 区 -->
                        <County>
                            <xsl:value-of select="$Insured/InsuCountry"/>
                        </County>
                        <!--<GrpName>
                            <xsl:value-of select="$Insured/Company"/>
                        </GrpName>-->
                    </LCAddress>
                    <xsl:if test="$bnfs/Count != 0">
                        <xsl:if test="$bnfs/Name1 != '' and exists(RequestNodes/RequestNode/Bnfs/Name1)">
                            <xsl:call-template name="addLCAddress">
                                <xsl:with-param name="count">
                                    <xsl:value-of select="$bnfs/Count"/>
                                </xsl:with-param>
                            </xsl:call-template>
                        </xsl:if>
                    </xsl:if>
                </LCAddresses>

                <!--被保人-->
                <LCInsureds>
                    <LCInsured>
                        <Name>
                            <xsl:value-of select="$Insured/InsuName"/>
                        </Name>
                        <Sex>
                            <xsl:value-of select="$Insured/InsuSex"/>
                        </Sex>
                        <Birthday>
                            <xsl:value-of
                                    select="$Insured/InsuBirthday"/>
                        </Birthday>
                        <IDType>
                            <xsl:value-of select="$Insured/InsuIDType"/>
                        </IDType>
                        <IDNo>
                            <xsl:value-of select="$Insured/InsuIDNo"/>
                        </IDNo>
                        <IdValiDate>
                            <xsl:value-of select="$Insured/IdexpDate"/>
                        </IdValiDate><!-- 被保人证件有效期 -->
                        <NativePlace>
                            <xsl:value-of select="$Insured/NativePlace"/>
                        </NativePlace>

                        <Degree>
                            <xsl:value-of select="$Insured/Degree"/>
                        </Degree>
                        <!--投被保人关系-->
                        <RelationToAppnt>
                            <xsl:value-of select="$Insured/InsuRelationToApp"/>
                        </RelationToAppnt>
                        <!--被保人与主被保人关系-->
                        <RelationToMainInsured>
                            <xsl:value-of select="$Insured/InsuRelationToMainInsu"/>
                        </RelationToMainInsured>
                        <!--主投保人标识-->
                        <!-- <IsMainInsured>
                             <xsl:value-of select="$Insured/IsMainInsured"/>
                         </IsMainInsured>-->
                        <!-- <SequenceNo>1</SequenceNo>-->
                        <!--被保人身高-->
                        <Stature>
                            <xsl:value-of select="$Insured/Stature"/>
                        </Stature>
                        <!--被保人体重-->
                        <Avoirdupois>
                            <xsl:value-of select="$Insured/Weight"/>
                        </Avoirdupois>
                        <!--年收入-->
                        <Salary>
                            <xsl:value-of select="$Insured/Salary"/>
                        </Salary>


                        <!--健康状况-->
                        <Health>
                            <xsl:value-of select="$Insured/Health"/>
                        </Health>

                        <SmokeFlag>
                            <xsl:value-of select="$Insured/SmokeFlag"/>
                        </SmokeFlag>
                        <!--婚姻状况-->
                        <!-- <Marriage>
                             <xsl:value-of select="$Insured/Marriage"/>
                         </Marriage>-->
                        <!--社保标记SSFlag-->
                        <SSFlag>
                            <xsl:value-of select="$Insured/SocialInsuFlag"/>
                        </SSFlag>
                        <!-- 新增-->
                        <ContPlanCode>
                            <xsl:value-of
                                    select="$req/ProductCode"/>
                        </ContPlanCode>
                        <PrtNo>
                            <!--投保单号 -->
                            <xsl:value-of select="$req/proposalNo"/>
                        </PrtNo>
                        <!--职业类别-->
                        <OccupationType>
                            <xsl:value-of select="$Insured/OccupationClass"/>
                        </OccupationType>
                        <!--职业代码-->
                        <OccupationCode>
                            <xsl:value-of select="$Insured/OccupationCode"/>
                        </OccupationCode>
                    </LCInsured>
                </LCInsureds>

                <!--受益人Beneficiaries 法定受益人不传递受益人信息-->
                <LCBnfs>
                    <xsl:for-each select="RequestNodes/RequestNode/Beneficiaries/Beneficiary">
                        <LCBnf>
                            <BnfNo>
                                <xsl:value-of select="BeneficiaryNo"/>
                            </BnfNo>
                            <BnfGrade>
                                <xsl:value-of select="BeneficiaryGrade"/>
                            </BnfGrade>
                            <!--受益人顺序 BeneficiaryOrder-->
                            <BeneficiaryOrder>
                                <xsl:value-of select="BeneficiaryOrder"/>
                            </BeneficiaryOrder>
                            <!--收益人份额-->

                            <BnfLot>
                                <xsl:value-of select="InterestPercent"/>
                            </BnfLot>
                            <BnfType>
                                <xsl:value-of select="BeneficiaryType"/>
                            </BnfType>


                            <Name>
                                <xsl:value-of select="BeneficiaryName"/>
                            </Name>
                            <Sex>
                                <xsl:value-of select="BeneficiarySex"/>
                            </Sex>
                            <Birthday>
                                <xsl:value-of
                                        select="BeneficiaryBirthday"/>
                            </Birthday>

                            <IDType>
                                <xsl:value-of select="BeneficiaryIDType"/>
                            </IDType>
                            <IDNo>
                                <xsl:value-of select="BeneficiaryIDNo"/>
                            </IDNo>
                            <IdValiDate>
                                <xsl:value-of select="IDExpDate"/>
                            </IdValiDate>
                            <!-- 受益人证件有效期 -->

                            <RelationToInsured>
                                <xsl:value-of select="RelationToInsured"/>
                            </RelationToInsured>


                            <Tel>
                                <xsl:value-of select="BeneficiaryMobile"/>
                            </Tel>
                            <ZipCode>
                                <xsl:value-of select="BeneficiaryZipCode"/>
                            </ZipCode>
                            <Address>
                                <xsl:value-of select="BeneficiaryAddress"/>
                            </Address>
                        </LCBnf>
                    </xsl:for-each>
                </LCBnfs>
                <!--最初始险种代码-->
                <RiskCodeWr>
                    <xsl:value-of select="$policyLiability/RiskCode"/>
                </RiskCodeWr>
                <!--险种-->
                <LCPols>
                    <LCPol>

                        <!-- 保单号ContNo-->
                        <ContNo>
                            <xsl:value-of select="$req/ContNo"/>
                        </ContNo>
                        <xsl:variable name="varRiskCode">
                            <xsl:call-template name="tran_riskCode">
                                <!--<xsl:with-param name="riskCode"
                                                select="java:com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.checkRiskCode($risks/RiskCode)"/>-->
                                <xsl:with-param name="riskCode" select="$policyLiability/RiskCode"/>
                            </xsl:call-template>
                        </xsl:variable>
                        <!-- 险种代码 -->
                        <RiskCode>
                            <xsl:value-of select="$varRiskCode"/>
                        </RiskCode>

                        <!--主附险标识-->
                        <MainPolNo>
                            <xsl:value-of select="$policyLiability/MainRiskFlag"/>
                        </MainPolNo>
                        <!-- 主险代码 -->
                        <!-- <MainPolNo>
                             <xsl:value-of select="$varRiskCode"/>
                         </MainPolNo>-->
                        <!-- 保额 -->
                        <Amnt>
                            <xsl:value-of select="$policyLiability/RiskAmount"/>
                        </Amnt>
                        <!-- 保费 -->
                        <Prem>
                            <xsl:value-of select="$policyLiability/RiskPremium"/>
                        </Prem>
                        <!--
                                                &lt;!&ndash;首期保费FirstPremium&ndash;&gt;
                                                <FirstPremium>
                                                    <xsl:value-of select="$policyLiability/FirstPremium"/>
                                                </FirstPremium>
                                                &lt;!&ndash; 首期保额InitialPremAmt&ndash;&gt;
                                                <InitialPremAmt>
                                                    <xsl:value-of select="$policyLiability/InitialPremAmt"/>
                                                </InitialPremAmt>-->


                        <PayIntv><!-- 缴费间隔 -->
                            <xsl:value-of select="$policyLiability/PayIntv"/>
                        </PayIntv>
                        <!--&lt;!&ndash;直接取报文值&ndash;&gt;
                        <PayMode>
                            <xsl:value-of select="$policyLiability/PayMode"/>
                        </PayMode>-->

                        <!-- 保险年期类型 -->
                        <InsuYearFlag>
                            <xsl:value-of select="$policyLiability/InsuYearFlag"/>
                        </InsuYearFlag>
                        <!--保险期间 -->
                        <InsuYear>
                            <xsl:value-of select="$policyLiability/InsuYear"/>
                        </InsuYear>
                        <PayEndYearFlag>
                            <xsl:value-of select="$policyLiability/PayEndYearFlag"/>
                        </PayEndYearFlag>


                        <!--免赔额-->
                        <!--  <NoPayPrem>
                              <xsl:value-of select="$policyLiability/Deductibles"/>
                          </NoPayPrem>
                          &lt;!&ndash;赔付比例&ndash;&gt;
                          <NoPayPortion>
                              <xsl:value-of select="$policyLiability/Lossratio"/>
                          </NoPayPortion>-->


                        <!--<xsl:if test="$risks/PayType = 1">
                            <PayYears>1000</PayYears>
                        </xsl:if>
                        <xsl:if test="$risks/PayType != 1">
                            <PayYears>
                                <xsl:value-of select="$risks/PayDueDate"/>
                            </PayYears>
                        </xsl:if>-->
                        <!-- 缴费期间 PayEndYear-->
                        <PayEndYear>
                            <xsl:value-of select="$policyLiability/PayEndYear"/>
                        </PayEndYear>


                        <!-- 自动续保标志  <RnewFlag><xsl:value-of select="$req/ExpireProcessMode"/></RnewFlag>-->
                        <RnewFlag>
                            <xsl:call-template name="tran_renewflag">
                                <xsl:with-param name="flag" select="RequestNodes/RequestNode/ExpireProcessMode"/>
                            </xsl:call-template>
                        </RnewFlag>
                        <!-- 自动垫交标志 -->
                        <!--<xsl:if test="$risks/AutoPayForFlag = ''">
                            <AutoPayFlag>0</AutoPayFlag>
                        </xsl:if>
                        <xsl:if test="$risks/AutoPayForFlag != ''">
                            <AutoPayFlag>
                                <xsl:value-of select="$risks/AutoPayForFlag"/>
                            </AutoPayFlag>
                        </xsl:if>-->
                        <!-- 保险止期   InsuranceEndPeriod-->

                        <!-- 领取年期 -->
                        <!--<GetYear>
                            <xsl:value-of select="$risks/GetYear"/>
                        </GetYear>-->
                        <!-- 年金/生存金领取年期类型: -->
                        <!--<GetYearFlag>
                            <xsl:call-template name="tran_getyearflag">
                                <xsl:with-param name="getyearflag">
                                    <xsl:value-of select="$risks/GetYearFlag"/>
                                </xsl:with-param>
                            </xsl:call-template>
                        </GetYearFlag>-->

                        <!-- <LiveGetMode>
                             <xsl:call-template name="tran_livegetmode">
                                 <xsl:with-param name="livegetmode">
                                     <xsl:if test="$flag = 01 or $flag = 02 or $flag = 04 or $flag = 22">
                                         <xsl:choose>
                                             <xsl:when
                                                     test="string($varRiskCode) = '5022' or string($varRiskCode) = 'C066' or string($varRiskCode) = 'C067' or string($varRiskCode) = 'C068' or string($varRiskCode) = 'C069' or string($varRiskCode) = 'C072'">
                                                 2
                                             </xsl:when>
                                             &lt;!&ndash; 1累计生息，2现金领取 &ndash;&gt;
                                             <xsl:when test="string($varRiskCode) = '2041'">
                                                 1
                                             </xsl:when>
                                             <xsl:when test="string($varRiskCode) = '1029'">2</xsl:when>
                                             <xsl:when test="string($varRiskCode) = '2039'">
                                                 <xsl:if test="$risks/FullBonusGetMode ='0'">
                                                     1
                                                 </xsl:if>
                                                 <xsl:if test="$risks/FullBonusGetMode ='1'">
                                                     2
                                                 </xsl:if>
                                                 <xsl:if test="$risks/FullBonusGetMode ='4'">
                                                     2
                                                 </xsl:if>
                                             </xsl:when>
                                             <xsl:when test="string($varRiskCode) = '2048'">2</xsl:when>
                                             <xsl:when test="string($varRiskCode) = '2052'">2</xsl:when>
                                             <xsl:when test="string($varRiskCode) = '2053'">2</xsl:when>
                                             <xsl:when test="string($varRiskCode) = '1044'">2</xsl:when>
                                             <xsl:otherwise>
                                                 <xsl:value-of select="''"/>
                                             </xsl:otherwise>
                                         </xsl:choose>
                                     </xsl:if>
                                     <xsl:if test="$flag = 11 or $flag = 08">
                                         <xsl:choose>
                                             <xsl:when
                                                     test="string($varRiskCode) = '5022' or string($varRiskCode) = 'C066' or string($varRiskCode) = 'C067' or string($varRiskCode) = 'C068' or string($varRiskCode) = 'C069' or string($varRiskCode) = 'C072'">
                                                 2
                                             </xsl:when>
                                             &lt;!&ndash; 1累计生息，2现金领取 &ndash;&gt;
                                             <xsl:when test="string($varRiskCode) = '2041'">
                                                 1
                                             </xsl:when>
                                             <xsl:when test="string($varRiskCode) = '2039'">
                                                 <xsl:if test="$risks/FullBonusGetMode ='0'">
                                                     1
                                                 </xsl:if>
                                                 <xsl:if test="$risks/FullBonusGetMode ='1'">
                                                     2
                                                 </xsl:if>
                                                 <xsl:if test="$risks/FullBonusGetMode ='4'">
                                                     2
                                                 </xsl:if>
                                             </xsl:when>
                                             <xsl:when test="string($varRiskCode) = '2048'">2</xsl:when>
                                             <xsl:when test="string($varRiskCode) = '2052'">2</xsl:when>
                                             <xsl:when test="string($varRiskCode) = '2053'">2</xsl:when>
                                             <xsl:when test="string($varRiskCode) = '1044'">2</xsl:when>
                                             <xsl:otherwise>
                                                 <xsl:value-of select="''"/>
                                             </xsl:otherwise>
                                         </xsl:choose>
                                     </xsl:if>
                                 </xsl:with-param>
                             </xsl:call-template>
                         </LiveGetMode>-->
                        <!--  <LiveAccFlag>
                              <xsl:call-template name="tran_liveaccflag">
                                  <xsl:with-param name="liveaccflag">
                                      <xsl:if test="$flag = 01 or $flag = 02 or $flag = 04 or $flag = 22 ">
                                          <xsl:choose>
                                              &lt;!&ndash; 1累计生息，2现金领取 &ndash;&gt;
                                              <xsl:when test="string($varRiskCode) = '2041'">
                                                  0
                                              </xsl:when>
                                              <xsl:when test="string($varRiskCode) = '1029'">2</xsl:when>
                                              <xsl:when test="string($varRiskCode) = '2039'">
                                                  <xsl:if test="$risks/FullBonusGetMode ='0'">
                                                      0
                                                  </xsl:if>
                                                  <xsl:if test="$risks/FullBonusGetMode ='1'">
                                                      2
                                                  </xsl:if>
                                                  <xsl:if test="$risks/FullBonusGetMode ='4'">
                                                      2
                                                  </xsl:if>
                                              </xsl:when>
                                              <xsl:otherwise>
                                                  <xsl:value-of select="''"/>
                                              </xsl:otherwise>
                                          </xsl:choose>
                                      </xsl:if>
                                      <xsl:if test="$flag = 11 or $flag = 08">
                                          <xsl:choose>
                                              &lt;!&ndash; 1累计生息，2现金领取 &ndash;&gt;
                                              <xsl:when test="string($varRiskCode) = '2041'">
                                                  0
                                              </xsl:when>
                                              <xsl:when test="string($varRiskCode) = '2039'">
                                                  <xsl:if test="$risks/FullBonusGetMode ='0'">
                                                      0
                                                  </xsl:if>
                                                  <xsl:if test="$risks/FullBonusGetMode ='1'">
                                                      2
                                                  </xsl:if>
                                                  <xsl:if test="$risks/FullBonusGetMode ='4'">
                                                      2
                                                  </xsl:if>
                                              </xsl:when>
                                              <xsl:otherwise>
                                                  <xsl:value-of select="''"/>
                                              </xsl:otherwise>
                                          </xsl:choose>
                                      </xsl:if>
                                  </xsl:with-param>
                              </xsl:call-template>
                          </LiveAccFlag>-->


                        <!--<SaleChnl>3</SaleChnl>
                        <BonusMan>0</BonusMan>
                        <SpecifyValiDate>N</SpecifyValiDate>
                        <AgentType>08</AgentType>-->
                        <!--投保日期-->
                        <CValiDate>
                            <xsl:value-of select="$policyLiability/InsuranceStartPeriod"/>
                        </CValiDate>
                        <EndDate>
                            <xsl:value-of select="$policyLiability/InsuranceEndPeriod"/>
                        </EndDate>

                        <InsuredSex>
                            <!--111-->
                            <xsl:value-of select="$Insured/InsuSex"/>
                        </InsuredSex>
                        <InsuredName>
                            <!--111-->
                            <xsl:value-of select="$Insured/InsuName"/>
                        </InsuredName>
                        <!--<PolApplyDate>
                            &lt;!&ndash;投保单申请日期&ndash;&gt;
                            &lt;!&ndash;111&ndash;&gt;
                            <xsl:value-of
                                    select="java:com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.dateFormat8To10($base/ApplyDate)"/>
                        </PolApplyDate>-->
                        <!--自动续保年龄-->
                        <!--<AutoRnewAge>
                            <xsl:call-template name="tran_autoRenewAge">
                                <xsl:with-param name="autoRenewAge" select="$base/AutoRenewAge"/>
                            </xsl:call-template>
                        </AutoRnewAge>-->
                        <!-- <xsl:if test="$Insured/ImmaturityFlag = 1">
                             &lt;!&ndash;111&ndash;&gt;
                             <OtherAmnt>
                                 <xsl:value-of select="$Insured/Immaturity"/>
                             </OtherAmnt>
                         </xsl:if>-->
                    </LCPol>
                    <!--<xsl:if test="$addt/Count != 0">
                        <xsl:call-template name="addLCPol">
                            <xsl:with-param name="count">
                                <xsl:value-of select="$addt/Count"/>
                            </xsl:with-param>
                        </xsl:call-template>
                    </xsl:if>
                </xsl:otherwise>
            </xsl:choose>-->
                </LCPols>
                <!--责任-->
                <LCDutys>
                    <LCDuty>
                        <DutyCode>
                            <xsl:value-of select="$DutyInfo/DutyCode"/>
                        </DutyCode>

                        <CalRule>
                            <xsl:value-of select="$DutyInfo/CalMode"/>
                        </CalRule>
                        <Prem>
                            <xsl:value-of select="$DutyInfo/DutyPrem"/>
                        </Prem>
                        <Amnt>
                            <xsl:value-of select="$DutyInfo/DutyAmnt"/>
                        </Amnt>
                        <InsuYearFlag>
                            <xsl:value-of select="$DutyInfo/InsuYearFlag"/>
                        </InsuYearFlag>
                        <InsuYear>
                            <xsl:value-of select="$DutyInfo/InsuYear"/>
                        </InsuYear>
                        <PayEndYearFlag>
                            <xsl:value-of select="$DutyInfo/PayEndYearFlag"/>
                        </PayEndYearFlag>
                        <PayEndYear>
                            <xsl:value-of select="$DutyInfo/PayEndYear"/>
                        </PayEndYear>
                        <PayIntv>
                            <xsl:value-of select="$DutyInfo/PayIntv"/>
                        </PayIntv>

                    </LCDuty>
                </LCDutys>


                <!--健康告知-->
                <LCCustomerImparts>
                    <!--被保人健康告知-->
                    <xsl:for-each select="RequestNodes/RequestNode/Insureds/Insured/InsuImpartInfo/InsuImpart">
                        <LCCustomerImpart>
                            <ImpartCode>
                                <xsl:value-of select="ImpartCode"/>
                            </ImpartCode>
                            <ImpartVer>
                                <xsl:value-of select="ImpartVer"/>
                            </ImpartVer>

                            <ImpartContent>
                                <xsl:value-of select="ImpartContent"/>
                            </ImpartContent>
                            <ImpartParamModle>
                                <xsl:value-of select="Impartparammodle"/>
                            </ImpartParamModle>
                            <CustomerNoType>1</CustomerNoType>
                            <PatchNo>0</PatchNo>
                        </LCCustomerImpart>
                    </xsl:for-each>
                </LCCustomerImparts>
                <!--添加紧急联系人表-->
                <LCAppntLinkManInfo>
                    <Name>
                        <xsl:value-of select="$req/Reserve10"/>
                    </Name>
                    <Tel1>
                        <xsl:value-of select="$req/Reserve12"/>
                    </Tel1>
                    <!--信函发送形式1.电子2.纸质-->
                    <LetterSendMode>
                        <xsl:value-of select="$req/Reserve14"/>
                    </LetterSendMode>
                    <PrtNo>
                        <xsl:value-of select="$req/ProposalNo"/>
                    </PrtNo>
                    <Email>
                        <xsl:value-of select="$appnt/AppEmail"/>
                    </Email>
                </LCAppntLinkManInfo>
                <!--以下仅有对象，没有值，但是微服务需要-->
                <LDPersons/>
                <!--<LCDutys/>-->
                <LCGets/>
                <LCPrems/>
                <LCCustomerImpartParamses/>
                <LCCustomerImpartDetails/>
            </Body>
        </TranData>
    </xsl:template>


    <!--多年期标志-->
    <!--<xsl:template name="tran_moreYearFlag">
        <xsl:param name="yearFlag">1</xsl:param>
        <xsl:if test="$yearFlag = 1">1</xsl:if>&lt;!&ndash;  传入的 1为   多年期预交&ndash;&gt;
        <xsl:if test="$yearFlag = 2">2</xsl:if>&lt;!&ndash;  传入的 2为   多年期续保&ndash;&gt;
        <xsl:if test="$yearFlag = 3">3</xsl:if>&lt;!&ndash;  传入的 3为  3非多年期&ndash;&gt;
        <xsl:if test="$yearFlag = ''">3</xsl:if>
    </xsl:template>
    <xsl:template name="mainRiskLCPOL">
        <LCPol>
            <xsl:variable name="mainRiskCode">
                <xsl:call-template name="tran_mainriskCode">
                    <xsl:with-param name="riskCode"
                                    select="java:com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.checkRiskCode(RequestNodes/RequestNode/Risks/RiskCode)"/>
                    &lt;!&ndash;<xsl:with-param name="riskCode" select="$risks/RiskCode"/>&ndash;&gt;
                </xsl:call-template>
            </xsl:variable>
            <xsl:variable name="varRiskCodess">
                <xsl:call-template name="tran_riskCode">
                    <xsl:with-param name="riskCode"
                                    select="java:com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.checkRiskCode(RequestNodes/RequestNode/Risks/RiskCode)"/>
                    &lt;!&ndash;<xsl:with-param name="riskCode" select="$risks/RiskCode"/>&ndash;&gt;
                </xsl:call-template>
            </xsl:variable>
            <RiskCode>&lt;!&ndash; 险种代码 &ndash;&gt;
                <xsl:value-of select="$mainRiskCode"/>
            </RiskCode>
            <MainPolNo>&lt;!&ndash; 主险代码 &ndash;&gt;
                <xsl:value-of select="$varRiskCodess"/>
            </MainPolNo>
            <Amnt>&lt;!&ndash; 保额 &ndash;&gt;
                <xsl:choose>
                    <xsl:when test="Header/EntrustWay = 01">
                        <xsl:call-template name="tran_amnt">
                            <xsl:with-param name="riskCode" select="$mainRiskCode"/>
                            <xsl:with-param name="amnt" select="$req/InsuredAmount"/>
                        </xsl:call-template>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="$req/InsuredAmount"/>
                    </xsl:otherwise>
                </xsl:choose>
            </Amnt>
            <Prem>&lt;!&ndash; 保费 &ndash;&gt;
                <xsl:value-of select="$req/Premium"/>
            </Prem>
            <Mult>&lt;!&ndash; 份数 &ndash;&gt;
                <xsl:value-of select="RequestNodes/RequestNode/Risks/Share"/>
            </Mult>
            &lt;!&ndash;总保费 总保额&ndash;&gt;
            <GrossPremium>
                <xsl:value-of select="RequestNodes/RequestNode/Risks/Share"/>
            </GrossPremium>
            <PayIntv>&lt;!&ndash; 缴费间隔 &ndash;&gt;
                <xsl:call-template name="tran_Contpayintv">
                    <xsl:with-param name="payintv">
                        <xsl:value-of select="RequestNodes/RequestNode/Risks/PayType"/>
                    </xsl:with-param>
                </xsl:call-template>
            </PayIntv>
            <xsl:if test="Header/EntrustWay = 01">
                <PayMode>C</PayMode>
            </xsl:if>
            <xsl:if test="Header/EntrustWay = 04">
                <PayMode>C</PayMode>
            </xsl:if>
            <xsl:if test="Header/EntrustWay= 11">
                <PayMode>A</PayMode>
            </xsl:if>
            <xsl:if test="Header/EntrustWay = 02">
                <PayMode>C</PayMode>
            </xsl:if>
            <xsl:if test="Header/EntrustWay = 22">
                <PayMode>A</PayMode>
            </xsl:if>
            <xsl:if test="Header/EntrustWay= 08">
                <PayMode>C</PayMode>
            </xsl:if>
            <InsuYearFlag>&lt;!&ndash; 保险年期类型 &ndash;&gt;
                <xsl:call-template name="tran_insuYearFlag">
                    <xsl:with-param name="yearFlag" select="RequestNodes/RequestNode/Risks/InsuDueType"/>
                </xsl:call-template>
            </InsuYearFlag>
            <InsuYear>
                <xsl:call-template name="tran_InsuYear">
                    <xsl:with-param name="insuYear" select="RequestNodes/RequestNode/Risks/InsuDueDate"/>
                </xsl:call-template>
            </InsuYear>
            <xsl:if test="RequestNodes/RequestNode/Risks/PayType = 1">
                <PayYears>1000</PayYears>
            </xsl:if>
            <xsl:if test="RequestNodes/RequestNode/Risks/PayType != 1">
                <PayYears>
                    <xsl:value-of select="RequestNodes/RequestNode/Risks/PayDueDate"/>
                </PayYears>
            </xsl:if>
            <xsl:if test="RequestNodes/RequestNode/Risks/PayType = 1">
                <PayEndYear>1000</PayEndYear>
                <PayEndYearFlag>Y</PayEndYearFlag>
            </xsl:if>
            <xsl:if test="RequestNodes/RequestNode/Risks/PayType != 1">
                <PayEndYear>
                    <xsl:value-of select="RequestNodes/RequestNode/Risks/PayDueDate"/>
                </PayEndYear>
                <PayEndYearFlag>
                    <xsl:if test="RequestNodes/RequestNode/Risks/PayDueType != ''">
                        <xsl:call-template name="tran_payendYearFlag">
                            <xsl:with-param name="yearFlag" select="RequestNodes/RequestNode/Risks/PayDueType"/>
                        </xsl:call-template>
                    </xsl:if>
                    <xsl:if test="RequestNodes/RequestNode/Risks/PayDueType = ''">Y</xsl:if>
                </PayEndYearFlag>
            </xsl:if>
            <BonusGetMode>&lt;!&ndash; 红利领取方式 &ndash;&gt;
                <xsl:choose>
                    <xsl:when test="string($mainRiskCode) = '2048'">1</xsl:when>
                    <xsl:when
                            test="string($mainRiskCode) = '2052' and Header/EntrustWay='11' and RequestNodes/RequestNode/Risks/BonusGetMode !='' and exists(RequestNodes/RequestNode/Risks/BonusGetMode)">
                        <xsl:call-template name="tran_getMode">
                            <xsl:with-param name="bonusGetMode" select="RequestNodes/RequestNode/Risks/BonusGetMode"/>
                        </xsl:call-template>
                    </xsl:when>
                    <xsl:when test="string($mainRiskCode) = '2052'">1</xsl:when>
                    <xsl:when
                            test="string($mainRiskCode) = '2053' and Header/EntrustWay='11' and RequestNodes/RequestNode/Risks/BonusGetMode !='' and exists(RequestNodes/RequestNode/Risks/BonusGetMode)">
                        <xsl:call-template name="tran_getMode">
                            <xsl:with-param name="bonusGetMode" select="RequestNodes/RequestNode/Risks/BonusGetMode"/>
                        </xsl:call-template>
                    </xsl:when>
                    <xsl:when test="string($mainRiskCode) = '2053'">1</xsl:when>
                    <xsl:otherwise>
                        <xsl:if test="RequestNodes/RequestNode/Risks/BonusGetMode!=''">
                            <xsl:call-template name="tran_getMode">
                                <xsl:with-param name="bonusGetMode"
                                                select="RequestNodes/RequestNode/Risks/BonusGetMode"/>
                            </xsl:call-template>
                        </xsl:if>
                    </xsl:otherwise>
                </xsl:choose>
            </BonusGetMode>
            <GetForm>&lt;!&ndash; 年金领取方式 /保险金领取方式&ndash;&gt;
                <xsl:call-template name="tran_getIntv">
                    <xsl:with-param name="riskCode">
                        <xsl:value-of select="$mainRiskCode"/>
                    </xsl:with-param>
                    <xsl:with-param name="getIntv">
                        <xsl:value-of select="RequestNodes/RequestNode/Risks/FullBonusGetMode"/>
                    </xsl:with-param>
                </xsl:call-template>
            </GetForm>
            <xsl:if test="Header/EntrustWay = 04 or Header/EntrustWay = 02 or Header/EntrustWay = 01 or Header/EntrustWay = 22 or Header/EntrustWay = 08">
                <OccupationType>
                    <xsl:call-template name="tran_occupationType">
                        <xsl:with-param name="occupationType" select="RequestNodes/RequestNode/Insu/JobType"/>
                        <xsl:with-param name="riskcode" select="$varRiskCodess"/>
                    </xsl:call-template>
                </OccupationType>
            </xsl:if>
            <xsl:if test="Header/EntrustWay = 11">
                <OccupationType>
                    <xsl:value-of select="RequestNodes/RequestNode/Insu/JobType"/>
                </OccupationType>
            </xsl:if>
            &lt;!&ndash;<xsl:if test="Header/EntrustWay = 11 ">&ndash;&gt;
            &lt;!&ndash;暂定 6807 全转成05&ndash;&gt;
            <xsl:if test="Header/EntrustWay = 11">
                <GetBankCode>05</GetBankCode>
            </xsl:if>
            <xsl:if test="Header/EntrustWay != 11">
                <GetBankCode>0501</GetBankCode>
            </xsl:if>
            <RnewFlag>
                <xsl:call-template name="tran_renew">
                    <xsl:with-param name="flag" select="RequestNodes/RequestNode/Risks/AutoPayFlag"/>
                </xsl:call-template>
            </RnewFlag>&lt;!&ndash; 自动续保标志 &ndash;&gt;
            <xsl:if test="RequestNodes/RequestNode/Risks/AutoPayForFlag = ''">
                <AutoPayFlag>0</AutoPayFlag>&lt;!&ndash; 自动垫交标志 &ndash;&gt;
            </xsl:if>
            <xsl:if test="RequestNodes/RequestNode/Risks/AutoPayForFlag != ''">
                <AutoPayFlag>
                    <xsl:value-of select="RequestNodes/RequestNode/Risks/AutoPayForFlag"/>
                </AutoPayFlag>&lt;!&ndash; 自动垫交标志 &ndash;&gt;
            </xsl:if>
            <EndDate>
                <xsl:value-of
                        select="java:com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.dateFormat8To10(RequestNodes/RequestNode/Risks/RiskEndDate)"/>
            </EndDate>&lt;!&ndash; 保险止期 &ndash;&gt;
            <GetYear>
                <xsl:value-of select="RequestNodes/RequestNode/Risks/GetYear"/>
            </GetYear>&lt;!&ndash; 领取年期 &ndash;&gt;
            <GetYearFlag>
                <xsl:call-template name="tran_getyearflag">
                    <xsl:with-param name="getyearflag">
                        <xsl:value-of select="RequestNodes/RequestNode/Risks/GetYearFlag"/>
                    </xsl:with-param>
                </xsl:call-template>
            </GetYearFlag>&lt;!&ndash; 年金/生存金领取年期类型: &ndash;&gt;

            <LiveGetMode>
                <xsl:call-template name="tran_livegetmode">
                    <xsl:with-param name="livegetmode">
                        <xsl:if test="Header/EntrustWay= 01 or Header/EntrustWay= 02 or Header/EntrustWay= 04 or Header/EntrustWay = 22">
                            <xsl:choose>
                                <xsl:when
                                        test="string($mainRiskCode) = '5022' or string($varRiskCodess) = 'C066' or string($varRiskCodess) = 'C067' or string($varRiskCodess) = 'C068' or string($varRiskCodess) = 'C069' or string($varRiskCodess) = 'C072'">
                                    2
                                </xsl:when>
                                &lt;!&ndash; 1累计生息，2现金领取 &ndash;&gt;
                                <xsl:when test="string($mainRiskCode) = '2041'">
                                    1
                                </xsl:when>
                                <xsl:when test="string($varRiskCodess) = '1029'">2</xsl:when>
                                <xsl:when test="string($varRiskCodess) = '2039'">
                                    <xsl:if test="RequestNodes/RequestNode/Risks/FullBonusGetMode ='0'">
                                        1
                                    </xsl:if>
                                    <xsl:if test="RequestNodes/RequestNode/Risks/FullBonusGetMode ='1'">
                                        2
                                    </xsl:if>
                                    <xsl:if test="RequestNodes/RequestNode/Risks/FullBonusGetMode ='4'">
                                        2
                                    </xsl:if>
                                </xsl:when>
                                <xsl:when test="string($varRiskCodess) = '2048'">2</xsl:when>
                                <xsl:when test="string($varRiskCodess) = '2052'">2</xsl:when>
                                <xsl:when test="string($varRiskCodess) = '2053'">2</xsl:when>
                                <xsl:otherwise>
                                    <xsl:value-of select="''"/>
                                </xsl:otherwise>
                            </xsl:choose>
                        </xsl:if>
                        <xsl:if test="Header/EntrustWay = 11 or Header/EntrustWay = 08">
                            <xsl:choose>
                                <xsl:when
                                        test="string($varRiskCodess) = '5022' or string($varRiskCodess) = 'C066' or string($varRiskCodess) = 'C067' or string($varRiskCodess) = 'C068' or string($varRiskCodess) = 'C069' or string($varRiskCodess) = 'C072'">
                                    2
                                </xsl:when>
                                &lt;!&ndash; 1累计生息，2现金领取 &ndash;&gt;
                                <xsl:when test="string($varRiskCodess) = '2041'">
                                    1
                                </xsl:when>
                                <xsl:when test="string($varRiskCodess) = '2039'">
                                    <xsl:if test="RequestNodes/RequestNode/Risks/FullBonusGetMode ='0'">
                                        1
                                    </xsl:if>
                                    <xsl:if test="RequestNodes/RequestNode/Risks/FullBonusGetMode ='1'">
                                        2
                                    </xsl:if>
                                    <xsl:if test="RequestNodes/RequestNode/Risks/FullBonusGetMode ='4'">
                                        2
                                    </xsl:if>
                                </xsl:when>
                                <xsl:when test="string($varRiskCodess) = '2048'">2</xsl:when>
                                <xsl:when test="string($varRiskCodess) = '2052'">2</xsl:when>
                                <xsl:when test="string($varRiskCodess) = '2053'">2</xsl:when>
                                <xsl:otherwise>
                                    <xsl:value-of select="''"/>
                                </xsl:otherwise>
                            </xsl:choose>
                        </xsl:if>
                    </xsl:with-param>
                </xsl:call-template>
            </LiveGetMode>
            <LiveAccFlag>
                <xsl:call-template name="tran_liveaccflag">
                    <xsl:with-param name="liveaccflag">
                        <xsl:if test="Header/EntrustWay = 01 or Header/EntrustWay = 02 or Header/EntrustWay = 04 or Header/EntrustWay = 22 ">
                            <xsl:choose>
                                &lt;!&ndash; 1累计生息，2现金领取 &ndash;&gt;
                                <xsl:when test="string($varRiskCodess) = '2041'">
                                    0
                                </xsl:when>
                                <xsl:when test="string($varRiskCodess) = '1029'">2</xsl:when>
                                <xsl:when test="string($varRiskCodess) = '2039'">
                                    <xsl:if test="RequestNodes/RequestNode/Risks/FullBonusGetMode ='0'">
                                        0
                                    </xsl:if>
                                    <xsl:if test="RequestNodes/RequestNode/Risks/FullBonusGetMode ='1'">
                                        2
                                    </xsl:if>
                                    <xsl:if test="RequestNodes/RequestNode/Risks/FullBonusGetMode ='4'">
                                        2
                                    </xsl:if>
                                </xsl:when>
                                <xsl:otherwise>
                                    <xsl:value-of select="''"/>
                                </xsl:otherwise>
                            </xsl:choose>
                        </xsl:if>
                        <xsl:if test="Header/EntrustWay = 11 or Header/EntrustWay = 08">
                            <xsl:choose>
                                &lt;!&ndash; 1累计生息，2现金领取 &ndash;&gt;
                                <xsl:when test="string($varRiskCodess) = '2041'">
                                    0
                                </xsl:when>
                                <xsl:when test="string($varRiskCodess) = '2039'">
                                    <xsl:if test="RequestNodes/RequestNode/Risks/FullBonusGetMode ='0'">
                                        0
                                    </xsl:if>
                                    <xsl:if test="RequestNodes/RequestNode/Risks/FullBonusGetMode ='1'">
                                        2
                                    </xsl:if>
                                    <xsl:if test="RequestNodes/RequestNode/Risks/FullBonusGetMode ='4'">
                                        2
                                    </xsl:if>
                                </xsl:when>
                                <xsl:otherwise>
                                    <xsl:value-of select="''"/>
                                </xsl:otherwise>
                            </xsl:choose>
                        </xsl:if>
                    </xsl:with-param>
                </xsl:call-template>
            </LiveAccFlag>
            <GetBankAccNo>
                <xsl:value-of select="RequestNodes/RequestNode/Base/ConAccNo"/>
            </GetBankAccNo>
            <PrtNo>&lt;!&ndash; 投保单号 &ndash;&gt;
                <xsl:value-of select="RequestNodes/RequestNode/Base/PolicyApplySerial"/>
            </PrtNo>
            <GetAccName>
                <xsl:value-of select="RequestNodes/RequestNode/Base/ConAccName"/>
            </GetAccName>
            <SaleChnl>3</SaleChnl>
            <BonusMan>0</BonusMan>
            <SpecifyValiDate>N</SpecifyValiDate>
            <AgentType>08</AgentType>
            <CValiDate>
                <xsl:value-of
                        select="java:com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.cValiDate(RequestNodes/RequestNode/Base/ApplyDate)"/>
            </CValiDate>
            <InsuredSex>
                &lt;!&ndash;111&ndash;&gt;
                <xsl:value-of select="RequestNodes/RequestNode/Insu/Sex"/>
            </InsuredSex>
            <InsuredName>
                &lt;!&ndash;111&ndash;&gt;
                <xsl:value-of select="RequestNodes/RequestNode/Insu/Name"/>
            </InsuredName>
            <PolApplyDate>
                &lt;!&ndash;投保单申请日期&ndash;&gt;
                &lt;!&ndash;111&ndash;&gt;
                <xsl:value-of
                        select="java:com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.dateFormat8To10(RequestNodes/RequestNode/Base/ApplyDate)"/>
            </PolApplyDate>
            <AutoRnewAge>&lt;!&ndash;自动续保年龄&ndash;&gt;
                <xsl:call-template name="tran_autoRenewAge">
                    <xsl:with-param name="autoRenewAge" select="RequestNodes/RequestNode/Base/AutoRenewAge"/>
                </xsl:call-template>
            </AutoRnewAge>
            <xsl:if test="RequestNodes/RequestNode/Insu/ImmaturityFlag = 1">
                &lt;!&ndash;111&ndash;&gt;
                <OtherAmnt>
                    <xsl:value-of select="RequestNodes/RequestNode/Insu/Immaturity"/>
                </OtherAmnt>
            </xsl:if>
        </LCPol>
        <xsl:if test="RequestNodes/RequestNode/Addt/Count != 0">
            <xsl:call-template name="addLCPol">
                <xsl:with-param name="count">
                    <xsl:value-of select="RequestNodes/RequestNode/Addt/Count"/>
                </xsl:with-param>
            </xsl:call-template>
        </xsl:if>
    </xsl:template>

    <xsl:template name="addtRiskLCPOL">
        <LCPol>
            <xsl:variable name="doubleRiskCode">
                <xsl:call-template name="tran_newriskCode">
                    <xsl:with-param name="riskCode"
                                    select="java:com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.checkRiskCode(RequestNodes/RequestNode/Risks/RiskCode)"/>
                    &lt;!&ndash;<xsl:with-param name="riskCode" select="$risks/RiskCode"/>&ndash;&gt;
                </xsl:call-template>
            </xsl:variable>
            <xsl:variable name="varRiskCodess">
                <xsl:call-template name="tran_riskCode">
                    <xsl:with-param name="riskCode"
                                    select="java:com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.checkRiskCode(RequestNodes/RequestNode/Risks/RiskCode)"/>
                    &lt;!&ndash;<xsl:with-param name="riskCode" select="$risks/RiskCode"/>&ndash;&gt;
                </xsl:call-template>
            </xsl:variable>
            <RiskCode>&lt;!&ndash; 险种代码 &ndash;&gt;
                <xsl:value-of select="$doubleRiskCode"/>
            </RiskCode>
            <MainPolNo>&lt;!&ndash; 主险代码 &ndash;&gt;
                <xsl:value-of select="$varRiskCodess"/>
            </MainPolNo>
            <Amnt>&lt;!&ndash; 保额 &ndash;&gt;
                <xsl:choose>
                    <xsl:when test="Header/EntrustWay = 01">
                        <xsl:call-template name="tran_amnt">
                            <xsl:with-param name="riskCode" select="$doubleRiskCode"/>
                            <xsl:with-param name="amnt" select="RequestNodes/RequestNode/Risks/Amnt"/>
                        </xsl:call-template>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="RequestNodes/RequestNode/Risks/Amnt"/>
                    </xsl:otherwise>
                </xsl:choose>
            </Amnt>
            <Prem>&lt;!&ndash; 保费 &ndash;&gt;
                <xsl:value-of select="RequestNodes/RequestNode/Risks/Prem"/>
            </Prem>
            <Mult>&lt;!&ndash; 份数 &ndash;&gt;
                <xsl:value-of select="RequestNodes/RequestNode/Risks/Share"/>
            </Mult>
            <PayIntv>&lt;!&ndash; 缴费间隔 &ndash;&gt;
                <xsl:call-template name="tran_Contpayintv">
                    <xsl:with-param name="payintv">
                        <xsl:value-of select="RequestNodes/RequestNode/Risks/PayType"/>
                    </xsl:with-param>
                </xsl:call-template>
            </PayIntv>
            <xsl:if test="Header/EntrustWay = 01">
                <PayMode>C</PayMode>
            </xsl:if>
            <xsl:if test="Header/EntrustWay = 04">
                <PayMode>C</PayMode>
            </xsl:if>
            <xsl:if test="Header/EntrustWay= 11">
                <PayMode>A</PayMode>
            </xsl:if>
            <xsl:if test="Header/EntrustWay = 02">
                <PayMode>C</PayMode>
            </xsl:if>
            <xsl:if test="Header/EntrustWay = 22">
                <PayMode>A</PayMode>
            </xsl:if>
            <xsl:if test="Header/EntrustWay= 08">
                <PayMode>C</PayMode>
            </xsl:if>
            <InsuYearFlag>&lt;!&ndash; 保险年期类型 &ndash;&gt;
                <xsl:call-template name="tran_insuYearFlag">
                    <xsl:with-param name="yearFlag" select="RequestNodes/RequestNode/Risks/InsuDueType"/>
                </xsl:call-template>
            </InsuYearFlag>
            <InsuYear>
                <xsl:call-template name="tran_InsuYear">
                    <xsl:with-param name="insuYear" select="RequestNodes/RequestNode/Risks/InsuDueDate"/>
                </xsl:call-template>
            </InsuYear>
            <xsl:if test="RequestNodes/RequestNode/Risks/PayType = 1">
                <PayYears>1000</PayYears>
            </xsl:if>
            <xsl:if test="RequestNodes/RequestNode/Risks/PayType != 1">
                <PayYears>
                    <xsl:value-of select="RequestNodes/RequestNode/Risks/PayDueDate"/>
                </PayYears>
            </xsl:if>
            <xsl:if test="RequestNodes/RequestNode/Risks/PayType = 1">
                <PayEndYear>1000</PayEndYear>
                <PayEndYearFlag>Y</PayEndYearFlag>
            </xsl:if>
            <xsl:if test="RequestNodes/RequestNode/Risks/PayType != 1">
                <PayEndYear>
                    <xsl:value-of select="RequestNodes/RequestNode/Risks/PayDueDate"/>
                </PayEndYear>
                <PayEndYearFlag>
                    <xsl:if test="RequestNodes/RequestNode/Risks/PayDueType != ''">
                        <xsl:call-template name="tran_payendYearFlag">
                            <xsl:with-param name="yearFlag" select="RequestNodes/RequestNode/Risks/PayDueType"/>
                        </xsl:call-template>
                    </xsl:if>
                    <xsl:if test="RequestNodes/RequestNode/Risks/PayDueType = ''">Y</xsl:if>
                </PayEndYearFlag>
            </xsl:if>
            <BonusGetMode>&lt;!&ndash; 红利领取方式 &ndash;&gt;
                <xsl:choose>
                    <xsl:when test="string($doubleRiskCode) = '2048'">1</xsl:when>
                    <xsl:when
                            test="string($doubleRiskCode) = '2052' and Header/EntrustWay='11' and RequestNodes/RequestNode/Risks/BonusGetMode !='' and exists(RequestNodes/RequestNode/Risks/BonusGetMode)">
                        <xsl:call-template name="tran_getMode">
                            <xsl:with-param name="bonusGetMode" select="RequestNodes/RequestNode/Risks/BonusGetMode"/>
                        </xsl:call-template>
                    </xsl:when>
                    <xsl:when test="string($doubleRiskCode) = '2052'">1</xsl:when>
                    <xsl:when
                            test="string($doubleRiskCode) = '2053' and Header/EntrustWay='11' and RequestNodes/RequestNode/Risks/BonusGetMode !='' and exists(RequestNodes/RequestNode/Risks/BonusGetMode)">
                        <xsl:call-template name="tran_getMode">
                            <xsl:with-param name="bonusGetMode" select="RequestNodes/RequestNode/Risks/BonusGetMode"/>
                        </xsl:call-template>
                    </xsl:when>
                    <xsl:when test="string($doubleRiskCode) = '2053'">1</xsl:when>
                    <xsl:otherwise>
                        <xsl:if test="RequestNodes/RequestNode/Risks/BonusGetMode!=''">
                            <xsl:call-template name="tran_getMode">
                                <xsl:with-param name="bonusGetMode"
                                                select="RequestNodes/RequestNode/Risks/BonusGetMode"/>
                            </xsl:call-template>
                        </xsl:if>
                    </xsl:otherwise>
                </xsl:choose>
            </BonusGetMode>
            <GetForm>&lt;!&ndash; 年金领取方式 /保险金领取方式&ndash;&gt;
                <xsl:call-template name="tran_getIntv">
                    <xsl:with-param name="riskCode">
                        <xsl:value-of select="$doubleRiskCode"/>
                    </xsl:with-param>
                    <xsl:with-param name="getIntv">
                        <xsl:value-of select="RequestNodes/RequestNode/Risks/FullBonusGetMode"/>
                    </xsl:with-param>
                </xsl:call-template>
            </GetForm>
            <xsl:if test="Header/EntrustWay = 04 or Header/EntrustWay = 02 or Header/EntrustWay = 01 or Header/EntrustWay = 22 or Header/EntrustWay = 08">
                <OccupationType>
                    <xsl:call-template name="tran_occupationType">
                        <xsl:with-param name="occupationType" select="RequestNodes/RequestNode/Insu/JobType"/>
                        <xsl:with-param name="riskcode" select="$varRiskCodess"/>
                    </xsl:call-template>
                </OccupationType>
            </xsl:if>
            <xsl:if test="Header/EntrustWay = 11">
                <OccupationType>
                    <xsl:value-of select="RequestNodes/RequestNode/Insu/JobType"/>
                </OccupationType>
            </xsl:if>
            &lt;!&ndash;<xsl:if test="Header/EntrustWay = 11 ">&ndash;&gt;
            &lt;!&ndash;暂定 6807 全转成05&ndash;&gt;
            <xsl:if test="Header/EntrustWay = 11">
                <GetBankCode>05</GetBankCode>
            </xsl:if>
            <xsl:if test="Header/EntrustWay != 11">
                <GetBankCode>0501</GetBankCode>
            </xsl:if>
            <RnewFlag>
                <xsl:call-template name="tran_renew">
                    <xsl:with-param name="flag" select="RequestNodes/RequestNode/Risks/AutoPayFlag"/>
                </xsl:call-template>
            </RnewFlag>&lt;!&ndash; 自动续保标志 &ndash;&gt;
            <xsl:if test="RequestNodes/RequestNode/Risks/AutoPayForFlag = ''">
                <AutoPayFlag>0</AutoPayFlag>&lt;!&ndash; 自动垫交标志 &ndash;&gt;
            </xsl:if>
            <xsl:if test="RequestNodes/RequestNode/Risks/AutoPayForFlag != ''">
                <AutoPayFlag>
                    <xsl:value-of select="RequestNodes/RequestNode/Risks/AutoPayForFlag"/>
                </AutoPayFlag>&lt;!&ndash; 自动垫交标志 &ndash;&gt;
            </xsl:if>
            <EndDate>
                <xsl:value-of
                        select="java:com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.dateFormat8To10(RequestNodes/RequestNode/Risks/RiskEndDate)"/>
            </EndDate>&lt;!&ndash; 保险止期 &ndash;&gt;
            <GetYear>
                <xsl:value-of select="RequestNodes/RequestNode/Risks/GetYear"/>
            </GetYear>&lt;!&ndash; 领取年期 &ndash;&gt;
            <GetYearFlag>
                <xsl:call-template name="tran_getyearflag">
                    <xsl:with-param name="getyearflag">
                        <xsl:value-of select="RequestNodes/RequestNode/Risks/GetYearFlag"/>
                    </xsl:with-param>
                </xsl:call-template>
            </GetYearFlag>&lt;!&ndash; 年金/生存金领取年期类型: &ndash;&gt;

            <LiveGetMode>
                <xsl:call-template name="tran_livegetmode">
                    <xsl:with-param name="livegetmode">
                        <xsl:if test="Header/EntrustWay= 01 or Header/EntrustWay= 02 or Header/EntrustWay= 04 or Header/EntrustWay = 22">
                            <xsl:choose>
                                <xsl:when
                                        test="string($doubleRiskCode) = '5022' or string($varRiskCodess) = 'C066' or string($varRiskCodess) = 'C067' or string($varRiskCodess) = 'C068' or string($varRiskCodess) = 'C069' or string($varRiskCodess) = 'C072'">
                                    2
                                </xsl:when>
                                &lt;!&ndash; 1累计生息，2现金领取 &ndash;&gt;
                                <xsl:when test="string($doubleRiskCode) = '2041'">
                                    1
                                </xsl:when>
                                <xsl:when test="string($varRiskCodess) = '1029'">2</xsl:when>
                                <xsl:when test="string($varRiskCodess) = '2039'">
                                    <xsl:if test="RequestNodes/RequestNode/Risks/FullBonusGetMode ='0'">
                                        1
                                    </xsl:if>
                                    <xsl:if test="RequestNodes/RequestNode/Risks/FullBonusGetMode ='1'">
                                        2
                                    </xsl:if>
                                    <xsl:if test="RequestNodes/RequestNode/Risks/FullBonusGetMode ='4'">
                                        2
                                    </xsl:if>
                                </xsl:when>
                                <xsl:when test="string($varRiskCodess) = '2048'">2</xsl:when>
                                <xsl:when test="string($varRiskCodess) = '2052'">2</xsl:when>
                                <xsl:when test="string($varRiskCodess) = '2053'">2</xsl:when>
                                <xsl:otherwise>
                                    <xsl:value-of select="''"/>
                                </xsl:otherwise>
                            </xsl:choose>
                        </xsl:if>
                        <xsl:if test="Header/EntrustWay = 11 or Header/EntrustWay = 08">
                            <xsl:choose>
                                <xsl:when
                                        test="string($varRiskCodess) = '5022' or string($varRiskCodess) = 'C066' or string($varRiskCodess) = 'C067' or string($varRiskCodess) = 'C068' or string($varRiskCodess) = 'C069' or string($varRiskCodess) = 'C072'">
                                    2
                                </xsl:when>
                                &lt;!&ndash; 1累计生息，2现金领取 &ndash;&gt;
                                <xsl:when test="string($varRiskCodess) = '2041'">
                                    1
                                </xsl:when>
                                <xsl:when test="string($varRiskCodess) = '2039'">
                                    <xsl:if test="RequestNodes/RequestNode/Risks/FullBonusGetMode ='0'">
                                        1
                                    </xsl:if>
                                    <xsl:if test="RequestNodes/RequestNode/Risks/FullBonusGetMode ='1'">
                                        2
                                    </xsl:if>
                                    <xsl:if test="RequestNodes/RequestNode/Risks/FullBonusGetMode ='4'">
                                        2
                                    </xsl:if>
                                </xsl:when>
                                <xsl:when test="string($varRiskCodess) = '2048'">2</xsl:when>
                                <xsl:when test="string($varRiskCodess) = '2052'">2</xsl:when>
                                <xsl:when test="string($varRiskCodess) = '2053'">2</xsl:when>
                                <xsl:otherwise>
                                    <xsl:value-of select="''"/>
                                </xsl:otherwise>
                            </xsl:choose>
                        </xsl:if>
                    </xsl:with-param>
                </xsl:call-template>
            </LiveGetMode>
            <LiveAccFlag>
                <xsl:call-template name="tran_liveaccflag">
                    <xsl:with-param name="liveaccflag">
                        <xsl:if test="Header/EntrustWay = 01 or Header/EntrustWay = 02 or Header/EntrustWay = 04 or Header/EntrustWay = 22 ">
                            <xsl:choose>
                                &lt;!&ndash; 1累计生息，2现金领取 &ndash;&gt;
                                <xsl:when test="string($varRiskCodess) = '2041'">
                                    0
                                </xsl:when>
                                <xsl:when test="string($varRiskCodess) = '1029'">2</xsl:when>
                                <xsl:when test="string($varRiskCodess) = '2039'">
                                    <xsl:if test="RequestNodes/RequestNode/Risks/FullBonusGetMode ='0'">
                                        0
                                    </xsl:if>
                                    <xsl:if test="RequestNodes/RequestNode/Risks/FullBonusGetMode ='1'">
                                        2
                                    </xsl:if>
                                    <xsl:if test="RequestNodes/RequestNode/Risks/FullBonusGetMode ='4'">
                                        2
                                    </xsl:if>
                                </xsl:when>
                                <xsl:otherwise>
                                    <xsl:value-of select="''"/>
                                </xsl:otherwise>
                            </xsl:choose>
                        </xsl:if>
                        <xsl:if test="Header/EntrustWay = 11 or Header/EntrustWay = 08">
                            <xsl:choose>
                                &lt;!&ndash; 1累计生息，2现金领取 &ndash;&gt;
                                <xsl:when test="string($varRiskCodess) = '2041'">
                                    0
                                </xsl:when>
                                <xsl:when test="string($varRiskCodess) = '2039'">
                                    <xsl:if test="RequestNodes/RequestNode/Risks/FullBonusGetMode ='0'">
                                        0
                                    </xsl:if>
                                    <xsl:if test="RequestNodes/RequestNode/Risks/FullBonusGetMode ='1'">
                                        2
                                    </xsl:if>
                                    <xsl:if test="RequestNodes/RequestNode/Risks/FullBonusGetMode ='4'">
                                        2
                                    </xsl:if>
                                </xsl:when>
                                <xsl:otherwise>
                                    <xsl:value-of select="''"/>
                                </xsl:otherwise>
                            </xsl:choose>
                        </xsl:if>
                    </xsl:with-param>
                </xsl:call-template>
            </LiveAccFlag>
            <GetBankAccNo>
                <xsl:value-of select="RequestNodes/RequestNode/Base/ConAccNo"/>
            </GetBankAccNo>
            <PrtNo>&lt;!&ndash; 投保单号 &ndash;&gt;
                <xsl:value-of select="RequestNodes/RequestNode/Base/PolicyApplySerial"/>
            </PrtNo>
            <GetAccName>
                <xsl:value-of select="RequestNodes/RequestNode/Base/ConAccName"/>
            </GetAccName>
            <SaleChnl>3</SaleChnl>
            <BonusMan>0</BonusMan>
            <SpecifyValiDate>N</SpecifyValiDate>
            <AgentType>08</AgentType>
            <CValiDate>
                <xsl:value-of
                        select="java:com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.cValiDate(RequestNodes/RequestNode/Base/ApplyDate)"/>
            </CValiDate>
            <InsuredSex>
                &lt;!&ndash;111&ndash;&gt;
                <xsl:value-of select="RequestNodes/RequestNode/Insu/Sex"/>
            </InsuredSex>
            <InsuredName>
                &lt;!&ndash;111&ndash;&gt;
                <xsl:value-of select="RequestNodes/RequestNode/Insu/Name"/>
            </InsuredName>
            <PolApplyDate>
                &lt;!&ndash;投保单申请日期&ndash;&gt;
                &lt;!&ndash;111&ndash;&gt;
                <xsl:value-of
                        select="java:com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.dateFormat8To10(RequestNodes/RequestNode/Base/ApplyDate)"/>
            </PolApplyDate>
            <AutoRnewAge>&lt;!&ndash;自动续保年龄&ndash;&gt;
                <xsl:call-template name="tran_autoRenewAge">
                    <xsl:with-param name="autoRenewAge" select="RequestNodes/RequestNode/Base/AutoRenewAge"/>
                </xsl:call-template>
            </AutoRnewAge>
            <xsl:if test="RequestNodes/RequestNode/Insu/ImmaturityFlag = 1">
                &lt;!&ndash;111&ndash;&gt;
                <OtherAmnt>
                    <xsl:value-of select="RequestNodes/RequestNode/Insu/Immaturity"/>
                </OtherAmnt>
            </xsl:if>
        </LCPol>
        <xsl:if test="RequestNodes/RequestNode/Addt/Count != 0">
            <xsl:call-template name="addLCPol">
                <xsl:with-param name="count">
                    <xsl:value-of select="RequestNodes/RequestNode/Addt/Count"/>
                </xsl:with-param>
            </xsl:call-template>
        </xsl:if>
    </xsl:template>-->
    <!-- 添加附加险 根据Addt/Count的值确定添加数量 -->
    <!-- <xsl:template name="addLCPol">
         <xsl:param name="count"/>
         <xsl:param name="position" select="1"/>
         <xsl:if test="$count>0">
             <xsl:variable name="riskCode" select="concat('RiskCode',$position)"/>
             <xsl:variable name="amnt" select="concat('Amnt',$position)"/>
             <xsl:variable name="prem" select="concat('Prem',$position)"/>
             <xsl:variable name="share" select="concat('Share',$position)"/>
             <xsl:variable name="payType" select="concat('PayType',$position)"/>
             <xsl:variable name="insuDueType" select="concat('InsuDueType',$position)"/>
             <xsl:variable name="insuDueDate" select="concat('InsuDueDate',$position)"/>
             <xsl:variable name="payDueType" select="concat('PayDueType',$position)"/>
             <xsl:variable name="payDueDate" select="concat('PayDueDate',$position)"/>
             <xsl:variable name="autoPayFlag" select="concat('AutoPayFlag',$position)"/>
             <xsl:variable name="autoPayForFlag" select="concat('AutoPayForFlag',$position)"/>
             <LCPol>
                 <xsl:variable name="varRiskCode">
                     <xsl:call-template name="tran_riskCode">
                         <xsl:with-param name="riskCode"
                                         select="java:com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.checkRiskCode(RequestNodes/RequestNode/Addt/*[name()=$riskCode])"/>
                         &lt;!&ndash;<xsl:with-param name="riskCode" select="RequestNodes/RequestNode/Addt/*[name()=$riskCode]"/>&ndash;&gt;
                     </xsl:call-template>
                 </xsl:variable>
                 <xsl:variable name="varMainRiskCode">
                     <xsl:call-template name="tran_riskCode">
                         <xsl:with-param name="riskCode"
                                         select="java:com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.checkRiskCode(RequestNodes/RequestNode/Risks/RiskCode)"/>
                         &lt;!&ndash;<xsl:with-param name="riskCode" select="RequestNodes/RequestNode/Risks/RiskCode"/>&ndash;&gt;
                     </xsl:call-template>
                 </xsl:variable>
                 <RiskCode>
                     <xsl:value-of select="$varRiskCode"/>
                 </RiskCode>
                 <MainPolNo>
                     <xsl:value-of select="$varMainRiskCode"/>
                 </MainPolNo>
                 <Amnt>
                     <xsl:value-of select="RequestNodes/RequestNode/Addt/*[name()=$amnt]"/>
                 </Amnt>
                 <Prem>
                     <xsl:value-of select="RequestNodes/RequestNode/Addt/*[name()=$prem]"/>
                 </Prem>
                 <Mult>
                     <xsl:value-of select="RequestNodes/RequestNode/Addt/*[name()=$share]"/>
                 </Mult>
                 <PayIntv>
                     <xsl:call-template name="tran_Contpayintv">
                         <xsl:with-param name="payintv" select="RequestNodes/RequestNode/Addt/*[name()=$payType]"/>
                     </xsl:call-template>
                 </PayIntv>
                 <xsl:if test="Header/EntrustWay = 01">
                     <PayMode>C</PayMode>
                 </xsl:if>
                 <xsl:if test="Header/EntrustWay = 04">
                     <PayMode>C</PayMode>
                 </xsl:if>
                 <xsl:if test="Header/EntrustWay = 11">
                     <PayMode>A</PayMode>
                 </xsl:if>
                 <xsl:if test="Header/EntrustWay = 02">
                     <PayMode>C</PayMode>
                 </xsl:if>
                 <xsl:if test="Header/EntrustWay = 22">
                     <PayMode>A</PayMode>
                 </xsl:if>
                 <xsl:if test="Header/EntrustWay = 08">
                     <PayMode>C</PayMode>
                 </xsl:if>
                 <InsuYearFlag>
                     &lt;!&ndash; 保险年期类型 &ndash;&gt;
                     <xsl:call-template name="tran_insuYearFlag">
                         <xsl:with-param name="yearFlag" select="RequestNodes/RequestNode/Addt/*[name()=$insuDueType]"/>
                     </xsl:call-template>
                 </InsuYearFlag>
                 <InsuYear>
                     <xsl:call-template name="tran_InsuYear">
                         <xsl:with-param name="insuYear" select="RequestNodes/RequestNode/Addt/*[name()=$insuDueDate]"/>
                     </xsl:call-template>
                 </InsuYear>
                 <PayYears>
                     <xsl:value-of select="RequestNodes/RequestNode/Addt/*[name()=$payDueDate]"/>
                 </PayYears>
                 <xsl:if test="RequestNodes/RequestNode/Addt/*[name()=$payType] = 1">
                     <PayEndYear>1000</PayEndYear>
                     <PayEndYearFlag>Y</PayEndYearFlag>
                 </xsl:if>
                 <xsl:if test="RequestNodes/RequestNode/Addt/*[name()=$payType] != 1">
                     <PayEndYear>
                         <xsl:value-of select="RequestNodes/RequestNode/Addt/*[name()=$payType]"/>
                     </PayEndYear>
                     <PayEndYearFlag>
                         <xsl:if test="RequestNodes/RequestNode/Addt/*[name()=$payType] != ''">
                             <xsl:call-template name="tran_payendYearFlag">
                                 <xsl:with-param name="yearFlag"
                                                 select="RequestNodes/RequestNode/Addt/*[name()=$payType]"/>
                             </xsl:call-template>
                         </xsl:if>
                         <xsl:if test="RequestNodes/RequestNode/Addt/*[name()=$payType] = ''">Y</xsl:if>
                         &lt;!&ndash;<xsl:if test="Header/EntrustWay = 01">&ndash;&gt;
                         &lt;!&ndash;<xsl:if test="RequestNodes/RequestNode/Addt/*[name()=$riskCode] = 1029">Y</xsl:if>&ndash;&gt;
                         &lt;!&ndash;<xsl:if test="RequestNodes/RequestNode/Addt/*[name()=$riskCode] = 1022">Y</xsl:if>&ndash;&gt;
                         &lt;!&ndash;<xsl:if test="RequestNodes/RequestNode/Addt/*[name()=$riskCode] = 2036">Y</xsl:if>&ndash;&gt;
                         &lt;!&ndash;<xsl:if test="RequestNodes/RequestNode/Addt/*[name()=$riskCode] = 2044">Y</xsl:if>&ndash;&gt;
                         &lt;!&ndash;</xsl:if>&ndash;&gt;
                     </PayEndYearFlag>
                 </xsl:if>
                 <BonusGetMode>
                     &lt;!&ndash; 红利领取方式 &ndash;&gt;
                 </BonusGetMode>
                 <GetForm>
                     &lt;!&ndash; 年金领取方式 /保险金领取方式&ndash;&gt;
                     <xsl:call-template name="tran_getIntv">
                         <xsl:with-param name="riskCode">
                             <xsl:value-of select="$varRiskCode"/>
                         </xsl:with-param>
                         <xsl:with-param name="getIntv">
                             <xsl:value-of select="RequestNodes/RequestNode/Risks/FullBonusGetMode"/>
                         </xsl:with-param>
                     </xsl:call-template>
                 </GetForm>
                 <xsl:if test="Header/EntrustWay = 04 or Header/EntrustWay = 02 or Header/EntrustWay = 01 or Header/EntrustWay = 22 or Header/EntrustWay = 08">
                     <OccupationType>
                         <xsl:call-template name="tran_occupationType">
                             <xsl:with-param name="occupationType" select="RequestNodes/RequestNode/Insu/JobType"/>
                             <xsl:with-param name="riskcode" select="$varRiskCode"/>
                         </xsl:call-template>
                     </OccupationType>
                 </xsl:if>
                 <xsl:if test="Header/EntrustWay = 11">
                     <OccupationType>
                         <xsl:value-of select="RequestNodes/RequestNode/Insu/JobType"/>
                     </OccupationType>
                 </xsl:if>
                 &lt;!&ndash;<xsl:if test="Header/EntrustWay = 11 ">&ndash;&gt;
                 &lt;!&ndash;暂定 6807 全转成05&ndash;&gt;
                 <xsl:if test="Header/EntrustWay = 11">
                     <GetBankCode>05</GetBankCode>
                 </xsl:if>
                 <xsl:if test="Header/EntrustWay != 11">
                     <GetBankCode>0501</GetBankCode>
                 </xsl:if>
                 <RnewFlag>
                     <xsl:call-template name="tran_renew">
                         <xsl:with-param name="flag" select="RequestNodes/RequestNode/Addt/*[name()=$autoPayFlag]"/>
                     </xsl:call-template>
                 </RnewFlag>
                 &lt;!&ndash; 自动续保标志 &ndash;&gt;
                 <AutoPayFlag>0</AutoPayFlag>
                 &lt;!&ndash; <xsl:if test="RequestNodes/RequestNode/Addt/*[name()=$autoPayForFlag] = ''">
                          <AutoPayFlag>0</AutoPayFlag>&lt;!&ndash; 自动垫交标志 &ndash;&gt;
                      </xsl:if>
                      <xsl:if test="RequestNodes/RequestNode/Addt/*[name()=$autoPayForFlag] != ''">
                          <AutoPayFlag>
                              0 &lt;!&ndash; <xsl:value-of select="RequestNodes/RequestNode/Risks/AutoPayForFlag"/>&ndash;&gt;
                          </AutoPayFlag>&lt;!&ndash; 自动垫交标志 &ndash;&gt;
                      </xsl:if>&ndash;&gt;
                 <EndDate>
                     <xsl:value-of
                             select="java:com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.dateFormat8To10(RequestNodes/RequestNode/Risks/RiskEndDate)"/>
                 </EndDate>
                 &lt;!&ndash; 保险止期 &ndash;&gt;
                 <LiveGetMode>
                     <xsl:call-template name="tran_livegetmode">
                         <xsl:with-param name="livegetmode">
                             <xsl:if test="Header/EntrustWay = 01 or Header/EntrustWay = 02 or Header/EntrustWay = 04 or Header/EntrustWay = 22 ">
                                 <xsl:choose>
                                     <xsl:when test="string($varRiskCode) = '5022' or string($varRiskCode) = 'C066'">2
                                     </xsl:when>
                                     &lt;!&ndash; 1累计生息，2现金领取 &ndash;&gt;
                                     <xsl:when test="string($varRiskCode) = '2041'">
                                         1
                                     </xsl:when>
                                     <xsl:when test="string($varRiskCode) = '1029'">2</xsl:when>
                                     <xsl:when test="string($varRiskCode) = '2039'">
                                         <xsl:if test="RequestNodes/RequestNode/Risks/FullBonusGetMode ='0'">
                                             1
                                         </xsl:if>
                                         <xsl:if test="RequestNodes/RequestNode/Risks/FullBonusGetMode ='1'">
                                             2
                                         </xsl:if>
                                         <xsl:if test="RequestNodes/RequestNode/Risks/FullBonusGetMode ='4'">
                                             2
                                         </xsl:if>
                                     </xsl:when>
                                     <xsl:otherwise>
                                         <xsl:value-of select="''"/>
                                     </xsl:otherwise>
                                 </xsl:choose>
                             </xsl:if>
                             <xsl:if test="Header/EntrustWay = 11 or Header/EntrustWay = 08">
                                 <xsl:choose>
                                     <xsl:when test="string($varRiskCode) = '5022' or string($varRiskCode) = 'C066'">2
                                     </xsl:when>
                                     &lt;!&ndash; 1累计生息，2现金领取 &ndash;&gt;
                                     <xsl:when test="string($varRiskCode) = '2041'">
                                         1
                                     </xsl:when>
                                     <xsl:when test="string($varRiskCode) = '2039'">
                                         <xsl:if test="RequestNodes/RequestNode/Risks/FullBonusGetMode ='0'">
                                             1
                                         </xsl:if>
                                         <xsl:if test="RequestNodes/RequestNode/Risks/FullBonusGetMode ='1'">
                                             2
                                         </xsl:if>
                                         <xsl:if test="RequestNodes/RequestNode/Risks/FullBonusGetMode ='4'">
                                             2
                                         </xsl:if>
                                     </xsl:when>
                                     <xsl:otherwise>
                                         <xsl:value-of select="''"/>
                                     </xsl:otherwise>
                                 </xsl:choose>
                             </xsl:if>
                         </xsl:with-param>
                     </xsl:call-template>
                 </LiveGetMode>
                 <LiveAccFlag>
                     <xsl:call-template name="tran_liveaccflag">
                         <xsl:with-param name="liveaccflag">
                             <xsl:if test="Header/EntrustWay = 01 or Header/EntrustWay = 02 or Header/EntrustWay = 04 or Header/EntrustWay = 22">
                                 <xsl:choose>
                                     &lt;!&ndash; 1累计生息，2现金领取 &ndash;&gt;
                                     <xsl:when test="string($varRiskCode) = '2041'">
                                         0
                                     </xsl:when>
                                     <xsl:when test="string($varRiskCode) = '1029'">2</xsl:when>
                                     <xsl:when test="string($varRiskCode) = '2039'">
                                         <xsl:if test="RequestNodes/RequestNode/Risks/FullBonusGetMode ='0'">
                                             0
                                         </xsl:if>
                                         <xsl:if test="RequestNodes/RequestNode/Risks/FullBonusGetMode ='1'">
                                             2
                                         </xsl:if>
                                         <xsl:if test="RequestNodes/RequestNode/Risks/FullBonusGetMode ='4'">
                                             2
                                         </xsl:if>
                                     </xsl:when>
                                     <xsl:otherwise>
                                         <xsl:value-of select="''"/>
                                     </xsl:otherwise>
                                 </xsl:choose>
                             </xsl:if>
                             <xsl:if test="Header/EntrustWay = 11 or Header/EntrustWay = 08">
                                 <xsl:choose>
                                     &lt;!&ndash; 1累计生息，2现金领取 &ndash;&gt;
                                     <xsl:when test="string($varRiskCode) = '2041'">
                                         0
                                     </xsl:when>
                                     <xsl:when test="string($varRiskCode) = '2039'">
                                         <xsl:if test="RequestNodes/RequestNode/Risks/FullBonusGetMode ='0'">
                                             0
                                         </xsl:if>
                                         <xsl:if test="RequestNodes/RequestNode/Risks/FullBonusGetMode ='1'">
                                             2
                                         </xsl:if>
                                         <xsl:if test="RequestNodes/RequestNode/Risks/FullBonusGetMode ='4'">
                                             2
                                         </xsl:if>
                                     </xsl:when>
                                     <xsl:otherwise>
                                         <xsl:value-of select="''"/>
                                     </xsl:otherwise>
                                 </xsl:choose>
                             </xsl:if>
                         </xsl:with-param>
                     </xsl:call-template>
                 </LiveAccFlag>
                 <GetBankAccNo>
                     <xsl:value-of select="RequestNodes/RequestNode/Base/ConAccNo"/>
                 </GetBankAccNo>
                 <PrtNo>
                     <xsl:value-of select="RequestNodes/RequestNode/Base/PolicyApplySerial"/>
                     &lt;!&ndash;lcpol还没加&ndash;&gt;
                 </PrtNo>
                 <GetAccName>
                     <xsl:value-of select="RequestNodes/RequestNode/Base/ConAccName"/>
                 </GetAccName>
                 <SaleChnl>3</SaleChnl>
                 <BonusMan>0</BonusMan>
                 <SpecifyValiDate>N</SpecifyValiDate>
                 <AgentType>08</AgentType>
                 <CValiDate>
                     <xsl:value-of select="$req/SignedDate"/>
                 </CValiDate>
             </LCPol>
             <xsl:call-template name="addLCPol">
                 <xsl:with-param name="count">
                     <xsl:value-of select="$count - 1"/>
                 </xsl:with-param>
                 <xsl:with-param name="position">
                     <xsl:value-of select="$position + 1"/>
                 </xsl:with-param>
             </xsl:call-template>
         </xsl:if>
     </xsl:template>-->

    <xsl:template name="tran_renewflag">
        <xsl:param name="flag"/>
        <xsl:choose>
            <xsl:when test="$flag = ''">-2</xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="$flag"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <!-- 添加受益人 根据Bnfs/Count的值确定添加数量 -->
    <xsl:template name="addLCBnf">
        <xsl:param name="count"/>
        <xsl:param name="position" select="1"/>
        <xsl:if test="$count>0">
            <xsl:variable name="type" select="concat('Type',$position)"/>
            <xsl:variable name="sequence" select="concat('Sequence',$position)"/>
            <xsl:variable name="name" select="concat('Name',$position)"/>
            <xsl:variable name="sex" select="concat('Sex',$position)"/>
            <xsl:variable name="birthday" select="concat('Birthday',$position)"/>
            <xsl:variable name="idKind" select="concat('IDKind',$position)"/>
            <xsl:variable name="idCode" select="concat('IDCode',$position)"/>
            <xsl:variable name="invalidDate" select="concat('InvalidDate',$position)"/>
            <xsl:variable name="prop" select="concat('Prop',$position)"/>
            <xsl:variable name="relationToInsured" select="concat('RelationToInsured',$position)"/>
            <xsl:variable name="country" select="concat('Country',$position)"/>
            <xsl:variable name="phone" select="concat('Phone',$position)"/>
            <xsl:variable name="address" select="concat('Address',$position)"/>
            <LCBnf>
                <BnfType>
                    <xsl:value-of select="RequestNodes/RequestNode/Bnfs/*[name()=$type]"/>
                </BnfType>
                <BnfGrade>
                    <xsl:value-of select="RequestNodes/RequestNode/Bnfs/*[name()=$sequence]"/>
                </BnfGrade>
                <Name>
                    <xsl:value-of select="RequestNodes/RequestNode/Bnfs/*[name()=$name]"/>
                </Name>
                <Sex>
                    <xsl:call-template name="tran_sex">
                        <xsl:with-param name="sex">
                            <xsl:value-of select="RequestNodes/RequestNode/Bnfs/*[name()=$sex]"/>
                        </xsl:with-param>
                    </xsl:call-template>
                </Sex>
                <Birthday>
                    <xsl:value-of
                            select="java:com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.dateFormat8To10(RequestNodes/RequestNode/Bnfs/*[name()=$birthday])"/>
                </Birthday>
                <IDType>
                    <xsl:call-template name="tran_idtype">
                        <xsl:with-param name="idtype">
                            <xsl:value-of select="RequestNodes/RequestNode/Bnfs/*[name()=$idKind]"/>
                        </xsl:with-param>
                    </xsl:call-template>
                </IDType>
                <IDNo>
                    <xsl:value-of select="RequestNodes/RequestNode/Bnfs/*[name()=$idCode]"/>
                </IDNo>
                <IdValiDate>
                    <xsl:call-template name="tran_idValidate">
                        <xsl:with-param name="idValidate"
                                        select="java:com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.dateFormat8To10(RequestNodes/RequestNode/Bnfs/*[name()=$invalidDate])"/>
                    </xsl:call-template>
                </IdValiDate>
                <!-- 受益人证件有效期 -->
                <xsl:if test="RequestNodes/RequestNode/Bnfs/*[name()=$prop] != ''">
                    <BnfLot>
                        <xsl:value-of select="RequestNodes/RequestNode/Bnfs/*[name()=$prop] div 100"/>
                    </BnfLot>
                </xsl:if>
                <xsl:if test="RequestNodes/RequestNode/Bnfs/*[name()=$prop] = ''">
                    <BnfLot>-1</BnfLot>
                </xsl:if>
                <RelationToInsured>
                    <xsl:value-of select="RequestNodes/RequestNode/Bnfs/*[name()=$relationToInsured]"/>
                </RelationToInsured>
                <!--<Nationality>
                        <xsl:call-template name="tran_nationality">
                            <xsl:with-param name="Flag" select="$flag"/>
                            <xsl:with-param name="nationality" select="$bnfs/*[name()=$country]"/>
                        </xsl:call-template>
                    </Nationality>-->
                <Tel>
                    <xsl:value-of select="RequestNodes/RequestNode/Bnfs/*[name()=$phone]"/>
                </Tel>
                <xsl:if test="RequestNodes/RequestNode/Bnfs/*[name()=$sequence] = ''">
                    <BnfNo>2147483647</BnfNo>
                </xsl:if>
                <xsl:if test="RequestNodes/RequestNode/Bnfs/*[name()=$sequence] != ''">
                    <BnfNo>
                        <xsl:value-of select="$position"/>
                    </BnfNo>
                </xsl:if>
                <xsl:if test="RequestNodes/RequestNode/Bnfs/*[name()=$address] !=''">
                    <Address>
                        <xsl:value-of select="concat('3~',RequestNodes/RequestNode/Bnfs/*[name()=$address])"/>
                    </Address>
                </xsl:if>
            </LCBnf>
            <xsl:call-template name="addLCBnf">
                <xsl:with-param name="count">
                    <xsl:value-of select="$count - 1"/>
                </xsl:with-param>
                <xsl:with-param name="position">
                    <xsl:value-of select="$position + 1"/>
                </xsl:with-param>
            </xsl:call-template>
        </xsl:if>
    </xsl:template>
    <!-- 健康告知 -->
    <xsl:template name="tran_Health">
        <xsl:param name="Health"/>
        <xsl:choose>
            <xsl:when test="$Health='1'">
                <xsl:value-of select="'Y'"/>
            </xsl:when>
            <xsl:when test="$Health='0'">
                <xsl:value-of select="'N'"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="''"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    <!-- 性别 -->
    <xsl:template name="tran_sex">
        <xsl:param name="sex">0</xsl:param>
        <xsl:choose>
            <xsl:when test="$sex = 0">0</xsl:when>
            <xsl:when test="$sex = 1">1</xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="$sex"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <!-- 保单的缴费方式 -->
    <xsl:template name="tran_Contpayintv">
        <xsl:param name="payintv">1</xsl:param>
        <xsl:if test="$payintv = 1">0</xsl:if><!-- 趸交 -->
        <xsl:if test="$payintv = 2">1</xsl:if><!-- 月缴 -->
        <xsl:if test="$payintv = 5">12</xsl:if><!-- 年缴 -->
        <xsl:if test="$payintv = ''">-1</xsl:if><!-- 不定期 -->
    </xsl:template>

    <!-- 保障年期/年龄标志 -->
    <xsl:template name="tran_insuYearFlag">
        <xsl:param name="yearFlag">4</xsl:param>
        <xsl:if test="$yearFlag = 0"></xsl:if>
        <xsl:if test="$yearFlag = 1"></xsl:if>
        <xsl:if test="$yearFlag = 2">M</xsl:if>
        <xsl:if test="$yearFlag = 3"></xsl:if>
        <xsl:if test="$yearFlag = 4">Y</xsl:if>
        <xsl:if test="$yearFlag = 5">A</xsl:if>
        <xsl:if test="$yearFlag = 6">A</xsl:if>
    </xsl:template>
    <xsl:template name="tran_InsuYear">
        <xsl:param name="insuYear"/>
        <xsl:choose>
            <xsl:when test="$insuYear=199">106</xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="$insuYear"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    <!-- 证件类型  -->
    <xsl:template name="tran_idtype">
        <xsl:param name="idtype"/>
        <xsl:if test="$idtype = 1">0</xsl:if>
        <xsl:if test="$idtype = 2">1</xsl:if>
        <xsl:if test="$idtype = 3">2</xsl:if>
        <xsl:if test="$idtype = 4">8</xsl:if>
        <xsl:if test="$idtype = 5">8</xsl:if>
        <xsl:if test="$idtype = 0">9</xsl:if>
        <xsl:if test="$idtype = 110001">0</xsl:if>
        <xsl:if test="$idtype = 110002">0</xsl:if>
        <xsl:if test="$idtype = 110003">12</xsl:if>
        <xsl:if test="$idtype = 110004">12</xsl:if>
        <xsl:if test="$idtype = 110005">4</xsl:if>
        <xsl:if test="$idtype = 110006">4</xsl:if>
        <xsl:if test="$idtype = 110011">8</xsl:if>
        <xsl:if test="$idtype = 110012">8</xsl:if>
        <xsl:if test="$idtype = 110013">2</xsl:if>
        <xsl:if test="$idtype = 110014">2</xsl:if>
        <xsl:if test="$idtype = 110015">8</xsl:if>
        <xsl:if test="$idtype = 110016">8</xsl:if>
        <xsl:if test="$idtype = 110017">8</xsl:if>
        <xsl:if test="$idtype = 110018">8</xsl:if>
        <xsl:if test="$idtype = 110019">15</xsl:if>
        <xsl:if test="$idtype = 110020">15</xsl:if>
        <xsl:if test="$idtype = 110021">16</xsl:if>
        <xsl:if test="$idtype = 110022">16</xsl:if>
        <xsl:if test="$idtype = 110023">1</xsl:if>
        <xsl:if test="$idtype = 110024">1</xsl:if>
        <xsl:if test="$idtype = 110025">1</xsl:if>
        <xsl:if test="$idtype = 110026">1</xsl:if>
        <xsl:if test="$idtype = 110027">2</xsl:if>
        <xsl:if test="$idtype = 110028">2</xsl:if>
        <xsl:if test="$idtype = 110029">8</xsl:if>
        <xsl:if test="$idtype = 110030">8</xsl:if>
        <xsl:if test="$idtype = 110031">13</xsl:if>
        <xsl:if test="$idtype = 110032">13</xsl:if>
        <xsl:if test="$idtype = 110033">2</xsl:if>
        <xsl:if test="$idtype = 110034">2</xsl:if>
        <xsl:if test="$idtype = 110035">13</xsl:if>
        <xsl:if test="$idtype = 110036">13</xsl:if>
        <xsl:if test="$idtype = 119998">8</xsl:if>
        <xsl:if test="$idtype = 119999">8</xsl:if>
        <xsl:if test="$idtype = 110037">17</xsl:if>
    </xsl:template>

    <!-- 缴费年期/年龄类型    暂时没用 -->
    <xsl:template name="tran_payendYearFlag">
        <xsl:param name="yearFlag">4</xsl:param>
        <xsl:if test="$yearFlag = 0"></xsl:if>
        <xsl:if test="$yearFlag = 1">A</xsl:if>
        <xsl:if test="$yearFlag = 2"></xsl:if>
        <xsl:if test="$yearFlag = 3"></xsl:if>
        <xsl:if test="$yearFlag = 4">Y</xsl:if>
        <xsl:if test="$yearFlag = 5"></xsl:if>
        <xsl:if test="$yearFlag = 6"></xsl:if>
    </xsl:template>
    <!-- 缴费形式 -->

    <!--国籍转换-->
    <xsl:template name="tran_nationality">
        <xsl:param name="nationality">156</xsl:param>
        <xsl:param name="Flag"></xsl:param>
        <xsl:choose>
            <!--<xsl:when test="$Flag = 01">CHN</xsl:when>--><!--网银渠道的转换为中国的需求没了 所以去掉-->
            <xsl:when test="$nationality = 	004">AFG</xsl:when>
            <xsl:when test="$nationality = 	008">ALB</xsl:when>
            <xsl:when test="$nationality = 	010">ATA</xsl:when>
            <xsl:when test="$nationality = 	012">DZA</xsl:when>
            <xsl:when test="$nationality = 	016">ASM</xsl:when>
            <xsl:when test="$nationality = 	020">AND</xsl:when>
            <xsl:when test="$nationality = 	024">AGO</xsl:when>
            <xsl:when test="$nationality = 	028">ATG</xsl:when>
            <xsl:when test="$nationality = 	031">AZE</xsl:when>
            <xsl:when test="$nationality = 	032">ARG</xsl:when>
            <xsl:when test="$nationality = 	036">AUS</xsl:when>
            <xsl:when test="$nationality = 	040">AUT</xsl:when>
            <xsl:when test="$nationality = 	044">BHS</xsl:when>
            <xsl:when test="$nationality = 	048">BHR</xsl:when>
            <xsl:when test="$nationality = 	050">BGD</xsl:when>
            <xsl:when test="$nationality = 	051">ARM</xsl:when>
            <xsl:when test="$nationality = 	052">BRB</xsl:when>
            <xsl:when test="$nationality = 	056">BEL</xsl:when>
            <xsl:when test="$nationality = 	060">BMU</xsl:when>
            <xsl:when test="$nationality = 	064">BTN</xsl:when>
            <xsl:when test="$nationality = 	068">BOL</xsl:when>
            <xsl:when test="$nationality = 	070">BIH</xsl:when>
            <xsl:when test="$nationality = 	072">BWA</xsl:when>
            <xsl:when test="$nationality = 	074">BVT</xsl:when>
            <xsl:when test="$nationality = 	076">BRA</xsl:when>
            <xsl:when test="$nationality = 	084">BLZ</xsl:when>
            <xsl:when test="$nationality = 	086">IOT</xsl:when>
            <xsl:when test="$nationality = 	090">SLB</xsl:when>
            <xsl:when test="$nationality = 	092">VGB</xsl:when>
            <xsl:when test="$nationality = 	096">BRN</xsl:when>
            <xsl:when test="$nationality = 	100">BGR</xsl:when>
            <xsl:when test="$nationality = 	104">MMR</xsl:when>
            <xsl:when test="$nationality = 	108">BDI</xsl:when>
            <xsl:when test="$nationality = 	112">BLR</xsl:when>
            <xsl:when test="$nationality = 	116">KHM</xsl:when>
            <xsl:when test="$nationality = 	120">CMR</xsl:when>
            <xsl:when test="$nationality = 	124">CAN</xsl:when>
            <xsl:when test="$nationality = 	132">CPV</xsl:when>
            <xsl:when test="$nationality = 	136">CYM</xsl:when>
            <xsl:when test="$nationality = 	140">CAF</xsl:when>
            <xsl:when test="$nationality = 	144">LKA</xsl:when>
            <xsl:when test="$nationality = 	148">TCD</xsl:when>
            <xsl:when test="$nationality = 	152">CHL</xsl:when>
            <xsl:when test="$nationality = 	156">CHN</xsl:when>
            <xsl:when test="$nationality = 	158">TWN</xsl:when>
            <xsl:when test="$nationality = 	162">CSR</xsl:when>
            <xsl:when test="$nationality = 	166">CCK</xsl:when>
            <xsl:when test="$nationality = 	170">COL</xsl:when>
            <xsl:when test="$nationality = 	174">COM</xsl:when>
            <xsl:when test="$nationality = 	175">MYT</xsl:when>
            <xsl:when test="$nationality = 	178">COG</xsl:when>
            <xsl:when test="$nationality = 	180">COD</xsl:when>
            <xsl:when test="$nationality = 	184">COK</xsl:when>
            <xsl:when test="$nationality = 	188">CR</xsl:when>
            <xsl:when test="$nationality = 	191">HRV</xsl:when>
            <xsl:when test="$nationality = 	192">CUB</xsl:when>
            <xsl:when test="$nationality = 	196">CYP</xsl:when>
            <xsl:when test="$nationality = 	203">CZE</xsl:when>
            <xsl:when test="$nationality = 	204">BEN</xsl:when>
            <xsl:when test="$nationality = 	208">DNK</xsl:when>
            <xsl:when test="$nationality = 	212">DMA</xsl:when>
            <xsl:when test="$nationality = 	214">DOM</xsl:when>
            <xsl:when test="$nationality = 	218">ECU</xsl:when>
            <xsl:when test="$nationality = 	222">SLV</xsl:when>
            <xsl:when test="$nationality = 	226">GNQ</xsl:when>
            <xsl:when test="$nationality = 	231">ETH</xsl:when>
            <xsl:when test="$nationality = 	232">ERI</xsl:when>
            <xsl:when test="$nationality = 	233">EST</xsl:when>
            <xsl:when test="$nationality = 	234">FRO</xsl:when>
            <xsl:when test="$nationality = 	238">FLK</xsl:when>
            <xsl:when test="$nationality = 	239">SGS</xsl:when>
            <xsl:when test="$nationality = 	242">FJI</xsl:when>
            <xsl:when test="$nationality = 	246">FIN</xsl:when>
            <xsl:when test="$nationality = 	248">ALA</xsl:when>
            <xsl:when test="$nationality = 	250">FRA</xsl:when>
            <xsl:when test="$nationality = 	254">GUF</xsl:when>
            <xsl:when test="$nationality = 	258">PYF</xsl:when>
            <xsl:when test="$nationality = 	260">ATF</xsl:when>
            <xsl:when test="$nationality = 	262">DJI</xsl:when>
            <xsl:when test="$nationality = 	266">GAB</xsl:when>
            <xsl:when test="$nationality = 	268">GEO</xsl:when>
            <xsl:when test="$nationality = 	270">GMB</xsl:when>
            <xsl:when test="$nationality = 	275">PST</xsl:when>
            <xsl:when test="$nationality = 	276">DEU</xsl:when>
            <xsl:when test="$nationality = 	288">GHA</xsl:when>
            <xsl:when test="$nationality = 	292">GIB</xsl:when>
            <xsl:when test="$nationality = 	296">KIR</xsl:when>
            <xsl:when test="$nationality = 	300">GRC</xsl:when>
            <xsl:when test="$nationality = 	304">GRL</xsl:when>
            <xsl:when test="$nationality = 	308">GRD</xsl:when>
            <xsl:when test="$nationality = 	312">GLP</xsl:when>
            <xsl:when test="$nationality = 	316">GUM</xsl:when>
            <xsl:when test="$nationality = 	320">GTM</xsl:when>
            <xsl:when test="$nationality = 	324">GIN</xsl:when>
            <xsl:when test="$nationality = 	328">GUY</xsl:when>
            <xsl:when test="$nationality = 	332">HTI</xsl:when>
            <xsl:when test="$nationality = 	334">HMD</xsl:when>
            <xsl:when test="$nationality = 	336">VAT</xsl:when>
            <xsl:when test="$nationality = 	340">HND</xsl:when>
            <xsl:when test="$nationality = 	344">HKG</xsl:when>
            <xsl:when test="$nationality = 	348">HUN</xsl:when>
            <xsl:when test="$nationality = 	352">ISL</xsl:when>
            <xsl:when test="$nationality = 	356">IND</xsl:when>
            <xsl:when test="$nationality = 	360">IDN</xsl:when>
            <xsl:when test="$nationality = 	364">IRN</xsl:when>
            <xsl:when test="$nationality = 	368">IRQ</xsl:when>
            <xsl:when test="$nationality = 	372">IRL</xsl:when>
            <xsl:when test="$nationality = 	376">ISR</xsl:when>
            <xsl:when test="$nationality = 	380">ITA</xsl:when>
            <xsl:when test="$nationality = 	384">CIV</xsl:when>
            <xsl:when test="$nationality = 	388">JAM</xsl:when>
            <xsl:when test="$nationality = 	392">JPN</xsl:when>
            <xsl:when test="$nationality = 	398">KAZ</xsl:when>
            <xsl:when test="$nationality = 	400">JOR</xsl:when>
            <xsl:when test="$nationality = 	404">KEN</xsl:when>
            <xsl:when test="$nationality = 	408">PRK</xsl:when>
            <xsl:when test="$nationality = 	410">KOR</xsl:when>
            <xsl:when test="$nationality = 	414">KWT</xsl:when>
            <xsl:when test="$nationality = 	417">KGZ</xsl:when>
            <xsl:when test="$nationality = 	418">LAO</xsl:when>
            <xsl:when test="$nationality = 	422">LBN</xsl:when>
            <xsl:when test="$nationality = 	426">LSO</xsl:when>
            <xsl:when test="$nationality = 	428">LVA</xsl:when>
            <xsl:when test="$nationality = 	430">LBR</xsl:when>
            <xsl:when test="$nationality = 	434">LBY</xsl:when>
            <xsl:when test="$nationality = 	438">LIE</xsl:when>
            <xsl:when test="$nationality = 	440">LTU</xsl:when>
            <xsl:when test="$nationality = 	442">LUX</xsl:when>
            <xsl:when test="$nationality = 	446">MAC</xsl:when>
            <xsl:when test="$nationality = 	450">MDG</xsl:when>
            <xsl:when test="$nationality = 	454">MWI</xsl:when>
            <xsl:when test="$nationality = 	458">MYS</xsl:when>
            <xsl:when test="$nationality = 	462">MDV</xsl:when>
            <xsl:when test="$nationality = 	466">MLI</xsl:when>
            <xsl:when test="$nationality = 	470">MLT</xsl:when>
            <xsl:when test="$nationality = 	474">MTQ</xsl:when>
            <xsl:when test="$nationality = 	478">MRT</xsl:when>
            <xsl:when test="$nationality = 	480">MUS</xsl:when>
            <xsl:when test="$nationality = 	484">MEX</xsl:when>
            <xsl:when test="$nationality = 	492">MCO</xsl:when>
            <xsl:when test="$nationality = 	496">MNG</xsl:when>
            <xsl:when test="$nationality = 	498">MDA</xsl:when>
            <xsl:when test="$nationality = 	499">MNE</xsl:when>
            <xsl:when test="$nationality = 	500">MSR</xsl:when>
            <xsl:when test="$nationality = 	504">MAR</xsl:when>
            <xsl:when test="$nationality = 	508">MOZ</xsl:when>
            <xsl:when test="$nationality = 	512">OMN</xsl:when>
            <xsl:when test="$nationality = 	516">NAM</xsl:when>
            <xsl:when test="$nationality = 	520">NRU</xsl:when>
            <xsl:when test="$nationality = 	524">NPL</xsl:when>
            <xsl:when test="$nationality = 	528">NLD</xsl:when>
            <xsl:when test="$nationality = 	530">ANT</xsl:when>
            <xsl:when test="$nationality = 	531">OTH</xsl:when>
            <xsl:when test="$nationality = 	533">ABW</xsl:when>
            <xsl:when test="$nationality = 	534">OTH</xsl:when>
            <xsl:when test="$nationality = 	535">OTH</xsl:when>
            <xsl:when test="$nationality = 	540">NCL</xsl:when>
            <xsl:when test="$nationality = 	548">VUT</xsl:when>
            <xsl:when test="$nationality = 	554">NZL</xsl:when>
            <xsl:when test="$nationality = 	558">NIC</xsl:when>
            <xsl:when test="$nationality = 	562">NER</xsl:when>
            <xsl:when test="$nationality = 	566">NGA</xsl:when>
            <xsl:when test="$nationality = 	570">NIU</xsl:when>
            <xsl:when test="$nationality = 	574">NFK</xsl:when>
            <xsl:when test="$nationality = 	578">NOR</xsl:when>
            <xsl:when test="$nationality = 	580">MNP</xsl:when>
            <xsl:when test="$nationality = 	581">UMI</xsl:when>
            <xsl:when test="$nationality = 	583">FSM</xsl:when>
            <xsl:when test="$nationality = 	584">MHL</xsl:when>
            <xsl:when test="$nationality = 	585">PLW</xsl:when>
            <xsl:when test="$nationality = 	586">PAK</xsl:when>
            <xsl:when test="$nationality = 	591">PAN</xsl:when>
            <xsl:when test="$nationality = 	598">PNG</xsl:when>
            <xsl:when test="$nationality = 	600">PRY</xsl:when>
            <xsl:when test="$nationality = 	604">PER</xsl:when>
            <xsl:when test="$nationality = 	608">PHL</xsl:when>
            <xsl:when test="$nationality = 	612">PCN</xsl:when>
            <xsl:when test="$nationality = 	616">POL</xsl:when>
            <xsl:when test="$nationality = 	620">PRT</xsl:when>
            <xsl:when test="$nationality = 	624">GNB</xsl:when>
            <xsl:when test="$nationality = 	626">TMP</xsl:when>
            <xsl:when test="$nationality = 	630">PRI</xsl:when>
            <xsl:when test="$nationality = 	634">QAT</xsl:when>
            <xsl:when test="$nationality = 	638">REU</xsl:when>
            <xsl:when test="$nationality = 	642">ROM</xsl:when>
            <xsl:when test="$nationality = 	643">RUS</xsl:when>
            <xsl:when test="$nationality = 	646">RWA</xsl:when>
            <xsl:when test="$nationality = 	652">BLM</xsl:when>
            <xsl:when test="$nationality = 	654">SHN</xsl:when>
            <xsl:when test="$nationality = 	659">KNA</xsl:when>
            <xsl:when test="$nationality = 	660">AIA</xsl:when>
            <xsl:when test="$nationality = 	662">LCA</xsl:when>
            <xsl:when test="$nationality = 	663">MAF</xsl:when>
            <xsl:when test="$nationality = 	666">SPM</xsl:when>
            <xsl:when test="$nationality = 	670">VCT</xsl:when>
            <xsl:when test="$nationality = 	674">SMR</xsl:when>
            <xsl:when test="$nationality = 	678">STp</xsl:when>
            <xsl:when test="$nationality = 	682">SAU</xsl:when>
            <xsl:when test="$nationality = 	686">SEN</xsl:when>
            <xsl:when test="$nationality = 	688">SRB</xsl:when>
            <xsl:when test="$nationality = 	690">SYC</xsl:when>
            <xsl:when test="$nationality = 	694">SLE</xsl:when>
            <xsl:when test="$nationality = 	702">SGP</xsl:when>
            <xsl:when test="$nationality = 	703">SVK</xsl:when>
            <xsl:when test="$nationality = 	704">VNM</xsl:when>
            <xsl:when test="$nationality = 	705">SVN</xsl:when>
            <xsl:when test="$nationality = 	706">SOM</xsl:when>
            <xsl:when test="$nationality = 	710">ZAF</xsl:when>
            <xsl:when test="$nationality = 	716">ZWE</xsl:when>
            <xsl:when test="$nationality = 	724">ESP</xsl:when>
            <xsl:when test="$nationality = 	728">SSD</xsl:when>
            <xsl:when test="$nationality = 	729">SDN</xsl:when>
            <xsl:when test="$nationality = 	732">ESH</xsl:when>
            <xsl:when test="$nationality = 	736">OTH</xsl:when>
            <xsl:when test="$nationality = 	740">SUR</xsl:when>
            <xsl:when test="$nationality = 	744">SJM</xsl:when>
            <xsl:when test="$nationality = 	748">SWZ</xsl:when>
            <xsl:when test="$nationality = 	752">SWE</xsl:when>
            <xsl:when test="$nationality = 	756">CHE</xsl:when>
            <xsl:when test="$nationality = 	760">SYR</xsl:when>
            <xsl:when test="$nationality = 	762">TJK</xsl:when>
            <xsl:when test="$nationality = 	764">THA</xsl:when>
            <xsl:when test="$nationality = 	768">TGO</xsl:when>
            <xsl:when test="$nationality = 	772">TKL</xsl:when>
            <xsl:when test="$nationality = 	776">TON</xsl:when>
            <xsl:when test="$nationality = 	780">TTO</xsl:when>
            <xsl:when test="$nationality = 	784">ARE</xsl:when>
            <xsl:when test="$nationality = 	788">TUN</xsl:when>
            <xsl:when test="$nationality = 	792">TUR</xsl:when>
            <xsl:when test="$nationality = 	795">TKM</xsl:when>
            <xsl:when test="$nationality = 	796">TCA</xsl:when>
            <xsl:when test="$nationality = 	798">TUV</xsl:when>
            <xsl:when test="$nationality = 	800">UGA</xsl:when>
            <xsl:when test="$nationality = 	804">UKR</xsl:when>
            <xsl:when test="$nationality = 	807">MKD</xsl:when>
            <xsl:when test="$nationality = 	818">EGY</xsl:when>
            <xsl:when test="$nationality = 	826">GBR</xsl:when>
            <xsl:when test="$nationality = 	831">GGY</xsl:when>
            <xsl:when test="$nationality = 	832">OTH</xsl:when>
            <xsl:when test="$nationality = 	833">IMN</xsl:when>
            <xsl:when test="$nationality = 	834">TZA</xsl:when>
            <xsl:when test="$nationality = 	840">USA</xsl:when>
            <xsl:when test="$nationality = 	850">VIR</xsl:when>
            <xsl:when test="$nationality = 	854">BFA</xsl:when>
            <xsl:when test="$nationality = 	858">URY</xsl:when>
            <xsl:when test="$nationality = 	860">UZB</xsl:when>
            <xsl:when test="$nationality = 	862">VEN</xsl:when>
            <xsl:when test="$nationality = 	876">WLF</xsl:when>
            <xsl:when test="$nationality = 	882">WSM</xsl:when>
            <xsl:when test="$nationality = 	887">YEM</xsl:when>
            <xsl:when test="$nationality = 	891">YUG</xsl:when>
            <xsl:when test="$nationality = 	894">ZMB</xsl:when>
            <xsl:when test="$nationality = 	998">OTH</xsl:when>
            <xsl:when test="$nationality = 	999">OTH</xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="''"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <!-- 职业类别 -->
    <xsl:template name="tran_idjobCode">
        <xsl:param name="idjobCode"></xsl:param>
        <xsl:if test="$idjobCode = 01">0101002</xsl:if><!-- 农夫 -->
        <xsl:if test="$idjobCode = 02">0001003</xsl:if><!-- 企\事业单位负责人 -->
        <xsl:if test="$idjobCode = 03">0001001</xsl:if><!-- 机关团体公司行号内勤人员 -->
        <xsl:if test="$idjobCode = 04">0101008</xsl:if><!-- xxx技术人员 -->
        <xsl:if test="$idjobCode = 05">0501008</xsl:if><!-- 客运车司机及服务员 -->
        <xsl:if test="$idjobCode = 06">1202002</xsl:if><!-- 商业店员 -->
        <xsl:if test="$idjobCode = 07">2147004</xsl:if><!-- 无业人员 -->
    </xsl:template>

    <!-- 渠道 -->
    <xsl:template name="tran_Type">
        <xsl:param name="Type"></xsl:param>
        <xsl:if test="$Type = 11">ybt</xsl:if>
        <xsl:if test="$Type = 01">wy</xsl:if>
        <xsl:if test="$Type = 04">atm</xsl:if>
        <xsl:if test="$Type = 02">zy</xsl:if>
    </xsl:template>

    <!-- 健康告知代码转换 -->
    <xsl:template name="tran_healthcode">
        <!-- Y 有健康告知，拒保，N可以正常投保 -->
        <xsl:param name="healthCode"/>
        <xsl:choose>
            <xsl:when test="$healthCode='1'">
                <xsl:value-of select="'Y'"/>
            </xsl:when>
            <xsl:when test="$healthCode='0'">
                <xsl:value-of select="'N'"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="''"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    <xsl:template name="tran_healthcode1">
        <!-- Y 有健康告知，拒保，N可以正常投保 -->
        <xsl:param name="healthCode"/>
        <xsl:choose>
            <xsl:when test="$healthCode='1'">
                <xsl:value-of select="'Y'"/>
            </xsl:when>
            <xsl:when test="$healthCode='0'">
                <xsl:value-of select="'N'"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="'N'"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>


    <xsl:template name="tran_appRela2InsuRela">
        <xsl:param name="sex"/>
        <xsl:param name="relaToInsured"/>
        <xsl:if test="$sex=0">
            <xsl:if test="$relaToInsured = 00">00</xsl:if>
            <xsl:if test="$relaToInsured = 01">00</xsl:if>
            <xsl:if test="$relaToInsured = 02">02</xsl:if>
            <xsl:if test="$relaToInsured = 03">01</xsl:if>
            <xsl:if test="$relaToInsured = 04">05</xsl:if>
            <xsl:if test="$relaToInsured = 05">05</xsl:if>
            <xsl:if test="$relaToInsured = 06">03</xsl:if>
            <xsl:if test="$relaToInsured = 07">03</xsl:if>
            <xsl:if test="$relaToInsured = 08">09</xsl:if>
            <xsl:if test="$relaToInsured = 09">09</xsl:if>
            <xsl:if test="$relaToInsured = 10">07</xsl:if>
            <xsl:if test="$relaToInsured = 11">07</xsl:if>
            <xsl:if test="$relaToInsured = 12">13</xsl:if>
            <xsl:if test="$relaToInsured = 13">13</xsl:if>
            <xsl:if test="$relaToInsured = 14">11</xsl:if>
            <xsl:if test="$relaToInsured = 15">11</xsl:if>
            <xsl:if test="$relaToInsured = 16">17</xsl:if>
            <xsl:if test="$relaToInsured = 17">17</xsl:if>
            <xsl:if test="$relaToInsured = 18">15</xsl:if>
            <xsl:if test="$relaToInsured = 19">15</xsl:if>
            <xsl:if test="$relaToInsured = 20">""</xsl:if>
            <xsl:if test="$relaToInsured = 21">""</xsl:if>
            <xsl:if test="$relaToInsured = 22">24</xsl:if>
            <xsl:if test="$relaToInsured = 23">24</xsl:if>
            <xsl:if test="$relaToInsured = 24">19</xsl:if>
            <xsl:if test="$relaToInsured = 25">21</xsl:if>
            <xsl:if test="$relaToInsured = 26">25</xsl:if>
            <xsl:if test="$relaToInsured = 27">26</xsl:if>
            <xsl:if test="$relaToInsured = 28">27</xsl:if>
            <xsl:if test="$relaToInsured = 29">29</xsl:if>
            <xsl:if test="$relaToInsured = 30">28</xsl:if>
        </xsl:if>
        <xsl:if test="$sex=1">
            <xsl:if test="$relaToInsured = 01">00</xsl:if>
            <xsl:if test="$relaToInsured = 02">02</xsl:if>
            <xsl:if test="$relaToInsured = 03">01</xsl:if>
            <xsl:if test="$relaToInsured = 04">06</xsl:if>
            <xsl:if test="$relaToInsured = 05">06</xsl:if>
            <xsl:if test="$relaToInsured = 06">04</xsl:if>
            <xsl:if test="$relaToInsured = 07">04</xsl:if>
            <xsl:if test="$relaToInsured = 08">10</xsl:if>
            <xsl:if test="$relaToInsured = 09">10</xsl:if>
            <xsl:if test="$relaToInsured = 10">08</xsl:if>
            <xsl:if test="$relaToInsured = 11">08</xsl:if>
            <xsl:if test="$relaToInsured = 12">14</xsl:if>
            <xsl:if test="$relaToInsured = 13">14</xsl:if>
            <xsl:if test="$relaToInsured = 14">12</xsl:if>
            <xsl:if test="$relaToInsured = 15">12</xsl:if>
            <xsl:if test="$relaToInsured = 16">18</xsl:if>
            <xsl:if test="$relaToInsured = 17">18</xsl:if>
            <xsl:if test="$relaToInsured = 18">16</xsl:if>
            <xsl:if test="$relaToInsured = 19">16</xsl:if>
            <xsl:if test="$relaToInsured = 20">23</xsl:if>
            <xsl:if test="$relaToInsured = 21">23</xsl:if>
            <xsl:if test="$relaToInsured = 22">20</xsl:if>
            <xsl:if test="$relaToInsured = 23">""</xsl:if>
            <xsl:if test="$relaToInsured = 24">""</xsl:if>
            <xsl:if test="$relaToInsured = 25">22</xsl:if>
            <xsl:if test="$relaToInsured = 26">25</xsl:if>
            <xsl:if test="$relaToInsured = 27">26</xsl:if>
            <xsl:if test="$relaToInsured = 28">27</xsl:if>
            <xsl:if test="$relaToInsured = 29">29</xsl:if>
            <xsl:if test="$relaToInsured = 30">30</xsl:if>
        </xsl:if>
        <xsl:if test="$sex=''">传入未知的被保人性别！</xsl:if>
    </xsl:template>

    <!-- 投保人与被保人关系转换 -->
    <xsl:template name="tran_rela">
        <xsl:param name="rela">00</xsl:param>
        <xsl:if test="$rela = 01">00</xsl:if>
        <xsl:if test="$rela = 02">01</xsl:if>
        <xsl:if test="$rela = 03">02</xsl:if>
        <xsl:if test="$rela = 04">03</xsl:if>
        <xsl:if test="$rela = 05">04</xsl:if>
        <xsl:if test="$rela = 06">05</xsl:if>
        <xsl:if test="$rela = 07">06</xsl:if>
        <xsl:if test="$rela = 08">07</xsl:if>
        <xsl:if test="$rela = 09">08</xsl:if>
        <xsl:if test="$rela = 10">09</xsl:if>
        <xsl:if test="$rela = 11">10</xsl:if>
        <xsl:if test="$rela = 12">11</xsl:if>
        <xsl:if test="$rela = 13">12</xsl:if>
        <xsl:if test="$rela = 14">13</xsl:if>
        <xsl:if test="$rela = 15">14</xsl:if>
        <xsl:if test="$rela = 16">15</xsl:if>
        <xsl:if test="$rela = 17">16</xsl:if>
        <xsl:if test="$rela = 18">17</xsl:if>
        <xsl:if test="$rela = 19">18</xsl:if>
        <xsl:if test="$rela = 20">19</xsl:if>
        <xsl:if test="$rela = 21">20</xsl:if>
        <xsl:if test="$rela = 22">23</xsl:if>
        <xsl:if test="$rela = 23">21</xsl:if>
        <xsl:if test="$rela = 24">22</xsl:if>
        <xsl:if test="$rela = 25">24</xsl:if>
        <xsl:if test="$rela = 26">25</xsl:if>
        <xsl:if test="$rela = 27">26</xsl:if>
        <xsl:if test="$rela = 28">27</xsl:if>
        <xsl:if test="$rela = 29">28</xsl:if>
        <xsl:if test="$rela = 30">30</xsl:if>
    </xsl:template>

    <!-- 起领年期标志转换 -->
    <xsl:template name="tran_getyearflag">
        <xsl:param name="getyearflag"/>
        <xsl:choose>
            <xsl:when test="$getyearflag=1">A</xsl:when>
            <xsl:when test="$getyearflag=0">Y</xsl:when>
        </xsl:choose>
    </xsl:template>

    <!-- liveaccflag,livegetmode,1累计生息,2现金领取 -->
    <xsl:template name="tran_livegetmode">
        <xsl:param name="livegetmode"/>
        <xsl:choose>
            <xsl:when test="$livegetmode=1">4</xsl:when>
            <xsl:when test="$livegetmode=2">1</xsl:when>
            <xsl:when test="$livegetmode=''">4</xsl:when>
        </xsl:choose>
    </xsl:template>

    <!-- 居民居住类型转换 -->
    <xsl:template name="tran_appRgtType">
        <xsl:param name="appntRgtType"/>
        <xsl:choose>
            <xsl:when test="$appntRgtType=0">1</xsl:when>
            <xsl:when test="$appntRgtType=1">2</xsl:when>
        </xsl:choose>
    </xsl:template>

    <!-- 职业类型转 -->
    <xsl:template name="tran_occupationType">
        <xsl:param name="occupationType"/>
        <xsl:param name="riskcode"/>
        <xsl:choose>
            <!-- 个险处理-->
            <xsl:when test="$occupationType=01 and ($riskcode !='6810'and $riskcode !='6807')">2</xsl:when>
            <xsl:when test="$occupationType=02 and ($riskcode !='6810'and $riskcode !='6807')">1</xsl:when>
            <xsl:when test="$occupationType=03 and ($riskcode !='6810'and $riskcode !='6807')">2</xsl:when>
            <xsl:when test="$occupationType=04 and ($riskcode !='6810'and $riskcode !='6807')">2</xsl:when>
            <xsl:when test="$occupationType=05 and ($riskcode !='6810'and $riskcode !='6807')">4</xsl:when>
            <xsl:when test="$occupationType=06 and ($riskcode !='6810'and $riskcode !='6807')">3</xsl:when>
            <xsl:when test="$occupationType=07 and ($riskcode !='6810'and $riskcode !='6807')">5</xsl:when>
            <!-- 团险处理-->
            <xsl:when test="$occupationType=01 and ($riskcode ='6810'or $riskcode ='6807')">2</xsl:when>
            <xsl:when test="$occupationType=02 and ($riskcode ='6810'or $riskcode ='6807')">1</xsl:when>
            <xsl:when test="$occupationType=03 and ($riskcode ='6810'or $riskcode ='6807')">2</xsl:when>
            <xsl:when test="$occupationType=04 and ($riskcode ='6810'or $riskcode ='6807')">2</xsl:when>
            <xsl:when test="$occupationType=05 and ($riskcode ='6810'or $riskcode ='6807')">4</xsl:when>
            <xsl:when test="$occupationType=06 and ($riskcode ='6810'or $riskcode ='6807')">2</xsl:when>
            <xsl:when test="$occupationType=07 and ($riskcode ='6810'or $riskcode ='6807')">1</xsl:when>

        </xsl:choose>
    </xsl:template>

    <!-- 职业代码转换 -->
    <xsl:template name="tran_occupationCode">
        <xsl:param name="occupationCode"/>
        <xsl:param name="riskcode"/>
        <xsl:choose>
            <xsl:when test="$occupationCode=01 and ($riskcode !='6810'and $riskcode !='6807')">0101002</xsl:when>
            <xsl:when test="$occupationCode=02 and ($riskcode !='6810'and $riskcode !='6807')">0001003</xsl:when>
            <xsl:when test="$occupationCode=03 and ($riskcode !='6810'and $riskcode !='6807')">0501002</xsl:when>
            <xsl:when test="$occupationCode=04 and ($riskcode !='6810'and $riskcode !='6807')">1402010</xsl:when>
            <xsl:when test="$occupationCode=05 and ($riskcode !='6810'and $riskcode !='6807')">0501010</xsl:when>
            <xsl:when test="$occupationCode=06 and ($riskcode !='6810'and $riskcode !='6807')">0501008</xsl:when>
            <xsl:when test="$occupationCode=07 and ($riskcode !='6810'and $riskcode !='6807')">0816018</xsl:when>

            <xsl:when test="$occupationCode=01 and ($riskcode ='6810'or $riskcode ='6807')">0101002</xsl:when>
            <xsl:when test="$occupationCode=02 and ($riskcode ='6810'or $riskcode ='6807')">0001003</xsl:when>
            <xsl:when test="$occupationCode=03 and ($riskcode ='6810'or $riskcode ='6807')">0001002</xsl:when>
            <xsl:when test="$occupationCode=04 and ($riskcode ='6810'or $riskcode ='6807')">0602005</xsl:when>
            <xsl:when test="$occupationCode=05 and ($riskcode ='6810'or $riskcode ='6807')">0802007</xsl:when>
            <xsl:when test="$occupationCode=06 and ($riskcode ='6810'or $riskcode ='6807')">0603007</xsl:when>
            <xsl:when test="$occupationCode=07 and ($riskcode ='6810'or $riskcode ='6807')">9900000</xsl:when>
        </xsl:choose>
    </xsl:template>

    <!-- 红利领取方式 -->
    <xsl:template name="tran_getMode">
        <xsl:param name="bonusGetMode">0</xsl:param>
        <xsl:if test="$bonusGetMode = 0">2</xsl:if>
        <xsl:if test="$bonusGetMode = 1">3</xsl:if>
        <xsl:if test="$bonusGetMode = 2">1</xsl:if>
        <xsl:if test="$bonusGetMode = 3">4</xsl:if>
    </xsl:template>

    <xsl:template name="tran_riskCode">
        <xsl:param name="riskCode"/>
        <xsl:choose>
            <xsl:when test="$riskCode=2024">2036</xsl:when>
            <xsl:when test="$riskCode=1015">1020</xsl:when>
            <xsl:when test="$riskCode='C026'">5010</xsl:when>
            <xsl:when test="$riskCode='6810a'">6810</xsl:when>
            <xsl:when test="$riskCode='6810b'">6810</xsl:when>
            <xsl:when test="$riskCode='6810c'">6810</xsl:when>
            <xsl:when test="$riskCode='6810d'">6810</xsl:when>
            <!--<xsl:when test="$riskCode='SEO'">6810</xsl:when>
            <xsl:when test="$riskCode='SEL'">6810</xsl:when>
            <xsl:when test="$riskCode='SEM'">6810</xsl:when>-->
            <xsl:otherwise>
                <xsl:value-of select="$riskCode"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template name="tran_newriskCode">
        <xsl:param name="riskCode"/>
        <xsl:choose>
            <xsl:when test="$riskCode=2024">2036</xsl:when>
            <xsl:when test="$riskCode=1015">1020</xsl:when>
            <xsl:when test="$riskCode='C026'">5010</xsl:when>
            <xsl:when test="$riskCode='6810a'">6810</xsl:when>
            <xsl:when test="$riskCode='6810b'">6810</xsl:when>
            <xsl:when test="$riskCode='6810c'">6810</xsl:when>
            <xsl:when test="$riskCode='6810d'">6810</xsl:when>
            <xsl:when test="$riskCode='C066'">1034</xsl:when>
            <xsl:when test="$riskCode='C067'">1037</xsl:when>
            <xsl:when test="$riskCode='C068'">1038</xsl:when>
            <xsl:when test="$riskCode='C072'">7065</xsl:when>
            <!--<xsl:when test="$riskCode='SEO'">6810</xsl:when>
            <xsl:when test="$riskCode='SEL'">6810</xsl:when>
            <xsl:when test="$riskCode='SEM'">6810</xsl:when>-->
            <xsl:otherwise>
                <xsl:value-of select="$riskCode"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template name="tran_mainriskCode">
        <xsl:param name="riskCode"/>
        <xsl:choose>
            <xsl:when test="$riskCode=2024">2036</xsl:when>
            <xsl:when test="$riskCode=1015">1020</xsl:when>
            <xsl:when test="$riskCode='C026'">5010</xsl:when>
            <xsl:when test="$riskCode='6810a'">6810</xsl:when>
            <xsl:when test="$riskCode='6810b'">6810</xsl:when>
            <xsl:when test="$riskCode='6810c'">6810</xsl:when>
            <xsl:when test="$riskCode='6810d'">6810</xsl:when>
            <xsl:when test="$riskCode='C066'">5022</xsl:when>
            <xsl:when test="$riskCode='C067'">5022</xsl:when>
            <xsl:when test="$riskCode='C068'">5022</xsl:when>
            <xsl:when test="$riskCode='C072'">1041</xsl:when>
            <xsl:when test="$riskCode='C071'">7063</xsl:when>
            <!--<xsl:when test="$riskCode='SEO'">6810</xsl:when>
            <xsl:when test="$riskCode='SEL'">6810</xsl:when>
            <xsl:when test="$riskCode='SEM'">6810</xsl:when>-->
            <xsl:otherwise>
                <xsl:value-of select="$riskCode"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template name="tran_Types">
        <xsl:param name="Type"/>
        <xsl:if test="$Type = 11">ABCOTC</xsl:if><!--柜面-->
        <xsl:if test="$Type = 01">ABCEBANK</xsl:if><!--网银-->
        <xsl:if test="$Type = 04">ABCSELF</xsl:if><!--自助终端-->
        <xsl:if test="$Type = 02">ABCMOBILE</xsl:if><!--掌银-->
        <xsl:if test="$Type = 22">ABCSUPOTC</xsl:if><!--超贵-->
    </xsl:template>

    <!-- 满期领取金的领取方式 -->
    <xsl:template name="tran_getIntv">
        <xsl:param name="riskCode"/>
        <xsl:param name="getIntv"/>
        <!-- 把对应生存金领取方式字段都改为统一的“一次性领”而不去取xxx发来的领取方式字段 -->
        <!-- <xsl:variable name="getIntv" select="'0'"></xsl:variable> -->
        <xsl:choose>
            <xsl:when test="$riskCode ='2020' and $getIntv='1'">
                12
            </xsl:when>
            <xsl:when test="$riskCode ='2020' and $getIntv='12'">
                1
            </xsl:when>
            <xsl:otherwise>
                <xsl:if test="$getIntv = 4">12</xsl:if>
                <xsl:if test="$getIntv = 1">1</xsl:if>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <!--证件有效期长期转换-->
    <xsl:template name="tran_idValidate">
        <xsl:param name="idValidate"/>
        <xsl:choose>
            <xsl:when test="$idValidate = 99991231">长期</xsl:when>
            <xsl:when test="$idValidate = 99999999">长期</xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="$idValidate"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <!-- 保额转换 -->
    <xsl:template name="tran_amnt">
        <xsl:param name="riskCode"/>
        <xsl:param name="amnt"></xsl:param>
        <xsl:choose>
            <xsl:when test="$riskCode=1022">0</xsl:when>
            <xsl:when test="$riskCode=2036">0</xsl:when>
            <xsl:when test="$riskCode=2044">0</xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="$amnt"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <!-- 自动续保标记 -->
    <xsl:template name="tran_renew">
        <xsl:param name="flag"/>
        <xsl:if test="$flag = '1'">-1</xsl:if>
        <xsl:if test="$flag = '0'">0</xsl:if>
        <xsl:if test="$flag = ''">-9</xsl:if><!-- -9代表xxx没有录入自动续保标记 -->
    </xsl:template>

    <xsl:template name="tran_liveaccflag">
        <xsl:param name="liveaccflag"/>
        <xsl:choose>
            <xsl:when test="$liveaccflag=1">0</xsl:when>
            <xsl:when test="$liveaccflag=4">1</xsl:when>
            <xsl:when test="$liveaccflag=''">1</xsl:when>
        </xsl:choose>
    </xsl:template>
    <!-- 销售方式 -->
    <xsl:template name="tran_sellType">
        <xsl:param name="sellType"></xsl:param>
        <xsl:if test="$sellType = 11">08</xsl:if>
        <xsl:if test="$sellType = 01">22</xsl:if>
        <xsl:if test="$sellType = 04">24</xsl:if>
        <xsl:if test="$sellType = 02">25</xsl:if>
        <xsl:if test="$sellType = 22">27</xsl:if>
        <xsl:if test="$sellType = 08">26</xsl:if>
    </xsl:template>
    <xsl:template name="tran_autoRenewAge">
        <xsl:param name="autoRenewAge"></xsl:param>
        <xsl:if test="$autoRenewAge = 1">17</xsl:if>
        <xsl:if test="$autoRenewAge = 2">70</xsl:if>
    </xsl:template>
    <xsl:template name="tran_LoanerNature">
        <xsl:param name="loanerNature"></xsl:param>
        <xsl:if test="$loanerNature = 1">1</xsl:if>
        <xsl:if test="$loanerNature = 2">2</xsl:if>
        <xsl:if test="$loanerNature = ''">1</xsl:if>
    </xsl:template>
    <xsl:template name="tran_lccontprodsetcode">
        <xsl:param name="riskCode"/>
        <xsl:choose>
            <xsl:when test="$riskCode='C066'">C066</xsl:when>
            <xsl:when test="$riskCode='C067'">C067</xsl:when>
            <xsl:when test="$riskCode='C068'">C068</xsl:when>
            <xsl:when test="$riskCode='C072'">C072</xsl:when>
            <xsl:when test="$riskCode='7063'">C071</xsl:when>
        </xsl:choose>
    </xsl:template>
    <xsl:template name="addLCAddress">
        <xsl:param name="count"/>
        <xsl:param name="position" select="1"/>
        <xsl:if test="$count>0">
            <xsl:variable name="address" select="concat('Address',$position)"/>
            <xsl:variable name="phone" select="concat('Phone',$position)"/>
            <xsl:variable name="prov" select="concat('Prov',$position)"/>
            <xsl:variable name="city" select="concat('City',$position)"/>
            <xsl:variable name="zone" select="concat('Zone',$position)"/>
            <LCAddress>
                <PostalAddress>
                    <xsl:value-of select="RequestNodes/RequestNode/Bnfs/*[name()=$address]"/>
                </PostalAddress>
                <HomeAddress>
                    <xsl:value-of select="RequestNodes/RequestNode/Bnfs/*[name()=$address]"/>
                </HomeAddress>
                <Mobile>
                    <xsl:value-of select="RequestNodes/RequestNode/Bnfs/*[name()=$phone]"/>
                </Mobile>
                <Province>
                    <xsl:value-of select="RequestNodes/RequestNode/Bnfs/*[name()=$prov]"/>
                </Province><!-- 省 -->
                <City>
                    <xsl:value-of select="RequestNodes/RequestNode/Bnfs/*[name()=$city]"/>
                </City><!-- 市 -->
                <County>
                    <xsl:value-of select="RequestNodes/RequestNode/Bnfs/*[name()=$zone]"/>
                </County><!-- 区 -->
            </LCAddress>
            <xsl:call-template name="addLCAddress">
                <xsl:with-param name="count">
                    <xsl:value-of select="$count - 1"/>
                </xsl:with-param>
                <xsl:with-param name="position">
                    <xsl:value-of select="$position + 1"/>
                </xsl:with-param>
            </xsl:call-template>
        </xsl:if>
    </xsl:template>

</xsl:stylesheet>

