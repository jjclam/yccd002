<?xml version="1.0" encoding="GBK"?>

<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:java="http://xml.apache.org/xslt/java"
                exclude-result-prefixes="java">
    <xsl:template match="ABCB2I">
        <TranData>
            <Body>
                <LKTransTrackses>
                    <LKTransTracks>
                        <!--交易流水号-->
                        <TransNo>
                            <xsl:value-of select="Header/SerialNo"/>
                        </TransNo>
                        <!--交易编码-->
                        <TransCode>XQ002</TransCode>
                        <!--原交易编码-->
                        <TransCodeOri>
                            <xsl:value-of select="Header/TransCode"/>
                        </TransCodeOri>
                        <!--交易子渠道-->
                        <AccessChnlSub>
                            <xsl:value-of select="Header/EntrustWay"/>
                        </AccessChnlSub>
                        <xsl:if test="Header/EntrustWay = 11">
                            <BankCode>05</BankCode>
                        </xsl:if>
                        <xsl:if test="Header/EntrustWay != 11">
                            <BankCode>0501</BankCode>
                        </xsl:if>
                        <BankBranch>
                            <xsl:value-of select="Header/ProvCode"/>
                        </BankBranch>
                        <BankNode>
                            <xsl:value-of select="Header/BranchNo"/>
                        </BankNode>
                        <TransDate>
                            <xsl:value-of select="java:com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.dateFormat8To10(Header/TransDate)"/>
                        </TransDate>
                        <TransTime>
                            <xsl:value-of select="java:com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.timeFormat(Header/TransTime)"/>
                        </TransTime>
                        <!--1 银保通 2 移动展业  3 微信卡单 4 微信银保通-->
                        <SellsWay>1</SellsWay>
                        <ContNo>
                            <xsl:value-of select="App/Req/PolicyNo"/>
                        </ContNo>
                        <TransMony>
                            <xsl:value-of select="App/Req/PayAmt"/>
                        </TransMony>
                    </LKTransTracks>
                </LKTransTrackses>
                <ReqHead>
                    <Version>1.0</Version>
                    <SerialNo>
                        <xsl:value-of select="Header/SerialNo"/>
                    </SerialNo>
                    <TransID>XQ002</TransID>
                    <TransDate>
                        <xsl:value-of select="java:com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.dateFormat8To10(Header/TransDate)"/>
                    </TransDate>
                    <TransTime>
                        <xsl:value-of select="java:com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.timeFormat(Header/TransTime)"/>
                    </TransTime>
                    <SysCode>WIC</SysCode>
                    <Channel>
                        <xsl:call-template name="tran_channel">
                            <xsl:with-param name="channel">
                                <xsl:value-of select="Header/EntrustWay"/>
                            </xsl:with-param>
                        </xsl:call-template><!-- 销售方式 -->
                    </Channel>
                </ReqHead>
                <ReqRenewalFee>
                    <!--保单号-->
                    <ContNo>
                        <xsl:value-of select="App/Req/PolicyNo"/>
                    </ContNo>
                    <!--缴费金额-->
                    <PayAmt>
                        <xsl:value-of select="App/Req/PayAmt"/>
                    </PayAmt>
                    <!-- 账号 -->
                    <PayAcc>
                        <xsl:value-of select="App/Req/PayAcc"/>
                    </PayAcc>
                    <!--保险公司方险种代码 -->
                    <RiskCode>
                        <xsl:value-of select="App/Req/RiskCode"/>
                    </RiskCode>
                    <!--产品代码-->
                    <ProdCode>
                        <xsl:value-of select="App/Req/ProdCode"/>
                    </ProdCode>
                    <!--投保单号-->
                    <PolicyApplyNo>
                        <xsl:value-of select="App/Req/PolicyApplyNo"/>
                    </PolicyApplyNo>
                    <!-- 投保人 -->
                    <AppntName>
                        <xsl:value-of select="App/Req/Appl/Name"/>
                    </AppntName>
                    <!-- 投保人证件类型 -->
                    <IDType>
                        <xsl:call-template name="tran_idtype">
                            <xsl:with-param name="idtype">
                                <xsl:value-of select="App/Req/Appl/IDKind"/>
                            </xsl:with-param>
                        </xsl:call-template>
                    </IDType>
                    <!-- 投保人证件号 -->
                    <IDNo>
                        <xsl:value-of select="App/Req/Appl/IDCode"/>
                    </IDNo>
                    <!--应交期数-->
                    <DuePeriod>
                        <xsl:value-of select="App/Req/DuePeriod"/>
                    </DuePeriod>
                    <BankCode>05</BankCode>
                    <ZoneNo>
                        <xsl:value-of select="Header/ProvCode"/>
                    </ZoneNo>
                    <BranchNo>
                        <xsl:value-of select="Header/BranchNo"/>
                    </BranchNo>
                    <TellerNo>
                        <xsl:value-of select="Header/Tlid"/>
                    </TellerNo>
                </ReqRenewalFee>
            </Body>
        </TranData>
    </xsl:template>
    <!-- 证件类型  -->
    <xsl:template name="tran_idtype">
        <xsl:param name="idtype"/>
        <xsl:if test="$idtype = 1">111</xsl:if>
        <xsl:if test="$idtype = 2">414</xsl:if>
        <xsl:if test="$idtype = 3">114</xsl:if>
        <xsl:if test="$idtype = 4">990</xsl:if>
        <xsl:if test="$idtype = 5">990</xsl:if>
        <xsl:if test="$idtype = 0">990</xsl:if>
        <xsl:if test="$idtype = 110001">111</xsl:if>
        <xsl:if test="$idtype = 110002">111</xsl:if>
        <xsl:if test="$idtype = 110003">112</xsl:if>
        <xsl:if test="$idtype = 110004">112</xsl:if>
        <xsl:if test="$idtype = 110005">113</xsl:if>
        <xsl:if test="$idtype = 110006">113</xsl:if>
        <xsl:if test="$idtype = 110011">990</xsl:if>
        <xsl:if test="$idtype = 110012">990</xsl:if>
        <xsl:if test="$idtype = 110013">114</xsl:if>
        <xsl:if test="$idtype = 110014">114</xsl:if>
        <xsl:if test="$idtype = 110015">990</xsl:if>
        <xsl:if test="$idtype = 110016">990</xsl:if>
        <xsl:if test="$idtype = 110017">990</xsl:if>
        <xsl:if test="$idtype = 110018">990</xsl:if>
        <xsl:if test="$idtype = 110019">513</xsl:if>
        <xsl:if test="$idtype = 110020">513</xsl:if>
        <xsl:if test="$idtype = 110021">511</xsl:if>
        <xsl:if test="$idtype = 110022">511</xsl:if>
        <xsl:if test="$idtype = 110023">414</xsl:if>
        <xsl:if test="$idtype = 110024">414</xsl:if>
        <xsl:if test="$idtype = 110025">414</xsl:if>
        <xsl:if test="$idtype = 110026">414</xsl:if>
        <xsl:if test="$idtype = 110027">114</xsl:if>
        <xsl:if test="$idtype = 110028">114</xsl:if>
        <xsl:if test="$idtype = 110029">990</xsl:if>
        <xsl:if test="$idtype = 110030">990</xsl:if>
        <xsl:if test="$idtype = 110031">123</xsl:if>
        <xsl:if test="$idtype = 110032">123</xsl:if>
        <xsl:if test="$idtype = 110033">114</xsl:if>
        <xsl:if test="$idtype = 110034">114</xsl:if>
        <xsl:if test="$idtype = 110035">123</xsl:if>
        <xsl:if test="$idtype = 110036">123</xsl:if>
        <xsl:if test="$idtype = 119998">990</xsl:if>
        <xsl:if test="$idtype = 119999">990</xsl:if>
        <xsl:if test="$idtype = 110037">553</xsl:if>
    </xsl:template>
    <xsl:template name="tran_channel">
        <xsl:param name="channel"></xsl:param>
        <xsl:if test="$channel = 11">ABCOTC</xsl:if>
        <xsl:if test="$channel = 01">ABCEBANK</xsl:if>
        <xsl:if test="$channel = 04">ABCSELF</xsl:if>
        <xsl:if test="$channel = 02">ABCMOBILE</xsl:if>
        <xsl:if test="$channel = 22">ABCSUPOTC</xsl:if>
        <xsl:if test="$channel = 08">ABCESTWD</xsl:if>
    </xsl:template>

</xsl:stylesheet>