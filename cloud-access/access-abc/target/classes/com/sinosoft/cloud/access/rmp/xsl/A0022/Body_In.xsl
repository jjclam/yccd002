<?xml version="1.0" encoding="GBK"?>

<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:java="http://xml.apache.org/xslt/java"
                exclude-result-prefixes="java">

    <xsl:template match="Package">
        <TranData>
            <xsl:variable name="header" select="Head"/>
            <!-- 报文体 -->
            <Body>
                <!--新增字段-->
                <payBankName>
                    <xsl:value-of select="RequestNodes/RequestNode/payBankName"/>
                </payBankName>


                <!--RequestNodes/RequestNode/payVirtualAcountNo 续期虚拟银行账号-->
                <payVirtualAcountNo>
                    <xsl:value-of select="RequestNodes/RequestNode/payVirtualAcountNo"/>
                </payVirtualAcountNo>

                <!--RequestNodes/RequestNode/payAccountId 续期付款账户ID-->
                <payAccountId>
                    <xsl:value-of select="RequestNodes/RequestNode/payAccountId"/>
                </payAccountId>

                <LJTempFees>
                    <LJTempFee>
                        <PayDate>
                            <xsl:value-of select="RequestNodes/RequestNode/payTime"/>
                        </PayDate>
                        <PayMoney>
                            <xsl:call-template name="tran_payAmt">
                                <xsl:with-param name="payAmt">
                                    <xsl:value-of select="RequestNodes/RequestNode/payAmount"/>
                                </xsl:with-param>
                            </xsl:call-template>
                        </PayMoney>
                    </LJTempFee>
                </LJTempFees>
<!--先注释-->
                <!--<LJAPayPojo>-->
                    <!--<PayDate>-->
                        <!--<xsl:value-of select="RequestNodes/RequestNode/payTime"/>-->
                    <!--</PayDate>-->
                    <!--<SumActuPayMoney>-->
                        <!--<xsl:call-template name="tran_payAmt">-->
                            <!--<xsl:with-param name="payAmt">-->
                                <!--<xsl:value-of select="RequestNodes/RequestNode/payAmount"/>-->
                            <!--</xsl:with-param>-->
                        <!--</xsl:call-template>-->
                    <!--</SumActuPayMoney>-->
                <!--</LJAPayPojo>-->

                <!--此处以上为新增-->

                <!--payTransNo	支付交易编号-->
                <OldTranno>
                    <xsl:value-of select="RequestNodes/RequestNode/payTransNo"/>
                </OldTranno>
                <!-- 试算申请顺序号 -->
                <Global>
                    <ApplySerial>
                        <xsl:value-of select="RequestNodes/RequestNode/checkPayNo"/>
                    </ApplySerial>
                    <EntrustWay>ABC</EntrustWay>

<!--                    <MainRiskCode>
                        <xsl:value-of select="App/Req/RiskCode"/>
                    </MainRiskCode>-->
                </Global>
                <!-- 应交保险费 -->
                <LCCont>
                    <Prem>
                        <xsl:call-template name="tran_payAmt">
                            <xsl:with-param name="payAmt">
                                <xsl:value-of select="RequestNodes/RequestNode/payAmount"/>
                            </xsl:with-param>
                        </xsl:call-template>
                    </Prem>
                    <ThirdPartyOrderId>
                        <!--RequestNodes/RequestNode/partnerOrderId 合作商订单号-->
                        <xsl:value-of select="RequestNodes/RequestNode/partnerOrderId"/>
                    </ThirdPartyOrderId>
                    <ContNo>
                        <xsl:value-of select="RequestNodes/RequestNode/contno"/>
                    </ContNo>
                    <!--新增（prtno）-->
                    <PrtNo>
                        <xsl:value-of select="RequestNodes/RequestNode/prtno"/>
                    </PrtNo>
                    <!--新增（newpaymode）-->
                    <NewPayMode>
                        <xsl:value-of select="RequestNodes/RequestNode/payMode"/>
                    </NewPayMode>
                    <!--新增（newbankcode）-->
                    <NewBankCode>
                        <xsl:value-of select="RequestNodes/RequestNode/payBankCode"/>
                    </NewBankCode>
                    <!--新增（newbankproivnce）-->
                    <newbankproivnce>
                        <!--RequestNodes/RequestNode/bankProvinceCode   开户行所在的省编码-->
                        <xsl:value-of select="RequestNodes/RequestNode/bankProvinceCode"/>
                    </newbankproivnce>
                    <!--新增（newbankcity）-->
                    <newbankcity>
                        <!--RequestNodes/RequestNode/bankCityCode  开户行所在的市编码-->
                        <xsl:value-of select="RequestNodes/RequestNode/bankCityCode"/>
                    </newbankcity>

                    <!--新增（newbankaccno）-->
                    <NewBankAccNo>
                        <xsl:value-of select="RequestNodes/RequestNode/payAcountNo"/>
                    </NewBankAccNo>
                    <BankAccNo>
                        <xsl:value-of select="RequestNodes/RequestNode/payAcountNo"/>
                    </BankAccNo>
                    <newacctype>
                        <!--RequestNodes/RequestNode/cardType   卡折类型-->
                            <xsl:value-of select="RequestNodes/RequestNode/cardType"/>
                    </newacctype>

                    <!--&lt;!&ndash;新增（newaccname）&ndash;&gt;-->
                    <!--&lt;!&ndash;RequestNodes/RequestNode/payAccountName 帐号持有人姓名&ndash;&gt;-->
                    <!--<newaccname>-->
                        <!--<xsl:value-of select="RequestNodes/RequestNode/payAccountName"/>-->
                    <!--</newaccname>-->
                    <!-- 销售方式 -->
                    <SellType>
                        <xsl:value-of select="$header/ThirdPartyCode"/>
                    </SellType>

                    <AccName>
                        <xsl:value-of select="RequestNodes/RequestNode/payAccountName"/>
                    </AccName>
                </LCCont>
<!--                &lt;!&ndash; 交费账户 &ndash;&gt;
                &lt;!&ndash; 保险公司方险种代码 &ndash;&gt;
                <LCPols>
                    <LCPol>
                        <RiskCode>
                            1032a
                        </RiskCode>
                    </LCPol>
                </LCPols>-->
                <LKTransTrackses>
                    <LKTransTracks>

                        <!-- 日期-->
                        <TransDate>
                            <xsl:value-of
                                    select="java:com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.getDate($header/TransTime)"/>
                        </TransDate>
                        <!--时间-->
                        <TransTime>
                            <xsl:value-of
                                    select="java:com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.getTime($header/TransTime)"/>
                        </TransTime>

                        <TransNo>
                            <xsl:value-of select="$header/TransNo"/>
                        </TransNo>
                        <!--交易编码-->
                        <TransCode>
                            <xsl:value-of select="$header/TransType"/>
                        </TransCode>

                        <xsl:if test="$header/TransType = A0022">
                            <xsl:value-of select="RequestNodes/RequestNode/payBankCode"/>
                        </xsl:if>

                        <!--&lt;!&ndash;报文头新增&ndash;&gt;-->
                        <!--&lt;!&ndash;Head/TransType  交易对象&ndash;&gt;-->
                        <!--<TransType>-->
                            <!--<xsl:value-of select="Head/TransType"/>-->
                        <!--</TransType>-->
                        <!--&lt;!&ndash;Head/Coworker  交互对象&ndash;&gt;-->
                        <!--<Coworker>-->
                            <!--<xsl:value-of select="Head/Coworker"/>-->
                        <!--</Coworker>-->
                        <!--&lt;!&ndash;以上标识为新增&ndash;&gt;-->

                        <!--&lt;!&ndash;交易流水号&ndash;&gt;-->
                        <!--<TransNo>-->
                            <!--<xsl:value-of select="Head/SerialNo"/>-->
                        <!--</TransNo>-->
                        <!--&lt;!&ndash;交易编码&ndash;&gt;-->
                        <!--<TransCode>-->
                            <!--<xsl:value-of select="Head/TransNo"/>-->
                        <!--</TransCode>-->
                        <!--&lt;!&ndash;原交易编码&ndash;&gt;-->
                        <!--<TransCodeOri>-->
                            <!--<xsl:value-of select="Head/TransCode"/>-->
                        <!--</TransCodeOri>-->
                        <!--&lt;!&ndash;交易子渠道&ndash;&gt;-->
                        <!--<AccessChnlSub>-->
                            <!--<xsl:value-of select="Head/ThirdPartyCode"/>-->
                        <!--</AccessChnlSub>-->
                        <!--<xsl:if test="RequestNodes/RequestNode/payBankCode = 11">-->
                            <!--<BankCode>05</BankCode>-->
                        <!--</xsl:if>-->

                        <!--<BankBranch>-->
                            <!--<xsl:value-of select="Head/ProvCode"/>-->
                        <!--</BankBranch>-->
                        <!--<BankNode>-->
                            <!--<xsl:value-of select="Head/BranchNo"/>-->
                        <!--</BankNode>-->
                        <!--1 银保通 2 移动展业  3 微信卡单 4 微信银保通-->
                        <SellsWay>1</SellsWay>
                    </LKTransTracks>
                </LKTransTrackses>

                <!-- 是否开户行标志 -->
                <!--<OpenBranchFlag>-->
                <!--<xsl:value-of select="App/Req/OpenBranchFlag"/>-->
                <!--</OpenBranchFlag>-->
                <!--<OldTranno>-->
                <!--<xsl:value-of select="App/Req/ApplySerial"/>-->
                <!--</OldTranno>-->
            </Body>
        </TranData>
    </xsl:template>

    <xsl:template name="tran_payAmt">
        <xsl:param name="payAmt"/>
        <xsl:choose>
            <xsl:when test="$payAmt &lt; 0">0.00</xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="$payAmt"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

</xsl:stylesheet>