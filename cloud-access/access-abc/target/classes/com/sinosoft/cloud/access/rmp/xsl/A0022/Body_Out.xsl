<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:java="http://xml.apache.org/xslt/java"
                exclude-result-prefixes="java">
    <xsl:template match="/TranData">
        <!--<xsl:variable name="lccont" select="Body/LCCont"/>-->
        <xsl:variable name="LCCont" select="Body/LCCont"/>
        <xsl:variable name="global" select="Body/Global"/>
        <xsl:variable name="LJTempFees" select="Body/LJTempFees"/>
        <xsl:variable name="LJTempFee" select="$LJTempFees/LJTempFee[1]"/>
        <xsl:variable name="risks" select="Body/LCPols"/>
        <xsl:variable name="lcPol" select="$risks/LCPol[1]"/>

        <!--新加-->
        <xsl:variable name="lKTransStatus" select="Body/LKTransStatus"/>
        <Package>
            <Header>

                <TransTime>
                    <xsl:value-of select="Head/TranTime"/>
                </TransTime>
                <ThirdPartyCode>
                    <xsl:value-of select="$LCCont/SellType"/>
                </ThirdPartyCode>
                <TransNo>
                    <xsl:value-of select="$global/SerialNo"/>
                </TransNo>
                <TransType>
                    <xsl:value-of select="Head/TransCode"/>
                    <!--<xsl:value-of select="Head/TransCode"/>-->
                </TransType>
                <Coworker>02</Coworker>
                <ResponseCode>0000</ResponseCode>
            </Header>
            <ResponseNodes>
                <ResponseNode>
                    <Result>
                        <xsl:choose>
                            <xsl:when test="Head/Flag=0">
                                <ResultStatus>1</ResultStatus>
                                <ResultInfoDesc>交易成功</ResultInfoDesc>
                            </xsl:when>
                            <xsl:otherwise>
                                <ResultStatus>0</ResultStatus>
                                <ResultInfoDesc>
                                    <xsl:value-of select="Head/Desc"/>
                                </ResultInfoDesc>
                            </xsl:otherwise>
                        </xsl:choose>
                    </Result>
                    <BusinessObject>
                        <partnerOrderId>
                            <xsl:value-of select="$LCCont/ThirdPartyOrderId"/>
                        </partnerOrderId>
                        <policyNo>
                            <xsl:value-of select="$LCCont/ContNo"/>
                        </policyNo>
                        <proposalNo>
                            <xsl:value-of select="$LCCont/ContNo"/>
                        </proposalNo>
                        <policyUrl></policyUrl><!-- 电子保单Url字段未使用 -->
                        <totalPremium>
                            <xsl:value-of select="$LCCont/Prem"/>
                        </totalPremium>
                        <accountDate>
                            <xsl:value-of select="$LCCont/FirstPayDate"/>
                        </accountDate>
                        <issuedTime>
                            <xsl:value-of select="concat($LCCont/SignDate,' ',$LCCont/SignTime)"/>
                        </issuedTime>
                        <xsl:choose>
                            <xsl:when test="Head/Flag=0">
                                <isSuccess>1</isSuccess>
                                <message>签单成功</message>
                            </xsl:when>
                            <xsl:otherwise>
                                <isSuccess>0</isSuccess>
                                <message>签单失败</message>
                            </xsl:otherwise>
                        </xsl:choose>
                        <InsuranceStartPeriod>
                            <xsl:value-of select="$LCCont/CValiDate"/>
                        </InsuranceStartPeriod>
                        <InsuranceEndPeriod>
                            <xsl:value-of select="$lcPol/EndDate"/>
                        </InsuranceEndPeriod>
                    </BusinessObject>
                </ResponseNode>
            </ResponseNodes>
        </Package>
    </xsl:template>
</xsl:stylesheet>