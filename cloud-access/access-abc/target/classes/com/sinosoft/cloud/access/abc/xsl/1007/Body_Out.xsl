<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:java="http://xml.apache.org/xslt/java"
                exclude-result-prefixes="java">
    <xsl:template match="/TranData">
        <xsl:variable name="xQJFQueryResult" select="Body/RespRenewalFeeQuery"/>
        <ABCB2I>
            <Header>
                <xsl:choose>
                    <xsl:when test="Head/Flag=0">
                        <RetCode>000000</RetCode>
                        <RetMsg>交易成功</RetMsg>
                    </xsl:when>
                    <xsl:otherwise>
                        <RetCode>009999</RetCode>
                        <RetMsg>
                            <xsl:value-of select="Head/Desc"/>
                        </RetMsg>
                    </xsl:otherwise>
                </xsl:choose>
                <SerialNo>
                    <xsl:value-of select="Head/TranNo"/>
                </SerialNo>
                <InsuSerial>
                    <xsl:value-of select="Head/InsuSerial"/>
                </InsuSerial>
                <TransDate>
                    <xsl:value-of
                            select="java:com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.date10to8(Head/TranDate)"/>
                </TransDate>
                <TransTime>
                    <xsl:value-of
                            select="java:com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.timeString10To8(Head/TranTime)"/>
                </TransTime>
                <BankCode>
                    <xsl:value-of select="Head/BankCode"/>
                </BankCode>
                <CorpNo>
                    <xsl:value-of select="Head/CorpNo"/>
                </CorpNo>
                <TransCode>
                    <xsl:value-of select="Head/TransCode"/>
                </TransCode>
            </Header>
            <xsl:if test="Head/Flag=0">
                <App>
                    <Ret>
                        <Appl>
                            <Name>
                                <xsl:value-of select="$xQJFQueryResult/AppntName"/>
                            </Name>
                            <IDKind>
                                <xsl:call-template name="tran_idtype">
                                    <xsl:with-param name="idtype">
                                        <xsl:value-of select="$xQJFQueryResult/IDType"/>
                                    </xsl:with-param>
                                </xsl:call-template>
                            </IDKind>
                            <IDCode>
                                <xsl:value-of select="$xQJFQueryResult/IDNo"/>
                            </IDCode>
                        </Appl>
                        <PolicyNo>
                            <xsl:value-of select="$xQJFQueryResult/Contno"/>
                        </PolicyNo>
                        <!--应交金额-->
                        <DueAmt>
                            <xsl:value-of select="$xQJFQueryResult/DueAmt"/>
                        </DueAmt>
                        <!--应交日期-->
                        <DueDate>
                            <xsl:value-of
                                    select="java:com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.date10to8($xQJFQueryResult/DueDate)"/>
                        </DueDate>
                        <!--应交期数-->
                        <DuePeriod>
                            <xsl:value-of select="$xQJFQueryResult/DuePeriod"/>
                        </DuePeriod>
                        <RiskCode>
                            <xsl:value-of select="$xQJFQueryResult/RiskCode"/>
                        </RiskCode>
                    </Ret>
                </App>
            </xsl:if>
        </ABCB2I>
    </xsl:template>
    <!-- 证件类型  -->
    <xsl:template name="tran_idtype">
        <xsl:param name="idtype">0</xsl:param>
        <xsl:if test="$idtype =0">110001</xsl:if>
        <xsl:if test="$idtype =1">110023</xsl:if>
        <xsl:if test="$idtype =2">110027</xsl:if>
        <xsl:if test="$idtype =3">119999</xsl:if>
        <xsl:if test="$idtype =4">110005</xsl:if>
        <xsl:if test="$idtype =5">119999</xsl:if>
        <xsl:if test="$idtype =6">119999</xsl:if>
        <xsl:if test="$idtype =7">110021</xsl:if>
        <xsl:if test="$idtype =8">119999</xsl:if>
        <xsl:if test="$idtype =9">119999</xsl:if>
        <xsl:if test="$idtype =10">110033</xsl:if>
        <xsl:if test="$idtype =11">119999</xsl:if>
        <xsl:if test="$idtype =12">110003</xsl:if>
        <xsl:if test="$idtype =13">110031</xsl:if>
        <xsl:if test="$idtype =15">110019</xsl:if>
        <xsl:if test="$idtype =16">110021</xsl:if>
        <xsl:if test="$idtype =17">110037</xsl:if>
        <xsl:if test="$idtype =111">110001</xsl:if>
        <xsl:if test="$idtype =414">110023</xsl:if>
        <xsl:if test="$idtype =114">110027</xsl:if>
        <xsl:if test="$idtype =335">119999</xsl:if>
        <xsl:if test="$idtype =113">110005</xsl:if>
        <xsl:if test="$idtype =133">119999</xsl:if>
        <xsl:if test="$idtype =131">119999</xsl:if>
        <xsl:if test="$idtype =990">110021</xsl:if>
        <xsl:if test="$idtype =990">119999</xsl:if>
        <xsl:if test="$idtype =990">119999</xsl:if>
        <xsl:if test="$idtype =516">119999</xsl:if>
        <xsl:if test="$idtype =112">110003</xsl:if>
        <xsl:if test="$idtype =123">110031</xsl:if>
        <xsl:if test="$idtype =511">110021</xsl:if>
        <xsl:if test="$idtype =553">110037</xsl:if>
        <xsl:if test="$idtype =117">119999</xsl:if>
        <xsl:if test="$idtype =513">110019</xsl:if>
    </xsl:template>

</xsl:stylesheet>
