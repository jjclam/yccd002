<?xml version="1.0" encoding="GBK"?>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:java="http://xml.apache.org/xslt/java" exclude-result-prefixes="java">
    <xsl:template match="/TranData">
        <ABCB2I>
            <Header>
                <SerialNo></SerialNo>
                <InsuSerial>
                    <xsl:value-of select="Head/InsuSerial"/>
                </InsuSerial>
                <TransDate>
                    <xsl:value-of
                            select="java:com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.date10to8(Head/TranDate)"/>
                </TransDate>
                <TransTime>
                    <xsl:value-of
                            select="java:com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.timeString10To8(Head/TranTime)"/>
                </TransTime>
                <BankCode>03</BankCode>
                <CorpNo>1118</CorpNo>
                <TransCode>1001</TransCode>
                <TransSide>0</TransSide>
                <EntrustWay></EntrustWay>
                <ProvCode></ProvCode>
                <BranchNo></BranchNo>
                <Tlid></Tlid>
            </Header>
            <App>
                <Ret>
                    <EncType>01</EncType>
                    <PriKey>
                        <xsl:value-of select="Body/PriKey"/>
                    </PriKey>
                    <OrgKey>
                        <xsl:value-of select="Body/OrgKey"/>
                    </OrgKey>
                </Ret>
            </App>
        </ABCB2I>
    </xsl:template>
</xsl:stylesheet>