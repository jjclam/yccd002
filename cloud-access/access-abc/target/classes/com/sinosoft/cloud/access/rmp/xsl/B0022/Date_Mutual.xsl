<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:java="http://xml.apache.org/xslt/java"
                exclude-result-prefixes="java">
    <xsl:template match="/TranData">
        <xsl:variable name="bqresult" select="Body/BQResult"/>
        <!--<xsl:variable name="LCInsured" select="Body/LCInsured"/>-->
        <xsl:variable name="LCCont" select="Body/LCCont"/>
        <xsl:variable name="LCAppnt" select="Body/LCAppnt"/>
        <xsl:variable name="global" select="Body/Global"/>
        <xsl:variable name="LJTempFees" select="Body/LJTempFees"/>
        <xsl:variable name="LJTempFee" select="$LJTempFees/LJTempFee[1]"/>
        <xsl:variable name="risks" select="Body/LCPols"/>
        <xsl:variable name="LCAddresses" select="Body/LCAddresses"/>
        <xsl:variable name="LCInsureds" select="Body/LCInsureds"/>
        <xsl:variable name="lcPol" select="$risks/LCPol[1]"/>
        <xsl:variable name="LCInsured" select="$LCInsureds/LCInsured[1]"/>
        <xsl:variable name="LCAddress" select="$LCAddresses/LCAddress[1]"/>
        <xsl:variable name="bnf" select="Body/LCBnfs/LCBnf"/>
        <xsl:variable name="insured" select="TranData/Body/LCInsureds/LCInsured"/>


        <!--新加-->
        <xsl:variable name="lKTransStatus" select="Body/LKTransStatus"/>
        <Package>
            <Header>
                <TransTime>
                    <xsl:value-of select="Head/TranTime"/>
                </TransTime>
                <ThirdPartyCode>
                    <xsl:value-of select="$LCCont/SellType"/>
                </ThirdPartyCode>
                <TransNo>
                    <xsl:value-of select="$global/SerialNo"/>
                </TransNo>
                <TransType>
                    <xsl:value-of select="$lKTransStatus/TransCode"/>
                </TransType>
                <Coworker>02</Coworker>
            </Header>
            <RequestNodes>
                <RequestNode>
                    <ProductCode>
                        <xsl:value-of select="$LCInsured/ContPlanCode"/>
                    </ProductCode>
                    <ProductName></ProductName>
                    <ThirdPartyOrderId>
                        <xsl:value-of select="$LCCont/ThirdPartyOrderId"/>
                    </ThirdPartyOrderId>
                    <UserId>
                        <xsl:value-of select="$LCCont/UserId"/>
                    </UserId>
                    <AssetId></AssetId>
                    <ContNo>
                        <xsl:value-of select="$LCCont/ContNo"/>
                    </ContNo>
                    <prtno>
                        <xsl:value-of select="$LCCont/PrtNo"/>
                    </prtno>
                    <ProposalNo>
                        <xsl:value-of select="$lcPol/ProposalNo"/>
                    </ProposalNo>
                    <InsuranceStartPeriod>
                        <xsl:value-of select="$LCCont/CValiDate"/>
                    </InsuranceStartPeriod>
                    <InsuranceEndPeriod>
                        <xsl:value-of select="$lcPol/EndDate"/>
                    </InsuranceEndPeriod>
                    <SignedDate>
                        <xsl:value-of select="$LCCont/polapplydate"/>
                    </SignedDate>
                    <InsuYear>
                        <xsl:value-of select="$lcPol/InsuYear"/>
                    </InsuYear>
                    <InsuYearFlag>
                        <xsl:value-of select="$lcPol/InsuYearFlag"/>
                    </InsuYearFlag>
                    <PayIntv>
                        <xsl:value-of select="$LCCont/PayIntv"/>
                    </PayIntv>
                    <PayEndYear>
                        <xsl:value-of select="$lcPol/PayEndYear"/>
                    </PayEndYear>
                    <PayEndYearFlag>
                        <xsl:value-of select="$lcPol/PayEndYearFlag"/>
                    </PayEndYearFlag>
                    <Premium>
                        <xsl:value-of select="$LCCont/Prem"/>
                    </Premium>
                    <InsuredAmount>
                        <xsl:value-of select="$LCCont/Amnt"/>
                    </InsuredAmount>
                    <GrossPremium>
                        <xsl:value-of select="$LCCont/Prem"/>
                    </GrossPremium>
                    <FaceAmount>
                        <xsl:value-of select="$LCCont/Amnt"/>
                    </FaceAmount>
                    <FirstPremium>
                        <xsl:value-of select="$LCCont/Prem"/>
                    </FirstPremium>
                    <InitialPremAmt>
                        <xsl:value-of select="$LCCont/Prem"/>
                    </InitialPremAmt>
                    <PolicyDeliveryFee/>
                    <UnitCount>
                        <xsl:value-of select="$lcPol/Mult"/>
                    </UnitCount>
                    <AgentNo>
                        <xsl:value-of select="$lcPol/AgentCode"/>
                    </AgentNo>
                    <ExpireProcessMode>
                        <xsl:value-of select="$lcPol/RnewFlag"/>
                    </ExpireProcessMode>
                    <ProductPeriod/>
                    <ProductPeriodFlag/>
                    <ABCode>
                        <xsl:value-of select="$LCCont/AgentBankCode"/>
                    </ABCode>
                    <Applicant>
                        <AppNo>
                            <xsl:value-of select="$LCCont/AppntNo"/>
                        </AppNo>
                        <RelationToInsured>
                            <xsl:value-of select="$LCAppnt/RelatToInsu"/>
                        </RelationToInsured>
                        <AppName>
                            <xsl:value-of select="$LCCont/AppntName"/>
                        </AppName>
                        <AppSex>
                            <xsl:value-of select="$LCCont/AppntSex"/>
                        </AppSex>
                        <AppBirthday>
                            <xsl:value-of select="$LCCont/AppntBirthday"/>
                        </AppBirthday>
                        <AppIDType>
                            <xsl:value-of select="$LCCont/AppntIDType"/>
                        </AppIDType>
                        <AppIDNo>
                            <xsl:value-of select="$LCCont/AppntIDNo"/>
                        </AppIDNo>
                        <IdexpDate>
                            <xsl:value-of select="$LCAppnt/IdValiDate"/>
                        </IdexpDate>
                        <NativePlace>
                            <xsl:value-of select="$LCAppnt/NativePlace"/>
                        </NativePlace>
                        <Language/>
                        <Marriage>
                            <xsl:value-of select="$LCAppnt/Marriage"/>
                        </Marriage>
                        <SocialInsuFlag>
                            <xsl:value-of select="$LCInsured/SSFlag"/>
                        </SocialInsuFlag>
                        <Stature>
                            <xsl:value-of select="$LCAppnt/Stature"/>
                        </Stature>
                        <Weight>
                            <xsl:value-of select="$LCAppnt/Avoirdupois"/>
                        </Weight>
                        <Salary>
                            <xsl:value-of select="$LCAppnt/Salary"/>
                        </Salary>
                        <Health>
                            <xsl:value-of select="$LCAppnt/Health"/>
                        </Health>
                        <Province>
                            <xsl:value-of select="$LCAddress/Province"/>
                        </Province>
                        <City>
                            <xsl:value-of select="$LCAddress/City"/>
                        </City>
                        <Country>
                            <xsl:value-of select="$LCAddress/County"/>
                        </Country>
                        <OccupationClass>
                            <xsl:value-of select="$lcPol/OccupationType"/>
                        </OccupationClass>
                        <OccupationCode>
                            <xsl:value-of select="$LCAppnt/OccupationCode"/>
                        </OccupationCode>
                        <AppEmail>
                            <xsl:value-of select="$LCAddress/EMail"/>
                        </AppEmail>
                        <AppTelephone>
                            <xsl:value-of select="$LCAddress/Phone"/>
                        </AppTelephone>
                        <AppMobile>
                            <xsl:value-of select="$LCAddress/Mobile"/>
                        </AppMobile>
                        <AppZipCode>
                            <xsl:value-of select="$LCAddress/ZipCode"/>
                        </AppZipCode>
                        <AppAddress>
                            <xsl:value-of select="$LCAddress/PostalAddress"/>
                        </AppAddress>
                    </Applicant>

                    <Insureds>
                        <xsl:for-each select="$insured">
                            <Insured>
                                <InsuNo>
                                    <xsl:value-of select="$LCCont/InsuredNo"/>
                                </InsuNo>
                                <InsuRelationToApp>
                                    <xsl:value-of select="RelationToAppnt"/>
                                </InsuRelationToApp>
                                <InsuRelationToMainInsu>
                                    <xsl:value-of select="RelationToMainInsured"/>
                                </InsuRelationToMainInsu>
                                <IsMainInsured>
                                    <!--先隐藏-->
                                    <!--<xsl:value-of select="$LCInsured/IsMainInsured"/>-->
                                </IsMainInsured>
                                <InsuName>
                                    <xsl:value-of select="$LCCont/InsuredName"/>
                                </InsuName>
                                <InsuSex>
                                    <xsl:value-of select="$LCCont/InsuredSex"/>
                                </InsuSex>
                                <InsuBirthday>
                                    <xsl:value-of select="$LCCont/InsuredBirthday"/>
                                </InsuBirthday>
                                <InsuIDType>
                                    <xsl:value-of select="$LCCont/InsuredIDType"/>
                                </InsuIDType>
                                <InsuIDNo>
                                    <xsl:value-of select="$LCCont/InsuredIDNo"/>
                                </InsuIDNo>
                                <IdexpDate>
                                    <xsl:value-of select="$LCAppnt/IdValiDate"/>
                                </IdexpDate>
                                <NativePlace>
                                    <xsl:value-of select="$LCAppnt/NativePlace"/>
                                </NativePlace>
                                <Language/>
                                <Stature>
                                    <xsl:value-of select="Stature"/>
                                </Stature>
                                <Weight>
                                    <xsl:value-of select="Avoirdupois"/>
                                </Weight>
                                <Salary>
                                    <xsl:value-of select="Salary"/>
                                </Salary>
                                <Health>
                                    <xsl:value-of select="Health"/>
                                </Health>
                                <Marriage/>
                                <SocialInsuFlag>
                                    <xsl:value-of select="SSFlag"/>
                                </SocialInsuFlag>
                                <InsuEmail>
                                    <xsl:value-of select="$LCAddress/EMail"/>
                                </InsuEmail>
                                <InsuTelephone>
                                    <xsl:value-of select="$LCAddress/Phone"/>
                                </InsuTelephone>
                                <InsuMobile>
                                    <xsl:value-of select="$LCAddress/Mobile"/>
                                </InsuMobile>
                                <InsuProvince>
                                    <xsl:value-of select="$LCAddress/Province"/>
                                </InsuProvince>
                                <InsuCity>
                                    <xsl:value-of select="$LCAddress/City"/>
                                </InsuCity>
                                <InsuCountry>
                                    <xsl:value-of select="$LCAddress/County"/>
                                </InsuCountry>
                                <InsuZipCode>
                                    <xsl:value-of select="$LCAddress/ZipCode"/>
                                </InsuZipCode>
                                <InsuAddress>
                                    <xsl:value-of select="$LCAddress/PostalAddress"/>
                                </InsuAddress>
                                <OccupationClass>
                                    <xsl:value-of select="$lcPol/OccupationType"/>
                                </OccupationClass>
                                <OccupationCode>
                                    <xsl:value-of select="$LCAppnt/OccupationCode"/>
                                </OccupationCode>
                                <SameIndustryInsuredAmount/>
                                <xsl:choose>
                                </xsl:choose>
                                <PolicyLiabilities>
                                    <xsl:when test="ContPlanCode != '' or ContPlanCode != null ">
                                        <xsl:for-each select="$bnf[BnfType=0]">
                                            <PolicyLiability>
                                                <RiskCode>1025004</RiskCode>
                                                <RiskName>上海人寿大蘑菇定期寿险</RiskName>
                                                <RiskPremium>294.00</RiskPremium>
                                                <RiskAmount>200000.00</RiskAmount>
                                                <MainRiskFlag>1</MainRiskFlag>
                                                <InsuranceStartPeriod>2019-08-13</InsuranceStartPeriod>
                                                <InsuranceEndPeriod>2039-08-13</InsuranceEndPeriod>
                                                <PayEndYearFlag>Y</PayEndYearFlag>
                                                <InsuYear>20</InsuYear>
                                                <InsuYearFlag>Y</InsuYearFlag>
                                                <PayIntv>12</PayIntv>
                                                <PayEndYear>20</PayEndYear>
                                                <Deductibles/>
                                                <Lossratio/>
                                                <DutiesInfo/>
                                            </PolicyLiability>
                                        </xsl:for-each>
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <PolicyLiability>
                                            <RiskCode>1025004</RiskCode>
                                            <RiskName>上海人寿大蘑菇定期寿险</RiskName>
                                            <RiskPremium>294.00</RiskPremium>
                                            <RiskAmount>200000.00</RiskAmount>
                                            <MainRiskFlag>1</MainRiskFlag>
                                            <InsuranceStartPeriod>2019-08-13</InsuranceStartPeriod>
                                            <InsuranceEndPeriod>2039-08-13</InsuranceEndPeriod>
                                            <PayEndYearFlag>Y</PayEndYearFlag>
                                            <InsuYear>20</InsuYear>
                                            <InsuYearFlag>Y</InsuYearFlag>
                                            <PayIntv>12</PayIntv>
                                            <PayEndYear>20</PayEndYear>
                                            <Deductibles/>
                                            <Lossratio/>
                                            <DutiesInfo/>
                                        </PolicyLiability>
                                    </xsl:otherwise>
                                </PolicyLiabilities>
                            </Insured>
                        </xsl:for-each>
                    </Insureds>

                    <Beneficiaries>
                        <xsl:for-each select="$bnf">
                            <Beneficiary>
                                <BeneficiaryNo>
                                    <xsl:value-of select="BnfNo"/>
                                </BeneficiaryNo>
                                <BeneficiaryGrade>
                                    <xsl:value-of select="BnfGrade"/>
                                </BeneficiaryGrade>
                                <BeneficiaryOrder>
                                    <!--暂隐藏-->
                                    <!--<xsl:value-of select="BeneficiaryOrder"/>-->
                                </BeneficiaryOrder>
                                <BeneficiaryType>
                                    <xsl:value-of select="BnfType"/>
                                </BeneficiaryType>
                                <InterestPercent>
                                    <xsl:value-of select="BnfLot"/>
                                </InterestPercent>
                                <BeneficiaryName>
                                    <xsl:value-of select="Name"/>
                                </BeneficiaryName>
                                <BeneficiarySex>
                                    <xsl:value-of select="Sex"/>
                                </BeneficiarySex>
                                <BeneficiaryBirthday>
                                    <xsl:value-of select="Birthday"/>
                                </BeneficiaryBirthday>
                                <BeneficiaryIDType>
                                    <xsl:value-of select="IDType"/>
                                </BeneficiaryIDType>
                                <BeneficiaryIDNo>
                                    <xsl:value-of select="IDNo"/>
                                </BeneficiaryIDNo>
                                <IDExpDate>
                                    <xsl:value-of select="IdValiDate"/>
                                </IDExpDate>
                                <RelationToInsured>
                                    <xsl:value-of select="RelationToInsured"/>
                                </RelationToInsured>
                                <BeneficiaryEmail></BeneficiaryEmail>
                                <BeneficiaryTelephone></BeneficiaryTelephone>
                                <BeneficiaryMobile>
                                    <xsl:value-of select="Tel"/>
                                </BeneficiaryMobile>
                                <BeneficiaryZipCode>
                                    <xsl:value-of select="ZipCode"/>
                                </BeneficiaryZipCode>
                                <BeneficiaryAddress>
                                    <xsl:value-of select="Address"/>
                                </BeneficiaryAddress>
                            </Beneficiary>
                        </xsl:for-each>
                    </Beneficiaries>
                    <RenewalPaymentInfo>
                        <PayMode>
                            <xsl:value-of select="$LCCont/PayMode"/>
                        </PayMode>
                        <PayBankCode>
                            <xsl:value-of select="$LCCont/NewBankCode"/>
                        </PayBankCode>
                        <PayBankName/>
                        <BankProvinceCode/>
                        <BankCityCode>
                            <xsl:value-of select="$LCCont/BankCode"/>
                        </BankCityCode>
                        <PayAcountNo>
                            <xsl:value-of select="$LCCont/NewBankAccNo"/>
                        </PayAcountNo>
                        <CardType>
                            <xsl:value-of select="$LCCont/AccType"/>
                        </CardType>
                        <PayVirtualAcountNo/>
                        <PayAccountName>
                            <xsl:value-of select="$LCCont/AccName"/>
                        </PayAccountName>
                        <PayAccountId/>
                    </RenewalPaymentInfo>
                    <!--暂传固定字段-->
                    <SaleChannel>
                        <IntermediaryCode>05</IntermediaryCode>
                        <IntermediaryName/>
                    </SaleChannel>
                    <checkPayNo>
                        <xsl:value-of select="$LJTempFee/TempFeeNo"/>
                    </checkPayNo>
                    <mobile>
                        <xsl:value-of select="$LCAddress/Mobile"/>
                    </mobile>
                    <payTime>
                        <xsl:value-of select="java:com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.getFormatDateTime2($LJTempFee/PayDate,$LCCont/SignTime)"/>
                    </payTime>
                    <payAmount>
                        <xsl:value-of select="$LJTempFee/PayMoney"/>
                    </payAmount>
                    <payMode>
                        <xsl:value-of select="$LCCont/PayMode"/>
                    </payMode>
                    <payBankCode>
                        <xsl:value-of select="$LCCont/NewBankCode"/>
                    </payBankCode>
                    <payBankName/>
                    <bankProvinceCode/>
                    <bankCityCode>
                        <xsl:value-of select="$LCCont/BankCode"/>
                    </bankCityCode>
                    <bankLocationCode/>
                    <payAccountName>
                        <xsl:value-of select="$LCCont/AccName"/>
                    </payAccountName>
                    <payAcountNo>
                        <xsl:value-of select="$LCCont/NewBankAccNo"/>
                    </payAcountNo>
                    <cardType>
                        <xsl:value-of select="$LCCont/AccType"/>
                    </cardType>
                    <payVirtualAcountNo></payVirtualAcountNo>
                    <payAccountId></payAccountId>
                </RequestNode>
            </RequestNodes>
        </Package>
    </xsl:template>
</xsl:stylesheet>