<?xml version="1.0" encoding="GBK"?>

<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:java="http://xml.apache.org/xslt/java"
                exclude-result-prefixes="java">

    <xsl:template match="AUTO_XML_DATA">
        <xsl:variable name="head" select="request/header"/>
        <!--订单基本信息-->
        <xsl:variable name="policy" select="request/body/policy"/>
        <!--账单信息-->
        <xsl:variable name="bills" select="request/body/bills"/>
        <!--投保人信息-->
        <xsl:variable name="holder" select="request/body/holder"/>
        <!--被保人-->
        <xsl:variable name="insured" select="request/body/insureds/insured"/>
        <xsl:variable name="varRiskCodess" select="$policy/prodNo"/>
        <TranData>

            <!-- 报文体 -->
            <Body>
                <!-- &lt;!&ndash;新增字段&ndash;&gt;
                 <payBankName>
                     <xsl:value-of select="RequestNodes/RequestNode/payBankName"/>
                 </payBankName>


                 &lt;!&ndash;RequestNodes/RequestNode/payVirtualAcountNo 续期虚拟银行账号&ndash;&gt;
                 <payVirtualAcountNo>
                     <xsl:value-of select="RequestNodes/RequestNode/payVirtualAcountNo"/>
                 </payVirtualAcountNo>

                 &lt;!&ndash;RequestNodes/RequestNode/payAccountId 续期付款账户ID&ndash;&gt;
                 <payAccountId>
                     <xsl:value-of select="RequestNodes/RequestNode/payAccountId"/>
                 </payAccountId>-->

                <LJTempFees>
                    <LJTempFee>
                        <PayDate>
                            <xsl:value-of
                                    select="java:com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.getExactDate($policy/issueTime)"/>
                        </PayDate>
                        <PayMoney>
                            <xsl:value-of
                                    select="java:com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.fenToYuan($bills/fee)"/>
                        </PayMoney>

                        <Remark>
                            <xsl:value-of select="$bills/payFlowId"/>
                        </Remark>
                    </LJTempFee>
                </LJTempFees>

                <!--payTransNo	支付交易编号-->
                <!--<OldTranno>
                    <xsl:value-of select="RequestNodes/RequestNode/payTransNo"/>
                </OldTranno>-->
                <!-- 试算申请顺序号 -->
                <Global>
                    <!--<ApplySerial>
                        <xsl:value-of select="RequestNodes/RequestNode/checkPayNo"/>
                    </ApplySerial>-->
                    <EntrustWay>Ali</EntrustWay>
                    <!--交易流水号-->
                    <SerialNo>
                        <xsl:value-of select="$head/reqMsgId"/>
                    </SerialNo>
                </Global>
                <!-- 应交保险费 -->
                <LCCont>
                    <!-- 实际保费 -->
                    <Prem>
                        <xsl:value-of
                                select="java:com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.fenToYuan($policy/actualPremium)"/>
                    </Prem>
                    <!-- 保额 -->
                    <Amnt>
                        <xsl:value-of
                                select="java:com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.fenToYuan($policy/sumInsured)"/>
                    </Amnt>


                    <ThirdPartyOrderId>
                        <xsl:value-of select="$policy/policyNo"/>
                    </ThirdPartyOrderId>

                    <!-- 投保单号 -->
                    <ProposalContNo>
                        <xsl:value-of select="$policy/policyNo"/>
                    </ProposalContNo>

                    <AccName>
                        <xsl:value-of select="$bills/otherAccountId"/>
                    </AccName>
                </LCCont>

                <LKTransTrackses>
                    <LKTransTracks>

                        <!--交易流水号 保险订单号-->
                        <TransNo>
                            <xsl:value-of select="$head/reqMsgId"/>
                        </TransNo>
                        <!--交易编码-->
                        <TransCode>
                            <xsl:value-of select="$head/function"/>
                        </TransCode>

                        <!-- 日期-->
                        <TransDate>
                            <xsl:value-of
                                    select="java:com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.getExactDate($head/transTime)"/>
                        </TransDate>
                        <!--时间-->
                        <TransTime>
                            <xsl:value-of
                                    select="java:com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.getExactTime($head/transTime)"/>
                        </TransTime>

                        <!-- 投保单号 -->
                        <ProposalNo>
                            <xsl:value-of select="$policy/policyNo"/>
                        </ProposalNo>
                        <BankCode>05</BankCode>
                        <BankBranch>26</BankBranch>
                        <BankNode>26</BankNode>
                        <SellsWay>1</SellsWay>
                    </LKTransTracks>
                </LKTransTrackses>
            </Body>
        </TranData>
    </xsl:template>
    <xsl:template name="tran_payAmt">
        <xsl:param name="payAmt"/>
        <xsl:choose>
            <xsl:when test="$payAmt &lt; 0">0.00</xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="$payAmt"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
</xsl:stylesheet>