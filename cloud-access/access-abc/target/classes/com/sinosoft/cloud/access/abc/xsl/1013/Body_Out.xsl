<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:java="http://xml.apache.org/xslt/java"
                exclude-result-prefixes="java">
    <xsl:template match="/">
        <xsl:variable name="head" select="TranData/Head"/>
        <xsl:variable name="RespWT" select="TranData/Body/RespWT"/><!--犹退pos018-->
        <xsl:variable name="RespExpirationApply" select="TranData/Body/RespExpirationApply"/><!--生存返回pos072-->
        <xsl:variable name="RespSurrenderApply" select="TranData/Body/RespSurrenderApply"/><!--退保申请pos089-->
        <xsl:variable name="resphead" select="TranData/Body/RespHead"/>
        <xsl:variable name="reqhead" select="TranData/Body/ReqHead"/>
        <ABCB2I>
            <Header>
                <xsl:choose>
                    <xsl:when test="$head/Flag=0">
                        <RetCode>000000</RetCode>
                        <RetMsg>交易成功</RetMsg>
                    </xsl:when>
                    <xsl:when test="$head/Flag=1">
                        <RetCode>009999</RetCode>
                        <RetMsg>
                            <xsl:value-of select="$head/Desc"/>
                        </RetMsg>
                    </xsl:when>
                </xsl:choose>
                <SerialNo>
                    <xsl:value-of select="$head/TranNo"/>
                </SerialNo>
                <InsuSerial>
                    <xsl:value-of select="$head/InsuSerial"/>
                </InsuSerial>
                <TransDate>
                    <xsl:value-of
                            select="java:com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.date10to8($head/TranDate)"/>
                </TransDate>
                <TransTime>
                    <xsl:value-of
                            select="java:com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.timeString10To8($head/TranTime)"/>
                </TransTime>
                <BankCode>
                    <xsl:value-of select="$head/BankCode"/>
                </BankCode>
                <CorpNo>
                    <xsl:value-of select="$head/CorpNo"/>
                </CorpNo>
                <TransCode>
                    <xsl:value-of select="$head/TransCode"/>
                </TransCode>
            </Header>
            <xsl:if test="$head/Flag=0">
                <App>
                    <Ret>
                        <xsl:if test="$resphead/TransID='POS018'or $reqhead/TransID='POS018'">
                            <InsuName>
                                <xsl:value-of select="$RespWT/RiskName"/>
                            </InsuName><!-- 险种名称 -->
                            <PolicyNo>
                                <xsl:value-of select="$RespWT/ContNo"/>
                            </PolicyNo><!-- 保单号 -->
                            <OccurBala>
                                <xsl:value-of select="$RespWT/OccurBala"/>
                            </OccurBala><!-- 拟领取金额 -->
                            <BQValidDate>
                                <xsl:value-of
                                        select="java:com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.date10to8($RespWT/BqValidDate)"/>
                                <!--<xsl:value-of select="$BQResult/ValidDate"/>-->
                            </BQValidDate><!-- 保单生效日 -->
                        </xsl:if>
                        <xsl:if test="$resphead/TransID='POS044'or $reqhead/TransID='POS044'">
                            <InsuName>
                                <xsl:value-of select="$RespExpirationApply/RiskName"/>
                            </InsuName><!-- 险种名称 -->
                            <PolicyNo>
                                <xsl:value-of select="$RespExpirationApply/ContNo"/>
                            </PolicyNo><!-- 保单号 -->
                            <OccurBala>
                                <xsl:value-of select="$RespExpirationApply/OccurBala"/>
                            </OccurBala><!-- 拟领取金额 -->
                            <BQValidDate>
                                <xsl:value-of
                                        select="java:com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.date10to8($RespExpirationApply/BqValidDate)"/>
                                <!--<xsl:value-of select="$BQResult/ValidDate"/>-->
                            </BQValidDate><!-- 保单生效日 -->
                        </xsl:if>
                        <xsl:if test="$resphead/TransID='POS089'or $reqhead/TransID='POS089'">
                            <InsuName>
                                <xsl:value-of select="$RespSurrenderApply/RiskName"/>
                            </InsuName><!-- 险种名称 -->
                            <PolicyNo>
                                <xsl:value-of select="$RespSurrenderApply/ContNo"/>
                            </PolicyNo><!-- 保单号 -->
                            <OccurBala>
                                <xsl:value-of select="$RespSurrenderApply/OccurBala"/>
                            </OccurBala><!-- 拟领取金额 -->
                            <BQValidDate>
                                <xsl:value-of
                                        select="java:com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.date10to8($RespSurrenderApply/ValidDate)"/>
                                <!--<xsl:value-of select="$BQResult/ValidDate"/>-->
                            </BQValidDate><!-- 保单生效日 -->
                        </xsl:if>
                        <PrntCount>4</PrntCount><!-- 逐行打印行数 -->
                        <Prnt1>　　　　　本人已确认以上内容代表本人真实意愿，相关信息真实无误。保险合同原件、身份证复印件、</Prnt1>
                        <Prnt2>　　　　　此次付款账户凭证复印件将委托xxx转交给xxxxxx保险股份有限公司，并接受具体付费</Prnt2>
                        <Prnt3>　　　　　金额以xxxxxx保险股份有限公司审核通过日期计算为准。</Prnt3>
                        <Prnt4>　　　　　xxxxxx客服热线95581或4007795581(人工服务时间为9:00-21:00)。</Prnt4>
                    </Ret>
                </App>
            </xsl:if>
        </ABCB2I>
    </xsl:template>
</xsl:stylesheet>