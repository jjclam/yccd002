<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:java="http://xml.apache.org/xslt/java"
                exclude-result-prefixes="java">
    <xsl:template match="/TranData">
        <xsl:variable name="lccont" select="Body/LCCont"/>
        <xsl:variable name="global" select="Body/Global"/>
        <xsl:variable name="bqresult" select="Body/BQResult"/>
        <!--�¼�-->
        <xsl:variable name="lKTransStatus" select="Body/LKTransStatus"/>
        <response>
            <Head>
                <version>
                    <xsl:value-of select="AliHead/version"/>
                </version>
                <function>
                    <xsl:value-of select="AliHead/function"/>
                </function>
                <transTime>
                    <xsl:value-of select="AliHead/transTime"/>
                </transTime>
                <transTimeZone>
                    <xsl:value-of select="AliHead/transTimeZone"/>
                </transTimeZone>
                <reqMsgId>
                    <xsl:value-of select="AliHead/reqMsgId"/>
                </reqMsgId>
                <format>
                    <xsl:value-of select="AliHead/format"/>
                </format>
                <signType>
                    <xsl:value-of select="AliHead/signType"/>
                </signType>
                <asyn>
                    <xsl:value-of select="AliHead/asyn"/>
                </asyn>
                <cid>
                    <xsl:value-of select="AliHead/cid"/>
                </cid>
            </Head>
            <body>
                <xsl:if test="Head/Flag=0">
                    <success>1</success>
                </xsl:if>
                <xsl:if test="Head/Flag=1">
                    <success>0</success>
                    <errorCode>AE10316060001005</errorCode>
                    <errorMessage>
                        <xsl:value-of select="Head/Desc"/>
                    </errorMessage>
                </xsl:if>
                <xsl:if test="Head/Flag=2">
                    <errorCode>AE10316060001005</errorCode>
                    <errorMessage>
                        <xsl:value-of select="Head/Desc"/>
                    </errorMessage>
                </xsl:if>
                <policyNo>
                    <xsl:value-of select="Body/LCCont/ThirdPartyOrderId"/>
                </policyNo>
                <proposalNo>
                    <xsl:value-of select="Body/LCCont/ContNo"/>
                </proposalNo>
            </body>
        </response>
    </xsl:template>
</xsl:stylesheet>