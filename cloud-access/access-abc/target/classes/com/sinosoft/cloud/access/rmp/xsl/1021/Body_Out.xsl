<?xml version="1.0" encoding="GBK"?>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:java="http://xml.apache.org/xslt/java" exclude-result-prefixes="java">
    <xsl:template match="/TranData">
        <xsl:variable name="LMriskchange" select="Body/LMriskchange"/>
        <xsl:variable name="mainRisk" select="Body/LCPols/LCPol[RiskCode=$LMriskchange/InputRiskCode]"/>
        <xsl:variable name="addtRisk" select="Body/LCPols/LCPol[RiskCode!=$LMriskchange/InputRiskCode]"/>
        <xsl:variable name="LCCustomerImpartParams" select="Body/LCCustomerImpartParamses/LCCustomerImpartParams"/>
        <xsl:variable name="LMRiskApp" select="Body/LMRiskApps/LMRiskApp"/>
        <xsl:variable name="LCAddress" select="Body/LCAddresses/LCAddress"/>
        <xsl:variable name="Reserve" select="Body/Reserves/Reserve"/>
        <xsl:variable name="appnt" select="Body/LCAppnt"/>
        <xsl:variable name="insured" select="Body/LCCont"/>
        <xsl:variable name="bnf" select="Body/LCBnfs/LCBnf"/>
        <xsl:variable name="loan" select="Body/LCPolicyInfo"/>
        <xsl:variable name="LKTransStatus" select="Body/LKTransStatuses/LKTransStatus"/>
        <ABCB2I>
            <Header>
                <xsl:if test="Head/Flag=0">
                    <RetCode>000000</RetCode>
                    <RetMsg>交易成功</RetMsg>
                </xsl:if>
                <xsl:if test="Head/Flag=1">
                    <RetCode>009999</RetCode>
                    <RetMsg>
                        <xsl:value-of select="Head/Desc"/>
                    </RetMsg>
                </xsl:if>
                <xsl:if test="Head/Flag=2">
                    <RetCode>009990</RetCode>
                    <RetMsg>
                        <xsl:value-of select="Head/Desc"/>
                    </RetMsg>
                </xsl:if>
                <SerialNo>
                    <xsl:value-of select="Head/TranNo"/>
                </SerialNo>
                <InsuSerial>
                    <xsl:value-of select="Head/InsuSerial"/>
                </InsuSerial>
                <TransDate>
                    <xsl:value-of
                            select="java:com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.date10to8(Head/TranDate)"/>
                </TransDate>
                <TransTime>
                    <xsl:value-of
                            select="java:com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.timeString10To8(Head/TranTime)"/>
                </TransTime>
                <BankCode>
                    <xsl:value-of select="Head/BankCode"/>
                </BankCode>
                <CorpNo>
                    <xsl:value-of select="Head/CorpNo"/>
                </CorpNo>
                <TransCode>
                    <xsl:value-of select="Head/TransCode"/>
                </TransCode>
            </Header>
            <xsl:if test="Head/Flag=0">
                <App>
                    <Ret>
                        <PolicyNo>
                            <xsl:value-of select="Body/LCCont/ContNo"/>
                        </PolicyNo>
                        <xsl:if test="Head/SourceType='ABC'">
                            <VchNo>
                                <xsl:value-of select="$LKTransStatus[1]/PrtNo"/>
                            </VchNo><!-- 保单印刷号 -->
                        </xsl:if>
                        <xsl:if test="Head/SourceType !='ABC'">
                            <VchNo/>
                        </xsl:if>
                        <RiskCode>
                            <xsl:value-of select="$mainRisk/RiskCode"/>
                        </RiskCode>
                        <RiskName>
                            <xsl:value-of select="$LMRiskApp[RiskCode=$mainRisk/RiskCode]/RiskName"/>
                        </RiskName>
                        <!-- 保单状态  00有效 01退保 02当日撤单 03犹撤 04满期给付 05状态不明确 06部分赎回 07理赔终止 08处理中 -->
                        <PolicyStatus>
                            <xsl:value-of
                                    select="Body/ResponseObj/PolicyStatus"/>
                        </PolicyStatus>
                        <!-- 保单质押状态  0-未质押；1-质押中 -->
                        <PolicyPledge>
                            <xsl:value-of select="Body/ResponseObj/PolicyPledge"/>
                        </PolicyPledge>
                        <AcceptDate>
                            <xsl:value-of select="java:com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.date10to8(Body/LCCont/PolApplyDate)"/>
                        </AcceptDate>
                        <PolicyBgnDate>
                            <xsl:value-of select="java:com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.date10to8($mainRisk/CValiDate)"/>
                        </PolicyBgnDate>
                        <PolicyEndDate>
                            <xsl:value-of select="java:com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.date10to8($mainRisk/EndDate)"/>
                        </PolicyEndDate>
                        <PolicyAmmount>
                            <xsl:value-of select="$mainRisk/Mult"/>
                        </PolicyAmmount><!-- 投保份数 -->
                        <Amt>
                            <xsl:value-of select="$mainRisk/Prem"/>
                            <!-- <xsl:value-of select="format-number($mainRisk/Prem,'#0')"/> -->
                        </Amt>
                        <Beamt> <!-- 特别注意 加上 format-number()函数就可以转换科学计数法为字符串输出 -->
                            <xsl:value-of select="format-number($mainRisk/Amnt,'#0.00')"/>
                        </Beamt>
                        <PolicyValue>
                            <xsl:value-of
                                    select="$Reserve[RiskCode=$mainRisk/RiskCode]/PolicyValue"/>
                        </PolicyValue><!-- 保单价值 -->
                        <AccountValue>
                            <xsl:value-of
                                    select="$Reserve[RiskCode=$mainRisk/RiskCode]/AccountValue"/>
                        </AccountValue><!-- 当前账户价值 -->
                        <xsl:variable name="riskCode">
                            <xsl:value-of select="$mainRisk/RiskCode"/>
                        </xsl:variable>
                        <xsl:choose>
                            <xsl:when test="$riskCode = '7058'or $riskCode = '1043'or $riskCode = '1040'">
                                <InsuDueDate>终身</InsuDueDate>
                            </xsl:when>
                            <xsl:otherwise>
                                <InsuDueDate>
                                    <xsl:call-template name="tran_insuYear">
                                        <xsl:with-param name="insuYearFlag">
                                            <xsl:value-of
                                                    select="$mainRisk/InsuYearFlag"/>
                                        </xsl:with-param>
                                        <xsl:with-param name="insuYear">
                                            <xsl:value-of
                                                    select="$mainRisk/InsuYear"></xsl:value-of>
                                        </xsl:with-param>
                                    </xsl:call-template>
                                </InsuDueDate>
                            </xsl:otherwise>
                        </xsl:choose><!-- 保险期间 -->
                        <!-- 缴费方式 -->
                        <PayType>
                            <xsl:call-template name="tran_paytype">
                                <xsl:with-param name="paytype">
                                    <xsl:value-of
                                            select="$mainRisk/PayIntv"/>
                                </xsl:with-param>
                            </xsl:call-template>
                        </PayType>
                        <!-- 缴费期间 -->
                        <PayDue>
                            <xsl:call-template name="tran_paydue">
                                <xsl:with-param name="payEndYearFlag">
                                    <xsl:value-of
                                            select="$mainRisk/PayEndYearFlag"/>
                                </xsl:with-param>
                                <xsl:with-param name="payEndYear">
                                    <xsl:value-of
                                            select="$mainRisk/PayEndYear"/>
                                </xsl:with-param>
                            </xsl:call-template>
                        </PayDue>
                        <Prem>
                            <xsl:value-of select="$mainRisk/Prem"/>
                        </Prem>
                        <PayAccount>
                            <xsl:value-of select="Body/LCCont/NewBankAccNo"/>
                        </PayAccount><!-- 缴费账户 -->
                        <PayProv>
                            <xsl:value-of select="$LKTransStatus[1]/BankBranch"/>
                        </PayProv><!-- 缴费省市代码 -->
                        <PayBranch>
                            <xsl:value-of select="$LKTransStatus[1]/BankNode"/>
                        </PayBranch><!-- 缴费网点号 -->
                        <!-- 投保人部分 -->
                        <Appl>
                            <IDKind>
                                <xsl:choose>
                                    <xsl:when test="Body/DateFlag = '2' and Body/ContType = '2'">
                                        <xsl:call-template name="tran_idtype_TX">
                                            <xsl:with-param name="idtype">
                                                <xsl:value-of select="$appnt/IDType"/>
                                            </xsl:with-param>
                                        </xsl:call-template>
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <xsl:call-template name="tran_idtype_newybt">
                                            <xsl:with-param name="idtype">
                                                <xsl:value-of select="$appnt/IDType"/>
                                            </xsl:with-param>
                                        </xsl:call-template>
                                    </xsl:otherwise>
                                </xsl:choose>
                            </IDKind>
                            <IDCode>
                                <xsl:value-of select="$appnt/IDNo"/>
                            </IDCode>
                            <Name>
                                <xsl:value-of select="$appnt/AppntName"/>
                            </Name>
                            <Sex>
                                <xsl:choose>
                                    <xsl:when test="Body/DateFlag = '2' and Body/ContType = '2'">
                                        <xsl:call-template name="tran_sex_TX">
                                            <xsl:with-param name="sex">
                                                <xsl:value-of select="$appnt/AppntSex"/>
                                            </xsl:with-param>
                                        </xsl:call-template>
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <xsl:value-of select="$appnt/AppntSex"/>
                                    </xsl:otherwise>
                                </xsl:choose>
                            </Sex>
                            <Birthday>
                                <xsl:value-of
                                        select="java:com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.date10to8($appnt/AppntBirthday)"/>
                            </Birthday>
                            <Address>
                                <xsl:value-of select="$LCAddress[1]/HomeAddress"/>
                            </Address>
                            <ZipCode>
                                <xsl:value-of select="$LCAddress[1]/ZipCode"/>
                            </ZipCode>
                            <Email>
                                <xsl:value-of select="$LCAddress[1]/EMail"/>
                            </Email>
                            <Phone>
                                <xsl:value-of select="$LCAddress[1]/Phone"/>
                            </Phone>
                            <Mobile>
                                <xsl:value-of select="$LCAddress[1]/Mobile"/>
                            </Mobile>
                            <!-- 投保人收人 -->
                            <xsl:choose>
                                <xsl:when test="$riskCode = '6807'">
                                    <AnnualIncome/>
                                </xsl:when>
                                <xsl:otherwise>
                                    <AnnualIncome>
                                        <xsl:call-template name="tran_AnnualIncome">
                                            <xsl:with-param name="tAnnualIncome">
                                                <xsl:value-of select="$LCCustomerImpartParams[1]/ImpartParam"/>
                                            </xsl:with-param>
                                        </xsl:call-template>
                                    </AnnualIncome>
                                </xsl:otherwise>
                            </xsl:choose>
                            <Country>
                                <xsl:call-template name="tran_nationality_opposite">
                                    <xsl:with-param name="nationality" select="$appnt/NativePlace"/>
                                </xsl:call-template>
                            </Country>
                            <!--  投保人与被保人关系 -->
                            <RelaToInsured>
                                <xsl:call-template name="tran_OLrela">
                                    <xsl:with-param name="rela">
                                        <xsl:value-of select="$appnt/RelatToInsu"/>
                                    </xsl:with-param>
                                </xsl:call-template>
                            </RelaToInsured>
                        </Appl>
                        <!-- 被保人信息 -->
                        <Insu>
                            <Name>
                                <xsl:value-of select="$insured/InsuredName"/>
                            </Name><!-- 被保人姓名 -->
                            <Sex>
                                <xsl:choose>
                                    <xsl:when test="Body/DateFlag = '2' and Body/ContType = '2'">
                                        <xsl:call-template name="tran_sex_TX">
                                            <xsl:with-param name="sex">
                                                <xsl:value-of select="$insured/InsuredSex"/>
                                            </xsl:with-param>
                                        </xsl:call-template>
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <xsl:value-of select="$insured/InsuredSex"/>
                                    </xsl:otherwise>
                                </xsl:choose>
                            </Sex><!-- 被保人性别 -->
                            <!-- 被保人证件类型 -->
                            <IDKind>
                                <xsl:choose>
                                    <xsl:when test="Body/DateFlag = '2' and Body/ContType = '2'">
                                        <xsl:call-template name="tran_idtype_TX">
                                            <xsl:with-param name="idtype">
                                                <xsl:value-of select="$insured/InsuredIDType"/>
                                            </xsl:with-param>
                                        </xsl:call-template>
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <xsl:call-template name="tran_idtype_newybt">
                                            <xsl:with-param name="idtype">
                                                <xsl:value-of select="$insured/InsuredIDType"/>
                                            </xsl:with-param>
                                        </xsl:call-template>
                                    </xsl:otherwise>
                                </xsl:choose>
                            </IDKind>
                            <IDCode>
                                <xsl:value-of select="$insured/InsuredIDNo"/>
                            </IDCode><!-- 被保人证件号码 -->
                            <Birthday>
                                <xsl:value-of
                                        select="java:com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.date10to8($insured/InsuredBirthday)"/>
                            </Birthday><!-- 被保人出生日期 -->
                        </Insu>
                        <!-- 受益人信息 -->

                        <xsl:choose>
                            <xsl:when test="$riskCode = '6807'">
                                <Bnfs>
                                    <Count>2</Count>
                                    <Name1><xsl:value-of select="$loan/LendCom"/></Name1>
                                    <Name2>法定受益人</Name2>
                                </Bnfs>
                            </xsl:when>
                            <xsl:otherwise>
                                <Bnfs>
                                    <Count>
                                        <xsl:value-of select="count($bnf)"/>
                                    </Count><!-- 受益人个数 -->
                                    <xsl:for-each select="$bnf">
                                        <xsl:variable name="bnfType">
                                            <xsl:choose>
                                                <xsl:when test="Body/DateFlag = '2' and Body/ContType = '2'">
                                                    <xsl:call-template name="tran_bnfType_TX">
                                                        <xsl:with-param name="bnfType">
                                                            <xsl:value-of select="BnfType"/>
                                                        </xsl:with-param>
                                                    </xsl:call-template>
                                                </xsl:when>
                                                <xsl:otherwise>
                                                    <xsl:value-of select="BnfType"/>
                                                </xsl:otherwise>
                                            </xsl:choose>
                                        </xsl:variable>
                                        <xsl:variable name="sex">
                                            <xsl:choose>
                                                <xsl:when test="Body/DateFlag = '2' and Body/ContType = '2'">
                                                    <xsl:call-template name="tran_sex_TX">
                                                        <xsl:with-param name="sex">
                                                            <xsl:value-of select="Sex"/>
                                                        </xsl:with-param>
                                                    </xsl:call-template>
                                                </xsl:when>
                                                <xsl:otherwise>
                                                    <xsl:value-of select="Sex"/>
                                                </xsl:otherwise>
                                            </xsl:choose>
                                        </xsl:variable>
                                        <xsl:call-template name="generateBTMark">
                                            <xsl:with-param name="value" select="$bnfType"/>
                                            <xsl:with-param name="number" select="position()"/>
                                            <xsl:with-param name="name" select="string('Type')"/>
                                        </xsl:call-template><!-- 受益人类型 -->
                                        <xsl:call-template name="generateBTMark">
                                            <xsl:with-param name="value" select="Name"/>
                                            <xsl:with-param name="number" select="position()"/>
                                            <xsl:with-param name="name" select="string('Name')"/>
                                        </xsl:call-template><!-- 受益人姓名 -->
                                        <xsl:call-template name="generateBTMark">  <!--1234-->
                                            <xsl:with-param name="value" select="$sex"/>
                                            <xsl:with-param name="number" select="position()"/>
                                            <xsl:with-param name="name" select="string('Sex')"/>
                                        </xsl:call-template><!-- 性别 -->
                                        <xsl:call-template name="generateBTMark">
                                            <xsl:with-param name="value"
                                                            select="java:com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.date10to8(Birthday)"/>
                                            <xsl:with-param name="number" select="position()"/>
                                            <xsl:with-param name="name" select="string('Birthday')"/>
                                        </xsl:call-template><!-- 出生日期 -->
                                        <xsl:variable name="idType">
                                            <xsl:choose>
                                                <xsl:when test="Body/DateFlag = '2' and Body/ContType = '2'">
                                                    <xsl:call-template name="tran_idtype_TX">
                                                        <xsl:with-param name="idtype">
                                                            <xsl:value-of select="IDType"/>
                                                        </xsl:with-param>
                                                    </xsl:call-template>
                                                </xsl:when>
                                                <xsl:otherwise>
                                                    <xsl:call-template name="tran_idtype_newybt">
                                                        <xsl:with-param name="idtype">
                                                            <xsl:value-of select="IDType"/>
                                                        </xsl:with-param>
                                                    </xsl:call-template>
                                                </xsl:otherwise>
                                            </xsl:choose>
                                        </xsl:variable>
                                        <xsl:call-template name="generateBTMark">
                                            <xsl:with-param name="value" select="$idType"/>
                                            <xsl:with-param name="number" select="position()"/>
                                            <xsl:with-param name="name" select="string('IDKind')"/>
                                        </xsl:call-template><!-- 证件类型 -->
                                        <xsl:call-template name="generateBTMark">
                                            <xsl:with-param name="value" select="IDNo"/>
                                            <xsl:with-param name="number" select="position()"/>
                                            <xsl:with-param name="name" select="string('IDCode')"/>
                                        </xsl:call-template><!-- 证件号码 -->
                                        <xsl:variable name="relationToInsured">
                                            <xsl:call-template name="tran_OLrela">
                                                <xsl:with-param name="rela" select="RelationToInsured"/>
                                            </xsl:call-template>
                                        </xsl:variable>
                                        <xsl:call-template name="generateBTMark">
                                            <xsl:with-param name="value" select="$relationToInsured"/>
                                            <xsl:with-param name="number" select="position()"/>
                                            <xsl:with-param name="name" select="string('RelationToInsured')"/>
                                        </xsl:call-template><!-- 与被保人关系 -->
                                        <xsl:call-template name="generateBTMark">
                                            <xsl:with-param name="value" select="BnfGrade"/>
                                            <xsl:with-param name="number" select="position()"/>
                                            <xsl:with-param name="name" select="string('Sequence')"/>
                                        </xsl:call-template><!-- 受益人受益顺序 -->
                                        <xsl:call-template name="generateBTMark">
                                            <xsl:with-param name="value" select="BnfLot*100"/>
                                            <xsl:with-param name="number" select="position()"/>
                                            <xsl:with-param name="name" select="string('Prop')"/>
                                        </xsl:call-template><!-- 受益人受益比例 -->
                                    </xsl:for-each>
                                </Bnfs>
                            </xsl:otherwise>
                        </xsl:choose>
                        <xsl:if test="Head/SourceType='ABC_Supotc'">
                            <Addt>
                                <Count>
                                    <xsl:value-of
                                            select="count($addtRisk)"/>
                                </Count>
                                <xsl:for-each
                                        select="$addtRisk">
                                    <xsl:variable name="riskCode" select="RiskCode"/>
                                    <xsl:call-template name="generateBTMark">
                                        <xsl:with-param name="value" select="$LMRiskApp[RiskCode=$riskCode]/RiskName"/>
                                        <xsl:with-param name="number" select="position()"/>
                                        <xsl:with-param name="name" select="string('Name')"/>
                                    </xsl:call-template>
                                    <xsl:call-template name="generateBTMark">
                                        <xsl:with-param name="value" select="Mult"/>
                                        <xsl:with-param name="number" select="position()"/>
                                        <xsl:with-param name="name" select="string('Share')"/>
                                    </xsl:call-template>
                                    <xsl:call-template name="generateBTMark">
                                        <xsl:with-param name="value" select="Prem"/>
                                        <xsl:with-param name="number" select="position()"/>
                                        <xsl:with-param name="name" select="string('Prem')"/>
                                    </xsl:call-template>
                                    <xsl:call-template name="generateBTMark">
                                        <xsl:with-param name="value" select="Amnt"/>
                                        <xsl:with-param name="number" select="position()"/>
                                        <xsl:with-param name="name" select="string('Beamt')"/>
                                    </xsl:call-template>
                                    <xsl:call-template name="generateBTMark">
                                        <xsl:with-param name="value" select="$Reserve[RiskCode=$riskCode]/PolicyValue"/>
                                        <xsl:with-param name="number" select="position()"/>
                                        <xsl:with-param name="name" select="string('PolicyValue')"/>
                                    </xsl:call-template>
                                    <xsl:call-template name="generateBTMark">
                                        <xsl:with-param name="value"
                                                        select="$Reserve[RiskCode=$riskCode]/AccountValue"/>
                                        <xsl:with-param name="number" select="position()"/>
                                        <xsl:with-param name="name" select="string('AccountValue')"/>
                                    </xsl:call-template>
                                    <xsl:variable name="payDueDate">
                                        <xsl:choose>
                                            <xsl:when test="PayEndYear='1000'">
                                                <xsl:value-of select="1"/>
                                            </xsl:when>
                                            <xsl:otherwise>
                                                <xsl:value-of select="PayEndYear"/>
                                            </xsl:otherwise>
                                        </xsl:choose>
                                    </xsl:variable>
                                    <xsl:call-template name="generateBTMark">
                                        <xsl:with-param name="value" select="$payDueDate"/>
                                        <xsl:with-param name="number" select="position()"/>
                                        <xsl:with-param name="name" select="string('PayDueDate')"/>
                                    </xsl:call-template>
                                    <xsl:variable name="payIntv">
                                        <xsl:call-template name="tran_paytype">
                                            <xsl:with-param name="paytype">
                                                <xsl:value-of
                                                        select="PayIntv"/>
                                            </xsl:with-param>
                                        </xsl:call-template>
                                    </xsl:variable>
                                    <xsl:call-template name="generateBTMark">
                                        <xsl:with-param name="value" select="$payIntv"/>
                                        <xsl:with-param name="number" select="position()"/>
                                        <xsl:with-param name="name" select="string('PayType')"/>
                                    </xsl:call-template>
                                    <xsl:call-template name="generateBTMark">
                                        <xsl:with-param name="value" select="EndDate"/>
                                        <xsl:with-param name="number" select="position()"/>
                                        <xsl:with-param name="name" select="string('PolicyEndDate')"/>
                                    </xsl:call-template>
                                    <xsl:variable name="insuDueDate">
                                        <xsl:call-template name="tran_insuYear">
                                            <xsl:with-param name="insuYearFlag">
                                                <xsl:value-of select="InsuYearFlag"/>
                                            </xsl:with-param>
                                            <xsl:with-param name="insuYear">
                                                <xsl:value-of select="InsuYear"/>
                                            </xsl:with-param>
                                        </xsl:call-template>
                                    </xsl:variable>
                                    <xsl:call-template name="generateBTMark">
                                        <xsl:with-param name="value" select="$insuDueDate"/>
                                        <xsl:with-param name="number" select="position()"/>
                                        <xsl:with-param name="name" select="string('InsuDueDate')"/>
                                    </xsl:call-template>
                                    <xsl:call-template name="generateBTMark">
                                        <xsl:with-param name="value" select="RiskCode"/>
                                        <xsl:with-param name="number" select="position()"/>
                                        <xsl:with-param name="name" select="string('RiskCode')"/>
                                    </xsl:call-template>
                                </xsl:for-each>
                            </Addt>
                        </xsl:if>
                        <PrntCount>0</PrntCount>
                        <Prnt1></Prnt1>
                        <Prnt2></Prnt2>
                        <Prnt3></Prnt3>
                        <Prnt4></Prnt4>
                        <Prnt5></Prnt5>
                        <Prnt6></Prnt6>
                    </Ret>
                </App>
            </xsl:if>
        </ABCB2I>
    </xsl:template>

    <!-- 缴费期间 -->
    <xsl:template name="tran_paydue">
        <xsl:param name="payEndYear"/>
        <xsl:param name="payEndYearFlag"/>
        <xsl:choose>
            <xsl:when test="$payEndYear = '1000'">趸交</xsl:when>
            <xsl:when test="$payEndYearFlag = 'Y'"><xsl:value-of select="$payEndYear"/>年</xsl:when>
            <xsl:when test="$payEndYearFlag = 'M'"><xsl:value-of select="$payEndYear"/>月</xsl:when>
            <xsl:when test="$payEndYearFlag = 'D'"><xsl:value-of select="$payEndYear"/>天 </xsl:when>

            <xsl:otherwise><xsl:value-of select="$payEndYear"/>周岁
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    <!-- 保险期间 -->
    <xsl:template name="tran_insuYear">
        <xsl:param name="insuYearFlag"></xsl:param>
        <xsl:param name="insuYear"></xsl:param>
        <xsl:choose>
            <xsl:when test="$insuYearFlag='A'">至<xsl:value-of select="$insuYear"/>周岁</xsl:when>
            <xsl:when test="$insuYearFlag='Y'"><xsl:value-of select="$insuYear"/>年</xsl:when>
            <!--<xsl:when test="$insuYearFlag='M'"><xsl:value-of select="$insuYear"/>月</xsl:when>-->
            <xsl:when test="$insuYearFlag='D'"><xsl:value-of select="$insuYear"/>日</xsl:when>
            <xsl:when test="$insuYearFlag='M'"><xsl:number value="$insuYear div 12"/>年</xsl:when>
        </xsl:choose>
    </xsl:template>
    <!--新一代银保通证件类型转换	-->
    <xsl:template name="tran_idtype_newybt">
        <xsl:param name="idtype">0</xsl:param>
        <xsl:if test="$idtype =0">110001</xsl:if>
        <xsl:if test="$idtype =1">110023</xsl:if>
        <xsl:if test="$idtype =2">110027</xsl:if>
        <xsl:if test="$idtype =3">119999</xsl:if>
        <xsl:if test="$idtype =4">110005</xsl:if>
        <xsl:if test="$idtype =5">119999</xsl:if>
        <xsl:if test="$idtype =6">119999</xsl:if>
        <xsl:if test="$idtype =7">110021</xsl:if>
        <xsl:if test="$idtype =8">119999</xsl:if>
        <xsl:if test="$idtype =9">119999</xsl:if>
        <xsl:if test="$idtype =10">110033</xsl:if>
        <xsl:if test="$idtype =11">119999</xsl:if>
        <xsl:if test="$idtype =12">110003</xsl:if>
        <xsl:if test="$idtype =13">110031</xsl:if>
        <xsl:if test="$idtype =15">110019</xsl:if>
        <xsl:if test="$idtype =16">110021</xsl:if>
        <xsl:if test="$idtype =17">110037</xsl:if>
        <xsl:if test="$idtype =111">110001</xsl:if>
        <xsl:if test="$idtype =414">110023</xsl:if>
        <xsl:if test="$idtype =114">110027</xsl:if>
        <xsl:if test="$idtype =335">119999</xsl:if>
        <xsl:if test="$idtype =113">110005</xsl:if>
        <xsl:if test="$idtype =133">119999</xsl:if>
        <xsl:if test="$idtype =131">119999</xsl:if>
        <xsl:if test="$idtype =990">110021</xsl:if>
        <xsl:if test="$idtype =990">119999</xsl:if>
        <xsl:if test="$idtype =990">119999</xsl:if>
        <xsl:if test="$idtype =516">119999</xsl:if>
        <xsl:if test="$idtype =112">110003</xsl:if>
        <xsl:if test="$idtype =123">110031</xsl:if>
        <xsl:if test="$idtype =511">110021</xsl:if>
        <xsl:if test="$idtype =553">110037</xsl:if>
        <xsl:if test="$idtype =117">119999</xsl:if>
        <xsl:if test="$idtype =513">110019</xsl:if>
    </xsl:template>

    <!--调用团险的转码-->
    <xsl:template name="tran_idtype_TX">
        <xsl:param name="idtype">0</xsl:param>
        <xsl:if test="$idtype =111">110001</xsl:if>
        <xsl:if test="$idtype =414">110023</xsl:if>
        <xsl:if test="$idtype =114">110027</xsl:if>
        <xsl:if test="$idtype =335">119999</xsl:if>
        <xsl:if test="$idtype =113">110005</xsl:if>
        <xsl:if test="$idtype =133">119999</xsl:if>
        <xsl:if test="$idtype =131">119999</xsl:if>
        <xsl:if test="$idtype =990">110021</xsl:if>
        <xsl:if test="$idtype =990">119999</xsl:if>
        <xsl:if test="$idtype =990">119999</xsl:if>
        <xsl:if test="$idtype =516">119999</xsl:if>
        <xsl:if test="$idtype =112">110003</xsl:if>
        <xsl:if test="$idtype =123">110031</xsl:if>
        <xsl:if test="$idtype =511">110021</xsl:if>
        <xsl:if test="$idtype =553">110037</xsl:if>
        <xsl:if test="$idtype =117">119999</xsl:if>
        <xsl:if test="$idtype =513">110019</xsl:if>

        <!--<xsl:if test="$idtype =117">14</xsl:if>-->
        <!--<xsl:if test="$idtype =513">15</xsl:if>-->
    </xsl:template>
    <!-- 保单的缴费方式 -->
    <xsl:template name="tran_paytype">
        <xsl:param name="paytype"></xsl:param>
        <xsl:choose>
            <xsl:when test="$paytype='-1'">0</xsl:when>
            <xsl:when test="$paytype='0'">1</xsl:when>
            <xsl:when test="$paytype='1'">2</xsl:when>
            <xsl:when test="$paytype='3'">3</xsl:when>
            <xsl:when test="$paytype='6'">4</xsl:when>
            <xsl:when test="$paytype='12'">5</xsl:when>
        </xsl:choose>
    </xsl:template>
    <!-- 投保人年收入 -->
    <xsl:template name="tran_AnnualIncome">
        <xsl:param name="tAnnualIncome">0</xsl:param>
        <xsl:choose>
            <xsl:when test="$tAnnualIncome = '*'"></xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="concat($tAnnualIncome,'万')"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    <!--国籍反转换（适应网银查询）-->
    <xsl:template name="tran_nationality_opposite">
        <xsl:param name="nationality">CHN</xsl:param>
        <xsl:choose>
            <xsl:when test="$nationality = 	'AFG'">004</xsl:when>
            <xsl:when test="$nationality = 	'ALB'">008</xsl:when>
            <xsl:when test="$nationality = 	'ATA'">010</xsl:when>
            <xsl:when test="$nationality = 	'DZA'">012</xsl:when>
            <xsl:when test="$nationality = 	'ASM'">016</xsl:when>
            <xsl:when test="$nationality = 	'AND'">020</xsl:when>
            <xsl:when test="$nationality = 	'AGO'">024</xsl:when>
            <xsl:when test="$nationality = 	'ATG'">028</xsl:when>
            <xsl:when test="$nationality = 	'AZE'">031</xsl:when>
            <xsl:when test="$nationality = 	'ARG'">032</xsl:when>
            <xsl:when test="$nationality = 	'AUS'">036</xsl:when>
            <xsl:when test="$nationality = 	'AUT'">040</xsl:when>
            <xsl:when test="$nationality = 	'BHS'">044</xsl:when>
            <xsl:when test="$nationality = 	'BHR'">048</xsl:when>
            <xsl:when test="$nationality = 	'BGD'">050</xsl:when>
            <xsl:when test="$nationality = 	'ARM'">051</xsl:when>
            <xsl:when test="$nationality = 	'BRB'">052</xsl:when>
            <xsl:when test="$nationality = 	'BEL'">056</xsl:when>
            <xsl:when test="$nationality = 	'BMU'">060</xsl:when>
            <xsl:when test="$nationality = 	'BTN'">064</xsl:when>
            <xsl:when test="$nationality = 	'BOL'">068</xsl:when>
            <xsl:when test="$nationality = 	'BIH'">070</xsl:when>
            <xsl:when test="$nationality = 	'BWA'">072</xsl:when>
            <xsl:when test="$nationality = 	'BVT'">074</xsl:when>
            <xsl:when test="$nationality = 	'BRA'">076</xsl:when>
            <xsl:when test="$nationality = 	'BLZ'">084</xsl:when>
            <xsl:when test="$nationality = 	'IOT'">086</xsl:when>
            <xsl:when test="$nationality = 	'SLB'">090</xsl:when>
            <xsl:when test="$nationality = 	'VGB'">092</xsl:when>
            <xsl:when test="$nationality = 	'BRN'">096</xsl:when>
            <xsl:when test="$nationality = 	'BGR'">100</xsl:when>
            <xsl:when test="$nationality = 	'MMR'">104</xsl:when>
            <xsl:when test="$nationality = 	'BDI'">108</xsl:when>
            <xsl:when test="$nationality = 	'BLR'">112</xsl:when>
            <xsl:when test="$nationality = 	'KHM'">116</xsl:when>
            <xsl:when test="$nationality = 	'CMR'">120</xsl:when>
            <xsl:when test="$nationality = 	'CAN'">124</xsl:when>
            <xsl:when test="$nationality = 	'CPV'">132</xsl:when>
            <xsl:when test="$nationality = 	'CYM'">136</xsl:when>
            <xsl:when test="$nationality = 	'CAF'">140</xsl:when>
            <xsl:when test="$nationality = 	'LKA'">144</xsl:when>
            <xsl:when test="$nationality = 	'TCD'">148</xsl:when>
            <xsl:when test="$nationality = 	'CHL'">152</xsl:when>
            <xsl:when test="$nationality = 	'CHN'">156</xsl:when>
            <xsl:when test="$nationality = 	'TWN'">158</xsl:when>
            <xsl:when test="$nationality = 	'CSR'">162</xsl:when>
            <xsl:when test="$nationality = 	'CCK'">166</xsl:when>
            <xsl:when test="$nationality = 	'COL'">170</xsl:when>
            <xsl:when test="$nationality = 	'COM'">174</xsl:when>
            <xsl:when test="$nationality = 	'MYT'">175</xsl:when>
            <xsl:when test="$nationality = 	'COG'">178</xsl:when>
            <xsl:when test="$nationality = 	'COD'">180</xsl:when>
            <xsl:when test="$nationality = 	'COK'">184</xsl:when>
            <xsl:when test="$nationality = 	'CR'">188</xsl:when>
            <xsl:when test="$nationality = 	'HRV'">191</xsl:when>
            <xsl:when test="$nationality = 	'CUB'">192</xsl:when>
            <xsl:when test="$nationality = 	'CYP'">196</xsl:when>
            <xsl:when test="$nationality = 	'CZE'">203</xsl:when>
            <xsl:when test="$nationality = 	'BEN'">204</xsl:when>
            <xsl:when test="$nationality = 	'DNK'">208</xsl:when>
            <xsl:when test="$nationality = 	'DMA'">212</xsl:when>
            <xsl:when test="$nationality = 	'DOM'">214</xsl:when>
            <xsl:when test="$nationality = 	'ECU'">218</xsl:when>
            <xsl:when test="$nationality = 	'SLV'">222</xsl:when>
            <xsl:when test="$nationality = 	'GNQ'">226</xsl:when>
            <xsl:when test="$nationality = 	'ETH'">231</xsl:when>
            <xsl:when test="$nationality = 	'ERI'">232</xsl:when>
            <xsl:when test="$nationality = 	'EST'">233</xsl:when>
            <xsl:when test="$nationality = 	'FRO'">234</xsl:when>
            <xsl:when test="$nationality = 	'FLK'">238</xsl:when>
            <xsl:when test="$nationality = 	'SGS'">239</xsl:when>
            <xsl:when test="$nationality = 	'FJI'">242</xsl:when>
            <xsl:when test="$nationality = 	'FIN'">246</xsl:when>
            <xsl:when test="$nationality = 	'ALA'">248</xsl:when>
            <xsl:when test="$nationality = 	'FRA'">250</xsl:when>
            <xsl:when test="$nationality = 	'GUF'">254</xsl:when>
            <xsl:when test="$nationality = 	'PYF'">258</xsl:when>
            <xsl:when test="$nationality = 	'ATF'">260</xsl:when>
            <xsl:when test="$nationality = 	'DJI'">262</xsl:when>
            <xsl:when test="$nationality = 	'GAB'">266</xsl:when>
            <xsl:when test="$nationality = 	'GEO'">268</xsl:when>
            <xsl:when test="$nationality = 	'GMB'">270</xsl:when>
            <xsl:when test="$nationality = 	'PST'">275</xsl:when>
            <xsl:when test="$nationality = 	'DEU'">276</xsl:when>
            <xsl:when test="$nationality = 	'GHA'">288</xsl:when>
            <xsl:when test="$nationality = 	'GIB'">292</xsl:when>
            <xsl:when test="$nationality = 	'KIR'">296</xsl:when>
            <xsl:when test="$nationality = 	'GRC'">300</xsl:when>
            <xsl:when test="$nationality = 	'GRL'">304</xsl:when>
            <xsl:when test="$nationality = 	'GRD'">308</xsl:when>
            <xsl:when test="$nationality = 	'GLP'">312</xsl:when>
            <xsl:when test="$nationality = 	'GUM'">316</xsl:when>
            <xsl:when test="$nationality = 	'GTM'">320</xsl:when>
            <xsl:when test="$nationality = 	'GIN'">324</xsl:when>
            <xsl:when test="$nationality = 	'GUY'">328</xsl:when>
            <xsl:when test="$nationality = 	'HTI'">332</xsl:when>
            <xsl:when test="$nationality = 	'HMD'">334</xsl:when>
            <xsl:when test="$nationality = 	'VAT'">336</xsl:when>
            <xsl:when test="$nationality = 	'HND'">340</xsl:when>
            <xsl:when test="$nationality = 	'HKG'">344</xsl:when>
            <xsl:when test="$nationality = 	'HUN'">348</xsl:when>
            <xsl:when test="$nationality = 	'ISL'">352</xsl:when>
            <xsl:when test="$nationality = 	'IND'">356</xsl:when>
            <xsl:when test="$nationality = 	'IDN'">360</xsl:when>
            <xsl:when test="$nationality = 	'IRN'">364</xsl:when>
            <xsl:when test="$nationality = 	'IRQ'">368</xsl:when>
            <xsl:when test="$nationality = 	'IRL'">372</xsl:when>
            <xsl:when test="$nationality = 	'ISR'">376</xsl:when>
            <xsl:when test="$nationality = 	'ITA'">380</xsl:when>
            <xsl:when test="$nationality = 	'CIV'">384</xsl:when>
            <xsl:when test="$nationality = 	'JAM'">388</xsl:when>
            <xsl:when test="$nationality = 	'JPN'">392</xsl:when>
            <xsl:when test="$nationality = 	'KAZ'">398</xsl:when>
            <xsl:when test="$nationality = 	'JOR'">400</xsl:when>
            <xsl:when test="$nationality = 	'KEN'">404</xsl:when>
            <xsl:when test="$nationality = 	'PRK'">408</xsl:when>
            <xsl:when test="$nationality = 	'KOR'">410</xsl:when>
            <xsl:when test="$nationality = 	'KWT'">414</xsl:when>
            <xsl:when test="$nationality = 	'KGZ'">417</xsl:when>
            <xsl:when test="$nationality = 	'LAO'">418</xsl:when>
            <xsl:when test="$nationality = 	'LBN'">422</xsl:when>
            <xsl:when test="$nationality = 	'LSO'">426</xsl:when>
            <xsl:when test="$nationality = 	'LVA'">428</xsl:when>
            <xsl:when test="$nationality = 	'LBR'">430</xsl:when>
            <xsl:when test="$nationality = 	'LBY'">434</xsl:when>
            <xsl:when test="$nationality = 	'LIE'">438</xsl:when>
            <xsl:when test="$nationality = 	'LTU'">440</xsl:when>
            <xsl:when test="$nationality = 	'LUX'">442</xsl:when>
            <xsl:when test="$nationality = 	'MAC'">446</xsl:when>
            <xsl:when test="$nationality = 	'MDG'">450</xsl:when>
            <xsl:when test="$nationality = 	'MWI'">454</xsl:when>
            <xsl:when test="$nationality = 	'MYS'">458</xsl:when>
            <xsl:when test="$nationality = 	'MDV'">462</xsl:when>
            <xsl:when test="$nationality = 	'MLI'">466</xsl:when>
            <xsl:when test="$nationality = 	'MLT'">470</xsl:when>
            <xsl:when test="$nationality = 	'MTQ'">474</xsl:when>
            <xsl:when test="$nationality = 	'MRT'">478</xsl:when>
            <xsl:when test="$nationality = 	'MUS'">480</xsl:when>
            <xsl:when test="$nationality = 	'MEX'">484</xsl:when>
            <xsl:when test="$nationality = 	'MCO'">492</xsl:when>
            <xsl:when test="$nationality = 	'MNG'">496</xsl:when>
            <xsl:when test="$nationality = 	'MDA'">498</xsl:when>
            <xsl:when test="$nationality = 	'MNE'">499</xsl:when>
            <xsl:when test="$nationality = 	'MSR'">500</xsl:when>
            <xsl:when test="$nationality = 	'MAR'">504</xsl:when>
            <xsl:when test="$nationality = 	'MOZ'">508</xsl:when>
            <xsl:when test="$nationality = 	'OMN'">512</xsl:when>
            <xsl:when test="$nationality = 	'NAM'">516</xsl:when>
            <xsl:when test="$nationality = 	'NRU'">520</xsl:when>
            <xsl:when test="$nationality = 	'NPL'">524</xsl:when>
            <xsl:when test="$nationality = 	'NLD'">528</xsl:when>
            <xsl:when test="$nationality = 	'ANT'">530</xsl:when>
            <xsl:when test="$nationality = 	'ABW'">533</xsl:when>
            <xsl:when test="$nationality = 	'NCL'">540</xsl:when>
            <xsl:when test="$nationality = 	'VUT'">548</xsl:when>
            <xsl:when test="$nationality = 	'NZL'">554</xsl:when>
            <xsl:when test="$nationality = 	'NIC'">558</xsl:when>
            <xsl:when test="$nationality = 	'NER'">562</xsl:when>
            <xsl:when test="$nationality = 	'NGA'">566</xsl:when>
            <xsl:when test="$nationality = 	'NIU'">570</xsl:when>
            <xsl:when test="$nationality = 	'NFK'">574</xsl:when>
            <xsl:when test="$nationality = 	'NOR'">578</xsl:when>
            <xsl:when test="$nationality = 	'MNP'">580</xsl:when>
            <xsl:when test="$nationality = 	'UMI'">581</xsl:when>
            <xsl:when test="$nationality = 	'FSM'">583</xsl:when>
            <xsl:when test="$nationality = 	'MHL'">584</xsl:when>
            <xsl:when test="$nationality = 	'PLW'">585</xsl:when>
            <xsl:when test="$nationality = 	'PAK'">586</xsl:when>
            <xsl:when test="$nationality = 	'PAN'">591</xsl:when>
            <xsl:when test="$nationality = 	'PNG'">598</xsl:when>
            <xsl:when test="$nationality = 	'PRY'">600</xsl:when>
            <xsl:when test="$nationality = 	'PER'">604</xsl:when>
            <xsl:when test="$nationality = 	'PHL'">608</xsl:when>
            <xsl:when test="$nationality = 	'PCN'">612</xsl:when>
            <xsl:when test="$nationality = 	'POL'">616</xsl:when>
            <xsl:when test="$nationality = 	'PRT'">620</xsl:when>
            <xsl:when test="$nationality = 	'GNB'">624</xsl:when>
            <xsl:when test="$nationality = 	'TMP'">626</xsl:when>
            <xsl:when test="$nationality = 	'PRI'">630</xsl:when>
            <xsl:when test="$nationality = 	'QAT'">634</xsl:when>
            <xsl:when test="$nationality = 	'REU'">638</xsl:when>
            <xsl:when test="$nationality = 	'ROM'">642</xsl:when>
            <xsl:when test="$nationality = 	'RUS'">643</xsl:when>
            <xsl:when test="$nationality = 	'RWA'">646</xsl:when>
            <xsl:when test="$nationality = 	'BLM'">652</xsl:when>
            <xsl:when test="$nationality = 	'SHN'">654</xsl:when>
            <xsl:when test="$nationality = 	'KNA'">659</xsl:when>
            <xsl:when test="$nationality = 	'AIA'">660</xsl:when>
            <xsl:when test="$nationality = 	'LCA'">662</xsl:when>
            <xsl:when test="$nationality = 	'MAF'">663</xsl:when>
            <xsl:when test="$nationality = 	'SPM'">666</xsl:when>
            <xsl:when test="$nationality = 	'VCT'">670</xsl:when>
            <xsl:when test="$nationality = 	'SMR'">674</xsl:when>
            <xsl:when test="$nationality = 	'STp'">678</xsl:when>
            <xsl:when test="$nationality = 	'SAU'">682</xsl:when>
            <xsl:when test="$nationality = 	'SEN'">686</xsl:when>
            <xsl:when test="$nationality = 	'SRB'">688</xsl:when>
            <xsl:when test="$nationality = 	'SYC'">690</xsl:when>
            <xsl:when test="$nationality = 	'SLE'">694</xsl:when>
            <xsl:when test="$nationality = 	'SGP'">702</xsl:when>
            <xsl:when test="$nationality = 	'SVK'">703</xsl:when>
            <xsl:when test="$nationality = 	'VNM'">704</xsl:when>
            <xsl:when test="$nationality = 	'SVN'">705</xsl:when>
            <xsl:when test="$nationality = 	'SOM'">706</xsl:when>
            <xsl:when test="$nationality = 	'ZAF'">710</xsl:when>
            <xsl:when test="$nationality = 	'ZWE'">716</xsl:when>
            <xsl:when test="$nationality = 	'ESP'">724</xsl:when>
            <xsl:when test="$nationality = 	'SSD'">728</xsl:when>
            <xsl:when test="$nationality = 	'SDN'">729</xsl:when>
            <xsl:when test="$nationality = 	'ESH'">732</xsl:when>
            <xsl:when test="$nationality = 	'SUR'">740</xsl:when>
            <xsl:when test="$nationality = 	'SJM'">744</xsl:when>
            <xsl:when test="$nationality = 	'SWZ'">748</xsl:when>
            <xsl:when test="$nationality = 	'SWE'">752</xsl:when>
            <xsl:when test="$nationality = 	'CHE'">756</xsl:when>
            <xsl:when test="$nationality = 	'SYR'">760</xsl:when>
            <xsl:when test="$nationality = 	'TJK'">762</xsl:when>
            <xsl:when test="$nationality = 	'THA'">764</xsl:when>
            <xsl:when test="$nationality = 	'TGO'">768</xsl:when>
            <xsl:when test="$nationality = 	'TKL'">772</xsl:when>
            <xsl:when test="$nationality = 	'TON'">776</xsl:when>
            <xsl:when test="$nationality = 	'TTO'">780</xsl:when>
            <xsl:when test="$nationality = 	'ARE'">784</xsl:when>
            <xsl:when test="$nationality = 	'TUN'">788</xsl:when>
            <xsl:when test="$nationality = 	'TUR'">792</xsl:when>
            <xsl:when test="$nationality = 	'TKM'">795</xsl:when>
            <xsl:when test="$nationality = 	'TCA'">796</xsl:when>
            <xsl:when test="$nationality = 	'TUV'">798</xsl:when>
            <xsl:when test="$nationality = 	'UGA'">800</xsl:when>
            <xsl:when test="$nationality = 	'UKR'">804</xsl:when>
            <xsl:when test="$nationality = 	'MKD'">807</xsl:when>
            <xsl:when test="$nationality = 	'EGY'">818</xsl:when>
            <xsl:when test="$nationality = 	'GBR'">826</xsl:when>
            <xsl:when test="$nationality = 	'GGY'">831</xsl:when>
            <xsl:when test="$nationality = 	'IMN'">833</xsl:when>
            <xsl:when test="$nationality = 	'TZA'">834</xsl:when>
            <xsl:when test="$nationality = 	'USA'">840</xsl:when>
            <xsl:when test="$nationality = 	'VIR'">850</xsl:when>
            <xsl:when test="$nationality = 	'BFA'">854</xsl:when>
            <xsl:when test="$nationality = 	'URY'">858</xsl:when>
            <xsl:when test="$nationality = 	'UZB'">860</xsl:when>
            <xsl:when test="$nationality = 	'VEN'">862</xsl:when>
            <xsl:when test="$nationality = 	'WLF'">876</xsl:when>
            <xsl:when test="$nationality = 	'WSM'">882</xsl:when>
            <xsl:when test="$nationality = 	'YEM'">887</xsl:when>
            <xsl:when test="$nationality = 	'YUG'">891</xsl:when>
            <xsl:when test="$nationality = 	'ZMB'">894</xsl:when>
            <xsl:when test="$nationality = 	'OTH'">999</xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="string('999')"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    <!--OL关系转换-->
    <xsl:template name="tran_OLrela">
        <xsl:param name="rela"></xsl:param>
        <xsl:if test="$rela = 00">01</xsl:if>
        <xsl:if test="$rela = 01">02</xsl:if>
        <xsl:if test="$rela = 02">03</xsl:if>
        <xsl:if test="$rela = 03">04</xsl:if>
        <xsl:if test="$rela = 04">05</xsl:if>
        <xsl:if test="$rela = 05">06</xsl:if>
        <xsl:if test="$rela = 06">07</xsl:if>
        <xsl:if test="$rela = 07">08</xsl:if>
        <xsl:if test="$rela = 08">09</xsl:if>
        <xsl:if test="$rela = 09">10</xsl:if>
        <xsl:if test="$rela = 10">11</xsl:if>
        <xsl:if test="$rela = 11">12</xsl:if>
        <xsl:if test="$rela = 12">13</xsl:if>
        <xsl:if test="$rela = 13">14</xsl:if>
        <xsl:if test="$rela = 14">15</xsl:if>
        <xsl:if test="$rela = 15">16</xsl:if>
        <xsl:if test="$rela = 16">17</xsl:if>
        <xsl:if test="$rela = 17">18</xsl:if>
        <xsl:if test="$rela = 18">19</xsl:if>
        <xsl:if test="$rela = 19">20</xsl:if>
        <xsl:if test="$rela = 20">21</xsl:if>
        <xsl:if test="$rela = 21">23</xsl:if>
        <xsl:if test="$rela = 22">24</xsl:if>
        <xsl:if test="$rela = 23">22</xsl:if>
        <xsl:if test="$rela = 24">25</xsl:if>
        <xsl:if test="$rela = 25">26</xsl:if>
        <xsl:if test="$rela = 26">27</xsl:if>
        <xsl:if test="$rela = 27">28</xsl:if>
        <xsl:if test="$rela = 28">29</xsl:if>
        <xsl:if test="$rela = 30">30</xsl:if>
    </xsl:template>
    <!-- 自定义标签 -->
    <xsl:template name="generateBTMark">
        <xsl:param name="number"/>
        <xsl:param name="name"/>
        <xsl:param name="value"/>
        <!--<xsl:text disable-output-escaping="yes">&#x000A;</xsl:text>-->
        <xsl:text disable-output-escaping="yes">&lt;</xsl:text><xsl:value-of select="concat($name,$number)"/><xsl:text
            disable-output-escaping="yes">&gt;</xsl:text>
        <xsl:value-of select="$value"/>
        <xsl:text disable-output-escaping="yes">&lt;</xsl:text><xsl:value-of select="concat('/',$name,$number)"/><xsl:text
            disable-output-escaping="yes">&gt;</xsl:text>
        <!--<xsl:text disable-output-escaping="yes">&#x000A;</xsl:text>-->
    </xsl:template>

    <xsl:template name="tran_sex_TX">
        <xsl:param name="sex">2</xsl:param>
        <xsl:if test="$sex = 1">0</xsl:if>
        <xsl:if test="$sex = 2">1</xsl:if>
        <xsl:if test="$sex = 0"></xsl:if>
    </xsl:template>

    <xsl:template name="tran_bnfType_TX">
        <xsl:param name="bnfType">2</xsl:param>
        <xsl:if test="$bnfType = 2">0</xsl:if>
        <xsl:if test="$bnfType = 1">1</xsl:if>
    </xsl:template>

</xsl:stylesheet>