<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:java="http://xml.apache.org/xslt/java" exclude-result-prefixes="java">
    <xsl:output method="xml" indent="yes"/>
    <!--保单查询交易-->
    <xsl:template match="Package">
        <TranData>
            <Body>
                <ReqHead>
                    <Version>1.0</Version>
                    <!--日期时间-->
                    <TransDate>
                        <xsl:value-of
                                select="java:com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.getDate(Head/TransTime)"/>
                    </TransDate>
                    <!--时间-->
                    <TransTime>
                        <xsl:value-of
                                select="java:com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.getTime(Head/TransTime)"/>
                    </TransTime>

                    <!--transno-->
                    <SerialNo>
                        <xsl:value-of select="Head/TransNo"/>
                    </SerialNo>
                    <!--what-->
                    <Channel>
                        <xsl:value-of select="Head/TransType"/>
                    </Channel>
                    <TransID>PC002</TransID>
                    <SysCode>A0037</SysCode>

                </ReqHead>
                <LKTransTrackses>
                    <LKTransTracks>
                        <!--交易流水号-->
                        <TransNo>
                            <xsl:value-of select="Head/TransNo"/>
                        </TransNo>
                        <TransDate>
                            <xsl:value-of
                                    select="java:com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.getDate(Head/TransTime)"/>
                        </TransDate>
                        <!--时间-->
                        <TransTime>
                            <xsl:value-of
                                    select="java:com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.getTime(Head/TransTime)"/>
                        </TransTime>
                        <!--1 银保通 2 移动展业  3 微信卡单 4 微信银保通-->
                        <SellsWay>1</SellsWay>
                    </LKTransTracks>
                </LKTransTrackses>
                <ReqContQuery>
                    <ThirdPartyOrderId>
                        <xsl:value-of select="RequestNodes/RequestNode/ThirdPartyOrderId"/>
                    </ThirdPartyOrderId>
                    <!--ThirdPartyCode-->
                    <RiskCodeWr>
                        <xsl:value-of select="Head/ThirdPartyCode"/>
                    </RiskCodeWr>
                </ReqContQuery>
            </Body>
        </TranData>
    </xsl:template>
</xsl:stylesheet>
