<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:java="http://xml.apache.org/xslt/java"
                exclude-result-prefixes="java">

    <xsl:template match="Package">
        <TranData>
            <xsl:variable name="header" select="Head"/>
            <xsl:variable name="req" select="RequestNodes/RequestNode"/>
            <xsl:variable name="Node" select="RequestNodes/RequestNode/Images/Node"/>
            <xsl:variable name="Image" select="RequestNodes/RequestNode/Images/Node/Image"/>
            <Body>

                <LCImageUpload>

                    <AppCode>
                        <xsl:value-of select="$req/AppCode"/>
                    </AppCode>
                    <AppName>
                        <xsl:value-of select="$req/AppName"/>
                    </AppName>
                    <BusiNum>
                        <xsl:value-of select="$req/BusiNum"/>
                    </BusiNum>
                    <DataType>
                        <xsl:value-of select="$req/DataType"/>
                    </DataType>
                </LCImageUpload>

                <Images>
                    <xsl:for-each select="RequestNodes/RequestNode/Images/Node">
                    <Node>
                        <DocType>
                            <xsl:value-of select="DocType"/>
                        </DocType>
                        <Remark>
                            <xsl:value-of select="Remark"/>
                        </Remark>
                        <PageNum>
                            <xsl:value-of
                                    select="PageNum"/>
                        </PageNum>
                      <xsl:for-each select="Image">
                            <Image>
                                <ImgData>
                                    <xsl:value-of
                                            select="ImgData"/>
                                </ImgData>
                                <ImgName>
                                    <xsl:value-of
                                            select="ImgName"/>
                                </ImgName>
                                <PageCode>
                                    <xsl:value-of
                                            select="PageCode"/>
                                </PageCode>
                            </Image>
                        </xsl:for-each>
                    </Node>
                    </xsl:for-each>
                </Images>
            </Body>
        </TranData>
    </xsl:template >
</xsl:stylesheet>

