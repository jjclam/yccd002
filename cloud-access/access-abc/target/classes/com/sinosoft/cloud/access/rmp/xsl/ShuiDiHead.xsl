<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:java="http://xml.apache.org/xslt/java"
                exclude-result-prefixes="java">

    <xsl:template match="*/headers">
           <ShuiDiHead>
               <!--合作商编号-->
               <supplierNo>
                   <xsl:value-of select="supplierNo"/>
               </supplierNo>
               <!--时间戳-->
               <timeStamp>
                   <xsl:value-of select="timeStamp"/>
               </timeStamp>
               <!-- 日期-->
               <TransDate>
                   <xsl:value-of
                           select="java:com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.getDate(timeStamp)"/>
               </TransDate>
               <!--时间-->
               <TransTime>
                   <xsl:value-of
                           select="java:com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.getTime(timeStamp)"/>
               </TransTime>

               <!--订单号-->
               <orderNo>
                   <xsl:value-of select="orderNo"/>
               </orderNo>
           </ShuiDiHead>
    </xsl:template>
</xsl:stylesheet>
