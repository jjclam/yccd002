<?xml version="1.0" encoding="GBK"?>

<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:java="http://xml.apache.org/xslt/java"
                exclude-result-prefixes="java">

    <xsl:template match="ABCB2I">
        <TranData>
            <Body>
                <LKTransTrackses>
                    <LKTransTracks>
                        <!--交易流水号-->
                        <TransNo>
                            <xsl:value-of select="Header/SerialNo"/>
                        </TransNo>
                        <!--交易编码-->
                        <TransCode>
                            <xsl:call-template name="tran_code">
                                <xsl:with-param name="busitype">
                                    <xsl:value-of select="App/Req/BusiType"/>
                                </xsl:with-param>
                            </xsl:call-template><!-- 销售方式 -->
                        </TransCode>
                        <!--原交易编码-->
                        <TransCodeOri>
                            <xsl:value-of select="Header/TransCode"/>
                        </TransCodeOri>
                        <!--交易子渠道-->
                        <AccessChnlSub>
                            <xsl:value-of select="Header/EntrustWay"/>
                        </AccessChnlSub>
                        <xsl:if test="Header/EntrustWay = 11">
                            <BankCode>05</BankCode>
                        </xsl:if>
                        <xsl:if test="Header/EntrustWay != 11">
                            <BankCode>0501</BankCode>
                        </xsl:if>
                        <BankBranch>
                            <xsl:value-of select="Header/ProvCode"/>
                        </BankBranch>
                        <BankNode>
                            <xsl:value-of select="Header/BranchNo"/>
                        </BankNode>
                        <TransDate>
                            <xsl:value-of select="java:com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.dateFormat8To10(Header/TransDate)"/>
                        </TransDate>
                        <TransTime>
                            <xsl:value-of select="java:com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.timeFormat(Header/TransTime)"/>
                        </TransTime>
                        <!--1 银保通 2 移动展业  3 微信卡单 4 微信银保通-->
                        <SellsWay>1</SellsWay>
                        <ContNo>
                            <xsl:value-of select="App/Req/PolicyNo"/>
                        </ContNo>
                        <TransMony>
                            <xsl:value-of select="App/Req/Amt"/>
                        </TransMony>
                    </LKTransTracks>
                </LKTransTrackses>
                <ReqHead>
                    <Version>1.0</Version>
                    <SerialNo>
                        <xsl:value-of select="Header/SerialNo"/>
                    </SerialNo>
                    <TransID>
                        <xsl:call-template name="tran_code">
                            <xsl:with-param name="busitype">
                                <xsl:value-of select="App/Req/BusiType"/>
                            </xsl:with-param>
                        </xsl:call-template>
                    </TransID>
                    <TransDate>
                        <xsl:value-of select="java:com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.dateFormat8To10(Header/TransDate)"/>
                    </TransDate>
                    <TransTime>
                        <xsl:value-of select="java:com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.timeFormat(Header/TransTime)"/>
                    </TransTime>
                    <SysCode>WIC</SysCode>
                    <Channel>
                        <xsl:call-template name="tran_channel">
                            <xsl:with-param name="channel">
                                <xsl:value-of select="Header/EntrustWay"/>
                            </xsl:with-param>
                        </xsl:call-template><!-- 销售方式 -->
                    </Channel>
                </ReqHead>
                <xsl:if test="App/Req/BusiType=01">
                    <ReqWT>
                        <EdorType>WT</EdorType>
                        <ContNo>
                            <xsl:value-of select="App/Req/PolicyNo"/>
                        </ContNo>
                        <PrintCode>
                            <xsl:value-of select="App/Req/PrintCode"/>
                        </PrintCode>
                        <ZoneNo>
                            <xsl:value-of select="Header/ProvCode"/>
                        </ZoneNo>
                        <BranchNo>
                            <xsl:value-of select="Header/BranchNo"/>
                        </BranchNo>
                        <TellerNo>
                            <xsl:value-of select="Header/Tlid"/>
                        </TellerNo>
                        <GetAmt>
                            <xsl:value-of select="App/Req/GetAmt"/>
                        </GetAmt>
                        <EdorAppInfo>
                            <EdorAppName>
                                <xsl:value-of select="App/Req/ClientName"/>
                            </EdorAppName>
                            <EdorAppIDType>
                                <xsl:call-template name="tran_idtype">
                                    <xsl:with-param name="idtype">
                                        <xsl:value-of select="App/Req/IdKind"/>
                                    </xsl:with-param>
                                </xsl:call-template>
                            </EdorAppIDType>
                            <EdorAppIDNO>
                                <xsl:value-of select="App/Req/IdCode"/>
                            </EdorAppIDNO>
                        </EdorAppInfo>
                        <BankInfo>
                            <BankAccNo>
                                <xsl:value-of select="App/Req/PayAcc"/>
                            </BankAccNo>
                        </BankInfo>
                        <!--   <PayAcc>
                               <xsl:value-of select="App/Req/PayAcc"/>
                           </PayAcc>-->
                    </ReqWT>
                </xsl:if>
                <xsl:if test="App/Req/BusiType=02">
                    <ReqExpirationApply>
                        <EdorType>AG</EdorType>
                        <ContNo>
                            <xsl:value-of select="App/Req/PolicyNo"/>
                        </ContNo>
                        <PrintCode>
                            <xsl:value-of select="App/Req/PrintCode"/>
                        </PrintCode>
                        <ZoneNo>
                            <xsl:value-of select="Header/ProvCode"/>
                        </ZoneNo>
                        <BranchNo>
                            <xsl:value-of select="Header/BranchNo"/>
                        </BranchNo>
                        <TellerNo>
                            <xsl:value-of select="Header/Tlid"/>
                        </TellerNo>
                        <GetMoney>
                            <xsl:value-of select="App/Req/GetAmt"/>
                        </GetMoney>
                        <EdorAppInfo>
                            <EdorAppName>
                                <xsl:value-of select="App/Req/ClientName"/>
                            </EdorAppName>
                            <EdorAppIDType>
                                <xsl:call-template name="tran_idtype">
                                    <xsl:with-param name="idtype">
                                        <xsl:value-of select="App/Req/IdKind"/>
                                    </xsl:with-param>
                                </xsl:call-template>
                            </EdorAppIDType>
                            <EdorAppIDNO>
                                <xsl:value-of select="App/Req/IdCode"/>
                            </EdorAppIDNO>
                        </EdorAppInfo>
                        <BankInfo>
                            <BankAccNo>
                                <xsl:value-of select="App/Req/PayAcc"/>
                            </BankAccNo>
                        </BankInfo>
                          <PayAcc>
                              <xsl:value-of select="App/Req/PayAcc"/>
                          </PayAcc>
                    </ReqExpirationApply>
                </xsl:if>
                <xsl:if test="App/Req/BusiType=03">
                    <ReqSurrenderApply>
                        <EdorType>CT</EdorType>
                        <ContNo>
                            <xsl:value-of select="App/Req/PolicyNo"/>
                        </ContNo>
                        <IdKind>
                            <xsl:call-template name="tran_idtype">
                                <xsl:with-param name="idtype">
                                    <xsl:value-of select="App/Req/IdKind"/>
                                </xsl:with-param>
                            </xsl:call-template>
                        </IdKind>
                        <IdCode>
                            <xsl:value-of select="App/Req/IdCode"/>
                        </IdCode>
                        <PrintCode>
                            <xsl:value-of select="App/Req/PrintCode"/>
                        </PrintCode>
                        <ClientName>
                            <xsl:value-of select="App/Req/ClientName"/>
                        </ClientName>
                        <PayAcc>
                            <xsl:value-of select="App/Req/PayAcc"/>
                        </PayAcc>
                        <xsl:if test="Header/EntrustWay = 11">
                            <BankCode>05</BankCode>
                        </xsl:if>
                        <xsl:if test="Header/EntrustWay != 11">
                            <BankCode>0501</BankCode>
                        </xsl:if>
                        <ZoneNo>
                            <xsl:value-of select="Header/ProvCode"/>
                        </ZoneNo>
                        <BranchNo>
                            <xsl:value-of select="Header/BranchNo"/>
                        </BranchNo>
                        <TellerNo>
                            <xsl:value-of select="Header/Tlid"/>
                        </TellerNo>
                        <GetAmnt>
                            <xsl:value-of select="App/Req/GetAmt"/>
                        </GetAmnt>
                    </ReqSurrenderApply>
                </xsl:if>
                <LCCont>
                    <ContNo>
                        <xsl:value-of select="App/Req/PolicyNo"/>
                    </ContNo>
                    <SellType><!-- 销售方式 -->
                        <xsl:call-template name="tran_sellType">
                            <xsl:with-param name="sellType">
                                <xsl:value-of select="Header/EntrustWay"/>
                            </xsl:with-param>
                        </xsl:call-template>
                    </SellType>
                </LCCont>
            </Body>
        </TranData>
    </xsl:template>
    <xsl:template name="tran_code">
        <xsl:param name="busitype"/>
        <xsl:if test="$busitype = 01">POS018</xsl:if>
        <xsl:if test="$busitype = 02">POS044</xsl:if>
        <xsl:if test="$busitype = 03">POS089</xsl:if>
    </xsl:template>
    <!-- 证件类型  -->
    <xsl:template name="tran_idtype">
        <xsl:param name="idtype"/>
        <xsl:if test="$idtype = 1">111</xsl:if>
        <xsl:if test="$idtype = 2">414</xsl:if>
        <xsl:if test="$idtype = 3">114</xsl:if>
        <xsl:if test="$idtype = 4">990</xsl:if>
        <xsl:if test="$idtype = 5">990</xsl:if>
        <xsl:if test="$idtype = 0">990</xsl:if>
        <xsl:if test="$idtype = 110001">111</xsl:if>
        <xsl:if test="$idtype = 110002">111</xsl:if>
        <xsl:if test="$idtype = 110003">112</xsl:if>
        <xsl:if test="$idtype = 110004">112</xsl:if>
        <xsl:if test="$idtype = 110005">113</xsl:if>
        <xsl:if test="$idtype = 110006">113</xsl:if>
        <xsl:if test="$idtype = 110011">990</xsl:if>
        <xsl:if test="$idtype = 110012">990</xsl:if>
        <xsl:if test="$idtype = 110013">114</xsl:if>
        <xsl:if test="$idtype = 110014">114</xsl:if>
        <xsl:if test="$idtype = 110015">990</xsl:if>
        <xsl:if test="$idtype = 110016">990</xsl:if>
        <xsl:if test="$idtype = 110017">990</xsl:if>
        <xsl:if test="$idtype = 110018">990</xsl:if>
        <xsl:if test="$idtype = 110019">513</xsl:if>
        <xsl:if test="$idtype = 110020">513</xsl:if>
        <xsl:if test="$idtype = 110021">511</xsl:if>
        <xsl:if test="$idtype = 110022">511</xsl:if>
        <xsl:if test="$idtype = 110023">414</xsl:if>
        <xsl:if test="$idtype = 110024">414</xsl:if>
        <xsl:if test="$idtype = 110025">414</xsl:if>
        <xsl:if test="$idtype = 110026">414</xsl:if>
        <xsl:if test="$idtype = 110027">114</xsl:if>
        <xsl:if test="$idtype = 110028">114</xsl:if>
        <xsl:if test="$idtype = 110029">990</xsl:if>
        <xsl:if test="$idtype = 110030">990</xsl:if>
        <xsl:if test="$idtype = 110031">123</xsl:if>
        <xsl:if test="$idtype = 110032">123</xsl:if>
        <xsl:if test="$idtype = 110033">114</xsl:if>
        <xsl:if test="$idtype = 110034">114</xsl:if>
        <xsl:if test="$idtype = 110035">123</xsl:if>
        <xsl:if test="$idtype = 110036">123</xsl:if>
        <xsl:if test="$idtype = 119998">990</xsl:if>
        <xsl:if test="$idtype = 119999">990</xsl:if>
        <xsl:if test="$idtype = 110037">553</xsl:if>
    </xsl:template>
    <!-- 销售方式 -->
    <xsl:template name="tran_sellType">
        <xsl:param name="sellType"></xsl:param>
        <xsl:if test="$sellType = 11">08</xsl:if>
        <xsl:if test="$sellType = 01">22</xsl:if>
        <xsl:if test="$sellType = 04">24</xsl:if>
        <xsl:if test="$sellType = 02">25</xsl:if>
        <xsl:if test="$sellType = 22">27</xsl:if>
        <xsl:if test="$sellType = 08">26</xsl:if>
    </xsl:template>

    <xsl:template name="tran_appType">
        <xsl:param name="appType"></xsl:param>
        <xsl:if test="$appType = 11">9</xsl:if><!--柜面-->
        <xsl:if test="$appType = 01">12</xsl:if><!--网银-->
        <xsl:if test="$appType = 04">11</xsl:if><!--自助-->
        <xsl:if test="$appType = 02">10</xsl:if><!--掌银-->
        <xsl:if test="$appType = 22">13</xsl:if><!--超贵-->
        <xsl:if test="$appType = 08">14</xsl:if><!--e管家-->
    </xsl:template>
    <xsl:template name="tran_channel">
        <xsl:param name="channel"></xsl:param>
        <xsl:if test="$channel = 11">ABCOTC</xsl:if>
        <xsl:if test="$channel = 01">ABCEBANK</xsl:if>
        <xsl:if test="$channel = 04">ABCSELF</xsl:if>
        <xsl:if test="$channel = 02">ABCMOBILE</xsl:if>
        <xsl:if test="$channel = 22">ABCSUPOTC</xsl:if>
        <xsl:if test="$channel = 08">ABCESTWD</xsl:if>
    </xsl:template>
</xsl:stylesheet>