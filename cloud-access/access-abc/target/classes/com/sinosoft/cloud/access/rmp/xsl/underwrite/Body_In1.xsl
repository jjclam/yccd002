<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:java="http://xml.apache.org/xslt/java"
                exclude-result-prefixes="java">

    <xsl:template match="/">
        <xsl:variable name="head" select="document/request/head"/>
        <!--订单基本信息-->
        <xsl:variable name="policy" select="document/request/body/policy"/>
        <!--投保人信息-->
        <xsl:variable name="holder" select="document/request/body/holder"/>
        <!--被保人-->
        <xsl:variable name="insured" select="document/request/body/insureds/insured"/>
        <xsl:variable name="varRiskCodess"  select="$policy/prodNo"/>
        <TranData>
            <Body>
                <LKTransTrackses>
                    <LKTransTracks>
                        <!--交易流水号 保险订单号-->
                        <TransNo>
                            <xsl:value-of select="$head/reqMsgId"/>
                        </TransNo>
                        <!--交易编码-->
                        <TransCode>
                            <xsl:value-of select="$head/function"/>
                        </TransCode>

                        <!-- 日期-->
                        <TransDate>
                            <xsl:value-of
                                    select="java:com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.getExactDate($head/transTime)"/>
                        </TransDate>
                        <!--时间-->
                        <TransTime>
                            <xsl:value-of
                                    select="java:com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.getTime($head/transTime)"/>
                        </TransTime>

                        <!-- 投保单号 -->
                        <ProposalNo>
                            <xsl:value-of select="$policy/policyNo"/>
                        </ProposalNo>

                        <!--保单号 新增-->
                        <!--<ContNo>
                            <xsl:value-of select="$req/ContNo"/>
                        </ContNo>-->
                        <BankCode>05</BankCode>
                        <!--&lt;!&ndash;这是干啥的&ndash;&gt;
                        <xsl:if test="Header/EntrustWay = 11">
                            <BankCode>05</BankCode>
                        </xsl:if>
                        <xsl:if test="Header/EntrustWay != 11">
                            <BankCode>0501</BankCode>
                        </xsl:if>-->
                        <BankBranch>26</BankBranch>

                        <!-- <xsl:value-of select="$header/ProvCode"/>-->

                        <BankNode>26</BankNode>
                    </LKTransTracks>
                </LKTransTrackses>


                <LKTransStatus>
                    <!-- 投保单号 -->
                    <ProposalNo>
                        <xsl:value-of select="$policy/policyNo"/>
                    </ProposalNo>
                    <PrtNo><!-- 投保单号 -->
                        <xsl:value-of select="$policy/policyNo"/>
                    </PrtNo>
                    <BankCode>05</BankCode>


                    <!--交易流水号 保险订单号-->
                    <TransNo>
                        <xsl:value-of select="$head/reqMsgId"/>
                    </TransNo>
                    <!--交易编码-->
                    <TransCode>
                        <xsl:value-of select="$head/function"/>
                    </TransCode>

                    <!-- 日期-->
                    <TransDate>
                        <xsl:value-of
                                select="java:com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.getExactDate($head/transTime)"/>
                    </TransDate>
                    <!--时间-->
                    <TransTime>
                        <xsl:value-of
                                select="java:com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.getTime($head/transTime)"/>
                    </TransTime>

                    <!--what-->
                    <BankNode>26</BankNode>




<!--
                    <FuncFlag>
                        <xsl:value-of select="Header/TransCode"/>
                    </FuncFlag>
                    <BankOperator>
                        <xsl:value-of select="Header/Tlid"/>
                    </BankOperator>-->


                    <!-- <BankBranch>
                         <xsl:value-of select="Header/ProvCode"/>
                     </BankBranch>-->
                </LKTransStatus>

                <!--LCContPojo-->
                <LCCont>

                    <!--合作机构订单号 ThirdPartyOrderId-->
                   <!-- <ThirdPartyOrderId>
                        <xsl:value-of select="$policy/policyNo"/>
                    </ThirdPartyOrderId>-->
                    <xsl:for-each select="$policy/extendInfos/extendInfo">
                        <!-- 缴费间隔 -->
                        <xsl:if test="./@key= 'continuousFrequency'">
                            <PayIntv>
                                <xsl:value-of select="./@value"/>
                            </PayIntv>
                        </xsl:if>
                    </xsl:for-each>

                    <!-- 投保单号 -->
                    <ProposalContNo>
                        <xsl:value-of select="$policy/policyNo"/>
                    </ProposalContNo>
                    <!-- 保单号ContNo-->
                   <!-- <ContNo>
                        <xsl:value-of select="$req/ContNo"/>
                    </ContNo>-->
                    <ContType>1</ContType>
                    <PrtNo>
                        <xsl:value-of select="$policy/policy"/>
                    </PrtNo>
                    <EAuto>0</EAuto>



                    <!--新增代理人编码-->
                    <!--<AgentCode>
                        <xsl:value-of select="$req/AgentNo"/>
                    </AgentCode>-->
                    <!-- 销售方式 -->
                   <!-- <SellType>
                        <xsl:value-of select="$header/ThirdPartyCode"/>
                    </SellType>-->
                    <!--投保入口新增-->
                   <!-- &lt;!&ndash;投保入口ABCode&ndash;&gt;
                    <AgentBankCode>
                        <xsl:value-of select="$req/ABCode"/>
                    </AgentBankCode>-->

                    <!--<BankAgent>
                        <xsl:value-of select="$base/SalerCertNo"/>
                    </BankAgent>-->


                   <!-- <CValiDate>
                        <xsl:value-of select="$req/InsuranceStartPeriod"/>
                    </CValiDate>-->

                    <!-- 新增字段 PolApplyDate -->
                    <!--<PolApplyDate>
                        <xsl:value-of select="$req/SignedDate"/>
                    </PolApplyDate>-->

<!--
                    <PayMode>
                        <xsl:value-of select="$req/RenewalPaymentInfo/PayMode"/>
                    </PayMode>
                    <BankCode>
                        <xsl:value-of select="$req/RenewalPaymentInfo/PayBankCode"/>
                    </BankCode>
                    <BankProvince>
                        <xsl:value-of select="$req/RenewalPaymentInfo/BankProvinceCode"/>
                    </BankProvince>
                    <BankCity>
                        <xsl:value-of select="$req/RenewalPaymentInfo/BankCityCode"/>
                    </BankCity>
                    <BankAccNo>
                        <xsl:value-of select="$req/RenewalPaymentInfo/PayAcountNo"/>
                    </BankAccNo>
                    <AccType>
                        <xsl:value-of select="$req/RenewalPaymentInfo/CardType"/>
                    </AccType>

                    <AccName>
                        <xsl:value-of select="$req/RenewalPaymentInfo/PayAccountName"/>
                    </AccName>

                    <NewBankAccNo>
                        <xsl:value-of select="$req/RenewalPaymentInfo/PayAcountNo"/>
                    </NewBankAccNo>

                    <NewAccName>
                        <xsl:value-of select="$req/RenewalPaymentInfo/PayAccountName"/>
                    </NewAccName>

                    <BankAgent>
                        <xsl:value-of select="$req/ZJAgentCode"/>
                    </BankAgent>
                    <BankAgentName>
                        <xsl:value-of select="$req/ZJAgentName"/>
                    </BankAgentName>
                    <AgentCom>
                        <xsl:value-of select="$req/AgentCom"/>
                    </AgentCom>-->


                    <!--投保人姓名-->
                    <AppntName>
                        <xsl:value-of select="$holder/certName"/>
                    </AppntName>
                    <!-- 投保人证件类型 -->
                    <AppntIDType>
                        <xsl:value-of select="$holder/certType"/>
                    </AppntIDType>
                    <!-- 投保人证件号 -->
                    <AppntIDNo>
                        <xsl:value-of select="$holder/certNo"/>
                    </AppntIDNo>
                    <!--扩展信息-->
                    <xsl:for-each select="$holder/extendInfos/extendInfo">
                        <xsl:if test="./@key= 'birthday'">
                            <AppntBirthday>
                                <xsl:value-of select="./@value"/>
                            </AppntBirthday>
                        </xsl:if>
                        <xsl:if test="./@key= 'gender'">
                            <AppntSex>
                                <xsl:value-of select="./@value"/>
                            </AppntSex>
                        </xsl:if>
                    </xsl:for-each>

                    <xsl:choose>
                        <!--与投保人为不同人-->
                        <xsl:when test="$insured/sameWithHolder ='0' ">
                            <InsuredName>
                                <xsl:value-of select="$holder/certName"/>
                            </InsuredName>
                            <InsuredIDType>
                                <xsl:value-of select="$insured/certType"/>
                            </InsuredIDType>
                            <InsuredIDNo>
                                <xsl:value-of select="$insured/certNo"/>
                            </InsuredIDNo>
                            <!--扩展信息-->
                            <xsl:for-each select="$insured/extendInfos/extendInfo">
                                <xsl:if test="./@key= 'birthday'">
                                    <InsuredBirthday>
                                        <xsl:value-of select="./@value"/>
                                    </InsuredBirthday>
                                </xsl:if>
                                <xsl:if test="./@key= 'gender'">
                                    <InsuredSex>
                                        <xsl:value-of select="./@value"/>
                                    </InsuredSex>
                                </xsl:if>
                            </xsl:for-each>
                        </xsl:when>
                        <!--与投保人为同人-->
                        <xsl:when test="$insured/sameWithHolder ='1' ">
                            <InsuredName>
                                <xsl:value-of select="$holder/certName"/>
                            </InsuredName>
                            <!-- 投保人证件类型 -->
                            <InsuredIDType>
                                <xsl:value-of select="$holder/certType"/>
                            </InsuredIDType>
                            <InsuredIDNo><!-- 投保人证件号 -->
                                <xsl:value-of select="$holder/certNo"/>
                            </InsuredIDNo>
                            <!--扩展信息-->
                            <xsl:for-each select="$holder/extendInfos/extendInfo">
                                <xsl:if test="./@key= 'birthday'">
                                    <InsuredBirthday>
                                        <xsl:value-of select="./@value"/>
                                    </InsuredBirthday>
                                </xsl:if>
                                <xsl:if test="./@key= 'gender'">
                                    <InsuredSex>
                                        <xsl:value-of select="./@value"/>
                                    </InsuredSex>
                                </xsl:if>
                            </xsl:for-each>

                        </xsl:when>


                    </xsl:choose>

                    <SaleChnl>05</SaleChnl>

                </LCCont>

                <!--GlobalPojo-->
                <Global>
                    <!--保险起期-->
                    <RiskBeginDate>
                        <xsl:value-of
                                select="java:com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.fenToYuan($policy/effectStartTime)"/>
                    </RiskBeginDate>
                    <!--保险止期-->
                    <RiskEndDate>
                        <xsl:value-of
                                select="java:com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.fenToYuan($policy/effectEndTime)"/>
                    </RiskEndDate>
                    <BankCode>05</BankCode>
                    <MainRiskCode>
                        <xsl:value-of select="$varRiskCodess"/>
                    </MainRiskCode>
                    <TransNo>
                        <xsl:value-of select="$head/cid"/>
                    </TransNo>

                </Global>

                <!--LCAppntPojo-->
                <LCAppnt>
                    <AppntID>
                        <xsl:value-of select="$holder/personNo"/>
                    </AppntID>
                    <AppntName>
                        <xsl:value-of select="$holder/certName"/>
                    </AppntName>
                    <!-- 投保人证件类型 -->
                    <IDType>
                        <xsl:value-of select="$holder/certType"/>
                    </IDType>
                    <IDNo><!-- 投保人证件号 -->
                        <xsl:value-of select="$holder/certNo"/>
                    </IDNo>
                    <!--扩展信息-->
                    <xsl:for-each select="$holder/extendInfos/extendInfo">
                        <xsl:if test="./@key= 'birthday'">
                            <AppntBirthday>
                                <xsl:value-of select="./@value"/>
                            </AppntBirthday>
                        </xsl:if>
                        <xsl:if test="./@key= 'gender'">
                            <AppntSex>
                                <xsl:value-of select="./@value"/>
                            </AppntSex>
                        </xsl:if>


                    </xsl:for-each>


                    <!-- 投保单号 -->
                    <PrtNo>
                        <xsl:value-of select="$policy/policyNo"/>
                    </PrtNo>


                    <!-- 投保人证件有效期 -->
                  <!--  <IdValiDate>
                        <xsl:value-of select="$appnt/IdexpDate"/>
                    </IdValiDate>
                    <Degree>
                        <xsl:value-of select="$appnt/Degree"/>
                    </Degree>-->

                    <!-- 投保人国籍 -->
                    <NativePlace>CHN</NativePlace>

                    <!--婚姻状况-->
                    <!--<Marriage>
                        <xsl:value-of select="$appnt/Marriage"/>
                    </Marriage>-->
                    <!--社保标记SSFlag-->
                   <!-- <SocialInsuFlag>
                        <xsl:value-of select="$appnt/SocialInsuFlag"/>
                    </SocialInsuFlag>-->
                    <!--年收入-->
                    <!--<Salary>
                        <xsl:value-of select="$appnt/Salary"/>
                    </Salary>-->
                    <!--健康状况-->
                   <!-- <Health>
                        <xsl:value-of select="$appnt/Health"/>
                    </Health>
                    <SmokeFlag>
                        <xsl:value-of select="$appnt/SmokeFlag"/>
                    </SmokeFlag>
                    <Stature>
                        <xsl:value-of select="$appnt/Stature"/>
                    </Stature>
                    <Avoirdupois>
                        <xsl:value-of select="$appnt/Weight"/>
                    </Avoirdupois>
                    <RelatToInsu>
                        <xsl:value-of select="$appnt/RelationToInsured"/>
                    </RelatToInsu>-->
                    <!--职业类别-->
                    <!--<OccupationType>
                        <xsl:value-of select="$appnt/OccupationClass"/>
                    </OccupationType>-->
                    <!--职业代码-->
                    <!--<OccupationCode>
                        <xsl:value-of select="$appnt/OccupationCode"/>
                    </OccupationCode>-->
                </LCAppnt>


                <!--LCAccountPojo-->
                <!--<LCAccount>
                    <AccName>
                        <xsl:value-of select="$req/Beneficiaries/PayAccountName"/>
                    </AccName>&lt;!&ndash; 账户姓名 &ndash;&gt;
                    <BankAccNo>
                        <xsl:value-of select="$req/Beneficiaries/PayAcountNo"/>
                    </BankAccNo>&lt;!&ndash; 续期缴费账号 &ndash;&gt;
                    <AccKind>1</AccKind>
                    <BankCode>
                        <xsl:value-of select="$req/Beneficiaries/PayBankCode"/>
                    </BankCode>

                </LCAccount>-->

                <!--LCSpecPojo-->
                <!--<LCSpecs>
                    <LCSpec>
                        &lt;!&ndash; 特别约定 &ndash;&gt;
                        <SpecContent>
                        </SpecContent>
                    </LCSpec>
                </LCSpecs>-->

                <!--LCAddressPojo-->
                <LCAddresses>
                    <!--投保人地址信息-->
                    <LCAddress>
                        <xsl:for-each select="$holder/extendInfos/extendInfo">
                            <!--地址信息非必传-->
                            <xsl:if test="./@key= 'address'">
                                <PostalAddress>
                                    <xsl:value-of select="./@value"/>
                                </PostalAddress>
                                <HomeAddress>
                                    <xsl:value-of select="./@value"/>
                                </HomeAddress>
                            </xsl:if>
                            <xsl:if test="./@key= 'email'">
                                <EMail>
                                    <xsl:value-of select="./@value"/>
                                </EMail>
                            </xsl:if>
                            Phone
                        </xsl:for-each>


                        <!-- 邮编 -->
                       <!-- <ZipCode>
                            <xsl:value-of select="$appnt/AppZipCode"/>
                        </ZipCode>

                        <HomeZipCode>
                            <xsl:value-of select="$appnt/AppZipCode"/>
                        </HomeZipCode>
                        <Mobile>
                            <xsl:value-of select="$appnt/AppMobile"/>
                        </Mobile>-->


                       <!-- <CompanyPhone>
                            <xsl:value-of select="$appnt/CompanyPhone"/>
                        </CompanyPhone>-->

                        <!-- 省 -->
                       <!-- <Province>
                            <xsl:value-of select="$appnt/Province"/>
                        </Province>-->
                        <!-- 市 -->
                        <!--<City>
                            <xsl:value-of select="$appnt/City"/>
                        </City>-->
                        <!-- 区 -->
                        <!--<County>
                            <xsl:value-of select="$appnt/Country"/>
                        </County>-->
                        <!-- <GrpName>
                             <xsl:value-of select="$appnt/Company"/>
                         </GrpName>-->
                    </LCAddress>
                    <!--被保人地址信息-->
                    <LCAddress>
                        <xsl:for-each select="$insured/extendInfos/extendInfo">
                            <!--地址信息非必传-->
                            <xsl:if test="./@key= 'address'">
                                <PostalAddress>
                                    <xsl:value-of select="./@value"/>
                                </PostalAddress>
                                <HomeAddress>
                                    <xsl:value-of select="./@value"/>
                                </HomeAddress>
                            </xsl:if>
                            <xsl:if test="./@key= 'email'">
                                <EMail>
                                    <xsl:value-of select="./@value"/>
                                </EMail>
                            </xsl:if>
                            <xsl:if test="./@key= 'phone'">
                                <Phone>
                                    <xsl:value-of select="./@value"/>
                                </Phone>
                            </xsl:if>


                        </xsl:for-each>


                       <!-- <ZipCode>
                            <xsl:value-of select="$Insured/InsuZipCode"/>
                        </ZipCode>

                        <Mobile>
                            <xsl:value-of select="$Insured/InsuMobile"/>
                        </Mobile>-->

                        <!--<HomePhone>
                            <xsl:value-of select="$Insured/Phone"/>
                        </HomePhone>-->
                        <!--<Phone>
                            <xsl:value-of select="$Insured/Phone"/>
                        </Phone>-->

                        <!-- 省 -->
                        <!--<Province>
                            <xsl:value-of select="$Insured/InsuProvince"/>
                        </Province>-->
                        <!-- 市 -->
                        <!--<City>
                            <xsl:value-of select="$Insured/InsuCity"/>
                        </City>-->
                        <!-- 区 -->
                        <!--<County>
                            <xsl:value-of select="$Insured/InsuCountry"/>
                        </County>-->

                    </LCAddress>
                </LCAddresses>

                <!--被保人-->
                <LCInsureds>
                    <LCInsured>
                        <xsl:choose>
                            <!--与投保人为不同人-->
                            <xsl:when test="$insured/sameWithHolder ='0' ">
                                <Name>
                                    <xsl:value-of select="$insured/certName"/>
                                </Name>
                                <InsuredID>
                                    <xsl:value-of select="$insured/personNo"/>
                                </InsuredID>
                                <IDType>
                                    <xsl:value-of select="$insured/certType"/>
                                </IDType>
                                <IDNo>
                                    <xsl:value-of select="$insured/certNo"/>
                                </IDNo>
                                <RelationToAppnt>
                                    <xsl:value-of select="$insured/relationWithHolder"/>
                                </RelationToAppnt>
                                <!--扩展信息-->
                                <xsl:for-each select="$insured/extendInfos/extendInfo">
                                    <xsl:if test="./@key= 'birthday'">
                                        <Birthday>
                                            <xsl:value-of select="./@value"/>
                                        </Birthday>
                                    </xsl:if>
                                    <xsl:if test="./@key= 'gender'">
                                        <Sex>
                                            <xsl:value-of select="./@value"/>
                                        </Sex>
                                    </xsl:if>
                                    <xsl:if test="./@key= 'phone'">
                                        <Phone>
                                            <xsl:value-of select="./@value"/>
                                        </Phone>
                                    </xsl:if>
                                </xsl:for-each>
                            </xsl:when>
                            <!--与投保人为同人-->
                            <xsl:when test="$insured/sameWithHolder ='1' ">
                                <InsuredID>
                                    <xsl:value-of select="$holder/personNo"/>
                                </InsuredID>
                                <Name>
                                    <xsl:value-of select="$holder/certName"/>
                                </Name>
                                <!-- 投保人证件类型 -->
                                <IDType>
                                    <xsl:value-of select="$holder/certType"/>
                                </IDType>
                                <IDNo><!-- 投保人证件号 -->
                                    <xsl:value-of select="$holder/certNo"/>
                                </IDNo>
                                <!--扩展信息-->
                                <xsl:for-each select="$holder/extendInfos/extendInfo">
                                    <xsl:if test="./@key= 'birthday'">
                                        <Birthday>
                                            <xsl:value-of select="./@value"/>
                                        </Birthday>
                                    </xsl:if>
                                    <xsl:if test="./@key= 'gender'">
                                        <Sex>
                                            <xsl:value-of select="./@value"/>
                                        </Sex>
                                    </xsl:if>
                                </xsl:for-each>

                            </xsl:when>


                        </xsl:choose>


                        <!-- 被保人证件有效期 -->
                       <!-- <IdValiDate>
                            <xsl:value-of select="$Insured/IdexpDate"/>
                        </IdValiDate>
                        <NativePlace>
                            <xsl:value-of select="$Insured/NativePlace"/>
                        </NativePlace>

                        <Degree>
                            <xsl:value-of select="$Insured/Degree"/>
                        </Degree>-->
                        <!--投被保人关系-->
                        <!--RelationToAppnt>
                            <xsl:value-of select="$Insured/InsuRelationToApp"/>
                        </RelationToAppnt>-->
                        <!--被保人与主被保人关系-->
                        <!--<RelationToMainInsured>
                            <xsl:value-of select="$Insured/InsuRelationToMainInsu"/>
                        </RelationToMainInsured>-->
                        <!--主投保人标识-->
                        <!-- <IsMainInsured>
                             <xsl:value-of select="$Insured/IsMainInsured"/>
                         </IsMainInsured>-->
                        <!-- <SequenceNo>1</SequenceNo>-->
                        <!--被保人身高-->
                       <!-- <Stature>
                            <xsl:value-of select="$Insured/Stature"/>
                        </Stature>-->
                        <!--被保人体重-->
                       <!-- <Avoirdupois>
                            <xsl:value-of select="$Insured/Weight"/>
                        </Avoirdupois>-->
                        <!--年收入-->
                        <!--<Salary>
                            <xsl:value-of select="$Insured/Salary"/>
                        </Salary>-->


                        <!--健康状况-->
                        <!--<Health>
                            <xsl:value-of select="$Insured/Health"/>
                        </Health>

                        <SmokeFlag>
                            <xsl:value-of select="$Insured/SmokeFlag"/>
                        </SmokeFlag>-->
                        <!--婚姻状况-->
                        <!-- <Marriage>
                             <xsl:value-of select="$Insured/Marriage"/>
                         </Marriage>-->
                        <!--社保标记SSFlag-->
                        <!--<SSFlag>
                            <xsl:value-of select="$Insured/SocialInsuFlag"/>
                        </SSFlag>-->
                        <!-- 新增-->
                       <!-- <ContPlanCode>
                            <xsl:value-of
                                    select="$req/ProductCode"/>
                        </ContPlanCode>-->
                        <PrtNo>
                            <!--投保单号 -->
                            <xsl:value-of select="$policy/policyNo"/>
                        </PrtNo>
                        <!--职业类别-->
                        <!--<OccupationType>
                            <xsl:value-of select="$Insured/OccupationClass"/>
                        </OccupationType>-->
                        <!--职业代码-->
                        <!--<OccupationCode>
                            <xsl:value-of select="$Insured/OccupationCode"/>
                        </OccupationCode>-->
                    </LCInsured>
                </LCInsureds>

                <!--受益人Beneficiaries 法定受益人不传递受益人信息-->
                <!--<LCBnfs>

                    <LCBnf>
                        <BnfNo>
                            <xsl:value-of select="$bnf/BeneficiaryNo"/>
                        </BnfNo>
                        <BnfGrade>
                            <xsl:value-of select="$bnf/BeneficiaryGrade"/>
                        </BnfGrade>
                        &lt;!&ndash;受益人顺序 BeneficiaryOrder&ndash;&gt;
                        <BeneficiaryOrder>
                            <xsl:value-of select="$bnf/BeneficiaryOrder"/>
                        </BeneficiaryOrder>
                        &lt;!&ndash;收益人份额&ndash;&gt;

                        <BnfLot>
                            <xsl:value-of select="$bnf/InterestPercent"/>
                        </BnfLot>
                        <BnfType>
                            <xsl:value-of select="$bnf/BeneficiaryType"/>
                        </BnfType>


                        <Name>
                            <xsl:value-of select="$bnf/BeneficiaryName"/>
                        </Name>
                        <Sex>
                            <xsl:value-of select="$bnf/BeneficiarySex"/>
                        </Sex>
                        <Birthday>
                            <xsl:value-of
                                    select="$bnf/BeneficiaryBirthday"/>
                        </Birthday>

                        <IDType>
                            <xsl:value-of select="$bnf/BeneficiaryIDType"/>
                        </IDType>
                        <IDNo>
                            <xsl:value-of select="$bnf/BeneficiaryIDNo"/>
                        </IDNo>
                        <IdValiDate>
                            <xsl:value-of select="$bnf/IDExpDate"/>
                        </IdValiDate>
                        &lt;!&ndash; 受益人证件有效期 &ndash;&gt;

                        <RelationToInsured>
                            <xsl:value-of select="$bnf/RelationToInsured"/>
                        </RelationToInsured>


                        <Tel>
                            <xsl:value-of select="$bnf/BeneficiaryMobile"/>
                        </Tel>
                        <ZipCode>
                            <xsl:value-of select="$bnf/BeneficiaryZipCode"/>
                        </ZipCode>
                        <Address>
                            <xsl:value-of select="$bnf/BeneficiaryAddress"/>
                        </Address>
                    </LCBnf>
                </LCBnfs>-->

                <!--险种-->
                <LCPols>
                    <LCPol>
                        <!-- 保费 -->
                        <StandPrem>
                            <xsl:value-of
                                    select="java:com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.fenToYuan($policy/premium)"/>
                        </StandPrem>
                        <!-- 实际保费 -->
                        <Prem>
                            <xsl:value-of
                                    select="java:com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.fenToYuan($policy/actualPremium)"/>
                        </Prem>
                        <!-- 保额 -->
                        <Amnt>
                            <xsl:value-of
                                    select="java:com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.fenToYuan($policy/sumInsured)"/>
                        </Amnt>
                        <!--投保申请时间-->
                        <PolApplyDate>
                            <xsl:value-of
                                    select="java:com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.getExactDate($policy/insuredTime)"/>
                        </PolApplyDate>
                        <!--份数-->
                        <Mult>
                            <xsl:value-of select="$policy/applyNum"/>
                        </Mult>
                        <xsl:for-each select="$policy/extendInfos/extendInfo">
                            <!-- 自动续期标志 -->
                            <xsl:if test="./@key= 'autoContinuous'">
                                <RnewFlag>
                                    <xsl:value-of select="./@value"/>
                                </RnewFlag>
                            </xsl:if>
                            <!-- 缴费间隔 -->
                            <xsl:if test="./@key= 'continuousFrequency'">
                                <PayIntv>
                                    <xsl:value-of select="./@value"/>
                                </PayIntv>
                            </xsl:if>

                        </xsl:for-each>


                        <!-- 保单号ContNo-->
                       <!-- <ContNo>
                            <xsl:value-of select="$req/ContNo"/>
                        </ContNo>-->
                        <!--<xsl:variable name="varRiskCode">
                            <xsl:with-param name="riskCode" select="$policy/prodNo"/>
                        </xsl:variable>-->
                        <!-- 险种代码 -->
                        <RiskCode>
                            <xsl:value-of select="$policy/prodNo"/>
                        </RiskCode>

                        <!--主附险标识-->
                        <!--<MainPolNo>
                            <xsl:value-of select="$policyLiability/MainRiskFlag"/>
                        </MainPolNo>-->
                        <!-- 主险代码 -->
                        <!-- <MainPolNo>
                             <xsl:value-of select="$varRiskCode"/>
                         </MainPolNo>-->


                        <!--&lt;!&ndash;直接取报文值&ndash;&gt;
                        <PayMode>
                            <xsl:value-of select="$policyLiability/PayMode"/>
                        </PayMode>-->

                        <!-- 保险年期类型 -->
                        <!--<InsuYearFlag>
                            <xsl:value-of select="$policyLiability/InsuYearFlag"/>
                        </InsuYearFlag>-->
                        <!--保险期间 -->
                        <!--<InsuYear>
                            <xsl:value-of select="$policyLiability/InsuYear"/>
                        </InsuYear>-->
                       <!-- <PayEndYearFlag>
                            <xsl:value-of select="$policyLiability/PayEndYearFlag"/>
                        </PayEndYearFlag>-->

                        <!-- 缴费期间 PayEndYear-->
                       <!-- <PayEndYear>
                            <xsl:value-of select="$policyLiability/PayEndYear"/>
                        </PayEndYear>-->


                        <!--投保日期-->
                       <!-- <CValiDate>
                            <xsl:value-of select="$policyLiability/InsuranceStartPeriod"/>
                        </CValiDate>
                        <EndDate>
                            <xsl:value-of select="$policyLiability/InsuranceEndPeriod"/>
                        </EndDate>-->

                        <xsl:choose>
                            <!--与投保人为不同人-->
                            <xsl:when test="$insured/sameWithHolder ='0' ">
                                <InsuredName>
                                    <xsl:value-of select="$insured/certName"/>
                                </InsuredName>

                                <!--扩展信息-->
                                <xsl:for-each select="$insured/extendInfos/extendInfo">
                                    <xsl:if test="./@key= 'gender'">
                                        <InsuredSex>
                                            <xsl:value-of select="./@value"/>
                                        </InsuredSex>
                                    </xsl:if>
                                </xsl:for-each>
                            </xsl:when>
                            <!--与投保人为同人-->
                            <xsl:when test="$insured/sameWithHolder ='1' ">
                                <InsuredName>
                                    <xsl:value-of select="$holder/certName"/>
                                </InsuredName>
                                <!--扩展信息-->
                                <xsl:for-each select="$holder/extendInfos/extendInfo">
                                    <xsl:if test="./@key= 'gender'">
                                        <InsuredSex>
                                            <xsl:value-of select="./@value"/>
                                        </InsuredSex>
                                    </xsl:if>
                                </xsl:for-each>
                            </xsl:when>
                        </xsl:choose>
                    </LCPol>
                </LCPols>
                <!--责任-->
              <!--  <LCDutys>
                    <LCDuty>
                        <DutyCode>
                            <xsl:value-of select="$DutyInfo/DutyCode"/>
                        </DutyCode>

                        <CalRule>
                            <xsl:value-of select="$DutyInfo/CalMode"/>
                        </CalRule>
                        <Prem>
                            <xsl:value-of select="$DutyInfo/DutyPrem"/>
                        </Prem>
                        <Amnt>
                            <xsl:value-of select="$DutyInfo/DutyAmnt"/>
                        </Amnt>
                        <InsuYearFlag>
                            <xsl:value-of select="$DutyInfo/InsuYearFlag"/>
                        </InsuYearFlag>
                        <InsuYear>
                            <xsl:value-of select="$DutyInfo/InsuYear"/>
                        </InsuYear>
                        <PayEndYearFlag>
                            <xsl:value-of select="$DutyInfo/PayEndYearFlag"/>
                        </PayEndYearFlag>
                        <PayEndYear>
                            <xsl:value-of select="$DutyInfo/PayEndYear"/>
                        </PayEndYear>
                        <PayIntv>
                            <xsl:value-of select="$DutyInfo/PayIntv"/>
                        </PayIntv>

                    </LCDuty>
                </LCDutys>-->


                <!--健康告知-->
              <!--  <LCCustomerImparts>
                    &lt;!&ndash;被保人健康告知&ndash;&gt;
                    <xsl:for-each select="RequestNodes/RequestNode/Insureds/Insured/InsuImpartInfo/InsuImpart">
                        <LCCustomerImpart>
                            <ImpartCode>
                                <xsl:value-of select="ImpartCode"/>
                            </ImpartCode>
                            <ImpartVer>
                                <xsl:value-of select="ImpartVer"/>
                            </ImpartVer>

                            <ImpartContent>
                                <xsl:value-of select="ImpartContent"/>
                            </ImpartContent>
                            <ImpartParamModle>
                                <xsl:value-of select="Impartparammodle"/>
                            </ImpartParamModle>
                            <CustomerNoType>1</CustomerNoType>
                            <PatchNo>0</PatchNo>
                        </LCCustomerImpart>
                    </xsl:for-each>
                </LCCustomerImparts>-->


                <!--添加紧急联系人表-->
              <!--  <LCAppntLinkManInfo>
                    <Name>
                        <xsl:value-of select="$req/Reserve10"/>
                    </Name>
                    <Tel1>
                        <xsl:value-of select="$req/Reserve12"/>
                    </Tel1>
                    &lt;!&ndash;信函发送形式1.电子2.纸质&ndash;&gt;
                    <LetterSendMode>
                        <xsl:value-of select="$req/Reserve14"/>
                    </LetterSendMode>
                    <PrtNo>
                        <xsl:value-of select="$req/ProposalNo"/>
                    </PrtNo>
                    <Email>
                        <xsl:value-of select="$holder/AppEmail"/>
                    </Email>
                </LCAppntLinkManInfo>-->
                <!--以下仅有对象，没有值，但是微服务需要-->
                <LDPersons/>
                <!--<LCDutys/>-->
                <LCGets/>
                <LCPrems/>
                <LCCustomerImpartParamses/>
                <LCCustomerImpartDetails/>
            </Body>
        </TranData>
    </xsl:template>
</xsl:stylesheet>

