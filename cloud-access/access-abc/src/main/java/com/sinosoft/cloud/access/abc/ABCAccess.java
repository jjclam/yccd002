package com.sinosoft.cloud.access.abc;

import com.sinosoft.cloud.access.annotations.AccessChannel;
import com.sinosoft.cloud.access.annotations.ProtocolType;
import com.sinosoft.cloud.access.configuration.Access;
import org.springframework.beans.factory.annotation.Value;

/**
 * @Author: yangming
 * @Date: 2017/9/01 14:55
 * @Description:
 */
@AccessChannel(protocol = ProtocolType.SOCKET, name = "rmp", remark = "无", port = 7000)
public class ABCAccess implements Access {


    @Value("${access.abc.port}")
    private String port;

    public ABCAccess() {
   //     System.out.println("new ABCYbtAccess");
    }

    @Override
    public String getPort() {
        return port;
    }

    @Override
    public void setPort(String port) {
        this.port = port;
    }
}
