package com.sinosoft.cloud.access.abc.router;


import com.sinosoft.cloud.access.annotations.AccessRouter;
import com.sinosoft.cloud.access.router.RouterBean;
import org.springframework.beans.factory.annotation.Value;

@AccessRouter(name = "rmp")
public class AbcRouterBean extends RouterBean {

    @Value("${cloud.access.abc.router.body}")
    String bodyEL;

    @Value("${cloud.access.abc.router.head}")
    String headEL;

    @Value("${cloud.access.abc.router.cross}")
    String crossEL;

    @Value("${cloud.access.abc.router.target.ip}")
    String targetIP;

    @Value("${cloud.access.abc.router.target.port}")
    String targetPort;

    @Override
    protected String getBodyEL() {
        return bodyEL;
    }

    @Override
    protected String getHeadEL() {
        return headEL;
    }


    @Override
    protected void setBodyEL(String bodyEL) {
        this.bodyEL = bodyEL;
    }

    @Override
    protected void setHeadEL(String headEL) {

        this.headEL = headEL;
    }

    @Override
    protected String getCrossEL() {
        return crossEL;
    }

    @Override
    protected void setCrossEL(String crossEL) {
        this.crossEL = crossEL;
    }

    @Override
    public String getTargetIP() {
        return targetIP;
    }

    @Override
    public String getTargetPort() {
        return targetPort;
    }

    @Override
    public String getTargetUrl() {
        return null;
    }

}
