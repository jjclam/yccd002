package com.sinosoft.cloud.access.abc.util;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;
import org.apache.commons.io.FileUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Iterator;
import java.util.Properties;
import java.util.Vector;

public class SftpManager {
    private static SftpManager instance = new SftpManager();
    private ChannelSftp channelSftp = null;
    private Session sshSession = null;
    private final Log logger = LogFactory.getLog(this.getClass());

    private SftpManager() {

    }

    public static SftpManager getInstance() {
        return instance;
    }

    /**
     * 登录，创建连接 返回file
     *
     * @param url
     * @param port
     * @param username
     * @param password
     * @throws Exception
     */
    public File downloadFile(String url, int port, String username, String password, String ftpDir, String ftpFilename) {
        try {
            JSch jsch = new JSch();
            sshSession = jsch.getSession(username, url, port);
            sshSession.setPassword(password);
            Properties sshConfig = new Properties();
            sshConfig.put("StrictHostKeyChecking", "no");
            sshSession.setConfig(sshConfig);
            sshSession.connect();
            Channel channel = sshSession.openChannel("sftp");
            channel.connect();
            channelSftp = (ChannelSftp) channel;
            logger.info("核心文件目录：" + ftpDir + "====文件名：" + ftpFilename);
            InputStream inputStream = channelSftp.get(ftpDir + ftpFilename);
            if (inputStream != null) {
                File newFile = new File(ftpFilename);
                //将InputStream转File
                FileUtils.copyToFile(inputStream, newFile);
                return newFile;
            }


        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            if (channelSftp != null) {
                channelSftp.exit();
            }
        }
        return null;
    }

    /**
     * 从sftp服务器下载文件到本地
     *
     * @param ftpDir
     * @param ftpFilename
     * @return
     */
   /* public InputStream downloadFile(String ftpDir, String ftpFilename,
                                    String localDir, String localFilename) throws Exception {

        channelSftp.cd(ftpDir);
        File localDirFile = new File(localDir);
        if (!localDirFile.exists()) {
            localDirFile.mkdirs();
        }
        File localFile = new File(localDir, localFilename);
        FileOutputStream output = new FileOutputStream(localFile);
        channelSftp.get(ftpFilename, output);
        logger.info(" 下载结束");
        // 关闭流
        close();
    }*/

    /**
     * ChannelSftp下载目录下所有或指定文件
     * ChannelSftp获取某目录下所有文件名称
     * InputStream转File
     */
    /*public Map<String, File> downFile(String remoteDir) {
        Map<String, File> fileMap = new HashMap<>(20);
        if (StringUtil.isEmpty(remoteDir)) {
            return fileMap;
        }
        ChannelSftp channelSftp = null;
        try {
            JSch jsch = new JSch();
            Session sshSession = jsch.getSession("name", "host", 22);
            sshSession.setPassword("password");
            Properties sshConfig = new Properties();
            //当第一次连接服务器时，自动接受新的公钥
            sshConfig.put("StrictHostKeyChecking", "no");
            sshSession.setConfig(sshConfig);
            sshSession.connect();
            Channel channel = sshSession.openChannel("sftp");
            channel.connect();
            channelSftp = (ChannelSftp) channel;

            //ls命令获取文件名列表
            Vector vector = channelSftp.ls(remoteDir);
            Iterator iterator = vector.iterator();
            while (iterator.hasNext()) {
                ChannelSftp.LsEntry file = (ChannelSftp.LsEntry) iterator.next();
                //文件名称
                String fileName = file.getFilename();
                //todo 这里可使用if或正则不下载一些文件
                InputStream inputStream = channelSftp.get(remoteDir + fileName);
                if (inputStream != null) {
                    File newFile = new File(fileName);
                    //将InputStream转File
                    FileUtils.copyToFile(inputStream, newFile);
                    fileMap.put(fileName, newFile);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (channelSftp != null) {
                channelSftp.exit();
            }
        }
        return fileMap;
    }*/


    /**
     * 上传本地文件到sftp服务器
     *
     * @param ftpDir
     * @param ftpFilename
     * @param localDir
     * @param localFilename
     * @return
     */
    public void uploadFile(String ftpDir, String ftpFilename, String localDir,
                           String localFilename) throws Exception {
        File localFile = new File(localDir, localFilename);
        FileInputStream input = new FileInputStream(localFile);
        changeDir(ftpDir);
        channelSftp.put(input, ftpFilename);
        // 关闭流
        close();
    }


    /**
     * 切换目录，若切换目录失败，则创建该目录
     *
     * @param ftpDir
     */
    private void changeDir(String ftpDir) {
        try {
            channelSftp.cd(ftpDir);
        } catch (Exception e) {
            try {
                channelSftp.mkdir(ftpDir);
                channelSftp.cd(ftpDir);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    /**
     * 关闭流
     */
    private void close() {
        try {
            if (channelSftp != null) {
                channelSftp.disconnect();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            if (sshSession != null) {
                sshSession.disconnect();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
