package com.sinosoft.cloud.access.abc;

import com.sinosoft.cloud.access.annotations.AccessChannel;
import com.sinosoft.cloud.access.annotations.ProtocolType;
import com.sinosoft.cloud.access.configuration.Access;
import org.springframework.beans.factory.annotation.Value;

/**
 * @Author: yangming
 * @Date: 2017/9/01 14:55
 * @Description:
 */
@AccessChannel(protocol = ProtocolType.SOCKET, name = "shlife", remark = "无", port = 7000)
public class AliABCAccess implements Access {


    @Value("${access.shlife.port}")
    private String port;

    public AliABCAccess() {
   //     System.out.println("new ABCYbtAccess");
    }

    @Override
    public String getPort() {
        return port;
    }

    @Override
    public void setPort(String port) {
        this.port = port;
    }
}
