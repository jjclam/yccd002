package com.sinosoft.cloud.access.abc.util;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.sinosoft.cloud.access.entity.AliHead;
import com.sinosoft.cloud.rest.TradeInfo;
import com.sinosoft.lis.entity.LCHangUpPojo;
import com.sinosoft.lis.entity.surrendValuePojo;
import org.springframework.stereotype.Component;

@Component
public class DataUtils {
    /**
     * 将string转data针对蚂蚁退保接口
     *
     * @param msg
     * @return
     */
    public static TradeInfo json2Info(String msg) {
        JSONObject dataJson = JSONObject.parseObject(msg);
        TradeInfo tradeInfo = new TradeInfo();
        //1、将请求报文头封装进alihead
        AliHead aliHead = new AliHead();
        surrendValuePojo surrendValue = new surrendValuePojo();
        JSONObject header = dataJson.getJSONObject("header");
        String version = header.getString("version");
        aliHead.setVersion(version);
        String function = header.getString("function");
        aliHead.setFunction(function);
        String transTime = header.getString("transTime");
        aliHead.setTransTime(transTime);
        String transTimeZone = header.getString("transTimeZone");
        aliHead.setTransTimeZone(transTimeZone);
        String reqMsgId = header.getString("reqMsgId");
        aliHead.setReqMsgId(reqMsgId);
        surrendValue.setReqMsgId(reqMsgId);
        String format = header.getString("format");
        aliHead.setFormat(format);
        String signType = header.getString("signType");
        aliHead.setSignType(signType);
        String asyn = header.getString("asyn");
        aliHead.setAsyn(asyn);
        String cid = header.getString("cid");
        aliHead.setCid(cid);
        tradeInfo.addData(AliHead.class.getName(), aliHead);


        //2、将请求报文body进行解析封装
        JSONObject body = dataJson.getJSONObject("body");
        //机构流水号
        String instSerialNo = body.getString("instSerialNo");
        surrendValue.setInstSerialNo(instSerialNo);
        //蚂蚁保险平台退保订单号
        String endorseNo = body.getString("endorseNo");
        surrendValue.setEndorseNo(endorseNo);
        //蚂蚁保险平台退保申请时间
        String endorseCreateTime = body.getString("endorseCreateTime");
        surrendValue.setEndorseCreateTime(endorseCreateTime);
        //蚂蚁保险平台退保时间
        String endorseTime = body.getString("endorseTime");
        surrendValue.setEndorseTime(endorseTime);
        //具体退保原因类型（代表用户意愿）
        String endorseReason = body.getString("endorseReason");
        surrendValue.setEndorseReason(endorseReason);
        //具体退保原因描述（代表用户意愿）
        String endorseReasonDesc = body.getString("endorseReasonDesc");
        surrendValue.setEndorseReasonDesc(endorseReasonDesc);
        //退保金额
        String endorseFee = body.getString("endorseFee");
        surrendValue.setEndorseFee(endorseFee);


        //2.1 保单信息
        JSONObject Policy = body.getJSONObject("Policy");
        // 机构保单号
        String outPolicyNo = Policy.getString("outPolicyNo");
        surrendValue.setContNo(outPolicyNo);
        // 蚂蚁保险平台订单号
        String policyNo = Policy.getString("policyNo");
        surrendValue.setPolicyNo(policyNo);
        //蚂蚁内部机构产品编号
        String prodNo = Policy.getString("prodNo");
        surrendValue.setProductCode(prodNo);
        //蚂蚁内部标准产品编号
        String spNo = Policy.getString("spNo");
        surrendValue.setRiskCode(spNo);
        //退保性质
        String surrenderReason = Policy.getString("surrenderReason");
        surrendValue.setSurrenderReason(surrenderReason);
        //2.2 账单信息 退保金额为0时，无此信息
        if (!"0".equals(endorseFee)) {
            JSONArray billList = body.getJSONArray("billList");
            for (int i = 0; i < billList.size(); i++) {
                JSONObject bill = billList.getJSONObject(i);
                //支付宝收入账户
                String inAlipayAccount = bill.getString("inAlipayAccount");
                surrendValue.setInAccount(inAlipayAccount);
                //支付宝支出账号
                String outAlipayAccoun = bill.getString("outAlipayAccoun");
                surrendValue.setOutAccount(outAlipayAccoun);
                //支付发生时间
                String payTime = bill.getString("payTime");
                surrendValue.setPayTime(payTime);
                //发生金额
                String fee = bill.getString("fee");
                surrendValue.setFee(fee);
                //支付流水号
                String payFlowId = bill.getString("payFlowId");
                surrendValue.setPayFlowId(payFlowId);
                //优惠金额
                String discountFee = bill.getString("discountFee");
                surrendValue.setDiscountFee(discountFee);
            }

        }

        //2.3 扩展信息
        JSONObject bizData = body.getJSONObject("bizData");
        /*退保犹豫期类型
         none: 无犹豫期
         in：犹豫期内
         out：犹豫期外*/
        String hesitationType = bizData.getString("hesitationType");
        surrendValue.setHesitationType(hesitationType);
        tradeInfo.addData(surrendValuePojo.class.getName(), surrendValue);
        return tradeInfo;
    }

    /**
     * 挂起接口\解挂接口解析json
     */
    public static TradeInfo hangUpJson2Info(String msg) {
        JSONObject dataJson = JSONObject.parseObject(msg);
        TradeInfo tradeInfo = new TradeInfo();
        //1、将请求报文头封装进alihead
        AliHead aliHead = new AliHead();
        LCHangUpPojo lcHangUpPojo = new LCHangUpPojo();
        JSONObject header = dataJson.getJSONObject("header");
        String version = header.getString("version");
        aliHead.setVersion(version);
        String function = header.getString("function");
        aliHead.setFunction(function);
        String transTime = header.getString("transTime");
        aliHead.setTransTime(transTime);
        String transTimeZone = header.getString("transTimeZone");
        aliHead.setTransTimeZone(transTimeZone);
        String reqMsgId = header.getString("reqMsgId");
        aliHead.setReqMsgId(reqMsgId);
        lcHangUpPojo.setReqMsgId(reqMsgId);
        String format = header.getString("format");
        aliHead.setFormat(format);
        String signType = header.getString("signType");
        aliHead.setSignType(signType);
        String asyn = header.getString("asyn");
        aliHead.setAsyn(asyn);
        String cid = header.getString("cid");
        aliHead.setCid(cid);
        tradeInfo.addData(AliHead.class.getName(), aliHead);

        //2、将请求报文body进行解析封装
        JSONObject body = dataJson.getJSONObject("body");
        //蚂蚁保单号
        String policyNo = body.getString("policyNo");
        lcHangUpPojo.setPolicyNo(policyNo);

        //蚂蚁保险平台退保订单号
        String instPolicyNo = body.getString("instPolicyNo");
        lcHangUpPojo.setContNo(instPolicyNo);


        //挂起保单用业务单号
        String hangUpBizNo = body.getString("hangUpBizNo");
        lcHangUpPojo.setHangUpBizNo(hangUpBizNo);
        tradeInfo.addData(LCHangUpPojo.class.getName(), lcHangUpPojo);

        return tradeInfo;

    }

    public static JSON hangInfo2Json(TradeInfo tradeInfo) {
        AliHead aliHead = (AliHead) tradeInfo.getData(AliHead.class.getName());
        LCHangUpPojo lcHangUpPojo = (LCHangUpPojo) tradeInfo.getData(LCHangUpPojo.class.getName());
        JSONObject object = new JSONObject();
        JSONObject response = new JSONObject();
        JSONObject head = new JSONObject();
        head.put("version", aliHead.getVersion());
        head.put("function", aliHead.getFunction());
        head.put("transTime", aliHead.getTransTime());
        head.put("transTimeZone", aliHead.getTransTimeZone());
        head.put("reqMsgId", aliHead.getReqMsgId());
        head.put("format", aliHead.getFormat());
        head.put("signType", aliHead.getSignType());
        head.put("asyn", aliHead.getAsyn());
        head.put("cid", aliHead.getCid());
        response.put("head", head);
        JSONObject body = new JSONObject();
        JSONObject instBizInfo = new JSONObject();
        String resultStatus = (String) tradeInfo.getData("resultStatus");
        if ("F".equals(resultStatus)) {//出现异常
            instBizInfo.put("hangUpBizNo", lcHangUpPojo.getHangUpBizNo());
            body.put("instBizInfo", instBizInfo);

        } else {
            body.put("resultStatus", resultStatus);
            String resultCode = (String) tradeInfo.getData("resultCode");
            body.put("resultCode", resultCode);
            String resultDesc = (String) tradeInfo.getData("resultDesc");
            body.put("resultDesc", resultDesc);


        }
        response.put("body", body);
        object.put("response", response);

        return object;

    }
}

