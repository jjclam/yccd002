package com.sinosoft.cloud.access.abc.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;

import java.util.Map;

/**
 * @Author: 崔广东
 * @Date: 2018-3-12 11:33
 * @Description:
 */
@PropertySource(value = "classpath*:bankServer.properties")
@ConfigurationProperties(prefix = "key")
public class BankServerProperties {

    private Map<String, BankServer> server;

    public Map<String, BankServer> getServer() {
        return server;
    }

    public void setServer(Map<String, BankServer> server) {
        this.server = server;
    }
}
