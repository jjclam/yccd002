package com.sinosoft.cloud.access.abc.util;


import com.sinosoft.cloud.access.configuration.MessageProducer;
import com.sinosoft.cloud.rest.TradeInfo;
import com.sinosoft.lis.entity.LogLktranStracksPojo;
import com.sinosoft.lis.pubfun.PubFun;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


@Component
public class SendMQBL {

    @Autowired
    MessageProducer producer;


    public void dealInfo(TradeInfo respTradeInfo) {
        CreateXMLBySAX tCreateXMLBySAX = new CreateXMLBySAX();
        String xmlStr = null;
        try {
            xmlStr = tCreateXMLBySAX.createXML(respTradeInfo);
        } catch (Exception e) {
            e.printStackTrace();
        }

        /*
         *发送mq消息
         */
        producer.send("sendToLis", xmlStr);
    }


}
