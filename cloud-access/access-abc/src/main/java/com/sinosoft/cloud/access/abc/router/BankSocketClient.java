package com.sinosoft.cloud.access.abc.router;

import com.sinosoft.cloud.access.net.AccessControl;
import com.sinosoft.cloud.access.net.DownloadDecoder;
import com.sinosoft.cloud.access.net.Handler;
import com.sinosoft.cloud.access.net.SocketHandler;
import com.sinosoft.cloud.access.service.AccessServiceGate;
import com.sinosoft.cloud.common.SpringContextUtils;
import io.netty.bootstrap.Bootstrap;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.timeout.ReadTimeoutHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.TimeUnit;


/**
 * @Author: 崔广东
 * @Date: 2018-3-13 9:14
 * @Description:
 */
public class BankSocketClient extends SocketHandler implements Handler {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    private AccessControl accessControl = SpringContextUtils.getBeanByClass(AccessControl.class);

    private String sendToBankMsg;
    private String originalMsg;
    private String path;
    private String transCode;
    private boolean hasError = false;

    /**
     * 连接xxx的客户端
     *
     * @param host
     * @param port
     * @throws Exception
     */
    public void connect(String host, String port) throws Exception {
        NioEventLoopGroup group = new NioEventLoopGroup();
        try {
            Bootstrap b = new Bootstrap();
            b.group(group).channel(NioSocketChannel.class)
                    .option(ChannelOption.TCP_NODELAY, true)
                    .handler(new ChannelInitializer<SocketChannel>() {
                        @Override
                        protected void initChannel(SocketChannel ch) throws Exception {
                            ch.pipeline().addLast(new ReadTimeoutHandler(accessControl.getCipReturnTimeOut(), TimeUnit.MILLISECONDS));
//                            DefaultDecoder defaultDecoder = new DefaultDecoder();
//                            defaultDecoder.setAccessName(accessName);
                            DownloadDecoder downloadDecoder = new DownloadDecoder();
                            downloadDecoder.setAccessName(accessName);
                            ch.pipeline().addLast(downloadDecoder);
                            ch.pipeline().addLast(new BankSocketClient());
                        }
                    });

            ChannelFuture f = b.connect(host, Integer.parseInt(port))/*.addListener(new ChannelFutureListener() {
                @Override
                public void operationComplete(ChannelFuture future) throws Exception {

                    if (!future.isSuccess()) {

                        for (int i = 0; i < socketHandlers.size(); i++) {
                            if (socketHandlers.get(i) instanceof RouterHandler) {
                                RouterHandler routerHandler = (RouterHandler) socketHandlers.get(i);
                                String errorStr = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><ABCB2I><Header><RetCode>009999</RetCode><RetMsg>系统繁忙，请稍后重试</RetMsg><SerialNo></SerialNo><InsuSerial/><TransDate></TransDate><TranTime></TranTime><BankCode></BankCode><CorpNo></CorpNo><TransCode></TransCode></Header></ABCB2I>";

                                if (routerHandler.needCrypt()) {
                                    errorStr = routerHandler.encrypt(errorStr.trim());
                                }

                                Coder coder = decoderFactory.getDecoder(routerHandler.getAccessName());
                                if (coder != null) {
                                    errorStr = coder.encoder(errorStr);
                                }

                                routerHandler.receive(errorStr);
                                logger.error("连接XXX异常！无法获取连接！" + System.getProperty("line.separator")
                                        + "解密后的报文：" + routerHandler.getOrgXml() + System.getProperty("line.separator")
                                        + "返回的报文：" + errorStr);
                            }
                        }
                    }
                }
            })*/.sync();
//            ChannelFuture f = b.connect(host, Integer.parseInt(port)).sync();
            f.channel().closeFuture().sync();
        } finally {
            group.shutdownGracefully();
        }
    }


    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) {
        if (msg instanceof String) {
//            ByteBuf in = (ByteBuf) msg;
//            String strMsg = in.toString(Charset.defaultCharset());
            String strMsg = (String) msg;
            logger.debug("收到xxx返回的信息为：" + strMsg);
            AccessServiceGate accessServiceGate = AccessServiceGate.getInstance();
            setOriginalMsg(strMsg);
            ctx.close();
        }
    }

    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        String encoderXml = getSendToBankMsg();
        ByteBuf encoded = ctx.alloc().buffer(4 * encoderXml.length());
        logger.debug("已经发送1017需要的报文 : " + encoderXml);
        encoded.writeBytes(encoderXml.getBytes());
        ctx.write(encoded);
        ctx.flush();
    }

    public String getOriginalMsg() {
        return originalMsg;
    }

    public void setOriginalMsg(String originalMsg) {
        this.originalMsg = originalMsg;
    }

    public String getSendToBankMsg() {
        return sendToBankMsg;
    }

    public void setSendToBankMsg(String sendToBankMsg) {
        this.sendToBankMsg = sendToBankMsg;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public boolean isHasError() {
        return hasError;
    }

    public void setHasError(boolean hasError) {
        this.hasError = hasError;
    }

    public String getTransCode() {
        return transCode;
    }

    public void setTransCode(String transCode) {
        this.transCode = transCode;
    }

    @Override
    public String receive(String msg) {
        return null;
    }

    @Override
    public String send() {
        return null;
    }
}
