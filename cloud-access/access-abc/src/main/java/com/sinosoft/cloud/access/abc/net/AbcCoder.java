package com.sinosoft.cloud.access.abc.net;

import com.sinosoft.cloud.access.annotations.AccessCoder;
import com.sinosoft.cloud.access.net.CoderBean;

@AccessCoder(name = "abc")
public class AbcCoder extends CoderBean {

//    private final static String ENCODER_FORMAT = "X1.0        3002    20000                                        00000000";
    private final static String ENCODER_FORMAT = "X1.000000000    111821000                                       000000000";

    /**
     * xxx报文头格式:
     * 字段              长度              说明
     * -----------------------------------
     * 报文类型                C1              固定为“X”，表示标准接口
     * 版本号              C3             当前版本为1.0
     * 数据包长度           N8             包体的长度，包体的长度不能超过50K字节。如果加密，指加密后的长度；如果压缩，指定压缩后的长度。
     * 公司代码                C8              xxx分配给公司的代码：
     * 加密标示                C1              数据包是否加密：0-不加密；1-关键数据加密；2-报文整体加密。
     * 加密算法                C1
     * 数据压缩标志              C1              数据包是否压缩传输：0-不压缩；1-压缩
     * 数据压缩算法              C1
     * 摘要算法                C1
     * 摘要              C40
     * 预留字段                C8              扩展域，固定值00000000
     *
     * @param msg
     * @return
     */
    @Override
    public boolean needDecoder(String msg) {
        boolean chk = msg.startsWith("X");
        logger.debug("是否需要校验解码? " + chk);
        return chk;
    }

    @Override
    public int getBodySizeByHead(String msg) {
        if (msg != null) {
            String strSize = msg.substring(4, 12);
            return Integer.parseInt(strSize.trim());
        }
        return 0;
    }

    @Override
    public int getHeadSize() {
        return 73;
    }

    /**
     * 获取报文信息
     *
     * @param msg
     * @return
     */
    @Override
    public int getBodySize(String msg) {

        if (msg == null) {
            logger.error("传入消息为空,请检查消息");
            throw new RuntimeException("传入消息为空,请检查消息");
        }

        if (msg.length() < 74) {
            logger.error("消息传入长度不对! xxx消息报文头应该有74,实际为:" + msg.length());
            return 0;
//            throw new RuntimeException("消息传入长度不对! xxx消息报文头应该有74,实际为:" + msg.length());
        }

        logger.debug("getBodySize 报文总长度:" + msg.getBytes().length);
        logger.debug("报文内容长度："+msg.substring(73).getBytes().length);

        //xxx传输的报文切掉前面的73个字符
        if (msg.startsWith("X")) {
            return msg.substring(73).getBytes().length;
        } else {
            logger.error("报文传入错误,xxx报文以X开头,实际为:" + msg);
            throw new RuntimeException("报文传入错误,xxx报文以X开头,实际为:" + msg);
        }
    }


    @Override
    public String encoder(String msg) {
        int size = msg.getBytes().length;
        logger.debug("返回报文体的长度："+size);
        String strSize = String.valueOf(size);
        StringBuilder stringBuilder = new StringBuilder(ENCODER_FORMAT);
//        StringBuilder x = stringBuilder.replace(4, 4 + strSize.getBytes().length, strSize).append(msg);
        StringBuilder x = stringBuilder.replace(12-strSize.getBytes().length, 12, strSize).append(msg);
        logger.debug("返回报文预期长度："+x.toString().substring(4,12));
        return x.toString();
    }

}
