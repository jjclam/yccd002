package com.sinosoft.cloud.access.abc.config;

import org.springframework.core.io.Resource;

/**
 * @Author: 崔广东
 * @Date: 2018-3-12 11:35
 * @Description:
 */
public class BankServer {

    private String id;
    private String name;
    private Resource rsaX509File;
    private String corpTradeIP;
    private String corpTradePort;
    private Resource tempKeyFile;
    private Resource keyFile;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Resource getRsaX509File() {
        return rsaX509File;
    }

    public void setRsaX509File(Resource rsaX509File) {
        this.rsaX509File = rsaX509File;
    }

    public String getCorpTradeIP() {
        return corpTradeIP;
    }

    public void setCorpTradeIP(String corpTradeIP) {
        this.corpTradeIP = corpTradeIP;
    }

    public String getCorpTradePort() {
        return corpTradePort;
    }

    public void setCorpTradePort(String corpTradePort) {
        this.corpTradePort = corpTradePort;
    }

    public Resource getTempKeyFile() {
        return tempKeyFile;
    }

    public void setTempKeyFile(Resource tempKeyFile) {
        this.tempKeyFile = tempKeyFile;
    }

    public Resource getKeyFile() {
        return keyFile;
    }

    public void setKeyFile(Resource keyFile) {
        this.keyFile = keyFile;
    }
}
