package com.sinosoft.cloud.access.abc.util;



import java.io.InputStream;
import java.util.Properties;

/**
 * @author wusr 2014-4-13
 * @version 1.0
 */
public class PropertiesUtil {


	/**
	 * 从class根路径加载配置文件
	 * @param name 不要有任何前缀，例如./，直接资源文件的名字
	 * @return
	 * @throws Exception
	 */
	public static Properties loadProperties(String name) throws Exception{
		InputStream inStream = PropertiesUtil.class.getClassLoader().getResourceAsStream(name);
		Properties prop = new Properties();
		prop.load(inStream);
		return prop;
	}
	

	
	public static void main(String[] args) throws Exception{
		Properties prop = PropertiesUtil.loadProperties("config.properties");
		System.out.println(prop.getProperty("appRealPath", "333"));
	}
}
