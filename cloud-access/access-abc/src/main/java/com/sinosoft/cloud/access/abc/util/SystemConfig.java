//package com.sinosoft.cloud.access.abc.util;
//
//
//import org.apache.log4j.Logger;
//
//import java.util.Properties;
//
///**
// * @author wusr 2014-9-9
// * @version 1.0
// */
//public class SystemConfig {
//	private final Logger logger = Logger.getLogger(SystemConfig.class);
//	private static SystemConfig instance = new SystemConfig();
//	private Properties config = null;
//
//	private SystemConfig(){
//		try{
//			config = PropertiesUtil.loadConfig();
//		}catch(Exception e){
//			logger.error("加载系统配置文件失败", e);
//		}
//	}
//
//	public static SystemConfig getInstance(){
//		return instance;
//	}
//
//	public Properties getConfig(){
//		return config;
//	}
//}
