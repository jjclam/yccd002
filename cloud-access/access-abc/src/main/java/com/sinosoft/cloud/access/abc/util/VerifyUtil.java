package com.sinosoft.cloud.access.abc.util;


import org.apache.log4j.Logger;

import java.util.Date;
import java.util.UUID;

/**
 * @author zhangyfsh 2018-03-01
 * @version 1.0
 */
public class VerifyUtil {

	private static final Logger logger = Logger.getLogger(VerifyUtil.class);
	public static final String XML_FILE_ENCODE = "GBK";
	//String privateKey = "123456";

	/**
	 * 合作机构渠道校验
	 * 
	 * @param sign 合作机构签名信息
	 * @param tRequestXml 合作机构请求内容
	 * @param tThirdPartyCode 合作机构号
	 * @return
	 */
	public static boolean verifyThirdPartyRequest(String sign, String tRequestXml, String tThirdPartyCode) {
		String taskID = "合作机构请求报文安全校验(任务ID：" + UUID.randomUUID().toString() + ")任务";
		//String privateKey = SystemConfig.getInstance().getConfig().getProperty("safety.verify.private.key.thirdparty." + tThirdPartyCode);
		//配置文件加载秘钥   注入方式
		// 无对应私钥，安全校验失败
		String privateKey = "43HtOOWFgdnj";

		if (privateKey == null || privateKey.trim().length() == 0) {
			return false;
		}
		try {
			logger.info(taskID + "开始。。。");
			if (sign == null || tRequestXml == null) {
				return false;
			}
			logger.info(taskID + "，合作机构机构代码：" + tThirdPartyCode);
			//logger.info(taskID + "，合作机构签名信息：" + sign)Md5;
			logger.info(taskID + "，合作机构请求内容：" + tRequestXml);
			
			// 将明文（key+msg）通过md5加密s			
			String cipherText = new Md5().md5Encode(privateKey + tRequestXml, XML_FILE_ENCODE);
			logger.info(taskID + " 合作机构MD5: " + sign);
			logger.info(taskID + " 保险公司MD5: " + cipherText);
			
			// 忽略大小写(MD5字符串由于算法不同，可能为abcdef也有可能为ABCDEF)
			boolean verifyResult = cipherText.equalsIgnoreCase(sign);
			logger.info(taskID + "结束！校验结果：" + verifyResult);
			return verifyResult;
		} catch (Exception e) {
			logger.error(taskID + "异常结束！校验结果：" + false, e);
			return false;
		}
	}
	
	/**
	 * 合作机构渠道校验
	 * 
	 * @param sign 合作机构签名信息
	 * @param thirdPartyOrderId 订单号
	 * @param seq 时间戳
	 * @param userId 用户Id
	 * @param tThirdPartyCode 合作机构号
	 * @return
	 */
	public static boolean verifyThirdPartyDownLoad(String sign, String thirdPartyOrderId, String seq, String userId, String tThirdPartyCode) {
		String plainText = thirdPartyOrderId + seq + "xx438imSsG"+ userId;// 明文
		return verifyThirdPartyRequest(sign, plainText, tThirdPartyCode);
	}

	public static void main(String[] args) {
	    //md5加密rs
		//19123155848761577753003 1578906288325 123456 13674
		try {
			String cipherText = new Md5().md5Encode("43HtOOWFgdnj" + "200310619725015838089601583817798218xx438imSsG4054", XML_FILE_ENCODE);
			//String cipherText =new Md5().md5Encode("123456 20010342523531578023845 1578281116440 123456 6650", "UTF8");
			System.out.println("cipherText:"+cipherText);
		}catch (Exception e){
			e.printStackTrace();
		}

		/*Date now = new Date();
		long nowtime=now.getTime()-3600 * 1 * 1000;
		System.out.println(nowtime);*/
	}
}
