package com.sinosoft.cloud.access.abc.util;


import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.File;
import java.util.Date;

/**
 * @author rs 2019-12-27
 * @version 1.0
 */
public class SftpUtil {
    private final Log logger = LogFactory.getLog(this.getClass());

    /**
     * 获取文件名
     *
     * @param date
     * @return
     */
    private static synchronized String createFileName(String date, String filename, String suffix) {
        return filename + "_" + date + suffix;
    }

    /**
     * 获取当前交易的txt本地存放路径
     *
     * @param config
     * @return
     * @throws Exception
     */
	/*public static synchronized File getLocalFile(LDFtpConfig config) throws Exception {
		return getLocalFile(null, config);
	}*/

    /**
     * 获取当前交易的txt本地存放路径
     *
     * @param date
     * @param config
     * @return
     * @throws Exception
     */
    /*public static synchronized File getLocalFile(String date, LDFtpConfig config) throws Exception {
     *//**
     * 如果date为空，默认为当前时间
     *//*
		if (date == null) {
			date = DateUtil.format(new Date(), TxtConstant.FILE_PATH_DATE_PATTERN);
		}

		// 本地文件目录
		String localDir = new File(config.getLocaldir(), date).getCanonicalPath();
		// 本地文件名
		String localFilename = createFileName(date, config.getLocalfilename(), config.getLocalsuffixname());

		return new File(localDir, localFilename);
	}*/

    /**
     * 从sftp服务器下载文件到本地
     *
     * @param date
     * @param config
     * @return
     * @throws Exception
     */
	/*public static synchronized void download(String date, LDFtpConfig config) throws Exception {
		// ftp服务器文件存放目录
		String ftpDir = config.getFtpdir() + date;
		// ftp文件名
		String ftpFilename = createFileName(date, config.getFtpfilename(), config.getFtpsuffixname());
		// 本地下载目录
		String localDir = new File(config.getLocaldir(), date).getCanonicalPath();
		// 本地文件名
		String localFilename = createFileName(date, config.getLocalfilename(), config.getLocalsuffixname());
		downloadFile(config.getFtpaddress(), config.getFtpusername(), config.getFtppassword(), config.getFtpport(), ftpDir, ftpFilename,
				localDir, localFilename);

	}
*/
    /**
     * 上传本地文件到sftp服务器
     *
     * @param date
     * @param config
     * @return
     */
	/*public static synchronized void upload(String date, LDFtpConfig config) throws Exception {
		// ftp服务器文件存放目录
		String ftpDir = config.getFtpdir() + date;
		// ftp文件名
		String ftpFilename = createFileName(date, config.getFtpfilename(), config.getFtpsuffixname());
		// 本地下载目录
		String localDir = new File(config.getLocaldir(), date).getCanonicalPath();
		// 本地文件名
		String localFilename = createFileName(date, config.getLocalfilename(), config.getLocalsuffixname());
		uploadFile(config.getFtpaddress(), config.getFtpusername(), config.getFtppassword(), config.getFtpport(), localDir, localFilename,
				ftpDir, ftpFilename);
	}*/

    /**
     * 下载sftp(address,username,password,port)服务器srcDir目录下srcFile文件到本地destDir目录，
     * 文件名为destFile
     *
     * @param address  sftp服务器地址
     * @param username sftp服务器登陆用户名
     * @param password sftp服务器登陆密码
     * @param port     sftp服务器端口号
     * @param srcDir   待下载文件目录
     * @param srcFile  待下载文件名
     */
    public static synchronized File downloadFile(String address, String username, String password, String port, String srcDir,
                                                 String srcFile) throws Exception {
        /*String destDir = "/home/wusimin" + srcDir;
        String destFile = srcFile;
        File destDirFile = new File(destDir);
        // 本地存放文件的目录不存在，创建新目录
        if (!destDirFile.exists()) {
            destDirFile.mkdirs();
        }*/

        int intPort = Integer.parseInt(port);
        //SftpManager.getInstance().login(address, intPort, username, password);
        File file = SftpManager.getInstance().downloadFile(address, intPort, username, password, srcDir, srcFile);
        return file;
    }

    /**
     * 将本地srcDir目录对的srcFile上传到sftp服务器（address,username,password,port）指定目录srcDir
     *
     * @param address
     *            sftp服务器地址
     * @param username
     *            sftp服务器登陆用户名
     * @param password
     *            sftp服务器登陆密码
     * @param port
     *            sftp服务器端口号
     * @param srcDir
     *            待上传文件目录
     * @param srcFile
     *            待上传文件名
     * @param destDir
     *            上传sftp服务器指定目录
     * @param destFile
     *            上传sftp服务器文件名
     */
	/*private static synchronized void uploadFile(String address, String username, String password, String port, String srcDir, String srcFile, String destDir,
			String destFile) throws Exception {
		int intPort = Integer.parseInt(port);
		SftpManager.getInstance().login(address, intPort, username, password);
		SftpManager.getInstance().uploadFile(destDir, destFile, srcDir, srcFile);
	}*/
}
