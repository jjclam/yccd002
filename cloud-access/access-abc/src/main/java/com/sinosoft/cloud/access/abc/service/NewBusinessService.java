package com.sinosoft.cloud.access.abc.service;

import com.sinosoft.cloud.access.abc.util.SendMQBL;
import com.sinosoft.cloud.access.annotations.AccessService;
import com.sinosoft.cloud.access.service.AccessServiceBean;
import com.sinosoft.cloud.access.util.TimeUtil;
import com.sinosoft.cloud.nb.app.client.*;
import com.sinosoft.cloud.rest.TradeInfo;
import feign.RetryableException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * 新单试算业务处理类
 *
 * @author：崔广东
 */
@AccessService(name = "rmp", transCode = "A0021")
public class NewBusinessService extends AccessServiceBean {

    /**
     * 日志管理
     */
    private final Log cLogger = LogFactory.getLog(this.getClass());

    /*需要调用的微服务接口*/
    //保单承保微服务
    @Autowired
    private ContInputService contInputService;

    @Autowired
    SendMQBL tSendMQBL;

    @Override
    public TradeInfo service(TradeInfo reqTradeInfo) {

        return dealData(reqTradeInfo);
    }

    public TradeInfo dealData(TradeInfo reqTradeInfo) {
        //错误信息
        String infoStr = "";
        String errorLog = "";

        TradeInfo respTradeInfo = removePos(reqTradeInfo);

        long microServiceStart = 0L;
        long microServiceEnd = 0L;


        //调用保单承保微服务
        try {
            cLogger.info("开始调用保单承保微服务");
            microServiceStart = System.currentTimeMillis();
            respTradeInfo = contInputService.service(respTradeInfo);
            microServiceEnd = System.currentTimeMillis();
            infoStr = TimeUtil.getMillis(microServiceStart, microServiceEnd, "保单承保微服务");
            cLogger.info(infoStr);
        } catch (RetryableException e) {
            microServiceEnd = System.currentTimeMillis();
            infoStr = getExceptionStr(microServiceStart, microServiceEnd, "保单承保", e);
            return getErrorTradeInfo(infoStr);
        } catch (Exception e) {
            infoStr = getExceptionStr(0L, 0L, "保单承保", e);
            return getErrorTradeInfo(infoStr);
        }
        if (respTradeInfo.getErrorList().size() == 0&&(respTradeInfo.getData ("return")==""||respTradeInfo.getData ("return")==null)) {
            cLogger.info("调用保单承保微服务成功。返回结果是：" + respTradeInfo.toString());
            //发送Mq消息到核心
            tSendMQBL.dealInfo(respTradeInfo);
        } else {
//            infoStr = respTradeInfo.getErrorList().get(0);
//            errorLog = new StringBuffer(1024).append("调用保单承保微服务内部发生错误，")
//                    .append(respTradeInfo.getErrorList().toString()).toString();
            return respTradeInfo;
        }



        return respTradeInfo;
    }

    @Override
    public TradeInfo test(TradeInfo msg) {
        return msg;
    }

}
