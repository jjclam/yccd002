package com.sinosoft.cloud.access.abc.service;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;

/**
 * 
 * @author 渠道提供
 *
 */
public class Md5 {

	public String md5Encode(String data, String encoding) 
			throws NoSuchAlgorithmException, UnsupportedEncodingException {
	  java.security.MessageDigest md = java.security.MessageDigest.getInstance("MD5");
	  md.update(data.getBytes(encoding));
	  byte tmp[] = md.digest();
	  
	  char hexDigits[] = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};
	  char str[] = new char[16 * 2];
	  int k = 0;
	  for (int i = 0; i < 16; i++) {
	    byte byte0 = tmp[i];
	    str[k++] = hexDigits[byte0 >>> 4 & 0xf];
	    str[k++] = hexDigits[byte0 & 0xf];
	  }
	  return new String(str);
	}

}