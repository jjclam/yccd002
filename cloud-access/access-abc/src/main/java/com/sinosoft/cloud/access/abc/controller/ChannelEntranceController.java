package com.sinosoft.cloud.access.abc.controller;

import com.sinosoft.cloud.access.abc.service.Md5;
import com.sinosoft.cloud.access.transformer.XsltTrans;
import com.sinosoft.cloud.access.util.HttpUtil;
import org.glassfish.jersey.server.internal.LocalizationMessages;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.ForbiddenException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.util.Map;

/**
 * Create by Geny on 2019.09
 */
@Controller
public class ChannelEntranceController {

    private static Logger logger = LoggerFactory.getLogger(ChannelEntranceController.class);


    @Autowired
    @Qualifier("accessMap")
    Map accessMap;

    /*
     * 核保请求
     */
    @RequestMapping(value = "rmp", method = RequestMethod.POST)
    @ResponseBody
    public void validate(HttpServletRequest request, HttpServletResponse response) {
        String sign = request.getParameter("sign");
        logger.info("input sign string: " + sign);
        String data = HttpUtil.readStringFromRequest(request,"GBK");
        logger.info("input requestXml string: " + data);

        if(sign==null || "".equals(sign)){
            throw new ForbiddenException("验签失败");
        }else if ("test".equals(sign)){
            logger.info("测试专用逻辑！！！: " + data);
        } else{
            try{
                String newSign = new Md5().md5Encode("123456" + data, "GBK");
                logger.info("New sign string: " + newSign);
                if(!sign.equals(newSign)){
                    throw new ForbiddenException("验签失败");
                }
            } catch (Exception e){
                throw new ForbiddenException("验签失败");
            }
        }

        if (accessMap.get("rmp") == null) {
            throw new ForbiddenException(LocalizationMessages.USER_NOT_AUTHORIZED());
        }

        XsltTrans xsltTrans = new XsltTrans();
        xsltTrans.setAccessName("rmp");
        String result = xsltTrans.transform(data);


        if (!"test".equals(sign)){
            try {
                result = changeCharset(result,"utf-8","gbk");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }


        logger.info("返回xxx前端GBK格式报文：" + result);
        printResult(response, result);
    }

    /*
    * result 传入字符串
    * charSet 原字符集编码
    * charSetNew 需要转换的字符集编码
    * */
    private String changeCharset(String result, String charSet, String charSetNew) {
        try {
            byte[] temp=result.getBytes(charSet);//这里写原编码方式
            byte[] newTemp=new String(temp,charSet).getBytes(charSetNew);//这里写转换后的编码方式
            result=new String(newTemp,charSetNew);//这里写转换后的编码方式

            logger.info("转换编码后的报文：" + result);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        return result;
    }

    /*
     * 输出最终处理结果
     *
     */
    private void printResult(HttpServletResponse response, String responseMsg) {
        try {
            response.reset();
            response.setHeader("Content-Type", "application/xml;charset=GBK");
            PrintWriter writer = response.getWriter();
            writer.print(responseMsg);
            writer.flush();
            writer.close();
        } catch (Exception e) {
            logger.error("输出结果失败！", e);
        }
    }

    public static void main(String[] args) {
        String requestXml = "<?xml version=\"1.0\" encoding=\"utf-8\"?><Package>   <Head>     <TransTime>2019-10-30 15:01:11</TransTime>      <ThirdPartyCode>14</ThirdPartyCode>      <TransNo>131910291642822873-C1-150111862</TransNo>      <TransType>A0021</TransType>      <Coworker>02</Coworker>   </Head>    <RequestNodes>     <RequestNode>       <ProductCode>ZM_KPB_JD_1</ProductCode>        <ProductName>上海人寿“鲲鹏保”个人意外组合计划经典版</ProductName>        <ThirdPartyOrderId>131910291642822873-1</ThirdPartyOrderId>        <UserId>10033326</UserId>        <AssetId/>        <ContNo/>        <ProposalNo/>        <InsuranceStartPeriod>2019-11-02</InsuranceStartPeriod>        <InsuranceEndPeriod>2020-11-01</InsuranceEndPeriod>        <SignedDate>2019-10-30</SignedDate>        <InsuYear>1</InsuYear>        <InsuYearFlag>Y</InsuYearFlag>        <PayIntv>0</PayIntv>        <PayEndYear>1000</PayEndYear>        <PayEndYearFlag>Y</PayEndYearFlag>        <Premium>38.00</Premium>        <InsuredAmount>0.00</InsuredAmount>        <GrossPremium>38.00</GrossPremium>        <FaceAmount>0.00</FaceAmount>        <FirstPremium>38.00</FirstPremium>        <InitialPremAmt>38.00</InitialPremAmt>        <PolicyDeliveryFee/>        <UnitCount/>        <AgentNo>0101500014</AgentNo>        <ExpireProcessMode></ExpireProcessMode>        <ProductPeriod/>        <ProductPeriodFlag/>        <ABCode>ZY</ABCode>        <Applicant>         <AppNo/>          <RelationToInsured>00</RelationToInsured>          <AppName>冯世琪</AppName>          <AppSex>0</AppSex>          <AppBirthday>1993-10-22</AppBirthday>          <AppIDType>0</AppIDType>          <AppIDNo>220802199310220016</AppIDNo>          <IdexpDate>9999-01-01</IdexpDate>          <NativePlace>CHN</NativePlace>          <Language/>          <Marriage/>          <Stature/>          <Weight/>          <Salary>100000</Salary>          <Province/>          <City/>          <OccupationClass>1</OccupationClass>          <OccupationCode>3010101</OccupationCode>          <SocialInsuFlag/>          <AppEmail>1127998658@qq.com</AppEmail>          <AppTelephone/>          <AppMobile>19806576086</AppMobile>          <AppZipCode></AppZipCode>          <AppAddress/>       </Applicant>        <Insureds>         <Insured>           <InsuNo/>            <InsuRelationToApp>00</InsuRelationToApp>             <InsuRelationToMainInsu>00</InsuRelationToMainInsu>            <IsMainInsured>1</IsMainInsured>            <InsuName>冯世琪</InsuName>            <InsuSex>0</InsuSex>            <InsuBirthday>1993-10-22</InsuBirthday>            <InsuIDType>0</InsuIDType>            <InsuIDNo>220802199310220016</InsuIDNo>            <IdexpDate>9999-01-01</IdexpDate>            <NativePlace>CHN</NativePlace>            <Language/>            <Stature/>            <Weight/>            <Salary>100000</Salary>           <Marriage/>            <SocialInsuFlag/>            <InsuEmail>1127998658@qq.com</InsuEmail>            <InsuTelephone/>            <InsuMobile>19806576086</InsuMobile>            <InsuProvince/>            <InsuCity/>            <InsuZipCode/>            <InsuAddress/>            <OccupationClass>1</OccupationClass>            <OccupationCode>3010101</OccupationCode>            <SameIndustryInsuredAmount/>            <PolicyLiabilities/>         </Insured>       </Insureds>        <Beneficiaries>                </Beneficiaries>        <RenewalPaymentInfo>         <PayMode>J</PayMode>          <PayBankCode/>          <PayBankName/>          <BankProvinceCode/>          <BankCityCode/>          <PayAcountNo/>          <CardType/>          <PayVirtualAcountNo/>          <PayAccountName/>          <PayAccountId/>       </RenewalPaymentInfo>     </RequestNode>   </RequestNodes> </Package>";
        try {
            System.out.println(new Md5().md5Encode("123456" + requestXml, "GBK"));
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }




    }
}
