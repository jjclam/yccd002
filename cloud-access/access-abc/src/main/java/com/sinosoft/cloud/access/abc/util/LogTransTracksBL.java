package com.sinosoft.cloud.access.abc.util;


import com.sinosoft.cloud.access.configuration.MessageProducer;
import com.sinosoft.cloud.rest.TradeInfo;
import com.sinosoft.lis.entity.LogLktranStracksPojo;
import com.sinosoft.lis.pubfun.PubFun;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


@Component
public class LogTransTracksBL {

    @Autowired
    MessageProducer producer;


    public void dealInfo(Long startTime, Long endTime, String serviceName, String AccessChnl, String infoStr, String TransNo, String ProposalNo, String ContNo) {
        TradeInfo tradeInfo = new TradeInfo();

        Long timeOut = endTime - startTime;
        //重新生成物理主键
        LogLktranStracksPojo logLktranStracksPojo = new LogLktranStracksPojo();
        logLktranStracksPojo.setTemp1(String.valueOf(startTime));
        logLktranStracksPojo.setTemp2(String.valueOf(endTime));
        logLktranStracksPojo.setTemp3(String.valueOf(Math.abs(timeOut)));
        if (infoStr.indexOf("。")!=-1) {
            String info = infoStr.split("。")[0];
            logLktranStracksPojo.setPHConclusion(info);
        }else{
            logLktranStracksPojo.setPHConclusion(infoStr);
        }

        logLktranStracksPojo.setTransCode(serviceName);
        logLktranStracksPojo.setTransDate(PubFun.getCurrentDate());
        logLktranStracksPojo.setTransTime(PubFun.getCurrentTime());
        logLktranStracksPojo.setContType("1");//个险
        logLktranStracksPojo.setTransNo(TransNo);
        logLktranStracksPojo.setProposalNo(ProposalNo);
        logLktranStracksPojo.setContNo(ContNo);
        logLktranStracksPojo.setAccessChnl(AccessChnl);
        tradeInfo.addData(logLktranStracksPojo);

        /*
         *发送mq消息logTransTracks
         */
        producer.send("logTransTracks", tradeInfo);
    }


}
