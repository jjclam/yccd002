package com.sinosoft.cloud.access.abc.util;

import com.sinosoft.cloud.access.abc.config.BankServer;
import com.sinosoft.cloud.access.abc.router.BankSocketClient;
import com.sinosoft.cloud.access.entity.TranData;
import com.sinosoft.cloud.access.net.Coder;
import com.sinosoft.cloud.access.net.DecoderFactory;
import com.sinosoft.cloud.access.transformer.XsltTrans;
import com.sinosoft.cloud.rest.TradeInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.Resource;

import java.io.IOException;

/**
 * @Author: 崔广东
 * @Date: 2018-3-15 11:12
 * @Description:
 */
public class BankSocketUtil {

    private static final Logger logger = LoggerFactory.getLogger(BankSocketUtil.class);

    /**
     * 调用发送xxx报文的socket客户端
     *
     * @param tranData
     * @param bankServer
     * @param transCode
     * @param coder
     * @param tradeInfo
     * @return
     */
    public static TradeInfo sendMsgToBank(TranData tranData, BankServer bankServer, String transCode, Coder coder, TradeInfo tradeInfo) {

        XsltTrans xsltTrans = new XsltTrans();
        xsltTrans.setAccessName(bankServer.getId());
//        Resource xslFile = xsltTrans.getBodyOutXsl(transCode);
//        String returnTrans = xsltTrans.returnTrans(tranData, xslFile);
        String returnTrans = "";

        //进行报文加密
        BankSocketClient bankSocketClient = new BankSocketClient();
        bankSocketClient.setAccessName(bankServer.getId());
        if (bankSocketClient.needCrypt()) {
            returnTrans = bankSocketClient.encrypt(returnTrans);
        }
        //增加报文头
        if (coder != null) {
            returnTrans = coder.encoder(returnTrans);
        }
        logger.debug("发送给xxx的" + transCode + "交易的报文为：" + returnTrans);
        bankSocketClient.setSendToBankMsg(returnTrans);
        bankSocketClient.setTransCode(transCode);
        try {
            bankSocketClient.setPath(bankServer.getRsaX509File().getURL().toString().substring(5));
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            bankSocketClient.connect(bankServer.getCorpTradeIP(), bankServer.getCorpTradePort());
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (bankSocketClient.isHasError()) {
            tradeInfo.addError("证书存储错误");
        }

        if ("1017".equals(bankSocketClient.getTransCode())) {
            String originalMsg = bankSocketClient.getOriginalMsg();
            tradeInfo.addData("cert", originalMsg);
        }

        if ("1001".equals(bankSocketClient.getTransCode())) {
            String originalMsg = bankSocketClient.getOriginalMsg();
            tradeInfo.addData("1001ReturnMsg", originalMsg);
        }
        return tradeInfo;
    }
}
