package com.sinosoft.cloud.access.abc.util;


import com.sinosoft.cloud.rest.TradeInfo;
import com.sinosoft.lis.db.LCAddressDB;
import com.sinosoft.lis.db.LCCustomerImpartDB;
import com.sinosoft.lis.entity.*;
import com.sinosoft.lis.vschema.LCAddressSet;
import com.sinosoft.lis.vschema.LCCustomerImpartSet;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.Reflections;
import org.xml.sax.helpers.AttributesImpl;

import javax.xml.transform.OutputKeys;
import javax.xml.transform.Result;
import javax.xml.transform.Transformer;
import javax.xml.transform.sax.SAXTransformerFactory;
import javax.xml.transform.sax.TransformerHandler;
import javax.xml.transform.stream.StreamResult;
import java.io.StringWriter;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class CreateXMLBySAX {


    public String createXML(TradeInfo requestInfo) throws Exception{
        String xmlStr = null;
        Reflections reflections = new Reflections();
        DecimalFormat df = new DecimalFormat("#.00");
        //获取拼装报文的相关类
        LCContPojo mLCContPojo = (LCContPojo) requestInfo.getData(LCContPojo.class.getName());
        List<LCPolPojo> mlcPolPojos = (List<LCPolPojo>) requestInfo.getData(LCPolPojo.class.getName());
        LCAppntPojo mLCAppntPojo = (LCAppntPojo) requestInfo.getData(LCAppntPojo.class.getName());
        List<LCAddressPojo> tlcAddressPojoList = (List<LCAddressPojo>) requestInfo.getData(LCAddressPojo.class.getName());
        ArrayList<LCBnfPojo> mlcBnfPojos = (ArrayList) requestInfo.getData(LCBnfPojo.class.getName());
        ArrayList<LCInsuredPojo> mLCInsuredPojos = (ArrayList<LCInsuredPojo>) requestInfo.getData(LCInsuredPojo.class.getName());
        GlobalPojo mglobalPojo = (GlobalPojo) requestInfo.getData(GlobalPojo.class.getName());
        ArrayList<LCDutyPojo> mlcDutyPojos = (ArrayList<LCDutyPojo>) requestInfo.getData(LCDutyPojo.class.getName());

        if(mlcPolPojos == null){
            mlcPolPojos = new ArrayList<LCPolPojo>();
            mlcPolPojos.add(new LCPolPojo());
        }

        if(tlcAddressPojoList == null){
            tlcAddressPojoList = new ArrayList<LCAddressPojo>();
            tlcAddressPojoList.add(new LCAddressPojo());
        }

        if(mlcDutyPojos == null){
            mlcDutyPojos = new ArrayList<LCDutyPojo>();
            mlcDutyPojos.add(new LCDutyPojo());
        }

        if(mlcBnfPojos == null){
            mlcBnfPojos = new ArrayList<LCBnfPojo>();
            mlcBnfPojos.add(new LCBnfPojo());
        }

        if(mLCInsuredPojos == null){
            mLCInsuredPojos = new ArrayList<LCInsuredPojo>();
            mLCInsuredPojos.add(new LCInsuredPojo());
        }

        //用来得到XML字符串形式
        // 一个字符流，可以用其回收在字符串缓冲区中的输出来构造字符串
        StringWriter writerStr = new StringWriter();
        // 构建转换结果树所需的信息。
        Result resultStr = new StreamResult(writerStr);

        // 创建SAX转换工厂
        SAXTransformerFactory sff = (SAXTransformerFactory)SAXTransformerFactory.newInstance();
        // 转换处理器，侦听 SAX ContentHandler
        //解析事件，并将它们转换为结果树 Result
        TransformerHandler transformerhandler = sff.newTransformerHandler();
        // 将源树转换为结果树
        Transformer transformer = transformerhandler.getTransformer();
        // 设置字符编码
        transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
        // 是否缩进
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");

        //设置与用于转换的此 TransformerHandler 关联的 Result
        //注：这两个th.setResult不能同时启用
        //   th.setResult(resultXml);
        transformerhandler.setResult(resultStr);
        transformerhandler.startDocument();
        AttributesImpl attr = new AttributesImpl();
        /**   Add By Xcc  开始加载解析实体   */


        //创建Package节点
        transformerhandler.startElement("", "Package", "Package", attr);

        //创建Head节点
        transformerhandler.startElement("", "Head", "Head", attr);
            //创建TransTime节点
            transformerhandler.startElement("", "TransTime", "TransTime", attr);
                String Nowdays = selectNowTime();
                if(Nowdays!=null&&!("".equals(Nowdays))){
                    transformerhandler.characters(Nowdays.toCharArray(), 0, Nowdays.length());
                }
            transformerhandler.endElement("", "", "TransTime");

            //创建ThirdPartyCode节点
            transformerhandler.startElement("", "ThirdPartyCode", "ThirdPartyCode", attr);
                if (mLCContPojo.getSellType()!=null&&!("".equals(mLCContPojo.getSellType()))){
                    transformerhandler.characters(mLCContPojo.getSellType().toCharArray(), 0, mLCContPojo.getSellType().length());
                }
            transformerhandler.endElement("", "", "ThirdPartyCode");

            //创建TransCode节点
            transformerhandler.startElement("", "TransType", "TransType", attr);
                String transType = "A0001";
                if (transType!=null&&(!("".equals(transType)))){
                    transformerhandler.characters(transType.toCharArray(), 0, transType.length());
                }
            transformerhandler.endElement("", "", "TransType");

            //创建Coworker节点
            transformerhandler.startElement("", "Coworker", "Coworker", attr);
                String Coworker = "02";
                transformerhandler.characters(Coworker.toCharArray(), 0, Coworker.length());
            transformerhandler.endElement("", "", "Coworker");
        transformerhandler.endElement("", "", "Head");

        //创建RequestNodes节点
        transformerhandler.startElement("", "RequestNodes", "RequestNodes", attr);
            //创建RequestNode节点
            transformerhandler.startElement("", "RequestNode", "RequestNode", attr);
                //创建ProductCode节点
                transformerhandler.startElement("", "ProductCode", "ProductCode", attr);
                    if (mLCInsuredPojos.get(0).getContPlanCode()!=null&&(!("".equals(mLCInsuredPojos.get(0).getContPlanCode())))){
                        transformerhandler.characters(mLCInsuredPojos.get(0).getContPlanCode().toCharArray(), 0, mLCInsuredPojos.get(0).getContPlanCode().length());
                    }
                transformerhandler.endElement("", "", "ProductCode");

                //创建ProductName节点
                transformerhandler.startElement("", "ProductName", "ProductName", attr);
                transformerhandler.endElement("", "", "ProductName");

                //创建ThirdPartyOrderId节点
                transformerhandler.startElement("", "ThirdPartyOrderId", "ThirdPartyOrderId", attr);
                    if (mLCContPojo.getThirdPartyOrderId()!=null&&(!("".equals(mLCContPojo.getThirdPartyOrderId())))){
                        transformerhandler.characters(mLCContPojo.getThirdPartyOrderId().toCharArray(), 0, mLCContPojo.getThirdPartyOrderId().length());
                    }
                transformerhandler.endElement("", "", "ThirdPartyOrderId");

                //创建UserId节点
                transformerhandler.startElement("", "UserId", "UserId", attr);
                    if (mLCContPojo.getUserId()!=null&&(!("".equals(mLCContPojo.getUserId())))){
                        transformerhandler.characters(mLCContPojo.getUserId().toCharArray(), 0,mLCContPojo.getUserId().length());
                    }
                transformerhandler.endElement("", "", "UserId");

                //AssetId
                transformerhandler.startElement("", "AssetId", "AssetId", attr);
                transformerhandler.endElement("", "", "AssetId");

                //创建ContNo节点
                transformerhandler.startElement("", "ContNo", "ContNo", attr);
                    if (mLCContPojo.getContNo()!=null&&(!("".equals(mLCContPojo.getContNo())))){
                        transformerhandler.characters(mLCContPojo.getContNo().toCharArray(), 0, mLCContPojo.getContNo().length());
                    }
                transformerhandler.endElement("", "", "ContNo");

                //创建prtno节点
                transformerhandler.startElement("", "prtno", "prtno", attr);
                    if (mLCContPojo.getPrtNo()!=null&&(!("".equals(mLCContPojo.getPrtNo())))){
                        transformerhandler.characters(mLCContPojo.getPrtNo().toCharArray(), 0, mLCContPojo.getPrtNo().length());

                    }
                transformerhandler.endElement("", "", "prtno");

                //创建ProposalNo节点
                transformerhandler.startElement("", "ProposalNo", "ProposalNo", attr);
                    if (mlcPolPojos.get(0).getProposalNo()!=null&&(!("".equals(mlcPolPojos.get(0).getProposalNo())))){
                         transformerhandler.characters(mlcPolPojos.get(0).getProposalNo().toCharArray(), 0, mlcPolPojos.get(0).getProposalNo().length());
                    }

                transformerhandler.endElement("", "", "ProposalNo");

                //创建InsuranceStartPeriod节点
                transformerhandler.startElement("", "InsuranceStartPeriod", "InsuranceStartPeriod", attr);
                    if (mLCContPojo.getCValiDate()!=null&&(!("".equals(mLCContPojo.getCValiDate())))){
                        transformerhandler.characters(mLCContPojo.getCValiDate().toCharArray(), 0, mLCContPojo.getCValiDate().length());

                    }
                transformerhandler.endElement("", "", "InsuranceStartPeriod");

                //创建InsuranceEndPeriod节点
                transformerhandler.startElement("", "InsuranceEndPeriod", "InsuranceEndPeriod", attr);
                    if (mlcPolPojos.get(0).getEndDate()!=null&&(!("".equals(mlcPolPojos.get(0).getEndDate())))){
                        transformerhandler.characters(mlcPolPojos.get(0).getEndDate().toCharArray(), 0, mlcPolPojos.get(0).getEndDate().length());

                    }
                transformerhandler.endElement("", "", "InsuranceEndPeriod");

                //创建SignedDate节点
                transformerhandler.startElement("", "SignedDate", "SignedDate", attr);
                    if (mLCContPojo.getPolApplyDate()!=null&&(!("".equals(mLCContPojo.getPolApplyDate())))){
                        transformerhandler.characters(mLCContPojo.getPolApplyDate().toCharArray(), 0, mLCContPojo.getPolApplyDate().length());

                    }
                transformerhandler.endElement("", "", "SignedDate");

                //新增节点PolApplyDate
                transformerhandler.startElement("", "PolApplyDate", "PolApplyDate", attr);
                if(mLCContPojo.getCValiDate()!=null&&(!("".equals(mLCContPojo.getCValiDate())))){
                    transformerhandler.characters(mLCContPojo.getCValiDate().toCharArray(), 0, mLCContPojo.getCValiDate().length());
                }
                transformerhandler.endElement("", "", "PolApplyDate");


                //创建InsuYear节点
                transformerhandler.startElement("", "InsuYear", "InsuYear", attr);
                    String insuYear = mlcPolPojos.get(0).getInsuYear()+"";
                    if (insuYear!=null&&(!("".equals(insuYear)))){
                        transformerhandler.characters(insuYear.toCharArray(), 0, insuYear.length());

                    }
                transformerhandler.endElement("", "", "InsuYear");

                //InsuYearFlag
                transformerhandler.startElement("", "InsuYearFlag", "InsuYearFlag", attr);
                    if (mlcPolPojos.get(0).getInsuYearFlag()!=null&&(!("".equals(mlcPolPojos.get(0).getInsuYearFlag())))){
                        transformerhandler.characters(mlcPolPojos.get(0).getInsuYearFlag().toCharArray(), 0, mlcPolPojos.get(0).getInsuYearFlag().length());

                    }
                transformerhandler.endElement("", "", "InsuYearFlag");

                //PayIntv
                transformerhandler.startElement("", "PayIntv", "PayIntv", attr);
                    String payIntv = mLCContPojo.getPayIntv()+"";
                    if (payIntv!=null&&(!("".equals(payIntv)))){
                        transformerhandler.characters(payIntv.toCharArray(), 0, payIntv.length());

                    }
                transformerhandler.endElement("", "", "PayIntv");


                //PayEndYear
                transformerhandler.startElement("", "PayEndYear", "PayEndYear", attr);
                    String PayEndYear = mlcPolPojos.get(0).getPayEndYear()+"";
                    if (PayEndYear!=null&&(!("".equals(PayEndYear)))){
                        transformerhandler.characters(PayEndYear.toCharArray(), 0, PayEndYear.length());

                    }
                transformerhandler.endElement("", "", "PayEndYear");

                //PayEndYearFlagY
                transformerhandler.startElement("", "PayEndYearFlag", "PayEndYearFlag", attr);
                    String payEndYearFlag = mlcPolPojos.get(0).getPayEndYearFlag();
                    if (payEndYearFlag!=null&&(!("".equals(payEndYearFlag)))){
                        transformerhandler.characters(payEndYearFlag.toCharArray(), 0, payEndYearFlag.length());

                    }
                transformerhandler.endElement("", "", "PayEndYearFlag");

                //Premium
                /**   Add By Xcc  Double保留两位小数   */
                transformerhandler.startElement("", "Premium", "Premium", attr);
                    String format = df.format(mLCContPojo.getPrem());
                     if (format!=null&&(!("".equals(format)))){
                        transformerhandler.characters(format.toCharArray(), 0, format.length());

                    }
                transformerhandler.endElement("", "", "Premium");

                //InsuredAmount
                transformerhandler.startElement("", "InsuredAmount", "InsuredAmount", attr);
                    String amnt = df.format(mLCContPojo.getAmnt());
                    if (amnt!=null&&(!("".equals(amnt)))){
                        transformerhandler.characters(amnt.toCharArray(), 0, amnt.length());

                    }
                transformerhandler.endElement("", "", "InsuredAmount");


                //GrossPremium
                transformerhandler.startElement("", "GrossPremium", "GrossPremium", attr);
                     String prem1 = df.format(mLCContPojo.getPrem());
                     if (prem1!=null&&(!("".equals(prem1)))){
                         transformerhandler.characters(prem1.toCharArray(), 0, prem1.length());

                     }
                transformerhandler.endElement("", "", "GrossPremium");

                //FaceAmount
                transformerhandler.startElement("", "FaceAmount", "FaceAmount", attr);
                     String amnt1 = df.format(mLCContPojo.getAmnt());
                     if (amnt1!=null&&(!("".equals(amnt1)))){
                         transformerhandler.characters(amnt1.toCharArray(), 0, amnt1.length());

                     }
                transformerhandler.endElement("", "", "FaceAmount");

                //FirstPremium
                transformerhandler.startElement("", "FirstPremium", "FirstPremium", attr);
                    String prem2 = df.format(mLCContPojo.getPrem());
                    if (prem2!=null&&(!("".equals(prem2)))){
                        transformerhandler.characters(prem2.toCharArray(), 0, prem2.length());

                    }
                transformerhandler.endElement("", "", "FirstPremium");


                //InitialPremAmt
                transformerhandler.startElement("", "InitialPremAmt", "InitialPremAmt", attr);
                    String prem3 = df.format(mLCContPojo.getPrem());
                    if (prem3!=null&&(!("".equals(prem3)))){
                        transformerhandler.characters(prem3.toCharArray(), 0, prem3.length());

                    }
                transformerhandler.endElement("", "", "InitialPremAmt");

                //PolicyDeliveryFee
                transformerhandler.startElement("", "PolicyDeliveryFee", "PolicyDeliveryFee", attr);
                transformerhandler.endElement("", "", "PolicyDeliveryFee");

                //UnitCount
                transformerhandler.startElement("", "UnitCount", "UnitCount", attr);
                    String mult = mlcPolPojos.get(0).getMult()+"";
                    if (mult!=null&&(!("".equals(mult)))){
                        transformerhandler.characters(mult.toCharArray(), 0, mult.length());

                    }
                transformerhandler.endElement("", "", "UnitCount");

                //AgentNo
                transformerhandler.startElement("", "AgentNo", "AgentNo", attr);
                    if (mlcPolPojos.get(0).getAgentCode()!=null&&(!("".equals(mlcPolPojos.get(0).getAgentCode())))){
                        transformerhandler.characters(mlcPolPojos.get(0).getAgentCode().toCharArray(), 0, mlcPolPojos.get(0).getAgentCode().length());

                    }
                transformerhandler.endElement("", "", "AgentNo");

                //ExpireProcessMode
                transformerhandler.startElement("", "ExpireProcessMode", "ExpireProcessMode", attr);
                    String rnewFlag = mlcPolPojos.get(0).getRnewFlag()+"";
                    if (rnewFlag!=null&&(!("".equals(rnewFlag)))){
                        transformerhandler.characters(rnewFlag.toCharArray(), 0, rnewFlag.length());

                    }
                transformerhandler.endElement("", "", "ExpireProcessMode");

                //ProductPeriod
                transformerhandler.startElement("", "ProductPeriod", "ProductPeriod", attr);
                transformerhandler.endElement("", "", "ProductPeriod");

                //ProductPeriodFlag
                transformerhandler.startElement("", "ProductPeriodFlag", "ProductPeriodFlag", attr);
                transformerhandler.endElement("", "", "ProductPeriodFlag");

                //ABCode
                transformerhandler.startElement("", "ABCode", "ABCode", attr);
                    if (mLCContPojo.getAgentBankCode()!=null&&(!("".equals(mLCContPojo.getAgentBankCode())))){
                        transformerhandler.characters(mLCContPojo.getAgentBankCode().toCharArray(), 0, mLCContPojo.getAgentBankCode().length());

                    }
                transformerhandler.endElement("", "", "ABCode");

                //Applicant
                transformerhandler.startElement("", "Applicant", "Applicant", attr);
                    //AppNo
                    transformerhandler.startElement("", "AppNo", "AppNo", attr);
                        if (mLCAppntPojo.getAppntNo()!=null&&(!("".equals(mLCAppntPojo.getAppntNo())))){
                            transformerhandler.characters(mLCAppntPojo.getAppntNo().toCharArray(), 0, mLCAppntPojo.getAppntNo().length());

                        }
                    transformerhandler.endElement("", "", "AppNo");

                    //RelationToInsured
                    transformerhandler.startElement("", "RelationToInsured", "RelationToInsured", attr);
                        if (mLCAppntPojo.getRelatToInsu()!=null&&(!("".equals(mLCAppntPojo.getRelatToInsu())))){
                            transformerhandler.characters(mLCAppntPojo.getRelatToInsu().toCharArray(), 0, mLCAppntPojo.getRelatToInsu().length());

                        }
                    transformerhandler.endElement("", "", "RelationToInsured");

                    //AppName
                    transformerhandler.startElement("", "AppName", "AppName", attr);
                        if (mLCAppntPojo.getAppntName()!=null&&(!("".equals(mLCAppntPojo.getAppntName())))){
                            transformerhandler.characters(mLCAppntPojo.getAppntName().toCharArray(), 0, mLCAppntPojo.getAppntName().length());

                        }
                    transformerhandler.endElement("", "", "AppName");

                    //AppSex
                    transformerhandler.startElement("", "AppSex", "AppSex", attr);
                        if (mLCAppntPojo.getAppntSex()!=null&&(!("".equals(mLCAppntPojo.getAppntSex())))){
                            transformerhandler.characters(mLCAppntPojo.getAppntSex().toCharArray(), 0, mLCAppntPojo.getAppntSex().length());

                        }
                    transformerhandler.endElement("", "", "AppSex");

                    //AppBirthday
                    transformerhandler.startElement("", "AppBirthday", "AppBirthday", attr);
                        if (mLCAppntPojo.getAppntBirthday()!=null&&(!("".equals(mLCAppntPojo.getAppntBirthday())))){
                            transformerhandler.characters(mLCAppntPojo.getAppntBirthday().toCharArray(), 0, mLCAppntPojo.getAppntBirthday().length());

                        }
                    transformerhandler.endElement("", "", "AppBirthday");

                    //AppIDType
                    transformerhandler.startElement("", "AppIDType", "AppIDType", attr);
                        if (mLCAppntPojo.getIDType()!=null&&(!("".equals(mLCAppntPojo.getIDType())))){
                            transformerhandler.characters(mLCAppntPojo.getIDType().toCharArray(), 0, mLCAppntPojo.getIDType().length());

                        }
                    transformerhandler.endElement("", "", "AppIDType");

                    //AppIDNo
                    transformerhandler.startElement("", "AppIDNo", "AppIDNo", attr);
                        if (mLCAppntPojo.getIDNo()!=null&&(!("".equals(mLCAppntPojo.getIDNo())))){
                            transformerhandler.characters(mLCAppntPojo.getIDNo().toCharArray(), 0, mLCAppntPojo.getIDNo().length());
                        }
                    transformerhandler.endElement("", "", "AppIDNo");

                    //IdexpDate
                    transformerhandler.startElement("", "IdexpDate", "IdexpDate", attr);
                        if (mLCAppntPojo.getIdValiDate()!=null&&(!("".equals(mLCAppntPojo.getIdValiDate())))){
                            transformerhandler.characters(mLCAppntPojo.getIdValiDate().toCharArray(), 0, mLCAppntPojo.getIdValiDate().length());

                        }
                    transformerhandler.endElement("", "", "IdexpDate");

                    //NativePlace
                    transformerhandler.startElement("", "NativePlace", "NativePlace", attr);
                        if (mLCAppntPojo.getNativePlace()!=null&&(!("".equals(mLCAppntPojo.getNativePlace())))){
                            transformerhandler.characters(mLCAppntPojo.getNativePlace().toCharArray(), 0,mLCAppntPojo.getNativePlace().length());

                        }
                    transformerhandler.endElement("", "", "NativePlace");

                    //Language
                    transformerhandler.startElement("", "Language", "Language", attr);
                    transformerhandler.endElement("", "", "Language");

                    //Marriage
                    transformerhandler.startElement("", "Marriage", "Marriage", attr);
                        if (mLCAppntPojo.getMarriage()!=null&&(!("".equals(mLCAppntPojo.getMarriage())))){
                            transformerhandler.characters(mLCAppntPojo.getMarriage().toCharArray(), 0, mLCAppntPojo.getMarriage().length());

                        }
                    transformerhandler.endElement("", "", "Marriage");

                    //SocialInsuFlag
                    transformerhandler.startElement("", "SocialInsuFlag", "SocialInsuFlag", attr);
                        if (mLCAppntPojo.getSocialInsuFlag()!=null&&(!("".equals(mLCAppntPojo.getSocialInsuFlag())))){
                            transformerhandler.characters(mLCAppntPojo.getSocialInsuFlag().toCharArray(), 0, mLCAppntPojo.getSocialInsuFlag().length());

                        }
                    transformerhandler.endElement("", "", "SocialInsuFlag");

                    //LCAppnt/Stature
                    transformerhandler.startElement("", "Stature", "Stature", attr);
                        String stature = mLCAppntPojo.getStature()+"";
                        if (stature!=null&&(!("".equals(stature)))){
                            transformerhandler.characters(stature.toCharArray(), 0, stature.length());
                        }
                    transformerhandler.endElement("", "", "Stature");

                    //Weight
                    transformerhandler.startElement("", "Weight", "Weight", attr);
                        String  mWeight = mLCAppntPojo.getAvoirdupois()+"";
                        if (mWeight!=null&&(!("".equals(mWeight)))){
                            transformerhandler.characters(mWeight.toCharArray(), 0, mWeight.length());

                        }
                    transformerhandler.endElement("", "", "Weight");

                    //添加是否吸烟标记SmokeFlag
                    transformerhandler.startElement("", "SmokeFlag", "SmokeFlag", attr);
                    String  mSmokeFlag = mLCAppntPojo.getSmokeFlag()+"";
                    if (mSmokeFlag!=null&&(!("".equals(mSmokeFlag)))&&mLCAppntPojo.getSmokeFlag()!=null){
                        transformerhandler.characters(mSmokeFlag.toCharArray(), 0, mSmokeFlag.length());
                    }
                    transformerhandler.endElement("", "", "SmokeFlag");

                    //Salary
                    transformerhandler.startElement("", "Salary", "Salary", attr);
                        String salary = mLCAppntPojo.getSalary()+"";
                        if (salary!=null&&(!("".equals(salary)))){
                            transformerhandler.characters(salary.toCharArray(), 0, salary.length());

                        }
                    transformerhandler.endElement("", "", "Salary");

                    //Health
                    transformerhandler.startElement("", "Health", "Health", attr);
                        if (mLCInsuredPojos.get(0).getHealth()!=null&&(!("".equals(mLCInsuredPojos.get(0).getHealth())))){
                            transformerhandler.characters(mLCInsuredPojos.get(0).getHealth().toCharArray(), 0,mLCInsuredPojos.get(0).getHealth().length());

                        }
                    transformerhandler.endElement("", "", "Health");

                    //Province
                    transformerhandler.startElement("", "Province", "Province", attr);
                        if (tlcAddressPojoList.get(0).getProvince()!=null&&(!("".equals(tlcAddressPojoList.get(0).getProvince())))){
                            transformerhandler.characters(tlcAddressPojoList.get(0).getProvince().toCharArray(), 0, tlcAddressPojoList.get(0).getProvince().length());

                        }
                    transformerhandler.endElement("", "", "Province");

                    //City
                    transformerhandler.startElement("", "City", "City", attr);
                        if (tlcAddressPojoList.get(0).getCity()!=null&&(!("".equals(tlcAddressPojoList.get(0).getCity())))){
                            transformerhandler.characters(tlcAddressPojoList.get(0).getCity().toCharArray(), 0, tlcAddressPojoList.get(0).getCity().length());

                        }
                    transformerhandler.endElement("", "", "City");

                    //Country
                    transformerhandler.startElement("", "Country", "Country", attr);
                        if (tlcAddressPojoList.get(0).getCounty()!=null&&(!("".equals(tlcAddressPojoList.get(0).getCounty())))){
                            transformerhandler.characters(tlcAddressPojoList.get(0).getCounty().toCharArray(), 0, tlcAddressPojoList.get(0).getCounty().length());

                        }
                    transformerhandler.endElement("", "", "Country");

                    //OccupationClass
                    transformerhandler.startElement("", "OccupationClass", "OccupationClass", attr);
                        if (mLCAppntPojo.getOccupationType()!=null&&(!("".equals(mLCAppntPojo.getOccupationType())))){
                            transformerhandler.characters(mLCAppntPojo.getOccupationType().toCharArray(), 0, mLCAppntPojo.getOccupationType().length());

                        }
                    transformerhandler.endElement("", "", "OccupationClass");


                    //OccupationCode
                    transformerhandler.startElement("", "OccupationCode", "OccupationCode", attr);
                        if (mLCAppntPojo.getOccupationCode()!=null&&(!("".equals(mLCAppntPojo.getOccupationCode())))){
                            transformerhandler.characters(mLCAppntPojo.getOccupationCode().toCharArray(), 0,mLCAppntPojo.getOccupationCode().length());

                        }
                    transformerhandler.endElement("", "", "OccupationCode");

                    //AppEmail
                    transformerhandler.startElement("", "AppEmail", "AppEmail", attr);
                        if (tlcAddressPojoList.get(0).getEMail()!=null&&(!("".equals(tlcAddressPojoList.get(0).getEMail())))){
                            transformerhandler.characters(tlcAddressPojoList.get(0).getEMail().toCharArray(), 0, tlcAddressPojoList.get(0).getEMail().length());

                        }
                    transformerhandler.endElement("", "", "AppEmail");

                    //AppTelephone
                    transformerhandler.startElement("", "AppTelephone", "AppTelephone", attr);
                        if (tlcAddressPojoList.get(0).getPhone()!=null&&(!("".equals(tlcAddressPojoList.get(0).getPhone())))){
                            transformerhandler.characters(tlcAddressPojoList.get(0).getPhone().toCharArray(), 0, tlcAddressPojoList.get(0).getPhone().length());

                        }
                    transformerhandler.endElement("", "", "AppTelephone");

                    //AppMobile
                    transformerhandler.startElement("", "AppMobile", "AppMobile", attr);
                        if (tlcAddressPojoList.get(0).getMobile()!=null&&(!("".equals(tlcAddressPojoList.get(0).getMobile())))){
                            transformerhandler.characters(tlcAddressPojoList.get(0).getMobile().toCharArray(), 0, tlcAddressPojoList.get(0).getMobile().length());

                        }
                    transformerhandler.endElement("", "", "AppMobile");

                    //AppZipCode
                    transformerhandler.startElement("", "AppZipCode", "AppZipCode", attr);
                        if (tlcAddressPojoList.get(0).getZipCode()!=null&&(!("".equals(tlcAddressPojoList.get(0).getZipCode())))){
                            transformerhandler.characters(tlcAddressPojoList.get(0).getZipCode().toCharArray(), 0, tlcAddressPojoList.get(0).getZipCode().length());

                        }
                    transformerhandler.endElement("", "", "AppZipCode");

                    //AppAddress
                    transformerhandler.startElement("", "AppAddress", "AppAddress", attr);
                        if (tlcAddressPojoList.get(0).getPostalAddress()!=null&&(!("".equals(tlcAddressPojoList.get(0).getPostalAddress())))){
                            transformerhandler.characters(tlcAddressPojoList.get(0).getPostalAddress().toCharArray(), 0, tlcAddressPojoList.get(0).getPostalAddress().length());

                        }
                    transformerhandler.endElement("", "", "AppAddress");

                transformerhandler.endElement("", "", "Applicant");


                //创建Insureds节点（此处需循环Insured被保人信息）
                //Insureds
                transformerhandler.startElement("", "Insureds", "Insureds", attr);
                        for (LCInsuredPojo mLCInsuredPojo : mLCInsuredPojos) {
                            //Insured
                            transformerhandler.startElement("", "Insured", "Insured", attr);
                                //InsuNo(LCCont/InsuredNo)
                                transformerhandler.startElement("", "InsuNo", "InsuNo", attr);
                                    if (mLCInsuredPojo.getInsuredNo()!=null&&(!("".equals(mLCInsuredPojo.getInsuredNo())))){
                                        transformerhandler.characters(mLCInsuredPojo.getInsuredNo().toCharArray(), 0,mLCInsuredPojo.getInsuredNo().length());

                                    }
                                transformerhandler.endElement("", "", "InsuNo");

                                //InsuRelationToApp
                                transformerhandler.startElement("", "InsuRelationToApp", "InsuRelationToApp", attr);
                                    if (mLCInsuredPojo.getRelationToAppnt()!=null&&(!("".equals(mLCInsuredPojo.getRelationToAppnt())))){
                                        transformerhandler.characters(mLCInsuredPojo.getRelationToAppnt().toCharArray(), 0,mLCInsuredPojo.getRelationToAppnt().length());

                                    }
                                transformerhandler.endElement("", "", "InsuRelationToApp");

                                //InsuRelationToMainInsu
                                transformerhandler.startElement("", "InsuRelationToMainInsu", "InsuRelationToMainInsu", attr);
                                    if (mLCInsuredPojo.getRelationToMainInsured()!=null&&(!("".equals(mLCInsuredPojo.getRelationToMainInsured())))){
                                        transformerhandler.characters(mLCInsuredPojo.getRelationToMainInsured().toCharArray(), 0, mLCInsuredPojo.getRelationToMainInsured().length());

                                    }
                                transformerhandler.endElement("", "", "InsuRelationToMainInsu");

                                //IsMainInsured
                                transformerhandler.startElement("", "IsMainInsured", "IsMainInsured", attr);
                                transformerhandler.endElement("", "", "IsMainInsured");

                                //InsuName
                                transformerhandler.startElement("", "InsuName", "InsuName", attr);
                                    if (mLCInsuredPojo.getName()!=null&&(!("".equals(mLCInsuredPojo.getName())))){
                                        transformerhandler.characters(mLCInsuredPojo.getName().toCharArray(), 0,mLCInsuredPojo.getName().length());

                                    }
                                transformerhandler.endElement("", "", "InsuName");

                                 //InsuSex
                                 transformerhandler.startElement("", "InsuSex", "InsuSex", attr);
                                     if (mLCInsuredPojo.getSex()!=null&&(!("".equals(mLCInsuredPojo.getSex())))){
                                         transformerhandler.characters(mLCInsuredPojo.getSex().toCharArray(), 0, mLCInsuredPojo.getSex().length());
                                     }
                                 transformerhandler.endElement("", "", "InsuSex");

                                 //InsuBirthday
                                 transformerhandler.startElement("", "InsuBirthday", "InsuBirthday", attr);
                                         if (mLCInsuredPojo.getBirthday()!=null&&(!("".equals(mLCInsuredPojo.getBirthday())))){
                                             transformerhandler.characters(mLCInsuredPojo.getBirthday().toCharArray(), 0, mLCInsuredPojo.getBirthday().length());
                                         }
                                 transformerhandler.endElement("", "", "InsuBirthday");

                                 //InsuIDType
                                 transformerhandler.startElement("", "InsuIDType", "InsuIDType", attr);
                                     if (mLCInsuredPojo.getIDType()!=null&&(!("".equals(mLCInsuredPojo.getIDType())))){
                                         transformerhandler.characters(mLCInsuredPojo.getIDType().toCharArray(), 0, mLCInsuredPojo.getIDType().length());
                                     }
                                 transformerhandler.endElement("", "", "InsuIDType");


                                  //InsuIDNo
                                  transformerhandler.startElement("", "InsuIDNo", "InsuIDNo", attr);
                                      if (mLCInsuredPojo.getIDNo()!=null&&(!("".equals(mLCInsuredPojo.getIDNo())))){
                                          transformerhandler.characters(mLCInsuredPojo.getIDNo().toCharArray(), 0, mLCInsuredPojo.getIDNo().length());
                                      }
                                  transformerhandler.endElement("", "", "InsuIDNo");


                                   //IdexpDate
                                   transformerhandler.startElement("", "IdexpDate", "IdexpDate", attr);
                                       if (mLCInsuredPojo.getIdValiDate()!=null&&(!("".equals(mLCInsuredPojo.getIdValiDate())))){
                                           transformerhandler.characters(mLCInsuredPojo.getIdValiDate().toCharArray(), 0, mLCInsuredPojo.getIdValiDate().length());
                                       }
                                   transformerhandler.endElement("", "", "IdexpDate");


                                   //NativePlace
                                   transformerhandler.startElement("", "NativePlace", "NativePlace", attr);
                                       if (mLCInsuredPojo.getNativePlace()!=null&&(!("".equals(mLCInsuredPojo.getNativePlace())))){
                                           transformerhandler.characters(mLCInsuredPojo.getNativePlace().toCharArray(), 0, mLCInsuredPojo.getNativePlace().length());
                                       }
                                   transformerhandler.endElement("", "", "NativePlace");

                                    //Language
                                    transformerhandler.startElement("", "Language", "Language", attr);
                                    transformerhandler.endElement("", "", "Language");

                                    //Stature
                                    transformerhandler.startElement("", "Stature", "Stature", attr);
                                        String mStature = mLCInsuredPojo.getStature()+"";
                                        if (mStature!=null&&(!("".equals(mStature)))){
                                            transformerhandler.characters(mStature.toCharArray(), 0, mStature.length());
                                        }
                                    transformerhandler.endElement("", "", "Stature");
                                        //Weight
                                        transformerhandler.startElement("", "Weight", "Weight", attr);
                                            String Avoirdupois = mLCInsuredPojo.getAvoirdupois()+"";
                                            if (Avoirdupois!=null&&(!("".equals(Avoirdupois)))){
                                                transformerhandler.characters(Avoirdupois.toCharArray(), 0, Avoirdupois.length());
                                            }
                                        transformerhandler.endElement("", "", "Weight");

                                            //添加是否吸烟标记SmokeFlag
                                            transformerhandler.startElement("", "SmokeFlag", "SmokeFlag", attr);
                                            String nSmokeFlag = mLCInsuredPojo.getSmokeFlag()+"";
                                            if (nSmokeFlag!=null&&(!("".equals(nSmokeFlag)))&&mLCInsuredPojo.getSmokeFlag()!=null){
                                                transformerhandler.characters(nSmokeFlag.toCharArray(), 0, nSmokeFlag.length());
                                            }
                                            transformerhandler.endElement("", "", "SmokeFlag");


                                            //Salary
                                            transformerhandler.startElement("", "Salary", "Salary", attr);
                                                    String salary1 = mLCInsuredPojo.getSalary()+"";
                                                    if (salary1!=null&&(!("".equals(salary1)))){
                                                          transformerhandler.characters(salary1.toCharArray(), 0, salary1.length());
                                                    }
                                            transformerhandler.endElement("", "", "Salary");

                                             //Health
                                             transformerhandler.startElement("", "Health", "Health", attr);
                                                 if (mLCInsuredPojo.getHealth()!=null&&(!("".equals(mLCInsuredPojo.getHealth())))){
                                                     transformerhandler.characters(mLCInsuredPojo.getHealth().toCharArray(), 0, mLCInsuredPojo.getHealth().length());
                                                 }
                                             transformerhandler.endElement("", "", "Health");

                                              //Marriage
                                              transformerhandler.startElement("", "Marriage", "Marriage", attr);
                                                  if (mLCInsuredPojo.getMarriage()!=null&&(!("".equals(mLCInsuredPojo.getMarriage())))){
                                                      transformerhandler.characters(mLCInsuredPojo.getMarriage().toCharArray(), 0, mLCInsuredPojo.getMarriage().length());
                                                  }
                                              transformerhandler.endElement("", "", "Marriage");

                                               //SocialInsuFlag
                                               transformerhandler.startElement("", "SocialInsuFlag", "SocialInsuFlag", attr);
                                                   if (mLCInsuredPojo.getSSFlag()!=null&&(!("".equals(mLCInsuredPojo.getSSFlag())))){
                                                       transformerhandler.characters(mLCInsuredPojo.getSSFlag().toCharArray(), 0, mLCInsuredPojo.getSSFlag().length());
                                                   }
                                               transformerhandler.endElement("", "", "SocialInsuFlag");

                                                    //OccupationClass
                                                    transformerhandler.startElement("", "OccupationClass", "OccupationClass", attr);
                                                        if (mLCInsuredPojo.getOccupationType()!=null&&(!("".equals(mLCInsuredPojo.getOccupationType())))){
                                                            transformerhandler.characters(mLCInsuredPojo.getOccupationType().toCharArray(), 0, mLCInsuredPojo.getOccupationType().length());
                                                        }
                                                    transformerhandler.endElement("", "", "OccupationClass");

                                                    //OccupationCode
                                                    transformerhandler.startElement("", "OccupationCode", "OccupationCode", attr);
                                                        if (mLCInsuredPojo.getOccupationCode()!=null&&(!("".equals(mLCInsuredPojo.getOccupationCode())))){
                                                            transformerhandler.characters(mLCInsuredPojo.getOccupationCode().toCharArray(), 0, mLCInsuredPojo.getOccupationCode().length());
                                                        }
                                                    transformerhandler.endElement("", "", "OccupationCode");


                                                    //SameIndustryInsuredAmount
                                                    transformerhandler.startElement("", "SameIndustryInsuredAmount", "SameIndustryInsuredAmount", attr);
                                                    transformerhandler.endElement("", "", "SameIndustryInsuredAmount");
                                                            //PolicyLiabilities
                                                            transformerhandler.startElement("", "PolicyLiabilities", "PolicyLiabilities", attr);
                                                                //PolicyLiability(此处遍历lcpol)
                                                                for (LCPolPojo mLCPolPojo : mlcPolPojos) {
                                                                    //取出Lcpol的险种号
                                                                    String riskCode = mLCPolPojo.getRiskCode();
                                                                    //取出polNO
                                                                    String mLCPolPojoPolno = mLCPolPojo.getPolNo();

                                                                    transformerhandler.startElement("", "PolicyLiability", "PolicyLiability", attr);
                                                                        //RiskCode
                                                                        transformerhandler.startElement("", "RiskCode", "RiskCode", attr);
                                                                                if (mLCPolPojo.getRiskCode()!=null&&(!("".equals(mLCPolPojo.getRiskCode())))){
                                                                                    transformerhandler.characters(mLCPolPojo.getRiskCode().toCharArray(), 0, mLCPolPojo.getRiskCode().length());
                                                                                }
                                                                        transformerhandler.endElement("", "", "RiskCode");

                                                                        //RiskName
                                                                        transformerhandler.startElement("", "RiskName", "RiskName", attr);
                                                                        transformerhandler.endElement("", "", "RiskName");

                                                                        //RiskPremium
                                                                        transformerhandler.startElement("", "RiskPremium", "RiskPremium", attr);
                                                                            String Prem1 = df.format(mLCPolPojo.getPrem());
                                                                            if (Prem1!=null&&(!("".equals(Prem1)))){
                                                                                transformerhandler.characters(Prem1.toCharArray(), 0, Prem1.length());
                                                                            }
                                                                        transformerhandler.endElement("", "", "RiskPremium");

                                                                        //RiskAmount
                                                                        transformerhandler.startElement("", "RiskAmount", "RiskAmount", attr);
                                                                            String Amnt = df.format(mLCPolPojo.getAmnt());
                                                                            if (Amnt!=null&&(!("".equals(Amnt)))){
                                                                                transformerhandler.characters(Amnt.toCharArray(), 0, Amnt.length());
                                                                            }
                                                                        transformerhandler.endElement("", "", "RiskAmount");

                                                                         //MainRiskFlag
                                                                         transformerhandler.startElement("", "MainRiskFlag", "MainRiskFlag", attr);
                                                                         //暂不添加数据
                                                                         transformerhandler.endElement("", "", "MainRiskFlag");

                                                                          //InsuranceStartPeriod
                                                                          transformerhandler.startElement("", "InsuranceStartPeriod", "InsuranceStartPeriod", attr);
                                                                              if (mLCPolPojo.getCValiDate()!=null&&(!("".equals(mLCPolPojo.getCValiDate())))){
                                                                                  transformerhandler.characters(mLCPolPojo.getCValiDate().toCharArray(), 0, mLCPolPojo.getCValiDate().length());
                                                                              }
                                                                          transformerhandler.endElement("", "", "InsuranceStartPeriod");

                                                                           //InsuranceEndPeriod
                                                                           transformerhandler.startElement("", "InsuranceEndPeriod", "InsuranceEndPeriod", attr);
                                                                               if (mLCPolPojo.getEndDate()!=null&&(!("".equals(mLCPolPojo.getEndDate())))){
                                                                                   transformerhandler.characters(mLCPolPojo.getEndDate().toCharArray(), 0, mLCPolPojo.getEndDate().length());
                                                                               }
                                                                           transformerhandler.endElement("", "", "InsuranceEndPeriod");

                                                                           //PayEndYearFlag
                                                                           transformerhandler.startElement("", "PayEndYearFlag", "PayEndYearFlag", attr);
                                                                               if (mLCPolPojo.getPayEndYearFlag()!=null&&(!("".equals(mLCPolPojo.getPayEndYearFlag())))){
                                                                                   transformerhandler.characters(mLCPolPojo.getPayEndYearFlag().toCharArray(), 0, mLCPolPojo.getPayEndYearFlag().length());
                                                                               }
                                                                           transformerhandler.endElement("", "", "PayEndYearFlag");

                                                                           //InsuYear
                                                                           transformerhandler.startElement("", "InsuYear", "InsuYear", attr);
                                                                                String mInsuYear = mLCPolPojo.getInsuYear()+"";
                                                                               if (mInsuYear!=null&&(!("".equals(mInsuYear)))){
                                                                                   transformerhandler.characters(mInsuYear.toCharArray(), 0, mInsuYear.length());
                                                                               }
                                                                           transformerhandler.endElement("", "", "InsuYear");

                                                                           //创建TransCode节点
                                                                           transformerhandler.startElement("", "InsuYearFlag", "InsuYearFlag", attr);
                                                                               if (mLCPolPojo.getInsuYearFlag()!=null&&(!("".equals(mLCPolPojo.getInsuYearFlag())))){
                                                                                   transformerhandler.characters(mLCPolPojo.getInsuYearFlag().toCharArray(), 0, mLCPolPojo.getInsuYearFlag().length());
                                                                               }
                                                                           transformerhandler.endElement("", "", "InsuYearFlag");

                                                                            //PayIntv
                                                                            transformerhandler.startElement("", "PayIntv", "PayIntv", attr);
                                                                                String PayIntv = mLCPolPojo.getPayIntv()+"";
                                                                                if (PayIntv!=null&&(!("".equals(PayIntv)))){
                                                                                    transformerhandler.characters(PayIntv.toCharArray(), 0, PayIntv.length());
                                                                                }
                                                                            transformerhandler.endElement("", "", "PayIntv");

                                                                             //PayEndYear
                                                                             transformerhandler.startElement("", "PayEndYear", "PayEndYear", attr);
                                                                                 String  payEndYear = mLCPolPojo.getPayEndYear()+"";
                                                                                    if (payEndYear!=null&&(!("".equals(payEndYear)))){
                                                                                     transformerhandler.characters(payEndYear.toCharArray(), 0, payEndYear.length());
                                                                                     }
                                                                             transformerhandler.endElement("", "", "PayEndYear");

                                                                             //Deductibles
                                                                             transformerhandler.startElement("", "Deductibles", "Deductibles", attr);
                                                                             transformerhandler.endElement("", "", "Deductibles");

                                                                             //Lossratio
                                                                             transformerhandler.startElement("", "Lossratio", "Lossratio", attr);
                                                                             transformerhandler.endElement("", "", "Lossratio");


                                                                             //DutiesInfo  此处循环lcduty
                                                                             transformerhandler.startElement("", "DutiesInfo", "DutiesInfo", attr);
                                                                                    for (LCDutyPojo mLcDutyPojo : mlcDutyPojos) {

                                                                                       // 取出lcduty中的polno
                                                                                        String mLcDutyPojoPolNo = mLcDutyPojo.getPolNo();

                                                                                        if (mLCPolPojoPolno!=null&&(!("".equals(mLCPolPojoPolno)))&&(mLcDutyPojoPolNo!=null&&!("".equals(mLcDutyPojoPolNo)))){
                                                                                                if(mLCPolPojoPolno.equals(mLcDutyPojoPolNo)){

                                                                                                    //创建TransCode节点
                                                                                                    transformerhandler.startElement("", "DutyInfo", "DutyInfo", attr);

                                                                                                    //DutyCode
                                                                                                    transformerhandler.startElement("", "DutyCode", "DutyCode", attr);
                                                                                                    if(mLcDutyPojo.getDutyCode()!=null&&(!("".equals(mLcDutyPojo.getDutyCode())))){
                                                                                                        transformerhandler.characters(mLcDutyPojo.getDutyCode().toCharArray(), 0, mLcDutyPojo.getDutyCode().length());
                                                                                                    }
                                                                                                    transformerhandler.endElement("", "", "DutyCode");

                                                                                                    //CalMode
                                                                                                    transformerhandler.startElement("", "CalMode", "CalMode", attr);
                                                                                                    if(mLcDutyPojo.getCalRule()!=null&&(!("".equals(mLcDutyPojo.getCalRule())))){
                                                                                                        transformerhandler.characters(mLcDutyPojo.getCalRule().toCharArray(), 0, mLcDutyPojo.getCalRule().length());
                                                                                                    }
                                                                                                    transformerhandler.endElement("", "", "CalMode");

                                                                                                    //DutyPrem
                                                                                                    transformerhandler.startElement("", "DutyPrem", "DutyPrem", attr);
                                                                                                    String  Prem =  df.format(mLcDutyPojo.getPrem());
                                                                                                    if(Prem!=null&&(!("".equals(Prem)))){
                                                                                                        transformerhandler.characters(Prem.toCharArray(), 0, Prem.length());
                                                                                                    }
                                                                                                    transformerhandler.endElement("", "", "DutyPrem");

                                                                                                    //DutyAmnt
                                                                                                    transformerhandler.startElement("", "DutyAmnt", "DutyAmnt", attr);
                                                                                                    String Amnt2 = df.format(mLcDutyPojo.getAmnt());
                                                                                                    if(Amnt2!=null&&(!("".equals(Amnt2)))){
                                                                                                        transformerhandler.characters(Amnt2.toCharArray(), 0, Amnt2.length());
                                                                                                    }
                                                                                                    transformerhandler.endElement("", "", "DutyAmnt");

                                                                                                    //InsuYearFlag
                                                                                                    transformerhandler.startElement("", "InsuYearFlag", "InsuYearFlag", attr);
                                                                                                    if(mLcDutyPojo.getInsuYearFlag()!=null&&(!("".equals(mLcDutyPojo.getInsuYearFlag())))){
                                                                                                        transformerhandler.characters(mLcDutyPojo.getInsuYearFlag().toCharArray(), 0, mLcDutyPojo.getInsuYearFlag().length());
                                                                                                    }
                                                                                                    transformerhandler.endElement("", "", "InsuYearFlag");

                                                                                                    //InsuYear
                                                                                                    transformerhandler.startElement("", "InsuYear", "InsuYear", attr);
                                                                                                    String insuYear1 = mLcDutyPojo.getInsuYear()+"";
                                                                                                    if(insuYear1!=null&&(!("".equals(insuYear1)))){
                                                                                                        transformerhandler.characters(insuYear1.toCharArray(), 0, insuYear1.length());
                                                                                                    }
                                                                                                    transformerhandler.endElement("", "", "InsuYear");
                                                                                                    //PayEndYearFlag
                                                                                                    transformerhandler.startElement("", "PayEndYearFlag", "PayEndYearFlag", attr);

                                                                                                    if(mLcDutyPojo.getPayEndYearFlag()!=null&&(!("".equals(mLcDutyPojo.getPayEndYearFlag())))){
                                                                                                        transformerhandler.characters(mLcDutyPojo.getPayEndYearFlag().toCharArray(), 0, mLcDutyPojo.getPayEndYearFlag().length());
                                                                                                    }
                                                                                                    transformerhandler.endElement("", "", "PayEndYearFlag");

                                                                                                    //PayEndYear
                                                                                                    transformerhandler.startElement("", "PayEndYear", "PayEndYear", attr);
                                                                                                    String payEndYear1 = mLcDutyPojo.getPayEndYear()+"";
                                                                                                    if(payEndYear1!=null&&(!("".equals(payEndYear1)))){
                                                                                                        transformerhandler.characters(payEndYear1.toCharArray(), 0, payEndYear1.length());
                                                                                                    }
                                                                                                    transformerhandler.endElement("", "", "PayEndYear");

                                                                                                    //PayIntv
                                                                                                    transformerhandler.startElement("", "PayIntv", "PayIntv", attr);
                                                                                                    String PayInTV = mLcDutyPojo.getPayIntv()+"";
                                                                                                    if(PayInTV!=null&&(!("".equals(PayInTV)))){
                                                                                                        transformerhandler.characters(PayInTV.toCharArray(), 0, PayInTV.length());
                                                                                                    }
                                                                                                    transformerhandler.endElement("", "", "PayIntv");

                                                                                                    transformerhandler.endElement("", "", "DutyInfo");

                                                                                                }
                                                                                        }
                                                                                    }

                                                                             transformerhandler.endElement("", "", "DutiesInfo");

                                                                    transformerhandler.endElement("", "", "PolicyLiability");
                                                                }
                                                            transformerhandler.endElement("", "", "PolicyLiabilities");

                            transformerhandler.endElement("", "", "Insured");
                        }
                transformerhandler.endElement("", "", "Insureds");

                        //Beneficiaries
                transformerhandler.startElement("", "Beneficiaries", "Beneficiaries", attr);
                        //循环遍历Beneficiary
                            for (LCBnfPojo mLCBnfPojo : mlcBnfPojos) {

                                if ("法定".equals(mLCBnfPojo.getName())){
                                      continue;
                                }
                                //Beneficiary
                                transformerhandler.startElement("", "Beneficiary", "Beneficiary", attr);
                                   //BeneficiaryNo
                                   transformerhandler.startElement("", "BeneficiaryNo", "BeneficiaryNo", attr);
                                       String bnfNo = mLCBnfPojo.getBnfNo()+"";
                                       if (bnfNo!=null&&(!("".equals(bnfNo)))){
                                              transformerhandler.characters(bnfNo.toCharArray(), 0, bnfNo.length());
                                       }
                                   transformerhandler.endElement("", "", "BeneficiaryNo");

                                    //BeneficiaryGrade
                                    transformerhandler.startElement("", "BeneficiaryGrade", "BeneficiaryGrade", attr);
                                        if (mLCBnfPojo.getBnfGrade()!=null&&(!("".equals(mLCBnfPojo.getBnfGrade())))){
                                            transformerhandler.characters(mLCBnfPojo.getBnfGrade().toCharArray(), 0, mLCBnfPojo.getBnfGrade().length());
                                        }
                                    transformerhandler.endElement("", "", "BeneficiaryGrade");

                                     //BeneficiaryOrder
                                     transformerhandler.startElement("", "BeneficiaryOrder", "BeneficiaryOrder", attr);
                                     transformerhandler.endElement("", "", "BeneficiaryOrder");

                                     //BeneficiaryType
                                     transformerhandler.startElement("", "BeneficiaryType", "BeneficiaryType", attr);
                                         if (mLCBnfPojo.getBnfType()!=null&&(!("".equals(mLCBnfPojo.getBnfType())))){
                                             transformerhandler.characters(mLCBnfPojo.getBnfType().toCharArray(), 0, mLCBnfPojo.getBnfType().length());
                                         }
                                     transformerhandler.endElement("", "", "BeneficiaryType");

                                      //InterestPercent
                                      transformerhandler.startElement("", "InterestPercent", "InterestPercent", attr);
                                            String BnfLot = mLCBnfPojo.getBnfLot()+"" ;
                                          if (BnfLot!=null&&(!("".equals(BnfLot)))){
                                              transformerhandler.characters(BnfLot.toCharArray(), 0, BnfLot.length());
                                          }
                                      transformerhandler.endElement("", "", "InterestPercent");

                                      //BeneficiaryName
                                      transformerhandler.startElement("", "BeneficiaryName", "BeneficiaryName", attr);
                                          if (mLCBnfPojo.getName()!=null&&(!("".equals(mLCBnfPojo.getName())))){
                                              transformerhandler.characters(mLCBnfPojo.getName().toCharArray(), 0, mLCBnfPojo.getName().length());
                                          }
                                      transformerhandler.endElement("", "", "BeneficiaryName");

                                      //BeneficiarySex
                                      transformerhandler.startElement("", "BeneficiarySex", "BeneficiarySex", attr);
                                          if (mLCBnfPojo.getSex()!=null&&(!("".equals(mLCBnfPojo.getSex())))){
                                              transformerhandler.characters(mLCBnfPojo.getSex().toCharArray(), 0, mLCBnfPojo.getSex().length());
                                          }
                                      transformerhandler.endElement("", "", "BeneficiarySex");

                                       //BeneficiaryBirthday
                                       transformerhandler.startElement("", "BeneficiaryBirthday", "BeneficiaryBirthday", attr);
                                           if (mLCBnfPojo.getBirthday()!=null&&(!("".equals(mLCBnfPojo.getBirthday())))){
                                               transformerhandler.characters(mLCBnfPojo.getBirthday().toCharArray(), 0, mLCBnfPojo.getBirthday().length());
                                           }
                                       transformerhandler.endElement("", "", "BeneficiaryBirthday");

                                        //BeneficiaryIDType
                                        transformerhandler.startElement("", "BeneficiaryIDType", "BeneficiaryIDType", attr);
                                            if (mLCBnfPojo.getIDType()!=null&&(!("".equals(mLCBnfPojo.getIDType())))){
                                                transformerhandler.characters(mLCBnfPojo.getIDType().toCharArray(), 0, mLCBnfPojo.getIDType().length());
                                            }
                                        transformerhandler.endElement("", "", "BeneficiaryIDType");

                                         //BeneficiaryIDNo
                                         transformerhandler.startElement("", "BeneficiaryIDNo", "BeneficiaryIDNo", attr);
                                             if (mLCBnfPojo.getIDNo()!=null&&(!("".equals(mLCBnfPojo.getIDNo())))){
                                                 transformerhandler.characters(mLCBnfPojo.getIDNo().toCharArray(), 0, mLCBnfPojo.getIDNo().length());
                                             }
                                         transformerhandler.endElement("", "", "BeneficiaryIDNo");

                                          //IDExpDate
                                          transformerhandler.startElement("", "IDExpDate", "IDExpDate", attr);
                                              if (mLCBnfPojo.getIdValiDate()!=null&&(!("".equals(mLCBnfPojo.getIdValiDate())))){
                                                  transformerhandler.characters(mLCBnfPojo.getIdValiDate().toCharArray(), 0, mLCBnfPojo.getIdValiDate().length());
                                              }
                                          transformerhandler.endElement("", "", "IDExpDate");



                                          //RelationToInsured
                                          transformerhandler.startElement("", "RelationToInsured", "RelationToInsured", attr);
                                              if (mLCBnfPojo.getRelationToInsured()!=null&&(!("".equals(mLCBnfPojo.getRelationToInsured())))){
                                                  transformerhandler.characters(mLCBnfPojo.getRelationToInsured().toCharArray(), 0, mLCBnfPojo.getRelationToInsured().length());
                                              }
                                          transformerhandler.endElement("", "", "RelationToInsured");

                                           //BeneficiaryEmail
                                           transformerhandler.startElement("", "BeneficiaryEmail", "BeneficiaryEmail", attr);
                                           transformerhandler.endElement("", "", "BeneficiaryEmail");

                                           //BeneficiaryTelephone
                                           transformerhandler.startElement("", "BeneficiaryTelephone", "BeneficiaryTelephone", attr);
                                           transformerhandler.endElement("", "", "BeneficiaryTelephone");

                                           //BeneficiaryMobile
                                           transformerhandler.startElement("", "BeneficiaryMobile", "BeneficiaryMobile", attr);
                                               if(mLCBnfPojo.getTel()!=null&&(!("".equals(mLCBnfPojo.getTel())))){
                                                   transformerhandler.characters(mLCBnfPojo.getTel().toCharArray(), 0, mLCBnfPojo.getTel().length());
                                               }
                                           transformerhandler.endElement("", "", "BeneficiaryMobile");

                                           //BeneficiaryZipCode
                                           transformerhandler.startElement("", "BeneficiaryZipCode", "BeneficiaryZipCode", attr);
                                               if(mLCBnfPojo.getZipCode()!=null&&(!("".equals(mLCBnfPojo.getZipCode())))){
                                                   transformerhandler.characters(mLCBnfPojo.getZipCode().toCharArray(), 0, mLCBnfPojo.getZipCode().length());
                                               }
                                           transformerhandler.endElement("", "", "BeneficiaryZipCode");


                                            //BeneficiaryAddress
                                            transformerhandler.startElement("", "BeneficiaryAddress", "BeneficiaryAddress", attr);
                                            if(mLCBnfPojo.getAddress()!=null&&(!("".equals(mLCBnfPojo.getAddress())))){
                                                transformerhandler.characters(mLCBnfPojo.getAddress().toCharArray(), 0, mLCBnfPojo.getAddress().length());
                                            }
                                            transformerhandler.endElement("", "", "BeneficiaryAddress");
                                transformerhandler.endElement("", "", "Beneficiary");

                            }
                transformerhandler.endElement("", "", "Beneficiaries");

                 //RenewalPaymentInfo
                 transformerhandler.startElement("", "RenewalPaymentInfo", "RenewalPaymentInfo", attr);
                         //PayModeJ/PayMode
                         transformerhandler.startElement("", "PayMode", "PayMode", attr);
                         if(mLCContPojo.getPayMode()!=null&&(!("".equals(mLCContPojo.getPayMode())))){
                             transformerhandler.characters(mLCContPojo.getPayMode().toCharArray(), 0, mLCContPojo.getPayMode().length());
                         }
                         transformerhandler.endElement("", "", "PayMode");


                         //PayBankCode
                         transformerhandler.startElement("", "PayBankCode", "PayBankCode", attr);
                         if(mLCContPojo.getBankCode()!=null&&(!("".equals(mLCContPojo.getBankCode())))){
                             transformerhandler.characters(mLCContPojo.getBankCode().toCharArray(), 0, mLCContPojo.getBankCode().length());
                         }
                         transformerhandler.endElement("", "", "PayBankCode");


                         //PayBankName
                         transformerhandler.startElement("", "PayBankName", "PayBankName", attr);
                         transformerhandler.endElement("", "", "PayBankName");

                         //BankProvinceCode
                         transformerhandler.startElement("", "BankProvinceCode", "BankProvinceCode", attr);
                         transformerhandler.endElement("", "", "BankProvinceCode");

                         //BankCityCode
                         transformerhandler.startElement("", "BankCityCode", "BankCityCode", attr);
                         if(mLCContPojo.getBankCity()!=null&&(!("".equals(mLCContPojo.getBankCity())))){
                             transformerhandler.characters(mLCContPojo.getBankCity().toCharArray(), 0, mLCContPojo.getBankCity().length());
                         }
                         transformerhandler.endElement("", "", "BankCityCode");


                         //PayAcountNo
                         transformerhandler.startElement("", "PayAcountNo", "PayAcountNo", attr);
                         if(mLCContPojo.getBankAccNo()!=null&&(!("".equals(mLCContPojo.getBankAccNo())))){
                             transformerhandler.characters(mLCContPojo.getBankAccNo().toCharArray(), 0, mLCContPojo.getBankAccNo().length());
                         }
                         transformerhandler.endElement("", "", "PayAcountNo");

                         //CardType
                         transformerhandler.startElement("", "CardType", "CardType", attr);
                         if(mLCContPojo.getAccType()!=null&&(!("".equals(mLCContPojo.getAccType())))){
                             transformerhandler.characters(mLCContPojo.getAccType().toCharArray(), 0, mLCContPojo.getAccType().length());
                         }
                         transformerhandler.endElement("", "", "CardType");


                         //创建TransCode节点
                         transformerhandler.startElement("", "PayVirtualAcountNo", "PayVirtualAcountNo", attr);
                         transformerhandler.endElement("", "", "PayVirtualAcountNo");

                         //PayAccountName
                         transformerhandler.startElement("", "PayAccountName", "PayAccountName", attr);
                             if(mLCContPojo.getAccName()!=null&&(!("".equals(mLCContPojo.getAccName())))){
                                 transformerhandler.characters(mLCContPojo.getAccName().toCharArray(), 0, mLCContPojo.getAccName().length());
                             }
                         transformerhandler.endElement("", "", "PayAccountName");

                         //PayAccountId
                         transformerhandler.startElement("", "PayAccountId", "PayAccountId", attr);
                         transformerhandler.endElement("", "", "PayAccountId");

                         transformerhandler.endElement("", "", "RenewalPaymentInfo");



                         //SaleChannel
                         transformerhandler.startElement("", "SaleChannel", "SaleChannel", attr);
                             //IntermediaryCode
                             transformerhandler.startElement("", "IntermediaryCode", "IntermediaryCode", attr);
                                String  mIntermediaryCode = "05";
                                 transformerhandler.characters(mIntermediaryCode.toCharArray(), 0, mIntermediaryCode.length());
                             transformerhandler.endElement("", "", "IntermediaryCode");

                             //IntermediaryName
                             transformerhandler.startElement("", "IntermediaryName", "IntermediaryName", attr);
                             transformerhandler.endElement("", "", "IntermediaryName");
                         transformerhandler.endElement("", "", "SaleChannel");

                         //checkPayNo
                         transformerhandler.startElement("", "checkPayNo", "checkPayNo", attr);
                         transformerhandler.endElement("", "", "checkPayNo");

                         //payTime
                         transformerhandler.startElement("", "payTime", "payTime", attr);
                                String transDate = mglobalPojo.getTransDate();
                                String transTime = mglobalPojo.getTransTime();
                                if(transDate!=null&&(!"".equals(transDate))&&transTime!=null&&(!"".equals(transTime))){
                                String PayTime2 = transDate+" "+transTime;
                                transformerhandler.characters(PayTime2.toCharArray(), 0, PayTime2.length());
                         }
                         transformerhandler.endElement("", "", "payTime");

                         //payAmount
                         transformerhandler.startElement("", "payAmount", "payAmount", attr);
                         String payMoney = df.format(mLCContPojo.getPrem());
                            if(payMoney !=null&&(!("".equals(payMoney)))){
                             transformerhandler.characters(payMoney.toCharArray(), 0, payMoney.length());
                         }
                         transformerhandler.endElement("", "", "payAmount");


                        //PayModeJ/PayMode
                        transformerhandler.startElement("", "payMode", "payMode", attr);
                        if(mLCContPojo.getPayMode()!=null&&(!("".equals(mLCContPojo.getPayMode())))){
                            transformerhandler.characters(mLCContPojo.getPayMode().toCharArray(), 0, mLCContPojo.getPayMode().length());
                        }
                        transformerhandler.endElement("", "", "payMode");


                         //payBankCode
                         transformerhandler.startElement("", "payBankCode", "payBankCode", attr);
                         if(mLCContPojo.getNewBankCode()!=null&&(!("".equals(mLCContPojo.getNewBankCode())))){
                             transformerhandler.characters(mLCContPojo.getNewBankCode().toCharArray(), 0, mLCContPojo.getNewBankCode().length());
                         }
                         transformerhandler.endElement("", "", "payBankCode");


                         //payBankName
                         transformerhandler.startElement("", "payBankName", "payBankName", attr);
                         transformerhandler.endElement("", "", "payBankName");

                         //bankProvinceCode
                         transformerhandler.startElement("", "bankProvinceCode", "bankProvinceCode", attr);
                         if(mLCContPojo.getNewBankProivnce()!=null&&(!("".equals(mLCContPojo.getNewBankProivnce())))){
                             transformerhandler.characters(mLCContPojo.getNewBankProivnce().toCharArray(), 0, mLCContPojo.getNewBankProivnce().length());
                         }
                         transformerhandler.endElement("", "", "bankProvinceCode");

                         //bankCityCode
                         transformerhandler.startElement("", "bankCityCode", "bankCityCode", attr);
                         if(mLCContPojo.getNewBankCity()!=null&&(!("".equals(mLCContPojo.getNewBankCity())))){
                             transformerhandler.characters(mLCContPojo.getNewBankCity().toCharArray(), 0, mLCContPojo.getNewBankCity().length());
                         }
                         transformerhandler.endElement("", "", "bankCityCode");

                         //创建TransCode节点
                         transformerhandler.startElement("", "bankLocationCode", "bankLocationCode", attr);
                         transformerhandler.endElement("", "", "bankLocationCode");

                         //payAccountName
                         transformerhandler.startElement("", "payAccountName", "payAccountName", attr);
                         if(mLCContPojo.getNewAccName()!=null&&(!("".equals(mLCContPojo.getNewAccName())))){
                             transformerhandler.characters(mLCContPojo.getNewAccName().toCharArray(), 0, mLCContPojo.getNewAccName().length());
                         }
                         transformerhandler.endElement("", "", "payAccountName");

                         //payAcountNo
                         transformerhandler.startElement("", "payAcountNo", "payAcountNo", attr);
                         if(mLCContPojo.getNewBankAccNo()!=null&&(!("".equals(mLCContPojo.getNewBankAccNo())))){
                             transformerhandler.characters(mLCContPojo.getNewBankAccNo().toCharArray(), 0, mLCContPojo.getNewBankAccNo().length());
                         }
                         transformerhandler.endElement("", "", "payAcountNo");

                         //cardType
                         transformerhandler.startElement("", "cardType", "cardType", attr);
                         if(mLCContPojo.getNewAccType()!=null&&(!("".equals(mLCContPojo.getNewAccType())))){
                             transformerhandler.characters(mLCContPojo.getNewAccType().toCharArray(), 0, mLCContPojo.getNewAccType().length());
                         }
                         transformerhandler.endElement("", "", "cardType");

                         //payVirtualAcountNo
                         transformerhandler.startElement("", "payVirtualAcountNo", "payVirtualAcountNo", attr);
                         transformerhandler.endElement("", "", "payVirtualAcountNo");

                         //payAccountId
                         transformerhandler.startElement("", "payAccountId", "payAccountId", attr);
                         transformerhandler.endElement("", "", "payAccountId");

            transformerhandler.endElement("", "", "RequestNode");
        transformerhandler.endElement("", "", "RequestNodes");


        transformerhandler.endElement("", "", "Package");
        transformerhandler.endDocument();
        xmlStr = writerStr.getBuffer().toString();
        System.out.println(" 报文——数据同步——转化pojo所得交互xml: " +xmlStr);
        return xmlStr;
    }

    //生成当前系统时间
    public String selectNowTime(){
        SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = new Date();
        String NewTime = sdf.format(date);
        return NewTime;
    }


    /**   Add By Xcc 获取省名    */
    public String findProvince(String shengStr){
        String sql ="select DISTINCT ld.PROVINCENAME from  ldplace ld where province = '"+shengStr+"'";
        ExeSQL exeSQL = new ExeSQL();
        String result = exeSQL.getOneValue(sql);
        return result;
    }


    /**   Add By Xcc  获取市名   */
    public String findCity(String city){
        String sql ="select DISTINCT ld.CITYNAME from  ldplace ld where city = '"+city+"'";
        ExeSQL exeSQL = new ExeSQL();
        String result = exeSQL.getOneValue(sql);
        return result;
    }

    /**   Add By Xcc  获取区名   */
    public String findCcountry(String country){
        String sql ="select DISTINCT ld.COUNTRYNAME from  ldplace ld where COUNTRY  = '"+country+"'";
        ExeSQL exeSQL = new ExeSQL();
        String result = exeSQL.getOneValue(sql);
        return result;
    }



}
