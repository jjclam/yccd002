<?xml version="1.0" encoding="GBK"?>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:java="http://xml.apache.org/xslt/java" exclude-result-prefixes="java">
    <xsl:template match="/TranData">
        <ABCB2I>
            <Header>
                <xsl:if test="Head/Flag=0">
                    <RetCode>000000</RetCode>
                    <RetMsg>���׳ɹ�</RetMsg>
                </xsl:if>
                <xsl:if test="Head/Flag!=0">
                    <RetCode>009999</RetCode>
                    <RetMsg><xsl:value-of select="Head/Desc"/></RetMsg>
                </xsl:if>
                <SerialNo></SerialNo>
                <InsuSerial></InsuSerial>
                <TransDate></TransDate>
                <BankCode>01</BankCode>
                <CorpNo></CorpNo>
                <TransCode></TransCode>
            </Header>
            <App>
                <Ret>
                    <InsuTime><xsl:value-of select="Body/InsuTime"/></InsuTime>
                    <Reserve><xsl:value-of select="Body/Reserve"/></Reserve>
                </Ret>
            </App>
        </ABCB2I>
    </xsl:template>
</xsl:stylesheet>