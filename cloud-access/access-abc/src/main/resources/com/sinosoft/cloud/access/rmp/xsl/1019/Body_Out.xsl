<?xml version="1.0" encoding="GBK"?>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:java="http://xml.apache.org/xslt/java" exclude-result-prefixes="java">
    <xsl:template match="/TranData">
        <xsl:variable name="hasCashValue" select="count(Body/CashValues/CashValue/cashValue/string)>0"/>
        <xsl:variable name="LMRiskApp" select="Body/LMRiskApps/LMRiskApp"/>
        <xsl:variable name="LCPol" select="Body/LCPols/LCPol"/>
        <xsl:variable name="CashValue" select="Body/CashValues/CashValue"/>
        <ABCB2I>
            <Header>
                <xsl:if test="Head/Flag=0">
                    <RetCode>000000</RetCode>
                    <RetMsg>交易成功</RetMsg>
                </xsl:if>
                <xsl:if test="Head/Flag=1">
                    <RetCode>009999</RetCode>
                    <RetMsg>
                        <xsl:value-of select="Head/Desc"/>
                    </RetMsg>
                </xsl:if>
                <xsl:if test="Head/Flag=2">
                    <RetCode>009990</RetCode>
                    <RetMsg>
                        <xsl:value-of select="Head/Desc"/>
                    </RetMsg>
                </xsl:if>
                <SerialNo>
                    <xsl:value-of select="Head/TranNo"/>
                </SerialNo>
                <InsuSerial>
                    <xsl:value-of select="Head/InsuSerial"/>
                </InsuSerial>
                <TransDate>
                    <xsl:value-of
                            select="java:com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.date10to8(Head/TranDate)"/>
                </TransDate>
                <TransTime>
                    <xsl:value-of
                            select="java:com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.timeString10To8(Head/TranTime)"/>
                </TransTime>
                <BankCode>
                    <xsl:value-of select="Head/BankCode"/>
                </BankCode>
                <CorpNo>
                    <xsl:value-of select="Head/CorpNo"/>
                </CorpNo>
                <TransCode>
                    <xsl:value-of select="Head/TransCode"/>
                </TransCode>
            </Header>

            <xsl:if test="Head/Flag='0'">
                <App>
                    <Ret>
                        <Amt>
                            <xsl:value-of select="Body/ResponseObj/Amt"/>
                        </Amt>
                        <FeeAmt>
                            <xsl:value-of select="Body/ResponseObj/FeeAmt"/>
                        </FeeAmt>
                        <xsl:if test="not($hasCashValue)">
                            <PrntCount>0</PrntCount>
                        </xsl:if>
                        <xsl:if test="$hasCashValue">
                            <PrntCount>
                                <xsl:value-of select="1+2*count($LCPol)+ceiling(count($CashValue/cashValue/string) div 3)"/>
                            </PrntCount>
                            <Prnt>　　　　　　　　　　　　　　　　　　　　　　 现金价值表</Prnt>
                            <xsl:for-each select="$LCPol">
                                <xsl:variable name="riskCode" select="RiskCode"/>
                                <xsl:if test="$riskCode !='5022'">
                                    <Prnt>险种：<xsl:value-of select="$LMRiskApp[RiskCode=$riskCode]/RiskName"/>　　　　　　　　　　(币值单位：人民币元)</Prnt>
                                    <Prnt>保单年度末　现金价值　　保单年度末　现金价值　　保单年度末　 现金价值</Prnt>
                                    <xsl:variable name="cashValue" select="$CashValue[riskCode=$riskCode]/cashValue/string"/>
                                    <xsl:for-each select="$cashValue">
                                        <xsl:variable name="times" select="ceiling(count($cashValue) div 3)"/>
                                        <xsl:variable name="count_plc" select="position()"/>
                                        <xsl:if test="$count_plc &lt; ($times+1)">
                                            <Prnt>
                                                <xsl:call-template name="fixedLengthContent">
                                                    <xsl:with-param name="contents"><xsl:value-of
                                                            select="substring-before($cashValue[$count_plc],'_')"/>,<xsl:choose>
                                                        <xsl:when test="$cashValue[$count_plc] !=''"><xsl:value-of
                                                                select="format-number(substring-after($cashValue[$count_plc],'_'),'#0.00')"/>
                                                        </xsl:when>
                                                        <xsl:otherwise><xsl:value-of
                                                                select="substring-after($cashValue[$count_plc],'_')"/>
                                                        </xsl:otherwise>
                                                    </xsl:choose>,<xsl:value-of
                                                            select="substring-before($cashValue[$count_plc+$times],'_')"/>,<xsl:choose>
                                                        <xsl:when test="$cashValue[$count_plc+$times] !=''"><xsl:value-of
                                                                select="format-number(substring-after($cashValue[$count_plc+$times],'_'),'#0.00')"/>
                                                        </xsl:when>
                                                        <xsl:otherwise><xsl:value-of
                                                                select="substring-after($cashValue[$count_plc+$times],'_')"/></xsl:otherwise>
                                                    </xsl:choose>,<xsl:value-of
                                                            select="substring-before($cashValue[$count_plc+2*$times],'_')"/>,<xsl:choose>
                                                        <xsl:when test="$cashValue[$count_plc+2*$times] != ''"><xsl:value-of
                                                                select="format-number(substring-after($cashValue[$count_plc+2*$times],'_'),'#0.00')"/>
                                                        </xsl:when>
                                                        <xsl:otherwise><xsl:value-of
                                                                select="substring-after($cashValue[$count_plc+2*$times],'_')"/>
                                                        </xsl:otherwise>
                                                    </xsl:choose>                                                </xsl:with-param>
                                                    <xsl:with-param name="lengths" select="'12,12,12,12,12,12'"/>
                                                </xsl:call-template>
                                            </Prnt>
                                        </xsl:if>
                                    </xsl:for-each>
                                </xsl:if>
                            </xsl:for-each>
                        </xsl:if>
                        <ExpireDate>
                            <xsl:value-of
                                    select="java:com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.dateFormat($LCPol[1]/EndDate)"/>
                        </ExpireDate>
                    </Ret>
                </App>
            </xsl:if>
        </ABCB2I>
    </xsl:template>
    <!-- 文本对齐 -->
    <xsl:template name="fixedLengthContent">
        <xsl:param name="contents"/>
        <xsl:param name="lengths"/>
        <xsl:value-of
                select="java:com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.fixedLengthString($contents,$lengths)"/>
    </xsl:template>
</xsl:stylesheet>