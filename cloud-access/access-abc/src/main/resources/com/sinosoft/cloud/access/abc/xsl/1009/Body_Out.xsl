<?xml version="1.0" encoding="GBK"?>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:java="http://xml.apache.org/xslt/java" exclude-result-prefixes="java">
    <xsl:template match="/">

        <xsl:variable name="head" select="TranData/Head"/>
        <xsl:variable name="LKTransStatus" select="TranData/Body/LKTransStatus"/>
        <ABCB2I>
            <Header>
                <xsl:if test="$head/Flag=0">
                    <RetCode>000000</RetCode>
                    <RetMsg>交易成功</RetMsg>
                </xsl:if>
                <xsl:if test="$head/Flag!=0">
                    <RetCode>009999</RetCode>
                    <RetMsg>
                        <xsl:value-of select="$head/Desc"/>
                    </RetMsg>
                </xsl:if>
                <SerialNo>
                    <xsl:value-of select="$head/TranNo"/>
                </SerialNo>
                <InsuSerial>
                    <xsl:value-of select="$head/InsuSerial"/>
                </InsuSerial>
                <TransDate>
                    <xsl:value-of
                            select="java:com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.date10to8($head/TranDate)"/>
                </TransDate>
                <TransTime>
                    <xsl:value-of
                            select="java:com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.timeString10To8($head/TranTime)"/>
                </TransTime>
                <BankCode>
                    <xsl:value-of select="$head/BankCode"/>
                </BankCode>
                <CorpNo>
                    <xsl:value-of select="$head/CorpNo"/>
                </CorpNo>
                <TransCode>
                    <xsl:value-of select="$head/TransCode"/>
                </TransCode>
            </Header>
            <xsl:if test="$head/Flag=0">
                <App>
                    <Ret>
                        <OrgSerialNo>
                            <xsl:value-of select="$LKTransStatus/TransNo"/>
                        </OrgSerialNo><!--原交易编码-->
                        <OrgTransDate>
                            <xsl:value-of
                                    select="java:com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.date10to8($LKTransStatus/TransDate)"/>
                        </OrgTransDate><!--原交易日期-->
                        <TransCode>
                            <xsl:value-of select="$LKTransStatus/bak3"/>
                        </TransCode><!-- 所冲正交易编码 -->
                    </Ret>
                </App>
            </xsl:if>
        </ABCB2I>
    </xsl:template>
</xsl:stylesheet>