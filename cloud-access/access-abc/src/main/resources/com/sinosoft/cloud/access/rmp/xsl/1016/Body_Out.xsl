<?xml version="1.0" encoding="GBK"?>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:java="http://xml.apache.org/xslt/java" exclude-result-prefixes="java">
    <xsl:template match="/TranData">
        <xsl:variable name="RespContQuery" select="Body/RespContQuery"/>

        <ABCB2I>
            <Header>
                <xsl:if test="Head/Flag=0">
                    <RetCode>000000</RetCode>
                    <RetMsg>交易成功</RetMsg>
                </xsl:if>
                <xsl:if test="Head/Flag=1">
                    <RetCode>009999</RetCode>
                    <RetMsg>
                        <xsl:value-of select="Head/Desc"/>
                    </RetMsg>
                </xsl:if>
                <xsl:if test="Head/Flag=2">
                    <RetCode>009990</RetCode>
                    <RetMsg>
                        <xsl:value-of select="Head/Desc"/>
                    </RetMsg>
                </xsl:if>
                <SerialNo>
                    <xsl:value-of select="Head/TranNo"/>
                </SerialNo>
                <InsuSerial>
                    <xsl:value-of select="Head/InsuSerial"/>
                </InsuSerial>
                <TransDate>
                    <xsl:value-of
                            select="java:com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.date10to8(Head/TranDate)"/>
                </TransDate>
                <TransTime>
                    <xsl:value-of
                            select="java:com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.timeString10To8(Head/TranTime)"/>
                </TransTime>
                <BankCode>
                    <xsl:value-of select="Head/BankCode"/>
                </BankCode>
                <CorpNo>
                    <xsl:value-of select="Head/CorpNo"/>
                </CorpNo>
                <TransCode>
                    <xsl:value-of select="Head/TransCode"/>
                </TransCode>
            </Header>
            <xsl:if test="Head/Flag='0'">
                <App>
                    <Ret>
                        <PolicyNo>
                            <xsl:value-of select="$RespContQuery/ContNo"/>
                        </PolicyNo>
                        <RiskCode>
                            <xsl:value-of select="$RespContQuery/RiskCode"/>
                        </RiskCode>
                        <RiskName>
                            <xsl:value-of select="$RespContQuery/RiskName"/>
                        </RiskName>
                        <PolicyStatus>
                            <xsl:value-of select="$RespContQuery/PolicyStatus"/>
                        </PolicyStatus><!-- 保单状态 -->
                        <PolicyPledge>
                            <xsl:value-of select="$RespContQuery/PolicyPledge"/>
                        </PolicyPledge><!-- 保单质押状态 -->
                        <AcceptDate>
                            <xsl:value-of
                                    select="java:com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.date10to8($RespContQuery/AcceptDate)"/>
                        </AcceptDate><!-- 受理日期 -->
                        <PolicyBgnDate>
                            <xsl:value-of
                                    select="java:com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.date10to8($RespContQuery/PolicyBgnDate)"/>
                        </PolicyBgnDate><!-- 保单生效日 -->
                        <PolicyEndDate>
                            <xsl:value-of
                                    select="java:com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.date10to8($RespContQuery/PolicyEndDate)"/>
                        </PolicyEndDate><!-- 保单到期日 -->
                        <PolicyAmmount>
                            <xsl:value-of select="java:com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.policyMult($RespContQuery/PolicyAmmount)"/>
                        </PolicyAmmount><!-- 投保份数 -->
                        <Amt>
                            <xsl:value-of select="$RespContQuery/Prem"/>
                        </Amt><!-- 保费 -->
                        <Beamt>
                            <xsl:value-of select="$RespContQuery/Amnt"/>
                        </Beamt><!-- 保额 -->
                        <PolicyValue>
                            <xsl:value-of select="$RespContQuery/PolicyValue"/>
                        </PolicyValue><!-- 保单价值 -->
                        <AutoTransferAccNo>
                            <xsl:value-of select="$RespContQuery/AutoTransferAccNo"/>
                        </AutoTransferAccNo><!-- 自动转账授权账号 -->
                    </Ret>
                </App>
            </xsl:if>
        </ABCB2I>
    </xsl:template>
</xsl:stylesheet>