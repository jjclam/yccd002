<?xml version="1.0" encoding="GBK"?>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:java="http://xml.apache.org/xslt/java" exclude-result-prefixes="java">
    <xsl:template match="/TranData">
        <xsl:variable name="LKTransStatus" select="Body/LKTransStatus"/>
        <xsl:variable name="LCCont" select="Body/LCCont"/>
        <xsl:variable name="LCContState" select="Body/LCContState"/>
        <Package>
            <Head>
                <TransTime>
                    <xsl:value-of select="Head/TranTime"/>
                </TransTime>
                <ThirdPartyCode>
                    <xsl:value-of select="Head/ThirdPartyCode"/>
                </ThirdPartyCode>
                <TransNo>
                    <xsl:value-of select="Head/TransNo"/>
                </TransNo>
                <TransType>
                    <xsl:value-of select="Head/TransCode"/>
                </TransType>
                <Coworker>02</Coworker>

                <xsl:if test="Head/Flag=0">
                    <ResponseCode>0000</ResponseCode>
                </xsl:if>
                <xsl:if test="Head/Flag=1">
                    <ResponseCode>009999</ResponseCode>
                </xsl:if>
                <xsl:if test="Head/Flag=2">
                    <ResponseCode>009990</ResponseCode>
                </xsl:if>
            </Head>
            <ResponseNodes>
                <ResponseNode>
                    <Result>
                        <xsl:if test="Head/Flag=0">
                            <ResultStatus>1</ResultStatus>
                            <ResultInfoDesc>交易成功</ResultInfoDesc>
                        </xsl:if>
                        <xsl:if test="Head/Flag=1">
                            <ResultStatus>0</ResultStatus>
                            <ResultInfoDesc>交易失败</ResultInfoDesc>
                        </xsl:if>
                        <xsl:if test="Head/Flag=2">
                            <ResultStatus>0</ResultStatus>
                            <ResultInfoDesc>交易失败</ResultInfoDesc>
                        </xsl:if>
                    </Result>
                    <BusinessObject>
                        <ThirdPartyOrderId>
                            <xsl:value-of select="$LCCont/ThirdPartyOrderId"/>
                        </ThirdPartyOrderId>
                        <ContNo>
                            <xsl:value-of select="$LCCont/ContNo"/>
                        </ContNo>

                        <ApplyDate>
                            <xsl:value-of select="$LCContState/MakeDate"/>
                        </ApplyDate>
                        <ApplyTime>
                            <xsl:value-of select="$LCContState/MakeTime"/>
                        </ApplyTime>
                        <ApplyFlag>
                            <xsl:value-of select="$LCContState/State"/>
                        </ApplyFlag>
                        <xsl:if test="Head/Flag=0">
                            <Result>1</Result>
                            <ResultDesc>撤单成功</ResultDesc>
                        </xsl:if>
                        <xsl:if test="Head/Flag=1">
                            <Result>0</Result>
                            <ResultDesc>
                                <xsl:value-of select="Head/Desc"/>
                            </ResultDesc>
                        </xsl:if>
                        <xsl:if test="Head/Flag=2">
                            <Result>0</Result>
                            <ResultDesc>
                                <xsl:value-of select="Head/Desc"/>
                            </ResultDesc>
                        </xsl:if>
                    </BusinessObject>
                </ResponseNode>
            </ResponseNodes>
        </Package>
    </xsl:template>
</xsl:stylesheet>