<?xml version="1.0" encoding="GBK"?>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:java="http://xml.apache.org/xslt/java" exclude-result-prefixes="java">
    <xsl:template match="/">
        <xsl:variable name="head" select="TranData/Head"/>
        <xsl:variable name="RespWTDetailsQuery" select="TranData/Body/RespWTDetailsQuery"/><!--犹退pos017-->
        <xsl:variable name="RespExpirationQuery" select="TranData/Body/RespExpirationQuery"/><!--生存返回pos071-->
        <xsl:variable name="RespSurrenderQuery" select="TranData/Body/RespSurrenderQuery"/><!--退保申请pos088-->
        <xsl:variable name="POS017AddtRisk" select="TranData/Body/RespWTDetailsQuery/AddtRisks/AddtRisk"/>
        <xsl:variable name="POS071AddtRisk" select="TranData/Body/RespExpirationQuery/AddtRisks/AddtRisk"/>
        <xsl:variable name="resphead" select="TranData/Body/RespHead"/>
        <xsl:variable name="reqhead" select="TranData/Body/ReqHead"/>
        <ABCB2I>
            <Header>
                <xsl:choose>
                    <xsl:when test="$head/Flag=0">
                        <RetCode>000000</RetCode>
                        <RetMsg>交易成功</RetMsg>
                    </xsl:when>
                    <xsl:when test="$head/Flag=1">
                        <RetCode>009999</RetCode>
                        <RetMsg>
                            <xsl:value-of select="$head/Desc"/>
                        </RetMsg>
                    </xsl:when>
                </xsl:choose>
                <SerialNo>
                    <xsl:value-of select="$head/TranNo"/>
                </SerialNo>
                <InsuSerial>
                    <xsl:value-of select="$head/InsuSerial"/>
                </InsuSerial>
                <TransDate>
                    <xsl:value-of
                            select="java:com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.date10to8($head/TranDate)"/>
                </TransDate>
                <TransTime>
                    <xsl:value-of
                            select="java:com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.timeString10To8($head/TranTime)"/>
                </TransTime>
                <BankCode>
                    <xsl:value-of select="$head/BankCode"/>
                </BankCode>
                <CorpNo>
                    <xsl:value-of select="$head/CorpNo"/>
                </CorpNo>
                <TransCode>
                    <xsl:value-of select="$head/TransCode"/>
                </TransCode>
            </Header>
            <xsl:if test="$head/Flag=0">
                <App>
                    <Ret>
                        <xsl:if test="$resphead/TransID='POS017'or $reqhead/TransID='POS017'">
                            <InsuName>
                                <xsl:value-of select="$RespWTDetailsQuery/MainRiskName"/>
                            </InsuName><!-- 险种名称 -->
                            <PolicyNo>
                                <xsl:value-of select="$RespWTDetailsQuery/ContNo"/>
                            </PolicyNo><!-- 保单号 -->
                            <!--
                            <xsl:variable name="GrossAmt" select="$BQResult/TransferAmt"/>
                            -->
                            <OccurBala>
                                <xsl:value-of select="$RespWTDetailsQuery/OccurBala"/>
                            </OccurBala><!-- 拟领取金额 -->
                            <ValidDate>
                                <xsl:value-of
                                        select="java:com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.date10to8($RespWTDetailsQuery/ValidDate)"/>
                                <!--<xsl:value-of select="$BQResult/ValidDate"/>-->
                            </ValidDate><!-- 保单生效日 -->
                            <ExpireDate>
                                <xsl:value-of
                                        select="java:com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.date10to8($RespWTDetailsQuery/ExpireDate)"/>
                                <!--<xsl:value-of select="$BQResult/ExpireDate"/>-->
                            </ExpireDate><!-- 保单到期日 -->
                            <FeeAmt></FeeAmt><!-- 退保手续费 非必录项 -->
                        </xsl:if>
                        <xsl:if test="$resphead/TransID='POS043'or $reqhead/TransID='POS043'">
                            <InsuName>
                                <xsl:value-of select="$RespExpirationQuery/RiskName"/>
                            </InsuName><!-- 险种名称 -->
                            <PolicyNo>
                                <xsl:value-of select="$RespExpirationQuery/ContNo"/>
                            </PolicyNo><!-- 保单号 -->
                            <OccurBala>
                                <xsl:value-of select="$RespExpirationQuery/OccurBala"/>
                            </OccurBala><!-- 拟领取金额 -->
                            <ValidDate>
                                <xsl:value-of
                                        select="java:com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.date10to8($RespExpirationQuery/ValidDate)"/>
                                <!--<xsl:value-of select="$BQResult/ValidDate"/>-->
                            </ValidDate><!-- 保单生效日 -->
                            <ExpireDate>
                                <xsl:value-of
                                        select="java:com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.date10to8($RespExpirationQuery/ExpireDate)"/>
                                <!--<xsl:value-of select="$BQResult/ExpireDate"/>-->
                            </ExpireDate><!-- 保单到期日 -->
                            <FeeAmt></FeeAmt><!-- 退保手续费 非必录项 -->
                        </xsl:if>
                        <xsl:if test="$resphead/TransID='POS088'or $reqhead/TransID='POS088'">
                            <InsuName>
                                <xsl:value-of select="$RespSurrenderQuery/RiskName"/>
                            </InsuName><!-- 险种名称 -->
                            <PolicyNo>
                                <xsl:value-of select="$RespSurrenderQuery/ContNo"/>
                            </PolicyNo><!-- 保单号 -->
                            <OccurBala>
                                <xsl:value-of select="$RespSurrenderQuery/OccurBala"/>
                            </OccurBala><!-- 拟领取金额 -->
                            <ValidDate>
                                <xsl:value-of
                                        select="java:com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.date10to8($RespSurrenderQuery/ValidDate)"/>
                                <!--<xsl:value-of select="$BQResult/ValidDate"/>-->
                            </ValidDate><!-- 保单生效日 -->
                            <ExpireDate>
                                <xsl:value-of
                                        select="java:com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.date10to8($RespSurrenderQuery/ExpireDate)"/>
                                <!--<xsl:value-of select="$BQResult/ExpireDate"/>-->
                            </ExpireDate><!-- 保单到期日 -->
                            <FeeAmt></FeeAmt><!-- 退保手续费 非必录项 -->
                        </xsl:if>
                        <PrntCount>4</PrntCount><!-- 逐行打印行数 -->
                        <Prnt1>　　　　　本人已确认以上内容代表本人真实意愿，相关信息真实无误。保险合同原件、身份证复印件、</Prnt1>
                        <Prnt2>　　　　　此次付款账户凭证复印件将委托xxx转交给xxxxxx保险股份有限公司，并接受具体付费</Prnt2>
                        <Prnt3>　　　　　金额以xxxxxx保险股份有限公司审核通过日期计算为准。</Prnt3>
                        <Prnt4>　　　　　xxxxxx客服热线95581或4007795581(人工服务时间为9:00-21:00)。</Prnt4>
                    </Ret>
                </App>
            </xsl:if>
        </ABCB2I>
    </xsl:template>
</xsl:stylesheet>