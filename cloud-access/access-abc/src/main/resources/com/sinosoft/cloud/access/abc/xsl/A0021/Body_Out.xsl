<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:java="http://xml.apache.org/xslt/java"
                exclude-result-prefixes="java">
    <xsl:template match="/TranData">
        <xsl:variable name="lccont" select="Body/LCCont"/>
        <xsl:variable name="global" select="Body/Global"/>
        <xsl:variable name="bqresult" select="Body/BQResult"/>
        <!--新加-->
        <xsl:variable name="lKTransStatus" select="Body/LKTransStatus"/>
        <Package>
            <Header>

                <TransTime>
                    <xsl:value-of
                            select="Head/TranTime"/>
                </TransTime>
                <ThirdPartyCode>
                    <xsl:value-of select="$lccont/SellType"/>
                </ThirdPartyCode>
                <TransNo>
                    <xsl:value-of select="$global/SerialNo"/>
                </TransNo>
                <TransType>
                    <xsl:value-of select="$lKTransStatus/TransCode"/>
                    <!--<xsl:value-of select="Head/TransCode"/>-->
                </TransType>
                <Coworker>02</Coworker>
            </Header>

            <ResponseNodes>
                <ResponseNode>
                    <Result>
                     <xsl:if test="Head/Flag=0">
                      <ResultStatus>1</ResultStatus>
                      <ResultInfoDesc>交易成功</ResultInfoDesc>
                    </xsl:if>
                    <xsl:if test="Head/Flag=1">
                      <ResultStatus>0</ResultStatus>
                      <ResultInfoDesc>
                       <xsl:value-of select="Head/Desc"/>
                      </ResultInfoDesc>
                    </xsl:if>
                    <xsl:if test="Head/Flag=2">
                      <ResultStatus>0</ResultStatus>
                      <ResultInfoDesc>
                         <xsl:value-of select="Head/Desc"/>
                      </ResultInfoDesc>
                     </xsl:if>
                    </Result>
                    <BusinessObject>
                        <partnerOrderId>
                            <xsl:value-of select="$lccont/ThirdPartyOrderId"/>
                        </partnerOrderId>
                        <proposalNo>
                            <xsl:value-of select="$lccont/PrtNo"/>
                        </proposalNo>
                        <totalPremium>
                            <xsl:value-of select="$lccont/Prem"/>
                            <!--<xsl:value-of
                                    select="java:com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.getZBPrem($lccont/Prem,$bqresult/TransferAmt)"/>-->
                        </totalPremium>
                        <postFee></postFee>

                        <allSumAmnt>
                            <xsl:value-of select="$lccont/Amnt"/>
                        </allSumAmnt>
                        <!--核保结果说明-->
                        <xsl:if test="Head/Flag=0">
                            <isSuccess>1</isSuccess>
                            <message>核保成功</message>
                        </xsl:if>
                        <xsl:if test="Head/Flag=1">
                            <isSuccess>0</isSuccess>
                            <message>核保失败</message>
                        </xsl:if>
                        <xsl:if test="Head/Flag=2">
                            <isSuccess>0</isSuccess>
                            <message>核保失败</message>
                        </xsl:if>
                    </BusinessObject>
                </ResponseNode>
            </ResponseNodes>
        </Package>
    </xsl:template>
</xsl:stylesheet>