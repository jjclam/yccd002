<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:java="http://xml.apache.org/xslt/java"
                exclude-result-prefixes="java">
    <xsl:template match="/TranData">
        <xsl:variable name="xQJFQueryResult" select="Body/RespAccountChange"/>
        <ABCB2I>
            <Header>
                <xsl:choose>
                    <xsl:when test="Head/Flag=0">
                        <RetCode>000000</RetCode>
                        <RetMsg>���׳ɹ�</RetMsg>
                    </xsl:when>
                    <xsl:otherwise>
                        <RetCode>009999</RetCode>
                        <RetMsg>
                            <xsl:value-of select="Head/Desc"/>
                        </RetMsg>
                    </xsl:otherwise>
                </xsl:choose>
                <SerialNo>
                    <xsl:value-of select="Head/TranNo"/>
                </SerialNo>
                <InsuSerial>
                    <xsl:value-of select="Head/InsuSerial" />
                </InsuSerial>
                <TransDate>
                    <xsl:value-of
                            select="java:com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.date10to8(Head/TranDate)"/>
                </TransDate>
                <TransTime>
                    <xsl:value-of
                            select="java:com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.timeString10To8(Head/TranTime)"/>
                </TransTime>
                <BankCode>
                    <xsl:value-of select="Head/BankCode"/>
                </BankCode>
                <CorpNo>
                    <xsl:value-of select="Head/CorpNo" />
                </CorpNo>
                <TransCode>
                    <xsl:value-of select="Head/TransCode" />
                </TransCode>
            </Header>
            <App>
                <Ret>
                    <RiskCode>
                        <xsl:value-of select="$xQJFQueryResult/RiskCode" />
                    </RiskCode>
                    <PolicyNo>
                        <xsl:value-of select="$xQJFQueryResult/ContNo"/>
                    </PolicyNo>
                </Ret>
            </App>
        </ABCB2I>
    </xsl:template>
</xsl:stylesheet>