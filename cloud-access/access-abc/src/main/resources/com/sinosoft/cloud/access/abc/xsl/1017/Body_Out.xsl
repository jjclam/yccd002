<?xml version="1.0" encoding="GBK"?>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:java="http://xml.apache.org/xslt/java" exclude-result-prefixes="java">
    <xsl:template match="/TranData">
        <ABCB2I>
            <Header>
                <SerialNo></SerialNo>
                <InsuSerial>
                    <xsl:value-of select="Head/InsuSerial"/>
                </InsuSerial>
                <TransDate>
                    <xsl:value-of select="Head/TranDate"/>
                </TransDate>
                <TransTime>
                    <xsl:value-of select="Head/TranTime"/>
                </TransTime>
                <BankCode>03</BankCode>
                <CorpNo>1118</CorpNo>
                <TransCode>1017</TransCode>
                <TransSide>0</TransSide>
                <EntrustWay></EntrustWay>
                <ProvCode></ProvCode>
                <BranchNo></BranchNo>
                <Tlid></Tlid>
            </Header>
            <App>
                <Ret>
                    <TransFlag>1</TransFlag>
                    <FileType>01</FileType>
                    <FileName>cacert.crt</FileName>
                    <FileLen>00000000</FileLen>
                    <FileTimeStamp></FileTimeStamp>
                </Ret>
            </App>
        </ABCB2I>
    </xsl:template>
</xsl:stylesheet>