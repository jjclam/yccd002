<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:java="http://xml.apache.org/xslt/java" exclude-result-prefixes="java">
    <xsl:output method="xml" indent="yes"/>
    <!--保单查询交易-->
    <xsl:template match="ABCB2I">
        <TranData>
            <Body>
                <ReqHead>
                    <Version>1.0</Version>
                    <SerialNo>
                        <xsl:value-of select="Header/SerialNo"/>
                    </SerialNo>
                    <TransID>PC002</TransID>
                    <TransDate>
                        <xsl:value-of select="java:com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.dateFormat8To10(Header/TransDate)"/>
                    </TransDate>
                    <TransTime>
                        <xsl:value-of select="java:com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.timeFormat(Header/TransTime)"/>
                    </TransTime>
                    <SysCode>WIC</SysCode>
                    <Channel>
                        <xsl:call-template name="tran_channel">
                            <xsl:with-param name="channel">
                                <xsl:value-of select="Header/EntrustWay"/>
                            </xsl:with-param>
                        </xsl:call-template><!-- 销售方式 -->
                    </Channel>
                </ReqHead>
                <LKTransTrackses>
                    <LKTransTracks>
                        <!--交易流水号-->
                        <TransNo>
                            <xsl:value-of select="Header/SerialNo"/>
                        </TransNo>
                        <!--交易编码-->
                        <TransCode>PC002</TransCode>
                        <!--原交易编码-->
                        <TransCodeOri>
                            <xsl:value-of select="Header/TransCode"/>
                        </TransCodeOri>
                        <!--交易子渠道-->
                        <AccessChnlSub>
                            <xsl:value-of select="Header/EntrustWay"/>
                        </AccessChnlSub>
                        <xsl:if test="Header/EntrustWay = 11">
                            <BankCode>05</BankCode>
                        </xsl:if>
                        <xsl:if test="Header/EntrustWay != 11">
                            <BankCode>0501</BankCode>
                        </xsl:if>
                        <BankBranch>
                            <xsl:value-of select="Header/ProvCode"/>
                        </BankBranch>
                        <BankNode>
                            <xsl:value-of select="Header/BranchNo"/>
                        </BankNode>
                        <TransDate>
                            <xsl:value-of select="java:com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.dateFormat8To10(Header/TransDate)"/>
                        </TransDate>
                        <TransTime>
                            <xsl:value-of select="java:com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.timeFormat(Header/TransTime)"/>
                        </TransTime>
                        <!--1 银保通 2 移动展业  3 微信卡单 4 微信银保通-->
                        <SellsWay>1</SellsWay>
                    </LKTransTracks>
                </LKTransTrackses>
                <ReqContQuery>
                    <xsl:variable name="varMainRiskCode">
                        <xsl:call-template name="tran_riskcode">
                            <xsl:with-param name="RiskCode" select="App/Req/RiskCode"/>
                        </xsl:call-template>
                    </xsl:variable>
                    <RiskCode>
                        <xsl:value-of select="$varMainRiskCode"/>
                    </RiskCode>
                    <RiskCodeWr>
                        <xsl:value-of select="App/Req/RiskCode"/>
                    </RiskCodeWr>
                    <ContNo>
                        <xsl:value-of select="App/Req/PolicyNo"/>
                    </ContNo>
                </ReqContQuery>
            </Body>
        </TranData>
    </xsl:template>
    <xsl:template name="tran_riskcode">
        <xsl:param name="RiskCode"/>
        <xsl:choose>
            <xsl:when test="$RiskCode='C066'">1034</xsl:when>
            <xsl:when test="$RiskCode='C067'">1037</xsl:when>
            <xsl:when test="$RiskCode='C068'">1038</xsl:when>
            <!--<xsl:when test="$RiskCode='C071'">7063</xsl:when>-->
            <xsl:when test="$RiskCode='C072'">1041</xsl:when>
            <xsl:when test="$RiskCode='SEM'">6810</xsl:when>
            <xsl:when test="$RiskCode='SEL'">6810</xsl:when>
            <xsl:when test="$RiskCode='SEO'">6810</xsl:when>
            <xsl:when test="$RiskCode='7053a'">7053</xsl:when>
            <xsl:when test="$RiskCode='7060a'">7060</xsl:when>
            <xsl:when test="$RiskCode='1032a'">1032</xsl:when>
            <xsl:when test="$RiskCode='5022'">1034</xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="$RiskCode"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    <xsl:template name="tran_channel">
        <xsl:param name="channel"></xsl:param>
        <xsl:if test="$channel = 11">ABCOTC</xsl:if>
        <xsl:if test="$channel = 01">ABCEBANK</xsl:if>
        <xsl:if test="$channel = 04">ABCSELF</xsl:if>
        <xsl:if test="$channel = 02">ABCMOBILE</xsl:if>
        <xsl:if test="$channel = 22">ABCSUPOTC</xsl:if>
        <xsl:if test="$channel = 08">ABCESTWD</xsl:if>
    </xsl:template>

</xsl:stylesheet>
