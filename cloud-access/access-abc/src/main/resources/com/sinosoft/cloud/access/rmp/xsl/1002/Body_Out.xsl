<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:java="http://xml.apache.org/xslt/java"
                exclude-result-prefixes="java">
    <xsl:template match="/TranData">
        <xsl:variable name="lccont" select="Body/LCCont"/>
        <xsl:variable name="global" select="Body/Global"/>
        <xsl:variable name="bqresult" select="Body/BQResult"/>
        <ABCB2I>
            <Header>
                <xsl:if test="Head/Flag=0">
                    <RetCode>000000</RetCode>
                    <RetMsg>���׳ɹ�</RetMsg>
                </xsl:if>
                <xsl:if test="Head/Flag=1">
                    <RetCode>009999</RetCode>
                    <RetMsg>
                        <xsl:value-of select="Head/Desc"/>
                    </RetMsg>
                </xsl:if>
                <xsl:if test="Head/Flag=2">
                    <RetCode>009990</RetCode>
                    <RetMsg>
                        <xsl:value-of select="Head/Desc"/>
                    </RetMsg>
                </xsl:if>
                <SerialNo>
                    <xsl:value-of select="Head/TranNo"/>
                </SerialNo>
                <InsuSerial>
                    <xsl:value-of select="Head/InsuSerial"/>
                </InsuSerial>
                <TransDate>
                    <xsl:value-of
                            select="java:com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.date10to8(Head/TranDate)"/>
                </TransDate>
                <TransTime>
                    <xsl:value-of
                            select="java:com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.timeString10To8(Head/TranTime)"/>
                </TransTime>
                <BankCode>
                    <xsl:value-of select="Head/BankCode"/>
                </BankCode>
                <CorpNo>
                    <xsl:value-of select="Head/CorpNo"/>
                </CorpNo>
                <TransCode>
                    <xsl:value-of select="Head/TransCode"/>
                </TransCode>
            </Header>
            <App>
                <Ret>
                    <xsl:if test="Head/Flag=0">
                        <xsl:choose>
                            <xsl:when test="$global/TransferFlag=1">
                                <TransferAmt>
                                    <xsl:value-of select="$bqresult/TransferAmt"/>
                                </TransferAmt>
                                <PolicyAmt>
                                    <xsl:value-of select="$lccont/Prem"/>
                                </PolicyAmt>
                                <PolicyNo>
                                    <xsl:value-of select="$lccont/ContNo"/>
                                </PolicyNo>
                                <Prem>
                                    <xsl:value-of
                                            select="java:com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.getZBPrem($lccont/Prem,$bqresult/TransferAmt)"/>
                                    <!--     <xsl:value-of select="BQResult/Prem"/>-->
                                </Prem>
                            </xsl:when>
                            <xsl:otherwise>
                                <Prem>
                                    <xsl:value-of select="$lccont/Prem"/>
                                </Prem>
                                <PolicyNo>
                                    <xsl:value-of select="$lccont/ContNo"/>
                                </PolicyNo>
                            </xsl:otherwise>
                        </xsl:choose>
                    </xsl:if>
                </Ret>
            </App>
        </ABCB2I>
    </xsl:template>
</xsl:stylesheet>