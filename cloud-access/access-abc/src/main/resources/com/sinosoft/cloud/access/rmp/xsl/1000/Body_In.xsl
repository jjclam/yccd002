<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:java="http://xml.apache.org/xslt/java" exclude-result-prefixes="java">
    <xsl:output method="xml" indent="yes"/>
    <!--自动冲正交易-->
    <xsl:template match="ABCB2I">
        <TranData>
            <Head>
                <TranDate><!-- 交易日期 -->
                    <xsl:value-of select="java:com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.dateFormat8To10(Header/TransDate)"/>
                </TranDate>
                <TranTime><!-- 交易时间 -->
                    <xsl:value-of select="java:com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.timeFormat(Header/TransTime)"/>
                </TranTime>
                <TellerNo>nonghang</TellerNo>
                <TranNo>
                    <xsl:value-of select="Header/SerialNo"/>
                </TranNo>
                <NodeNo>xtjywd</NodeNo>
                <TranCom>2</TranCom>
                <BankCode>02</BankCode>
                <FuncFlag>322</FuncFlag>
                <AgentCom></AgentCom>
                <AgentCode>-</AgentCode>
            </Head>
            <Body>
                <Reserve>
                    <xsl:value-of select="App/Req/Reserve"/>
                </Reserve><!-- 备注 -->
            </Body>
        </TranData>
    </xsl:template>
</xsl:stylesheet>
