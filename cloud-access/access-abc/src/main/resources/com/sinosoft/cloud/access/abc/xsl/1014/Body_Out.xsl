<?xml version="1.0" encoding="GBK"?>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:java="http://xml.apache.org/xslt/java" exclude-result-prefixes="java">
    <xsl:template match="/">
        <xsl:variable name="head" select="TranData/Head"/>
        <xsl:variable name="BQResult" select="TranData/Body/RespPreservationStateQuery"/>
        <ABCB2I>
            <Header>
                <xsl:if test="$head/Flag=0">
                    <RetCode>000000</RetCode>
                    <RetMsg>交易成功</RetMsg>
                </xsl:if>
                <xsl:if test="$head/Flag=1">
                    <RetCode>009999</RetCode>
                    <RetMsg>
                        <xsl:value-of select="$head/Desc"/>
                    </RetMsg>
                </xsl:if>
                <xsl:if test="$head/Flag=2">
                    <RetCode>009990</RetCode>
                    <RetMsg>
                        <xsl:value-of select="$head/Desc"/>
                    </RetMsg>
                </xsl:if>
                <SerialNo>
                    <xsl:value-of select="$head/TranNo"/>
                </SerialNo>
                <InsuSerial>
                    <xsl:value-of select="$head/InsuSerial"/>
                </InsuSerial>
                <TransDate>
                    <xsl:value-of
                            select="java:com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.date10to8($head/TranDate)"/>
                </TransDate>
                <TransTime>
                    <xsl:value-of
                            select="java:com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.timeString10To8($head/TranTime)"/>
                </TransTime>
                <BankCode>
                    <xsl:value-of select="$head/BankCode"/>
                </BankCode>
                <CorpNo>
                    <xsl:value-of select="$head/CorpNo"/>
                </CorpNo>
                <TransCode>
                    <xsl:value-of select="$head/TransCode"/>
                </TransCode>
            </Header>
            <xsl:if test="$head/Flag=0">
                <App>
                    <Ret>
                        <RiskCode>
                            <xsl:value-of select="$BQResult/RiskCode"/>
                        </RiskCode><!-- 险种 -->
                        <PolicyNo>
                            <xsl:value-of select="$BQResult/ContNo"/>
                        </PolicyNo><!-- 保单号 -->
                        <PolicyStatus><!--保单状态-->
                            <xsl:value-of select="$BQResult/PolicyStatus"/>
                        </PolicyStatus>
                        <BQStatus><!--保全状态-->
                            <xsl:value-of select="$BQResult/BqStatus"/>
                        </BQStatus>
                        <ApplyDate><!--申请日期-->
                            <xsl:value-of
                                    select="java:com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.date10to8($BQResult/ApplyDate)"/>
                        </ApplyDate>
                        <ValidDate><!--生效日期-->
                            <xsl:choose>
                                <xsl:when test="$BQResult/ValidDate != null">
                                    <xsl:value-of
                                            select="java:com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.date10to8($BQResult/ValiDate)"/>
                                </xsl:when>
                                <xsl:otherwise>

                                </xsl:otherwise>
                            </xsl:choose>
                        </ValidDate>
                        <BusinType><!--业务类别-->
                            <xsl:value-of select="$BQResult/EdorType"/>
                        </BusinType>
                    </Ret>
                </App>
            </xsl:if>
        </ABCB2I>
    </xsl:template>
</xsl:stylesheet>