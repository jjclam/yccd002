<?xml version="1.0" encoding="GBK"?>

<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:java="http://xml.apache.org/xslt/java"
                exclude-result-prefixes="java">

    <xsl:template match="ABCB2I">
        <TranData>
            <!-- 报文体 -->
            <Body>
                <OldTranno>
                    <xsl:value-of select="App/Req/ApplySerial"/>
                </OldTranno>
                <!-- 试算申请顺序号 -->
                <Global>
                    <ApplySerial>
                        <xsl:value-of select="App/Req/ApplySerial"/>
                    </ApplySerial>
                    <MainRiskCode>
                        <xsl:value-of select="App/Req/RiskCode"/>
                    </MainRiskCode>
                </Global>
                <!-- 应交保险费 -->
                <LCCont>
                    <Prem>
                        <xsl:call-template name="tran_payAmt">
                            <xsl:with-param name="payAmt">
                                <xsl:value-of select="App/Req/PayAmt"/>
                            </xsl:with-param>
                        </xsl:call-template>
                    </Prem>
                    <BankAccNo>
                        <xsl:value-of select="App/Req/PayAccount"/>
                    </BankAccNo>
                    <ContNo>
                        <xsl:value-of select="App/Req/PolicyNo"/>
                    </ContNo>
                    <AccName>
                        <xsl:value-of select="App/Req/AccName"/>
                    </AccName>
                </LCCont>
                <!-- 交费账户 -->
                <!-- 保险公司方险种代码 -->
                <LCPols>
                    <LCPol>
                        <RiskCode>
                            <xsl:value-of select="App/Req/RiskCode"/>
                        </RiskCode>
                    </LCPol>
                </LCPols>
                <LKTransTrackses>
                    <LKTransTracks>
                        <!--交易流水号-->
                        <TransNo>
                            <xsl:value-of select="Header/SerialNo"/>
                        </TransNo>
                        <!--交易编码-->
                        <TransCode>
                            <xsl:value-of select="Header/TransCode"/>
                        </TransCode>
                        <!--原交易编码-->
                        <TransCodeOri>
                            <xsl:value-of select="Header/TransCode"/>
                        </TransCodeOri>
                        <!--交易子渠道-->
                        <AccessChnlSub>
                            <xsl:value-of select="Header/EntrustWay"/>
                        </AccessChnlSub>
                        <xsl:if test="Header/EntrustWay = 11">
                            <BankCode>05</BankCode>
                        </xsl:if>
                        <xsl:if test="Header/EntrustWay != 11">
                            <BankCode>0501</BankCode>
                        </xsl:if>
                        <BankBranch>
                            <xsl:value-of select="Header/ProvCode"/>
                        </BankBranch>
                        <BankNode>
                            <xsl:value-of select="Header/BranchNo"/>
                        </BankNode>
                        <TransDate>
                            <xsl:value-of select="java:com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.dateFormat8To10(Header/TransDate)"/>
                        </TransDate>
                        <TransTime>
                            <xsl:value-of select="java:com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.timeFormat(Header/TransTime)"/>
                        </TransTime>
                        <!--1 银保通 2 移动展业  3 微信卡单 4 微信银保通-->
                        <SellsWay>1</SellsWay>
                    </LKTransTracks>
                </LKTransTrackses>

                <!-- 是否开户行标志 -->
                <!--<OpenBranchFlag>-->
                <!--<xsl:value-of select="App/Req/OpenBranchFlag"/>-->
                <!--</OpenBranchFlag>-->
                <!--<OldTranno>-->
                <!--<xsl:value-of select="App/Req/ApplySerial"/>-->
                <!--</OldTranno>-->
            </Body>
        </TranData>
    </xsl:template>

    <xsl:template name="tran_payAmt">
        <xsl:param name="payAmt"/>
        <xsl:choose>
            <xsl:when test="$payAmt &lt; 0">0.00</xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="$payAmt"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

</xsl:stylesheet>