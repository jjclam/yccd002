<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:java="http://xml.apache.org/xslt/java"
                exclude-result-prefixes="java">
    <xsl:template match="/ABCB2I">
        <RuleSet>
            <xsl:if test="Header/EntrustWay='22'">
                <Rule regex="(\d+(\.\d+)?)" errMsg="投保人年收入必须为阿拉伯数字"><xsl:value-of select="App/Req/Appl/AnnualIncome"/></Rule>
            </xsl:if>
           <!-- <xsl:if test="Header/EntrustWay='11'">
                <Rule regex="(true)" errMsg="预留儿童其它公司身故保额不能为空"><xsl:value-of select="java:com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.checkReserve4ByInsuredAgeTo18(App/Req/Insu/Birthday,App/Req/Reserve4)"/></Rule>
            </xsl:if>-->
        </RuleSet>
    </xsl:template>
</xsl:stylesheet>
