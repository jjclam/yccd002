<?xml version="1.0" encoding="GBK"?>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:java="http://xml.apache.org/xslt/java" exclude-result-prefixes="java">
    <xsl:template match="/TranData">
        <xsl:variable name="RespContQuery" select="Body/RespContQuery"/>
        <xsl:variable name="ReqContQuery" select="Body/ReqContQuery"/>

        <Package>
            <Head>

                <TransTime>
                    <xsl:value-of select="Head/TranTime"/>
                </TransTime>
                <ThirdPartyCode>
                    <xsl:value-of select="Head/ThirdPartyCode"/>
                </ThirdPartyCode>
                <TransNo>
                    <xsl:value-of select="Head/TransNo"/>
                </TransNo>
                <TransType>
                    <xsl:value-of select="Head/TransCode"/>
                </TransType>
                <Coworker>02</Coworker>
                <xsl:if test="Head/Flag=0">
                    <ResponseCode>1</ResponseCode>
                </xsl:if>
                <xsl:if test="Head/Flag=1">
                    <ResponseCode>0</ResponseCode>
                </xsl:if>
                <xsl:if test="Head/Flag=2">
                    <ResponseCode>0</ResponseCode>
                </xsl:if>

            </Head>
                <ResponseNodes>
                    <ResponseNode>
                        <Result>
                            <xsl:if test="Head/Flag=0">
                                <ResultStatus>1</ResultStatus>
                                <ResultInfoDesc>交易成功</ResultInfoDesc>
                            </xsl:if>
                            <xsl:if test="Head/Flag=1">
                                <ResultStatus>0</ResultStatus>
                                <ResultInfoDesc>
                                    <xsl:value-of select="Head/Desc"/>
                                </ResultInfoDesc>
                            </xsl:if>
                            <xsl:if test="Head/Flag=2">
                                <ResultStatus>0</ResultStatus>
                                <ResultInfoDesc>
                                    <xsl:value-of select="Head/Desc"/>
                                </ResultInfoDesc>
                            </xsl:if>
                        </Result>
                        <BusinessObject>
                            <ThirdPartyOrderId>
                                <xsl:value-of select="$ReqContQuery/ThirdPartyOrderId"/>
                            </ThirdPartyOrderId>
                            <ContNo>
                                <xsl:value-of select="$ReqContQuery/ContNo"/>
                            </ContNo>
                            <!--保单状态  appflag为1承保成功  appflag为0时去查lcuwerror，为空表示核保成功，不为空，则核保失败-->
                            <DealStatus>
                                <xsl:value-of
                                        select="$RespContQuery/PolicyStatus"/>
                            </DealStatus>

                        </BusinessObject>
                    </ResponseNode>
                </ResponseNodes>
        </Package>
    </xsl:template>
</xsl:stylesheet>