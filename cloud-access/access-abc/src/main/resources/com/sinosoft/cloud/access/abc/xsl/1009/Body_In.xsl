<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:java="http://xml.apache.org/xslt/java" exclude-result-prefixes="java">
    <xsl:output method="xml" indent="yes"/>
    <!--自动冲正交易-->
    <xsl:template match="ABCB2I">

        <TranData>
            <Body>
                <OldTranno>
                    <xsl:value-of select="App/Req/OrgSerialNo"/>
                </OldTranno><!--原交易流水号-->
                <LKTransTrackses>
                    <LKTransTracks>
                        <!--交易流水号-->
                        <TransNo>
                            <xsl:value-of select="Header/SerialNo"/>
                        </TransNo>
                        <!--交易编码-->
                        <TransCode>
                            <xsl:value-of select="Header/TransCode"/>
                        </TransCode>
                        <!--原交易编码-->
                        <TransCodeOri>
                            <xsl:value-of select="Header/TransCode"/>
                        </TransCodeOri>
                        <!--交易子渠道-->
                        <AccessChnlSub>
                            <xsl:value-of select="Header/EntrustWay"/>
                        </AccessChnlSub>
                        <xsl:if test="Header/EntrustWay = 11">
                            <BankCode>05</BankCode>
                        </xsl:if>
                        <xsl:if test="Header/EntrustWay != 11">
                            <BankCode>0501</BankCode>
                        </xsl:if>
                        <BankBranch>
                            <xsl:value-of select="Header/ProvCode"/>
                        </BankBranch>
                        <BankNode>
                            <xsl:value-of select="Header/BranchNo"/>
                        </BankNode>
                        <TransDate>
                            <xsl:value-of select="java:com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.dateFormat8To10(Header/TransDate)"/>
                        </TransDate>
                        <TransTime>
                            <xsl:value-of select="java:com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.timeFormat(Header/TransTime)"/>
                        </TransTime>
                        <!--1 银保通 2 移动展业  3 微信卡单 4 微信银保通-->
                        <SellsWay>1</SellsWay>
                    </LKTransTracks>
                </LKTransTrackses>
                <LCCont>
                    <ContNo>
                        <xsl:value-of select="App/Req/PolicyNo"/>
                    </ContNo>
                </LCCont>
                <LKTransStatus>
                    <xsl:if test="Header/EntrustWay = 11">
                        <BankCode>05</BankCode>
                    </xsl:if>
                    <xsl:if test="Header/EntrustWay != 11">
                        <BankCode>0501</BankCode>
                    </xsl:if>
                    <FuncFlag>
                        <xsl:value-of select="Header/TransCode"/>
                    </FuncFlag>

                    <TransNo><!--原交易流水号-->
                        <xsl:value-of select="App/Req/OrgSerialNo"/>
                    </TransNo>
                    <TransCode><!--原交易流水号-->
                        <xsl:value-of select="App/Req/OrgSerialNo"/>
                    </TransCode>
                    <BankNode>
                        <xsl:value-of select="Header/BranchNo"/>
                    </BankNode>
                    <BankBranch>
                        <xsl:value-of select="Header/ProvCode"/>
                    </BankBranch>
                    <TransDate>
                        <xsl:value-of
                                select="java:com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.dateFormat8To10(App/Req/OrgTransDate)"/>
                    </TransDate>
                    <bak3><!--原交易类型-->
                        <xsl:value-of select="App/Req/TransCode"/>
                    </bak3>
                    <PolNo>
                        <xsl:value-of select="App/Req/PolicyNo"/>
                    </PolNo>
                </LKTransStatus>
            </Body>
        </TranData>
    </xsl:template>
</xsl:stylesheet>
