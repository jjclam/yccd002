<?xml version="1.0" encoding="GBK"?>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:java="http://xml.apache.org/xslt/java" exclude-result-prefixes="java">

    <xsl:template match="ABCB2I">
        <TranData>
            <Body>
             <!--   <ReqHead>
                    <Version>1.0</Version>
                    <SerialNo>
                        <xsl:value-of select="Header/SerialNo"/>
                    </SerialNo>
                    <TransID>NB006</TransID>
                    <TransDate>
                        <xsl:value-of select="java:com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.dateFormat8To10(Header/TransDate)"/>
                    </TransDate>
                    <TransTime>
                        <xsl:value-of select="java:com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.timeFormat(Header/TransTime)"/>
                    </TransTime>
                    <SysCode>WIC</SysCode>
                    <Channel>
                      &lt;!&ndash;  <xsl:call-template name="tran_channel">
                            <xsl:with-param name="channel">
                                <xsl:value-of select="Header/EntrustWay"/>
                            </xsl:with-param>
                        </xsl:call-template> 销售方式 &ndash;&gt;
                        <xsl:value-of select="Header/EntrustWay"/>
                    </Channel>
                </ReqHead>
                <ReqRetypePolicy>
                    <NewVchNo>
                        <xsl:value-of select="App/Req/NewVchNo"/>
                    </NewVchNo>
                    <OldVchNo>
                        <xsl:value-of select="App/Req/OldVchNo"/>
                    </OldVchNo>
                    <ProposalContNo>
                        <xsl:value-of select="App/Req/PolicyApplyNo"/>
                    </ProposalContNo>
                    <xsl:if test="Header/EntrustWay = 11">
                        <BankCode>05</BankCode>
                    </xsl:if>
                    <xsl:if test="Header/EntrustWay != 11">
                        <BankCode>0501</BankCode>
                    </xsl:if>
                    <ZoneNo>
                        <xsl:value-of select="Header/ProvCode"/>
                    </ZoneNo>
                    <BranchNo>
                        <xsl:value-of select="Header/BranchNo"/>
                    </BranchNo>
                    <TellerNo>&lt;!&ndash; 柜员代码 &ndash;&gt;
                        <xsl:value-of select="Header/Tlid"/>
                    </TellerNo>
                    <RiskCode>
                        <xsl:value-of select="App/Req/InsuCode"/>
                    </RiskCode>
                </ReqRetypePolicy>
                <LKTransTrackses>
                    <LKTransTracks>
                        &lt;!&ndash;交易流水号&ndash;&gt;
                        <TransNo>
                            <xsl:value-of select="Header/SerialNo"/>
                        </TransNo>
                        &lt;!&ndash;交易编码&ndash;&gt;
                        <TransCode>NB006</TransCode>
                        &lt;!&ndash;原交易编码&ndash;&gt;
                        <TransCodeOri>
                            <xsl:value-of select="Header/TransCode"/>
                        </TransCodeOri>
                        &lt;!&ndash;交易子渠道&ndash;&gt;
                        <AccessChnlSub>
                            <xsl:value-of select="Header/EntrustWay"/>
                        </AccessChnlSub>
                        <xsl:if test="Header/EntrustWay = 11">
                            <BankCode>05</BankCode>
                        </xsl:if>
                        <xsl:if test="Header/EntrustWay != 11">
                            <BankCode>0501</BankCode>
                        </xsl:if>
                        <BankBranch>
                            <xsl:value-of select="Header/ProvCode"/>
                        </BankBranch>
                        <BankNode>
                            <xsl:value-of select="Header/BranchNo"/>
                        </BankNode>
                        <TransDate>
                            <xsl:value-of select="java:com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.dateFormat8To10(Header/TransDate)"/>
                        </TransDate>
                        <TransTime>
                            <xsl:value-of select="java:com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction.timeFormat(Header/TransTime)"/>
                        </TransTime>
                        &lt;!&ndash;1 银保通 2 移动展业  3 微信卡单 4 微信银保通&ndash;&gt;
                        <SellsWay>1</SellsWay>
                    </LKTransTracks>
                </LKTransTrackses>
                <Global>
                    <ContPrtNo>
                        <xsl:value-of select="App/Req/NewVchNo"/>
                    </ContPrtNo>
                    <OldVchNo>
                        <xsl:value-of select="App/Req/OldVchNo"/>
                    </OldVchNo>
                </Global>
                <OldContPrtNo><xsl:value-of select="App/Req/OldVchNo"/></OldContPrtNo>-->
                <LCPols>
                    <LCPol>
                        <RiskCode>
                            <xsl:value-of select="App/Req/InsuCode"/>
                        </RiskCode>
                    </LCPol>
                </LCPols>
                <LCCont>
                    <PrtNo>
                        <xsl:value-of select="App/Req/PolicyApplyNo"/>
                    </PrtNo>
                </LCCont>
                <Global>
                    <ContPrtNo>
                        <xsl:value-of select="App/Req/NewVchNo"/>
                    </ContPrtNo>
                    <OldVchNo>
                        <xsl:value-of select="App/Req/OldVchNo"/>
                    </OldVchNo>
                </Global>
                <OldContPrtNo><xsl:value-of select="App/Req/OldVchNo"/></OldContPrtNo>
            </Body>
        </TranData>
    </xsl:template>
    <xsl:template name="tran_channel">
        <xsl:param name="channel"></xsl:param>
        <xsl:if test="$channel = 11">ABCOTC</xsl:if>
        <xsl:if test="$channel = 01">ABCEBANK</xsl:if>
        <xsl:if test="$channel = 04">ABCSELF</xsl:if>
        <xsl:if test="$channel = 02">ABCMOBILE</xsl:if>
        <xsl:if test="$channel = 22">ABCSUPOTC</xsl:if>
        <xsl:if test="$channel = 08">ABCESTWD</xsl:if>
    </xsl:template>

</xsl:stylesheet>
