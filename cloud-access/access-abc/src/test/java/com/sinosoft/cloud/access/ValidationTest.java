package com.sinosoft.cloud.access;

import com.sinosoft.cloud.access.entity.Body;
import com.sinosoft.cloud.access.entity.Head;
import com.sinosoft.cloud.access.entity.Insured;
import com.sinosoft.cloud.access.entity.TranData;
import com.sinosoft.cloud.access.transformer.Validation;
import org.junit.Test;

/**
 * cloud-access
 *
 * @title: cloud-access
 * @package: com.sinosoft.cloud.access
 * @author: yangming
 * @date: 2017/11/15 下午5:53
 */
public class ValidationTest {

    @Test
    public void test1() {
        Validation validation = new Validation("abc");
    }

    @Test
    public void testRule1() {
        Validation validation = new Validation("abc");
        TranData tranData = new TranData();
        Body body = new Body();
        Head head = new Head();
       /* body.setPrem("2");*/
        Insured insured = new Insured();
        insured.setInsValidityday("99991231");
     /*   body.setInsured(insured);*/
        head.setFlag("");
        head.setTransCode("1002");
        tranData.setHead(head);
        tranData.setBody(body);

        if (!validation.check(tranData)) {
            System.out.println(validation.getErrors());
        }
    }


    @Test
    public void test(){
        String s = "111.";
        boolean b = s.endsWith(".");
        System.out.println(b);
        String s1 = s.replaceAll("[0-9]", "");
        System.out.println(s1);
        if (s1.length()== 0 || (s1.length() == 1 && s1.contains(".") && !s.endsWith("."))){
            System.out.println("true");
        }
    }
}
