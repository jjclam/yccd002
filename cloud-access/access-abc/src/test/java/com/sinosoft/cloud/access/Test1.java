package com.sinosoft.cloud.access;

import com.sinosoft.cloud.access.entity.LCCustomerImpart;
import com.sinosoft.cloud.access.entity.LCCustomerImpartParams;
import com.sinosoft.cloud.access.entity.LCPol;
import com.sinosoft.cloud.access.transformer.function.XSLTransfromFunction;
import com.sinosoft.cloud.access.util.TimeUtil;
import com.sinosoft.cloud.rest.TradeInfo;
import com.sinosoft.cloud.util.TradeInfoUtil;
import com.sinosoft.lis.entity.LCCustomerImpartParamsPojo;
import com.sinosoft.lis.entity.LCCustomerImpartPojo;
import com.sinosoft.lis.entity.LCPolPojo;
import com.sinosoft.lis.entity.LCResultInfoPojo;
import com.sinosoft.utility.Reflections;
import org.apache.commons.lang.StringEscapeUtils;
import org.junit.Test;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Administrator on 2018-3-8.
 */
public class Test1 {
    @Test
    public void testInsured(){
        String name="朱映山";
        char[] nameChars = name.toCharArray();
        if ((!checkAllChineseName(nameChars)) && (!checkLatinName(nameChars))) {

        }
    }

    // 校验是否全为汉字
    private boolean checkAllChineseName(final char[] names) {
        for (int i = 0; i < names.length; i++) {
            if (names[i] < 255) {
                return false;
            }
        }
        return true;
    }

    private boolean checkLatinName(final char[] names) {
        for (int i = 0; i < names.length; i++) {
            if (!((names[i] >= 'a' && names[i] <= 'z') || (names[i] >= 'A' && names[i] <= 'Z') || names[i] == ' '
                    || names[i] == '.')) {
                return false;
            }
        }
        return true;
    }

    @Test
    public void testLCResultInfoPojo(){
        //错误信息
        String errorStr = "";
        String errorLog = "";
        TradeInfo tradeInfo=new TradeInfo();
        List<LCResultInfoPojo> lcResultInfoPojos=new ArrayList<>();
        LCResultInfoPojo lcResultInfoPojo=new LCResultInfoPojo();
        lcResultInfoPojo.setContNo("123123");
        lcResultInfoPojos.add(lcResultInfoPojo);
        tradeInfo.addData(LCResultInfoPojo.class.getName(),lcResultInfoPojos);
        TradeInfo respTradeInfo=tradeInfo;

        List<LCResultInfoPojo> LCResultInfoList = (List<LCResultInfoPojo>) respTradeInfo.getData(LCResultInfoPojo.class.getName());
        if (null != LCResultInfoList && LCResultInfoList.size() > 0) {
            errorStr = new StringBuffer(1024).append("[").append(LCResultInfoList.get(0).getResultNo())
                    .append("]:").append(LCResultInfoList.get(0).getResultContent()).toString();
            errorLog = "LCResultInfoList信息不为空，错误信息是：" + LCResultInfoList.toString();
            System.out.println(errorStr+errorLog);
          //  return getCheckRuleFailInfo(errorStr, errorLog);
        }
       /* String transDate="2019-03-13";
        String appbirthday="1990-01-12";
        String polApplyDate="2019-03-13";
        String birthday="1990-01-12";
        String s= XSLTransfromFunction.modify1035CValiDate(transDate,birthday,polApplyDate,appbirthday);
        System.out.println(s);*/
    }
    @Test
    public void test1035RiskCode(){

        LCCustomerImpartPojo l=new LCCustomerImpartPojo();
        if (l.getImpartParamModle() == null && "".equals(l.getImpartParamModle())){
           System.out.println("error");
        }




        String transDate="2019-03-13";
        String appbirthday="1990-01-12";
        String polApplyDate="2019-03-13";
        String birthday="1990-01-12";
        String s1= XSLTransfromFunction.modify1035CValiDate(transDate,birthday,polApplyDate,appbirthday);
        System.out.println(s1);
    }
    @Test
    public void testdate(){
        String transDate="2019-03-14";
        String appbirthday="1990-01-12";
        String polApplyDate="2019-03-14";
        String birthday="1990-01-12";
        String s= XSLTransfromFunction.modifyCValiDate(transDate,birthday,polApplyDate);
        System.out.println(s);
    }
 @Test
 public void test04(){
     String s="3019-01-19";

 }
    @Test
    public void test03() throws Exception{
        String s = "456 ";
        double v = Double.parseDouble(s);
        System.out.println(v);
    }

    @Test
    public void test02() throws Exception{
        Reflections reflections = new Reflections();
        LCPolPojo lcPolPojo = new LCPolPojo();
        LCPol lcPol = new LCPol();
        reflections.transFields(lcPol,lcPolPojo);
        reflections.transFields(lcPolPojo,lcPol);
    }

    @Test
    public void test() throws Exception {

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String date = simpleDateFormat.format(new Date());

        String s = "with allinfo as                                                                  "
                + " (select tt.busstype type, sum(tt.getmoney) allmoney, count(1) allcount          "
                + "    from (																		"
                + "select (case                                                           "
                + "                   when (select distinct '1'                                     "
                + "                           from lpedoritem c                                     "
                + "                          where c.edoracceptno = a.edoracceptno                  "
                + "                            and c.edortype = 'CT'                                "
                + "                            and c.standbyflag1 = '1') = '1' then                 "
                + "                    '01'                                                         "
                + "                   when (select distinct '1'                                     "
                + "                           from lpedoritem c                                     "
                + "                          where c.edoracceptno = a.edoracceptno                  "
                + "                            and c.edortype = 'CT'                                "
                + "                            and c.standbyflag1 = '2') = '1' then                 "
                + "                    '03'                                                         "
                + "                   when (select distinct '1'                                     "
                + "                           from lpedoritem c                                     "
                + "                          where c.edoracceptno = a.edoracceptno                  "
                + "                            and c.edortype = 'AG') = '1' then                    "
                + "                    '02'                                                         "
                + "                   else                                                          "
                + "                    ''                                                           "
                + "                 end) busstype,                                                  "
                + "                 a.approvedate,                                                  "
                + "                 a.otherno,                                                      "
                + "       decode(a.edorappname,'',(select d.appntname from lcappnt d where d.contno = a.otherno),a.edorappname),                                                 "
                + "		  case when(select distinct '1'  from lktransinfo lk where lk.funcflag='1004' and lk.edoracceptno= a.edoracceptno)='1' then '0' else decode(a.apptype, '14', '1','18', '1','19','1','22','1','0') end,"
                + "                 (select d.appntname from lcappnt d where d.contno = a.otherno), "
                + "                 (select d.idtype from lcappnt d where d.contno = a.otherno),    "
                + "                 (select d.idno from lcappnt d where d.contno = a.otherno),      "
                + "                decode(a.edorstate,'0','2', '4','3','6', '3','8','3','9','3','7','3',''),"
                + "                 nvl(abs(a.getmoney),0) getmoney                                        "
                + "            from lpedorapp a,                                                    "
                + "                 (select b.approvedate,                                          "
                + "                         b.otherno,                                              "
                + "                         max(b.approvetime) approvetime                          "
                + "                    from lpedorapp b                                             "
                + "                   where exists (select 1                                        "
                + "                            from lccont                                          "
                + "                           where b.otherno = contno                              "
                + "                             and selltype in ('02','08','16','22','24','25')                  "
                + "                             and salechnl in ('3', '7')                          "
                + "                             and bankcode = '03')                                "
                + "                     and b.edorstate in ('0', '4', '6', '8', '9','7')                "
                + "                     and exists                                                  "
                + "                   (select 1                                                     "
                + "                            from lpedoritem c                                    "
                + "                           where b.edoracceptno = c.edoracceptno                 "
                + "                             and b.approvedate = c.approvedate                   "
                + "                             and b.edorstate = c.edorstate                       "
                + "                             and c.edortype in ('AG', 'CT'))                     "
                + "                   group by b.approvedate, b.otherno) t                          "
                + "         where a.approvedate = date '" + date + "'                                   "
                + "             and a.otherno = t.otherno                                           "
                + "             and a.approvedate = t.approvedate                                   "
                + "             and a.approvetime = t.approvetime                                   "
                //保全优化
                + "  and (																"
                + "  exists(select 1 from lpedorapp lp where lp.edoracceptno=a.edoracceptno and lp.edorstate in('0', '4', '6', '8','7','9')  and lp.apptype in('14','18','19','22') and not exists((select distinct '1'  from lktransinfo lk where lk.funcflag='1004' and lk.edoracceptno= a.edoracceptno)) and not exists(select  1 from lpedoritem lpe where lpe.contno=lp.otherno  and lpe.edortype  in ('AG', 'CT') and lpe.edorstate!=lp.edorstate and lpe.approvedate>a.approvedate)) "
                + "  or exists(select 1 from lpedorapp lp where lp.edoracceptno=a.edoracceptno and lp.edorstate in('0') and not exists(select  1 from lpedoritem lpe where lpe.contno=lp.otherno  and lpe.edortype  in ('AG', 'CT') and lpe.edorstate!=lp.edorstate and lpe.approvedate>a.approvedate) and lp.apptype not in('14','18','19','22'))) "
                //保全优化
                + "             and exists (select 1                                                "
                + "                    from lccont                                                  "
                + "                   where a.otherno = contno                                      "
                + "                     and selltype in ('02','08','16','22','24','25')                           "
                + "                     and salechnl in ('3', '7')                                  "
                + "                     and bankcode = '03')                                        "
                + "             and a.edorstate in ('0', '4', '6', '8', '9','7')                        "
                + "             and exists (select 1                                                "
                + "                    from lpedoritem c                                            "
                + "                   where a.edoracceptno = c.edoracceptno                         "
                + "                     and a.approvedate = c.approvedate                           "
                + "                     and a.edorstate = c.edorstate                               "
                + "                     and c.edortype in ('AG', 'CT'))								"
                + " union "
                + " select '03' as busstype, "
                + " b.makedate, "
                + " a.contno,    "
                + " (select d.appntname from lcappnt d where d.contno = a.contno), "
                + " '0', "
                + " (select d.appntname from lcappnt d where d.contno = a.contno), "
                + " (select d.idtype from lcappnt d where d.contno = a.contno), "
                + " (select d.idno from lcappnt d where d.contno = a.contno), "
                + " '2', "
                + " nvl(sum(c.sumgetmoney),0) "
                + " from lccont a ,llcontstate b,ljaget c "
                + " where a.contno = b.contno  "
                + " and c.otherno=b.clmno"
                + " and a.selltype in ('02','08','16','22','24','25')  "
                + " and a.salechnl in ('3', '7')  "
                + " and a.bankcode = '03' "
                + " and b.statereason='04' "
                + " and b.makedate = date'" + date + "' "
                + " group by a.contno,a.managecom,a.conttype,a.agentcom,a.agentcode,b.clmno,b.makedate"
                + " union "
                + " select '03' as busstype, "
                + " a.confdate, "
                + " a.otherno, "
                + " decode(a.edorappname,'',(select d.appntname from lcappnt d where d.contno = a.otherno),a.edorappname), "
                + " case when(select distinct '1'  from lktransinfo lk where lk.funcflag='1004' and lk.edoracceptno= a.edoracceptno)='1' then '0' else decode(a.apptype, '14', '1','18', '1','19','1','22','1','0') end, "
                + " (select d.appntname from lcappnt d where d.contno = a.otherno), "
                + " (select d.idtype from lcappnt d where d.contno = a.otherno),  "
                + " (select d.idno from lcappnt d where d.contno = a.otherno), "
                + " decode(a.edorstate,'0','2', '4','3','6', '3','8','3','9','3','7','3',''), "
                + " nvl(abs(a.getmoney),0) getmoney  "
                + " from lpedorapp a, lpedoritem b, lccont c "
                + " where a.edoracceptno = b.edoracceptno "
                + " and b.contno = c.contno "
                + " and b.edortype in ('ZT', 'ST') "
                + " and exists(select 1 from lcpol lp where lp.contno=c.contno and lp.riskcode='6810') "
                + " and b.edorstate = '0' "
                + " and a.confdate = date '" + date + "' "
                + " and c.agenttype in (select code "
                + " from ldcode "
                + " where codetype = 'agenttype' "
                + " and othersign = '1') "
                + " union "
                + " select '03' as busstype, "
                + " a.confdate, "
                + " a.otherno, "
                + " decode(a.edorappname,'',(select d.appntname from lcappnt d where d.contno = a.otherno),a.edorappname),   "
                + " case when(select distinct '1'  from lktransinfo lk where lk.funcflag='1004' and lk.edoracceptno= a.edoracceptno)='1' then '0' else decode(a.apptype, '14', '1','18', '1','19','1','22','1','0') end,    "
                + " (select d.appntname from lcappnt d where d.contno = a.otherno),  "
                + " (select d.idtype from lcappnt d where d.contno = a.otherno),  "
                + " (select d.idno from lcappnt d where d.contno = a.otherno),    "
                + " decode(a.edorstate,'0','2', '4','3','6', '3','8','3','9','3','7','3',''), "
                + " nvl(abs(a.getmoney),0) getmoney  "
                + " from lpedorapp a, lpedoritem b, lccont c, lmcertifydes m, lacom n "
                + " where a.edoracceptno = b.edoracceptno "
                + " and b.contno = c.contno "
                + " and c.cardtypecode = m.certifycode "
                + " and c.agentcom = n.agentcom "
                + " and a.confdate = date '" + date + "' "
                + " and c.policyno is not null "
                + " and n.bankcode = '03' "
                + " and m.certifyname like '%借款人%' "
                + " and exists "
                + " (select 1 from lcaipcontregister r where r.contno = c.policyno) "
                + " and b.edortype in ('ZT', 'ST') "
                + " and b.edorstate = '0' "
                + " and c.agenttype in (select code "
                + " from ldcode "
                + " where codetype = 'agenttype' "
                + " and othersign = '1') "
                + " union "
                + " select '02' as busstype, "
                + " a.makedate, "
                + " a.contno, "
                + " (select d.appntname from lcappnt d where d.contno = a.contno),  "
                + " '0',    "
                + " (select d.appntname from lcappnt d where d.contno = a.contno),  "
                + " (select d.idtype from lcappnt d where d.contno = a.contno),  "
                + " (select d.idno from lcappnt d where d.contno = a.contno),   "
                + " '2', "
                + " 0 as getmoney  "
                + " from lccontstate a, lccont b "
                + " where a.contno = b.contno "
                + " and a.statetype='Terminate' "
                + " and exists(select 1 from lcpol lp where lp.contno=b.contno and lp.riskcode='6810') "
                + " and a.state='1' "
                + " and b.agenttype in (select code "
                + " from ldcode "
                + " where codetype = 'agenttype' "
                + " and othersign = '1') "
                + " and a.makedate = date '" + date + "' "
                + " union "
                + " select '02' as busstype, "
                + "  a.makedate, "
                + "  a.contno, "
                + " (select d.appntname from lcappnt d where d.contno = a.contno),    "
                + " '0',    "
                + " (select d.appntname from lcappnt d where d.contno = a.contno), "
                + " (select d.idtype from lcappnt d where d.contno = a.contno),  "
                + " (select d.idno from lcappnt d where d.contno = a.contno),   "
                + " '2', "
                + "  0 as getmoney  "
                + "  from lccontstate a,lccont c, lmcertifydes m, lacom n "
                + " where a.contno = c.contno "
                + " and c.cardtypecode = m.certifycode "
                + " and m.certifyname like '%借款人%' "
                + " and c.agentcom = n.agentcom "
                + " and c.policyno is not null "
                + " and n.bankcode = '03' "
                + " and exists "
                + " (select 1 from lcaipcontregister r where r.contno = c.policyno) "
                + " and a.statetype='Terminate' "
                + " and a.state='1' "
                + " and c.agenttype in (select code "
                + " from ldcode "
                + " where codetype = 'agenttype' "
                + " and othersign = '1') "
                + " and a.makedate = date '" + date + "' "
                + " union "
                + "select (case                                                           "
                + "                   when (select distinct '1'                                     "
                + "                           from lpedoritem c                                     "
                + "                          where c.edoracceptno = a.edoracceptno                  "
                + "                            and c.edortype = 'CT'                                "
                + "                            and c.standbyflag1 = '1') = '1' then                 "
                + "                    '01'                                                         "
                + "                   when (select distinct '1'                                     "
                + "                           from lpedoritem c                                     "
                + "                          where c.edoracceptno = a.edoracceptno                  "
                + "                            and c.edortype = 'CT'                                "
                + "                            and c.standbyflag1 = '2') = '1' then                 "
                + "                    '03'                                                         "
                + "                   when (select distinct '1'                                     "
                + "                           from lpedoritem c                                     "
                + "                          where c.edoracceptno = a.edoracceptno                  "
                + "                            and c.edortype = 'AG') = '1' then                    "
                + "                    '02'                                                         "
                + "                   else                                                          "
                + "                    ''                                                           "
                + "                 end) busstype,                                                  "
                + "                 a.approvedate,                                                  "
                + "                 a.otherno,                                                      "
                + "       decode(a.edorappname,'',(select d.appntname from lcappnt d where d.contno = a.otherno),a.edorappname),                                                 "
                + "		  case when(select distinct '1'  from lktransinfo lk where lk.funcflag='1004' and lk.edoracceptno= a.edoracceptno)='1' then '0' else decode(a.apptype, '14', '1','18', '1','19','1','22','1','0') end,"
                + "                 (select d.appntname from lcappnt d where d.contno = a.otherno), "
                + "                 (select d.idtype from lcappnt d where d.contno = a.otherno),    "
                + "                 (select d.idno from lcappnt d where d.contno = a.otherno),      "
                + "                decode(a.edorstate,'0','2', '4','3','6', '3','8','3','9','3','7','3',''),"
                + "                 nvl(abs(a.getmoney),0) getmoney                                        "
                + "            from lpedorapp a,                                                    "
                + "                 (select b.approvedate,                                          "
                + "                         b.otherno,                                              "
                + "                         max(b.approvetime) approvetime                          "
                + "                    from lpedorapp b                                             "
                + "                   where exists (select 1                                        "
                + "                            from lccont                                          "
                + "                           where b.otherno = contno                              "
                + "                             and selltype in ('02','08','16','22','24','25')                  "
                + "                             and salechnl in ('3', '7')                          "
                + "                             and bankcode = '03')                                "
                + "             and b.edorstate in ('0', '4', '6', '8', '9','7')                        "
                + "                     and exists                                                  "
                + "                   (select 1                                                     "
                + "                            from lpedoritem c                                    "
                + "                           where b.edoracceptno = c.edoracceptno                 "
                + "                             and b.approvedate = c.approvedate                   "
                + "                             and b.edorstate = c.edorstate                       "
                + "                             and c.edortype in ('AG', 'CT'))                     "
                + "                   group by b.approvedate, b.otherno) t                          "
                + "         where a.otherno = t.otherno                                           "
                + "             and a.approvedate = t.approvedate                                   "
                + "             and a.approvetime = t.approvetime                                   "
                //保全优化
                + "  and (																"
                + "  exists(select 1 from lpedorapp lp where lp.edoracceptno=a.edoracceptno and lp.edorstate in('0', '4', '6', '8','7','9')  and lp.apptype in('14','18','19','22') and not exists((select distinct '1'  from lktransinfo lk where lk.funcflag='1004' and lk.edoracceptno= a.edoracceptno)) and not exists(select  1 from lpedoritem lpe where lpe.contno=lp.otherno  and lpe.edortype  in ('AG', 'CT') and lpe.edorstate!=lp.edorstate and lpe.approvedate>a.approvedate)) "
                + "  or exists(select 1 from lpedorapp lp where lp.edoracceptno=a.edoracceptno and lp.edorstate in('0') and not exists(select  1 from lpedoritem lpe where lpe.contno=lp.otherno  and lpe.edortype  in ('AG', 'CT') and lpe.edorstate!=lp.edorstate and lpe.approvedate>a.approvedate) and lp.apptype not in('14','18','19','22'))) "
                //保全优化
                + "             and exists (select 1                                                "
                + "                    from lccont                                                  "
                + "                   where a.otherno = contno                                      "
                + "                     and selltype in ('02','08','16','22','24','25')                         "
                + "                     and salechnl in ('3', '7')                                  "
                + "                     and bankcode = '03')                                        "
                + "             and a.edorstate in ('0', '4', '6', '8', '9','7')                        "
                + "             and exists (select 1                                                "
                + "                    from lpedoritem c                                            "
                + "                   where a.edoracceptno = c.edoracceptno                         "
                + "                     and a.approvedate = c.approvedate                           "
                + "                     and a.edorstate = c.edorstate                               "
                + "                     and c.edortype in ('AG', 'CT'))								"
                + " and exists (SELECT  1 FROM LAYBTTRANSRESULT  LAY WHERE LAY.Uploadfiletype='TB' AND (LAY.BANKDEALFLAG!='Y' or LAY.BANKDEALFLAG is null)  AND LAY.CONTNO=a.otherno) "
                + " union "
                + " select '03' as busstype, "
                + " b.makedate, "
                + " a.contno,    "
                + " (select d.appntname from lcappnt d where d.contno = a.contno), "
                + " '0', "
                + " (select d.appntname from lcappnt d where d.contno = a.contno), "
                + " (select d.idtype from lcappnt d where d.contno = a.contno), "
                + " (select d.idno from lcappnt d where d.contno = a.contno), "
                + " '2', "
                + " nvl(sum(c.sumgetmoney),0)    "
                + " from lccont a ,llcontstate b,ljaget c  "
                + " where a.contno = b.contno  "
                + " and b.clmno = c.otherno "
                + " and a.selltype in ('02','08','16','22','24','25')  "
                + " and a.salechnl in ('3', '7')  "
                + " and a.bankcode = '03' "
                + " and b.statereason='04' "
                + " and exists (SELECT  1 FROM LAYBTTRANSRESULT  LAY WHERE LAY.Uploadfiletype='TB' AND (LAY.BANKDEALFLAG!='Y' or LAY.BANKDEALFLAG is null) AND LAY.CONTNO=a.contno) "
                + " group by a.contno,b.makedate "
                + " union "
                + " select '03' as busstype, "
                + " a.confdate, "
                + " a.otherno, "
                + " decode(a.edorappname,'',(select d.appntname from lcappnt d where d.contno = a.otherno),a.edorappname), "
                + " case when(select distinct '1'  from lktransinfo lk where lk.funcflag='1004' and lk.edoracceptno= a.edoracceptno)='1' then '0' else decode(a.apptype, '14', '1','18', '1','19','1','22','1','0') end, "
                + " (select d.appntname from lcappnt d where d.contno = a.otherno), "
                + " (select d.idtype from lcappnt d where d.contno = a.otherno),  "
                + " (select d.idno from lcappnt d where d.contno = a.otherno), "
                + " decode(a.edorstate,'0','2', '4','3','6', '3','8','3','9','3','7','3',''), "
                + " nvl(abs(a.getmoney),0) getmoney  "
                + " from lpedorapp a, lpedoritem b, lccont c "
                + " where a.edoracceptno = b.edoracceptno "
                + " and b.contno = c.contno "
                + " and b.edortype in ('ZT', 'ST') "
                + " and exists(select 1 from lcpol lp where lp.contno=c.contno and lp.riskcode='6810') "
                + " and b.edorstate = '0' "
                + " and c.agenttype in (select code "
                + " from ldcode "
                + " where codetype = 'agenttype' "
                + " and othersign = '1') "
                + " and exists (SELECT  1 FROM LAYBTTRANSRESULT  LAY WHERE LAY.Uploadfiletype='TB' AND (LAY.BANKDEALFLAG!='Y' or LAY.BANKDEALFLAG is null) AND LAY.CONTNO=a.otherno) "
                + " union "
                + " select '03' as busstype, "
                + " a.confdate, "
                + " a.otherno, "
                + " decode(a.edorappname,'',(select d.appntname from lcappnt d where d.contno = a.otherno),a.edorappname), "
                + " case when(select distinct '1'  from lktransinfo lk where lk.funcflag='1004' and lk.edoracceptno= a.edoracceptno)='1' then '0' else decode(a.apptype, '14', '1','18', '1','19','1','22','1','0') end,  "
                + " (select d.appntname from lcappnt d where d.contno = a.otherno),  "
                + " (select d.idtype from lcappnt d where d.contno = a.otherno),   "
                + " (select d.idno from lcappnt d where d.contno = a.otherno),  "
                + " decode(a.edorstate,'0','2', '4','3','6', '3','8','3','9','3','7','3',''), "
                + " nvl(abs(a.getmoney),0) getmoney  "
                + " from lpedorapp a, lpedoritem b, lccont c, lmcertifydes m, lacom n "
                + " where a.edoracceptno = b.edoracceptno "
                + " and b.contno = c.contno "
                + " and c.cardtypecode = m.certifycode "
                + " and c.agentcom = n.agentcom "
                + " and c.policyno is not null "
                + " and n.bankcode = '03' "
                + " and m.certifyname like '%借款人%' "
                + " and exists "
                + " (select 1 from lcaipcontregister r where r.contno = c.policyno) "
                + " and b.edortype in ('ZT', 'ST') "
                + " and b.edorstate = '0' "
                + " and c.agenttype in (select code "
                + " from ldcode "
                + " where codetype = 'agenttype' "
                + " and othersign = '1') "
                + " and exists (SELECT  1 FROM LAYBTTRANSRESULT  LAY WHERE LAY.Uploadfiletype='TB' AND (LAY.BANKDEALFLAG!='Y' or LAY.BANKDEALFLAG is null)AND LAY.CONTNO=a.otherno) "
                + " union "
                + " select '02' as busstype, "
                + " a.makedate, "
                + " a.contno, "
                + " (select d.appntname from lcappnt d where d.contno = a.contno),  "
                + " '0',    "
                + " (select d.appntname from lcappnt d where d.contno = a.contno),  "
                + " (select d.idtype from lcappnt d where d.contno = a.contno),    "
                + " (select d.idno from lcappnt d where d.contno = a.contno),   "
                + " '2', "
                + "  0 as getmoney  "
                + "  from lccontstate a, lccont b "
                + " where a.contno = b.contno "
                + " and a.statetype='Terminate' "
                + " and exists(select 1 from lcpol lp where lp.contno=b.contno and lp.riskcode='6810') "
                + " and a.state='1' "
                + " and b.agenttype in (select code "
                + " from ldcode "
                + " where codetype = 'agenttype' "
                + " and othersign = '1') "
                + " and exists (SELECT  1 FROM LAYBTTRANSRESULT  LAY WHERE LAY.Uploadfiletype='TB' AND (LAY.BANKDEALFLAG!='Y' or LAY.BANKDEALFLAG is null) AND LAY.CONTNO=b.contno) "
                + " union "
                + " select '02' as busstype, "
                + " a.makedate, "
                + " a.contno, "
                + " (select d.appntname from lcappnt d where d.contno = a.contno),  "
                + " '0',    "
                + " (select d.appntname from lcappnt d where d.contno = a.contno),  "
                + " (select d.idtype from lcappnt d where d.contno = a.contno),  "
                + " (select d.idno from lcappnt d where d.contno = a.contno),    "
                + " '2', "
                + " 0 as getmoney  "
                + " from lccontstate a,lccont c, lmcertifydes m, lacom n "
                + " where a.contno = c.contno "
                + " and c.cardtypecode = m.certifycode "
                + " and c.agentcom = n.agentcom "
                + " and c.policyno is not null "
                + " and n.bankcode = '03' "
                + " and m.certifyname like '%借款人%' "
                + " and exists "
                + " (select 1 from lcaipcontregister r where r.contno = c.policyno) "
                + " and a.statetype='Terminate' "
                + " and a.state='1' "
                + " and c.agenttype in (select code "
                + " from ldcode "
                + " where codetype = 'agenttype' "
                + " and othersign = '1') "
                + " and exists (SELECT  1 FROM LAYBTTRANSRESULT  LAY WHERE LAY.Uploadfiletype='TB' AND (LAY.BANKDEALFLAG!='Y' or LAY.BANKDEALFLAG is null) AND LAY.CONTNO=c.contno) "
                + " ) tt                         "
                + "   group by tt.busstype)                                                         "
                + "select '1118',                                                                   "
                + "       '03',                                                                     "
                + "       (select sum(a.allcount) from allinfo a),                                  "
                + "       (select sum(b.allmoney) from allinfo b),                                  "
                + "       nvl((select c.allcount from allinfo c where c.type='01'),0),              "
                + "       nvl((select d.allmoney from allinfo d where d.type='01'),0),              "
                + "       nvl((select e.allcount from allinfo e where e.type='02'),0),              "
                + "       nvl((select f.allmoney from allinfo f where f.type='02'),0),              "
                + "       nvl((select h.allcount from allinfo h where h.type='03'),0),              "
                + "       nvl((select j.allmoney from allinfo j where j.type='03') ,0)              "
                + "  from dual                                                                      ";

        System.out.println(date);
        System.out.println(s);
    }
    @Test
    public void test44(){
        String s ="{\"content\":{\"rtnValue\":\"<?xml version=\\\"1.0\\\" encoding=\\\"UTF-8\\\"?><Response><ResponseCode>07</ResponseCode><ResponseText>落库失败，异常</ResponseText></Response>\"},\"successFlag\":\"1\"}";
        String result = StringEscapeUtils.unescapeJava(s);
        System.out.println(result);
    }
}
