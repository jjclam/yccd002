package com.sinosoft.cloud.access.transform;


import com.sinosoft.cloud.access.entity.*;

import com.sinosoft.cloud.access.transformer.PojoTrans;
import com.sinosoft.cloud.rest.TradeInfo;
import com.sinosoft.lis.entity.LCContPojo;
import com.sinosoft.lis.entity.LCPolPojo;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2018/4/18.
 */
@RunWith(JUnit4.class)
public class PojoTransTest {

    private Body body;
    private TradeInfo tradeInfo;


    @Before
    public void init() {
//        准备测试数据
        TranData tranData = new TranData();
        body = new Body();
        tranData.setBody(body);
        LCCont lcCont = new LCCont();
        lcCont.setContNo("123");
        body.setLCCont(lcCont);

        LCPrems lcPrems = new LCPrems();
        LCPrem lcPrem = new LCPrem();
        lcPrem.setAppntNo("08908");
        ArrayList<LCPrem> lcPrems1 = new ArrayList<>();
        lcPrems1.add(lcPrem);
        lcPrems.setLCPrems(lcPrems1);
        body.setLCPrems(lcPrems);

        body.setContNo("jkl");

        LCAddresses lcAddresses = new LCAddresses();
        LCAddress lcAddress = new LCAddress();
        lcAddress.setCity("addr11111");
        LCAddress lcAddress1 = new LCAddress();
        lcAddress1.setCounty("addr222222");
        List<LCAddress> lcAddresses1 = new ArrayList<>();
        lcAddresses1.add(lcAddress);
        lcAddresses1.add(lcAddress1);
        lcAddresses.setLCAddresses(lcAddresses1);
        body.setLCAddresses(lcAddresses);

        BQQuery bqQuery = new BQQuery();
        bqQuery.setRiskCode("7058a");
//        body.setBQQuery(bqQuery);
        body.setLCBnfs(new LCBnfs());
        tradeInfo = new TradeInfo();
        LCContPojo lcContPojo = new LCContPojo();
        lcContPojo.setContNo("Contno");
        LCPolPojo lcPolPojo = new LCPolPojo();
        lcPolPojo.setRiskCode("1032");
        LCPolPojo lcPolPojo1 = new LCPolPojo();
        lcPolPojo1.setRiskCode("1033");
        List lcpolList = new ArrayList();
        lcpolList.add(lcPolPojo);
        lcpolList.add(lcPolPojo1);
        tradeInfo.addData(LCContPojo.class.getName(), lcContPojo);
        tradeInfo.addData(LCPolPojo.class.getName(), lcpolList);
    }

    @Test
    public void pojoTransTest3() throws Exception {
        PojoTrans.transPojos(body);
    }

    @Test
    public void pojoTransTranData() throws Exception {
        PojoTrans.transTranData(tradeInfo, body);
    }
}
