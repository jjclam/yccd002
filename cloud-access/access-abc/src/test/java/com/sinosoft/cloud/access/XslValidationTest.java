package com.sinosoft.cloud.access;

import com.sinosoft.cloud.access.transformer.XsltTrans;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * @Description
 * @Author:zhaoshulei
 * @Date:2018/4/28
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = ABCApplication.class)
public class XslValidationTest {

    @Test
    public void xsltTrans(){
        XsltTrans xsltTrans = new XsltTrans();
        xsltTrans.setAccessName("abc");
        String orgXml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
                "<ABCB2I>\n" +
                "    <Header>\n" +
                "        <TransDate>20180403</TransDate>\n" +
                "        <TransTime>151420</TransTime>\n" +
                "        <TransCode>1002</TransCode>\n" +
                "        <ProvCode>15</ProvCode>\n" +
                "        <SerialNo>159456654632659</SerialNo>\n" +
                "        <BankCode>05</BankCode>\n" +
                "        <CorpNo>1118</CorpNo>\n" +
                "        <TransSide>1</TransSide>\n" +
                "        <EntrustWay>22</EntrustWay>\n" +
                "        <BranchNo>4742</BranchNo>\n" +
                "        <Tlid>2eaf</Tlid>\n" +
                "    </Header>\n" +
                "    <App>\n" +
                "        <Req>\n" +
                "            <AppNo>159456654632659</AppNo>\n" +
                "            <Appl>\n" +
                "                <IDKind>110027</IDKind>\n" +
                "                <IDCode>834749668765409</IDCode>\n" +
                "                <BeginDate />\n" +
                "                <InvalidDate>20361219</InvalidDate>\n" +
                "                <Name>杨将</Name>\n" +
                "                <Sex>1</Sex>\n" +
                "                <Birthday>19580101</Birthday>\n" +
                "                <Country>156</Country>\n" +
                "                <Address>河北省石家庄市２１－６</Address>\n" +
                "                <Prov>河北省</Prov>\n" +
                "                <City>石家庄市</City>\n" +
                "                <Zone>石家庄市辖区</Zone>\n" +
                "                <ZipCode>264000</ZipCode>\n" +
                "                <Email>122364598@qq.com</Email>\n" +
                "                <Phone />\n" +
                "                <Mobile>15686632594</Mobile>\n" +
                "                <OtherConnect />\n" +
                "                <ShortMsg />\n" +
                "                <FixIncome>1</FixIncome>\n" +
                "                <AnnualIncome>10000000000.00s</AnnualIncome>\n" +
                "                <Company />\n" +
                "                <JobType>1</JobType>\n" +
                "                <JobCode>0001001</JobCode>\n" +
                "                <Notice />\n" +
                "                <CustSource>1</CustSource>\n" +
                "                <RelaToInsured>01</RelaToInsured>\n" +
                "            </Appl>\n" +
                "            <Insu>\n" +
                "                <IDKind>110027</IDKind>\n" +
                "                <IDCode>834749668765409</IDCode>\n" +
                "                <BeginDate />\n" +
                "                <ValidDate>20361226</ValidDate>\n" +
                "                <Name>杨将</Name>\n" +
                "                <Sex>1</Sex>\n" +
                "                <Birthday>19580101</Birthday>\n" +
                "                <Country>156</Country>\n" +
                "                <Address>石家庄 1895/35/301</Address>\n" +
                "                <Prov>河北省</Prov>\n" +
                "                <City>石家庄</City>\n" +
                "                <Zone>石家庄市辖区</Zone>\n" +
                "                <ZipCode>200442</ZipCode>\n" +
                "                <Email />\n" +
                "                <Phone />\n" +
                "                <Mobile>15686632594</Mobile>\n" +
                "                <JobType>1</JobType>\n" +
                "                <JobCode>0001001</JobCode>\n" +
                "                <Company />\n" +
                "                <AnnualIncome />\n" +
                "                <Tall />\n" +
                "                <Weight />\n" +
                "                <IsRiskJob />\n" +
                "                <HealthNotice>0</HealthNotice>\n" +
                "                <Notice />\n" +
                "                <CreditCard />\n" +
                "                <CreditType />\n" +
                "                <Immaturity />\n" +
                "            </Insu>\n" +
                "            <Bnfs>\n" +
                "                <Count>2</Count>\n" +
                "                <Type1>1</Type1>\n" +
                "                <Name1>周芝如</Name1>\n" +
                "                <Sex1>1</Sex1>\n" +
                "                <Birthday1>19930101</Birthday1>\n" +
                "                <IDCode1>150302199301013546</IDCode1>\n" +
                "                <BeginDate1>20161226</BeginDate1>\n" +
                "                <InvalidDate1>20261226</InvalidDate1>\n" +
                "                <IDKind1>110001</IDKind1>\n" +
                "                <RelationToInsured1>05</RelationToInsured1>\n" +
                "                <Sequence1>1</Sequence1>\n" +
                "                <Prop1>100</Prop1>\n" +
                "                <Phone1/>\n" +
                "                <Country1>156</Country1>\n" +
                "                <Address1>四川省广元市利州区【改】财经路8号</Address1>\n" +
                "                <Prov1>四川省</Prov1>\n" +
                "                <City1>广元市</City1>\n" +
                "                <Zone1>利州区【改】</Zone1>\n" +
                "                <Type2/>\n" +
                "                <Name2/>\n" +
                "                <Sex2/>\n" +
                "                <Birthday2/>\n" +
                "                <IDCode2/>\n" +
                "                <BeginDate2/>\n" +
                "                <InvalidDate2/>\n" +
                "                <IDKind2/>\n" +
                "                <RelationToInsured2/>\n" +
                "                <Sequence2>0</Sequence2>\n" +
                "                <Prop2>0</Prop2>\n" +
                "                <Phone2/>\n" +
                "                <Country2/>\n" +
                "                <Address2/>\n" +
                "                <Prov2/>\n" +
                "                <City2/>\n" +
                "                <Zone2/>\n" +
                "                <Type3/>\n" +
                "                <Name3/>\n" +
                "                <Sex3/>\n" +
                "                <Birthday3/>\n" +
                "                <IDCode3/>\n" +
                "                <BeginDate3/>\n" +
                "                <InvalidDate3/>\n" +
                "                <IDKind3/>\n" +
                "                <RelationToInsured3/>\n" +
                "                <Sequence3>0</Sequence3>\n" +
                "                <Prop3>0</Prop3>\n" +
                "                <Phone3/>\n" +
                "                <Country3/>\n" +
                "                <Address3/>\n" +
                "                <Prov3/>\n" +
                "                <City3/>\n" +
                "                <Zone3/>\n" +
                "            </Bnfs>\n" +
                "            <Risks>\n" +
                "                <Prodset />\n" +
                "                <RiskCode>1032</RiskCode>\n" +
                "                <Name>xxx财富世享年金保险计划</Name>\n" +
                "                <PassWord />\n" +
                "                <Share>10.0</Share>\n" +
                "                <Prem />\n" +
                "                <Amnt />\n" +
                "                <PayType>1</PayType>\n" +
                "                <PayDueType>4</PayDueType>\n" +
                "                <PayDueDate>1000</PayDueDate>\n" +
                "                <InsuDueType>5</InsuDueType>\n" +
                "                <InsuDueDate>100</InsuDueDate>\n" +
                "                <BonusGetMode />\n" +
                "                <AutoPayForFlag />\n" +
                "                <GetYear />\n" +
                "                <GetYearFlag />\n" +
                "                <FullBonusGetMode />\n" +
                "            </Risks>\n" +
                "            <Addt>\n" +
                "                <Count>2</Count>\n" +
                "                <RiskCode1>1033</RiskCode1>\n" +
                "                <InsuCode1 />\n" +
                "                <Name1>xxx附加财富宝年金保险（万能型）</Name1>\n" +
                "                <Share1>1000</Share1>\n" +
                "                <AutoPayFlag1 />\n" +
                "                <Prem1 />\n" +
                "                <Amnt1 />\n" +
                "                <PayType1>1</PayType1>\n" +
                "                <PayDueType1 />\n" +
                "                <PayDueDate1 />\n" +
                "                <InsuDueType1>5</InsuDueType1>\n" +
                "                <InsuDueDate1>100</InsuDueDate1>\n" +
                "            </Addt>\n" +
                "            <Base>\n" +
                "                <SpecArranged />\n" +
                "                <ArugeFlag />\n" +
                "                <ArbteName />\n" +
                "                <ConAccName>杨将</ConAccName>\n" +
                "                <ConAccNo>6373729633756663001</ConAccNo>\n" +
                "                <ApplGetAccNo />\n" +
                "                <InsuGetAccNo />\n" +
                "                <ApplyDate>20180403</ApplyDate>\n" +
                "                <PolicyApplySerial />\n" +
                "                <VchType>8008</VchType>\n" +
                "                <VchName>xxxxxx</VchName>\n" +
                "                <VchNo />\n" +
                "                <Saler>姚笛</Saler>\n" +
                "                <BranchCertNo>637388384894912344</BranchCertNo>\n" +
                "                <SalerCertNo>002</SalerCertNo>\n" +
                "                <BranchName>中国xxxxxx股份有限公司烟台黄务支行</BranchName>\n" +
                "                <RenewPayType />\n" +
                "                <ApplGetAccName />\n" +
                "                <InsuGetAccName />\n" +
                "            </Base>\n" +
                "            <Reserve1>家庭年收入</Reserve1>\n" +
                "            <Reserve2>100000000.00</Reserve2>\n" +
                "            <Reserve3>未成年人风险保额</Reserve3>\n" +
                "            <Reserve4>0</Reserve4>\n" +
                "        </Req>\n" +
                "    </App>\n" +
                "</ABCB2I>\n";
        String result = xsltTrans.transform(orgXml);
        System.out.println(result);
    }
}
