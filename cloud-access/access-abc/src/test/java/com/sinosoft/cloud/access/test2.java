package com.sinosoft.cloud.access;

import com.sinosoft.cloud.access.net.SocketClient;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;

public class test2 {

    public String go(String xmlInfo) throws UnknownHostException, IOException {

        // 向服务器端发送请求，服务器IP地址和服务器监听的端口号

        Socket client = new Socket("127.0.0.1", 8000);

        // 通过printWriter 来向服务器发送消息
        PrintWriter printWriter = new PrintWriter(client.getOutputStream());
        System.out.println("连接已建立...");

        // 发送消息
        printWriter.println(xmlInfo);

        printWriter.flush();

        // InputStreamReader是低层和高层串流之间的桥梁
        // client.getInputStream()从Socket取得输入串流
        InputStreamReader streamReader = new InputStreamReader(client.getInputStream());

        // 链接数据串流，建立BufferedReader来读取，将
        //BufferReader链接到InputStreamReder
        BufferedReader reader = new BufferedReader(streamReader);
        String advice = reader.readLine();

        reader.close();
        return advice;
    }

    public static void main(String[] args) throws UnknownHostException, IOException {


        // 拼接xml报文
    /*
     * <?xml version="1.0" encoding="GB2312" standalone="yes" ?> <TX>
     * <REQUEST_SN>请求序列码</REQUEST_SN> <CUST_ID>客户号</CUST_ID>
     * <USER_ID>操作员号</USER_ID> <PASSWORD>密码</PASSWORD>
     * <TX_CODE>6W0201</TX_CODE> <LANGUAGE>CN</LANGUAGE> <TX_INFO>
     * </TX_INFO> </TX>
     */
        StringBuffer sb = new StringBuffer();
        // sb.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
        sb.append("<ABCB2I><Header><TransDate>20180718</TransDate><TransTime>172151</TransTime><TransCode>1013</TransCode><CorpNo>1118</CorpNo><TransSide>1</TransSide><EntrustWay>11</EntrustWay><ProvCode>16</ProvCode><BranchNo>2820</BranchNo><SerialNo>703028219638</SerialNo><Tlid>281k</Tlid><BankCode>03</BankCode></Header><App><Req><PolicyNo>201541110020037231</PolicyNo><PrintCode>201301428944</PrintCode><PolicyPwd></PolicyPwd><ClientName>魏慧敏</ClientName><IdKind>110001</IdKind><IdCode>411121197106256503</IdCode><PayAcc>6228480989472621071</PayAcc><Amt>10000.00</Amt><GetAmt>11361.61</GetAmt><PicFileName></PicFileName><BusiType>03</BusiType></Req></App></ABCB2I>");
        // 提交请求
        String utf8 = new String(sb.toString().getBytes("UTF-8"));
        String unicode = new String(utf8.getBytes(), "UTF-8");
        String gbk = new String(unicode.getBytes("GBK"));
        test2 c = new test2();
        String advice = c.go(gbk);
        System.out.println("接收到服务器的消息 ：" + advice);
    }
}