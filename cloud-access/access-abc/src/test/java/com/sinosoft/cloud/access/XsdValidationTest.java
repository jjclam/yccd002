package com.sinosoft.cloud.access;

import com.sinosoft.cloud.access.transformer.XsdValidator;
import org.junit.Test;
import org.springframework.core.io.ClassPathResource;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;

/**
 * xsd校验测试类.
 *
 * @Author: cuiguangdong
 * @Date: 2018/4/9 13:53
 * @Description: 测试xsd校验
 */
public class XsdValidationTest {

    @Test
    public void xsdValidationTest() throws Exception {

        ClassPathResource classPathResource = new ClassPathResource("1002In.xml");
        StringBuffer str = new StringBuffer(1024);
        FileInputStream fs = new FileInputStream(classPathResource.getFile());
        InputStreamReader isr = new InputStreamReader(fs);
        BufferedReader br = new BufferedReader(isr);
        String data = "";
        while ((data = br.readLine()) != null) {
            str.append(data);
        }
        String xmlFile = str.toString();


        XsdValidator xsdValidator = new XsdValidator();

        String result = xsdValidator.validate(xmlFile, "rmp");
        System.out.println("错误信息：" + result);

    }
}
