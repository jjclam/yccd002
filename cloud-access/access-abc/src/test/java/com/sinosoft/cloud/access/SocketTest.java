package com.sinosoft.cloud.access;

import com.sinosoft.cloud.access.abc.net.AbcCoder;
import com.sinosoft.cloud.access.crypto.AesUtil;
import io.netty.bootstrap.Bootstrap;
import io.netty.buffer.ByteBuf;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.util.ReferenceCountUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.ClassPathResource;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.Hashtable;

public class SocketTest extends ChannelInboundHandlerAdapter {
    private static Logger logger = LoggerFactory.getLogger(SocketTest.class);

    protected static final Hashtable<String, byte[]> messageHashTable = new Hashtable<>();
    private static final String KEY = "KEY";

    // 接收server端的消息，并打印出来
    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
//        logger.info("HelloClientIntHandler.channelRead");
        try {
            if (msg instanceof ByteBuf) {
                ByteBuf in = (ByteBuf) msg;
                byte[] tempBytes = messageHashTable.get(KEY);
                if (tempBytes == null) {
                    tempBytes = new byte[0];
                }
                byte[] channelReadBytes = new byte[in.readableBytes()];
                in.readBytes(channelReadBytes);
                String channelReadMsg = new String(channelReadBytes,Charset.defaultCharset());
                logger.info("channelRead 报文:" + channelReadMsg);

                byte[] finalBytes = new byte[tempBytes.length + channelReadBytes.length];
                System.arraycopy(tempBytes,0,finalBytes,0,tempBytes.length);
                System.arraycopy(channelReadBytes,0,finalBytes,tempBytes.length,channelReadBytes.length);
                messageHashTable.remove(KEY);
                messageHashTable.put(KEY, finalBytes);
            }
        } finally {
            ReferenceCountUtil.release(msg);
        }

        ctx.flush();
    }

    @Override
    public void channelReadComplete(ChannelHandlerContext ctx) {

        byte[] completeMsg = messageHashTable.get(KEY);
        if (completeMsg == null) {
            logger.error("在channelReadComplete中收到消息为空:" + completeMsg);
            return;
        }
        String channelReadCompleteMsg = new String(completeMsg,Charset.defaultCharset());
        logger.info("channelReadComplete 报文:" + channelReadCompleteMsg);


        int expBodySize = getBodySizeByHead(channelReadCompleteMsg);
        int actuBodySize = getBodySize(channelReadCompleteMsg);

        logger.info("期望报文长度:" + expBodySize + ";实际报文长度:" + actuBodySize);
        if (expBodySize - actuBodySize == 0) {
            String substring = channelReadCompleteMsg.substring(73);
            if (substring.startsWith("<TXLife>") || substring.startsWith("<ABCB2I>")
                    || substring.startsWith("<?xml")){
                System.out.println("Server Said: " + substring);
            } else {
                String decode = AesUtil.decode(substring, "ZMfoXerMMpqczx0D", "GBK");
                System.out.println("Server Said: " + decode);
            }
            ctx.close();
        } else {
            ctx.flush();
            return;
        }
        ctx.flush();
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {
        // Close the connection when an exception is raised.
        logger.error("netty发生错误：", cause);

        cause.printStackTrace();
        ctx.close();
    }

    // 连接成功后，向server发送消息
    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        ClassPathResource xml = new ClassPathResource(getXmlName());
        String strXml = "";
        String encoderXml = "";
        try {
            StringBuffer str = new StringBuffer(1024);
            FileInputStream fs = new FileInputStream(xml.getFile());
            InputStreamReader isr;
            isr = new InputStreamReader(fs);
            BufferedReader br = new BufferedReader(isr);
            try {
                String data = "";
                while ((data = br.readLine()) != null) {
                    str.append(data + "");
                }
                strXml = str.toString();
                if (strXml.startsWith("<ABCB2I>")){
                    encoderXml = strXml;
                }else {
                    encoderXml = AesUtil.encode(strXml, "ZMfoXerMMpqczx0D", "GBK");
                }
                AbcCoder abcCoder = new AbcCoder();
                encoderXml = abcCoder.encoder(encoderXml);//rs
                System.out.println("期望的报文长度："+Integer.parseInt(encoderXml.substring(4,12).trim()));
                System.out.println("实际报文长度："+encoderXml.substring(73).getBytes().length);
            } catch (Exception e) {
                str.append(e.toString());
            }
        } catch (IOException es) {
        }
//        String msg = "Are you ok?";
//        encoderXml = encoderXml.substring(0,100);
        System.out.println(encoderXml);
        ByteBuf encoded = ctx.alloc().buffer(4 * encoderXml.length());
        encoded.writeBytes(encoderXml.getBytes());
        ctx.write(encoded);
        ctx.flush();
    }


    public void connect(String host, int port) throws Exception {
        EventLoopGroup workerGroup = new NioEventLoopGroup();

        try {
            Bootstrap b = new Bootstrap();
            b.group(workerGroup);
            b.channel(NioSocketChannel.class);
            b.option(ChannelOption.SO_KEEPALIVE, true);
            b.handler(new ChannelInitializer<SocketChannel>() {
                @Override
                public void initChannel(SocketChannel ch) throws Exception {
                    ch.config().setRecvByteBufAllocator(new FixedRecvByteBufAllocator(2048000)); //set  buf size
                    ch.pipeline().addLast(new SocketTest());
                }
            });

            // Start the client.
            ChannelFuture f = b.connect(host, port).sync();

            // Wait until the connection is closed.
            f.channel().closeFuture().sync();
        } finally {
            workerGroup.shutdownGracefully();
        }

    }

    public int getBodySizeByHead(String msg) {
        if (msg != null) {
            String strSize = msg.substring(4, 12);
            return Integer.parseInt(strSize.trim());
        }
        return 0;
    }

    public int getBodySize(String msg) {

        if (msg == null) {
            logger.error("传入消息为空,请检查消息");
            throw new RuntimeException("传入消息为空,请检查消息");
        }

        if (msg.length() < 74) {
            logger.error("消息传入长度不对! xxx消息报文头应该有74,实际为:" + msg.getBytes().length);
            return 0;
//            throw new RuntimeException("消息传入长度不对! xxx消息报文头应该有74,实际为:" + msg.length());
        }

        logger.info("getBodySize 报文总长度:" + msg.getBytes().length);
        //xxx传输的报文切掉前面的73个字符
        if (msg.startsWith("X")) {
            return msg.substring(73).getBytes().length;
        } else {
            logger.error("报文传入错误,xxx报文以X开头,实际为:" + msg);
            throw new RuntimeException("报文传入错误,xxx报文以X开头,实际为:" + msg);
        }
    }
    public String getXmlName(){
        //return "A0026.xml";
        return "1010In.xml";
        //return "1002In.xml";
    }

    public static void main(String[] args) throws Exception {

        SocketTest client = new SocketTest();
  //     client.connect("10.0.58.23", 8000);
        client.connect("192.168.11.54", 8001);
//        client.connect("10.10.32.97", 9000);
  //       client.connect("127.0.0.1", 8000);
//        client.connect("10.10.68.228", 8000);
//        client.connect("10.10.68.169", 8000);
  //     client.connect("10.10.68.168", 8000);
 //       client.connect("10.10.68.210", 8000);
 //       client.connect("10.10.68.176", 8101);
  //      client.connect("10.0.59.98", 8011);
    }

}
