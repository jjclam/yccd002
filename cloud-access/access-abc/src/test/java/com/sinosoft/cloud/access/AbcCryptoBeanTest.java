package com.sinosoft.cloud.access;

import com.sinosoft.cloud.access.abc.crypto.AbcCryptoBean;
import com.sinosoft.cloud.access.configuration.AccessConfiguration;
import com.sinosoft.cloud.access.crypto.AesUtil;
import com.xiaoleilu.hutool.util.HexUtil;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

@SpringBootTest(classes = AccessConfiguration.class)
public class AbcCryptoBeanTest extends AbstractJUnit4SpringContextTests {


    @Autowired
    private AbcCryptoBean abcCryptoBean;

//    String x = "";


    byte[] keys = null;

//    @Before
//    public void getKey() {
//        //随机生成密钥
//        byte[] key = SecureUtil.generateKey(SymmetricAlgorithm.AES.getValue()).getEncoded();
//        keys = key;
//        abcCryptoBean.setKey(new String(keys));
//    }


    @Test
    public void test3_encryt() throws NoSuchPaddingException, InvalidKeyException, UnsupportedEncodingException, IllegalBlockSizeException, BadPaddingException, NoSuchAlgorithmException, InvalidAlgorithmParameterException {
        String raw = AesUtil.encode("<ABCB2I><Header><TransDate>20140217</TransDate><TransTime>100418</TransTime><TransCode>1006</TransCode><SerialNo>710000003038</SerialNo><BankCode></BankCode><CorpNo>4518</CorpNo><TransSide>1</TransSide><EntrustWay>11</EntrustWay><ProvCode>11</ProvCode><BranchNo>0102</BranchNo></Header><App><Req><Appl><Name>郑新芳</Name><IDKind>110001</IDKind><IDCode>142424198612094514</IDCode><Phone>01070513942</Phone><CellPhone></CellPhone><Address>阿打发打发点发大水发第三方</Address><ZipCode>100073</ZipCode></Appl><PolicyApplyNo>1006080123000458</PolicyApplyNo><RiskCode>BELA</RiskCode><ProdCode>110001</ProdCode><LoanContact></LoanContact><SpecialRate></SpecialRate><Prem>5000000.00</Prem><AccNo>6228450018010609178</AccNo><Reserve1></Reserve1></Req></App></ABCB2I>         ", abcCryptoBean.getKey(),"GBK");
        System.out.println(HexUtil.encodeHexStr(raw));
    }

    @Test
    public void test4_decryt() throws NoSuchPaddingException, InvalidKeyException, UnsupportedEncodingException, IllegalBlockSizeException, BadPaddingException, NoSuchAlgorithmException, InvalidAlgorithmParameterException {
        String raw = AesUtil.decode("CA4404208D0B6CB09F9FA383CB1EC875D7B4E709E6A71BDCF7AF2E8AD63DA4002C8B237D6DEC807DFEA5FFC45433F8F96F7A914FA901C23049D9DB0932BE04C8072A5FB1C196100008755AFFFEB1787254AE97DA7758AC1B0040032720B2146443651D065ECD1F2416128385345DB2299F96B13686EF8CCE99E500581A9B2576E7AD44311838CD1C467598133712938BF1CEAFE80587785480B62A20FD79B9A0E556DAE1BA52DD760D7AF6FF23B1EB45D78986BCA6A8B8DCB974036A34AA7E154503240A3F7902DB3B97B561C572C78BEA3F328261C0903353B3CB7B97CE1DEE1FA2342E8C97D92E6FB669C75A6B2EE5E0CE532D88EE197D271F4CB52F5F24019BB4494820481F9422C7603BD874A7568D5479A9B317F149670075FD7FD277AA790D2E79AA5C5060B14DFD3E71581F387C9976842DB8EB1B71B95542423203FF0583055B02584E0A9A1B5977B803EDC86801098177E56D0108E92D8DDDD0EE7B1F695C3015D00573910E9651656892B6747A3ADE3D6C19D9BAF23C0CE6FD696D93EB28BEA705C7E6D2E3A3005C80A82E38A8DC0F22ADE60B86F2F5F258DC157286365EDAC30B2ECEC9D726F148C57C6C0419529674E7F4C240A63482310E54D293A667793C6055F257B1B9C3E3B9C0A5BBC94F49F34903914A121B823D4DB30DD62CDCD1173D5F06CAE90FF5E99455B8A6C0C73B17E3306CFEC0C66987EF564E0C6A6672AF103AD28C5C3052C08786C423A5643D3F01471837F6068E7B98D280DF7476792BB9C8160826A3E5EFA9A4F4BAB5EAF8E5C054BDD6395FB7AB8D7436CC6D4E44E6DBC8E3CF50F7DB4AEF38F3671721B45DBB8A9419CBDD3A6FD41ABA27B5DD1D1D384EC9C4FDBB2809ED5696D27602DDAA7A4DFA690BAF1FE9D35955BD3632D6A5491F988AC906DA3C962709A8C3A14664CC9ADF8878360E8EEE27D72CDF0741E581FF5886D1F8FDB0AB77E6D2D0AC6587E68EE23E06D1FA1A970573B2B27DE0952231B4F8B726108C6476859C940FDD1090289ECE43515C6455B29B97B0E655DE07E13076D4A6FE590D17B8AA742EA51EA5E9C8D00F88ACFBC13ABA", abcCryptoBean.getKey(),"GBK");
        System.out.println(new String(raw));
//        System.out.println(HexUtil.decodeHexStr(raw, Charset.defaultCharset()));
    }




}