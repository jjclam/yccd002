package com.sinosoft.cloud.access;
import com.sinosoft.cloud.access.abc.net.AbcCoder;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;

import static io.netty.buffer.Unpooled.buffer;

public class name {
    private final static String ENCODER_FORMAT = "X1.000000000    111821000                                       000000000";
    public String go(String xmlInfo) throws UnknownHostException, IOException {

        // 向服务器端发送请求，服务器IP地址和服务器监听的端口号

        Socket client = new Socket("10.10.68.210", 8000);

        // 通过printWriter 来向服务器发送消息
        PrintWriter printWriter = new PrintWriter(client.getOutputStream());
        System.out.println("连接已建立...");
        ChannelHandlerContext ctx =null;
        ByteBuf encoded =buffer(4 * xmlInfo.length());
        encoded.writeBytes(xmlInfo.getBytes());
        // 发送消息
        printWriter.println(xmlInfo);

        printWriter.flush();

        // InputStreamReader是低层和高层串流之间的桥梁
        // client.getInputStream()从Socket取得输入串流
        InputStreamReader streamReader = new InputStreamReader(client.getInputStream());

        // 链接数据串流，建立BufferedReader来读取，将
        //BufferReader链接到InputStreamReder
        BufferedReader reader = new BufferedReader(streamReader);
        String advice = reader.readLine();

        reader.close();
        return advice;
    }

    public static void main(String[] args) throws UnknownHostException, IOException {


        // 拼接xml报文
    /*
     * <?xml version="1.0" encoding="GB2312" standalone="yes" ?> <TX>
     * <REQUEST_SN>请求序列码</REQUEST_SN> <CUST_ID>客户号</CUST_ID>
     * <USER_ID>操作员号</USER_ID> <PASSWORD>密码</PASSWORD>
     * <TX_CODE>6W0201</TX_CODE> <LANGUAGE>CN</LANGUAGE> <TX_INFO>
     * </TX_INFO> </TX>
     */
        String s ="<ABCB2I>\n" +
                "    <Header>\n" +
                "        <TransDate>20180713</TransDate>\n" +
                "        <TransTime>160347</TransTime>\n" +
                "        <TransCode>1002</TransCode>\n" +
                "        <ProvCode>26</ProvCode>\n" +
                "        <SerialNo>701029607108</SerialNo>\n" +
                "        <CorpNo>1118</CorpNo>\n" +
                "        <TransSide>1</TransSide>\n" +
                "        <EntrustWay>11</EntrustWay>\n" +
                "        <BranchNo>1930</BranchNo>\n" +
                "        <Tlid>1e2k</Tlid>\n" +
                "        <BankCode>03</BankCode>\n" +
                "    </Header>\n" +
                "    <App>\n" +
                "        <Req>\n" +
                "            <AppNo>701029607108</AppNo>\n" +
                "            <Appl>\n" +
                "                <IDKind>110001</IDKind>\n" +
                "                <IDCode>610113196909250086</IDCode>\n" +
                "                <BeginDate/>\n" +
                "                <InvalidDate>20270813</InvalidDate>\n" +
                "                <Name>柴宝琴</Name>\n" +
                "                <Sex>1</Sex>\n" +
                "                <Birthday>19690925</Birthday>\n" +
                "                <Country>156</Country>\n" +
                "                <Address>唐延路31号xxx陕西省分行1702</Address>\n" +
                "                <Prov>陕西省</Prov>\n" +
                "                <City>西安市</City>\n" +
                "                <Zone>雁塔区</Zone>\n" +
                "                <ZipCode>710069</ZipCode>\n" +
                "                <Email/>\n" +
                "                <Phone/>\n" +
                "                <Mobile>13991821038</Mobile>\n" +
                "                <OtherConnect/>\n" +
                "                <ShortMsg/>\n" +
                "                <FixIncome>1</FixIncome>\n" +
                "                <AnnualIncome>200000</AnnualIncome>\n" +
                "                <Company/>\n" +
                "                <JobType>1</JobType>\n" +
                "                <JobCode>0001001</JobCode>\n" +
                "                <Notice/>\n" +
                "                <CustSource>0</CustSource>\n" +
                "                <RelaToInsured>05</RelaToInsured>\n" +
                "            </Appl>\n" +
                "            <Insu>\n" +
                "                <IDKind>110001</IDKind>\n" +
                "                <IDCode>610112199507302522</IDCode>\n" +
                "                <BeginDate/>\n" +
                "                <ValidDate>20220702</ValidDate>\n" +
                "                <Name>张嘉益</Name>\n" +
                "                <Sex>1</Sex>\n" +
                "                <Birthday>19950730</Birthday>\n" +
                "                <Country>156</Country>\n" +
                "                <Address>西安市未央区二府庄小区1楼3门3层1号</Address>\n" +
                "                <Prov>陕西省</Prov>\n" +
                "                <City>西安市</City>\n" +
                "                <Zone>未央区</Zone>\n" +
                "                <ZipCode>710000</ZipCode>\n" +
                "                <Email/>\n" +
                "                <Phone/>\n" +
                "                <Mobile>13991821038</Mobile>\n" +
                "                <JobType>1</JobType>\n" +
                "                <JobCode>1201002</JobCode>\n" +
                "                <Company/>\n" +
                "                <AnnualIncome>0</AnnualIncome>\n" +
                "                <Tall>165</Tall>\n" +
                "                <Weight>50</Weight>\n" +
                "                <IsRiskJob>0</IsRiskJob>\n" +
                "                <HealthNotice>0</HealthNotice>\n" +
                "                <Notice>0</Notice>\n" +
                "                <CreditCard/>\n" +
                "                <CreditType/>\n" +
                "                <Immaturity>0</Immaturity>\n" +
                "                <list1/>\n" +
                "                <list2/>\n" +
                "                <list3/>\n" +
                "                <list4/>\n" +
                "                <list5/>\n" +
                "                <list6/>\n" +
                "            </Insu>\n" +
                "            <Bnfs>\n" +
                "                <Count>2</Count>\n" +
                "                <Type1>1</Type1>\n" +
                "                <Name1>柴宝琴</Name1>\n" +
                "                <Sex1>1</Sex1>\n" +
                "                <Birthday1>19690925</Birthday1>\n" +
                "                <IDCode1>610113196909250086</IDCode1>\n" +
                "                <BeginDate1/>\n" +
                "                <InvalidDate1>20270813</InvalidDate1>\n" +
                "                <IDKind1>110001</IDKind1>\n" +
                "                <RelationToInsured1>05</RelationToInsured1>\n" +
                "                <Sequence1>1</Sequence1>\n" +
                "                <Prop1>60</Prop1>\n" +
                "                <Phone1/>\n" +
                "                <Country1>156</Country1>\n" +
                "                <Address1>西安市未央区二府庄小区</Address1>\n" +
                "                <Prov1>陕西省</Prov1>\n" +
                "                <City1>西安市</City1>\n" +
                "                <Zone1>雁塔区</Zone1>\n" +
                "                <Type2>1</Type2>\n" +
                "                <Name2>张伟</Name2>\n" +
                "                <Sex2>0</Sex2>\n" +
                "                <Birthday2>19681110</Birthday2>\n" +
                "                <IDCode2>61011219681110253X</IDCode2>\n" +
                "                <BeginDate2/>\n" +
                "                <InvalidDate2>20270813</InvalidDate2>\n" +
                "                <IDKind2>110001</IDKind2>\n" +
                "                <RelationToInsured2>04</RelationToInsured2>\n" +
                "                <Sequence2>2</Sequence2>\n" +
                "                <Prop2>40</Prop2>\n" +
                "                <Phone2/>\n" +
                "                <Country2>156</Country2>\n" +
                "                <Address2>陕西省西安市未央区二府庄小区1楼3门3层1号</Address2>\n" +
                "                <Prov2>陕西省</Prov2>\n" +
                "                <City2>西安市</City2>\n" +
                "                <Zone2>未央区</Zone2>\n" +
                "                <Type3/>\n" +
                "                <Name3/>\n" +
                "                <Sex3/>\n" +
                "                <Birthday3/>\n" +
                "                <IDCode3/>\n" +
                "                <BeginDate3/>\n" +
                "                <InvalidDate3/>\n" +
                "                <IDKind3/>\n" +
                "                <RelationToInsured3/>\n" +
                "                <Sequence3>0</Sequence3>\n" +
                "                <Prop3>0</Prop3>\n" +
                "                <Phone3/>\n" +
                "                <Country3/>\n" +
                "                <Address3/>\n" +
                "                <Prov3/>\n" +
                "                <City3/>\n" +
                "                <Zone3/>\n" +
                "            </Bnfs>\n" +
                "            <Risks>\n" +
                "                <Count/>\n" +
                "                <InsuCode>0243</InsuCode>\n" +
                "                <RiskCode>6666</RiskCode>\n" +
                "                <Name>xxx爱康宝重大疾病保险(20年交)</Name>\n" +
                "                <Policypwd/>\n" +
                "                <Share>0</Share>\n" +
                "                <Prem>0</Prem>\n" +
                "                <Price>0</Price>\n" +
                "                <Amnt>650000</Amnt>\n" +
                "                <PayType>5</PayType>\n" +
                "                <PayDueType>4</PayDueType>\n" +
                "                <PayDueDate>20</PayDueDate>\n" +
                "                <InsuDueType>6</InsuDueType>\n" +
                "                <InsuDueDate>199</InsuDueDate>\n" +
                "                <FullBonusGetMode/>\n" +
                "                <GetYearFlag/>\n" +
                "                <GetYear/>\n" +
                "                <BonusPayMode/>\n" +
                "                <BonusGetMode/>\n" +
                "                <WNRiskAccCount/>\n" +
                "                <WNRiskAccType/>\n" +
                "                <InvestProtion/>\n" +
                "                <AutoPayFlag/>\n" +
                "                <AutoPayForFlag/>\n" +
                "                <SubFlag/>\n" +
                "                <RiskBeginDate/>\n" +
                "                <RiskEndDate/>\n" +
                "                <InvestType/>\n" +
                "                <ContinuePay/>\n" +
                "                <ContinuePayAmt/>\n" +
                "                <ContinuePayDue/>\n" +
                "                <ContinuePayBeginDate/>\n" +
                "                <SameFlag/>\n" +
                "            </Risks>\n" +
                "            <Inv>\n" +
                "                <Count/>\n" +
                "                <Type1/>\n" +
                "                <Pro1/>\n" +
                "                <Type2/>\n" +
                "                <Pro2/>\n" +
                "                <Type3/>\n" +
                "                <Pro3/>\n" +
                "                <Type4/>\n" +
                "                <Pro4/>\n" +
                "                <Type5/>\n" +
                "                <Pro5/>\n" +
                "            </Inv>\n" +
                "            <ContinuePay>\n" +
                "                <Count/>\n" +
                "                <Type1/>\n" +
                "                <Pro1/>\n" +
                "                <Type2/>\n" +
                "                <Pro2/>\n" +
                "                <Type3/>\n" +
                "                <Pro3/>\n" +
                "                <Type4/>\n" +
                "                <Pro4/>\n" +
                "                <Type5/>\n" +
                "                <Pro5/>\n" +
                "            </ContinuePay>\n" +
                "            <Objt>\n" +
                "                <Prov/>\n" +
                "                <City/>\n" +
                "                <Zone/>\n" +
                "                <Address/>\n" +
                "                <Amnt/>\n" +
                "                <ZipCode/>\n" +
                "                <Area/>\n" +
                "                <PreFlag/>\n" +
                "                <NoPayPortion/>\n" +
                "                <NoPayPrem/>\n" +
                "                <Struts/>\n" +
                "                <Usage/>\n" +
                "                <FasAmnt/>\n" +
                "                <FloAmnt/>\n" +
                "                <Count/>\n" +
                "                <Name1/>\n" +
                "                <Amnt1/>\n" +
                "                <Name2/>\n" +
                "                <Amnt2/>\n" +
                "                <Name3/>\n" +
                "                <Amnt3/>\n" +
                "                <Name4/>\n" +
                "                <Amnt4/>\n" +
                "                <Name5/>\n" +
                "                <Amnt5/>\n" +
                "                <Name6/>\n" +
                "                <Amnt6/>\n" +
                "                <Name7/>\n" +
                "                <Amnt7/>\n" +
                "            </Objt>\n" +
                "            <Loan>\n" +
                "                <ContNo/>\n" +
                "                <LoanType/>\n" +
                "                <LoanBank/>\n" +
                "                <VchCheckNo/>\n" +
                "                <Flag/>\n" +
                "                <HurtAmnt/>\n" +
                "                <VchNo/>\n" +
                "                <AccNo/>\n" +
                "                <Prem/>\n" +
                "                <BegDate/>\n" +
                "                <EndDate/>\n" +
                "            </Loan>\n" +
                "            <Addt>\n" +
                "                <Count>0</Count>\n" +
                "                <RiskCode1/>\n" +
                "                <InsuCode1/>\n" +
                "                <Name1/>\n" +
                "                <Share1>0</Share1>\n" +
                "                <AutoPayFlag1/>\n" +
                "                <Prem1>0</Prem1>\n" +
                "                <Amnt1/>\n" +
                "                <PayType1/>\n" +
                "                <PayDueType1/>\n" +
                "                <PayDueDate1/>\n" +
                "                <InsuDueType1/>\n" +
                "                <InsuDueDate1/>\n" +
                "                <RiskCode2/>\n" +
                "                <InsuCode2/>\n" +
                "                <Name2/>\n" +
                "                <Share2>0</Share2>\n" +
                "                <AutoPayFlag2/>\n" +
                "                <Prem2>0</Prem2>\n" +
                "                <Amnt2/>\n" +
                "                <PayType2/>\n" +
                "                <PayDueType2/>\n" +
                "                <PayDueDate2/>\n" +
                "                <InsuDueType2/>\n" +
                "                <InsuDueDate2/>\n" +
                "                <RiskCode3/>\n" +
                "                <InsuCode3/>\n" +
                "                <Name3/>\n" +
                "                <Share3>0</Share3>\n" +
                "                <AutoPayFlag3/>\n" +
                "                <Prem3>0</Prem3>\n" +
                "                <Amnt3/>\n" +
                "                <PayType3/>\n" +
                "                <PayDueType3/>\n" +
                "                <PayDueDate3/>\n" +
                "                <InsuDueType3/>\n" +
                "                <InsuDueDate3/>\n" +
                "                <RiskCode4/>\n" +
                "                <InsuCode4/>\n" +
                "                <Name4/>\n" +
                "                <Share4>0</Share4>\n" +
                "                <AutoPayFlag4/>\n" +
                "                <Prem4>0</Prem4>\n" +
                "                <Amnt4/>\n" +
                "                <PayType4/>\n" +
                "                <PayDueType4/>\n" +
                "                <PayDueDate4/>\n" +
                "                <InsuDueType4/>\n" +
                "                <InsuDueDate4/>\n" +
                "                <RiskCode5/>\n" +
                "                <InsuCode5/>\n" +
                "                <Name5/>\n" +
                "                <Share5>0</Share5>\n" +
                "                <AutoPayFlag5/>\n" +
                "                <Prem5>0</Prem5>\n" +
                "                <Amnt5/>\n" +
                "                <PayType5/>\n" +
                "                <PayDueType5/>\n" +
                "                <PayDueDate5/>\n" +
                "                <InsuDueType5/>\n" +
                "                <InsuDueDate5/>\n" +
                "                <RiskCode6/>\n" +
                "                <InsuCode6/>\n" +
                "                <Name6/>\n" +
                "                <Share6>0</Share6>\n" +
                "                <AutoPayFlag6/>\n" +
                "                <Prem6>0</Prem6>\n" +
                "                <Amnt6/>\n" +
                "                <PayType6/>\n" +
                "                <PayDueType6/>\n" +
                "                <PayDueDate6/>\n" +
                "                <InsuDueType6/>\n" +
                "                <InsuDueDate6/>\n" +
                "            </Addt>\n" +
                "            <Base>\n" +
                "                <SpecArranged/>\n" +
                "                <ArugeFlag/>\n" +
                "                <ArbteName/>\n" +
                "                <ConAccName>柴宝琴</ConAccName>\n" +
                "                <ConAccNo>6228450216001095668</ConAccNo>\n" +
                "                <ApplGetAccNo/>\n" +
                "                <InsuGetAccNo/>\n" +
                "                <ApplyDate>20180713</ApplyDate>\n" +
                "                <PolicyApplySerial>41108596</PolicyApplySerial>\n" +
                "                <VchType>8008</VchType>\n" +
                "                <VchName>xxxxxx</VchName>\n" +
                "                <VchNo>201303406554</VchNo>\n" +
                "                <Saler>李慧妮</Saler>\n" +
                "                <BranchCertNo>61011306532879500</BranchCertNo>\n" +
                "                <SalerCertNo>00200812610000035286</SalerCertNo>\n" +
                "                <BranchName>中国xxxxxx股份有限公司西安唐延路支行</BranchName>\n" +
                "                <RenewPayType/>\n" +
                "                <ApplGetAccName/>\n" +
                "                <InsuGetAccName/>\n" +
                "                <PoliValidDate/>\n" +
                "                <TransferFlag>0</TransferFlag>\n" +
                "                <OldPolicy/>\n" +
                "                <OldPolicyPwd/>\n" +
                "                <OldVchNo/>\n" +
                "                <AutoRenewAge/>\n" +
                "            </Base>\n" +
                "            <Reserve1>投保人家庭年收入</Reserve1>\n" +
                "            <Reserve2>500000</Reserve2>\n" +
                "            <Reserve3>儿童其它公司身故保额</Reserve3>\n" +
                "            <Reserve4/>\n" +
                "            <Reserve5/>\n" +
                "            <Reserve6/>\n" +
                "            <Reserve7/>\n" +
                "            <Reserve8/>\n" +
                "            <Reserve9/>\n" +
                "            <Reserve10/>\n" +
                "            <Reserve11/>\n" +
                "            <Reserve12/>\n" +
                "            <Reserve13/>\n" +
                "            <Reserve14/>\n" +
                "            <Reserve15/>\n" +
                "            <Reserve16/>\n" +
                "            <Reserve17/>\n" +
                "            <Reserve18/>\n" +
                "            <Reserve19/>\n" +
                "            <Reserve20/>\n" +
                "            <group>\n" +
                "                <representative/>\n" +
                "                <property/>\n" +
                "                <peopleNum/>\n" +
                "                <OPName/>\n" +
                "                <OPIDKind/>\n" +
                "                <OPIDCode/>\n" +
                "                <OPTel/>\n" +
                "                <email/>\n" +
                "                <mailAddr/>\n" +
                "                <corpName/>\n" +
                "                <regAddr/>\n" +
                "                <tel/>\n" +
                "                <BankName/>\n" +
                "                <BankAcc/>\n" +
                "            </group>\n" +
                "            <tax>\n" +
                "                <IsNeedReceipt/>\n" +
                "                <taxpayerType/>\n" +
                "                <Base>\n" +
                "                    <TIN/>\n" +
                "                </Base>\n" +
                "                <receiptType/>\n" +
                "                <headerType/>\n" +
                "                <type/>\n" +
                "            </tax>\n" +
                "        </Req>\n" +
                "    </App>\n" +
                "</ABCB2I>";
        /*AbcCoder abcCoder = new AbcCoder();
        s = abcCoder.encoder(s);*/
        int size = s.getBytes().length;
        String strSize = String.valueOf(size);
        StringBuilder stringBuilder = new StringBuilder(ENCODER_FORMAT);
        StringBuilder x = stringBuilder.replace(12-strSize.getBytes().length, 12, strSize).append(s);
        StringBuffer sb = new StringBuffer();
        // sb.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
        sb.append(x.toString());  // 提交请求
        String utf8 = new String(sb.toString().getBytes("UTF-8"));
        String unicode = new String(utf8.getBytes(), "UTF-8");
        String gbk = new String(unicode.getBytes("GBK"));
        name c = new name();
        String advice = c.go(gbk);
        System.out.println("接收到服务器的消息 ：" + advice);
    }
}
