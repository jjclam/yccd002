package com.sinosoft.cloud.access;
import com.sinosoft.cloud.access.abc.net.AbcCoder;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;

import static io.netty.buffer.Unpooled.buffer;

public class Test3 {
    private final static String ENCODER_FORMAT = "X1.000000000    111821000                                       000000000";
    public String go(String xmlInfo) throws UnknownHostException, IOException {

        // 向服务器端发送请求，服务器IP地址和服务器监听的端口号

        Socket client = new Socket("127.0.0.1", 8000);

        // 通过printWriter 来向服务器发送消息
        PrintWriter printWriter = new PrintWriter(client.getOutputStream());
        System.out.println("连接已建立...");
        ChannelHandlerContext ctx =null;
        ByteBuf encoded =buffer(4 * xmlInfo.length());
        encoded.writeBytes(xmlInfo.getBytes());
        // 发送消息
        printWriter.println(xmlInfo);

        printWriter.flush();

        // InputStreamReader是低层和高层串流之间的桥梁
        // client.getInputStream()从Socket取得输入串流
        InputStreamReader streamReader = new InputStreamReader(client.getInputStream());

        // 链接数据串流，建立BufferedReader来读取，将
        //BufferReader链接到InputStreamReder
        BufferedReader reader = new BufferedReader(streamReader);
        String advice = reader.readLine();

        reader.close();
        return advice;
    }

    public static void main(String[] args) throws UnknownHostException, IOException {


        // 拼接xml报文
    /*
     * <?xml version="1.0" encoding="GB2312" standalone="yes" ?> <TX>
     * <REQUEST_SN>请求序列码</REQUEST_SN> <CUST_ID>客户号</CUST_ID>
     * <USER_ID>操作员号</USER_ID> <PASSWORD>密码</PASSWORD>
     * <TX_CODE>6W0201</TX_CODE> <LANGUAGE>CN</LANGUAGE> <TX_INFO>
     * </TX_INFO> </TX>
     */
        String s ="<?xml version=\"1.0\" encoding=\"GB2312\"?>\n" +
                "<ABCB2I>\n" +
                "    <Header>\n" +
                "        <TransDate>20151018</TransDate>\n" +
                "        <TransTime>91012</TransTime>\n" +
                "        <TransCode>1005</TransCode>\n" +
                "        <SerialNo>6589744554502</SerialNo>\n" +
                "        <ProvCode>16</ProvCode>\n" +
                "        <BranchNo>2883</BranchNo>\n" +
                "        <BankCode>03</BankCode>\n" +
                "        <CorpNo>1118</CorpNo>\n" +
                "        <TransSide>1</TransSide>\n" +
                "        <EntrustWay>11</EntrustWay>\n" +
                "        <Tlid>298D</Tlid>\n" +
                "    </Header>\n" +
                "    <App>\n" +
                "        <Req>\n" +
                "            <ApplySerial>2812232308151327</ApplySerial>\n" +
                "        </Req>\n" +
                "    </App>\n" +
                "</ABCB2I>\n" +
                "\n";
        /*AbcCoder abcCoder = new AbcCoder();
        s = abcCoder.encoder(s);*/
        int size = s.getBytes().length;
        String strSize = String.valueOf(size);
        StringBuilder stringBuilder = new StringBuilder(ENCODER_FORMAT);
        StringBuilder x = stringBuilder.replace(12-strSize.getBytes().length, 12, strSize).append(s);
        StringBuffer sb = new StringBuffer();
        // sb.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
        sb.append(x.toString());  // 提交请求
        String utf8 = new String(sb.toString().getBytes("UTF-8"));
        String unicode = new String(utf8.getBytes(), "UTF-8");
        String gbk = new String(unicode.getBytes("GBK"));
        Test3 c = new Test3();
        String advice = c.go(gbk);
        System.out.println("接收到服务器的消息 ：" + advice);
    }
}
