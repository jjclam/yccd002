package com.sinosoft.cloud.access;

import com.sinosoft.cloud.access.entity.*;
import com.sinosoft.cloud.access.transformer.PojoTrans;
import com.sinosoft.cloud.access.transformer.XsltTrans;
import com.sinosoft.cloud.rest.TradeInfo;
import com.sinosoft.lis.pos.entity.*;
import com.sinosoft.lis.pos.entity.group.BQResultPojo;
import com.sinosoft.lis.pos.entity.preserve.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.ArrayList;
import java.util.List;

@RunWith(JUnit4.class)
public class PojoTransTest {
    @Test
    public void test2(){
        LCCont lcCont=new LCCont();
        Class<? extends LCCont> aClass = lcCont.getClass();
        aClass.getClass();
        System.out.println(aClass.getClass());
    }
    @Test
    public void test() throws Exception {
        TradeInfo msg =new TradeInfo();
        Body body=new Body();
       /* RespBQProgressQueryPojo p1 =new RespBQProgressQueryPojo();
        ProgressQueryInfosPojo p2 =new ProgressQueryInfosPojo();
        List<ProgressQueryInfoPojo> p3 = new ArrayList<>();
        ProgressQueryInfoPojo p4=new ProgressQueryInfoPojo();
        p4.setContNo("13123123");
        p3.add(p4);
        p2.setProgressQueryInfoPojo(p3);
        p1.setProgressQueryInfosPojo(p2);
        msg.addData(RespBQProgressQueryPojo.class.getName(),p1);*/
       /* RespPolicyLoanClearQueryPojo respPolicyLoanClearQueryPojo=new RespPolicyLoanClearQueryPojo();
        PolicyQueryInfosPojo policyQueryInfosPojo =new PolicyQueryInfosPojo();
        List<PolicyQueryInfoPojo>list =new ArrayList<>();
        PolicyQueryInfoPojo policyQueryInfoPojo=new PolicyQueryInfoPojo();
        EdorAppInfoPojo edorAppInfoPojo =new EdorAppInfoPojo();
        edorAppInfoPojo.setEdorAppName("哇哈哈");
        policyQueryInfoPojo.setContNo("123");
        policyQueryInfoPojo.setEdorAppInfoPojo(edorAppInfoPojo);
       list.add(policyQueryInfoPojo);
        policyQueryInfosPojo.setPolicyQueryInfoPojo(list);
        respPolicyLoanClearQueryPojo.setPolicyQueryInfosPojo(policyQueryInfosPojo);
        msg.addData(RespPolicyLoanClearQueryPojo.class.getName(),respPolicyLoanClearQueryPojo);
*/
        /*RespPolicyReportQuery respPolicyReportQuery =new RespPolicyReportQuery();
        PolicyQueryInfos policyQueryInfos =new PolicyQueryInfos();
        List<PolicyQueryInfo> list1 =new ArrayList<>();
        PolicyQueryInfo policyQueryInfo =new PolicyQueryInfo();
        policyQueryInfo.setContNo("23123");
        list1.add(policyQueryInfo);
        policyQueryInfos.setPolicyQueryInfo(list1);
        respPolicyReportQuery.setPolicyQueryInfos(policyQueryInfos);
        msg.addData(RespPolicyReportQuery.class.getName(),respPolicyReportQuery);*/
  /*      RespCustomerAddressEmailChangeQueryPojo r1 =new RespCustomerAddressEmailChangeQueryPojo();
        PolicyQueryInfosPojo r2=new PolicyQueryInfosPojo();
        List<PolicyQueryInfoPojo> list =new ArrayList<>();
        PolicyQueryInfoPojo r3 =new PolicyQueryInfoPojo();
        r3.setContNo("201811151613");
        list.add(r3);
        r2.setPolicyQueryInfoPojo(list);
        r1.setPolicyQueryInfosPojo(r2);

        PolicyQueryComInfosPojo p2 =new PolicyQueryComInfosPojo();
        List<PolicyQueryComInfoPojo> p3 =new ArrayList<>();
        PolicyQueryComInfoPojo p4=new PolicyQueryComInfoPojo();
        p4.setContNo("201811151612");
        p3.add(p4);
        p2.setPolicyQueryComInfoPojo(p3);
        r1.setPolicyQueryComInfosPojo(p2);
        msg.addData(RespCustomerAddressEmailChangeQueryPojo.class.getName(),r1);*/


       /* RespWTDetailsQueryPojo pojo =new RespWTDetailsQueryPojo();
        AddtRisksPojo addtRisksPojo =new AddtRisksPojo();
        List<AddtRiskPojo> list1 =new ArrayList<>();
        AddtRiskPojo addtRiskPojo =new AddtRiskPojo();
        addtRiskPojo.setPrem("3123");
        list1.add(addtRiskPojo);
        addtRisksPojo.setCount("2");
        addtRisksPojo.setAddtRiskPojo(list1);
        pojo.setAddtRisksPojo(addtRisksPojo);
        pojo.setPrem("66666");
        pojo.setApplyDate("2018-11-19");
        msg.addData(RespWTDetailsQueryPojo.class.getName(),pojo);*/
       /* RespPolicyReportQueryPojo p=new RespPolicyReportQueryPojo();
        PolicyQueryInfosPojo p1=new PolicyQueryInfosPojo();
        List<PolicyQueryInfoPojo> list =new ArrayList<>();
        PolicyQueryInfoPojo p2 =new PolicyQueryInfoPojo();
        p2.setContNo("123");
        list.add(p2);
        p1.setPolicyQueryInfoPojo(list);
        p.setPolicyQueryInfosPojo(p1);
        msg.addData(RespPolicyReportQueryPojo.class.getName(),p);*/
        /*RespAccountHalfDetailsQueryPojo  r=new RespAccountHalfDetailsQueryPojo();
        r.setCount("1");
        AddtRisksPojo a=new AddtRisksPojo();
        a.setCount("2");

        AddtRiskPojo addtRiskPojo=new AddtRiskPojo();
        List<AddtRiskPojo> AddtRiskPojo=new ArrayList<>();
        addtRiskPojo.setAmnt("123");
        AddtRiskPojo.add(addtRiskPojo);
        a.setAddtRiskPojo(AddtRiskPojo);
        r.setAddtRisksPojo(a);
        msg.addData(RespAccountHalfDetailsQueryPojo.class.getName(),r);
        PojoTrans.transTranData(msg,body);*/
       /* RespUAccountListQueryPojo respUAccountListQueryPojo =new RespUAccountListQueryPojo();
        PolicyQueryInfosPojo p =new PolicyQueryInfosPojo();
        List<PolicyQueryInfoPojo> a=new ArrayList<>();
        PolicyQueryInfoPojo p1 =new PolicyQueryInfoPojo();
        p1.setMobilePhone("123");
        p1.setContNo("201043040010647284");
        p1.setMainRiskName("嘉禾财智赢家终身寿险（万能型）");
        p1.setTotalPrem("4720");
        p1.setCValiDate("2010-06-18");
        p1.setEdorStatus("有效");
        a.add(p1);
        p.setPolicyQueryInfoPojo(a);
        respUAccountListQueryPojo.setPolicyQueryInfosPojo(p);
        msg.addData(RespUAccountListQueryPojo.class.getName(),respUAccountListQueryPojo);*/
        BQResultPojo bqResultPojo=new BQResultPojo();
        bqResultPojo.setTransferAmt(102312.00);
        msg.addData(BQResultPojo.class.getName(),bqResultPojo);
        PojoTrans.transTranData(msg,body);
    }

    @Test
    public void test1() throws Exception {
        TranData tranData = new TranData();
        Body body=new Body();
//        ReqWTPojo reqWTPojo=new ReqWTPojo();
      /*  EdorAppInfoPojo edorAppInfoPojo=new EdorAppInfoPojo();
        edorAppInfoPojo.setEdorAppName("建设大街沙发的");
        edorAppInfoPojo.setEdorAppIDNO("123123");
        reqWTPojo.setEdorAppInfoPojo(edorAppInfoPojo);
        reqWTPojo.setContNo("2018111310281234");*/
     /*   msg.addData(ReqWTPojo.class.getName(),reqWTPojo);
        PojoTrans.transPojos(body);*/
        /*ReqWT reqWT =new ReqWT();
        EdorAppInfo edorAppInfo =new EdorAppInfo();
        edorAppInfo.setEdorAppIDNO("123");
        reqWT.setEdorAppInfo(edorAppInfo);
        body.setReqWT(reqWT);
        tranData.setBody(body);*/
/*        ReqReturnSignSure reqReturnSignSure =new ReqReturnSignSure();
        UpdateContInfos updateContInfos =new UpdateContInfos();
        List<UpdateInfo> list =new ArrayList<>();
        UpdateInfo updateInfo=new UpdateInfo();
        updateInfo.setContNo("123");
        list.add(updateInfo);
        updateContInfos.setUpdateInfo(list);
        reqReturnSignSure.setUpdateContInfos(updateContInfos);
        body.setReqReturnSignSure(reqReturnSignSure);

        RespPolicyReportQuery respPolicyReportQuery =new RespPolicyReportQuery();
        PolicyQueryInfos policyQueryInfos =new PolicyQueryInfos();
        List<PolicyQueryInfo> list1 =new ArrayList<>();
        PolicyQueryInfo policyQueryInfo =new PolicyQueryInfo();
        policyQueryInfo.setContNo("23123");
        list1.add(policyQueryInfo);
        policyQueryInfos.setPolicyQueryInfo(list1);
        respPolicyReportQuery.setPolicyQueryInfos(policyQueryInfos);
        body.setRespPolicyReportQuery(respPolicyReportQuery);*/
 //       body.setReqWT(reqWT);
//        body = new Body();
       /* ReqImageProcessing processing=new ReqImageProcessing();
        List<ImgPaths> list =new ArrayList<>();
        ImgPaths i=new ImgPaths();
        i.setImgName("士大夫");
        list.add(i);
        processing.setImgPaths(list);
        body.setReqImageProcessing(processing);*/
/*  *//*      ReqReturnSignSure reqReturnSignSure =new ReqReturnSignSure();
        UpdateContInfos updateContInfos =new UpdateContInfos();
        List<UpdateInfo> list =new ArrayList<>();
        UpdateInfo updateInfo=new UpdateInfo();
        updateInfo.setContNo("123");
        list.add(updateInfo);
        updateContInfos.setUpdateInfo(list);
        reqReturnSignSure.setUpdateContInfos(updateContInfos);
        reqReturnSignSure.setBussType("123");*//*
        body.setReqReturnSignSure(reqReturnSignSure);*/

    /*    ReqPolicySupplyDetailsQuery p =new ReqPolicySupplyDetailsQuery();
        p.setContNo("123");
        p.setEdorType("234");
       body.setReqPolicySupplyDetailsQuery(p);
*/
      /*  ReqRenewalPayChangeApply r1=new ReqRenewalPayChangeApply();
        PolicyQueryInfos r2=new PolicyQueryInfos();
        List<PolicyQueryInfo> list=new ArrayList<>();
        PolicyQueryInfo r3=new PolicyQueryInfo();
        EdorAppInfo r4 =new EdorAppInfo();
        r4.setEdorAppIDNO("123");
        r3.setEdorAppInfo(r4);
        list.add(r3);
        r2.setPolicyQueryInfo(list);
        r1.setPolicyQueryInfos(r2);
        body.setReqRenewalPayChangeApply(r1);*/
        tranData.setBody(body);
        PojoTrans.transPojos(body);
    }
}