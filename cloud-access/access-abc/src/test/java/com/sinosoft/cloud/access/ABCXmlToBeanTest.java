package com.sinosoft.cloud.access;

import com.sinosoft.cloud.access.entity.TranData;
import com.sinosoft.cloud.access.transformer.XsltTrans;
import org.junit.Test;
import org.springframework.core.io.ClassPathResource;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;

/**
 * 测试xml转化为TranData.
 *
 * @Author: cuiguangdong
 * @Date: 2018/4/16 11:49
 * @Description: 写描述
 */
//@RunWith(SpringJUnit4ClassRunner.class)
//@SpringBootTest(classes = ABCApplication.class)
public class ABCXmlToBeanTest {

    @Test
    public void xmlToBeanTest() throws Exception {

        String xml = "";

        ClassPathResource xmlResource = new ClassPathResource("1002In.xml");


        StringBuffer str = new StringBuffer(1024);
        FileInputStream fs = new FileInputStream(xmlResource.getFile());
        InputStreamReader isr;
        isr = new InputStreamReader(fs);
        BufferedReader br = new BufferedReader(isr);

        String data = "";
        while ((data = br.readLine()) != null) {
            str.append(data + "");
        }
        xml = str.toString();


        ClassPathResource classPathResource = new ClassPathResource("com/sinosoft/cloud/access/abc/xsl/1002/Body_In_bak.xsl");


        XsltTrans xsltTrans = new XsltTrans();
        xsltTrans.setAccessName("abc");
        TranData tranData = xsltTrans.bodyTrans(xml, classPathResource);
        String autoPayFlag = tranData.getBody().getLCPols().getLCPols().get(0).getAutoPayFlag();
        System.out.println("autoPayFlag=="+autoPayFlag);
        System.out.println(tranData.toString());
    }
}
