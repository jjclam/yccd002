package com.sinosoft.cloud.access;

import com.sinosoft.cloud.access.abc.router.AbcRouterBean;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * cloud-access
 *
 * @title: cloud-access
 * @package: com.sinosoft.cloud.access.abc.router
 * @author: yangming
 * @date: 2017/11/20 下午6:14
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = ABCApplication.class)
public class AbcRouterBeanTest {


    @Autowired
    AbcRouterBean abcRouterBean;


    @Test
    public void testRouterCache() {


        String key = "123123123";

        System.out.println("开始"+abcRouterBean.getRouterKey(key));


        abcRouterBean.setRouterKey(key);

        System.out.println("结束："+abcRouterBean.getRouterKey(key));

        System.out.println("完事了");

    }

}