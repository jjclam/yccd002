/**
 * Copyright (c) 2018 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.vdb;

import com.sinosoft.lis.vschema.LJAGetSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.DBConnPool;
import com.sinosoft.utility.DBOper;
import com.sinosoft.utility.ExceptionUtils;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;

/**
 * <p>ClassName: LJAGetDBSet </p>
 * <p>Description: DB层多记录数据库操作类文件 </p>
 * <p>Copyright: Copyright (c) 2018</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2018-09-07
 */
public class LJAGetDBSet extends LJAGetSet {
    // @Field
    private Connection con;
    private DBOper db;
    /**
     * flag = true: 传入Connection
     * flag = false: 不传入Connection
     **/
    private boolean mflag = false;

    // @Constructor
    public LJAGetDBSet(Connection tConnection) {
        con = tConnection;
        db = new DBOper(con,"LJAGet");
        mflag = true;
    }

    public LJAGetDBSet() {
        db = new DBOper( "LJAGet" );
    }
    // @Method
    public boolean deleteSQL() {
        if (db.deleteSQL(this)) {
            return true;
        } else {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LJAGetDBSet";
            tError.functionName = "deleteSQL";
            tError.errorMessage = "操作失败!";
            this.mErrors .addOneError(tError);
            return false;
        }
    }

    /**
     * 删除操作
     * 删除条件：主键
     * @return boolean
     */
    public boolean delete() {
        PreparedStatement pstmt = null;

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try {
            int tCount = this.size();
            pstmt = con.prepareStatement("DELETE FROM LJAGet WHERE  1=1  AND ActuGetNo = ?");
            for (int i = 1; i <= tCount; i++) {
                if(this.get(i).getActuGetNo() == null || this.get(i).getActuGetNo().equals("null")) {
                    pstmt.setString(1,null);
                } else {
                    pstmt.setString(1, this.get(i).getActuGetNo());
                }
                pstmt.addBatch();
            }
            pstmt.executeBatch();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            ex.printStackTrace();
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LJAGetDBSet";
            tError.functionName = "delete()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            try {
                pstmt.close();
            } catch (Exception e){
                e.printStackTrace();
            }

            if (!mflag) {
                try {
                    con.close();
                } catch (Exception e){e.printStackTrace();}
            }

            return false;
        }

        if( !mflag ) {
            try {
                con.close();
            } catch (Exception e){e.printStackTrace();}
        }

        return true;
    }

    /**
     * 更新操作
     * 更新条件：主键
     * @return boolean
     */
    public boolean update() {
        PreparedStatement pstmt = null;

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try {
            int tCount = this.size();
            pstmt = con.prepareStatement("UPDATE LJAGet SET  ActuGetNo = ? , OtherNo = ? , OtherNoType = ? , PayMode = ? , BankOnTheWayFlag = ? , BankSuccFlag = ? , SendBankCount = ? , ManageCom = ? , AgentCom = ? , AgentType = ? , AgentCode = ? , AgentGroup = ? , AccName = ? , StartGetDate = ? , AppntNo = ? , SumGetMoney = ? , SaleChnl = ? , ShouldDate = ? , EnterAccDate = ? , ConfDate = ? , ApproveCode = ? , ApproveDate = ? , GetNoticeNo = ? , BankCode = ? , BankAccNo = ? , Drawer = ? , DrawerID = ? , SerialNo = ? , Operator = ? , MakeDate = ? , MakeTime = ? , ModifyDate = ? , ModifyTime = ? , ActualDrawer = ? , ActualDrawerID = ? , PolicyCom = ? , InBankCode = ? , InBankAccNo = ? , BalanceOnTime = ? WHERE  1=1  AND ActuGetNo = ?");
            for (int i = 1; i <= tCount; i++) {
                if(this.get(i).getActuGetNo() == null || this.get(i).getActuGetNo().equals("null")) {
                    pstmt.setString(1,null);
                } else {
                    pstmt.setString(1, this.get(i).getActuGetNo());
                }
                if(this.get(i).getOtherNo() == null || this.get(i).getOtherNo().equals("null")) {
                    pstmt.setString(2,null);
                } else {
                    pstmt.setString(2, this.get(i).getOtherNo());
                }
                if(this.get(i).getOtherNoType() == null || this.get(i).getOtherNoType().equals("null")) {
                    pstmt.setString(3,null);
                } else {
                    pstmt.setString(3, this.get(i).getOtherNoType());
                }
                if(this.get(i).getPayMode() == null || this.get(i).getPayMode().equals("null")) {
                    pstmt.setString(4,null);
                } else {
                    pstmt.setString(4, this.get(i).getPayMode());
                }
                if(this.get(i).getBankOnTheWayFlag() == null || this.get(i).getBankOnTheWayFlag().equals("null")) {
                    pstmt.setString(5,null);
                } else {
                    pstmt.setString(5, this.get(i).getBankOnTheWayFlag());
                }
                if(this.get(i).getBankSuccFlag() == null || this.get(i).getBankSuccFlag().equals("null")) {
                    pstmt.setString(6,null);
                } else {
                    pstmt.setString(6, this.get(i).getBankSuccFlag());
                }
                pstmt.setInt(7, this.get(i).getSendBankCount());
                if(this.get(i).getManageCom() == null || this.get(i).getManageCom().equals("null")) {
                    pstmt.setString(8,null);
                } else {
                    pstmt.setString(8, this.get(i).getManageCom());
                }
                if(this.get(i).getAgentCom() == null || this.get(i).getAgentCom().equals("null")) {
                    pstmt.setString(9,null);
                } else {
                    pstmt.setString(9, this.get(i).getAgentCom());
                }
                if(this.get(i).getAgentType() == null || this.get(i).getAgentType().equals("null")) {
                    pstmt.setString(10,null);
                } else {
                    pstmt.setString(10, this.get(i).getAgentType());
                }
                if(this.get(i).getAgentCode() == null || this.get(i).getAgentCode().equals("null")) {
                    pstmt.setString(11,null);
                } else {
                    pstmt.setString(11, this.get(i).getAgentCode());
                }
                if(this.get(i).getAgentGroup() == null || this.get(i).getAgentGroup().equals("null")) {
                    pstmt.setString(12,null);
                } else {
                    pstmt.setString(12, this.get(i).getAgentGroup());
                }
                if(this.get(i).getAccName() == null || this.get(i).getAccName().equals("null")) {
                    pstmt.setString(13,null);
                } else {
                    pstmt.setString(13, this.get(i).getAccName());
                }
                if(this.get(i).getStartGetDate() == null || this.get(i).getStartGetDate().equals("null")) {
                    pstmt.setDate(14,null);
                } else {
                    pstmt.setDate(14, Date.valueOf(this.get(i).getStartGetDate()));
                }
                if(this.get(i).getAppntNo() == null || this.get(i).getAppntNo().equals("null")) {
                    pstmt.setString(15,null);
                } else {
                    pstmt.setString(15, this.get(i).getAppntNo());
                }
                pstmt.setDouble(16, this.get(i).getSumGetMoney());
                if(this.get(i).getSaleChnl() == null || this.get(i).getSaleChnl().equals("null")) {
                    pstmt.setString(17,null);
                } else {
                    pstmt.setString(17, this.get(i).getSaleChnl());
                }
                if(this.get(i).getShouldDate() == null || this.get(i).getShouldDate().equals("null")) {
                    pstmt.setDate(18,null);
                } else {
                    pstmt.setDate(18, Date.valueOf(this.get(i).getShouldDate()));
                }
                if(this.get(i).getEnterAccDate() == null || this.get(i).getEnterAccDate().equals("null")) {
                    pstmt.setDate(19,null);
                } else {
                    pstmt.setDate(19, Date.valueOf(this.get(i).getEnterAccDate()));
                }
                if(this.get(i).getConfDate() == null || this.get(i).getConfDate().equals("null")) {
                    pstmt.setDate(20,null);
                } else {
                    pstmt.setDate(20, Date.valueOf(this.get(i).getConfDate()));
                }
                if(this.get(i).getApproveCode() == null || this.get(i).getApproveCode().equals("null")) {
                    pstmt.setString(21,null);
                } else {
                    pstmt.setString(21, this.get(i).getApproveCode());
                }
                if(this.get(i).getApproveDate() == null || this.get(i).getApproveDate().equals("null")) {
                    pstmt.setDate(22,null);
                } else {
                    pstmt.setDate(22, Date.valueOf(this.get(i).getApproveDate()));
                }
                if(this.get(i).getGetNoticeNo() == null || this.get(i).getGetNoticeNo().equals("null")) {
                    pstmt.setString(23,null);
                } else {
                    pstmt.setString(23, this.get(i).getGetNoticeNo());
                }
                if(this.get(i).getBankCode() == null || this.get(i).getBankCode().equals("null")) {
                    pstmt.setString(24,null);
                } else {
                    pstmt.setString(24, this.get(i).getBankCode());
                }
                if(this.get(i).getBankAccNo() == null || this.get(i).getBankAccNo().equals("null")) {
                    pstmt.setString(25,null);
                } else {
                    pstmt.setString(25, this.get(i).getBankAccNo());
                }
                if(this.get(i).getDrawer() == null || this.get(i).getDrawer().equals("null")) {
                    pstmt.setString(26,null);
                } else {
                    pstmt.setString(26, this.get(i).getDrawer());
                }
                if(this.get(i).getDrawerID() == null || this.get(i).getDrawerID().equals("null")) {
                    pstmt.setString(27,null);
                } else {
                    pstmt.setString(27, this.get(i).getDrawerID());
                }
                if(this.get(i).getSerialNo() == null || this.get(i).getSerialNo().equals("null")) {
                    pstmt.setString(28,null);
                } else {
                    pstmt.setString(28, this.get(i).getSerialNo());
                }
                if(this.get(i).getOperator() == null || this.get(i).getOperator().equals("null")) {
                    pstmt.setString(29,null);
                } else {
                    pstmt.setString(29, this.get(i).getOperator());
                }
                if(this.get(i).getMakeDate() == null || this.get(i).getMakeDate().equals("null")) {
                    pstmt.setDate(30,null);
                } else {
                    pstmt.setDate(30, Date.valueOf(this.get(i).getMakeDate()));
                }
                if(this.get(i).getMakeTime() == null || this.get(i).getMakeTime().equals("null")) {
                    pstmt.setString(31,null);
                } else {
                    pstmt.setString(31, this.get(i).getMakeTime());
                }
                if(this.get(i).getModifyDate() == null || this.get(i).getModifyDate().equals("null")) {
                    pstmt.setDate(32,null);
                } else {
                    pstmt.setDate(32, Date.valueOf(this.get(i).getModifyDate()));
                }
                if(this.get(i).getModifyTime() == null || this.get(i).getModifyTime().equals("null")) {
                    pstmt.setString(33,null);
                } else {
                    pstmt.setString(33, this.get(i).getModifyTime());
                }
                if(this.get(i).getActualDrawer() == null || this.get(i).getActualDrawer().equals("null")) {
                    pstmt.setString(34,null);
                } else {
                    pstmt.setString(34, this.get(i).getActualDrawer());
                }
                if(this.get(i).getActualDrawerID() == null || this.get(i).getActualDrawerID().equals("null")) {
                    pstmt.setString(35,null);
                } else {
                    pstmt.setString(35, this.get(i).getActualDrawerID());
                }
                if(this.get(i).getPolicyCom() == null || this.get(i).getPolicyCom().equals("null")) {
                    pstmt.setString(36,null);
                } else {
                    pstmt.setString(36, this.get(i).getPolicyCom());
                }
                if(this.get(i).getInBankCode() == null || this.get(i).getInBankCode().equals("null")) {
                    pstmt.setString(37,null);
                } else {
                    pstmt.setString(37, this.get(i).getInBankCode());
                }
                if(this.get(i).getInBankAccNo() == null || this.get(i).getInBankAccNo().equals("null")) {
                    pstmt.setString(38,null);
                } else {
                    pstmt.setString(38, this.get(i).getInBankAccNo());
                }
                if(this.get(i).getBalanceOnTime() == null || this.get(i).getBalanceOnTime().equals("null")) {
                    pstmt.setString(39,null);
                } else {
                    pstmt.setString(39, this.get(i).getBalanceOnTime());
                }
                // set where condition
                if(this.get(i).getActuGetNo() == null || this.get(i).getActuGetNo().equals("null")) {
                    pstmt.setString(40,null);
                } else {
                    pstmt.setString(40, this.get(i).getActuGetNo());
                }
                pstmt.addBatch();
            }
            pstmt.executeBatch();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            ex.printStackTrace();
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LJAGetDBSet";
            tError.functionName = "update()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            try {
                pstmt.close();
            } catch (Exception e){e.printStackTrace();}

            if( !mflag ) {
                try {
                    con.close();
                } catch (Exception e){e.printStackTrace();}
            }

            return false;
        }

        if( !mflag ) {
            try {
                con.close();
            } catch (Exception e){e.printStackTrace();}
        }

        return true;
    }

    /**
     * 新增操作
     * @return boolean
     */
    public boolean insert() {
        PreparedStatement pstmt = null;

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try
        {
            int tCount = this.size();
            pstmt = con.prepareStatement("INSERT INTO LJAGet VALUES( ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ?)");
            for (int i = 1; i <= tCount; i++) {
                if(this.get(i).getActuGetNo() == null || this.get(i).getActuGetNo().equals("null")) {
                    pstmt.setString(1,null);
                } else {
                    pstmt.setString(1, this.get(i).getActuGetNo());
                }
                if(this.get(i).getOtherNo() == null || this.get(i).getOtherNo().equals("null")) {
                    pstmt.setString(2,null);
                } else {
                    pstmt.setString(2, this.get(i).getOtherNo());
                }
                if(this.get(i).getOtherNoType() == null || this.get(i).getOtherNoType().equals("null")) {
                    pstmt.setString(3,null);
                } else {
                    pstmt.setString(3, this.get(i).getOtherNoType());
                }
                if(this.get(i).getPayMode() == null || this.get(i).getPayMode().equals("null")) {
                    pstmt.setString(4,null);
                } else {
                    pstmt.setString(4, this.get(i).getPayMode());
                }
                if(this.get(i).getBankOnTheWayFlag() == null || this.get(i).getBankOnTheWayFlag().equals("null")) {
                    pstmt.setString(5,null);
                } else {
                    pstmt.setString(5, this.get(i).getBankOnTheWayFlag());
                }
                if(this.get(i).getBankSuccFlag() == null || this.get(i).getBankSuccFlag().equals("null")) {
                    pstmt.setString(6,null);
                } else {
                    pstmt.setString(6, this.get(i).getBankSuccFlag());
                }
                pstmt.setInt(7, this.get(i).getSendBankCount());
                if(this.get(i).getManageCom() == null || this.get(i).getManageCom().equals("null")) {
                    pstmt.setString(8,null);
                } else {
                    pstmt.setString(8, this.get(i).getManageCom());
                }
                if(this.get(i).getAgentCom() == null || this.get(i).getAgentCom().equals("null")) {
                    pstmt.setString(9,null);
                } else {
                    pstmt.setString(9, this.get(i).getAgentCom());
                }
                if(this.get(i).getAgentType() == null || this.get(i).getAgentType().equals("null")) {
                    pstmt.setString(10,null);
                } else {
                    pstmt.setString(10, this.get(i).getAgentType());
                }
                if(this.get(i).getAgentCode() == null || this.get(i).getAgentCode().equals("null")) {
                    pstmt.setString(11,null);
                } else {
                    pstmt.setString(11, this.get(i).getAgentCode());
                }
                if(this.get(i).getAgentGroup() == null || this.get(i).getAgentGroup().equals("null")) {
                    pstmt.setString(12,null);
                } else {
                    pstmt.setString(12, this.get(i).getAgentGroup());
                }
                if(this.get(i).getAccName() == null || this.get(i).getAccName().equals("null")) {
                    pstmt.setString(13,null);
                } else {
                    pstmt.setString(13, this.get(i).getAccName());
                }
                if(this.get(i).getStartGetDate() == null || this.get(i).getStartGetDate().equals("null")) {
                    pstmt.setDate(14,null);
                } else {
                    pstmt.setDate(14, Date.valueOf(this.get(i).getStartGetDate()));
                }
                if(this.get(i).getAppntNo() == null || this.get(i).getAppntNo().equals("null")) {
                    pstmt.setString(15,null);
                } else {
                    pstmt.setString(15, this.get(i).getAppntNo());
                }
                pstmt.setDouble(16, this.get(i).getSumGetMoney());
                if(this.get(i).getSaleChnl() == null || this.get(i).getSaleChnl().equals("null")) {
                    pstmt.setString(17,null);
                } else {
                    pstmt.setString(17, this.get(i).getSaleChnl());
                }
                if(this.get(i).getShouldDate() == null || this.get(i).getShouldDate().equals("null")) {
                    pstmt.setDate(18,null);
                } else {
                    pstmt.setDate(18, Date.valueOf(this.get(i).getShouldDate()));
                }
                if(this.get(i).getEnterAccDate() == null || this.get(i).getEnterAccDate().equals("null")) {
                    pstmt.setDate(19,null);
                } else {
                    pstmt.setDate(19, Date.valueOf(this.get(i).getEnterAccDate()));
                }
                if(this.get(i).getConfDate() == null || this.get(i).getConfDate().equals("null")) {
                    pstmt.setDate(20,null);
                } else {
                    pstmt.setDate(20, Date.valueOf(this.get(i).getConfDate()));
                }
                if(this.get(i).getApproveCode() == null || this.get(i).getApproveCode().equals("null")) {
                    pstmt.setString(21,null);
                } else {
                    pstmt.setString(21, this.get(i).getApproveCode());
                }
                if(this.get(i).getApproveDate() == null || this.get(i).getApproveDate().equals("null")) {
                    pstmt.setDate(22,null);
                } else {
                    pstmt.setDate(22, Date.valueOf(this.get(i).getApproveDate()));
                }
                if(this.get(i).getGetNoticeNo() == null || this.get(i).getGetNoticeNo().equals("null")) {
                    pstmt.setString(23,null);
                } else {
                    pstmt.setString(23, this.get(i).getGetNoticeNo());
                }
                if(this.get(i).getBankCode() == null || this.get(i).getBankCode().equals("null")) {
                    pstmt.setString(24,null);
                } else {
                    pstmt.setString(24, this.get(i).getBankCode());
                }
                if(this.get(i).getBankAccNo() == null || this.get(i).getBankAccNo().equals("null")) {
                    pstmt.setString(25,null);
                } else {
                    pstmt.setString(25, this.get(i).getBankAccNo());
                }
                if(this.get(i).getDrawer() == null || this.get(i).getDrawer().equals("null")) {
                    pstmt.setString(26,null);
                } else {
                    pstmt.setString(26, this.get(i).getDrawer());
                }
                if(this.get(i).getDrawerID() == null || this.get(i).getDrawerID().equals("null")) {
                    pstmt.setString(27,null);
                } else {
                    pstmt.setString(27, this.get(i).getDrawerID());
                }
                if(this.get(i).getSerialNo() == null || this.get(i).getSerialNo().equals("null")) {
                    pstmt.setString(28,null);
                } else {
                    pstmt.setString(28, this.get(i).getSerialNo());
                }
                if(this.get(i).getOperator() == null || this.get(i).getOperator().equals("null")) {
                    pstmt.setString(29,null);
                } else {
                    pstmt.setString(29, this.get(i).getOperator());
                }
                if(this.get(i).getMakeDate() == null || this.get(i).getMakeDate().equals("null")) {
                    pstmt.setDate(30,null);
                } else {
                    pstmt.setDate(30, Date.valueOf(this.get(i).getMakeDate()));
                }
                if(this.get(i).getMakeTime() == null || this.get(i).getMakeTime().equals("null")) {
                    pstmt.setString(31,null);
                } else {
                    pstmt.setString(31, this.get(i).getMakeTime());
                }
                if(this.get(i).getModifyDate() == null || this.get(i).getModifyDate().equals("null")) {
                    pstmt.setDate(32,null);
                } else {
                    pstmt.setDate(32, Date.valueOf(this.get(i).getModifyDate()));
                }
                if(this.get(i).getModifyTime() == null || this.get(i).getModifyTime().equals("null")) {
                    pstmt.setString(33,null);
                } else {
                    pstmt.setString(33, this.get(i).getModifyTime());
                }
                if(this.get(i).getActualDrawer() == null || this.get(i).getActualDrawer().equals("null")) {
                    pstmt.setString(34,null);
                } else {
                    pstmt.setString(34, this.get(i).getActualDrawer());
                }
                if(this.get(i).getActualDrawerID() == null || this.get(i).getActualDrawerID().equals("null")) {
                    pstmt.setString(35,null);
                } else {
                    pstmt.setString(35, this.get(i).getActualDrawerID());
                }
                if(this.get(i).getPolicyCom() == null || this.get(i).getPolicyCom().equals("null")) {
                    pstmt.setString(36,null);
                } else {
                    pstmt.setString(36, this.get(i).getPolicyCom());
                }
                if(this.get(i).getInBankCode() == null || this.get(i).getInBankCode().equals("null")) {
                    pstmt.setString(37,null);
                } else {
                    pstmt.setString(37, this.get(i).getInBankCode());
                }
                if(this.get(i).getInBankAccNo() == null || this.get(i).getInBankAccNo().equals("null")) {
                    pstmt.setString(38,null);
                } else {
                    pstmt.setString(38, this.get(i).getInBankAccNo());
                }
                if(this.get(i).getBalanceOnTime() == null || this.get(i).getBalanceOnTime().equals("null")) {
                    pstmt.setString(39,null);
                } else {
                    pstmt.setString(39, this.get(i).getBalanceOnTime());
                }
                pstmt.addBatch();
            }
            pstmt.executeBatch();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            ex.printStackTrace();
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LJAGetDBSet";
            tError.functionName = "insert()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            try {
                pstmt.close();
            } catch (Exception e){e.printStackTrace();}

            if( !mflag ) {
                try {
                    con.close();
                } catch (Exception e){e.printStackTrace();}
            }

            return false;
        }

        if( !mflag ) {
            try {
                con.close();
            } catch (Exception e){e.printStackTrace();}
        }

        return true;
    }
}
