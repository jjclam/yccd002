/**
 * Copyright (c) 2018 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import com.sinosoft.lis.db.LJAGetDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.*;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

/**
 * <p>ClassName: LJAGetSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2018</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2018-09-07
 */
public class LJAGetSchema implements Schema, Cloneable {
    // @Field
    /** 实付号码 */
    private String ActuGetNo;
    /** 其它号码 */
    private String OtherNo;
    /** 其它号码类型 */
    private String OtherNoType;
    /** 交费方式 */
    private String PayMode;
    /** 银行在途标志 */
    private String BankOnTheWayFlag;
    /** 银行转帐成功标记 */
    private String BankSuccFlag;
    /** 送银行次数 */
    private int SendBankCount;
    /** 管理机构 */
    private String ManageCom;
    /** 代理机构 */
    private String AgentCom;
    /** 代理机构内部分类 */
    private String AgentType;
    /** 代理人编码 */
    private String AgentCode;
    /** 代理人组别 */
    private String AgentGroup;
    /** 帐户名 */
    private String AccName;
    /** 最早付费日期 */
    private Date StartGetDate;
    /** 投保人客户号码 */
    private String AppntNo;
    /** 总给付金额 */
    private double SumGetMoney;
    /** 销售渠道 */
    private String SaleChnl;
    /** 应付日期 */
    private Date ShouldDate;
    /** 财务到帐日期 */
    private Date EnterAccDate;
    /** 财务确认日期 */
    private Date ConfDate;
    /** 复核人编码 */
    private String ApproveCode;
    /** 复核日期 */
    private Date ApproveDate;
    /** 给付通知书号码 */
    private String GetNoticeNo;
    /** 银行编码 */
    private String BankCode;
    /** 银行帐号 */
    private String BankAccNo;
    /** 领取人 */
    private String Drawer;
    /** 领取人身份证号 */
    private String DrawerID;
    /** 流水号 */
    private String SerialNo;
    /** 操作员 */
    private String Operator;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 最后一次修改日期 */
    private Date ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime;
    /** 实际领取人 */
    private String ActualDrawer;
    /** 实际领取人身份证号 */
    private String ActualDrawerID;
    /** 服务机构 */
    private String PolicyCom;
    /** Inbankcode */
    private String InBankCode;
    /** Inbankaccno */
    private String InBankAccNo;
    /** 定期结算标志 */
    private String BalanceOnTime;

    public static final int FIELDNUM = 39;    // 数据库表的字段个数

    private static String[] PK;                // 主键

    private FDate fDate = new FDate();        // 处理日期

    public CErrors mErrors;            // 错误信息

    // @Constructor
    public LJAGetSchema() {
        mErrors = new CErrors();

        String[] pk = new String[1];
        pk[0] = "ActuGetNo";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException {
        LJAGetSchema cloned = (LJAGetSchema)super.clone();
        cloned.fDate = (FDate) fDate.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK() {
        return PK;
    }

    public String getActuGetNo() {
        return ActuGetNo;
    }
    public void setActuGetNo(String aActuGetNo) {
        ActuGetNo = aActuGetNo;
    }
    public String getOtherNo() {
        return OtherNo;
    }
    public void setOtherNo(String aOtherNo) {
        OtherNo = aOtherNo;
    }
    public String getOtherNoType() {
        return OtherNoType;
    }
    public void setOtherNoType(String aOtherNoType) {
        OtherNoType = aOtherNoType;
    }
    public String getPayMode() {
        return PayMode;
    }
    public void setPayMode(String aPayMode) {
        PayMode = aPayMode;
    }
    public String getBankOnTheWayFlag() {
        return BankOnTheWayFlag;
    }
    public void setBankOnTheWayFlag(String aBankOnTheWayFlag) {
        BankOnTheWayFlag = aBankOnTheWayFlag;
    }
    public String getBankSuccFlag() {
        return BankSuccFlag;
    }
    public void setBankSuccFlag(String aBankSuccFlag) {
        BankSuccFlag = aBankSuccFlag;
    }
    public int getSendBankCount() {
        return SendBankCount;
    }
    public void setSendBankCount(int aSendBankCount) {
        SendBankCount = aSendBankCount;
    }
    public void setSendBankCount(String aSendBankCount) {
        if (aSendBankCount != null && !aSendBankCount.equals("")) {
            Integer tInteger = new Integer(aSendBankCount);
            int i = tInteger.intValue();
            SendBankCount = i;
        }
    }

    public String getManageCom() {
        return ManageCom;
    }
    public void setManageCom(String aManageCom) {
        ManageCom = aManageCom;
    }
    public String getAgentCom() {
        return AgentCom;
    }
    public void setAgentCom(String aAgentCom) {
        AgentCom = aAgentCom;
    }
    public String getAgentType() {
        return AgentType;
    }
    public void setAgentType(String aAgentType) {
        AgentType = aAgentType;
    }
    public String getAgentCode() {
        return AgentCode;
    }
    public void setAgentCode(String aAgentCode) {
        AgentCode = aAgentCode;
    }
    public String getAgentGroup() {
        return AgentGroup;
    }
    public void setAgentGroup(String aAgentGroup) {
        AgentGroup = aAgentGroup;
    }
    public String getAccName() {
        return AccName;
    }
    public void setAccName(String aAccName) {
        AccName = aAccName;
    }
    public String getStartGetDate() {
        if(StartGetDate != null) {
            return fDate.getString(StartGetDate);
        } else {
            return null;
        }
    }
    public void setStartGetDate(Date aStartGetDate) {
        StartGetDate = aStartGetDate;
    }
    public void setStartGetDate(String aStartGetDate) {
        if (aStartGetDate != null && !aStartGetDate.equals("")) {
            StartGetDate = fDate.getDate(aStartGetDate);
        } else
            StartGetDate = null;
    }

    public String getAppntNo() {
        return AppntNo;
    }
    public void setAppntNo(String aAppntNo) {
        AppntNo = aAppntNo;
    }
    public double getSumGetMoney() {
        return SumGetMoney;
    }
    public void setSumGetMoney(double aSumGetMoney) {
        SumGetMoney = aSumGetMoney;
    }
    public void setSumGetMoney(String aSumGetMoney) {
        if (aSumGetMoney != null && !aSumGetMoney.equals("")) {
            Double tDouble = new Double(aSumGetMoney);
            double d = tDouble.doubleValue();
            SumGetMoney = d;
        }
    }

    public String getSaleChnl() {
        return SaleChnl;
    }
    public void setSaleChnl(String aSaleChnl) {
        SaleChnl = aSaleChnl;
    }
    public String getShouldDate() {
        if(ShouldDate != null) {
            return fDate.getString(ShouldDate);
        } else {
            return null;
        }
    }
    public void setShouldDate(Date aShouldDate) {
        ShouldDate = aShouldDate;
    }
    public void setShouldDate(String aShouldDate) {
        if (aShouldDate != null && !aShouldDate.equals("")) {
            ShouldDate = fDate.getDate(aShouldDate);
        } else
            ShouldDate = null;
    }

    public String getEnterAccDate() {
        if(EnterAccDate != null) {
            return fDate.getString(EnterAccDate);
        } else {
            return null;
        }
    }
    public void setEnterAccDate(Date aEnterAccDate) {
        EnterAccDate = aEnterAccDate;
    }
    public void setEnterAccDate(String aEnterAccDate) {
        if (aEnterAccDate != null && !aEnterAccDate.equals("")) {
            EnterAccDate = fDate.getDate(aEnterAccDate);
        } else
            EnterAccDate = null;
    }

    public String getConfDate() {
        if(ConfDate != null) {
            return fDate.getString(ConfDate);
        } else {
            return null;
        }
    }
    public void setConfDate(Date aConfDate) {
        ConfDate = aConfDate;
    }
    public void setConfDate(String aConfDate) {
        if (aConfDate != null && !aConfDate.equals("")) {
            ConfDate = fDate.getDate(aConfDate);
        } else
            ConfDate = null;
    }

    public String getApproveCode() {
        return ApproveCode;
    }
    public void setApproveCode(String aApproveCode) {
        ApproveCode = aApproveCode;
    }
    public String getApproveDate() {
        if(ApproveDate != null) {
            return fDate.getString(ApproveDate);
        } else {
            return null;
        }
    }
    public void setApproveDate(Date aApproveDate) {
        ApproveDate = aApproveDate;
    }
    public void setApproveDate(String aApproveDate) {
        if (aApproveDate != null && !aApproveDate.equals("")) {
            ApproveDate = fDate.getDate(aApproveDate);
        } else
            ApproveDate = null;
    }

    public String getGetNoticeNo() {
        return GetNoticeNo;
    }
    public void setGetNoticeNo(String aGetNoticeNo) {
        GetNoticeNo = aGetNoticeNo;
    }
    public String getBankCode() {
        return BankCode;
    }
    public void setBankCode(String aBankCode) {
        BankCode = aBankCode;
    }
    public String getBankAccNo() {
        return BankAccNo;
    }
    public void setBankAccNo(String aBankAccNo) {
        BankAccNo = aBankAccNo;
    }
    public String getDrawer() {
        return Drawer;
    }
    public void setDrawer(String aDrawer) {
        Drawer = aDrawer;
    }
    public String getDrawerID() {
        return DrawerID;
    }
    public void setDrawerID(String aDrawerID) {
        DrawerID = aDrawerID;
    }
    public String getSerialNo() {
        return SerialNo;
    }
    public void setSerialNo(String aSerialNo) {
        SerialNo = aSerialNo;
    }
    public String getOperator() {
        return Operator;
    }
    public void setOperator(String aOperator) {
        Operator = aOperator;
    }
    public String getMakeDate() {
        if(MakeDate != null) {
            return fDate.getString(MakeDate);
        } else {
            return null;
        }
    }
    public void setMakeDate(Date aMakeDate) {
        MakeDate = aMakeDate;
    }
    public void setMakeDate(String aMakeDate) {
        if (aMakeDate != null && !aMakeDate.equals("")) {
            MakeDate = fDate.getDate(aMakeDate);
        } else
            MakeDate = null;
    }

    public String getMakeTime() {
        return MakeTime;
    }
    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }
    public String getModifyDate() {
        if(ModifyDate != null) {
            return fDate.getString(ModifyDate);
        } else {
            return null;
        }
    }
    public void setModifyDate(Date aModifyDate) {
        ModifyDate = aModifyDate;
    }
    public void setModifyDate(String aModifyDate) {
        if (aModifyDate != null && !aModifyDate.equals("")) {
            ModifyDate = fDate.getDate(aModifyDate);
        } else
            ModifyDate = null;
    }

    public String getModifyTime() {
        return ModifyTime;
    }
    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }
    public String getActualDrawer() {
        return ActualDrawer;
    }
    public void setActualDrawer(String aActualDrawer) {
        ActualDrawer = aActualDrawer;
    }
    public String getActualDrawerID() {
        return ActualDrawerID;
    }
    public void setActualDrawerID(String aActualDrawerID) {
        ActualDrawerID = aActualDrawerID;
    }
    public String getPolicyCom() {
        return PolicyCom;
    }
    public void setPolicyCom(String aPolicyCom) {
        PolicyCom = aPolicyCom;
    }
    public String getInBankCode() {
        return InBankCode;
    }
    public void setInBankCode(String aInBankCode) {
        InBankCode = aInBankCode;
    }
    public String getInBankAccNo() {
        return InBankAccNo;
    }
    public void setInBankAccNo(String aInBankAccNo) {
        InBankAccNo = aInBankAccNo;
    }
    public String getBalanceOnTime() {
        return BalanceOnTime;
    }
    public void setBalanceOnTime(String aBalanceOnTime) {
        BalanceOnTime = aBalanceOnTime;
    }

    /**
     * 使用另外一个 LJAGetSchema 对象给 Schema 赋值
     * @param: aLJAGetSchema LJAGetSchema
     **/
    public void setSchema(LJAGetSchema aLJAGetSchema) {
        this.ActuGetNo = aLJAGetSchema.getActuGetNo();
        this.OtherNo = aLJAGetSchema.getOtherNo();
        this.OtherNoType = aLJAGetSchema.getOtherNoType();
        this.PayMode = aLJAGetSchema.getPayMode();
        this.BankOnTheWayFlag = aLJAGetSchema.getBankOnTheWayFlag();
        this.BankSuccFlag = aLJAGetSchema.getBankSuccFlag();
        this.SendBankCount = aLJAGetSchema.getSendBankCount();
        this.ManageCom = aLJAGetSchema.getManageCom();
        this.AgentCom = aLJAGetSchema.getAgentCom();
        this.AgentType = aLJAGetSchema.getAgentType();
        this.AgentCode = aLJAGetSchema.getAgentCode();
        this.AgentGroup = aLJAGetSchema.getAgentGroup();
        this.AccName = aLJAGetSchema.getAccName();
        this.StartGetDate = fDate.getDate( aLJAGetSchema.getStartGetDate());
        this.AppntNo = aLJAGetSchema.getAppntNo();
        this.SumGetMoney = aLJAGetSchema.getSumGetMoney();
        this.SaleChnl = aLJAGetSchema.getSaleChnl();
        this.ShouldDate = fDate.getDate( aLJAGetSchema.getShouldDate());
        this.EnterAccDate = fDate.getDate( aLJAGetSchema.getEnterAccDate());
        this.ConfDate = fDate.getDate( aLJAGetSchema.getConfDate());
        this.ApproveCode = aLJAGetSchema.getApproveCode();
        this.ApproveDate = fDate.getDate( aLJAGetSchema.getApproveDate());
        this.GetNoticeNo = aLJAGetSchema.getGetNoticeNo();
        this.BankCode = aLJAGetSchema.getBankCode();
        this.BankAccNo = aLJAGetSchema.getBankAccNo();
        this.Drawer = aLJAGetSchema.getDrawer();
        this.DrawerID = aLJAGetSchema.getDrawerID();
        this.SerialNo = aLJAGetSchema.getSerialNo();
        this.Operator = aLJAGetSchema.getOperator();
        this.MakeDate = fDate.getDate( aLJAGetSchema.getMakeDate());
        this.MakeTime = aLJAGetSchema.getMakeTime();
        this.ModifyDate = fDate.getDate( aLJAGetSchema.getModifyDate());
        this.ModifyTime = aLJAGetSchema.getModifyTime();
        this.ActualDrawer = aLJAGetSchema.getActualDrawer();
        this.ActualDrawerID = aLJAGetSchema.getActualDrawerID();
        this.PolicyCom = aLJAGetSchema.getPolicyCom();
        this.InBankCode = aLJAGetSchema.getInBankCode();
        this.InBankAccNo = aLJAGetSchema.getInBankAccNo();
        this.BalanceOnTime = aLJAGetSchema.getBalanceOnTime();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: rs ResultSet
     * @param: i int
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs,int i) {
        try {
            //rs.absolute(i);        // 非滚动游标
            if( rs.getString("ActuGetNo") == null )
                this.ActuGetNo = null;
            else
                this.ActuGetNo = rs.getString("ActuGetNo").trim();

            if( rs.getString("OtherNo") == null )
                this.OtherNo = null;
            else
                this.OtherNo = rs.getString("OtherNo").trim();

            if( rs.getString("OtherNoType") == null )
                this.OtherNoType = null;
            else
                this.OtherNoType = rs.getString("OtherNoType").trim();

            if( rs.getString("PayMode") == null )
                this.PayMode = null;
            else
                this.PayMode = rs.getString("PayMode").trim();

            if( rs.getString("BankOnTheWayFlag") == null )
                this.BankOnTheWayFlag = null;
            else
                this.BankOnTheWayFlag = rs.getString("BankOnTheWayFlag").trim();

            if( rs.getString("BankSuccFlag") == null )
                this.BankSuccFlag = null;
            else
                this.BankSuccFlag = rs.getString("BankSuccFlag").trim();

            this.SendBankCount = rs.getInt("SendBankCount");
            if( rs.getString("ManageCom") == null )
                this.ManageCom = null;
            else
                this.ManageCom = rs.getString("ManageCom").trim();

            if( rs.getString("AgentCom") == null )
                this.AgentCom = null;
            else
                this.AgentCom = rs.getString("AgentCom").trim();

            if( rs.getString("AgentType") == null )
                this.AgentType = null;
            else
                this.AgentType = rs.getString("AgentType").trim();

            if( rs.getString("AgentCode") == null )
                this.AgentCode = null;
            else
                this.AgentCode = rs.getString("AgentCode").trim();

            if( rs.getString("AgentGroup") == null )
                this.AgentGroup = null;
            else
                this.AgentGroup = rs.getString("AgentGroup").trim();

            if( rs.getString("AccName") == null )
                this.AccName = null;
            else
                this.AccName = rs.getString("AccName").trim();

            this.StartGetDate = rs.getDate("StartGetDate");
            if( rs.getString("AppntNo") == null )
                this.AppntNo = null;
            else
                this.AppntNo = rs.getString("AppntNo").trim();

            this.SumGetMoney = rs.getDouble("SumGetMoney");
            if( rs.getString("SaleChnl") == null )
                this.SaleChnl = null;
            else
                this.SaleChnl = rs.getString("SaleChnl").trim();

            this.ShouldDate = rs.getDate("ShouldDate");
            this.EnterAccDate = rs.getDate("EnterAccDate");
            this.ConfDate = rs.getDate("ConfDate");
            if( rs.getString("ApproveCode") == null )
                this.ApproveCode = null;
            else
                this.ApproveCode = rs.getString("ApproveCode").trim();

            this.ApproveDate = rs.getDate("ApproveDate");
            if( rs.getString("GetNoticeNo") == null )
                this.GetNoticeNo = null;
            else
                this.GetNoticeNo = rs.getString("GetNoticeNo").trim();

            if( rs.getString("BankCode") == null )
                this.BankCode = null;
            else
                this.BankCode = rs.getString("BankCode").trim();

            if( rs.getString("BankAccNo") == null )
                this.BankAccNo = null;
            else
                this.BankAccNo = rs.getString("BankAccNo").trim();

            if( rs.getString("Drawer") == null )
                this.Drawer = null;
            else
                this.Drawer = rs.getString("Drawer").trim();

            if( rs.getString("DrawerID") == null )
                this.DrawerID = null;
            else
                this.DrawerID = rs.getString("DrawerID").trim();

            if( rs.getString("SerialNo") == null )
                this.SerialNo = null;
            else
                this.SerialNo = rs.getString("SerialNo").trim();

            if( rs.getString("Operator") == null )
                this.Operator = null;
            else
                this.Operator = rs.getString("Operator").trim();

            this.MakeDate = rs.getDate("MakeDate");
            if( rs.getString("MakeTime") == null )
                this.MakeTime = null;
            else
                this.MakeTime = rs.getString("MakeTime").trim();

            this.ModifyDate = rs.getDate("ModifyDate");
            if( rs.getString("ModifyTime") == null )
                this.ModifyTime = null;
            else
                this.ModifyTime = rs.getString("ModifyTime").trim();

            if( rs.getString("ActualDrawer") == null )
                this.ActualDrawer = null;
            else
                this.ActualDrawer = rs.getString("ActualDrawer").trim();

            if( rs.getString("ActualDrawerID") == null )
                this.ActualDrawerID = null;
            else
                this.ActualDrawerID = rs.getString("ActualDrawerID").trim();

            if( rs.getString("PolicyCom") == null )
                this.PolicyCom = null;
            else
                this.PolicyCom = rs.getString("PolicyCom").trim();

            if( rs.getString("InBankCode") == null )
                this.InBankCode = null;
            else
                this.InBankCode = rs.getString("InBankCode").trim();

            if( rs.getString("InBankAccNo") == null )
                this.InBankAccNo = null;
            else
                this.InBankAccNo = rs.getString("InBankAccNo").trim();

            if( rs.getString("BalanceOnTime") == null )
                this.BalanceOnTime = null;
            else
                this.BalanceOnTime = rs.getString("BalanceOnTime").trim();

        }
        catch(SQLException sqle) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LJAGetSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = ExceptionUtils.exceptionToString(sqle);
            this.mErrors .addOneError(tError);
            return false;
        }
        return true;
    }

    public LJAGetSchema getSchema() {
        LJAGetSchema aLJAGetSchema = new LJAGetSchema();
        aLJAGetSchema.setSchema(this);
        return aLJAGetSchema;
    }

    public LJAGetDB getDB() {
        LJAGetDB aDBOper = new LJAGetDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLJAGet描述/A>表字段
     * @return: String 返回打包后字符串
     **/
    public String encode() {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(ActuGetNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(OtherNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(OtherNoType)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(PayMode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(BankOnTheWayFlag)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(BankSuccFlag)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(SendBankCount));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ManageCom)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AgentCom)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AgentType)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AgentCode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AgentGroup)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AccName)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( StartGetDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AppntNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(SumGetMoney));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(SaleChnl)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( ShouldDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( EnterAccDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( ConfDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ApproveCode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( ApproveDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(GetNoticeNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(BankCode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(BankAccNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Drawer)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(DrawerID)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(SerialNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ModifyTime)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ActualDrawer)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ActualDrawerID)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(PolicyCom)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(InBankCode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(InBankAccNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(BalanceOnTime));
        return strReturn.toString();
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLJAGet>历史记账凭证主表信息</A>表字段
     * @param: strMessage String 包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage) {
        try {
            ActuGetNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
            OtherNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
            OtherNoType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
            PayMode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
            BankOnTheWayFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
            BankSuccFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
            SendBankCount = new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,7, SysConst.PACKAGESPILTER))).intValue();
            ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
            AgentCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
            AgentType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
            AgentCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
            AgentGroup = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
            AccName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
            StartGetDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER));
            AppntNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15, SysConst.PACKAGESPILTER );
            SumGetMoney = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,16, SysConst.PACKAGESPILTER))).doubleValue();
            SaleChnl = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17, SysConst.PACKAGESPILTER );
            ShouldDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18, SysConst.PACKAGESPILTER));
            EnterAccDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19, SysConst.PACKAGESPILTER));
            ConfDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20, SysConst.PACKAGESPILTER));
            ApproveCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 21, SysConst.PACKAGESPILTER );
            ApproveDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 22, SysConst.PACKAGESPILTER));
            GetNoticeNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 23, SysConst.PACKAGESPILTER );
            BankCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 24, SysConst.PACKAGESPILTER );
            BankAccNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 25, SysConst.PACKAGESPILTER );
            Drawer = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 26, SysConst.PACKAGESPILTER );
            DrawerID = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 27, SysConst.PACKAGESPILTER );
            SerialNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 28, SysConst.PACKAGESPILTER );
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 29, SysConst.PACKAGESPILTER );
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 30, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 31, SysConst.PACKAGESPILTER );
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 32, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 33, SysConst.PACKAGESPILTER );
            ActualDrawer = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 34, SysConst.PACKAGESPILTER );
            ActualDrawerID = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 35, SysConst.PACKAGESPILTER );
            PolicyCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 36, SysConst.PACKAGESPILTER );
            InBankCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 37, SysConst.PACKAGESPILTER );
            InBankAccNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 38, SysConst.PACKAGESPILTER );
            BalanceOnTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 39, SysConst.PACKAGESPILTER );
        }
        catch(NumberFormatException ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LJAGetSchema";
            tError.functionName = "decode";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode String 希望取得的字段名
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("ActuGetNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ActuGetNo));
        }
        if (FCode.equalsIgnoreCase("OtherNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OtherNo));
        }
        if (FCode.equalsIgnoreCase("OtherNoType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OtherNoType));
        }
        if (FCode.equalsIgnoreCase("PayMode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PayMode));
        }
        if (FCode.equalsIgnoreCase("BankOnTheWayFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BankOnTheWayFlag));
        }
        if (FCode.equalsIgnoreCase("BankSuccFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BankSuccFlag));
        }
        if (FCode.equalsIgnoreCase("SendBankCount")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SendBankCount));
        }
        if (FCode.equalsIgnoreCase("ManageCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
        }
        if (FCode.equalsIgnoreCase("AgentCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentCom));
        }
        if (FCode.equalsIgnoreCase("AgentType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentType));
        }
        if (FCode.equalsIgnoreCase("AgentCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentCode));
        }
        if (FCode.equalsIgnoreCase("AgentGroup")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentGroup));
        }
        if (FCode.equalsIgnoreCase("AccName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AccName));
        }
        if (FCode.equalsIgnoreCase("StartGetDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getStartGetDate()));
        }
        if (FCode.equalsIgnoreCase("AppntNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AppntNo));
        }
        if (FCode.equalsIgnoreCase("SumGetMoney")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SumGetMoney));
        }
        if (FCode.equalsIgnoreCase("SaleChnl")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SaleChnl));
        }
        if (FCode.equalsIgnoreCase("ShouldDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getShouldDate()));
        }
        if (FCode.equalsIgnoreCase("EnterAccDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getEnterAccDate()));
        }
        if (FCode.equalsIgnoreCase("ConfDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getConfDate()));
        }
        if (FCode.equalsIgnoreCase("ApproveCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ApproveCode));
        }
        if (FCode.equalsIgnoreCase("ApproveDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getApproveDate()));
        }
        if (FCode.equalsIgnoreCase("GetNoticeNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GetNoticeNo));
        }
        if (FCode.equalsIgnoreCase("BankCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BankCode));
        }
        if (FCode.equalsIgnoreCase("BankAccNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BankAccNo));
        }
        if (FCode.equalsIgnoreCase("Drawer")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Drawer));
        }
        if (FCode.equalsIgnoreCase("DrawerID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DrawerID));
        }
        if (FCode.equalsIgnoreCase("SerialNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SerialNo));
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (FCode.equalsIgnoreCase("ActualDrawer")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ActualDrawer));
        }
        if (FCode.equalsIgnoreCase("ActualDrawerID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ActualDrawerID));
        }
        if (FCode.equalsIgnoreCase("PolicyCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PolicyCom));
        }
        if (FCode.equalsIgnoreCase("InBankCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InBankCode));
        }
        if (FCode.equalsIgnoreCase("InBankAccNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InBankAccNo));
        }
        if (FCode.equalsIgnoreCase("BalanceOnTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BalanceOnTime));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex int 指定的字段索引值
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(ActuGetNo);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(OtherNo);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(OtherNoType);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(PayMode);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(BankOnTheWayFlag);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(BankSuccFlag);
                break;
            case 6:
                strFieldValue = String.valueOf(SendBankCount);
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(ManageCom);
                break;
            case 8:
                strFieldValue = StrTool.GBKToUnicode(AgentCom);
                break;
            case 9:
                strFieldValue = StrTool.GBKToUnicode(AgentType);
                break;
            case 10:
                strFieldValue = StrTool.GBKToUnicode(AgentCode);
                break;
            case 11:
                strFieldValue = StrTool.GBKToUnicode(AgentGroup);
                break;
            case 12:
                strFieldValue = StrTool.GBKToUnicode(AccName);
                break;
            case 13:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getStartGetDate()));
                break;
            case 14:
                strFieldValue = StrTool.GBKToUnicode(AppntNo);
                break;
            case 15:
                strFieldValue = String.valueOf(SumGetMoney);
                break;
            case 16:
                strFieldValue = StrTool.GBKToUnicode(SaleChnl);
                break;
            case 17:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getShouldDate()));
                break;
            case 18:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getEnterAccDate()));
                break;
            case 19:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getConfDate()));
                break;
            case 20:
                strFieldValue = StrTool.GBKToUnicode(ApproveCode);
                break;
            case 21:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getApproveDate()));
                break;
            case 22:
                strFieldValue = StrTool.GBKToUnicode(GetNoticeNo);
                break;
            case 23:
                strFieldValue = StrTool.GBKToUnicode(BankCode);
                break;
            case 24:
                strFieldValue = StrTool.GBKToUnicode(BankAccNo);
                break;
            case 25:
                strFieldValue = StrTool.GBKToUnicode(Drawer);
                break;
            case 26:
                strFieldValue = StrTool.GBKToUnicode(DrawerID);
                break;
            case 27:
                strFieldValue = StrTool.GBKToUnicode(SerialNo);
                break;
            case 28:
                strFieldValue = StrTool.GBKToUnicode(Operator);
                break;
            case 29:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
                break;
            case 30:
                strFieldValue = StrTool.GBKToUnicode(MakeTime);
                break;
            case 31:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
                break;
            case 32:
                strFieldValue = StrTool.GBKToUnicode(ModifyTime);
                break;
            case 33:
                strFieldValue = StrTool.GBKToUnicode(ActualDrawer);
                break;
            case 34:
                strFieldValue = StrTool.GBKToUnicode(ActualDrawerID);
                break;
            case 35:
                strFieldValue = StrTool.GBKToUnicode(PolicyCom);
                break;
            case 36:
                strFieldValue = StrTool.GBKToUnicode(InBankCode);
                break;
            case 37:
                strFieldValue = StrTool.GBKToUnicode(InBankAccNo);
                break;
            case 38:
                strFieldValue = StrTool.GBKToUnicode(BalanceOnTime);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode String 需要赋值的对象
     * @param: FValue String 要赋的值
     * @return: boolean
     **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("ActuGetNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                ActuGetNo = FValue.trim();
            }
            else
                ActuGetNo = null;
        }
        if (FCode.equalsIgnoreCase("OtherNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                OtherNo = FValue.trim();
            }
            else
                OtherNo = null;
        }
        if (FCode.equalsIgnoreCase("OtherNoType")) {
            if( FValue != null && !FValue.equals(""))
            {
                OtherNoType = FValue.trim();
            }
            else
                OtherNoType = null;
        }
        if (FCode.equalsIgnoreCase("PayMode")) {
            if( FValue != null && !FValue.equals(""))
            {
                PayMode = FValue.trim();
            }
            else
                PayMode = null;
        }
        if (FCode.equalsIgnoreCase("BankOnTheWayFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                BankOnTheWayFlag = FValue.trim();
            }
            else
                BankOnTheWayFlag = null;
        }
        if (FCode.equalsIgnoreCase("BankSuccFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                BankSuccFlag = FValue.trim();
            }
            else
                BankSuccFlag = null;
        }
        if (FCode.equalsIgnoreCase("SendBankCount")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                SendBankCount = i;
            }
        }
        if (FCode.equalsIgnoreCase("ManageCom")) {
            if( FValue != null && !FValue.equals(""))
            {
                ManageCom = FValue.trim();
            }
            else
                ManageCom = null;
        }
        if (FCode.equalsIgnoreCase("AgentCom")) {
            if( FValue != null && !FValue.equals(""))
            {
                AgentCom = FValue.trim();
            }
            else
                AgentCom = null;
        }
        if (FCode.equalsIgnoreCase("AgentType")) {
            if( FValue != null && !FValue.equals(""))
            {
                AgentType = FValue.trim();
            }
            else
                AgentType = null;
        }
        if (FCode.equalsIgnoreCase("AgentCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                AgentCode = FValue.trim();
            }
            else
                AgentCode = null;
        }
        if (FCode.equalsIgnoreCase("AgentGroup")) {
            if( FValue != null && !FValue.equals(""))
            {
                AgentGroup = FValue.trim();
            }
            else
                AgentGroup = null;
        }
        if (FCode.equalsIgnoreCase("AccName")) {
            if( FValue != null && !FValue.equals(""))
            {
                AccName = FValue.trim();
            }
            else
                AccName = null;
        }
        if (FCode.equalsIgnoreCase("StartGetDate")) {
            if(FValue != null && !FValue.equals("")) {
                StartGetDate = fDate.getDate( FValue );
            }
            else
                StartGetDate = null;
        }
        if (FCode.equalsIgnoreCase("AppntNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                AppntNo = FValue.trim();
            }
            else
                AppntNo = null;
        }
        if (FCode.equalsIgnoreCase("SumGetMoney")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                SumGetMoney = d;
            }
        }
        if (FCode.equalsIgnoreCase("SaleChnl")) {
            if( FValue != null && !FValue.equals(""))
            {
                SaleChnl = FValue.trim();
            }
            else
                SaleChnl = null;
        }
        if (FCode.equalsIgnoreCase("ShouldDate")) {
            if(FValue != null && !FValue.equals("")) {
                ShouldDate = fDate.getDate( FValue );
            }
            else
                ShouldDate = null;
        }
        if (FCode.equalsIgnoreCase("EnterAccDate")) {
            if(FValue != null && !FValue.equals("")) {
                EnterAccDate = fDate.getDate( FValue );
            }
            else
                EnterAccDate = null;
        }
        if (FCode.equalsIgnoreCase("ConfDate")) {
            if(FValue != null && !FValue.equals("")) {
                ConfDate = fDate.getDate( FValue );
            }
            else
                ConfDate = null;
        }
        if (FCode.equalsIgnoreCase("ApproveCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                ApproveCode = FValue.trim();
            }
            else
                ApproveCode = null;
        }
        if (FCode.equalsIgnoreCase("ApproveDate")) {
            if(FValue != null && !FValue.equals("")) {
                ApproveDate = fDate.getDate( FValue );
            }
            else
                ApproveDate = null;
        }
        if (FCode.equalsIgnoreCase("GetNoticeNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                GetNoticeNo = FValue.trim();
            }
            else
                GetNoticeNo = null;
        }
        if (FCode.equalsIgnoreCase("BankCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                BankCode = FValue.trim();
            }
            else
                BankCode = null;
        }
        if (FCode.equalsIgnoreCase("BankAccNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                BankAccNo = FValue.trim();
            }
            else
                BankAccNo = null;
        }
        if (FCode.equalsIgnoreCase("Drawer")) {
            if( FValue != null && !FValue.equals(""))
            {
                Drawer = FValue.trim();
            }
            else
                Drawer = null;
        }
        if (FCode.equalsIgnoreCase("DrawerID")) {
            if( FValue != null && !FValue.equals(""))
            {
                DrawerID = FValue.trim();
            }
            else
                DrawerID = null;
        }
        if (FCode.equalsIgnoreCase("SerialNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                SerialNo = FValue.trim();
            }
            else
                SerialNo = null;
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            if( FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
                Operator = null;
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if(FValue != null && !FValue.equals("")) {
                MakeDate = fDate.getDate( FValue );
            }
            else
                MakeDate = null;
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
                MakeTime = null;
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if(FValue != null && !FValue.equals("")) {
                ModifyDate = fDate.getDate( FValue );
            }
            else
                ModifyDate = null;
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
                ModifyTime = null;
        }
        if (FCode.equalsIgnoreCase("ActualDrawer")) {
            if( FValue != null && !FValue.equals(""))
            {
                ActualDrawer = FValue.trim();
            }
            else
                ActualDrawer = null;
        }
        if (FCode.equalsIgnoreCase("ActualDrawerID")) {
            if( FValue != null && !FValue.equals(""))
            {
                ActualDrawerID = FValue.trim();
            }
            else
                ActualDrawerID = null;
        }
        if (FCode.equalsIgnoreCase("PolicyCom")) {
            if( FValue != null && !FValue.equals(""))
            {
                PolicyCom = FValue.trim();
            }
            else
                PolicyCom = null;
        }
        if (FCode.equalsIgnoreCase("InBankCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                InBankCode = FValue.trim();
            }
            else
                InBankCode = null;
        }
        if (FCode.equalsIgnoreCase("InBankAccNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                InBankAccNo = FValue.trim();
            }
            else
                InBankAccNo = null;
        }
        if (FCode.equalsIgnoreCase("BalanceOnTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                BalanceOnTime = FValue.trim();
            }
            else
                BalanceOnTime = null;
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject) return true;
        if (otherObject == null) return false;
        if (getClass() != otherObject.getClass()) return false;
        LJAGetSchema other = (LJAGetSchema)otherObject;
        return
                ActuGetNo.equals(other.getActuGetNo())
                        && OtherNo.equals(other.getOtherNo())
                        && OtherNoType.equals(other.getOtherNoType())
                        && PayMode.equals(other.getPayMode())
                        && BankOnTheWayFlag.equals(other.getBankOnTheWayFlag())
                        && BankSuccFlag.equals(other.getBankSuccFlag())
                        && SendBankCount == other.getSendBankCount()
                        && ManageCom.equals(other.getManageCom())
                        && AgentCom.equals(other.getAgentCom())
                        && AgentType.equals(other.getAgentType())
                        && AgentCode.equals(other.getAgentCode())
                        && AgentGroup.equals(other.getAgentGroup())
                        && AccName.equals(other.getAccName())
                        && fDate.getString(StartGetDate).equals(other.getStartGetDate())
                        && AppntNo.equals(other.getAppntNo())
                        && SumGetMoney == other.getSumGetMoney()
                        && SaleChnl.equals(other.getSaleChnl())
                        && fDate.getString(ShouldDate).equals(other.getShouldDate())
                        && fDate.getString(EnterAccDate).equals(other.getEnterAccDate())
                        && fDate.getString(ConfDate).equals(other.getConfDate())
                        && ApproveCode.equals(other.getApproveCode())
                        && fDate.getString(ApproveDate).equals(other.getApproveDate())
                        && GetNoticeNo.equals(other.getGetNoticeNo())
                        && BankCode.equals(other.getBankCode())
                        && BankAccNo.equals(other.getBankAccNo())
                        && Drawer.equals(other.getDrawer())
                        && DrawerID.equals(other.getDrawerID())
                        && SerialNo.equals(other.getSerialNo())
                        && Operator.equals(other.getOperator())
                        && fDate.getString(MakeDate).equals(other.getMakeDate())
                        && MakeTime.equals(other.getMakeTime())
                        && fDate.getString(ModifyDate).equals(other.getModifyDate())
                        && ModifyTime.equals(other.getModifyTime())
                        && ActualDrawer.equals(other.getActualDrawer())
                        && ActualDrawerID.equals(other.getActualDrawerID())
                        && PolicyCom.equals(other.getPolicyCom())
                        && InBankCode.equals(other.getInBankCode())
                        && InBankAccNo.equals(other.getInBankAccNo())
                        && BalanceOnTime.equals(other.getBalanceOnTime());
    }

    /**
     * 取得Schema拥有字段的数量
     * @return: int
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("ActuGetNo") ) {
            return 0;
        }
        if( strFieldName.equals("OtherNo") ) {
            return 1;
        }
        if( strFieldName.equals("OtherNoType") ) {
            return 2;
        }
        if( strFieldName.equals("PayMode") ) {
            return 3;
        }
        if( strFieldName.equals("BankOnTheWayFlag") ) {
            return 4;
        }
        if( strFieldName.equals("BankSuccFlag") ) {
            return 5;
        }
        if( strFieldName.equals("SendBankCount") ) {
            return 6;
        }
        if( strFieldName.equals("ManageCom") ) {
            return 7;
        }
        if( strFieldName.equals("AgentCom") ) {
            return 8;
        }
        if( strFieldName.equals("AgentType") ) {
            return 9;
        }
        if( strFieldName.equals("AgentCode") ) {
            return 10;
        }
        if( strFieldName.equals("AgentGroup") ) {
            return 11;
        }
        if( strFieldName.equals("AccName") ) {
            return 12;
        }
        if( strFieldName.equals("StartGetDate") ) {
            return 13;
        }
        if( strFieldName.equals("AppntNo") ) {
            return 14;
        }
        if( strFieldName.equals("SumGetMoney") ) {
            return 15;
        }
        if( strFieldName.equals("SaleChnl") ) {
            return 16;
        }
        if( strFieldName.equals("ShouldDate") ) {
            return 17;
        }
        if( strFieldName.equals("EnterAccDate") ) {
            return 18;
        }
        if( strFieldName.equals("ConfDate") ) {
            return 19;
        }
        if( strFieldName.equals("ApproveCode") ) {
            return 20;
        }
        if( strFieldName.equals("ApproveDate") ) {
            return 21;
        }
        if( strFieldName.equals("GetNoticeNo") ) {
            return 22;
        }
        if( strFieldName.equals("BankCode") ) {
            return 23;
        }
        if( strFieldName.equals("BankAccNo") ) {
            return 24;
        }
        if( strFieldName.equals("Drawer") ) {
            return 25;
        }
        if( strFieldName.equals("DrawerID") ) {
            return 26;
        }
        if( strFieldName.equals("SerialNo") ) {
            return 27;
        }
        if( strFieldName.equals("Operator") ) {
            return 28;
        }
        if( strFieldName.equals("MakeDate") ) {
            return 29;
        }
        if( strFieldName.equals("MakeTime") ) {
            return 30;
        }
        if( strFieldName.equals("ModifyDate") ) {
            return 31;
        }
        if( strFieldName.equals("ModifyTime") ) {
            return 32;
        }
        if( strFieldName.equals("ActualDrawer") ) {
            return 33;
        }
        if( strFieldName.equals("ActualDrawerID") ) {
            return 34;
        }
        if( strFieldName.equals("PolicyCom") ) {
            return 35;
        }
        if( strFieldName.equals("InBankCode") ) {
            return 36;
        }
        if( strFieldName.equals("InBankAccNo") ) {
            return 37;
        }
        if( strFieldName.equals("BalanceOnTime") ) {
            return 38;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     * @param: nFieldIndex int
     * @return: String
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "ActuGetNo";
                break;
            case 1:
                strFieldName = "OtherNo";
                break;
            case 2:
                strFieldName = "OtherNoType";
                break;
            case 3:
                strFieldName = "PayMode";
                break;
            case 4:
                strFieldName = "BankOnTheWayFlag";
                break;
            case 5:
                strFieldName = "BankSuccFlag";
                break;
            case 6:
                strFieldName = "SendBankCount";
                break;
            case 7:
                strFieldName = "ManageCom";
                break;
            case 8:
                strFieldName = "AgentCom";
                break;
            case 9:
                strFieldName = "AgentType";
                break;
            case 10:
                strFieldName = "AgentCode";
                break;
            case 11:
                strFieldName = "AgentGroup";
                break;
            case 12:
                strFieldName = "AccName";
                break;
            case 13:
                strFieldName = "StartGetDate";
                break;
            case 14:
                strFieldName = "AppntNo";
                break;
            case 15:
                strFieldName = "SumGetMoney";
                break;
            case 16:
                strFieldName = "SaleChnl";
                break;
            case 17:
                strFieldName = "ShouldDate";
                break;
            case 18:
                strFieldName = "EnterAccDate";
                break;
            case 19:
                strFieldName = "ConfDate";
                break;
            case 20:
                strFieldName = "ApproveCode";
                break;
            case 21:
                strFieldName = "ApproveDate";
                break;
            case 22:
                strFieldName = "GetNoticeNo";
                break;
            case 23:
                strFieldName = "BankCode";
                break;
            case 24:
                strFieldName = "BankAccNo";
                break;
            case 25:
                strFieldName = "Drawer";
                break;
            case 26:
                strFieldName = "DrawerID";
                break;
            case 27:
                strFieldName = "SerialNo";
                break;
            case 28:
                strFieldName = "Operator";
                break;
            case 29:
                strFieldName = "MakeDate";
                break;
            case 30:
                strFieldName = "MakeTime";
                break;
            case 31:
                strFieldName = "ModifyDate";
                break;
            case 32:
                strFieldName = "ModifyTime";
                break;
            case 33:
                strFieldName = "ActualDrawer";
                break;
            case 34:
                strFieldName = "ActualDrawerID";
                break;
            case 35:
                strFieldName = "PolicyCom";
                break;
            case 36:
                strFieldName = "InBankCode";
                break;
            case 37:
                strFieldName = "InBankAccNo";
                break;
            case 38:
                strFieldName = "BalanceOnTime";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "ACTUGETNO":
                return Schema.TYPE_STRING;
            case "OTHERNO":
                return Schema.TYPE_STRING;
            case "OTHERNOTYPE":
                return Schema.TYPE_STRING;
            case "PAYMODE":
                return Schema.TYPE_STRING;
            case "BANKONTHEWAYFLAG":
                return Schema.TYPE_STRING;
            case "BANKSUCCFLAG":
                return Schema.TYPE_STRING;
            case "SENDBANKCOUNT":
                return Schema.TYPE_INT;
            case "MANAGECOM":
                return Schema.TYPE_STRING;
            case "AGENTCOM":
                return Schema.TYPE_STRING;
            case "AGENTTYPE":
                return Schema.TYPE_STRING;
            case "AGENTCODE":
                return Schema.TYPE_STRING;
            case "AGENTGROUP":
                return Schema.TYPE_STRING;
            case "ACCNAME":
                return Schema.TYPE_STRING;
            case "STARTGETDATE":
                return Schema.TYPE_DATE;
            case "APPNTNO":
                return Schema.TYPE_STRING;
            case "SUMGETMONEY":
                return Schema.TYPE_DOUBLE;
            case "SALECHNL":
                return Schema.TYPE_STRING;
            case "SHOULDDATE":
                return Schema.TYPE_DATE;
            case "ENTERACCDATE":
                return Schema.TYPE_DATE;
            case "CONFDATE":
                return Schema.TYPE_DATE;
            case "APPROVECODE":
                return Schema.TYPE_STRING;
            case "APPROVEDATE":
                return Schema.TYPE_DATE;
            case "GETNOTICENO":
                return Schema.TYPE_STRING;
            case "BANKCODE":
                return Schema.TYPE_STRING;
            case "BANKACCNO":
                return Schema.TYPE_STRING;
            case "DRAWER":
                return Schema.TYPE_STRING;
            case "DRAWERID":
                return Schema.TYPE_STRING;
            case "SERIALNO":
                return Schema.TYPE_STRING;
            case "OPERATOR":
                return Schema.TYPE_STRING;
            case "MAKEDATE":
                return Schema.TYPE_DATE;
            case "MAKETIME":
                return Schema.TYPE_STRING;
            case "MODIFYDATE":
                return Schema.TYPE_DATE;
            case "MODIFYTIME":
                return Schema.TYPE_STRING;
            case "ACTUALDRAWER":
                return Schema.TYPE_STRING;
            case "ACTUALDRAWERID":
                return Schema.TYPE_STRING;
            case "POLICYCOM":
                return Schema.TYPE_STRING;
            case "INBANKCODE":
                return Schema.TYPE_STRING;
            case "INBANKACCNO":
                return Schema.TYPE_STRING;
            case "BALANCEONTIME":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: nFieldIndex int
     * @return: int
     **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_STRING;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_STRING;
            case 6:
                return Schema.TYPE_INT;
            case 7:
                return Schema.TYPE_STRING;
            case 8:
                return Schema.TYPE_STRING;
            case 9:
                return Schema.TYPE_STRING;
            case 10:
                return Schema.TYPE_STRING;
            case 11:
                return Schema.TYPE_STRING;
            case 12:
                return Schema.TYPE_STRING;
            case 13:
                return Schema.TYPE_DATE;
            case 14:
                return Schema.TYPE_STRING;
            case 15:
                return Schema.TYPE_DOUBLE;
            case 16:
                return Schema.TYPE_STRING;
            case 17:
                return Schema.TYPE_DATE;
            case 18:
                return Schema.TYPE_DATE;
            case 19:
                return Schema.TYPE_DATE;
            case 20:
                return Schema.TYPE_STRING;
            case 21:
                return Schema.TYPE_DATE;
            case 22:
                return Schema.TYPE_STRING;
            case 23:
                return Schema.TYPE_STRING;
            case 24:
                return Schema.TYPE_STRING;
            case 25:
                return Schema.TYPE_STRING;
            case 26:
                return Schema.TYPE_STRING;
            case 27:
                return Schema.TYPE_STRING;
            case 28:
                return Schema.TYPE_STRING;
            case 29:
                return Schema.TYPE_DATE;
            case 30:
                return Schema.TYPE_STRING;
            case 31:
                return Schema.TYPE_DATE;
            case 32:
                return Schema.TYPE_STRING;
            case 33:
                return Schema.TYPE_STRING;
            case 34:
                return Schema.TYPE_STRING;
            case 35:
                return Schema.TYPE_STRING;
            case 36:
                return Schema.TYPE_STRING;
            case 37:
                return Schema.TYPE_STRING;
            case 38:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    public String toString() {
        return "LJAGetSchema {" +
                "ActuGetNo="+ActuGetNo +
                ", OtherNo="+OtherNo +
                ", OtherNoType="+OtherNoType +
                ", PayMode="+PayMode +
                ", BankOnTheWayFlag="+BankOnTheWayFlag +
                ", BankSuccFlag="+BankSuccFlag +
                ", SendBankCount="+SendBankCount +
                ", ManageCom="+ManageCom +
                ", AgentCom="+AgentCom +
                ", AgentType="+AgentType +
                ", AgentCode="+AgentCode +
                ", AgentGroup="+AgentGroup +
                ", AccName="+AccName +
                ", StartGetDate="+StartGetDate +
                ", AppntNo="+AppntNo +
                ", SumGetMoney="+SumGetMoney +
                ", SaleChnl="+SaleChnl +
                ", ShouldDate="+ShouldDate +
                ", EnterAccDate="+EnterAccDate +
                ", ConfDate="+ConfDate +
                ", ApproveCode="+ApproveCode +
                ", ApproveDate="+ApproveDate +
                ", GetNoticeNo="+GetNoticeNo +
                ", BankCode="+BankCode +
                ", BankAccNo="+BankAccNo +
                ", Drawer="+Drawer +
                ", DrawerID="+DrawerID +
                ", SerialNo="+SerialNo +
                ", Operator="+Operator +
                ", MakeDate="+MakeDate +
                ", MakeTime="+MakeTime +
                ", ModifyDate="+ModifyDate +
                ", ModifyTime="+ModifyTime +
                ", ActualDrawer="+ActualDrawer +
                ", ActualDrawerID="+ActualDrawerID +
                ", PolicyCom="+PolicyCom +
                ", InBankCode="+InBankCode +
                ", InBankAccNo="+InBankAccNo +
                ", BalanceOnTime="+BalanceOnTime +"}";
    }
}
