/**
 * Copyright (c) 2017 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import com.sinosoft.lis.db.VMS_OUTPUT_LOGDB;
import com.sinosoft.utility.*;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * <p>ClassName: VMS_OUTPUT_LOGSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2017</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2017-10-21
 */
public class VMS_OUTPUT_LOGSchema implements Schema, Cloneable {
    // @Field
    /** 序列号 */
    private String SERIALNO;
    /** 服务类型 */
    private String SERVICETYPE;
    /** 批次号 */
    private String BATCHNO;
    /** 交易编码 */
    private String BUSSNO;
    /** 创建日期 */
    private String MAKEDATE;
    /** 创建时间 */
    private String MAKETIME;

    public static final int FIELDNUM = 6;    // 数据库表的字段个数

    private static String[] PK;                // 主键

    public CErrors mErrors;            // 错误信息

    // @Constructor
    public VMS_OUTPUT_LOGSchema() {
        mErrors = new CErrors();

        String[] pk = new String[2];
        pk[0] = "SERVICETYPE";
        pk[1] = "BATCHNO";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException {
        VMS_OUTPUT_LOGSchema cloned = (VMS_OUTPUT_LOGSchema)super.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK() {
        return PK;
    }

    public String getSERIALNO() {
        return SERIALNO;
    }
    public void setSERIALNO(String aSERIALNO) {
        SERIALNO = aSERIALNO;
    }
    public String getSERVICETYPE() {
        return SERVICETYPE;
    }
    public void setSERVICETYPE(String aSERVICETYPE) {
        SERVICETYPE = aSERVICETYPE;
    }
    public String getBATCHNO() {
        return BATCHNO;
    }
    public void setBATCHNO(String aBATCHNO) {
        BATCHNO = aBATCHNO;
    }
    public String getBUSSNO() {
        return BUSSNO;
    }
    public void setBUSSNO(String aBUSSNO) {
        BUSSNO = aBUSSNO;
    }
    public String getMAKEDATE() {
        return MAKEDATE;
    }
    public void setMAKEDATE(String aMAKEDATE) {
        MAKEDATE = aMAKEDATE;
    }
    public String getMAKETIME() {
        return MAKETIME;
    }
    public void setMAKETIME(String aMAKETIME) {
        MAKETIME = aMAKETIME;
    }

    /**
    * 使用另外一个 VMS_OUTPUT_LOGSchema 对象给 Schema 赋值
    * @param: aVMS_OUTPUT_LOGSchema VMS_OUTPUT_LOGSchema
    **/
    public void setSchema(VMS_OUTPUT_LOGSchema aVMS_OUTPUT_LOGSchema) {
        this.SERIALNO = aVMS_OUTPUT_LOGSchema.getSERIALNO();
        this.SERVICETYPE = aVMS_OUTPUT_LOGSchema.getSERVICETYPE();
        this.BATCHNO = aVMS_OUTPUT_LOGSchema.getBATCHNO();
        this.BUSSNO = aVMS_OUTPUT_LOGSchema.getBUSSNO();
        this.MAKEDATE = aVMS_OUTPUT_LOGSchema.getMAKEDATE();
        this.MAKETIME = aVMS_OUTPUT_LOGSchema.getMAKETIME();
    }

    /**
    * 使用 ResultSet 中的第 i 行给 Schema 赋值
    * @param: rs ResultSet
    * @param: i int
    * @return: boolean
    **/
    public boolean setSchema(ResultSet rs,int i) {
        try {
            //rs.absolute(i);        // 非滚动游标
            if( rs.getString("SERIALNO") == null )
                this.SERIALNO = null;
            else
                this.SERIALNO = rs.getString("SERIALNO").trim();

            if( rs.getString("SERVICETYPE") == null )
                this.SERVICETYPE = null;
            else
                this.SERVICETYPE = rs.getString("SERVICETYPE").trim();

            if( rs.getString("BATCHNO") == null )
                this.BATCHNO = null;
            else
                this.BATCHNO = rs.getString("BATCHNO").trim();

            if( rs.getString("BUSSNO") == null )
                this.BUSSNO = null;
            else
                this.BUSSNO = rs.getString("BUSSNO").trim();

            if( rs.getString("MAKEDATE") == null )
                this.MAKEDATE = null;
            else
                this.MAKEDATE = rs.getString("MAKEDATE").trim();

            if( rs.getString("MAKETIME") == null )
                this.MAKETIME = null;
            else
                this.MAKETIME = rs.getString("MAKETIME").trim();

        }
        catch(SQLException sqle) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "VMS_OUTPUT_LOGSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = ExceptionUtils.exceptionToString(sqle);
            this.mErrors .addOneError(tError);
            return false;
        }
        return true;
    }

    public VMS_OUTPUT_LOGSchema getSchema() {
        VMS_OUTPUT_LOGSchema aVMS_OUTPUT_LOGSchema = new VMS_OUTPUT_LOGSchema();
        aVMS_OUTPUT_LOGSchema.setSchema(this);
        return aVMS_OUTPUT_LOGSchema;
    }

    public VMS_OUTPUT_LOGDB getDB() {
        VMS_OUTPUT_LOGDB aDBOper = new VMS_OUTPUT_LOGDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
    * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpVMS_OUTPUT_LOG描述/A>表字段
    * @return: String 返回打包后字符串
    **/
    public String encode() {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(SERIALNO)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(SERVICETYPE)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(BATCHNO)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(BUSSNO)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MAKEDATE)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MAKETIME));
        return strReturn.toString();
    }

    /**
    * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpVMS_OUTPUT_LOG>历史记账凭证主表信息</A>表字段
    * @param: strMessage String 包含一条纪录数据的字符串
    * @return: boolean
    **/
    public boolean decode(String strMessage) {
        try {
            SERIALNO = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
            SERVICETYPE = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
            BATCHNO = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
            BUSSNO = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
            MAKEDATE = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
            MAKETIME = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
        }
        catch(NumberFormatException ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "VMS_OUTPUT_LOGSchema";
            tError.functionName = "decode";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            return false;
        }
        return true;
    }

    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("SERIALNO")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SERIALNO));
        }
        if (FCode.equalsIgnoreCase("SERVICETYPE")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SERVICETYPE));
        }
        if (FCode.equalsIgnoreCase("BATCHNO")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BATCHNO));
        }
        if (FCode.equalsIgnoreCase("BUSSNO")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BUSSNO));
        }
        if (FCode.equalsIgnoreCase("MAKEDATE")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MAKEDATE));
        }
        if (FCode.equalsIgnoreCase("MAKETIME")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MAKETIME));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(SERIALNO);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(SERVICETYPE);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(BATCHNO);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(BUSSNO);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(MAKEDATE);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(MAKETIME);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("SERIALNO")) {
            if( FValue != null && !FValue.equals(""))
            {
                SERIALNO = FValue.trim();
            }
            else
                SERIALNO = null;
        }
        if (FCode.equalsIgnoreCase("SERVICETYPE")) {
            if( FValue != null && !FValue.equals(""))
            {
                SERVICETYPE = FValue.trim();
            }
            else
                SERVICETYPE = null;
        }
        if (FCode.equalsIgnoreCase("BATCHNO")) {
            if( FValue != null && !FValue.equals(""))
            {
                BATCHNO = FValue.trim();
            }
            else
                BATCHNO = null;
        }
        if (FCode.equalsIgnoreCase("BUSSNO")) {
            if( FValue != null && !FValue.equals(""))
            {
                BUSSNO = FValue.trim();
            }
            else
                BUSSNO = null;
        }
        if (FCode.equalsIgnoreCase("MAKEDATE")) {
            if( FValue != null && !FValue.equals(""))
            {
                MAKEDATE = FValue.trim();
            }
            else
                MAKEDATE = null;
        }
        if (FCode.equalsIgnoreCase("MAKETIME")) {
            if( FValue != null && !FValue.equals(""))
            {
                MAKETIME = FValue.trim();
            }
            else
                MAKETIME = null;
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject) return true;
        if (otherObject == null) return false;
        if (getClass() != otherObject.getClass()) return false;
        VMS_OUTPUT_LOGSchema other = (VMS_OUTPUT_LOGSchema)otherObject;
        return
            SERIALNO.equals(other.getSERIALNO())
            && SERVICETYPE.equals(other.getSERVICETYPE())
            && BATCHNO.equals(other.getBATCHNO())
            && BUSSNO.equals(other.getBUSSNO())
            && MAKEDATE.equals(other.getMAKEDATE())
            && MAKETIME.equals(other.getMAKETIME());
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("SERIALNO") ) {
            return 0;
        }
        if( strFieldName.equals("SERVICETYPE") ) {
            return 1;
        }
        if( strFieldName.equals("BATCHNO") ) {
            return 2;
        }
        if( strFieldName.equals("BUSSNO") ) {
            return 3;
        }
        if( strFieldName.equals("MAKEDATE") ) {
            return 4;
        }
        if( strFieldName.equals("MAKETIME") ) {
            return 5;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "SERIALNO";
                break;
            case 1:
                strFieldName = "SERVICETYPE";
                break;
            case 2:
                strFieldName = "BATCHNO";
                break;
            case 3:
                strFieldName = "BUSSNO";
                break;
            case 4:
                strFieldName = "MAKEDATE";
                break;
            case 5:
                strFieldName = "MAKETIME";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "SERIALNO":
                return Schema.TYPE_STRING;
            case "SERVICETYPE":
                return Schema.TYPE_STRING;
            case "BATCHNO":
                return Schema.TYPE_STRING;
            case "BUSSNO":
                return Schema.TYPE_STRING;
            case "MAKEDATE":
                return Schema.TYPE_STRING;
            case "MAKETIME":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_STRING;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
}
