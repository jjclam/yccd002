/**
 * Copyright (c) 2018 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.db;

import com.sinosoft.lis.schema.LJAPayGrpSchema;
import com.sinosoft.lis.vschema.LJAPayGrpSet;
import com.sinosoft.utility.*;

import java.sql.*;

/**
 * <p>ClassName: LJAPayGrpDB </p>
 * <p>Description: DB层数据库操作类文件 </p>
 * <p>Copyright: Copyright (c) 2018</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2018-06-19
 */
public class LJAPayGrpDB extends LJAPayGrpSchema {
    // @Field
    private Connection con;
    private DBOper db;
    /**
    * flag = true: 传入Connection
    * flag = false: 不传入Connection
    **/
    private boolean mflag = false;

    public CErrors mErrors = new CErrors();        // 错误信息

    /**
     * 为批量操作而准备的语句和游标对象
     */
    private ResultSet mResultSet = null;
    private Statement mStatement = null;
    // @Constructor
    public LJAPayGrpDB(Connection tConnection ) {
        con = tConnection;
        db = new DBOper( con, "LJAPayGrp" );
        mflag = true;
    }

    public LJAPayGrpDB() {
        con = null;
        db = new DBOper( "LJAPayGrp" );
        mflag = false;
    }

    // @Method
    public boolean deleteSQL() {
        LJAPayGrpSchema tSchema = this.getSchema();
        if (db.deleteSQL(tSchema)) {
             return true;
        } else {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LJAPayGrpDB";
            tError.functionName = "deleteSQL";
            tError.errorMessage = "操作失败!";
            this.mErrors .addOneError(tError);
            return false;
        }
    }

    public int getCount() {
        LJAPayGrpSchema tSchema = this.getSchema();

        int tCount = db.getCount(tSchema);
        if (tCount < 0) {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LJAPayGrpDB";
            tError.functionName = "getCount";
            tError.errorMessage = "操作失败!";
            this.mErrors .addOneError(tError);

            return -1;
        }

        return tCount;
    }

    public boolean delete() {
        PreparedStatement pstmt = null;

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try {
            pstmt = con.prepareStatement("DELETE FROM LJAPayGrp WHERE  1=1  AND GrpPolNo = ? AND PayCount = ? AND PayNo = ? AND PayType = ?");
            if(this.getGrpPolNo() == null || this.getGrpPolNo().equals("null")) {
            	pstmt.setNull(1, 12);
            } else {
            	pstmt.setString(1, this.getGrpPolNo());
            }
            pstmt.setInt(2, this.getPayCount());
            if(this.getPayNo() == null || this.getPayNo().equals("null")) {
            	pstmt.setNull(3, 12);
            } else {
            	pstmt.setString(3, this.getPayNo());
            }
            if(this.getPayType() == null || this.getPayType().equals("null")) {
            	pstmt.setNull(4, 12);
            } else {
            	pstmt.setString(4, this.getPayType());
            }
            pstmt.executeUpdate();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LJAPayGrpDB";
            tError.functionName = "delete()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);
            ex.printStackTrace();

            try {
                pstmt.close();
            } catch (Exception e){}

            if(!mflag) {
                try {
                    con.close();
                } catch (Exception e){}
            }

            return false;
        }

        if(!mflag) {
            try {
                con.close();
            } catch (Exception e){}
        }

        return true;
    }

    public boolean update() {
        PreparedStatement pstmt = null;

        SQLString sqlObj = new SQLString("LJAPayGrp");
        sqlObj.setSQL(2, this);
        String sql = sqlObj.getSQL();

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try {
            pstmt = con.prepareStatement("UPDATE LJAPayGrp SET  GrpPolNo = ? , PayCount = ? , GrpContNo = ? , ManageCom = ? , AgentCom = ? , AgentType = ? , RiskCode = ? , AgentCode = ? , AgentGroup = ? , PayTypeFlag = ? , AppntNo = ? , PayNo = ? , EndorsementNo = ? , SumDuePayMoney = ? , SumActuPayMoney = ? , PayIntv = ? , PayDate = ? , PayType = ? , EnterAccDate = ? , ConfDate = ? , LastPayToDate = ? , CurPayToDate = ? , ApproveCode = ? , ApproveDate = ? , ApproveTime = ? , SerialNo = ? , GetNoticeNo = ? , Operator = ? , MakeDate = ? , MakeTime = ? , ModifyDate = ? , ModifyTime = ? , FeeMoney = ? , OperState = ? , Currency = ? , BussType = ? , ComCode = ? , ModifyOperator = ? , NetAmount = ? , TaxAmount = ? , Tax = ? , PolicyYear = ? , OrderPay = ? WHERE  1=1  AND GrpPolNo = ? AND PayCount = ? AND PayNo = ? AND PayType = ?");
            if(this.getGrpPolNo() == null || this.getGrpPolNo().equals("null")) {
            	pstmt.setNull(1, 12);
            } else {
            	pstmt.setString(1, this.getGrpPolNo());
            }
            pstmt.setInt(2, this.getPayCount());
            if(this.getGrpContNo() == null || this.getGrpContNo().equals("null")) {
            	pstmt.setNull(3, 12);
            } else {
            	pstmt.setString(3, this.getGrpContNo());
            }
            if(this.getManageCom() == null || this.getManageCom().equals("null")) {
            	pstmt.setNull(4, 12);
            } else {
            	pstmt.setString(4, this.getManageCom());
            }
            if(this.getAgentCom() == null || this.getAgentCom().equals("null")) {
            	pstmt.setNull(5, 12);
            } else {
            	pstmt.setString(5, this.getAgentCom());
            }
            if(this.getAgentType() == null || this.getAgentType().equals("null")) {
            	pstmt.setNull(6, 12);
            } else {
            	pstmt.setString(6, this.getAgentType());
            }
            if(this.getRiskCode() == null || this.getRiskCode().equals("null")) {
            	pstmt.setNull(7, 12);
            } else {
            	pstmt.setString(7, this.getRiskCode());
            }
            if(this.getAgentCode() == null || this.getAgentCode().equals("null")) {
            	pstmt.setNull(8, 12);
            } else {
            	pstmt.setString(8, this.getAgentCode());
            }
            if(this.getAgentGroup() == null || this.getAgentGroup().equals("null")) {
            	pstmt.setNull(9, 12);
            } else {
            	pstmt.setString(9, this.getAgentGroup());
            }
            if(this.getPayTypeFlag() == null || this.getPayTypeFlag().equals("null")) {
            	pstmt.setNull(10, 12);
            } else {
            	pstmt.setString(10, this.getPayTypeFlag());
            }
            if(this.getAppntNo() == null || this.getAppntNo().equals("null")) {
            	pstmt.setNull(11, 12);
            } else {
            	pstmt.setString(11, this.getAppntNo());
            }
            if(this.getPayNo() == null || this.getPayNo().equals("null")) {
            	pstmt.setNull(12, 12);
            } else {
            	pstmt.setString(12, this.getPayNo());
            }
            if(this.getEndorsementNo() == null || this.getEndorsementNo().equals("null")) {
            	pstmt.setNull(13, 12);
            } else {
            	pstmt.setString(13, this.getEndorsementNo());
            }
            pstmt.setDouble(14, this.getSumDuePayMoney());
            pstmt.setDouble(15, this.getSumActuPayMoney());
            pstmt.setInt(16, this.getPayIntv());
            if(this.getPayDate() == null || this.getPayDate().equals("null")) {
            	pstmt.setNull(17, 93);
            } else {
            	pstmt.setDate(17, Date.valueOf(this.getPayDate()));
            }
            if(this.getPayType() == null || this.getPayType().equals("null")) {
            	pstmt.setNull(18, 12);
            } else {
            	pstmt.setString(18, this.getPayType());
            }
            if(this.getEnterAccDate() == null || this.getEnterAccDate().equals("null")) {
            	pstmt.setNull(19, 93);
            } else {
            	pstmt.setDate(19, Date.valueOf(this.getEnterAccDate()));
            }
            if(this.getConfDate() == null || this.getConfDate().equals("null")) {
            	pstmt.setNull(20, 93);
            } else {
            	pstmt.setDate(20, Date.valueOf(this.getConfDate()));
            }
            if(this.getLastPayToDate() == null || this.getLastPayToDate().equals("null")) {
            	pstmt.setNull(21, 93);
            } else {
            	pstmt.setDate(21, Date.valueOf(this.getLastPayToDate()));
            }
            if(this.getCurPayToDate() == null || this.getCurPayToDate().equals("null")) {
            	pstmt.setNull(22, 93);
            } else {
            	pstmt.setDate(22, Date.valueOf(this.getCurPayToDate()));
            }
            if(this.getApproveCode() == null || this.getApproveCode().equals("null")) {
            	pstmt.setNull(23, 12);
            } else {
            	pstmt.setString(23, this.getApproveCode());
            }
            if(this.getApproveDate() == null || this.getApproveDate().equals("null")) {
            	pstmt.setNull(24, 93);
            } else {
            	pstmt.setDate(24, Date.valueOf(this.getApproveDate()));
            }
            if(this.getApproveTime() == null || this.getApproveTime().equals("null")) {
            	pstmt.setNull(25, 12);
            } else {
            	pstmt.setString(25, this.getApproveTime());
            }
            if(this.getSerialNo() == null || this.getSerialNo().equals("null")) {
            	pstmt.setNull(26, 12);
            } else {
            	pstmt.setString(26, this.getSerialNo());
            }
            if(this.getGetNoticeNo() == null || this.getGetNoticeNo().equals("null")) {
            	pstmt.setNull(27, 12);
            } else {
            	pstmt.setString(27, this.getGetNoticeNo());
            }
            if(this.getOperator() == null || this.getOperator().equals("null")) {
            	pstmt.setNull(28, 12);
            } else {
            	pstmt.setString(28, this.getOperator());
            }
            if(this.getMakeDate() == null || this.getMakeDate().equals("null")) {
            	pstmt.setNull(29, 93);
            } else {
            	pstmt.setDate(29, Date.valueOf(this.getMakeDate()));
            }
            if(this.getMakeTime() == null || this.getMakeTime().equals("null")) {
            	pstmt.setNull(30, 12);
            } else {
            	pstmt.setString(30, this.getMakeTime());
            }
            if(this.getModifyDate() == null || this.getModifyDate().equals("null")) {
            	pstmt.setNull(31, 93);
            } else {
            	pstmt.setDate(31, Date.valueOf(this.getModifyDate()));
            }
            if(this.getModifyTime() == null || this.getModifyTime().equals("null")) {
            	pstmt.setNull(32, 12);
            } else {
            	pstmt.setString(32, this.getModifyTime());
            }
            pstmt.setDouble(33, this.getFeeMoney());
            if(this.getOperState() == null || this.getOperState().equals("null")) {
            	pstmt.setNull(34, 12);
            } else {
            	pstmt.setString(34, this.getOperState());
            }
            if(this.getCurrency() == null || this.getCurrency().equals("null")) {
            	pstmt.setNull(35, 12);
            } else {
            	pstmt.setString(35, this.getCurrency());
            }
            if(this.getBussType() == null || this.getBussType().equals("null")) {
            	pstmt.setNull(36, 12);
            } else {
            	pstmt.setString(36, this.getBussType());
            }
            if(this.getComCode() == null || this.getComCode().equals("null")) {
            	pstmt.setNull(37, 12);
            } else {
            	pstmt.setString(37, this.getComCode());
            }
            if(this.getModifyOperator() == null || this.getModifyOperator().equals("null")) {
            	pstmt.setNull(38, 12);
            } else {
            	pstmt.setString(38, this.getModifyOperator());
            }
            pstmt.setDouble(39, this.getNetAmount());
            pstmt.setDouble(40, this.getTaxAmount());
            pstmt.setDouble(41, this.getTax());
            if(this.getPolicyYear() == null || this.getPolicyYear().equals("null")) {
            	pstmt.setNull(42, 12);
            } else {
            	pstmt.setString(42, this.getPolicyYear());
            }
            if(this.getOrderPay() == null || this.getOrderPay().equals("null")) {
            	pstmt.setNull(43, 12);
            } else {
            	pstmt.setString(43, this.getOrderPay());
            }
            // set where condition
            if(this.getGrpPolNo() == null || this.getGrpPolNo().equals("null")) {
            	pstmt.setNull(44, 12);
            } else {
            	pstmt.setString(44, this.getGrpPolNo());
            }
            pstmt.setInt(45, this.getPayCount());
            if(this.getPayNo() == null || this.getPayNo().equals("null")) {
            	pstmt.setNull(46, 12);
            } else {
            	pstmt.setString(46, this.getPayNo());
            }
            if(this.getPayType() == null || this.getPayType().equals("null")) {
            	pstmt.setNull(47, 12);
            } else {
            	pstmt.setString(47, this.getPayType());
            }
            pstmt.executeUpdate();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LJAPayGrpDB";
            tError.functionName = "update()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);
            ex.printStackTrace();

            try {
                pstmt.close();
            } catch (Exception e){}

            if( !mflag ) {
                try {
                    con.close();
                } catch (Exception e){}
            }

            return false;
        }

        if(!mflag) {
            try {
                con.close();
            } catch (Exception e){}
        }

        return true;
    }

    public boolean insert() {
        PreparedStatement pstmt = null;

        SQLString sqlObj = new SQLString("LJAPayGrp");
        sqlObj.setSQL(1, this);
        String sql = sqlObj.getSQL();

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try {
            pstmt = con.prepareStatement("INSERT INTO LJAPayGrp VALUES( ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ?)");
            if(this.getGrpPolNo() == null || this.getGrpPolNo().equals("null")) {
            	pstmt.setNull(1, 12);
            } else {
            	pstmt.setString(1, this.getGrpPolNo());
            }
            pstmt.setInt(2, this.getPayCount());
            if(this.getGrpContNo() == null || this.getGrpContNo().equals("null")) {
            	pstmt.setNull(3, 12);
            } else {
            	pstmt.setString(3, this.getGrpContNo());
            }
            if(this.getManageCom() == null || this.getManageCom().equals("null")) {
            	pstmt.setNull(4, 12);
            } else {
            	pstmt.setString(4, this.getManageCom());
            }
            if(this.getAgentCom() == null || this.getAgentCom().equals("null")) {
            	pstmt.setNull(5, 12);
            } else {
            	pstmt.setString(5, this.getAgentCom());
            }
            if(this.getAgentType() == null || this.getAgentType().equals("null")) {
            	pstmt.setNull(6, 12);
            } else {
            	pstmt.setString(6, this.getAgentType());
            }
            if(this.getRiskCode() == null || this.getRiskCode().equals("null")) {
            	pstmt.setNull(7, 12);
            } else {
            	pstmt.setString(7, this.getRiskCode());
            }
            if(this.getAgentCode() == null || this.getAgentCode().equals("null")) {
            	pstmt.setNull(8, 12);
            } else {
            	pstmt.setString(8, this.getAgentCode());
            }
            if(this.getAgentGroup() == null || this.getAgentGroup().equals("null")) {
            	pstmt.setNull(9, 12);
            } else {
            	pstmt.setString(9, this.getAgentGroup());
            }
            if(this.getPayTypeFlag() == null || this.getPayTypeFlag().equals("null")) {
            	pstmt.setNull(10, 12);
            } else {
            	pstmt.setString(10, this.getPayTypeFlag());
            }
            if(this.getAppntNo() == null || this.getAppntNo().equals("null")) {
            	pstmt.setNull(11, 12);
            } else {
            	pstmt.setString(11, this.getAppntNo());
            }
            if(this.getPayNo() == null || this.getPayNo().equals("null")) {
            	pstmt.setNull(12, 12);
            } else {
            	pstmt.setString(12, this.getPayNo());
            }
            if(this.getEndorsementNo() == null || this.getEndorsementNo().equals("null")) {
            	pstmt.setNull(13, 12);
            } else {
            	pstmt.setString(13, this.getEndorsementNo());
            }
            pstmt.setDouble(14, this.getSumDuePayMoney());
            pstmt.setDouble(15, this.getSumActuPayMoney());
            pstmt.setInt(16, this.getPayIntv());
            if(this.getPayDate() == null || this.getPayDate().equals("null")) {
            	pstmt.setNull(17, 93);
            } else {
            	pstmt.setDate(17, Date.valueOf(this.getPayDate()));
            }
            if(this.getPayType() == null || this.getPayType().equals("null")) {
            	pstmt.setNull(18, 12);
            } else {
            	pstmt.setString(18, this.getPayType());
            }
            if(this.getEnterAccDate() == null || this.getEnterAccDate().equals("null")) {
            	pstmt.setNull(19, 93);
            } else {
            	pstmt.setDate(19, Date.valueOf(this.getEnterAccDate()));
            }
            if(this.getConfDate() == null || this.getConfDate().equals("null")) {
            	pstmt.setNull(20, 93);
            } else {
            	pstmt.setDate(20, Date.valueOf(this.getConfDate()));
            }
            if(this.getLastPayToDate() == null || this.getLastPayToDate().equals("null")) {
            	pstmt.setNull(21, 93);
            } else {
            	pstmt.setDate(21, Date.valueOf(this.getLastPayToDate()));
            }
            if(this.getCurPayToDate() == null || this.getCurPayToDate().equals("null")) {
            	pstmt.setNull(22, 93);
            } else {
            	pstmt.setDate(22, Date.valueOf(this.getCurPayToDate()));
            }
            if(this.getApproveCode() == null || this.getApproveCode().equals("null")) {
            	pstmt.setNull(23, 12);
            } else {
            	pstmt.setString(23, this.getApproveCode());
            }
            if(this.getApproveDate() == null || this.getApproveDate().equals("null")) {
            	pstmt.setNull(24, 93);
            } else {
            	pstmt.setDate(24, Date.valueOf(this.getApproveDate()));
            }
            if(this.getApproveTime() == null || this.getApproveTime().equals("null")) {
            	pstmt.setNull(25, 12);
            } else {
            	pstmt.setString(25, this.getApproveTime());
            }
            if(this.getSerialNo() == null || this.getSerialNo().equals("null")) {
            	pstmt.setNull(26, 12);
            } else {
            	pstmt.setString(26, this.getSerialNo());
            }
            if(this.getGetNoticeNo() == null || this.getGetNoticeNo().equals("null")) {
            	pstmt.setNull(27, 12);
            } else {
            	pstmt.setString(27, this.getGetNoticeNo());
            }
            if(this.getOperator() == null || this.getOperator().equals("null")) {
            	pstmt.setNull(28, 12);
            } else {
            	pstmt.setString(28, this.getOperator());
            }
            if(this.getMakeDate() == null || this.getMakeDate().equals("null")) {
            	pstmt.setNull(29, 93);
            } else {
            	pstmt.setDate(29, Date.valueOf(this.getMakeDate()));
            }
            if(this.getMakeTime() == null || this.getMakeTime().equals("null")) {
            	pstmt.setNull(30, 12);
            } else {
            	pstmt.setString(30, this.getMakeTime());
            }
            if(this.getModifyDate() == null || this.getModifyDate().equals("null")) {
            	pstmt.setNull(31, 93);
            } else {
            	pstmt.setDate(31, Date.valueOf(this.getModifyDate()));
            }
            if(this.getModifyTime() == null || this.getModifyTime().equals("null")) {
            	pstmt.setNull(32, 12);
            } else {
            	pstmt.setString(32, this.getModifyTime());
            }
            pstmt.setDouble(33, this.getFeeMoney());
            if(this.getOperState() == null || this.getOperState().equals("null")) {
            	pstmt.setNull(34, 12);
            } else {
            	pstmt.setString(34, this.getOperState());
            }
            if(this.getCurrency() == null || this.getCurrency().equals("null")) {
            	pstmt.setNull(35, 12);
            } else {
            	pstmt.setString(35, this.getCurrency());
            }
            if(this.getBussType() == null || this.getBussType().equals("null")) {
            	pstmt.setNull(36, 12);
            } else {
            	pstmt.setString(36, this.getBussType());
            }
            if(this.getComCode() == null || this.getComCode().equals("null")) {
            	pstmt.setNull(37, 12);
            } else {
            	pstmt.setString(37, this.getComCode());
            }
            if(this.getModifyOperator() == null || this.getModifyOperator().equals("null")) {
            	pstmt.setNull(38, 12);
            } else {
            	pstmt.setString(38, this.getModifyOperator());
            }
            pstmt.setDouble(39, this.getNetAmount());
            pstmt.setDouble(40, this.getTaxAmount());
            pstmt.setDouble(41, this.getTax());
            if(this.getPolicyYear() == null || this.getPolicyYear().equals("null")) {
            	pstmt.setNull(42, 12);
            } else {
            	pstmt.setString(42, this.getPolicyYear());
            }
            if(this.getOrderPay() == null || this.getOrderPay().equals("null")) {
            	pstmt.setNull(43, 12);
            } else {
            	pstmt.setString(43, this.getOrderPay());
            }
            // execute sql
            pstmt.executeUpdate();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LJAPayGrpDB";
            tError.functionName = "insert()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors.addOneError(tError);
            ex.printStackTrace();

            try {
                pstmt.close();
            } catch (Exception e){}

            if( !mflag ) {
                try {
                    con.close();
                } catch (Exception e){}
            }

            return false;
        }

        if(!mflag) {
            try {
                con.close();
            } catch (Exception e){}
        }

        return true;
    }

    public boolean getInfo() {
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        if(!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            pstmt = con.prepareStatement("SELECT * FROM LJAPayGrp WHERE  1=1  AND GrpPolNo = ? AND PayCount = ? AND PayNo = ? AND PayType = ?", 
                ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
            if(this.getGrpPolNo() == null || this.getGrpPolNo().equals("null")) {
            	pstmt.setNull(1, 12);
            } else {
            	pstmt.setString(1, this.getGrpPolNo());
            }
            pstmt.setInt(2, this.getPayCount());
            if(this.getPayNo() == null || this.getPayNo().equals("null")) {
            	pstmt.setNull(3, 12);
            } else {
            	pstmt.setString(3, this.getPayNo());
            }
            if(this.getPayType() == null || this.getPayType().equals("null")) {
            	pstmt.setNull(4, 12);
            } else {
            	pstmt.setString(4, this.getPayType());
            }
            rs = pstmt.executeQuery();
            int i = 0;
            while (rs.next()) {
                i++;
                if (!this.setSchema(rs,i)) {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "LJAPayGrpDB";
                    tError.functionName = "getInfo";
                    tError.errorMessage = "取数失败!";
                    this.mErrors .addOneError(tError);

                    try{ rs.close(); } catch( Exception ex ) {}
                    try{ pstmt.close(); } catch( Exception ex1 ) {}

                    if (!mflag) {
                        try {
                            con.close();
                        }
                        catch(Exception et){}
                    }
                    return false;
                }
                break;
            }
            try{ rs.close(); } catch( Exception ex2 ) {}
            try{ pstmt.close(); } catch( Exception ex3 ) {}

            if( i == 0 ) {
                if (!mflag) {
                    try {
                        con.close();
                    }
                    catch(Exception et){}
                }
                return false;
            }
        }
        catch(Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LJAPayGrpDB";
            tError.functionName = "getInfo";
            tError.errorMessage = ExceptionUtils.exceptionToString(e);
            this.mErrors .addOneError(tError);

            try{ rs.close(); } catch( Exception ex ) {}
            try{ pstmt.close(); } catch( Exception ex1 ) {}

            if (!mflag) {
                try {
                    con.close();
                }
                catch(Exception et){}
            }
            return false;
        }
        // 断开数据库连接
        if (!mflag) {
            try {
                con.close();
            }
            catch(Exception e){}
        }

        return true;
    }

    public LJAPayGrpSet query() {
        Statement stmt = null;
        ResultSet rs = null;
        LJAPayGrpSet aLJAPayGrpSet = new LJAPayGrpSet();

        if (!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            stmt = con.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
            SQLString sqlObj = new SQLString("LJAPayGrp");
            LJAPayGrpSchema aSchema = this.getSchema();
            sqlObj.setSQL(5, aSchema);
            String sql = sqlObj.getSQL();

            rs = stmt.executeQuery(sql);
            int i = 0;
            while (rs.next()) {
                i++;
                if (i>10000) {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "LJAPayGrpDB";
                    tError.functionName = "query";
                    tError.errorMessage = "SQL语句返回结果集太多，请换用其他方式!";
                    this.mErrors .addOneError(tError);
                        break;
                }
                LJAPayGrpSchema s1 = new LJAPayGrpSchema();
                s1.setSchema(rs,i);
                aLJAPayGrpSet.add(s1);
            }
            try{ rs.close(); } catch( Exception ex ) {}
            try{ stmt.close(); } catch( Exception ex1 ) {}
        }
        catch(Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LJAPayGrpDB";
            tError.functionName = "query";
            tError.errorMessage = ExceptionUtils.exceptionToString(e);
            this.mErrors.addOneError(tError);
            e.printStackTrace();

            try { rs.close(); } catch (Exception ex2) { ex2.printStackTrace(); }
            try { stmt.close(); } catch(Exception ex3) { ex3.printStackTrace(); }

            if (!mflag) {
                try {
                    con.close();
                }
                catch(Exception et){et.printStackTrace();}
            }
        }

        if (!mflag) {
            try {
                con.close();
            }
            catch(Exception e){e.printStackTrace();}
        }

        return aLJAPayGrpSet;
    }

    public LJAPayGrpSet executeQuery(String sql)  {
        Statement stmt = null;
        ResultSet rs = null;
        LJAPayGrpSet aLJAPayGrpSet = new LJAPayGrpSet();

        if (!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            stmt = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);

            rs = stmt.executeQuery(StrTool.GBKToUnicode(sql));
            int i = 0;
            while (rs.next()) {
                i++;
                if (i>10000) {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "LJAPayGrpDB";
                    tError.functionName = "executeQuery";
                    tError.errorMessage = "SQL语句返回结果集太多，请换用其他方式!";
                    this.mErrors .addOneError(tError);
                        break;
                }
                LJAPayGrpSchema s1 = new LJAPayGrpSchema();
                if (!s1.setSchema(rs,i)) {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "LJAPayGrpDB";
                    tError.functionName = "executeQuery";
                    tError.errorMessage = "sql语句有误，请查看表名及字段名信息!";
                    this.mErrors .addOneError(tError);
                }
                aLJAPayGrpSet.add(s1);
            }
            try{ rs.close(); } catch( Exception ex ) {}
            try{ stmt.close(); } catch( Exception ex1 ) {}
        }
        catch(Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LJAPayGrpDB";
            tError.functionName = "executeQuery";
            tError.errorMessage = ExceptionUtils.exceptionToString(e);
            this.mErrors .addOneError(tError);

            try{ rs.close(); } catch( Exception ex2 ) {}
            try{ stmt.close(); } catch( Exception ex3 ) {}

            if (!mflag) {
                try {
                    con.close();
                }
                catch(Exception et){}
            }
        }

        if (!mflag) {
            try {
                con.close();
            }
            catch(Exception e){}
        }

        return aLJAPayGrpSet;
    }

    public LJAPayGrpSet query(int nStart, int nCount) {
        Statement stmt = null;
        ResultSet rs = null;
        LJAPayGrpSet aLJAPayGrpSet = new LJAPayGrpSet();

        if (!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            stmt = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);
            SQLString sqlObj = new SQLString("LJAPayGrp");
            LJAPayGrpSchema aSchema = this.getSchema();
            sqlObj.setSQL(5,aSchema);
            String sql = sqlObj.getSQL();

            rs = stmt.executeQuery(sql);
            int i = 0;
            while (rs.next()) {
                i++;

                if( i < nStart ) {
                    continue;
                }

                if( i >= nStart + nCount ) {
                    break;
                }

                LJAPayGrpSchema s1 = new LJAPayGrpSchema();
                s1.setSchema(rs,i);
                aLJAPayGrpSet.add(s1);
            }
            try{ rs.close(); } catch( Exception ex ) {}
            try{ stmt.close(); } catch( Exception ex1 ) {}
        }
        catch(Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LJAPayGrpDB";
            tError.functionName = "query";
            tError.errorMessage = ExceptionUtils.exceptionToString(e);
            this.mErrors .addOneError(tError);

            try{ rs.close(); } catch( Exception ex2 ) {}
            try{ stmt.close(); } catch( Exception ex3 ) {}

            if (!mflag) {
                try {
                    con.close();
                }
                catch(Exception et){}
            }
        }

        if (!mflag) {
            try {
                con.close();
            }
            catch(Exception e){}
        }

        return aLJAPayGrpSet;
    }

    public LJAPayGrpSet executeQuery(String sql, int nStart, int nCount) {
        Statement stmt = null;
        ResultSet rs = null;
        LJAPayGrpSet aLJAPayGrpSet = new LJAPayGrpSet();

        if (!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            stmt = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);

            rs = stmt.executeQuery(StrTool.GBKToUnicode(sql));
            int i = 0;
            while (rs.next()) {
                i++;

                if( i < nStart ) {
                    continue;
                }

                if( i >= nStart + nCount ) {
                    break;
                }

                LJAPayGrpSchema s1 = new LJAPayGrpSchema();
                if (!s1.setSchema(rs,i)) {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "LJAPayGrpDB";
                    tError.functionName = "executeQuery";
                    tError.errorMessage = "sql语句有误，请查看表名及字段名信息!";
                    this.mErrors .addOneError(tError);
                }
                aLJAPayGrpSet.add(s1);
            }
            try{ rs.close(); } catch( Exception ex ) {}
            try{ stmt.close(); } catch( Exception ex1 ) {}
        }
        catch(Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LJAPayGrpDB";
            tError.functionName = "executeQuery";
            tError.errorMessage = ExceptionUtils.exceptionToString(e);
            this.mErrors .addOneError(tError);

            try{ rs.close(); } catch( Exception ex2 ) {}
            try{ stmt.close(); } catch( Exception ex3 ) {}

            if (!mflag) {
                try {
                    con.close();
                }
                catch(Exception et){}
            }
        }

        if (!mflag) {
            try {
                con.close();
            }
            catch(Exception e){}
        }

        return aLJAPayGrpSet;
    }

    public boolean update(String strWherePart) {
        Statement stmt = null;

        if (!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            stmt = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);
            SQLString sqlObj = new SQLString("LJAPayGrp");
            LJAPayGrpSchema aSchema = this.getSchema();
            sqlObj.setSQL(2,aSchema);
            String sql = "update LJAPayGrp " + sqlObj.getUpdPart() + " where " + strWherePart;

            int operCount = stmt.executeUpdate(sql);
            if (operCount == 0) {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "LJAPayGrpDB";
                tError.functionName = "update";
                tError.errorMessage = "更新数据失败!";
                this.mErrors .addOneError(tError);

                if (!mflag) {
                    try {
                        con.close();
                    }
                    catch(Exception et){}
                }
                return false;
            }
        }
        catch(Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LJAPayGrpDB";
            tError.functionName = "update";
            tError.errorMessage = ExceptionUtils.exceptionToString(e);
            this.mErrors .addOneError(tError);

            try{ stmt.close(); } catch( Exception ex1 ) {}

            if (!mflag) {
                try {
                    con.close();
                }
                catch(Exception et){}
            }
            return false;
        }
        // 断开数据库连接
        if (!mflag) {
            try {
                con.close();
            }
            catch(Exception e){}
        }

        return true;
    }

    /**
     * 准备数据查询条件
     * @param strSQL String
     * @return boolean
     */
    public boolean prepareData(String strSQL) {
        if (mResultSet != null) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LJAPayGrpDB";
            tError.functionName = "prepareData";
            tError.errorMessage = "数据集非空，程序在准备数据集之后，没有关闭！";
            this.mErrors.addOneError(tError);
            return false;
        }

        if (!mflag) {
            con = DBConnPool.getConnection();
        }
        try {
            mStatement = con.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
            mResultSet = mStatement.executeQuery(StrTool.GBKToUnicode(strSQL));
        }
        catch (Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LJAPayGrpDB";
            tError.functionName = "prepareData";
            tError.errorMessage = ExceptionUtils.exceptionToString(e);
            this.mErrors.addOneError(tError);
            try {
                mResultSet.close();
            }
            catch (Exception ex2) {}
            try {
                mStatement.close();
            }
            catch (Exception ex3) {}
            if (!mflag) {
                try {
                    con.close();
                }
                catch (Exception et) {}
            }
            return false;
        }

        if (!mflag) {
            try {
                con.close();
            }
            catch (Exception e) {}
        }
        return true;
    }

    /**
     * 获取数据集
     * @return boolean
     */
    public boolean hasMoreData() {
        boolean flag = true;
        if (null == mResultSet) {
            CError tError = new CError();
            tError.moduleName = "LJAPayGrpDB";
            tError.functionName = "hasMoreData";
            tError.errorMessage = "数据集为空，请先准备数据集！";
            this.mErrors.addOneError(tError);
            return false;
        }
        try {
            flag = mResultSet.next();
        }
        catch (Exception ex) {
            CError tError = new CError();
            tError.moduleName = "LJAPayGrpDB";
            tError.functionName = "hasMoreData";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors.addOneError(tError);
            try {
                mResultSet.close();
                mResultSet = null;
            }
            catch (Exception ex2) {}
            try {
                mStatement.close();
                mStatement = null;
            }
            catch (Exception ex3) {}
            if (!mflag) {
                try {
                    con.close();
                }
                catch (Exception et) {}
            }
            return false;
        }
        return flag;
    }
    /**
     * 获取定量数据
     * @return LJAPayGrpSet
     */
    public LJAPayGrpSet getData() {
        int tCount = 0;
        LJAPayGrpSet tLJAPayGrpSet = new LJAPayGrpSet();
        LJAPayGrpSchema tLJAPayGrpSchema = null;
        if (null == mResultSet) {
            CError tError = new CError();
            tError.moduleName = "LJAPayGrpDB";
            tError.functionName = "getData";
            tError.errorMessage = "数据集为空，请先准备数据集！";
            this.mErrors.addOneError(tError);
            return null;
        }
        try {
            tCount = 1;
            tLJAPayGrpSchema = new LJAPayGrpSchema();
            tLJAPayGrpSchema.setSchema(mResultSet, 1);
            tLJAPayGrpSet.add(tLJAPayGrpSchema);
            //注意mResultSet.next()的作用
            while (tCount++ < SysConst.FETCHCOUNT) {
                if (mResultSet.next()) {
                    tLJAPayGrpSchema = new LJAPayGrpSchema();
                    tLJAPayGrpSchema.setSchema(mResultSet, 1);
                    tLJAPayGrpSet.add(tLJAPayGrpSchema);
                }
            }
        }
        catch (Exception ex) {
            CError tError = new CError();
            tError.moduleName = "LJAPayGrpDB";
            tError.functionName = "getData";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors.addOneError(tError);
            try {
                mResultSet.close();
                mResultSet = null;
            }
            catch (Exception ex2) {}
            try {
                mStatement.close();
                mStatement = null;
            }
            catch (Exception ex3) {}
            if (!mflag) {
                try {
                    con.close();
                }
                catch (Exception et) {}
            }
            return null;
        }
        return tLJAPayGrpSet;
    }
    /**
     * 关闭数据集
     * @return boolean
     */
    public boolean closeData() {
        boolean flag = true;
        try {
            if (null == mResultSet) {
                CError tError = new CError();
                tError.moduleName = "LJAPayGrpDB";
                tError.functionName = "closeData";
                tError.errorMessage = "数据集已经关闭了！";
                this.mErrors.addOneError(tError);
                flag = false;
            } else {
                mResultSet.close();
                mResultSet = null;
            }
        }
        catch (Exception ex2) {
            CError tError = new CError();
            tError.moduleName = "LJAPayGrpDB";
            tError.functionName = "closeData";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex2);
            this.mErrors.addOneError(tError);
            flag = false;
        }
        try {
            if (null == mStatement) {
                CError tError = new CError();
                tError.moduleName = "LJAPayGrpDB";
                tError.functionName = "closeData";
                tError.errorMessage = "语句已经关闭了！";
                this.mErrors.addOneError(tError);
                flag = false;
            } else {
                mStatement.close();
                mStatement = null;
            }
        }
        catch (Exception ex3) {
            CError tError = new CError();
            tError.moduleName = "LJAPayGrpDB";
            tError.functionName = "closeData";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex3);
            this.mErrors.addOneError(tError);
            flag = false;
        }
        return flag;
    }
}
