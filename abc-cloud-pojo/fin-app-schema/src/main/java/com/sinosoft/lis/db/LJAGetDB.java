/**
 * Copyright (c) 2018 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.db;

import com.sinosoft.lis.schema.LJAGetSchema;
import com.sinosoft.lis.vschema.LJAGetSet;
import com.sinosoft.utility.*;

import java.sql.*;

/**
 * <p>ClassName: LJAGetDB </p>
 * <p>Description: DB层数据库操作类文件 </p>
 * <p>Copyright: Copyright (c) 2018</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2018-09-07
 */
public class LJAGetDB extends LJAGetSchema {
    // @Field
    private Connection con;
    private DBOper db;
    /**
     * flag = true: 传入Connection
     * flag = false: 不传入Connection
     **/
    private boolean mflag = false;

    public CErrors mErrors = new CErrors();        // 错误信息

    /**
     * 为批量操作而准备的语句和游标对象
     */
    private ResultSet mResultSet = null;
    private Statement mStatement = null;
    // @Constructor
    public LJAGetDB( Connection tConnection ) {
        con = tConnection;
        db = new DBOper( con, "LJAGet" );
        mflag = true;
    }

    public LJAGetDB() {
        con = null;
        db = new DBOper( "LJAGet" );
        mflag = false;
    }

    // @Method
    public boolean deleteSQL() {
        LJAGetSchema tSchema = this.getSchema();
        if (db.deleteSQL(tSchema)) {
            return true;
        } else {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LJAGetDB";
            tError.functionName = "deleteSQL";
            tError.errorMessage = "操作失败!";
            this.mErrors .addOneError(tError);
            return false;
        }
    }

    public int getCount() {
        LJAGetSchema tSchema = this.getSchema();

        int tCount = db.getCount(tSchema);
        if (tCount < 0) {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LJAGetDB";
            tError.functionName = "getCount";
            tError.errorMessage = "操作失败!";
            this.mErrors .addOneError(tError);

            return -1;
        }

        return tCount;
    }

    public boolean delete() {
        PreparedStatement pstmt = null;

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try {
            pstmt = con.prepareStatement("DELETE FROM LJAGet WHERE  1=1  AND ActuGetNo = ?");
            if(this.getActuGetNo() == null || this.getActuGetNo().equals("null")) {
                pstmt.setNull(1, 12);
            } else {
                pstmt.setString(1, this.getActuGetNo());
            }
            pstmt.executeUpdate();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LJAGetDB";
            tError.functionName = "delete()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);
            ex.printStackTrace();

            try {
                pstmt.close();
            } catch (Exception e){}

            if(!mflag) {
                try {
                    con.close();
                } catch (Exception e){}
            }

            return false;
        }

        if(!mflag) {
            try {
                con.close();
            } catch (Exception e){}
        }

        return true;
    }

    public boolean update() {
        PreparedStatement pstmt = null;

        SQLString sqlObj = new SQLString("LJAGet");
        sqlObj.setSQL(2, this);
        String sql = sqlObj.getSQL();

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try {
            pstmt = con.prepareStatement("UPDATE LJAGet SET  ActuGetNo = ? , OtherNo = ? , OtherNoType = ? , PayMode = ? , BankOnTheWayFlag = ? , BankSuccFlag = ? , SendBankCount = ? , ManageCom = ? , AgentCom = ? , AgentType = ? , AgentCode = ? , AgentGroup = ? , AccName = ? , StartGetDate = ? , AppntNo = ? , SumGetMoney = ? , SaleChnl = ? , ShouldDate = ? , EnterAccDate = ? , ConfDate = ? , ApproveCode = ? , ApproveDate = ? , GetNoticeNo = ? , BankCode = ? , BankAccNo = ? , Drawer = ? , DrawerID = ? , SerialNo = ? , Operator = ? , MakeDate = ? , MakeTime = ? , ModifyDate = ? , ModifyTime = ? , ActualDrawer = ? , ActualDrawerID = ? , PolicyCom = ? , InBankCode = ? , InBankAccNo = ? , BalanceOnTime = ? WHERE  1=1  AND ActuGetNo = ?");
            if(this.getActuGetNo() == null || this.getActuGetNo().equals("null")) {
                pstmt.setNull(1, 12);
            } else {
                pstmt.setString(1, this.getActuGetNo());
            }
            if(this.getOtherNo() == null || this.getOtherNo().equals("null")) {
                pstmt.setNull(2, 12);
            } else {
                pstmt.setString(2, this.getOtherNo());
            }
            if(this.getOtherNoType() == null || this.getOtherNoType().equals("null")) {
                pstmt.setNull(3, 12);
            } else {
                pstmt.setString(3, this.getOtherNoType());
            }
            if(this.getPayMode() == null || this.getPayMode().equals("null")) {
                pstmt.setNull(4, 12);
            } else {
                pstmt.setString(4, this.getPayMode());
            }
            if(this.getBankOnTheWayFlag() == null || this.getBankOnTheWayFlag().equals("null")) {
                pstmt.setNull(5, 12);
            } else {
                pstmt.setString(5, this.getBankOnTheWayFlag());
            }
            if(this.getBankSuccFlag() == null || this.getBankSuccFlag().equals("null")) {
                pstmt.setNull(6, 12);
            } else {
                pstmt.setString(6, this.getBankSuccFlag());
            }
            pstmt.setInt(7, this.getSendBankCount());
            if(this.getManageCom() == null || this.getManageCom().equals("null")) {
                pstmt.setNull(8, 12);
            } else {
                pstmt.setString(8, this.getManageCom());
            }
            if(this.getAgentCom() == null || this.getAgentCom().equals("null")) {
                pstmt.setNull(9, 12);
            } else {
                pstmt.setString(9, this.getAgentCom());
            }
            if(this.getAgentType() == null || this.getAgentType().equals("null")) {
                pstmt.setNull(10, 12);
            } else {
                pstmt.setString(10, this.getAgentType());
            }
            if(this.getAgentCode() == null || this.getAgentCode().equals("null")) {
                pstmt.setNull(11, 12);
            } else {
                pstmt.setString(11, this.getAgentCode());
            }
            if(this.getAgentGroup() == null || this.getAgentGroup().equals("null")) {
                pstmt.setNull(12, 12);
            } else {
                pstmt.setString(12, this.getAgentGroup());
            }
            if(this.getAccName() == null || this.getAccName().equals("null")) {
                pstmt.setNull(13, 12);
            } else {
                pstmt.setString(13, this.getAccName());
            }
            if(this.getStartGetDate() == null || this.getStartGetDate().equals("null")) {
                pstmt.setNull(14, 93);
            } else {
                pstmt.setDate(14, Date.valueOf(this.getStartGetDate()));
            }
            if(this.getAppntNo() == null || this.getAppntNo().equals("null")) {
                pstmt.setNull(15, 12);
            } else {
                pstmt.setString(15, this.getAppntNo());
            }
            pstmt.setDouble(16, this.getSumGetMoney());
            if(this.getSaleChnl() == null || this.getSaleChnl().equals("null")) {
                pstmt.setNull(17, 12);
            } else {
                pstmt.setString(17, this.getSaleChnl());
            }
            if(this.getShouldDate() == null || this.getShouldDate().equals("null")) {
                pstmt.setNull(18, 93);
            } else {
                pstmt.setDate(18, Date.valueOf(this.getShouldDate()));
            }
            if(this.getEnterAccDate() == null || this.getEnterAccDate().equals("null")) {
                pstmt.setNull(19, 93);
            } else {
                pstmt.setDate(19, Date.valueOf(this.getEnterAccDate()));
            }
            if(this.getConfDate() == null || this.getConfDate().equals("null")) {
                pstmt.setNull(20, 93);
            } else {
                pstmt.setDate(20, Date.valueOf(this.getConfDate()));
            }
            if(this.getApproveCode() == null || this.getApproveCode().equals("null")) {
                pstmt.setNull(21, 12);
            } else {
                pstmt.setString(21, this.getApproveCode());
            }
            if(this.getApproveDate() == null || this.getApproveDate().equals("null")) {
                pstmt.setNull(22, 93);
            } else {
                pstmt.setDate(22, Date.valueOf(this.getApproveDate()));
            }
            if(this.getGetNoticeNo() == null || this.getGetNoticeNo().equals("null")) {
                pstmt.setNull(23, 12);
            } else {
                pstmt.setString(23, this.getGetNoticeNo());
            }
            if(this.getBankCode() == null || this.getBankCode().equals("null")) {
                pstmt.setNull(24, 12);
            } else {
                pstmt.setString(24, this.getBankCode());
            }
            if(this.getBankAccNo() == null || this.getBankAccNo().equals("null")) {
                pstmt.setNull(25, 12);
            } else {
                pstmt.setString(25, this.getBankAccNo());
            }
            if(this.getDrawer() == null || this.getDrawer().equals("null")) {
                pstmt.setNull(26, 12);
            } else {
                pstmt.setString(26, this.getDrawer());
            }
            if(this.getDrawerID() == null || this.getDrawerID().equals("null")) {
                pstmt.setNull(27, 12);
            } else {
                pstmt.setString(27, this.getDrawerID());
            }
            if(this.getSerialNo() == null || this.getSerialNo().equals("null")) {
                pstmt.setNull(28, 12);
            } else {
                pstmt.setString(28, this.getSerialNo());
            }
            if(this.getOperator() == null || this.getOperator().equals("null")) {
                pstmt.setNull(29, 12);
            } else {
                pstmt.setString(29, this.getOperator());
            }
            if(this.getMakeDate() == null || this.getMakeDate().equals("null")) {
                pstmt.setNull(30, 93);
            } else {
                pstmt.setDate(30, Date.valueOf(this.getMakeDate()));
            }
            if(this.getMakeTime() == null || this.getMakeTime().equals("null")) {
                pstmt.setNull(31, 12);
            } else {
                pstmt.setString(31, this.getMakeTime());
            }
            if(this.getModifyDate() == null || this.getModifyDate().equals("null")) {
                pstmt.setNull(32, 93);
            } else {
                pstmt.setDate(32, Date.valueOf(this.getModifyDate()));
            }
            if(this.getModifyTime() == null || this.getModifyTime().equals("null")) {
                pstmt.setNull(33, 12);
            } else {
                pstmt.setString(33, this.getModifyTime());
            }
            if(this.getActualDrawer() == null || this.getActualDrawer().equals("null")) {
                pstmt.setNull(34, 12);
            } else {
                pstmt.setString(34, this.getActualDrawer());
            }
            if(this.getActualDrawerID() == null || this.getActualDrawerID().equals("null")) {
                pstmt.setNull(35, 12);
            } else {
                pstmt.setString(35, this.getActualDrawerID());
            }
            if(this.getPolicyCom() == null || this.getPolicyCom().equals("null")) {
                pstmt.setNull(36, 12);
            } else {
                pstmt.setString(36, this.getPolicyCom());
            }
            if(this.getInBankCode() == null || this.getInBankCode().equals("null")) {
                pstmt.setNull(37, 12);
            } else {
                pstmt.setString(37, this.getInBankCode());
            }
            if(this.getInBankAccNo() == null || this.getInBankAccNo().equals("null")) {
                pstmt.setNull(38, 12);
            } else {
                pstmt.setString(38, this.getInBankAccNo());
            }
            if(this.getBalanceOnTime() == null || this.getBalanceOnTime().equals("null")) {
                pstmt.setNull(39, 12);
            } else {
                pstmt.setString(39, this.getBalanceOnTime());
            }
            // set where condition
            if(this.getActuGetNo() == null || this.getActuGetNo().equals("null")) {
                pstmt.setNull(40, 12);
            } else {
                pstmt.setString(40, this.getActuGetNo());
            }
            pstmt.executeUpdate();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LJAGetDB";
            tError.functionName = "update()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);
            ex.printStackTrace();

            try {
                pstmt.close();
            } catch (Exception e){}

            if( !mflag ) {
                try {
                    con.close();
                } catch (Exception e){}
            }

            return false;
        }

        if(!mflag) {
            try {
                con.close();
            } catch (Exception e){}
        }

        return true;
    }

    public boolean insert() {
        PreparedStatement pstmt = null;

        SQLString sqlObj = new SQLString("LJAGet");
        sqlObj.setSQL(1, this);
        String sql = sqlObj.getSQL();

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try {
            pstmt = con.prepareStatement("INSERT INTO LJAGet VALUES( ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ?)");
            if(this.getActuGetNo() == null || this.getActuGetNo().equals("null")) {
                pstmt.setNull(1, 12);
            } else {
                pstmt.setString(1, this.getActuGetNo());
            }
            if(this.getOtherNo() == null || this.getOtherNo().equals("null")) {
                pstmt.setNull(2, 12);
            } else {
                pstmt.setString(2, this.getOtherNo());
            }
            if(this.getOtherNoType() == null || this.getOtherNoType().equals("null")) {
                pstmt.setNull(3, 12);
            } else {
                pstmt.setString(3, this.getOtherNoType());
            }
            if(this.getPayMode() == null || this.getPayMode().equals("null")) {
                pstmt.setNull(4, 12);
            } else {
                pstmt.setString(4, this.getPayMode());
            }
            if(this.getBankOnTheWayFlag() == null || this.getBankOnTheWayFlag().equals("null")) {
                pstmt.setNull(5, 12);
            } else {
                pstmt.setString(5, this.getBankOnTheWayFlag());
            }
            if(this.getBankSuccFlag() == null || this.getBankSuccFlag().equals("null")) {
                pstmt.setNull(6, 12);
            } else {
                pstmt.setString(6, this.getBankSuccFlag());
            }
            pstmt.setInt(7, this.getSendBankCount());
            if(this.getManageCom() == null || this.getManageCom().equals("null")) {
                pstmt.setNull(8, 12);
            } else {
                pstmt.setString(8, this.getManageCom());
            }
            if(this.getAgentCom() == null || this.getAgentCom().equals("null")) {
                pstmt.setNull(9, 12);
            } else {
                pstmt.setString(9, this.getAgentCom());
            }
            if(this.getAgentType() == null || this.getAgentType().equals("null")) {
                pstmt.setNull(10, 12);
            } else {
                pstmt.setString(10, this.getAgentType());
            }
            if(this.getAgentCode() == null || this.getAgentCode().equals("null")) {
                pstmt.setNull(11, 12);
            } else {
                pstmt.setString(11, this.getAgentCode());
            }
            if(this.getAgentGroup() == null || this.getAgentGroup().equals("null")) {
                pstmt.setNull(12, 12);
            } else {
                pstmt.setString(12, this.getAgentGroup());
            }
            if(this.getAccName() == null || this.getAccName().equals("null")) {
                pstmt.setNull(13, 12);
            } else {
                pstmt.setString(13, this.getAccName());
            }
            if(this.getStartGetDate() == null || this.getStartGetDate().equals("null")) {
                pstmt.setNull(14, 93);
            } else {
                pstmt.setDate(14, Date.valueOf(this.getStartGetDate()));
            }
            if(this.getAppntNo() == null || this.getAppntNo().equals("null")) {
                pstmt.setNull(15, 12);
            } else {
                pstmt.setString(15, this.getAppntNo());
            }
            pstmt.setDouble(16, this.getSumGetMoney());
            if(this.getSaleChnl() == null || this.getSaleChnl().equals("null")) {
                pstmt.setNull(17, 12);
            } else {
                pstmt.setString(17, this.getSaleChnl());
            }
            if(this.getShouldDate() == null || this.getShouldDate().equals("null")) {
                pstmt.setNull(18, 93);
            } else {
                pstmt.setDate(18, Date.valueOf(this.getShouldDate()));
            }
            if(this.getEnterAccDate() == null || this.getEnterAccDate().equals("null")) {
                pstmt.setNull(19, 93);
            } else {
                pstmt.setDate(19, Date.valueOf(this.getEnterAccDate()));
            }
            if(this.getConfDate() == null || this.getConfDate().equals("null")) {
                pstmt.setNull(20, 93);
            } else {
                pstmt.setDate(20, Date.valueOf(this.getConfDate()));
            }
            if(this.getApproveCode() == null || this.getApproveCode().equals("null")) {
                pstmt.setNull(21, 12);
            } else {
                pstmt.setString(21, this.getApproveCode());
            }
            if(this.getApproveDate() == null || this.getApproveDate().equals("null")) {
                pstmt.setNull(22, 93);
            } else {
                pstmt.setDate(22, Date.valueOf(this.getApproveDate()));
            }
            if(this.getGetNoticeNo() == null || this.getGetNoticeNo().equals("null")) {
                pstmt.setNull(23, 12);
            } else {
                pstmt.setString(23, this.getGetNoticeNo());
            }
            if(this.getBankCode() == null || this.getBankCode().equals("null")) {
                pstmt.setNull(24, 12);
            } else {
                pstmt.setString(24, this.getBankCode());
            }
            if(this.getBankAccNo() == null || this.getBankAccNo().equals("null")) {
                pstmt.setNull(25, 12);
            } else {
                pstmt.setString(25, this.getBankAccNo());
            }
            if(this.getDrawer() == null || this.getDrawer().equals("null")) {
                pstmt.setNull(26, 12);
            } else {
                pstmt.setString(26, this.getDrawer());
            }
            if(this.getDrawerID() == null || this.getDrawerID().equals("null")) {
                pstmt.setNull(27, 12);
            } else {
                pstmt.setString(27, this.getDrawerID());
            }
            if(this.getSerialNo() == null || this.getSerialNo().equals("null")) {
                pstmt.setNull(28, 12);
            } else {
                pstmt.setString(28, this.getSerialNo());
            }
            if(this.getOperator() == null || this.getOperator().equals("null")) {
                pstmt.setNull(29, 12);
            } else {
                pstmt.setString(29, this.getOperator());
            }
            if(this.getMakeDate() == null || this.getMakeDate().equals("null")) {
                pstmt.setNull(30, 93);
            } else {
                pstmt.setDate(30, Date.valueOf(this.getMakeDate()));
            }
            if(this.getMakeTime() == null || this.getMakeTime().equals("null")) {
                pstmt.setNull(31, 12);
            } else {
                pstmt.setString(31, this.getMakeTime());
            }
            if(this.getModifyDate() == null || this.getModifyDate().equals("null")) {
                pstmt.setNull(32, 93);
            } else {
                pstmt.setDate(32, Date.valueOf(this.getModifyDate()));
            }
            if(this.getModifyTime() == null || this.getModifyTime().equals("null")) {
                pstmt.setNull(33, 12);
            } else {
                pstmt.setString(33, this.getModifyTime());
            }
            if(this.getActualDrawer() == null || this.getActualDrawer().equals("null")) {
                pstmt.setNull(34, 12);
            } else {
                pstmt.setString(34, this.getActualDrawer());
            }
            if(this.getActualDrawerID() == null || this.getActualDrawerID().equals("null")) {
                pstmt.setNull(35, 12);
            } else {
                pstmt.setString(35, this.getActualDrawerID());
            }
            if(this.getPolicyCom() == null || this.getPolicyCom().equals("null")) {
                pstmt.setNull(36, 12);
            } else {
                pstmt.setString(36, this.getPolicyCom());
            }
            if(this.getInBankCode() == null || this.getInBankCode().equals("null")) {
                pstmt.setNull(37, 12);
            } else {
                pstmt.setString(37, this.getInBankCode());
            }
            if(this.getInBankAccNo() == null || this.getInBankAccNo().equals("null")) {
                pstmt.setNull(38, 12);
            } else {
                pstmt.setString(38, this.getInBankAccNo());
            }
            if(this.getBalanceOnTime() == null || this.getBalanceOnTime().equals("null")) {
                pstmt.setNull(39, 12);
            } else {
                pstmt.setString(39, this.getBalanceOnTime());
            }
            // execute sql
            pstmt.executeUpdate();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LJAGetDB";
            tError.functionName = "insert()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors.addOneError(tError);
            ex.printStackTrace();

            try {
                pstmt.close();
            } catch (Exception e){}

            if( !mflag ) {
                try {
                    con.close();
                } catch (Exception e){}
            }

            return false;
        }

        if(!mflag) {
            try {
                con.close();
            } catch (Exception e){}
        }

        return true;
    }

    public boolean getInfo() {
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        if(!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            pstmt = con.prepareStatement("SELECT * FROM LJAGet WHERE  1=1  AND ActuGetNo = ?",
                    ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
            if(this.getActuGetNo() == null || this.getActuGetNo().equals("null")) {
                pstmt.setNull(1, 12);
            } else {
                pstmt.setString(1, this.getActuGetNo());
            }
            rs = pstmt.executeQuery();
            int i = 0;
            while (rs.next()) {
                i++;
                if (!this.setSchema(rs,i)) {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "LJAGetDB";
                    tError.functionName = "getInfo";
                    tError.errorMessage = "取数失败!";
                    this.mErrors .addOneError(tError);

                    try{ rs.close(); } catch( Exception ex ) {}
                    try{ pstmt.close(); } catch( Exception ex1 ) {}

                    if (!mflag) {
                        try {
                            con.close();
                        }
                        catch(Exception et){}
                    }
                    return false;
                }
                break;
            }
            try{ rs.close(); } catch( Exception ex2 ) {}
            try{ pstmt.close(); } catch( Exception ex3 ) {}

            if( i == 0 ) {
                if (!mflag) {
                    try {
                        con.close();
                    }
                    catch(Exception et){}
                }
                return false;
            }
        }
        catch(Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LJAGetDB";
            tError.functionName = "getInfo";
            tError.errorMessage = ExceptionUtils.exceptionToString(e);
            this.mErrors .addOneError(tError);

            try{ rs.close(); } catch( Exception ex ) {}
            try{ pstmt.close(); } catch( Exception ex1 ) {}

            if (!mflag) {
                try {
                    con.close();
                }
                catch(Exception et){}
            }
            return false;
        }
        // 断开数据库连接
        if (!mflag) {
            try {
                con.close();
            }
            catch(Exception e){}
        }

        return true;
    }

    public LJAGetSet query() {
        Statement stmt = null;
        ResultSet rs = null;
        LJAGetSet aLJAGetSet = new LJAGetSet();

        if (!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            stmt = con.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
            SQLString sqlObj = new SQLString("LJAGet");
            LJAGetSchema aSchema = this.getSchema();
            sqlObj.setSQL(5, aSchema);
            String sql = sqlObj.getSQL();

            rs = stmt.executeQuery(sql);
            int i = 0;
            while (rs.next()) {
                i++;
                if (i>10000) {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "LJAGetDB";
                    tError.functionName = "query";
                    tError.errorMessage = "SQL语句返回结果集太多，请换用其他方式!";
                    this.mErrors .addOneError(tError);
                    break;
                }
                LJAGetSchema s1 = new LJAGetSchema();
                s1.setSchema(rs,i);
                aLJAGetSet.add(s1);
            }
            try{ rs.close(); } catch( Exception ex ) {}
            try{ stmt.close(); } catch( Exception ex1 ) {}
        }
        catch(Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LJAGetDB";
            tError.functionName = "query";
            tError.errorMessage = ExceptionUtils.exceptionToString(e);
            this.mErrors.addOneError(tError);
            e.printStackTrace();

            try { rs.close(); } catch (Exception ex2) { ex2.printStackTrace(); }
            try { stmt.close(); } catch(Exception ex3) { ex3.printStackTrace(); }

            if (!mflag) {
                try {
                    con.close();
                }
                catch(Exception et){et.printStackTrace();}
            }
        }

        if (!mflag) {
            try {
                con.close();
            }
            catch(Exception e){e.printStackTrace();}
        }

        return aLJAGetSet;
    }

    public LJAGetSet executeQuery(String sql)  {
        Statement stmt = null;
        ResultSet rs = null;
        LJAGetSet aLJAGetSet = new LJAGetSet();

        if (!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            stmt = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);

            rs = stmt.executeQuery(StrTool.GBKToUnicode(sql));
            int i = 0;
            while (rs.next()) {
                i++;
                if (i>10000) {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "LJAGetDB";
                    tError.functionName = "executeQuery";
                    tError.errorMessage = "SQL语句返回结果集太多，请换用其他方式!";
                    this.mErrors .addOneError(tError);
                    break;
                }
                LJAGetSchema s1 = new LJAGetSchema();
                if (!s1.setSchema(rs,i)) {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "LJAGetDB";
                    tError.functionName = "executeQuery";
                    tError.errorMessage = "sql语句有误，请查看表名及字段名信息!";
                    this.mErrors .addOneError(tError);
                }
                aLJAGetSet.add(s1);
            }
            try{ rs.close(); } catch( Exception ex ) {}
            try{ stmt.close(); } catch( Exception ex1 ) {}
        }
        catch(Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LJAGetDB";
            tError.functionName = "executeQuery";
            tError.errorMessage = ExceptionUtils.exceptionToString(e);
            this.mErrors .addOneError(tError);

            try{ rs.close(); } catch( Exception ex2 ) {}
            try{ stmt.close(); } catch( Exception ex3 ) {}

            if (!mflag) {
                try {
                    con.close();
                }
                catch(Exception et){}
            }
        }

        if (!mflag) {
            try {
                con.close();
            }
            catch(Exception e){}
        }

        return aLJAGetSet;
    }

    public LJAGetSet query(int nStart, int nCount) {
        Statement stmt = null;
        ResultSet rs = null;
        LJAGetSet aLJAGetSet = new LJAGetSet();

        if (!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            stmt = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);
            SQLString sqlObj = new SQLString("LJAGet");
            LJAGetSchema aSchema = this.getSchema();
            sqlObj.setSQL(5,aSchema);
            String sql = sqlObj.getSQL();

            rs = stmt.executeQuery(sql);
            int i = 0;
            while (rs.next()) {
                i++;

                if( i < nStart ) {
                    continue;
                }

                if( i >= nStart + nCount ) {
                    break;
                }

                LJAGetSchema s1 = new LJAGetSchema();
                s1.setSchema(rs,i);
                aLJAGetSet.add(s1);
            }
            try{ rs.close(); } catch( Exception ex ) {}
            try{ stmt.close(); } catch( Exception ex1 ) {}
        }
        catch(Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LJAGetDB";
            tError.functionName = "query";
            tError.errorMessage = ExceptionUtils.exceptionToString(e);
            this.mErrors .addOneError(tError);

            try{ rs.close(); } catch( Exception ex2 ) {}
            try{ stmt.close(); } catch( Exception ex3 ) {}

            if (!mflag) {
                try {
                    con.close();
                }
                catch(Exception et){}
            }
        }

        if (!mflag) {
            try {
                con.close();
            }
            catch(Exception e){}
        }

        return aLJAGetSet;
    }

    public LJAGetSet executeQuery(String sql, int nStart, int nCount) {
        Statement stmt = null;
        ResultSet rs = null;
        LJAGetSet aLJAGetSet = new LJAGetSet();

        if (!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            stmt = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);

            rs = stmt.executeQuery(StrTool.GBKToUnicode(sql));
            int i = 0;
            while (rs.next()) {
                i++;

                if( i < nStart ) {
                    continue;
                }

                if( i >= nStart + nCount ) {
                    break;
                }

                LJAGetSchema s1 = new LJAGetSchema();
                if (!s1.setSchema(rs,i)) {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "LJAGetDB";
                    tError.functionName = "executeQuery";
                    tError.errorMessage = "sql语句有误，请查看表名及字段名信息!";
                    this.mErrors .addOneError(tError);
                }
                aLJAGetSet.add(s1);
            }
            try{ rs.close(); } catch( Exception ex ) {}
            try{ stmt.close(); } catch( Exception ex1 ) {}
        }
        catch(Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LJAGetDB";
            tError.functionName = "executeQuery";
            tError.errorMessage = ExceptionUtils.exceptionToString(e);
            this.mErrors .addOneError(tError);

            try{ rs.close(); } catch( Exception ex2 ) {}
            try{ stmt.close(); } catch( Exception ex3 ) {}

            if (!mflag) {
                try {
                    con.close();
                }
                catch(Exception et){}
            }
        }

        if (!mflag) {
            try {
                con.close();
            }
            catch(Exception e){}
        }

        return aLJAGetSet;
    }

    public boolean update(String strWherePart) {
        Statement stmt = null;

        if (!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            stmt = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);
            SQLString sqlObj = new SQLString("LJAGet");
            LJAGetSchema aSchema = this.getSchema();
            sqlObj.setSQL(2,aSchema);
            String sql = "update LJAGet " + sqlObj.getUpdPart() + " where " + strWherePart;

            int operCount = stmt.executeUpdate(sql);
            if (operCount == 0) {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "LJAGetDB";
                tError.functionName = "update";
                tError.errorMessage = "更新数据失败!";
                this.mErrors .addOneError(tError);

                if (!mflag) {
                    try {
                        con.close();
                    }
                    catch(Exception et){}
                }
                return false;
            }
        }
        catch(Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LJAGetDB";
            tError.functionName = "update";
            tError.errorMessage = ExceptionUtils.exceptionToString(e);
            this.mErrors .addOneError(tError);

            try{ stmt.close(); } catch( Exception ex1 ) {}

            if (!mflag) {
                try {
                    con.close();
                }
                catch(Exception et){}
            }
            return false;
        }
        // 断开数据库连接
        if (!mflag) {
            try {
                con.close();
            }
            catch(Exception e){}
        }

        return true;
    }

    /**
     * 准备数据查询条件
     * @param strSQL String
     * @return boolean
     */
    public boolean prepareData(String strSQL) {
        if (mResultSet != null) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LJAGetDB";
            tError.functionName = "prepareData";
            tError.errorMessage = "数据集非空，程序在准备数据集之后，没有关闭！";
            this.mErrors.addOneError(tError);
            return false;
        }

        if (!mflag) {
            con = DBConnPool.getConnection();
        }
        try {
            mStatement = con.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
            mResultSet = mStatement.executeQuery(StrTool.GBKToUnicode(strSQL));
        }
        catch (Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LJAGetDB";
            tError.functionName = "prepareData";
            tError.errorMessage = ExceptionUtils.exceptionToString(e);
            this.mErrors.addOneError(tError);
            try {
                mResultSet.close();
            }
            catch (Exception ex2) {}
            try {
                mStatement.close();
            }
            catch (Exception ex3) {}
            if (!mflag) {
                try {
                    con.close();
                }
                catch (Exception et) {}
            }
            return false;
        }

        if (!mflag) {
            try {
                con.close();
            }
            catch (Exception e) {}
        }
        return true;
    }

    /**
     * 获取数据集
     * @return boolean
     */
    public boolean hasMoreData() {
        boolean flag = true;
        if (null == mResultSet) {
            CError tError = new CError();
            tError.moduleName = "LJAGetDB";
            tError.functionName = "hasMoreData";
            tError.errorMessage = "数据集为空，请先准备数据集！";
            this.mErrors.addOneError(tError);
            return false;
        }
        try {
            flag = mResultSet.next();
        }
        catch (Exception ex) {
            CError tError = new CError();
            tError.moduleName = "LJAGetDB";
            tError.functionName = "hasMoreData";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors.addOneError(tError);
            try {
                mResultSet.close();
                mResultSet = null;
            }
            catch (Exception ex2) {}
            try {
                mStatement.close();
                mStatement = null;
            }
            catch (Exception ex3) {}
            if (!mflag) {
                try {
                    con.close();
                }
                catch (Exception et) {}
            }
            return false;
        }
        return flag;
    }
    /**
     * 获取定量数据
     * @return LJAGetSet
     */
    public LJAGetSet getData() {
        int tCount = 0;
        LJAGetSet tLJAGetSet = new LJAGetSet();
        LJAGetSchema tLJAGetSchema = null;
        if (null == mResultSet) {
            CError tError = new CError();
            tError.moduleName = "LJAGetDB";
            tError.functionName = "getData";
            tError.errorMessage = "数据集为空，请先准备数据集！";
            this.mErrors.addOneError(tError);
            return null;
        }
        try {
            tCount = 1;
            tLJAGetSchema = new LJAGetSchema();
            tLJAGetSchema.setSchema(mResultSet, 1);
            tLJAGetSet.add(tLJAGetSchema);
            //注意mResultSet.next()的作用
            while (tCount++ < SysConst.FETCHCOUNT) {
                if (mResultSet.next()) {
                    tLJAGetSchema = new LJAGetSchema();
                    tLJAGetSchema.setSchema(mResultSet, 1);
                    tLJAGetSet.add(tLJAGetSchema);
                }
            }
        }
        catch (Exception ex) {
            CError tError = new CError();
            tError.moduleName = "LJAGetDB";
            tError.functionName = "getData";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors.addOneError(tError);
            try {
                mResultSet.close();
                mResultSet = null;
            }
            catch (Exception ex2) {}
            try {
                mStatement.close();
                mStatement = null;
            }
            catch (Exception ex3) {}
            if (!mflag) {
                try {
                    con.close();
                }
                catch (Exception et) {}
            }
            return null;
        }
        return tLJAGetSet;
    }
    /**
     * 关闭数据集
     * @return boolean
     */
    public boolean closeData() {
        boolean flag = true;
        try {
            if (null == mResultSet) {
                CError tError = new CError();
                tError.moduleName = "LJAGetDB";
                tError.functionName = "closeData";
                tError.errorMessage = "数据集已经关闭了！";
                this.mErrors.addOneError(tError);
                flag = false;
            } else {
                mResultSet.close();
                mResultSet = null;
            }
        }
        catch (Exception ex2) {
            CError tError = new CError();
            tError.moduleName = "LJAGetDB";
            tError.functionName = "closeData";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex2);
            this.mErrors.addOneError(tError);
            flag = false;
        }
        try {
            if (null == mStatement) {
                CError tError = new CError();
                tError.moduleName = "LJAGetDB";
                tError.functionName = "closeData";
                tError.errorMessage = "语句已经关闭了！";
                this.mErrors.addOneError(tError);
                flag = false;
            } else {
                mStatement.close();
                mStatement = null;
            }
        }
        catch (Exception ex3) {
            CError tError = new CError();
            tError.moduleName = "LJAGetDB";
            tError.functionName = "closeData";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex3);
            this.mErrors.addOneError(tError);
            flag = false;
        }
        return flag;
    }
}
