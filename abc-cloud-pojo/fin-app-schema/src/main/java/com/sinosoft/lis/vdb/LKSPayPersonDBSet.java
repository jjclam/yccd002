/**
 * Copyright (c) 2017 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.vdb;

import com.sinosoft.lis.vschema.LKSPayPersonSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.DBConnPool;
import com.sinosoft.utility.DBOper;
import com.sinosoft.utility.ExceptionUtils;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;

/**
 * <p>ClassName: LKSPayPersonDBSet </p>
 * <p>Description: DB层多记录数据库操作类文件 </p>
 * <p>Copyright: Copyright (c) 2017</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2017-10-23
 */
public class LKSPayPersonDBSet extends LKSPayPersonSet {
    // @Field
    private Connection con;
    private DBOper db;
    /**
    * flag = true: 传入Connection
    * flag = false: 不传入Connection
    **/
    private boolean mflag = false;

    // @Constructor
    public LKSPayPersonDBSet(Connection tConnection) {
        con = tConnection;
        db = new DBOper(con,"LKSPayPerson");
        mflag = true;
    }

    public LKSPayPersonDBSet() {
        db = new DBOper( "LKSPayPerson" );
    }
    // @Method
    public boolean deleteSQL() {
        if (db.deleteSQL(this)) {
            return true;
        } else {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LKSPayPersonDBSet";
            tError.functionName = "deleteSQL";
            tError.errorMessage = "操作失败!";
            this.mErrors .addOneError(tError);
            return false;
        }
    }

    /**
     * 删除操作
     * 删除条件：主键
     * @return boolean
     */
    public boolean delete() {
        PreparedStatement pstmt = null;

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try {
            int tCount = this.size();
            pstmt = con.prepareStatement("DELETE FROM LKSPayPerson WHERE  1=1  AND SPayPersonID = ?");
            for (int i = 1; i <= tCount; i++) {
            pstmt.setLong(1, this.get(i).getSPayPersonID());
                pstmt.addBatch();
            }
            pstmt.executeBatch();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            ex.printStackTrace();
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LKSPayPersonDBSet";
            tError.functionName = "delete()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            try {
                pstmt.close();
            } catch (Exception e){
                e.printStackTrace();
            }

            if (!mflag) {
                try {
                    con.close();
                } catch (Exception e){e.printStackTrace();}
            }

            return false;
        }

        if( !mflag ) {
            try {
                con.close();
            } catch (Exception e){e.printStackTrace();}
        }

        return true;
    }

    /**
     * 更新操作
     * 更新条件：主键
     * @return boolean
     */
    public boolean update() {
        PreparedStatement pstmt = null;

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try {
            int tCount = this.size();
            pstmt = con.prepareStatement("UPDATE LKSPayPerson SET  SPayPersonID = ? , ShardingID = ? , PolNo = ? , PayCount = ? , GrpContNo = ? , GrpPolNo = ? , ContNo = ? , ManageCom = ? , AgentCom = ? , AgentType = ? , RiskCode = ? , AgentCode = ? , AgentGroup = ? , PayTypeFlag = ? , AppntNo = ? , GetNoticeNo = ? , PayAimClass = ? , DutyCode = ? , PayPlanCode = ? , SumDuePayMoney = ? , SumActuPayMoney = ? , PayIntv = ? , PayDate = ? , PayType = ? , LastPayToDate = ? , CurPayToDate = ? , InInsuAccState = ? , BankCode = ? , BankAccNo = ? , BankOnTheWayFlag = ? , BankSuccFlag = ? , ApproveCode = ? , ApproveDate = ? , ApproveTime = ? , SerialNo = ? , InputFlag = ? , Operator = ? , MakeDate = ? , MakeTime = ? , ModifyDate = ? , ModifyTime = ? , EdorNo = ? , MainPolYear = ? , OtherNo = ? , OtherNoType = ? , PrtNo = ? , SaleChnl = ? , ChargeCom = ? , APPntName = ? , KindCode = ? , PaytoDate = ? , PayEndDate = ? , SignDate = ? , CValiDate = ? , PayEndYear = ? , PayMoney = ? WHERE  1=1  AND SPayPersonID = ?");
            for (int i = 1; i <= tCount; i++) {
            pstmt.setLong(1, this.get(i).getSPayPersonID());
            if(this.get(i).getShardingID() == null || this.get(i).getShardingID().equals("null")) {
            	pstmt.setString(2,null);
            } else {
            	pstmt.setString(2, this.get(i).getShardingID());
            }
            if(this.get(i).getPolNo() == null || this.get(i).getPolNo().equals("null")) {
            	pstmt.setString(3,null);
            } else {
            	pstmt.setString(3, this.get(i).getPolNo());
            }
            pstmt.setInt(4, this.get(i).getPayCount());
            if(this.get(i).getGrpContNo() == null || this.get(i).getGrpContNo().equals("null")) {
            	pstmt.setString(5,null);
            } else {
            	pstmt.setString(5, this.get(i).getGrpContNo());
            }
            if(this.get(i).getGrpPolNo() == null || this.get(i).getGrpPolNo().equals("null")) {
            	pstmt.setString(6,null);
            } else {
            	pstmt.setString(6, this.get(i).getGrpPolNo());
            }
            if(this.get(i).getContNo() == null || this.get(i).getContNo().equals("null")) {
            	pstmt.setString(7,null);
            } else {
            	pstmt.setString(7, this.get(i).getContNo());
            }
            if(this.get(i).getManageCom() == null || this.get(i).getManageCom().equals("null")) {
            	pstmt.setString(8,null);
            } else {
            	pstmt.setString(8, this.get(i).getManageCom());
            }
            if(this.get(i).getAgentCom() == null || this.get(i).getAgentCom().equals("null")) {
            	pstmt.setString(9,null);
            } else {
            	pstmt.setString(9, this.get(i).getAgentCom());
            }
            if(this.get(i).getAgentType() == null || this.get(i).getAgentType().equals("null")) {
            	pstmt.setString(10,null);
            } else {
            	pstmt.setString(10, this.get(i).getAgentType());
            }
            if(this.get(i).getRiskCode() == null || this.get(i).getRiskCode().equals("null")) {
            	pstmt.setString(11,null);
            } else {
            	pstmt.setString(11, this.get(i).getRiskCode());
            }
            if(this.get(i).getAgentCode() == null || this.get(i).getAgentCode().equals("null")) {
            	pstmt.setString(12,null);
            } else {
            	pstmt.setString(12, this.get(i).getAgentCode());
            }
            if(this.get(i).getAgentGroup() == null || this.get(i).getAgentGroup().equals("null")) {
            	pstmt.setString(13,null);
            } else {
            	pstmt.setString(13, this.get(i).getAgentGroup());
            }
            if(this.get(i).getPayTypeFlag() == null || this.get(i).getPayTypeFlag().equals("null")) {
            	pstmt.setString(14,null);
            } else {
            	pstmt.setString(14, this.get(i).getPayTypeFlag());
            }
            if(this.get(i).getAppntNo() == null || this.get(i).getAppntNo().equals("null")) {
            	pstmt.setString(15,null);
            } else {
            	pstmt.setString(15, this.get(i).getAppntNo());
            }
            if(this.get(i).getGetNoticeNo() == null || this.get(i).getGetNoticeNo().equals("null")) {
            	pstmt.setString(16,null);
            } else {
            	pstmt.setString(16, this.get(i).getGetNoticeNo());
            }
            if(this.get(i).getPayAimClass() == null || this.get(i).getPayAimClass().equals("null")) {
            	pstmt.setString(17,null);
            } else {
            	pstmt.setString(17, this.get(i).getPayAimClass());
            }
            if(this.get(i).getDutyCode() == null || this.get(i).getDutyCode().equals("null")) {
            	pstmt.setString(18,null);
            } else {
            	pstmt.setString(18, this.get(i).getDutyCode());
            }
            if(this.get(i).getPayPlanCode() == null || this.get(i).getPayPlanCode().equals("null")) {
            	pstmt.setString(19,null);
            } else {
            	pstmt.setString(19, this.get(i).getPayPlanCode());
            }
            pstmt.setDouble(20, this.get(i).getSumDuePayMoney());
            pstmt.setDouble(21, this.get(i).getSumActuPayMoney());
            pstmt.setInt(22, this.get(i).getPayIntv());
            if(this.get(i).getPayDate() == null || this.get(i).getPayDate().equals("null")) {
                pstmt.setDate(23,null);
            } else {
                pstmt.setDate(23, Date.valueOf(this.get(i).getPayDate()));
            }
            if(this.get(i).getPayType() == null || this.get(i).getPayType().equals("null")) {
            	pstmt.setString(24,null);
            } else {
            	pstmt.setString(24, this.get(i).getPayType());
            }
            if(this.get(i).getLastPayToDate() == null || this.get(i).getLastPayToDate().equals("null")) {
                pstmt.setDate(25,null);
            } else {
                pstmt.setDate(25, Date.valueOf(this.get(i).getLastPayToDate()));
            }
            if(this.get(i).getCurPayToDate() == null || this.get(i).getCurPayToDate().equals("null")) {
                pstmt.setDate(26,null);
            } else {
                pstmt.setDate(26, Date.valueOf(this.get(i).getCurPayToDate()));
            }
            if(this.get(i).getInInsuAccState() == null || this.get(i).getInInsuAccState().equals("null")) {
            	pstmt.setString(27,null);
            } else {
            	pstmt.setString(27, this.get(i).getInInsuAccState());
            }
            if(this.get(i).getBankCode() == null || this.get(i).getBankCode().equals("null")) {
            	pstmt.setString(28,null);
            } else {
            	pstmt.setString(28, this.get(i).getBankCode());
            }
            if(this.get(i).getBankAccNo() == null || this.get(i).getBankAccNo().equals("null")) {
            	pstmt.setString(29,null);
            } else {
            	pstmt.setString(29, this.get(i).getBankAccNo());
            }
            if(this.get(i).getBankOnTheWayFlag() == null || this.get(i).getBankOnTheWayFlag().equals("null")) {
            	pstmt.setString(30,null);
            } else {
            	pstmt.setString(30, this.get(i).getBankOnTheWayFlag());
            }
            if(this.get(i).getBankSuccFlag() == null || this.get(i).getBankSuccFlag().equals("null")) {
            	pstmt.setString(31,null);
            } else {
            	pstmt.setString(31, this.get(i).getBankSuccFlag());
            }
            if(this.get(i).getApproveCode() == null || this.get(i).getApproveCode().equals("null")) {
            	pstmt.setString(32,null);
            } else {
            	pstmt.setString(32, this.get(i).getApproveCode());
            }
            if(this.get(i).getApproveDate() == null || this.get(i).getApproveDate().equals("null")) {
                pstmt.setDate(33,null);
            } else {
                pstmt.setDate(33, Date.valueOf(this.get(i).getApproveDate()));
            }
            if(this.get(i).getApproveTime() == null || this.get(i).getApproveTime().equals("null")) {
            	pstmt.setString(34,null);
            } else {
            	pstmt.setString(34, this.get(i).getApproveTime());
            }
            if(this.get(i).getSerialNo() == null || this.get(i).getSerialNo().equals("null")) {
            	pstmt.setString(35,null);
            } else {
            	pstmt.setString(35, this.get(i).getSerialNo());
            }
            if(this.get(i).getInputFlag() == null || this.get(i).getInputFlag().equals("null")) {
            	pstmt.setString(36,null);
            } else {
            	pstmt.setString(36, this.get(i).getInputFlag());
            }
            if(this.get(i).getOperator() == null || this.get(i).getOperator().equals("null")) {
            	pstmt.setString(37,null);
            } else {
            	pstmt.setString(37, this.get(i).getOperator());
            }
            if(this.get(i).getMakeDate() == null || this.get(i).getMakeDate().equals("null")) {
                pstmt.setDate(38,null);
            } else {
                pstmt.setDate(38, Date.valueOf(this.get(i).getMakeDate()));
            }
            if(this.get(i).getMakeTime() == null || this.get(i).getMakeTime().equals("null")) {
            	pstmt.setString(39,null);
            } else {
            	pstmt.setString(39, this.get(i).getMakeTime());
            }
            if(this.get(i).getModifyDate() == null || this.get(i).getModifyDate().equals("null")) {
                pstmt.setDate(40,null);
            } else {
                pstmt.setDate(40, Date.valueOf(this.get(i).getModifyDate()));
            }
            if(this.get(i).getModifyTime() == null || this.get(i).getModifyTime().equals("null")) {
            	pstmt.setString(41,null);
            } else {
            	pstmt.setString(41, this.get(i).getModifyTime());
            }
            if(this.get(i).getEdorNo() == null || this.get(i).getEdorNo().equals("null")) {
            	pstmt.setString(42,null);
            } else {
            	pstmt.setString(42, this.get(i).getEdorNo());
            }
            pstmt.setInt(43, this.get(i).getMainPolYear());
            if(this.get(i).getOtherNo() == null || this.get(i).getOtherNo().equals("null")) {
            	pstmt.setString(44,null);
            } else {
            	pstmt.setString(44, this.get(i).getOtherNo());
            }
            if(this.get(i).getOtherNoType() == null || this.get(i).getOtherNoType().equals("null")) {
            	pstmt.setString(45,null);
            } else {
            	pstmt.setString(45, this.get(i).getOtherNoType());
            }
            if(this.get(i).getPrtNo() == null || this.get(i).getPrtNo().equals("null")) {
            	pstmt.setString(46,null);
            } else {
            	pstmt.setString(46, this.get(i).getPrtNo());
            }
            if(this.get(i).getSaleChnl() == null || this.get(i).getSaleChnl().equals("null")) {
            	pstmt.setString(47,null);
            } else {
            	pstmt.setString(47, this.get(i).getSaleChnl());
            }
            if(this.get(i).getChargeCom() == null || this.get(i).getChargeCom().equals("null")) {
            	pstmt.setString(48,null);
            } else {
            	pstmt.setString(48, this.get(i).getChargeCom());
            }
            if(this.get(i).getAPPntName() == null || this.get(i).getAPPntName().equals("null")) {
            	pstmt.setString(49,null);
            } else {
            	pstmt.setString(49, this.get(i).getAPPntName());
            }
            if(this.get(i).getKindCode() == null || this.get(i).getKindCode().equals("null")) {
            	pstmt.setString(50,null);
            } else {
            	pstmt.setString(50, this.get(i).getKindCode());
            }
            if(this.get(i).getPaytoDate() == null || this.get(i).getPaytoDate().equals("null")) {
                pstmt.setDate(51,null);
            } else {
                pstmt.setDate(51, Date.valueOf(this.get(i).getPaytoDate()));
            }
            if(this.get(i).getPayEndDate() == null || this.get(i).getPayEndDate().equals("null")) {
                pstmt.setDate(52,null);
            } else {
                pstmt.setDate(52, Date.valueOf(this.get(i).getPayEndDate()));
            }
            if(this.get(i).getSignDate() == null || this.get(i).getSignDate().equals("null")) {
                pstmt.setDate(53,null);
            } else {
                pstmt.setDate(53, Date.valueOf(this.get(i).getSignDate()));
            }
            if(this.get(i).getCValiDate() == null || this.get(i).getCValiDate().equals("null")) {
                pstmt.setDate(54,null);
            } else {
                pstmt.setDate(54, Date.valueOf(this.get(i).getCValiDate()));
            }
            pstmt.setInt(55, this.get(i).getPayEndYear());
            pstmt.setDouble(56, this.get(i).getPayMoney());
            // set where condition
            pstmt.setLong(57, this.get(i).getSPayPersonID());
                pstmt.addBatch();
            }
            pstmt.executeBatch();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            ex.printStackTrace();
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LKSPayPersonDBSet";
            tError.functionName = "update()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            try {
                pstmt.close();
            } catch (Exception e){e.printStackTrace();}

            if( !mflag ) {
                try {
                    con.close();
                } catch (Exception e){e.printStackTrace();}
            }

            return false;
        }

        if( !mflag ) {
            try {
                con.close();
            } catch (Exception e){e.printStackTrace();}
        }

        return true;
    }

    /**
     * 新增操作
     * @return boolean
     */
    public boolean insert() {
        PreparedStatement pstmt = null;

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try
        {
            int tCount = this.size();
            pstmt = con.prepareStatement("INSERT INTO LKSPayPerson VALUES( ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ?)");
            for (int i = 1; i <= tCount; i++) {
            pstmt.setLong(1, this.get(i).getSPayPersonID());
            if(this.get(i).getShardingID() == null || this.get(i).getShardingID().equals("null")) {
            	pstmt.setString(2,null);
            } else {
            	pstmt.setString(2, this.get(i).getShardingID());
            }
            if(this.get(i).getPolNo() == null || this.get(i).getPolNo().equals("null")) {
            	pstmt.setString(3,null);
            } else {
            	pstmt.setString(3, this.get(i).getPolNo());
            }
            pstmt.setInt(4, this.get(i).getPayCount());
            if(this.get(i).getGrpContNo() == null || this.get(i).getGrpContNo().equals("null")) {
            	pstmt.setString(5,null);
            } else {
            	pstmt.setString(5, this.get(i).getGrpContNo());
            }
            if(this.get(i).getGrpPolNo() == null || this.get(i).getGrpPolNo().equals("null")) {
            	pstmt.setString(6,null);
            } else {
            	pstmt.setString(6, this.get(i).getGrpPolNo());
            }
            if(this.get(i).getContNo() == null || this.get(i).getContNo().equals("null")) {
            	pstmt.setString(7,null);
            } else {
            	pstmt.setString(7, this.get(i).getContNo());
            }
            if(this.get(i).getManageCom() == null || this.get(i).getManageCom().equals("null")) {
            	pstmt.setString(8,null);
            } else {
            	pstmt.setString(8, this.get(i).getManageCom());
            }
            if(this.get(i).getAgentCom() == null || this.get(i).getAgentCom().equals("null")) {
            	pstmt.setString(9,null);
            } else {
            	pstmt.setString(9, this.get(i).getAgentCom());
            }
            if(this.get(i).getAgentType() == null || this.get(i).getAgentType().equals("null")) {
            	pstmt.setString(10,null);
            } else {
            	pstmt.setString(10, this.get(i).getAgentType());
            }
            if(this.get(i).getRiskCode() == null || this.get(i).getRiskCode().equals("null")) {
            	pstmt.setString(11,null);
            } else {
            	pstmt.setString(11, this.get(i).getRiskCode());
            }
            if(this.get(i).getAgentCode() == null || this.get(i).getAgentCode().equals("null")) {
            	pstmt.setString(12,null);
            } else {
            	pstmt.setString(12, this.get(i).getAgentCode());
            }
            if(this.get(i).getAgentGroup() == null || this.get(i).getAgentGroup().equals("null")) {
            	pstmt.setString(13,null);
            } else {
            	pstmt.setString(13, this.get(i).getAgentGroup());
            }
            if(this.get(i).getPayTypeFlag() == null || this.get(i).getPayTypeFlag().equals("null")) {
            	pstmt.setString(14,null);
            } else {
            	pstmt.setString(14, this.get(i).getPayTypeFlag());
            }
            if(this.get(i).getAppntNo() == null || this.get(i).getAppntNo().equals("null")) {
            	pstmt.setString(15,null);
            } else {
            	pstmt.setString(15, this.get(i).getAppntNo());
            }
            if(this.get(i).getGetNoticeNo() == null || this.get(i).getGetNoticeNo().equals("null")) {
            	pstmt.setString(16,null);
            } else {
            	pstmt.setString(16, this.get(i).getGetNoticeNo());
            }
            if(this.get(i).getPayAimClass() == null || this.get(i).getPayAimClass().equals("null")) {
            	pstmt.setString(17,null);
            } else {
            	pstmt.setString(17, this.get(i).getPayAimClass());
            }
            if(this.get(i).getDutyCode() == null || this.get(i).getDutyCode().equals("null")) {
            	pstmt.setString(18,null);
            } else {
            	pstmt.setString(18, this.get(i).getDutyCode());
            }
            if(this.get(i).getPayPlanCode() == null || this.get(i).getPayPlanCode().equals("null")) {
            	pstmt.setString(19,null);
            } else {
            	pstmt.setString(19, this.get(i).getPayPlanCode());
            }
            pstmt.setDouble(20, this.get(i).getSumDuePayMoney());
            pstmt.setDouble(21, this.get(i).getSumActuPayMoney());
            pstmt.setInt(22, this.get(i).getPayIntv());
            if(this.get(i).getPayDate() == null || this.get(i).getPayDate().equals("null")) {
                pstmt.setDate(23,null);
            } else {
                pstmt.setDate(23, Date.valueOf(this.get(i).getPayDate()));
            }
            if(this.get(i).getPayType() == null || this.get(i).getPayType().equals("null")) {
            	pstmt.setString(24,null);
            } else {
            	pstmt.setString(24, this.get(i).getPayType());
            }
            if(this.get(i).getLastPayToDate() == null || this.get(i).getLastPayToDate().equals("null")) {
                pstmt.setDate(25,null);
            } else {
                pstmt.setDate(25, Date.valueOf(this.get(i).getLastPayToDate()));
            }
            if(this.get(i).getCurPayToDate() == null || this.get(i).getCurPayToDate().equals("null")) {
                pstmt.setDate(26,null);
            } else {
                pstmt.setDate(26, Date.valueOf(this.get(i).getCurPayToDate()));
            }
            if(this.get(i).getInInsuAccState() == null || this.get(i).getInInsuAccState().equals("null")) {
            	pstmt.setString(27,null);
            } else {
            	pstmt.setString(27, this.get(i).getInInsuAccState());
            }
            if(this.get(i).getBankCode() == null || this.get(i).getBankCode().equals("null")) {
            	pstmt.setString(28,null);
            } else {
            	pstmt.setString(28, this.get(i).getBankCode());
            }
            if(this.get(i).getBankAccNo() == null || this.get(i).getBankAccNo().equals("null")) {
            	pstmt.setString(29,null);
            } else {
            	pstmt.setString(29, this.get(i).getBankAccNo());
            }
            if(this.get(i).getBankOnTheWayFlag() == null || this.get(i).getBankOnTheWayFlag().equals("null")) {
            	pstmt.setString(30,null);
            } else {
            	pstmt.setString(30, this.get(i).getBankOnTheWayFlag());
            }
            if(this.get(i).getBankSuccFlag() == null || this.get(i).getBankSuccFlag().equals("null")) {
            	pstmt.setString(31,null);
            } else {
            	pstmt.setString(31, this.get(i).getBankSuccFlag());
            }
            if(this.get(i).getApproveCode() == null || this.get(i).getApproveCode().equals("null")) {
            	pstmt.setString(32,null);
            } else {
            	pstmt.setString(32, this.get(i).getApproveCode());
            }
            if(this.get(i).getApproveDate() == null || this.get(i).getApproveDate().equals("null")) {
                pstmt.setDate(33,null);
            } else {
                pstmt.setDate(33, Date.valueOf(this.get(i).getApproveDate()));
            }
            if(this.get(i).getApproveTime() == null || this.get(i).getApproveTime().equals("null")) {
            	pstmt.setString(34,null);
            } else {
            	pstmt.setString(34, this.get(i).getApproveTime());
            }
            if(this.get(i).getSerialNo() == null || this.get(i).getSerialNo().equals("null")) {
            	pstmt.setString(35,null);
            } else {
            	pstmt.setString(35, this.get(i).getSerialNo());
            }
            if(this.get(i).getInputFlag() == null || this.get(i).getInputFlag().equals("null")) {
            	pstmt.setString(36,null);
            } else {
            	pstmt.setString(36, this.get(i).getInputFlag());
            }
            if(this.get(i).getOperator() == null || this.get(i).getOperator().equals("null")) {
            	pstmt.setString(37,null);
            } else {
            	pstmt.setString(37, this.get(i).getOperator());
            }
            if(this.get(i).getMakeDate() == null || this.get(i).getMakeDate().equals("null")) {
                pstmt.setDate(38,null);
            } else {
                pstmt.setDate(38, Date.valueOf(this.get(i).getMakeDate()));
            }
            if(this.get(i).getMakeTime() == null || this.get(i).getMakeTime().equals("null")) {
            	pstmt.setString(39,null);
            } else {
            	pstmt.setString(39, this.get(i).getMakeTime());
            }
            if(this.get(i).getModifyDate() == null || this.get(i).getModifyDate().equals("null")) {
                pstmt.setDate(40,null);
            } else {
                pstmt.setDate(40, Date.valueOf(this.get(i).getModifyDate()));
            }
            if(this.get(i).getModifyTime() == null || this.get(i).getModifyTime().equals("null")) {
            	pstmt.setString(41,null);
            } else {
            	pstmt.setString(41, this.get(i).getModifyTime());
            }
            if(this.get(i).getEdorNo() == null || this.get(i).getEdorNo().equals("null")) {
            	pstmt.setString(42,null);
            } else {
            	pstmt.setString(42, this.get(i).getEdorNo());
            }
            pstmt.setInt(43, this.get(i).getMainPolYear());
            if(this.get(i).getOtherNo() == null || this.get(i).getOtherNo().equals("null")) {
            	pstmt.setString(44,null);
            } else {
            	pstmt.setString(44, this.get(i).getOtherNo());
            }
            if(this.get(i).getOtherNoType() == null || this.get(i).getOtherNoType().equals("null")) {
            	pstmt.setString(45,null);
            } else {
            	pstmt.setString(45, this.get(i).getOtherNoType());
            }
            if(this.get(i).getPrtNo() == null || this.get(i).getPrtNo().equals("null")) {
            	pstmt.setString(46,null);
            } else {
            	pstmt.setString(46, this.get(i).getPrtNo());
            }
            if(this.get(i).getSaleChnl() == null || this.get(i).getSaleChnl().equals("null")) {
            	pstmt.setString(47,null);
            } else {
            	pstmt.setString(47, this.get(i).getSaleChnl());
            }
            if(this.get(i).getChargeCom() == null || this.get(i).getChargeCom().equals("null")) {
            	pstmt.setString(48,null);
            } else {
            	pstmt.setString(48, this.get(i).getChargeCom());
            }
            if(this.get(i).getAPPntName() == null || this.get(i).getAPPntName().equals("null")) {
            	pstmt.setString(49,null);
            } else {
            	pstmt.setString(49, this.get(i).getAPPntName());
            }
            if(this.get(i).getKindCode() == null || this.get(i).getKindCode().equals("null")) {
            	pstmt.setString(50,null);
            } else {
            	pstmt.setString(50, this.get(i).getKindCode());
            }
            if(this.get(i).getPaytoDate() == null || this.get(i).getPaytoDate().equals("null")) {
                pstmt.setDate(51,null);
            } else {
                pstmt.setDate(51, Date.valueOf(this.get(i).getPaytoDate()));
            }
            if(this.get(i).getPayEndDate() == null || this.get(i).getPayEndDate().equals("null")) {
                pstmt.setDate(52,null);
            } else {
                pstmt.setDate(52, Date.valueOf(this.get(i).getPayEndDate()));
            }
            if(this.get(i).getSignDate() == null || this.get(i).getSignDate().equals("null")) {
                pstmt.setDate(53,null);
            } else {
                pstmt.setDate(53, Date.valueOf(this.get(i).getSignDate()));
            }
            if(this.get(i).getCValiDate() == null || this.get(i).getCValiDate().equals("null")) {
                pstmt.setDate(54,null);
            } else {
                pstmt.setDate(54, Date.valueOf(this.get(i).getCValiDate()));
            }
            pstmt.setInt(55, this.get(i).getPayEndYear());
            pstmt.setDouble(56, this.get(i).getPayMoney());
                pstmt.addBatch();
            }
            pstmt.executeBatch();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            ex.printStackTrace();
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LKSPayPersonDBSet";
            tError.functionName = "insert()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            try {
                pstmt.close();
            } catch (Exception e){e.printStackTrace();}

            if( !mflag ) {
                try {
                    con.close();
                } catch (Exception e){e.printStackTrace();}
            }

            return false;
        }

        if( !mflag ) {
            try {
                con.close();
            } catch (Exception e){e.printStackTrace();}
        }

        return true;
    }
}
