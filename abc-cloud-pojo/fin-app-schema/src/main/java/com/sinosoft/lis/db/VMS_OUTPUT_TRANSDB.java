/**
 * Copyright (c) 2017 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.db;

import com.sinosoft.lis.schema.VMS_OUTPUT_TRANSSchema;
import com.sinosoft.lis.vschema.VMS_OUTPUT_TRANSSet;
import com.sinosoft.utility.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

/**
 * <p>ClassName: VMS_OUTPUT_TRANSDB </p>
 * <p>Description: DB层数据库操作类文件 </p>
 * <p>Copyright: Copyright (c) 2017</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2017-10-21
 */
public class VMS_OUTPUT_TRANSDB extends VMS_OUTPUT_TRANSSchema {
    // @Field
    private Connection con;
    private DBOper db;
    /**
    * flag = true: 传入Connection
    * flag = false: 不传入Connection
    **/
    private boolean mflag = false;

    public CErrors mErrors = new CErrors();        // 错误信息

    /**
     * 为批量操作而准备的语句和游标对象
     */
    private ResultSet mResultSet = null;
    private Statement mStatement = null;
    // @Constructor
    public VMS_OUTPUT_TRANSDB( Connection tConnection ) {
        con = tConnection;
        db = new DBOper( con, "VMS_OUTPUT_TRANS" );
        mflag = true;
    }

    public VMS_OUTPUT_TRANSDB() {
        con = null;
        db = new DBOper( "VMS_OUTPUT_TRANS" );
        mflag = false;
    }

    // @Method
    public boolean deleteSQL() {
        VMS_OUTPUT_TRANSSchema tSchema = this.getSchema();
        if (db.deleteSQL(tSchema)) {
             return true;
        } else {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "VMS_OUTPUT_TRANSDB";
            tError.functionName = "deleteSQL";
            tError.errorMessage = "操作失败!";
            this.mErrors .addOneError(tError);
            return false;
        }
    }

    public int getCount() {
        VMS_OUTPUT_TRANSSchema tSchema = this.getSchema();

        int tCount = db.getCount(tSchema);
        if (tCount < 0) {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "VMS_OUTPUT_TRANSDB";
            tError.functionName = "getCount";
            tError.errorMessage = "操作失败!";
            this.mErrors .addOneError(tError);

            return -1;
        }

        return tCount;
    }

    public boolean delete() {
        PreparedStatement pstmt = null;

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try {
            pstmt = con.prepareStatement("DELETE FROM VMS_OUTPUT_TRANS WHERE  1=1  AND BUSINESS_ID = ?");
            if(this.getBUSINESS_ID() == null || this.getBUSINESS_ID().equals("null")) {
            	pstmt.setNull(1, 12);
            } else {
            	pstmt.setString(1, this.getBUSINESS_ID());
            }
            pstmt.executeUpdate();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "VMS_OUTPUT_TRANSDB";
            tError.functionName = "delete()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);
            ex.printStackTrace();

            try {
                pstmt.close();
            } catch (Exception e){}

            if(!mflag) {
                try {
                    con.close();
                } catch (Exception e){}
            }

            return false;
        }

        if(!mflag) {
            try {
                con.close();
            } catch (Exception e){}
        }

        return true;
    }

    public boolean update() {
        PreparedStatement pstmt = null;

        SQLString sqlObj = new SQLString("VMS_OUTPUT_TRANS");
        sqlObj.setSQL(2, this);
        String sql = sqlObj.getSQL();

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try {
            pstmt = con.prepareStatement("UPDATE VMS_OUTPUT_TRANS SET  BUSINESS_ID = ? , GRPCONTNO = ? , GRPPOLNO = ? , CHERNUM = ? , POLNO = ? , TTMPRCNO = ? , COWNNUM = ? , ORIGCURR = ? , TAX_RATE = ? , ORIG_AMT = ? , ORIG_INCOME = ? , ORIG_TAX_AMT = ? , AMT_CNY = ? , INCOME_CNY = ? , TAX_AMT_CNY = ? , DC_FLAG = ? , HESITAGE = ? , SERIALNO = ? , TRA_DT = ? , TRA_TYP = ? , BATCTRCDE = ? , TRA_BRA = ? , INVTYP = ? , FEETYP = ? , INCOMETYP = ? , BILLFREQ = ? , POLYEAR = ? , HISSDTE = ? , DSOURCE = ? , GL_CD_ID = ? , GL_CD_GRD = ? , PRODUCT_COD = ? , INVONO = ? , OTHERNO = ? , INS_COD = ? , INSTFROM = ? , INSTTO = ? , OCCDATE = ? , PREMTERM = ? , UPLOADFLAG = ? , BUSSNO = ? , STANDBYFLAG1 = ? , STANDBYFLAG2 = ? , FPFLAG = ? , MAKEDATE = ? , MAKETIME = ? , MODIFYDATE = ? , MODIFYTIME = ? , PRODUCT_NAME = ? , SALECHNL = ? WHERE  1=1  AND BUSINESS_ID = ?");
            if(this.getBUSINESS_ID() == null || this.getBUSINESS_ID().equals("null")) {
            	pstmt.setNull(1, 12);
            } else {
            	pstmt.setString(1, this.getBUSINESS_ID());
            }
            if(this.getGRPCONTNO() == null || this.getGRPCONTNO().equals("null")) {
            	pstmt.setNull(2, 12);
            } else {
            	pstmt.setString(2, this.getGRPCONTNO());
            }
            if(this.getGRPPOLNO() == null || this.getGRPPOLNO().equals("null")) {
            	pstmt.setNull(3, 12);
            } else {
            	pstmt.setString(3, this.getGRPPOLNO());
            }
            if(this.getCHERNUM() == null || this.getCHERNUM().equals("null")) {
            	pstmt.setNull(4, 12);
            } else {
            	pstmt.setString(4, this.getCHERNUM());
            }
            if(this.getPOLNO() == null || this.getPOLNO().equals("null")) {
            	pstmt.setNull(5, 12);
            } else {
            	pstmt.setString(5, this.getPOLNO());
            }
            if(this.getTTMPRCNO() == null || this.getTTMPRCNO().equals("null")) {
            	pstmt.setNull(6, 12);
            } else {
            	pstmt.setString(6, this.getTTMPRCNO());
            }
            if(this.getCOWNNUM() == null || this.getCOWNNUM().equals("null")) {
            	pstmt.setNull(7, 12);
            } else {
            	pstmt.setString(7, this.getCOWNNUM());
            }
            if(this.getORIGCURR() == null || this.getORIGCURR().equals("null")) {
            	pstmt.setNull(8, 1);
            } else {
            	pstmt.setString(8, this.getORIGCURR());
            }
            pstmt.setDouble(9, this.getTAX_RATE());
            pstmt.setDouble(10, this.getORIG_AMT());
            pstmt.setDouble(11, this.getORIG_INCOME());
            pstmt.setDouble(12, this.getORIG_TAX_AMT());
            pstmt.setDouble(13, this.getAMT_CNY());
            pstmt.setDouble(14, this.getINCOME_CNY());
            pstmt.setDouble(15, this.getTAX_AMT_CNY());
            if(this.getDC_FLAG() == null || this.getDC_FLAG().equals("null")) {
            	pstmt.setNull(16, 1);
            } else {
            	pstmt.setString(16, this.getDC_FLAG());
            }
            if(this.getHESITAGE() == null || this.getHESITAGE().equals("null")) {
            	pstmt.setNull(17, 12);
            } else {
            	pstmt.setString(17, this.getHESITAGE());
            }
            if(this.getSERIALNO() == null || this.getSERIALNO().equals("null")) {
            	pstmt.setNull(18, 12);
            } else {
            	pstmt.setString(18, this.getSERIALNO());
            }
            if(this.getTRA_DT() == null || this.getTRA_DT().equals("null")) {
            	pstmt.setNull(19, 12);
            } else {
            	pstmt.setString(19, this.getTRA_DT());
            }
            if(this.getTRA_TYP() == null || this.getTRA_TYP().equals("null")) {
            	pstmt.setNull(20, 12);
            } else {
            	pstmt.setString(20, this.getTRA_TYP());
            }
            if(this.getBATCTRCDE() == null || this.getBATCTRCDE().equals("null")) {
            	pstmt.setNull(21, 12);
            } else {
            	pstmt.setString(21, this.getBATCTRCDE());
            }
            if(this.getTRA_BRA() == null || this.getTRA_BRA().equals("null")) {
            	pstmt.setNull(22, 12);
            } else {
            	pstmt.setString(22, this.getTRA_BRA());
            }
            if(this.getINVTYP() == null || this.getINVTYP().equals("null")) {
            	pstmt.setNull(23, 1);
            } else {
            	pstmt.setString(23, this.getINVTYP());
            }
            if(this.getFEETYP() == null || this.getFEETYP().equals("null")) {
            	pstmt.setNull(24, 1);
            } else {
            	pstmt.setString(24, this.getFEETYP());
            }
            if(this.getINCOMETYP() == null || this.getINCOMETYP().equals("null")) {
            	pstmt.setNull(25, 1);
            } else {
            	pstmt.setString(25, this.getINCOMETYP());
            }
            if(this.getBILLFREQ() == null || this.getBILLFREQ().equals("null")) {
            	pstmt.setNull(26, 1);
            } else {
            	pstmt.setString(26, this.getBILLFREQ());
            }
            if(this.getPOLYEAR() == null || this.getPOLYEAR().equals("null")) {
            	pstmt.setNull(27, 12);
            } else {
            	pstmt.setString(27, this.getPOLYEAR());
            }
            if(this.getHISSDTE() == null || this.getHISSDTE().equals("null")) {
            	pstmt.setNull(28, 12);
            } else {
            	pstmt.setString(28, this.getHISSDTE());
            }
            if(this.getDSOURCE() == null || this.getDSOURCE().equals("null")) {
            	pstmt.setNull(29, 12);
            } else {
            	pstmt.setString(29, this.getDSOURCE());
            }
            if(this.getGL_CD_ID() == null || this.getGL_CD_ID().equals("null")) {
            	pstmt.setNull(30, 12);
            } else {
            	pstmt.setString(30, this.getGL_CD_ID());
            }
            if(this.getGL_CD_GRD() == null || this.getGL_CD_GRD().equals("null")) {
            	pstmt.setNull(31, 12);
            } else {
            	pstmt.setString(31, this.getGL_CD_GRD());
            }
            if(this.getPRODUCT_COD() == null || this.getPRODUCT_COD().equals("null")) {
            	pstmt.setNull(32, 12);
            } else {
            	pstmt.setString(32, this.getPRODUCT_COD());
            }
            if(this.getINVONO() == null || this.getINVONO().equals("null")) {
            	pstmt.setNull(33, 12);
            } else {
            	pstmt.setString(33, this.getINVONO());
            }
            if(this.getOTHERNO() == null || this.getOTHERNO().equals("null")) {
            	pstmt.setNull(34, 12);
            } else {
            	pstmt.setString(34, this.getOTHERNO());
            }
            if(this.getINS_COD() == null || this.getINS_COD().equals("null")) {
            	pstmt.setNull(35, 12);
            } else {
            	pstmt.setString(35, this.getINS_COD());
            }
            if(this.getINSTFROM() == null || this.getINSTFROM().equals("null")) {
            	pstmt.setNull(36, 12);
            } else {
            	pstmt.setString(36, this.getINSTFROM());
            }
            if(this.getINSTTO() == null || this.getINSTTO().equals("null")) {
            	pstmt.setNull(37, 12);
            } else {
            	pstmt.setString(37, this.getINSTTO());
            }
            if(this.getOCCDATE() == null || this.getOCCDATE().equals("null")) {
            	pstmt.setNull(38, 12);
            } else {
            	pstmt.setString(38, this.getOCCDATE());
            }
            if(this.getPREMTERM() == null || this.getPREMTERM().equals("null")) {
            	pstmt.setNull(39, 12);
            } else {
            	pstmt.setString(39, this.getPREMTERM());
            }
            if(this.getUPLOADFLAG() == null || this.getUPLOADFLAG().equals("null")) {
            	pstmt.setNull(40, 12);
            } else {
            	pstmt.setString(40, this.getUPLOADFLAG());
            }
            if(this.getBUSSNO() == null || this.getBUSSNO().equals("null")) {
            	pstmt.setNull(41, 12);
            } else {
            	pstmt.setString(41, this.getBUSSNO());
            }
            if(this.getSTANDBYFLAG1() == null || this.getSTANDBYFLAG1().equals("null")) {
            	pstmt.setNull(42, 12);
            } else {
            	pstmt.setString(42, this.getSTANDBYFLAG1());
            }
            if(this.getSTANDBYFLAG2() == null || this.getSTANDBYFLAG2().equals("null")) {
            	pstmt.setNull(43, 12);
            } else {
            	pstmt.setString(43, this.getSTANDBYFLAG2());
            }
            if(this.getFPFLAG() == null || this.getFPFLAG().equals("null")) {
            	pstmt.setNull(44, 12);
            } else {
            	pstmt.setString(44, this.getFPFLAG());
            }
            if(this.getMAKEDATE() == null || this.getMAKEDATE().equals("null")) {
            	pstmt.setNull(45, 12);
            } else {
            	pstmt.setString(45, this.getMAKEDATE());
            }
            if(this.getMAKETIME() == null || this.getMAKETIME().equals("null")) {
            	pstmt.setNull(46, 12);
            } else {
            	pstmt.setString(46, this.getMAKETIME());
            }
            if(this.getMODIFYDATE() == null || this.getMODIFYDATE().equals("null")) {
            	pstmt.setNull(47, 12);
            } else {
            	pstmt.setString(47, this.getMODIFYDATE());
            }
            if(this.getMODIFYTIME() == null || this.getMODIFYTIME().equals("null")) {
            	pstmt.setNull(48, 12);
            } else {
            	pstmt.setString(48, this.getMODIFYTIME());
            }
            if(this.getPRODUCT_NAME() == null || this.getPRODUCT_NAME().equals("null")) {
            	pstmt.setNull(49, 12);
            } else {
            	pstmt.setString(49, this.getPRODUCT_NAME());
            }
            if(this.getSALECHNL() == null || this.getSALECHNL().equals("null")) {
            	pstmt.setNull(50, 12);
            } else {
            	pstmt.setString(50, this.getSALECHNL());
            }
            // set where condition
            if(this.getBUSINESS_ID() == null || this.getBUSINESS_ID().equals("null")) {
            	pstmt.setNull(51, 12);
            } else {
            	pstmt.setString(51, this.getBUSINESS_ID());
            }
            pstmt.executeUpdate();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "VMS_OUTPUT_TRANSDB";
            tError.functionName = "update()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);
            ex.printStackTrace();

            try {
                pstmt.close();
            } catch (Exception e){}

            if( !mflag ) {
                try {
                    con.close();
                } catch (Exception e){}
            }

            return false;
        }

        if(!mflag) {
            try {
                con.close();
            } catch (Exception e){}
        }

        return true;
    }

    public boolean insert() {
        PreparedStatement pstmt = null;

        SQLString sqlObj = new SQLString("VMS_OUTPUT_TRANS");
        sqlObj.setSQL(1, this);
        String sql = sqlObj.getSQL();

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try {
            pstmt = con.prepareStatement("INSERT INTO VMS_OUTPUT_TRANS VALUES( ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ?)");
            if(this.getBUSINESS_ID() == null || this.getBUSINESS_ID().equals("null")) {
            	pstmt.setNull(1, 12);
            } else {
            	pstmt.setString(1, this.getBUSINESS_ID());
            }
            if(this.getGRPCONTNO() == null || this.getGRPCONTNO().equals("null")) {
            	pstmt.setNull(2, 12);
            } else {
            	pstmt.setString(2, this.getGRPCONTNO());
            }
            if(this.getGRPPOLNO() == null || this.getGRPPOLNO().equals("null")) {
            	pstmt.setNull(3, 12);
            } else {
            	pstmt.setString(3, this.getGRPPOLNO());
            }
            if(this.getCHERNUM() == null || this.getCHERNUM().equals("null")) {
            	pstmt.setNull(4, 12);
            } else {
            	pstmt.setString(4, this.getCHERNUM());
            }
            if(this.getPOLNO() == null || this.getPOLNO().equals("null")) {
            	pstmt.setNull(5, 12);
            } else {
            	pstmt.setString(5, this.getPOLNO());
            }
            if(this.getTTMPRCNO() == null || this.getTTMPRCNO().equals("null")) {
            	pstmt.setNull(6, 12);
            } else {
            	pstmt.setString(6, this.getTTMPRCNO());
            }
            if(this.getCOWNNUM() == null || this.getCOWNNUM().equals("null")) {
            	pstmt.setNull(7, 12);
            } else {
            	pstmt.setString(7, this.getCOWNNUM());
            }
            if(this.getORIGCURR() == null || this.getORIGCURR().equals("null")) {
            	pstmt.setNull(8, 1);
            } else {
            	pstmt.setString(8, this.getORIGCURR());
            }
            pstmt.setDouble(9, this.getTAX_RATE());
            pstmt.setDouble(10, this.getORIG_AMT());
            pstmt.setDouble(11, this.getORIG_INCOME());
            pstmt.setDouble(12, this.getORIG_TAX_AMT());
            pstmt.setDouble(13, this.getAMT_CNY());
            pstmt.setDouble(14, this.getINCOME_CNY());
            pstmt.setDouble(15, this.getTAX_AMT_CNY());
            if(this.getDC_FLAG() == null || this.getDC_FLAG().equals("null")) {
            	pstmt.setNull(16, 1);
            } else {
            	pstmt.setString(16, this.getDC_FLAG());
            }
            if(this.getHESITAGE() == null || this.getHESITAGE().equals("null")) {
            	pstmt.setNull(17, 12);
            } else {
            	pstmt.setString(17, this.getHESITAGE());
            }
            if(this.getSERIALNO() == null || this.getSERIALNO().equals("null")) {
            	pstmt.setNull(18, 12);
            } else {
            	pstmt.setString(18, this.getSERIALNO());
            }
            if(this.getTRA_DT() == null || this.getTRA_DT().equals("null")) {
            	pstmt.setNull(19, 12);
            } else {
            	pstmt.setString(19, this.getTRA_DT());
            }
            if(this.getTRA_TYP() == null || this.getTRA_TYP().equals("null")) {
            	pstmt.setNull(20, 12);
            } else {
            	pstmt.setString(20, this.getTRA_TYP());
            }
            if(this.getBATCTRCDE() == null || this.getBATCTRCDE().equals("null")) {
            	pstmt.setNull(21, 12);
            } else {
            	pstmt.setString(21, this.getBATCTRCDE());
            }
            if(this.getTRA_BRA() == null || this.getTRA_BRA().equals("null")) {
            	pstmt.setNull(22, 12);
            } else {
            	pstmt.setString(22, this.getTRA_BRA());
            }
            if(this.getINVTYP() == null || this.getINVTYP().equals("null")) {
            	pstmt.setNull(23, 1);
            } else {
            	pstmt.setString(23, this.getINVTYP());
            }
            if(this.getFEETYP() == null || this.getFEETYP().equals("null")) {
            	pstmt.setNull(24, 1);
            } else {
            	pstmt.setString(24, this.getFEETYP());
            }
            if(this.getINCOMETYP() == null || this.getINCOMETYP().equals("null")) {
            	pstmt.setNull(25, 1);
            } else {
            	pstmt.setString(25, this.getINCOMETYP());
            }
            if(this.getBILLFREQ() == null || this.getBILLFREQ().equals("null")) {
            	pstmt.setNull(26, 1);
            } else {
            	pstmt.setString(26, this.getBILLFREQ());
            }
            if(this.getPOLYEAR() == null || this.getPOLYEAR().equals("null")) {
            	pstmt.setNull(27, 12);
            } else {
            	pstmt.setString(27, this.getPOLYEAR());
            }
            if(this.getHISSDTE() == null || this.getHISSDTE().equals("null")) {
            	pstmt.setNull(28, 12);
            } else {
            	pstmt.setString(28, this.getHISSDTE());
            }
            if(this.getDSOURCE() == null || this.getDSOURCE().equals("null")) {
            	pstmt.setNull(29, 12);
            } else {
            	pstmt.setString(29, this.getDSOURCE());
            }
            if(this.getGL_CD_ID() == null || this.getGL_CD_ID().equals("null")) {
            	pstmt.setNull(30, 12);
            } else {
            	pstmt.setString(30, this.getGL_CD_ID());
            }
            if(this.getGL_CD_GRD() == null || this.getGL_CD_GRD().equals("null")) {
            	pstmt.setNull(31, 12);
            } else {
            	pstmt.setString(31, this.getGL_CD_GRD());
            }
            if(this.getPRODUCT_COD() == null || this.getPRODUCT_COD().equals("null")) {
            	pstmt.setNull(32, 12);
            } else {
            	pstmt.setString(32, this.getPRODUCT_COD());
            }
            if(this.getINVONO() == null || this.getINVONO().equals("null")) {
            	pstmt.setNull(33, 12);
            } else {
            	pstmt.setString(33, this.getINVONO());
            }
            if(this.getOTHERNO() == null || this.getOTHERNO().equals("null")) {
            	pstmt.setNull(34, 12);
            } else {
            	pstmt.setString(34, this.getOTHERNO());
            }
            if(this.getINS_COD() == null || this.getINS_COD().equals("null")) {
            	pstmt.setNull(35, 12);
            } else {
            	pstmt.setString(35, this.getINS_COD());
            }
            if(this.getINSTFROM() == null || this.getINSTFROM().equals("null")) {
            	pstmt.setNull(36, 12);
            } else {
            	pstmt.setString(36, this.getINSTFROM());
            }
            if(this.getINSTTO() == null || this.getINSTTO().equals("null")) {
            	pstmt.setNull(37, 12);
            } else {
            	pstmt.setString(37, this.getINSTTO());
            }
            if(this.getOCCDATE() == null || this.getOCCDATE().equals("null")) {
            	pstmt.setNull(38, 12);
            } else {
            	pstmt.setString(38, this.getOCCDATE());
            }
            if(this.getPREMTERM() == null || this.getPREMTERM().equals("null")) {
            	pstmt.setNull(39, 12);
            } else {
            	pstmt.setString(39, this.getPREMTERM());
            }
            if(this.getUPLOADFLAG() == null || this.getUPLOADFLAG().equals("null")) {
            	pstmt.setNull(40, 12);
            } else {
            	pstmt.setString(40, this.getUPLOADFLAG());
            }
            if(this.getBUSSNO() == null || this.getBUSSNO().equals("null")) {
            	pstmt.setNull(41, 12);
            } else {
            	pstmt.setString(41, this.getBUSSNO());
            }
            if(this.getSTANDBYFLAG1() == null || this.getSTANDBYFLAG1().equals("null")) {
            	pstmt.setNull(42, 12);
            } else {
            	pstmt.setString(42, this.getSTANDBYFLAG1());
            }
            if(this.getSTANDBYFLAG2() == null || this.getSTANDBYFLAG2().equals("null")) {
            	pstmt.setNull(43, 12);
            } else {
            	pstmt.setString(43, this.getSTANDBYFLAG2());
            }
            if(this.getFPFLAG() == null || this.getFPFLAG().equals("null")) {
            	pstmt.setNull(44, 12);
            } else {
            	pstmt.setString(44, this.getFPFLAG());
            }
            if(this.getMAKEDATE() == null || this.getMAKEDATE().equals("null")) {
            	pstmt.setNull(45, 12);
            } else {
            	pstmt.setString(45, this.getMAKEDATE());
            }
            if(this.getMAKETIME() == null || this.getMAKETIME().equals("null")) {
            	pstmt.setNull(46, 12);
            } else {
            	pstmt.setString(46, this.getMAKETIME());
            }
            if(this.getMODIFYDATE() == null || this.getMODIFYDATE().equals("null")) {
            	pstmt.setNull(47, 12);
            } else {
            	pstmt.setString(47, this.getMODIFYDATE());
            }
            if(this.getMODIFYTIME() == null || this.getMODIFYTIME().equals("null")) {
            	pstmt.setNull(48, 12);
            } else {
            	pstmt.setString(48, this.getMODIFYTIME());
            }
            if(this.getPRODUCT_NAME() == null || this.getPRODUCT_NAME().equals("null")) {
            	pstmt.setNull(49, 12);
            } else {
            	pstmt.setString(49, this.getPRODUCT_NAME());
            }
            if(this.getSALECHNL() == null || this.getSALECHNL().equals("null")) {
            	pstmt.setNull(50, 12);
            } else {
            	pstmt.setString(50, this.getSALECHNL());
            }
            // execute sql
            pstmt.executeUpdate();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "VMS_OUTPUT_TRANSDB";
            tError.functionName = "insert()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors.addOneError(tError);
            ex.printStackTrace();

            try {
                pstmt.close();
            } catch (Exception e){}

            if( !mflag ) {
                try {
                    con.close();
                } catch (Exception e){}
            }

            return false;
        }

        if(!mflag) {
            try {
                con.close();
            } catch (Exception e){}
        }

        return true;
    }

    public boolean getInfo() {
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        if(!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            pstmt = con.prepareStatement("SELECT * FROM VMS_OUTPUT_TRANS WHERE  1=1  AND BUSINESS_ID = ?", 
                ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
            if(this.getBUSINESS_ID() == null || this.getBUSINESS_ID().equals("null")) {
            	pstmt.setNull(1, 12);
            } else {
            	pstmt.setString(1, this.getBUSINESS_ID());
            }
            rs = pstmt.executeQuery();
            int i = 0;
            while (rs.next()) {
                i++;
                if (!this.setSchema(rs,i)) {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "VMS_OUTPUT_TRANSDB";
                    tError.functionName = "getInfo";
                    tError.errorMessage = "取数失败!";
                    this.mErrors .addOneError(tError);

                    try{ rs.close(); } catch( Exception ex ) {}
                    try{ pstmt.close(); } catch( Exception ex1 ) {}

                    if (!mflag) {
                        try {
                            con.close();
                        }
                        catch(Exception et){}
                    }
                    return false;
                }
                break;
            }
            try{ rs.close(); } catch( Exception ex2 ) {}
            try{ pstmt.close(); } catch( Exception ex3 ) {}

            if( i == 0 ) {
                if (!mflag) {
                    try {
                        con.close();
                    }
                    catch(Exception et){}
                }
                return false;
            }
        }
        catch(Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "VMS_OUTPUT_TRANSDB";
            tError.functionName = "getInfo";
            tError.errorMessage = ExceptionUtils.exceptionToString(e);
            this.mErrors .addOneError(tError);

            try{ rs.close(); } catch( Exception ex ) {}
            try{ pstmt.close(); } catch( Exception ex1 ) {}

            if (!mflag) {
                try {
                    con.close();
                }
                catch(Exception et){}
            }
            return false;
        }
        // 断开数据库连接
        if (!mflag) {
            try {
                con.close();
            }
            catch(Exception e){}
        }

        return true;
    }

    public VMS_OUTPUT_TRANSSet query() {
        Statement stmt = null;
        ResultSet rs = null;
        VMS_OUTPUT_TRANSSet aVMS_OUTPUT_TRANSSet = new VMS_OUTPUT_TRANSSet();

        if (!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            stmt = con.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
            SQLString sqlObj = new SQLString("VMS_OUTPUT_TRANS");
            VMS_OUTPUT_TRANSSchema aSchema = this.getSchema();
            sqlObj.setSQL(5, aSchema);
            String sql = sqlObj.getSQL();

            rs = stmt.executeQuery(sql);
            int i = 0;
            while (rs.next()) {
                i++;
                if (i>10000) {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "VMS_OUTPUT_TRANSDB";
                    tError.functionName = "query";
                    tError.errorMessage = "SQL语句返回结果集太多，请换用其他方式!";
                    this.mErrors .addOneError(tError);
                        break;
                }
                VMS_OUTPUT_TRANSSchema s1 = new VMS_OUTPUT_TRANSSchema();
                s1.setSchema(rs,i);
                aVMS_OUTPUT_TRANSSet.add(s1);
            }
            try{ rs.close(); } catch( Exception ex ) {}
            try{ stmt.close(); } catch( Exception ex1 ) {}
        }
        catch(Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "VMS_OUTPUT_TRANSDB";
            tError.functionName = "query";
            tError.errorMessage = ExceptionUtils.exceptionToString(e);
            this.mErrors.addOneError(tError);
            e.printStackTrace();

            try { rs.close(); } catch (Exception ex2) { ex2.printStackTrace(); }
            try { stmt.close(); } catch(Exception ex3) { ex3.printStackTrace(); }

            if (!mflag) {
                try {
                    con.close();
                }
                catch(Exception et){et.printStackTrace();}
            }
        }

        if (!mflag) {
            try {
                con.close();
            }
            catch(Exception e){e.printStackTrace();}
        }

        return aVMS_OUTPUT_TRANSSet;
    }

    public VMS_OUTPUT_TRANSSet executeQuery(String sql)  {
        Statement stmt = null;
        ResultSet rs = null;
        VMS_OUTPUT_TRANSSet aVMS_OUTPUT_TRANSSet = new VMS_OUTPUT_TRANSSet();

        if (!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            stmt = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);

            rs = stmt.executeQuery(StrTool.GBKToUnicode(sql));
            int i = 0;
            while (rs.next()) {
                i++;
                if (i>10000) {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "VMS_OUTPUT_TRANSDB";
                    tError.functionName = "executeQuery";
                    tError.errorMessage = "SQL语句返回结果集太多，请换用其他方式!";
                    this.mErrors .addOneError(tError);
                        break;
                }
                VMS_OUTPUT_TRANSSchema s1 = new VMS_OUTPUT_TRANSSchema();
                if (!s1.setSchema(rs,i)) {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "VMS_OUTPUT_TRANSDB";
                    tError.functionName = "executeQuery";
                    tError.errorMessage = "sql语句有误，请查看表名及字段名信息!";
                    this.mErrors .addOneError(tError);
                }
                aVMS_OUTPUT_TRANSSet.add(s1);
            }
            try{ rs.close(); } catch( Exception ex ) {}
            try{ stmt.close(); } catch( Exception ex1 ) {}
        }
        catch(Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "VMS_OUTPUT_TRANSDB";
            tError.functionName = "executeQuery";
            tError.errorMessage = ExceptionUtils.exceptionToString(e);
            this.mErrors .addOneError(tError);

            try{ rs.close(); } catch( Exception ex2 ) {}
            try{ stmt.close(); } catch( Exception ex3 ) {}

            if (!mflag) {
                try {
                    con.close();
                }
                catch(Exception et){}
            }
        }

        if (!mflag) {
            try {
                con.close();
            }
            catch(Exception e){}
        }

        return aVMS_OUTPUT_TRANSSet;
    }

    public VMS_OUTPUT_TRANSSet query(int nStart, int nCount) {
        Statement stmt = null;
        ResultSet rs = null;
        VMS_OUTPUT_TRANSSet aVMS_OUTPUT_TRANSSet = new VMS_OUTPUT_TRANSSet();

        if (!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            stmt = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);
            SQLString sqlObj = new SQLString("VMS_OUTPUT_TRANS");
            VMS_OUTPUT_TRANSSchema aSchema = this.getSchema();
            sqlObj.setSQL(5,aSchema);
            String sql = sqlObj.getSQL();

            rs = stmt.executeQuery(sql);
            int i = 0;
            while (rs.next()) {
                i++;

                if( i < nStart ) {
                    continue;
                }

                if( i >= nStart + nCount ) {
                    break;
                }

                VMS_OUTPUT_TRANSSchema s1 = new VMS_OUTPUT_TRANSSchema();
                s1.setSchema(rs,i);
                aVMS_OUTPUT_TRANSSet.add(s1);
            }
            try{ rs.close(); } catch( Exception ex ) {}
            try{ stmt.close(); } catch( Exception ex1 ) {}
        }
        catch(Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "VMS_OUTPUT_TRANSDB";
            tError.functionName = "query";
            tError.errorMessage = ExceptionUtils.exceptionToString(e);
            this.mErrors .addOneError(tError);

            try{ rs.close(); } catch( Exception ex2 ) {}
            try{ stmt.close(); } catch( Exception ex3 ) {}

            if (!mflag) {
                try {
                    con.close();
                }
                catch(Exception et){}
            }
        }

        if (!mflag) {
            try {
                con.close();
            }
            catch(Exception e){}
        }

        return aVMS_OUTPUT_TRANSSet;
    }

    public VMS_OUTPUT_TRANSSet executeQuery(String sql, int nStart, int nCount) {
        Statement stmt = null;
        ResultSet rs = null;
        VMS_OUTPUT_TRANSSet aVMS_OUTPUT_TRANSSet = new VMS_OUTPUT_TRANSSet();

        if (!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            stmt = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);

            rs = stmt.executeQuery(StrTool.GBKToUnicode(sql));
            int i = 0;
            while (rs.next()) {
                i++;

                if( i < nStart ) {
                    continue;
                }

                if( i >= nStart + nCount ) {
                    break;
                }

                VMS_OUTPUT_TRANSSchema s1 = new VMS_OUTPUT_TRANSSchema();
                if (!s1.setSchema(rs,i)) {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "VMS_OUTPUT_TRANSDB";
                    tError.functionName = "executeQuery";
                    tError.errorMessage = "sql语句有误，请查看表名及字段名信息!";
                    this.mErrors .addOneError(tError);
                }
                aVMS_OUTPUT_TRANSSet.add(s1);
            }
            try{ rs.close(); } catch( Exception ex ) {}
            try{ stmt.close(); } catch( Exception ex1 ) {}
        }
        catch(Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "VMS_OUTPUT_TRANSDB";
            tError.functionName = "executeQuery";
            tError.errorMessage = ExceptionUtils.exceptionToString(e);
            this.mErrors .addOneError(tError);

            try{ rs.close(); } catch( Exception ex2 ) {}
            try{ stmt.close(); } catch( Exception ex3 ) {}

            if (!mflag) {
                try {
                    con.close();
                }
                catch(Exception et){}
            }
        }

        if (!mflag) {
            try {
                con.close();
            }
            catch(Exception e){}
        }

        return aVMS_OUTPUT_TRANSSet;
    }

    public boolean update(String strWherePart) {
        Statement stmt = null;

        if (!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            stmt = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);
            SQLString sqlObj = new SQLString("VMS_OUTPUT_TRANS");
            VMS_OUTPUT_TRANSSchema aSchema = this.getSchema();
            sqlObj.setSQL(2,aSchema);
            String sql = "update VMS_OUTPUT_TRANS " + sqlObj.getUpdPart() + " where " + strWherePart;

            int operCount = stmt.executeUpdate(sql);
            if (operCount == 0) {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "VMS_OUTPUT_TRANSDB";
                tError.functionName = "update";
                tError.errorMessage = "更新数据失败!";
                this.mErrors .addOneError(tError);

                if (!mflag) {
                    try {
                        con.close();
                    }
                    catch(Exception et){}
                }
                return false;
            }
        }
        catch(Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "VMS_OUTPUT_TRANSDB";
            tError.functionName = "update";
            tError.errorMessage = ExceptionUtils.exceptionToString(e);
            this.mErrors .addOneError(tError);

            try{ stmt.close(); } catch( Exception ex1 ) {}

            if (!mflag) {
                try {
                    con.close();
                }
                catch(Exception et){}
            }
            return false;
        }
        // 断开数据库连接
        if (!mflag) {
            try {
                con.close();
            }
            catch(Exception e){}
        }

        return true;
    }

    /**
     * 准备数据查询条件
     * @param strSQL String
     * @return boolean
     */
    public boolean prepareData(String strSQL) {
        if (mResultSet != null) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "VMS_OUTPUT_TRANSDB";
            tError.functionName = "prepareData";
            tError.errorMessage = "数据集非空，程序在准备数据集之后，没有关闭！";
            this.mErrors.addOneError(tError);
            return false;
        }

        if (!mflag) {
            con = DBConnPool.getConnection();
        }
        try {
            mStatement = con.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
            mResultSet = mStatement.executeQuery(StrTool.GBKToUnicode(strSQL));
        }
        catch (Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "VMS_OUTPUT_TRANSDB";
            tError.functionName = "prepareData";
            tError.errorMessage = ExceptionUtils.exceptionToString(e);
            this.mErrors.addOneError(tError);
            try {
                mResultSet.close();
            }
            catch (Exception ex2) {}
            try {
                mStatement.close();
            }
            catch (Exception ex3) {}
            if (!mflag) {
                try {
                    con.close();
                }
                catch (Exception et) {}
            }
            return false;
        }

        if (!mflag) {
            try {
                con.close();
            }
            catch (Exception e) {}
        }
        return true;
    }

    /**
     * 获取数据集
     * @return boolean
     */
    public boolean hasMoreData() {
        boolean flag = true;
        if (null == mResultSet) {
            CError tError = new CError();
            tError.moduleName = "VMS_OUTPUT_TRANSDB";
            tError.functionName = "hasMoreData";
            tError.errorMessage = "数据集为空，请先准备数据集！";
            this.mErrors.addOneError(tError);
            return false;
        }
        try {
            flag = mResultSet.next();
        }
        catch (Exception ex) {
            CError tError = new CError();
            tError.moduleName = "VMS_OUTPUT_TRANSDB";
            tError.functionName = "hasMoreData";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors.addOneError(tError);
            try {
                mResultSet.close();
                mResultSet = null;
            }
            catch (Exception ex2) {}
            try {
                mStatement.close();
                mStatement = null;
            }
            catch (Exception ex3) {}
            if (!mflag) {
                try {
                    con.close();
                }
                catch (Exception et) {}
            }
            return false;
        }
        return flag;
    }
    /**
     * 获取定量数据
     * @return VMS_OUTPUT_TRANSSet
     */
    public VMS_OUTPUT_TRANSSet getData() {
        int tCount = 0;
        VMS_OUTPUT_TRANSSet tVMS_OUTPUT_TRANSSet = new VMS_OUTPUT_TRANSSet();
        VMS_OUTPUT_TRANSSchema tVMS_OUTPUT_TRANSSchema = null;
        if (null == mResultSet) {
            CError tError = new CError();
            tError.moduleName = "VMS_OUTPUT_TRANSDB";
            tError.functionName = "getData";
            tError.errorMessage = "数据集为空，请先准备数据集！";
            this.mErrors.addOneError(tError);
            return null;
        }
        try {
            tCount = 1;
            tVMS_OUTPUT_TRANSSchema = new VMS_OUTPUT_TRANSSchema();
            tVMS_OUTPUT_TRANSSchema.setSchema(mResultSet, 1);
            tVMS_OUTPUT_TRANSSet.add(tVMS_OUTPUT_TRANSSchema);
            //注意mResultSet.next()的作用
            while (tCount++ < SysConst.FETCHCOUNT) {
                if (mResultSet.next()) {
                    tVMS_OUTPUT_TRANSSchema = new VMS_OUTPUT_TRANSSchema();
                    tVMS_OUTPUT_TRANSSchema.setSchema(mResultSet, 1);
                    tVMS_OUTPUT_TRANSSet.add(tVMS_OUTPUT_TRANSSchema);
                }
            }
        }
        catch (Exception ex) {
            CError tError = new CError();
            tError.moduleName = "VMS_OUTPUT_TRANSDB";
            tError.functionName = "getData";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors.addOneError(tError);
            try {
                mResultSet.close();
                mResultSet = null;
            }
            catch (Exception ex2) {}
            try {
                mStatement.close();
                mStatement = null;
            }
            catch (Exception ex3) {}
            if (!mflag) {
                try {
                    con.close();
                }
                catch (Exception et) {}
            }
            return null;
        }
        return tVMS_OUTPUT_TRANSSet;
    }
    /**
     * 关闭数据集
     * @return boolean
     */
    public boolean closeData() {
        boolean flag = true;
        try {
            if (null == mResultSet) {
                CError tError = new CError();
                tError.moduleName = "VMS_OUTPUT_TRANSDB";
                tError.functionName = "closeData";
                tError.errorMessage = "数据集已经关闭了！";
                this.mErrors.addOneError(tError);
                flag = false;
            } else {
                mResultSet.close();
                mResultSet = null;
            }
        }
        catch (Exception ex2) {
            CError tError = new CError();
            tError.moduleName = "VMS_OUTPUT_TRANSDB";
            tError.functionName = "closeData";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex2);
            this.mErrors.addOneError(tError);
            flag = false;
        }
        try {
            if (null == mStatement) {
                CError tError = new CError();
                tError.moduleName = "VMS_OUTPUT_TRANSDB";
                tError.functionName = "closeData";
                tError.errorMessage = "语句已经关闭了！";
                this.mErrors.addOneError(tError);
                flag = false;
            } else {
                mStatement.close();
                mStatement = null;
            }
        }
        catch (Exception ex3) {
            CError tError = new CError();
            tError.moduleName = "VMS_OUTPUT_TRANSDB";
            tError.functionName = "closeData";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex3);
            this.mErrors.addOneError(tError);
            flag = false;
        }
        return flag;
    }
}
