/**
 * Copyright (c) 2017 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import com.sinosoft.lis.db.LJSPayDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.*;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

/**
 * <p>ClassName: LJSPaySchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2017</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2017-10-21
 */
public class LJSPaySchema implements Schema, Cloneable {
    // @Field
    /** Id */
    private long SPayID;
    /** Shardingid */
    private String ShardingID;
    /** 通知书号码 */
    private String GetNoticeNo;
    /** 其它号码 */
    private String OtherNo;
    /** 其它号码类型 */
    private String OtherNoType;
    /** 投保人客户号码 */
    private String AppntNo;
    /** 总应收金额 */
    private double SumDuePayMoney;
    /** 交费日期 */
    private Date PayDate;
    /** 银行在途标志 */
    private String BankOnTheWayFlag;
    /** 银行转帐成功标记 */
    private String BankSuccFlag;
    /** 送银行次数 */
    private int SendBankCount;
    /** 复核人编码 */
    private String ApproveCode;
    /** 复核日期 */
    private Date ApproveDate;
    /** 流水号 */
    private String SerialNo;
    /** 操作员 */
    private String Operator;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 最后一次修改日期 */
    private Date ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime;
    /** 管理机构 */
    private String ManageCom;
    /** 代理机构 */
    private String AgentCom;
    /** 代理机构内部分类 */
    private String AgentType;
    /** 银行编码 */
    private String BankCode;
    /** 银行帐号 */
    private String BankAccNo;
    /** 险种编码 */
    private String RiskCode;
    /** 代理人编码 */
    private String AgentCode;
    /** 代理人组别 */
    private String AgentGroup;
    /** 帐户名 */
    private String AccName;
    /** 最早收费日期 */
    private Date StartPayDate;
    /** 续保收费标记 */
    private String PayTypeFlag;
    /** 定期结算标志 */
    private String BalanceOnTime;
    /** 缴费方式 */
    private String PayMode;
    /** 投保人姓名 */
    private String APPntName;
    /** 收费机构 */
    private String ChargeCom;

    public static final int FIELDNUM = 34;    // 数据库表的字段个数

    private static String[] PK;                // 主键

    private FDate fDate = new FDate();        // 处理日期

    public CErrors mErrors;            // 错误信息

    // @Constructor
    public LJSPaySchema() {
        mErrors = new CErrors();

        String[] pk = new String[1];
        pk[0] = "SPayID";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException {
        LJSPaySchema cloned = (LJSPaySchema)super.clone();
        cloned.fDate = (FDate) fDate.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK() {
        return PK;
    }

    public long getSPayID() {
        return SPayID;
    }
    public void setSPayID(long aSPayID) {
        SPayID = aSPayID;
    }
    public void setSPayID(String aSPayID) {
        if (aSPayID != null && !aSPayID.equals("")) {
            SPayID = new Long(aSPayID).longValue();
        }
    }

    public String getShardingID() {
        return ShardingID;
    }
    public void setShardingID(String aShardingID) {
        ShardingID = aShardingID;
    }
    public String getGetNoticeNo() {
        return GetNoticeNo;
    }
    public void setGetNoticeNo(String aGetNoticeNo) {
        GetNoticeNo = aGetNoticeNo;
    }
    public String getOtherNo() {
        return OtherNo;
    }
    public void setOtherNo(String aOtherNo) {
        OtherNo = aOtherNo;
    }
    public String getOtherNoType() {
        return OtherNoType;
    }
    public void setOtherNoType(String aOtherNoType) {
        OtherNoType = aOtherNoType;
    }
    public String getAppntNo() {
        return AppntNo;
    }
    public void setAppntNo(String aAppntNo) {
        AppntNo = aAppntNo;
    }
    public double getSumDuePayMoney() {
        return SumDuePayMoney;
    }
    public void setSumDuePayMoney(double aSumDuePayMoney) {
        SumDuePayMoney = aSumDuePayMoney;
    }
    public void setSumDuePayMoney(String aSumDuePayMoney) {
        if (aSumDuePayMoney != null && !aSumDuePayMoney.equals("")) {
            Double tDouble = new Double(aSumDuePayMoney);
            double d = tDouble.doubleValue();
            SumDuePayMoney = d;
        }
    }

    public String getPayDate() {
        if(PayDate != null) {
            return fDate.getString(PayDate);
        } else {
            return null;
        }
    }
    public void setPayDate(Date aPayDate) {
        PayDate = aPayDate;
    }
    public void setPayDate(String aPayDate) {
        if (aPayDate != null && !aPayDate.equals("")) {
            PayDate = fDate.getDate(aPayDate);
        } else
            PayDate = null;
    }

    public String getBankOnTheWayFlag() {
        return BankOnTheWayFlag;
    }
    public void setBankOnTheWayFlag(String aBankOnTheWayFlag) {
        BankOnTheWayFlag = aBankOnTheWayFlag;
    }
    public String getBankSuccFlag() {
        return BankSuccFlag;
    }
    public void setBankSuccFlag(String aBankSuccFlag) {
        BankSuccFlag = aBankSuccFlag;
    }
    public int getSendBankCount() {
        return SendBankCount;
    }
    public void setSendBankCount(int aSendBankCount) {
        SendBankCount = aSendBankCount;
    }
    public void setSendBankCount(String aSendBankCount) {
        if (aSendBankCount != null && !aSendBankCount.equals("")) {
            Integer tInteger = new Integer(aSendBankCount);
            int i = tInteger.intValue();
            SendBankCount = i;
        }
    }

    public String getApproveCode() {
        return ApproveCode;
    }
    public void setApproveCode(String aApproveCode) {
        ApproveCode = aApproveCode;
    }
    public String getApproveDate() {
        if(ApproveDate != null) {
            return fDate.getString(ApproveDate);
        } else {
            return null;
        }
    }
    public void setApproveDate(Date aApproveDate) {
        ApproveDate = aApproveDate;
    }
    public void setApproveDate(String aApproveDate) {
        if (aApproveDate != null && !aApproveDate.equals("")) {
            ApproveDate = fDate.getDate(aApproveDate);
        } else
            ApproveDate = null;
    }

    public String getSerialNo() {
        return SerialNo;
    }
    public void setSerialNo(String aSerialNo) {
        SerialNo = aSerialNo;
    }
    public String getOperator() {
        return Operator;
    }
    public void setOperator(String aOperator) {
        Operator = aOperator;
    }
    public String getMakeDate() {
        if(MakeDate != null) {
            return fDate.getString(MakeDate);
        } else {
            return null;
        }
    }
    public void setMakeDate(Date aMakeDate) {
        MakeDate = aMakeDate;
    }
    public void setMakeDate(String aMakeDate) {
        if (aMakeDate != null && !aMakeDate.equals("")) {
            MakeDate = fDate.getDate(aMakeDate);
        } else
            MakeDate = null;
    }

    public String getMakeTime() {
        return MakeTime;
    }
    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }
    public String getModifyDate() {
        if(ModifyDate != null) {
            return fDate.getString(ModifyDate);
        } else {
            return null;
        }
    }
    public void setModifyDate(Date aModifyDate) {
        ModifyDate = aModifyDate;
    }
    public void setModifyDate(String aModifyDate) {
        if (aModifyDate != null && !aModifyDate.equals("")) {
            ModifyDate = fDate.getDate(aModifyDate);
        } else
            ModifyDate = null;
    }

    public String getModifyTime() {
        return ModifyTime;
    }
    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }
    public String getManageCom() {
        return ManageCom;
    }
    public void setManageCom(String aManageCom) {
        ManageCom = aManageCom;
    }
    public String getAgentCom() {
        return AgentCom;
    }
    public void setAgentCom(String aAgentCom) {
        AgentCom = aAgentCom;
    }
    public String getAgentType() {
        return AgentType;
    }
    public void setAgentType(String aAgentType) {
        AgentType = aAgentType;
    }
    public String getBankCode() {
        return BankCode;
    }
    public void setBankCode(String aBankCode) {
        BankCode = aBankCode;
    }
    public String getBankAccNo() {
        return BankAccNo;
    }
    public void setBankAccNo(String aBankAccNo) {
        BankAccNo = aBankAccNo;
    }
    public String getRiskCode() {
        return RiskCode;
    }
    public void setRiskCode(String aRiskCode) {
        RiskCode = aRiskCode;
    }
    public String getAgentCode() {
        return AgentCode;
    }
    public void setAgentCode(String aAgentCode) {
        AgentCode = aAgentCode;
    }
    public String getAgentGroup() {
        return AgentGroup;
    }
    public void setAgentGroup(String aAgentGroup) {
        AgentGroup = aAgentGroup;
    }
    public String getAccName() {
        return AccName;
    }
    public void setAccName(String aAccName) {
        AccName = aAccName;
    }
    public String getStartPayDate() {
        if(StartPayDate != null) {
            return fDate.getString(StartPayDate);
        } else {
            return null;
        }
    }
    public void setStartPayDate(Date aStartPayDate) {
        StartPayDate = aStartPayDate;
    }
    public void setStartPayDate(String aStartPayDate) {
        if (aStartPayDate != null && !aStartPayDate.equals("")) {
            StartPayDate = fDate.getDate(aStartPayDate);
        } else
            StartPayDate = null;
    }

    public String getPayTypeFlag() {
        return PayTypeFlag;
    }
    public void setPayTypeFlag(String aPayTypeFlag) {
        PayTypeFlag = aPayTypeFlag;
    }
    public String getBalanceOnTime() {
        return BalanceOnTime;
    }
    public void setBalanceOnTime(String aBalanceOnTime) {
        BalanceOnTime = aBalanceOnTime;
    }
    public String getPayMode() {
        return PayMode;
    }
    public void setPayMode(String aPayMode) {
        PayMode = aPayMode;
    }
    public String getAPPntName() {
        return APPntName;
    }
    public void setAPPntName(String aAPPntName) {
        APPntName = aAPPntName;
    }
    public String getChargeCom() {
        return ChargeCom;
    }
    public void setChargeCom(String aChargeCom) {
        ChargeCom = aChargeCom;
    }

    /**
    * 使用另外一个 LJSPaySchema 对象给 Schema 赋值
    * @param: aLJSPaySchema LJSPaySchema
    **/
    public void setSchema(LJSPaySchema aLJSPaySchema) {
        this.SPayID = aLJSPaySchema.getSPayID();
        this.ShardingID = aLJSPaySchema.getShardingID();
        this.GetNoticeNo = aLJSPaySchema.getGetNoticeNo();
        this.OtherNo = aLJSPaySchema.getOtherNo();
        this.OtherNoType = aLJSPaySchema.getOtherNoType();
        this.AppntNo = aLJSPaySchema.getAppntNo();
        this.SumDuePayMoney = aLJSPaySchema.getSumDuePayMoney();
        this.PayDate = fDate.getDate( aLJSPaySchema.getPayDate());
        this.BankOnTheWayFlag = aLJSPaySchema.getBankOnTheWayFlag();
        this.BankSuccFlag = aLJSPaySchema.getBankSuccFlag();
        this.SendBankCount = aLJSPaySchema.getSendBankCount();
        this.ApproveCode = aLJSPaySchema.getApproveCode();
        this.ApproveDate = fDate.getDate( aLJSPaySchema.getApproveDate());
        this.SerialNo = aLJSPaySchema.getSerialNo();
        this.Operator = aLJSPaySchema.getOperator();
        this.MakeDate = fDate.getDate( aLJSPaySchema.getMakeDate());
        this.MakeTime = aLJSPaySchema.getMakeTime();
        this.ModifyDate = fDate.getDate( aLJSPaySchema.getModifyDate());
        this.ModifyTime = aLJSPaySchema.getModifyTime();
        this.ManageCom = aLJSPaySchema.getManageCom();
        this.AgentCom = aLJSPaySchema.getAgentCom();
        this.AgentType = aLJSPaySchema.getAgentType();
        this.BankCode = aLJSPaySchema.getBankCode();
        this.BankAccNo = aLJSPaySchema.getBankAccNo();
        this.RiskCode = aLJSPaySchema.getRiskCode();
        this.AgentCode = aLJSPaySchema.getAgentCode();
        this.AgentGroup = aLJSPaySchema.getAgentGroup();
        this.AccName = aLJSPaySchema.getAccName();
        this.StartPayDate = fDate.getDate( aLJSPaySchema.getStartPayDate());
        this.PayTypeFlag = aLJSPaySchema.getPayTypeFlag();
        this.BalanceOnTime = aLJSPaySchema.getBalanceOnTime();
        this.PayMode = aLJSPaySchema.getPayMode();
        this.APPntName = aLJSPaySchema.getAPPntName();
        this.ChargeCom = aLJSPaySchema.getChargeCom();
    }

    /**
    * 使用 ResultSet 中的第 i 行给 Schema 赋值
    * @param: rs ResultSet
    * @param: i int
    * @return: boolean
    **/
    public boolean setSchema(ResultSet rs,int i) {
        try {
            //rs.absolute(i);        // 非滚动游标
            this.SPayID = rs.getLong("SPayID");
            if( rs.getString("ShardingID") == null )
                this.ShardingID = null;
            else
                this.ShardingID = rs.getString("ShardingID").trim();

            if( rs.getString("GetNoticeNo") == null )
                this.GetNoticeNo = null;
            else
                this.GetNoticeNo = rs.getString("GetNoticeNo").trim();

            if( rs.getString("OtherNo") == null )
                this.OtherNo = null;
            else
                this.OtherNo = rs.getString("OtherNo").trim();

            if( rs.getString("OtherNoType") == null )
                this.OtherNoType = null;
            else
                this.OtherNoType = rs.getString("OtherNoType").trim();

            if( rs.getString("AppntNo") == null )
                this.AppntNo = null;
            else
                this.AppntNo = rs.getString("AppntNo").trim();

            this.SumDuePayMoney = rs.getDouble("SumDuePayMoney");
            this.PayDate = rs.getDate("PayDate");
            if( rs.getString("BankOnTheWayFlag") == null )
                this.BankOnTheWayFlag = null;
            else
                this.BankOnTheWayFlag = rs.getString("BankOnTheWayFlag").trim();

            if( rs.getString("BankSuccFlag") == null )
                this.BankSuccFlag = null;
            else
                this.BankSuccFlag = rs.getString("BankSuccFlag").trim();

            this.SendBankCount = rs.getInt("SendBankCount");
            if( rs.getString("ApproveCode") == null )
                this.ApproveCode = null;
            else
                this.ApproveCode = rs.getString("ApproveCode").trim();

            this.ApproveDate = rs.getDate("ApproveDate");
            if( rs.getString("SerialNo") == null )
                this.SerialNo = null;
            else
                this.SerialNo = rs.getString("SerialNo").trim();

            if( rs.getString("Operator") == null )
                this.Operator = null;
            else
                this.Operator = rs.getString("Operator").trim();

            this.MakeDate = rs.getDate("MakeDate");
            if( rs.getString("MakeTime") == null )
                this.MakeTime = null;
            else
                this.MakeTime = rs.getString("MakeTime").trim();

            this.ModifyDate = rs.getDate("ModifyDate");
            if( rs.getString("ModifyTime") == null )
                this.ModifyTime = null;
            else
                this.ModifyTime = rs.getString("ModifyTime").trim();

            if( rs.getString("ManageCom") == null )
                this.ManageCom = null;
            else
                this.ManageCom = rs.getString("ManageCom").trim();

            if( rs.getString("AgentCom") == null )
                this.AgentCom = null;
            else
                this.AgentCom = rs.getString("AgentCom").trim();

            if( rs.getString("AgentType") == null )
                this.AgentType = null;
            else
                this.AgentType = rs.getString("AgentType").trim();

            if( rs.getString("BankCode") == null )
                this.BankCode = null;
            else
                this.BankCode = rs.getString("BankCode").trim();

            if( rs.getString("BankAccNo") == null )
                this.BankAccNo = null;
            else
                this.BankAccNo = rs.getString("BankAccNo").trim();

            if( rs.getString("RiskCode") == null )
                this.RiskCode = null;
            else
                this.RiskCode = rs.getString("RiskCode").trim();

            if( rs.getString("AgentCode") == null )
                this.AgentCode = null;
            else
                this.AgentCode = rs.getString("AgentCode").trim();

            if( rs.getString("AgentGroup") == null )
                this.AgentGroup = null;
            else
                this.AgentGroup = rs.getString("AgentGroup").trim();

            if( rs.getString("AccName") == null )
                this.AccName = null;
            else
                this.AccName = rs.getString("AccName").trim();

            this.StartPayDate = rs.getDate("StartPayDate");
            if( rs.getString("PayTypeFlag") == null )
                this.PayTypeFlag = null;
            else
                this.PayTypeFlag = rs.getString("PayTypeFlag").trim();

            if( rs.getString("BalanceOnTime") == null )
                this.BalanceOnTime = null;
            else
                this.BalanceOnTime = rs.getString("BalanceOnTime").trim();

            if( rs.getString("PayMode") == null )
                this.PayMode = null;
            else
                this.PayMode = rs.getString("PayMode").trim();

            if( rs.getString("APPntName") == null )
                this.APPntName = null;
            else
                this.APPntName = rs.getString("APPntName").trim();

            if( rs.getString("ChargeCom") == null )
                this.ChargeCom = null;
            else
                this.ChargeCom = rs.getString("ChargeCom").trim();

        }
        catch(SQLException sqle) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LJSPaySchema";
            tError.functionName = "setSchema";
            tError.errorMessage = ExceptionUtils.exceptionToString(sqle);
            this.mErrors .addOneError(tError);
            return false;
        }
        return true;
    }

    public LJSPaySchema getSchema() {
        LJSPaySchema aLJSPaySchema = new LJSPaySchema();
        aLJSPaySchema.setSchema(this);
        return aLJSPaySchema;
    }

    public LJSPayDB getDB() {
        LJSPayDB aDBOper = new LJSPayDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
    * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLJSPay描述/A>表字段
    * @return: String 返回打包后字符串
    **/
    public String encode() {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(ChgData.chgData(SPayID));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ShardingID)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(GetNoticeNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(OtherNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(OtherNoType)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AppntNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(SumDuePayMoney));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( PayDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(BankOnTheWayFlag)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(BankSuccFlag)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(SendBankCount));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ApproveCode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( ApproveDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(SerialNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ModifyTime)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ManageCom)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AgentCom)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AgentType)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(BankCode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(BankAccNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(RiskCode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AgentCode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AgentGroup)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AccName)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( StartPayDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(PayTypeFlag)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(BalanceOnTime)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(PayMode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(APPntName)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ChargeCom));
        return strReturn.toString();
    }

    /**
    * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLJSPay>历史记账凭证主表信息</A>表字段
    * @param: strMessage String 包含一条纪录数据的字符串
    * @return: boolean
    **/
    public boolean decode(String strMessage) {
        try {
            SPayID = new Long(ChgData.chgNumericStr(StrTool.getStr(strMessage,1, SysConst.PACKAGESPILTER))).longValue();
            ShardingID = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
            GetNoticeNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
            OtherNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
            OtherNoType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
            AppntNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
            SumDuePayMoney = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,7, SysConst.PACKAGESPILTER))).doubleValue();
            PayDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER));
            BankOnTheWayFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
            BankSuccFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
            SendBankCount = new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,11, SysConst.PACKAGESPILTER))).intValue();
            ApproveCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
            ApproveDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER));
            SerialNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER );
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15, SysConst.PACKAGESPILTER );
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17, SysConst.PACKAGESPILTER );
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19, SysConst.PACKAGESPILTER );
            ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20, SysConst.PACKAGESPILTER );
            AgentCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 21, SysConst.PACKAGESPILTER );
            AgentType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 22, SysConst.PACKAGESPILTER );
            BankCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 23, SysConst.PACKAGESPILTER );
            BankAccNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 24, SysConst.PACKAGESPILTER );
            RiskCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 25, SysConst.PACKAGESPILTER );
            AgentCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 26, SysConst.PACKAGESPILTER );
            AgentGroup = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 27, SysConst.PACKAGESPILTER );
            AccName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 28, SysConst.PACKAGESPILTER );
            StartPayDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 29, SysConst.PACKAGESPILTER));
            PayTypeFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 30, SysConst.PACKAGESPILTER );
            BalanceOnTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 31, SysConst.PACKAGESPILTER );
            PayMode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 32, SysConst.PACKAGESPILTER );
            APPntName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 33, SysConst.PACKAGESPILTER );
            ChargeCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 34, SysConst.PACKAGESPILTER );
        }
        catch(NumberFormatException ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LJSPaySchema";
            tError.functionName = "decode";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            return false;
        }
        return true;
    }

    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("SPayID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SPayID));
        }
        if (FCode.equalsIgnoreCase("ShardingID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ShardingID));
        }
        if (FCode.equalsIgnoreCase("GetNoticeNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GetNoticeNo));
        }
        if (FCode.equalsIgnoreCase("OtherNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OtherNo));
        }
        if (FCode.equalsIgnoreCase("OtherNoType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OtherNoType));
        }
        if (FCode.equalsIgnoreCase("AppntNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AppntNo));
        }
        if (FCode.equalsIgnoreCase("SumDuePayMoney")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SumDuePayMoney));
        }
        if (FCode.equalsIgnoreCase("PayDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getPayDate()));
        }
        if (FCode.equalsIgnoreCase("BankOnTheWayFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BankOnTheWayFlag));
        }
        if (FCode.equalsIgnoreCase("BankSuccFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BankSuccFlag));
        }
        if (FCode.equalsIgnoreCase("SendBankCount")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SendBankCount));
        }
        if (FCode.equalsIgnoreCase("ApproveCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ApproveCode));
        }
        if (FCode.equalsIgnoreCase("ApproveDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getApproveDate()));
        }
        if (FCode.equalsIgnoreCase("SerialNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SerialNo));
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (FCode.equalsIgnoreCase("ManageCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
        }
        if (FCode.equalsIgnoreCase("AgentCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentCom));
        }
        if (FCode.equalsIgnoreCase("AgentType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentType));
        }
        if (FCode.equalsIgnoreCase("BankCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BankCode));
        }
        if (FCode.equalsIgnoreCase("BankAccNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BankAccNo));
        }
        if (FCode.equalsIgnoreCase("RiskCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RiskCode));
        }
        if (FCode.equalsIgnoreCase("AgentCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentCode));
        }
        if (FCode.equalsIgnoreCase("AgentGroup")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentGroup));
        }
        if (FCode.equalsIgnoreCase("AccName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AccName));
        }
        if (FCode.equalsIgnoreCase("StartPayDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getStartPayDate()));
        }
        if (FCode.equalsIgnoreCase("PayTypeFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PayTypeFlag));
        }
        if (FCode.equalsIgnoreCase("BalanceOnTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BalanceOnTime));
        }
        if (FCode.equalsIgnoreCase("PayMode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PayMode));
        }
        if (FCode.equalsIgnoreCase("APPntName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(APPntName));
        }
        if (FCode.equalsIgnoreCase("ChargeCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ChargeCom));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = String.valueOf(SPayID);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(ShardingID);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(GetNoticeNo);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(OtherNo);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(OtherNoType);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(AppntNo);
                break;
            case 6:
                strFieldValue = String.valueOf(SumDuePayMoney);
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getPayDate()));
                break;
            case 8:
                strFieldValue = StrTool.GBKToUnicode(BankOnTheWayFlag);
                break;
            case 9:
                strFieldValue = StrTool.GBKToUnicode(BankSuccFlag);
                break;
            case 10:
                strFieldValue = String.valueOf(SendBankCount);
                break;
            case 11:
                strFieldValue = StrTool.GBKToUnicode(ApproveCode);
                break;
            case 12:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getApproveDate()));
                break;
            case 13:
                strFieldValue = StrTool.GBKToUnicode(SerialNo);
                break;
            case 14:
                strFieldValue = StrTool.GBKToUnicode(Operator);
                break;
            case 15:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
                break;
            case 16:
                strFieldValue = StrTool.GBKToUnicode(MakeTime);
                break;
            case 17:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
                break;
            case 18:
                strFieldValue = StrTool.GBKToUnicode(ModifyTime);
                break;
            case 19:
                strFieldValue = StrTool.GBKToUnicode(ManageCom);
                break;
            case 20:
                strFieldValue = StrTool.GBKToUnicode(AgentCom);
                break;
            case 21:
                strFieldValue = StrTool.GBKToUnicode(AgentType);
                break;
            case 22:
                strFieldValue = StrTool.GBKToUnicode(BankCode);
                break;
            case 23:
                strFieldValue = StrTool.GBKToUnicode(BankAccNo);
                break;
            case 24:
                strFieldValue = StrTool.GBKToUnicode(RiskCode);
                break;
            case 25:
                strFieldValue = StrTool.GBKToUnicode(AgentCode);
                break;
            case 26:
                strFieldValue = StrTool.GBKToUnicode(AgentGroup);
                break;
            case 27:
                strFieldValue = StrTool.GBKToUnicode(AccName);
                break;
            case 28:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getStartPayDate()));
                break;
            case 29:
                strFieldValue = StrTool.GBKToUnicode(PayTypeFlag);
                break;
            case 30:
                strFieldValue = StrTool.GBKToUnicode(BalanceOnTime);
                break;
            case 31:
                strFieldValue = StrTool.GBKToUnicode(PayMode);
                break;
            case 32:
                strFieldValue = StrTool.GBKToUnicode(APPntName);
                break;
            case 33:
                strFieldValue = StrTool.GBKToUnicode(ChargeCom);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("SPayID")) {
            if( FValue != null && !FValue.equals("")) {
                SPayID = new Long(FValue).longValue();
            }
        }
        if (FCode.equalsIgnoreCase("ShardingID")) {
            if( FValue != null && !FValue.equals(""))
            {
                ShardingID = FValue.trim();
            }
            else
                ShardingID = null;
        }
        if (FCode.equalsIgnoreCase("GetNoticeNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                GetNoticeNo = FValue.trim();
            }
            else
                GetNoticeNo = null;
        }
        if (FCode.equalsIgnoreCase("OtherNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                OtherNo = FValue.trim();
            }
            else
                OtherNo = null;
        }
        if (FCode.equalsIgnoreCase("OtherNoType")) {
            if( FValue != null && !FValue.equals(""))
            {
                OtherNoType = FValue.trim();
            }
            else
                OtherNoType = null;
        }
        if (FCode.equalsIgnoreCase("AppntNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                AppntNo = FValue.trim();
            }
            else
                AppntNo = null;
        }
        if (FCode.equalsIgnoreCase("SumDuePayMoney")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                SumDuePayMoney = d;
            }
        }
        if (FCode.equalsIgnoreCase("PayDate")) {
            if(FValue != null && !FValue.equals("")) {
                PayDate = fDate.getDate( FValue );
            }
            else
                PayDate = null;
        }
        if (FCode.equalsIgnoreCase("BankOnTheWayFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                BankOnTheWayFlag = FValue.trim();
            }
            else
                BankOnTheWayFlag = null;
        }
        if (FCode.equalsIgnoreCase("BankSuccFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                BankSuccFlag = FValue.trim();
            }
            else
                BankSuccFlag = null;
        }
        if (FCode.equalsIgnoreCase("SendBankCount")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                SendBankCount = i;
            }
        }
        if (FCode.equalsIgnoreCase("ApproveCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                ApproveCode = FValue.trim();
            }
            else
                ApproveCode = null;
        }
        if (FCode.equalsIgnoreCase("ApproveDate")) {
            if(FValue != null && !FValue.equals("")) {
                ApproveDate = fDate.getDate( FValue );
            }
            else
                ApproveDate = null;
        }
        if (FCode.equalsIgnoreCase("SerialNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                SerialNo = FValue.trim();
            }
            else
                SerialNo = null;
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            if( FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
                Operator = null;
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if(FValue != null && !FValue.equals("")) {
                MakeDate = fDate.getDate( FValue );
            }
            else
                MakeDate = null;
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
                MakeTime = null;
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if(FValue != null && !FValue.equals("")) {
                ModifyDate = fDate.getDate( FValue );
            }
            else
                ModifyDate = null;
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
                ModifyTime = null;
        }
        if (FCode.equalsIgnoreCase("ManageCom")) {
            if( FValue != null && !FValue.equals(""))
            {
                ManageCom = FValue.trim();
            }
            else
                ManageCom = null;
        }
        if (FCode.equalsIgnoreCase("AgentCom")) {
            if( FValue != null && !FValue.equals(""))
            {
                AgentCom = FValue.trim();
            }
            else
                AgentCom = null;
        }
        if (FCode.equalsIgnoreCase("AgentType")) {
            if( FValue != null && !FValue.equals(""))
            {
                AgentType = FValue.trim();
            }
            else
                AgentType = null;
        }
        if (FCode.equalsIgnoreCase("BankCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                BankCode = FValue.trim();
            }
            else
                BankCode = null;
        }
        if (FCode.equalsIgnoreCase("BankAccNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                BankAccNo = FValue.trim();
            }
            else
                BankAccNo = null;
        }
        if (FCode.equalsIgnoreCase("RiskCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                RiskCode = FValue.trim();
            }
            else
                RiskCode = null;
        }
        if (FCode.equalsIgnoreCase("AgentCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                AgentCode = FValue.trim();
            }
            else
                AgentCode = null;
        }
        if (FCode.equalsIgnoreCase("AgentGroup")) {
            if( FValue != null && !FValue.equals(""))
            {
                AgentGroup = FValue.trim();
            }
            else
                AgentGroup = null;
        }
        if (FCode.equalsIgnoreCase("AccName")) {
            if( FValue != null && !FValue.equals(""))
            {
                AccName = FValue.trim();
            }
            else
                AccName = null;
        }
        if (FCode.equalsIgnoreCase("StartPayDate")) {
            if(FValue != null && !FValue.equals("")) {
                StartPayDate = fDate.getDate( FValue );
            }
            else
                StartPayDate = null;
        }
        if (FCode.equalsIgnoreCase("PayTypeFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                PayTypeFlag = FValue.trim();
            }
            else
                PayTypeFlag = null;
        }
        if (FCode.equalsIgnoreCase("BalanceOnTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                BalanceOnTime = FValue.trim();
            }
            else
                BalanceOnTime = null;
        }
        if (FCode.equalsIgnoreCase("PayMode")) {
            if( FValue != null && !FValue.equals(""))
            {
                PayMode = FValue.trim();
            }
            else
                PayMode = null;
        }
        if (FCode.equalsIgnoreCase("APPntName")) {
            if( FValue != null && !FValue.equals(""))
            {
                APPntName = FValue.trim();
            }
            else
                APPntName = null;
        }
        if (FCode.equalsIgnoreCase("ChargeCom")) {
            if( FValue != null && !FValue.equals(""))
            {
                ChargeCom = FValue.trim();
            }
            else
                ChargeCom = null;
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject) return true;
        if (otherObject == null) return false;
        if (getClass() != otherObject.getClass()) return false;
        LJSPaySchema other = (LJSPaySchema)otherObject;
        return
            SPayID == other.getSPayID()
            && ShardingID.equals(other.getShardingID())
            && GetNoticeNo.equals(other.getGetNoticeNo())
            && OtherNo.equals(other.getOtherNo())
            && OtherNoType.equals(other.getOtherNoType())
            && AppntNo.equals(other.getAppntNo())
            && SumDuePayMoney == other.getSumDuePayMoney()
            && fDate.getString(PayDate).equals(other.getPayDate())
            && BankOnTheWayFlag.equals(other.getBankOnTheWayFlag())
            && BankSuccFlag.equals(other.getBankSuccFlag())
            && SendBankCount == other.getSendBankCount()
            && ApproveCode.equals(other.getApproveCode())
            && fDate.getString(ApproveDate).equals(other.getApproveDate())
            && SerialNo.equals(other.getSerialNo())
            && Operator.equals(other.getOperator())
            && fDate.getString(MakeDate).equals(other.getMakeDate())
            && MakeTime.equals(other.getMakeTime())
            && fDate.getString(ModifyDate).equals(other.getModifyDate())
            && ModifyTime.equals(other.getModifyTime())
            && ManageCom.equals(other.getManageCom())
            && AgentCom.equals(other.getAgentCom())
            && AgentType.equals(other.getAgentType())
            && BankCode.equals(other.getBankCode())
            && BankAccNo.equals(other.getBankAccNo())
            && RiskCode.equals(other.getRiskCode())
            && AgentCode.equals(other.getAgentCode())
            && AgentGroup.equals(other.getAgentGroup())
            && AccName.equals(other.getAccName())
            && fDate.getString(StartPayDate).equals(other.getStartPayDate())
            && PayTypeFlag.equals(other.getPayTypeFlag())
            && BalanceOnTime.equals(other.getBalanceOnTime())
            && PayMode.equals(other.getPayMode())
            && APPntName.equals(other.getAPPntName())
            && ChargeCom.equals(other.getChargeCom());
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("SPayID") ) {
            return 0;
        }
        if( strFieldName.equals("ShardingID") ) {
            return 1;
        }
        if( strFieldName.equals("GetNoticeNo") ) {
            return 2;
        }
        if( strFieldName.equals("OtherNo") ) {
            return 3;
        }
        if( strFieldName.equals("OtherNoType") ) {
            return 4;
        }
        if( strFieldName.equals("AppntNo") ) {
            return 5;
        }
        if( strFieldName.equals("SumDuePayMoney") ) {
            return 6;
        }
        if( strFieldName.equals("PayDate") ) {
            return 7;
        }
        if( strFieldName.equals("BankOnTheWayFlag") ) {
            return 8;
        }
        if( strFieldName.equals("BankSuccFlag") ) {
            return 9;
        }
        if( strFieldName.equals("SendBankCount") ) {
            return 10;
        }
        if( strFieldName.equals("ApproveCode") ) {
            return 11;
        }
        if( strFieldName.equals("ApproveDate") ) {
            return 12;
        }
        if( strFieldName.equals("SerialNo") ) {
            return 13;
        }
        if( strFieldName.equals("Operator") ) {
            return 14;
        }
        if( strFieldName.equals("MakeDate") ) {
            return 15;
        }
        if( strFieldName.equals("MakeTime") ) {
            return 16;
        }
        if( strFieldName.equals("ModifyDate") ) {
            return 17;
        }
        if( strFieldName.equals("ModifyTime") ) {
            return 18;
        }
        if( strFieldName.equals("ManageCom") ) {
            return 19;
        }
        if( strFieldName.equals("AgentCom") ) {
            return 20;
        }
        if( strFieldName.equals("AgentType") ) {
            return 21;
        }
        if( strFieldName.equals("BankCode") ) {
            return 22;
        }
        if( strFieldName.equals("BankAccNo") ) {
            return 23;
        }
        if( strFieldName.equals("RiskCode") ) {
            return 24;
        }
        if( strFieldName.equals("AgentCode") ) {
            return 25;
        }
        if( strFieldName.equals("AgentGroup") ) {
            return 26;
        }
        if( strFieldName.equals("AccName") ) {
            return 27;
        }
        if( strFieldName.equals("StartPayDate") ) {
            return 28;
        }
        if( strFieldName.equals("PayTypeFlag") ) {
            return 29;
        }
        if( strFieldName.equals("BalanceOnTime") ) {
            return 30;
        }
        if( strFieldName.equals("PayMode") ) {
            return 31;
        }
        if( strFieldName.equals("APPntName") ) {
            return 32;
        }
        if( strFieldName.equals("ChargeCom") ) {
            return 33;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "SPayID";
                break;
            case 1:
                strFieldName = "ShardingID";
                break;
            case 2:
                strFieldName = "GetNoticeNo";
                break;
            case 3:
                strFieldName = "OtherNo";
                break;
            case 4:
                strFieldName = "OtherNoType";
                break;
            case 5:
                strFieldName = "AppntNo";
                break;
            case 6:
                strFieldName = "SumDuePayMoney";
                break;
            case 7:
                strFieldName = "PayDate";
                break;
            case 8:
                strFieldName = "BankOnTheWayFlag";
                break;
            case 9:
                strFieldName = "BankSuccFlag";
                break;
            case 10:
                strFieldName = "SendBankCount";
                break;
            case 11:
                strFieldName = "ApproveCode";
                break;
            case 12:
                strFieldName = "ApproveDate";
                break;
            case 13:
                strFieldName = "SerialNo";
                break;
            case 14:
                strFieldName = "Operator";
                break;
            case 15:
                strFieldName = "MakeDate";
                break;
            case 16:
                strFieldName = "MakeTime";
                break;
            case 17:
                strFieldName = "ModifyDate";
                break;
            case 18:
                strFieldName = "ModifyTime";
                break;
            case 19:
                strFieldName = "ManageCom";
                break;
            case 20:
                strFieldName = "AgentCom";
                break;
            case 21:
                strFieldName = "AgentType";
                break;
            case 22:
                strFieldName = "BankCode";
                break;
            case 23:
                strFieldName = "BankAccNo";
                break;
            case 24:
                strFieldName = "RiskCode";
                break;
            case 25:
                strFieldName = "AgentCode";
                break;
            case 26:
                strFieldName = "AgentGroup";
                break;
            case 27:
                strFieldName = "AccName";
                break;
            case 28:
                strFieldName = "StartPayDate";
                break;
            case 29:
                strFieldName = "PayTypeFlag";
                break;
            case 30:
                strFieldName = "BalanceOnTime";
                break;
            case 31:
                strFieldName = "PayMode";
                break;
            case 32:
                strFieldName = "APPntName";
                break;
            case 33:
                strFieldName = "ChargeCom";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "SPAYID":
                return Schema.TYPE_LONG;
            case "SHARDINGID":
                return Schema.TYPE_STRING;
            case "GETNOTICENO":
                return Schema.TYPE_STRING;
            case "OTHERNO":
                return Schema.TYPE_STRING;
            case "OTHERNOTYPE":
                return Schema.TYPE_STRING;
            case "APPNTNO":
                return Schema.TYPE_STRING;
            case "SUMDUEPAYMONEY":
                return Schema.TYPE_DOUBLE;
            case "PAYDATE":
                return Schema.TYPE_DATE;
            case "BANKONTHEWAYFLAG":
                return Schema.TYPE_STRING;
            case "BANKSUCCFLAG":
                return Schema.TYPE_STRING;
            case "SENDBANKCOUNT":
                return Schema.TYPE_INT;
            case "APPROVECODE":
                return Schema.TYPE_STRING;
            case "APPROVEDATE":
                return Schema.TYPE_DATE;
            case "SERIALNO":
                return Schema.TYPE_STRING;
            case "OPERATOR":
                return Schema.TYPE_STRING;
            case "MAKEDATE":
                return Schema.TYPE_DATE;
            case "MAKETIME":
                return Schema.TYPE_STRING;
            case "MODIFYDATE":
                return Schema.TYPE_DATE;
            case "MODIFYTIME":
                return Schema.TYPE_STRING;
            case "MANAGECOM":
                return Schema.TYPE_STRING;
            case "AGENTCOM":
                return Schema.TYPE_STRING;
            case "AGENTTYPE":
                return Schema.TYPE_STRING;
            case "BANKCODE":
                return Schema.TYPE_STRING;
            case "BANKACCNO":
                return Schema.TYPE_STRING;
            case "RISKCODE":
                return Schema.TYPE_STRING;
            case "AGENTCODE":
                return Schema.TYPE_STRING;
            case "AGENTGROUP":
                return Schema.TYPE_STRING;
            case "ACCNAME":
                return Schema.TYPE_STRING;
            case "STARTPAYDATE":
                return Schema.TYPE_DATE;
            case "PAYTYPEFLAG":
                return Schema.TYPE_STRING;
            case "BALANCEONTIME":
                return Schema.TYPE_STRING;
            case "PAYMODE":
                return Schema.TYPE_STRING;
            case "APPNTNAME":
                return Schema.TYPE_STRING;
            case "CHARGECOM":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_LONG;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_STRING;
            case 6:
                return Schema.TYPE_DOUBLE;
            case 7:
                return Schema.TYPE_DATE;
            case 8:
                return Schema.TYPE_STRING;
            case 9:
                return Schema.TYPE_STRING;
            case 10:
                return Schema.TYPE_INT;
            case 11:
                return Schema.TYPE_STRING;
            case 12:
                return Schema.TYPE_DATE;
            case 13:
                return Schema.TYPE_STRING;
            case 14:
                return Schema.TYPE_STRING;
            case 15:
                return Schema.TYPE_DATE;
            case 16:
                return Schema.TYPE_STRING;
            case 17:
                return Schema.TYPE_DATE;
            case 18:
                return Schema.TYPE_STRING;
            case 19:
                return Schema.TYPE_STRING;
            case 20:
                return Schema.TYPE_STRING;
            case 21:
                return Schema.TYPE_STRING;
            case 22:
                return Schema.TYPE_STRING;
            case 23:
                return Schema.TYPE_STRING;
            case 24:
                return Schema.TYPE_STRING;
            case 25:
                return Schema.TYPE_STRING;
            case 26:
                return Schema.TYPE_STRING;
            case 27:
                return Schema.TYPE_STRING;
            case 28:
                return Schema.TYPE_DATE;
            case 29:
                return Schema.TYPE_STRING;
            case 30:
                return Schema.TYPE_STRING;
            case 31:
                return Schema.TYPE_STRING;
            case 32:
                return Schema.TYPE_STRING;
            case 33:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
}
