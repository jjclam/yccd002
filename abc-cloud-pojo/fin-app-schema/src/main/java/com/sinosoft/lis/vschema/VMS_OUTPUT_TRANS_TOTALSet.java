/**
 * Copyright (c) 2017 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.vschema;

import com.sinosoft.lis.schema.VMS_OUTPUT_TRANS_TOTALSchema;
import com.sinosoft.utility.SchemaSet;
import com.sinosoft.utility.SysConst;

/**
 * <p>ClassName: VMS_OUTPUT_TRANS_TOTALSet </p>
 * <p>Description: VMS_OUTPUT_TRANS_TOTALSchemaSet类文件 </p>
 * <p>Copyright: Copyright (c) 2017</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2017-10-21
 */
public class VMS_OUTPUT_TRANS_TOTALSet extends SchemaSet {
	// @Method
	public boolean add(VMS_OUTPUT_TRANS_TOTALSchema aSchema) {
		return super.add(aSchema);
	}

	public boolean add(VMS_OUTPUT_TRANS_TOTALSet aSet) {
		return super.add(aSet);
	}

	public boolean remove(VMS_OUTPUT_TRANS_TOTALSchema aSchema) {
		return super.remove(aSchema);
	}

	public VMS_OUTPUT_TRANS_TOTALSchema get(int index) {
		VMS_OUTPUT_TRANS_TOTALSchema tSchema = (VMS_OUTPUT_TRANS_TOTALSchema)super.getObj(index);
		return tSchema;
	}

	public boolean set(int index, VMS_OUTPUT_TRANS_TOTALSchema aSchema) {
		return super.set(index,aSchema);
	}

	public boolean set(VMS_OUTPUT_TRANS_TOTALSet aSet) {
		return super.set(aSet);
	}

	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpVMS_OUTPUT_TRANS_TOTAL描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode() {
		StringBuffer strReturn = new StringBuffer("");
		int n = this.size();
		for (int i = 1; i <= n; i++) {
			VMS_OUTPUT_TRANS_TOTALSchema aSchema = this.get(i);
			strReturn.append(aSchema.encode());
			if( i != n ) strReturn.append(SysConst.RECORDSPLITER);
		}

		return strReturn.toString();
	}

	/**
	* 数据解包
	* @param: str String 打包后字符串
	* @return: boolean
	**/
	public boolean decode( String str ) {
		int nBeginPos = 0;
		int nEndPos = str.indexOf('^');
		this.clear();

		while( nEndPos != -1 ) {
			VMS_OUTPUT_TRANS_TOTALSchema aSchema = new VMS_OUTPUT_TRANS_TOTALSchema();
			if (aSchema.decode(str.substring(nBeginPos, nEndPos))) {
			    this.add(aSchema);
			    nBeginPos = nEndPos + 1;
			    nEndPos = str.indexOf('^', nEndPos + 1);
			} else {
				// @@错误处理
				this.mErrors.copyAllErrors( aSchema.mErrors );
				return false;
			}
		}
		VMS_OUTPUT_TRANS_TOTALSchema tSchema = new VMS_OUTPUT_TRANS_TOTALSchema();
		if(tSchema.decode(str.substring(nBeginPos))) {
		    this.add(tSchema);
		    return true;
		} else {
			// @@错误处理
			this.mErrors.copyAllErrors( tSchema.mErrors );
			return false;
		}
	}

}
