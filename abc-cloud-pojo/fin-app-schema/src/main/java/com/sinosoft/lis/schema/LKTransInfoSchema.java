/**
 * Copyright (c) 2017 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import com.sinosoft.lis.db.LKTransInfoDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.*;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

/**
 * <p>ClassName: LKTransInfoSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2017</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2017-10-21
 */
public class LKTransInfoSchema implements Schema, Cloneable {
    // @Field
    /** Id */
    private long TransInfoID;
    /** Shardingid */
    private String ShardingID;
    /** Sstranscode */
    private String SSTransCode;
    /** Suspendtranscode */
    private String SuSpendTransCode;
    /** Tbtranscode */
    private String TBTransCode;
    /** Newpolicyno */
    private String NewPolicyNo;
    /** Oldpolicyno */
    private String OldPolicyNo;
    /** Edoracceptno */
    private String EdorAcceptNo;
    /** Occurbala */
    private double OccurBala;
    /** Prem */
    private double Prem;
    /** Transferprem */
    private double TransferPrem;
    /** Funcflag */
    private String FuncFlag;
    /** Transdate */
    private Date TransDate;
    /** Transtime */
    private String TransTime;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 最后修改日期 */
    private Date ModifyDate;
    /** 最后修改时间 */
    private String ModifyTime;
    /** Bak1 */
    private String Bak1;
    /** Bak2 */
    private String Bak2;
    /** Bak3 */
    private String Bak3;
    /** Bak4 */
    private String Bak4;

    public static final int FIELDNUM = 22;    // 数据库表的字段个数

    private static String[] PK;                // 主键

    private FDate fDate = new FDate();        // 处理日期

    public CErrors mErrors;            // 错误信息

    // @Constructor
    public LKTransInfoSchema() {
        mErrors = new CErrors();

        String[] pk = new String[1];
        pk[0] = "TransInfoID";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException {
        LKTransInfoSchema cloned = (LKTransInfoSchema)super.clone();
        cloned.fDate = (FDate) fDate.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK() {
        return PK;
    }

    public long getTransInfoID() {
        return TransInfoID;
    }
    public void setTransInfoID(long aTransInfoID) {
        TransInfoID = aTransInfoID;
    }
    public void setTransInfoID(String aTransInfoID) {
        if (aTransInfoID != null && !aTransInfoID.equals("")) {
            TransInfoID = new Long(aTransInfoID).longValue();
        }
    }

    public String getShardingID() {
        return ShardingID;
    }
    public void setShardingID(String aShardingID) {
        ShardingID = aShardingID;
    }
    public String getSSTransCode() {
        return SSTransCode;
    }
    public void setSSTransCode(String aSSTransCode) {
        SSTransCode = aSSTransCode;
    }
    public String getSuSpendTransCode() {
        return SuSpendTransCode;
    }
    public void setSuSpendTransCode(String aSuSpendTransCode) {
        SuSpendTransCode = aSuSpendTransCode;
    }
    public String getTBTransCode() {
        return TBTransCode;
    }
    public void setTBTransCode(String aTBTransCode) {
        TBTransCode = aTBTransCode;
    }
    public String getNewPolicyNo() {
        return NewPolicyNo;
    }
    public void setNewPolicyNo(String aNewPolicyNo) {
        NewPolicyNo = aNewPolicyNo;
    }
    public String getOldPolicyNo() {
        return OldPolicyNo;
    }
    public void setOldPolicyNo(String aOldPolicyNo) {
        OldPolicyNo = aOldPolicyNo;
    }
    public String getEdorAcceptNo() {
        return EdorAcceptNo;
    }
    public void setEdorAcceptNo(String aEdorAcceptNo) {
        EdorAcceptNo = aEdorAcceptNo;
    }
    public double getOccurBala() {
        return OccurBala;
    }
    public void setOccurBala(double aOccurBala) {
        OccurBala = aOccurBala;
    }
    public void setOccurBala(String aOccurBala) {
        if (aOccurBala != null && !aOccurBala.equals("")) {
            Double tDouble = new Double(aOccurBala);
            double d = tDouble.doubleValue();
            OccurBala = d;
        }
    }

    public double getPrem() {
        return Prem;
    }
    public void setPrem(double aPrem) {
        Prem = aPrem;
    }
    public void setPrem(String aPrem) {
        if (aPrem != null && !aPrem.equals("")) {
            Double tDouble = new Double(aPrem);
            double d = tDouble.doubleValue();
            Prem = d;
        }
    }

    public double getTransferPrem() {
        return TransferPrem;
    }
    public void setTransferPrem(double aTransferPrem) {
        TransferPrem = aTransferPrem;
    }
    public void setTransferPrem(String aTransferPrem) {
        if (aTransferPrem != null && !aTransferPrem.equals("")) {
            Double tDouble = new Double(aTransferPrem);
            double d = tDouble.doubleValue();
            TransferPrem = d;
        }
    }

    public String getFuncFlag() {
        return FuncFlag;
    }
    public void setFuncFlag(String aFuncFlag) {
        FuncFlag = aFuncFlag;
    }
    public String getTransDate() {
        if(TransDate != null) {
            return fDate.getString(TransDate);
        } else {
            return null;
        }
    }
    public void setTransDate(Date aTransDate) {
        TransDate = aTransDate;
    }
    public void setTransDate(String aTransDate) {
        if (aTransDate != null && !aTransDate.equals("")) {
            TransDate = fDate.getDate(aTransDate);
        } else
            TransDate = null;
    }

    public String getTransTime() {
        return TransTime;
    }
    public void setTransTime(String aTransTime) {
        TransTime = aTransTime;
    }
    public String getMakeDate() {
        if(MakeDate != null) {
            return fDate.getString(MakeDate);
        } else {
            return null;
        }
    }
    public void setMakeDate(Date aMakeDate) {
        MakeDate = aMakeDate;
    }
    public void setMakeDate(String aMakeDate) {
        if (aMakeDate != null && !aMakeDate.equals("")) {
            MakeDate = fDate.getDate(aMakeDate);
        } else
            MakeDate = null;
    }

    public String getMakeTime() {
        return MakeTime;
    }
    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }
    public String getModifyDate() {
        if(ModifyDate != null) {
            return fDate.getString(ModifyDate);
        } else {
            return null;
        }
    }
    public void setModifyDate(Date aModifyDate) {
        ModifyDate = aModifyDate;
    }
    public void setModifyDate(String aModifyDate) {
        if (aModifyDate != null && !aModifyDate.equals("")) {
            ModifyDate = fDate.getDate(aModifyDate);
        } else
            ModifyDate = null;
    }

    public String getModifyTime() {
        return ModifyTime;
    }
    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }
    public String getBak1() {
        return Bak1;
    }
    public void setBak1(String aBak1) {
        Bak1 = aBak1;
    }
    public String getBak2() {
        return Bak2;
    }
    public void setBak2(String aBak2) {
        Bak2 = aBak2;
    }
    public String getBak3() {
        return Bak3;
    }
    public void setBak3(String aBak3) {
        Bak3 = aBak3;
    }
    public String getBak4() {
        return Bak4;
    }
    public void setBak4(String aBak4) {
        Bak4 = aBak4;
    }

    /**
    * 使用另外一个 LKTransInfoSchema 对象给 Schema 赋值
    * @param: aLKTransInfoSchema LKTransInfoSchema
    **/
    public void setSchema(LKTransInfoSchema aLKTransInfoSchema) {
        this.TransInfoID = aLKTransInfoSchema.getTransInfoID();
        this.ShardingID = aLKTransInfoSchema.getShardingID();
        this.SSTransCode = aLKTransInfoSchema.getSSTransCode();
        this.SuSpendTransCode = aLKTransInfoSchema.getSuSpendTransCode();
        this.TBTransCode = aLKTransInfoSchema.getTBTransCode();
        this.NewPolicyNo = aLKTransInfoSchema.getNewPolicyNo();
        this.OldPolicyNo = aLKTransInfoSchema.getOldPolicyNo();
        this.EdorAcceptNo = aLKTransInfoSchema.getEdorAcceptNo();
        this.OccurBala = aLKTransInfoSchema.getOccurBala();
        this.Prem = aLKTransInfoSchema.getPrem();
        this.TransferPrem = aLKTransInfoSchema.getTransferPrem();
        this.FuncFlag = aLKTransInfoSchema.getFuncFlag();
        this.TransDate = fDate.getDate( aLKTransInfoSchema.getTransDate());
        this.TransTime = aLKTransInfoSchema.getTransTime();
        this.MakeDate = fDate.getDate( aLKTransInfoSchema.getMakeDate());
        this.MakeTime = aLKTransInfoSchema.getMakeTime();
        this.ModifyDate = fDate.getDate( aLKTransInfoSchema.getModifyDate());
        this.ModifyTime = aLKTransInfoSchema.getModifyTime();
        this.Bak1 = aLKTransInfoSchema.getBak1();
        this.Bak2 = aLKTransInfoSchema.getBak2();
        this.Bak3 = aLKTransInfoSchema.getBak3();
        this.Bak4 = aLKTransInfoSchema.getBak4();
    }

    /**
    * 使用 ResultSet 中的第 i 行给 Schema 赋值
    * @param: rs ResultSet
    * @param: i int
    * @return: boolean
    **/
    public boolean setSchema(ResultSet rs,int i) {
        try {
            //rs.absolute(i);        // 非滚动游标
            this.TransInfoID = rs.getLong("TransInfoID");
            if( rs.getString("ShardingID") == null )
                this.ShardingID = null;
            else
                this.ShardingID = rs.getString("ShardingID").trim();

            if( rs.getString("SSTransCode") == null )
                this.SSTransCode = null;
            else
                this.SSTransCode = rs.getString("SSTransCode").trim();

            if( rs.getString("SuSpendTransCode") == null )
                this.SuSpendTransCode = null;
            else
                this.SuSpendTransCode = rs.getString("SuSpendTransCode").trim();

            if( rs.getString("TBTransCode") == null )
                this.TBTransCode = null;
            else
                this.TBTransCode = rs.getString("TBTransCode").trim();

            if( rs.getString("NewPolicyNo") == null )
                this.NewPolicyNo = null;
            else
                this.NewPolicyNo = rs.getString("NewPolicyNo").trim();

            if( rs.getString("OldPolicyNo") == null )
                this.OldPolicyNo = null;
            else
                this.OldPolicyNo = rs.getString("OldPolicyNo").trim();

            if( rs.getString("EdorAcceptNo") == null )
                this.EdorAcceptNo = null;
            else
                this.EdorAcceptNo = rs.getString("EdorAcceptNo").trim();

            this.OccurBala = rs.getDouble("OccurBala");
            this.Prem = rs.getDouble("Prem");
            this.TransferPrem = rs.getDouble("TransferPrem");
            if( rs.getString("FuncFlag") == null )
                this.FuncFlag = null;
            else
                this.FuncFlag = rs.getString("FuncFlag").trim();

            this.TransDate = rs.getDate("TransDate");
            if( rs.getString("TransTime") == null )
                this.TransTime = null;
            else
                this.TransTime = rs.getString("TransTime").trim();

            this.MakeDate = rs.getDate("MakeDate");
            if( rs.getString("MakeTime") == null )
                this.MakeTime = null;
            else
                this.MakeTime = rs.getString("MakeTime").trim();

            this.ModifyDate = rs.getDate("ModifyDate");
            if( rs.getString("ModifyTime") == null )
                this.ModifyTime = null;
            else
                this.ModifyTime = rs.getString("ModifyTime").trim();

            if( rs.getString("Bak1") == null )
                this.Bak1 = null;
            else
                this.Bak1 = rs.getString("Bak1").trim();

            if( rs.getString("Bak2") == null )
                this.Bak2 = null;
            else
                this.Bak2 = rs.getString("Bak2").trim();

            if( rs.getString("Bak3") == null )
                this.Bak3 = null;
            else
                this.Bak3 = rs.getString("Bak3").trim();

            if( rs.getString("Bak4") == null )
                this.Bak4 = null;
            else
                this.Bak4 = rs.getString("Bak4").trim();

        }
        catch(SQLException sqle) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LKTransInfoSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = ExceptionUtils.exceptionToString(sqle);
            this.mErrors .addOneError(tError);
            return false;
        }
        return true;
    }

    public LKTransInfoSchema getSchema() {
        LKTransInfoSchema aLKTransInfoSchema = new LKTransInfoSchema();
        aLKTransInfoSchema.setSchema(this);
        return aLKTransInfoSchema;
    }

    public LKTransInfoDB getDB() {
        LKTransInfoDB aDBOper = new LKTransInfoDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
    * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLKTransInfo描述/A>表字段
    * @return: String 返回打包后字符串
    **/
    public String encode() {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(ChgData.chgData(TransInfoID));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ShardingID)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(SSTransCode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(SuSpendTransCode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(TBTransCode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(NewPolicyNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(OldPolicyNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(EdorAcceptNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(OccurBala));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(Prem));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(TransferPrem));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(FuncFlag)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( TransDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(TransTime)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ModifyTime)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Bak1)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Bak2)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Bak3)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Bak4));
        return strReturn.toString();
    }

    /**
    * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLKTransInfo>历史记账凭证主表信息</A>表字段
    * @param: strMessage String 包含一条纪录数据的字符串
    * @return: boolean
    **/
    public boolean decode(String strMessage) {
        try {
            TransInfoID = new Long(ChgData.chgNumericStr(StrTool.getStr(strMessage,1, SysConst.PACKAGESPILTER))).longValue();
            ShardingID = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
            SSTransCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
            SuSpendTransCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
            TBTransCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
            NewPolicyNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
            OldPolicyNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
            EdorAcceptNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
            OccurBala = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,9, SysConst.PACKAGESPILTER))).doubleValue();
            Prem = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,10, SysConst.PACKAGESPILTER))).doubleValue();
            TransferPrem = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,11, SysConst.PACKAGESPILTER))).doubleValue();
            FuncFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
            TransDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER));
            TransTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER );
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16, SysConst.PACKAGESPILTER );
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18, SysConst.PACKAGESPILTER );
            Bak1 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19, SysConst.PACKAGESPILTER );
            Bak2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20, SysConst.PACKAGESPILTER );
            Bak3 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 21, SysConst.PACKAGESPILTER );
            Bak4 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 22, SysConst.PACKAGESPILTER );
        }
        catch(NumberFormatException ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LKTransInfoSchema";
            tError.functionName = "decode";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            return false;
        }
        return true;
    }

    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("TransInfoID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(TransInfoID));
        }
        if (FCode.equalsIgnoreCase("ShardingID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ShardingID));
        }
        if (FCode.equalsIgnoreCase("SSTransCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SSTransCode));
        }
        if (FCode.equalsIgnoreCase("SuSpendTransCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SuSpendTransCode));
        }
        if (FCode.equalsIgnoreCase("TBTransCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(TBTransCode));
        }
        if (FCode.equalsIgnoreCase("NewPolicyNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(NewPolicyNo));
        }
        if (FCode.equalsIgnoreCase("OldPolicyNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OldPolicyNo));
        }
        if (FCode.equalsIgnoreCase("EdorAcceptNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(EdorAcceptNo));
        }
        if (FCode.equalsIgnoreCase("OccurBala")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OccurBala));
        }
        if (FCode.equalsIgnoreCase("Prem")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Prem));
        }
        if (FCode.equalsIgnoreCase("TransferPrem")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(TransferPrem));
        }
        if (FCode.equalsIgnoreCase("FuncFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FuncFlag));
        }
        if (FCode.equalsIgnoreCase("TransDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getTransDate()));
        }
        if (FCode.equalsIgnoreCase("TransTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(TransTime));
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (FCode.equalsIgnoreCase("Bak1")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Bak1));
        }
        if (FCode.equalsIgnoreCase("Bak2")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Bak2));
        }
        if (FCode.equalsIgnoreCase("Bak3")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Bak3));
        }
        if (FCode.equalsIgnoreCase("Bak4")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Bak4));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = String.valueOf(TransInfoID);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(ShardingID);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(SSTransCode);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(SuSpendTransCode);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(TBTransCode);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(NewPolicyNo);
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(OldPolicyNo);
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(EdorAcceptNo);
                break;
            case 8:
                strFieldValue = String.valueOf(OccurBala);
                break;
            case 9:
                strFieldValue = String.valueOf(Prem);
                break;
            case 10:
                strFieldValue = String.valueOf(TransferPrem);
                break;
            case 11:
                strFieldValue = StrTool.GBKToUnicode(FuncFlag);
                break;
            case 12:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getTransDate()));
                break;
            case 13:
                strFieldValue = StrTool.GBKToUnicode(TransTime);
                break;
            case 14:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
                break;
            case 15:
                strFieldValue = StrTool.GBKToUnicode(MakeTime);
                break;
            case 16:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
                break;
            case 17:
                strFieldValue = StrTool.GBKToUnicode(ModifyTime);
                break;
            case 18:
                strFieldValue = StrTool.GBKToUnicode(Bak1);
                break;
            case 19:
                strFieldValue = StrTool.GBKToUnicode(Bak2);
                break;
            case 20:
                strFieldValue = StrTool.GBKToUnicode(Bak3);
                break;
            case 21:
                strFieldValue = StrTool.GBKToUnicode(Bak4);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("TransInfoID")) {
            if( FValue != null && !FValue.equals("")) {
                TransInfoID = new Long(FValue).longValue();
            }
        }
        if (FCode.equalsIgnoreCase("ShardingID")) {
            if( FValue != null && !FValue.equals(""))
            {
                ShardingID = FValue.trim();
            }
            else
                ShardingID = null;
        }
        if (FCode.equalsIgnoreCase("SSTransCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                SSTransCode = FValue.trim();
            }
            else
                SSTransCode = null;
        }
        if (FCode.equalsIgnoreCase("SuSpendTransCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                SuSpendTransCode = FValue.trim();
            }
            else
                SuSpendTransCode = null;
        }
        if (FCode.equalsIgnoreCase("TBTransCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                TBTransCode = FValue.trim();
            }
            else
                TBTransCode = null;
        }
        if (FCode.equalsIgnoreCase("NewPolicyNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                NewPolicyNo = FValue.trim();
            }
            else
                NewPolicyNo = null;
        }
        if (FCode.equalsIgnoreCase("OldPolicyNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                OldPolicyNo = FValue.trim();
            }
            else
                OldPolicyNo = null;
        }
        if (FCode.equalsIgnoreCase("EdorAcceptNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                EdorAcceptNo = FValue.trim();
            }
            else
                EdorAcceptNo = null;
        }
        if (FCode.equalsIgnoreCase("OccurBala")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                OccurBala = d;
            }
        }
        if (FCode.equalsIgnoreCase("Prem")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                Prem = d;
            }
        }
        if (FCode.equalsIgnoreCase("TransferPrem")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                TransferPrem = d;
            }
        }
        if (FCode.equalsIgnoreCase("FuncFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                FuncFlag = FValue.trim();
            }
            else
                FuncFlag = null;
        }
        if (FCode.equalsIgnoreCase("TransDate")) {
            if(FValue != null && !FValue.equals("")) {
                TransDate = fDate.getDate( FValue );
            }
            else
                TransDate = null;
        }
        if (FCode.equalsIgnoreCase("TransTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                TransTime = FValue.trim();
            }
            else
                TransTime = null;
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if(FValue != null && !FValue.equals("")) {
                MakeDate = fDate.getDate( FValue );
            }
            else
                MakeDate = null;
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
                MakeTime = null;
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if(FValue != null && !FValue.equals("")) {
                ModifyDate = fDate.getDate( FValue );
            }
            else
                ModifyDate = null;
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
                ModifyTime = null;
        }
        if (FCode.equalsIgnoreCase("Bak1")) {
            if( FValue != null && !FValue.equals(""))
            {
                Bak1 = FValue.trim();
            }
            else
                Bak1 = null;
        }
        if (FCode.equalsIgnoreCase("Bak2")) {
            if( FValue != null && !FValue.equals(""))
            {
                Bak2 = FValue.trim();
            }
            else
                Bak2 = null;
        }
        if (FCode.equalsIgnoreCase("Bak3")) {
            if( FValue != null && !FValue.equals(""))
            {
                Bak3 = FValue.trim();
            }
            else
                Bak3 = null;
        }
        if (FCode.equalsIgnoreCase("Bak4")) {
            if( FValue != null && !FValue.equals(""))
            {
                Bak4 = FValue.trim();
            }
            else
                Bak4 = null;
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject) return true;
        if (otherObject == null) return false;
        if (getClass() != otherObject.getClass()) return false;
        LKTransInfoSchema other = (LKTransInfoSchema)otherObject;
        return
            TransInfoID == other.getTransInfoID()
            && ShardingID.equals(other.getShardingID())
            && SSTransCode.equals(other.getSSTransCode())
            && SuSpendTransCode.equals(other.getSuSpendTransCode())
            && TBTransCode.equals(other.getTBTransCode())
            && NewPolicyNo.equals(other.getNewPolicyNo())
            && OldPolicyNo.equals(other.getOldPolicyNo())
            && EdorAcceptNo.equals(other.getEdorAcceptNo())
            && OccurBala == other.getOccurBala()
            && Prem == other.getPrem()
            && TransferPrem == other.getTransferPrem()
            && FuncFlag.equals(other.getFuncFlag())
            && fDate.getString(TransDate).equals(other.getTransDate())
            && TransTime.equals(other.getTransTime())
            && fDate.getString(MakeDate).equals(other.getMakeDate())
            && MakeTime.equals(other.getMakeTime())
            && fDate.getString(ModifyDate).equals(other.getModifyDate())
            && ModifyTime.equals(other.getModifyTime())
            && Bak1.equals(other.getBak1())
            && Bak2.equals(other.getBak2())
            && Bak3.equals(other.getBak3())
            && Bak4.equals(other.getBak4());
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("TransInfoID") ) {
            return 0;
        }
        if( strFieldName.equals("ShardingID") ) {
            return 1;
        }
        if( strFieldName.equals("SSTransCode") ) {
            return 2;
        }
        if( strFieldName.equals("SuSpendTransCode") ) {
            return 3;
        }
        if( strFieldName.equals("TBTransCode") ) {
            return 4;
        }
        if( strFieldName.equals("NewPolicyNo") ) {
            return 5;
        }
        if( strFieldName.equals("OldPolicyNo") ) {
            return 6;
        }
        if( strFieldName.equals("EdorAcceptNo") ) {
            return 7;
        }
        if( strFieldName.equals("OccurBala") ) {
            return 8;
        }
        if( strFieldName.equals("Prem") ) {
            return 9;
        }
        if( strFieldName.equals("TransferPrem") ) {
            return 10;
        }
        if( strFieldName.equals("FuncFlag") ) {
            return 11;
        }
        if( strFieldName.equals("TransDate") ) {
            return 12;
        }
        if( strFieldName.equals("TransTime") ) {
            return 13;
        }
        if( strFieldName.equals("MakeDate") ) {
            return 14;
        }
        if( strFieldName.equals("MakeTime") ) {
            return 15;
        }
        if( strFieldName.equals("ModifyDate") ) {
            return 16;
        }
        if( strFieldName.equals("ModifyTime") ) {
            return 17;
        }
        if( strFieldName.equals("Bak1") ) {
            return 18;
        }
        if( strFieldName.equals("Bak2") ) {
            return 19;
        }
        if( strFieldName.equals("Bak3") ) {
            return 20;
        }
        if( strFieldName.equals("Bak4") ) {
            return 21;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "TransInfoID";
                break;
            case 1:
                strFieldName = "ShardingID";
                break;
            case 2:
                strFieldName = "SSTransCode";
                break;
            case 3:
                strFieldName = "SuSpendTransCode";
                break;
            case 4:
                strFieldName = "TBTransCode";
                break;
            case 5:
                strFieldName = "NewPolicyNo";
                break;
            case 6:
                strFieldName = "OldPolicyNo";
                break;
            case 7:
                strFieldName = "EdorAcceptNo";
                break;
            case 8:
                strFieldName = "OccurBala";
                break;
            case 9:
                strFieldName = "Prem";
                break;
            case 10:
                strFieldName = "TransferPrem";
                break;
            case 11:
                strFieldName = "FuncFlag";
                break;
            case 12:
                strFieldName = "TransDate";
                break;
            case 13:
                strFieldName = "TransTime";
                break;
            case 14:
                strFieldName = "MakeDate";
                break;
            case 15:
                strFieldName = "MakeTime";
                break;
            case 16:
                strFieldName = "ModifyDate";
                break;
            case 17:
                strFieldName = "ModifyTime";
                break;
            case 18:
                strFieldName = "Bak1";
                break;
            case 19:
                strFieldName = "Bak2";
                break;
            case 20:
                strFieldName = "Bak3";
                break;
            case 21:
                strFieldName = "Bak4";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "TRANSINFOID":
                return Schema.TYPE_LONG;
            case "SHARDINGID":
                return Schema.TYPE_STRING;
            case "SSTRANSCODE":
                return Schema.TYPE_STRING;
            case "SUSPENDTRANSCODE":
                return Schema.TYPE_STRING;
            case "TBTRANSCODE":
                return Schema.TYPE_STRING;
            case "NEWPOLICYNO":
                return Schema.TYPE_STRING;
            case "OLDPOLICYNO":
                return Schema.TYPE_STRING;
            case "EDORACCEPTNO":
                return Schema.TYPE_STRING;
            case "OCCURBALA":
                return Schema.TYPE_DOUBLE;
            case "PREM":
                return Schema.TYPE_DOUBLE;
            case "TRANSFERPREM":
                return Schema.TYPE_DOUBLE;
            case "FUNCFLAG":
                return Schema.TYPE_STRING;
            case "TRANSDATE":
                return Schema.TYPE_DATE;
            case "TRANSTIME":
                return Schema.TYPE_STRING;
            case "MAKEDATE":
                return Schema.TYPE_DATE;
            case "MAKETIME":
                return Schema.TYPE_STRING;
            case "MODIFYDATE":
                return Schema.TYPE_DATE;
            case "MODIFYTIME":
                return Schema.TYPE_STRING;
            case "BAK1":
                return Schema.TYPE_STRING;
            case "BAK2":
                return Schema.TYPE_STRING;
            case "BAK3":
                return Schema.TYPE_STRING;
            case "BAK4":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_LONG;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_STRING;
            case 6:
                return Schema.TYPE_STRING;
            case 7:
                return Schema.TYPE_STRING;
            case 8:
                return Schema.TYPE_DOUBLE;
            case 9:
                return Schema.TYPE_DOUBLE;
            case 10:
                return Schema.TYPE_DOUBLE;
            case 11:
                return Schema.TYPE_STRING;
            case 12:
                return Schema.TYPE_DATE;
            case 13:
                return Schema.TYPE_STRING;
            case 14:
                return Schema.TYPE_DATE;
            case 15:
                return Schema.TYPE_STRING;
            case 16:
                return Schema.TYPE_DATE;
            case 17:
                return Schema.TYPE_STRING;
            case 18:
                return Schema.TYPE_STRING;
            case 19:
                return Schema.TYPE_STRING;
            case 20:
                return Schema.TYPE_STRING;
            case 21:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
}
