/**
 * Copyright (c) 2017 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import com.sinosoft.lis.db.LJTempFeeDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.*;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

/**
 * <p>ClassName: LJTempFeeSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2017</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2017-10-21
 */
public class LJTempFeeSchema implements Schema, Cloneable {
    // @Field
    /** Id */
    private long TempFeeID;
    /** Fk_ljtempfeeclass */
    private long TempFeeClassID;
    /** Shardingid */
    private String ShardingID;
    /** 暂交费收据号码 */
    private String TempFeeNo;
    /** 暂交费收据号类型 */
    private String TempFeeType;
    /** 险种编码 */
    private String RiskCode;
    /** 交费间隔 */
    private int PayIntv;
    /** 对应其它号码 */
    private String OtherNo;
    /** 其它号码类型 */
    private String OtherNoType;
    /** 交费金额 */
    private double PayMoney;
    /** 交费日期 */
    private Date PayDate;
    /** 到帐日期 */
    private Date EnterAccDate;
    /** 确认日期 */
    private Date ConfDate;
    /** 财务确认操作日期 */
    private Date ConfMakeDate;
    /** 财务确认操作时间 */
    private String ConfMakeTime;
    /** 销售渠道 */
    private String SaleChnl;
    /** 交费机构 */
    private String ManageCom;
    /** 管理机构 */
    private String PolicyCom;
    /** 代理机构 */
    private String AgentCom;
    /** 代理机构内部分类 */
    private String AgentType;
    /** 投保人名称 */
    private String APPntName;
    /** 代理人组别 */
    private String AgentGroup;
    /** 代理人编码 */
    private String AgentCode;
    /** 是否核销标志 */
    private String ConfFlag;
    /** 流水号 */
    private String SerialNo;
    /** 操作员 */
    private String Operator;
    /** 状态 */
    private String State;
    /** 入机时间 */
    private String MakeTime;
    /** 入机日期 */
    private Date MakeDate;
    /** 最后一次修改日期 */
    private Date ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime;
    /** 保单所属机构 */
    private String ContCom;
    /** 交费年期 */
    private int PayEndYear;
    /** 收据类型 */
    private String TempFeeNoType;
    /** 预收标保 */
    private double StandPrem;
    /** 备注 */
    private String Remark;
    /** 代理人所在区 */
    private String Distict;
    /** 代理人所在部 */
    private String Department;
    /** 代理人所在组 */
    private String BranchCode;
    /** 批单号 */
    private String BatchNo;
    /** 合同号码 */
    private String ContNo;

    public static final int FIELDNUM = 41;    // 数据库表的字段个数

    private static String[] PK;                // 主键

    private FDate fDate = new FDate();        // 处理日期

    public CErrors mErrors;            // 错误信息

    // @Constructor
    public LJTempFeeSchema() {
        mErrors = new CErrors();

        String[] pk = new String[1];
        pk[0] = "TempFeeID";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException {
        LJTempFeeSchema cloned = (LJTempFeeSchema)super.clone();
        cloned.fDate = (FDate) fDate.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK() {
        return PK;
    }

    public long getTempFeeID() {
        return TempFeeID;
    }
    public void setTempFeeID(long aTempFeeID) {
        TempFeeID = aTempFeeID;
    }
    public void setTempFeeID(String aTempFeeID) {
        if (aTempFeeID != null && !aTempFeeID.equals("")) {
            TempFeeID = new Long(aTempFeeID).longValue();
        }
    }

    public long getTempFeeClassID() {
        return TempFeeClassID;
    }
    public void setTempFeeClassID(long aTempFeeClassID) {
        TempFeeClassID = aTempFeeClassID;
    }
    public void setTempFeeClassID(String aTempFeeClassID) {
        if (aTempFeeClassID != null && !aTempFeeClassID.equals("")) {
            TempFeeClassID = new Long(aTempFeeClassID).longValue();
        }
    }

    public String getShardingID() {
        return ShardingID;
    }
    public void setShardingID(String aShardingID) {
        ShardingID = aShardingID;
    }
    public String getTempFeeNo() {
        return TempFeeNo;
    }
    public void setTempFeeNo(String aTempFeeNo) {
        TempFeeNo = aTempFeeNo;
    }
    public String getTempFeeType() {
        return TempFeeType;
    }
    public void setTempFeeType(String aTempFeeType) {
        TempFeeType = aTempFeeType;
    }
    public String getRiskCode() {
        return RiskCode;
    }
    public void setRiskCode(String aRiskCode) {
        RiskCode = aRiskCode;
    }
    public int getPayIntv() {
        return PayIntv;
    }
    public void setPayIntv(int aPayIntv) {
        PayIntv = aPayIntv;
    }
    public void setPayIntv(String aPayIntv) {
        if (aPayIntv != null && !aPayIntv.equals("")) {
            Integer tInteger = new Integer(aPayIntv);
            int i = tInteger.intValue();
            PayIntv = i;
        }
    }

    public String getOtherNo() {
        return OtherNo;
    }
    public void setOtherNo(String aOtherNo) {
        OtherNo = aOtherNo;
    }
    public String getOtherNoType() {
        return OtherNoType;
    }
    public void setOtherNoType(String aOtherNoType) {
        OtherNoType = aOtherNoType;
    }
    public double getPayMoney() {
        return PayMoney;
    }
    public void setPayMoney(double aPayMoney) {
        PayMoney = aPayMoney;
    }
    public void setPayMoney(String aPayMoney) {
        if (aPayMoney != null && !aPayMoney.equals("")) {
            Double tDouble = new Double(aPayMoney);
            double d = tDouble.doubleValue();
            PayMoney = d;
        }
    }

    public String getPayDate() {
        if(PayDate != null) {
            return fDate.getString(PayDate);
        } else {
            return null;
        }
    }
    public void setPayDate(Date aPayDate) {
        PayDate = aPayDate;
    }
    public void setPayDate(String aPayDate) {
        if (aPayDate != null && !aPayDate.equals("")) {
            PayDate = fDate.getDate(aPayDate);
        } else
            PayDate = null;
    }

    public String getEnterAccDate() {
        if(EnterAccDate != null) {
            return fDate.getString(EnterAccDate);
        } else {
            return null;
        }
    }
    public void setEnterAccDate(Date aEnterAccDate) {
        EnterAccDate = aEnterAccDate;
    }
    public void setEnterAccDate(String aEnterAccDate) {
        if (aEnterAccDate != null && !aEnterAccDate.equals("")) {
            EnterAccDate = fDate.getDate(aEnterAccDate);
        } else
            EnterAccDate = null;
    }

    public String getConfDate() {
        if(ConfDate != null) {
            return fDate.getString(ConfDate);
        } else {
            return null;
        }
    }
    public void setConfDate(Date aConfDate) {
        ConfDate = aConfDate;
    }
    public void setConfDate(String aConfDate) {
        if (aConfDate != null && !aConfDate.equals("")) {
            ConfDate = fDate.getDate(aConfDate);
        } else
            ConfDate = null;
    }

    public String getConfMakeDate() {
        if(ConfMakeDate != null) {
            return fDate.getString(ConfMakeDate);
        } else {
            return null;
        }
    }
    public void setConfMakeDate(Date aConfMakeDate) {
        ConfMakeDate = aConfMakeDate;
    }
    public void setConfMakeDate(String aConfMakeDate) {
        if (aConfMakeDate != null && !aConfMakeDate.equals("")) {
            ConfMakeDate = fDate.getDate(aConfMakeDate);
        } else
            ConfMakeDate = null;
    }

    public String getConfMakeTime() {
        return ConfMakeTime;
    }
    public void setConfMakeTime(String aConfMakeTime) {
        ConfMakeTime = aConfMakeTime;
    }
    public String getSaleChnl() {
        return SaleChnl;
    }
    public void setSaleChnl(String aSaleChnl) {
        SaleChnl = aSaleChnl;
    }
    public String getManageCom() {
        return ManageCom;
    }
    public void setManageCom(String aManageCom) {
        ManageCom = aManageCom;
    }
    public String getPolicyCom() {
        return PolicyCom;
    }
    public void setPolicyCom(String aPolicyCom) {
        PolicyCom = aPolicyCom;
    }
    public String getAgentCom() {
        return AgentCom;
    }
    public void setAgentCom(String aAgentCom) {
        AgentCom = aAgentCom;
    }
    public String getAgentType() {
        return AgentType;
    }
    public void setAgentType(String aAgentType) {
        AgentType = aAgentType;
    }
    public String getAPPntName() {
        return APPntName;
    }
    public void setAPPntName(String aAPPntName) {
        APPntName = aAPPntName;
    }
    public String getAgentGroup() {
        return AgentGroup;
    }
    public void setAgentGroup(String aAgentGroup) {
        AgentGroup = aAgentGroup;
    }
    public String getAgentCode() {
        return AgentCode;
    }
    public void setAgentCode(String aAgentCode) {
        AgentCode = aAgentCode;
    }
    public String getConfFlag() {
        return ConfFlag;
    }
    public void setConfFlag(String aConfFlag) {
        ConfFlag = aConfFlag;
    }
    public String getSerialNo() {
        return SerialNo;
    }
    public void setSerialNo(String aSerialNo) {
        SerialNo = aSerialNo;
    }
    public String getOperator() {
        return Operator;
    }
    public void setOperator(String aOperator) {
        Operator = aOperator;
    }
    public String getState() {
        return State;
    }
    public void setState(String aState) {
        State = aState;
    }
    public String getMakeTime() {
        return MakeTime;
    }
    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }
    public String getMakeDate() {
        if(MakeDate != null) {
            return fDate.getString(MakeDate);
        } else {
            return null;
        }
    }
    public void setMakeDate(Date aMakeDate) {
        MakeDate = aMakeDate;
    }
    public void setMakeDate(String aMakeDate) {
        if (aMakeDate != null && !aMakeDate.equals("")) {
            MakeDate = fDate.getDate(aMakeDate);
        } else
            MakeDate = null;
    }

    public String getModifyDate() {
        if(ModifyDate != null) {
            return fDate.getString(ModifyDate);
        } else {
            return null;
        }
    }
    public void setModifyDate(Date aModifyDate) {
        ModifyDate = aModifyDate;
    }
    public void setModifyDate(String aModifyDate) {
        if (aModifyDate != null && !aModifyDate.equals("")) {
            ModifyDate = fDate.getDate(aModifyDate);
        } else
            ModifyDate = null;
    }

    public String getModifyTime() {
        return ModifyTime;
    }
    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }
    public String getContCom() {
        return ContCom;
    }
    public void setContCom(String aContCom) {
        ContCom = aContCom;
    }
    public int getPayEndYear() {
        return PayEndYear;
    }
    public void setPayEndYear(int aPayEndYear) {
        PayEndYear = aPayEndYear;
    }
    public void setPayEndYear(String aPayEndYear) {
        if (aPayEndYear != null && !aPayEndYear.equals("")) {
            Integer tInteger = new Integer(aPayEndYear);
            int i = tInteger.intValue();
            PayEndYear = i;
        }
    }

    public String getTempFeeNoType() {
        return TempFeeNoType;
    }
    public void setTempFeeNoType(String aTempFeeNoType) {
        TempFeeNoType = aTempFeeNoType;
    }
    public double getStandPrem() {
        return StandPrem;
    }
    public void setStandPrem(double aStandPrem) {
        StandPrem = aStandPrem;
    }
    public void setStandPrem(String aStandPrem) {
        if (aStandPrem != null && !aStandPrem.equals("")) {
            Double tDouble = new Double(aStandPrem);
            double d = tDouble.doubleValue();
            StandPrem = d;
        }
    }

    public String getRemark() {
        return Remark;
    }
    public void setRemark(String aRemark) {
        Remark = aRemark;
    }
    public String getDistict() {
        return Distict;
    }
    public void setDistict(String aDistict) {
        Distict = aDistict;
    }
    public String getDepartment() {
        return Department;
    }
    public void setDepartment(String aDepartment) {
        Department = aDepartment;
    }
    public String getBranchCode() {
        return BranchCode;
    }
    public void setBranchCode(String aBranchCode) {
        BranchCode = aBranchCode;
    }
    public String getBatchNo() {
        return BatchNo;
    }
    public void setBatchNo(String aBatchNo) {
        BatchNo = aBatchNo;
    }
    public String getContNo() {
        return ContNo;
    }
    public void setContNo(String aContNo) {
        ContNo = aContNo;
    }

    /**
    * 使用另外一个 LJTempFeeSchema 对象给 Schema 赋值
    * @param: aLJTempFeeSchema LJTempFeeSchema
    **/
    public void setSchema(LJTempFeeSchema aLJTempFeeSchema) {
        this.TempFeeID = aLJTempFeeSchema.getTempFeeID();
        this.TempFeeClassID = aLJTempFeeSchema.getTempFeeClassID();
        this.ShardingID = aLJTempFeeSchema.getShardingID();
        this.TempFeeNo = aLJTempFeeSchema.getTempFeeNo();
        this.TempFeeType = aLJTempFeeSchema.getTempFeeType();
        this.RiskCode = aLJTempFeeSchema.getRiskCode();
        this.PayIntv = aLJTempFeeSchema.getPayIntv();
        this.OtherNo = aLJTempFeeSchema.getOtherNo();
        this.OtherNoType = aLJTempFeeSchema.getOtherNoType();
        this.PayMoney = aLJTempFeeSchema.getPayMoney();
        this.PayDate = fDate.getDate( aLJTempFeeSchema.getPayDate());
        this.EnterAccDate = fDate.getDate( aLJTempFeeSchema.getEnterAccDate());
        this.ConfDate = fDate.getDate( aLJTempFeeSchema.getConfDate());
        this.ConfMakeDate = fDate.getDate( aLJTempFeeSchema.getConfMakeDate());
        this.ConfMakeTime = aLJTempFeeSchema.getConfMakeTime();
        this.SaleChnl = aLJTempFeeSchema.getSaleChnl();
        this.ManageCom = aLJTempFeeSchema.getManageCom();
        this.PolicyCom = aLJTempFeeSchema.getPolicyCom();
        this.AgentCom = aLJTempFeeSchema.getAgentCom();
        this.AgentType = aLJTempFeeSchema.getAgentType();
        this.APPntName = aLJTempFeeSchema.getAPPntName();
        this.AgentGroup = aLJTempFeeSchema.getAgentGroup();
        this.AgentCode = aLJTempFeeSchema.getAgentCode();
        this.ConfFlag = aLJTempFeeSchema.getConfFlag();
        this.SerialNo = aLJTempFeeSchema.getSerialNo();
        this.Operator = aLJTempFeeSchema.getOperator();
        this.State = aLJTempFeeSchema.getState();
        this.MakeTime = aLJTempFeeSchema.getMakeTime();
        this.MakeDate = fDate.getDate( aLJTempFeeSchema.getMakeDate());
        this.ModifyDate = fDate.getDate( aLJTempFeeSchema.getModifyDate());
        this.ModifyTime = aLJTempFeeSchema.getModifyTime();
        this.ContCom = aLJTempFeeSchema.getContCom();
        this.PayEndYear = aLJTempFeeSchema.getPayEndYear();
        this.TempFeeNoType = aLJTempFeeSchema.getTempFeeNoType();
        this.StandPrem = aLJTempFeeSchema.getStandPrem();
        this.Remark = aLJTempFeeSchema.getRemark();
        this.Distict = aLJTempFeeSchema.getDistict();
        this.Department = aLJTempFeeSchema.getDepartment();
        this.BranchCode = aLJTempFeeSchema.getBranchCode();
        this.BatchNo = aLJTempFeeSchema.getBatchNo();
        this.ContNo = aLJTempFeeSchema.getContNo();
    }

    /**
    * 使用 ResultSet 中的第 i 行给 Schema 赋值
    * @param: rs ResultSet
    * @param: i int
    * @return: boolean
    **/
    public boolean setSchema(ResultSet rs,int i) {
        try {
            //rs.absolute(i);        // 非滚动游标
            this.TempFeeID = rs.getLong("TempFeeID");
            this.TempFeeClassID = rs.getLong("TempFeeClassID");
            if( rs.getString("ShardingID") == null )
                this.ShardingID = null;
            else
                this.ShardingID = rs.getString("ShardingID").trim();

            if( rs.getString("TempFeeNo") == null )
                this.TempFeeNo = null;
            else
                this.TempFeeNo = rs.getString("TempFeeNo").trim();

            if( rs.getString("TempFeeType") == null )
                this.TempFeeType = null;
            else
                this.TempFeeType = rs.getString("TempFeeType").trim();

            if( rs.getString("RiskCode") == null )
                this.RiskCode = null;
            else
                this.RiskCode = rs.getString("RiskCode").trim();

            this.PayIntv = rs.getInt("PayIntv");
            if( rs.getString("OtherNo") == null )
                this.OtherNo = null;
            else
                this.OtherNo = rs.getString("OtherNo").trim();

            if( rs.getString("OtherNoType") == null )
                this.OtherNoType = null;
            else
                this.OtherNoType = rs.getString("OtherNoType").trim();

            this.PayMoney = rs.getDouble("PayMoney");
            this.PayDate = rs.getDate("PayDate");
            this.EnterAccDate = rs.getDate("EnterAccDate");
            this.ConfDate = rs.getDate("ConfDate");
            this.ConfMakeDate = rs.getDate("ConfMakeDate");
            if( rs.getString("ConfMakeTime") == null )
                this.ConfMakeTime = null;
            else
                this.ConfMakeTime = rs.getString("ConfMakeTime").trim();

            if( rs.getString("SaleChnl") == null )
                this.SaleChnl = null;
            else
                this.SaleChnl = rs.getString("SaleChnl").trim();

            if( rs.getString("ManageCom") == null )
                this.ManageCom = null;
            else
                this.ManageCom = rs.getString("ManageCom").trim();

            if( rs.getString("PolicyCom") == null )
                this.PolicyCom = null;
            else
                this.PolicyCom = rs.getString("PolicyCom").trim();

            if( rs.getString("AgentCom") == null )
                this.AgentCom = null;
            else
                this.AgentCom = rs.getString("AgentCom").trim();

            if( rs.getString("AgentType") == null )
                this.AgentType = null;
            else
                this.AgentType = rs.getString("AgentType").trim();

            if( rs.getString("APPntName") == null )
                this.APPntName = null;
            else
                this.APPntName = rs.getString("APPntName").trim();

            if( rs.getString("AgentGroup") == null )
                this.AgentGroup = null;
            else
                this.AgentGroup = rs.getString("AgentGroup").trim();

            if( rs.getString("AgentCode") == null )
                this.AgentCode = null;
            else
                this.AgentCode = rs.getString("AgentCode").trim();

            if( rs.getString("ConfFlag") == null )
                this.ConfFlag = null;
            else
                this.ConfFlag = rs.getString("ConfFlag").trim();

            if( rs.getString("SerialNo") == null )
                this.SerialNo = null;
            else
                this.SerialNo = rs.getString("SerialNo").trim();

            if( rs.getString("Operator") == null )
                this.Operator = null;
            else
                this.Operator = rs.getString("Operator").trim();

            if( rs.getString("State") == null )
                this.State = null;
            else
                this.State = rs.getString("State").trim();

            if( rs.getString("MakeTime") == null )
                this.MakeTime = null;
            else
                this.MakeTime = rs.getString("MakeTime").trim();

            this.MakeDate = rs.getDate("MakeDate");
            this.ModifyDate = rs.getDate("ModifyDate");
            if( rs.getString("ModifyTime") == null )
                this.ModifyTime = null;
            else
                this.ModifyTime = rs.getString("ModifyTime").trim();

            if( rs.getString("ContCom") == null )
                this.ContCom = null;
            else
                this.ContCom = rs.getString("ContCom").trim();

            this.PayEndYear = rs.getInt("PayEndYear");
            if( rs.getString("TempFeeNoType") == null )
                this.TempFeeNoType = null;
            else
                this.TempFeeNoType = rs.getString("TempFeeNoType").trim();

            this.StandPrem = rs.getDouble("StandPrem");
            if( rs.getString("Remark") == null )
                this.Remark = null;
            else
                this.Remark = rs.getString("Remark").trim();

            if( rs.getString("Distict") == null )
                this.Distict = null;
            else
                this.Distict = rs.getString("Distict").trim();

            if( rs.getString("Department") == null )
                this.Department = null;
            else
                this.Department = rs.getString("Department").trim();

            if( rs.getString("BranchCode") == null )
                this.BranchCode = null;
            else
                this.BranchCode = rs.getString("BranchCode").trim();

            if( rs.getString("BatchNo") == null )
                this.BatchNo = null;
            else
                this.BatchNo = rs.getString("BatchNo").trim();

            if( rs.getString("ContNo") == null )
                this.ContNo = null;
            else
                this.ContNo = rs.getString("ContNo").trim();

        }
        catch(SQLException sqle) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LJTempFeeSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = ExceptionUtils.exceptionToString(sqle);
            this.mErrors .addOneError(tError);
            return false;
        }
        return true;
    }

    public LJTempFeeSchema getSchema() {
        LJTempFeeSchema aLJTempFeeSchema = new LJTempFeeSchema();
        aLJTempFeeSchema.setSchema(this);
        return aLJTempFeeSchema;
    }

    public LJTempFeeDB getDB() {
        LJTempFeeDB aDBOper = new LJTempFeeDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
    * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLJTempFee描述/A>表字段
    * @return: String 返回打包后字符串
    **/
    public String encode() {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(ChgData.chgData(TempFeeID));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(TempFeeClassID));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ShardingID)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(TempFeeNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(TempFeeType)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(RiskCode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(PayIntv));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(OtherNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(OtherNoType)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(PayMoney));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( PayDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( EnterAccDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( ConfDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( ConfMakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ConfMakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(SaleChnl)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ManageCom)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(PolicyCom)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AgentCom)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AgentType)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(APPntName)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AgentGroup)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AgentCode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ConfFlag)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(SerialNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(State)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ModifyTime)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ContCom)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(PayEndYear));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(TempFeeNoType)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(StandPrem));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Remark)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Distict)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Department)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(BranchCode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(BatchNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ContNo));
        return strReturn.toString();
    }

    /**
    * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLJTempFee>历史记账凭证主表信息</A>表字段
    * @param: strMessage String 包含一条纪录数据的字符串
    * @return: boolean
    **/
    public boolean decode(String strMessage) {
        try {
            TempFeeID = new Long(ChgData.chgNumericStr(StrTool.getStr(strMessage,1, SysConst.PACKAGESPILTER))).longValue();
            TempFeeClassID = new Long(ChgData.chgNumericStr(StrTool.getStr(strMessage,2, SysConst.PACKAGESPILTER))).longValue();
            ShardingID = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
            TempFeeNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
            TempFeeType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
            RiskCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
            PayIntv = new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,7, SysConst.PACKAGESPILTER))).intValue();
            OtherNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
            OtherNoType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
            PayMoney = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,10, SysConst.PACKAGESPILTER))).doubleValue();
            PayDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER));
            EnterAccDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER));
            ConfDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER));
            ConfMakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER));
            ConfMakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15, SysConst.PACKAGESPILTER );
            SaleChnl = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16, SysConst.PACKAGESPILTER );
            ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17, SysConst.PACKAGESPILTER );
            PolicyCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18, SysConst.PACKAGESPILTER );
            AgentCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19, SysConst.PACKAGESPILTER );
            AgentType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20, SysConst.PACKAGESPILTER );
            APPntName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 21, SysConst.PACKAGESPILTER );
            AgentGroup = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 22, SysConst.PACKAGESPILTER );
            AgentCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 23, SysConst.PACKAGESPILTER );
            ConfFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 24, SysConst.PACKAGESPILTER );
            SerialNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 25, SysConst.PACKAGESPILTER );
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 26, SysConst.PACKAGESPILTER );
            State = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 27, SysConst.PACKAGESPILTER );
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 28, SysConst.PACKAGESPILTER );
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 29, SysConst.PACKAGESPILTER));
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 30, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 31, SysConst.PACKAGESPILTER );
            ContCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 32, SysConst.PACKAGESPILTER );
            PayEndYear = new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,33, SysConst.PACKAGESPILTER))).intValue();
            TempFeeNoType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 34, SysConst.PACKAGESPILTER );
            StandPrem = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,35, SysConst.PACKAGESPILTER))).doubleValue();
            Remark = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 36, SysConst.PACKAGESPILTER );
            Distict = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 37, SysConst.PACKAGESPILTER );
            Department = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 38, SysConst.PACKAGESPILTER );
            BranchCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 39, SysConst.PACKAGESPILTER );
            BatchNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 40, SysConst.PACKAGESPILTER );
            ContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 41, SysConst.PACKAGESPILTER );
        }
        catch(NumberFormatException ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LJTempFeeSchema";
            tError.functionName = "decode";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            return false;
        }
        return true;
    }

    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("TempFeeID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(TempFeeID));
        }
        if (FCode.equalsIgnoreCase("TempFeeClassID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(TempFeeClassID));
        }
        if (FCode.equalsIgnoreCase("ShardingID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ShardingID));
        }
        if (FCode.equalsIgnoreCase("TempFeeNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(TempFeeNo));
        }
        if (FCode.equalsIgnoreCase("TempFeeType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(TempFeeType));
        }
        if (FCode.equalsIgnoreCase("RiskCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RiskCode));
        }
        if (FCode.equalsIgnoreCase("PayIntv")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PayIntv));
        }
        if (FCode.equalsIgnoreCase("OtherNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OtherNo));
        }
        if (FCode.equalsIgnoreCase("OtherNoType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OtherNoType));
        }
        if (FCode.equalsIgnoreCase("PayMoney")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PayMoney));
        }
        if (FCode.equalsIgnoreCase("PayDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getPayDate()));
        }
        if (FCode.equalsIgnoreCase("EnterAccDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getEnterAccDate()));
        }
        if (FCode.equalsIgnoreCase("ConfDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getConfDate()));
        }
        if (FCode.equalsIgnoreCase("ConfMakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getConfMakeDate()));
        }
        if (FCode.equalsIgnoreCase("ConfMakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ConfMakeTime));
        }
        if (FCode.equalsIgnoreCase("SaleChnl")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SaleChnl));
        }
        if (FCode.equalsIgnoreCase("ManageCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
        }
        if (FCode.equalsIgnoreCase("PolicyCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PolicyCom));
        }
        if (FCode.equalsIgnoreCase("AgentCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentCom));
        }
        if (FCode.equalsIgnoreCase("AgentType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentType));
        }
        if (FCode.equalsIgnoreCase("APPntName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(APPntName));
        }
        if (FCode.equalsIgnoreCase("AgentGroup")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentGroup));
        }
        if (FCode.equalsIgnoreCase("AgentCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentCode));
        }
        if (FCode.equalsIgnoreCase("ConfFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ConfFlag));
        }
        if (FCode.equalsIgnoreCase("SerialNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SerialNo));
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equalsIgnoreCase("State")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(State));
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (FCode.equalsIgnoreCase("ContCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ContCom));
        }
        if (FCode.equalsIgnoreCase("PayEndYear")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PayEndYear));
        }
        if (FCode.equalsIgnoreCase("TempFeeNoType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(TempFeeNoType));
        }
        if (FCode.equalsIgnoreCase("StandPrem")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StandPrem));
        }
        if (FCode.equalsIgnoreCase("Remark")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Remark));
        }
        if (FCode.equalsIgnoreCase("Distict")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Distict));
        }
        if (FCode.equalsIgnoreCase("Department")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Department));
        }
        if (FCode.equalsIgnoreCase("BranchCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BranchCode));
        }
        if (FCode.equalsIgnoreCase("BatchNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BatchNo));
        }
        if (FCode.equalsIgnoreCase("ContNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ContNo));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = String.valueOf(TempFeeID);
                break;
            case 1:
                strFieldValue = String.valueOf(TempFeeClassID);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(ShardingID);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(TempFeeNo);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(TempFeeType);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(RiskCode);
                break;
            case 6:
                strFieldValue = String.valueOf(PayIntv);
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(OtherNo);
                break;
            case 8:
                strFieldValue = StrTool.GBKToUnicode(OtherNoType);
                break;
            case 9:
                strFieldValue = String.valueOf(PayMoney);
                break;
            case 10:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getPayDate()));
                break;
            case 11:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getEnterAccDate()));
                break;
            case 12:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getConfDate()));
                break;
            case 13:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getConfMakeDate()));
                break;
            case 14:
                strFieldValue = StrTool.GBKToUnicode(ConfMakeTime);
                break;
            case 15:
                strFieldValue = StrTool.GBKToUnicode(SaleChnl);
                break;
            case 16:
                strFieldValue = StrTool.GBKToUnicode(ManageCom);
                break;
            case 17:
                strFieldValue = StrTool.GBKToUnicode(PolicyCom);
                break;
            case 18:
                strFieldValue = StrTool.GBKToUnicode(AgentCom);
                break;
            case 19:
                strFieldValue = StrTool.GBKToUnicode(AgentType);
                break;
            case 20:
                strFieldValue = StrTool.GBKToUnicode(APPntName);
                break;
            case 21:
                strFieldValue = StrTool.GBKToUnicode(AgentGroup);
                break;
            case 22:
                strFieldValue = StrTool.GBKToUnicode(AgentCode);
                break;
            case 23:
                strFieldValue = StrTool.GBKToUnicode(ConfFlag);
                break;
            case 24:
                strFieldValue = StrTool.GBKToUnicode(SerialNo);
                break;
            case 25:
                strFieldValue = StrTool.GBKToUnicode(Operator);
                break;
            case 26:
                strFieldValue = StrTool.GBKToUnicode(State);
                break;
            case 27:
                strFieldValue = StrTool.GBKToUnicode(MakeTime);
                break;
            case 28:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
                break;
            case 29:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
                break;
            case 30:
                strFieldValue = StrTool.GBKToUnicode(ModifyTime);
                break;
            case 31:
                strFieldValue = StrTool.GBKToUnicode(ContCom);
                break;
            case 32:
                strFieldValue = String.valueOf(PayEndYear);
                break;
            case 33:
                strFieldValue = StrTool.GBKToUnicode(TempFeeNoType);
                break;
            case 34:
                strFieldValue = String.valueOf(StandPrem);
                break;
            case 35:
                strFieldValue = StrTool.GBKToUnicode(Remark);
                break;
            case 36:
                strFieldValue = StrTool.GBKToUnicode(Distict);
                break;
            case 37:
                strFieldValue = StrTool.GBKToUnicode(Department);
                break;
            case 38:
                strFieldValue = StrTool.GBKToUnicode(BranchCode);
                break;
            case 39:
                strFieldValue = StrTool.GBKToUnicode(BatchNo);
                break;
            case 40:
                strFieldValue = StrTool.GBKToUnicode(ContNo);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("TempFeeID")) {
            if( FValue != null && !FValue.equals("")) {
                TempFeeID = new Long(FValue).longValue();
            }
        }
        if (FCode.equalsIgnoreCase("TempFeeClassID")) {
            if( FValue != null && !FValue.equals("")) {
                TempFeeClassID = new Long(FValue).longValue();
            }
        }
        if (FCode.equalsIgnoreCase("ShardingID")) {
            if( FValue != null && !FValue.equals(""))
            {
                ShardingID = FValue.trim();
            }
            else
                ShardingID = null;
        }
        if (FCode.equalsIgnoreCase("TempFeeNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                TempFeeNo = FValue.trim();
            }
            else
                TempFeeNo = null;
        }
        if (FCode.equalsIgnoreCase("TempFeeType")) {
            if( FValue != null && !FValue.equals(""))
            {
                TempFeeType = FValue.trim();
            }
            else
                TempFeeType = null;
        }
        if (FCode.equalsIgnoreCase("RiskCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                RiskCode = FValue.trim();
            }
            else
                RiskCode = null;
        }
        if (FCode.equalsIgnoreCase("PayIntv")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                PayIntv = i;
            }
        }
        if (FCode.equalsIgnoreCase("OtherNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                OtherNo = FValue.trim();
            }
            else
                OtherNo = null;
        }
        if (FCode.equalsIgnoreCase("OtherNoType")) {
            if( FValue != null && !FValue.equals(""))
            {
                OtherNoType = FValue.trim();
            }
            else
                OtherNoType = null;
        }
        if (FCode.equalsIgnoreCase("PayMoney")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                PayMoney = d;
            }
        }
        if (FCode.equalsIgnoreCase("PayDate")) {
            if(FValue != null && !FValue.equals("")) {
                PayDate = fDate.getDate( FValue );
            }
            else
                PayDate = null;
        }
        if (FCode.equalsIgnoreCase("EnterAccDate")) {
            if(FValue != null && !FValue.equals("")) {
                EnterAccDate = fDate.getDate( FValue );
            }
            else
                EnterAccDate = null;
        }
        if (FCode.equalsIgnoreCase("ConfDate")) {
            if(FValue != null && !FValue.equals("")) {
                ConfDate = fDate.getDate( FValue );
            }
            else
                ConfDate = null;
        }
        if (FCode.equalsIgnoreCase("ConfMakeDate")) {
            if(FValue != null && !FValue.equals("")) {
                ConfMakeDate = fDate.getDate( FValue );
            }
            else
                ConfMakeDate = null;
        }
        if (FCode.equalsIgnoreCase("ConfMakeTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                ConfMakeTime = FValue.trim();
            }
            else
                ConfMakeTime = null;
        }
        if (FCode.equalsIgnoreCase("SaleChnl")) {
            if( FValue != null && !FValue.equals(""))
            {
                SaleChnl = FValue.trim();
            }
            else
                SaleChnl = null;
        }
        if (FCode.equalsIgnoreCase("ManageCom")) {
            if( FValue != null && !FValue.equals(""))
            {
                ManageCom = FValue.trim();
            }
            else
                ManageCom = null;
        }
        if (FCode.equalsIgnoreCase("PolicyCom")) {
            if( FValue != null && !FValue.equals(""))
            {
                PolicyCom = FValue.trim();
            }
            else
                PolicyCom = null;
        }
        if (FCode.equalsIgnoreCase("AgentCom")) {
            if( FValue != null && !FValue.equals(""))
            {
                AgentCom = FValue.trim();
            }
            else
                AgentCom = null;
        }
        if (FCode.equalsIgnoreCase("AgentType")) {
            if( FValue != null && !FValue.equals(""))
            {
                AgentType = FValue.trim();
            }
            else
                AgentType = null;
        }
        if (FCode.equalsIgnoreCase("APPntName")) {
            if( FValue != null && !FValue.equals(""))
            {
                APPntName = FValue.trim();
            }
            else
                APPntName = null;
        }
        if (FCode.equalsIgnoreCase("AgentGroup")) {
            if( FValue != null && !FValue.equals(""))
            {
                AgentGroup = FValue.trim();
            }
            else
                AgentGroup = null;
        }
        if (FCode.equalsIgnoreCase("AgentCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                AgentCode = FValue.trim();
            }
            else
                AgentCode = null;
        }
        if (FCode.equalsIgnoreCase("ConfFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                ConfFlag = FValue.trim();
            }
            else
                ConfFlag = null;
        }
        if (FCode.equalsIgnoreCase("SerialNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                SerialNo = FValue.trim();
            }
            else
                SerialNo = null;
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            if( FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
                Operator = null;
        }
        if (FCode.equalsIgnoreCase("State")) {
            if( FValue != null && !FValue.equals(""))
            {
                State = FValue.trim();
            }
            else
                State = null;
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
                MakeTime = null;
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if(FValue != null && !FValue.equals("")) {
                MakeDate = fDate.getDate( FValue );
            }
            else
                MakeDate = null;
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if(FValue != null && !FValue.equals("")) {
                ModifyDate = fDate.getDate( FValue );
            }
            else
                ModifyDate = null;
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
                ModifyTime = null;
        }
        if (FCode.equalsIgnoreCase("ContCom")) {
            if( FValue != null && !FValue.equals(""))
            {
                ContCom = FValue.trim();
            }
            else
                ContCom = null;
        }
        if (FCode.equalsIgnoreCase("PayEndYear")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                PayEndYear = i;
            }
        }
        if (FCode.equalsIgnoreCase("TempFeeNoType")) {
            if( FValue != null && !FValue.equals(""))
            {
                TempFeeNoType = FValue.trim();
            }
            else
                TempFeeNoType = null;
        }
        if (FCode.equalsIgnoreCase("StandPrem")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                StandPrem = d;
            }
        }
        if (FCode.equalsIgnoreCase("Remark")) {
            if( FValue != null && !FValue.equals(""))
            {
                Remark = FValue.trim();
            }
            else
                Remark = null;
        }
        if (FCode.equalsIgnoreCase("Distict")) {
            if( FValue != null && !FValue.equals(""))
            {
                Distict = FValue.trim();
            }
            else
                Distict = null;
        }
        if (FCode.equalsIgnoreCase("Department")) {
            if( FValue != null && !FValue.equals(""))
            {
                Department = FValue.trim();
            }
            else
                Department = null;
        }
        if (FCode.equalsIgnoreCase("BranchCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                BranchCode = FValue.trim();
            }
            else
                BranchCode = null;
        }
        if (FCode.equalsIgnoreCase("BatchNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                BatchNo = FValue.trim();
            }
            else
                BatchNo = null;
        }
        if (FCode.equalsIgnoreCase("ContNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                ContNo = FValue.trim();
            }
            else
                ContNo = null;
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject) return true;
        if (otherObject == null) return false;
        if (getClass() != otherObject.getClass()) return false;
        LJTempFeeSchema other = (LJTempFeeSchema)otherObject;
        return
            TempFeeID == other.getTempFeeID()
            && TempFeeClassID == other.getTempFeeClassID()
            && ShardingID.equals(other.getShardingID())
            && TempFeeNo.equals(other.getTempFeeNo())
            && TempFeeType.equals(other.getTempFeeType())
            && RiskCode.equals(other.getRiskCode())
            && PayIntv == other.getPayIntv()
            && OtherNo.equals(other.getOtherNo())
            && OtherNoType.equals(other.getOtherNoType())
            && PayMoney == other.getPayMoney()
            && fDate.getString(PayDate).equals(other.getPayDate())
            && fDate.getString(EnterAccDate).equals(other.getEnterAccDate())
            && fDate.getString(ConfDate).equals(other.getConfDate())
            && fDate.getString(ConfMakeDate).equals(other.getConfMakeDate())
            && ConfMakeTime.equals(other.getConfMakeTime())
            && SaleChnl.equals(other.getSaleChnl())
            && ManageCom.equals(other.getManageCom())
            && PolicyCom.equals(other.getPolicyCom())
            && AgentCom.equals(other.getAgentCom())
            && AgentType.equals(other.getAgentType())
            && APPntName.equals(other.getAPPntName())
            && AgentGroup.equals(other.getAgentGroup())
            && AgentCode.equals(other.getAgentCode())
            && ConfFlag.equals(other.getConfFlag())
            && SerialNo.equals(other.getSerialNo())
            && Operator.equals(other.getOperator())
            && State.equals(other.getState())
            && MakeTime.equals(other.getMakeTime())
            && fDate.getString(MakeDate).equals(other.getMakeDate())
            && fDate.getString(ModifyDate).equals(other.getModifyDate())
            && ModifyTime.equals(other.getModifyTime())
            && ContCom.equals(other.getContCom())
            && PayEndYear == other.getPayEndYear()
            && TempFeeNoType.equals(other.getTempFeeNoType())
            && StandPrem == other.getStandPrem()
            && Remark.equals(other.getRemark())
            && Distict.equals(other.getDistict())
            && Department.equals(other.getDepartment())
            && BranchCode.equals(other.getBranchCode())
            && BatchNo.equals(other.getBatchNo())
            && ContNo.equals(other.getContNo());
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("TempFeeID") ) {
            return 0;
        }
        if( strFieldName.equals("TempFeeClassID") ) {
            return 1;
        }
        if( strFieldName.equals("ShardingID") ) {
            return 2;
        }
        if( strFieldName.equals("TempFeeNo") ) {
            return 3;
        }
        if( strFieldName.equals("TempFeeType") ) {
            return 4;
        }
        if( strFieldName.equals("RiskCode") ) {
            return 5;
        }
        if( strFieldName.equals("PayIntv") ) {
            return 6;
        }
        if( strFieldName.equals("OtherNo") ) {
            return 7;
        }
        if( strFieldName.equals("OtherNoType") ) {
            return 8;
        }
        if( strFieldName.equals("PayMoney") ) {
            return 9;
        }
        if( strFieldName.equals("PayDate") ) {
            return 10;
        }
        if( strFieldName.equals("EnterAccDate") ) {
            return 11;
        }
        if( strFieldName.equals("ConfDate") ) {
            return 12;
        }
        if( strFieldName.equals("ConfMakeDate") ) {
            return 13;
        }
        if( strFieldName.equals("ConfMakeTime") ) {
            return 14;
        }
        if( strFieldName.equals("SaleChnl") ) {
            return 15;
        }
        if( strFieldName.equals("ManageCom") ) {
            return 16;
        }
        if( strFieldName.equals("PolicyCom") ) {
            return 17;
        }
        if( strFieldName.equals("AgentCom") ) {
            return 18;
        }
        if( strFieldName.equals("AgentType") ) {
            return 19;
        }
        if( strFieldName.equals("APPntName") ) {
            return 20;
        }
        if( strFieldName.equals("AgentGroup") ) {
            return 21;
        }
        if( strFieldName.equals("AgentCode") ) {
            return 22;
        }
        if( strFieldName.equals("ConfFlag") ) {
            return 23;
        }
        if( strFieldName.equals("SerialNo") ) {
            return 24;
        }
        if( strFieldName.equals("Operator") ) {
            return 25;
        }
        if( strFieldName.equals("State") ) {
            return 26;
        }
        if( strFieldName.equals("MakeTime") ) {
            return 27;
        }
        if( strFieldName.equals("MakeDate") ) {
            return 28;
        }
        if( strFieldName.equals("ModifyDate") ) {
            return 29;
        }
        if( strFieldName.equals("ModifyTime") ) {
            return 30;
        }
        if( strFieldName.equals("ContCom") ) {
            return 31;
        }
        if( strFieldName.equals("PayEndYear") ) {
            return 32;
        }
        if( strFieldName.equals("TempFeeNoType") ) {
            return 33;
        }
        if( strFieldName.equals("StandPrem") ) {
            return 34;
        }
        if( strFieldName.equals("Remark") ) {
            return 35;
        }
        if( strFieldName.equals("Distict") ) {
            return 36;
        }
        if( strFieldName.equals("Department") ) {
            return 37;
        }
        if( strFieldName.equals("BranchCode") ) {
            return 38;
        }
        if( strFieldName.equals("BatchNo") ) {
            return 39;
        }
        if( strFieldName.equals("ContNo") ) {
            return 40;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "TempFeeID";
                break;
            case 1:
                strFieldName = "TempFeeClassID";
                break;
            case 2:
                strFieldName = "ShardingID";
                break;
            case 3:
                strFieldName = "TempFeeNo";
                break;
            case 4:
                strFieldName = "TempFeeType";
                break;
            case 5:
                strFieldName = "RiskCode";
                break;
            case 6:
                strFieldName = "PayIntv";
                break;
            case 7:
                strFieldName = "OtherNo";
                break;
            case 8:
                strFieldName = "OtherNoType";
                break;
            case 9:
                strFieldName = "PayMoney";
                break;
            case 10:
                strFieldName = "PayDate";
                break;
            case 11:
                strFieldName = "EnterAccDate";
                break;
            case 12:
                strFieldName = "ConfDate";
                break;
            case 13:
                strFieldName = "ConfMakeDate";
                break;
            case 14:
                strFieldName = "ConfMakeTime";
                break;
            case 15:
                strFieldName = "SaleChnl";
                break;
            case 16:
                strFieldName = "ManageCom";
                break;
            case 17:
                strFieldName = "PolicyCom";
                break;
            case 18:
                strFieldName = "AgentCom";
                break;
            case 19:
                strFieldName = "AgentType";
                break;
            case 20:
                strFieldName = "APPntName";
                break;
            case 21:
                strFieldName = "AgentGroup";
                break;
            case 22:
                strFieldName = "AgentCode";
                break;
            case 23:
                strFieldName = "ConfFlag";
                break;
            case 24:
                strFieldName = "SerialNo";
                break;
            case 25:
                strFieldName = "Operator";
                break;
            case 26:
                strFieldName = "State";
                break;
            case 27:
                strFieldName = "MakeTime";
                break;
            case 28:
                strFieldName = "MakeDate";
                break;
            case 29:
                strFieldName = "ModifyDate";
                break;
            case 30:
                strFieldName = "ModifyTime";
                break;
            case 31:
                strFieldName = "ContCom";
                break;
            case 32:
                strFieldName = "PayEndYear";
                break;
            case 33:
                strFieldName = "TempFeeNoType";
                break;
            case 34:
                strFieldName = "StandPrem";
                break;
            case 35:
                strFieldName = "Remark";
                break;
            case 36:
                strFieldName = "Distict";
                break;
            case 37:
                strFieldName = "Department";
                break;
            case 38:
                strFieldName = "BranchCode";
                break;
            case 39:
                strFieldName = "BatchNo";
                break;
            case 40:
                strFieldName = "ContNo";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "TEMPFEEID":
                return Schema.TYPE_LONG;
            case "TEMPFEECLASSID":
                return Schema.TYPE_LONG;
            case "SHARDINGID":
                return Schema.TYPE_STRING;
            case "TEMPFEENO":
                return Schema.TYPE_STRING;
            case "TEMPFEETYPE":
                return Schema.TYPE_STRING;
            case "RISKCODE":
                return Schema.TYPE_STRING;
            case "PAYINTV":
                return Schema.TYPE_INT;
            case "OTHERNO":
                return Schema.TYPE_STRING;
            case "OTHERNOTYPE":
                return Schema.TYPE_STRING;
            case "PAYMONEY":
                return Schema.TYPE_DOUBLE;
            case "PAYDATE":
                return Schema.TYPE_DATE;
            case "ENTERACCDATE":
                return Schema.TYPE_DATE;
            case "CONFDATE":
                return Schema.TYPE_DATE;
            case "CONFMAKEDATE":
                return Schema.TYPE_DATE;
            case "CONFMAKETIME":
                return Schema.TYPE_STRING;
            case "SALECHNL":
                return Schema.TYPE_STRING;
            case "MANAGECOM":
                return Schema.TYPE_STRING;
            case "POLICYCOM":
                return Schema.TYPE_STRING;
            case "AGENTCOM":
                return Schema.TYPE_STRING;
            case "AGENTTYPE":
                return Schema.TYPE_STRING;
            case "APPNTNAME":
                return Schema.TYPE_STRING;
            case "AGENTGROUP":
                return Schema.TYPE_STRING;
            case "AGENTCODE":
                return Schema.TYPE_STRING;
            case "CONFFLAG":
                return Schema.TYPE_STRING;
            case "SERIALNO":
                return Schema.TYPE_STRING;
            case "OPERATOR":
                return Schema.TYPE_STRING;
            case "STATE":
                return Schema.TYPE_STRING;
            case "MAKETIME":
                return Schema.TYPE_STRING;
            case "MAKEDATE":
                return Schema.TYPE_DATE;
            case "MODIFYDATE":
                return Schema.TYPE_DATE;
            case "MODIFYTIME":
                return Schema.TYPE_STRING;
            case "CONTCOM":
                return Schema.TYPE_STRING;
            case "PAYENDYEAR":
                return Schema.TYPE_INT;
            case "TEMPFEENOTYPE":
                return Schema.TYPE_STRING;
            case "STANDPREM":
                return Schema.TYPE_DOUBLE;
            case "REMARK":
                return Schema.TYPE_STRING;
            case "DISTICT":
                return Schema.TYPE_STRING;
            case "DEPARTMENT":
                return Schema.TYPE_STRING;
            case "BRANCHCODE":
                return Schema.TYPE_STRING;
            case "BATCHNO":
                return Schema.TYPE_STRING;
            case "CONTNO":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_LONG;
            case 1:
                return Schema.TYPE_LONG;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_STRING;
            case 6:
                return Schema.TYPE_INT;
            case 7:
                return Schema.TYPE_STRING;
            case 8:
                return Schema.TYPE_STRING;
            case 9:
                return Schema.TYPE_DOUBLE;
            case 10:
                return Schema.TYPE_DATE;
            case 11:
                return Schema.TYPE_DATE;
            case 12:
                return Schema.TYPE_DATE;
            case 13:
                return Schema.TYPE_DATE;
            case 14:
                return Schema.TYPE_STRING;
            case 15:
                return Schema.TYPE_STRING;
            case 16:
                return Schema.TYPE_STRING;
            case 17:
                return Schema.TYPE_STRING;
            case 18:
                return Schema.TYPE_STRING;
            case 19:
                return Schema.TYPE_STRING;
            case 20:
                return Schema.TYPE_STRING;
            case 21:
                return Schema.TYPE_STRING;
            case 22:
                return Schema.TYPE_STRING;
            case 23:
                return Schema.TYPE_STRING;
            case 24:
                return Schema.TYPE_STRING;
            case 25:
                return Schema.TYPE_STRING;
            case 26:
                return Schema.TYPE_STRING;
            case 27:
                return Schema.TYPE_STRING;
            case 28:
                return Schema.TYPE_DATE;
            case 29:
                return Schema.TYPE_DATE;
            case 30:
                return Schema.TYPE_STRING;
            case 31:
                return Schema.TYPE_STRING;
            case 32:
                return Schema.TYPE_INT;
            case 33:
                return Schema.TYPE_STRING;
            case 34:
                return Schema.TYPE_DOUBLE;
            case 35:
                return Schema.TYPE_STRING;
            case 36:
                return Schema.TYPE_STRING;
            case 37:
                return Schema.TYPE_STRING;
            case 38:
                return Schema.TYPE_STRING;
            case 39:
                return Schema.TYPE_STRING;
            case 40:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
}
