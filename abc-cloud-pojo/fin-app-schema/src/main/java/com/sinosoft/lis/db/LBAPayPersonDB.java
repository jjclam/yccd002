/**
 * Copyright (c) 2017 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.db;

import com.sinosoft.lis.schema.LBAPayPersonSchema;
import com.sinosoft.lis.vschema.LBAPayPersonSet;
import com.sinosoft.utility.*;

import java.sql.*;

/**
 * <p>ClassName: LBAPayPersonDB </p>
 * <p>Description: DB层数据库操作类文件 </p>
 * <p>Copyright: Copyright (c) 2017</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2017-10-21
 */
public class LBAPayPersonDB extends LBAPayPersonSchema {
    // @Field
    private Connection con;
    private DBOper db;
    /**
    * flag = true: 传入Connection
    * flag = false: 不传入Connection
    **/
    private boolean mflag = false;

    public CErrors mErrors = new CErrors();        // 错误信息

    /**
     * 为批量操作而准备的语句和游标对象
     */
    private ResultSet mResultSet = null;
    private Statement mStatement = null;
    // @Constructor
    public LBAPayPersonDB( Connection tConnection ) {
        con = tConnection;
        db = new DBOper( con, "LBAPayPerson" );
        mflag = true;
    }

    public LBAPayPersonDB() {
        con = null;
        db = new DBOper( "LBAPayPerson" );
        mflag = false;
    }

    // @Method
    public boolean deleteSQL() {
        LBAPayPersonSchema tSchema = this.getSchema();
        if (db.deleteSQL(tSchema)) {
             return true;
        } else {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LBAPayPersonDB";
            tError.functionName = "deleteSQL";
            tError.errorMessage = "操作失败!";
            this.mErrors .addOneError(tError);
            return false;
        }
    }

    public int getCount() {
        LBAPayPersonSchema tSchema = this.getSchema();

        int tCount = db.getCount(tSchema);
        if (tCount < 0) {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LBAPayPersonDB";
            tError.functionName = "getCount";
            tError.errorMessage = "操作失败!";
            this.mErrors .addOneError(tError);

            return -1;
        }

        return tCount;
    }

    public boolean delete() {
        PreparedStatement pstmt = null;

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try {
            pstmt = con.prepareStatement("DELETE FROM LBAPayPerson WHERE  1=1  AND APayPersonID = ?");
            pstmt.setLong(1, this.getAPayPersonID());
            pstmt.executeUpdate();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LBAPayPersonDB";
            tError.functionName = "delete()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);
            ex.printStackTrace();

            try {
                pstmt.close();
            } catch (Exception e){}

            if(!mflag) {
                try {
                    con.close();
                } catch (Exception e){}
            }

            return false;
        }

        if(!mflag) {
            try {
                con.close();
            } catch (Exception e){}
        }

        return true;
    }

    public boolean update() {
        PreparedStatement pstmt = null;

        SQLString sqlObj = new SQLString("LBAPayPerson");
        sqlObj.setSQL(2, this);
        String sql = sqlObj.getSQL();

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try {
            pstmt = con.prepareStatement("UPDATE LBAPayPerson SET  APayPersonID = ? , ShardingID = ? , PolNo = ? , PayCount = ? , GrpContNo = ? , GrpPolNo = ? , ContNo = ? , ManageCom = ? , AgentCom = ? , AgentType = ? , RiskCode = ? , AgentCode = ? , AgentGroup = ? , PayTypeFlag = ? , AppntNo = ? , PayNo = ? , PayAimClass = ? , DutyCode = ? , PayPlanCode = ? , SumDuePayMoney = ? , SumActuPayMoney = ? , PayIntv = ? , PayDate = ? , PayType = ? , EnterAccDate = ? , ConfDate = ? , LastPayToDate = ? , CurPayToDate = ? , InInsuAccState = ? , ApproveCode = ? , ApproveDate = ? , ApproveTime = ? , SerialNo = ? , Operator = ? , MakeDate = ? , MakeTime = ? , GetNoticeNo = ? , ModifyDate = ? , ModifyTime = ? , EdorNo = ? , MainPolYear = ? , BackUpSerialNo = ? , BackUpReason = ? WHERE  1=1  AND APayPersonID = ?");
            pstmt.setLong(1, this.getAPayPersonID());
            if(this.getShardingID() == null || this.getShardingID().equals("null")) {
            	pstmt.setNull(2, 12);
            } else {
            	pstmt.setString(2, this.getShardingID());
            }
            if(this.getPolNo() == null || this.getPolNo().equals("null")) {
            	pstmt.setNull(3, 12);
            } else {
            	pstmt.setString(3, this.getPolNo());
            }
            pstmt.setInt(4, this.getPayCount());
            if(this.getGrpContNo() == null || this.getGrpContNo().equals("null")) {
            	pstmt.setNull(5, 12);
            } else {
            	pstmt.setString(5, this.getGrpContNo());
            }
            if(this.getGrpPolNo() == null || this.getGrpPolNo().equals("null")) {
            	pstmt.setNull(6, 12);
            } else {
            	pstmt.setString(6, this.getGrpPolNo());
            }
            if(this.getContNo() == null || this.getContNo().equals("null")) {
            	pstmt.setNull(7, 12);
            } else {
            	pstmt.setString(7, this.getContNo());
            }
            if(this.getManageCom() == null || this.getManageCom().equals("null")) {
            	pstmt.setNull(8, 12);
            } else {
            	pstmt.setString(8, this.getManageCom());
            }
            if(this.getAgentCom() == null || this.getAgentCom().equals("null")) {
            	pstmt.setNull(9, 12);
            } else {
            	pstmt.setString(9, this.getAgentCom());
            }
            if(this.getAgentType() == null || this.getAgentType().equals("null")) {
            	pstmt.setNull(10, 12);
            } else {
            	pstmt.setString(10, this.getAgentType());
            }
            if(this.getRiskCode() == null || this.getRiskCode().equals("null")) {
            	pstmt.setNull(11, 12);
            } else {
            	pstmt.setString(11, this.getRiskCode());
            }
            if(this.getAgentCode() == null || this.getAgentCode().equals("null")) {
            	pstmt.setNull(12, 12);
            } else {
            	pstmt.setString(12, this.getAgentCode());
            }
            if(this.getAgentGroup() == null || this.getAgentGroup().equals("null")) {
            	pstmt.setNull(13, 12);
            } else {
            	pstmt.setString(13, this.getAgentGroup());
            }
            if(this.getPayTypeFlag() == null || this.getPayTypeFlag().equals("null")) {
            	pstmt.setNull(14, 12);
            } else {
            	pstmt.setString(14, this.getPayTypeFlag());
            }
            if(this.getAppntNo() == null || this.getAppntNo().equals("null")) {
            	pstmt.setNull(15, 12);
            } else {
            	pstmt.setString(15, this.getAppntNo());
            }
            if(this.getPayNo() == null || this.getPayNo().equals("null")) {
            	pstmt.setNull(16, 12);
            } else {
            	pstmt.setString(16, this.getPayNo());
            }
            if(this.getPayAimClass() == null || this.getPayAimClass().equals("null")) {
            	pstmt.setNull(17, 12);
            } else {
            	pstmt.setString(17, this.getPayAimClass());
            }
            if(this.getDutyCode() == null || this.getDutyCode().equals("null")) {
            	pstmt.setNull(18, 12);
            } else {
            	pstmt.setString(18, this.getDutyCode());
            }
            if(this.getPayPlanCode() == null || this.getPayPlanCode().equals("null")) {
            	pstmt.setNull(19, 12);
            } else {
            	pstmt.setString(19, this.getPayPlanCode());
            }
            pstmt.setDouble(20, this.getSumDuePayMoney());
            pstmt.setDouble(21, this.getSumActuPayMoney());
            pstmt.setInt(22, this.getPayIntv());
            if(this.getPayDate() == null || this.getPayDate().equals("null")) {
            	pstmt.setNull(23, 93);
            } else {
            	pstmt.setDate(23, Date.valueOf(this.getPayDate()));
            }
            if(this.getPayType() == null || this.getPayType().equals("null")) {
            	pstmt.setNull(24, 12);
            } else {
            	pstmt.setString(24, this.getPayType());
            }
            if(this.getEnterAccDate() == null || this.getEnterAccDate().equals("null")) {
            	pstmt.setNull(25, 93);
            } else {
            	pstmt.setDate(25, Date.valueOf(this.getEnterAccDate()));
            }
            if(this.getConfDate() == null || this.getConfDate().equals("null")) {
            	pstmt.setNull(26, 93);
            } else {
            	pstmt.setDate(26, Date.valueOf(this.getConfDate()));
            }
            if(this.getLastPayToDate() == null || this.getLastPayToDate().equals("null")) {
            	pstmt.setNull(27, 93);
            } else {
            	pstmt.setDate(27, Date.valueOf(this.getLastPayToDate()));
            }
            if(this.getCurPayToDate() == null || this.getCurPayToDate().equals("null")) {
            	pstmt.setNull(28, 93);
            } else {
            	pstmt.setDate(28, Date.valueOf(this.getCurPayToDate()));
            }
            if(this.getInInsuAccState() == null || this.getInInsuAccState().equals("null")) {
            	pstmt.setNull(29, 12);
            } else {
            	pstmt.setString(29, this.getInInsuAccState());
            }
            if(this.getApproveCode() == null || this.getApproveCode().equals("null")) {
            	pstmt.setNull(30, 12);
            } else {
            	pstmt.setString(30, this.getApproveCode());
            }
            if(this.getApproveDate() == null || this.getApproveDate().equals("null")) {
            	pstmt.setNull(31, 93);
            } else {
            	pstmt.setDate(31, Date.valueOf(this.getApproveDate()));
            }
            if(this.getApproveTime() == null || this.getApproveTime().equals("null")) {
            	pstmt.setNull(32, 12);
            } else {
            	pstmt.setString(32, this.getApproveTime());
            }
            if(this.getSerialNo() == null || this.getSerialNo().equals("null")) {
            	pstmt.setNull(33, 12);
            } else {
            	pstmt.setString(33, this.getSerialNo());
            }
            if(this.getOperator() == null || this.getOperator().equals("null")) {
            	pstmt.setNull(34, 12);
            } else {
            	pstmt.setString(34, this.getOperator());
            }
            if(this.getMakeDate() == null || this.getMakeDate().equals("null")) {
            	pstmt.setNull(35, 93);
            } else {
            	pstmt.setDate(35, Date.valueOf(this.getMakeDate()));
            }
            if(this.getMakeTime() == null || this.getMakeTime().equals("null")) {
            	pstmt.setNull(36, 12);
            } else {
            	pstmt.setString(36, this.getMakeTime());
            }
            if(this.getGetNoticeNo() == null || this.getGetNoticeNo().equals("null")) {
            	pstmt.setNull(37, 12);
            } else {
            	pstmt.setString(37, this.getGetNoticeNo());
            }
            if(this.getModifyDate() == null || this.getModifyDate().equals("null")) {
            	pstmt.setNull(38, 93);
            } else {
            	pstmt.setDate(38, Date.valueOf(this.getModifyDate()));
            }
            if(this.getModifyTime() == null || this.getModifyTime().equals("null")) {
            	pstmt.setNull(39, 12);
            } else {
            	pstmt.setString(39, this.getModifyTime());
            }
            if(this.getEdorNo() == null || this.getEdorNo().equals("null")) {
            	pstmt.setNull(40, 12);
            } else {
            	pstmt.setString(40, this.getEdorNo());
            }
            pstmt.setInt(41, this.getMainPolYear());
            if(this.getBackUpSerialNo() == null || this.getBackUpSerialNo().equals("null")) {
            	pstmt.setNull(42, 12);
            } else {
            	pstmt.setString(42, this.getBackUpSerialNo());
            }
            if(this.getBackUpReason() == null || this.getBackUpReason().equals("null")) {
            	pstmt.setNull(43, 12);
            } else {
            	pstmt.setString(43, this.getBackUpReason());
            }
            // set where condition
            pstmt.setLong(44, this.getAPayPersonID());
            pstmt.executeUpdate();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LBAPayPersonDB";
            tError.functionName = "update()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);
            ex.printStackTrace();

            try {
                pstmt.close();
            } catch (Exception e){}

            if( !mflag ) {
                try {
                    con.close();
                } catch (Exception e){}
            }

            return false;
        }

        if(!mflag) {
            try {
                con.close();
            } catch (Exception e){}
        }

        return true;
    }

    public boolean insert() {
        PreparedStatement pstmt = null;

        SQLString sqlObj = new SQLString("LBAPayPerson");
        sqlObj.setSQL(1, this);
        String sql = sqlObj.getSQL();

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try {
            pstmt = con.prepareStatement("INSERT INTO LBAPayPerson VALUES( ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ?)");
            pstmt.setLong(1, this.getAPayPersonID());
            if(this.getShardingID() == null || this.getShardingID().equals("null")) {
            	pstmt.setNull(2, 12);
            } else {
            	pstmt.setString(2, this.getShardingID());
            }
            if(this.getPolNo() == null || this.getPolNo().equals("null")) {
            	pstmt.setNull(3, 12);
            } else {
            	pstmt.setString(3, this.getPolNo());
            }
            pstmt.setInt(4, this.getPayCount());
            if(this.getGrpContNo() == null || this.getGrpContNo().equals("null")) {
            	pstmt.setNull(5, 12);
            } else {
            	pstmt.setString(5, this.getGrpContNo());
            }
            if(this.getGrpPolNo() == null || this.getGrpPolNo().equals("null")) {
            	pstmt.setNull(6, 12);
            } else {
            	pstmt.setString(6, this.getGrpPolNo());
            }
            if(this.getContNo() == null || this.getContNo().equals("null")) {
            	pstmt.setNull(7, 12);
            } else {
            	pstmt.setString(7, this.getContNo());
            }
            if(this.getManageCom() == null || this.getManageCom().equals("null")) {
            	pstmt.setNull(8, 12);
            } else {
            	pstmt.setString(8, this.getManageCom());
            }
            if(this.getAgentCom() == null || this.getAgentCom().equals("null")) {
            	pstmt.setNull(9, 12);
            } else {
            	pstmt.setString(9, this.getAgentCom());
            }
            if(this.getAgentType() == null || this.getAgentType().equals("null")) {
            	pstmt.setNull(10, 12);
            } else {
            	pstmt.setString(10, this.getAgentType());
            }
            if(this.getRiskCode() == null || this.getRiskCode().equals("null")) {
            	pstmt.setNull(11, 12);
            } else {
            	pstmt.setString(11, this.getRiskCode());
            }
            if(this.getAgentCode() == null || this.getAgentCode().equals("null")) {
            	pstmt.setNull(12, 12);
            } else {
            	pstmt.setString(12, this.getAgentCode());
            }
            if(this.getAgentGroup() == null || this.getAgentGroup().equals("null")) {
            	pstmt.setNull(13, 12);
            } else {
            	pstmt.setString(13, this.getAgentGroup());
            }
            if(this.getPayTypeFlag() == null || this.getPayTypeFlag().equals("null")) {
            	pstmt.setNull(14, 12);
            } else {
            	pstmt.setString(14, this.getPayTypeFlag());
            }
            if(this.getAppntNo() == null || this.getAppntNo().equals("null")) {
            	pstmt.setNull(15, 12);
            } else {
            	pstmt.setString(15, this.getAppntNo());
            }
            if(this.getPayNo() == null || this.getPayNo().equals("null")) {
            	pstmt.setNull(16, 12);
            } else {
            	pstmt.setString(16, this.getPayNo());
            }
            if(this.getPayAimClass() == null || this.getPayAimClass().equals("null")) {
            	pstmt.setNull(17, 12);
            } else {
            	pstmt.setString(17, this.getPayAimClass());
            }
            if(this.getDutyCode() == null || this.getDutyCode().equals("null")) {
            	pstmt.setNull(18, 12);
            } else {
            	pstmt.setString(18, this.getDutyCode());
            }
            if(this.getPayPlanCode() == null || this.getPayPlanCode().equals("null")) {
            	pstmt.setNull(19, 12);
            } else {
            	pstmt.setString(19, this.getPayPlanCode());
            }
            pstmt.setDouble(20, this.getSumDuePayMoney());
            pstmt.setDouble(21, this.getSumActuPayMoney());
            pstmt.setInt(22, this.getPayIntv());
            if(this.getPayDate() == null || this.getPayDate().equals("null")) {
            	pstmt.setNull(23, 93);
            } else {
            	pstmt.setDate(23, Date.valueOf(this.getPayDate()));
            }
            if(this.getPayType() == null || this.getPayType().equals("null")) {
            	pstmt.setNull(24, 12);
            } else {
            	pstmt.setString(24, this.getPayType());
            }
            if(this.getEnterAccDate() == null || this.getEnterAccDate().equals("null")) {
            	pstmt.setNull(25, 93);
            } else {
            	pstmt.setDate(25, Date.valueOf(this.getEnterAccDate()));
            }
            if(this.getConfDate() == null || this.getConfDate().equals("null")) {
            	pstmt.setNull(26, 93);
            } else {
            	pstmt.setDate(26, Date.valueOf(this.getConfDate()));
            }
            if(this.getLastPayToDate() == null || this.getLastPayToDate().equals("null")) {
            	pstmt.setNull(27, 93);
            } else {
            	pstmt.setDate(27, Date.valueOf(this.getLastPayToDate()));
            }
            if(this.getCurPayToDate() == null || this.getCurPayToDate().equals("null")) {
            	pstmt.setNull(28, 93);
            } else {
            	pstmt.setDate(28, Date.valueOf(this.getCurPayToDate()));
            }
            if(this.getInInsuAccState() == null || this.getInInsuAccState().equals("null")) {
            	pstmt.setNull(29, 12);
            } else {
            	pstmt.setString(29, this.getInInsuAccState());
            }
            if(this.getApproveCode() == null || this.getApproveCode().equals("null")) {
            	pstmt.setNull(30, 12);
            } else {
            	pstmt.setString(30, this.getApproveCode());
            }
            if(this.getApproveDate() == null || this.getApproveDate().equals("null")) {
            	pstmt.setNull(31, 93);
            } else {
            	pstmt.setDate(31, Date.valueOf(this.getApproveDate()));
            }
            if(this.getApproveTime() == null || this.getApproveTime().equals("null")) {
            	pstmt.setNull(32, 12);
            } else {
            	pstmt.setString(32, this.getApproveTime());
            }
            if(this.getSerialNo() == null || this.getSerialNo().equals("null")) {
            	pstmt.setNull(33, 12);
            } else {
            	pstmt.setString(33, this.getSerialNo());
            }
            if(this.getOperator() == null || this.getOperator().equals("null")) {
            	pstmt.setNull(34, 12);
            } else {
            	pstmt.setString(34, this.getOperator());
            }
            if(this.getMakeDate() == null || this.getMakeDate().equals("null")) {
            	pstmt.setNull(35, 93);
            } else {
            	pstmt.setDate(35, Date.valueOf(this.getMakeDate()));
            }
            if(this.getMakeTime() == null || this.getMakeTime().equals("null")) {
            	pstmt.setNull(36, 12);
            } else {
            	pstmt.setString(36, this.getMakeTime());
            }
            if(this.getGetNoticeNo() == null || this.getGetNoticeNo().equals("null")) {
            	pstmt.setNull(37, 12);
            } else {
            	pstmt.setString(37, this.getGetNoticeNo());
            }
            if(this.getModifyDate() == null || this.getModifyDate().equals("null")) {
            	pstmt.setNull(38, 93);
            } else {
            	pstmt.setDate(38, Date.valueOf(this.getModifyDate()));
            }
            if(this.getModifyTime() == null || this.getModifyTime().equals("null")) {
            	pstmt.setNull(39, 12);
            } else {
            	pstmt.setString(39, this.getModifyTime());
            }
            if(this.getEdorNo() == null || this.getEdorNo().equals("null")) {
            	pstmt.setNull(40, 12);
            } else {
            	pstmt.setString(40, this.getEdorNo());
            }
            pstmt.setInt(41, this.getMainPolYear());
            if(this.getBackUpSerialNo() == null || this.getBackUpSerialNo().equals("null")) {
            	pstmt.setNull(42, 12);
            } else {
            	pstmt.setString(42, this.getBackUpSerialNo());
            }
            if(this.getBackUpReason() == null || this.getBackUpReason().equals("null")) {
            	pstmt.setNull(43, 12);
            } else {
            	pstmt.setString(43, this.getBackUpReason());
            }
            // execute sql
            pstmt.executeUpdate();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LBAPayPersonDB";
            tError.functionName = "insert()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors.addOneError(tError);
            ex.printStackTrace();

            try {
                pstmt.close();
            } catch (Exception e){}

            if( !mflag ) {
                try {
                    con.close();
                } catch (Exception e){}
            }

            return false;
        }

        if(!mflag) {
            try {
                con.close();
            } catch (Exception e){}
        }

        return true;
    }

    public boolean getInfo() {
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        if(!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            pstmt = con.prepareStatement("SELECT * FROM LBAPayPerson WHERE  1=1  AND APayPersonID = ?", 
                ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
            pstmt.setLong(1, this.getAPayPersonID());
            rs = pstmt.executeQuery();
            int i = 0;
            while (rs.next()) {
                i++;
                if (!this.setSchema(rs,i)) {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "LBAPayPersonDB";
                    tError.functionName = "getInfo";
                    tError.errorMessage = "取数失败!";
                    this.mErrors .addOneError(tError);

                    try{ rs.close(); } catch( Exception ex ) {}
                    try{ pstmt.close(); } catch( Exception ex1 ) {}

                    if (!mflag) {
                        try {
                            con.close();
                        }
                        catch(Exception et){}
                    }
                    return false;
                }
                break;
            }
            try{ rs.close(); } catch( Exception ex2 ) {}
            try{ pstmt.close(); } catch( Exception ex3 ) {}

            if( i == 0 ) {
                if (!mflag) {
                    try {
                        con.close();
                    }
                    catch(Exception et){}
                }
                return false;
            }
        }
        catch(Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LBAPayPersonDB";
            tError.functionName = "getInfo";
            tError.errorMessage = ExceptionUtils.exceptionToString(e);
            this.mErrors .addOneError(tError);

            try{ rs.close(); } catch( Exception ex ) {}
            try{ pstmt.close(); } catch( Exception ex1 ) {}

            if (!mflag) {
                try {
                    con.close();
                }
                catch(Exception et){}
            }
            return false;
        }
        // 断开数据库连接
        if (!mflag) {
            try {
                con.close();
            }
            catch(Exception e){}
        }

        return true;
    }

    public LBAPayPersonSet query() {
        Statement stmt = null;
        ResultSet rs = null;
        LBAPayPersonSet aLBAPayPersonSet = new LBAPayPersonSet();

        if (!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            stmt = con.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
            SQLString sqlObj = new SQLString("LBAPayPerson");
            LBAPayPersonSchema aSchema = this.getSchema();
            sqlObj.setSQL(5, aSchema);
            String sql = sqlObj.getSQL();

            rs = stmt.executeQuery(sql);
            int i = 0;
            while (rs.next()) {
                i++;
                if (i>10000) {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "LBAPayPersonDB";
                    tError.functionName = "query";
                    tError.errorMessage = "SQL语句返回结果集太多，请换用其他方式!";
                    this.mErrors .addOneError(tError);
                        break;
                }
                LBAPayPersonSchema s1 = new LBAPayPersonSchema();
                s1.setSchema(rs,i);
                aLBAPayPersonSet.add(s1);
            }
            try{ rs.close(); } catch( Exception ex ) {}
            try{ stmt.close(); } catch( Exception ex1 ) {}
        }
        catch(Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LBAPayPersonDB";
            tError.functionName = "query";
            tError.errorMessage = ExceptionUtils.exceptionToString(e);
            this.mErrors.addOneError(tError);
            e.printStackTrace();

            try { rs.close(); } catch (Exception ex2) { ex2.printStackTrace(); }
            try { stmt.close(); } catch(Exception ex3) { ex3.printStackTrace(); }

            if (!mflag) {
                try {
                    con.close();
                }
                catch(Exception et){et.printStackTrace();}
            }
        }

        if (!mflag) {
            try {
                con.close();
            }
            catch(Exception e){e.printStackTrace();}
        }

        return aLBAPayPersonSet;
    }

    public LBAPayPersonSet executeQuery(String sql)  {
        Statement stmt = null;
        ResultSet rs = null;
        LBAPayPersonSet aLBAPayPersonSet = new LBAPayPersonSet();

        if (!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            stmt = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);

            rs = stmt.executeQuery(StrTool.GBKToUnicode(sql));
            int i = 0;
            while (rs.next()) {
                i++;
                if (i>10000) {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "LBAPayPersonDB";
                    tError.functionName = "executeQuery";
                    tError.errorMessage = "SQL语句返回结果集太多，请换用其他方式!";
                    this.mErrors .addOneError(tError);
                        break;
                }
                LBAPayPersonSchema s1 = new LBAPayPersonSchema();
                if (!s1.setSchema(rs,i)) {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "LBAPayPersonDB";
                    tError.functionName = "executeQuery";
                    tError.errorMessage = "sql语句有误，请查看表名及字段名信息!";
                    this.mErrors .addOneError(tError);
                }
                aLBAPayPersonSet.add(s1);
            }
            try{ rs.close(); } catch( Exception ex ) {}
            try{ stmt.close(); } catch( Exception ex1 ) {}
        }
        catch(Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LBAPayPersonDB";
            tError.functionName = "executeQuery";
            tError.errorMessage = ExceptionUtils.exceptionToString(e);
            this.mErrors .addOneError(tError);

            try{ rs.close(); } catch( Exception ex2 ) {}
            try{ stmt.close(); } catch( Exception ex3 ) {}

            if (!mflag) {
                try {
                    con.close();
                }
                catch(Exception et){}
            }
        }

        if (!mflag) {
            try {
                con.close();
            }
            catch(Exception e){}
        }

        return aLBAPayPersonSet;
    }

    public LBAPayPersonSet query(int nStart, int nCount) {
        Statement stmt = null;
        ResultSet rs = null;
        LBAPayPersonSet aLBAPayPersonSet = new LBAPayPersonSet();

        if (!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            stmt = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);
            SQLString sqlObj = new SQLString("LBAPayPerson");
            LBAPayPersonSchema aSchema = this.getSchema();
            sqlObj.setSQL(5,aSchema);
            String sql = sqlObj.getSQL();

            rs = stmt.executeQuery(sql);
            int i = 0;
            while (rs.next()) {
                i++;

                if( i < nStart ) {
                    continue;
                }

                if( i >= nStart + nCount ) {
                    break;
                }

                LBAPayPersonSchema s1 = new LBAPayPersonSchema();
                s1.setSchema(rs,i);
                aLBAPayPersonSet.add(s1);
            }
            try{ rs.close(); } catch( Exception ex ) {}
            try{ stmt.close(); } catch( Exception ex1 ) {}
        }
        catch(Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LBAPayPersonDB";
            tError.functionName = "query";
            tError.errorMessage = ExceptionUtils.exceptionToString(e);
            this.mErrors .addOneError(tError);

            try{ rs.close(); } catch( Exception ex2 ) {}
            try{ stmt.close(); } catch( Exception ex3 ) {}

            if (!mflag) {
                try {
                    con.close();
                }
                catch(Exception et){}
            }
        }

        if (!mflag) {
            try {
                con.close();
            }
            catch(Exception e){}
        }

        return aLBAPayPersonSet;
    }

    public LBAPayPersonSet executeQuery(String sql, int nStart, int nCount) {
        Statement stmt = null;
        ResultSet rs = null;
        LBAPayPersonSet aLBAPayPersonSet = new LBAPayPersonSet();

        if (!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            stmt = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);

            rs = stmt.executeQuery(StrTool.GBKToUnicode(sql));
            int i = 0;
            while (rs.next()) {
                i++;

                if( i < nStart ) {
                    continue;
                }

                if( i >= nStart + nCount ) {
                    break;
                }

                LBAPayPersonSchema s1 = new LBAPayPersonSchema();
                if (!s1.setSchema(rs,i)) {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "LBAPayPersonDB";
                    tError.functionName = "executeQuery";
                    tError.errorMessage = "sql语句有误，请查看表名及字段名信息!";
                    this.mErrors .addOneError(tError);
                }
                aLBAPayPersonSet.add(s1);
            }
            try{ rs.close(); } catch( Exception ex ) {}
            try{ stmt.close(); } catch( Exception ex1 ) {}
        }
        catch(Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LBAPayPersonDB";
            tError.functionName = "executeQuery";
            tError.errorMessage = ExceptionUtils.exceptionToString(e);
            this.mErrors .addOneError(tError);

            try{ rs.close(); } catch( Exception ex2 ) {}
            try{ stmt.close(); } catch( Exception ex3 ) {}

            if (!mflag) {
                try {
                    con.close();
                }
                catch(Exception et){}
            }
        }

        if (!mflag) {
            try {
                con.close();
            }
            catch(Exception e){}
        }

        return aLBAPayPersonSet;
    }

    public boolean update(String strWherePart) {
        Statement stmt = null;

        if (!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            stmt = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);
            SQLString sqlObj = new SQLString("LBAPayPerson");
            LBAPayPersonSchema aSchema = this.getSchema();
            sqlObj.setSQL(2,aSchema);
            String sql = "update LBAPayPerson " + sqlObj.getUpdPart() + " where " + strWherePart;

            int operCount = stmt.executeUpdate(sql);
            if (operCount == 0) {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "LBAPayPersonDB";
                tError.functionName = "update";
                tError.errorMessage = "更新数据失败!";
                this.mErrors .addOneError(tError);

                if (!mflag) {
                    try {
                        con.close();
                    }
                    catch(Exception et){}
                }
                return false;
            }
        }
        catch(Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LBAPayPersonDB";
            tError.functionName = "update";
            tError.errorMessage = ExceptionUtils.exceptionToString(e);
            this.mErrors .addOneError(tError);

            try{ stmt.close(); } catch( Exception ex1 ) {}

            if (!mflag) {
                try {
                    con.close();
                }
                catch(Exception et){}
            }
            return false;
        }
        // 断开数据库连接
        if (!mflag) {
            try {
                con.close();
            }
            catch(Exception e){}
        }

        return true;
    }

    /**
     * 准备数据查询条件
     * @param strSQL String
     * @return boolean
     */
    public boolean prepareData(String strSQL) {
        if (mResultSet != null) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LBAPayPersonDB";
            tError.functionName = "prepareData";
            tError.errorMessage = "数据集非空，程序在准备数据集之后，没有关闭！";
            this.mErrors.addOneError(tError);
            return false;
        }

        if (!mflag) {
            con = DBConnPool.getConnection();
        }
        try {
            mStatement = con.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
            mResultSet = mStatement.executeQuery(StrTool.GBKToUnicode(strSQL));
        }
        catch (Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LBAPayPersonDB";
            tError.functionName = "prepareData";
            tError.errorMessage = ExceptionUtils.exceptionToString(e);
            this.mErrors.addOneError(tError);
            try {
                mResultSet.close();
            }
            catch (Exception ex2) {}
            try {
                mStatement.close();
            }
            catch (Exception ex3) {}
            if (!mflag) {
                try {
                    con.close();
                }
                catch (Exception et) {}
            }
            return false;
        }

        if (!mflag) {
            try {
                con.close();
            }
            catch (Exception e) {}
        }
        return true;
    }

    /**
     * 获取数据集
     * @return boolean
     */
    public boolean hasMoreData() {
        boolean flag = true;
        if (null == mResultSet) {
            CError tError = new CError();
            tError.moduleName = "LBAPayPersonDB";
            tError.functionName = "hasMoreData";
            tError.errorMessage = "数据集为空，请先准备数据集！";
            this.mErrors.addOneError(tError);
            return false;
        }
        try {
            flag = mResultSet.next();
        }
        catch (Exception ex) {
            CError tError = new CError();
            tError.moduleName = "LBAPayPersonDB";
            tError.functionName = "hasMoreData";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors.addOneError(tError);
            try {
                mResultSet.close();
                mResultSet = null;
            }
            catch (Exception ex2) {}
            try {
                mStatement.close();
                mStatement = null;
            }
            catch (Exception ex3) {}
            if (!mflag) {
                try {
                    con.close();
                }
                catch (Exception et) {}
            }
            return false;
        }
        return flag;
    }
    /**
     * 获取定量数据
     * @return LBAPayPersonSet
     */
    public LBAPayPersonSet getData() {
        int tCount = 0;
        LBAPayPersonSet tLBAPayPersonSet = new LBAPayPersonSet();
        LBAPayPersonSchema tLBAPayPersonSchema = null;
        if (null == mResultSet) {
            CError tError = new CError();
            tError.moduleName = "LBAPayPersonDB";
            tError.functionName = "getData";
            tError.errorMessage = "数据集为空，请先准备数据集！";
            this.mErrors.addOneError(tError);
            return null;
        }
        try {
            tCount = 1;
            tLBAPayPersonSchema = new LBAPayPersonSchema();
            tLBAPayPersonSchema.setSchema(mResultSet, 1);
            tLBAPayPersonSet.add(tLBAPayPersonSchema);
            //注意mResultSet.next()的作用
            while (tCount++ < SysConst.FETCHCOUNT) {
                if (mResultSet.next()) {
                    tLBAPayPersonSchema = new LBAPayPersonSchema();
                    tLBAPayPersonSchema.setSchema(mResultSet, 1);
                    tLBAPayPersonSet.add(tLBAPayPersonSchema);
                }
            }
        }
        catch (Exception ex) {
            CError tError = new CError();
            tError.moduleName = "LBAPayPersonDB";
            tError.functionName = "getData";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors.addOneError(tError);
            try {
                mResultSet.close();
                mResultSet = null;
            }
            catch (Exception ex2) {}
            try {
                mStatement.close();
                mStatement = null;
            }
            catch (Exception ex3) {}
            if (!mflag) {
                try {
                    con.close();
                }
                catch (Exception et) {}
            }
            return null;
        }
        return tLBAPayPersonSet;
    }
    /**
     * 关闭数据集
     * @return boolean
     */
    public boolean closeData() {
        boolean flag = true;
        try {
            if (null == mResultSet) {
                CError tError = new CError();
                tError.moduleName = "LBAPayPersonDB";
                tError.functionName = "closeData";
                tError.errorMessage = "数据集已经关闭了！";
                this.mErrors.addOneError(tError);
                flag = false;
            } else {
                mResultSet.close();
                mResultSet = null;
            }
        }
        catch (Exception ex2) {
            CError tError = new CError();
            tError.moduleName = "LBAPayPersonDB";
            tError.functionName = "closeData";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex2);
            this.mErrors.addOneError(tError);
            flag = false;
        }
        try {
            if (null == mStatement) {
                CError tError = new CError();
                tError.moduleName = "LBAPayPersonDB";
                tError.functionName = "closeData";
                tError.errorMessage = "语句已经关闭了！";
                this.mErrors.addOneError(tError);
                flag = false;
            } else {
                mStatement.close();
                mStatement = null;
            }
        }
        catch (Exception ex3) {
            CError tError = new CError();
            tError.moduleName = "LBAPayPersonDB";
            tError.functionName = "closeData";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex3);
            this.mErrors.addOneError(tError);
            flag = false;
        }
        return flag;
    }
}
