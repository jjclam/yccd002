/**
 * Copyright (c) 2017 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import com.sinosoft.lis.db.LJSPayPersonDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.*;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

/**
 * <p>ClassName: LJSPayPersonSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2017</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2017-10-21
 */
public class LJSPayPersonSchema implements Schema, Cloneable {
    // @Field
    /** Id */
    private long SPayPersonID;
    /** Fk_ljspay */
    private long SPayID;
    /** Shardingid */
    private String ShardingID;
    /** 保单号码 */
    private String PolNo;
    /** 第几次交费 */
    private int PayCount;
    /** 集体合同号码 */
    private String GrpContNo;
    /** 集体保单号码 */
    private String GrpPolNo;
    /** 合同号码 */
    private String ContNo;
    /** 管理机构 */
    private String ManageCom;
    /** 代理机构 */
    private String AgentCom;
    /** 代理机构内部分类 */
    private String AgentType;
    /** 险种编码 */
    private String RiskCode;
    /** 代理人编码 */
    private String AgentCode;
    /** 代理人组别 */
    private String AgentGroup;
    /** 续保收费标记 */
    private String PayTypeFlag;
    /** 投保人客户号码 */
    private String AppntNo;
    /** 通知书号码 */
    private String GetNoticeNo;
    /** 交费目的分类 */
    private String PayAimClass;
    /** 责任编码 */
    private String DutyCode;
    /** 交费计划编码 */
    private String PayPlanCode;
    /** 总应交金额 */
    private double SumDuePayMoney;
    /** 总实交金额 */
    private double SumActuPayMoney;
    /** 交费间隔 */
    private int PayIntv;
    /** 交费日期 */
    private Date PayDate;
    /** 交费类型 */
    private String PayType;
    /** 原交至日期 */
    private Date LastPayToDate;
    /** 现交至日期 */
    private Date CurPayToDate;
    /** 转入保险帐户状态 */
    private String InInsuAccState;
    /** 银行编码 */
    private String BankCode;
    /** 银行帐号 */
    private String BankAccNo;
    /** 银行在途标志 */
    private String BankOnTheWayFlag;
    /** 银行转帐成功标记 */
    private String BankSuccFlag;
    /** 复核人编码 */
    private String ApproveCode;
    /** 复核日期 */
    private Date ApproveDate;
    /** 复核时间 */
    private String ApproveTime;
    /** 流水号 */
    private String SerialNo;
    /** 录入标志 */
    private String InputFlag;
    /** 操作员 */
    private String Operator;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 最后一次修改日期 */
    private Date ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime;
    /** 批单号 */
    private String EdorNo;
    /** 主险保单年度 */
    private int MainPolYear;
    /** 其他号码 */
    private String OtherNo;
    /** 其他号码类型 */
    private String OtherNoType;
    /** 印刷号码 */
    private String PrtNo;
    /** 销售渠道 */
    private String SaleChnl;
    /** 收费机构 */
    private String ChargeCom;
    /** 投保人姓名 */
    private String APPntName;
    /** 险类编码 */
    private String KindCode;
    /** 缴至日期 */
    private Date PaytoDate;
    /** 终交日期 */
    private Date PayEndDate;
    /** 签单日期 */
    private Date SignDate;
    /** 生效日期 */
    private Date CValiDate;
    /** 终交年龄年期 */
    private int PayEndYear;
    /** 缴费金额 */
    private double PayMoney;

    public static final int FIELDNUM = 57;    // 数据库表的字段个数

    private static String[] PK;                // 主键

    private FDate fDate = new FDate();        // 处理日期

    public CErrors mErrors;            // 错误信息

    // @Constructor
    public LJSPayPersonSchema() {
        mErrors = new CErrors();

        String[] pk = new String[1];
        pk[0] = "SPayPersonID";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException {
        LJSPayPersonSchema cloned = (LJSPayPersonSchema)super.clone();
        cloned.fDate = (FDate) fDate.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK() {
        return PK;
    }

    public long getSPayPersonID() {
        return SPayPersonID;
    }
    public void setSPayPersonID(long aSPayPersonID) {
        SPayPersonID = aSPayPersonID;
    }
    public void setSPayPersonID(String aSPayPersonID) {
        if (aSPayPersonID != null && !aSPayPersonID.equals("")) {
            SPayPersonID = new Long(aSPayPersonID).longValue();
        }
    }

    public long getSPayID() {
        return SPayID;
    }
    public void setSPayID(long aSPayID) {
        SPayID = aSPayID;
    }
    public void setSPayID(String aSPayID) {
        if (aSPayID != null && !aSPayID.equals("")) {
            SPayID = new Long(aSPayID).longValue();
        }
    }

    public String getShardingID() {
        return ShardingID;
    }
    public void setShardingID(String aShardingID) {
        ShardingID = aShardingID;
    }
    public String getPolNo() {
        return PolNo;
    }
    public void setPolNo(String aPolNo) {
        PolNo = aPolNo;
    }
    public int getPayCount() {
        return PayCount;
    }
    public void setPayCount(int aPayCount) {
        PayCount = aPayCount;
    }
    public void setPayCount(String aPayCount) {
        if (aPayCount != null && !aPayCount.equals("")) {
            Integer tInteger = new Integer(aPayCount);
            int i = tInteger.intValue();
            PayCount = i;
        }
    }

    public String getGrpContNo() {
        return GrpContNo;
    }
    public void setGrpContNo(String aGrpContNo) {
        GrpContNo = aGrpContNo;
    }
    public String getGrpPolNo() {
        return GrpPolNo;
    }
    public void setGrpPolNo(String aGrpPolNo) {
        GrpPolNo = aGrpPolNo;
    }
    public String getContNo() {
        return ContNo;
    }
    public void setContNo(String aContNo) {
        ContNo = aContNo;
    }
    public String getManageCom() {
        return ManageCom;
    }
    public void setManageCom(String aManageCom) {
        ManageCom = aManageCom;
    }
    public String getAgentCom() {
        return AgentCom;
    }
    public void setAgentCom(String aAgentCom) {
        AgentCom = aAgentCom;
    }
    public String getAgentType() {
        return AgentType;
    }
    public void setAgentType(String aAgentType) {
        AgentType = aAgentType;
    }
    public String getRiskCode() {
        return RiskCode;
    }
    public void setRiskCode(String aRiskCode) {
        RiskCode = aRiskCode;
    }
    public String getAgentCode() {
        return AgentCode;
    }
    public void setAgentCode(String aAgentCode) {
        AgentCode = aAgentCode;
    }
    public String getAgentGroup() {
        return AgentGroup;
    }
    public void setAgentGroup(String aAgentGroup) {
        AgentGroup = aAgentGroup;
    }
    public String getPayTypeFlag() {
        return PayTypeFlag;
    }
    public void setPayTypeFlag(String aPayTypeFlag) {
        PayTypeFlag = aPayTypeFlag;
    }
    public String getAppntNo() {
        return AppntNo;
    }
    public void setAppntNo(String aAppntNo) {
        AppntNo = aAppntNo;
    }
    public String getGetNoticeNo() {
        return GetNoticeNo;
    }
    public void setGetNoticeNo(String aGetNoticeNo) {
        GetNoticeNo = aGetNoticeNo;
    }
    public String getPayAimClass() {
        return PayAimClass;
    }
    public void setPayAimClass(String aPayAimClass) {
        PayAimClass = aPayAimClass;
    }
    public String getDutyCode() {
        return DutyCode;
    }
    public void setDutyCode(String aDutyCode) {
        DutyCode = aDutyCode;
    }
    public String getPayPlanCode() {
        return PayPlanCode;
    }
    public void setPayPlanCode(String aPayPlanCode) {
        PayPlanCode = aPayPlanCode;
    }
    public double getSumDuePayMoney() {
        return SumDuePayMoney;
    }
    public void setSumDuePayMoney(double aSumDuePayMoney) {
        SumDuePayMoney = aSumDuePayMoney;
    }
    public void setSumDuePayMoney(String aSumDuePayMoney) {
        if (aSumDuePayMoney != null && !aSumDuePayMoney.equals("")) {
            Double tDouble = new Double(aSumDuePayMoney);
            double d = tDouble.doubleValue();
            SumDuePayMoney = d;
        }
    }

    public double getSumActuPayMoney() {
        return SumActuPayMoney;
    }
    public void setSumActuPayMoney(double aSumActuPayMoney) {
        SumActuPayMoney = aSumActuPayMoney;
    }
    public void setSumActuPayMoney(String aSumActuPayMoney) {
        if (aSumActuPayMoney != null && !aSumActuPayMoney.equals("")) {
            Double tDouble = new Double(aSumActuPayMoney);
            double d = tDouble.doubleValue();
            SumActuPayMoney = d;
        }
    }

    public int getPayIntv() {
        return PayIntv;
    }
    public void setPayIntv(int aPayIntv) {
        PayIntv = aPayIntv;
    }
    public void setPayIntv(String aPayIntv) {
        if (aPayIntv != null && !aPayIntv.equals("")) {
            Integer tInteger = new Integer(aPayIntv);
            int i = tInteger.intValue();
            PayIntv = i;
        }
    }

    public String getPayDate() {
        if(PayDate != null) {
            return fDate.getString(PayDate);
        } else {
            return null;
        }
    }
    public void setPayDate(Date aPayDate) {
        PayDate = aPayDate;
    }
    public void setPayDate(String aPayDate) {
        if (aPayDate != null && !aPayDate.equals("")) {
            PayDate = fDate.getDate(aPayDate);
        } else
            PayDate = null;
    }

    public String getPayType() {
        return PayType;
    }
    public void setPayType(String aPayType) {
        PayType = aPayType;
    }
    public String getLastPayToDate() {
        if(LastPayToDate != null) {
            return fDate.getString(LastPayToDate);
        } else {
            return null;
        }
    }
    public void setLastPayToDate(Date aLastPayToDate) {
        LastPayToDate = aLastPayToDate;
    }
    public void setLastPayToDate(String aLastPayToDate) {
        if (aLastPayToDate != null && !aLastPayToDate.equals("")) {
            LastPayToDate = fDate.getDate(aLastPayToDate);
        } else
            LastPayToDate = null;
    }

    public String getCurPayToDate() {
        if(CurPayToDate != null) {
            return fDate.getString(CurPayToDate);
        } else {
            return null;
        }
    }
    public void setCurPayToDate(Date aCurPayToDate) {
        CurPayToDate = aCurPayToDate;
    }
    public void setCurPayToDate(String aCurPayToDate) {
        if (aCurPayToDate != null && !aCurPayToDate.equals("")) {
            CurPayToDate = fDate.getDate(aCurPayToDate);
        } else
            CurPayToDate = null;
    }

    public String getInInsuAccState() {
        return InInsuAccState;
    }
    public void setInInsuAccState(String aInInsuAccState) {
        InInsuAccState = aInInsuAccState;
    }
    public String getBankCode() {
        return BankCode;
    }
    public void setBankCode(String aBankCode) {
        BankCode = aBankCode;
    }
    public String getBankAccNo() {
        return BankAccNo;
    }
    public void setBankAccNo(String aBankAccNo) {
        BankAccNo = aBankAccNo;
    }
    public String getBankOnTheWayFlag() {
        return BankOnTheWayFlag;
    }
    public void setBankOnTheWayFlag(String aBankOnTheWayFlag) {
        BankOnTheWayFlag = aBankOnTheWayFlag;
    }
    public String getBankSuccFlag() {
        return BankSuccFlag;
    }
    public void setBankSuccFlag(String aBankSuccFlag) {
        BankSuccFlag = aBankSuccFlag;
    }
    public String getApproveCode() {
        return ApproveCode;
    }
    public void setApproveCode(String aApproveCode) {
        ApproveCode = aApproveCode;
    }
    public String getApproveDate() {
        if(ApproveDate != null) {
            return fDate.getString(ApproveDate);
        } else {
            return null;
        }
    }
    public void setApproveDate(Date aApproveDate) {
        ApproveDate = aApproveDate;
    }
    public void setApproveDate(String aApproveDate) {
        if (aApproveDate != null && !aApproveDate.equals("")) {
            ApproveDate = fDate.getDate(aApproveDate);
        } else
            ApproveDate = null;
    }

    public String getApproveTime() {
        return ApproveTime;
    }
    public void setApproveTime(String aApproveTime) {
        ApproveTime = aApproveTime;
    }
    public String getSerialNo() {
        return SerialNo;
    }
    public void setSerialNo(String aSerialNo) {
        SerialNo = aSerialNo;
    }
    public String getInputFlag() {
        return InputFlag;
    }
    public void setInputFlag(String aInputFlag) {
        InputFlag = aInputFlag;
    }
    public String getOperator() {
        return Operator;
    }
    public void setOperator(String aOperator) {
        Operator = aOperator;
    }
    public String getMakeDate() {
        if(MakeDate != null) {
            return fDate.getString(MakeDate);
        } else {
            return null;
        }
    }
    public void setMakeDate(Date aMakeDate) {
        MakeDate = aMakeDate;
    }
    public void setMakeDate(String aMakeDate) {
        if (aMakeDate != null && !aMakeDate.equals("")) {
            MakeDate = fDate.getDate(aMakeDate);
        } else
            MakeDate = null;
    }

    public String getMakeTime() {
        return MakeTime;
    }
    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }
    public String getModifyDate() {
        if(ModifyDate != null) {
            return fDate.getString(ModifyDate);
        } else {
            return null;
        }
    }
    public void setModifyDate(Date aModifyDate) {
        ModifyDate = aModifyDate;
    }
    public void setModifyDate(String aModifyDate) {
        if (aModifyDate != null && !aModifyDate.equals("")) {
            ModifyDate = fDate.getDate(aModifyDate);
        } else
            ModifyDate = null;
    }

    public String getModifyTime() {
        return ModifyTime;
    }
    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }
    public String getEdorNo() {
        return EdorNo;
    }
    public void setEdorNo(String aEdorNo) {
        EdorNo = aEdorNo;
    }
    public int getMainPolYear() {
        return MainPolYear;
    }
    public void setMainPolYear(int aMainPolYear) {
        MainPolYear = aMainPolYear;
    }
    public void setMainPolYear(String aMainPolYear) {
        if (aMainPolYear != null && !aMainPolYear.equals("")) {
            Integer tInteger = new Integer(aMainPolYear);
            int i = tInteger.intValue();
            MainPolYear = i;
        }
    }

    public String getOtherNo() {
        return OtherNo;
    }
    public void setOtherNo(String aOtherNo) {
        OtherNo = aOtherNo;
    }
    public String getOtherNoType() {
        return OtherNoType;
    }
    public void setOtherNoType(String aOtherNoType) {
        OtherNoType = aOtherNoType;
    }
    public String getPrtNo() {
        return PrtNo;
    }
    public void setPrtNo(String aPrtNo) {
        PrtNo = aPrtNo;
    }
    public String getSaleChnl() {
        return SaleChnl;
    }
    public void setSaleChnl(String aSaleChnl) {
        SaleChnl = aSaleChnl;
    }
    public String getChargeCom() {
        return ChargeCom;
    }
    public void setChargeCom(String aChargeCom) {
        ChargeCom = aChargeCom;
    }
    public String getAPPntName() {
        return APPntName;
    }
    public void setAPPntName(String aAPPntName) {
        APPntName = aAPPntName;
    }
    public String getKindCode() {
        return KindCode;
    }
    public void setKindCode(String aKindCode) {
        KindCode = aKindCode;
    }
    public String getPaytoDate() {
        if(PaytoDate != null) {
            return fDate.getString(PaytoDate);
        } else {
            return null;
        }
    }
    public void setPaytoDate(Date aPaytoDate) {
        PaytoDate = aPaytoDate;
    }
    public void setPaytoDate(String aPaytoDate) {
        if (aPaytoDate != null && !aPaytoDate.equals("")) {
            PaytoDate = fDate.getDate(aPaytoDate);
        } else
            PaytoDate = null;
    }

    public String getPayEndDate() {
        if(PayEndDate != null) {
            return fDate.getString(PayEndDate);
        } else {
            return null;
        }
    }
    public void setPayEndDate(Date aPayEndDate) {
        PayEndDate = aPayEndDate;
    }
    public void setPayEndDate(String aPayEndDate) {
        if (aPayEndDate != null && !aPayEndDate.equals("")) {
            PayEndDate = fDate.getDate(aPayEndDate);
        } else
            PayEndDate = null;
    }

    public String getSignDate() {
        if(SignDate != null) {
            return fDate.getString(SignDate);
        } else {
            return null;
        }
    }
    public void setSignDate(Date aSignDate) {
        SignDate = aSignDate;
    }
    public void setSignDate(String aSignDate) {
        if (aSignDate != null && !aSignDate.equals("")) {
            SignDate = fDate.getDate(aSignDate);
        } else
            SignDate = null;
    }

    public String getCValiDate() {
        if(CValiDate != null) {
            return fDate.getString(CValiDate);
        } else {
            return null;
        }
    }
    public void setCValiDate(Date aCValiDate) {
        CValiDate = aCValiDate;
    }
    public void setCValiDate(String aCValiDate) {
        if (aCValiDate != null && !aCValiDate.equals("")) {
            CValiDate = fDate.getDate(aCValiDate);
        } else
            CValiDate = null;
    }

    public int getPayEndYear() {
        return PayEndYear;
    }
    public void setPayEndYear(int aPayEndYear) {
        PayEndYear = aPayEndYear;
    }
    public void setPayEndYear(String aPayEndYear) {
        if (aPayEndYear != null && !aPayEndYear.equals("")) {
            Integer tInteger = new Integer(aPayEndYear);
            int i = tInteger.intValue();
            PayEndYear = i;
        }
    }

    public double getPayMoney() {
        return PayMoney;
    }
    public void setPayMoney(double aPayMoney) {
        PayMoney = aPayMoney;
    }
    public void setPayMoney(String aPayMoney) {
        if (aPayMoney != null && !aPayMoney.equals("")) {
            Double tDouble = new Double(aPayMoney);
            double d = tDouble.doubleValue();
            PayMoney = d;
        }
    }


    /**
    * 使用另外一个 LJSPayPersonSchema 对象给 Schema 赋值
    * @param: aLJSPayPersonSchema LJSPayPersonSchema
    **/
    public void setSchema(LJSPayPersonSchema aLJSPayPersonSchema) {
        this.SPayPersonID = aLJSPayPersonSchema.getSPayPersonID();
        this.SPayID = aLJSPayPersonSchema.getSPayID();
        this.ShardingID = aLJSPayPersonSchema.getShardingID();
        this.PolNo = aLJSPayPersonSchema.getPolNo();
        this.PayCount = aLJSPayPersonSchema.getPayCount();
        this.GrpContNo = aLJSPayPersonSchema.getGrpContNo();
        this.GrpPolNo = aLJSPayPersonSchema.getGrpPolNo();
        this.ContNo = aLJSPayPersonSchema.getContNo();
        this.ManageCom = aLJSPayPersonSchema.getManageCom();
        this.AgentCom = aLJSPayPersonSchema.getAgentCom();
        this.AgentType = aLJSPayPersonSchema.getAgentType();
        this.RiskCode = aLJSPayPersonSchema.getRiskCode();
        this.AgentCode = aLJSPayPersonSchema.getAgentCode();
        this.AgentGroup = aLJSPayPersonSchema.getAgentGroup();
        this.PayTypeFlag = aLJSPayPersonSchema.getPayTypeFlag();
        this.AppntNo = aLJSPayPersonSchema.getAppntNo();
        this.GetNoticeNo = aLJSPayPersonSchema.getGetNoticeNo();
        this.PayAimClass = aLJSPayPersonSchema.getPayAimClass();
        this.DutyCode = aLJSPayPersonSchema.getDutyCode();
        this.PayPlanCode = aLJSPayPersonSchema.getPayPlanCode();
        this.SumDuePayMoney = aLJSPayPersonSchema.getSumDuePayMoney();
        this.SumActuPayMoney = aLJSPayPersonSchema.getSumActuPayMoney();
        this.PayIntv = aLJSPayPersonSchema.getPayIntv();
        this.PayDate = fDate.getDate( aLJSPayPersonSchema.getPayDate());
        this.PayType = aLJSPayPersonSchema.getPayType();
        this.LastPayToDate = fDate.getDate( aLJSPayPersonSchema.getLastPayToDate());
        this.CurPayToDate = fDate.getDate( aLJSPayPersonSchema.getCurPayToDate());
        this.InInsuAccState = aLJSPayPersonSchema.getInInsuAccState();
        this.BankCode = aLJSPayPersonSchema.getBankCode();
        this.BankAccNo = aLJSPayPersonSchema.getBankAccNo();
        this.BankOnTheWayFlag = aLJSPayPersonSchema.getBankOnTheWayFlag();
        this.BankSuccFlag = aLJSPayPersonSchema.getBankSuccFlag();
        this.ApproveCode = aLJSPayPersonSchema.getApproveCode();
        this.ApproveDate = fDate.getDate( aLJSPayPersonSchema.getApproveDate());
        this.ApproveTime = aLJSPayPersonSchema.getApproveTime();
        this.SerialNo = aLJSPayPersonSchema.getSerialNo();
        this.InputFlag = aLJSPayPersonSchema.getInputFlag();
        this.Operator = aLJSPayPersonSchema.getOperator();
        this.MakeDate = fDate.getDate( aLJSPayPersonSchema.getMakeDate());
        this.MakeTime = aLJSPayPersonSchema.getMakeTime();
        this.ModifyDate = fDate.getDate( aLJSPayPersonSchema.getModifyDate());
        this.ModifyTime = aLJSPayPersonSchema.getModifyTime();
        this.EdorNo = aLJSPayPersonSchema.getEdorNo();
        this.MainPolYear = aLJSPayPersonSchema.getMainPolYear();
        this.OtherNo = aLJSPayPersonSchema.getOtherNo();
        this.OtherNoType = aLJSPayPersonSchema.getOtherNoType();
        this.PrtNo = aLJSPayPersonSchema.getPrtNo();
        this.SaleChnl = aLJSPayPersonSchema.getSaleChnl();
        this.ChargeCom = aLJSPayPersonSchema.getChargeCom();
        this.APPntName = aLJSPayPersonSchema.getAPPntName();
        this.KindCode = aLJSPayPersonSchema.getKindCode();
        this.PaytoDate = fDate.getDate( aLJSPayPersonSchema.getPaytoDate());
        this.PayEndDate = fDate.getDate( aLJSPayPersonSchema.getPayEndDate());
        this.SignDate = fDate.getDate( aLJSPayPersonSchema.getSignDate());
        this.CValiDate = fDate.getDate( aLJSPayPersonSchema.getCValiDate());
        this.PayEndYear = aLJSPayPersonSchema.getPayEndYear();
        this.PayMoney = aLJSPayPersonSchema.getPayMoney();
    }

    /**
    * 使用 ResultSet 中的第 i 行给 Schema 赋值
    * @param: rs ResultSet
    * @param: i int
    * @return: boolean
    **/
    public boolean setSchema(ResultSet rs,int i) {
        try {
            //rs.absolute(i);        // 非滚动游标
            this.SPayPersonID = rs.getLong("SPayPersonID");
            this.SPayID = rs.getLong("SPayID");
            if( rs.getString("ShardingID") == null )
                this.ShardingID = null;
            else
                this.ShardingID = rs.getString("ShardingID").trim();

            if( rs.getString("PolNo") == null )
                this.PolNo = null;
            else
                this.PolNo = rs.getString("PolNo").trim();

            this.PayCount = rs.getInt("PayCount");
            if( rs.getString("GrpContNo") == null )
                this.GrpContNo = null;
            else
                this.GrpContNo = rs.getString("GrpContNo").trim();

            if( rs.getString("GrpPolNo") == null )
                this.GrpPolNo = null;
            else
                this.GrpPolNo = rs.getString("GrpPolNo").trim();

            if( rs.getString("ContNo") == null )
                this.ContNo = null;
            else
                this.ContNo = rs.getString("ContNo").trim();

            if( rs.getString("ManageCom") == null )
                this.ManageCom = null;
            else
                this.ManageCom = rs.getString("ManageCom").trim();

            if( rs.getString("AgentCom") == null )
                this.AgentCom = null;
            else
                this.AgentCom = rs.getString("AgentCom").trim();

            if( rs.getString("AgentType") == null )
                this.AgentType = null;
            else
                this.AgentType = rs.getString("AgentType").trim();

            if( rs.getString("RiskCode") == null )
                this.RiskCode = null;
            else
                this.RiskCode = rs.getString("RiskCode").trim();

            if( rs.getString("AgentCode") == null )
                this.AgentCode = null;
            else
                this.AgentCode = rs.getString("AgentCode").trim();

            if( rs.getString("AgentGroup") == null )
                this.AgentGroup = null;
            else
                this.AgentGroup = rs.getString("AgentGroup").trim();

            if( rs.getString("PayTypeFlag") == null )
                this.PayTypeFlag = null;
            else
                this.PayTypeFlag = rs.getString("PayTypeFlag").trim();

            if( rs.getString("AppntNo") == null )
                this.AppntNo = null;
            else
                this.AppntNo = rs.getString("AppntNo").trim();

            if( rs.getString("GetNoticeNo") == null )
                this.GetNoticeNo = null;
            else
                this.GetNoticeNo = rs.getString("GetNoticeNo").trim();

            if( rs.getString("PayAimClass") == null )
                this.PayAimClass = null;
            else
                this.PayAimClass = rs.getString("PayAimClass").trim();

            if( rs.getString("DutyCode") == null )
                this.DutyCode = null;
            else
                this.DutyCode = rs.getString("DutyCode").trim();

            if( rs.getString("PayPlanCode") == null )
                this.PayPlanCode = null;
            else
                this.PayPlanCode = rs.getString("PayPlanCode").trim();

            this.SumDuePayMoney = rs.getDouble("SumDuePayMoney");
            this.SumActuPayMoney = rs.getDouble("SumActuPayMoney");
            this.PayIntv = rs.getInt("PayIntv");
            this.PayDate = rs.getDate("PayDate");
            if( rs.getString("PayType") == null )
                this.PayType = null;
            else
                this.PayType = rs.getString("PayType").trim();

            this.LastPayToDate = rs.getDate("LastPayToDate");
            this.CurPayToDate = rs.getDate("CurPayToDate");
            if( rs.getString("InInsuAccState") == null )
                this.InInsuAccState = null;
            else
                this.InInsuAccState = rs.getString("InInsuAccState").trim();

            if( rs.getString("BankCode") == null )
                this.BankCode = null;
            else
                this.BankCode = rs.getString("BankCode").trim();

            if( rs.getString("BankAccNo") == null )
                this.BankAccNo = null;
            else
                this.BankAccNo = rs.getString("BankAccNo").trim();

            if( rs.getString("BankOnTheWayFlag") == null )
                this.BankOnTheWayFlag = null;
            else
                this.BankOnTheWayFlag = rs.getString("BankOnTheWayFlag").trim();

            if( rs.getString("BankSuccFlag") == null )
                this.BankSuccFlag = null;
            else
                this.BankSuccFlag = rs.getString("BankSuccFlag").trim();

            if( rs.getString("ApproveCode") == null )
                this.ApproveCode = null;
            else
                this.ApproveCode = rs.getString("ApproveCode").trim();

            this.ApproveDate = rs.getDate("ApproveDate");
            if( rs.getString("ApproveTime") == null )
                this.ApproveTime = null;
            else
                this.ApproveTime = rs.getString("ApproveTime").trim();

            if( rs.getString("SerialNo") == null )
                this.SerialNo = null;
            else
                this.SerialNo = rs.getString("SerialNo").trim();

            if( rs.getString("InputFlag") == null )
                this.InputFlag = null;
            else
                this.InputFlag = rs.getString("InputFlag").trim();

            if( rs.getString("Operator") == null )
                this.Operator = null;
            else
                this.Operator = rs.getString("Operator").trim();

            this.MakeDate = rs.getDate("MakeDate");
            if( rs.getString("MakeTime") == null )
                this.MakeTime = null;
            else
                this.MakeTime = rs.getString("MakeTime").trim();

            this.ModifyDate = rs.getDate("ModifyDate");
            if( rs.getString("ModifyTime") == null )
                this.ModifyTime = null;
            else
                this.ModifyTime = rs.getString("ModifyTime").trim();

            if( rs.getString("EdorNo") == null )
                this.EdorNo = null;
            else
                this.EdorNo = rs.getString("EdorNo").trim();

            this.MainPolYear = rs.getInt("MainPolYear");
            if( rs.getString("OtherNo") == null )
                this.OtherNo = null;
            else
                this.OtherNo = rs.getString("OtherNo").trim();

            if( rs.getString("OtherNoType") == null )
                this.OtherNoType = null;
            else
                this.OtherNoType = rs.getString("OtherNoType").trim();

            if( rs.getString("PrtNo") == null )
                this.PrtNo = null;
            else
                this.PrtNo = rs.getString("PrtNo").trim();

            if( rs.getString("SaleChnl") == null )
                this.SaleChnl = null;
            else
                this.SaleChnl = rs.getString("SaleChnl").trim();

            if( rs.getString("ChargeCom") == null )
                this.ChargeCom = null;
            else
                this.ChargeCom = rs.getString("ChargeCom").trim();

            if( rs.getString("APPntName") == null )
                this.APPntName = null;
            else
                this.APPntName = rs.getString("APPntName").trim();

            if( rs.getString("KindCode") == null )
                this.KindCode = null;
            else
                this.KindCode = rs.getString("KindCode").trim();

            this.PaytoDate = rs.getDate("PaytoDate");
            this.PayEndDate = rs.getDate("PayEndDate");
            this.SignDate = rs.getDate("SignDate");
            this.CValiDate = rs.getDate("CValiDate");
            this.PayEndYear = rs.getInt("PayEndYear");
            this.PayMoney = rs.getDouble("PayMoney");
        }
        catch(SQLException sqle) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LJSPayPersonSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = ExceptionUtils.exceptionToString(sqle);
            this.mErrors .addOneError(tError);
            return false;
        }
        return true;
    }

    public LJSPayPersonSchema getSchema() {
        LJSPayPersonSchema aLJSPayPersonSchema = new LJSPayPersonSchema();
        aLJSPayPersonSchema.setSchema(this);
        return aLJSPayPersonSchema;
    }

    public LJSPayPersonDB getDB() {
        LJSPayPersonDB aDBOper = new LJSPayPersonDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
    * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLJSPayPerson描述/A>表字段
    * @return: String 返回打包后字符串
    **/
    public String encode() {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(ChgData.chgData(SPayPersonID));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(SPayID));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ShardingID)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(PolNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(PayCount));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(GrpContNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(GrpPolNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ContNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ManageCom)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AgentCom)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AgentType)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(RiskCode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AgentCode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AgentGroup)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(PayTypeFlag)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AppntNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(GetNoticeNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(PayAimClass)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(DutyCode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(PayPlanCode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(SumDuePayMoney));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(SumActuPayMoney));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(PayIntv));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( PayDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(PayType)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( LastPayToDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( CurPayToDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(InInsuAccState)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(BankCode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(BankAccNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(BankOnTheWayFlag)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(BankSuccFlag)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ApproveCode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( ApproveDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ApproveTime)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(SerialNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(InputFlag)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ModifyTime)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(EdorNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(MainPolYear));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(OtherNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(OtherNoType)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(PrtNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(SaleChnl)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ChargeCom)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(APPntName)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(KindCode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( PaytoDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( PayEndDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( SignDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( CValiDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(PayEndYear));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(PayMoney));
        return strReturn.toString();
    }

    /**
    * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLJSPayPerson>历史记账凭证主表信息</A>表字段
    * @param: strMessage String 包含一条纪录数据的字符串
    * @return: boolean
    **/
    public boolean decode(String strMessage) {
        try {
            SPayPersonID = new Long(ChgData.chgNumericStr(StrTool.getStr(strMessage,1, SysConst.PACKAGESPILTER))).longValue();
            SPayID = new Long(ChgData.chgNumericStr(StrTool.getStr(strMessage,2, SysConst.PACKAGESPILTER))).longValue();
            ShardingID = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
            PolNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
            PayCount = new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,5, SysConst.PACKAGESPILTER))).intValue();
            GrpContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
            GrpPolNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
            ContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
            ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
            AgentCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
            AgentType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
            RiskCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
            AgentCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
            AgentGroup = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER );
            PayTypeFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15, SysConst.PACKAGESPILTER );
            AppntNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16, SysConst.PACKAGESPILTER );
            GetNoticeNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17, SysConst.PACKAGESPILTER );
            PayAimClass = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18, SysConst.PACKAGESPILTER );
            DutyCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19, SysConst.PACKAGESPILTER );
            PayPlanCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20, SysConst.PACKAGESPILTER );
            SumDuePayMoney = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,21, SysConst.PACKAGESPILTER))).doubleValue();
            SumActuPayMoney = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,22, SysConst.PACKAGESPILTER))).doubleValue();
            PayIntv = new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,23, SysConst.PACKAGESPILTER))).intValue();
            PayDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 24, SysConst.PACKAGESPILTER));
            PayType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 25, SysConst.PACKAGESPILTER );
            LastPayToDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 26, SysConst.PACKAGESPILTER));
            CurPayToDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 27, SysConst.PACKAGESPILTER));
            InInsuAccState = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 28, SysConst.PACKAGESPILTER );
            BankCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 29, SysConst.PACKAGESPILTER );
            BankAccNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 30, SysConst.PACKAGESPILTER );
            BankOnTheWayFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 31, SysConst.PACKAGESPILTER );
            BankSuccFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 32, SysConst.PACKAGESPILTER );
            ApproveCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 33, SysConst.PACKAGESPILTER );
            ApproveDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 34, SysConst.PACKAGESPILTER));
            ApproveTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 35, SysConst.PACKAGESPILTER );
            SerialNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 36, SysConst.PACKAGESPILTER );
            InputFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 37, SysConst.PACKAGESPILTER );
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 38, SysConst.PACKAGESPILTER );
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 39, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 40, SysConst.PACKAGESPILTER );
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 41, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 42, SysConst.PACKAGESPILTER );
            EdorNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 43, SysConst.PACKAGESPILTER );
            MainPolYear = new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,44, SysConst.PACKAGESPILTER))).intValue();
            OtherNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 45, SysConst.PACKAGESPILTER );
            OtherNoType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 46, SysConst.PACKAGESPILTER );
            PrtNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 47, SysConst.PACKAGESPILTER );
            SaleChnl = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 48, SysConst.PACKAGESPILTER );
            ChargeCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 49, SysConst.PACKAGESPILTER );
            APPntName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 50, SysConst.PACKAGESPILTER );
            KindCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 51, SysConst.PACKAGESPILTER );
            PaytoDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 52, SysConst.PACKAGESPILTER));
            PayEndDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 53, SysConst.PACKAGESPILTER));
            SignDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 54, SysConst.PACKAGESPILTER));
            CValiDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 55, SysConst.PACKAGESPILTER));
            PayEndYear = new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,56, SysConst.PACKAGESPILTER))).intValue();
            PayMoney = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,57, SysConst.PACKAGESPILTER))).doubleValue();
        }
        catch(NumberFormatException ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LJSPayPersonSchema";
            tError.functionName = "decode";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            return false;
        }
        return true;
    }

    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("SPayPersonID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SPayPersonID));
        }
        if (FCode.equalsIgnoreCase("SPayID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SPayID));
        }
        if (FCode.equalsIgnoreCase("ShardingID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ShardingID));
        }
        if (FCode.equalsIgnoreCase("PolNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PolNo));
        }
        if (FCode.equalsIgnoreCase("PayCount")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PayCount));
        }
        if (FCode.equalsIgnoreCase("GrpContNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GrpContNo));
        }
        if (FCode.equalsIgnoreCase("GrpPolNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GrpPolNo));
        }
        if (FCode.equalsIgnoreCase("ContNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ContNo));
        }
        if (FCode.equalsIgnoreCase("ManageCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
        }
        if (FCode.equalsIgnoreCase("AgentCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentCom));
        }
        if (FCode.equalsIgnoreCase("AgentType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentType));
        }
        if (FCode.equalsIgnoreCase("RiskCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RiskCode));
        }
        if (FCode.equalsIgnoreCase("AgentCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentCode));
        }
        if (FCode.equalsIgnoreCase("AgentGroup")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentGroup));
        }
        if (FCode.equalsIgnoreCase("PayTypeFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PayTypeFlag));
        }
        if (FCode.equalsIgnoreCase("AppntNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AppntNo));
        }
        if (FCode.equalsIgnoreCase("GetNoticeNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GetNoticeNo));
        }
        if (FCode.equalsIgnoreCase("PayAimClass")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PayAimClass));
        }
        if (FCode.equalsIgnoreCase("DutyCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DutyCode));
        }
        if (FCode.equalsIgnoreCase("PayPlanCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PayPlanCode));
        }
        if (FCode.equalsIgnoreCase("SumDuePayMoney")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SumDuePayMoney));
        }
        if (FCode.equalsIgnoreCase("SumActuPayMoney")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SumActuPayMoney));
        }
        if (FCode.equalsIgnoreCase("PayIntv")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PayIntv));
        }
        if (FCode.equalsIgnoreCase("PayDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getPayDate()));
        }
        if (FCode.equalsIgnoreCase("PayType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PayType));
        }
        if (FCode.equalsIgnoreCase("LastPayToDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getLastPayToDate()));
        }
        if (FCode.equalsIgnoreCase("CurPayToDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getCurPayToDate()));
        }
        if (FCode.equalsIgnoreCase("InInsuAccState")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InInsuAccState));
        }
        if (FCode.equalsIgnoreCase("BankCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BankCode));
        }
        if (FCode.equalsIgnoreCase("BankAccNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BankAccNo));
        }
        if (FCode.equalsIgnoreCase("BankOnTheWayFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BankOnTheWayFlag));
        }
        if (FCode.equalsIgnoreCase("BankSuccFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BankSuccFlag));
        }
        if (FCode.equalsIgnoreCase("ApproveCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ApproveCode));
        }
        if (FCode.equalsIgnoreCase("ApproveDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getApproveDate()));
        }
        if (FCode.equalsIgnoreCase("ApproveTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ApproveTime));
        }
        if (FCode.equalsIgnoreCase("SerialNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SerialNo));
        }
        if (FCode.equalsIgnoreCase("InputFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InputFlag));
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (FCode.equalsIgnoreCase("EdorNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(EdorNo));
        }
        if (FCode.equalsIgnoreCase("MainPolYear")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MainPolYear));
        }
        if (FCode.equalsIgnoreCase("OtherNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OtherNo));
        }
        if (FCode.equalsIgnoreCase("OtherNoType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OtherNoType));
        }
        if (FCode.equalsIgnoreCase("PrtNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PrtNo));
        }
        if (FCode.equalsIgnoreCase("SaleChnl")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SaleChnl));
        }
        if (FCode.equalsIgnoreCase("ChargeCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ChargeCom));
        }
        if (FCode.equalsIgnoreCase("APPntName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(APPntName));
        }
        if (FCode.equalsIgnoreCase("KindCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(KindCode));
        }
        if (FCode.equalsIgnoreCase("PaytoDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getPaytoDate()));
        }
        if (FCode.equalsIgnoreCase("PayEndDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getPayEndDate()));
        }
        if (FCode.equalsIgnoreCase("SignDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getSignDate()));
        }
        if (FCode.equalsIgnoreCase("CValiDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getCValiDate()));
        }
        if (FCode.equalsIgnoreCase("PayEndYear")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PayEndYear));
        }
        if (FCode.equalsIgnoreCase("PayMoney")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PayMoney));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = String.valueOf(SPayPersonID);
                break;
            case 1:
                strFieldValue = String.valueOf(SPayID);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(ShardingID);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(PolNo);
                break;
            case 4:
                strFieldValue = String.valueOf(PayCount);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(GrpContNo);
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(GrpPolNo);
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(ContNo);
                break;
            case 8:
                strFieldValue = StrTool.GBKToUnicode(ManageCom);
                break;
            case 9:
                strFieldValue = StrTool.GBKToUnicode(AgentCom);
                break;
            case 10:
                strFieldValue = StrTool.GBKToUnicode(AgentType);
                break;
            case 11:
                strFieldValue = StrTool.GBKToUnicode(RiskCode);
                break;
            case 12:
                strFieldValue = StrTool.GBKToUnicode(AgentCode);
                break;
            case 13:
                strFieldValue = StrTool.GBKToUnicode(AgentGroup);
                break;
            case 14:
                strFieldValue = StrTool.GBKToUnicode(PayTypeFlag);
                break;
            case 15:
                strFieldValue = StrTool.GBKToUnicode(AppntNo);
                break;
            case 16:
                strFieldValue = StrTool.GBKToUnicode(GetNoticeNo);
                break;
            case 17:
                strFieldValue = StrTool.GBKToUnicode(PayAimClass);
                break;
            case 18:
                strFieldValue = StrTool.GBKToUnicode(DutyCode);
                break;
            case 19:
                strFieldValue = StrTool.GBKToUnicode(PayPlanCode);
                break;
            case 20:
                strFieldValue = String.valueOf(SumDuePayMoney);
                break;
            case 21:
                strFieldValue = String.valueOf(SumActuPayMoney);
                break;
            case 22:
                strFieldValue = String.valueOf(PayIntv);
                break;
            case 23:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getPayDate()));
                break;
            case 24:
                strFieldValue = StrTool.GBKToUnicode(PayType);
                break;
            case 25:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getLastPayToDate()));
                break;
            case 26:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getCurPayToDate()));
                break;
            case 27:
                strFieldValue = StrTool.GBKToUnicode(InInsuAccState);
                break;
            case 28:
                strFieldValue = StrTool.GBKToUnicode(BankCode);
                break;
            case 29:
                strFieldValue = StrTool.GBKToUnicode(BankAccNo);
                break;
            case 30:
                strFieldValue = StrTool.GBKToUnicode(BankOnTheWayFlag);
                break;
            case 31:
                strFieldValue = StrTool.GBKToUnicode(BankSuccFlag);
                break;
            case 32:
                strFieldValue = StrTool.GBKToUnicode(ApproveCode);
                break;
            case 33:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getApproveDate()));
                break;
            case 34:
                strFieldValue = StrTool.GBKToUnicode(ApproveTime);
                break;
            case 35:
                strFieldValue = StrTool.GBKToUnicode(SerialNo);
                break;
            case 36:
                strFieldValue = StrTool.GBKToUnicode(InputFlag);
                break;
            case 37:
                strFieldValue = StrTool.GBKToUnicode(Operator);
                break;
            case 38:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
                break;
            case 39:
                strFieldValue = StrTool.GBKToUnicode(MakeTime);
                break;
            case 40:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
                break;
            case 41:
                strFieldValue = StrTool.GBKToUnicode(ModifyTime);
                break;
            case 42:
                strFieldValue = StrTool.GBKToUnicode(EdorNo);
                break;
            case 43:
                strFieldValue = String.valueOf(MainPolYear);
                break;
            case 44:
                strFieldValue = StrTool.GBKToUnicode(OtherNo);
                break;
            case 45:
                strFieldValue = StrTool.GBKToUnicode(OtherNoType);
                break;
            case 46:
                strFieldValue = StrTool.GBKToUnicode(PrtNo);
                break;
            case 47:
                strFieldValue = StrTool.GBKToUnicode(SaleChnl);
                break;
            case 48:
                strFieldValue = StrTool.GBKToUnicode(ChargeCom);
                break;
            case 49:
                strFieldValue = StrTool.GBKToUnicode(APPntName);
                break;
            case 50:
                strFieldValue = StrTool.GBKToUnicode(KindCode);
                break;
            case 51:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getPaytoDate()));
                break;
            case 52:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getPayEndDate()));
                break;
            case 53:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getSignDate()));
                break;
            case 54:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getCValiDate()));
                break;
            case 55:
                strFieldValue = String.valueOf(PayEndYear);
                break;
            case 56:
                strFieldValue = String.valueOf(PayMoney);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("SPayPersonID")) {
            if( FValue != null && !FValue.equals("")) {
                SPayPersonID = new Long(FValue).longValue();
            }
        }
        if (FCode.equalsIgnoreCase("SPayID")) {
            if( FValue != null && !FValue.equals("")) {
                SPayID = new Long(FValue).longValue();
            }
        }
        if (FCode.equalsIgnoreCase("ShardingID")) {
            if( FValue != null && !FValue.equals(""))
            {
                ShardingID = FValue.trim();
            }
            else
                ShardingID = null;
        }
        if (FCode.equalsIgnoreCase("PolNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                PolNo = FValue.trim();
            }
            else
                PolNo = null;
        }
        if (FCode.equalsIgnoreCase("PayCount")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                PayCount = i;
            }
        }
        if (FCode.equalsIgnoreCase("GrpContNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                GrpContNo = FValue.trim();
            }
            else
                GrpContNo = null;
        }
        if (FCode.equalsIgnoreCase("GrpPolNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                GrpPolNo = FValue.trim();
            }
            else
                GrpPolNo = null;
        }
        if (FCode.equalsIgnoreCase("ContNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                ContNo = FValue.trim();
            }
            else
                ContNo = null;
        }
        if (FCode.equalsIgnoreCase("ManageCom")) {
            if( FValue != null && !FValue.equals(""))
            {
                ManageCom = FValue.trim();
            }
            else
                ManageCom = null;
        }
        if (FCode.equalsIgnoreCase("AgentCom")) {
            if( FValue != null && !FValue.equals(""))
            {
                AgentCom = FValue.trim();
            }
            else
                AgentCom = null;
        }
        if (FCode.equalsIgnoreCase("AgentType")) {
            if( FValue != null && !FValue.equals(""))
            {
                AgentType = FValue.trim();
            }
            else
                AgentType = null;
        }
        if (FCode.equalsIgnoreCase("RiskCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                RiskCode = FValue.trim();
            }
            else
                RiskCode = null;
        }
        if (FCode.equalsIgnoreCase("AgentCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                AgentCode = FValue.trim();
            }
            else
                AgentCode = null;
        }
        if (FCode.equalsIgnoreCase("AgentGroup")) {
            if( FValue != null && !FValue.equals(""))
            {
                AgentGroup = FValue.trim();
            }
            else
                AgentGroup = null;
        }
        if (FCode.equalsIgnoreCase("PayTypeFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                PayTypeFlag = FValue.trim();
            }
            else
                PayTypeFlag = null;
        }
        if (FCode.equalsIgnoreCase("AppntNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                AppntNo = FValue.trim();
            }
            else
                AppntNo = null;
        }
        if (FCode.equalsIgnoreCase("GetNoticeNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                GetNoticeNo = FValue.trim();
            }
            else
                GetNoticeNo = null;
        }
        if (FCode.equalsIgnoreCase("PayAimClass")) {
            if( FValue != null && !FValue.equals(""))
            {
                PayAimClass = FValue.trim();
            }
            else
                PayAimClass = null;
        }
        if (FCode.equalsIgnoreCase("DutyCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                DutyCode = FValue.trim();
            }
            else
                DutyCode = null;
        }
        if (FCode.equalsIgnoreCase("PayPlanCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                PayPlanCode = FValue.trim();
            }
            else
                PayPlanCode = null;
        }
        if (FCode.equalsIgnoreCase("SumDuePayMoney")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                SumDuePayMoney = d;
            }
        }
        if (FCode.equalsIgnoreCase("SumActuPayMoney")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                SumActuPayMoney = d;
            }
        }
        if (FCode.equalsIgnoreCase("PayIntv")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                PayIntv = i;
            }
        }
        if (FCode.equalsIgnoreCase("PayDate")) {
            if(FValue != null && !FValue.equals("")) {
                PayDate = fDate.getDate( FValue );
            }
            else
                PayDate = null;
        }
        if (FCode.equalsIgnoreCase("PayType")) {
            if( FValue != null && !FValue.equals(""))
            {
                PayType = FValue.trim();
            }
            else
                PayType = null;
        }
        if (FCode.equalsIgnoreCase("LastPayToDate")) {
            if(FValue != null && !FValue.equals("")) {
                LastPayToDate = fDate.getDate( FValue );
            }
            else
                LastPayToDate = null;
        }
        if (FCode.equalsIgnoreCase("CurPayToDate")) {
            if(FValue != null && !FValue.equals("")) {
                CurPayToDate = fDate.getDate( FValue );
            }
            else
                CurPayToDate = null;
        }
        if (FCode.equalsIgnoreCase("InInsuAccState")) {
            if( FValue != null && !FValue.equals(""))
            {
                InInsuAccState = FValue.trim();
            }
            else
                InInsuAccState = null;
        }
        if (FCode.equalsIgnoreCase("BankCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                BankCode = FValue.trim();
            }
            else
                BankCode = null;
        }
        if (FCode.equalsIgnoreCase("BankAccNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                BankAccNo = FValue.trim();
            }
            else
                BankAccNo = null;
        }
        if (FCode.equalsIgnoreCase("BankOnTheWayFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                BankOnTheWayFlag = FValue.trim();
            }
            else
                BankOnTheWayFlag = null;
        }
        if (FCode.equalsIgnoreCase("BankSuccFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                BankSuccFlag = FValue.trim();
            }
            else
                BankSuccFlag = null;
        }
        if (FCode.equalsIgnoreCase("ApproveCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                ApproveCode = FValue.trim();
            }
            else
                ApproveCode = null;
        }
        if (FCode.equalsIgnoreCase("ApproveDate")) {
            if(FValue != null && !FValue.equals("")) {
                ApproveDate = fDate.getDate( FValue );
            }
            else
                ApproveDate = null;
        }
        if (FCode.equalsIgnoreCase("ApproveTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                ApproveTime = FValue.trim();
            }
            else
                ApproveTime = null;
        }
        if (FCode.equalsIgnoreCase("SerialNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                SerialNo = FValue.trim();
            }
            else
                SerialNo = null;
        }
        if (FCode.equalsIgnoreCase("InputFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                InputFlag = FValue.trim();
            }
            else
                InputFlag = null;
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            if( FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
                Operator = null;
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if(FValue != null && !FValue.equals("")) {
                MakeDate = fDate.getDate( FValue );
            }
            else
                MakeDate = null;
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
                MakeTime = null;
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if(FValue != null && !FValue.equals("")) {
                ModifyDate = fDate.getDate( FValue );
            }
            else
                ModifyDate = null;
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
                ModifyTime = null;
        }
        if (FCode.equalsIgnoreCase("EdorNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                EdorNo = FValue.trim();
            }
            else
                EdorNo = null;
        }
        if (FCode.equalsIgnoreCase("MainPolYear")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                MainPolYear = i;
            }
        }
        if (FCode.equalsIgnoreCase("OtherNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                OtherNo = FValue.trim();
            }
            else
                OtherNo = null;
        }
        if (FCode.equalsIgnoreCase("OtherNoType")) {
            if( FValue != null && !FValue.equals(""))
            {
                OtherNoType = FValue.trim();
            }
            else
                OtherNoType = null;
        }
        if (FCode.equalsIgnoreCase("PrtNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                PrtNo = FValue.trim();
            }
            else
                PrtNo = null;
        }
        if (FCode.equalsIgnoreCase("SaleChnl")) {
            if( FValue != null && !FValue.equals(""))
            {
                SaleChnl = FValue.trim();
            }
            else
                SaleChnl = null;
        }
        if (FCode.equalsIgnoreCase("ChargeCom")) {
            if( FValue != null && !FValue.equals(""))
            {
                ChargeCom = FValue.trim();
            }
            else
                ChargeCom = null;
        }
        if (FCode.equalsIgnoreCase("APPntName")) {
            if( FValue != null && !FValue.equals(""))
            {
                APPntName = FValue.trim();
            }
            else
                APPntName = null;
        }
        if (FCode.equalsIgnoreCase("KindCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                KindCode = FValue.trim();
            }
            else
                KindCode = null;
        }
        if (FCode.equalsIgnoreCase("PaytoDate")) {
            if(FValue != null && !FValue.equals("")) {
                PaytoDate = fDate.getDate( FValue );
            }
            else
                PaytoDate = null;
        }
        if (FCode.equalsIgnoreCase("PayEndDate")) {
            if(FValue != null && !FValue.equals("")) {
                PayEndDate = fDate.getDate( FValue );
            }
            else
                PayEndDate = null;
        }
        if (FCode.equalsIgnoreCase("SignDate")) {
            if(FValue != null && !FValue.equals("")) {
                SignDate = fDate.getDate( FValue );
            }
            else
                SignDate = null;
        }
        if (FCode.equalsIgnoreCase("CValiDate")) {
            if(FValue != null && !FValue.equals("")) {
                CValiDate = fDate.getDate( FValue );
            }
            else
                CValiDate = null;
        }
        if (FCode.equalsIgnoreCase("PayEndYear")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                PayEndYear = i;
            }
        }
        if (FCode.equalsIgnoreCase("PayMoney")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                PayMoney = d;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject) return true;
        if (otherObject == null) return false;
        if (getClass() != otherObject.getClass()) return false;
        LJSPayPersonSchema other = (LJSPayPersonSchema)otherObject;
        return
            SPayPersonID == other.getSPayPersonID()
            && SPayID == other.getSPayID()
            && ShardingID.equals(other.getShardingID())
            && PolNo.equals(other.getPolNo())
            && PayCount == other.getPayCount()
            && GrpContNo.equals(other.getGrpContNo())
            && GrpPolNo.equals(other.getGrpPolNo())
            && ContNo.equals(other.getContNo())
            && ManageCom.equals(other.getManageCom())
            && AgentCom.equals(other.getAgentCom())
            && AgentType.equals(other.getAgentType())
            && RiskCode.equals(other.getRiskCode())
            && AgentCode.equals(other.getAgentCode())
            && AgentGroup.equals(other.getAgentGroup())
            && PayTypeFlag.equals(other.getPayTypeFlag())
            && AppntNo.equals(other.getAppntNo())
            && GetNoticeNo.equals(other.getGetNoticeNo())
            && PayAimClass.equals(other.getPayAimClass())
            && DutyCode.equals(other.getDutyCode())
            && PayPlanCode.equals(other.getPayPlanCode())
            && SumDuePayMoney == other.getSumDuePayMoney()
            && SumActuPayMoney == other.getSumActuPayMoney()
            && PayIntv == other.getPayIntv()
            && fDate.getString(PayDate).equals(other.getPayDate())
            && PayType.equals(other.getPayType())
            && fDate.getString(LastPayToDate).equals(other.getLastPayToDate())
            && fDate.getString(CurPayToDate).equals(other.getCurPayToDate())
            && InInsuAccState.equals(other.getInInsuAccState())
            && BankCode.equals(other.getBankCode())
            && BankAccNo.equals(other.getBankAccNo())
            && BankOnTheWayFlag.equals(other.getBankOnTheWayFlag())
            && BankSuccFlag.equals(other.getBankSuccFlag())
            && ApproveCode.equals(other.getApproveCode())
            && fDate.getString(ApproveDate).equals(other.getApproveDate())
            && ApproveTime.equals(other.getApproveTime())
            && SerialNo.equals(other.getSerialNo())
            && InputFlag.equals(other.getInputFlag())
            && Operator.equals(other.getOperator())
            && fDate.getString(MakeDate).equals(other.getMakeDate())
            && MakeTime.equals(other.getMakeTime())
            && fDate.getString(ModifyDate).equals(other.getModifyDate())
            && ModifyTime.equals(other.getModifyTime())
            && EdorNo.equals(other.getEdorNo())
            && MainPolYear == other.getMainPolYear()
            && OtherNo.equals(other.getOtherNo())
            && OtherNoType.equals(other.getOtherNoType())
            && PrtNo.equals(other.getPrtNo())
            && SaleChnl.equals(other.getSaleChnl())
            && ChargeCom.equals(other.getChargeCom())
            && APPntName.equals(other.getAPPntName())
            && KindCode.equals(other.getKindCode())
            && fDate.getString(PaytoDate).equals(other.getPaytoDate())
            && fDate.getString(PayEndDate).equals(other.getPayEndDate())
            && fDate.getString(SignDate).equals(other.getSignDate())
            && fDate.getString(CValiDate).equals(other.getCValiDate())
            && PayEndYear == other.getPayEndYear()
            && PayMoney == other.getPayMoney();
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("SPayPersonID") ) {
            return 0;
        }
        if( strFieldName.equals("SPayID") ) {
            return 1;
        }
        if( strFieldName.equals("ShardingID") ) {
            return 2;
        }
        if( strFieldName.equals("PolNo") ) {
            return 3;
        }
        if( strFieldName.equals("PayCount") ) {
            return 4;
        }
        if( strFieldName.equals("GrpContNo") ) {
            return 5;
        }
        if( strFieldName.equals("GrpPolNo") ) {
            return 6;
        }
        if( strFieldName.equals("ContNo") ) {
            return 7;
        }
        if( strFieldName.equals("ManageCom") ) {
            return 8;
        }
        if( strFieldName.equals("AgentCom") ) {
            return 9;
        }
        if( strFieldName.equals("AgentType") ) {
            return 10;
        }
        if( strFieldName.equals("RiskCode") ) {
            return 11;
        }
        if( strFieldName.equals("AgentCode") ) {
            return 12;
        }
        if( strFieldName.equals("AgentGroup") ) {
            return 13;
        }
        if( strFieldName.equals("PayTypeFlag") ) {
            return 14;
        }
        if( strFieldName.equals("AppntNo") ) {
            return 15;
        }
        if( strFieldName.equals("GetNoticeNo") ) {
            return 16;
        }
        if( strFieldName.equals("PayAimClass") ) {
            return 17;
        }
        if( strFieldName.equals("DutyCode") ) {
            return 18;
        }
        if( strFieldName.equals("PayPlanCode") ) {
            return 19;
        }
        if( strFieldName.equals("SumDuePayMoney") ) {
            return 20;
        }
        if( strFieldName.equals("SumActuPayMoney") ) {
            return 21;
        }
        if( strFieldName.equals("PayIntv") ) {
            return 22;
        }
        if( strFieldName.equals("PayDate") ) {
            return 23;
        }
        if( strFieldName.equals("PayType") ) {
            return 24;
        }
        if( strFieldName.equals("LastPayToDate") ) {
            return 25;
        }
        if( strFieldName.equals("CurPayToDate") ) {
            return 26;
        }
        if( strFieldName.equals("InInsuAccState") ) {
            return 27;
        }
        if( strFieldName.equals("BankCode") ) {
            return 28;
        }
        if( strFieldName.equals("BankAccNo") ) {
            return 29;
        }
        if( strFieldName.equals("BankOnTheWayFlag") ) {
            return 30;
        }
        if( strFieldName.equals("BankSuccFlag") ) {
            return 31;
        }
        if( strFieldName.equals("ApproveCode") ) {
            return 32;
        }
        if( strFieldName.equals("ApproveDate") ) {
            return 33;
        }
        if( strFieldName.equals("ApproveTime") ) {
            return 34;
        }
        if( strFieldName.equals("SerialNo") ) {
            return 35;
        }
        if( strFieldName.equals("InputFlag") ) {
            return 36;
        }
        if( strFieldName.equals("Operator") ) {
            return 37;
        }
        if( strFieldName.equals("MakeDate") ) {
            return 38;
        }
        if( strFieldName.equals("MakeTime") ) {
            return 39;
        }
        if( strFieldName.equals("ModifyDate") ) {
            return 40;
        }
        if( strFieldName.equals("ModifyTime") ) {
            return 41;
        }
        if( strFieldName.equals("EdorNo") ) {
            return 42;
        }
        if( strFieldName.equals("MainPolYear") ) {
            return 43;
        }
        if( strFieldName.equals("OtherNo") ) {
            return 44;
        }
        if( strFieldName.equals("OtherNoType") ) {
            return 45;
        }
        if( strFieldName.equals("PrtNo") ) {
            return 46;
        }
        if( strFieldName.equals("SaleChnl") ) {
            return 47;
        }
        if( strFieldName.equals("ChargeCom") ) {
            return 48;
        }
        if( strFieldName.equals("APPntName") ) {
            return 49;
        }
        if( strFieldName.equals("KindCode") ) {
            return 50;
        }
        if( strFieldName.equals("PaytoDate") ) {
            return 51;
        }
        if( strFieldName.equals("PayEndDate") ) {
            return 52;
        }
        if( strFieldName.equals("SignDate") ) {
            return 53;
        }
        if( strFieldName.equals("CValiDate") ) {
            return 54;
        }
        if( strFieldName.equals("PayEndYear") ) {
            return 55;
        }
        if( strFieldName.equals("PayMoney") ) {
            return 56;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "SPayPersonID";
                break;
            case 1:
                strFieldName = "SPayID";
                break;
            case 2:
                strFieldName = "ShardingID";
                break;
            case 3:
                strFieldName = "PolNo";
                break;
            case 4:
                strFieldName = "PayCount";
                break;
            case 5:
                strFieldName = "GrpContNo";
                break;
            case 6:
                strFieldName = "GrpPolNo";
                break;
            case 7:
                strFieldName = "ContNo";
                break;
            case 8:
                strFieldName = "ManageCom";
                break;
            case 9:
                strFieldName = "AgentCom";
                break;
            case 10:
                strFieldName = "AgentType";
                break;
            case 11:
                strFieldName = "RiskCode";
                break;
            case 12:
                strFieldName = "AgentCode";
                break;
            case 13:
                strFieldName = "AgentGroup";
                break;
            case 14:
                strFieldName = "PayTypeFlag";
                break;
            case 15:
                strFieldName = "AppntNo";
                break;
            case 16:
                strFieldName = "GetNoticeNo";
                break;
            case 17:
                strFieldName = "PayAimClass";
                break;
            case 18:
                strFieldName = "DutyCode";
                break;
            case 19:
                strFieldName = "PayPlanCode";
                break;
            case 20:
                strFieldName = "SumDuePayMoney";
                break;
            case 21:
                strFieldName = "SumActuPayMoney";
                break;
            case 22:
                strFieldName = "PayIntv";
                break;
            case 23:
                strFieldName = "PayDate";
                break;
            case 24:
                strFieldName = "PayType";
                break;
            case 25:
                strFieldName = "LastPayToDate";
                break;
            case 26:
                strFieldName = "CurPayToDate";
                break;
            case 27:
                strFieldName = "InInsuAccState";
                break;
            case 28:
                strFieldName = "BankCode";
                break;
            case 29:
                strFieldName = "BankAccNo";
                break;
            case 30:
                strFieldName = "BankOnTheWayFlag";
                break;
            case 31:
                strFieldName = "BankSuccFlag";
                break;
            case 32:
                strFieldName = "ApproveCode";
                break;
            case 33:
                strFieldName = "ApproveDate";
                break;
            case 34:
                strFieldName = "ApproveTime";
                break;
            case 35:
                strFieldName = "SerialNo";
                break;
            case 36:
                strFieldName = "InputFlag";
                break;
            case 37:
                strFieldName = "Operator";
                break;
            case 38:
                strFieldName = "MakeDate";
                break;
            case 39:
                strFieldName = "MakeTime";
                break;
            case 40:
                strFieldName = "ModifyDate";
                break;
            case 41:
                strFieldName = "ModifyTime";
                break;
            case 42:
                strFieldName = "EdorNo";
                break;
            case 43:
                strFieldName = "MainPolYear";
                break;
            case 44:
                strFieldName = "OtherNo";
                break;
            case 45:
                strFieldName = "OtherNoType";
                break;
            case 46:
                strFieldName = "PrtNo";
                break;
            case 47:
                strFieldName = "SaleChnl";
                break;
            case 48:
                strFieldName = "ChargeCom";
                break;
            case 49:
                strFieldName = "APPntName";
                break;
            case 50:
                strFieldName = "KindCode";
                break;
            case 51:
                strFieldName = "PaytoDate";
                break;
            case 52:
                strFieldName = "PayEndDate";
                break;
            case 53:
                strFieldName = "SignDate";
                break;
            case 54:
                strFieldName = "CValiDate";
                break;
            case 55:
                strFieldName = "PayEndYear";
                break;
            case 56:
                strFieldName = "PayMoney";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "SPAYPERSONID":
                return Schema.TYPE_LONG;
            case "SPAYID":
                return Schema.TYPE_LONG;
            case "SHARDINGID":
                return Schema.TYPE_STRING;
            case "POLNO":
                return Schema.TYPE_STRING;
            case "PAYCOUNT":
                return Schema.TYPE_INT;
            case "GRPCONTNO":
                return Schema.TYPE_STRING;
            case "GRPPOLNO":
                return Schema.TYPE_STRING;
            case "CONTNO":
                return Schema.TYPE_STRING;
            case "MANAGECOM":
                return Schema.TYPE_STRING;
            case "AGENTCOM":
                return Schema.TYPE_STRING;
            case "AGENTTYPE":
                return Schema.TYPE_STRING;
            case "RISKCODE":
                return Schema.TYPE_STRING;
            case "AGENTCODE":
                return Schema.TYPE_STRING;
            case "AGENTGROUP":
                return Schema.TYPE_STRING;
            case "PAYTYPEFLAG":
                return Schema.TYPE_STRING;
            case "APPNTNO":
                return Schema.TYPE_STRING;
            case "GETNOTICENO":
                return Schema.TYPE_STRING;
            case "PAYAIMCLASS":
                return Schema.TYPE_STRING;
            case "DUTYCODE":
                return Schema.TYPE_STRING;
            case "PAYPLANCODE":
                return Schema.TYPE_STRING;
            case "SUMDUEPAYMONEY":
                return Schema.TYPE_DOUBLE;
            case "SUMACTUPAYMONEY":
                return Schema.TYPE_DOUBLE;
            case "PAYINTV":
                return Schema.TYPE_INT;
            case "PAYDATE":
                return Schema.TYPE_DATE;
            case "PAYTYPE":
                return Schema.TYPE_STRING;
            case "LASTPAYTODATE":
                return Schema.TYPE_DATE;
            case "CURPAYTODATE":
                return Schema.TYPE_DATE;
            case "ININSUACCSTATE":
                return Schema.TYPE_STRING;
            case "BANKCODE":
                return Schema.TYPE_STRING;
            case "BANKACCNO":
                return Schema.TYPE_STRING;
            case "BANKONTHEWAYFLAG":
                return Schema.TYPE_STRING;
            case "BANKSUCCFLAG":
                return Schema.TYPE_STRING;
            case "APPROVECODE":
                return Schema.TYPE_STRING;
            case "APPROVEDATE":
                return Schema.TYPE_DATE;
            case "APPROVETIME":
                return Schema.TYPE_STRING;
            case "SERIALNO":
                return Schema.TYPE_STRING;
            case "INPUTFLAG":
                return Schema.TYPE_STRING;
            case "OPERATOR":
                return Schema.TYPE_STRING;
            case "MAKEDATE":
                return Schema.TYPE_DATE;
            case "MAKETIME":
                return Schema.TYPE_STRING;
            case "MODIFYDATE":
                return Schema.TYPE_DATE;
            case "MODIFYTIME":
                return Schema.TYPE_STRING;
            case "EDORNO":
                return Schema.TYPE_STRING;
            case "MAINPOLYEAR":
                return Schema.TYPE_INT;
            case "OTHERNO":
                return Schema.TYPE_STRING;
            case "OTHERNOTYPE":
                return Schema.TYPE_STRING;
            case "PRTNO":
                return Schema.TYPE_STRING;
            case "SALECHNL":
                return Schema.TYPE_STRING;
            case "CHARGECOM":
                return Schema.TYPE_STRING;
            case "APPNTNAME":
                return Schema.TYPE_STRING;
            case "KINDCODE":
                return Schema.TYPE_STRING;
            case "PAYTODATE":
                return Schema.TYPE_DATE;
            case "PAYENDDATE":
                return Schema.TYPE_DATE;
            case "SIGNDATE":
                return Schema.TYPE_DATE;
            case "CVALIDATE":
                return Schema.TYPE_DATE;
            case "PAYENDYEAR":
                return Schema.TYPE_INT;
            case "PAYMONEY":
                return Schema.TYPE_DOUBLE;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_LONG;
            case 1:
                return Schema.TYPE_LONG;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_INT;
            case 5:
                return Schema.TYPE_STRING;
            case 6:
                return Schema.TYPE_STRING;
            case 7:
                return Schema.TYPE_STRING;
            case 8:
                return Schema.TYPE_STRING;
            case 9:
                return Schema.TYPE_STRING;
            case 10:
                return Schema.TYPE_STRING;
            case 11:
                return Schema.TYPE_STRING;
            case 12:
                return Schema.TYPE_STRING;
            case 13:
                return Schema.TYPE_STRING;
            case 14:
                return Schema.TYPE_STRING;
            case 15:
                return Schema.TYPE_STRING;
            case 16:
                return Schema.TYPE_STRING;
            case 17:
                return Schema.TYPE_STRING;
            case 18:
                return Schema.TYPE_STRING;
            case 19:
                return Schema.TYPE_STRING;
            case 20:
                return Schema.TYPE_DOUBLE;
            case 21:
                return Schema.TYPE_DOUBLE;
            case 22:
                return Schema.TYPE_INT;
            case 23:
                return Schema.TYPE_DATE;
            case 24:
                return Schema.TYPE_STRING;
            case 25:
                return Schema.TYPE_DATE;
            case 26:
                return Schema.TYPE_DATE;
            case 27:
                return Schema.TYPE_STRING;
            case 28:
                return Schema.TYPE_STRING;
            case 29:
                return Schema.TYPE_STRING;
            case 30:
                return Schema.TYPE_STRING;
            case 31:
                return Schema.TYPE_STRING;
            case 32:
                return Schema.TYPE_STRING;
            case 33:
                return Schema.TYPE_DATE;
            case 34:
                return Schema.TYPE_STRING;
            case 35:
                return Schema.TYPE_STRING;
            case 36:
                return Schema.TYPE_STRING;
            case 37:
                return Schema.TYPE_STRING;
            case 38:
                return Schema.TYPE_DATE;
            case 39:
                return Schema.TYPE_STRING;
            case 40:
                return Schema.TYPE_DATE;
            case 41:
                return Schema.TYPE_STRING;
            case 42:
                return Schema.TYPE_STRING;
            case 43:
                return Schema.TYPE_INT;
            case 44:
                return Schema.TYPE_STRING;
            case 45:
                return Schema.TYPE_STRING;
            case 46:
                return Schema.TYPE_STRING;
            case 47:
                return Schema.TYPE_STRING;
            case 48:
                return Schema.TYPE_STRING;
            case 49:
                return Schema.TYPE_STRING;
            case 50:
                return Schema.TYPE_STRING;
            case 51:
                return Schema.TYPE_DATE;
            case 52:
                return Schema.TYPE_DATE;
            case 53:
                return Schema.TYPE_DATE;
            case 54:
                return Schema.TYPE_DATE;
            case 55:
                return Schema.TYPE_INT;
            case 56:
                return Schema.TYPE_DOUBLE;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
}
