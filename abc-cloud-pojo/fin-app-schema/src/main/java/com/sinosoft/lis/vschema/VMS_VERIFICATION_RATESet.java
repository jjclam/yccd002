/**
 * Copyright (c) 2017 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.vschema;

import com.sinosoft.lis.schema.VMS_VERIFICATION_RATESchema;
import com.sinosoft.utility.SchemaSet;
import com.sinosoft.utility.SysConst;

/**
 * <p>ClassName: VMS_VERIFICATION_RATESet </p>
 * <p>Description: VMS_VERIFICATION_RATESchemaSet类文件 </p>
 * <p>Copyright: Copyright (c) 2017</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2017-10-21
 */
public class VMS_VERIFICATION_RATESet extends SchemaSet {
	// @Method
	public boolean add(VMS_VERIFICATION_RATESchema aSchema) {
		return super.add(aSchema);
	}

	public boolean add(VMS_VERIFICATION_RATESet aSet) {
		return super.add(aSet);
	}

	public boolean remove(VMS_VERIFICATION_RATESchema aSchema) {
		return super.remove(aSchema);
	}

	public VMS_VERIFICATION_RATESchema get(int index) {
		VMS_VERIFICATION_RATESchema tSchema = (VMS_VERIFICATION_RATESchema)super.getObj(index);
		return tSchema;
	}

	public boolean set(int index, VMS_VERIFICATION_RATESchema aSchema) {
		return super.set(index,aSchema);
	}

	public boolean set(VMS_VERIFICATION_RATESet aSet) {
		return super.set(aSet);
	}

	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpVMS_VERIFICATION_RATE描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode() {
		StringBuffer strReturn = new StringBuffer("");
		int n = this.size();
		for (int i = 1; i <= n; i++) {
			VMS_VERIFICATION_RATESchema aSchema = this.get(i);
			strReturn.append(aSchema.encode());
			if( i != n ) strReturn.append(SysConst.RECORDSPLITER);
		}

		return strReturn.toString();
	}

	/**
	* 数据解包
	* @param: str String 打包后字符串
	* @return: boolean
	**/
	public boolean decode( String str ) {
		int nBeginPos = 0;
		int nEndPos = str.indexOf('^');
		this.clear();

		while( nEndPos != -1 ) {
			VMS_VERIFICATION_RATESchema aSchema = new VMS_VERIFICATION_RATESchema();
			if (aSchema.decode(str.substring(nBeginPos, nEndPos))) {
			    this.add(aSchema);
			    nBeginPos = nEndPos + 1;
			    nEndPos = str.indexOf('^', nEndPos + 1);
			} else {
				// @@错误处理
				this.mErrors.copyAllErrors( aSchema.mErrors );
				return false;
			}
		}
		VMS_VERIFICATION_RATESchema tSchema = new VMS_VERIFICATION_RATESchema();
		if(tSchema.decode(str.substring(nBeginPos))) {
		    this.add(tSchema);
		    return true;
		} else {
			// @@错误处理
			this.mErrors.copyAllErrors( tSchema.mErrors );
			return false;
		}
	}

}
