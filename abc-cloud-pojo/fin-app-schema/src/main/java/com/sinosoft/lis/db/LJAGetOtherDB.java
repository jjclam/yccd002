/**
 * Copyright (c) 2018 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.db;

import com.sinosoft.lis.schema.LJAGetOtherSchema;
import com.sinosoft.lis.vschema.LJAGetOtherSet;
import com.sinosoft.utility.*;

import java.sql.*;

/**
 * <p>ClassName: LJAGetOtherDB </p>
 * <p>Description: DB层数据库操作类文件 </p>
 * <p>Copyright: Copyright (c) 2018</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2018-09-07
 */
public class LJAGetOtherDB extends LJAGetOtherSchema {
    // @Field
    private Connection con;
    private DBOper db;
    /**
     * flag = true: 传入Connection
     * flag = false: 不传入Connection
     **/
    private boolean mflag = false;

    public CErrors mErrors = new CErrors();        // 错误信息

    /**
     * 为批量操作而准备的语句和游标对象
     */
    private ResultSet mResultSet = null;
    private Statement mStatement = null;
    // @Constructor
    public LJAGetOtherDB( Connection tConnection ) {
        con = tConnection;
        db = new DBOper( con, "LJAGetOther" );
        mflag = true;
    }

    public LJAGetOtherDB() {
        con = null;
        db = new DBOper( "LJAGetOther" );
        mflag = false;
    }

    // @Method
    public boolean deleteSQL() {
        LJAGetOtherSchema tSchema = this.getSchema();
        if (db.deleteSQL(tSchema)) {
            return true;
        } else {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LJAGetOtherDB";
            tError.functionName = "deleteSQL";
            tError.errorMessage = "操作失败!";
            this.mErrors .addOneError(tError);
            return false;
        }
    }

    public int getCount() {
        LJAGetOtherSchema tSchema = this.getSchema();

        int tCount = db.getCount(tSchema);
        if (tCount < 0) {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LJAGetOtherDB";
            tError.functionName = "getCount";
            tError.errorMessage = "操作失败!";
            this.mErrors .addOneError(tError);

            return -1;
        }

        return tCount;
    }

    public boolean delete() {
        PreparedStatement pstmt = null;

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try {
            pstmt = con.prepareStatement("DELETE FROM LJAGetOther WHERE  1=1  AND ActuGetNo = ? AND OtherNo = ? AND OtherNoType = ?");
            if(this.getActuGetNo() == null || this.getActuGetNo().equals("null")) {
                pstmt.setNull(1, 12);
            } else {
                pstmt.setString(1, this.getActuGetNo());
            }
            if(this.getOtherNo() == null || this.getOtherNo().equals("null")) {
                pstmt.setNull(2, 12);
            } else {
                pstmt.setString(2, this.getOtherNo());
            }
            if(this.getOtherNoType() == null || this.getOtherNoType().equals("null")) {
                pstmt.setNull(3, 12);
            } else {
                pstmt.setString(3, this.getOtherNoType());
            }
            pstmt.executeUpdate();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LJAGetOtherDB";
            tError.functionName = "delete()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);
            ex.printStackTrace();

            try {
                pstmt.close();
            } catch (Exception e){}

            if(!mflag) {
                try {
                    con.close();
                } catch (Exception e){}
            }

            return false;
        }

        if(!mflag) {
            try {
                con.close();
            } catch (Exception e){}
        }

        return true;
    }

    public boolean update() {
        PreparedStatement pstmt = null;

        SQLString sqlObj = new SQLString("LJAGetOther");
        sqlObj.setSQL(2, this);
        String sql = sqlObj.getSQL();

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try {
            pstmt = con.prepareStatement("UPDATE LJAGetOther SET  ActuGetNo = ? , OtherNo = ? , OtherNoType = ? , PayMode = ? , GetMoney = ? , GetDate = ? , EnterAccDate = ? , ConfDate = ? , ManageCom = ? , AgentCom = ? , AgentType = ? , APPntName = ? , AgentGroup = ? , AgentCode = ? , FeeOperationType = ? , FeeFinaType = ? , SerialNo = ? , Operator = ? , MakeTime = ? , MakeDate = ? , State = ? , GetNoticeNo = ? , ModifyDate = ? , ModifyTime = ? WHERE  1=1  AND ActuGetNo = ? AND OtherNo = ? AND OtherNoType = ?");
            if(this.getActuGetNo() == null || this.getActuGetNo().equals("null")) {
                pstmt.setNull(1, 12);
            } else {
                pstmt.setString(1, this.getActuGetNo());
            }
            if(this.getOtherNo() == null || this.getOtherNo().equals("null")) {
                pstmt.setNull(2, 12);
            } else {
                pstmt.setString(2, this.getOtherNo());
            }
            if(this.getOtherNoType() == null || this.getOtherNoType().equals("null")) {
                pstmt.setNull(3, 12);
            } else {
                pstmt.setString(3, this.getOtherNoType());
            }
            if(this.getPayMode() == null || this.getPayMode().equals("null")) {
                pstmt.setNull(4, 12);
            } else {
                pstmt.setString(4, this.getPayMode());
            }
            pstmt.setDouble(5, this.getGetMoney());
            if(this.getGetDate() == null || this.getGetDate().equals("null")) {
                pstmt.setNull(6, 93);
            } else {
                pstmt.setDate(6, Date.valueOf(this.getGetDate()));
            }
            if(this.getEnterAccDate() == null || this.getEnterAccDate().equals("null")) {
                pstmt.setNull(7, 93);
            } else {
                pstmt.setDate(7, Date.valueOf(this.getEnterAccDate()));
            }
            if(this.getConfDate() == null || this.getConfDate().equals("null")) {
                pstmt.setNull(8, 93);
            } else {
                pstmt.setDate(8, Date.valueOf(this.getConfDate()));
            }
            if(this.getManageCom() == null || this.getManageCom().equals("null")) {
                pstmt.setNull(9, 12);
            } else {
                pstmt.setString(9, this.getManageCom());
            }
            if(this.getAgentCom() == null || this.getAgentCom().equals("null")) {
                pstmt.setNull(10, 12);
            } else {
                pstmt.setString(10, this.getAgentCom());
            }
            if(this.getAgentType() == null || this.getAgentType().equals("null")) {
                pstmt.setNull(11, 12);
            } else {
                pstmt.setString(11, this.getAgentType());
            }
            if(this.getAPPntName() == null || this.getAPPntName().equals("null")) {
                pstmt.setNull(12, 12);
            } else {
                pstmt.setString(12, this.getAPPntName());
            }
            if(this.getAgentGroup() == null || this.getAgentGroup().equals("null")) {
                pstmt.setNull(13, 12);
            } else {
                pstmt.setString(13, this.getAgentGroup());
            }
            if(this.getAgentCode() == null || this.getAgentCode().equals("null")) {
                pstmt.setNull(14, 12);
            } else {
                pstmt.setString(14, this.getAgentCode());
            }
            if(this.getFeeOperationType() == null || this.getFeeOperationType().equals("null")) {
                pstmt.setNull(15, 12);
            } else {
                pstmt.setString(15, this.getFeeOperationType());
            }
            if(this.getFeeFinaType() == null || this.getFeeFinaType().equals("null")) {
                pstmt.setNull(16, 12);
            } else {
                pstmt.setString(16, this.getFeeFinaType());
            }
            if(this.getSerialNo() == null || this.getSerialNo().equals("null")) {
                pstmt.setNull(17, 12);
            } else {
                pstmt.setString(17, this.getSerialNo());
            }
            if(this.getOperator() == null || this.getOperator().equals("null")) {
                pstmt.setNull(18, 12);
            } else {
                pstmt.setString(18, this.getOperator());
            }
            if(this.getMakeTime() == null || this.getMakeTime().equals("null")) {
                pstmt.setNull(19, 12);
            } else {
                pstmt.setString(19, this.getMakeTime());
            }
            if(this.getMakeDate() == null || this.getMakeDate().equals("null")) {
                pstmt.setNull(20, 93);
            } else {
                pstmt.setDate(20, Date.valueOf(this.getMakeDate()));
            }
            if(this.getState() == null || this.getState().equals("null")) {
                pstmt.setNull(21, 12);
            } else {
                pstmt.setString(21, this.getState());
            }
            if(this.getGetNoticeNo() == null || this.getGetNoticeNo().equals("null")) {
                pstmt.setNull(22, 12);
            } else {
                pstmt.setString(22, this.getGetNoticeNo());
            }
            if(this.getModifyDate() == null || this.getModifyDate().equals("null")) {
                pstmt.setNull(23, 93);
            } else {
                pstmt.setDate(23, Date.valueOf(this.getModifyDate()));
            }
            if(this.getModifyTime() == null || this.getModifyTime().equals("null")) {
                pstmt.setNull(24, 12);
            } else {
                pstmt.setString(24, this.getModifyTime());
            }
            // set where condition
            if(this.getActuGetNo() == null || this.getActuGetNo().equals("null")) {
                pstmt.setNull(25, 12);
            } else {
                pstmt.setString(25, this.getActuGetNo());
            }
            if(this.getOtherNo() == null || this.getOtherNo().equals("null")) {
                pstmt.setNull(26, 12);
            } else {
                pstmt.setString(26, this.getOtherNo());
            }
            if(this.getOtherNoType() == null || this.getOtherNoType().equals("null")) {
                pstmt.setNull(27, 12);
            } else {
                pstmt.setString(27, this.getOtherNoType());
            }
            pstmt.executeUpdate();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LJAGetOtherDB";
            tError.functionName = "update()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);
            ex.printStackTrace();

            try {
                pstmt.close();
            } catch (Exception e){}

            if( !mflag ) {
                try {
                    con.close();
                } catch (Exception e){}
            }

            return false;
        }

        if(!mflag) {
            try {
                con.close();
            } catch (Exception e){}
        }

        return true;
    }

    public boolean insert() {
        PreparedStatement pstmt = null;

        SQLString sqlObj = new SQLString("LJAGetOther");
        sqlObj.setSQL(1, this);
        String sql = sqlObj.getSQL();

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try {
            pstmt = con.prepareStatement("INSERT INTO LJAGetOther VALUES( ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ?)");
            if(this.getActuGetNo() == null || this.getActuGetNo().equals("null")) {
                pstmt.setNull(1, 12);
            } else {
                pstmt.setString(1, this.getActuGetNo());
            }
            if(this.getOtherNo() == null || this.getOtherNo().equals("null")) {
                pstmt.setNull(2, 12);
            } else {
                pstmt.setString(2, this.getOtherNo());
            }
            if(this.getOtherNoType() == null || this.getOtherNoType().equals("null")) {
                pstmt.setNull(3, 12);
            } else {
                pstmt.setString(3, this.getOtherNoType());
            }
            if(this.getPayMode() == null || this.getPayMode().equals("null")) {
                pstmt.setNull(4, 12);
            } else {
                pstmt.setString(4, this.getPayMode());
            }
            pstmt.setDouble(5, this.getGetMoney());
            if(this.getGetDate() == null || this.getGetDate().equals("null")) {
                pstmt.setNull(6, 93);
            } else {
                pstmt.setDate(6, Date.valueOf(this.getGetDate()));
            }
            if(this.getEnterAccDate() == null || this.getEnterAccDate().equals("null")) {
                pstmt.setNull(7, 93);
            } else {
                pstmt.setDate(7, Date.valueOf(this.getEnterAccDate()));
            }
            if(this.getConfDate() == null || this.getConfDate().equals("null")) {
                pstmt.setNull(8, 93);
            } else {
                pstmt.setDate(8, Date.valueOf(this.getConfDate()));
            }
            if(this.getManageCom() == null || this.getManageCom().equals("null")) {
                pstmt.setNull(9, 12);
            } else {
                pstmt.setString(9, this.getManageCom());
            }
            if(this.getAgentCom() == null || this.getAgentCom().equals("null")) {
                pstmt.setNull(10, 12);
            } else {
                pstmt.setString(10, this.getAgentCom());
            }
            if(this.getAgentType() == null || this.getAgentType().equals("null")) {
                pstmt.setNull(11, 12);
            } else {
                pstmt.setString(11, this.getAgentType());
            }
            if(this.getAPPntName() == null || this.getAPPntName().equals("null")) {
                pstmt.setNull(12, 12);
            } else {
                pstmt.setString(12, this.getAPPntName());
            }
            if(this.getAgentGroup() == null || this.getAgentGroup().equals("null")) {
                pstmt.setNull(13, 12);
            } else {
                pstmt.setString(13, this.getAgentGroup());
            }
            if(this.getAgentCode() == null || this.getAgentCode().equals("null")) {
                pstmt.setNull(14, 12);
            } else {
                pstmt.setString(14, this.getAgentCode());
            }
            if(this.getFeeOperationType() == null || this.getFeeOperationType().equals("null")) {
                pstmt.setNull(15, 12);
            } else {
                pstmt.setString(15, this.getFeeOperationType());
            }
            if(this.getFeeFinaType() == null || this.getFeeFinaType().equals("null")) {
                pstmt.setNull(16, 12);
            } else {
                pstmt.setString(16, this.getFeeFinaType());
            }
            if(this.getSerialNo() == null || this.getSerialNo().equals("null")) {
                pstmt.setNull(17, 12);
            } else {
                pstmt.setString(17, this.getSerialNo());
            }
            if(this.getOperator() == null || this.getOperator().equals("null")) {
                pstmt.setNull(18, 12);
            } else {
                pstmt.setString(18, this.getOperator());
            }
            if(this.getMakeTime() == null || this.getMakeTime().equals("null")) {
                pstmt.setNull(19, 12);
            } else {
                pstmt.setString(19, this.getMakeTime());
            }
            if(this.getMakeDate() == null || this.getMakeDate().equals("null")) {
                pstmt.setNull(20, 93);
            } else {
                pstmt.setDate(20, Date.valueOf(this.getMakeDate()));
            }
            if(this.getState() == null || this.getState().equals("null")) {
                pstmt.setNull(21, 12);
            } else {
                pstmt.setString(21, this.getState());
            }
            if(this.getGetNoticeNo() == null || this.getGetNoticeNo().equals("null")) {
                pstmt.setNull(22, 12);
            } else {
                pstmt.setString(22, this.getGetNoticeNo());
            }
            if(this.getModifyDate() == null || this.getModifyDate().equals("null")) {
                pstmt.setNull(23, 93);
            } else {
                pstmt.setDate(23, Date.valueOf(this.getModifyDate()));
            }
            if(this.getModifyTime() == null || this.getModifyTime().equals("null")) {
                pstmt.setNull(24, 12);
            } else {
                pstmt.setString(24, this.getModifyTime());
            }
            // execute sql
            pstmt.executeUpdate();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LJAGetOtherDB";
            tError.functionName = "insert()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors.addOneError(tError);
            ex.printStackTrace();

            try {
                pstmt.close();
            } catch (Exception e){}

            if( !mflag ) {
                try {
                    con.close();
                } catch (Exception e){}
            }

            return false;
        }

        if(!mflag) {
            try {
                con.close();
            } catch (Exception e){}
        }

        return true;
    }

    public boolean getInfo() {
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        if(!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            pstmt = con.prepareStatement("SELECT * FROM LJAGetOther WHERE  1=1  AND ActuGetNo = ? AND OtherNo = ? AND OtherNoType = ?",
                    ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
            if(this.getActuGetNo() == null || this.getActuGetNo().equals("null")) {
                pstmt.setNull(1, 12);
            } else {
                pstmt.setString(1, this.getActuGetNo());
            }
            if(this.getOtherNo() == null || this.getOtherNo().equals("null")) {
                pstmt.setNull(2, 12);
            } else {
                pstmt.setString(2, this.getOtherNo());
            }
            if(this.getOtherNoType() == null || this.getOtherNoType().equals("null")) {
                pstmt.setNull(3, 12);
            } else {
                pstmt.setString(3, this.getOtherNoType());
            }
            rs = pstmt.executeQuery();
            int i = 0;
            while (rs.next()) {
                i++;
                if (!this.setSchema(rs,i)) {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "LJAGetOtherDB";
                    tError.functionName = "getInfo";
                    tError.errorMessage = "取数失败!";
                    this.mErrors .addOneError(tError);

                    try{ rs.close(); } catch( Exception ex ) {}
                    try{ pstmt.close(); } catch( Exception ex1 ) {}

                    if (!mflag) {
                        try {
                            con.close();
                        }
                        catch(Exception et){}
                    }
                    return false;
                }
                break;
            }
            try{ rs.close(); } catch( Exception ex2 ) {}
            try{ pstmt.close(); } catch( Exception ex3 ) {}

            if( i == 0 ) {
                if (!mflag) {
                    try {
                        con.close();
                    }
                    catch(Exception et){}
                }
                return false;
            }
        }
        catch(Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LJAGetOtherDB";
            tError.functionName = "getInfo";
            tError.errorMessage = ExceptionUtils.exceptionToString(e);
            this.mErrors .addOneError(tError);

            try{ rs.close(); } catch( Exception ex ) {}
            try{ pstmt.close(); } catch( Exception ex1 ) {}

            if (!mflag) {
                try {
                    con.close();
                }
                catch(Exception et){}
            }
            return false;
        }
        // 断开数据库连接
        if (!mflag) {
            try {
                con.close();
            }
            catch(Exception e){}
        }

        return true;
    }

    public LJAGetOtherSet query() {
        Statement stmt = null;
        ResultSet rs = null;
        LJAGetOtherSet aLJAGetOtherSet = new LJAGetOtherSet();

        if (!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            stmt = con.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
            SQLString sqlObj = new SQLString("LJAGetOther");
            LJAGetOtherSchema aSchema = this.getSchema();
            sqlObj.setSQL(5, aSchema);
            String sql = sqlObj.getSQL();

            rs = stmt.executeQuery(sql);
            int i = 0;
            while (rs.next()) {
                i++;
                if (i>10000) {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "LJAGetOtherDB";
                    tError.functionName = "query";
                    tError.errorMessage = "SQL语句返回结果集太多，请换用其他方式!";
                    this.mErrors .addOneError(tError);
                    break;
                }
                LJAGetOtherSchema s1 = new LJAGetOtherSchema();
                s1.setSchema(rs,i);
                aLJAGetOtherSet.add(s1);
            }
            try{ rs.close(); } catch( Exception ex ) {}
            try{ stmt.close(); } catch( Exception ex1 ) {}
        }
        catch(Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LJAGetOtherDB";
            tError.functionName = "query";
            tError.errorMessage = ExceptionUtils.exceptionToString(e);
            this.mErrors.addOneError(tError);
            e.printStackTrace();

            try { rs.close(); } catch (Exception ex2) { ex2.printStackTrace(); }
            try { stmt.close(); } catch(Exception ex3) { ex3.printStackTrace(); }

            if (!mflag) {
                try {
                    con.close();
                }
                catch(Exception et){et.printStackTrace();}
            }
        }

        if (!mflag) {
            try {
                con.close();
            }
            catch(Exception e){e.printStackTrace();}
        }

        return aLJAGetOtherSet;
    }

    public LJAGetOtherSet executeQuery(String sql)  {
        Statement stmt = null;
        ResultSet rs = null;
        LJAGetOtherSet aLJAGetOtherSet = new LJAGetOtherSet();

        if (!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            stmt = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);

            rs = stmt.executeQuery(StrTool.GBKToUnicode(sql));
            int i = 0;
            while (rs.next()) {
                i++;
                if (i>10000) {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "LJAGetOtherDB";
                    tError.functionName = "executeQuery";
                    tError.errorMessage = "SQL语句返回结果集太多，请换用其他方式!";
                    this.mErrors .addOneError(tError);
                    break;
                }
                LJAGetOtherSchema s1 = new LJAGetOtherSchema();
                if (!s1.setSchema(rs,i)) {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "LJAGetOtherDB";
                    tError.functionName = "executeQuery";
                    tError.errorMessage = "sql语句有误，请查看表名及字段名信息!";
                    this.mErrors .addOneError(tError);
                }
                aLJAGetOtherSet.add(s1);
            }
            try{ rs.close(); } catch( Exception ex ) {}
            try{ stmt.close(); } catch( Exception ex1 ) {}
        }
        catch(Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LJAGetOtherDB";
            tError.functionName = "executeQuery";
            tError.errorMessage = ExceptionUtils.exceptionToString(e);
            this.mErrors .addOneError(tError);

            try{ rs.close(); } catch( Exception ex2 ) {}
            try{ stmt.close(); } catch( Exception ex3 ) {}

            if (!mflag) {
                try {
                    con.close();
                }
                catch(Exception et){}
            }
        }

        if (!mflag) {
            try {
                con.close();
            }
            catch(Exception e){}
        }

        return aLJAGetOtherSet;
    }

    public LJAGetOtherSet query(int nStart, int nCount) {
        Statement stmt = null;
        ResultSet rs = null;
        LJAGetOtherSet aLJAGetOtherSet = new LJAGetOtherSet();

        if (!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            stmt = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);
            SQLString sqlObj = new SQLString("LJAGetOther");
            LJAGetOtherSchema aSchema = this.getSchema();
            sqlObj.setSQL(5,aSchema);
            String sql = sqlObj.getSQL();

            rs = stmt.executeQuery(sql);
            int i = 0;
            while (rs.next()) {
                i++;

                if( i < nStart ) {
                    continue;
                }

                if( i >= nStart + nCount ) {
                    break;
                }

                LJAGetOtherSchema s1 = new LJAGetOtherSchema();
                s1.setSchema(rs,i);
                aLJAGetOtherSet.add(s1);
            }
            try{ rs.close(); } catch( Exception ex ) {}
            try{ stmt.close(); } catch( Exception ex1 ) {}
        }
        catch(Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LJAGetOtherDB";
            tError.functionName = "query";
            tError.errorMessage = ExceptionUtils.exceptionToString(e);
            this.mErrors .addOneError(tError);

            try{ rs.close(); } catch( Exception ex2 ) {}
            try{ stmt.close(); } catch( Exception ex3 ) {}

            if (!mflag) {
                try {
                    con.close();
                }
                catch(Exception et){}
            }
        }

        if (!mflag) {
            try {
                con.close();
            }
            catch(Exception e){}
        }

        return aLJAGetOtherSet;
    }

    public LJAGetOtherSet executeQuery(String sql, int nStart, int nCount) {
        Statement stmt = null;
        ResultSet rs = null;
        LJAGetOtherSet aLJAGetOtherSet = new LJAGetOtherSet();

        if (!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            stmt = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);

            rs = stmt.executeQuery(StrTool.GBKToUnicode(sql));
            int i = 0;
            while (rs.next()) {
                i++;

                if( i < nStart ) {
                    continue;
                }

                if( i >= nStart + nCount ) {
                    break;
                }

                LJAGetOtherSchema s1 = new LJAGetOtherSchema();
                if (!s1.setSchema(rs,i)) {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "LJAGetOtherDB";
                    tError.functionName = "executeQuery";
                    tError.errorMessage = "sql语句有误，请查看表名及字段名信息!";
                    this.mErrors .addOneError(tError);
                }
                aLJAGetOtherSet.add(s1);
            }
            try{ rs.close(); } catch( Exception ex ) {}
            try{ stmt.close(); } catch( Exception ex1 ) {}
        }
        catch(Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LJAGetOtherDB";
            tError.functionName = "executeQuery";
            tError.errorMessage = ExceptionUtils.exceptionToString(e);
            this.mErrors .addOneError(tError);

            try{ rs.close(); } catch( Exception ex2 ) {}
            try{ stmt.close(); } catch( Exception ex3 ) {}

            if (!mflag) {
                try {
                    con.close();
                }
                catch(Exception et){}
            }
        }

        if (!mflag) {
            try {
                con.close();
            }
            catch(Exception e){}
        }

        return aLJAGetOtherSet;
    }

    public boolean update(String strWherePart) {
        Statement stmt = null;

        if (!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            stmt = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);
            SQLString sqlObj = new SQLString("LJAGetOther");
            LJAGetOtherSchema aSchema = this.getSchema();
            sqlObj.setSQL(2,aSchema);
            String sql = "update LJAGetOther " + sqlObj.getUpdPart() + " where " + strWherePart;

            int operCount = stmt.executeUpdate(sql);
            if (operCount == 0) {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "LJAGetOtherDB";
                tError.functionName = "update";
                tError.errorMessage = "更新数据失败!";
                this.mErrors .addOneError(tError);

                if (!mflag) {
                    try {
                        con.close();
                    }
                    catch(Exception et){}
                }
                return false;
            }
        }
        catch(Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LJAGetOtherDB";
            tError.functionName = "update";
            tError.errorMessage = ExceptionUtils.exceptionToString(e);
            this.mErrors .addOneError(tError);

            try{ stmt.close(); } catch( Exception ex1 ) {}

            if (!mflag) {
                try {
                    con.close();
                }
                catch(Exception et){}
            }
            return false;
        }
        // 断开数据库连接
        if (!mflag) {
            try {
                con.close();
            }
            catch(Exception e){}
        }

        return true;
    }

    /**
     * 准备数据查询条件
     * @param strSQL String
     * @return boolean
     */
    public boolean prepareData(String strSQL) {
        if (mResultSet != null) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LJAGetOtherDB";
            tError.functionName = "prepareData";
            tError.errorMessage = "数据集非空，程序在准备数据集之后，没有关闭！";
            this.mErrors.addOneError(tError);
            return false;
        }

        if (!mflag) {
            con = DBConnPool.getConnection();
        }
        try {
            mStatement = con.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
            mResultSet = mStatement.executeQuery(StrTool.GBKToUnicode(strSQL));
        }
        catch (Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LJAGetOtherDB";
            tError.functionName = "prepareData";
            tError.errorMessage = ExceptionUtils.exceptionToString(e);
            this.mErrors.addOneError(tError);
            try {
                mResultSet.close();
            }
            catch (Exception ex2) {}
            try {
                mStatement.close();
            }
            catch (Exception ex3) {}
            if (!mflag) {
                try {
                    con.close();
                }
                catch (Exception et) {}
            }
            return false;
        }

        if (!mflag) {
            try {
                con.close();
            }
            catch (Exception e) {}
        }
        return true;
    }

    /**
     * 获取数据集
     * @return boolean
     */
    public boolean hasMoreData() {
        boolean flag = true;
        if (null == mResultSet) {
            CError tError = new CError();
            tError.moduleName = "LJAGetOtherDB";
            tError.functionName = "hasMoreData";
            tError.errorMessage = "数据集为空，请先准备数据集！";
            this.mErrors.addOneError(tError);
            return false;
        }
        try {
            flag = mResultSet.next();
        }
        catch (Exception ex) {
            CError tError = new CError();
            tError.moduleName = "LJAGetOtherDB";
            tError.functionName = "hasMoreData";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors.addOneError(tError);
            try {
                mResultSet.close();
                mResultSet = null;
            }
            catch (Exception ex2) {}
            try {
                mStatement.close();
                mStatement = null;
            }
            catch (Exception ex3) {}
            if (!mflag) {
                try {
                    con.close();
                }
                catch (Exception et) {}
            }
            return false;
        }
        return flag;
    }
    /**
     * 获取定量数据
     * @return LJAGetOtherSet
     */
    public LJAGetOtherSet getData() {
        int tCount = 0;
        LJAGetOtherSet tLJAGetOtherSet = new LJAGetOtherSet();
        LJAGetOtherSchema tLJAGetOtherSchema = null;
        if (null == mResultSet) {
            CError tError = new CError();
            tError.moduleName = "LJAGetOtherDB";
            tError.functionName = "getData";
            tError.errorMessage = "数据集为空，请先准备数据集！";
            this.mErrors.addOneError(tError);
            return null;
        }
        try {
            tCount = 1;
            tLJAGetOtherSchema = new LJAGetOtherSchema();
            tLJAGetOtherSchema.setSchema(mResultSet, 1);
            tLJAGetOtherSet.add(tLJAGetOtherSchema);
            //注意mResultSet.next()的作用
            while (tCount++ < SysConst.FETCHCOUNT) {
                if (mResultSet.next()) {
                    tLJAGetOtherSchema = new LJAGetOtherSchema();
                    tLJAGetOtherSchema.setSchema(mResultSet, 1);
                    tLJAGetOtherSet.add(tLJAGetOtherSchema);
                }
            }
        }
        catch (Exception ex) {
            CError tError = new CError();
            tError.moduleName = "LJAGetOtherDB";
            tError.functionName = "getData";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors.addOneError(tError);
            try {
                mResultSet.close();
                mResultSet = null;
            }
            catch (Exception ex2) {}
            try {
                mStatement.close();
                mStatement = null;
            }
            catch (Exception ex3) {}
            if (!mflag) {
                try {
                    con.close();
                }
                catch (Exception et) {}
            }
            return null;
        }
        return tLJAGetOtherSet;
    }
    /**
     * 关闭数据集
     * @return boolean
     */
    public boolean closeData() {
        boolean flag = true;
        try {
            if (null == mResultSet) {
                CError tError = new CError();
                tError.moduleName = "LJAGetOtherDB";
                tError.functionName = "closeData";
                tError.errorMessage = "数据集已经关闭了！";
                this.mErrors.addOneError(tError);
                flag = false;
            } else {
                mResultSet.close();
                mResultSet = null;
            }
        }
        catch (Exception ex2) {
            CError tError = new CError();
            tError.moduleName = "LJAGetOtherDB";
            tError.functionName = "closeData";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex2);
            this.mErrors.addOneError(tError);
            flag = false;
        }
        try {
            if (null == mStatement) {
                CError tError = new CError();
                tError.moduleName = "LJAGetOtherDB";
                tError.functionName = "closeData";
                tError.errorMessage = "语句已经关闭了！";
                this.mErrors.addOneError(tError);
                flag = false;
            } else {
                mStatement.close();
                mStatement = null;
            }
        }
        catch (Exception ex3) {
            CError tError = new CError();
            tError.moduleName = "LJAGetOtherDB";
            tError.functionName = "closeData";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex3);
            this.mErrors.addOneError(tError);
            flag = false;
        }
        return flag;
    }
}
