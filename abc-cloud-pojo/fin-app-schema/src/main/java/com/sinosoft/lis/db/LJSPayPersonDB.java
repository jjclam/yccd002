/**
 * Copyright (c) 2017 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.db;

import com.sinosoft.lis.schema.LJSPayPersonSchema;
import com.sinosoft.lis.vschema.LJSPayPersonSet;
import com.sinosoft.utility.*;

import java.sql.*;

/**
 * <p>ClassName: LJSPayPersonDB </p>
 * <p>Description: DB层数据库操作类文件 </p>
 * <p>Copyright: Copyright (c) 2017</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2017-10-21
 */
public class LJSPayPersonDB extends LJSPayPersonSchema {
    // @Field
    private Connection con;
    private DBOper db;
    /**
    * flag = true: 传入Connection
    * flag = false: 不传入Connection
    **/
    private boolean mflag = false;

    public CErrors mErrors = new CErrors();        // 错误信息

    /**
     * 为批量操作而准备的语句和游标对象
     */
    private ResultSet mResultSet = null;
    private Statement mStatement = null;
    // @Constructor
    public LJSPayPersonDB( Connection tConnection ) {
        con = tConnection;
        db = new DBOper( con, "LJSPayPerson" );
        mflag = true;
    }

    public LJSPayPersonDB() {
        con = null;
        db = new DBOper( "LJSPayPerson" );
        mflag = false;
    }

    // @Method
    public boolean deleteSQL() {
        LJSPayPersonSchema tSchema = this.getSchema();
        if (db.deleteSQL(tSchema)) {
             return true;
        } else {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LJSPayPersonDB";
            tError.functionName = "deleteSQL";
            tError.errorMessage = "操作失败!";
            this.mErrors .addOneError(tError);
            return false;
        }
    }

    public int getCount() {
        LJSPayPersonSchema tSchema = this.getSchema();

        int tCount = db.getCount(tSchema);
        if (tCount < 0) {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LJSPayPersonDB";
            tError.functionName = "getCount";
            tError.errorMessage = "操作失败!";
            this.mErrors .addOneError(tError);

            return -1;
        }

        return tCount;
    }

    public boolean delete() {
        PreparedStatement pstmt = null;

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try {
            pstmt = con.prepareStatement("DELETE FROM LJSPayPerson WHERE  1=1  AND SPayPersonID = ?");
            pstmt.setLong(1, this.getSPayPersonID());
            pstmt.executeUpdate();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LJSPayPersonDB";
            tError.functionName = "delete()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);
            ex.printStackTrace();

            try {
                pstmt.close();
            } catch (Exception e){}

            if(!mflag) {
                try {
                    con.close();
                } catch (Exception e){}
            }

            return false;
        }

        if(!mflag) {
            try {
                con.close();
            } catch (Exception e){}
        }

        return true;
    }

    public boolean update() {
        PreparedStatement pstmt = null;

        SQLString sqlObj = new SQLString("LJSPayPerson");
        sqlObj.setSQL(2, this);
        String sql = sqlObj.getSQL();

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try {
            pstmt = con.prepareStatement("UPDATE LJSPayPerson SET  SPayPersonID = ? , SPayID = ? , ShardingID = ? , PolNo = ? , PayCount = ? , GrpContNo = ? , GrpPolNo = ? , ContNo = ? , ManageCom = ? , AgentCom = ? , AgentType = ? , RiskCode = ? , AgentCode = ? , AgentGroup = ? , PayTypeFlag = ? , AppntNo = ? , GetNoticeNo = ? , PayAimClass = ? , DutyCode = ? , PayPlanCode = ? , SumDuePayMoney = ? , SumActuPayMoney = ? , PayIntv = ? , PayDate = ? , PayType = ? , LastPayToDate = ? , CurPayToDate = ? , InInsuAccState = ? , BankCode = ? , BankAccNo = ? , BankOnTheWayFlag = ? , BankSuccFlag = ? , ApproveCode = ? , ApproveDate = ? , ApproveTime = ? , SerialNo = ? , InputFlag = ? , Operator = ? , MakeDate = ? , MakeTime = ? , ModifyDate = ? , ModifyTime = ? , EdorNo = ? , MainPolYear = ? , OtherNo = ? , OtherNoType = ? , PrtNo = ? , SaleChnl = ? , ChargeCom = ? , APPntName = ? , KindCode = ? , PaytoDate = ? , PayEndDate = ? , SignDate = ? , CValiDate = ? , PayEndYear = ? , PayMoney = ? WHERE  1=1  AND SPayPersonID = ?");
            pstmt.setLong(1, this.getSPayPersonID());
            pstmt.setLong(2, this.getSPayID());
            if(this.getShardingID() == null || this.getShardingID().equals("null")) {
            	pstmt.setNull(3, 12);
            } else {
            	pstmt.setString(3, this.getShardingID());
            }
            if(this.getPolNo() == null || this.getPolNo().equals("null")) {
            	pstmt.setNull(4, 12);
            } else {
            	pstmt.setString(4, this.getPolNo());
            }
            pstmt.setInt(5, this.getPayCount());
            if(this.getGrpContNo() == null || this.getGrpContNo().equals("null")) {
            	pstmt.setNull(6, 12);
            } else {
            	pstmt.setString(6, this.getGrpContNo());
            }
            if(this.getGrpPolNo() == null || this.getGrpPolNo().equals("null")) {
            	pstmt.setNull(7, 12);
            } else {
            	pstmt.setString(7, this.getGrpPolNo());
            }
            if(this.getContNo() == null || this.getContNo().equals("null")) {
            	pstmt.setNull(8, 12);
            } else {
            	pstmt.setString(8, this.getContNo());
            }
            if(this.getManageCom() == null || this.getManageCom().equals("null")) {
            	pstmt.setNull(9, 12);
            } else {
            	pstmt.setString(9, this.getManageCom());
            }
            if(this.getAgentCom() == null || this.getAgentCom().equals("null")) {
            	pstmt.setNull(10, 12);
            } else {
            	pstmt.setString(10, this.getAgentCom());
            }
            if(this.getAgentType() == null || this.getAgentType().equals("null")) {
            	pstmt.setNull(11, 12);
            } else {
            	pstmt.setString(11, this.getAgentType());
            }
            if(this.getRiskCode() == null || this.getRiskCode().equals("null")) {
            	pstmt.setNull(12, 12);
            } else {
            	pstmt.setString(12, this.getRiskCode());
            }
            if(this.getAgentCode() == null || this.getAgentCode().equals("null")) {
            	pstmt.setNull(13, 12);
            } else {
            	pstmt.setString(13, this.getAgentCode());
            }
            if(this.getAgentGroup() == null || this.getAgentGroup().equals("null")) {
            	pstmt.setNull(14, 12);
            } else {
            	pstmt.setString(14, this.getAgentGroup());
            }
            if(this.getPayTypeFlag() == null || this.getPayTypeFlag().equals("null")) {
            	pstmt.setNull(15, 12);
            } else {
            	pstmt.setString(15, this.getPayTypeFlag());
            }
            if(this.getAppntNo() == null || this.getAppntNo().equals("null")) {
            	pstmt.setNull(16, 12);
            } else {
            	pstmt.setString(16, this.getAppntNo());
            }
            if(this.getGetNoticeNo() == null || this.getGetNoticeNo().equals("null")) {
            	pstmt.setNull(17, 12);
            } else {
            	pstmt.setString(17, this.getGetNoticeNo());
            }
            if(this.getPayAimClass() == null || this.getPayAimClass().equals("null")) {
            	pstmt.setNull(18, 12);
            } else {
            	pstmt.setString(18, this.getPayAimClass());
            }
            if(this.getDutyCode() == null || this.getDutyCode().equals("null")) {
            	pstmt.setNull(19, 12);
            } else {
            	pstmt.setString(19, this.getDutyCode());
            }
            if(this.getPayPlanCode() == null || this.getPayPlanCode().equals("null")) {
            	pstmt.setNull(20, 12);
            } else {
            	pstmt.setString(20, this.getPayPlanCode());
            }
            pstmt.setDouble(21, this.getSumDuePayMoney());
            pstmt.setDouble(22, this.getSumActuPayMoney());
            pstmt.setInt(23, this.getPayIntv());
            if(this.getPayDate() == null || this.getPayDate().equals("null")) {
            	pstmt.setNull(24, 93);
            } else {
            	pstmt.setDate(24, Date.valueOf(this.getPayDate()));
            }
            if(this.getPayType() == null || this.getPayType().equals("null")) {
            	pstmt.setNull(25, 12);
            } else {
            	pstmt.setString(25, this.getPayType());
            }
            if(this.getLastPayToDate() == null || this.getLastPayToDate().equals("null")) {
            	pstmt.setNull(26, 93);
            } else {
            	pstmt.setDate(26, Date.valueOf(this.getLastPayToDate()));
            }
            if(this.getCurPayToDate() == null || this.getCurPayToDate().equals("null")) {
            	pstmt.setNull(27, 93);
            } else {
            	pstmt.setDate(27, Date.valueOf(this.getCurPayToDate()));
            }
            if(this.getInInsuAccState() == null || this.getInInsuAccState().equals("null")) {
            	pstmt.setNull(28, 12);
            } else {
            	pstmt.setString(28, this.getInInsuAccState());
            }
            if(this.getBankCode() == null || this.getBankCode().equals("null")) {
            	pstmt.setNull(29, 12);
            } else {
            	pstmt.setString(29, this.getBankCode());
            }
            if(this.getBankAccNo() == null || this.getBankAccNo().equals("null")) {
            	pstmt.setNull(30, 12);
            } else {
            	pstmt.setString(30, this.getBankAccNo());
            }
            if(this.getBankOnTheWayFlag() == null || this.getBankOnTheWayFlag().equals("null")) {
            	pstmt.setNull(31, 12);
            } else {
            	pstmt.setString(31, this.getBankOnTheWayFlag());
            }
            if(this.getBankSuccFlag() == null || this.getBankSuccFlag().equals("null")) {
            	pstmt.setNull(32, 12);
            } else {
            	pstmt.setString(32, this.getBankSuccFlag());
            }
            if(this.getApproveCode() == null || this.getApproveCode().equals("null")) {
            	pstmt.setNull(33, 12);
            } else {
            	pstmt.setString(33, this.getApproveCode());
            }
            if(this.getApproveDate() == null || this.getApproveDate().equals("null")) {
            	pstmt.setNull(34, 93);
            } else {
            	pstmt.setDate(34, Date.valueOf(this.getApproveDate()));
            }
            if(this.getApproveTime() == null || this.getApproveTime().equals("null")) {
            	pstmt.setNull(35, 12);
            } else {
            	pstmt.setString(35, this.getApproveTime());
            }
            if(this.getSerialNo() == null || this.getSerialNo().equals("null")) {
            	pstmt.setNull(36, 12);
            } else {
            	pstmt.setString(36, this.getSerialNo());
            }
            if(this.getInputFlag() == null || this.getInputFlag().equals("null")) {
            	pstmt.setNull(37, 12);
            } else {
            	pstmt.setString(37, this.getInputFlag());
            }
            if(this.getOperator() == null || this.getOperator().equals("null")) {
            	pstmt.setNull(38, 12);
            } else {
            	pstmt.setString(38, this.getOperator());
            }
            if(this.getMakeDate() == null || this.getMakeDate().equals("null")) {
            	pstmt.setNull(39, 93);
            } else {
            	pstmt.setDate(39, Date.valueOf(this.getMakeDate()));
            }
            if(this.getMakeTime() == null || this.getMakeTime().equals("null")) {
            	pstmt.setNull(40, 12);
            } else {
            	pstmt.setString(40, this.getMakeTime());
            }
            if(this.getModifyDate() == null || this.getModifyDate().equals("null")) {
            	pstmt.setNull(41, 93);
            } else {
            	pstmt.setDate(41, Date.valueOf(this.getModifyDate()));
            }
            if(this.getModifyTime() == null || this.getModifyTime().equals("null")) {
            	pstmt.setNull(42, 12);
            } else {
            	pstmt.setString(42, this.getModifyTime());
            }
            if(this.getEdorNo() == null || this.getEdorNo().equals("null")) {
            	pstmt.setNull(43, 12);
            } else {
            	pstmt.setString(43, this.getEdorNo());
            }
            pstmt.setInt(44, this.getMainPolYear());
            if(this.getOtherNo() == null || this.getOtherNo().equals("null")) {
            	pstmt.setNull(45, 12);
            } else {
            	pstmt.setString(45, this.getOtherNo());
            }
            if(this.getOtherNoType() == null || this.getOtherNoType().equals("null")) {
            	pstmt.setNull(46, 12);
            } else {
            	pstmt.setString(46, this.getOtherNoType());
            }
            if(this.getPrtNo() == null || this.getPrtNo().equals("null")) {
            	pstmt.setNull(47, 12);
            } else {
            	pstmt.setString(47, this.getPrtNo());
            }
            if(this.getSaleChnl() == null || this.getSaleChnl().equals("null")) {
            	pstmt.setNull(48, 12);
            } else {
            	pstmt.setString(48, this.getSaleChnl());
            }
            if(this.getChargeCom() == null || this.getChargeCom().equals("null")) {
            	pstmt.setNull(49, 12);
            } else {
            	pstmt.setString(49, this.getChargeCom());
            }
            if(this.getAPPntName() == null || this.getAPPntName().equals("null")) {
            	pstmt.setNull(50, 12);
            } else {
            	pstmt.setString(50, this.getAPPntName());
            }
            if(this.getKindCode() == null || this.getKindCode().equals("null")) {
            	pstmt.setNull(51, 12);
            } else {
            	pstmt.setString(51, this.getKindCode());
            }
            if(this.getPaytoDate() == null || this.getPaytoDate().equals("null")) {
            	pstmt.setNull(52, 93);
            } else {
            	pstmt.setDate(52, Date.valueOf(this.getPaytoDate()));
            }
            if(this.getPayEndDate() == null || this.getPayEndDate().equals("null")) {
            	pstmt.setNull(53, 93);
            } else {
            	pstmt.setDate(53, Date.valueOf(this.getPayEndDate()));
            }
            if(this.getSignDate() == null || this.getSignDate().equals("null")) {
            	pstmt.setNull(54, 93);
            } else {
            	pstmt.setDate(54, Date.valueOf(this.getSignDate()));
            }
            if(this.getCValiDate() == null || this.getCValiDate().equals("null")) {
            	pstmt.setNull(55, 93);
            } else {
            	pstmt.setDate(55, Date.valueOf(this.getCValiDate()));
            }
            pstmt.setInt(56, this.getPayEndYear());
            pstmt.setDouble(57, this.getPayMoney());
            // set where condition
            pstmt.setLong(58, this.getSPayPersonID());
            pstmt.executeUpdate();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LJSPayPersonDB";
            tError.functionName = "update()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);
            ex.printStackTrace();

            try {
                pstmt.close();
            } catch (Exception e){}

            if( !mflag ) {
                try {
                    con.close();
                } catch (Exception e){}
            }

            return false;
        }

        if(!mflag) {
            try {
                con.close();
            } catch (Exception e){}
        }

        return true;
    }

    public boolean insert() {
        PreparedStatement pstmt = null;

        SQLString sqlObj = new SQLString("LJSPayPerson");
        sqlObj.setSQL(1, this);
        String sql = sqlObj.getSQL();

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try {
            pstmt = con.prepareStatement("INSERT INTO LJSPayPerson VALUES( ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ?)");
            pstmt.setLong(1, this.getSPayPersonID());
            pstmt.setLong(2, this.getSPayID());
            if(this.getShardingID() == null || this.getShardingID().equals("null")) {
            	pstmt.setNull(3, 12);
            } else {
            	pstmt.setString(3, this.getShardingID());
            }
            if(this.getPolNo() == null || this.getPolNo().equals("null")) {
            	pstmt.setNull(4, 12);
            } else {
            	pstmt.setString(4, this.getPolNo());
            }
            pstmt.setInt(5, this.getPayCount());
            if(this.getGrpContNo() == null || this.getGrpContNo().equals("null")) {
            	pstmt.setNull(6, 12);
            } else {
            	pstmt.setString(6, this.getGrpContNo());
            }
            if(this.getGrpPolNo() == null || this.getGrpPolNo().equals("null")) {
            	pstmt.setNull(7, 12);
            } else {
            	pstmt.setString(7, this.getGrpPolNo());
            }
            if(this.getContNo() == null || this.getContNo().equals("null")) {
            	pstmt.setNull(8, 12);
            } else {
            	pstmt.setString(8, this.getContNo());
            }
            if(this.getManageCom() == null || this.getManageCom().equals("null")) {
            	pstmt.setNull(9, 12);
            } else {
            	pstmt.setString(9, this.getManageCom());
            }
            if(this.getAgentCom() == null || this.getAgentCom().equals("null")) {
            	pstmt.setNull(10, 12);
            } else {
            	pstmt.setString(10, this.getAgentCom());
            }
            if(this.getAgentType() == null || this.getAgentType().equals("null")) {
            	pstmt.setNull(11, 12);
            } else {
            	pstmt.setString(11, this.getAgentType());
            }
            if(this.getRiskCode() == null || this.getRiskCode().equals("null")) {
            	pstmt.setNull(12, 12);
            } else {
            	pstmt.setString(12, this.getRiskCode());
            }
            if(this.getAgentCode() == null || this.getAgentCode().equals("null")) {
            	pstmt.setNull(13, 12);
            } else {
            	pstmt.setString(13, this.getAgentCode());
            }
            if(this.getAgentGroup() == null || this.getAgentGroup().equals("null")) {
            	pstmt.setNull(14, 12);
            } else {
            	pstmt.setString(14, this.getAgentGroup());
            }
            if(this.getPayTypeFlag() == null || this.getPayTypeFlag().equals("null")) {
            	pstmt.setNull(15, 12);
            } else {
            	pstmt.setString(15, this.getPayTypeFlag());
            }
            if(this.getAppntNo() == null || this.getAppntNo().equals("null")) {
            	pstmt.setNull(16, 12);
            } else {
            	pstmt.setString(16, this.getAppntNo());
            }
            if(this.getGetNoticeNo() == null || this.getGetNoticeNo().equals("null")) {
            	pstmt.setNull(17, 12);
            } else {
            	pstmt.setString(17, this.getGetNoticeNo());
            }
            if(this.getPayAimClass() == null || this.getPayAimClass().equals("null")) {
            	pstmt.setNull(18, 12);
            } else {
            	pstmt.setString(18, this.getPayAimClass());
            }
            if(this.getDutyCode() == null || this.getDutyCode().equals("null")) {
            	pstmt.setNull(19, 12);
            } else {
            	pstmt.setString(19, this.getDutyCode());
            }
            if(this.getPayPlanCode() == null || this.getPayPlanCode().equals("null")) {
            	pstmt.setNull(20, 12);
            } else {
            	pstmt.setString(20, this.getPayPlanCode());
            }
            pstmt.setDouble(21, this.getSumDuePayMoney());
            pstmt.setDouble(22, this.getSumActuPayMoney());
            pstmt.setInt(23, this.getPayIntv());
            if(this.getPayDate() == null || this.getPayDate().equals("null")) {
            	pstmt.setNull(24, 93);
            } else {
            	pstmt.setDate(24, Date.valueOf(this.getPayDate()));
            }
            if(this.getPayType() == null || this.getPayType().equals("null")) {
            	pstmt.setNull(25, 12);
            } else {
            	pstmt.setString(25, this.getPayType());
            }
            if(this.getLastPayToDate() == null || this.getLastPayToDate().equals("null")) {
            	pstmt.setNull(26, 93);
            } else {
            	pstmt.setDate(26, Date.valueOf(this.getLastPayToDate()));
            }
            if(this.getCurPayToDate() == null || this.getCurPayToDate().equals("null")) {
            	pstmt.setNull(27, 93);
            } else {
            	pstmt.setDate(27, Date.valueOf(this.getCurPayToDate()));
            }
            if(this.getInInsuAccState() == null || this.getInInsuAccState().equals("null")) {
            	pstmt.setNull(28, 12);
            } else {
            	pstmt.setString(28, this.getInInsuAccState());
            }
            if(this.getBankCode() == null || this.getBankCode().equals("null")) {
            	pstmt.setNull(29, 12);
            } else {
            	pstmt.setString(29, this.getBankCode());
            }
            if(this.getBankAccNo() == null || this.getBankAccNo().equals("null")) {
            	pstmt.setNull(30, 12);
            } else {
            	pstmt.setString(30, this.getBankAccNo());
            }
            if(this.getBankOnTheWayFlag() == null || this.getBankOnTheWayFlag().equals("null")) {
            	pstmt.setNull(31, 12);
            } else {
            	pstmt.setString(31, this.getBankOnTheWayFlag());
            }
            if(this.getBankSuccFlag() == null || this.getBankSuccFlag().equals("null")) {
            	pstmt.setNull(32, 12);
            } else {
            	pstmt.setString(32, this.getBankSuccFlag());
            }
            if(this.getApproveCode() == null || this.getApproveCode().equals("null")) {
            	pstmt.setNull(33, 12);
            } else {
            	pstmt.setString(33, this.getApproveCode());
            }
            if(this.getApproveDate() == null || this.getApproveDate().equals("null")) {
            	pstmt.setNull(34, 93);
            } else {
            	pstmt.setDate(34, Date.valueOf(this.getApproveDate()));
            }
            if(this.getApproveTime() == null || this.getApproveTime().equals("null")) {
            	pstmt.setNull(35, 12);
            } else {
            	pstmt.setString(35, this.getApproveTime());
            }
            if(this.getSerialNo() == null || this.getSerialNo().equals("null")) {
            	pstmt.setNull(36, 12);
            } else {
            	pstmt.setString(36, this.getSerialNo());
            }
            if(this.getInputFlag() == null || this.getInputFlag().equals("null")) {
            	pstmt.setNull(37, 12);
            } else {
            	pstmt.setString(37, this.getInputFlag());
            }
            if(this.getOperator() == null || this.getOperator().equals("null")) {
            	pstmt.setNull(38, 12);
            } else {
            	pstmt.setString(38, this.getOperator());
            }
            if(this.getMakeDate() == null || this.getMakeDate().equals("null")) {
            	pstmt.setNull(39, 93);
            } else {
            	pstmt.setDate(39, Date.valueOf(this.getMakeDate()));
            }
            if(this.getMakeTime() == null || this.getMakeTime().equals("null")) {
            	pstmt.setNull(40, 12);
            } else {
            	pstmt.setString(40, this.getMakeTime());
            }
            if(this.getModifyDate() == null || this.getModifyDate().equals("null")) {
            	pstmt.setNull(41, 93);
            } else {
            	pstmt.setDate(41, Date.valueOf(this.getModifyDate()));
            }
            if(this.getModifyTime() == null || this.getModifyTime().equals("null")) {
            	pstmt.setNull(42, 12);
            } else {
            	pstmt.setString(42, this.getModifyTime());
            }
            if(this.getEdorNo() == null || this.getEdorNo().equals("null")) {
            	pstmt.setNull(43, 12);
            } else {
            	pstmt.setString(43, this.getEdorNo());
            }
            pstmt.setInt(44, this.getMainPolYear());
            if(this.getOtherNo() == null || this.getOtherNo().equals("null")) {
            	pstmt.setNull(45, 12);
            } else {
            	pstmt.setString(45, this.getOtherNo());
            }
            if(this.getOtherNoType() == null || this.getOtherNoType().equals("null")) {
            	pstmt.setNull(46, 12);
            } else {
            	pstmt.setString(46, this.getOtherNoType());
            }
            if(this.getPrtNo() == null || this.getPrtNo().equals("null")) {
            	pstmt.setNull(47, 12);
            } else {
            	pstmt.setString(47, this.getPrtNo());
            }
            if(this.getSaleChnl() == null || this.getSaleChnl().equals("null")) {
            	pstmt.setNull(48, 12);
            } else {
            	pstmt.setString(48, this.getSaleChnl());
            }
            if(this.getChargeCom() == null || this.getChargeCom().equals("null")) {
            	pstmt.setNull(49, 12);
            } else {
            	pstmt.setString(49, this.getChargeCom());
            }
            if(this.getAPPntName() == null || this.getAPPntName().equals("null")) {
            	pstmt.setNull(50, 12);
            } else {
            	pstmt.setString(50, this.getAPPntName());
            }
            if(this.getKindCode() == null || this.getKindCode().equals("null")) {
            	pstmt.setNull(51, 12);
            } else {
            	pstmt.setString(51, this.getKindCode());
            }
            if(this.getPaytoDate() == null || this.getPaytoDate().equals("null")) {
            	pstmt.setNull(52, 93);
            } else {
            	pstmt.setDate(52, Date.valueOf(this.getPaytoDate()));
            }
            if(this.getPayEndDate() == null || this.getPayEndDate().equals("null")) {
            	pstmt.setNull(53, 93);
            } else {
            	pstmt.setDate(53, Date.valueOf(this.getPayEndDate()));
            }
            if(this.getSignDate() == null || this.getSignDate().equals("null")) {
            	pstmt.setNull(54, 93);
            } else {
            	pstmt.setDate(54, Date.valueOf(this.getSignDate()));
            }
            if(this.getCValiDate() == null || this.getCValiDate().equals("null")) {
            	pstmt.setNull(55, 93);
            } else {
            	pstmt.setDate(55, Date.valueOf(this.getCValiDate()));
            }
            pstmt.setInt(56, this.getPayEndYear());
            pstmt.setDouble(57, this.getPayMoney());
            // execute sql
            pstmt.executeUpdate();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LJSPayPersonDB";
            tError.functionName = "insert()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors.addOneError(tError);
            ex.printStackTrace();

            try {
                pstmt.close();
            } catch (Exception e){}

            if( !mflag ) {
                try {
                    con.close();
                } catch (Exception e){}
            }

            return false;
        }

        if(!mflag) {
            try {
                con.close();
            } catch (Exception e){}
        }

        return true;
    }

    public boolean getInfo() {
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        if(!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            pstmt = con.prepareStatement("SELECT * FROM LJSPayPerson WHERE  1=1  AND SPayPersonID = ?", 
                ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
            pstmt.setLong(1, this.getSPayPersonID());
            rs = pstmt.executeQuery();
            int i = 0;
            while (rs.next()) {
                i++;
                if (!this.setSchema(rs,i)) {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "LJSPayPersonDB";
                    tError.functionName = "getInfo";
                    tError.errorMessage = "取数失败!";
                    this.mErrors .addOneError(tError);

                    try{ rs.close(); } catch( Exception ex ) {}
                    try{ pstmt.close(); } catch( Exception ex1 ) {}

                    if (!mflag) {
                        try {
                            con.close();
                        }
                        catch(Exception et){}
                    }
                    return false;
                }
                break;
            }
            try{ rs.close(); } catch( Exception ex2 ) {}
            try{ pstmt.close(); } catch( Exception ex3 ) {}

            if( i == 0 ) {
                if (!mflag) {
                    try {
                        con.close();
                    }
                    catch(Exception et){}
                }
                return false;
            }
        }
        catch(Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LJSPayPersonDB";
            tError.functionName = "getInfo";
            tError.errorMessage = ExceptionUtils.exceptionToString(e);
            this.mErrors .addOneError(tError);

            try{ rs.close(); } catch( Exception ex ) {}
            try{ pstmt.close(); } catch( Exception ex1 ) {}

            if (!mflag) {
                try {
                    con.close();
                }
                catch(Exception et){}
            }
            return false;
        }
        // 断开数据库连接
        if (!mflag) {
            try {
                con.close();
            }
            catch(Exception e){}
        }

        return true;
    }

    public LJSPayPersonSet query() {
        Statement stmt = null;
        ResultSet rs = null;
        LJSPayPersonSet aLJSPayPersonSet = new LJSPayPersonSet();

        if (!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            stmt = con.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
            SQLString sqlObj = new SQLString("LJSPayPerson");
            LJSPayPersonSchema aSchema = this.getSchema();
            sqlObj.setSQL(5, aSchema);
            String sql = sqlObj.getSQL();

            rs = stmt.executeQuery(sql);
            int i = 0;
            while (rs.next()) {
                i++;
                if (i>10000) {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "LJSPayPersonDB";
                    tError.functionName = "query";
                    tError.errorMessage = "SQL语句返回结果集太多，请换用其他方式!";
                    this.mErrors .addOneError(tError);
                        break;
                }
                LJSPayPersonSchema s1 = new LJSPayPersonSchema();
                s1.setSchema(rs,i);
                aLJSPayPersonSet.add(s1);
            }
            try{ rs.close(); } catch( Exception ex ) {}
            try{ stmt.close(); } catch( Exception ex1 ) {}
        }
        catch(Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LJSPayPersonDB";
            tError.functionName = "query";
            tError.errorMessage = ExceptionUtils.exceptionToString(e);
            this.mErrors.addOneError(tError);
            e.printStackTrace();

            try { rs.close(); } catch (Exception ex2) { ex2.printStackTrace(); }
            try { stmt.close(); } catch(Exception ex3) { ex3.printStackTrace(); }

            if (!mflag) {
                try {
                    con.close();
                }
                catch(Exception et){et.printStackTrace();}
            }
        }

        if (!mflag) {
            try {
                con.close();
            }
            catch(Exception e){e.printStackTrace();}
        }

        return aLJSPayPersonSet;
    }

    public LJSPayPersonSet executeQuery(String sql)  {
        Statement stmt = null;
        ResultSet rs = null;
        LJSPayPersonSet aLJSPayPersonSet = new LJSPayPersonSet();

        if (!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            stmt = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);

            rs = stmt.executeQuery(StrTool.GBKToUnicode(sql));
            int i = 0;
            while (rs.next()) {
                i++;
                if (i>10000) {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "LJSPayPersonDB";
                    tError.functionName = "executeQuery";
                    tError.errorMessage = "SQL语句返回结果集太多，请换用其他方式!";
                    this.mErrors .addOneError(tError);
                        break;
                }
                LJSPayPersonSchema s1 = new LJSPayPersonSchema();
                if (!s1.setSchema(rs,i)) {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "LJSPayPersonDB";
                    tError.functionName = "executeQuery";
                    tError.errorMessage = "sql语句有误，请查看表名及字段名信息!";
                    this.mErrors .addOneError(tError);
                }
                aLJSPayPersonSet.add(s1);
            }
            try{ rs.close(); } catch( Exception ex ) {}
            try{ stmt.close(); } catch( Exception ex1 ) {}
        }
        catch(Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LJSPayPersonDB";
            tError.functionName = "executeQuery";
            tError.errorMessage = ExceptionUtils.exceptionToString(e);
            this.mErrors .addOneError(tError);

            try{ rs.close(); } catch( Exception ex2 ) {}
            try{ stmt.close(); } catch( Exception ex3 ) {}

            if (!mflag) {
                try {
                    con.close();
                }
                catch(Exception et){}
            }
        }

        if (!mflag) {
            try {
                con.close();
            }
            catch(Exception e){}
        }

        return aLJSPayPersonSet;
    }

    public LJSPayPersonSet query(int nStart, int nCount) {
        Statement stmt = null;
        ResultSet rs = null;
        LJSPayPersonSet aLJSPayPersonSet = new LJSPayPersonSet();

        if (!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            stmt = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);
            SQLString sqlObj = new SQLString("LJSPayPerson");
            LJSPayPersonSchema aSchema = this.getSchema();
            sqlObj.setSQL(5,aSchema);
            String sql = sqlObj.getSQL();

            rs = stmt.executeQuery(sql);
            int i = 0;
            while (rs.next()) {
                i++;

                if( i < nStart ) {
                    continue;
                }

                if( i >= nStart + nCount ) {
                    break;
                }

                LJSPayPersonSchema s1 = new LJSPayPersonSchema();
                s1.setSchema(rs,i);
                aLJSPayPersonSet.add(s1);
            }
            try{ rs.close(); } catch( Exception ex ) {}
            try{ stmt.close(); } catch( Exception ex1 ) {}
        }
        catch(Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LJSPayPersonDB";
            tError.functionName = "query";
            tError.errorMessage = ExceptionUtils.exceptionToString(e);
            this.mErrors .addOneError(tError);

            try{ rs.close(); } catch( Exception ex2 ) {}
            try{ stmt.close(); } catch( Exception ex3 ) {}

            if (!mflag) {
                try {
                    con.close();
                }
                catch(Exception et){}
            }
        }

        if (!mflag) {
            try {
                con.close();
            }
            catch(Exception e){}
        }

        return aLJSPayPersonSet;
    }

    public LJSPayPersonSet executeQuery(String sql, int nStart, int nCount) {
        Statement stmt = null;
        ResultSet rs = null;
        LJSPayPersonSet aLJSPayPersonSet = new LJSPayPersonSet();

        if (!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            stmt = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);

            rs = stmt.executeQuery(StrTool.GBKToUnicode(sql));
            int i = 0;
            while (rs.next()) {
                i++;

                if( i < nStart ) {
                    continue;
                }

                if( i >= nStart + nCount ) {
                    break;
                }

                LJSPayPersonSchema s1 = new LJSPayPersonSchema();
                if (!s1.setSchema(rs,i)) {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "LJSPayPersonDB";
                    tError.functionName = "executeQuery";
                    tError.errorMessage = "sql语句有误，请查看表名及字段名信息!";
                    this.mErrors .addOneError(tError);
                }
                aLJSPayPersonSet.add(s1);
            }
            try{ rs.close(); } catch( Exception ex ) {}
            try{ stmt.close(); } catch( Exception ex1 ) {}
        }
        catch(Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LJSPayPersonDB";
            tError.functionName = "executeQuery";
            tError.errorMessage = ExceptionUtils.exceptionToString(e);
            this.mErrors .addOneError(tError);

            try{ rs.close(); } catch( Exception ex2 ) {}
            try{ stmt.close(); } catch( Exception ex3 ) {}

            if (!mflag) {
                try {
                    con.close();
                }
                catch(Exception et){}
            }
        }

        if (!mflag) {
            try {
                con.close();
            }
            catch(Exception e){}
        }

        return aLJSPayPersonSet;
    }

    public boolean update(String strWherePart) {
        Statement stmt = null;

        if (!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            stmt = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);
            SQLString sqlObj = new SQLString("LJSPayPerson");
            LJSPayPersonSchema aSchema = this.getSchema();
            sqlObj.setSQL(2,aSchema);
            String sql = "update LJSPayPerson " + sqlObj.getUpdPart() + " where " + strWherePart;

            int operCount = stmt.executeUpdate(sql);
            if (operCount == 0) {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "LJSPayPersonDB";
                tError.functionName = "update";
                tError.errorMessage = "更新数据失败!";
                this.mErrors .addOneError(tError);

                if (!mflag) {
                    try {
                        con.close();
                    }
                    catch(Exception et){}
                }
                return false;
            }
        }
        catch(Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LJSPayPersonDB";
            tError.functionName = "update";
            tError.errorMessage = ExceptionUtils.exceptionToString(e);
            this.mErrors .addOneError(tError);

            try{ stmt.close(); } catch( Exception ex1 ) {}

            if (!mflag) {
                try {
                    con.close();
                }
                catch(Exception et){}
            }
            return false;
        }
        // 断开数据库连接
        if (!mflag) {
            try {
                con.close();
            }
            catch(Exception e){}
        }

        return true;
    }

    /**
     * 准备数据查询条件
     * @param strSQL String
     * @return boolean
     */
    public boolean prepareData(String strSQL) {
        if (mResultSet != null) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LJSPayPersonDB";
            tError.functionName = "prepareData";
            tError.errorMessage = "数据集非空，程序在准备数据集之后，没有关闭！";
            this.mErrors.addOneError(tError);
            return false;
        }

        if (!mflag) {
            con = DBConnPool.getConnection();
        }
        try {
            mStatement = con.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
            mResultSet = mStatement.executeQuery(StrTool.GBKToUnicode(strSQL));
        }
        catch (Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LJSPayPersonDB";
            tError.functionName = "prepareData";
            tError.errorMessage = ExceptionUtils.exceptionToString(e);
            this.mErrors.addOneError(tError);
            try {
                mResultSet.close();
            }
            catch (Exception ex2) {}
            try {
                mStatement.close();
            }
            catch (Exception ex3) {}
            if (!mflag) {
                try {
                    con.close();
                }
                catch (Exception et) {}
            }
            return false;
        }

        if (!mflag) {
            try {
                con.close();
            }
            catch (Exception e) {}
        }
        return true;
    }

    /**
     * 获取数据集
     * @return boolean
     */
    public boolean hasMoreData() {
        boolean flag = true;
        if (null == mResultSet) {
            CError tError = new CError();
            tError.moduleName = "LJSPayPersonDB";
            tError.functionName = "hasMoreData";
            tError.errorMessage = "数据集为空，请先准备数据集！";
            this.mErrors.addOneError(tError);
            return false;
        }
        try {
            flag = mResultSet.next();
        }
        catch (Exception ex) {
            CError tError = new CError();
            tError.moduleName = "LJSPayPersonDB";
            tError.functionName = "hasMoreData";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors.addOneError(tError);
            try {
                mResultSet.close();
                mResultSet = null;
            }
            catch (Exception ex2) {}
            try {
                mStatement.close();
                mStatement = null;
            }
            catch (Exception ex3) {}
            if (!mflag) {
                try {
                    con.close();
                }
                catch (Exception et) {}
            }
            return false;
        }
        return flag;
    }
    /**
     * 获取定量数据
     * @return LJSPayPersonSet
     */
    public LJSPayPersonSet getData() {
        int tCount = 0;
        LJSPayPersonSet tLJSPayPersonSet = new LJSPayPersonSet();
        LJSPayPersonSchema tLJSPayPersonSchema = null;
        if (null == mResultSet) {
            CError tError = new CError();
            tError.moduleName = "LJSPayPersonDB";
            tError.functionName = "getData";
            tError.errorMessage = "数据集为空，请先准备数据集！";
            this.mErrors.addOneError(tError);
            return null;
        }
        try {
            tCount = 1;
            tLJSPayPersonSchema = new LJSPayPersonSchema();
            tLJSPayPersonSchema.setSchema(mResultSet, 1);
            tLJSPayPersonSet.add(tLJSPayPersonSchema);
            //注意mResultSet.next()的作用
            while (tCount++ < SysConst.FETCHCOUNT) {
                if (mResultSet.next()) {
                    tLJSPayPersonSchema = new LJSPayPersonSchema();
                    tLJSPayPersonSchema.setSchema(mResultSet, 1);
                    tLJSPayPersonSet.add(tLJSPayPersonSchema);
                }
            }
        }
        catch (Exception ex) {
            CError tError = new CError();
            tError.moduleName = "LJSPayPersonDB";
            tError.functionName = "getData";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors.addOneError(tError);
            try {
                mResultSet.close();
                mResultSet = null;
            }
            catch (Exception ex2) {}
            try {
                mStatement.close();
                mStatement = null;
            }
            catch (Exception ex3) {}
            if (!mflag) {
                try {
                    con.close();
                }
                catch (Exception et) {}
            }
            return null;
        }
        return tLJSPayPersonSet;
    }
    /**
     * 关闭数据集
     * @return boolean
     */
    public boolean closeData() {
        boolean flag = true;
        try {
            if (null == mResultSet) {
                CError tError = new CError();
                tError.moduleName = "LJSPayPersonDB";
                tError.functionName = "closeData";
                tError.errorMessage = "数据集已经关闭了！";
                this.mErrors.addOneError(tError);
                flag = false;
            } else {
                mResultSet.close();
                mResultSet = null;
            }
        }
        catch (Exception ex2) {
            CError tError = new CError();
            tError.moduleName = "LJSPayPersonDB";
            tError.functionName = "closeData";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex2);
            this.mErrors.addOneError(tError);
            flag = false;
        }
        try {
            if (null == mStatement) {
                CError tError = new CError();
                tError.moduleName = "LJSPayPersonDB";
                tError.functionName = "closeData";
                tError.errorMessage = "语句已经关闭了！";
                this.mErrors.addOneError(tError);
                flag = false;
            } else {
                mStatement.close();
                mStatement = null;
            }
        }
        catch (Exception ex3) {
            CError tError = new CError();
            tError.moduleName = "LJSPayPersonDB";
            tError.functionName = "closeData";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex3);
            this.mErrors.addOneError(tError);
            flag = false;
        }
        return flag;
    }
}
