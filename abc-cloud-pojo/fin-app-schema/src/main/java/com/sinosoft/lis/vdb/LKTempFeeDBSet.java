/**
 * Copyright (c) 2018 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.vdb;

import com.sinosoft.lis.vschema.LKTempFeeSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.DBConnPool;
import com.sinosoft.utility.DBOper;
import com.sinosoft.utility.ExceptionUtils;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;

/**
 * <p>ClassName: LKTempFeeDBSet </p>
 * <p>Description: DB层多记录数据库操作类文件 </p>
 * <p>Copyright: Copyright (c) 2018</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2018-08-08
 */
public class LKTempFeeDBSet extends LKTempFeeSet {
    // @Field
    private Connection con;
    private DBOper db;
    /**
    * flag = true: 传入Connection
    * flag = false: 不传入Connection
    **/
    private boolean mflag = false;

    // @Constructor
    public LKTempFeeDBSet(Connection tConnection) {
        con = tConnection;
        db = new DBOper(con,"LKTempFee");
        mflag = true;
    }

    public LKTempFeeDBSet() {
        db = new DBOper( "LKTempFee" );
    }
    // @Method
    public boolean deleteSQL() {
        if (db.deleteSQL(this)) {
            return true;
        } else {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LKTempFeeDBSet";
            tError.functionName = "deleteSQL";
            tError.errorMessage = "操作失败!";
            this.mErrors .addOneError(tError);
            return false;
        }
    }

    /**
     * 删除操作
     * 删除条件：主键
     * @return boolean
     */
    public boolean delete() {
        PreparedStatement pstmt = null;

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try {
            int tCount = this.size();
            pstmt = con.prepareStatement("DELETE FROM LKTempFee WHERE  1=1  AND TempFeeID = ?");
            for (int i = 1; i <= tCount; i++) {
            pstmt.setLong(1, this.get(i).getTempFeeID());
                pstmt.addBatch();
            }
            pstmt.executeBatch();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            ex.printStackTrace();
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LKTempFeeDBSet";
            tError.functionName = "delete()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            try {
                pstmt.close();
            } catch (Exception e){
                e.printStackTrace();
            }

            if (!mflag) {
                try {
                    con.close();
                } catch (Exception e){e.printStackTrace();}
            }

            return false;
        }

        if( !mflag ) {
            try {
                con.close();
            } catch (Exception e){e.printStackTrace();}
        }

        return true;
    }

    /**
     * 更新操作
     * 更新条件：主键
     * @return boolean
     */
    public boolean update() {
        PreparedStatement pstmt = null;

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try {
            int tCount = this.size();
            pstmt = con.prepareStatement("UPDATE LKTempFee SET  TempFeeID = ? , ShardingID = ? , TempFeeNo = ? , TempFeeType = ? , RiskCode = ? , PayIntv = ? , OtherNo = ? , OtherNoType = ? , PayMoney = ? , PayDate = ? , EnterAccDate = ? , ConfDate = ? , ConfMakeDate = ? , ConfMakeTime = ? , SaleChnl = ? , ManageCom = ? , PolicyCom = ? , AgentCom = ? , AgentType = ? , APPntName = ? , AgentGroup = ? , AgentCode = ? , ConfFlag = ? , SerialNo = ? , Operator = ? , State = ? , MakeTime = ? , MakeDate = ? , ModifyDate = ? , ModifyTime = ? , ContCom = ? , PayEndYear = ? , TempFeeNoType = ? , StandPrem = ? , Remark = ? , Distict = ? , Department = ? , BranchCode = ? , ContNo = ? WHERE  1=1  AND TempFeeID = ?");
            for (int i = 1; i <= tCount; i++) {
            pstmt.setLong(1, this.get(i).getTempFeeID());
            if(this.get(i).getShardingID() == null || this.get(i).getShardingID().equals("null")) {
            	pstmt.setString(2,null);
            } else {
            	pstmt.setString(2, this.get(i).getShardingID());
            }
            if(this.get(i).getTempFeeNo() == null || this.get(i).getTempFeeNo().equals("null")) {
            	pstmt.setString(3,null);
            } else {
            	pstmt.setString(3, this.get(i).getTempFeeNo());
            }
            if(this.get(i).getTempFeeType() == null || this.get(i).getTempFeeType().equals("null")) {
            	pstmt.setString(4,null);
            } else {
            	pstmt.setString(4, this.get(i).getTempFeeType());
            }
            if(this.get(i).getRiskCode() == null || this.get(i).getRiskCode().equals("null")) {
            	pstmt.setString(5,null);
            } else {
            	pstmt.setString(5, this.get(i).getRiskCode());
            }
            pstmt.setInt(6, this.get(i).getPayIntv());
            if(this.get(i).getOtherNo() == null || this.get(i).getOtherNo().equals("null")) {
            	pstmt.setString(7,null);
            } else {
            	pstmt.setString(7, this.get(i).getOtherNo());
            }
            if(this.get(i).getOtherNoType() == null || this.get(i).getOtherNoType().equals("null")) {
            	pstmt.setString(8,null);
            } else {
            	pstmt.setString(8, this.get(i).getOtherNoType());
            }
            pstmt.setDouble(9, this.get(i).getPayMoney());
            if(this.get(i).getPayDate() == null || this.get(i).getPayDate().equals("null")) {
                pstmt.setDate(10,null);
            } else {
                pstmt.setDate(10, Date.valueOf(this.get(i).getPayDate()));
            }
            if(this.get(i).getEnterAccDate() == null || this.get(i).getEnterAccDate().equals("null")) {
                pstmt.setDate(11,null);
            } else {
                pstmt.setDate(11, Date.valueOf(this.get(i).getEnterAccDate()));
            }
            if(this.get(i).getConfDate() == null || this.get(i).getConfDate().equals("null")) {
                pstmt.setDate(12,null);
            } else {
                pstmt.setDate(12, Date.valueOf(this.get(i).getConfDate()));
            }
            if(this.get(i).getConfMakeDate() == null || this.get(i).getConfMakeDate().equals("null")) {
                pstmt.setDate(13,null);
            } else {
                pstmt.setDate(13, Date.valueOf(this.get(i).getConfMakeDate()));
            }
            if(this.get(i).getConfMakeTime() == null || this.get(i).getConfMakeTime().equals("null")) {
            	pstmt.setString(14,null);
            } else {
            	pstmt.setString(14, this.get(i).getConfMakeTime());
            }
            if(this.get(i).getSaleChnl() == null || this.get(i).getSaleChnl().equals("null")) {
            	pstmt.setString(15,null);
            } else {
            	pstmt.setString(15, this.get(i).getSaleChnl());
            }
            if(this.get(i).getManageCom() == null || this.get(i).getManageCom().equals("null")) {
            	pstmt.setString(16,null);
            } else {
            	pstmt.setString(16, this.get(i).getManageCom());
            }
            if(this.get(i).getPolicyCom() == null || this.get(i).getPolicyCom().equals("null")) {
            	pstmt.setString(17,null);
            } else {
            	pstmt.setString(17, this.get(i).getPolicyCom());
            }
            if(this.get(i).getAgentCom() == null || this.get(i).getAgentCom().equals("null")) {
            	pstmt.setString(18,null);
            } else {
            	pstmt.setString(18, this.get(i).getAgentCom());
            }
            if(this.get(i).getAgentType() == null || this.get(i).getAgentType().equals("null")) {
            	pstmt.setString(19,null);
            } else {
            	pstmt.setString(19, this.get(i).getAgentType());
            }
            if(this.get(i).getAPPntName() == null || this.get(i).getAPPntName().equals("null")) {
            	pstmt.setString(20,null);
            } else {
            	pstmt.setString(20, this.get(i).getAPPntName());
            }
            if(this.get(i).getAgentGroup() == null || this.get(i).getAgentGroup().equals("null")) {
            	pstmt.setString(21,null);
            } else {
            	pstmt.setString(21, this.get(i).getAgentGroup());
            }
            if(this.get(i).getAgentCode() == null || this.get(i).getAgentCode().equals("null")) {
            	pstmt.setString(22,null);
            } else {
            	pstmt.setString(22, this.get(i).getAgentCode());
            }
            if(this.get(i).getConfFlag() == null || this.get(i).getConfFlag().equals("null")) {
            	pstmt.setString(23,null);
            } else {
            	pstmt.setString(23, this.get(i).getConfFlag());
            }
            if(this.get(i).getSerialNo() == null || this.get(i).getSerialNo().equals("null")) {
            	pstmt.setString(24,null);
            } else {
            	pstmt.setString(24, this.get(i).getSerialNo());
            }
            if(this.get(i).getOperator() == null || this.get(i).getOperator().equals("null")) {
            	pstmt.setString(25,null);
            } else {
            	pstmt.setString(25, this.get(i).getOperator());
            }
            if(this.get(i).getState() == null || this.get(i).getState().equals("null")) {
            	pstmt.setString(26,null);
            } else {
            	pstmt.setString(26, this.get(i).getState());
            }
            if(this.get(i).getMakeTime() == null || this.get(i).getMakeTime().equals("null")) {
            	pstmt.setString(27,null);
            } else {
            	pstmt.setString(27, this.get(i).getMakeTime());
            }
            if(this.get(i).getMakeDate() == null || this.get(i).getMakeDate().equals("null")) {
                pstmt.setDate(28,null);
            } else {
                pstmt.setDate(28, Date.valueOf(this.get(i).getMakeDate()));
            }
            if(this.get(i).getModifyDate() == null || this.get(i).getModifyDate().equals("null")) {
                pstmt.setDate(29,null);
            } else {
                pstmt.setDate(29, Date.valueOf(this.get(i).getModifyDate()));
            }
            if(this.get(i).getModifyTime() == null || this.get(i).getModifyTime().equals("null")) {
            	pstmt.setString(30,null);
            } else {
            	pstmt.setString(30, this.get(i).getModifyTime());
            }
            if(this.get(i).getContCom() == null || this.get(i).getContCom().equals("null")) {
            	pstmt.setString(31,null);
            } else {
            	pstmt.setString(31, this.get(i).getContCom());
            }
            pstmt.setInt(32, this.get(i).getPayEndYear());
            if(this.get(i).getTempFeeNoType() == null || this.get(i).getTempFeeNoType().equals("null")) {
            	pstmt.setString(33,null);
            } else {
            	pstmt.setString(33, this.get(i).getTempFeeNoType());
            }
            pstmt.setDouble(34, this.get(i).getStandPrem());
            if(this.get(i).getRemark() == null || this.get(i).getRemark().equals("null")) {
            	pstmt.setString(35,null);
            } else {
            	pstmt.setString(35, this.get(i).getRemark());
            }
            if(this.get(i).getDistict() == null || this.get(i).getDistict().equals("null")) {
            	pstmt.setString(36,null);
            } else {
            	pstmt.setString(36, this.get(i).getDistict());
            }
            if(this.get(i).getDepartment() == null || this.get(i).getDepartment().equals("null")) {
            	pstmt.setString(37,null);
            } else {
            	pstmt.setString(37, this.get(i).getDepartment());
            }
            if(this.get(i).getBranchCode() == null || this.get(i).getBranchCode().equals("null")) {
            	pstmt.setString(38,null);
            } else {
            	pstmt.setString(38, this.get(i).getBranchCode());
            }
            if(this.get(i).getContNo() == null || this.get(i).getContNo().equals("null")) {
            	pstmt.setString(39,null);
            } else {
            	pstmt.setString(39, this.get(i).getContNo());
            }
            // set where condition
            pstmt.setLong(40, this.get(i).getTempFeeID());
                pstmt.addBatch();
            }
            pstmt.executeBatch();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            ex.printStackTrace();
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LKTempFeeDBSet";
            tError.functionName = "update()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            try {
                pstmt.close();
            } catch (Exception e){e.printStackTrace();}

            if( !mflag ) {
                try {
                    con.close();
                } catch (Exception e){e.printStackTrace();}
            }

            return false;
        }

        if( !mflag ) {
            try {
                con.close();
            } catch (Exception e){e.printStackTrace();}
        }

        return true;
    }

    /**
     * 新增操作
     * @return boolean
     */
    public boolean insert() {
        PreparedStatement pstmt = null;

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try
        {
            int tCount = this.size();
            pstmt = con.prepareStatement("INSERT INTO LKTempFee VALUES( ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ?)");
            for (int i = 1; i <= tCount; i++) {
            pstmt.setLong(1, this.get(i).getTempFeeID());
            if(this.get(i).getShardingID() == null || this.get(i).getShardingID().equals("null")) {
            	pstmt.setString(2,null);
            } else {
            	pstmt.setString(2, this.get(i).getShardingID());
            }
            if(this.get(i).getTempFeeNo() == null || this.get(i).getTempFeeNo().equals("null")) {
            	pstmt.setString(3,null);
            } else {
            	pstmt.setString(3, this.get(i).getTempFeeNo());
            }
            if(this.get(i).getTempFeeType() == null || this.get(i).getTempFeeType().equals("null")) {
            	pstmt.setString(4,null);
            } else {
            	pstmt.setString(4, this.get(i).getTempFeeType());
            }
            if(this.get(i).getRiskCode() == null || this.get(i).getRiskCode().equals("null")) {
            	pstmt.setString(5,null);
            } else {
            	pstmt.setString(5, this.get(i).getRiskCode());
            }
            pstmt.setInt(6, this.get(i).getPayIntv());
            if(this.get(i).getOtherNo() == null || this.get(i).getOtherNo().equals("null")) {
            	pstmt.setString(7,null);
            } else {
            	pstmt.setString(7, this.get(i).getOtherNo());
            }
            if(this.get(i).getOtherNoType() == null || this.get(i).getOtherNoType().equals("null")) {
            	pstmt.setString(8,null);
            } else {
            	pstmt.setString(8, this.get(i).getOtherNoType());
            }
            pstmt.setDouble(9, this.get(i).getPayMoney());
            if(this.get(i).getPayDate() == null || this.get(i).getPayDate().equals("null")) {
                pstmt.setDate(10,null);
            } else {
                pstmt.setDate(10, Date.valueOf(this.get(i).getPayDate()));
            }
            if(this.get(i).getEnterAccDate() == null || this.get(i).getEnterAccDate().equals("null")) {
                pstmt.setDate(11,null);
            } else {
                pstmt.setDate(11, Date.valueOf(this.get(i).getEnterAccDate()));
            }
            if(this.get(i).getConfDate() == null || this.get(i).getConfDate().equals("null")) {
                pstmt.setDate(12,null);
            } else {
                pstmt.setDate(12, Date.valueOf(this.get(i).getConfDate()));
            }
            if(this.get(i).getConfMakeDate() == null || this.get(i).getConfMakeDate().equals("null")) {
                pstmt.setDate(13,null);
            } else {
                pstmt.setDate(13, Date.valueOf(this.get(i).getConfMakeDate()));
            }
            if(this.get(i).getConfMakeTime() == null || this.get(i).getConfMakeTime().equals("null")) {
            	pstmt.setString(14,null);
            } else {
            	pstmt.setString(14, this.get(i).getConfMakeTime());
            }
            if(this.get(i).getSaleChnl() == null || this.get(i).getSaleChnl().equals("null")) {
            	pstmt.setString(15,null);
            } else {
            	pstmt.setString(15, this.get(i).getSaleChnl());
            }
            if(this.get(i).getManageCom() == null || this.get(i).getManageCom().equals("null")) {
            	pstmt.setString(16,null);
            } else {
            	pstmt.setString(16, this.get(i).getManageCom());
            }
            if(this.get(i).getPolicyCom() == null || this.get(i).getPolicyCom().equals("null")) {
            	pstmt.setString(17,null);
            } else {
            	pstmt.setString(17, this.get(i).getPolicyCom());
            }
            if(this.get(i).getAgentCom() == null || this.get(i).getAgentCom().equals("null")) {
            	pstmt.setString(18,null);
            } else {
            	pstmt.setString(18, this.get(i).getAgentCom());
            }
            if(this.get(i).getAgentType() == null || this.get(i).getAgentType().equals("null")) {
            	pstmt.setString(19,null);
            } else {
            	pstmt.setString(19, this.get(i).getAgentType());
            }
            if(this.get(i).getAPPntName() == null || this.get(i).getAPPntName().equals("null")) {
            	pstmt.setString(20,null);
            } else {
            	pstmt.setString(20, this.get(i).getAPPntName());
            }
            if(this.get(i).getAgentGroup() == null || this.get(i).getAgentGroup().equals("null")) {
            	pstmt.setString(21,null);
            } else {
            	pstmt.setString(21, this.get(i).getAgentGroup());
            }
            if(this.get(i).getAgentCode() == null || this.get(i).getAgentCode().equals("null")) {
            	pstmt.setString(22,null);
            } else {
            	pstmt.setString(22, this.get(i).getAgentCode());
            }
            if(this.get(i).getConfFlag() == null || this.get(i).getConfFlag().equals("null")) {
            	pstmt.setString(23,null);
            } else {
            	pstmt.setString(23, this.get(i).getConfFlag());
            }
            if(this.get(i).getSerialNo() == null || this.get(i).getSerialNo().equals("null")) {
            	pstmt.setString(24,null);
            } else {
            	pstmt.setString(24, this.get(i).getSerialNo());
            }
            if(this.get(i).getOperator() == null || this.get(i).getOperator().equals("null")) {
            	pstmt.setString(25,null);
            } else {
            	pstmt.setString(25, this.get(i).getOperator());
            }
            if(this.get(i).getState() == null || this.get(i).getState().equals("null")) {
            	pstmt.setString(26,null);
            } else {
            	pstmt.setString(26, this.get(i).getState());
            }
            if(this.get(i).getMakeTime() == null || this.get(i).getMakeTime().equals("null")) {
            	pstmt.setString(27,null);
            } else {
            	pstmt.setString(27, this.get(i).getMakeTime());
            }
            if(this.get(i).getMakeDate() == null || this.get(i).getMakeDate().equals("null")) {
                pstmt.setDate(28,null);
            } else {
                pstmt.setDate(28, Date.valueOf(this.get(i).getMakeDate()));
            }
            if(this.get(i).getModifyDate() == null || this.get(i).getModifyDate().equals("null")) {
                pstmt.setDate(29,null);
            } else {
                pstmt.setDate(29, Date.valueOf(this.get(i).getModifyDate()));
            }
            if(this.get(i).getModifyTime() == null || this.get(i).getModifyTime().equals("null")) {
            	pstmt.setString(30,null);
            } else {
            	pstmt.setString(30, this.get(i).getModifyTime());
            }
            if(this.get(i).getContCom() == null || this.get(i).getContCom().equals("null")) {
            	pstmt.setString(31,null);
            } else {
            	pstmt.setString(31, this.get(i).getContCom());
            }
            pstmt.setInt(32, this.get(i).getPayEndYear());
            if(this.get(i).getTempFeeNoType() == null || this.get(i).getTempFeeNoType().equals("null")) {
            	pstmt.setString(33,null);
            } else {
            	pstmt.setString(33, this.get(i).getTempFeeNoType());
            }
            pstmt.setDouble(34, this.get(i).getStandPrem());
            if(this.get(i).getRemark() == null || this.get(i).getRemark().equals("null")) {
            	pstmt.setString(35,null);
            } else {
            	pstmt.setString(35, this.get(i).getRemark());
            }
            if(this.get(i).getDistict() == null || this.get(i).getDistict().equals("null")) {
            	pstmt.setString(36,null);
            } else {
            	pstmt.setString(36, this.get(i).getDistict());
            }
            if(this.get(i).getDepartment() == null || this.get(i).getDepartment().equals("null")) {
            	pstmt.setString(37,null);
            } else {
            	pstmt.setString(37, this.get(i).getDepartment());
            }
            if(this.get(i).getBranchCode() == null || this.get(i).getBranchCode().equals("null")) {
            	pstmt.setString(38,null);
            } else {
            	pstmt.setString(38, this.get(i).getBranchCode());
            }
            if(this.get(i).getContNo() == null || this.get(i).getContNo().equals("null")) {
            	pstmt.setString(39,null);
            } else {
            	pstmt.setString(39, this.get(i).getContNo());
            }
                pstmt.addBatch();
            }
            pstmt.executeBatch();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            ex.printStackTrace();
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LKTempFeeDBSet";
            tError.functionName = "insert()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            try {
                pstmt.close();
            } catch (Exception e){e.printStackTrace();}

            if( !mflag ) {
                try {
                    con.close();
                } catch (Exception e){e.printStackTrace();}
            }

            return false;
        }

        if( !mflag ) {
            try {
                con.close();
            } catch (Exception e){e.printStackTrace();}
        }

        return true;
    }
}
