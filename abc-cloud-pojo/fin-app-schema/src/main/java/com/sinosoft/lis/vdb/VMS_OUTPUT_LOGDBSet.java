/**
 * Copyright (c) 2017 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.vdb;

import com.sinosoft.lis.vschema.VMS_OUTPUT_LOGSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.DBConnPool;
import com.sinosoft.utility.DBOper;
import com.sinosoft.utility.ExceptionUtils;

import java.sql.Connection;
import java.sql.PreparedStatement;

/**
 * <p>ClassName: VMS_OUTPUT_LOGDBSet </p>
 * <p>Description: DB层多记录数据库操作类文件 </p>
 * <p>Copyright: Copyright (c) 2017</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2017-10-21
 */
public class VMS_OUTPUT_LOGDBSet extends VMS_OUTPUT_LOGSet {
    // @Field
    private Connection con;
    private DBOper db;
    /**
    * flag = true: 传入Connection
    * flag = false: 不传入Connection
    **/
    private boolean mflag = false;

    // @Constructor
    public VMS_OUTPUT_LOGDBSet(Connection tConnection) {
        con = tConnection;
        db = new DBOper(con,"VMS_OUTPUT_LOG");
        mflag = true;
    }

    public VMS_OUTPUT_LOGDBSet() {
        db = new DBOper( "VMS_OUTPUT_LOG" );
    }
    // @Method
    public boolean deleteSQL() {
        if (db.deleteSQL(this)) {
            return true;
        } else {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "VMS_OUTPUT_LOGDBSet";
            tError.functionName = "deleteSQL";
            tError.errorMessage = "操作失败!";
            this.mErrors .addOneError(tError);
            return false;
        }
    }

    /**
     * 删除操作
     * 删除条件：主键
     * @return boolean
     */
    public boolean delete() {
        PreparedStatement pstmt = null;

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try {
            int tCount = this.size();
            pstmt = con.prepareStatement("DELETE FROM VMS_OUTPUT_LOG WHERE  1=1  AND SERVICETYPE = ? AND BATCHNO = ?");
            for (int i = 1; i <= tCount; i++) {
            if(this.get(i).getSERVICETYPE() == null || this.get(i).getSERVICETYPE().equals("null")) {
            	pstmt.setString(1,null);
            } else {
            	pstmt.setString(1, this.get(i).getSERVICETYPE());
            }
            if(this.get(i).getBATCHNO() == null || this.get(i).getBATCHNO().equals("null")) {
            	pstmt.setString(2,null);
            } else {
            	pstmt.setString(2, this.get(i).getBATCHNO());
            }
                pstmt.addBatch();
            }
            pstmt.executeBatch();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            ex.printStackTrace();
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "VMS_OUTPUT_LOGDBSet";
            tError.functionName = "delete()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            try {
                pstmt.close();
            } catch (Exception e){
                e.printStackTrace();
            }

            if (!mflag) {
                try {
                    con.close();
                } catch (Exception e){e.printStackTrace();}
            }

            return false;
        }

        if( !mflag ) {
            try {
                con.close();
            } catch (Exception e){e.printStackTrace();}
        }

        return true;
    }

    /**
     * 更新操作
     * 更新条件：主键
     * @return boolean
     */
    public boolean update() {
        PreparedStatement pstmt = null;

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try {
            int tCount = this.size();
            pstmt = con.prepareStatement("UPDATE VMS_OUTPUT_LOG SET  SERIALNO = ? , SERVICETYPE = ? , BATCHNO = ? , BUSSNO = ? , MAKEDATE = ? , MAKETIME = ? WHERE  1=1  AND SERVICETYPE = ? AND BATCHNO = ?");
            for (int i = 1; i <= tCount; i++) {
            if(this.get(i).getSERIALNO() == null || this.get(i).getSERIALNO().equals("null")) {
            	pstmt.setString(1,null);
            } else {
            	pstmt.setString(1, this.get(i).getSERIALNO());
            }
            if(this.get(i).getSERVICETYPE() == null || this.get(i).getSERVICETYPE().equals("null")) {
            	pstmt.setString(2,null);
            } else {
            	pstmt.setString(2, this.get(i).getSERVICETYPE());
            }
            if(this.get(i).getBATCHNO() == null || this.get(i).getBATCHNO().equals("null")) {
            	pstmt.setString(3,null);
            } else {
            	pstmt.setString(3, this.get(i).getBATCHNO());
            }
            if(this.get(i).getBUSSNO() == null || this.get(i).getBUSSNO().equals("null")) {
            	pstmt.setString(4,null);
            } else {
            	pstmt.setString(4, this.get(i).getBUSSNO());
            }
            if(this.get(i).getMAKEDATE() == null || this.get(i).getMAKEDATE().equals("null")) {
            	pstmt.setString(5,null);
            } else {
            	pstmt.setString(5, this.get(i).getMAKEDATE());
            }
            if(this.get(i).getMAKETIME() == null || this.get(i).getMAKETIME().equals("null")) {
            	pstmt.setString(6,null);
            } else {
            	pstmt.setString(6, this.get(i).getMAKETIME());
            }
            // set where condition
            if(this.get(i).getSERVICETYPE() == null || this.get(i).getSERVICETYPE().equals("null")) {
            	pstmt.setString(7,null);
            } else {
            	pstmt.setString(7, this.get(i).getSERVICETYPE());
            }
            if(this.get(i).getBATCHNO() == null || this.get(i).getBATCHNO().equals("null")) {
            	pstmt.setString(8,null);
            } else {
            	pstmt.setString(8, this.get(i).getBATCHNO());
            }
                pstmt.addBatch();
            }
            pstmt.executeBatch();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            ex.printStackTrace();
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "VMS_OUTPUT_LOGDBSet";
            tError.functionName = "update()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            try {
                pstmt.close();
            } catch (Exception e){e.printStackTrace();}

            if( !mflag ) {
                try {
                    con.close();
                } catch (Exception e){e.printStackTrace();}
            }

            return false;
        }

        if( !mflag ) {
            try {
                con.close();
            } catch (Exception e){e.printStackTrace();}
        }

        return true;
    }

    /**
     * 新增操作
     * @return boolean
     */
    public boolean insert() {
        PreparedStatement pstmt = null;

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try
        {
            int tCount = this.size();
            pstmt = con.prepareStatement("INSERT INTO VMS_OUTPUT_LOG VALUES( ? , ? , ? , ? , ? , ?)");
            for (int i = 1; i <= tCount; i++) {
            if(this.get(i).getSERIALNO() == null || this.get(i).getSERIALNO().equals("null")) {
            	pstmt.setString(1,null);
            } else {
            	pstmt.setString(1, this.get(i).getSERIALNO());
            }
            if(this.get(i).getSERVICETYPE() == null || this.get(i).getSERVICETYPE().equals("null")) {
            	pstmt.setString(2,null);
            } else {
            	pstmt.setString(2, this.get(i).getSERVICETYPE());
            }
            if(this.get(i).getBATCHNO() == null || this.get(i).getBATCHNO().equals("null")) {
            	pstmt.setString(3,null);
            } else {
            	pstmt.setString(3, this.get(i).getBATCHNO());
            }
            if(this.get(i).getBUSSNO() == null || this.get(i).getBUSSNO().equals("null")) {
            	pstmt.setString(4,null);
            } else {
            	pstmt.setString(4, this.get(i).getBUSSNO());
            }
            if(this.get(i).getMAKEDATE() == null || this.get(i).getMAKEDATE().equals("null")) {
            	pstmt.setString(5,null);
            } else {
            	pstmt.setString(5, this.get(i).getMAKEDATE());
            }
            if(this.get(i).getMAKETIME() == null || this.get(i).getMAKETIME().equals("null")) {
            	pstmt.setString(6,null);
            } else {
            	pstmt.setString(6, this.get(i).getMAKETIME());
            }
                pstmt.addBatch();
            }
            pstmt.executeBatch();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            ex.printStackTrace();
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "VMS_OUTPUT_LOGDBSet";
            tError.functionName = "insert()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            try {
                pstmt.close();
            } catch (Exception e){e.printStackTrace();}

            if( !mflag ) {
                try {
                    con.close();
                } catch (Exception e){e.printStackTrace();}
            }

            return false;
        }

        if( !mflag ) {
            try {
                con.close();
            } catch (Exception e){e.printStackTrace();}
        }

        return true;
    }
}
