/**
 * Copyright (c) 2017 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import com.sinosoft.lis.db.LKTransStatusDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.*;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

/**
 * <p>ClassName: LKTransStatusSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2017</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2017-10-21
 */
public class LKTransStatusSchema implements Schema, Cloneable {
    // @Field
    /** Id */
    private long TransStatusID;
    /** Shardingid */
    private String ShardingID;
    /** 业务流水号 */
    private String TransCode;
    /** 报文编号 */
    private String ReportNo;
    /** 银行代码 */
    private String BankCode;
    /** 银行机构代码 */
    private String BankBranch;
    /** 银行网点代码 */
    private String BankNode;
    /** 银行操作员代码 */
    private String BankOperator;
    /** 交易流水号(银行) */
    private String TransNo;
    /** 处理标志 */
    private String FuncFlag;
    /** 交易日期 */
    private Date TransDate;
    /** 交易时间 */
    private String TransTime;
    /** 区站(管理机构) */
    private String ManageCom;
    /** 险种代码 */
    private String RiskCode;
    /** 投保单号 */
    private String ProposalNo;
    /** 印刷号 */
    private String PrtNo;
    /** 保单号 */
    private String PolNo;
    /** 批单号 */
    private String EdorNo;
    /** 收据号 */
    private String TempFeeNo;
    /** 交易金额 */
    private double TransAmnt;
    /** 银行信用卡号 */
    private String BankAcc;
    /** 返还码 */
    private String RCode;
    /** 交易状态 */
    private String TransStatus;
    /** 业务状态 */
    private String Status;
    /** 描述 */
    private String Descr;
    /** 备用字段 */
    private String Temp;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 最后修改日期 */
    private Date ModifyDate;
    /** 最后修改时间 */
    private String ModifyTime;
    /** 状态代码 */
    private String State_Code;
    /** 内部服务流水号 */
    private String RequestId;
    /** 外部的服务代码 */
    private String OutServiceCode;
    /** 客户端ip */
    private String ClientIP;
    /** 客户端port */
    private String ClientPort;
    /** 渠道类型id */
    private String IssueWay;
    /** 服务处理开始时间 */
    private Date ServiceStartTime;
    /** 服务处理结束时间 */
    private Date ServiceEndTime;
    /** 银行与midplat的对账比对结果 */
    private String RBankVSMP;
    /** 银行与midplat的对账比对结果描述 */
    private String DesBankVSMP;
    /** Midplat与核心的对账比对结果 */
    private String RMPVSKernel;
    /** Midplat与核心的对账比对结果描述 */
    private String DesMPVSKernel;
    /** 对账后续处理结果 */
    private String ResultBalance;
    /** 对账后续处理结果描述 */
    private String DesBalance;
    /** 备用字段2 */
    private String bak1;
    /** 备用字段3 */
    private String bak2;
    /** 备用字段4 */
    private String bak3;
    /** 备用字段5 */
    private String bak4;

    public static final int FIELDNUM = 48;    // 数据库表的字段个数

    private static String[] PK;                // 主键

    private FDate fDate = new FDate();        // 处理日期

    public CErrors mErrors;            // 错误信息

    // @Constructor
    public LKTransStatusSchema() {
        mErrors = new CErrors();

        String[] pk = new String[1];
        pk[0] = "TransStatusID";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException {
        LKTransStatusSchema cloned = (LKTransStatusSchema)super.clone();
        cloned.fDate = (FDate) fDate.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK() {
        return PK;
    }

    public long getTransStatusID() {
        return TransStatusID;
    }
    public void setTransStatusID(long aTransStatusID) {
        TransStatusID = aTransStatusID;
    }
    public void setTransStatusID(String aTransStatusID) {
        if (aTransStatusID != null && !aTransStatusID.equals("")) {
            TransStatusID = new Long(aTransStatusID).longValue();
        }
    }

    public String getShardingID() {
        return ShardingID;
    }
    public void setShardingID(String aShardingID) {
        ShardingID = aShardingID;
    }
    public String getTransCode() {
        return TransCode;
    }
    public void setTransCode(String aTransCode) {
        TransCode = aTransCode;
    }
    public String getReportNo() {
        return ReportNo;
    }
    public void setReportNo(String aReportNo) {
        ReportNo = aReportNo;
    }
    public String getBankCode() {
        return BankCode;
    }
    public void setBankCode(String aBankCode) {
        BankCode = aBankCode;
    }
    public String getBankBranch() {
        return BankBranch;
    }
    public void setBankBranch(String aBankBranch) {
        BankBranch = aBankBranch;
    }
    public String getBankNode() {
        return BankNode;
    }
    public void setBankNode(String aBankNode) {
        BankNode = aBankNode;
    }
    public String getBankOperator() {
        return BankOperator;
    }
    public void setBankOperator(String aBankOperator) {
        BankOperator = aBankOperator;
    }
    public String getTransNo() {
        return TransNo;
    }
    public void setTransNo(String aTransNo) {
        TransNo = aTransNo;
    }
    public String getFuncFlag() {
        return FuncFlag;
    }
    public void setFuncFlag(String aFuncFlag) {
        FuncFlag = aFuncFlag;
    }
    public String getTransDate() {
        if(TransDate != null) {
            return fDate.getString(TransDate);
        } else {
            return null;
        }
    }
    public void setTransDate(Date aTransDate) {
        TransDate = aTransDate;
    }
    public void setTransDate(String aTransDate) {
        if (aTransDate != null && !aTransDate.equals("")) {
            TransDate = fDate.getDate(aTransDate);
        } else
            TransDate = null;
    }

    public String getTransTime() {
        return TransTime;
    }
    public void setTransTime(String aTransTime) {
        TransTime = aTransTime;
    }
    public String getManageCom() {
        return ManageCom;
    }
    public void setManageCom(String aManageCom) {
        ManageCom = aManageCom;
    }
    public String getRiskCode() {
        return RiskCode;
    }
    public void setRiskCode(String aRiskCode) {
        RiskCode = aRiskCode;
    }
    public String getProposalNo() {
        return ProposalNo;
    }
    public void setProposalNo(String aProposalNo) {
        ProposalNo = aProposalNo;
    }
    public String getPrtNo() {
        return PrtNo;
    }
    public void setPrtNo(String aPrtNo) {
        PrtNo = aPrtNo;
    }
    public String getPolNo() {
        return PolNo;
    }
    public void setPolNo(String aPolNo) {
        PolNo = aPolNo;
    }
    public String getEdorNo() {
        return EdorNo;
    }
    public void setEdorNo(String aEdorNo) {
        EdorNo = aEdorNo;
    }
    public String getTempFeeNo() {
        return TempFeeNo;
    }
    public void setTempFeeNo(String aTempFeeNo) {
        TempFeeNo = aTempFeeNo;
    }
    public double getTransAmnt() {
        return TransAmnt;
    }
    public void setTransAmnt(double aTransAmnt) {
        TransAmnt = aTransAmnt;
    }
    public void setTransAmnt(String aTransAmnt) {
        if (aTransAmnt != null && !aTransAmnt.equals("")) {
            Double tDouble = new Double(aTransAmnt);
            double d = tDouble.doubleValue();
            TransAmnt = d;
        }
    }

    public String getBankAcc() {
        return BankAcc;
    }
    public void setBankAcc(String aBankAcc) {
        BankAcc = aBankAcc;
    }
    public String getRCode() {
        return RCode;
    }
    public void setRCode(String aRCode) {
        RCode = aRCode;
    }
    public String getTransStatus() {
        return TransStatus;
    }
    public void setTransStatus(String aTransStatus) {
        TransStatus = aTransStatus;
    }
    public String getStatus() {
        return Status;
    }
    public void setStatus(String aStatus) {
        Status = aStatus;
    }
    public String getDescr() {
        return Descr;
    }
    public void setDescr(String aDescr) {
        Descr = aDescr;
    }
    public String getTemp() {
        return Temp;
    }
    public void setTemp(String aTemp) {
        Temp = aTemp;
    }
    public String getMakeDate() {
        if(MakeDate != null) {
            return fDate.getString(MakeDate);
        } else {
            return null;
        }
    }
    public void setMakeDate(Date aMakeDate) {
        MakeDate = aMakeDate;
    }
    public void setMakeDate(String aMakeDate) {
        if (aMakeDate != null && !aMakeDate.equals("")) {
            MakeDate = fDate.getDate(aMakeDate);
        } else
            MakeDate = null;
    }

    public String getMakeTime() {
        return MakeTime;
    }
    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }
    public String getModifyDate() {
        if(ModifyDate != null) {
            return fDate.getString(ModifyDate);
        } else {
            return null;
        }
    }
    public void setModifyDate(Date aModifyDate) {
        ModifyDate = aModifyDate;
    }
    public void setModifyDate(String aModifyDate) {
        if (aModifyDate != null && !aModifyDate.equals("")) {
            ModifyDate = fDate.getDate(aModifyDate);
        } else
            ModifyDate = null;
    }

    public String getModifyTime() {
        return ModifyTime;
    }
    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }
    public String getState_Code() {
        return State_Code;
    }
    public void setState_Code(String aState_Code) {
        State_Code = aState_Code;
    }
    public String getRequestId() {
        return RequestId;
    }
    public void setRequestId(String aRequestId) {
        RequestId = aRequestId;
    }
    public String getOutServiceCode() {
        return OutServiceCode;
    }
    public void setOutServiceCode(String aOutServiceCode) {
        OutServiceCode = aOutServiceCode;
    }
    public String getClientIP() {
        return ClientIP;
    }
    public void setClientIP(String aClientIP) {
        ClientIP = aClientIP;
    }
    public String getClientPort() {
        return ClientPort;
    }
    public void setClientPort(String aClientPort) {
        ClientPort = aClientPort;
    }
    public String getIssueWay() {
        return IssueWay;
    }
    public void setIssueWay(String aIssueWay) {
        IssueWay = aIssueWay;
    }
    public String getServiceStartTime() {
        if(ServiceStartTime != null) {
            return fDate.getString(ServiceStartTime);
        } else {
            return null;
        }
    }
    public void setServiceStartTime(Date aServiceStartTime) {
        ServiceStartTime = aServiceStartTime;
    }
    public void setServiceStartTime(String aServiceStartTime) {
        if (aServiceStartTime != null && !aServiceStartTime.equals("")) {
            ServiceStartTime = fDate.getDate(aServiceStartTime);
        } else
            ServiceStartTime = null;
    }

    public String getServiceEndTime() {
        if(ServiceEndTime != null) {
            return fDate.getString(ServiceEndTime);
        } else {
            return null;
        }
    }
    public void setServiceEndTime(Date aServiceEndTime) {
        ServiceEndTime = aServiceEndTime;
    }
    public void setServiceEndTime(String aServiceEndTime) {
        if (aServiceEndTime != null && !aServiceEndTime.equals("")) {
            ServiceEndTime = fDate.getDate(aServiceEndTime);
        } else
            ServiceEndTime = null;
    }

    public String getRBankVSMP() {
        return RBankVSMP;
    }
    public void setRBankVSMP(String aRBankVSMP) {
        RBankVSMP = aRBankVSMP;
    }
    public String getDesBankVSMP() {
        return DesBankVSMP;
    }
    public void setDesBankVSMP(String aDesBankVSMP) {
        DesBankVSMP = aDesBankVSMP;
    }
    public String getRMPVSKernel() {
        return RMPVSKernel;
    }
    public void setRMPVSKernel(String aRMPVSKernel) {
        RMPVSKernel = aRMPVSKernel;
    }
    public String getDesMPVSKernel() {
        return DesMPVSKernel;
    }
    public void setDesMPVSKernel(String aDesMPVSKernel) {
        DesMPVSKernel = aDesMPVSKernel;
    }
    public String getResultBalance() {
        return ResultBalance;
    }
    public void setResultBalance(String aResultBalance) {
        ResultBalance = aResultBalance;
    }
    public String getDesBalance() {
        return DesBalance;
    }
    public void setDesBalance(String aDesBalance) {
        DesBalance = aDesBalance;
    }
    public String getBak1() {
        return bak1;
    }
    public void setBak1(String abak1) {
        bak1 = abak1;
    }
    public String getBak2() {
        return bak2;
    }
    public void setBak2(String abak2) {
        bak2 = abak2;
    }
    public String getBak3() {
        return bak3;
    }
    public void setBak3(String abak3) {
        bak3 = abak3;
    }
    public String getBak4() {
        return bak4;
    }
    public void setBak4(String abak4) {
        bak4 = abak4;
    }

    /**
    * 使用另外一个 LKTransStatusSchema 对象给 Schema 赋值
    * @param: aLKTransStatusSchema LKTransStatusSchema
    **/
    public void setSchema(LKTransStatusSchema aLKTransStatusSchema) {
        this.TransStatusID = aLKTransStatusSchema.getTransStatusID();
        this.ShardingID = aLKTransStatusSchema.getShardingID();
        this.TransCode = aLKTransStatusSchema.getTransCode();
        this.ReportNo = aLKTransStatusSchema.getReportNo();
        this.BankCode = aLKTransStatusSchema.getBankCode();
        this.BankBranch = aLKTransStatusSchema.getBankBranch();
        this.BankNode = aLKTransStatusSchema.getBankNode();
        this.BankOperator = aLKTransStatusSchema.getBankOperator();
        this.TransNo = aLKTransStatusSchema.getTransNo();
        this.FuncFlag = aLKTransStatusSchema.getFuncFlag();
        this.TransDate = fDate.getDate( aLKTransStatusSchema.getTransDate());
        this.TransTime = aLKTransStatusSchema.getTransTime();
        this.ManageCom = aLKTransStatusSchema.getManageCom();
        this.RiskCode = aLKTransStatusSchema.getRiskCode();
        this.ProposalNo = aLKTransStatusSchema.getProposalNo();
        this.PrtNo = aLKTransStatusSchema.getPrtNo();
        this.PolNo = aLKTransStatusSchema.getPolNo();
        this.EdorNo = aLKTransStatusSchema.getEdorNo();
        this.TempFeeNo = aLKTransStatusSchema.getTempFeeNo();
        this.TransAmnt = aLKTransStatusSchema.getTransAmnt();
        this.BankAcc = aLKTransStatusSchema.getBankAcc();
        this.RCode = aLKTransStatusSchema.getRCode();
        this.TransStatus = aLKTransStatusSchema.getTransStatus();
        this.Status = aLKTransStatusSchema.getStatus();
        this.Descr = aLKTransStatusSchema.getDescr();
        this.Temp = aLKTransStatusSchema.getTemp();
        this.MakeDate = fDate.getDate( aLKTransStatusSchema.getMakeDate());
        this.MakeTime = aLKTransStatusSchema.getMakeTime();
        this.ModifyDate = fDate.getDate( aLKTransStatusSchema.getModifyDate());
        this.ModifyTime = aLKTransStatusSchema.getModifyTime();
        this.State_Code = aLKTransStatusSchema.getState_Code();
        this.RequestId = aLKTransStatusSchema.getRequestId();
        this.OutServiceCode = aLKTransStatusSchema.getOutServiceCode();
        this.ClientIP = aLKTransStatusSchema.getClientIP();
        this.ClientPort = aLKTransStatusSchema.getClientPort();
        this.IssueWay = aLKTransStatusSchema.getIssueWay();
        this.ServiceStartTime = fDate.getDate( aLKTransStatusSchema.getServiceStartTime());
        this.ServiceEndTime = fDate.getDate( aLKTransStatusSchema.getServiceEndTime());
        this.RBankVSMP = aLKTransStatusSchema.getRBankVSMP();
        this.DesBankVSMP = aLKTransStatusSchema.getDesBankVSMP();
        this.RMPVSKernel = aLKTransStatusSchema.getRMPVSKernel();
        this.DesMPVSKernel = aLKTransStatusSchema.getDesMPVSKernel();
        this.ResultBalance = aLKTransStatusSchema.getResultBalance();
        this.DesBalance = aLKTransStatusSchema.getDesBalance();
        this.bak1 = aLKTransStatusSchema.getBak1();
        this.bak2 = aLKTransStatusSchema.getBak2();
        this.bak3 = aLKTransStatusSchema.getBak3();
        this.bak4 = aLKTransStatusSchema.getBak4();
    }

    /**
    * 使用 ResultSet 中的第 i 行给 Schema 赋值
    * @param: rs ResultSet
    * @param: i int
    * @return: boolean
    **/
    public boolean setSchema(ResultSet rs,int i) {
        try {
            //rs.absolute(i);        // 非滚动游标
            this.TransStatusID = rs.getLong("TransStatusID");
            if( rs.getString("ShardingID") == null )
                this.ShardingID = null;
            else
                this.ShardingID = rs.getString("ShardingID").trim();

            if( rs.getString("TransCode") == null )
                this.TransCode = null;
            else
                this.TransCode = rs.getString("TransCode").trim();

            if( rs.getString("ReportNo") == null )
                this.ReportNo = null;
            else
                this.ReportNo = rs.getString("ReportNo").trim();

            if( rs.getString("BankCode") == null )
                this.BankCode = null;
            else
                this.BankCode = rs.getString("BankCode").trim();

            if( rs.getString("BankBranch") == null )
                this.BankBranch = null;
            else
                this.BankBranch = rs.getString("BankBranch").trim();

            if( rs.getString("BankNode") == null )
                this.BankNode = null;
            else
                this.BankNode = rs.getString("BankNode").trim();

            if( rs.getString("BankOperator") == null )
                this.BankOperator = null;
            else
                this.BankOperator = rs.getString("BankOperator").trim();

            if( rs.getString("TransNo") == null )
                this.TransNo = null;
            else
                this.TransNo = rs.getString("TransNo").trim();

            if( rs.getString("FuncFlag") == null )
                this.FuncFlag = null;
            else
                this.FuncFlag = rs.getString("FuncFlag").trim();

            this.TransDate = rs.getDate("TransDate");
            if( rs.getString("TransTime") == null )
                this.TransTime = null;
            else
                this.TransTime = rs.getString("TransTime").trim();

            if( rs.getString("ManageCom") == null )
                this.ManageCom = null;
            else
                this.ManageCom = rs.getString("ManageCom").trim();

            if( rs.getString("RiskCode") == null )
                this.RiskCode = null;
            else
                this.RiskCode = rs.getString("RiskCode").trim();

            if( rs.getString("ProposalNo") == null )
                this.ProposalNo = null;
            else
                this.ProposalNo = rs.getString("ProposalNo").trim();

            if( rs.getString("PrtNo") == null )
                this.PrtNo = null;
            else
                this.PrtNo = rs.getString("PrtNo").trim();

            if( rs.getString("PolNo") == null )
                this.PolNo = null;
            else
                this.PolNo = rs.getString("PolNo").trim();

            if( rs.getString("EdorNo") == null )
                this.EdorNo = null;
            else
                this.EdorNo = rs.getString("EdorNo").trim();

            if( rs.getString("TempFeeNo") == null )
                this.TempFeeNo = null;
            else
                this.TempFeeNo = rs.getString("TempFeeNo").trim();

            this.TransAmnt = rs.getDouble("TransAmnt");
            if( rs.getString("BankAcc") == null )
                this.BankAcc = null;
            else
                this.BankAcc = rs.getString("BankAcc").trim();

            if( rs.getString("RCode") == null )
                this.RCode = null;
            else
                this.RCode = rs.getString("RCode").trim();

            if( rs.getString("TransStatus") == null )
                this.TransStatus = null;
            else
                this.TransStatus = rs.getString("TransStatus").trim();

            if( rs.getString("Status") == null )
                this.Status = null;
            else
                this.Status = rs.getString("Status").trim();

            if( rs.getString("Descr") == null )
                this.Descr = null;
            else
                this.Descr = rs.getString("Descr").trim();

            if( rs.getString("Temp") == null )
                this.Temp = null;
            else
                this.Temp = rs.getString("Temp").trim();

            this.MakeDate = rs.getDate("MakeDate");
            if( rs.getString("MakeTime") == null )
                this.MakeTime = null;
            else
                this.MakeTime = rs.getString("MakeTime").trim();

            this.ModifyDate = rs.getDate("ModifyDate");
            if( rs.getString("ModifyTime") == null )
                this.ModifyTime = null;
            else
                this.ModifyTime = rs.getString("ModifyTime").trim();

            if( rs.getString("State_Code") == null )
                this.State_Code = null;
            else
                this.State_Code = rs.getString("State_Code").trim();

            if( rs.getString("RequestId") == null )
                this.RequestId = null;
            else
                this.RequestId = rs.getString("RequestId").trim();

            if( rs.getString("OutServiceCode") == null )
                this.OutServiceCode = null;
            else
                this.OutServiceCode = rs.getString("OutServiceCode").trim();

            if( rs.getString("ClientIP") == null )
                this.ClientIP = null;
            else
                this.ClientIP = rs.getString("ClientIP").trim();

            if( rs.getString("ClientPort") == null )
                this.ClientPort = null;
            else
                this.ClientPort = rs.getString("ClientPort").trim();

            if( rs.getString("IssueWay") == null )
                this.IssueWay = null;
            else
                this.IssueWay = rs.getString("IssueWay").trim();

            this.ServiceStartTime = rs.getDate("ServiceStartTime");
            this.ServiceEndTime = rs.getDate("ServiceEndTime");
            if( rs.getString("RBankVSMP") == null )
                this.RBankVSMP = null;
            else
                this.RBankVSMP = rs.getString("RBankVSMP").trim();

            if( rs.getString("DesBankVSMP") == null )
                this.DesBankVSMP = null;
            else
                this.DesBankVSMP = rs.getString("DesBankVSMP").trim();

            if( rs.getString("RMPVSKernel") == null )
                this.RMPVSKernel = null;
            else
                this.RMPVSKernel = rs.getString("RMPVSKernel").trim();

            if( rs.getString("DesMPVSKernel") == null )
                this.DesMPVSKernel = null;
            else
                this.DesMPVSKernel = rs.getString("DesMPVSKernel").trim();

            if( rs.getString("ResultBalance") == null )
                this.ResultBalance = null;
            else
                this.ResultBalance = rs.getString("ResultBalance").trim();

            if( rs.getString("DesBalance") == null )
                this.DesBalance = null;
            else
                this.DesBalance = rs.getString("DesBalance").trim();

            if( rs.getString("bak1") == null )
                this.bak1 = null;
            else
                this.bak1 = rs.getString("bak1").trim();

            if( rs.getString("bak2") == null )
                this.bak2 = null;
            else
                this.bak2 = rs.getString("bak2").trim();

            if( rs.getString("bak3") == null )
                this.bak3 = null;
            else
                this.bak3 = rs.getString("bak3").trim();

            if( rs.getString("bak4") == null )
                this.bak4 = null;
            else
                this.bak4 = rs.getString("bak4").trim();

        }
        catch(SQLException sqle) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LKTransStatusSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = ExceptionUtils.exceptionToString(sqle);
            this.mErrors .addOneError(tError);
            return false;
        }
        return true;
    }

    public LKTransStatusSchema getSchema() {
        LKTransStatusSchema aLKTransStatusSchema = new LKTransStatusSchema();
        aLKTransStatusSchema.setSchema(this);
        return aLKTransStatusSchema;
    }

    public LKTransStatusDB getDB() {
        LKTransStatusDB aDBOper = new LKTransStatusDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
    * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLKTransStatus描述/A>表字段
    * @return: String 返回打包后字符串
    **/
    public String encode() {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(ChgData.chgData(TransStatusID));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ShardingID)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(TransCode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ReportNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(BankCode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(BankBranch)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(BankNode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(BankOperator)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(TransNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(FuncFlag)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( TransDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(TransTime)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ManageCom)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(RiskCode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ProposalNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(PrtNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(PolNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(EdorNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(TempFeeNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(TransAmnt));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(BankAcc)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(RCode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(TransStatus)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Status)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Descr)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Temp)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ModifyTime)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(State_Code)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(RequestId)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(OutServiceCode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ClientIP)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ClientPort)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(IssueWay)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( ServiceStartTime ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( ServiceEndTime ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(RBankVSMP)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(DesBankVSMP)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(RMPVSKernel)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(DesMPVSKernel)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ResultBalance)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(DesBalance)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(bak1)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(bak2)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(bak3)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(bak4));
        return strReturn.toString();
    }

    /**
    * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLKTransStatus>历史记账凭证主表信息</A>表字段
    * @param: strMessage String 包含一条纪录数据的字符串
    * @return: boolean
    **/
    public boolean decode(String strMessage) {
        try {
            TransStatusID = new Long(ChgData.chgNumericStr(StrTool.getStr(strMessage,1, SysConst.PACKAGESPILTER))).longValue();
            ShardingID = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
            TransCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
            ReportNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
            BankCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
            BankBranch = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
            BankNode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
            BankOperator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
            TransNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
            FuncFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
            TransDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER));
            TransTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
            ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
            RiskCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER );
            ProposalNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15, SysConst.PACKAGESPILTER );
            PrtNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16, SysConst.PACKAGESPILTER );
            PolNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17, SysConst.PACKAGESPILTER );
            EdorNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18, SysConst.PACKAGESPILTER );
            TempFeeNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19, SysConst.PACKAGESPILTER );
            TransAmnt = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,20, SysConst.PACKAGESPILTER))).doubleValue();
            BankAcc = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 21, SysConst.PACKAGESPILTER );
            RCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 22, SysConst.PACKAGESPILTER );
            TransStatus = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 23, SysConst.PACKAGESPILTER );
            Status = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 24, SysConst.PACKAGESPILTER );
            Descr = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 25, SysConst.PACKAGESPILTER );
            Temp = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 26, SysConst.PACKAGESPILTER );
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 27, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 28, SysConst.PACKAGESPILTER );
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 29, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 30, SysConst.PACKAGESPILTER );
            State_Code = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 31, SysConst.PACKAGESPILTER );
            RequestId = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 32, SysConst.PACKAGESPILTER );
            OutServiceCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 33, SysConst.PACKAGESPILTER );
            ClientIP = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 34, SysConst.PACKAGESPILTER );
            ClientPort = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 35, SysConst.PACKAGESPILTER );
            IssueWay = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 36, SysConst.PACKAGESPILTER );
            ServiceStartTime = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 37, SysConst.PACKAGESPILTER));
            ServiceEndTime = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 38, SysConst.PACKAGESPILTER));
            RBankVSMP = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 39, SysConst.PACKAGESPILTER );
            DesBankVSMP = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 40, SysConst.PACKAGESPILTER );
            RMPVSKernel = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 41, SysConst.PACKAGESPILTER );
            DesMPVSKernel = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 42, SysConst.PACKAGESPILTER );
            ResultBalance = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 43, SysConst.PACKAGESPILTER );
            DesBalance = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 44, SysConst.PACKAGESPILTER );
            bak1 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 45, SysConst.PACKAGESPILTER );
            bak2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 46, SysConst.PACKAGESPILTER );
            bak3 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 47, SysConst.PACKAGESPILTER );
            bak4 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 48, SysConst.PACKAGESPILTER );
        }
        catch(NumberFormatException ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LKTransStatusSchema";
            tError.functionName = "decode";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            return false;
        }
        return true;
    }

    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("TransStatusID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(TransStatusID));
        }
        if (FCode.equalsIgnoreCase("ShardingID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ShardingID));
        }
        if (FCode.equalsIgnoreCase("TransCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(TransCode));
        }
        if (FCode.equalsIgnoreCase("ReportNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ReportNo));
        }
        if (FCode.equalsIgnoreCase("BankCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BankCode));
        }
        if (FCode.equalsIgnoreCase("BankBranch")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BankBranch));
        }
        if (FCode.equalsIgnoreCase("BankNode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BankNode));
        }
        if (FCode.equalsIgnoreCase("BankOperator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BankOperator));
        }
        if (FCode.equalsIgnoreCase("TransNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(TransNo));
        }
        if (FCode.equalsIgnoreCase("FuncFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FuncFlag));
        }
        if (FCode.equalsIgnoreCase("TransDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getTransDate()));
        }
        if (FCode.equalsIgnoreCase("TransTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(TransTime));
        }
        if (FCode.equalsIgnoreCase("ManageCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
        }
        if (FCode.equalsIgnoreCase("RiskCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RiskCode));
        }
        if (FCode.equalsIgnoreCase("ProposalNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ProposalNo));
        }
        if (FCode.equalsIgnoreCase("PrtNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PrtNo));
        }
        if (FCode.equalsIgnoreCase("PolNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PolNo));
        }
        if (FCode.equalsIgnoreCase("EdorNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(EdorNo));
        }
        if (FCode.equalsIgnoreCase("TempFeeNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(TempFeeNo));
        }
        if (FCode.equalsIgnoreCase("TransAmnt")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(TransAmnt));
        }
        if (FCode.equalsIgnoreCase("BankAcc")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BankAcc));
        }
        if (FCode.equalsIgnoreCase("RCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RCode));
        }
        if (FCode.equalsIgnoreCase("TransStatus")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(TransStatus));
        }
        if (FCode.equalsIgnoreCase("Status")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Status));
        }
        if (FCode.equalsIgnoreCase("Descr")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Descr));
        }
        if (FCode.equalsIgnoreCase("Temp")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Temp));
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (FCode.equalsIgnoreCase("State_Code")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(State_Code));
        }
        if (FCode.equalsIgnoreCase("RequestId")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RequestId));
        }
        if (FCode.equalsIgnoreCase("OutServiceCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OutServiceCode));
        }
        if (FCode.equalsIgnoreCase("ClientIP")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ClientIP));
        }
        if (FCode.equalsIgnoreCase("ClientPort")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ClientPort));
        }
        if (FCode.equalsIgnoreCase("IssueWay")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(IssueWay));
        }
        if (FCode.equalsIgnoreCase("ServiceStartTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getServiceStartTime()));
        }
        if (FCode.equalsIgnoreCase("ServiceEndTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getServiceEndTime()));
        }
        if (FCode.equalsIgnoreCase("RBankVSMP")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RBankVSMP));
        }
        if (FCode.equalsIgnoreCase("DesBankVSMP")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DesBankVSMP));
        }
        if (FCode.equalsIgnoreCase("RMPVSKernel")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RMPVSKernel));
        }
        if (FCode.equalsIgnoreCase("DesMPVSKernel")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DesMPVSKernel));
        }
        if (FCode.equalsIgnoreCase("ResultBalance")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ResultBalance));
        }
        if (FCode.equalsIgnoreCase("DesBalance")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DesBalance));
        }
        if (FCode.equalsIgnoreCase("bak1")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(bak1));
        }
        if (FCode.equalsIgnoreCase("bak2")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(bak2));
        }
        if (FCode.equalsIgnoreCase("bak3")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(bak3));
        }
        if (FCode.equalsIgnoreCase("bak4")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(bak4));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = String.valueOf(TransStatusID);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(ShardingID);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(TransCode);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(ReportNo);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(BankCode);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(BankBranch);
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(BankNode);
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(BankOperator);
                break;
            case 8:
                strFieldValue = StrTool.GBKToUnicode(TransNo);
                break;
            case 9:
                strFieldValue = StrTool.GBKToUnicode(FuncFlag);
                break;
            case 10:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getTransDate()));
                break;
            case 11:
                strFieldValue = StrTool.GBKToUnicode(TransTime);
                break;
            case 12:
                strFieldValue = StrTool.GBKToUnicode(ManageCom);
                break;
            case 13:
                strFieldValue = StrTool.GBKToUnicode(RiskCode);
                break;
            case 14:
                strFieldValue = StrTool.GBKToUnicode(ProposalNo);
                break;
            case 15:
                strFieldValue = StrTool.GBKToUnicode(PrtNo);
                break;
            case 16:
                strFieldValue = StrTool.GBKToUnicode(PolNo);
                break;
            case 17:
                strFieldValue = StrTool.GBKToUnicode(EdorNo);
                break;
            case 18:
                strFieldValue = StrTool.GBKToUnicode(TempFeeNo);
                break;
            case 19:
                strFieldValue = String.valueOf(TransAmnt);
                break;
            case 20:
                strFieldValue = StrTool.GBKToUnicode(BankAcc);
                break;
            case 21:
                strFieldValue = StrTool.GBKToUnicode(RCode);
                break;
            case 22:
                strFieldValue = StrTool.GBKToUnicode(TransStatus);
                break;
            case 23:
                strFieldValue = StrTool.GBKToUnicode(Status);
                break;
            case 24:
                strFieldValue = StrTool.GBKToUnicode(Descr);
                break;
            case 25:
                strFieldValue = StrTool.GBKToUnicode(Temp);
                break;
            case 26:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
                break;
            case 27:
                strFieldValue = StrTool.GBKToUnicode(MakeTime);
                break;
            case 28:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
                break;
            case 29:
                strFieldValue = StrTool.GBKToUnicode(ModifyTime);
                break;
            case 30:
                strFieldValue = StrTool.GBKToUnicode(State_Code);
                break;
            case 31:
                strFieldValue = StrTool.GBKToUnicode(RequestId);
                break;
            case 32:
                strFieldValue = StrTool.GBKToUnicode(OutServiceCode);
                break;
            case 33:
                strFieldValue = StrTool.GBKToUnicode(ClientIP);
                break;
            case 34:
                strFieldValue = StrTool.GBKToUnicode(ClientPort);
                break;
            case 35:
                strFieldValue = StrTool.GBKToUnicode(IssueWay);
                break;
            case 36:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getServiceStartTime()));
                break;
            case 37:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getServiceEndTime()));
                break;
            case 38:
                strFieldValue = StrTool.GBKToUnicode(RBankVSMP);
                break;
            case 39:
                strFieldValue = StrTool.GBKToUnicode(DesBankVSMP);
                break;
            case 40:
                strFieldValue = StrTool.GBKToUnicode(RMPVSKernel);
                break;
            case 41:
                strFieldValue = StrTool.GBKToUnicode(DesMPVSKernel);
                break;
            case 42:
                strFieldValue = StrTool.GBKToUnicode(ResultBalance);
                break;
            case 43:
                strFieldValue = StrTool.GBKToUnicode(DesBalance);
                break;
            case 44:
                strFieldValue = StrTool.GBKToUnicode(bak1);
                break;
            case 45:
                strFieldValue = StrTool.GBKToUnicode(bak2);
                break;
            case 46:
                strFieldValue = StrTool.GBKToUnicode(bak3);
                break;
            case 47:
                strFieldValue = StrTool.GBKToUnicode(bak4);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("TransStatusID")) {
            if( FValue != null && !FValue.equals("")) {
                TransStatusID = new Long(FValue).longValue();
            }
        }
        if (FCode.equalsIgnoreCase("ShardingID")) {
            if( FValue != null && !FValue.equals(""))
            {
                ShardingID = FValue.trim();
            }
            else
                ShardingID = null;
        }
        if (FCode.equalsIgnoreCase("TransCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                TransCode = FValue.trim();
            }
            else
                TransCode = null;
        }
        if (FCode.equalsIgnoreCase("ReportNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                ReportNo = FValue.trim();
            }
            else
                ReportNo = null;
        }
        if (FCode.equalsIgnoreCase("BankCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                BankCode = FValue.trim();
            }
            else
                BankCode = null;
        }
        if (FCode.equalsIgnoreCase("BankBranch")) {
            if( FValue != null && !FValue.equals(""))
            {
                BankBranch = FValue.trim();
            }
            else
                BankBranch = null;
        }
        if (FCode.equalsIgnoreCase("BankNode")) {
            if( FValue != null && !FValue.equals(""))
            {
                BankNode = FValue.trim();
            }
            else
                BankNode = null;
        }
        if (FCode.equalsIgnoreCase("BankOperator")) {
            if( FValue != null && !FValue.equals(""))
            {
                BankOperator = FValue.trim();
            }
            else
                BankOperator = null;
        }
        if (FCode.equalsIgnoreCase("TransNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                TransNo = FValue.trim();
            }
            else
                TransNo = null;
        }
        if (FCode.equalsIgnoreCase("FuncFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                FuncFlag = FValue.trim();
            }
            else
                FuncFlag = null;
        }
        if (FCode.equalsIgnoreCase("TransDate")) {
            if(FValue != null && !FValue.equals("")) {
                TransDate = fDate.getDate( FValue );
            }
            else
                TransDate = null;
        }
        if (FCode.equalsIgnoreCase("TransTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                TransTime = FValue.trim();
            }
            else
                TransTime = null;
        }
        if (FCode.equalsIgnoreCase("ManageCom")) {
            if( FValue != null && !FValue.equals(""))
            {
                ManageCom = FValue.trim();
            }
            else
                ManageCom = null;
        }
        if (FCode.equalsIgnoreCase("RiskCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                RiskCode = FValue.trim();
            }
            else
                RiskCode = null;
        }
        if (FCode.equalsIgnoreCase("ProposalNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                ProposalNo = FValue.trim();
            }
            else
                ProposalNo = null;
        }
        if (FCode.equalsIgnoreCase("PrtNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                PrtNo = FValue.trim();
            }
            else
                PrtNo = null;
        }
        if (FCode.equalsIgnoreCase("PolNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                PolNo = FValue.trim();
            }
            else
                PolNo = null;
        }
        if (FCode.equalsIgnoreCase("EdorNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                EdorNo = FValue.trim();
            }
            else
                EdorNo = null;
        }
        if (FCode.equalsIgnoreCase("TempFeeNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                TempFeeNo = FValue.trim();
            }
            else
                TempFeeNo = null;
        }
        if (FCode.equalsIgnoreCase("TransAmnt")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                TransAmnt = d;
            }
        }
        if (FCode.equalsIgnoreCase("BankAcc")) {
            if( FValue != null && !FValue.equals(""))
            {
                BankAcc = FValue.trim();
            }
            else
                BankAcc = null;
        }
        if (FCode.equalsIgnoreCase("RCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                RCode = FValue.trim();
            }
            else
                RCode = null;
        }
        if (FCode.equalsIgnoreCase("TransStatus")) {
            if( FValue != null && !FValue.equals(""))
            {
                TransStatus = FValue.trim();
            }
            else
                TransStatus = null;
        }
        if (FCode.equalsIgnoreCase("Status")) {
            if( FValue != null && !FValue.equals(""))
            {
                Status = FValue.trim();
            }
            else
                Status = null;
        }
        if (FCode.equalsIgnoreCase("Descr")) {
            if( FValue != null && !FValue.equals(""))
            {
                Descr = FValue.trim();
            }
            else
                Descr = null;
        }
        if (FCode.equalsIgnoreCase("Temp")) {
            if( FValue != null && !FValue.equals(""))
            {
                Temp = FValue.trim();
            }
            else
                Temp = null;
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if(FValue != null && !FValue.equals("")) {
                MakeDate = fDate.getDate( FValue );
            }
            else
                MakeDate = null;
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
                MakeTime = null;
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if(FValue != null && !FValue.equals("")) {
                ModifyDate = fDate.getDate( FValue );
            }
            else
                ModifyDate = null;
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
                ModifyTime = null;
        }
        if (FCode.equalsIgnoreCase("State_Code")) {
            if( FValue != null && !FValue.equals(""))
            {
                State_Code = FValue.trim();
            }
            else
                State_Code = null;
        }
        if (FCode.equalsIgnoreCase("RequestId")) {
            if( FValue != null && !FValue.equals(""))
            {
                RequestId = FValue.trim();
            }
            else
                RequestId = null;
        }
        if (FCode.equalsIgnoreCase("OutServiceCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                OutServiceCode = FValue.trim();
            }
            else
                OutServiceCode = null;
        }
        if (FCode.equalsIgnoreCase("ClientIP")) {
            if( FValue != null && !FValue.equals(""))
            {
                ClientIP = FValue.trim();
            }
            else
                ClientIP = null;
        }
        if (FCode.equalsIgnoreCase("ClientPort")) {
            if( FValue != null && !FValue.equals(""))
            {
                ClientPort = FValue.trim();
            }
            else
                ClientPort = null;
        }
        if (FCode.equalsIgnoreCase("IssueWay")) {
            if( FValue != null && !FValue.equals(""))
            {
                IssueWay = FValue.trim();
            }
            else
                IssueWay = null;
        }
        if (FCode.equalsIgnoreCase("ServiceStartTime")) {
            if(FValue != null && !FValue.equals("")) {
                ServiceStartTime = fDate.getDate( FValue );
            }
            else
                ServiceStartTime = null;
        }
        if (FCode.equalsIgnoreCase("ServiceEndTime")) {
            if(FValue != null && !FValue.equals("")) {
                ServiceEndTime = fDate.getDate( FValue );
            }
            else
                ServiceEndTime = null;
        }
        if (FCode.equalsIgnoreCase("RBankVSMP")) {
            if( FValue != null && !FValue.equals(""))
            {
                RBankVSMP = FValue.trim();
            }
            else
                RBankVSMP = null;
        }
        if (FCode.equalsIgnoreCase("DesBankVSMP")) {
            if( FValue != null && !FValue.equals(""))
            {
                DesBankVSMP = FValue.trim();
            }
            else
                DesBankVSMP = null;
        }
        if (FCode.equalsIgnoreCase("RMPVSKernel")) {
            if( FValue != null && !FValue.equals(""))
            {
                RMPVSKernel = FValue.trim();
            }
            else
                RMPVSKernel = null;
        }
        if (FCode.equalsIgnoreCase("DesMPVSKernel")) {
            if( FValue != null && !FValue.equals(""))
            {
                DesMPVSKernel = FValue.trim();
            }
            else
                DesMPVSKernel = null;
        }
        if (FCode.equalsIgnoreCase("ResultBalance")) {
            if( FValue != null && !FValue.equals(""))
            {
                ResultBalance = FValue.trim();
            }
            else
                ResultBalance = null;
        }
        if (FCode.equalsIgnoreCase("DesBalance")) {
            if( FValue != null && !FValue.equals(""))
            {
                DesBalance = FValue.trim();
            }
            else
                DesBalance = null;
        }
        if (FCode.equalsIgnoreCase("bak1")) {
            if( FValue != null && !FValue.equals(""))
            {
                bak1 = FValue.trim();
            }
            else
                bak1 = null;
        }
        if (FCode.equalsIgnoreCase("bak2")) {
            if( FValue != null && !FValue.equals(""))
            {
                bak2 = FValue.trim();
            }
            else
                bak2 = null;
        }
        if (FCode.equalsIgnoreCase("bak3")) {
            if( FValue != null && !FValue.equals(""))
            {
                bak3 = FValue.trim();
            }
            else
                bak3 = null;
        }
        if (FCode.equalsIgnoreCase("bak4")) {
            if( FValue != null && !FValue.equals(""))
            {
                bak4 = FValue.trim();
            }
            else
                bak4 = null;
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject) return true;
        if (otherObject == null) return false;
        if (getClass() != otherObject.getClass()) return false;
        LKTransStatusSchema other = (LKTransStatusSchema)otherObject;
        return
            TransStatusID == other.getTransStatusID()
            && ShardingID.equals(other.getShardingID())
            && TransCode.equals(other.getTransCode())
            && ReportNo.equals(other.getReportNo())
            && BankCode.equals(other.getBankCode())
            && BankBranch.equals(other.getBankBranch())
            && BankNode.equals(other.getBankNode())
            && BankOperator.equals(other.getBankOperator())
            && TransNo.equals(other.getTransNo())
            && FuncFlag.equals(other.getFuncFlag())
            && fDate.getString(TransDate).equals(other.getTransDate())
            && TransTime.equals(other.getTransTime())
            && ManageCom.equals(other.getManageCom())
            && RiskCode.equals(other.getRiskCode())
            && ProposalNo.equals(other.getProposalNo())
            && PrtNo.equals(other.getPrtNo())
            && PolNo.equals(other.getPolNo())
            && EdorNo.equals(other.getEdorNo())
            && TempFeeNo.equals(other.getTempFeeNo())
            && TransAmnt == other.getTransAmnt()
            && BankAcc.equals(other.getBankAcc())
            && RCode.equals(other.getRCode())
            && TransStatus.equals(other.getTransStatus())
            && Status.equals(other.getStatus())
            && Descr.equals(other.getDescr())
            && Temp.equals(other.getTemp())
            && fDate.getString(MakeDate).equals(other.getMakeDate())
            && MakeTime.equals(other.getMakeTime())
            && fDate.getString(ModifyDate).equals(other.getModifyDate())
            && ModifyTime.equals(other.getModifyTime())
            && State_Code.equals(other.getState_Code())
            && RequestId.equals(other.getRequestId())
            && OutServiceCode.equals(other.getOutServiceCode())
            && ClientIP.equals(other.getClientIP())
            && ClientPort.equals(other.getClientPort())
            && IssueWay.equals(other.getIssueWay())
            && fDate.getString(ServiceStartTime).equals(other.getServiceStartTime())
            && fDate.getString(ServiceEndTime).equals(other.getServiceEndTime())
            && RBankVSMP.equals(other.getRBankVSMP())
            && DesBankVSMP.equals(other.getDesBankVSMP())
            && RMPVSKernel.equals(other.getRMPVSKernel())
            && DesMPVSKernel.equals(other.getDesMPVSKernel())
            && ResultBalance.equals(other.getResultBalance())
            && DesBalance.equals(other.getDesBalance())
            && bak1.equals(other.getBak1())
            && bak2.equals(other.getBak2())
            && bak3.equals(other.getBak3())
            && bak4.equals(other.getBak4());
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("TransStatusID") ) {
            return 0;
        }
        if( strFieldName.equals("ShardingID") ) {
            return 1;
        }
        if( strFieldName.equals("TransCode") ) {
            return 2;
        }
        if( strFieldName.equals("ReportNo") ) {
            return 3;
        }
        if( strFieldName.equals("BankCode") ) {
            return 4;
        }
        if( strFieldName.equals("BankBranch") ) {
            return 5;
        }
        if( strFieldName.equals("BankNode") ) {
            return 6;
        }
        if( strFieldName.equals("BankOperator") ) {
            return 7;
        }
        if( strFieldName.equals("TransNo") ) {
            return 8;
        }
        if( strFieldName.equals("FuncFlag") ) {
            return 9;
        }
        if( strFieldName.equals("TransDate") ) {
            return 10;
        }
        if( strFieldName.equals("TransTime") ) {
            return 11;
        }
        if( strFieldName.equals("ManageCom") ) {
            return 12;
        }
        if( strFieldName.equals("RiskCode") ) {
            return 13;
        }
        if( strFieldName.equals("ProposalNo") ) {
            return 14;
        }
        if( strFieldName.equals("PrtNo") ) {
            return 15;
        }
        if( strFieldName.equals("PolNo") ) {
            return 16;
        }
        if( strFieldName.equals("EdorNo") ) {
            return 17;
        }
        if( strFieldName.equals("TempFeeNo") ) {
            return 18;
        }
        if( strFieldName.equals("TransAmnt") ) {
            return 19;
        }
        if( strFieldName.equals("BankAcc") ) {
            return 20;
        }
        if( strFieldName.equals("RCode") ) {
            return 21;
        }
        if( strFieldName.equals("TransStatus") ) {
            return 22;
        }
        if( strFieldName.equals("Status") ) {
            return 23;
        }
        if( strFieldName.equals("Descr") ) {
            return 24;
        }
        if( strFieldName.equals("Temp") ) {
            return 25;
        }
        if( strFieldName.equals("MakeDate") ) {
            return 26;
        }
        if( strFieldName.equals("MakeTime") ) {
            return 27;
        }
        if( strFieldName.equals("ModifyDate") ) {
            return 28;
        }
        if( strFieldName.equals("ModifyTime") ) {
            return 29;
        }
        if( strFieldName.equals("State_Code") ) {
            return 30;
        }
        if( strFieldName.equals("RequestId") ) {
            return 31;
        }
        if( strFieldName.equals("OutServiceCode") ) {
            return 32;
        }
        if( strFieldName.equals("ClientIP") ) {
            return 33;
        }
        if( strFieldName.equals("ClientPort") ) {
            return 34;
        }
        if( strFieldName.equals("IssueWay") ) {
            return 35;
        }
        if( strFieldName.equals("ServiceStartTime") ) {
            return 36;
        }
        if( strFieldName.equals("ServiceEndTime") ) {
            return 37;
        }
        if( strFieldName.equals("RBankVSMP") ) {
            return 38;
        }
        if( strFieldName.equals("DesBankVSMP") ) {
            return 39;
        }
        if( strFieldName.equals("RMPVSKernel") ) {
            return 40;
        }
        if( strFieldName.equals("DesMPVSKernel") ) {
            return 41;
        }
        if( strFieldName.equals("ResultBalance") ) {
            return 42;
        }
        if( strFieldName.equals("DesBalance") ) {
            return 43;
        }
        if( strFieldName.equals("bak1") ) {
            return 44;
        }
        if( strFieldName.equals("bak2") ) {
            return 45;
        }
        if( strFieldName.equals("bak3") ) {
            return 46;
        }
        if( strFieldName.equals("bak4") ) {
            return 47;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "TransStatusID";
                break;
            case 1:
                strFieldName = "ShardingID";
                break;
            case 2:
                strFieldName = "TransCode";
                break;
            case 3:
                strFieldName = "ReportNo";
                break;
            case 4:
                strFieldName = "BankCode";
                break;
            case 5:
                strFieldName = "BankBranch";
                break;
            case 6:
                strFieldName = "BankNode";
                break;
            case 7:
                strFieldName = "BankOperator";
                break;
            case 8:
                strFieldName = "TransNo";
                break;
            case 9:
                strFieldName = "FuncFlag";
                break;
            case 10:
                strFieldName = "TransDate";
                break;
            case 11:
                strFieldName = "TransTime";
                break;
            case 12:
                strFieldName = "ManageCom";
                break;
            case 13:
                strFieldName = "RiskCode";
                break;
            case 14:
                strFieldName = "ProposalNo";
                break;
            case 15:
                strFieldName = "PrtNo";
                break;
            case 16:
                strFieldName = "PolNo";
                break;
            case 17:
                strFieldName = "EdorNo";
                break;
            case 18:
                strFieldName = "TempFeeNo";
                break;
            case 19:
                strFieldName = "TransAmnt";
                break;
            case 20:
                strFieldName = "BankAcc";
                break;
            case 21:
                strFieldName = "RCode";
                break;
            case 22:
                strFieldName = "TransStatus";
                break;
            case 23:
                strFieldName = "Status";
                break;
            case 24:
                strFieldName = "Descr";
                break;
            case 25:
                strFieldName = "Temp";
                break;
            case 26:
                strFieldName = "MakeDate";
                break;
            case 27:
                strFieldName = "MakeTime";
                break;
            case 28:
                strFieldName = "ModifyDate";
                break;
            case 29:
                strFieldName = "ModifyTime";
                break;
            case 30:
                strFieldName = "State_Code";
                break;
            case 31:
                strFieldName = "RequestId";
                break;
            case 32:
                strFieldName = "OutServiceCode";
                break;
            case 33:
                strFieldName = "ClientIP";
                break;
            case 34:
                strFieldName = "ClientPort";
                break;
            case 35:
                strFieldName = "IssueWay";
                break;
            case 36:
                strFieldName = "ServiceStartTime";
                break;
            case 37:
                strFieldName = "ServiceEndTime";
                break;
            case 38:
                strFieldName = "RBankVSMP";
                break;
            case 39:
                strFieldName = "DesBankVSMP";
                break;
            case 40:
                strFieldName = "RMPVSKernel";
                break;
            case 41:
                strFieldName = "DesMPVSKernel";
                break;
            case 42:
                strFieldName = "ResultBalance";
                break;
            case 43:
                strFieldName = "DesBalance";
                break;
            case 44:
                strFieldName = "bak1";
                break;
            case 45:
                strFieldName = "bak2";
                break;
            case 46:
                strFieldName = "bak3";
                break;
            case 47:
                strFieldName = "bak4";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "TRANSSTATUSID":
                return Schema.TYPE_LONG;
            case "SHARDINGID":
                return Schema.TYPE_STRING;
            case "TRANSCODE":
                return Schema.TYPE_STRING;
            case "REPORTNO":
                return Schema.TYPE_STRING;
            case "BANKCODE":
                return Schema.TYPE_STRING;
            case "BANKBRANCH":
                return Schema.TYPE_STRING;
            case "BANKNODE":
                return Schema.TYPE_STRING;
            case "BANKOPERATOR":
                return Schema.TYPE_STRING;
            case "TRANSNO":
                return Schema.TYPE_STRING;
            case "FUNCFLAG":
                return Schema.TYPE_STRING;
            case "TRANSDATE":
                return Schema.TYPE_DATE;
            case "TRANSTIME":
                return Schema.TYPE_STRING;
            case "MANAGECOM":
                return Schema.TYPE_STRING;
            case "RISKCODE":
                return Schema.TYPE_STRING;
            case "PROPOSALNO":
                return Schema.TYPE_STRING;
            case "PRTNO":
                return Schema.TYPE_STRING;
            case "POLNO":
                return Schema.TYPE_STRING;
            case "EDORNO":
                return Schema.TYPE_STRING;
            case "TEMPFEENO":
                return Schema.TYPE_STRING;
            case "TRANSAMNT":
                return Schema.TYPE_DOUBLE;
            case "BANKACC":
                return Schema.TYPE_STRING;
            case "RCODE":
                return Schema.TYPE_STRING;
            case "TRANSSTATUS":
                return Schema.TYPE_STRING;
            case "STATUS":
                return Schema.TYPE_STRING;
            case "DESCR":
                return Schema.TYPE_STRING;
            case "TEMP":
                return Schema.TYPE_STRING;
            case "MAKEDATE":
                return Schema.TYPE_DATE;
            case "MAKETIME":
                return Schema.TYPE_STRING;
            case "MODIFYDATE":
                return Schema.TYPE_DATE;
            case "MODIFYTIME":
                return Schema.TYPE_STRING;
            case "STATE_CODE":
                return Schema.TYPE_STRING;
            case "REQUESTID":
                return Schema.TYPE_STRING;
            case "OUTSERVICECODE":
                return Schema.TYPE_STRING;
            case "CLIENTIP":
                return Schema.TYPE_STRING;
            case "CLIENTPORT":
                return Schema.TYPE_STRING;
            case "ISSUEWAY":
                return Schema.TYPE_STRING;
            case "SERVICESTARTTIME":
                return Schema.TYPE_DATE;
            case "SERVICEENDTIME":
                return Schema.TYPE_DATE;
            case "RBANKVSMP":
                return Schema.TYPE_STRING;
            case "DESBANKVSMP":
                return Schema.TYPE_STRING;
            case "RMPVSKERNEL":
                return Schema.TYPE_STRING;
            case "DESMPVSKERNEL":
                return Schema.TYPE_STRING;
            case "RESULTBALANCE":
                return Schema.TYPE_STRING;
            case "DESBALANCE":
                return Schema.TYPE_STRING;
            case "BAK1":
                return Schema.TYPE_STRING;
            case "BAK2":
                return Schema.TYPE_STRING;
            case "BAK3":
                return Schema.TYPE_STRING;
            case "BAK4":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_LONG;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_STRING;
            case 6:
                return Schema.TYPE_STRING;
            case 7:
                return Schema.TYPE_STRING;
            case 8:
                return Schema.TYPE_STRING;
            case 9:
                return Schema.TYPE_STRING;
            case 10:
                return Schema.TYPE_DATE;
            case 11:
                return Schema.TYPE_STRING;
            case 12:
                return Schema.TYPE_STRING;
            case 13:
                return Schema.TYPE_STRING;
            case 14:
                return Schema.TYPE_STRING;
            case 15:
                return Schema.TYPE_STRING;
            case 16:
                return Schema.TYPE_STRING;
            case 17:
                return Schema.TYPE_STRING;
            case 18:
                return Schema.TYPE_STRING;
            case 19:
                return Schema.TYPE_DOUBLE;
            case 20:
                return Schema.TYPE_STRING;
            case 21:
                return Schema.TYPE_STRING;
            case 22:
                return Schema.TYPE_STRING;
            case 23:
                return Schema.TYPE_STRING;
            case 24:
                return Schema.TYPE_STRING;
            case 25:
                return Schema.TYPE_STRING;
            case 26:
                return Schema.TYPE_DATE;
            case 27:
                return Schema.TYPE_STRING;
            case 28:
                return Schema.TYPE_DATE;
            case 29:
                return Schema.TYPE_STRING;
            case 30:
                return Schema.TYPE_STRING;
            case 31:
                return Schema.TYPE_STRING;
            case 32:
                return Schema.TYPE_STRING;
            case 33:
                return Schema.TYPE_STRING;
            case 34:
                return Schema.TYPE_STRING;
            case 35:
                return Schema.TYPE_STRING;
            case 36:
                return Schema.TYPE_DATE;
            case 37:
                return Schema.TYPE_DATE;
            case 38:
                return Schema.TYPE_STRING;
            case 39:
                return Schema.TYPE_STRING;
            case 40:
                return Schema.TYPE_STRING;
            case 41:
                return Schema.TYPE_STRING;
            case 42:
                return Schema.TYPE_STRING;
            case 43:
                return Schema.TYPE_STRING;
            case 44:
                return Schema.TYPE_STRING;
            case 45:
                return Schema.TYPE_STRING;
            case 46:
                return Schema.TYPE_STRING;
            case 47:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
}
