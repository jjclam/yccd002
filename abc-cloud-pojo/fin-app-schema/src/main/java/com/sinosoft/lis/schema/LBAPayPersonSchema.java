/**
 * Copyright (c) 2017 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import com.sinosoft.lis.db.LBAPayPersonDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.*;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

/**
 * <p>ClassName: LBAPayPersonSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2017</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2017-10-21
 */
public class LBAPayPersonSchema implements Schema, Cloneable {
    // @Field
    /** Id */
    private long APayPersonID;
    /** Shardingid */
    private String ShardingID;
    /** 保单号码 */
    private String PolNo;
    /** 第几次交费 */
    private int PayCount;
    /** 集体合同号码 */
    private String GrpContNo;
    /** 集体保单号码 */
    private String GrpPolNo;
    /** 个人合同号码 */
    private String ContNo;
    /** 管理机构 */
    private String ManageCom;
    /** 代理机构 */
    private String AgentCom;
    /** 代理机构内部分类 */
    private String AgentType;
    /** 险种编码 */
    private String RiskCode;
    /** 代理人编码 */
    private String AgentCode;
    /** 代理人组别 */
    private String AgentGroup;
    /** 续保收费标记 */
    private String PayTypeFlag;
    /** 投保人客户号码 */
    private String AppntNo;
    /** 交费收据号码 */
    private String PayNo;
    /** 交费目的分类 */
    private String PayAimClass;
    /** 责任编码 */
    private String DutyCode;
    /** 交费计划编码 */
    private String PayPlanCode;
    /** 总应交金额 */
    private double SumDuePayMoney;
    /** 总实交金额 */
    private double SumActuPayMoney;
    /** 交费间隔 */
    private int PayIntv;
    /** 交费日期 */
    private Date PayDate;
    /** 交费类型 */
    private String PayType;
    /** 到帐日期 */
    private Date EnterAccDate;
    /** 确认日期 */
    private Date ConfDate;
    /** 原交至日期 */
    private Date LastPayToDate;
    /** 现交至日期 */
    private Date CurPayToDate;
    /** 转入保险帐户状态 */
    private String InInsuAccState;
    /** 复核人编码 */
    private String ApproveCode;
    /** 复核日期 */
    private Date ApproveDate;
    /** 复核时间 */
    private String ApproveTime;
    /** 流水号 */
    private String SerialNo;
    /** 操作员 */
    private String Operator;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 通知书号码 */
    private String GetNoticeNo;
    /** 最后一次修改日期 */
    private Date ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime;
    /** 保全批单号 */
    private String EdorNo;
    /** 主险保单年度 */
    private int MainPolYear;
    /** 备份流水号 */
    private String BackUpSerialNo;
    /** 备份原因 */
    private String BackUpReason;

    public static final int FIELDNUM = 43;    // 数据库表的字段个数

    private static String[] PK;                // 主键

    private FDate fDate = new FDate();        // 处理日期

    public CErrors mErrors;            // 错误信息

    // @Constructor
    public LBAPayPersonSchema() {
        mErrors = new CErrors();

        String[] pk = new String[1];
        pk[0] = "APayPersonID";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException {
        LBAPayPersonSchema cloned = (LBAPayPersonSchema)super.clone();
        cloned.fDate = (FDate) fDate.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK() {
        return PK;
    }

    public long getAPayPersonID() {
        return APayPersonID;
    }
    public void setAPayPersonID(long aAPayPersonID) {
        APayPersonID = aAPayPersonID;
    }
    public void setAPayPersonID(String aAPayPersonID) {
        if (aAPayPersonID != null && !aAPayPersonID.equals("")) {
            APayPersonID = new Long(aAPayPersonID).longValue();
        }
    }

    public String getShardingID() {
        return ShardingID;
    }
    public void setShardingID(String aShardingID) {
        ShardingID = aShardingID;
    }
    public String getPolNo() {
        return PolNo;
    }
    public void setPolNo(String aPolNo) {
        PolNo = aPolNo;
    }
    public int getPayCount() {
        return PayCount;
    }
    public void setPayCount(int aPayCount) {
        PayCount = aPayCount;
    }
    public void setPayCount(String aPayCount) {
        if (aPayCount != null && !aPayCount.equals("")) {
            Integer tInteger = new Integer(aPayCount);
            int i = tInteger.intValue();
            PayCount = i;
        }
    }

    public String getGrpContNo() {
        return GrpContNo;
    }
    public void setGrpContNo(String aGrpContNo) {
        GrpContNo = aGrpContNo;
    }
    public String getGrpPolNo() {
        return GrpPolNo;
    }
    public void setGrpPolNo(String aGrpPolNo) {
        GrpPolNo = aGrpPolNo;
    }
    public String getContNo() {
        return ContNo;
    }
    public void setContNo(String aContNo) {
        ContNo = aContNo;
    }
    public String getManageCom() {
        return ManageCom;
    }
    public void setManageCom(String aManageCom) {
        ManageCom = aManageCom;
    }
    public String getAgentCom() {
        return AgentCom;
    }
    public void setAgentCom(String aAgentCom) {
        AgentCom = aAgentCom;
    }
    public String getAgentType() {
        return AgentType;
    }
    public void setAgentType(String aAgentType) {
        AgentType = aAgentType;
    }
    public String getRiskCode() {
        return RiskCode;
    }
    public void setRiskCode(String aRiskCode) {
        RiskCode = aRiskCode;
    }
    public String getAgentCode() {
        return AgentCode;
    }
    public void setAgentCode(String aAgentCode) {
        AgentCode = aAgentCode;
    }
    public String getAgentGroup() {
        return AgentGroup;
    }
    public void setAgentGroup(String aAgentGroup) {
        AgentGroup = aAgentGroup;
    }
    public String getPayTypeFlag() {
        return PayTypeFlag;
    }
    public void setPayTypeFlag(String aPayTypeFlag) {
        PayTypeFlag = aPayTypeFlag;
    }
    public String getAppntNo() {
        return AppntNo;
    }
    public void setAppntNo(String aAppntNo) {
        AppntNo = aAppntNo;
    }
    public String getPayNo() {
        return PayNo;
    }
    public void setPayNo(String aPayNo) {
        PayNo = aPayNo;
    }
    public String getPayAimClass() {
        return PayAimClass;
    }
    public void setPayAimClass(String aPayAimClass) {
        PayAimClass = aPayAimClass;
    }
    public String getDutyCode() {
        return DutyCode;
    }
    public void setDutyCode(String aDutyCode) {
        DutyCode = aDutyCode;
    }
    public String getPayPlanCode() {
        return PayPlanCode;
    }
    public void setPayPlanCode(String aPayPlanCode) {
        PayPlanCode = aPayPlanCode;
    }
    public double getSumDuePayMoney() {
        return SumDuePayMoney;
    }
    public void setSumDuePayMoney(double aSumDuePayMoney) {
        SumDuePayMoney = aSumDuePayMoney;
    }
    public void setSumDuePayMoney(String aSumDuePayMoney) {
        if (aSumDuePayMoney != null && !aSumDuePayMoney.equals("")) {
            Double tDouble = new Double(aSumDuePayMoney);
            double d = tDouble.doubleValue();
            SumDuePayMoney = d;
        }
    }

    public double getSumActuPayMoney() {
        return SumActuPayMoney;
    }
    public void setSumActuPayMoney(double aSumActuPayMoney) {
        SumActuPayMoney = aSumActuPayMoney;
    }
    public void setSumActuPayMoney(String aSumActuPayMoney) {
        if (aSumActuPayMoney != null && !aSumActuPayMoney.equals("")) {
            Double tDouble = new Double(aSumActuPayMoney);
            double d = tDouble.doubleValue();
            SumActuPayMoney = d;
        }
    }

    public int getPayIntv() {
        return PayIntv;
    }
    public void setPayIntv(int aPayIntv) {
        PayIntv = aPayIntv;
    }
    public void setPayIntv(String aPayIntv) {
        if (aPayIntv != null && !aPayIntv.equals("")) {
            Integer tInteger = new Integer(aPayIntv);
            int i = tInteger.intValue();
            PayIntv = i;
        }
    }

    public String getPayDate() {
        if(PayDate != null) {
            return fDate.getString(PayDate);
        } else {
            return null;
        }
    }
    public void setPayDate(Date aPayDate) {
        PayDate = aPayDate;
    }
    public void setPayDate(String aPayDate) {
        if (aPayDate != null && !aPayDate.equals("")) {
            PayDate = fDate.getDate(aPayDate);
        } else
            PayDate = null;
    }

    public String getPayType() {
        return PayType;
    }
    public void setPayType(String aPayType) {
        PayType = aPayType;
    }
    public String getEnterAccDate() {
        if(EnterAccDate != null) {
            return fDate.getString(EnterAccDate);
        } else {
            return null;
        }
    }
    public void setEnterAccDate(Date aEnterAccDate) {
        EnterAccDate = aEnterAccDate;
    }
    public void setEnterAccDate(String aEnterAccDate) {
        if (aEnterAccDate != null && !aEnterAccDate.equals("")) {
            EnterAccDate = fDate.getDate(aEnterAccDate);
        } else
            EnterAccDate = null;
    }

    public String getConfDate() {
        if(ConfDate != null) {
            return fDate.getString(ConfDate);
        } else {
            return null;
        }
    }
    public void setConfDate(Date aConfDate) {
        ConfDate = aConfDate;
    }
    public void setConfDate(String aConfDate) {
        if (aConfDate != null && !aConfDate.equals("")) {
            ConfDate = fDate.getDate(aConfDate);
        } else
            ConfDate = null;
    }

    public String getLastPayToDate() {
        if(LastPayToDate != null) {
            return fDate.getString(LastPayToDate);
        } else {
            return null;
        }
    }
    public void setLastPayToDate(Date aLastPayToDate) {
        LastPayToDate = aLastPayToDate;
    }
    public void setLastPayToDate(String aLastPayToDate) {
        if (aLastPayToDate != null && !aLastPayToDate.equals("")) {
            LastPayToDate = fDate.getDate(aLastPayToDate);
        } else
            LastPayToDate = null;
    }

    public String getCurPayToDate() {
        if(CurPayToDate != null) {
            return fDate.getString(CurPayToDate);
        } else {
            return null;
        }
    }
    public void setCurPayToDate(Date aCurPayToDate) {
        CurPayToDate = aCurPayToDate;
    }
    public void setCurPayToDate(String aCurPayToDate) {
        if (aCurPayToDate != null && !aCurPayToDate.equals("")) {
            CurPayToDate = fDate.getDate(aCurPayToDate);
        } else
            CurPayToDate = null;
    }

    public String getInInsuAccState() {
        return InInsuAccState;
    }
    public void setInInsuAccState(String aInInsuAccState) {
        InInsuAccState = aInInsuAccState;
    }
    public String getApproveCode() {
        return ApproveCode;
    }
    public void setApproveCode(String aApproveCode) {
        ApproveCode = aApproveCode;
    }
    public String getApproveDate() {
        if(ApproveDate != null) {
            return fDate.getString(ApproveDate);
        } else {
            return null;
        }
    }
    public void setApproveDate(Date aApproveDate) {
        ApproveDate = aApproveDate;
    }
    public void setApproveDate(String aApproveDate) {
        if (aApproveDate != null && !aApproveDate.equals("")) {
            ApproveDate = fDate.getDate(aApproveDate);
        } else
            ApproveDate = null;
    }

    public String getApproveTime() {
        return ApproveTime;
    }
    public void setApproveTime(String aApproveTime) {
        ApproveTime = aApproveTime;
    }
    public String getSerialNo() {
        return SerialNo;
    }
    public void setSerialNo(String aSerialNo) {
        SerialNo = aSerialNo;
    }
    public String getOperator() {
        return Operator;
    }
    public void setOperator(String aOperator) {
        Operator = aOperator;
    }
    public String getMakeDate() {
        if(MakeDate != null) {
            return fDate.getString(MakeDate);
        } else {
            return null;
        }
    }
    public void setMakeDate(Date aMakeDate) {
        MakeDate = aMakeDate;
    }
    public void setMakeDate(String aMakeDate) {
        if (aMakeDate != null && !aMakeDate.equals("")) {
            MakeDate = fDate.getDate(aMakeDate);
        } else
            MakeDate = null;
    }

    public String getMakeTime() {
        return MakeTime;
    }
    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }
    public String getGetNoticeNo() {
        return GetNoticeNo;
    }
    public void setGetNoticeNo(String aGetNoticeNo) {
        GetNoticeNo = aGetNoticeNo;
    }
    public String getModifyDate() {
        if(ModifyDate != null) {
            return fDate.getString(ModifyDate);
        } else {
            return null;
        }
    }
    public void setModifyDate(Date aModifyDate) {
        ModifyDate = aModifyDate;
    }
    public void setModifyDate(String aModifyDate) {
        if (aModifyDate != null && !aModifyDate.equals("")) {
            ModifyDate = fDate.getDate(aModifyDate);
        } else
            ModifyDate = null;
    }

    public String getModifyTime() {
        return ModifyTime;
    }
    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }
    public String getEdorNo() {
        return EdorNo;
    }
    public void setEdorNo(String aEdorNo) {
        EdorNo = aEdorNo;
    }
    public int getMainPolYear() {
        return MainPolYear;
    }
    public void setMainPolYear(int aMainPolYear) {
        MainPolYear = aMainPolYear;
    }
    public void setMainPolYear(String aMainPolYear) {
        if (aMainPolYear != null && !aMainPolYear.equals("")) {
            Integer tInteger = new Integer(aMainPolYear);
            int i = tInteger.intValue();
            MainPolYear = i;
        }
    }

    public String getBackUpSerialNo() {
        return BackUpSerialNo;
    }
    public void setBackUpSerialNo(String aBackUpSerialNo) {
        BackUpSerialNo = aBackUpSerialNo;
    }
    public String getBackUpReason() {
        return BackUpReason;
    }
    public void setBackUpReason(String aBackUpReason) {
        BackUpReason = aBackUpReason;
    }

    /**
    * 使用另外一个 LBAPayPersonSchema 对象给 Schema 赋值
    * @param: aLBAPayPersonSchema LBAPayPersonSchema
    **/
    public void setSchema(LBAPayPersonSchema aLBAPayPersonSchema) {
        this.APayPersonID = aLBAPayPersonSchema.getAPayPersonID();
        this.ShardingID = aLBAPayPersonSchema.getShardingID();
        this.PolNo = aLBAPayPersonSchema.getPolNo();
        this.PayCount = aLBAPayPersonSchema.getPayCount();
        this.GrpContNo = aLBAPayPersonSchema.getGrpContNo();
        this.GrpPolNo = aLBAPayPersonSchema.getGrpPolNo();
        this.ContNo = aLBAPayPersonSchema.getContNo();
        this.ManageCom = aLBAPayPersonSchema.getManageCom();
        this.AgentCom = aLBAPayPersonSchema.getAgentCom();
        this.AgentType = aLBAPayPersonSchema.getAgentType();
        this.RiskCode = aLBAPayPersonSchema.getRiskCode();
        this.AgentCode = aLBAPayPersonSchema.getAgentCode();
        this.AgentGroup = aLBAPayPersonSchema.getAgentGroup();
        this.PayTypeFlag = aLBAPayPersonSchema.getPayTypeFlag();
        this.AppntNo = aLBAPayPersonSchema.getAppntNo();
        this.PayNo = aLBAPayPersonSchema.getPayNo();
        this.PayAimClass = aLBAPayPersonSchema.getPayAimClass();
        this.DutyCode = aLBAPayPersonSchema.getDutyCode();
        this.PayPlanCode = aLBAPayPersonSchema.getPayPlanCode();
        this.SumDuePayMoney = aLBAPayPersonSchema.getSumDuePayMoney();
        this.SumActuPayMoney = aLBAPayPersonSchema.getSumActuPayMoney();
        this.PayIntv = aLBAPayPersonSchema.getPayIntv();
        this.PayDate = fDate.getDate( aLBAPayPersonSchema.getPayDate());
        this.PayType = aLBAPayPersonSchema.getPayType();
        this.EnterAccDate = fDate.getDate( aLBAPayPersonSchema.getEnterAccDate());
        this.ConfDate = fDate.getDate( aLBAPayPersonSchema.getConfDate());
        this.LastPayToDate = fDate.getDate( aLBAPayPersonSchema.getLastPayToDate());
        this.CurPayToDate = fDate.getDate( aLBAPayPersonSchema.getCurPayToDate());
        this.InInsuAccState = aLBAPayPersonSchema.getInInsuAccState();
        this.ApproveCode = aLBAPayPersonSchema.getApproveCode();
        this.ApproveDate = fDate.getDate( aLBAPayPersonSchema.getApproveDate());
        this.ApproveTime = aLBAPayPersonSchema.getApproveTime();
        this.SerialNo = aLBAPayPersonSchema.getSerialNo();
        this.Operator = aLBAPayPersonSchema.getOperator();
        this.MakeDate = fDate.getDate( aLBAPayPersonSchema.getMakeDate());
        this.MakeTime = aLBAPayPersonSchema.getMakeTime();
        this.GetNoticeNo = aLBAPayPersonSchema.getGetNoticeNo();
        this.ModifyDate = fDate.getDate( aLBAPayPersonSchema.getModifyDate());
        this.ModifyTime = aLBAPayPersonSchema.getModifyTime();
        this.EdorNo = aLBAPayPersonSchema.getEdorNo();
        this.MainPolYear = aLBAPayPersonSchema.getMainPolYear();
        this.BackUpSerialNo = aLBAPayPersonSchema.getBackUpSerialNo();
        this.BackUpReason = aLBAPayPersonSchema.getBackUpReason();
    }

    /**
    * 使用 ResultSet 中的第 i 行给 Schema 赋值
    * @param: rs ResultSet
    * @param: i int
    * @return: boolean
    **/
    public boolean setSchema(ResultSet rs,int i) {
        try {
            //rs.absolute(i);        // 非滚动游标
            this.APayPersonID = rs.getLong("APayPersonID");
            if( rs.getString("ShardingID") == null )
                this.ShardingID = null;
            else
                this.ShardingID = rs.getString("ShardingID").trim();

            if( rs.getString("PolNo") == null )
                this.PolNo = null;
            else
                this.PolNo = rs.getString("PolNo").trim();

            this.PayCount = rs.getInt("PayCount");
            if( rs.getString("GrpContNo") == null )
                this.GrpContNo = null;
            else
                this.GrpContNo = rs.getString("GrpContNo").trim();

            if( rs.getString("GrpPolNo") == null )
                this.GrpPolNo = null;
            else
                this.GrpPolNo = rs.getString("GrpPolNo").trim();

            if( rs.getString("ContNo") == null )
                this.ContNo = null;
            else
                this.ContNo = rs.getString("ContNo").trim();

            if( rs.getString("ManageCom") == null )
                this.ManageCom = null;
            else
                this.ManageCom = rs.getString("ManageCom").trim();

            if( rs.getString("AgentCom") == null )
                this.AgentCom = null;
            else
                this.AgentCom = rs.getString("AgentCom").trim();

            if( rs.getString("AgentType") == null )
                this.AgentType = null;
            else
                this.AgentType = rs.getString("AgentType").trim();

            if( rs.getString("RiskCode") == null )
                this.RiskCode = null;
            else
                this.RiskCode = rs.getString("RiskCode").trim();

            if( rs.getString("AgentCode") == null )
                this.AgentCode = null;
            else
                this.AgentCode = rs.getString("AgentCode").trim();

            if( rs.getString("AgentGroup") == null )
                this.AgentGroup = null;
            else
                this.AgentGroup = rs.getString("AgentGroup").trim();

            if( rs.getString("PayTypeFlag") == null )
                this.PayTypeFlag = null;
            else
                this.PayTypeFlag = rs.getString("PayTypeFlag").trim();

            if( rs.getString("AppntNo") == null )
                this.AppntNo = null;
            else
                this.AppntNo = rs.getString("AppntNo").trim();

            if( rs.getString("PayNo") == null )
                this.PayNo = null;
            else
                this.PayNo = rs.getString("PayNo").trim();

            if( rs.getString("PayAimClass") == null )
                this.PayAimClass = null;
            else
                this.PayAimClass = rs.getString("PayAimClass").trim();

            if( rs.getString("DutyCode") == null )
                this.DutyCode = null;
            else
                this.DutyCode = rs.getString("DutyCode").trim();

            if( rs.getString("PayPlanCode") == null )
                this.PayPlanCode = null;
            else
                this.PayPlanCode = rs.getString("PayPlanCode").trim();

            this.SumDuePayMoney = rs.getDouble("SumDuePayMoney");
            this.SumActuPayMoney = rs.getDouble("SumActuPayMoney");
            this.PayIntv = rs.getInt("PayIntv");
            this.PayDate = rs.getDate("PayDate");
            if( rs.getString("PayType") == null )
                this.PayType = null;
            else
                this.PayType = rs.getString("PayType").trim();

            this.EnterAccDate = rs.getDate("EnterAccDate");
            this.ConfDate = rs.getDate("ConfDate");
            this.LastPayToDate = rs.getDate("LastPayToDate");
            this.CurPayToDate = rs.getDate("CurPayToDate");
            if( rs.getString("InInsuAccState") == null )
                this.InInsuAccState = null;
            else
                this.InInsuAccState = rs.getString("InInsuAccState").trim();

            if( rs.getString("ApproveCode") == null )
                this.ApproveCode = null;
            else
                this.ApproveCode = rs.getString("ApproveCode").trim();

            this.ApproveDate = rs.getDate("ApproveDate");
            if( rs.getString("ApproveTime") == null )
                this.ApproveTime = null;
            else
                this.ApproveTime = rs.getString("ApproveTime").trim();

            if( rs.getString("SerialNo") == null )
                this.SerialNo = null;
            else
                this.SerialNo = rs.getString("SerialNo").trim();

            if( rs.getString("Operator") == null )
                this.Operator = null;
            else
                this.Operator = rs.getString("Operator").trim();

            this.MakeDate = rs.getDate("MakeDate");
            if( rs.getString("MakeTime") == null )
                this.MakeTime = null;
            else
                this.MakeTime = rs.getString("MakeTime").trim();

            if( rs.getString("GetNoticeNo") == null )
                this.GetNoticeNo = null;
            else
                this.GetNoticeNo = rs.getString("GetNoticeNo").trim();

            this.ModifyDate = rs.getDate("ModifyDate");
            if( rs.getString("ModifyTime") == null )
                this.ModifyTime = null;
            else
                this.ModifyTime = rs.getString("ModifyTime").trim();

            if( rs.getString("EdorNo") == null )
                this.EdorNo = null;
            else
                this.EdorNo = rs.getString("EdorNo").trim();

            this.MainPolYear = rs.getInt("MainPolYear");
            if( rs.getString("BackUpSerialNo") == null )
                this.BackUpSerialNo = null;
            else
                this.BackUpSerialNo = rs.getString("BackUpSerialNo").trim();

            if( rs.getString("BackUpReason") == null )
                this.BackUpReason = null;
            else
                this.BackUpReason = rs.getString("BackUpReason").trim();

        }
        catch(SQLException sqle) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LBAPayPersonSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = ExceptionUtils.exceptionToString(sqle);
            this.mErrors .addOneError(tError);
            return false;
        }
        return true;
    }

    public LBAPayPersonSchema getSchema() {
        LBAPayPersonSchema aLBAPayPersonSchema = new LBAPayPersonSchema();
        aLBAPayPersonSchema.setSchema(this);
        return aLBAPayPersonSchema;
    }

    public LBAPayPersonDB getDB() {
        LBAPayPersonDB aDBOper = new LBAPayPersonDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
    * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLBAPayPerson描述/A>表字段
    * @return: String 返回打包后字符串
    **/
    public String encode() {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(ChgData.chgData(APayPersonID));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ShardingID)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(PolNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(PayCount));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(GrpContNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(GrpPolNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ContNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ManageCom)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AgentCom)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AgentType)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(RiskCode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AgentCode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AgentGroup)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(PayTypeFlag)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AppntNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(PayNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(PayAimClass)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(DutyCode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(PayPlanCode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(SumDuePayMoney));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(SumActuPayMoney));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(PayIntv));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( PayDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(PayType)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( EnterAccDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( ConfDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( LastPayToDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( CurPayToDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(InInsuAccState)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ApproveCode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( ApproveDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ApproveTime)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(SerialNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(GetNoticeNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ModifyTime)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(EdorNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(MainPolYear));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(BackUpSerialNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(BackUpReason));
        return strReturn.toString();
    }

    /**
    * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLBAPayPerson>历史记账凭证主表信息</A>表字段
    * @param: strMessage String 包含一条纪录数据的字符串
    * @return: boolean
    **/
    public boolean decode(String strMessage) {
        try {
            APayPersonID = new Long(ChgData.chgNumericStr(StrTool.getStr(strMessage,1, SysConst.PACKAGESPILTER))).longValue();
            ShardingID = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
            PolNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
            PayCount = new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,4, SysConst.PACKAGESPILTER))).intValue();
            GrpContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
            GrpPolNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
            ContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
            ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
            AgentCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
            AgentType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
            RiskCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
            AgentCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
            AgentGroup = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
            PayTypeFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER );
            AppntNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15, SysConst.PACKAGESPILTER );
            PayNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16, SysConst.PACKAGESPILTER );
            PayAimClass = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17, SysConst.PACKAGESPILTER );
            DutyCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18, SysConst.PACKAGESPILTER );
            PayPlanCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19, SysConst.PACKAGESPILTER );
            SumDuePayMoney = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,20, SysConst.PACKAGESPILTER))).doubleValue();
            SumActuPayMoney = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,21, SysConst.PACKAGESPILTER))).doubleValue();
            PayIntv = new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,22, SysConst.PACKAGESPILTER))).intValue();
            PayDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 23, SysConst.PACKAGESPILTER));
            PayType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 24, SysConst.PACKAGESPILTER );
            EnterAccDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 25, SysConst.PACKAGESPILTER));
            ConfDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 26, SysConst.PACKAGESPILTER));
            LastPayToDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 27, SysConst.PACKAGESPILTER));
            CurPayToDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 28, SysConst.PACKAGESPILTER));
            InInsuAccState = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 29, SysConst.PACKAGESPILTER );
            ApproveCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 30, SysConst.PACKAGESPILTER );
            ApproveDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 31, SysConst.PACKAGESPILTER));
            ApproveTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 32, SysConst.PACKAGESPILTER );
            SerialNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 33, SysConst.PACKAGESPILTER );
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 34, SysConst.PACKAGESPILTER );
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 35, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 36, SysConst.PACKAGESPILTER );
            GetNoticeNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 37, SysConst.PACKAGESPILTER );
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 38, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 39, SysConst.PACKAGESPILTER );
            EdorNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 40, SysConst.PACKAGESPILTER );
            MainPolYear = new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,41, SysConst.PACKAGESPILTER))).intValue();
            BackUpSerialNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 42, SysConst.PACKAGESPILTER );
            BackUpReason = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 43, SysConst.PACKAGESPILTER );
        }
        catch(NumberFormatException ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LBAPayPersonSchema";
            tError.functionName = "decode";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            return false;
        }
        return true;
    }

    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("APayPersonID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(APayPersonID));
        }
        if (FCode.equalsIgnoreCase("ShardingID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ShardingID));
        }
        if (FCode.equalsIgnoreCase("PolNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PolNo));
        }
        if (FCode.equalsIgnoreCase("PayCount")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PayCount));
        }
        if (FCode.equalsIgnoreCase("GrpContNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GrpContNo));
        }
        if (FCode.equalsIgnoreCase("GrpPolNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GrpPolNo));
        }
        if (FCode.equalsIgnoreCase("ContNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ContNo));
        }
        if (FCode.equalsIgnoreCase("ManageCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
        }
        if (FCode.equalsIgnoreCase("AgentCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentCom));
        }
        if (FCode.equalsIgnoreCase("AgentType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentType));
        }
        if (FCode.equalsIgnoreCase("RiskCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RiskCode));
        }
        if (FCode.equalsIgnoreCase("AgentCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentCode));
        }
        if (FCode.equalsIgnoreCase("AgentGroup")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentGroup));
        }
        if (FCode.equalsIgnoreCase("PayTypeFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PayTypeFlag));
        }
        if (FCode.equalsIgnoreCase("AppntNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AppntNo));
        }
        if (FCode.equalsIgnoreCase("PayNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PayNo));
        }
        if (FCode.equalsIgnoreCase("PayAimClass")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PayAimClass));
        }
        if (FCode.equalsIgnoreCase("DutyCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DutyCode));
        }
        if (FCode.equalsIgnoreCase("PayPlanCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PayPlanCode));
        }
        if (FCode.equalsIgnoreCase("SumDuePayMoney")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SumDuePayMoney));
        }
        if (FCode.equalsIgnoreCase("SumActuPayMoney")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SumActuPayMoney));
        }
        if (FCode.equalsIgnoreCase("PayIntv")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PayIntv));
        }
        if (FCode.equalsIgnoreCase("PayDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getPayDate()));
        }
        if (FCode.equalsIgnoreCase("PayType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PayType));
        }
        if (FCode.equalsIgnoreCase("EnterAccDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getEnterAccDate()));
        }
        if (FCode.equalsIgnoreCase("ConfDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getConfDate()));
        }
        if (FCode.equalsIgnoreCase("LastPayToDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getLastPayToDate()));
        }
        if (FCode.equalsIgnoreCase("CurPayToDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getCurPayToDate()));
        }
        if (FCode.equalsIgnoreCase("InInsuAccState")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InInsuAccState));
        }
        if (FCode.equalsIgnoreCase("ApproveCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ApproveCode));
        }
        if (FCode.equalsIgnoreCase("ApproveDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getApproveDate()));
        }
        if (FCode.equalsIgnoreCase("ApproveTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ApproveTime));
        }
        if (FCode.equalsIgnoreCase("SerialNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SerialNo));
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equalsIgnoreCase("GetNoticeNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GetNoticeNo));
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (FCode.equalsIgnoreCase("EdorNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(EdorNo));
        }
        if (FCode.equalsIgnoreCase("MainPolYear")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MainPolYear));
        }
        if (FCode.equalsIgnoreCase("BackUpSerialNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BackUpSerialNo));
        }
        if (FCode.equalsIgnoreCase("BackUpReason")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BackUpReason));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = String.valueOf(APayPersonID);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(ShardingID);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(PolNo);
                break;
            case 3:
                strFieldValue = String.valueOf(PayCount);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(GrpContNo);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(GrpPolNo);
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(ContNo);
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(ManageCom);
                break;
            case 8:
                strFieldValue = StrTool.GBKToUnicode(AgentCom);
                break;
            case 9:
                strFieldValue = StrTool.GBKToUnicode(AgentType);
                break;
            case 10:
                strFieldValue = StrTool.GBKToUnicode(RiskCode);
                break;
            case 11:
                strFieldValue = StrTool.GBKToUnicode(AgentCode);
                break;
            case 12:
                strFieldValue = StrTool.GBKToUnicode(AgentGroup);
                break;
            case 13:
                strFieldValue = StrTool.GBKToUnicode(PayTypeFlag);
                break;
            case 14:
                strFieldValue = StrTool.GBKToUnicode(AppntNo);
                break;
            case 15:
                strFieldValue = StrTool.GBKToUnicode(PayNo);
                break;
            case 16:
                strFieldValue = StrTool.GBKToUnicode(PayAimClass);
                break;
            case 17:
                strFieldValue = StrTool.GBKToUnicode(DutyCode);
                break;
            case 18:
                strFieldValue = StrTool.GBKToUnicode(PayPlanCode);
                break;
            case 19:
                strFieldValue = String.valueOf(SumDuePayMoney);
                break;
            case 20:
                strFieldValue = String.valueOf(SumActuPayMoney);
                break;
            case 21:
                strFieldValue = String.valueOf(PayIntv);
                break;
            case 22:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getPayDate()));
                break;
            case 23:
                strFieldValue = StrTool.GBKToUnicode(PayType);
                break;
            case 24:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getEnterAccDate()));
                break;
            case 25:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getConfDate()));
                break;
            case 26:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getLastPayToDate()));
                break;
            case 27:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getCurPayToDate()));
                break;
            case 28:
                strFieldValue = StrTool.GBKToUnicode(InInsuAccState);
                break;
            case 29:
                strFieldValue = StrTool.GBKToUnicode(ApproveCode);
                break;
            case 30:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getApproveDate()));
                break;
            case 31:
                strFieldValue = StrTool.GBKToUnicode(ApproveTime);
                break;
            case 32:
                strFieldValue = StrTool.GBKToUnicode(SerialNo);
                break;
            case 33:
                strFieldValue = StrTool.GBKToUnicode(Operator);
                break;
            case 34:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
                break;
            case 35:
                strFieldValue = StrTool.GBKToUnicode(MakeTime);
                break;
            case 36:
                strFieldValue = StrTool.GBKToUnicode(GetNoticeNo);
                break;
            case 37:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
                break;
            case 38:
                strFieldValue = StrTool.GBKToUnicode(ModifyTime);
                break;
            case 39:
                strFieldValue = StrTool.GBKToUnicode(EdorNo);
                break;
            case 40:
                strFieldValue = String.valueOf(MainPolYear);
                break;
            case 41:
                strFieldValue = StrTool.GBKToUnicode(BackUpSerialNo);
                break;
            case 42:
                strFieldValue = StrTool.GBKToUnicode(BackUpReason);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("APayPersonID")) {
            if( FValue != null && !FValue.equals("")) {
                APayPersonID = new Long(FValue).longValue();
            }
        }
        if (FCode.equalsIgnoreCase("ShardingID")) {
            if( FValue != null && !FValue.equals(""))
            {
                ShardingID = FValue.trim();
            }
            else
                ShardingID = null;
        }
        if (FCode.equalsIgnoreCase("PolNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                PolNo = FValue.trim();
            }
            else
                PolNo = null;
        }
        if (FCode.equalsIgnoreCase("PayCount")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                PayCount = i;
            }
        }
        if (FCode.equalsIgnoreCase("GrpContNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                GrpContNo = FValue.trim();
            }
            else
                GrpContNo = null;
        }
        if (FCode.equalsIgnoreCase("GrpPolNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                GrpPolNo = FValue.trim();
            }
            else
                GrpPolNo = null;
        }
        if (FCode.equalsIgnoreCase("ContNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                ContNo = FValue.trim();
            }
            else
                ContNo = null;
        }
        if (FCode.equalsIgnoreCase("ManageCom")) {
            if( FValue != null && !FValue.equals(""))
            {
                ManageCom = FValue.trim();
            }
            else
                ManageCom = null;
        }
        if (FCode.equalsIgnoreCase("AgentCom")) {
            if( FValue != null && !FValue.equals(""))
            {
                AgentCom = FValue.trim();
            }
            else
                AgentCom = null;
        }
        if (FCode.equalsIgnoreCase("AgentType")) {
            if( FValue != null && !FValue.equals(""))
            {
                AgentType = FValue.trim();
            }
            else
                AgentType = null;
        }
        if (FCode.equalsIgnoreCase("RiskCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                RiskCode = FValue.trim();
            }
            else
                RiskCode = null;
        }
        if (FCode.equalsIgnoreCase("AgentCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                AgentCode = FValue.trim();
            }
            else
                AgentCode = null;
        }
        if (FCode.equalsIgnoreCase("AgentGroup")) {
            if( FValue != null && !FValue.equals(""))
            {
                AgentGroup = FValue.trim();
            }
            else
                AgentGroup = null;
        }
        if (FCode.equalsIgnoreCase("PayTypeFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                PayTypeFlag = FValue.trim();
            }
            else
                PayTypeFlag = null;
        }
        if (FCode.equalsIgnoreCase("AppntNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                AppntNo = FValue.trim();
            }
            else
                AppntNo = null;
        }
        if (FCode.equalsIgnoreCase("PayNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                PayNo = FValue.trim();
            }
            else
                PayNo = null;
        }
        if (FCode.equalsIgnoreCase("PayAimClass")) {
            if( FValue != null && !FValue.equals(""))
            {
                PayAimClass = FValue.trim();
            }
            else
                PayAimClass = null;
        }
        if (FCode.equalsIgnoreCase("DutyCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                DutyCode = FValue.trim();
            }
            else
                DutyCode = null;
        }
        if (FCode.equalsIgnoreCase("PayPlanCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                PayPlanCode = FValue.trim();
            }
            else
                PayPlanCode = null;
        }
        if (FCode.equalsIgnoreCase("SumDuePayMoney")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                SumDuePayMoney = d;
            }
        }
        if (FCode.equalsIgnoreCase("SumActuPayMoney")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                SumActuPayMoney = d;
            }
        }
        if (FCode.equalsIgnoreCase("PayIntv")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                PayIntv = i;
            }
        }
        if (FCode.equalsIgnoreCase("PayDate")) {
            if(FValue != null && !FValue.equals("")) {
                PayDate = fDate.getDate( FValue );
            }
            else
                PayDate = null;
        }
        if (FCode.equalsIgnoreCase("PayType")) {
            if( FValue != null && !FValue.equals(""))
            {
                PayType = FValue.trim();
            }
            else
                PayType = null;
        }
        if (FCode.equalsIgnoreCase("EnterAccDate")) {
            if(FValue != null && !FValue.equals("")) {
                EnterAccDate = fDate.getDate( FValue );
            }
            else
                EnterAccDate = null;
        }
        if (FCode.equalsIgnoreCase("ConfDate")) {
            if(FValue != null && !FValue.equals("")) {
                ConfDate = fDate.getDate( FValue );
            }
            else
                ConfDate = null;
        }
        if (FCode.equalsIgnoreCase("LastPayToDate")) {
            if(FValue != null && !FValue.equals("")) {
                LastPayToDate = fDate.getDate( FValue );
            }
            else
                LastPayToDate = null;
        }
        if (FCode.equalsIgnoreCase("CurPayToDate")) {
            if(FValue != null && !FValue.equals("")) {
                CurPayToDate = fDate.getDate( FValue );
            }
            else
                CurPayToDate = null;
        }
        if (FCode.equalsIgnoreCase("InInsuAccState")) {
            if( FValue != null && !FValue.equals(""))
            {
                InInsuAccState = FValue.trim();
            }
            else
                InInsuAccState = null;
        }
        if (FCode.equalsIgnoreCase("ApproveCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                ApproveCode = FValue.trim();
            }
            else
                ApproveCode = null;
        }
        if (FCode.equalsIgnoreCase("ApproveDate")) {
            if(FValue != null && !FValue.equals("")) {
                ApproveDate = fDate.getDate( FValue );
            }
            else
                ApproveDate = null;
        }
        if (FCode.equalsIgnoreCase("ApproveTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                ApproveTime = FValue.trim();
            }
            else
                ApproveTime = null;
        }
        if (FCode.equalsIgnoreCase("SerialNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                SerialNo = FValue.trim();
            }
            else
                SerialNo = null;
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            if( FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
                Operator = null;
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if(FValue != null && !FValue.equals("")) {
                MakeDate = fDate.getDate( FValue );
            }
            else
                MakeDate = null;
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
                MakeTime = null;
        }
        if (FCode.equalsIgnoreCase("GetNoticeNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                GetNoticeNo = FValue.trim();
            }
            else
                GetNoticeNo = null;
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if(FValue != null && !FValue.equals("")) {
                ModifyDate = fDate.getDate( FValue );
            }
            else
                ModifyDate = null;
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
                ModifyTime = null;
        }
        if (FCode.equalsIgnoreCase("EdorNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                EdorNo = FValue.trim();
            }
            else
                EdorNo = null;
        }
        if (FCode.equalsIgnoreCase("MainPolYear")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                MainPolYear = i;
            }
        }
        if (FCode.equalsIgnoreCase("BackUpSerialNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                BackUpSerialNo = FValue.trim();
            }
            else
                BackUpSerialNo = null;
        }
        if (FCode.equalsIgnoreCase("BackUpReason")) {
            if( FValue != null && !FValue.equals(""))
            {
                BackUpReason = FValue.trim();
            }
            else
                BackUpReason = null;
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject) return true;
        if (otherObject == null) return false;
        if (getClass() != otherObject.getClass()) return false;
        LBAPayPersonSchema other = (LBAPayPersonSchema)otherObject;
        return
            APayPersonID == other.getAPayPersonID()
            && ShardingID.equals(other.getShardingID())
            && PolNo.equals(other.getPolNo())
            && PayCount == other.getPayCount()
            && GrpContNo.equals(other.getGrpContNo())
            && GrpPolNo.equals(other.getGrpPolNo())
            && ContNo.equals(other.getContNo())
            && ManageCom.equals(other.getManageCom())
            && AgentCom.equals(other.getAgentCom())
            && AgentType.equals(other.getAgentType())
            && RiskCode.equals(other.getRiskCode())
            && AgentCode.equals(other.getAgentCode())
            && AgentGroup.equals(other.getAgentGroup())
            && PayTypeFlag.equals(other.getPayTypeFlag())
            && AppntNo.equals(other.getAppntNo())
            && PayNo.equals(other.getPayNo())
            && PayAimClass.equals(other.getPayAimClass())
            && DutyCode.equals(other.getDutyCode())
            && PayPlanCode.equals(other.getPayPlanCode())
            && SumDuePayMoney == other.getSumDuePayMoney()
            && SumActuPayMoney == other.getSumActuPayMoney()
            && PayIntv == other.getPayIntv()
            && fDate.getString(PayDate).equals(other.getPayDate())
            && PayType.equals(other.getPayType())
            && fDate.getString(EnterAccDate).equals(other.getEnterAccDate())
            && fDate.getString(ConfDate).equals(other.getConfDate())
            && fDate.getString(LastPayToDate).equals(other.getLastPayToDate())
            && fDate.getString(CurPayToDate).equals(other.getCurPayToDate())
            && InInsuAccState.equals(other.getInInsuAccState())
            && ApproveCode.equals(other.getApproveCode())
            && fDate.getString(ApproveDate).equals(other.getApproveDate())
            && ApproveTime.equals(other.getApproveTime())
            && SerialNo.equals(other.getSerialNo())
            && Operator.equals(other.getOperator())
            && fDate.getString(MakeDate).equals(other.getMakeDate())
            && MakeTime.equals(other.getMakeTime())
            && GetNoticeNo.equals(other.getGetNoticeNo())
            && fDate.getString(ModifyDate).equals(other.getModifyDate())
            && ModifyTime.equals(other.getModifyTime())
            && EdorNo.equals(other.getEdorNo())
            && MainPolYear == other.getMainPolYear()
            && BackUpSerialNo.equals(other.getBackUpSerialNo())
            && BackUpReason.equals(other.getBackUpReason());
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("APayPersonID") ) {
            return 0;
        }
        if( strFieldName.equals("ShardingID") ) {
            return 1;
        }
        if( strFieldName.equals("PolNo") ) {
            return 2;
        }
        if( strFieldName.equals("PayCount") ) {
            return 3;
        }
        if( strFieldName.equals("GrpContNo") ) {
            return 4;
        }
        if( strFieldName.equals("GrpPolNo") ) {
            return 5;
        }
        if( strFieldName.equals("ContNo") ) {
            return 6;
        }
        if( strFieldName.equals("ManageCom") ) {
            return 7;
        }
        if( strFieldName.equals("AgentCom") ) {
            return 8;
        }
        if( strFieldName.equals("AgentType") ) {
            return 9;
        }
        if( strFieldName.equals("RiskCode") ) {
            return 10;
        }
        if( strFieldName.equals("AgentCode") ) {
            return 11;
        }
        if( strFieldName.equals("AgentGroup") ) {
            return 12;
        }
        if( strFieldName.equals("PayTypeFlag") ) {
            return 13;
        }
        if( strFieldName.equals("AppntNo") ) {
            return 14;
        }
        if( strFieldName.equals("PayNo") ) {
            return 15;
        }
        if( strFieldName.equals("PayAimClass") ) {
            return 16;
        }
        if( strFieldName.equals("DutyCode") ) {
            return 17;
        }
        if( strFieldName.equals("PayPlanCode") ) {
            return 18;
        }
        if( strFieldName.equals("SumDuePayMoney") ) {
            return 19;
        }
        if( strFieldName.equals("SumActuPayMoney") ) {
            return 20;
        }
        if( strFieldName.equals("PayIntv") ) {
            return 21;
        }
        if( strFieldName.equals("PayDate") ) {
            return 22;
        }
        if( strFieldName.equals("PayType") ) {
            return 23;
        }
        if( strFieldName.equals("EnterAccDate") ) {
            return 24;
        }
        if( strFieldName.equals("ConfDate") ) {
            return 25;
        }
        if( strFieldName.equals("LastPayToDate") ) {
            return 26;
        }
        if( strFieldName.equals("CurPayToDate") ) {
            return 27;
        }
        if( strFieldName.equals("InInsuAccState") ) {
            return 28;
        }
        if( strFieldName.equals("ApproveCode") ) {
            return 29;
        }
        if( strFieldName.equals("ApproveDate") ) {
            return 30;
        }
        if( strFieldName.equals("ApproveTime") ) {
            return 31;
        }
        if( strFieldName.equals("SerialNo") ) {
            return 32;
        }
        if( strFieldName.equals("Operator") ) {
            return 33;
        }
        if( strFieldName.equals("MakeDate") ) {
            return 34;
        }
        if( strFieldName.equals("MakeTime") ) {
            return 35;
        }
        if( strFieldName.equals("GetNoticeNo") ) {
            return 36;
        }
        if( strFieldName.equals("ModifyDate") ) {
            return 37;
        }
        if( strFieldName.equals("ModifyTime") ) {
            return 38;
        }
        if( strFieldName.equals("EdorNo") ) {
            return 39;
        }
        if( strFieldName.equals("MainPolYear") ) {
            return 40;
        }
        if( strFieldName.equals("BackUpSerialNo") ) {
            return 41;
        }
        if( strFieldName.equals("BackUpReason") ) {
            return 42;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "APayPersonID";
                break;
            case 1:
                strFieldName = "ShardingID";
                break;
            case 2:
                strFieldName = "PolNo";
                break;
            case 3:
                strFieldName = "PayCount";
                break;
            case 4:
                strFieldName = "GrpContNo";
                break;
            case 5:
                strFieldName = "GrpPolNo";
                break;
            case 6:
                strFieldName = "ContNo";
                break;
            case 7:
                strFieldName = "ManageCom";
                break;
            case 8:
                strFieldName = "AgentCom";
                break;
            case 9:
                strFieldName = "AgentType";
                break;
            case 10:
                strFieldName = "RiskCode";
                break;
            case 11:
                strFieldName = "AgentCode";
                break;
            case 12:
                strFieldName = "AgentGroup";
                break;
            case 13:
                strFieldName = "PayTypeFlag";
                break;
            case 14:
                strFieldName = "AppntNo";
                break;
            case 15:
                strFieldName = "PayNo";
                break;
            case 16:
                strFieldName = "PayAimClass";
                break;
            case 17:
                strFieldName = "DutyCode";
                break;
            case 18:
                strFieldName = "PayPlanCode";
                break;
            case 19:
                strFieldName = "SumDuePayMoney";
                break;
            case 20:
                strFieldName = "SumActuPayMoney";
                break;
            case 21:
                strFieldName = "PayIntv";
                break;
            case 22:
                strFieldName = "PayDate";
                break;
            case 23:
                strFieldName = "PayType";
                break;
            case 24:
                strFieldName = "EnterAccDate";
                break;
            case 25:
                strFieldName = "ConfDate";
                break;
            case 26:
                strFieldName = "LastPayToDate";
                break;
            case 27:
                strFieldName = "CurPayToDate";
                break;
            case 28:
                strFieldName = "InInsuAccState";
                break;
            case 29:
                strFieldName = "ApproveCode";
                break;
            case 30:
                strFieldName = "ApproveDate";
                break;
            case 31:
                strFieldName = "ApproveTime";
                break;
            case 32:
                strFieldName = "SerialNo";
                break;
            case 33:
                strFieldName = "Operator";
                break;
            case 34:
                strFieldName = "MakeDate";
                break;
            case 35:
                strFieldName = "MakeTime";
                break;
            case 36:
                strFieldName = "GetNoticeNo";
                break;
            case 37:
                strFieldName = "ModifyDate";
                break;
            case 38:
                strFieldName = "ModifyTime";
                break;
            case 39:
                strFieldName = "EdorNo";
                break;
            case 40:
                strFieldName = "MainPolYear";
                break;
            case 41:
                strFieldName = "BackUpSerialNo";
                break;
            case 42:
                strFieldName = "BackUpReason";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "APAYPERSONID":
                return Schema.TYPE_LONG;
            case "SHARDINGID":
                return Schema.TYPE_STRING;
            case "POLNO":
                return Schema.TYPE_STRING;
            case "PAYCOUNT":
                return Schema.TYPE_INT;
            case "GRPCONTNO":
                return Schema.TYPE_STRING;
            case "GRPPOLNO":
                return Schema.TYPE_STRING;
            case "CONTNO":
                return Schema.TYPE_STRING;
            case "MANAGECOM":
                return Schema.TYPE_STRING;
            case "AGENTCOM":
                return Schema.TYPE_STRING;
            case "AGENTTYPE":
                return Schema.TYPE_STRING;
            case "RISKCODE":
                return Schema.TYPE_STRING;
            case "AGENTCODE":
                return Schema.TYPE_STRING;
            case "AGENTGROUP":
                return Schema.TYPE_STRING;
            case "PAYTYPEFLAG":
                return Schema.TYPE_STRING;
            case "APPNTNO":
                return Schema.TYPE_STRING;
            case "PAYNO":
                return Schema.TYPE_STRING;
            case "PAYAIMCLASS":
                return Schema.TYPE_STRING;
            case "DUTYCODE":
                return Schema.TYPE_STRING;
            case "PAYPLANCODE":
                return Schema.TYPE_STRING;
            case "SUMDUEPAYMONEY":
                return Schema.TYPE_DOUBLE;
            case "SUMACTUPAYMONEY":
                return Schema.TYPE_DOUBLE;
            case "PAYINTV":
                return Schema.TYPE_INT;
            case "PAYDATE":
                return Schema.TYPE_DATE;
            case "PAYTYPE":
                return Schema.TYPE_STRING;
            case "ENTERACCDATE":
                return Schema.TYPE_DATE;
            case "CONFDATE":
                return Schema.TYPE_DATE;
            case "LASTPAYTODATE":
                return Schema.TYPE_DATE;
            case "CURPAYTODATE":
                return Schema.TYPE_DATE;
            case "ININSUACCSTATE":
                return Schema.TYPE_STRING;
            case "APPROVECODE":
                return Schema.TYPE_STRING;
            case "APPROVEDATE":
                return Schema.TYPE_DATE;
            case "APPROVETIME":
                return Schema.TYPE_STRING;
            case "SERIALNO":
                return Schema.TYPE_STRING;
            case "OPERATOR":
                return Schema.TYPE_STRING;
            case "MAKEDATE":
                return Schema.TYPE_DATE;
            case "MAKETIME":
                return Schema.TYPE_STRING;
            case "GETNOTICENO":
                return Schema.TYPE_STRING;
            case "MODIFYDATE":
                return Schema.TYPE_DATE;
            case "MODIFYTIME":
                return Schema.TYPE_STRING;
            case "EDORNO":
                return Schema.TYPE_STRING;
            case "MAINPOLYEAR":
                return Schema.TYPE_INT;
            case "BACKUPSERIALNO":
                return Schema.TYPE_STRING;
            case "BACKUPREASON":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_LONG;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_INT;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_STRING;
            case 6:
                return Schema.TYPE_STRING;
            case 7:
                return Schema.TYPE_STRING;
            case 8:
                return Schema.TYPE_STRING;
            case 9:
                return Schema.TYPE_STRING;
            case 10:
                return Schema.TYPE_STRING;
            case 11:
                return Schema.TYPE_STRING;
            case 12:
                return Schema.TYPE_STRING;
            case 13:
                return Schema.TYPE_STRING;
            case 14:
                return Schema.TYPE_STRING;
            case 15:
                return Schema.TYPE_STRING;
            case 16:
                return Schema.TYPE_STRING;
            case 17:
                return Schema.TYPE_STRING;
            case 18:
                return Schema.TYPE_STRING;
            case 19:
                return Schema.TYPE_DOUBLE;
            case 20:
                return Schema.TYPE_DOUBLE;
            case 21:
                return Schema.TYPE_INT;
            case 22:
                return Schema.TYPE_DATE;
            case 23:
                return Schema.TYPE_STRING;
            case 24:
                return Schema.TYPE_DATE;
            case 25:
                return Schema.TYPE_DATE;
            case 26:
                return Schema.TYPE_DATE;
            case 27:
                return Schema.TYPE_DATE;
            case 28:
                return Schema.TYPE_STRING;
            case 29:
                return Schema.TYPE_STRING;
            case 30:
                return Schema.TYPE_DATE;
            case 31:
                return Schema.TYPE_STRING;
            case 32:
                return Schema.TYPE_STRING;
            case 33:
                return Schema.TYPE_STRING;
            case 34:
                return Schema.TYPE_DATE;
            case 35:
                return Schema.TYPE_STRING;
            case 36:
                return Schema.TYPE_STRING;
            case 37:
                return Schema.TYPE_DATE;
            case 38:
                return Schema.TYPE_STRING;
            case 39:
                return Schema.TYPE_STRING;
            case 40:
                return Schema.TYPE_INT;
            case 41:
                return Schema.TYPE_STRING;
            case 42:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
}
