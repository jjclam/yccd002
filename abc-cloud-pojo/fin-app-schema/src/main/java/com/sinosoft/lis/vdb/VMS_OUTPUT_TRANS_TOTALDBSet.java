/**
 * Copyright (c) 2017 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.vdb;

import com.sinosoft.lis.vschema.VMS_OUTPUT_TRANS_TOTALSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.DBConnPool;
import com.sinosoft.utility.DBOper;
import com.sinosoft.utility.ExceptionUtils;

import java.sql.Connection;
import java.sql.PreparedStatement;

/**
 * <p>ClassName: VMS_OUTPUT_TRANS_TOTALDBSet </p>
 * <p>Description: DB层多记录数据库操作类文件 </p>
 * <p>Copyright: Copyright (c) 2017</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2017-10-21
 */
public class VMS_OUTPUT_TRANS_TOTALDBSet extends VMS_OUTPUT_TRANS_TOTALSet {
    // @Field
    private Connection con;
    private DBOper db;
    /**
    * flag = true: 传入Connection
    * flag = false: 不传入Connection
    **/
    private boolean mflag = false;

    // @Constructor
    public VMS_OUTPUT_TRANS_TOTALDBSet(Connection tConnection) {
        con = tConnection;
        db = new DBOper(con,"VMS_OUTPUT_TRANS_TOTAL");
        mflag = true;
    }

    public VMS_OUTPUT_TRANS_TOTALDBSet() {
        db = new DBOper( "VMS_OUTPUT_TRANS_TOTAL" );
    }
    // @Method
    public boolean deleteSQL() {
        if (db.deleteSQL(this)) {
            return true;
        } else {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "VMS_OUTPUT_TRANS_TOTALDBSet";
            tError.functionName = "deleteSQL";
            tError.errorMessage = "操作失败!";
            this.mErrors .addOneError(tError);
            return false;
        }
    }

    /**
     * 删除操作
     * 删除条件：主键
     * @return boolean
     */
    public boolean delete() {
        PreparedStatement pstmt = null;

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try {
            int tCount = this.size();
            pstmt = con.prepareStatement("DELETE FROM VMS_OUTPUT_TRANS_TOTAL WHERE  1=1  AND BUSINESS_ID = ?");
            for (int i = 1; i <= tCount; i++) {
            if(this.get(i).getBUSINESS_ID() == null || this.get(i).getBUSINESS_ID().equals("null")) {
            	pstmt.setString(1,null);
            } else {
            	pstmt.setString(1, this.get(i).getBUSINESS_ID());
            }
                pstmt.addBatch();
            }
            pstmt.executeBatch();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            ex.printStackTrace();
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "VMS_OUTPUT_TRANS_TOTALDBSet";
            tError.functionName = "delete()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            try {
                pstmt.close();
            } catch (Exception e){
                e.printStackTrace();
            }

            if (!mflag) {
                try {
                    con.close();
                } catch (Exception e){e.printStackTrace();}
            }

            return false;
        }

        if( !mflag ) {
            try {
                con.close();
            } catch (Exception e){e.printStackTrace();}
        }

        return true;
    }

    /**
     * 更新操作
     * 更新条件：主键
     * @return boolean
     */
    public boolean update() {
        PreparedStatement pstmt = null;

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try {
            int tCount = this.size();
            pstmt = con.prepareStatement("UPDATE VMS_OUTPUT_TRANS_TOTAL SET  BUSINESS_ID = ? , GRPCONTNO = ? , GRPPOLNO = ? , CHERNUM = ? , POLNO = ? , TTMPRCNO = ? , COWNNUM = ? , ORIGCURR = ? , TAX_RATE = ? , ORIG_AMT = ? , ORIG_INCOME = ? , ORIG_TAX_AMT = ? , AMT_CNY = ? , INCOME_CNY = ? , TAX_AMT_CNY = ? , DC_FLAG = ? , HESITAGE = ? , SERIALNO = ? , TRA_DT = ? , TRA_TYP = ? , BATCTRCDE = ? , TRA_BRA = ? , INVTYP = ? , FEETYP = ? , INCOMETYP = ? , BILLFREQ = ? , POLYEAR = ? , HISSDTE = ? , DSOURCE = ? , GL_CD_ID = ? , GL_CD_GRD = ? , PRODUCT_COD = ? , INVONO = ? , OTHERNO = ? , INS_COD = ? , INSTFROM = ? , INSTTO = ? , OCCDATE = ? , PREMTERM = ? , UPLOADFLAG = ? , BUSSNO = ? , STANDBYFLAG1 = ? , STANDBYFLAG2 = ? , FPFLAG = ? , MAKEDATE = ? , MAKETIME = ? , MODIFYDATE = ? , MODIFYTIME = ? , PRODUCT_NAME = ? , SALECHNL = ? WHERE  1=1  AND BUSINESS_ID = ?");
            for (int i = 1; i <= tCount; i++) {
            if(this.get(i).getBUSINESS_ID() == null || this.get(i).getBUSINESS_ID().equals("null")) {
            	pstmt.setString(1,null);
            } else {
            	pstmt.setString(1, this.get(i).getBUSINESS_ID());
            }
            if(this.get(i).getGRPCONTNO() == null || this.get(i).getGRPCONTNO().equals("null")) {
            	pstmt.setString(2,null);
            } else {
            	pstmt.setString(2, this.get(i).getGRPCONTNO());
            }
            if(this.get(i).getGRPPOLNO() == null || this.get(i).getGRPPOLNO().equals("null")) {
            	pstmt.setString(3,null);
            } else {
            	pstmt.setString(3, this.get(i).getGRPPOLNO());
            }
            if(this.get(i).getCHERNUM() == null || this.get(i).getCHERNUM().equals("null")) {
            	pstmt.setString(4,null);
            } else {
            	pstmt.setString(4, this.get(i).getCHERNUM());
            }
            if(this.get(i).getPOLNO() == null || this.get(i).getPOLNO().equals("null")) {
            	pstmt.setString(5,null);
            } else {
            	pstmt.setString(5, this.get(i).getPOLNO());
            }
            if(this.get(i).getTTMPRCNO() == null || this.get(i).getTTMPRCNO().equals("null")) {
            	pstmt.setString(6,null);
            } else {
            	pstmt.setString(6, this.get(i).getTTMPRCNO());
            }
            if(this.get(i).getCOWNNUM() == null || this.get(i).getCOWNNUM().equals("null")) {
            	pstmt.setString(7,null);
            } else {
            	pstmt.setString(7, this.get(i).getCOWNNUM());
            }
            if(this.get(i).getORIGCURR() == null || this.get(i).getORIGCURR().equals("null")) {
            	pstmt.setString(8,null);
            } else {
            	pstmt.setString(8, this.get(i).getORIGCURR());
            }
            pstmt.setDouble(9, this.get(i).getTAX_RATE());
            pstmt.setDouble(10, this.get(i).getORIG_AMT());
            pstmt.setDouble(11, this.get(i).getORIG_INCOME());
            pstmt.setDouble(12, this.get(i).getORIG_TAX_AMT());
            pstmt.setDouble(13, this.get(i).getAMT_CNY());
            pstmt.setDouble(14, this.get(i).getINCOME_CNY());
            pstmt.setDouble(15, this.get(i).getTAX_AMT_CNY());
            if(this.get(i).getDC_FLAG() == null || this.get(i).getDC_FLAG().equals("null")) {
            	pstmt.setString(16,null);
            } else {
            	pstmt.setString(16, this.get(i).getDC_FLAG());
            }
            if(this.get(i).getHESITAGE() == null || this.get(i).getHESITAGE().equals("null")) {
            	pstmt.setString(17,null);
            } else {
            	pstmt.setString(17, this.get(i).getHESITAGE());
            }
            if(this.get(i).getSERIALNO() == null || this.get(i).getSERIALNO().equals("null")) {
            	pstmt.setString(18,null);
            } else {
            	pstmt.setString(18, this.get(i).getSERIALNO());
            }
            if(this.get(i).getTRA_DT() == null || this.get(i).getTRA_DT().equals("null")) {
            	pstmt.setString(19,null);
            } else {
            	pstmt.setString(19, this.get(i).getTRA_DT());
            }
            if(this.get(i).getTRA_TYP() == null || this.get(i).getTRA_TYP().equals("null")) {
            	pstmt.setString(20,null);
            } else {
            	pstmt.setString(20, this.get(i).getTRA_TYP());
            }
            if(this.get(i).getBATCTRCDE() == null || this.get(i).getBATCTRCDE().equals("null")) {
            	pstmt.setString(21,null);
            } else {
            	pstmt.setString(21, this.get(i).getBATCTRCDE());
            }
            if(this.get(i).getTRA_BRA() == null || this.get(i).getTRA_BRA().equals("null")) {
            	pstmt.setString(22,null);
            } else {
            	pstmt.setString(22, this.get(i).getTRA_BRA());
            }
            if(this.get(i).getINVTYP() == null || this.get(i).getINVTYP().equals("null")) {
            	pstmt.setString(23,null);
            } else {
            	pstmt.setString(23, this.get(i).getINVTYP());
            }
            if(this.get(i).getFEETYP() == null || this.get(i).getFEETYP().equals("null")) {
            	pstmt.setString(24,null);
            } else {
            	pstmt.setString(24, this.get(i).getFEETYP());
            }
            if(this.get(i).getINCOMETYP() == null || this.get(i).getINCOMETYP().equals("null")) {
            	pstmt.setString(25,null);
            } else {
            	pstmt.setString(25, this.get(i).getINCOMETYP());
            }
            if(this.get(i).getBILLFREQ() == null || this.get(i).getBILLFREQ().equals("null")) {
            	pstmt.setString(26,null);
            } else {
            	pstmt.setString(26, this.get(i).getBILLFREQ());
            }
            if(this.get(i).getPOLYEAR() == null || this.get(i).getPOLYEAR().equals("null")) {
            	pstmt.setString(27,null);
            } else {
            	pstmt.setString(27, this.get(i).getPOLYEAR());
            }
            if(this.get(i).getHISSDTE() == null || this.get(i).getHISSDTE().equals("null")) {
            	pstmt.setString(28,null);
            } else {
            	pstmt.setString(28, this.get(i).getHISSDTE());
            }
            if(this.get(i).getDSOURCE() == null || this.get(i).getDSOURCE().equals("null")) {
            	pstmt.setString(29,null);
            } else {
            	pstmt.setString(29, this.get(i).getDSOURCE());
            }
            if(this.get(i).getGL_CD_ID() == null || this.get(i).getGL_CD_ID().equals("null")) {
            	pstmt.setString(30,null);
            } else {
            	pstmt.setString(30, this.get(i).getGL_CD_ID());
            }
            if(this.get(i).getGL_CD_GRD() == null || this.get(i).getGL_CD_GRD().equals("null")) {
            	pstmt.setString(31,null);
            } else {
            	pstmt.setString(31, this.get(i).getGL_CD_GRD());
            }
            if(this.get(i).getPRODUCT_COD() == null || this.get(i).getPRODUCT_COD().equals("null")) {
            	pstmt.setString(32,null);
            } else {
            	pstmt.setString(32, this.get(i).getPRODUCT_COD());
            }
            if(this.get(i).getINVONO() == null || this.get(i).getINVONO().equals("null")) {
            	pstmt.setString(33,null);
            } else {
            	pstmt.setString(33, this.get(i).getINVONO());
            }
            if(this.get(i).getOTHERNO() == null || this.get(i).getOTHERNO().equals("null")) {
            	pstmt.setString(34,null);
            } else {
            	pstmt.setString(34, this.get(i).getOTHERNO());
            }
            if(this.get(i).getINS_COD() == null || this.get(i).getINS_COD().equals("null")) {
            	pstmt.setString(35,null);
            } else {
            	pstmt.setString(35, this.get(i).getINS_COD());
            }
            if(this.get(i).getINSTFROM() == null || this.get(i).getINSTFROM().equals("null")) {
            	pstmt.setString(36,null);
            } else {
            	pstmt.setString(36, this.get(i).getINSTFROM());
            }
            if(this.get(i).getINSTTO() == null || this.get(i).getINSTTO().equals("null")) {
            	pstmt.setString(37,null);
            } else {
            	pstmt.setString(37, this.get(i).getINSTTO());
            }
            if(this.get(i).getOCCDATE() == null || this.get(i).getOCCDATE().equals("null")) {
            	pstmt.setString(38,null);
            } else {
            	pstmt.setString(38, this.get(i).getOCCDATE());
            }
            if(this.get(i).getPREMTERM() == null || this.get(i).getPREMTERM().equals("null")) {
            	pstmt.setString(39,null);
            } else {
            	pstmt.setString(39, this.get(i).getPREMTERM());
            }
            if(this.get(i).getUPLOADFLAG() == null || this.get(i).getUPLOADFLAG().equals("null")) {
            	pstmt.setString(40,null);
            } else {
            	pstmt.setString(40, this.get(i).getUPLOADFLAG());
            }
            if(this.get(i).getBUSSNO() == null || this.get(i).getBUSSNO().equals("null")) {
            	pstmt.setString(41,null);
            } else {
            	pstmt.setString(41, this.get(i).getBUSSNO());
            }
            if(this.get(i).getSTANDBYFLAG1() == null || this.get(i).getSTANDBYFLAG1().equals("null")) {
            	pstmt.setString(42,null);
            } else {
            	pstmt.setString(42, this.get(i).getSTANDBYFLAG1());
            }
            if(this.get(i).getSTANDBYFLAG2() == null || this.get(i).getSTANDBYFLAG2().equals("null")) {
            	pstmt.setString(43,null);
            } else {
            	pstmt.setString(43, this.get(i).getSTANDBYFLAG2());
            }
            if(this.get(i).getFPFLAG() == null || this.get(i).getFPFLAG().equals("null")) {
            	pstmt.setString(44,null);
            } else {
            	pstmt.setString(44, this.get(i).getFPFLAG());
            }
            if(this.get(i).getMAKEDATE() == null || this.get(i).getMAKEDATE().equals("null")) {
            	pstmt.setString(45,null);
            } else {
            	pstmt.setString(45, this.get(i).getMAKEDATE());
            }
            if(this.get(i).getMAKETIME() == null || this.get(i).getMAKETIME().equals("null")) {
            	pstmt.setString(46,null);
            } else {
            	pstmt.setString(46, this.get(i).getMAKETIME());
            }
            if(this.get(i).getMODIFYDATE() == null || this.get(i).getMODIFYDATE().equals("null")) {
            	pstmt.setString(47,null);
            } else {
            	pstmt.setString(47, this.get(i).getMODIFYDATE());
            }
            if(this.get(i).getMODIFYTIME() == null || this.get(i).getMODIFYTIME().equals("null")) {
            	pstmt.setString(48,null);
            } else {
            	pstmt.setString(48, this.get(i).getMODIFYTIME());
            }
            if(this.get(i).getPRODUCT_NAME() == null || this.get(i).getPRODUCT_NAME().equals("null")) {
            	pstmt.setString(49,null);
            } else {
            	pstmt.setString(49, this.get(i).getPRODUCT_NAME());
            }
            if(this.get(i).getSALECHNL() == null || this.get(i).getSALECHNL().equals("null")) {
            	pstmt.setString(50,null);
            } else {
            	pstmt.setString(50, this.get(i).getSALECHNL());
            }
            // set where condition
            if(this.get(i).getBUSINESS_ID() == null || this.get(i).getBUSINESS_ID().equals("null")) {
            	pstmt.setString(51,null);
            } else {
            	pstmt.setString(51, this.get(i).getBUSINESS_ID());
            }
                pstmt.addBatch();
            }
            pstmt.executeBatch();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            ex.printStackTrace();
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "VMS_OUTPUT_TRANS_TOTALDBSet";
            tError.functionName = "update()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            try {
                pstmt.close();
            } catch (Exception e){e.printStackTrace();}

            if( !mflag ) {
                try {
                    con.close();
                } catch (Exception e){e.printStackTrace();}
            }

            return false;
        }

        if( !mflag ) {
            try {
                con.close();
            } catch (Exception e){e.printStackTrace();}
        }

        return true;
    }

    /**
     * 新增操作
     * @return boolean
     */
    public boolean insert() {
        PreparedStatement pstmt = null;

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try
        {
            int tCount = this.size();
            pstmt = con.prepareStatement("INSERT INTO VMS_OUTPUT_TRANS_TOTAL VALUES( ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ?)");
            for (int i = 1; i <= tCount; i++) {
            if(this.get(i).getBUSINESS_ID() == null || this.get(i).getBUSINESS_ID().equals("null")) {
            	pstmt.setString(1,null);
            } else {
            	pstmt.setString(1, this.get(i).getBUSINESS_ID());
            }
            if(this.get(i).getGRPCONTNO() == null || this.get(i).getGRPCONTNO().equals("null")) {
            	pstmt.setString(2,null);
            } else {
            	pstmt.setString(2, this.get(i).getGRPCONTNO());
            }
            if(this.get(i).getGRPPOLNO() == null || this.get(i).getGRPPOLNO().equals("null")) {
            	pstmt.setString(3,null);
            } else {
            	pstmt.setString(3, this.get(i).getGRPPOLNO());
            }
            if(this.get(i).getCHERNUM() == null || this.get(i).getCHERNUM().equals("null")) {
            	pstmt.setString(4,null);
            } else {
            	pstmt.setString(4, this.get(i).getCHERNUM());
            }
            if(this.get(i).getPOLNO() == null || this.get(i).getPOLNO().equals("null")) {
            	pstmt.setString(5,null);
            } else {
            	pstmt.setString(5, this.get(i).getPOLNO());
            }
            if(this.get(i).getTTMPRCNO() == null || this.get(i).getTTMPRCNO().equals("null")) {
            	pstmt.setString(6,null);
            } else {
            	pstmt.setString(6, this.get(i).getTTMPRCNO());
            }
            if(this.get(i).getCOWNNUM() == null || this.get(i).getCOWNNUM().equals("null")) {
            	pstmt.setString(7,null);
            } else {
            	pstmt.setString(7, this.get(i).getCOWNNUM());
            }
            if(this.get(i).getORIGCURR() == null || this.get(i).getORIGCURR().equals("null")) {
            	pstmt.setString(8,null);
            } else {
            	pstmt.setString(8, this.get(i).getORIGCURR());
            }
            pstmt.setDouble(9, this.get(i).getTAX_RATE());
            pstmt.setDouble(10, this.get(i).getORIG_AMT());
            pstmt.setDouble(11, this.get(i).getORIG_INCOME());
            pstmt.setDouble(12, this.get(i).getORIG_TAX_AMT());
            pstmt.setDouble(13, this.get(i).getAMT_CNY());
            pstmt.setDouble(14, this.get(i).getINCOME_CNY());
            pstmt.setDouble(15, this.get(i).getTAX_AMT_CNY());
            if(this.get(i).getDC_FLAG() == null || this.get(i).getDC_FLAG().equals("null")) {
            	pstmt.setString(16,null);
            } else {
            	pstmt.setString(16, this.get(i).getDC_FLAG());
            }
            if(this.get(i).getHESITAGE() == null || this.get(i).getHESITAGE().equals("null")) {
            	pstmt.setString(17,null);
            } else {
            	pstmt.setString(17, this.get(i).getHESITAGE());
            }
            if(this.get(i).getSERIALNO() == null || this.get(i).getSERIALNO().equals("null")) {
            	pstmt.setString(18,null);
            } else {
            	pstmt.setString(18, this.get(i).getSERIALNO());
            }
            if(this.get(i).getTRA_DT() == null || this.get(i).getTRA_DT().equals("null")) {
            	pstmt.setString(19,null);
            } else {
            	pstmt.setString(19, this.get(i).getTRA_DT());
            }
            if(this.get(i).getTRA_TYP() == null || this.get(i).getTRA_TYP().equals("null")) {
            	pstmt.setString(20,null);
            } else {
            	pstmt.setString(20, this.get(i).getTRA_TYP());
            }
            if(this.get(i).getBATCTRCDE() == null || this.get(i).getBATCTRCDE().equals("null")) {
            	pstmt.setString(21,null);
            } else {
            	pstmt.setString(21, this.get(i).getBATCTRCDE());
            }
            if(this.get(i).getTRA_BRA() == null || this.get(i).getTRA_BRA().equals("null")) {
            	pstmt.setString(22,null);
            } else {
            	pstmt.setString(22, this.get(i).getTRA_BRA());
            }
            if(this.get(i).getINVTYP() == null || this.get(i).getINVTYP().equals("null")) {
            	pstmt.setString(23,null);
            } else {
            	pstmt.setString(23, this.get(i).getINVTYP());
            }
            if(this.get(i).getFEETYP() == null || this.get(i).getFEETYP().equals("null")) {
            	pstmt.setString(24,null);
            } else {
            	pstmt.setString(24, this.get(i).getFEETYP());
            }
            if(this.get(i).getINCOMETYP() == null || this.get(i).getINCOMETYP().equals("null")) {
            	pstmt.setString(25,null);
            } else {
            	pstmt.setString(25, this.get(i).getINCOMETYP());
            }
            if(this.get(i).getBILLFREQ() == null || this.get(i).getBILLFREQ().equals("null")) {
            	pstmt.setString(26,null);
            } else {
            	pstmt.setString(26, this.get(i).getBILLFREQ());
            }
            if(this.get(i).getPOLYEAR() == null || this.get(i).getPOLYEAR().equals("null")) {
            	pstmt.setString(27,null);
            } else {
            	pstmt.setString(27, this.get(i).getPOLYEAR());
            }
            if(this.get(i).getHISSDTE() == null || this.get(i).getHISSDTE().equals("null")) {
            	pstmt.setString(28,null);
            } else {
            	pstmt.setString(28, this.get(i).getHISSDTE());
            }
            if(this.get(i).getDSOURCE() == null || this.get(i).getDSOURCE().equals("null")) {
            	pstmt.setString(29,null);
            } else {
            	pstmt.setString(29, this.get(i).getDSOURCE());
            }
            if(this.get(i).getGL_CD_ID() == null || this.get(i).getGL_CD_ID().equals("null")) {
            	pstmt.setString(30,null);
            } else {
            	pstmt.setString(30, this.get(i).getGL_CD_ID());
            }
            if(this.get(i).getGL_CD_GRD() == null || this.get(i).getGL_CD_GRD().equals("null")) {
            	pstmt.setString(31,null);
            } else {
            	pstmt.setString(31, this.get(i).getGL_CD_GRD());
            }
            if(this.get(i).getPRODUCT_COD() == null || this.get(i).getPRODUCT_COD().equals("null")) {
            	pstmt.setString(32,null);
            } else {
            	pstmt.setString(32, this.get(i).getPRODUCT_COD());
            }
            if(this.get(i).getINVONO() == null || this.get(i).getINVONO().equals("null")) {
            	pstmt.setString(33,null);
            } else {
            	pstmt.setString(33, this.get(i).getINVONO());
            }
            if(this.get(i).getOTHERNO() == null || this.get(i).getOTHERNO().equals("null")) {
            	pstmt.setString(34,null);
            } else {
            	pstmt.setString(34, this.get(i).getOTHERNO());
            }
            if(this.get(i).getINS_COD() == null || this.get(i).getINS_COD().equals("null")) {
            	pstmt.setString(35,null);
            } else {
            	pstmt.setString(35, this.get(i).getINS_COD());
            }
            if(this.get(i).getINSTFROM() == null || this.get(i).getINSTFROM().equals("null")) {
            	pstmt.setString(36,null);
            } else {
            	pstmt.setString(36, this.get(i).getINSTFROM());
            }
            if(this.get(i).getINSTTO() == null || this.get(i).getINSTTO().equals("null")) {
            	pstmt.setString(37,null);
            } else {
            	pstmt.setString(37, this.get(i).getINSTTO());
            }
            if(this.get(i).getOCCDATE() == null || this.get(i).getOCCDATE().equals("null")) {
            	pstmt.setString(38,null);
            } else {
            	pstmt.setString(38, this.get(i).getOCCDATE());
            }
            if(this.get(i).getPREMTERM() == null || this.get(i).getPREMTERM().equals("null")) {
            	pstmt.setString(39,null);
            } else {
            	pstmt.setString(39, this.get(i).getPREMTERM());
            }
            if(this.get(i).getUPLOADFLAG() == null || this.get(i).getUPLOADFLAG().equals("null")) {
            	pstmt.setString(40,null);
            } else {
            	pstmt.setString(40, this.get(i).getUPLOADFLAG());
            }
            if(this.get(i).getBUSSNO() == null || this.get(i).getBUSSNO().equals("null")) {
            	pstmt.setString(41,null);
            } else {
            	pstmt.setString(41, this.get(i).getBUSSNO());
            }
            if(this.get(i).getSTANDBYFLAG1() == null || this.get(i).getSTANDBYFLAG1().equals("null")) {
            	pstmt.setString(42,null);
            } else {
            	pstmt.setString(42, this.get(i).getSTANDBYFLAG1());
            }
            if(this.get(i).getSTANDBYFLAG2() == null || this.get(i).getSTANDBYFLAG2().equals("null")) {
            	pstmt.setString(43,null);
            } else {
            	pstmt.setString(43, this.get(i).getSTANDBYFLAG2());
            }
            if(this.get(i).getFPFLAG() == null || this.get(i).getFPFLAG().equals("null")) {
            	pstmt.setString(44,null);
            } else {
            	pstmt.setString(44, this.get(i).getFPFLAG());
            }
            if(this.get(i).getMAKEDATE() == null || this.get(i).getMAKEDATE().equals("null")) {
            	pstmt.setString(45,null);
            } else {
            	pstmt.setString(45, this.get(i).getMAKEDATE());
            }
            if(this.get(i).getMAKETIME() == null || this.get(i).getMAKETIME().equals("null")) {
            	pstmt.setString(46,null);
            } else {
            	pstmt.setString(46, this.get(i).getMAKETIME());
            }
            if(this.get(i).getMODIFYDATE() == null || this.get(i).getMODIFYDATE().equals("null")) {
            	pstmt.setString(47,null);
            } else {
            	pstmt.setString(47, this.get(i).getMODIFYDATE());
            }
            if(this.get(i).getMODIFYTIME() == null || this.get(i).getMODIFYTIME().equals("null")) {
            	pstmt.setString(48,null);
            } else {
            	pstmt.setString(48, this.get(i).getMODIFYTIME());
            }
            if(this.get(i).getPRODUCT_NAME() == null || this.get(i).getPRODUCT_NAME().equals("null")) {
            	pstmt.setString(49,null);
            } else {
            	pstmt.setString(49, this.get(i).getPRODUCT_NAME());
            }
            if(this.get(i).getSALECHNL() == null || this.get(i).getSALECHNL().equals("null")) {
            	pstmt.setString(50,null);
            } else {
            	pstmt.setString(50, this.get(i).getSALECHNL());
            }
                pstmt.addBatch();
            }
            pstmt.executeBatch();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            ex.printStackTrace();
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "VMS_OUTPUT_TRANS_TOTALDBSet";
            tError.functionName = "insert()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            try {
                pstmt.close();
            } catch (Exception e){e.printStackTrace();}

            if( !mflag ) {
                try {
                    con.close();
                } catch (Exception e){e.printStackTrace();}
            }

            return false;
        }

        if( !mflag ) {
            try {
                con.close();
            } catch (Exception e){e.printStackTrace();}
        }

        return true;
    }
}
