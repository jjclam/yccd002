/**
 * Copyright (c) 2017 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import com.sinosoft.lis.db.LKTempFeeClassDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.*;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

/**
 * <p>ClassName: LKTempFeeClassSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2017</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2017-10-21
 */
public class LKTempFeeClassSchema implements Schema, Cloneable {
    // @Field
    /** Id */
    private String TempFeeClassID;
    /** Shardingid */
    private String ShardingID;
    /** 暂交费收据号码 */
    private String TempFeeNo;
    /** 交费方式 */
    private String PayMode;
    /** 票据号 */
    private String ChequeNo;
    /** 交费金额 */
    private double PayMoney;
    /** 投保人名称 */
    private String AppntName;
    /** 交费日期 */
    private Date PayDate;
    /** 确认日期 */
    private Date ConfDate;
    /** 复核日期 */
    private Date ApproveDate;
    /** 到帐日期 */
    private Date EnterAccDate;
    /** 是否核销标志 */
    private String ConfFlag;
    /** 流水号 */
    private String SerialNo;
    /** 交费机构 */
    private String ManageCom;
    /** 管理机构 */
    private String PolicyCom;
    /** 银行编码 */
    private String BankCode;
    /** 银行帐号 */
    private String BankAccNo;
    /** 银行帐户名 */
    private String AccName;
    /** 财务确认操作日期 */
    private Date ConfMakeDate;
    /** 财务确认操作时间 */
    private String ConfMakeTime;
    /** 支票日期 */
    private Date ChequeDate;
    /** 操作员 */
    private String Operator;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 最后一次修改日期 */
    private Date ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime;
    /** 收费银行编码 */
    private String InBankCode;
    /** 收费银行帐号 */
    private String InBankAccNo;
    /** 收费银行帐户名 */
    private String InAccName;
    /** 保单所属机构 */
    private String ContCom;
    /** 对应其它号码 */
    private String OtherNo;
    /** 其它号码类型 */
    private String OtherNoType;
    /** 收据类型 */
    private String TempFeeNoType;
    /** 证件类型 */
    private String IDType;
    /** 证件号码 */
    private String IDNo;

    public static final int FIELDNUM = 35;    // 数据库表的字段个数

    private static String[] PK;                // 主键

    private FDate fDate = new FDate();        // 处理日期

    public CErrors mErrors;            // 错误信息

    // @Constructor
    public LKTempFeeClassSchema() {
        mErrors = new CErrors();

        String[] pk = new String[1];
        pk[0] = "TempFeeClassID";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException {
        LKTempFeeClassSchema cloned = (LKTempFeeClassSchema)super.clone();
        cloned.fDate = (FDate) fDate.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK() {
        return PK;
    }

    public String getTempFeeClassID() {
        return TempFeeClassID;
    }
    public void setTempFeeClassID(String aTempFeeClassID) {
        TempFeeClassID = aTempFeeClassID;
    }
    public String getShardingID() {
        return ShardingID;
    }
    public void setShardingID(String aShardingID) {
        ShardingID = aShardingID;
    }
    public String getTempFeeNo() {
        return TempFeeNo;
    }
    public void setTempFeeNo(String aTempFeeNo) {
        TempFeeNo = aTempFeeNo;
    }
    public String getPayMode() {
        return PayMode;
    }
    public void setPayMode(String aPayMode) {
        PayMode = aPayMode;
    }
    public String getChequeNo() {
        return ChequeNo;
    }
    public void setChequeNo(String aChequeNo) {
        ChequeNo = aChequeNo;
    }
    public double getPayMoney() {
        return PayMoney;
    }
    public void setPayMoney(double aPayMoney) {
        PayMoney = aPayMoney;
    }
    public void setPayMoney(String aPayMoney) {
        if (aPayMoney != null && !aPayMoney.equals("")) {
            Double tDouble = new Double(aPayMoney);
            double d = tDouble.doubleValue();
            PayMoney = d;
        }
    }

    public String getAppntName() {
        return AppntName;
    }
    public void setAppntName(String aAppntName) {
        AppntName = aAppntName;
    }
    public String getPayDate() {
        if(PayDate != null) {
            return fDate.getString(PayDate);
        } else {
            return null;
        }
    }
    public void setPayDate(Date aPayDate) {
        PayDate = aPayDate;
    }
    public void setPayDate(String aPayDate) {
        if (aPayDate != null && !aPayDate.equals("")) {
            PayDate = fDate.getDate(aPayDate);
        } else
            PayDate = null;
    }

    public String getConfDate() {
        if(ConfDate != null) {
            return fDate.getString(ConfDate);
        } else {
            return null;
        }
    }
    public void setConfDate(Date aConfDate) {
        ConfDate = aConfDate;
    }
    public void setConfDate(String aConfDate) {
        if (aConfDate != null && !aConfDate.equals("")) {
            ConfDate = fDate.getDate(aConfDate);
        } else
            ConfDate = null;
    }

    public String getApproveDate() {
        if(ApproveDate != null) {
            return fDate.getString(ApproveDate);
        } else {
            return null;
        }
    }
    public void setApproveDate(Date aApproveDate) {
        ApproveDate = aApproveDate;
    }
    public void setApproveDate(String aApproveDate) {
        if (aApproveDate != null && !aApproveDate.equals("")) {
            ApproveDate = fDate.getDate(aApproveDate);
        } else
            ApproveDate = null;
    }

    public String getEnterAccDate() {
        if(EnterAccDate != null) {
            return fDate.getString(EnterAccDate);
        } else {
            return null;
        }
    }
    public void setEnterAccDate(Date aEnterAccDate) {
        EnterAccDate = aEnterAccDate;
    }
    public void setEnterAccDate(String aEnterAccDate) {
        if (aEnterAccDate != null && !aEnterAccDate.equals("")) {
            EnterAccDate = fDate.getDate(aEnterAccDate);
        } else
            EnterAccDate = null;
    }

    public String getConfFlag() {
        return ConfFlag;
    }
    public void setConfFlag(String aConfFlag) {
        ConfFlag = aConfFlag;
    }
    public String getSerialNo() {
        return SerialNo;
    }
    public void setSerialNo(String aSerialNo) {
        SerialNo = aSerialNo;
    }
    public String getManageCom() {
        return ManageCom;
    }
    public void setManageCom(String aManageCom) {
        ManageCom = aManageCom;
    }
    public String getPolicyCom() {
        return PolicyCom;
    }
    public void setPolicyCom(String aPolicyCom) {
        PolicyCom = aPolicyCom;
    }
    public String getBankCode() {
        return BankCode;
    }
    public void setBankCode(String aBankCode) {
        BankCode = aBankCode;
    }
    public String getBankAccNo() {
        return BankAccNo;
    }
    public void setBankAccNo(String aBankAccNo) {
        BankAccNo = aBankAccNo;
    }
    public String getAccName() {
        return AccName;
    }
    public void setAccName(String aAccName) {
        AccName = aAccName;
    }
    public String getConfMakeDate() {
        if(ConfMakeDate != null) {
            return fDate.getString(ConfMakeDate);
        } else {
            return null;
        }
    }
    public void setConfMakeDate(Date aConfMakeDate) {
        ConfMakeDate = aConfMakeDate;
    }
    public void setConfMakeDate(String aConfMakeDate) {
        if (aConfMakeDate != null && !aConfMakeDate.equals("")) {
            ConfMakeDate = fDate.getDate(aConfMakeDate);
        } else
            ConfMakeDate = null;
    }

    public String getConfMakeTime() {
        return ConfMakeTime;
    }
    public void setConfMakeTime(String aConfMakeTime) {
        ConfMakeTime = aConfMakeTime;
    }
    public String getChequeDate() {
        if(ChequeDate != null) {
            return fDate.getString(ChequeDate);
        } else {
            return null;
        }
    }
    public void setChequeDate(Date aChequeDate) {
        ChequeDate = aChequeDate;
    }
    public void setChequeDate(String aChequeDate) {
        if (aChequeDate != null && !aChequeDate.equals("")) {
            ChequeDate = fDate.getDate(aChequeDate);
        } else
            ChequeDate = null;
    }

    public String getOperator() {
        return Operator;
    }
    public void setOperator(String aOperator) {
        Operator = aOperator;
    }
    public String getMakeDate() {
        if(MakeDate != null) {
            return fDate.getString(MakeDate);
        } else {
            return null;
        }
    }
    public void setMakeDate(Date aMakeDate) {
        MakeDate = aMakeDate;
    }
    public void setMakeDate(String aMakeDate) {
        if (aMakeDate != null && !aMakeDate.equals("")) {
            MakeDate = fDate.getDate(aMakeDate);
        } else
            MakeDate = null;
    }

    public String getMakeTime() {
        return MakeTime;
    }
    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }
    public String getModifyDate() {
        if(ModifyDate != null) {
            return fDate.getString(ModifyDate);
        } else {
            return null;
        }
    }
    public void setModifyDate(Date aModifyDate) {
        ModifyDate = aModifyDate;
    }
    public void setModifyDate(String aModifyDate) {
        if (aModifyDate != null && !aModifyDate.equals("")) {
            ModifyDate = fDate.getDate(aModifyDate);
        } else
            ModifyDate = null;
    }

    public String getModifyTime() {
        return ModifyTime;
    }
    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }
    public String getInBankCode() {
        return InBankCode;
    }
    public void setInBankCode(String aInBankCode) {
        InBankCode = aInBankCode;
    }
    public String getInBankAccNo() {
        return InBankAccNo;
    }
    public void setInBankAccNo(String aInBankAccNo) {
        InBankAccNo = aInBankAccNo;
    }
    public String getInAccName() {
        return InAccName;
    }
    public void setInAccName(String aInAccName) {
        InAccName = aInAccName;
    }
    public String getContCom() {
        return ContCom;
    }
    public void setContCom(String aContCom) {
        ContCom = aContCom;
    }
    public String getOtherNo() {
        return OtherNo;
    }
    public void setOtherNo(String aOtherNo) {
        OtherNo = aOtherNo;
    }
    public String getOtherNoType() {
        return OtherNoType;
    }
    public void setOtherNoType(String aOtherNoType) {
        OtherNoType = aOtherNoType;
    }
    public String getTempFeeNoType() {
        return TempFeeNoType;
    }
    public void setTempFeeNoType(String aTempFeeNoType) {
        TempFeeNoType = aTempFeeNoType;
    }
    public String getIDType() {
        return IDType;
    }
    public void setIDType(String aIDType) {
        IDType = aIDType;
    }
    public String getIDNo() {
        return IDNo;
    }
    public void setIDNo(String aIDNo) {
        IDNo = aIDNo;
    }

    /**
    * 使用另外一个 LKTempFeeClassSchema 对象给 Schema 赋值
    * @param: aLKTempFeeClassSchema LKTempFeeClassSchema
    **/
    public void setSchema(LKTempFeeClassSchema aLKTempFeeClassSchema) {
        this.TempFeeClassID = aLKTempFeeClassSchema.getTempFeeClassID();
        this.ShardingID = aLKTempFeeClassSchema.getShardingID();
        this.TempFeeNo = aLKTempFeeClassSchema.getTempFeeNo();
        this.PayMode = aLKTempFeeClassSchema.getPayMode();
        this.ChequeNo = aLKTempFeeClassSchema.getChequeNo();
        this.PayMoney = aLKTempFeeClassSchema.getPayMoney();
        this.AppntName = aLKTempFeeClassSchema.getAppntName();
        this.PayDate = fDate.getDate( aLKTempFeeClassSchema.getPayDate());
        this.ConfDate = fDate.getDate( aLKTempFeeClassSchema.getConfDate());
        this.ApproveDate = fDate.getDate( aLKTempFeeClassSchema.getApproveDate());
        this.EnterAccDate = fDate.getDate( aLKTempFeeClassSchema.getEnterAccDate());
        this.ConfFlag = aLKTempFeeClassSchema.getConfFlag();
        this.SerialNo = aLKTempFeeClassSchema.getSerialNo();
        this.ManageCom = aLKTempFeeClassSchema.getManageCom();
        this.PolicyCom = aLKTempFeeClassSchema.getPolicyCom();
        this.BankCode = aLKTempFeeClassSchema.getBankCode();
        this.BankAccNo = aLKTempFeeClassSchema.getBankAccNo();
        this.AccName = aLKTempFeeClassSchema.getAccName();
        this.ConfMakeDate = fDate.getDate( aLKTempFeeClassSchema.getConfMakeDate());
        this.ConfMakeTime = aLKTempFeeClassSchema.getConfMakeTime();
        this.ChequeDate = fDate.getDate( aLKTempFeeClassSchema.getChequeDate());
        this.Operator = aLKTempFeeClassSchema.getOperator();
        this.MakeDate = fDate.getDate( aLKTempFeeClassSchema.getMakeDate());
        this.MakeTime = aLKTempFeeClassSchema.getMakeTime();
        this.ModifyDate = fDate.getDate( aLKTempFeeClassSchema.getModifyDate());
        this.ModifyTime = aLKTempFeeClassSchema.getModifyTime();
        this.InBankCode = aLKTempFeeClassSchema.getInBankCode();
        this.InBankAccNo = aLKTempFeeClassSchema.getInBankAccNo();
        this.InAccName = aLKTempFeeClassSchema.getInAccName();
        this.ContCom = aLKTempFeeClassSchema.getContCom();
        this.OtherNo = aLKTempFeeClassSchema.getOtherNo();
        this.OtherNoType = aLKTempFeeClassSchema.getOtherNoType();
        this.TempFeeNoType = aLKTempFeeClassSchema.getTempFeeNoType();
        this.IDType = aLKTempFeeClassSchema.getIDType();
        this.IDNo = aLKTempFeeClassSchema.getIDNo();
    }

    /**
    * 使用 ResultSet 中的第 i 行给 Schema 赋值
    * @param: rs ResultSet
    * @param: i int
    * @return: boolean
    **/
    public boolean setSchema(ResultSet rs,int i) {
        try {
            //rs.absolute(i);        // 非滚动游标
            if( rs.getString("TempFeeClassID") == null )
                this.TempFeeClassID = null;
            else
                this.TempFeeClassID = rs.getString("TempFeeClassID").trim();

            if( rs.getString("ShardingID") == null )
                this.ShardingID = null;
            else
                this.ShardingID = rs.getString("ShardingID").trim();

            if( rs.getString("TempFeeNo") == null )
                this.TempFeeNo = null;
            else
                this.TempFeeNo = rs.getString("TempFeeNo").trim();

            if( rs.getString("PayMode") == null )
                this.PayMode = null;
            else
                this.PayMode = rs.getString("PayMode").trim();

            if( rs.getString("ChequeNo") == null )
                this.ChequeNo = null;
            else
                this.ChequeNo = rs.getString("ChequeNo").trim();

            this.PayMoney = rs.getDouble("PayMoney");
            if( rs.getString("AppntName") == null )
                this.AppntName = null;
            else
                this.AppntName = rs.getString("AppntName").trim();

            this.PayDate = rs.getDate("PayDate");
            this.ConfDate = rs.getDate("ConfDate");
            this.ApproveDate = rs.getDate("ApproveDate");
            this.EnterAccDate = rs.getDate("EnterAccDate");
            if( rs.getString("ConfFlag") == null )
                this.ConfFlag = null;
            else
                this.ConfFlag = rs.getString("ConfFlag").trim();

            if( rs.getString("SerialNo") == null )
                this.SerialNo = null;
            else
                this.SerialNo = rs.getString("SerialNo").trim();

            if( rs.getString("ManageCom") == null )
                this.ManageCom = null;
            else
                this.ManageCom = rs.getString("ManageCom").trim();

            if( rs.getString("PolicyCom") == null )
                this.PolicyCom = null;
            else
                this.PolicyCom = rs.getString("PolicyCom").trim();

            if( rs.getString("BankCode") == null )
                this.BankCode = null;
            else
                this.BankCode = rs.getString("BankCode").trim();

            if( rs.getString("BankAccNo") == null )
                this.BankAccNo = null;
            else
                this.BankAccNo = rs.getString("BankAccNo").trim();

            if( rs.getString("AccName") == null )
                this.AccName = null;
            else
                this.AccName = rs.getString("AccName").trim();

            this.ConfMakeDate = rs.getDate("ConfMakeDate");
            if( rs.getString("ConfMakeTime") == null )
                this.ConfMakeTime = null;
            else
                this.ConfMakeTime = rs.getString("ConfMakeTime").trim();

            this.ChequeDate = rs.getDate("ChequeDate");
            if( rs.getString("Operator") == null )
                this.Operator = null;
            else
                this.Operator = rs.getString("Operator").trim();

            this.MakeDate = rs.getDate("MakeDate");
            if( rs.getString("MakeTime") == null )
                this.MakeTime = null;
            else
                this.MakeTime = rs.getString("MakeTime").trim();

            this.ModifyDate = rs.getDate("ModifyDate");
            if( rs.getString("ModifyTime") == null )
                this.ModifyTime = null;
            else
                this.ModifyTime = rs.getString("ModifyTime").trim();

            if( rs.getString("InBankCode") == null )
                this.InBankCode = null;
            else
                this.InBankCode = rs.getString("InBankCode").trim();

            if( rs.getString("InBankAccNo") == null )
                this.InBankAccNo = null;
            else
                this.InBankAccNo = rs.getString("InBankAccNo").trim();

            if( rs.getString("InAccName") == null )
                this.InAccName = null;
            else
                this.InAccName = rs.getString("InAccName").trim();

            if( rs.getString("ContCom") == null )
                this.ContCom = null;
            else
                this.ContCom = rs.getString("ContCom").trim();

            if( rs.getString("OtherNo") == null )
                this.OtherNo = null;
            else
                this.OtherNo = rs.getString("OtherNo").trim();

            if( rs.getString("OtherNoType") == null )
                this.OtherNoType = null;
            else
                this.OtherNoType = rs.getString("OtherNoType").trim();

            if( rs.getString("TempFeeNoType") == null )
                this.TempFeeNoType = null;
            else
                this.TempFeeNoType = rs.getString("TempFeeNoType").trim();

            if( rs.getString("IDType") == null )
                this.IDType = null;
            else
                this.IDType = rs.getString("IDType").trim();

            if( rs.getString("IDNo") == null )
                this.IDNo = null;
            else
                this.IDNo = rs.getString("IDNo").trim();

        }
        catch(SQLException sqle) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LKTempFeeClassSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = ExceptionUtils.exceptionToString(sqle);
            this.mErrors .addOneError(tError);
            return false;
        }
        return true;
    }

    public LKTempFeeClassSchema getSchema() {
        LKTempFeeClassSchema aLKTempFeeClassSchema = new LKTempFeeClassSchema();
        aLKTempFeeClassSchema.setSchema(this);
        return aLKTempFeeClassSchema;
    }

    public LKTempFeeClassDB getDB() {
        LKTempFeeClassDB aDBOper = new LKTempFeeClassDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
    * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLKTempFeeClass描述/A>表字段
    * @return: String 返回打包后字符串
    **/
    public String encode() {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(TempFeeClassID)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ShardingID)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(TempFeeNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(PayMode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ChequeNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(PayMoney));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AppntName)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( PayDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( ConfDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( ApproveDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( EnterAccDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ConfFlag)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(SerialNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ManageCom)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(PolicyCom)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(BankCode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(BankAccNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AccName)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( ConfMakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ConfMakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( ChequeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ModifyTime)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(InBankCode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(InBankAccNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(InAccName)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ContCom)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(OtherNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(OtherNoType)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(TempFeeNoType)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(IDType)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(IDNo));
        return strReturn.toString();
    }

    /**
    * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLKTempFeeClass>历史记账凭证主表信息</A>表字段
    * @param: strMessage String 包含一条纪录数据的字符串
    * @return: boolean
    **/
    public boolean decode(String strMessage) {
        try {
            TempFeeClassID = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
            ShardingID = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
            TempFeeNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
            PayMode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
            ChequeNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
            PayMoney = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,6, SysConst.PACKAGESPILTER))).doubleValue();
            AppntName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
            PayDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER));
            ConfDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER));
            ApproveDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER));
            EnterAccDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER));
            ConfFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
            SerialNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
            ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER );
            PolicyCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15, SysConst.PACKAGESPILTER );
            BankCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16, SysConst.PACKAGESPILTER );
            BankAccNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17, SysConst.PACKAGESPILTER );
            AccName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18, SysConst.PACKAGESPILTER );
            ConfMakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19, SysConst.PACKAGESPILTER));
            ConfMakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20, SysConst.PACKAGESPILTER );
            ChequeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 21, SysConst.PACKAGESPILTER));
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 22, SysConst.PACKAGESPILTER );
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 23, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 24, SysConst.PACKAGESPILTER );
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 25, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 26, SysConst.PACKAGESPILTER );
            InBankCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 27, SysConst.PACKAGESPILTER );
            InBankAccNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 28, SysConst.PACKAGESPILTER );
            InAccName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 29, SysConst.PACKAGESPILTER );
            ContCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 30, SysConst.PACKAGESPILTER );
            OtherNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 31, SysConst.PACKAGESPILTER );
            OtherNoType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 32, SysConst.PACKAGESPILTER );
            TempFeeNoType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 33, SysConst.PACKAGESPILTER );
            IDType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 34, SysConst.PACKAGESPILTER );
            IDNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 35, SysConst.PACKAGESPILTER );
        }
        catch(NumberFormatException ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LKTempFeeClassSchema";
            tError.functionName = "decode";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            return false;
        }
        return true;
    }

    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("TempFeeClassID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(TempFeeClassID));
        }
        if (FCode.equalsIgnoreCase("ShardingID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ShardingID));
        }
        if (FCode.equalsIgnoreCase("TempFeeNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(TempFeeNo));
        }
        if (FCode.equalsIgnoreCase("PayMode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PayMode));
        }
        if (FCode.equalsIgnoreCase("ChequeNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ChequeNo));
        }
        if (FCode.equalsIgnoreCase("PayMoney")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PayMoney));
        }
        if (FCode.equalsIgnoreCase("AppntName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AppntName));
        }
        if (FCode.equalsIgnoreCase("PayDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getPayDate()));
        }
        if (FCode.equalsIgnoreCase("ConfDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getConfDate()));
        }
        if (FCode.equalsIgnoreCase("ApproveDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getApproveDate()));
        }
        if (FCode.equalsIgnoreCase("EnterAccDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getEnterAccDate()));
        }
        if (FCode.equalsIgnoreCase("ConfFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ConfFlag));
        }
        if (FCode.equalsIgnoreCase("SerialNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SerialNo));
        }
        if (FCode.equalsIgnoreCase("ManageCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
        }
        if (FCode.equalsIgnoreCase("PolicyCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PolicyCom));
        }
        if (FCode.equalsIgnoreCase("BankCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BankCode));
        }
        if (FCode.equalsIgnoreCase("BankAccNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BankAccNo));
        }
        if (FCode.equalsIgnoreCase("AccName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AccName));
        }
        if (FCode.equalsIgnoreCase("ConfMakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getConfMakeDate()));
        }
        if (FCode.equalsIgnoreCase("ConfMakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ConfMakeTime));
        }
        if (FCode.equalsIgnoreCase("ChequeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getChequeDate()));
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (FCode.equalsIgnoreCase("InBankCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InBankCode));
        }
        if (FCode.equalsIgnoreCase("InBankAccNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InBankAccNo));
        }
        if (FCode.equalsIgnoreCase("InAccName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InAccName));
        }
        if (FCode.equalsIgnoreCase("ContCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ContCom));
        }
        if (FCode.equalsIgnoreCase("OtherNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OtherNo));
        }
        if (FCode.equalsIgnoreCase("OtherNoType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OtherNoType));
        }
        if (FCode.equalsIgnoreCase("TempFeeNoType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(TempFeeNoType));
        }
        if (FCode.equalsIgnoreCase("IDType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(IDType));
        }
        if (FCode.equalsIgnoreCase("IDNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(IDNo));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(TempFeeClassID);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(ShardingID);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(TempFeeNo);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(PayMode);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(ChequeNo);
                break;
            case 5:
                strFieldValue = String.valueOf(PayMoney);
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(AppntName);
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getPayDate()));
                break;
            case 8:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getConfDate()));
                break;
            case 9:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getApproveDate()));
                break;
            case 10:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getEnterAccDate()));
                break;
            case 11:
                strFieldValue = StrTool.GBKToUnicode(ConfFlag);
                break;
            case 12:
                strFieldValue = StrTool.GBKToUnicode(SerialNo);
                break;
            case 13:
                strFieldValue = StrTool.GBKToUnicode(ManageCom);
                break;
            case 14:
                strFieldValue = StrTool.GBKToUnicode(PolicyCom);
                break;
            case 15:
                strFieldValue = StrTool.GBKToUnicode(BankCode);
                break;
            case 16:
                strFieldValue = StrTool.GBKToUnicode(BankAccNo);
                break;
            case 17:
                strFieldValue = StrTool.GBKToUnicode(AccName);
                break;
            case 18:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getConfMakeDate()));
                break;
            case 19:
                strFieldValue = StrTool.GBKToUnicode(ConfMakeTime);
                break;
            case 20:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getChequeDate()));
                break;
            case 21:
                strFieldValue = StrTool.GBKToUnicode(Operator);
                break;
            case 22:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
                break;
            case 23:
                strFieldValue = StrTool.GBKToUnicode(MakeTime);
                break;
            case 24:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
                break;
            case 25:
                strFieldValue = StrTool.GBKToUnicode(ModifyTime);
                break;
            case 26:
                strFieldValue = StrTool.GBKToUnicode(InBankCode);
                break;
            case 27:
                strFieldValue = StrTool.GBKToUnicode(InBankAccNo);
                break;
            case 28:
                strFieldValue = StrTool.GBKToUnicode(InAccName);
                break;
            case 29:
                strFieldValue = StrTool.GBKToUnicode(ContCom);
                break;
            case 30:
                strFieldValue = StrTool.GBKToUnicode(OtherNo);
                break;
            case 31:
                strFieldValue = StrTool.GBKToUnicode(OtherNoType);
                break;
            case 32:
                strFieldValue = StrTool.GBKToUnicode(TempFeeNoType);
                break;
            case 33:
                strFieldValue = StrTool.GBKToUnicode(IDType);
                break;
            case 34:
                strFieldValue = StrTool.GBKToUnicode(IDNo);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("TempFeeClassID")) {
            if( FValue != null && !FValue.equals(""))
            {
                TempFeeClassID = FValue.trim();
            }
            else
                TempFeeClassID = null;
        }
        if (FCode.equalsIgnoreCase("ShardingID")) {
            if( FValue != null && !FValue.equals(""))
            {
                ShardingID = FValue.trim();
            }
            else
                ShardingID = null;
        }
        if (FCode.equalsIgnoreCase("TempFeeNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                TempFeeNo = FValue.trim();
            }
            else
                TempFeeNo = null;
        }
        if (FCode.equalsIgnoreCase("PayMode")) {
            if( FValue != null && !FValue.equals(""))
            {
                PayMode = FValue.trim();
            }
            else
                PayMode = null;
        }
        if (FCode.equalsIgnoreCase("ChequeNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                ChequeNo = FValue.trim();
            }
            else
                ChequeNo = null;
        }
        if (FCode.equalsIgnoreCase("PayMoney")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                PayMoney = d;
            }
        }
        if (FCode.equalsIgnoreCase("AppntName")) {
            if( FValue != null && !FValue.equals(""))
            {
                AppntName = FValue.trim();
            }
            else
                AppntName = null;
        }
        if (FCode.equalsIgnoreCase("PayDate")) {
            if(FValue != null && !FValue.equals("")) {
                PayDate = fDate.getDate( FValue );
            }
            else
                PayDate = null;
        }
        if (FCode.equalsIgnoreCase("ConfDate")) {
            if(FValue != null && !FValue.equals("")) {
                ConfDate = fDate.getDate( FValue );
            }
            else
                ConfDate = null;
        }
        if (FCode.equalsIgnoreCase("ApproveDate")) {
            if(FValue != null && !FValue.equals("")) {
                ApproveDate = fDate.getDate( FValue );
            }
            else
                ApproveDate = null;
        }
        if (FCode.equalsIgnoreCase("EnterAccDate")) {
            if(FValue != null && !FValue.equals("")) {
                EnterAccDate = fDate.getDate( FValue );
            }
            else
                EnterAccDate = null;
        }
        if (FCode.equalsIgnoreCase("ConfFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                ConfFlag = FValue.trim();
            }
            else
                ConfFlag = null;
        }
        if (FCode.equalsIgnoreCase("SerialNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                SerialNo = FValue.trim();
            }
            else
                SerialNo = null;
        }
        if (FCode.equalsIgnoreCase("ManageCom")) {
            if( FValue != null && !FValue.equals(""))
            {
                ManageCom = FValue.trim();
            }
            else
                ManageCom = null;
        }
        if (FCode.equalsIgnoreCase("PolicyCom")) {
            if( FValue != null && !FValue.equals(""))
            {
                PolicyCom = FValue.trim();
            }
            else
                PolicyCom = null;
        }
        if (FCode.equalsIgnoreCase("BankCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                BankCode = FValue.trim();
            }
            else
                BankCode = null;
        }
        if (FCode.equalsIgnoreCase("BankAccNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                BankAccNo = FValue.trim();
            }
            else
                BankAccNo = null;
        }
        if (FCode.equalsIgnoreCase("AccName")) {
            if( FValue != null && !FValue.equals(""))
            {
                AccName = FValue.trim();
            }
            else
                AccName = null;
        }
        if (FCode.equalsIgnoreCase("ConfMakeDate")) {
            if(FValue != null && !FValue.equals("")) {
                ConfMakeDate = fDate.getDate( FValue );
            }
            else
                ConfMakeDate = null;
        }
        if (FCode.equalsIgnoreCase("ConfMakeTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                ConfMakeTime = FValue.trim();
            }
            else
                ConfMakeTime = null;
        }
        if (FCode.equalsIgnoreCase("ChequeDate")) {
            if(FValue != null && !FValue.equals("")) {
                ChequeDate = fDate.getDate( FValue );
            }
            else
                ChequeDate = null;
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            if( FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
                Operator = null;
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if(FValue != null && !FValue.equals("")) {
                MakeDate = fDate.getDate( FValue );
            }
            else
                MakeDate = null;
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
                MakeTime = null;
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if(FValue != null && !FValue.equals("")) {
                ModifyDate = fDate.getDate( FValue );
            }
            else
                ModifyDate = null;
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
                ModifyTime = null;
        }
        if (FCode.equalsIgnoreCase("InBankCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                InBankCode = FValue.trim();
            }
            else
                InBankCode = null;
        }
        if (FCode.equalsIgnoreCase("InBankAccNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                InBankAccNo = FValue.trim();
            }
            else
                InBankAccNo = null;
        }
        if (FCode.equalsIgnoreCase("InAccName")) {
            if( FValue != null && !FValue.equals(""))
            {
                InAccName = FValue.trim();
            }
            else
                InAccName = null;
        }
        if (FCode.equalsIgnoreCase("ContCom")) {
            if( FValue != null && !FValue.equals(""))
            {
                ContCom = FValue.trim();
            }
            else
                ContCom = null;
        }
        if (FCode.equalsIgnoreCase("OtherNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                OtherNo = FValue.trim();
            }
            else
                OtherNo = null;
        }
        if (FCode.equalsIgnoreCase("OtherNoType")) {
            if( FValue != null && !FValue.equals(""))
            {
                OtherNoType = FValue.trim();
            }
            else
                OtherNoType = null;
        }
        if (FCode.equalsIgnoreCase("TempFeeNoType")) {
            if( FValue != null && !FValue.equals(""))
            {
                TempFeeNoType = FValue.trim();
            }
            else
                TempFeeNoType = null;
        }
        if (FCode.equalsIgnoreCase("IDType")) {
            if( FValue != null && !FValue.equals(""))
            {
                IDType = FValue.trim();
            }
            else
                IDType = null;
        }
        if (FCode.equalsIgnoreCase("IDNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                IDNo = FValue.trim();
            }
            else
                IDNo = null;
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject) return true;
        if (otherObject == null) return false;
        if (getClass() != otherObject.getClass()) return false;
        LKTempFeeClassSchema other = (LKTempFeeClassSchema)otherObject;
        return
            TempFeeClassID.equals(other.getTempFeeClassID())
            && ShardingID.equals(other.getShardingID())
            && TempFeeNo.equals(other.getTempFeeNo())
            && PayMode.equals(other.getPayMode())
            && ChequeNo.equals(other.getChequeNo())
            && PayMoney == other.getPayMoney()
            && AppntName.equals(other.getAppntName())
            && fDate.getString(PayDate).equals(other.getPayDate())
            && fDate.getString(ConfDate).equals(other.getConfDate())
            && fDate.getString(ApproveDate).equals(other.getApproveDate())
            && fDate.getString(EnterAccDate).equals(other.getEnterAccDate())
            && ConfFlag.equals(other.getConfFlag())
            && SerialNo.equals(other.getSerialNo())
            && ManageCom.equals(other.getManageCom())
            && PolicyCom.equals(other.getPolicyCom())
            && BankCode.equals(other.getBankCode())
            && BankAccNo.equals(other.getBankAccNo())
            && AccName.equals(other.getAccName())
            && fDate.getString(ConfMakeDate).equals(other.getConfMakeDate())
            && ConfMakeTime.equals(other.getConfMakeTime())
            && fDate.getString(ChequeDate).equals(other.getChequeDate())
            && Operator.equals(other.getOperator())
            && fDate.getString(MakeDate).equals(other.getMakeDate())
            && MakeTime.equals(other.getMakeTime())
            && fDate.getString(ModifyDate).equals(other.getModifyDate())
            && ModifyTime.equals(other.getModifyTime())
            && InBankCode.equals(other.getInBankCode())
            && InBankAccNo.equals(other.getInBankAccNo())
            && InAccName.equals(other.getInAccName())
            && ContCom.equals(other.getContCom())
            && OtherNo.equals(other.getOtherNo())
            && OtherNoType.equals(other.getOtherNoType())
            && TempFeeNoType.equals(other.getTempFeeNoType())
            && IDType.equals(other.getIDType())
            && IDNo.equals(other.getIDNo());
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("TempFeeClassID") ) {
            return 0;
        }
        if( strFieldName.equals("ShardingID") ) {
            return 1;
        }
        if( strFieldName.equals("TempFeeNo") ) {
            return 2;
        }
        if( strFieldName.equals("PayMode") ) {
            return 3;
        }
        if( strFieldName.equals("ChequeNo") ) {
            return 4;
        }
        if( strFieldName.equals("PayMoney") ) {
            return 5;
        }
        if( strFieldName.equals("AppntName") ) {
            return 6;
        }
        if( strFieldName.equals("PayDate") ) {
            return 7;
        }
        if( strFieldName.equals("ConfDate") ) {
            return 8;
        }
        if( strFieldName.equals("ApproveDate") ) {
            return 9;
        }
        if( strFieldName.equals("EnterAccDate") ) {
            return 10;
        }
        if( strFieldName.equals("ConfFlag") ) {
            return 11;
        }
        if( strFieldName.equals("SerialNo") ) {
            return 12;
        }
        if( strFieldName.equals("ManageCom") ) {
            return 13;
        }
        if( strFieldName.equals("PolicyCom") ) {
            return 14;
        }
        if( strFieldName.equals("BankCode") ) {
            return 15;
        }
        if( strFieldName.equals("BankAccNo") ) {
            return 16;
        }
        if( strFieldName.equals("AccName") ) {
            return 17;
        }
        if( strFieldName.equals("ConfMakeDate") ) {
            return 18;
        }
        if( strFieldName.equals("ConfMakeTime") ) {
            return 19;
        }
        if( strFieldName.equals("ChequeDate") ) {
            return 20;
        }
        if( strFieldName.equals("Operator") ) {
            return 21;
        }
        if( strFieldName.equals("MakeDate") ) {
            return 22;
        }
        if( strFieldName.equals("MakeTime") ) {
            return 23;
        }
        if( strFieldName.equals("ModifyDate") ) {
            return 24;
        }
        if( strFieldName.equals("ModifyTime") ) {
            return 25;
        }
        if( strFieldName.equals("InBankCode") ) {
            return 26;
        }
        if( strFieldName.equals("InBankAccNo") ) {
            return 27;
        }
        if( strFieldName.equals("InAccName") ) {
            return 28;
        }
        if( strFieldName.equals("ContCom") ) {
            return 29;
        }
        if( strFieldName.equals("OtherNo") ) {
            return 30;
        }
        if( strFieldName.equals("OtherNoType") ) {
            return 31;
        }
        if( strFieldName.equals("TempFeeNoType") ) {
            return 32;
        }
        if( strFieldName.equals("IDType") ) {
            return 33;
        }
        if( strFieldName.equals("IDNo") ) {
            return 34;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "TempFeeClassID";
                break;
            case 1:
                strFieldName = "ShardingID";
                break;
            case 2:
                strFieldName = "TempFeeNo";
                break;
            case 3:
                strFieldName = "PayMode";
                break;
            case 4:
                strFieldName = "ChequeNo";
                break;
            case 5:
                strFieldName = "PayMoney";
                break;
            case 6:
                strFieldName = "AppntName";
                break;
            case 7:
                strFieldName = "PayDate";
                break;
            case 8:
                strFieldName = "ConfDate";
                break;
            case 9:
                strFieldName = "ApproveDate";
                break;
            case 10:
                strFieldName = "EnterAccDate";
                break;
            case 11:
                strFieldName = "ConfFlag";
                break;
            case 12:
                strFieldName = "SerialNo";
                break;
            case 13:
                strFieldName = "ManageCom";
                break;
            case 14:
                strFieldName = "PolicyCom";
                break;
            case 15:
                strFieldName = "BankCode";
                break;
            case 16:
                strFieldName = "BankAccNo";
                break;
            case 17:
                strFieldName = "AccName";
                break;
            case 18:
                strFieldName = "ConfMakeDate";
                break;
            case 19:
                strFieldName = "ConfMakeTime";
                break;
            case 20:
                strFieldName = "ChequeDate";
                break;
            case 21:
                strFieldName = "Operator";
                break;
            case 22:
                strFieldName = "MakeDate";
                break;
            case 23:
                strFieldName = "MakeTime";
                break;
            case 24:
                strFieldName = "ModifyDate";
                break;
            case 25:
                strFieldName = "ModifyTime";
                break;
            case 26:
                strFieldName = "InBankCode";
                break;
            case 27:
                strFieldName = "InBankAccNo";
                break;
            case 28:
                strFieldName = "InAccName";
                break;
            case 29:
                strFieldName = "ContCom";
                break;
            case 30:
                strFieldName = "OtherNo";
                break;
            case 31:
                strFieldName = "OtherNoType";
                break;
            case 32:
                strFieldName = "TempFeeNoType";
                break;
            case 33:
                strFieldName = "IDType";
                break;
            case 34:
                strFieldName = "IDNo";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "TEMPFEECLASSID":
                return Schema.TYPE_STRING;
            case "SHARDINGID":
                return Schema.TYPE_STRING;
            case "TEMPFEENO":
                return Schema.TYPE_STRING;
            case "PAYMODE":
                return Schema.TYPE_STRING;
            case "CHEQUENO":
                return Schema.TYPE_STRING;
            case "PAYMONEY":
                return Schema.TYPE_DOUBLE;
            case "APPNTNAME":
                return Schema.TYPE_STRING;
            case "PAYDATE":
                return Schema.TYPE_DATE;
            case "CONFDATE":
                return Schema.TYPE_DATE;
            case "APPROVEDATE":
                return Schema.TYPE_DATE;
            case "ENTERACCDATE":
                return Schema.TYPE_DATE;
            case "CONFFLAG":
                return Schema.TYPE_STRING;
            case "SERIALNO":
                return Schema.TYPE_STRING;
            case "MANAGECOM":
                return Schema.TYPE_STRING;
            case "POLICYCOM":
                return Schema.TYPE_STRING;
            case "BANKCODE":
                return Schema.TYPE_STRING;
            case "BANKACCNO":
                return Schema.TYPE_STRING;
            case "ACCNAME":
                return Schema.TYPE_STRING;
            case "CONFMAKEDATE":
                return Schema.TYPE_DATE;
            case "CONFMAKETIME":
                return Schema.TYPE_STRING;
            case "CHEQUEDATE":
                return Schema.TYPE_DATE;
            case "OPERATOR":
                return Schema.TYPE_STRING;
            case "MAKEDATE":
                return Schema.TYPE_DATE;
            case "MAKETIME":
                return Schema.TYPE_STRING;
            case "MODIFYDATE":
                return Schema.TYPE_DATE;
            case "MODIFYTIME":
                return Schema.TYPE_STRING;
            case "INBANKCODE":
                return Schema.TYPE_STRING;
            case "INBANKACCNO":
                return Schema.TYPE_STRING;
            case "INACCNAME":
                return Schema.TYPE_STRING;
            case "CONTCOM":
                return Schema.TYPE_STRING;
            case "OTHERNO":
                return Schema.TYPE_STRING;
            case "OTHERNOTYPE":
                return Schema.TYPE_STRING;
            case "TEMPFEENOTYPE":
                return Schema.TYPE_STRING;
            case "IDTYPE":
                return Schema.TYPE_STRING;
            case "IDNO":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_STRING;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_DOUBLE;
            case 6:
                return Schema.TYPE_STRING;
            case 7:
                return Schema.TYPE_DATE;
            case 8:
                return Schema.TYPE_DATE;
            case 9:
                return Schema.TYPE_DATE;
            case 10:
                return Schema.TYPE_DATE;
            case 11:
                return Schema.TYPE_STRING;
            case 12:
                return Schema.TYPE_STRING;
            case 13:
                return Schema.TYPE_STRING;
            case 14:
                return Schema.TYPE_STRING;
            case 15:
                return Schema.TYPE_STRING;
            case 16:
                return Schema.TYPE_STRING;
            case 17:
                return Schema.TYPE_STRING;
            case 18:
                return Schema.TYPE_DATE;
            case 19:
                return Schema.TYPE_STRING;
            case 20:
                return Schema.TYPE_DATE;
            case 21:
                return Schema.TYPE_STRING;
            case 22:
                return Schema.TYPE_DATE;
            case 23:
                return Schema.TYPE_STRING;
            case 24:
                return Schema.TYPE_DATE;
            case 25:
                return Schema.TYPE_STRING;
            case 26:
                return Schema.TYPE_STRING;
            case 27:
                return Schema.TYPE_STRING;
            case 28:
                return Schema.TYPE_STRING;
            case 29:
                return Schema.TYPE_STRING;
            case 30:
                return Schema.TYPE_STRING;
            case 31:
                return Schema.TYPE_STRING;
            case 32:
                return Schema.TYPE_STRING;
            case 33:
                return Schema.TYPE_STRING;
            case 34:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
}
