/**
 * Copyright (c) 2018 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.entity;

import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.Pojo;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;

import java.io.Serializable;

/**
 * <p>ClassName: LKTempFeePojo </p>
 * <p>Copyright: Copyright (c) 2018</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2018-08-08
 */
public class LKTempFeePojo implements Pojo,Serializable {
    // @Field
    /** Id */
    private long TempFeeID; 
    /** Shardingid */
    private String ShardingID; 
    /** 暂交费收据号码 */
    private String TempFeeNo; 
    /** 暂交费收据号类型 */
    private String TempFeeType; 
    /** 险种编码 */
    private String RiskCode; 
    /** 交费间隔 */
    private int PayIntv; 
    /** 对应其它号码 */
    private String OtherNo; 
    /** 其它号码类型 */
    private String OtherNoType; 
    /** 交费金额 */
    private double PayMoney; 
    /** 交费日期 */
    private String  PayDate;
    /** 到帐日期 */
    private String  EnterAccDate;
    /** 确认日期 */
    private String  ConfDate;
    /** 财务确认操作日期 */
    private String  ConfMakeDate;
    /** 财务确认操作时间 */
    private String ConfMakeTime; 
    /** 销售渠道 */
    private String SaleChnl; 
    /** 交费机构 */
    private String ManageCom; 
    /** 管理机构 */
    private String PolicyCom; 
    /** 代理机构 */
    private String AgentCom; 
    /** 代理机构内部分类 */
    private String AgentType; 
    /** 投保人名称 */
    private String APPntName; 
    /** 代理人组别 */
    private String AgentGroup; 
    /** 代理人编码 */
    private String AgentCode; 
    /** 是否核销标志 */
    private String ConfFlag; 
    /** 流水号 */
    private String SerialNo; 
    /** 操作员 */
    private String Operator; 
    /** 状态 */
    private String State; 
    /** 入机时间 */
    private String MakeTime; 
    /** 入机日期 */
    private String  MakeDate;
    /** 最后一次修改日期 */
    private String  ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime; 
    /** 保单所属机构 */
    private String ContCom; 
    /** 交费年期 */
    private int PayEndYear; 
    /** 收据类型 */
    private String TempFeeNoType; 
    /** 预收标保 */
    private double StandPrem; 
    /** 备注 */
    private String Remark; 
    /** 代理人所在区 */
    private String Distict; 
    /** 代理人所在部 */
    private String Department; 
    /** 代理人所在组 */
    private String BranchCode; 
    /** 合同号码 */
    private String ContNo; 


    public static final int FIELDNUM = 39;    // 数据库表的字段个数

    private FDate fDate = new FDate();        // 处理日期
    public long getTempFeeID() {
        return TempFeeID;
    }
    public void setTempFeeID(long aTempFeeID) {
        TempFeeID = aTempFeeID;
    }
    public void setTempFeeID(String aTempFeeID) {
        if (aTempFeeID != null && !aTempFeeID.equals("")) {
            TempFeeID = new Long(aTempFeeID).longValue();
        }
    }

    public String getShardingID() {
        return ShardingID;
    }
    public void setShardingID(String aShardingID) {
        ShardingID = aShardingID;
    }
    public String getTempFeeNo() {
        return TempFeeNo;
    }
    public void setTempFeeNo(String aTempFeeNo) {
        TempFeeNo = aTempFeeNo;
    }
    public String getTempFeeType() {
        return TempFeeType;
    }
    public void setTempFeeType(String aTempFeeType) {
        TempFeeType = aTempFeeType;
    }
    public String getRiskCode() {
        return RiskCode;
    }
    public void setRiskCode(String aRiskCode) {
        RiskCode = aRiskCode;
    }
    public int getPayIntv() {
        return PayIntv;
    }
    public void setPayIntv(int aPayIntv) {
        PayIntv = aPayIntv;
    }
    public void setPayIntv(String aPayIntv) {
        if (aPayIntv != null && !aPayIntv.equals("")) {
            Integer tInteger = new Integer(aPayIntv);
            int i = tInteger.intValue();
            PayIntv = i;
        }
    }

    public String getOtherNo() {
        return OtherNo;
    }
    public void setOtherNo(String aOtherNo) {
        OtherNo = aOtherNo;
    }
    public String getOtherNoType() {
        return OtherNoType;
    }
    public void setOtherNoType(String aOtherNoType) {
        OtherNoType = aOtherNoType;
    }
    public double getPayMoney() {
        return PayMoney;
    }
    public void setPayMoney(double aPayMoney) {
        PayMoney = aPayMoney;
    }
    public void setPayMoney(String aPayMoney) {
        if (aPayMoney != null && !aPayMoney.equals("")) {
            Double tDouble = new Double(aPayMoney);
            double d = tDouble.doubleValue();
            PayMoney = d;
        }
    }

    public String getPayDate() {
        return PayDate;
    }
    public void setPayDate(String aPayDate) {
        PayDate = aPayDate;
    }
    public String getEnterAccDate() {
        return EnterAccDate;
    }
    public void setEnterAccDate(String aEnterAccDate) {
        EnterAccDate = aEnterAccDate;
    }
    public String getConfDate() {
        return ConfDate;
    }
    public void setConfDate(String aConfDate) {
        ConfDate = aConfDate;
    }
    public String getConfMakeDate() {
        return ConfMakeDate;
    }
    public void setConfMakeDate(String aConfMakeDate) {
        ConfMakeDate = aConfMakeDate;
    }
    public String getConfMakeTime() {
        return ConfMakeTime;
    }
    public void setConfMakeTime(String aConfMakeTime) {
        ConfMakeTime = aConfMakeTime;
    }
    public String getSaleChnl() {
        return SaleChnl;
    }
    public void setSaleChnl(String aSaleChnl) {
        SaleChnl = aSaleChnl;
    }
    public String getManageCom() {
        return ManageCom;
    }
    public void setManageCom(String aManageCom) {
        ManageCom = aManageCom;
    }
    public String getPolicyCom() {
        return PolicyCom;
    }
    public void setPolicyCom(String aPolicyCom) {
        PolicyCom = aPolicyCom;
    }
    public String getAgentCom() {
        return AgentCom;
    }
    public void setAgentCom(String aAgentCom) {
        AgentCom = aAgentCom;
    }
    public String getAgentType() {
        return AgentType;
    }
    public void setAgentType(String aAgentType) {
        AgentType = aAgentType;
    }
    public String getAPPntName() {
        return APPntName;
    }
    public void setAPPntName(String aAPPntName) {
        APPntName = aAPPntName;
    }
    public String getAgentGroup() {
        return AgentGroup;
    }
    public void setAgentGroup(String aAgentGroup) {
        AgentGroup = aAgentGroup;
    }
    public String getAgentCode() {
        return AgentCode;
    }
    public void setAgentCode(String aAgentCode) {
        AgentCode = aAgentCode;
    }
    public String getConfFlag() {
        return ConfFlag;
    }
    public void setConfFlag(String aConfFlag) {
        ConfFlag = aConfFlag;
    }
    public String getSerialNo() {
        return SerialNo;
    }
    public void setSerialNo(String aSerialNo) {
        SerialNo = aSerialNo;
    }
    public String getOperator() {
        return Operator;
    }
    public void setOperator(String aOperator) {
        Operator = aOperator;
    }
    public String getState() {
        return State;
    }
    public void setState(String aState) {
        State = aState;
    }
    public String getMakeTime() {
        return MakeTime;
    }
    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }
    public String getMakeDate() {
        return MakeDate;
    }
    public void setMakeDate(String aMakeDate) {
        MakeDate = aMakeDate;
    }
    public String getModifyDate() {
        return ModifyDate;
    }
    public void setModifyDate(String aModifyDate) {
        ModifyDate = aModifyDate;
    }
    public String getModifyTime() {
        return ModifyTime;
    }
    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }
    public String getContCom() {
        return ContCom;
    }
    public void setContCom(String aContCom) {
        ContCom = aContCom;
    }
    public int getPayEndYear() {
        return PayEndYear;
    }
    public void setPayEndYear(int aPayEndYear) {
        PayEndYear = aPayEndYear;
    }
    public void setPayEndYear(String aPayEndYear) {
        if (aPayEndYear != null && !aPayEndYear.equals("")) {
            Integer tInteger = new Integer(aPayEndYear);
            int i = tInteger.intValue();
            PayEndYear = i;
        }
    }

    public String getTempFeeNoType() {
        return TempFeeNoType;
    }
    public void setTempFeeNoType(String aTempFeeNoType) {
        TempFeeNoType = aTempFeeNoType;
    }
    public double getStandPrem() {
        return StandPrem;
    }
    public void setStandPrem(double aStandPrem) {
        StandPrem = aStandPrem;
    }
    public void setStandPrem(String aStandPrem) {
        if (aStandPrem != null && !aStandPrem.equals("")) {
            Double tDouble = new Double(aStandPrem);
            double d = tDouble.doubleValue();
            StandPrem = d;
        }
    }

    public String getRemark() {
        return Remark;
    }
    public void setRemark(String aRemark) {
        Remark = aRemark;
    }
    public String getDistict() {
        return Distict;
    }
    public void setDistict(String aDistict) {
        Distict = aDistict;
    }
    public String getDepartment() {
        return Department;
    }
    public void setDepartment(String aDepartment) {
        Department = aDepartment;
    }
    public String getBranchCode() {
        return BranchCode;
    }
    public void setBranchCode(String aBranchCode) {
        BranchCode = aBranchCode;
    }
    public String getContNo() {
        return ContNo;
    }
    public void setContNo(String aContNo) {
        ContNo = aContNo;
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("TempFeeID") ) {
            return 0;
        }
        if( strFieldName.equals("ShardingID") ) {
            return 1;
        }
        if( strFieldName.equals("TempFeeNo") ) {
            return 2;
        }
        if( strFieldName.equals("TempFeeType") ) {
            return 3;
        }
        if( strFieldName.equals("RiskCode") ) {
            return 4;
        }
        if( strFieldName.equals("PayIntv") ) {
            return 5;
        }
        if( strFieldName.equals("OtherNo") ) {
            return 6;
        }
        if( strFieldName.equals("OtherNoType") ) {
            return 7;
        }
        if( strFieldName.equals("PayMoney") ) {
            return 8;
        }
        if( strFieldName.equals("PayDate") ) {
            return 9;
        }
        if( strFieldName.equals("EnterAccDate") ) {
            return 10;
        }
        if( strFieldName.equals("ConfDate") ) {
            return 11;
        }
        if( strFieldName.equals("ConfMakeDate") ) {
            return 12;
        }
        if( strFieldName.equals("ConfMakeTime") ) {
            return 13;
        }
        if( strFieldName.equals("SaleChnl") ) {
            return 14;
        }
        if( strFieldName.equals("ManageCom") ) {
            return 15;
        }
        if( strFieldName.equals("PolicyCom") ) {
            return 16;
        }
        if( strFieldName.equals("AgentCom") ) {
            return 17;
        }
        if( strFieldName.equals("AgentType") ) {
            return 18;
        }
        if( strFieldName.equals("APPntName") ) {
            return 19;
        }
        if( strFieldName.equals("AgentGroup") ) {
            return 20;
        }
        if( strFieldName.equals("AgentCode") ) {
            return 21;
        }
        if( strFieldName.equals("ConfFlag") ) {
            return 22;
        }
        if( strFieldName.equals("SerialNo") ) {
            return 23;
        }
        if( strFieldName.equals("Operator") ) {
            return 24;
        }
        if( strFieldName.equals("State") ) {
            return 25;
        }
        if( strFieldName.equals("MakeTime") ) {
            return 26;
        }
        if( strFieldName.equals("MakeDate") ) {
            return 27;
        }
        if( strFieldName.equals("ModifyDate") ) {
            return 28;
        }
        if( strFieldName.equals("ModifyTime") ) {
            return 29;
        }
        if( strFieldName.equals("ContCom") ) {
            return 30;
        }
        if( strFieldName.equals("PayEndYear") ) {
            return 31;
        }
        if( strFieldName.equals("TempFeeNoType") ) {
            return 32;
        }
        if( strFieldName.equals("StandPrem") ) {
            return 33;
        }
        if( strFieldName.equals("Remark") ) {
            return 34;
        }
        if( strFieldName.equals("Distict") ) {
            return 35;
        }
        if( strFieldName.equals("Department") ) {
            return 36;
        }
        if( strFieldName.equals("BranchCode") ) {
            return 37;
        }
        if( strFieldName.equals("ContNo") ) {
            return 38;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "TempFeeID";
                break;
            case 1:
                strFieldName = "ShardingID";
                break;
            case 2:
                strFieldName = "TempFeeNo";
                break;
            case 3:
                strFieldName = "TempFeeType";
                break;
            case 4:
                strFieldName = "RiskCode";
                break;
            case 5:
                strFieldName = "PayIntv";
                break;
            case 6:
                strFieldName = "OtherNo";
                break;
            case 7:
                strFieldName = "OtherNoType";
                break;
            case 8:
                strFieldName = "PayMoney";
                break;
            case 9:
                strFieldName = "PayDate";
                break;
            case 10:
                strFieldName = "EnterAccDate";
                break;
            case 11:
                strFieldName = "ConfDate";
                break;
            case 12:
                strFieldName = "ConfMakeDate";
                break;
            case 13:
                strFieldName = "ConfMakeTime";
                break;
            case 14:
                strFieldName = "SaleChnl";
                break;
            case 15:
                strFieldName = "ManageCom";
                break;
            case 16:
                strFieldName = "PolicyCom";
                break;
            case 17:
                strFieldName = "AgentCom";
                break;
            case 18:
                strFieldName = "AgentType";
                break;
            case 19:
                strFieldName = "APPntName";
                break;
            case 20:
                strFieldName = "AgentGroup";
                break;
            case 21:
                strFieldName = "AgentCode";
                break;
            case 22:
                strFieldName = "ConfFlag";
                break;
            case 23:
                strFieldName = "SerialNo";
                break;
            case 24:
                strFieldName = "Operator";
                break;
            case 25:
                strFieldName = "State";
                break;
            case 26:
                strFieldName = "MakeTime";
                break;
            case 27:
                strFieldName = "MakeDate";
                break;
            case 28:
                strFieldName = "ModifyDate";
                break;
            case 29:
                strFieldName = "ModifyTime";
                break;
            case 30:
                strFieldName = "ContCom";
                break;
            case 31:
                strFieldName = "PayEndYear";
                break;
            case 32:
                strFieldName = "TempFeeNoType";
                break;
            case 33:
                strFieldName = "StandPrem";
                break;
            case 34:
                strFieldName = "Remark";
                break;
            case 35:
                strFieldName = "Distict";
                break;
            case 36:
                strFieldName = "Department";
                break;
            case 37:
                strFieldName = "BranchCode";
                break;
            case 38:
                strFieldName = "ContNo";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "TEMPFEEID":
                return Schema.TYPE_LONG;
            case "SHARDINGID":
                return Schema.TYPE_STRING;
            case "TEMPFEENO":
                return Schema.TYPE_STRING;
            case "TEMPFEETYPE":
                return Schema.TYPE_STRING;
            case "RISKCODE":
                return Schema.TYPE_STRING;
            case "PAYINTV":
                return Schema.TYPE_INT;
            case "OTHERNO":
                return Schema.TYPE_STRING;
            case "OTHERNOTYPE":
                return Schema.TYPE_STRING;
            case "PAYMONEY":
                return Schema.TYPE_DOUBLE;
            case "PAYDATE":
                return Schema.TYPE_STRING;
            case "ENTERACCDATE":
                return Schema.TYPE_STRING;
            case "CONFDATE":
                return Schema.TYPE_STRING;
            case "CONFMAKEDATE":
                return Schema.TYPE_STRING;
            case "CONFMAKETIME":
                return Schema.TYPE_STRING;
            case "SALECHNL":
                return Schema.TYPE_STRING;
            case "MANAGECOM":
                return Schema.TYPE_STRING;
            case "POLICYCOM":
                return Schema.TYPE_STRING;
            case "AGENTCOM":
                return Schema.TYPE_STRING;
            case "AGENTTYPE":
                return Schema.TYPE_STRING;
            case "APPNTNAME":
                return Schema.TYPE_STRING;
            case "AGENTGROUP":
                return Schema.TYPE_STRING;
            case "AGENTCODE":
                return Schema.TYPE_STRING;
            case "CONFFLAG":
                return Schema.TYPE_STRING;
            case "SERIALNO":
                return Schema.TYPE_STRING;
            case "OPERATOR":
                return Schema.TYPE_STRING;
            case "STATE":
                return Schema.TYPE_STRING;
            case "MAKETIME":
                return Schema.TYPE_STRING;
            case "MAKEDATE":
                return Schema.TYPE_STRING;
            case "MODIFYDATE":
                return Schema.TYPE_STRING;
            case "MODIFYTIME":
                return Schema.TYPE_STRING;
            case "CONTCOM":
                return Schema.TYPE_STRING;
            case "PAYENDYEAR":
                return Schema.TYPE_INT;
            case "TEMPFEENOTYPE":
                return Schema.TYPE_STRING;
            case "STANDPREM":
                return Schema.TYPE_DOUBLE;
            case "REMARK":
                return Schema.TYPE_STRING;
            case "DISTICT":
                return Schema.TYPE_STRING;
            case "DEPARTMENT":
                return Schema.TYPE_STRING;
            case "BRANCHCODE":
                return Schema.TYPE_STRING;
            case "CONTNO":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_LONG;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_INT;
            case 6:
                return Schema.TYPE_STRING;
            case 7:
                return Schema.TYPE_STRING;
            case 8:
                return Schema.TYPE_DOUBLE;
            case 9:
                return Schema.TYPE_STRING;
            case 10:
                return Schema.TYPE_STRING;
            case 11:
                return Schema.TYPE_STRING;
            case 12:
                return Schema.TYPE_STRING;
            case 13:
                return Schema.TYPE_STRING;
            case 14:
                return Schema.TYPE_STRING;
            case 15:
                return Schema.TYPE_STRING;
            case 16:
                return Schema.TYPE_STRING;
            case 17:
                return Schema.TYPE_STRING;
            case 18:
                return Schema.TYPE_STRING;
            case 19:
                return Schema.TYPE_STRING;
            case 20:
                return Schema.TYPE_STRING;
            case 21:
                return Schema.TYPE_STRING;
            case 22:
                return Schema.TYPE_STRING;
            case 23:
                return Schema.TYPE_STRING;
            case 24:
                return Schema.TYPE_STRING;
            case 25:
                return Schema.TYPE_STRING;
            case 26:
                return Schema.TYPE_STRING;
            case 27:
                return Schema.TYPE_STRING;
            case 28:
                return Schema.TYPE_STRING;
            case 29:
                return Schema.TYPE_STRING;
            case 30:
                return Schema.TYPE_STRING;
            case 31:
                return Schema.TYPE_INT;
            case 32:
                return Schema.TYPE_STRING;
            case 33:
                return Schema.TYPE_DOUBLE;
            case 34:
                return Schema.TYPE_STRING;
            case 35:
                return Schema.TYPE_STRING;
            case 36:
                return Schema.TYPE_STRING;
            case 37:
                return Schema.TYPE_STRING;
            case 38:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("TempFeeID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(TempFeeID));
        }
        if (FCode.equalsIgnoreCase("ShardingID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ShardingID));
        }
        if (FCode.equalsIgnoreCase("TempFeeNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(TempFeeNo));
        }
        if (FCode.equalsIgnoreCase("TempFeeType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(TempFeeType));
        }
        if (FCode.equalsIgnoreCase("RiskCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RiskCode));
        }
        if (FCode.equalsIgnoreCase("PayIntv")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PayIntv));
        }
        if (FCode.equalsIgnoreCase("OtherNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OtherNo));
        }
        if (FCode.equalsIgnoreCase("OtherNoType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OtherNoType));
        }
        if (FCode.equalsIgnoreCase("PayMoney")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PayMoney));
        }
        if (FCode.equalsIgnoreCase("PayDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PayDate));
        }
        if (FCode.equalsIgnoreCase("EnterAccDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(EnterAccDate));
        }
        if (FCode.equalsIgnoreCase("ConfDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ConfDate));
        }
        if (FCode.equalsIgnoreCase("ConfMakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ConfMakeDate));
        }
        if (FCode.equalsIgnoreCase("ConfMakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ConfMakeTime));
        }
        if (FCode.equalsIgnoreCase("SaleChnl")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SaleChnl));
        }
        if (FCode.equalsIgnoreCase("ManageCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
        }
        if (FCode.equalsIgnoreCase("PolicyCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PolicyCom));
        }
        if (FCode.equalsIgnoreCase("AgentCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentCom));
        }
        if (FCode.equalsIgnoreCase("AgentType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentType));
        }
        if (FCode.equalsIgnoreCase("APPntName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(APPntName));
        }
        if (FCode.equalsIgnoreCase("AgentGroup")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentGroup));
        }
        if (FCode.equalsIgnoreCase("AgentCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentCode));
        }
        if (FCode.equalsIgnoreCase("ConfFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ConfFlag));
        }
        if (FCode.equalsIgnoreCase("SerialNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SerialNo));
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equalsIgnoreCase("State")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(State));
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeDate));
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyDate));
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (FCode.equalsIgnoreCase("ContCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ContCom));
        }
        if (FCode.equalsIgnoreCase("PayEndYear")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PayEndYear));
        }
        if (FCode.equalsIgnoreCase("TempFeeNoType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(TempFeeNoType));
        }
        if (FCode.equalsIgnoreCase("StandPrem")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StandPrem));
        }
        if (FCode.equalsIgnoreCase("Remark")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Remark));
        }
        if (FCode.equalsIgnoreCase("Distict")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Distict));
        }
        if (FCode.equalsIgnoreCase("Department")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Department));
        }
        if (FCode.equalsIgnoreCase("BranchCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BranchCode));
        }
        if (FCode.equalsIgnoreCase("ContNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ContNo));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = String.valueOf(TempFeeID);
                break;
            case 1:
                strFieldValue = String.valueOf(ShardingID);
                break;
            case 2:
                strFieldValue = String.valueOf(TempFeeNo);
                break;
            case 3:
                strFieldValue = String.valueOf(TempFeeType);
                break;
            case 4:
                strFieldValue = String.valueOf(RiskCode);
                break;
            case 5:
                strFieldValue = String.valueOf(PayIntv);
                break;
            case 6:
                strFieldValue = String.valueOf(OtherNo);
                break;
            case 7:
                strFieldValue = String.valueOf(OtherNoType);
                break;
            case 8:
                strFieldValue = String.valueOf(PayMoney);
                break;
            case 9:
                strFieldValue = String.valueOf(PayDate);
                break;
            case 10:
                strFieldValue = String.valueOf(EnterAccDate);
                break;
            case 11:
                strFieldValue = String.valueOf(ConfDate);
                break;
            case 12:
                strFieldValue = String.valueOf(ConfMakeDate);
                break;
            case 13:
                strFieldValue = String.valueOf(ConfMakeTime);
                break;
            case 14:
                strFieldValue = String.valueOf(SaleChnl);
                break;
            case 15:
                strFieldValue = String.valueOf(ManageCom);
                break;
            case 16:
                strFieldValue = String.valueOf(PolicyCom);
                break;
            case 17:
                strFieldValue = String.valueOf(AgentCom);
                break;
            case 18:
                strFieldValue = String.valueOf(AgentType);
                break;
            case 19:
                strFieldValue = String.valueOf(APPntName);
                break;
            case 20:
                strFieldValue = String.valueOf(AgentGroup);
                break;
            case 21:
                strFieldValue = String.valueOf(AgentCode);
                break;
            case 22:
                strFieldValue = String.valueOf(ConfFlag);
                break;
            case 23:
                strFieldValue = String.valueOf(SerialNo);
                break;
            case 24:
                strFieldValue = String.valueOf(Operator);
                break;
            case 25:
                strFieldValue = String.valueOf(State);
                break;
            case 26:
                strFieldValue = String.valueOf(MakeTime);
                break;
            case 27:
                strFieldValue = String.valueOf(MakeDate);
                break;
            case 28:
                strFieldValue = String.valueOf(ModifyDate);
                break;
            case 29:
                strFieldValue = String.valueOf(ModifyTime);
                break;
            case 30:
                strFieldValue = String.valueOf(ContCom);
                break;
            case 31:
                strFieldValue = String.valueOf(PayEndYear);
                break;
            case 32:
                strFieldValue = String.valueOf(TempFeeNoType);
                break;
            case 33:
                strFieldValue = String.valueOf(StandPrem);
                break;
            case 34:
                strFieldValue = String.valueOf(Remark);
                break;
            case 35:
                strFieldValue = String.valueOf(Distict);
                break;
            case 36:
                strFieldValue = String.valueOf(Department);
                break;
            case 37:
                strFieldValue = String.valueOf(BranchCode);
                break;
            case 38:
                strFieldValue = String.valueOf(ContNo);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("TempFeeID")) {
            if( FValue != null && !FValue.equals("")) {
                TempFeeID = new Long(FValue).longValue();
            }
        }
        if (FCode.equalsIgnoreCase("ShardingID")) {
            if( FValue != null && !FValue.equals(""))
            {
                ShardingID = FValue.trim();
            }
            else
                ShardingID = null;
        }
        if (FCode.equalsIgnoreCase("TempFeeNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                TempFeeNo = FValue.trim();
            }
            else
                TempFeeNo = null;
        }
        if (FCode.equalsIgnoreCase("TempFeeType")) {
            if( FValue != null && !FValue.equals(""))
            {
                TempFeeType = FValue.trim();
            }
            else
                TempFeeType = null;
        }
        if (FCode.equalsIgnoreCase("RiskCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                RiskCode = FValue.trim();
            }
            else
                RiskCode = null;
        }
        if (FCode.equalsIgnoreCase("PayIntv")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                PayIntv = i;
            }
        }
        if (FCode.equalsIgnoreCase("OtherNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                OtherNo = FValue.trim();
            }
            else
                OtherNo = null;
        }
        if (FCode.equalsIgnoreCase("OtherNoType")) {
            if( FValue != null && !FValue.equals(""))
            {
                OtherNoType = FValue.trim();
            }
            else
                OtherNoType = null;
        }
        if (FCode.equalsIgnoreCase("PayMoney")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                PayMoney = d;
            }
        }
        if (FCode.equalsIgnoreCase("PayDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                PayDate = FValue.trim();
            }
            else
                PayDate = null;
        }
        if (FCode.equalsIgnoreCase("EnterAccDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                EnterAccDate = FValue.trim();
            }
            else
                EnterAccDate = null;
        }
        if (FCode.equalsIgnoreCase("ConfDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                ConfDate = FValue.trim();
            }
            else
                ConfDate = null;
        }
        if (FCode.equalsIgnoreCase("ConfMakeDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                ConfMakeDate = FValue.trim();
            }
            else
                ConfMakeDate = null;
        }
        if (FCode.equalsIgnoreCase("ConfMakeTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                ConfMakeTime = FValue.trim();
            }
            else
                ConfMakeTime = null;
        }
        if (FCode.equalsIgnoreCase("SaleChnl")) {
            if( FValue != null && !FValue.equals(""))
            {
                SaleChnl = FValue.trim();
            }
            else
                SaleChnl = null;
        }
        if (FCode.equalsIgnoreCase("ManageCom")) {
            if( FValue != null && !FValue.equals(""))
            {
                ManageCom = FValue.trim();
            }
            else
                ManageCom = null;
        }
        if (FCode.equalsIgnoreCase("PolicyCom")) {
            if( FValue != null && !FValue.equals(""))
            {
                PolicyCom = FValue.trim();
            }
            else
                PolicyCom = null;
        }
        if (FCode.equalsIgnoreCase("AgentCom")) {
            if( FValue != null && !FValue.equals(""))
            {
                AgentCom = FValue.trim();
            }
            else
                AgentCom = null;
        }
        if (FCode.equalsIgnoreCase("AgentType")) {
            if( FValue != null && !FValue.equals(""))
            {
                AgentType = FValue.trim();
            }
            else
                AgentType = null;
        }
        if (FCode.equalsIgnoreCase("APPntName")) {
            if( FValue != null && !FValue.equals(""))
            {
                APPntName = FValue.trim();
            }
            else
                APPntName = null;
        }
        if (FCode.equalsIgnoreCase("AgentGroup")) {
            if( FValue != null && !FValue.equals(""))
            {
                AgentGroup = FValue.trim();
            }
            else
                AgentGroup = null;
        }
        if (FCode.equalsIgnoreCase("AgentCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                AgentCode = FValue.trim();
            }
            else
                AgentCode = null;
        }
        if (FCode.equalsIgnoreCase("ConfFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                ConfFlag = FValue.trim();
            }
            else
                ConfFlag = null;
        }
        if (FCode.equalsIgnoreCase("SerialNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                SerialNo = FValue.trim();
            }
            else
                SerialNo = null;
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            if( FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
                Operator = null;
        }
        if (FCode.equalsIgnoreCase("State")) {
            if( FValue != null && !FValue.equals(""))
            {
                State = FValue.trim();
            }
            else
                State = null;
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
                MakeTime = null;
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeDate = FValue.trim();
            }
            else
                MakeDate = null;
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyDate = FValue.trim();
            }
            else
                ModifyDate = null;
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
                ModifyTime = null;
        }
        if (FCode.equalsIgnoreCase("ContCom")) {
            if( FValue != null && !FValue.equals(""))
            {
                ContCom = FValue.trim();
            }
            else
                ContCom = null;
        }
        if (FCode.equalsIgnoreCase("PayEndYear")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                PayEndYear = i;
            }
        }
        if (FCode.equalsIgnoreCase("TempFeeNoType")) {
            if( FValue != null && !FValue.equals(""))
            {
                TempFeeNoType = FValue.trim();
            }
            else
                TempFeeNoType = null;
        }
        if (FCode.equalsIgnoreCase("StandPrem")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                StandPrem = d;
            }
        }
        if (FCode.equalsIgnoreCase("Remark")) {
            if( FValue != null && !FValue.equals(""))
            {
                Remark = FValue.trim();
            }
            else
                Remark = null;
        }
        if (FCode.equalsIgnoreCase("Distict")) {
            if( FValue != null && !FValue.equals(""))
            {
                Distict = FValue.trim();
            }
            else
                Distict = null;
        }
        if (FCode.equalsIgnoreCase("Department")) {
            if( FValue != null && !FValue.equals(""))
            {
                Department = FValue.trim();
            }
            else
                Department = null;
        }
        if (FCode.equalsIgnoreCase("BranchCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                BranchCode = FValue.trim();
            }
            else
                BranchCode = null;
        }
        if (FCode.equalsIgnoreCase("ContNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                ContNo = FValue.trim();
            }
            else
                ContNo = null;
        }
        return true;
    }


    public String toString() {
    return "LKTempFeePojo [" +
            "TempFeeID="+TempFeeID +
            ", ShardingID="+ShardingID +
            ", TempFeeNo="+TempFeeNo +
            ", TempFeeType="+TempFeeType +
            ", RiskCode="+RiskCode +
            ", PayIntv="+PayIntv +
            ", OtherNo="+OtherNo +
            ", OtherNoType="+OtherNoType +
            ", PayMoney="+PayMoney +
            ", PayDate="+PayDate +
            ", EnterAccDate="+EnterAccDate +
            ", ConfDate="+ConfDate +
            ", ConfMakeDate="+ConfMakeDate +
            ", ConfMakeTime="+ConfMakeTime +
            ", SaleChnl="+SaleChnl +
            ", ManageCom="+ManageCom +
            ", PolicyCom="+PolicyCom +
            ", AgentCom="+AgentCom +
            ", AgentType="+AgentType +
            ", APPntName="+APPntName +
            ", AgentGroup="+AgentGroup +
            ", AgentCode="+AgentCode +
            ", ConfFlag="+ConfFlag +
            ", SerialNo="+SerialNo +
            ", Operator="+Operator +
            ", State="+State +
            ", MakeTime="+MakeTime +
            ", MakeDate="+MakeDate +
            ", ModifyDate="+ModifyDate +
            ", ModifyTime="+ModifyTime +
            ", ContCom="+ContCom +
            ", PayEndYear="+PayEndYear +
            ", TempFeeNoType="+TempFeeNoType +
            ", StandPrem="+StandPrem +
            ", Remark="+Remark +
            ", Distict="+Distict +
            ", Department="+Department +
            ", BranchCode="+BranchCode +
            ", ContNo="+ContNo +"]";
    }
}
