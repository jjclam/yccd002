/**
 * Copyright (c) 2019 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.entity;

import java.io.*;
import java.util.Date;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.StrTool;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * <p>ClassName: LdJobRunLogPojo </p>
 * <p>Copyright: Copyright (c) 2019</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2019-11-23
 */
public class LdJobRunLogPojo implements  Pojo,Serializable {
    // @Field
    /** 序号 */
    private long SerialNo; 
    /** 基本任务名称 */
    private String TaskName; 
    /** 基本任务编码 */
    private String TaskCode; 
    /** 执行日期 */
    private String  ExecuteDate;
    /** 执行时间 */
    private String ExecuteTime; 
    /** 完成日期 */
    private String  FinishDate;
    /** 完成时间 */
    private String FinishTime; 
    /** 执行次数 */
    private long ExecuteCount; 
    /** 执行状态 */
    private String ExecuteState; 
    /** 执行结果 */
    private String ExecuteResult; 
    /** 备用字段1 */
    private String Remark1; 
    /** 备用字段2 */
    private String Remark2; 


    public static final int FIELDNUM = 12;    // 数据库表的字段个数

    private FDate fDate = new FDate();        // 处理日期
    public long getSerialNo() {
        return SerialNo;
    }
    public void setSerialNo(long aSerialNo) {
        SerialNo = aSerialNo;
    }
    public void setSerialNo(String aSerialNo) {
        if (aSerialNo != null && !aSerialNo.equals("")) {
            SerialNo = new Long(aSerialNo).longValue();
        }
    }

    public String getTaskName() {
        return TaskName;
    }
    public void setTaskName(String aTaskName) {
        TaskName = aTaskName;
    }
    public String getTaskCode() {
        return TaskCode;
    }
    public void setTaskCode(String aTaskCode) {
        TaskCode = aTaskCode;
    }
    public String getExecuteDate() {
        return ExecuteDate;
    }
    public void setExecuteDate(String aExecuteDate) {
        ExecuteDate = aExecuteDate;
    }
    public String getExecuteTime() {
        return ExecuteTime;
    }
    public void setExecuteTime(String aExecuteTime) {
        ExecuteTime = aExecuteTime;
    }
    public String getFinishDate() {
        return FinishDate;
    }
    public void setFinishDate(String aFinishDate) {
        FinishDate = aFinishDate;
    }
    public String getFinishTime() {
        return FinishTime;
    }
    public void setFinishTime(String aFinishTime) {
        FinishTime = aFinishTime;
    }
    public long getExecuteCount() {
        return ExecuteCount;
    }
    public void setExecuteCount(long aExecuteCount) {
        ExecuteCount = aExecuteCount;
    }
    public void setExecuteCount(String aExecuteCount) {
        if (aExecuteCount != null && !aExecuteCount.equals("")) {
            ExecuteCount = new Long(aExecuteCount).longValue();
        }
    }

    public String getExecuteState() {
        return ExecuteState;
    }
    public void setExecuteState(String aExecuteState) {
        ExecuteState = aExecuteState;
    }
    public String getExecuteResult() {
        return ExecuteResult;
    }
    public void setExecuteResult(String aExecuteResult) {
        ExecuteResult = aExecuteResult;
    }
    public String getRemark1() {
        return Remark1;
    }
    public void setRemark1(String aRemark1) {
        Remark1 = aRemark1;
    }
    public String getRemark2() {
        return Remark2;
    }
    public void setRemark2(String aRemark2) {
        Remark2 = aRemark2;
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("SerialNo") ) {
            return 0;
        }
        if( strFieldName.equals("TaskName") ) {
            return 1;
        }
        if( strFieldName.equals("TaskCode") ) {
            return 2;
        }
        if( strFieldName.equals("ExecuteDate") ) {
            return 3;
        }
        if( strFieldName.equals("ExecuteTime") ) {
            return 4;
        }
        if( strFieldName.equals("FinishDate") ) {
            return 5;
        }
        if( strFieldName.equals("FinishTime") ) {
            return 6;
        }
        if( strFieldName.equals("ExecuteCount") ) {
            return 7;
        }
        if( strFieldName.equals("ExecuteState") ) {
            return 8;
        }
        if( strFieldName.equals("ExecuteResult") ) {
            return 9;
        }
        if( strFieldName.equals("Remark1") ) {
            return 10;
        }
        if( strFieldName.equals("Remark2") ) {
            return 11;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "SerialNo";
                break;
            case 1:
                strFieldName = "TaskName";
                break;
            case 2:
                strFieldName = "TaskCode";
                break;
            case 3:
                strFieldName = "ExecuteDate";
                break;
            case 4:
                strFieldName = "ExecuteTime";
                break;
            case 5:
                strFieldName = "FinishDate";
                break;
            case 6:
                strFieldName = "FinishTime";
                break;
            case 7:
                strFieldName = "ExecuteCount";
                break;
            case 8:
                strFieldName = "ExecuteState";
                break;
            case 9:
                strFieldName = "ExecuteResult";
                break;
            case 10:
                strFieldName = "Remark1";
                break;
            case 11:
                strFieldName = "Remark2";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "SERIALNO":
                return Schema.TYPE_LONG;
            case "TASKNAME":
                return Schema.TYPE_STRING;
            case "TASKCODE":
                return Schema.TYPE_STRING;
            case "EXECUTEDATE":
                return Schema.TYPE_STRING;
            case "EXECUTETIME":
                return Schema.TYPE_STRING;
            case "FINISHDATE":
                return Schema.TYPE_STRING;
            case "FINISHTIME":
                return Schema.TYPE_STRING;
            case "EXECUTECOUNT":
                return Schema.TYPE_LONG;
            case "EXECUTESTATE":
                return Schema.TYPE_STRING;
            case "EXECUTERESULT":
                return Schema.TYPE_STRING;
            case "REMARK1":
                return Schema.TYPE_STRING;
            case "REMARK2":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_LONG;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_STRING;
            case 6:
                return Schema.TYPE_STRING;
            case 7:
                return Schema.TYPE_LONG;
            case 8:
                return Schema.TYPE_STRING;
            case 9:
                return Schema.TYPE_STRING;
            case 10:
                return Schema.TYPE_STRING;
            case 11:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("SerialNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SerialNo));
        }
        if (FCode.equalsIgnoreCase("TaskName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(TaskName));
        }
        if (FCode.equalsIgnoreCase("TaskCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(TaskCode));
        }
        if (FCode.equalsIgnoreCase("ExecuteDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ExecuteDate));
        }
        if (FCode.equalsIgnoreCase("ExecuteTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ExecuteTime));
        }
        if (FCode.equalsIgnoreCase("FinishDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FinishDate));
        }
        if (FCode.equalsIgnoreCase("FinishTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FinishTime));
        }
        if (FCode.equalsIgnoreCase("ExecuteCount")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ExecuteCount));
        }
        if (FCode.equalsIgnoreCase("ExecuteState")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ExecuteState));
        }
        if (FCode.equalsIgnoreCase("ExecuteResult")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ExecuteResult));
        }
        if (FCode.equalsIgnoreCase("Remark1")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Remark1));
        }
        if (FCode.equalsIgnoreCase("Remark2")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Remark2));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = String.valueOf(SerialNo);
                break;
            case 1:
                strFieldValue = String.valueOf(TaskName);
                break;
            case 2:
                strFieldValue = String.valueOf(TaskCode);
                break;
            case 3:
                strFieldValue = String.valueOf(ExecuteDate);
                break;
            case 4:
                strFieldValue = String.valueOf(ExecuteTime);
                break;
            case 5:
                strFieldValue = String.valueOf(FinishDate);
                break;
            case 6:
                strFieldValue = String.valueOf(FinishTime);
                break;
            case 7:
                strFieldValue = String.valueOf(ExecuteCount);
                break;
            case 8:
                strFieldValue = String.valueOf(ExecuteState);
                break;
            case 9:
                strFieldValue = String.valueOf(ExecuteResult);
                break;
            case 10:
                strFieldValue = String.valueOf(Remark1);
                break;
            case 11:
                strFieldValue = String.valueOf(Remark2);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("SerialNo")) {
            if( FValue != null && !FValue.equals("")) {
                SerialNo = new Long(FValue).longValue();
            }
        }
        if (FCode.equalsIgnoreCase("TaskName")) {
            if( FValue != null && !FValue.equals(""))
            {
                TaskName = FValue.trim();
            }
            else
                TaskName = null;
        }
        if (FCode.equalsIgnoreCase("TaskCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                TaskCode = FValue.trim();
            }
            else
                TaskCode = null;
        }
        if (FCode.equalsIgnoreCase("ExecuteDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                ExecuteDate = FValue.trim();
            }
            else
                ExecuteDate = null;
        }
        if (FCode.equalsIgnoreCase("ExecuteTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                ExecuteTime = FValue.trim();
            }
            else
                ExecuteTime = null;
        }
        if (FCode.equalsIgnoreCase("FinishDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                FinishDate = FValue.trim();
            }
            else
                FinishDate = null;
        }
        if (FCode.equalsIgnoreCase("FinishTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                FinishTime = FValue.trim();
            }
            else
                FinishTime = null;
        }
        if (FCode.equalsIgnoreCase("ExecuteCount")) {
            if( FValue != null && !FValue.equals("")) {
                ExecuteCount = new Long(FValue).longValue();
            }
        }
        if (FCode.equalsIgnoreCase("ExecuteState")) {
            if( FValue != null && !FValue.equals(""))
            {
                ExecuteState = FValue.trim();
            }
            else
                ExecuteState = null;
        }
        if (FCode.equalsIgnoreCase("ExecuteResult")) {
            if( FValue != null && !FValue.equals(""))
            {
                ExecuteResult = FValue.trim();
            }
            else
                ExecuteResult = null;
        }
        if (FCode.equalsIgnoreCase("Remark1")) {
            if( FValue != null && !FValue.equals(""))
            {
                Remark1 = FValue.trim();
            }
            else
                Remark1 = null;
        }
        if (FCode.equalsIgnoreCase("Remark2")) {
            if( FValue != null && !FValue.equals(""))
            {
                Remark2 = FValue.trim();
            }
            else
                Remark2 = null;
        }
        return true;
    }


    public String toString() {
    return "LdJobRunLogPojo [" +
            "SerialNo="+SerialNo +
            ", TaskName="+TaskName +
            ", TaskCode="+TaskCode +
            ", ExecuteDate="+ExecuteDate +
            ", ExecuteTime="+ExecuteTime +
            ", FinishDate="+FinishDate +
            ", FinishTime="+FinishTime +
            ", ExecuteCount="+ExecuteCount +
            ", ExecuteState="+ExecuteState +
            ", ExecuteResult="+ExecuteResult +
            ", Remark1="+Remark1 +
            ", Remark2="+Remark2 +"]";
    }
}
