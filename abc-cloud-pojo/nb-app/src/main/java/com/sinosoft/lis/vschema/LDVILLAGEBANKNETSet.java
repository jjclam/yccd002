/**
 * Copyright (c) 2019 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.vschema;

import com.sinosoft.lis.schema.LDVILLAGEBANKNETSchema;
import com.sinosoft.utility.*;

/**
 * <p>ClassName: LDVILLAGEBANKNETSet </p>
 * <p>Description: LDVILLAGEBANKNETSchemaSet类文件 </p>
 * <p>Copyright: Copyright (c) 2019</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2019-01-09
 */
public class LDVILLAGEBANKNETSet extends SchemaSet {
	// @Method
	public boolean add(LDVILLAGEBANKNETSchema aSchema) {
		return super.add(aSchema);
	}

	public boolean add(LDVILLAGEBANKNETSet aSet) {
		return super.add(aSet);
	}

	public boolean remove(LDVILLAGEBANKNETSchema aSchema) {
		return super.remove(aSchema);
	}

	public LDVILLAGEBANKNETSchema get(int index) {
		LDVILLAGEBANKNETSchema tSchema = (LDVILLAGEBANKNETSchema)super.getObj(index);
		return tSchema;
	}

	public boolean set(int index, LDVILLAGEBANKNETSchema aSchema) {
		return super.set(index,aSchema);
	}

	public boolean set(LDVILLAGEBANKNETSet aSet) {
		return super.set(aSet);
	}

	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLDVILLAGEBANKNET描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode() {
		StringBuffer strReturn = new StringBuffer("");
		int n = this.size();
		for (int i = 1; i <= n; i++) {
			LDVILLAGEBANKNETSchema aSchema = this.get(i);
			strReturn.append(aSchema.encode());
			if( i != n ) strReturn.append(SysConst.RECORDSPLITER);
		}

		return strReturn.toString();
	}

	/**
	* 数据解包
	* @param: str String 打包后字符串
	* @return: boolean
	**/
	public boolean decode( String str ) {
		int nBeginPos = 0;
		int nEndPos = str.indexOf('^');
		this.clear();

		while( nEndPos != -1 ) {
			LDVILLAGEBANKNETSchema aSchema = new LDVILLAGEBANKNETSchema();
			if (aSchema.decode(str.substring(nBeginPos, nEndPos))) {
			    this.add(aSchema);
			    nBeginPos = nEndPos + 1;
			    nEndPos = str.indexOf('^', nEndPos + 1);
			} else {
				// @@错误处理
				this.mErrors.copyAllErrors( aSchema.mErrors );
				return false;
			}
		}
		LDVILLAGEBANKNETSchema tSchema = new LDVILLAGEBANKNETSchema();
		if(tSchema.decode(str.substring(nBeginPos))) {
		    this.add(tSchema);
		    return true;
		} else {
			// @@错误处理
			this.mErrors.copyAllErrors( tSchema.mErrors );
			return false;
		}
	}

}
