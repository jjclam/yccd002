/**
 * Copyright (c) 2017 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.entity;

import com.sinosoft.utility.Pojo;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;

import java.io.Serializable;

/**
 * <p>ClassName: LMClaimCtrlPeriodPojo </p>
 * <p>Copyright: Copyright (c) 2017</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2017-10-17
 */
public class LMClaimCtrlPeriodPojo implements Pojo,Serializable {
    // @Field
    /** 理赔控制编号 */
    private String ClaimCtrlCode; 
    /** 起始期间 */
    private int ClmPeriodStart; 
    /** 起始期间单位 */
    private String ClmPeriodStartFlag; 
    /** 期间间隔 */
    private int ClmPeriodInterval; 
    /** 期间间隔单位 */
    private String ClmPeriodFlag; 
    /** 赔付金额计算sql */
    private String CalCode; 
    /** 赔付金额类型 */
    private String CalResultType; 
    /** 赔付金额默认值 */
    private double DefaultValue; 
    /** 赔付金额计算方式 */
    private String CalCtrlFlag; 


    public static final int FIELDNUM = 9;    // 数据库表的字段个数
    public String getClaimCtrlCode() {
        return ClaimCtrlCode;
    }
    public void setClaimCtrlCode(String aClaimCtrlCode) {
        ClaimCtrlCode = aClaimCtrlCode;
    }
    public int getClmPeriodStart() {
        return ClmPeriodStart;
    }
    public void setClmPeriodStart(int aClmPeriodStart) {
        ClmPeriodStart = aClmPeriodStart;
    }
    public void setClmPeriodStart(String aClmPeriodStart) {
        if (aClmPeriodStart != null && !aClmPeriodStart.equals("")) {
            Integer tInteger = new Integer(aClmPeriodStart);
            int i = tInteger.intValue();
            ClmPeriodStart = i;
        }
    }

    public String getClmPeriodStartFlag() {
        return ClmPeriodStartFlag;
    }
    public void setClmPeriodStartFlag(String aClmPeriodStartFlag) {
        ClmPeriodStartFlag = aClmPeriodStartFlag;
    }
    public int getClmPeriodInterval() {
        return ClmPeriodInterval;
    }
    public void setClmPeriodInterval(int aClmPeriodInterval) {
        ClmPeriodInterval = aClmPeriodInterval;
    }
    public void setClmPeriodInterval(String aClmPeriodInterval) {
        if (aClmPeriodInterval != null && !aClmPeriodInterval.equals("")) {
            Integer tInteger = new Integer(aClmPeriodInterval);
            int i = tInteger.intValue();
            ClmPeriodInterval = i;
        }
    }

    public String getClmPeriodFlag() {
        return ClmPeriodFlag;
    }
    public void setClmPeriodFlag(String aClmPeriodFlag) {
        ClmPeriodFlag = aClmPeriodFlag;
    }
    public String getCalCode() {
        return CalCode;
    }
    public void setCalCode(String aCalCode) {
        CalCode = aCalCode;
    }
    public String getCalResultType() {
        return CalResultType;
    }
    public void setCalResultType(String aCalResultType) {
        CalResultType = aCalResultType;
    }
    public double getDefaultValue() {
        return DefaultValue;
    }
    public void setDefaultValue(double aDefaultValue) {
        DefaultValue = aDefaultValue;
    }
    public void setDefaultValue(String aDefaultValue) {
        if (aDefaultValue != null && !aDefaultValue.equals("")) {
            Double tDouble = new Double(aDefaultValue);
            double d = tDouble.doubleValue();
            DefaultValue = d;
        }
    }

    public String getCalCtrlFlag() {
        return CalCtrlFlag;
    }
    public void setCalCtrlFlag(String aCalCtrlFlag) {
        CalCtrlFlag = aCalCtrlFlag;
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("ClaimCtrlCode") ) {
            return 0;
        }
        if( strFieldName.equals("ClmPeriodStart") ) {
            return 1;
        }
        if( strFieldName.equals("ClmPeriodStartFlag") ) {
            return 2;
        }
        if( strFieldName.equals("ClmPeriodInterval") ) {
            return 3;
        }
        if( strFieldName.equals("ClmPeriodFlag") ) {
            return 4;
        }
        if( strFieldName.equals("CalCode") ) {
            return 5;
        }
        if( strFieldName.equals("CalResultType") ) {
            return 6;
        }
        if( strFieldName.equals("DefaultValue") ) {
            return 7;
        }
        if( strFieldName.equals("CalCtrlFlag") ) {
            return 8;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "ClaimCtrlCode";
                break;
            case 1:
                strFieldName = "ClmPeriodStart";
                break;
            case 2:
                strFieldName = "ClmPeriodStartFlag";
                break;
            case 3:
                strFieldName = "ClmPeriodInterval";
                break;
            case 4:
                strFieldName = "ClmPeriodFlag";
                break;
            case 5:
                strFieldName = "CalCode";
                break;
            case 6:
                strFieldName = "CalResultType";
                break;
            case 7:
                strFieldName = "DefaultValue";
                break;
            case 8:
                strFieldName = "CalCtrlFlag";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "CLAIMCTRLCODE":
                return Schema.TYPE_STRING;
            case "CLMPERIODSTART":
                return Schema.TYPE_INT;
            case "CLMPERIODSTARTFLAG":
                return Schema.TYPE_STRING;
            case "CLMPERIODINTERVAL":
                return Schema.TYPE_INT;
            case "CLMPERIODFLAG":
                return Schema.TYPE_STRING;
            case "CALCODE":
                return Schema.TYPE_STRING;
            case "CALRESULTTYPE":
                return Schema.TYPE_STRING;
            case "DEFAULTVALUE":
                return Schema.TYPE_DOUBLE;
            case "CALCTRLFLAG":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_STRING;
            case 1:
                return Schema.TYPE_INT;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_INT;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_STRING;
            case 6:
                return Schema.TYPE_STRING;
            case 7:
                return Schema.TYPE_DOUBLE;
            case 8:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("ClaimCtrlCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ClaimCtrlCode));
        }
        if (FCode.equalsIgnoreCase("ClmPeriodStart")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ClmPeriodStart));
        }
        if (FCode.equalsIgnoreCase("ClmPeriodStartFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ClmPeriodStartFlag));
        }
        if (FCode.equalsIgnoreCase("ClmPeriodInterval")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ClmPeriodInterval));
        }
        if (FCode.equalsIgnoreCase("ClmPeriodFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ClmPeriodFlag));
        }
        if (FCode.equalsIgnoreCase("CalCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CalCode));
        }
        if (FCode.equalsIgnoreCase("CalResultType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CalResultType));
        }
        if (FCode.equalsIgnoreCase("DefaultValue")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DefaultValue));
        }
        if (FCode.equalsIgnoreCase("CalCtrlFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CalCtrlFlag));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = String.valueOf(ClaimCtrlCode);
                break;
            case 1:
                strFieldValue = String.valueOf(ClmPeriodStart);
                break;
            case 2:
                strFieldValue = String.valueOf(ClmPeriodStartFlag);
                break;
            case 3:
                strFieldValue = String.valueOf(ClmPeriodInterval);
                break;
            case 4:
                strFieldValue = String.valueOf(ClmPeriodFlag);
                break;
            case 5:
                strFieldValue = String.valueOf(CalCode);
                break;
            case 6:
                strFieldValue = String.valueOf(CalResultType);
                break;
            case 7:
                strFieldValue = String.valueOf(DefaultValue);
                break;
            case 8:
                strFieldValue = String.valueOf(CalCtrlFlag);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("ClaimCtrlCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                ClaimCtrlCode = FValue.trim();
            }
            else
                ClaimCtrlCode = null;
        }
        if (FCode.equalsIgnoreCase("ClmPeriodStart")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                ClmPeriodStart = i;
            }
        }
        if (FCode.equalsIgnoreCase("ClmPeriodStartFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                ClmPeriodStartFlag = FValue.trim();
            }
            else
                ClmPeriodStartFlag = null;
        }
        if (FCode.equalsIgnoreCase("ClmPeriodInterval")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                ClmPeriodInterval = i;
            }
        }
        if (FCode.equalsIgnoreCase("ClmPeriodFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                ClmPeriodFlag = FValue.trim();
            }
            else
                ClmPeriodFlag = null;
        }
        if (FCode.equalsIgnoreCase("CalCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                CalCode = FValue.trim();
            }
            else
                CalCode = null;
        }
        if (FCode.equalsIgnoreCase("CalResultType")) {
            if( FValue != null && !FValue.equals(""))
            {
                CalResultType = FValue.trim();
            }
            else
                CalResultType = null;
        }
        if (FCode.equalsIgnoreCase("DefaultValue")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                DefaultValue = d;
            }
        }
        if (FCode.equalsIgnoreCase("CalCtrlFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                CalCtrlFlag = FValue.trim();
            }
            else
                CalCtrlFlag = null;
        }
        return true;
    }


    public String toString() {
    return "LMClaimCtrlPeriodPojo [" +
            "ClaimCtrlCode="+ClaimCtrlCode +
            ", ClmPeriodStart="+ClmPeriodStart +
            ", ClmPeriodStartFlag="+ClmPeriodStartFlag +
            ", ClmPeriodInterval="+ClmPeriodInterval +
            ", ClmPeriodFlag="+ClmPeriodFlag +
            ", CalCode="+CalCode +
            ", CalResultType="+CalResultType +
            ", DefaultValue="+DefaultValue +
            ", CalCtrlFlag="+CalCtrlFlag +"]";
    }
}
