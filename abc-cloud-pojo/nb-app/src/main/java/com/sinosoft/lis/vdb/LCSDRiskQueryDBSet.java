/**
 * Copyright (c) 2017 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.vdb;

import com.sinosoft.lis.vschema.LCSDRiskQuerySet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.DBConnPool;
import com.sinosoft.utility.DBOper;
import com.sinosoft.utility.ExceptionUtils;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;

/**
 * <p>ClassName: LCSDRiskQueryDBSet </p>
 * <p>Description: DB层多记录数据库操作类文件 </p>
 * <p>Copyright: Copyright (c) 2017</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2017-10-21
 */
public class LCSDRiskQueryDBSet extends LCSDRiskQuerySet {
    // @Field
    private Connection con;
    private DBOper db;
    /**
    * flag = true: 传入Connection
    * flag = false: 不传入Connection
    **/
    private boolean mflag = false;

    // @Constructor
    public LCSDRiskQueryDBSet(Connection tConnection) {
        con = tConnection;
        db = new DBOper(con,"LCSDRiskQuery");
        mflag = true;
    }

    public LCSDRiskQueryDBSet() {
        db = new DBOper( "LCSDRiskQuery" );
    }
    // @Method
    public boolean deleteSQL() {
        if (db.deleteSQL(this)) {
            return true;
        } else {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LCSDRiskQueryDBSet";
            tError.functionName = "deleteSQL";
            tError.errorMessage = "操作失败!";
            this.mErrors .addOneError(tError);
            return false;
        }
    }

    /**
     * 删除操作
     * 删除条件：主键
     * @return boolean
     */
    public boolean delete() {
        PreparedStatement pstmt = null;

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try {
            int tCount = this.size();
            pstmt = con.prepareStatement("DELETE FROM LCSDRiskQuery WHERE  1=1  AND SerialNo = ?");
            for (int i = 1; i <= tCount; i++) {
            if(this.get(i).getSerialNo() == null || this.get(i).getSerialNo().equals("null")) {
            	pstmt.setString(1,null);
            } else {
            	pstmt.setString(1, this.get(i).getSerialNo());
            }
                pstmt.addBatch();
            }
            pstmt.executeBatch();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            ex.printStackTrace();
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LCSDRiskQueryDBSet";
            tError.functionName = "delete()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            try {
                pstmt.close();
            } catch (Exception e){
                e.printStackTrace();
            }

            if (!mflag) {
                try {
                    con.close();
                } catch (Exception e){e.printStackTrace();}
            }

            return false;
        }

        if( !mflag ) {
            try {
                con.close();
            } catch (Exception e){e.printStackTrace();}
        }

        return true;
    }

    /**
     * 更新操作
     * 更新条件：主键
     * @return boolean
     */
    public boolean update() {
        PreparedStatement pstmt = null;

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try {
            int tCount = this.size();
            pstmt = con.prepareStatement("UPDATE LCSDRiskQuery SET  SerialNo = ? , CustomerNo = ? , BussType = ? , BussNo = ? , ConType = ? , Name = ? , Sex = ? , Birthday = ? , IdType = ? , IdNo = ? , IsRisk = ? , LossTimes = ? , SumClaimAmount = ? , RefuseTimes = ? , CompanyCode = ? , LossDate = ? , LossResult = ? , ClaimAmount = ? , CompanyQuantity = ? , PolicyQuantity = ? , SumAmount = ? , IsRiskPerson = ? , MakeDate = ? , MakeTime = ? , ModifyDate = ? , ModifyTime = ? WHERE  1=1  AND SerialNo = ?");
            for (int i = 1; i <= tCount; i++) {
            if(this.get(i).getSerialNo() == null || this.get(i).getSerialNo().equals("null")) {
            	pstmt.setString(1,null);
            } else {
            	pstmt.setString(1, this.get(i).getSerialNo());
            }
            if(this.get(i).getCustomerNo() == null || this.get(i).getCustomerNo().equals("null")) {
            	pstmt.setString(2,null);
            } else {
            	pstmt.setString(2, this.get(i).getCustomerNo());
            }
            if(this.get(i).getBussType() == null || this.get(i).getBussType().equals("null")) {
            	pstmt.setString(3,null);
            } else {
            	pstmt.setString(3, this.get(i).getBussType());
            }
            if(this.get(i).getBussNo() == null || this.get(i).getBussNo().equals("null")) {
            	pstmt.setString(4,null);
            } else {
            	pstmt.setString(4, this.get(i).getBussNo());
            }
            if(this.get(i).getConType() == null || this.get(i).getConType().equals("null")) {
            	pstmt.setString(5,null);
            } else {
            	pstmt.setString(5, this.get(i).getConType());
            }
            if(this.get(i).getName() == null || this.get(i).getName().equals("null")) {
            	pstmt.setString(6,null);
            } else {
            	pstmt.setString(6, this.get(i).getName());
            }
            if(this.get(i).getSex() == null || this.get(i).getSex().equals("null")) {
            	pstmt.setString(7,null);
            } else {
            	pstmt.setString(7, this.get(i).getSex());
            }
            if(this.get(i).getBirthday() == null || this.get(i).getBirthday().equals("null")) {
                pstmt.setDate(8,null);
            } else {
                pstmt.setDate(8, Date.valueOf(this.get(i).getBirthday()));
            }
            if(this.get(i).getIdType() == null || this.get(i).getIdType().equals("null")) {
            	pstmt.setString(9,null);
            } else {
            	pstmt.setString(9, this.get(i).getIdType());
            }
            if(this.get(i).getIdNo() == null || this.get(i).getIdNo().equals("null")) {
            	pstmt.setString(10,null);
            } else {
            	pstmt.setString(10, this.get(i).getIdNo());
            }
            if(this.get(i).getIsRisk() == null || this.get(i).getIsRisk().equals("null")) {
            	pstmt.setString(11,null);
            } else {
            	pstmt.setString(11, this.get(i).getIsRisk());
            }
            pstmt.setInt(12, this.get(i).getLossTimes());
            pstmt.setDouble(13, this.get(i).getSumClaimAmount());
            pstmt.setInt(14, this.get(i).getRefuseTimes());
            if(this.get(i).getCompanyCode() == null || this.get(i).getCompanyCode().equals("null")) {
            	pstmt.setString(15,null);
            } else {
            	pstmt.setString(15, this.get(i).getCompanyCode());
            }
            if(this.get(i).getLossDate() == null || this.get(i).getLossDate().equals("null")) {
                pstmt.setDate(16,null);
            } else {
                pstmt.setDate(16, Date.valueOf(this.get(i).getLossDate()));
            }
            if(this.get(i).getLossResult() == null || this.get(i).getLossResult().equals("null")) {
            	pstmt.setString(17,null);
            } else {
            	pstmt.setString(17, this.get(i).getLossResult());
            }
            pstmt.setDouble(18, this.get(i).getClaimAmount());
            pstmt.setInt(19, this.get(i).getCompanyQuantity());
            pstmt.setInt(20, this.get(i).getPolicyQuantity());
            pstmt.setDouble(21, this.get(i).getSumAmount());
            if(this.get(i).getIsRiskPerson() == null || this.get(i).getIsRiskPerson().equals("null")) {
            	pstmt.setString(22,null);
            } else {
            	pstmt.setString(22, this.get(i).getIsRiskPerson());
            }
            if(this.get(i).getMakeDate() == null || this.get(i).getMakeDate().equals("null")) {
                pstmt.setDate(23,null);
            } else {
                pstmt.setDate(23, Date.valueOf(this.get(i).getMakeDate()));
            }
            if(this.get(i).getMakeTime() == null || this.get(i).getMakeTime().equals("null")) {
            	pstmt.setString(24,null);
            } else {
            	pstmt.setString(24, this.get(i).getMakeTime());
            }
            if(this.get(i).getModifyDate() == null || this.get(i).getModifyDate().equals("null")) {
                pstmt.setDate(25,null);
            } else {
                pstmt.setDate(25, Date.valueOf(this.get(i).getModifyDate()));
            }
            if(this.get(i).getModifyTime() == null || this.get(i).getModifyTime().equals("null")) {
            	pstmt.setString(26,null);
            } else {
            	pstmt.setString(26, this.get(i).getModifyTime());
            }
            // set where condition
            if(this.get(i).getSerialNo() == null || this.get(i).getSerialNo().equals("null")) {
            	pstmt.setString(27,null);
            } else {
            	pstmt.setString(27, this.get(i).getSerialNo());
            }
                pstmt.addBatch();
            }
            pstmt.executeBatch();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            ex.printStackTrace();
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LCSDRiskQueryDBSet";
            tError.functionName = "update()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            try {
                pstmt.close();
            } catch (Exception e){e.printStackTrace();}

            if( !mflag ) {
                try {
                    con.close();
                } catch (Exception e){e.printStackTrace();}
            }

            return false;
        }

        if( !mflag ) {
            try {
                con.close();
            } catch (Exception e){e.printStackTrace();}
        }

        return true;
    }

    /**
     * 新增操作
     * @return boolean
     */
    public boolean insert() {
        PreparedStatement pstmt = null;

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try
        {
            int tCount = this.size();
            pstmt = con.prepareStatement("INSERT INTO LCSDRiskQuery VALUES( ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ?)");
            for (int i = 1; i <= tCount; i++) {
            if(this.get(i).getSerialNo() == null || this.get(i).getSerialNo().equals("null")) {
            	pstmt.setString(1,null);
            } else {
            	pstmt.setString(1, this.get(i).getSerialNo());
            }
            if(this.get(i).getCustomerNo() == null || this.get(i).getCustomerNo().equals("null")) {
            	pstmt.setString(2,null);
            } else {
            	pstmt.setString(2, this.get(i).getCustomerNo());
            }
            if(this.get(i).getBussType() == null || this.get(i).getBussType().equals("null")) {
            	pstmt.setString(3,null);
            } else {
            	pstmt.setString(3, this.get(i).getBussType());
            }
            if(this.get(i).getBussNo() == null || this.get(i).getBussNo().equals("null")) {
            	pstmt.setString(4,null);
            } else {
            	pstmt.setString(4, this.get(i).getBussNo());
            }
            if(this.get(i).getConType() == null || this.get(i).getConType().equals("null")) {
            	pstmt.setString(5,null);
            } else {
            	pstmt.setString(5, this.get(i).getConType());
            }
            if(this.get(i).getName() == null || this.get(i).getName().equals("null")) {
            	pstmt.setString(6,null);
            } else {
            	pstmt.setString(6, this.get(i).getName());
            }
            if(this.get(i).getSex() == null || this.get(i).getSex().equals("null")) {
            	pstmt.setString(7,null);
            } else {
            	pstmt.setString(7, this.get(i).getSex());
            }
            if(this.get(i).getBirthday() == null || this.get(i).getBirthday().equals("null")) {
                pstmt.setDate(8,null);
            } else {
                pstmt.setDate(8, Date.valueOf(this.get(i).getBirthday()));
            }
            if(this.get(i).getIdType() == null || this.get(i).getIdType().equals("null")) {
            	pstmt.setString(9,null);
            } else {
            	pstmt.setString(9, this.get(i).getIdType());
            }
            if(this.get(i).getIdNo() == null || this.get(i).getIdNo().equals("null")) {
            	pstmt.setString(10,null);
            } else {
            	pstmt.setString(10, this.get(i).getIdNo());
            }
            if(this.get(i).getIsRisk() == null || this.get(i).getIsRisk().equals("null")) {
            	pstmt.setString(11,null);
            } else {
            	pstmt.setString(11, this.get(i).getIsRisk());
            }
            pstmt.setInt(12, this.get(i).getLossTimes());
            pstmt.setDouble(13, this.get(i).getSumClaimAmount());
            pstmt.setInt(14, this.get(i).getRefuseTimes());
            if(this.get(i).getCompanyCode() == null || this.get(i).getCompanyCode().equals("null")) {
            	pstmt.setString(15,null);
            } else {
            	pstmt.setString(15, this.get(i).getCompanyCode());
            }
            if(this.get(i).getLossDate() == null || this.get(i).getLossDate().equals("null")) {
                pstmt.setDate(16,null);
            } else {
                pstmt.setDate(16, Date.valueOf(this.get(i).getLossDate()));
            }
            if(this.get(i).getLossResult() == null || this.get(i).getLossResult().equals("null")) {
            	pstmt.setString(17,null);
            } else {
            	pstmt.setString(17, this.get(i).getLossResult());
            }
            pstmt.setDouble(18, this.get(i).getClaimAmount());
            pstmt.setInt(19, this.get(i).getCompanyQuantity());
            pstmt.setInt(20, this.get(i).getPolicyQuantity());
            pstmt.setDouble(21, this.get(i).getSumAmount());
            if(this.get(i).getIsRiskPerson() == null || this.get(i).getIsRiskPerson().equals("null")) {
            	pstmt.setString(22,null);
            } else {
            	pstmt.setString(22, this.get(i).getIsRiskPerson());
            }
            if(this.get(i).getMakeDate() == null || this.get(i).getMakeDate().equals("null")) {
                pstmt.setDate(23,null);
            } else {
                pstmt.setDate(23, Date.valueOf(this.get(i).getMakeDate()));
            }
            if(this.get(i).getMakeTime() == null || this.get(i).getMakeTime().equals("null")) {
            	pstmt.setString(24,null);
            } else {
            	pstmt.setString(24, this.get(i).getMakeTime());
            }
            if(this.get(i).getModifyDate() == null || this.get(i).getModifyDate().equals("null")) {
                pstmt.setDate(25,null);
            } else {
                pstmt.setDate(25, Date.valueOf(this.get(i).getModifyDate()));
            }
            if(this.get(i).getModifyTime() == null || this.get(i).getModifyTime().equals("null")) {
            	pstmt.setString(26,null);
            } else {
            	pstmt.setString(26, this.get(i).getModifyTime());
            }
                pstmt.addBatch();
            }
            pstmt.executeBatch();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            ex.printStackTrace();
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LCSDRiskQueryDBSet";
            tError.functionName = "insert()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            try {
                pstmt.close();
            } catch (Exception e){e.printStackTrace();}

            if( !mflag ) {
                try {
                    con.close();
                } catch (Exception e){e.printStackTrace();}
            }

            return false;
        }

        if( !mflag ) {
            try {
                con.close();
            } catch (Exception e){e.printStackTrace();}
        }

        return true;
    }
}
