/**
 * Copyright (c) 2018 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.vdb;

import com.sinosoft.lis.vschema.LCPolicyInfoSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.DBConnPool;
import com.sinosoft.utility.DBOper;
import com.sinosoft.utility.ExceptionUtils;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;

/**
 * <p>ClassName: LCPolicyInfoDBSet </p>
 * <p>Description: DB层多记录数据库操作类文件 </p>
 * <p>Copyright: Copyright (c) 2018</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2018-07-09
 */
public class LCPolicyInfoDBSet extends LCPolicyInfoSet {
    // @Field
    private Connection con;
    private DBOper db;
    /**
     * flag = true: 传入Connection
     * flag = false: 不传入Connection
     **/
    private boolean mflag = false;

    // @Constructor
    public LCPolicyInfoDBSet(Connection tConnection) {
        con = tConnection;
        db = new DBOper(con,"LCPolicyInfo");
        mflag = true;
    }

    public LCPolicyInfoDBSet() {
        db = new DBOper( "LCPolicyInfo" );
    }
    // @Method
    public boolean deleteSQL() {
        if (db.deleteSQL(this)) {
            return true;
        } else {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LCPolicyInfoDBSet";
            tError.functionName = "deleteSQL";
            tError.errorMessage = "操作失败!";
            this.mErrors .addOneError(tError);
            return false;
        }
    }

    /**
     * 删除操作
     * 删除条件：主键
     * @return boolean
     */
    public boolean delete() {
        PreparedStatement pstmt = null;

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try {
            int tCount = this.size();
            pstmt = con.prepareStatement("DELETE FROM LCPolicyInfo WHERE  1=1  AND ContNo = ? AND InsuredNo = ?");
            for (int i = 1; i <= tCount; i++) {
                if(this.get(i).getContNo() == null || this.get(i).getContNo().equals("null")) {
                    pstmt.setString(1,null);
                } else {
                    pstmt.setString(1, this.get(i).getContNo());
                }
                if(this.get(i).getInsuredNo() == null || this.get(i).getInsuredNo().equals("null")) {
                    pstmt.setString(2,null);
                } else {
                    pstmt.setString(2, this.get(i).getInsuredNo());
                }
                pstmt.addBatch();
            }
            pstmt.executeBatch();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            ex.printStackTrace();
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LCPolicyInfoDBSet";
            tError.functionName = "delete()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            try {
                pstmt.close();
            } catch (Exception e){
                e.printStackTrace();
            }

            if (!mflag) {
                try {
                    con.close();
                } catch (Exception e){e.printStackTrace();}
            }

            return false;
        }

        if( !mflag ) {
            try {
                con.close();
            } catch (Exception e){e.printStackTrace();}
        }

        return true;
    }

    /**
     * 更新操作
     * 更新条件：主键
     * @return boolean
     */
    public boolean update() {
        PreparedStatement pstmt = null;

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try {
            int tCount = this.size();
            pstmt = con.prepareStatement("UPDATE LCPolicyInfo SET  ContNo = ? , InsuredNo = ? , ContplanCode = ? , ContplanName = ? , BasePrem = ? , AddPrem = ? , ActiveDate = ? , InsuYear = ? , InsuYearFlag = ? , Cvaliintv = ? , CvaliintvFlag = ? , SisinSuredNo = ? , TraveLcountry = ? , AgentIp = ? , FlightNo = ? , RescuecardNo = ? , HandlerName = ? , HandlerPhone = ? , SignDate = ? , SignTime = ? , agentcom = ? , RiskType = ? , OrderType = ? , JYContNo = ? , JYCertNo = ? , JYStartDate = ? , JYEndDate = ? , LoanAmount = ? , LendCom = ? , LoanerNature = ? , LoanerNatureName = ? , LendTerm = ? , CountryCode = ? , GasCompany = ? , GasUserAddress = ? , PayoutPro = ? , School = ? , ChargeDate = ? , Mark = ? WHERE  1=1  AND ContNo = ? AND InsuredNo = ?");
            for (int i = 1; i <= tCount; i++) {
                if(this.get(i).getContNo() == null || this.get(i).getContNo().equals("null")) {
                    pstmt.setString(1,null);
                } else {
                    pstmt.setString(1, this.get(i).getContNo());
                }
                if(this.get(i).getInsuredNo() == null || this.get(i).getInsuredNo().equals("null")) {
                    pstmt.setString(2,null);
                } else {
                    pstmt.setString(2, this.get(i).getInsuredNo());
                }
                if(this.get(i).getContplanCode() == null || this.get(i).getContplanCode().equals("null")) {
                    pstmt.setString(3,null);
                } else {
                    pstmt.setString(3, this.get(i).getContplanCode());
                }
                if(this.get(i).getContplanName() == null || this.get(i).getContplanName().equals("null")) {
                    pstmt.setString(4,null);
                } else {
                    pstmt.setString(4, this.get(i).getContplanName());
                }
                pstmt.setDouble(5, this.get(i).getBasePrem());
                pstmt.setDouble(6, this.get(i).getAddPrem());
                if(this.get(i).getActiveDate() == null || this.get(i).getActiveDate().equals("null")) {
                    pstmt.setDate(7,null);
                } else {
                    pstmt.setDate(7, Date.valueOf(this.get(i).getActiveDate()));
                }
                pstmt.setInt(8, this.get(i).getInsuYear());
                if(this.get(i).getInsuYearFlag() == null || this.get(i).getInsuYearFlag().equals("null")) {
                    pstmt.setString(9,null);
                } else {
                    pstmt.setString(9, this.get(i).getInsuYearFlag());
                }
                pstmt.setInt(10, this.get(i).getCvaliintv());
                if(this.get(i).getCvaliintvFlag() == null || this.get(i).getCvaliintvFlag().equals("null")) {
                    pstmt.setString(11,null);
                } else {
                    pstmt.setString(11, this.get(i).getCvaliintvFlag());
                }
                if(this.get(i).getSisinSuredNo() == null || this.get(i).getSisinSuredNo().equals("null")) {
                    pstmt.setString(12,null);
                } else {
                    pstmt.setString(12, this.get(i).getSisinSuredNo());
                }
                if(this.get(i).getTraveLcountry() == null || this.get(i).getTraveLcountry().equals("null")) {
                    pstmt.setString(13,null);
                } else {
                    pstmt.setString(13, this.get(i).getTraveLcountry());
                }
                if(this.get(i).getAgentIp() == null || this.get(i).getAgentIp().equals("null")) {
                    pstmt.setString(14,null);
                } else {
                    pstmt.setString(14, this.get(i).getAgentIp());
                }
                if(this.get(i).getFlightNo() == null || this.get(i).getFlightNo().equals("null")) {
                    pstmt.setString(15,null);
                } else {
                    pstmt.setString(15, this.get(i).getFlightNo());
                }
                if(this.get(i).getRescuecardNo() == null || this.get(i).getRescuecardNo().equals("null")) {
                    pstmt.setString(16,null);
                } else {
                    pstmt.setString(16, this.get(i).getRescuecardNo());
                }
                if(this.get(i).getHandlerName() == null || this.get(i).getHandlerName().equals("null")) {
                    pstmt.setString(17,null);
                } else {
                    pstmt.setString(17, this.get(i).getHandlerName());
                }
                if(this.get(i).getHandlerPhone() == null || this.get(i).getHandlerPhone().equals("null")) {
                    pstmt.setString(18,null);
                } else {
                    pstmt.setString(18, this.get(i).getHandlerPhone());
                }
                if(this.get(i).getSignDate() == null || this.get(i).getSignDate().equals("null")) {
                    pstmt.setDate(19,null);
                } else {
                    pstmt.setDate(19, Date.valueOf(this.get(i).getSignDate()));
                }
                if(this.get(i).getSignTime() == null || this.get(i).getSignTime().equals("null")) {
                    pstmt.setString(20,null);
                } else {
                    pstmt.setString(20, this.get(i).getSignTime());
                }
                if(this.get(i).getAgentcom() == null || this.get(i).getAgentcom().equals("null")) {
                    pstmt.setString(21,null);
                } else {
                    pstmt.setString(21, this.get(i).getAgentcom());
                }
                if(this.get(i).getRiskType() == null || this.get(i).getRiskType().equals("null")) {
                    pstmt.setString(22,null);
                } else {
                    pstmt.setString(22, this.get(i).getRiskType());
                }
                if(this.get(i).getOrderType() == null || this.get(i).getOrderType().equals("null")) {
                    pstmt.setString(23,null);
                } else {
                    pstmt.setString(23, this.get(i).getOrderType());
                }
                if(this.get(i).getJYContNo() == null || this.get(i).getJYContNo().equals("null")) {
                    pstmt.setString(24,null);
                } else {
                    pstmt.setString(24, this.get(i).getJYContNo());
                }
                if(this.get(i).getJYCertNo() == null || this.get(i).getJYCertNo().equals("null")) {
                    pstmt.setString(25,null);
                } else {
                    pstmt.setString(25, this.get(i).getJYCertNo());
                }
                if(this.get(i).getJYStartDate() == null || this.get(i).getJYStartDate().equals("null")) {
                    pstmt.setString(26,null);
                } else {
                    pstmt.setString(26, this.get(i).getJYStartDate());
                }
                if(this.get(i).getJYEndDate() == null || this.get(i).getJYEndDate().equals("null")) {
                    pstmt.setString(27,null);
                } else {
                    pstmt.setString(27, this.get(i).getJYEndDate());
                }
                if(this.get(i).getLoanAmount() == null || this.get(i).getLoanAmount().equals("null")) {
                    pstmt.setString(28,null);
                } else {
                    pstmt.setString(28, this.get(i).getLoanAmount());
                }
                if(this.get(i).getLendCom() == null || this.get(i).getLendCom().equals("null")) {
                    pstmt.setString(29,null);
                } else {
                    pstmt.setString(29, this.get(i).getLendCom());
                }
                if(this.get(i).getLoanerNature() == null || this.get(i).getLoanerNature().equals("null")) {
                    pstmt.setString(30,null);
                } else {
                    pstmt.setString(30, this.get(i).getLoanerNature());
                }
                if(this.get(i).getLoanerNatureName() == null || this.get(i).getLoanerNatureName().equals("null")) {
                    pstmt.setString(31,null);
                } else {
                    pstmt.setString(31, this.get(i).getLoanerNatureName());
                }
                if(this.get(i).getLendTerm() == null || this.get(i).getLendTerm().equals("null")) {
                    pstmt.setString(32,null);
                } else {
                    pstmt.setString(32, this.get(i).getLendTerm());
                }
                if(this.get(i).getCountryCode() == null || this.get(i).getCountryCode().equals("null")) {
                    pstmt.setString(33,null);
                } else {
                    pstmt.setString(33, this.get(i).getCountryCode());
                }
                if(this.get(i).getGasCompany() == null || this.get(i).getGasCompany().equals("null")) {
                    pstmt.setString(34,null);
                } else {
                    pstmt.setString(34, this.get(i).getGasCompany());
                }
                if(this.get(i).getGasUserAddress() == null || this.get(i).getGasUserAddress().equals("null")) {
                    pstmt.setString(35,null);
                } else {
                    pstmt.setString(35, this.get(i).getGasUserAddress());
                }
                if(this.get(i).getPayoutPro() == null || this.get(i).getPayoutPro().equals("null")) {
                    pstmt.setString(36,null);
                } else {
                    pstmt.setString(36, this.get(i).getPayoutPro());
                }
                if(this.get(i).getSchool() == null || this.get(i).getSchool().equals("null")) {
                    pstmt.setString(37,null);
                } else {
                    pstmt.setString(37, this.get(i).getSchool());
                }
                if(this.get(i).getChargeDate() == null || this.get(i).getChargeDate().equals("null")) {
                    pstmt.setString(38,null);
                } else {
                    pstmt.setString(38, this.get(i).getChargeDate());
                }
                if(this.get(i).getMark() == null || this.get(i).getMark().equals("null")) {
                    pstmt.setString(39,null);
                } else {
                    pstmt.setString(39, this.get(i).getMark());
                }
                // set where condition
                if(this.get(i).getContNo() == null || this.get(i).getContNo().equals("null")) {
                    pstmt.setString(40,null);
                } else {
                    pstmt.setString(40, this.get(i).getContNo());
                }
                if(this.get(i).getInsuredNo() == null || this.get(i).getInsuredNo().equals("null")) {
                    pstmt.setString(41,null);
                } else {
                    pstmt.setString(41, this.get(i).getInsuredNo());
                }
                pstmt.addBatch();
            }
            pstmt.executeBatch();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            ex.printStackTrace();
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LCPolicyInfoDBSet";
            tError.functionName = "update()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            try {
                pstmt.close();
            } catch (Exception e){e.printStackTrace();}

            if( !mflag ) {
                try {
                    con.close();
                } catch (Exception e){e.printStackTrace();}
            }

            return false;
        }

        if( !mflag ) {
            try {
                con.close();
            } catch (Exception e){e.printStackTrace();}
        }

        return true;
    }

    /**
     * 新增操作
     * @return boolean
     */
    public boolean insert() {
        PreparedStatement pstmt = null;

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try
        {
            int tCount = this.size();
            pstmt = con.prepareStatement("INSERT INTO LCPolicyInfo VALUES( ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ?)");
            for (int i = 1; i <= tCount; i++) {
                if(this.get(i).getContNo() == null || this.get(i).getContNo().equals("null")) {
                    pstmt.setString(1,null);
                } else {
                    pstmt.setString(1, this.get(i).getContNo());
                }
                if(this.get(i).getInsuredNo() == null || this.get(i).getInsuredNo().equals("null")) {
                    pstmt.setString(2,null);
                } else {
                    pstmt.setString(2, this.get(i).getInsuredNo());
                }
                if(this.get(i).getContplanCode() == null || this.get(i).getContplanCode().equals("null")) {
                    pstmt.setString(3,null);
                } else {
                    pstmt.setString(3, this.get(i).getContplanCode());
                }
                if(this.get(i).getContplanName() == null || this.get(i).getContplanName().equals("null")) {
                    pstmt.setString(4,null);
                } else {
                    pstmt.setString(4, this.get(i).getContplanName());
                }
                pstmt.setDouble(5, this.get(i).getBasePrem());
                pstmt.setDouble(6, this.get(i).getAddPrem());
                if(this.get(i).getActiveDate() == null || this.get(i).getActiveDate().equals("null")) {
                    pstmt.setDate(7,null);
                } else {
                    pstmt.setDate(7, Date.valueOf(this.get(i).getActiveDate()));
                }
                pstmt.setInt(8, this.get(i).getInsuYear());
                if(this.get(i).getInsuYearFlag() == null || this.get(i).getInsuYearFlag().equals("null")) {
                    pstmt.setString(9,null);
                } else {
                    pstmt.setString(9, this.get(i).getInsuYearFlag());
                }
                pstmt.setInt(10, this.get(i).getCvaliintv());
                if(this.get(i).getCvaliintvFlag() == null || this.get(i).getCvaliintvFlag().equals("null")) {
                    pstmt.setString(11,null);
                } else {
                    pstmt.setString(11, this.get(i).getCvaliintvFlag());
                }
                if(this.get(i).getSisinSuredNo() == null || this.get(i).getSisinSuredNo().equals("null")) {
                    pstmt.setString(12,null);
                } else {
                    pstmt.setString(12, this.get(i).getSisinSuredNo());
                }
                if(this.get(i).getTraveLcountry() == null || this.get(i).getTraveLcountry().equals("null")) {
                    pstmt.setString(13,null);
                } else {
                    pstmt.setString(13, this.get(i).getTraveLcountry());
                }
                if(this.get(i).getAgentIp() == null || this.get(i).getAgentIp().equals("null")) {
                    pstmt.setString(14,null);
                } else {
                    pstmt.setString(14, this.get(i).getAgentIp());
                }
                if(this.get(i).getFlightNo() == null || this.get(i).getFlightNo().equals("null")) {
                    pstmt.setString(15,null);
                } else {
                    pstmt.setString(15, this.get(i).getFlightNo());
                }
                if(this.get(i).getRescuecardNo() == null || this.get(i).getRescuecardNo().equals("null")) {
                    pstmt.setString(16,null);
                } else {
                    pstmt.setString(16, this.get(i).getRescuecardNo());
                }
                if(this.get(i).getHandlerName() == null || this.get(i).getHandlerName().equals("null")) {
                    pstmt.setString(17,null);
                } else {
                    pstmt.setString(17, this.get(i).getHandlerName());
                }
                if(this.get(i).getHandlerPhone() == null || this.get(i).getHandlerPhone().equals("null")) {
                    pstmt.setString(18,null);
                } else {
                    pstmt.setString(18, this.get(i).getHandlerPhone());
                }
                if(this.get(i).getSignDate() == null || this.get(i).getSignDate().equals("null")) {
                    pstmt.setDate(19,null);
                } else {
                    pstmt.setDate(19, Date.valueOf(this.get(i).getSignDate()));
                }
                if(this.get(i).getSignTime() == null || this.get(i).getSignTime().equals("null")) {
                    pstmt.setString(20,null);
                } else {
                    pstmt.setString(20, this.get(i).getSignTime());
                }
                if(this.get(i).getAgentcom() == null || this.get(i).getAgentcom().equals("null")) {
                    pstmt.setString(21,null);
                } else {
                    pstmt.setString(21, this.get(i).getAgentcom());
                }
                if(this.get(i).getRiskType() == null || this.get(i).getRiskType().equals("null")) {
                    pstmt.setString(22,null);
                } else {
                    pstmt.setString(22, this.get(i).getRiskType());
                }
                if(this.get(i).getOrderType() == null || this.get(i).getOrderType().equals("null")) {
                    pstmt.setString(23,null);
                } else {
                    pstmt.setString(23, this.get(i).getOrderType());
                }
                if(this.get(i).getJYContNo() == null || this.get(i).getJYContNo().equals("null")) {
                    pstmt.setString(24,null);
                } else {
                    pstmt.setString(24, this.get(i).getJYContNo());
                }
                if(this.get(i).getJYCertNo() == null || this.get(i).getJYCertNo().equals("null")) {
                    pstmt.setString(25,null);
                } else {
                    pstmt.setString(25, this.get(i).getJYCertNo());
                }
                if(this.get(i).getJYStartDate() == null || this.get(i).getJYStartDate().equals("null")) {
                    pstmt.setString(26,null);
                } else {
                    pstmt.setString(26, this.get(i).getJYStartDate());
                }
                if(this.get(i).getJYEndDate() == null || this.get(i).getJYEndDate().equals("null")) {
                    pstmt.setString(27,null);
                } else {
                    pstmt.setString(27, this.get(i).getJYEndDate());
                }
                if(this.get(i).getLoanAmount() == null || this.get(i).getLoanAmount().equals("null")) {
                    pstmt.setString(28,null);
                } else {
                    pstmt.setString(28, this.get(i).getLoanAmount());
                }
                if(this.get(i).getLendCom() == null || this.get(i).getLendCom().equals("null")) {
                    pstmt.setString(29,null);
                } else {
                    pstmt.setString(29, this.get(i).getLendCom());
                }
                if(this.get(i).getLoanerNature() == null || this.get(i).getLoanerNature().equals("null")) {
                    pstmt.setString(30,null);
                } else {
                    pstmt.setString(30, this.get(i).getLoanerNature());
                }
                if(this.get(i).getLoanerNatureName() == null || this.get(i).getLoanerNatureName().equals("null")) {
                    pstmt.setString(31,null);
                } else {
                    pstmt.setString(31, this.get(i).getLoanerNatureName());
                }
                if(this.get(i).getLendTerm() == null || this.get(i).getLendTerm().equals("null")) {
                    pstmt.setString(32,null);
                } else {
                    pstmt.setString(32, this.get(i).getLendTerm());
                }
                if(this.get(i).getCountryCode() == null || this.get(i).getCountryCode().equals("null")) {
                    pstmt.setString(33,null);
                } else {
                    pstmt.setString(33, this.get(i).getCountryCode());
                }
                if(this.get(i).getGasCompany() == null || this.get(i).getGasCompany().equals("null")) {
                    pstmt.setString(34,null);
                } else {
                    pstmt.setString(34, this.get(i).getGasCompany());
                }
                if(this.get(i).getGasUserAddress() == null || this.get(i).getGasUserAddress().equals("null")) {
                    pstmt.setString(35,null);
                } else {
                    pstmt.setString(35, this.get(i).getGasUserAddress());
                }
                if(this.get(i).getPayoutPro() == null || this.get(i).getPayoutPro().equals("null")) {
                    pstmt.setString(36,null);
                } else {
                    pstmt.setString(36, this.get(i).getPayoutPro());
                }
                if(this.get(i).getSchool() == null || this.get(i).getSchool().equals("null")) {
                    pstmt.setString(37,null);
                } else {
                    pstmt.setString(37, this.get(i).getSchool());
                }
                if(this.get(i).getChargeDate() == null || this.get(i).getChargeDate().equals("null")) {
                    pstmt.setString(38,null);
                } else {
                    pstmt.setString(38, this.get(i).getChargeDate());
                }
                if(this.get(i).getMark() == null || this.get(i).getMark().equals("null")) {
                    pstmt.setString(39,null);
                } else {
                    pstmt.setString(39, this.get(i).getMark());
                }
                pstmt.addBatch();
            }
            pstmt.executeBatch();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            ex.printStackTrace();
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LCPolicyInfoDBSet";
            tError.functionName = "insert()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            try {
                pstmt.close();
            } catch (Exception e){e.printStackTrace();}

            if( !mflag ) {
                try {
                    con.close();
                } catch (Exception e){e.printStackTrace();}
            }

            return false;
        }

        if( !mflag ) {
            try {
                con.close();
            } catch (Exception e){e.printStackTrace();}
        }

        return true;
    }
}
