/**
 * Copyright (c) 2018 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import com.sinosoft.lis.db.LCGrpAppntDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.*;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

/**
 * <p>ClassName: LCGrpAppntSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2018</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2018-05-23
 */
public class LCGrpAppntSchema implements Schema, Cloneable {
    // @Field
    /** 集体合同号码 */
    private String GrpContNo;
    /** 客户号码 */
    private String CustomerNo;
    /** 印刷号码 */
    private String PrtNo;
    /** 客户地址号码 */
    private String AddressNo;
    /** 投保人级别 */
    private String AppntGrade;
    /** 单位名称 */
    private String Name;
    /** 通讯地址 */
    private String PostalAddress;
    /** 单位邮编 */
    private String ZipCode;
    /** 单位电话 */
    private String Phone;
    /** 密码 */
    private String Password;
    /** 状态 */
    private String State;
    /** 投保人类型 */
    private String AppntType;
    /** 被保人与投保人关系 */
    private String RelationToInsured;
    /** 操作员 */
    private String Operator;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 最后一次修改日期 */
    private Date ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime;
    /** 行业分类 */
    private String BusiCategory;
    /** 单位性质 */
    private String GrpNature;
    /** 员工总人数 */
    private int SumNumPeople;
    /** 经营范围 */
    private String MainBusiness;
    /** 法人 */
    private String Corporation;
    /** 法人证件类型 */
    private String CorIDType;
    /** 法人证件号码 */
    private String CorID;
    /** 联系人证件有效止期 */
    private Date CorIDExpiryDate;
    /** 在职人数 */
    private int OnJobNumber;
    /** 退休人数 */
    private int RetireNumber;
    /** 其它人数 */
    private int OtherNumber;
    /** 注册资本 */
    private double RgtCapital;
    /** 资产总额 */
    private double TotalAssets;
    /** 净资产收益率 */
    private double NetProfitRate;
    /** 负责人 */
    private String Satrap;
    /** 实际控制人 */
    private String ActuCtrl;
    /** 营业执照号 */
    private String License;
    /** 社保保险登记证号 */
    private String SocialInsuCode;
    /** 组织机构代码 */
    private String OrganizationCode;
    /** 税务登记证 */
    private String TaxCode;
    /** 单位传真 */
    private String Fax;
    /** 单位emaill */
    private String EMail;
    /** 成立日期 */
    private Date FoundDate;
    /** 备注 */
    private String Remark;
    /** 备用字段1 */
    private String Segment1;
    /** 备用字段2 */
    private String Segment2;
    /** 备用字段3 */
    private String Segment3;
    /** 管理机构 */
    private String ManageCom;
    /** 公司代码 */
    private String ComCode;
    /** 最后一次修改操作员 */
    private String ModifyOperator;
    /** 性别 */
    private String Sex;
    /** 国籍 */
    private String NativePlace;
    /** 职业描述 */
    private String Occupation;
    /** 职业编码 */
    private String OccupationCode;
    /** 纳税类型 */
    private String TaxPayerType;
    /** 是否固定方案 */
    private String FixedPlan;
    /** 是否开具有增值税的专用发票 */
    private String AddTaxFlag;
    /** 取得增值税人资质日期 */
    private Date AddTaxDate;
    /** 银行账号(税务登记) */
    private String BankAccTaxNo;
    /** 开户银行全称(税务登记) */
    private String BankTaxName;
    /** 开户银行编码(税务登记) */
    private String BankTaxCode;
    /** 投保团体证件类型 */
    private String GrpAppIDType;
    /** 投保团体证件有效期 */
    private Date GrpAppIDExpDate;
    /** 控股股东/实际控制人 */
    private String CtrPeople;
    /** 单位可投保人数 */
    private int Peoples3;
    /** 连带投保人数 */
    private int RelaPeoples;
    /** 连带配偶投保人数 */
    private int RelaMatePeoples;
    /** 连带子女投保人数 */
    private int RelaYoungPeoples;
    /** 连带其它投保人数 */
    private int RelaOtherPeoples;

    public static final int FIELDNUM = 67;    // 数据库表的字段个数

    private static String[] PK;                // 主键

    private FDate fDate = new FDate();        // 处理日期

    public CErrors mErrors;            // 错误信息

    // @Constructor
    public LCGrpAppntSchema() {
        mErrors = new CErrors();

        String[] pk = new String[1];
        pk[0] = "GrpContNo";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException {
        LCGrpAppntSchema cloned = (LCGrpAppntSchema)super.clone();
        cloned.fDate = (FDate) fDate.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK() {
        return PK;
    }

    public String getGrpContNo() {
        return GrpContNo;
    }
    public void setGrpContNo(String aGrpContNo) {
        GrpContNo = aGrpContNo;
    }
    public String getCustomerNo() {
        return CustomerNo;
    }
    public void setCustomerNo(String aCustomerNo) {
        CustomerNo = aCustomerNo;
    }
    public String getPrtNo() {
        return PrtNo;
    }
    public void setPrtNo(String aPrtNo) {
        PrtNo = aPrtNo;
    }
    public String getAddressNo() {
        return AddressNo;
    }
    public void setAddressNo(String aAddressNo) {
        AddressNo = aAddressNo;
    }
    public String getAppntGrade() {
        return AppntGrade;
    }
    public void setAppntGrade(String aAppntGrade) {
        AppntGrade = aAppntGrade;
    }
    public String getName() {
        return Name;
    }
    public void setName(String aName) {
        Name = aName;
    }
    public String getPostalAddress() {
        return PostalAddress;
    }
    public void setPostalAddress(String aPostalAddress) {
        PostalAddress = aPostalAddress;
    }
    public String getZipCode() {
        return ZipCode;
    }
    public void setZipCode(String aZipCode) {
        ZipCode = aZipCode;
    }
    public String getPhone() {
        return Phone;
    }
    public void setPhone(String aPhone) {
        Phone = aPhone;
    }
    public String getPassword() {
        return Password;
    }
    public void setPassword(String aPassword) {
        Password = aPassword;
    }
    public String getState() {
        return State;
    }
    public void setState(String aState) {
        State = aState;
    }
    public String getAppntType() {
        return AppntType;
    }
    public void setAppntType(String aAppntType) {
        AppntType = aAppntType;
    }
    public String getRelationToInsured() {
        return RelationToInsured;
    }
    public void setRelationToInsured(String aRelationToInsured) {
        RelationToInsured = aRelationToInsured;
    }
    public String getOperator() {
        return Operator;
    }
    public void setOperator(String aOperator) {
        Operator = aOperator;
    }
    public String getMakeDate() {
        if(MakeDate != null) {
            return fDate.getString(MakeDate);
        } else {
            return null;
        }
    }
    public void setMakeDate(Date aMakeDate) {
        MakeDate = aMakeDate;
    }
    public void setMakeDate(String aMakeDate) {
        if (aMakeDate != null && !aMakeDate.equals("")) {
            MakeDate = fDate.getDate(aMakeDate);
        } else
            MakeDate = null;
    }

    public String getMakeTime() {
        return MakeTime;
    }
    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }
    public String getModifyDate() {
        if(ModifyDate != null) {
            return fDate.getString(ModifyDate);
        } else {
            return null;
        }
    }
    public void setModifyDate(Date aModifyDate) {
        ModifyDate = aModifyDate;
    }
    public void setModifyDate(String aModifyDate) {
        if (aModifyDate != null && !aModifyDate.equals("")) {
            ModifyDate = fDate.getDate(aModifyDate);
        } else
            ModifyDate = null;
    }

    public String getModifyTime() {
        return ModifyTime;
    }
    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }
    public String getBusiCategory() {
        return BusiCategory;
    }
    public void setBusiCategory(String aBusiCategory) {
        BusiCategory = aBusiCategory;
    }
    public String getGrpNature() {
        return GrpNature;
    }
    public void setGrpNature(String aGrpNature) {
        GrpNature = aGrpNature;
    }
    public int getSumNumPeople() {
        return SumNumPeople;
    }
    public void setSumNumPeople(int aSumNumPeople) {
        SumNumPeople = aSumNumPeople;
    }
    public void setSumNumPeople(String aSumNumPeople) {
        if (aSumNumPeople != null && !aSumNumPeople.equals("")) {
            Integer tInteger = new Integer(aSumNumPeople);
            int i = tInteger.intValue();
            SumNumPeople = i;
        }
    }

    public String getMainBusiness() {
        return MainBusiness;
    }
    public void setMainBusiness(String aMainBusiness) {
        MainBusiness = aMainBusiness;
    }
    public String getCorporation() {
        return Corporation;
    }
    public void setCorporation(String aCorporation) {
        Corporation = aCorporation;
    }
    public String getCorIDType() {
        return CorIDType;
    }
    public void setCorIDType(String aCorIDType) {
        CorIDType = aCorIDType;
    }
    public String getCorID() {
        return CorID;
    }
    public void setCorID(String aCorID) {
        CorID = aCorID;
    }
    public String getCorIDExpiryDate() {
        if(CorIDExpiryDate != null) {
            return fDate.getString(CorIDExpiryDate);
        } else {
            return null;
        }
    }
    public void setCorIDExpiryDate(Date aCorIDExpiryDate) {
        CorIDExpiryDate = aCorIDExpiryDate;
    }
    public void setCorIDExpiryDate(String aCorIDExpiryDate) {
        if (aCorIDExpiryDate != null && !aCorIDExpiryDate.equals("")) {
            CorIDExpiryDate = fDate.getDate(aCorIDExpiryDate);
        } else
            CorIDExpiryDate = null;
    }

    public int getOnJobNumber() {
        return OnJobNumber;
    }
    public void setOnJobNumber(int aOnJobNumber) {
        OnJobNumber = aOnJobNumber;
    }
    public void setOnJobNumber(String aOnJobNumber) {
        if (aOnJobNumber != null && !aOnJobNumber.equals("")) {
            Integer tInteger = new Integer(aOnJobNumber);
            int i = tInteger.intValue();
            OnJobNumber = i;
        }
    }

    public int getRetireNumber() {
        return RetireNumber;
    }
    public void setRetireNumber(int aRetireNumber) {
        RetireNumber = aRetireNumber;
    }
    public void setRetireNumber(String aRetireNumber) {
        if (aRetireNumber != null && !aRetireNumber.equals("")) {
            Integer tInteger = new Integer(aRetireNumber);
            int i = tInteger.intValue();
            RetireNumber = i;
        }
    }

    public int getOtherNumber() {
        return OtherNumber;
    }
    public void setOtherNumber(int aOtherNumber) {
        OtherNumber = aOtherNumber;
    }
    public void setOtherNumber(String aOtherNumber) {
        if (aOtherNumber != null && !aOtherNumber.equals("")) {
            Integer tInteger = new Integer(aOtherNumber);
            int i = tInteger.intValue();
            OtherNumber = i;
        }
    }

    public double getRgtCapital() {
        return RgtCapital;
    }
    public void setRgtCapital(double aRgtCapital) {
        RgtCapital = aRgtCapital;
    }
    public void setRgtCapital(String aRgtCapital) {
        if (aRgtCapital != null && !aRgtCapital.equals("")) {
            Double tDouble = new Double(aRgtCapital);
            double d = tDouble.doubleValue();
            RgtCapital = d;
        }
    }

    public double getTotalAssets() {
        return TotalAssets;
    }
    public void setTotalAssets(double aTotalAssets) {
        TotalAssets = aTotalAssets;
    }
    public void setTotalAssets(String aTotalAssets) {
        if (aTotalAssets != null && !aTotalAssets.equals("")) {
            Double tDouble = new Double(aTotalAssets);
            double d = tDouble.doubleValue();
            TotalAssets = d;
        }
    }

    public double getNetProfitRate() {
        return NetProfitRate;
    }
    public void setNetProfitRate(double aNetProfitRate) {
        NetProfitRate = aNetProfitRate;
    }
    public void setNetProfitRate(String aNetProfitRate) {
        if (aNetProfitRate != null && !aNetProfitRate.equals("")) {
            Double tDouble = new Double(aNetProfitRate);
            double d = tDouble.doubleValue();
            NetProfitRate = d;
        }
    }

    public String getSatrap() {
        return Satrap;
    }
    public void setSatrap(String aSatrap) {
        Satrap = aSatrap;
    }
    public String getActuCtrl() {
        return ActuCtrl;
    }
    public void setActuCtrl(String aActuCtrl) {
        ActuCtrl = aActuCtrl;
    }
    public String getLicense() {
        return License;
    }
    public void setLicense(String aLicense) {
        License = aLicense;
    }
    public String getSocialInsuCode() {
        return SocialInsuCode;
    }
    public void setSocialInsuCode(String aSocialInsuCode) {
        SocialInsuCode = aSocialInsuCode;
    }
    public String getOrganizationCode() {
        return OrganizationCode;
    }
    public void setOrganizationCode(String aOrganizationCode) {
        OrganizationCode = aOrganizationCode;
    }
    public String getTaxCode() {
        return TaxCode;
    }
    public void setTaxCode(String aTaxCode) {
        TaxCode = aTaxCode;
    }
    public String getFax() {
        return Fax;
    }
    public void setFax(String aFax) {
        Fax = aFax;
    }
    public String getEMail() {
        return EMail;
    }
    public void setEMail(String aEMail) {
        EMail = aEMail;
    }
    public String getFoundDate() {
        if(FoundDate != null) {
            return fDate.getString(FoundDate);
        } else {
            return null;
        }
    }
    public void setFoundDate(Date aFoundDate) {
        FoundDate = aFoundDate;
    }
    public void setFoundDate(String aFoundDate) {
        if (aFoundDate != null && !aFoundDate.equals("")) {
            FoundDate = fDate.getDate(aFoundDate);
        } else
            FoundDate = null;
    }

    public String getRemark() {
        return Remark;
    }
    public void setRemark(String aRemark) {
        Remark = aRemark;
    }
    public String getSegment1() {
        return Segment1;
    }
    public void setSegment1(String aSegment1) {
        Segment1 = aSegment1;
    }
    public String getSegment2() {
        return Segment2;
    }
    public void setSegment2(String aSegment2) {
        Segment2 = aSegment2;
    }
    public String getSegment3() {
        return Segment3;
    }
    public void setSegment3(String aSegment3) {
        Segment3 = aSegment3;
    }
    public String getManageCom() {
        return ManageCom;
    }
    public void setManageCom(String aManageCom) {
        ManageCom = aManageCom;
    }
    public String getComCode() {
        return ComCode;
    }
    public void setComCode(String aComCode) {
        ComCode = aComCode;
    }
    public String getModifyOperator() {
        return ModifyOperator;
    }
    public void setModifyOperator(String aModifyOperator) {
        ModifyOperator = aModifyOperator;
    }
    public String getSex() {
        return Sex;
    }
    public void setSex(String aSex) {
        Sex = aSex;
    }
    public String getNativePlace() {
        return NativePlace;
    }
    public void setNativePlace(String aNativePlace) {
        NativePlace = aNativePlace;
    }
    public String getOccupation() {
        return Occupation;
    }
    public void setOccupation(String aOccupation) {
        Occupation = aOccupation;
    }
    public String getOccupationCode() {
        return OccupationCode;
    }
    public void setOccupationCode(String aOccupationCode) {
        OccupationCode = aOccupationCode;
    }
    public String getTaxPayerType() {
        return TaxPayerType;
    }
    public void setTaxPayerType(String aTaxPayerType) {
        TaxPayerType = aTaxPayerType;
    }
    public String getFixedPlan() {
        return FixedPlan;
    }
    public void setFixedPlan(String aFixedPlan) {
        FixedPlan = aFixedPlan;
    }
    public String getAddTaxFlag() {
        return AddTaxFlag;
    }
    public void setAddTaxFlag(String aAddTaxFlag) {
        AddTaxFlag = aAddTaxFlag;
    }
    public String getAddTaxDate() {
        if(AddTaxDate != null) {
            return fDate.getString(AddTaxDate);
        } else {
            return null;
        }
    }
    public void setAddTaxDate(Date aAddTaxDate) {
        AddTaxDate = aAddTaxDate;
    }
    public void setAddTaxDate(String aAddTaxDate) {
        if (aAddTaxDate != null && !aAddTaxDate.equals("")) {
            AddTaxDate = fDate.getDate(aAddTaxDate);
        } else
            AddTaxDate = null;
    }

    public String getBankAccTaxNo() {
        return BankAccTaxNo;
    }
    public void setBankAccTaxNo(String aBankAccTaxNo) {
        BankAccTaxNo = aBankAccTaxNo;
    }
    public String getBankTaxName() {
        return BankTaxName;
    }
    public void setBankTaxName(String aBankTaxName) {
        BankTaxName = aBankTaxName;
    }
    public String getBankTaxCode() {
        return BankTaxCode;
    }
    public void setBankTaxCode(String aBankTaxCode) {
        BankTaxCode = aBankTaxCode;
    }
    public String getGrpAppIDType() {
        return GrpAppIDType;
    }
    public void setGrpAppIDType(String aGrpAppIDType) {
        GrpAppIDType = aGrpAppIDType;
    }
    public String getGrpAppIDExpDate() {
        if(GrpAppIDExpDate != null) {
            return fDate.getString(GrpAppIDExpDate);
        } else {
            return null;
        }
    }
    public void setGrpAppIDExpDate(Date aGrpAppIDExpDate) {
        GrpAppIDExpDate = aGrpAppIDExpDate;
    }
    public void setGrpAppIDExpDate(String aGrpAppIDExpDate) {
        if (aGrpAppIDExpDate != null && !aGrpAppIDExpDate.equals("")) {
            GrpAppIDExpDate = fDate.getDate(aGrpAppIDExpDate);
        } else
            GrpAppIDExpDate = null;
    }

    public String getCtrPeople() {
        return CtrPeople;
    }
    public void setCtrPeople(String aCtrPeople) {
        CtrPeople = aCtrPeople;
    }
    public int getPeoples3() {
        return Peoples3;
    }
    public void setPeoples3(int aPeoples3) {
        Peoples3 = aPeoples3;
    }
    public void setPeoples3(String aPeoples3) {
        if (aPeoples3 != null && !aPeoples3.equals("")) {
            Integer tInteger = new Integer(aPeoples3);
            int i = tInteger.intValue();
            Peoples3 = i;
        }
    }

    public int getRelaPeoples() {
        return RelaPeoples;
    }
    public void setRelaPeoples(int aRelaPeoples) {
        RelaPeoples = aRelaPeoples;
    }
    public void setRelaPeoples(String aRelaPeoples) {
        if (aRelaPeoples != null && !aRelaPeoples.equals("")) {
            Integer tInteger = new Integer(aRelaPeoples);
            int i = tInteger.intValue();
            RelaPeoples = i;
        }
    }

    public int getRelaMatePeoples() {
        return RelaMatePeoples;
    }
    public void setRelaMatePeoples(int aRelaMatePeoples) {
        RelaMatePeoples = aRelaMatePeoples;
    }
    public void setRelaMatePeoples(String aRelaMatePeoples) {
        if (aRelaMatePeoples != null && !aRelaMatePeoples.equals("")) {
            Integer tInteger = new Integer(aRelaMatePeoples);
            int i = tInteger.intValue();
            RelaMatePeoples = i;
        }
    }

    public int getRelaYoungPeoples() {
        return RelaYoungPeoples;
    }
    public void setRelaYoungPeoples(int aRelaYoungPeoples) {
        RelaYoungPeoples = aRelaYoungPeoples;
    }
    public void setRelaYoungPeoples(String aRelaYoungPeoples) {
        if (aRelaYoungPeoples != null && !aRelaYoungPeoples.equals("")) {
            Integer tInteger = new Integer(aRelaYoungPeoples);
            int i = tInteger.intValue();
            RelaYoungPeoples = i;
        }
    }

    public int getRelaOtherPeoples() {
        return RelaOtherPeoples;
    }
    public void setRelaOtherPeoples(int aRelaOtherPeoples) {
        RelaOtherPeoples = aRelaOtherPeoples;
    }
    public void setRelaOtherPeoples(String aRelaOtherPeoples) {
        if (aRelaOtherPeoples != null && !aRelaOtherPeoples.equals("")) {
            Integer tInteger = new Integer(aRelaOtherPeoples);
            int i = tInteger.intValue();
            RelaOtherPeoples = i;
        }
    }


    /**
    * 使用另外一个 LCGrpAppntSchema 对象给 Schema 赋值
    * @param: aLCGrpAppntSchema LCGrpAppntSchema
    **/
    public void setSchema(LCGrpAppntSchema aLCGrpAppntSchema) {
        this.GrpContNo = aLCGrpAppntSchema.getGrpContNo();
        this.CustomerNo = aLCGrpAppntSchema.getCustomerNo();
        this.PrtNo = aLCGrpAppntSchema.getPrtNo();
        this.AddressNo = aLCGrpAppntSchema.getAddressNo();
        this.AppntGrade = aLCGrpAppntSchema.getAppntGrade();
        this.Name = aLCGrpAppntSchema.getName();
        this.PostalAddress = aLCGrpAppntSchema.getPostalAddress();
        this.ZipCode = aLCGrpAppntSchema.getZipCode();
        this.Phone = aLCGrpAppntSchema.getPhone();
        this.Password = aLCGrpAppntSchema.getPassword();
        this.State = aLCGrpAppntSchema.getState();
        this.AppntType = aLCGrpAppntSchema.getAppntType();
        this.RelationToInsured = aLCGrpAppntSchema.getRelationToInsured();
        this.Operator = aLCGrpAppntSchema.getOperator();
        this.MakeDate = fDate.getDate( aLCGrpAppntSchema.getMakeDate());
        this.MakeTime = aLCGrpAppntSchema.getMakeTime();
        this.ModifyDate = fDate.getDate( aLCGrpAppntSchema.getModifyDate());
        this.ModifyTime = aLCGrpAppntSchema.getModifyTime();
        this.BusiCategory = aLCGrpAppntSchema.getBusiCategory();
        this.GrpNature = aLCGrpAppntSchema.getGrpNature();
        this.SumNumPeople = aLCGrpAppntSchema.getSumNumPeople();
        this.MainBusiness = aLCGrpAppntSchema.getMainBusiness();
        this.Corporation = aLCGrpAppntSchema.getCorporation();
        this.CorIDType = aLCGrpAppntSchema.getCorIDType();
        this.CorID = aLCGrpAppntSchema.getCorID();
        this.CorIDExpiryDate = fDate.getDate( aLCGrpAppntSchema.getCorIDExpiryDate());
        this.OnJobNumber = aLCGrpAppntSchema.getOnJobNumber();
        this.RetireNumber = aLCGrpAppntSchema.getRetireNumber();
        this.OtherNumber = aLCGrpAppntSchema.getOtherNumber();
        this.RgtCapital = aLCGrpAppntSchema.getRgtCapital();
        this.TotalAssets = aLCGrpAppntSchema.getTotalAssets();
        this.NetProfitRate = aLCGrpAppntSchema.getNetProfitRate();
        this.Satrap = aLCGrpAppntSchema.getSatrap();
        this.ActuCtrl = aLCGrpAppntSchema.getActuCtrl();
        this.License = aLCGrpAppntSchema.getLicense();
        this.SocialInsuCode = aLCGrpAppntSchema.getSocialInsuCode();
        this.OrganizationCode = aLCGrpAppntSchema.getOrganizationCode();
        this.TaxCode = aLCGrpAppntSchema.getTaxCode();
        this.Fax = aLCGrpAppntSchema.getFax();
        this.EMail = aLCGrpAppntSchema.getEMail();
        this.FoundDate = fDate.getDate( aLCGrpAppntSchema.getFoundDate());
        this.Remark = aLCGrpAppntSchema.getRemark();
        this.Segment1 = aLCGrpAppntSchema.getSegment1();
        this.Segment2 = aLCGrpAppntSchema.getSegment2();
        this.Segment3 = aLCGrpAppntSchema.getSegment3();
        this.ManageCom = aLCGrpAppntSchema.getManageCom();
        this.ComCode = aLCGrpAppntSchema.getComCode();
        this.ModifyOperator = aLCGrpAppntSchema.getModifyOperator();
        this.Sex = aLCGrpAppntSchema.getSex();
        this.NativePlace = aLCGrpAppntSchema.getNativePlace();
        this.Occupation = aLCGrpAppntSchema.getOccupation();
        this.OccupationCode = aLCGrpAppntSchema.getOccupationCode();
        this.TaxPayerType = aLCGrpAppntSchema.getTaxPayerType();
        this.FixedPlan = aLCGrpAppntSchema.getFixedPlan();
        this.AddTaxFlag = aLCGrpAppntSchema.getAddTaxFlag();
        this.AddTaxDate = fDate.getDate( aLCGrpAppntSchema.getAddTaxDate());
        this.BankAccTaxNo = aLCGrpAppntSchema.getBankAccTaxNo();
        this.BankTaxName = aLCGrpAppntSchema.getBankTaxName();
        this.BankTaxCode = aLCGrpAppntSchema.getBankTaxCode();
        this.GrpAppIDType = aLCGrpAppntSchema.getGrpAppIDType();
        this.GrpAppIDExpDate = fDate.getDate( aLCGrpAppntSchema.getGrpAppIDExpDate());
        this.CtrPeople = aLCGrpAppntSchema.getCtrPeople();
        this.Peoples3 = aLCGrpAppntSchema.getPeoples3();
        this.RelaPeoples = aLCGrpAppntSchema.getRelaPeoples();
        this.RelaMatePeoples = aLCGrpAppntSchema.getRelaMatePeoples();
        this.RelaYoungPeoples = aLCGrpAppntSchema.getRelaYoungPeoples();
        this.RelaOtherPeoples = aLCGrpAppntSchema.getRelaOtherPeoples();
    }

    /**
    * 使用 ResultSet 中的第 i 行给 Schema 赋值
    * @param: rs ResultSet
    * @param: i int
    * @return: boolean
    **/
    public boolean setSchema(ResultSet rs,int i) {
        try {
            //rs.absolute(i);        // 非滚动游标
            if( rs.getString("GrpContNo") == null )
                this.GrpContNo = null;
            else
                this.GrpContNo = rs.getString("GrpContNo").trim();

            if( rs.getString("CustomerNo") == null )
                this.CustomerNo = null;
            else
                this.CustomerNo = rs.getString("CustomerNo").trim();

            if( rs.getString("PrtNo") == null )
                this.PrtNo = null;
            else
                this.PrtNo = rs.getString("PrtNo").trim();

            if( rs.getString("AddressNo") == null )
                this.AddressNo = null;
            else
                this.AddressNo = rs.getString("AddressNo").trim();

            if( rs.getString("AppntGrade") == null )
                this.AppntGrade = null;
            else
                this.AppntGrade = rs.getString("AppntGrade").trim();

            if( rs.getString("Name") == null )
                this.Name = null;
            else
                this.Name = rs.getString("Name").trim();

            if( rs.getString("PostalAddress") == null )
                this.PostalAddress = null;
            else
                this.PostalAddress = rs.getString("PostalAddress").trim();

            if( rs.getString("ZipCode") == null )
                this.ZipCode = null;
            else
                this.ZipCode = rs.getString("ZipCode").trim();

            if( rs.getString("Phone") == null )
                this.Phone = null;
            else
                this.Phone = rs.getString("Phone").trim();

            if( rs.getString("Password") == null )
                this.Password = null;
            else
                this.Password = rs.getString("Password").trim();

            if( rs.getString("State") == null )
                this.State = null;
            else
                this.State = rs.getString("State").trim();

            if( rs.getString("AppntType") == null )
                this.AppntType = null;
            else
                this.AppntType = rs.getString("AppntType").trim();

            if( rs.getString("RelationToInsured") == null )
                this.RelationToInsured = null;
            else
                this.RelationToInsured = rs.getString("RelationToInsured").trim();

            if( rs.getString("Operator") == null )
                this.Operator = null;
            else
                this.Operator = rs.getString("Operator").trim();

            this.MakeDate = rs.getDate("MakeDate");
            if( rs.getString("MakeTime") == null )
                this.MakeTime = null;
            else
                this.MakeTime = rs.getString("MakeTime").trim();

            this.ModifyDate = rs.getDate("ModifyDate");
            if( rs.getString("ModifyTime") == null )
                this.ModifyTime = null;
            else
                this.ModifyTime = rs.getString("ModifyTime").trim();

            if( rs.getString("BusiCategory") == null )
                this.BusiCategory = null;
            else
                this.BusiCategory = rs.getString("BusiCategory").trim();

            if( rs.getString("GrpNature") == null )
                this.GrpNature = null;
            else
                this.GrpNature = rs.getString("GrpNature").trim();

            this.SumNumPeople = rs.getInt("SumNumPeople");
            if( rs.getString("MainBusiness") == null )
                this.MainBusiness = null;
            else
                this.MainBusiness = rs.getString("MainBusiness").trim();

            if( rs.getString("Corporation") == null )
                this.Corporation = null;
            else
                this.Corporation = rs.getString("Corporation").trim();

            if( rs.getString("CorIDType") == null )
                this.CorIDType = null;
            else
                this.CorIDType = rs.getString("CorIDType").trim();

            if( rs.getString("CorID") == null )
                this.CorID = null;
            else
                this.CorID = rs.getString("CorID").trim();

            this.CorIDExpiryDate = rs.getDate("CorIDExpiryDate");
            this.OnJobNumber = rs.getInt("OnJobNumber");
            this.RetireNumber = rs.getInt("RetireNumber");
            this.OtherNumber = rs.getInt("OtherNumber");
            this.RgtCapital = rs.getDouble("RgtCapital");
            this.TotalAssets = rs.getDouble("TotalAssets");
            this.NetProfitRate = rs.getDouble("NetProfitRate");
            if( rs.getString("Satrap") == null )
                this.Satrap = null;
            else
                this.Satrap = rs.getString("Satrap").trim();

            if( rs.getString("ActuCtrl") == null )
                this.ActuCtrl = null;
            else
                this.ActuCtrl = rs.getString("ActuCtrl").trim();

            if( rs.getString("License") == null )
                this.License = null;
            else
                this.License = rs.getString("License").trim();

            if( rs.getString("SocialInsuCode") == null )
                this.SocialInsuCode = null;
            else
                this.SocialInsuCode = rs.getString("SocialInsuCode").trim();

            if( rs.getString("OrganizationCode") == null )
                this.OrganizationCode = null;
            else
                this.OrganizationCode = rs.getString("OrganizationCode").trim();

            if( rs.getString("TaxCode") == null )
                this.TaxCode = null;
            else
                this.TaxCode = rs.getString("TaxCode").trim();

            if( rs.getString("Fax") == null )
                this.Fax = null;
            else
                this.Fax = rs.getString("Fax").trim();

            if( rs.getString("EMail") == null )
                this.EMail = null;
            else
                this.EMail = rs.getString("EMail").trim();

            this.FoundDate = rs.getDate("FoundDate");
            if( rs.getString("Remark") == null )
                this.Remark = null;
            else
                this.Remark = rs.getString("Remark").trim();

            if( rs.getString("Segment1") == null )
                this.Segment1 = null;
            else
                this.Segment1 = rs.getString("Segment1").trim();

            if( rs.getString("Segment2") == null )
                this.Segment2 = null;
            else
                this.Segment2 = rs.getString("Segment2").trim();

            if( rs.getString("Segment3") == null )
                this.Segment3 = null;
            else
                this.Segment3 = rs.getString("Segment3").trim();

            if( rs.getString("ManageCom") == null )
                this.ManageCom = null;
            else
                this.ManageCom = rs.getString("ManageCom").trim();

            if( rs.getString("ComCode") == null )
                this.ComCode = null;
            else
                this.ComCode = rs.getString("ComCode").trim();

            if( rs.getString("ModifyOperator") == null )
                this.ModifyOperator = null;
            else
                this.ModifyOperator = rs.getString("ModifyOperator").trim();

            if( rs.getString("Sex") == null )
                this.Sex = null;
            else
                this.Sex = rs.getString("Sex").trim();

            if( rs.getString("NativePlace") == null )
                this.NativePlace = null;
            else
                this.NativePlace = rs.getString("NativePlace").trim();

            if( rs.getString("Occupation") == null )
                this.Occupation = null;
            else
                this.Occupation = rs.getString("Occupation").trim();

            if( rs.getString("OccupationCode") == null )
                this.OccupationCode = null;
            else
                this.OccupationCode = rs.getString("OccupationCode").trim();

            if( rs.getString("TaxPayerType") == null )
                this.TaxPayerType = null;
            else
                this.TaxPayerType = rs.getString("TaxPayerType").trim();

            if( rs.getString("FixedPlan") == null )
                this.FixedPlan = null;
            else
                this.FixedPlan = rs.getString("FixedPlan").trim();

            if( rs.getString("AddTaxFlag") == null )
                this.AddTaxFlag = null;
            else
                this.AddTaxFlag = rs.getString("AddTaxFlag").trim();

            this.AddTaxDate = rs.getDate("AddTaxDate");
            if( rs.getString("BankAccTaxNo") == null )
                this.BankAccTaxNo = null;
            else
                this.BankAccTaxNo = rs.getString("BankAccTaxNo").trim();

            if( rs.getString("BankTaxName") == null )
                this.BankTaxName = null;
            else
                this.BankTaxName = rs.getString("BankTaxName").trim();

            if( rs.getString("BankTaxCode") == null )
                this.BankTaxCode = null;
            else
                this.BankTaxCode = rs.getString("BankTaxCode").trim();

            if( rs.getString("GrpAppIDType") == null )
                this.GrpAppIDType = null;
            else
                this.GrpAppIDType = rs.getString("GrpAppIDType").trim();

            this.GrpAppIDExpDate = rs.getDate("GrpAppIDExpDate");
            if( rs.getString("CtrPeople") == null )
                this.CtrPeople = null;
            else
                this.CtrPeople = rs.getString("CtrPeople").trim();

            this.Peoples3 = rs.getInt("Peoples3");
            this.RelaPeoples = rs.getInt("RelaPeoples");
            this.RelaMatePeoples = rs.getInt("RelaMatePeoples");
            this.RelaYoungPeoples = rs.getInt("RelaYoungPeoples");
            this.RelaOtherPeoples = rs.getInt("RelaOtherPeoples");
        }
        catch(SQLException sqle) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LCGrpAppntSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = ExceptionUtils.exceptionToString(sqle);
            this.mErrors .addOneError(tError);
            return false;
        }
        return true;
    }

    public LCGrpAppntSchema getSchema() {
        LCGrpAppntSchema aLCGrpAppntSchema = new LCGrpAppntSchema();
        aLCGrpAppntSchema.setSchema(this);
        return aLCGrpAppntSchema;
    }

    public LCGrpAppntDB getDB() {
        LCGrpAppntDB aDBOper = new LCGrpAppntDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
    * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLCGrpAppnt描述/A>表字段
    * @return: String 返回打包后字符串
    **/
    public String encode() {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(GrpContNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(CustomerNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(PrtNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AddressNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AppntGrade)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Name)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(PostalAddress)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ZipCode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Phone)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Password)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(State)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AppntType)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(RelationToInsured)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ModifyTime)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(BusiCategory)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(GrpNature)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(SumNumPeople));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MainBusiness)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Corporation)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(CorIDType)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(CorID)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( CorIDExpiryDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(OnJobNumber));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(RetireNumber));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(OtherNumber));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(RgtCapital));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(TotalAssets));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(NetProfitRate));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Satrap)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ActuCtrl)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(License)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(SocialInsuCode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(OrganizationCode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(TaxCode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Fax)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(EMail)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( FoundDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Remark)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Segment1)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Segment2)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Segment3)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ManageCom)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ComCode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ModifyOperator)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Sex)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(NativePlace)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Occupation)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(OccupationCode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(TaxPayerType)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(FixedPlan)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AddTaxFlag)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( AddTaxDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(BankAccTaxNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(BankTaxName)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(BankTaxCode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(GrpAppIDType)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( GrpAppIDExpDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(CtrPeople)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(Peoples3));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(RelaPeoples));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(RelaMatePeoples));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(RelaYoungPeoples));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(RelaOtherPeoples));
        return strReturn.toString();
    }

    /**
    * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLCGrpAppnt>历史记账凭证主表信息</A>表字段
    * @param: strMessage String 包含一条纪录数据的字符串
    * @return: boolean
    **/
    public boolean decode(String strMessage) {
        try {
            GrpContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
            CustomerNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
            PrtNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
            AddressNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
            AppntGrade = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
            Name = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
            PostalAddress = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
            ZipCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
            Phone = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
            Password = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
            State = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
            AppntType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
            RelationToInsured = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER );
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16, SysConst.PACKAGESPILTER );
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18, SysConst.PACKAGESPILTER );
            BusiCategory = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19, SysConst.PACKAGESPILTER );
            GrpNature = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20, SysConst.PACKAGESPILTER );
            SumNumPeople = new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,21, SysConst.PACKAGESPILTER))).intValue();
            MainBusiness = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 22, SysConst.PACKAGESPILTER );
            Corporation = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 23, SysConst.PACKAGESPILTER );
            CorIDType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 24, SysConst.PACKAGESPILTER );
            CorID = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 25, SysConst.PACKAGESPILTER );
            CorIDExpiryDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 26, SysConst.PACKAGESPILTER));
            OnJobNumber = new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,27, SysConst.PACKAGESPILTER))).intValue();
            RetireNumber = new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,28, SysConst.PACKAGESPILTER))).intValue();
            OtherNumber = new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,29, SysConst.PACKAGESPILTER))).intValue();
            RgtCapital = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,30, SysConst.PACKAGESPILTER))).doubleValue();
            TotalAssets = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,31, SysConst.PACKAGESPILTER))).doubleValue();
            NetProfitRate = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,32, SysConst.PACKAGESPILTER))).doubleValue();
            Satrap = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 33, SysConst.PACKAGESPILTER );
            ActuCtrl = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 34, SysConst.PACKAGESPILTER );
            License = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 35, SysConst.PACKAGESPILTER );
            SocialInsuCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 36, SysConst.PACKAGESPILTER );
            OrganizationCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 37, SysConst.PACKAGESPILTER );
            TaxCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 38, SysConst.PACKAGESPILTER );
            Fax = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 39, SysConst.PACKAGESPILTER );
            EMail = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 40, SysConst.PACKAGESPILTER );
            FoundDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 41, SysConst.PACKAGESPILTER));
            Remark = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 42, SysConst.PACKAGESPILTER );
            Segment1 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 43, SysConst.PACKAGESPILTER );
            Segment2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 44, SysConst.PACKAGESPILTER );
            Segment3 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 45, SysConst.PACKAGESPILTER );
            ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 46, SysConst.PACKAGESPILTER );
            ComCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 47, SysConst.PACKAGESPILTER );
            ModifyOperator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 48, SysConst.PACKAGESPILTER );
            Sex = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 49, SysConst.PACKAGESPILTER );
            NativePlace = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 50, SysConst.PACKAGESPILTER );
            Occupation = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 51, SysConst.PACKAGESPILTER );
            OccupationCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 52, SysConst.PACKAGESPILTER );
            TaxPayerType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 53, SysConst.PACKAGESPILTER );
            FixedPlan = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 54, SysConst.PACKAGESPILTER );
            AddTaxFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 55, SysConst.PACKAGESPILTER );
            AddTaxDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 56, SysConst.PACKAGESPILTER));
            BankAccTaxNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 57, SysConst.PACKAGESPILTER );
            BankTaxName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 58, SysConst.PACKAGESPILTER );
            BankTaxCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 59, SysConst.PACKAGESPILTER );
            GrpAppIDType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 60, SysConst.PACKAGESPILTER );
            GrpAppIDExpDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 61, SysConst.PACKAGESPILTER));
            CtrPeople = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 62, SysConst.PACKAGESPILTER );
            Peoples3 = new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,63, SysConst.PACKAGESPILTER))).intValue();
            RelaPeoples = new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,64, SysConst.PACKAGESPILTER))).intValue();
            RelaMatePeoples = new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,65, SysConst.PACKAGESPILTER))).intValue();
            RelaYoungPeoples = new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,66, SysConst.PACKAGESPILTER))).intValue();
            RelaOtherPeoples = new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,67, SysConst.PACKAGESPILTER))).intValue();
        }
        catch(NumberFormatException ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LCGrpAppntSchema";
            tError.functionName = "decode";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            return false;
        }
        return true;
    }

    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("GrpContNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GrpContNo));
        }
        if (FCode.equalsIgnoreCase("CustomerNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CustomerNo));
        }
        if (FCode.equalsIgnoreCase("PrtNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PrtNo));
        }
        if (FCode.equalsIgnoreCase("AddressNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AddressNo));
        }
        if (FCode.equalsIgnoreCase("AppntGrade")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AppntGrade));
        }
        if (FCode.equalsIgnoreCase("Name")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Name));
        }
        if (FCode.equalsIgnoreCase("PostalAddress")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PostalAddress));
        }
        if (FCode.equalsIgnoreCase("ZipCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ZipCode));
        }
        if (FCode.equalsIgnoreCase("Phone")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Phone));
        }
        if (FCode.equalsIgnoreCase("Password")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Password));
        }
        if (FCode.equalsIgnoreCase("State")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(State));
        }
        if (FCode.equalsIgnoreCase("AppntType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AppntType));
        }
        if (FCode.equalsIgnoreCase("RelationToInsured")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RelationToInsured));
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (FCode.equalsIgnoreCase("BusiCategory")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BusiCategory));
        }
        if (FCode.equalsIgnoreCase("GrpNature")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GrpNature));
        }
        if (FCode.equalsIgnoreCase("SumNumPeople")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SumNumPeople));
        }
        if (FCode.equalsIgnoreCase("MainBusiness")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MainBusiness));
        }
        if (FCode.equalsIgnoreCase("Corporation")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Corporation));
        }
        if (FCode.equalsIgnoreCase("CorIDType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CorIDType));
        }
        if (FCode.equalsIgnoreCase("CorID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CorID));
        }
        if (FCode.equalsIgnoreCase("CorIDExpiryDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getCorIDExpiryDate()));
        }
        if (FCode.equalsIgnoreCase("OnJobNumber")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OnJobNumber));
        }
        if (FCode.equalsIgnoreCase("RetireNumber")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RetireNumber));
        }
        if (FCode.equalsIgnoreCase("OtherNumber")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OtherNumber));
        }
        if (FCode.equalsIgnoreCase("RgtCapital")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RgtCapital));
        }
        if (FCode.equalsIgnoreCase("TotalAssets")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(TotalAssets));
        }
        if (FCode.equalsIgnoreCase("NetProfitRate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(NetProfitRate));
        }
        if (FCode.equalsIgnoreCase("Satrap")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Satrap));
        }
        if (FCode.equalsIgnoreCase("ActuCtrl")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ActuCtrl));
        }
        if (FCode.equalsIgnoreCase("License")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(License));
        }
        if (FCode.equalsIgnoreCase("SocialInsuCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SocialInsuCode));
        }
        if (FCode.equalsIgnoreCase("OrganizationCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OrganizationCode));
        }
        if (FCode.equalsIgnoreCase("TaxCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(TaxCode));
        }
        if (FCode.equalsIgnoreCase("Fax")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Fax));
        }
        if (FCode.equalsIgnoreCase("EMail")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(EMail));
        }
        if (FCode.equalsIgnoreCase("FoundDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getFoundDate()));
        }
        if (FCode.equalsIgnoreCase("Remark")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Remark));
        }
        if (FCode.equalsIgnoreCase("Segment1")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Segment1));
        }
        if (FCode.equalsIgnoreCase("Segment2")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Segment2));
        }
        if (FCode.equalsIgnoreCase("Segment3")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Segment3));
        }
        if (FCode.equalsIgnoreCase("ManageCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
        }
        if (FCode.equalsIgnoreCase("ComCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ComCode));
        }
        if (FCode.equalsIgnoreCase("ModifyOperator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyOperator));
        }
        if (FCode.equalsIgnoreCase("Sex")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Sex));
        }
        if (FCode.equalsIgnoreCase("NativePlace")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(NativePlace));
        }
        if (FCode.equalsIgnoreCase("Occupation")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Occupation));
        }
        if (FCode.equalsIgnoreCase("OccupationCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OccupationCode));
        }
        if (FCode.equalsIgnoreCase("TaxPayerType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(TaxPayerType));
        }
        if (FCode.equalsIgnoreCase("FixedPlan")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FixedPlan));
        }
        if (FCode.equalsIgnoreCase("AddTaxFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AddTaxFlag));
        }
        if (FCode.equalsIgnoreCase("AddTaxDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getAddTaxDate()));
        }
        if (FCode.equalsIgnoreCase("BankAccTaxNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BankAccTaxNo));
        }
        if (FCode.equalsIgnoreCase("BankTaxName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BankTaxName));
        }
        if (FCode.equalsIgnoreCase("BankTaxCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BankTaxCode));
        }
        if (FCode.equalsIgnoreCase("GrpAppIDType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GrpAppIDType));
        }
        if (FCode.equalsIgnoreCase("GrpAppIDExpDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getGrpAppIDExpDate()));
        }
        if (FCode.equalsIgnoreCase("CtrPeople")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CtrPeople));
        }
        if (FCode.equalsIgnoreCase("Peoples3")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Peoples3));
        }
        if (FCode.equalsIgnoreCase("RelaPeoples")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RelaPeoples));
        }
        if (FCode.equalsIgnoreCase("RelaMatePeoples")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RelaMatePeoples));
        }
        if (FCode.equalsIgnoreCase("RelaYoungPeoples")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RelaYoungPeoples));
        }
        if (FCode.equalsIgnoreCase("RelaOtherPeoples")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RelaOtherPeoples));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(GrpContNo);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(CustomerNo);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(PrtNo);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(AddressNo);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(AppntGrade);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(Name);
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(PostalAddress);
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(ZipCode);
                break;
            case 8:
                strFieldValue = StrTool.GBKToUnicode(Phone);
                break;
            case 9:
                strFieldValue = StrTool.GBKToUnicode(Password);
                break;
            case 10:
                strFieldValue = StrTool.GBKToUnicode(State);
                break;
            case 11:
                strFieldValue = StrTool.GBKToUnicode(AppntType);
                break;
            case 12:
                strFieldValue = StrTool.GBKToUnicode(RelationToInsured);
                break;
            case 13:
                strFieldValue = StrTool.GBKToUnicode(Operator);
                break;
            case 14:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
                break;
            case 15:
                strFieldValue = StrTool.GBKToUnicode(MakeTime);
                break;
            case 16:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
                break;
            case 17:
                strFieldValue = StrTool.GBKToUnicode(ModifyTime);
                break;
            case 18:
                strFieldValue = StrTool.GBKToUnicode(BusiCategory);
                break;
            case 19:
                strFieldValue = StrTool.GBKToUnicode(GrpNature);
                break;
            case 20:
                strFieldValue = String.valueOf(SumNumPeople);
                break;
            case 21:
                strFieldValue = StrTool.GBKToUnicode(MainBusiness);
                break;
            case 22:
                strFieldValue = StrTool.GBKToUnicode(Corporation);
                break;
            case 23:
                strFieldValue = StrTool.GBKToUnicode(CorIDType);
                break;
            case 24:
                strFieldValue = StrTool.GBKToUnicode(CorID);
                break;
            case 25:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getCorIDExpiryDate()));
                break;
            case 26:
                strFieldValue = String.valueOf(OnJobNumber);
                break;
            case 27:
                strFieldValue = String.valueOf(RetireNumber);
                break;
            case 28:
                strFieldValue = String.valueOf(OtherNumber);
                break;
            case 29:
                strFieldValue = String.valueOf(RgtCapital);
                break;
            case 30:
                strFieldValue = String.valueOf(TotalAssets);
                break;
            case 31:
                strFieldValue = String.valueOf(NetProfitRate);
                break;
            case 32:
                strFieldValue = StrTool.GBKToUnicode(Satrap);
                break;
            case 33:
                strFieldValue = StrTool.GBKToUnicode(ActuCtrl);
                break;
            case 34:
                strFieldValue = StrTool.GBKToUnicode(License);
                break;
            case 35:
                strFieldValue = StrTool.GBKToUnicode(SocialInsuCode);
                break;
            case 36:
                strFieldValue = StrTool.GBKToUnicode(OrganizationCode);
                break;
            case 37:
                strFieldValue = StrTool.GBKToUnicode(TaxCode);
                break;
            case 38:
                strFieldValue = StrTool.GBKToUnicode(Fax);
                break;
            case 39:
                strFieldValue = StrTool.GBKToUnicode(EMail);
                break;
            case 40:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getFoundDate()));
                break;
            case 41:
                strFieldValue = StrTool.GBKToUnicode(Remark);
                break;
            case 42:
                strFieldValue = StrTool.GBKToUnicode(Segment1);
                break;
            case 43:
                strFieldValue = StrTool.GBKToUnicode(Segment2);
                break;
            case 44:
                strFieldValue = StrTool.GBKToUnicode(Segment3);
                break;
            case 45:
                strFieldValue = StrTool.GBKToUnicode(ManageCom);
                break;
            case 46:
                strFieldValue = StrTool.GBKToUnicode(ComCode);
                break;
            case 47:
                strFieldValue = StrTool.GBKToUnicode(ModifyOperator);
                break;
            case 48:
                strFieldValue = StrTool.GBKToUnicode(Sex);
                break;
            case 49:
                strFieldValue = StrTool.GBKToUnicode(NativePlace);
                break;
            case 50:
                strFieldValue = StrTool.GBKToUnicode(Occupation);
                break;
            case 51:
                strFieldValue = StrTool.GBKToUnicode(OccupationCode);
                break;
            case 52:
                strFieldValue = StrTool.GBKToUnicode(TaxPayerType);
                break;
            case 53:
                strFieldValue = StrTool.GBKToUnicode(FixedPlan);
                break;
            case 54:
                strFieldValue = StrTool.GBKToUnicode(AddTaxFlag);
                break;
            case 55:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getAddTaxDate()));
                break;
            case 56:
                strFieldValue = StrTool.GBKToUnicode(BankAccTaxNo);
                break;
            case 57:
                strFieldValue = StrTool.GBKToUnicode(BankTaxName);
                break;
            case 58:
                strFieldValue = StrTool.GBKToUnicode(BankTaxCode);
                break;
            case 59:
                strFieldValue = StrTool.GBKToUnicode(GrpAppIDType);
                break;
            case 60:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getGrpAppIDExpDate()));
                break;
            case 61:
                strFieldValue = StrTool.GBKToUnicode(CtrPeople);
                break;
            case 62:
                strFieldValue = String.valueOf(Peoples3);
                break;
            case 63:
                strFieldValue = String.valueOf(RelaPeoples);
                break;
            case 64:
                strFieldValue = String.valueOf(RelaMatePeoples);
                break;
            case 65:
                strFieldValue = String.valueOf(RelaYoungPeoples);
                break;
            case 66:
                strFieldValue = String.valueOf(RelaOtherPeoples);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("GrpContNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                GrpContNo = FValue.trim();
            }
            else
                GrpContNo = null;
        }
        if (FCode.equalsIgnoreCase("CustomerNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                CustomerNo = FValue.trim();
            }
            else
                CustomerNo = null;
        }
        if (FCode.equalsIgnoreCase("PrtNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                PrtNo = FValue.trim();
            }
            else
                PrtNo = null;
        }
        if (FCode.equalsIgnoreCase("AddressNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                AddressNo = FValue.trim();
            }
            else
                AddressNo = null;
        }
        if (FCode.equalsIgnoreCase("AppntGrade")) {
            if( FValue != null && !FValue.equals(""))
            {
                AppntGrade = FValue.trim();
            }
            else
                AppntGrade = null;
        }
        if (FCode.equalsIgnoreCase("Name")) {
            if( FValue != null && !FValue.equals(""))
            {
                Name = FValue.trim();
            }
            else
                Name = null;
        }
        if (FCode.equalsIgnoreCase("PostalAddress")) {
            if( FValue != null && !FValue.equals(""))
            {
                PostalAddress = FValue.trim();
            }
            else
                PostalAddress = null;
        }
        if (FCode.equalsIgnoreCase("ZipCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                ZipCode = FValue.trim();
            }
            else
                ZipCode = null;
        }
        if (FCode.equalsIgnoreCase("Phone")) {
            if( FValue != null && !FValue.equals(""))
            {
                Phone = FValue.trim();
            }
            else
                Phone = null;
        }
        if (FCode.equalsIgnoreCase("Password")) {
            if( FValue != null && !FValue.equals(""))
            {
                Password = FValue.trim();
            }
            else
                Password = null;
        }
        if (FCode.equalsIgnoreCase("State")) {
            if( FValue != null && !FValue.equals(""))
            {
                State = FValue.trim();
            }
            else
                State = null;
        }
        if (FCode.equalsIgnoreCase("AppntType")) {
            if( FValue != null && !FValue.equals(""))
            {
                AppntType = FValue.trim();
            }
            else
                AppntType = null;
        }
        if (FCode.equalsIgnoreCase("RelationToInsured")) {
            if( FValue != null && !FValue.equals(""))
            {
                RelationToInsured = FValue.trim();
            }
            else
                RelationToInsured = null;
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            if( FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
                Operator = null;
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if(FValue != null && !FValue.equals("")) {
                MakeDate = fDate.getDate( FValue );
            }
            else
                MakeDate = null;
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
                MakeTime = null;
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if(FValue != null && !FValue.equals("")) {
                ModifyDate = fDate.getDate( FValue );
            }
            else
                ModifyDate = null;
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
                ModifyTime = null;
        }
        if (FCode.equalsIgnoreCase("BusiCategory")) {
            if( FValue != null && !FValue.equals(""))
            {
                BusiCategory = FValue.trim();
            }
            else
                BusiCategory = null;
        }
        if (FCode.equalsIgnoreCase("GrpNature")) {
            if( FValue != null && !FValue.equals(""))
            {
                GrpNature = FValue.trim();
            }
            else
                GrpNature = null;
        }
        if (FCode.equalsIgnoreCase("SumNumPeople")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                SumNumPeople = i;
            }
        }
        if (FCode.equalsIgnoreCase("MainBusiness")) {
            if( FValue != null && !FValue.equals(""))
            {
                MainBusiness = FValue.trim();
            }
            else
                MainBusiness = null;
        }
        if (FCode.equalsIgnoreCase("Corporation")) {
            if( FValue != null && !FValue.equals(""))
            {
                Corporation = FValue.trim();
            }
            else
                Corporation = null;
        }
        if (FCode.equalsIgnoreCase("CorIDType")) {
            if( FValue != null && !FValue.equals(""))
            {
                CorIDType = FValue.trim();
            }
            else
                CorIDType = null;
        }
        if (FCode.equalsIgnoreCase("CorID")) {
            if( FValue != null && !FValue.equals(""))
            {
                CorID = FValue.trim();
            }
            else
                CorID = null;
        }
        if (FCode.equalsIgnoreCase("CorIDExpiryDate")) {
            if(FValue != null && !FValue.equals("")) {
                CorIDExpiryDate = fDate.getDate( FValue );
            }
            else
                CorIDExpiryDate = null;
        }
        if (FCode.equalsIgnoreCase("OnJobNumber")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                OnJobNumber = i;
            }
        }
        if (FCode.equalsIgnoreCase("RetireNumber")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                RetireNumber = i;
            }
        }
        if (FCode.equalsIgnoreCase("OtherNumber")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                OtherNumber = i;
            }
        }
        if (FCode.equalsIgnoreCase("RgtCapital")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                RgtCapital = d;
            }
        }
        if (FCode.equalsIgnoreCase("TotalAssets")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                TotalAssets = d;
            }
        }
        if (FCode.equalsIgnoreCase("NetProfitRate")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                NetProfitRate = d;
            }
        }
        if (FCode.equalsIgnoreCase("Satrap")) {
            if( FValue != null && !FValue.equals(""))
            {
                Satrap = FValue.trim();
            }
            else
                Satrap = null;
        }
        if (FCode.equalsIgnoreCase("ActuCtrl")) {
            if( FValue != null && !FValue.equals(""))
            {
                ActuCtrl = FValue.trim();
            }
            else
                ActuCtrl = null;
        }
        if (FCode.equalsIgnoreCase("License")) {
            if( FValue != null && !FValue.equals(""))
            {
                License = FValue.trim();
            }
            else
                License = null;
        }
        if (FCode.equalsIgnoreCase("SocialInsuCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                SocialInsuCode = FValue.trim();
            }
            else
                SocialInsuCode = null;
        }
        if (FCode.equalsIgnoreCase("OrganizationCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                OrganizationCode = FValue.trim();
            }
            else
                OrganizationCode = null;
        }
        if (FCode.equalsIgnoreCase("TaxCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                TaxCode = FValue.trim();
            }
            else
                TaxCode = null;
        }
        if (FCode.equalsIgnoreCase("Fax")) {
            if( FValue != null && !FValue.equals(""))
            {
                Fax = FValue.trim();
            }
            else
                Fax = null;
        }
        if (FCode.equalsIgnoreCase("EMail")) {
            if( FValue != null && !FValue.equals(""))
            {
                EMail = FValue.trim();
            }
            else
                EMail = null;
        }
        if (FCode.equalsIgnoreCase("FoundDate")) {
            if(FValue != null && !FValue.equals("")) {
                FoundDate = fDate.getDate( FValue );
            }
            else
                FoundDate = null;
        }
        if (FCode.equalsIgnoreCase("Remark")) {
            if( FValue != null && !FValue.equals(""))
            {
                Remark = FValue.trim();
            }
            else
                Remark = null;
        }
        if (FCode.equalsIgnoreCase("Segment1")) {
            if( FValue != null && !FValue.equals(""))
            {
                Segment1 = FValue.trim();
            }
            else
                Segment1 = null;
        }
        if (FCode.equalsIgnoreCase("Segment2")) {
            if( FValue != null && !FValue.equals(""))
            {
                Segment2 = FValue.trim();
            }
            else
                Segment2 = null;
        }
        if (FCode.equalsIgnoreCase("Segment3")) {
            if( FValue != null && !FValue.equals(""))
            {
                Segment3 = FValue.trim();
            }
            else
                Segment3 = null;
        }
        if (FCode.equalsIgnoreCase("ManageCom")) {
            if( FValue != null && !FValue.equals(""))
            {
                ManageCom = FValue.trim();
            }
            else
                ManageCom = null;
        }
        if (FCode.equalsIgnoreCase("ComCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                ComCode = FValue.trim();
            }
            else
                ComCode = null;
        }
        if (FCode.equalsIgnoreCase("ModifyOperator")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyOperator = FValue.trim();
            }
            else
                ModifyOperator = null;
        }
        if (FCode.equalsIgnoreCase("Sex")) {
            if( FValue != null && !FValue.equals(""))
            {
                Sex = FValue.trim();
            }
            else
                Sex = null;
        }
        if (FCode.equalsIgnoreCase("NativePlace")) {
            if( FValue != null && !FValue.equals(""))
            {
                NativePlace = FValue.trim();
            }
            else
                NativePlace = null;
        }
        if (FCode.equalsIgnoreCase("Occupation")) {
            if( FValue != null && !FValue.equals(""))
            {
                Occupation = FValue.trim();
            }
            else
                Occupation = null;
        }
        if (FCode.equalsIgnoreCase("OccupationCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                OccupationCode = FValue.trim();
            }
            else
                OccupationCode = null;
        }
        if (FCode.equalsIgnoreCase("TaxPayerType")) {
            if( FValue != null && !FValue.equals(""))
            {
                TaxPayerType = FValue.trim();
            }
            else
                TaxPayerType = null;
        }
        if (FCode.equalsIgnoreCase("FixedPlan")) {
            if( FValue != null && !FValue.equals(""))
            {
                FixedPlan = FValue.trim();
            }
            else
                FixedPlan = null;
        }
        if (FCode.equalsIgnoreCase("AddTaxFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                AddTaxFlag = FValue.trim();
            }
            else
                AddTaxFlag = null;
        }
        if (FCode.equalsIgnoreCase("AddTaxDate")) {
            if(FValue != null && !FValue.equals("")) {
                AddTaxDate = fDate.getDate( FValue );
            }
            else
                AddTaxDate = null;
        }
        if (FCode.equalsIgnoreCase("BankAccTaxNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                BankAccTaxNo = FValue.trim();
            }
            else
                BankAccTaxNo = null;
        }
        if (FCode.equalsIgnoreCase("BankTaxName")) {
            if( FValue != null && !FValue.equals(""))
            {
                BankTaxName = FValue.trim();
            }
            else
                BankTaxName = null;
        }
        if (FCode.equalsIgnoreCase("BankTaxCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                BankTaxCode = FValue.trim();
            }
            else
                BankTaxCode = null;
        }
        if (FCode.equalsIgnoreCase("GrpAppIDType")) {
            if( FValue != null && !FValue.equals(""))
            {
                GrpAppIDType = FValue.trim();
            }
            else
                GrpAppIDType = null;
        }
        if (FCode.equalsIgnoreCase("GrpAppIDExpDate")) {
            if(FValue != null && !FValue.equals("")) {
                GrpAppIDExpDate = fDate.getDate( FValue );
            }
            else
                GrpAppIDExpDate = null;
        }
        if (FCode.equalsIgnoreCase("CtrPeople")) {
            if( FValue != null && !FValue.equals(""))
            {
                CtrPeople = FValue.trim();
            }
            else
                CtrPeople = null;
        }
        if (FCode.equalsIgnoreCase("Peoples3")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                Peoples3 = i;
            }
        }
        if (FCode.equalsIgnoreCase("RelaPeoples")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                RelaPeoples = i;
            }
        }
        if (FCode.equalsIgnoreCase("RelaMatePeoples")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                RelaMatePeoples = i;
            }
        }
        if (FCode.equalsIgnoreCase("RelaYoungPeoples")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                RelaYoungPeoples = i;
            }
        }
        if (FCode.equalsIgnoreCase("RelaOtherPeoples")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                RelaOtherPeoples = i;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject) return true;
        if (otherObject == null) return false;
        if (getClass() != otherObject.getClass()) return false;
        LCGrpAppntSchema other = (LCGrpAppntSchema)otherObject;
        return
            GrpContNo.equals(other.getGrpContNo())
            && CustomerNo.equals(other.getCustomerNo())
            && PrtNo.equals(other.getPrtNo())
            && AddressNo.equals(other.getAddressNo())
            && AppntGrade.equals(other.getAppntGrade())
            && Name.equals(other.getName())
            && PostalAddress.equals(other.getPostalAddress())
            && ZipCode.equals(other.getZipCode())
            && Phone.equals(other.getPhone())
            && Password.equals(other.getPassword())
            && State.equals(other.getState())
            && AppntType.equals(other.getAppntType())
            && RelationToInsured.equals(other.getRelationToInsured())
            && Operator.equals(other.getOperator())
            && fDate.getString(MakeDate).equals(other.getMakeDate())
            && MakeTime.equals(other.getMakeTime())
            && fDate.getString(ModifyDate).equals(other.getModifyDate())
            && ModifyTime.equals(other.getModifyTime())
            && BusiCategory.equals(other.getBusiCategory())
            && GrpNature.equals(other.getGrpNature())
            && SumNumPeople == other.getSumNumPeople()
            && MainBusiness.equals(other.getMainBusiness())
            && Corporation.equals(other.getCorporation())
            && CorIDType.equals(other.getCorIDType())
            && CorID.equals(other.getCorID())
            && fDate.getString(CorIDExpiryDate).equals(other.getCorIDExpiryDate())
            && OnJobNumber == other.getOnJobNumber()
            && RetireNumber == other.getRetireNumber()
            && OtherNumber == other.getOtherNumber()
            && RgtCapital == other.getRgtCapital()
            && TotalAssets == other.getTotalAssets()
            && NetProfitRate == other.getNetProfitRate()
            && Satrap.equals(other.getSatrap())
            && ActuCtrl.equals(other.getActuCtrl())
            && License.equals(other.getLicense())
            && SocialInsuCode.equals(other.getSocialInsuCode())
            && OrganizationCode.equals(other.getOrganizationCode())
            && TaxCode.equals(other.getTaxCode())
            && Fax.equals(other.getFax())
            && EMail.equals(other.getEMail())
            && fDate.getString(FoundDate).equals(other.getFoundDate())
            && Remark.equals(other.getRemark())
            && Segment1.equals(other.getSegment1())
            && Segment2.equals(other.getSegment2())
            && Segment3.equals(other.getSegment3())
            && ManageCom.equals(other.getManageCom())
            && ComCode.equals(other.getComCode())
            && ModifyOperator.equals(other.getModifyOperator())
            && Sex.equals(other.getSex())
            && NativePlace.equals(other.getNativePlace())
            && Occupation.equals(other.getOccupation())
            && OccupationCode.equals(other.getOccupationCode())
            && TaxPayerType.equals(other.getTaxPayerType())
            && FixedPlan.equals(other.getFixedPlan())
            && AddTaxFlag.equals(other.getAddTaxFlag())
            && fDate.getString(AddTaxDate).equals(other.getAddTaxDate())
            && BankAccTaxNo.equals(other.getBankAccTaxNo())
            && BankTaxName.equals(other.getBankTaxName())
            && BankTaxCode.equals(other.getBankTaxCode())
            && GrpAppIDType.equals(other.getGrpAppIDType())
            && fDate.getString(GrpAppIDExpDate).equals(other.getGrpAppIDExpDate())
            && CtrPeople.equals(other.getCtrPeople())
            && Peoples3 == other.getPeoples3()
            && RelaPeoples == other.getRelaPeoples()
            && RelaMatePeoples == other.getRelaMatePeoples()
            && RelaYoungPeoples == other.getRelaYoungPeoples()
            && RelaOtherPeoples == other.getRelaOtherPeoples();
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("GrpContNo") ) {
            return 0;
        }
        if( strFieldName.equals("CustomerNo") ) {
            return 1;
        }
        if( strFieldName.equals("PrtNo") ) {
            return 2;
        }
        if( strFieldName.equals("AddressNo") ) {
            return 3;
        }
        if( strFieldName.equals("AppntGrade") ) {
            return 4;
        }
        if( strFieldName.equals("Name") ) {
            return 5;
        }
        if( strFieldName.equals("PostalAddress") ) {
            return 6;
        }
        if( strFieldName.equals("ZipCode") ) {
            return 7;
        }
        if( strFieldName.equals("Phone") ) {
            return 8;
        }
        if( strFieldName.equals("Password") ) {
            return 9;
        }
        if( strFieldName.equals("State") ) {
            return 10;
        }
        if( strFieldName.equals("AppntType") ) {
            return 11;
        }
        if( strFieldName.equals("RelationToInsured") ) {
            return 12;
        }
        if( strFieldName.equals("Operator") ) {
            return 13;
        }
        if( strFieldName.equals("MakeDate") ) {
            return 14;
        }
        if( strFieldName.equals("MakeTime") ) {
            return 15;
        }
        if( strFieldName.equals("ModifyDate") ) {
            return 16;
        }
        if( strFieldName.equals("ModifyTime") ) {
            return 17;
        }
        if( strFieldName.equals("BusiCategory") ) {
            return 18;
        }
        if( strFieldName.equals("GrpNature") ) {
            return 19;
        }
        if( strFieldName.equals("SumNumPeople") ) {
            return 20;
        }
        if( strFieldName.equals("MainBusiness") ) {
            return 21;
        }
        if( strFieldName.equals("Corporation") ) {
            return 22;
        }
        if( strFieldName.equals("CorIDType") ) {
            return 23;
        }
        if( strFieldName.equals("CorID") ) {
            return 24;
        }
        if( strFieldName.equals("CorIDExpiryDate") ) {
            return 25;
        }
        if( strFieldName.equals("OnJobNumber") ) {
            return 26;
        }
        if( strFieldName.equals("RetireNumber") ) {
            return 27;
        }
        if( strFieldName.equals("OtherNumber") ) {
            return 28;
        }
        if( strFieldName.equals("RgtCapital") ) {
            return 29;
        }
        if( strFieldName.equals("TotalAssets") ) {
            return 30;
        }
        if( strFieldName.equals("NetProfitRate") ) {
            return 31;
        }
        if( strFieldName.equals("Satrap") ) {
            return 32;
        }
        if( strFieldName.equals("ActuCtrl") ) {
            return 33;
        }
        if( strFieldName.equals("License") ) {
            return 34;
        }
        if( strFieldName.equals("SocialInsuCode") ) {
            return 35;
        }
        if( strFieldName.equals("OrganizationCode") ) {
            return 36;
        }
        if( strFieldName.equals("TaxCode") ) {
            return 37;
        }
        if( strFieldName.equals("Fax") ) {
            return 38;
        }
        if( strFieldName.equals("EMail") ) {
            return 39;
        }
        if( strFieldName.equals("FoundDate") ) {
            return 40;
        }
        if( strFieldName.equals("Remark") ) {
            return 41;
        }
        if( strFieldName.equals("Segment1") ) {
            return 42;
        }
        if( strFieldName.equals("Segment2") ) {
            return 43;
        }
        if( strFieldName.equals("Segment3") ) {
            return 44;
        }
        if( strFieldName.equals("ManageCom") ) {
            return 45;
        }
        if( strFieldName.equals("ComCode") ) {
            return 46;
        }
        if( strFieldName.equals("ModifyOperator") ) {
            return 47;
        }
        if( strFieldName.equals("Sex") ) {
            return 48;
        }
        if( strFieldName.equals("NativePlace") ) {
            return 49;
        }
        if( strFieldName.equals("Occupation") ) {
            return 50;
        }
        if( strFieldName.equals("OccupationCode") ) {
            return 51;
        }
        if( strFieldName.equals("TaxPayerType") ) {
            return 52;
        }
        if( strFieldName.equals("FixedPlan") ) {
            return 53;
        }
        if( strFieldName.equals("AddTaxFlag") ) {
            return 54;
        }
        if( strFieldName.equals("AddTaxDate") ) {
            return 55;
        }
        if( strFieldName.equals("BankAccTaxNo") ) {
            return 56;
        }
        if( strFieldName.equals("BankTaxName") ) {
            return 57;
        }
        if( strFieldName.equals("BankTaxCode") ) {
            return 58;
        }
        if( strFieldName.equals("GrpAppIDType") ) {
            return 59;
        }
        if( strFieldName.equals("GrpAppIDExpDate") ) {
            return 60;
        }
        if( strFieldName.equals("CtrPeople") ) {
            return 61;
        }
        if( strFieldName.equals("Peoples3") ) {
            return 62;
        }
        if( strFieldName.equals("RelaPeoples") ) {
            return 63;
        }
        if( strFieldName.equals("RelaMatePeoples") ) {
            return 64;
        }
        if( strFieldName.equals("RelaYoungPeoples") ) {
            return 65;
        }
        if( strFieldName.equals("RelaOtherPeoples") ) {
            return 66;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "GrpContNo";
                break;
            case 1:
                strFieldName = "CustomerNo";
                break;
            case 2:
                strFieldName = "PrtNo";
                break;
            case 3:
                strFieldName = "AddressNo";
                break;
            case 4:
                strFieldName = "AppntGrade";
                break;
            case 5:
                strFieldName = "Name";
                break;
            case 6:
                strFieldName = "PostalAddress";
                break;
            case 7:
                strFieldName = "ZipCode";
                break;
            case 8:
                strFieldName = "Phone";
                break;
            case 9:
                strFieldName = "Password";
                break;
            case 10:
                strFieldName = "State";
                break;
            case 11:
                strFieldName = "AppntType";
                break;
            case 12:
                strFieldName = "RelationToInsured";
                break;
            case 13:
                strFieldName = "Operator";
                break;
            case 14:
                strFieldName = "MakeDate";
                break;
            case 15:
                strFieldName = "MakeTime";
                break;
            case 16:
                strFieldName = "ModifyDate";
                break;
            case 17:
                strFieldName = "ModifyTime";
                break;
            case 18:
                strFieldName = "BusiCategory";
                break;
            case 19:
                strFieldName = "GrpNature";
                break;
            case 20:
                strFieldName = "SumNumPeople";
                break;
            case 21:
                strFieldName = "MainBusiness";
                break;
            case 22:
                strFieldName = "Corporation";
                break;
            case 23:
                strFieldName = "CorIDType";
                break;
            case 24:
                strFieldName = "CorID";
                break;
            case 25:
                strFieldName = "CorIDExpiryDate";
                break;
            case 26:
                strFieldName = "OnJobNumber";
                break;
            case 27:
                strFieldName = "RetireNumber";
                break;
            case 28:
                strFieldName = "OtherNumber";
                break;
            case 29:
                strFieldName = "RgtCapital";
                break;
            case 30:
                strFieldName = "TotalAssets";
                break;
            case 31:
                strFieldName = "NetProfitRate";
                break;
            case 32:
                strFieldName = "Satrap";
                break;
            case 33:
                strFieldName = "ActuCtrl";
                break;
            case 34:
                strFieldName = "License";
                break;
            case 35:
                strFieldName = "SocialInsuCode";
                break;
            case 36:
                strFieldName = "OrganizationCode";
                break;
            case 37:
                strFieldName = "TaxCode";
                break;
            case 38:
                strFieldName = "Fax";
                break;
            case 39:
                strFieldName = "EMail";
                break;
            case 40:
                strFieldName = "FoundDate";
                break;
            case 41:
                strFieldName = "Remark";
                break;
            case 42:
                strFieldName = "Segment1";
                break;
            case 43:
                strFieldName = "Segment2";
                break;
            case 44:
                strFieldName = "Segment3";
                break;
            case 45:
                strFieldName = "ManageCom";
                break;
            case 46:
                strFieldName = "ComCode";
                break;
            case 47:
                strFieldName = "ModifyOperator";
                break;
            case 48:
                strFieldName = "Sex";
                break;
            case 49:
                strFieldName = "NativePlace";
                break;
            case 50:
                strFieldName = "Occupation";
                break;
            case 51:
                strFieldName = "OccupationCode";
                break;
            case 52:
                strFieldName = "TaxPayerType";
                break;
            case 53:
                strFieldName = "FixedPlan";
                break;
            case 54:
                strFieldName = "AddTaxFlag";
                break;
            case 55:
                strFieldName = "AddTaxDate";
                break;
            case 56:
                strFieldName = "BankAccTaxNo";
                break;
            case 57:
                strFieldName = "BankTaxName";
                break;
            case 58:
                strFieldName = "BankTaxCode";
                break;
            case 59:
                strFieldName = "GrpAppIDType";
                break;
            case 60:
                strFieldName = "GrpAppIDExpDate";
                break;
            case 61:
                strFieldName = "CtrPeople";
                break;
            case 62:
                strFieldName = "Peoples3";
                break;
            case 63:
                strFieldName = "RelaPeoples";
                break;
            case 64:
                strFieldName = "RelaMatePeoples";
                break;
            case 65:
                strFieldName = "RelaYoungPeoples";
                break;
            case 66:
                strFieldName = "RelaOtherPeoples";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "GRPCONTNO":
                return Schema.TYPE_STRING;
            case "CUSTOMERNO":
                return Schema.TYPE_STRING;
            case "PRTNO":
                return Schema.TYPE_STRING;
            case "ADDRESSNO":
                return Schema.TYPE_STRING;
            case "APPNTGRADE":
                return Schema.TYPE_STRING;
            case "NAME":
                return Schema.TYPE_STRING;
            case "POSTALADDRESS":
                return Schema.TYPE_STRING;
            case "ZIPCODE":
                return Schema.TYPE_STRING;
            case "PHONE":
                return Schema.TYPE_STRING;
            case "PASSWORD":
                return Schema.TYPE_STRING;
            case "STATE":
                return Schema.TYPE_STRING;
            case "APPNTTYPE":
                return Schema.TYPE_STRING;
            case "RELATIONTOINSURED":
                return Schema.TYPE_STRING;
            case "OPERATOR":
                return Schema.TYPE_STRING;
            case "MAKEDATE":
                return Schema.TYPE_DATE;
            case "MAKETIME":
                return Schema.TYPE_STRING;
            case "MODIFYDATE":
                return Schema.TYPE_DATE;
            case "MODIFYTIME":
                return Schema.TYPE_STRING;
            case "BUSICATEGORY":
                return Schema.TYPE_STRING;
            case "GRPNATURE":
                return Schema.TYPE_STRING;
            case "SUMNUMPEOPLE":
                return Schema.TYPE_INT;
            case "MAINBUSINESS":
                return Schema.TYPE_STRING;
            case "CORPORATION":
                return Schema.TYPE_STRING;
            case "CORIDTYPE":
                return Schema.TYPE_STRING;
            case "CORID":
                return Schema.TYPE_STRING;
            case "CORIDEXPIRYDATE":
                return Schema.TYPE_DATE;
            case "ONJOBNUMBER":
                return Schema.TYPE_INT;
            case "RETIRENUMBER":
                return Schema.TYPE_INT;
            case "OTHERNUMBER":
                return Schema.TYPE_INT;
            case "RGTCAPITAL":
                return Schema.TYPE_DOUBLE;
            case "TOTALASSETS":
                return Schema.TYPE_DOUBLE;
            case "NETPROFITRATE":
                return Schema.TYPE_DOUBLE;
            case "SATRAP":
                return Schema.TYPE_STRING;
            case "ACTUCTRL":
                return Schema.TYPE_STRING;
            case "LICENSE":
                return Schema.TYPE_STRING;
            case "SOCIALINSUCODE":
                return Schema.TYPE_STRING;
            case "ORGANIZATIONCODE":
                return Schema.TYPE_STRING;
            case "TAXCODE":
                return Schema.TYPE_STRING;
            case "FAX":
                return Schema.TYPE_STRING;
            case "EMAIL":
                return Schema.TYPE_STRING;
            case "FOUNDDATE":
                return Schema.TYPE_DATE;
            case "REMARK":
                return Schema.TYPE_STRING;
            case "SEGMENT1":
                return Schema.TYPE_STRING;
            case "SEGMENT2":
                return Schema.TYPE_STRING;
            case "SEGMENT3":
                return Schema.TYPE_STRING;
            case "MANAGECOM":
                return Schema.TYPE_STRING;
            case "COMCODE":
                return Schema.TYPE_STRING;
            case "MODIFYOPERATOR":
                return Schema.TYPE_STRING;
            case "SEX":
                return Schema.TYPE_STRING;
            case "NATIVEPLACE":
                return Schema.TYPE_STRING;
            case "OCCUPATION":
                return Schema.TYPE_STRING;
            case "OCCUPATIONCODE":
                return Schema.TYPE_STRING;
            case "TAXPAYERTYPE":
                return Schema.TYPE_STRING;
            case "FIXEDPLAN":
                return Schema.TYPE_STRING;
            case "ADDTAXFLAG":
                return Schema.TYPE_STRING;
            case "ADDTAXDATE":
                return Schema.TYPE_DATE;
            case "BANKACCTAXNO":
                return Schema.TYPE_STRING;
            case "BANKTAXNAME":
                return Schema.TYPE_STRING;
            case "BANKTAXCODE":
                return Schema.TYPE_STRING;
            case "GRPAPPIDTYPE":
                return Schema.TYPE_STRING;
            case "GRPAPPIDEXPDATE":
                return Schema.TYPE_DATE;
            case "CTRPEOPLE":
                return Schema.TYPE_STRING;
            case "PEOPLES3":
                return Schema.TYPE_INT;
            case "RELAPEOPLES":
                return Schema.TYPE_INT;
            case "RELAMATEPEOPLES":
                return Schema.TYPE_INT;
            case "RELAYOUNGPEOPLES":
                return Schema.TYPE_INT;
            case "RELAOTHERPEOPLES":
                return Schema.TYPE_INT;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_STRING;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_STRING;
            case 6:
                return Schema.TYPE_STRING;
            case 7:
                return Schema.TYPE_STRING;
            case 8:
                return Schema.TYPE_STRING;
            case 9:
                return Schema.TYPE_STRING;
            case 10:
                return Schema.TYPE_STRING;
            case 11:
                return Schema.TYPE_STRING;
            case 12:
                return Schema.TYPE_STRING;
            case 13:
                return Schema.TYPE_STRING;
            case 14:
                return Schema.TYPE_DATE;
            case 15:
                return Schema.TYPE_STRING;
            case 16:
                return Schema.TYPE_DATE;
            case 17:
                return Schema.TYPE_STRING;
            case 18:
                return Schema.TYPE_STRING;
            case 19:
                return Schema.TYPE_STRING;
            case 20:
                return Schema.TYPE_INT;
            case 21:
                return Schema.TYPE_STRING;
            case 22:
                return Schema.TYPE_STRING;
            case 23:
                return Schema.TYPE_STRING;
            case 24:
                return Schema.TYPE_STRING;
            case 25:
                return Schema.TYPE_DATE;
            case 26:
                return Schema.TYPE_INT;
            case 27:
                return Schema.TYPE_INT;
            case 28:
                return Schema.TYPE_INT;
            case 29:
                return Schema.TYPE_DOUBLE;
            case 30:
                return Schema.TYPE_DOUBLE;
            case 31:
                return Schema.TYPE_DOUBLE;
            case 32:
                return Schema.TYPE_STRING;
            case 33:
                return Schema.TYPE_STRING;
            case 34:
                return Schema.TYPE_STRING;
            case 35:
                return Schema.TYPE_STRING;
            case 36:
                return Schema.TYPE_STRING;
            case 37:
                return Schema.TYPE_STRING;
            case 38:
                return Schema.TYPE_STRING;
            case 39:
                return Schema.TYPE_STRING;
            case 40:
                return Schema.TYPE_DATE;
            case 41:
                return Schema.TYPE_STRING;
            case 42:
                return Schema.TYPE_STRING;
            case 43:
                return Schema.TYPE_STRING;
            case 44:
                return Schema.TYPE_STRING;
            case 45:
                return Schema.TYPE_STRING;
            case 46:
                return Schema.TYPE_STRING;
            case 47:
                return Schema.TYPE_STRING;
            case 48:
                return Schema.TYPE_STRING;
            case 49:
                return Schema.TYPE_STRING;
            case 50:
                return Schema.TYPE_STRING;
            case 51:
                return Schema.TYPE_STRING;
            case 52:
                return Schema.TYPE_STRING;
            case 53:
                return Schema.TYPE_STRING;
            case 54:
                return Schema.TYPE_STRING;
            case 55:
                return Schema.TYPE_DATE;
            case 56:
                return Schema.TYPE_STRING;
            case 57:
                return Schema.TYPE_STRING;
            case 58:
                return Schema.TYPE_STRING;
            case 59:
                return Schema.TYPE_STRING;
            case 60:
                return Schema.TYPE_DATE;
            case 61:
                return Schema.TYPE_STRING;
            case 62:
                return Schema.TYPE_INT;
            case 63:
                return Schema.TYPE_INT;
            case 64:
                return Schema.TYPE_INT;
            case 65:
                return Schema.TYPE_INT;
            case 66:
                return Schema.TYPE_INT;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
}
