/**
 * Copyright (c) 2017 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import com.sinosoft.lis.db.LVInsureAccTraceDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.*;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

/**
 * <p>ClassName: LVInsureAccTraceSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2017</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2017-10-21
 */
public class LVInsureAccTraceSchema implements Schema, Cloneable {
    // @Field
    /** Id */
    private long VInsureAccTraceID;
    /** Shardingid */
    private String ShardingID;
    /** 集体合同号码 */
    private String GrpContNo;
    /** 集体保单险种号码 */
    private String GrpPolNo;
    /** 保单号 */
    private String ContNo;
    /** 险种号 */
    private String PolNo;
    /** 流水号 */
    private String SerialNo;
    /** 保险帐户号码 */
    private String InsuAccNo;
    /** 险种编码 */
    private String RiskCode;
    /** 交费计划编码 */
    private String PayPlanCode;
    /** 对应其它号码 */
    private String OtherNo;
    /** 账户归属属性 */
    private String AccAscription;
    /** 金额类型 */
    private String MoneyType;
    /** 本次金额 */
    private double Money;
    /** 本次单位数 */
    private double UnitCount;
    /** 交费日期 */
    private Date PayDate;
    /** 状态 */
    private String State;
    /** 管理机构 */
    private String ManageCom;
    /** 对应其它号码类型 */
    private String OtherType;
    /** 操作员 */
    private String Operator;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 最后一次修改日期 */
    private Date ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime;
    /** 管理费编码 */
    private String FeeCode;
    /** 账户批单号码 */
    private String AccAlterNo;
    /** 账户批单号码类型 */
    private String AccAlterType;
    /** 业务号码 */
    private String PayNo;
    /** 业务批改类型 */
    private String BusyType;
    /** 应该计价日期 */
    private Date ShouldValueDate;
    /** 实际计价日期 */
    private Date ValueDate;
    /** 校正金额 */
    private double RectifyMoney;
    /** 本息金额类型 */
    private String BXMoneyType;

    public static final int FIELDNUM = 33;    // 数据库表的字段个数

    private static String[] PK;                // 主键

    private FDate fDate = new FDate();        // 处理日期

    public CErrors mErrors;            // 错误信息

    // @Constructor
    public LVInsureAccTraceSchema() {
        mErrors = new CErrors();

        String[] pk = new String[1];
        pk[0] = "VInsureAccTraceID";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException {
        LVInsureAccTraceSchema cloned = (LVInsureAccTraceSchema)super.clone();
        cloned.fDate = (FDate) fDate.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK() {
        return PK;
    }

    public long getVInsureAccTraceID() {
        return VInsureAccTraceID;
    }
    public void setVInsureAccTraceID(long aVInsureAccTraceID) {
        VInsureAccTraceID = aVInsureAccTraceID;
    }
    public void setVInsureAccTraceID(String aVInsureAccTraceID) {
        if (aVInsureAccTraceID != null && !aVInsureAccTraceID.equals("")) {
            VInsureAccTraceID = new Long(aVInsureAccTraceID).longValue();
        }
    }

    public String getShardingID() {
        return ShardingID;
    }
    public void setShardingID(String aShardingID) {
        ShardingID = aShardingID;
    }
    public String getGrpContNo() {
        return GrpContNo;
    }
    public void setGrpContNo(String aGrpContNo) {
        GrpContNo = aGrpContNo;
    }
    public String getGrpPolNo() {
        return GrpPolNo;
    }
    public void setGrpPolNo(String aGrpPolNo) {
        GrpPolNo = aGrpPolNo;
    }
    public String getContNo() {
        return ContNo;
    }
    public void setContNo(String aContNo) {
        ContNo = aContNo;
    }
    public String getPolNo() {
        return PolNo;
    }
    public void setPolNo(String aPolNo) {
        PolNo = aPolNo;
    }
    public String getSerialNo() {
        return SerialNo;
    }
    public void setSerialNo(String aSerialNo) {
        SerialNo = aSerialNo;
    }
    public String getInsuAccNo() {
        return InsuAccNo;
    }
    public void setInsuAccNo(String aInsuAccNo) {
        InsuAccNo = aInsuAccNo;
    }
    public String getRiskCode() {
        return RiskCode;
    }
    public void setRiskCode(String aRiskCode) {
        RiskCode = aRiskCode;
    }
    public String getPayPlanCode() {
        return PayPlanCode;
    }
    public void setPayPlanCode(String aPayPlanCode) {
        PayPlanCode = aPayPlanCode;
    }
    public String getOtherNo() {
        return OtherNo;
    }
    public void setOtherNo(String aOtherNo) {
        OtherNo = aOtherNo;
    }
    public String getAccAscription() {
        return AccAscription;
    }
    public void setAccAscription(String aAccAscription) {
        AccAscription = aAccAscription;
    }
    public String getMoneyType() {
        return MoneyType;
    }
    public void setMoneyType(String aMoneyType) {
        MoneyType = aMoneyType;
    }
    public double getMoney() {
        return Money;
    }
    public void setMoney(double aMoney) {
        Money = aMoney;
    }
    public void setMoney(String aMoney) {
        if (aMoney != null && !aMoney.equals("")) {
            Double tDouble = new Double(aMoney);
            double d = tDouble.doubleValue();
            Money = d;
        }
    }

    public double getUnitCount() {
        return UnitCount;
    }
    public void setUnitCount(double aUnitCount) {
        UnitCount = aUnitCount;
    }
    public void setUnitCount(String aUnitCount) {
        if (aUnitCount != null && !aUnitCount.equals("")) {
            Double tDouble = new Double(aUnitCount);
            double d = tDouble.doubleValue();
            UnitCount = d;
        }
    }

    public String getPayDate() {
        if(PayDate != null) {
            return fDate.getString(PayDate);
        } else {
            return null;
        }
    }
    public void setPayDate(Date aPayDate) {
        PayDate = aPayDate;
    }
    public void setPayDate(String aPayDate) {
        if (aPayDate != null && !aPayDate.equals("")) {
            PayDate = fDate.getDate(aPayDate);
        } else
            PayDate = null;
    }

    public String getState() {
        return State;
    }
    public void setState(String aState) {
        State = aState;
    }
    public String getManageCom() {
        return ManageCom;
    }
    public void setManageCom(String aManageCom) {
        ManageCom = aManageCom;
    }
    public String getOtherType() {
        return OtherType;
    }
    public void setOtherType(String aOtherType) {
        OtherType = aOtherType;
    }
    public String getOperator() {
        return Operator;
    }
    public void setOperator(String aOperator) {
        Operator = aOperator;
    }
    public String getMakeDate() {
        if(MakeDate != null) {
            return fDate.getString(MakeDate);
        } else {
            return null;
        }
    }
    public void setMakeDate(Date aMakeDate) {
        MakeDate = aMakeDate;
    }
    public void setMakeDate(String aMakeDate) {
        if (aMakeDate != null && !aMakeDate.equals("")) {
            MakeDate = fDate.getDate(aMakeDate);
        } else
            MakeDate = null;
    }

    public String getMakeTime() {
        return MakeTime;
    }
    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }
    public String getModifyDate() {
        if(ModifyDate != null) {
            return fDate.getString(ModifyDate);
        } else {
            return null;
        }
    }
    public void setModifyDate(Date aModifyDate) {
        ModifyDate = aModifyDate;
    }
    public void setModifyDate(String aModifyDate) {
        if (aModifyDate != null && !aModifyDate.equals("")) {
            ModifyDate = fDate.getDate(aModifyDate);
        } else
            ModifyDate = null;
    }

    public String getModifyTime() {
        return ModifyTime;
    }
    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }
    public String getFeeCode() {
        return FeeCode;
    }
    public void setFeeCode(String aFeeCode) {
        FeeCode = aFeeCode;
    }
    public String getAccAlterNo() {
        return AccAlterNo;
    }
    public void setAccAlterNo(String aAccAlterNo) {
        AccAlterNo = aAccAlterNo;
    }
    public String getAccAlterType() {
        return AccAlterType;
    }
    public void setAccAlterType(String aAccAlterType) {
        AccAlterType = aAccAlterType;
    }
    public String getPayNo() {
        return PayNo;
    }
    public void setPayNo(String aPayNo) {
        PayNo = aPayNo;
    }
    public String getBusyType() {
        return BusyType;
    }
    public void setBusyType(String aBusyType) {
        BusyType = aBusyType;
    }
    public String getShouldValueDate() {
        if(ShouldValueDate != null) {
            return fDate.getString(ShouldValueDate);
        } else {
            return null;
        }
    }
    public void setShouldValueDate(Date aShouldValueDate) {
        ShouldValueDate = aShouldValueDate;
    }
    public void setShouldValueDate(String aShouldValueDate) {
        if (aShouldValueDate != null && !aShouldValueDate.equals("")) {
            ShouldValueDate = fDate.getDate(aShouldValueDate);
        } else
            ShouldValueDate = null;
    }

    public String getValueDate() {
        if(ValueDate != null) {
            return fDate.getString(ValueDate);
        } else {
            return null;
        }
    }
    public void setValueDate(Date aValueDate) {
        ValueDate = aValueDate;
    }
    public void setValueDate(String aValueDate) {
        if (aValueDate != null && !aValueDate.equals("")) {
            ValueDate = fDate.getDate(aValueDate);
        } else
            ValueDate = null;
    }

    public double getRectifyMoney() {
        return RectifyMoney;
    }
    public void setRectifyMoney(double aRectifyMoney) {
        RectifyMoney = aRectifyMoney;
    }
    public void setRectifyMoney(String aRectifyMoney) {
        if (aRectifyMoney != null && !aRectifyMoney.equals("")) {
            Double tDouble = new Double(aRectifyMoney);
            double d = tDouble.doubleValue();
            RectifyMoney = d;
        }
    }

    public String getBXMoneyType() {
        return BXMoneyType;
    }
    public void setBXMoneyType(String aBXMoneyType) {
        BXMoneyType = aBXMoneyType;
    }

    /**
    * 使用另外一个 LVInsureAccTraceSchema 对象给 Schema 赋值
    * @param: aLVInsureAccTraceSchema LVInsureAccTraceSchema
    **/
    public void setSchema(LVInsureAccTraceSchema aLVInsureAccTraceSchema) {
        this.VInsureAccTraceID = aLVInsureAccTraceSchema.getVInsureAccTraceID();
        this.ShardingID = aLVInsureAccTraceSchema.getShardingID();
        this.GrpContNo = aLVInsureAccTraceSchema.getGrpContNo();
        this.GrpPolNo = aLVInsureAccTraceSchema.getGrpPolNo();
        this.ContNo = aLVInsureAccTraceSchema.getContNo();
        this.PolNo = aLVInsureAccTraceSchema.getPolNo();
        this.SerialNo = aLVInsureAccTraceSchema.getSerialNo();
        this.InsuAccNo = aLVInsureAccTraceSchema.getInsuAccNo();
        this.RiskCode = aLVInsureAccTraceSchema.getRiskCode();
        this.PayPlanCode = aLVInsureAccTraceSchema.getPayPlanCode();
        this.OtherNo = aLVInsureAccTraceSchema.getOtherNo();
        this.AccAscription = aLVInsureAccTraceSchema.getAccAscription();
        this.MoneyType = aLVInsureAccTraceSchema.getMoneyType();
        this.Money = aLVInsureAccTraceSchema.getMoney();
        this.UnitCount = aLVInsureAccTraceSchema.getUnitCount();
        this.PayDate = fDate.getDate( aLVInsureAccTraceSchema.getPayDate());
        this.State = aLVInsureAccTraceSchema.getState();
        this.ManageCom = aLVInsureAccTraceSchema.getManageCom();
        this.OtherType = aLVInsureAccTraceSchema.getOtherType();
        this.Operator = aLVInsureAccTraceSchema.getOperator();
        this.MakeDate = fDate.getDate( aLVInsureAccTraceSchema.getMakeDate());
        this.MakeTime = aLVInsureAccTraceSchema.getMakeTime();
        this.ModifyDate = fDate.getDate( aLVInsureAccTraceSchema.getModifyDate());
        this.ModifyTime = aLVInsureAccTraceSchema.getModifyTime();
        this.FeeCode = aLVInsureAccTraceSchema.getFeeCode();
        this.AccAlterNo = aLVInsureAccTraceSchema.getAccAlterNo();
        this.AccAlterType = aLVInsureAccTraceSchema.getAccAlterType();
        this.PayNo = aLVInsureAccTraceSchema.getPayNo();
        this.BusyType = aLVInsureAccTraceSchema.getBusyType();
        this.ShouldValueDate = fDate.getDate( aLVInsureAccTraceSchema.getShouldValueDate());
        this.ValueDate = fDate.getDate( aLVInsureAccTraceSchema.getValueDate());
        this.RectifyMoney = aLVInsureAccTraceSchema.getRectifyMoney();
        this.BXMoneyType = aLVInsureAccTraceSchema.getBXMoneyType();
    }

    /**
    * 使用 ResultSet 中的第 i 行给 Schema 赋值
    * @param: rs ResultSet
    * @param: i int
    * @return: boolean
    **/
    public boolean setSchema(ResultSet rs,int i) {
        try {
            //rs.absolute(i);        // 非滚动游标
            this.VInsureAccTraceID = rs.getLong("VInsureAccTraceID");
            if( rs.getString("ShardingID") == null )
                this.ShardingID = null;
            else
                this.ShardingID = rs.getString("ShardingID").trim();

            if( rs.getString("GrpContNo") == null )
                this.GrpContNo = null;
            else
                this.GrpContNo = rs.getString("GrpContNo").trim();

            if( rs.getString("GrpPolNo") == null )
                this.GrpPolNo = null;
            else
                this.GrpPolNo = rs.getString("GrpPolNo").trim();

            if( rs.getString("ContNo") == null )
                this.ContNo = null;
            else
                this.ContNo = rs.getString("ContNo").trim();

            if( rs.getString("PolNo") == null )
                this.PolNo = null;
            else
                this.PolNo = rs.getString("PolNo").trim();

            if( rs.getString("SerialNo") == null )
                this.SerialNo = null;
            else
                this.SerialNo = rs.getString("SerialNo").trim();

            if( rs.getString("InsuAccNo") == null )
                this.InsuAccNo = null;
            else
                this.InsuAccNo = rs.getString("InsuAccNo").trim();

            if( rs.getString("RiskCode") == null )
                this.RiskCode = null;
            else
                this.RiskCode = rs.getString("RiskCode").trim();

            if( rs.getString("PayPlanCode") == null )
                this.PayPlanCode = null;
            else
                this.PayPlanCode = rs.getString("PayPlanCode").trim();

            if( rs.getString("OtherNo") == null )
                this.OtherNo = null;
            else
                this.OtherNo = rs.getString("OtherNo").trim();

            if( rs.getString("AccAscription") == null )
                this.AccAscription = null;
            else
                this.AccAscription = rs.getString("AccAscription").trim();

            if( rs.getString("MoneyType") == null )
                this.MoneyType = null;
            else
                this.MoneyType = rs.getString("MoneyType").trim();

            this.Money = rs.getDouble("Money");
            this.UnitCount = rs.getDouble("UnitCount");
            this.PayDate = rs.getDate("PayDate");
            if( rs.getString("State") == null )
                this.State = null;
            else
                this.State = rs.getString("State").trim();

            if( rs.getString("ManageCom") == null )
                this.ManageCom = null;
            else
                this.ManageCom = rs.getString("ManageCom").trim();

            if( rs.getString("OtherType") == null )
                this.OtherType = null;
            else
                this.OtherType = rs.getString("OtherType").trim();

            if( rs.getString("Operator") == null )
                this.Operator = null;
            else
                this.Operator = rs.getString("Operator").trim();

            this.MakeDate = rs.getDate("MakeDate");
            if( rs.getString("MakeTime") == null )
                this.MakeTime = null;
            else
                this.MakeTime = rs.getString("MakeTime").trim();

            this.ModifyDate = rs.getDate("ModifyDate");
            if( rs.getString("ModifyTime") == null )
                this.ModifyTime = null;
            else
                this.ModifyTime = rs.getString("ModifyTime").trim();

            if( rs.getString("FeeCode") == null )
                this.FeeCode = null;
            else
                this.FeeCode = rs.getString("FeeCode").trim();

            if( rs.getString("AccAlterNo") == null )
                this.AccAlterNo = null;
            else
                this.AccAlterNo = rs.getString("AccAlterNo").trim();

            if( rs.getString("AccAlterType") == null )
                this.AccAlterType = null;
            else
                this.AccAlterType = rs.getString("AccAlterType").trim();

            if( rs.getString("PayNo") == null )
                this.PayNo = null;
            else
                this.PayNo = rs.getString("PayNo").trim();

            if( rs.getString("BusyType") == null )
                this.BusyType = null;
            else
                this.BusyType = rs.getString("BusyType").trim();

            this.ShouldValueDate = rs.getDate("ShouldValueDate");
            this.ValueDate = rs.getDate("ValueDate");
            this.RectifyMoney = rs.getDouble("RectifyMoney");
            if( rs.getString("BXMoneyType") == null )
                this.BXMoneyType = null;
            else
                this.BXMoneyType = rs.getString("BXMoneyType").trim();

        }
        catch(SQLException sqle) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LVInsureAccTraceSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = ExceptionUtils.exceptionToString(sqle);
            this.mErrors .addOneError(tError);
            return false;
        }
        return true;
    }

    public LVInsureAccTraceSchema getSchema() {
        LVInsureAccTraceSchema aLVInsureAccTraceSchema = new LVInsureAccTraceSchema();
        aLVInsureAccTraceSchema.setSchema(this);
        return aLVInsureAccTraceSchema;
    }

    public LVInsureAccTraceDB getDB() {
        LVInsureAccTraceDB aDBOper = new LVInsureAccTraceDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
    * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLVInsureAccTrace描述/A>表字段
    * @return: String 返回打包后字符串
    **/
    public String encode() {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(ChgData.chgData(VInsureAccTraceID));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ShardingID)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(GrpContNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(GrpPolNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ContNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(PolNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(SerialNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(InsuAccNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(RiskCode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(PayPlanCode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(OtherNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AccAscription)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MoneyType)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(Money));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(UnitCount));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( PayDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(State)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ManageCom)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(OtherType)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ModifyTime)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(FeeCode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AccAlterNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AccAlterType)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(PayNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(BusyType)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( ShouldValueDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( ValueDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(RectifyMoney));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(BXMoneyType));
        return strReturn.toString();
    }

    /**
    * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLVInsureAccTrace>历史记账凭证主表信息</A>表字段
    * @param: strMessage String 包含一条纪录数据的字符串
    * @return: boolean
    **/
    public boolean decode(String strMessage) {
        try {
            VInsureAccTraceID = new Long(ChgData.chgNumericStr(StrTool.getStr(strMessage,1, SysConst.PACKAGESPILTER))).longValue();
            ShardingID = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
            GrpContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
            GrpPolNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
            ContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
            PolNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
            SerialNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
            InsuAccNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
            RiskCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
            PayPlanCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
            OtherNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
            AccAscription = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
            MoneyType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
            Money = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,14, SysConst.PACKAGESPILTER))).doubleValue();
            UnitCount = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,15, SysConst.PACKAGESPILTER))).doubleValue();
            PayDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16, SysConst.PACKAGESPILTER));
            State = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17, SysConst.PACKAGESPILTER );
            ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18, SysConst.PACKAGESPILTER );
            OtherType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19, SysConst.PACKAGESPILTER );
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20, SysConst.PACKAGESPILTER );
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 21, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 22, SysConst.PACKAGESPILTER );
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 23, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 24, SysConst.PACKAGESPILTER );
            FeeCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 25, SysConst.PACKAGESPILTER );
            AccAlterNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 26, SysConst.PACKAGESPILTER );
            AccAlterType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 27, SysConst.PACKAGESPILTER );
            PayNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 28, SysConst.PACKAGESPILTER );
            BusyType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 29, SysConst.PACKAGESPILTER );
            ShouldValueDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 30, SysConst.PACKAGESPILTER));
            ValueDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 31, SysConst.PACKAGESPILTER));
            RectifyMoney = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,32, SysConst.PACKAGESPILTER))).doubleValue();
            BXMoneyType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 33, SysConst.PACKAGESPILTER );
        }
        catch(NumberFormatException ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LVInsureAccTraceSchema";
            tError.functionName = "decode";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            return false;
        }
        return true;
    }

    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("VInsureAccTraceID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(VInsureAccTraceID));
        }
        if (FCode.equalsIgnoreCase("ShardingID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ShardingID));
        }
        if (FCode.equalsIgnoreCase("GrpContNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GrpContNo));
        }
        if (FCode.equalsIgnoreCase("GrpPolNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GrpPolNo));
        }
        if (FCode.equalsIgnoreCase("ContNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ContNo));
        }
        if (FCode.equalsIgnoreCase("PolNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PolNo));
        }
        if (FCode.equalsIgnoreCase("SerialNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SerialNo));
        }
        if (FCode.equalsIgnoreCase("InsuAccNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InsuAccNo));
        }
        if (FCode.equalsIgnoreCase("RiskCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RiskCode));
        }
        if (FCode.equalsIgnoreCase("PayPlanCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PayPlanCode));
        }
        if (FCode.equalsIgnoreCase("OtherNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OtherNo));
        }
        if (FCode.equalsIgnoreCase("AccAscription")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AccAscription));
        }
        if (FCode.equalsIgnoreCase("MoneyType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MoneyType));
        }
        if (FCode.equalsIgnoreCase("Money")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Money));
        }
        if (FCode.equalsIgnoreCase("UnitCount")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(UnitCount));
        }
        if (FCode.equalsIgnoreCase("PayDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getPayDate()));
        }
        if (FCode.equalsIgnoreCase("State")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(State));
        }
        if (FCode.equalsIgnoreCase("ManageCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
        }
        if (FCode.equalsIgnoreCase("OtherType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OtherType));
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (FCode.equalsIgnoreCase("FeeCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FeeCode));
        }
        if (FCode.equalsIgnoreCase("AccAlterNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AccAlterNo));
        }
        if (FCode.equalsIgnoreCase("AccAlterType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AccAlterType));
        }
        if (FCode.equalsIgnoreCase("PayNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PayNo));
        }
        if (FCode.equalsIgnoreCase("BusyType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BusyType));
        }
        if (FCode.equalsIgnoreCase("ShouldValueDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getShouldValueDate()));
        }
        if (FCode.equalsIgnoreCase("ValueDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getValueDate()));
        }
        if (FCode.equalsIgnoreCase("RectifyMoney")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RectifyMoney));
        }
        if (FCode.equalsIgnoreCase("BXMoneyType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BXMoneyType));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = String.valueOf(VInsureAccTraceID);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(ShardingID);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(GrpContNo);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(GrpPolNo);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(ContNo);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(PolNo);
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(SerialNo);
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(InsuAccNo);
                break;
            case 8:
                strFieldValue = StrTool.GBKToUnicode(RiskCode);
                break;
            case 9:
                strFieldValue = StrTool.GBKToUnicode(PayPlanCode);
                break;
            case 10:
                strFieldValue = StrTool.GBKToUnicode(OtherNo);
                break;
            case 11:
                strFieldValue = StrTool.GBKToUnicode(AccAscription);
                break;
            case 12:
                strFieldValue = StrTool.GBKToUnicode(MoneyType);
                break;
            case 13:
                strFieldValue = String.valueOf(Money);
                break;
            case 14:
                strFieldValue = String.valueOf(UnitCount);
                break;
            case 15:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getPayDate()));
                break;
            case 16:
                strFieldValue = StrTool.GBKToUnicode(State);
                break;
            case 17:
                strFieldValue = StrTool.GBKToUnicode(ManageCom);
                break;
            case 18:
                strFieldValue = StrTool.GBKToUnicode(OtherType);
                break;
            case 19:
                strFieldValue = StrTool.GBKToUnicode(Operator);
                break;
            case 20:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
                break;
            case 21:
                strFieldValue = StrTool.GBKToUnicode(MakeTime);
                break;
            case 22:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
                break;
            case 23:
                strFieldValue = StrTool.GBKToUnicode(ModifyTime);
                break;
            case 24:
                strFieldValue = StrTool.GBKToUnicode(FeeCode);
                break;
            case 25:
                strFieldValue = StrTool.GBKToUnicode(AccAlterNo);
                break;
            case 26:
                strFieldValue = StrTool.GBKToUnicode(AccAlterType);
                break;
            case 27:
                strFieldValue = StrTool.GBKToUnicode(PayNo);
                break;
            case 28:
                strFieldValue = StrTool.GBKToUnicode(BusyType);
                break;
            case 29:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getShouldValueDate()));
                break;
            case 30:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getValueDate()));
                break;
            case 31:
                strFieldValue = String.valueOf(RectifyMoney);
                break;
            case 32:
                strFieldValue = StrTool.GBKToUnicode(BXMoneyType);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("VInsureAccTraceID")) {
            if( FValue != null && !FValue.equals("")) {
                VInsureAccTraceID = new Long(FValue).longValue();
            }
        }
        if (FCode.equalsIgnoreCase("ShardingID")) {
            if( FValue != null && !FValue.equals(""))
            {
                ShardingID = FValue.trim();
            }
            else
                ShardingID = null;
        }
        if (FCode.equalsIgnoreCase("GrpContNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                GrpContNo = FValue.trim();
            }
            else
                GrpContNo = null;
        }
        if (FCode.equalsIgnoreCase("GrpPolNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                GrpPolNo = FValue.trim();
            }
            else
                GrpPolNo = null;
        }
        if (FCode.equalsIgnoreCase("ContNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                ContNo = FValue.trim();
            }
            else
                ContNo = null;
        }
        if (FCode.equalsIgnoreCase("PolNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                PolNo = FValue.trim();
            }
            else
                PolNo = null;
        }
        if (FCode.equalsIgnoreCase("SerialNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                SerialNo = FValue.trim();
            }
            else
                SerialNo = null;
        }
        if (FCode.equalsIgnoreCase("InsuAccNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                InsuAccNo = FValue.trim();
            }
            else
                InsuAccNo = null;
        }
        if (FCode.equalsIgnoreCase("RiskCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                RiskCode = FValue.trim();
            }
            else
                RiskCode = null;
        }
        if (FCode.equalsIgnoreCase("PayPlanCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                PayPlanCode = FValue.trim();
            }
            else
                PayPlanCode = null;
        }
        if (FCode.equalsIgnoreCase("OtherNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                OtherNo = FValue.trim();
            }
            else
                OtherNo = null;
        }
        if (FCode.equalsIgnoreCase("AccAscription")) {
            if( FValue != null && !FValue.equals(""))
            {
                AccAscription = FValue.trim();
            }
            else
                AccAscription = null;
        }
        if (FCode.equalsIgnoreCase("MoneyType")) {
            if( FValue != null && !FValue.equals(""))
            {
                MoneyType = FValue.trim();
            }
            else
                MoneyType = null;
        }
        if (FCode.equalsIgnoreCase("Money")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                Money = d;
            }
        }
        if (FCode.equalsIgnoreCase("UnitCount")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                UnitCount = d;
            }
        }
        if (FCode.equalsIgnoreCase("PayDate")) {
            if(FValue != null && !FValue.equals("")) {
                PayDate = fDate.getDate( FValue );
            }
            else
                PayDate = null;
        }
        if (FCode.equalsIgnoreCase("State")) {
            if( FValue != null && !FValue.equals(""))
            {
                State = FValue.trim();
            }
            else
                State = null;
        }
        if (FCode.equalsIgnoreCase("ManageCom")) {
            if( FValue != null && !FValue.equals(""))
            {
                ManageCom = FValue.trim();
            }
            else
                ManageCom = null;
        }
        if (FCode.equalsIgnoreCase("OtherType")) {
            if( FValue != null && !FValue.equals(""))
            {
                OtherType = FValue.trim();
            }
            else
                OtherType = null;
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            if( FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
                Operator = null;
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if(FValue != null && !FValue.equals("")) {
                MakeDate = fDate.getDate( FValue );
            }
            else
                MakeDate = null;
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
                MakeTime = null;
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if(FValue != null && !FValue.equals("")) {
                ModifyDate = fDate.getDate( FValue );
            }
            else
                ModifyDate = null;
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
                ModifyTime = null;
        }
        if (FCode.equalsIgnoreCase("FeeCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                FeeCode = FValue.trim();
            }
            else
                FeeCode = null;
        }
        if (FCode.equalsIgnoreCase("AccAlterNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                AccAlterNo = FValue.trim();
            }
            else
                AccAlterNo = null;
        }
        if (FCode.equalsIgnoreCase("AccAlterType")) {
            if( FValue != null && !FValue.equals(""))
            {
                AccAlterType = FValue.trim();
            }
            else
                AccAlterType = null;
        }
        if (FCode.equalsIgnoreCase("PayNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                PayNo = FValue.trim();
            }
            else
                PayNo = null;
        }
        if (FCode.equalsIgnoreCase("BusyType")) {
            if( FValue != null && !FValue.equals(""))
            {
                BusyType = FValue.trim();
            }
            else
                BusyType = null;
        }
        if (FCode.equalsIgnoreCase("ShouldValueDate")) {
            if(FValue != null && !FValue.equals("")) {
                ShouldValueDate = fDate.getDate( FValue );
            }
            else
                ShouldValueDate = null;
        }
        if (FCode.equalsIgnoreCase("ValueDate")) {
            if(FValue != null && !FValue.equals("")) {
                ValueDate = fDate.getDate( FValue );
            }
            else
                ValueDate = null;
        }
        if (FCode.equalsIgnoreCase("RectifyMoney")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                RectifyMoney = d;
            }
        }
        if (FCode.equalsIgnoreCase("BXMoneyType")) {
            if( FValue != null && !FValue.equals(""))
            {
                BXMoneyType = FValue.trim();
            }
            else
                BXMoneyType = null;
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject) return true;
        if (otherObject == null) return false;
        if (getClass() != otherObject.getClass()) return false;
        LVInsureAccTraceSchema other = (LVInsureAccTraceSchema)otherObject;
        return
            VInsureAccTraceID == other.getVInsureAccTraceID()
            && ShardingID.equals(other.getShardingID())
            && GrpContNo.equals(other.getGrpContNo())
            && GrpPolNo.equals(other.getGrpPolNo())
            && ContNo.equals(other.getContNo())
            && PolNo.equals(other.getPolNo())
            && SerialNo.equals(other.getSerialNo())
            && InsuAccNo.equals(other.getInsuAccNo())
            && RiskCode.equals(other.getRiskCode())
            && PayPlanCode.equals(other.getPayPlanCode())
            && OtherNo.equals(other.getOtherNo())
            && AccAscription.equals(other.getAccAscription())
            && MoneyType.equals(other.getMoneyType())
            && Money == other.getMoney()
            && UnitCount == other.getUnitCount()
            && fDate.getString(PayDate).equals(other.getPayDate())
            && State.equals(other.getState())
            && ManageCom.equals(other.getManageCom())
            && OtherType.equals(other.getOtherType())
            && Operator.equals(other.getOperator())
            && fDate.getString(MakeDate).equals(other.getMakeDate())
            && MakeTime.equals(other.getMakeTime())
            && fDate.getString(ModifyDate).equals(other.getModifyDate())
            && ModifyTime.equals(other.getModifyTime())
            && FeeCode.equals(other.getFeeCode())
            && AccAlterNo.equals(other.getAccAlterNo())
            && AccAlterType.equals(other.getAccAlterType())
            && PayNo.equals(other.getPayNo())
            && BusyType.equals(other.getBusyType())
            && fDate.getString(ShouldValueDate).equals(other.getShouldValueDate())
            && fDate.getString(ValueDate).equals(other.getValueDate())
            && RectifyMoney == other.getRectifyMoney()
            && BXMoneyType.equals(other.getBXMoneyType());
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("VInsureAccTraceID") ) {
            return 0;
        }
        if( strFieldName.equals("ShardingID") ) {
            return 1;
        }
        if( strFieldName.equals("GrpContNo") ) {
            return 2;
        }
        if( strFieldName.equals("GrpPolNo") ) {
            return 3;
        }
        if( strFieldName.equals("ContNo") ) {
            return 4;
        }
        if( strFieldName.equals("PolNo") ) {
            return 5;
        }
        if( strFieldName.equals("SerialNo") ) {
            return 6;
        }
        if( strFieldName.equals("InsuAccNo") ) {
            return 7;
        }
        if( strFieldName.equals("RiskCode") ) {
            return 8;
        }
        if( strFieldName.equals("PayPlanCode") ) {
            return 9;
        }
        if( strFieldName.equals("OtherNo") ) {
            return 10;
        }
        if( strFieldName.equals("AccAscription") ) {
            return 11;
        }
        if( strFieldName.equals("MoneyType") ) {
            return 12;
        }
        if( strFieldName.equals("Money") ) {
            return 13;
        }
        if( strFieldName.equals("UnitCount") ) {
            return 14;
        }
        if( strFieldName.equals("PayDate") ) {
            return 15;
        }
        if( strFieldName.equals("State") ) {
            return 16;
        }
        if( strFieldName.equals("ManageCom") ) {
            return 17;
        }
        if( strFieldName.equals("OtherType") ) {
            return 18;
        }
        if( strFieldName.equals("Operator") ) {
            return 19;
        }
        if( strFieldName.equals("MakeDate") ) {
            return 20;
        }
        if( strFieldName.equals("MakeTime") ) {
            return 21;
        }
        if( strFieldName.equals("ModifyDate") ) {
            return 22;
        }
        if( strFieldName.equals("ModifyTime") ) {
            return 23;
        }
        if( strFieldName.equals("FeeCode") ) {
            return 24;
        }
        if( strFieldName.equals("AccAlterNo") ) {
            return 25;
        }
        if( strFieldName.equals("AccAlterType") ) {
            return 26;
        }
        if( strFieldName.equals("PayNo") ) {
            return 27;
        }
        if( strFieldName.equals("BusyType") ) {
            return 28;
        }
        if( strFieldName.equals("ShouldValueDate") ) {
            return 29;
        }
        if( strFieldName.equals("ValueDate") ) {
            return 30;
        }
        if( strFieldName.equals("RectifyMoney") ) {
            return 31;
        }
        if( strFieldName.equals("BXMoneyType") ) {
            return 32;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "VInsureAccTraceID";
                break;
            case 1:
                strFieldName = "ShardingID";
                break;
            case 2:
                strFieldName = "GrpContNo";
                break;
            case 3:
                strFieldName = "GrpPolNo";
                break;
            case 4:
                strFieldName = "ContNo";
                break;
            case 5:
                strFieldName = "PolNo";
                break;
            case 6:
                strFieldName = "SerialNo";
                break;
            case 7:
                strFieldName = "InsuAccNo";
                break;
            case 8:
                strFieldName = "RiskCode";
                break;
            case 9:
                strFieldName = "PayPlanCode";
                break;
            case 10:
                strFieldName = "OtherNo";
                break;
            case 11:
                strFieldName = "AccAscription";
                break;
            case 12:
                strFieldName = "MoneyType";
                break;
            case 13:
                strFieldName = "Money";
                break;
            case 14:
                strFieldName = "UnitCount";
                break;
            case 15:
                strFieldName = "PayDate";
                break;
            case 16:
                strFieldName = "State";
                break;
            case 17:
                strFieldName = "ManageCom";
                break;
            case 18:
                strFieldName = "OtherType";
                break;
            case 19:
                strFieldName = "Operator";
                break;
            case 20:
                strFieldName = "MakeDate";
                break;
            case 21:
                strFieldName = "MakeTime";
                break;
            case 22:
                strFieldName = "ModifyDate";
                break;
            case 23:
                strFieldName = "ModifyTime";
                break;
            case 24:
                strFieldName = "FeeCode";
                break;
            case 25:
                strFieldName = "AccAlterNo";
                break;
            case 26:
                strFieldName = "AccAlterType";
                break;
            case 27:
                strFieldName = "PayNo";
                break;
            case 28:
                strFieldName = "BusyType";
                break;
            case 29:
                strFieldName = "ShouldValueDate";
                break;
            case 30:
                strFieldName = "ValueDate";
                break;
            case 31:
                strFieldName = "RectifyMoney";
                break;
            case 32:
                strFieldName = "BXMoneyType";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "VINSUREACCTRACEID":
                return Schema.TYPE_LONG;
            case "SHARDINGID":
                return Schema.TYPE_STRING;
            case "GRPCONTNO":
                return Schema.TYPE_STRING;
            case "GRPPOLNO":
                return Schema.TYPE_STRING;
            case "CONTNO":
                return Schema.TYPE_STRING;
            case "POLNO":
                return Schema.TYPE_STRING;
            case "SERIALNO":
                return Schema.TYPE_STRING;
            case "INSUACCNO":
                return Schema.TYPE_STRING;
            case "RISKCODE":
                return Schema.TYPE_STRING;
            case "PAYPLANCODE":
                return Schema.TYPE_STRING;
            case "OTHERNO":
                return Schema.TYPE_STRING;
            case "ACCASCRIPTION":
                return Schema.TYPE_STRING;
            case "MONEYTYPE":
                return Schema.TYPE_STRING;
            case "MONEY":
                return Schema.TYPE_DOUBLE;
            case "UNITCOUNT":
                return Schema.TYPE_DOUBLE;
            case "PAYDATE":
                return Schema.TYPE_DATE;
            case "STATE":
                return Schema.TYPE_STRING;
            case "MANAGECOM":
                return Schema.TYPE_STRING;
            case "OTHERTYPE":
                return Schema.TYPE_STRING;
            case "OPERATOR":
                return Schema.TYPE_STRING;
            case "MAKEDATE":
                return Schema.TYPE_DATE;
            case "MAKETIME":
                return Schema.TYPE_STRING;
            case "MODIFYDATE":
                return Schema.TYPE_DATE;
            case "MODIFYTIME":
                return Schema.TYPE_STRING;
            case "FEECODE":
                return Schema.TYPE_STRING;
            case "ACCALTERNO":
                return Schema.TYPE_STRING;
            case "ACCALTERTYPE":
                return Schema.TYPE_STRING;
            case "PAYNO":
                return Schema.TYPE_STRING;
            case "BUSYTYPE":
                return Schema.TYPE_STRING;
            case "SHOULDVALUEDATE":
                return Schema.TYPE_DATE;
            case "VALUEDATE":
                return Schema.TYPE_DATE;
            case "RECTIFYMONEY":
                return Schema.TYPE_DOUBLE;
            case "BXMONEYTYPE":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_LONG;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_STRING;
            case 6:
                return Schema.TYPE_STRING;
            case 7:
                return Schema.TYPE_STRING;
            case 8:
                return Schema.TYPE_STRING;
            case 9:
                return Schema.TYPE_STRING;
            case 10:
                return Schema.TYPE_STRING;
            case 11:
                return Schema.TYPE_STRING;
            case 12:
                return Schema.TYPE_STRING;
            case 13:
                return Schema.TYPE_DOUBLE;
            case 14:
                return Schema.TYPE_DOUBLE;
            case 15:
                return Schema.TYPE_DATE;
            case 16:
                return Schema.TYPE_STRING;
            case 17:
                return Schema.TYPE_STRING;
            case 18:
                return Schema.TYPE_STRING;
            case 19:
                return Schema.TYPE_STRING;
            case 20:
                return Schema.TYPE_DATE;
            case 21:
                return Schema.TYPE_STRING;
            case 22:
                return Schema.TYPE_DATE;
            case 23:
                return Schema.TYPE_STRING;
            case 24:
                return Schema.TYPE_STRING;
            case 25:
                return Schema.TYPE_STRING;
            case 26:
                return Schema.TYPE_STRING;
            case 27:
                return Schema.TYPE_STRING;
            case 28:
                return Schema.TYPE_STRING;
            case 29:
                return Schema.TYPE_DATE;
            case 30:
                return Schema.TYPE_DATE;
            case 31:
                return Schema.TYPE_DOUBLE;
            case 32:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
}
