/**
 * Copyright (c) 2017 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.entity;

import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.Pojo;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;

import java.io.Serializable;

/**
 * <p>ClassName: LCInsureAccTracePojo </p>
 * <p>Copyright: Copyright (c) 2017</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2017-10-17
 */
public class LCInsureAccTracePojo implements Pojo,Serializable {
    // @Field
    /** Id */
    private long InsureAccTraceID; 
    /** Shardingid */
    private String ShardingID; 
    /** 集体保单险种号码 */
    private String GrpPolNo; 
    /** 集体合同号码 */
    private String GrpContNo; 
    /** 合同号码 */
    private String ContNo; 
    /** 保单险种号码 */
    private String PolNo; 
    /** 流水号 */
    private String SerialNo; 
    /** 保险帐户号码 */
    private String InsuAccNo; 
    /** 险种编码 */
    private String RiskCode; 
    /** 交费计划编码 */
    private String PayPlanCode; 
    /** 对应其它号码 */
    private String OtherNo; 
    /** 对应其它号码类型 */
    private String OtherType; 
    /** 账户归属属性 */
    private String AccAscription; 
    /** 金额类型 */
    private String MoneyType; 
    /** 本次金额 */
    private double Money; 
    /** 本次单位数 */
    private double UnitCount; 
    /** 交费日期 */
    private String  PayDate;
    /** 状态 */
    private String State; 
    /** 管理机构 */
    private String ManageCom; 
    /** 操作员 */
    private String Operator; 
    /** 入机日期 */
    private String  MakeDate;
    /** 入机时间 */
    private String MakeTime; 
    /** 最后一次修改日期 */
    private String  ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime; 
    /** 管理费编码 */
    private String FeeCode; 
    /** 账户批单号码 */
    private String AccAlterNo; 
    /** 账户批单号码类型 */
    private String AccAlterType; 
    /** 业务号码 */
    private String PayNo; 
    /** 业务批改类型 */
    private String BusyType; 
    /** 应该计价日期 */
    private String  ShouldValueDate;
    /** 实际计价日期 */
    private String  ValueDate;
    /** 校正金额 */
    private double RectifyMoney; 


    public static final int FIELDNUM = 32;    // 数据库表的字段个数

    private FDate fDate = new FDate();        // 处理日期
    public long getInsureAccTraceID() {
        return InsureAccTraceID;
    }
    public void setInsureAccTraceID(long aInsureAccTraceID) {
        InsureAccTraceID = aInsureAccTraceID;
    }
    public void setInsureAccTraceID(String aInsureAccTraceID) {
        if (aInsureAccTraceID != null && !aInsureAccTraceID.equals("")) {
            InsureAccTraceID = new Long(aInsureAccTraceID).longValue();
        }
    }

    public String getShardingID() {
        return ShardingID;
    }
    public void setShardingID(String aShardingID) {
        ShardingID = aShardingID;
    }
    public String getGrpPolNo() {
        return GrpPolNo;
    }
    public void setGrpPolNo(String aGrpPolNo) {
        GrpPolNo = aGrpPolNo;
    }
    public String getGrpContNo() {
        return GrpContNo;
    }
    public void setGrpContNo(String aGrpContNo) {
        GrpContNo = aGrpContNo;
    }
    public String getContNo() {
        return ContNo;
    }
    public void setContNo(String aContNo) {
        ContNo = aContNo;
    }
    public String getPolNo() {
        return PolNo;
    }
    public void setPolNo(String aPolNo) {
        PolNo = aPolNo;
    }
    public String getSerialNo() {
        return SerialNo;
    }
    public void setSerialNo(String aSerialNo) {
        SerialNo = aSerialNo;
    }
    public String getInsuAccNo() {
        return InsuAccNo;
    }
    public void setInsuAccNo(String aInsuAccNo) {
        InsuAccNo = aInsuAccNo;
    }
    public String getRiskCode() {
        return RiskCode;
    }
    public void setRiskCode(String aRiskCode) {
        RiskCode = aRiskCode;
    }
    public String getPayPlanCode() {
        return PayPlanCode;
    }
    public void setPayPlanCode(String aPayPlanCode) {
        PayPlanCode = aPayPlanCode;
    }
    public String getOtherNo() {
        return OtherNo;
    }
    public void setOtherNo(String aOtherNo) {
        OtherNo = aOtherNo;
    }
    public String getOtherType() {
        return OtherType;
    }
    public void setOtherType(String aOtherType) {
        OtherType = aOtherType;
    }
    public String getAccAscription() {
        return AccAscription;
    }
    public void setAccAscription(String aAccAscription) {
        AccAscription = aAccAscription;
    }
    public String getMoneyType() {
        return MoneyType;
    }
    public void setMoneyType(String aMoneyType) {
        MoneyType = aMoneyType;
    }
    public double getMoney() {
        return Money;
    }
    public void setMoney(double aMoney) {
        Money = aMoney;
    }
    public void setMoney(String aMoney) {
        if (aMoney != null && !aMoney.equals("")) {
            Double tDouble = new Double(aMoney);
            double d = tDouble.doubleValue();
            Money = d;
        }
    }

    public double getUnitCount() {
        return UnitCount;
    }
    public void setUnitCount(double aUnitCount) {
        UnitCount = aUnitCount;
    }
    public void setUnitCount(String aUnitCount) {
        if (aUnitCount != null && !aUnitCount.equals("")) {
            Double tDouble = new Double(aUnitCount);
            double d = tDouble.doubleValue();
            UnitCount = d;
        }
    }

    public String getPayDate() {
        return PayDate;
    }
    public void setPayDate(String aPayDate) {
        PayDate = aPayDate;
    }
    public String getState() {
        return State;
    }
    public void setState(String aState) {
        State = aState;
    }
    public String getManageCom() {
        return ManageCom;
    }
    public void setManageCom(String aManageCom) {
        ManageCom = aManageCom;
    }
    public String getOperator() {
        return Operator;
    }
    public void setOperator(String aOperator) {
        Operator = aOperator;
    }
    public String getMakeDate() {
        return MakeDate;
    }
    public void setMakeDate(String aMakeDate) {
        MakeDate = aMakeDate;
    }
    public String getMakeTime() {
        return MakeTime;
    }
    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }
    public String getModifyDate() {
        return ModifyDate;
    }
    public void setModifyDate(String aModifyDate) {
        ModifyDate = aModifyDate;
    }
    public String getModifyTime() {
        return ModifyTime;
    }
    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }
    public String getFeeCode() {
        return FeeCode;
    }
    public void setFeeCode(String aFeeCode) {
        FeeCode = aFeeCode;
    }
    public String getAccAlterNo() {
        return AccAlterNo;
    }
    public void setAccAlterNo(String aAccAlterNo) {
        AccAlterNo = aAccAlterNo;
    }
    public String getAccAlterType() {
        return AccAlterType;
    }
    public void setAccAlterType(String aAccAlterType) {
        AccAlterType = aAccAlterType;
    }
    public String getPayNo() {
        return PayNo;
    }
    public void setPayNo(String aPayNo) {
        PayNo = aPayNo;
    }
    public String getBusyType() {
        return BusyType;
    }
    public void setBusyType(String aBusyType) {
        BusyType = aBusyType;
    }
    public String getShouldValueDate() {
        return ShouldValueDate;
    }
    public void setShouldValueDate(String aShouldValueDate) {
        ShouldValueDate = aShouldValueDate;
    }
    public String getValueDate() {
        return ValueDate;
    }
    public void setValueDate(String aValueDate) {
        ValueDate = aValueDate;
    }
    public double getRectifyMoney() {
        return RectifyMoney;
    }
    public void setRectifyMoney(double aRectifyMoney) {
        RectifyMoney = aRectifyMoney;
    }
    public void setRectifyMoney(String aRectifyMoney) {
        if (aRectifyMoney != null && !aRectifyMoney.equals("")) {
            Double tDouble = new Double(aRectifyMoney);
            double d = tDouble.doubleValue();
            RectifyMoney = d;
        }
    }


    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("InsureAccTraceID") ) {
            return 0;
        }
        if( strFieldName.equals("ShardingID") ) {
            return 1;
        }
        if( strFieldName.equals("GrpPolNo") ) {
            return 2;
        }
        if( strFieldName.equals("GrpContNo") ) {
            return 3;
        }
        if( strFieldName.equals("ContNo") ) {
            return 4;
        }
        if( strFieldName.equals("PolNo") ) {
            return 5;
        }
        if( strFieldName.equals("SerialNo") ) {
            return 6;
        }
        if( strFieldName.equals("InsuAccNo") ) {
            return 7;
        }
        if( strFieldName.equals("RiskCode") ) {
            return 8;
        }
        if( strFieldName.equals("PayPlanCode") ) {
            return 9;
        }
        if( strFieldName.equals("OtherNo") ) {
            return 10;
        }
        if( strFieldName.equals("OtherType") ) {
            return 11;
        }
        if( strFieldName.equals("AccAscription") ) {
            return 12;
        }
        if( strFieldName.equals("MoneyType") ) {
            return 13;
        }
        if( strFieldName.equals("Money") ) {
            return 14;
        }
        if( strFieldName.equals("UnitCount") ) {
            return 15;
        }
        if( strFieldName.equals("PayDate") ) {
            return 16;
        }
        if( strFieldName.equals("State") ) {
            return 17;
        }
        if( strFieldName.equals("ManageCom") ) {
            return 18;
        }
        if( strFieldName.equals("Operator") ) {
            return 19;
        }
        if( strFieldName.equals("MakeDate") ) {
            return 20;
        }
        if( strFieldName.equals("MakeTime") ) {
            return 21;
        }
        if( strFieldName.equals("ModifyDate") ) {
            return 22;
        }
        if( strFieldName.equals("ModifyTime") ) {
            return 23;
        }
        if( strFieldName.equals("FeeCode") ) {
            return 24;
        }
        if( strFieldName.equals("AccAlterNo") ) {
            return 25;
        }
        if( strFieldName.equals("AccAlterType") ) {
            return 26;
        }
        if( strFieldName.equals("PayNo") ) {
            return 27;
        }
        if( strFieldName.equals("BusyType") ) {
            return 28;
        }
        if( strFieldName.equals("ShouldValueDate") ) {
            return 29;
        }
        if( strFieldName.equals("ValueDate") ) {
            return 30;
        }
        if( strFieldName.equals("RectifyMoney") ) {
            return 31;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "InsureAccTraceID";
                break;
            case 1:
                strFieldName = "ShardingID";
                break;
            case 2:
                strFieldName = "GrpPolNo";
                break;
            case 3:
                strFieldName = "GrpContNo";
                break;
            case 4:
                strFieldName = "ContNo";
                break;
            case 5:
                strFieldName = "PolNo";
                break;
            case 6:
                strFieldName = "SerialNo";
                break;
            case 7:
                strFieldName = "InsuAccNo";
                break;
            case 8:
                strFieldName = "RiskCode";
                break;
            case 9:
                strFieldName = "PayPlanCode";
                break;
            case 10:
                strFieldName = "OtherNo";
                break;
            case 11:
                strFieldName = "OtherType";
                break;
            case 12:
                strFieldName = "AccAscription";
                break;
            case 13:
                strFieldName = "MoneyType";
                break;
            case 14:
                strFieldName = "Money";
                break;
            case 15:
                strFieldName = "UnitCount";
                break;
            case 16:
                strFieldName = "PayDate";
                break;
            case 17:
                strFieldName = "State";
                break;
            case 18:
                strFieldName = "ManageCom";
                break;
            case 19:
                strFieldName = "Operator";
                break;
            case 20:
                strFieldName = "MakeDate";
                break;
            case 21:
                strFieldName = "MakeTime";
                break;
            case 22:
                strFieldName = "ModifyDate";
                break;
            case 23:
                strFieldName = "ModifyTime";
                break;
            case 24:
                strFieldName = "FeeCode";
                break;
            case 25:
                strFieldName = "AccAlterNo";
                break;
            case 26:
                strFieldName = "AccAlterType";
                break;
            case 27:
                strFieldName = "PayNo";
                break;
            case 28:
                strFieldName = "BusyType";
                break;
            case 29:
                strFieldName = "ShouldValueDate";
                break;
            case 30:
                strFieldName = "ValueDate";
                break;
            case 31:
                strFieldName = "RectifyMoney";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "INSUREACCTRACEID":
                return Schema.TYPE_LONG;
            case "SHARDINGID":
                return Schema.TYPE_STRING;
            case "GRPPOLNO":
                return Schema.TYPE_STRING;
            case "GRPCONTNO":
                return Schema.TYPE_STRING;
            case "CONTNO":
                return Schema.TYPE_STRING;
            case "POLNO":
                return Schema.TYPE_STRING;
            case "SERIALNO":
                return Schema.TYPE_STRING;
            case "INSUACCNO":
                return Schema.TYPE_STRING;
            case "RISKCODE":
                return Schema.TYPE_STRING;
            case "PAYPLANCODE":
                return Schema.TYPE_STRING;
            case "OTHERNO":
                return Schema.TYPE_STRING;
            case "OTHERTYPE":
                return Schema.TYPE_STRING;
            case "ACCASCRIPTION":
                return Schema.TYPE_STRING;
            case "MONEYTYPE":
                return Schema.TYPE_STRING;
            case "MONEY":
                return Schema.TYPE_DOUBLE;
            case "UNITCOUNT":
                return Schema.TYPE_DOUBLE;
            case "PAYDATE":
                return Schema.TYPE_STRING;
            case "STATE":
                return Schema.TYPE_STRING;
            case "MANAGECOM":
                return Schema.TYPE_STRING;
            case "OPERATOR":
                return Schema.TYPE_STRING;
            case "MAKEDATE":
                return Schema.TYPE_STRING;
            case "MAKETIME":
                return Schema.TYPE_STRING;
            case "MODIFYDATE":
                return Schema.TYPE_STRING;
            case "MODIFYTIME":
                return Schema.TYPE_STRING;
            case "FEECODE":
                return Schema.TYPE_STRING;
            case "ACCALTERNO":
                return Schema.TYPE_STRING;
            case "ACCALTERTYPE":
                return Schema.TYPE_STRING;
            case "PAYNO":
                return Schema.TYPE_STRING;
            case "BUSYTYPE":
                return Schema.TYPE_STRING;
            case "SHOULDVALUEDATE":
                return Schema.TYPE_STRING;
            case "VALUEDATE":
                return Schema.TYPE_STRING;
            case "RECTIFYMONEY":
                return Schema.TYPE_DOUBLE;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_LONG;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_STRING;
            case 6:
                return Schema.TYPE_STRING;
            case 7:
                return Schema.TYPE_STRING;
            case 8:
                return Schema.TYPE_STRING;
            case 9:
                return Schema.TYPE_STRING;
            case 10:
                return Schema.TYPE_STRING;
            case 11:
                return Schema.TYPE_STRING;
            case 12:
                return Schema.TYPE_STRING;
            case 13:
                return Schema.TYPE_STRING;
            case 14:
                return Schema.TYPE_DOUBLE;
            case 15:
                return Schema.TYPE_DOUBLE;
            case 16:
                return Schema.TYPE_STRING;
            case 17:
                return Schema.TYPE_STRING;
            case 18:
                return Schema.TYPE_STRING;
            case 19:
                return Schema.TYPE_STRING;
            case 20:
                return Schema.TYPE_STRING;
            case 21:
                return Schema.TYPE_STRING;
            case 22:
                return Schema.TYPE_STRING;
            case 23:
                return Schema.TYPE_STRING;
            case 24:
                return Schema.TYPE_STRING;
            case 25:
                return Schema.TYPE_STRING;
            case 26:
                return Schema.TYPE_STRING;
            case 27:
                return Schema.TYPE_STRING;
            case 28:
                return Schema.TYPE_STRING;
            case 29:
                return Schema.TYPE_STRING;
            case 30:
                return Schema.TYPE_STRING;
            case 31:
                return Schema.TYPE_DOUBLE;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("InsureAccTraceID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InsureAccTraceID));
        }
        if (FCode.equalsIgnoreCase("ShardingID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ShardingID));
        }
        if (FCode.equalsIgnoreCase("GrpPolNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GrpPolNo));
        }
        if (FCode.equalsIgnoreCase("GrpContNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GrpContNo));
        }
        if (FCode.equalsIgnoreCase("ContNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ContNo));
        }
        if (FCode.equalsIgnoreCase("PolNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PolNo));
        }
        if (FCode.equalsIgnoreCase("SerialNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SerialNo));
        }
        if (FCode.equalsIgnoreCase("InsuAccNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InsuAccNo));
        }
        if (FCode.equalsIgnoreCase("RiskCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RiskCode));
        }
        if (FCode.equalsIgnoreCase("PayPlanCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PayPlanCode));
        }
        if (FCode.equalsIgnoreCase("OtherNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OtherNo));
        }
        if (FCode.equalsIgnoreCase("OtherType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OtherType));
        }
        if (FCode.equalsIgnoreCase("AccAscription")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AccAscription));
        }
        if (FCode.equalsIgnoreCase("MoneyType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MoneyType));
        }
        if (FCode.equalsIgnoreCase("Money")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Money));
        }
        if (FCode.equalsIgnoreCase("UnitCount")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(UnitCount));
        }
        if (FCode.equalsIgnoreCase("PayDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PayDate));
        }
        if (FCode.equalsIgnoreCase("State")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(State));
        }
        if (FCode.equalsIgnoreCase("ManageCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeDate));
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyDate));
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (FCode.equalsIgnoreCase("FeeCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FeeCode));
        }
        if (FCode.equalsIgnoreCase("AccAlterNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AccAlterNo));
        }
        if (FCode.equalsIgnoreCase("AccAlterType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AccAlterType));
        }
        if (FCode.equalsIgnoreCase("PayNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PayNo));
        }
        if (FCode.equalsIgnoreCase("BusyType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BusyType));
        }
        if (FCode.equalsIgnoreCase("ShouldValueDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ShouldValueDate));
        }
        if (FCode.equalsIgnoreCase("ValueDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ValueDate));
        }
        if (FCode.equalsIgnoreCase("RectifyMoney")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RectifyMoney));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = String.valueOf(InsureAccTraceID);
                break;
            case 1:
                strFieldValue = String.valueOf(ShardingID);
                break;
            case 2:
                strFieldValue = String.valueOf(GrpPolNo);
                break;
            case 3:
                strFieldValue = String.valueOf(GrpContNo);
                break;
            case 4:
                strFieldValue = String.valueOf(ContNo);
                break;
            case 5:
                strFieldValue = String.valueOf(PolNo);
                break;
            case 6:
                strFieldValue = String.valueOf(SerialNo);
                break;
            case 7:
                strFieldValue = String.valueOf(InsuAccNo);
                break;
            case 8:
                strFieldValue = String.valueOf(RiskCode);
                break;
            case 9:
                strFieldValue = String.valueOf(PayPlanCode);
                break;
            case 10:
                strFieldValue = String.valueOf(OtherNo);
                break;
            case 11:
                strFieldValue = String.valueOf(OtherType);
                break;
            case 12:
                strFieldValue = String.valueOf(AccAscription);
                break;
            case 13:
                strFieldValue = String.valueOf(MoneyType);
                break;
            case 14:
                strFieldValue = String.valueOf(Money);
                break;
            case 15:
                strFieldValue = String.valueOf(UnitCount);
                break;
            case 16:
                strFieldValue = String.valueOf(PayDate);
                break;
            case 17:
                strFieldValue = String.valueOf(State);
                break;
            case 18:
                strFieldValue = String.valueOf(ManageCom);
                break;
            case 19:
                strFieldValue = String.valueOf(Operator);
                break;
            case 20:
                strFieldValue = String.valueOf(MakeDate);
                break;
            case 21:
                strFieldValue = String.valueOf(MakeTime);
                break;
            case 22:
                strFieldValue = String.valueOf(ModifyDate);
                break;
            case 23:
                strFieldValue = String.valueOf(ModifyTime);
                break;
            case 24:
                strFieldValue = String.valueOf(FeeCode);
                break;
            case 25:
                strFieldValue = String.valueOf(AccAlterNo);
                break;
            case 26:
                strFieldValue = String.valueOf(AccAlterType);
                break;
            case 27:
                strFieldValue = String.valueOf(PayNo);
                break;
            case 28:
                strFieldValue = String.valueOf(BusyType);
                break;
            case 29:
                strFieldValue = String.valueOf(ShouldValueDate);
                break;
            case 30:
                strFieldValue = String.valueOf(ValueDate);
                break;
            case 31:
                strFieldValue = String.valueOf(RectifyMoney);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("InsureAccTraceID")) {
            if( FValue != null && !FValue.equals("")) {
                InsureAccTraceID = new Long(FValue).longValue();
            }
        }
        if (FCode.equalsIgnoreCase("ShardingID")) {
            if( FValue != null && !FValue.equals(""))
            {
                ShardingID = FValue.trim();
            }
            else
                ShardingID = null;
        }
        if (FCode.equalsIgnoreCase("GrpPolNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                GrpPolNo = FValue.trim();
            }
            else
                GrpPolNo = null;
        }
        if (FCode.equalsIgnoreCase("GrpContNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                GrpContNo = FValue.trim();
            }
            else
                GrpContNo = null;
        }
        if (FCode.equalsIgnoreCase("ContNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                ContNo = FValue.trim();
            }
            else
                ContNo = null;
        }
        if (FCode.equalsIgnoreCase("PolNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                PolNo = FValue.trim();
            }
            else
                PolNo = null;
        }
        if (FCode.equalsIgnoreCase("SerialNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                SerialNo = FValue.trim();
            }
            else
                SerialNo = null;
        }
        if (FCode.equalsIgnoreCase("InsuAccNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                InsuAccNo = FValue.trim();
            }
            else
                InsuAccNo = null;
        }
        if (FCode.equalsIgnoreCase("RiskCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                RiskCode = FValue.trim();
            }
            else
                RiskCode = null;
        }
        if (FCode.equalsIgnoreCase("PayPlanCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                PayPlanCode = FValue.trim();
            }
            else
                PayPlanCode = null;
        }
        if (FCode.equalsIgnoreCase("OtherNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                OtherNo = FValue.trim();
            }
            else
                OtherNo = null;
        }
        if (FCode.equalsIgnoreCase("OtherType")) {
            if( FValue != null && !FValue.equals(""))
            {
                OtherType = FValue.trim();
            }
            else
                OtherType = null;
        }
        if (FCode.equalsIgnoreCase("AccAscription")) {
            if( FValue != null && !FValue.equals(""))
            {
                AccAscription = FValue.trim();
            }
            else
                AccAscription = null;
        }
        if (FCode.equalsIgnoreCase("MoneyType")) {
            if( FValue != null && !FValue.equals(""))
            {
                MoneyType = FValue.trim();
            }
            else
                MoneyType = null;
        }
        if (FCode.equalsIgnoreCase("Money")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                Money = d;
            }
        }
        if (FCode.equalsIgnoreCase("UnitCount")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                UnitCount = d;
            }
        }
        if (FCode.equalsIgnoreCase("PayDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                PayDate = FValue.trim();
            }
            else
                PayDate = null;
        }
        if (FCode.equalsIgnoreCase("State")) {
            if( FValue != null && !FValue.equals(""))
            {
                State = FValue.trim();
            }
            else
                State = null;
        }
        if (FCode.equalsIgnoreCase("ManageCom")) {
            if( FValue != null && !FValue.equals(""))
            {
                ManageCom = FValue.trim();
            }
            else
                ManageCom = null;
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            if( FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
                Operator = null;
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeDate = FValue.trim();
            }
            else
                MakeDate = null;
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
                MakeTime = null;
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyDate = FValue.trim();
            }
            else
                ModifyDate = null;
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
                ModifyTime = null;
        }
        if (FCode.equalsIgnoreCase("FeeCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                FeeCode = FValue.trim();
            }
            else
                FeeCode = null;
        }
        if (FCode.equalsIgnoreCase("AccAlterNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                AccAlterNo = FValue.trim();
            }
            else
                AccAlterNo = null;
        }
        if (FCode.equalsIgnoreCase("AccAlterType")) {
            if( FValue != null && !FValue.equals(""))
            {
                AccAlterType = FValue.trim();
            }
            else
                AccAlterType = null;
        }
        if (FCode.equalsIgnoreCase("PayNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                PayNo = FValue.trim();
            }
            else
                PayNo = null;
        }
        if (FCode.equalsIgnoreCase("BusyType")) {
            if( FValue != null && !FValue.equals(""))
            {
                BusyType = FValue.trim();
            }
            else
                BusyType = null;
        }
        if (FCode.equalsIgnoreCase("ShouldValueDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                ShouldValueDate = FValue.trim();
            }
            else
                ShouldValueDate = null;
        }
        if (FCode.equalsIgnoreCase("ValueDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                ValueDate = FValue.trim();
            }
            else
                ValueDate = null;
        }
        if (FCode.equalsIgnoreCase("RectifyMoney")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                RectifyMoney = d;
            }
        }
        return true;
    }


    public String toString() {
    return "LCInsureAccTracePojo [" +
            "InsureAccTraceID="+InsureAccTraceID +
            ", ShardingID="+ShardingID +
            ", GrpPolNo="+GrpPolNo +
            ", GrpContNo="+GrpContNo +
            ", ContNo="+ContNo +
            ", PolNo="+PolNo +
            ", SerialNo="+SerialNo +
            ", InsuAccNo="+InsuAccNo +
            ", RiskCode="+RiskCode +
            ", PayPlanCode="+PayPlanCode +
            ", OtherNo="+OtherNo +
            ", OtherType="+OtherType +
            ", AccAscription="+AccAscription +
            ", MoneyType="+MoneyType +
            ", Money="+Money +
            ", UnitCount="+UnitCount +
            ", PayDate="+PayDate +
            ", State="+State +
            ", ManageCom="+ManageCom +
            ", Operator="+Operator +
            ", MakeDate="+MakeDate +
            ", MakeTime="+MakeTime +
            ", ModifyDate="+ModifyDate +
            ", ModifyTime="+ModifyTime +
            ", FeeCode="+FeeCode +
            ", AccAlterNo="+AccAlterNo +
            ", AccAlterType="+AccAlterType +
            ", PayNo="+PayNo +
            ", BusyType="+BusyType +
            ", ShouldValueDate="+ShouldValueDate +
            ", ValueDate="+ValueDate +
            ", RectifyMoney="+RectifyMoney +"]";
    }
}
