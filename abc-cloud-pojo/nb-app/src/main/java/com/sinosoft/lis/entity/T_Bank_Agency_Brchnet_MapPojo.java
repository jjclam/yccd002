/**
 * Copyright (c) 2018 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.entity;

import com.sinosoft.cloud.cache.annotation.RedisPrimaryHKey;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.Pojo;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;

import java.io.Serializable;

/**
 * <p>ClassName: T_Bank_Agency_Brchnet_MapPojo </p>
 * <p>Copyright: Copyright (c) 2018</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2018-09-07
 */
public class T_Bank_Agency_Brchnet_MapPojo implements Pojo,Serializable {
    // @Field
    /** Bank_agency_brchnet_map_id */
    @RedisPrimaryHKey
    private long BANK_AGENCY_BRCHNET_MAP_ID; 
    /** Bank_code */
    private String BANK_CODE; 
    /** Bank_area_code */
    private String BANK_AREA_CODE; 
    /** Bank_brchnet_code */
    private String BANK_BRCHNET_CODE; 
    /** Agency_brchnet_code */
    private String AGENCY_BRCHNET_CODE; 
    /** Agency_code */
    private String AGENCY_CODE; 
    /** Mngorg_code */
    private String MNGORG_CODE; 
    /** Insert_oper */
    private String INSERT_OPER; 
    /** Insert_time */
    private String  INSERT_TIME;
    /** Insert_consignor */
    private String INSERT_CONSIGNOR; 
    /** Update_oper */
    private String UPDATE_OPER; 
    /** Update_time */
    private String  UPDATE_TIME;
    /** Update_consignor */
    private String UPDATE_CONSIGNOR; 


    public static final int FIELDNUM = 13;    // 数据库表的字段个数

    private FDate fDate = new FDate();        // 处理日期
    public long getBANK_AGENCY_BRCHNET_MAP_ID() {
        return BANK_AGENCY_BRCHNET_MAP_ID;
    }
    public void setBANK_AGENCY_BRCHNET_MAP_ID(long aBANK_AGENCY_BRCHNET_MAP_ID) {
        BANK_AGENCY_BRCHNET_MAP_ID = aBANK_AGENCY_BRCHNET_MAP_ID;
    }
    public void setBANK_AGENCY_BRCHNET_MAP_ID(String aBANK_AGENCY_BRCHNET_MAP_ID) {
        if (aBANK_AGENCY_BRCHNET_MAP_ID != null && !aBANK_AGENCY_BRCHNET_MAP_ID.equals("")) {
            BANK_AGENCY_BRCHNET_MAP_ID = new Long(aBANK_AGENCY_BRCHNET_MAP_ID).longValue();
        }
    }

    public String getBANK_CODE() {
        return BANK_CODE;
    }
    public void setBANK_CODE(String aBANK_CODE) {
        BANK_CODE = aBANK_CODE;
    }
    public String getBANK_AREA_CODE() {
        return BANK_AREA_CODE;
    }
    public void setBANK_AREA_CODE(String aBANK_AREA_CODE) {
        BANK_AREA_CODE = aBANK_AREA_CODE;
    }
    public String getBANK_BRCHNET_CODE() {
        return BANK_BRCHNET_CODE;
    }
    public void setBANK_BRCHNET_CODE(String aBANK_BRCHNET_CODE) {
        BANK_BRCHNET_CODE = aBANK_BRCHNET_CODE;
    }
    public String getAGENCY_BRCHNET_CODE() {
        return AGENCY_BRCHNET_CODE;
    }
    public void setAGENCY_BRCHNET_CODE(String aAGENCY_BRCHNET_CODE) {
        AGENCY_BRCHNET_CODE = aAGENCY_BRCHNET_CODE;
    }
    public String getAGENCY_CODE() {
        return AGENCY_CODE;
    }
    public void setAGENCY_CODE(String aAGENCY_CODE) {
        AGENCY_CODE = aAGENCY_CODE;
    }
    public String getMNGORG_CODE() {
        return MNGORG_CODE;
    }
    public void setMNGORG_CODE(String aMNGORG_CODE) {
        MNGORG_CODE = aMNGORG_CODE;
    }
    public String getINSERT_OPER() {
        return INSERT_OPER;
    }
    public void setINSERT_OPER(String aINSERT_OPER) {
        INSERT_OPER = aINSERT_OPER;
    }
    public String getINSERT_TIME() {
        return INSERT_TIME;
    }
    public void setINSERT_TIME(String aINSERT_TIME) {
        INSERT_TIME = aINSERT_TIME;
    }
    public String getINSERT_CONSIGNOR() {
        return INSERT_CONSIGNOR;
    }
    public void setINSERT_CONSIGNOR(String aINSERT_CONSIGNOR) {
        INSERT_CONSIGNOR = aINSERT_CONSIGNOR;
    }
    public String getUPDATE_OPER() {
        return UPDATE_OPER;
    }
    public void setUPDATE_OPER(String aUPDATE_OPER) {
        UPDATE_OPER = aUPDATE_OPER;
    }
    public String getUPDATE_TIME() {
        return UPDATE_TIME;
    }
    public void setUPDATE_TIME(String aUPDATE_TIME) {
        UPDATE_TIME = aUPDATE_TIME;
    }
    public String getUPDATE_CONSIGNOR() {
        return UPDATE_CONSIGNOR;
    }
    public void setUPDATE_CONSIGNOR(String aUPDATE_CONSIGNOR) {
        UPDATE_CONSIGNOR = aUPDATE_CONSIGNOR;
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("BANK_AGENCY_BRCHNET_MAP_ID") ) {
            return 0;
        }
        if( strFieldName.equals("BANK_CODE") ) {
            return 1;
        }
        if( strFieldName.equals("BANK_AREA_CODE") ) {
            return 2;
        }
        if( strFieldName.equals("BANK_BRCHNET_CODE") ) {
            return 3;
        }
        if( strFieldName.equals("AGENCY_BRCHNET_CODE") ) {
            return 4;
        }
        if( strFieldName.equals("AGENCY_CODE") ) {
            return 5;
        }
        if( strFieldName.equals("MNGORG_CODE") ) {
            return 6;
        }
        if( strFieldName.equals("INSERT_OPER") ) {
            return 7;
        }
        if( strFieldName.equals("INSERT_TIME") ) {
            return 8;
        }
        if( strFieldName.equals("INSERT_CONSIGNOR") ) {
            return 9;
        }
        if( strFieldName.equals("UPDATE_OPER") ) {
            return 10;
        }
        if( strFieldName.equals("UPDATE_TIME") ) {
            return 11;
        }
        if( strFieldName.equals("UPDATE_CONSIGNOR") ) {
            return 12;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "BANK_AGENCY_BRCHNET_MAP_ID";
                break;
            case 1:
                strFieldName = "BANK_CODE";
                break;
            case 2:
                strFieldName = "BANK_AREA_CODE";
                break;
            case 3:
                strFieldName = "BANK_BRCHNET_CODE";
                break;
            case 4:
                strFieldName = "AGENCY_BRCHNET_CODE";
                break;
            case 5:
                strFieldName = "AGENCY_CODE";
                break;
            case 6:
                strFieldName = "MNGORG_CODE";
                break;
            case 7:
                strFieldName = "INSERT_OPER";
                break;
            case 8:
                strFieldName = "INSERT_TIME";
                break;
            case 9:
                strFieldName = "INSERT_CONSIGNOR";
                break;
            case 10:
                strFieldName = "UPDATE_OPER";
                break;
            case 11:
                strFieldName = "UPDATE_TIME";
                break;
            case 12:
                strFieldName = "UPDATE_CONSIGNOR";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "BANK_AGENCY_BRCHNET_MAP_ID":
                return Schema.TYPE_LONG;
            case "BANK_CODE":
                return Schema.TYPE_STRING;
            case "BANK_AREA_CODE":
                return Schema.TYPE_STRING;
            case "BANK_BRCHNET_CODE":
                return Schema.TYPE_STRING;
            case "AGENCY_BRCHNET_CODE":
                return Schema.TYPE_STRING;
            case "AGENCY_CODE":
                return Schema.TYPE_STRING;
            case "MNGORG_CODE":
                return Schema.TYPE_STRING;
            case "INSERT_OPER":
                return Schema.TYPE_STRING;
            case "INSERT_TIME":
                return Schema.TYPE_STRING;
            case "INSERT_CONSIGNOR":
                return Schema.TYPE_STRING;
            case "UPDATE_OPER":
                return Schema.TYPE_STRING;
            case "UPDATE_TIME":
                return Schema.TYPE_STRING;
            case "UPDATE_CONSIGNOR":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_LONG;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_STRING;
            case 6:
                return Schema.TYPE_STRING;
            case 7:
                return Schema.TYPE_STRING;
            case 8:
                return Schema.TYPE_STRING;
            case 9:
                return Schema.TYPE_STRING;
            case 10:
                return Schema.TYPE_STRING;
            case 11:
                return Schema.TYPE_STRING;
            case 12:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("BANK_AGENCY_BRCHNET_MAP_ID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BANK_AGENCY_BRCHNET_MAP_ID));
        }
        if (FCode.equalsIgnoreCase("BANK_CODE")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BANK_CODE));
        }
        if (FCode.equalsIgnoreCase("BANK_AREA_CODE")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BANK_AREA_CODE));
        }
        if (FCode.equalsIgnoreCase("BANK_BRCHNET_CODE")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BANK_BRCHNET_CODE));
        }
        if (FCode.equalsIgnoreCase("AGENCY_BRCHNET_CODE")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AGENCY_BRCHNET_CODE));
        }
        if (FCode.equalsIgnoreCase("AGENCY_CODE")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AGENCY_CODE));
        }
        if (FCode.equalsIgnoreCase("MNGORG_CODE")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MNGORG_CODE));
        }
        if (FCode.equalsIgnoreCase("INSERT_OPER")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(INSERT_OPER));
        }
        if (FCode.equalsIgnoreCase("INSERT_TIME")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(INSERT_TIME));
        }
        if (FCode.equalsIgnoreCase("INSERT_CONSIGNOR")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(INSERT_CONSIGNOR));
        }
        if (FCode.equalsIgnoreCase("UPDATE_OPER")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(UPDATE_OPER));
        }
        if (FCode.equalsIgnoreCase("UPDATE_TIME")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(UPDATE_TIME));
        }
        if (FCode.equalsIgnoreCase("UPDATE_CONSIGNOR")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(UPDATE_CONSIGNOR));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = String.valueOf(BANK_AGENCY_BRCHNET_MAP_ID);
                break;
            case 1:
                strFieldValue = String.valueOf(BANK_CODE);
                break;
            case 2:
                strFieldValue = String.valueOf(BANK_AREA_CODE);
                break;
            case 3:
                strFieldValue = String.valueOf(BANK_BRCHNET_CODE);
                break;
            case 4:
                strFieldValue = String.valueOf(AGENCY_BRCHNET_CODE);
                break;
            case 5:
                strFieldValue = String.valueOf(AGENCY_CODE);
                break;
            case 6:
                strFieldValue = String.valueOf(MNGORG_CODE);
                break;
            case 7:
                strFieldValue = String.valueOf(INSERT_OPER);
                break;
            case 8:
                strFieldValue = String.valueOf(INSERT_TIME);
                break;
            case 9:
                strFieldValue = String.valueOf(INSERT_CONSIGNOR);
                break;
            case 10:
                strFieldValue = String.valueOf(UPDATE_OPER);
                break;
            case 11:
                strFieldValue = String.valueOf(UPDATE_TIME);
                break;
            case 12:
                strFieldValue = String.valueOf(UPDATE_CONSIGNOR);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("BANK_AGENCY_BRCHNET_MAP_ID")) {
            if( FValue != null && !FValue.equals("")) {
                BANK_AGENCY_BRCHNET_MAP_ID = new Long(FValue).longValue();
            }
        }
        if (FCode.equalsIgnoreCase("BANK_CODE")) {
            if( FValue != null && !FValue.equals(""))
            {
                BANK_CODE = FValue.trim();
            }
            else
                BANK_CODE = null;
        }
        if (FCode.equalsIgnoreCase("BANK_AREA_CODE")) {
            if( FValue != null && !FValue.equals(""))
            {
                BANK_AREA_CODE = FValue.trim();
            }
            else
                BANK_AREA_CODE = null;
        }
        if (FCode.equalsIgnoreCase("BANK_BRCHNET_CODE")) {
            if( FValue != null && !FValue.equals(""))
            {
                BANK_BRCHNET_CODE = FValue.trim();
            }
            else
                BANK_BRCHNET_CODE = null;
        }
        if (FCode.equalsIgnoreCase("AGENCY_BRCHNET_CODE")) {
            if( FValue != null && !FValue.equals(""))
            {
                AGENCY_BRCHNET_CODE = FValue.trim();
            }
            else
                AGENCY_BRCHNET_CODE = null;
        }
        if (FCode.equalsIgnoreCase("AGENCY_CODE")) {
            if( FValue != null && !FValue.equals(""))
            {
                AGENCY_CODE = FValue.trim();
            }
            else
                AGENCY_CODE = null;
        }
        if (FCode.equalsIgnoreCase("MNGORG_CODE")) {
            if( FValue != null && !FValue.equals(""))
            {
                MNGORG_CODE = FValue.trim();
            }
            else
                MNGORG_CODE = null;
        }
        if (FCode.equalsIgnoreCase("INSERT_OPER")) {
            if( FValue != null && !FValue.equals(""))
            {
                INSERT_OPER = FValue.trim();
            }
            else
                INSERT_OPER = null;
        }
        if (FCode.equalsIgnoreCase("INSERT_TIME")) {
            if( FValue != null && !FValue.equals(""))
            {
                INSERT_TIME = FValue.trim();
            }
            else
                INSERT_TIME = null;
        }
        if (FCode.equalsIgnoreCase("INSERT_CONSIGNOR")) {
            if( FValue != null && !FValue.equals(""))
            {
                INSERT_CONSIGNOR = FValue.trim();
            }
            else
                INSERT_CONSIGNOR = null;
        }
        if (FCode.equalsIgnoreCase("UPDATE_OPER")) {
            if( FValue != null && !FValue.equals(""))
            {
                UPDATE_OPER = FValue.trim();
            }
            else
                UPDATE_OPER = null;
        }
        if (FCode.equalsIgnoreCase("UPDATE_TIME")) {
            if( FValue != null && !FValue.equals(""))
            {
                UPDATE_TIME = FValue.trim();
            }
            else
                UPDATE_TIME = null;
        }
        if (FCode.equalsIgnoreCase("UPDATE_CONSIGNOR")) {
            if( FValue != null && !FValue.equals(""))
            {
                UPDATE_CONSIGNOR = FValue.trim();
            }
            else
                UPDATE_CONSIGNOR = null;
        }
        return true;
    }


    public String toString() {
    return "T_Bank_Agency_Brchnet_MapPojo [" +
            "BANK_AGENCY_BRCHNET_MAP_ID="+BANK_AGENCY_BRCHNET_MAP_ID +
            ", BANK_CODE="+BANK_CODE +
            ", BANK_AREA_CODE="+BANK_AREA_CODE +
            ", BANK_BRCHNET_CODE="+BANK_BRCHNET_CODE +
            ", AGENCY_BRCHNET_CODE="+AGENCY_BRCHNET_CODE +
            ", AGENCY_CODE="+AGENCY_CODE +
            ", MNGORG_CODE="+MNGORG_CODE +
            ", INSERT_OPER="+INSERT_OPER +
            ", INSERT_TIME="+INSERT_TIME +
            ", INSERT_CONSIGNOR="+INSERT_CONSIGNOR +
            ", UPDATE_OPER="+UPDATE_OPER +
            ", UPDATE_TIME="+UPDATE_TIME +
            ", UPDATE_CONSIGNOR="+UPDATE_CONSIGNOR +"]";
    }
}
