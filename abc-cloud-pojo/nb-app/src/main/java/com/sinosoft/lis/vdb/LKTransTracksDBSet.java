/**
 * Copyright (c) 2018 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.vdb;

import com.sinosoft.lis.vschema.LKTransTracksSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.DBConnPool;
import com.sinosoft.utility.DBOper;
import com.sinosoft.utility.ExceptionUtils;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;

/**
 * <p>ClassName: LKTransTracksDBSet </p>
 * <p>Description: DB层多记录数据库操作类文件 </p>
 * <p>Copyright: Copyright (c) 2018</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2018-09-17
 */
public class LKTransTracksDBSet extends LKTransTracksSet {
    // @Field
    private Connection con;
    private DBOper db;
    /**
    * flag = true: 传入Connection
    * flag = false: 不传入Connection
    **/
    private boolean mflag = false;

    // @Constructor
    public LKTransTracksDBSet(Connection tConnection) {
        con = tConnection;
        db = new DBOper(con,"LKTransTracks");
        mflag = true;
    }

    public LKTransTracksDBSet() {
        db = new DBOper( "LKTransTracks" );
    }
    // @Method
    public boolean deleteSQL() {
        if (db.deleteSQL(this)) {
            return true;
        } else {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LKTransTracksDBSet";
            tError.functionName = "deleteSQL";
            tError.errorMessage = "操作失败!";
            this.mErrors .addOneError(tError);
            return false;
        }
    }

    /**
     * 删除操作
     * 删除条件：主键
     * @return boolean
     */
    public boolean delete() {
        PreparedStatement pstmt = null;

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try {
            int tCount = this.size();
            pstmt = con.prepareStatement("DELETE FROM LKTransTracks WHERE  1=1  AND TransID = ?");
            for (int i = 1; i <= tCount; i++) {
            pstmt.setLong(1, this.get(i).getTransID());
                pstmt.addBatch();
            }
            pstmt.executeBatch();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            ex.printStackTrace();
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LKTransTracksDBSet";
            tError.functionName = "delete()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            try {
                pstmt.close();
            } catch (Exception e){
                e.printStackTrace();
            }

            if (!mflag) {
                try {
                    con.close();
                } catch (Exception e){e.printStackTrace();}
            }

            return false;
        }

        if( !mflag ) {
            try {
                con.close();
            } catch (Exception e){e.printStackTrace();}
        }

        return true;
    }

    /**
     * 更新操作
     * 更新条件：主键
     * @return boolean
     */
    public boolean update() {
        PreparedStatement pstmt = null;

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try {
            int tCount = this.size();
            pstmt = con.prepareStatement("UPDATE LKTransTracks SET  TransID = ? , ShadingID = ? , TransNo = ? , AccessChnl = ? , AccessChnlSub = ? , TransCode = ? , TransCodeOri = ? , BankCode = ? , BankBranch = ? , BankNode = ? , ContNo = ? , ProposalNo = ? , TransMony = ? , TransDate = ? , TransTime = ? , ContType = ? , PHConclusion = ? , UWConclusion = ? , SellsWay = ? , Perform = ? , BalanceStateCode = ? , BalanceResult = ? , BalanceResultDesc = ? , Temp1 = ? , Temp2 = ? , Temp3 = ? , Temp4 = ? , Temp5 = ? , PayMode = ? WHERE  1=1  AND TransID = ?");
            for (int i = 1; i <= tCount; i++) {
            pstmt.setLong(1, this.get(i).getTransID());
            if(this.get(i).getShadingID() == null || this.get(i).getShadingID().equals("null")) {
            	pstmt.setString(2,null);
            } else {
            	pstmt.setString(2, this.get(i).getShadingID());
            }
            if(this.get(i).getTransNo() == null || this.get(i).getTransNo().equals("null")) {
            	pstmt.setString(3,null);
            } else {
            	pstmt.setString(3, this.get(i).getTransNo());
            }
            if(this.get(i).getAccessChnl() == null || this.get(i).getAccessChnl().equals("null")) {
            	pstmt.setString(4,null);
            } else {
            	pstmt.setString(4, this.get(i).getAccessChnl());
            }
            if(this.get(i).getAccessChnlSub() == null || this.get(i).getAccessChnlSub().equals("null")) {
            	pstmt.setString(5,null);
            } else {
            	pstmt.setString(5, this.get(i).getAccessChnlSub());
            }
            if(this.get(i).getTransCode() == null || this.get(i).getTransCode().equals("null")) {
            	pstmt.setString(6,null);
            } else {
            	pstmt.setString(6, this.get(i).getTransCode());
            }
            if(this.get(i).getTransCodeOri() == null || this.get(i).getTransCodeOri().equals("null")) {
            	pstmt.setString(7,null);
            } else {
            	pstmt.setString(7, this.get(i).getTransCodeOri());
            }
            if(this.get(i).getBankCode() == null || this.get(i).getBankCode().equals("null")) {
            	pstmt.setString(8,null);
            } else {
            	pstmt.setString(8, this.get(i).getBankCode());
            }
            if(this.get(i).getBankBranch() == null || this.get(i).getBankBranch().equals("null")) {
            	pstmt.setString(9,null);
            } else {
            	pstmt.setString(9, this.get(i).getBankBranch());
            }
            if(this.get(i).getBankNode() == null || this.get(i).getBankNode().equals("null")) {
            	pstmt.setString(10,null);
            } else {
            	pstmt.setString(10, this.get(i).getBankNode());
            }
            if(this.get(i).getContNo() == null || this.get(i).getContNo().equals("null")) {
            	pstmt.setString(11,null);
            } else {
            	pstmt.setString(11, this.get(i).getContNo());
            }
            if(this.get(i).getProposalNo() == null || this.get(i).getProposalNo().equals("null")) {
            	pstmt.setString(12,null);
            } else {
            	pstmt.setString(12, this.get(i).getProposalNo());
            }
            pstmt.setDouble(13, this.get(i).getTransMony());
            if(this.get(i).getTransDate() == null || this.get(i).getTransDate().equals("null")) {
                pstmt.setDate(14,null);
            } else {
                pstmt.setDate(14, Date.valueOf(this.get(i).getTransDate()));
            }
            if(this.get(i).getTransTime() == null || this.get(i).getTransTime().equals("null")) {
            	pstmt.setString(15,null);
            } else {
            	pstmt.setString(15, this.get(i).getTransTime());
            }
            if(this.get(i).getContType() == null || this.get(i).getContType().equals("null")) {
            	pstmt.setString(16,null);
            } else {
            	pstmt.setString(16, this.get(i).getContType());
            }
            if(this.get(i).getPHConclusion() == null || this.get(i).getPHConclusion().equals("null")) {
            	pstmt.setString(17,null);
            } else {
            	pstmt.setString(17, this.get(i).getPHConclusion());
            }
            if(this.get(i).getUWConclusion() == null || this.get(i).getUWConclusion().equals("null")) {
            	pstmt.setString(18,null);
            } else {
            	pstmt.setString(18, this.get(i).getUWConclusion());
            }
            if(this.get(i).getSellsWay() == null || this.get(i).getSellsWay().equals("null")) {
            	pstmt.setString(19,null);
            } else {
            	pstmt.setString(19, this.get(i).getSellsWay());
            }
            if(this.get(i).getPerform() == null || this.get(i).getPerform().equals("null")) {
            	pstmt.setString(20,null);
            } else {
            	pstmt.setString(20, this.get(i).getPerform());
            }
            if(this.get(i).getBalanceStateCode() == null || this.get(i).getBalanceStateCode().equals("null")) {
            	pstmt.setString(21,null);
            } else {
            	pstmt.setString(21, this.get(i).getBalanceStateCode());
            }
            if(this.get(i).getBalanceResult() == null || this.get(i).getBalanceResult().equals("null")) {
            	pstmt.setString(22,null);
            } else {
            	pstmt.setString(22, this.get(i).getBalanceResult());
            }
            if(this.get(i).getBalanceResultDesc() == null || this.get(i).getBalanceResultDesc().equals("null")) {
            	pstmt.setString(23,null);
            } else {
            	pstmt.setString(23, this.get(i).getBalanceResultDesc());
            }
            if(this.get(i).getTemp1() == null || this.get(i).getTemp1().equals("null")) {
            	pstmt.setString(24,null);
            } else {
            	pstmt.setString(24, this.get(i).getTemp1());
            }
            if(this.get(i).getTemp2() == null || this.get(i).getTemp2().equals("null")) {
            	pstmt.setString(25,null);
            } else {
            	pstmt.setString(25, this.get(i).getTemp2());
            }
            if(this.get(i).getTemp3() == null || this.get(i).getTemp3().equals("null")) {
            	pstmt.setString(26,null);
            } else {
            	pstmt.setString(26, this.get(i).getTemp3());
            }
            if(this.get(i).getTemp4() == null || this.get(i).getTemp4().equals("null")) {
            	pstmt.setString(27,null);
            } else {
            	pstmt.setString(27, this.get(i).getTemp4());
            }
            if(this.get(i).getTemp5() == null || this.get(i).getTemp5().equals("null")) {
            	pstmt.setString(28,null);
            } else {
            	pstmt.setString(28, this.get(i).getTemp5());
            }
            if(this.get(i).getPayMode() == null || this.get(i).getPayMode().equals("null")) {
            	pstmt.setString(29,null);
            } else {
            	pstmt.setString(29, this.get(i).getPayMode());
            }
            // set where condition
            pstmt.setLong(30, this.get(i).getTransID());
                pstmt.addBatch();
            }
            pstmt.executeBatch();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            ex.printStackTrace();
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LKTransTracksDBSet";
            tError.functionName = "update()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            try {
                pstmt.close();
            } catch (Exception e){e.printStackTrace();}

            if( !mflag ) {
                try {
                    con.close();
                } catch (Exception e){e.printStackTrace();}
            }

            return false;
        }

        if( !mflag ) {
            try {
                con.close();
            } catch (Exception e){e.printStackTrace();}
        }

        return true;
    }

    /**
     * 新增操作
     * @return boolean
     */
    public boolean insert() {
        PreparedStatement pstmt = null;

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try
        {
            int tCount = this.size();
            pstmt = con.prepareStatement("INSERT INTO LKTransTracks VALUES( ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ?)");
            for (int i = 1; i <= tCount; i++) {
            pstmt.setLong(1, this.get(i).getTransID());
            if(this.get(i).getShadingID() == null || this.get(i).getShadingID().equals("null")) {
            	pstmt.setString(2,null);
            } else {
            	pstmt.setString(2, this.get(i).getShadingID());
            }
            if(this.get(i).getTransNo() == null || this.get(i).getTransNo().equals("null")) {
            	pstmt.setString(3,null);
            } else {
            	pstmt.setString(3, this.get(i).getTransNo());
            }
            if(this.get(i).getAccessChnl() == null || this.get(i).getAccessChnl().equals("null")) {
            	pstmt.setString(4,null);
            } else {
            	pstmt.setString(4, this.get(i).getAccessChnl());
            }
            if(this.get(i).getAccessChnlSub() == null || this.get(i).getAccessChnlSub().equals("null")) {
            	pstmt.setString(5,null);
            } else {
            	pstmt.setString(5, this.get(i).getAccessChnlSub());
            }
            if(this.get(i).getTransCode() == null || this.get(i).getTransCode().equals("null")) {
            	pstmt.setString(6,null);
            } else {
            	pstmt.setString(6, this.get(i).getTransCode());
            }
            if(this.get(i).getTransCodeOri() == null || this.get(i).getTransCodeOri().equals("null")) {
            	pstmt.setString(7,null);
            } else {
            	pstmt.setString(7, this.get(i).getTransCodeOri());
            }
            if(this.get(i).getBankCode() == null || this.get(i).getBankCode().equals("null")) {
            	pstmt.setString(8,null);
            } else {
            	pstmt.setString(8, this.get(i).getBankCode());
            }
            if(this.get(i).getBankBranch() == null || this.get(i).getBankBranch().equals("null")) {
            	pstmt.setString(9,null);
            } else {
            	pstmt.setString(9, this.get(i).getBankBranch());
            }
            if(this.get(i).getBankNode() == null || this.get(i).getBankNode().equals("null")) {
            	pstmt.setString(10,null);
            } else {
            	pstmt.setString(10, this.get(i).getBankNode());
            }
            if(this.get(i).getContNo() == null || this.get(i).getContNo().equals("null")) {
            	pstmt.setString(11,null);
            } else {
            	pstmt.setString(11, this.get(i).getContNo());
            }
            if(this.get(i).getProposalNo() == null || this.get(i).getProposalNo().equals("null")) {
            	pstmt.setString(12,null);
            } else {
            	pstmt.setString(12, this.get(i).getProposalNo());
            }
            pstmt.setDouble(13, this.get(i).getTransMony());
            if(this.get(i).getTransDate() == null || this.get(i).getTransDate().equals("null")) {
                pstmt.setDate(14,null);
            } else {
                pstmt.setDate(14, Date.valueOf(this.get(i).getTransDate()));
            }
            if(this.get(i).getTransTime() == null || this.get(i).getTransTime().equals("null")) {
            	pstmt.setString(15,null);
            } else {
            	pstmt.setString(15, this.get(i).getTransTime());
            }
            if(this.get(i).getContType() == null || this.get(i).getContType().equals("null")) {
            	pstmt.setString(16,null);
            } else {
            	pstmt.setString(16, this.get(i).getContType());
            }
            if(this.get(i).getPHConclusion() == null || this.get(i).getPHConclusion().equals("null")) {
            	pstmt.setString(17,null);
            } else {
            	pstmt.setString(17, this.get(i).getPHConclusion());
            }
            if(this.get(i).getUWConclusion() == null || this.get(i).getUWConclusion().equals("null")) {
            	pstmt.setString(18,null);
            } else {
            	pstmt.setString(18, this.get(i).getUWConclusion());
            }
            if(this.get(i).getSellsWay() == null || this.get(i).getSellsWay().equals("null")) {
            	pstmt.setString(19,null);
            } else {
            	pstmt.setString(19, this.get(i).getSellsWay());
            }
            if(this.get(i).getPerform() == null || this.get(i).getPerform().equals("null")) {
            	pstmt.setString(20,null);
            } else {
            	pstmt.setString(20, this.get(i).getPerform());
            }
            if(this.get(i).getBalanceStateCode() == null || this.get(i).getBalanceStateCode().equals("null")) {
            	pstmt.setString(21,null);
            } else {
            	pstmt.setString(21, this.get(i).getBalanceStateCode());
            }
            if(this.get(i).getBalanceResult() == null || this.get(i).getBalanceResult().equals("null")) {
            	pstmt.setString(22,null);
            } else {
            	pstmt.setString(22, this.get(i).getBalanceResult());
            }
            if(this.get(i).getBalanceResultDesc() == null || this.get(i).getBalanceResultDesc().equals("null")) {
            	pstmt.setString(23,null);
            } else {
            	pstmt.setString(23, this.get(i).getBalanceResultDesc());
            }
            if(this.get(i).getTemp1() == null || this.get(i).getTemp1().equals("null")) {
            	pstmt.setString(24,null);
            } else {
            	pstmt.setString(24, this.get(i).getTemp1());
            }
            if(this.get(i).getTemp2() == null || this.get(i).getTemp2().equals("null")) {
            	pstmt.setString(25,null);
            } else {
            	pstmt.setString(25, this.get(i).getTemp2());
            }
            if(this.get(i).getTemp3() == null || this.get(i).getTemp3().equals("null")) {
            	pstmt.setString(26,null);
            } else {
            	pstmt.setString(26, this.get(i).getTemp3());
            }
            if(this.get(i).getTemp4() == null || this.get(i).getTemp4().equals("null")) {
            	pstmt.setString(27,null);
            } else {
            	pstmt.setString(27, this.get(i).getTemp4());
            }
            if(this.get(i).getTemp5() == null || this.get(i).getTemp5().equals("null")) {
            	pstmt.setString(28,null);
            } else {
            	pstmt.setString(28, this.get(i).getTemp5());
            }
            if(this.get(i).getPayMode() == null || this.get(i).getPayMode().equals("null")) {
            	pstmt.setString(29,null);
            } else {
            	pstmt.setString(29, this.get(i).getPayMode());
            }
                pstmt.addBatch();
            }
            pstmt.executeBatch();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            ex.printStackTrace();
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LKTransTracksDBSet";
            tError.functionName = "insert()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            try {
                pstmt.close();
            } catch (Exception e){e.printStackTrace();}

            if( !mflag ) {
                try {
                    con.close();
                } catch (Exception e){e.printStackTrace();}
            }

            return false;
        }

        if( !mflag ) {
            try {
                con.close();
            } catch (Exception e){e.printStackTrace();}
        }

        return true;
    }
}
