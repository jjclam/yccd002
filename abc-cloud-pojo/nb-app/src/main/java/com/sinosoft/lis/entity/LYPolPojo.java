/**
 * Copyright (c) 2017 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.entity;

import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.Pojo;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;

import java.io.Serializable;

/**
 * <p>ClassName: LYPolPojo </p>
 * <p>Copyright: Copyright (c) 2017</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2017-10-17
 */
public class LYPolPojo implements Pojo,Serializable {
    // @Field
    /** Id */
    private long YPolID; 
    /** Fk_lyverifyapp */
    private long VerifyAppID; 
    /** Shardingid */
    private String ShardingID; 
    /** 初审号码 */
    private String ContNo; 
    /** 受理日期 */
    private String  ApplyDate;
    /** 受理完毕日期 */
    private String  ApplyEndDate;
    /** 初审状态 */
    private String State; 
    /** 险种编码 */
    private String RiskCode; 
    /** 管理机构 */
    private String ManageCom; 
    /** 银代银行代码 */
    private String AgentBankCode; 
    /** 银行网点(或中介编码） */
    private String AgentCom; 
    /** 银代柜员（或中介代理人） */
    private String BankAgent; 
    /** 代理人姓名（中介） */
    private String AgentName; 
    /** 代理人电话(中介) */
    private String AgentPhone; 
    /** 代理人编码(银代专官员或中介经理) */
    private String AgentCode; 
    /** 代理人组别 */
    private String AgentGroup; 
    /** 销售渠道 */
    private String SaleChnl; 
    /** 总保费 */
    private double Prem; 
    /** 交费间隔 */
    private double PayIntv; 
    /** 交费年期 */
    private double PayYears; 
    /** 备用属性字段1 */
    private String StandbyFlag1; 
    /** 备用属性字段2 */
    private String StandbyFlag2; 
    /** 备用属性字段3 */
    private String StandbyFlag3; 
    /** 操作员 */
    private String Operator; 
    /** 入机日期 */
    private String  MakeDate;
    /** 入机时间 */
    private String MakeTime; 
    /** 最后一次修改日期 */
    private String  ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime; 
    /** 万能基本保费 */
    private double Prem1; 
    /** 万能额外保费 */
    private double Prem2; 
    /** 万能追加保费 */
    private double Prem3; 
    /** 万能基本保额 */
    private double Amnt1; 
    /** 万能额外保额 */
    private double Amnt2; 
    /** 万能追加保额 */
    private double Amnt3; 
    /** 险种的保额 */
    private double Amnt; 
    /** 总份数 */
    private double Mult; 
    /** 组合险代码 */
    private String ProdSetCode; 
    /** 组合份数 */
    private double ProdSetMult; 
    /** 终交年龄年期标志 */
    private String PayEndYearFlag; 
    /** 终交年龄年期 */
    private int PayEndYear; 
    /** 保险年龄年期标志 */
    private String InsuYearFlag; 
    /** 保险年龄年期 */
    private int InsuYear; 
    /** 领取年龄年期标志 */
    private String GetYearFlag; 
    /** 领取年龄年期 */
    private int GetYear; 


    public static final int FIELDNUM = 44;    // 数据库表的字段个数

    private FDate fDate = new FDate();        // 处理日期
    public long getYPolID() {
        return YPolID;
    }
    public void setYPolID(long aYPolID) {
        YPolID = aYPolID;
    }
    public void setYPolID(String aYPolID) {
        if (aYPolID != null && !aYPolID.equals("")) {
            YPolID = new Long(aYPolID).longValue();
        }
    }

    public long getVerifyAppID() {
        return VerifyAppID;
    }
    public void setVerifyAppID(long aVerifyAppID) {
        VerifyAppID = aVerifyAppID;
    }
    public void setVerifyAppID(String aVerifyAppID) {
        if (aVerifyAppID != null && !aVerifyAppID.equals("")) {
            VerifyAppID = new Long(aVerifyAppID).longValue();
        }
    }

    public String getShardingID() {
        return ShardingID;
    }
    public void setShardingID(String aShardingID) {
        ShardingID = aShardingID;
    }
    public String getContNo() {
        return ContNo;
    }
    public void setContNo(String aContNo) {
        ContNo = aContNo;
    }
    public String getApplyDate() {
        return ApplyDate;
    }
    public void setApplyDate(String aApplyDate) {
        ApplyDate = aApplyDate;
    }
    public String getApplyEndDate() {
        return ApplyEndDate;
    }
    public void setApplyEndDate(String aApplyEndDate) {
        ApplyEndDate = aApplyEndDate;
    }
    public String getState() {
        return State;
    }
    public void setState(String aState) {
        State = aState;
    }
    public String getRiskCode() {
        return RiskCode;
    }
    public void setRiskCode(String aRiskCode) {
        RiskCode = aRiskCode;
    }
    public String getManageCom() {
        return ManageCom;
    }
    public void setManageCom(String aManageCom) {
        ManageCom = aManageCom;
    }
    public String getAgentBankCode() {
        return AgentBankCode;
    }
    public void setAgentBankCode(String aAgentBankCode) {
        AgentBankCode = aAgentBankCode;
    }
    public String getAgentCom() {
        return AgentCom;
    }
    public void setAgentCom(String aAgentCom) {
        AgentCom = aAgentCom;
    }
    public String getBankAgent() {
        return BankAgent;
    }
    public void setBankAgent(String aBankAgent) {
        BankAgent = aBankAgent;
    }
    public String getAgentName() {
        return AgentName;
    }
    public void setAgentName(String aAgentName) {
        AgentName = aAgentName;
    }
    public String getAgentPhone() {
        return AgentPhone;
    }
    public void setAgentPhone(String aAgentPhone) {
        AgentPhone = aAgentPhone;
    }
    public String getAgentCode() {
        return AgentCode;
    }
    public void setAgentCode(String aAgentCode) {
        AgentCode = aAgentCode;
    }
    public String getAgentGroup() {
        return AgentGroup;
    }
    public void setAgentGroup(String aAgentGroup) {
        AgentGroup = aAgentGroup;
    }
    public String getSaleChnl() {
        return SaleChnl;
    }
    public void setSaleChnl(String aSaleChnl) {
        SaleChnl = aSaleChnl;
    }
    public double getPrem() {
        return Prem;
    }
    public void setPrem(double aPrem) {
        Prem = aPrem;
    }
    public void setPrem(String aPrem) {
        if (aPrem != null && !aPrem.equals("")) {
            Double tDouble = new Double(aPrem);
            double d = tDouble.doubleValue();
            Prem = d;
        }
    }

    public double getPayIntv() {
        return PayIntv;
    }
    public void setPayIntv(double aPayIntv) {
        PayIntv = aPayIntv;
    }
    public void setPayIntv(String aPayIntv) {
        if (aPayIntv != null && !aPayIntv.equals("")) {
            Double tDouble = new Double(aPayIntv);
            double d = tDouble.doubleValue();
            PayIntv = d;
        }
    }

    public double getPayYears() {
        return PayYears;
    }
    public void setPayYears(double aPayYears) {
        PayYears = aPayYears;
    }
    public void setPayYears(String aPayYears) {
        if (aPayYears != null && !aPayYears.equals("")) {
            Double tDouble = new Double(aPayYears);
            double d = tDouble.doubleValue();
            PayYears = d;
        }
    }

    public String getStandbyFlag1() {
        return StandbyFlag1;
    }
    public void setStandbyFlag1(String aStandbyFlag1) {
        StandbyFlag1 = aStandbyFlag1;
    }
    public String getStandbyFlag2() {
        return StandbyFlag2;
    }
    public void setStandbyFlag2(String aStandbyFlag2) {
        StandbyFlag2 = aStandbyFlag2;
    }
    public String getStandbyFlag3() {
        return StandbyFlag3;
    }
    public void setStandbyFlag3(String aStandbyFlag3) {
        StandbyFlag3 = aStandbyFlag3;
    }
    public String getOperator() {
        return Operator;
    }
    public void setOperator(String aOperator) {
        Operator = aOperator;
    }
    public String getMakeDate() {
        return MakeDate;
    }
    public void setMakeDate(String aMakeDate) {
        MakeDate = aMakeDate;
    }
    public String getMakeTime() {
        return MakeTime;
    }
    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }
    public String getModifyDate() {
        return ModifyDate;
    }
    public void setModifyDate(String aModifyDate) {
        ModifyDate = aModifyDate;
    }
    public String getModifyTime() {
        return ModifyTime;
    }
    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }
    public double getPrem1() {
        return Prem1;
    }
    public void setPrem1(double aPrem1) {
        Prem1 = aPrem1;
    }
    public void setPrem1(String aPrem1) {
        if (aPrem1 != null && !aPrem1.equals("")) {
            Double tDouble = new Double(aPrem1);
            double d = tDouble.doubleValue();
            Prem1 = d;
        }
    }

    public double getPrem2() {
        return Prem2;
    }
    public void setPrem2(double aPrem2) {
        Prem2 = aPrem2;
    }
    public void setPrem2(String aPrem2) {
        if (aPrem2 != null && !aPrem2.equals("")) {
            Double tDouble = new Double(aPrem2);
            double d = tDouble.doubleValue();
            Prem2 = d;
        }
    }

    public double getPrem3() {
        return Prem3;
    }
    public void setPrem3(double aPrem3) {
        Prem3 = aPrem3;
    }
    public void setPrem3(String aPrem3) {
        if (aPrem3 != null && !aPrem3.equals("")) {
            Double tDouble = new Double(aPrem3);
            double d = tDouble.doubleValue();
            Prem3 = d;
        }
    }

    public double getAmnt1() {
        return Amnt1;
    }
    public void setAmnt1(double aAmnt1) {
        Amnt1 = aAmnt1;
    }
    public void setAmnt1(String aAmnt1) {
        if (aAmnt1 != null && !aAmnt1.equals("")) {
            Double tDouble = new Double(aAmnt1);
            double d = tDouble.doubleValue();
            Amnt1 = d;
        }
    }

    public double getAmnt2() {
        return Amnt2;
    }
    public void setAmnt2(double aAmnt2) {
        Amnt2 = aAmnt2;
    }
    public void setAmnt2(String aAmnt2) {
        if (aAmnt2 != null && !aAmnt2.equals("")) {
            Double tDouble = new Double(aAmnt2);
            double d = tDouble.doubleValue();
            Amnt2 = d;
        }
    }

    public double getAmnt3() {
        return Amnt3;
    }
    public void setAmnt3(double aAmnt3) {
        Amnt3 = aAmnt3;
    }
    public void setAmnt3(String aAmnt3) {
        if (aAmnt3 != null && !aAmnt3.equals("")) {
            Double tDouble = new Double(aAmnt3);
            double d = tDouble.doubleValue();
            Amnt3 = d;
        }
    }

    public double getAmnt() {
        return Amnt;
    }
    public void setAmnt(double aAmnt) {
        Amnt = aAmnt;
    }
    public void setAmnt(String aAmnt) {
        if (aAmnt != null && !aAmnt.equals("")) {
            Double tDouble = new Double(aAmnt);
            double d = tDouble.doubleValue();
            Amnt = d;
        }
    }

    public double getMult() {
        return Mult;
    }
    public void setMult(double aMult) {
        Mult = aMult;
    }
    public void setMult(String aMult) {
        if (aMult != null && !aMult.equals("")) {
            Double tDouble = new Double(aMult);
            double d = tDouble.doubleValue();
            Mult = d;
        }
    }

    public String getProdSetCode() {
        return ProdSetCode;
    }
    public void setProdSetCode(String aProdSetCode) {
        ProdSetCode = aProdSetCode;
    }
    public double getProdSetMult() {
        return ProdSetMult;
    }
    public void setProdSetMult(double aProdSetMult) {
        ProdSetMult = aProdSetMult;
    }
    public void setProdSetMult(String aProdSetMult) {
        if (aProdSetMult != null && !aProdSetMult.equals("")) {
            Double tDouble = new Double(aProdSetMult);
            double d = tDouble.doubleValue();
            ProdSetMult = d;
        }
    }

    public String getPayEndYearFlag() {
        return PayEndYearFlag;
    }
    public void setPayEndYearFlag(String aPayEndYearFlag) {
        PayEndYearFlag = aPayEndYearFlag;
    }
    public int getPayEndYear() {
        return PayEndYear;
    }
    public void setPayEndYear(int aPayEndYear) {
        PayEndYear = aPayEndYear;
    }
    public void setPayEndYear(String aPayEndYear) {
        if (aPayEndYear != null && !aPayEndYear.equals("")) {
            Integer tInteger = new Integer(aPayEndYear);
            int i = tInteger.intValue();
            PayEndYear = i;
        }
    }

    public String getInsuYearFlag() {
        return InsuYearFlag;
    }
    public void setInsuYearFlag(String aInsuYearFlag) {
        InsuYearFlag = aInsuYearFlag;
    }
    public int getInsuYear() {
        return InsuYear;
    }
    public void setInsuYear(int aInsuYear) {
        InsuYear = aInsuYear;
    }
    public void setInsuYear(String aInsuYear) {
        if (aInsuYear != null && !aInsuYear.equals("")) {
            Integer tInteger = new Integer(aInsuYear);
            int i = tInteger.intValue();
            InsuYear = i;
        }
    }

    public String getGetYearFlag() {
        return GetYearFlag;
    }
    public void setGetYearFlag(String aGetYearFlag) {
        GetYearFlag = aGetYearFlag;
    }
    public int getGetYear() {
        return GetYear;
    }
    public void setGetYear(int aGetYear) {
        GetYear = aGetYear;
    }
    public void setGetYear(String aGetYear) {
        if (aGetYear != null && !aGetYear.equals("")) {
            Integer tInteger = new Integer(aGetYear);
            int i = tInteger.intValue();
            GetYear = i;
        }
    }


    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("YPolID") ) {
            return 0;
        }
        if( strFieldName.equals("VerifyAppID") ) {
            return 1;
        }
        if( strFieldName.equals("ShardingID") ) {
            return 2;
        }
        if( strFieldName.equals("ContNo") ) {
            return 3;
        }
        if( strFieldName.equals("ApplyDate") ) {
            return 4;
        }
        if( strFieldName.equals("ApplyEndDate") ) {
            return 5;
        }
        if( strFieldName.equals("State") ) {
            return 6;
        }
        if( strFieldName.equals("RiskCode") ) {
            return 7;
        }
        if( strFieldName.equals("ManageCom") ) {
            return 8;
        }
        if( strFieldName.equals("AgentBankCode") ) {
            return 9;
        }
        if( strFieldName.equals("AgentCom") ) {
            return 10;
        }
        if( strFieldName.equals("BankAgent") ) {
            return 11;
        }
        if( strFieldName.equals("AgentName") ) {
            return 12;
        }
        if( strFieldName.equals("AgentPhone") ) {
            return 13;
        }
        if( strFieldName.equals("AgentCode") ) {
            return 14;
        }
        if( strFieldName.equals("AgentGroup") ) {
            return 15;
        }
        if( strFieldName.equals("SaleChnl") ) {
            return 16;
        }
        if( strFieldName.equals("Prem") ) {
            return 17;
        }
        if( strFieldName.equals("PayIntv") ) {
            return 18;
        }
        if( strFieldName.equals("PayYears") ) {
            return 19;
        }
        if( strFieldName.equals("StandbyFlag1") ) {
            return 20;
        }
        if( strFieldName.equals("StandbyFlag2") ) {
            return 21;
        }
        if( strFieldName.equals("StandbyFlag3") ) {
            return 22;
        }
        if( strFieldName.equals("Operator") ) {
            return 23;
        }
        if( strFieldName.equals("MakeDate") ) {
            return 24;
        }
        if( strFieldName.equals("MakeTime") ) {
            return 25;
        }
        if( strFieldName.equals("ModifyDate") ) {
            return 26;
        }
        if( strFieldName.equals("ModifyTime") ) {
            return 27;
        }
        if( strFieldName.equals("Prem1") ) {
            return 28;
        }
        if( strFieldName.equals("Prem2") ) {
            return 29;
        }
        if( strFieldName.equals("Prem3") ) {
            return 30;
        }
        if( strFieldName.equals("Amnt1") ) {
            return 31;
        }
        if( strFieldName.equals("Amnt2") ) {
            return 32;
        }
        if( strFieldName.equals("Amnt3") ) {
            return 33;
        }
        if( strFieldName.equals("Amnt") ) {
            return 34;
        }
        if( strFieldName.equals("Mult") ) {
            return 35;
        }
        if( strFieldName.equals("ProdSetCode") ) {
            return 36;
        }
        if( strFieldName.equals("ProdSetMult") ) {
            return 37;
        }
        if( strFieldName.equals("PayEndYearFlag") ) {
            return 38;
        }
        if( strFieldName.equals("PayEndYear") ) {
            return 39;
        }
        if( strFieldName.equals("InsuYearFlag") ) {
            return 40;
        }
        if( strFieldName.equals("InsuYear") ) {
            return 41;
        }
        if( strFieldName.equals("GetYearFlag") ) {
            return 42;
        }
        if( strFieldName.equals("GetYear") ) {
            return 43;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "YPolID";
                break;
            case 1:
                strFieldName = "VerifyAppID";
                break;
            case 2:
                strFieldName = "ShardingID";
                break;
            case 3:
                strFieldName = "ContNo";
                break;
            case 4:
                strFieldName = "ApplyDate";
                break;
            case 5:
                strFieldName = "ApplyEndDate";
                break;
            case 6:
                strFieldName = "State";
                break;
            case 7:
                strFieldName = "RiskCode";
                break;
            case 8:
                strFieldName = "ManageCom";
                break;
            case 9:
                strFieldName = "AgentBankCode";
                break;
            case 10:
                strFieldName = "AgentCom";
                break;
            case 11:
                strFieldName = "BankAgent";
                break;
            case 12:
                strFieldName = "AgentName";
                break;
            case 13:
                strFieldName = "AgentPhone";
                break;
            case 14:
                strFieldName = "AgentCode";
                break;
            case 15:
                strFieldName = "AgentGroup";
                break;
            case 16:
                strFieldName = "SaleChnl";
                break;
            case 17:
                strFieldName = "Prem";
                break;
            case 18:
                strFieldName = "PayIntv";
                break;
            case 19:
                strFieldName = "PayYears";
                break;
            case 20:
                strFieldName = "StandbyFlag1";
                break;
            case 21:
                strFieldName = "StandbyFlag2";
                break;
            case 22:
                strFieldName = "StandbyFlag3";
                break;
            case 23:
                strFieldName = "Operator";
                break;
            case 24:
                strFieldName = "MakeDate";
                break;
            case 25:
                strFieldName = "MakeTime";
                break;
            case 26:
                strFieldName = "ModifyDate";
                break;
            case 27:
                strFieldName = "ModifyTime";
                break;
            case 28:
                strFieldName = "Prem1";
                break;
            case 29:
                strFieldName = "Prem2";
                break;
            case 30:
                strFieldName = "Prem3";
                break;
            case 31:
                strFieldName = "Amnt1";
                break;
            case 32:
                strFieldName = "Amnt2";
                break;
            case 33:
                strFieldName = "Amnt3";
                break;
            case 34:
                strFieldName = "Amnt";
                break;
            case 35:
                strFieldName = "Mult";
                break;
            case 36:
                strFieldName = "ProdSetCode";
                break;
            case 37:
                strFieldName = "ProdSetMult";
                break;
            case 38:
                strFieldName = "PayEndYearFlag";
                break;
            case 39:
                strFieldName = "PayEndYear";
                break;
            case 40:
                strFieldName = "InsuYearFlag";
                break;
            case 41:
                strFieldName = "InsuYear";
                break;
            case 42:
                strFieldName = "GetYearFlag";
                break;
            case 43:
                strFieldName = "GetYear";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "YPOLID":
                return Schema.TYPE_LONG;
            case "VERIFYAPPID":
                return Schema.TYPE_LONG;
            case "SHARDINGID":
                return Schema.TYPE_STRING;
            case "CONTNO":
                return Schema.TYPE_STRING;
            case "APPLYDATE":
                return Schema.TYPE_STRING;
            case "APPLYENDDATE":
                return Schema.TYPE_STRING;
            case "STATE":
                return Schema.TYPE_STRING;
            case "RISKCODE":
                return Schema.TYPE_STRING;
            case "MANAGECOM":
                return Schema.TYPE_STRING;
            case "AGENTBANKCODE":
                return Schema.TYPE_STRING;
            case "AGENTCOM":
                return Schema.TYPE_STRING;
            case "BANKAGENT":
                return Schema.TYPE_STRING;
            case "AGENTNAME":
                return Schema.TYPE_STRING;
            case "AGENTPHONE":
                return Schema.TYPE_STRING;
            case "AGENTCODE":
                return Schema.TYPE_STRING;
            case "AGENTGROUP":
                return Schema.TYPE_STRING;
            case "SALECHNL":
                return Schema.TYPE_STRING;
            case "PREM":
                return Schema.TYPE_DOUBLE;
            case "PAYINTV":
                return Schema.TYPE_DOUBLE;
            case "PAYYEARS":
                return Schema.TYPE_DOUBLE;
            case "STANDBYFLAG1":
                return Schema.TYPE_STRING;
            case "STANDBYFLAG2":
                return Schema.TYPE_STRING;
            case "STANDBYFLAG3":
                return Schema.TYPE_STRING;
            case "OPERATOR":
                return Schema.TYPE_STRING;
            case "MAKEDATE":
                return Schema.TYPE_STRING;
            case "MAKETIME":
                return Schema.TYPE_STRING;
            case "MODIFYDATE":
                return Schema.TYPE_STRING;
            case "MODIFYTIME":
                return Schema.TYPE_STRING;
            case "PREM1":
                return Schema.TYPE_DOUBLE;
            case "PREM2":
                return Schema.TYPE_DOUBLE;
            case "PREM3":
                return Schema.TYPE_DOUBLE;
            case "AMNT1":
                return Schema.TYPE_DOUBLE;
            case "AMNT2":
                return Schema.TYPE_DOUBLE;
            case "AMNT3":
                return Schema.TYPE_DOUBLE;
            case "AMNT":
                return Schema.TYPE_DOUBLE;
            case "MULT":
                return Schema.TYPE_DOUBLE;
            case "PRODSETCODE":
                return Schema.TYPE_STRING;
            case "PRODSETMULT":
                return Schema.TYPE_DOUBLE;
            case "PAYENDYEARFLAG":
                return Schema.TYPE_STRING;
            case "PAYENDYEAR":
                return Schema.TYPE_INT;
            case "INSUYEARFLAG":
                return Schema.TYPE_STRING;
            case "INSUYEAR":
                return Schema.TYPE_INT;
            case "GETYEARFLAG":
                return Schema.TYPE_STRING;
            case "GETYEAR":
                return Schema.TYPE_INT;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_LONG;
            case 1:
                return Schema.TYPE_LONG;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_STRING;
            case 6:
                return Schema.TYPE_STRING;
            case 7:
                return Schema.TYPE_STRING;
            case 8:
                return Schema.TYPE_STRING;
            case 9:
                return Schema.TYPE_STRING;
            case 10:
                return Schema.TYPE_STRING;
            case 11:
                return Schema.TYPE_STRING;
            case 12:
                return Schema.TYPE_STRING;
            case 13:
                return Schema.TYPE_STRING;
            case 14:
                return Schema.TYPE_STRING;
            case 15:
                return Schema.TYPE_STRING;
            case 16:
                return Schema.TYPE_STRING;
            case 17:
                return Schema.TYPE_DOUBLE;
            case 18:
                return Schema.TYPE_DOUBLE;
            case 19:
                return Schema.TYPE_DOUBLE;
            case 20:
                return Schema.TYPE_STRING;
            case 21:
                return Schema.TYPE_STRING;
            case 22:
                return Schema.TYPE_STRING;
            case 23:
                return Schema.TYPE_STRING;
            case 24:
                return Schema.TYPE_STRING;
            case 25:
                return Schema.TYPE_STRING;
            case 26:
                return Schema.TYPE_STRING;
            case 27:
                return Schema.TYPE_STRING;
            case 28:
                return Schema.TYPE_DOUBLE;
            case 29:
                return Schema.TYPE_DOUBLE;
            case 30:
                return Schema.TYPE_DOUBLE;
            case 31:
                return Schema.TYPE_DOUBLE;
            case 32:
                return Schema.TYPE_DOUBLE;
            case 33:
                return Schema.TYPE_DOUBLE;
            case 34:
                return Schema.TYPE_DOUBLE;
            case 35:
                return Schema.TYPE_DOUBLE;
            case 36:
                return Schema.TYPE_STRING;
            case 37:
                return Schema.TYPE_DOUBLE;
            case 38:
                return Schema.TYPE_STRING;
            case 39:
                return Schema.TYPE_INT;
            case 40:
                return Schema.TYPE_STRING;
            case 41:
                return Schema.TYPE_INT;
            case 42:
                return Schema.TYPE_STRING;
            case 43:
                return Schema.TYPE_INT;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("YPolID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(YPolID));
        }
        if (FCode.equalsIgnoreCase("VerifyAppID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(VerifyAppID));
        }
        if (FCode.equalsIgnoreCase("ShardingID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ShardingID));
        }
        if (FCode.equalsIgnoreCase("ContNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ContNo));
        }
        if (FCode.equalsIgnoreCase("ApplyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ApplyDate));
        }
        if (FCode.equalsIgnoreCase("ApplyEndDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ApplyEndDate));
        }
        if (FCode.equalsIgnoreCase("State")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(State));
        }
        if (FCode.equalsIgnoreCase("RiskCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RiskCode));
        }
        if (FCode.equalsIgnoreCase("ManageCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
        }
        if (FCode.equalsIgnoreCase("AgentBankCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentBankCode));
        }
        if (FCode.equalsIgnoreCase("AgentCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentCom));
        }
        if (FCode.equalsIgnoreCase("BankAgent")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BankAgent));
        }
        if (FCode.equalsIgnoreCase("AgentName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentName));
        }
        if (FCode.equalsIgnoreCase("AgentPhone")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentPhone));
        }
        if (FCode.equalsIgnoreCase("AgentCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentCode));
        }
        if (FCode.equalsIgnoreCase("AgentGroup")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentGroup));
        }
        if (FCode.equalsIgnoreCase("SaleChnl")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SaleChnl));
        }
        if (FCode.equalsIgnoreCase("Prem")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Prem));
        }
        if (FCode.equalsIgnoreCase("PayIntv")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PayIntv));
        }
        if (FCode.equalsIgnoreCase("PayYears")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PayYears));
        }
        if (FCode.equalsIgnoreCase("StandbyFlag1")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StandbyFlag1));
        }
        if (FCode.equalsIgnoreCase("StandbyFlag2")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StandbyFlag2));
        }
        if (FCode.equalsIgnoreCase("StandbyFlag3")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StandbyFlag3));
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeDate));
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyDate));
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (FCode.equalsIgnoreCase("Prem1")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Prem1));
        }
        if (FCode.equalsIgnoreCase("Prem2")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Prem2));
        }
        if (FCode.equalsIgnoreCase("Prem3")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Prem3));
        }
        if (FCode.equalsIgnoreCase("Amnt1")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Amnt1));
        }
        if (FCode.equalsIgnoreCase("Amnt2")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Amnt2));
        }
        if (FCode.equalsIgnoreCase("Amnt3")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Amnt3));
        }
        if (FCode.equalsIgnoreCase("Amnt")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Amnt));
        }
        if (FCode.equalsIgnoreCase("Mult")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Mult));
        }
        if (FCode.equalsIgnoreCase("ProdSetCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ProdSetCode));
        }
        if (FCode.equalsIgnoreCase("ProdSetMult")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ProdSetMult));
        }
        if (FCode.equalsIgnoreCase("PayEndYearFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PayEndYearFlag));
        }
        if (FCode.equalsIgnoreCase("PayEndYear")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PayEndYear));
        }
        if (FCode.equalsIgnoreCase("InsuYearFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InsuYearFlag));
        }
        if (FCode.equalsIgnoreCase("InsuYear")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InsuYear));
        }
        if (FCode.equalsIgnoreCase("GetYearFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GetYearFlag));
        }
        if (FCode.equalsIgnoreCase("GetYear")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GetYear));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = String.valueOf(YPolID);
                break;
            case 1:
                strFieldValue = String.valueOf(VerifyAppID);
                break;
            case 2:
                strFieldValue = String.valueOf(ShardingID);
                break;
            case 3:
                strFieldValue = String.valueOf(ContNo);
                break;
            case 4:
                strFieldValue = String.valueOf(ApplyDate);
                break;
            case 5:
                strFieldValue = String.valueOf(ApplyEndDate);
                break;
            case 6:
                strFieldValue = String.valueOf(State);
                break;
            case 7:
                strFieldValue = String.valueOf(RiskCode);
                break;
            case 8:
                strFieldValue = String.valueOf(ManageCom);
                break;
            case 9:
                strFieldValue = String.valueOf(AgentBankCode);
                break;
            case 10:
                strFieldValue = String.valueOf(AgentCom);
                break;
            case 11:
                strFieldValue = String.valueOf(BankAgent);
                break;
            case 12:
                strFieldValue = String.valueOf(AgentName);
                break;
            case 13:
                strFieldValue = String.valueOf(AgentPhone);
                break;
            case 14:
                strFieldValue = String.valueOf(AgentCode);
                break;
            case 15:
                strFieldValue = String.valueOf(AgentGroup);
                break;
            case 16:
                strFieldValue = String.valueOf(SaleChnl);
                break;
            case 17:
                strFieldValue = String.valueOf(Prem);
                break;
            case 18:
                strFieldValue = String.valueOf(PayIntv);
                break;
            case 19:
                strFieldValue = String.valueOf(PayYears);
                break;
            case 20:
                strFieldValue = String.valueOf(StandbyFlag1);
                break;
            case 21:
                strFieldValue = String.valueOf(StandbyFlag2);
                break;
            case 22:
                strFieldValue = String.valueOf(StandbyFlag3);
                break;
            case 23:
                strFieldValue = String.valueOf(Operator);
                break;
            case 24:
                strFieldValue = String.valueOf(MakeDate);
                break;
            case 25:
                strFieldValue = String.valueOf(MakeTime);
                break;
            case 26:
                strFieldValue = String.valueOf(ModifyDate);
                break;
            case 27:
                strFieldValue = String.valueOf(ModifyTime);
                break;
            case 28:
                strFieldValue = String.valueOf(Prem1);
                break;
            case 29:
                strFieldValue = String.valueOf(Prem2);
                break;
            case 30:
                strFieldValue = String.valueOf(Prem3);
                break;
            case 31:
                strFieldValue = String.valueOf(Amnt1);
                break;
            case 32:
                strFieldValue = String.valueOf(Amnt2);
                break;
            case 33:
                strFieldValue = String.valueOf(Amnt3);
                break;
            case 34:
                strFieldValue = String.valueOf(Amnt);
                break;
            case 35:
                strFieldValue = String.valueOf(Mult);
                break;
            case 36:
                strFieldValue = String.valueOf(ProdSetCode);
                break;
            case 37:
                strFieldValue = String.valueOf(ProdSetMult);
                break;
            case 38:
                strFieldValue = String.valueOf(PayEndYearFlag);
                break;
            case 39:
                strFieldValue = String.valueOf(PayEndYear);
                break;
            case 40:
                strFieldValue = String.valueOf(InsuYearFlag);
                break;
            case 41:
                strFieldValue = String.valueOf(InsuYear);
                break;
            case 42:
                strFieldValue = String.valueOf(GetYearFlag);
                break;
            case 43:
                strFieldValue = String.valueOf(GetYear);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("YPolID")) {
            if( FValue != null && !FValue.equals("")) {
                YPolID = new Long(FValue).longValue();
            }
        }
        if (FCode.equalsIgnoreCase("VerifyAppID")) {
            if( FValue != null && !FValue.equals("")) {
                VerifyAppID = new Long(FValue).longValue();
            }
        }
        if (FCode.equalsIgnoreCase("ShardingID")) {
            if( FValue != null && !FValue.equals(""))
            {
                ShardingID = FValue.trim();
            }
            else
                ShardingID = null;
        }
        if (FCode.equalsIgnoreCase("ContNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                ContNo = FValue.trim();
            }
            else
                ContNo = null;
        }
        if (FCode.equalsIgnoreCase("ApplyDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                ApplyDate = FValue.trim();
            }
            else
                ApplyDate = null;
        }
        if (FCode.equalsIgnoreCase("ApplyEndDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                ApplyEndDate = FValue.trim();
            }
            else
                ApplyEndDate = null;
        }
        if (FCode.equalsIgnoreCase("State")) {
            if( FValue != null && !FValue.equals(""))
            {
                State = FValue.trim();
            }
            else
                State = null;
        }
        if (FCode.equalsIgnoreCase("RiskCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                RiskCode = FValue.trim();
            }
            else
                RiskCode = null;
        }
        if (FCode.equalsIgnoreCase("ManageCom")) {
            if( FValue != null && !FValue.equals(""))
            {
                ManageCom = FValue.trim();
            }
            else
                ManageCom = null;
        }
        if (FCode.equalsIgnoreCase("AgentBankCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                AgentBankCode = FValue.trim();
            }
            else
                AgentBankCode = null;
        }
        if (FCode.equalsIgnoreCase("AgentCom")) {
            if( FValue != null && !FValue.equals(""))
            {
                AgentCom = FValue.trim();
            }
            else
                AgentCom = null;
        }
        if (FCode.equalsIgnoreCase("BankAgent")) {
            if( FValue != null && !FValue.equals(""))
            {
                BankAgent = FValue.trim();
            }
            else
                BankAgent = null;
        }
        if (FCode.equalsIgnoreCase("AgentName")) {
            if( FValue != null && !FValue.equals(""))
            {
                AgentName = FValue.trim();
            }
            else
                AgentName = null;
        }
        if (FCode.equalsIgnoreCase("AgentPhone")) {
            if( FValue != null && !FValue.equals(""))
            {
                AgentPhone = FValue.trim();
            }
            else
                AgentPhone = null;
        }
        if (FCode.equalsIgnoreCase("AgentCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                AgentCode = FValue.trim();
            }
            else
                AgentCode = null;
        }
        if (FCode.equalsIgnoreCase("AgentGroup")) {
            if( FValue != null && !FValue.equals(""))
            {
                AgentGroup = FValue.trim();
            }
            else
                AgentGroup = null;
        }
        if (FCode.equalsIgnoreCase("SaleChnl")) {
            if( FValue != null && !FValue.equals(""))
            {
                SaleChnl = FValue.trim();
            }
            else
                SaleChnl = null;
        }
        if (FCode.equalsIgnoreCase("Prem")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                Prem = d;
            }
        }
        if (FCode.equalsIgnoreCase("PayIntv")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                PayIntv = d;
            }
        }
        if (FCode.equalsIgnoreCase("PayYears")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                PayYears = d;
            }
        }
        if (FCode.equalsIgnoreCase("StandbyFlag1")) {
            if( FValue != null && !FValue.equals(""))
            {
                StandbyFlag1 = FValue.trim();
            }
            else
                StandbyFlag1 = null;
        }
        if (FCode.equalsIgnoreCase("StandbyFlag2")) {
            if( FValue != null && !FValue.equals(""))
            {
                StandbyFlag2 = FValue.trim();
            }
            else
                StandbyFlag2 = null;
        }
        if (FCode.equalsIgnoreCase("StandbyFlag3")) {
            if( FValue != null && !FValue.equals(""))
            {
                StandbyFlag3 = FValue.trim();
            }
            else
                StandbyFlag3 = null;
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            if( FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
                Operator = null;
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeDate = FValue.trim();
            }
            else
                MakeDate = null;
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
                MakeTime = null;
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyDate = FValue.trim();
            }
            else
                ModifyDate = null;
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
                ModifyTime = null;
        }
        if (FCode.equalsIgnoreCase("Prem1")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                Prem1 = d;
            }
        }
        if (FCode.equalsIgnoreCase("Prem2")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                Prem2 = d;
            }
        }
        if (FCode.equalsIgnoreCase("Prem3")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                Prem3 = d;
            }
        }
        if (FCode.equalsIgnoreCase("Amnt1")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                Amnt1 = d;
            }
        }
        if (FCode.equalsIgnoreCase("Amnt2")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                Amnt2 = d;
            }
        }
        if (FCode.equalsIgnoreCase("Amnt3")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                Amnt3 = d;
            }
        }
        if (FCode.equalsIgnoreCase("Amnt")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                Amnt = d;
            }
        }
        if (FCode.equalsIgnoreCase("Mult")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                Mult = d;
            }
        }
        if (FCode.equalsIgnoreCase("ProdSetCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                ProdSetCode = FValue.trim();
            }
            else
                ProdSetCode = null;
        }
        if (FCode.equalsIgnoreCase("ProdSetMult")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                ProdSetMult = d;
            }
        }
        if (FCode.equalsIgnoreCase("PayEndYearFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                PayEndYearFlag = FValue.trim();
            }
            else
                PayEndYearFlag = null;
        }
        if (FCode.equalsIgnoreCase("PayEndYear")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                PayEndYear = i;
            }
        }
        if (FCode.equalsIgnoreCase("InsuYearFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                InsuYearFlag = FValue.trim();
            }
            else
                InsuYearFlag = null;
        }
        if (FCode.equalsIgnoreCase("InsuYear")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                InsuYear = i;
            }
        }
        if (FCode.equalsIgnoreCase("GetYearFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                GetYearFlag = FValue.trim();
            }
            else
                GetYearFlag = null;
        }
        if (FCode.equalsIgnoreCase("GetYear")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                GetYear = i;
            }
        }
        return true;
    }


    public String toString() {
    return "LYPolPojo [" +
            "YPolID="+YPolID +
            ", VerifyAppID="+VerifyAppID +
            ", ShardingID="+ShardingID +
            ", ContNo="+ContNo +
            ", ApplyDate="+ApplyDate +
            ", ApplyEndDate="+ApplyEndDate +
            ", State="+State +
            ", RiskCode="+RiskCode +
            ", ManageCom="+ManageCom +
            ", AgentBankCode="+AgentBankCode +
            ", AgentCom="+AgentCom +
            ", BankAgent="+BankAgent +
            ", AgentName="+AgentName +
            ", AgentPhone="+AgentPhone +
            ", AgentCode="+AgentCode +
            ", AgentGroup="+AgentGroup +
            ", SaleChnl="+SaleChnl +
            ", Prem="+Prem +
            ", PayIntv="+PayIntv +
            ", PayYears="+PayYears +
            ", StandbyFlag1="+StandbyFlag1 +
            ", StandbyFlag2="+StandbyFlag2 +
            ", StandbyFlag3="+StandbyFlag3 +
            ", Operator="+Operator +
            ", MakeDate="+MakeDate +
            ", MakeTime="+MakeTime +
            ", ModifyDate="+ModifyDate +
            ", ModifyTime="+ModifyTime +
            ", Prem1="+Prem1 +
            ", Prem2="+Prem2 +
            ", Prem3="+Prem3 +
            ", Amnt1="+Amnt1 +
            ", Amnt2="+Amnt2 +
            ", Amnt3="+Amnt3 +
            ", Amnt="+Amnt +
            ", Mult="+Mult +
            ", ProdSetCode="+ProdSetCode +
            ", ProdSetMult="+ProdSetMult +
            ", PayEndYearFlag="+PayEndYearFlag +
            ", PayEndYear="+PayEndYear +
            ", InsuYearFlag="+InsuYearFlag +
            ", InsuYear="+InsuYear +
            ", GetYearFlag="+GetYearFlag +
            ", GetYear="+GetYear +"]";
    }
}
