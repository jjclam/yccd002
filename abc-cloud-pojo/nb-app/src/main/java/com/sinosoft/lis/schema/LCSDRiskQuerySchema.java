/**
 * Copyright (c) 2017 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import com.sinosoft.lis.db.LCSDRiskQueryDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.*;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

/**
 * <p>ClassName: LCSDRiskQuerySchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2017</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2017-10-21
 */
public class LCSDRiskQuerySchema implements Schema, Cloneable {
    // @Field
    /** 序列号 */
    private String SerialNo;
    /** 客户号码 */
    private String CustomerNo;
    /** 业务类型 */
    private String BussType;
    /** 业务号码 */
    private String BussNo;
    /** 保单类型 */
    private String ConType;
    /** 姓名 */
    private String Name;
    /** 性别 */
    private String Sex;
    /** 出生日期 */
    private Date Birthday;
    /** 证件类型 */
    private String IdType;
    /** 证件号码 */
    private String IdNo;
    /** 是否符合风险特征 */
    private String IsRisk;
    /** 出险次数 */
    private int LossTimes;
    /** 累计赔付金额 */
    private double SumClaimAmount;
    /** 拒赔次数/拒保次数 */
    private int RefuseTimes;
    /** 出险公司代码 */
    private String CompanyCode;
    /** 出险日期 */
    private Date LossDate;
    /** 出险结果 */
    private String LossResult;
    /** 总赔款金额 */
    private double ClaimAmount;
    /** 承保公司数量 */
    private int CompanyQuantity;
    /** 投保保单数量 */
    private int PolicyQuantity;
    /** 累计最高风险保额 */
    private double SumAmount;
    /** 是否高风险人员 */
    private String IsRiskPerson;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 最后一次修改日期 */
    private Date ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime;

    public static final int FIELDNUM = 26;    // 数据库表的字段个数

    private static String[] PK;                // 主键

    private FDate fDate = new FDate();        // 处理日期

    public CErrors mErrors;            // 错误信息

    // @Constructor
    public LCSDRiskQuerySchema() {
        mErrors = new CErrors();

        String[] pk = new String[1];
        pk[0] = "SerialNo";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException {
        LCSDRiskQuerySchema cloned = (LCSDRiskQuerySchema)super.clone();
        cloned.fDate = (FDate) fDate.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK() {
        return PK;
    }

    public String getSerialNo() {
        return SerialNo;
    }
    public void setSerialNo(String aSerialNo) {
        SerialNo = aSerialNo;
    }
    public String getCustomerNo() {
        return CustomerNo;
    }
    public void setCustomerNo(String aCustomerNo) {
        CustomerNo = aCustomerNo;
    }
    public String getBussType() {
        return BussType;
    }
    public void setBussType(String aBussType) {
        BussType = aBussType;
    }
    public String getBussNo() {
        return BussNo;
    }
    public void setBussNo(String aBussNo) {
        BussNo = aBussNo;
    }
    public String getConType() {
        return ConType;
    }
    public void setConType(String aConType) {
        ConType = aConType;
    }
    public String getName() {
        return Name;
    }
    public void setName(String aName) {
        Name = aName;
    }
    public String getSex() {
        return Sex;
    }
    public void setSex(String aSex) {
        Sex = aSex;
    }
    public String getBirthday() {
        if(Birthday != null) {
            return fDate.getString(Birthday);
        } else {
            return null;
        }
    }
    public void setBirthday(Date aBirthday) {
        Birthday = aBirthday;
    }
    public void setBirthday(String aBirthday) {
        if (aBirthday != null && !aBirthday.equals("")) {
            Birthday = fDate.getDate(aBirthday);
        } else
            Birthday = null;
    }

    public String getIdType() {
        return IdType;
    }
    public void setIdType(String aIdType) {
        IdType = aIdType;
    }
    public String getIdNo() {
        return IdNo;
    }
    public void setIdNo(String aIdNo) {
        IdNo = aIdNo;
    }
    public String getIsRisk() {
        return IsRisk;
    }
    public void setIsRisk(String aIsRisk) {
        IsRisk = aIsRisk;
    }
    public int getLossTimes() {
        return LossTimes;
    }
    public void setLossTimes(int aLossTimes) {
        LossTimes = aLossTimes;
    }
    public void setLossTimes(String aLossTimes) {
        if (aLossTimes != null && !aLossTimes.equals("")) {
            Integer tInteger = new Integer(aLossTimes);
            int i = tInteger.intValue();
            LossTimes = i;
        }
    }

    public double getSumClaimAmount() {
        return SumClaimAmount;
    }
    public void setSumClaimAmount(double aSumClaimAmount) {
        SumClaimAmount = aSumClaimAmount;
    }
    public void setSumClaimAmount(String aSumClaimAmount) {
        if (aSumClaimAmount != null && !aSumClaimAmount.equals("")) {
            Double tDouble = new Double(aSumClaimAmount);
            double d = tDouble.doubleValue();
            SumClaimAmount = d;
        }
    }

    public int getRefuseTimes() {
        return RefuseTimes;
    }
    public void setRefuseTimes(int aRefuseTimes) {
        RefuseTimes = aRefuseTimes;
    }
    public void setRefuseTimes(String aRefuseTimes) {
        if (aRefuseTimes != null && !aRefuseTimes.equals("")) {
            Integer tInteger = new Integer(aRefuseTimes);
            int i = tInteger.intValue();
            RefuseTimes = i;
        }
    }

    public String getCompanyCode() {
        return CompanyCode;
    }
    public void setCompanyCode(String aCompanyCode) {
        CompanyCode = aCompanyCode;
    }
    public String getLossDate() {
        if(LossDate != null) {
            return fDate.getString(LossDate);
        } else {
            return null;
        }
    }
    public void setLossDate(Date aLossDate) {
        LossDate = aLossDate;
    }
    public void setLossDate(String aLossDate) {
        if (aLossDate != null && !aLossDate.equals("")) {
            LossDate = fDate.getDate(aLossDate);
        } else
            LossDate = null;
    }

    public String getLossResult() {
        return LossResult;
    }
    public void setLossResult(String aLossResult) {
        LossResult = aLossResult;
    }
    public double getClaimAmount() {
        return ClaimAmount;
    }
    public void setClaimAmount(double aClaimAmount) {
        ClaimAmount = aClaimAmount;
    }
    public void setClaimAmount(String aClaimAmount) {
        if (aClaimAmount != null && !aClaimAmount.equals("")) {
            Double tDouble = new Double(aClaimAmount);
            double d = tDouble.doubleValue();
            ClaimAmount = d;
        }
    }

    public int getCompanyQuantity() {
        return CompanyQuantity;
    }
    public void setCompanyQuantity(int aCompanyQuantity) {
        CompanyQuantity = aCompanyQuantity;
    }
    public void setCompanyQuantity(String aCompanyQuantity) {
        if (aCompanyQuantity != null && !aCompanyQuantity.equals("")) {
            Integer tInteger = new Integer(aCompanyQuantity);
            int i = tInteger.intValue();
            CompanyQuantity = i;
        }
    }

    public int getPolicyQuantity() {
        return PolicyQuantity;
    }
    public void setPolicyQuantity(int aPolicyQuantity) {
        PolicyQuantity = aPolicyQuantity;
    }
    public void setPolicyQuantity(String aPolicyQuantity) {
        if (aPolicyQuantity != null && !aPolicyQuantity.equals("")) {
            Integer tInteger = new Integer(aPolicyQuantity);
            int i = tInteger.intValue();
            PolicyQuantity = i;
        }
    }

    public double getSumAmount() {
        return SumAmount;
    }
    public void setSumAmount(double aSumAmount) {
        SumAmount = aSumAmount;
    }
    public void setSumAmount(String aSumAmount) {
        if (aSumAmount != null && !aSumAmount.equals("")) {
            Double tDouble = new Double(aSumAmount);
            double d = tDouble.doubleValue();
            SumAmount = d;
        }
    }

    public String getIsRiskPerson() {
        return IsRiskPerson;
    }
    public void setIsRiskPerson(String aIsRiskPerson) {
        IsRiskPerson = aIsRiskPerson;
    }
    public String getMakeDate() {
        if(MakeDate != null) {
            return fDate.getString(MakeDate);
        } else {
            return null;
        }
    }
    public void setMakeDate(Date aMakeDate) {
        MakeDate = aMakeDate;
    }
    public void setMakeDate(String aMakeDate) {
        if (aMakeDate != null && !aMakeDate.equals("")) {
            MakeDate = fDate.getDate(aMakeDate);
        } else
            MakeDate = null;
    }

    public String getMakeTime() {
        return MakeTime;
    }
    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }
    public String getModifyDate() {
        if(ModifyDate != null) {
            return fDate.getString(ModifyDate);
        } else {
            return null;
        }
    }
    public void setModifyDate(Date aModifyDate) {
        ModifyDate = aModifyDate;
    }
    public void setModifyDate(String aModifyDate) {
        if (aModifyDate != null && !aModifyDate.equals("")) {
            ModifyDate = fDate.getDate(aModifyDate);
        } else
            ModifyDate = null;
    }

    public String getModifyTime() {
        return ModifyTime;
    }
    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }

    /**
    * 使用另外一个 LCSDRiskQuerySchema 对象给 Schema 赋值
    * @param: aLCSDRiskQuerySchema LCSDRiskQuerySchema
    **/
    public void setSchema(LCSDRiskQuerySchema aLCSDRiskQuerySchema) {
        this.SerialNo = aLCSDRiskQuerySchema.getSerialNo();
        this.CustomerNo = aLCSDRiskQuerySchema.getCustomerNo();
        this.BussType = aLCSDRiskQuerySchema.getBussType();
        this.BussNo = aLCSDRiskQuerySchema.getBussNo();
        this.ConType = aLCSDRiskQuerySchema.getConType();
        this.Name = aLCSDRiskQuerySchema.getName();
        this.Sex = aLCSDRiskQuerySchema.getSex();
        this.Birthday = fDate.getDate( aLCSDRiskQuerySchema.getBirthday());
        this.IdType = aLCSDRiskQuerySchema.getIdType();
        this.IdNo = aLCSDRiskQuerySchema.getIdNo();
        this.IsRisk = aLCSDRiskQuerySchema.getIsRisk();
        this.LossTimes = aLCSDRiskQuerySchema.getLossTimes();
        this.SumClaimAmount = aLCSDRiskQuerySchema.getSumClaimAmount();
        this.RefuseTimes = aLCSDRiskQuerySchema.getRefuseTimes();
        this.CompanyCode = aLCSDRiskQuerySchema.getCompanyCode();
        this.LossDate = fDate.getDate( aLCSDRiskQuerySchema.getLossDate());
        this.LossResult = aLCSDRiskQuerySchema.getLossResult();
        this.ClaimAmount = aLCSDRiskQuerySchema.getClaimAmount();
        this.CompanyQuantity = aLCSDRiskQuerySchema.getCompanyQuantity();
        this.PolicyQuantity = aLCSDRiskQuerySchema.getPolicyQuantity();
        this.SumAmount = aLCSDRiskQuerySchema.getSumAmount();
        this.IsRiskPerson = aLCSDRiskQuerySchema.getIsRiskPerson();
        this.MakeDate = fDate.getDate( aLCSDRiskQuerySchema.getMakeDate());
        this.MakeTime = aLCSDRiskQuerySchema.getMakeTime();
        this.ModifyDate = fDate.getDate( aLCSDRiskQuerySchema.getModifyDate());
        this.ModifyTime = aLCSDRiskQuerySchema.getModifyTime();
    }

    /**
    * 使用 ResultSet 中的第 i 行给 Schema 赋值
    * @param: rs ResultSet
    * @param: i int
    * @return: boolean
    **/
    public boolean setSchema(ResultSet rs,int i) {
        try {
            //rs.absolute(i);        // 非滚动游标
            if( rs.getString("SerialNo") == null )
                this.SerialNo = null;
            else
                this.SerialNo = rs.getString("SerialNo").trim();

            if( rs.getString("CustomerNo") == null )
                this.CustomerNo = null;
            else
                this.CustomerNo = rs.getString("CustomerNo").trim();

            if( rs.getString("BussType") == null )
                this.BussType = null;
            else
                this.BussType = rs.getString("BussType").trim();

            if( rs.getString("BussNo") == null )
                this.BussNo = null;
            else
                this.BussNo = rs.getString("BussNo").trim();

            if( rs.getString("ConType") == null )
                this.ConType = null;
            else
                this.ConType = rs.getString("ConType").trim();

            if( rs.getString("Name") == null )
                this.Name = null;
            else
                this.Name = rs.getString("Name").trim();

            if( rs.getString("Sex") == null )
                this.Sex = null;
            else
                this.Sex = rs.getString("Sex").trim();

            this.Birthday = rs.getDate("Birthday");
            if( rs.getString("IdType") == null )
                this.IdType = null;
            else
                this.IdType = rs.getString("IdType").trim();

            if( rs.getString("IdNo") == null )
                this.IdNo = null;
            else
                this.IdNo = rs.getString("IdNo").trim();

            if( rs.getString("IsRisk") == null )
                this.IsRisk = null;
            else
                this.IsRisk = rs.getString("IsRisk").trim();

            this.LossTimes = rs.getInt("LossTimes");
            this.SumClaimAmount = rs.getDouble("SumClaimAmount");
            this.RefuseTimes = rs.getInt("RefuseTimes");
            if( rs.getString("CompanyCode") == null )
                this.CompanyCode = null;
            else
                this.CompanyCode = rs.getString("CompanyCode").trim();

            this.LossDate = rs.getDate("LossDate");
            if( rs.getString("LossResult") == null )
                this.LossResult = null;
            else
                this.LossResult = rs.getString("LossResult").trim();

            this.ClaimAmount = rs.getDouble("ClaimAmount");
            this.CompanyQuantity = rs.getInt("CompanyQuantity");
            this.PolicyQuantity = rs.getInt("PolicyQuantity");
            this.SumAmount = rs.getDouble("SumAmount");
            if( rs.getString("IsRiskPerson") == null )
                this.IsRiskPerson = null;
            else
                this.IsRiskPerson = rs.getString("IsRiskPerson").trim();

            this.MakeDate = rs.getDate("MakeDate");
            if( rs.getString("MakeTime") == null )
                this.MakeTime = null;
            else
                this.MakeTime = rs.getString("MakeTime").trim();

            this.ModifyDate = rs.getDate("ModifyDate");
            if( rs.getString("ModifyTime") == null )
                this.ModifyTime = null;
            else
                this.ModifyTime = rs.getString("ModifyTime").trim();

        }
        catch(SQLException sqle) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LCSDRiskQuerySchema";
            tError.functionName = "setSchema";
            tError.errorMessage = ExceptionUtils.exceptionToString(sqle);
            this.mErrors .addOneError(tError);
            return false;
        }
        return true;
    }

    public LCSDRiskQuerySchema getSchema() {
        LCSDRiskQuerySchema aLCSDRiskQuerySchema = new LCSDRiskQuerySchema();
        aLCSDRiskQuerySchema.setSchema(this);
        return aLCSDRiskQuerySchema;
    }

    public LCSDRiskQueryDB getDB() {
        LCSDRiskQueryDB aDBOper = new LCSDRiskQueryDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
    * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLCSDRiskQuery描述/A>表字段
    * @return: String 返回打包后字符串
    **/
    public String encode() {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(SerialNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(CustomerNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(BussType)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(BussNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ConType)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Name)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Sex)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( Birthday ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(IdType)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(IdNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(IsRisk)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(LossTimes));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(SumClaimAmount));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(RefuseTimes));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(CompanyCode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( LossDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(LossResult)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(ClaimAmount));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(CompanyQuantity));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(PolicyQuantity));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(SumAmount));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(IsRiskPerson)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ModifyTime));
        return strReturn.toString();
    }

    /**
    * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLCSDRiskQuery>历史记账凭证主表信息</A>表字段
    * @param: strMessage String 包含一条纪录数据的字符串
    * @return: boolean
    **/
    public boolean decode(String strMessage) {
        try {
            SerialNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
            CustomerNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
            BussType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
            BussNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
            ConType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
            Name = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
            Sex = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
            Birthday = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER));
            IdType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
            IdNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
            IsRisk = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
            LossTimes = new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,12, SysConst.PACKAGESPILTER))).intValue();
            SumClaimAmount = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,13, SysConst.PACKAGESPILTER))).doubleValue();
            RefuseTimes = new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,14, SysConst.PACKAGESPILTER))).intValue();
            CompanyCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15, SysConst.PACKAGESPILTER );
            LossDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16, SysConst.PACKAGESPILTER));
            LossResult = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17, SysConst.PACKAGESPILTER );
            ClaimAmount = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,18, SysConst.PACKAGESPILTER))).doubleValue();
            CompanyQuantity = new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,19, SysConst.PACKAGESPILTER))).intValue();
            PolicyQuantity = new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,20, SysConst.PACKAGESPILTER))).intValue();
            SumAmount = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,21, SysConst.PACKAGESPILTER))).doubleValue();
            IsRiskPerson = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 22, SysConst.PACKAGESPILTER );
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 23, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 24, SysConst.PACKAGESPILTER );
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 25, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 26, SysConst.PACKAGESPILTER );
        }
        catch(NumberFormatException ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LCSDRiskQuerySchema";
            tError.functionName = "decode";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            return false;
        }
        return true;
    }

    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("SerialNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SerialNo));
        }
        if (FCode.equalsIgnoreCase("CustomerNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CustomerNo));
        }
        if (FCode.equalsIgnoreCase("BussType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BussType));
        }
        if (FCode.equalsIgnoreCase("BussNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BussNo));
        }
        if (FCode.equalsIgnoreCase("ConType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ConType));
        }
        if (FCode.equalsIgnoreCase("Name")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Name));
        }
        if (FCode.equalsIgnoreCase("Sex")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Sex));
        }
        if (FCode.equalsIgnoreCase("Birthday")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getBirthday()));
        }
        if (FCode.equalsIgnoreCase("IdType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(IdType));
        }
        if (FCode.equalsIgnoreCase("IdNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(IdNo));
        }
        if (FCode.equalsIgnoreCase("IsRisk")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(IsRisk));
        }
        if (FCode.equalsIgnoreCase("LossTimes")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(LossTimes));
        }
        if (FCode.equalsIgnoreCase("SumClaimAmount")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SumClaimAmount));
        }
        if (FCode.equalsIgnoreCase("RefuseTimes")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RefuseTimes));
        }
        if (FCode.equalsIgnoreCase("CompanyCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CompanyCode));
        }
        if (FCode.equalsIgnoreCase("LossDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getLossDate()));
        }
        if (FCode.equalsIgnoreCase("LossResult")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(LossResult));
        }
        if (FCode.equalsIgnoreCase("ClaimAmount")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ClaimAmount));
        }
        if (FCode.equalsIgnoreCase("CompanyQuantity")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CompanyQuantity));
        }
        if (FCode.equalsIgnoreCase("PolicyQuantity")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PolicyQuantity));
        }
        if (FCode.equalsIgnoreCase("SumAmount")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SumAmount));
        }
        if (FCode.equalsIgnoreCase("IsRiskPerson")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(IsRiskPerson));
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(SerialNo);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(CustomerNo);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(BussType);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(BussNo);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(ConType);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(Name);
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(Sex);
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getBirthday()));
                break;
            case 8:
                strFieldValue = StrTool.GBKToUnicode(IdType);
                break;
            case 9:
                strFieldValue = StrTool.GBKToUnicode(IdNo);
                break;
            case 10:
                strFieldValue = StrTool.GBKToUnicode(IsRisk);
                break;
            case 11:
                strFieldValue = String.valueOf(LossTimes);
                break;
            case 12:
                strFieldValue = String.valueOf(SumClaimAmount);
                break;
            case 13:
                strFieldValue = String.valueOf(RefuseTimes);
                break;
            case 14:
                strFieldValue = StrTool.GBKToUnicode(CompanyCode);
                break;
            case 15:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getLossDate()));
                break;
            case 16:
                strFieldValue = StrTool.GBKToUnicode(LossResult);
                break;
            case 17:
                strFieldValue = String.valueOf(ClaimAmount);
                break;
            case 18:
                strFieldValue = String.valueOf(CompanyQuantity);
                break;
            case 19:
                strFieldValue = String.valueOf(PolicyQuantity);
                break;
            case 20:
                strFieldValue = String.valueOf(SumAmount);
                break;
            case 21:
                strFieldValue = StrTool.GBKToUnicode(IsRiskPerson);
                break;
            case 22:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
                break;
            case 23:
                strFieldValue = StrTool.GBKToUnicode(MakeTime);
                break;
            case 24:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
                break;
            case 25:
                strFieldValue = StrTool.GBKToUnicode(ModifyTime);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("SerialNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                SerialNo = FValue.trim();
            }
            else
                SerialNo = null;
        }
        if (FCode.equalsIgnoreCase("CustomerNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                CustomerNo = FValue.trim();
            }
            else
                CustomerNo = null;
        }
        if (FCode.equalsIgnoreCase("BussType")) {
            if( FValue != null && !FValue.equals(""))
            {
                BussType = FValue.trim();
            }
            else
                BussType = null;
        }
        if (FCode.equalsIgnoreCase("BussNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                BussNo = FValue.trim();
            }
            else
                BussNo = null;
        }
        if (FCode.equalsIgnoreCase("ConType")) {
            if( FValue != null && !FValue.equals(""))
            {
                ConType = FValue.trim();
            }
            else
                ConType = null;
        }
        if (FCode.equalsIgnoreCase("Name")) {
            if( FValue != null && !FValue.equals(""))
            {
                Name = FValue.trim();
            }
            else
                Name = null;
        }
        if (FCode.equalsIgnoreCase("Sex")) {
            if( FValue != null && !FValue.equals(""))
            {
                Sex = FValue.trim();
            }
            else
                Sex = null;
        }
        if (FCode.equalsIgnoreCase("Birthday")) {
            if(FValue != null && !FValue.equals("")) {
                Birthday = fDate.getDate( FValue );
            }
            else
                Birthday = null;
        }
        if (FCode.equalsIgnoreCase("IdType")) {
            if( FValue != null && !FValue.equals(""))
            {
                IdType = FValue.trim();
            }
            else
                IdType = null;
        }
        if (FCode.equalsIgnoreCase("IdNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                IdNo = FValue.trim();
            }
            else
                IdNo = null;
        }
        if (FCode.equalsIgnoreCase("IsRisk")) {
            if( FValue != null && !FValue.equals(""))
            {
                IsRisk = FValue.trim();
            }
            else
                IsRisk = null;
        }
        if (FCode.equalsIgnoreCase("LossTimes")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                LossTimes = i;
            }
        }
        if (FCode.equalsIgnoreCase("SumClaimAmount")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                SumClaimAmount = d;
            }
        }
        if (FCode.equalsIgnoreCase("RefuseTimes")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                RefuseTimes = i;
            }
        }
        if (FCode.equalsIgnoreCase("CompanyCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                CompanyCode = FValue.trim();
            }
            else
                CompanyCode = null;
        }
        if (FCode.equalsIgnoreCase("LossDate")) {
            if(FValue != null && !FValue.equals("")) {
                LossDate = fDate.getDate( FValue );
            }
            else
                LossDate = null;
        }
        if (FCode.equalsIgnoreCase("LossResult")) {
            if( FValue != null && !FValue.equals(""))
            {
                LossResult = FValue.trim();
            }
            else
                LossResult = null;
        }
        if (FCode.equalsIgnoreCase("ClaimAmount")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                ClaimAmount = d;
            }
        }
        if (FCode.equalsIgnoreCase("CompanyQuantity")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                CompanyQuantity = i;
            }
        }
        if (FCode.equalsIgnoreCase("PolicyQuantity")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                PolicyQuantity = i;
            }
        }
        if (FCode.equalsIgnoreCase("SumAmount")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                SumAmount = d;
            }
        }
        if (FCode.equalsIgnoreCase("IsRiskPerson")) {
            if( FValue != null && !FValue.equals(""))
            {
                IsRiskPerson = FValue.trim();
            }
            else
                IsRiskPerson = null;
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if(FValue != null && !FValue.equals("")) {
                MakeDate = fDate.getDate( FValue );
            }
            else
                MakeDate = null;
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
                MakeTime = null;
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if(FValue != null && !FValue.equals("")) {
                ModifyDate = fDate.getDate( FValue );
            }
            else
                ModifyDate = null;
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
                ModifyTime = null;
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject) return true;
        if (otherObject == null) return false;
        if (getClass() != otherObject.getClass()) return false;
        LCSDRiskQuerySchema other = (LCSDRiskQuerySchema)otherObject;
        return
            SerialNo.equals(other.getSerialNo())
            && CustomerNo.equals(other.getCustomerNo())
            && BussType.equals(other.getBussType())
            && BussNo.equals(other.getBussNo())
            && ConType.equals(other.getConType())
            && Name.equals(other.getName())
            && Sex.equals(other.getSex())
            && fDate.getString(Birthday).equals(other.getBirthday())
            && IdType.equals(other.getIdType())
            && IdNo.equals(other.getIdNo())
            && IsRisk.equals(other.getIsRisk())
            && LossTimes == other.getLossTimes()
            && SumClaimAmount == other.getSumClaimAmount()
            && RefuseTimes == other.getRefuseTimes()
            && CompanyCode.equals(other.getCompanyCode())
            && fDate.getString(LossDate).equals(other.getLossDate())
            && LossResult.equals(other.getLossResult())
            && ClaimAmount == other.getClaimAmount()
            && CompanyQuantity == other.getCompanyQuantity()
            && PolicyQuantity == other.getPolicyQuantity()
            && SumAmount == other.getSumAmount()
            && IsRiskPerson.equals(other.getIsRiskPerson())
            && fDate.getString(MakeDate).equals(other.getMakeDate())
            && MakeTime.equals(other.getMakeTime())
            && fDate.getString(ModifyDate).equals(other.getModifyDate())
            && ModifyTime.equals(other.getModifyTime());
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("SerialNo") ) {
            return 0;
        }
        if( strFieldName.equals("CustomerNo") ) {
            return 1;
        }
        if( strFieldName.equals("BussType") ) {
            return 2;
        }
        if( strFieldName.equals("BussNo") ) {
            return 3;
        }
        if( strFieldName.equals("ConType") ) {
            return 4;
        }
        if( strFieldName.equals("Name") ) {
            return 5;
        }
        if( strFieldName.equals("Sex") ) {
            return 6;
        }
        if( strFieldName.equals("Birthday") ) {
            return 7;
        }
        if( strFieldName.equals("IdType") ) {
            return 8;
        }
        if( strFieldName.equals("IdNo") ) {
            return 9;
        }
        if( strFieldName.equals("IsRisk") ) {
            return 10;
        }
        if( strFieldName.equals("LossTimes") ) {
            return 11;
        }
        if( strFieldName.equals("SumClaimAmount") ) {
            return 12;
        }
        if( strFieldName.equals("RefuseTimes") ) {
            return 13;
        }
        if( strFieldName.equals("CompanyCode") ) {
            return 14;
        }
        if( strFieldName.equals("LossDate") ) {
            return 15;
        }
        if( strFieldName.equals("LossResult") ) {
            return 16;
        }
        if( strFieldName.equals("ClaimAmount") ) {
            return 17;
        }
        if( strFieldName.equals("CompanyQuantity") ) {
            return 18;
        }
        if( strFieldName.equals("PolicyQuantity") ) {
            return 19;
        }
        if( strFieldName.equals("SumAmount") ) {
            return 20;
        }
        if( strFieldName.equals("IsRiskPerson") ) {
            return 21;
        }
        if( strFieldName.equals("MakeDate") ) {
            return 22;
        }
        if( strFieldName.equals("MakeTime") ) {
            return 23;
        }
        if( strFieldName.equals("ModifyDate") ) {
            return 24;
        }
        if( strFieldName.equals("ModifyTime") ) {
            return 25;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "SerialNo";
                break;
            case 1:
                strFieldName = "CustomerNo";
                break;
            case 2:
                strFieldName = "BussType";
                break;
            case 3:
                strFieldName = "BussNo";
                break;
            case 4:
                strFieldName = "ConType";
                break;
            case 5:
                strFieldName = "Name";
                break;
            case 6:
                strFieldName = "Sex";
                break;
            case 7:
                strFieldName = "Birthday";
                break;
            case 8:
                strFieldName = "IdType";
                break;
            case 9:
                strFieldName = "IdNo";
                break;
            case 10:
                strFieldName = "IsRisk";
                break;
            case 11:
                strFieldName = "LossTimes";
                break;
            case 12:
                strFieldName = "SumClaimAmount";
                break;
            case 13:
                strFieldName = "RefuseTimes";
                break;
            case 14:
                strFieldName = "CompanyCode";
                break;
            case 15:
                strFieldName = "LossDate";
                break;
            case 16:
                strFieldName = "LossResult";
                break;
            case 17:
                strFieldName = "ClaimAmount";
                break;
            case 18:
                strFieldName = "CompanyQuantity";
                break;
            case 19:
                strFieldName = "PolicyQuantity";
                break;
            case 20:
                strFieldName = "SumAmount";
                break;
            case 21:
                strFieldName = "IsRiskPerson";
                break;
            case 22:
                strFieldName = "MakeDate";
                break;
            case 23:
                strFieldName = "MakeTime";
                break;
            case 24:
                strFieldName = "ModifyDate";
                break;
            case 25:
                strFieldName = "ModifyTime";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "SERIALNO":
                return Schema.TYPE_STRING;
            case "CUSTOMERNO":
                return Schema.TYPE_STRING;
            case "BUSSTYPE":
                return Schema.TYPE_STRING;
            case "BUSSNO":
                return Schema.TYPE_STRING;
            case "CONTYPE":
                return Schema.TYPE_STRING;
            case "NAME":
                return Schema.TYPE_STRING;
            case "SEX":
                return Schema.TYPE_STRING;
            case "BIRTHDAY":
                return Schema.TYPE_DATE;
            case "IDTYPE":
                return Schema.TYPE_STRING;
            case "IDNO":
                return Schema.TYPE_STRING;
            case "ISRISK":
                return Schema.TYPE_STRING;
            case "LOSSTIMES":
                return Schema.TYPE_INT;
            case "SUMCLAIMAMOUNT":
                return Schema.TYPE_DOUBLE;
            case "REFUSETIMES":
                return Schema.TYPE_INT;
            case "COMPANYCODE":
                return Schema.TYPE_STRING;
            case "LOSSDATE":
                return Schema.TYPE_DATE;
            case "LOSSRESULT":
                return Schema.TYPE_STRING;
            case "CLAIMAMOUNT":
                return Schema.TYPE_DOUBLE;
            case "COMPANYQUANTITY":
                return Schema.TYPE_INT;
            case "POLICYQUANTITY":
                return Schema.TYPE_INT;
            case "SUMAMOUNT":
                return Schema.TYPE_DOUBLE;
            case "ISRISKPERSON":
                return Schema.TYPE_STRING;
            case "MAKEDATE":
                return Schema.TYPE_DATE;
            case "MAKETIME":
                return Schema.TYPE_STRING;
            case "MODIFYDATE":
                return Schema.TYPE_DATE;
            case "MODIFYTIME":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_STRING;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_STRING;
            case 6:
                return Schema.TYPE_STRING;
            case 7:
                return Schema.TYPE_DATE;
            case 8:
                return Schema.TYPE_STRING;
            case 9:
                return Schema.TYPE_STRING;
            case 10:
                return Schema.TYPE_STRING;
            case 11:
                return Schema.TYPE_INT;
            case 12:
                return Schema.TYPE_DOUBLE;
            case 13:
                return Schema.TYPE_INT;
            case 14:
                return Schema.TYPE_STRING;
            case 15:
                return Schema.TYPE_DATE;
            case 16:
                return Schema.TYPE_STRING;
            case 17:
                return Schema.TYPE_DOUBLE;
            case 18:
                return Schema.TYPE_INT;
            case 19:
                return Schema.TYPE_INT;
            case 20:
                return Schema.TYPE_DOUBLE;
            case 21:
                return Schema.TYPE_STRING;
            case 22:
                return Schema.TYPE_DATE;
            case 23:
                return Schema.TYPE_STRING;
            case 24:
                return Schema.TYPE_DATE;
            case 25:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
}
