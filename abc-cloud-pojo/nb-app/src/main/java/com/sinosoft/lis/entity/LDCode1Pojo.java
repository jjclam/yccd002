/**
 * Copyright (c) 2017 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.entity;

import com.sinosoft.cloud.cache.annotation.RedisIndexHKey;
import com.sinosoft.cloud.cache.annotation.RedisPrimaryHKey;
import com.sinosoft.utility.Pojo;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;

import java.io.Serializable;

/**
 * <p>ClassName: LDCode1Pojo </p>
 * <p>Copyright: Copyright (c) 2017</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2017-10-17
 */
public class LDCode1Pojo implements Pojo,Serializable {
    @RedisIndexHKey
    @RedisPrimaryHKey
    // @Field
    /** 编码类型 */
    private String CodeType; 
    /** 编码 */
    private String Code; 
    /** 子编码 */
    @RedisPrimaryHKey
    private String Code1; 
    /** 编码名称 */
    private String CodeName; 
    /** 编码别名 */
    private String CodeAlias; 
    /** 机构代码 */
    private String ComCode; 
    /** 其它标志 */
    @RedisPrimaryHKey
    private String OtherSign; 


    public static final int FIELDNUM = 7;    // 数据库表的字段个数
    public String getCodeType() {
        return CodeType;
    }
    public void setCodeType(String aCodeType) {
        CodeType = aCodeType;
    }
    public String getCode() {
        return Code;
    }
    public void setCode(String aCode) {
        Code = aCode;
    }
    public String getCode1() {
        return Code1;
    }
    public void setCode1(String aCode1) {
        Code1 = aCode1;
    }
    public String getCodeName() {
        return CodeName;
    }
    public void setCodeName(String aCodeName) {
        CodeName = aCodeName;
    }
    public String getCodeAlias() {
        return CodeAlias;
    }
    public void setCodeAlias(String aCodeAlias) {
        CodeAlias = aCodeAlias;
    }
    public String getComCode() {
        return ComCode;
    }
    public void setComCode(String aComCode) {
        ComCode = aComCode;
    }
    public String getOtherSign() {
        return OtherSign;
    }
    public void setOtherSign(String aOtherSign) {
        OtherSign = aOtherSign;
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("CodeType") ) {
            return 0;
        }
        if( strFieldName.equals("Code") ) {
            return 1;
        }
        if( strFieldName.equals("Code1") ) {
            return 2;
        }
        if( strFieldName.equals("CodeName") ) {
            return 3;
        }
        if( strFieldName.equals("CodeAlias") ) {
            return 4;
        }
        if( strFieldName.equals("ComCode") ) {
            return 5;
        }
        if( strFieldName.equals("OtherSign") ) {
            return 6;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "CodeType";
                break;
            case 1:
                strFieldName = "Code";
                break;
            case 2:
                strFieldName = "Code1";
                break;
            case 3:
                strFieldName = "CodeName";
                break;
            case 4:
                strFieldName = "CodeAlias";
                break;
            case 5:
                strFieldName = "ComCode";
                break;
            case 6:
                strFieldName = "OtherSign";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "CODETYPE":
                return Schema.TYPE_STRING;
            case "CODE":
                return Schema.TYPE_STRING;
            case "CODE1":
                return Schema.TYPE_STRING;
            case "CODENAME":
                return Schema.TYPE_STRING;
            case "CODEALIAS":
                return Schema.TYPE_STRING;
            case "COMCODE":
                return Schema.TYPE_STRING;
            case "OTHERSIGN":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_STRING;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_STRING;
            case 6:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("CodeType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CodeType));
        }
        if (FCode.equalsIgnoreCase("Code")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Code));
        }
        if (FCode.equalsIgnoreCase("Code1")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Code1));
        }
        if (FCode.equalsIgnoreCase("CodeName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CodeName));
        }
        if (FCode.equalsIgnoreCase("CodeAlias")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CodeAlias));
        }
        if (FCode.equalsIgnoreCase("ComCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ComCode));
        }
        if (FCode.equalsIgnoreCase("OtherSign")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OtherSign));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = String.valueOf(CodeType);
                break;
            case 1:
                strFieldValue = String.valueOf(Code);
                break;
            case 2:
                strFieldValue = String.valueOf(Code1);
                break;
            case 3:
                strFieldValue = String.valueOf(CodeName);
                break;
            case 4:
                strFieldValue = String.valueOf(CodeAlias);
                break;
            case 5:
                strFieldValue = String.valueOf(ComCode);
                break;
            case 6:
                strFieldValue = String.valueOf(OtherSign);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("CodeType")) {
            if( FValue != null && !FValue.equals(""))
            {
                CodeType = FValue.trim();
            }
            else
                CodeType = null;
        }
        if (FCode.equalsIgnoreCase("Code")) {
            if( FValue != null && !FValue.equals(""))
            {
                Code = FValue.trim();
            }
            else
                Code = null;
        }
        if (FCode.equalsIgnoreCase("Code1")) {
            if( FValue != null && !FValue.equals(""))
            {
                Code1 = FValue.trim();
            }
            else
                Code1 = null;
        }
        if (FCode.equalsIgnoreCase("CodeName")) {
            if( FValue != null && !FValue.equals(""))
            {
                CodeName = FValue.trim();
            }
            else
                CodeName = null;
        }
        if (FCode.equalsIgnoreCase("CodeAlias")) {
            if( FValue != null && !FValue.equals(""))
            {
                CodeAlias = FValue.trim();
            }
            else
                CodeAlias = null;
        }
        if (FCode.equalsIgnoreCase("ComCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                ComCode = FValue.trim();
            }
            else
                ComCode = null;
        }
        if (FCode.equalsIgnoreCase("OtherSign")) {
            if( FValue != null && !FValue.equals(""))
            {
                OtherSign = FValue.trim();
            }
            else
                OtherSign = null;
        }
        return true;
    }


    public String toString() {
    return "LDCode1Pojo [" +
            "CodeType="+CodeType +
            ", Code="+Code +
            ", Code1="+Code1 +
            ", CodeName="+CodeName +
            ", CodeAlias="+CodeAlias +
            ", ComCode="+ComCode +
            ", OtherSign="+OtherSign +"]";
    }
}
