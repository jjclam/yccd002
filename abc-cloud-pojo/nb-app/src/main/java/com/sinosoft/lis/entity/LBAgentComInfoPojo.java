/**
 * Copyright (c) 2018 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.entity;

import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.Pojo;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;

import java.io.Serializable;

/**
 * <p>ClassName: LBAgentComInfoPojo </p>
 * <p>Copyright: Copyright (c) 2018</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2018-05-23
 */
public class LBAgentComInfoPojo implements Pojo,Serializable {
    // @Field
    /** 序号 */
    private String SerialNo; 
    /** 中介机构编码 */
    private String AgentCom; 
    /** 保单号码 */
    private String PolicyNo; 
    /** 中介机构名称 */
    private String AgentComName; 
    /** 代理人编码 */
    private String AgentCode; 
    /** 代理人姓名 */
    private String AgentName; 
    /** 网点编码 */
    private String AgentBranchesCode; 
    /** 网点名称 */
    private String AgentBranchesName; 
    /** 所属区域编码 */
    private String AgentGroup; 
    /** 所属区域名称 */
    private String AgentGroupName; 
    /** 分佣比例 */
    private double CommBusiRate; 
    /** 管理机构 */
    private String ManageCom; 
    /** 公司代码 */
    private String ComCode; 
    /** 入机操作员 */
    private String MakeOperator; 
    /** 入机日期 */
    private String  MakeDate;
    /** 入机时间 */
    private String MakeTime; 
    /** 最后一次修改操作员 */
    private String ModifyOperator; 
    /** 最后一次修改日期 */
    private String  ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime; 


    public static final int FIELDNUM = 19;    // 数据库表的字段个数

    private FDate fDate = new FDate();        // 处理日期
    public String getSerialNo() {
        return SerialNo;
    }
    public void setSerialNo(String aSerialNo) {
        SerialNo = aSerialNo;
    }
    public String getAgentCom() {
        return AgentCom;
    }
    public void setAgentCom(String aAgentCom) {
        AgentCom = aAgentCom;
    }
    public String getPolicyNo() {
        return PolicyNo;
    }
    public void setPolicyNo(String aPolicyNo) {
        PolicyNo = aPolicyNo;
    }
    public String getAgentComName() {
        return AgentComName;
    }
    public void setAgentComName(String aAgentComName) {
        AgentComName = aAgentComName;
    }
    public String getAgentCode() {
        return AgentCode;
    }
    public void setAgentCode(String aAgentCode) {
        AgentCode = aAgentCode;
    }
    public String getAgentName() {
        return AgentName;
    }
    public void setAgentName(String aAgentName) {
        AgentName = aAgentName;
    }
    public String getAgentBranchesCode() {
        return AgentBranchesCode;
    }
    public void setAgentBranchesCode(String aAgentBranchesCode) {
        AgentBranchesCode = aAgentBranchesCode;
    }
    public String getAgentBranchesName() {
        return AgentBranchesName;
    }
    public void setAgentBranchesName(String aAgentBranchesName) {
        AgentBranchesName = aAgentBranchesName;
    }
    public String getAgentGroup() {
        return AgentGroup;
    }
    public void setAgentGroup(String aAgentGroup) {
        AgentGroup = aAgentGroup;
    }
    public String getAgentGroupName() {
        return AgentGroupName;
    }
    public void setAgentGroupName(String aAgentGroupName) {
        AgentGroupName = aAgentGroupName;
    }
    public double getCommBusiRate() {
        return CommBusiRate;
    }
    public void setCommBusiRate(double aCommBusiRate) {
        CommBusiRate = aCommBusiRate;
    }
    public void setCommBusiRate(String aCommBusiRate) {
        if (aCommBusiRate != null && !aCommBusiRate.equals("")) {
            Double tDouble = new Double(aCommBusiRate);
            double d = tDouble.doubleValue();
            CommBusiRate = d;
        }
    }

    public String getManageCom() {
        return ManageCom;
    }
    public void setManageCom(String aManageCom) {
        ManageCom = aManageCom;
    }
    public String getComCode() {
        return ComCode;
    }
    public void setComCode(String aComCode) {
        ComCode = aComCode;
    }
    public String getMakeOperator() {
        return MakeOperator;
    }
    public void setMakeOperator(String aMakeOperator) {
        MakeOperator = aMakeOperator;
    }
    public String getMakeDate() {
        return MakeDate;
    }
    public void setMakeDate(String aMakeDate) {
        MakeDate = aMakeDate;
    }
    public String getMakeTime() {
        return MakeTime;
    }
    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }
    public String getModifyOperator() {
        return ModifyOperator;
    }
    public void setModifyOperator(String aModifyOperator) {
        ModifyOperator = aModifyOperator;
    }
    public String getModifyDate() {
        return ModifyDate;
    }
    public void setModifyDate(String aModifyDate) {
        ModifyDate = aModifyDate;
    }
    public String getModifyTime() {
        return ModifyTime;
    }
    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("SerialNo") ) {
            return 0;
        }
        if( strFieldName.equals("AgentCom") ) {
            return 1;
        }
        if( strFieldName.equals("PolicyNo") ) {
            return 2;
        }
        if( strFieldName.equals("AgentComName") ) {
            return 3;
        }
        if( strFieldName.equals("AgentCode") ) {
            return 4;
        }
        if( strFieldName.equals("AgentName") ) {
            return 5;
        }
        if( strFieldName.equals("AgentBranchesCode") ) {
            return 6;
        }
        if( strFieldName.equals("AgentBranchesName") ) {
            return 7;
        }
        if( strFieldName.equals("AgentGroup") ) {
            return 8;
        }
        if( strFieldName.equals("AgentGroupName") ) {
            return 9;
        }
        if( strFieldName.equals("CommBusiRate") ) {
            return 10;
        }
        if( strFieldName.equals("ManageCom") ) {
            return 11;
        }
        if( strFieldName.equals("ComCode") ) {
            return 12;
        }
        if( strFieldName.equals("MakeOperator") ) {
            return 13;
        }
        if( strFieldName.equals("MakeDate") ) {
            return 14;
        }
        if( strFieldName.equals("MakeTime") ) {
            return 15;
        }
        if( strFieldName.equals("ModifyOperator") ) {
            return 16;
        }
        if( strFieldName.equals("ModifyDate") ) {
            return 17;
        }
        if( strFieldName.equals("ModifyTime") ) {
            return 18;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "SerialNo";
                break;
            case 1:
                strFieldName = "AgentCom";
                break;
            case 2:
                strFieldName = "PolicyNo";
                break;
            case 3:
                strFieldName = "AgentComName";
                break;
            case 4:
                strFieldName = "AgentCode";
                break;
            case 5:
                strFieldName = "AgentName";
                break;
            case 6:
                strFieldName = "AgentBranchesCode";
                break;
            case 7:
                strFieldName = "AgentBranchesName";
                break;
            case 8:
                strFieldName = "AgentGroup";
                break;
            case 9:
                strFieldName = "AgentGroupName";
                break;
            case 10:
                strFieldName = "CommBusiRate";
                break;
            case 11:
                strFieldName = "ManageCom";
                break;
            case 12:
                strFieldName = "ComCode";
                break;
            case 13:
                strFieldName = "MakeOperator";
                break;
            case 14:
                strFieldName = "MakeDate";
                break;
            case 15:
                strFieldName = "MakeTime";
                break;
            case 16:
                strFieldName = "ModifyOperator";
                break;
            case 17:
                strFieldName = "ModifyDate";
                break;
            case 18:
                strFieldName = "ModifyTime";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "SERIALNO":
                return Schema.TYPE_STRING;
            case "AGENTCOM":
                return Schema.TYPE_STRING;
            case "POLICYNO":
                return Schema.TYPE_STRING;
            case "AGENTCOMNAME":
                return Schema.TYPE_STRING;
            case "AGENTCODE":
                return Schema.TYPE_STRING;
            case "AGENTNAME":
                return Schema.TYPE_STRING;
            case "AGENTBRANCHESCODE":
                return Schema.TYPE_STRING;
            case "AGENTBRANCHESNAME":
                return Schema.TYPE_STRING;
            case "AGENTGROUP":
                return Schema.TYPE_STRING;
            case "AGENTGROUPNAME":
                return Schema.TYPE_STRING;
            case "COMMBUSIRATE":
                return Schema.TYPE_DOUBLE;
            case "MANAGECOM":
                return Schema.TYPE_STRING;
            case "COMCODE":
                return Schema.TYPE_STRING;
            case "MAKEOPERATOR":
                return Schema.TYPE_STRING;
            case "MAKEDATE":
                return Schema.TYPE_STRING;
            case "MAKETIME":
                return Schema.TYPE_STRING;
            case "MODIFYOPERATOR":
                return Schema.TYPE_STRING;
            case "MODIFYDATE":
                return Schema.TYPE_STRING;
            case "MODIFYTIME":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_STRING;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_STRING;
            case 6:
                return Schema.TYPE_STRING;
            case 7:
                return Schema.TYPE_STRING;
            case 8:
                return Schema.TYPE_STRING;
            case 9:
                return Schema.TYPE_STRING;
            case 10:
                return Schema.TYPE_DOUBLE;
            case 11:
                return Schema.TYPE_STRING;
            case 12:
                return Schema.TYPE_STRING;
            case 13:
                return Schema.TYPE_STRING;
            case 14:
                return Schema.TYPE_STRING;
            case 15:
                return Schema.TYPE_STRING;
            case 16:
                return Schema.TYPE_STRING;
            case 17:
                return Schema.TYPE_STRING;
            case 18:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("SerialNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SerialNo));
        }
        if (FCode.equalsIgnoreCase("AgentCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentCom));
        }
        if (FCode.equalsIgnoreCase("PolicyNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PolicyNo));
        }
        if (FCode.equalsIgnoreCase("AgentComName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentComName));
        }
        if (FCode.equalsIgnoreCase("AgentCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentCode));
        }
        if (FCode.equalsIgnoreCase("AgentName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentName));
        }
        if (FCode.equalsIgnoreCase("AgentBranchesCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentBranchesCode));
        }
        if (FCode.equalsIgnoreCase("AgentBranchesName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentBranchesName));
        }
        if (FCode.equalsIgnoreCase("AgentGroup")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentGroup));
        }
        if (FCode.equalsIgnoreCase("AgentGroupName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentGroupName));
        }
        if (FCode.equalsIgnoreCase("CommBusiRate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CommBusiRate));
        }
        if (FCode.equalsIgnoreCase("ManageCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
        }
        if (FCode.equalsIgnoreCase("ComCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ComCode));
        }
        if (FCode.equalsIgnoreCase("MakeOperator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeOperator));
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeDate));
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equalsIgnoreCase("ModifyOperator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyOperator));
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyDate));
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = String.valueOf(SerialNo);
                break;
            case 1:
                strFieldValue = String.valueOf(AgentCom);
                break;
            case 2:
                strFieldValue = String.valueOf(PolicyNo);
                break;
            case 3:
                strFieldValue = String.valueOf(AgentComName);
                break;
            case 4:
                strFieldValue = String.valueOf(AgentCode);
                break;
            case 5:
                strFieldValue = String.valueOf(AgentName);
                break;
            case 6:
                strFieldValue = String.valueOf(AgentBranchesCode);
                break;
            case 7:
                strFieldValue = String.valueOf(AgentBranchesName);
                break;
            case 8:
                strFieldValue = String.valueOf(AgentGroup);
                break;
            case 9:
                strFieldValue = String.valueOf(AgentGroupName);
                break;
            case 10:
                strFieldValue = String.valueOf(CommBusiRate);
                break;
            case 11:
                strFieldValue = String.valueOf(ManageCom);
                break;
            case 12:
                strFieldValue = String.valueOf(ComCode);
                break;
            case 13:
                strFieldValue = String.valueOf(MakeOperator);
                break;
            case 14:
                strFieldValue = String.valueOf(MakeDate);
                break;
            case 15:
                strFieldValue = String.valueOf(MakeTime);
                break;
            case 16:
                strFieldValue = String.valueOf(ModifyOperator);
                break;
            case 17:
                strFieldValue = String.valueOf(ModifyDate);
                break;
            case 18:
                strFieldValue = String.valueOf(ModifyTime);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("SerialNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                SerialNo = FValue.trim();
            }
            else
                SerialNo = null;
        }
        if (FCode.equalsIgnoreCase("AgentCom")) {
            if( FValue != null && !FValue.equals(""))
            {
                AgentCom = FValue.trim();
            }
            else
                AgentCom = null;
        }
        if (FCode.equalsIgnoreCase("PolicyNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                PolicyNo = FValue.trim();
            }
            else
                PolicyNo = null;
        }
        if (FCode.equalsIgnoreCase("AgentComName")) {
            if( FValue != null && !FValue.equals(""))
            {
                AgentComName = FValue.trim();
            }
            else
                AgentComName = null;
        }
        if (FCode.equalsIgnoreCase("AgentCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                AgentCode = FValue.trim();
            }
            else
                AgentCode = null;
        }
        if (FCode.equalsIgnoreCase("AgentName")) {
            if( FValue != null && !FValue.equals(""))
            {
                AgentName = FValue.trim();
            }
            else
                AgentName = null;
        }
        if (FCode.equalsIgnoreCase("AgentBranchesCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                AgentBranchesCode = FValue.trim();
            }
            else
                AgentBranchesCode = null;
        }
        if (FCode.equalsIgnoreCase("AgentBranchesName")) {
            if( FValue != null && !FValue.equals(""))
            {
                AgentBranchesName = FValue.trim();
            }
            else
                AgentBranchesName = null;
        }
        if (FCode.equalsIgnoreCase("AgentGroup")) {
            if( FValue != null && !FValue.equals(""))
            {
                AgentGroup = FValue.trim();
            }
            else
                AgentGroup = null;
        }
        if (FCode.equalsIgnoreCase("AgentGroupName")) {
            if( FValue != null && !FValue.equals(""))
            {
                AgentGroupName = FValue.trim();
            }
            else
                AgentGroupName = null;
        }
        if (FCode.equalsIgnoreCase("CommBusiRate")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                CommBusiRate = d;
            }
        }
        if (FCode.equalsIgnoreCase("ManageCom")) {
            if( FValue != null && !FValue.equals(""))
            {
                ManageCom = FValue.trim();
            }
            else
                ManageCom = null;
        }
        if (FCode.equalsIgnoreCase("ComCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                ComCode = FValue.trim();
            }
            else
                ComCode = null;
        }
        if (FCode.equalsIgnoreCase("MakeOperator")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeOperator = FValue.trim();
            }
            else
                MakeOperator = null;
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeDate = FValue.trim();
            }
            else
                MakeDate = null;
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
                MakeTime = null;
        }
        if (FCode.equalsIgnoreCase("ModifyOperator")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyOperator = FValue.trim();
            }
            else
                ModifyOperator = null;
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyDate = FValue.trim();
            }
            else
                ModifyDate = null;
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
                ModifyTime = null;
        }
        return true;
    }


    public String toString() {
    return "LBAgentComInfoPojo [" +
            "SerialNo="+SerialNo +
            ", AgentCom="+AgentCom +
            ", PolicyNo="+PolicyNo +
            ", AgentComName="+AgentComName +
            ", AgentCode="+AgentCode +
            ", AgentName="+AgentName +
            ", AgentBranchesCode="+AgentBranchesCode +
            ", AgentBranchesName="+AgentBranchesName +
            ", AgentGroup="+AgentGroup +
            ", AgentGroupName="+AgentGroupName +
            ", CommBusiRate="+CommBusiRate +
            ", ManageCom="+ManageCom +
            ", ComCode="+ComCode +
            ", MakeOperator="+MakeOperator +
            ", MakeDate="+MakeDate +
            ", MakeTime="+MakeTime +
            ", ModifyOperator="+ModifyOperator +
            ", ModifyDate="+ModifyDate +
            ", ModifyTime="+ModifyTime +"]";
    }
}
