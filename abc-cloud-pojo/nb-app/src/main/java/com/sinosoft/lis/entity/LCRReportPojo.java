/**
 * Copyright (c) 2017 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.entity;

import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.Pojo;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;

import java.io.Serializable;

/**
 * <p>ClassName: LCRReportPojo </p>
 * <p>Copyright: Copyright (c) 2017</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2017-10-17
 */
public class LCRReportPojo implements Pojo,Serializable {
    // @Field
    /** 集体合同号码 */
    private String GrpContNo; 
    /** 合同号码 */
    private String ContNo; 
    /** 总单投保单号码 */
    private String ProposalContNo; 
    /** 打印流水号 */
    private String PrtSeq; 
    /** 投保人编码 */
    private String AppntNo; 
    /** 投保人 */
    private String AppntName; 
    /** 生调人编码 */
    private String CustomerNo; 
    /** 生调人姓名 */
    private String Name; 
    /** 管理机构 */
    private String ManageCom; 
    /** 生调内容 */
    private String Contente; 
    /** 回复内容 */
    private String ReplyContente; 
    /** 回复标记 */
    private String ReplyFlag; 
    /** 操作员 */
    private String Operator; 
    /** 回复人 */
    private String ReplyOperator; 
    /** 回复日期 */
    private String  ReplyDate;
    /** 回复时间 */
    private String ReplyTime; 
    /** 录入日期 */
    private String  MakeDate;
    /** 录入时间 */
    private String MakeTime; 
    /** 修改日期 */
    private String  ModifyDate;
    /** 修改时间 */
    private String ModifyTime; 
    /** 团单生调总表打印流水号 */
    private String GrpPrtSeq; 
    /** 备注 */
    private String ReMark; 
    /** 生调原因 */
    private String RReportReason; 
    /** 生调费用 */
    private int RReportFee; 
    /** 是否在保单中打印 */
    private String NeedPrint; 


    public static final int FIELDNUM = 25;    // 数据库表的字段个数

    private FDate fDate = new FDate();        // 处理日期
    public String getGrpContNo() {
        return GrpContNo;
    }
    public void setGrpContNo(String aGrpContNo) {
        GrpContNo = aGrpContNo;
    }
    public String getContNo() {
        return ContNo;
    }
    public void setContNo(String aContNo) {
        ContNo = aContNo;
    }
    public String getProposalContNo() {
        return ProposalContNo;
    }
    public void setProposalContNo(String aProposalContNo) {
        ProposalContNo = aProposalContNo;
    }
    public String getPrtSeq() {
        return PrtSeq;
    }
    public void setPrtSeq(String aPrtSeq) {
        PrtSeq = aPrtSeq;
    }
    public String getAppntNo() {
        return AppntNo;
    }
    public void setAppntNo(String aAppntNo) {
        AppntNo = aAppntNo;
    }
    public String getAppntName() {
        return AppntName;
    }
    public void setAppntName(String aAppntName) {
        AppntName = aAppntName;
    }
    public String getCustomerNo() {
        return CustomerNo;
    }
    public void setCustomerNo(String aCustomerNo) {
        CustomerNo = aCustomerNo;
    }
    public String getName() {
        return Name;
    }
    public void setName(String aName) {
        Name = aName;
    }
    public String getManageCom() {
        return ManageCom;
    }
    public void setManageCom(String aManageCom) {
        ManageCom = aManageCom;
    }
    public String getContente() {
        return Contente;
    }
    public void setContente(String aContente) {
        Contente = aContente;
    }
    public String getReplyContente() {
        return ReplyContente;
    }
    public void setReplyContente(String aReplyContente) {
        ReplyContente = aReplyContente;
    }
    public String getReplyFlag() {
        return ReplyFlag;
    }
    public void setReplyFlag(String aReplyFlag) {
        ReplyFlag = aReplyFlag;
    }
    public String getOperator() {
        return Operator;
    }
    public void setOperator(String aOperator) {
        Operator = aOperator;
    }
    public String getReplyOperator() {
        return ReplyOperator;
    }
    public void setReplyOperator(String aReplyOperator) {
        ReplyOperator = aReplyOperator;
    }
    public String getReplyDate() {
        return ReplyDate;
    }
    public void setReplyDate(String aReplyDate) {
        ReplyDate = aReplyDate;
    }
    public String getReplyTime() {
        return ReplyTime;
    }
    public void setReplyTime(String aReplyTime) {
        ReplyTime = aReplyTime;
    }
    public String getMakeDate() {
        return MakeDate;
    }
    public void setMakeDate(String aMakeDate) {
        MakeDate = aMakeDate;
    }
    public String getMakeTime() {
        return MakeTime;
    }
    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }
    public String getModifyDate() {
        return ModifyDate;
    }
    public void setModifyDate(String aModifyDate) {
        ModifyDate = aModifyDate;
    }
    public String getModifyTime() {
        return ModifyTime;
    }
    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }
    public String getGrpPrtSeq() {
        return GrpPrtSeq;
    }
    public void setGrpPrtSeq(String aGrpPrtSeq) {
        GrpPrtSeq = aGrpPrtSeq;
    }
    public String getReMark() {
        return ReMark;
    }
    public void setReMark(String aReMark) {
        ReMark = aReMark;
    }
    public String getRReportReason() {
        return RReportReason;
    }
    public void setRReportReason(String aRReportReason) {
        RReportReason = aRReportReason;
    }
    public int getRReportFee() {
        return RReportFee;
    }
    public void setRReportFee(int aRReportFee) {
        RReportFee = aRReportFee;
    }
    public void setRReportFee(String aRReportFee) {
        if (aRReportFee != null && !aRReportFee.equals("")) {
            Integer tInteger = new Integer(aRReportFee);
            int i = tInteger.intValue();
            RReportFee = i;
        }
    }

    public String getNeedPrint() {
        return NeedPrint;
    }
    public void setNeedPrint(String aNeedPrint) {
        NeedPrint = aNeedPrint;
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("GrpContNo") ) {
            return 0;
        }
        if( strFieldName.equals("ContNo") ) {
            return 1;
        }
        if( strFieldName.equals("ProposalContNo") ) {
            return 2;
        }
        if( strFieldName.equals("PrtSeq") ) {
            return 3;
        }
        if( strFieldName.equals("AppntNo") ) {
            return 4;
        }
        if( strFieldName.equals("AppntName") ) {
            return 5;
        }
        if( strFieldName.equals("CustomerNo") ) {
            return 6;
        }
        if( strFieldName.equals("Name") ) {
            return 7;
        }
        if( strFieldName.equals("ManageCom") ) {
            return 8;
        }
        if( strFieldName.equals("Contente") ) {
            return 9;
        }
        if( strFieldName.equals("ReplyContente") ) {
            return 10;
        }
        if( strFieldName.equals("ReplyFlag") ) {
            return 11;
        }
        if( strFieldName.equals("Operator") ) {
            return 12;
        }
        if( strFieldName.equals("ReplyOperator") ) {
            return 13;
        }
        if( strFieldName.equals("ReplyDate") ) {
            return 14;
        }
        if( strFieldName.equals("ReplyTime") ) {
            return 15;
        }
        if( strFieldName.equals("MakeDate") ) {
            return 16;
        }
        if( strFieldName.equals("MakeTime") ) {
            return 17;
        }
        if( strFieldName.equals("ModifyDate") ) {
            return 18;
        }
        if( strFieldName.equals("ModifyTime") ) {
            return 19;
        }
        if( strFieldName.equals("GrpPrtSeq") ) {
            return 20;
        }
        if( strFieldName.equals("ReMark") ) {
            return 21;
        }
        if( strFieldName.equals("RReportReason") ) {
            return 22;
        }
        if( strFieldName.equals("RReportFee") ) {
            return 23;
        }
        if( strFieldName.equals("NeedPrint") ) {
            return 24;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "GrpContNo";
                break;
            case 1:
                strFieldName = "ContNo";
                break;
            case 2:
                strFieldName = "ProposalContNo";
                break;
            case 3:
                strFieldName = "PrtSeq";
                break;
            case 4:
                strFieldName = "AppntNo";
                break;
            case 5:
                strFieldName = "AppntName";
                break;
            case 6:
                strFieldName = "CustomerNo";
                break;
            case 7:
                strFieldName = "Name";
                break;
            case 8:
                strFieldName = "ManageCom";
                break;
            case 9:
                strFieldName = "Contente";
                break;
            case 10:
                strFieldName = "ReplyContente";
                break;
            case 11:
                strFieldName = "ReplyFlag";
                break;
            case 12:
                strFieldName = "Operator";
                break;
            case 13:
                strFieldName = "ReplyOperator";
                break;
            case 14:
                strFieldName = "ReplyDate";
                break;
            case 15:
                strFieldName = "ReplyTime";
                break;
            case 16:
                strFieldName = "MakeDate";
                break;
            case 17:
                strFieldName = "MakeTime";
                break;
            case 18:
                strFieldName = "ModifyDate";
                break;
            case 19:
                strFieldName = "ModifyTime";
                break;
            case 20:
                strFieldName = "GrpPrtSeq";
                break;
            case 21:
                strFieldName = "ReMark";
                break;
            case 22:
                strFieldName = "RReportReason";
                break;
            case 23:
                strFieldName = "RReportFee";
                break;
            case 24:
                strFieldName = "NeedPrint";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "GRPCONTNO":
                return Schema.TYPE_STRING;
            case "CONTNO":
                return Schema.TYPE_STRING;
            case "PROPOSALCONTNO":
                return Schema.TYPE_STRING;
            case "PRTSEQ":
                return Schema.TYPE_STRING;
            case "APPNTNO":
                return Schema.TYPE_STRING;
            case "APPNTNAME":
                return Schema.TYPE_STRING;
            case "CUSTOMERNO":
                return Schema.TYPE_STRING;
            case "NAME":
                return Schema.TYPE_STRING;
            case "MANAGECOM":
                return Schema.TYPE_STRING;
            case "CONTENTE":
                return Schema.TYPE_STRING;
            case "REPLYCONTENTE":
                return Schema.TYPE_STRING;
            case "REPLYFLAG":
                return Schema.TYPE_STRING;
            case "OPERATOR":
                return Schema.TYPE_STRING;
            case "REPLYOPERATOR":
                return Schema.TYPE_STRING;
            case "REPLYDATE":
                return Schema.TYPE_STRING;
            case "REPLYTIME":
                return Schema.TYPE_STRING;
            case "MAKEDATE":
                return Schema.TYPE_STRING;
            case "MAKETIME":
                return Schema.TYPE_STRING;
            case "MODIFYDATE":
                return Schema.TYPE_STRING;
            case "MODIFYTIME":
                return Schema.TYPE_STRING;
            case "GRPPRTSEQ":
                return Schema.TYPE_STRING;
            case "REMARK":
                return Schema.TYPE_STRING;
            case "RREPORTREASON":
                return Schema.TYPE_STRING;
            case "RREPORTFEE":
                return Schema.TYPE_INT;
            case "NEEDPRINT":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_STRING;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_STRING;
            case 6:
                return Schema.TYPE_STRING;
            case 7:
                return Schema.TYPE_STRING;
            case 8:
                return Schema.TYPE_STRING;
            case 9:
                return Schema.TYPE_STRING;
            case 10:
                return Schema.TYPE_STRING;
            case 11:
                return Schema.TYPE_STRING;
            case 12:
                return Schema.TYPE_STRING;
            case 13:
                return Schema.TYPE_STRING;
            case 14:
                return Schema.TYPE_STRING;
            case 15:
                return Schema.TYPE_STRING;
            case 16:
                return Schema.TYPE_STRING;
            case 17:
                return Schema.TYPE_STRING;
            case 18:
                return Schema.TYPE_STRING;
            case 19:
                return Schema.TYPE_STRING;
            case 20:
                return Schema.TYPE_STRING;
            case 21:
                return Schema.TYPE_STRING;
            case 22:
                return Schema.TYPE_STRING;
            case 23:
                return Schema.TYPE_INT;
            case 24:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("GrpContNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GrpContNo));
        }
        if (FCode.equalsIgnoreCase("ContNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ContNo));
        }
        if (FCode.equalsIgnoreCase("ProposalContNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ProposalContNo));
        }
        if (FCode.equalsIgnoreCase("PrtSeq")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PrtSeq));
        }
        if (FCode.equalsIgnoreCase("AppntNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AppntNo));
        }
        if (FCode.equalsIgnoreCase("AppntName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AppntName));
        }
        if (FCode.equalsIgnoreCase("CustomerNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CustomerNo));
        }
        if (FCode.equalsIgnoreCase("Name")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Name));
        }
        if (FCode.equalsIgnoreCase("ManageCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
        }
        if (FCode.equalsIgnoreCase("Contente")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Contente));
        }
        if (FCode.equalsIgnoreCase("ReplyContente")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ReplyContente));
        }
        if (FCode.equalsIgnoreCase("ReplyFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ReplyFlag));
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equalsIgnoreCase("ReplyOperator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ReplyOperator));
        }
        if (FCode.equalsIgnoreCase("ReplyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ReplyDate));
        }
        if (FCode.equalsIgnoreCase("ReplyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ReplyTime));
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeDate));
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyDate));
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (FCode.equalsIgnoreCase("GrpPrtSeq")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GrpPrtSeq));
        }
        if (FCode.equalsIgnoreCase("ReMark")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ReMark));
        }
        if (FCode.equalsIgnoreCase("RReportReason")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RReportReason));
        }
        if (FCode.equalsIgnoreCase("RReportFee")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RReportFee));
        }
        if (FCode.equalsIgnoreCase("NeedPrint")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(NeedPrint));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = String.valueOf(GrpContNo);
                break;
            case 1:
                strFieldValue = String.valueOf(ContNo);
                break;
            case 2:
                strFieldValue = String.valueOf(ProposalContNo);
                break;
            case 3:
                strFieldValue = String.valueOf(PrtSeq);
                break;
            case 4:
                strFieldValue = String.valueOf(AppntNo);
                break;
            case 5:
                strFieldValue = String.valueOf(AppntName);
                break;
            case 6:
                strFieldValue = String.valueOf(CustomerNo);
                break;
            case 7:
                strFieldValue = String.valueOf(Name);
                break;
            case 8:
                strFieldValue = String.valueOf(ManageCom);
                break;
            case 9:
                strFieldValue = String.valueOf(Contente);
                break;
            case 10:
                strFieldValue = String.valueOf(ReplyContente);
                break;
            case 11:
                strFieldValue = String.valueOf(ReplyFlag);
                break;
            case 12:
                strFieldValue = String.valueOf(Operator);
                break;
            case 13:
                strFieldValue = String.valueOf(ReplyOperator);
                break;
            case 14:
                strFieldValue = String.valueOf(ReplyDate);
                break;
            case 15:
                strFieldValue = String.valueOf(ReplyTime);
                break;
            case 16:
                strFieldValue = String.valueOf(MakeDate);
                break;
            case 17:
                strFieldValue = String.valueOf(MakeTime);
                break;
            case 18:
                strFieldValue = String.valueOf(ModifyDate);
                break;
            case 19:
                strFieldValue = String.valueOf(ModifyTime);
                break;
            case 20:
                strFieldValue = String.valueOf(GrpPrtSeq);
                break;
            case 21:
                strFieldValue = String.valueOf(ReMark);
                break;
            case 22:
                strFieldValue = String.valueOf(RReportReason);
                break;
            case 23:
                strFieldValue = String.valueOf(RReportFee);
                break;
            case 24:
                strFieldValue = String.valueOf(NeedPrint);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("GrpContNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                GrpContNo = FValue.trim();
            }
            else
                GrpContNo = null;
        }
        if (FCode.equalsIgnoreCase("ContNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                ContNo = FValue.trim();
            }
            else
                ContNo = null;
        }
        if (FCode.equalsIgnoreCase("ProposalContNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                ProposalContNo = FValue.trim();
            }
            else
                ProposalContNo = null;
        }
        if (FCode.equalsIgnoreCase("PrtSeq")) {
            if( FValue != null && !FValue.equals(""))
            {
                PrtSeq = FValue.trim();
            }
            else
                PrtSeq = null;
        }
        if (FCode.equalsIgnoreCase("AppntNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                AppntNo = FValue.trim();
            }
            else
                AppntNo = null;
        }
        if (FCode.equalsIgnoreCase("AppntName")) {
            if( FValue != null && !FValue.equals(""))
            {
                AppntName = FValue.trim();
            }
            else
                AppntName = null;
        }
        if (FCode.equalsIgnoreCase("CustomerNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                CustomerNo = FValue.trim();
            }
            else
                CustomerNo = null;
        }
        if (FCode.equalsIgnoreCase("Name")) {
            if( FValue != null && !FValue.equals(""))
            {
                Name = FValue.trim();
            }
            else
                Name = null;
        }
        if (FCode.equalsIgnoreCase("ManageCom")) {
            if( FValue != null && !FValue.equals(""))
            {
                ManageCom = FValue.trim();
            }
            else
                ManageCom = null;
        }
        if (FCode.equalsIgnoreCase("Contente")) {
            if( FValue != null && !FValue.equals(""))
            {
                Contente = FValue.trim();
            }
            else
                Contente = null;
        }
        if (FCode.equalsIgnoreCase("ReplyContente")) {
            if( FValue != null && !FValue.equals(""))
            {
                ReplyContente = FValue.trim();
            }
            else
                ReplyContente = null;
        }
        if (FCode.equalsIgnoreCase("ReplyFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                ReplyFlag = FValue.trim();
            }
            else
                ReplyFlag = null;
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            if( FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
                Operator = null;
        }
        if (FCode.equalsIgnoreCase("ReplyOperator")) {
            if( FValue != null && !FValue.equals(""))
            {
                ReplyOperator = FValue.trim();
            }
            else
                ReplyOperator = null;
        }
        if (FCode.equalsIgnoreCase("ReplyDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                ReplyDate = FValue.trim();
            }
            else
                ReplyDate = null;
        }
        if (FCode.equalsIgnoreCase("ReplyTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                ReplyTime = FValue.trim();
            }
            else
                ReplyTime = null;
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeDate = FValue.trim();
            }
            else
                MakeDate = null;
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
                MakeTime = null;
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyDate = FValue.trim();
            }
            else
                ModifyDate = null;
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
                ModifyTime = null;
        }
        if (FCode.equalsIgnoreCase("GrpPrtSeq")) {
            if( FValue != null && !FValue.equals(""))
            {
                GrpPrtSeq = FValue.trim();
            }
            else
                GrpPrtSeq = null;
        }
        if (FCode.equalsIgnoreCase("ReMark")) {
            if( FValue != null && !FValue.equals(""))
            {
                ReMark = FValue.trim();
            }
            else
                ReMark = null;
        }
        if (FCode.equalsIgnoreCase("RReportReason")) {
            if( FValue != null && !FValue.equals(""))
            {
                RReportReason = FValue.trim();
            }
            else
                RReportReason = null;
        }
        if (FCode.equalsIgnoreCase("RReportFee")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                RReportFee = i;
            }
        }
        if (FCode.equalsIgnoreCase("NeedPrint")) {
            if( FValue != null && !FValue.equals(""))
            {
                NeedPrint = FValue.trim();
            }
            else
                NeedPrint = null;
        }
        return true;
    }


    public String toString() {
    return "LCRReportPojo [" +
            "GrpContNo="+GrpContNo +
            ", ContNo="+ContNo +
            ", ProposalContNo="+ProposalContNo +
            ", PrtSeq="+PrtSeq +
            ", AppntNo="+AppntNo +
            ", AppntName="+AppntName +
            ", CustomerNo="+CustomerNo +
            ", Name="+Name +
            ", ManageCom="+ManageCom +
            ", Contente="+Contente +
            ", ReplyContente="+ReplyContente +
            ", ReplyFlag="+ReplyFlag +
            ", Operator="+Operator +
            ", ReplyOperator="+ReplyOperator +
            ", ReplyDate="+ReplyDate +
            ", ReplyTime="+ReplyTime +
            ", MakeDate="+MakeDate +
            ", MakeTime="+MakeTime +
            ", ModifyDate="+ModifyDate +
            ", ModifyTime="+ModifyTime +
            ", GrpPrtSeq="+GrpPrtSeq +
            ", ReMark="+ReMark +
            ", RReportReason="+RReportReason +
            ", RReportFee="+RReportFee +
            ", NeedPrint="+NeedPrint +"]";
    }
}
