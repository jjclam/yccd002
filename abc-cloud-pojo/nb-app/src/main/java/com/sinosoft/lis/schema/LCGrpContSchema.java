/**
 * Copyright (c) 2018 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import com.sinosoft.lis.db.LCGrpContDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.*;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

/**
 * <p>ClassName: LCGrpContSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2018</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2018-05-23
 */
public class LCGrpContSchema implements Schema, Cloneable {
    // @Field
    /** 集体合同号码 */
    private String GrpContNo;
    /** 集体投保单号码 */
    private String ProposalGrpContNo;
    /** 印刷号码 */
    private String PrtNo;
    /** 销售渠道 */
    private String SaleChnl;
    /** 销售方式 */
    private String SalesWay;
    /** 销售方式子类 */
    private String SalesWaySub;
    /** 业务性质 */
    private String BizNature;
    /** 出单平台 */
    private String PolIssuPlat;
    /** 总单类型 */
    private String ContType;
    /** 代理机构 */
    private String AgentCom;
    /** 代理人编码 */
    private String AgentCode;
    /** 代理人组别 */
    private String AgentGroup;
    /** 联合代理人代码 */
    private String AgentCode1;
    /** 客户号码 */
    private String AppntNo;
    /** 投保总人数 */
    private int Peoples2;
    /** 单位名称 */
    private String GrpName;
    /** 合同争议处理方式 */
    private String DisputedFlag;
    /** 溢交处理方式 */
    private String OutPayFlag;
    /** 语种标记 */
    private String Lang;
    /** 币别 */
    private String Currency;
    /** 遗失补发次数 */
    private int LostTimes;
    /** 保单打印次数 */
    private int PrintCount;
    /** 最后一次保全日期 */
    private Date LastEdorDate;
    /** 团体特殊业务标志 */
    private String SpecFlag;
    /** 签单机构 */
    private String SignCom;
    /** 签单日期 */
    private Date SignDate;
    /** 签单时间 */
    private String SignTime;
    /** 保单生效日期 */
    private Date CValiDate;
    /** 交费间隔 */
    private int PayIntv;
    /** 管理费比例 */
    private double ManageFeeRate;
    /** 预计人数 */
    private int ExpPeoples;
    /** 预计保费 */
    private double ExpPremium;
    /** 预计保额 */
    private double ExpAmnt;
    /** 总人数 */
    private int Peoples;
    /** 总档次 */
    private double Mult;
    /** 总保费 */
    private double Prem;
    /** 总保额 */
    private double Amnt;
    /** 总累计保费 */
    private double SumPrem;
    /** 总累计交费 */
    private double SumPay;
    /** 差额 */
    private double Dif;
    /** 备用属性字段1 */
    private String StandbyFlag1;
    /** 备用属性字段2 */
    private String StandbyFlag2;
    /** 备用属性字段3 */
    private String StandbyFlag3;
    /** 录单人 */
    private String InputOperator;
    /** 录单完成日期 */
    private Date InputDate;
    /** 录单完成时间 */
    private String InputTime;
    /** 复核状态 */
    private String ApproveFlag;
    /** 复核人编码 */
    private String ApproveCode;
    /** 复核日期 */
    private Date ApproveDate;
    /** 复核时间 */
    private String ApproveTime;
    /** 核保人 */
    private String UWOperator;
    /** 核保状态 */
    private String UWFlag;
    /** 核保完成日期 */
    private Date UWDate;
    /** 核保完成时间 */
    private String UWTime;
    /** 投保单/保单标志 */
    private String AppFlag;
    /** 状态 */
    private String State;
    /** 投保单申请日期 */
    private Date PolApplyDate;
    /** 保单回执客户签收日期 */
    private Date CustomGetPolDate;
    /** 保单送达日期 */
    private Date GetPolDate;
    /** 管理机构 */
    private String ManageCom;
    /** 公司代码 */
    private String ComCode;
    /** 操作员 */
    private String Operator;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 最后一次修改操作员 */
    private String ModifyOperator;
    /** 最后一次修改日期 */
    private Date ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime;
    /** 参保形式 */
    private String EnterKind;
    /** 保障层级划分标准 */
    private String AmntGrade;
    /** 初审人 */
    private String FirstTrialOperator;
    /** 初审日期 */
    private Date FirstTrialDate;
    /** 初审时间 */
    private String FirstTrialTime;
    /** 收单人 */
    private String ReceiveOperator;
    /** 收单日期 */
    private Date ReceiveDate;
    /** 收单时间 */
    private String ReceiveTime;
    /** 市场类型 */
    private String MarketType;
    /** 呈报号 */
    private String ReportNo;
    /** 是否使用保全特殊算法 */
    private String EdorCalType;
    /** 续保标识 */
    private String RenewFlag;
    /** 续保保单原始保单号 */
    private String RenewContNo;
    /** 终止日期 */
    private Date EndDate;
    /** 是否共保 */
    private String Coinsurance;
    /** 卡单 */
    private String CardTypeCode;
    /** 与被保人关系 */
    private String RelationToappnt;
    /** 临分标记 */
    private String NewReinsureFlag;
    /** 投保比例 */
    private long InsureRate;
    /** 中介代理人编码 */
    private String AgcAgentCode;
    /** 中介代理人姓名 */
    private String AgcAgentName;
    /** 约定生效日期 */
    private String AvailiaDateFlag;
    /** 共保业务类型 */
    private String ComBusType;
    /** 是否打印保单 */
    private String PrintFlag;
    /** 特殊承保标记 */
    private String SpecAcceptFlag;
    /** 投保类型 */
    private String PolicyType;
    /** 是否计业绩 */
    private String AchvAccruFlag;
    /** 备注 */
    private String Remark;
    /** 生效日期类型 */
    private String ValDateType;
    /** 保障层级划分其他说明 */
    private String SecLevelOther;
    /** 出单方式 */
    private String TBType;
    /** 保单形式 */
    private String SlipForm;

    public static final int FIELDNUM = 99;    // 数据库表的字段个数

    private static String[] PK;                // 主键

    private FDate fDate = new FDate();        // 处理日期

    public CErrors mErrors;            // 错误信息

    // @Constructor
    public LCGrpContSchema() {
        mErrors = new CErrors();

        String[] pk = new String[1];
        pk[0] = "GrpContNo";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException {
        LCGrpContSchema cloned = (LCGrpContSchema)super.clone();
        cloned.fDate = (FDate) fDate.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK() {
        return PK;
    }

    public String getGrpContNo() {
        return GrpContNo;
    }
    public void setGrpContNo(String aGrpContNo) {
        GrpContNo = aGrpContNo;
    }
    public String getProposalGrpContNo() {
        return ProposalGrpContNo;
    }
    public void setProposalGrpContNo(String aProposalGrpContNo) {
        ProposalGrpContNo = aProposalGrpContNo;
    }
    public String getPrtNo() {
        return PrtNo;
    }
    public void setPrtNo(String aPrtNo) {
        PrtNo = aPrtNo;
    }
    public String getSaleChnl() {
        return SaleChnl;
    }
    public void setSaleChnl(String aSaleChnl) {
        SaleChnl = aSaleChnl;
    }
    public String getSalesWay() {
        return SalesWay;
    }
    public void setSalesWay(String aSalesWay) {
        SalesWay = aSalesWay;
    }
    public String getSalesWaySub() {
        return SalesWaySub;
    }
    public void setSalesWaySub(String aSalesWaySub) {
        SalesWaySub = aSalesWaySub;
    }
    public String getBizNature() {
        return BizNature;
    }
    public void setBizNature(String aBizNature) {
        BizNature = aBizNature;
    }
    public String getPolIssuPlat() {
        return PolIssuPlat;
    }
    public void setPolIssuPlat(String aPolIssuPlat) {
        PolIssuPlat = aPolIssuPlat;
    }
    public String getContType() {
        return ContType;
    }
    public void setContType(String aContType) {
        ContType = aContType;
    }
    public String getAgentCom() {
        return AgentCom;
    }
    public void setAgentCom(String aAgentCom) {
        AgentCom = aAgentCom;
    }
    public String getAgentCode() {
        return AgentCode;
    }
    public void setAgentCode(String aAgentCode) {
        AgentCode = aAgentCode;
    }
    public String getAgentGroup() {
        return AgentGroup;
    }
    public void setAgentGroup(String aAgentGroup) {
        AgentGroup = aAgentGroup;
    }
    public String getAgentCode1() {
        return AgentCode1;
    }
    public void setAgentCode1(String aAgentCode1) {
        AgentCode1 = aAgentCode1;
    }
    public String getAppntNo() {
        return AppntNo;
    }
    public void setAppntNo(String aAppntNo) {
        AppntNo = aAppntNo;
    }
    public int getPeoples2() {
        return Peoples2;
    }
    public void setPeoples2(int aPeoples2) {
        Peoples2 = aPeoples2;
    }
    public void setPeoples2(String aPeoples2) {
        if (aPeoples2 != null && !aPeoples2.equals("")) {
            Integer tInteger = new Integer(aPeoples2);
            int i = tInteger.intValue();
            Peoples2 = i;
        }
    }

    public String getGrpName() {
        return GrpName;
    }
    public void setGrpName(String aGrpName) {
        GrpName = aGrpName;
    }
    public String getDisputedFlag() {
        return DisputedFlag;
    }
    public void setDisputedFlag(String aDisputedFlag) {
        DisputedFlag = aDisputedFlag;
    }
    public String getOutPayFlag() {
        return OutPayFlag;
    }
    public void setOutPayFlag(String aOutPayFlag) {
        OutPayFlag = aOutPayFlag;
    }
    public String getLang() {
        return Lang;
    }
    public void setLang(String aLang) {
        Lang = aLang;
    }
    public String getCurrency() {
        return Currency;
    }
    public void setCurrency(String aCurrency) {
        Currency = aCurrency;
    }
    public int getLostTimes() {
        return LostTimes;
    }
    public void setLostTimes(int aLostTimes) {
        LostTimes = aLostTimes;
    }
    public void setLostTimes(String aLostTimes) {
        if (aLostTimes != null && !aLostTimes.equals("")) {
            Integer tInteger = new Integer(aLostTimes);
            int i = tInteger.intValue();
            LostTimes = i;
        }
    }

    public int getPrintCount() {
        return PrintCount;
    }
    public void setPrintCount(int aPrintCount) {
        PrintCount = aPrintCount;
    }
    public void setPrintCount(String aPrintCount) {
        if (aPrintCount != null && !aPrintCount.equals("")) {
            Integer tInteger = new Integer(aPrintCount);
            int i = tInteger.intValue();
            PrintCount = i;
        }
    }

    public String getLastEdorDate() {
        if(LastEdorDate != null) {
            return fDate.getString(LastEdorDate);
        } else {
            return null;
        }
    }
    public void setLastEdorDate(Date aLastEdorDate) {
        LastEdorDate = aLastEdorDate;
    }
    public void setLastEdorDate(String aLastEdorDate) {
        if (aLastEdorDate != null && !aLastEdorDate.equals("")) {
            LastEdorDate = fDate.getDate(aLastEdorDate);
        } else
            LastEdorDate = null;
    }

    public String getSpecFlag() {
        return SpecFlag;
    }
    public void setSpecFlag(String aSpecFlag) {
        SpecFlag = aSpecFlag;
    }
    public String getSignCom() {
        return SignCom;
    }
    public void setSignCom(String aSignCom) {
        SignCom = aSignCom;
    }
    public String getSignDate() {
        if(SignDate != null) {
            return fDate.getString(SignDate);
        } else {
            return null;
        }
    }
    public void setSignDate(Date aSignDate) {
        SignDate = aSignDate;
    }
    public void setSignDate(String aSignDate) {
        if (aSignDate != null && !aSignDate.equals("")) {
            SignDate = fDate.getDate(aSignDate);
        } else
            SignDate = null;
    }

    public String getSignTime() {
        return SignTime;
    }
    public void setSignTime(String aSignTime) {
        SignTime = aSignTime;
    }
    public String getCValiDate() {
        if(CValiDate != null) {
            return fDate.getString(CValiDate);
        } else {
            return null;
        }
    }
    public void setCValiDate(Date aCValiDate) {
        CValiDate = aCValiDate;
    }
    public void setCValiDate(String aCValiDate) {
        if (aCValiDate != null && !aCValiDate.equals("")) {
            CValiDate = fDate.getDate(aCValiDate);
        } else
            CValiDate = null;
    }

    public int getPayIntv() {
        return PayIntv;
    }
    public void setPayIntv(int aPayIntv) {
        PayIntv = aPayIntv;
    }
    public void setPayIntv(String aPayIntv) {
        if (aPayIntv != null && !aPayIntv.equals("")) {
            Integer tInteger = new Integer(aPayIntv);
            int i = tInteger.intValue();
            PayIntv = i;
        }
    }

    public double getManageFeeRate() {
        return ManageFeeRate;
    }
    public void setManageFeeRate(double aManageFeeRate) {
        ManageFeeRate = aManageFeeRate;
    }
    public void setManageFeeRate(String aManageFeeRate) {
        if (aManageFeeRate != null && !aManageFeeRate.equals("")) {
            Double tDouble = new Double(aManageFeeRate);
            double d = tDouble.doubleValue();
            ManageFeeRate = d;
        }
    }

    public int getExpPeoples() {
        return ExpPeoples;
    }
    public void setExpPeoples(int aExpPeoples) {
        ExpPeoples = aExpPeoples;
    }
    public void setExpPeoples(String aExpPeoples) {
        if (aExpPeoples != null && !aExpPeoples.equals("")) {
            Integer tInteger = new Integer(aExpPeoples);
            int i = tInteger.intValue();
            ExpPeoples = i;
        }
    }

    public double getExpPremium() {
        return ExpPremium;
    }
    public void setExpPremium(double aExpPremium) {
        ExpPremium = aExpPremium;
    }
    public void setExpPremium(String aExpPremium) {
        if (aExpPremium != null && !aExpPremium.equals("")) {
            Double tDouble = new Double(aExpPremium);
            double d = tDouble.doubleValue();
            ExpPremium = d;
        }
    }

    public double getExpAmnt() {
        return ExpAmnt;
    }
    public void setExpAmnt(double aExpAmnt) {
        ExpAmnt = aExpAmnt;
    }
    public void setExpAmnt(String aExpAmnt) {
        if (aExpAmnt != null && !aExpAmnt.equals("")) {
            Double tDouble = new Double(aExpAmnt);
            double d = tDouble.doubleValue();
            ExpAmnt = d;
        }
    }

    public int getPeoples() {
        return Peoples;
    }
    public void setPeoples(int aPeoples) {
        Peoples = aPeoples;
    }
    public void setPeoples(String aPeoples) {
        if (aPeoples != null && !aPeoples.equals("")) {
            Integer tInteger = new Integer(aPeoples);
            int i = tInteger.intValue();
            Peoples = i;
        }
    }

    public double getMult() {
        return Mult;
    }
    public void setMult(double aMult) {
        Mult = aMult;
    }
    public void setMult(String aMult) {
        if (aMult != null && !aMult.equals("")) {
            Double tDouble = new Double(aMult);
            double d = tDouble.doubleValue();
            Mult = d;
        }
    }

    public double getPrem() {
        return Prem;
    }
    public void setPrem(double aPrem) {
        Prem = aPrem;
    }
    public void setPrem(String aPrem) {
        if (aPrem != null && !aPrem.equals("")) {
            Double tDouble = new Double(aPrem);
            double d = tDouble.doubleValue();
            Prem = d;
        }
    }

    public double getAmnt() {
        return Amnt;
    }
    public void setAmnt(double aAmnt) {
        Amnt = aAmnt;
    }
    public void setAmnt(String aAmnt) {
        if (aAmnt != null && !aAmnt.equals("")) {
            Double tDouble = new Double(aAmnt);
            double d = tDouble.doubleValue();
            Amnt = d;
        }
    }

    public double getSumPrem() {
        return SumPrem;
    }
    public void setSumPrem(double aSumPrem) {
        SumPrem = aSumPrem;
    }
    public void setSumPrem(String aSumPrem) {
        if (aSumPrem != null && !aSumPrem.equals("")) {
            Double tDouble = new Double(aSumPrem);
            double d = tDouble.doubleValue();
            SumPrem = d;
        }
    }

    public double getSumPay() {
        return SumPay;
    }
    public void setSumPay(double aSumPay) {
        SumPay = aSumPay;
    }
    public void setSumPay(String aSumPay) {
        if (aSumPay != null && !aSumPay.equals("")) {
            Double tDouble = new Double(aSumPay);
            double d = tDouble.doubleValue();
            SumPay = d;
        }
    }

    public double getDif() {
        return Dif;
    }
    public void setDif(double aDif) {
        Dif = aDif;
    }
    public void setDif(String aDif) {
        if (aDif != null && !aDif.equals("")) {
            Double tDouble = new Double(aDif);
            double d = tDouble.doubleValue();
            Dif = d;
        }
    }

    public String getStandbyFlag1() {
        return StandbyFlag1;
    }
    public void setStandbyFlag1(String aStandbyFlag1) {
        StandbyFlag1 = aStandbyFlag1;
    }
    public String getStandbyFlag2() {
        return StandbyFlag2;
    }
    public void setStandbyFlag2(String aStandbyFlag2) {
        StandbyFlag2 = aStandbyFlag2;
    }
    public String getStandbyFlag3() {
        return StandbyFlag3;
    }
    public void setStandbyFlag3(String aStandbyFlag3) {
        StandbyFlag3 = aStandbyFlag3;
    }
    public String getInputOperator() {
        return InputOperator;
    }
    public void setInputOperator(String aInputOperator) {
        InputOperator = aInputOperator;
    }
    public String getInputDate() {
        if(InputDate != null) {
            return fDate.getString(InputDate);
        } else {
            return null;
        }
    }
    public void setInputDate(Date aInputDate) {
        InputDate = aInputDate;
    }
    public void setInputDate(String aInputDate) {
        if (aInputDate != null && !aInputDate.equals("")) {
            InputDate = fDate.getDate(aInputDate);
        } else
            InputDate = null;
    }

    public String getInputTime() {
        return InputTime;
    }
    public void setInputTime(String aInputTime) {
        InputTime = aInputTime;
    }
    public String getApproveFlag() {
        return ApproveFlag;
    }
    public void setApproveFlag(String aApproveFlag) {
        ApproveFlag = aApproveFlag;
    }
    public String getApproveCode() {
        return ApproveCode;
    }
    public void setApproveCode(String aApproveCode) {
        ApproveCode = aApproveCode;
    }
    public String getApproveDate() {
        if(ApproveDate != null) {
            return fDate.getString(ApproveDate);
        } else {
            return null;
        }
    }
    public void setApproveDate(Date aApproveDate) {
        ApproveDate = aApproveDate;
    }
    public void setApproveDate(String aApproveDate) {
        if (aApproveDate != null && !aApproveDate.equals("")) {
            ApproveDate = fDate.getDate(aApproveDate);
        } else
            ApproveDate = null;
    }

    public String getApproveTime() {
        return ApproveTime;
    }
    public void setApproveTime(String aApproveTime) {
        ApproveTime = aApproveTime;
    }
    public String getUWOperator() {
        return UWOperator;
    }
    public void setUWOperator(String aUWOperator) {
        UWOperator = aUWOperator;
    }
    public String getUWFlag() {
        return UWFlag;
    }
    public void setUWFlag(String aUWFlag) {
        UWFlag = aUWFlag;
    }
    public String getUWDate() {
        if(UWDate != null) {
            return fDate.getString(UWDate);
        } else {
            return null;
        }
    }
    public void setUWDate(Date aUWDate) {
        UWDate = aUWDate;
    }
    public void setUWDate(String aUWDate) {
        if (aUWDate != null && !aUWDate.equals("")) {
            UWDate = fDate.getDate(aUWDate);
        } else
            UWDate = null;
    }

    public String getUWTime() {
        return UWTime;
    }
    public void setUWTime(String aUWTime) {
        UWTime = aUWTime;
    }
    public String getAppFlag() {
        return AppFlag;
    }
    public void setAppFlag(String aAppFlag) {
        AppFlag = aAppFlag;
    }
    public String getState() {
        return State;
    }
    public void setState(String aState) {
        State = aState;
    }
    public String getPolApplyDate() {
        if(PolApplyDate != null) {
            return fDate.getString(PolApplyDate);
        } else {
            return null;
        }
    }
    public void setPolApplyDate(Date aPolApplyDate) {
        PolApplyDate = aPolApplyDate;
    }
    public void setPolApplyDate(String aPolApplyDate) {
        if (aPolApplyDate != null && !aPolApplyDate.equals("")) {
            PolApplyDate = fDate.getDate(aPolApplyDate);
        } else
            PolApplyDate = null;
    }

    public String getCustomGetPolDate() {
        if(CustomGetPolDate != null) {
            return fDate.getString(CustomGetPolDate);
        } else {
            return null;
        }
    }
    public void setCustomGetPolDate(Date aCustomGetPolDate) {
        CustomGetPolDate = aCustomGetPolDate;
    }
    public void setCustomGetPolDate(String aCustomGetPolDate) {
        if (aCustomGetPolDate != null && !aCustomGetPolDate.equals("")) {
            CustomGetPolDate = fDate.getDate(aCustomGetPolDate);
        } else
            CustomGetPolDate = null;
    }

    public String getGetPolDate() {
        if(GetPolDate != null) {
            return fDate.getString(GetPolDate);
        } else {
            return null;
        }
    }
    public void setGetPolDate(Date aGetPolDate) {
        GetPolDate = aGetPolDate;
    }
    public void setGetPolDate(String aGetPolDate) {
        if (aGetPolDate != null && !aGetPolDate.equals("")) {
            GetPolDate = fDate.getDate(aGetPolDate);
        } else
            GetPolDate = null;
    }

    public String getManageCom() {
        return ManageCom;
    }
    public void setManageCom(String aManageCom) {
        ManageCom = aManageCom;
    }
    public String getComCode() {
        return ComCode;
    }
    public void setComCode(String aComCode) {
        ComCode = aComCode;
    }
    public String getOperator() {
        return Operator;
    }
    public void setOperator(String aOperator) {
        Operator = aOperator;
    }
    public String getMakeDate() {
        if(MakeDate != null) {
            return fDate.getString(MakeDate);
        } else {
            return null;
        }
    }
    public void setMakeDate(Date aMakeDate) {
        MakeDate = aMakeDate;
    }
    public void setMakeDate(String aMakeDate) {
        if (aMakeDate != null && !aMakeDate.equals("")) {
            MakeDate = fDate.getDate(aMakeDate);
        } else
            MakeDate = null;
    }

    public String getMakeTime() {
        return MakeTime;
    }
    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }
    public String getModifyOperator() {
        return ModifyOperator;
    }
    public void setModifyOperator(String aModifyOperator) {
        ModifyOperator = aModifyOperator;
    }
    public String getModifyDate() {
        if(ModifyDate != null) {
            return fDate.getString(ModifyDate);
        } else {
            return null;
        }
    }
    public void setModifyDate(Date aModifyDate) {
        ModifyDate = aModifyDate;
    }
    public void setModifyDate(String aModifyDate) {
        if (aModifyDate != null && !aModifyDate.equals("")) {
            ModifyDate = fDate.getDate(aModifyDate);
        } else
            ModifyDate = null;
    }

    public String getModifyTime() {
        return ModifyTime;
    }
    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }
    public String getEnterKind() {
        return EnterKind;
    }
    public void setEnterKind(String aEnterKind) {
        EnterKind = aEnterKind;
    }
    public String getAmntGrade() {
        return AmntGrade;
    }
    public void setAmntGrade(String aAmntGrade) {
        AmntGrade = aAmntGrade;
    }
    public String getFirstTrialOperator() {
        return FirstTrialOperator;
    }
    public void setFirstTrialOperator(String aFirstTrialOperator) {
        FirstTrialOperator = aFirstTrialOperator;
    }
    public String getFirstTrialDate() {
        if(FirstTrialDate != null) {
            return fDate.getString(FirstTrialDate);
        } else {
            return null;
        }
    }
    public void setFirstTrialDate(Date aFirstTrialDate) {
        FirstTrialDate = aFirstTrialDate;
    }
    public void setFirstTrialDate(String aFirstTrialDate) {
        if (aFirstTrialDate != null && !aFirstTrialDate.equals("")) {
            FirstTrialDate = fDate.getDate(aFirstTrialDate);
        } else
            FirstTrialDate = null;
    }

    public String getFirstTrialTime() {
        return FirstTrialTime;
    }
    public void setFirstTrialTime(String aFirstTrialTime) {
        FirstTrialTime = aFirstTrialTime;
    }
    public String getReceiveOperator() {
        return ReceiveOperator;
    }
    public void setReceiveOperator(String aReceiveOperator) {
        ReceiveOperator = aReceiveOperator;
    }
    public String getReceiveDate() {
        if(ReceiveDate != null) {
            return fDate.getString(ReceiveDate);
        } else {
            return null;
        }
    }
    public void setReceiveDate(Date aReceiveDate) {
        ReceiveDate = aReceiveDate;
    }
    public void setReceiveDate(String aReceiveDate) {
        if (aReceiveDate != null && !aReceiveDate.equals("")) {
            ReceiveDate = fDate.getDate(aReceiveDate);
        } else
            ReceiveDate = null;
    }

    public String getReceiveTime() {
        return ReceiveTime;
    }
    public void setReceiveTime(String aReceiveTime) {
        ReceiveTime = aReceiveTime;
    }
    public String getMarketType() {
        return MarketType;
    }
    public void setMarketType(String aMarketType) {
        MarketType = aMarketType;
    }
    public String getReportNo() {
        return ReportNo;
    }
    public void setReportNo(String aReportNo) {
        ReportNo = aReportNo;
    }
    public String getEdorCalType() {
        return EdorCalType;
    }
    public void setEdorCalType(String aEdorCalType) {
        EdorCalType = aEdorCalType;
    }
    public String getRenewFlag() {
        return RenewFlag;
    }
    public void setRenewFlag(String aRenewFlag) {
        RenewFlag = aRenewFlag;
    }
    public String getRenewContNo() {
        return RenewContNo;
    }
    public void setRenewContNo(String aRenewContNo) {
        RenewContNo = aRenewContNo;
    }
    public String getEndDate() {
        if(EndDate != null) {
            return fDate.getString(EndDate);
        } else {
            return null;
        }
    }
    public void setEndDate(Date aEndDate) {
        EndDate = aEndDate;
    }
    public void setEndDate(String aEndDate) {
        if (aEndDate != null && !aEndDate.equals("")) {
            EndDate = fDate.getDate(aEndDate);
        } else
            EndDate = null;
    }

    public String getCoinsurance() {
        return Coinsurance;
    }
    public void setCoinsurance(String aCoinsurance) {
        Coinsurance = aCoinsurance;
    }
    public String getCardTypeCode() {
        return CardTypeCode;
    }
    public void setCardTypeCode(String aCardTypeCode) {
        CardTypeCode = aCardTypeCode;
    }
    public String getRelationToappnt() {
        return RelationToappnt;
    }
    public void setRelationToappnt(String aRelationToappnt) {
        RelationToappnt = aRelationToappnt;
    }
    public String getNewReinsureFlag() {
        return NewReinsureFlag;
    }
    public void setNewReinsureFlag(String aNewReinsureFlag) {
        NewReinsureFlag = aNewReinsureFlag;
    }
    public long getInsureRate() {
        return InsureRate;
    }
    public void setInsureRate(long aInsureRate) {
        InsureRate = aInsureRate;
    }
    public void setInsureRate(String aInsureRate) {
        if (aInsureRate != null && !aInsureRate.equals("")) {
            InsureRate = new Long(aInsureRate).longValue();
        }
    }

    public String getAgcAgentCode() {
        return AgcAgentCode;
    }
    public void setAgcAgentCode(String aAgcAgentCode) {
        AgcAgentCode = aAgcAgentCode;
    }
    public String getAgcAgentName() {
        return AgcAgentName;
    }
    public void setAgcAgentName(String aAgcAgentName) {
        AgcAgentName = aAgcAgentName;
    }
    public String getAvailiaDateFlag() {
        return AvailiaDateFlag;
    }
    public void setAvailiaDateFlag(String aAvailiaDateFlag) {
        AvailiaDateFlag = aAvailiaDateFlag;
    }
    public String getComBusType() {
        return ComBusType;
    }
    public void setComBusType(String aComBusType) {
        ComBusType = aComBusType;
    }
    public String getPrintFlag() {
        return PrintFlag;
    }
    public void setPrintFlag(String aPrintFlag) {
        PrintFlag = aPrintFlag;
    }
    public String getSpecAcceptFlag() {
        return SpecAcceptFlag;
    }
    public void setSpecAcceptFlag(String aSpecAcceptFlag) {
        SpecAcceptFlag = aSpecAcceptFlag;
    }
    public String getPolicyType() {
        return PolicyType;
    }
    public void setPolicyType(String aPolicyType) {
        PolicyType = aPolicyType;
    }
    public String getAchvAccruFlag() {
        return AchvAccruFlag;
    }
    public void setAchvAccruFlag(String aAchvAccruFlag) {
        AchvAccruFlag = aAchvAccruFlag;
    }
    public String getRemark() {
        return Remark;
    }
    public void setRemark(String aRemark) {
        Remark = aRemark;
    }
    public String getValDateType() {
        return ValDateType;
    }
    public void setValDateType(String aValDateType) {
        ValDateType = aValDateType;
    }
    public String getSecLevelOther() {
        return SecLevelOther;
    }
    public void setSecLevelOther(String aSecLevelOther) {
        SecLevelOther = aSecLevelOther;
    }
    public String getTBType() {
        return TBType;
    }
    public void setTBType(String aTBType) {
        TBType = aTBType;
    }
    public String getSlipForm() {
        return SlipForm;
    }
    public void setSlipForm(String aSlipForm) {
        SlipForm = aSlipForm;
    }

    /**
    * 使用另外一个 LCGrpContSchema 对象给 Schema 赋值
    * @param: aLCGrpContSchema LCGrpContSchema
    **/
    public void setSchema(LCGrpContSchema aLCGrpContSchema) {
        this.GrpContNo = aLCGrpContSchema.getGrpContNo();
        this.ProposalGrpContNo = aLCGrpContSchema.getProposalGrpContNo();
        this.PrtNo = aLCGrpContSchema.getPrtNo();
        this.SaleChnl = aLCGrpContSchema.getSaleChnl();
        this.SalesWay = aLCGrpContSchema.getSalesWay();
        this.SalesWaySub = aLCGrpContSchema.getSalesWaySub();
        this.BizNature = aLCGrpContSchema.getBizNature();
        this.PolIssuPlat = aLCGrpContSchema.getPolIssuPlat();
        this.ContType = aLCGrpContSchema.getContType();
        this.AgentCom = aLCGrpContSchema.getAgentCom();
        this.AgentCode = aLCGrpContSchema.getAgentCode();
        this.AgentGroup = aLCGrpContSchema.getAgentGroup();
        this.AgentCode1 = aLCGrpContSchema.getAgentCode1();
        this.AppntNo = aLCGrpContSchema.getAppntNo();
        this.Peoples2 = aLCGrpContSchema.getPeoples2();
        this.GrpName = aLCGrpContSchema.getGrpName();
        this.DisputedFlag = aLCGrpContSchema.getDisputedFlag();
        this.OutPayFlag = aLCGrpContSchema.getOutPayFlag();
        this.Lang = aLCGrpContSchema.getLang();
        this.Currency = aLCGrpContSchema.getCurrency();
        this.LostTimes = aLCGrpContSchema.getLostTimes();
        this.PrintCount = aLCGrpContSchema.getPrintCount();
        this.LastEdorDate = fDate.getDate( aLCGrpContSchema.getLastEdorDate());
        this.SpecFlag = aLCGrpContSchema.getSpecFlag();
        this.SignCom = aLCGrpContSchema.getSignCom();
        this.SignDate = fDate.getDate( aLCGrpContSchema.getSignDate());
        this.SignTime = aLCGrpContSchema.getSignTime();
        this.CValiDate = fDate.getDate( aLCGrpContSchema.getCValiDate());
        this.PayIntv = aLCGrpContSchema.getPayIntv();
        this.ManageFeeRate = aLCGrpContSchema.getManageFeeRate();
        this.ExpPeoples = aLCGrpContSchema.getExpPeoples();
        this.ExpPremium = aLCGrpContSchema.getExpPremium();
        this.ExpAmnt = aLCGrpContSchema.getExpAmnt();
        this.Peoples = aLCGrpContSchema.getPeoples();
        this.Mult = aLCGrpContSchema.getMult();
        this.Prem = aLCGrpContSchema.getPrem();
        this.Amnt = aLCGrpContSchema.getAmnt();
        this.SumPrem = aLCGrpContSchema.getSumPrem();
        this.SumPay = aLCGrpContSchema.getSumPay();
        this.Dif = aLCGrpContSchema.getDif();
        this.StandbyFlag1 = aLCGrpContSchema.getStandbyFlag1();
        this.StandbyFlag2 = aLCGrpContSchema.getStandbyFlag2();
        this.StandbyFlag3 = aLCGrpContSchema.getStandbyFlag3();
        this.InputOperator = aLCGrpContSchema.getInputOperator();
        this.InputDate = fDate.getDate( aLCGrpContSchema.getInputDate());
        this.InputTime = aLCGrpContSchema.getInputTime();
        this.ApproveFlag = aLCGrpContSchema.getApproveFlag();
        this.ApproveCode = aLCGrpContSchema.getApproveCode();
        this.ApproveDate = fDate.getDate( aLCGrpContSchema.getApproveDate());
        this.ApproveTime = aLCGrpContSchema.getApproveTime();
        this.UWOperator = aLCGrpContSchema.getUWOperator();
        this.UWFlag = aLCGrpContSchema.getUWFlag();
        this.UWDate = fDate.getDate( aLCGrpContSchema.getUWDate());
        this.UWTime = aLCGrpContSchema.getUWTime();
        this.AppFlag = aLCGrpContSchema.getAppFlag();
        this.State = aLCGrpContSchema.getState();
        this.PolApplyDate = fDate.getDate( aLCGrpContSchema.getPolApplyDate());
        this.CustomGetPolDate = fDate.getDate( aLCGrpContSchema.getCustomGetPolDate());
        this.GetPolDate = fDate.getDate( aLCGrpContSchema.getGetPolDate());
        this.ManageCom = aLCGrpContSchema.getManageCom();
        this.ComCode = aLCGrpContSchema.getComCode();
        this.Operator = aLCGrpContSchema.getOperator();
        this.MakeDate = fDate.getDate( aLCGrpContSchema.getMakeDate());
        this.MakeTime = aLCGrpContSchema.getMakeTime();
        this.ModifyOperator = aLCGrpContSchema.getModifyOperator();
        this.ModifyDate = fDate.getDate( aLCGrpContSchema.getModifyDate());
        this.ModifyTime = aLCGrpContSchema.getModifyTime();
        this.EnterKind = aLCGrpContSchema.getEnterKind();
        this.AmntGrade = aLCGrpContSchema.getAmntGrade();
        this.FirstTrialOperator = aLCGrpContSchema.getFirstTrialOperator();
        this.FirstTrialDate = fDate.getDate( aLCGrpContSchema.getFirstTrialDate());
        this.FirstTrialTime = aLCGrpContSchema.getFirstTrialTime();
        this.ReceiveOperator = aLCGrpContSchema.getReceiveOperator();
        this.ReceiveDate = fDate.getDate( aLCGrpContSchema.getReceiveDate());
        this.ReceiveTime = aLCGrpContSchema.getReceiveTime();
        this.MarketType = aLCGrpContSchema.getMarketType();
        this.ReportNo = aLCGrpContSchema.getReportNo();
        this.EdorCalType = aLCGrpContSchema.getEdorCalType();
        this.RenewFlag = aLCGrpContSchema.getRenewFlag();
        this.RenewContNo = aLCGrpContSchema.getRenewContNo();
        this.EndDate = fDate.getDate( aLCGrpContSchema.getEndDate());
        this.Coinsurance = aLCGrpContSchema.getCoinsurance();
        this.CardTypeCode = aLCGrpContSchema.getCardTypeCode();
        this.RelationToappnt = aLCGrpContSchema.getRelationToappnt();
        this.NewReinsureFlag = aLCGrpContSchema.getNewReinsureFlag();
        this.InsureRate = aLCGrpContSchema.getInsureRate();
        this.AgcAgentCode = aLCGrpContSchema.getAgcAgentCode();
        this.AgcAgentName = aLCGrpContSchema.getAgcAgentName();
        this.AvailiaDateFlag = aLCGrpContSchema.getAvailiaDateFlag();
        this.ComBusType = aLCGrpContSchema.getComBusType();
        this.PrintFlag = aLCGrpContSchema.getPrintFlag();
        this.SpecAcceptFlag = aLCGrpContSchema.getSpecAcceptFlag();
        this.PolicyType = aLCGrpContSchema.getPolicyType();
        this.AchvAccruFlag = aLCGrpContSchema.getAchvAccruFlag();
        this.Remark = aLCGrpContSchema.getRemark();
        this.ValDateType = aLCGrpContSchema.getValDateType();
        this.SecLevelOther = aLCGrpContSchema.getSecLevelOther();
        this.TBType = aLCGrpContSchema.getTBType();
        this.SlipForm = aLCGrpContSchema.getSlipForm();
    }

    /**
    * 使用 ResultSet 中的第 i 行给 Schema 赋值
    * @param: rs ResultSet
    * @param: i int
    * @return: boolean
    **/
    public boolean setSchema(ResultSet rs,int i) {
        try {
            //rs.absolute(i);        // 非滚动游标
            if( rs.getString("GrpContNo") == null )
                this.GrpContNo = null;
            else
                this.GrpContNo = rs.getString("GrpContNo").trim();

            if( rs.getString("ProposalGrpContNo") == null )
                this.ProposalGrpContNo = null;
            else
                this.ProposalGrpContNo = rs.getString("ProposalGrpContNo").trim();

            if( rs.getString("PrtNo") == null )
                this.PrtNo = null;
            else
                this.PrtNo = rs.getString("PrtNo").trim();

            if( rs.getString("SaleChnl") == null )
                this.SaleChnl = null;
            else
                this.SaleChnl = rs.getString("SaleChnl").trim();

            if( rs.getString("SalesWay") == null )
                this.SalesWay = null;
            else
                this.SalesWay = rs.getString("SalesWay").trim();

            if( rs.getString("SalesWaySub") == null )
                this.SalesWaySub = null;
            else
                this.SalesWaySub = rs.getString("SalesWaySub").trim();

            if( rs.getString("BizNature") == null )
                this.BizNature = null;
            else
                this.BizNature = rs.getString("BizNature").trim();

            if( rs.getString("PolIssuPlat") == null )
                this.PolIssuPlat = null;
            else
                this.PolIssuPlat = rs.getString("PolIssuPlat").trim();

            if( rs.getString("ContType") == null )
                this.ContType = null;
            else
                this.ContType = rs.getString("ContType").trim();

            if( rs.getString("AgentCom") == null )
                this.AgentCom = null;
            else
                this.AgentCom = rs.getString("AgentCom").trim();

            if( rs.getString("AgentCode") == null )
                this.AgentCode = null;
            else
                this.AgentCode = rs.getString("AgentCode").trim();

            if( rs.getString("AgentGroup") == null )
                this.AgentGroup = null;
            else
                this.AgentGroup = rs.getString("AgentGroup").trim();

            if( rs.getString("AgentCode1") == null )
                this.AgentCode1 = null;
            else
                this.AgentCode1 = rs.getString("AgentCode1").trim();

            if( rs.getString("AppntNo") == null )
                this.AppntNo = null;
            else
                this.AppntNo = rs.getString("AppntNo").trim();

            this.Peoples2 = rs.getInt("Peoples2");
            if( rs.getString("GrpName") == null )
                this.GrpName = null;
            else
                this.GrpName = rs.getString("GrpName").trim();

            if( rs.getString("DisputedFlag") == null )
                this.DisputedFlag = null;
            else
                this.DisputedFlag = rs.getString("DisputedFlag").trim();

            if( rs.getString("OutPayFlag") == null )
                this.OutPayFlag = null;
            else
                this.OutPayFlag = rs.getString("OutPayFlag").trim();

            if( rs.getString("Lang") == null )
                this.Lang = null;
            else
                this.Lang = rs.getString("Lang").trim();

            if( rs.getString("Currency") == null )
                this.Currency = null;
            else
                this.Currency = rs.getString("Currency").trim();

            this.LostTimes = rs.getInt("LostTimes");
            this.PrintCount = rs.getInt("PrintCount");
            this.LastEdorDate = rs.getDate("LastEdorDate");
            if( rs.getString("SpecFlag") == null )
                this.SpecFlag = null;
            else
                this.SpecFlag = rs.getString("SpecFlag").trim();

            if( rs.getString("SignCom") == null )
                this.SignCom = null;
            else
                this.SignCom = rs.getString("SignCom").trim();

            this.SignDate = rs.getDate("SignDate");
            if( rs.getString("SignTime") == null )
                this.SignTime = null;
            else
                this.SignTime = rs.getString("SignTime").trim();

            this.CValiDate = rs.getDate("CValiDate");
            this.PayIntv = rs.getInt("PayIntv");
            this.ManageFeeRate = rs.getDouble("ManageFeeRate");
            this.ExpPeoples = rs.getInt("ExpPeoples");
            this.ExpPremium = rs.getDouble("ExpPremium");
            this.ExpAmnt = rs.getDouble("ExpAmnt");
            this.Peoples = rs.getInt("Peoples");
            this.Mult = rs.getDouble("Mult");
            this.Prem = rs.getDouble("Prem");
            this.Amnt = rs.getDouble("Amnt");
            this.SumPrem = rs.getDouble("SumPrem");
            this.SumPay = rs.getDouble("SumPay");
            this.Dif = rs.getDouble("Dif");
            if( rs.getString("StandbyFlag1") == null )
                this.StandbyFlag1 = null;
            else
                this.StandbyFlag1 = rs.getString("StandbyFlag1").trim();

            if( rs.getString("StandbyFlag2") == null )
                this.StandbyFlag2 = null;
            else
                this.StandbyFlag2 = rs.getString("StandbyFlag2").trim();

            if( rs.getString("StandbyFlag3") == null )
                this.StandbyFlag3 = null;
            else
                this.StandbyFlag3 = rs.getString("StandbyFlag3").trim();

            if( rs.getString("InputOperator") == null )
                this.InputOperator = null;
            else
                this.InputOperator = rs.getString("InputOperator").trim();

            this.InputDate = rs.getDate("InputDate");
            if( rs.getString("InputTime") == null )
                this.InputTime = null;
            else
                this.InputTime = rs.getString("InputTime").trim();

            if( rs.getString("ApproveFlag") == null )
                this.ApproveFlag = null;
            else
                this.ApproveFlag = rs.getString("ApproveFlag").trim();

            if( rs.getString("ApproveCode") == null )
                this.ApproveCode = null;
            else
                this.ApproveCode = rs.getString("ApproveCode").trim();

            this.ApproveDate = rs.getDate("ApproveDate");
            if( rs.getString("ApproveTime") == null )
                this.ApproveTime = null;
            else
                this.ApproveTime = rs.getString("ApproveTime").trim();

            if( rs.getString("UWOperator") == null )
                this.UWOperator = null;
            else
                this.UWOperator = rs.getString("UWOperator").trim();

            if( rs.getString("UWFlag") == null )
                this.UWFlag = null;
            else
                this.UWFlag = rs.getString("UWFlag").trim();

            this.UWDate = rs.getDate("UWDate");
            if( rs.getString("UWTime") == null )
                this.UWTime = null;
            else
                this.UWTime = rs.getString("UWTime").trim();

            if( rs.getString("AppFlag") == null )
                this.AppFlag = null;
            else
                this.AppFlag = rs.getString("AppFlag").trim();

            if( rs.getString("State") == null )
                this.State = null;
            else
                this.State = rs.getString("State").trim();

            this.PolApplyDate = rs.getDate("PolApplyDate");
            this.CustomGetPolDate = rs.getDate("CustomGetPolDate");
            this.GetPolDate = rs.getDate("GetPolDate");
            if( rs.getString("ManageCom") == null )
                this.ManageCom = null;
            else
                this.ManageCom = rs.getString("ManageCom").trim();

            if( rs.getString("ComCode") == null )
                this.ComCode = null;
            else
                this.ComCode = rs.getString("ComCode").trim();

            if( rs.getString("Operator") == null )
                this.Operator = null;
            else
                this.Operator = rs.getString("Operator").trim();

            this.MakeDate = rs.getDate("MakeDate");
            if( rs.getString("MakeTime") == null )
                this.MakeTime = null;
            else
                this.MakeTime = rs.getString("MakeTime").trim();

            if( rs.getString("ModifyOperator") == null )
                this.ModifyOperator = null;
            else
                this.ModifyOperator = rs.getString("ModifyOperator").trim();

            this.ModifyDate = rs.getDate("ModifyDate");
            if( rs.getString("ModifyTime") == null )
                this.ModifyTime = null;
            else
                this.ModifyTime = rs.getString("ModifyTime").trim();

            if( rs.getString("EnterKind") == null )
                this.EnterKind = null;
            else
                this.EnterKind = rs.getString("EnterKind").trim();

            if( rs.getString("AmntGrade") == null )
                this.AmntGrade = null;
            else
                this.AmntGrade = rs.getString("AmntGrade").trim();

            if( rs.getString("FirstTrialOperator") == null )
                this.FirstTrialOperator = null;
            else
                this.FirstTrialOperator = rs.getString("FirstTrialOperator").trim();

            this.FirstTrialDate = rs.getDate("FirstTrialDate");
            if( rs.getString("FirstTrialTime") == null )
                this.FirstTrialTime = null;
            else
                this.FirstTrialTime = rs.getString("FirstTrialTime").trim();

            if( rs.getString("ReceiveOperator") == null )
                this.ReceiveOperator = null;
            else
                this.ReceiveOperator = rs.getString("ReceiveOperator").trim();

            this.ReceiveDate = rs.getDate("ReceiveDate");
            if( rs.getString("ReceiveTime") == null )
                this.ReceiveTime = null;
            else
                this.ReceiveTime = rs.getString("ReceiveTime").trim();

            if( rs.getString("MarketType") == null )
                this.MarketType = null;
            else
                this.MarketType = rs.getString("MarketType").trim();

            if( rs.getString("ReportNo") == null )
                this.ReportNo = null;
            else
                this.ReportNo = rs.getString("ReportNo").trim();

            if( rs.getString("EdorCalType") == null )
                this.EdorCalType = null;
            else
                this.EdorCalType = rs.getString("EdorCalType").trim();

            if( rs.getString("RenewFlag") == null )
                this.RenewFlag = null;
            else
                this.RenewFlag = rs.getString("RenewFlag").trim();

            if( rs.getString("RenewContNo") == null )
                this.RenewContNo = null;
            else
                this.RenewContNo = rs.getString("RenewContNo").trim();

            this.EndDate = rs.getDate("EndDate");
            if( rs.getString("Coinsurance") == null )
                this.Coinsurance = null;
            else
                this.Coinsurance = rs.getString("Coinsurance").trim();

            if( rs.getString("CardTypeCode") == null )
                this.CardTypeCode = null;
            else
                this.CardTypeCode = rs.getString("CardTypeCode").trim();

            if( rs.getString("RelationToappnt") == null )
                this.RelationToappnt = null;
            else
                this.RelationToappnt = rs.getString("RelationToappnt").trim();

            if( rs.getString("NewReinsureFlag") == null )
                this.NewReinsureFlag = null;
            else
                this.NewReinsureFlag = rs.getString("NewReinsureFlag").trim();

            this.InsureRate = rs.getLong("InsureRate");
            if( rs.getString("AgcAgentCode") == null )
                this.AgcAgentCode = null;
            else
                this.AgcAgentCode = rs.getString("AgcAgentCode").trim();

            if( rs.getString("AgcAgentName") == null )
                this.AgcAgentName = null;
            else
                this.AgcAgentName = rs.getString("AgcAgentName").trim();

            if( rs.getString("AvailiaDateFlag") == null )
                this.AvailiaDateFlag = null;
            else
                this.AvailiaDateFlag = rs.getString("AvailiaDateFlag").trim();

            if( rs.getString("ComBusType") == null )
                this.ComBusType = null;
            else
                this.ComBusType = rs.getString("ComBusType").trim();

            if( rs.getString("PrintFlag") == null )
                this.PrintFlag = null;
            else
                this.PrintFlag = rs.getString("PrintFlag").trim();

            if( rs.getString("SpecAcceptFlag") == null )
                this.SpecAcceptFlag = null;
            else
                this.SpecAcceptFlag = rs.getString("SpecAcceptFlag").trim();

            if( rs.getString("PolicyType") == null )
                this.PolicyType = null;
            else
                this.PolicyType = rs.getString("PolicyType").trim();

            if( rs.getString("AchvAccruFlag") == null )
                this.AchvAccruFlag = null;
            else
                this.AchvAccruFlag = rs.getString("AchvAccruFlag").trim();

            if( rs.getString("Remark") == null )
                this.Remark = null;
            else
                this.Remark = rs.getString("Remark").trim();

            if( rs.getString("ValDateType") == null )
                this.ValDateType = null;
            else
                this.ValDateType = rs.getString("ValDateType").trim();

            if( rs.getString("SecLevelOther") == null )
                this.SecLevelOther = null;
            else
                this.SecLevelOther = rs.getString("SecLevelOther").trim();

            if( rs.getString("TBType") == null )
                this.TBType = null;
            else
                this.TBType = rs.getString("TBType").trim();

            if( rs.getString("SlipForm") == null )
                this.SlipForm = null;
            else
                this.SlipForm = rs.getString("SlipForm").trim();

        }
        catch(SQLException sqle) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LCGrpContSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = ExceptionUtils.exceptionToString(sqle);
            this.mErrors .addOneError(tError);
            return false;
        }
        return true;
    }

    public LCGrpContSchema getSchema() {
        LCGrpContSchema aLCGrpContSchema = new LCGrpContSchema();
        aLCGrpContSchema.setSchema(this);
        return aLCGrpContSchema;
    }

    public LCGrpContDB getDB() {
        LCGrpContDB aDBOper = new LCGrpContDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
    * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLCGrpCont描述/A>表字段
    * @return: String 返回打包后字符串
    **/
    public String encode() {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(GrpContNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ProposalGrpContNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(PrtNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(SaleChnl)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(SalesWay)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(SalesWaySub)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(BizNature)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(PolIssuPlat)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ContType)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AgentCom)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AgentCode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AgentGroup)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AgentCode1)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AppntNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(Peoples2));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(GrpName)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(DisputedFlag)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(OutPayFlag)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Lang)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Currency)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(LostTimes));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(PrintCount));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( LastEdorDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(SpecFlag)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(SignCom)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( SignDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(SignTime)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( CValiDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(PayIntv));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(ManageFeeRate));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(ExpPeoples));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(ExpPremium));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(ExpAmnt));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(Peoples));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(Mult));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(Prem));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(Amnt));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(SumPrem));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(SumPay));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(Dif));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StandbyFlag1)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StandbyFlag2)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StandbyFlag3)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(InputOperator)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( InputDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(InputTime)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ApproveFlag)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ApproveCode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( ApproveDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ApproveTime)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(UWOperator)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(UWFlag)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( UWDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(UWTime)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AppFlag)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(State)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( PolApplyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( CustomGetPolDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( GetPolDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ManageCom)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ComCode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ModifyOperator)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ModifyTime)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(EnterKind)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AmntGrade)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(FirstTrialOperator)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( FirstTrialDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(FirstTrialTime)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ReceiveOperator)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( ReceiveDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ReceiveTime)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MarketType)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ReportNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(EdorCalType)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(RenewFlag)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(RenewContNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( EndDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Coinsurance)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(CardTypeCode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(RelationToappnt)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(NewReinsureFlag)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(InsureRate));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AgcAgentCode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AgcAgentName)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AvailiaDateFlag)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ComBusType)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(PrintFlag)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(SpecAcceptFlag)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(PolicyType)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AchvAccruFlag)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Remark)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ValDateType)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(SecLevelOther)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(TBType)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(SlipForm));
        return strReturn.toString();
    }

    /**
    * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLCGrpCont>历史记账凭证主表信息</A>表字段
    * @param: strMessage String 包含一条纪录数据的字符串
    * @return: boolean
    **/
    public boolean decode(String strMessage) {
        try {
            GrpContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
            ProposalGrpContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
            PrtNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
            SaleChnl = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
            SalesWay = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
            SalesWaySub = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
            BizNature = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
            PolIssuPlat = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
            ContType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
            AgentCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
            AgentCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
            AgentGroup = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
            AgentCode1 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
            AppntNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER );
            Peoples2 = new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,15, SysConst.PACKAGESPILTER))).intValue();
            GrpName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16, SysConst.PACKAGESPILTER );
            DisputedFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17, SysConst.PACKAGESPILTER );
            OutPayFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18, SysConst.PACKAGESPILTER );
            Lang = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19, SysConst.PACKAGESPILTER );
            Currency = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20, SysConst.PACKAGESPILTER );
            LostTimes = new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,21, SysConst.PACKAGESPILTER))).intValue();
            PrintCount = new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,22, SysConst.PACKAGESPILTER))).intValue();
            LastEdorDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 23, SysConst.PACKAGESPILTER));
            SpecFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 24, SysConst.PACKAGESPILTER );
            SignCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 25, SysConst.PACKAGESPILTER );
            SignDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 26, SysConst.PACKAGESPILTER));
            SignTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 27, SysConst.PACKAGESPILTER );
            CValiDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 28, SysConst.PACKAGESPILTER));
            PayIntv = new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,29, SysConst.PACKAGESPILTER))).intValue();
            ManageFeeRate = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,30, SysConst.PACKAGESPILTER))).doubleValue();
            ExpPeoples = new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,31, SysConst.PACKAGESPILTER))).intValue();
            ExpPremium = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,32, SysConst.PACKAGESPILTER))).doubleValue();
            ExpAmnt = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,33, SysConst.PACKAGESPILTER))).doubleValue();
            Peoples = new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,34, SysConst.PACKAGESPILTER))).intValue();
            Mult = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,35, SysConst.PACKAGESPILTER))).doubleValue();
            Prem = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,36, SysConst.PACKAGESPILTER))).doubleValue();
            Amnt = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,37, SysConst.PACKAGESPILTER))).doubleValue();
            SumPrem = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,38, SysConst.PACKAGESPILTER))).doubleValue();
            SumPay = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,39, SysConst.PACKAGESPILTER))).doubleValue();
            Dif = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,40, SysConst.PACKAGESPILTER))).doubleValue();
            StandbyFlag1 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 41, SysConst.PACKAGESPILTER );
            StandbyFlag2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 42, SysConst.PACKAGESPILTER );
            StandbyFlag3 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 43, SysConst.PACKAGESPILTER );
            InputOperator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 44, SysConst.PACKAGESPILTER );
            InputDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 45, SysConst.PACKAGESPILTER));
            InputTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 46, SysConst.PACKAGESPILTER );
            ApproveFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 47, SysConst.PACKAGESPILTER );
            ApproveCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 48, SysConst.PACKAGESPILTER );
            ApproveDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 49, SysConst.PACKAGESPILTER));
            ApproveTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 50, SysConst.PACKAGESPILTER );
            UWOperator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 51, SysConst.PACKAGESPILTER );
            UWFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 52, SysConst.PACKAGESPILTER );
            UWDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 53, SysConst.PACKAGESPILTER));
            UWTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 54, SysConst.PACKAGESPILTER );
            AppFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 55, SysConst.PACKAGESPILTER );
            State = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 56, SysConst.PACKAGESPILTER );
            PolApplyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 57, SysConst.PACKAGESPILTER));
            CustomGetPolDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 58, SysConst.PACKAGESPILTER));
            GetPolDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 59, SysConst.PACKAGESPILTER));
            ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 60, SysConst.PACKAGESPILTER );
            ComCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 61, SysConst.PACKAGESPILTER );
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 62, SysConst.PACKAGESPILTER );
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 63, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 64, SysConst.PACKAGESPILTER );
            ModifyOperator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 65, SysConst.PACKAGESPILTER );
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 66, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 67, SysConst.PACKAGESPILTER );
            EnterKind = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 68, SysConst.PACKAGESPILTER );
            AmntGrade = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 69, SysConst.PACKAGESPILTER );
            FirstTrialOperator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 70, SysConst.PACKAGESPILTER );
            FirstTrialDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 71, SysConst.PACKAGESPILTER));
            FirstTrialTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 72, SysConst.PACKAGESPILTER );
            ReceiveOperator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 73, SysConst.PACKAGESPILTER );
            ReceiveDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 74, SysConst.PACKAGESPILTER));
            ReceiveTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 75, SysConst.PACKAGESPILTER );
            MarketType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 76, SysConst.PACKAGESPILTER );
            ReportNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 77, SysConst.PACKAGESPILTER );
            EdorCalType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 78, SysConst.PACKAGESPILTER );
            RenewFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 79, SysConst.PACKAGESPILTER );
            RenewContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 80, SysConst.PACKAGESPILTER );
            EndDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 81, SysConst.PACKAGESPILTER));
            Coinsurance = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 82, SysConst.PACKAGESPILTER );
            CardTypeCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 83, SysConst.PACKAGESPILTER );
            RelationToappnt = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 84, SysConst.PACKAGESPILTER );
            NewReinsureFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 85, SysConst.PACKAGESPILTER );
            InsureRate = new Long(ChgData.chgNumericStr(StrTool.getStr(strMessage,86, SysConst.PACKAGESPILTER))).longValue();
            AgcAgentCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 87, SysConst.PACKAGESPILTER );
            AgcAgentName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 88, SysConst.PACKAGESPILTER );
            AvailiaDateFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 89, SysConst.PACKAGESPILTER );
            ComBusType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 90, SysConst.PACKAGESPILTER );
            PrintFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 91, SysConst.PACKAGESPILTER );
            SpecAcceptFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 92, SysConst.PACKAGESPILTER );
            PolicyType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 93, SysConst.PACKAGESPILTER );
            AchvAccruFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 94, SysConst.PACKAGESPILTER );
            Remark = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 95, SysConst.PACKAGESPILTER );
            ValDateType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 96, SysConst.PACKAGESPILTER );
            SecLevelOther = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 97, SysConst.PACKAGESPILTER );
            TBType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 98, SysConst.PACKAGESPILTER );
            SlipForm = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 99, SysConst.PACKAGESPILTER );
        }
        catch(NumberFormatException ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LCGrpContSchema";
            tError.functionName = "decode";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            return false;
        }
        return true;
    }

    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("GrpContNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GrpContNo));
        }
        if (FCode.equalsIgnoreCase("ProposalGrpContNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ProposalGrpContNo));
        }
        if (FCode.equalsIgnoreCase("PrtNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PrtNo));
        }
        if (FCode.equalsIgnoreCase("SaleChnl")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SaleChnl));
        }
        if (FCode.equalsIgnoreCase("SalesWay")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SalesWay));
        }
        if (FCode.equalsIgnoreCase("SalesWaySub")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SalesWaySub));
        }
        if (FCode.equalsIgnoreCase("BizNature")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BizNature));
        }
        if (FCode.equalsIgnoreCase("PolIssuPlat")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PolIssuPlat));
        }
        if (FCode.equalsIgnoreCase("ContType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ContType));
        }
        if (FCode.equalsIgnoreCase("AgentCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentCom));
        }
        if (FCode.equalsIgnoreCase("AgentCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentCode));
        }
        if (FCode.equalsIgnoreCase("AgentGroup")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentGroup));
        }
        if (FCode.equalsIgnoreCase("AgentCode1")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentCode1));
        }
        if (FCode.equalsIgnoreCase("AppntNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AppntNo));
        }
        if (FCode.equalsIgnoreCase("Peoples2")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Peoples2));
        }
        if (FCode.equalsIgnoreCase("GrpName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GrpName));
        }
        if (FCode.equalsIgnoreCase("DisputedFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DisputedFlag));
        }
        if (FCode.equalsIgnoreCase("OutPayFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OutPayFlag));
        }
        if (FCode.equalsIgnoreCase("Lang")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Lang));
        }
        if (FCode.equalsIgnoreCase("Currency")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Currency));
        }
        if (FCode.equalsIgnoreCase("LostTimes")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(LostTimes));
        }
        if (FCode.equalsIgnoreCase("PrintCount")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PrintCount));
        }
        if (FCode.equalsIgnoreCase("LastEdorDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getLastEdorDate()));
        }
        if (FCode.equalsIgnoreCase("SpecFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SpecFlag));
        }
        if (FCode.equalsIgnoreCase("SignCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SignCom));
        }
        if (FCode.equalsIgnoreCase("SignDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getSignDate()));
        }
        if (FCode.equalsIgnoreCase("SignTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SignTime));
        }
        if (FCode.equalsIgnoreCase("CValiDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getCValiDate()));
        }
        if (FCode.equalsIgnoreCase("PayIntv")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PayIntv));
        }
        if (FCode.equalsIgnoreCase("ManageFeeRate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ManageFeeRate));
        }
        if (FCode.equalsIgnoreCase("ExpPeoples")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ExpPeoples));
        }
        if (FCode.equalsIgnoreCase("ExpPremium")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ExpPremium));
        }
        if (FCode.equalsIgnoreCase("ExpAmnt")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ExpAmnt));
        }
        if (FCode.equalsIgnoreCase("Peoples")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Peoples));
        }
        if (FCode.equalsIgnoreCase("Mult")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Mult));
        }
        if (FCode.equalsIgnoreCase("Prem")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Prem));
        }
        if (FCode.equalsIgnoreCase("Amnt")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Amnt));
        }
        if (FCode.equalsIgnoreCase("SumPrem")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SumPrem));
        }
        if (FCode.equalsIgnoreCase("SumPay")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SumPay));
        }
        if (FCode.equalsIgnoreCase("Dif")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Dif));
        }
        if (FCode.equalsIgnoreCase("StandbyFlag1")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StandbyFlag1));
        }
        if (FCode.equalsIgnoreCase("StandbyFlag2")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StandbyFlag2));
        }
        if (FCode.equalsIgnoreCase("StandbyFlag3")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StandbyFlag3));
        }
        if (FCode.equalsIgnoreCase("InputOperator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InputOperator));
        }
        if (FCode.equalsIgnoreCase("InputDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getInputDate()));
        }
        if (FCode.equalsIgnoreCase("InputTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InputTime));
        }
        if (FCode.equalsIgnoreCase("ApproveFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ApproveFlag));
        }
        if (FCode.equalsIgnoreCase("ApproveCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ApproveCode));
        }
        if (FCode.equalsIgnoreCase("ApproveDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getApproveDate()));
        }
        if (FCode.equalsIgnoreCase("ApproveTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ApproveTime));
        }
        if (FCode.equalsIgnoreCase("UWOperator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(UWOperator));
        }
        if (FCode.equalsIgnoreCase("UWFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(UWFlag));
        }
        if (FCode.equalsIgnoreCase("UWDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getUWDate()));
        }
        if (FCode.equalsIgnoreCase("UWTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(UWTime));
        }
        if (FCode.equalsIgnoreCase("AppFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AppFlag));
        }
        if (FCode.equalsIgnoreCase("State")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(State));
        }
        if (FCode.equalsIgnoreCase("PolApplyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getPolApplyDate()));
        }
        if (FCode.equalsIgnoreCase("CustomGetPolDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getCustomGetPolDate()));
        }
        if (FCode.equalsIgnoreCase("GetPolDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getGetPolDate()));
        }
        if (FCode.equalsIgnoreCase("ManageCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
        }
        if (FCode.equalsIgnoreCase("ComCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ComCode));
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equalsIgnoreCase("ModifyOperator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyOperator));
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (FCode.equalsIgnoreCase("EnterKind")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(EnterKind));
        }
        if (FCode.equalsIgnoreCase("AmntGrade")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AmntGrade));
        }
        if (FCode.equalsIgnoreCase("FirstTrialOperator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FirstTrialOperator));
        }
        if (FCode.equalsIgnoreCase("FirstTrialDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getFirstTrialDate()));
        }
        if (FCode.equalsIgnoreCase("FirstTrialTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FirstTrialTime));
        }
        if (FCode.equalsIgnoreCase("ReceiveOperator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ReceiveOperator));
        }
        if (FCode.equalsIgnoreCase("ReceiveDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getReceiveDate()));
        }
        if (FCode.equalsIgnoreCase("ReceiveTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ReceiveTime));
        }
        if (FCode.equalsIgnoreCase("MarketType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MarketType));
        }
        if (FCode.equalsIgnoreCase("ReportNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ReportNo));
        }
        if (FCode.equalsIgnoreCase("EdorCalType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(EdorCalType));
        }
        if (FCode.equalsIgnoreCase("RenewFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RenewFlag));
        }
        if (FCode.equalsIgnoreCase("RenewContNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RenewContNo));
        }
        if (FCode.equalsIgnoreCase("EndDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getEndDate()));
        }
        if (FCode.equalsIgnoreCase("Coinsurance")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Coinsurance));
        }
        if (FCode.equalsIgnoreCase("CardTypeCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CardTypeCode));
        }
        if (FCode.equalsIgnoreCase("RelationToappnt")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RelationToappnt));
        }
        if (FCode.equalsIgnoreCase("NewReinsureFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(NewReinsureFlag));
        }
        if (FCode.equalsIgnoreCase("InsureRate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InsureRate));
        }
        if (FCode.equalsIgnoreCase("AgcAgentCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgcAgentCode));
        }
        if (FCode.equalsIgnoreCase("AgcAgentName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgcAgentName));
        }
        if (FCode.equalsIgnoreCase("AvailiaDateFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AvailiaDateFlag));
        }
        if (FCode.equalsIgnoreCase("ComBusType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ComBusType));
        }
        if (FCode.equalsIgnoreCase("PrintFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PrintFlag));
        }
        if (FCode.equalsIgnoreCase("SpecAcceptFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SpecAcceptFlag));
        }
        if (FCode.equalsIgnoreCase("PolicyType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PolicyType));
        }
        if (FCode.equalsIgnoreCase("AchvAccruFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AchvAccruFlag));
        }
        if (FCode.equalsIgnoreCase("Remark")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Remark));
        }
        if (FCode.equalsIgnoreCase("ValDateType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ValDateType));
        }
        if (FCode.equalsIgnoreCase("SecLevelOther")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SecLevelOther));
        }
        if (FCode.equalsIgnoreCase("TBType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(TBType));
        }
        if (FCode.equalsIgnoreCase("SlipForm")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SlipForm));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(GrpContNo);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(ProposalGrpContNo);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(PrtNo);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(SaleChnl);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(SalesWay);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(SalesWaySub);
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(BizNature);
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(PolIssuPlat);
                break;
            case 8:
                strFieldValue = StrTool.GBKToUnicode(ContType);
                break;
            case 9:
                strFieldValue = StrTool.GBKToUnicode(AgentCom);
                break;
            case 10:
                strFieldValue = StrTool.GBKToUnicode(AgentCode);
                break;
            case 11:
                strFieldValue = StrTool.GBKToUnicode(AgentGroup);
                break;
            case 12:
                strFieldValue = StrTool.GBKToUnicode(AgentCode1);
                break;
            case 13:
                strFieldValue = StrTool.GBKToUnicode(AppntNo);
                break;
            case 14:
                strFieldValue = String.valueOf(Peoples2);
                break;
            case 15:
                strFieldValue = StrTool.GBKToUnicode(GrpName);
                break;
            case 16:
                strFieldValue = StrTool.GBKToUnicode(DisputedFlag);
                break;
            case 17:
                strFieldValue = StrTool.GBKToUnicode(OutPayFlag);
                break;
            case 18:
                strFieldValue = StrTool.GBKToUnicode(Lang);
                break;
            case 19:
                strFieldValue = StrTool.GBKToUnicode(Currency);
                break;
            case 20:
                strFieldValue = String.valueOf(LostTimes);
                break;
            case 21:
                strFieldValue = String.valueOf(PrintCount);
                break;
            case 22:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getLastEdorDate()));
                break;
            case 23:
                strFieldValue = StrTool.GBKToUnicode(SpecFlag);
                break;
            case 24:
                strFieldValue = StrTool.GBKToUnicode(SignCom);
                break;
            case 25:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getSignDate()));
                break;
            case 26:
                strFieldValue = StrTool.GBKToUnicode(SignTime);
                break;
            case 27:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getCValiDate()));
                break;
            case 28:
                strFieldValue = String.valueOf(PayIntv);
                break;
            case 29:
                strFieldValue = String.valueOf(ManageFeeRate);
                break;
            case 30:
                strFieldValue = String.valueOf(ExpPeoples);
                break;
            case 31:
                strFieldValue = String.valueOf(ExpPremium);
                break;
            case 32:
                strFieldValue = String.valueOf(ExpAmnt);
                break;
            case 33:
                strFieldValue = String.valueOf(Peoples);
                break;
            case 34:
                strFieldValue = String.valueOf(Mult);
                break;
            case 35:
                strFieldValue = String.valueOf(Prem);
                break;
            case 36:
                strFieldValue = String.valueOf(Amnt);
                break;
            case 37:
                strFieldValue = String.valueOf(SumPrem);
                break;
            case 38:
                strFieldValue = String.valueOf(SumPay);
                break;
            case 39:
                strFieldValue = String.valueOf(Dif);
                break;
            case 40:
                strFieldValue = StrTool.GBKToUnicode(StandbyFlag1);
                break;
            case 41:
                strFieldValue = StrTool.GBKToUnicode(StandbyFlag2);
                break;
            case 42:
                strFieldValue = StrTool.GBKToUnicode(StandbyFlag3);
                break;
            case 43:
                strFieldValue = StrTool.GBKToUnicode(InputOperator);
                break;
            case 44:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getInputDate()));
                break;
            case 45:
                strFieldValue = StrTool.GBKToUnicode(InputTime);
                break;
            case 46:
                strFieldValue = StrTool.GBKToUnicode(ApproveFlag);
                break;
            case 47:
                strFieldValue = StrTool.GBKToUnicode(ApproveCode);
                break;
            case 48:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getApproveDate()));
                break;
            case 49:
                strFieldValue = StrTool.GBKToUnicode(ApproveTime);
                break;
            case 50:
                strFieldValue = StrTool.GBKToUnicode(UWOperator);
                break;
            case 51:
                strFieldValue = StrTool.GBKToUnicode(UWFlag);
                break;
            case 52:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getUWDate()));
                break;
            case 53:
                strFieldValue = StrTool.GBKToUnicode(UWTime);
                break;
            case 54:
                strFieldValue = StrTool.GBKToUnicode(AppFlag);
                break;
            case 55:
                strFieldValue = StrTool.GBKToUnicode(State);
                break;
            case 56:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getPolApplyDate()));
                break;
            case 57:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getCustomGetPolDate()));
                break;
            case 58:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getGetPolDate()));
                break;
            case 59:
                strFieldValue = StrTool.GBKToUnicode(ManageCom);
                break;
            case 60:
                strFieldValue = StrTool.GBKToUnicode(ComCode);
                break;
            case 61:
                strFieldValue = StrTool.GBKToUnicode(Operator);
                break;
            case 62:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
                break;
            case 63:
                strFieldValue = StrTool.GBKToUnicode(MakeTime);
                break;
            case 64:
                strFieldValue = StrTool.GBKToUnicode(ModifyOperator);
                break;
            case 65:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
                break;
            case 66:
                strFieldValue = StrTool.GBKToUnicode(ModifyTime);
                break;
            case 67:
                strFieldValue = StrTool.GBKToUnicode(EnterKind);
                break;
            case 68:
                strFieldValue = StrTool.GBKToUnicode(AmntGrade);
                break;
            case 69:
                strFieldValue = StrTool.GBKToUnicode(FirstTrialOperator);
                break;
            case 70:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getFirstTrialDate()));
                break;
            case 71:
                strFieldValue = StrTool.GBKToUnicode(FirstTrialTime);
                break;
            case 72:
                strFieldValue = StrTool.GBKToUnicode(ReceiveOperator);
                break;
            case 73:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getReceiveDate()));
                break;
            case 74:
                strFieldValue = StrTool.GBKToUnicode(ReceiveTime);
                break;
            case 75:
                strFieldValue = StrTool.GBKToUnicode(MarketType);
                break;
            case 76:
                strFieldValue = StrTool.GBKToUnicode(ReportNo);
                break;
            case 77:
                strFieldValue = StrTool.GBKToUnicode(EdorCalType);
                break;
            case 78:
                strFieldValue = StrTool.GBKToUnicode(RenewFlag);
                break;
            case 79:
                strFieldValue = StrTool.GBKToUnicode(RenewContNo);
                break;
            case 80:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getEndDate()));
                break;
            case 81:
                strFieldValue = StrTool.GBKToUnicode(Coinsurance);
                break;
            case 82:
                strFieldValue = StrTool.GBKToUnicode(CardTypeCode);
                break;
            case 83:
                strFieldValue = StrTool.GBKToUnicode(RelationToappnt);
                break;
            case 84:
                strFieldValue = StrTool.GBKToUnicode(NewReinsureFlag);
                break;
            case 85:
                strFieldValue = String.valueOf(InsureRate);
                break;
            case 86:
                strFieldValue = StrTool.GBKToUnicode(AgcAgentCode);
                break;
            case 87:
                strFieldValue = StrTool.GBKToUnicode(AgcAgentName);
                break;
            case 88:
                strFieldValue = StrTool.GBKToUnicode(AvailiaDateFlag);
                break;
            case 89:
                strFieldValue = StrTool.GBKToUnicode(ComBusType);
                break;
            case 90:
                strFieldValue = StrTool.GBKToUnicode(PrintFlag);
                break;
            case 91:
                strFieldValue = StrTool.GBKToUnicode(SpecAcceptFlag);
                break;
            case 92:
                strFieldValue = StrTool.GBKToUnicode(PolicyType);
                break;
            case 93:
                strFieldValue = StrTool.GBKToUnicode(AchvAccruFlag);
                break;
            case 94:
                strFieldValue = StrTool.GBKToUnicode(Remark);
                break;
            case 95:
                strFieldValue = StrTool.GBKToUnicode(ValDateType);
                break;
            case 96:
                strFieldValue = StrTool.GBKToUnicode(SecLevelOther);
                break;
            case 97:
                strFieldValue = StrTool.GBKToUnicode(TBType);
                break;
            case 98:
                strFieldValue = StrTool.GBKToUnicode(SlipForm);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("GrpContNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                GrpContNo = FValue.trim();
            }
            else
                GrpContNo = null;
        }
        if (FCode.equalsIgnoreCase("ProposalGrpContNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                ProposalGrpContNo = FValue.trim();
            }
            else
                ProposalGrpContNo = null;
        }
        if (FCode.equalsIgnoreCase("PrtNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                PrtNo = FValue.trim();
            }
            else
                PrtNo = null;
        }
        if (FCode.equalsIgnoreCase("SaleChnl")) {
            if( FValue != null && !FValue.equals(""))
            {
                SaleChnl = FValue.trim();
            }
            else
                SaleChnl = null;
        }
        if (FCode.equalsIgnoreCase("SalesWay")) {
            if( FValue != null && !FValue.equals(""))
            {
                SalesWay = FValue.trim();
            }
            else
                SalesWay = null;
        }
        if (FCode.equalsIgnoreCase("SalesWaySub")) {
            if( FValue != null && !FValue.equals(""))
            {
                SalesWaySub = FValue.trim();
            }
            else
                SalesWaySub = null;
        }
        if (FCode.equalsIgnoreCase("BizNature")) {
            if( FValue != null && !FValue.equals(""))
            {
                BizNature = FValue.trim();
            }
            else
                BizNature = null;
        }
        if (FCode.equalsIgnoreCase("PolIssuPlat")) {
            if( FValue != null && !FValue.equals(""))
            {
                PolIssuPlat = FValue.trim();
            }
            else
                PolIssuPlat = null;
        }
        if (FCode.equalsIgnoreCase("ContType")) {
            if( FValue != null && !FValue.equals(""))
            {
                ContType = FValue.trim();
            }
            else
                ContType = null;
        }
        if (FCode.equalsIgnoreCase("AgentCom")) {
            if( FValue != null && !FValue.equals(""))
            {
                AgentCom = FValue.trim();
            }
            else
                AgentCom = null;
        }
        if (FCode.equalsIgnoreCase("AgentCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                AgentCode = FValue.trim();
            }
            else
                AgentCode = null;
        }
        if (FCode.equalsIgnoreCase("AgentGroup")) {
            if( FValue != null && !FValue.equals(""))
            {
                AgentGroup = FValue.trim();
            }
            else
                AgentGroup = null;
        }
        if (FCode.equalsIgnoreCase("AgentCode1")) {
            if( FValue != null && !FValue.equals(""))
            {
                AgentCode1 = FValue.trim();
            }
            else
                AgentCode1 = null;
        }
        if (FCode.equalsIgnoreCase("AppntNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                AppntNo = FValue.trim();
            }
            else
                AppntNo = null;
        }
        if (FCode.equalsIgnoreCase("Peoples2")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                Peoples2 = i;
            }
        }
        if (FCode.equalsIgnoreCase("GrpName")) {
            if( FValue != null && !FValue.equals(""))
            {
                GrpName = FValue.trim();
            }
            else
                GrpName = null;
        }
        if (FCode.equalsIgnoreCase("DisputedFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                DisputedFlag = FValue.trim();
            }
            else
                DisputedFlag = null;
        }
        if (FCode.equalsIgnoreCase("OutPayFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                OutPayFlag = FValue.trim();
            }
            else
                OutPayFlag = null;
        }
        if (FCode.equalsIgnoreCase("Lang")) {
            if( FValue != null && !FValue.equals(""))
            {
                Lang = FValue.trim();
            }
            else
                Lang = null;
        }
        if (FCode.equalsIgnoreCase("Currency")) {
            if( FValue != null && !FValue.equals(""))
            {
                Currency = FValue.trim();
            }
            else
                Currency = null;
        }
        if (FCode.equalsIgnoreCase("LostTimes")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                LostTimes = i;
            }
        }
        if (FCode.equalsIgnoreCase("PrintCount")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                PrintCount = i;
            }
        }
        if (FCode.equalsIgnoreCase("LastEdorDate")) {
            if(FValue != null && !FValue.equals("")) {
                LastEdorDate = fDate.getDate( FValue );
            }
            else
                LastEdorDate = null;
        }
        if (FCode.equalsIgnoreCase("SpecFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                SpecFlag = FValue.trim();
            }
            else
                SpecFlag = null;
        }
        if (FCode.equalsIgnoreCase("SignCom")) {
            if( FValue != null && !FValue.equals(""))
            {
                SignCom = FValue.trim();
            }
            else
                SignCom = null;
        }
        if (FCode.equalsIgnoreCase("SignDate")) {
            if(FValue != null && !FValue.equals("")) {
                SignDate = fDate.getDate( FValue );
            }
            else
                SignDate = null;
        }
        if (FCode.equalsIgnoreCase("SignTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                SignTime = FValue.trim();
            }
            else
                SignTime = null;
        }
        if (FCode.equalsIgnoreCase("CValiDate")) {
            if(FValue != null && !FValue.equals("")) {
                CValiDate = fDate.getDate( FValue );
            }
            else
                CValiDate = null;
        }
        if (FCode.equalsIgnoreCase("PayIntv")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                PayIntv = i;
            }
        }
        if (FCode.equalsIgnoreCase("ManageFeeRate")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                ManageFeeRate = d;
            }
        }
        if (FCode.equalsIgnoreCase("ExpPeoples")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                ExpPeoples = i;
            }
        }
        if (FCode.equalsIgnoreCase("ExpPremium")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                ExpPremium = d;
            }
        }
        if (FCode.equalsIgnoreCase("ExpAmnt")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                ExpAmnt = d;
            }
        }
        if (FCode.equalsIgnoreCase("Peoples")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                Peoples = i;
            }
        }
        if (FCode.equalsIgnoreCase("Mult")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                Mult = d;
            }
        }
        if (FCode.equalsIgnoreCase("Prem")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                Prem = d;
            }
        }
        if (FCode.equalsIgnoreCase("Amnt")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                Amnt = d;
            }
        }
        if (FCode.equalsIgnoreCase("SumPrem")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                SumPrem = d;
            }
        }
        if (FCode.equalsIgnoreCase("SumPay")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                SumPay = d;
            }
        }
        if (FCode.equalsIgnoreCase("Dif")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                Dif = d;
            }
        }
        if (FCode.equalsIgnoreCase("StandbyFlag1")) {
            if( FValue != null && !FValue.equals(""))
            {
                StandbyFlag1 = FValue.trim();
            }
            else
                StandbyFlag1 = null;
        }
        if (FCode.equalsIgnoreCase("StandbyFlag2")) {
            if( FValue != null && !FValue.equals(""))
            {
                StandbyFlag2 = FValue.trim();
            }
            else
                StandbyFlag2 = null;
        }
        if (FCode.equalsIgnoreCase("StandbyFlag3")) {
            if( FValue != null && !FValue.equals(""))
            {
                StandbyFlag3 = FValue.trim();
            }
            else
                StandbyFlag3 = null;
        }
        if (FCode.equalsIgnoreCase("InputOperator")) {
            if( FValue != null && !FValue.equals(""))
            {
                InputOperator = FValue.trim();
            }
            else
                InputOperator = null;
        }
        if (FCode.equalsIgnoreCase("InputDate")) {
            if(FValue != null && !FValue.equals("")) {
                InputDate = fDate.getDate( FValue );
            }
            else
                InputDate = null;
        }
        if (FCode.equalsIgnoreCase("InputTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                InputTime = FValue.trim();
            }
            else
                InputTime = null;
        }
        if (FCode.equalsIgnoreCase("ApproveFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                ApproveFlag = FValue.trim();
            }
            else
                ApproveFlag = null;
        }
        if (FCode.equalsIgnoreCase("ApproveCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                ApproveCode = FValue.trim();
            }
            else
                ApproveCode = null;
        }
        if (FCode.equalsIgnoreCase("ApproveDate")) {
            if(FValue != null && !FValue.equals("")) {
                ApproveDate = fDate.getDate( FValue );
            }
            else
                ApproveDate = null;
        }
        if (FCode.equalsIgnoreCase("ApproveTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                ApproveTime = FValue.trim();
            }
            else
                ApproveTime = null;
        }
        if (FCode.equalsIgnoreCase("UWOperator")) {
            if( FValue != null && !FValue.equals(""))
            {
                UWOperator = FValue.trim();
            }
            else
                UWOperator = null;
        }
        if (FCode.equalsIgnoreCase("UWFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                UWFlag = FValue.trim();
            }
            else
                UWFlag = null;
        }
        if (FCode.equalsIgnoreCase("UWDate")) {
            if(FValue != null && !FValue.equals("")) {
                UWDate = fDate.getDate( FValue );
            }
            else
                UWDate = null;
        }
        if (FCode.equalsIgnoreCase("UWTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                UWTime = FValue.trim();
            }
            else
                UWTime = null;
        }
        if (FCode.equalsIgnoreCase("AppFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                AppFlag = FValue.trim();
            }
            else
                AppFlag = null;
        }
        if (FCode.equalsIgnoreCase("State")) {
            if( FValue != null && !FValue.equals(""))
            {
                State = FValue.trim();
            }
            else
                State = null;
        }
        if (FCode.equalsIgnoreCase("PolApplyDate")) {
            if(FValue != null && !FValue.equals("")) {
                PolApplyDate = fDate.getDate( FValue );
            }
            else
                PolApplyDate = null;
        }
        if (FCode.equalsIgnoreCase("CustomGetPolDate")) {
            if(FValue != null && !FValue.equals("")) {
                CustomGetPolDate = fDate.getDate( FValue );
            }
            else
                CustomGetPolDate = null;
        }
        if (FCode.equalsIgnoreCase("GetPolDate")) {
            if(FValue != null && !FValue.equals("")) {
                GetPolDate = fDate.getDate( FValue );
            }
            else
                GetPolDate = null;
        }
        if (FCode.equalsIgnoreCase("ManageCom")) {
            if( FValue != null && !FValue.equals(""))
            {
                ManageCom = FValue.trim();
            }
            else
                ManageCom = null;
        }
        if (FCode.equalsIgnoreCase("ComCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                ComCode = FValue.trim();
            }
            else
                ComCode = null;
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            if( FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
                Operator = null;
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if(FValue != null && !FValue.equals("")) {
                MakeDate = fDate.getDate( FValue );
            }
            else
                MakeDate = null;
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
                MakeTime = null;
        }
        if (FCode.equalsIgnoreCase("ModifyOperator")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyOperator = FValue.trim();
            }
            else
                ModifyOperator = null;
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if(FValue != null && !FValue.equals("")) {
                ModifyDate = fDate.getDate( FValue );
            }
            else
                ModifyDate = null;
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
                ModifyTime = null;
        }
        if (FCode.equalsIgnoreCase("EnterKind")) {
            if( FValue != null && !FValue.equals(""))
            {
                EnterKind = FValue.trim();
            }
            else
                EnterKind = null;
        }
        if (FCode.equalsIgnoreCase("AmntGrade")) {
            if( FValue != null && !FValue.equals(""))
            {
                AmntGrade = FValue.trim();
            }
            else
                AmntGrade = null;
        }
        if (FCode.equalsIgnoreCase("FirstTrialOperator")) {
            if( FValue != null && !FValue.equals(""))
            {
                FirstTrialOperator = FValue.trim();
            }
            else
                FirstTrialOperator = null;
        }
        if (FCode.equalsIgnoreCase("FirstTrialDate")) {
            if(FValue != null && !FValue.equals("")) {
                FirstTrialDate = fDate.getDate( FValue );
            }
            else
                FirstTrialDate = null;
        }
        if (FCode.equalsIgnoreCase("FirstTrialTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                FirstTrialTime = FValue.trim();
            }
            else
                FirstTrialTime = null;
        }
        if (FCode.equalsIgnoreCase("ReceiveOperator")) {
            if( FValue != null && !FValue.equals(""))
            {
                ReceiveOperator = FValue.trim();
            }
            else
                ReceiveOperator = null;
        }
        if (FCode.equalsIgnoreCase("ReceiveDate")) {
            if(FValue != null && !FValue.equals("")) {
                ReceiveDate = fDate.getDate( FValue );
            }
            else
                ReceiveDate = null;
        }
        if (FCode.equalsIgnoreCase("ReceiveTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                ReceiveTime = FValue.trim();
            }
            else
                ReceiveTime = null;
        }
        if (FCode.equalsIgnoreCase("MarketType")) {
            if( FValue != null && !FValue.equals(""))
            {
                MarketType = FValue.trim();
            }
            else
                MarketType = null;
        }
        if (FCode.equalsIgnoreCase("ReportNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                ReportNo = FValue.trim();
            }
            else
                ReportNo = null;
        }
        if (FCode.equalsIgnoreCase("EdorCalType")) {
            if( FValue != null && !FValue.equals(""))
            {
                EdorCalType = FValue.trim();
            }
            else
                EdorCalType = null;
        }
        if (FCode.equalsIgnoreCase("RenewFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                RenewFlag = FValue.trim();
            }
            else
                RenewFlag = null;
        }
        if (FCode.equalsIgnoreCase("RenewContNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                RenewContNo = FValue.trim();
            }
            else
                RenewContNo = null;
        }
        if (FCode.equalsIgnoreCase("EndDate")) {
            if(FValue != null && !FValue.equals("")) {
                EndDate = fDate.getDate( FValue );
            }
            else
                EndDate = null;
        }
        if (FCode.equalsIgnoreCase("Coinsurance")) {
            if( FValue != null && !FValue.equals(""))
            {
                Coinsurance = FValue.trim();
            }
            else
                Coinsurance = null;
        }
        if (FCode.equalsIgnoreCase("CardTypeCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                CardTypeCode = FValue.trim();
            }
            else
                CardTypeCode = null;
        }
        if (FCode.equalsIgnoreCase("RelationToappnt")) {
            if( FValue != null && !FValue.equals(""))
            {
                RelationToappnt = FValue.trim();
            }
            else
                RelationToappnt = null;
        }
        if (FCode.equalsIgnoreCase("NewReinsureFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                NewReinsureFlag = FValue.trim();
            }
            else
                NewReinsureFlag = null;
        }
        if (FCode.equalsIgnoreCase("InsureRate")) {
            if( FValue != null && !FValue.equals("")) {
                InsureRate = new Long(FValue).longValue();
            }
        }
        if (FCode.equalsIgnoreCase("AgcAgentCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                AgcAgentCode = FValue.trim();
            }
            else
                AgcAgentCode = null;
        }
        if (FCode.equalsIgnoreCase("AgcAgentName")) {
            if( FValue != null && !FValue.equals(""))
            {
                AgcAgentName = FValue.trim();
            }
            else
                AgcAgentName = null;
        }
        if (FCode.equalsIgnoreCase("AvailiaDateFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                AvailiaDateFlag = FValue.trim();
            }
            else
                AvailiaDateFlag = null;
        }
        if (FCode.equalsIgnoreCase("ComBusType")) {
            if( FValue != null && !FValue.equals(""))
            {
                ComBusType = FValue.trim();
            }
            else
                ComBusType = null;
        }
        if (FCode.equalsIgnoreCase("PrintFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                PrintFlag = FValue.trim();
            }
            else
                PrintFlag = null;
        }
        if (FCode.equalsIgnoreCase("SpecAcceptFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                SpecAcceptFlag = FValue.trim();
            }
            else
                SpecAcceptFlag = null;
        }
        if (FCode.equalsIgnoreCase("PolicyType")) {
            if( FValue != null && !FValue.equals(""))
            {
                PolicyType = FValue.trim();
            }
            else
                PolicyType = null;
        }
        if (FCode.equalsIgnoreCase("AchvAccruFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                AchvAccruFlag = FValue.trim();
            }
            else
                AchvAccruFlag = null;
        }
        if (FCode.equalsIgnoreCase("Remark")) {
            if( FValue != null && !FValue.equals(""))
            {
                Remark = FValue.trim();
            }
            else
                Remark = null;
        }
        if (FCode.equalsIgnoreCase("ValDateType")) {
            if( FValue != null && !FValue.equals(""))
            {
                ValDateType = FValue.trim();
            }
            else
                ValDateType = null;
        }
        if (FCode.equalsIgnoreCase("SecLevelOther")) {
            if( FValue != null && !FValue.equals(""))
            {
                SecLevelOther = FValue.trim();
            }
            else
                SecLevelOther = null;
        }
        if (FCode.equalsIgnoreCase("TBType")) {
            if( FValue != null && !FValue.equals(""))
            {
                TBType = FValue.trim();
            }
            else
                TBType = null;
        }
        if (FCode.equalsIgnoreCase("SlipForm")) {
            if( FValue != null && !FValue.equals(""))
            {
                SlipForm = FValue.trim();
            }
            else
                SlipForm = null;
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject) return true;
        if (otherObject == null) return false;
        if (getClass() != otherObject.getClass()) return false;
        LCGrpContSchema other = (LCGrpContSchema)otherObject;
        return
            GrpContNo.equals(other.getGrpContNo())
            && ProposalGrpContNo.equals(other.getProposalGrpContNo())
            && PrtNo.equals(other.getPrtNo())
            && SaleChnl.equals(other.getSaleChnl())
            && SalesWay.equals(other.getSalesWay())
            && SalesWaySub.equals(other.getSalesWaySub())
            && BizNature.equals(other.getBizNature())
            && PolIssuPlat.equals(other.getPolIssuPlat())
            && ContType.equals(other.getContType())
            && AgentCom.equals(other.getAgentCom())
            && AgentCode.equals(other.getAgentCode())
            && AgentGroup.equals(other.getAgentGroup())
            && AgentCode1.equals(other.getAgentCode1())
            && AppntNo.equals(other.getAppntNo())
            && Peoples2 == other.getPeoples2()
            && GrpName.equals(other.getGrpName())
            && DisputedFlag.equals(other.getDisputedFlag())
            && OutPayFlag.equals(other.getOutPayFlag())
            && Lang.equals(other.getLang())
            && Currency.equals(other.getCurrency())
            && LostTimes == other.getLostTimes()
            && PrintCount == other.getPrintCount()
            && fDate.getString(LastEdorDate).equals(other.getLastEdorDate())
            && SpecFlag.equals(other.getSpecFlag())
            && SignCom.equals(other.getSignCom())
            && fDate.getString(SignDate).equals(other.getSignDate())
            && SignTime.equals(other.getSignTime())
            && fDate.getString(CValiDate).equals(other.getCValiDate())
            && PayIntv == other.getPayIntv()
            && ManageFeeRate == other.getManageFeeRate()
            && ExpPeoples == other.getExpPeoples()
            && ExpPremium == other.getExpPremium()
            && ExpAmnt == other.getExpAmnt()
            && Peoples == other.getPeoples()
            && Mult == other.getMult()
            && Prem == other.getPrem()
            && Amnt == other.getAmnt()
            && SumPrem == other.getSumPrem()
            && SumPay == other.getSumPay()
            && Dif == other.getDif()
            && StandbyFlag1.equals(other.getStandbyFlag1())
            && StandbyFlag2.equals(other.getStandbyFlag2())
            && StandbyFlag3.equals(other.getStandbyFlag3())
            && InputOperator.equals(other.getInputOperator())
            && fDate.getString(InputDate).equals(other.getInputDate())
            && InputTime.equals(other.getInputTime())
            && ApproveFlag.equals(other.getApproveFlag())
            && ApproveCode.equals(other.getApproveCode())
            && fDate.getString(ApproveDate).equals(other.getApproveDate())
            && ApproveTime.equals(other.getApproveTime())
            && UWOperator.equals(other.getUWOperator())
            && UWFlag.equals(other.getUWFlag())
            && fDate.getString(UWDate).equals(other.getUWDate())
            && UWTime.equals(other.getUWTime())
            && AppFlag.equals(other.getAppFlag())
            && State.equals(other.getState())
            && fDate.getString(PolApplyDate).equals(other.getPolApplyDate())
            && fDate.getString(CustomGetPolDate).equals(other.getCustomGetPolDate())
            && fDate.getString(GetPolDate).equals(other.getGetPolDate())
            && ManageCom.equals(other.getManageCom())
            && ComCode.equals(other.getComCode())
            && Operator.equals(other.getOperator())
            && fDate.getString(MakeDate).equals(other.getMakeDate())
            && MakeTime.equals(other.getMakeTime())
            && ModifyOperator.equals(other.getModifyOperator())
            && fDate.getString(ModifyDate).equals(other.getModifyDate())
            && ModifyTime.equals(other.getModifyTime())
            && EnterKind.equals(other.getEnterKind())
            && AmntGrade.equals(other.getAmntGrade())
            && FirstTrialOperator.equals(other.getFirstTrialOperator())
            && fDate.getString(FirstTrialDate).equals(other.getFirstTrialDate())
            && FirstTrialTime.equals(other.getFirstTrialTime())
            && ReceiveOperator.equals(other.getReceiveOperator())
            && fDate.getString(ReceiveDate).equals(other.getReceiveDate())
            && ReceiveTime.equals(other.getReceiveTime())
            && MarketType.equals(other.getMarketType())
            && ReportNo.equals(other.getReportNo())
            && EdorCalType.equals(other.getEdorCalType())
            && RenewFlag.equals(other.getRenewFlag())
            && RenewContNo.equals(other.getRenewContNo())
            && fDate.getString(EndDate).equals(other.getEndDate())
            && Coinsurance.equals(other.getCoinsurance())
            && CardTypeCode.equals(other.getCardTypeCode())
            && RelationToappnt.equals(other.getRelationToappnt())
            && NewReinsureFlag.equals(other.getNewReinsureFlag())
            && InsureRate == other.getInsureRate()
            && AgcAgentCode.equals(other.getAgcAgentCode())
            && AgcAgentName.equals(other.getAgcAgentName())
            && AvailiaDateFlag.equals(other.getAvailiaDateFlag())
            && ComBusType.equals(other.getComBusType())
            && PrintFlag.equals(other.getPrintFlag())
            && SpecAcceptFlag.equals(other.getSpecAcceptFlag())
            && PolicyType.equals(other.getPolicyType())
            && AchvAccruFlag.equals(other.getAchvAccruFlag())
            && Remark.equals(other.getRemark())
            && ValDateType.equals(other.getValDateType())
            && SecLevelOther.equals(other.getSecLevelOther())
            && TBType.equals(other.getTBType())
            && SlipForm.equals(other.getSlipForm());
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("GrpContNo") ) {
            return 0;
        }
        if( strFieldName.equals("ProposalGrpContNo") ) {
            return 1;
        }
        if( strFieldName.equals("PrtNo") ) {
            return 2;
        }
        if( strFieldName.equals("SaleChnl") ) {
            return 3;
        }
        if( strFieldName.equals("SalesWay") ) {
            return 4;
        }
        if( strFieldName.equals("SalesWaySub") ) {
            return 5;
        }
        if( strFieldName.equals("BizNature") ) {
            return 6;
        }
        if( strFieldName.equals("PolIssuPlat") ) {
            return 7;
        }
        if( strFieldName.equals("ContType") ) {
            return 8;
        }
        if( strFieldName.equals("AgentCom") ) {
            return 9;
        }
        if( strFieldName.equals("AgentCode") ) {
            return 10;
        }
        if( strFieldName.equals("AgentGroup") ) {
            return 11;
        }
        if( strFieldName.equals("AgentCode1") ) {
            return 12;
        }
        if( strFieldName.equals("AppntNo") ) {
            return 13;
        }
        if( strFieldName.equals("Peoples2") ) {
            return 14;
        }
        if( strFieldName.equals("GrpName") ) {
            return 15;
        }
        if( strFieldName.equals("DisputedFlag") ) {
            return 16;
        }
        if( strFieldName.equals("OutPayFlag") ) {
            return 17;
        }
        if( strFieldName.equals("Lang") ) {
            return 18;
        }
        if( strFieldName.equals("Currency") ) {
            return 19;
        }
        if( strFieldName.equals("LostTimes") ) {
            return 20;
        }
        if( strFieldName.equals("PrintCount") ) {
            return 21;
        }
        if( strFieldName.equals("LastEdorDate") ) {
            return 22;
        }
        if( strFieldName.equals("SpecFlag") ) {
            return 23;
        }
        if( strFieldName.equals("SignCom") ) {
            return 24;
        }
        if( strFieldName.equals("SignDate") ) {
            return 25;
        }
        if( strFieldName.equals("SignTime") ) {
            return 26;
        }
        if( strFieldName.equals("CValiDate") ) {
            return 27;
        }
        if( strFieldName.equals("PayIntv") ) {
            return 28;
        }
        if( strFieldName.equals("ManageFeeRate") ) {
            return 29;
        }
        if( strFieldName.equals("ExpPeoples") ) {
            return 30;
        }
        if( strFieldName.equals("ExpPremium") ) {
            return 31;
        }
        if( strFieldName.equals("ExpAmnt") ) {
            return 32;
        }
        if( strFieldName.equals("Peoples") ) {
            return 33;
        }
        if( strFieldName.equals("Mult") ) {
            return 34;
        }
        if( strFieldName.equals("Prem") ) {
            return 35;
        }
        if( strFieldName.equals("Amnt") ) {
            return 36;
        }
        if( strFieldName.equals("SumPrem") ) {
            return 37;
        }
        if( strFieldName.equals("SumPay") ) {
            return 38;
        }
        if( strFieldName.equals("Dif") ) {
            return 39;
        }
        if( strFieldName.equals("StandbyFlag1") ) {
            return 40;
        }
        if( strFieldName.equals("StandbyFlag2") ) {
            return 41;
        }
        if( strFieldName.equals("StandbyFlag3") ) {
            return 42;
        }
        if( strFieldName.equals("InputOperator") ) {
            return 43;
        }
        if( strFieldName.equals("InputDate") ) {
            return 44;
        }
        if( strFieldName.equals("InputTime") ) {
            return 45;
        }
        if( strFieldName.equals("ApproveFlag") ) {
            return 46;
        }
        if( strFieldName.equals("ApproveCode") ) {
            return 47;
        }
        if( strFieldName.equals("ApproveDate") ) {
            return 48;
        }
        if( strFieldName.equals("ApproveTime") ) {
            return 49;
        }
        if( strFieldName.equals("UWOperator") ) {
            return 50;
        }
        if( strFieldName.equals("UWFlag") ) {
            return 51;
        }
        if( strFieldName.equals("UWDate") ) {
            return 52;
        }
        if( strFieldName.equals("UWTime") ) {
            return 53;
        }
        if( strFieldName.equals("AppFlag") ) {
            return 54;
        }
        if( strFieldName.equals("State") ) {
            return 55;
        }
        if( strFieldName.equals("PolApplyDate") ) {
            return 56;
        }
        if( strFieldName.equals("CustomGetPolDate") ) {
            return 57;
        }
        if( strFieldName.equals("GetPolDate") ) {
            return 58;
        }
        if( strFieldName.equals("ManageCom") ) {
            return 59;
        }
        if( strFieldName.equals("ComCode") ) {
            return 60;
        }
        if( strFieldName.equals("Operator") ) {
            return 61;
        }
        if( strFieldName.equals("MakeDate") ) {
            return 62;
        }
        if( strFieldName.equals("MakeTime") ) {
            return 63;
        }
        if( strFieldName.equals("ModifyOperator") ) {
            return 64;
        }
        if( strFieldName.equals("ModifyDate") ) {
            return 65;
        }
        if( strFieldName.equals("ModifyTime") ) {
            return 66;
        }
        if( strFieldName.equals("EnterKind") ) {
            return 67;
        }
        if( strFieldName.equals("AmntGrade") ) {
            return 68;
        }
        if( strFieldName.equals("FirstTrialOperator") ) {
            return 69;
        }
        if( strFieldName.equals("FirstTrialDate") ) {
            return 70;
        }
        if( strFieldName.equals("FirstTrialTime") ) {
            return 71;
        }
        if( strFieldName.equals("ReceiveOperator") ) {
            return 72;
        }
        if( strFieldName.equals("ReceiveDate") ) {
            return 73;
        }
        if( strFieldName.equals("ReceiveTime") ) {
            return 74;
        }
        if( strFieldName.equals("MarketType") ) {
            return 75;
        }
        if( strFieldName.equals("ReportNo") ) {
            return 76;
        }
        if( strFieldName.equals("EdorCalType") ) {
            return 77;
        }
        if( strFieldName.equals("RenewFlag") ) {
            return 78;
        }
        if( strFieldName.equals("RenewContNo") ) {
            return 79;
        }
        if( strFieldName.equals("EndDate") ) {
            return 80;
        }
        if( strFieldName.equals("Coinsurance") ) {
            return 81;
        }
        if( strFieldName.equals("CardTypeCode") ) {
            return 82;
        }
        if( strFieldName.equals("RelationToappnt") ) {
            return 83;
        }
        if( strFieldName.equals("NewReinsureFlag") ) {
            return 84;
        }
        if( strFieldName.equals("InsureRate") ) {
            return 85;
        }
        if( strFieldName.equals("AgcAgentCode") ) {
            return 86;
        }
        if( strFieldName.equals("AgcAgentName") ) {
            return 87;
        }
        if( strFieldName.equals("AvailiaDateFlag") ) {
            return 88;
        }
        if( strFieldName.equals("ComBusType") ) {
            return 89;
        }
        if( strFieldName.equals("PrintFlag") ) {
            return 90;
        }
        if( strFieldName.equals("SpecAcceptFlag") ) {
            return 91;
        }
        if( strFieldName.equals("PolicyType") ) {
            return 92;
        }
        if( strFieldName.equals("AchvAccruFlag") ) {
            return 93;
        }
        if( strFieldName.equals("Remark") ) {
            return 94;
        }
        if( strFieldName.equals("ValDateType") ) {
            return 95;
        }
        if( strFieldName.equals("SecLevelOther") ) {
            return 96;
        }
        if( strFieldName.equals("TBType") ) {
            return 97;
        }
        if( strFieldName.equals("SlipForm") ) {
            return 98;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "GrpContNo";
                break;
            case 1:
                strFieldName = "ProposalGrpContNo";
                break;
            case 2:
                strFieldName = "PrtNo";
                break;
            case 3:
                strFieldName = "SaleChnl";
                break;
            case 4:
                strFieldName = "SalesWay";
                break;
            case 5:
                strFieldName = "SalesWaySub";
                break;
            case 6:
                strFieldName = "BizNature";
                break;
            case 7:
                strFieldName = "PolIssuPlat";
                break;
            case 8:
                strFieldName = "ContType";
                break;
            case 9:
                strFieldName = "AgentCom";
                break;
            case 10:
                strFieldName = "AgentCode";
                break;
            case 11:
                strFieldName = "AgentGroup";
                break;
            case 12:
                strFieldName = "AgentCode1";
                break;
            case 13:
                strFieldName = "AppntNo";
                break;
            case 14:
                strFieldName = "Peoples2";
                break;
            case 15:
                strFieldName = "GrpName";
                break;
            case 16:
                strFieldName = "DisputedFlag";
                break;
            case 17:
                strFieldName = "OutPayFlag";
                break;
            case 18:
                strFieldName = "Lang";
                break;
            case 19:
                strFieldName = "Currency";
                break;
            case 20:
                strFieldName = "LostTimes";
                break;
            case 21:
                strFieldName = "PrintCount";
                break;
            case 22:
                strFieldName = "LastEdorDate";
                break;
            case 23:
                strFieldName = "SpecFlag";
                break;
            case 24:
                strFieldName = "SignCom";
                break;
            case 25:
                strFieldName = "SignDate";
                break;
            case 26:
                strFieldName = "SignTime";
                break;
            case 27:
                strFieldName = "CValiDate";
                break;
            case 28:
                strFieldName = "PayIntv";
                break;
            case 29:
                strFieldName = "ManageFeeRate";
                break;
            case 30:
                strFieldName = "ExpPeoples";
                break;
            case 31:
                strFieldName = "ExpPremium";
                break;
            case 32:
                strFieldName = "ExpAmnt";
                break;
            case 33:
                strFieldName = "Peoples";
                break;
            case 34:
                strFieldName = "Mult";
                break;
            case 35:
                strFieldName = "Prem";
                break;
            case 36:
                strFieldName = "Amnt";
                break;
            case 37:
                strFieldName = "SumPrem";
                break;
            case 38:
                strFieldName = "SumPay";
                break;
            case 39:
                strFieldName = "Dif";
                break;
            case 40:
                strFieldName = "StandbyFlag1";
                break;
            case 41:
                strFieldName = "StandbyFlag2";
                break;
            case 42:
                strFieldName = "StandbyFlag3";
                break;
            case 43:
                strFieldName = "InputOperator";
                break;
            case 44:
                strFieldName = "InputDate";
                break;
            case 45:
                strFieldName = "InputTime";
                break;
            case 46:
                strFieldName = "ApproveFlag";
                break;
            case 47:
                strFieldName = "ApproveCode";
                break;
            case 48:
                strFieldName = "ApproveDate";
                break;
            case 49:
                strFieldName = "ApproveTime";
                break;
            case 50:
                strFieldName = "UWOperator";
                break;
            case 51:
                strFieldName = "UWFlag";
                break;
            case 52:
                strFieldName = "UWDate";
                break;
            case 53:
                strFieldName = "UWTime";
                break;
            case 54:
                strFieldName = "AppFlag";
                break;
            case 55:
                strFieldName = "State";
                break;
            case 56:
                strFieldName = "PolApplyDate";
                break;
            case 57:
                strFieldName = "CustomGetPolDate";
                break;
            case 58:
                strFieldName = "GetPolDate";
                break;
            case 59:
                strFieldName = "ManageCom";
                break;
            case 60:
                strFieldName = "ComCode";
                break;
            case 61:
                strFieldName = "Operator";
                break;
            case 62:
                strFieldName = "MakeDate";
                break;
            case 63:
                strFieldName = "MakeTime";
                break;
            case 64:
                strFieldName = "ModifyOperator";
                break;
            case 65:
                strFieldName = "ModifyDate";
                break;
            case 66:
                strFieldName = "ModifyTime";
                break;
            case 67:
                strFieldName = "EnterKind";
                break;
            case 68:
                strFieldName = "AmntGrade";
                break;
            case 69:
                strFieldName = "FirstTrialOperator";
                break;
            case 70:
                strFieldName = "FirstTrialDate";
                break;
            case 71:
                strFieldName = "FirstTrialTime";
                break;
            case 72:
                strFieldName = "ReceiveOperator";
                break;
            case 73:
                strFieldName = "ReceiveDate";
                break;
            case 74:
                strFieldName = "ReceiveTime";
                break;
            case 75:
                strFieldName = "MarketType";
                break;
            case 76:
                strFieldName = "ReportNo";
                break;
            case 77:
                strFieldName = "EdorCalType";
                break;
            case 78:
                strFieldName = "RenewFlag";
                break;
            case 79:
                strFieldName = "RenewContNo";
                break;
            case 80:
                strFieldName = "EndDate";
                break;
            case 81:
                strFieldName = "Coinsurance";
                break;
            case 82:
                strFieldName = "CardTypeCode";
                break;
            case 83:
                strFieldName = "RelationToappnt";
                break;
            case 84:
                strFieldName = "NewReinsureFlag";
                break;
            case 85:
                strFieldName = "InsureRate";
                break;
            case 86:
                strFieldName = "AgcAgentCode";
                break;
            case 87:
                strFieldName = "AgcAgentName";
                break;
            case 88:
                strFieldName = "AvailiaDateFlag";
                break;
            case 89:
                strFieldName = "ComBusType";
                break;
            case 90:
                strFieldName = "PrintFlag";
                break;
            case 91:
                strFieldName = "SpecAcceptFlag";
                break;
            case 92:
                strFieldName = "PolicyType";
                break;
            case 93:
                strFieldName = "AchvAccruFlag";
                break;
            case 94:
                strFieldName = "Remark";
                break;
            case 95:
                strFieldName = "ValDateType";
                break;
            case 96:
                strFieldName = "SecLevelOther";
                break;
            case 97:
                strFieldName = "TBType";
                break;
            case 98:
                strFieldName = "SlipForm";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "GRPCONTNO":
                return Schema.TYPE_STRING;
            case "PROPOSALGRPCONTNO":
                return Schema.TYPE_STRING;
            case "PRTNO":
                return Schema.TYPE_STRING;
            case "SALECHNL":
                return Schema.TYPE_STRING;
            case "SALESWAY":
                return Schema.TYPE_STRING;
            case "SALESWAYSUB":
                return Schema.TYPE_STRING;
            case "BIZNATURE":
                return Schema.TYPE_STRING;
            case "POLISSUPLAT":
                return Schema.TYPE_STRING;
            case "CONTTYPE":
                return Schema.TYPE_STRING;
            case "AGENTCOM":
                return Schema.TYPE_STRING;
            case "AGENTCODE":
                return Schema.TYPE_STRING;
            case "AGENTGROUP":
                return Schema.TYPE_STRING;
            case "AGENTCODE1":
                return Schema.TYPE_STRING;
            case "APPNTNO":
                return Schema.TYPE_STRING;
            case "PEOPLES2":
                return Schema.TYPE_INT;
            case "GRPNAME":
                return Schema.TYPE_STRING;
            case "DISPUTEDFLAG":
                return Schema.TYPE_STRING;
            case "OUTPAYFLAG":
                return Schema.TYPE_STRING;
            case "LANG":
                return Schema.TYPE_STRING;
            case "CURRENCY":
                return Schema.TYPE_STRING;
            case "LOSTTIMES":
                return Schema.TYPE_INT;
            case "PRINTCOUNT":
                return Schema.TYPE_INT;
            case "LASTEDORDATE":
                return Schema.TYPE_DATE;
            case "SPECFLAG":
                return Schema.TYPE_STRING;
            case "SIGNCOM":
                return Schema.TYPE_STRING;
            case "SIGNDATE":
                return Schema.TYPE_DATE;
            case "SIGNTIME":
                return Schema.TYPE_STRING;
            case "CVALIDATE":
                return Schema.TYPE_DATE;
            case "PAYINTV":
                return Schema.TYPE_INT;
            case "MANAGEFEERATE":
                return Schema.TYPE_DOUBLE;
            case "EXPPEOPLES":
                return Schema.TYPE_INT;
            case "EXPPREMIUM":
                return Schema.TYPE_DOUBLE;
            case "EXPAMNT":
                return Schema.TYPE_DOUBLE;
            case "PEOPLES":
                return Schema.TYPE_INT;
            case "MULT":
                return Schema.TYPE_DOUBLE;
            case "PREM":
                return Schema.TYPE_DOUBLE;
            case "AMNT":
                return Schema.TYPE_DOUBLE;
            case "SUMPREM":
                return Schema.TYPE_DOUBLE;
            case "SUMPAY":
                return Schema.TYPE_DOUBLE;
            case "DIF":
                return Schema.TYPE_DOUBLE;
            case "STANDBYFLAG1":
                return Schema.TYPE_STRING;
            case "STANDBYFLAG2":
                return Schema.TYPE_STRING;
            case "STANDBYFLAG3":
                return Schema.TYPE_STRING;
            case "INPUTOPERATOR":
                return Schema.TYPE_STRING;
            case "INPUTDATE":
                return Schema.TYPE_DATE;
            case "INPUTTIME":
                return Schema.TYPE_STRING;
            case "APPROVEFLAG":
                return Schema.TYPE_STRING;
            case "APPROVECODE":
                return Schema.TYPE_STRING;
            case "APPROVEDATE":
                return Schema.TYPE_DATE;
            case "APPROVETIME":
                return Schema.TYPE_STRING;
            case "UWOPERATOR":
                return Schema.TYPE_STRING;
            case "UWFLAG":
                return Schema.TYPE_STRING;
            case "UWDATE":
                return Schema.TYPE_DATE;
            case "UWTIME":
                return Schema.TYPE_STRING;
            case "APPFLAG":
                return Schema.TYPE_STRING;
            case "STATE":
                return Schema.TYPE_STRING;
            case "POLAPPLYDATE":
                return Schema.TYPE_DATE;
            case "CUSTOMGETPOLDATE":
                return Schema.TYPE_DATE;
            case "GETPOLDATE":
                return Schema.TYPE_DATE;
            case "MANAGECOM":
                return Schema.TYPE_STRING;
            case "COMCODE":
                return Schema.TYPE_STRING;
            case "OPERATOR":
                return Schema.TYPE_STRING;
            case "MAKEDATE":
                return Schema.TYPE_DATE;
            case "MAKETIME":
                return Schema.TYPE_STRING;
            case "MODIFYOPERATOR":
                return Schema.TYPE_STRING;
            case "MODIFYDATE":
                return Schema.TYPE_DATE;
            case "MODIFYTIME":
                return Schema.TYPE_STRING;
            case "ENTERKIND":
                return Schema.TYPE_STRING;
            case "AMNTGRADE":
                return Schema.TYPE_STRING;
            case "FIRSTTRIALOPERATOR":
                return Schema.TYPE_STRING;
            case "FIRSTTRIALDATE":
                return Schema.TYPE_DATE;
            case "FIRSTTRIALTIME":
                return Schema.TYPE_STRING;
            case "RECEIVEOPERATOR":
                return Schema.TYPE_STRING;
            case "RECEIVEDATE":
                return Schema.TYPE_DATE;
            case "RECEIVETIME":
                return Schema.TYPE_STRING;
            case "MARKETTYPE":
                return Schema.TYPE_STRING;
            case "REPORTNO":
                return Schema.TYPE_STRING;
            case "EDORCALTYPE":
                return Schema.TYPE_STRING;
            case "RENEWFLAG":
                return Schema.TYPE_STRING;
            case "RENEWCONTNO":
                return Schema.TYPE_STRING;
            case "ENDDATE":
                return Schema.TYPE_DATE;
            case "COINSURANCE":
                return Schema.TYPE_STRING;
            case "CARDTYPECODE":
                return Schema.TYPE_STRING;
            case "RELATIONTOAPPNT":
                return Schema.TYPE_STRING;
            case "NEWREINSUREFLAG":
                return Schema.TYPE_STRING;
            case "INSURERATE":
                return Schema.TYPE_LONG;
            case "AGCAGENTCODE":
                return Schema.TYPE_STRING;
            case "AGCAGENTNAME":
                return Schema.TYPE_STRING;
            case "AVAILIADATEFLAG":
                return Schema.TYPE_STRING;
            case "COMBUSTYPE":
                return Schema.TYPE_STRING;
            case "PRINTFLAG":
                return Schema.TYPE_STRING;
            case "SPECACCEPTFLAG":
                return Schema.TYPE_STRING;
            case "POLICYTYPE":
                return Schema.TYPE_STRING;
            case "ACHVACCRUFLAG":
                return Schema.TYPE_STRING;
            case "REMARK":
                return Schema.TYPE_STRING;
            case "VALDATETYPE":
                return Schema.TYPE_STRING;
            case "SECLEVELOTHER":
                return Schema.TYPE_STRING;
            case "TBTYPE":
                return Schema.TYPE_STRING;
            case "SLIPFORM":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_STRING;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_STRING;
            case 6:
                return Schema.TYPE_STRING;
            case 7:
                return Schema.TYPE_STRING;
            case 8:
                return Schema.TYPE_STRING;
            case 9:
                return Schema.TYPE_STRING;
            case 10:
                return Schema.TYPE_STRING;
            case 11:
                return Schema.TYPE_STRING;
            case 12:
                return Schema.TYPE_STRING;
            case 13:
                return Schema.TYPE_STRING;
            case 14:
                return Schema.TYPE_INT;
            case 15:
                return Schema.TYPE_STRING;
            case 16:
                return Schema.TYPE_STRING;
            case 17:
                return Schema.TYPE_STRING;
            case 18:
                return Schema.TYPE_STRING;
            case 19:
                return Schema.TYPE_STRING;
            case 20:
                return Schema.TYPE_INT;
            case 21:
                return Schema.TYPE_INT;
            case 22:
                return Schema.TYPE_DATE;
            case 23:
                return Schema.TYPE_STRING;
            case 24:
                return Schema.TYPE_STRING;
            case 25:
                return Schema.TYPE_DATE;
            case 26:
                return Schema.TYPE_STRING;
            case 27:
                return Schema.TYPE_DATE;
            case 28:
                return Schema.TYPE_INT;
            case 29:
                return Schema.TYPE_DOUBLE;
            case 30:
                return Schema.TYPE_INT;
            case 31:
                return Schema.TYPE_DOUBLE;
            case 32:
                return Schema.TYPE_DOUBLE;
            case 33:
                return Schema.TYPE_INT;
            case 34:
                return Schema.TYPE_DOUBLE;
            case 35:
                return Schema.TYPE_DOUBLE;
            case 36:
                return Schema.TYPE_DOUBLE;
            case 37:
                return Schema.TYPE_DOUBLE;
            case 38:
                return Schema.TYPE_DOUBLE;
            case 39:
                return Schema.TYPE_DOUBLE;
            case 40:
                return Schema.TYPE_STRING;
            case 41:
                return Schema.TYPE_STRING;
            case 42:
                return Schema.TYPE_STRING;
            case 43:
                return Schema.TYPE_STRING;
            case 44:
                return Schema.TYPE_DATE;
            case 45:
                return Schema.TYPE_STRING;
            case 46:
                return Schema.TYPE_STRING;
            case 47:
                return Schema.TYPE_STRING;
            case 48:
                return Schema.TYPE_DATE;
            case 49:
                return Schema.TYPE_STRING;
            case 50:
                return Schema.TYPE_STRING;
            case 51:
                return Schema.TYPE_STRING;
            case 52:
                return Schema.TYPE_DATE;
            case 53:
                return Schema.TYPE_STRING;
            case 54:
                return Schema.TYPE_STRING;
            case 55:
                return Schema.TYPE_STRING;
            case 56:
                return Schema.TYPE_DATE;
            case 57:
                return Schema.TYPE_DATE;
            case 58:
                return Schema.TYPE_DATE;
            case 59:
                return Schema.TYPE_STRING;
            case 60:
                return Schema.TYPE_STRING;
            case 61:
                return Schema.TYPE_STRING;
            case 62:
                return Schema.TYPE_DATE;
            case 63:
                return Schema.TYPE_STRING;
            case 64:
                return Schema.TYPE_STRING;
            case 65:
                return Schema.TYPE_DATE;
            case 66:
                return Schema.TYPE_STRING;
            case 67:
                return Schema.TYPE_STRING;
            case 68:
                return Schema.TYPE_STRING;
            case 69:
                return Schema.TYPE_STRING;
            case 70:
                return Schema.TYPE_DATE;
            case 71:
                return Schema.TYPE_STRING;
            case 72:
                return Schema.TYPE_STRING;
            case 73:
                return Schema.TYPE_DATE;
            case 74:
                return Schema.TYPE_STRING;
            case 75:
                return Schema.TYPE_STRING;
            case 76:
                return Schema.TYPE_STRING;
            case 77:
                return Schema.TYPE_STRING;
            case 78:
                return Schema.TYPE_STRING;
            case 79:
                return Schema.TYPE_STRING;
            case 80:
                return Schema.TYPE_DATE;
            case 81:
                return Schema.TYPE_STRING;
            case 82:
                return Schema.TYPE_STRING;
            case 83:
                return Schema.TYPE_STRING;
            case 84:
                return Schema.TYPE_STRING;
            case 85:
                return Schema.TYPE_LONG;
            case 86:
                return Schema.TYPE_STRING;
            case 87:
                return Schema.TYPE_STRING;
            case 88:
                return Schema.TYPE_STRING;
            case 89:
                return Schema.TYPE_STRING;
            case 90:
                return Schema.TYPE_STRING;
            case 91:
                return Schema.TYPE_STRING;
            case 92:
                return Schema.TYPE_STRING;
            case 93:
                return Schema.TYPE_STRING;
            case 94:
                return Schema.TYPE_STRING;
            case 95:
                return Schema.TYPE_STRING;
            case 96:
                return Schema.TYPE_STRING;
            case 97:
                return Schema.TYPE_STRING;
            case 98:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
}
