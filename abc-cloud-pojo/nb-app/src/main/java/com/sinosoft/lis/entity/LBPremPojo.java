/**
 * Copyright (c) 2018 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.entity;

import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.Pojo;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;

import java.io.Serializable;

/**
 * <p>ClassName: LBPremPojo </p>
 * <p>Copyright: Copyright (c) 2018</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2018-05-23
 */
public class LBPremPojo implements Pojo,Serializable {
    // @Field
    /** Id */
    private long PremID; 
    /** Shardingid */
    private String ShardingID; 
    /** 批单号 */
    private String EdorNo; 
    /** 集体合同号码 */
    private String GrpContNo; 
    /** 合同号码 */
    private String ContNo; 
    /** 保单险种号码 */
    private String PolNo; 
    /** 责任编码 */
    private String DutyCode; 
    /** 交费计划编码 */
    private String PayPlanCode; 
    /** 交费计划类型 */
    private String PayPlanType; 
    /** 投保人类型 */
    private String AppntType; 
    /** 投保人客户号码 */
    private String AppntNo; 
    /** 催缴标记 */
    private String UrgePayFlag; 
    /** 是否和账户相关 */
    private String NeedAcc; 
    /** 已交费次数 */
    private int PayTimes; 
    /** 保费分配比率 */
    private double Rate; 
    /** 起交日期 */
    private String  PayStartDate;
    /** 终交日期 */
    private String  PayEndDate;
    /** 交至日期 */
    private String  PaytoDate;
    /** 交费间隔 */
    private int PayIntv; 
    /** 每期保费 */
    private double StandPrem; 
    /** 实际保费 */
    private double Prem; 
    /** 累计保费 */
    private double SumPrem; 
    /** 额外风险评分 */
    private double SuppRiskScore; 
    /** 免交标志 */
    private String FreeFlag; 
    /** 免交比率 */
    private double FreeRate; 
    /** 免交起期 */
    private String  FreeStartDate;
    /** 免交止期 */
    private String  FreeEndDate;
    /** 状态 */
    private String State; 
    /** 管理机构 */
    private String ManageCom; 
    /** 操作员 */
    private String Operator; 
    /** 入机日期 */
    private String  MakeDate;
    /** 入机时间 */
    private String MakeTime; 
    /** 最后一次修改日期 */
    private String  ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime; 
    /** 加费指向标记 */
    private String AddFeeDirect; 
    /** 第二被保人加费评点 */
    private double SecInsuAddPoint; 
    /** 加费开始时间类型 */
    private String AddForm; 
    /** 期交保费 */
    private double PeriodPrem; 
    /** 加费类型 */
    private String PayType; 


    public static final int FIELDNUM = 39;    // 数据库表的字段个数

    private FDate fDate = new FDate();        // 处理日期
    public long getPremID() {
        return PremID;
    }
    public void setPremID(long aPremID) {
        PremID = aPremID;
    }
    public void setPremID(String aPremID) {
        if (aPremID != null && !aPremID.equals("")) {
            PremID = new Long(aPremID).longValue();
        }
    }

    public String getShardingID() {
        return ShardingID;
    }
    public void setShardingID(String aShardingID) {
        ShardingID = aShardingID;
    }
    public String getEdorNo() {
        return EdorNo;
    }
    public void setEdorNo(String aEdorNo) {
        EdorNo = aEdorNo;
    }
    public String getGrpContNo() {
        return GrpContNo;
    }
    public void setGrpContNo(String aGrpContNo) {
        GrpContNo = aGrpContNo;
    }
    public String getContNo() {
        return ContNo;
    }
    public void setContNo(String aContNo) {
        ContNo = aContNo;
    }
    public String getPolNo() {
        return PolNo;
    }
    public void setPolNo(String aPolNo) {
        PolNo = aPolNo;
    }
    public String getDutyCode() {
        return DutyCode;
    }
    public void setDutyCode(String aDutyCode) {
        DutyCode = aDutyCode;
    }
    public String getPayPlanCode() {
        return PayPlanCode;
    }
    public void setPayPlanCode(String aPayPlanCode) {
        PayPlanCode = aPayPlanCode;
    }
    public String getPayPlanType() {
        return PayPlanType;
    }
    public void setPayPlanType(String aPayPlanType) {
        PayPlanType = aPayPlanType;
    }
    public String getAppntType() {
        return AppntType;
    }
    public void setAppntType(String aAppntType) {
        AppntType = aAppntType;
    }
    public String getAppntNo() {
        return AppntNo;
    }
    public void setAppntNo(String aAppntNo) {
        AppntNo = aAppntNo;
    }
    public String getUrgePayFlag() {
        return UrgePayFlag;
    }
    public void setUrgePayFlag(String aUrgePayFlag) {
        UrgePayFlag = aUrgePayFlag;
    }
    public String getNeedAcc() {
        return NeedAcc;
    }
    public void setNeedAcc(String aNeedAcc) {
        NeedAcc = aNeedAcc;
    }
    public int getPayTimes() {
        return PayTimes;
    }
    public void setPayTimes(int aPayTimes) {
        PayTimes = aPayTimes;
    }
    public void setPayTimes(String aPayTimes) {
        if (aPayTimes != null && !aPayTimes.equals("")) {
            Integer tInteger = new Integer(aPayTimes);
            int i = tInteger.intValue();
            PayTimes = i;
        }
    }

    public double getRate() {
        return Rate;
    }
    public void setRate(double aRate) {
        Rate = aRate;
    }
    public void setRate(String aRate) {
        if (aRate != null && !aRate.equals("")) {
            Double tDouble = new Double(aRate);
            double d = tDouble.doubleValue();
            Rate = d;
        }
    }

    public String getPayStartDate() {
        return PayStartDate;
    }
    public void setPayStartDate(String aPayStartDate) {
        PayStartDate = aPayStartDate;
    }
    public String getPayEndDate() {
        return PayEndDate;
    }
    public void setPayEndDate(String aPayEndDate) {
        PayEndDate = aPayEndDate;
    }
    public String getPaytoDate() {
        return PaytoDate;
    }
    public void setPaytoDate(String aPaytoDate) {
        PaytoDate = aPaytoDate;
    }
    public int getPayIntv() {
        return PayIntv;
    }
    public void setPayIntv(int aPayIntv) {
        PayIntv = aPayIntv;
    }
    public void setPayIntv(String aPayIntv) {
        if (aPayIntv != null && !aPayIntv.equals("")) {
            Integer tInteger = new Integer(aPayIntv);
            int i = tInteger.intValue();
            PayIntv = i;
        }
    }

    public double getStandPrem() {
        return StandPrem;
    }
    public void setStandPrem(double aStandPrem) {
        StandPrem = aStandPrem;
    }
    public void setStandPrem(String aStandPrem) {
        if (aStandPrem != null && !aStandPrem.equals("")) {
            Double tDouble = new Double(aStandPrem);
            double d = tDouble.doubleValue();
            StandPrem = d;
        }
    }

    public double getPrem() {
        return Prem;
    }
    public void setPrem(double aPrem) {
        Prem = aPrem;
    }
    public void setPrem(String aPrem) {
        if (aPrem != null && !aPrem.equals("")) {
            Double tDouble = new Double(aPrem);
            double d = tDouble.doubleValue();
            Prem = d;
        }
    }

    public double getSumPrem() {
        return SumPrem;
    }
    public void setSumPrem(double aSumPrem) {
        SumPrem = aSumPrem;
    }
    public void setSumPrem(String aSumPrem) {
        if (aSumPrem != null && !aSumPrem.equals("")) {
            Double tDouble = new Double(aSumPrem);
            double d = tDouble.doubleValue();
            SumPrem = d;
        }
    }

    public double getSuppRiskScore() {
        return SuppRiskScore;
    }
    public void setSuppRiskScore(double aSuppRiskScore) {
        SuppRiskScore = aSuppRiskScore;
    }
    public void setSuppRiskScore(String aSuppRiskScore) {
        if (aSuppRiskScore != null && !aSuppRiskScore.equals("")) {
            Double tDouble = new Double(aSuppRiskScore);
            double d = tDouble.doubleValue();
            SuppRiskScore = d;
        }
    }

    public String getFreeFlag() {
        return FreeFlag;
    }
    public void setFreeFlag(String aFreeFlag) {
        FreeFlag = aFreeFlag;
    }
    public double getFreeRate() {
        return FreeRate;
    }
    public void setFreeRate(double aFreeRate) {
        FreeRate = aFreeRate;
    }
    public void setFreeRate(String aFreeRate) {
        if (aFreeRate != null && !aFreeRate.equals("")) {
            Double tDouble = new Double(aFreeRate);
            double d = tDouble.doubleValue();
            FreeRate = d;
        }
    }

    public String getFreeStartDate() {
        return FreeStartDate;
    }
    public void setFreeStartDate(String aFreeStartDate) {
        FreeStartDate = aFreeStartDate;
    }
    public String getFreeEndDate() {
        return FreeEndDate;
    }
    public void setFreeEndDate(String aFreeEndDate) {
        FreeEndDate = aFreeEndDate;
    }
    public String getState() {
        return State;
    }
    public void setState(String aState) {
        State = aState;
    }
    public String getManageCom() {
        return ManageCom;
    }
    public void setManageCom(String aManageCom) {
        ManageCom = aManageCom;
    }
    public String getOperator() {
        return Operator;
    }
    public void setOperator(String aOperator) {
        Operator = aOperator;
    }
    public String getMakeDate() {
        return MakeDate;
    }
    public void setMakeDate(String aMakeDate) {
        MakeDate = aMakeDate;
    }
    public String getMakeTime() {
        return MakeTime;
    }
    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }
    public String getModifyDate() {
        return ModifyDate;
    }
    public void setModifyDate(String aModifyDate) {
        ModifyDate = aModifyDate;
    }
    public String getModifyTime() {
        return ModifyTime;
    }
    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }
    public String getAddFeeDirect() {
        return AddFeeDirect;
    }
    public void setAddFeeDirect(String aAddFeeDirect) {
        AddFeeDirect = aAddFeeDirect;
    }
    public double getSecInsuAddPoint() {
        return SecInsuAddPoint;
    }
    public void setSecInsuAddPoint(double aSecInsuAddPoint) {
        SecInsuAddPoint = aSecInsuAddPoint;
    }
    public void setSecInsuAddPoint(String aSecInsuAddPoint) {
        if (aSecInsuAddPoint != null && !aSecInsuAddPoint.equals("")) {
            Double tDouble = new Double(aSecInsuAddPoint);
            double d = tDouble.doubleValue();
            SecInsuAddPoint = d;
        }
    }

    public String getAddForm() {
        return AddForm;
    }
    public void setAddForm(String aAddForm) {
        AddForm = aAddForm;
    }
    public double getPeriodPrem() {
        return PeriodPrem;
    }
    public void setPeriodPrem(double aPeriodPrem) {
        PeriodPrem = aPeriodPrem;
    }
    public void setPeriodPrem(String aPeriodPrem) {
        if (aPeriodPrem != null && !aPeriodPrem.equals("")) {
            Double tDouble = new Double(aPeriodPrem);
            double d = tDouble.doubleValue();
            PeriodPrem = d;
        }
    }

    public String getPayType() {
        return PayType;
    }
    public void setPayType(String aPayType) {
        PayType = aPayType;
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("PremID") ) {
            return 0;
        }
        if( strFieldName.equals("ShardingID") ) {
            return 1;
        }
        if( strFieldName.equals("EdorNo") ) {
            return 2;
        }
        if( strFieldName.equals("GrpContNo") ) {
            return 3;
        }
        if( strFieldName.equals("ContNo") ) {
            return 4;
        }
        if( strFieldName.equals("PolNo") ) {
            return 5;
        }
        if( strFieldName.equals("DutyCode") ) {
            return 6;
        }
        if( strFieldName.equals("PayPlanCode") ) {
            return 7;
        }
        if( strFieldName.equals("PayPlanType") ) {
            return 8;
        }
        if( strFieldName.equals("AppntType") ) {
            return 9;
        }
        if( strFieldName.equals("AppntNo") ) {
            return 10;
        }
        if( strFieldName.equals("UrgePayFlag") ) {
            return 11;
        }
        if( strFieldName.equals("NeedAcc") ) {
            return 12;
        }
        if( strFieldName.equals("PayTimes") ) {
            return 13;
        }
        if( strFieldName.equals("Rate") ) {
            return 14;
        }
        if( strFieldName.equals("PayStartDate") ) {
            return 15;
        }
        if( strFieldName.equals("PayEndDate") ) {
            return 16;
        }
        if( strFieldName.equals("PaytoDate") ) {
            return 17;
        }
        if( strFieldName.equals("PayIntv") ) {
            return 18;
        }
        if( strFieldName.equals("StandPrem") ) {
            return 19;
        }
        if( strFieldName.equals("Prem") ) {
            return 20;
        }
        if( strFieldName.equals("SumPrem") ) {
            return 21;
        }
        if( strFieldName.equals("SuppRiskScore") ) {
            return 22;
        }
        if( strFieldName.equals("FreeFlag") ) {
            return 23;
        }
        if( strFieldName.equals("FreeRate") ) {
            return 24;
        }
        if( strFieldName.equals("FreeStartDate") ) {
            return 25;
        }
        if( strFieldName.equals("FreeEndDate") ) {
            return 26;
        }
        if( strFieldName.equals("State") ) {
            return 27;
        }
        if( strFieldName.equals("ManageCom") ) {
            return 28;
        }
        if( strFieldName.equals("Operator") ) {
            return 29;
        }
        if( strFieldName.equals("MakeDate") ) {
            return 30;
        }
        if( strFieldName.equals("MakeTime") ) {
            return 31;
        }
        if( strFieldName.equals("ModifyDate") ) {
            return 32;
        }
        if( strFieldName.equals("ModifyTime") ) {
            return 33;
        }
        if( strFieldName.equals("AddFeeDirect") ) {
            return 34;
        }
        if( strFieldName.equals("SecInsuAddPoint") ) {
            return 35;
        }
        if( strFieldName.equals("AddForm") ) {
            return 36;
        }
        if( strFieldName.equals("PeriodPrem") ) {
            return 37;
        }
        if( strFieldName.equals("PayType") ) {
            return 38;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "PremID";
                break;
            case 1:
                strFieldName = "ShardingID";
                break;
            case 2:
                strFieldName = "EdorNo";
                break;
            case 3:
                strFieldName = "GrpContNo";
                break;
            case 4:
                strFieldName = "ContNo";
                break;
            case 5:
                strFieldName = "PolNo";
                break;
            case 6:
                strFieldName = "DutyCode";
                break;
            case 7:
                strFieldName = "PayPlanCode";
                break;
            case 8:
                strFieldName = "PayPlanType";
                break;
            case 9:
                strFieldName = "AppntType";
                break;
            case 10:
                strFieldName = "AppntNo";
                break;
            case 11:
                strFieldName = "UrgePayFlag";
                break;
            case 12:
                strFieldName = "NeedAcc";
                break;
            case 13:
                strFieldName = "PayTimes";
                break;
            case 14:
                strFieldName = "Rate";
                break;
            case 15:
                strFieldName = "PayStartDate";
                break;
            case 16:
                strFieldName = "PayEndDate";
                break;
            case 17:
                strFieldName = "PaytoDate";
                break;
            case 18:
                strFieldName = "PayIntv";
                break;
            case 19:
                strFieldName = "StandPrem";
                break;
            case 20:
                strFieldName = "Prem";
                break;
            case 21:
                strFieldName = "SumPrem";
                break;
            case 22:
                strFieldName = "SuppRiskScore";
                break;
            case 23:
                strFieldName = "FreeFlag";
                break;
            case 24:
                strFieldName = "FreeRate";
                break;
            case 25:
                strFieldName = "FreeStartDate";
                break;
            case 26:
                strFieldName = "FreeEndDate";
                break;
            case 27:
                strFieldName = "State";
                break;
            case 28:
                strFieldName = "ManageCom";
                break;
            case 29:
                strFieldName = "Operator";
                break;
            case 30:
                strFieldName = "MakeDate";
                break;
            case 31:
                strFieldName = "MakeTime";
                break;
            case 32:
                strFieldName = "ModifyDate";
                break;
            case 33:
                strFieldName = "ModifyTime";
                break;
            case 34:
                strFieldName = "AddFeeDirect";
                break;
            case 35:
                strFieldName = "SecInsuAddPoint";
                break;
            case 36:
                strFieldName = "AddForm";
                break;
            case 37:
                strFieldName = "PeriodPrem";
                break;
            case 38:
                strFieldName = "PayType";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "PREMID":
                return Schema.TYPE_LONG;
            case "SHARDINGID":
                return Schema.TYPE_STRING;
            case "EDORNO":
                return Schema.TYPE_STRING;
            case "GRPCONTNO":
                return Schema.TYPE_STRING;
            case "CONTNO":
                return Schema.TYPE_STRING;
            case "POLNO":
                return Schema.TYPE_STRING;
            case "DUTYCODE":
                return Schema.TYPE_STRING;
            case "PAYPLANCODE":
                return Schema.TYPE_STRING;
            case "PAYPLANTYPE":
                return Schema.TYPE_STRING;
            case "APPNTTYPE":
                return Schema.TYPE_STRING;
            case "APPNTNO":
                return Schema.TYPE_STRING;
            case "URGEPAYFLAG":
                return Schema.TYPE_STRING;
            case "NEEDACC":
                return Schema.TYPE_STRING;
            case "PAYTIMES":
                return Schema.TYPE_INT;
            case "RATE":
                return Schema.TYPE_DOUBLE;
            case "PAYSTARTDATE":
                return Schema.TYPE_STRING;
            case "PAYENDDATE":
                return Schema.TYPE_STRING;
            case "PAYTODATE":
                return Schema.TYPE_STRING;
            case "PAYINTV":
                return Schema.TYPE_INT;
            case "STANDPREM":
                return Schema.TYPE_DOUBLE;
            case "PREM":
                return Schema.TYPE_DOUBLE;
            case "SUMPREM":
                return Schema.TYPE_DOUBLE;
            case "SUPPRISKSCORE":
                return Schema.TYPE_DOUBLE;
            case "FREEFLAG":
                return Schema.TYPE_STRING;
            case "FREERATE":
                return Schema.TYPE_DOUBLE;
            case "FREESTARTDATE":
                return Schema.TYPE_STRING;
            case "FREEENDDATE":
                return Schema.TYPE_STRING;
            case "STATE":
                return Schema.TYPE_STRING;
            case "MANAGECOM":
                return Schema.TYPE_STRING;
            case "OPERATOR":
                return Schema.TYPE_STRING;
            case "MAKEDATE":
                return Schema.TYPE_STRING;
            case "MAKETIME":
                return Schema.TYPE_STRING;
            case "MODIFYDATE":
                return Schema.TYPE_STRING;
            case "MODIFYTIME":
                return Schema.TYPE_STRING;
            case "ADDFEEDIRECT":
                return Schema.TYPE_STRING;
            case "SECINSUADDPOINT":
                return Schema.TYPE_DOUBLE;
            case "ADDFORM":
                return Schema.TYPE_STRING;
            case "PERIODPREM":
                return Schema.TYPE_DOUBLE;
            case "PAYTYPE":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_LONG;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_STRING;
            case 6:
                return Schema.TYPE_STRING;
            case 7:
                return Schema.TYPE_STRING;
            case 8:
                return Schema.TYPE_STRING;
            case 9:
                return Schema.TYPE_STRING;
            case 10:
                return Schema.TYPE_STRING;
            case 11:
                return Schema.TYPE_STRING;
            case 12:
                return Schema.TYPE_STRING;
            case 13:
                return Schema.TYPE_INT;
            case 14:
                return Schema.TYPE_DOUBLE;
            case 15:
                return Schema.TYPE_STRING;
            case 16:
                return Schema.TYPE_STRING;
            case 17:
                return Schema.TYPE_STRING;
            case 18:
                return Schema.TYPE_INT;
            case 19:
                return Schema.TYPE_DOUBLE;
            case 20:
                return Schema.TYPE_DOUBLE;
            case 21:
                return Schema.TYPE_DOUBLE;
            case 22:
                return Schema.TYPE_DOUBLE;
            case 23:
                return Schema.TYPE_STRING;
            case 24:
                return Schema.TYPE_DOUBLE;
            case 25:
                return Schema.TYPE_STRING;
            case 26:
                return Schema.TYPE_STRING;
            case 27:
                return Schema.TYPE_STRING;
            case 28:
                return Schema.TYPE_STRING;
            case 29:
                return Schema.TYPE_STRING;
            case 30:
                return Schema.TYPE_STRING;
            case 31:
                return Schema.TYPE_STRING;
            case 32:
                return Schema.TYPE_STRING;
            case 33:
                return Schema.TYPE_STRING;
            case 34:
                return Schema.TYPE_STRING;
            case 35:
                return Schema.TYPE_DOUBLE;
            case 36:
                return Schema.TYPE_STRING;
            case 37:
                return Schema.TYPE_DOUBLE;
            case 38:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("PremID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PremID));
        }
        if (FCode.equalsIgnoreCase("ShardingID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ShardingID));
        }
        if (FCode.equalsIgnoreCase("EdorNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(EdorNo));
        }
        if (FCode.equalsIgnoreCase("GrpContNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GrpContNo));
        }
        if (FCode.equalsIgnoreCase("ContNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ContNo));
        }
        if (FCode.equalsIgnoreCase("PolNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PolNo));
        }
        if (FCode.equalsIgnoreCase("DutyCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DutyCode));
        }
        if (FCode.equalsIgnoreCase("PayPlanCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PayPlanCode));
        }
        if (FCode.equalsIgnoreCase("PayPlanType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PayPlanType));
        }
        if (FCode.equalsIgnoreCase("AppntType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AppntType));
        }
        if (FCode.equalsIgnoreCase("AppntNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AppntNo));
        }
        if (FCode.equalsIgnoreCase("UrgePayFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(UrgePayFlag));
        }
        if (FCode.equalsIgnoreCase("NeedAcc")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(NeedAcc));
        }
        if (FCode.equalsIgnoreCase("PayTimes")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PayTimes));
        }
        if (FCode.equalsIgnoreCase("Rate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Rate));
        }
        if (FCode.equalsIgnoreCase("PayStartDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PayStartDate));
        }
        if (FCode.equalsIgnoreCase("PayEndDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PayEndDate));
        }
        if (FCode.equalsIgnoreCase("PaytoDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PaytoDate));
        }
        if (FCode.equalsIgnoreCase("PayIntv")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PayIntv));
        }
        if (FCode.equalsIgnoreCase("StandPrem")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StandPrem));
        }
        if (FCode.equalsIgnoreCase("Prem")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Prem));
        }
        if (FCode.equalsIgnoreCase("SumPrem")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SumPrem));
        }
        if (FCode.equalsIgnoreCase("SuppRiskScore")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SuppRiskScore));
        }
        if (FCode.equalsIgnoreCase("FreeFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FreeFlag));
        }
        if (FCode.equalsIgnoreCase("FreeRate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FreeRate));
        }
        if (FCode.equalsIgnoreCase("FreeStartDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FreeStartDate));
        }
        if (FCode.equalsIgnoreCase("FreeEndDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FreeEndDate));
        }
        if (FCode.equalsIgnoreCase("State")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(State));
        }
        if (FCode.equalsIgnoreCase("ManageCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeDate));
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyDate));
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (FCode.equalsIgnoreCase("AddFeeDirect")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AddFeeDirect));
        }
        if (FCode.equalsIgnoreCase("SecInsuAddPoint")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SecInsuAddPoint));
        }
        if (FCode.equalsIgnoreCase("AddForm")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AddForm));
        }
        if (FCode.equalsIgnoreCase("PeriodPrem")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PeriodPrem));
        }
        if (FCode.equalsIgnoreCase("PayType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PayType));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = String.valueOf(PremID);
                break;
            case 1:
                strFieldValue = String.valueOf(ShardingID);
                break;
            case 2:
                strFieldValue = String.valueOf(EdorNo);
                break;
            case 3:
                strFieldValue = String.valueOf(GrpContNo);
                break;
            case 4:
                strFieldValue = String.valueOf(ContNo);
                break;
            case 5:
                strFieldValue = String.valueOf(PolNo);
                break;
            case 6:
                strFieldValue = String.valueOf(DutyCode);
                break;
            case 7:
                strFieldValue = String.valueOf(PayPlanCode);
                break;
            case 8:
                strFieldValue = String.valueOf(PayPlanType);
                break;
            case 9:
                strFieldValue = String.valueOf(AppntType);
                break;
            case 10:
                strFieldValue = String.valueOf(AppntNo);
                break;
            case 11:
                strFieldValue = String.valueOf(UrgePayFlag);
                break;
            case 12:
                strFieldValue = String.valueOf(NeedAcc);
                break;
            case 13:
                strFieldValue = String.valueOf(PayTimes);
                break;
            case 14:
                strFieldValue = String.valueOf(Rate);
                break;
            case 15:
                strFieldValue = String.valueOf(PayStartDate);
                break;
            case 16:
                strFieldValue = String.valueOf(PayEndDate);
                break;
            case 17:
                strFieldValue = String.valueOf(PaytoDate);
                break;
            case 18:
                strFieldValue = String.valueOf(PayIntv);
                break;
            case 19:
                strFieldValue = String.valueOf(StandPrem);
                break;
            case 20:
                strFieldValue = String.valueOf(Prem);
                break;
            case 21:
                strFieldValue = String.valueOf(SumPrem);
                break;
            case 22:
                strFieldValue = String.valueOf(SuppRiskScore);
                break;
            case 23:
                strFieldValue = String.valueOf(FreeFlag);
                break;
            case 24:
                strFieldValue = String.valueOf(FreeRate);
                break;
            case 25:
                strFieldValue = String.valueOf(FreeStartDate);
                break;
            case 26:
                strFieldValue = String.valueOf(FreeEndDate);
                break;
            case 27:
                strFieldValue = String.valueOf(State);
                break;
            case 28:
                strFieldValue = String.valueOf(ManageCom);
                break;
            case 29:
                strFieldValue = String.valueOf(Operator);
                break;
            case 30:
                strFieldValue = String.valueOf(MakeDate);
                break;
            case 31:
                strFieldValue = String.valueOf(MakeTime);
                break;
            case 32:
                strFieldValue = String.valueOf(ModifyDate);
                break;
            case 33:
                strFieldValue = String.valueOf(ModifyTime);
                break;
            case 34:
                strFieldValue = String.valueOf(AddFeeDirect);
                break;
            case 35:
                strFieldValue = String.valueOf(SecInsuAddPoint);
                break;
            case 36:
                strFieldValue = String.valueOf(AddForm);
                break;
            case 37:
                strFieldValue = String.valueOf(PeriodPrem);
                break;
            case 38:
                strFieldValue = String.valueOf(PayType);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("PremID")) {
            if( FValue != null && !FValue.equals("")) {
                PremID = new Long(FValue).longValue();
            }
        }
        if (FCode.equalsIgnoreCase("ShardingID")) {
            if( FValue != null && !FValue.equals(""))
            {
                ShardingID = FValue.trim();
            }
            else
                ShardingID = null;
        }
        if (FCode.equalsIgnoreCase("EdorNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                EdorNo = FValue.trim();
            }
            else
                EdorNo = null;
        }
        if (FCode.equalsIgnoreCase("GrpContNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                GrpContNo = FValue.trim();
            }
            else
                GrpContNo = null;
        }
        if (FCode.equalsIgnoreCase("ContNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                ContNo = FValue.trim();
            }
            else
                ContNo = null;
        }
        if (FCode.equalsIgnoreCase("PolNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                PolNo = FValue.trim();
            }
            else
                PolNo = null;
        }
        if (FCode.equalsIgnoreCase("DutyCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                DutyCode = FValue.trim();
            }
            else
                DutyCode = null;
        }
        if (FCode.equalsIgnoreCase("PayPlanCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                PayPlanCode = FValue.trim();
            }
            else
                PayPlanCode = null;
        }
        if (FCode.equalsIgnoreCase("PayPlanType")) {
            if( FValue != null && !FValue.equals(""))
            {
                PayPlanType = FValue.trim();
            }
            else
                PayPlanType = null;
        }
        if (FCode.equalsIgnoreCase("AppntType")) {
            if( FValue != null && !FValue.equals(""))
            {
                AppntType = FValue.trim();
            }
            else
                AppntType = null;
        }
        if (FCode.equalsIgnoreCase("AppntNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                AppntNo = FValue.trim();
            }
            else
                AppntNo = null;
        }
        if (FCode.equalsIgnoreCase("UrgePayFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                UrgePayFlag = FValue.trim();
            }
            else
                UrgePayFlag = null;
        }
        if (FCode.equalsIgnoreCase("NeedAcc")) {
            if( FValue != null && !FValue.equals(""))
            {
                NeedAcc = FValue.trim();
            }
            else
                NeedAcc = null;
        }
        if (FCode.equalsIgnoreCase("PayTimes")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                PayTimes = i;
            }
        }
        if (FCode.equalsIgnoreCase("Rate")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                Rate = d;
            }
        }
        if (FCode.equalsIgnoreCase("PayStartDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                PayStartDate = FValue.trim();
            }
            else
                PayStartDate = null;
        }
        if (FCode.equalsIgnoreCase("PayEndDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                PayEndDate = FValue.trim();
            }
            else
                PayEndDate = null;
        }
        if (FCode.equalsIgnoreCase("PaytoDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                PaytoDate = FValue.trim();
            }
            else
                PaytoDate = null;
        }
        if (FCode.equalsIgnoreCase("PayIntv")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                PayIntv = i;
            }
        }
        if (FCode.equalsIgnoreCase("StandPrem")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                StandPrem = d;
            }
        }
        if (FCode.equalsIgnoreCase("Prem")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                Prem = d;
            }
        }
        if (FCode.equalsIgnoreCase("SumPrem")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                SumPrem = d;
            }
        }
        if (FCode.equalsIgnoreCase("SuppRiskScore")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                SuppRiskScore = d;
            }
        }
        if (FCode.equalsIgnoreCase("FreeFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                FreeFlag = FValue.trim();
            }
            else
                FreeFlag = null;
        }
        if (FCode.equalsIgnoreCase("FreeRate")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                FreeRate = d;
            }
        }
        if (FCode.equalsIgnoreCase("FreeStartDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                FreeStartDate = FValue.trim();
            }
            else
                FreeStartDate = null;
        }
        if (FCode.equalsIgnoreCase("FreeEndDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                FreeEndDate = FValue.trim();
            }
            else
                FreeEndDate = null;
        }
        if (FCode.equalsIgnoreCase("State")) {
            if( FValue != null && !FValue.equals(""))
            {
                State = FValue.trim();
            }
            else
                State = null;
        }
        if (FCode.equalsIgnoreCase("ManageCom")) {
            if( FValue != null && !FValue.equals(""))
            {
                ManageCom = FValue.trim();
            }
            else
                ManageCom = null;
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            if( FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
                Operator = null;
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeDate = FValue.trim();
            }
            else
                MakeDate = null;
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
                MakeTime = null;
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyDate = FValue.trim();
            }
            else
                ModifyDate = null;
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
                ModifyTime = null;
        }
        if (FCode.equalsIgnoreCase("AddFeeDirect")) {
            if( FValue != null && !FValue.equals(""))
            {
                AddFeeDirect = FValue.trim();
            }
            else
                AddFeeDirect = null;
        }
        if (FCode.equalsIgnoreCase("SecInsuAddPoint")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                SecInsuAddPoint = d;
            }
        }
        if (FCode.equalsIgnoreCase("AddForm")) {
            if( FValue != null && !FValue.equals(""))
            {
                AddForm = FValue.trim();
            }
            else
                AddForm = null;
        }
        if (FCode.equalsIgnoreCase("PeriodPrem")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                PeriodPrem = d;
            }
        }
        if (FCode.equalsIgnoreCase("PayType")) {
            if( FValue != null && !FValue.equals(""))
            {
                PayType = FValue.trim();
            }
            else
                PayType = null;
        }
        return true;
    }


    public String toString() {
    return "LBPremPojo [" +
            "PremID="+PremID +
            ", ShardingID="+ShardingID +
            ", EdorNo="+EdorNo +
            ", GrpContNo="+GrpContNo +
            ", ContNo="+ContNo +
            ", PolNo="+PolNo +
            ", DutyCode="+DutyCode +
            ", PayPlanCode="+PayPlanCode +
            ", PayPlanType="+PayPlanType +
            ", AppntType="+AppntType +
            ", AppntNo="+AppntNo +
            ", UrgePayFlag="+UrgePayFlag +
            ", NeedAcc="+NeedAcc +
            ", PayTimes="+PayTimes +
            ", Rate="+Rate +
            ", PayStartDate="+PayStartDate +
            ", PayEndDate="+PayEndDate +
            ", PaytoDate="+PaytoDate +
            ", PayIntv="+PayIntv +
            ", StandPrem="+StandPrem +
            ", Prem="+Prem +
            ", SumPrem="+SumPrem +
            ", SuppRiskScore="+SuppRiskScore +
            ", FreeFlag="+FreeFlag +
            ", FreeRate="+FreeRate +
            ", FreeStartDate="+FreeStartDate +
            ", FreeEndDate="+FreeEndDate +
            ", State="+State +
            ", ManageCom="+ManageCom +
            ", Operator="+Operator +
            ", MakeDate="+MakeDate +
            ", MakeTime="+MakeTime +
            ", ModifyDate="+ModifyDate +
            ", ModifyTime="+ModifyTime +
            ", AddFeeDirect="+AddFeeDirect +
            ", SecInsuAddPoint="+SecInsuAddPoint +
            ", AddForm="+AddForm +
            ", PeriodPrem="+PeriodPrem +
            ", PayType="+PayType +"]";
    }
}
