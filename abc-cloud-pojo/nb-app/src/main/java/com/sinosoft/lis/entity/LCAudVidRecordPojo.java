/**
 * Copyright (c) 2017 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.entity;

import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.Pojo;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;

import java.io.Serializable;

/**
 * <p>ClassName: LCAudVidRecordPojo </p>
 * <p>Copyright: Copyright (c) 2017</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2017-11-13
 */
public class LCAudVidRecordPojo implements Pojo,Serializable {
    // @Field
    /** Prtno */
    private String PrtNo; 
    /** Isrecord */
    private String IsRecord; 
    /** Isquailty */
    private String IsQuailty; 
    /** Donerecorddate */
    private String  DoneRecordDate;
    /** Donerecordtime */
    private String DoneRecordTime; 
    /** Quailtyresult */
    private String QuailtyResult; 
    /** Quailtydate */
    private String  QuailtyDate;
    /** Qualitytime */
    private String QualityTime; 
    /** 操作员 */
    private String Operator; 
    /** 入机日期 */
    private String  MakeDate;
    /** 入机时间 */
    private String MakeTime; 
    /** 最后一次修改日期 */
    private String  ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime; 
    /** Remark */
    private String Remark; 


    public static final int FIELDNUM = 14;    // 数据库表的字段个数

    private FDate fDate = new FDate();        // 处理日期
    public String getPrtNo() {
        return PrtNo;
    }
    public void setPrtNo(String aPrtNo) {
        PrtNo = aPrtNo;
    }
    public String getIsRecord() {
        return IsRecord;
    }
    public void setIsRecord(String aIsRecord) {
        IsRecord = aIsRecord;
    }
    public String getIsQuailty() {
        return IsQuailty;
    }
    public void setIsQuailty(String aIsQuailty) {
        IsQuailty = aIsQuailty;
    }
    public String getDoneRecordDate() {
        return DoneRecordDate;
    }
    public void setDoneRecordDate(String aDoneRecordDate) {
        DoneRecordDate = aDoneRecordDate;
    }
    public String getDoneRecordTime() {
        return DoneRecordTime;
    }
    public void setDoneRecordTime(String aDoneRecordTime) {
        DoneRecordTime = aDoneRecordTime;
    }
    public String getQuailtyResult() {
        return QuailtyResult;
    }
    public void setQuailtyResult(String aQuailtyResult) {
        QuailtyResult = aQuailtyResult;
    }
    public String getQuailtyDate() {
        return QuailtyDate;
    }
    public void setQuailtyDate(String aQuailtyDate) {
        QuailtyDate = aQuailtyDate;
    }
    public String getQualityTime() {
        return QualityTime;
    }
    public void setQualityTime(String aQualityTime) {
        QualityTime = aQualityTime;
    }
    public String getOperator() {
        return Operator;
    }
    public void setOperator(String aOperator) {
        Operator = aOperator;
    }
    public String getMakeDate() {
        return MakeDate;
    }
    public void setMakeDate(String aMakeDate) {
        MakeDate = aMakeDate;
    }
    public String getMakeTime() {
        return MakeTime;
    }
    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }
    public String getModifyDate() {
        return ModifyDate;
    }
    public void setModifyDate(String aModifyDate) {
        ModifyDate = aModifyDate;
    }
    public String getModifyTime() {
        return ModifyTime;
    }
    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }
    public String getRemark() {
        return Remark;
    }
    public void setRemark(String aRemark) {
        Remark = aRemark;
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("PrtNo") ) {
            return 0;
        }
        if( strFieldName.equals("IsRecord") ) {
            return 1;
        }
        if( strFieldName.equals("IsQuailty") ) {
            return 2;
        }
        if( strFieldName.equals("DoneRecordDate") ) {
            return 3;
        }
        if( strFieldName.equals("DoneRecordTime") ) {
            return 4;
        }
        if( strFieldName.equals("QuailtyResult") ) {
            return 5;
        }
        if( strFieldName.equals("QuailtyDate") ) {
            return 6;
        }
        if( strFieldName.equals("QualityTime") ) {
            return 7;
        }
        if( strFieldName.equals("Operator") ) {
            return 8;
        }
        if( strFieldName.equals("MakeDate") ) {
            return 9;
        }
        if( strFieldName.equals("MakeTime") ) {
            return 10;
        }
        if( strFieldName.equals("ModifyDate") ) {
            return 11;
        }
        if( strFieldName.equals("ModifyTime") ) {
            return 12;
        }
        if( strFieldName.equals("Remark") ) {
            return 13;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "PrtNo";
                break;
            case 1:
                strFieldName = "IsRecord";
                break;
            case 2:
                strFieldName = "IsQuailty";
                break;
            case 3:
                strFieldName = "DoneRecordDate";
                break;
            case 4:
                strFieldName = "DoneRecordTime";
                break;
            case 5:
                strFieldName = "QuailtyResult";
                break;
            case 6:
                strFieldName = "QuailtyDate";
                break;
            case 7:
                strFieldName = "QualityTime";
                break;
            case 8:
                strFieldName = "Operator";
                break;
            case 9:
                strFieldName = "MakeDate";
                break;
            case 10:
                strFieldName = "MakeTime";
                break;
            case 11:
                strFieldName = "ModifyDate";
                break;
            case 12:
                strFieldName = "ModifyTime";
                break;
            case 13:
                strFieldName = "Remark";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "PRTNO":
                return Schema.TYPE_STRING;
            case "ISRECORD":
                return Schema.TYPE_STRING;
            case "ISQUAILTY":
                return Schema.TYPE_STRING;
            case "DONERECORDDATE":
                return Schema.TYPE_STRING;
            case "DONERECORDTIME":
                return Schema.TYPE_STRING;
            case "QUAILTYRESULT":
                return Schema.TYPE_STRING;
            case "QUAILTYDATE":
                return Schema.TYPE_STRING;
            case "QUALITYTIME":
                return Schema.TYPE_STRING;
            case "OPERATOR":
                return Schema.TYPE_STRING;
            case "MAKEDATE":
                return Schema.TYPE_STRING;
            case "MAKETIME":
                return Schema.TYPE_STRING;
            case "MODIFYDATE":
                return Schema.TYPE_STRING;
            case "MODIFYTIME":
                return Schema.TYPE_STRING;
            case "REMARK":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_STRING;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_STRING;
            case 6:
                return Schema.TYPE_STRING;
            case 7:
                return Schema.TYPE_STRING;
            case 8:
                return Schema.TYPE_STRING;
            case 9:
                return Schema.TYPE_STRING;
            case 10:
                return Schema.TYPE_STRING;
            case 11:
                return Schema.TYPE_STRING;
            case 12:
                return Schema.TYPE_STRING;
            case 13:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("PrtNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PrtNo));
        }
        if (FCode.equalsIgnoreCase("IsRecord")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(IsRecord));
        }
        if (FCode.equalsIgnoreCase("IsQuailty")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(IsQuailty));
        }
        if (FCode.equalsIgnoreCase("DoneRecordDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DoneRecordDate));
        }
        if (FCode.equalsIgnoreCase("DoneRecordTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DoneRecordTime));
        }
        if (FCode.equalsIgnoreCase("QuailtyResult")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(QuailtyResult));
        }
        if (FCode.equalsIgnoreCase("QuailtyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(QuailtyDate));
        }
        if (FCode.equalsIgnoreCase("QualityTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(QualityTime));
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeDate));
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyDate));
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (FCode.equalsIgnoreCase("Remark")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Remark));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = String.valueOf(PrtNo);
                break;
            case 1:
                strFieldValue = String.valueOf(IsRecord);
                break;
            case 2:
                strFieldValue = String.valueOf(IsQuailty);
                break;
            case 3:
                strFieldValue = String.valueOf(DoneRecordDate);
                break;
            case 4:
                strFieldValue = String.valueOf(DoneRecordTime);
                break;
            case 5:
                strFieldValue = String.valueOf(QuailtyResult);
                break;
            case 6:
                strFieldValue = String.valueOf(QuailtyDate);
                break;
            case 7:
                strFieldValue = String.valueOf(QualityTime);
                break;
            case 8:
                strFieldValue = String.valueOf(Operator);
                break;
            case 9:
                strFieldValue = String.valueOf(MakeDate);
                break;
            case 10:
                strFieldValue = String.valueOf(MakeTime);
                break;
            case 11:
                strFieldValue = String.valueOf(ModifyDate);
                break;
            case 12:
                strFieldValue = String.valueOf(ModifyTime);
                break;
            case 13:
                strFieldValue = String.valueOf(Remark);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("PrtNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                PrtNo = FValue.trim();
            }
            else
                PrtNo = null;
        }
        if (FCode.equalsIgnoreCase("IsRecord")) {
            if( FValue != null && !FValue.equals(""))
            {
                IsRecord = FValue.trim();
            }
            else
                IsRecord = null;
        }
        if (FCode.equalsIgnoreCase("IsQuailty")) {
            if( FValue != null && !FValue.equals(""))
            {
                IsQuailty = FValue.trim();
            }
            else
                IsQuailty = null;
        }
        if (FCode.equalsIgnoreCase("DoneRecordDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                DoneRecordDate = FValue.trim();
            }
            else
                DoneRecordDate = null;
        }
        if (FCode.equalsIgnoreCase("DoneRecordTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                DoneRecordTime = FValue.trim();
            }
            else
                DoneRecordTime = null;
        }
        if (FCode.equalsIgnoreCase("QuailtyResult")) {
            if( FValue != null && !FValue.equals(""))
            {
                QuailtyResult = FValue.trim();
            }
            else
                QuailtyResult = null;
        }
        if (FCode.equalsIgnoreCase("QuailtyDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                QuailtyDate = FValue.trim();
            }
            else
                QuailtyDate = null;
        }
        if (FCode.equalsIgnoreCase("QualityTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                QualityTime = FValue.trim();
            }
            else
                QualityTime = null;
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            if( FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
                Operator = null;
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeDate = FValue.trim();
            }
            else
                MakeDate = null;
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
                MakeTime = null;
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyDate = FValue.trim();
            }
            else
                ModifyDate = null;
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
                ModifyTime = null;
        }
        if (FCode.equalsIgnoreCase("Remark")) {
            if( FValue != null && !FValue.equals(""))
            {
                Remark = FValue.trim();
            }
            else
                Remark = null;
        }
        return true;
    }


    public String toString() {
    return "LCAudVidRecordPojo [" +
            "PrtNo="+PrtNo +
            ", IsRecord="+IsRecord +
            ", IsQuailty="+IsQuailty +
            ", DoneRecordDate="+DoneRecordDate +
            ", DoneRecordTime="+DoneRecordTime +
            ", QuailtyResult="+QuailtyResult +
            ", QuailtyDate="+QuailtyDate +
            ", QualityTime="+QualityTime +
            ", Operator="+Operator +
            ", MakeDate="+MakeDate +
            ", MakeTime="+MakeTime +
            ", ModifyDate="+ModifyDate +
            ", ModifyTime="+ModifyTime +
            ", Remark="+Remark +"]";
    }
}
