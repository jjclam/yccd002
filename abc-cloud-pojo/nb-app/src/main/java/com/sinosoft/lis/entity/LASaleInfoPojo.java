/**
 * Copyright (c) 2019 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.entity;

import java.io.*;
import java.util.Date;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.StrTool;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * <p>ClassName: LASaleInfoPojo </p>
 * <p>Copyright: Copyright (c) 2019</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2019-11-20
 */
public class LASaleInfoPojo implements  Pojo,Serializable {
    // @Field
    /** 渠道编码 */
    private String ChannelCode; 
    /** 渠道名称 */
    private String ChannelName; 
    /** 机构编码 */
    private String ComCode; 
    /** 销售渠道 */
    private String SaleChnl; 
    /** 机构代码 */
    private String ManageCom; 
    /** 备用字段1 */
    private String Remark1; 
    /** 备用字段2 */
    private String Remark2; 


    public static final int FIELDNUM = 7;    // 数据库表的字段个数
    public String getChannelCode() {
        return ChannelCode;
    }
    public void setChannelCode(String aChannelCode) {
        ChannelCode = aChannelCode;
    }
    public String getChannelName() {
        return ChannelName;
    }
    public void setChannelName(String aChannelName) {
        ChannelName = aChannelName;
    }
    public String getComCode() {
        return ComCode;
    }
    public void setComCode(String aComCode) {
        ComCode = aComCode;
    }
    public String getSaleChnl() {
        return SaleChnl;
    }
    public void setSaleChnl(String aSaleChnl) {
        SaleChnl = aSaleChnl;
    }
    public String getManageCom() {
        return ManageCom;
    }
    public void setManageCom(String aManageCom) {
        ManageCom = aManageCom;
    }
    public String getRemark1() {
        return Remark1;
    }
    public void setRemark1(String aRemark1) {
        Remark1 = aRemark1;
    }
    public String getRemark2() {
        return Remark2;
    }
    public void setRemark2(String aRemark2) {
        Remark2 = aRemark2;
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("ChannelCode") ) {
            return 0;
        }
        if( strFieldName.equals("ChannelName") ) {
            return 1;
        }
        if( strFieldName.equals("ComCode") ) {
            return 2;
        }
        if( strFieldName.equals("SaleChnl") ) {
            return 3;
        }
        if( strFieldName.equals("ManageCom") ) {
            return 4;
        }
        if( strFieldName.equals("Remark1") ) {
            return 5;
        }
        if( strFieldName.equals("Remark2") ) {
            return 6;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "ChannelCode";
                break;
            case 1:
                strFieldName = "ChannelName";
                break;
            case 2:
                strFieldName = "ComCode";
                break;
            case 3:
                strFieldName = "SaleChnl";
                break;
            case 4:
                strFieldName = "ManageCom";
                break;
            case 5:
                strFieldName = "Remark1";
                break;
            case 6:
                strFieldName = "Remark2";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "CHANNELCODE":
                return Schema.TYPE_STRING;
            case "CHANNELNAME":
                return Schema.TYPE_STRING;
            case "COMCODE":
                return Schema.TYPE_STRING;
            case "SALECHNL":
                return Schema.TYPE_STRING;
            case "MANAGECOM":
                return Schema.TYPE_STRING;
            case "REMARK1":
                return Schema.TYPE_STRING;
            case "REMARK2":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_STRING;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_STRING;
            case 6:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("ChannelCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ChannelCode));
        }
        if (FCode.equalsIgnoreCase("ChannelName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ChannelName));
        }
        if (FCode.equalsIgnoreCase("ComCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ComCode));
        }
        if (FCode.equalsIgnoreCase("SaleChnl")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SaleChnl));
        }
        if (FCode.equalsIgnoreCase("ManageCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
        }
        if (FCode.equalsIgnoreCase("Remark1")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Remark1));
        }
        if (FCode.equalsIgnoreCase("Remark2")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Remark2));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = String.valueOf(ChannelCode);
                break;
            case 1:
                strFieldValue = String.valueOf(ChannelName);
                break;
            case 2:
                strFieldValue = String.valueOf(ComCode);
                break;
            case 3:
                strFieldValue = String.valueOf(SaleChnl);
                break;
            case 4:
                strFieldValue = String.valueOf(ManageCom);
                break;
            case 5:
                strFieldValue = String.valueOf(Remark1);
                break;
            case 6:
                strFieldValue = String.valueOf(Remark2);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("ChannelCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                ChannelCode = FValue.trim();
            }
            else
                ChannelCode = null;
        }
        if (FCode.equalsIgnoreCase("ChannelName")) {
            if( FValue != null && !FValue.equals(""))
            {
                ChannelName = FValue.trim();
            }
            else
                ChannelName = null;
        }
        if (FCode.equalsIgnoreCase("ComCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                ComCode = FValue.trim();
            }
            else
                ComCode = null;
        }
        if (FCode.equalsIgnoreCase("SaleChnl")) {
            if( FValue != null && !FValue.equals(""))
            {
                SaleChnl = FValue.trim();
            }
            else
                SaleChnl = null;
        }
        if (FCode.equalsIgnoreCase("ManageCom")) {
            if( FValue != null && !FValue.equals(""))
            {
                ManageCom = FValue.trim();
            }
            else
                ManageCom = null;
        }
        if (FCode.equalsIgnoreCase("Remark1")) {
            if( FValue != null && !FValue.equals(""))
            {
                Remark1 = FValue.trim();
            }
            else
                Remark1 = null;
        }
        if (FCode.equalsIgnoreCase("Remark2")) {
            if( FValue != null && !FValue.equals(""))
            {
                Remark2 = FValue.trim();
            }
            else
                Remark2 = null;
        }
        return true;
    }


    public String toString() {
    return "LASaleInfoPojo [" +
            "ChannelCode="+ChannelCode +
            ", ChannelName="+ChannelName +
            ", ComCode="+ComCode +
            ", SaleChnl="+SaleChnl +
            ", ManageCom="+ManageCom +
            ", Remark1="+Remark1 +
            ", Remark2="+Remark2 +"]";
    }
}
