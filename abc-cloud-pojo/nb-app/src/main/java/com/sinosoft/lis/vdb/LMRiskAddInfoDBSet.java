/**
 * Copyright (c) 2019 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.vdb;

import java.sql.*;
import com.sinosoft.lis.schema.LMRiskAddInfoSchema;
import com.sinosoft.lis.vschema.LMRiskAddInfoSet;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;

/**
 * <p>ClassName: LMRiskAddInfoDBSet </p>
 * <p>Description: DB层多记录数据库操作类文件 </p>
 * <p>Copyright: Copyright (c) 2019</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2019-01-15
 */
public class LMRiskAddInfoDBSet extends LMRiskAddInfoSet {
    // @Field
    private Connection con;
    private DBOper db;
    /**
    * flag = true: 传入Connection
    * flag = false: 不传入Connection
    **/
    private boolean mflag = false;

    // @Constructor
    public LMRiskAddInfoDBSet(Connection tConnection) {
        con = tConnection;
        db = new DBOper(con,"LMRiskAddInfo");
        mflag = true;
    }

    public LMRiskAddInfoDBSet() {
        db = new DBOper( "LMRiskAddInfo" );
    }
    // @Method
    public boolean deleteSQL() {
        if (db.deleteSQL(this)) {
            return true;
        } else {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LMRiskAddInfoDBSet";
            tError.functionName = "deleteSQL";
            tError.errorMessage = "操作失败!";
            this.mErrors .addOneError(tError);
            return false;
        }
    }

    /**
     * 删除操作
     * 删除条件：主键
     * @return boolean
     */
    public boolean delete() {
        PreparedStatement pstmt = null;

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try {
            int tCount = this.size();
            pstmt = con.prepareStatement("DELETE FROM LMRiskAddInfo WHERE  1=1  AND AddInfoId = ?");
            for (int i = 1; i <= tCount; i++) {
            if(this.get(i).getAddInfoId() == null || this.get(i).getAddInfoId().equals("null")) {
            	pstmt.setString(1,null);
            } else {
            	pstmt.setString(1, this.get(i).getAddInfoId());
            }
                pstmt.addBatch();
            }
            pstmt.executeBatch();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            ex.printStackTrace();
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LMRiskAddInfoDBSet";
            tError.functionName = "delete()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            try {
                pstmt.close();
            } catch (Exception e){
                e.printStackTrace();
            }

            if (!mflag) {
                try {
                    con.close();
                } catch (Exception e){e.printStackTrace();}
            }

            return false;
        }

        if( !mflag ) {
            try {
                con.close();
            } catch (Exception e){e.printStackTrace();}
        }

        return true;
    }

    /**
     * 更新操作
     * 更新条件：主键
     * @return boolean
     */
    public boolean update() {
        PreparedStatement pstmt = null;

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try {
            int tCount = this.size();
            pstmt = con.prepareStatement("UPDATE LMRiskAddInfo SET  AddInfoId = ? , RiskCode = ? , SellType = ? , PayIntv = ? , PayYears = ? , InsuYear = ? , InsuYearFlag = ? , PayEndYear = ? , PayEndYearFlag = ? , GetYearFlag = ? , GetYear = ? , RnewFlag = ? , BonusGetMode = ? , LiveGetMode = ? , PremPerUnit = ? WHERE  1=1  AND AddInfoId = ?");
            for (int i = 1; i <= tCount; i++) {
            if(this.get(i).getAddInfoId() == null || this.get(i).getAddInfoId().equals("null")) {
            	pstmt.setString(1,null);
            } else {
            	pstmt.setString(1, this.get(i).getAddInfoId());
            }
            if(this.get(i).getRiskCode() == null || this.get(i).getRiskCode().equals("null")) {
            	pstmt.setString(2,null);
            } else {
            	pstmt.setString(2, this.get(i).getRiskCode());
            }
            if(this.get(i).getSellType() == null || this.get(i).getSellType().equals("null")) {
            	pstmt.setString(3,null);
            } else {
            	pstmt.setString(3, this.get(i).getSellType());
            }
            pstmt.setInt(4, this.get(i).getPayIntv());
            pstmt.setInt(5, this.get(i).getPayYears());
            pstmt.setInt(6, this.get(i).getInsuYear());
            if(this.get(i).getInsuYearFlag() == null || this.get(i).getInsuYearFlag().equals("null")) {
            	pstmt.setString(7,null);
            } else {
            	pstmt.setString(7, this.get(i).getInsuYearFlag());
            }
            pstmt.setInt(8, this.get(i).getPayEndYear());
            if(this.get(i).getPayEndYearFlag() == null || this.get(i).getPayEndYearFlag().equals("null")) {
            	pstmt.setString(9,null);
            } else {
            	pstmt.setString(9, this.get(i).getPayEndYearFlag());
            }
            if(this.get(i).getGetYearFlag() == null || this.get(i).getGetYearFlag().equals("null")) {
            	pstmt.setString(10,null);
            } else {
            	pstmt.setString(10, this.get(i).getGetYearFlag());
            }
            pstmt.setInt(11, this.get(i).getGetYear());
            if(this.get(i).getRnewFlag() == null || this.get(i).getRnewFlag().equals("null")) {
            	pstmt.setString(12,null);
            } else {
            	pstmt.setString(12, this.get(i).getRnewFlag());
            }
            if(this.get(i).getBonusGetMode() == null || this.get(i).getBonusGetMode().equals("null")) {
            	pstmt.setString(13,null);
            } else {
            	pstmt.setString(13, this.get(i).getBonusGetMode());
            }
            if(this.get(i).getLiveGetMode() == null || this.get(i).getLiveGetMode().equals("null")) {
            	pstmt.setString(14,null);
            } else {
            	pstmt.setString(14, this.get(i).getLiveGetMode());
            }
            pstmt.setLong(15, this.get(i).getPremPerUnit());
            // set where condition
            if(this.get(i).getAddInfoId() == null || this.get(i).getAddInfoId().equals("null")) {
            	pstmt.setString(16,null);
            } else {
            	pstmt.setString(16, this.get(i).getAddInfoId());
            }
                pstmt.addBatch();
            }
            pstmt.executeBatch();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            ex.printStackTrace();
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LMRiskAddInfoDBSet";
            tError.functionName = "update()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            try {
                pstmt.close();
            } catch (Exception e){e.printStackTrace();}

            if( !mflag ) {
                try {
                    con.close();
                } catch (Exception e){e.printStackTrace();}
            }

            return false;
        }

        if( !mflag ) {
            try {
                con.close();
            } catch (Exception e){e.printStackTrace();}
        }

        return true;
    }

    /**
     * 新增操作
     * @return boolean
     */
    public boolean insert() {
        PreparedStatement pstmt = null;

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try
        {
            int tCount = this.size();
            pstmt = con.prepareStatement("INSERT INTO LMRiskAddInfo VALUES( ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ?)");
            for (int i = 1; i <= tCount; i++) {
            if(this.get(i).getAddInfoId() == null || this.get(i).getAddInfoId().equals("null")) {
            	pstmt.setString(1,null);
            } else {
            	pstmt.setString(1, this.get(i).getAddInfoId());
            }
            if(this.get(i).getRiskCode() == null || this.get(i).getRiskCode().equals("null")) {
            	pstmt.setString(2,null);
            } else {
            	pstmt.setString(2, this.get(i).getRiskCode());
            }
            if(this.get(i).getSellType() == null || this.get(i).getSellType().equals("null")) {
            	pstmt.setString(3,null);
            } else {
            	pstmt.setString(3, this.get(i).getSellType());
            }
            pstmt.setInt(4, this.get(i).getPayIntv());
            pstmt.setInt(5, this.get(i).getPayYears());
            pstmt.setInt(6, this.get(i).getInsuYear());
            if(this.get(i).getInsuYearFlag() == null || this.get(i).getInsuYearFlag().equals("null")) {
            	pstmt.setString(7,null);
            } else {
            	pstmt.setString(7, this.get(i).getInsuYearFlag());
            }
            pstmt.setInt(8, this.get(i).getPayEndYear());
            if(this.get(i).getPayEndYearFlag() == null || this.get(i).getPayEndYearFlag().equals("null")) {
            	pstmt.setString(9,null);
            } else {
            	pstmt.setString(9, this.get(i).getPayEndYearFlag());
            }
            if(this.get(i).getGetYearFlag() == null || this.get(i).getGetYearFlag().equals("null")) {
            	pstmt.setString(10,null);
            } else {
            	pstmt.setString(10, this.get(i).getGetYearFlag());
            }
            pstmt.setInt(11, this.get(i).getGetYear());
            if(this.get(i).getRnewFlag() == null || this.get(i).getRnewFlag().equals("null")) {
            	pstmt.setString(12,null);
            } else {
            	pstmt.setString(12, this.get(i).getRnewFlag());
            }
            if(this.get(i).getBonusGetMode() == null || this.get(i).getBonusGetMode().equals("null")) {
            	pstmt.setString(13,null);
            } else {
            	pstmt.setString(13, this.get(i).getBonusGetMode());
            }
            if(this.get(i).getLiveGetMode() == null || this.get(i).getLiveGetMode().equals("null")) {
            	pstmt.setString(14,null);
            } else {
            	pstmt.setString(14, this.get(i).getLiveGetMode());
            }
            pstmt.setLong(15, this.get(i).getPremPerUnit());
                pstmt.addBatch();
            }
            pstmt.executeBatch();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            ex.printStackTrace();
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LMRiskAddInfoDBSet";
            tError.functionName = "insert()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            try {
                pstmt.close();
            } catch (Exception e){e.printStackTrace();}

            if( !mflag ) {
                try {
                    con.close();
                } catch (Exception e){e.printStackTrace();}
            }

            return false;
        }

        if( !mflag ) {
            try {
                con.close();
            } catch (Exception e){e.printStackTrace();}
        }

        return true;
    }
}
