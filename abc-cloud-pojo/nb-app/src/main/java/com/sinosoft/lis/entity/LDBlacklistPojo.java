/**
 * Copyright (c) 2017 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.entity;

import com.sinosoft.cloud.cache.annotation.RedisPrimaryHKey;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.Pojo;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;

import java.io.Serializable;

/**
 * <p>ClassName: LDBlacklistPojo </p>
 * <p>Copyright: Copyright (c) 2017</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2017-10-17
 */
public class LDBlacklistPojo implements Pojo,Serializable {
    // @Field
    /** 序列号 */
    @RedisPrimaryHKey
    private String SerialNo; 
    /** 黑名单客户号码 */
    private String BlacklistNo; 
    /** 黑名单客户类型 */
    private String BlacklistType; 
    /** 黑名单名称 */
    private String BlackName; 
    /** 证件类型 */
    private String IDType; 
    /** 证件号码 */
    private String IDNo; 
    /** 出生日期 */
    private String  BirthDate;
    /** 性别 */
    private String Sex; 
    /** 黑名单操作员 */
    private String BlacklistOperator; 
    /** 黑名单日期 */
    private String  BlacklistMakeDate;
    /** 黑名单时间 */
    private String BlacklistMakeTime; 
    /** 黑名单原因描述 */
    private String BlacklistReason; 
    /** 入机日期 */
    private String  MakeDate;
    /** 入机时间 */
    private String MakeTime; 
    /** 最后一次修改日期 */
    private String  ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime; 
    /** 备用字段1 */
    private String StandByFlag1; 
    /** 备用字段2 */
    private String StandByFlag2; 
    /** 备用字段3 */
    private String StandByFlag3; 
    /** 备用字段4 */
    private String StandByFlag4; 
    /** 备用字段5 */
    private String StandByFlag5; 


    public static final int FIELDNUM = 21;    // 数据库表的字段个数

    private FDate fDate = new FDate();        // 处理日期
    public String getSerialNo() {
        return SerialNo;
    }
    public void setSerialNo(String aSerialNo) {
        SerialNo = aSerialNo;
    }
    public String getBlacklistNo() {
        return BlacklistNo;
    }
    public void setBlacklistNo(String aBlacklistNo) {
        BlacklistNo = aBlacklistNo;
    }
    public String getBlacklistType() {
        return BlacklistType;
    }
    public void setBlacklistType(String aBlacklistType) {
        BlacklistType = aBlacklistType;
    }
    public String getBlackName() {
        return BlackName;
    }
    public void setBlackName(String aBlackName) {
        BlackName = aBlackName;
    }
    public String getIDType() {
        return IDType;
    }
    public void setIDType(String aIDType) {
        IDType = aIDType;
    }
    public String getIDNo() {
        return IDNo;
    }
    public void setIDNo(String aIDNo) {
        IDNo = aIDNo;
    }
    public String getBirthDate() {
        return BirthDate;
    }
    public void setBirthDate(String aBirthDate) {
        BirthDate = aBirthDate;
    }
    public String getSex() {
        return Sex;
    }
    public void setSex(String aSex) {
        Sex = aSex;
    }
    public String getBlacklistOperator() {
        return BlacklistOperator;
    }
    public void setBlacklistOperator(String aBlacklistOperator) {
        BlacklistOperator = aBlacklistOperator;
    }
    public String getBlacklistMakeDate() {
        return BlacklistMakeDate;
    }
    public void setBlacklistMakeDate(String aBlacklistMakeDate) {
        BlacklistMakeDate = aBlacklistMakeDate;
    }
    public String getBlacklistMakeTime() {
        return BlacklistMakeTime;
    }
    public void setBlacklistMakeTime(String aBlacklistMakeTime) {
        BlacklistMakeTime = aBlacklistMakeTime;
    }
    public String getBlacklistReason() {
        return BlacklistReason;
    }
    public void setBlacklistReason(String aBlacklistReason) {
        BlacklistReason = aBlacklistReason;
    }
    public String getMakeDate() {
        return MakeDate;
    }
    public void setMakeDate(String aMakeDate) {
        MakeDate = aMakeDate;
    }
    public String getMakeTime() {
        return MakeTime;
    }
    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }
    public String getModifyDate() {
        return ModifyDate;
    }
    public void setModifyDate(String aModifyDate) {
        ModifyDate = aModifyDate;
    }
    public String getModifyTime() {
        return ModifyTime;
    }
    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }
    public String getStandByFlag1() {
        return StandByFlag1;
    }
    public void setStandByFlag1(String aStandByFlag1) {
        StandByFlag1 = aStandByFlag1;
    }
    public String getStandByFlag2() {
        return StandByFlag2;
    }
    public void setStandByFlag2(String aStandByFlag2) {
        StandByFlag2 = aStandByFlag2;
    }
    public String getStandByFlag3() {
        return StandByFlag3;
    }
    public void setStandByFlag3(String aStandByFlag3) {
        StandByFlag3 = aStandByFlag3;
    }
    public String getStandByFlag4() {
        return StandByFlag4;
    }
    public void setStandByFlag4(String aStandByFlag4) {
        StandByFlag4 = aStandByFlag4;
    }
    public String getStandByFlag5() {
        return StandByFlag5;
    }
    public void setStandByFlag5(String aStandByFlag5) {
        StandByFlag5 = aStandByFlag5;
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("SerialNo") ) {
            return 0;
        }
        if( strFieldName.equals("BlacklistNo") ) {
            return 1;
        }
        if( strFieldName.equals("BlacklistType") ) {
            return 2;
        }
        if( strFieldName.equals("BlackName") ) {
            return 3;
        }
        if( strFieldName.equals("IDType") ) {
            return 4;
        }
        if( strFieldName.equals("IDNo") ) {
            return 5;
        }
        if( strFieldName.equals("BirthDate") ) {
            return 6;
        }
        if( strFieldName.equals("Sex") ) {
            return 7;
        }
        if( strFieldName.equals("BlacklistOperator") ) {
            return 8;
        }
        if( strFieldName.equals("BlacklistMakeDate") ) {
            return 9;
        }
        if( strFieldName.equals("BlacklistMakeTime") ) {
            return 10;
        }
        if( strFieldName.equals("BlacklistReason") ) {
            return 11;
        }
        if( strFieldName.equals("MakeDate") ) {
            return 12;
        }
        if( strFieldName.equals("MakeTime") ) {
            return 13;
        }
        if( strFieldName.equals("ModifyDate") ) {
            return 14;
        }
        if( strFieldName.equals("ModifyTime") ) {
            return 15;
        }
        if( strFieldName.equals("StandByFlag1") ) {
            return 16;
        }
        if( strFieldName.equals("StandByFlag2") ) {
            return 17;
        }
        if( strFieldName.equals("StandByFlag3") ) {
            return 18;
        }
        if( strFieldName.equals("StandByFlag4") ) {
            return 19;
        }
        if( strFieldName.equals("StandByFlag5") ) {
            return 20;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "SerialNo";
                break;
            case 1:
                strFieldName = "BlacklistNo";
                break;
            case 2:
                strFieldName = "BlacklistType";
                break;
            case 3:
                strFieldName = "BlackName";
                break;
            case 4:
                strFieldName = "IDType";
                break;
            case 5:
                strFieldName = "IDNo";
                break;
            case 6:
                strFieldName = "BirthDate";
                break;
            case 7:
                strFieldName = "Sex";
                break;
            case 8:
                strFieldName = "BlacklistOperator";
                break;
            case 9:
                strFieldName = "BlacklistMakeDate";
                break;
            case 10:
                strFieldName = "BlacklistMakeTime";
                break;
            case 11:
                strFieldName = "BlacklistReason";
                break;
            case 12:
                strFieldName = "MakeDate";
                break;
            case 13:
                strFieldName = "MakeTime";
                break;
            case 14:
                strFieldName = "ModifyDate";
                break;
            case 15:
                strFieldName = "ModifyTime";
                break;
            case 16:
                strFieldName = "StandByFlag1";
                break;
            case 17:
                strFieldName = "StandByFlag2";
                break;
            case 18:
                strFieldName = "StandByFlag3";
                break;
            case 19:
                strFieldName = "StandByFlag4";
                break;
            case 20:
                strFieldName = "StandByFlag5";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "SERIALNO":
                return Schema.TYPE_STRING;
            case "BLACKLISTNO":
                return Schema.TYPE_STRING;
            case "BLACKLISTTYPE":
                return Schema.TYPE_STRING;
            case "BLACKNAME":
                return Schema.TYPE_STRING;
            case "IDTYPE":
                return Schema.TYPE_STRING;
            case "IDNO":
                return Schema.TYPE_STRING;
            case "BIRTHDATE":
                return Schema.TYPE_STRING;
            case "SEX":
                return Schema.TYPE_STRING;
            case "BLACKLISTOPERATOR":
                return Schema.TYPE_STRING;
            case "BLACKLISTMAKEDATE":
                return Schema.TYPE_STRING;
            case "BLACKLISTMAKETIME":
                return Schema.TYPE_STRING;
            case "BLACKLISTREASON":
                return Schema.TYPE_STRING;
            case "MAKEDATE":
                return Schema.TYPE_STRING;
            case "MAKETIME":
                return Schema.TYPE_STRING;
            case "MODIFYDATE":
                return Schema.TYPE_STRING;
            case "MODIFYTIME":
                return Schema.TYPE_STRING;
            case "STANDBYFLAG1":
                return Schema.TYPE_STRING;
            case "STANDBYFLAG2":
                return Schema.TYPE_STRING;
            case "STANDBYFLAG3":
                return Schema.TYPE_STRING;
            case "STANDBYFLAG4":
                return Schema.TYPE_STRING;
            case "STANDBYFLAG5":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_STRING;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_STRING;
            case 6:
                return Schema.TYPE_STRING;
            case 7:
                return Schema.TYPE_STRING;
            case 8:
                return Schema.TYPE_STRING;
            case 9:
                return Schema.TYPE_STRING;
            case 10:
                return Schema.TYPE_STRING;
            case 11:
                return Schema.TYPE_STRING;
            case 12:
                return Schema.TYPE_STRING;
            case 13:
                return Schema.TYPE_STRING;
            case 14:
                return Schema.TYPE_STRING;
            case 15:
                return Schema.TYPE_STRING;
            case 16:
                return Schema.TYPE_STRING;
            case 17:
                return Schema.TYPE_STRING;
            case 18:
                return Schema.TYPE_STRING;
            case 19:
                return Schema.TYPE_STRING;
            case 20:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("SerialNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SerialNo));
        }
        if (FCode.equalsIgnoreCase("BlacklistNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BlacklistNo));
        }
        if (FCode.equalsIgnoreCase("BlacklistType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BlacklistType));
        }
        if (FCode.equalsIgnoreCase("BlackName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BlackName));
        }
        if (FCode.equalsIgnoreCase("IDType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(IDType));
        }
        if (FCode.equalsIgnoreCase("IDNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(IDNo));
        }
        if (FCode.equalsIgnoreCase("BirthDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BirthDate));
        }
        if (FCode.equalsIgnoreCase("Sex")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Sex));
        }
        if (FCode.equalsIgnoreCase("BlacklistOperator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BlacklistOperator));
        }
        if (FCode.equalsIgnoreCase("BlacklistMakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BlacklistMakeDate));
        }
        if (FCode.equalsIgnoreCase("BlacklistMakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BlacklistMakeTime));
        }
        if (FCode.equalsIgnoreCase("BlacklistReason")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BlacklistReason));
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeDate));
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyDate));
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (FCode.equalsIgnoreCase("StandByFlag1")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StandByFlag1));
        }
        if (FCode.equalsIgnoreCase("StandByFlag2")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StandByFlag2));
        }
        if (FCode.equalsIgnoreCase("StandByFlag3")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StandByFlag3));
        }
        if (FCode.equalsIgnoreCase("StandByFlag4")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StandByFlag4));
        }
        if (FCode.equalsIgnoreCase("StandByFlag5")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StandByFlag5));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = String.valueOf(SerialNo);
                break;
            case 1:
                strFieldValue = String.valueOf(BlacklistNo);
                break;
            case 2:
                strFieldValue = String.valueOf(BlacklistType);
                break;
            case 3:
                strFieldValue = String.valueOf(BlackName);
                break;
            case 4:
                strFieldValue = String.valueOf(IDType);
                break;
            case 5:
                strFieldValue = String.valueOf(IDNo);
                break;
            case 6:
                strFieldValue = String.valueOf(BirthDate);
                break;
            case 7:
                strFieldValue = String.valueOf(Sex);
                break;
            case 8:
                strFieldValue = String.valueOf(BlacklistOperator);
                break;
            case 9:
                strFieldValue = String.valueOf(BlacklistMakeDate);
                break;
            case 10:
                strFieldValue = String.valueOf(BlacklistMakeTime);
                break;
            case 11:
                strFieldValue = String.valueOf(BlacklistReason);
                break;
            case 12:
                strFieldValue = String.valueOf(MakeDate);
                break;
            case 13:
                strFieldValue = String.valueOf(MakeTime);
                break;
            case 14:
                strFieldValue = String.valueOf(ModifyDate);
                break;
            case 15:
                strFieldValue = String.valueOf(ModifyTime);
                break;
            case 16:
                strFieldValue = String.valueOf(StandByFlag1);
                break;
            case 17:
                strFieldValue = String.valueOf(StandByFlag2);
                break;
            case 18:
                strFieldValue = String.valueOf(StandByFlag3);
                break;
            case 19:
                strFieldValue = String.valueOf(StandByFlag4);
                break;
            case 20:
                strFieldValue = String.valueOf(StandByFlag5);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("SerialNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                SerialNo = FValue.trim();
            }
            else
                SerialNo = null;
        }
        if (FCode.equalsIgnoreCase("BlacklistNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                BlacklistNo = FValue.trim();
            }
            else
                BlacklistNo = null;
        }
        if (FCode.equalsIgnoreCase("BlacklistType")) {
            if( FValue != null && !FValue.equals(""))
            {
                BlacklistType = FValue.trim();
            }
            else
                BlacklistType = null;
        }
        if (FCode.equalsIgnoreCase("BlackName")) {
            if( FValue != null && !FValue.equals(""))
            {
                BlackName = FValue.trim();
            }
            else
                BlackName = null;
        }
        if (FCode.equalsIgnoreCase("IDType")) {
            if( FValue != null && !FValue.equals(""))
            {
                IDType = FValue.trim();
            }
            else
                IDType = null;
        }
        if (FCode.equalsIgnoreCase("IDNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                IDNo = FValue.trim();
            }
            else
                IDNo = null;
        }
        if (FCode.equalsIgnoreCase("BirthDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                BirthDate = FValue.trim();
            }
            else
                BirthDate = null;
        }
        if (FCode.equalsIgnoreCase("Sex")) {
            if( FValue != null && !FValue.equals(""))
            {
                Sex = FValue.trim();
            }
            else
                Sex = null;
        }
        if (FCode.equalsIgnoreCase("BlacklistOperator")) {
            if( FValue != null && !FValue.equals(""))
            {
                BlacklistOperator = FValue.trim();
            }
            else
                BlacklistOperator = null;
        }
        if (FCode.equalsIgnoreCase("BlacklistMakeDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                BlacklistMakeDate = FValue.trim();
            }
            else
                BlacklistMakeDate = null;
        }
        if (FCode.equalsIgnoreCase("BlacklistMakeTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                BlacklistMakeTime = FValue.trim();
            }
            else
                BlacklistMakeTime = null;
        }
        if (FCode.equalsIgnoreCase("BlacklistReason")) {
            if( FValue != null && !FValue.equals(""))
            {
                BlacklistReason = FValue.trim();
            }
            else
                BlacklistReason = null;
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeDate = FValue.trim();
            }
            else
                MakeDate = null;
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
                MakeTime = null;
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyDate = FValue.trim();
            }
            else
                ModifyDate = null;
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
                ModifyTime = null;
        }
        if (FCode.equalsIgnoreCase("StandByFlag1")) {
            if( FValue != null && !FValue.equals(""))
            {
                StandByFlag1 = FValue.trim();
            }
            else
                StandByFlag1 = null;
        }
        if (FCode.equalsIgnoreCase("StandByFlag2")) {
            if( FValue != null && !FValue.equals(""))
            {
                StandByFlag2 = FValue.trim();
            }
            else
                StandByFlag2 = null;
        }
        if (FCode.equalsIgnoreCase("StandByFlag3")) {
            if( FValue != null && !FValue.equals(""))
            {
                StandByFlag3 = FValue.trim();
            }
            else
                StandByFlag3 = null;
        }
        if (FCode.equalsIgnoreCase("StandByFlag4")) {
            if( FValue != null && !FValue.equals(""))
            {
                StandByFlag4 = FValue.trim();
            }
            else
                StandByFlag4 = null;
        }
        if (FCode.equalsIgnoreCase("StandByFlag5")) {
            if( FValue != null && !FValue.equals(""))
            {
                StandByFlag5 = FValue.trim();
            }
            else
                StandByFlag5 = null;
        }
        return true;
    }


    public String toString() {
    return "LDBlacklistPojo [" +
            "SerialNo="+SerialNo +
            ", BlacklistNo="+BlacklistNo +
            ", BlacklistType="+BlacklistType +
            ", BlackName="+BlackName +
            ", IDType="+IDType +
            ", IDNo="+IDNo +
            ", BirthDate="+BirthDate +
            ", Sex="+Sex +
            ", BlacklistOperator="+BlacklistOperator +
            ", BlacklistMakeDate="+BlacklistMakeDate +
            ", BlacklistMakeTime="+BlacklistMakeTime +
            ", BlacklistReason="+BlacklistReason +
            ", MakeDate="+MakeDate +
            ", MakeTime="+MakeTime +
            ", ModifyDate="+ModifyDate +
            ", ModifyTime="+ModifyTime +
            ", StandByFlag1="+StandByFlag1 +
            ", StandByFlag2="+StandByFlag2 +
            ", StandByFlag3="+StandByFlag3 +
            ", StandByFlag4="+StandByFlag4 +
            ", StandByFlag5="+StandByFlag5 +"]";
    }
}
