/**
 * Copyright (c) 2017 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.entity;

import com.sinosoft.utility.Pojo;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;

import java.io.Serializable;

/**
 * <p>ClassName: LMClaimCtrlPojo </p>
 * <p>Copyright: Copyright (c) 2017</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2017-10-17
 */
public class LMClaimCtrlPojo implements Pojo,Serializable {
    // @Field
    /** 理赔控制编号 */
    private String ClaimCtrlCode; 
    /** 理赔控制名称 */
    private String ClaimCtrlName; 
    /** 类型 */
    private String ClaimCtrlType; 
    /** 理赔控制描述 */
    private String ClaimEngineDesc; 
    /** 有效期间 */
    private String PeriodFlag; 
    /** 自定义期间标记 */
    private String DefPeriodFlag; 
    /** 个人家庭标记 */
    private String FamilyFlag; 
    /** 保单有效期/内外 */
    private String InsPeriodFlag; 
    /** 赔付期间控制上限 */
    private int ClmPeriodMAX; 
    /** 赔付期间上限单位 */
    private String ClmPeriodMAXFlag; 
    /** 赔付期间上限控制计算参考 */
    private String ClmPeriodMAXCtrl; 
    /** 赔付期间下限控制 */
    private int ClmPeriodMIN; 
    /** 赔付期间下限控制单位 */
    private String ClmPeriodMINFlag; 
    /** 赔付期间下限控制计算参考 */
    private String ClmPeriodMINCtrl; 
    /** 理赔控制计算sql */
    private String CalCode; 
    /** 理赔控制处理值类型 */
    private String CalResultType; 
    /** 理赔控制默认值 */
    private double DefaultValue; 
    /** 理赔控制计算方式 */
    private String CalCtrlFlag; 
    /** 理赔费用控制计算sql */
    private String FeeCalCode; 


    public static final int FIELDNUM = 19;    // 数据库表的字段个数
    public String getClaimCtrlCode() {
        return ClaimCtrlCode;
    }
    public void setClaimCtrlCode(String aClaimCtrlCode) {
        ClaimCtrlCode = aClaimCtrlCode;
    }
    public String getClaimCtrlName() {
        return ClaimCtrlName;
    }
    public void setClaimCtrlName(String aClaimCtrlName) {
        ClaimCtrlName = aClaimCtrlName;
    }
    public String getClaimCtrlType() {
        return ClaimCtrlType;
    }
    public void setClaimCtrlType(String aClaimCtrlType) {
        ClaimCtrlType = aClaimCtrlType;
    }
    public String getClaimEngineDesc() {
        return ClaimEngineDesc;
    }
    public void setClaimEngineDesc(String aClaimEngineDesc) {
        ClaimEngineDesc = aClaimEngineDesc;
    }
    public String getPeriodFlag() {
        return PeriodFlag;
    }
    public void setPeriodFlag(String aPeriodFlag) {
        PeriodFlag = aPeriodFlag;
    }
    public String getDefPeriodFlag() {
        return DefPeriodFlag;
    }
    public void setDefPeriodFlag(String aDefPeriodFlag) {
        DefPeriodFlag = aDefPeriodFlag;
    }
    public String getFamilyFlag() {
        return FamilyFlag;
    }
    public void setFamilyFlag(String aFamilyFlag) {
        FamilyFlag = aFamilyFlag;
    }
    public String getInsPeriodFlag() {
        return InsPeriodFlag;
    }
    public void setInsPeriodFlag(String aInsPeriodFlag) {
        InsPeriodFlag = aInsPeriodFlag;
    }
    public int getClmPeriodMAX() {
        return ClmPeriodMAX;
    }
    public void setClmPeriodMAX(int aClmPeriodMAX) {
        ClmPeriodMAX = aClmPeriodMAX;
    }
    public void setClmPeriodMAX(String aClmPeriodMAX) {
        if (aClmPeriodMAX != null && !aClmPeriodMAX.equals("")) {
            Integer tInteger = new Integer(aClmPeriodMAX);
            int i = tInteger.intValue();
            ClmPeriodMAX = i;
        }
    }

    public String getClmPeriodMAXFlag() {
        return ClmPeriodMAXFlag;
    }
    public void setClmPeriodMAXFlag(String aClmPeriodMAXFlag) {
        ClmPeriodMAXFlag = aClmPeriodMAXFlag;
    }
    public String getClmPeriodMAXCtrl() {
        return ClmPeriodMAXCtrl;
    }
    public void setClmPeriodMAXCtrl(String aClmPeriodMAXCtrl) {
        ClmPeriodMAXCtrl = aClmPeriodMAXCtrl;
    }
    public int getClmPeriodMIN() {
        return ClmPeriodMIN;
    }
    public void setClmPeriodMIN(int aClmPeriodMIN) {
        ClmPeriodMIN = aClmPeriodMIN;
    }
    public void setClmPeriodMIN(String aClmPeriodMIN) {
        if (aClmPeriodMIN != null && !aClmPeriodMIN.equals("")) {
            Integer tInteger = new Integer(aClmPeriodMIN);
            int i = tInteger.intValue();
            ClmPeriodMIN = i;
        }
    }

    public String getClmPeriodMINFlag() {
        return ClmPeriodMINFlag;
    }
    public void setClmPeriodMINFlag(String aClmPeriodMINFlag) {
        ClmPeriodMINFlag = aClmPeriodMINFlag;
    }
    public String getClmPeriodMINCtrl() {
        return ClmPeriodMINCtrl;
    }
    public void setClmPeriodMINCtrl(String aClmPeriodMINCtrl) {
        ClmPeriodMINCtrl = aClmPeriodMINCtrl;
    }
    public String getCalCode() {
        return CalCode;
    }
    public void setCalCode(String aCalCode) {
        CalCode = aCalCode;
    }
    public String getCalResultType() {
        return CalResultType;
    }
    public void setCalResultType(String aCalResultType) {
        CalResultType = aCalResultType;
    }
    public double getDefaultValue() {
        return DefaultValue;
    }
    public void setDefaultValue(double aDefaultValue) {
        DefaultValue = aDefaultValue;
    }
    public void setDefaultValue(String aDefaultValue) {
        if (aDefaultValue != null && !aDefaultValue.equals("")) {
            Double tDouble = new Double(aDefaultValue);
            double d = tDouble.doubleValue();
            DefaultValue = d;
        }
    }

    public String getCalCtrlFlag() {
        return CalCtrlFlag;
    }
    public void setCalCtrlFlag(String aCalCtrlFlag) {
        CalCtrlFlag = aCalCtrlFlag;
    }
    public String getFeeCalCode() {
        return FeeCalCode;
    }
    public void setFeeCalCode(String aFeeCalCode) {
        FeeCalCode = aFeeCalCode;
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("ClaimCtrlCode") ) {
            return 0;
        }
        if( strFieldName.equals("ClaimCtrlName") ) {
            return 1;
        }
        if( strFieldName.equals("ClaimCtrlType") ) {
            return 2;
        }
        if( strFieldName.equals("ClaimEngineDesc") ) {
            return 3;
        }
        if( strFieldName.equals("PeriodFlag") ) {
            return 4;
        }
        if( strFieldName.equals("DefPeriodFlag") ) {
            return 5;
        }
        if( strFieldName.equals("FamilyFlag") ) {
            return 6;
        }
        if( strFieldName.equals("InsPeriodFlag") ) {
            return 7;
        }
        if( strFieldName.equals("ClmPeriodMAX") ) {
            return 8;
        }
        if( strFieldName.equals("ClmPeriodMAXFlag") ) {
            return 9;
        }
        if( strFieldName.equals("ClmPeriodMAXCtrl") ) {
            return 10;
        }
        if( strFieldName.equals("ClmPeriodMIN") ) {
            return 11;
        }
        if( strFieldName.equals("ClmPeriodMINFlag") ) {
            return 12;
        }
        if( strFieldName.equals("ClmPeriodMINCtrl") ) {
            return 13;
        }
        if( strFieldName.equals("CalCode") ) {
            return 14;
        }
        if( strFieldName.equals("CalResultType") ) {
            return 15;
        }
        if( strFieldName.equals("DefaultValue") ) {
            return 16;
        }
        if( strFieldName.equals("CalCtrlFlag") ) {
            return 17;
        }
        if( strFieldName.equals("FeeCalCode") ) {
            return 18;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "ClaimCtrlCode";
                break;
            case 1:
                strFieldName = "ClaimCtrlName";
                break;
            case 2:
                strFieldName = "ClaimCtrlType";
                break;
            case 3:
                strFieldName = "ClaimEngineDesc";
                break;
            case 4:
                strFieldName = "PeriodFlag";
                break;
            case 5:
                strFieldName = "DefPeriodFlag";
                break;
            case 6:
                strFieldName = "FamilyFlag";
                break;
            case 7:
                strFieldName = "InsPeriodFlag";
                break;
            case 8:
                strFieldName = "ClmPeriodMAX";
                break;
            case 9:
                strFieldName = "ClmPeriodMAXFlag";
                break;
            case 10:
                strFieldName = "ClmPeriodMAXCtrl";
                break;
            case 11:
                strFieldName = "ClmPeriodMIN";
                break;
            case 12:
                strFieldName = "ClmPeriodMINFlag";
                break;
            case 13:
                strFieldName = "ClmPeriodMINCtrl";
                break;
            case 14:
                strFieldName = "CalCode";
                break;
            case 15:
                strFieldName = "CalResultType";
                break;
            case 16:
                strFieldName = "DefaultValue";
                break;
            case 17:
                strFieldName = "CalCtrlFlag";
                break;
            case 18:
                strFieldName = "FeeCalCode";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "CLAIMCTRLCODE":
                return Schema.TYPE_STRING;
            case "CLAIMCTRLNAME":
                return Schema.TYPE_STRING;
            case "CLAIMCTRLTYPE":
                return Schema.TYPE_STRING;
            case "CLAIMENGINEDESC":
                return Schema.TYPE_STRING;
            case "PERIODFLAG":
                return Schema.TYPE_STRING;
            case "DEFPERIODFLAG":
                return Schema.TYPE_STRING;
            case "FAMILYFLAG":
                return Schema.TYPE_STRING;
            case "INSPERIODFLAG":
                return Schema.TYPE_STRING;
            case "CLMPERIODMAX":
                return Schema.TYPE_INT;
            case "CLMPERIODMAXFLAG":
                return Schema.TYPE_STRING;
            case "CLMPERIODMAXCTRL":
                return Schema.TYPE_STRING;
            case "CLMPERIODMIN":
                return Schema.TYPE_INT;
            case "CLMPERIODMINFLAG":
                return Schema.TYPE_STRING;
            case "CLMPERIODMINCTRL":
                return Schema.TYPE_STRING;
            case "CALCODE":
                return Schema.TYPE_STRING;
            case "CALRESULTTYPE":
                return Schema.TYPE_STRING;
            case "DEFAULTVALUE":
                return Schema.TYPE_DOUBLE;
            case "CALCTRLFLAG":
                return Schema.TYPE_STRING;
            case "FEECALCODE":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_STRING;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_STRING;
            case 6:
                return Schema.TYPE_STRING;
            case 7:
                return Schema.TYPE_STRING;
            case 8:
                return Schema.TYPE_INT;
            case 9:
                return Schema.TYPE_STRING;
            case 10:
                return Schema.TYPE_STRING;
            case 11:
                return Schema.TYPE_INT;
            case 12:
                return Schema.TYPE_STRING;
            case 13:
                return Schema.TYPE_STRING;
            case 14:
                return Schema.TYPE_STRING;
            case 15:
                return Schema.TYPE_STRING;
            case 16:
                return Schema.TYPE_DOUBLE;
            case 17:
                return Schema.TYPE_STRING;
            case 18:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("ClaimCtrlCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ClaimCtrlCode));
        }
        if (FCode.equalsIgnoreCase("ClaimCtrlName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ClaimCtrlName));
        }
        if (FCode.equalsIgnoreCase("ClaimCtrlType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ClaimCtrlType));
        }
        if (FCode.equalsIgnoreCase("ClaimEngineDesc")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ClaimEngineDesc));
        }
        if (FCode.equalsIgnoreCase("PeriodFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PeriodFlag));
        }
        if (FCode.equalsIgnoreCase("DefPeriodFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DefPeriodFlag));
        }
        if (FCode.equalsIgnoreCase("FamilyFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FamilyFlag));
        }
        if (FCode.equalsIgnoreCase("InsPeriodFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InsPeriodFlag));
        }
        if (FCode.equalsIgnoreCase("ClmPeriodMAX")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ClmPeriodMAX));
        }
        if (FCode.equalsIgnoreCase("ClmPeriodMAXFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ClmPeriodMAXFlag));
        }
        if (FCode.equalsIgnoreCase("ClmPeriodMAXCtrl")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ClmPeriodMAXCtrl));
        }
        if (FCode.equalsIgnoreCase("ClmPeriodMIN")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ClmPeriodMIN));
        }
        if (FCode.equalsIgnoreCase("ClmPeriodMINFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ClmPeriodMINFlag));
        }
        if (FCode.equalsIgnoreCase("ClmPeriodMINCtrl")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ClmPeriodMINCtrl));
        }
        if (FCode.equalsIgnoreCase("CalCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CalCode));
        }
        if (FCode.equalsIgnoreCase("CalResultType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CalResultType));
        }
        if (FCode.equalsIgnoreCase("DefaultValue")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DefaultValue));
        }
        if (FCode.equalsIgnoreCase("CalCtrlFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CalCtrlFlag));
        }
        if (FCode.equalsIgnoreCase("FeeCalCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FeeCalCode));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = String.valueOf(ClaimCtrlCode);
                break;
            case 1:
                strFieldValue = String.valueOf(ClaimCtrlName);
                break;
            case 2:
                strFieldValue = String.valueOf(ClaimCtrlType);
                break;
            case 3:
                strFieldValue = String.valueOf(ClaimEngineDesc);
                break;
            case 4:
                strFieldValue = String.valueOf(PeriodFlag);
                break;
            case 5:
                strFieldValue = String.valueOf(DefPeriodFlag);
                break;
            case 6:
                strFieldValue = String.valueOf(FamilyFlag);
                break;
            case 7:
                strFieldValue = String.valueOf(InsPeriodFlag);
                break;
            case 8:
                strFieldValue = String.valueOf(ClmPeriodMAX);
                break;
            case 9:
                strFieldValue = String.valueOf(ClmPeriodMAXFlag);
                break;
            case 10:
                strFieldValue = String.valueOf(ClmPeriodMAXCtrl);
                break;
            case 11:
                strFieldValue = String.valueOf(ClmPeriodMIN);
                break;
            case 12:
                strFieldValue = String.valueOf(ClmPeriodMINFlag);
                break;
            case 13:
                strFieldValue = String.valueOf(ClmPeriodMINCtrl);
                break;
            case 14:
                strFieldValue = String.valueOf(CalCode);
                break;
            case 15:
                strFieldValue = String.valueOf(CalResultType);
                break;
            case 16:
                strFieldValue = String.valueOf(DefaultValue);
                break;
            case 17:
                strFieldValue = String.valueOf(CalCtrlFlag);
                break;
            case 18:
                strFieldValue = String.valueOf(FeeCalCode);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("ClaimCtrlCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                ClaimCtrlCode = FValue.trim();
            }
            else
                ClaimCtrlCode = null;
        }
        if (FCode.equalsIgnoreCase("ClaimCtrlName")) {
            if( FValue != null && !FValue.equals(""))
            {
                ClaimCtrlName = FValue.trim();
            }
            else
                ClaimCtrlName = null;
        }
        if (FCode.equalsIgnoreCase("ClaimCtrlType")) {
            if( FValue != null && !FValue.equals(""))
            {
                ClaimCtrlType = FValue.trim();
            }
            else
                ClaimCtrlType = null;
        }
        if (FCode.equalsIgnoreCase("ClaimEngineDesc")) {
            if( FValue != null && !FValue.equals(""))
            {
                ClaimEngineDesc = FValue.trim();
            }
            else
                ClaimEngineDesc = null;
        }
        if (FCode.equalsIgnoreCase("PeriodFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                PeriodFlag = FValue.trim();
            }
            else
                PeriodFlag = null;
        }
        if (FCode.equalsIgnoreCase("DefPeriodFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                DefPeriodFlag = FValue.trim();
            }
            else
                DefPeriodFlag = null;
        }
        if (FCode.equalsIgnoreCase("FamilyFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                FamilyFlag = FValue.trim();
            }
            else
                FamilyFlag = null;
        }
        if (FCode.equalsIgnoreCase("InsPeriodFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                InsPeriodFlag = FValue.trim();
            }
            else
                InsPeriodFlag = null;
        }
        if (FCode.equalsIgnoreCase("ClmPeriodMAX")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                ClmPeriodMAX = i;
            }
        }
        if (FCode.equalsIgnoreCase("ClmPeriodMAXFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                ClmPeriodMAXFlag = FValue.trim();
            }
            else
                ClmPeriodMAXFlag = null;
        }
        if (FCode.equalsIgnoreCase("ClmPeriodMAXCtrl")) {
            if( FValue != null && !FValue.equals(""))
            {
                ClmPeriodMAXCtrl = FValue.trim();
            }
            else
                ClmPeriodMAXCtrl = null;
        }
        if (FCode.equalsIgnoreCase("ClmPeriodMIN")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                ClmPeriodMIN = i;
            }
        }
        if (FCode.equalsIgnoreCase("ClmPeriodMINFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                ClmPeriodMINFlag = FValue.trim();
            }
            else
                ClmPeriodMINFlag = null;
        }
        if (FCode.equalsIgnoreCase("ClmPeriodMINCtrl")) {
            if( FValue != null && !FValue.equals(""))
            {
                ClmPeriodMINCtrl = FValue.trim();
            }
            else
                ClmPeriodMINCtrl = null;
        }
        if (FCode.equalsIgnoreCase("CalCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                CalCode = FValue.trim();
            }
            else
                CalCode = null;
        }
        if (FCode.equalsIgnoreCase("CalResultType")) {
            if( FValue != null && !FValue.equals(""))
            {
                CalResultType = FValue.trim();
            }
            else
                CalResultType = null;
        }
        if (FCode.equalsIgnoreCase("DefaultValue")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                DefaultValue = d;
            }
        }
        if (FCode.equalsIgnoreCase("CalCtrlFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                CalCtrlFlag = FValue.trim();
            }
            else
                CalCtrlFlag = null;
        }
        if (FCode.equalsIgnoreCase("FeeCalCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                FeeCalCode = FValue.trim();
            }
            else
                FeeCalCode = null;
        }
        return true;
    }


    public String toString() {
    return "LMClaimCtrlPojo [" +
            "ClaimCtrlCode="+ClaimCtrlCode +
            ", ClaimCtrlName="+ClaimCtrlName +
            ", ClaimCtrlType="+ClaimCtrlType +
            ", ClaimEngineDesc="+ClaimEngineDesc +
            ", PeriodFlag="+PeriodFlag +
            ", DefPeriodFlag="+DefPeriodFlag +
            ", FamilyFlag="+FamilyFlag +
            ", InsPeriodFlag="+InsPeriodFlag +
            ", ClmPeriodMAX="+ClmPeriodMAX +
            ", ClmPeriodMAXFlag="+ClmPeriodMAXFlag +
            ", ClmPeriodMAXCtrl="+ClmPeriodMAXCtrl +
            ", ClmPeriodMIN="+ClmPeriodMIN +
            ", ClmPeriodMINFlag="+ClmPeriodMINFlag +
            ", ClmPeriodMINCtrl="+ClmPeriodMINCtrl +
            ", CalCode="+CalCode +
            ", CalResultType="+CalResultType +
            ", DefaultValue="+DefaultValue +
            ", CalCtrlFlag="+CalCtrlFlag +
            ", FeeCalCode="+FeeCalCode +"]";
    }
}
