/**
 * Copyright (c) 2019 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.entity;

import com.sinosoft.cloud.cache.annotation.RedisIndexHKey;
import com.sinosoft.cloud.cache.annotation.RedisPrimaryHKey;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.Pojo;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;

import java.io.Serializable;

/**
 * <p>ClassName: LDPlanPojo </p>
 * <p>Copyright: Copyright (c) 2019</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2019-12-04
 */
public class LDPlanPojo implements  Pojo,Serializable {
    // @Field
    /** 保险计划编码 */
    @RedisPrimaryHKey
    @RedisIndexHKey
    private String ContPlanCode;
    /** 保险计划名称 */
    private String ContPlanName; 
    /** 计划类别 */
    @RedisPrimaryHKey
    private String PlanType;
    /** 计划规则 */
    private String PlanRule; 
    /** 计划规则sql */
    private String PlanSql; 
    /** 备注 */
    private String Remark; 
    /** 操作员 */
    private String Operator; 
    /** 入机日期 */
    private String  MakeDate;
    /** 入机时间 */
    private String MakeTime; 
    /** 最后一次修改日期 */
    private String  ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime; 
    /** 可投保人数 */
    private int Peoples3; 
    /** 备注2 */
    private String Remark2; 
    /** 管理机构 */
    private String ManageCom; 
    /** 销售渠道 */
    private String SaleChnl; 
    /** 销售起期 */
    private String  StartDate;
    /** 销售止期 */
    private String  EndDate;
    /** 状态 */
    private String State; 
    /** 计划层级1 */
    private String PlanKind1; 
    /** 计划层级2 */
    private String PlanKind2; 
    /** 计划层级3 */
    private String PlanKind3; 
    /** 产品组合标记 */
    private String PortfolioFlag; 
    /** 标准产品标记 */
    private String StandardFlag; 
    /** 产品上下架状态 */
    private String UpDownStatus; 


    public static final int FIELDNUM = 24;    // 数据库表的字段个数

    private FDate fDate = new FDate();        // 处理日期
    public String getContPlanCode() {
        return ContPlanCode;
    }
    public void setContPlanCode(String aContPlanCode) {
        ContPlanCode = aContPlanCode;
    }
    public String getContPlanName() {
        return ContPlanName;
    }
    public void setContPlanName(String aContPlanName) {
        ContPlanName = aContPlanName;
    }
    public String getPlanType() {
        return PlanType;
    }
    public void setPlanType(String aPlanType) {
        PlanType = aPlanType;
    }
    public String getPlanRule() {
        return PlanRule;
    }
    public void setPlanRule(String aPlanRule) {
        PlanRule = aPlanRule;
    }
    public String getPlanSql() {
        return PlanSql;
    }
    public void setPlanSql(String aPlanSql) {
        PlanSql = aPlanSql;
    }
    public String getRemark() {
        return Remark;
    }
    public void setRemark(String aRemark) {
        Remark = aRemark;
    }
    public String getOperator() {
        return Operator;
    }
    public void setOperator(String aOperator) {
        Operator = aOperator;
    }
    public String getMakeDate() {
        return MakeDate;
    }
    public void setMakeDate(String aMakeDate) {
        MakeDate = aMakeDate;
    }
    public String getMakeTime() {
        return MakeTime;
    }
    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }
    public String getModifyDate() {
        return ModifyDate;
    }
    public void setModifyDate(String aModifyDate) {
        ModifyDate = aModifyDate;
    }
    public String getModifyTime() {
        return ModifyTime;
    }
    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }
    public int getPeoples3() {
        return Peoples3;
    }
    public void setPeoples3(int aPeoples3) {
        Peoples3 = aPeoples3;
    }
    public void setPeoples3(String aPeoples3) {
        if (aPeoples3 != null && !aPeoples3.equals("")) {
            Integer tInteger = new Integer(aPeoples3);
            int i = tInteger.intValue();
            Peoples3 = i;
        }
    }

    public String getRemark2() {
        return Remark2;
    }
    public void setRemark2(String aRemark2) {
        Remark2 = aRemark2;
    }
    public String getManageCom() {
        return ManageCom;
    }
    public void setManageCom(String aManageCom) {
        ManageCom = aManageCom;
    }
    public String getSaleChnl() {
        return SaleChnl;
    }
    public void setSaleChnl(String aSaleChnl) {
        SaleChnl = aSaleChnl;
    }
    public String getStartDate() {
        return StartDate;
    }
    public void setStartDate(String aStartDate) {
        StartDate = aStartDate;
    }
    public String getEndDate() {
        return EndDate;
    }
    public void setEndDate(String aEndDate) {
        EndDate = aEndDate;
    }
    public String getState() {
        return State;
    }
    public void setState(String aState) {
        State = aState;
    }
    public String getPlanKind1() {
        return PlanKind1;
    }
    public void setPlanKind1(String aPlanKind1) {
        PlanKind1 = aPlanKind1;
    }
    public String getPlanKind2() {
        return PlanKind2;
    }
    public void setPlanKind2(String aPlanKind2) {
        PlanKind2 = aPlanKind2;
    }
    public String getPlanKind3() {
        return PlanKind3;
    }
    public void setPlanKind3(String aPlanKind3) {
        PlanKind3 = aPlanKind3;
    }
    public String getPortfolioFlag() {
        return PortfolioFlag;
    }
    public void setPortfolioFlag(String aPortfolioFlag) {
        PortfolioFlag = aPortfolioFlag;
    }
    public String getStandardFlag() {
        return StandardFlag;
    }
    public void setStandardFlag(String aStandardFlag) {
        StandardFlag = aStandardFlag;
    }
    public String getUpDownStatus() {
        return UpDownStatus;
    }
    public void setUpDownStatus(String aUpDownStatus) {
        UpDownStatus = aUpDownStatus;
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("ContPlanCode") ) {
            return 0;
        }
        if( strFieldName.equals("ContPlanName") ) {
            return 1;
        }
        if( strFieldName.equals("PlanType") ) {
            return 2;
        }
        if( strFieldName.equals("PlanRule") ) {
            return 3;
        }
        if( strFieldName.equals("PlanSql") ) {
            return 4;
        }
        if( strFieldName.equals("Remark") ) {
            return 5;
        }
        if( strFieldName.equals("Operator") ) {
            return 6;
        }
        if( strFieldName.equals("MakeDate") ) {
            return 7;
        }
        if( strFieldName.equals("MakeTime") ) {
            return 8;
        }
        if( strFieldName.equals("ModifyDate") ) {
            return 9;
        }
        if( strFieldName.equals("ModifyTime") ) {
            return 10;
        }
        if( strFieldName.equals("Peoples3") ) {
            return 11;
        }
        if( strFieldName.equals("Remark2") ) {
            return 12;
        }
        if( strFieldName.equals("ManageCom") ) {
            return 13;
        }
        if( strFieldName.equals("SaleChnl") ) {
            return 14;
        }
        if( strFieldName.equals("StartDate") ) {
            return 15;
        }
        if( strFieldName.equals("EndDate") ) {
            return 16;
        }
        if( strFieldName.equals("State") ) {
            return 17;
        }
        if( strFieldName.equals("PlanKind1") ) {
            return 18;
        }
        if( strFieldName.equals("PlanKind2") ) {
            return 19;
        }
        if( strFieldName.equals("PlanKind3") ) {
            return 20;
        }
        if( strFieldName.equals("PortfolioFlag") ) {
            return 21;
        }
        if( strFieldName.equals("StandardFlag") ) {
            return 22;
        }
        if( strFieldName.equals("UpDownStatus") ) {
            return 23;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "ContPlanCode";
                break;
            case 1:
                strFieldName = "ContPlanName";
                break;
            case 2:
                strFieldName = "PlanType";
                break;
            case 3:
                strFieldName = "PlanRule";
                break;
            case 4:
                strFieldName = "PlanSql";
                break;
            case 5:
                strFieldName = "Remark";
                break;
            case 6:
                strFieldName = "Operator";
                break;
            case 7:
                strFieldName = "MakeDate";
                break;
            case 8:
                strFieldName = "MakeTime";
                break;
            case 9:
                strFieldName = "ModifyDate";
                break;
            case 10:
                strFieldName = "ModifyTime";
                break;
            case 11:
                strFieldName = "Peoples3";
                break;
            case 12:
                strFieldName = "Remark2";
                break;
            case 13:
                strFieldName = "ManageCom";
                break;
            case 14:
                strFieldName = "SaleChnl";
                break;
            case 15:
                strFieldName = "StartDate";
                break;
            case 16:
                strFieldName = "EndDate";
                break;
            case 17:
                strFieldName = "State";
                break;
            case 18:
                strFieldName = "PlanKind1";
                break;
            case 19:
                strFieldName = "PlanKind2";
                break;
            case 20:
                strFieldName = "PlanKind3";
                break;
            case 21:
                strFieldName = "PortfolioFlag";
                break;
            case 22:
                strFieldName = "StandardFlag";
                break;
            case 23:
                strFieldName = "UpDownStatus";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "CONTPLANCODE":
                return Schema.TYPE_STRING;
            case "CONTPLANNAME":
                return Schema.TYPE_STRING;
            case "PLANTYPE":
                return Schema.TYPE_STRING;
            case "PLANRULE":
                return Schema.TYPE_STRING;
            case "PLANSQL":
                return Schema.TYPE_STRING;
            case "REMARK":
                return Schema.TYPE_STRING;
            case "OPERATOR":
                return Schema.TYPE_STRING;
            case "MAKEDATE":
                return Schema.TYPE_STRING;
            case "MAKETIME":
                return Schema.TYPE_STRING;
            case "MODIFYDATE":
                return Schema.TYPE_STRING;
            case "MODIFYTIME":
                return Schema.TYPE_STRING;
            case "PEOPLES3":
                return Schema.TYPE_INT;
            case "REMARK2":
                return Schema.TYPE_STRING;
            case "MANAGECOM":
                return Schema.TYPE_STRING;
            case "SALECHNL":
                return Schema.TYPE_STRING;
            case "STARTDATE":
                return Schema.TYPE_STRING;
            case "ENDDATE":
                return Schema.TYPE_STRING;
            case "STATE":
                return Schema.TYPE_STRING;
            case "PLANKIND1":
                return Schema.TYPE_STRING;
            case "PLANKIND2":
                return Schema.TYPE_STRING;
            case "PLANKIND3":
                return Schema.TYPE_STRING;
            case "PORTFOLIOFLAG":
                return Schema.TYPE_STRING;
            case "STANDARDFLAG":
                return Schema.TYPE_STRING;
            case "UPDOWNSTATUS":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_STRING;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_STRING;
            case 6:
                return Schema.TYPE_STRING;
            case 7:
                return Schema.TYPE_STRING;
            case 8:
                return Schema.TYPE_STRING;
            case 9:
                return Schema.TYPE_STRING;
            case 10:
                return Schema.TYPE_STRING;
            case 11:
                return Schema.TYPE_INT;
            case 12:
                return Schema.TYPE_STRING;
            case 13:
                return Schema.TYPE_STRING;
            case 14:
                return Schema.TYPE_STRING;
            case 15:
                return Schema.TYPE_STRING;
            case 16:
                return Schema.TYPE_STRING;
            case 17:
                return Schema.TYPE_STRING;
            case 18:
                return Schema.TYPE_STRING;
            case 19:
                return Schema.TYPE_STRING;
            case 20:
                return Schema.TYPE_STRING;
            case 21:
                return Schema.TYPE_STRING;
            case 22:
                return Schema.TYPE_STRING;
            case 23:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("ContPlanCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ContPlanCode));
        }
        if (FCode.equalsIgnoreCase("ContPlanName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ContPlanName));
        }
        if (FCode.equalsIgnoreCase("PlanType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PlanType));
        }
        if (FCode.equalsIgnoreCase("PlanRule")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PlanRule));
        }
        if (FCode.equalsIgnoreCase("PlanSql")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PlanSql));
        }
        if (FCode.equalsIgnoreCase("Remark")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Remark));
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeDate));
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyDate));
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (FCode.equalsIgnoreCase("Peoples3")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Peoples3));
        }
        if (FCode.equalsIgnoreCase("Remark2")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Remark2));
        }
        if (FCode.equalsIgnoreCase("ManageCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
        }
        if (FCode.equalsIgnoreCase("SaleChnl")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SaleChnl));
        }
        if (FCode.equalsIgnoreCase("StartDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StartDate));
        }
        if (FCode.equalsIgnoreCase("EndDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(EndDate));
        }
        if (FCode.equalsIgnoreCase("State")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(State));
        }
        if (FCode.equalsIgnoreCase("PlanKind1")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PlanKind1));
        }
        if (FCode.equalsIgnoreCase("PlanKind2")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PlanKind2));
        }
        if (FCode.equalsIgnoreCase("PlanKind3")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PlanKind3));
        }
        if (FCode.equalsIgnoreCase("PortfolioFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PortfolioFlag));
        }
        if (FCode.equalsIgnoreCase("StandardFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StandardFlag));
        }
        if (FCode.equalsIgnoreCase("UpDownStatus")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(UpDownStatus));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = String.valueOf(ContPlanCode);
                break;
            case 1:
                strFieldValue = String.valueOf(ContPlanName);
                break;
            case 2:
                strFieldValue = String.valueOf(PlanType);
                break;
            case 3:
                strFieldValue = String.valueOf(PlanRule);
                break;
            case 4:
                strFieldValue = String.valueOf(PlanSql);
                break;
            case 5:
                strFieldValue = String.valueOf(Remark);
                break;
            case 6:
                strFieldValue = String.valueOf(Operator);
                break;
            case 7:
                strFieldValue = String.valueOf(MakeDate);
                break;
            case 8:
                strFieldValue = String.valueOf(MakeTime);
                break;
            case 9:
                strFieldValue = String.valueOf(ModifyDate);
                break;
            case 10:
                strFieldValue = String.valueOf(ModifyTime);
                break;
            case 11:
                strFieldValue = String.valueOf(Peoples3);
                break;
            case 12:
                strFieldValue = String.valueOf(Remark2);
                break;
            case 13:
                strFieldValue = String.valueOf(ManageCom);
                break;
            case 14:
                strFieldValue = String.valueOf(SaleChnl);
                break;
            case 15:
                strFieldValue = String.valueOf(StartDate);
                break;
            case 16:
                strFieldValue = String.valueOf(EndDate);
                break;
            case 17:
                strFieldValue = String.valueOf(State);
                break;
            case 18:
                strFieldValue = String.valueOf(PlanKind1);
                break;
            case 19:
                strFieldValue = String.valueOf(PlanKind2);
                break;
            case 20:
                strFieldValue = String.valueOf(PlanKind3);
                break;
            case 21:
                strFieldValue = String.valueOf(PortfolioFlag);
                break;
            case 22:
                strFieldValue = String.valueOf(StandardFlag);
                break;
            case 23:
                strFieldValue = String.valueOf(UpDownStatus);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("ContPlanCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                ContPlanCode = FValue.trim();
            }
            else
                ContPlanCode = null;
        }
        if (FCode.equalsIgnoreCase("ContPlanName")) {
            if( FValue != null && !FValue.equals(""))
            {
                ContPlanName = FValue.trim();
            }
            else
                ContPlanName = null;
        }
        if (FCode.equalsIgnoreCase("PlanType")) {
            if( FValue != null && !FValue.equals(""))
            {
                PlanType = FValue.trim();
            }
            else
                PlanType = null;
        }
        if (FCode.equalsIgnoreCase("PlanRule")) {
            if( FValue != null && !FValue.equals(""))
            {
                PlanRule = FValue.trim();
            }
            else
                PlanRule = null;
        }
        if (FCode.equalsIgnoreCase("PlanSql")) {
            if( FValue != null && !FValue.equals(""))
            {
                PlanSql = FValue.trim();
            }
            else
                PlanSql = null;
        }
        if (FCode.equalsIgnoreCase("Remark")) {
            if( FValue != null && !FValue.equals(""))
            {
                Remark = FValue.trim();
            }
            else
                Remark = null;
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            if( FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
                Operator = null;
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeDate = FValue.trim();
            }
            else
                MakeDate = null;
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
                MakeTime = null;
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyDate = FValue.trim();
            }
            else
                ModifyDate = null;
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
                ModifyTime = null;
        }
        if (FCode.equalsIgnoreCase("Peoples3")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                Peoples3 = i;
            }
        }
        if (FCode.equalsIgnoreCase("Remark2")) {
            if( FValue != null && !FValue.equals(""))
            {
                Remark2 = FValue.trim();
            }
            else
                Remark2 = null;
        }
        if (FCode.equalsIgnoreCase("ManageCom")) {
            if( FValue != null && !FValue.equals(""))
            {
                ManageCom = FValue.trim();
            }
            else
                ManageCom = null;
        }
        if (FCode.equalsIgnoreCase("SaleChnl")) {
            if( FValue != null && !FValue.equals(""))
            {
                SaleChnl = FValue.trim();
            }
            else
                SaleChnl = null;
        }
        if (FCode.equalsIgnoreCase("StartDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                StartDate = FValue.trim();
            }
            else
                StartDate = null;
        }
        if (FCode.equalsIgnoreCase("EndDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                EndDate = FValue.trim();
            }
            else
                EndDate = null;
        }
        if (FCode.equalsIgnoreCase("State")) {
            if( FValue != null && !FValue.equals(""))
            {
                State = FValue.trim();
            }
            else
                State = null;
        }
        if (FCode.equalsIgnoreCase("PlanKind1")) {
            if( FValue != null && !FValue.equals(""))
            {
                PlanKind1 = FValue.trim();
            }
            else
                PlanKind1 = null;
        }
        if (FCode.equalsIgnoreCase("PlanKind2")) {
            if( FValue != null && !FValue.equals(""))
            {
                PlanKind2 = FValue.trim();
            }
            else
                PlanKind2 = null;
        }
        if (FCode.equalsIgnoreCase("PlanKind3")) {
            if( FValue != null && !FValue.equals(""))
            {
                PlanKind3 = FValue.trim();
            }
            else
                PlanKind3 = null;
        }
        if (FCode.equalsIgnoreCase("PortfolioFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                PortfolioFlag = FValue.trim();
            }
            else
                PortfolioFlag = null;
        }
        if (FCode.equalsIgnoreCase("StandardFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                StandardFlag = FValue.trim();
            }
            else
                StandardFlag = null;
        }
        if (FCode.equalsIgnoreCase("UpDownStatus")) {
            if( FValue != null && !FValue.equals(""))
            {
                UpDownStatus = FValue.trim();
            }
            else
                UpDownStatus = null;
        }
        return true;
    }


    public String toString() {
    return "LDPlanPojo [" +
            "ContPlanCode="+ContPlanCode +
            ", ContPlanName="+ContPlanName +
            ", PlanType="+PlanType +
            ", PlanRule="+PlanRule +
            ", PlanSql="+PlanSql +
            ", Remark="+Remark +
            ", Operator="+Operator +
            ", MakeDate="+MakeDate +
            ", MakeTime="+MakeTime +
            ", ModifyDate="+ModifyDate +
            ", ModifyTime="+ModifyTime +
            ", Peoples3="+Peoples3 +
            ", Remark2="+Remark2 +
            ", ManageCom="+ManageCom +
            ", SaleChnl="+SaleChnl +
            ", StartDate="+StartDate +
            ", EndDate="+EndDate +
            ", State="+State +
            ", PlanKind1="+PlanKind1 +
            ", PlanKind2="+PlanKind2 +
            ", PlanKind3="+PlanKind3 +
            ", PortfolioFlag="+PortfolioFlag +
            ", StandardFlag="+StandardFlag +
            ", UpDownStatus="+UpDownStatus +"]";
    }
}
