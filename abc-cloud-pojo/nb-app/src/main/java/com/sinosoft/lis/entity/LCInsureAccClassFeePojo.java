/**
 * Copyright (c) 2017 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.entity;

import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.Pojo;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;

import java.io.Serializable;

/**
 * <p>ClassName: LCInsureAccClassFeePojo </p>
 * <p>Copyright: Copyright (c) 2017</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2017-10-17
 */
public class LCInsureAccClassFeePojo implements Pojo,Serializable {
    // @Field
    /** Id */
    private long InsureAccClassFeeID; 
    /** Shardingid */
    private String ShardingID; 
    /** 保单险种号码 */
    private String PolNo; 
    /** 保险帐户号码 */
    private String InsuAccNo; 
    /** 交费计划编码 */
    private String PayPlanCode; 
    /** 对应其它号码 */
    private String OtherNo; 
    /** 对应其它号码类型 */
    private String OtherType; 
    /** 账户归属属性 */
    private String AccAscription; 
    /** 险种编码 */
    private String RiskCode; 
    /** 合同号码 */
    private String ContNo; 
    /** 集体保单号码 */
    private String GrpPolNo; 
    /** 集体合同号码 */
    private String GrpContNo; 
    /** 管理机构 */
    private String ManageCom; 
    /** 被保人客户号码 */
    private String InsuredNo; 
    /** 投保人客户号码 */
    private String AppntNo; 
    /** 账户类型 */
    private String AccType; 
    /** 账户结算方式 */
    private String AccComputeFlag; 
    /** 账户成立日期 */
    private String  AccFoundDate;
    /** 账户成立时间 */
    private String AccFoundTime; 
    /** 结算日期 */
    private String  BalaDate;
    /** 结算时间 */
    private String BalaTime; 
    /** 管理费比例 */
    private double FeeRate; 
    /** 本次管理费金额 */
    private double Fee; 
    /** 本次管理费单位数 */
    private double FeeUnit; 
    /** 操作员 */
    private String Operator; 
    /** 入机日期 */
    private String  MakeDate;
    /** 入机时间 */
    private String MakeTime; 
    /** 最后一次修改日期 */
    private String  ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime; 
    /** 管理费编码 */
    private String FeeCode; 
    /** 是否初始费用 */
    private String IsInitFee; 


    public static final int FIELDNUM = 31;    // 数据库表的字段个数

    private FDate fDate = new FDate();        // 处理日期
    public long getInsureAccClassFeeID() {
        return InsureAccClassFeeID;
    }
    public void setInsureAccClassFeeID(long aInsureAccClassFeeID) {
        InsureAccClassFeeID = aInsureAccClassFeeID;
    }
    public void setInsureAccClassFeeID(String aInsureAccClassFeeID) {
        if (aInsureAccClassFeeID != null && !aInsureAccClassFeeID.equals("")) {
            InsureAccClassFeeID = new Long(aInsureAccClassFeeID).longValue();
        }
    }

    public String getShardingID() {
        return ShardingID;
    }
    public void setShardingID(String aShardingID) {
        ShardingID = aShardingID;
    }
    public String getPolNo() {
        return PolNo;
    }
    public void setPolNo(String aPolNo) {
        PolNo = aPolNo;
    }
    public String getInsuAccNo() {
        return InsuAccNo;
    }
    public void setInsuAccNo(String aInsuAccNo) {
        InsuAccNo = aInsuAccNo;
    }
    public String getPayPlanCode() {
        return PayPlanCode;
    }
    public void setPayPlanCode(String aPayPlanCode) {
        PayPlanCode = aPayPlanCode;
    }
    public String getOtherNo() {
        return OtherNo;
    }
    public void setOtherNo(String aOtherNo) {
        OtherNo = aOtherNo;
    }
    public String getOtherType() {
        return OtherType;
    }
    public void setOtherType(String aOtherType) {
        OtherType = aOtherType;
    }
    public String getAccAscription() {
        return AccAscription;
    }
    public void setAccAscription(String aAccAscription) {
        AccAscription = aAccAscription;
    }
    public String getRiskCode() {
        return RiskCode;
    }
    public void setRiskCode(String aRiskCode) {
        RiskCode = aRiskCode;
    }
    public String getContNo() {
        return ContNo;
    }
    public void setContNo(String aContNo) {
        ContNo = aContNo;
    }
    public String getGrpPolNo() {
        return GrpPolNo;
    }
    public void setGrpPolNo(String aGrpPolNo) {
        GrpPolNo = aGrpPolNo;
    }
    public String getGrpContNo() {
        return GrpContNo;
    }
    public void setGrpContNo(String aGrpContNo) {
        GrpContNo = aGrpContNo;
    }
    public String getManageCom() {
        return ManageCom;
    }
    public void setManageCom(String aManageCom) {
        ManageCom = aManageCom;
    }
    public String getInsuredNo() {
        return InsuredNo;
    }
    public void setInsuredNo(String aInsuredNo) {
        InsuredNo = aInsuredNo;
    }
    public String getAppntNo() {
        return AppntNo;
    }
    public void setAppntNo(String aAppntNo) {
        AppntNo = aAppntNo;
    }
    public String getAccType() {
        return AccType;
    }
    public void setAccType(String aAccType) {
        AccType = aAccType;
    }
    public String getAccComputeFlag() {
        return AccComputeFlag;
    }
    public void setAccComputeFlag(String aAccComputeFlag) {
        AccComputeFlag = aAccComputeFlag;
    }
    public String getAccFoundDate() {
        return AccFoundDate;
    }
    public void setAccFoundDate(String aAccFoundDate) {
        AccFoundDate = aAccFoundDate;
    }
    public String getAccFoundTime() {
        return AccFoundTime;
    }
    public void setAccFoundTime(String aAccFoundTime) {
        AccFoundTime = aAccFoundTime;
    }
    public String getBalaDate() {
        return BalaDate;
    }
    public void setBalaDate(String aBalaDate) {
        BalaDate = aBalaDate;
    }
    public String getBalaTime() {
        return BalaTime;
    }
    public void setBalaTime(String aBalaTime) {
        BalaTime = aBalaTime;
    }
    public double getFeeRate() {
        return FeeRate;
    }
    public void setFeeRate(double aFeeRate) {
        FeeRate = aFeeRate;
    }
    public void setFeeRate(String aFeeRate) {
        if (aFeeRate != null && !aFeeRate.equals("")) {
            Double tDouble = new Double(aFeeRate);
            double d = tDouble.doubleValue();
            FeeRate = d;
        }
    }

    public double getFee() {
        return Fee;
    }
    public void setFee(double aFee) {
        Fee = aFee;
    }
    public void setFee(String aFee) {
        if (aFee != null && !aFee.equals("")) {
            Double tDouble = new Double(aFee);
            double d = tDouble.doubleValue();
            Fee = d;
        }
    }

    public double getFeeUnit() {
        return FeeUnit;
    }
    public void setFeeUnit(double aFeeUnit) {
        FeeUnit = aFeeUnit;
    }
    public void setFeeUnit(String aFeeUnit) {
        if (aFeeUnit != null && !aFeeUnit.equals("")) {
            Double tDouble = new Double(aFeeUnit);
            double d = tDouble.doubleValue();
            FeeUnit = d;
        }
    }

    public String getOperator() {
        return Operator;
    }
    public void setOperator(String aOperator) {
        Operator = aOperator;
    }
    public String getMakeDate() {
        return MakeDate;
    }
    public void setMakeDate(String aMakeDate) {
        MakeDate = aMakeDate;
    }
    public String getMakeTime() {
        return MakeTime;
    }
    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }
    public String getModifyDate() {
        return ModifyDate;
    }
    public void setModifyDate(String aModifyDate) {
        ModifyDate = aModifyDate;
    }
    public String getModifyTime() {
        return ModifyTime;
    }
    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }
    public String getFeeCode() {
        return FeeCode;
    }
    public void setFeeCode(String aFeeCode) {
        FeeCode = aFeeCode;
    }
    public String getIsInitFee() {
        return IsInitFee;
    }
    public void setIsInitFee(String aIsInitFee) {
        IsInitFee = aIsInitFee;
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("InsureAccClassFeeID") ) {
            return 0;
        }
        if( strFieldName.equals("ShardingID") ) {
            return 1;
        }
        if( strFieldName.equals("PolNo") ) {
            return 2;
        }
        if( strFieldName.equals("InsuAccNo") ) {
            return 3;
        }
        if( strFieldName.equals("PayPlanCode") ) {
            return 4;
        }
        if( strFieldName.equals("OtherNo") ) {
            return 5;
        }
        if( strFieldName.equals("OtherType") ) {
            return 6;
        }
        if( strFieldName.equals("AccAscription") ) {
            return 7;
        }
        if( strFieldName.equals("RiskCode") ) {
            return 8;
        }
        if( strFieldName.equals("ContNo") ) {
            return 9;
        }
        if( strFieldName.equals("GrpPolNo") ) {
            return 10;
        }
        if( strFieldName.equals("GrpContNo") ) {
            return 11;
        }
        if( strFieldName.equals("ManageCom") ) {
            return 12;
        }
        if( strFieldName.equals("InsuredNo") ) {
            return 13;
        }
        if( strFieldName.equals("AppntNo") ) {
            return 14;
        }
        if( strFieldName.equals("AccType") ) {
            return 15;
        }
        if( strFieldName.equals("AccComputeFlag") ) {
            return 16;
        }
        if( strFieldName.equals("AccFoundDate") ) {
            return 17;
        }
        if( strFieldName.equals("AccFoundTime") ) {
            return 18;
        }
        if( strFieldName.equals("BalaDate") ) {
            return 19;
        }
        if( strFieldName.equals("BalaTime") ) {
            return 20;
        }
        if( strFieldName.equals("FeeRate") ) {
            return 21;
        }
        if( strFieldName.equals("Fee") ) {
            return 22;
        }
        if( strFieldName.equals("FeeUnit") ) {
            return 23;
        }
        if( strFieldName.equals("Operator") ) {
            return 24;
        }
        if( strFieldName.equals("MakeDate") ) {
            return 25;
        }
        if( strFieldName.equals("MakeTime") ) {
            return 26;
        }
        if( strFieldName.equals("ModifyDate") ) {
            return 27;
        }
        if( strFieldName.equals("ModifyTime") ) {
            return 28;
        }
        if( strFieldName.equals("FeeCode") ) {
            return 29;
        }
        if( strFieldName.equals("IsInitFee") ) {
            return 30;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "InsureAccClassFeeID";
                break;
            case 1:
                strFieldName = "ShardingID";
                break;
            case 2:
                strFieldName = "PolNo";
                break;
            case 3:
                strFieldName = "InsuAccNo";
                break;
            case 4:
                strFieldName = "PayPlanCode";
                break;
            case 5:
                strFieldName = "OtherNo";
                break;
            case 6:
                strFieldName = "OtherType";
                break;
            case 7:
                strFieldName = "AccAscription";
                break;
            case 8:
                strFieldName = "RiskCode";
                break;
            case 9:
                strFieldName = "ContNo";
                break;
            case 10:
                strFieldName = "GrpPolNo";
                break;
            case 11:
                strFieldName = "GrpContNo";
                break;
            case 12:
                strFieldName = "ManageCom";
                break;
            case 13:
                strFieldName = "InsuredNo";
                break;
            case 14:
                strFieldName = "AppntNo";
                break;
            case 15:
                strFieldName = "AccType";
                break;
            case 16:
                strFieldName = "AccComputeFlag";
                break;
            case 17:
                strFieldName = "AccFoundDate";
                break;
            case 18:
                strFieldName = "AccFoundTime";
                break;
            case 19:
                strFieldName = "BalaDate";
                break;
            case 20:
                strFieldName = "BalaTime";
                break;
            case 21:
                strFieldName = "FeeRate";
                break;
            case 22:
                strFieldName = "Fee";
                break;
            case 23:
                strFieldName = "FeeUnit";
                break;
            case 24:
                strFieldName = "Operator";
                break;
            case 25:
                strFieldName = "MakeDate";
                break;
            case 26:
                strFieldName = "MakeTime";
                break;
            case 27:
                strFieldName = "ModifyDate";
                break;
            case 28:
                strFieldName = "ModifyTime";
                break;
            case 29:
                strFieldName = "FeeCode";
                break;
            case 30:
                strFieldName = "IsInitFee";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "INSUREACCCLASSFEEID":
                return Schema.TYPE_LONG;
            case "SHARDINGID":
                return Schema.TYPE_STRING;
            case "POLNO":
                return Schema.TYPE_STRING;
            case "INSUACCNO":
                return Schema.TYPE_STRING;
            case "PAYPLANCODE":
                return Schema.TYPE_STRING;
            case "OTHERNO":
                return Schema.TYPE_STRING;
            case "OTHERTYPE":
                return Schema.TYPE_STRING;
            case "ACCASCRIPTION":
                return Schema.TYPE_STRING;
            case "RISKCODE":
                return Schema.TYPE_STRING;
            case "CONTNO":
                return Schema.TYPE_STRING;
            case "GRPPOLNO":
                return Schema.TYPE_STRING;
            case "GRPCONTNO":
                return Schema.TYPE_STRING;
            case "MANAGECOM":
                return Schema.TYPE_STRING;
            case "INSUREDNO":
                return Schema.TYPE_STRING;
            case "APPNTNO":
                return Schema.TYPE_STRING;
            case "ACCTYPE":
                return Schema.TYPE_STRING;
            case "ACCCOMPUTEFLAG":
                return Schema.TYPE_STRING;
            case "ACCFOUNDDATE":
                return Schema.TYPE_STRING;
            case "ACCFOUNDTIME":
                return Schema.TYPE_STRING;
            case "BALADATE":
                return Schema.TYPE_STRING;
            case "BALATIME":
                return Schema.TYPE_STRING;
            case "FEERATE":
                return Schema.TYPE_DOUBLE;
            case "FEE":
                return Schema.TYPE_DOUBLE;
            case "FEEUNIT":
                return Schema.TYPE_DOUBLE;
            case "OPERATOR":
                return Schema.TYPE_STRING;
            case "MAKEDATE":
                return Schema.TYPE_STRING;
            case "MAKETIME":
                return Schema.TYPE_STRING;
            case "MODIFYDATE":
                return Schema.TYPE_STRING;
            case "MODIFYTIME":
                return Schema.TYPE_STRING;
            case "FEECODE":
                return Schema.TYPE_STRING;
            case "ISINITFEE":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_LONG;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_STRING;
            case 6:
                return Schema.TYPE_STRING;
            case 7:
                return Schema.TYPE_STRING;
            case 8:
                return Schema.TYPE_STRING;
            case 9:
                return Schema.TYPE_STRING;
            case 10:
                return Schema.TYPE_STRING;
            case 11:
                return Schema.TYPE_STRING;
            case 12:
                return Schema.TYPE_STRING;
            case 13:
                return Schema.TYPE_STRING;
            case 14:
                return Schema.TYPE_STRING;
            case 15:
                return Schema.TYPE_STRING;
            case 16:
                return Schema.TYPE_STRING;
            case 17:
                return Schema.TYPE_STRING;
            case 18:
                return Schema.TYPE_STRING;
            case 19:
                return Schema.TYPE_STRING;
            case 20:
                return Schema.TYPE_STRING;
            case 21:
                return Schema.TYPE_DOUBLE;
            case 22:
                return Schema.TYPE_DOUBLE;
            case 23:
                return Schema.TYPE_DOUBLE;
            case 24:
                return Schema.TYPE_STRING;
            case 25:
                return Schema.TYPE_STRING;
            case 26:
                return Schema.TYPE_STRING;
            case 27:
                return Schema.TYPE_STRING;
            case 28:
                return Schema.TYPE_STRING;
            case 29:
                return Schema.TYPE_STRING;
            case 30:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("InsureAccClassFeeID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InsureAccClassFeeID));
        }
        if (FCode.equalsIgnoreCase("ShardingID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ShardingID));
        }
        if (FCode.equalsIgnoreCase("PolNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PolNo));
        }
        if (FCode.equalsIgnoreCase("InsuAccNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InsuAccNo));
        }
        if (FCode.equalsIgnoreCase("PayPlanCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PayPlanCode));
        }
        if (FCode.equalsIgnoreCase("OtherNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OtherNo));
        }
        if (FCode.equalsIgnoreCase("OtherType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OtherType));
        }
        if (FCode.equalsIgnoreCase("AccAscription")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AccAscription));
        }
        if (FCode.equalsIgnoreCase("RiskCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RiskCode));
        }
        if (FCode.equalsIgnoreCase("ContNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ContNo));
        }
        if (FCode.equalsIgnoreCase("GrpPolNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GrpPolNo));
        }
        if (FCode.equalsIgnoreCase("GrpContNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GrpContNo));
        }
        if (FCode.equalsIgnoreCase("ManageCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
        }
        if (FCode.equalsIgnoreCase("InsuredNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InsuredNo));
        }
        if (FCode.equalsIgnoreCase("AppntNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AppntNo));
        }
        if (FCode.equalsIgnoreCase("AccType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AccType));
        }
        if (FCode.equalsIgnoreCase("AccComputeFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AccComputeFlag));
        }
        if (FCode.equalsIgnoreCase("AccFoundDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AccFoundDate));
        }
        if (FCode.equalsIgnoreCase("AccFoundTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AccFoundTime));
        }
        if (FCode.equalsIgnoreCase("BalaDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BalaDate));
        }
        if (FCode.equalsIgnoreCase("BalaTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BalaTime));
        }
        if (FCode.equalsIgnoreCase("FeeRate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FeeRate));
        }
        if (FCode.equalsIgnoreCase("Fee")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Fee));
        }
        if (FCode.equalsIgnoreCase("FeeUnit")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FeeUnit));
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeDate));
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyDate));
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (FCode.equalsIgnoreCase("FeeCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FeeCode));
        }
        if (FCode.equalsIgnoreCase("IsInitFee")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(IsInitFee));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = String.valueOf(InsureAccClassFeeID);
                break;
            case 1:
                strFieldValue = String.valueOf(ShardingID);
                break;
            case 2:
                strFieldValue = String.valueOf(PolNo);
                break;
            case 3:
                strFieldValue = String.valueOf(InsuAccNo);
                break;
            case 4:
                strFieldValue = String.valueOf(PayPlanCode);
                break;
            case 5:
                strFieldValue = String.valueOf(OtherNo);
                break;
            case 6:
                strFieldValue = String.valueOf(OtherType);
                break;
            case 7:
                strFieldValue = String.valueOf(AccAscription);
                break;
            case 8:
                strFieldValue = String.valueOf(RiskCode);
                break;
            case 9:
                strFieldValue = String.valueOf(ContNo);
                break;
            case 10:
                strFieldValue = String.valueOf(GrpPolNo);
                break;
            case 11:
                strFieldValue = String.valueOf(GrpContNo);
                break;
            case 12:
                strFieldValue = String.valueOf(ManageCom);
                break;
            case 13:
                strFieldValue = String.valueOf(InsuredNo);
                break;
            case 14:
                strFieldValue = String.valueOf(AppntNo);
                break;
            case 15:
                strFieldValue = String.valueOf(AccType);
                break;
            case 16:
                strFieldValue = String.valueOf(AccComputeFlag);
                break;
            case 17:
                strFieldValue = String.valueOf(AccFoundDate);
                break;
            case 18:
                strFieldValue = String.valueOf(AccFoundTime);
                break;
            case 19:
                strFieldValue = String.valueOf(BalaDate);
                break;
            case 20:
                strFieldValue = String.valueOf(BalaTime);
                break;
            case 21:
                strFieldValue = String.valueOf(FeeRate);
                break;
            case 22:
                strFieldValue = String.valueOf(Fee);
                break;
            case 23:
                strFieldValue = String.valueOf(FeeUnit);
                break;
            case 24:
                strFieldValue = String.valueOf(Operator);
                break;
            case 25:
                strFieldValue = String.valueOf(MakeDate);
                break;
            case 26:
                strFieldValue = String.valueOf(MakeTime);
                break;
            case 27:
                strFieldValue = String.valueOf(ModifyDate);
                break;
            case 28:
                strFieldValue = String.valueOf(ModifyTime);
                break;
            case 29:
                strFieldValue = String.valueOf(FeeCode);
                break;
            case 30:
                strFieldValue = String.valueOf(IsInitFee);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("InsureAccClassFeeID")) {
            if( FValue != null && !FValue.equals("")) {
                InsureAccClassFeeID = new Long(FValue).longValue();
            }
        }
        if (FCode.equalsIgnoreCase("ShardingID")) {
            if( FValue != null && !FValue.equals(""))
            {
                ShardingID = FValue.trim();
            }
            else
                ShardingID = null;
        }
        if (FCode.equalsIgnoreCase("PolNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                PolNo = FValue.trim();
            }
            else
                PolNo = null;
        }
        if (FCode.equalsIgnoreCase("InsuAccNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                InsuAccNo = FValue.trim();
            }
            else
                InsuAccNo = null;
        }
        if (FCode.equalsIgnoreCase("PayPlanCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                PayPlanCode = FValue.trim();
            }
            else
                PayPlanCode = null;
        }
        if (FCode.equalsIgnoreCase("OtherNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                OtherNo = FValue.trim();
            }
            else
                OtherNo = null;
        }
        if (FCode.equalsIgnoreCase("OtherType")) {
            if( FValue != null && !FValue.equals(""))
            {
                OtherType = FValue.trim();
            }
            else
                OtherType = null;
        }
        if (FCode.equalsIgnoreCase("AccAscription")) {
            if( FValue != null && !FValue.equals(""))
            {
                AccAscription = FValue.trim();
            }
            else
                AccAscription = null;
        }
        if (FCode.equalsIgnoreCase("RiskCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                RiskCode = FValue.trim();
            }
            else
                RiskCode = null;
        }
        if (FCode.equalsIgnoreCase("ContNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                ContNo = FValue.trim();
            }
            else
                ContNo = null;
        }
        if (FCode.equalsIgnoreCase("GrpPolNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                GrpPolNo = FValue.trim();
            }
            else
                GrpPolNo = null;
        }
        if (FCode.equalsIgnoreCase("GrpContNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                GrpContNo = FValue.trim();
            }
            else
                GrpContNo = null;
        }
        if (FCode.equalsIgnoreCase("ManageCom")) {
            if( FValue != null && !FValue.equals(""))
            {
                ManageCom = FValue.trim();
            }
            else
                ManageCom = null;
        }
        if (FCode.equalsIgnoreCase("InsuredNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                InsuredNo = FValue.trim();
            }
            else
                InsuredNo = null;
        }
        if (FCode.equalsIgnoreCase("AppntNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                AppntNo = FValue.trim();
            }
            else
                AppntNo = null;
        }
        if (FCode.equalsIgnoreCase("AccType")) {
            if( FValue != null && !FValue.equals(""))
            {
                AccType = FValue.trim();
            }
            else
                AccType = null;
        }
        if (FCode.equalsIgnoreCase("AccComputeFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                AccComputeFlag = FValue.trim();
            }
            else
                AccComputeFlag = null;
        }
        if (FCode.equalsIgnoreCase("AccFoundDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                AccFoundDate = FValue.trim();
            }
            else
                AccFoundDate = null;
        }
        if (FCode.equalsIgnoreCase("AccFoundTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                AccFoundTime = FValue.trim();
            }
            else
                AccFoundTime = null;
        }
        if (FCode.equalsIgnoreCase("BalaDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                BalaDate = FValue.trim();
            }
            else
                BalaDate = null;
        }
        if (FCode.equalsIgnoreCase("BalaTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                BalaTime = FValue.trim();
            }
            else
                BalaTime = null;
        }
        if (FCode.equalsIgnoreCase("FeeRate")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                FeeRate = d;
            }
        }
        if (FCode.equalsIgnoreCase("Fee")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                Fee = d;
            }
        }
        if (FCode.equalsIgnoreCase("FeeUnit")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                FeeUnit = d;
            }
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            if( FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
                Operator = null;
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeDate = FValue.trim();
            }
            else
                MakeDate = null;
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
                MakeTime = null;
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyDate = FValue.trim();
            }
            else
                ModifyDate = null;
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
                ModifyTime = null;
        }
        if (FCode.equalsIgnoreCase("FeeCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                FeeCode = FValue.trim();
            }
            else
                FeeCode = null;
        }
        if (FCode.equalsIgnoreCase("IsInitFee")) {
            if( FValue != null && !FValue.equals(""))
            {
                IsInitFee = FValue.trim();
            }
            else
                IsInitFee = null;
        }
        return true;
    }


    public String toString() {
    return "LCInsureAccClassFeePojo [" +
            "InsureAccClassFeeID="+InsureAccClassFeeID +
            ", ShardingID="+ShardingID +
            ", PolNo="+PolNo +
            ", InsuAccNo="+InsuAccNo +
            ", PayPlanCode="+PayPlanCode +
            ", OtherNo="+OtherNo +
            ", OtherType="+OtherType +
            ", AccAscription="+AccAscription +
            ", RiskCode="+RiskCode +
            ", ContNo="+ContNo +
            ", GrpPolNo="+GrpPolNo +
            ", GrpContNo="+GrpContNo +
            ", ManageCom="+ManageCom +
            ", InsuredNo="+InsuredNo +
            ", AppntNo="+AppntNo +
            ", AccType="+AccType +
            ", AccComputeFlag="+AccComputeFlag +
            ", AccFoundDate="+AccFoundDate +
            ", AccFoundTime="+AccFoundTime +
            ", BalaDate="+BalaDate +
            ", BalaTime="+BalaTime +
            ", FeeRate="+FeeRate +
            ", Fee="+Fee +
            ", FeeUnit="+FeeUnit +
            ", Operator="+Operator +
            ", MakeDate="+MakeDate +
            ", MakeTime="+MakeTime +
            ", ModifyDate="+ModifyDate +
            ", ModifyTime="+ModifyTime +
            ", FeeCode="+FeeCode +
            ", IsInitFee="+IsInitFee +"]";
    }
}
