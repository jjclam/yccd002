/**
 * Copyright (c) 2017 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.entity;

import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.Pojo;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;

import java.io.Serializable;

/**
 * <p>ClassName: LCInsureAccPojo </p>
 * <p>Copyright: Copyright (c) 2017</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2017-10-17
 */
public class LCInsureAccPojo implements Pojo,Serializable {
    // @Field
    /** Id */
    private long InsureAccID; 
    /** Shardingid */
    private String ShardingID; 
    /** 保单险种号码 */
    private String PolNo; 
    /** 保险帐户号码 */
    private String InsuAccNo; 
    /** 合同号码 */
    private String ContNo; 
    /** 集体合同号码 */
    private String GrpContNo; 
    /** 集体保单险种号码 */
    private String GrpPolNo; 
    /** 印刷号码 */
    private String PrtNo; 
    /** 险种编码 */
    private String RiskCode; 
    /** 账户类型 */
    private String AccType; 
    /** 投资类型 */
    private String InvestType; 
    /** 基金公司代码 */
    private String FundCompanyCode; 
    /** 被保人客户号码 */
    private String InsuredNo; 
    /** 投保人客户号码 */
    private String AppntNo; 
    /** 账户所有者 */
    private String Owner; 
    /** 账户结算方式 */
    private String AccComputeFlag; 
    /** 账户成立日期 */
    private String  AccFoundDate;
    /** 账户成立时间 */
    private String AccFoundTime; 
    /** 结算日期 */
    private String  BalaDate;
    /** 结算时间 */
    private String BalaTime; 
    /** 累计交费 */
    private double SumPay; 
    /** 累计领取 */
    private double SumPaym; 
    /** 期初账户现金余额 */
    private double LastAccBala; 
    /** 期初账户单位数 */
    private double LastUnitCount; 
    /** 期初账户单位价格 */
    private double LastUnitPrice; 
    /** 帐户现金余额 */
    private double InsuAccBala; 
    /** 帐户单位数 */
    private double UnitCount; 
    /** 帐户单位价格 */
    private double UnitPrice; 
    /** 账户可领金额 */
    private double InsuAccGetMoney; 
    /** 冻结金额 */
    private double FrozenMoney; 
    /** 状态 */
    private String State; 
    /** 管理机构 */
    private String ManageCom; 
    /** 操作员 */
    private String Operator; 
    /** 入机日期 */
    private String  MakeDate;
    /** 入机时间 */
    private String MakeTime; 
    /** 最后一次修改日期 */
    private String  ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime; 
    /** 万能贷款宽限期开始 */
    private String  WNPreEndDate;


    public static final int FIELDNUM = 38;    // 数据库表的字段个数

    private FDate fDate = new FDate();        // 处理日期
    public long getInsureAccID() {
        return InsureAccID;
    }
    public void setInsureAccID(long aInsureAccID) {
        InsureAccID = aInsureAccID;
    }
    public void setInsureAccID(String aInsureAccID) {
        if (aInsureAccID != null && !aInsureAccID.equals("")) {
            InsureAccID = new Long(aInsureAccID).longValue();
        }
    }

    public String getShardingID() {
        return ShardingID;
    }
    public void setShardingID(String aShardingID) {
        ShardingID = aShardingID;
    }
    public String getPolNo() {
        return PolNo;
    }
    public void setPolNo(String aPolNo) {
        PolNo = aPolNo;
    }
    public String getInsuAccNo() {
        return InsuAccNo;
    }
    public void setInsuAccNo(String aInsuAccNo) {
        InsuAccNo = aInsuAccNo;
    }
    public String getContNo() {
        return ContNo;
    }
    public void setContNo(String aContNo) {
        ContNo = aContNo;
    }
    public String getGrpContNo() {
        return GrpContNo;
    }
    public void setGrpContNo(String aGrpContNo) {
        GrpContNo = aGrpContNo;
    }
    public String getGrpPolNo() {
        return GrpPolNo;
    }
    public void setGrpPolNo(String aGrpPolNo) {
        GrpPolNo = aGrpPolNo;
    }
    public String getPrtNo() {
        return PrtNo;
    }
    public void setPrtNo(String aPrtNo) {
        PrtNo = aPrtNo;
    }
    public String getRiskCode() {
        return RiskCode;
    }
    public void setRiskCode(String aRiskCode) {
        RiskCode = aRiskCode;
    }
    public String getAccType() {
        return AccType;
    }
    public void setAccType(String aAccType) {
        AccType = aAccType;
    }
    public String getInvestType() {
        return InvestType;
    }
    public void setInvestType(String aInvestType) {
        InvestType = aInvestType;
    }
    public String getFundCompanyCode() {
        return FundCompanyCode;
    }
    public void setFundCompanyCode(String aFundCompanyCode) {
        FundCompanyCode = aFundCompanyCode;
    }
    public String getInsuredNo() {
        return InsuredNo;
    }
    public void setInsuredNo(String aInsuredNo) {
        InsuredNo = aInsuredNo;
    }
    public String getAppntNo() {
        return AppntNo;
    }
    public void setAppntNo(String aAppntNo) {
        AppntNo = aAppntNo;
    }
    public String getOwner() {
        return Owner;
    }
    public void setOwner(String aOwner) {
        Owner = aOwner;
    }
    public String getAccComputeFlag() {
        return AccComputeFlag;
    }
    public void setAccComputeFlag(String aAccComputeFlag) {
        AccComputeFlag = aAccComputeFlag;
    }
    public String getAccFoundDate() {
        return AccFoundDate;
    }
    public void setAccFoundDate(String aAccFoundDate) {
        AccFoundDate = aAccFoundDate;
    }
    public String getAccFoundTime() {
        return AccFoundTime;
    }
    public void setAccFoundTime(String aAccFoundTime) {
        AccFoundTime = aAccFoundTime;
    }
    public String getBalaDate() {
        return BalaDate;
    }
    public void setBalaDate(String aBalaDate) {
        BalaDate = aBalaDate;
    }
    public String getBalaTime() {
        return BalaTime;
    }
    public void setBalaTime(String aBalaTime) {
        BalaTime = aBalaTime;
    }
    public double getSumPay() {
        return SumPay;
    }
    public void setSumPay(double aSumPay) {
        SumPay = aSumPay;
    }
    public void setSumPay(String aSumPay) {
        if (aSumPay != null && !aSumPay.equals("")) {
            Double tDouble = new Double(aSumPay);
            double d = tDouble.doubleValue();
            SumPay = d;
        }
    }

    public double getSumPaym() {
        return SumPaym;
    }
    public void setSumPaym(double aSumPaym) {
        SumPaym = aSumPaym;
    }
    public void setSumPaym(String aSumPaym) {
        if (aSumPaym != null && !aSumPaym.equals("")) {
            Double tDouble = new Double(aSumPaym);
            double d = tDouble.doubleValue();
            SumPaym = d;
        }
    }

    public double getLastAccBala() {
        return LastAccBala;
    }
    public void setLastAccBala(double aLastAccBala) {
        LastAccBala = aLastAccBala;
    }
    public void setLastAccBala(String aLastAccBala) {
        if (aLastAccBala != null && !aLastAccBala.equals("")) {
            Double tDouble = new Double(aLastAccBala);
            double d = tDouble.doubleValue();
            LastAccBala = d;
        }
    }

    public double getLastUnitCount() {
        return LastUnitCount;
    }
    public void setLastUnitCount(double aLastUnitCount) {
        LastUnitCount = aLastUnitCount;
    }
    public void setLastUnitCount(String aLastUnitCount) {
        if (aLastUnitCount != null && !aLastUnitCount.equals("")) {
            Double tDouble = new Double(aLastUnitCount);
            double d = tDouble.doubleValue();
            LastUnitCount = d;
        }
    }

    public double getLastUnitPrice() {
        return LastUnitPrice;
    }
    public void setLastUnitPrice(double aLastUnitPrice) {
        LastUnitPrice = aLastUnitPrice;
    }
    public void setLastUnitPrice(String aLastUnitPrice) {
        if (aLastUnitPrice != null && !aLastUnitPrice.equals("")) {
            Double tDouble = new Double(aLastUnitPrice);
            double d = tDouble.doubleValue();
            LastUnitPrice = d;
        }
    }

    public double getInsuAccBala() {
        return InsuAccBala;
    }
    public void setInsuAccBala(double aInsuAccBala) {
        InsuAccBala = aInsuAccBala;
    }
    public void setInsuAccBala(String aInsuAccBala) {
        if (aInsuAccBala != null && !aInsuAccBala.equals("")) {
            Double tDouble = new Double(aInsuAccBala);
            double d = tDouble.doubleValue();
            InsuAccBala = d;
        }
    }

    public double getUnitCount() {
        return UnitCount;
    }
    public void setUnitCount(double aUnitCount) {
        UnitCount = aUnitCount;
    }
    public void setUnitCount(String aUnitCount) {
        if (aUnitCount != null && !aUnitCount.equals("")) {
            Double tDouble = new Double(aUnitCount);
            double d = tDouble.doubleValue();
            UnitCount = d;
        }
    }

    public double getUnitPrice() {
        return UnitPrice;
    }
    public void setUnitPrice(double aUnitPrice) {
        UnitPrice = aUnitPrice;
    }
    public void setUnitPrice(String aUnitPrice) {
        if (aUnitPrice != null && !aUnitPrice.equals("")) {
            Double tDouble = new Double(aUnitPrice);
            double d = tDouble.doubleValue();
            UnitPrice = d;
        }
    }

    public double getInsuAccGetMoney() {
        return InsuAccGetMoney;
    }
    public void setInsuAccGetMoney(double aInsuAccGetMoney) {
        InsuAccGetMoney = aInsuAccGetMoney;
    }
    public void setInsuAccGetMoney(String aInsuAccGetMoney) {
        if (aInsuAccGetMoney != null && !aInsuAccGetMoney.equals("")) {
            Double tDouble = new Double(aInsuAccGetMoney);
            double d = tDouble.doubleValue();
            InsuAccGetMoney = d;
        }
    }

    public double getFrozenMoney() {
        return FrozenMoney;
    }
    public void setFrozenMoney(double aFrozenMoney) {
        FrozenMoney = aFrozenMoney;
    }
    public void setFrozenMoney(String aFrozenMoney) {
        if (aFrozenMoney != null && !aFrozenMoney.equals("")) {
            Double tDouble = new Double(aFrozenMoney);
            double d = tDouble.doubleValue();
            FrozenMoney = d;
        }
    }

    public String getState() {
        return State;
    }
    public void setState(String aState) {
        State = aState;
    }
    public String getManageCom() {
        return ManageCom;
    }
    public void setManageCom(String aManageCom) {
        ManageCom = aManageCom;
    }
    public String getOperator() {
        return Operator;
    }
    public void setOperator(String aOperator) {
        Operator = aOperator;
    }
    public String getMakeDate() {
        return MakeDate;
    }
    public void setMakeDate(String aMakeDate) {
        MakeDate = aMakeDate;
    }
    public String getMakeTime() {
        return MakeTime;
    }
    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }
    public String getModifyDate() {
        return ModifyDate;
    }
    public void setModifyDate(String aModifyDate) {
        ModifyDate = aModifyDate;
    }
    public String getModifyTime() {
        return ModifyTime;
    }
    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }
    public String getWNPreEndDate() {
        return WNPreEndDate;
    }
    public void setWNPreEndDate(String aWNPreEndDate) {
        WNPreEndDate = aWNPreEndDate;
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("InsureAccID") ) {
            return 0;
        }
        if( strFieldName.equals("ShardingID") ) {
            return 1;
        }
        if( strFieldName.equals("PolNo") ) {
            return 2;
        }
        if( strFieldName.equals("InsuAccNo") ) {
            return 3;
        }
        if( strFieldName.equals("ContNo") ) {
            return 4;
        }
        if( strFieldName.equals("GrpContNo") ) {
            return 5;
        }
        if( strFieldName.equals("GrpPolNo") ) {
            return 6;
        }
        if( strFieldName.equals("PrtNo") ) {
            return 7;
        }
        if( strFieldName.equals("RiskCode") ) {
            return 8;
        }
        if( strFieldName.equals("AccType") ) {
            return 9;
        }
        if( strFieldName.equals("InvestType") ) {
            return 10;
        }
        if( strFieldName.equals("FundCompanyCode") ) {
            return 11;
        }
        if( strFieldName.equals("InsuredNo") ) {
            return 12;
        }
        if( strFieldName.equals("AppntNo") ) {
            return 13;
        }
        if( strFieldName.equals("Owner") ) {
            return 14;
        }
        if( strFieldName.equals("AccComputeFlag") ) {
            return 15;
        }
        if( strFieldName.equals("AccFoundDate") ) {
            return 16;
        }
        if( strFieldName.equals("AccFoundTime") ) {
            return 17;
        }
        if( strFieldName.equals("BalaDate") ) {
            return 18;
        }
        if( strFieldName.equals("BalaTime") ) {
            return 19;
        }
        if( strFieldName.equals("SumPay") ) {
            return 20;
        }
        if( strFieldName.equals("SumPaym") ) {
            return 21;
        }
        if( strFieldName.equals("LastAccBala") ) {
            return 22;
        }
        if( strFieldName.equals("LastUnitCount") ) {
            return 23;
        }
        if( strFieldName.equals("LastUnitPrice") ) {
            return 24;
        }
        if( strFieldName.equals("InsuAccBala") ) {
            return 25;
        }
        if( strFieldName.equals("UnitCount") ) {
            return 26;
        }
        if( strFieldName.equals("UnitPrice") ) {
            return 27;
        }
        if( strFieldName.equals("InsuAccGetMoney") ) {
            return 28;
        }
        if( strFieldName.equals("FrozenMoney") ) {
            return 29;
        }
        if( strFieldName.equals("State") ) {
            return 30;
        }
        if( strFieldName.equals("ManageCom") ) {
            return 31;
        }
        if( strFieldName.equals("Operator") ) {
            return 32;
        }
        if( strFieldName.equals("MakeDate") ) {
            return 33;
        }
        if( strFieldName.equals("MakeTime") ) {
            return 34;
        }
        if( strFieldName.equals("ModifyDate") ) {
            return 35;
        }
        if( strFieldName.equals("ModifyTime") ) {
            return 36;
        }
        if( strFieldName.equals("WNPreEndDate") ) {
            return 37;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "InsureAccID";
                break;
            case 1:
                strFieldName = "ShardingID";
                break;
            case 2:
                strFieldName = "PolNo";
                break;
            case 3:
                strFieldName = "InsuAccNo";
                break;
            case 4:
                strFieldName = "ContNo";
                break;
            case 5:
                strFieldName = "GrpContNo";
                break;
            case 6:
                strFieldName = "GrpPolNo";
                break;
            case 7:
                strFieldName = "PrtNo";
                break;
            case 8:
                strFieldName = "RiskCode";
                break;
            case 9:
                strFieldName = "AccType";
                break;
            case 10:
                strFieldName = "InvestType";
                break;
            case 11:
                strFieldName = "FundCompanyCode";
                break;
            case 12:
                strFieldName = "InsuredNo";
                break;
            case 13:
                strFieldName = "AppntNo";
                break;
            case 14:
                strFieldName = "Owner";
                break;
            case 15:
                strFieldName = "AccComputeFlag";
                break;
            case 16:
                strFieldName = "AccFoundDate";
                break;
            case 17:
                strFieldName = "AccFoundTime";
                break;
            case 18:
                strFieldName = "BalaDate";
                break;
            case 19:
                strFieldName = "BalaTime";
                break;
            case 20:
                strFieldName = "SumPay";
                break;
            case 21:
                strFieldName = "SumPaym";
                break;
            case 22:
                strFieldName = "LastAccBala";
                break;
            case 23:
                strFieldName = "LastUnitCount";
                break;
            case 24:
                strFieldName = "LastUnitPrice";
                break;
            case 25:
                strFieldName = "InsuAccBala";
                break;
            case 26:
                strFieldName = "UnitCount";
                break;
            case 27:
                strFieldName = "UnitPrice";
                break;
            case 28:
                strFieldName = "InsuAccGetMoney";
                break;
            case 29:
                strFieldName = "FrozenMoney";
                break;
            case 30:
                strFieldName = "State";
                break;
            case 31:
                strFieldName = "ManageCom";
                break;
            case 32:
                strFieldName = "Operator";
                break;
            case 33:
                strFieldName = "MakeDate";
                break;
            case 34:
                strFieldName = "MakeTime";
                break;
            case 35:
                strFieldName = "ModifyDate";
                break;
            case 36:
                strFieldName = "ModifyTime";
                break;
            case 37:
                strFieldName = "WNPreEndDate";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "INSUREACCID":
                return Schema.TYPE_LONG;
            case "SHARDINGID":
                return Schema.TYPE_STRING;
            case "POLNO":
                return Schema.TYPE_STRING;
            case "INSUACCNO":
                return Schema.TYPE_STRING;
            case "CONTNO":
                return Schema.TYPE_STRING;
            case "GRPCONTNO":
                return Schema.TYPE_STRING;
            case "GRPPOLNO":
                return Schema.TYPE_STRING;
            case "PRTNO":
                return Schema.TYPE_STRING;
            case "RISKCODE":
                return Schema.TYPE_STRING;
            case "ACCTYPE":
                return Schema.TYPE_STRING;
            case "INVESTTYPE":
                return Schema.TYPE_STRING;
            case "FUNDCOMPANYCODE":
                return Schema.TYPE_STRING;
            case "INSUREDNO":
                return Schema.TYPE_STRING;
            case "APPNTNO":
                return Schema.TYPE_STRING;
            case "OWNER":
                return Schema.TYPE_STRING;
            case "ACCCOMPUTEFLAG":
                return Schema.TYPE_STRING;
            case "ACCFOUNDDATE":
                return Schema.TYPE_STRING;
            case "ACCFOUNDTIME":
                return Schema.TYPE_STRING;
            case "BALADATE":
                return Schema.TYPE_STRING;
            case "BALATIME":
                return Schema.TYPE_STRING;
            case "SUMPAY":
                return Schema.TYPE_DOUBLE;
            case "SUMPAYM":
                return Schema.TYPE_DOUBLE;
            case "LASTACCBALA":
                return Schema.TYPE_DOUBLE;
            case "LASTUNITCOUNT":
                return Schema.TYPE_DOUBLE;
            case "LASTUNITPRICE":
                return Schema.TYPE_DOUBLE;
            case "INSUACCBALA":
                return Schema.TYPE_DOUBLE;
            case "UNITCOUNT":
                return Schema.TYPE_DOUBLE;
            case "UNITPRICE":
                return Schema.TYPE_DOUBLE;
            case "INSUACCGETMONEY":
                return Schema.TYPE_DOUBLE;
            case "FROZENMONEY":
                return Schema.TYPE_DOUBLE;
            case "STATE":
                return Schema.TYPE_STRING;
            case "MANAGECOM":
                return Schema.TYPE_STRING;
            case "OPERATOR":
                return Schema.TYPE_STRING;
            case "MAKEDATE":
                return Schema.TYPE_STRING;
            case "MAKETIME":
                return Schema.TYPE_STRING;
            case "MODIFYDATE":
                return Schema.TYPE_STRING;
            case "MODIFYTIME":
                return Schema.TYPE_STRING;
            case "WNPREENDDATE":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_LONG;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_STRING;
            case 6:
                return Schema.TYPE_STRING;
            case 7:
                return Schema.TYPE_STRING;
            case 8:
                return Schema.TYPE_STRING;
            case 9:
                return Schema.TYPE_STRING;
            case 10:
                return Schema.TYPE_STRING;
            case 11:
                return Schema.TYPE_STRING;
            case 12:
                return Schema.TYPE_STRING;
            case 13:
                return Schema.TYPE_STRING;
            case 14:
                return Schema.TYPE_STRING;
            case 15:
                return Schema.TYPE_STRING;
            case 16:
                return Schema.TYPE_STRING;
            case 17:
                return Schema.TYPE_STRING;
            case 18:
                return Schema.TYPE_STRING;
            case 19:
                return Schema.TYPE_STRING;
            case 20:
                return Schema.TYPE_DOUBLE;
            case 21:
                return Schema.TYPE_DOUBLE;
            case 22:
                return Schema.TYPE_DOUBLE;
            case 23:
                return Schema.TYPE_DOUBLE;
            case 24:
                return Schema.TYPE_DOUBLE;
            case 25:
                return Schema.TYPE_DOUBLE;
            case 26:
                return Schema.TYPE_DOUBLE;
            case 27:
                return Schema.TYPE_DOUBLE;
            case 28:
                return Schema.TYPE_DOUBLE;
            case 29:
                return Schema.TYPE_DOUBLE;
            case 30:
                return Schema.TYPE_STRING;
            case 31:
                return Schema.TYPE_STRING;
            case 32:
                return Schema.TYPE_STRING;
            case 33:
                return Schema.TYPE_STRING;
            case 34:
                return Schema.TYPE_STRING;
            case 35:
                return Schema.TYPE_STRING;
            case 36:
                return Schema.TYPE_STRING;
            case 37:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("InsureAccID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InsureAccID));
        }
        if (FCode.equalsIgnoreCase("ShardingID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ShardingID));
        }
        if (FCode.equalsIgnoreCase("PolNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PolNo));
        }
        if (FCode.equalsIgnoreCase("InsuAccNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InsuAccNo));
        }
        if (FCode.equalsIgnoreCase("ContNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ContNo));
        }
        if (FCode.equalsIgnoreCase("GrpContNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GrpContNo));
        }
        if (FCode.equalsIgnoreCase("GrpPolNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GrpPolNo));
        }
        if (FCode.equalsIgnoreCase("PrtNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PrtNo));
        }
        if (FCode.equalsIgnoreCase("RiskCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RiskCode));
        }
        if (FCode.equalsIgnoreCase("AccType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AccType));
        }
        if (FCode.equalsIgnoreCase("InvestType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InvestType));
        }
        if (FCode.equalsIgnoreCase("FundCompanyCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FundCompanyCode));
        }
        if (FCode.equalsIgnoreCase("InsuredNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InsuredNo));
        }
        if (FCode.equalsIgnoreCase("AppntNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AppntNo));
        }
        if (FCode.equalsIgnoreCase("Owner")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Owner));
        }
        if (FCode.equalsIgnoreCase("AccComputeFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AccComputeFlag));
        }
        if (FCode.equalsIgnoreCase("AccFoundDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AccFoundDate));
        }
        if (FCode.equalsIgnoreCase("AccFoundTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AccFoundTime));
        }
        if (FCode.equalsIgnoreCase("BalaDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BalaDate));
        }
        if (FCode.equalsIgnoreCase("BalaTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BalaTime));
        }
        if (FCode.equalsIgnoreCase("SumPay")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SumPay));
        }
        if (FCode.equalsIgnoreCase("SumPaym")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SumPaym));
        }
        if (FCode.equalsIgnoreCase("LastAccBala")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(LastAccBala));
        }
        if (FCode.equalsIgnoreCase("LastUnitCount")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(LastUnitCount));
        }
        if (FCode.equalsIgnoreCase("LastUnitPrice")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(LastUnitPrice));
        }
        if (FCode.equalsIgnoreCase("InsuAccBala")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InsuAccBala));
        }
        if (FCode.equalsIgnoreCase("UnitCount")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(UnitCount));
        }
        if (FCode.equalsIgnoreCase("UnitPrice")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(UnitPrice));
        }
        if (FCode.equalsIgnoreCase("InsuAccGetMoney")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InsuAccGetMoney));
        }
        if (FCode.equalsIgnoreCase("FrozenMoney")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FrozenMoney));
        }
        if (FCode.equalsIgnoreCase("State")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(State));
        }
        if (FCode.equalsIgnoreCase("ManageCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeDate));
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyDate));
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (FCode.equalsIgnoreCase("WNPreEndDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(WNPreEndDate));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = String.valueOf(InsureAccID);
                break;
            case 1:
                strFieldValue = String.valueOf(ShardingID);
                break;
            case 2:
                strFieldValue = String.valueOf(PolNo);
                break;
            case 3:
                strFieldValue = String.valueOf(InsuAccNo);
                break;
            case 4:
                strFieldValue = String.valueOf(ContNo);
                break;
            case 5:
                strFieldValue = String.valueOf(GrpContNo);
                break;
            case 6:
                strFieldValue = String.valueOf(GrpPolNo);
                break;
            case 7:
                strFieldValue = String.valueOf(PrtNo);
                break;
            case 8:
                strFieldValue = String.valueOf(RiskCode);
                break;
            case 9:
                strFieldValue = String.valueOf(AccType);
                break;
            case 10:
                strFieldValue = String.valueOf(InvestType);
                break;
            case 11:
                strFieldValue = String.valueOf(FundCompanyCode);
                break;
            case 12:
                strFieldValue = String.valueOf(InsuredNo);
                break;
            case 13:
                strFieldValue = String.valueOf(AppntNo);
                break;
            case 14:
                strFieldValue = String.valueOf(Owner);
                break;
            case 15:
                strFieldValue = String.valueOf(AccComputeFlag);
                break;
            case 16:
                strFieldValue = String.valueOf(AccFoundDate);
                break;
            case 17:
                strFieldValue = String.valueOf(AccFoundTime);
                break;
            case 18:
                strFieldValue = String.valueOf(BalaDate);
                break;
            case 19:
                strFieldValue = String.valueOf(BalaTime);
                break;
            case 20:
                strFieldValue = String.valueOf(SumPay);
                break;
            case 21:
                strFieldValue = String.valueOf(SumPaym);
                break;
            case 22:
                strFieldValue = String.valueOf(LastAccBala);
                break;
            case 23:
                strFieldValue = String.valueOf(LastUnitCount);
                break;
            case 24:
                strFieldValue = String.valueOf(LastUnitPrice);
                break;
            case 25:
                strFieldValue = String.valueOf(InsuAccBala);
                break;
            case 26:
                strFieldValue = String.valueOf(UnitCount);
                break;
            case 27:
                strFieldValue = String.valueOf(UnitPrice);
                break;
            case 28:
                strFieldValue = String.valueOf(InsuAccGetMoney);
                break;
            case 29:
                strFieldValue = String.valueOf(FrozenMoney);
                break;
            case 30:
                strFieldValue = String.valueOf(State);
                break;
            case 31:
                strFieldValue = String.valueOf(ManageCom);
                break;
            case 32:
                strFieldValue = String.valueOf(Operator);
                break;
            case 33:
                strFieldValue = String.valueOf(MakeDate);
                break;
            case 34:
                strFieldValue = String.valueOf(MakeTime);
                break;
            case 35:
                strFieldValue = String.valueOf(ModifyDate);
                break;
            case 36:
                strFieldValue = String.valueOf(ModifyTime);
                break;
            case 37:
                strFieldValue = String.valueOf(WNPreEndDate);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("InsureAccID")) {
            if( FValue != null && !FValue.equals("")) {
                InsureAccID = new Long(FValue).longValue();
            }
        }
        if (FCode.equalsIgnoreCase("ShardingID")) {
            if( FValue != null && !FValue.equals(""))
            {
                ShardingID = FValue.trim();
            }
            else
                ShardingID = null;
        }
        if (FCode.equalsIgnoreCase("PolNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                PolNo = FValue.trim();
            }
            else
                PolNo = null;
        }
        if (FCode.equalsIgnoreCase("InsuAccNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                InsuAccNo = FValue.trim();
            }
            else
                InsuAccNo = null;
        }
        if (FCode.equalsIgnoreCase("ContNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                ContNo = FValue.trim();
            }
            else
                ContNo = null;
        }
        if (FCode.equalsIgnoreCase("GrpContNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                GrpContNo = FValue.trim();
            }
            else
                GrpContNo = null;
        }
        if (FCode.equalsIgnoreCase("GrpPolNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                GrpPolNo = FValue.trim();
            }
            else
                GrpPolNo = null;
        }
        if (FCode.equalsIgnoreCase("PrtNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                PrtNo = FValue.trim();
            }
            else
                PrtNo = null;
        }
        if (FCode.equalsIgnoreCase("RiskCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                RiskCode = FValue.trim();
            }
            else
                RiskCode = null;
        }
        if (FCode.equalsIgnoreCase("AccType")) {
            if( FValue != null && !FValue.equals(""))
            {
                AccType = FValue.trim();
            }
            else
                AccType = null;
        }
        if (FCode.equalsIgnoreCase("InvestType")) {
            if( FValue != null && !FValue.equals(""))
            {
                InvestType = FValue.trim();
            }
            else
                InvestType = null;
        }
        if (FCode.equalsIgnoreCase("FundCompanyCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                FundCompanyCode = FValue.trim();
            }
            else
                FundCompanyCode = null;
        }
        if (FCode.equalsIgnoreCase("InsuredNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                InsuredNo = FValue.trim();
            }
            else
                InsuredNo = null;
        }
        if (FCode.equalsIgnoreCase("AppntNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                AppntNo = FValue.trim();
            }
            else
                AppntNo = null;
        }
        if (FCode.equalsIgnoreCase("Owner")) {
            if( FValue != null && !FValue.equals(""))
            {
                Owner = FValue.trim();
            }
            else
                Owner = null;
        }
        if (FCode.equalsIgnoreCase("AccComputeFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                AccComputeFlag = FValue.trim();
            }
            else
                AccComputeFlag = null;
        }
        if (FCode.equalsIgnoreCase("AccFoundDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                AccFoundDate = FValue.trim();
            }
            else
                AccFoundDate = null;
        }
        if (FCode.equalsIgnoreCase("AccFoundTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                AccFoundTime = FValue.trim();
            }
            else
                AccFoundTime = null;
        }
        if (FCode.equalsIgnoreCase("BalaDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                BalaDate = FValue.trim();
            }
            else
                BalaDate = null;
        }
        if (FCode.equalsIgnoreCase("BalaTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                BalaTime = FValue.trim();
            }
            else
                BalaTime = null;
        }
        if (FCode.equalsIgnoreCase("SumPay")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                SumPay = d;
            }
        }
        if (FCode.equalsIgnoreCase("SumPaym")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                SumPaym = d;
            }
        }
        if (FCode.equalsIgnoreCase("LastAccBala")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                LastAccBala = d;
            }
        }
        if (FCode.equalsIgnoreCase("LastUnitCount")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                LastUnitCount = d;
            }
        }
        if (FCode.equalsIgnoreCase("LastUnitPrice")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                LastUnitPrice = d;
            }
        }
        if (FCode.equalsIgnoreCase("InsuAccBala")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                InsuAccBala = d;
            }
        }
        if (FCode.equalsIgnoreCase("UnitCount")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                UnitCount = d;
            }
        }
        if (FCode.equalsIgnoreCase("UnitPrice")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                UnitPrice = d;
            }
        }
        if (FCode.equalsIgnoreCase("InsuAccGetMoney")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                InsuAccGetMoney = d;
            }
        }
        if (FCode.equalsIgnoreCase("FrozenMoney")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                FrozenMoney = d;
            }
        }
        if (FCode.equalsIgnoreCase("State")) {
            if( FValue != null && !FValue.equals(""))
            {
                State = FValue.trim();
            }
            else
                State = null;
        }
        if (FCode.equalsIgnoreCase("ManageCom")) {
            if( FValue != null && !FValue.equals(""))
            {
                ManageCom = FValue.trim();
            }
            else
                ManageCom = null;
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            if( FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
                Operator = null;
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeDate = FValue.trim();
            }
            else
                MakeDate = null;
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
                MakeTime = null;
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyDate = FValue.trim();
            }
            else
                ModifyDate = null;
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
                ModifyTime = null;
        }
        if (FCode.equalsIgnoreCase("WNPreEndDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                WNPreEndDate = FValue.trim();
            }
            else
                WNPreEndDate = null;
        }
        return true;
    }


    public String toString() {
    return "LCInsureAccPojo [" +
            "InsureAccID="+InsureAccID +
            ", ShardingID="+ShardingID +
            ", PolNo="+PolNo +
            ", InsuAccNo="+InsuAccNo +
            ", ContNo="+ContNo +
            ", GrpContNo="+GrpContNo +
            ", GrpPolNo="+GrpPolNo +
            ", PrtNo="+PrtNo +
            ", RiskCode="+RiskCode +
            ", AccType="+AccType +
            ", InvestType="+InvestType +
            ", FundCompanyCode="+FundCompanyCode +
            ", InsuredNo="+InsuredNo +
            ", AppntNo="+AppntNo +
            ", Owner="+Owner +
            ", AccComputeFlag="+AccComputeFlag +
            ", AccFoundDate="+AccFoundDate +
            ", AccFoundTime="+AccFoundTime +
            ", BalaDate="+BalaDate +
            ", BalaTime="+BalaTime +
            ", SumPay="+SumPay +
            ", SumPaym="+SumPaym +
            ", LastAccBala="+LastAccBala +
            ", LastUnitCount="+LastUnitCount +
            ", LastUnitPrice="+LastUnitPrice +
            ", InsuAccBala="+InsuAccBala +
            ", UnitCount="+UnitCount +
            ", UnitPrice="+UnitPrice +
            ", InsuAccGetMoney="+InsuAccGetMoney +
            ", FrozenMoney="+FrozenMoney +
            ", State="+State +
            ", ManageCom="+ManageCom +
            ", Operator="+Operator +
            ", MakeDate="+MakeDate +
            ", MakeTime="+MakeTime +
            ", ModifyDate="+ModifyDate +
            ", ModifyTime="+ModifyTime +
            ", WNPreEndDate="+WNPreEndDate +"]";
    }
}
