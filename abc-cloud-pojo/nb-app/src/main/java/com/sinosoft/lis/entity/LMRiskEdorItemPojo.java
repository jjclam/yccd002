/**
 * Copyright (c) 2017 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.entity;

import com.sinosoft.utility.Pojo;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;

import java.io.Serializable;

/**
 * <p>ClassName: LMRiskEdorItemPojo </p>
 * <p>Copyright: Copyright (c) 2017</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2017-10-17
 */
public class LMRiskEdorItemPojo implements Pojo,Serializable {
    // @Field
    /** 险种编码 */
    private String RiskCode; 
    /** 险种名称 */
    private String RiskName; 
    /** 保全项目编码 */
    private String EdorCode; 
    /** 财务处理类型 */
    private String FinType; 
    /** 保全项目属性 */
    private String EdorProperty; 


    public static final int FIELDNUM = 5;    // 数据库表的字段个数
    public String getRiskCode() {
        return RiskCode;
    }
    public void setRiskCode(String aRiskCode) {
        RiskCode = aRiskCode;
    }
    public String getRiskName() {
        return RiskName;
    }
    public void setRiskName(String aRiskName) {
        RiskName = aRiskName;
    }
    public String getEdorCode() {
        return EdorCode;
    }
    public void setEdorCode(String aEdorCode) {
        EdorCode = aEdorCode;
    }
    public String getFinType() {
        return FinType;
    }
    public void setFinType(String aFinType) {
        FinType = aFinType;
    }
    public String getEdorProperty() {
        return EdorProperty;
    }
    public void setEdorProperty(String aEdorProperty) {
        EdorProperty = aEdorProperty;
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("RiskCode") ) {
            return 0;
        }
        if( strFieldName.equals("RiskName") ) {
            return 1;
        }
        if( strFieldName.equals("EdorCode") ) {
            return 2;
        }
        if( strFieldName.equals("FinType") ) {
            return 3;
        }
        if( strFieldName.equals("EdorProperty") ) {
            return 4;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "RiskCode";
                break;
            case 1:
                strFieldName = "RiskName";
                break;
            case 2:
                strFieldName = "EdorCode";
                break;
            case 3:
                strFieldName = "FinType";
                break;
            case 4:
                strFieldName = "EdorProperty";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "RISKCODE":
                return Schema.TYPE_STRING;
            case "RISKNAME":
                return Schema.TYPE_STRING;
            case "EDORCODE":
                return Schema.TYPE_STRING;
            case "FINTYPE":
                return Schema.TYPE_STRING;
            case "EDORPROPERTY":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_STRING;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("RiskCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RiskCode));
        }
        if (FCode.equalsIgnoreCase("RiskName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RiskName));
        }
        if (FCode.equalsIgnoreCase("EdorCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(EdorCode));
        }
        if (FCode.equalsIgnoreCase("FinType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FinType));
        }
        if (FCode.equalsIgnoreCase("EdorProperty")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(EdorProperty));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = String.valueOf(RiskCode);
                break;
            case 1:
                strFieldValue = String.valueOf(RiskName);
                break;
            case 2:
                strFieldValue = String.valueOf(EdorCode);
                break;
            case 3:
                strFieldValue = String.valueOf(FinType);
                break;
            case 4:
                strFieldValue = String.valueOf(EdorProperty);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("RiskCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                RiskCode = FValue.trim();
            }
            else
                RiskCode = null;
        }
        if (FCode.equalsIgnoreCase("RiskName")) {
            if( FValue != null && !FValue.equals(""))
            {
                RiskName = FValue.trim();
            }
            else
                RiskName = null;
        }
        if (FCode.equalsIgnoreCase("EdorCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                EdorCode = FValue.trim();
            }
            else
                EdorCode = null;
        }
        if (FCode.equalsIgnoreCase("FinType")) {
            if( FValue != null && !FValue.equals(""))
            {
                FinType = FValue.trim();
            }
            else
                FinType = null;
        }
        if (FCode.equalsIgnoreCase("EdorProperty")) {
            if( FValue != null && !FValue.equals(""))
            {
                EdorProperty = FValue.trim();
            }
            else
                EdorProperty = null;
        }
        return true;
    }


    public String toString() {
    return "LMRiskEdorItemPojo [" +
            "RiskCode="+RiskCode +
            ", RiskName="+RiskName +
            ", EdorCode="+EdorCode +
            ", FinType="+FinType +
            ", EdorProperty="+EdorProperty +"]";
    }
}
