/**
 * Copyright (c) 2019 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LMRiskCalBaseDB;

/**
 * <p>ClassName: LMRiskCalBaseSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2019</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2019-01-23
 */
public class LMRiskCalBaseSchema implements Schema, Cloneable {
    // @Field
    /** 险种编码 */
    private String RiskCode;
    /** 销售类型 */
    private String SellType;
    /** 组合编码 */
    private String ProdSetCode;
    /** 保费转化基数 */
    private int PremPerUnit;
    /** 销售渠道 */
    private String SaleChnl;

    public static final int FIELDNUM = 5;    // 数据库表的字段个数

    private static String[] PK;                // 主键

    public CErrors mErrors;            // 错误信息

    // @Constructor
    public LMRiskCalBaseSchema() {
        mErrors = new CErrors();

        String[] pk = new String[0];

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException {
        LMRiskCalBaseSchema cloned = (LMRiskCalBaseSchema)super.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK() {
        return PK;
    }

    public String getRiskCode() {
        return RiskCode;
    }
    public void setRiskCode(String aRiskCode) {
        RiskCode = aRiskCode;
    }
    public String getSellType() {
        return SellType;
    }
    public void setSellType(String aSellType) {
        SellType = aSellType;
    }
    public String getProdSetCode() {
        return ProdSetCode;
    }
    public void setProdSetCode(String aProdSetCode) {
        ProdSetCode = aProdSetCode;
    }
    public int getPremPerUnit() {
        return PremPerUnit;
    }
    public void setPremPerUnit(int aPremPerUnit) {
        PremPerUnit = aPremPerUnit;
    }
    public void setPremPerUnit(String aPremPerUnit) {
        if (aPremPerUnit != null && !aPremPerUnit.equals("")) {
            Integer tInteger = new Integer(aPremPerUnit);
            int i = tInteger.intValue();
            PremPerUnit = i;
        }
    }

    public String getSaleChnl() {
        return SaleChnl;
    }
    public void setSaleChnl(String aSaleChnl) {
        SaleChnl = aSaleChnl;
    }

    /**
    * 使用另外一个 LMRiskCalBaseSchema 对象给 Schema 赋值
    * @param: aLMRiskCalBaseSchema LMRiskCalBaseSchema
    **/
    public void setSchema(LMRiskCalBaseSchema aLMRiskCalBaseSchema) {
        this.RiskCode = aLMRiskCalBaseSchema.getRiskCode();
        this.SellType = aLMRiskCalBaseSchema.getSellType();
        this.ProdSetCode = aLMRiskCalBaseSchema.getProdSetCode();
        this.PremPerUnit = aLMRiskCalBaseSchema.getPremPerUnit();
        this.SaleChnl = aLMRiskCalBaseSchema.getSaleChnl();
    }

    /**
    * 使用 ResultSet 中的第 i 行给 Schema 赋值
    * @param: rs ResultSet
    * @param: i int
    * @return: boolean
    **/
    public boolean setSchema(ResultSet rs,int i) {
        try {
            //rs.absolute(i);        // 非滚动游标
            if( rs.getString("RiskCode") == null )
                this.RiskCode = null;
            else
                this.RiskCode = rs.getString("RiskCode").trim();

            if( rs.getString("SellType") == null )
                this.SellType = null;
            else
                this.SellType = rs.getString("SellType").trim();

            if( rs.getString("ProdSetCode") == null )
                this.ProdSetCode = null;
            else
                this.ProdSetCode = rs.getString("ProdSetCode").trim();

            this.PremPerUnit = rs.getInt("PremPerUnit");
            if( rs.getString("SaleChnl") == null )
                this.SaleChnl = null;
            else
                this.SaleChnl = rs.getString("SaleChnl").trim();

        }
        catch(SQLException sqle) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LMRiskCalBaseSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = ExceptionUtils.exceptionToString(sqle);
            this.mErrors .addOneError(tError);
            return false;
        }
        return true;
    }

    public LMRiskCalBaseSchema getSchema() {
        LMRiskCalBaseSchema aLMRiskCalBaseSchema = new LMRiskCalBaseSchema();
        aLMRiskCalBaseSchema.setSchema(this);
        return aLMRiskCalBaseSchema;
    }

    public LMRiskCalBaseDB getDB() {
        LMRiskCalBaseDB aDBOper = new LMRiskCalBaseDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
    * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLMRiskCalBase描述/A>表字段
    * @return: String 返回打包后字符串
    **/
    public String encode() {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(RiskCode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(SellType)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ProdSetCode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(PremPerUnit));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(SaleChnl));
        return strReturn.toString();
    }

    /**
    * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLMRiskCalBase>历史记账凭证主表信息</A>表字段
    * @param: strMessage String 包含一条纪录数据的字符串
    * @return: boolean
    **/
    public boolean decode(String strMessage) {
        try {
            RiskCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
            SellType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
            ProdSetCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
            PremPerUnit = new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,4,SysConst.PACKAGESPILTER))).intValue();
            SaleChnl = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
        }
        catch(NumberFormatException ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LMRiskCalBaseSchema";
            tError.functionName = "decode";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            return false;
        }
        return true;
    }

    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("RiskCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RiskCode));
        }
        if (FCode.equalsIgnoreCase("SellType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SellType));
        }
        if (FCode.equalsIgnoreCase("ProdSetCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ProdSetCode));
        }
        if (FCode.equalsIgnoreCase("PremPerUnit")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PremPerUnit));
        }
        if (FCode.equalsIgnoreCase("SaleChnl")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SaleChnl));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(RiskCode);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(SellType);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(ProdSetCode);
                break;
            case 3:
                strFieldValue = String.valueOf(PremPerUnit);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(SaleChnl);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("RiskCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                RiskCode = FValue.trim();
            }
            else
                RiskCode = null;
        }
        if (FCode.equalsIgnoreCase("SellType")) {
            if( FValue != null && !FValue.equals(""))
            {
                SellType = FValue.trim();
            }
            else
                SellType = null;
        }
        if (FCode.equalsIgnoreCase("ProdSetCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                ProdSetCode = FValue.trim();
            }
            else
                ProdSetCode = null;
        }
        if (FCode.equalsIgnoreCase("PremPerUnit")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                PremPerUnit = i;
            }
        }
        if (FCode.equalsIgnoreCase("SaleChnl")) {
            if( FValue != null && !FValue.equals(""))
            {
                SaleChnl = FValue.trim();
            }
            else
                SaleChnl = null;
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject) return true;
        if (otherObject == null) return false;
        if (getClass() != otherObject.getClass()) return false;
        LMRiskCalBaseSchema other = (LMRiskCalBaseSchema)otherObject;
        return
            RiskCode.equals(other.getRiskCode())
            && SellType.equals(other.getSellType())
            && ProdSetCode.equals(other.getProdSetCode())
            && PremPerUnit == other.getPremPerUnit()
            && SaleChnl.equals(other.getSaleChnl());
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("RiskCode") ) {
            return 0;
        }
        if( strFieldName.equals("SellType") ) {
            return 1;
        }
        if( strFieldName.equals("ProdSetCode") ) {
            return 2;
        }
        if( strFieldName.equals("PremPerUnit") ) {
            return 3;
        }
        if( strFieldName.equals("SaleChnl") ) {
            return 4;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "RiskCode";
                break;
            case 1:
                strFieldName = "SellType";
                break;
            case 2:
                strFieldName = "ProdSetCode";
                break;
            case 3:
                strFieldName = "PremPerUnit";
                break;
            case 4:
                strFieldName = "SaleChnl";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "RISKCODE":
                return Schema.TYPE_STRING;
            case "SELLTYPE":
                return Schema.TYPE_STRING;
            case "PRODSETCODE":
                return Schema.TYPE_STRING;
            case "PREMPERUNIT":
                return Schema.TYPE_INT;
            case "SALECHNL":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_STRING;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_INT;
            case 4:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    public String toString() {
    return "LMRiskCalBaseSchema {" +
            "RiskCode="+RiskCode +
            ", SellType="+SellType +
            ", ProdSetCode="+ProdSetCode +
            ", PremPerUnit="+PremPerUnit +
            ", SaleChnl="+SaleChnl +"}";
    }
}
