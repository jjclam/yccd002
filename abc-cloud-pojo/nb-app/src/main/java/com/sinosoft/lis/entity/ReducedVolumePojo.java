package com.sinosoft.lis.entity;

import java.io.Serializable;
import java.util.List;

/**
 * @Author: zhuyiming
 * @Description:
 * @Date: Created in 21:56 2017/11/8
 * @Modified By
 */
public class ReducedVolumePojo implements Serializable {
    //险种编码
    private String riskCode;

    private List<String> reducedVolume;

    public ReducedVolumePojo() {
    }

    public ReducedVolumePojo(String riskCode, List<String> reducedVolume) {

        this.riskCode = riskCode;
        this.reducedVolume = reducedVolume;
    }

    public String getRiskCode() {
        return riskCode;
    }

    public void setRiskCode(String riskCode) {
        this.riskCode = riskCode;
    }

    public List<String> getReducedVolume() {
        return reducedVolume;
    }

    public void setReducedVolume(List<String> reducedVolume) {
        this.reducedVolume = reducedVolume;
    }

    @Override
    public String toString() {
        return "ReducedVolumePojo{" +
                "riskCode='" + riskCode + '\'' +
                ", reducedVolume=" + reducedVolume +
                '}';
    }
}
