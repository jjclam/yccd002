/**
 * Copyright (c) 2017 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.db;

import com.sinosoft.lis.schema.LPContSchema;
import com.sinosoft.lis.vschema.LPContSet;
import com.sinosoft.utility.*;

import java.sql.*;

/**
 * <p>ClassName: LPContDB </p>
 * <p>Description: DB层数据库操作类文件 </p>
 * <p>Copyright: Copyright (c) 2017</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2017-10-21
 */
public class LPContDB extends LPContSchema {
    // @Field
    private Connection con;
    private DBOper db;
    /**
    * flag = true: 传入Connection
    * flag = false: 不传入Connection
    **/
    private boolean mflag = false;

    public CErrors mErrors = new CErrors();        // 错误信息

    /**
     * 为批量操作而准备的语句和游标对象
     */
    private ResultSet mResultSet = null;
    private Statement mStatement = null;
    // @Constructor
    public LPContDB( Connection tConnection ) {
        con = tConnection;
        db = new DBOper( con, "LPCont" );
        mflag = true;
    }

    public LPContDB() {
        con = null;
        db = new DBOper( "LPCont" );
        mflag = false;
    }

    // @Method
    public boolean deleteSQL() {
        LPContSchema tSchema = this.getSchema();
        if (db.deleteSQL(tSchema)) {
             return true;
        } else {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LPContDB";
            tError.functionName = "deleteSQL";
            tError.errorMessage = "操作失败!";
            this.mErrors .addOneError(tError);
            return false;
        }
    }

    public int getCount() {
        LPContSchema tSchema = this.getSchema();

        int tCount = db.getCount(tSchema);
        if (tCount < 0) {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LPContDB";
            tError.functionName = "getCount";
            tError.errorMessage = "操作失败!";
            this.mErrors .addOneError(tError);

            return -1;
        }

        return tCount;
    }

    public boolean delete() {
        PreparedStatement pstmt = null;

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try {
            pstmt = con.prepareStatement("DELETE FROM LPCont WHERE  1=1  AND EdorNo = ? AND EdorType = ? AND ContNo = ?");
            if(this.getEdorNo() == null || this.getEdorNo().equals("null")) {
            	pstmt.setNull(1, 12);
            } else {
            	pstmt.setString(1, this.getEdorNo());
            }
            if(this.getEdorType() == null || this.getEdorType().equals("null")) {
            	pstmt.setNull(2, 12);
            } else {
            	pstmt.setString(2, this.getEdorType());
            }
            if(this.getContNo() == null || this.getContNo().equals("null")) {
            	pstmt.setNull(3, 12);
            } else {
            	pstmt.setString(3, this.getContNo());
            }
            pstmt.executeUpdate();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LPContDB";
            tError.functionName = "delete()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);
            ex.printStackTrace();

            try {
                pstmt.close();
            } catch (Exception e){}

            if(!mflag) {
                try {
                    con.close();
                } catch (Exception e){}
            }

            return false;
        }

        if(!mflag) {
            try {
                con.close();
            } catch (Exception e){}
        }

        return true;
    }

    public boolean update() {
        PreparedStatement pstmt = null;

        SQLString sqlObj = new SQLString("LPCont");
        sqlObj.setSQL(2, this);
        String sql = sqlObj.getSQL();

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try {
            pstmt = con.prepareStatement("UPDATE LPCont SET  EdorNo = ? , EdorType = ? , GrpContNo = ? , ContNo = ? , ProposalContNo = ? , PrtNo = ? , ContType = ? , FamilyType = ? , FamilyID = ? , PolType = ? , CardFlag = ? , ManageCom = ? , ExecuteCom = ? , AgentCom = ? , AgentCode = ? , AgentGroup = ? , AgentCode1 = ? , AgentType = ? , SaleChnl = ? , Handler = ? , Password = ? , AppntNo = ? , AppntName = ? , AppntSex = ? , AppntBirthday = ? , AppntIDType = ? , AppntIDNo = ? , InsuredNo = ? , InsuredName = ? , InsuredSex = ? , InsuredBirthday = ? , InsuredIDType = ? , InsuredIDNo = ? , PayIntv = ? , PayMode = ? , PayLocation = ? , DisputedFlag = ? , OutPayFlag = ? , GetPolMode = ? , SignCom = ? , SignDate = ? , SignTime = ? , ConsignNo = ? , BankCode = ? , BankAccNo = ? , AccName = ? , PrintCount = ? , LostTimes = ? , Lang = ? , Currency = ? , Remark = ? , Peoples = ? , Mult = ? , Prem = ? , Amnt = ? , SumPrem = ? , Dif = ? , PaytoDate = ? , FirstPayDate = ? , CValiDate = ? , InputOperator = ? , InputDate = ? , InputTime = ? , ApproveFlag = ? , ApproveCode = ? , ApproveDate = ? , ApproveTime = ? , UWFlag = ? , UWOperator = ? , UWDate = ? , UWTime = ? , AppFlag = ? , PolApplyDate = ? , GetPolDate = ? , GetPolTime = ? , CustomGetPolDate = ? , State = ? , Operator = ? , MakeDate = ? , MakeTime = ? , ModifyDate = ? , ModifyTime = ? , FirstTrialOperator = ? , FirstTrialDate = ? , FirstTrialTime = ? , ReceiveOperator = ? , ReceiveDate = ? , ReceiveTime = ? , TempFeeNo = ? , SellType = ? , ForceUWFlag = ? , ForceUWReason = ? , NewBankCode = ? , NewBankAccNo = ? , NewAccName = ? , NewPayMode = ? , AgentBankCode = ? , BankAgent = ? , BankAgentName = ? , BankAgentTel = ? , ProdSetCode = ? , PolicyNo = ? , BillPressNo = ? , CardTypeCode = ? , VisitDate = ? , VisitTime = ? , SaleCom = ? , PrintFlag = ? , InvoicePrtFlag = ? , NewReinsureFlag = ? , RenewPayFlag = ? , AppntFirstName = ? , AppntLastName = ? , InsuredFirstName = ? , InsuredLastName = ? , AuthorFlag = ? , GreenChnl = ? , TBType = ? , EAuto = ? , SlipForm = ? WHERE  1=1  AND EdorNo = ? AND EdorType = ? AND ContNo = ?");
            if(this.getEdorNo() == null || this.getEdorNo().equals("null")) {
            	pstmt.setNull(1, 12);
            } else {
            	pstmt.setString(1, this.getEdorNo());
            }
            if(this.getEdorType() == null || this.getEdorType().equals("null")) {
            	pstmt.setNull(2, 12);
            } else {
            	pstmt.setString(2, this.getEdorType());
            }
            if(this.getGrpContNo() == null || this.getGrpContNo().equals("null")) {
            	pstmt.setNull(3, 12);
            } else {
            	pstmt.setString(3, this.getGrpContNo());
            }
            if(this.getContNo() == null || this.getContNo().equals("null")) {
            	pstmt.setNull(4, 12);
            } else {
            	pstmt.setString(4, this.getContNo());
            }
            if(this.getProposalContNo() == null || this.getProposalContNo().equals("null")) {
            	pstmt.setNull(5, 12);
            } else {
            	pstmt.setString(5, this.getProposalContNo());
            }
            if(this.getPrtNo() == null || this.getPrtNo().equals("null")) {
            	pstmt.setNull(6, 12);
            } else {
            	pstmt.setString(6, this.getPrtNo());
            }
            if(this.getContType() == null || this.getContType().equals("null")) {
            	pstmt.setNull(7, 12);
            } else {
            	pstmt.setString(7, this.getContType());
            }
            if(this.getFamilyType() == null || this.getFamilyType().equals("null")) {
            	pstmt.setNull(8, 12);
            } else {
            	pstmt.setString(8, this.getFamilyType());
            }
            if(this.getFamilyID() == null || this.getFamilyID().equals("null")) {
            	pstmt.setNull(9, 12);
            } else {
            	pstmt.setString(9, this.getFamilyID());
            }
            if(this.getPolType() == null || this.getPolType().equals("null")) {
            	pstmt.setNull(10, 12);
            } else {
            	pstmt.setString(10, this.getPolType());
            }
            if(this.getCardFlag() == null || this.getCardFlag().equals("null")) {
            	pstmt.setNull(11, 12);
            } else {
            	pstmt.setString(11, this.getCardFlag());
            }
            if(this.getManageCom() == null || this.getManageCom().equals("null")) {
            	pstmt.setNull(12, 12);
            } else {
            	pstmt.setString(12, this.getManageCom());
            }
            if(this.getExecuteCom() == null || this.getExecuteCom().equals("null")) {
            	pstmt.setNull(13, 12);
            } else {
            	pstmt.setString(13, this.getExecuteCom());
            }
            if(this.getAgentCom() == null || this.getAgentCom().equals("null")) {
            	pstmt.setNull(14, 12);
            } else {
            	pstmt.setString(14, this.getAgentCom());
            }
            if(this.getAgentCode() == null || this.getAgentCode().equals("null")) {
            	pstmt.setNull(15, 12);
            } else {
            	pstmt.setString(15, this.getAgentCode());
            }
            if(this.getAgentGroup() == null || this.getAgentGroup().equals("null")) {
            	pstmt.setNull(16, 12);
            } else {
            	pstmt.setString(16, this.getAgentGroup());
            }
            if(this.getAgentCode1() == null || this.getAgentCode1().equals("null")) {
            	pstmt.setNull(17, 12);
            } else {
            	pstmt.setString(17, this.getAgentCode1());
            }
            if(this.getAgentType() == null || this.getAgentType().equals("null")) {
            	pstmt.setNull(18, 12);
            } else {
            	pstmt.setString(18, this.getAgentType());
            }
            if(this.getSaleChnl() == null || this.getSaleChnl().equals("null")) {
            	pstmt.setNull(19, 12);
            } else {
            	pstmt.setString(19, this.getSaleChnl());
            }
            if(this.getHandler() == null || this.getHandler().equals("null")) {
            	pstmt.setNull(20, 12);
            } else {
            	pstmt.setString(20, this.getHandler());
            }
            if(this.getPassword() == null || this.getPassword().equals("null")) {
            	pstmt.setNull(21, 12);
            } else {
            	pstmt.setString(21, this.getPassword());
            }
            if(this.getAppntNo() == null || this.getAppntNo().equals("null")) {
            	pstmt.setNull(22, 12);
            } else {
            	pstmt.setString(22, this.getAppntNo());
            }
            if(this.getAppntName() == null || this.getAppntName().equals("null")) {
            	pstmt.setNull(23, 12);
            } else {
            	pstmt.setString(23, this.getAppntName());
            }
            if(this.getAppntSex() == null || this.getAppntSex().equals("null")) {
            	pstmt.setNull(24, 12);
            } else {
            	pstmt.setString(24, this.getAppntSex());
            }
            if(this.getAppntBirthday() == null || this.getAppntBirthday().equals("null")) {
            	pstmt.setNull(25, 93);
            } else {
            	pstmt.setDate(25, Date.valueOf(this.getAppntBirthday()));
            }
            if(this.getAppntIDType() == null || this.getAppntIDType().equals("null")) {
            	pstmt.setNull(26, 12);
            } else {
            	pstmt.setString(26, this.getAppntIDType());
            }
            if(this.getAppntIDNo() == null || this.getAppntIDNo().equals("null")) {
            	pstmt.setNull(27, 12);
            } else {
            	pstmt.setString(27, this.getAppntIDNo());
            }
            if(this.getInsuredNo() == null || this.getInsuredNo().equals("null")) {
            	pstmt.setNull(28, 12);
            } else {
            	pstmt.setString(28, this.getInsuredNo());
            }
            if(this.getInsuredName() == null || this.getInsuredName().equals("null")) {
            	pstmt.setNull(29, 12);
            } else {
            	pstmt.setString(29, this.getInsuredName());
            }
            if(this.getInsuredSex() == null || this.getInsuredSex().equals("null")) {
            	pstmt.setNull(30, 12);
            } else {
            	pstmt.setString(30, this.getInsuredSex());
            }
            if(this.getInsuredBirthday() == null || this.getInsuredBirthday().equals("null")) {
            	pstmt.setNull(31, 93);
            } else {
            	pstmt.setDate(31, Date.valueOf(this.getInsuredBirthday()));
            }
            if(this.getInsuredIDType() == null || this.getInsuredIDType().equals("null")) {
            	pstmt.setNull(32, 12);
            } else {
            	pstmt.setString(32, this.getInsuredIDType());
            }
            if(this.getInsuredIDNo() == null || this.getInsuredIDNo().equals("null")) {
            	pstmt.setNull(33, 12);
            } else {
            	pstmt.setString(33, this.getInsuredIDNo());
            }
            pstmt.setInt(34, this.getPayIntv());
            if(this.getPayMode() == null || this.getPayMode().equals("null")) {
            	pstmt.setNull(35, 12);
            } else {
            	pstmt.setString(35, this.getPayMode());
            }
            if(this.getPayLocation() == null || this.getPayLocation().equals("null")) {
            	pstmt.setNull(36, 12);
            } else {
            	pstmt.setString(36, this.getPayLocation());
            }
            if(this.getDisputedFlag() == null || this.getDisputedFlag().equals("null")) {
            	pstmt.setNull(37, 12);
            } else {
            	pstmt.setString(37, this.getDisputedFlag());
            }
            if(this.getOutPayFlag() == null || this.getOutPayFlag().equals("null")) {
            	pstmt.setNull(38, 12);
            } else {
            	pstmt.setString(38, this.getOutPayFlag());
            }
            if(this.getGetPolMode() == null || this.getGetPolMode().equals("null")) {
            	pstmt.setNull(39, 12);
            } else {
            	pstmt.setString(39, this.getGetPolMode());
            }
            if(this.getSignCom() == null || this.getSignCom().equals("null")) {
            	pstmt.setNull(40, 12);
            } else {
            	pstmt.setString(40, this.getSignCom());
            }
            if(this.getSignDate() == null || this.getSignDate().equals("null")) {
            	pstmt.setNull(41, 93);
            } else {
            	pstmt.setDate(41, Date.valueOf(this.getSignDate()));
            }
            if(this.getSignTime() == null || this.getSignTime().equals("null")) {
            	pstmt.setNull(42, 12);
            } else {
            	pstmt.setString(42, this.getSignTime());
            }
            if(this.getConsignNo() == null || this.getConsignNo().equals("null")) {
            	pstmt.setNull(43, 12);
            } else {
            	pstmt.setString(43, this.getConsignNo());
            }
            if(this.getBankCode() == null || this.getBankCode().equals("null")) {
            	pstmt.setNull(44, 12);
            } else {
            	pstmt.setString(44, this.getBankCode());
            }
            if(this.getBankAccNo() == null || this.getBankAccNo().equals("null")) {
            	pstmt.setNull(45, 12);
            } else {
            	pstmt.setString(45, this.getBankAccNo());
            }
            if(this.getAccName() == null || this.getAccName().equals("null")) {
            	pstmt.setNull(46, 12);
            } else {
            	pstmt.setString(46, this.getAccName());
            }
            pstmt.setInt(47, this.getPrintCount());
            pstmt.setInt(48, this.getLostTimes());
            if(this.getLang() == null || this.getLang().equals("null")) {
            	pstmt.setNull(49, 12);
            } else {
            	pstmt.setString(49, this.getLang());
            }
            if(this.getCurrency() == null || this.getCurrency().equals("null")) {
            	pstmt.setNull(50, 12);
            } else {
            	pstmt.setString(50, this.getCurrency());
            }
            if(this.getRemark() == null || this.getRemark().equals("null")) {
            	pstmt.setNull(51, 12);
            } else {
            	pstmt.setString(51, this.getRemark());
            }
            pstmt.setInt(52, this.getPeoples());
            pstmt.setDouble(53, this.getMult());
            pstmt.setDouble(54, this.getPrem());
            pstmt.setDouble(55, this.getAmnt());
            pstmt.setDouble(56, this.getSumPrem());
            pstmt.setDouble(57, this.getDif());
            if(this.getPaytoDate() == null || this.getPaytoDate().equals("null")) {
            	pstmt.setNull(58, 93);
            } else {
            	pstmt.setDate(58, Date.valueOf(this.getPaytoDate()));
            }
            if(this.getFirstPayDate() == null || this.getFirstPayDate().equals("null")) {
            	pstmt.setNull(59, 93);
            } else {
            	pstmt.setDate(59, Date.valueOf(this.getFirstPayDate()));
            }
            if(this.getCValiDate() == null || this.getCValiDate().equals("null")) {
            	pstmt.setNull(60, 93);
            } else {
            	pstmt.setDate(60, Date.valueOf(this.getCValiDate()));
            }
            if(this.getInputOperator() == null || this.getInputOperator().equals("null")) {
            	pstmt.setNull(61, 12);
            } else {
            	pstmt.setString(61, this.getInputOperator());
            }
            if(this.getInputDate() == null || this.getInputDate().equals("null")) {
            	pstmt.setNull(62, 93);
            } else {
            	pstmt.setDate(62, Date.valueOf(this.getInputDate()));
            }
            if(this.getInputTime() == null || this.getInputTime().equals("null")) {
            	pstmt.setNull(63, 12);
            } else {
            	pstmt.setString(63, this.getInputTime());
            }
            if(this.getApproveFlag() == null || this.getApproveFlag().equals("null")) {
            	pstmt.setNull(64, 12);
            } else {
            	pstmt.setString(64, this.getApproveFlag());
            }
            if(this.getApproveCode() == null || this.getApproveCode().equals("null")) {
            	pstmt.setNull(65, 12);
            } else {
            	pstmt.setString(65, this.getApproveCode());
            }
            if(this.getApproveDate() == null || this.getApproveDate().equals("null")) {
            	pstmt.setNull(66, 93);
            } else {
            	pstmt.setDate(66, Date.valueOf(this.getApproveDate()));
            }
            if(this.getApproveTime() == null || this.getApproveTime().equals("null")) {
            	pstmt.setNull(67, 12);
            } else {
            	pstmt.setString(67, this.getApproveTime());
            }
            if(this.getUWFlag() == null || this.getUWFlag().equals("null")) {
            	pstmt.setNull(68, 12);
            } else {
            	pstmt.setString(68, this.getUWFlag());
            }
            if(this.getUWOperator() == null || this.getUWOperator().equals("null")) {
            	pstmt.setNull(69, 12);
            } else {
            	pstmt.setString(69, this.getUWOperator());
            }
            if(this.getUWDate() == null || this.getUWDate().equals("null")) {
            	pstmt.setNull(70, 93);
            } else {
            	pstmt.setDate(70, Date.valueOf(this.getUWDate()));
            }
            if(this.getUWTime() == null || this.getUWTime().equals("null")) {
            	pstmt.setNull(71, 12);
            } else {
            	pstmt.setString(71, this.getUWTime());
            }
            if(this.getAppFlag() == null || this.getAppFlag().equals("null")) {
            	pstmt.setNull(72, 12);
            } else {
            	pstmt.setString(72, this.getAppFlag());
            }
            if(this.getPolApplyDate() == null || this.getPolApplyDate().equals("null")) {
            	pstmt.setNull(73, 93);
            } else {
            	pstmt.setDate(73, Date.valueOf(this.getPolApplyDate()));
            }
            if(this.getGetPolDate() == null || this.getGetPolDate().equals("null")) {
            	pstmt.setNull(74, 93);
            } else {
            	pstmt.setDate(74, Date.valueOf(this.getGetPolDate()));
            }
            if(this.getGetPolTime() == null || this.getGetPolTime().equals("null")) {
            	pstmt.setNull(75, 12);
            } else {
            	pstmt.setString(75, this.getGetPolTime());
            }
            if(this.getCustomGetPolDate() == null || this.getCustomGetPolDate().equals("null")) {
            	pstmt.setNull(76, 93);
            } else {
            	pstmt.setDate(76, Date.valueOf(this.getCustomGetPolDate()));
            }
            if(this.getState() == null || this.getState().equals("null")) {
            	pstmt.setNull(77, 12);
            } else {
            	pstmt.setString(77, this.getState());
            }
            if(this.getOperator() == null || this.getOperator().equals("null")) {
            	pstmt.setNull(78, 12);
            } else {
            	pstmt.setString(78, this.getOperator());
            }
            if(this.getMakeDate() == null || this.getMakeDate().equals("null")) {
            	pstmt.setNull(79, 93);
            } else {
            	pstmt.setDate(79, Date.valueOf(this.getMakeDate()));
            }
            if(this.getMakeTime() == null || this.getMakeTime().equals("null")) {
            	pstmt.setNull(80, 12);
            } else {
            	pstmt.setString(80, this.getMakeTime());
            }
            if(this.getModifyDate() == null || this.getModifyDate().equals("null")) {
            	pstmt.setNull(81, 93);
            } else {
            	pstmt.setDate(81, Date.valueOf(this.getModifyDate()));
            }
            if(this.getModifyTime() == null || this.getModifyTime().equals("null")) {
            	pstmt.setNull(82, 12);
            } else {
            	pstmt.setString(82, this.getModifyTime());
            }
            if(this.getFirstTrialOperator() == null || this.getFirstTrialOperator().equals("null")) {
            	pstmt.setNull(83, 12);
            } else {
            	pstmt.setString(83, this.getFirstTrialOperator());
            }
            if(this.getFirstTrialDate() == null || this.getFirstTrialDate().equals("null")) {
            	pstmt.setNull(84, 93);
            } else {
            	pstmt.setDate(84, Date.valueOf(this.getFirstTrialDate()));
            }
            if(this.getFirstTrialTime() == null || this.getFirstTrialTime().equals("null")) {
            	pstmt.setNull(85, 12);
            } else {
            	pstmt.setString(85, this.getFirstTrialTime());
            }
            if(this.getReceiveOperator() == null || this.getReceiveOperator().equals("null")) {
            	pstmt.setNull(86, 12);
            } else {
            	pstmt.setString(86, this.getReceiveOperator());
            }
            if(this.getReceiveDate() == null || this.getReceiveDate().equals("null")) {
            	pstmt.setNull(87, 93);
            } else {
            	pstmt.setDate(87, Date.valueOf(this.getReceiveDate()));
            }
            if(this.getReceiveTime() == null || this.getReceiveTime().equals("null")) {
            	pstmt.setNull(88, 12);
            } else {
            	pstmt.setString(88, this.getReceiveTime());
            }
            if(this.getTempFeeNo() == null || this.getTempFeeNo().equals("null")) {
            	pstmt.setNull(89, 12);
            } else {
            	pstmt.setString(89, this.getTempFeeNo());
            }
            if(this.getSellType() == null || this.getSellType().equals("null")) {
            	pstmt.setNull(90, 12);
            } else {
            	pstmt.setString(90, this.getSellType());
            }
            if(this.getForceUWFlag() == null || this.getForceUWFlag().equals("null")) {
            	pstmt.setNull(91, 12);
            } else {
            	pstmt.setString(91, this.getForceUWFlag());
            }
            if(this.getForceUWReason() == null || this.getForceUWReason().equals("null")) {
            	pstmt.setNull(92, 12);
            } else {
            	pstmt.setString(92, this.getForceUWReason());
            }
            if(this.getNewBankCode() == null || this.getNewBankCode().equals("null")) {
            	pstmt.setNull(93, 12);
            } else {
            	pstmt.setString(93, this.getNewBankCode());
            }
            if(this.getNewBankAccNo() == null || this.getNewBankAccNo().equals("null")) {
            	pstmt.setNull(94, 12);
            } else {
            	pstmt.setString(94, this.getNewBankAccNo());
            }
            if(this.getNewAccName() == null || this.getNewAccName().equals("null")) {
            	pstmt.setNull(95, 12);
            } else {
            	pstmt.setString(95, this.getNewAccName());
            }
            if(this.getNewPayMode() == null || this.getNewPayMode().equals("null")) {
            	pstmt.setNull(96, 12);
            } else {
            	pstmt.setString(96, this.getNewPayMode());
            }
            if(this.getAgentBankCode() == null || this.getAgentBankCode().equals("null")) {
            	pstmt.setNull(97, 12);
            } else {
            	pstmt.setString(97, this.getAgentBankCode());
            }
            if(this.getBankAgent() == null || this.getBankAgent().equals("null")) {
            	pstmt.setNull(98, 12);
            } else {
            	pstmt.setString(98, this.getBankAgent());
            }
            if(this.getBankAgentName() == null || this.getBankAgentName().equals("null")) {
            	pstmt.setNull(99, 12);
            } else {
            	pstmt.setString(99, this.getBankAgentName());
            }
            if(this.getBankAgentTel() == null || this.getBankAgentTel().equals("null")) {
            	pstmt.setNull(100, 12);
            } else {
            	pstmt.setString(100, this.getBankAgentTel());
            }
            if(this.getProdSetCode() == null || this.getProdSetCode().equals("null")) {
            	pstmt.setNull(101, 12);
            } else {
            	pstmt.setString(101, this.getProdSetCode());
            }
            if(this.getPolicyNo() == null || this.getPolicyNo().equals("null")) {
            	pstmt.setNull(102, 12);
            } else {
            	pstmt.setString(102, this.getPolicyNo());
            }
            if(this.getBillPressNo() == null || this.getBillPressNo().equals("null")) {
            	pstmt.setNull(103, 12);
            } else {
            	pstmt.setString(103, this.getBillPressNo());
            }
            if(this.getCardTypeCode() == null || this.getCardTypeCode().equals("null")) {
            	pstmt.setNull(104, 12);
            } else {
            	pstmt.setString(104, this.getCardTypeCode());
            }
            if(this.getVisitDate() == null || this.getVisitDate().equals("null")) {
            	pstmt.setNull(105, 93);
            } else {
            	pstmt.setDate(105, Date.valueOf(this.getVisitDate()));
            }
            if(this.getVisitTime() == null || this.getVisitTime().equals("null")) {
            	pstmt.setNull(106, 12);
            } else {
            	pstmt.setString(106, this.getVisitTime());
            }
            if(this.getSaleCom() == null || this.getSaleCom().equals("null")) {
            	pstmt.setNull(107, 12);
            } else {
            	pstmt.setString(107, this.getSaleCom());
            }
            if(this.getPrintFlag() == null || this.getPrintFlag().equals("null")) {
            	pstmt.setNull(108, 12);
            } else {
            	pstmt.setString(108, this.getPrintFlag());
            }
            if(this.getInvoicePrtFlag() == null || this.getInvoicePrtFlag().equals("null")) {
            	pstmt.setNull(109, 12);
            } else {
            	pstmt.setString(109, this.getInvoicePrtFlag());
            }
            if(this.getNewReinsureFlag() == null || this.getNewReinsureFlag().equals("null")) {
            	pstmt.setNull(110, 12);
            } else {
            	pstmt.setString(110, this.getNewReinsureFlag());
            }
            if(this.getRenewPayFlag() == null || this.getRenewPayFlag().equals("null")) {
            	pstmt.setNull(111, 12);
            } else {
            	pstmt.setString(111, this.getRenewPayFlag());
            }
            if(this.getAppntFirstName() == null || this.getAppntFirstName().equals("null")) {
            	pstmt.setNull(112, 12);
            } else {
            	pstmt.setString(112, this.getAppntFirstName());
            }
            if(this.getAppntLastName() == null || this.getAppntLastName().equals("null")) {
            	pstmt.setNull(113, 12);
            } else {
            	pstmt.setString(113, this.getAppntLastName());
            }
            if(this.getInsuredFirstName() == null || this.getInsuredFirstName().equals("null")) {
            	pstmt.setNull(114, 12);
            } else {
            	pstmt.setString(114, this.getInsuredFirstName());
            }
            if(this.getInsuredLastName() == null || this.getInsuredLastName().equals("null")) {
            	pstmt.setNull(115, 12);
            } else {
            	pstmt.setString(115, this.getInsuredLastName());
            }
            if(this.getAuthorFlag() == null || this.getAuthorFlag().equals("null")) {
            	pstmt.setNull(116, 12);
            } else {
            	pstmt.setString(116, this.getAuthorFlag());
            }
            pstmt.setInt(117, this.getGreenChnl());
            if(this.getTBType() == null || this.getTBType().equals("null")) {
            	pstmt.setNull(118, 12);
            } else {
            	pstmt.setString(118, this.getTBType());
            }
            if(this.getEAuto() == null || this.getEAuto().equals("null")) {
            	pstmt.setNull(119, 12);
            } else {
            	pstmt.setString(119, this.getEAuto());
            }
            if(this.getSlipForm() == null || this.getSlipForm().equals("null")) {
            	pstmt.setNull(120, 12);
            } else {
            	pstmt.setString(120, this.getSlipForm());
            }
            // set where condition
            if(this.getEdorNo() == null || this.getEdorNo().equals("null")) {
            	pstmt.setNull(121, 12);
            } else {
            	pstmt.setString(121, this.getEdorNo());
            }
            if(this.getEdorType() == null || this.getEdorType().equals("null")) {
            	pstmt.setNull(122, 12);
            } else {
            	pstmt.setString(122, this.getEdorType());
            }
            if(this.getContNo() == null || this.getContNo().equals("null")) {
            	pstmt.setNull(123, 12);
            } else {
            	pstmt.setString(123, this.getContNo());
            }
            pstmt.executeUpdate();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LPContDB";
            tError.functionName = "update()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);
            ex.printStackTrace();

            try {
                pstmt.close();
            } catch (Exception e){}

            if( !mflag ) {
                try {
                    con.close();
                } catch (Exception e){}
            }

            return false;
        }

        if(!mflag) {
            try {
                con.close();
            } catch (Exception e){}
        }

        return true;
    }

    public boolean insert() {
        PreparedStatement pstmt = null;

        SQLString sqlObj = new SQLString("LPCont");
        sqlObj.setSQL(1, this);
        String sql = sqlObj.getSQL();

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try {
            pstmt = con.prepareStatement("INSERT INTO LPCont VALUES( ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ?)");
            if(this.getEdorNo() == null || this.getEdorNo().equals("null")) {
            	pstmt.setNull(1, 12);
            } else {
            	pstmt.setString(1, this.getEdorNo());
            }
            if(this.getEdorType() == null || this.getEdorType().equals("null")) {
            	pstmt.setNull(2, 12);
            } else {
            	pstmt.setString(2, this.getEdorType());
            }
            if(this.getGrpContNo() == null || this.getGrpContNo().equals("null")) {
            	pstmt.setNull(3, 12);
            } else {
            	pstmt.setString(3, this.getGrpContNo());
            }
            if(this.getContNo() == null || this.getContNo().equals("null")) {
            	pstmt.setNull(4, 12);
            } else {
            	pstmt.setString(4, this.getContNo());
            }
            if(this.getProposalContNo() == null || this.getProposalContNo().equals("null")) {
            	pstmt.setNull(5, 12);
            } else {
            	pstmt.setString(5, this.getProposalContNo());
            }
            if(this.getPrtNo() == null || this.getPrtNo().equals("null")) {
            	pstmt.setNull(6, 12);
            } else {
            	pstmt.setString(6, this.getPrtNo());
            }
            if(this.getContType() == null || this.getContType().equals("null")) {
            	pstmt.setNull(7, 12);
            } else {
            	pstmt.setString(7, this.getContType());
            }
            if(this.getFamilyType() == null || this.getFamilyType().equals("null")) {
            	pstmt.setNull(8, 12);
            } else {
            	pstmt.setString(8, this.getFamilyType());
            }
            if(this.getFamilyID() == null || this.getFamilyID().equals("null")) {
            	pstmt.setNull(9, 12);
            } else {
            	pstmt.setString(9, this.getFamilyID());
            }
            if(this.getPolType() == null || this.getPolType().equals("null")) {
            	pstmt.setNull(10, 12);
            } else {
            	pstmt.setString(10, this.getPolType());
            }
            if(this.getCardFlag() == null || this.getCardFlag().equals("null")) {
            	pstmt.setNull(11, 12);
            } else {
            	pstmt.setString(11, this.getCardFlag());
            }
            if(this.getManageCom() == null || this.getManageCom().equals("null")) {
            	pstmt.setNull(12, 12);
            } else {
            	pstmt.setString(12, this.getManageCom());
            }
            if(this.getExecuteCom() == null || this.getExecuteCom().equals("null")) {
            	pstmt.setNull(13, 12);
            } else {
            	pstmt.setString(13, this.getExecuteCom());
            }
            if(this.getAgentCom() == null || this.getAgentCom().equals("null")) {
            	pstmt.setNull(14, 12);
            } else {
            	pstmt.setString(14, this.getAgentCom());
            }
            if(this.getAgentCode() == null || this.getAgentCode().equals("null")) {
            	pstmt.setNull(15, 12);
            } else {
            	pstmt.setString(15, this.getAgentCode());
            }
            if(this.getAgentGroup() == null || this.getAgentGroup().equals("null")) {
            	pstmt.setNull(16, 12);
            } else {
            	pstmt.setString(16, this.getAgentGroup());
            }
            if(this.getAgentCode1() == null || this.getAgentCode1().equals("null")) {
            	pstmt.setNull(17, 12);
            } else {
            	pstmt.setString(17, this.getAgentCode1());
            }
            if(this.getAgentType() == null || this.getAgentType().equals("null")) {
            	pstmt.setNull(18, 12);
            } else {
            	pstmt.setString(18, this.getAgentType());
            }
            if(this.getSaleChnl() == null || this.getSaleChnl().equals("null")) {
            	pstmt.setNull(19, 12);
            } else {
            	pstmt.setString(19, this.getSaleChnl());
            }
            if(this.getHandler() == null || this.getHandler().equals("null")) {
            	pstmt.setNull(20, 12);
            } else {
            	pstmt.setString(20, this.getHandler());
            }
            if(this.getPassword() == null || this.getPassword().equals("null")) {
            	pstmt.setNull(21, 12);
            } else {
            	pstmt.setString(21, this.getPassword());
            }
            if(this.getAppntNo() == null || this.getAppntNo().equals("null")) {
            	pstmt.setNull(22, 12);
            } else {
            	pstmt.setString(22, this.getAppntNo());
            }
            if(this.getAppntName() == null || this.getAppntName().equals("null")) {
            	pstmt.setNull(23, 12);
            } else {
            	pstmt.setString(23, this.getAppntName());
            }
            if(this.getAppntSex() == null || this.getAppntSex().equals("null")) {
            	pstmt.setNull(24, 12);
            } else {
            	pstmt.setString(24, this.getAppntSex());
            }
            if(this.getAppntBirthday() == null || this.getAppntBirthday().equals("null")) {
            	pstmt.setNull(25, 93);
            } else {
            	pstmt.setDate(25, Date.valueOf(this.getAppntBirthday()));
            }
            if(this.getAppntIDType() == null || this.getAppntIDType().equals("null")) {
            	pstmt.setNull(26, 12);
            } else {
            	pstmt.setString(26, this.getAppntIDType());
            }
            if(this.getAppntIDNo() == null || this.getAppntIDNo().equals("null")) {
            	pstmt.setNull(27, 12);
            } else {
            	pstmt.setString(27, this.getAppntIDNo());
            }
            if(this.getInsuredNo() == null || this.getInsuredNo().equals("null")) {
            	pstmt.setNull(28, 12);
            } else {
            	pstmt.setString(28, this.getInsuredNo());
            }
            if(this.getInsuredName() == null || this.getInsuredName().equals("null")) {
            	pstmt.setNull(29, 12);
            } else {
            	pstmt.setString(29, this.getInsuredName());
            }
            if(this.getInsuredSex() == null || this.getInsuredSex().equals("null")) {
            	pstmt.setNull(30, 12);
            } else {
            	pstmt.setString(30, this.getInsuredSex());
            }
            if(this.getInsuredBirthday() == null || this.getInsuredBirthday().equals("null")) {
            	pstmt.setNull(31, 93);
            } else {
            	pstmt.setDate(31, Date.valueOf(this.getInsuredBirthday()));
            }
            if(this.getInsuredIDType() == null || this.getInsuredIDType().equals("null")) {
            	pstmt.setNull(32, 12);
            } else {
            	pstmt.setString(32, this.getInsuredIDType());
            }
            if(this.getInsuredIDNo() == null || this.getInsuredIDNo().equals("null")) {
            	pstmt.setNull(33, 12);
            } else {
            	pstmt.setString(33, this.getInsuredIDNo());
            }
            pstmt.setInt(34, this.getPayIntv());
            if(this.getPayMode() == null || this.getPayMode().equals("null")) {
            	pstmt.setNull(35, 12);
            } else {
            	pstmt.setString(35, this.getPayMode());
            }
            if(this.getPayLocation() == null || this.getPayLocation().equals("null")) {
            	pstmt.setNull(36, 12);
            } else {
            	pstmt.setString(36, this.getPayLocation());
            }
            if(this.getDisputedFlag() == null || this.getDisputedFlag().equals("null")) {
            	pstmt.setNull(37, 12);
            } else {
            	pstmt.setString(37, this.getDisputedFlag());
            }
            if(this.getOutPayFlag() == null || this.getOutPayFlag().equals("null")) {
            	pstmt.setNull(38, 12);
            } else {
            	pstmt.setString(38, this.getOutPayFlag());
            }
            if(this.getGetPolMode() == null || this.getGetPolMode().equals("null")) {
            	pstmt.setNull(39, 12);
            } else {
            	pstmt.setString(39, this.getGetPolMode());
            }
            if(this.getSignCom() == null || this.getSignCom().equals("null")) {
            	pstmt.setNull(40, 12);
            } else {
            	pstmt.setString(40, this.getSignCom());
            }
            if(this.getSignDate() == null || this.getSignDate().equals("null")) {
            	pstmt.setNull(41, 93);
            } else {
            	pstmt.setDate(41, Date.valueOf(this.getSignDate()));
            }
            if(this.getSignTime() == null || this.getSignTime().equals("null")) {
            	pstmt.setNull(42, 12);
            } else {
            	pstmt.setString(42, this.getSignTime());
            }
            if(this.getConsignNo() == null || this.getConsignNo().equals("null")) {
            	pstmt.setNull(43, 12);
            } else {
            	pstmt.setString(43, this.getConsignNo());
            }
            if(this.getBankCode() == null || this.getBankCode().equals("null")) {
            	pstmt.setNull(44, 12);
            } else {
            	pstmt.setString(44, this.getBankCode());
            }
            if(this.getBankAccNo() == null || this.getBankAccNo().equals("null")) {
            	pstmt.setNull(45, 12);
            } else {
            	pstmt.setString(45, this.getBankAccNo());
            }
            if(this.getAccName() == null || this.getAccName().equals("null")) {
            	pstmt.setNull(46, 12);
            } else {
            	pstmt.setString(46, this.getAccName());
            }
            pstmt.setInt(47, this.getPrintCount());
            pstmt.setInt(48, this.getLostTimes());
            if(this.getLang() == null || this.getLang().equals("null")) {
            	pstmt.setNull(49, 12);
            } else {
            	pstmt.setString(49, this.getLang());
            }
            if(this.getCurrency() == null || this.getCurrency().equals("null")) {
            	pstmt.setNull(50, 12);
            } else {
            	pstmt.setString(50, this.getCurrency());
            }
            if(this.getRemark() == null || this.getRemark().equals("null")) {
            	pstmt.setNull(51, 12);
            } else {
            	pstmt.setString(51, this.getRemark());
            }
            pstmt.setInt(52, this.getPeoples());
            pstmt.setDouble(53, this.getMult());
            pstmt.setDouble(54, this.getPrem());
            pstmt.setDouble(55, this.getAmnt());
            pstmt.setDouble(56, this.getSumPrem());
            pstmt.setDouble(57, this.getDif());
            if(this.getPaytoDate() == null || this.getPaytoDate().equals("null")) {
            	pstmt.setNull(58, 93);
            } else {
            	pstmt.setDate(58, Date.valueOf(this.getPaytoDate()));
            }
            if(this.getFirstPayDate() == null || this.getFirstPayDate().equals("null")) {
            	pstmt.setNull(59, 93);
            } else {
            	pstmt.setDate(59, Date.valueOf(this.getFirstPayDate()));
            }
            if(this.getCValiDate() == null || this.getCValiDate().equals("null")) {
            	pstmt.setNull(60, 93);
            } else {
            	pstmt.setDate(60, Date.valueOf(this.getCValiDate()));
            }
            if(this.getInputOperator() == null || this.getInputOperator().equals("null")) {
            	pstmt.setNull(61, 12);
            } else {
            	pstmt.setString(61, this.getInputOperator());
            }
            if(this.getInputDate() == null || this.getInputDate().equals("null")) {
            	pstmt.setNull(62, 93);
            } else {
            	pstmt.setDate(62, Date.valueOf(this.getInputDate()));
            }
            if(this.getInputTime() == null || this.getInputTime().equals("null")) {
            	pstmt.setNull(63, 12);
            } else {
            	pstmt.setString(63, this.getInputTime());
            }
            if(this.getApproveFlag() == null || this.getApproveFlag().equals("null")) {
            	pstmt.setNull(64, 12);
            } else {
            	pstmt.setString(64, this.getApproveFlag());
            }
            if(this.getApproveCode() == null || this.getApproveCode().equals("null")) {
            	pstmt.setNull(65, 12);
            } else {
            	pstmt.setString(65, this.getApproveCode());
            }
            if(this.getApproveDate() == null || this.getApproveDate().equals("null")) {
            	pstmt.setNull(66, 93);
            } else {
            	pstmt.setDate(66, Date.valueOf(this.getApproveDate()));
            }
            if(this.getApproveTime() == null || this.getApproveTime().equals("null")) {
            	pstmt.setNull(67, 12);
            } else {
            	pstmt.setString(67, this.getApproveTime());
            }
            if(this.getUWFlag() == null || this.getUWFlag().equals("null")) {
            	pstmt.setNull(68, 12);
            } else {
            	pstmt.setString(68, this.getUWFlag());
            }
            if(this.getUWOperator() == null || this.getUWOperator().equals("null")) {
            	pstmt.setNull(69, 12);
            } else {
            	pstmt.setString(69, this.getUWOperator());
            }
            if(this.getUWDate() == null || this.getUWDate().equals("null")) {
            	pstmt.setNull(70, 93);
            } else {
            	pstmt.setDate(70, Date.valueOf(this.getUWDate()));
            }
            if(this.getUWTime() == null || this.getUWTime().equals("null")) {
            	pstmt.setNull(71, 12);
            } else {
            	pstmt.setString(71, this.getUWTime());
            }
            if(this.getAppFlag() == null || this.getAppFlag().equals("null")) {
            	pstmt.setNull(72, 12);
            } else {
            	pstmt.setString(72, this.getAppFlag());
            }
            if(this.getPolApplyDate() == null || this.getPolApplyDate().equals("null")) {
            	pstmt.setNull(73, 93);
            } else {
            	pstmt.setDate(73, Date.valueOf(this.getPolApplyDate()));
            }
            if(this.getGetPolDate() == null || this.getGetPolDate().equals("null")) {
            	pstmt.setNull(74, 93);
            } else {
            	pstmt.setDate(74, Date.valueOf(this.getGetPolDate()));
            }
            if(this.getGetPolTime() == null || this.getGetPolTime().equals("null")) {
            	pstmt.setNull(75, 12);
            } else {
            	pstmt.setString(75, this.getGetPolTime());
            }
            if(this.getCustomGetPolDate() == null || this.getCustomGetPolDate().equals("null")) {
            	pstmt.setNull(76, 93);
            } else {
            	pstmt.setDate(76, Date.valueOf(this.getCustomGetPolDate()));
            }
            if(this.getState() == null || this.getState().equals("null")) {
            	pstmt.setNull(77, 12);
            } else {
            	pstmt.setString(77, this.getState());
            }
            if(this.getOperator() == null || this.getOperator().equals("null")) {
            	pstmt.setNull(78, 12);
            } else {
            	pstmt.setString(78, this.getOperator());
            }
            if(this.getMakeDate() == null || this.getMakeDate().equals("null")) {
            	pstmt.setNull(79, 93);
            } else {
            	pstmt.setDate(79, Date.valueOf(this.getMakeDate()));
            }
            if(this.getMakeTime() == null || this.getMakeTime().equals("null")) {
            	pstmt.setNull(80, 12);
            } else {
            	pstmt.setString(80, this.getMakeTime());
            }
            if(this.getModifyDate() == null || this.getModifyDate().equals("null")) {
            	pstmt.setNull(81, 93);
            } else {
            	pstmt.setDate(81, Date.valueOf(this.getModifyDate()));
            }
            if(this.getModifyTime() == null || this.getModifyTime().equals("null")) {
            	pstmt.setNull(82, 12);
            } else {
            	pstmt.setString(82, this.getModifyTime());
            }
            if(this.getFirstTrialOperator() == null || this.getFirstTrialOperator().equals("null")) {
            	pstmt.setNull(83, 12);
            } else {
            	pstmt.setString(83, this.getFirstTrialOperator());
            }
            if(this.getFirstTrialDate() == null || this.getFirstTrialDate().equals("null")) {
            	pstmt.setNull(84, 93);
            } else {
            	pstmt.setDate(84, Date.valueOf(this.getFirstTrialDate()));
            }
            if(this.getFirstTrialTime() == null || this.getFirstTrialTime().equals("null")) {
            	pstmt.setNull(85, 12);
            } else {
            	pstmt.setString(85, this.getFirstTrialTime());
            }
            if(this.getReceiveOperator() == null || this.getReceiveOperator().equals("null")) {
            	pstmt.setNull(86, 12);
            } else {
            	pstmt.setString(86, this.getReceiveOperator());
            }
            if(this.getReceiveDate() == null || this.getReceiveDate().equals("null")) {
            	pstmt.setNull(87, 93);
            } else {
            	pstmt.setDate(87, Date.valueOf(this.getReceiveDate()));
            }
            if(this.getReceiveTime() == null || this.getReceiveTime().equals("null")) {
            	pstmt.setNull(88, 12);
            } else {
            	pstmt.setString(88, this.getReceiveTime());
            }
            if(this.getTempFeeNo() == null || this.getTempFeeNo().equals("null")) {
            	pstmt.setNull(89, 12);
            } else {
            	pstmt.setString(89, this.getTempFeeNo());
            }
            if(this.getSellType() == null || this.getSellType().equals("null")) {
            	pstmt.setNull(90, 12);
            } else {
            	pstmt.setString(90, this.getSellType());
            }
            if(this.getForceUWFlag() == null || this.getForceUWFlag().equals("null")) {
            	pstmt.setNull(91, 12);
            } else {
            	pstmt.setString(91, this.getForceUWFlag());
            }
            if(this.getForceUWReason() == null || this.getForceUWReason().equals("null")) {
            	pstmt.setNull(92, 12);
            } else {
            	pstmt.setString(92, this.getForceUWReason());
            }
            if(this.getNewBankCode() == null || this.getNewBankCode().equals("null")) {
            	pstmt.setNull(93, 12);
            } else {
            	pstmt.setString(93, this.getNewBankCode());
            }
            if(this.getNewBankAccNo() == null || this.getNewBankAccNo().equals("null")) {
            	pstmt.setNull(94, 12);
            } else {
            	pstmt.setString(94, this.getNewBankAccNo());
            }
            if(this.getNewAccName() == null || this.getNewAccName().equals("null")) {
            	pstmt.setNull(95, 12);
            } else {
            	pstmt.setString(95, this.getNewAccName());
            }
            if(this.getNewPayMode() == null || this.getNewPayMode().equals("null")) {
            	pstmt.setNull(96, 12);
            } else {
            	pstmt.setString(96, this.getNewPayMode());
            }
            if(this.getAgentBankCode() == null || this.getAgentBankCode().equals("null")) {
            	pstmt.setNull(97, 12);
            } else {
            	pstmt.setString(97, this.getAgentBankCode());
            }
            if(this.getBankAgent() == null || this.getBankAgent().equals("null")) {
            	pstmt.setNull(98, 12);
            } else {
            	pstmt.setString(98, this.getBankAgent());
            }
            if(this.getBankAgentName() == null || this.getBankAgentName().equals("null")) {
            	pstmt.setNull(99, 12);
            } else {
            	pstmt.setString(99, this.getBankAgentName());
            }
            if(this.getBankAgentTel() == null || this.getBankAgentTel().equals("null")) {
            	pstmt.setNull(100, 12);
            } else {
            	pstmt.setString(100, this.getBankAgentTel());
            }
            if(this.getProdSetCode() == null || this.getProdSetCode().equals("null")) {
            	pstmt.setNull(101, 12);
            } else {
            	pstmt.setString(101, this.getProdSetCode());
            }
            if(this.getPolicyNo() == null || this.getPolicyNo().equals("null")) {
            	pstmt.setNull(102, 12);
            } else {
            	pstmt.setString(102, this.getPolicyNo());
            }
            if(this.getBillPressNo() == null || this.getBillPressNo().equals("null")) {
            	pstmt.setNull(103, 12);
            } else {
            	pstmt.setString(103, this.getBillPressNo());
            }
            if(this.getCardTypeCode() == null || this.getCardTypeCode().equals("null")) {
            	pstmt.setNull(104, 12);
            } else {
            	pstmt.setString(104, this.getCardTypeCode());
            }
            if(this.getVisitDate() == null || this.getVisitDate().equals("null")) {
            	pstmt.setNull(105, 93);
            } else {
            	pstmt.setDate(105, Date.valueOf(this.getVisitDate()));
            }
            if(this.getVisitTime() == null || this.getVisitTime().equals("null")) {
            	pstmt.setNull(106, 12);
            } else {
            	pstmt.setString(106, this.getVisitTime());
            }
            if(this.getSaleCom() == null || this.getSaleCom().equals("null")) {
            	pstmt.setNull(107, 12);
            } else {
            	pstmt.setString(107, this.getSaleCom());
            }
            if(this.getPrintFlag() == null || this.getPrintFlag().equals("null")) {
            	pstmt.setNull(108, 12);
            } else {
            	pstmt.setString(108, this.getPrintFlag());
            }
            if(this.getInvoicePrtFlag() == null || this.getInvoicePrtFlag().equals("null")) {
            	pstmt.setNull(109, 12);
            } else {
            	pstmt.setString(109, this.getInvoicePrtFlag());
            }
            if(this.getNewReinsureFlag() == null || this.getNewReinsureFlag().equals("null")) {
            	pstmt.setNull(110, 12);
            } else {
            	pstmt.setString(110, this.getNewReinsureFlag());
            }
            if(this.getRenewPayFlag() == null || this.getRenewPayFlag().equals("null")) {
            	pstmt.setNull(111, 12);
            } else {
            	pstmt.setString(111, this.getRenewPayFlag());
            }
            if(this.getAppntFirstName() == null || this.getAppntFirstName().equals("null")) {
            	pstmt.setNull(112, 12);
            } else {
            	pstmt.setString(112, this.getAppntFirstName());
            }
            if(this.getAppntLastName() == null || this.getAppntLastName().equals("null")) {
            	pstmt.setNull(113, 12);
            } else {
            	pstmt.setString(113, this.getAppntLastName());
            }
            if(this.getInsuredFirstName() == null || this.getInsuredFirstName().equals("null")) {
            	pstmt.setNull(114, 12);
            } else {
            	pstmt.setString(114, this.getInsuredFirstName());
            }
            if(this.getInsuredLastName() == null || this.getInsuredLastName().equals("null")) {
            	pstmt.setNull(115, 12);
            } else {
            	pstmt.setString(115, this.getInsuredLastName());
            }
            if(this.getAuthorFlag() == null || this.getAuthorFlag().equals("null")) {
            	pstmt.setNull(116, 12);
            } else {
            	pstmt.setString(116, this.getAuthorFlag());
            }
            pstmt.setInt(117, this.getGreenChnl());
            if(this.getTBType() == null || this.getTBType().equals("null")) {
            	pstmt.setNull(118, 12);
            } else {
            	pstmt.setString(118, this.getTBType());
            }
            if(this.getEAuto() == null || this.getEAuto().equals("null")) {
            	pstmt.setNull(119, 12);
            } else {
            	pstmt.setString(119, this.getEAuto());
            }
            if(this.getSlipForm() == null || this.getSlipForm().equals("null")) {
            	pstmt.setNull(120, 12);
            } else {
            	pstmt.setString(120, this.getSlipForm());
            }
            // execute sql
            pstmt.executeUpdate();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LPContDB";
            tError.functionName = "insert()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors.addOneError(tError);
            ex.printStackTrace();

            try {
                pstmt.close();
            } catch (Exception e){}

            if( !mflag ) {
                try {
                    con.close();
                } catch (Exception e){}
            }

            return false;
        }

        if(!mflag) {
            try {
                con.close();
            } catch (Exception e){}
        }

        return true;
    }

    public boolean getInfo() {
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        if(!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            pstmt = con.prepareStatement("SELECT * FROM LPCont WHERE  1=1  AND EdorNo = ? AND EdorType = ? AND ContNo = ?", 
                ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
            if(this.getEdorNo() == null || this.getEdorNo().equals("null")) {
            	pstmt.setNull(1, 12);
            } else {
            	pstmt.setString(1, this.getEdorNo());
            }
            if(this.getEdorType() == null || this.getEdorType().equals("null")) {
            	pstmt.setNull(2, 12);
            } else {
            	pstmt.setString(2, this.getEdorType());
            }
            if(this.getContNo() == null || this.getContNo().equals("null")) {
            	pstmt.setNull(3, 12);
            } else {
            	pstmt.setString(3, this.getContNo());
            }
            rs = pstmt.executeQuery();
            int i = 0;
            while (rs.next()) {
                i++;
                if (!this.setSchema(rs,i)) {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "LPContDB";
                    tError.functionName = "getInfo";
                    tError.errorMessage = "取数失败!";
                    this.mErrors .addOneError(tError);

                    try{ rs.close(); } catch( Exception ex ) {}
                    try{ pstmt.close(); } catch( Exception ex1 ) {}

                    if (!mflag) {
                        try {
                            con.close();
                        }
                        catch(Exception et){}
                    }
                    return false;
                }
                break;
            }
            try{ rs.close(); } catch( Exception ex2 ) {}
            try{ pstmt.close(); } catch( Exception ex3 ) {}

            if( i == 0 ) {
                if (!mflag) {
                    try {
                        con.close();
                    }
                    catch(Exception et){}
                }
                return false;
            }
        }
        catch(Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LPContDB";
            tError.functionName = "getInfo";
            tError.errorMessage = ExceptionUtils.exceptionToString(e);
            this.mErrors .addOneError(tError);

            try{ rs.close(); } catch( Exception ex ) {}
            try{ pstmt.close(); } catch( Exception ex1 ) {}

            if (!mflag) {
                try {
                    con.close();
                }
                catch(Exception et){}
            }
            return false;
        }
        // 断开数据库连接
        if (!mflag) {
            try {
                con.close();
            }
            catch(Exception e){}
        }

        return true;
    }

    public LPContSet query() {
        Statement stmt = null;
        ResultSet rs = null;
        LPContSet aLPContSet = new LPContSet();

        if (!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            stmt = con.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
            SQLString sqlObj = new SQLString("LPCont");
            LPContSchema aSchema = this.getSchema();
            sqlObj.setSQL(5, aSchema);
            String sql = sqlObj.getSQL();

            rs = stmt.executeQuery(sql);
            int i = 0;
            while (rs.next()) {
                i++;
                if (i>10000) {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "LPContDB";
                    tError.functionName = "query";
                    tError.errorMessage = "SQL语句返回结果集太多，请换用其他方式!";
                    this.mErrors .addOneError(tError);
                        break;
                }
                LPContSchema s1 = new LPContSchema();
                s1.setSchema(rs,i);
                aLPContSet.add(s1);
            }
            try{ rs.close(); } catch( Exception ex ) {}
            try{ stmt.close(); } catch( Exception ex1 ) {}
        }
        catch(Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LPContDB";
            tError.functionName = "query";
            tError.errorMessage = ExceptionUtils.exceptionToString(e);
            this.mErrors.addOneError(tError);
            e.printStackTrace();

            try { rs.close(); } catch (Exception ex2) { ex2.printStackTrace(); }
            try { stmt.close(); } catch(Exception ex3) { ex3.printStackTrace(); }

            if (!mflag) {
                try {
                    con.close();
                }
                catch(Exception et){et.printStackTrace();}
            }
        }

        if (!mflag) {
            try {
                con.close();
            }
            catch(Exception e){e.printStackTrace();}
        }

        return aLPContSet;
    }

    public LPContSet executeQuery(String sql)  {
        Statement stmt = null;
        ResultSet rs = null;
        LPContSet aLPContSet = new LPContSet();

        if (!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            stmt = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);

            rs = stmt.executeQuery(StrTool.GBKToUnicode(sql));
            int i = 0;
            while (rs.next()) {
                i++;
                if (i>10000) {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "LPContDB";
                    tError.functionName = "executeQuery";
                    tError.errorMessage = "SQL语句返回结果集太多，请换用其他方式!";
                    this.mErrors .addOneError(tError);
                        break;
                }
                LPContSchema s1 = new LPContSchema();
                if (!s1.setSchema(rs,i)) {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "LPContDB";
                    tError.functionName = "executeQuery";
                    tError.errorMessage = "sql语句有误，请查看表名及字段名信息!";
                    this.mErrors .addOneError(tError);
                }
                aLPContSet.add(s1);
            }
            try{ rs.close(); } catch( Exception ex ) {}
            try{ stmt.close(); } catch( Exception ex1 ) {}
        }
        catch(Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LPContDB";
            tError.functionName = "executeQuery";
            tError.errorMessage = ExceptionUtils.exceptionToString(e);
            this.mErrors .addOneError(tError);

            try{ rs.close(); } catch( Exception ex2 ) {}
            try{ stmt.close(); } catch( Exception ex3 ) {}

            if (!mflag) {
                try {
                    con.close();
                }
                catch(Exception et){}
            }
        }

        if (!mflag) {
            try {
                con.close();
            }
            catch(Exception e){}
        }

        return aLPContSet;
    }

    public LPContSet query(int nStart, int nCount) {
        Statement stmt = null;
        ResultSet rs = null;
        LPContSet aLPContSet = new LPContSet();

        if (!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            stmt = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);
            SQLString sqlObj = new SQLString("LPCont");
            LPContSchema aSchema = this.getSchema();
            sqlObj.setSQL(5,aSchema);
            String sql = sqlObj.getSQL();

            rs = stmt.executeQuery(sql);
            int i = 0;
            while (rs.next()) {
                i++;

                if( i < nStart ) {
                    continue;
                }

                if( i >= nStart + nCount ) {
                    break;
                }

                LPContSchema s1 = new LPContSchema();
                s1.setSchema(rs,i);
                aLPContSet.add(s1);
            }
            try{ rs.close(); } catch( Exception ex ) {}
            try{ stmt.close(); } catch( Exception ex1 ) {}
        }
        catch(Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LPContDB";
            tError.functionName = "query";
            tError.errorMessage = ExceptionUtils.exceptionToString(e);
            this.mErrors .addOneError(tError);

            try{ rs.close(); } catch( Exception ex2 ) {}
            try{ stmt.close(); } catch( Exception ex3 ) {}

            if (!mflag) {
                try {
                    con.close();
                }
                catch(Exception et){}
            }
        }

        if (!mflag) {
            try {
                con.close();
            }
            catch(Exception e){}
        }

        return aLPContSet;
    }

    public LPContSet executeQuery(String sql, int nStart, int nCount) {
        Statement stmt = null;
        ResultSet rs = null;
        LPContSet aLPContSet = new LPContSet();

        if (!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            stmt = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);

            rs = stmt.executeQuery(StrTool.GBKToUnicode(sql));
            int i = 0;
            while (rs.next()) {
                i++;

                if( i < nStart ) {
                    continue;
                }

                if( i >= nStart + nCount ) {
                    break;
                }

                LPContSchema s1 = new LPContSchema();
                if (!s1.setSchema(rs,i)) {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "LPContDB";
                    tError.functionName = "executeQuery";
                    tError.errorMessage = "sql语句有误，请查看表名及字段名信息!";
                    this.mErrors .addOneError(tError);
                }
                aLPContSet.add(s1);
            }
            try{ rs.close(); } catch( Exception ex ) {}
            try{ stmt.close(); } catch( Exception ex1 ) {}
        }
        catch(Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LPContDB";
            tError.functionName = "executeQuery";
            tError.errorMessage = ExceptionUtils.exceptionToString(e);
            this.mErrors .addOneError(tError);

            try{ rs.close(); } catch( Exception ex2 ) {}
            try{ stmt.close(); } catch( Exception ex3 ) {}

            if (!mflag) {
                try {
                    con.close();
                }
                catch(Exception et){}
            }
        }

        if (!mflag) {
            try {
                con.close();
            }
            catch(Exception e){}
        }

        return aLPContSet;
    }

    public boolean update(String strWherePart) {
        Statement stmt = null;

        if (!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            stmt = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);
            SQLString sqlObj = new SQLString("LPCont");
            LPContSchema aSchema = this.getSchema();
            sqlObj.setSQL(2,aSchema);
            String sql = "update LPCont " + sqlObj.getUpdPart() + " where " + strWherePart;

            int operCount = stmt.executeUpdate(sql);
            if (operCount == 0) {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "LPContDB";
                tError.functionName = "update";
                tError.errorMessage = "更新数据失败!";
                this.mErrors .addOneError(tError);

                if (!mflag) {
                    try {
                        con.close();
                    }
                    catch(Exception et){}
                }
                return false;
            }
        }
        catch(Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LPContDB";
            tError.functionName = "update";
            tError.errorMessage = ExceptionUtils.exceptionToString(e);
            this.mErrors .addOneError(tError);

            try{ stmt.close(); } catch( Exception ex1 ) {}

            if (!mflag) {
                try {
                    con.close();
                }
                catch(Exception et){}
            }
            return false;
        }
        // 断开数据库连接
        if (!mflag) {
            try {
                con.close();
            }
            catch(Exception e){}
        }

        return true;
    }

    /**
     * 准备数据查询条件
     * @param strSQL String
     * @return boolean
     */
    public boolean prepareData(String strSQL) {
        if (mResultSet != null) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LPContDB";
            tError.functionName = "prepareData";
            tError.errorMessage = "数据集非空，程序在准备数据集之后，没有关闭！";
            this.mErrors.addOneError(tError);
            return false;
        }

        if (!mflag) {
            con = DBConnPool.getConnection();
        }
        try {
            mStatement = con.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
            mResultSet = mStatement.executeQuery(StrTool.GBKToUnicode(strSQL));
        }
        catch (Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LPContDB";
            tError.functionName = "prepareData";
            tError.errorMessage = ExceptionUtils.exceptionToString(e);
            this.mErrors.addOneError(tError);
            try {
                mResultSet.close();
            }
            catch (Exception ex2) {}
            try {
                mStatement.close();
            }
            catch (Exception ex3) {}
            if (!mflag) {
                try {
                    con.close();
                }
                catch (Exception et) {}
            }
            return false;
        }

        if (!mflag) {
            try {
                con.close();
            }
            catch (Exception e) {}
        }
        return true;
    }

    /**
     * 获取数据集
     * @return boolean
     */
    public boolean hasMoreData() {
        boolean flag = true;
        if (null == mResultSet) {
            CError tError = new CError();
            tError.moduleName = "LPContDB";
            tError.functionName = "hasMoreData";
            tError.errorMessage = "数据集为空，请先准备数据集！";
            this.mErrors.addOneError(tError);
            return false;
        }
        try {
            flag = mResultSet.next();
        }
        catch (Exception ex) {
            CError tError = new CError();
            tError.moduleName = "LPContDB";
            tError.functionName = "hasMoreData";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors.addOneError(tError);
            try {
                mResultSet.close();
                mResultSet = null;
            }
            catch (Exception ex2) {}
            try {
                mStatement.close();
                mStatement = null;
            }
            catch (Exception ex3) {}
            if (!mflag) {
                try {
                    con.close();
                }
                catch (Exception et) {}
            }
            return false;
        }
        return flag;
    }
    /**
     * 获取定量数据
     * @return LPContSet
     */
    public LPContSet getData() {
        int tCount = 0;
        LPContSet tLPContSet = new LPContSet();
        LPContSchema tLPContSchema = null;
        if (null == mResultSet) {
            CError tError = new CError();
            tError.moduleName = "LPContDB";
            tError.functionName = "getData";
            tError.errorMessage = "数据集为空，请先准备数据集！";
            this.mErrors.addOneError(tError);
            return null;
        }
        try {
            tCount = 1;
            tLPContSchema = new LPContSchema();
            tLPContSchema.setSchema(mResultSet, 1);
            tLPContSet.add(tLPContSchema);
            //注意mResultSet.next()的作用
            while (tCount++ < SysConst.FETCHCOUNT) {
                if (mResultSet.next()) {
                    tLPContSchema = new LPContSchema();
                    tLPContSchema.setSchema(mResultSet, 1);
                    tLPContSet.add(tLPContSchema);
                }
            }
        }
        catch (Exception ex) {
            CError tError = new CError();
            tError.moduleName = "LPContDB";
            tError.functionName = "getData";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors.addOneError(tError);
            try {
                mResultSet.close();
                mResultSet = null;
            }
            catch (Exception ex2) {}
            try {
                mStatement.close();
                mStatement = null;
            }
            catch (Exception ex3) {}
            if (!mflag) {
                try {
                    con.close();
                }
                catch (Exception et) {}
            }
            return null;
        }
        return tLPContSet;
    }
    /**
     * 关闭数据集
     * @return boolean
     */
    public boolean closeData() {
        boolean flag = true;
        try {
            if (null == mResultSet) {
                CError tError = new CError();
                tError.moduleName = "LPContDB";
                tError.functionName = "closeData";
                tError.errorMessage = "数据集已经关闭了！";
                this.mErrors.addOneError(tError);
                flag = false;
            } else {
                mResultSet.close();
                mResultSet = null;
            }
        }
        catch (Exception ex2) {
            CError tError = new CError();
            tError.moduleName = "LPContDB";
            tError.functionName = "closeData";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex2);
            this.mErrors.addOneError(tError);
            flag = false;
        }
        try {
            if (null == mStatement) {
                CError tError = new CError();
                tError.moduleName = "LPContDB";
                tError.functionName = "closeData";
                tError.errorMessage = "语句已经关闭了！";
                this.mErrors.addOneError(tError);
                flag = false;
            } else {
                mStatement.close();
                mStatement = null;
            }
        }
        catch (Exception ex3) {
            CError tError = new CError();
            tError.moduleName = "LPContDB";
            tError.functionName = "closeData";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex3);
            this.mErrors.addOneError(tError);
            flag = false;
        }
        return flag;
    }
}
