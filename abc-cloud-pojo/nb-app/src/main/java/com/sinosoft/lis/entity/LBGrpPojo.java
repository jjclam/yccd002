/**
 * Copyright (c) 2018 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.entity;

import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.Pojo;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;

import java.io.Serializable;

/**
 * <p>ClassName: LBGrpPojo </p>
 * <p>Copyright: Copyright (c) 2018</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2018-05-23
 */
public class LBGrpPojo implements Pojo,Serializable {
    // @Field
    /** 客户号码 */
    private String CustomerNo; 
    /** 密码 */
    private String Password; 
    /** 单位名称 */
    private String GrpName; 
    /** 行业分类 */
    private String BusinessType; 
    /** 单位性质 */
    private String GrpNature; 
    /** 总人数 */
    private int Peoples; 
    /** 注册资本 */
    private double RgtMoney; 
    /** 资产总额 */
    private double Asset; 
    /** 净资产收益率 */
    private double NetProfitRate; 
    /** 主营业务 */
    private String MainBussiness; 
    /** 法人 */
    private String Corporation; 
    /** 机构分布区域 */
    private String ComAera; 
    /** 单位传真 */
    private String Fax; 
    /** 单位电话 */
    private String Phone; 
    /** 付款方式 */
    private String GetFlag; 
    /** 负责人 */
    private String Satrap; 
    /** 公司e_mail */
    private String EMail; 
    /** 成立日期 */
    private String  FoundDate;
    /** 银行编码 */
    private String BankCode; 
    /** 银行帐号 */
    private String BankAccNo; 
    /** 客户组号码 */
    private String GrpGroupNo; 
    /** 黑名单标记 */
    private String BlacklistFlag; 
    /** 状态 */
    private String State; 
    /** 备注 */
    private String Remark; 
    /** Vip值 */
    private String VIPValue; 
    /** 操作员代码 */
    private String Operator; 
    /** 入机日期 */
    private String  MakeDate;
    /** 入机时间 */
    private String MakeTime; 
    /** 最后一次修改日期 */
    private String  ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime; 
    /** 子公司标志 */
    private String SubCompanyFlag; 
    /** 上级客户号码 */
    private String SupCustoemrNo; 
    /** 级别代码 */
    private String LevelCode; 
    /** 在职人数 */
    private int OnWorkPeoples; 
    /** 退休人数 */
    private int OffWorkPeoples; 
    /** 其它人员人数 */
    private int OtherPeoples; 
    /** 行业大类 */
    private String BusinessBigType; 
    /** 单位社保登记号 */
    private String SocialInsuNo; 
    /** 黑名单原因 */
    private String BlackListReason; 
    /** 单位拼音名称 */
    private String GrpNamePY; 
    /** 检索关键字 */
    private String SearchKeyWord; 
    /** 法人证件类型 */
    private String CorIDType; 
    /** 法人证件号码 */
    private String CorID; 
    /** 联系人证件有效止期 */
    private String  CorIDExpiryDate;
    /** 实际控制人 */
    private String ActuCtrl; 
    /** 营业执照号 */
    private String License; 
    /** 组织机构代码 */
    private String OrganizationCode; 
    /** 税务登记证 */
    private String TaxCode; 
    /** 管理机构 */
    private String ManageCom; 
    /** 公司代码 */
    private String ComCode; 
    /** 最后一次修改操作员 */
    private String ModifyOperator; 
    /** 证件类型 */
    private String IDType; 
    /** 证件号码 */
    private String IDNo; 
    /** 证件有效期 */
    private String  IDExpDate;
    /** 性别 */
    private String Sex; 
    /** 国籍 */
    private String NativePlace; 
    /** 职业描述 */
    private String Occupation; 
    /** 职业编码 */
    private String OccupationCode; 
    /** 投保人email */
    private String Email2; 


    public static final int FIELDNUM = 59;    // 数据库表的字段个数

    private FDate fDate = new FDate();        // 处理日期
    public String getCustomerNo() {
        return CustomerNo;
    }
    public void setCustomerNo(String aCustomerNo) {
        CustomerNo = aCustomerNo;
    }
    public String getPassword() {
        return Password;
    }
    public void setPassword(String aPassword) {
        Password = aPassword;
    }
    public String getGrpName() {
        return GrpName;
    }
    public void setGrpName(String aGrpName) {
        GrpName = aGrpName;
    }
    public String getBusinessType() {
        return BusinessType;
    }
    public void setBusinessType(String aBusinessType) {
        BusinessType = aBusinessType;
    }
    public String getGrpNature() {
        return GrpNature;
    }
    public void setGrpNature(String aGrpNature) {
        GrpNature = aGrpNature;
    }
    public int getPeoples() {
        return Peoples;
    }
    public void setPeoples(int aPeoples) {
        Peoples = aPeoples;
    }
    public void setPeoples(String aPeoples) {
        if (aPeoples != null && !aPeoples.equals("")) {
            Integer tInteger = new Integer(aPeoples);
            int i = tInteger.intValue();
            Peoples = i;
        }
    }

    public double getRgtMoney() {
        return RgtMoney;
    }
    public void setRgtMoney(double aRgtMoney) {
        RgtMoney = aRgtMoney;
    }
    public void setRgtMoney(String aRgtMoney) {
        if (aRgtMoney != null && !aRgtMoney.equals("")) {
            Double tDouble = new Double(aRgtMoney);
            double d = tDouble.doubleValue();
            RgtMoney = d;
        }
    }

    public double getAsset() {
        return Asset;
    }
    public void setAsset(double aAsset) {
        Asset = aAsset;
    }
    public void setAsset(String aAsset) {
        if (aAsset != null && !aAsset.equals("")) {
            Double tDouble = new Double(aAsset);
            double d = tDouble.doubleValue();
            Asset = d;
        }
    }

    public double getNetProfitRate() {
        return NetProfitRate;
    }
    public void setNetProfitRate(double aNetProfitRate) {
        NetProfitRate = aNetProfitRate;
    }
    public void setNetProfitRate(String aNetProfitRate) {
        if (aNetProfitRate != null && !aNetProfitRate.equals("")) {
            Double tDouble = new Double(aNetProfitRate);
            double d = tDouble.doubleValue();
            NetProfitRate = d;
        }
    }

    public String getMainBussiness() {
        return MainBussiness;
    }
    public void setMainBussiness(String aMainBussiness) {
        MainBussiness = aMainBussiness;
    }
    public String getCorporation() {
        return Corporation;
    }
    public void setCorporation(String aCorporation) {
        Corporation = aCorporation;
    }
    public String getComAera() {
        return ComAera;
    }
    public void setComAera(String aComAera) {
        ComAera = aComAera;
    }
    public String getFax() {
        return Fax;
    }
    public void setFax(String aFax) {
        Fax = aFax;
    }
    public String getPhone() {
        return Phone;
    }
    public void setPhone(String aPhone) {
        Phone = aPhone;
    }
    public String getGetFlag() {
        return GetFlag;
    }
    public void setGetFlag(String aGetFlag) {
        GetFlag = aGetFlag;
    }
    public String getSatrap() {
        return Satrap;
    }
    public void setSatrap(String aSatrap) {
        Satrap = aSatrap;
    }
    public String getEMail() {
        return EMail;
    }
    public void setEMail(String aEMail) {
        EMail = aEMail;
    }
    public String getFoundDate() {
        return FoundDate;
    }
    public void setFoundDate(String aFoundDate) {
        FoundDate = aFoundDate;
    }
    public String getBankCode() {
        return BankCode;
    }
    public void setBankCode(String aBankCode) {
        BankCode = aBankCode;
    }
    public String getBankAccNo() {
        return BankAccNo;
    }
    public void setBankAccNo(String aBankAccNo) {
        BankAccNo = aBankAccNo;
    }
    public String getGrpGroupNo() {
        return GrpGroupNo;
    }
    public void setGrpGroupNo(String aGrpGroupNo) {
        GrpGroupNo = aGrpGroupNo;
    }
    public String getBlacklistFlag() {
        return BlacklistFlag;
    }
    public void setBlacklistFlag(String aBlacklistFlag) {
        BlacklistFlag = aBlacklistFlag;
    }
    public String getState() {
        return State;
    }
    public void setState(String aState) {
        State = aState;
    }
    public String getRemark() {
        return Remark;
    }
    public void setRemark(String aRemark) {
        Remark = aRemark;
    }
    public String getVIPValue() {
        return VIPValue;
    }
    public void setVIPValue(String aVIPValue) {
        VIPValue = aVIPValue;
    }
    public String getOperator() {
        return Operator;
    }
    public void setOperator(String aOperator) {
        Operator = aOperator;
    }
    public String getMakeDate() {
        return MakeDate;
    }
    public void setMakeDate(String aMakeDate) {
        MakeDate = aMakeDate;
    }
    public String getMakeTime() {
        return MakeTime;
    }
    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }
    public String getModifyDate() {
        return ModifyDate;
    }
    public void setModifyDate(String aModifyDate) {
        ModifyDate = aModifyDate;
    }
    public String getModifyTime() {
        return ModifyTime;
    }
    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }
    public String getSubCompanyFlag() {
        return SubCompanyFlag;
    }
    public void setSubCompanyFlag(String aSubCompanyFlag) {
        SubCompanyFlag = aSubCompanyFlag;
    }
    public String getSupCustoemrNo() {
        return SupCustoemrNo;
    }
    public void setSupCustoemrNo(String aSupCustoemrNo) {
        SupCustoemrNo = aSupCustoemrNo;
    }
    public String getLevelCode() {
        return LevelCode;
    }
    public void setLevelCode(String aLevelCode) {
        LevelCode = aLevelCode;
    }
    public int getOnWorkPeoples() {
        return OnWorkPeoples;
    }
    public void setOnWorkPeoples(int aOnWorkPeoples) {
        OnWorkPeoples = aOnWorkPeoples;
    }
    public void setOnWorkPeoples(String aOnWorkPeoples) {
        if (aOnWorkPeoples != null && !aOnWorkPeoples.equals("")) {
            Integer tInteger = new Integer(aOnWorkPeoples);
            int i = tInteger.intValue();
            OnWorkPeoples = i;
        }
    }

    public int getOffWorkPeoples() {
        return OffWorkPeoples;
    }
    public void setOffWorkPeoples(int aOffWorkPeoples) {
        OffWorkPeoples = aOffWorkPeoples;
    }
    public void setOffWorkPeoples(String aOffWorkPeoples) {
        if (aOffWorkPeoples != null && !aOffWorkPeoples.equals("")) {
            Integer tInteger = new Integer(aOffWorkPeoples);
            int i = tInteger.intValue();
            OffWorkPeoples = i;
        }
    }

    public int getOtherPeoples() {
        return OtherPeoples;
    }
    public void setOtherPeoples(int aOtherPeoples) {
        OtherPeoples = aOtherPeoples;
    }
    public void setOtherPeoples(String aOtherPeoples) {
        if (aOtherPeoples != null && !aOtherPeoples.equals("")) {
            Integer tInteger = new Integer(aOtherPeoples);
            int i = tInteger.intValue();
            OtherPeoples = i;
        }
    }

    public String getBusinessBigType() {
        return BusinessBigType;
    }
    public void setBusinessBigType(String aBusinessBigType) {
        BusinessBigType = aBusinessBigType;
    }
    public String getSocialInsuNo() {
        return SocialInsuNo;
    }
    public void setSocialInsuNo(String aSocialInsuNo) {
        SocialInsuNo = aSocialInsuNo;
    }
    public String getBlackListReason() {
        return BlackListReason;
    }
    public void setBlackListReason(String aBlackListReason) {
        BlackListReason = aBlackListReason;
    }
    public String getGrpNamePY() {
        return GrpNamePY;
    }
    public void setGrpNamePY(String aGrpNamePY) {
        GrpNamePY = aGrpNamePY;
    }
    public String getSearchKeyWord() {
        return SearchKeyWord;
    }
    public void setSearchKeyWord(String aSearchKeyWord) {
        SearchKeyWord = aSearchKeyWord;
    }
    public String getCorIDType() {
        return CorIDType;
    }
    public void setCorIDType(String aCorIDType) {
        CorIDType = aCorIDType;
    }
    public String getCorID() {
        return CorID;
    }
    public void setCorID(String aCorID) {
        CorID = aCorID;
    }
    public String getCorIDExpiryDate() {
        return CorIDExpiryDate;
    }
    public void setCorIDExpiryDate(String aCorIDExpiryDate) {
        CorIDExpiryDate = aCorIDExpiryDate;
    }
    public String getActuCtrl() {
        return ActuCtrl;
    }
    public void setActuCtrl(String aActuCtrl) {
        ActuCtrl = aActuCtrl;
    }
    public String getLicense() {
        return License;
    }
    public void setLicense(String aLicense) {
        License = aLicense;
    }
    public String getOrganizationCode() {
        return OrganizationCode;
    }
    public void setOrganizationCode(String aOrganizationCode) {
        OrganizationCode = aOrganizationCode;
    }
    public String getTaxCode() {
        return TaxCode;
    }
    public void setTaxCode(String aTaxCode) {
        TaxCode = aTaxCode;
    }
    public String getManageCom() {
        return ManageCom;
    }
    public void setManageCom(String aManageCom) {
        ManageCom = aManageCom;
    }
    public String getComCode() {
        return ComCode;
    }
    public void setComCode(String aComCode) {
        ComCode = aComCode;
    }
    public String getModifyOperator() {
        return ModifyOperator;
    }
    public void setModifyOperator(String aModifyOperator) {
        ModifyOperator = aModifyOperator;
    }
    public String getIDType() {
        return IDType;
    }
    public void setIDType(String aIDType) {
        IDType = aIDType;
    }
    public String getIDNo() {
        return IDNo;
    }
    public void setIDNo(String aIDNo) {
        IDNo = aIDNo;
    }
    public String getIDExpDate() {
        return IDExpDate;
    }
    public void setIDExpDate(String aIDExpDate) {
        IDExpDate = aIDExpDate;
    }
    public String getSex() {
        return Sex;
    }
    public void setSex(String aSex) {
        Sex = aSex;
    }
    public String getNativePlace() {
        return NativePlace;
    }
    public void setNativePlace(String aNativePlace) {
        NativePlace = aNativePlace;
    }
    public String getOccupation() {
        return Occupation;
    }
    public void setOccupation(String aOccupation) {
        Occupation = aOccupation;
    }
    public String getOccupationCode() {
        return OccupationCode;
    }
    public void setOccupationCode(String aOccupationCode) {
        OccupationCode = aOccupationCode;
    }
    public String getEmail2() {
        return Email2;
    }
    public void setEmail2(String aEmail2) {
        Email2 = aEmail2;
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("CustomerNo") ) {
            return 0;
        }
        if( strFieldName.equals("Password") ) {
            return 1;
        }
        if( strFieldName.equals("GrpName") ) {
            return 2;
        }
        if( strFieldName.equals("BusinessType") ) {
            return 3;
        }
        if( strFieldName.equals("GrpNature") ) {
            return 4;
        }
        if( strFieldName.equals("Peoples") ) {
            return 5;
        }
        if( strFieldName.equals("RgtMoney") ) {
            return 6;
        }
        if( strFieldName.equals("Asset") ) {
            return 7;
        }
        if( strFieldName.equals("NetProfitRate") ) {
            return 8;
        }
        if( strFieldName.equals("MainBussiness") ) {
            return 9;
        }
        if( strFieldName.equals("Corporation") ) {
            return 10;
        }
        if( strFieldName.equals("ComAera") ) {
            return 11;
        }
        if( strFieldName.equals("Fax") ) {
            return 12;
        }
        if( strFieldName.equals("Phone") ) {
            return 13;
        }
        if( strFieldName.equals("GetFlag") ) {
            return 14;
        }
        if( strFieldName.equals("Satrap") ) {
            return 15;
        }
        if( strFieldName.equals("EMail") ) {
            return 16;
        }
        if( strFieldName.equals("FoundDate") ) {
            return 17;
        }
        if( strFieldName.equals("BankCode") ) {
            return 18;
        }
        if( strFieldName.equals("BankAccNo") ) {
            return 19;
        }
        if( strFieldName.equals("GrpGroupNo") ) {
            return 20;
        }
        if( strFieldName.equals("BlacklistFlag") ) {
            return 21;
        }
        if( strFieldName.equals("State") ) {
            return 22;
        }
        if( strFieldName.equals("Remark") ) {
            return 23;
        }
        if( strFieldName.equals("VIPValue") ) {
            return 24;
        }
        if( strFieldName.equals("Operator") ) {
            return 25;
        }
        if( strFieldName.equals("MakeDate") ) {
            return 26;
        }
        if( strFieldName.equals("MakeTime") ) {
            return 27;
        }
        if( strFieldName.equals("ModifyDate") ) {
            return 28;
        }
        if( strFieldName.equals("ModifyTime") ) {
            return 29;
        }
        if( strFieldName.equals("SubCompanyFlag") ) {
            return 30;
        }
        if( strFieldName.equals("SupCustoemrNo") ) {
            return 31;
        }
        if( strFieldName.equals("LevelCode") ) {
            return 32;
        }
        if( strFieldName.equals("OnWorkPeoples") ) {
            return 33;
        }
        if( strFieldName.equals("OffWorkPeoples") ) {
            return 34;
        }
        if( strFieldName.equals("OtherPeoples") ) {
            return 35;
        }
        if( strFieldName.equals("BusinessBigType") ) {
            return 36;
        }
        if( strFieldName.equals("SocialInsuNo") ) {
            return 37;
        }
        if( strFieldName.equals("BlackListReason") ) {
            return 38;
        }
        if( strFieldName.equals("GrpNamePY") ) {
            return 39;
        }
        if( strFieldName.equals("SearchKeyWord") ) {
            return 40;
        }
        if( strFieldName.equals("CorIDType") ) {
            return 41;
        }
        if( strFieldName.equals("CorID") ) {
            return 42;
        }
        if( strFieldName.equals("CorIDExpiryDate") ) {
            return 43;
        }
        if( strFieldName.equals("ActuCtrl") ) {
            return 44;
        }
        if( strFieldName.equals("License") ) {
            return 45;
        }
        if( strFieldName.equals("OrganizationCode") ) {
            return 46;
        }
        if( strFieldName.equals("TaxCode") ) {
            return 47;
        }
        if( strFieldName.equals("ManageCom") ) {
            return 48;
        }
        if( strFieldName.equals("ComCode") ) {
            return 49;
        }
        if( strFieldName.equals("ModifyOperator") ) {
            return 50;
        }
        if( strFieldName.equals("IDType") ) {
            return 51;
        }
        if( strFieldName.equals("IDNo") ) {
            return 52;
        }
        if( strFieldName.equals("IDExpDate") ) {
            return 53;
        }
        if( strFieldName.equals("Sex") ) {
            return 54;
        }
        if( strFieldName.equals("NativePlace") ) {
            return 55;
        }
        if( strFieldName.equals("Occupation") ) {
            return 56;
        }
        if( strFieldName.equals("OccupationCode") ) {
            return 57;
        }
        if( strFieldName.equals("Email2") ) {
            return 58;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "CustomerNo";
                break;
            case 1:
                strFieldName = "Password";
                break;
            case 2:
                strFieldName = "GrpName";
                break;
            case 3:
                strFieldName = "BusinessType";
                break;
            case 4:
                strFieldName = "GrpNature";
                break;
            case 5:
                strFieldName = "Peoples";
                break;
            case 6:
                strFieldName = "RgtMoney";
                break;
            case 7:
                strFieldName = "Asset";
                break;
            case 8:
                strFieldName = "NetProfitRate";
                break;
            case 9:
                strFieldName = "MainBussiness";
                break;
            case 10:
                strFieldName = "Corporation";
                break;
            case 11:
                strFieldName = "ComAera";
                break;
            case 12:
                strFieldName = "Fax";
                break;
            case 13:
                strFieldName = "Phone";
                break;
            case 14:
                strFieldName = "GetFlag";
                break;
            case 15:
                strFieldName = "Satrap";
                break;
            case 16:
                strFieldName = "EMail";
                break;
            case 17:
                strFieldName = "FoundDate";
                break;
            case 18:
                strFieldName = "BankCode";
                break;
            case 19:
                strFieldName = "BankAccNo";
                break;
            case 20:
                strFieldName = "GrpGroupNo";
                break;
            case 21:
                strFieldName = "BlacklistFlag";
                break;
            case 22:
                strFieldName = "State";
                break;
            case 23:
                strFieldName = "Remark";
                break;
            case 24:
                strFieldName = "VIPValue";
                break;
            case 25:
                strFieldName = "Operator";
                break;
            case 26:
                strFieldName = "MakeDate";
                break;
            case 27:
                strFieldName = "MakeTime";
                break;
            case 28:
                strFieldName = "ModifyDate";
                break;
            case 29:
                strFieldName = "ModifyTime";
                break;
            case 30:
                strFieldName = "SubCompanyFlag";
                break;
            case 31:
                strFieldName = "SupCustoemrNo";
                break;
            case 32:
                strFieldName = "LevelCode";
                break;
            case 33:
                strFieldName = "OnWorkPeoples";
                break;
            case 34:
                strFieldName = "OffWorkPeoples";
                break;
            case 35:
                strFieldName = "OtherPeoples";
                break;
            case 36:
                strFieldName = "BusinessBigType";
                break;
            case 37:
                strFieldName = "SocialInsuNo";
                break;
            case 38:
                strFieldName = "BlackListReason";
                break;
            case 39:
                strFieldName = "GrpNamePY";
                break;
            case 40:
                strFieldName = "SearchKeyWord";
                break;
            case 41:
                strFieldName = "CorIDType";
                break;
            case 42:
                strFieldName = "CorID";
                break;
            case 43:
                strFieldName = "CorIDExpiryDate";
                break;
            case 44:
                strFieldName = "ActuCtrl";
                break;
            case 45:
                strFieldName = "License";
                break;
            case 46:
                strFieldName = "OrganizationCode";
                break;
            case 47:
                strFieldName = "TaxCode";
                break;
            case 48:
                strFieldName = "ManageCom";
                break;
            case 49:
                strFieldName = "ComCode";
                break;
            case 50:
                strFieldName = "ModifyOperator";
                break;
            case 51:
                strFieldName = "IDType";
                break;
            case 52:
                strFieldName = "IDNo";
                break;
            case 53:
                strFieldName = "IDExpDate";
                break;
            case 54:
                strFieldName = "Sex";
                break;
            case 55:
                strFieldName = "NativePlace";
                break;
            case 56:
                strFieldName = "Occupation";
                break;
            case 57:
                strFieldName = "OccupationCode";
                break;
            case 58:
                strFieldName = "Email2";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "CUSTOMERNO":
                return Schema.TYPE_STRING;
            case "PASSWORD":
                return Schema.TYPE_STRING;
            case "GRPNAME":
                return Schema.TYPE_STRING;
            case "BUSINESSTYPE":
                return Schema.TYPE_STRING;
            case "GRPNATURE":
                return Schema.TYPE_STRING;
            case "PEOPLES":
                return Schema.TYPE_INT;
            case "RGTMONEY":
                return Schema.TYPE_DOUBLE;
            case "ASSET":
                return Schema.TYPE_DOUBLE;
            case "NETPROFITRATE":
                return Schema.TYPE_DOUBLE;
            case "MAINBUSSINESS":
                return Schema.TYPE_STRING;
            case "CORPORATION":
                return Schema.TYPE_STRING;
            case "COMAERA":
                return Schema.TYPE_STRING;
            case "FAX":
                return Schema.TYPE_STRING;
            case "PHONE":
                return Schema.TYPE_STRING;
            case "GETFLAG":
                return Schema.TYPE_STRING;
            case "SATRAP":
                return Schema.TYPE_STRING;
            case "EMAIL":
                return Schema.TYPE_STRING;
            case "FOUNDDATE":
                return Schema.TYPE_STRING;
            case "BANKCODE":
                return Schema.TYPE_STRING;
            case "BANKACCNO":
                return Schema.TYPE_STRING;
            case "GRPGROUPNO":
                return Schema.TYPE_STRING;
            case "BLACKLISTFLAG":
                return Schema.TYPE_STRING;
            case "STATE":
                return Schema.TYPE_STRING;
            case "REMARK":
                return Schema.TYPE_STRING;
            case "VIPVALUE":
                return Schema.TYPE_STRING;
            case "OPERATOR":
                return Schema.TYPE_STRING;
            case "MAKEDATE":
                return Schema.TYPE_STRING;
            case "MAKETIME":
                return Schema.TYPE_STRING;
            case "MODIFYDATE":
                return Schema.TYPE_STRING;
            case "MODIFYTIME":
                return Schema.TYPE_STRING;
            case "SUBCOMPANYFLAG":
                return Schema.TYPE_STRING;
            case "SUPCUSTOEMRNO":
                return Schema.TYPE_STRING;
            case "LEVELCODE":
                return Schema.TYPE_STRING;
            case "ONWORKPEOPLES":
                return Schema.TYPE_INT;
            case "OFFWORKPEOPLES":
                return Schema.TYPE_INT;
            case "OTHERPEOPLES":
                return Schema.TYPE_INT;
            case "BUSINESSBIGTYPE":
                return Schema.TYPE_STRING;
            case "SOCIALINSUNO":
                return Schema.TYPE_STRING;
            case "BLACKLISTREASON":
                return Schema.TYPE_STRING;
            case "GRPNAMEPY":
                return Schema.TYPE_STRING;
            case "SEARCHKEYWORD":
                return Schema.TYPE_STRING;
            case "CORIDTYPE":
                return Schema.TYPE_STRING;
            case "CORID":
                return Schema.TYPE_STRING;
            case "CORIDEXPIRYDATE":
                return Schema.TYPE_STRING;
            case "ACTUCTRL":
                return Schema.TYPE_STRING;
            case "LICENSE":
                return Schema.TYPE_STRING;
            case "ORGANIZATIONCODE":
                return Schema.TYPE_STRING;
            case "TAXCODE":
                return Schema.TYPE_STRING;
            case "MANAGECOM":
                return Schema.TYPE_STRING;
            case "COMCODE":
                return Schema.TYPE_STRING;
            case "MODIFYOPERATOR":
                return Schema.TYPE_STRING;
            case "IDTYPE":
                return Schema.TYPE_STRING;
            case "IDNO":
                return Schema.TYPE_STRING;
            case "IDEXPDATE":
                return Schema.TYPE_STRING;
            case "SEX":
                return Schema.TYPE_STRING;
            case "NATIVEPLACE":
                return Schema.TYPE_STRING;
            case "OCCUPATION":
                return Schema.TYPE_STRING;
            case "OCCUPATIONCODE":
                return Schema.TYPE_STRING;
            case "EMAIL2":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_STRING;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_INT;
            case 6:
                return Schema.TYPE_DOUBLE;
            case 7:
                return Schema.TYPE_DOUBLE;
            case 8:
                return Schema.TYPE_DOUBLE;
            case 9:
                return Schema.TYPE_STRING;
            case 10:
                return Schema.TYPE_STRING;
            case 11:
                return Schema.TYPE_STRING;
            case 12:
                return Schema.TYPE_STRING;
            case 13:
                return Schema.TYPE_STRING;
            case 14:
                return Schema.TYPE_STRING;
            case 15:
                return Schema.TYPE_STRING;
            case 16:
                return Schema.TYPE_STRING;
            case 17:
                return Schema.TYPE_STRING;
            case 18:
                return Schema.TYPE_STRING;
            case 19:
                return Schema.TYPE_STRING;
            case 20:
                return Schema.TYPE_STRING;
            case 21:
                return Schema.TYPE_STRING;
            case 22:
                return Schema.TYPE_STRING;
            case 23:
                return Schema.TYPE_STRING;
            case 24:
                return Schema.TYPE_STRING;
            case 25:
                return Schema.TYPE_STRING;
            case 26:
                return Schema.TYPE_STRING;
            case 27:
                return Schema.TYPE_STRING;
            case 28:
                return Schema.TYPE_STRING;
            case 29:
                return Schema.TYPE_STRING;
            case 30:
                return Schema.TYPE_STRING;
            case 31:
                return Schema.TYPE_STRING;
            case 32:
                return Schema.TYPE_STRING;
            case 33:
                return Schema.TYPE_INT;
            case 34:
                return Schema.TYPE_INT;
            case 35:
                return Schema.TYPE_INT;
            case 36:
                return Schema.TYPE_STRING;
            case 37:
                return Schema.TYPE_STRING;
            case 38:
                return Schema.TYPE_STRING;
            case 39:
                return Schema.TYPE_STRING;
            case 40:
                return Schema.TYPE_STRING;
            case 41:
                return Schema.TYPE_STRING;
            case 42:
                return Schema.TYPE_STRING;
            case 43:
                return Schema.TYPE_STRING;
            case 44:
                return Schema.TYPE_STRING;
            case 45:
                return Schema.TYPE_STRING;
            case 46:
                return Schema.TYPE_STRING;
            case 47:
                return Schema.TYPE_STRING;
            case 48:
                return Schema.TYPE_STRING;
            case 49:
                return Schema.TYPE_STRING;
            case 50:
                return Schema.TYPE_STRING;
            case 51:
                return Schema.TYPE_STRING;
            case 52:
                return Schema.TYPE_STRING;
            case 53:
                return Schema.TYPE_STRING;
            case 54:
                return Schema.TYPE_STRING;
            case 55:
                return Schema.TYPE_STRING;
            case 56:
                return Schema.TYPE_STRING;
            case 57:
                return Schema.TYPE_STRING;
            case 58:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("CustomerNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CustomerNo));
        }
        if (FCode.equalsIgnoreCase("Password")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Password));
        }
        if (FCode.equalsIgnoreCase("GrpName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GrpName));
        }
        if (FCode.equalsIgnoreCase("BusinessType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BusinessType));
        }
        if (FCode.equalsIgnoreCase("GrpNature")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GrpNature));
        }
        if (FCode.equalsIgnoreCase("Peoples")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Peoples));
        }
        if (FCode.equalsIgnoreCase("RgtMoney")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RgtMoney));
        }
        if (FCode.equalsIgnoreCase("Asset")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Asset));
        }
        if (FCode.equalsIgnoreCase("NetProfitRate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(NetProfitRate));
        }
        if (FCode.equalsIgnoreCase("MainBussiness")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MainBussiness));
        }
        if (FCode.equalsIgnoreCase("Corporation")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Corporation));
        }
        if (FCode.equalsIgnoreCase("ComAera")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ComAera));
        }
        if (FCode.equalsIgnoreCase("Fax")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Fax));
        }
        if (FCode.equalsIgnoreCase("Phone")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Phone));
        }
        if (FCode.equalsIgnoreCase("GetFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GetFlag));
        }
        if (FCode.equalsIgnoreCase("Satrap")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Satrap));
        }
        if (FCode.equalsIgnoreCase("EMail")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(EMail));
        }
        if (FCode.equalsIgnoreCase("FoundDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FoundDate));
        }
        if (FCode.equalsIgnoreCase("BankCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BankCode));
        }
        if (FCode.equalsIgnoreCase("BankAccNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BankAccNo));
        }
        if (FCode.equalsIgnoreCase("GrpGroupNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GrpGroupNo));
        }
        if (FCode.equalsIgnoreCase("BlacklistFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BlacklistFlag));
        }
        if (FCode.equalsIgnoreCase("State")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(State));
        }
        if (FCode.equalsIgnoreCase("Remark")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Remark));
        }
        if (FCode.equalsIgnoreCase("VIPValue")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(VIPValue));
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeDate));
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyDate));
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (FCode.equalsIgnoreCase("SubCompanyFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SubCompanyFlag));
        }
        if (FCode.equalsIgnoreCase("SupCustoemrNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SupCustoemrNo));
        }
        if (FCode.equalsIgnoreCase("LevelCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(LevelCode));
        }
        if (FCode.equalsIgnoreCase("OnWorkPeoples")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OnWorkPeoples));
        }
        if (FCode.equalsIgnoreCase("OffWorkPeoples")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OffWorkPeoples));
        }
        if (FCode.equalsIgnoreCase("OtherPeoples")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OtherPeoples));
        }
        if (FCode.equalsIgnoreCase("BusinessBigType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BusinessBigType));
        }
        if (FCode.equalsIgnoreCase("SocialInsuNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SocialInsuNo));
        }
        if (FCode.equalsIgnoreCase("BlackListReason")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BlackListReason));
        }
        if (FCode.equalsIgnoreCase("GrpNamePY")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GrpNamePY));
        }
        if (FCode.equalsIgnoreCase("SearchKeyWord")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SearchKeyWord));
        }
        if (FCode.equalsIgnoreCase("CorIDType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CorIDType));
        }
        if (FCode.equalsIgnoreCase("CorID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CorID));
        }
        if (FCode.equalsIgnoreCase("CorIDExpiryDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CorIDExpiryDate));
        }
        if (FCode.equalsIgnoreCase("ActuCtrl")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ActuCtrl));
        }
        if (FCode.equalsIgnoreCase("License")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(License));
        }
        if (FCode.equalsIgnoreCase("OrganizationCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OrganizationCode));
        }
        if (FCode.equalsIgnoreCase("TaxCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(TaxCode));
        }
        if (FCode.equalsIgnoreCase("ManageCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
        }
        if (FCode.equalsIgnoreCase("ComCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ComCode));
        }
        if (FCode.equalsIgnoreCase("ModifyOperator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyOperator));
        }
        if (FCode.equalsIgnoreCase("IDType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(IDType));
        }
        if (FCode.equalsIgnoreCase("IDNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(IDNo));
        }
        if (FCode.equalsIgnoreCase("IDExpDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(IDExpDate));
        }
        if (FCode.equalsIgnoreCase("Sex")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Sex));
        }
        if (FCode.equalsIgnoreCase("NativePlace")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(NativePlace));
        }
        if (FCode.equalsIgnoreCase("Occupation")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Occupation));
        }
        if (FCode.equalsIgnoreCase("OccupationCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OccupationCode));
        }
        if (FCode.equalsIgnoreCase("Email2")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Email2));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = String.valueOf(CustomerNo);
                break;
            case 1:
                strFieldValue = String.valueOf(Password);
                break;
            case 2:
                strFieldValue = String.valueOf(GrpName);
                break;
            case 3:
                strFieldValue = String.valueOf(BusinessType);
                break;
            case 4:
                strFieldValue = String.valueOf(GrpNature);
                break;
            case 5:
                strFieldValue = String.valueOf(Peoples);
                break;
            case 6:
                strFieldValue = String.valueOf(RgtMoney);
                break;
            case 7:
                strFieldValue = String.valueOf(Asset);
                break;
            case 8:
                strFieldValue = String.valueOf(NetProfitRate);
                break;
            case 9:
                strFieldValue = String.valueOf(MainBussiness);
                break;
            case 10:
                strFieldValue = String.valueOf(Corporation);
                break;
            case 11:
                strFieldValue = String.valueOf(ComAera);
                break;
            case 12:
                strFieldValue = String.valueOf(Fax);
                break;
            case 13:
                strFieldValue = String.valueOf(Phone);
                break;
            case 14:
                strFieldValue = String.valueOf(GetFlag);
                break;
            case 15:
                strFieldValue = String.valueOf(Satrap);
                break;
            case 16:
                strFieldValue = String.valueOf(EMail);
                break;
            case 17:
                strFieldValue = String.valueOf(FoundDate);
                break;
            case 18:
                strFieldValue = String.valueOf(BankCode);
                break;
            case 19:
                strFieldValue = String.valueOf(BankAccNo);
                break;
            case 20:
                strFieldValue = String.valueOf(GrpGroupNo);
                break;
            case 21:
                strFieldValue = String.valueOf(BlacklistFlag);
                break;
            case 22:
                strFieldValue = String.valueOf(State);
                break;
            case 23:
                strFieldValue = String.valueOf(Remark);
                break;
            case 24:
                strFieldValue = String.valueOf(VIPValue);
                break;
            case 25:
                strFieldValue = String.valueOf(Operator);
                break;
            case 26:
                strFieldValue = String.valueOf(MakeDate);
                break;
            case 27:
                strFieldValue = String.valueOf(MakeTime);
                break;
            case 28:
                strFieldValue = String.valueOf(ModifyDate);
                break;
            case 29:
                strFieldValue = String.valueOf(ModifyTime);
                break;
            case 30:
                strFieldValue = String.valueOf(SubCompanyFlag);
                break;
            case 31:
                strFieldValue = String.valueOf(SupCustoemrNo);
                break;
            case 32:
                strFieldValue = String.valueOf(LevelCode);
                break;
            case 33:
                strFieldValue = String.valueOf(OnWorkPeoples);
                break;
            case 34:
                strFieldValue = String.valueOf(OffWorkPeoples);
                break;
            case 35:
                strFieldValue = String.valueOf(OtherPeoples);
                break;
            case 36:
                strFieldValue = String.valueOf(BusinessBigType);
                break;
            case 37:
                strFieldValue = String.valueOf(SocialInsuNo);
                break;
            case 38:
                strFieldValue = String.valueOf(BlackListReason);
                break;
            case 39:
                strFieldValue = String.valueOf(GrpNamePY);
                break;
            case 40:
                strFieldValue = String.valueOf(SearchKeyWord);
                break;
            case 41:
                strFieldValue = String.valueOf(CorIDType);
                break;
            case 42:
                strFieldValue = String.valueOf(CorID);
                break;
            case 43:
                strFieldValue = String.valueOf(CorIDExpiryDate);
                break;
            case 44:
                strFieldValue = String.valueOf(ActuCtrl);
                break;
            case 45:
                strFieldValue = String.valueOf(License);
                break;
            case 46:
                strFieldValue = String.valueOf(OrganizationCode);
                break;
            case 47:
                strFieldValue = String.valueOf(TaxCode);
                break;
            case 48:
                strFieldValue = String.valueOf(ManageCom);
                break;
            case 49:
                strFieldValue = String.valueOf(ComCode);
                break;
            case 50:
                strFieldValue = String.valueOf(ModifyOperator);
                break;
            case 51:
                strFieldValue = String.valueOf(IDType);
                break;
            case 52:
                strFieldValue = String.valueOf(IDNo);
                break;
            case 53:
                strFieldValue = String.valueOf(IDExpDate);
                break;
            case 54:
                strFieldValue = String.valueOf(Sex);
                break;
            case 55:
                strFieldValue = String.valueOf(NativePlace);
                break;
            case 56:
                strFieldValue = String.valueOf(Occupation);
                break;
            case 57:
                strFieldValue = String.valueOf(OccupationCode);
                break;
            case 58:
                strFieldValue = String.valueOf(Email2);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("CustomerNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                CustomerNo = FValue.trim();
            }
            else
                CustomerNo = null;
        }
        if (FCode.equalsIgnoreCase("Password")) {
            if( FValue != null && !FValue.equals(""))
            {
                Password = FValue.trim();
            }
            else
                Password = null;
        }
        if (FCode.equalsIgnoreCase("GrpName")) {
            if( FValue != null && !FValue.equals(""))
            {
                GrpName = FValue.trim();
            }
            else
                GrpName = null;
        }
        if (FCode.equalsIgnoreCase("BusinessType")) {
            if( FValue != null && !FValue.equals(""))
            {
                BusinessType = FValue.trim();
            }
            else
                BusinessType = null;
        }
        if (FCode.equalsIgnoreCase("GrpNature")) {
            if( FValue != null && !FValue.equals(""))
            {
                GrpNature = FValue.trim();
            }
            else
                GrpNature = null;
        }
        if (FCode.equalsIgnoreCase("Peoples")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                Peoples = i;
            }
        }
        if (FCode.equalsIgnoreCase("RgtMoney")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                RgtMoney = d;
            }
        }
        if (FCode.equalsIgnoreCase("Asset")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                Asset = d;
            }
        }
        if (FCode.equalsIgnoreCase("NetProfitRate")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                NetProfitRate = d;
            }
        }
        if (FCode.equalsIgnoreCase("MainBussiness")) {
            if( FValue != null && !FValue.equals(""))
            {
                MainBussiness = FValue.trim();
            }
            else
                MainBussiness = null;
        }
        if (FCode.equalsIgnoreCase("Corporation")) {
            if( FValue != null && !FValue.equals(""))
            {
                Corporation = FValue.trim();
            }
            else
                Corporation = null;
        }
        if (FCode.equalsIgnoreCase("ComAera")) {
            if( FValue != null && !FValue.equals(""))
            {
                ComAera = FValue.trim();
            }
            else
                ComAera = null;
        }
        if (FCode.equalsIgnoreCase("Fax")) {
            if( FValue != null && !FValue.equals(""))
            {
                Fax = FValue.trim();
            }
            else
                Fax = null;
        }
        if (FCode.equalsIgnoreCase("Phone")) {
            if( FValue != null && !FValue.equals(""))
            {
                Phone = FValue.trim();
            }
            else
                Phone = null;
        }
        if (FCode.equalsIgnoreCase("GetFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                GetFlag = FValue.trim();
            }
            else
                GetFlag = null;
        }
        if (FCode.equalsIgnoreCase("Satrap")) {
            if( FValue != null && !FValue.equals(""))
            {
                Satrap = FValue.trim();
            }
            else
                Satrap = null;
        }
        if (FCode.equalsIgnoreCase("EMail")) {
            if( FValue != null && !FValue.equals(""))
            {
                EMail = FValue.trim();
            }
            else
                EMail = null;
        }
        if (FCode.equalsIgnoreCase("FoundDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                FoundDate = FValue.trim();
            }
            else
                FoundDate = null;
        }
        if (FCode.equalsIgnoreCase("BankCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                BankCode = FValue.trim();
            }
            else
                BankCode = null;
        }
        if (FCode.equalsIgnoreCase("BankAccNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                BankAccNo = FValue.trim();
            }
            else
                BankAccNo = null;
        }
        if (FCode.equalsIgnoreCase("GrpGroupNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                GrpGroupNo = FValue.trim();
            }
            else
                GrpGroupNo = null;
        }
        if (FCode.equalsIgnoreCase("BlacklistFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                BlacklistFlag = FValue.trim();
            }
            else
                BlacklistFlag = null;
        }
        if (FCode.equalsIgnoreCase("State")) {
            if( FValue != null && !FValue.equals(""))
            {
                State = FValue.trim();
            }
            else
                State = null;
        }
        if (FCode.equalsIgnoreCase("Remark")) {
            if( FValue != null && !FValue.equals(""))
            {
                Remark = FValue.trim();
            }
            else
                Remark = null;
        }
        if (FCode.equalsIgnoreCase("VIPValue")) {
            if( FValue != null && !FValue.equals(""))
            {
                VIPValue = FValue.trim();
            }
            else
                VIPValue = null;
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            if( FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
                Operator = null;
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeDate = FValue.trim();
            }
            else
                MakeDate = null;
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
                MakeTime = null;
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyDate = FValue.trim();
            }
            else
                ModifyDate = null;
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
                ModifyTime = null;
        }
        if (FCode.equalsIgnoreCase("SubCompanyFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                SubCompanyFlag = FValue.trim();
            }
            else
                SubCompanyFlag = null;
        }
        if (FCode.equalsIgnoreCase("SupCustoemrNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                SupCustoemrNo = FValue.trim();
            }
            else
                SupCustoemrNo = null;
        }
        if (FCode.equalsIgnoreCase("LevelCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                LevelCode = FValue.trim();
            }
            else
                LevelCode = null;
        }
        if (FCode.equalsIgnoreCase("OnWorkPeoples")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                OnWorkPeoples = i;
            }
        }
        if (FCode.equalsIgnoreCase("OffWorkPeoples")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                OffWorkPeoples = i;
            }
        }
        if (FCode.equalsIgnoreCase("OtherPeoples")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                OtherPeoples = i;
            }
        }
        if (FCode.equalsIgnoreCase("BusinessBigType")) {
            if( FValue != null && !FValue.equals(""))
            {
                BusinessBigType = FValue.trim();
            }
            else
                BusinessBigType = null;
        }
        if (FCode.equalsIgnoreCase("SocialInsuNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                SocialInsuNo = FValue.trim();
            }
            else
                SocialInsuNo = null;
        }
        if (FCode.equalsIgnoreCase("BlackListReason")) {
            if( FValue != null && !FValue.equals(""))
            {
                BlackListReason = FValue.trim();
            }
            else
                BlackListReason = null;
        }
        if (FCode.equalsIgnoreCase("GrpNamePY")) {
            if( FValue != null && !FValue.equals(""))
            {
                GrpNamePY = FValue.trim();
            }
            else
                GrpNamePY = null;
        }
        if (FCode.equalsIgnoreCase("SearchKeyWord")) {
            if( FValue != null && !FValue.equals(""))
            {
                SearchKeyWord = FValue.trim();
            }
            else
                SearchKeyWord = null;
        }
        if (FCode.equalsIgnoreCase("CorIDType")) {
            if( FValue != null && !FValue.equals(""))
            {
                CorIDType = FValue.trim();
            }
            else
                CorIDType = null;
        }
        if (FCode.equalsIgnoreCase("CorID")) {
            if( FValue != null && !FValue.equals(""))
            {
                CorID = FValue.trim();
            }
            else
                CorID = null;
        }
        if (FCode.equalsIgnoreCase("CorIDExpiryDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                CorIDExpiryDate = FValue.trim();
            }
            else
                CorIDExpiryDate = null;
        }
        if (FCode.equalsIgnoreCase("ActuCtrl")) {
            if( FValue != null && !FValue.equals(""))
            {
                ActuCtrl = FValue.trim();
            }
            else
                ActuCtrl = null;
        }
        if (FCode.equalsIgnoreCase("License")) {
            if( FValue != null && !FValue.equals(""))
            {
                License = FValue.trim();
            }
            else
                License = null;
        }
        if (FCode.equalsIgnoreCase("OrganizationCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                OrganizationCode = FValue.trim();
            }
            else
                OrganizationCode = null;
        }
        if (FCode.equalsIgnoreCase("TaxCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                TaxCode = FValue.trim();
            }
            else
                TaxCode = null;
        }
        if (FCode.equalsIgnoreCase("ManageCom")) {
            if( FValue != null && !FValue.equals(""))
            {
                ManageCom = FValue.trim();
            }
            else
                ManageCom = null;
        }
        if (FCode.equalsIgnoreCase("ComCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                ComCode = FValue.trim();
            }
            else
                ComCode = null;
        }
        if (FCode.equalsIgnoreCase("ModifyOperator")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyOperator = FValue.trim();
            }
            else
                ModifyOperator = null;
        }
        if (FCode.equalsIgnoreCase("IDType")) {
            if( FValue != null && !FValue.equals(""))
            {
                IDType = FValue.trim();
            }
            else
                IDType = null;
        }
        if (FCode.equalsIgnoreCase("IDNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                IDNo = FValue.trim();
            }
            else
                IDNo = null;
        }
        if (FCode.equalsIgnoreCase("IDExpDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                IDExpDate = FValue.trim();
            }
            else
                IDExpDate = null;
        }
        if (FCode.equalsIgnoreCase("Sex")) {
            if( FValue != null && !FValue.equals(""))
            {
                Sex = FValue.trim();
            }
            else
                Sex = null;
        }
        if (FCode.equalsIgnoreCase("NativePlace")) {
            if( FValue != null && !FValue.equals(""))
            {
                NativePlace = FValue.trim();
            }
            else
                NativePlace = null;
        }
        if (FCode.equalsIgnoreCase("Occupation")) {
            if( FValue != null && !FValue.equals(""))
            {
                Occupation = FValue.trim();
            }
            else
                Occupation = null;
        }
        if (FCode.equalsIgnoreCase("OccupationCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                OccupationCode = FValue.trim();
            }
            else
                OccupationCode = null;
        }
        if (FCode.equalsIgnoreCase("Email2")) {
            if( FValue != null && !FValue.equals(""))
            {
                Email2 = FValue.trim();
            }
            else
                Email2 = null;
        }
        return true;
    }


    public String toString() {
    return "LBGrpPojo [" +
            "CustomerNo="+CustomerNo +
            ", Password="+Password +
            ", GrpName="+GrpName +
            ", BusinessType="+BusinessType +
            ", GrpNature="+GrpNature +
            ", Peoples="+Peoples +
            ", RgtMoney="+RgtMoney +
            ", Asset="+Asset +
            ", NetProfitRate="+NetProfitRate +
            ", MainBussiness="+MainBussiness +
            ", Corporation="+Corporation +
            ", ComAera="+ComAera +
            ", Fax="+Fax +
            ", Phone="+Phone +
            ", GetFlag="+GetFlag +
            ", Satrap="+Satrap +
            ", EMail="+EMail +
            ", FoundDate="+FoundDate +
            ", BankCode="+BankCode +
            ", BankAccNo="+BankAccNo +
            ", GrpGroupNo="+GrpGroupNo +
            ", BlacklistFlag="+BlacklistFlag +
            ", State="+State +
            ", Remark="+Remark +
            ", VIPValue="+VIPValue +
            ", Operator="+Operator +
            ", MakeDate="+MakeDate +
            ", MakeTime="+MakeTime +
            ", ModifyDate="+ModifyDate +
            ", ModifyTime="+ModifyTime +
            ", SubCompanyFlag="+SubCompanyFlag +
            ", SupCustoemrNo="+SupCustoemrNo +
            ", LevelCode="+LevelCode +
            ", OnWorkPeoples="+OnWorkPeoples +
            ", OffWorkPeoples="+OffWorkPeoples +
            ", OtherPeoples="+OtherPeoples +
            ", BusinessBigType="+BusinessBigType +
            ", SocialInsuNo="+SocialInsuNo +
            ", BlackListReason="+BlackListReason +
            ", GrpNamePY="+GrpNamePY +
            ", SearchKeyWord="+SearchKeyWord +
            ", CorIDType="+CorIDType +
            ", CorID="+CorID +
            ", CorIDExpiryDate="+CorIDExpiryDate +
            ", ActuCtrl="+ActuCtrl +
            ", License="+License +
            ", OrganizationCode="+OrganizationCode +
            ", TaxCode="+TaxCode +
            ", ManageCom="+ManageCom +
            ", ComCode="+ComCode +
            ", ModifyOperator="+ModifyOperator +
            ", IDType="+IDType +
            ", IDNo="+IDNo +
            ", IDExpDate="+IDExpDate +
            ", Sex="+Sex +
            ", NativePlace="+NativePlace +
            ", Occupation="+Occupation +
            ", OccupationCode="+OccupationCode +
            ", Email2="+Email2 +"]";
    }
}
