/**
 * Copyright (c) 2017 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.entity;

import com.sinosoft.utility.Pojo;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;

import java.io.Serializable;

/**
 * <p>ClassName: LMDutyGetClmCalPojo </p>
 * <p>Copyright: Copyright (c) 2017</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2017-10-17
 */
public class LMDutyGetClmCalPojo implements Pojo,Serializable {
    // @Field
    /** 给付代码 */
    private String GetDutyCode; 
    /** 给付名称 */
    private String GetDutyName; 
    /** 给付责任类型 */
    private String GetDutyKind; 
    /** 算法1 */
    private String CalCode1; 
    /** 算法2 */
    private String CalCode2; 
    /** 算法3 */
    private String CalCode3; 
    /** 算法4 */
    private String CalCode4; 
    /** 算法5 */
    private String CalCode5; 
    /** 算法6 */
    private String CalCode6; 
    /** 算法7 */
    private String CalCode7; 
    /** 算法8 */
    private String CalCode8; 
    /** 算法9 */
    private String CalCode9; 
    /** 算法10 */
    private String CalCode10; 


    public static final int FIELDNUM = 13;    // 数据库表的字段个数
    public String getGetDutyCode() {
        return GetDutyCode;
    }
    public void setGetDutyCode(String aGetDutyCode) {
        GetDutyCode = aGetDutyCode;
    }
    public String getGetDutyName() {
        return GetDutyName;
    }
    public void setGetDutyName(String aGetDutyName) {
        GetDutyName = aGetDutyName;
    }
    public String getGetDutyKind() {
        return GetDutyKind;
    }
    public void setGetDutyKind(String aGetDutyKind) {
        GetDutyKind = aGetDutyKind;
    }
    public String getCalCode1() {
        return CalCode1;
    }
    public void setCalCode1(String aCalCode1) {
        CalCode1 = aCalCode1;
    }
    public String getCalCode2() {
        return CalCode2;
    }
    public void setCalCode2(String aCalCode2) {
        CalCode2 = aCalCode2;
    }
    public String getCalCode3() {
        return CalCode3;
    }
    public void setCalCode3(String aCalCode3) {
        CalCode3 = aCalCode3;
    }
    public String getCalCode4() {
        return CalCode4;
    }
    public void setCalCode4(String aCalCode4) {
        CalCode4 = aCalCode4;
    }
    public String getCalCode5() {
        return CalCode5;
    }
    public void setCalCode5(String aCalCode5) {
        CalCode5 = aCalCode5;
    }
    public String getCalCode6() {
        return CalCode6;
    }
    public void setCalCode6(String aCalCode6) {
        CalCode6 = aCalCode6;
    }
    public String getCalCode7() {
        return CalCode7;
    }
    public void setCalCode7(String aCalCode7) {
        CalCode7 = aCalCode7;
    }
    public String getCalCode8() {
        return CalCode8;
    }
    public void setCalCode8(String aCalCode8) {
        CalCode8 = aCalCode8;
    }
    public String getCalCode9() {
        return CalCode9;
    }
    public void setCalCode9(String aCalCode9) {
        CalCode9 = aCalCode9;
    }
    public String getCalCode10() {
        return CalCode10;
    }
    public void setCalCode10(String aCalCode10) {
        CalCode10 = aCalCode10;
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("GetDutyCode") ) {
            return 0;
        }
        if( strFieldName.equals("GetDutyName") ) {
            return 1;
        }
        if( strFieldName.equals("GetDutyKind") ) {
            return 2;
        }
        if( strFieldName.equals("CalCode1") ) {
            return 3;
        }
        if( strFieldName.equals("CalCode2") ) {
            return 4;
        }
        if( strFieldName.equals("CalCode3") ) {
            return 5;
        }
        if( strFieldName.equals("CalCode4") ) {
            return 6;
        }
        if( strFieldName.equals("CalCode5") ) {
            return 7;
        }
        if( strFieldName.equals("CalCode6") ) {
            return 8;
        }
        if( strFieldName.equals("CalCode7") ) {
            return 9;
        }
        if( strFieldName.equals("CalCode8") ) {
            return 10;
        }
        if( strFieldName.equals("CalCode9") ) {
            return 11;
        }
        if( strFieldName.equals("CalCode10") ) {
            return 12;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "GetDutyCode";
                break;
            case 1:
                strFieldName = "GetDutyName";
                break;
            case 2:
                strFieldName = "GetDutyKind";
                break;
            case 3:
                strFieldName = "CalCode1";
                break;
            case 4:
                strFieldName = "CalCode2";
                break;
            case 5:
                strFieldName = "CalCode3";
                break;
            case 6:
                strFieldName = "CalCode4";
                break;
            case 7:
                strFieldName = "CalCode5";
                break;
            case 8:
                strFieldName = "CalCode6";
                break;
            case 9:
                strFieldName = "CalCode7";
                break;
            case 10:
                strFieldName = "CalCode8";
                break;
            case 11:
                strFieldName = "CalCode9";
                break;
            case 12:
                strFieldName = "CalCode10";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "GETDUTYCODE":
                return Schema.TYPE_STRING;
            case "GETDUTYNAME":
                return Schema.TYPE_STRING;
            case "GETDUTYKIND":
                return Schema.TYPE_STRING;
            case "CALCODE1":
                return Schema.TYPE_STRING;
            case "CALCODE2":
                return Schema.TYPE_STRING;
            case "CALCODE3":
                return Schema.TYPE_STRING;
            case "CALCODE4":
                return Schema.TYPE_STRING;
            case "CALCODE5":
                return Schema.TYPE_STRING;
            case "CALCODE6":
                return Schema.TYPE_STRING;
            case "CALCODE7":
                return Schema.TYPE_STRING;
            case "CALCODE8":
                return Schema.TYPE_STRING;
            case "CALCODE9":
                return Schema.TYPE_STRING;
            case "CALCODE10":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_STRING;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_STRING;
            case 6:
                return Schema.TYPE_STRING;
            case 7:
                return Schema.TYPE_STRING;
            case 8:
                return Schema.TYPE_STRING;
            case 9:
                return Schema.TYPE_STRING;
            case 10:
                return Schema.TYPE_STRING;
            case 11:
                return Schema.TYPE_STRING;
            case 12:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("GetDutyCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GetDutyCode));
        }
        if (FCode.equalsIgnoreCase("GetDutyName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GetDutyName));
        }
        if (FCode.equalsIgnoreCase("GetDutyKind")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GetDutyKind));
        }
        if (FCode.equalsIgnoreCase("CalCode1")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CalCode1));
        }
        if (FCode.equalsIgnoreCase("CalCode2")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CalCode2));
        }
        if (FCode.equalsIgnoreCase("CalCode3")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CalCode3));
        }
        if (FCode.equalsIgnoreCase("CalCode4")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CalCode4));
        }
        if (FCode.equalsIgnoreCase("CalCode5")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CalCode5));
        }
        if (FCode.equalsIgnoreCase("CalCode6")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CalCode6));
        }
        if (FCode.equalsIgnoreCase("CalCode7")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CalCode7));
        }
        if (FCode.equalsIgnoreCase("CalCode8")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CalCode8));
        }
        if (FCode.equalsIgnoreCase("CalCode9")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CalCode9));
        }
        if (FCode.equalsIgnoreCase("CalCode10")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CalCode10));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = String.valueOf(GetDutyCode);
                break;
            case 1:
                strFieldValue = String.valueOf(GetDutyName);
                break;
            case 2:
                strFieldValue = String.valueOf(GetDutyKind);
                break;
            case 3:
                strFieldValue = String.valueOf(CalCode1);
                break;
            case 4:
                strFieldValue = String.valueOf(CalCode2);
                break;
            case 5:
                strFieldValue = String.valueOf(CalCode3);
                break;
            case 6:
                strFieldValue = String.valueOf(CalCode4);
                break;
            case 7:
                strFieldValue = String.valueOf(CalCode5);
                break;
            case 8:
                strFieldValue = String.valueOf(CalCode6);
                break;
            case 9:
                strFieldValue = String.valueOf(CalCode7);
                break;
            case 10:
                strFieldValue = String.valueOf(CalCode8);
                break;
            case 11:
                strFieldValue = String.valueOf(CalCode9);
                break;
            case 12:
                strFieldValue = String.valueOf(CalCode10);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("GetDutyCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                GetDutyCode = FValue.trim();
            }
            else
                GetDutyCode = null;
        }
        if (FCode.equalsIgnoreCase("GetDutyName")) {
            if( FValue != null && !FValue.equals(""))
            {
                GetDutyName = FValue.trim();
            }
            else
                GetDutyName = null;
        }
        if (FCode.equalsIgnoreCase("GetDutyKind")) {
            if( FValue != null && !FValue.equals(""))
            {
                GetDutyKind = FValue.trim();
            }
            else
                GetDutyKind = null;
        }
        if (FCode.equalsIgnoreCase("CalCode1")) {
            if( FValue != null && !FValue.equals(""))
            {
                CalCode1 = FValue.trim();
            }
            else
                CalCode1 = null;
        }
        if (FCode.equalsIgnoreCase("CalCode2")) {
            if( FValue != null && !FValue.equals(""))
            {
                CalCode2 = FValue.trim();
            }
            else
                CalCode2 = null;
        }
        if (FCode.equalsIgnoreCase("CalCode3")) {
            if( FValue != null && !FValue.equals(""))
            {
                CalCode3 = FValue.trim();
            }
            else
                CalCode3 = null;
        }
        if (FCode.equalsIgnoreCase("CalCode4")) {
            if( FValue != null && !FValue.equals(""))
            {
                CalCode4 = FValue.trim();
            }
            else
                CalCode4 = null;
        }
        if (FCode.equalsIgnoreCase("CalCode5")) {
            if( FValue != null && !FValue.equals(""))
            {
                CalCode5 = FValue.trim();
            }
            else
                CalCode5 = null;
        }
        if (FCode.equalsIgnoreCase("CalCode6")) {
            if( FValue != null && !FValue.equals(""))
            {
                CalCode6 = FValue.trim();
            }
            else
                CalCode6 = null;
        }
        if (FCode.equalsIgnoreCase("CalCode7")) {
            if( FValue != null && !FValue.equals(""))
            {
                CalCode7 = FValue.trim();
            }
            else
                CalCode7 = null;
        }
        if (FCode.equalsIgnoreCase("CalCode8")) {
            if( FValue != null && !FValue.equals(""))
            {
                CalCode8 = FValue.trim();
            }
            else
                CalCode8 = null;
        }
        if (FCode.equalsIgnoreCase("CalCode9")) {
            if( FValue != null && !FValue.equals(""))
            {
                CalCode9 = FValue.trim();
            }
            else
                CalCode9 = null;
        }
        if (FCode.equalsIgnoreCase("CalCode10")) {
            if( FValue != null && !FValue.equals(""))
            {
                CalCode10 = FValue.trim();
            }
            else
                CalCode10 = null;
        }
        return true;
    }


    public String toString() {
    return "LMDutyGetClmCalPojo [" +
            "GetDutyCode="+GetDutyCode +
            ", GetDutyName="+GetDutyName +
            ", GetDutyKind="+GetDutyKind +
            ", CalCode1="+CalCode1 +
            ", CalCode2="+CalCode2 +
            ", CalCode3="+CalCode3 +
            ", CalCode4="+CalCode4 +
            ", CalCode5="+CalCode5 +
            ", CalCode6="+CalCode6 +
            ", CalCode7="+CalCode7 +
            ", CalCode8="+CalCode8 +
            ", CalCode9="+CalCode9 +
            ", CalCode10="+CalCode10 +"]";
    }
}
