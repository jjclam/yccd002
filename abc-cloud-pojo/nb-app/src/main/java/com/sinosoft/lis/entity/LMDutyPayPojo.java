/**
 * Copyright (c) 2019 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.entity;

import java.io.*;
import java.util.Date;

import com.sinosoft.cloud.cache.annotation.RedisPrimaryHKey;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.StrTool;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * <p>ClassName: LMDutyPayPojo </p>
 * <p>Copyright: Copyright (c) 2019</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2019-10-16
 */
public class LMDutyPayPojo implements  Pojo,Serializable {

    // @Field
    /** 缴费编码 */
    @RedisPrimaryHKey
    private String PayPlanCode; 
    /** 缴费名称 */
    private String PayPlanName; 
    /** 缴费类型 */
    private String Type; 
    /** 交费间隔 */
    private int PayIntv; 
    /** 缴费终止期间单位 */
    private String PayEndYearFlag; 
    /** 缴费终止期间 */
    private int PayEndYear; 
    /** 缴费终止日期计算参照 */
    private String PayEndDateCalRef; 
    /** 缴费终止日期计算方式 */
    private String PayEndDateCalMode; 
    /** 默认值 */
    private double DefaultVal; 
    /** 算法 */
    private String CalCode; 
    /** 反算算法 */
    private String CnterCalCode; 
    /** 其他算法 */
    private String OthCalCode; 
    /** 保费分配比例 */
    private double Rate; 
    /** 最低限额 */
    private double MinPay; 
    /** 保证收益率 */
    private double AssuYield; 
    /** 提取管理费比例 */
    private double FeeRate; 
    /** 缴至日期计算方法 */
    private String PayToDateCalMode; 
    /** 催缴标记 */
    private String UrgePayFlag; 
    /** 部分缴费标记 */
    private String PayLackFlag; 
    /** 挂帐标记 */
    private String PayOverFlag; 
    /** 挂帐处理 */
    private String PayOverDeal; 
    /** 免交标记 */
    private String AvoidPayFlag; 
    /** 缴费宽限期 */
    private int GracePeriod; 
    /** 公用标记 */
    private String PubFlag; 
    /** 是否允许零值标记 */
    private String ZeroFlag; 
    /** 是否和账户相关 */
    private String NeedAcc; 
    /** 交费目的分类 */
    private String PayAimClass; 
    /** 交费起始期间 */
    private int PayStartYear; 
    /** 交费起始期间单位 */
    private String PayStartYearFlag; 
    /** 交费起始日期计算参照 */
    private String PayStartDateCalRef; 
    /** 交费起始日期计算方式 */
    private String PayStartDateCalMode; 


    public static final int FIELDNUM = 31;    // 数据库表的字段个数
    public String getPayPlanCode() {
        return PayPlanCode;
    }
    public void setPayPlanCode(String aPayPlanCode) {
        PayPlanCode = aPayPlanCode;
    }
    public String getPayPlanName() {
        return PayPlanName;
    }
    public void setPayPlanName(String aPayPlanName) {
        PayPlanName = aPayPlanName;
    }
    public String getType() {
        return Type;
    }
    public void setType(String aType) {
        Type = aType;
    }
    public int getPayIntv() {
        return PayIntv;
    }
    public void setPayIntv(int aPayIntv) {
        PayIntv = aPayIntv;
    }
    public void setPayIntv(String aPayIntv) {
        if (aPayIntv != null && !aPayIntv.equals("")) {
            Integer tInteger = new Integer(aPayIntv);
            int i = tInteger.intValue();
            PayIntv = i;
        }
    }

    public String getPayEndYearFlag() {
        return PayEndYearFlag;
    }
    public void setPayEndYearFlag(String aPayEndYearFlag) {
        PayEndYearFlag = aPayEndYearFlag;
    }
    public int getPayEndYear() {
        return PayEndYear;
    }
    public void setPayEndYear(int aPayEndYear) {
        PayEndYear = aPayEndYear;
    }
    public void setPayEndYear(String aPayEndYear) {
        if (aPayEndYear != null && !aPayEndYear.equals("")) {
            Integer tInteger = new Integer(aPayEndYear);
            int i = tInteger.intValue();
            PayEndYear = i;
        }
    }

    public String getPayEndDateCalRef() {
        return PayEndDateCalRef;
    }
    public void setPayEndDateCalRef(String aPayEndDateCalRef) {
        PayEndDateCalRef = aPayEndDateCalRef;
    }
    public String getPayEndDateCalMode() {
        return PayEndDateCalMode;
    }
    public void setPayEndDateCalMode(String aPayEndDateCalMode) {
        PayEndDateCalMode = aPayEndDateCalMode;
    }
    public double getDefaultVal() {
        return DefaultVal;
    }
    public void setDefaultVal(double aDefaultVal) {
        DefaultVal = aDefaultVal;
    }
    public void setDefaultVal(String aDefaultVal) {
        if (aDefaultVal != null && !aDefaultVal.equals("")) {
            Double tDouble = new Double(aDefaultVal);
            double d = tDouble.doubleValue();
            DefaultVal = d;
        }
    }

    public String getCalCode() {
        return CalCode;
    }
    public void setCalCode(String aCalCode) {
        CalCode = aCalCode;
    }
    public String getCnterCalCode() {
        return CnterCalCode;
    }
    public void setCnterCalCode(String aCnterCalCode) {
        CnterCalCode = aCnterCalCode;
    }
    public String getOthCalCode() {
        return OthCalCode;
    }
    public void setOthCalCode(String aOthCalCode) {
        OthCalCode = aOthCalCode;
    }
    public double getRate() {
        return Rate;
    }
    public void setRate(double aRate) {
        Rate = aRate;
    }
    public void setRate(String aRate) {
        if (aRate != null && !aRate.equals("")) {
            Double tDouble = new Double(aRate);
            double d = tDouble.doubleValue();
            Rate = d;
        }
    }

    public double getMinPay() {
        return MinPay;
    }
    public void setMinPay(double aMinPay) {
        MinPay = aMinPay;
    }
    public void setMinPay(String aMinPay) {
        if (aMinPay != null && !aMinPay.equals("")) {
            Double tDouble = new Double(aMinPay);
            double d = tDouble.doubleValue();
            MinPay = d;
        }
    }

    public double getAssuYield() {
        return AssuYield;
    }
    public void setAssuYield(double aAssuYield) {
        AssuYield = aAssuYield;
    }
    public void setAssuYield(String aAssuYield) {
        if (aAssuYield != null && !aAssuYield.equals("")) {
            Double tDouble = new Double(aAssuYield);
            double d = tDouble.doubleValue();
            AssuYield = d;
        }
    }

    public double getFeeRate() {
        return FeeRate;
    }
    public void setFeeRate(double aFeeRate) {
        FeeRate = aFeeRate;
    }
    public void setFeeRate(String aFeeRate) {
        if (aFeeRate != null && !aFeeRate.equals("")) {
            Double tDouble = new Double(aFeeRate);
            double d = tDouble.doubleValue();
            FeeRate = d;
        }
    }

    public String getPayToDateCalMode() {
        return PayToDateCalMode;
    }
    public void setPayToDateCalMode(String aPayToDateCalMode) {
        PayToDateCalMode = aPayToDateCalMode;
    }
    public String getUrgePayFlag() {
        return UrgePayFlag;
    }
    public void setUrgePayFlag(String aUrgePayFlag) {
        UrgePayFlag = aUrgePayFlag;
    }
    public String getPayLackFlag() {
        return PayLackFlag;
    }
    public void setPayLackFlag(String aPayLackFlag) {
        PayLackFlag = aPayLackFlag;
    }
    public String getPayOverFlag() {
        return PayOverFlag;
    }
    public void setPayOverFlag(String aPayOverFlag) {
        PayOverFlag = aPayOverFlag;
    }
    public String getPayOverDeal() {
        return PayOverDeal;
    }
    public void setPayOverDeal(String aPayOverDeal) {
        PayOverDeal = aPayOverDeal;
    }
    public String getAvoidPayFlag() {
        return AvoidPayFlag;
    }
    public void setAvoidPayFlag(String aAvoidPayFlag) {
        AvoidPayFlag = aAvoidPayFlag;
    }
    public int getGracePeriod() {
        return GracePeriod;
    }
    public void setGracePeriod(int aGracePeriod) {
        GracePeriod = aGracePeriod;
    }
    public void setGracePeriod(String aGracePeriod) {
        if (aGracePeriod != null && !aGracePeriod.equals("")) {
            Integer tInteger = new Integer(aGracePeriod);
            int i = tInteger.intValue();
            GracePeriod = i;
        }
    }

    public String getPubFlag() {
        return PubFlag;
    }
    public void setPubFlag(String aPubFlag) {
        PubFlag = aPubFlag;
    }
    public String getZeroFlag() {
        return ZeroFlag;
    }
    public void setZeroFlag(String aZeroFlag) {
        ZeroFlag = aZeroFlag;
    }
    public String getNeedAcc() {
        return NeedAcc;
    }
    public void setNeedAcc(String aNeedAcc) {
        NeedAcc = aNeedAcc;
    }
    public String getPayAimClass() {
        return PayAimClass;
    }
    public void setPayAimClass(String aPayAimClass) {
        PayAimClass = aPayAimClass;
    }
    public int getPayStartYear() {
        return PayStartYear;
    }
    public void setPayStartYear(int aPayStartYear) {
        PayStartYear = aPayStartYear;
    }
    public void setPayStartYear(String aPayStartYear) {
        if (aPayStartYear != null && !aPayStartYear.equals("")) {
            Integer tInteger = new Integer(aPayStartYear);
            int i = tInteger.intValue();
            PayStartYear = i;
        }
    }

    public String getPayStartYearFlag() {
        return PayStartYearFlag;
    }
    public void setPayStartYearFlag(String aPayStartYearFlag) {
        PayStartYearFlag = aPayStartYearFlag;
    }
    public String getPayStartDateCalRef() {
        return PayStartDateCalRef;
    }
    public void setPayStartDateCalRef(String aPayStartDateCalRef) {
        PayStartDateCalRef = aPayStartDateCalRef;
    }
    public String getPayStartDateCalMode() {
        return PayStartDateCalMode;
    }
    public void setPayStartDateCalMode(String aPayStartDateCalMode) {
        PayStartDateCalMode = aPayStartDateCalMode;
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("PayPlanCode") ) {
            return 0;
        }
        if( strFieldName.equals("PayPlanName") ) {
            return 1;
        }
        if( strFieldName.equals("Type") ) {
            return 2;
        }
        if( strFieldName.equals("PayIntv") ) {
            return 3;
        }
        if( strFieldName.equals("PayEndYearFlag") ) {
            return 4;
        }
        if( strFieldName.equals("PayEndYear") ) {
            return 5;
        }
        if( strFieldName.equals("PayEndDateCalRef") ) {
            return 6;
        }
        if( strFieldName.equals("PayEndDateCalMode") ) {
            return 7;
        }
        if( strFieldName.equals("DefaultVal") ) {
            return 8;
        }
        if( strFieldName.equals("CalCode") ) {
            return 9;
        }
        if( strFieldName.equals("CnterCalCode") ) {
            return 10;
        }
        if( strFieldName.equals("OthCalCode") ) {
            return 11;
        }
        if( strFieldName.equals("Rate") ) {
            return 12;
        }
        if( strFieldName.equals("MinPay") ) {
            return 13;
        }
        if( strFieldName.equals("AssuYield") ) {
            return 14;
        }
        if( strFieldName.equals("FeeRate") ) {
            return 15;
        }
        if( strFieldName.equals("PayToDateCalMode") ) {
            return 16;
        }
        if( strFieldName.equals("UrgePayFlag") ) {
            return 17;
        }
        if( strFieldName.equals("PayLackFlag") ) {
            return 18;
        }
        if( strFieldName.equals("PayOverFlag") ) {
            return 19;
        }
        if( strFieldName.equals("PayOverDeal") ) {
            return 20;
        }
        if( strFieldName.equals("AvoidPayFlag") ) {
            return 21;
        }
        if( strFieldName.equals("GracePeriod") ) {
            return 22;
        }
        if( strFieldName.equals("PubFlag") ) {
            return 23;
        }
        if( strFieldName.equals("ZeroFlag") ) {
            return 24;
        }
        if( strFieldName.equals("NeedAcc") ) {
            return 25;
        }
        if( strFieldName.equals("PayAimClass") ) {
            return 26;
        }
        if( strFieldName.equals("PayStartYear") ) {
            return 27;
        }
        if( strFieldName.equals("PayStartYearFlag") ) {
            return 28;
        }
        if( strFieldName.equals("PayStartDateCalRef") ) {
            return 29;
        }
        if( strFieldName.equals("PayStartDateCalMode") ) {
            return 30;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "PayPlanCode";
                break;
            case 1:
                strFieldName = "PayPlanName";
                break;
            case 2:
                strFieldName = "Type";
                break;
            case 3:
                strFieldName = "PayIntv";
                break;
            case 4:
                strFieldName = "PayEndYearFlag";
                break;
            case 5:
                strFieldName = "PayEndYear";
                break;
            case 6:
                strFieldName = "PayEndDateCalRef";
                break;
            case 7:
                strFieldName = "PayEndDateCalMode";
                break;
            case 8:
                strFieldName = "DefaultVal";
                break;
            case 9:
                strFieldName = "CalCode";
                break;
            case 10:
                strFieldName = "CnterCalCode";
                break;
            case 11:
                strFieldName = "OthCalCode";
                break;
            case 12:
                strFieldName = "Rate";
                break;
            case 13:
                strFieldName = "MinPay";
                break;
            case 14:
                strFieldName = "AssuYield";
                break;
            case 15:
                strFieldName = "FeeRate";
                break;
            case 16:
                strFieldName = "PayToDateCalMode";
                break;
            case 17:
                strFieldName = "UrgePayFlag";
                break;
            case 18:
                strFieldName = "PayLackFlag";
                break;
            case 19:
                strFieldName = "PayOverFlag";
                break;
            case 20:
                strFieldName = "PayOverDeal";
                break;
            case 21:
                strFieldName = "AvoidPayFlag";
                break;
            case 22:
                strFieldName = "GracePeriod";
                break;
            case 23:
                strFieldName = "PubFlag";
                break;
            case 24:
                strFieldName = "ZeroFlag";
                break;
            case 25:
                strFieldName = "NeedAcc";
                break;
            case 26:
                strFieldName = "PayAimClass";
                break;
            case 27:
                strFieldName = "PayStartYear";
                break;
            case 28:
                strFieldName = "PayStartYearFlag";
                break;
            case 29:
                strFieldName = "PayStartDateCalRef";
                break;
            case 30:
                strFieldName = "PayStartDateCalMode";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "PAYPLANCODE":
                return Schema.TYPE_STRING;
            case "PAYPLANNAME":
                return Schema.TYPE_STRING;
            case "TYPE":
                return Schema.TYPE_STRING;
            case "PAYINTV":
                return Schema.TYPE_INT;
            case "PAYENDYEARFLAG":
                return Schema.TYPE_STRING;
            case "PAYENDYEAR":
                return Schema.TYPE_INT;
            case "PAYENDDATECALREF":
                return Schema.TYPE_STRING;
            case "PAYENDDATECALMODE":
                return Schema.TYPE_STRING;
            case "DEFAULTVAL":
                return Schema.TYPE_DOUBLE;
            case "CALCODE":
                return Schema.TYPE_STRING;
            case "CNTERCALCODE":
                return Schema.TYPE_STRING;
            case "OTHCALCODE":
                return Schema.TYPE_STRING;
            case "RATE":
                return Schema.TYPE_DOUBLE;
            case "MINPAY":
                return Schema.TYPE_DOUBLE;
            case "ASSUYIELD":
                return Schema.TYPE_DOUBLE;
            case "FEERATE":
                return Schema.TYPE_DOUBLE;
            case "PAYTODATECALMODE":
                return Schema.TYPE_STRING;
            case "URGEPAYFLAG":
                return Schema.TYPE_STRING;
            case "PAYLACKFLAG":
                return Schema.TYPE_STRING;
            case "PAYOVERFLAG":
                return Schema.TYPE_STRING;
            case "PAYOVERDEAL":
                return Schema.TYPE_STRING;
            case "AVOIDPAYFLAG":
                return Schema.TYPE_STRING;
            case "GRACEPERIOD":
                return Schema.TYPE_INT;
            case "PUBFLAG":
                return Schema.TYPE_STRING;
            case "ZEROFLAG":
                return Schema.TYPE_STRING;
            case "NEEDACC":
                return Schema.TYPE_STRING;
            case "PAYAIMCLASS":
                return Schema.TYPE_STRING;
            case "PAYSTARTYEAR":
                return Schema.TYPE_INT;
            case "PAYSTARTYEARFLAG":
                return Schema.TYPE_STRING;
            case "PAYSTARTDATECALREF":
                return Schema.TYPE_STRING;
            case "PAYSTARTDATECALMODE":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_STRING;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_INT;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_INT;
            case 6:
                return Schema.TYPE_STRING;
            case 7:
                return Schema.TYPE_STRING;
            case 8:
                return Schema.TYPE_DOUBLE;
            case 9:
                return Schema.TYPE_STRING;
            case 10:
                return Schema.TYPE_STRING;
            case 11:
                return Schema.TYPE_STRING;
            case 12:
                return Schema.TYPE_DOUBLE;
            case 13:
                return Schema.TYPE_DOUBLE;
            case 14:
                return Schema.TYPE_DOUBLE;
            case 15:
                return Schema.TYPE_DOUBLE;
            case 16:
                return Schema.TYPE_STRING;
            case 17:
                return Schema.TYPE_STRING;
            case 18:
                return Schema.TYPE_STRING;
            case 19:
                return Schema.TYPE_STRING;
            case 20:
                return Schema.TYPE_STRING;
            case 21:
                return Schema.TYPE_STRING;
            case 22:
                return Schema.TYPE_INT;
            case 23:
                return Schema.TYPE_STRING;
            case 24:
                return Schema.TYPE_STRING;
            case 25:
                return Schema.TYPE_STRING;
            case 26:
                return Schema.TYPE_STRING;
            case 27:
                return Schema.TYPE_INT;
            case 28:
                return Schema.TYPE_STRING;
            case 29:
                return Schema.TYPE_STRING;
            case 30:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("PayPlanCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PayPlanCode));
        }
        if (FCode.equalsIgnoreCase("PayPlanName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PayPlanName));
        }
        if (FCode.equalsIgnoreCase("Type")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Type));
        }
        if (FCode.equalsIgnoreCase("PayIntv")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PayIntv));
        }
        if (FCode.equalsIgnoreCase("PayEndYearFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PayEndYearFlag));
        }
        if (FCode.equalsIgnoreCase("PayEndYear")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PayEndYear));
        }
        if (FCode.equalsIgnoreCase("PayEndDateCalRef")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PayEndDateCalRef));
        }
        if (FCode.equalsIgnoreCase("PayEndDateCalMode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PayEndDateCalMode));
        }
        if (FCode.equalsIgnoreCase("DefaultVal")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DefaultVal));
        }
        if (FCode.equalsIgnoreCase("CalCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CalCode));
        }
        if (FCode.equalsIgnoreCase("CnterCalCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CnterCalCode));
        }
        if (FCode.equalsIgnoreCase("OthCalCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OthCalCode));
        }
        if (FCode.equalsIgnoreCase("Rate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Rate));
        }
        if (FCode.equalsIgnoreCase("MinPay")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MinPay));
        }
        if (FCode.equalsIgnoreCase("AssuYield")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AssuYield));
        }
        if (FCode.equalsIgnoreCase("FeeRate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FeeRate));
        }
        if (FCode.equalsIgnoreCase("PayToDateCalMode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PayToDateCalMode));
        }
        if (FCode.equalsIgnoreCase("UrgePayFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(UrgePayFlag));
        }
        if (FCode.equalsIgnoreCase("PayLackFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PayLackFlag));
        }
        if (FCode.equalsIgnoreCase("PayOverFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PayOverFlag));
        }
        if (FCode.equalsIgnoreCase("PayOverDeal")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PayOverDeal));
        }
        if (FCode.equalsIgnoreCase("AvoidPayFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AvoidPayFlag));
        }
        if (FCode.equalsIgnoreCase("GracePeriod")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GracePeriod));
        }
        if (FCode.equalsIgnoreCase("PubFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PubFlag));
        }
        if (FCode.equalsIgnoreCase("ZeroFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ZeroFlag));
        }
        if (FCode.equalsIgnoreCase("NeedAcc")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(NeedAcc));
        }
        if (FCode.equalsIgnoreCase("PayAimClass")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PayAimClass));
        }
        if (FCode.equalsIgnoreCase("PayStartYear")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PayStartYear));
        }
        if (FCode.equalsIgnoreCase("PayStartYearFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PayStartYearFlag));
        }
        if (FCode.equalsIgnoreCase("PayStartDateCalRef")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PayStartDateCalRef));
        }
        if (FCode.equalsIgnoreCase("PayStartDateCalMode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PayStartDateCalMode));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = String.valueOf(PayPlanCode);
                break;
            case 1:
                strFieldValue = String.valueOf(PayPlanName);
                break;
            case 2:
                strFieldValue = String.valueOf(Type);
                break;
            case 3:
                strFieldValue = String.valueOf(PayIntv);
                break;
            case 4:
                strFieldValue = String.valueOf(PayEndYearFlag);
                break;
            case 5:
                strFieldValue = String.valueOf(PayEndYear);
                break;
            case 6:
                strFieldValue = String.valueOf(PayEndDateCalRef);
                break;
            case 7:
                strFieldValue = String.valueOf(PayEndDateCalMode);
                break;
            case 8:
                strFieldValue = String.valueOf(DefaultVal);
                break;
            case 9:
                strFieldValue = String.valueOf(CalCode);
                break;
            case 10:
                strFieldValue = String.valueOf(CnterCalCode);
                break;
            case 11:
                strFieldValue = String.valueOf(OthCalCode);
                break;
            case 12:
                strFieldValue = String.valueOf(Rate);
                break;
            case 13:
                strFieldValue = String.valueOf(MinPay);
                break;
            case 14:
                strFieldValue = String.valueOf(AssuYield);
                break;
            case 15:
                strFieldValue = String.valueOf(FeeRate);
                break;
            case 16:
                strFieldValue = String.valueOf(PayToDateCalMode);
                break;
            case 17:
                strFieldValue = String.valueOf(UrgePayFlag);
                break;
            case 18:
                strFieldValue = String.valueOf(PayLackFlag);
                break;
            case 19:
                strFieldValue = String.valueOf(PayOverFlag);
                break;
            case 20:
                strFieldValue = String.valueOf(PayOverDeal);
                break;
            case 21:
                strFieldValue = String.valueOf(AvoidPayFlag);
                break;
            case 22:
                strFieldValue = String.valueOf(GracePeriod);
                break;
            case 23:
                strFieldValue = String.valueOf(PubFlag);
                break;
            case 24:
                strFieldValue = String.valueOf(ZeroFlag);
                break;
            case 25:
                strFieldValue = String.valueOf(NeedAcc);
                break;
            case 26:
                strFieldValue = String.valueOf(PayAimClass);
                break;
            case 27:
                strFieldValue = String.valueOf(PayStartYear);
                break;
            case 28:
                strFieldValue = String.valueOf(PayStartYearFlag);
                break;
            case 29:
                strFieldValue = String.valueOf(PayStartDateCalRef);
                break;
            case 30:
                strFieldValue = String.valueOf(PayStartDateCalMode);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("PayPlanCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                PayPlanCode = FValue.trim();
            }
            else
                PayPlanCode = null;
        }
        if (FCode.equalsIgnoreCase("PayPlanName")) {
            if( FValue != null && !FValue.equals(""))
            {
                PayPlanName = FValue.trim();
            }
            else
                PayPlanName = null;
        }
        if (FCode.equalsIgnoreCase("Type")) {
            if( FValue != null && !FValue.equals(""))
            {
                Type = FValue.trim();
            }
            else
                Type = null;
        }
        if (FCode.equalsIgnoreCase("PayIntv")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                PayIntv = i;
            }
        }
        if (FCode.equalsIgnoreCase("PayEndYearFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                PayEndYearFlag = FValue.trim();
            }
            else
                PayEndYearFlag = null;
        }
        if (FCode.equalsIgnoreCase("PayEndYear")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                PayEndYear = i;
            }
        }
        if (FCode.equalsIgnoreCase("PayEndDateCalRef")) {
            if( FValue != null && !FValue.equals(""))
            {
                PayEndDateCalRef = FValue.trim();
            }
            else
                PayEndDateCalRef = null;
        }
        if (FCode.equalsIgnoreCase("PayEndDateCalMode")) {
            if( FValue != null && !FValue.equals(""))
            {
                PayEndDateCalMode = FValue.trim();
            }
            else
                PayEndDateCalMode = null;
        }
        if (FCode.equalsIgnoreCase("DefaultVal")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                DefaultVal = d;
            }
        }
        if (FCode.equalsIgnoreCase("CalCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                CalCode = FValue.trim();
            }
            else
                CalCode = null;
        }
        if (FCode.equalsIgnoreCase("CnterCalCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                CnterCalCode = FValue.trim();
            }
            else
                CnterCalCode = null;
        }
        if (FCode.equalsIgnoreCase("OthCalCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                OthCalCode = FValue.trim();
            }
            else
                OthCalCode = null;
        }
        if (FCode.equalsIgnoreCase("Rate")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                Rate = d;
            }
        }
        if (FCode.equalsIgnoreCase("MinPay")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                MinPay = d;
            }
        }
        if (FCode.equalsIgnoreCase("AssuYield")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                AssuYield = d;
            }
        }
        if (FCode.equalsIgnoreCase("FeeRate")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                FeeRate = d;
            }
        }
        if (FCode.equalsIgnoreCase("PayToDateCalMode")) {
            if( FValue != null && !FValue.equals(""))
            {
                PayToDateCalMode = FValue.trim();
            }
            else
                PayToDateCalMode = null;
        }
        if (FCode.equalsIgnoreCase("UrgePayFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                UrgePayFlag = FValue.trim();
            }
            else
                UrgePayFlag = null;
        }
        if (FCode.equalsIgnoreCase("PayLackFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                PayLackFlag = FValue.trim();
            }
            else
                PayLackFlag = null;
        }
        if (FCode.equalsIgnoreCase("PayOverFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                PayOverFlag = FValue.trim();
            }
            else
                PayOverFlag = null;
        }
        if (FCode.equalsIgnoreCase("PayOverDeal")) {
            if( FValue != null && !FValue.equals(""))
            {
                PayOverDeal = FValue.trim();
            }
            else
                PayOverDeal = null;
        }
        if (FCode.equalsIgnoreCase("AvoidPayFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                AvoidPayFlag = FValue.trim();
            }
            else
                AvoidPayFlag = null;
        }
        if (FCode.equalsIgnoreCase("GracePeriod")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                GracePeriod = i;
            }
        }
        if (FCode.equalsIgnoreCase("PubFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                PubFlag = FValue.trim();
            }
            else
                PubFlag = null;
        }
        if (FCode.equalsIgnoreCase("ZeroFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                ZeroFlag = FValue.trim();
            }
            else
                ZeroFlag = null;
        }
        if (FCode.equalsIgnoreCase("NeedAcc")) {
            if( FValue != null && !FValue.equals(""))
            {
                NeedAcc = FValue.trim();
            }
            else
                NeedAcc = null;
        }
        if (FCode.equalsIgnoreCase("PayAimClass")) {
            if( FValue != null && !FValue.equals(""))
            {
                PayAimClass = FValue.trim();
            }
            else
                PayAimClass = null;
        }
        if (FCode.equalsIgnoreCase("PayStartYear")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                PayStartYear = i;
            }
        }
        if (FCode.equalsIgnoreCase("PayStartYearFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                PayStartYearFlag = FValue.trim();
            }
            else
                PayStartYearFlag = null;
        }
        if (FCode.equalsIgnoreCase("PayStartDateCalRef")) {
            if( FValue != null && !FValue.equals(""))
            {
                PayStartDateCalRef = FValue.trim();
            }
            else
                PayStartDateCalRef = null;
        }
        if (FCode.equalsIgnoreCase("PayStartDateCalMode")) {
            if( FValue != null && !FValue.equals(""))
            {
                PayStartDateCalMode = FValue.trim();
            }
            else
                PayStartDateCalMode = null;
        }
        return true;
    }


    public String toString() {
    return "LMDutyPayPojo [" +
            "PayPlanCode="+PayPlanCode +
            ", PayPlanName="+PayPlanName +
            ", Type="+Type +
            ", PayIntv="+PayIntv +
            ", PayEndYearFlag="+PayEndYearFlag +
            ", PayEndYear="+PayEndYear +
            ", PayEndDateCalRef="+PayEndDateCalRef +
            ", PayEndDateCalMode="+PayEndDateCalMode +
            ", DefaultVal="+DefaultVal +
            ", CalCode="+CalCode +
            ", CnterCalCode="+CnterCalCode +
            ", OthCalCode="+OthCalCode +
            ", Rate="+Rate +
            ", MinPay="+MinPay +
            ", AssuYield="+AssuYield +
            ", FeeRate="+FeeRate +
            ", PayToDateCalMode="+PayToDateCalMode +
            ", UrgePayFlag="+UrgePayFlag +
            ", PayLackFlag="+PayLackFlag +
            ", PayOverFlag="+PayOverFlag +
            ", PayOverDeal="+PayOverDeal +
            ", AvoidPayFlag="+AvoidPayFlag +
            ", GracePeriod="+GracePeriod +
            ", PubFlag="+PubFlag +
            ", ZeroFlag="+ZeroFlag +
            ", NeedAcc="+NeedAcc +
            ", PayAimClass="+PayAimClass +
            ", PayStartYear="+PayStartYear +
            ", PayStartYearFlag="+PayStartYearFlag +
            ", PayStartDateCalRef="+PayStartDateCalRef +
            ", PayStartDateCalMode="+PayStartDateCalMode +"]";
    }
}
