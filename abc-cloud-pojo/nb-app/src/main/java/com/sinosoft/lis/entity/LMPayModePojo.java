/**
 * Copyright (c) 2017 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.entity;

import com.sinosoft.utility.Pojo;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;

import java.io.Serializable;

/**
 * <p>ClassName: LMPayModePojo </p>
 * <p>Copyright: Copyright (c) 2017</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2017-10-17
 */
public class LMPayModePojo implements Pojo,Serializable {
    // @Field
    /** 险种编码 */
    private String RiskCode; 
    /** 险种版本 */
    private String RiskVer; 
    /** 终交年龄年期标志 */
    private String PayEndYearFlag; 
    /** 终交年龄年期 */
    private int PayEndYear; 
    /** 交费间隔 */
    private int PayIntv; 
    /** 允许交至满期标志 */
    private String PayToExpiryFlag; 
    /** 显示信息 */
    private String ShowInfo; 


    public static final int FIELDNUM = 7;    // 数据库表的字段个数
    public String getRiskCode() {
        return RiskCode;
    }
    public void setRiskCode(String aRiskCode) {
        RiskCode = aRiskCode;
    }
    public String getRiskVer() {
        return RiskVer;
    }
    public void setRiskVer(String aRiskVer) {
        RiskVer = aRiskVer;
    }
    public String getPayEndYearFlag() {
        return PayEndYearFlag;
    }
    public void setPayEndYearFlag(String aPayEndYearFlag) {
        PayEndYearFlag = aPayEndYearFlag;
    }
    public int getPayEndYear() {
        return PayEndYear;
    }
    public void setPayEndYear(int aPayEndYear) {
        PayEndYear = aPayEndYear;
    }
    public void setPayEndYear(String aPayEndYear) {
        if (aPayEndYear != null && !aPayEndYear.equals("")) {
            Integer tInteger = new Integer(aPayEndYear);
            int i = tInteger.intValue();
            PayEndYear = i;
        }
    }

    public int getPayIntv() {
        return PayIntv;
    }
    public void setPayIntv(int aPayIntv) {
        PayIntv = aPayIntv;
    }
    public void setPayIntv(String aPayIntv) {
        if (aPayIntv != null && !aPayIntv.equals("")) {
            Integer tInteger = new Integer(aPayIntv);
            int i = tInteger.intValue();
            PayIntv = i;
        }
    }

    public String getPayToExpiryFlag() {
        return PayToExpiryFlag;
    }
    public void setPayToExpiryFlag(String aPayToExpiryFlag) {
        PayToExpiryFlag = aPayToExpiryFlag;
    }
    public String getShowInfo() {
        return ShowInfo;
    }
    public void setShowInfo(String aShowInfo) {
        ShowInfo = aShowInfo;
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("RiskCode") ) {
            return 0;
        }
        if( strFieldName.equals("RiskVer") ) {
            return 1;
        }
        if( strFieldName.equals("PayEndYearFlag") ) {
            return 2;
        }
        if( strFieldName.equals("PayEndYear") ) {
            return 3;
        }
        if( strFieldName.equals("PayIntv") ) {
            return 4;
        }
        if( strFieldName.equals("PayToExpiryFlag") ) {
            return 5;
        }
        if( strFieldName.equals("ShowInfo") ) {
            return 6;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "RiskCode";
                break;
            case 1:
                strFieldName = "RiskVer";
                break;
            case 2:
                strFieldName = "PayEndYearFlag";
                break;
            case 3:
                strFieldName = "PayEndYear";
                break;
            case 4:
                strFieldName = "PayIntv";
                break;
            case 5:
                strFieldName = "PayToExpiryFlag";
                break;
            case 6:
                strFieldName = "ShowInfo";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "RISKCODE":
                return Schema.TYPE_STRING;
            case "RISKVER":
                return Schema.TYPE_STRING;
            case "PAYENDYEARFLAG":
                return Schema.TYPE_STRING;
            case "PAYENDYEAR":
                return Schema.TYPE_INT;
            case "PAYINTV":
                return Schema.TYPE_INT;
            case "PAYTOEXPIRYFLAG":
                return Schema.TYPE_STRING;
            case "SHOWINFO":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_STRING;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_INT;
            case 4:
                return Schema.TYPE_INT;
            case 5:
                return Schema.TYPE_STRING;
            case 6:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("RiskCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RiskCode));
        }
        if (FCode.equalsIgnoreCase("RiskVer")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RiskVer));
        }
        if (FCode.equalsIgnoreCase("PayEndYearFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PayEndYearFlag));
        }
        if (FCode.equalsIgnoreCase("PayEndYear")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PayEndYear));
        }
        if (FCode.equalsIgnoreCase("PayIntv")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PayIntv));
        }
        if (FCode.equalsIgnoreCase("PayToExpiryFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PayToExpiryFlag));
        }
        if (FCode.equalsIgnoreCase("ShowInfo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ShowInfo));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = String.valueOf(RiskCode);
                break;
            case 1:
                strFieldValue = String.valueOf(RiskVer);
                break;
            case 2:
                strFieldValue = String.valueOf(PayEndYearFlag);
                break;
            case 3:
                strFieldValue = String.valueOf(PayEndYear);
                break;
            case 4:
                strFieldValue = String.valueOf(PayIntv);
                break;
            case 5:
                strFieldValue = String.valueOf(PayToExpiryFlag);
                break;
            case 6:
                strFieldValue = String.valueOf(ShowInfo);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("RiskCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                RiskCode = FValue.trim();
            }
            else
                RiskCode = null;
        }
        if (FCode.equalsIgnoreCase("RiskVer")) {
            if( FValue != null && !FValue.equals(""))
            {
                RiskVer = FValue.trim();
            }
            else
                RiskVer = null;
        }
        if (FCode.equalsIgnoreCase("PayEndYearFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                PayEndYearFlag = FValue.trim();
            }
            else
                PayEndYearFlag = null;
        }
        if (FCode.equalsIgnoreCase("PayEndYear")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                PayEndYear = i;
            }
        }
        if (FCode.equalsIgnoreCase("PayIntv")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                PayIntv = i;
            }
        }
        if (FCode.equalsIgnoreCase("PayToExpiryFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                PayToExpiryFlag = FValue.trim();
            }
            else
                PayToExpiryFlag = null;
        }
        if (FCode.equalsIgnoreCase("ShowInfo")) {
            if( FValue != null && !FValue.equals(""))
            {
                ShowInfo = FValue.trim();
            }
            else
                ShowInfo = null;
        }
        return true;
    }


    public String toString() {
    return "LMPayModePojo [" +
            "RiskCode="+RiskCode +
            ", RiskVer="+RiskVer +
            ", PayEndYearFlag="+PayEndYearFlag +
            ", PayEndYear="+PayEndYear +
            ", PayIntv="+PayIntv +
            ", PayToExpiryFlag="+PayToExpiryFlag +
            ", ShowInfo="+ShowInfo +"]";
    }
}
