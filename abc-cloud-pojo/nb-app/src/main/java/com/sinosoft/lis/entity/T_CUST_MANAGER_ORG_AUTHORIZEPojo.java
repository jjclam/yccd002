/**
 * Copyright (c) 2018 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.entity;

import com.sinosoft.cloud.cache.annotation.RedisPrimaryHKey;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.Pojo;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;

import java.io.Serializable;

/**
 * <p>ClassName: T_CUST_MANAGER_ORG_AUTHORIZEPojo </p>
 * <p>Copyright: Copyright (c) 2018</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2018-11-22
 */
public class T_CUST_MANAGER_ORG_AUTHORIZEPojo implements Pojo,Serializable {
    // @Field
    /** 客户经理授权id */
    @RedisPrimaryHKey
    private long CUST_MANAGER_AUTHORIZE_ID; 
    /** 管理机构id */
    private long MNGORG_ID; 
    /** 中介机构id */
    private long AGENCY_ID; 
    /** 客户经理id */
    private long CUST_MANAGER_ID; 
    /** 插入操作员 */
    private String INSERT_OPER; 
    /** 插入委托人 */
    private String INSERT_CONSIGNOR; 
    /** 插入时间 */
    private String  INSERT_TIME;
    /** 更新操作员 */
    private String UPDATE_OPER; 
    /** 更新委托人 */
    private String UPDATE_CONSIGNOR; 
    /** 更新时间 */
    private String  UPDATE_TIME;


    public static final int FIELDNUM = 10;    // 数据库表的字段个数

    private FDate fDate = new FDate();        // 处理日期
    public long getCUST_MANAGER_AUTHORIZE_ID() {
        return CUST_MANAGER_AUTHORIZE_ID;
    }
    public void setCUST_MANAGER_AUTHORIZE_ID(long aCUST_MANAGER_AUTHORIZE_ID) {
        CUST_MANAGER_AUTHORIZE_ID = aCUST_MANAGER_AUTHORIZE_ID;
    }
    public void setCUST_MANAGER_AUTHORIZE_ID(String aCUST_MANAGER_AUTHORIZE_ID) {
        if (aCUST_MANAGER_AUTHORIZE_ID != null && !aCUST_MANAGER_AUTHORIZE_ID.equals("")) {
            CUST_MANAGER_AUTHORIZE_ID = new Long(aCUST_MANAGER_AUTHORIZE_ID).longValue();
        }
    }

    public long getMNGORG_ID() {
        return MNGORG_ID;
    }
    public void setMNGORG_ID(long aMNGORG_ID) {
        MNGORG_ID = aMNGORG_ID;
    }
    public void setMNGORG_ID(String aMNGORG_ID) {
        if (aMNGORG_ID != null && !aMNGORG_ID.equals("")) {
            MNGORG_ID = new Long(aMNGORG_ID).longValue();
        }
    }

    public long getAGENCY_ID() {
        return AGENCY_ID;
    }
    public void setAGENCY_ID(long aAGENCY_ID) {
        AGENCY_ID = aAGENCY_ID;
    }
    public void setAGENCY_ID(String aAGENCY_ID) {
        if (aAGENCY_ID != null && !aAGENCY_ID.equals("")) {
            AGENCY_ID = new Long(aAGENCY_ID).longValue();
        }
    }

    public long getCUST_MANAGER_ID() {
        return CUST_MANAGER_ID;
    }
    public void setCUST_MANAGER_ID(long aCUST_MANAGER_ID) {
        CUST_MANAGER_ID = aCUST_MANAGER_ID;
    }
    public void setCUST_MANAGER_ID(String aCUST_MANAGER_ID) {
        if (aCUST_MANAGER_ID != null && !aCUST_MANAGER_ID.equals("")) {
            CUST_MANAGER_ID = new Long(aCUST_MANAGER_ID).longValue();
        }
    }

    public String getINSERT_OPER() {
        return INSERT_OPER;
    }
    public void setINSERT_OPER(String aINSERT_OPER) {
        INSERT_OPER = aINSERT_OPER;
    }
    public String getINSERT_CONSIGNOR() {
        return INSERT_CONSIGNOR;
    }
    public void setINSERT_CONSIGNOR(String aINSERT_CONSIGNOR) {
        INSERT_CONSIGNOR = aINSERT_CONSIGNOR;
    }
    public String getINSERT_TIME() {
        return INSERT_TIME;
    }
    public void setINSERT_TIME(String aINSERT_TIME) {
        INSERT_TIME = aINSERT_TIME;
    }
    public String getUPDATE_OPER() {
        return UPDATE_OPER;
    }
    public void setUPDATE_OPER(String aUPDATE_OPER) {
        UPDATE_OPER = aUPDATE_OPER;
    }
    public String getUPDATE_CONSIGNOR() {
        return UPDATE_CONSIGNOR;
    }
    public void setUPDATE_CONSIGNOR(String aUPDATE_CONSIGNOR) {
        UPDATE_CONSIGNOR = aUPDATE_CONSIGNOR;
    }
    public String getUPDATE_TIME() {
        return UPDATE_TIME;
    }
    public void setUPDATE_TIME(String aUPDATE_TIME) {
        UPDATE_TIME = aUPDATE_TIME;
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("CUST_MANAGER_AUTHORIZE_ID") ) {
            return 0;
        }
        if( strFieldName.equals("MNGORG_ID") ) {
            return 1;
        }
        if( strFieldName.equals("AGENCY_ID") ) {
            return 2;
        }
        if( strFieldName.equals("CUST_MANAGER_ID") ) {
            return 3;
        }
        if( strFieldName.equals("INSERT_OPER") ) {
            return 4;
        }
        if( strFieldName.equals("INSERT_CONSIGNOR") ) {
            return 5;
        }
        if( strFieldName.equals("INSERT_TIME") ) {
            return 6;
        }
        if( strFieldName.equals("UPDATE_OPER") ) {
            return 7;
        }
        if( strFieldName.equals("UPDATE_CONSIGNOR") ) {
            return 8;
        }
        if( strFieldName.equals("UPDATE_TIME") ) {
            return 9;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "CUST_MANAGER_AUTHORIZE_ID";
                break;
            case 1:
                strFieldName = "MNGORG_ID";
                break;
            case 2:
                strFieldName = "AGENCY_ID";
                break;
            case 3:
                strFieldName = "CUST_MANAGER_ID";
                break;
            case 4:
                strFieldName = "INSERT_OPER";
                break;
            case 5:
                strFieldName = "INSERT_CONSIGNOR";
                break;
            case 6:
                strFieldName = "INSERT_TIME";
                break;
            case 7:
                strFieldName = "UPDATE_OPER";
                break;
            case 8:
                strFieldName = "UPDATE_CONSIGNOR";
                break;
            case 9:
                strFieldName = "UPDATE_TIME";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "CUST_MANAGER_AUTHORIZE_ID":
                return Schema.TYPE_LONG;
            case "MNGORG_ID":
                return Schema.TYPE_LONG;
            case "AGENCY_ID":
                return Schema.TYPE_LONG;
            case "CUST_MANAGER_ID":
                return Schema.TYPE_LONG;
            case "INSERT_OPER":
                return Schema.TYPE_STRING;
            case "INSERT_CONSIGNOR":
                return Schema.TYPE_STRING;
            case "INSERT_TIME":
                return Schema.TYPE_STRING;
            case "UPDATE_OPER":
                return Schema.TYPE_STRING;
            case "UPDATE_CONSIGNOR":
                return Schema.TYPE_STRING;
            case "UPDATE_TIME":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_LONG;
            case 1:
                return Schema.TYPE_LONG;
            case 2:
                return Schema.TYPE_LONG;
            case 3:
                return Schema.TYPE_LONG;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_STRING;
            case 6:
                return Schema.TYPE_STRING;
            case 7:
                return Schema.TYPE_STRING;
            case 8:
                return Schema.TYPE_STRING;
            case 9:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("CUST_MANAGER_AUTHORIZE_ID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CUST_MANAGER_AUTHORIZE_ID));
        }
        if (FCode.equalsIgnoreCase("MNGORG_ID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MNGORG_ID));
        }
        if (FCode.equalsIgnoreCase("AGENCY_ID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AGENCY_ID));
        }
        if (FCode.equalsIgnoreCase("CUST_MANAGER_ID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CUST_MANAGER_ID));
        }
        if (FCode.equalsIgnoreCase("INSERT_OPER")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(INSERT_OPER));
        }
        if (FCode.equalsIgnoreCase("INSERT_CONSIGNOR")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(INSERT_CONSIGNOR));
        }
        if (FCode.equalsIgnoreCase("INSERT_TIME")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(INSERT_TIME));
        }
        if (FCode.equalsIgnoreCase("UPDATE_OPER")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(UPDATE_OPER));
        }
        if (FCode.equalsIgnoreCase("UPDATE_CONSIGNOR")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(UPDATE_CONSIGNOR));
        }
        if (FCode.equalsIgnoreCase("UPDATE_TIME")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(UPDATE_TIME));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = String.valueOf(CUST_MANAGER_AUTHORIZE_ID);
                break;
            case 1:
                strFieldValue = String.valueOf(MNGORG_ID);
                break;
            case 2:
                strFieldValue = String.valueOf(AGENCY_ID);
                break;
            case 3:
                strFieldValue = String.valueOf(CUST_MANAGER_ID);
                break;
            case 4:
                strFieldValue = String.valueOf(INSERT_OPER);
                break;
            case 5:
                strFieldValue = String.valueOf(INSERT_CONSIGNOR);
                break;
            case 6:
                strFieldValue = String.valueOf(INSERT_TIME);
                break;
            case 7:
                strFieldValue = String.valueOf(UPDATE_OPER);
                break;
            case 8:
                strFieldValue = String.valueOf(UPDATE_CONSIGNOR);
                break;
            case 9:
                strFieldValue = String.valueOf(UPDATE_TIME);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("CUST_MANAGER_AUTHORIZE_ID")) {
            if( FValue != null && !FValue.equals("")) {
                CUST_MANAGER_AUTHORIZE_ID = new Long(FValue).longValue();
            }
        }
        if (FCode.equalsIgnoreCase("MNGORG_ID")) {
            if( FValue != null && !FValue.equals("")) {
                MNGORG_ID = new Long(FValue).longValue();
            }
        }
        if (FCode.equalsIgnoreCase("AGENCY_ID")) {
            if( FValue != null && !FValue.equals("")) {
                AGENCY_ID = new Long(FValue).longValue();
            }
        }
        if (FCode.equalsIgnoreCase("CUST_MANAGER_ID")) {
            if( FValue != null && !FValue.equals("")) {
                CUST_MANAGER_ID = new Long(FValue).longValue();
            }
        }
        if (FCode.equalsIgnoreCase("INSERT_OPER")) {
            if( FValue != null && !FValue.equals(""))
            {
                INSERT_OPER = FValue.trim();
            }
            else
                INSERT_OPER = null;
        }
        if (FCode.equalsIgnoreCase("INSERT_CONSIGNOR")) {
            if( FValue != null && !FValue.equals(""))
            {
                INSERT_CONSIGNOR = FValue.trim();
            }
            else
                INSERT_CONSIGNOR = null;
        }
        if (FCode.equalsIgnoreCase("INSERT_TIME")) {
            if( FValue != null && !FValue.equals(""))
            {
                INSERT_TIME = FValue.trim();
            }
            else
                INSERT_TIME = null;
        }
        if (FCode.equalsIgnoreCase("UPDATE_OPER")) {
            if( FValue != null && !FValue.equals(""))
            {
                UPDATE_OPER = FValue.trim();
            }
            else
                UPDATE_OPER = null;
        }
        if (FCode.equalsIgnoreCase("UPDATE_CONSIGNOR")) {
            if( FValue != null && !FValue.equals(""))
            {
                UPDATE_CONSIGNOR = FValue.trim();
            }
            else
                UPDATE_CONSIGNOR = null;
        }
        if (FCode.equalsIgnoreCase("UPDATE_TIME")) {
            if( FValue != null && !FValue.equals(""))
            {
                UPDATE_TIME = FValue.trim();
            }
            else
                UPDATE_TIME = null;
        }
        return true;
    }


    public String toString() {
    return "T_CUST_MANAGER_ORG_AUTHORIZEPojo [" +
            "CUST_MANAGER_AUTHORIZE_ID="+CUST_MANAGER_AUTHORIZE_ID +
            ", MNGORG_ID="+MNGORG_ID +
            ", AGENCY_ID="+AGENCY_ID +
            ", CUST_MANAGER_ID="+CUST_MANAGER_ID +
            ", INSERT_OPER="+INSERT_OPER +
            ", INSERT_CONSIGNOR="+INSERT_CONSIGNOR +
            ", INSERT_TIME="+INSERT_TIME +
            ", UPDATE_OPER="+UPDATE_OPER +
            ", UPDATE_CONSIGNOR="+UPDATE_CONSIGNOR +
            ", UPDATE_TIME="+UPDATE_TIME +"]";
    }
}
