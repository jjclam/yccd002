/**
 * Copyright (c) 2018 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.entity;

import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.Pojo;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;

import java.io.Serializable;

/**
 * <p>ClassName: LJAGetPojo </p>
 * <p>Copyright: Copyright (c) 2018</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2018-09-07
 */
public class LJAGetPojo implements Pojo,Serializable {
    // @Field
    /** 实付号码 */
    private String ActuGetNo;
    /** 其它号码 */
    private String OtherNo;
    /** 其它号码类型 */
    private String OtherNoType;
    /** 交费方式 */
    private String PayMode;
    /** 银行在途标志 */
    private String BankOnTheWayFlag;
    /** 银行转帐成功标记 */
    private String BankSuccFlag;
    /** 送银行次数 */
    private int SendBankCount;
    /** 管理机构 */
    private String ManageCom;
    /** 代理机构 */
    private String AgentCom;
    /** 代理机构内部分类 */
    private String AgentType;
    /** 代理人编码 */
    private String AgentCode;
    /** 代理人组别 */
    private String AgentGroup;
    /** 帐户名 */
    private String AccName;
    /** 最早付费日期 */
    private String  StartGetDate;
    /** 投保人客户号码 */
    private String AppntNo;
    /** 总给付金额 */
    private double SumGetMoney;
    /** 销售渠道 */
    private String SaleChnl;
    /** 应付日期 */
    private String  ShouldDate;
    /** 财务到帐日期 */
    private String  EnterAccDate;
    /** 财务确认日期 */
    private String  ConfDate;
    /** 复核人编码 */
    private String ApproveCode;
    /** 复核日期 */
    private String  ApproveDate;
    /** 给付通知书号码 */
    private String GetNoticeNo;
    /** 银行编码 */
    private String BankCode;
    /** 银行帐号 */
    private String BankAccNo;
    /** 领取人 */
    private String Drawer;
    /** 领取人身份证号 */
    private String DrawerID;
    /** 流水号 */
    private String SerialNo;
    /** 操作员 */
    private String Operator;
    /** 入机日期 */
    private String  MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 最后一次修改日期 */
    private String  ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime;
    /** 实际领取人 */
    private String ActualDrawer;
    /** 实际领取人身份证号 */
    private String ActualDrawerID;
    /** 服务机构 */
    private String PolicyCom;
    /** Inbankcode */
    private String InBankCode;
    /** Inbankaccno */
    private String InBankAccNo;
    /** 定期结算标志 */
    private String BalanceOnTime;


    public static final int FIELDNUM = 39;    // 数据库表的字段个数

    private FDate fDate = new FDate();        // 处理日期
    public String getActuGetNo() {
        return ActuGetNo;
    }
    public void setActuGetNo(String aActuGetNo) {
        ActuGetNo = aActuGetNo;
    }
    public String getOtherNo() {
        return OtherNo;
    }
    public void setOtherNo(String aOtherNo) {
        OtherNo = aOtherNo;
    }
    public String getOtherNoType() {
        return OtherNoType;
    }
    public void setOtherNoType(String aOtherNoType) {
        OtherNoType = aOtherNoType;
    }
    public String getPayMode() {
        return PayMode;
    }
    public void setPayMode(String aPayMode) {
        PayMode = aPayMode;
    }
    public String getBankOnTheWayFlag() {
        return BankOnTheWayFlag;
    }
    public void setBankOnTheWayFlag(String aBankOnTheWayFlag) {
        BankOnTheWayFlag = aBankOnTheWayFlag;
    }
    public String getBankSuccFlag() {
        return BankSuccFlag;
    }
    public void setBankSuccFlag(String aBankSuccFlag) {
        BankSuccFlag = aBankSuccFlag;
    }
    public int getSendBankCount() {
        return SendBankCount;
    }
    public void setSendBankCount(int aSendBankCount) {
        SendBankCount = aSendBankCount;
    }
    public void setSendBankCount(String aSendBankCount) {
        if (aSendBankCount != null && !aSendBankCount.equals("")) {
            Integer tInteger = new Integer(aSendBankCount);
            int i = tInteger.intValue();
            SendBankCount = i;
        }
    }

    public String getManageCom() {
        return ManageCom;
    }
    public void setManageCom(String aManageCom) {
        ManageCom = aManageCom;
    }
    public String getAgentCom() {
        return AgentCom;
    }
    public void setAgentCom(String aAgentCom) {
        AgentCom = aAgentCom;
    }
    public String getAgentType() {
        return AgentType;
    }
    public void setAgentType(String aAgentType) {
        AgentType = aAgentType;
    }
    public String getAgentCode() {
        return AgentCode;
    }
    public void setAgentCode(String aAgentCode) {
        AgentCode = aAgentCode;
    }
    public String getAgentGroup() {
        return AgentGroup;
    }
    public void setAgentGroup(String aAgentGroup) {
        AgentGroup = aAgentGroup;
    }
    public String getAccName() {
        return AccName;
    }
    public void setAccName(String aAccName) {
        AccName = aAccName;
    }
    public String getStartGetDate() {
        return StartGetDate;
    }
    public void setStartGetDate(String aStartGetDate) {
        StartGetDate = aStartGetDate;
    }
    public String getAppntNo() {
        return AppntNo;
    }
    public void setAppntNo(String aAppntNo) {
        AppntNo = aAppntNo;
    }
    public double getSumGetMoney() {
        return SumGetMoney;
    }
    public void setSumGetMoney(double aSumGetMoney) {
        SumGetMoney = aSumGetMoney;
    }
    public void setSumGetMoney(String aSumGetMoney) {
        if (aSumGetMoney != null && !aSumGetMoney.equals("")) {
            Double tDouble = new Double(aSumGetMoney);
            double d = tDouble.doubleValue();
            SumGetMoney = d;
        }
    }

    public String getSaleChnl() {
        return SaleChnl;
    }
    public void setSaleChnl(String aSaleChnl) {
        SaleChnl = aSaleChnl;
    }
    public String getShouldDate() {
        return ShouldDate;
    }
    public void setShouldDate(String aShouldDate) {
        ShouldDate = aShouldDate;
    }
    public String getEnterAccDate() {
        return EnterAccDate;
    }
    public void setEnterAccDate(String aEnterAccDate) {
        EnterAccDate = aEnterAccDate;
    }
    public String getConfDate() {
        return ConfDate;
    }
    public void setConfDate(String aConfDate) {
        ConfDate = aConfDate;
    }
    public String getApproveCode() {
        return ApproveCode;
    }
    public void setApproveCode(String aApproveCode) {
        ApproveCode = aApproveCode;
    }
    public String getApproveDate() {
        return ApproveDate;
    }
    public void setApproveDate(String aApproveDate) {
        ApproveDate = aApproveDate;
    }
    public String getGetNoticeNo() {
        return GetNoticeNo;
    }
    public void setGetNoticeNo(String aGetNoticeNo) {
        GetNoticeNo = aGetNoticeNo;
    }
    public String getBankCode() {
        return BankCode;
    }
    public void setBankCode(String aBankCode) {
        BankCode = aBankCode;
    }
    public String getBankAccNo() {
        return BankAccNo;
    }
    public void setBankAccNo(String aBankAccNo) {
        BankAccNo = aBankAccNo;
    }
    public String getDrawer() {
        return Drawer;
    }
    public void setDrawer(String aDrawer) {
        Drawer = aDrawer;
    }
    public String getDrawerID() {
        return DrawerID;
    }
    public void setDrawerID(String aDrawerID) {
        DrawerID = aDrawerID;
    }
    public String getSerialNo() {
        return SerialNo;
    }
    public void setSerialNo(String aSerialNo) {
        SerialNo = aSerialNo;
    }
    public String getOperator() {
        return Operator;
    }
    public void setOperator(String aOperator) {
        Operator = aOperator;
    }
    public String getMakeDate() {
        return MakeDate;
    }
    public void setMakeDate(String aMakeDate) {
        MakeDate = aMakeDate;
    }
    public String getMakeTime() {
        return MakeTime;
    }
    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }
    public String getModifyDate() {
        return ModifyDate;
    }
    public void setModifyDate(String aModifyDate) {
        ModifyDate = aModifyDate;
    }
    public String getModifyTime() {
        return ModifyTime;
    }
    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }
    public String getActualDrawer() {
        return ActualDrawer;
    }
    public void setActualDrawer(String aActualDrawer) {
        ActualDrawer = aActualDrawer;
    }
    public String getActualDrawerID() {
        return ActualDrawerID;
    }
    public void setActualDrawerID(String aActualDrawerID) {
        ActualDrawerID = aActualDrawerID;
    }
    public String getPolicyCom() {
        return PolicyCom;
    }
    public void setPolicyCom(String aPolicyCom) {
        PolicyCom = aPolicyCom;
    }
    public String getInBankCode() {
        return InBankCode;
    }
    public void setInBankCode(String aInBankCode) {
        InBankCode = aInBankCode;
    }
    public String getInBankAccNo() {
        return InBankAccNo;
    }
    public void setInBankAccNo(String aInBankAccNo) {
        InBankAccNo = aInBankAccNo;
    }
    public String getBalanceOnTime() {
        return BalanceOnTime;
    }
    public void setBalanceOnTime(String aBalanceOnTime) {
        BalanceOnTime = aBalanceOnTime;
    }

    /**
     * 取得Schema拥有字段的数量
     * @return: int
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("ActuGetNo") ) {
            return 0;
        }
        if( strFieldName.equals("OtherNo") ) {
            return 1;
        }
        if( strFieldName.equals("OtherNoType") ) {
            return 2;
        }
        if( strFieldName.equals("PayMode") ) {
            return 3;
        }
        if( strFieldName.equals("BankOnTheWayFlag") ) {
            return 4;
        }
        if( strFieldName.equals("BankSuccFlag") ) {
            return 5;
        }
        if( strFieldName.equals("SendBankCount") ) {
            return 6;
        }
        if( strFieldName.equals("ManageCom") ) {
            return 7;
        }
        if( strFieldName.equals("AgentCom") ) {
            return 8;
        }
        if( strFieldName.equals("AgentType") ) {
            return 9;
        }
        if( strFieldName.equals("AgentCode") ) {
            return 10;
        }
        if( strFieldName.equals("AgentGroup") ) {
            return 11;
        }
        if( strFieldName.equals("AccName") ) {
            return 12;
        }
        if( strFieldName.equals("StartGetDate") ) {
            return 13;
        }
        if( strFieldName.equals("AppntNo") ) {
            return 14;
        }
        if( strFieldName.equals("SumGetMoney") ) {
            return 15;
        }
        if( strFieldName.equals("SaleChnl") ) {
            return 16;
        }
        if( strFieldName.equals("ShouldDate") ) {
            return 17;
        }
        if( strFieldName.equals("EnterAccDate") ) {
            return 18;
        }
        if( strFieldName.equals("ConfDate") ) {
            return 19;
        }
        if( strFieldName.equals("ApproveCode") ) {
            return 20;
        }
        if( strFieldName.equals("ApproveDate") ) {
            return 21;
        }
        if( strFieldName.equals("GetNoticeNo") ) {
            return 22;
        }
        if( strFieldName.equals("BankCode") ) {
            return 23;
        }
        if( strFieldName.equals("BankAccNo") ) {
            return 24;
        }
        if( strFieldName.equals("Drawer") ) {
            return 25;
        }
        if( strFieldName.equals("DrawerID") ) {
            return 26;
        }
        if( strFieldName.equals("SerialNo") ) {
            return 27;
        }
        if( strFieldName.equals("Operator") ) {
            return 28;
        }
        if( strFieldName.equals("MakeDate") ) {
            return 29;
        }
        if( strFieldName.equals("MakeTime") ) {
            return 30;
        }
        if( strFieldName.equals("ModifyDate") ) {
            return 31;
        }
        if( strFieldName.equals("ModifyTime") ) {
            return 32;
        }
        if( strFieldName.equals("ActualDrawer") ) {
            return 33;
        }
        if( strFieldName.equals("ActualDrawerID") ) {
            return 34;
        }
        if( strFieldName.equals("PolicyCom") ) {
            return 35;
        }
        if( strFieldName.equals("InBankCode") ) {
            return 36;
        }
        if( strFieldName.equals("InBankAccNo") ) {
            return 37;
        }
        if( strFieldName.equals("BalanceOnTime") ) {
            return 38;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     * @param: nFieldIndex int
     * @return: String
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "ActuGetNo";
                break;
            case 1:
                strFieldName = "OtherNo";
                break;
            case 2:
                strFieldName = "OtherNoType";
                break;
            case 3:
                strFieldName = "PayMode";
                break;
            case 4:
                strFieldName = "BankOnTheWayFlag";
                break;
            case 5:
                strFieldName = "BankSuccFlag";
                break;
            case 6:
                strFieldName = "SendBankCount";
                break;
            case 7:
                strFieldName = "ManageCom";
                break;
            case 8:
                strFieldName = "AgentCom";
                break;
            case 9:
                strFieldName = "AgentType";
                break;
            case 10:
                strFieldName = "AgentCode";
                break;
            case 11:
                strFieldName = "AgentGroup";
                break;
            case 12:
                strFieldName = "AccName";
                break;
            case 13:
                strFieldName = "StartGetDate";
                break;
            case 14:
                strFieldName = "AppntNo";
                break;
            case 15:
                strFieldName = "SumGetMoney";
                break;
            case 16:
                strFieldName = "SaleChnl";
                break;
            case 17:
                strFieldName = "ShouldDate";
                break;
            case 18:
                strFieldName = "EnterAccDate";
                break;
            case 19:
                strFieldName = "ConfDate";
                break;
            case 20:
                strFieldName = "ApproveCode";
                break;
            case 21:
                strFieldName = "ApproveDate";
                break;
            case 22:
                strFieldName = "GetNoticeNo";
                break;
            case 23:
                strFieldName = "BankCode";
                break;
            case 24:
                strFieldName = "BankAccNo";
                break;
            case 25:
                strFieldName = "Drawer";
                break;
            case 26:
                strFieldName = "DrawerID";
                break;
            case 27:
                strFieldName = "SerialNo";
                break;
            case 28:
                strFieldName = "Operator";
                break;
            case 29:
                strFieldName = "MakeDate";
                break;
            case 30:
                strFieldName = "MakeTime";
                break;
            case 31:
                strFieldName = "ModifyDate";
                break;
            case 32:
                strFieldName = "ModifyTime";
                break;
            case 33:
                strFieldName = "ActualDrawer";
                break;
            case 34:
                strFieldName = "ActualDrawerID";
                break;
            case 35:
                strFieldName = "PolicyCom";
                break;
            case 36:
                strFieldName = "InBankCode";
                break;
            case 37:
                strFieldName = "InBankAccNo";
                break;
            case 38:
                strFieldName = "BalanceOnTime";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "ACTUGETNO":
                return Schema.TYPE_STRING;
            case "OTHERNO":
                return Schema.TYPE_STRING;
            case "OTHERNOTYPE":
                return Schema.TYPE_STRING;
            case "PAYMODE":
                return Schema.TYPE_STRING;
            case "BANKONTHEWAYFLAG":
                return Schema.TYPE_STRING;
            case "BANKSUCCFLAG":
                return Schema.TYPE_STRING;
            case "SENDBANKCOUNT":
                return Schema.TYPE_INT;
            case "MANAGECOM":
                return Schema.TYPE_STRING;
            case "AGENTCOM":
                return Schema.TYPE_STRING;
            case "AGENTTYPE":
                return Schema.TYPE_STRING;
            case "AGENTCODE":
                return Schema.TYPE_STRING;
            case "AGENTGROUP":
                return Schema.TYPE_STRING;
            case "ACCNAME":
                return Schema.TYPE_STRING;
            case "STARTGETDATE":
                return Schema.TYPE_STRING;
            case "APPNTNO":
                return Schema.TYPE_STRING;
            case "SUMGETMONEY":
                return Schema.TYPE_DOUBLE;
            case "SALECHNL":
                return Schema.TYPE_STRING;
            case "SHOULDDATE":
                return Schema.TYPE_STRING;
            case "ENTERACCDATE":
                return Schema.TYPE_STRING;
            case "CONFDATE":
                return Schema.TYPE_STRING;
            case "APPROVECODE":
                return Schema.TYPE_STRING;
            case "APPROVEDATE":
                return Schema.TYPE_STRING;
            case "GETNOTICENO":
                return Schema.TYPE_STRING;
            case "BANKCODE":
                return Schema.TYPE_STRING;
            case "BANKACCNO":
                return Schema.TYPE_STRING;
            case "DRAWER":
                return Schema.TYPE_STRING;
            case "DRAWERID":
                return Schema.TYPE_STRING;
            case "SERIALNO":
                return Schema.TYPE_STRING;
            case "OPERATOR":
                return Schema.TYPE_STRING;
            case "MAKEDATE":
                return Schema.TYPE_STRING;
            case "MAKETIME":
                return Schema.TYPE_STRING;
            case "MODIFYDATE":
                return Schema.TYPE_STRING;
            case "MODIFYTIME":
                return Schema.TYPE_STRING;
            case "ACTUALDRAWER":
                return Schema.TYPE_STRING;
            case "ACTUALDRAWERID":
                return Schema.TYPE_STRING;
            case "POLICYCOM":
                return Schema.TYPE_STRING;
            case "INBANKCODE":
                return Schema.TYPE_STRING;
            case "INBANKACCNO":
                return Schema.TYPE_STRING;
            case "BALANCEONTIME":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: nFieldIndex int
     * @return: int
     **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_STRING;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_STRING;
            case 6:
                return Schema.TYPE_INT;
            case 7:
                return Schema.TYPE_STRING;
            case 8:
                return Schema.TYPE_STRING;
            case 9:
                return Schema.TYPE_STRING;
            case 10:
                return Schema.TYPE_STRING;
            case 11:
                return Schema.TYPE_STRING;
            case 12:
                return Schema.TYPE_STRING;
            case 13:
                return Schema.TYPE_STRING;
            case 14:
                return Schema.TYPE_STRING;
            case 15:
                return Schema.TYPE_DOUBLE;
            case 16:
                return Schema.TYPE_STRING;
            case 17:
                return Schema.TYPE_STRING;
            case 18:
                return Schema.TYPE_STRING;
            case 19:
                return Schema.TYPE_STRING;
            case 20:
                return Schema.TYPE_STRING;
            case 21:
                return Schema.TYPE_STRING;
            case 22:
                return Schema.TYPE_STRING;
            case 23:
                return Schema.TYPE_STRING;
            case 24:
                return Schema.TYPE_STRING;
            case 25:
                return Schema.TYPE_STRING;
            case 26:
                return Schema.TYPE_STRING;
            case 27:
                return Schema.TYPE_STRING;
            case 28:
                return Schema.TYPE_STRING;
            case 29:
                return Schema.TYPE_STRING;
            case 30:
                return Schema.TYPE_STRING;
            case 31:
                return Schema.TYPE_STRING;
            case 32:
                return Schema.TYPE_STRING;
            case 33:
                return Schema.TYPE_STRING;
            case 34:
                return Schema.TYPE_STRING;
            case 35:
                return Schema.TYPE_STRING;
            case 36:
                return Schema.TYPE_STRING;
            case 37:
                return Schema.TYPE_STRING;
            case 38:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode String 希望取得的字段名
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("ActuGetNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ActuGetNo));
        }
        if (FCode.equalsIgnoreCase("OtherNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OtherNo));
        }
        if (FCode.equalsIgnoreCase("OtherNoType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OtherNoType));
        }
        if (FCode.equalsIgnoreCase("PayMode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PayMode));
        }
        if (FCode.equalsIgnoreCase("BankOnTheWayFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BankOnTheWayFlag));
        }
        if (FCode.equalsIgnoreCase("BankSuccFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BankSuccFlag));
        }
        if (FCode.equalsIgnoreCase("SendBankCount")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SendBankCount));
        }
        if (FCode.equalsIgnoreCase("ManageCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
        }
        if (FCode.equalsIgnoreCase("AgentCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentCom));
        }
        if (FCode.equalsIgnoreCase("AgentType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentType));
        }
        if (FCode.equalsIgnoreCase("AgentCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentCode));
        }
        if (FCode.equalsIgnoreCase("AgentGroup")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentGroup));
        }
        if (FCode.equalsIgnoreCase("AccName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AccName));
        }
        if (FCode.equalsIgnoreCase("StartGetDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StartGetDate));
        }
        if (FCode.equalsIgnoreCase("AppntNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AppntNo));
        }
        if (FCode.equalsIgnoreCase("SumGetMoney")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SumGetMoney));
        }
        if (FCode.equalsIgnoreCase("SaleChnl")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SaleChnl));
        }
        if (FCode.equalsIgnoreCase("ShouldDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ShouldDate));
        }
        if (FCode.equalsIgnoreCase("EnterAccDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(EnterAccDate));
        }
        if (FCode.equalsIgnoreCase("ConfDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ConfDate));
        }
        if (FCode.equalsIgnoreCase("ApproveCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ApproveCode));
        }
        if (FCode.equalsIgnoreCase("ApproveDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ApproveDate));
        }
        if (FCode.equalsIgnoreCase("GetNoticeNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GetNoticeNo));
        }
        if (FCode.equalsIgnoreCase("BankCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BankCode));
        }
        if (FCode.equalsIgnoreCase("BankAccNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BankAccNo));
        }
        if (FCode.equalsIgnoreCase("Drawer")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Drawer));
        }
        if (FCode.equalsIgnoreCase("DrawerID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DrawerID));
        }
        if (FCode.equalsIgnoreCase("SerialNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SerialNo));
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeDate));
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyDate));
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (FCode.equalsIgnoreCase("ActualDrawer")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ActualDrawer));
        }
        if (FCode.equalsIgnoreCase("ActualDrawerID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ActualDrawerID));
        }
        if (FCode.equalsIgnoreCase("PolicyCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PolicyCom));
        }
        if (FCode.equalsIgnoreCase("InBankCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InBankCode));
        }
        if (FCode.equalsIgnoreCase("InBankAccNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InBankAccNo));
        }
        if (FCode.equalsIgnoreCase("BalanceOnTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BalanceOnTime));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex int 指定的字段索引值
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = String.valueOf(ActuGetNo);
                break;
            case 1:
                strFieldValue = String.valueOf(OtherNo);
                break;
            case 2:
                strFieldValue = String.valueOf(OtherNoType);
                break;
            case 3:
                strFieldValue = String.valueOf(PayMode);
                break;
            case 4:
                strFieldValue = String.valueOf(BankOnTheWayFlag);
                break;
            case 5:
                strFieldValue = String.valueOf(BankSuccFlag);
                break;
            case 6:
                strFieldValue = String.valueOf(SendBankCount);
                break;
            case 7:
                strFieldValue = String.valueOf(ManageCom);
                break;
            case 8:
                strFieldValue = String.valueOf(AgentCom);
                break;
            case 9:
                strFieldValue = String.valueOf(AgentType);
                break;
            case 10:
                strFieldValue = String.valueOf(AgentCode);
                break;
            case 11:
                strFieldValue = String.valueOf(AgentGroup);
                break;
            case 12:
                strFieldValue = String.valueOf(AccName);
                break;
            case 13:
                strFieldValue = String.valueOf(StartGetDate);
                break;
            case 14:
                strFieldValue = String.valueOf(AppntNo);
                break;
            case 15:
                strFieldValue = String.valueOf(SumGetMoney);
                break;
            case 16:
                strFieldValue = String.valueOf(SaleChnl);
                break;
            case 17:
                strFieldValue = String.valueOf(ShouldDate);
                break;
            case 18:
                strFieldValue = String.valueOf(EnterAccDate);
                break;
            case 19:
                strFieldValue = String.valueOf(ConfDate);
                break;
            case 20:
                strFieldValue = String.valueOf(ApproveCode);
                break;
            case 21:
                strFieldValue = String.valueOf(ApproveDate);
                break;
            case 22:
                strFieldValue = String.valueOf(GetNoticeNo);
                break;
            case 23:
                strFieldValue = String.valueOf(BankCode);
                break;
            case 24:
                strFieldValue = String.valueOf(BankAccNo);
                break;
            case 25:
                strFieldValue = String.valueOf(Drawer);
                break;
            case 26:
                strFieldValue = String.valueOf(DrawerID);
                break;
            case 27:
                strFieldValue = String.valueOf(SerialNo);
                break;
            case 28:
                strFieldValue = String.valueOf(Operator);
                break;
            case 29:
                strFieldValue = String.valueOf(MakeDate);
                break;
            case 30:
                strFieldValue = String.valueOf(MakeTime);
                break;
            case 31:
                strFieldValue = String.valueOf(ModifyDate);
                break;
            case 32:
                strFieldValue = String.valueOf(ModifyTime);
                break;
            case 33:
                strFieldValue = String.valueOf(ActualDrawer);
                break;
            case 34:
                strFieldValue = String.valueOf(ActualDrawerID);
                break;
            case 35:
                strFieldValue = String.valueOf(PolicyCom);
                break;
            case 36:
                strFieldValue = String.valueOf(InBankCode);
                break;
            case 37:
                strFieldValue = String.valueOf(InBankAccNo);
                break;
            case 38:
                strFieldValue = String.valueOf(BalanceOnTime);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode String 需要赋值的对象
     * @param: FValue String 要赋的值
     * @return: boolean
     **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("ActuGetNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                ActuGetNo = FValue.trim();
            }
            else
                ActuGetNo = null;
        }
        if (FCode.equalsIgnoreCase("OtherNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                OtherNo = FValue.trim();
            }
            else
                OtherNo = null;
        }
        if (FCode.equalsIgnoreCase("OtherNoType")) {
            if( FValue != null && !FValue.equals(""))
            {
                OtherNoType = FValue.trim();
            }
            else
                OtherNoType = null;
        }
        if (FCode.equalsIgnoreCase("PayMode")) {
            if( FValue != null && !FValue.equals(""))
            {
                PayMode = FValue.trim();
            }
            else
                PayMode = null;
        }
        if (FCode.equalsIgnoreCase("BankOnTheWayFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                BankOnTheWayFlag = FValue.trim();
            }
            else
                BankOnTheWayFlag = null;
        }
        if (FCode.equalsIgnoreCase("BankSuccFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                BankSuccFlag = FValue.trim();
            }
            else
                BankSuccFlag = null;
        }
        if (FCode.equalsIgnoreCase("SendBankCount")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                SendBankCount = i;
            }
        }
        if (FCode.equalsIgnoreCase("ManageCom")) {
            if( FValue != null && !FValue.equals(""))
            {
                ManageCom = FValue.trim();
            }
            else
                ManageCom = null;
        }
        if (FCode.equalsIgnoreCase("AgentCom")) {
            if( FValue != null && !FValue.equals(""))
            {
                AgentCom = FValue.trim();
            }
            else
                AgentCom = null;
        }
        if (FCode.equalsIgnoreCase("AgentType")) {
            if( FValue != null && !FValue.equals(""))
            {
                AgentType = FValue.trim();
            }
            else
                AgentType = null;
        }
        if (FCode.equalsIgnoreCase("AgentCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                AgentCode = FValue.trim();
            }
            else
                AgentCode = null;
        }
        if (FCode.equalsIgnoreCase("AgentGroup")) {
            if( FValue != null && !FValue.equals(""))
            {
                AgentGroup = FValue.trim();
            }
            else
                AgentGroup = null;
        }
        if (FCode.equalsIgnoreCase("AccName")) {
            if( FValue != null && !FValue.equals(""))
            {
                AccName = FValue.trim();
            }
            else
                AccName = null;
        }
        if (FCode.equalsIgnoreCase("StartGetDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                StartGetDate = FValue.trim();
            }
            else
                StartGetDate = null;
        }
        if (FCode.equalsIgnoreCase("AppntNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                AppntNo = FValue.trim();
            }
            else
                AppntNo = null;
        }
        if (FCode.equalsIgnoreCase("SumGetMoney")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                SumGetMoney = d;
            }
        }
        if (FCode.equalsIgnoreCase("SaleChnl")) {
            if( FValue != null && !FValue.equals(""))
            {
                SaleChnl = FValue.trim();
            }
            else
                SaleChnl = null;
        }
        if (FCode.equalsIgnoreCase("ShouldDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                ShouldDate = FValue.trim();
            }
            else
                ShouldDate = null;
        }
        if (FCode.equalsIgnoreCase("EnterAccDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                EnterAccDate = FValue.trim();
            }
            else
                EnterAccDate = null;
        }
        if (FCode.equalsIgnoreCase("ConfDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                ConfDate = FValue.trim();
            }
            else
                ConfDate = null;
        }
        if (FCode.equalsIgnoreCase("ApproveCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                ApproveCode = FValue.trim();
            }
            else
                ApproveCode = null;
        }
        if (FCode.equalsIgnoreCase("ApproveDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                ApproveDate = FValue.trim();
            }
            else
                ApproveDate = null;
        }
        if (FCode.equalsIgnoreCase("GetNoticeNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                GetNoticeNo = FValue.trim();
            }
            else
                GetNoticeNo = null;
        }
        if (FCode.equalsIgnoreCase("BankCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                BankCode = FValue.trim();
            }
            else
                BankCode = null;
        }
        if (FCode.equalsIgnoreCase("BankAccNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                BankAccNo = FValue.trim();
            }
            else
                BankAccNo = null;
        }
        if (FCode.equalsIgnoreCase("Drawer")) {
            if( FValue != null && !FValue.equals(""))
            {
                Drawer = FValue.trim();
            }
            else
                Drawer = null;
        }
        if (FCode.equalsIgnoreCase("DrawerID")) {
            if( FValue != null && !FValue.equals(""))
            {
                DrawerID = FValue.trim();
            }
            else
                DrawerID = null;
        }
        if (FCode.equalsIgnoreCase("SerialNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                SerialNo = FValue.trim();
            }
            else
                SerialNo = null;
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            if( FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
                Operator = null;
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeDate = FValue.trim();
            }
            else
                MakeDate = null;
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
                MakeTime = null;
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyDate = FValue.trim();
            }
            else
                ModifyDate = null;
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
                ModifyTime = null;
        }
        if (FCode.equalsIgnoreCase("ActualDrawer")) {
            if( FValue != null && !FValue.equals(""))
            {
                ActualDrawer = FValue.trim();
            }
            else
                ActualDrawer = null;
        }
        if (FCode.equalsIgnoreCase("ActualDrawerID")) {
            if( FValue != null && !FValue.equals(""))
            {
                ActualDrawerID = FValue.trim();
            }
            else
                ActualDrawerID = null;
        }
        if (FCode.equalsIgnoreCase("PolicyCom")) {
            if( FValue != null && !FValue.equals(""))
            {
                PolicyCom = FValue.trim();
            }
            else
                PolicyCom = null;
        }
        if (FCode.equalsIgnoreCase("InBankCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                InBankCode = FValue.trim();
            }
            else
                InBankCode = null;
        }
        if (FCode.equalsIgnoreCase("InBankAccNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                InBankAccNo = FValue.trim();
            }
            else
                InBankAccNo = null;
        }
        if (FCode.equalsIgnoreCase("BalanceOnTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                BalanceOnTime = FValue.trim();
            }
            else
                BalanceOnTime = null;
        }
        return true;
    }


    public String toString() {
        return "LJAGetPojo [" +
                "ActuGetNo="+ActuGetNo +
                ", OtherNo="+OtherNo +
                ", OtherNoType="+OtherNoType +
                ", PayMode="+PayMode +
                ", BankOnTheWayFlag="+BankOnTheWayFlag +
                ", BankSuccFlag="+BankSuccFlag +
                ", SendBankCount="+SendBankCount +
                ", ManageCom="+ManageCom +
                ", AgentCom="+AgentCom +
                ", AgentType="+AgentType +
                ", AgentCode="+AgentCode +
                ", AgentGroup="+AgentGroup +
                ", AccName="+AccName +
                ", StartGetDate="+StartGetDate +
                ", AppntNo="+AppntNo +
                ", SumGetMoney="+SumGetMoney +
                ", SaleChnl="+SaleChnl +
                ", ShouldDate="+ShouldDate +
                ", EnterAccDate="+EnterAccDate +
                ", ConfDate="+ConfDate +
                ", ApproveCode="+ApproveCode +
                ", ApproveDate="+ApproveDate +
                ", GetNoticeNo="+GetNoticeNo +
                ", BankCode="+BankCode +
                ", BankAccNo="+BankAccNo +
                ", Drawer="+Drawer +
                ", DrawerID="+DrawerID +
                ", SerialNo="+SerialNo +
                ", Operator="+Operator +
                ", MakeDate="+MakeDate +
                ", MakeTime="+MakeTime +
                ", ModifyDate="+ModifyDate +
                ", ModifyTime="+ModifyTime +
                ", ActualDrawer="+ActualDrawer +
                ", ActualDrawerID="+ActualDrawerID +
                ", PolicyCom="+PolicyCom +
                ", InBankCode="+InBankCode +
                ", InBankAccNo="+InBankAccNo +
                ", BalanceOnTime="+BalanceOnTime +"]";
    }
}
