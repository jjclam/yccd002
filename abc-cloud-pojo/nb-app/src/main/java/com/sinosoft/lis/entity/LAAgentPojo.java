/**
 * Copyright (c) 2017 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.entity;

import com.sinosoft.cloud.cache.annotation.RedisPrimaryHKey;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.Pojo;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;

import java.io.Serializable;

/**
 * <p>ClassName: LAAgentPojo </p>
 * <p>Copyright: Copyright (c) 2017</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2017-10-17
 */
public class LAAgentPojo implements Pojo,Serializable {
    // @Field
    /** 代理人编码 */
    @RedisPrimaryHKey
    private String AgentCode; 
    /** 代理人展业机构代码 */
    private String AgentGroup; 
    /** 管理机构 */
    private String ManageCom; 
    /** 密码 */
    private String Password; 
    /** 推荐报名编号 */
    private String EntryNo; 
    /** 姓名 */
    private String Name; 
    /** 性别 */
    private String Sex; 
    /** 出生日期 */
    private String  Birthday;
    /** 籍贯 */
    private String NativePlace; 
    /** 民族 */
    private String Nationality; 
    /** 婚姻状况 */
    private String Marriage; 
    /** 信用等级 */
    private String CreditGrade; 
    /** 家庭地址编码 */
    private String HomeAddressCode; 
    /** 家庭地址 */
    private String HomeAddress; 
    /** 通讯地址 */
    private String PostalAddress; 
    /** 邮政编码 */
    private String ZipCode; 
    /** 电话 */
    private String Phone; 
    /** 传呼 */
    private String BP; 
    /** 手机 */
    private String Mobile; 
    /** E_mail */
    private String EMail; 
    /** 结婚日期 */
    private String  MarriageDate;
    /** 身份证号码 */
    private String IDNo; 
    /** 来源地 */
    private String Source; 
    /** 血型 */
    private String BloodType; 
    /** 政治面貌 */
    private String PolityVisage; 
    /** 学历 */
    private String Degree; 
    /** 毕业院校 */
    private String GraduateSchool; 
    /** 专业 */
    private String Speciality; 
    /** 职称 */
    private String PostTitle; 
    /** 外语水平 */
    private String ForeignLevel; 
    /** 从业年限 */
    private int WorkAge; 
    /** 原工作单位 */
    private String OldCom; 
    /** 原职业 */
    private String OldOccupation; 
    /** 工作职务（体制） */
    private String HeadShip; 
    /** 推荐代理人 */
    private String RecommendAgent; 
    /** 工种/行业 */
    private String Business; 
    /** 销售资格 */
    private String SaleQuaf; 
    /** 代理人资格证号码 */
    private String QuafNo; 
    /** 证书开始日期 */
    private String  QuafStartDate;
    /** 证书结束日期 */
    private String  QuafEndDate;
    /** 展业证号码1 */
    private String DevNo1; 
    /** 展业证号码2 */
    private String DevNo2; 
    /** 聘用合同号码 */
    private String RetainContNo; 
    /** 代理人类别 */
    private String AgentKind; 
    /** 业务拓展级别 */
    private String DevGrade; 
    /** 内勤标志 */
    private String InsideFlag; 
    /** 是否专职标志 */
    private String FullTimeFlag; 
    /** 是否有待业证标志 */
    private String NoWorkFlag; 
    /** 培训日期 */
    private String  TrainDate;
    /** 录用日期 */
    private String  EmployDate;
    /** 转正日期 */
    private String  InDueFormDate;
    /** 离司日期 */
    private String  OutWorkDate;
    /** 推荐名编号2 */
    private String RecommendNo; 
    /** 担保人名称 */
    private String CautionerName; 
    /** 担保人性别 */
    private String CautionerSex; 
    /** 担保人身份证 */
    private String CautionerID; 
    /** 准离职日期 */
    private String  CautionerBirthday;
    /** 复核员 */
    private String Approver; 
    /** 档案调入日期（复核日期） */
    private String  ApproveDate;
    /** 保证金 */
    private double AssuMoney; 
    /** 备注 */
    private String Remark; 
    /** 代理人状态 */
    private String AgentState; 
    /** 档案标志位 */
    private String QualiPassFlag; 
    /** 销售资格标记 */
    private String SmokeFlag; 
    /** 户口所在地 */
    private String RgtAddress; 
    /** 银行编码 */
    private String BankCode; 
    /** 银行帐户 */
    private String BankAccNo; 
    /** 操作员代码 */
    private String Operator; 
    /** 入机日期 */
    private String  MakeDate;
    /** 入机时间 */
    private String MakeTime; 
    /** 最后一次修改日期 */
    private String  ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime; 
    /** 展业类型 */
    private String BranchType; 
    /** 培训期数 */
    private String TrainPeriods; 
    /** 代理人组别 */
    private String BranchCode; 
    /** 代理人年龄 */
    private int Age; 
    /** 所属渠道 */
    private String ChannelName; 
    /** 保证金收据号 */
    private String ReceiptNo; 
    /** 证件号码类型 */
    private String IDNoType; 
    /** 渠道 */
    private String BranchType2; 
    /** 培训通过标记 */
    private String TrainPassFlag; 
    /** 紧急联系人 */
    private String EmergentLink; 
    /** 紧急联系人电话 */
    private String EmergentPhone; 
    /** 劳动合同开始日期 */
    private String  RetainStartDate;
    /** 劳动合同截至日期 */
    private String  RetainEndDate;
    /** 司服是否领取标志 */
    private String TogaeFlag; 
    /** 档案编码 */
    private String ArchieveCode; 
    /** 附件 */
    private String Affix; 
    /** 附件名 */
    private String AffixName; 
    /** 开户银行地址 */
    private String BankAddress; 
    /** 电销渠道 */
    private String DXBranchtype2; 
    /** 电销中心 */
    private String DXSaleOrg; 


    public static final int FIELDNUM = 92;    // 数据库表的字段个数

    private FDate fDate = new FDate();        // 处理日期
    public String getAgentCode() {
        return AgentCode;
    }
    public void setAgentCode(String aAgentCode) {
        AgentCode = aAgentCode;
    }
    public String getAgentGroup() {
        return AgentGroup;
    }
    public void setAgentGroup(String aAgentGroup) {
        AgentGroup = aAgentGroup;
    }
    public String getManageCom() {
        return ManageCom;
    }
    public void setManageCom(String aManageCom) {
        ManageCom = aManageCom;
    }
    public String getPassword() {
        return Password;
    }
    public void setPassword(String aPassword) {
        Password = aPassword;
    }
    public String getEntryNo() {
        return EntryNo;
    }
    public void setEntryNo(String aEntryNo) {
        EntryNo = aEntryNo;
    }
    public String getName() {
        return Name;
    }
    public void setName(String aName) {
        Name = aName;
    }
    public String getSex() {
        return Sex;
    }
    public void setSex(String aSex) {
        Sex = aSex;
    }
    public String getBirthday() {
        return Birthday;
    }
    public void setBirthday(String aBirthday) {
        Birthday = aBirthday;
    }
    public String getNativePlace() {
        return NativePlace;
    }
    public void setNativePlace(String aNativePlace) {
        NativePlace = aNativePlace;
    }
    public String getNationality() {
        return Nationality;
    }
    public void setNationality(String aNationality) {
        Nationality = aNationality;
    }
    public String getMarriage() {
        return Marriage;
    }
    public void setMarriage(String aMarriage) {
        Marriage = aMarriage;
    }
    public String getCreditGrade() {
        return CreditGrade;
    }
    public void setCreditGrade(String aCreditGrade) {
        CreditGrade = aCreditGrade;
    }
    public String getHomeAddressCode() {
        return HomeAddressCode;
    }
    public void setHomeAddressCode(String aHomeAddressCode) {
        HomeAddressCode = aHomeAddressCode;
    }
    public String getHomeAddress() {
        return HomeAddress;
    }
    public void setHomeAddress(String aHomeAddress) {
        HomeAddress = aHomeAddress;
    }
    public String getPostalAddress() {
        return PostalAddress;
    }
    public void setPostalAddress(String aPostalAddress) {
        PostalAddress = aPostalAddress;
    }
    public String getZipCode() {
        return ZipCode;
    }
    public void setZipCode(String aZipCode) {
        ZipCode = aZipCode;
    }
    public String getPhone() {
        return Phone;
    }
    public void setPhone(String aPhone) {
        Phone = aPhone;
    }
    public String getBP() {
        return BP;
    }
    public void setBP(String aBP) {
        BP = aBP;
    }
    public String getMobile() {
        return Mobile;
    }
    public void setMobile(String aMobile) {
        Mobile = aMobile;
    }
    public String getEMail() {
        return EMail;
    }
    public void setEMail(String aEMail) {
        EMail = aEMail;
    }
    public String getMarriageDate() {
        return MarriageDate;
    }
    public void setMarriageDate(String aMarriageDate) {
        MarriageDate = aMarriageDate;
    }
    public String getIDNo() {
        return IDNo;
    }
    public void setIDNo(String aIDNo) {
        IDNo = aIDNo;
    }
    public String getSource() {
        return Source;
    }
    public void setSource(String aSource) {
        Source = aSource;
    }
    public String getBloodType() {
        return BloodType;
    }
    public void setBloodType(String aBloodType) {
        BloodType = aBloodType;
    }
    public String getPolityVisage() {
        return PolityVisage;
    }
    public void setPolityVisage(String aPolityVisage) {
        PolityVisage = aPolityVisage;
    }
    public String getDegree() {
        return Degree;
    }
    public void setDegree(String aDegree) {
        Degree = aDegree;
    }
    public String getGraduateSchool() {
        return GraduateSchool;
    }
    public void setGraduateSchool(String aGraduateSchool) {
        GraduateSchool = aGraduateSchool;
    }
    public String getSpeciality() {
        return Speciality;
    }
    public void setSpeciality(String aSpeciality) {
        Speciality = aSpeciality;
    }
    public String getPostTitle() {
        return PostTitle;
    }
    public void setPostTitle(String aPostTitle) {
        PostTitle = aPostTitle;
    }
    public String getForeignLevel() {
        return ForeignLevel;
    }
    public void setForeignLevel(String aForeignLevel) {
        ForeignLevel = aForeignLevel;
    }
    public int getWorkAge() {
        return WorkAge;
    }
    public void setWorkAge(int aWorkAge) {
        WorkAge = aWorkAge;
    }
    public void setWorkAge(String aWorkAge) {
        if (aWorkAge != null && !aWorkAge.equals("")) {
            Integer tInteger = new Integer(aWorkAge);
            int i = tInteger.intValue();
            WorkAge = i;
        }
    }

    public String getOldCom() {
        return OldCom;
    }
    public void setOldCom(String aOldCom) {
        OldCom = aOldCom;
    }
    public String getOldOccupation() {
        return OldOccupation;
    }
    public void setOldOccupation(String aOldOccupation) {
        OldOccupation = aOldOccupation;
    }
    public String getHeadShip() {
        return HeadShip;
    }
    public void setHeadShip(String aHeadShip) {
        HeadShip = aHeadShip;
    }
    public String getRecommendAgent() {
        return RecommendAgent;
    }
    public void setRecommendAgent(String aRecommendAgent) {
        RecommendAgent = aRecommendAgent;
    }
    public String getBusiness() {
        return Business;
    }
    public void setBusiness(String aBusiness) {
        Business = aBusiness;
    }
    public String getSaleQuaf() {
        return SaleQuaf;
    }
    public void setSaleQuaf(String aSaleQuaf) {
        SaleQuaf = aSaleQuaf;
    }
    public String getQuafNo() {
        return QuafNo;
    }
    public void setQuafNo(String aQuafNo) {
        QuafNo = aQuafNo;
    }
    public String getQuafStartDate() {
        return QuafStartDate;
    }
    public void setQuafStartDate(String aQuafStartDate) {
        QuafStartDate = aQuafStartDate;
    }
    public String getQuafEndDate() {
        return QuafEndDate;
    }
    public void setQuafEndDate(String aQuafEndDate) {
        QuafEndDate = aQuafEndDate;
    }
    public String getDevNo1() {
        return DevNo1;
    }
    public void setDevNo1(String aDevNo1) {
        DevNo1 = aDevNo1;
    }
    public String getDevNo2() {
        return DevNo2;
    }
    public void setDevNo2(String aDevNo2) {
        DevNo2 = aDevNo2;
    }
    public String getRetainContNo() {
        return RetainContNo;
    }
    public void setRetainContNo(String aRetainContNo) {
        RetainContNo = aRetainContNo;
    }
    public String getAgentKind() {
        return AgentKind;
    }
    public void setAgentKind(String aAgentKind) {
        AgentKind = aAgentKind;
    }
    public String getDevGrade() {
        return DevGrade;
    }
    public void setDevGrade(String aDevGrade) {
        DevGrade = aDevGrade;
    }
    public String getInsideFlag() {
        return InsideFlag;
    }
    public void setInsideFlag(String aInsideFlag) {
        InsideFlag = aInsideFlag;
    }
    public String getFullTimeFlag() {
        return FullTimeFlag;
    }
    public void setFullTimeFlag(String aFullTimeFlag) {
        FullTimeFlag = aFullTimeFlag;
    }
    public String getNoWorkFlag() {
        return NoWorkFlag;
    }
    public void setNoWorkFlag(String aNoWorkFlag) {
        NoWorkFlag = aNoWorkFlag;
    }
    public String getTrainDate() {
        return TrainDate;
    }
    public void setTrainDate(String aTrainDate) {
        TrainDate = aTrainDate;
    }
    public String getEmployDate() {
        return EmployDate;
    }
    public void setEmployDate(String aEmployDate) {
        EmployDate = aEmployDate;
    }
    public String getInDueFormDate() {
        return InDueFormDate;
    }
    public void setInDueFormDate(String aInDueFormDate) {
        InDueFormDate = aInDueFormDate;
    }
    public String getOutWorkDate() {
        return OutWorkDate;
    }
    public void setOutWorkDate(String aOutWorkDate) {
        OutWorkDate = aOutWorkDate;
    }
    public String getRecommendNo() {
        return RecommendNo;
    }
    public void setRecommendNo(String aRecommendNo) {
        RecommendNo = aRecommendNo;
    }
    public String getCautionerName() {
        return CautionerName;
    }
    public void setCautionerName(String aCautionerName) {
        CautionerName = aCautionerName;
    }
    public String getCautionerSex() {
        return CautionerSex;
    }
    public void setCautionerSex(String aCautionerSex) {
        CautionerSex = aCautionerSex;
    }
    public String getCautionerID() {
        return CautionerID;
    }
    public void setCautionerID(String aCautionerID) {
        CautionerID = aCautionerID;
    }
    public String getCautionerBirthday() {
        return CautionerBirthday;
    }
    public void setCautionerBirthday(String aCautionerBirthday) {
        CautionerBirthday = aCautionerBirthday;
    }
    public String getApprover() {
        return Approver;
    }
    public void setApprover(String aApprover) {
        Approver = aApprover;
    }
    public String getApproveDate() {
        return ApproveDate;
    }
    public void setApproveDate(String aApproveDate) {
        ApproveDate = aApproveDate;
    }
    public double getAssuMoney() {
        return AssuMoney;
    }
    public void setAssuMoney(double aAssuMoney) {
        AssuMoney = aAssuMoney;
    }
    public void setAssuMoney(String aAssuMoney) {
        if (aAssuMoney != null && !aAssuMoney.equals("")) {
            Double tDouble = new Double(aAssuMoney);
            double d = tDouble.doubleValue();
            AssuMoney = d;
        }
    }

    public String getRemark() {
        return Remark;
    }
    public void setRemark(String aRemark) {
        Remark = aRemark;
    }
    public String getAgentState() {
        return AgentState;
    }
    public void setAgentState(String aAgentState) {
        AgentState = aAgentState;
    }
    public String getQualiPassFlag() {
        return QualiPassFlag;
    }
    public void setQualiPassFlag(String aQualiPassFlag) {
        QualiPassFlag = aQualiPassFlag;
    }
    public String getSmokeFlag() {
        return SmokeFlag;
    }
    public void setSmokeFlag(String aSmokeFlag) {
        SmokeFlag = aSmokeFlag;
    }
    public String getRgtAddress() {
        return RgtAddress;
    }
    public void setRgtAddress(String aRgtAddress) {
        RgtAddress = aRgtAddress;
    }
    public String getBankCode() {
        return BankCode;
    }
    public void setBankCode(String aBankCode) {
        BankCode = aBankCode;
    }
    public String getBankAccNo() {
        return BankAccNo;
    }
    public void setBankAccNo(String aBankAccNo) {
        BankAccNo = aBankAccNo;
    }
    public String getOperator() {
        return Operator;
    }
    public void setOperator(String aOperator) {
        Operator = aOperator;
    }
    public String getMakeDate() {
        return MakeDate;
    }
    public void setMakeDate(String aMakeDate) {
        MakeDate = aMakeDate;
    }
    public String getMakeTime() {
        return MakeTime;
    }
    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }
    public String getModifyDate() {
        return ModifyDate;
    }
    public void setModifyDate(String aModifyDate) {
        ModifyDate = aModifyDate;
    }
    public String getModifyTime() {
        return ModifyTime;
    }
    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }
    public String getBranchType() {
        return BranchType;
    }
    public void setBranchType(String aBranchType) {
        BranchType = aBranchType;
    }
    public String getTrainPeriods() {
        return TrainPeriods;
    }
    public void setTrainPeriods(String aTrainPeriods) {
        TrainPeriods = aTrainPeriods;
    }
    public String getBranchCode() {
        return BranchCode;
    }
    public void setBranchCode(String aBranchCode) {
        BranchCode = aBranchCode;
    }
    public int getAge() {
        return Age;
    }
    public void setAge(int aAge) {
        Age = aAge;
    }
    public void setAge(String aAge) {
        if (aAge != null && !aAge.equals("")) {
            Integer tInteger = new Integer(aAge);
            int i = tInteger.intValue();
            Age = i;
        }
    }

    public String getChannelName() {
        return ChannelName;
    }
    public void setChannelName(String aChannelName) {
        ChannelName = aChannelName;
    }
    public String getReceiptNo() {
        return ReceiptNo;
    }
    public void setReceiptNo(String aReceiptNo) {
        ReceiptNo = aReceiptNo;
    }
    public String getIDNoType() {
        return IDNoType;
    }
    public void setIDNoType(String aIDNoType) {
        IDNoType = aIDNoType;
    }
    public String getBranchType2() {
        return BranchType2;
    }
    public void setBranchType2(String aBranchType2) {
        BranchType2 = aBranchType2;
    }
    public String getTrainPassFlag() {
        return TrainPassFlag;
    }
    public void setTrainPassFlag(String aTrainPassFlag) {
        TrainPassFlag = aTrainPassFlag;
    }
    public String getEmergentLink() {
        return EmergentLink;
    }
    public void setEmergentLink(String aEmergentLink) {
        EmergentLink = aEmergentLink;
    }
    public String getEmergentPhone() {
        return EmergentPhone;
    }
    public void setEmergentPhone(String aEmergentPhone) {
        EmergentPhone = aEmergentPhone;
    }
    public String getRetainStartDate() {
        return RetainStartDate;
    }
    public void setRetainStartDate(String aRetainStartDate) {
        RetainStartDate = aRetainStartDate;
    }
    public String getRetainEndDate() {
        return RetainEndDate;
    }
    public void setRetainEndDate(String aRetainEndDate) {
        RetainEndDate = aRetainEndDate;
    }
    public String getTogaeFlag() {
        return TogaeFlag;
    }
    public void setTogaeFlag(String aTogaeFlag) {
        TogaeFlag = aTogaeFlag;
    }
    public String getArchieveCode() {
        return ArchieveCode;
    }
    public void setArchieveCode(String aArchieveCode) {
        ArchieveCode = aArchieveCode;
    }
    public String getAffix() {
        return Affix;
    }
    public void setAffix(String aAffix) {
        Affix = aAffix;
    }
    public String getAffixName() {
        return AffixName;
    }
    public void setAffixName(String aAffixName) {
        AffixName = aAffixName;
    }
    public String getBankAddress() {
        return BankAddress;
    }
    public void setBankAddress(String aBankAddress) {
        BankAddress = aBankAddress;
    }
    public String getDXBranchtype2() {
        return DXBranchtype2;
    }
    public void setDXBranchtype2(String aDXBranchtype2) {
        DXBranchtype2 = aDXBranchtype2;
    }
    public String getDXSaleOrg() {
        return DXSaleOrg;
    }
    public void setDXSaleOrg(String aDXSaleOrg) {
        DXSaleOrg = aDXSaleOrg;
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("AgentCode") ) {
            return 0;
        }
        if( strFieldName.equals("AgentGroup") ) {
            return 1;
        }
        if( strFieldName.equals("ManageCom") ) {
            return 2;
        }
        if( strFieldName.equals("Password") ) {
            return 3;
        }
        if( strFieldName.equals("EntryNo") ) {
            return 4;
        }
        if( strFieldName.equals("Name") ) {
            return 5;
        }
        if( strFieldName.equals("Sex") ) {
            return 6;
        }
        if( strFieldName.equals("Birthday") ) {
            return 7;
        }
        if( strFieldName.equals("NativePlace") ) {
            return 8;
        }
        if( strFieldName.equals("Nationality") ) {
            return 9;
        }
        if( strFieldName.equals("Marriage") ) {
            return 10;
        }
        if( strFieldName.equals("CreditGrade") ) {
            return 11;
        }
        if( strFieldName.equals("HomeAddressCode") ) {
            return 12;
        }
        if( strFieldName.equals("HomeAddress") ) {
            return 13;
        }
        if( strFieldName.equals("PostalAddress") ) {
            return 14;
        }
        if( strFieldName.equals("ZipCode") ) {
            return 15;
        }
        if( strFieldName.equals("Phone") ) {
            return 16;
        }
        if( strFieldName.equals("BP") ) {
            return 17;
        }
        if( strFieldName.equals("Mobile") ) {
            return 18;
        }
        if( strFieldName.equals("EMail") ) {
            return 19;
        }
        if( strFieldName.equals("MarriageDate") ) {
            return 20;
        }
        if( strFieldName.equals("IDNo") ) {
            return 21;
        }
        if( strFieldName.equals("Source") ) {
            return 22;
        }
        if( strFieldName.equals("BloodType") ) {
            return 23;
        }
        if( strFieldName.equals("PolityVisage") ) {
            return 24;
        }
        if( strFieldName.equals("Degree") ) {
            return 25;
        }
        if( strFieldName.equals("GraduateSchool") ) {
            return 26;
        }
        if( strFieldName.equals("Speciality") ) {
            return 27;
        }
        if( strFieldName.equals("PostTitle") ) {
            return 28;
        }
        if( strFieldName.equals("ForeignLevel") ) {
            return 29;
        }
        if( strFieldName.equals("WorkAge") ) {
            return 30;
        }
        if( strFieldName.equals("OldCom") ) {
            return 31;
        }
        if( strFieldName.equals("OldOccupation") ) {
            return 32;
        }
        if( strFieldName.equals("HeadShip") ) {
            return 33;
        }
        if( strFieldName.equals("RecommendAgent") ) {
            return 34;
        }
        if( strFieldName.equals("Business") ) {
            return 35;
        }
        if( strFieldName.equals("SaleQuaf") ) {
            return 36;
        }
        if( strFieldName.equals("QuafNo") ) {
            return 37;
        }
        if( strFieldName.equals("QuafStartDate") ) {
            return 38;
        }
        if( strFieldName.equals("QuafEndDate") ) {
            return 39;
        }
        if( strFieldName.equals("DevNo1") ) {
            return 40;
        }
        if( strFieldName.equals("DevNo2") ) {
            return 41;
        }
        if( strFieldName.equals("RetainContNo") ) {
            return 42;
        }
        if( strFieldName.equals("AgentKind") ) {
            return 43;
        }
        if( strFieldName.equals("DevGrade") ) {
            return 44;
        }
        if( strFieldName.equals("InsideFlag") ) {
            return 45;
        }
        if( strFieldName.equals("FullTimeFlag") ) {
            return 46;
        }
        if( strFieldName.equals("NoWorkFlag") ) {
            return 47;
        }
        if( strFieldName.equals("TrainDate") ) {
            return 48;
        }
        if( strFieldName.equals("EmployDate") ) {
            return 49;
        }
        if( strFieldName.equals("InDueFormDate") ) {
            return 50;
        }
        if( strFieldName.equals("OutWorkDate") ) {
            return 51;
        }
        if( strFieldName.equals("RecommendNo") ) {
            return 52;
        }
        if( strFieldName.equals("CautionerName") ) {
            return 53;
        }
        if( strFieldName.equals("CautionerSex") ) {
            return 54;
        }
        if( strFieldName.equals("CautionerID") ) {
            return 55;
        }
        if( strFieldName.equals("CautionerBirthday") ) {
            return 56;
        }
        if( strFieldName.equals("Approver") ) {
            return 57;
        }
        if( strFieldName.equals("ApproveDate") ) {
            return 58;
        }
        if( strFieldName.equals("AssuMoney") ) {
            return 59;
        }
        if( strFieldName.equals("Remark") ) {
            return 60;
        }
        if( strFieldName.equals("AgentState") ) {
            return 61;
        }
        if( strFieldName.equals("QualiPassFlag") ) {
            return 62;
        }
        if( strFieldName.equals("SmokeFlag") ) {
            return 63;
        }
        if( strFieldName.equals("RgtAddress") ) {
            return 64;
        }
        if( strFieldName.equals("BankCode") ) {
            return 65;
        }
        if( strFieldName.equals("BankAccNo") ) {
            return 66;
        }
        if( strFieldName.equals("Operator") ) {
            return 67;
        }
        if( strFieldName.equals("MakeDate") ) {
            return 68;
        }
        if( strFieldName.equals("MakeTime") ) {
            return 69;
        }
        if( strFieldName.equals("ModifyDate") ) {
            return 70;
        }
        if( strFieldName.equals("ModifyTime") ) {
            return 71;
        }
        if( strFieldName.equals("BranchType") ) {
            return 72;
        }
        if( strFieldName.equals("TrainPeriods") ) {
            return 73;
        }
        if( strFieldName.equals("BranchCode") ) {
            return 74;
        }
        if( strFieldName.equals("Age") ) {
            return 75;
        }
        if( strFieldName.equals("ChannelName") ) {
            return 76;
        }
        if( strFieldName.equals("ReceiptNo") ) {
            return 77;
        }
        if( strFieldName.equals("IDNoType") ) {
            return 78;
        }
        if( strFieldName.equals("BranchType2") ) {
            return 79;
        }
        if( strFieldName.equals("TrainPassFlag") ) {
            return 80;
        }
        if( strFieldName.equals("EmergentLink") ) {
            return 81;
        }
        if( strFieldName.equals("EmergentPhone") ) {
            return 82;
        }
        if( strFieldName.equals("RetainStartDate") ) {
            return 83;
        }
        if( strFieldName.equals("RetainEndDate") ) {
            return 84;
        }
        if( strFieldName.equals("TogaeFlag") ) {
            return 85;
        }
        if( strFieldName.equals("ArchieveCode") ) {
            return 86;
        }
        if( strFieldName.equals("Affix") ) {
            return 87;
        }
        if( strFieldName.equals("AffixName") ) {
            return 88;
        }
        if( strFieldName.equals("BankAddress") ) {
            return 89;
        }
        if( strFieldName.equals("DXBranchtype2") ) {
            return 90;
        }
        if( strFieldName.equals("DXSaleOrg") ) {
            return 91;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "AgentCode";
                break;
            case 1:
                strFieldName = "AgentGroup";
                break;
            case 2:
                strFieldName = "ManageCom";
                break;
            case 3:
                strFieldName = "Password";
                break;
            case 4:
                strFieldName = "EntryNo";
                break;
            case 5:
                strFieldName = "Name";
                break;
            case 6:
                strFieldName = "Sex";
                break;
            case 7:
                strFieldName = "Birthday";
                break;
            case 8:
                strFieldName = "NativePlace";
                break;
            case 9:
                strFieldName = "Nationality";
                break;
            case 10:
                strFieldName = "Marriage";
                break;
            case 11:
                strFieldName = "CreditGrade";
                break;
            case 12:
                strFieldName = "HomeAddressCode";
                break;
            case 13:
                strFieldName = "HomeAddress";
                break;
            case 14:
                strFieldName = "PostalAddress";
                break;
            case 15:
                strFieldName = "ZipCode";
                break;
            case 16:
                strFieldName = "Phone";
                break;
            case 17:
                strFieldName = "BP";
                break;
            case 18:
                strFieldName = "Mobile";
                break;
            case 19:
                strFieldName = "EMail";
                break;
            case 20:
                strFieldName = "MarriageDate";
                break;
            case 21:
                strFieldName = "IDNo";
                break;
            case 22:
                strFieldName = "Source";
                break;
            case 23:
                strFieldName = "BloodType";
                break;
            case 24:
                strFieldName = "PolityVisage";
                break;
            case 25:
                strFieldName = "Degree";
                break;
            case 26:
                strFieldName = "GraduateSchool";
                break;
            case 27:
                strFieldName = "Speciality";
                break;
            case 28:
                strFieldName = "PostTitle";
                break;
            case 29:
                strFieldName = "ForeignLevel";
                break;
            case 30:
                strFieldName = "WorkAge";
                break;
            case 31:
                strFieldName = "OldCom";
                break;
            case 32:
                strFieldName = "OldOccupation";
                break;
            case 33:
                strFieldName = "HeadShip";
                break;
            case 34:
                strFieldName = "RecommendAgent";
                break;
            case 35:
                strFieldName = "Business";
                break;
            case 36:
                strFieldName = "SaleQuaf";
                break;
            case 37:
                strFieldName = "QuafNo";
                break;
            case 38:
                strFieldName = "QuafStartDate";
                break;
            case 39:
                strFieldName = "QuafEndDate";
                break;
            case 40:
                strFieldName = "DevNo1";
                break;
            case 41:
                strFieldName = "DevNo2";
                break;
            case 42:
                strFieldName = "RetainContNo";
                break;
            case 43:
                strFieldName = "AgentKind";
                break;
            case 44:
                strFieldName = "DevGrade";
                break;
            case 45:
                strFieldName = "InsideFlag";
                break;
            case 46:
                strFieldName = "FullTimeFlag";
                break;
            case 47:
                strFieldName = "NoWorkFlag";
                break;
            case 48:
                strFieldName = "TrainDate";
                break;
            case 49:
                strFieldName = "EmployDate";
                break;
            case 50:
                strFieldName = "InDueFormDate";
                break;
            case 51:
                strFieldName = "OutWorkDate";
                break;
            case 52:
                strFieldName = "RecommendNo";
                break;
            case 53:
                strFieldName = "CautionerName";
                break;
            case 54:
                strFieldName = "CautionerSex";
                break;
            case 55:
                strFieldName = "CautionerID";
                break;
            case 56:
                strFieldName = "CautionerBirthday";
                break;
            case 57:
                strFieldName = "Approver";
                break;
            case 58:
                strFieldName = "ApproveDate";
                break;
            case 59:
                strFieldName = "AssuMoney";
                break;
            case 60:
                strFieldName = "Remark";
                break;
            case 61:
                strFieldName = "AgentState";
                break;
            case 62:
                strFieldName = "QualiPassFlag";
                break;
            case 63:
                strFieldName = "SmokeFlag";
                break;
            case 64:
                strFieldName = "RgtAddress";
                break;
            case 65:
                strFieldName = "BankCode";
                break;
            case 66:
                strFieldName = "BankAccNo";
                break;
            case 67:
                strFieldName = "Operator";
                break;
            case 68:
                strFieldName = "MakeDate";
                break;
            case 69:
                strFieldName = "MakeTime";
                break;
            case 70:
                strFieldName = "ModifyDate";
                break;
            case 71:
                strFieldName = "ModifyTime";
                break;
            case 72:
                strFieldName = "BranchType";
                break;
            case 73:
                strFieldName = "TrainPeriods";
                break;
            case 74:
                strFieldName = "BranchCode";
                break;
            case 75:
                strFieldName = "Age";
                break;
            case 76:
                strFieldName = "ChannelName";
                break;
            case 77:
                strFieldName = "ReceiptNo";
                break;
            case 78:
                strFieldName = "IDNoType";
                break;
            case 79:
                strFieldName = "BranchType2";
                break;
            case 80:
                strFieldName = "TrainPassFlag";
                break;
            case 81:
                strFieldName = "EmergentLink";
                break;
            case 82:
                strFieldName = "EmergentPhone";
                break;
            case 83:
                strFieldName = "RetainStartDate";
                break;
            case 84:
                strFieldName = "RetainEndDate";
                break;
            case 85:
                strFieldName = "TogaeFlag";
                break;
            case 86:
                strFieldName = "ArchieveCode";
                break;
            case 87:
                strFieldName = "Affix";
                break;
            case 88:
                strFieldName = "AffixName";
                break;
            case 89:
                strFieldName = "BankAddress";
                break;
            case 90:
                strFieldName = "DXBranchtype2";
                break;
            case 91:
                strFieldName = "DXSaleOrg";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "AGENTCODE":
                return Schema.TYPE_STRING;
            case "AGENTGROUP":
                return Schema.TYPE_STRING;
            case "MANAGECOM":
                return Schema.TYPE_STRING;
            case "PASSWORD":
                return Schema.TYPE_STRING;
            case "ENTRYNO":
                return Schema.TYPE_STRING;
            case "NAME":
                return Schema.TYPE_STRING;
            case "SEX":
                return Schema.TYPE_STRING;
            case "BIRTHDAY":
                return Schema.TYPE_STRING;
            case "NATIVEPLACE":
                return Schema.TYPE_STRING;
            case "NATIONALITY":
                return Schema.TYPE_STRING;
            case "MARRIAGE":
                return Schema.TYPE_STRING;
            case "CREDITGRADE":
                return Schema.TYPE_STRING;
            case "HOMEADDRESSCODE":
                return Schema.TYPE_STRING;
            case "HOMEADDRESS":
                return Schema.TYPE_STRING;
            case "POSTALADDRESS":
                return Schema.TYPE_STRING;
            case "ZIPCODE":
                return Schema.TYPE_STRING;
            case "PHONE":
                return Schema.TYPE_STRING;
            case "BP":
                return Schema.TYPE_STRING;
            case "MOBILE":
                return Schema.TYPE_STRING;
            case "EMAIL":
                return Schema.TYPE_STRING;
            case "MARRIAGEDATE":
                return Schema.TYPE_STRING;
            case "IDNO":
                return Schema.TYPE_STRING;
            case "SOURCE":
                return Schema.TYPE_STRING;
            case "BLOODTYPE":
                return Schema.TYPE_STRING;
            case "POLITYVISAGE":
                return Schema.TYPE_STRING;
            case "DEGREE":
                return Schema.TYPE_STRING;
            case "GRADUATESCHOOL":
                return Schema.TYPE_STRING;
            case "SPECIALITY":
                return Schema.TYPE_STRING;
            case "POSTTITLE":
                return Schema.TYPE_STRING;
            case "FOREIGNLEVEL":
                return Schema.TYPE_STRING;
            case "WORKAGE":
                return Schema.TYPE_INT;
            case "OLDCOM":
                return Schema.TYPE_STRING;
            case "OLDOCCUPATION":
                return Schema.TYPE_STRING;
            case "HEADSHIP":
                return Schema.TYPE_STRING;
            case "RECOMMENDAGENT":
                return Schema.TYPE_STRING;
            case "BUSINESS":
                return Schema.TYPE_STRING;
            case "SALEQUAF":
                return Schema.TYPE_STRING;
            case "QUAFNO":
                return Schema.TYPE_STRING;
            case "QUAFSTARTDATE":
                return Schema.TYPE_STRING;
            case "QUAFENDDATE":
                return Schema.TYPE_STRING;
            case "DEVNO1":
                return Schema.TYPE_STRING;
            case "DEVNO2":
                return Schema.TYPE_STRING;
            case "RETAINCONTNO":
                return Schema.TYPE_STRING;
            case "AGENTKIND":
                return Schema.TYPE_STRING;
            case "DEVGRADE":
                return Schema.TYPE_STRING;
            case "INSIDEFLAG":
                return Schema.TYPE_STRING;
            case "FULLTIMEFLAG":
                return Schema.TYPE_STRING;
            case "NOWORKFLAG":
                return Schema.TYPE_STRING;
            case "TRAINDATE":
                return Schema.TYPE_STRING;
            case "EMPLOYDATE":
                return Schema.TYPE_STRING;
            case "INDUEFORMDATE":
                return Schema.TYPE_STRING;
            case "OUTWORKDATE":
                return Schema.TYPE_STRING;
            case "RECOMMENDNO":
                return Schema.TYPE_STRING;
            case "CAUTIONERNAME":
                return Schema.TYPE_STRING;
            case "CAUTIONERSEX":
                return Schema.TYPE_STRING;
            case "CAUTIONERID":
                return Schema.TYPE_STRING;
            case "CAUTIONERBIRTHDAY":
                return Schema.TYPE_STRING;
            case "APPROVER":
                return Schema.TYPE_STRING;
            case "APPROVEDATE":
                return Schema.TYPE_STRING;
            case "ASSUMONEY":
                return Schema.TYPE_DOUBLE;
            case "REMARK":
                return Schema.TYPE_STRING;
            case "AGENTSTATE":
                return Schema.TYPE_STRING;
            case "QUALIPASSFLAG":
                return Schema.TYPE_STRING;
            case "SMOKEFLAG":
                return Schema.TYPE_STRING;
            case "RGTADDRESS":
                return Schema.TYPE_STRING;
            case "BANKCODE":
                return Schema.TYPE_STRING;
            case "BANKACCNO":
                return Schema.TYPE_STRING;
            case "OPERATOR":
                return Schema.TYPE_STRING;
            case "MAKEDATE":
                return Schema.TYPE_STRING;
            case "MAKETIME":
                return Schema.TYPE_STRING;
            case "MODIFYDATE":
                return Schema.TYPE_STRING;
            case "MODIFYTIME":
                return Schema.TYPE_STRING;
            case "BRANCHTYPE":
                return Schema.TYPE_STRING;
            case "TRAINPERIODS":
                return Schema.TYPE_STRING;
            case "BRANCHCODE":
                return Schema.TYPE_STRING;
            case "AGE":
                return Schema.TYPE_INT;
            case "CHANNELNAME":
                return Schema.TYPE_STRING;
            case "RECEIPTNO":
                return Schema.TYPE_STRING;
            case "IDNOTYPE":
                return Schema.TYPE_STRING;
            case "BRANCHTYPE2":
                return Schema.TYPE_STRING;
            case "TRAINPASSFLAG":
                return Schema.TYPE_STRING;
            case "EMERGENTLINK":
                return Schema.TYPE_STRING;
            case "EMERGENTPHONE":
                return Schema.TYPE_STRING;
            case "RETAINSTARTDATE":
                return Schema.TYPE_STRING;
            case "RETAINENDDATE":
                return Schema.TYPE_STRING;
            case "TOGAEFLAG":
                return Schema.TYPE_STRING;
            case "ARCHIEVECODE":
                return Schema.TYPE_STRING;
            case "AFFIX":
                return Schema.TYPE_STRING;
            case "AFFIXNAME":
                return Schema.TYPE_STRING;
            case "BANKADDRESS":
                return Schema.TYPE_STRING;
            case "DXBRANCHTYPE2":
                return Schema.TYPE_STRING;
            case "DXSALEORG":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_STRING;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_STRING;
            case 6:
                return Schema.TYPE_STRING;
            case 7:
                return Schema.TYPE_STRING;
            case 8:
                return Schema.TYPE_STRING;
            case 9:
                return Schema.TYPE_STRING;
            case 10:
                return Schema.TYPE_STRING;
            case 11:
                return Schema.TYPE_STRING;
            case 12:
                return Schema.TYPE_STRING;
            case 13:
                return Schema.TYPE_STRING;
            case 14:
                return Schema.TYPE_STRING;
            case 15:
                return Schema.TYPE_STRING;
            case 16:
                return Schema.TYPE_STRING;
            case 17:
                return Schema.TYPE_STRING;
            case 18:
                return Schema.TYPE_STRING;
            case 19:
                return Schema.TYPE_STRING;
            case 20:
                return Schema.TYPE_STRING;
            case 21:
                return Schema.TYPE_STRING;
            case 22:
                return Schema.TYPE_STRING;
            case 23:
                return Schema.TYPE_STRING;
            case 24:
                return Schema.TYPE_STRING;
            case 25:
                return Schema.TYPE_STRING;
            case 26:
                return Schema.TYPE_STRING;
            case 27:
                return Schema.TYPE_STRING;
            case 28:
                return Schema.TYPE_STRING;
            case 29:
                return Schema.TYPE_STRING;
            case 30:
                return Schema.TYPE_INT;
            case 31:
                return Schema.TYPE_STRING;
            case 32:
                return Schema.TYPE_STRING;
            case 33:
                return Schema.TYPE_STRING;
            case 34:
                return Schema.TYPE_STRING;
            case 35:
                return Schema.TYPE_STRING;
            case 36:
                return Schema.TYPE_STRING;
            case 37:
                return Schema.TYPE_STRING;
            case 38:
                return Schema.TYPE_STRING;
            case 39:
                return Schema.TYPE_STRING;
            case 40:
                return Schema.TYPE_STRING;
            case 41:
                return Schema.TYPE_STRING;
            case 42:
                return Schema.TYPE_STRING;
            case 43:
                return Schema.TYPE_STRING;
            case 44:
                return Schema.TYPE_STRING;
            case 45:
                return Schema.TYPE_STRING;
            case 46:
                return Schema.TYPE_STRING;
            case 47:
                return Schema.TYPE_STRING;
            case 48:
                return Schema.TYPE_STRING;
            case 49:
                return Schema.TYPE_STRING;
            case 50:
                return Schema.TYPE_STRING;
            case 51:
                return Schema.TYPE_STRING;
            case 52:
                return Schema.TYPE_STRING;
            case 53:
                return Schema.TYPE_STRING;
            case 54:
                return Schema.TYPE_STRING;
            case 55:
                return Schema.TYPE_STRING;
            case 56:
                return Schema.TYPE_STRING;
            case 57:
                return Schema.TYPE_STRING;
            case 58:
                return Schema.TYPE_STRING;
            case 59:
                return Schema.TYPE_DOUBLE;
            case 60:
                return Schema.TYPE_STRING;
            case 61:
                return Schema.TYPE_STRING;
            case 62:
                return Schema.TYPE_STRING;
            case 63:
                return Schema.TYPE_STRING;
            case 64:
                return Schema.TYPE_STRING;
            case 65:
                return Schema.TYPE_STRING;
            case 66:
                return Schema.TYPE_STRING;
            case 67:
                return Schema.TYPE_STRING;
            case 68:
                return Schema.TYPE_STRING;
            case 69:
                return Schema.TYPE_STRING;
            case 70:
                return Schema.TYPE_STRING;
            case 71:
                return Schema.TYPE_STRING;
            case 72:
                return Schema.TYPE_STRING;
            case 73:
                return Schema.TYPE_STRING;
            case 74:
                return Schema.TYPE_STRING;
            case 75:
                return Schema.TYPE_INT;
            case 76:
                return Schema.TYPE_STRING;
            case 77:
                return Schema.TYPE_STRING;
            case 78:
                return Schema.TYPE_STRING;
            case 79:
                return Schema.TYPE_STRING;
            case 80:
                return Schema.TYPE_STRING;
            case 81:
                return Schema.TYPE_STRING;
            case 82:
                return Schema.TYPE_STRING;
            case 83:
                return Schema.TYPE_STRING;
            case 84:
                return Schema.TYPE_STRING;
            case 85:
                return Schema.TYPE_STRING;
            case 86:
                return Schema.TYPE_STRING;
            case 87:
                return Schema.TYPE_STRING;
            case 88:
                return Schema.TYPE_STRING;
            case 89:
                return Schema.TYPE_STRING;
            case 90:
                return Schema.TYPE_STRING;
            case 91:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("AgentCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentCode));
        }
        if (FCode.equalsIgnoreCase("AgentGroup")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentGroup));
        }
        if (FCode.equalsIgnoreCase("ManageCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
        }
        if (FCode.equalsIgnoreCase("Password")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Password));
        }
        if (FCode.equalsIgnoreCase("EntryNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(EntryNo));
        }
        if (FCode.equalsIgnoreCase("Name")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Name));
        }
        if (FCode.equalsIgnoreCase("Sex")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Sex));
        }
        if (FCode.equalsIgnoreCase("Birthday")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Birthday));
        }
        if (FCode.equalsIgnoreCase("NativePlace")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(NativePlace));
        }
        if (FCode.equalsIgnoreCase("Nationality")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Nationality));
        }
        if (FCode.equalsIgnoreCase("Marriage")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Marriage));
        }
        if (FCode.equalsIgnoreCase("CreditGrade")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CreditGrade));
        }
        if (FCode.equalsIgnoreCase("HomeAddressCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(HomeAddressCode));
        }
        if (FCode.equalsIgnoreCase("HomeAddress")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(HomeAddress));
        }
        if (FCode.equalsIgnoreCase("PostalAddress")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PostalAddress));
        }
        if (FCode.equalsIgnoreCase("ZipCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ZipCode));
        }
        if (FCode.equalsIgnoreCase("Phone")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Phone));
        }
        if (FCode.equalsIgnoreCase("BP")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BP));
        }
        if (FCode.equalsIgnoreCase("Mobile")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Mobile));
        }
        if (FCode.equalsIgnoreCase("EMail")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(EMail));
        }
        if (FCode.equalsIgnoreCase("MarriageDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MarriageDate));
        }
        if (FCode.equalsIgnoreCase("IDNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(IDNo));
        }
        if (FCode.equalsIgnoreCase("Source")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Source));
        }
        if (FCode.equalsIgnoreCase("BloodType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BloodType));
        }
        if (FCode.equalsIgnoreCase("PolityVisage")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PolityVisage));
        }
        if (FCode.equalsIgnoreCase("Degree")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Degree));
        }
        if (FCode.equalsIgnoreCase("GraduateSchool")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GraduateSchool));
        }
        if (FCode.equalsIgnoreCase("Speciality")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Speciality));
        }
        if (FCode.equalsIgnoreCase("PostTitle")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PostTitle));
        }
        if (FCode.equalsIgnoreCase("ForeignLevel")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ForeignLevel));
        }
        if (FCode.equalsIgnoreCase("WorkAge")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(WorkAge));
        }
        if (FCode.equalsIgnoreCase("OldCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OldCom));
        }
        if (FCode.equalsIgnoreCase("OldOccupation")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OldOccupation));
        }
        if (FCode.equalsIgnoreCase("HeadShip")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(HeadShip));
        }
        if (FCode.equalsIgnoreCase("RecommendAgent")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RecommendAgent));
        }
        if (FCode.equalsIgnoreCase("Business")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Business));
        }
        if (FCode.equalsIgnoreCase("SaleQuaf")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SaleQuaf));
        }
        if (FCode.equalsIgnoreCase("QuafNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(QuafNo));
        }
        if (FCode.equalsIgnoreCase("QuafStartDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(QuafStartDate));
        }
        if (FCode.equalsIgnoreCase("QuafEndDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(QuafEndDate));
        }
        if (FCode.equalsIgnoreCase("DevNo1")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DevNo1));
        }
        if (FCode.equalsIgnoreCase("DevNo2")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DevNo2));
        }
        if (FCode.equalsIgnoreCase("RetainContNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RetainContNo));
        }
        if (FCode.equalsIgnoreCase("AgentKind")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentKind));
        }
        if (FCode.equalsIgnoreCase("DevGrade")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DevGrade));
        }
        if (FCode.equalsIgnoreCase("InsideFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InsideFlag));
        }
        if (FCode.equalsIgnoreCase("FullTimeFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FullTimeFlag));
        }
        if (FCode.equalsIgnoreCase("NoWorkFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(NoWorkFlag));
        }
        if (FCode.equalsIgnoreCase("TrainDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(TrainDate));
        }
        if (FCode.equalsIgnoreCase("EmployDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(EmployDate));
        }
        if (FCode.equalsIgnoreCase("InDueFormDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InDueFormDate));
        }
        if (FCode.equalsIgnoreCase("OutWorkDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OutWorkDate));
        }
        if (FCode.equalsIgnoreCase("RecommendNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RecommendNo));
        }
        if (FCode.equalsIgnoreCase("CautionerName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CautionerName));
        }
        if (FCode.equalsIgnoreCase("CautionerSex")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CautionerSex));
        }
        if (FCode.equalsIgnoreCase("CautionerID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CautionerID));
        }
        if (FCode.equalsIgnoreCase("CautionerBirthday")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CautionerBirthday));
        }
        if (FCode.equalsIgnoreCase("Approver")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Approver));
        }
        if (FCode.equalsIgnoreCase("ApproveDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ApproveDate));
        }
        if (FCode.equalsIgnoreCase("AssuMoney")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AssuMoney));
        }
        if (FCode.equalsIgnoreCase("Remark")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Remark));
        }
        if (FCode.equalsIgnoreCase("AgentState")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentState));
        }
        if (FCode.equalsIgnoreCase("QualiPassFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(QualiPassFlag));
        }
        if (FCode.equalsIgnoreCase("SmokeFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SmokeFlag));
        }
        if (FCode.equalsIgnoreCase("RgtAddress")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RgtAddress));
        }
        if (FCode.equalsIgnoreCase("BankCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BankCode));
        }
        if (FCode.equalsIgnoreCase("BankAccNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BankAccNo));
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeDate));
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyDate));
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (FCode.equalsIgnoreCase("BranchType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BranchType));
        }
        if (FCode.equalsIgnoreCase("TrainPeriods")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(TrainPeriods));
        }
        if (FCode.equalsIgnoreCase("BranchCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BranchCode));
        }
        if (FCode.equalsIgnoreCase("Age")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Age));
        }
        if (FCode.equalsIgnoreCase("ChannelName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ChannelName));
        }
        if (FCode.equalsIgnoreCase("ReceiptNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ReceiptNo));
        }
        if (FCode.equalsIgnoreCase("IDNoType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(IDNoType));
        }
        if (FCode.equalsIgnoreCase("BranchType2")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BranchType2));
        }
        if (FCode.equalsIgnoreCase("TrainPassFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(TrainPassFlag));
        }
        if (FCode.equalsIgnoreCase("EmergentLink")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(EmergentLink));
        }
        if (FCode.equalsIgnoreCase("EmergentPhone")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(EmergentPhone));
        }
        if (FCode.equalsIgnoreCase("RetainStartDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RetainStartDate));
        }
        if (FCode.equalsIgnoreCase("RetainEndDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RetainEndDate));
        }
        if (FCode.equalsIgnoreCase("TogaeFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(TogaeFlag));
        }
        if (FCode.equalsIgnoreCase("ArchieveCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ArchieveCode));
        }
        if (FCode.equalsIgnoreCase("Affix")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Affix));
        }
        if (FCode.equalsIgnoreCase("AffixName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AffixName));
        }
        if (FCode.equalsIgnoreCase("BankAddress")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BankAddress));
        }
        if (FCode.equalsIgnoreCase("DXBranchtype2")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DXBranchtype2));
        }
        if (FCode.equalsIgnoreCase("DXSaleOrg")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DXSaleOrg));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = String.valueOf(AgentCode);
                break;
            case 1:
                strFieldValue = String.valueOf(AgentGroup);
                break;
            case 2:
                strFieldValue = String.valueOf(ManageCom);
                break;
            case 3:
                strFieldValue = String.valueOf(Password);
                break;
            case 4:
                strFieldValue = String.valueOf(EntryNo);
                break;
            case 5:
                strFieldValue = String.valueOf(Name);
                break;
            case 6:
                strFieldValue = String.valueOf(Sex);
                break;
            case 7:
                strFieldValue = String.valueOf(Birthday);
                break;
            case 8:
                strFieldValue = String.valueOf(NativePlace);
                break;
            case 9:
                strFieldValue = String.valueOf(Nationality);
                break;
            case 10:
                strFieldValue = String.valueOf(Marriage);
                break;
            case 11:
                strFieldValue = String.valueOf(CreditGrade);
                break;
            case 12:
                strFieldValue = String.valueOf(HomeAddressCode);
                break;
            case 13:
                strFieldValue = String.valueOf(HomeAddress);
                break;
            case 14:
                strFieldValue = String.valueOf(PostalAddress);
                break;
            case 15:
                strFieldValue = String.valueOf(ZipCode);
                break;
            case 16:
                strFieldValue = String.valueOf(Phone);
                break;
            case 17:
                strFieldValue = String.valueOf(BP);
                break;
            case 18:
                strFieldValue = String.valueOf(Mobile);
                break;
            case 19:
                strFieldValue = String.valueOf(EMail);
                break;
            case 20:
                strFieldValue = String.valueOf(MarriageDate);
                break;
            case 21:
                strFieldValue = String.valueOf(IDNo);
                break;
            case 22:
                strFieldValue = String.valueOf(Source);
                break;
            case 23:
                strFieldValue = String.valueOf(BloodType);
                break;
            case 24:
                strFieldValue = String.valueOf(PolityVisage);
                break;
            case 25:
                strFieldValue = String.valueOf(Degree);
                break;
            case 26:
                strFieldValue = String.valueOf(GraduateSchool);
                break;
            case 27:
                strFieldValue = String.valueOf(Speciality);
                break;
            case 28:
                strFieldValue = String.valueOf(PostTitle);
                break;
            case 29:
                strFieldValue = String.valueOf(ForeignLevel);
                break;
            case 30:
                strFieldValue = String.valueOf(WorkAge);
                break;
            case 31:
                strFieldValue = String.valueOf(OldCom);
                break;
            case 32:
                strFieldValue = String.valueOf(OldOccupation);
                break;
            case 33:
                strFieldValue = String.valueOf(HeadShip);
                break;
            case 34:
                strFieldValue = String.valueOf(RecommendAgent);
                break;
            case 35:
                strFieldValue = String.valueOf(Business);
                break;
            case 36:
                strFieldValue = String.valueOf(SaleQuaf);
                break;
            case 37:
                strFieldValue = String.valueOf(QuafNo);
                break;
            case 38:
                strFieldValue = String.valueOf(QuafStartDate);
                break;
            case 39:
                strFieldValue = String.valueOf(QuafEndDate);
                break;
            case 40:
                strFieldValue = String.valueOf(DevNo1);
                break;
            case 41:
                strFieldValue = String.valueOf(DevNo2);
                break;
            case 42:
                strFieldValue = String.valueOf(RetainContNo);
                break;
            case 43:
                strFieldValue = String.valueOf(AgentKind);
                break;
            case 44:
                strFieldValue = String.valueOf(DevGrade);
                break;
            case 45:
                strFieldValue = String.valueOf(InsideFlag);
                break;
            case 46:
                strFieldValue = String.valueOf(FullTimeFlag);
                break;
            case 47:
                strFieldValue = String.valueOf(NoWorkFlag);
                break;
            case 48:
                strFieldValue = String.valueOf(TrainDate);
                break;
            case 49:
                strFieldValue = String.valueOf(EmployDate);
                break;
            case 50:
                strFieldValue = String.valueOf(InDueFormDate);
                break;
            case 51:
                strFieldValue = String.valueOf(OutWorkDate);
                break;
            case 52:
                strFieldValue = String.valueOf(RecommendNo);
                break;
            case 53:
                strFieldValue = String.valueOf(CautionerName);
                break;
            case 54:
                strFieldValue = String.valueOf(CautionerSex);
                break;
            case 55:
                strFieldValue = String.valueOf(CautionerID);
                break;
            case 56:
                strFieldValue = String.valueOf(CautionerBirthday);
                break;
            case 57:
                strFieldValue = String.valueOf(Approver);
                break;
            case 58:
                strFieldValue = String.valueOf(ApproveDate);
                break;
            case 59:
                strFieldValue = String.valueOf(AssuMoney);
                break;
            case 60:
                strFieldValue = String.valueOf(Remark);
                break;
            case 61:
                strFieldValue = String.valueOf(AgentState);
                break;
            case 62:
                strFieldValue = String.valueOf(QualiPassFlag);
                break;
            case 63:
                strFieldValue = String.valueOf(SmokeFlag);
                break;
            case 64:
                strFieldValue = String.valueOf(RgtAddress);
                break;
            case 65:
                strFieldValue = String.valueOf(BankCode);
                break;
            case 66:
                strFieldValue = String.valueOf(BankAccNo);
                break;
            case 67:
                strFieldValue = String.valueOf(Operator);
                break;
            case 68:
                strFieldValue = String.valueOf(MakeDate);
                break;
            case 69:
                strFieldValue = String.valueOf(MakeTime);
                break;
            case 70:
                strFieldValue = String.valueOf(ModifyDate);
                break;
            case 71:
                strFieldValue = String.valueOf(ModifyTime);
                break;
            case 72:
                strFieldValue = String.valueOf(BranchType);
                break;
            case 73:
                strFieldValue = String.valueOf(TrainPeriods);
                break;
            case 74:
                strFieldValue = String.valueOf(BranchCode);
                break;
            case 75:
                strFieldValue = String.valueOf(Age);
                break;
            case 76:
                strFieldValue = String.valueOf(ChannelName);
                break;
            case 77:
                strFieldValue = String.valueOf(ReceiptNo);
                break;
            case 78:
                strFieldValue = String.valueOf(IDNoType);
                break;
            case 79:
                strFieldValue = String.valueOf(BranchType2);
                break;
            case 80:
                strFieldValue = String.valueOf(TrainPassFlag);
                break;
            case 81:
                strFieldValue = String.valueOf(EmergentLink);
                break;
            case 82:
                strFieldValue = String.valueOf(EmergentPhone);
                break;
            case 83:
                strFieldValue = String.valueOf(RetainStartDate);
                break;
            case 84:
                strFieldValue = String.valueOf(RetainEndDate);
                break;
            case 85:
                strFieldValue = String.valueOf(TogaeFlag);
                break;
            case 86:
                strFieldValue = String.valueOf(ArchieveCode);
                break;
            case 87:
                strFieldValue = String.valueOf(Affix);
                break;
            case 88:
                strFieldValue = String.valueOf(AffixName);
                break;
            case 89:
                strFieldValue = String.valueOf(BankAddress);
                break;
            case 90:
                strFieldValue = String.valueOf(DXBranchtype2);
                break;
            case 91:
                strFieldValue = String.valueOf(DXSaleOrg);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("AgentCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                AgentCode = FValue.trim();
            }
            else
                AgentCode = null;
        }
        if (FCode.equalsIgnoreCase("AgentGroup")) {
            if( FValue != null && !FValue.equals(""))
            {
                AgentGroup = FValue.trim();
            }
            else
                AgentGroup = null;
        }
        if (FCode.equalsIgnoreCase("ManageCom")) {
            if( FValue != null && !FValue.equals(""))
            {
                ManageCom = FValue.trim();
            }
            else
                ManageCom = null;
        }
        if (FCode.equalsIgnoreCase("Password")) {
            if( FValue != null && !FValue.equals(""))
            {
                Password = FValue.trim();
            }
            else
                Password = null;
        }
        if (FCode.equalsIgnoreCase("EntryNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                EntryNo = FValue.trim();
            }
            else
                EntryNo = null;
        }
        if (FCode.equalsIgnoreCase("Name")) {
            if( FValue != null && !FValue.equals(""))
            {
                Name = FValue.trim();
            }
            else
                Name = null;
        }
        if (FCode.equalsIgnoreCase("Sex")) {
            if( FValue != null && !FValue.equals(""))
            {
                Sex = FValue.trim();
            }
            else
                Sex = null;
        }
        if (FCode.equalsIgnoreCase("Birthday")) {
            if( FValue != null && !FValue.equals(""))
            {
                Birthday = FValue.trim();
            }
            else
                Birthday = null;
        }
        if (FCode.equalsIgnoreCase("NativePlace")) {
            if( FValue != null && !FValue.equals(""))
            {
                NativePlace = FValue.trim();
            }
            else
                NativePlace = null;
        }
        if (FCode.equalsIgnoreCase("Nationality")) {
            if( FValue != null && !FValue.equals(""))
            {
                Nationality = FValue.trim();
            }
            else
                Nationality = null;
        }
        if (FCode.equalsIgnoreCase("Marriage")) {
            if( FValue != null && !FValue.equals(""))
            {
                Marriage = FValue.trim();
            }
            else
                Marriage = null;
        }
        if (FCode.equalsIgnoreCase("CreditGrade")) {
            if( FValue != null && !FValue.equals(""))
            {
                CreditGrade = FValue.trim();
            }
            else
                CreditGrade = null;
        }
        if (FCode.equalsIgnoreCase("HomeAddressCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                HomeAddressCode = FValue.trim();
            }
            else
                HomeAddressCode = null;
        }
        if (FCode.equalsIgnoreCase("HomeAddress")) {
            if( FValue != null && !FValue.equals(""))
            {
                HomeAddress = FValue.trim();
            }
            else
                HomeAddress = null;
        }
        if (FCode.equalsIgnoreCase("PostalAddress")) {
            if( FValue != null && !FValue.equals(""))
            {
                PostalAddress = FValue.trim();
            }
            else
                PostalAddress = null;
        }
        if (FCode.equalsIgnoreCase("ZipCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                ZipCode = FValue.trim();
            }
            else
                ZipCode = null;
        }
        if (FCode.equalsIgnoreCase("Phone")) {
            if( FValue != null && !FValue.equals(""))
            {
                Phone = FValue.trim();
            }
            else
                Phone = null;
        }
        if (FCode.equalsIgnoreCase("BP")) {
            if( FValue != null && !FValue.equals(""))
            {
                BP = FValue.trim();
            }
            else
                BP = null;
        }
        if (FCode.equalsIgnoreCase("Mobile")) {
            if( FValue != null && !FValue.equals(""))
            {
                Mobile = FValue.trim();
            }
            else
                Mobile = null;
        }
        if (FCode.equalsIgnoreCase("EMail")) {
            if( FValue != null && !FValue.equals(""))
            {
                EMail = FValue.trim();
            }
            else
                EMail = null;
        }
        if (FCode.equalsIgnoreCase("MarriageDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                MarriageDate = FValue.trim();
            }
            else
                MarriageDate = null;
        }
        if (FCode.equalsIgnoreCase("IDNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                IDNo = FValue.trim();
            }
            else
                IDNo = null;
        }
        if (FCode.equalsIgnoreCase("Source")) {
            if( FValue != null && !FValue.equals(""))
            {
                Source = FValue.trim();
            }
            else
                Source = null;
        }
        if (FCode.equalsIgnoreCase("BloodType")) {
            if( FValue != null && !FValue.equals(""))
            {
                BloodType = FValue.trim();
            }
            else
                BloodType = null;
        }
        if (FCode.equalsIgnoreCase("PolityVisage")) {
            if( FValue != null && !FValue.equals(""))
            {
                PolityVisage = FValue.trim();
            }
            else
                PolityVisage = null;
        }
        if (FCode.equalsIgnoreCase("Degree")) {
            if( FValue != null && !FValue.equals(""))
            {
                Degree = FValue.trim();
            }
            else
                Degree = null;
        }
        if (FCode.equalsIgnoreCase("GraduateSchool")) {
            if( FValue != null && !FValue.equals(""))
            {
                GraduateSchool = FValue.trim();
            }
            else
                GraduateSchool = null;
        }
        if (FCode.equalsIgnoreCase("Speciality")) {
            if( FValue != null && !FValue.equals(""))
            {
                Speciality = FValue.trim();
            }
            else
                Speciality = null;
        }
        if (FCode.equalsIgnoreCase("PostTitle")) {
            if( FValue != null && !FValue.equals(""))
            {
                PostTitle = FValue.trim();
            }
            else
                PostTitle = null;
        }
        if (FCode.equalsIgnoreCase("ForeignLevel")) {
            if( FValue != null && !FValue.equals(""))
            {
                ForeignLevel = FValue.trim();
            }
            else
                ForeignLevel = null;
        }
        if (FCode.equalsIgnoreCase("WorkAge")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                WorkAge = i;
            }
        }
        if (FCode.equalsIgnoreCase("OldCom")) {
            if( FValue != null && !FValue.equals(""))
            {
                OldCom = FValue.trim();
            }
            else
                OldCom = null;
        }
        if (FCode.equalsIgnoreCase("OldOccupation")) {
            if( FValue != null && !FValue.equals(""))
            {
                OldOccupation = FValue.trim();
            }
            else
                OldOccupation = null;
        }
        if (FCode.equalsIgnoreCase("HeadShip")) {
            if( FValue != null && !FValue.equals(""))
            {
                HeadShip = FValue.trim();
            }
            else
                HeadShip = null;
        }
        if (FCode.equalsIgnoreCase("RecommendAgent")) {
            if( FValue != null && !FValue.equals(""))
            {
                RecommendAgent = FValue.trim();
            }
            else
                RecommendAgent = null;
        }
        if (FCode.equalsIgnoreCase("Business")) {
            if( FValue != null && !FValue.equals(""))
            {
                Business = FValue.trim();
            }
            else
                Business = null;
        }
        if (FCode.equalsIgnoreCase("SaleQuaf")) {
            if( FValue != null && !FValue.equals(""))
            {
                SaleQuaf = FValue.trim();
            }
            else
                SaleQuaf = null;
        }
        if (FCode.equalsIgnoreCase("QuafNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                QuafNo = FValue.trim();
            }
            else
                QuafNo = null;
        }
        if (FCode.equalsIgnoreCase("QuafStartDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                QuafStartDate = FValue.trim();
            }
            else
                QuafStartDate = null;
        }
        if (FCode.equalsIgnoreCase("QuafEndDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                QuafEndDate = FValue.trim();
            }
            else
                QuafEndDate = null;
        }
        if (FCode.equalsIgnoreCase("DevNo1")) {
            if( FValue != null && !FValue.equals(""))
            {
                DevNo1 = FValue.trim();
            }
            else
                DevNo1 = null;
        }
        if (FCode.equalsIgnoreCase("DevNo2")) {
            if( FValue != null && !FValue.equals(""))
            {
                DevNo2 = FValue.trim();
            }
            else
                DevNo2 = null;
        }
        if (FCode.equalsIgnoreCase("RetainContNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                RetainContNo = FValue.trim();
            }
            else
                RetainContNo = null;
        }
        if (FCode.equalsIgnoreCase("AgentKind")) {
            if( FValue != null && !FValue.equals(""))
            {
                AgentKind = FValue.trim();
            }
            else
                AgentKind = null;
        }
        if (FCode.equalsIgnoreCase("DevGrade")) {
            if( FValue != null && !FValue.equals(""))
            {
                DevGrade = FValue.trim();
            }
            else
                DevGrade = null;
        }
        if (FCode.equalsIgnoreCase("InsideFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                InsideFlag = FValue.trim();
            }
            else
                InsideFlag = null;
        }
        if (FCode.equalsIgnoreCase("FullTimeFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                FullTimeFlag = FValue.trim();
            }
            else
                FullTimeFlag = null;
        }
        if (FCode.equalsIgnoreCase("NoWorkFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                NoWorkFlag = FValue.trim();
            }
            else
                NoWorkFlag = null;
        }
        if (FCode.equalsIgnoreCase("TrainDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                TrainDate = FValue.trim();
            }
            else
                TrainDate = null;
        }
        if (FCode.equalsIgnoreCase("EmployDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                EmployDate = FValue.trim();
            }
            else
                EmployDate = null;
        }
        if (FCode.equalsIgnoreCase("InDueFormDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                InDueFormDate = FValue.trim();
            }
            else
                InDueFormDate = null;
        }
        if (FCode.equalsIgnoreCase("OutWorkDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                OutWorkDate = FValue.trim();
            }
            else
                OutWorkDate = null;
        }
        if (FCode.equalsIgnoreCase("RecommendNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                RecommendNo = FValue.trim();
            }
            else
                RecommendNo = null;
        }
        if (FCode.equalsIgnoreCase("CautionerName")) {
            if( FValue != null && !FValue.equals(""))
            {
                CautionerName = FValue.trim();
            }
            else
                CautionerName = null;
        }
        if (FCode.equalsIgnoreCase("CautionerSex")) {
            if( FValue != null && !FValue.equals(""))
            {
                CautionerSex = FValue.trim();
            }
            else
                CautionerSex = null;
        }
        if (FCode.equalsIgnoreCase("CautionerID")) {
            if( FValue != null && !FValue.equals(""))
            {
                CautionerID = FValue.trim();
            }
            else
                CautionerID = null;
        }
        if (FCode.equalsIgnoreCase("CautionerBirthday")) {
            if( FValue != null && !FValue.equals(""))
            {
                CautionerBirthday = FValue.trim();
            }
            else
                CautionerBirthday = null;
        }
        if (FCode.equalsIgnoreCase("Approver")) {
            if( FValue != null && !FValue.equals(""))
            {
                Approver = FValue.trim();
            }
            else
                Approver = null;
        }
        if (FCode.equalsIgnoreCase("ApproveDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                ApproveDate = FValue.trim();
            }
            else
                ApproveDate = null;
        }
        if (FCode.equalsIgnoreCase("AssuMoney")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                AssuMoney = d;
            }
        }
        if (FCode.equalsIgnoreCase("Remark")) {
            if( FValue != null && !FValue.equals(""))
            {
                Remark = FValue.trim();
            }
            else
                Remark = null;
        }
        if (FCode.equalsIgnoreCase("AgentState")) {
            if( FValue != null && !FValue.equals(""))
            {
                AgentState = FValue.trim();
            }
            else
                AgentState = null;
        }
        if (FCode.equalsIgnoreCase("QualiPassFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                QualiPassFlag = FValue.trim();
            }
            else
                QualiPassFlag = null;
        }
        if (FCode.equalsIgnoreCase("SmokeFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                SmokeFlag = FValue.trim();
            }
            else
                SmokeFlag = null;
        }
        if (FCode.equalsIgnoreCase("RgtAddress")) {
            if( FValue != null && !FValue.equals(""))
            {
                RgtAddress = FValue.trim();
            }
            else
                RgtAddress = null;
        }
        if (FCode.equalsIgnoreCase("BankCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                BankCode = FValue.trim();
            }
            else
                BankCode = null;
        }
        if (FCode.equalsIgnoreCase("BankAccNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                BankAccNo = FValue.trim();
            }
            else
                BankAccNo = null;
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            if( FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
                Operator = null;
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeDate = FValue.trim();
            }
            else
                MakeDate = null;
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
                MakeTime = null;
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyDate = FValue.trim();
            }
            else
                ModifyDate = null;
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
                ModifyTime = null;
        }
        if (FCode.equalsIgnoreCase("BranchType")) {
            if( FValue != null && !FValue.equals(""))
            {
                BranchType = FValue.trim();
            }
            else
                BranchType = null;
        }
        if (FCode.equalsIgnoreCase("TrainPeriods")) {
            if( FValue != null && !FValue.equals(""))
            {
                TrainPeriods = FValue.trim();
            }
            else
                TrainPeriods = null;
        }
        if (FCode.equalsIgnoreCase("BranchCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                BranchCode = FValue.trim();
            }
            else
                BranchCode = null;
        }
        if (FCode.equalsIgnoreCase("Age")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                Age = i;
            }
        }
        if (FCode.equalsIgnoreCase("ChannelName")) {
            if( FValue != null && !FValue.equals(""))
            {
                ChannelName = FValue.trim();
            }
            else
                ChannelName = null;
        }
        if (FCode.equalsIgnoreCase("ReceiptNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                ReceiptNo = FValue.trim();
            }
            else
                ReceiptNo = null;
        }
        if (FCode.equalsIgnoreCase("IDNoType")) {
            if( FValue != null && !FValue.equals(""))
            {
                IDNoType = FValue.trim();
            }
            else
                IDNoType = null;
        }
        if (FCode.equalsIgnoreCase("BranchType2")) {
            if( FValue != null && !FValue.equals(""))
            {
                BranchType2 = FValue.trim();
            }
            else
                BranchType2 = null;
        }
        if (FCode.equalsIgnoreCase("TrainPassFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                TrainPassFlag = FValue.trim();
            }
            else
                TrainPassFlag = null;
        }
        if (FCode.equalsIgnoreCase("EmergentLink")) {
            if( FValue != null && !FValue.equals(""))
            {
                EmergentLink = FValue.trim();
            }
            else
                EmergentLink = null;
        }
        if (FCode.equalsIgnoreCase("EmergentPhone")) {
            if( FValue != null && !FValue.equals(""))
            {
                EmergentPhone = FValue.trim();
            }
            else
                EmergentPhone = null;
        }
        if (FCode.equalsIgnoreCase("RetainStartDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                RetainStartDate = FValue.trim();
            }
            else
                RetainStartDate = null;
        }
        if (FCode.equalsIgnoreCase("RetainEndDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                RetainEndDate = FValue.trim();
            }
            else
                RetainEndDate = null;
        }
        if (FCode.equalsIgnoreCase("TogaeFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                TogaeFlag = FValue.trim();
            }
            else
                TogaeFlag = null;
        }
        if (FCode.equalsIgnoreCase("ArchieveCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                ArchieveCode = FValue.trim();
            }
            else
                ArchieveCode = null;
        }
        if (FCode.equalsIgnoreCase("Affix")) {
            if( FValue != null && !FValue.equals(""))
            {
                Affix = FValue.trim();
            }
            else
                Affix = null;
        }
        if (FCode.equalsIgnoreCase("AffixName")) {
            if( FValue != null && !FValue.equals(""))
            {
                AffixName = FValue.trim();
            }
            else
                AffixName = null;
        }
        if (FCode.equalsIgnoreCase("BankAddress")) {
            if( FValue != null && !FValue.equals(""))
            {
                BankAddress = FValue.trim();
            }
            else
                BankAddress = null;
        }
        if (FCode.equalsIgnoreCase("DXBranchtype2")) {
            if( FValue != null && !FValue.equals(""))
            {
                DXBranchtype2 = FValue.trim();
            }
            else
                DXBranchtype2 = null;
        }
        if (FCode.equalsIgnoreCase("DXSaleOrg")) {
            if( FValue != null && !FValue.equals(""))
            {
                DXSaleOrg = FValue.trim();
            }
            else
                DXSaleOrg = null;
        }
        return true;
    }


    public String toString() {
    return "LAAgentPojo [" +
            "AgentCode="+AgentCode +
            ", AgentGroup="+AgentGroup +
            ", ManageCom="+ManageCom +
            ", Password="+Password +
            ", EntryNo="+EntryNo +
            ", Name="+Name +
            ", Sex="+Sex +
            ", Birthday="+Birthday +
            ", NativePlace="+NativePlace +
            ", Nationality="+Nationality +
            ", Marriage="+Marriage +
            ", CreditGrade="+CreditGrade +
            ", HomeAddressCode="+HomeAddressCode +
            ", HomeAddress="+HomeAddress +
            ", PostalAddress="+PostalAddress +
            ", ZipCode="+ZipCode +
            ", Phone="+Phone +
            ", BP="+BP +
            ", Mobile="+Mobile +
            ", EMail="+EMail +
            ", MarriageDate="+MarriageDate +
            ", IDNo="+IDNo +
            ", Source="+Source +
            ", BloodType="+BloodType +
            ", PolityVisage="+PolityVisage +
            ", Degree="+Degree +
            ", GraduateSchool="+GraduateSchool +
            ", Speciality="+Speciality +
            ", PostTitle="+PostTitle +
            ", ForeignLevel="+ForeignLevel +
            ", WorkAge="+WorkAge +
            ", OldCom="+OldCom +
            ", OldOccupation="+OldOccupation +
            ", HeadShip="+HeadShip +
            ", RecommendAgent="+RecommendAgent +
            ", Business="+Business +
            ", SaleQuaf="+SaleQuaf +
            ", QuafNo="+QuafNo +
            ", QuafStartDate="+QuafStartDate +
            ", QuafEndDate="+QuafEndDate +
            ", DevNo1="+DevNo1 +
            ", DevNo2="+DevNo2 +
            ", RetainContNo="+RetainContNo +
            ", AgentKind="+AgentKind +
            ", DevGrade="+DevGrade +
            ", InsideFlag="+InsideFlag +
            ", FullTimeFlag="+FullTimeFlag +
            ", NoWorkFlag="+NoWorkFlag +
            ", TrainDate="+TrainDate +
            ", EmployDate="+EmployDate +
            ", InDueFormDate="+InDueFormDate +
            ", OutWorkDate="+OutWorkDate +
            ", RecommendNo="+RecommendNo +
            ", CautionerName="+CautionerName +
            ", CautionerSex="+CautionerSex +
            ", CautionerID="+CautionerID +
            ", CautionerBirthday="+CautionerBirthday +
            ", Approver="+Approver +
            ", ApproveDate="+ApproveDate +
            ", AssuMoney="+AssuMoney +
            ", Remark="+Remark +
            ", AgentState="+AgentState +
            ", QualiPassFlag="+QualiPassFlag +
            ", SmokeFlag="+SmokeFlag +
            ", RgtAddress="+RgtAddress +
            ", BankCode="+BankCode +
            ", BankAccNo="+BankAccNo +
            ", Operator="+Operator +
            ", MakeDate="+MakeDate +
            ", MakeTime="+MakeTime +
            ", ModifyDate="+ModifyDate +
            ", ModifyTime="+ModifyTime +
            ", BranchType="+BranchType +
            ", TrainPeriods="+TrainPeriods +
            ", BranchCode="+BranchCode +
            ", Age="+Age +
            ", ChannelName="+ChannelName +
            ", ReceiptNo="+ReceiptNo +
            ", IDNoType="+IDNoType +
            ", BranchType2="+BranchType2 +
            ", TrainPassFlag="+TrainPassFlag +
            ", EmergentLink="+EmergentLink +
            ", EmergentPhone="+EmergentPhone +
            ", RetainStartDate="+RetainStartDate +
            ", RetainEndDate="+RetainEndDate +
            ", TogaeFlag="+TogaeFlag +
            ", ArchieveCode="+ArchieveCode +
            ", Affix="+Affix +
            ", AffixName="+AffixName +
            ", BankAddress="+BankAddress +
            ", DXBranchtype2="+DXBranchtype2 +
            ", DXSaleOrg="+DXSaleOrg +"]";
    }
}
