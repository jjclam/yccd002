/**
 * Copyright (c) 2017 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import com.sinosoft.lis.db.LYVerifyAppBDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.*;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

/**
 * <p>ClassName: LYVerifyAppBSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2017</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2017-10-21
 */
public class LYVerifyAppBSchema implements Schema, Cloneable {
    // @Field
    /** Id */
    private long VerifyAppID;
    /** Fk_lccont */
    private long ContID;
    /** Shardingid */
    private String ShardingID;
    /** 批单号 */
    private String EdorNo;
    /** 初审号码 */
    private String ContNo;
    /** 受理日期 */
    private Date ApplyDate;
    /** 受理完毕日期 */
    private Date ApplyEndDate;
    /** 销售渠道 */
    private String SaleChnl;
    /** 初审状态 */
    private String State;
    /** 投保单号 */
    private String PrtNo;
    /** 银代银行代码 */
    private String AgentBankCode;
    /** 银行网点(或中介编码） */
    private String AgentCom;
    /** 银代柜员（或中介代理人） */
    private String BankAgent;
    /** 代理人姓名（中介） */
    private String AgentName;
    /** 代理人电话(中介) */
    private String AgentPhone;
    /** 代理人编码(银代专官员或中介经理) */
    private String AgentCode;
    /** 代理人组别 */
    private String AgentGroup;
    /** 管理机构 */
    private String ManageCom;
    /** 总保费 */
    private double Prem;
    /** 备用属性字段1 */
    private String StandbyFlag1;
    /** 备用属性字段2 */
    private String StandbyFlag2;
    /** 备用属性字段3 */
    private String StandbyFlag3;
    /** 操作员 */
    private String Operator;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 最后一次修改时间 */
    private String ModifyTime;
    /** 最后一次修改日期 */
    private Date ModifyDate;
    /** 投保人名称 */
    private String AppntName;
    /** 投保人性别 */
    private String AppntSex;
    /** 投保人出生日期 */
    private Date AppntBirthday;
    /** 与被保人关系 */
    private String RelatToInsu;
    /** 投保人年龄 */
    private int AppAge;
    /** 投保人职业类别 */
    private String AppntOccupationType;
    /** 投保人职业代码 */
    private String AppntOccupationCode;
    /** 被保人名称 */
    private String Name;
    /** 被保人性别 */
    private String Sex;
    /** 被保人出生日期 */
    private Date Birthday;
    /** 被保人年龄 */
    private int Age;
    /** 职业类别 */
    private String OccupationType;
    /** 职业代码 */
    private String OccupationCode;
    /** 外包录入标识 */
    private String BpoFlag;
    /** 风险测评结果 */
    private String RiskEvaluationResult;
    /** 投保人年收入 */
    private double Income;
    /** 美国纳税人识别号 */
    private String TINNO;
    /** 是否为美国纳税义务的个人 */
    private String TINFlag;

    public static final int FIELDNUM = 45;    // 数据库表的字段个数

    private static String[] PK;                // 主键

    private FDate fDate = new FDate();        // 处理日期

    public CErrors mErrors;            // 错误信息

    // @Constructor
    public LYVerifyAppBSchema() {
        mErrors = new CErrors();

        String[] pk = new String[1];
        pk[0] = "VerifyAppID";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException {
        LYVerifyAppBSchema cloned = (LYVerifyAppBSchema)super.clone();
        cloned.fDate = (FDate) fDate.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK() {
        return PK;
    }

    public long getVerifyAppID() {
        return VerifyAppID;
    }
    public void setVerifyAppID(long aVerifyAppID) {
        VerifyAppID = aVerifyAppID;
    }
    public void setVerifyAppID(String aVerifyAppID) {
        if (aVerifyAppID != null && !aVerifyAppID.equals("")) {
            VerifyAppID = new Long(aVerifyAppID).longValue();
        }
    }

    public long getContID() {
        return ContID;
    }
    public void setContID(long aContID) {
        ContID = aContID;
    }
    public void setContID(String aContID) {
        if (aContID != null && !aContID.equals("")) {
            ContID = new Long(aContID).longValue();
        }
    }

    public String getShardingID() {
        return ShardingID;
    }
    public void setShardingID(String aShardingID) {
        ShardingID = aShardingID;
    }
    public String getEdorNo() {
        return EdorNo;
    }
    public void setEdorNo(String aEdorNo) {
        EdorNo = aEdorNo;
    }
    public String getContNo() {
        return ContNo;
    }
    public void setContNo(String aContNo) {
        ContNo = aContNo;
    }
    public String getApplyDate() {
        if(ApplyDate != null) {
            return fDate.getString(ApplyDate);
        } else {
            return null;
        }
    }
    public void setApplyDate(Date aApplyDate) {
        ApplyDate = aApplyDate;
    }
    public void setApplyDate(String aApplyDate) {
        if (aApplyDate != null && !aApplyDate.equals("")) {
            ApplyDate = fDate.getDate(aApplyDate);
        } else
            ApplyDate = null;
    }

    public String getApplyEndDate() {
        if(ApplyEndDate != null) {
            return fDate.getString(ApplyEndDate);
        } else {
            return null;
        }
    }
    public void setApplyEndDate(Date aApplyEndDate) {
        ApplyEndDate = aApplyEndDate;
    }
    public void setApplyEndDate(String aApplyEndDate) {
        if (aApplyEndDate != null && !aApplyEndDate.equals("")) {
            ApplyEndDate = fDate.getDate(aApplyEndDate);
        } else
            ApplyEndDate = null;
    }

    public String getSaleChnl() {
        return SaleChnl;
    }
    public void setSaleChnl(String aSaleChnl) {
        SaleChnl = aSaleChnl;
    }
    public String getState() {
        return State;
    }
    public void setState(String aState) {
        State = aState;
    }
    public String getPrtNo() {
        return PrtNo;
    }
    public void setPrtNo(String aPrtNo) {
        PrtNo = aPrtNo;
    }
    public String getAgentBankCode() {
        return AgentBankCode;
    }
    public void setAgentBankCode(String aAgentBankCode) {
        AgentBankCode = aAgentBankCode;
    }
    public String getAgentCom() {
        return AgentCom;
    }
    public void setAgentCom(String aAgentCom) {
        AgentCom = aAgentCom;
    }
    public String getBankAgent() {
        return BankAgent;
    }
    public void setBankAgent(String aBankAgent) {
        BankAgent = aBankAgent;
    }
    public String getAgentName() {
        return AgentName;
    }
    public void setAgentName(String aAgentName) {
        AgentName = aAgentName;
    }
    public String getAgentPhone() {
        return AgentPhone;
    }
    public void setAgentPhone(String aAgentPhone) {
        AgentPhone = aAgentPhone;
    }
    public String getAgentCode() {
        return AgentCode;
    }
    public void setAgentCode(String aAgentCode) {
        AgentCode = aAgentCode;
    }
    public String getAgentGroup() {
        return AgentGroup;
    }
    public void setAgentGroup(String aAgentGroup) {
        AgentGroup = aAgentGroup;
    }
    public String getManageCom() {
        return ManageCom;
    }
    public void setManageCom(String aManageCom) {
        ManageCom = aManageCom;
    }
    public double getPrem() {
        return Prem;
    }
    public void setPrem(double aPrem) {
        Prem = aPrem;
    }
    public void setPrem(String aPrem) {
        if (aPrem != null && !aPrem.equals("")) {
            Double tDouble = new Double(aPrem);
            double d = tDouble.doubleValue();
            Prem = d;
        }
    }

    public String getStandbyFlag1() {
        return StandbyFlag1;
    }
    public void setStandbyFlag1(String aStandbyFlag1) {
        StandbyFlag1 = aStandbyFlag1;
    }
    public String getStandbyFlag2() {
        return StandbyFlag2;
    }
    public void setStandbyFlag2(String aStandbyFlag2) {
        StandbyFlag2 = aStandbyFlag2;
    }
    public String getStandbyFlag3() {
        return StandbyFlag3;
    }
    public void setStandbyFlag3(String aStandbyFlag3) {
        StandbyFlag3 = aStandbyFlag3;
    }
    public String getOperator() {
        return Operator;
    }
    public void setOperator(String aOperator) {
        Operator = aOperator;
    }
    public String getMakeDate() {
        if(MakeDate != null) {
            return fDate.getString(MakeDate);
        } else {
            return null;
        }
    }
    public void setMakeDate(Date aMakeDate) {
        MakeDate = aMakeDate;
    }
    public void setMakeDate(String aMakeDate) {
        if (aMakeDate != null && !aMakeDate.equals("")) {
            MakeDate = fDate.getDate(aMakeDate);
        } else
            MakeDate = null;
    }

    public String getMakeTime() {
        return MakeTime;
    }
    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }
    public String getModifyTime() {
        return ModifyTime;
    }
    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }
    public String getModifyDate() {
        if(ModifyDate != null) {
            return fDate.getString(ModifyDate);
        } else {
            return null;
        }
    }
    public void setModifyDate(Date aModifyDate) {
        ModifyDate = aModifyDate;
    }
    public void setModifyDate(String aModifyDate) {
        if (aModifyDate != null && !aModifyDate.equals("")) {
            ModifyDate = fDate.getDate(aModifyDate);
        } else
            ModifyDate = null;
    }

    public String getAppntName() {
        return AppntName;
    }
    public void setAppntName(String aAppntName) {
        AppntName = aAppntName;
    }
    public String getAppntSex() {
        return AppntSex;
    }
    public void setAppntSex(String aAppntSex) {
        AppntSex = aAppntSex;
    }
    public String getAppntBirthday() {
        if(AppntBirthday != null) {
            return fDate.getString(AppntBirthday);
        } else {
            return null;
        }
    }
    public void setAppntBirthday(Date aAppntBirthday) {
        AppntBirthday = aAppntBirthday;
    }
    public void setAppntBirthday(String aAppntBirthday) {
        if (aAppntBirthday != null && !aAppntBirthday.equals("")) {
            AppntBirthday = fDate.getDate(aAppntBirthday);
        } else
            AppntBirthday = null;
    }

    public String getRelatToInsu() {
        return RelatToInsu;
    }
    public void setRelatToInsu(String aRelatToInsu) {
        RelatToInsu = aRelatToInsu;
    }
    public int getAppAge() {
        return AppAge;
    }
    public void setAppAge(int aAppAge) {
        AppAge = aAppAge;
    }
    public void setAppAge(String aAppAge) {
        if (aAppAge != null && !aAppAge.equals("")) {
            Integer tInteger = new Integer(aAppAge);
            int i = tInteger.intValue();
            AppAge = i;
        }
    }

    public String getAppntOccupationType() {
        return AppntOccupationType;
    }
    public void setAppntOccupationType(String aAppntOccupationType) {
        AppntOccupationType = aAppntOccupationType;
    }
    public String getAppntOccupationCode() {
        return AppntOccupationCode;
    }
    public void setAppntOccupationCode(String aAppntOccupationCode) {
        AppntOccupationCode = aAppntOccupationCode;
    }
    public String getName() {
        return Name;
    }
    public void setName(String aName) {
        Name = aName;
    }
    public String getSex() {
        return Sex;
    }
    public void setSex(String aSex) {
        Sex = aSex;
    }
    public String getBirthday() {
        if(Birthday != null) {
            return fDate.getString(Birthday);
        } else {
            return null;
        }
    }
    public void setBirthday(Date aBirthday) {
        Birthday = aBirthday;
    }
    public void setBirthday(String aBirthday) {
        if (aBirthday != null && !aBirthday.equals("")) {
            Birthday = fDate.getDate(aBirthday);
        } else
            Birthday = null;
    }

    public int getAge() {
        return Age;
    }
    public void setAge(int aAge) {
        Age = aAge;
    }
    public void setAge(String aAge) {
        if (aAge != null && !aAge.equals("")) {
            Integer tInteger = new Integer(aAge);
            int i = tInteger.intValue();
            Age = i;
        }
    }

    public String getOccupationType() {
        return OccupationType;
    }
    public void setOccupationType(String aOccupationType) {
        OccupationType = aOccupationType;
    }
    public String getOccupationCode() {
        return OccupationCode;
    }
    public void setOccupationCode(String aOccupationCode) {
        OccupationCode = aOccupationCode;
    }
    public String getBpoFlag() {
        return BpoFlag;
    }
    public void setBpoFlag(String aBpoFlag) {
        BpoFlag = aBpoFlag;
    }
    public String getRiskEvaluationResult() {
        return RiskEvaluationResult;
    }
    public void setRiskEvaluationResult(String aRiskEvaluationResult) {
        RiskEvaluationResult = aRiskEvaluationResult;
    }
    public double getIncome() {
        return Income;
    }
    public void setIncome(double aIncome) {
        Income = aIncome;
    }
    public void setIncome(String aIncome) {
        if (aIncome != null && !aIncome.equals("")) {
            Double tDouble = new Double(aIncome);
            double d = tDouble.doubleValue();
            Income = d;
        }
    }

    public String getTINNO() {
        return TINNO;
    }
    public void setTINNO(String aTINNO) {
        TINNO = aTINNO;
    }
    public String getTINFlag() {
        return TINFlag;
    }
    public void setTINFlag(String aTINFlag) {
        TINFlag = aTINFlag;
    }

    /**
    * 使用另外一个 LYVerifyAppBSchema 对象给 Schema 赋值
    * @param: aLYVerifyAppBSchema LYVerifyAppBSchema
    **/
    public void setSchema(LYVerifyAppBSchema aLYVerifyAppBSchema) {
        this.VerifyAppID = aLYVerifyAppBSchema.getVerifyAppID();
        this.ContID = aLYVerifyAppBSchema.getContID();
        this.ShardingID = aLYVerifyAppBSchema.getShardingID();
        this.EdorNo = aLYVerifyAppBSchema.getEdorNo();
        this.ContNo = aLYVerifyAppBSchema.getContNo();
        this.ApplyDate = fDate.getDate( aLYVerifyAppBSchema.getApplyDate());
        this.ApplyEndDate = fDate.getDate( aLYVerifyAppBSchema.getApplyEndDate());
        this.SaleChnl = aLYVerifyAppBSchema.getSaleChnl();
        this.State = aLYVerifyAppBSchema.getState();
        this.PrtNo = aLYVerifyAppBSchema.getPrtNo();
        this.AgentBankCode = aLYVerifyAppBSchema.getAgentBankCode();
        this.AgentCom = aLYVerifyAppBSchema.getAgentCom();
        this.BankAgent = aLYVerifyAppBSchema.getBankAgent();
        this.AgentName = aLYVerifyAppBSchema.getAgentName();
        this.AgentPhone = aLYVerifyAppBSchema.getAgentPhone();
        this.AgentCode = aLYVerifyAppBSchema.getAgentCode();
        this.AgentGroup = aLYVerifyAppBSchema.getAgentGroup();
        this.ManageCom = aLYVerifyAppBSchema.getManageCom();
        this.Prem = aLYVerifyAppBSchema.getPrem();
        this.StandbyFlag1 = aLYVerifyAppBSchema.getStandbyFlag1();
        this.StandbyFlag2 = aLYVerifyAppBSchema.getStandbyFlag2();
        this.StandbyFlag3 = aLYVerifyAppBSchema.getStandbyFlag3();
        this.Operator = aLYVerifyAppBSchema.getOperator();
        this.MakeDate = fDate.getDate( aLYVerifyAppBSchema.getMakeDate());
        this.MakeTime = aLYVerifyAppBSchema.getMakeTime();
        this.ModifyTime = aLYVerifyAppBSchema.getModifyTime();
        this.ModifyDate = fDate.getDate( aLYVerifyAppBSchema.getModifyDate());
        this.AppntName = aLYVerifyAppBSchema.getAppntName();
        this.AppntSex = aLYVerifyAppBSchema.getAppntSex();
        this.AppntBirthday = fDate.getDate( aLYVerifyAppBSchema.getAppntBirthday());
        this.RelatToInsu = aLYVerifyAppBSchema.getRelatToInsu();
        this.AppAge = aLYVerifyAppBSchema.getAppAge();
        this.AppntOccupationType = aLYVerifyAppBSchema.getAppntOccupationType();
        this.AppntOccupationCode = aLYVerifyAppBSchema.getAppntOccupationCode();
        this.Name = aLYVerifyAppBSchema.getName();
        this.Sex = aLYVerifyAppBSchema.getSex();
        this.Birthday = fDate.getDate( aLYVerifyAppBSchema.getBirthday());
        this.Age = aLYVerifyAppBSchema.getAge();
        this.OccupationType = aLYVerifyAppBSchema.getOccupationType();
        this.OccupationCode = aLYVerifyAppBSchema.getOccupationCode();
        this.BpoFlag = aLYVerifyAppBSchema.getBpoFlag();
        this.RiskEvaluationResult = aLYVerifyAppBSchema.getRiskEvaluationResult();
        this.Income = aLYVerifyAppBSchema.getIncome();
        this.TINNO = aLYVerifyAppBSchema.getTINNO();
        this.TINFlag = aLYVerifyAppBSchema.getTINFlag();
    }

    /**
    * 使用 ResultSet 中的第 i 行给 Schema 赋值
    * @param: rs ResultSet
    * @param: i int
    * @return: boolean
    **/
    public boolean setSchema(ResultSet rs,int i) {
        try {
            //rs.absolute(i);        // 非滚动游标
            this.VerifyAppID = rs.getLong("VerifyAppID");
            this.ContID = rs.getLong("ContID");
            if( rs.getString("ShardingID") == null )
                this.ShardingID = null;
            else
                this.ShardingID = rs.getString("ShardingID").trim();

            if( rs.getString("EdorNo") == null )
                this.EdorNo = null;
            else
                this.EdorNo = rs.getString("EdorNo").trim();

            if( rs.getString("ContNo") == null )
                this.ContNo = null;
            else
                this.ContNo = rs.getString("ContNo").trim();

            this.ApplyDate = rs.getDate("ApplyDate");
            this.ApplyEndDate = rs.getDate("ApplyEndDate");
            if( rs.getString("SaleChnl") == null )
                this.SaleChnl = null;
            else
                this.SaleChnl = rs.getString("SaleChnl").trim();

            if( rs.getString("State") == null )
                this.State = null;
            else
                this.State = rs.getString("State").trim();

            if( rs.getString("PrtNo") == null )
                this.PrtNo = null;
            else
                this.PrtNo = rs.getString("PrtNo").trim();

            if( rs.getString("AgentBankCode") == null )
                this.AgentBankCode = null;
            else
                this.AgentBankCode = rs.getString("AgentBankCode").trim();

            if( rs.getString("AgentCom") == null )
                this.AgentCom = null;
            else
                this.AgentCom = rs.getString("AgentCom").trim();

            if( rs.getString("BankAgent") == null )
                this.BankAgent = null;
            else
                this.BankAgent = rs.getString("BankAgent").trim();

            if( rs.getString("AgentName") == null )
                this.AgentName = null;
            else
                this.AgentName = rs.getString("AgentName").trim();

            if( rs.getString("AgentPhone") == null )
                this.AgentPhone = null;
            else
                this.AgentPhone = rs.getString("AgentPhone").trim();

            if( rs.getString("AgentCode") == null )
                this.AgentCode = null;
            else
                this.AgentCode = rs.getString("AgentCode").trim();

            if( rs.getString("AgentGroup") == null )
                this.AgentGroup = null;
            else
                this.AgentGroup = rs.getString("AgentGroup").trim();

            if( rs.getString("ManageCom") == null )
                this.ManageCom = null;
            else
                this.ManageCom = rs.getString("ManageCom").trim();

            this.Prem = rs.getDouble("Prem");
            if( rs.getString("StandbyFlag1") == null )
                this.StandbyFlag1 = null;
            else
                this.StandbyFlag1 = rs.getString("StandbyFlag1").trim();

            if( rs.getString("StandbyFlag2") == null )
                this.StandbyFlag2 = null;
            else
                this.StandbyFlag2 = rs.getString("StandbyFlag2").trim();

            if( rs.getString("StandbyFlag3") == null )
                this.StandbyFlag3 = null;
            else
                this.StandbyFlag3 = rs.getString("StandbyFlag3").trim();

            if( rs.getString("Operator") == null )
                this.Operator = null;
            else
                this.Operator = rs.getString("Operator").trim();

            this.MakeDate = rs.getDate("MakeDate");
            if( rs.getString("MakeTime") == null )
                this.MakeTime = null;
            else
                this.MakeTime = rs.getString("MakeTime").trim();

            if( rs.getString("ModifyTime") == null )
                this.ModifyTime = null;
            else
                this.ModifyTime = rs.getString("ModifyTime").trim();

            this.ModifyDate = rs.getDate("ModifyDate");
            if( rs.getString("AppntName") == null )
                this.AppntName = null;
            else
                this.AppntName = rs.getString("AppntName").trim();

            if( rs.getString("AppntSex") == null )
                this.AppntSex = null;
            else
                this.AppntSex = rs.getString("AppntSex").trim();

            this.AppntBirthday = rs.getDate("AppntBirthday");
            if( rs.getString("RelatToInsu") == null )
                this.RelatToInsu = null;
            else
                this.RelatToInsu = rs.getString("RelatToInsu").trim();

            this.AppAge = rs.getInt("AppAge");
            if( rs.getString("AppntOccupationType") == null )
                this.AppntOccupationType = null;
            else
                this.AppntOccupationType = rs.getString("AppntOccupationType").trim();

            if( rs.getString("AppntOccupationCode") == null )
                this.AppntOccupationCode = null;
            else
                this.AppntOccupationCode = rs.getString("AppntOccupationCode").trim();

            if( rs.getString("Name") == null )
                this.Name = null;
            else
                this.Name = rs.getString("Name").trim();

            if( rs.getString("Sex") == null )
                this.Sex = null;
            else
                this.Sex = rs.getString("Sex").trim();

            this.Birthday = rs.getDate("Birthday");
            this.Age = rs.getInt("Age");
            if( rs.getString("OccupationType") == null )
                this.OccupationType = null;
            else
                this.OccupationType = rs.getString("OccupationType").trim();

            if( rs.getString("OccupationCode") == null )
                this.OccupationCode = null;
            else
                this.OccupationCode = rs.getString("OccupationCode").trim();

            if( rs.getString("BpoFlag") == null )
                this.BpoFlag = null;
            else
                this.BpoFlag = rs.getString("BpoFlag").trim();

            if( rs.getString("RiskEvaluationResult") == null )
                this.RiskEvaluationResult = null;
            else
                this.RiskEvaluationResult = rs.getString("RiskEvaluationResult").trim();

            this.Income = rs.getDouble("Income");
            if( rs.getString("TINNO") == null )
                this.TINNO = null;
            else
                this.TINNO = rs.getString("TINNO").trim();

            if( rs.getString("TINFlag") == null )
                this.TINFlag = null;
            else
                this.TINFlag = rs.getString("TINFlag").trim();

        }
        catch(SQLException sqle) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LYVerifyAppBSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = ExceptionUtils.exceptionToString(sqle);
            this.mErrors .addOneError(tError);
            return false;
        }
        return true;
    }

    public LYVerifyAppBSchema getSchema() {
        LYVerifyAppBSchema aLYVerifyAppBSchema = new LYVerifyAppBSchema();
        aLYVerifyAppBSchema.setSchema(this);
        return aLYVerifyAppBSchema;
    }

    public LYVerifyAppBDB getDB() {
        LYVerifyAppBDB aDBOper = new LYVerifyAppBDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
    * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLYVerifyAppB描述/A>表字段
    * @return: String 返回打包后字符串
    **/
    public String encode() {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(ChgData.chgData(VerifyAppID));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(ContID));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ShardingID)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(EdorNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ContNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( ApplyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( ApplyEndDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(SaleChnl)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(State)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(PrtNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AgentBankCode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AgentCom)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(BankAgent)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AgentName)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AgentPhone)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AgentCode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AgentGroup)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ManageCom)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(Prem));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StandbyFlag1)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StandbyFlag2)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StandbyFlag3)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ModifyTime)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AppntName)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AppntSex)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( AppntBirthday ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(RelatToInsu)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(AppAge));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AppntOccupationType)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AppntOccupationCode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Name)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Sex)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( Birthday ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(Age));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(OccupationType)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(OccupationCode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(BpoFlag)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(RiskEvaluationResult)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(Income));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(TINNO)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(TINFlag));
        return strReturn.toString();
    }

    /**
    * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLYVerifyAppB>历史记账凭证主表信息</A>表字段
    * @param: strMessage String 包含一条纪录数据的字符串
    * @return: boolean
    **/
    public boolean decode(String strMessage) {
        try {
            VerifyAppID = new Long(ChgData.chgNumericStr(StrTool.getStr(strMessage,1, SysConst.PACKAGESPILTER))).longValue();
            ContID = new Long(ChgData.chgNumericStr(StrTool.getStr(strMessage,2, SysConst.PACKAGESPILTER))).longValue();
            ShardingID = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
            EdorNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
            ContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
            ApplyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER));
            ApplyEndDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER));
            SaleChnl = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
            State = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
            PrtNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
            AgentBankCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
            AgentCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
            BankAgent = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
            AgentName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER );
            AgentPhone = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15, SysConst.PACKAGESPILTER );
            AgentCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16, SysConst.PACKAGESPILTER );
            AgentGroup = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17, SysConst.PACKAGESPILTER );
            ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18, SysConst.PACKAGESPILTER );
            Prem = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,19, SysConst.PACKAGESPILTER))).doubleValue();
            StandbyFlag1 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20, SysConst.PACKAGESPILTER );
            StandbyFlag2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 21, SysConst.PACKAGESPILTER );
            StandbyFlag3 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 22, SysConst.PACKAGESPILTER );
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 23, SysConst.PACKAGESPILTER );
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 24, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 25, SysConst.PACKAGESPILTER );
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 26, SysConst.PACKAGESPILTER );
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 27, SysConst.PACKAGESPILTER));
            AppntName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 28, SysConst.PACKAGESPILTER );
            AppntSex = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 29, SysConst.PACKAGESPILTER );
            AppntBirthday = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 30, SysConst.PACKAGESPILTER));
            RelatToInsu = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 31, SysConst.PACKAGESPILTER );
            AppAge = new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,32, SysConst.PACKAGESPILTER))).intValue();
            AppntOccupationType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 33, SysConst.PACKAGESPILTER );
            AppntOccupationCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 34, SysConst.PACKAGESPILTER );
            Name = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 35, SysConst.PACKAGESPILTER );
            Sex = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 36, SysConst.PACKAGESPILTER );
            Birthday = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 37, SysConst.PACKAGESPILTER));
            Age = new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,38, SysConst.PACKAGESPILTER))).intValue();
            OccupationType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 39, SysConst.PACKAGESPILTER );
            OccupationCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 40, SysConst.PACKAGESPILTER );
            BpoFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 41, SysConst.PACKAGESPILTER );
            RiskEvaluationResult = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 42, SysConst.PACKAGESPILTER );
            Income = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,43, SysConst.PACKAGESPILTER))).doubleValue();
            TINNO = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 44, SysConst.PACKAGESPILTER );
            TINFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 45, SysConst.PACKAGESPILTER );
        }
        catch(NumberFormatException ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LYVerifyAppBSchema";
            tError.functionName = "decode";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            return false;
        }
        return true;
    }

    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("VerifyAppID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(VerifyAppID));
        }
        if (FCode.equalsIgnoreCase("ContID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ContID));
        }
        if (FCode.equalsIgnoreCase("ShardingID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ShardingID));
        }
        if (FCode.equalsIgnoreCase("EdorNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(EdorNo));
        }
        if (FCode.equalsIgnoreCase("ContNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ContNo));
        }
        if (FCode.equalsIgnoreCase("ApplyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getApplyDate()));
        }
        if (FCode.equalsIgnoreCase("ApplyEndDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getApplyEndDate()));
        }
        if (FCode.equalsIgnoreCase("SaleChnl")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SaleChnl));
        }
        if (FCode.equalsIgnoreCase("State")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(State));
        }
        if (FCode.equalsIgnoreCase("PrtNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PrtNo));
        }
        if (FCode.equalsIgnoreCase("AgentBankCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentBankCode));
        }
        if (FCode.equalsIgnoreCase("AgentCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentCom));
        }
        if (FCode.equalsIgnoreCase("BankAgent")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BankAgent));
        }
        if (FCode.equalsIgnoreCase("AgentName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentName));
        }
        if (FCode.equalsIgnoreCase("AgentPhone")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentPhone));
        }
        if (FCode.equalsIgnoreCase("AgentCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentCode));
        }
        if (FCode.equalsIgnoreCase("AgentGroup")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentGroup));
        }
        if (FCode.equalsIgnoreCase("ManageCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
        }
        if (FCode.equalsIgnoreCase("Prem")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Prem));
        }
        if (FCode.equalsIgnoreCase("StandbyFlag1")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StandbyFlag1));
        }
        if (FCode.equalsIgnoreCase("StandbyFlag2")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StandbyFlag2));
        }
        if (FCode.equalsIgnoreCase("StandbyFlag3")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StandbyFlag3));
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
        }
        if (FCode.equalsIgnoreCase("AppntName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AppntName));
        }
        if (FCode.equalsIgnoreCase("AppntSex")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AppntSex));
        }
        if (FCode.equalsIgnoreCase("AppntBirthday")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getAppntBirthday()));
        }
        if (FCode.equalsIgnoreCase("RelatToInsu")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RelatToInsu));
        }
        if (FCode.equalsIgnoreCase("AppAge")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AppAge));
        }
        if (FCode.equalsIgnoreCase("AppntOccupationType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AppntOccupationType));
        }
        if (FCode.equalsIgnoreCase("AppntOccupationCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AppntOccupationCode));
        }
        if (FCode.equalsIgnoreCase("Name")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Name));
        }
        if (FCode.equalsIgnoreCase("Sex")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Sex));
        }
        if (FCode.equalsIgnoreCase("Birthday")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getBirthday()));
        }
        if (FCode.equalsIgnoreCase("Age")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Age));
        }
        if (FCode.equalsIgnoreCase("OccupationType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OccupationType));
        }
        if (FCode.equalsIgnoreCase("OccupationCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OccupationCode));
        }
        if (FCode.equalsIgnoreCase("BpoFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BpoFlag));
        }
        if (FCode.equalsIgnoreCase("RiskEvaluationResult")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RiskEvaluationResult));
        }
        if (FCode.equalsIgnoreCase("Income")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Income));
        }
        if (FCode.equalsIgnoreCase("TINNO")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(TINNO));
        }
        if (FCode.equalsIgnoreCase("TINFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(TINFlag));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = String.valueOf(VerifyAppID);
                break;
            case 1:
                strFieldValue = String.valueOf(ContID);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(ShardingID);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(EdorNo);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(ContNo);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getApplyDate()));
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getApplyEndDate()));
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(SaleChnl);
                break;
            case 8:
                strFieldValue = StrTool.GBKToUnicode(State);
                break;
            case 9:
                strFieldValue = StrTool.GBKToUnicode(PrtNo);
                break;
            case 10:
                strFieldValue = StrTool.GBKToUnicode(AgentBankCode);
                break;
            case 11:
                strFieldValue = StrTool.GBKToUnicode(AgentCom);
                break;
            case 12:
                strFieldValue = StrTool.GBKToUnicode(BankAgent);
                break;
            case 13:
                strFieldValue = StrTool.GBKToUnicode(AgentName);
                break;
            case 14:
                strFieldValue = StrTool.GBKToUnicode(AgentPhone);
                break;
            case 15:
                strFieldValue = StrTool.GBKToUnicode(AgentCode);
                break;
            case 16:
                strFieldValue = StrTool.GBKToUnicode(AgentGroup);
                break;
            case 17:
                strFieldValue = StrTool.GBKToUnicode(ManageCom);
                break;
            case 18:
                strFieldValue = String.valueOf(Prem);
                break;
            case 19:
                strFieldValue = StrTool.GBKToUnicode(StandbyFlag1);
                break;
            case 20:
                strFieldValue = StrTool.GBKToUnicode(StandbyFlag2);
                break;
            case 21:
                strFieldValue = StrTool.GBKToUnicode(StandbyFlag3);
                break;
            case 22:
                strFieldValue = StrTool.GBKToUnicode(Operator);
                break;
            case 23:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
                break;
            case 24:
                strFieldValue = StrTool.GBKToUnicode(MakeTime);
                break;
            case 25:
                strFieldValue = StrTool.GBKToUnicode(ModifyTime);
                break;
            case 26:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
                break;
            case 27:
                strFieldValue = StrTool.GBKToUnicode(AppntName);
                break;
            case 28:
                strFieldValue = StrTool.GBKToUnicode(AppntSex);
                break;
            case 29:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getAppntBirthday()));
                break;
            case 30:
                strFieldValue = StrTool.GBKToUnicode(RelatToInsu);
                break;
            case 31:
                strFieldValue = String.valueOf(AppAge);
                break;
            case 32:
                strFieldValue = StrTool.GBKToUnicode(AppntOccupationType);
                break;
            case 33:
                strFieldValue = StrTool.GBKToUnicode(AppntOccupationCode);
                break;
            case 34:
                strFieldValue = StrTool.GBKToUnicode(Name);
                break;
            case 35:
                strFieldValue = StrTool.GBKToUnicode(Sex);
                break;
            case 36:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getBirthday()));
                break;
            case 37:
                strFieldValue = String.valueOf(Age);
                break;
            case 38:
                strFieldValue = StrTool.GBKToUnicode(OccupationType);
                break;
            case 39:
                strFieldValue = StrTool.GBKToUnicode(OccupationCode);
                break;
            case 40:
                strFieldValue = StrTool.GBKToUnicode(BpoFlag);
                break;
            case 41:
                strFieldValue = StrTool.GBKToUnicode(RiskEvaluationResult);
                break;
            case 42:
                strFieldValue = String.valueOf(Income);
                break;
            case 43:
                strFieldValue = StrTool.GBKToUnicode(TINNO);
                break;
            case 44:
                strFieldValue = StrTool.GBKToUnicode(TINFlag);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("VerifyAppID")) {
            if( FValue != null && !FValue.equals("")) {
                VerifyAppID = new Long(FValue).longValue();
            }
        }
        if (FCode.equalsIgnoreCase("ContID")) {
            if( FValue != null && !FValue.equals("")) {
                ContID = new Long(FValue).longValue();
            }
        }
        if (FCode.equalsIgnoreCase("ShardingID")) {
            if( FValue != null && !FValue.equals(""))
            {
                ShardingID = FValue.trim();
            }
            else
                ShardingID = null;
        }
        if (FCode.equalsIgnoreCase("EdorNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                EdorNo = FValue.trim();
            }
            else
                EdorNo = null;
        }
        if (FCode.equalsIgnoreCase("ContNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                ContNo = FValue.trim();
            }
            else
                ContNo = null;
        }
        if (FCode.equalsIgnoreCase("ApplyDate")) {
            if(FValue != null && !FValue.equals("")) {
                ApplyDate = fDate.getDate( FValue );
            }
            else
                ApplyDate = null;
        }
        if (FCode.equalsIgnoreCase("ApplyEndDate")) {
            if(FValue != null && !FValue.equals("")) {
                ApplyEndDate = fDate.getDate( FValue );
            }
            else
                ApplyEndDate = null;
        }
        if (FCode.equalsIgnoreCase("SaleChnl")) {
            if( FValue != null && !FValue.equals(""))
            {
                SaleChnl = FValue.trim();
            }
            else
                SaleChnl = null;
        }
        if (FCode.equalsIgnoreCase("State")) {
            if( FValue != null && !FValue.equals(""))
            {
                State = FValue.trim();
            }
            else
                State = null;
        }
        if (FCode.equalsIgnoreCase("PrtNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                PrtNo = FValue.trim();
            }
            else
                PrtNo = null;
        }
        if (FCode.equalsIgnoreCase("AgentBankCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                AgentBankCode = FValue.trim();
            }
            else
                AgentBankCode = null;
        }
        if (FCode.equalsIgnoreCase("AgentCom")) {
            if( FValue != null && !FValue.equals(""))
            {
                AgentCom = FValue.trim();
            }
            else
                AgentCom = null;
        }
        if (FCode.equalsIgnoreCase("BankAgent")) {
            if( FValue != null && !FValue.equals(""))
            {
                BankAgent = FValue.trim();
            }
            else
                BankAgent = null;
        }
        if (FCode.equalsIgnoreCase("AgentName")) {
            if( FValue != null && !FValue.equals(""))
            {
                AgentName = FValue.trim();
            }
            else
                AgentName = null;
        }
        if (FCode.equalsIgnoreCase("AgentPhone")) {
            if( FValue != null && !FValue.equals(""))
            {
                AgentPhone = FValue.trim();
            }
            else
                AgentPhone = null;
        }
        if (FCode.equalsIgnoreCase("AgentCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                AgentCode = FValue.trim();
            }
            else
                AgentCode = null;
        }
        if (FCode.equalsIgnoreCase("AgentGroup")) {
            if( FValue != null && !FValue.equals(""))
            {
                AgentGroup = FValue.trim();
            }
            else
                AgentGroup = null;
        }
        if (FCode.equalsIgnoreCase("ManageCom")) {
            if( FValue != null && !FValue.equals(""))
            {
                ManageCom = FValue.trim();
            }
            else
                ManageCom = null;
        }
        if (FCode.equalsIgnoreCase("Prem")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                Prem = d;
            }
        }
        if (FCode.equalsIgnoreCase("StandbyFlag1")) {
            if( FValue != null && !FValue.equals(""))
            {
                StandbyFlag1 = FValue.trim();
            }
            else
                StandbyFlag1 = null;
        }
        if (FCode.equalsIgnoreCase("StandbyFlag2")) {
            if( FValue != null && !FValue.equals(""))
            {
                StandbyFlag2 = FValue.trim();
            }
            else
                StandbyFlag2 = null;
        }
        if (FCode.equalsIgnoreCase("StandbyFlag3")) {
            if( FValue != null && !FValue.equals(""))
            {
                StandbyFlag3 = FValue.trim();
            }
            else
                StandbyFlag3 = null;
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            if( FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
                Operator = null;
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if(FValue != null && !FValue.equals("")) {
                MakeDate = fDate.getDate( FValue );
            }
            else
                MakeDate = null;
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
                MakeTime = null;
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
                ModifyTime = null;
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if(FValue != null && !FValue.equals("")) {
                ModifyDate = fDate.getDate( FValue );
            }
            else
                ModifyDate = null;
        }
        if (FCode.equalsIgnoreCase("AppntName")) {
            if( FValue != null && !FValue.equals(""))
            {
                AppntName = FValue.trim();
            }
            else
                AppntName = null;
        }
        if (FCode.equalsIgnoreCase("AppntSex")) {
            if( FValue != null && !FValue.equals(""))
            {
                AppntSex = FValue.trim();
            }
            else
                AppntSex = null;
        }
        if (FCode.equalsIgnoreCase("AppntBirthday")) {
            if(FValue != null && !FValue.equals("")) {
                AppntBirthday = fDate.getDate( FValue );
            }
            else
                AppntBirthday = null;
        }
        if (FCode.equalsIgnoreCase("RelatToInsu")) {
            if( FValue != null && !FValue.equals(""))
            {
                RelatToInsu = FValue.trim();
            }
            else
                RelatToInsu = null;
        }
        if (FCode.equalsIgnoreCase("AppAge")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                AppAge = i;
            }
        }
        if (FCode.equalsIgnoreCase("AppntOccupationType")) {
            if( FValue != null && !FValue.equals(""))
            {
                AppntOccupationType = FValue.trim();
            }
            else
                AppntOccupationType = null;
        }
        if (FCode.equalsIgnoreCase("AppntOccupationCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                AppntOccupationCode = FValue.trim();
            }
            else
                AppntOccupationCode = null;
        }
        if (FCode.equalsIgnoreCase("Name")) {
            if( FValue != null && !FValue.equals(""))
            {
                Name = FValue.trim();
            }
            else
                Name = null;
        }
        if (FCode.equalsIgnoreCase("Sex")) {
            if( FValue != null && !FValue.equals(""))
            {
                Sex = FValue.trim();
            }
            else
                Sex = null;
        }
        if (FCode.equalsIgnoreCase("Birthday")) {
            if(FValue != null && !FValue.equals("")) {
                Birthday = fDate.getDate( FValue );
            }
            else
                Birthday = null;
        }
        if (FCode.equalsIgnoreCase("Age")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                Age = i;
            }
        }
        if (FCode.equalsIgnoreCase("OccupationType")) {
            if( FValue != null && !FValue.equals(""))
            {
                OccupationType = FValue.trim();
            }
            else
                OccupationType = null;
        }
        if (FCode.equalsIgnoreCase("OccupationCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                OccupationCode = FValue.trim();
            }
            else
                OccupationCode = null;
        }
        if (FCode.equalsIgnoreCase("BpoFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                BpoFlag = FValue.trim();
            }
            else
                BpoFlag = null;
        }
        if (FCode.equalsIgnoreCase("RiskEvaluationResult")) {
            if( FValue != null && !FValue.equals(""))
            {
                RiskEvaluationResult = FValue.trim();
            }
            else
                RiskEvaluationResult = null;
        }
        if (FCode.equalsIgnoreCase("Income")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                Income = d;
            }
        }
        if (FCode.equalsIgnoreCase("TINNO")) {
            if( FValue != null && !FValue.equals(""))
            {
                TINNO = FValue.trim();
            }
            else
                TINNO = null;
        }
        if (FCode.equalsIgnoreCase("TINFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                TINFlag = FValue.trim();
            }
            else
                TINFlag = null;
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject) return true;
        if (otherObject == null) return false;
        if (getClass() != otherObject.getClass()) return false;
        LYVerifyAppBSchema other = (LYVerifyAppBSchema)otherObject;
        return
            VerifyAppID == other.getVerifyAppID()
            && ContID == other.getContID()
            && ShardingID.equals(other.getShardingID())
            && EdorNo.equals(other.getEdorNo())
            && ContNo.equals(other.getContNo())
            && fDate.getString(ApplyDate).equals(other.getApplyDate())
            && fDate.getString(ApplyEndDate).equals(other.getApplyEndDate())
            && SaleChnl.equals(other.getSaleChnl())
            && State.equals(other.getState())
            && PrtNo.equals(other.getPrtNo())
            && AgentBankCode.equals(other.getAgentBankCode())
            && AgentCom.equals(other.getAgentCom())
            && BankAgent.equals(other.getBankAgent())
            && AgentName.equals(other.getAgentName())
            && AgentPhone.equals(other.getAgentPhone())
            && AgentCode.equals(other.getAgentCode())
            && AgentGroup.equals(other.getAgentGroup())
            && ManageCom.equals(other.getManageCom())
            && Prem == other.getPrem()
            && StandbyFlag1.equals(other.getStandbyFlag1())
            && StandbyFlag2.equals(other.getStandbyFlag2())
            && StandbyFlag3.equals(other.getStandbyFlag3())
            && Operator.equals(other.getOperator())
            && fDate.getString(MakeDate).equals(other.getMakeDate())
            && MakeTime.equals(other.getMakeTime())
            && ModifyTime.equals(other.getModifyTime())
            && fDate.getString(ModifyDate).equals(other.getModifyDate())
            && AppntName.equals(other.getAppntName())
            && AppntSex.equals(other.getAppntSex())
            && fDate.getString(AppntBirthday).equals(other.getAppntBirthday())
            && RelatToInsu.equals(other.getRelatToInsu())
            && AppAge == other.getAppAge()
            && AppntOccupationType.equals(other.getAppntOccupationType())
            && AppntOccupationCode.equals(other.getAppntOccupationCode())
            && Name.equals(other.getName())
            && Sex.equals(other.getSex())
            && fDate.getString(Birthday).equals(other.getBirthday())
            && Age == other.getAge()
            && OccupationType.equals(other.getOccupationType())
            && OccupationCode.equals(other.getOccupationCode())
            && BpoFlag.equals(other.getBpoFlag())
            && RiskEvaluationResult.equals(other.getRiskEvaluationResult())
            && Income == other.getIncome()
            && TINNO.equals(other.getTINNO())
            && TINFlag.equals(other.getTINFlag());
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("VerifyAppID") ) {
            return 0;
        }
        if( strFieldName.equals("ContID") ) {
            return 1;
        }
        if( strFieldName.equals("ShardingID") ) {
            return 2;
        }
        if( strFieldName.equals("EdorNo") ) {
            return 3;
        }
        if( strFieldName.equals("ContNo") ) {
            return 4;
        }
        if( strFieldName.equals("ApplyDate") ) {
            return 5;
        }
        if( strFieldName.equals("ApplyEndDate") ) {
            return 6;
        }
        if( strFieldName.equals("SaleChnl") ) {
            return 7;
        }
        if( strFieldName.equals("State") ) {
            return 8;
        }
        if( strFieldName.equals("PrtNo") ) {
            return 9;
        }
        if( strFieldName.equals("AgentBankCode") ) {
            return 10;
        }
        if( strFieldName.equals("AgentCom") ) {
            return 11;
        }
        if( strFieldName.equals("BankAgent") ) {
            return 12;
        }
        if( strFieldName.equals("AgentName") ) {
            return 13;
        }
        if( strFieldName.equals("AgentPhone") ) {
            return 14;
        }
        if( strFieldName.equals("AgentCode") ) {
            return 15;
        }
        if( strFieldName.equals("AgentGroup") ) {
            return 16;
        }
        if( strFieldName.equals("ManageCom") ) {
            return 17;
        }
        if( strFieldName.equals("Prem") ) {
            return 18;
        }
        if( strFieldName.equals("StandbyFlag1") ) {
            return 19;
        }
        if( strFieldName.equals("StandbyFlag2") ) {
            return 20;
        }
        if( strFieldName.equals("StandbyFlag3") ) {
            return 21;
        }
        if( strFieldName.equals("Operator") ) {
            return 22;
        }
        if( strFieldName.equals("MakeDate") ) {
            return 23;
        }
        if( strFieldName.equals("MakeTime") ) {
            return 24;
        }
        if( strFieldName.equals("ModifyTime") ) {
            return 25;
        }
        if( strFieldName.equals("ModifyDate") ) {
            return 26;
        }
        if( strFieldName.equals("AppntName") ) {
            return 27;
        }
        if( strFieldName.equals("AppntSex") ) {
            return 28;
        }
        if( strFieldName.equals("AppntBirthday") ) {
            return 29;
        }
        if( strFieldName.equals("RelatToInsu") ) {
            return 30;
        }
        if( strFieldName.equals("AppAge") ) {
            return 31;
        }
        if( strFieldName.equals("AppntOccupationType") ) {
            return 32;
        }
        if( strFieldName.equals("AppntOccupationCode") ) {
            return 33;
        }
        if( strFieldName.equals("Name") ) {
            return 34;
        }
        if( strFieldName.equals("Sex") ) {
            return 35;
        }
        if( strFieldName.equals("Birthday") ) {
            return 36;
        }
        if( strFieldName.equals("Age") ) {
            return 37;
        }
        if( strFieldName.equals("OccupationType") ) {
            return 38;
        }
        if( strFieldName.equals("OccupationCode") ) {
            return 39;
        }
        if( strFieldName.equals("BpoFlag") ) {
            return 40;
        }
        if( strFieldName.equals("RiskEvaluationResult") ) {
            return 41;
        }
        if( strFieldName.equals("Income") ) {
            return 42;
        }
        if( strFieldName.equals("TINNO") ) {
            return 43;
        }
        if( strFieldName.equals("TINFlag") ) {
            return 44;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "VerifyAppID";
                break;
            case 1:
                strFieldName = "ContID";
                break;
            case 2:
                strFieldName = "ShardingID";
                break;
            case 3:
                strFieldName = "EdorNo";
                break;
            case 4:
                strFieldName = "ContNo";
                break;
            case 5:
                strFieldName = "ApplyDate";
                break;
            case 6:
                strFieldName = "ApplyEndDate";
                break;
            case 7:
                strFieldName = "SaleChnl";
                break;
            case 8:
                strFieldName = "State";
                break;
            case 9:
                strFieldName = "PrtNo";
                break;
            case 10:
                strFieldName = "AgentBankCode";
                break;
            case 11:
                strFieldName = "AgentCom";
                break;
            case 12:
                strFieldName = "BankAgent";
                break;
            case 13:
                strFieldName = "AgentName";
                break;
            case 14:
                strFieldName = "AgentPhone";
                break;
            case 15:
                strFieldName = "AgentCode";
                break;
            case 16:
                strFieldName = "AgentGroup";
                break;
            case 17:
                strFieldName = "ManageCom";
                break;
            case 18:
                strFieldName = "Prem";
                break;
            case 19:
                strFieldName = "StandbyFlag1";
                break;
            case 20:
                strFieldName = "StandbyFlag2";
                break;
            case 21:
                strFieldName = "StandbyFlag3";
                break;
            case 22:
                strFieldName = "Operator";
                break;
            case 23:
                strFieldName = "MakeDate";
                break;
            case 24:
                strFieldName = "MakeTime";
                break;
            case 25:
                strFieldName = "ModifyTime";
                break;
            case 26:
                strFieldName = "ModifyDate";
                break;
            case 27:
                strFieldName = "AppntName";
                break;
            case 28:
                strFieldName = "AppntSex";
                break;
            case 29:
                strFieldName = "AppntBirthday";
                break;
            case 30:
                strFieldName = "RelatToInsu";
                break;
            case 31:
                strFieldName = "AppAge";
                break;
            case 32:
                strFieldName = "AppntOccupationType";
                break;
            case 33:
                strFieldName = "AppntOccupationCode";
                break;
            case 34:
                strFieldName = "Name";
                break;
            case 35:
                strFieldName = "Sex";
                break;
            case 36:
                strFieldName = "Birthday";
                break;
            case 37:
                strFieldName = "Age";
                break;
            case 38:
                strFieldName = "OccupationType";
                break;
            case 39:
                strFieldName = "OccupationCode";
                break;
            case 40:
                strFieldName = "BpoFlag";
                break;
            case 41:
                strFieldName = "RiskEvaluationResult";
                break;
            case 42:
                strFieldName = "Income";
                break;
            case 43:
                strFieldName = "TINNO";
                break;
            case 44:
                strFieldName = "TINFlag";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "VERIFYAPPID":
                return Schema.TYPE_LONG;
            case "CONTID":
                return Schema.TYPE_LONG;
            case "SHARDINGID":
                return Schema.TYPE_STRING;
            case "EDORNO":
                return Schema.TYPE_STRING;
            case "CONTNO":
                return Schema.TYPE_STRING;
            case "APPLYDATE":
                return Schema.TYPE_DATE;
            case "APPLYENDDATE":
                return Schema.TYPE_DATE;
            case "SALECHNL":
                return Schema.TYPE_STRING;
            case "STATE":
                return Schema.TYPE_STRING;
            case "PRTNO":
                return Schema.TYPE_STRING;
            case "AGENTBANKCODE":
                return Schema.TYPE_STRING;
            case "AGENTCOM":
                return Schema.TYPE_STRING;
            case "BANKAGENT":
                return Schema.TYPE_STRING;
            case "AGENTNAME":
                return Schema.TYPE_STRING;
            case "AGENTPHONE":
                return Schema.TYPE_STRING;
            case "AGENTCODE":
                return Schema.TYPE_STRING;
            case "AGENTGROUP":
                return Schema.TYPE_STRING;
            case "MANAGECOM":
                return Schema.TYPE_STRING;
            case "PREM":
                return Schema.TYPE_DOUBLE;
            case "STANDBYFLAG1":
                return Schema.TYPE_STRING;
            case "STANDBYFLAG2":
                return Schema.TYPE_STRING;
            case "STANDBYFLAG3":
                return Schema.TYPE_STRING;
            case "OPERATOR":
                return Schema.TYPE_STRING;
            case "MAKEDATE":
                return Schema.TYPE_DATE;
            case "MAKETIME":
                return Schema.TYPE_STRING;
            case "MODIFYTIME":
                return Schema.TYPE_STRING;
            case "MODIFYDATE":
                return Schema.TYPE_DATE;
            case "APPNTNAME":
                return Schema.TYPE_STRING;
            case "APPNTSEX":
                return Schema.TYPE_STRING;
            case "APPNTBIRTHDAY":
                return Schema.TYPE_DATE;
            case "RELATTOINSU":
                return Schema.TYPE_STRING;
            case "APPAGE":
                return Schema.TYPE_INT;
            case "APPNTOCCUPATIONTYPE":
                return Schema.TYPE_STRING;
            case "APPNTOCCUPATIONCODE":
                return Schema.TYPE_STRING;
            case "NAME":
                return Schema.TYPE_STRING;
            case "SEX":
                return Schema.TYPE_STRING;
            case "BIRTHDAY":
                return Schema.TYPE_DATE;
            case "AGE":
                return Schema.TYPE_INT;
            case "OCCUPATIONTYPE":
                return Schema.TYPE_STRING;
            case "OCCUPATIONCODE":
                return Schema.TYPE_STRING;
            case "BPOFLAG":
                return Schema.TYPE_STRING;
            case "RISKEVALUATIONRESULT":
                return Schema.TYPE_STRING;
            case "INCOME":
                return Schema.TYPE_DOUBLE;
            case "TINNO":
                return Schema.TYPE_STRING;
            case "TINFLAG":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_LONG;
            case 1:
                return Schema.TYPE_LONG;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_DATE;
            case 6:
                return Schema.TYPE_DATE;
            case 7:
                return Schema.TYPE_STRING;
            case 8:
                return Schema.TYPE_STRING;
            case 9:
                return Schema.TYPE_STRING;
            case 10:
                return Schema.TYPE_STRING;
            case 11:
                return Schema.TYPE_STRING;
            case 12:
                return Schema.TYPE_STRING;
            case 13:
                return Schema.TYPE_STRING;
            case 14:
                return Schema.TYPE_STRING;
            case 15:
                return Schema.TYPE_STRING;
            case 16:
                return Schema.TYPE_STRING;
            case 17:
                return Schema.TYPE_STRING;
            case 18:
                return Schema.TYPE_DOUBLE;
            case 19:
                return Schema.TYPE_STRING;
            case 20:
                return Schema.TYPE_STRING;
            case 21:
                return Schema.TYPE_STRING;
            case 22:
                return Schema.TYPE_STRING;
            case 23:
                return Schema.TYPE_DATE;
            case 24:
                return Schema.TYPE_STRING;
            case 25:
                return Schema.TYPE_STRING;
            case 26:
                return Schema.TYPE_DATE;
            case 27:
                return Schema.TYPE_STRING;
            case 28:
                return Schema.TYPE_STRING;
            case 29:
                return Schema.TYPE_DATE;
            case 30:
                return Schema.TYPE_STRING;
            case 31:
                return Schema.TYPE_INT;
            case 32:
                return Schema.TYPE_STRING;
            case 33:
                return Schema.TYPE_STRING;
            case 34:
                return Schema.TYPE_STRING;
            case 35:
                return Schema.TYPE_STRING;
            case 36:
                return Schema.TYPE_DATE;
            case 37:
                return Schema.TYPE_INT;
            case 38:
                return Schema.TYPE_STRING;
            case 39:
                return Schema.TYPE_STRING;
            case 40:
                return Schema.TYPE_STRING;
            case 41:
                return Schema.TYPE_STRING;
            case 42:
                return Schema.TYPE_DOUBLE;
            case 43:
                return Schema.TYPE_STRING;
            case 44:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
}
