/**
 * Copyright (c) 2017 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import com.sinosoft.lis.db.VMS_OUTPUT_CUSTOMERDB;
import com.sinosoft.utility.*;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * <p>ClassName: VMS_OUTPUT_CUSTOMERSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2017</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2017-10-21
 */
public class VMS_OUTPUT_CUSTOMERSchema implements Schema, Cloneable {
    // @Field
    /** 基准日 */
    private String VIC_INDATE;
    /** 客户号 */
    private String VIC_CUSTOMER_ID;
    /** 客户纳税人中文名称 */
    private String VIC_CUSTOMER_CNAME;
    /** 客户纳税人识别号 */
    private String VIC_CUSTOMER_TAXNO;
    /** 客户开户账号 */
    private String VIC_CUSTOMER_ACCOUNT;
    /** 客户开户银行中文名称 */
    private String VIC_CUSTOMER_CBANK;
    /** 客户电话 */
    private String VIC_CUSTOMER_PHONE;
    /** 客户邮箱地址 */
    private String VIC_CUSTOMER_EMAIL;
    /** 客户地址 */
    private String VIC_CUSTOMER_ADDRESS;
    /** 客户纳税人类别 */
    private String VIC_TAXPAYER_TYPE;
    /** 发票类型 */
    private String VIC_FAPIAO_TYPE;
    /** 客户类型 */
    private String VIC_CUSTOMER_TYPE;
    /** 客户是否打票 */
    private String VIC_CUSTOMER_FAPIAO_FLAG;
    /** 客户国籍 */
    private String VIC_CUSTOMER_NATIONALITY;
    /** 数据来源 */
    private String VIC_DATA_SOURCE;
    /** 联系人名称 */
    private String VIC_LINK_NAME;
    /** 联系人电话 */
    private String VIC_LINK_PHONE;
    /** 联系人地址 */
    private String VIC_LINK_ADDRESS;
    /** 客户邮编 */
    private String VIC_CUSTOMER_ZIP_CODE;
    /** 上传处理标记 */
    private String VIC_CUSTOMER_UPLOAD_FLAG;

    public static final int FIELDNUM = 20;    // 数据库表的字段个数

    private static String[] PK;                // 主键

    public CErrors mErrors;            // 错误信息

    // @Constructor
    public VMS_OUTPUT_CUSTOMERSchema() {
        mErrors = new CErrors();

        String[] pk = new String[2];
        pk[0] = "VIC_INDATE";
        pk[1] = "VIC_CUSTOMER_ID";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException {
        VMS_OUTPUT_CUSTOMERSchema cloned = (VMS_OUTPUT_CUSTOMERSchema)super.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK() {
        return PK;
    }

    public String getVIC_INDATE() {
        return VIC_INDATE;
    }
    public void setVIC_INDATE(String aVIC_INDATE) {
        VIC_INDATE = aVIC_INDATE;
    }
    public String getVIC_CUSTOMER_ID() {
        return VIC_CUSTOMER_ID;
    }
    public void setVIC_CUSTOMER_ID(String aVIC_CUSTOMER_ID) {
        VIC_CUSTOMER_ID = aVIC_CUSTOMER_ID;
    }
    public String getVIC_CUSTOMER_CNAME() {
        return VIC_CUSTOMER_CNAME;
    }
    public void setVIC_CUSTOMER_CNAME(String aVIC_CUSTOMER_CNAME) {
        VIC_CUSTOMER_CNAME = aVIC_CUSTOMER_CNAME;
    }
    public String getVIC_CUSTOMER_TAXNO() {
        return VIC_CUSTOMER_TAXNO;
    }
    public void setVIC_CUSTOMER_TAXNO(String aVIC_CUSTOMER_TAXNO) {
        VIC_CUSTOMER_TAXNO = aVIC_CUSTOMER_TAXNO;
    }
    public String getVIC_CUSTOMER_ACCOUNT() {
        return VIC_CUSTOMER_ACCOUNT;
    }
    public void setVIC_CUSTOMER_ACCOUNT(String aVIC_CUSTOMER_ACCOUNT) {
        VIC_CUSTOMER_ACCOUNT = aVIC_CUSTOMER_ACCOUNT;
    }
    public String getVIC_CUSTOMER_CBANK() {
        return VIC_CUSTOMER_CBANK;
    }
    public void setVIC_CUSTOMER_CBANK(String aVIC_CUSTOMER_CBANK) {
        VIC_CUSTOMER_CBANK = aVIC_CUSTOMER_CBANK;
    }
    public String getVIC_CUSTOMER_PHONE() {
        return VIC_CUSTOMER_PHONE;
    }
    public void setVIC_CUSTOMER_PHONE(String aVIC_CUSTOMER_PHONE) {
        VIC_CUSTOMER_PHONE = aVIC_CUSTOMER_PHONE;
    }
    public String getVIC_CUSTOMER_EMAIL() {
        return VIC_CUSTOMER_EMAIL;
    }
    public void setVIC_CUSTOMER_EMAIL(String aVIC_CUSTOMER_EMAIL) {
        VIC_CUSTOMER_EMAIL = aVIC_CUSTOMER_EMAIL;
    }
    public String getVIC_CUSTOMER_ADDRESS() {
        return VIC_CUSTOMER_ADDRESS;
    }
    public void setVIC_CUSTOMER_ADDRESS(String aVIC_CUSTOMER_ADDRESS) {
        VIC_CUSTOMER_ADDRESS = aVIC_CUSTOMER_ADDRESS;
    }
    public String getVIC_TAXPAYER_TYPE() {
        return VIC_TAXPAYER_TYPE;
    }
    public void setVIC_TAXPAYER_TYPE(String aVIC_TAXPAYER_TYPE) {
        VIC_TAXPAYER_TYPE = aVIC_TAXPAYER_TYPE;
    }
    public String getVIC_FAPIAO_TYPE() {
        return VIC_FAPIAO_TYPE;
    }
    public void setVIC_FAPIAO_TYPE(String aVIC_FAPIAO_TYPE) {
        VIC_FAPIAO_TYPE = aVIC_FAPIAO_TYPE;
    }
    public String getVIC_CUSTOMER_TYPE() {
        return VIC_CUSTOMER_TYPE;
    }
    public void setVIC_CUSTOMER_TYPE(String aVIC_CUSTOMER_TYPE) {
        VIC_CUSTOMER_TYPE = aVIC_CUSTOMER_TYPE;
    }
    public String getVIC_CUSTOMER_FAPIAO_FLAG() {
        return VIC_CUSTOMER_FAPIAO_FLAG;
    }
    public void setVIC_CUSTOMER_FAPIAO_FLAG(String aVIC_CUSTOMER_FAPIAO_FLAG) {
        VIC_CUSTOMER_FAPIAO_FLAG = aVIC_CUSTOMER_FAPIAO_FLAG;
    }
    public String getVIC_CUSTOMER_NATIONALITY() {
        return VIC_CUSTOMER_NATIONALITY;
    }
    public void setVIC_CUSTOMER_NATIONALITY(String aVIC_CUSTOMER_NATIONALITY) {
        VIC_CUSTOMER_NATIONALITY = aVIC_CUSTOMER_NATIONALITY;
    }
    public String getVIC_DATA_SOURCE() {
        return VIC_DATA_SOURCE;
    }
    public void setVIC_DATA_SOURCE(String aVIC_DATA_SOURCE) {
        VIC_DATA_SOURCE = aVIC_DATA_SOURCE;
    }
    public String getVIC_LINK_NAME() {
        return VIC_LINK_NAME;
    }
    public void setVIC_LINK_NAME(String aVIC_LINK_NAME) {
        VIC_LINK_NAME = aVIC_LINK_NAME;
    }
    public String getVIC_LINK_PHONE() {
        return VIC_LINK_PHONE;
    }
    public void setVIC_LINK_PHONE(String aVIC_LINK_PHONE) {
        VIC_LINK_PHONE = aVIC_LINK_PHONE;
    }
    public String getVIC_LINK_ADDRESS() {
        return VIC_LINK_ADDRESS;
    }
    public void setVIC_LINK_ADDRESS(String aVIC_LINK_ADDRESS) {
        VIC_LINK_ADDRESS = aVIC_LINK_ADDRESS;
    }
    public String getVIC_CUSTOMER_ZIP_CODE() {
        return VIC_CUSTOMER_ZIP_CODE;
    }
    public void setVIC_CUSTOMER_ZIP_CODE(String aVIC_CUSTOMER_ZIP_CODE) {
        VIC_CUSTOMER_ZIP_CODE = aVIC_CUSTOMER_ZIP_CODE;
    }
    public String getVIC_CUSTOMER_UPLOAD_FLAG() {
        return VIC_CUSTOMER_UPLOAD_FLAG;
    }
    public void setVIC_CUSTOMER_UPLOAD_FLAG(String aVIC_CUSTOMER_UPLOAD_FLAG) {
        VIC_CUSTOMER_UPLOAD_FLAG = aVIC_CUSTOMER_UPLOAD_FLAG;
    }

    /**
    * 使用另外一个 VMS_OUTPUT_CUSTOMERSchema 对象给 Schema 赋值
    * @param: aVMS_OUTPUT_CUSTOMERSchema VMS_OUTPUT_CUSTOMERSchema
    **/
    public void setSchema(VMS_OUTPUT_CUSTOMERSchema aVMS_OUTPUT_CUSTOMERSchema) {
        this.VIC_INDATE = aVMS_OUTPUT_CUSTOMERSchema.getVIC_INDATE();
        this.VIC_CUSTOMER_ID = aVMS_OUTPUT_CUSTOMERSchema.getVIC_CUSTOMER_ID();
        this.VIC_CUSTOMER_CNAME = aVMS_OUTPUT_CUSTOMERSchema.getVIC_CUSTOMER_CNAME();
        this.VIC_CUSTOMER_TAXNO = aVMS_OUTPUT_CUSTOMERSchema.getVIC_CUSTOMER_TAXNO();
        this.VIC_CUSTOMER_ACCOUNT = aVMS_OUTPUT_CUSTOMERSchema.getVIC_CUSTOMER_ACCOUNT();
        this.VIC_CUSTOMER_CBANK = aVMS_OUTPUT_CUSTOMERSchema.getVIC_CUSTOMER_CBANK();
        this.VIC_CUSTOMER_PHONE = aVMS_OUTPUT_CUSTOMERSchema.getVIC_CUSTOMER_PHONE();
        this.VIC_CUSTOMER_EMAIL = aVMS_OUTPUT_CUSTOMERSchema.getVIC_CUSTOMER_EMAIL();
        this.VIC_CUSTOMER_ADDRESS = aVMS_OUTPUT_CUSTOMERSchema.getVIC_CUSTOMER_ADDRESS();
        this.VIC_TAXPAYER_TYPE = aVMS_OUTPUT_CUSTOMERSchema.getVIC_TAXPAYER_TYPE();
        this.VIC_FAPIAO_TYPE = aVMS_OUTPUT_CUSTOMERSchema.getVIC_FAPIAO_TYPE();
        this.VIC_CUSTOMER_TYPE = aVMS_OUTPUT_CUSTOMERSchema.getVIC_CUSTOMER_TYPE();
        this.VIC_CUSTOMER_FAPIAO_FLAG = aVMS_OUTPUT_CUSTOMERSchema.getVIC_CUSTOMER_FAPIAO_FLAG();
        this.VIC_CUSTOMER_NATIONALITY = aVMS_OUTPUT_CUSTOMERSchema.getVIC_CUSTOMER_NATIONALITY();
        this.VIC_DATA_SOURCE = aVMS_OUTPUT_CUSTOMERSchema.getVIC_DATA_SOURCE();
        this.VIC_LINK_NAME = aVMS_OUTPUT_CUSTOMERSchema.getVIC_LINK_NAME();
        this.VIC_LINK_PHONE = aVMS_OUTPUT_CUSTOMERSchema.getVIC_LINK_PHONE();
        this.VIC_LINK_ADDRESS = aVMS_OUTPUT_CUSTOMERSchema.getVIC_LINK_ADDRESS();
        this.VIC_CUSTOMER_ZIP_CODE = aVMS_OUTPUT_CUSTOMERSchema.getVIC_CUSTOMER_ZIP_CODE();
        this.VIC_CUSTOMER_UPLOAD_FLAG = aVMS_OUTPUT_CUSTOMERSchema.getVIC_CUSTOMER_UPLOAD_FLAG();
    }

    /**
    * 使用 ResultSet 中的第 i 行给 Schema 赋值
    * @param: rs ResultSet
    * @param: i int
    * @return: boolean
    **/
    public boolean setSchema(ResultSet rs,int i) {
        try {
            //rs.absolute(i);        // 非滚动游标
            if( rs.getString("VIC_INDATE") == null )
                this.VIC_INDATE = null;
            else
                this.VIC_INDATE = rs.getString("VIC_INDATE").trim();

            if( rs.getString("VIC_CUSTOMER_ID") == null )
                this.VIC_CUSTOMER_ID = null;
            else
                this.VIC_CUSTOMER_ID = rs.getString("VIC_CUSTOMER_ID").trim();

            if( rs.getString("VIC_CUSTOMER_CNAME") == null )
                this.VIC_CUSTOMER_CNAME = null;
            else
                this.VIC_CUSTOMER_CNAME = rs.getString("VIC_CUSTOMER_CNAME").trim();

            if( rs.getString("VIC_CUSTOMER_TAXNO") == null )
                this.VIC_CUSTOMER_TAXNO = null;
            else
                this.VIC_CUSTOMER_TAXNO = rs.getString("VIC_CUSTOMER_TAXNO").trim();

            if( rs.getString("VIC_CUSTOMER_ACCOUNT") == null )
                this.VIC_CUSTOMER_ACCOUNT = null;
            else
                this.VIC_CUSTOMER_ACCOUNT = rs.getString("VIC_CUSTOMER_ACCOUNT").trim();

            if( rs.getString("VIC_CUSTOMER_CBANK") == null )
                this.VIC_CUSTOMER_CBANK = null;
            else
                this.VIC_CUSTOMER_CBANK = rs.getString("VIC_CUSTOMER_CBANK").trim();

            if( rs.getString("VIC_CUSTOMER_PHONE") == null )
                this.VIC_CUSTOMER_PHONE = null;
            else
                this.VIC_CUSTOMER_PHONE = rs.getString("VIC_CUSTOMER_PHONE").trim();

            if( rs.getString("VIC_CUSTOMER_EMAIL") == null )
                this.VIC_CUSTOMER_EMAIL = null;
            else
                this.VIC_CUSTOMER_EMAIL = rs.getString("VIC_CUSTOMER_EMAIL").trim();

            if( rs.getString("VIC_CUSTOMER_ADDRESS") == null )
                this.VIC_CUSTOMER_ADDRESS = null;
            else
                this.VIC_CUSTOMER_ADDRESS = rs.getString("VIC_CUSTOMER_ADDRESS").trim();

            if( rs.getString("VIC_TAXPAYER_TYPE") == null )
                this.VIC_TAXPAYER_TYPE = null;
            else
                this.VIC_TAXPAYER_TYPE = rs.getString("VIC_TAXPAYER_TYPE").trim();

            if( rs.getString("VIC_FAPIAO_TYPE") == null )
                this.VIC_FAPIAO_TYPE = null;
            else
                this.VIC_FAPIAO_TYPE = rs.getString("VIC_FAPIAO_TYPE").trim();

            if( rs.getString("VIC_CUSTOMER_TYPE") == null )
                this.VIC_CUSTOMER_TYPE = null;
            else
                this.VIC_CUSTOMER_TYPE = rs.getString("VIC_CUSTOMER_TYPE").trim();

            if( rs.getString("VIC_CUSTOMER_FAPIAO_FLAG") == null )
                this.VIC_CUSTOMER_FAPIAO_FLAG = null;
            else
                this.VIC_CUSTOMER_FAPIAO_FLAG = rs.getString("VIC_CUSTOMER_FAPIAO_FLAG").trim();

            if( rs.getString("VIC_CUSTOMER_NATIONALITY") == null )
                this.VIC_CUSTOMER_NATIONALITY = null;
            else
                this.VIC_CUSTOMER_NATIONALITY = rs.getString("VIC_CUSTOMER_NATIONALITY").trim();

            if( rs.getString("VIC_DATA_SOURCE") == null )
                this.VIC_DATA_SOURCE = null;
            else
                this.VIC_DATA_SOURCE = rs.getString("VIC_DATA_SOURCE").trim();

            if( rs.getString("VIC_LINK_NAME") == null )
                this.VIC_LINK_NAME = null;
            else
                this.VIC_LINK_NAME = rs.getString("VIC_LINK_NAME").trim();

            if( rs.getString("VIC_LINK_PHONE") == null )
                this.VIC_LINK_PHONE = null;
            else
                this.VIC_LINK_PHONE = rs.getString("VIC_LINK_PHONE").trim();

            if( rs.getString("VIC_LINK_ADDRESS") == null )
                this.VIC_LINK_ADDRESS = null;
            else
                this.VIC_LINK_ADDRESS = rs.getString("VIC_LINK_ADDRESS").trim();

            if( rs.getString("VIC_CUSTOMER_ZIP_CODE") == null )
                this.VIC_CUSTOMER_ZIP_CODE = null;
            else
                this.VIC_CUSTOMER_ZIP_CODE = rs.getString("VIC_CUSTOMER_ZIP_CODE").trim();

            if( rs.getString("VIC_CUSTOMER_UPLOAD_FLAG") == null )
                this.VIC_CUSTOMER_UPLOAD_FLAG = null;
            else
                this.VIC_CUSTOMER_UPLOAD_FLAG = rs.getString("VIC_CUSTOMER_UPLOAD_FLAG").trim();

        }
        catch(SQLException sqle) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "VMS_OUTPUT_CUSTOMERSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = ExceptionUtils.exceptionToString(sqle);
            this.mErrors .addOneError(tError);
            return false;
        }
        return true;
    }

    public VMS_OUTPUT_CUSTOMERSchema getSchema() {
        VMS_OUTPUT_CUSTOMERSchema aVMS_OUTPUT_CUSTOMERSchema = new VMS_OUTPUT_CUSTOMERSchema();
        aVMS_OUTPUT_CUSTOMERSchema.setSchema(this);
        return aVMS_OUTPUT_CUSTOMERSchema;
    }

    public VMS_OUTPUT_CUSTOMERDB getDB() {
        VMS_OUTPUT_CUSTOMERDB aDBOper = new VMS_OUTPUT_CUSTOMERDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
    * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpVMS_OUTPUT_CUSTOMER描述/A>表字段
    * @return: String 返回打包后字符串
    **/
    public String encode() {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(VIC_INDATE)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(VIC_CUSTOMER_ID)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(VIC_CUSTOMER_CNAME)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(VIC_CUSTOMER_TAXNO)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(VIC_CUSTOMER_ACCOUNT)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(VIC_CUSTOMER_CBANK)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(VIC_CUSTOMER_PHONE)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(VIC_CUSTOMER_EMAIL)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(VIC_CUSTOMER_ADDRESS)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(VIC_TAXPAYER_TYPE)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(VIC_FAPIAO_TYPE)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(VIC_CUSTOMER_TYPE)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(VIC_CUSTOMER_FAPIAO_FLAG)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(VIC_CUSTOMER_NATIONALITY)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(VIC_DATA_SOURCE)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(VIC_LINK_NAME)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(VIC_LINK_PHONE)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(VIC_LINK_ADDRESS)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(VIC_CUSTOMER_ZIP_CODE)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(VIC_CUSTOMER_UPLOAD_FLAG));
        return strReturn.toString();
    }

    /**
    * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpVMS_OUTPUT_CUSTOMER>历史记账凭证主表信息</A>表字段
    * @param: strMessage String 包含一条纪录数据的字符串
    * @return: boolean
    **/
    public boolean decode(String strMessage) {
        try {
            VIC_INDATE = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
            VIC_CUSTOMER_ID = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
            VIC_CUSTOMER_CNAME = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
            VIC_CUSTOMER_TAXNO = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
            VIC_CUSTOMER_ACCOUNT = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
            VIC_CUSTOMER_CBANK = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
            VIC_CUSTOMER_PHONE = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
            VIC_CUSTOMER_EMAIL = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
            VIC_CUSTOMER_ADDRESS = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
            VIC_TAXPAYER_TYPE = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
            VIC_FAPIAO_TYPE = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
            VIC_CUSTOMER_TYPE = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
            VIC_CUSTOMER_FAPIAO_FLAG = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
            VIC_CUSTOMER_NATIONALITY = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER );
            VIC_DATA_SOURCE = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15, SysConst.PACKAGESPILTER );
            VIC_LINK_NAME = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16, SysConst.PACKAGESPILTER );
            VIC_LINK_PHONE = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17, SysConst.PACKAGESPILTER );
            VIC_LINK_ADDRESS = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18, SysConst.PACKAGESPILTER );
            VIC_CUSTOMER_ZIP_CODE = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19, SysConst.PACKAGESPILTER );
            VIC_CUSTOMER_UPLOAD_FLAG = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20, SysConst.PACKAGESPILTER );
        }
        catch(NumberFormatException ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "VMS_OUTPUT_CUSTOMERSchema";
            tError.functionName = "decode";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            return false;
        }
        return true;
    }

    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("VIC_INDATE")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(VIC_INDATE));
        }
        if (FCode.equalsIgnoreCase("VIC_CUSTOMER_ID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(VIC_CUSTOMER_ID));
        }
        if (FCode.equalsIgnoreCase("VIC_CUSTOMER_CNAME")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(VIC_CUSTOMER_CNAME));
        }
        if (FCode.equalsIgnoreCase("VIC_CUSTOMER_TAXNO")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(VIC_CUSTOMER_TAXNO));
        }
        if (FCode.equalsIgnoreCase("VIC_CUSTOMER_ACCOUNT")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(VIC_CUSTOMER_ACCOUNT));
        }
        if (FCode.equalsIgnoreCase("VIC_CUSTOMER_CBANK")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(VIC_CUSTOMER_CBANK));
        }
        if (FCode.equalsIgnoreCase("VIC_CUSTOMER_PHONE")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(VIC_CUSTOMER_PHONE));
        }
        if (FCode.equalsIgnoreCase("VIC_CUSTOMER_EMAIL")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(VIC_CUSTOMER_EMAIL));
        }
        if (FCode.equalsIgnoreCase("VIC_CUSTOMER_ADDRESS")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(VIC_CUSTOMER_ADDRESS));
        }
        if (FCode.equalsIgnoreCase("VIC_TAXPAYER_TYPE")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(VIC_TAXPAYER_TYPE));
        }
        if (FCode.equalsIgnoreCase("VIC_FAPIAO_TYPE")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(VIC_FAPIAO_TYPE));
        }
        if (FCode.equalsIgnoreCase("VIC_CUSTOMER_TYPE")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(VIC_CUSTOMER_TYPE));
        }
        if (FCode.equalsIgnoreCase("VIC_CUSTOMER_FAPIAO_FLAG")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(VIC_CUSTOMER_FAPIAO_FLAG));
        }
        if (FCode.equalsIgnoreCase("VIC_CUSTOMER_NATIONALITY")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(VIC_CUSTOMER_NATIONALITY));
        }
        if (FCode.equalsIgnoreCase("VIC_DATA_SOURCE")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(VIC_DATA_SOURCE));
        }
        if (FCode.equalsIgnoreCase("VIC_LINK_NAME")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(VIC_LINK_NAME));
        }
        if (FCode.equalsIgnoreCase("VIC_LINK_PHONE")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(VIC_LINK_PHONE));
        }
        if (FCode.equalsIgnoreCase("VIC_LINK_ADDRESS")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(VIC_LINK_ADDRESS));
        }
        if (FCode.equalsIgnoreCase("VIC_CUSTOMER_ZIP_CODE")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(VIC_CUSTOMER_ZIP_CODE));
        }
        if (FCode.equalsIgnoreCase("VIC_CUSTOMER_UPLOAD_FLAG")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(VIC_CUSTOMER_UPLOAD_FLAG));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(VIC_INDATE);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(VIC_CUSTOMER_ID);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(VIC_CUSTOMER_CNAME);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(VIC_CUSTOMER_TAXNO);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(VIC_CUSTOMER_ACCOUNT);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(VIC_CUSTOMER_CBANK);
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(VIC_CUSTOMER_PHONE);
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(VIC_CUSTOMER_EMAIL);
                break;
            case 8:
                strFieldValue = StrTool.GBKToUnicode(VIC_CUSTOMER_ADDRESS);
                break;
            case 9:
                strFieldValue = StrTool.GBKToUnicode(VIC_TAXPAYER_TYPE);
                break;
            case 10:
                strFieldValue = StrTool.GBKToUnicode(VIC_FAPIAO_TYPE);
                break;
            case 11:
                strFieldValue = StrTool.GBKToUnicode(VIC_CUSTOMER_TYPE);
                break;
            case 12:
                strFieldValue = StrTool.GBKToUnicode(VIC_CUSTOMER_FAPIAO_FLAG);
                break;
            case 13:
                strFieldValue = StrTool.GBKToUnicode(VIC_CUSTOMER_NATIONALITY);
                break;
            case 14:
                strFieldValue = StrTool.GBKToUnicode(VIC_DATA_SOURCE);
                break;
            case 15:
                strFieldValue = StrTool.GBKToUnicode(VIC_LINK_NAME);
                break;
            case 16:
                strFieldValue = StrTool.GBKToUnicode(VIC_LINK_PHONE);
                break;
            case 17:
                strFieldValue = StrTool.GBKToUnicode(VIC_LINK_ADDRESS);
                break;
            case 18:
                strFieldValue = StrTool.GBKToUnicode(VIC_CUSTOMER_ZIP_CODE);
                break;
            case 19:
                strFieldValue = StrTool.GBKToUnicode(VIC_CUSTOMER_UPLOAD_FLAG);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("VIC_INDATE")) {
            if( FValue != null && !FValue.equals(""))
            {
                VIC_INDATE = FValue.trim();
            }
            else
                VIC_INDATE = null;
        }
        if (FCode.equalsIgnoreCase("VIC_CUSTOMER_ID")) {
            if( FValue != null && !FValue.equals(""))
            {
                VIC_CUSTOMER_ID = FValue.trim();
            }
            else
                VIC_CUSTOMER_ID = null;
        }
        if (FCode.equalsIgnoreCase("VIC_CUSTOMER_CNAME")) {
            if( FValue != null && !FValue.equals(""))
            {
                VIC_CUSTOMER_CNAME = FValue.trim();
            }
            else
                VIC_CUSTOMER_CNAME = null;
        }
        if (FCode.equalsIgnoreCase("VIC_CUSTOMER_TAXNO")) {
            if( FValue != null && !FValue.equals(""))
            {
                VIC_CUSTOMER_TAXNO = FValue.trim();
            }
            else
                VIC_CUSTOMER_TAXNO = null;
        }
        if (FCode.equalsIgnoreCase("VIC_CUSTOMER_ACCOUNT")) {
            if( FValue != null && !FValue.equals(""))
            {
                VIC_CUSTOMER_ACCOUNT = FValue.trim();
            }
            else
                VIC_CUSTOMER_ACCOUNT = null;
        }
        if (FCode.equalsIgnoreCase("VIC_CUSTOMER_CBANK")) {
            if( FValue != null && !FValue.equals(""))
            {
                VIC_CUSTOMER_CBANK = FValue.trim();
            }
            else
                VIC_CUSTOMER_CBANK = null;
        }
        if (FCode.equalsIgnoreCase("VIC_CUSTOMER_PHONE")) {
            if( FValue != null && !FValue.equals(""))
            {
                VIC_CUSTOMER_PHONE = FValue.trim();
            }
            else
                VIC_CUSTOMER_PHONE = null;
        }
        if (FCode.equalsIgnoreCase("VIC_CUSTOMER_EMAIL")) {
            if( FValue != null && !FValue.equals(""))
            {
                VIC_CUSTOMER_EMAIL = FValue.trim();
            }
            else
                VIC_CUSTOMER_EMAIL = null;
        }
        if (FCode.equalsIgnoreCase("VIC_CUSTOMER_ADDRESS")) {
            if( FValue != null && !FValue.equals(""))
            {
                VIC_CUSTOMER_ADDRESS = FValue.trim();
            }
            else
                VIC_CUSTOMER_ADDRESS = null;
        }
        if (FCode.equalsIgnoreCase("VIC_TAXPAYER_TYPE")) {
            if( FValue != null && !FValue.equals(""))
            {
                VIC_TAXPAYER_TYPE = FValue.trim();
            }
            else
                VIC_TAXPAYER_TYPE = null;
        }
        if (FCode.equalsIgnoreCase("VIC_FAPIAO_TYPE")) {
            if( FValue != null && !FValue.equals(""))
            {
                VIC_FAPIAO_TYPE = FValue.trim();
            }
            else
                VIC_FAPIAO_TYPE = null;
        }
        if (FCode.equalsIgnoreCase("VIC_CUSTOMER_TYPE")) {
            if( FValue != null && !FValue.equals(""))
            {
                VIC_CUSTOMER_TYPE = FValue.trim();
            }
            else
                VIC_CUSTOMER_TYPE = null;
        }
        if (FCode.equalsIgnoreCase("VIC_CUSTOMER_FAPIAO_FLAG")) {
            if( FValue != null && !FValue.equals(""))
            {
                VIC_CUSTOMER_FAPIAO_FLAG = FValue.trim();
            }
            else
                VIC_CUSTOMER_FAPIAO_FLAG = null;
        }
        if (FCode.equalsIgnoreCase("VIC_CUSTOMER_NATIONALITY")) {
            if( FValue != null && !FValue.equals(""))
            {
                VIC_CUSTOMER_NATIONALITY = FValue.trim();
            }
            else
                VIC_CUSTOMER_NATIONALITY = null;
        }
        if (FCode.equalsIgnoreCase("VIC_DATA_SOURCE")) {
            if( FValue != null && !FValue.equals(""))
            {
                VIC_DATA_SOURCE = FValue.trim();
            }
            else
                VIC_DATA_SOURCE = null;
        }
        if (FCode.equalsIgnoreCase("VIC_LINK_NAME")) {
            if( FValue != null && !FValue.equals(""))
            {
                VIC_LINK_NAME = FValue.trim();
            }
            else
                VIC_LINK_NAME = null;
        }
        if (FCode.equalsIgnoreCase("VIC_LINK_PHONE")) {
            if( FValue != null && !FValue.equals(""))
            {
                VIC_LINK_PHONE = FValue.trim();
            }
            else
                VIC_LINK_PHONE = null;
        }
        if (FCode.equalsIgnoreCase("VIC_LINK_ADDRESS")) {
            if( FValue != null && !FValue.equals(""))
            {
                VIC_LINK_ADDRESS = FValue.trim();
            }
            else
                VIC_LINK_ADDRESS = null;
        }
        if (FCode.equalsIgnoreCase("VIC_CUSTOMER_ZIP_CODE")) {
            if( FValue != null && !FValue.equals(""))
            {
                VIC_CUSTOMER_ZIP_CODE = FValue.trim();
            }
            else
                VIC_CUSTOMER_ZIP_CODE = null;
        }
        if (FCode.equalsIgnoreCase("VIC_CUSTOMER_UPLOAD_FLAG")) {
            if( FValue != null && !FValue.equals(""))
            {
                VIC_CUSTOMER_UPLOAD_FLAG = FValue.trim();
            }
            else
                VIC_CUSTOMER_UPLOAD_FLAG = null;
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject) return true;
        if (otherObject == null) return false;
        if (getClass() != otherObject.getClass()) return false;
        VMS_OUTPUT_CUSTOMERSchema other = (VMS_OUTPUT_CUSTOMERSchema)otherObject;
        return
            VIC_INDATE.equals(other.getVIC_INDATE())
            && VIC_CUSTOMER_ID.equals(other.getVIC_CUSTOMER_ID())
            && VIC_CUSTOMER_CNAME.equals(other.getVIC_CUSTOMER_CNAME())
            && VIC_CUSTOMER_TAXNO.equals(other.getVIC_CUSTOMER_TAXNO())
            && VIC_CUSTOMER_ACCOUNT.equals(other.getVIC_CUSTOMER_ACCOUNT())
            && VIC_CUSTOMER_CBANK.equals(other.getVIC_CUSTOMER_CBANK())
            && VIC_CUSTOMER_PHONE.equals(other.getVIC_CUSTOMER_PHONE())
            && VIC_CUSTOMER_EMAIL.equals(other.getVIC_CUSTOMER_EMAIL())
            && VIC_CUSTOMER_ADDRESS.equals(other.getVIC_CUSTOMER_ADDRESS())
            && VIC_TAXPAYER_TYPE.equals(other.getVIC_TAXPAYER_TYPE())
            && VIC_FAPIAO_TYPE.equals(other.getVIC_FAPIAO_TYPE())
            && VIC_CUSTOMER_TYPE.equals(other.getVIC_CUSTOMER_TYPE())
            && VIC_CUSTOMER_FAPIAO_FLAG.equals(other.getVIC_CUSTOMER_FAPIAO_FLAG())
            && VIC_CUSTOMER_NATIONALITY.equals(other.getVIC_CUSTOMER_NATIONALITY())
            && VIC_DATA_SOURCE.equals(other.getVIC_DATA_SOURCE())
            && VIC_LINK_NAME.equals(other.getVIC_LINK_NAME())
            && VIC_LINK_PHONE.equals(other.getVIC_LINK_PHONE())
            && VIC_LINK_ADDRESS.equals(other.getVIC_LINK_ADDRESS())
            && VIC_CUSTOMER_ZIP_CODE.equals(other.getVIC_CUSTOMER_ZIP_CODE())
            && VIC_CUSTOMER_UPLOAD_FLAG.equals(other.getVIC_CUSTOMER_UPLOAD_FLAG());
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("VIC_INDATE") ) {
            return 0;
        }
        if( strFieldName.equals("VIC_CUSTOMER_ID") ) {
            return 1;
        }
        if( strFieldName.equals("VIC_CUSTOMER_CNAME") ) {
            return 2;
        }
        if( strFieldName.equals("VIC_CUSTOMER_TAXNO") ) {
            return 3;
        }
        if( strFieldName.equals("VIC_CUSTOMER_ACCOUNT") ) {
            return 4;
        }
        if( strFieldName.equals("VIC_CUSTOMER_CBANK") ) {
            return 5;
        }
        if( strFieldName.equals("VIC_CUSTOMER_PHONE") ) {
            return 6;
        }
        if( strFieldName.equals("VIC_CUSTOMER_EMAIL") ) {
            return 7;
        }
        if( strFieldName.equals("VIC_CUSTOMER_ADDRESS") ) {
            return 8;
        }
        if( strFieldName.equals("VIC_TAXPAYER_TYPE") ) {
            return 9;
        }
        if( strFieldName.equals("VIC_FAPIAO_TYPE") ) {
            return 10;
        }
        if( strFieldName.equals("VIC_CUSTOMER_TYPE") ) {
            return 11;
        }
        if( strFieldName.equals("VIC_CUSTOMER_FAPIAO_FLAG") ) {
            return 12;
        }
        if( strFieldName.equals("VIC_CUSTOMER_NATIONALITY") ) {
            return 13;
        }
        if( strFieldName.equals("VIC_DATA_SOURCE") ) {
            return 14;
        }
        if( strFieldName.equals("VIC_LINK_NAME") ) {
            return 15;
        }
        if( strFieldName.equals("VIC_LINK_PHONE") ) {
            return 16;
        }
        if( strFieldName.equals("VIC_LINK_ADDRESS") ) {
            return 17;
        }
        if( strFieldName.equals("VIC_CUSTOMER_ZIP_CODE") ) {
            return 18;
        }
        if( strFieldName.equals("VIC_CUSTOMER_UPLOAD_FLAG") ) {
            return 19;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "VIC_INDATE";
                break;
            case 1:
                strFieldName = "VIC_CUSTOMER_ID";
                break;
            case 2:
                strFieldName = "VIC_CUSTOMER_CNAME";
                break;
            case 3:
                strFieldName = "VIC_CUSTOMER_TAXNO";
                break;
            case 4:
                strFieldName = "VIC_CUSTOMER_ACCOUNT";
                break;
            case 5:
                strFieldName = "VIC_CUSTOMER_CBANK";
                break;
            case 6:
                strFieldName = "VIC_CUSTOMER_PHONE";
                break;
            case 7:
                strFieldName = "VIC_CUSTOMER_EMAIL";
                break;
            case 8:
                strFieldName = "VIC_CUSTOMER_ADDRESS";
                break;
            case 9:
                strFieldName = "VIC_TAXPAYER_TYPE";
                break;
            case 10:
                strFieldName = "VIC_FAPIAO_TYPE";
                break;
            case 11:
                strFieldName = "VIC_CUSTOMER_TYPE";
                break;
            case 12:
                strFieldName = "VIC_CUSTOMER_FAPIAO_FLAG";
                break;
            case 13:
                strFieldName = "VIC_CUSTOMER_NATIONALITY";
                break;
            case 14:
                strFieldName = "VIC_DATA_SOURCE";
                break;
            case 15:
                strFieldName = "VIC_LINK_NAME";
                break;
            case 16:
                strFieldName = "VIC_LINK_PHONE";
                break;
            case 17:
                strFieldName = "VIC_LINK_ADDRESS";
                break;
            case 18:
                strFieldName = "VIC_CUSTOMER_ZIP_CODE";
                break;
            case 19:
                strFieldName = "VIC_CUSTOMER_UPLOAD_FLAG";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "VIC_INDATE":
                return Schema.TYPE_STRING;
            case "VIC_CUSTOMER_ID":
                return Schema.TYPE_STRING;
            case "VIC_CUSTOMER_CNAME":
                return Schema.TYPE_STRING;
            case "VIC_CUSTOMER_TAXNO":
                return Schema.TYPE_STRING;
            case "VIC_CUSTOMER_ACCOUNT":
                return Schema.TYPE_STRING;
            case "VIC_CUSTOMER_CBANK":
                return Schema.TYPE_STRING;
            case "VIC_CUSTOMER_PHONE":
                return Schema.TYPE_STRING;
            case "VIC_CUSTOMER_EMAIL":
                return Schema.TYPE_STRING;
            case "VIC_CUSTOMER_ADDRESS":
                return Schema.TYPE_STRING;
            case "VIC_TAXPAYER_TYPE":
                return Schema.TYPE_STRING;
            case "VIC_FAPIAO_TYPE":
                return Schema.TYPE_STRING;
            case "VIC_CUSTOMER_TYPE":
                return Schema.TYPE_STRING;
            case "VIC_CUSTOMER_FAPIAO_FLAG":
                return Schema.TYPE_STRING;
            case "VIC_CUSTOMER_NATIONALITY":
                return Schema.TYPE_STRING;
            case "VIC_DATA_SOURCE":
                return Schema.TYPE_STRING;
            case "VIC_LINK_NAME":
                return Schema.TYPE_STRING;
            case "VIC_LINK_PHONE":
                return Schema.TYPE_STRING;
            case "VIC_LINK_ADDRESS":
                return Schema.TYPE_STRING;
            case "VIC_CUSTOMER_ZIP_CODE":
                return Schema.TYPE_STRING;
            case "VIC_CUSTOMER_UPLOAD_FLAG":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_STRING;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_STRING;
            case 6:
                return Schema.TYPE_STRING;
            case 7:
                return Schema.TYPE_STRING;
            case 8:
                return Schema.TYPE_STRING;
            case 9:
                return Schema.TYPE_STRING;
            case 10:
                return Schema.TYPE_STRING;
            case 11:
                return Schema.TYPE_STRING;
            case 12:
                return Schema.TYPE_STRING;
            case 13:
                return Schema.TYPE_STRING;
            case 14:
                return Schema.TYPE_STRING;
            case 15:
                return Schema.TYPE_STRING;
            case 16:
                return Schema.TYPE_STRING;
            case 17:
                return Schema.TYPE_STRING;
            case 18:
                return Schema.TYPE_STRING;
            case 19:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
}
