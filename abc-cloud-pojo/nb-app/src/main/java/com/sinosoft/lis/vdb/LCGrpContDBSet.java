/**
 * Copyright (c) 2018 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.vdb;

import com.sinosoft.lis.vschema.LCGrpContSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.DBConnPool;
import com.sinosoft.utility.DBOper;
import com.sinosoft.utility.ExceptionUtils;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;

/**
 * <p>ClassName: LCGrpContDBSet </p>
 * <p>Description: DB层多记录数据库操作类文件 </p>
 * <p>Copyright: Copyright (c) 2018</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2018-05-23
 */
public class LCGrpContDBSet extends LCGrpContSet {
    // @Field
    private Connection con;
    private DBOper db;
    /**
    * flag = true: 传入Connection
    * flag = false: 不传入Connection
    **/
    private boolean mflag = false;

    // @Constructor
    public LCGrpContDBSet(Connection tConnection) {
        con = tConnection;
        db = new DBOper(con,"LCGrpCont");
        mflag = true;
    }

    public LCGrpContDBSet() {
        db = new DBOper( "LCGrpCont" );
    }
    // @Method
    public boolean deleteSQL() {
        if (db.deleteSQL(this)) {
            return true;
        } else {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LCGrpContDBSet";
            tError.functionName = "deleteSQL";
            tError.errorMessage = "操作失败!";
            this.mErrors .addOneError(tError);
            return false;
        }
    }

    /**
     * 删除操作
     * 删除条件：主键
     * @return boolean
     */
    public boolean delete() {
        PreparedStatement pstmt = null;

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try {
            int tCount = this.size();
            pstmt = con.prepareStatement("DELETE FROM LCGrpCont WHERE  1=1  AND GrpContNo = ?");
            for (int i = 1; i <= tCount; i++) {
            if(this.get(i).getGrpContNo() == null || this.get(i).getGrpContNo().equals("null")) {
            	pstmt.setString(1,null);
            } else {
            	pstmt.setString(1, this.get(i).getGrpContNo());
            }
                pstmt.addBatch();
            }
            pstmt.executeBatch();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            ex.printStackTrace();
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LCGrpContDBSet";
            tError.functionName = "delete()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            try {
                pstmt.close();
            } catch (Exception e){
                e.printStackTrace();
            }

            if (!mflag) {
                try {
                    con.close();
                } catch (Exception e){e.printStackTrace();}
            }

            return false;
        }

        if( !mflag ) {
            try {
                con.close();
            } catch (Exception e){e.printStackTrace();}
        }

        return true;
    }

    /**
     * 更新操作
     * 更新条件：主键
     * @return boolean
     */
    public boolean update() {
        PreparedStatement pstmt = null;

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try {
            int tCount = this.size();
            pstmt = con.prepareStatement("UPDATE LCGrpCont SET  GrpContNo = ? , ProposalGrpContNo = ? , PrtNo = ? , SaleChnl = ? , SalesWay = ? , SalesWaySub = ? , BizNature = ? , PolIssuPlat = ? , ContType = ? , AgentCom = ? , AgentCode = ? , AgentGroup = ? , AgentCode1 = ? , AppntNo = ? , Peoples2 = ? , GrpName = ? , DisputedFlag = ? , OutPayFlag = ? , Lang = ? , Currency = ? , LostTimes = ? , PrintCount = ? , LastEdorDate = ? , SpecFlag = ? , SignCom = ? , SignDate = ? , SignTime = ? , CValiDate = ? , PayIntv = ? , ManageFeeRate = ? , ExpPeoples = ? , ExpPremium = ? , ExpAmnt = ? , Peoples = ? , Mult = ? , Prem = ? , Amnt = ? , SumPrem = ? , SumPay = ? , Dif = ? , StandbyFlag1 = ? , StandbyFlag2 = ? , StandbyFlag3 = ? , InputOperator = ? , InputDate = ? , InputTime = ? , ApproveFlag = ? , ApproveCode = ? , ApproveDate = ? , ApproveTime = ? , UWOperator = ? , UWFlag = ? , UWDate = ? , UWTime = ? , AppFlag = ? , State = ? , PolApplyDate = ? , CustomGetPolDate = ? , GetPolDate = ? , ManageCom = ? , ComCode = ? , Operator = ? , MakeDate = ? , MakeTime = ? , ModifyOperator = ? , ModifyDate = ? , ModifyTime = ? , EnterKind = ? , AmntGrade = ? , FirstTrialOperator = ? , FirstTrialDate = ? , FirstTrialTime = ? , ReceiveOperator = ? , ReceiveDate = ? , ReceiveTime = ? , MarketType = ? , ReportNo = ? , EdorCalType = ? , RenewFlag = ? , RenewContNo = ? , EndDate = ? , Coinsurance = ? , CardTypeCode = ? , RelationToappnt = ? , NewReinsureFlag = ? , InsureRate = ? , AgcAgentCode = ? , AgcAgentName = ? , AvailiaDateFlag = ? , ComBusType = ? , PrintFlag = ? , SpecAcceptFlag = ? , PolicyType = ? , AchvAccruFlag = ? , Remark = ? , ValDateType = ? , SecLevelOther = ? , TBType = ? , SlipForm = ? WHERE  1=1  AND GrpContNo = ?");
            for (int i = 1; i <= tCount; i++) {
            if(this.get(i).getGrpContNo() == null || this.get(i).getGrpContNo().equals("null")) {
            	pstmt.setString(1,null);
            } else {
            	pstmt.setString(1, this.get(i).getGrpContNo());
            }
            if(this.get(i).getProposalGrpContNo() == null || this.get(i).getProposalGrpContNo().equals("null")) {
            	pstmt.setString(2,null);
            } else {
            	pstmt.setString(2, this.get(i).getProposalGrpContNo());
            }
            if(this.get(i).getPrtNo() == null || this.get(i).getPrtNo().equals("null")) {
            	pstmt.setString(3,null);
            } else {
            	pstmt.setString(3, this.get(i).getPrtNo());
            }
            if(this.get(i).getSaleChnl() == null || this.get(i).getSaleChnl().equals("null")) {
            	pstmt.setString(4,null);
            } else {
            	pstmt.setString(4, this.get(i).getSaleChnl());
            }
            if(this.get(i).getSalesWay() == null || this.get(i).getSalesWay().equals("null")) {
            	pstmt.setString(5,null);
            } else {
            	pstmt.setString(5, this.get(i).getSalesWay());
            }
            if(this.get(i).getSalesWaySub() == null || this.get(i).getSalesWaySub().equals("null")) {
            	pstmt.setString(6,null);
            } else {
            	pstmt.setString(6, this.get(i).getSalesWaySub());
            }
            if(this.get(i).getBizNature() == null || this.get(i).getBizNature().equals("null")) {
            	pstmt.setString(7,null);
            } else {
            	pstmt.setString(7, this.get(i).getBizNature());
            }
            if(this.get(i).getPolIssuPlat() == null || this.get(i).getPolIssuPlat().equals("null")) {
            	pstmt.setString(8,null);
            } else {
            	pstmt.setString(8, this.get(i).getPolIssuPlat());
            }
            if(this.get(i).getContType() == null || this.get(i).getContType().equals("null")) {
            	pstmt.setString(9,null);
            } else {
            	pstmt.setString(9, this.get(i).getContType());
            }
            if(this.get(i).getAgentCom() == null || this.get(i).getAgentCom().equals("null")) {
            	pstmt.setString(10,null);
            } else {
            	pstmt.setString(10, this.get(i).getAgentCom());
            }
            if(this.get(i).getAgentCode() == null || this.get(i).getAgentCode().equals("null")) {
            	pstmt.setString(11,null);
            } else {
            	pstmt.setString(11, this.get(i).getAgentCode());
            }
            if(this.get(i).getAgentGroup() == null || this.get(i).getAgentGroup().equals("null")) {
            	pstmt.setString(12,null);
            } else {
            	pstmt.setString(12, this.get(i).getAgentGroup());
            }
            if(this.get(i).getAgentCode1() == null || this.get(i).getAgentCode1().equals("null")) {
            	pstmt.setString(13,null);
            } else {
            	pstmt.setString(13, this.get(i).getAgentCode1());
            }
            if(this.get(i).getAppntNo() == null || this.get(i).getAppntNo().equals("null")) {
            	pstmt.setString(14,null);
            } else {
            	pstmt.setString(14, this.get(i).getAppntNo());
            }
            pstmt.setInt(15, this.get(i).getPeoples2());
            if(this.get(i).getGrpName() == null || this.get(i).getGrpName().equals("null")) {
            	pstmt.setString(16,null);
            } else {
            	pstmt.setString(16, this.get(i).getGrpName());
            }
            if(this.get(i).getDisputedFlag() == null || this.get(i).getDisputedFlag().equals("null")) {
            	pstmt.setString(17,null);
            } else {
            	pstmt.setString(17, this.get(i).getDisputedFlag());
            }
            if(this.get(i).getOutPayFlag() == null || this.get(i).getOutPayFlag().equals("null")) {
            	pstmt.setString(18,null);
            } else {
            	pstmt.setString(18, this.get(i).getOutPayFlag());
            }
            if(this.get(i).getLang() == null || this.get(i).getLang().equals("null")) {
            	pstmt.setString(19,null);
            } else {
            	pstmt.setString(19, this.get(i).getLang());
            }
            if(this.get(i).getCurrency() == null || this.get(i).getCurrency().equals("null")) {
            	pstmt.setString(20,null);
            } else {
            	pstmt.setString(20, this.get(i).getCurrency());
            }
            pstmt.setInt(21, this.get(i).getLostTimes());
            pstmt.setInt(22, this.get(i).getPrintCount());
            if(this.get(i).getLastEdorDate() == null || this.get(i).getLastEdorDate().equals("null")) {
                pstmt.setDate(23,null);
            } else {
                pstmt.setDate(23, Date.valueOf(this.get(i).getLastEdorDate()));
            }
            if(this.get(i).getSpecFlag() == null || this.get(i).getSpecFlag().equals("null")) {
            	pstmt.setString(24,null);
            } else {
            	pstmt.setString(24, this.get(i).getSpecFlag());
            }
            if(this.get(i).getSignCom() == null || this.get(i).getSignCom().equals("null")) {
            	pstmt.setString(25,null);
            } else {
            	pstmt.setString(25, this.get(i).getSignCom());
            }
            if(this.get(i).getSignDate() == null || this.get(i).getSignDate().equals("null")) {
                pstmt.setDate(26,null);
            } else {
                pstmt.setDate(26, Date.valueOf(this.get(i).getSignDate()));
            }
            if(this.get(i).getSignTime() == null || this.get(i).getSignTime().equals("null")) {
            	pstmt.setString(27,null);
            } else {
            	pstmt.setString(27, this.get(i).getSignTime());
            }
            if(this.get(i).getCValiDate() == null || this.get(i).getCValiDate().equals("null")) {
                pstmt.setDate(28,null);
            } else {
                pstmt.setDate(28, Date.valueOf(this.get(i).getCValiDate()));
            }
            pstmt.setInt(29, this.get(i).getPayIntv());
            pstmt.setDouble(30, this.get(i).getManageFeeRate());
            pstmt.setInt(31, this.get(i).getExpPeoples());
            pstmt.setDouble(32, this.get(i).getExpPremium());
            pstmt.setDouble(33, this.get(i).getExpAmnt());
            pstmt.setInt(34, this.get(i).getPeoples());
            pstmt.setDouble(35, this.get(i).getMult());
            pstmt.setDouble(36, this.get(i).getPrem());
            pstmt.setDouble(37, this.get(i).getAmnt());
            pstmt.setDouble(38, this.get(i).getSumPrem());
            pstmt.setDouble(39, this.get(i).getSumPay());
            pstmt.setDouble(40, this.get(i).getDif());
            if(this.get(i).getStandbyFlag1() == null || this.get(i).getStandbyFlag1().equals("null")) {
            	pstmt.setString(41,null);
            } else {
            	pstmt.setString(41, this.get(i).getStandbyFlag1());
            }
            if(this.get(i).getStandbyFlag2() == null || this.get(i).getStandbyFlag2().equals("null")) {
            	pstmt.setString(42,null);
            } else {
            	pstmt.setString(42, this.get(i).getStandbyFlag2());
            }
            if(this.get(i).getStandbyFlag3() == null || this.get(i).getStandbyFlag3().equals("null")) {
            	pstmt.setString(43,null);
            } else {
            	pstmt.setString(43, this.get(i).getStandbyFlag3());
            }
            if(this.get(i).getInputOperator() == null || this.get(i).getInputOperator().equals("null")) {
            	pstmt.setString(44,null);
            } else {
            	pstmt.setString(44, this.get(i).getInputOperator());
            }
            if(this.get(i).getInputDate() == null || this.get(i).getInputDate().equals("null")) {
                pstmt.setDate(45,null);
            } else {
                pstmt.setDate(45, Date.valueOf(this.get(i).getInputDate()));
            }
            if(this.get(i).getInputTime() == null || this.get(i).getInputTime().equals("null")) {
            	pstmt.setString(46,null);
            } else {
            	pstmt.setString(46, this.get(i).getInputTime());
            }
            if(this.get(i).getApproveFlag() == null || this.get(i).getApproveFlag().equals("null")) {
            	pstmt.setString(47,null);
            } else {
            	pstmt.setString(47, this.get(i).getApproveFlag());
            }
            if(this.get(i).getApproveCode() == null || this.get(i).getApproveCode().equals("null")) {
            	pstmt.setString(48,null);
            } else {
            	pstmt.setString(48, this.get(i).getApproveCode());
            }
            if(this.get(i).getApproveDate() == null || this.get(i).getApproveDate().equals("null")) {
                pstmt.setDate(49,null);
            } else {
                pstmt.setDate(49, Date.valueOf(this.get(i).getApproveDate()));
            }
            if(this.get(i).getApproveTime() == null || this.get(i).getApproveTime().equals("null")) {
            	pstmt.setString(50,null);
            } else {
            	pstmt.setString(50, this.get(i).getApproveTime());
            }
            if(this.get(i).getUWOperator() == null || this.get(i).getUWOperator().equals("null")) {
            	pstmt.setString(51,null);
            } else {
            	pstmt.setString(51, this.get(i).getUWOperator());
            }
            if(this.get(i).getUWFlag() == null || this.get(i).getUWFlag().equals("null")) {
            	pstmt.setString(52,null);
            } else {
            	pstmt.setString(52, this.get(i).getUWFlag());
            }
            if(this.get(i).getUWDate() == null || this.get(i).getUWDate().equals("null")) {
                pstmt.setDate(53,null);
            } else {
                pstmt.setDate(53, Date.valueOf(this.get(i).getUWDate()));
            }
            if(this.get(i).getUWTime() == null || this.get(i).getUWTime().equals("null")) {
            	pstmt.setString(54,null);
            } else {
            	pstmt.setString(54, this.get(i).getUWTime());
            }
            if(this.get(i).getAppFlag() == null || this.get(i).getAppFlag().equals("null")) {
            	pstmt.setString(55,null);
            } else {
            	pstmt.setString(55, this.get(i).getAppFlag());
            }
            if(this.get(i).getState() == null || this.get(i).getState().equals("null")) {
            	pstmt.setString(56,null);
            } else {
            	pstmt.setString(56, this.get(i).getState());
            }
            if(this.get(i).getPolApplyDate() == null || this.get(i).getPolApplyDate().equals("null")) {
                pstmt.setDate(57,null);
            } else {
                pstmt.setDate(57, Date.valueOf(this.get(i).getPolApplyDate()));
            }
            if(this.get(i).getCustomGetPolDate() == null || this.get(i).getCustomGetPolDate().equals("null")) {
                pstmt.setDate(58,null);
            } else {
                pstmt.setDate(58, Date.valueOf(this.get(i).getCustomGetPolDate()));
            }
            if(this.get(i).getGetPolDate() == null || this.get(i).getGetPolDate().equals("null")) {
                pstmt.setDate(59,null);
            } else {
                pstmt.setDate(59, Date.valueOf(this.get(i).getGetPolDate()));
            }
            if(this.get(i).getManageCom() == null || this.get(i).getManageCom().equals("null")) {
            	pstmt.setString(60,null);
            } else {
            	pstmt.setString(60, this.get(i).getManageCom());
            }
            if(this.get(i).getComCode() == null || this.get(i).getComCode().equals("null")) {
            	pstmt.setString(61,null);
            } else {
            	pstmt.setString(61, this.get(i).getComCode());
            }
            if(this.get(i).getOperator() == null || this.get(i).getOperator().equals("null")) {
            	pstmt.setString(62,null);
            } else {
            	pstmt.setString(62, this.get(i).getOperator());
            }
            if(this.get(i).getMakeDate() == null || this.get(i).getMakeDate().equals("null")) {
                pstmt.setDate(63,null);
            } else {
                pstmt.setDate(63, Date.valueOf(this.get(i).getMakeDate()));
            }
            if(this.get(i).getMakeTime() == null || this.get(i).getMakeTime().equals("null")) {
            	pstmt.setString(64,null);
            } else {
            	pstmt.setString(64, this.get(i).getMakeTime());
            }
            if(this.get(i).getModifyOperator() == null || this.get(i).getModifyOperator().equals("null")) {
            	pstmt.setString(65,null);
            } else {
            	pstmt.setString(65, this.get(i).getModifyOperator());
            }
            if(this.get(i).getModifyDate() == null || this.get(i).getModifyDate().equals("null")) {
                pstmt.setDate(66,null);
            } else {
                pstmt.setDate(66, Date.valueOf(this.get(i).getModifyDate()));
            }
            if(this.get(i).getModifyTime() == null || this.get(i).getModifyTime().equals("null")) {
            	pstmt.setString(67,null);
            } else {
            	pstmt.setString(67, this.get(i).getModifyTime());
            }
            if(this.get(i).getEnterKind() == null || this.get(i).getEnterKind().equals("null")) {
            	pstmt.setString(68,null);
            } else {
            	pstmt.setString(68, this.get(i).getEnterKind());
            }
            if(this.get(i).getAmntGrade() == null || this.get(i).getAmntGrade().equals("null")) {
            	pstmt.setString(69,null);
            } else {
            	pstmt.setString(69, this.get(i).getAmntGrade());
            }
            if(this.get(i).getFirstTrialOperator() == null || this.get(i).getFirstTrialOperator().equals("null")) {
            	pstmt.setString(70,null);
            } else {
            	pstmt.setString(70, this.get(i).getFirstTrialOperator());
            }
            if(this.get(i).getFirstTrialDate() == null || this.get(i).getFirstTrialDate().equals("null")) {
                pstmt.setDate(71,null);
            } else {
                pstmt.setDate(71, Date.valueOf(this.get(i).getFirstTrialDate()));
            }
            if(this.get(i).getFirstTrialTime() == null || this.get(i).getFirstTrialTime().equals("null")) {
            	pstmt.setString(72,null);
            } else {
            	pstmt.setString(72, this.get(i).getFirstTrialTime());
            }
            if(this.get(i).getReceiveOperator() == null || this.get(i).getReceiveOperator().equals("null")) {
            	pstmt.setString(73,null);
            } else {
            	pstmt.setString(73, this.get(i).getReceiveOperator());
            }
            if(this.get(i).getReceiveDate() == null || this.get(i).getReceiveDate().equals("null")) {
                pstmt.setDate(74,null);
            } else {
                pstmt.setDate(74, Date.valueOf(this.get(i).getReceiveDate()));
            }
            if(this.get(i).getReceiveTime() == null || this.get(i).getReceiveTime().equals("null")) {
            	pstmt.setString(75,null);
            } else {
            	pstmt.setString(75, this.get(i).getReceiveTime());
            }
            if(this.get(i).getMarketType() == null || this.get(i).getMarketType().equals("null")) {
            	pstmt.setString(76,null);
            } else {
            	pstmt.setString(76, this.get(i).getMarketType());
            }
            if(this.get(i).getReportNo() == null || this.get(i).getReportNo().equals("null")) {
            	pstmt.setString(77,null);
            } else {
            	pstmt.setString(77, this.get(i).getReportNo());
            }
            if(this.get(i).getEdorCalType() == null || this.get(i).getEdorCalType().equals("null")) {
            	pstmt.setString(78,null);
            } else {
            	pstmt.setString(78, this.get(i).getEdorCalType());
            }
            if(this.get(i).getRenewFlag() == null || this.get(i).getRenewFlag().equals("null")) {
            	pstmt.setString(79,null);
            } else {
            	pstmt.setString(79, this.get(i).getRenewFlag());
            }
            if(this.get(i).getRenewContNo() == null || this.get(i).getRenewContNo().equals("null")) {
            	pstmt.setString(80,null);
            } else {
            	pstmt.setString(80, this.get(i).getRenewContNo());
            }
            if(this.get(i).getEndDate() == null || this.get(i).getEndDate().equals("null")) {
                pstmt.setDate(81,null);
            } else {
                pstmt.setDate(81, Date.valueOf(this.get(i).getEndDate()));
            }
            if(this.get(i).getCoinsurance() == null || this.get(i).getCoinsurance().equals("null")) {
            	pstmt.setString(82,null);
            } else {
            	pstmt.setString(82, this.get(i).getCoinsurance());
            }
            if(this.get(i).getCardTypeCode() == null || this.get(i).getCardTypeCode().equals("null")) {
            	pstmt.setString(83,null);
            } else {
            	pstmt.setString(83, this.get(i).getCardTypeCode());
            }
            if(this.get(i).getRelationToappnt() == null || this.get(i).getRelationToappnt().equals("null")) {
            	pstmt.setString(84,null);
            } else {
            	pstmt.setString(84, this.get(i).getRelationToappnt());
            }
            if(this.get(i).getNewReinsureFlag() == null || this.get(i).getNewReinsureFlag().equals("null")) {
            	pstmt.setString(85,null);
            } else {
            	pstmt.setString(85, this.get(i).getNewReinsureFlag());
            }
            pstmt.setLong(86, this.get(i).getInsureRate());
            if(this.get(i).getAgcAgentCode() == null || this.get(i).getAgcAgentCode().equals("null")) {
            	pstmt.setString(87,null);
            } else {
            	pstmt.setString(87, this.get(i).getAgcAgentCode());
            }
            if(this.get(i).getAgcAgentName() == null || this.get(i).getAgcAgentName().equals("null")) {
            	pstmt.setString(88,null);
            } else {
            	pstmt.setString(88, this.get(i).getAgcAgentName());
            }
            if(this.get(i).getAvailiaDateFlag() == null || this.get(i).getAvailiaDateFlag().equals("null")) {
            	pstmt.setString(89,null);
            } else {
            	pstmt.setString(89, this.get(i).getAvailiaDateFlag());
            }
            if(this.get(i).getComBusType() == null || this.get(i).getComBusType().equals("null")) {
            	pstmt.setString(90,null);
            } else {
            	pstmt.setString(90, this.get(i).getComBusType());
            }
            if(this.get(i).getPrintFlag() == null || this.get(i).getPrintFlag().equals("null")) {
            	pstmt.setString(91,null);
            } else {
            	pstmt.setString(91, this.get(i).getPrintFlag());
            }
            if(this.get(i).getSpecAcceptFlag() == null || this.get(i).getSpecAcceptFlag().equals("null")) {
            	pstmt.setString(92,null);
            } else {
            	pstmt.setString(92, this.get(i).getSpecAcceptFlag());
            }
            if(this.get(i).getPolicyType() == null || this.get(i).getPolicyType().equals("null")) {
            	pstmt.setString(93,null);
            } else {
            	pstmt.setString(93, this.get(i).getPolicyType());
            }
            if(this.get(i).getAchvAccruFlag() == null || this.get(i).getAchvAccruFlag().equals("null")) {
            	pstmt.setString(94,null);
            } else {
            	pstmt.setString(94, this.get(i).getAchvAccruFlag());
            }
            if(this.get(i).getRemark() == null || this.get(i).getRemark().equals("null")) {
            	pstmt.setString(95,null);
            } else {
            	pstmt.setString(95, this.get(i).getRemark());
            }
            if(this.get(i).getValDateType() == null || this.get(i).getValDateType().equals("null")) {
            	pstmt.setString(96,null);
            } else {
            	pstmt.setString(96, this.get(i).getValDateType());
            }
            if(this.get(i).getSecLevelOther() == null || this.get(i).getSecLevelOther().equals("null")) {
            	pstmt.setString(97,null);
            } else {
            	pstmt.setString(97, this.get(i).getSecLevelOther());
            }
            if(this.get(i).getTBType() == null || this.get(i).getTBType().equals("null")) {
            	pstmt.setString(98,null);
            } else {
            	pstmt.setString(98, this.get(i).getTBType());
            }
            if(this.get(i).getSlipForm() == null || this.get(i).getSlipForm().equals("null")) {
            	pstmt.setString(99,null);
            } else {
            	pstmt.setString(99, this.get(i).getSlipForm());
            }
            // set where condition
            if(this.get(i).getGrpContNo() == null || this.get(i).getGrpContNo().equals("null")) {
            	pstmt.setString(100,null);
            } else {
            	pstmt.setString(100, this.get(i).getGrpContNo());
            }
                pstmt.addBatch();
            }
            pstmt.executeBatch();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            ex.printStackTrace();
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LCGrpContDBSet";
            tError.functionName = "update()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            try {
                pstmt.close();
            } catch (Exception e){e.printStackTrace();}

            if( !mflag ) {
                try {
                    con.close();
                } catch (Exception e){e.printStackTrace();}
            }

            return false;
        }

        if( !mflag ) {
            try {
                con.close();
            } catch (Exception e){e.printStackTrace();}
        }

        return true;
    }

    /**
     * 新增操作
     * @return boolean
     */
    public boolean insert() {
        PreparedStatement pstmt = null;

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try
        {
            int tCount = this.size();
            pstmt = con.prepareStatement("INSERT INTO LCGrpCont VALUES( ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ?)");
            for (int i = 1; i <= tCount; i++) {
            if(this.get(i).getGrpContNo() == null || this.get(i).getGrpContNo().equals("null")) {
            	pstmt.setString(1,null);
            } else {
            	pstmt.setString(1, this.get(i).getGrpContNo());
            }
            if(this.get(i).getProposalGrpContNo() == null || this.get(i).getProposalGrpContNo().equals("null")) {
            	pstmt.setString(2,null);
            } else {
            	pstmt.setString(2, this.get(i).getProposalGrpContNo());
            }
            if(this.get(i).getPrtNo() == null || this.get(i).getPrtNo().equals("null")) {
            	pstmt.setString(3,null);
            } else {
            	pstmt.setString(3, this.get(i).getPrtNo());
            }
            if(this.get(i).getSaleChnl() == null || this.get(i).getSaleChnl().equals("null")) {
            	pstmt.setString(4,null);
            } else {
            	pstmt.setString(4, this.get(i).getSaleChnl());
            }
            if(this.get(i).getSalesWay() == null || this.get(i).getSalesWay().equals("null")) {
            	pstmt.setString(5,null);
            } else {
            	pstmt.setString(5, this.get(i).getSalesWay());
            }
            if(this.get(i).getSalesWaySub() == null || this.get(i).getSalesWaySub().equals("null")) {
            	pstmt.setString(6,null);
            } else {
            	pstmt.setString(6, this.get(i).getSalesWaySub());
            }
            if(this.get(i).getBizNature() == null || this.get(i).getBizNature().equals("null")) {
            	pstmt.setString(7,null);
            } else {
            	pstmt.setString(7, this.get(i).getBizNature());
            }
            if(this.get(i).getPolIssuPlat() == null || this.get(i).getPolIssuPlat().equals("null")) {
            	pstmt.setString(8,null);
            } else {
            	pstmt.setString(8, this.get(i).getPolIssuPlat());
            }
            if(this.get(i).getContType() == null || this.get(i).getContType().equals("null")) {
            	pstmt.setString(9,null);
            } else {
            	pstmt.setString(9, this.get(i).getContType());
            }
            if(this.get(i).getAgentCom() == null || this.get(i).getAgentCom().equals("null")) {
            	pstmt.setString(10,null);
            } else {
            	pstmt.setString(10, this.get(i).getAgentCom());
            }
            if(this.get(i).getAgentCode() == null || this.get(i).getAgentCode().equals("null")) {
            	pstmt.setString(11,null);
            } else {
            	pstmt.setString(11, this.get(i).getAgentCode());
            }
            if(this.get(i).getAgentGroup() == null || this.get(i).getAgentGroup().equals("null")) {
            	pstmt.setString(12,null);
            } else {
            	pstmt.setString(12, this.get(i).getAgentGroup());
            }
            if(this.get(i).getAgentCode1() == null || this.get(i).getAgentCode1().equals("null")) {
            	pstmt.setString(13,null);
            } else {
            	pstmt.setString(13, this.get(i).getAgentCode1());
            }
            if(this.get(i).getAppntNo() == null || this.get(i).getAppntNo().equals("null")) {
            	pstmt.setString(14,null);
            } else {
            	pstmt.setString(14, this.get(i).getAppntNo());
            }
            pstmt.setInt(15, this.get(i).getPeoples2());
            if(this.get(i).getGrpName() == null || this.get(i).getGrpName().equals("null")) {
            	pstmt.setString(16,null);
            } else {
            	pstmt.setString(16, this.get(i).getGrpName());
            }
            if(this.get(i).getDisputedFlag() == null || this.get(i).getDisputedFlag().equals("null")) {
            	pstmt.setString(17,null);
            } else {
            	pstmt.setString(17, this.get(i).getDisputedFlag());
            }
            if(this.get(i).getOutPayFlag() == null || this.get(i).getOutPayFlag().equals("null")) {
            	pstmt.setString(18,null);
            } else {
            	pstmt.setString(18, this.get(i).getOutPayFlag());
            }
            if(this.get(i).getLang() == null || this.get(i).getLang().equals("null")) {
            	pstmt.setString(19,null);
            } else {
            	pstmt.setString(19, this.get(i).getLang());
            }
            if(this.get(i).getCurrency() == null || this.get(i).getCurrency().equals("null")) {
            	pstmt.setString(20,null);
            } else {
            	pstmt.setString(20, this.get(i).getCurrency());
            }
            pstmt.setInt(21, this.get(i).getLostTimes());
            pstmt.setInt(22, this.get(i).getPrintCount());
            if(this.get(i).getLastEdorDate() == null || this.get(i).getLastEdorDate().equals("null")) {
                pstmt.setDate(23,null);
            } else {
                pstmt.setDate(23, Date.valueOf(this.get(i).getLastEdorDate()));
            }
            if(this.get(i).getSpecFlag() == null || this.get(i).getSpecFlag().equals("null")) {
            	pstmt.setString(24,null);
            } else {
            	pstmt.setString(24, this.get(i).getSpecFlag());
            }
            if(this.get(i).getSignCom() == null || this.get(i).getSignCom().equals("null")) {
            	pstmt.setString(25,null);
            } else {
            	pstmt.setString(25, this.get(i).getSignCom());
            }
            if(this.get(i).getSignDate() == null || this.get(i).getSignDate().equals("null")) {
                pstmt.setDate(26,null);
            } else {
                pstmt.setDate(26, Date.valueOf(this.get(i).getSignDate()));
            }
            if(this.get(i).getSignTime() == null || this.get(i).getSignTime().equals("null")) {
            	pstmt.setString(27,null);
            } else {
            	pstmt.setString(27, this.get(i).getSignTime());
            }
            if(this.get(i).getCValiDate() == null || this.get(i).getCValiDate().equals("null")) {
                pstmt.setDate(28,null);
            } else {
                pstmt.setDate(28, Date.valueOf(this.get(i).getCValiDate()));
            }
            pstmt.setInt(29, this.get(i).getPayIntv());
            pstmt.setDouble(30, this.get(i).getManageFeeRate());
            pstmt.setInt(31, this.get(i).getExpPeoples());
            pstmt.setDouble(32, this.get(i).getExpPremium());
            pstmt.setDouble(33, this.get(i).getExpAmnt());
            pstmt.setInt(34, this.get(i).getPeoples());
            pstmt.setDouble(35, this.get(i).getMult());
            pstmt.setDouble(36, this.get(i).getPrem());
            pstmt.setDouble(37, this.get(i).getAmnt());
            pstmt.setDouble(38, this.get(i).getSumPrem());
            pstmt.setDouble(39, this.get(i).getSumPay());
            pstmt.setDouble(40, this.get(i).getDif());
            if(this.get(i).getStandbyFlag1() == null || this.get(i).getStandbyFlag1().equals("null")) {
            	pstmt.setString(41,null);
            } else {
            	pstmt.setString(41, this.get(i).getStandbyFlag1());
            }
            if(this.get(i).getStandbyFlag2() == null || this.get(i).getStandbyFlag2().equals("null")) {
            	pstmt.setString(42,null);
            } else {
            	pstmt.setString(42, this.get(i).getStandbyFlag2());
            }
            if(this.get(i).getStandbyFlag3() == null || this.get(i).getStandbyFlag3().equals("null")) {
            	pstmt.setString(43,null);
            } else {
            	pstmt.setString(43, this.get(i).getStandbyFlag3());
            }
            if(this.get(i).getInputOperator() == null || this.get(i).getInputOperator().equals("null")) {
            	pstmt.setString(44,null);
            } else {
            	pstmt.setString(44, this.get(i).getInputOperator());
            }
            if(this.get(i).getInputDate() == null || this.get(i).getInputDate().equals("null")) {
                pstmt.setDate(45,null);
            } else {
                pstmt.setDate(45, Date.valueOf(this.get(i).getInputDate()));
            }
            if(this.get(i).getInputTime() == null || this.get(i).getInputTime().equals("null")) {
            	pstmt.setString(46,null);
            } else {
            	pstmt.setString(46, this.get(i).getInputTime());
            }
            if(this.get(i).getApproveFlag() == null || this.get(i).getApproveFlag().equals("null")) {
            	pstmt.setString(47,null);
            } else {
            	pstmt.setString(47, this.get(i).getApproveFlag());
            }
            if(this.get(i).getApproveCode() == null || this.get(i).getApproveCode().equals("null")) {
            	pstmt.setString(48,null);
            } else {
            	pstmt.setString(48, this.get(i).getApproveCode());
            }
            if(this.get(i).getApproveDate() == null || this.get(i).getApproveDate().equals("null")) {
                pstmt.setDate(49,null);
            } else {
                pstmt.setDate(49, Date.valueOf(this.get(i).getApproveDate()));
            }
            if(this.get(i).getApproveTime() == null || this.get(i).getApproveTime().equals("null")) {
            	pstmt.setString(50,null);
            } else {
            	pstmt.setString(50, this.get(i).getApproveTime());
            }
            if(this.get(i).getUWOperator() == null || this.get(i).getUWOperator().equals("null")) {
            	pstmt.setString(51,null);
            } else {
            	pstmt.setString(51, this.get(i).getUWOperator());
            }
            if(this.get(i).getUWFlag() == null || this.get(i).getUWFlag().equals("null")) {
            	pstmt.setString(52,null);
            } else {
            	pstmt.setString(52, this.get(i).getUWFlag());
            }
            if(this.get(i).getUWDate() == null || this.get(i).getUWDate().equals("null")) {
                pstmt.setDate(53,null);
            } else {
                pstmt.setDate(53, Date.valueOf(this.get(i).getUWDate()));
            }
            if(this.get(i).getUWTime() == null || this.get(i).getUWTime().equals("null")) {
            	pstmt.setString(54,null);
            } else {
            	pstmt.setString(54, this.get(i).getUWTime());
            }
            if(this.get(i).getAppFlag() == null || this.get(i).getAppFlag().equals("null")) {
            	pstmt.setString(55,null);
            } else {
            	pstmt.setString(55, this.get(i).getAppFlag());
            }
            if(this.get(i).getState() == null || this.get(i).getState().equals("null")) {
            	pstmt.setString(56,null);
            } else {
            	pstmt.setString(56, this.get(i).getState());
            }
            if(this.get(i).getPolApplyDate() == null || this.get(i).getPolApplyDate().equals("null")) {
                pstmt.setDate(57,null);
            } else {
                pstmt.setDate(57, Date.valueOf(this.get(i).getPolApplyDate()));
            }
            if(this.get(i).getCustomGetPolDate() == null || this.get(i).getCustomGetPolDate().equals("null")) {
                pstmt.setDate(58,null);
            } else {
                pstmt.setDate(58, Date.valueOf(this.get(i).getCustomGetPolDate()));
            }
            if(this.get(i).getGetPolDate() == null || this.get(i).getGetPolDate().equals("null")) {
                pstmt.setDate(59,null);
            } else {
                pstmt.setDate(59, Date.valueOf(this.get(i).getGetPolDate()));
            }
            if(this.get(i).getManageCom() == null || this.get(i).getManageCom().equals("null")) {
            	pstmt.setString(60,null);
            } else {
            	pstmt.setString(60, this.get(i).getManageCom());
            }
            if(this.get(i).getComCode() == null || this.get(i).getComCode().equals("null")) {
            	pstmt.setString(61,null);
            } else {
            	pstmt.setString(61, this.get(i).getComCode());
            }
            if(this.get(i).getOperator() == null || this.get(i).getOperator().equals("null")) {
            	pstmt.setString(62,null);
            } else {
            	pstmt.setString(62, this.get(i).getOperator());
            }
            if(this.get(i).getMakeDate() == null || this.get(i).getMakeDate().equals("null")) {
                pstmt.setDate(63,null);
            } else {
                pstmt.setDate(63, Date.valueOf(this.get(i).getMakeDate()));
            }
            if(this.get(i).getMakeTime() == null || this.get(i).getMakeTime().equals("null")) {
            	pstmt.setString(64,null);
            } else {
            	pstmt.setString(64, this.get(i).getMakeTime());
            }
            if(this.get(i).getModifyOperator() == null || this.get(i).getModifyOperator().equals("null")) {
            	pstmt.setString(65,null);
            } else {
            	pstmt.setString(65, this.get(i).getModifyOperator());
            }
            if(this.get(i).getModifyDate() == null || this.get(i).getModifyDate().equals("null")) {
                pstmt.setDate(66,null);
            } else {
                pstmt.setDate(66, Date.valueOf(this.get(i).getModifyDate()));
            }
            if(this.get(i).getModifyTime() == null || this.get(i).getModifyTime().equals("null")) {
            	pstmt.setString(67,null);
            } else {
            	pstmt.setString(67, this.get(i).getModifyTime());
            }
            if(this.get(i).getEnterKind() == null || this.get(i).getEnterKind().equals("null")) {
            	pstmt.setString(68,null);
            } else {
            	pstmt.setString(68, this.get(i).getEnterKind());
            }
            if(this.get(i).getAmntGrade() == null || this.get(i).getAmntGrade().equals("null")) {
            	pstmt.setString(69,null);
            } else {
            	pstmt.setString(69, this.get(i).getAmntGrade());
            }
            if(this.get(i).getFirstTrialOperator() == null || this.get(i).getFirstTrialOperator().equals("null")) {
            	pstmt.setString(70,null);
            } else {
            	pstmt.setString(70, this.get(i).getFirstTrialOperator());
            }
            if(this.get(i).getFirstTrialDate() == null || this.get(i).getFirstTrialDate().equals("null")) {
                pstmt.setDate(71,null);
            } else {
                pstmt.setDate(71, Date.valueOf(this.get(i).getFirstTrialDate()));
            }
            if(this.get(i).getFirstTrialTime() == null || this.get(i).getFirstTrialTime().equals("null")) {
            	pstmt.setString(72,null);
            } else {
            	pstmt.setString(72, this.get(i).getFirstTrialTime());
            }
            if(this.get(i).getReceiveOperator() == null || this.get(i).getReceiveOperator().equals("null")) {
            	pstmt.setString(73,null);
            } else {
            	pstmt.setString(73, this.get(i).getReceiveOperator());
            }
            if(this.get(i).getReceiveDate() == null || this.get(i).getReceiveDate().equals("null")) {
                pstmt.setDate(74,null);
            } else {
                pstmt.setDate(74, Date.valueOf(this.get(i).getReceiveDate()));
            }
            if(this.get(i).getReceiveTime() == null || this.get(i).getReceiveTime().equals("null")) {
            	pstmt.setString(75,null);
            } else {
            	pstmt.setString(75, this.get(i).getReceiveTime());
            }
            if(this.get(i).getMarketType() == null || this.get(i).getMarketType().equals("null")) {
            	pstmt.setString(76,null);
            } else {
            	pstmt.setString(76, this.get(i).getMarketType());
            }
            if(this.get(i).getReportNo() == null || this.get(i).getReportNo().equals("null")) {
            	pstmt.setString(77,null);
            } else {
            	pstmt.setString(77, this.get(i).getReportNo());
            }
            if(this.get(i).getEdorCalType() == null || this.get(i).getEdorCalType().equals("null")) {
            	pstmt.setString(78,null);
            } else {
            	pstmt.setString(78, this.get(i).getEdorCalType());
            }
            if(this.get(i).getRenewFlag() == null || this.get(i).getRenewFlag().equals("null")) {
            	pstmt.setString(79,null);
            } else {
            	pstmt.setString(79, this.get(i).getRenewFlag());
            }
            if(this.get(i).getRenewContNo() == null || this.get(i).getRenewContNo().equals("null")) {
            	pstmt.setString(80,null);
            } else {
            	pstmt.setString(80, this.get(i).getRenewContNo());
            }
            if(this.get(i).getEndDate() == null || this.get(i).getEndDate().equals("null")) {
                pstmt.setDate(81,null);
            } else {
                pstmt.setDate(81, Date.valueOf(this.get(i).getEndDate()));
            }
            if(this.get(i).getCoinsurance() == null || this.get(i).getCoinsurance().equals("null")) {
            	pstmt.setString(82,null);
            } else {
            	pstmt.setString(82, this.get(i).getCoinsurance());
            }
            if(this.get(i).getCardTypeCode() == null || this.get(i).getCardTypeCode().equals("null")) {
            	pstmt.setString(83,null);
            } else {
            	pstmt.setString(83, this.get(i).getCardTypeCode());
            }
            if(this.get(i).getRelationToappnt() == null || this.get(i).getRelationToappnt().equals("null")) {
            	pstmt.setString(84,null);
            } else {
            	pstmt.setString(84, this.get(i).getRelationToappnt());
            }
            if(this.get(i).getNewReinsureFlag() == null || this.get(i).getNewReinsureFlag().equals("null")) {
            	pstmt.setString(85,null);
            } else {
            	pstmt.setString(85, this.get(i).getNewReinsureFlag());
            }
            pstmt.setLong(86, this.get(i).getInsureRate());
            if(this.get(i).getAgcAgentCode() == null || this.get(i).getAgcAgentCode().equals("null")) {
            	pstmt.setString(87,null);
            } else {
            	pstmt.setString(87, this.get(i).getAgcAgentCode());
            }
            if(this.get(i).getAgcAgentName() == null || this.get(i).getAgcAgentName().equals("null")) {
            	pstmt.setString(88,null);
            } else {
            	pstmt.setString(88, this.get(i).getAgcAgentName());
            }
            if(this.get(i).getAvailiaDateFlag() == null || this.get(i).getAvailiaDateFlag().equals("null")) {
            	pstmt.setString(89,null);
            } else {
            	pstmt.setString(89, this.get(i).getAvailiaDateFlag());
            }
            if(this.get(i).getComBusType() == null || this.get(i).getComBusType().equals("null")) {
            	pstmt.setString(90,null);
            } else {
            	pstmt.setString(90, this.get(i).getComBusType());
            }
            if(this.get(i).getPrintFlag() == null || this.get(i).getPrintFlag().equals("null")) {
            	pstmt.setString(91,null);
            } else {
            	pstmt.setString(91, this.get(i).getPrintFlag());
            }
            if(this.get(i).getSpecAcceptFlag() == null || this.get(i).getSpecAcceptFlag().equals("null")) {
            	pstmt.setString(92,null);
            } else {
            	pstmt.setString(92, this.get(i).getSpecAcceptFlag());
            }
            if(this.get(i).getPolicyType() == null || this.get(i).getPolicyType().equals("null")) {
            	pstmt.setString(93,null);
            } else {
            	pstmt.setString(93, this.get(i).getPolicyType());
            }
            if(this.get(i).getAchvAccruFlag() == null || this.get(i).getAchvAccruFlag().equals("null")) {
            	pstmt.setString(94,null);
            } else {
            	pstmt.setString(94, this.get(i).getAchvAccruFlag());
            }
            if(this.get(i).getRemark() == null || this.get(i).getRemark().equals("null")) {
            	pstmt.setString(95,null);
            } else {
            	pstmt.setString(95, this.get(i).getRemark());
            }
            if(this.get(i).getValDateType() == null || this.get(i).getValDateType().equals("null")) {
            	pstmt.setString(96,null);
            } else {
            	pstmt.setString(96, this.get(i).getValDateType());
            }
            if(this.get(i).getSecLevelOther() == null || this.get(i).getSecLevelOther().equals("null")) {
            	pstmt.setString(97,null);
            } else {
            	pstmt.setString(97, this.get(i).getSecLevelOther());
            }
            if(this.get(i).getTBType() == null || this.get(i).getTBType().equals("null")) {
            	pstmt.setString(98,null);
            } else {
            	pstmt.setString(98, this.get(i).getTBType());
            }
            if(this.get(i).getSlipForm() == null || this.get(i).getSlipForm().equals("null")) {
            	pstmt.setString(99,null);
            } else {
            	pstmt.setString(99, this.get(i).getSlipForm());
            }
                pstmt.addBatch();
            }
            pstmt.executeBatch();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            ex.printStackTrace();
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LCGrpContDBSet";
            tError.functionName = "insert()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            try {
                pstmt.close();
            } catch (Exception e){e.printStackTrace();}

            if( !mflag ) {
                try {
                    con.close();
                } catch (Exception e){e.printStackTrace();}
            }

            return false;
        }

        if( !mflag ) {
            try {
                con.close();
            } catch (Exception e){e.printStackTrace();}
        }

        return true;
    }
}
