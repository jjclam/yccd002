/**
 * Copyright (c) 2018 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.entity;

import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.Pojo;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;

import java.io.Serializable;

/**
 * <p>ClassName: LBGrpContPojo </p>
 * <p>Copyright: Copyright (c) 2018</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2018-05-23
 */
public class LBGrpContPojo implements Pojo,Serializable {
    // @Field
    /** 集体合同号码 */
    private String GrpContNo; 
    /** 集体投保单号码 */
    private String ProposalGrpContNo; 
    /** 印刷号码 */
    private String PrtNo; 
    /** 销售渠道 */
    private String SaleChnl; 
    /** 销售方式 */
    private String SalesWay; 
    /** 销售方式子类 */
    private String SalesWaySub; 
    /** 业务性质 */
    private String BizNature; 
    /** 出单平台 */
    private String PolIssuPlat; 
    /** 总单类型 */
    private String ContType; 
    /** 代理机构 */
    private String AgentCom; 
    /** 代理人编码 */
    private String AgentCode; 
    /** 代理人组别 */
    private String AgentGroup; 
    /** 联合代理人代码 */
    private String AgentCode1; 
    /** 客户号码 */
    private String AppntNo; 
    /** 投保总人数 */
    private int Peoples2; 
    /** 单位名称 */
    private String GrpName; 
    /** 合同争议处理方式 */
    private String DisputedFlag; 
    /** 溢交处理方式 */
    private String OutPayFlag; 
    /** 语种标记 */
    private String Lang; 
    /** 币别 */
    private String Currency; 
    /** 遗失补发次数 */
    private int LostTimes; 
    /** 保单打印次数 */
    private int PrintCount; 
    /** 最后一次保全日期 */
    private String  LastEdorDate;
    /** 团体特殊业务标志 */
    private String SpecFlag; 
    /** 签单机构 */
    private String SignCom; 
    /** 签单日期 */
    private String  SignDate;
    /** 签单时间 */
    private String SignTime; 
    /** 保单生效日期 */
    private String  CValiDate;
    /** 交费间隔 */
    private int PayIntv; 
    /** 管理费比例 */
    private double ManageFeeRate; 
    /** 预计人数 */
    private int ExpPeoples; 
    /** 预计保费 */
    private double ExpPremium; 
    /** 预计保额 */
    private double ExpAmnt; 
    /** 总人数 */
    private int Peoples; 
    /** 总档次 */
    private double Mult; 
    /** 总保费 */
    private double Prem; 
    /** 总保额 */
    private double Amnt; 
    /** 总累计保费 */
    private double SumPrem; 
    /** 总累计交费 */
    private double SumPay; 
    /** 差额 */
    private double Dif; 
    /** 备用属性字段1 */
    private String StandbyFlag1; 
    /** 备用属性字段2 */
    private String StandbyFlag2; 
    /** 备用属性字段3 */
    private String StandbyFlag3; 
    /** 录单人 */
    private String InputOperator; 
    /** 录单完成日期 */
    private String  InputDate;
    /** 录单完成时间 */
    private String InputTime; 
    /** 复核状态 */
    private String ApproveFlag; 
    /** 复核人编码 */
    private String ApproveCode; 
    /** 复核日期 */
    private String  ApproveDate;
    /** 复核时间 */
    private String ApproveTime; 
    /** 核保人 */
    private String UWOperator; 
    /** 核保状态 */
    private String UWFlag; 
    /** 核保完成日期 */
    private String  UWDate;
    /** 核保完成时间 */
    private String UWTime; 
    /** 投保单/保单标志 */
    private String AppFlag; 
    /** 状态 */
    private String State; 
    /** 投保单申请日期 */
    private String  PolApplyDate;
    /** 保单回执客户签收日期 */
    private String  CustomGetPolDate;
    /** 保单送达日期 */
    private String  GetPolDate;
    /** 管理机构 */
    private String ManageCom; 
    /** 公司代码 */
    private String ComCode; 
    /** 操作员 */
    private String Operator; 
    /** 入机日期 */
    private String  MakeDate;
    /** 入机时间 */
    private String MakeTime; 
    /** 最后一次修改操作员 */
    private String ModifyOperator; 
    /** 最后一次修改日期 */
    private String  ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime; 
    /** 参保形式 */
    private String EnterKind; 
    /** 保障层级划分标准 */
    private String AmntGrade; 
    /** 初审人 */
    private String FirstTrialOperator; 
    /** 初审日期 */
    private String  FirstTrialDate;
    /** 初审时间 */
    private String FirstTrialTime; 
    /** 收单人 */
    private String ReceiveOperator; 
    /** 收单日期 */
    private String  ReceiveDate;
    /** 收单时间 */
    private String ReceiveTime; 
    /** 市场类型 */
    private String MarketType; 
    /** 呈报号 */
    private String ReportNo; 
    /** 是否使用保全特殊算法 */
    private String EdorCalType; 
    /** 续保标识 */
    private String RenewFlag; 
    /** 续保保单原始保单号 */
    private String RenewContNo; 
    /** 终止日期 */
    private String  EndDate;
    /** 是否共保 */
    private String Coinsurance; 
    /** 卡单 */
    private String CardTypeCode; 
    /** 与被保人关系 */
    private String RelationToappnt; 
    /** 临分标记 */
    private String NewReinsureFlag; 
    /** 投保比例 */
    private long InsureRate; 
    /** 中介代理人编码 */
    private String AgcAgentCode; 
    /** 中介代理人姓名 */
    private String AgcAgentName; 
    /** 约定生效日期 */
    private String AvailiaDateFlag; 
    /** 共保业务类型 */
    private String ComBusType; 
    /** 是否打印保单 */
    private String PrintFlag; 
    /** 特殊承保标记 */
    private String SpecAcceptFlag; 
    /** 投保类型 */
    private String PolicyType; 
    /** 是否计业绩 */
    private String AchvAccruFlag; 
    /** 备注 */
    private String Remark; 
    /** 生效日期类型 */
    private String ValDateType; 
    /** 保障层级划分其他说明 */
    private String SecLevelOther; 
    /** 出单方式 */
    private String TBType; 
    /** 保单形式 */
    private String SlipForm; 


    public static final int FIELDNUM = 99;    // 数据库表的字段个数

    private FDate fDate = new FDate();        // 处理日期
    public String getGrpContNo() {
        return GrpContNo;
    }
    public void setGrpContNo(String aGrpContNo) {
        GrpContNo = aGrpContNo;
    }
    public String getProposalGrpContNo() {
        return ProposalGrpContNo;
    }
    public void setProposalGrpContNo(String aProposalGrpContNo) {
        ProposalGrpContNo = aProposalGrpContNo;
    }
    public String getPrtNo() {
        return PrtNo;
    }
    public void setPrtNo(String aPrtNo) {
        PrtNo = aPrtNo;
    }
    public String getSaleChnl() {
        return SaleChnl;
    }
    public void setSaleChnl(String aSaleChnl) {
        SaleChnl = aSaleChnl;
    }
    public String getSalesWay() {
        return SalesWay;
    }
    public void setSalesWay(String aSalesWay) {
        SalesWay = aSalesWay;
    }
    public String getSalesWaySub() {
        return SalesWaySub;
    }
    public void setSalesWaySub(String aSalesWaySub) {
        SalesWaySub = aSalesWaySub;
    }
    public String getBizNature() {
        return BizNature;
    }
    public void setBizNature(String aBizNature) {
        BizNature = aBizNature;
    }
    public String getPolIssuPlat() {
        return PolIssuPlat;
    }
    public void setPolIssuPlat(String aPolIssuPlat) {
        PolIssuPlat = aPolIssuPlat;
    }
    public String getContType() {
        return ContType;
    }
    public void setContType(String aContType) {
        ContType = aContType;
    }
    public String getAgentCom() {
        return AgentCom;
    }
    public void setAgentCom(String aAgentCom) {
        AgentCom = aAgentCom;
    }
    public String getAgentCode() {
        return AgentCode;
    }
    public void setAgentCode(String aAgentCode) {
        AgentCode = aAgentCode;
    }
    public String getAgentGroup() {
        return AgentGroup;
    }
    public void setAgentGroup(String aAgentGroup) {
        AgentGroup = aAgentGroup;
    }
    public String getAgentCode1() {
        return AgentCode1;
    }
    public void setAgentCode1(String aAgentCode1) {
        AgentCode1 = aAgentCode1;
    }
    public String getAppntNo() {
        return AppntNo;
    }
    public void setAppntNo(String aAppntNo) {
        AppntNo = aAppntNo;
    }
    public int getPeoples2() {
        return Peoples2;
    }
    public void setPeoples2(int aPeoples2) {
        Peoples2 = aPeoples2;
    }
    public void setPeoples2(String aPeoples2) {
        if (aPeoples2 != null && !aPeoples2.equals("")) {
            Integer tInteger = new Integer(aPeoples2);
            int i = tInteger.intValue();
            Peoples2 = i;
        }
    }

    public String getGrpName() {
        return GrpName;
    }
    public void setGrpName(String aGrpName) {
        GrpName = aGrpName;
    }
    public String getDisputedFlag() {
        return DisputedFlag;
    }
    public void setDisputedFlag(String aDisputedFlag) {
        DisputedFlag = aDisputedFlag;
    }
    public String getOutPayFlag() {
        return OutPayFlag;
    }
    public void setOutPayFlag(String aOutPayFlag) {
        OutPayFlag = aOutPayFlag;
    }
    public String getLang() {
        return Lang;
    }
    public void setLang(String aLang) {
        Lang = aLang;
    }
    public String getCurrency() {
        return Currency;
    }
    public void setCurrency(String aCurrency) {
        Currency = aCurrency;
    }
    public int getLostTimes() {
        return LostTimes;
    }
    public void setLostTimes(int aLostTimes) {
        LostTimes = aLostTimes;
    }
    public void setLostTimes(String aLostTimes) {
        if (aLostTimes != null && !aLostTimes.equals("")) {
            Integer tInteger = new Integer(aLostTimes);
            int i = tInteger.intValue();
            LostTimes = i;
        }
    }

    public int getPrintCount() {
        return PrintCount;
    }
    public void setPrintCount(int aPrintCount) {
        PrintCount = aPrintCount;
    }
    public void setPrintCount(String aPrintCount) {
        if (aPrintCount != null && !aPrintCount.equals("")) {
            Integer tInteger = new Integer(aPrintCount);
            int i = tInteger.intValue();
            PrintCount = i;
        }
    }

    public String getLastEdorDate() {
        return LastEdorDate;
    }
    public void setLastEdorDate(String aLastEdorDate) {
        LastEdorDate = aLastEdorDate;
    }
    public String getSpecFlag() {
        return SpecFlag;
    }
    public void setSpecFlag(String aSpecFlag) {
        SpecFlag = aSpecFlag;
    }
    public String getSignCom() {
        return SignCom;
    }
    public void setSignCom(String aSignCom) {
        SignCom = aSignCom;
    }
    public String getSignDate() {
        return SignDate;
    }
    public void setSignDate(String aSignDate) {
        SignDate = aSignDate;
    }
    public String getSignTime() {
        return SignTime;
    }
    public void setSignTime(String aSignTime) {
        SignTime = aSignTime;
    }
    public String getCValiDate() {
        return CValiDate;
    }
    public void setCValiDate(String aCValiDate) {
        CValiDate = aCValiDate;
    }
    public int getPayIntv() {
        return PayIntv;
    }
    public void setPayIntv(int aPayIntv) {
        PayIntv = aPayIntv;
    }
    public void setPayIntv(String aPayIntv) {
        if (aPayIntv != null && !aPayIntv.equals("")) {
            Integer tInteger = new Integer(aPayIntv);
            int i = tInteger.intValue();
            PayIntv = i;
        }
    }

    public double getManageFeeRate() {
        return ManageFeeRate;
    }
    public void setManageFeeRate(double aManageFeeRate) {
        ManageFeeRate = aManageFeeRate;
    }
    public void setManageFeeRate(String aManageFeeRate) {
        if (aManageFeeRate != null && !aManageFeeRate.equals("")) {
            Double tDouble = new Double(aManageFeeRate);
            double d = tDouble.doubleValue();
            ManageFeeRate = d;
        }
    }

    public int getExpPeoples() {
        return ExpPeoples;
    }
    public void setExpPeoples(int aExpPeoples) {
        ExpPeoples = aExpPeoples;
    }
    public void setExpPeoples(String aExpPeoples) {
        if (aExpPeoples != null && !aExpPeoples.equals("")) {
            Integer tInteger = new Integer(aExpPeoples);
            int i = tInteger.intValue();
            ExpPeoples = i;
        }
    }

    public double getExpPremium() {
        return ExpPremium;
    }
    public void setExpPremium(double aExpPremium) {
        ExpPremium = aExpPremium;
    }
    public void setExpPremium(String aExpPremium) {
        if (aExpPremium != null && !aExpPremium.equals("")) {
            Double tDouble = new Double(aExpPremium);
            double d = tDouble.doubleValue();
            ExpPremium = d;
        }
    }

    public double getExpAmnt() {
        return ExpAmnt;
    }
    public void setExpAmnt(double aExpAmnt) {
        ExpAmnt = aExpAmnt;
    }
    public void setExpAmnt(String aExpAmnt) {
        if (aExpAmnt != null && !aExpAmnt.equals("")) {
            Double tDouble = new Double(aExpAmnt);
            double d = tDouble.doubleValue();
            ExpAmnt = d;
        }
    }

    public int getPeoples() {
        return Peoples;
    }
    public void setPeoples(int aPeoples) {
        Peoples = aPeoples;
    }
    public void setPeoples(String aPeoples) {
        if (aPeoples != null && !aPeoples.equals("")) {
            Integer tInteger = new Integer(aPeoples);
            int i = tInteger.intValue();
            Peoples = i;
        }
    }

    public double getMult() {
        return Mult;
    }
    public void setMult(double aMult) {
        Mult = aMult;
    }
    public void setMult(String aMult) {
        if (aMult != null && !aMult.equals("")) {
            Double tDouble = new Double(aMult);
            double d = tDouble.doubleValue();
            Mult = d;
        }
    }

    public double getPrem() {
        return Prem;
    }
    public void setPrem(double aPrem) {
        Prem = aPrem;
    }
    public void setPrem(String aPrem) {
        if (aPrem != null && !aPrem.equals("")) {
            Double tDouble = new Double(aPrem);
            double d = tDouble.doubleValue();
            Prem = d;
        }
    }

    public double getAmnt() {
        return Amnt;
    }
    public void setAmnt(double aAmnt) {
        Amnt = aAmnt;
    }
    public void setAmnt(String aAmnt) {
        if (aAmnt != null && !aAmnt.equals("")) {
            Double tDouble = new Double(aAmnt);
            double d = tDouble.doubleValue();
            Amnt = d;
        }
    }

    public double getSumPrem() {
        return SumPrem;
    }
    public void setSumPrem(double aSumPrem) {
        SumPrem = aSumPrem;
    }
    public void setSumPrem(String aSumPrem) {
        if (aSumPrem != null && !aSumPrem.equals("")) {
            Double tDouble = new Double(aSumPrem);
            double d = tDouble.doubleValue();
            SumPrem = d;
        }
    }

    public double getSumPay() {
        return SumPay;
    }
    public void setSumPay(double aSumPay) {
        SumPay = aSumPay;
    }
    public void setSumPay(String aSumPay) {
        if (aSumPay != null && !aSumPay.equals("")) {
            Double tDouble = new Double(aSumPay);
            double d = tDouble.doubleValue();
            SumPay = d;
        }
    }

    public double getDif() {
        return Dif;
    }
    public void setDif(double aDif) {
        Dif = aDif;
    }
    public void setDif(String aDif) {
        if (aDif != null && !aDif.equals("")) {
            Double tDouble = new Double(aDif);
            double d = tDouble.doubleValue();
            Dif = d;
        }
    }

    public String getStandbyFlag1() {
        return StandbyFlag1;
    }
    public void setStandbyFlag1(String aStandbyFlag1) {
        StandbyFlag1 = aStandbyFlag1;
    }
    public String getStandbyFlag2() {
        return StandbyFlag2;
    }
    public void setStandbyFlag2(String aStandbyFlag2) {
        StandbyFlag2 = aStandbyFlag2;
    }
    public String getStandbyFlag3() {
        return StandbyFlag3;
    }
    public void setStandbyFlag3(String aStandbyFlag3) {
        StandbyFlag3 = aStandbyFlag3;
    }
    public String getInputOperator() {
        return InputOperator;
    }
    public void setInputOperator(String aInputOperator) {
        InputOperator = aInputOperator;
    }
    public String getInputDate() {
        return InputDate;
    }
    public void setInputDate(String aInputDate) {
        InputDate = aInputDate;
    }
    public String getInputTime() {
        return InputTime;
    }
    public void setInputTime(String aInputTime) {
        InputTime = aInputTime;
    }
    public String getApproveFlag() {
        return ApproveFlag;
    }
    public void setApproveFlag(String aApproveFlag) {
        ApproveFlag = aApproveFlag;
    }
    public String getApproveCode() {
        return ApproveCode;
    }
    public void setApproveCode(String aApproveCode) {
        ApproveCode = aApproveCode;
    }
    public String getApproveDate() {
        return ApproveDate;
    }
    public void setApproveDate(String aApproveDate) {
        ApproveDate = aApproveDate;
    }
    public String getApproveTime() {
        return ApproveTime;
    }
    public void setApproveTime(String aApproveTime) {
        ApproveTime = aApproveTime;
    }
    public String getUWOperator() {
        return UWOperator;
    }
    public void setUWOperator(String aUWOperator) {
        UWOperator = aUWOperator;
    }
    public String getUWFlag() {
        return UWFlag;
    }
    public void setUWFlag(String aUWFlag) {
        UWFlag = aUWFlag;
    }
    public String getUWDate() {
        return UWDate;
    }
    public void setUWDate(String aUWDate) {
        UWDate = aUWDate;
    }
    public String getUWTime() {
        return UWTime;
    }
    public void setUWTime(String aUWTime) {
        UWTime = aUWTime;
    }
    public String getAppFlag() {
        return AppFlag;
    }
    public void setAppFlag(String aAppFlag) {
        AppFlag = aAppFlag;
    }
    public String getState() {
        return State;
    }
    public void setState(String aState) {
        State = aState;
    }
    public String getPolApplyDate() {
        return PolApplyDate;
    }
    public void setPolApplyDate(String aPolApplyDate) {
        PolApplyDate = aPolApplyDate;
    }
    public String getCustomGetPolDate() {
        return CustomGetPolDate;
    }
    public void setCustomGetPolDate(String aCustomGetPolDate) {
        CustomGetPolDate = aCustomGetPolDate;
    }
    public String getGetPolDate() {
        return GetPolDate;
    }
    public void setGetPolDate(String aGetPolDate) {
        GetPolDate = aGetPolDate;
    }
    public String getManageCom() {
        return ManageCom;
    }
    public void setManageCom(String aManageCom) {
        ManageCom = aManageCom;
    }
    public String getComCode() {
        return ComCode;
    }
    public void setComCode(String aComCode) {
        ComCode = aComCode;
    }
    public String getOperator() {
        return Operator;
    }
    public void setOperator(String aOperator) {
        Operator = aOperator;
    }
    public String getMakeDate() {
        return MakeDate;
    }
    public void setMakeDate(String aMakeDate) {
        MakeDate = aMakeDate;
    }
    public String getMakeTime() {
        return MakeTime;
    }
    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }
    public String getModifyOperator() {
        return ModifyOperator;
    }
    public void setModifyOperator(String aModifyOperator) {
        ModifyOperator = aModifyOperator;
    }
    public String getModifyDate() {
        return ModifyDate;
    }
    public void setModifyDate(String aModifyDate) {
        ModifyDate = aModifyDate;
    }
    public String getModifyTime() {
        return ModifyTime;
    }
    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }
    public String getEnterKind() {
        return EnterKind;
    }
    public void setEnterKind(String aEnterKind) {
        EnterKind = aEnterKind;
    }
    public String getAmntGrade() {
        return AmntGrade;
    }
    public void setAmntGrade(String aAmntGrade) {
        AmntGrade = aAmntGrade;
    }
    public String getFirstTrialOperator() {
        return FirstTrialOperator;
    }
    public void setFirstTrialOperator(String aFirstTrialOperator) {
        FirstTrialOperator = aFirstTrialOperator;
    }
    public String getFirstTrialDate() {
        return FirstTrialDate;
    }
    public void setFirstTrialDate(String aFirstTrialDate) {
        FirstTrialDate = aFirstTrialDate;
    }
    public String getFirstTrialTime() {
        return FirstTrialTime;
    }
    public void setFirstTrialTime(String aFirstTrialTime) {
        FirstTrialTime = aFirstTrialTime;
    }
    public String getReceiveOperator() {
        return ReceiveOperator;
    }
    public void setReceiveOperator(String aReceiveOperator) {
        ReceiveOperator = aReceiveOperator;
    }
    public String getReceiveDate() {
        return ReceiveDate;
    }
    public void setReceiveDate(String aReceiveDate) {
        ReceiveDate = aReceiveDate;
    }
    public String getReceiveTime() {
        return ReceiveTime;
    }
    public void setReceiveTime(String aReceiveTime) {
        ReceiveTime = aReceiveTime;
    }
    public String getMarketType() {
        return MarketType;
    }
    public void setMarketType(String aMarketType) {
        MarketType = aMarketType;
    }
    public String getReportNo() {
        return ReportNo;
    }
    public void setReportNo(String aReportNo) {
        ReportNo = aReportNo;
    }
    public String getEdorCalType() {
        return EdorCalType;
    }
    public void setEdorCalType(String aEdorCalType) {
        EdorCalType = aEdorCalType;
    }
    public String getRenewFlag() {
        return RenewFlag;
    }
    public void setRenewFlag(String aRenewFlag) {
        RenewFlag = aRenewFlag;
    }
    public String getRenewContNo() {
        return RenewContNo;
    }
    public void setRenewContNo(String aRenewContNo) {
        RenewContNo = aRenewContNo;
    }
    public String getEndDate() {
        return EndDate;
    }
    public void setEndDate(String aEndDate) {
        EndDate = aEndDate;
    }
    public String getCoinsurance() {
        return Coinsurance;
    }
    public void setCoinsurance(String aCoinsurance) {
        Coinsurance = aCoinsurance;
    }
    public String getCardTypeCode() {
        return CardTypeCode;
    }
    public void setCardTypeCode(String aCardTypeCode) {
        CardTypeCode = aCardTypeCode;
    }
    public String getRelationToappnt() {
        return RelationToappnt;
    }
    public void setRelationToappnt(String aRelationToappnt) {
        RelationToappnt = aRelationToappnt;
    }
    public String getNewReinsureFlag() {
        return NewReinsureFlag;
    }
    public void setNewReinsureFlag(String aNewReinsureFlag) {
        NewReinsureFlag = aNewReinsureFlag;
    }
    public long getInsureRate() {
        return InsureRate;
    }
    public void setInsureRate(long aInsureRate) {
        InsureRate = aInsureRate;
    }
    public void setInsureRate(String aInsureRate) {
        if (aInsureRate != null && !aInsureRate.equals("")) {
            InsureRate = new Long(aInsureRate).longValue();
        }
    }

    public String getAgcAgentCode() {
        return AgcAgentCode;
    }
    public void setAgcAgentCode(String aAgcAgentCode) {
        AgcAgentCode = aAgcAgentCode;
    }
    public String getAgcAgentName() {
        return AgcAgentName;
    }
    public void setAgcAgentName(String aAgcAgentName) {
        AgcAgentName = aAgcAgentName;
    }
    public String getAvailiaDateFlag() {
        return AvailiaDateFlag;
    }
    public void setAvailiaDateFlag(String aAvailiaDateFlag) {
        AvailiaDateFlag = aAvailiaDateFlag;
    }
    public String getComBusType() {
        return ComBusType;
    }
    public void setComBusType(String aComBusType) {
        ComBusType = aComBusType;
    }
    public String getPrintFlag() {
        return PrintFlag;
    }
    public void setPrintFlag(String aPrintFlag) {
        PrintFlag = aPrintFlag;
    }
    public String getSpecAcceptFlag() {
        return SpecAcceptFlag;
    }
    public void setSpecAcceptFlag(String aSpecAcceptFlag) {
        SpecAcceptFlag = aSpecAcceptFlag;
    }
    public String getPolicyType() {
        return PolicyType;
    }
    public void setPolicyType(String aPolicyType) {
        PolicyType = aPolicyType;
    }
    public String getAchvAccruFlag() {
        return AchvAccruFlag;
    }
    public void setAchvAccruFlag(String aAchvAccruFlag) {
        AchvAccruFlag = aAchvAccruFlag;
    }
    public String getRemark() {
        return Remark;
    }
    public void setRemark(String aRemark) {
        Remark = aRemark;
    }
    public String getValDateType() {
        return ValDateType;
    }
    public void setValDateType(String aValDateType) {
        ValDateType = aValDateType;
    }
    public String getSecLevelOther() {
        return SecLevelOther;
    }
    public void setSecLevelOther(String aSecLevelOther) {
        SecLevelOther = aSecLevelOther;
    }
    public String getTBType() {
        return TBType;
    }
    public void setTBType(String aTBType) {
        TBType = aTBType;
    }
    public String getSlipForm() {
        return SlipForm;
    }
    public void setSlipForm(String aSlipForm) {
        SlipForm = aSlipForm;
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("GrpContNo") ) {
            return 0;
        }
        if( strFieldName.equals("ProposalGrpContNo") ) {
            return 1;
        }
        if( strFieldName.equals("PrtNo") ) {
            return 2;
        }
        if( strFieldName.equals("SaleChnl") ) {
            return 3;
        }
        if( strFieldName.equals("SalesWay") ) {
            return 4;
        }
        if( strFieldName.equals("SalesWaySub") ) {
            return 5;
        }
        if( strFieldName.equals("BizNature") ) {
            return 6;
        }
        if( strFieldName.equals("PolIssuPlat") ) {
            return 7;
        }
        if( strFieldName.equals("ContType") ) {
            return 8;
        }
        if( strFieldName.equals("AgentCom") ) {
            return 9;
        }
        if( strFieldName.equals("AgentCode") ) {
            return 10;
        }
        if( strFieldName.equals("AgentGroup") ) {
            return 11;
        }
        if( strFieldName.equals("AgentCode1") ) {
            return 12;
        }
        if( strFieldName.equals("AppntNo") ) {
            return 13;
        }
        if( strFieldName.equals("Peoples2") ) {
            return 14;
        }
        if( strFieldName.equals("GrpName") ) {
            return 15;
        }
        if( strFieldName.equals("DisputedFlag") ) {
            return 16;
        }
        if( strFieldName.equals("OutPayFlag") ) {
            return 17;
        }
        if( strFieldName.equals("Lang") ) {
            return 18;
        }
        if( strFieldName.equals("Currency") ) {
            return 19;
        }
        if( strFieldName.equals("LostTimes") ) {
            return 20;
        }
        if( strFieldName.equals("PrintCount") ) {
            return 21;
        }
        if( strFieldName.equals("LastEdorDate") ) {
            return 22;
        }
        if( strFieldName.equals("SpecFlag") ) {
            return 23;
        }
        if( strFieldName.equals("SignCom") ) {
            return 24;
        }
        if( strFieldName.equals("SignDate") ) {
            return 25;
        }
        if( strFieldName.equals("SignTime") ) {
            return 26;
        }
        if( strFieldName.equals("CValiDate") ) {
            return 27;
        }
        if( strFieldName.equals("PayIntv") ) {
            return 28;
        }
        if( strFieldName.equals("ManageFeeRate") ) {
            return 29;
        }
        if( strFieldName.equals("ExpPeoples") ) {
            return 30;
        }
        if( strFieldName.equals("ExpPremium") ) {
            return 31;
        }
        if( strFieldName.equals("ExpAmnt") ) {
            return 32;
        }
        if( strFieldName.equals("Peoples") ) {
            return 33;
        }
        if( strFieldName.equals("Mult") ) {
            return 34;
        }
        if( strFieldName.equals("Prem") ) {
            return 35;
        }
        if( strFieldName.equals("Amnt") ) {
            return 36;
        }
        if( strFieldName.equals("SumPrem") ) {
            return 37;
        }
        if( strFieldName.equals("SumPay") ) {
            return 38;
        }
        if( strFieldName.equals("Dif") ) {
            return 39;
        }
        if( strFieldName.equals("StandbyFlag1") ) {
            return 40;
        }
        if( strFieldName.equals("StandbyFlag2") ) {
            return 41;
        }
        if( strFieldName.equals("StandbyFlag3") ) {
            return 42;
        }
        if( strFieldName.equals("InputOperator") ) {
            return 43;
        }
        if( strFieldName.equals("InputDate") ) {
            return 44;
        }
        if( strFieldName.equals("InputTime") ) {
            return 45;
        }
        if( strFieldName.equals("ApproveFlag") ) {
            return 46;
        }
        if( strFieldName.equals("ApproveCode") ) {
            return 47;
        }
        if( strFieldName.equals("ApproveDate") ) {
            return 48;
        }
        if( strFieldName.equals("ApproveTime") ) {
            return 49;
        }
        if( strFieldName.equals("UWOperator") ) {
            return 50;
        }
        if( strFieldName.equals("UWFlag") ) {
            return 51;
        }
        if( strFieldName.equals("UWDate") ) {
            return 52;
        }
        if( strFieldName.equals("UWTime") ) {
            return 53;
        }
        if( strFieldName.equals("AppFlag") ) {
            return 54;
        }
        if( strFieldName.equals("State") ) {
            return 55;
        }
        if( strFieldName.equals("PolApplyDate") ) {
            return 56;
        }
        if( strFieldName.equals("CustomGetPolDate") ) {
            return 57;
        }
        if( strFieldName.equals("GetPolDate") ) {
            return 58;
        }
        if( strFieldName.equals("ManageCom") ) {
            return 59;
        }
        if( strFieldName.equals("ComCode") ) {
            return 60;
        }
        if( strFieldName.equals("Operator") ) {
            return 61;
        }
        if( strFieldName.equals("MakeDate") ) {
            return 62;
        }
        if( strFieldName.equals("MakeTime") ) {
            return 63;
        }
        if( strFieldName.equals("ModifyOperator") ) {
            return 64;
        }
        if( strFieldName.equals("ModifyDate") ) {
            return 65;
        }
        if( strFieldName.equals("ModifyTime") ) {
            return 66;
        }
        if( strFieldName.equals("EnterKind") ) {
            return 67;
        }
        if( strFieldName.equals("AmntGrade") ) {
            return 68;
        }
        if( strFieldName.equals("FirstTrialOperator") ) {
            return 69;
        }
        if( strFieldName.equals("FirstTrialDate") ) {
            return 70;
        }
        if( strFieldName.equals("FirstTrialTime") ) {
            return 71;
        }
        if( strFieldName.equals("ReceiveOperator") ) {
            return 72;
        }
        if( strFieldName.equals("ReceiveDate") ) {
            return 73;
        }
        if( strFieldName.equals("ReceiveTime") ) {
            return 74;
        }
        if( strFieldName.equals("MarketType") ) {
            return 75;
        }
        if( strFieldName.equals("ReportNo") ) {
            return 76;
        }
        if( strFieldName.equals("EdorCalType") ) {
            return 77;
        }
        if( strFieldName.equals("RenewFlag") ) {
            return 78;
        }
        if( strFieldName.equals("RenewContNo") ) {
            return 79;
        }
        if( strFieldName.equals("EndDate") ) {
            return 80;
        }
        if( strFieldName.equals("Coinsurance") ) {
            return 81;
        }
        if( strFieldName.equals("CardTypeCode") ) {
            return 82;
        }
        if( strFieldName.equals("RelationToappnt") ) {
            return 83;
        }
        if( strFieldName.equals("NewReinsureFlag") ) {
            return 84;
        }
        if( strFieldName.equals("InsureRate") ) {
            return 85;
        }
        if( strFieldName.equals("AgcAgentCode") ) {
            return 86;
        }
        if( strFieldName.equals("AgcAgentName") ) {
            return 87;
        }
        if( strFieldName.equals("AvailiaDateFlag") ) {
            return 88;
        }
        if( strFieldName.equals("ComBusType") ) {
            return 89;
        }
        if( strFieldName.equals("PrintFlag") ) {
            return 90;
        }
        if( strFieldName.equals("SpecAcceptFlag") ) {
            return 91;
        }
        if( strFieldName.equals("PolicyType") ) {
            return 92;
        }
        if( strFieldName.equals("AchvAccruFlag") ) {
            return 93;
        }
        if( strFieldName.equals("Remark") ) {
            return 94;
        }
        if( strFieldName.equals("ValDateType") ) {
            return 95;
        }
        if( strFieldName.equals("SecLevelOther") ) {
            return 96;
        }
        if( strFieldName.equals("TBType") ) {
            return 97;
        }
        if( strFieldName.equals("SlipForm") ) {
            return 98;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "GrpContNo";
                break;
            case 1:
                strFieldName = "ProposalGrpContNo";
                break;
            case 2:
                strFieldName = "PrtNo";
                break;
            case 3:
                strFieldName = "SaleChnl";
                break;
            case 4:
                strFieldName = "SalesWay";
                break;
            case 5:
                strFieldName = "SalesWaySub";
                break;
            case 6:
                strFieldName = "BizNature";
                break;
            case 7:
                strFieldName = "PolIssuPlat";
                break;
            case 8:
                strFieldName = "ContType";
                break;
            case 9:
                strFieldName = "AgentCom";
                break;
            case 10:
                strFieldName = "AgentCode";
                break;
            case 11:
                strFieldName = "AgentGroup";
                break;
            case 12:
                strFieldName = "AgentCode1";
                break;
            case 13:
                strFieldName = "AppntNo";
                break;
            case 14:
                strFieldName = "Peoples2";
                break;
            case 15:
                strFieldName = "GrpName";
                break;
            case 16:
                strFieldName = "DisputedFlag";
                break;
            case 17:
                strFieldName = "OutPayFlag";
                break;
            case 18:
                strFieldName = "Lang";
                break;
            case 19:
                strFieldName = "Currency";
                break;
            case 20:
                strFieldName = "LostTimes";
                break;
            case 21:
                strFieldName = "PrintCount";
                break;
            case 22:
                strFieldName = "LastEdorDate";
                break;
            case 23:
                strFieldName = "SpecFlag";
                break;
            case 24:
                strFieldName = "SignCom";
                break;
            case 25:
                strFieldName = "SignDate";
                break;
            case 26:
                strFieldName = "SignTime";
                break;
            case 27:
                strFieldName = "CValiDate";
                break;
            case 28:
                strFieldName = "PayIntv";
                break;
            case 29:
                strFieldName = "ManageFeeRate";
                break;
            case 30:
                strFieldName = "ExpPeoples";
                break;
            case 31:
                strFieldName = "ExpPremium";
                break;
            case 32:
                strFieldName = "ExpAmnt";
                break;
            case 33:
                strFieldName = "Peoples";
                break;
            case 34:
                strFieldName = "Mult";
                break;
            case 35:
                strFieldName = "Prem";
                break;
            case 36:
                strFieldName = "Amnt";
                break;
            case 37:
                strFieldName = "SumPrem";
                break;
            case 38:
                strFieldName = "SumPay";
                break;
            case 39:
                strFieldName = "Dif";
                break;
            case 40:
                strFieldName = "StandbyFlag1";
                break;
            case 41:
                strFieldName = "StandbyFlag2";
                break;
            case 42:
                strFieldName = "StandbyFlag3";
                break;
            case 43:
                strFieldName = "InputOperator";
                break;
            case 44:
                strFieldName = "InputDate";
                break;
            case 45:
                strFieldName = "InputTime";
                break;
            case 46:
                strFieldName = "ApproveFlag";
                break;
            case 47:
                strFieldName = "ApproveCode";
                break;
            case 48:
                strFieldName = "ApproveDate";
                break;
            case 49:
                strFieldName = "ApproveTime";
                break;
            case 50:
                strFieldName = "UWOperator";
                break;
            case 51:
                strFieldName = "UWFlag";
                break;
            case 52:
                strFieldName = "UWDate";
                break;
            case 53:
                strFieldName = "UWTime";
                break;
            case 54:
                strFieldName = "AppFlag";
                break;
            case 55:
                strFieldName = "State";
                break;
            case 56:
                strFieldName = "PolApplyDate";
                break;
            case 57:
                strFieldName = "CustomGetPolDate";
                break;
            case 58:
                strFieldName = "GetPolDate";
                break;
            case 59:
                strFieldName = "ManageCom";
                break;
            case 60:
                strFieldName = "ComCode";
                break;
            case 61:
                strFieldName = "Operator";
                break;
            case 62:
                strFieldName = "MakeDate";
                break;
            case 63:
                strFieldName = "MakeTime";
                break;
            case 64:
                strFieldName = "ModifyOperator";
                break;
            case 65:
                strFieldName = "ModifyDate";
                break;
            case 66:
                strFieldName = "ModifyTime";
                break;
            case 67:
                strFieldName = "EnterKind";
                break;
            case 68:
                strFieldName = "AmntGrade";
                break;
            case 69:
                strFieldName = "FirstTrialOperator";
                break;
            case 70:
                strFieldName = "FirstTrialDate";
                break;
            case 71:
                strFieldName = "FirstTrialTime";
                break;
            case 72:
                strFieldName = "ReceiveOperator";
                break;
            case 73:
                strFieldName = "ReceiveDate";
                break;
            case 74:
                strFieldName = "ReceiveTime";
                break;
            case 75:
                strFieldName = "MarketType";
                break;
            case 76:
                strFieldName = "ReportNo";
                break;
            case 77:
                strFieldName = "EdorCalType";
                break;
            case 78:
                strFieldName = "RenewFlag";
                break;
            case 79:
                strFieldName = "RenewContNo";
                break;
            case 80:
                strFieldName = "EndDate";
                break;
            case 81:
                strFieldName = "Coinsurance";
                break;
            case 82:
                strFieldName = "CardTypeCode";
                break;
            case 83:
                strFieldName = "RelationToappnt";
                break;
            case 84:
                strFieldName = "NewReinsureFlag";
                break;
            case 85:
                strFieldName = "InsureRate";
                break;
            case 86:
                strFieldName = "AgcAgentCode";
                break;
            case 87:
                strFieldName = "AgcAgentName";
                break;
            case 88:
                strFieldName = "AvailiaDateFlag";
                break;
            case 89:
                strFieldName = "ComBusType";
                break;
            case 90:
                strFieldName = "PrintFlag";
                break;
            case 91:
                strFieldName = "SpecAcceptFlag";
                break;
            case 92:
                strFieldName = "PolicyType";
                break;
            case 93:
                strFieldName = "AchvAccruFlag";
                break;
            case 94:
                strFieldName = "Remark";
                break;
            case 95:
                strFieldName = "ValDateType";
                break;
            case 96:
                strFieldName = "SecLevelOther";
                break;
            case 97:
                strFieldName = "TBType";
                break;
            case 98:
                strFieldName = "SlipForm";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "GRPCONTNO":
                return Schema.TYPE_STRING;
            case "PROPOSALGRPCONTNO":
                return Schema.TYPE_STRING;
            case "PRTNO":
                return Schema.TYPE_STRING;
            case "SALECHNL":
                return Schema.TYPE_STRING;
            case "SALESWAY":
                return Schema.TYPE_STRING;
            case "SALESWAYSUB":
                return Schema.TYPE_STRING;
            case "BIZNATURE":
                return Schema.TYPE_STRING;
            case "POLISSUPLAT":
                return Schema.TYPE_STRING;
            case "CONTTYPE":
                return Schema.TYPE_STRING;
            case "AGENTCOM":
                return Schema.TYPE_STRING;
            case "AGENTCODE":
                return Schema.TYPE_STRING;
            case "AGENTGROUP":
                return Schema.TYPE_STRING;
            case "AGENTCODE1":
                return Schema.TYPE_STRING;
            case "APPNTNO":
                return Schema.TYPE_STRING;
            case "PEOPLES2":
                return Schema.TYPE_INT;
            case "GRPNAME":
                return Schema.TYPE_STRING;
            case "DISPUTEDFLAG":
                return Schema.TYPE_STRING;
            case "OUTPAYFLAG":
                return Schema.TYPE_STRING;
            case "LANG":
                return Schema.TYPE_STRING;
            case "CURRENCY":
                return Schema.TYPE_STRING;
            case "LOSTTIMES":
                return Schema.TYPE_INT;
            case "PRINTCOUNT":
                return Schema.TYPE_INT;
            case "LASTEDORDATE":
                return Schema.TYPE_STRING;
            case "SPECFLAG":
                return Schema.TYPE_STRING;
            case "SIGNCOM":
                return Schema.TYPE_STRING;
            case "SIGNDATE":
                return Schema.TYPE_STRING;
            case "SIGNTIME":
                return Schema.TYPE_STRING;
            case "CVALIDATE":
                return Schema.TYPE_STRING;
            case "PAYINTV":
                return Schema.TYPE_INT;
            case "MANAGEFEERATE":
                return Schema.TYPE_DOUBLE;
            case "EXPPEOPLES":
                return Schema.TYPE_INT;
            case "EXPPREMIUM":
                return Schema.TYPE_DOUBLE;
            case "EXPAMNT":
                return Schema.TYPE_DOUBLE;
            case "PEOPLES":
                return Schema.TYPE_INT;
            case "MULT":
                return Schema.TYPE_DOUBLE;
            case "PREM":
                return Schema.TYPE_DOUBLE;
            case "AMNT":
                return Schema.TYPE_DOUBLE;
            case "SUMPREM":
                return Schema.TYPE_DOUBLE;
            case "SUMPAY":
                return Schema.TYPE_DOUBLE;
            case "DIF":
                return Schema.TYPE_DOUBLE;
            case "STANDBYFLAG1":
                return Schema.TYPE_STRING;
            case "STANDBYFLAG2":
                return Schema.TYPE_STRING;
            case "STANDBYFLAG3":
                return Schema.TYPE_STRING;
            case "INPUTOPERATOR":
                return Schema.TYPE_STRING;
            case "INPUTDATE":
                return Schema.TYPE_STRING;
            case "INPUTTIME":
                return Schema.TYPE_STRING;
            case "APPROVEFLAG":
                return Schema.TYPE_STRING;
            case "APPROVECODE":
                return Schema.TYPE_STRING;
            case "APPROVEDATE":
                return Schema.TYPE_STRING;
            case "APPROVETIME":
                return Schema.TYPE_STRING;
            case "UWOPERATOR":
                return Schema.TYPE_STRING;
            case "UWFLAG":
                return Schema.TYPE_STRING;
            case "UWDATE":
                return Schema.TYPE_STRING;
            case "UWTIME":
                return Schema.TYPE_STRING;
            case "APPFLAG":
                return Schema.TYPE_STRING;
            case "STATE":
                return Schema.TYPE_STRING;
            case "POLAPPLYDATE":
                return Schema.TYPE_STRING;
            case "CUSTOMGETPOLDATE":
                return Schema.TYPE_STRING;
            case "GETPOLDATE":
                return Schema.TYPE_STRING;
            case "MANAGECOM":
                return Schema.TYPE_STRING;
            case "COMCODE":
                return Schema.TYPE_STRING;
            case "OPERATOR":
                return Schema.TYPE_STRING;
            case "MAKEDATE":
                return Schema.TYPE_STRING;
            case "MAKETIME":
                return Schema.TYPE_STRING;
            case "MODIFYOPERATOR":
                return Schema.TYPE_STRING;
            case "MODIFYDATE":
                return Schema.TYPE_STRING;
            case "MODIFYTIME":
                return Schema.TYPE_STRING;
            case "ENTERKIND":
                return Schema.TYPE_STRING;
            case "AMNTGRADE":
                return Schema.TYPE_STRING;
            case "FIRSTTRIALOPERATOR":
                return Schema.TYPE_STRING;
            case "FIRSTTRIALDATE":
                return Schema.TYPE_STRING;
            case "FIRSTTRIALTIME":
                return Schema.TYPE_STRING;
            case "RECEIVEOPERATOR":
                return Schema.TYPE_STRING;
            case "RECEIVEDATE":
                return Schema.TYPE_STRING;
            case "RECEIVETIME":
                return Schema.TYPE_STRING;
            case "MARKETTYPE":
                return Schema.TYPE_STRING;
            case "REPORTNO":
                return Schema.TYPE_STRING;
            case "EDORCALTYPE":
                return Schema.TYPE_STRING;
            case "RENEWFLAG":
                return Schema.TYPE_STRING;
            case "RENEWCONTNO":
                return Schema.TYPE_STRING;
            case "ENDDATE":
                return Schema.TYPE_STRING;
            case "COINSURANCE":
                return Schema.TYPE_STRING;
            case "CARDTYPECODE":
                return Schema.TYPE_STRING;
            case "RELATIONTOAPPNT":
                return Schema.TYPE_STRING;
            case "NEWREINSUREFLAG":
                return Schema.TYPE_STRING;
            case "INSURERATE":
                return Schema.TYPE_LONG;
            case "AGCAGENTCODE":
                return Schema.TYPE_STRING;
            case "AGCAGENTNAME":
                return Schema.TYPE_STRING;
            case "AVAILIADATEFLAG":
                return Schema.TYPE_STRING;
            case "COMBUSTYPE":
                return Schema.TYPE_STRING;
            case "PRINTFLAG":
                return Schema.TYPE_STRING;
            case "SPECACCEPTFLAG":
                return Schema.TYPE_STRING;
            case "POLICYTYPE":
                return Schema.TYPE_STRING;
            case "ACHVACCRUFLAG":
                return Schema.TYPE_STRING;
            case "REMARK":
                return Schema.TYPE_STRING;
            case "VALDATETYPE":
                return Schema.TYPE_STRING;
            case "SECLEVELOTHER":
                return Schema.TYPE_STRING;
            case "TBTYPE":
                return Schema.TYPE_STRING;
            case "SLIPFORM":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_STRING;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_STRING;
            case 6:
                return Schema.TYPE_STRING;
            case 7:
                return Schema.TYPE_STRING;
            case 8:
                return Schema.TYPE_STRING;
            case 9:
                return Schema.TYPE_STRING;
            case 10:
                return Schema.TYPE_STRING;
            case 11:
                return Schema.TYPE_STRING;
            case 12:
                return Schema.TYPE_STRING;
            case 13:
                return Schema.TYPE_STRING;
            case 14:
                return Schema.TYPE_INT;
            case 15:
                return Schema.TYPE_STRING;
            case 16:
                return Schema.TYPE_STRING;
            case 17:
                return Schema.TYPE_STRING;
            case 18:
                return Schema.TYPE_STRING;
            case 19:
                return Schema.TYPE_STRING;
            case 20:
                return Schema.TYPE_INT;
            case 21:
                return Schema.TYPE_INT;
            case 22:
                return Schema.TYPE_STRING;
            case 23:
                return Schema.TYPE_STRING;
            case 24:
                return Schema.TYPE_STRING;
            case 25:
                return Schema.TYPE_STRING;
            case 26:
                return Schema.TYPE_STRING;
            case 27:
                return Schema.TYPE_STRING;
            case 28:
                return Schema.TYPE_INT;
            case 29:
                return Schema.TYPE_DOUBLE;
            case 30:
                return Schema.TYPE_INT;
            case 31:
                return Schema.TYPE_DOUBLE;
            case 32:
                return Schema.TYPE_DOUBLE;
            case 33:
                return Schema.TYPE_INT;
            case 34:
                return Schema.TYPE_DOUBLE;
            case 35:
                return Schema.TYPE_DOUBLE;
            case 36:
                return Schema.TYPE_DOUBLE;
            case 37:
                return Schema.TYPE_DOUBLE;
            case 38:
                return Schema.TYPE_DOUBLE;
            case 39:
                return Schema.TYPE_DOUBLE;
            case 40:
                return Schema.TYPE_STRING;
            case 41:
                return Schema.TYPE_STRING;
            case 42:
                return Schema.TYPE_STRING;
            case 43:
                return Schema.TYPE_STRING;
            case 44:
                return Schema.TYPE_STRING;
            case 45:
                return Schema.TYPE_STRING;
            case 46:
                return Schema.TYPE_STRING;
            case 47:
                return Schema.TYPE_STRING;
            case 48:
                return Schema.TYPE_STRING;
            case 49:
                return Schema.TYPE_STRING;
            case 50:
                return Schema.TYPE_STRING;
            case 51:
                return Schema.TYPE_STRING;
            case 52:
                return Schema.TYPE_STRING;
            case 53:
                return Schema.TYPE_STRING;
            case 54:
                return Schema.TYPE_STRING;
            case 55:
                return Schema.TYPE_STRING;
            case 56:
                return Schema.TYPE_STRING;
            case 57:
                return Schema.TYPE_STRING;
            case 58:
                return Schema.TYPE_STRING;
            case 59:
                return Schema.TYPE_STRING;
            case 60:
                return Schema.TYPE_STRING;
            case 61:
                return Schema.TYPE_STRING;
            case 62:
                return Schema.TYPE_STRING;
            case 63:
                return Schema.TYPE_STRING;
            case 64:
                return Schema.TYPE_STRING;
            case 65:
                return Schema.TYPE_STRING;
            case 66:
                return Schema.TYPE_STRING;
            case 67:
                return Schema.TYPE_STRING;
            case 68:
                return Schema.TYPE_STRING;
            case 69:
                return Schema.TYPE_STRING;
            case 70:
                return Schema.TYPE_STRING;
            case 71:
                return Schema.TYPE_STRING;
            case 72:
                return Schema.TYPE_STRING;
            case 73:
                return Schema.TYPE_STRING;
            case 74:
                return Schema.TYPE_STRING;
            case 75:
                return Schema.TYPE_STRING;
            case 76:
                return Schema.TYPE_STRING;
            case 77:
                return Schema.TYPE_STRING;
            case 78:
                return Schema.TYPE_STRING;
            case 79:
                return Schema.TYPE_STRING;
            case 80:
                return Schema.TYPE_STRING;
            case 81:
                return Schema.TYPE_STRING;
            case 82:
                return Schema.TYPE_STRING;
            case 83:
                return Schema.TYPE_STRING;
            case 84:
                return Schema.TYPE_STRING;
            case 85:
                return Schema.TYPE_LONG;
            case 86:
                return Schema.TYPE_STRING;
            case 87:
                return Schema.TYPE_STRING;
            case 88:
                return Schema.TYPE_STRING;
            case 89:
                return Schema.TYPE_STRING;
            case 90:
                return Schema.TYPE_STRING;
            case 91:
                return Schema.TYPE_STRING;
            case 92:
                return Schema.TYPE_STRING;
            case 93:
                return Schema.TYPE_STRING;
            case 94:
                return Schema.TYPE_STRING;
            case 95:
                return Schema.TYPE_STRING;
            case 96:
                return Schema.TYPE_STRING;
            case 97:
                return Schema.TYPE_STRING;
            case 98:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("GrpContNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GrpContNo));
        }
        if (FCode.equalsIgnoreCase("ProposalGrpContNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ProposalGrpContNo));
        }
        if (FCode.equalsIgnoreCase("PrtNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PrtNo));
        }
        if (FCode.equalsIgnoreCase("SaleChnl")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SaleChnl));
        }
        if (FCode.equalsIgnoreCase("SalesWay")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SalesWay));
        }
        if (FCode.equalsIgnoreCase("SalesWaySub")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SalesWaySub));
        }
        if (FCode.equalsIgnoreCase("BizNature")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BizNature));
        }
        if (FCode.equalsIgnoreCase("PolIssuPlat")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PolIssuPlat));
        }
        if (FCode.equalsIgnoreCase("ContType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ContType));
        }
        if (FCode.equalsIgnoreCase("AgentCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentCom));
        }
        if (FCode.equalsIgnoreCase("AgentCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentCode));
        }
        if (FCode.equalsIgnoreCase("AgentGroup")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentGroup));
        }
        if (FCode.equalsIgnoreCase("AgentCode1")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentCode1));
        }
        if (FCode.equalsIgnoreCase("AppntNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AppntNo));
        }
        if (FCode.equalsIgnoreCase("Peoples2")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Peoples2));
        }
        if (FCode.equalsIgnoreCase("GrpName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GrpName));
        }
        if (FCode.equalsIgnoreCase("DisputedFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DisputedFlag));
        }
        if (FCode.equalsIgnoreCase("OutPayFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OutPayFlag));
        }
        if (FCode.equalsIgnoreCase("Lang")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Lang));
        }
        if (FCode.equalsIgnoreCase("Currency")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Currency));
        }
        if (FCode.equalsIgnoreCase("LostTimes")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(LostTimes));
        }
        if (FCode.equalsIgnoreCase("PrintCount")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PrintCount));
        }
        if (FCode.equalsIgnoreCase("LastEdorDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(LastEdorDate));
        }
        if (FCode.equalsIgnoreCase("SpecFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SpecFlag));
        }
        if (FCode.equalsIgnoreCase("SignCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SignCom));
        }
        if (FCode.equalsIgnoreCase("SignDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SignDate));
        }
        if (FCode.equalsIgnoreCase("SignTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SignTime));
        }
        if (FCode.equalsIgnoreCase("CValiDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CValiDate));
        }
        if (FCode.equalsIgnoreCase("PayIntv")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PayIntv));
        }
        if (FCode.equalsIgnoreCase("ManageFeeRate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ManageFeeRate));
        }
        if (FCode.equalsIgnoreCase("ExpPeoples")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ExpPeoples));
        }
        if (FCode.equalsIgnoreCase("ExpPremium")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ExpPremium));
        }
        if (FCode.equalsIgnoreCase("ExpAmnt")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ExpAmnt));
        }
        if (FCode.equalsIgnoreCase("Peoples")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Peoples));
        }
        if (FCode.equalsIgnoreCase("Mult")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Mult));
        }
        if (FCode.equalsIgnoreCase("Prem")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Prem));
        }
        if (FCode.equalsIgnoreCase("Amnt")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Amnt));
        }
        if (FCode.equalsIgnoreCase("SumPrem")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SumPrem));
        }
        if (FCode.equalsIgnoreCase("SumPay")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SumPay));
        }
        if (FCode.equalsIgnoreCase("Dif")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Dif));
        }
        if (FCode.equalsIgnoreCase("StandbyFlag1")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StandbyFlag1));
        }
        if (FCode.equalsIgnoreCase("StandbyFlag2")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StandbyFlag2));
        }
        if (FCode.equalsIgnoreCase("StandbyFlag3")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StandbyFlag3));
        }
        if (FCode.equalsIgnoreCase("InputOperator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InputOperator));
        }
        if (FCode.equalsIgnoreCase("InputDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InputDate));
        }
        if (FCode.equalsIgnoreCase("InputTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InputTime));
        }
        if (FCode.equalsIgnoreCase("ApproveFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ApproveFlag));
        }
        if (FCode.equalsIgnoreCase("ApproveCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ApproveCode));
        }
        if (FCode.equalsIgnoreCase("ApproveDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ApproveDate));
        }
        if (FCode.equalsIgnoreCase("ApproveTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ApproveTime));
        }
        if (FCode.equalsIgnoreCase("UWOperator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(UWOperator));
        }
        if (FCode.equalsIgnoreCase("UWFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(UWFlag));
        }
        if (FCode.equalsIgnoreCase("UWDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(UWDate));
        }
        if (FCode.equalsIgnoreCase("UWTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(UWTime));
        }
        if (FCode.equalsIgnoreCase("AppFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AppFlag));
        }
        if (FCode.equalsIgnoreCase("State")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(State));
        }
        if (FCode.equalsIgnoreCase("PolApplyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PolApplyDate));
        }
        if (FCode.equalsIgnoreCase("CustomGetPolDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CustomGetPolDate));
        }
        if (FCode.equalsIgnoreCase("GetPolDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GetPolDate));
        }
        if (FCode.equalsIgnoreCase("ManageCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
        }
        if (FCode.equalsIgnoreCase("ComCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ComCode));
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeDate));
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equalsIgnoreCase("ModifyOperator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyOperator));
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyDate));
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (FCode.equalsIgnoreCase("EnterKind")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(EnterKind));
        }
        if (FCode.equalsIgnoreCase("AmntGrade")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AmntGrade));
        }
        if (FCode.equalsIgnoreCase("FirstTrialOperator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FirstTrialOperator));
        }
        if (FCode.equalsIgnoreCase("FirstTrialDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FirstTrialDate));
        }
        if (FCode.equalsIgnoreCase("FirstTrialTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FirstTrialTime));
        }
        if (FCode.equalsIgnoreCase("ReceiveOperator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ReceiveOperator));
        }
        if (FCode.equalsIgnoreCase("ReceiveDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ReceiveDate));
        }
        if (FCode.equalsIgnoreCase("ReceiveTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ReceiveTime));
        }
        if (FCode.equalsIgnoreCase("MarketType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MarketType));
        }
        if (FCode.equalsIgnoreCase("ReportNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ReportNo));
        }
        if (FCode.equalsIgnoreCase("EdorCalType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(EdorCalType));
        }
        if (FCode.equalsIgnoreCase("RenewFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RenewFlag));
        }
        if (FCode.equalsIgnoreCase("RenewContNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RenewContNo));
        }
        if (FCode.equalsIgnoreCase("EndDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(EndDate));
        }
        if (FCode.equalsIgnoreCase("Coinsurance")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Coinsurance));
        }
        if (FCode.equalsIgnoreCase("CardTypeCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CardTypeCode));
        }
        if (FCode.equalsIgnoreCase("RelationToappnt")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RelationToappnt));
        }
        if (FCode.equalsIgnoreCase("NewReinsureFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(NewReinsureFlag));
        }
        if (FCode.equalsIgnoreCase("InsureRate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InsureRate));
        }
        if (FCode.equalsIgnoreCase("AgcAgentCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgcAgentCode));
        }
        if (FCode.equalsIgnoreCase("AgcAgentName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgcAgentName));
        }
        if (FCode.equalsIgnoreCase("AvailiaDateFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AvailiaDateFlag));
        }
        if (FCode.equalsIgnoreCase("ComBusType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ComBusType));
        }
        if (FCode.equalsIgnoreCase("PrintFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PrintFlag));
        }
        if (FCode.equalsIgnoreCase("SpecAcceptFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SpecAcceptFlag));
        }
        if (FCode.equalsIgnoreCase("PolicyType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PolicyType));
        }
        if (FCode.equalsIgnoreCase("AchvAccruFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AchvAccruFlag));
        }
        if (FCode.equalsIgnoreCase("Remark")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Remark));
        }
        if (FCode.equalsIgnoreCase("ValDateType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ValDateType));
        }
        if (FCode.equalsIgnoreCase("SecLevelOther")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SecLevelOther));
        }
        if (FCode.equalsIgnoreCase("TBType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(TBType));
        }
        if (FCode.equalsIgnoreCase("SlipForm")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SlipForm));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = String.valueOf(GrpContNo);
                break;
            case 1:
                strFieldValue = String.valueOf(ProposalGrpContNo);
                break;
            case 2:
                strFieldValue = String.valueOf(PrtNo);
                break;
            case 3:
                strFieldValue = String.valueOf(SaleChnl);
                break;
            case 4:
                strFieldValue = String.valueOf(SalesWay);
                break;
            case 5:
                strFieldValue = String.valueOf(SalesWaySub);
                break;
            case 6:
                strFieldValue = String.valueOf(BizNature);
                break;
            case 7:
                strFieldValue = String.valueOf(PolIssuPlat);
                break;
            case 8:
                strFieldValue = String.valueOf(ContType);
                break;
            case 9:
                strFieldValue = String.valueOf(AgentCom);
                break;
            case 10:
                strFieldValue = String.valueOf(AgentCode);
                break;
            case 11:
                strFieldValue = String.valueOf(AgentGroup);
                break;
            case 12:
                strFieldValue = String.valueOf(AgentCode1);
                break;
            case 13:
                strFieldValue = String.valueOf(AppntNo);
                break;
            case 14:
                strFieldValue = String.valueOf(Peoples2);
                break;
            case 15:
                strFieldValue = String.valueOf(GrpName);
                break;
            case 16:
                strFieldValue = String.valueOf(DisputedFlag);
                break;
            case 17:
                strFieldValue = String.valueOf(OutPayFlag);
                break;
            case 18:
                strFieldValue = String.valueOf(Lang);
                break;
            case 19:
                strFieldValue = String.valueOf(Currency);
                break;
            case 20:
                strFieldValue = String.valueOf(LostTimes);
                break;
            case 21:
                strFieldValue = String.valueOf(PrintCount);
                break;
            case 22:
                strFieldValue = String.valueOf(LastEdorDate);
                break;
            case 23:
                strFieldValue = String.valueOf(SpecFlag);
                break;
            case 24:
                strFieldValue = String.valueOf(SignCom);
                break;
            case 25:
                strFieldValue = String.valueOf(SignDate);
                break;
            case 26:
                strFieldValue = String.valueOf(SignTime);
                break;
            case 27:
                strFieldValue = String.valueOf(CValiDate);
                break;
            case 28:
                strFieldValue = String.valueOf(PayIntv);
                break;
            case 29:
                strFieldValue = String.valueOf(ManageFeeRate);
                break;
            case 30:
                strFieldValue = String.valueOf(ExpPeoples);
                break;
            case 31:
                strFieldValue = String.valueOf(ExpPremium);
                break;
            case 32:
                strFieldValue = String.valueOf(ExpAmnt);
                break;
            case 33:
                strFieldValue = String.valueOf(Peoples);
                break;
            case 34:
                strFieldValue = String.valueOf(Mult);
                break;
            case 35:
                strFieldValue = String.valueOf(Prem);
                break;
            case 36:
                strFieldValue = String.valueOf(Amnt);
                break;
            case 37:
                strFieldValue = String.valueOf(SumPrem);
                break;
            case 38:
                strFieldValue = String.valueOf(SumPay);
                break;
            case 39:
                strFieldValue = String.valueOf(Dif);
                break;
            case 40:
                strFieldValue = String.valueOf(StandbyFlag1);
                break;
            case 41:
                strFieldValue = String.valueOf(StandbyFlag2);
                break;
            case 42:
                strFieldValue = String.valueOf(StandbyFlag3);
                break;
            case 43:
                strFieldValue = String.valueOf(InputOperator);
                break;
            case 44:
                strFieldValue = String.valueOf(InputDate);
                break;
            case 45:
                strFieldValue = String.valueOf(InputTime);
                break;
            case 46:
                strFieldValue = String.valueOf(ApproveFlag);
                break;
            case 47:
                strFieldValue = String.valueOf(ApproveCode);
                break;
            case 48:
                strFieldValue = String.valueOf(ApproveDate);
                break;
            case 49:
                strFieldValue = String.valueOf(ApproveTime);
                break;
            case 50:
                strFieldValue = String.valueOf(UWOperator);
                break;
            case 51:
                strFieldValue = String.valueOf(UWFlag);
                break;
            case 52:
                strFieldValue = String.valueOf(UWDate);
                break;
            case 53:
                strFieldValue = String.valueOf(UWTime);
                break;
            case 54:
                strFieldValue = String.valueOf(AppFlag);
                break;
            case 55:
                strFieldValue = String.valueOf(State);
                break;
            case 56:
                strFieldValue = String.valueOf(PolApplyDate);
                break;
            case 57:
                strFieldValue = String.valueOf(CustomGetPolDate);
                break;
            case 58:
                strFieldValue = String.valueOf(GetPolDate);
                break;
            case 59:
                strFieldValue = String.valueOf(ManageCom);
                break;
            case 60:
                strFieldValue = String.valueOf(ComCode);
                break;
            case 61:
                strFieldValue = String.valueOf(Operator);
                break;
            case 62:
                strFieldValue = String.valueOf(MakeDate);
                break;
            case 63:
                strFieldValue = String.valueOf(MakeTime);
                break;
            case 64:
                strFieldValue = String.valueOf(ModifyOperator);
                break;
            case 65:
                strFieldValue = String.valueOf(ModifyDate);
                break;
            case 66:
                strFieldValue = String.valueOf(ModifyTime);
                break;
            case 67:
                strFieldValue = String.valueOf(EnterKind);
                break;
            case 68:
                strFieldValue = String.valueOf(AmntGrade);
                break;
            case 69:
                strFieldValue = String.valueOf(FirstTrialOperator);
                break;
            case 70:
                strFieldValue = String.valueOf(FirstTrialDate);
                break;
            case 71:
                strFieldValue = String.valueOf(FirstTrialTime);
                break;
            case 72:
                strFieldValue = String.valueOf(ReceiveOperator);
                break;
            case 73:
                strFieldValue = String.valueOf(ReceiveDate);
                break;
            case 74:
                strFieldValue = String.valueOf(ReceiveTime);
                break;
            case 75:
                strFieldValue = String.valueOf(MarketType);
                break;
            case 76:
                strFieldValue = String.valueOf(ReportNo);
                break;
            case 77:
                strFieldValue = String.valueOf(EdorCalType);
                break;
            case 78:
                strFieldValue = String.valueOf(RenewFlag);
                break;
            case 79:
                strFieldValue = String.valueOf(RenewContNo);
                break;
            case 80:
                strFieldValue = String.valueOf(EndDate);
                break;
            case 81:
                strFieldValue = String.valueOf(Coinsurance);
                break;
            case 82:
                strFieldValue = String.valueOf(CardTypeCode);
                break;
            case 83:
                strFieldValue = String.valueOf(RelationToappnt);
                break;
            case 84:
                strFieldValue = String.valueOf(NewReinsureFlag);
                break;
            case 85:
                strFieldValue = String.valueOf(InsureRate);
                break;
            case 86:
                strFieldValue = String.valueOf(AgcAgentCode);
                break;
            case 87:
                strFieldValue = String.valueOf(AgcAgentName);
                break;
            case 88:
                strFieldValue = String.valueOf(AvailiaDateFlag);
                break;
            case 89:
                strFieldValue = String.valueOf(ComBusType);
                break;
            case 90:
                strFieldValue = String.valueOf(PrintFlag);
                break;
            case 91:
                strFieldValue = String.valueOf(SpecAcceptFlag);
                break;
            case 92:
                strFieldValue = String.valueOf(PolicyType);
                break;
            case 93:
                strFieldValue = String.valueOf(AchvAccruFlag);
                break;
            case 94:
                strFieldValue = String.valueOf(Remark);
                break;
            case 95:
                strFieldValue = String.valueOf(ValDateType);
                break;
            case 96:
                strFieldValue = String.valueOf(SecLevelOther);
                break;
            case 97:
                strFieldValue = String.valueOf(TBType);
                break;
            case 98:
                strFieldValue = String.valueOf(SlipForm);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("GrpContNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                GrpContNo = FValue.trim();
            }
            else
                GrpContNo = null;
        }
        if (FCode.equalsIgnoreCase("ProposalGrpContNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                ProposalGrpContNo = FValue.trim();
            }
            else
                ProposalGrpContNo = null;
        }
        if (FCode.equalsIgnoreCase("PrtNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                PrtNo = FValue.trim();
            }
            else
                PrtNo = null;
        }
        if (FCode.equalsIgnoreCase("SaleChnl")) {
            if( FValue != null && !FValue.equals(""))
            {
                SaleChnl = FValue.trim();
            }
            else
                SaleChnl = null;
        }
        if (FCode.equalsIgnoreCase("SalesWay")) {
            if( FValue != null && !FValue.equals(""))
            {
                SalesWay = FValue.trim();
            }
            else
                SalesWay = null;
        }
        if (FCode.equalsIgnoreCase("SalesWaySub")) {
            if( FValue != null && !FValue.equals(""))
            {
                SalesWaySub = FValue.trim();
            }
            else
                SalesWaySub = null;
        }
        if (FCode.equalsIgnoreCase("BizNature")) {
            if( FValue != null && !FValue.equals(""))
            {
                BizNature = FValue.trim();
            }
            else
                BizNature = null;
        }
        if (FCode.equalsIgnoreCase("PolIssuPlat")) {
            if( FValue != null && !FValue.equals(""))
            {
                PolIssuPlat = FValue.trim();
            }
            else
                PolIssuPlat = null;
        }
        if (FCode.equalsIgnoreCase("ContType")) {
            if( FValue != null && !FValue.equals(""))
            {
                ContType = FValue.trim();
            }
            else
                ContType = null;
        }
        if (FCode.equalsIgnoreCase("AgentCom")) {
            if( FValue != null && !FValue.equals(""))
            {
                AgentCom = FValue.trim();
            }
            else
                AgentCom = null;
        }
        if (FCode.equalsIgnoreCase("AgentCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                AgentCode = FValue.trim();
            }
            else
                AgentCode = null;
        }
        if (FCode.equalsIgnoreCase("AgentGroup")) {
            if( FValue != null && !FValue.equals(""))
            {
                AgentGroup = FValue.trim();
            }
            else
                AgentGroup = null;
        }
        if (FCode.equalsIgnoreCase("AgentCode1")) {
            if( FValue != null && !FValue.equals(""))
            {
                AgentCode1 = FValue.trim();
            }
            else
                AgentCode1 = null;
        }
        if (FCode.equalsIgnoreCase("AppntNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                AppntNo = FValue.trim();
            }
            else
                AppntNo = null;
        }
        if (FCode.equalsIgnoreCase("Peoples2")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                Peoples2 = i;
            }
        }
        if (FCode.equalsIgnoreCase("GrpName")) {
            if( FValue != null && !FValue.equals(""))
            {
                GrpName = FValue.trim();
            }
            else
                GrpName = null;
        }
        if (FCode.equalsIgnoreCase("DisputedFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                DisputedFlag = FValue.trim();
            }
            else
                DisputedFlag = null;
        }
        if (FCode.equalsIgnoreCase("OutPayFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                OutPayFlag = FValue.trim();
            }
            else
                OutPayFlag = null;
        }
        if (FCode.equalsIgnoreCase("Lang")) {
            if( FValue != null && !FValue.equals(""))
            {
                Lang = FValue.trim();
            }
            else
                Lang = null;
        }
        if (FCode.equalsIgnoreCase("Currency")) {
            if( FValue != null && !FValue.equals(""))
            {
                Currency = FValue.trim();
            }
            else
                Currency = null;
        }
        if (FCode.equalsIgnoreCase("LostTimes")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                LostTimes = i;
            }
        }
        if (FCode.equalsIgnoreCase("PrintCount")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                PrintCount = i;
            }
        }
        if (FCode.equalsIgnoreCase("LastEdorDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                LastEdorDate = FValue.trim();
            }
            else
                LastEdorDate = null;
        }
        if (FCode.equalsIgnoreCase("SpecFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                SpecFlag = FValue.trim();
            }
            else
                SpecFlag = null;
        }
        if (FCode.equalsIgnoreCase("SignCom")) {
            if( FValue != null && !FValue.equals(""))
            {
                SignCom = FValue.trim();
            }
            else
                SignCom = null;
        }
        if (FCode.equalsIgnoreCase("SignDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                SignDate = FValue.trim();
            }
            else
                SignDate = null;
        }
        if (FCode.equalsIgnoreCase("SignTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                SignTime = FValue.trim();
            }
            else
                SignTime = null;
        }
        if (FCode.equalsIgnoreCase("CValiDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                CValiDate = FValue.trim();
            }
            else
                CValiDate = null;
        }
        if (FCode.equalsIgnoreCase("PayIntv")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                PayIntv = i;
            }
        }
        if (FCode.equalsIgnoreCase("ManageFeeRate")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                ManageFeeRate = d;
            }
        }
        if (FCode.equalsIgnoreCase("ExpPeoples")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                ExpPeoples = i;
            }
        }
        if (FCode.equalsIgnoreCase("ExpPremium")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                ExpPremium = d;
            }
        }
        if (FCode.equalsIgnoreCase("ExpAmnt")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                ExpAmnt = d;
            }
        }
        if (FCode.equalsIgnoreCase("Peoples")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                Peoples = i;
            }
        }
        if (FCode.equalsIgnoreCase("Mult")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                Mult = d;
            }
        }
        if (FCode.equalsIgnoreCase("Prem")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                Prem = d;
            }
        }
        if (FCode.equalsIgnoreCase("Amnt")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                Amnt = d;
            }
        }
        if (FCode.equalsIgnoreCase("SumPrem")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                SumPrem = d;
            }
        }
        if (FCode.equalsIgnoreCase("SumPay")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                SumPay = d;
            }
        }
        if (FCode.equalsIgnoreCase("Dif")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                Dif = d;
            }
        }
        if (FCode.equalsIgnoreCase("StandbyFlag1")) {
            if( FValue != null && !FValue.equals(""))
            {
                StandbyFlag1 = FValue.trim();
            }
            else
                StandbyFlag1 = null;
        }
        if (FCode.equalsIgnoreCase("StandbyFlag2")) {
            if( FValue != null && !FValue.equals(""))
            {
                StandbyFlag2 = FValue.trim();
            }
            else
                StandbyFlag2 = null;
        }
        if (FCode.equalsIgnoreCase("StandbyFlag3")) {
            if( FValue != null && !FValue.equals(""))
            {
                StandbyFlag3 = FValue.trim();
            }
            else
                StandbyFlag3 = null;
        }
        if (FCode.equalsIgnoreCase("InputOperator")) {
            if( FValue != null && !FValue.equals(""))
            {
                InputOperator = FValue.trim();
            }
            else
                InputOperator = null;
        }
        if (FCode.equalsIgnoreCase("InputDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                InputDate = FValue.trim();
            }
            else
                InputDate = null;
        }
        if (FCode.equalsIgnoreCase("InputTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                InputTime = FValue.trim();
            }
            else
                InputTime = null;
        }
        if (FCode.equalsIgnoreCase("ApproveFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                ApproveFlag = FValue.trim();
            }
            else
                ApproveFlag = null;
        }
        if (FCode.equalsIgnoreCase("ApproveCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                ApproveCode = FValue.trim();
            }
            else
                ApproveCode = null;
        }
        if (FCode.equalsIgnoreCase("ApproveDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                ApproveDate = FValue.trim();
            }
            else
                ApproveDate = null;
        }
        if (FCode.equalsIgnoreCase("ApproveTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                ApproveTime = FValue.trim();
            }
            else
                ApproveTime = null;
        }
        if (FCode.equalsIgnoreCase("UWOperator")) {
            if( FValue != null && !FValue.equals(""))
            {
                UWOperator = FValue.trim();
            }
            else
                UWOperator = null;
        }
        if (FCode.equalsIgnoreCase("UWFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                UWFlag = FValue.trim();
            }
            else
                UWFlag = null;
        }
        if (FCode.equalsIgnoreCase("UWDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                UWDate = FValue.trim();
            }
            else
                UWDate = null;
        }
        if (FCode.equalsIgnoreCase("UWTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                UWTime = FValue.trim();
            }
            else
                UWTime = null;
        }
        if (FCode.equalsIgnoreCase("AppFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                AppFlag = FValue.trim();
            }
            else
                AppFlag = null;
        }
        if (FCode.equalsIgnoreCase("State")) {
            if( FValue != null && !FValue.equals(""))
            {
                State = FValue.trim();
            }
            else
                State = null;
        }
        if (FCode.equalsIgnoreCase("PolApplyDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                PolApplyDate = FValue.trim();
            }
            else
                PolApplyDate = null;
        }
        if (FCode.equalsIgnoreCase("CustomGetPolDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                CustomGetPolDate = FValue.trim();
            }
            else
                CustomGetPolDate = null;
        }
        if (FCode.equalsIgnoreCase("GetPolDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                GetPolDate = FValue.trim();
            }
            else
                GetPolDate = null;
        }
        if (FCode.equalsIgnoreCase("ManageCom")) {
            if( FValue != null && !FValue.equals(""))
            {
                ManageCom = FValue.trim();
            }
            else
                ManageCom = null;
        }
        if (FCode.equalsIgnoreCase("ComCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                ComCode = FValue.trim();
            }
            else
                ComCode = null;
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            if( FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
                Operator = null;
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeDate = FValue.trim();
            }
            else
                MakeDate = null;
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
                MakeTime = null;
        }
        if (FCode.equalsIgnoreCase("ModifyOperator")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyOperator = FValue.trim();
            }
            else
                ModifyOperator = null;
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyDate = FValue.trim();
            }
            else
                ModifyDate = null;
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
                ModifyTime = null;
        }
        if (FCode.equalsIgnoreCase("EnterKind")) {
            if( FValue != null && !FValue.equals(""))
            {
                EnterKind = FValue.trim();
            }
            else
                EnterKind = null;
        }
        if (FCode.equalsIgnoreCase("AmntGrade")) {
            if( FValue != null && !FValue.equals(""))
            {
                AmntGrade = FValue.trim();
            }
            else
                AmntGrade = null;
        }
        if (FCode.equalsIgnoreCase("FirstTrialOperator")) {
            if( FValue != null && !FValue.equals(""))
            {
                FirstTrialOperator = FValue.trim();
            }
            else
                FirstTrialOperator = null;
        }
        if (FCode.equalsIgnoreCase("FirstTrialDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                FirstTrialDate = FValue.trim();
            }
            else
                FirstTrialDate = null;
        }
        if (FCode.equalsIgnoreCase("FirstTrialTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                FirstTrialTime = FValue.trim();
            }
            else
                FirstTrialTime = null;
        }
        if (FCode.equalsIgnoreCase("ReceiveOperator")) {
            if( FValue != null && !FValue.equals(""))
            {
                ReceiveOperator = FValue.trim();
            }
            else
                ReceiveOperator = null;
        }
        if (FCode.equalsIgnoreCase("ReceiveDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                ReceiveDate = FValue.trim();
            }
            else
                ReceiveDate = null;
        }
        if (FCode.equalsIgnoreCase("ReceiveTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                ReceiveTime = FValue.trim();
            }
            else
                ReceiveTime = null;
        }
        if (FCode.equalsIgnoreCase("MarketType")) {
            if( FValue != null && !FValue.equals(""))
            {
                MarketType = FValue.trim();
            }
            else
                MarketType = null;
        }
        if (FCode.equalsIgnoreCase("ReportNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                ReportNo = FValue.trim();
            }
            else
                ReportNo = null;
        }
        if (FCode.equalsIgnoreCase("EdorCalType")) {
            if( FValue != null && !FValue.equals(""))
            {
                EdorCalType = FValue.trim();
            }
            else
                EdorCalType = null;
        }
        if (FCode.equalsIgnoreCase("RenewFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                RenewFlag = FValue.trim();
            }
            else
                RenewFlag = null;
        }
        if (FCode.equalsIgnoreCase("RenewContNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                RenewContNo = FValue.trim();
            }
            else
                RenewContNo = null;
        }
        if (FCode.equalsIgnoreCase("EndDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                EndDate = FValue.trim();
            }
            else
                EndDate = null;
        }
        if (FCode.equalsIgnoreCase("Coinsurance")) {
            if( FValue != null && !FValue.equals(""))
            {
                Coinsurance = FValue.trim();
            }
            else
                Coinsurance = null;
        }
        if (FCode.equalsIgnoreCase("CardTypeCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                CardTypeCode = FValue.trim();
            }
            else
                CardTypeCode = null;
        }
        if (FCode.equalsIgnoreCase("RelationToappnt")) {
            if( FValue != null && !FValue.equals(""))
            {
                RelationToappnt = FValue.trim();
            }
            else
                RelationToappnt = null;
        }
        if (FCode.equalsIgnoreCase("NewReinsureFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                NewReinsureFlag = FValue.trim();
            }
            else
                NewReinsureFlag = null;
        }
        if (FCode.equalsIgnoreCase("InsureRate")) {
            if( FValue != null && !FValue.equals("")) {
                InsureRate = new Long(FValue).longValue();
            }
        }
        if (FCode.equalsIgnoreCase("AgcAgentCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                AgcAgentCode = FValue.trim();
            }
            else
                AgcAgentCode = null;
        }
        if (FCode.equalsIgnoreCase("AgcAgentName")) {
            if( FValue != null && !FValue.equals(""))
            {
                AgcAgentName = FValue.trim();
            }
            else
                AgcAgentName = null;
        }
        if (FCode.equalsIgnoreCase("AvailiaDateFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                AvailiaDateFlag = FValue.trim();
            }
            else
                AvailiaDateFlag = null;
        }
        if (FCode.equalsIgnoreCase("ComBusType")) {
            if( FValue != null && !FValue.equals(""))
            {
                ComBusType = FValue.trim();
            }
            else
                ComBusType = null;
        }
        if (FCode.equalsIgnoreCase("PrintFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                PrintFlag = FValue.trim();
            }
            else
                PrintFlag = null;
        }
        if (FCode.equalsIgnoreCase("SpecAcceptFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                SpecAcceptFlag = FValue.trim();
            }
            else
                SpecAcceptFlag = null;
        }
        if (FCode.equalsIgnoreCase("PolicyType")) {
            if( FValue != null && !FValue.equals(""))
            {
                PolicyType = FValue.trim();
            }
            else
                PolicyType = null;
        }
        if (FCode.equalsIgnoreCase("AchvAccruFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                AchvAccruFlag = FValue.trim();
            }
            else
                AchvAccruFlag = null;
        }
        if (FCode.equalsIgnoreCase("Remark")) {
            if( FValue != null && !FValue.equals(""))
            {
                Remark = FValue.trim();
            }
            else
                Remark = null;
        }
        if (FCode.equalsIgnoreCase("ValDateType")) {
            if( FValue != null && !FValue.equals(""))
            {
                ValDateType = FValue.trim();
            }
            else
                ValDateType = null;
        }
        if (FCode.equalsIgnoreCase("SecLevelOther")) {
            if( FValue != null && !FValue.equals(""))
            {
                SecLevelOther = FValue.trim();
            }
            else
                SecLevelOther = null;
        }
        if (FCode.equalsIgnoreCase("TBType")) {
            if( FValue != null && !FValue.equals(""))
            {
                TBType = FValue.trim();
            }
            else
                TBType = null;
        }
        if (FCode.equalsIgnoreCase("SlipForm")) {
            if( FValue != null && !FValue.equals(""))
            {
                SlipForm = FValue.trim();
            }
            else
                SlipForm = null;
        }
        return true;
    }


    public String toString() {
    return "LBGrpContPojo [" +
            "GrpContNo="+GrpContNo +
            ", ProposalGrpContNo="+ProposalGrpContNo +
            ", PrtNo="+PrtNo +
            ", SaleChnl="+SaleChnl +
            ", SalesWay="+SalesWay +
            ", SalesWaySub="+SalesWaySub +
            ", BizNature="+BizNature +
            ", PolIssuPlat="+PolIssuPlat +
            ", ContType="+ContType +
            ", AgentCom="+AgentCom +
            ", AgentCode="+AgentCode +
            ", AgentGroup="+AgentGroup +
            ", AgentCode1="+AgentCode1 +
            ", AppntNo="+AppntNo +
            ", Peoples2="+Peoples2 +
            ", GrpName="+GrpName +
            ", DisputedFlag="+DisputedFlag +
            ", OutPayFlag="+OutPayFlag +
            ", Lang="+Lang +
            ", Currency="+Currency +
            ", LostTimes="+LostTimes +
            ", PrintCount="+PrintCount +
            ", LastEdorDate="+LastEdorDate +
            ", SpecFlag="+SpecFlag +
            ", SignCom="+SignCom +
            ", SignDate="+SignDate +
            ", SignTime="+SignTime +
            ", CValiDate="+CValiDate +
            ", PayIntv="+PayIntv +
            ", ManageFeeRate="+ManageFeeRate +
            ", ExpPeoples="+ExpPeoples +
            ", ExpPremium="+ExpPremium +
            ", ExpAmnt="+ExpAmnt +
            ", Peoples="+Peoples +
            ", Mult="+Mult +
            ", Prem="+Prem +
            ", Amnt="+Amnt +
            ", SumPrem="+SumPrem +
            ", SumPay="+SumPay +
            ", Dif="+Dif +
            ", StandbyFlag1="+StandbyFlag1 +
            ", StandbyFlag2="+StandbyFlag2 +
            ", StandbyFlag3="+StandbyFlag3 +
            ", InputOperator="+InputOperator +
            ", InputDate="+InputDate +
            ", InputTime="+InputTime +
            ", ApproveFlag="+ApproveFlag +
            ", ApproveCode="+ApproveCode +
            ", ApproveDate="+ApproveDate +
            ", ApproveTime="+ApproveTime +
            ", UWOperator="+UWOperator +
            ", UWFlag="+UWFlag +
            ", UWDate="+UWDate +
            ", UWTime="+UWTime +
            ", AppFlag="+AppFlag +
            ", State="+State +
            ", PolApplyDate="+PolApplyDate +
            ", CustomGetPolDate="+CustomGetPolDate +
            ", GetPolDate="+GetPolDate +
            ", ManageCom="+ManageCom +
            ", ComCode="+ComCode +
            ", Operator="+Operator +
            ", MakeDate="+MakeDate +
            ", MakeTime="+MakeTime +
            ", ModifyOperator="+ModifyOperator +
            ", ModifyDate="+ModifyDate +
            ", ModifyTime="+ModifyTime +
            ", EnterKind="+EnterKind +
            ", AmntGrade="+AmntGrade +
            ", FirstTrialOperator="+FirstTrialOperator +
            ", FirstTrialDate="+FirstTrialDate +
            ", FirstTrialTime="+FirstTrialTime +
            ", ReceiveOperator="+ReceiveOperator +
            ", ReceiveDate="+ReceiveDate +
            ", ReceiveTime="+ReceiveTime +
            ", MarketType="+MarketType +
            ", ReportNo="+ReportNo +
            ", EdorCalType="+EdorCalType +
            ", RenewFlag="+RenewFlag +
            ", RenewContNo="+RenewContNo +
            ", EndDate="+EndDate +
            ", Coinsurance="+Coinsurance +
            ", CardTypeCode="+CardTypeCode +
            ", RelationToappnt="+RelationToappnt +
            ", NewReinsureFlag="+NewReinsureFlag +
            ", InsureRate="+InsureRate +
            ", AgcAgentCode="+AgcAgentCode +
            ", AgcAgentName="+AgcAgentName +
            ", AvailiaDateFlag="+AvailiaDateFlag +
            ", ComBusType="+ComBusType +
            ", PrintFlag="+PrintFlag +
            ", SpecAcceptFlag="+SpecAcceptFlag +
            ", PolicyType="+PolicyType +
            ", AchvAccruFlag="+AchvAccruFlag +
            ", Remark="+Remark +
            ", ValDateType="+ValDateType +
            ", SecLevelOther="+SecLevelOther +
            ", TBType="+TBType +
            ", SlipForm="+SlipForm +"]";
    }
}
