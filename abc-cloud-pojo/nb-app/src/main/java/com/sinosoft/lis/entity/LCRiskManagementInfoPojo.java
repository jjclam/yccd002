/**
 * Copyright (c) 2019 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.entity;

import java.io.*;
import java.util.Date;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.StrTool;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * <p>ClassName: LCRiskManagementInfoPojo </p>
 * <p>Copyright: Copyright (c) 2019</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2019-11-14
 */
public class LCRiskManagementInfoPojo implements  Pojo,Serializable {
    // @Field
    /** 业务流水号 */
    private String SerialNo; 
    /** 账户号 */
    private String AccNo; 
    /** 保单号 */
    private String ContNo; 
    /** 投保单号 */
    private String PrtNo; 
    /** 赔案号 */
    private String CaseNo; 
    /** 产品编码 */
    private String RiskCode; 
    /** 被保人姓名 */
    private String InsuredName; 
    /** 被保人证件类型 */
    private String InsuredIDType; 
    /** 被保人证件号码 */
    private String InsuredIDNo; 
    /** 保险公司机构编码 */
    private String CompanyCode; 
    /** 所属业务 */
    private String Business; 
    /** 风险类型 */
    private String RiskType; 
    /** 授权标志 */
    private String CustomerAllowed; 
    /** 存在多公司承保 */
    private String MultiCompany; 
    /** 存在重疾理赔史 */
    private String MajorDiseasePayment; 
    /** 存在伤残理赔史 */
    private String Disability; 
    /** 存在密集投保 */
    private String Dense; 
    /** 累计保额提示 */
    private String AccumulativeMoney; 
    /** 网页查询码 */
    private String PageQueryCode; 
    /** 数据截止日期 */
    private String TagDate; 
    /** 是否有网页数据 */
    private String DisplayPage; 
    /** 存在非正常核保结论 */
    private String AbnormalCheck; 
    /** 存在非正常理赔结论 */
    private String AbnormalPayment; 
    /** 存在慢性病理赔史 */
    private String ChronicDiseasePayment; 
    /** 累计赔付总金额 */
    private String PayMoney; 
    /** 有效保单累计日额提示 */
    private String DayMoney; 
    /** 一年内津贴赔付次数提示 */
    private String CountOneYear; 
    /** 一年内总赔付次数提示 */
    private String PaymentCountOneYear; 
    /** 投保前理赔情况提示 */
    private String Paymented; 
    /** 理赔次数风险提示 */
    private String PaymentCount; 
    /** 有效保单的累计日额提示 */
    private String DayMoneySum; 
    /** 存在重疾保额 */
    private String MajorDiseaseMoney; 
    /** 累计赔付总次数 */
    private String PayCount; 
    /** 累计津贴险赔付天数提示 */
    private String PaymentDayCount; 
    /** 该收据号是否有其它赔案 */
    private String Paymented2; 
    /** 收据编号 */
    private String ReceiptCode; 
    /** 医院代码 */
    private String HospitalCode; 
    /** 操作人 */
    private String Operator; 
    /** 入机日期 */
    private String  MakeDate;
    /** 入机时间 */
    private String MakeTime; 
    /** 修改日期 */
    private String  ModifyDate;
    /** 修改时间 */
    private String ModifyTime; 
    /** 查询流水号 */
    private String InsurerUuid; 


    public static final int FIELDNUM = 43;    // 数据库表的字段个数

    private FDate fDate = new FDate();        // 处理日期
    public String getSerialNo() {
        return SerialNo;
    }
    public void setSerialNo(String aSerialNo) {
        SerialNo = aSerialNo;
    }
    public String getAccNo() {
        return AccNo;
    }
    public void setAccNo(String aAccNo) {
        AccNo = aAccNo;
    }
    public String getContNo() {
        return ContNo;
    }
    public void setContNo(String aContNo) {
        ContNo = aContNo;
    }
    public String getPrtNo() {
        return PrtNo;
    }
    public void setPrtNo(String aPrtNo) {
        PrtNo = aPrtNo;
    }
    public String getCaseNo() {
        return CaseNo;
    }
    public void setCaseNo(String aCaseNo) {
        CaseNo = aCaseNo;
    }
    public String getRiskCode() {
        return RiskCode;
    }
    public void setRiskCode(String aRiskCode) {
        RiskCode = aRiskCode;
    }
    public String getInsuredName() {
        return InsuredName;
    }
    public void setInsuredName(String aInsuredName) {
        InsuredName = aInsuredName;
    }
    public String getInsuredIDType() {
        return InsuredIDType;
    }
    public void setInsuredIDType(String aInsuredIDType) {
        InsuredIDType = aInsuredIDType;
    }
    public String getInsuredIDNo() {
        return InsuredIDNo;
    }
    public void setInsuredIDNo(String aInsuredIDNo) {
        InsuredIDNo = aInsuredIDNo;
    }
    public String getCompanyCode() {
        return CompanyCode;
    }
    public void setCompanyCode(String aCompanyCode) {
        CompanyCode = aCompanyCode;
    }
    public String getBusiness() {
        return Business;
    }
    public void setBusiness(String aBusiness) {
        Business = aBusiness;
    }
    public String getRiskType() {
        return RiskType;
    }
    public void setRiskType(String aRiskType) {
        RiskType = aRiskType;
    }
    public String getCustomerAllowed() {
        return CustomerAllowed;
    }
    public void setCustomerAllowed(String aCustomerAllowed) {
        CustomerAllowed = aCustomerAllowed;
    }
    public String getMultiCompany() {
        return MultiCompany;
    }
    public void setMultiCompany(String aMultiCompany) {
        MultiCompany = aMultiCompany;
    }
    public String getMajorDiseasePayment() {
        return MajorDiseasePayment;
    }
    public void setMajorDiseasePayment(String aMajorDiseasePayment) {
        MajorDiseasePayment = aMajorDiseasePayment;
    }
    public String getDisability() {
        return Disability;
    }
    public void setDisability(String aDisability) {
        Disability = aDisability;
    }
    public String getDense() {
        return Dense;
    }
    public void setDense(String aDense) {
        Dense = aDense;
    }
    public String getAccumulativeMoney() {
        return AccumulativeMoney;
    }
    public void setAccumulativeMoney(String aAccumulativeMoney) {
        AccumulativeMoney = aAccumulativeMoney;
    }
    public String getPageQueryCode() {
        return PageQueryCode;
    }
    public void setPageQueryCode(String aPageQueryCode) {
        PageQueryCode = aPageQueryCode;
    }
    public String getTagDate() {
        return TagDate;
    }
    public void setTagDate(String aTagDate) {
        TagDate = aTagDate;
    }
    public String getDisplayPage() {
        return DisplayPage;
    }
    public void setDisplayPage(String aDisplayPage) {
        DisplayPage = aDisplayPage;
    }
    public String getAbnormalCheck() {
        return AbnormalCheck;
    }
    public void setAbnormalCheck(String aAbnormalCheck) {
        AbnormalCheck = aAbnormalCheck;
    }
    public String getAbnormalPayment() {
        return AbnormalPayment;
    }
    public void setAbnormalPayment(String aAbnormalPayment) {
        AbnormalPayment = aAbnormalPayment;
    }
    public String getChronicDiseasePayment() {
        return ChronicDiseasePayment;
    }
    public void setChronicDiseasePayment(String aChronicDiseasePayment) {
        ChronicDiseasePayment = aChronicDiseasePayment;
    }
    public String getPayMoney() {
        return PayMoney;
    }
    public void setPayMoney(String aPayMoney) {
        PayMoney = aPayMoney;
    }
    public String getDayMoney() {
        return DayMoney;
    }
    public void setDayMoney(String aDayMoney) {
        DayMoney = aDayMoney;
    }
    public String getCountOneYear() {
        return CountOneYear;
    }
    public void setCountOneYear(String aCountOneYear) {
        CountOneYear = aCountOneYear;
    }
    public String getPaymentCountOneYear() {
        return PaymentCountOneYear;
    }
    public void setPaymentCountOneYear(String aPaymentCountOneYear) {
        PaymentCountOneYear = aPaymentCountOneYear;
    }
    public String getPaymented() {
        return Paymented;
    }
    public void setPaymented(String aPaymented) {
        Paymented = aPaymented;
    }
    public String getPaymentCount() {
        return PaymentCount;
    }
    public void setPaymentCount(String aPaymentCount) {
        PaymentCount = aPaymentCount;
    }
    public String getDayMoneySum() {
        return DayMoneySum;
    }
    public void setDayMoneySum(String aDayMoneySum) {
        DayMoneySum = aDayMoneySum;
    }
    public String getMajorDiseaseMoney() {
        return MajorDiseaseMoney;
    }
    public void setMajorDiseaseMoney(String aMajorDiseaseMoney) {
        MajorDiseaseMoney = aMajorDiseaseMoney;
    }
    public String getPayCount() {
        return PayCount;
    }
    public void setPayCount(String aPayCount) {
        PayCount = aPayCount;
    }
    public String getPaymentDayCount() {
        return PaymentDayCount;
    }
    public void setPaymentDayCount(String aPaymentDayCount) {
        PaymentDayCount = aPaymentDayCount;
    }
    public String getPaymented2() {
        return Paymented2;
    }
    public void setPaymented2(String aPaymented2) {
        Paymented2 = aPaymented2;
    }
    public String getReceiptCode() {
        return ReceiptCode;
    }
    public void setReceiptCode(String aReceiptCode) {
        ReceiptCode = aReceiptCode;
    }
    public String getHospitalCode() {
        return HospitalCode;
    }
    public void setHospitalCode(String aHospitalCode) {
        HospitalCode = aHospitalCode;
    }
    public String getOperator() {
        return Operator;
    }
    public void setOperator(String aOperator) {
        Operator = aOperator;
    }
    public String getMakeDate() {
        return MakeDate;
    }
    public void setMakeDate(String aMakeDate) {
        MakeDate = aMakeDate;
    }
    public String getMakeTime() {
        return MakeTime;
    }
    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }
    public String getModifyDate() {
        return ModifyDate;
    }
    public void setModifyDate(String aModifyDate) {
        ModifyDate = aModifyDate;
    }
    public String getModifyTime() {
        return ModifyTime;
    }
    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }
    public String getInsurerUuid() {
        return InsurerUuid;
    }
    public void setInsurerUuid(String aInsurerUuid) {
        InsurerUuid = aInsurerUuid;
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("SerialNo") ) {
            return 0;
        }
        if( strFieldName.equals("AccNo") ) {
            return 1;
        }
        if( strFieldName.equals("ContNo") ) {
            return 2;
        }
        if( strFieldName.equals("PrtNo") ) {
            return 3;
        }
        if( strFieldName.equals("CaseNo") ) {
            return 4;
        }
        if( strFieldName.equals("RiskCode") ) {
            return 5;
        }
        if( strFieldName.equals("InsuredName") ) {
            return 6;
        }
        if( strFieldName.equals("InsuredIDType") ) {
            return 7;
        }
        if( strFieldName.equals("InsuredIDNo") ) {
            return 8;
        }
        if( strFieldName.equals("CompanyCode") ) {
            return 9;
        }
        if( strFieldName.equals("Business") ) {
            return 10;
        }
        if( strFieldName.equals("RiskType") ) {
            return 11;
        }
        if( strFieldName.equals("CustomerAllowed") ) {
            return 12;
        }
        if( strFieldName.equals("MultiCompany") ) {
            return 13;
        }
        if( strFieldName.equals("MajorDiseasePayment") ) {
            return 14;
        }
        if( strFieldName.equals("Disability") ) {
            return 15;
        }
        if( strFieldName.equals("Dense") ) {
            return 16;
        }
        if( strFieldName.equals("AccumulativeMoney") ) {
            return 17;
        }
        if( strFieldName.equals("PageQueryCode") ) {
            return 18;
        }
        if( strFieldName.equals("TagDate") ) {
            return 19;
        }
        if( strFieldName.equals("DisplayPage") ) {
            return 20;
        }
        if( strFieldName.equals("AbnormalCheck") ) {
            return 21;
        }
        if( strFieldName.equals("AbnormalPayment") ) {
            return 22;
        }
        if( strFieldName.equals("ChronicDiseasePayment") ) {
            return 23;
        }
        if( strFieldName.equals("PayMoney") ) {
            return 24;
        }
        if( strFieldName.equals("DayMoney") ) {
            return 25;
        }
        if( strFieldName.equals("CountOneYear") ) {
            return 26;
        }
        if( strFieldName.equals("PaymentCountOneYear") ) {
            return 27;
        }
        if( strFieldName.equals("Paymented") ) {
            return 28;
        }
        if( strFieldName.equals("PaymentCount") ) {
            return 29;
        }
        if( strFieldName.equals("DayMoneySum") ) {
            return 30;
        }
        if( strFieldName.equals("MajorDiseaseMoney") ) {
            return 31;
        }
        if( strFieldName.equals("PayCount") ) {
            return 32;
        }
        if( strFieldName.equals("PaymentDayCount") ) {
            return 33;
        }
        if( strFieldName.equals("Paymented2") ) {
            return 34;
        }
        if( strFieldName.equals("ReceiptCode") ) {
            return 35;
        }
        if( strFieldName.equals("HospitalCode") ) {
            return 36;
        }
        if( strFieldName.equals("Operator") ) {
            return 37;
        }
        if( strFieldName.equals("MakeDate") ) {
            return 38;
        }
        if( strFieldName.equals("MakeTime") ) {
            return 39;
        }
        if( strFieldName.equals("ModifyDate") ) {
            return 40;
        }
        if( strFieldName.equals("ModifyTime") ) {
            return 41;
        }
        if( strFieldName.equals("InsurerUuid") ) {
            return 42;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "SerialNo";
                break;
            case 1:
                strFieldName = "AccNo";
                break;
            case 2:
                strFieldName = "ContNo";
                break;
            case 3:
                strFieldName = "PrtNo";
                break;
            case 4:
                strFieldName = "CaseNo";
                break;
            case 5:
                strFieldName = "RiskCode";
                break;
            case 6:
                strFieldName = "InsuredName";
                break;
            case 7:
                strFieldName = "InsuredIDType";
                break;
            case 8:
                strFieldName = "InsuredIDNo";
                break;
            case 9:
                strFieldName = "CompanyCode";
                break;
            case 10:
                strFieldName = "Business";
                break;
            case 11:
                strFieldName = "RiskType";
                break;
            case 12:
                strFieldName = "CustomerAllowed";
                break;
            case 13:
                strFieldName = "MultiCompany";
                break;
            case 14:
                strFieldName = "MajorDiseasePayment";
                break;
            case 15:
                strFieldName = "Disability";
                break;
            case 16:
                strFieldName = "Dense";
                break;
            case 17:
                strFieldName = "AccumulativeMoney";
                break;
            case 18:
                strFieldName = "PageQueryCode";
                break;
            case 19:
                strFieldName = "TagDate";
                break;
            case 20:
                strFieldName = "DisplayPage";
                break;
            case 21:
                strFieldName = "AbnormalCheck";
                break;
            case 22:
                strFieldName = "AbnormalPayment";
                break;
            case 23:
                strFieldName = "ChronicDiseasePayment";
                break;
            case 24:
                strFieldName = "PayMoney";
                break;
            case 25:
                strFieldName = "DayMoney";
                break;
            case 26:
                strFieldName = "CountOneYear";
                break;
            case 27:
                strFieldName = "PaymentCountOneYear";
                break;
            case 28:
                strFieldName = "Paymented";
                break;
            case 29:
                strFieldName = "PaymentCount";
                break;
            case 30:
                strFieldName = "DayMoneySum";
                break;
            case 31:
                strFieldName = "MajorDiseaseMoney";
                break;
            case 32:
                strFieldName = "PayCount";
                break;
            case 33:
                strFieldName = "PaymentDayCount";
                break;
            case 34:
                strFieldName = "Paymented2";
                break;
            case 35:
                strFieldName = "ReceiptCode";
                break;
            case 36:
                strFieldName = "HospitalCode";
                break;
            case 37:
                strFieldName = "Operator";
                break;
            case 38:
                strFieldName = "MakeDate";
                break;
            case 39:
                strFieldName = "MakeTime";
                break;
            case 40:
                strFieldName = "ModifyDate";
                break;
            case 41:
                strFieldName = "ModifyTime";
                break;
            case 42:
                strFieldName = "InsurerUuid";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "SERIALNO":
                return Schema.TYPE_STRING;
            case "ACCNO":
                return Schema.TYPE_STRING;
            case "CONTNO":
                return Schema.TYPE_STRING;
            case "PRTNO":
                return Schema.TYPE_STRING;
            case "CASENO":
                return Schema.TYPE_STRING;
            case "RISKCODE":
                return Schema.TYPE_STRING;
            case "INSUREDNAME":
                return Schema.TYPE_STRING;
            case "INSUREDIDTYPE":
                return Schema.TYPE_STRING;
            case "INSUREDIDNO":
                return Schema.TYPE_STRING;
            case "COMPANYCODE":
                return Schema.TYPE_STRING;
            case "BUSINESS":
                return Schema.TYPE_STRING;
            case "RISKTYPE":
                return Schema.TYPE_STRING;
            case "CUSTOMERALLOWED":
                return Schema.TYPE_STRING;
            case "MULTICOMPANY":
                return Schema.TYPE_STRING;
            case "MAJORDISEASEPAYMENT":
                return Schema.TYPE_STRING;
            case "DISABILITY":
                return Schema.TYPE_STRING;
            case "DENSE":
                return Schema.TYPE_STRING;
            case "ACCUMULATIVEMONEY":
                return Schema.TYPE_STRING;
            case "PAGEQUERYCODE":
                return Schema.TYPE_STRING;
            case "TAGDATE":
                return Schema.TYPE_STRING;
            case "DISPLAYPAGE":
                return Schema.TYPE_STRING;
            case "ABNORMALCHECK":
                return Schema.TYPE_STRING;
            case "ABNORMALPAYMENT":
                return Schema.TYPE_STRING;
            case "CHRONICDISEASEPAYMENT":
                return Schema.TYPE_STRING;
            case "PAYMONEY":
                return Schema.TYPE_STRING;
            case "DAYMONEY":
                return Schema.TYPE_STRING;
            case "COUNTONEYEAR":
                return Schema.TYPE_STRING;
            case "PAYMENTCOUNTONEYEAR":
                return Schema.TYPE_STRING;
            case "PAYMENTED":
                return Schema.TYPE_STRING;
            case "PAYMENTCOUNT":
                return Schema.TYPE_STRING;
            case "DAYMONEYSUM":
                return Schema.TYPE_STRING;
            case "MAJORDISEASEMONEY":
                return Schema.TYPE_STRING;
            case "PAYCOUNT":
                return Schema.TYPE_STRING;
            case "PAYMENTDAYCOUNT":
                return Schema.TYPE_STRING;
            case "PAYMENTED2":
                return Schema.TYPE_STRING;
            case "RECEIPTCODE":
                return Schema.TYPE_STRING;
            case "HOSPITALCODE":
                return Schema.TYPE_STRING;
            case "OPERATOR":
                return Schema.TYPE_STRING;
            case "MAKEDATE":
                return Schema.TYPE_STRING;
            case "MAKETIME":
                return Schema.TYPE_STRING;
            case "MODIFYDATE":
                return Schema.TYPE_STRING;
            case "MODIFYTIME":
                return Schema.TYPE_STRING;
            case "INSURERUUID":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_STRING;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_STRING;
            case 6:
                return Schema.TYPE_STRING;
            case 7:
                return Schema.TYPE_STRING;
            case 8:
                return Schema.TYPE_STRING;
            case 9:
                return Schema.TYPE_STRING;
            case 10:
                return Schema.TYPE_STRING;
            case 11:
                return Schema.TYPE_STRING;
            case 12:
                return Schema.TYPE_STRING;
            case 13:
                return Schema.TYPE_STRING;
            case 14:
                return Schema.TYPE_STRING;
            case 15:
                return Schema.TYPE_STRING;
            case 16:
                return Schema.TYPE_STRING;
            case 17:
                return Schema.TYPE_STRING;
            case 18:
                return Schema.TYPE_STRING;
            case 19:
                return Schema.TYPE_STRING;
            case 20:
                return Schema.TYPE_STRING;
            case 21:
                return Schema.TYPE_STRING;
            case 22:
                return Schema.TYPE_STRING;
            case 23:
                return Schema.TYPE_STRING;
            case 24:
                return Schema.TYPE_STRING;
            case 25:
                return Schema.TYPE_STRING;
            case 26:
                return Schema.TYPE_STRING;
            case 27:
                return Schema.TYPE_STRING;
            case 28:
                return Schema.TYPE_STRING;
            case 29:
                return Schema.TYPE_STRING;
            case 30:
                return Schema.TYPE_STRING;
            case 31:
                return Schema.TYPE_STRING;
            case 32:
                return Schema.TYPE_STRING;
            case 33:
                return Schema.TYPE_STRING;
            case 34:
                return Schema.TYPE_STRING;
            case 35:
                return Schema.TYPE_STRING;
            case 36:
                return Schema.TYPE_STRING;
            case 37:
                return Schema.TYPE_STRING;
            case 38:
                return Schema.TYPE_STRING;
            case 39:
                return Schema.TYPE_STRING;
            case 40:
                return Schema.TYPE_STRING;
            case 41:
                return Schema.TYPE_STRING;
            case 42:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("SerialNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SerialNo));
        }
        if (FCode.equalsIgnoreCase("AccNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AccNo));
        }
        if (FCode.equalsIgnoreCase("ContNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ContNo));
        }
        if (FCode.equalsIgnoreCase("PrtNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PrtNo));
        }
        if (FCode.equalsIgnoreCase("CaseNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CaseNo));
        }
        if (FCode.equalsIgnoreCase("RiskCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RiskCode));
        }
        if (FCode.equalsIgnoreCase("InsuredName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InsuredName));
        }
        if (FCode.equalsIgnoreCase("InsuredIDType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InsuredIDType));
        }
        if (FCode.equalsIgnoreCase("InsuredIDNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InsuredIDNo));
        }
        if (FCode.equalsIgnoreCase("CompanyCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CompanyCode));
        }
        if (FCode.equalsIgnoreCase("Business")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Business));
        }
        if (FCode.equalsIgnoreCase("RiskType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RiskType));
        }
        if (FCode.equalsIgnoreCase("CustomerAllowed")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CustomerAllowed));
        }
        if (FCode.equalsIgnoreCase("MultiCompany")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MultiCompany));
        }
        if (FCode.equalsIgnoreCase("MajorDiseasePayment")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MajorDiseasePayment));
        }
        if (FCode.equalsIgnoreCase("Disability")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Disability));
        }
        if (FCode.equalsIgnoreCase("Dense")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Dense));
        }
        if (FCode.equalsIgnoreCase("AccumulativeMoney")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AccumulativeMoney));
        }
        if (FCode.equalsIgnoreCase("PageQueryCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PageQueryCode));
        }
        if (FCode.equalsIgnoreCase("TagDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(TagDate));
        }
        if (FCode.equalsIgnoreCase("DisplayPage")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DisplayPage));
        }
        if (FCode.equalsIgnoreCase("AbnormalCheck")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AbnormalCheck));
        }
        if (FCode.equalsIgnoreCase("AbnormalPayment")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AbnormalPayment));
        }
        if (FCode.equalsIgnoreCase("ChronicDiseasePayment")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ChronicDiseasePayment));
        }
        if (FCode.equalsIgnoreCase("PayMoney")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PayMoney));
        }
        if (FCode.equalsIgnoreCase("DayMoney")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DayMoney));
        }
        if (FCode.equalsIgnoreCase("CountOneYear")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CountOneYear));
        }
        if (FCode.equalsIgnoreCase("PaymentCountOneYear")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PaymentCountOneYear));
        }
        if (FCode.equalsIgnoreCase("Paymented")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Paymented));
        }
        if (FCode.equalsIgnoreCase("PaymentCount")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PaymentCount));
        }
        if (FCode.equalsIgnoreCase("DayMoneySum")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DayMoneySum));
        }
        if (FCode.equalsIgnoreCase("MajorDiseaseMoney")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MajorDiseaseMoney));
        }
        if (FCode.equalsIgnoreCase("PayCount")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PayCount));
        }
        if (FCode.equalsIgnoreCase("PaymentDayCount")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PaymentDayCount));
        }
        if (FCode.equalsIgnoreCase("Paymented2")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Paymented2));
        }
        if (FCode.equalsIgnoreCase("ReceiptCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ReceiptCode));
        }
        if (FCode.equalsIgnoreCase("HospitalCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(HospitalCode));
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeDate));
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyDate));
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (FCode.equalsIgnoreCase("InsurerUuid")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InsurerUuid));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = String.valueOf(SerialNo);
                break;
            case 1:
                strFieldValue = String.valueOf(AccNo);
                break;
            case 2:
                strFieldValue = String.valueOf(ContNo);
                break;
            case 3:
                strFieldValue = String.valueOf(PrtNo);
                break;
            case 4:
                strFieldValue = String.valueOf(CaseNo);
                break;
            case 5:
                strFieldValue = String.valueOf(RiskCode);
                break;
            case 6:
                strFieldValue = String.valueOf(InsuredName);
                break;
            case 7:
                strFieldValue = String.valueOf(InsuredIDType);
                break;
            case 8:
                strFieldValue = String.valueOf(InsuredIDNo);
                break;
            case 9:
                strFieldValue = String.valueOf(CompanyCode);
                break;
            case 10:
                strFieldValue = String.valueOf(Business);
                break;
            case 11:
                strFieldValue = String.valueOf(RiskType);
                break;
            case 12:
                strFieldValue = String.valueOf(CustomerAllowed);
                break;
            case 13:
                strFieldValue = String.valueOf(MultiCompany);
                break;
            case 14:
                strFieldValue = String.valueOf(MajorDiseasePayment);
                break;
            case 15:
                strFieldValue = String.valueOf(Disability);
                break;
            case 16:
                strFieldValue = String.valueOf(Dense);
                break;
            case 17:
                strFieldValue = String.valueOf(AccumulativeMoney);
                break;
            case 18:
                strFieldValue = String.valueOf(PageQueryCode);
                break;
            case 19:
                strFieldValue = String.valueOf(TagDate);
                break;
            case 20:
                strFieldValue = String.valueOf(DisplayPage);
                break;
            case 21:
                strFieldValue = String.valueOf(AbnormalCheck);
                break;
            case 22:
                strFieldValue = String.valueOf(AbnormalPayment);
                break;
            case 23:
                strFieldValue = String.valueOf(ChronicDiseasePayment);
                break;
            case 24:
                strFieldValue = String.valueOf(PayMoney);
                break;
            case 25:
                strFieldValue = String.valueOf(DayMoney);
                break;
            case 26:
                strFieldValue = String.valueOf(CountOneYear);
                break;
            case 27:
                strFieldValue = String.valueOf(PaymentCountOneYear);
                break;
            case 28:
                strFieldValue = String.valueOf(Paymented);
                break;
            case 29:
                strFieldValue = String.valueOf(PaymentCount);
                break;
            case 30:
                strFieldValue = String.valueOf(DayMoneySum);
                break;
            case 31:
                strFieldValue = String.valueOf(MajorDiseaseMoney);
                break;
            case 32:
                strFieldValue = String.valueOf(PayCount);
                break;
            case 33:
                strFieldValue = String.valueOf(PaymentDayCount);
                break;
            case 34:
                strFieldValue = String.valueOf(Paymented2);
                break;
            case 35:
                strFieldValue = String.valueOf(ReceiptCode);
                break;
            case 36:
                strFieldValue = String.valueOf(HospitalCode);
                break;
            case 37:
                strFieldValue = String.valueOf(Operator);
                break;
            case 38:
                strFieldValue = String.valueOf(MakeDate);
                break;
            case 39:
                strFieldValue = String.valueOf(MakeTime);
                break;
            case 40:
                strFieldValue = String.valueOf(ModifyDate);
                break;
            case 41:
                strFieldValue = String.valueOf(ModifyTime);
                break;
            case 42:
                strFieldValue = String.valueOf(InsurerUuid);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("SerialNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                SerialNo = FValue.trim();
            }
            else
                SerialNo = null;
        }
        if (FCode.equalsIgnoreCase("AccNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                AccNo = FValue.trim();
            }
            else
                AccNo = null;
        }
        if (FCode.equalsIgnoreCase("ContNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                ContNo = FValue.trim();
            }
            else
                ContNo = null;
        }
        if (FCode.equalsIgnoreCase("PrtNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                PrtNo = FValue.trim();
            }
            else
                PrtNo = null;
        }
        if (FCode.equalsIgnoreCase("CaseNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                CaseNo = FValue.trim();
            }
            else
                CaseNo = null;
        }
        if (FCode.equalsIgnoreCase("RiskCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                RiskCode = FValue.trim();
            }
            else
                RiskCode = null;
        }
        if (FCode.equalsIgnoreCase("InsuredName")) {
            if( FValue != null && !FValue.equals(""))
            {
                InsuredName = FValue.trim();
            }
            else
                InsuredName = null;
        }
        if (FCode.equalsIgnoreCase("InsuredIDType")) {
            if( FValue != null && !FValue.equals(""))
            {
                InsuredIDType = FValue.trim();
            }
            else
                InsuredIDType = null;
        }
        if (FCode.equalsIgnoreCase("InsuredIDNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                InsuredIDNo = FValue.trim();
            }
            else
                InsuredIDNo = null;
        }
        if (FCode.equalsIgnoreCase("CompanyCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                CompanyCode = FValue.trim();
            }
            else
                CompanyCode = null;
        }
        if (FCode.equalsIgnoreCase("Business")) {
            if( FValue != null && !FValue.equals(""))
            {
                Business = FValue.trim();
            }
            else
                Business = null;
        }
        if (FCode.equalsIgnoreCase("RiskType")) {
            if( FValue != null && !FValue.equals(""))
            {
                RiskType = FValue.trim();
            }
            else
                RiskType = null;
        }
        if (FCode.equalsIgnoreCase("CustomerAllowed")) {
            if( FValue != null && !FValue.equals(""))
            {
                CustomerAllowed = FValue.trim();
            }
            else
                CustomerAllowed = null;
        }
        if (FCode.equalsIgnoreCase("MultiCompany")) {
            if( FValue != null && !FValue.equals(""))
            {
                MultiCompany = FValue.trim();
            }
            else
                MultiCompany = null;
        }
        if (FCode.equalsIgnoreCase("MajorDiseasePayment")) {
            if( FValue != null && !FValue.equals(""))
            {
                MajorDiseasePayment = FValue.trim();
            }
            else
                MajorDiseasePayment = null;
        }
        if (FCode.equalsIgnoreCase("Disability")) {
            if( FValue != null && !FValue.equals(""))
            {
                Disability = FValue.trim();
            }
            else
                Disability = null;
        }
        if (FCode.equalsIgnoreCase("Dense")) {
            if( FValue != null && !FValue.equals(""))
            {
                Dense = FValue.trim();
            }
            else
                Dense = null;
        }
        if (FCode.equalsIgnoreCase("AccumulativeMoney")) {
            if( FValue != null && !FValue.equals(""))
            {
                AccumulativeMoney = FValue.trim();
            }
            else
                AccumulativeMoney = null;
        }
        if (FCode.equalsIgnoreCase("PageQueryCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                PageQueryCode = FValue.trim();
            }
            else
                PageQueryCode = null;
        }
        if (FCode.equalsIgnoreCase("TagDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                TagDate = FValue.trim();
            }
            else
                TagDate = null;
        }
        if (FCode.equalsIgnoreCase("DisplayPage")) {
            if( FValue != null && !FValue.equals(""))
            {
                DisplayPage = FValue.trim();
            }
            else
                DisplayPage = null;
        }
        if (FCode.equalsIgnoreCase("AbnormalCheck")) {
            if( FValue != null && !FValue.equals(""))
            {
                AbnormalCheck = FValue.trim();
            }
            else
                AbnormalCheck = null;
        }
        if (FCode.equalsIgnoreCase("AbnormalPayment")) {
            if( FValue != null && !FValue.equals(""))
            {
                AbnormalPayment = FValue.trim();
            }
            else
                AbnormalPayment = null;
        }
        if (FCode.equalsIgnoreCase("ChronicDiseasePayment")) {
            if( FValue != null && !FValue.equals(""))
            {
                ChronicDiseasePayment = FValue.trim();
            }
            else
                ChronicDiseasePayment = null;
        }
        if (FCode.equalsIgnoreCase("PayMoney")) {
            if( FValue != null && !FValue.equals(""))
            {
                PayMoney = FValue.trim();
            }
            else
                PayMoney = null;
        }
        if (FCode.equalsIgnoreCase("DayMoney")) {
            if( FValue != null && !FValue.equals(""))
            {
                DayMoney = FValue.trim();
            }
            else
                DayMoney = null;
        }
        if (FCode.equalsIgnoreCase("CountOneYear")) {
            if( FValue != null && !FValue.equals(""))
            {
                CountOneYear = FValue.trim();
            }
            else
                CountOneYear = null;
        }
        if (FCode.equalsIgnoreCase("PaymentCountOneYear")) {
            if( FValue != null && !FValue.equals(""))
            {
                PaymentCountOneYear = FValue.trim();
            }
            else
                PaymentCountOneYear = null;
        }
        if (FCode.equalsIgnoreCase("Paymented")) {
            if( FValue != null && !FValue.equals(""))
            {
                Paymented = FValue.trim();
            }
            else
                Paymented = null;
        }
        if (FCode.equalsIgnoreCase("PaymentCount")) {
            if( FValue != null && !FValue.equals(""))
            {
                PaymentCount = FValue.trim();
            }
            else
                PaymentCount = null;
        }
        if (FCode.equalsIgnoreCase("DayMoneySum")) {
            if( FValue != null && !FValue.equals(""))
            {
                DayMoneySum = FValue.trim();
            }
            else
                DayMoneySum = null;
        }
        if (FCode.equalsIgnoreCase("MajorDiseaseMoney")) {
            if( FValue != null && !FValue.equals(""))
            {
                MajorDiseaseMoney = FValue.trim();
            }
            else
                MajorDiseaseMoney = null;
        }
        if (FCode.equalsIgnoreCase("PayCount")) {
            if( FValue != null && !FValue.equals(""))
            {
                PayCount = FValue.trim();
            }
            else
                PayCount = null;
        }
        if (FCode.equalsIgnoreCase("PaymentDayCount")) {
            if( FValue != null && !FValue.equals(""))
            {
                PaymentDayCount = FValue.trim();
            }
            else
                PaymentDayCount = null;
        }
        if (FCode.equalsIgnoreCase("Paymented2")) {
            if( FValue != null && !FValue.equals(""))
            {
                Paymented2 = FValue.trim();
            }
            else
                Paymented2 = null;
        }
        if (FCode.equalsIgnoreCase("ReceiptCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                ReceiptCode = FValue.trim();
            }
            else
                ReceiptCode = null;
        }
        if (FCode.equalsIgnoreCase("HospitalCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                HospitalCode = FValue.trim();
            }
            else
                HospitalCode = null;
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            if( FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
                Operator = null;
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeDate = FValue.trim();
            }
            else
                MakeDate = null;
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
                MakeTime = null;
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyDate = FValue.trim();
            }
            else
                ModifyDate = null;
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
                ModifyTime = null;
        }
        if (FCode.equalsIgnoreCase("InsurerUuid")) {
            if( FValue != null && !FValue.equals(""))
            {
                InsurerUuid = FValue.trim();
            }
            else
                InsurerUuid = null;
        }
        return true;
    }


    public String toString() {
    return "LCRiskManagementInfoPojo [" +
            "SerialNo="+SerialNo +
            ", AccNo="+AccNo +
            ", ContNo="+ContNo +
            ", PrtNo="+PrtNo +
            ", CaseNo="+CaseNo +
            ", RiskCode="+RiskCode +
            ", InsuredName="+InsuredName +
            ", InsuredIDType="+InsuredIDType +
            ", InsuredIDNo="+InsuredIDNo +
            ", CompanyCode="+CompanyCode +
            ", Business="+Business +
            ", RiskType="+RiskType +
            ", CustomerAllowed="+CustomerAllowed +
            ", MultiCompany="+MultiCompany +
            ", MajorDiseasePayment="+MajorDiseasePayment +
            ", Disability="+Disability +
            ", Dense="+Dense +
            ", AccumulativeMoney="+AccumulativeMoney +
            ", PageQueryCode="+PageQueryCode +
            ", TagDate="+TagDate +
            ", DisplayPage="+DisplayPage +
            ", AbnormalCheck="+AbnormalCheck +
            ", AbnormalPayment="+AbnormalPayment +
            ", ChronicDiseasePayment="+ChronicDiseasePayment +
            ", PayMoney="+PayMoney +
            ", DayMoney="+DayMoney +
            ", CountOneYear="+CountOneYear +
            ", PaymentCountOneYear="+PaymentCountOneYear +
            ", Paymented="+Paymented +
            ", PaymentCount="+PaymentCount +
            ", DayMoneySum="+DayMoneySum +
            ", MajorDiseaseMoney="+MajorDiseaseMoney +
            ", PayCount="+PayCount +
            ", PaymentDayCount="+PaymentDayCount +
            ", Paymented2="+Paymented2 +
            ", ReceiptCode="+ReceiptCode +
            ", HospitalCode="+HospitalCode +
            ", Operator="+Operator +
            ", MakeDate="+MakeDate +
            ", MakeTime="+MakeTime +
            ", ModifyDate="+ModifyDate +
            ", ModifyTime="+ModifyTime +
            ", InsurerUuid="+InsurerUuid +"]";
    }
}
