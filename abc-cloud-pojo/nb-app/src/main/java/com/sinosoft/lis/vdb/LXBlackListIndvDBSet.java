/**
 * Copyright (c) 2017 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.vdb;

import com.sinosoft.lis.vschema.LXBlackListIndvSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.DBConnPool;
import com.sinosoft.utility.DBOper;
import com.sinosoft.utility.ExceptionUtils;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;

/**
 * <p>ClassName: LXBlackListIndvDBSet </p>
 * <p>Description: DB层多记录数据库操作类文件 </p>
 * <p>Copyright: Copyright (c) 2017</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2017-10-21
 */
public class LXBlackListIndvDBSet extends LXBlackListIndvSet {
    // @Field
    private Connection con;
    private DBOper db;
    /**
    * flag = true: 传入Connection
    * flag = false: 不传入Connection
    **/
    private boolean mflag = false;

    // @Constructor
    public LXBlackListIndvDBSet(Connection tConnection) {
        con = tConnection;
        db = new DBOper(con,"LXBlackListIndv");
        mflag = true;
    }

    public LXBlackListIndvDBSet() {
        db = new DBOper( "LXBlackListIndv" );
    }
    // @Method
    public boolean deleteSQL() {
        if (db.deleteSQL(this)) {
            return true;
        } else {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LXBlackListIndvDBSet";
            tError.functionName = "deleteSQL";
            tError.errorMessage = "操作失败!";
            this.mErrors .addOneError(tError);
            return false;
        }
    }

    /**
     * 删除操作
     * 删除条件：主键
     * @return boolean
     */
    public boolean delete() {
        PreparedStatement pstmt = null;

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try {
            int tCount = this.size();
            pstmt = con.prepareStatement("DELETE FROM LXBlackListIndv WHERE  1=1  AND BlacklistID = ?");
            for (int i = 1; i <= tCount; i++) {
            if(this.get(i).getBlacklistID() == null || this.get(i).getBlacklistID().equals("null")) {
            	pstmt.setString(1,null);
            } else {
            	pstmt.setString(1, this.get(i).getBlacklistID());
            }
                pstmt.addBatch();
            }
            pstmt.executeBatch();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            ex.printStackTrace();
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LXBlackListIndvDBSet";
            tError.functionName = "delete()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            try {
                pstmt.close();
            } catch (Exception e){
                e.printStackTrace();
            }

            if (!mflag) {
                try {
                    con.close();
                } catch (Exception e){e.printStackTrace();}
            }

            return false;
        }

        if( !mflag ) {
            try {
                con.close();
            } catch (Exception e){e.printStackTrace();}
        }

        return true;
    }

    /**
     * 更新操作
     * 更新条件：主键
     * @return boolean
     */
    public boolean update() {
        PreparedStatement pstmt = null;

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try {
            int tCount = this.size();
            pstmt = con.prepareStatement("UPDATE LXBlackListIndv SET  BlacklistID = ? , Name = ? , Usedname = ? , Gender = ? , Birthplace = ? , Nationality = ? , IDType = ? , IDNo = ? , Nation = ? , Occupation = ? , Title = ? , Address = ? , OtherInfo = ? , ReferNo = ? , Remark = ? , StandbyString1 = ? , StandbyString2 = ? , StandbyString3 = ? , StandbyString4 = ? , StandbyString5 = ? , MakeDate = ? , MakeTime = ? , MakeOperator = ? , ModifyDate = ? , ModifyTime = ? , ModifyOperator = ? , MiddleName = ? , SurName = ? , RelevanceId = ? , RelevanceType = ? , ListProperty = ? , Description1Name = ? , Description2Name = ? , Description3Name = ? , ProFileNotes = ? , ReferenceName = ? , RecordType = ? , DateOfBirth = ? , MinBirthday = ? , MaxBirthday = ? , ListedDate = ? , ScriptLanguageId = ? , FirstName = ? WHERE  1=1  AND BlacklistID = ?");
            for (int i = 1; i <= tCount; i++) {
            if(this.get(i).getBlacklistID() == null || this.get(i).getBlacklistID().equals("null")) {
            	pstmt.setString(1,null);
            } else {
            	pstmt.setString(1, this.get(i).getBlacklistID());
            }
            if(this.get(i).getName() == null || this.get(i).getName().equals("null")) {
            	pstmt.setString(2,null);
            } else {
            	pstmt.setString(2, this.get(i).getName());
            }
            if(this.get(i).getUsedname() == null || this.get(i).getUsedname().equals("null")) {
            	pstmt.setString(3,null);
            } else {
            	pstmt.setString(3, this.get(i).getUsedname());
            }
            if(this.get(i).getGender() == null || this.get(i).getGender().equals("null")) {
            	pstmt.setString(4,null);
            } else {
            	pstmt.setString(4, this.get(i).getGender());
            }
            if(this.get(i).getBirthplace() == null || this.get(i).getBirthplace().equals("null")) {
            	pstmt.setString(5,null);
            } else {
            	pstmt.setString(5, this.get(i).getBirthplace());
            }
            if(this.get(i).getNationality() == null || this.get(i).getNationality().equals("null")) {
            	pstmt.setString(6,null);
            } else {
            	pstmt.setString(6, this.get(i).getNationality());
            }
            if(this.get(i).getIDType() == null || this.get(i).getIDType().equals("null")) {
            	pstmt.setString(7,null);
            } else {
            	pstmt.setString(7, this.get(i).getIDType());
            }
            if(this.get(i).getIDNo() == null || this.get(i).getIDNo().equals("null")) {
            	pstmt.setString(8,null);
            } else {
            	pstmt.setString(8, this.get(i).getIDNo());
            }
            if(this.get(i).getNation() == null || this.get(i).getNation().equals("null")) {
            	pstmt.setString(9,null);
            } else {
            	pstmt.setString(9, this.get(i).getNation());
            }
            if(this.get(i).getOccupation() == null || this.get(i).getOccupation().equals("null")) {
            	pstmt.setString(10,null);
            } else {
            	pstmt.setString(10, this.get(i).getOccupation());
            }
            if(this.get(i).getTitle() == null || this.get(i).getTitle().equals("null")) {
            	pstmt.setString(11,null);
            } else {
            	pstmt.setString(11, this.get(i).getTitle());
            }
            if(this.get(i).getAddress() == null || this.get(i).getAddress().equals("null")) {
            	pstmt.setString(12,null);
            } else {
            	pstmt.setString(12, this.get(i).getAddress());
            }
            if(this.get(i).getOtherInfo() == null || this.get(i).getOtherInfo().equals("null")) {
            	pstmt.setString(13,null);
            } else {
            	pstmt.setString(13, this.get(i).getOtherInfo());
            }
            if(this.get(i).getReferNo() == null || this.get(i).getReferNo().equals("null")) {
            	pstmt.setString(14,null);
            } else {
            	pstmt.setString(14, this.get(i).getReferNo());
            }
            if(this.get(i).getRemark() == null || this.get(i).getRemark().equals("null")) {
            	pstmt.setString(15,null);
            } else {
            	pstmt.setString(15, this.get(i).getRemark());
            }
            if(this.get(i).getStandbyString1() == null || this.get(i).getStandbyString1().equals("null")) {
            	pstmt.setString(16,null);
            } else {
            	pstmt.setString(16, this.get(i).getStandbyString1());
            }
            if(this.get(i).getStandbyString2() == null || this.get(i).getStandbyString2().equals("null")) {
            	pstmt.setString(17,null);
            } else {
            	pstmt.setString(17, this.get(i).getStandbyString2());
            }
            if(this.get(i).getStandbyString3() == null || this.get(i).getStandbyString3().equals("null")) {
            	pstmt.setString(18,null);
            } else {
            	pstmt.setString(18, this.get(i).getStandbyString3());
            }
            if(this.get(i).getStandbyString4() == null || this.get(i).getStandbyString4().equals("null")) {
            	pstmt.setString(19,null);
            } else {
            	pstmt.setString(19, this.get(i).getStandbyString4());
            }
            if(this.get(i).getStandbyString5() == null || this.get(i).getStandbyString5().equals("null")) {
            	pstmt.setString(20,null);
            } else {
            	pstmt.setString(20, this.get(i).getStandbyString5());
            }
            if(this.get(i).getMakeDate() == null || this.get(i).getMakeDate().equals("null")) {
                pstmt.setDate(21,null);
            } else {
                pstmt.setDate(21, Date.valueOf(this.get(i).getMakeDate()));
            }
            if(this.get(i).getMakeTime() == null || this.get(i).getMakeTime().equals("null")) {
            	pstmt.setString(22,null);
            } else {
            	pstmt.setString(22, this.get(i).getMakeTime());
            }
            if(this.get(i).getMakeOperator() == null || this.get(i).getMakeOperator().equals("null")) {
            	pstmt.setString(23,null);
            } else {
            	pstmt.setString(23, this.get(i).getMakeOperator());
            }
            if(this.get(i).getModifyDate() == null || this.get(i).getModifyDate().equals("null")) {
                pstmt.setDate(24,null);
            } else {
                pstmt.setDate(24, Date.valueOf(this.get(i).getModifyDate()));
            }
            if(this.get(i).getModifyTime() == null || this.get(i).getModifyTime().equals("null")) {
            	pstmt.setString(25,null);
            } else {
            	pstmt.setString(25, this.get(i).getModifyTime());
            }
            if(this.get(i).getModifyOperator() == null || this.get(i).getModifyOperator().equals("null")) {
            	pstmt.setString(26,null);
            } else {
            	pstmt.setString(26, this.get(i).getModifyOperator());
            }
            if(this.get(i).getMiddleName() == null || this.get(i).getMiddleName().equals("null")) {
            	pstmt.setString(27,null);
            } else {
            	pstmt.setString(27, this.get(i).getMiddleName());
            }
            if(this.get(i).getSurName() == null || this.get(i).getSurName().equals("null")) {
            	pstmt.setString(28,null);
            } else {
            	pstmt.setString(28, this.get(i).getSurName());
            }
            if(this.get(i).getRelevanceId() == null || this.get(i).getRelevanceId().equals("null")) {
            	pstmt.setString(29,null);
            } else {
            	pstmt.setString(29, this.get(i).getRelevanceId());
            }
            if(this.get(i).getRelevanceType() == null || this.get(i).getRelevanceType().equals("null")) {
            	pstmt.setString(30,null);
            } else {
            	pstmt.setString(30, this.get(i).getRelevanceType());
            }
            if(this.get(i).getListProperty() == null || this.get(i).getListProperty().equals("null")) {
            	pstmt.setString(31,null);
            } else {
            	pstmt.setString(31, this.get(i).getListProperty());
            }
            if(this.get(i).getDescription1Name() == null || this.get(i).getDescription1Name().equals("null")) {
            	pstmt.setString(32,null);
            } else {
            	pstmt.setString(32, this.get(i).getDescription1Name());
            }
            if(this.get(i).getDescription2Name() == null || this.get(i).getDescription2Name().equals("null")) {
            	pstmt.setString(33,null);
            } else {
            	pstmt.setString(33, this.get(i).getDescription2Name());
            }
            if(this.get(i).getDescription3Name() == null || this.get(i).getDescription3Name().equals("null")) {
            	pstmt.setString(34,null);
            } else {
            	pstmt.setString(34, this.get(i).getDescription3Name());
            }
            if(this.get(i).getProFileNotes() == null || this.get(i).getProFileNotes().equals("null")) {
            	pstmt.setString(35,null);
            } else {
            	pstmt.setString(35, this.get(i).getProFileNotes());
            }
            if(this.get(i).getReferenceName() == null || this.get(i).getReferenceName().equals("null")) {
            	pstmt.setString(36,null);
            } else {
            	pstmt.setString(36, this.get(i).getReferenceName());
            }
            if(this.get(i).getRecordType() == null || this.get(i).getRecordType().equals("null")) {
            	pstmt.setString(37,null);
            } else {
            	pstmt.setString(37, this.get(i).getRecordType());
            }
            if(this.get(i).getDateOfBirth() == null || this.get(i).getDateOfBirth().equals("null")) {
            	pstmt.setString(38,null);
            } else {
            	pstmt.setString(38, this.get(i).getDateOfBirth());
            }
            if(this.get(i).getMinBirthday() == null || this.get(i).getMinBirthday().equals("null")) {
            	pstmt.setString(39,null);
            } else {
            	pstmt.setString(39, this.get(i).getMinBirthday());
            }
            if(this.get(i).getMaxBirthday() == null || this.get(i).getMaxBirthday().equals("null")) {
            	pstmt.setString(40,null);
            } else {
            	pstmt.setString(40, this.get(i).getMaxBirthday());
            }
            if(this.get(i).getListedDate() == null || this.get(i).getListedDate().equals("null")) {
            	pstmt.setString(41,null);
            } else {
            	pstmt.setString(41, this.get(i).getListedDate());
            }
            if(this.get(i).getScriptLanguageId() == null || this.get(i).getScriptLanguageId().equals("null")) {
            	pstmt.setString(42,null);
            } else {
            	pstmt.setString(42, this.get(i).getScriptLanguageId());
            }
            if(this.get(i).getFirstName() == null || this.get(i).getFirstName().equals("null")) {
            	pstmt.setString(43,null);
            } else {
            	pstmt.setString(43, this.get(i).getFirstName());
            }
            // set where condition
            if(this.get(i).getBlacklistID() == null || this.get(i).getBlacklistID().equals("null")) {
            	pstmt.setString(44,null);
            } else {
            	pstmt.setString(44, this.get(i).getBlacklistID());
            }
                pstmt.addBatch();
            }
            pstmt.executeBatch();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            ex.printStackTrace();
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LXBlackListIndvDBSet";
            tError.functionName = "update()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            try {
                pstmt.close();
            } catch (Exception e){e.printStackTrace();}

            if( !mflag ) {
                try {
                    con.close();
                } catch (Exception e){e.printStackTrace();}
            }

            return false;
        }

        if( !mflag ) {
            try {
                con.close();
            } catch (Exception e){e.printStackTrace();}
        }

        return true;
    }

    /**
     * 新增操作
     * @return boolean
     */
    public boolean insert() {
        PreparedStatement pstmt = null;

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try
        {
            int tCount = this.size();
            pstmt = con.prepareStatement("INSERT INTO LXBlackListIndv VALUES( ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ?)");
            for (int i = 1; i <= tCount; i++) {
            if(this.get(i).getBlacklistID() == null || this.get(i).getBlacklistID().equals("null")) {
            	pstmt.setString(1,null);
            } else {
            	pstmt.setString(1, this.get(i).getBlacklistID());
            }
            if(this.get(i).getName() == null || this.get(i).getName().equals("null")) {
            	pstmt.setString(2,null);
            } else {
            	pstmt.setString(2, this.get(i).getName());
            }
            if(this.get(i).getUsedname() == null || this.get(i).getUsedname().equals("null")) {
            	pstmt.setString(3,null);
            } else {
            	pstmt.setString(3, this.get(i).getUsedname());
            }
            if(this.get(i).getGender() == null || this.get(i).getGender().equals("null")) {
            	pstmt.setString(4,null);
            } else {
            	pstmt.setString(4, this.get(i).getGender());
            }
            if(this.get(i).getBirthplace() == null || this.get(i).getBirthplace().equals("null")) {
            	pstmt.setString(5,null);
            } else {
            	pstmt.setString(5, this.get(i).getBirthplace());
            }
            if(this.get(i).getNationality() == null || this.get(i).getNationality().equals("null")) {
            	pstmt.setString(6,null);
            } else {
            	pstmt.setString(6, this.get(i).getNationality());
            }
            if(this.get(i).getIDType() == null || this.get(i).getIDType().equals("null")) {
            	pstmt.setString(7,null);
            } else {
            	pstmt.setString(7, this.get(i).getIDType());
            }
            if(this.get(i).getIDNo() == null || this.get(i).getIDNo().equals("null")) {
            	pstmt.setString(8,null);
            } else {
            	pstmt.setString(8, this.get(i).getIDNo());
            }
            if(this.get(i).getNation() == null || this.get(i).getNation().equals("null")) {
            	pstmt.setString(9,null);
            } else {
            	pstmt.setString(9, this.get(i).getNation());
            }
            if(this.get(i).getOccupation() == null || this.get(i).getOccupation().equals("null")) {
            	pstmt.setString(10,null);
            } else {
            	pstmt.setString(10, this.get(i).getOccupation());
            }
            if(this.get(i).getTitle() == null || this.get(i).getTitle().equals("null")) {
            	pstmt.setString(11,null);
            } else {
            	pstmt.setString(11, this.get(i).getTitle());
            }
            if(this.get(i).getAddress() == null || this.get(i).getAddress().equals("null")) {
            	pstmt.setString(12,null);
            } else {
            	pstmt.setString(12, this.get(i).getAddress());
            }
            if(this.get(i).getOtherInfo() == null || this.get(i).getOtherInfo().equals("null")) {
            	pstmt.setString(13,null);
            } else {
            	pstmt.setString(13, this.get(i).getOtherInfo());
            }
            if(this.get(i).getReferNo() == null || this.get(i).getReferNo().equals("null")) {
            	pstmt.setString(14,null);
            } else {
            	pstmt.setString(14, this.get(i).getReferNo());
            }
            if(this.get(i).getRemark() == null || this.get(i).getRemark().equals("null")) {
            	pstmt.setString(15,null);
            } else {
            	pstmt.setString(15, this.get(i).getRemark());
            }
            if(this.get(i).getStandbyString1() == null || this.get(i).getStandbyString1().equals("null")) {
            	pstmt.setString(16,null);
            } else {
            	pstmt.setString(16, this.get(i).getStandbyString1());
            }
            if(this.get(i).getStandbyString2() == null || this.get(i).getStandbyString2().equals("null")) {
            	pstmt.setString(17,null);
            } else {
            	pstmt.setString(17, this.get(i).getStandbyString2());
            }
            if(this.get(i).getStandbyString3() == null || this.get(i).getStandbyString3().equals("null")) {
            	pstmt.setString(18,null);
            } else {
            	pstmt.setString(18, this.get(i).getStandbyString3());
            }
            if(this.get(i).getStandbyString4() == null || this.get(i).getStandbyString4().equals("null")) {
            	pstmt.setString(19,null);
            } else {
            	pstmt.setString(19, this.get(i).getStandbyString4());
            }
            if(this.get(i).getStandbyString5() == null || this.get(i).getStandbyString5().equals("null")) {
            	pstmt.setString(20,null);
            } else {
            	pstmt.setString(20, this.get(i).getStandbyString5());
            }
            if(this.get(i).getMakeDate() == null || this.get(i).getMakeDate().equals("null")) {
                pstmt.setDate(21,null);
            } else {
                pstmt.setDate(21, Date.valueOf(this.get(i).getMakeDate()));
            }
            if(this.get(i).getMakeTime() == null || this.get(i).getMakeTime().equals("null")) {
            	pstmt.setString(22,null);
            } else {
            	pstmt.setString(22, this.get(i).getMakeTime());
            }
            if(this.get(i).getMakeOperator() == null || this.get(i).getMakeOperator().equals("null")) {
            	pstmt.setString(23,null);
            } else {
            	pstmt.setString(23, this.get(i).getMakeOperator());
            }
            if(this.get(i).getModifyDate() == null || this.get(i).getModifyDate().equals("null")) {
                pstmt.setDate(24,null);
            } else {
                pstmt.setDate(24, Date.valueOf(this.get(i).getModifyDate()));
            }
            if(this.get(i).getModifyTime() == null || this.get(i).getModifyTime().equals("null")) {
            	pstmt.setString(25,null);
            } else {
            	pstmt.setString(25, this.get(i).getModifyTime());
            }
            if(this.get(i).getModifyOperator() == null || this.get(i).getModifyOperator().equals("null")) {
            	pstmt.setString(26,null);
            } else {
            	pstmt.setString(26, this.get(i).getModifyOperator());
            }
            if(this.get(i).getMiddleName() == null || this.get(i).getMiddleName().equals("null")) {
            	pstmt.setString(27,null);
            } else {
            	pstmt.setString(27, this.get(i).getMiddleName());
            }
            if(this.get(i).getSurName() == null || this.get(i).getSurName().equals("null")) {
            	pstmt.setString(28,null);
            } else {
            	pstmt.setString(28, this.get(i).getSurName());
            }
            if(this.get(i).getRelevanceId() == null || this.get(i).getRelevanceId().equals("null")) {
            	pstmt.setString(29,null);
            } else {
            	pstmt.setString(29, this.get(i).getRelevanceId());
            }
            if(this.get(i).getRelevanceType() == null || this.get(i).getRelevanceType().equals("null")) {
            	pstmt.setString(30,null);
            } else {
            	pstmt.setString(30, this.get(i).getRelevanceType());
            }
            if(this.get(i).getListProperty() == null || this.get(i).getListProperty().equals("null")) {
            	pstmt.setString(31,null);
            } else {
            	pstmt.setString(31, this.get(i).getListProperty());
            }
            if(this.get(i).getDescription1Name() == null || this.get(i).getDescription1Name().equals("null")) {
            	pstmt.setString(32,null);
            } else {
            	pstmt.setString(32, this.get(i).getDescription1Name());
            }
            if(this.get(i).getDescription2Name() == null || this.get(i).getDescription2Name().equals("null")) {
            	pstmt.setString(33,null);
            } else {
            	pstmt.setString(33, this.get(i).getDescription2Name());
            }
            if(this.get(i).getDescription3Name() == null || this.get(i).getDescription3Name().equals("null")) {
            	pstmt.setString(34,null);
            } else {
            	pstmt.setString(34, this.get(i).getDescription3Name());
            }
            if(this.get(i).getProFileNotes() == null || this.get(i).getProFileNotes().equals("null")) {
            	pstmt.setString(35,null);
            } else {
            	pstmt.setString(35, this.get(i).getProFileNotes());
            }
            if(this.get(i).getReferenceName() == null || this.get(i).getReferenceName().equals("null")) {
            	pstmt.setString(36,null);
            } else {
            	pstmt.setString(36, this.get(i).getReferenceName());
            }
            if(this.get(i).getRecordType() == null || this.get(i).getRecordType().equals("null")) {
            	pstmt.setString(37,null);
            } else {
            	pstmt.setString(37, this.get(i).getRecordType());
            }
            if(this.get(i).getDateOfBirth() == null || this.get(i).getDateOfBirth().equals("null")) {
            	pstmt.setString(38,null);
            } else {
            	pstmt.setString(38, this.get(i).getDateOfBirth());
            }
            if(this.get(i).getMinBirthday() == null || this.get(i).getMinBirthday().equals("null")) {
            	pstmt.setString(39,null);
            } else {
            	pstmt.setString(39, this.get(i).getMinBirthday());
            }
            if(this.get(i).getMaxBirthday() == null || this.get(i).getMaxBirthday().equals("null")) {
            	pstmt.setString(40,null);
            } else {
            	pstmt.setString(40, this.get(i).getMaxBirthday());
            }
            if(this.get(i).getListedDate() == null || this.get(i).getListedDate().equals("null")) {
            	pstmt.setString(41,null);
            } else {
            	pstmt.setString(41, this.get(i).getListedDate());
            }
            if(this.get(i).getScriptLanguageId() == null || this.get(i).getScriptLanguageId().equals("null")) {
            	pstmt.setString(42,null);
            } else {
            	pstmt.setString(42, this.get(i).getScriptLanguageId());
            }
            if(this.get(i).getFirstName() == null || this.get(i).getFirstName().equals("null")) {
            	pstmt.setString(43,null);
            } else {
            	pstmt.setString(43, this.get(i).getFirstName());
            }
                pstmt.addBatch();
            }
            pstmt.executeBatch();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            ex.printStackTrace();
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LXBlackListIndvDBSet";
            tError.functionName = "insert()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            try {
                pstmt.close();
            } catch (Exception e){e.printStackTrace();}

            if( !mflag ) {
                try {
                    con.close();
                } catch (Exception e){e.printStackTrace();}
            }

            return false;
        }

        if( !mflag ) {
            try {
                con.close();
            } catch (Exception e){e.printStackTrace();}
        }

        return true;
    }
}
