/**
 * Copyright (c) 2017 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.entity;

import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.Pojo;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;

import java.io.Serializable;

/**
 * <p>ClassName: LBInsureAccFeePojo </p>
 * <p>Copyright: Copyright (c) 2017</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2017-10-17
 */
public class LBInsureAccFeePojo implements Pojo,Serializable {
    // @Field
    /** Id */
    private long InsureAccFeeID; 
    /** Shardingid */
    private String ShardingID; 
    /** 批单号 */
    private String EdorNo; 
    /** 集体合同号码 */
    private String GrpContNo; 
    /** 集体保单险种号码 */
    private String GrpPolNo; 
    /** 保单险种号码 */
    private String PolNo; 
    /** 保险帐户号码 */
    private String InsuAccNo; 
    /** 合同号码 */
    private String ContNo; 
    /** 管理机构 */
    private String ManageCom; 
    /** 印刷号码 */
    private String PrtNo; 
    /** 险种编码 */
    private String RiskCode; 
    /** 账户类型 */
    private String AccType; 
    /** 投资类型 */
    private String InvestType; 
    /** 基金公司代码 */
    private String FundCompanyCode; 
    /** 被保人客户号码 */
    private String InsuredNo; 
    /** 投保人客户号码 */
    private String AppntNo; 
    /** 账户所有者 */
    private String Owner; 
    /** 账户结算方式 */
    private String AccComputeFlag; 
    /** 账户成立日期 */
    private String  AccFoundDate;
    /** 账户成立时间 */
    private String AccFoundTime; 
    /** 管理费比例 */
    private double FeeRate; 
    /** 本次管理费金额 */
    private double Fee; 
    /** 本次管理费单位数 */
    private double FeeUnit; 
    /** 结算日期 */
    private String  BalaDate;
    /** 结算时间 */
    private String BalaTime; 
    /** 操作员 */
    private String Operator; 
    /** 入机日期 */
    private String  MakeDate;
    /** 入机时间 */
    private String MakeTime; 
    /** 最后一次修改日期 */
    private String  ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime; 


    public static final int FIELDNUM = 30;    // 数据库表的字段个数

    private FDate fDate = new FDate();        // 处理日期
    public long getInsureAccFeeID() {
        return InsureAccFeeID;
    }
    public void setInsureAccFeeID(long aInsureAccFeeID) {
        InsureAccFeeID = aInsureAccFeeID;
    }
    public void setInsureAccFeeID(String aInsureAccFeeID) {
        if (aInsureAccFeeID != null && !aInsureAccFeeID.equals("")) {
            InsureAccFeeID = new Long(aInsureAccFeeID).longValue();
        }
    }

    public String getShardingID() {
        return ShardingID;
    }
    public void setShardingID(String aShardingID) {
        ShardingID = aShardingID;
    }
    public String getEdorNo() {
        return EdorNo;
    }
    public void setEdorNo(String aEdorNo) {
        EdorNo = aEdorNo;
    }
    public String getGrpContNo() {
        return GrpContNo;
    }
    public void setGrpContNo(String aGrpContNo) {
        GrpContNo = aGrpContNo;
    }
    public String getGrpPolNo() {
        return GrpPolNo;
    }
    public void setGrpPolNo(String aGrpPolNo) {
        GrpPolNo = aGrpPolNo;
    }
    public String getPolNo() {
        return PolNo;
    }
    public void setPolNo(String aPolNo) {
        PolNo = aPolNo;
    }
    public String getInsuAccNo() {
        return InsuAccNo;
    }
    public void setInsuAccNo(String aInsuAccNo) {
        InsuAccNo = aInsuAccNo;
    }
    public String getContNo() {
        return ContNo;
    }
    public void setContNo(String aContNo) {
        ContNo = aContNo;
    }
    public String getManageCom() {
        return ManageCom;
    }
    public void setManageCom(String aManageCom) {
        ManageCom = aManageCom;
    }
    public String getPrtNo() {
        return PrtNo;
    }
    public void setPrtNo(String aPrtNo) {
        PrtNo = aPrtNo;
    }
    public String getRiskCode() {
        return RiskCode;
    }
    public void setRiskCode(String aRiskCode) {
        RiskCode = aRiskCode;
    }
    public String getAccType() {
        return AccType;
    }
    public void setAccType(String aAccType) {
        AccType = aAccType;
    }
    public String getInvestType() {
        return InvestType;
    }
    public void setInvestType(String aInvestType) {
        InvestType = aInvestType;
    }
    public String getFundCompanyCode() {
        return FundCompanyCode;
    }
    public void setFundCompanyCode(String aFundCompanyCode) {
        FundCompanyCode = aFundCompanyCode;
    }
    public String getInsuredNo() {
        return InsuredNo;
    }
    public void setInsuredNo(String aInsuredNo) {
        InsuredNo = aInsuredNo;
    }
    public String getAppntNo() {
        return AppntNo;
    }
    public void setAppntNo(String aAppntNo) {
        AppntNo = aAppntNo;
    }
    public String getOwner() {
        return Owner;
    }
    public void setOwner(String aOwner) {
        Owner = aOwner;
    }
    public String getAccComputeFlag() {
        return AccComputeFlag;
    }
    public void setAccComputeFlag(String aAccComputeFlag) {
        AccComputeFlag = aAccComputeFlag;
    }
    public String getAccFoundDate() {
        return AccFoundDate;
    }
    public void setAccFoundDate(String aAccFoundDate) {
        AccFoundDate = aAccFoundDate;
    }
    public String getAccFoundTime() {
        return AccFoundTime;
    }
    public void setAccFoundTime(String aAccFoundTime) {
        AccFoundTime = aAccFoundTime;
    }
    public double getFeeRate() {
        return FeeRate;
    }
    public void setFeeRate(double aFeeRate) {
        FeeRate = aFeeRate;
    }
    public void setFeeRate(String aFeeRate) {
        if (aFeeRate != null && !aFeeRate.equals("")) {
            Double tDouble = new Double(aFeeRate);
            double d = tDouble.doubleValue();
            FeeRate = d;
        }
    }

    public double getFee() {
        return Fee;
    }
    public void setFee(double aFee) {
        Fee = aFee;
    }
    public void setFee(String aFee) {
        if (aFee != null && !aFee.equals("")) {
            Double tDouble = new Double(aFee);
            double d = tDouble.doubleValue();
            Fee = d;
        }
    }

    public double getFeeUnit() {
        return FeeUnit;
    }
    public void setFeeUnit(double aFeeUnit) {
        FeeUnit = aFeeUnit;
    }
    public void setFeeUnit(String aFeeUnit) {
        if (aFeeUnit != null && !aFeeUnit.equals("")) {
            Double tDouble = new Double(aFeeUnit);
            double d = tDouble.doubleValue();
            FeeUnit = d;
        }
    }

    public String getBalaDate() {
        return BalaDate;
    }
    public void setBalaDate(String aBalaDate) {
        BalaDate = aBalaDate;
    }
    public String getBalaTime() {
        return BalaTime;
    }
    public void setBalaTime(String aBalaTime) {
        BalaTime = aBalaTime;
    }
    public String getOperator() {
        return Operator;
    }
    public void setOperator(String aOperator) {
        Operator = aOperator;
    }
    public String getMakeDate() {
        return MakeDate;
    }
    public void setMakeDate(String aMakeDate) {
        MakeDate = aMakeDate;
    }
    public String getMakeTime() {
        return MakeTime;
    }
    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }
    public String getModifyDate() {
        return ModifyDate;
    }
    public void setModifyDate(String aModifyDate) {
        ModifyDate = aModifyDate;
    }
    public String getModifyTime() {
        return ModifyTime;
    }
    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("InsureAccFeeID") ) {
            return 0;
        }
        if( strFieldName.equals("ShardingID") ) {
            return 1;
        }
        if( strFieldName.equals("EdorNo") ) {
            return 2;
        }
        if( strFieldName.equals("GrpContNo") ) {
            return 3;
        }
        if( strFieldName.equals("GrpPolNo") ) {
            return 4;
        }
        if( strFieldName.equals("PolNo") ) {
            return 5;
        }
        if( strFieldName.equals("InsuAccNo") ) {
            return 6;
        }
        if( strFieldName.equals("ContNo") ) {
            return 7;
        }
        if( strFieldName.equals("ManageCom") ) {
            return 8;
        }
        if( strFieldName.equals("PrtNo") ) {
            return 9;
        }
        if( strFieldName.equals("RiskCode") ) {
            return 10;
        }
        if( strFieldName.equals("AccType") ) {
            return 11;
        }
        if( strFieldName.equals("InvestType") ) {
            return 12;
        }
        if( strFieldName.equals("FundCompanyCode") ) {
            return 13;
        }
        if( strFieldName.equals("InsuredNo") ) {
            return 14;
        }
        if( strFieldName.equals("AppntNo") ) {
            return 15;
        }
        if( strFieldName.equals("Owner") ) {
            return 16;
        }
        if( strFieldName.equals("AccComputeFlag") ) {
            return 17;
        }
        if( strFieldName.equals("AccFoundDate") ) {
            return 18;
        }
        if( strFieldName.equals("AccFoundTime") ) {
            return 19;
        }
        if( strFieldName.equals("FeeRate") ) {
            return 20;
        }
        if( strFieldName.equals("Fee") ) {
            return 21;
        }
        if( strFieldName.equals("FeeUnit") ) {
            return 22;
        }
        if( strFieldName.equals("BalaDate") ) {
            return 23;
        }
        if( strFieldName.equals("BalaTime") ) {
            return 24;
        }
        if( strFieldName.equals("Operator") ) {
            return 25;
        }
        if( strFieldName.equals("MakeDate") ) {
            return 26;
        }
        if( strFieldName.equals("MakeTime") ) {
            return 27;
        }
        if( strFieldName.equals("ModifyDate") ) {
            return 28;
        }
        if( strFieldName.equals("ModifyTime") ) {
            return 29;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "InsureAccFeeID";
                break;
            case 1:
                strFieldName = "ShardingID";
                break;
            case 2:
                strFieldName = "EdorNo";
                break;
            case 3:
                strFieldName = "GrpContNo";
                break;
            case 4:
                strFieldName = "GrpPolNo";
                break;
            case 5:
                strFieldName = "PolNo";
                break;
            case 6:
                strFieldName = "InsuAccNo";
                break;
            case 7:
                strFieldName = "ContNo";
                break;
            case 8:
                strFieldName = "ManageCom";
                break;
            case 9:
                strFieldName = "PrtNo";
                break;
            case 10:
                strFieldName = "RiskCode";
                break;
            case 11:
                strFieldName = "AccType";
                break;
            case 12:
                strFieldName = "InvestType";
                break;
            case 13:
                strFieldName = "FundCompanyCode";
                break;
            case 14:
                strFieldName = "InsuredNo";
                break;
            case 15:
                strFieldName = "AppntNo";
                break;
            case 16:
                strFieldName = "Owner";
                break;
            case 17:
                strFieldName = "AccComputeFlag";
                break;
            case 18:
                strFieldName = "AccFoundDate";
                break;
            case 19:
                strFieldName = "AccFoundTime";
                break;
            case 20:
                strFieldName = "FeeRate";
                break;
            case 21:
                strFieldName = "Fee";
                break;
            case 22:
                strFieldName = "FeeUnit";
                break;
            case 23:
                strFieldName = "BalaDate";
                break;
            case 24:
                strFieldName = "BalaTime";
                break;
            case 25:
                strFieldName = "Operator";
                break;
            case 26:
                strFieldName = "MakeDate";
                break;
            case 27:
                strFieldName = "MakeTime";
                break;
            case 28:
                strFieldName = "ModifyDate";
                break;
            case 29:
                strFieldName = "ModifyTime";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "INSUREACCFEEID":
                return Schema.TYPE_LONG;
            case "SHARDINGID":
                return Schema.TYPE_STRING;
            case "EDORNO":
                return Schema.TYPE_STRING;
            case "GRPCONTNO":
                return Schema.TYPE_STRING;
            case "GRPPOLNO":
                return Schema.TYPE_STRING;
            case "POLNO":
                return Schema.TYPE_STRING;
            case "INSUACCNO":
                return Schema.TYPE_STRING;
            case "CONTNO":
                return Schema.TYPE_STRING;
            case "MANAGECOM":
                return Schema.TYPE_STRING;
            case "PRTNO":
                return Schema.TYPE_STRING;
            case "RISKCODE":
                return Schema.TYPE_STRING;
            case "ACCTYPE":
                return Schema.TYPE_STRING;
            case "INVESTTYPE":
                return Schema.TYPE_STRING;
            case "FUNDCOMPANYCODE":
                return Schema.TYPE_STRING;
            case "INSUREDNO":
                return Schema.TYPE_STRING;
            case "APPNTNO":
                return Schema.TYPE_STRING;
            case "OWNER":
                return Schema.TYPE_STRING;
            case "ACCCOMPUTEFLAG":
                return Schema.TYPE_STRING;
            case "ACCFOUNDDATE":
                return Schema.TYPE_STRING;
            case "ACCFOUNDTIME":
                return Schema.TYPE_STRING;
            case "FEERATE":
                return Schema.TYPE_DOUBLE;
            case "FEE":
                return Schema.TYPE_DOUBLE;
            case "FEEUNIT":
                return Schema.TYPE_DOUBLE;
            case "BALADATE":
                return Schema.TYPE_STRING;
            case "BALATIME":
                return Schema.TYPE_STRING;
            case "OPERATOR":
                return Schema.TYPE_STRING;
            case "MAKEDATE":
                return Schema.TYPE_STRING;
            case "MAKETIME":
                return Schema.TYPE_STRING;
            case "MODIFYDATE":
                return Schema.TYPE_STRING;
            case "MODIFYTIME":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_LONG;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_STRING;
            case 6:
                return Schema.TYPE_STRING;
            case 7:
                return Schema.TYPE_STRING;
            case 8:
                return Schema.TYPE_STRING;
            case 9:
                return Schema.TYPE_STRING;
            case 10:
                return Schema.TYPE_STRING;
            case 11:
                return Schema.TYPE_STRING;
            case 12:
                return Schema.TYPE_STRING;
            case 13:
                return Schema.TYPE_STRING;
            case 14:
                return Schema.TYPE_STRING;
            case 15:
                return Schema.TYPE_STRING;
            case 16:
                return Schema.TYPE_STRING;
            case 17:
                return Schema.TYPE_STRING;
            case 18:
                return Schema.TYPE_STRING;
            case 19:
                return Schema.TYPE_STRING;
            case 20:
                return Schema.TYPE_DOUBLE;
            case 21:
                return Schema.TYPE_DOUBLE;
            case 22:
                return Schema.TYPE_DOUBLE;
            case 23:
                return Schema.TYPE_STRING;
            case 24:
                return Schema.TYPE_STRING;
            case 25:
                return Schema.TYPE_STRING;
            case 26:
                return Schema.TYPE_STRING;
            case 27:
                return Schema.TYPE_STRING;
            case 28:
                return Schema.TYPE_STRING;
            case 29:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("InsureAccFeeID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InsureAccFeeID));
        }
        if (FCode.equalsIgnoreCase("ShardingID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ShardingID));
        }
        if (FCode.equalsIgnoreCase("EdorNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(EdorNo));
        }
        if (FCode.equalsIgnoreCase("GrpContNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GrpContNo));
        }
        if (FCode.equalsIgnoreCase("GrpPolNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GrpPolNo));
        }
        if (FCode.equalsIgnoreCase("PolNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PolNo));
        }
        if (FCode.equalsIgnoreCase("InsuAccNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InsuAccNo));
        }
        if (FCode.equalsIgnoreCase("ContNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ContNo));
        }
        if (FCode.equalsIgnoreCase("ManageCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
        }
        if (FCode.equalsIgnoreCase("PrtNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PrtNo));
        }
        if (FCode.equalsIgnoreCase("RiskCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RiskCode));
        }
        if (FCode.equalsIgnoreCase("AccType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AccType));
        }
        if (FCode.equalsIgnoreCase("InvestType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InvestType));
        }
        if (FCode.equalsIgnoreCase("FundCompanyCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FundCompanyCode));
        }
        if (FCode.equalsIgnoreCase("InsuredNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InsuredNo));
        }
        if (FCode.equalsIgnoreCase("AppntNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AppntNo));
        }
        if (FCode.equalsIgnoreCase("Owner")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Owner));
        }
        if (FCode.equalsIgnoreCase("AccComputeFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AccComputeFlag));
        }
        if (FCode.equalsIgnoreCase("AccFoundDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AccFoundDate));
        }
        if (FCode.equalsIgnoreCase("AccFoundTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AccFoundTime));
        }
        if (FCode.equalsIgnoreCase("FeeRate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FeeRate));
        }
        if (FCode.equalsIgnoreCase("Fee")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Fee));
        }
        if (FCode.equalsIgnoreCase("FeeUnit")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FeeUnit));
        }
        if (FCode.equalsIgnoreCase("BalaDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BalaDate));
        }
        if (FCode.equalsIgnoreCase("BalaTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BalaTime));
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeDate));
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyDate));
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = String.valueOf(InsureAccFeeID);
                break;
            case 1:
                strFieldValue = String.valueOf(ShardingID);
                break;
            case 2:
                strFieldValue = String.valueOf(EdorNo);
                break;
            case 3:
                strFieldValue = String.valueOf(GrpContNo);
                break;
            case 4:
                strFieldValue = String.valueOf(GrpPolNo);
                break;
            case 5:
                strFieldValue = String.valueOf(PolNo);
                break;
            case 6:
                strFieldValue = String.valueOf(InsuAccNo);
                break;
            case 7:
                strFieldValue = String.valueOf(ContNo);
                break;
            case 8:
                strFieldValue = String.valueOf(ManageCom);
                break;
            case 9:
                strFieldValue = String.valueOf(PrtNo);
                break;
            case 10:
                strFieldValue = String.valueOf(RiskCode);
                break;
            case 11:
                strFieldValue = String.valueOf(AccType);
                break;
            case 12:
                strFieldValue = String.valueOf(InvestType);
                break;
            case 13:
                strFieldValue = String.valueOf(FundCompanyCode);
                break;
            case 14:
                strFieldValue = String.valueOf(InsuredNo);
                break;
            case 15:
                strFieldValue = String.valueOf(AppntNo);
                break;
            case 16:
                strFieldValue = String.valueOf(Owner);
                break;
            case 17:
                strFieldValue = String.valueOf(AccComputeFlag);
                break;
            case 18:
                strFieldValue = String.valueOf(AccFoundDate);
                break;
            case 19:
                strFieldValue = String.valueOf(AccFoundTime);
                break;
            case 20:
                strFieldValue = String.valueOf(FeeRate);
                break;
            case 21:
                strFieldValue = String.valueOf(Fee);
                break;
            case 22:
                strFieldValue = String.valueOf(FeeUnit);
                break;
            case 23:
                strFieldValue = String.valueOf(BalaDate);
                break;
            case 24:
                strFieldValue = String.valueOf(BalaTime);
                break;
            case 25:
                strFieldValue = String.valueOf(Operator);
                break;
            case 26:
                strFieldValue = String.valueOf(MakeDate);
                break;
            case 27:
                strFieldValue = String.valueOf(MakeTime);
                break;
            case 28:
                strFieldValue = String.valueOf(ModifyDate);
                break;
            case 29:
                strFieldValue = String.valueOf(ModifyTime);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("InsureAccFeeID")) {
            if( FValue != null && !FValue.equals("")) {
                InsureAccFeeID = new Long(FValue).longValue();
            }
        }
        if (FCode.equalsIgnoreCase("ShardingID")) {
            if( FValue != null && !FValue.equals(""))
            {
                ShardingID = FValue.trim();
            }
            else
                ShardingID = null;
        }
        if (FCode.equalsIgnoreCase("EdorNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                EdorNo = FValue.trim();
            }
            else
                EdorNo = null;
        }
        if (FCode.equalsIgnoreCase("GrpContNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                GrpContNo = FValue.trim();
            }
            else
                GrpContNo = null;
        }
        if (FCode.equalsIgnoreCase("GrpPolNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                GrpPolNo = FValue.trim();
            }
            else
                GrpPolNo = null;
        }
        if (FCode.equalsIgnoreCase("PolNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                PolNo = FValue.trim();
            }
            else
                PolNo = null;
        }
        if (FCode.equalsIgnoreCase("InsuAccNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                InsuAccNo = FValue.trim();
            }
            else
                InsuAccNo = null;
        }
        if (FCode.equalsIgnoreCase("ContNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                ContNo = FValue.trim();
            }
            else
                ContNo = null;
        }
        if (FCode.equalsIgnoreCase("ManageCom")) {
            if( FValue != null && !FValue.equals(""))
            {
                ManageCom = FValue.trim();
            }
            else
                ManageCom = null;
        }
        if (FCode.equalsIgnoreCase("PrtNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                PrtNo = FValue.trim();
            }
            else
                PrtNo = null;
        }
        if (FCode.equalsIgnoreCase("RiskCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                RiskCode = FValue.trim();
            }
            else
                RiskCode = null;
        }
        if (FCode.equalsIgnoreCase("AccType")) {
            if( FValue != null && !FValue.equals(""))
            {
                AccType = FValue.trim();
            }
            else
                AccType = null;
        }
        if (FCode.equalsIgnoreCase("InvestType")) {
            if( FValue != null && !FValue.equals(""))
            {
                InvestType = FValue.trim();
            }
            else
                InvestType = null;
        }
        if (FCode.equalsIgnoreCase("FundCompanyCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                FundCompanyCode = FValue.trim();
            }
            else
                FundCompanyCode = null;
        }
        if (FCode.equalsIgnoreCase("InsuredNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                InsuredNo = FValue.trim();
            }
            else
                InsuredNo = null;
        }
        if (FCode.equalsIgnoreCase("AppntNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                AppntNo = FValue.trim();
            }
            else
                AppntNo = null;
        }
        if (FCode.equalsIgnoreCase("Owner")) {
            if( FValue != null && !FValue.equals(""))
            {
                Owner = FValue.trim();
            }
            else
                Owner = null;
        }
        if (FCode.equalsIgnoreCase("AccComputeFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                AccComputeFlag = FValue.trim();
            }
            else
                AccComputeFlag = null;
        }
        if (FCode.equalsIgnoreCase("AccFoundDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                AccFoundDate = FValue.trim();
            }
            else
                AccFoundDate = null;
        }
        if (FCode.equalsIgnoreCase("AccFoundTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                AccFoundTime = FValue.trim();
            }
            else
                AccFoundTime = null;
        }
        if (FCode.equalsIgnoreCase("FeeRate")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                FeeRate = d;
            }
        }
        if (FCode.equalsIgnoreCase("Fee")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                Fee = d;
            }
        }
        if (FCode.equalsIgnoreCase("FeeUnit")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                FeeUnit = d;
            }
        }
        if (FCode.equalsIgnoreCase("BalaDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                BalaDate = FValue.trim();
            }
            else
                BalaDate = null;
        }
        if (FCode.equalsIgnoreCase("BalaTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                BalaTime = FValue.trim();
            }
            else
                BalaTime = null;
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            if( FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
                Operator = null;
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeDate = FValue.trim();
            }
            else
                MakeDate = null;
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
                MakeTime = null;
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyDate = FValue.trim();
            }
            else
                ModifyDate = null;
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
                ModifyTime = null;
        }
        return true;
    }


    public String toString() {
    return "LBInsureAccFeePojo [" +
            "InsureAccFeeID="+InsureAccFeeID +
            ", ShardingID="+ShardingID +
            ", EdorNo="+EdorNo +
            ", GrpContNo="+GrpContNo +
            ", GrpPolNo="+GrpPolNo +
            ", PolNo="+PolNo +
            ", InsuAccNo="+InsuAccNo +
            ", ContNo="+ContNo +
            ", ManageCom="+ManageCom +
            ", PrtNo="+PrtNo +
            ", RiskCode="+RiskCode +
            ", AccType="+AccType +
            ", InvestType="+InvestType +
            ", FundCompanyCode="+FundCompanyCode +
            ", InsuredNo="+InsuredNo +
            ", AppntNo="+AppntNo +
            ", Owner="+Owner +
            ", AccComputeFlag="+AccComputeFlag +
            ", AccFoundDate="+AccFoundDate +
            ", AccFoundTime="+AccFoundTime +
            ", FeeRate="+FeeRate +
            ", Fee="+Fee +
            ", FeeUnit="+FeeUnit +
            ", BalaDate="+BalaDate +
            ", BalaTime="+BalaTime +
            ", Operator="+Operator +
            ", MakeDate="+MakeDate +
            ", MakeTime="+MakeTime +
            ", ModifyDate="+ModifyDate +
            ", ModifyTime="+ModifyTime +"]";
    }
}
