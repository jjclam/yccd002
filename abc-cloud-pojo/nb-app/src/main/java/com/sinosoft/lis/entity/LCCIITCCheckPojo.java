/**
 * Copyright (c) 2019 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.entity;

import java.io.*;
import java.util.Date;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.StrTool;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * <p>ClassName: LCCIITCCheckPojo </p>
 * <p>Copyright: Copyright (c) 2019</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2019-07-05
 */
public class LCCIITCCheckPojo implements  Pojo,Serializable {
    // @Field
    /** 序列号 */
    private String SerialNo; 
    /** 投保单号 */
    private String PrtNo; 
    /** 校验次数 */
    private int CheckNum; 
    /** 投保人客户号码 */
    private String AppntNo; 
    /** 投保人名称 */
    private String AppntName; 
    /** 意健险标记 */
    private String AccidentFlag; 
    /** 意健险产品编码 */
    private String AccidentProductCode; 
    /** 累计意外险风险保额 */
    private double AccidentProdRiskAmnt; 
    /** 意外险请求报文 */
    private String AccidentRequestMsg; 
    /** 重疾险标记 */
    private String MDFlag; 
    /** 重疾险产品编码 */
    private String MDProductCode; 
    /** 累计重疾险风险保额 */
    private double MDProdRiskAmnt; 
    /** 重疾险请求报文 */
    private String MDRequestMsg; 
    /** 管理机构 */
    private String ManageCom; 
    /** 操作员 */
    private String Operator; 
    /** 入机日期 */
    private String  MakeDate;
    /** 入机时间 */
    private String MakeTime; 
    /** 最后一次修改日期 */
    private String  ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime; 
    /** 备用属性字段1 */
    private String StandByFlag1; 
    /** 意外险基本保额 */
    private double AccidentAmnt; 
    /** 意外险应收保费 */
    private double AccidentPrem; 
    /** 重疾险基本保额 */
    private double MdAmnt; 
    /** 重疾险应收保费 */
    private double MdPrem; 


    public static final int FIELDNUM = 24;    // 数据库表的字段个数

    private FDate fDate = new FDate();        // 处理日期
    public String getSerialNo() {
        return SerialNo;
    }
    public void setSerialNo(String aSerialNo) {
        SerialNo = aSerialNo;
    }
    public String getPrtNo() {
        return PrtNo;
    }
    public void setPrtNo(String aPrtNo) {
        PrtNo = aPrtNo;
    }
    public int getCheckNum() {
        return CheckNum;
    }
    public void setCheckNum(int aCheckNum) {
        CheckNum = aCheckNum;
    }
    public void setCheckNum(String aCheckNum) {
        if (aCheckNum != null && !aCheckNum.equals("")) {
            Integer tInteger = new Integer(aCheckNum);
            int i = tInteger.intValue();
            CheckNum = i;
        }
    }

    public String getAppntNo() {
        return AppntNo;
    }
    public void setAppntNo(String aAppntNo) {
        AppntNo = aAppntNo;
    }
    public String getAppntName() {
        return AppntName;
    }
    public void setAppntName(String aAppntName) {
        AppntName = aAppntName;
    }
    public String getAccidentFlag() {
        return AccidentFlag;
    }
    public void setAccidentFlag(String aAccidentFlag) {
        AccidentFlag = aAccidentFlag;
    }
    public String getAccidentProductCode() {
        return AccidentProductCode;
    }
    public void setAccidentProductCode(String aAccidentProductCode) {
        AccidentProductCode = aAccidentProductCode;
    }
    public double getAccidentProdRiskAmnt() {
        return AccidentProdRiskAmnt;
    }
    public void setAccidentProdRiskAmnt(double aAccidentProdRiskAmnt) {
        AccidentProdRiskAmnt = aAccidentProdRiskAmnt;
    }
    public void setAccidentProdRiskAmnt(String aAccidentProdRiskAmnt) {
        if (aAccidentProdRiskAmnt != null && !aAccidentProdRiskAmnt.equals("")) {
            Double tDouble = new Double(aAccidentProdRiskAmnt);
            double d = tDouble.doubleValue();
            AccidentProdRiskAmnt = d;
        }
    }

    public String getAccidentRequestMsg() {
        return AccidentRequestMsg;
    }
    public void setAccidentRequestMsg(String aAccidentRequestMsg) {
        AccidentRequestMsg = aAccidentRequestMsg;
    }
    public String getMDFlag() {
        return MDFlag;
    }
    public void setMDFlag(String aMDFlag) {
        MDFlag = aMDFlag;
    }
    public String getMDProductCode() {
        return MDProductCode;
    }
    public void setMDProductCode(String aMDProductCode) {
        MDProductCode = aMDProductCode;
    }
    public double getMDProdRiskAmnt() {
        return MDProdRiskAmnt;
    }
    public void setMDProdRiskAmnt(double aMDProdRiskAmnt) {
        MDProdRiskAmnt = aMDProdRiskAmnt;
    }
    public void setMDProdRiskAmnt(String aMDProdRiskAmnt) {
        if (aMDProdRiskAmnt != null && !aMDProdRiskAmnt.equals("")) {
            Double tDouble = new Double(aMDProdRiskAmnt);
            double d = tDouble.doubleValue();
            MDProdRiskAmnt = d;
        }
    }

    public String getMDRequestMsg() {
        return MDRequestMsg;
    }
    public void setMDRequestMsg(String aMDRequestMsg) {
        MDRequestMsg = aMDRequestMsg;
    }
    public String getManageCom() {
        return ManageCom;
    }
    public void setManageCom(String aManageCom) {
        ManageCom = aManageCom;
    }
    public String getOperator() {
        return Operator;
    }
    public void setOperator(String aOperator) {
        Operator = aOperator;
    }
    public String getMakeDate() {
        return MakeDate;
    }
    public void setMakeDate(String aMakeDate) {
        MakeDate = aMakeDate;
    }
    public String getMakeTime() {
        return MakeTime;
    }
    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }
    public String getModifyDate() {
        return ModifyDate;
    }
    public void setModifyDate(String aModifyDate) {
        ModifyDate = aModifyDate;
    }
    public String getModifyTime() {
        return ModifyTime;
    }
    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }
    public String getStandByFlag1() {
        return StandByFlag1;
    }
    public void setStandByFlag1(String aStandByFlag1) {
        StandByFlag1 = aStandByFlag1;
    }
    public double getAccidentAmnt() {
        return AccidentAmnt;
    }
    public void setAccidentAmnt(double aAccidentAmnt) {
        AccidentAmnt = aAccidentAmnt;
    }
    public void setAccidentAmnt(String aAccidentAmnt) {
        if (aAccidentAmnt != null && !aAccidentAmnt.equals("")) {
            Double tDouble = new Double(aAccidentAmnt);
            double d = tDouble.doubleValue();
            AccidentAmnt = d;
        }
    }

    public double getAccidentPrem() {
        return AccidentPrem;
    }
    public void setAccidentPrem(double aAccidentPrem) {
        AccidentPrem = aAccidentPrem;
    }
    public void setAccidentPrem(String aAccidentPrem) {
        if (aAccidentPrem != null && !aAccidentPrem.equals("")) {
            Double tDouble = new Double(aAccidentPrem);
            double d = tDouble.doubleValue();
            AccidentPrem = d;
        }
    }

    public double getMdAmnt() {
        return MdAmnt;
    }
    public void setMdAmnt(double aMdAmnt) {
        MdAmnt = aMdAmnt;
    }
    public void setMdAmnt(String aMdAmnt) {
        if (aMdAmnt != null && !aMdAmnt.equals("")) {
            Double tDouble = new Double(aMdAmnt);
            double d = tDouble.doubleValue();
            MdAmnt = d;
        }
    }

    public double getMdPrem() {
        return MdPrem;
    }
    public void setMdPrem(double aMdPrem) {
        MdPrem = aMdPrem;
    }
    public void setMdPrem(String aMdPrem) {
        if (aMdPrem != null && !aMdPrem.equals("")) {
            Double tDouble = new Double(aMdPrem);
            double d = tDouble.doubleValue();
            MdPrem = d;
        }
    }


    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("SerialNo") ) {
            return 0;
        }
        if( strFieldName.equals("PrtNo") ) {
            return 1;
        }
        if( strFieldName.equals("CheckNum") ) {
            return 2;
        }
        if( strFieldName.equals("AppntNo") ) {
            return 3;
        }
        if( strFieldName.equals("AppntName") ) {
            return 4;
        }
        if( strFieldName.equals("AccidentFlag") ) {
            return 5;
        }
        if( strFieldName.equals("AccidentProductCode") ) {
            return 6;
        }
        if( strFieldName.equals("AccidentProdRiskAmnt") ) {
            return 7;
        }
        if( strFieldName.equals("AccidentRequestMsg") ) {
            return 8;
        }
        if( strFieldName.equals("MDFlag") ) {
            return 9;
        }
        if( strFieldName.equals("MDProductCode") ) {
            return 10;
        }
        if( strFieldName.equals("MDProdRiskAmnt") ) {
            return 11;
        }
        if( strFieldName.equals("MDRequestMsg") ) {
            return 12;
        }
        if( strFieldName.equals("ManageCom") ) {
            return 13;
        }
        if( strFieldName.equals("Operator") ) {
            return 14;
        }
        if( strFieldName.equals("MakeDate") ) {
            return 15;
        }
        if( strFieldName.equals("MakeTime") ) {
            return 16;
        }
        if( strFieldName.equals("ModifyDate") ) {
            return 17;
        }
        if( strFieldName.equals("ModifyTime") ) {
            return 18;
        }
        if( strFieldName.equals("StandByFlag1") ) {
            return 19;
        }
        if( strFieldName.equals("AccidentAmnt") ) {
            return 20;
        }
        if( strFieldName.equals("AccidentPrem") ) {
            return 21;
        }
        if( strFieldName.equals("MdAmnt") ) {
            return 22;
        }
        if( strFieldName.equals("MdPrem") ) {
            return 23;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "SerialNo";
                break;
            case 1:
                strFieldName = "PrtNo";
                break;
            case 2:
                strFieldName = "CheckNum";
                break;
            case 3:
                strFieldName = "AppntNo";
                break;
            case 4:
                strFieldName = "AppntName";
                break;
            case 5:
                strFieldName = "AccidentFlag";
                break;
            case 6:
                strFieldName = "AccidentProductCode";
                break;
            case 7:
                strFieldName = "AccidentProdRiskAmnt";
                break;
            case 8:
                strFieldName = "AccidentRequestMsg";
                break;
            case 9:
                strFieldName = "MDFlag";
                break;
            case 10:
                strFieldName = "MDProductCode";
                break;
            case 11:
                strFieldName = "MDProdRiskAmnt";
                break;
            case 12:
                strFieldName = "MDRequestMsg";
                break;
            case 13:
                strFieldName = "ManageCom";
                break;
            case 14:
                strFieldName = "Operator";
                break;
            case 15:
                strFieldName = "MakeDate";
                break;
            case 16:
                strFieldName = "MakeTime";
                break;
            case 17:
                strFieldName = "ModifyDate";
                break;
            case 18:
                strFieldName = "ModifyTime";
                break;
            case 19:
                strFieldName = "StandByFlag1";
                break;
            case 20:
                strFieldName = "AccidentAmnt";
                break;
            case 21:
                strFieldName = "AccidentPrem";
                break;
            case 22:
                strFieldName = "MdAmnt";
                break;
            case 23:
                strFieldName = "MdPrem";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "SERIALNO":
                return Schema.TYPE_STRING;
            case "PRTNO":
                return Schema.TYPE_STRING;
            case "CHECKNUM":
                return Schema.TYPE_INT;
            case "APPNTNO":
                return Schema.TYPE_STRING;
            case "APPNTNAME":
                return Schema.TYPE_STRING;
            case "ACCIDENTFLAG":
                return Schema.TYPE_STRING;
            case "ACCIDENTPRODUCTCODE":
                return Schema.TYPE_STRING;
            case "ACCIDENTPRODRISKAMNT":
                return Schema.TYPE_DOUBLE;
            case "ACCIDENTREQUESTMSG":
                return Schema.TYPE_STRING;
            case "MDFLAG":
                return Schema.TYPE_STRING;
            case "MDPRODUCTCODE":
                return Schema.TYPE_STRING;
            case "MDPRODRISKAMNT":
                return Schema.TYPE_DOUBLE;
            case "MDREQUESTMSG":
                return Schema.TYPE_STRING;
            case "MANAGECOM":
                return Schema.TYPE_STRING;
            case "OPERATOR":
                return Schema.TYPE_STRING;
            case "MAKEDATE":
                return Schema.TYPE_STRING;
            case "MAKETIME":
                return Schema.TYPE_STRING;
            case "MODIFYDATE":
                return Schema.TYPE_STRING;
            case "MODIFYTIME":
                return Schema.TYPE_STRING;
            case "STANDBYFLAG1":
                return Schema.TYPE_STRING;
            case "ACCIDENTAMNT":
                return Schema.TYPE_DOUBLE;
            case "ACCIDENTPREM":
                return Schema.TYPE_DOUBLE;
            case "MDAMNT":
                return Schema.TYPE_DOUBLE;
            case "MDPREM":
                return Schema.TYPE_DOUBLE;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_STRING;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_INT;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_STRING;
            case 6:
                return Schema.TYPE_STRING;
            case 7:
                return Schema.TYPE_DOUBLE;
            case 8:
                return Schema.TYPE_STRING;
            case 9:
                return Schema.TYPE_STRING;
            case 10:
                return Schema.TYPE_STRING;
            case 11:
                return Schema.TYPE_DOUBLE;
            case 12:
                return Schema.TYPE_STRING;
            case 13:
                return Schema.TYPE_STRING;
            case 14:
                return Schema.TYPE_STRING;
            case 15:
                return Schema.TYPE_STRING;
            case 16:
                return Schema.TYPE_STRING;
            case 17:
                return Schema.TYPE_STRING;
            case 18:
                return Schema.TYPE_STRING;
            case 19:
                return Schema.TYPE_STRING;
            case 20:
                return Schema.TYPE_DOUBLE;
            case 21:
                return Schema.TYPE_DOUBLE;
            case 22:
                return Schema.TYPE_DOUBLE;
            case 23:
                return Schema.TYPE_DOUBLE;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("SerialNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SerialNo));
        }
        if (FCode.equalsIgnoreCase("PrtNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PrtNo));
        }
        if (FCode.equalsIgnoreCase("CheckNum")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CheckNum));
        }
        if (FCode.equalsIgnoreCase("AppntNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AppntNo));
        }
        if (FCode.equalsIgnoreCase("AppntName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AppntName));
        }
        if (FCode.equalsIgnoreCase("AccidentFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AccidentFlag));
        }
        if (FCode.equalsIgnoreCase("AccidentProductCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AccidentProductCode));
        }
        if (FCode.equalsIgnoreCase("AccidentProdRiskAmnt")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AccidentProdRiskAmnt));
        }
        if (FCode.equalsIgnoreCase("AccidentRequestMsg")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AccidentRequestMsg));
        }
        if (FCode.equalsIgnoreCase("MDFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MDFlag));
        }
        if (FCode.equalsIgnoreCase("MDProductCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MDProductCode));
        }
        if (FCode.equalsIgnoreCase("MDProdRiskAmnt")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MDProdRiskAmnt));
        }
        if (FCode.equalsIgnoreCase("MDRequestMsg")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MDRequestMsg));
        }
        if (FCode.equalsIgnoreCase("ManageCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeDate));
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyDate));
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (FCode.equalsIgnoreCase("StandByFlag1")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StandByFlag1));
        }
        if (FCode.equalsIgnoreCase("AccidentAmnt")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AccidentAmnt));
        }
        if (FCode.equalsIgnoreCase("AccidentPrem")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AccidentPrem));
        }
        if (FCode.equalsIgnoreCase("MdAmnt")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MdAmnt));
        }
        if (FCode.equalsIgnoreCase("MdPrem")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MdPrem));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = String.valueOf(SerialNo);
                break;
            case 1:
                strFieldValue = String.valueOf(PrtNo);
                break;
            case 2:
                strFieldValue = String.valueOf(CheckNum);
                break;
            case 3:
                strFieldValue = String.valueOf(AppntNo);
                break;
            case 4:
                strFieldValue = String.valueOf(AppntName);
                break;
            case 5:
                strFieldValue = String.valueOf(AccidentFlag);
                break;
            case 6:
                strFieldValue = String.valueOf(AccidentProductCode);
                break;
            case 7:
                strFieldValue = String.valueOf(AccidentProdRiskAmnt);
                break;
            case 8:
                strFieldValue = String.valueOf(AccidentRequestMsg);
                break;
            case 9:
                strFieldValue = String.valueOf(MDFlag);
                break;
            case 10:
                strFieldValue = String.valueOf(MDProductCode);
                break;
            case 11:
                strFieldValue = String.valueOf(MDProdRiskAmnt);
                break;
            case 12:
                strFieldValue = String.valueOf(MDRequestMsg);
                break;
            case 13:
                strFieldValue = String.valueOf(ManageCom);
                break;
            case 14:
                strFieldValue = String.valueOf(Operator);
                break;
            case 15:
                strFieldValue = String.valueOf(MakeDate);
                break;
            case 16:
                strFieldValue = String.valueOf(MakeTime);
                break;
            case 17:
                strFieldValue = String.valueOf(ModifyDate);
                break;
            case 18:
                strFieldValue = String.valueOf(ModifyTime);
                break;
            case 19:
                strFieldValue = String.valueOf(StandByFlag1);
                break;
            case 20:
                strFieldValue = String.valueOf(AccidentAmnt);
                break;
            case 21:
                strFieldValue = String.valueOf(AccidentPrem);
                break;
            case 22:
                strFieldValue = String.valueOf(MdAmnt);
                break;
            case 23:
                strFieldValue = String.valueOf(MdPrem);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("SerialNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                SerialNo = FValue.trim();
            }
            else
                SerialNo = null;
        }
        if (FCode.equalsIgnoreCase("PrtNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                PrtNo = FValue.trim();
            }
            else
                PrtNo = null;
        }
        if (FCode.equalsIgnoreCase("CheckNum")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                CheckNum = i;
            }
        }
        if (FCode.equalsIgnoreCase("AppntNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                AppntNo = FValue.trim();
            }
            else
                AppntNo = null;
        }
        if (FCode.equalsIgnoreCase("AppntName")) {
            if( FValue != null && !FValue.equals(""))
            {
                AppntName = FValue.trim();
            }
            else
                AppntName = null;
        }
        if (FCode.equalsIgnoreCase("AccidentFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                AccidentFlag = FValue.trim();
            }
            else
                AccidentFlag = null;
        }
        if (FCode.equalsIgnoreCase("AccidentProductCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                AccidentProductCode = FValue.trim();
            }
            else
                AccidentProductCode = null;
        }
        if (FCode.equalsIgnoreCase("AccidentProdRiskAmnt")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                AccidentProdRiskAmnt = d;
            }
        }
        if (FCode.equalsIgnoreCase("AccidentRequestMsg")) {
            if( FValue != null && !FValue.equals(""))
            {
                AccidentRequestMsg = FValue.trim();
            }
            else
                AccidentRequestMsg = null;
        }
        if (FCode.equalsIgnoreCase("MDFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                MDFlag = FValue.trim();
            }
            else
                MDFlag = null;
        }
        if (FCode.equalsIgnoreCase("MDProductCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                MDProductCode = FValue.trim();
            }
            else
                MDProductCode = null;
        }
        if (FCode.equalsIgnoreCase("MDProdRiskAmnt")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                MDProdRiskAmnt = d;
            }
        }
        if (FCode.equalsIgnoreCase("MDRequestMsg")) {
            if( FValue != null && !FValue.equals(""))
            {
                MDRequestMsg = FValue.trim();
            }
            else
                MDRequestMsg = null;
        }
        if (FCode.equalsIgnoreCase("ManageCom")) {
            if( FValue != null && !FValue.equals(""))
            {
                ManageCom = FValue.trim();
            }
            else
                ManageCom = null;
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            if( FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
                Operator = null;
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeDate = FValue.trim();
            }
            else
                MakeDate = null;
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
                MakeTime = null;
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyDate = FValue.trim();
            }
            else
                ModifyDate = null;
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
                ModifyTime = null;
        }
        if (FCode.equalsIgnoreCase("StandByFlag1")) {
            if( FValue != null && !FValue.equals(""))
            {
                StandByFlag1 = FValue.trim();
            }
            else
                StandByFlag1 = null;
        }
        if (FCode.equalsIgnoreCase("AccidentAmnt")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                AccidentAmnt = d;
            }
        }
        if (FCode.equalsIgnoreCase("AccidentPrem")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                AccidentPrem = d;
            }
        }
        if (FCode.equalsIgnoreCase("MdAmnt")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                MdAmnt = d;
            }
        }
        if (FCode.equalsIgnoreCase("MdPrem")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                MdPrem = d;
            }
        }
        return true;
    }


    public String toString() {
    return "LCCIITCCheckPojo [" +
            "SerialNo="+SerialNo +
            ", PrtNo="+PrtNo +
            ", CheckNum="+CheckNum +
            ", AppntNo="+AppntNo +
            ", AppntName="+AppntName +
            ", AccidentFlag="+AccidentFlag +
            ", AccidentProductCode="+AccidentProductCode +
            ", AccidentProdRiskAmnt="+AccidentProdRiskAmnt +
            ", AccidentRequestMsg="+AccidentRequestMsg +
            ", MDFlag="+MDFlag +
            ", MDProductCode="+MDProductCode +
            ", MDProdRiskAmnt="+MDProdRiskAmnt +
            ", MDRequestMsg="+MDRequestMsg +
            ", ManageCom="+ManageCom +
            ", Operator="+Operator +
            ", MakeDate="+MakeDate +
            ", MakeTime="+MakeTime +
            ", ModifyDate="+ModifyDate +
            ", ModifyTime="+ModifyTime +
            ", StandByFlag1="+StandByFlag1 +
            ", AccidentAmnt="+AccidentAmnt +
            ", AccidentPrem="+AccidentPrem +
            ", MdAmnt="+MdAmnt +
            ", MdPrem="+MdPrem +"]";
    }
}
