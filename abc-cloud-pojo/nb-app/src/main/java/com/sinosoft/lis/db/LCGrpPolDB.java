/**
 * Copyright (c) 2018 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.db;

import com.sinosoft.lis.schema.LCGrpPolSchema;
import com.sinosoft.lis.vschema.LCGrpPolSet;
import com.sinosoft.utility.*;

import java.sql.*;

/**
 * <p>ClassName: LCGrpPolDB </p>
 * <p>Description: DB层数据库操作类文件 </p>
 * <p>Copyright: Copyright (c) 2018</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2018-05-23
 */
public class LCGrpPolDB extends LCGrpPolSchema {
    // @Field
    private Connection con;
    private DBOper db;
    /**
    * flag = true: 传入Connection
    * flag = false: 不传入Connection
    **/
    private boolean mflag = false;

    public CErrors mErrors = new CErrors();        // 错误信息

    /**
     * 为批量操作而准备的语句和游标对象
     */
    private ResultSet mResultSet = null;
    private Statement mStatement = null;
    // @Constructor
    public LCGrpPolDB( Connection tConnection ) {
        con = tConnection;
        db = new DBOper( con, "LCGrpPol" );
        mflag = true;
    }

    public LCGrpPolDB() {
        con = null;
        db = new DBOper( "LCGrpPol" );
        mflag = false;
    }

    // @Method
    public boolean deleteSQL() {
        LCGrpPolSchema tSchema = this.getSchema();
        if (db.deleteSQL(tSchema)) {
             return true;
        } else {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LCGrpPolDB";
            tError.functionName = "deleteSQL";
            tError.errorMessage = "操作失败!";
            this.mErrors .addOneError(tError);
            return false;
        }
    }

    public int getCount() {
        LCGrpPolSchema tSchema = this.getSchema();

        int tCount = db.getCount(tSchema);
        if (tCount < 0) {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LCGrpPolDB";
            tError.functionName = "getCount";
            tError.errorMessage = "操作失败!";
            this.mErrors .addOneError(tError);

            return -1;
        }

        return tCount;
    }

    public boolean delete() {
        PreparedStatement pstmt = null;

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try {
            pstmt = con.prepareStatement("DELETE FROM LCGrpPol WHERE  1=1  AND GrpPolNo = ?");
            if(this.getGrpPolNo() == null || this.getGrpPolNo().equals("null")) {
            	pstmt.setNull(1, 12);
            } else {
            	pstmt.setString(1, this.getGrpPolNo());
            }
            pstmt.executeUpdate();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LCGrpPolDB";
            tError.functionName = "delete()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);
            ex.printStackTrace();

            try {
                pstmt.close();
            } catch (Exception e){}

            if(!mflag) {
                try {
                    con.close();
                } catch (Exception e){}
            }

            return false;
        }

        if(!mflag) {
            try {
                con.close();
            } catch (Exception e){}
        }

        return true;
    }

    public boolean update() {
        PreparedStatement pstmt = null;

        SQLString sqlObj = new SQLString("LCGrpPol");
        sqlObj.setSQL(2, this);
        String sql = sqlObj.getSQL();

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try {
            pstmt = con.prepareStatement("UPDATE LCGrpPol SET  GrpPolNo = ? , GrpContNo = ? , GrpProposalNo = ? , PrtNo = ? , SaleChnl = ? , SalesWay = ? , SalesWaySub = ? , BizNature = ? , PolIssuPlat = ? , KindCode = ? , RiskCode = ? , RiskVersion = ? , AgentCom = ? , AgentCode = ? , AgentGroup = ? , AgentCode1 = ? , CustomerNo = ? , GrpName = ? , PayEndDate = ? , PaytoDate = ? , PeakLine = ? , GetLimit = ? , GetRate = ? , BonusRate = ? , MaxMedFee = ? , EmployeeRate = ? , FamilyRate = ? , ExpPeoples = ? , ExpPremium = ? , ExpAmnt = ? , ManageFeeRate = ? , PayIntv = ? , CValiDate = ? , Peoples2 = ? , Mult = ? , Prem = ? , Amnt = ? , SumPrem = ? , SumPay = ? , Dif = ? , ApproveFlag = ? , ApproveCode = ? , ApproveDate = ? , ApproveTime = ? , UWFlag = ? , UWOperator = ? , UWDate = ? , UWTime = ? , AppFlag = ? , State = ? , StandbyFlag1 = ? , StandbyFlag2 = ? , StandbyFlag3 = ? , ManageCom = ? , ComCode = ? , Operator = ? , MakeDate = ? , MakeTime = ? , ModifyOperator = ? , ModifyDate = ? , ModifyTime = ? , OnWorkPeoples = ? , OffWorkPeoples = ? , OtherPeoples = ? , RelaPeoples = ? , RelaMatePeoples = ? , RelaYoungPeoples = ? , RelaOtherPeoples = ? , WaitPeriod = ? , BonusFlag = ? , InitRate = ? , DistriFlag = ? , FeeRate = ? , RenewFlag = ? , DistriRate = ? , Currency = ? , StandPrem = ? , Lang = ? , CleanDisRate = ? , AchvAccruFlag = ? WHERE  1=1  AND GrpPolNo = ?");
            if(this.getGrpPolNo() == null || this.getGrpPolNo().equals("null")) {
            	pstmt.setNull(1, 12);
            } else {
            	pstmt.setString(1, this.getGrpPolNo());
            }
            if(this.getGrpContNo() == null || this.getGrpContNo().equals("null")) {
            	pstmt.setNull(2, 12);
            } else {
            	pstmt.setString(2, this.getGrpContNo());
            }
            if(this.getGrpProposalNo() == null || this.getGrpProposalNo().equals("null")) {
            	pstmt.setNull(3, 12);
            } else {
            	pstmt.setString(3, this.getGrpProposalNo());
            }
            if(this.getPrtNo() == null || this.getPrtNo().equals("null")) {
            	pstmt.setNull(4, 12);
            } else {
            	pstmt.setString(4, this.getPrtNo());
            }
            if(this.getSaleChnl() == null || this.getSaleChnl().equals("null")) {
            	pstmt.setNull(5, 12);
            } else {
            	pstmt.setString(5, this.getSaleChnl());
            }
            if(this.getSalesWay() == null || this.getSalesWay().equals("null")) {
            	pstmt.setNull(6, 12);
            } else {
            	pstmt.setString(6, this.getSalesWay());
            }
            if(this.getSalesWaySub() == null || this.getSalesWaySub().equals("null")) {
            	pstmt.setNull(7, 12);
            } else {
            	pstmt.setString(7, this.getSalesWaySub());
            }
            if(this.getBizNature() == null || this.getBizNature().equals("null")) {
            	pstmt.setNull(8, 12);
            } else {
            	pstmt.setString(8, this.getBizNature());
            }
            if(this.getPolIssuPlat() == null || this.getPolIssuPlat().equals("null")) {
            	pstmt.setNull(9, 12);
            } else {
            	pstmt.setString(9, this.getPolIssuPlat());
            }
            if(this.getKindCode() == null || this.getKindCode().equals("null")) {
            	pstmt.setNull(10, 12);
            } else {
            	pstmt.setString(10, this.getKindCode());
            }
            if(this.getRiskCode() == null || this.getRiskCode().equals("null")) {
            	pstmt.setNull(11, 12);
            } else {
            	pstmt.setString(11, this.getRiskCode());
            }
            if(this.getRiskVersion() == null || this.getRiskVersion().equals("null")) {
            	pstmt.setNull(12, 12);
            } else {
            	pstmt.setString(12, this.getRiskVersion());
            }
            if(this.getAgentCom() == null || this.getAgentCom().equals("null")) {
            	pstmt.setNull(13, 12);
            } else {
            	pstmt.setString(13, this.getAgentCom());
            }
            if(this.getAgentCode() == null || this.getAgentCode().equals("null")) {
            	pstmt.setNull(14, 12);
            } else {
            	pstmt.setString(14, this.getAgentCode());
            }
            if(this.getAgentGroup() == null || this.getAgentGroup().equals("null")) {
            	pstmt.setNull(15, 12);
            } else {
            	pstmt.setString(15, this.getAgentGroup());
            }
            if(this.getAgentCode1() == null || this.getAgentCode1().equals("null")) {
            	pstmt.setNull(16, 12);
            } else {
            	pstmt.setString(16, this.getAgentCode1());
            }
            if(this.getCustomerNo() == null || this.getCustomerNo().equals("null")) {
            	pstmt.setNull(17, 12);
            } else {
            	pstmt.setString(17, this.getCustomerNo());
            }
            if(this.getGrpName() == null || this.getGrpName().equals("null")) {
            	pstmt.setNull(18, 12);
            } else {
            	pstmt.setString(18, this.getGrpName());
            }
            if(this.getPayEndDate() == null || this.getPayEndDate().equals("null")) {
            	pstmt.setNull(19, 93);
            } else {
            	pstmt.setDate(19, Date.valueOf(this.getPayEndDate()));
            }
            if(this.getPaytoDate() == null || this.getPaytoDate().equals("null")) {
            	pstmt.setNull(20, 93);
            } else {
            	pstmt.setDate(20, Date.valueOf(this.getPaytoDate()));
            }
            pstmt.setDouble(21, this.getPeakLine());
            pstmt.setDouble(22, this.getGetLimit());
            pstmt.setDouble(23, this.getGetRate());
            pstmt.setDouble(24, this.getBonusRate());
            pstmt.setDouble(25, this.getMaxMedFee());
            pstmt.setDouble(26, this.getEmployeeRate());
            pstmt.setDouble(27, this.getFamilyRate());
            pstmt.setInt(28, this.getExpPeoples());
            pstmt.setDouble(29, this.getExpPremium());
            pstmt.setDouble(30, this.getExpAmnt());
            pstmt.setDouble(31, this.getManageFeeRate());
            pstmt.setInt(32, this.getPayIntv());
            if(this.getCValiDate() == null || this.getCValiDate().equals("null")) {
            	pstmt.setNull(33, 93);
            } else {
            	pstmt.setDate(33, Date.valueOf(this.getCValiDate()));
            }
            pstmt.setInt(34, this.getPeoples2());
            pstmt.setDouble(35, this.getMult());
            pstmt.setDouble(36, this.getPrem());
            pstmt.setDouble(37, this.getAmnt());
            pstmt.setDouble(38, this.getSumPrem());
            pstmt.setDouble(39, this.getSumPay());
            pstmt.setDouble(40, this.getDif());
            if(this.getApproveFlag() == null || this.getApproveFlag().equals("null")) {
            	pstmt.setNull(41, 12);
            } else {
            	pstmt.setString(41, this.getApproveFlag());
            }
            if(this.getApproveCode() == null || this.getApproveCode().equals("null")) {
            	pstmt.setNull(42, 12);
            } else {
            	pstmt.setString(42, this.getApproveCode());
            }
            if(this.getApproveDate() == null || this.getApproveDate().equals("null")) {
            	pstmt.setNull(43, 93);
            } else {
            	pstmt.setDate(43, Date.valueOf(this.getApproveDate()));
            }
            if(this.getApproveTime() == null || this.getApproveTime().equals("null")) {
            	pstmt.setNull(44, 12);
            } else {
            	pstmt.setString(44, this.getApproveTime());
            }
            if(this.getUWFlag() == null || this.getUWFlag().equals("null")) {
            	pstmt.setNull(45, 12);
            } else {
            	pstmt.setString(45, this.getUWFlag());
            }
            if(this.getUWOperator() == null || this.getUWOperator().equals("null")) {
            	pstmt.setNull(46, 12);
            } else {
            	pstmt.setString(46, this.getUWOperator());
            }
            if(this.getUWDate() == null || this.getUWDate().equals("null")) {
            	pstmt.setNull(47, 93);
            } else {
            	pstmt.setDate(47, Date.valueOf(this.getUWDate()));
            }
            if(this.getUWTime() == null || this.getUWTime().equals("null")) {
            	pstmt.setNull(48, 12);
            } else {
            	pstmt.setString(48, this.getUWTime());
            }
            if(this.getAppFlag() == null || this.getAppFlag().equals("null")) {
            	pstmt.setNull(49, 12);
            } else {
            	pstmt.setString(49, this.getAppFlag());
            }
            if(this.getState() == null || this.getState().equals("null")) {
            	pstmt.setNull(50, 12);
            } else {
            	pstmt.setString(50, this.getState());
            }
            if(this.getStandbyFlag1() == null || this.getStandbyFlag1().equals("null")) {
            	pstmt.setNull(51, 12);
            } else {
            	pstmt.setString(51, this.getStandbyFlag1());
            }
            if(this.getStandbyFlag2() == null || this.getStandbyFlag2().equals("null")) {
            	pstmt.setNull(52, 12);
            } else {
            	pstmt.setString(52, this.getStandbyFlag2());
            }
            if(this.getStandbyFlag3() == null || this.getStandbyFlag3().equals("null")) {
            	pstmt.setNull(53, 12);
            } else {
            	pstmt.setString(53, this.getStandbyFlag3());
            }
            if(this.getManageCom() == null || this.getManageCom().equals("null")) {
            	pstmt.setNull(54, 12);
            } else {
            	pstmt.setString(54, this.getManageCom());
            }
            if(this.getComCode() == null || this.getComCode().equals("null")) {
            	pstmt.setNull(55, 12);
            } else {
            	pstmt.setString(55, this.getComCode());
            }
            if(this.getOperator() == null || this.getOperator().equals("null")) {
            	pstmt.setNull(56, 12);
            } else {
            	pstmt.setString(56, this.getOperator());
            }
            if(this.getMakeDate() == null || this.getMakeDate().equals("null")) {
            	pstmt.setNull(57, 93);
            } else {
            	pstmt.setDate(57, Date.valueOf(this.getMakeDate()));
            }
            if(this.getMakeTime() == null || this.getMakeTime().equals("null")) {
            	pstmt.setNull(58, 12);
            } else {
            	pstmt.setString(58, this.getMakeTime());
            }
            if(this.getModifyOperator() == null || this.getModifyOperator().equals("null")) {
            	pstmt.setNull(59, 12);
            } else {
            	pstmt.setString(59, this.getModifyOperator());
            }
            if(this.getModifyDate() == null || this.getModifyDate().equals("null")) {
            	pstmt.setNull(60, 93);
            } else {
            	pstmt.setDate(60, Date.valueOf(this.getModifyDate()));
            }
            if(this.getModifyTime() == null || this.getModifyTime().equals("null")) {
            	pstmt.setNull(61, 12);
            } else {
            	pstmt.setString(61, this.getModifyTime());
            }
            pstmt.setInt(62, this.getOnWorkPeoples());
            pstmt.setInt(63, this.getOffWorkPeoples());
            pstmt.setInt(64, this.getOtherPeoples());
            pstmt.setInt(65, this.getRelaPeoples());
            pstmt.setInt(66, this.getRelaMatePeoples());
            pstmt.setInt(67, this.getRelaYoungPeoples());
            pstmt.setInt(68, this.getRelaOtherPeoples());
            pstmt.setInt(69, this.getWaitPeriod());
            if(this.getBonusFlag() == null || this.getBonusFlag().equals("null")) {
            	pstmt.setNull(70, 12);
            } else {
            	pstmt.setString(70, this.getBonusFlag());
            }
            pstmt.setDouble(71, this.getInitRate());
            if(this.getDistriFlag() == null || this.getDistriFlag().equals("null")) {
            	pstmt.setNull(72, 12);
            } else {
            	pstmt.setString(72, this.getDistriFlag());
            }
            pstmt.setDouble(73, this.getFeeRate());
            if(this.getRenewFlag() == null || this.getRenewFlag().equals("null")) {
            	pstmt.setNull(74, 12);
            } else {
            	pstmt.setString(74, this.getRenewFlag());
            }
            pstmt.setDouble(75, this.getDistriRate());
            if(this.getCurrency() == null || this.getCurrency().equals("null")) {
            	pstmt.setNull(76, 12);
            } else {
            	pstmt.setString(76, this.getCurrency());
            }
            pstmt.setDouble(77, this.getStandPrem());
            if(this.getLang() == null || this.getLang().equals("null")) {
            	pstmt.setNull(78, 12);
            } else {
            	pstmt.setString(78, this.getLang());
            }
            pstmt.setLong(79, this.getCleanDisRate());
            if(this.getAchvAccruFlag() == null || this.getAchvAccruFlag().equals("null")) {
            	pstmt.setNull(80, 12);
            } else {
            	pstmt.setString(80, this.getAchvAccruFlag());
            }
            // set where condition
            if(this.getGrpPolNo() == null || this.getGrpPolNo().equals("null")) {
            	pstmt.setNull(81, 12);
            } else {
            	pstmt.setString(81, this.getGrpPolNo());
            }
            pstmt.executeUpdate();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LCGrpPolDB";
            tError.functionName = "update()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);
            ex.printStackTrace();

            try {
                pstmt.close();
            } catch (Exception e){}

            if( !mflag ) {
                try {
                    con.close();
                } catch (Exception e){}
            }

            return false;
        }

        if(!mflag) {
            try {
                con.close();
            } catch (Exception e){}
        }

        return true;
    }

    public boolean insert() {
        PreparedStatement pstmt = null;

        SQLString sqlObj = new SQLString("LCGrpPol");
        sqlObj.setSQL(1, this);
        String sql = sqlObj.getSQL();

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try {
            pstmt = con.prepareStatement("INSERT INTO LCGrpPol VALUES( ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ?)");
            if(this.getGrpPolNo() == null || this.getGrpPolNo().equals("null")) {
            	pstmt.setNull(1, 12);
            } else {
            	pstmt.setString(1, this.getGrpPolNo());
            }
            if(this.getGrpContNo() == null || this.getGrpContNo().equals("null")) {
            	pstmt.setNull(2, 12);
            } else {
            	pstmt.setString(2, this.getGrpContNo());
            }
            if(this.getGrpProposalNo() == null || this.getGrpProposalNo().equals("null")) {
            	pstmt.setNull(3, 12);
            } else {
            	pstmt.setString(3, this.getGrpProposalNo());
            }
            if(this.getPrtNo() == null || this.getPrtNo().equals("null")) {
            	pstmt.setNull(4, 12);
            } else {
            	pstmt.setString(4, this.getPrtNo());
            }
            if(this.getSaleChnl() == null || this.getSaleChnl().equals("null")) {
            	pstmt.setNull(5, 12);
            } else {
            	pstmt.setString(5, this.getSaleChnl());
            }
            if(this.getSalesWay() == null || this.getSalesWay().equals("null")) {
            	pstmt.setNull(6, 12);
            } else {
            	pstmt.setString(6, this.getSalesWay());
            }
            if(this.getSalesWaySub() == null || this.getSalesWaySub().equals("null")) {
            	pstmt.setNull(7, 12);
            } else {
            	pstmt.setString(7, this.getSalesWaySub());
            }
            if(this.getBizNature() == null || this.getBizNature().equals("null")) {
            	pstmt.setNull(8, 12);
            } else {
            	pstmt.setString(8, this.getBizNature());
            }
            if(this.getPolIssuPlat() == null || this.getPolIssuPlat().equals("null")) {
            	pstmt.setNull(9, 12);
            } else {
            	pstmt.setString(9, this.getPolIssuPlat());
            }
            if(this.getKindCode() == null || this.getKindCode().equals("null")) {
            	pstmt.setNull(10, 12);
            } else {
            	pstmt.setString(10, this.getKindCode());
            }
            if(this.getRiskCode() == null || this.getRiskCode().equals("null")) {
            	pstmt.setNull(11, 12);
            } else {
            	pstmt.setString(11, this.getRiskCode());
            }
            if(this.getRiskVersion() == null || this.getRiskVersion().equals("null")) {
            	pstmt.setNull(12, 12);
            } else {
            	pstmt.setString(12, this.getRiskVersion());
            }
            if(this.getAgentCom() == null || this.getAgentCom().equals("null")) {
            	pstmt.setNull(13, 12);
            } else {
            	pstmt.setString(13, this.getAgentCom());
            }
            if(this.getAgentCode() == null || this.getAgentCode().equals("null")) {
            	pstmt.setNull(14, 12);
            } else {
            	pstmt.setString(14, this.getAgentCode());
            }
            if(this.getAgentGroup() == null || this.getAgentGroup().equals("null")) {
            	pstmt.setNull(15, 12);
            } else {
            	pstmt.setString(15, this.getAgentGroup());
            }
            if(this.getAgentCode1() == null || this.getAgentCode1().equals("null")) {
            	pstmt.setNull(16, 12);
            } else {
            	pstmt.setString(16, this.getAgentCode1());
            }
            if(this.getCustomerNo() == null || this.getCustomerNo().equals("null")) {
            	pstmt.setNull(17, 12);
            } else {
            	pstmt.setString(17, this.getCustomerNo());
            }
            if(this.getGrpName() == null || this.getGrpName().equals("null")) {
            	pstmt.setNull(18, 12);
            } else {
            	pstmt.setString(18, this.getGrpName());
            }
            if(this.getPayEndDate() == null || this.getPayEndDate().equals("null")) {
            	pstmt.setNull(19, 93);
            } else {
            	pstmt.setDate(19, Date.valueOf(this.getPayEndDate()));
            }
            if(this.getPaytoDate() == null || this.getPaytoDate().equals("null")) {
            	pstmt.setNull(20, 93);
            } else {
            	pstmt.setDate(20, Date.valueOf(this.getPaytoDate()));
            }
            pstmt.setDouble(21, this.getPeakLine());
            pstmt.setDouble(22, this.getGetLimit());
            pstmt.setDouble(23, this.getGetRate());
            pstmt.setDouble(24, this.getBonusRate());
            pstmt.setDouble(25, this.getMaxMedFee());
            pstmt.setDouble(26, this.getEmployeeRate());
            pstmt.setDouble(27, this.getFamilyRate());
            pstmt.setInt(28, this.getExpPeoples());
            pstmt.setDouble(29, this.getExpPremium());
            pstmt.setDouble(30, this.getExpAmnt());
            pstmt.setDouble(31, this.getManageFeeRate());
            pstmt.setInt(32, this.getPayIntv());
            if(this.getCValiDate() == null || this.getCValiDate().equals("null")) {
            	pstmt.setNull(33, 93);
            } else {
            	pstmt.setDate(33, Date.valueOf(this.getCValiDate()));
            }
            pstmt.setInt(34, this.getPeoples2());
            pstmt.setDouble(35, this.getMult());
            pstmt.setDouble(36, this.getPrem());
            pstmt.setDouble(37, this.getAmnt());
            pstmt.setDouble(38, this.getSumPrem());
            pstmt.setDouble(39, this.getSumPay());
            pstmt.setDouble(40, this.getDif());
            if(this.getApproveFlag() == null || this.getApproveFlag().equals("null")) {
            	pstmt.setNull(41, 12);
            } else {
            	pstmt.setString(41, this.getApproveFlag());
            }
            if(this.getApproveCode() == null || this.getApproveCode().equals("null")) {
            	pstmt.setNull(42, 12);
            } else {
            	pstmt.setString(42, this.getApproveCode());
            }
            if(this.getApproveDate() == null || this.getApproveDate().equals("null")) {
            	pstmt.setNull(43, 93);
            } else {
            	pstmt.setDate(43, Date.valueOf(this.getApproveDate()));
            }
            if(this.getApproveTime() == null || this.getApproveTime().equals("null")) {
            	pstmt.setNull(44, 12);
            } else {
            	pstmt.setString(44, this.getApproveTime());
            }
            if(this.getUWFlag() == null || this.getUWFlag().equals("null")) {
            	pstmt.setNull(45, 12);
            } else {
            	pstmt.setString(45, this.getUWFlag());
            }
            if(this.getUWOperator() == null || this.getUWOperator().equals("null")) {
            	pstmt.setNull(46, 12);
            } else {
            	pstmt.setString(46, this.getUWOperator());
            }
            if(this.getUWDate() == null || this.getUWDate().equals("null")) {
            	pstmt.setNull(47, 93);
            } else {
            	pstmt.setDate(47, Date.valueOf(this.getUWDate()));
            }
            if(this.getUWTime() == null || this.getUWTime().equals("null")) {
            	pstmt.setNull(48, 12);
            } else {
            	pstmt.setString(48, this.getUWTime());
            }
            if(this.getAppFlag() == null || this.getAppFlag().equals("null")) {
            	pstmt.setNull(49, 12);
            } else {
            	pstmt.setString(49, this.getAppFlag());
            }
            if(this.getState() == null || this.getState().equals("null")) {
            	pstmt.setNull(50, 12);
            } else {
            	pstmt.setString(50, this.getState());
            }
            if(this.getStandbyFlag1() == null || this.getStandbyFlag1().equals("null")) {
            	pstmt.setNull(51, 12);
            } else {
            	pstmt.setString(51, this.getStandbyFlag1());
            }
            if(this.getStandbyFlag2() == null || this.getStandbyFlag2().equals("null")) {
            	pstmt.setNull(52, 12);
            } else {
            	pstmt.setString(52, this.getStandbyFlag2());
            }
            if(this.getStandbyFlag3() == null || this.getStandbyFlag3().equals("null")) {
            	pstmt.setNull(53, 12);
            } else {
            	pstmt.setString(53, this.getStandbyFlag3());
            }
            if(this.getManageCom() == null || this.getManageCom().equals("null")) {
            	pstmt.setNull(54, 12);
            } else {
            	pstmt.setString(54, this.getManageCom());
            }
            if(this.getComCode() == null || this.getComCode().equals("null")) {
            	pstmt.setNull(55, 12);
            } else {
            	pstmt.setString(55, this.getComCode());
            }
            if(this.getOperator() == null || this.getOperator().equals("null")) {
            	pstmt.setNull(56, 12);
            } else {
            	pstmt.setString(56, this.getOperator());
            }
            if(this.getMakeDate() == null || this.getMakeDate().equals("null")) {
            	pstmt.setNull(57, 93);
            } else {
            	pstmt.setDate(57, Date.valueOf(this.getMakeDate()));
            }
            if(this.getMakeTime() == null || this.getMakeTime().equals("null")) {
            	pstmt.setNull(58, 12);
            } else {
            	pstmt.setString(58, this.getMakeTime());
            }
            if(this.getModifyOperator() == null || this.getModifyOperator().equals("null")) {
            	pstmt.setNull(59, 12);
            } else {
            	pstmt.setString(59, this.getModifyOperator());
            }
            if(this.getModifyDate() == null || this.getModifyDate().equals("null")) {
            	pstmt.setNull(60, 93);
            } else {
            	pstmt.setDate(60, Date.valueOf(this.getModifyDate()));
            }
            if(this.getModifyTime() == null || this.getModifyTime().equals("null")) {
            	pstmt.setNull(61, 12);
            } else {
            	pstmt.setString(61, this.getModifyTime());
            }
            pstmt.setInt(62, this.getOnWorkPeoples());
            pstmt.setInt(63, this.getOffWorkPeoples());
            pstmt.setInt(64, this.getOtherPeoples());
            pstmt.setInt(65, this.getRelaPeoples());
            pstmt.setInt(66, this.getRelaMatePeoples());
            pstmt.setInt(67, this.getRelaYoungPeoples());
            pstmt.setInt(68, this.getRelaOtherPeoples());
            pstmt.setInt(69, this.getWaitPeriod());
            if(this.getBonusFlag() == null || this.getBonusFlag().equals("null")) {
            	pstmt.setNull(70, 12);
            } else {
            	pstmt.setString(70, this.getBonusFlag());
            }
            pstmt.setDouble(71, this.getInitRate());
            if(this.getDistriFlag() == null || this.getDistriFlag().equals("null")) {
            	pstmt.setNull(72, 12);
            } else {
            	pstmt.setString(72, this.getDistriFlag());
            }
            pstmt.setDouble(73, this.getFeeRate());
            if(this.getRenewFlag() == null || this.getRenewFlag().equals("null")) {
            	pstmt.setNull(74, 12);
            } else {
            	pstmt.setString(74, this.getRenewFlag());
            }
            pstmt.setDouble(75, this.getDistriRate());
            if(this.getCurrency() == null || this.getCurrency().equals("null")) {
            	pstmt.setNull(76, 12);
            } else {
            	pstmt.setString(76, this.getCurrency());
            }
            pstmt.setDouble(77, this.getStandPrem());
            if(this.getLang() == null || this.getLang().equals("null")) {
            	pstmt.setNull(78, 12);
            } else {
            	pstmt.setString(78, this.getLang());
            }
            pstmt.setLong(79, this.getCleanDisRate());
            if(this.getAchvAccruFlag() == null || this.getAchvAccruFlag().equals("null")) {
            	pstmt.setNull(80, 12);
            } else {
            	pstmt.setString(80, this.getAchvAccruFlag());
            }
            // execute sql
            pstmt.executeUpdate();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LCGrpPolDB";
            tError.functionName = "insert()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors.addOneError(tError);
            ex.printStackTrace();

            try {
                pstmt.close();
            } catch (Exception e){}

            if( !mflag ) {
                try {
                    con.close();
                } catch (Exception e){}
            }

            return false;
        }

        if(!mflag) {
            try {
                con.close();
            } catch (Exception e){}
        }

        return true;
    }

    public boolean getInfo() {
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        if(!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            pstmt = con.prepareStatement("SELECT * FROM LCGrpPol WHERE  1=1  AND GrpPolNo = ?", 
                ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
            if(this.getGrpPolNo() == null || this.getGrpPolNo().equals("null")) {
            	pstmt.setNull(1, 12);
            } else {
            	pstmt.setString(1, this.getGrpPolNo());
            }
            rs = pstmt.executeQuery();
            int i = 0;
            while (rs.next()) {
                i++;
                if (!this.setSchema(rs,i)) {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "LCGrpPolDB";
                    tError.functionName = "getInfo";
                    tError.errorMessage = "取数失败!";
                    this.mErrors .addOneError(tError);

                    try{ rs.close(); } catch( Exception ex ) {}
                    try{ pstmt.close(); } catch( Exception ex1 ) {}

                    if (!mflag) {
                        try {
                            con.close();
                        }
                        catch(Exception et){}
                    }
                    return false;
                }
                break;
            }
            try{ rs.close(); } catch( Exception ex2 ) {}
            try{ pstmt.close(); } catch( Exception ex3 ) {}

            if( i == 0 ) {
                if (!mflag) {
                    try {
                        con.close();
                    }
                    catch(Exception et){}
                }
                return false;
            }
        }
        catch(Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LCGrpPolDB";
            tError.functionName = "getInfo";
            tError.errorMessage = ExceptionUtils.exceptionToString(e);
            this.mErrors .addOneError(tError);

            try{ rs.close(); } catch( Exception ex ) {}
            try{ pstmt.close(); } catch( Exception ex1 ) {}

            if (!mflag) {
                try {
                    con.close();
                }
                catch(Exception et){}
            }
            return false;
        }
        // 断开数据库连接
        if (!mflag) {
            try {
                con.close();
            }
            catch(Exception e){}
        }

        return true;
    }

    public LCGrpPolSet query() {
        Statement stmt = null;
        ResultSet rs = null;
        LCGrpPolSet aLCGrpPolSet = new LCGrpPolSet();

        if (!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            stmt = con.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
            SQLString sqlObj = new SQLString("LCGrpPol");
            LCGrpPolSchema aSchema = this.getSchema();
            sqlObj.setSQL(5, aSchema);
            String sql = sqlObj.getSQL();

            rs = stmt.executeQuery(sql);
            int i = 0;
            while (rs.next()) {
                i++;
                if (i>10000) {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "LCGrpPolDB";
                    tError.functionName = "query";
                    tError.errorMessage = "SQL语句返回结果集太多，请换用其他方式!";
                    this.mErrors .addOneError(tError);
                        break;
                }
                LCGrpPolSchema s1 = new LCGrpPolSchema();
                s1.setSchema(rs,i);
                aLCGrpPolSet.add(s1);
            }
            try{ rs.close(); } catch( Exception ex ) {}
            try{ stmt.close(); } catch( Exception ex1 ) {}
        }
        catch(Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LCGrpPolDB";
            tError.functionName = "query";
            tError.errorMessage = ExceptionUtils.exceptionToString(e);
            this.mErrors.addOneError(tError);
            e.printStackTrace();

            try { rs.close(); } catch (Exception ex2) { ex2.printStackTrace(); }
            try { stmt.close(); } catch(Exception ex3) { ex3.printStackTrace(); }

            if (!mflag) {
                try {
                    con.close();
                }
                catch(Exception et){et.printStackTrace();}
            }
        }

        if (!mflag) {
            try {
                con.close();
            }
            catch(Exception e){e.printStackTrace();}
        }

        return aLCGrpPolSet;
    }

    public LCGrpPolSet executeQuery(String sql)  {
        Statement stmt = null;
        ResultSet rs = null;
        LCGrpPolSet aLCGrpPolSet = new LCGrpPolSet();

        if (!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            stmt = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);

            rs = stmt.executeQuery(StrTool.GBKToUnicode(sql));
            int i = 0;
            while (rs.next()) {
                i++;
                if (i>10000) {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "LCGrpPolDB";
                    tError.functionName = "executeQuery";
                    tError.errorMessage = "SQL语句返回结果集太多，请换用其他方式!";
                    this.mErrors .addOneError(tError);
                        break;
                }
                LCGrpPolSchema s1 = new LCGrpPolSchema();
                if (!s1.setSchema(rs,i)) {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "LCGrpPolDB";
                    tError.functionName = "executeQuery";
                    tError.errorMessage = "sql语句有误，请查看表名及字段名信息!";
                    this.mErrors .addOneError(tError);
                }
                aLCGrpPolSet.add(s1);
            }
            try{ rs.close(); } catch( Exception ex ) {}
            try{ stmt.close(); } catch( Exception ex1 ) {}
        }
        catch(Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LCGrpPolDB";
            tError.functionName = "executeQuery";
            tError.errorMessage = ExceptionUtils.exceptionToString(e);
            this.mErrors .addOneError(tError);

            try{ rs.close(); } catch( Exception ex2 ) {}
            try{ stmt.close(); } catch( Exception ex3 ) {}

            if (!mflag) {
                try {
                    con.close();
                }
                catch(Exception et){}
            }
        }

        if (!mflag) {
            try {
                con.close();
            }
            catch(Exception e){}
        }

        return aLCGrpPolSet;
    }

    public LCGrpPolSet query(int nStart, int nCount) {
        Statement stmt = null;
        ResultSet rs = null;
        LCGrpPolSet aLCGrpPolSet = new LCGrpPolSet();

        if (!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            stmt = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);
            SQLString sqlObj = new SQLString("LCGrpPol");
            LCGrpPolSchema aSchema = this.getSchema();
            sqlObj.setSQL(5,aSchema);
            String sql = sqlObj.getSQL();

            rs = stmt.executeQuery(sql);
            int i = 0;
            while (rs.next()) {
                i++;

                if( i < nStart ) {
                    continue;
                }

                if( i >= nStart + nCount ) {
                    break;
                }

                LCGrpPolSchema s1 = new LCGrpPolSchema();
                s1.setSchema(rs,i);
                aLCGrpPolSet.add(s1);
            }
            try{ rs.close(); } catch( Exception ex ) {}
            try{ stmt.close(); } catch( Exception ex1 ) {}
        }
        catch(Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LCGrpPolDB";
            tError.functionName = "query";
            tError.errorMessage = ExceptionUtils.exceptionToString(e);
            this.mErrors .addOneError(tError);

            try{ rs.close(); } catch( Exception ex2 ) {}
            try{ stmt.close(); } catch( Exception ex3 ) {}

            if (!mflag) {
                try {
                    con.close();
                }
                catch(Exception et){}
            }
        }

        if (!mflag) {
            try {
                con.close();
            }
            catch(Exception e){}
        }

        return aLCGrpPolSet;
    }

    public LCGrpPolSet executeQuery(String sql, int nStart, int nCount) {
        Statement stmt = null;
        ResultSet rs = null;
        LCGrpPolSet aLCGrpPolSet = new LCGrpPolSet();

        if (!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            stmt = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);

            rs = stmt.executeQuery(StrTool.GBKToUnicode(sql));
            int i = 0;
            while (rs.next()) {
                i++;

                if( i < nStart ) {
                    continue;
                }

                if( i >= nStart + nCount ) {
                    break;
                }

                LCGrpPolSchema s1 = new LCGrpPolSchema();
                if (!s1.setSchema(rs,i)) {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "LCGrpPolDB";
                    tError.functionName = "executeQuery";
                    tError.errorMessage = "sql语句有误，请查看表名及字段名信息!";
                    this.mErrors .addOneError(tError);
                }
                aLCGrpPolSet.add(s1);
            }
            try{ rs.close(); } catch( Exception ex ) {}
            try{ stmt.close(); } catch( Exception ex1 ) {}
        }
        catch(Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LCGrpPolDB";
            tError.functionName = "executeQuery";
            tError.errorMessage = ExceptionUtils.exceptionToString(e);
            this.mErrors .addOneError(tError);

            try{ rs.close(); } catch( Exception ex2 ) {}
            try{ stmt.close(); } catch( Exception ex3 ) {}

            if (!mflag) {
                try {
                    con.close();
                }
                catch(Exception et){}
            }
        }

        if (!mflag) {
            try {
                con.close();
            }
            catch(Exception e){}
        }

        return aLCGrpPolSet;
    }

    public boolean update(String strWherePart) {
        Statement stmt = null;

        if (!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            stmt = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);
            SQLString sqlObj = new SQLString("LCGrpPol");
            LCGrpPolSchema aSchema = this.getSchema();
            sqlObj.setSQL(2,aSchema);
            String sql = "update LCGrpPol " + sqlObj.getUpdPart() + " where " + strWherePart;

            int operCount = stmt.executeUpdate(sql);
            if (operCount == 0) {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "LCGrpPolDB";
                tError.functionName = "update";
                tError.errorMessage = "更新数据失败!";
                this.mErrors .addOneError(tError);

                if (!mflag) {
                    try {
                        con.close();
                    }
                    catch(Exception et){}
                }
                return false;
            }
        }
        catch(Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LCGrpPolDB";
            tError.functionName = "update";
            tError.errorMessage = ExceptionUtils.exceptionToString(e);
            this.mErrors .addOneError(tError);

            try{ stmt.close(); } catch( Exception ex1 ) {}

            if (!mflag) {
                try {
                    con.close();
                }
                catch(Exception et){}
            }
            return false;
        }
        // 断开数据库连接
        if (!mflag) {
            try {
                con.close();
            }
            catch(Exception e){}
        }

        return true;
    }

    /**
     * 准备数据查询条件
     * @param strSQL String
     * @return boolean
     */
    public boolean prepareData(String strSQL) {
        if (mResultSet != null) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LCGrpPolDB";
            tError.functionName = "prepareData";
            tError.errorMessage = "数据集非空，程序在准备数据集之后，没有关闭！";
            this.mErrors.addOneError(tError);
            return false;
        }

        if (!mflag) {
            con = DBConnPool.getConnection();
        }
        try {
            mStatement = con.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
            mResultSet = mStatement.executeQuery(StrTool.GBKToUnicode(strSQL));
        }
        catch (Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LCGrpPolDB";
            tError.functionName = "prepareData";
            tError.errorMessage = ExceptionUtils.exceptionToString(e);
            this.mErrors.addOneError(tError);
            try {
                mResultSet.close();
            }
            catch (Exception ex2) {}
            try {
                mStatement.close();
            }
            catch (Exception ex3) {}
            if (!mflag) {
                try {
                    con.close();
                }
                catch (Exception et) {}
            }
            return false;
        }

        if (!mflag) {
            try {
                con.close();
            }
            catch (Exception e) {}
        }
        return true;
    }

    /**
     * 获取数据集
     * @return boolean
     */
    public boolean hasMoreData() {
        boolean flag = true;
        if (null == mResultSet) {
            CError tError = new CError();
            tError.moduleName = "LCGrpPolDB";
            tError.functionName = "hasMoreData";
            tError.errorMessage = "数据集为空，请先准备数据集！";
            this.mErrors.addOneError(tError);
            return false;
        }
        try {
            flag = mResultSet.next();
        }
        catch (Exception ex) {
            CError tError = new CError();
            tError.moduleName = "LCGrpPolDB";
            tError.functionName = "hasMoreData";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors.addOneError(tError);
            try {
                mResultSet.close();
                mResultSet = null;
            }
            catch (Exception ex2) {}
            try {
                mStatement.close();
                mStatement = null;
            }
            catch (Exception ex3) {}
            if (!mflag) {
                try {
                    con.close();
                }
                catch (Exception et) {}
            }
            return false;
        }
        return flag;
    }
    /**
     * 获取定量数据
     * @return LCGrpPolSet
     */
    public LCGrpPolSet getData() {
        int tCount = 0;
        LCGrpPolSet tLCGrpPolSet = new LCGrpPolSet();
        LCGrpPolSchema tLCGrpPolSchema = null;
        if (null == mResultSet) {
            CError tError = new CError();
            tError.moduleName = "LCGrpPolDB";
            tError.functionName = "getData";
            tError.errorMessage = "数据集为空，请先准备数据集！";
            this.mErrors.addOneError(tError);
            return null;
        }
        try {
            tCount = 1;
            tLCGrpPolSchema = new LCGrpPolSchema();
            tLCGrpPolSchema.setSchema(mResultSet, 1);
            tLCGrpPolSet.add(tLCGrpPolSchema);
            //注意mResultSet.next()的作用
            while (tCount++ < SysConst.FETCHCOUNT) {
                if (mResultSet.next()) {
                    tLCGrpPolSchema = new LCGrpPolSchema();
                    tLCGrpPolSchema.setSchema(mResultSet, 1);
                    tLCGrpPolSet.add(tLCGrpPolSchema);
                }
            }
        }
        catch (Exception ex) {
            CError tError = new CError();
            tError.moduleName = "LCGrpPolDB";
            tError.functionName = "getData";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors.addOneError(tError);
            try {
                mResultSet.close();
                mResultSet = null;
            }
            catch (Exception ex2) {}
            try {
                mStatement.close();
                mStatement = null;
            }
            catch (Exception ex3) {}
            if (!mflag) {
                try {
                    con.close();
                }
                catch (Exception et) {}
            }
            return null;
        }
        return tLCGrpPolSet;
    }
    /**
     * 关闭数据集
     * @return boolean
     */
    public boolean closeData() {
        boolean flag = true;
        try {
            if (null == mResultSet) {
                CError tError = new CError();
                tError.moduleName = "LCGrpPolDB";
                tError.functionName = "closeData";
                tError.errorMessage = "数据集已经关闭了！";
                this.mErrors.addOneError(tError);
                flag = false;
            } else {
                mResultSet.close();
                mResultSet = null;
            }
        }
        catch (Exception ex2) {
            CError tError = new CError();
            tError.moduleName = "LCGrpPolDB";
            tError.functionName = "closeData";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex2);
            this.mErrors.addOneError(tError);
            flag = false;
        }
        try {
            if (null == mStatement) {
                CError tError = new CError();
                tError.moduleName = "LCGrpPolDB";
                tError.functionName = "closeData";
                tError.errorMessage = "语句已经关闭了！";
                this.mErrors.addOneError(tError);
                flag = false;
            } else {
                mStatement.close();
                mStatement = null;
            }
        }
        catch (Exception ex3) {
            CError tError = new CError();
            tError.moduleName = "LCGrpPolDB";
            tError.functionName = "closeData";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex3);
            this.mErrors.addOneError(tError);
            flag = false;
        }
        return flag;
    }
}
