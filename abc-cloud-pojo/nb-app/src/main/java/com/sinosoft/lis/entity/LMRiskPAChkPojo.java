/**
 * Copyright (c) 2017 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.entity;

import com.sinosoft.cloud.cache.annotation.RedisPrimaryHKey;
import com.sinosoft.utility.Pojo;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;

import java.io.Serializable;

/**
 * <p>ClassName: LMRiskPAChkPojo </p>
 * <p>Copyright: Copyright (c) 2017</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2017-11-06
 */
public class LMRiskPAChkPojo implements Pojo,Serializable {
    // @Field
    /** 险种编码 */
    @RedisPrimaryHKey
    private String RiskCode;
    /** 销售渠道 */
    @RedisPrimaryHKey
    private String Salechnl;
    /** 销售类型 */
    @RedisPrimaryHKey
    private String Selltype;
    /** 保费校验标志 */
    private String PremFlag;
    /** 保额校验标志 */
    private String AmntFlag;
    /** 备用字段1 */
    private String StandByString1;
    /** 备用字段2 */
    private String StandByString2;
    /** 备用字段3 */
    private String StandByString3;
    /** 备用字段4 */
    private String StandByString4;
    /** 备用字段5 */
    private String StandByString5;


    public static final int FIELDNUM = 10;    // 数据库表的字段个数
    public String getRiskCode() {
        return RiskCode;
    }
    public void setRiskCode(String aRiskCode) {
        RiskCode = aRiskCode;
    }
    public String getSalechnl() {
        return Salechnl;
    }
    public void setSalechnl(String aSalechnl) {
        Salechnl = aSalechnl;
    }
    public String getSelltype() {
        return Selltype;
    }
    public void setSelltype(String aSelltype) {
        Selltype = aSelltype;
    }
    public String getPremFlag() {
        return PremFlag;
    }
    public void setPremFlag(String aPremFlag) {
        PremFlag = aPremFlag;
    }
    public String getAmntFlag() {
        return AmntFlag;
    }
    public void setAmntFlag(String aAmntFlag) {
        AmntFlag = aAmntFlag;
    }
    public String getStandByString1() {
        return StandByString1;
    }
    public void setStandByString1(String aStandByString1) {
        StandByString1 = aStandByString1;
    }
    public String getStandByString2() {
        return StandByString2;
    }
    public void setStandByString2(String aStandByString2) {
        StandByString2 = aStandByString2;
    }
    public String getStandByString3() {
        return StandByString3;
    }
    public void setStandByString3(String aStandByString3) {
        StandByString3 = aStandByString3;
    }
    public String getStandByString4() {
        return StandByString4;
    }
    public void setStandByString4(String aStandByString4) {
        StandByString4 = aStandByString4;
    }
    public String getStandByString5() {
        return StandByString5;
    }
    public void setStandByString5(String aStandByString5) {
        StandByString5 = aStandByString5;
    }

    /**
     * 取得Schema拥有字段的数量
     * @return: int
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("RiskCode") ) {
            return 0;
        }
        if( strFieldName.equals("Salechnl") ) {
            return 1;
        }
        if( strFieldName.equals("Selltype") ) {
            return 2;
        }
        if( strFieldName.equals("PremFlag") ) {
            return 3;
        }
        if( strFieldName.equals("AmntFlag") ) {
            return 4;
        }
        if( strFieldName.equals("StandByString1") ) {
            return 5;
        }
        if( strFieldName.equals("StandByString2") ) {
            return 6;
        }
        if( strFieldName.equals("StandByString3") ) {
            return 7;
        }
        if( strFieldName.equals("StandByString4") ) {
            return 8;
        }
        if( strFieldName.equals("StandByString5") ) {
            return 9;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     * @param: nFieldIndex int
     * @return: String
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "RiskCode";
                break;
            case 1:
                strFieldName = "Salechnl";
                break;
            case 2:
                strFieldName = "Selltype";
                break;
            case 3:
                strFieldName = "PremFlag";
                break;
            case 4:
                strFieldName = "AmntFlag";
                break;
            case 5:
                strFieldName = "StandByString1";
                break;
            case 6:
                strFieldName = "StandByString2";
                break;
            case 7:
                strFieldName = "StandByString3";
                break;
            case 8:
                strFieldName = "StandByString4";
                break;
            case 9:
                strFieldName = "StandByString5";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "RISKCODE":
                return Schema.TYPE_STRING;
            case "SALECHNL":
                return Schema.TYPE_STRING;
            case "SELLTYPE":
                return Schema.TYPE_STRING;
            case "PREMFLAG":
                return Schema.TYPE_STRING;
            case "AMNTFLAG":
                return Schema.TYPE_STRING;
            case "STANDBYSTRING1":
                return Schema.TYPE_STRING;
            case "STANDBYSTRING2":
                return Schema.TYPE_STRING;
            case "STANDBYSTRING3":
                return Schema.TYPE_STRING;
            case "STANDBYSTRING4":
                return Schema.TYPE_STRING;
            case "STANDBYSTRING5":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: nFieldIndex int
     * @return: int
     **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_STRING;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_STRING;
            case 6:
                return Schema.TYPE_STRING;
            case 7:
                return Schema.TYPE_STRING;
            case 8:
                return Schema.TYPE_STRING;
            case 9:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode String 希望取得的字段名
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("RiskCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RiskCode));
        }
        if (FCode.equalsIgnoreCase("Salechnl")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Salechnl));
        }
        if (FCode.equalsIgnoreCase("Selltype")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Selltype));
        }
        if (FCode.equalsIgnoreCase("PremFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PremFlag));
        }
        if (FCode.equalsIgnoreCase("AmntFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AmntFlag));
        }
        if (FCode.equalsIgnoreCase("StandByString1")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StandByString1));
        }
        if (FCode.equalsIgnoreCase("StandByString2")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StandByString2));
        }
        if (FCode.equalsIgnoreCase("StandByString3")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StandByString3));
        }
        if (FCode.equalsIgnoreCase("StandByString4")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StandByString4));
        }
        if (FCode.equalsIgnoreCase("StandByString5")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StandByString5));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex int 指定的字段索引值
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = String.valueOf(RiskCode);
                break;
            case 1:
                strFieldValue = String.valueOf(Salechnl);
                break;
            case 2:
                strFieldValue = String.valueOf(Selltype);
                break;
            case 3:
                strFieldValue = String.valueOf(PremFlag);
                break;
            case 4:
                strFieldValue = String.valueOf(AmntFlag);
                break;
            case 5:
                strFieldValue = String.valueOf(StandByString1);
                break;
            case 6:
                strFieldValue = String.valueOf(StandByString2);
                break;
            case 7:
                strFieldValue = String.valueOf(StandByString3);
                break;
            case 8:
                strFieldValue = String.valueOf(StandByString4);
                break;
            case 9:
                strFieldValue = String.valueOf(StandByString5);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode String 需要赋值的对象
     * @param: FValue String 要赋的值
     * @return: boolean
     **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("RiskCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                RiskCode = FValue.trim();
            }
            else
                RiskCode = null;
        }
        if (FCode.equalsIgnoreCase("Salechnl")) {
            if( FValue != null && !FValue.equals(""))
            {
                Salechnl = FValue.trim();
            }
            else
                Salechnl = null;
        }
        if (FCode.equalsIgnoreCase("Selltype")) {
            if( FValue != null && !FValue.equals(""))
            {
                Selltype = FValue.trim();
            }
            else
                Selltype = null;
        }
        if (FCode.equalsIgnoreCase("PremFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                PremFlag = FValue.trim();
            }
            else
                PremFlag = null;
        }
        if (FCode.equalsIgnoreCase("AmntFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                AmntFlag = FValue.trim();
            }
            else
                AmntFlag = null;
        }
        if (FCode.equalsIgnoreCase("StandByString1")) {
            if( FValue != null && !FValue.equals(""))
            {
                StandByString1 = FValue.trim();
            }
            else
                StandByString1 = null;
        }
        if (FCode.equalsIgnoreCase("StandByString2")) {
            if( FValue != null && !FValue.equals(""))
            {
                StandByString2 = FValue.trim();
            }
            else
                StandByString2 = null;
        }
        if (FCode.equalsIgnoreCase("StandByString3")) {
            if( FValue != null && !FValue.equals(""))
            {
                StandByString3 = FValue.trim();
            }
            else
                StandByString3 = null;
        }
        if (FCode.equalsIgnoreCase("StandByString4")) {
            if( FValue != null && !FValue.equals(""))
            {
                StandByString4 = FValue.trim();
            }
            else
                StandByString4 = null;
        }
        if (FCode.equalsIgnoreCase("StandByString5")) {
            if( FValue != null && !FValue.equals(""))
            {
                StandByString5 = FValue.trim();
            }
            else
                StandByString5 = null;
        }
        return true;
    }


    public String toString() {
        return "LMRiskPAChkPojo [" +
                "RiskCode="+RiskCode +
                ", Salechnl="+Salechnl +
                ", Selltype="+Selltype +
                ", PremFlag="+PremFlag +
                ", AmntFlag="+AmntFlag +
                ", StandByString1="+StandByString1 +
                ", StandByString2="+StandByString2 +
                ", StandByString3="+StandByString3 +
                ", StandByString4="+StandByString4 +
                ", StandByString5="+StandByString5 +"]";
    }
}
