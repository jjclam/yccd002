/**
 * Copyright (c) 2017 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.entity;

import com.sinosoft.cloud.cache.annotation.RedisPrimaryHKey;
import com.sinosoft.utility.Pojo;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;

import java.io.Serializable;

/**
 * <p>ClassName: RiskAmnt_DeathPojo </p>
 * <p>Copyright: Copyright (c) 2017</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2017-10-17
 */
public class RiskAmnt_DeathPojo implements Pojo,Serializable {
    // @Field
    /** Riskcode */
    @RedisPrimaryHKey
    private String Riskcode; 
    /** Adultflag */
    @RedisPrimaryHKey
    private String AdultFlag; 
    /** Calcode */
    private String CalCode; 
    /** Remark */
    private String Remark; 


    public static final int FIELDNUM = 4;    // 数据库表的字段个数
    public String getRiskcode() {
        return Riskcode;
    }
    public void setRiskcode(String aRiskcode) {
        Riskcode = aRiskcode;
    }
    public String getAdultFlag() {
        return AdultFlag;
    }
    public void setAdultFlag(String aAdultFlag) {
        AdultFlag = aAdultFlag;
    }
    public String getCalCode() {
        return CalCode;
    }
    public void setCalCode(String aCalCode) {
        CalCode = aCalCode;
    }
    public String getRemark() {
        return Remark;
    }
    public void setRemark(String aRemark) {
        Remark = aRemark;
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("Riskcode") ) {
            return 0;
        }
        if( strFieldName.equals("AdultFlag") ) {
            return 1;
        }
        if( strFieldName.equals("CalCode") ) {
            return 2;
        }
        if( strFieldName.equals("Remark") ) {
            return 3;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "Riskcode";
                break;
            case 1:
                strFieldName = "AdultFlag";
                break;
            case 2:
                strFieldName = "CalCode";
                break;
            case 3:
                strFieldName = "Remark";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "RISKCODE":
                return Schema.TYPE_STRING;
            case "ADULTFLAG":
                return Schema.TYPE_STRING;
            case "CALCODE":
                return Schema.TYPE_STRING;
            case "REMARK":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_STRING;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("Riskcode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Riskcode));
        }
        if (FCode.equalsIgnoreCase("AdultFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AdultFlag));
        }
        if (FCode.equalsIgnoreCase("CalCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CalCode));
        }
        if (FCode.equalsIgnoreCase("Remark")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Remark));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = String.valueOf(Riskcode);
                break;
            case 1:
                strFieldValue = String.valueOf(AdultFlag);
                break;
            case 2:
                strFieldValue = String.valueOf(CalCode);
                break;
            case 3:
                strFieldValue = String.valueOf(Remark);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("Riskcode")) {
            if( FValue != null && !FValue.equals(""))
            {
                Riskcode = FValue.trim();
            }
            else
                Riskcode = null;
        }
        if (FCode.equalsIgnoreCase("AdultFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                AdultFlag = FValue.trim();
            }
            else
                AdultFlag = null;
        }
        if (FCode.equalsIgnoreCase("CalCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                CalCode = FValue.trim();
            }
            else
                CalCode = null;
        }
        if (FCode.equalsIgnoreCase("Remark")) {
            if( FValue != null && !FValue.equals(""))
            {
                Remark = FValue.trim();
            }
            else
                Remark = null;
        }
        return true;
    }


    public String toString() {
    return "RiskAmnt_DeathPojo [" +
            "Riskcode="+Riskcode +
            ", AdultFlag="+AdultFlag +
            ", CalCode="+CalCode +
            ", Remark="+Remark +"]";
    }
}
