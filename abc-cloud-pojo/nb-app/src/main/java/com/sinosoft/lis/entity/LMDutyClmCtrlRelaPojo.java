/**
 * Copyright (c) 2017 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.entity;

import com.sinosoft.utility.Pojo;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;

import java.io.Serializable;

/**
 * <p>ClassName: LMDutyClmCtrlRelaPojo </p>
 * <p>Copyright: Copyright (c) 2017</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2017-10-17
 */
public class LMDutyClmCtrlRelaPojo implements Pojo,Serializable {
    // @Field
    /** 理赔控制编码 */
    private String ClaimCtrlCode; 
    /** 险种代码 */
    private String RiskCode; 
    /** 责任代码 */
    private String DutyCode; 
    /** 给付代码 */
    private String GetDutyCode; 
    /** 给付责任类型 */
    private String GetDutyKind; 
    /** 账单项目编码 */
    private String Feecode; 
    /** Serialno */
    private int SerialNo; 


    public static final int FIELDNUM = 7;    // 数据库表的字段个数
    public String getClaimCtrlCode() {
        return ClaimCtrlCode;
    }
    public void setClaimCtrlCode(String aClaimCtrlCode) {
        ClaimCtrlCode = aClaimCtrlCode;
    }
    public String getRiskCode() {
        return RiskCode;
    }
    public void setRiskCode(String aRiskCode) {
        RiskCode = aRiskCode;
    }
    public String getDutyCode() {
        return DutyCode;
    }
    public void setDutyCode(String aDutyCode) {
        DutyCode = aDutyCode;
    }
    public String getGetDutyCode() {
        return GetDutyCode;
    }
    public void setGetDutyCode(String aGetDutyCode) {
        GetDutyCode = aGetDutyCode;
    }
    public String getGetDutyKind() {
        return GetDutyKind;
    }
    public void setGetDutyKind(String aGetDutyKind) {
        GetDutyKind = aGetDutyKind;
    }
    public String getFeecode() {
        return Feecode;
    }
    public void setFeecode(String aFeecode) {
        Feecode = aFeecode;
    }
    public int getSerialNo() {
        return SerialNo;
    }
    public void setSerialNo(int aSerialNo) {
        SerialNo = aSerialNo;
    }
    public void setSerialNo(String aSerialNo) {
        if (aSerialNo != null && !aSerialNo.equals("")) {
            Integer tInteger = new Integer(aSerialNo);
            int i = tInteger.intValue();
            SerialNo = i;
        }
    }


    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("ClaimCtrlCode") ) {
            return 0;
        }
        if( strFieldName.equals("RiskCode") ) {
            return 1;
        }
        if( strFieldName.equals("DutyCode") ) {
            return 2;
        }
        if( strFieldName.equals("GetDutyCode") ) {
            return 3;
        }
        if( strFieldName.equals("GetDutyKind") ) {
            return 4;
        }
        if( strFieldName.equals("Feecode") ) {
            return 5;
        }
        if( strFieldName.equals("SerialNo") ) {
            return 6;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "ClaimCtrlCode";
                break;
            case 1:
                strFieldName = "RiskCode";
                break;
            case 2:
                strFieldName = "DutyCode";
                break;
            case 3:
                strFieldName = "GetDutyCode";
                break;
            case 4:
                strFieldName = "GetDutyKind";
                break;
            case 5:
                strFieldName = "Feecode";
                break;
            case 6:
                strFieldName = "SerialNo";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "CLAIMCTRLCODE":
                return Schema.TYPE_STRING;
            case "RISKCODE":
                return Schema.TYPE_STRING;
            case "DUTYCODE":
                return Schema.TYPE_STRING;
            case "GETDUTYCODE":
                return Schema.TYPE_STRING;
            case "GETDUTYKIND":
                return Schema.TYPE_STRING;
            case "FEECODE":
                return Schema.TYPE_STRING;
            case "SERIALNO":
                return Schema.TYPE_INT;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_STRING;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_STRING;
            case 6:
                return Schema.TYPE_INT;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("ClaimCtrlCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ClaimCtrlCode));
        }
        if (FCode.equalsIgnoreCase("RiskCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RiskCode));
        }
        if (FCode.equalsIgnoreCase("DutyCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DutyCode));
        }
        if (FCode.equalsIgnoreCase("GetDutyCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GetDutyCode));
        }
        if (FCode.equalsIgnoreCase("GetDutyKind")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GetDutyKind));
        }
        if (FCode.equalsIgnoreCase("Feecode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Feecode));
        }
        if (FCode.equalsIgnoreCase("SerialNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SerialNo));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = String.valueOf(ClaimCtrlCode);
                break;
            case 1:
                strFieldValue = String.valueOf(RiskCode);
                break;
            case 2:
                strFieldValue = String.valueOf(DutyCode);
                break;
            case 3:
                strFieldValue = String.valueOf(GetDutyCode);
                break;
            case 4:
                strFieldValue = String.valueOf(GetDutyKind);
                break;
            case 5:
                strFieldValue = String.valueOf(Feecode);
                break;
            case 6:
                strFieldValue = String.valueOf(SerialNo);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("ClaimCtrlCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                ClaimCtrlCode = FValue.trim();
            }
            else
                ClaimCtrlCode = null;
        }
        if (FCode.equalsIgnoreCase("RiskCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                RiskCode = FValue.trim();
            }
            else
                RiskCode = null;
        }
        if (FCode.equalsIgnoreCase("DutyCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                DutyCode = FValue.trim();
            }
            else
                DutyCode = null;
        }
        if (FCode.equalsIgnoreCase("GetDutyCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                GetDutyCode = FValue.trim();
            }
            else
                GetDutyCode = null;
        }
        if (FCode.equalsIgnoreCase("GetDutyKind")) {
            if( FValue != null && !FValue.equals(""))
            {
                GetDutyKind = FValue.trim();
            }
            else
                GetDutyKind = null;
        }
        if (FCode.equalsIgnoreCase("Feecode")) {
            if( FValue != null && !FValue.equals(""))
            {
                Feecode = FValue.trim();
            }
            else
                Feecode = null;
        }
        if (FCode.equalsIgnoreCase("SerialNo")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                SerialNo = i;
            }
        }
        return true;
    }


    public String toString() {
    return "LMDutyClmCtrlRelaPojo [" +
            "ClaimCtrlCode="+ClaimCtrlCode +
            ", RiskCode="+RiskCode +
            ", DutyCode="+DutyCode +
            ", GetDutyCode="+GetDutyCode +
            ", GetDutyKind="+GetDutyKind +
            ", Feecode="+Feecode +
            ", SerialNo="+SerialNo +"]";
    }
}
