/**
 * Copyright (c) 2018 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.entity;

import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.Pojo;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;

import java.io.Serializable;

/**
 * <p>ClassName: LCAIPContRegisterPojo </p>
 * <p>Copyright: Copyright (c) 2018</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2018-06-30
 */
public class LCAIPContRegisterPojo implements Pojo,Serializable {
    // @Field
    /** 合同号码 */
    private String ContNo; 
    /** 单证印刷号码 */
    private String CardNo; 
    /** 单证类型码 */
    private String CertifyCode; 
    /** 网点 */
    private String AgentCom; 
    /** 结算标记 */
    private String PayFlag; 
    /** 结算日期 */
    private String  PayDate;
    /** 结算时间 */
    private String PayTime; 
    /** 收费模式 */
    private String ChargeMode; 
    /** 收费类型 */
    private String ChargeType; 
    /** 渠道编码 */
    private String ChannelCode; 
    /** 账户编码 */
    private String InBankCode; 
    /** 账户号码 */
    private String InBankAccNo; 
    /** 账户名称 */
    private String InBankName; 


    public static final int FIELDNUM = 13;    // 数据库表的字段个数

    private FDate fDate = new FDate();        // 处理日期
    public String getContNo() {
        return ContNo;
    }
    public void setContNo(String aContNo) {
        ContNo = aContNo;
    }
    public String getCardNo() {
        return CardNo;
    }
    public void setCardNo(String aCardNo) {
        CardNo = aCardNo;
    }
    public String getCertifyCode() {
        return CertifyCode;
    }
    public void setCertifyCode(String aCertifyCode) {
        CertifyCode = aCertifyCode;
    }
    public String getAgentCom() {
        return AgentCom;
    }
    public void setAgentCom(String aAgentCom) {
        AgentCom = aAgentCom;
    }
    public String getPayFlag() {
        return PayFlag;
    }
    public void setPayFlag(String aPayFlag) {
        PayFlag = aPayFlag;
    }
    public String getPayDate() {
        return PayDate;
    }
    public void setPayDate(String aPayDate) {
        PayDate = aPayDate;
    }
    public String getPayTime() {
        return PayTime;
    }
    public void setPayTime(String aPayTime) {
        PayTime = aPayTime;
    }
    public String getChargeMode() {
        return ChargeMode;
    }
    public void setChargeMode(String aChargeMode) {
        ChargeMode = aChargeMode;
    }
    public String getChargeType() {
        return ChargeType;
    }
    public void setChargeType(String aChargeType) {
        ChargeType = aChargeType;
    }
    public String getChannelCode() {
        return ChannelCode;
    }
    public void setChannelCode(String aChannelCode) {
        ChannelCode = aChannelCode;
    }
    public String getInBankCode() {
        return InBankCode;
    }
    public void setInBankCode(String aInBankCode) {
        InBankCode = aInBankCode;
    }
    public String getInBankAccNo() {
        return InBankAccNo;
    }
    public void setInBankAccNo(String aInBankAccNo) {
        InBankAccNo = aInBankAccNo;
    }
    public String getInBankName() {
        return InBankName;
    }
    public void setInBankName(String aInBankName) {
        InBankName = aInBankName;
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("ContNo") ) {
            return 0;
        }
        if( strFieldName.equals("CardNo") ) {
            return 1;
        }
        if( strFieldName.equals("CertifyCode") ) {
            return 2;
        }
        if( strFieldName.equals("AgentCom") ) {
            return 3;
        }
        if( strFieldName.equals("PayFlag") ) {
            return 4;
        }
        if( strFieldName.equals("PayDate") ) {
            return 5;
        }
        if( strFieldName.equals("PayTime") ) {
            return 6;
        }
        if( strFieldName.equals("ChargeMode") ) {
            return 7;
        }
        if( strFieldName.equals("ChargeType") ) {
            return 8;
        }
        if( strFieldName.equals("ChannelCode") ) {
            return 9;
        }
        if( strFieldName.equals("InBankCode") ) {
            return 10;
        }
        if( strFieldName.equals("InBankAccNo") ) {
            return 11;
        }
        if( strFieldName.equals("InBankName") ) {
            return 12;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "ContNo";
                break;
            case 1:
                strFieldName = "CardNo";
                break;
            case 2:
                strFieldName = "CertifyCode";
                break;
            case 3:
                strFieldName = "AgentCom";
                break;
            case 4:
                strFieldName = "PayFlag";
                break;
            case 5:
                strFieldName = "PayDate";
                break;
            case 6:
                strFieldName = "PayTime";
                break;
            case 7:
                strFieldName = "ChargeMode";
                break;
            case 8:
                strFieldName = "ChargeType";
                break;
            case 9:
                strFieldName = "ChannelCode";
                break;
            case 10:
                strFieldName = "InBankCode";
                break;
            case 11:
                strFieldName = "InBankAccNo";
                break;
            case 12:
                strFieldName = "InBankName";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "CONTNO":
                return Schema.TYPE_STRING;
            case "CARDNO":
                return Schema.TYPE_STRING;
            case "CERTIFYCODE":
                return Schema.TYPE_STRING;
            case "AGENTCOM":
                return Schema.TYPE_STRING;
            case "PAYFLAG":
                return Schema.TYPE_STRING;
            case "PAYDATE":
                return Schema.TYPE_STRING;
            case "PAYTIME":
                return Schema.TYPE_STRING;
            case "CHARGEMODE":
                return Schema.TYPE_STRING;
            case "CHARGETYPE":
                return Schema.TYPE_STRING;
            case "CHANNELCODE":
                return Schema.TYPE_STRING;
            case "INBANKCODE":
                return Schema.TYPE_STRING;
            case "INBANKACCNO":
                return Schema.TYPE_STRING;
            case "INBANKNAME":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_STRING;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_STRING;
            case 6:
                return Schema.TYPE_STRING;
            case 7:
                return Schema.TYPE_STRING;
            case 8:
                return Schema.TYPE_STRING;
            case 9:
                return Schema.TYPE_STRING;
            case 10:
                return Schema.TYPE_STRING;
            case 11:
                return Schema.TYPE_STRING;
            case 12:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("ContNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ContNo));
        }
        if (FCode.equalsIgnoreCase("CardNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CardNo));
        }
        if (FCode.equalsIgnoreCase("CertifyCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CertifyCode));
        }
        if (FCode.equalsIgnoreCase("AgentCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentCom));
        }
        if (FCode.equalsIgnoreCase("PayFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PayFlag));
        }
        if (FCode.equalsIgnoreCase("PayDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PayDate));
        }
        if (FCode.equalsIgnoreCase("PayTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PayTime));
        }
        if (FCode.equalsIgnoreCase("ChargeMode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ChargeMode));
        }
        if (FCode.equalsIgnoreCase("ChargeType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ChargeType));
        }
        if (FCode.equalsIgnoreCase("ChannelCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ChannelCode));
        }
        if (FCode.equalsIgnoreCase("InBankCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InBankCode));
        }
        if (FCode.equalsIgnoreCase("InBankAccNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InBankAccNo));
        }
        if (FCode.equalsIgnoreCase("InBankName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InBankName));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = String.valueOf(ContNo);
                break;
            case 1:
                strFieldValue = String.valueOf(CardNo);
                break;
            case 2:
                strFieldValue = String.valueOf(CertifyCode);
                break;
            case 3:
                strFieldValue = String.valueOf(AgentCom);
                break;
            case 4:
                strFieldValue = String.valueOf(PayFlag);
                break;
            case 5:
                strFieldValue = String.valueOf(PayDate);
                break;
            case 6:
                strFieldValue = String.valueOf(PayTime);
                break;
            case 7:
                strFieldValue = String.valueOf(ChargeMode);
                break;
            case 8:
                strFieldValue = String.valueOf(ChargeType);
                break;
            case 9:
                strFieldValue = String.valueOf(ChannelCode);
                break;
            case 10:
                strFieldValue = String.valueOf(InBankCode);
                break;
            case 11:
                strFieldValue = String.valueOf(InBankAccNo);
                break;
            case 12:
                strFieldValue = String.valueOf(InBankName);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("ContNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                ContNo = FValue.trim();
            }
            else
                ContNo = null;
        }
        if (FCode.equalsIgnoreCase("CardNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                CardNo = FValue.trim();
            }
            else
                CardNo = null;
        }
        if (FCode.equalsIgnoreCase("CertifyCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                CertifyCode = FValue.trim();
            }
            else
                CertifyCode = null;
        }
        if (FCode.equalsIgnoreCase("AgentCom")) {
            if( FValue != null && !FValue.equals(""))
            {
                AgentCom = FValue.trim();
            }
            else
                AgentCom = null;
        }
        if (FCode.equalsIgnoreCase("PayFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                PayFlag = FValue.trim();
            }
            else
                PayFlag = null;
        }
        if (FCode.equalsIgnoreCase("PayDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                PayDate = FValue.trim();
            }
            else
                PayDate = null;
        }
        if (FCode.equalsIgnoreCase("PayTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                PayTime = FValue.trim();
            }
            else
                PayTime = null;
        }
        if (FCode.equalsIgnoreCase("ChargeMode")) {
            if( FValue != null && !FValue.equals(""))
            {
                ChargeMode = FValue.trim();
            }
            else
                ChargeMode = null;
        }
        if (FCode.equalsIgnoreCase("ChargeType")) {
            if( FValue != null && !FValue.equals(""))
            {
                ChargeType = FValue.trim();
            }
            else
                ChargeType = null;
        }
        if (FCode.equalsIgnoreCase("ChannelCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                ChannelCode = FValue.trim();
            }
            else
                ChannelCode = null;
        }
        if (FCode.equalsIgnoreCase("InBankCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                InBankCode = FValue.trim();
            }
            else
                InBankCode = null;
        }
        if (FCode.equalsIgnoreCase("InBankAccNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                InBankAccNo = FValue.trim();
            }
            else
                InBankAccNo = null;
        }
        if (FCode.equalsIgnoreCase("InBankName")) {
            if( FValue != null && !FValue.equals(""))
            {
                InBankName = FValue.trim();
            }
            else
                InBankName = null;
        }
        return true;
    }


    public String toString() {
    return "LCAIPContRegisterPojo [" +
            "ContNo="+ContNo +
            ", CardNo="+CardNo +
            ", CertifyCode="+CertifyCode +
            ", AgentCom="+AgentCom +
            ", PayFlag="+PayFlag +
            ", PayDate="+PayDate +
            ", PayTime="+PayTime +
            ", ChargeMode="+ChargeMode +
            ", ChargeType="+ChargeType +
            ", ChannelCode="+ChannelCode +
            ", InBankCode="+InBankCode +
            ", InBankAccNo="+InBankAccNo +
            ", InBankName="+InBankName +"]";
    }
}
