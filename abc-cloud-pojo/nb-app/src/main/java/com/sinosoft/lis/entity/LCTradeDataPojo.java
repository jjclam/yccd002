/**
 * Copyright (c) 2017 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.Pojo;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>ClassName: lctradedataPojo </p>
 * <p>Copyright: Copyright (c) 2017</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2017-09-05
 */
public class LCTradeDataPojo implements Pojo,Serializable {
    // @Field
    /** Serialid */
    private long Serialid;
    /** Tradecode */
    private String TradeCode;
    /** Transacno */
    private String TransacNo;
    /** Contno */
    private String ContNo;
    /** Prtno */
    private String PrtNo;
    /** Channeltype */
    private String ChannelType;
    /** Resulttype */
    private String ResultType;
    /** Result */
    private String Result;
    /** Requestdate */
    private Date RequestDate;
    /** Responsedate */
    private Date ResponseDate;
    /** Key1 */
    private String key1;
    /** Key2 */
    private String key2;
    /** Key3 */
    private String key3;
    /** Key4 */
    private String key4;


    public static final int FIELDNUM = 14;    // 数据库表的字段个数

    private FDate fDate = new FDate();        // 处理日期
    public long getSerialid() {
        return Serialid;
    }
    public void setSerialid(long aSerialid) {
        Serialid = aSerialid;
    }
    public void setSerialid(String aSerialid) {
        if (aSerialid != null && !aSerialid.equals("")) {
            Serialid = new Long(aSerialid).longValue();
        }
    }

    public String getTradeCode() {
        return TradeCode;
    }
    public void setTradeCode(String aTradeCode) {
        TradeCode = aTradeCode;
    }
    public String getTransacNo() {
        return TransacNo;
    }
    public void setTransacNo(String aTransacNo) {
        TransacNo = aTransacNo;
    }
    public String getContNo() {
        return ContNo;
    }
    public void setContNo(String aContNo) {
        ContNo = aContNo;
    }
    public String getPrtNo() {
        return PrtNo;
    }
    public void setPrtNo(String aPrtNo) {
        PrtNo = aPrtNo;
    }
    public String getChannelType() {
        return ChannelType;
    }
    public void setChannelType(String aChannelType) {
        ChannelType = aChannelType;
    }
    public String getResultType() {
        return ResultType;
    }
    public void setResultType(String aResultType) {
        ResultType = aResultType;
    }
    public String getResult() {
        return Result;
    }
    public void setResult(String aResult) {
        Result = aResult;
    }
    @JsonFormat(pattern = "yyyy-MM-dd",timezone="GMT+8")
    public Date getRequestDate() {
        if( RequestDate != null ) {
            return RequestDate;
        } else {
            return null;
        }
    }
    public void setRequestDate(Date aRequestDate) {
        RequestDate = aRequestDate;
    }
    public void setRequestDate(String aRequestDate) {
        if (aRequestDate != null && !aRequestDate.equals("")) {
            RequestDate = fDate.getDate(aRequestDate);
        } else
            RequestDate = null;
    }

    @JsonFormat(pattern = "yyyy-MM-dd",timezone="GMT+8")
    public Date getResponseDate() {
        if( ResponseDate != null ) {
            return ResponseDate;
        } else {
            return null;
        }
    }
    public void setResponseDate(Date aResponseDate) {
        ResponseDate = aResponseDate;
    }
    public void setResponseDate(String aResponseDate) {
        if (aResponseDate != null && !aResponseDate.equals("")) {
            ResponseDate = fDate.getDate(aResponseDate);
        } else
            ResponseDate = null;
    }

    public String getKey1() {
        return key1;
    }
    public void setKey1(String akey1) {
        key1 = akey1;
    }
    public String getKey2() {
        return key2;
    }
    public void setKey2(String akey2) {
        key2 = akey2;
    }
    public String getKey3() {
        return key3;
    }
    public void setKey3(String akey3) {
        key3 = akey3;
    }
    public String getKey4() {
        return key4;
    }
    public void setKey4(String akey4) {
        key4 = akey4;
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("Serialid") ) {
            return 0;
        }
        if( strFieldName.equals("TradeCode") ) {
            return 1;
        }
        if( strFieldName.equals("TransacNo") ) {
            return 2;
        }
        if( strFieldName.equals("ContNo") ) {
            return 3;
        }
        if( strFieldName.equals("PrtNo") ) {
            return 4;
        }
        if( strFieldName.equals("ChannelType") ) {
            return 5;
        }
        if( strFieldName.equals("ResultType") ) {
            return 6;
        }
        if( strFieldName.equals("Result") ) {
            return 7;
        }
        if( strFieldName.equals("RequestDate") ) {
            return 8;
        }
        if( strFieldName.equals("ResponseDate") ) {
            return 9;
        }
        if( strFieldName.equals("key1") ) {
            return 10;
        }
        if( strFieldName.equals("key2") ) {
            return 11;
        }
        if( strFieldName.equals("key3") ) {
            return 12;
        }
        if( strFieldName.equals("key4") ) {
            return 13;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "Serialid";
                break;
            case 1:
                strFieldName = "TradeCode";
                break;
            case 2:
                strFieldName = "TransacNo";
                break;
            case 3:
                strFieldName = "ContNo";
                break;
            case 4:
                strFieldName = "PrtNo";
                break;
            case 5:
                strFieldName = "ChannelType";
                break;
            case 6:
                strFieldName = "ResultType";
                break;
            case 7:
                strFieldName = "Result";
                break;
            case 8:
                strFieldName = "RequestDate";
                break;
            case 9:
                strFieldName = "ResponseDate";
                break;
            case 10:
                strFieldName = "key1";
                break;
            case 11:
                strFieldName = "key2";
                break;
            case 12:
                strFieldName = "key3";
                break;
            case 13:
                strFieldName = "key4";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "SERIALID":
                return Schema.TYPE_LONG;
            case "TRADECODE":
                return Schema.TYPE_STRING;
            case "TRANSACNO":
                return Schema.TYPE_STRING;
            case "CONTNO":
                return Schema.TYPE_STRING;
            case "PRTNO":
                return Schema.TYPE_STRING;
            case "CHANNELTYPE":
                return Schema.TYPE_STRING;
            case "RESULTTYPE":
                return Schema.TYPE_STRING;
            case "RESULT":
                return Schema.TYPE_STRING;
            case "REQUESTDATE":
                return Schema.TYPE_DATE;
            case "RESPONSEDATE":
                return Schema.TYPE_DATE;
            case "KEY1":
                return Schema.TYPE_STRING;
            case "KEY2":
                return Schema.TYPE_STRING;
            case "KEY3":
                return Schema.TYPE_STRING;
            case "KEY4":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_LONG;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_STRING;
            case 6:
                return Schema.TYPE_STRING;
            case 7:
                return Schema.TYPE_STRING;
            case 8:
                return Schema.TYPE_DATE;
            case 9:
                return Schema.TYPE_DATE;
            case 10:
                return Schema.TYPE_STRING;
            case 11:
                return Schema.TYPE_STRING;
            case 12:
                return Schema.TYPE_STRING;
            case 13:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("Serialid")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Serialid));
        }
        if (FCode.equalsIgnoreCase("TradeCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(TradeCode));
        }
        if (FCode.equalsIgnoreCase("TransacNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(TransacNo));
        }
        if (FCode.equalsIgnoreCase("ContNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ContNo));
        }
        if (FCode.equalsIgnoreCase("PrtNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PrtNo));
        }
        if (FCode.equalsIgnoreCase("ChannelType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ChannelType));
        }
        if (FCode.equalsIgnoreCase("ResultType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ResultType));
        }
        if (FCode.equalsIgnoreCase("Result")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Result));
        }
        if (FCode.equalsIgnoreCase("RequestDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getRequestDate()));
        }
        if (FCode.equalsIgnoreCase("ResponseDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getResponseDate()));
        }
        if (FCode.equalsIgnoreCase("key1")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(key1));
        }
        if (FCode.equalsIgnoreCase("key2")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(key2));
        }
        if (FCode.equalsIgnoreCase("key3")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(key3));
        }
        if (FCode.equalsIgnoreCase("key4")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(key4));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = String.valueOf(Serialid);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(TradeCode);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(TransacNo);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(ContNo);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(PrtNo);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(ChannelType);
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(ResultType);
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(Result);
                break;
            case 8:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getRequestDate()));
                break;
            case 9:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getResponseDate()));
                break;
            case 10:
                strFieldValue = StrTool.GBKToUnicode(key1);
                break;
            case 11:
                strFieldValue = StrTool.GBKToUnicode(key2);
                break;
            case 12:
                strFieldValue = StrTool.GBKToUnicode(key3);
                break;
            case 13:
                strFieldValue = StrTool.GBKToUnicode(key4);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("Serialid")) {
            if( FValue != null && !FValue.equals("")) {
                Serialid = new Long(FValue).longValue();
            }
        }
        if (FCode.equalsIgnoreCase("TradeCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                TradeCode = FValue.trim();
            }
            else
                TradeCode = null;
        }
        if (FCode.equalsIgnoreCase("TransacNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                TransacNo = FValue.trim();
            }
            else
                TransacNo = null;
        }
        if (FCode.equalsIgnoreCase("ContNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                ContNo = FValue.trim();
            }
            else
                ContNo = null;
        }
        if (FCode.equalsIgnoreCase("PrtNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                PrtNo = FValue.trim();
            }
            else
                PrtNo = null;
        }
        if (FCode.equalsIgnoreCase("ChannelType")) {
            if( FValue != null && !FValue.equals(""))
            {
                ChannelType = FValue.trim();
            }
            else
                ChannelType = null;
        }
        if (FCode.equalsIgnoreCase("ResultType")) {
            if( FValue != null && !FValue.equals(""))
            {
                ResultType = FValue.trim();
            }
            else
                ResultType = null;
        }
        if (FCode.equalsIgnoreCase("Result")) {
            if( FValue != null && !FValue.equals(""))
            {
                Result = FValue.trim();
            }
            else
                Result = null;
        }
        if (FCode.equalsIgnoreCase("RequestDate")) {
            if(FValue != null && !FValue.equals("")) {
                RequestDate = fDate.getDate( FValue );
            }
            else
                RequestDate = null;
        }
        if (FCode.equalsIgnoreCase("ResponseDate")) {
            if(FValue != null && !FValue.equals("")) {
                ResponseDate = fDate.getDate( FValue );
            }
            else
                ResponseDate = null;
        }
        if (FCode.equalsIgnoreCase("key1")) {
            if( FValue != null && !FValue.equals(""))
            {
                key1 = FValue.trim();
            }
            else
                key1 = null;
        }
        if (FCode.equalsIgnoreCase("key2")) {
            if( FValue != null && !FValue.equals(""))
            {
                key2 = FValue.trim();
            }
            else
                key2 = null;
        }
        if (FCode.equalsIgnoreCase("key3")) {
            if( FValue != null && !FValue.equals(""))
            {
                key3 = FValue.trim();
            }
            else
                key3 = null;
        }
        if (FCode.equalsIgnoreCase("key4")) {
            if( FValue != null && !FValue.equals(""))
            {
                key4 = FValue.trim();
            }
            else
                key4 = null;
        }
        return true;
    }


    public String toString() {
    return "lctradedataPojo [" +
            "Serialid="+Serialid +
            ", TradeCode="+TradeCode +
            ", TransacNo="+TransacNo +
            ", ContNo="+ContNo +
            ", PrtNo="+PrtNo +
            ", ChannelType="+ChannelType +
            ", ResultType="+ResultType +
            ", Result="+Result +
            ", RequestDate="+RequestDate +
            ", ResponseDate="+ResponseDate +
            ", key1="+key1 +
            ", key2="+key2 +
            ", key3="+key3 +
            ", key4="+key4 +"]";
    }
}
