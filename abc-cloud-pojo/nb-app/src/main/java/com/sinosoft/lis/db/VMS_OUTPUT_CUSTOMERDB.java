/**
 * Copyright (c) 2017 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.db;

import com.sinosoft.lis.schema.VMS_OUTPUT_CUSTOMERSchema;
import com.sinosoft.lis.vschema.VMS_OUTPUT_CUSTOMERSet;
import com.sinosoft.utility.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

/**
 * <p>ClassName: VMS_OUTPUT_CUSTOMERDB </p>
 * <p>Description: DB层数据库操作类文件 </p>
 * <p>Copyright: Copyright (c) 2017</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2017-10-21
 */
public class VMS_OUTPUT_CUSTOMERDB extends VMS_OUTPUT_CUSTOMERSchema {
    // @Field
    private Connection con;
    private DBOper db;
    /**
    * flag = true: 传入Connection
    * flag = false: 不传入Connection
    **/
    private boolean mflag = false;

    public CErrors mErrors = new CErrors();        // 错误信息

    /**
     * 为批量操作而准备的语句和游标对象
     */
    private ResultSet mResultSet = null;
    private Statement mStatement = null;
    // @Constructor
    public VMS_OUTPUT_CUSTOMERDB( Connection tConnection ) {
        con = tConnection;
        db = new DBOper( con, "VMS_OUTPUT_CUSTOMER" );
        mflag = true;
    }

    public VMS_OUTPUT_CUSTOMERDB() {
        con = null;
        db = new DBOper( "VMS_OUTPUT_CUSTOMER" );
        mflag = false;
    }

    // @Method
    public boolean deleteSQL() {
        VMS_OUTPUT_CUSTOMERSchema tSchema = this.getSchema();
        if (db.deleteSQL(tSchema)) {
             return true;
        } else {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "VMS_OUTPUT_CUSTOMERDB";
            tError.functionName = "deleteSQL";
            tError.errorMessage = "操作失败!";
            this.mErrors .addOneError(tError);
            return false;
        }
    }

    public int getCount() {
        VMS_OUTPUT_CUSTOMERSchema tSchema = this.getSchema();

        int tCount = db.getCount(tSchema);
        if (tCount < 0) {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "VMS_OUTPUT_CUSTOMERDB";
            tError.functionName = "getCount";
            tError.errorMessage = "操作失败!";
            this.mErrors .addOneError(tError);

            return -1;
        }

        return tCount;
    }

    public boolean delete() {
        PreparedStatement pstmt = null;

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try {
            pstmt = con.prepareStatement("DELETE FROM VMS_OUTPUT_CUSTOMER WHERE  1=1  AND VIC_INDATE = ? AND VIC_CUSTOMER_ID = ?");
            if(this.getVIC_INDATE() == null || this.getVIC_INDATE().equals("null")) {
            	pstmt.setNull(1, 12);
            } else {
            	pstmt.setString(1, this.getVIC_INDATE());
            }
            if(this.getVIC_CUSTOMER_ID() == null || this.getVIC_CUSTOMER_ID().equals("null")) {
            	pstmt.setNull(2, 12);
            } else {
            	pstmt.setString(2, this.getVIC_CUSTOMER_ID());
            }
            pstmt.executeUpdate();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "VMS_OUTPUT_CUSTOMERDB";
            tError.functionName = "delete()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);
            ex.printStackTrace();

            try {
                pstmt.close();
            } catch (Exception e){}

            if(!mflag) {
                try {
                    con.close();
                } catch (Exception e){}
            }

            return false;
        }

        if(!mflag) {
            try {
                con.close();
            } catch (Exception e){}
        }

        return true;
    }

    public boolean update() {
        PreparedStatement pstmt = null;

        SQLString sqlObj = new SQLString("VMS_OUTPUT_CUSTOMER");
        sqlObj.setSQL(2, this);
        String sql = sqlObj.getSQL();

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try {
            pstmt = con.prepareStatement("UPDATE VMS_OUTPUT_CUSTOMER SET  VIC_INDATE = ? , VIC_CUSTOMER_ID = ? , VIC_CUSTOMER_CNAME = ? , VIC_CUSTOMER_TAXNO = ? , VIC_CUSTOMER_ACCOUNT = ? , VIC_CUSTOMER_CBANK = ? , VIC_CUSTOMER_PHONE = ? , VIC_CUSTOMER_EMAIL = ? , VIC_CUSTOMER_ADDRESS = ? , VIC_TAXPAYER_TYPE = ? , VIC_FAPIAO_TYPE = ? , VIC_CUSTOMER_TYPE = ? , VIC_CUSTOMER_FAPIAO_FLAG = ? , VIC_CUSTOMER_NATIONALITY = ? , VIC_DATA_SOURCE = ? , VIC_LINK_NAME = ? , VIC_LINK_PHONE = ? , VIC_LINK_ADDRESS = ? , VIC_CUSTOMER_ZIP_CODE = ? , VIC_CUSTOMER_UPLOAD_FLAG = ? WHERE  1=1  AND VIC_INDATE = ? AND VIC_CUSTOMER_ID = ?");
            if(this.getVIC_INDATE() == null || this.getVIC_INDATE().equals("null")) {
            	pstmt.setNull(1, 12);
            } else {
            	pstmt.setString(1, this.getVIC_INDATE());
            }
            if(this.getVIC_CUSTOMER_ID() == null || this.getVIC_CUSTOMER_ID().equals("null")) {
            	pstmt.setNull(2, 12);
            } else {
            	pstmt.setString(2, this.getVIC_CUSTOMER_ID());
            }
            if(this.getVIC_CUSTOMER_CNAME() == null || this.getVIC_CUSTOMER_CNAME().equals("null")) {
            	pstmt.setNull(3, 12);
            } else {
            	pstmt.setString(3, this.getVIC_CUSTOMER_CNAME());
            }
            if(this.getVIC_CUSTOMER_TAXNO() == null || this.getVIC_CUSTOMER_TAXNO().equals("null")) {
            	pstmt.setNull(4, 12);
            } else {
            	pstmt.setString(4, this.getVIC_CUSTOMER_TAXNO());
            }
            if(this.getVIC_CUSTOMER_ACCOUNT() == null || this.getVIC_CUSTOMER_ACCOUNT().equals("null")) {
            	pstmt.setNull(5, 12);
            } else {
            	pstmt.setString(5, this.getVIC_CUSTOMER_ACCOUNT());
            }
            if(this.getVIC_CUSTOMER_CBANK() == null || this.getVIC_CUSTOMER_CBANK().equals("null")) {
            	pstmt.setNull(6, 12);
            } else {
            	pstmt.setString(6, this.getVIC_CUSTOMER_CBANK());
            }
            if(this.getVIC_CUSTOMER_PHONE() == null || this.getVIC_CUSTOMER_PHONE().equals("null")) {
            	pstmt.setNull(7, 12);
            } else {
            	pstmt.setString(7, this.getVIC_CUSTOMER_PHONE());
            }
            if(this.getVIC_CUSTOMER_EMAIL() == null || this.getVIC_CUSTOMER_EMAIL().equals("null")) {
            	pstmt.setNull(8, 12);
            } else {
            	pstmt.setString(8, this.getVIC_CUSTOMER_EMAIL());
            }
            if(this.getVIC_CUSTOMER_ADDRESS() == null || this.getVIC_CUSTOMER_ADDRESS().equals("null")) {
            	pstmt.setNull(9, 12);
            } else {
            	pstmt.setString(9, this.getVIC_CUSTOMER_ADDRESS());
            }
            if(this.getVIC_TAXPAYER_TYPE() == null || this.getVIC_TAXPAYER_TYPE().equals("null")) {
            	pstmt.setNull(10, 12);
            } else {
            	pstmt.setString(10, this.getVIC_TAXPAYER_TYPE());
            }
            if(this.getVIC_FAPIAO_TYPE() == null || this.getVIC_FAPIAO_TYPE().equals("null")) {
            	pstmt.setNull(11, 12);
            } else {
            	pstmt.setString(11, this.getVIC_FAPIAO_TYPE());
            }
            if(this.getVIC_CUSTOMER_TYPE() == null || this.getVIC_CUSTOMER_TYPE().equals("null")) {
            	pstmt.setNull(12, 12);
            } else {
            	pstmt.setString(12, this.getVIC_CUSTOMER_TYPE());
            }
            if(this.getVIC_CUSTOMER_FAPIAO_FLAG() == null || this.getVIC_CUSTOMER_FAPIAO_FLAG().equals("null")) {
            	pstmt.setNull(13, 12);
            } else {
            	pstmt.setString(13, this.getVIC_CUSTOMER_FAPIAO_FLAG());
            }
            if(this.getVIC_CUSTOMER_NATIONALITY() == null || this.getVIC_CUSTOMER_NATIONALITY().equals("null")) {
            	pstmt.setNull(14, 12);
            } else {
            	pstmt.setString(14, this.getVIC_CUSTOMER_NATIONALITY());
            }
            if(this.getVIC_DATA_SOURCE() == null || this.getVIC_DATA_SOURCE().equals("null")) {
            	pstmt.setNull(15, 12);
            } else {
            	pstmt.setString(15, this.getVIC_DATA_SOURCE());
            }
            if(this.getVIC_LINK_NAME() == null || this.getVIC_LINK_NAME().equals("null")) {
            	pstmt.setNull(16, 12);
            } else {
            	pstmt.setString(16, this.getVIC_LINK_NAME());
            }
            if(this.getVIC_LINK_PHONE() == null || this.getVIC_LINK_PHONE().equals("null")) {
            	pstmt.setNull(17, 12);
            } else {
            	pstmt.setString(17, this.getVIC_LINK_PHONE());
            }
            if(this.getVIC_LINK_ADDRESS() == null || this.getVIC_LINK_ADDRESS().equals("null")) {
            	pstmt.setNull(18, 12);
            } else {
            	pstmt.setString(18, this.getVIC_LINK_ADDRESS());
            }
            if(this.getVIC_CUSTOMER_ZIP_CODE() == null || this.getVIC_CUSTOMER_ZIP_CODE().equals("null")) {
            	pstmt.setNull(19, 12);
            } else {
            	pstmt.setString(19, this.getVIC_CUSTOMER_ZIP_CODE());
            }
            if(this.getVIC_CUSTOMER_UPLOAD_FLAG() == null || this.getVIC_CUSTOMER_UPLOAD_FLAG().equals("null")) {
            	pstmt.setNull(20, 12);
            } else {
            	pstmt.setString(20, this.getVIC_CUSTOMER_UPLOAD_FLAG());
            }
            // set where condition
            if(this.getVIC_INDATE() == null || this.getVIC_INDATE().equals("null")) {
            	pstmt.setNull(21, 12);
            } else {
            	pstmt.setString(21, this.getVIC_INDATE());
            }
            if(this.getVIC_CUSTOMER_ID() == null || this.getVIC_CUSTOMER_ID().equals("null")) {
            	pstmt.setNull(22, 12);
            } else {
            	pstmt.setString(22, this.getVIC_CUSTOMER_ID());
            }
            pstmt.executeUpdate();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "VMS_OUTPUT_CUSTOMERDB";
            tError.functionName = "update()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);
            ex.printStackTrace();

            try {
                pstmt.close();
            } catch (Exception e){}

            if( !mflag ) {
                try {
                    con.close();
                } catch (Exception e){}
            }

            return false;
        }

        if(!mflag) {
            try {
                con.close();
            } catch (Exception e){}
        }

        return true;
    }

    public boolean insert() {
        PreparedStatement pstmt = null;

        SQLString sqlObj = new SQLString("VMS_OUTPUT_CUSTOMER");
        sqlObj.setSQL(1, this);
        String sql = sqlObj.getSQL();

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try {
            pstmt = con.prepareStatement("INSERT INTO VMS_OUTPUT_CUSTOMER VALUES( ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ?)");
            if(this.getVIC_INDATE() == null || this.getVIC_INDATE().equals("null")) {
            	pstmt.setNull(1, 12);
            } else {
            	pstmt.setString(1, this.getVIC_INDATE());
            }
            if(this.getVIC_CUSTOMER_ID() == null || this.getVIC_CUSTOMER_ID().equals("null")) {
            	pstmt.setNull(2, 12);
            } else {
            	pstmt.setString(2, this.getVIC_CUSTOMER_ID());
            }
            if(this.getVIC_CUSTOMER_CNAME() == null || this.getVIC_CUSTOMER_CNAME().equals("null")) {
            	pstmt.setNull(3, 12);
            } else {
            	pstmt.setString(3, this.getVIC_CUSTOMER_CNAME());
            }
            if(this.getVIC_CUSTOMER_TAXNO() == null || this.getVIC_CUSTOMER_TAXNO().equals("null")) {
            	pstmt.setNull(4, 12);
            } else {
            	pstmt.setString(4, this.getVIC_CUSTOMER_TAXNO());
            }
            if(this.getVIC_CUSTOMER_ACCOUNT() == null || this.getVIC_CUSTOMER_ACCOUNT().equals("null")) {
            	pstmt.setNull(5, 12);
            } else {
            	pstmt.setString(5, this.getVIC_CUSTOMER_ACCOUNT());
            }
            if(this.getVIC_CUSTOMER_CBANK() == null || this.getVIC_CUSTOMER_CBANK().equals("null")) {
            	pstmt.setNull(6, 12);
            } else {
            	pstmt.setString(6, this.getVIC_CUSTOMER_CBANK());
            }
            if(this.getVIC_CUSTOMER_PHONE() == null || this.getVIC_CUSTOMER_PHONE().equals("null")) {
            	pstmt.setNull(7, 12);
            } else {
            	pstmt.setString(7, this.getVIC_CUSTOMER_PHONE());
            }
            if(this.getVIC_CUSTOMER_EMAIL() == null || this.getVIC_CUSTOMER_EMAIL().equals("null")) {
            	pstmt.setNull(8, 12);
            } else {
            	pstmt.setString(8, this.getVIC_CUSTOMER_EMAIL());
            }
            if(this.getVIC_CUSTOMER_ADDRESS() == null || this.getVIC_CUSTOMER_ADDRESS().equals("null")) {
            	pstmt.setNull(9, 12);
            } else {
            	pstmt.setString(9, this.getVIC_CUSTOMER_ADDRESS());
            }
            if(this.getVIC_TAXPAYER_TYPE() == null || this.getVIC_TAXPAYER_TYPE().equals("null")) {
            	pstmt.setNull(10, 12);
            } else {
            	pstmt.setString(10, this.getVIC_TAXPAYER_TYPE());
            }
            if(this.getVIC_FAPIAO_TYPE() == null || this.getVIC_FAPIAO_TYPE().equals("null")) {
            	pstmt.setNull(11, 12);
            } else {
            	pstmt.setString(11, this.getVIC_FAPIAO_TYPE());
            }
            if(this.getVIC_CUSTOMER_TYPE() == null || this.getVIC_CUSTOMER_TYPE().equals("null")) {
            	pstmt.setNull(12, 12);
            } else {
            	pstmt.setString(12, this.getVIC_CUSTOMER_TYPE());
            }
            if(this.getVIC_CUSTOMER_FAPIAO_FLAG() == null || this.getVIC_CUSTOMER_FAPIAO_FLAG().equals("null")) {
            	pstmt.setNull(13, 12);
            } else {
            	pstmt.setString(13, this.getVIC_CUSTOMER_FAPIAO_FLAG());
            }
            if(this.getVIC_CUSTOMER_NATIONALITY() == null || this.getVIC_CUSTOMER_NATIONALITY().equals("null")) {
            	pstmt.setNull(14, 12);
            } else {
            	pstmt.setString(14, this.getVIC_CUSTOMER_NATIONALITY());
            }
            if(this.getVIC_DATA_SOURCE() == null || this.getVIC_DATA_SOURCE().equals("null")) {
            	pstmt.setNull(15, 12);
            } else {
            	pstmt.setString(15, this.getVIC_DATA_SOURCE());
            }
            if(this.getVIC_LINK_NAME() == null || this.getVIC_LINK_NAME().equals("null")) {
            	pstmt.setNull(16, 12);
            } else {
            	pstmt.setString(16, this.getVIC_LINK_NAME());
            }
            if(this.getVIC_LINK_PHONE() == null || this.getVIC_LINK_PHONE().equals("null")) {
            	pstmt.setNull(17, 12);
            } else {
            	pstmt.setString(17, this.getVIC_LINK_PHONE());
            }
            if(this.getVIC_LINK_ADDRESS() == null || this.getVIC_LINK_ADDRESS().equals("null")) {
            	pstmt.setNull(18, 12);
            } else {
            	pstmt.setString(18, this.getVIC_LINK_ADDRESS());
            }
            if(this.getVIC_CUSTOMER_ZIP_CODE() == null || this.getVIC_CUSTOMER_ZIP_CODE().equals("null")) {
            	pstmt.setNull(19, 12);
            } else {
            	pstmt.setString(19, this.getVIC_CUSTOMER_ZIP_CODE());
            }
            if(this.getVIC_CUSTOMER_UPLOAD_FLAG() == null || this.getVIC_CUSTOMER_UPLOAD_FLAG().equals("null")) {
            	pstmt.setNull(20, 12);
            } else {
            	pstmt.setString(20, this.getVIC_CUSTOMER_UPLOAD_FLAG());
            }
            // execute sql
            pstmt.executeUpdate();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "VMS_OUTPUT_CUSTOMERDB";
            tError.functionName = "insert()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors.addOneError(tError);
            ex.printStackTrace();

            try {
                pstmt.close();
            } catch (Exception e){}

            if( !mflag ) {
                try {
                    con.close();
                } catch (Exception e){}
            }

            return false;
        }

        if(!mflag) {
            try {
                con.close();
            } catch (Exception e){}
        }

        return true;
    }

    public boolean getInfo() {
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        if(!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            pstmt = con.prepareStatement("SELECT * FROM VMS_OUTPUT_CUSTOMER WHERE  1=1  AND VIC_INDATE = ? AND VIC_CUSTOMER_ID = ?", 
                ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
            if(this.getVIC_INDATE() == null || this.getVIC_INDATE().equals("null")) {
            	pstmt.setNull(1, 12);
            } else {
            	pstmt.setString(1, this.getVIC_INDATE());
            }
            if(this.getVIC_CUSTOMER_ID() == null || this.getVIC_CUSTOMER_ID().equals("null")) {
            	pstmt.setNull(2, 12);
            } else {
            	pstmt.setString(2, this.getVIC_CUSTOMER_ID());
            }
            rs = pstmt.executeQuery();
            int i = 0;
            while (rs.next()) {
                i++;
                if (!this.setSchema(rs,i)) {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "VMS_OUTPUT_CUSTOMERDB";
                    tError.functionName = "getInfo";
                    tError.errorMessage = "取数失败!";
                    this.mErrors .addOneError(tError);

                    try{ rs.close(); } catch( Exception ex ) {}
                    try{ pstmt.close(); } catch( Exception ex1 ) {}

                    if (!mflag) {
                        try {
                            con.close();
                        }
                        catch(Exception et){}
                    }
                    return false;
                }
                break;
            }
            try{ rs.close(); } catch( Exception ex2 ) {}
            try{ pstmt.close(); } catch( Exception ex3 ) {}

            if( i == 0 ) {
                if (!mflag) {
                    try {
                        con.close();
                    }
                    catch(Exception et){}
                }
                return false;
            }
        }
        catch(Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "VMS_OUTPUT_CUSTOMERDB";
            tError.functionName = "getInfo";
            tError.errorMessage = ExceptionUtils.exceptionToString(e);
            this.mErrors .addOneError(tError);

            try{ rs.close(); } catch( Exception ex ) {}
            try{ pstmt.close(); } catch( Exception ex1 ) {}

            if (!mflag) {
                try {
                    con.close();
                }
                catch(Exception et){}
            }
            return false;
        }
        // 断开数据库连接
        if (!mflag) {
            try {
                con.close();
            }
            catch(Exception e){}
        }

        return true;
    }

    public VMS_OUTPUT_CUSTOMERSet query() {
        Statement stmt = null;
        ResultSet rs = null;
        VMS_OUTPUT_CUSTOMERSet aVMS_OUTPUT_CUSTOMERSet = new VMS_OUTPUT_CUSTOMERSet();

        if (!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            stmt = con.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
            SQLString sqlObj = new SQLString("VMS_OUTPUT_CUSTOMER");
            VMS_OUTPUT_CUSTOMERSchema aSchema = this.getSchema();
            sqlObj.setSQL(5, aSchema);
            String sql = sqlObj.getSQL();

            rs = stmt.executeQuery(sql);
            int i = 0;
            while (rs.next()) {
                i++;
                if (i>10000) {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "VMS_OUTPUT_CUSTOMERDB";
                    tError.functionName = "query";
                    tError.errorMessage = "SQL语句返回结果集太多，请换用其他方式!";
                    this.mErrors .addOneError(tError);
                        break;
                }
                VMS_OUTPUT_CUSTOMERSchema s1 = new VMS_OUTPUT_CUSTOMERSchema();
                s1.setSchema(rs,i);
                aVMS_OUTPUT_CUSTOMERSet.add(s1);
            }
            try{ rs.close(); } catch( Exception ex ) {}
            try{ stmt.close(); } catch( Exception ex1 ) {}
        }
        catch(Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "VMS_OUTPUT_CUSTOMERDB";
            tError.functionName = "query";
            tError.errorMessage = ExceptionUtils.exceptionToString(e);
            this.mErrors.addOneError(tError);
            e.printStackTrace();

            try { rs.close(); } catch (Exception ex2) { ex2.printStackTrace(); }
            try { stmt.close(); } catch(Exception ex3) { ex3.printStackTrace(); }

            if (!mflag) {
                try {
                    con.close();
                }
                catch(Exception et){et.printStackTrace();}
            }
        }

        if (!mflag) {
            try {
                con.close();
            }
            catch(Exception e){e.printStackTrace();}
        }

        return aVMS_OUTPUT_CUSTOMERSet;
    }

    public VMS_OUTPUT_CUSTOMERSet executeQuery(String sql)  {
        Statement stmt = null;
        ResultSet rs = null;
        VMS_OUTPUT_CUSTOMERSet aVMS_OUTPUT_CUSTOMERSet = new VMS_OUTPUT_CUSTOMERSet();

        if (!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            stmt = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);

            rs = stmt.executeQuery(StrTool.GBKToUnicode(sql));
            int i = 0;
            while (rs.next()) {
                i++;
                if (i>10000) {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "VMS_OUTPUT_CUSTOMERDB";
                    tError.functionName = "executeQuery";
                    tError.errorMessage = "SQL语句返回结果集太多，请换用其他方式!";
                    this.mErrors .addOneError(tError);
                        break;
                }
                VMS_OUTPUT_CUSTOMERSchema s1 = new VMS_OUTPUT_CUSTOMERSchema();
                if (!s1.setSchema(rs,i)) {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "VMS_OUTPUT_CUSTOMERDB";
                    tError.functionName = "executeQuery";
                    tError.errorMessage = "sql语句有误，请查看表名及字段名信息!";
                    this.mErrors .addOneError(tError);
                }
                aVMS_OUTPUT_CUSTOMERSet.add(s1);
            }
            try{ rs.close(); } catch( Exception ex ) {}
            try{ stmt.close(); } catch( Exception ex1 ) {}
        }
        catch(Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "VMS_OUTPUT_CUSTOMERDB";
            tError.functionName = "executeQuery";
            tError.errorMessage = ExceptionUtils.exceptionToString(e);
            this.mErrors .addOneError(tError);

            try{ rs.close(); } catch( Exception ex2 ) {}
            try{ stmt.close(); } catch( Exception ex3 ) {}

            if (!mflag) {
                try {
                    con.close();
                }
                catch(Exception et){}
            }
        }

        if (!mflag) {
            try {
                con.close();
            }
            catch(Exception e){}
        }

        return aVMS_OUTPUT_CUSTOMERSet;
    }

    public VMS_OUTPUT_CUSTOMERSet query(int nStart, int nCount) {
        Statement stmt = null;
        ResultSet rs = null;
        VMS_OUTPUT_CUSTOMERSet aVMS_OUTPUT_CUSTOMERSet = new VMS_OUTPUT_CUSTOMERSet();

        if (!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            stmt = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);
            SQLString sqlObj = new SQLString("VMS_OUTPUT_CUSTOMER");
            VMS_OUTPUT_CUSTOMERSchema aSchema = this.getSchema();
            sqlObj.setSQL(5,aSchema);
            String sql = sqlObj.getSQL();

            rs = stmt.executeQuery(sql);
            int i = 0;
            while (rs.next()) {
                i++;

                if( i < nStart ) {
                    continue;
                }

                if( i >= nStart + nCount ) {
                    break;
                }

                VMS_OUTPUT_CUSTOMERSchema s1 = new VMS_OUTPUT_CUSTOMERSchema();
                s1.setSchema(rs,i);
                aVMS_OUTPUT_CUSTOMERSet.add(s1);
            }
            try{ rs.close(); } catch( Exception ex ) {}
            try{ stmt.close(); } catch( Exception ex1 ) {}
        }
        catch(Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "VMS_OUTPUT_CUSTOMERDB";
            tError.functionName = "query";
            tError.errorMessage = ExceptionUtils.exceptionToString(e);
            this.mErrors .addOneError(tError);

            try{ rs.close(); } catch( Exception ex2 ) {}
            try{ stmt.close(); } catch( Exception ex3 ) {}

            if (!mflag) {
                try {
                    con.close();
                }
                catch(Exception et){}
            }
        }

        if (!mflag) {
            try {
                con.close();
            }
            catch(Exception e){}
        }

        return aVMS_OUTPUT_CUSTOMERSet;
    }

    public VMS_OUTPUT_CUSTOMERSet executeQuery(String sql, int nStart, int nCount) {
        Statement stmt = null;
        ResultSet rs = null;
        VMS_OUTPUT_CUSTOMERSet aVMS_OUTPUT_CUSTOMERSet = new VMS_OUTPUT_CUSTOMERSet();

        if (!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            stmt = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);

            rs = stmt.executeQuery(StrTool.GBKToUnicode(sql));
            int i = 0;
            while (rs.next()) {
                i++;

                if( i < nStart ) {
                    continue;
                }

                if( i >= nStart + nCount ) {
                    break;
                }

                VMS_OUTPUT_CUSTOMERSchema s1 = new VMS_OUTPUT_CUSTOMERSchema();
                if (!s1.setSchema(rs,i)) {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "VMS_OUTPUT_CUSTOMERDB";
                    tError.functionName = "executeQuery";
                    tError.errorMessage = "sql语句有误，请查看表名及字段名信息!";
                    this.mErrors .addOneError(tError);
                }
                aVMS_OUTPUT_CUSTOMERSet.add(s1);
            }
            try{ rs.close(); } catch( Exception ex ) {}
            try{ stmt.close(); } catch( Exception ex1 ) {}
        }
        catch(Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "VMS_OUTPUT_CUSTOMERDB";
            tError.functionName = "executeQuery";
            tError.errorMessage = ExceptionUtils.exceptionToString(e);
            this.mErrors .addOneError(tError);

            try{ rs.close(); } catch( Exception ex2 ) {}
            try{ stmt.close(); } catch( Exception ex3 ) {}

            if (!mflag) {
                try {
                    con.close();
                }
                catch(Exception et){}
            }
        }

        if (!mflag) {
            try {
                con.close();
            }
            catch(Exception e){}
        }

        return aVMS_OUTPUT_CUSTOMERSet;
    }

    public boolean update(String strWherePart) {
        Statement stmt = null;

        if (!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            stmt = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);
            SQLString sqlObj = new SQLString("VMS_OUTPUT_CUSTOMER");
            VMS_OUTPUT_CUSTOMERSchema aSchema = this.getSchema();
            sqlObj.setSQL(2,aSchema);
            String sql = "update VMS_OUTPUT_CUSTOMER " + sqlObj.getUpdPart() + " where " + strWherePart;

            int operCount = stmt.executeUpdate(sql);
            if (operCount == 0) {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "VMS_OUTPUT_CUSTOMERDB";
                tError.functionName = "update";
                tError.errorMessage = "更新数据失败!";
                this.mErrors .addOneError(tError);

                if (!mflag) {
                    try {
                        con.close();
                    }
                    catch(Exception et){}
                }
                return false;
            }
        }
        catch(Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "VMS_OUTPUT_CUSTOMERDB";
            tError.functionName = "update";
            tError.errorMessage = ExceptionUtils.exceptionToString(e);
            this.mErrors .addOneError(tError);

            try{ stmt.close(); } catch( Exception ex1 ) {}

            if (!mflag) {
                try {
                    con.close();
                }
                catch(Exception et){}
            }
            return false;
        }
        // 断开数据库连接
        if (!mflag) {
            try {
                con.close();
            }
            catch(Exception e){}
        }

        return true;
    }

    /**
     * 准备数据查询条件
     * @param strSQL String
     * @return boolean
     */
    public boolean prepareData(String strSQL) {
        if (mResultSet != null) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "VMS_OUTPUT_CUSTOMERDB";
            tError.functionName = "prepareData";
            tError.errorMessage = "数据集非空，程序在准备数据集之后，没有关闭！";
            this.mErrors.addOneError(tError);
            return false;
        }

        if (!mflag) {
            con = DBConnPool.getConnection();
        }
        try {
            mStatement = con.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
            mResultSet = mStatement.executeQuery(StrTool.GBKToUnicode(strSQL));
        }
        catch (Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "VMS_OUTPUT_CUSTOMERDB";
            tError.functionName = "prepareData";
            tError.errorMessage = ExceptionUtils.exceptionToString(e);
            this.mErrors.addOneError(tError);
            try {
                mResultSet.close();
            }
            catch (Exception ex2) {}
            try {
                mStatement.close();
            }
            catch (Exception ex3) {}
            if (!mflag) {
                try {
                    con.close();
                }
                catch (Exception et) {}
            }
            return false;
        }

        if (!mflag) {
            try {
                con.close();
            }
            catch (Exception e) {}
        }
        return true;
    }

    /**
     * 获取数据集
     * @return boolean
     */
    public boolean hasMoreData() {
        boolean flag = true;
        if (null == mResultSet) {
            CError tError = new CError();
            tError.moduleName = "VMS_OUTPUT_CUSTOMERDB";
            tError.functionName = "hasMoreData";
            tError.errorMessage = "数据集为空，请先准备数据集！";
            this.mErrors.addOneError(tError);
            return false;
        }
        try {
            flag = mResultSet.next();
        }
        catch (Exception ex) {
            CError tError = new CError();
            tError.moduleName = "VMS_OUTPUT_CUSTOMERDB";
            tError.functionName = "hasMoreData";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors.addOneError(tError);
            try {
                mResultSet.close();
                mResultSet = null;
            }
            catch (Exception ex2) {}
            try {
                mStatement.close();
                mStatement = null;
            }
            catch (Exception ex3) {}
            if (!mflag) {
                try {
                    con.close();
                }
                catch (Exception et) {}
            }
            return false;
        }
        return flag;
    }
    /**
     * 获取定量数据
     * @return VMS_OUTPUT_CUSTOMERSet
     */
    public VMS_OUTPUT_CUSTOMERSet getData() {
        int tCount = 0;
        VMS_OUTPUT_CUSTOMERSet tVMS_OUTPUT_CUSTOMERSet = new VMS_OUTPUT_CUSTOMERSet();
        VMS_OUTPUT_CUSTOMERSchema tVMS_OUTPUT_CUSTOMERSchema = null;
        if (null == mResultSet) {
            CError tError = new CError();
            tError.moduleName = "VMS_OUTPUT_CUSTOMERDB";
            tError.functionName = "getData";
            tError.errorMessage = "数据集为空，请先准备数据集！";
            this.mErrors.addOneError(tError);
            return null;
        }
        try {
            tCount = 1;
            tVMS_OUTPUT_CUSTOMERSchema = new VMS_OUTPUT_CUSTOMERSchema();
            tVMS_OUTPUT_CUSTOMERSchema.setSchema(mResultSet, 1);
            tVMS_OUTPUT_CUSTOMERSet.add(tVMS_OUTPUT_CUSTOMERSchema);
            //注意mResultSet.next()的作用
            while (tCount++ < SysConst.FETCHCOUNT) {
                if (mResultSet.next()) {
                    tVMS_OUTPUT_CUSTOMERSchema = new VMS_OUTPUT_CUSTOMERSchema();
                    tVMS_OUTPUT_CUSTOMERSchema.setSchema(mResultSet, 1);
                    tVMS_OUTPUT_CUSTOMERSet.add(tVMS_OUTPUT_CUSTOMERSchema);
                }
            }
        }
        catch (Exception ex) {
            CError tError = new CError();
            tError.moduleName = "VMS_OUTPUT_CUSTOMERDB";
            tError.functionName = "getData";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors.addOneError(tError);
            try {
                mResultSet.close();
                mResultSet = null;
            }
            catch (Exception ex2) {}
            try {
                mStatement.close();
                mStatement = null;
            }
            catch (Exception ex3) {}
            if (!mflag) {
                try {
                    con.close();
                }
                catch (Exception et) {}
            }
            return null;
        }
        return tVMS_OUTPUT_CUSTOMERSet;
    }
    /**
     * 关闭数据集
     * @return boolean
     */
    public boolean closeData() {
        boolean flag = true;
        try {
            if (null == mResultSet) {
                CError tError = new CError();
                tError.moduleName = "VMS_OUTPUT_CUSTOMERDB";
                tError.functionName = "closeData";
                tError.errorMessage = "数据集已经关闭了！";
                this.mErrors.addOneError(tError);
                flag = false;
            } else {
                mResultSet.close();
                mResultSet = null;
            }
        }
        catch (Exception ex2) {
            CError tError = new CError();
            tError.moduleName = "VMS_OUTPUT_CUSTOMERDB";
            tError.functionName = "closeData";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex2);
            this.mErrors.addOneError(tError);
            flag = false;
        }
        try {
            if (null == mStatement) {
                CError tError = new CError();
                tError.moduleName = "VMS_OUTPUT_CUSTOMERDB";
                tError.functionName = "closeData";
                tError.errorMessage = "语句已经关闭了！";
                this.mErrors.addOneError(tError);
                flag = false;
            } else {
                mStatement.close();
                mStatement = null;
            }
        }
        catch (Exception ex3) {
            CError tError = new CError();
            tError.moduleName = "VMS_OUTPUT_CUSTOMERDB";
            tError.functionName = "closeData";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex3);
            this.mErrors.addOneError(tError);
            flag = false;
        }
        return flag;
    }
}
