/**
 * Copyright (c) 2017 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.db;

import com.sinosoft.lis.schema.LCAudVidRecordSchema;
import com.sinosoft.lis.vschema.LCAudVidRecordSet;
import com.sinosoft.utility.*;

import java.sql.*;

/**
 * <p>ClassName: LCAudVidRecordDB </p>
 * <p>Description: DB层数据库操作类文件 </p>
 * <p>Copyright: Copyright (c) 2017</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2017-11-13
 */
public class LCAudVidRecordDB extends LCAudVidRecordSchema {
    // @Field
    private Connection con;
    private DBOper db;
    /**
    * flag = true: 传入Connection
    * flag = false: 不传入Connection
    **/
    private boolean mflag = false;

    public CErrors mErrors = new CErrors();        // 错误信息

    /**
     * 为批量操作而准备的语句和游标对象
     */
    private ResultSet mResultSet = null;
    private Statement mStatement = null;
    // @Constructor
    public LCAudVidRecordDB( Connection tConnection ) {
        con = tConnection;
        db = new DBOper( con, "LCAudVidRecord" );
        mflag = true;
    }

    public LCAudVidRecordDB() {
        con = null;
        db = new DBOper( "LCAudVidRecord" );
        mflag = false;
    }

    // @Method
    public boolean deleteSQL() {
        LCAudVidRecordSchema tSchema = this.getSchema();
        if (db.deleteSQL(tSchema)) {
             return true;
        } else {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LCAudVidRecordDB";
            tError.functionName = "deleteSQL";
            tError.errorMessage = "操作失败!";
            this.mErrors .addOneError(tError);
            return false;
        }
    }

    public int getCount() {
        LCAudVidRecordSchema tSchema = this.getSchema();

        int tCount = db.getCount(tSchema);
        if (tCount < 0) {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LCAudVidRecordDB";
            tError.functionName = "getCount";
            tError.errorMessage = "操作失败!";
            this.mErrors .addOneError(tError);

            return -1;
        }

        return tCount;
    }

    public boolean delete() {
        PreparedStatement pstmt = null;

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try {
            pstmt = con.prepareStatement("DELETE FROM LCAudVidRecord WHERE  1=1  AND PrtNo = ?");
            if(this.getPrtNo() == null || this.getPrtNo().equals("null")) {
            	pstmt.setNull(1, 12);
            } else {
            	pstmt.setString(1, this.getPrtNo());
            }
            pstmt.executeUpdate();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LCAudVidRecordDB";
            tError.functionName = "delete()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);
            ex.printStackTrace();

            try {
                pstmt.close();
            } catch (Exception e){}

            if(!mflag) {
                try {
                    con.close();
                } catch (Exception e){}
            }

            return false;
        }

        if(!mflag) {
            try {
                con.close();
            } catch (Exception e){}
        }

        return true;
    }

    public boolean update() {
        PreparedStatement pstmt = null;

        SQLString sqlObj = new SQLString("LCAudVidRecord");
        sqlObj.setSQL(2, this);
        String sql = sqlObj.getSQL();

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try {
            pstmt = con.prepareStatement("UPDATE LCAudVidRecord SET  PrtNo = ? , IsRecord = ? , IsQuailty = ? , DoneRecordDate = ? , DoneRecordTime = ? , QuailtyResult = ? , QuailtyDate = ? , QualityTime = ? , Operator = ? , MakeDate = ? , MakeTime = ? , ModifyDate = ? , ModifyTime = ? , Remark = ? WHERE  1=1  AND PrtNo = ?");
            if(this.getPrtNo() == null || this.getPrtNo().equals("null")) {
            	pstmt.setNull(1, 12);
            } else {
            	pstmt.setString(1, this.getPrtNo());
            }
            if(this.getIsRecord() == null || this.getIsRecord().equals("null")) {
            	pstmt.setNull(2, 12);
            } else {
            	pstmt.setString(2, this.getIsRecord());
            }
            if(this.getIsQuailty() == null || this.getIsQuailty().equals("null")) {
            	pstmt.setNull(3, 12);
            } else {
            	pstmt.setString(3, this.getIsQuailty());
            }
            if(this.getDoneRecordDate() == null || this.getDoneRecordDate().equals("null")) {
            	pstmt.setNull(4, 93);
            } else {
            	pstmt.setDate(4, Date.valueOf(this.getDoneRecordDate()));
            }
            if(this.getDoneRecordTime() == null || this.getDoneRecordTime().equals("null")) {
            	pstmt.setNull(5, 12);
            } else {
            	pstmt.setString(5, this.getDoneRecordTime());
            }
            if(this.getQuailtyResult() == null || this.getQuailtyResult().equals("null")) {
            	pstmt.setNull(6, 12);
            } else {
            	pstmt.setString(6, this.getQuailtyResult());
            }
            if(this.getQuailtyDate() == null || this.getQuailtyDate().equals("null")) {
            	pstmt.setNull(7, 93);
            } else {
            	pstmt.setDate(7, Date.valueOf(this.getQuailtyDate()));
            }
            if(this.getQualityTime() == null || this.getQualityTime().equals("null")) {
            	pstmt.setNull(8, 12);
            } else {
            	pstmt.setString(8, this.getQualityTime());
            }
            if(this.getOperator() == null || this.getOperator().equals("null")) {
            	pstmt.setNull(9, 12);
            } else {
            	pstmt.setString(9, this.getOperator());
            }
            if(this.getMakeDate() == null || this.getMakeDate().equals("null")) {
            	pstmt.setNull(10, 93);
            } else {
            	pstmt.setDate(10, Date.valueOf(this.getMakeDate()));
            }
            if(this.getMakeTime() == null || this.getMakeTime().equals("null")) {
            	pstmt.setNull(11, 12);
            } else {
            	pstmt.setString(11, this.getMakeTime());
            }
            if(this.getModifyDate() == null || this.getModifyDate().equals("null")) {
            	pstmt.setNull(12, 93);
            } else {
            	pstmt.setDate(12, Date.valueOf(this.getModifyDate()));
            }
            if(this.getModifyTime() == null || this.getModifyTime().equals("null")) {
            	pstmt.setNull(13, 12);
            } else {
            	pstmt.setString(13, this.getModifyTime());
            }
            if(this.getRemark() == null || this.getRemark().equals("null")) {
            	pstmt.setNull(14, 12);
            } else {
            	pstmt.setString(14, this.getRemark());
            }
            // set where condition
            if(this.getPrtNo() == null || this.getPrtNo().equals("null")) {
            	pstmt.setNull(15, 12);
            } else {
            	pstmt.setString(15, this.getPrtNo());
            }
            pstmt.executeUpdate();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LCAudVidRecordDB";
            tError.functionName = "update()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);
            ex.printStackTrace();

            try {
                pstmt.close();
            } catch (Exception e){}

            if( !mflag ) {
                try {
                    con.close();
                } catch (Exception e){}
            }

            return false;
        }

        if(!mflag) {
            try {
                con.close();
            } catch (Exception e){}
        }

        return true;
    }

    public boolean insert() {
        PreparedStatement pstmt = null;

        SQLString sqlObj = new SQLString("LCAudVidRecord");
        sqlObj.setSQL(1, this);
        String sql = sqlObj.getSQL();

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try {
            pstmt = con.prepareStatement("INSERT INTO LCAudVidRecord VALUES( ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ?)");
            if(this.getPrtNo() == null || this.getPrtNo().equals("null")) {
            	pstmt.setNull(1, 12);
            } else {
            	pstmt.setString(1, this.getPrtNo());
            }
            if(this.getIsRecord() == null || this.getIsRecord().equals("null")) {
            	pstmt.setNull(2, 12);
            } else {
            	pstmt.setString(2, this.getIsRecord());
            }
            if(this.getIsQuailty() == null || this.getIsQuailty().equals("null")) {
            	pstmt.setNull(3, 12);
            } else {
            	pstmt.setString(3, this.getIsQuailty());
            }
            if(this.getDoneRecordDate() == null || this.getDoneRecordDate().equals("null")) {
            	pstmt.setNull(4, 93);
            } else {
            	pstmt.setDate(4, Date.valueOf(this.getDoneRecordDate()));
            }
            if(this.getDoneRecordTime() == null || this.getDoneRecordTime().equals("null")) {
            	pstmt.setNull(5, 12);
            } else {
            	pstmt.setString(5, this.getDoneRecordTime());
            }
            if(this.getQuailtyResult() == null || this.getQuailtyResult().equals("null")) {
            	pstmt.setNull(6, 12);
            } else {
            	pstmt.setString(6, this.getQuailtyResult());
            }
            if(this.getQuailtyDate() == null || this.getQuailtyDate().equals("null")) {
            	pstmt.setNull(7, 93);
            } else {
            	pstmt.setDate(7, Date.valueOf(this.getQuailtyDate()));
            }
            if(this.getQualityTime() == null || this.getQualityTime().equals("null")) {
            	pstmt.setNull(8, 12);
            } else {
            	pstmt.setString(8, this.getQualityTime());
            }
            if(this.getOperator() == null || this.getOperator().equals("null")) {
            	pstmt.setNull(9, 12);
            } else {
            	pstmt.setString(9, this.getOperator());
            }
            if(this.getMakeDate() == null || this.getMakeDate().equals("null")) {
            	pstmt.setNull(10, 93);
            } else {
            	pstmt.setDate(10, Date.valueOf(this.getMakeDate()));
            }
            if(this.getMakeTime() == null || this.getMakeTime().equals("null")) {
            	pstmt.setNull(11, 12);
            } else {
            	pstmt.setString(11, this.getMakeTime());
            }
            if(this.getModifyDate() == null || this.getModifyDate().equals("null")) {
            	pstmt.setNull(12, 93);
            } else {
            	pstmt.setDate(12, Date.valueOf(this.getModifyDate()));
            }
            if(this.getModifyTime() == null || this.getModifyTime().equals("null")) {
            	pstmt.setNull(13, 12);
            } else {
            	pstmt.setString(13, this.getModifyTime());
            }
            if(this.getRemark() == null || this.getRemark().equals("null")) {
            	pstmt.setNull(14, 12);
            } else {
            	pstmt.setString(14, this.getRemark());
            }
            // execute sql
            pstmt.executeUpdate();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LCAudVidRecordDB";
            tError.functionName = "insert()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors.addOneError(tError);
            ex.printStackTrace();

            try {
                pstmt.close();
            } catch (Exception e){}

            if( !mflag ) {
                try {
                    con.close();
                } catch (Exception e){}
            }

            return false;
        }

        if(!mflag) {
            try {
                con.close();
            } catch (Exception e){}
        }

        return true;
    }

    public boolean getInfo() {
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        if(!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            pstmt = con.prepareStatement("SELECT * FROM LCAudVidRecord WHERE  1=1  AND PrtNo = ?", 
                ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
            if(this.getPrtNo() == null || this.getPrtNo().equals("null")) {
            	pstmt.setNull(1, 12);
            } else {
            	pstmt.setString(1, this.getPrtNo());
            }
            rs = pstmt.executeQuery();
            int i = 0;
            while (rs.next()) {
                i++;
                if (!this.setSchema(rs,i)) {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "LCAudVidRecordDB";
                    tError.functionName = "getInfo";
                    tError.errorMessage = "取数失败!";
                    this.mErrors .addOneError(tError);

                    try{ rs.close(); } catch( Exception ex ) {}
                    try{ pstmt.close(); } catch( Exception ex1 ) {}

                    if (!mflag) {
                        try {
                            con.close();
                        }
                        catch(Exception et){}
                    }
                    return false;
                }
                break;
            }
            try{ rs.close(); } catch( Exception ex2 ) {}
            try{ pstmt.close(); } catch( Exception ex3 ) {}

            if( i == 0 ) {
                if (!mflag) {
                    try {
                        con.close();
                    }
                    catch(Exception et){}
                }
                return false;
            }
        }
        catch(Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LCAudVidRecordDB";
            tError.functionName = "getInfo";
            tError.errorMessage = ExceptionUtils.exceptionToString(e);
            this.mErrors .addOneError(tError);

            try{ rs.close(); } catch( Exception ex ) {}
            try{ pstmt.close(); } catch( Exception ex1 ) {}

            if (!mflag) {
                try {
                    con.close();
                }
                catch(Exception et){}
            }
            return false;
        }
        // 断开数据库连接
        if (!mflag) {
            try {
                con.close();
            }
            catch(Exception e){}
        }

        return true;
    }

    public LCAudVidRecordSet query() {
        Statement stmt = null;
        ResultSet rs = null;
        LCAudVidRecordSet aLCAudVidRecordSet = new LCAudVidRecordSet();

        if (!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            stmt = con.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
            SQLString sqlObj = new SQLString("LCAudVidRecord");
            LCAudVidRecordSchema aSchema = this.getSchema();
            sqlObj.setSQL(5, aSchema);
            String sql = sqlObj.getSQL();

            rs = stmt.executeQuery(sql);
            int i = 0;
            while (rs.next()) {
                i++;
                if (i>10000) {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "LCAudVidRecordDB";
                    tError.functionName = "query";
                    tError.errorMessage = "SQL语句返回结果集太多，请换用其他方式!";
                    this.mErrors .addOneError(tError);
                        break;
                }
                LCAudVidRecordSchema s1 = new LCAudVidRecordSchema();
                s1.setSchema(rs,i);
                aLCAudVidRecordSet.add(s1);
            }
            try{ rs.close(); } catch( Exception ex ) {}
            try{ stmt.close(); } catch( Exception ex1 ) {}
        }
        catch(Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LCAudVidRecordDB";
            tError.functionName = "query";
            tError.errorMessage = ExceptionUtils.exceptionToString(e);
            this.mErrors.addOneError(tError);
            e.printStackTrace();

            try { rs.close(); } catch (Exception ex2) { ex2.printStackTrace(); }
            try { stmt.close(); } catch(Exception ex3) { ex3.printStackTrace(); }

            if (!mflag) {
                try {
                    con.close();
                }
                catch(Exception et){et.printStackTrace();}
            }
        }

        if (!mflag) {
            try {
                con.close();
            }
            catch(Exception e){e.printStackTrace();}
        }

        return aLCAudVidRecordSet;
    }

    public LCAudVidRecordSet executeQuery(String sql)  {
        Statement stmt = null;
        ResultSet rs = null;
        LCAudVidRecordSet aLCAudVidRecordSet = new LCAudVidRecordSet();

        if (!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            stmt = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);

            rs = stmt.executeQuery(StrTool.GBKToUnicode(sql));
            int i = 0;
            while (rs.next()) {
                i++;
                if (i>10000) {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "LCAudVidRecordDB";
                    tError.functionName = "executeQuery";
                    tError.errorMessage = "SQL语句返回结果集太多，请换用其他方式!";
                    this.mErrors .addOneError(tError);
                        break;
                }
                LCAudVidRecordSchema s1 = new LCAudVidRecordSchema();
                if (!s1.setSchema(rs,i)) {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "LCAudVidRecordDB";
                    tError.functionName = "executeQuery";
                    tError.errorMessage = "sql语句有误，请查看表名及字段名信息!";
                    this.mErrors .addOneError(tError);
                }
                aLCAudVidRecordSet.add(s1);
            }
            try{ rs.close(); } catch( Exception ex ) {}
            try{ stmt.close(); } catch( Exception ex1 ) {}
        }
        catch(Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LCAudVidRecordDB";
            tError.functionName = "executeQuery";
            tError.errorMessage = ExceptionUtils.exceptionToString(e);
            this.mErrors .addOneError(tError);

            try{ rs.close(); } catch( Exception ex2 ) {}
            try{ stmt.close(); } catch( Exception ex3 ) {}

            if (!mflag) {
                try {
                    con.close();
                }
                catch(Exception et){}
            }
        }

        if (!mflag) {
            try {
                con.close();
            }
            catch(Exception e){}
        }

        return aLCAudVidRecordSet;
    }

    public LCAudVidRecordSet query(int nStart, int nCount) {
        Statement stmt = null;
        ResultSet rs = null;
        LCAudVidRecordSet aLCAudVidRecordSet = new LCAudVidRecordSet();

        if (!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            stmt = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);
            SQLString sqlObj = new SQLString("LCAudVidRecord");
            LCAudVidRecordSchema aSchema = this.getSchema();
            sqlObj.setSQL(5,aSchema);
            String sql = sqlObj.getSQL();

            rs = stmt.executeQuery(sql);
            int i = 0;
            while (rs.next()) {
                i++;

                if( i < nStart ) {
                    continue;
                }

                if( i >= nStart + nCount ) {
                    break;
                }

                LCAudVidRecordSchema s1 = new LCAudVidRecordSchema();
                s1.setSchema(rs,i);
                aLCAudVidRecordSet.add(s1);
            }
            try{ rs.close(); } catch( Exception ex ) {}
            try{ stmt.close(); } catch( Exception ex1 ) {}
        }
        catch(Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LCAudVidRecordDB";
            tError.functionName = "query";
            tError.errorMessage = ExceptionUtils.exceptionToString(e);
            this.mErrors .addOneError(tError);

            try{ rs.close(); } catch( Exception ex2 ) {}
            try{ stmt.close(); } catch( Exception ex3 ) {}

            if (!mflag) {
                try {
                    con.close();
                }
                catch(Exception et){}
            }
        }

        if (!mflag) {
            try {
                con.close();
            }
            catch(Exception e){}
        }

        return aLCAudVidRecordSet;
    }

    public LCAudVidRecordSet executeQuery(String sql, int nStart, int nCount) {
        Statement stmt = null;
        ResultSet rs = null;
        LCAudVidRecordSet aLCAudVidRecordSet = new LCAudVidRecordSet();

        if (!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            stmt = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);

            rs = stmt.executeQuery(StrTool.GBKToUnicode(sql));
            int i = 0;
            while (rs.next()) {
                i++;

                if( i < nStart ) {
                    continue;
                }

                if( i >= nStart + nCount ) {
                    break;
                }

                LCAudVidRecordSchema s1 = new LCAudVidRecordSchema();
                if (!s1.setSchema(rs,i)) {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "LCAudVidRecordDB";
                    tError.functionName = "executeQuery";
                    tError.errorMessage = "sql语句有误，请查看表名及字段名信息!";
                    this.mErrors .addOneError(tError);
                }
                aLCAudVidRecordSet.add(s1);
            }
            try{ rs.close(); } catch( Exception ex ) {}
            try{ stmt.close(); } catch( Exception ex1 ) {}
        }
        catch(Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LCAudVidRecordDB";
            tError.functionName = "executeQuery";
            tError.errorMessage = ExceptionUtils.exceptionToString(e);
            this.mErrors .addOneError(tError);

            try{ rs.close(); } catch( Exception ex2 ) {}
            try{ stmt.close(); } catch( Exception ex3 ) {}

            if (!mflag) {
                try {
                    con.close();
                }
                catch(Exception et){}
            }
        }

        if (!mflag) {
            try {
                con.close();
            }
            catch(Exception e){}
        }

        return aLCAudVidRecordSet;
    }

    public boolean update(String strWherePart) {
        Statement stmt = null;

        if (!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            stmt = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);
            SQLString sqlObj = new SQLString("LCAudVidRecord");
            LCAudVidRecordSchema aSchema = this.getSchema();
            sqlObj.setSQL(2,aSchema);
            String sql = "update LCAudVidRecord " + sqlObj.getUpdPart() + " where " + strWherePart;

            int operCount = stmt.executeUpdate(sql);
            if (operCount == 0) {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "LCAudVidRecordDB";
                tError.functionName = "update";
                tError.errorMessage = "更新数据失败!";
                this.mErrors .addOneError(tError);

                if (!mflag) {
                    try {
                        con.close();
                    }
                    catch(Exception et){}
                }
                return false;
            }
        }
        catch(Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LCAudVidRecordDB";
            tError.functionName = "update";
            tError.errorMessage = ExceptionUtils.exceptionToString(e);
            this.mErrors .addOneError(tError);

            try{ stmt.close(); } catch( Exception ex1 ) {}

            if (!mflag) {
                try {
                    con.close();
                }
                catch(Exception et){}
            }
            return false;
        }
        // 断开数据库连接
        if (!mflag) {
            try {
                con.close();
            }
            catch(Exception e){}
        }

        return true;
    }

    /**
     * 准备数据查询条件
     * @param strSQL String
     * @return boolean
     */
    public boolean prepareData(String strSQL) {
        if (mResultSet != null) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LCAudVidRecordDB";
            tError.functionName = "prepareData";
            tError.errorMessage = "数据集非空，程序在准备数据集之后，没有关闭！";
            this.mErrors.addOneError(tError);
            return false;
        }

        if (!mflag) {
            con = DBConnPool.getConnection();
        }
        try {
            mStatement = con.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
            mResultSet = mStatement.executeQuery(StrTool.GBKToUnicode(strSQL));
        }
        catch (Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LCAudVidRecordDB";
            tError.functionName = "prepareData";
            tError.errorMessage = ExceptionUtils.exceptionToString(e);
            this.mErrors.addOneError(tError);
            try {
                mResultSet.close();
            }
            catch (Exception ex2) {}
            try {
                mStatement.close();
            }
            catch (Exception ex3) {}
            if (!mflag) {
                try {
                    con.close();
                }
                catch (Exception et) {}
            }
            return false;
        }

        if (!mflag) {
            try {
                con.close();
            }
            catch (Exception e) {}
        }
        return true;
    }

    /**
     * 获取数据集
     * @return boolean
     */
    public boolean hasMoreData() {
        boolean flag = true;
        if (null == mResultSet) {
            CError tError = new CError();
            tError.moduleName = "LCAudVidRecordDB";
            tError.functionName = "hasMoreData";
            tError.errorMessage = "数据集为空，请先准备数据集！";
            this.mErrors.addOneError(tError);
            return false;
        }
        try {
            flag = mResultSet.next();
        }
        catch (Exception ex) {
            CError tError = new CError();
            tError.moduleName = "LCAudVidRecordDB";
            tError.functionName = "hasMoreData";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors.addOneError(tError);
            try {
                mResultSet.close();
                mResultSet = null;
            }
            catch (Exception ex2) {}
            try {
                mStatement.close();
                mStatement = null;
            }
            catch (Exception ex3) {}
            if (!mflag) {
                try {
                    con.close();
                }
                catch (Exception et) {}
            }
            return false;
        }
        return flag;
    }
    /**
     * 获取定量数据
     * @return LCAudVidRecordSet
     */
    public LCAudVidRecordSet getData() {
        int tCount = 0;
        LCAudVidRecordSet tLCAudVidRecordSet = new LCAudVidRecordSet();
        LCAudVidRecordSchema tLCAudVidRecordSchema = null;
        if (null == mResultSet) {
            CError tError = new CError();
            tError.moduleName = "LCAudVidRecordDB";
            tError.functionName = "getData";
            tError.errorMessage = "数据集为空，请先准备数据集！";
            this.mErrors.addOneError(tError);
            return null;
        }
        try {
            tCount = 1;
            tLCAudVidRecordSchema = new LCAudVidRecordSchema();
            tLCAudVidRecordSchema.setSchema(mResultSet, 1);
            tLCAudVidRecordSet.add(tLCAudVidRecordSchema);
            //注意mResultSet.next()的作用
            while (tCount++ < SysConst.FETCHCOUNT) {
                if (mResultSet.next()) {
                    tLCAudVidRecordSchema = new LCAudVidRecordSchema();
                    tLCAudVidRecordSchema.setSchema(mResultSet, 1);
                    tLCAudVidRecordSet.add(tLCAudVidRecordSchema);
                }
            }
        }
        catch (Exception ex) {
            CError tError = new CError();
            tError.moduleName = "LCAudVidRecordDB";
            tError.functionName = "getData";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors.addOneError(tError);
            try {
                mResultSet.close();
                mResultSet = null;
            }
            catch (Exception ex2) {}
            try {
                mStatement.close();
                mStatement = null;
            }
            catch (Exception ex3) {}
            if (!mflag) {
                try {
                    con.close();
                }
                catch (Exception et) {}
            }
            return null;
        }
        return tLCAudVidRecordSet;
    }
    /**
     * 关闭数据集
     * @return boolean
     */
    public boolean closeData() {
        boolean flag = true;
        try {
            if (null == mResultSet) {
                CError tError = new CError();
                tError.moduleName = "LCAudVidRecordDB";
                tError.functionName = "closeData";
                tError.errorMessage = "数据集已经关闭了！";
                this.mErrors.addOneError(tError);
                flag = false;
            } else {
                mResultSet.close();
                mResultSet = null;
            }
        }
        catch (Exception ex2) {
            CError tError = new CError();
            tError.moduleName = "LCAudVidRecordDB";
            tError.functionName = "closeData";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex2);
            this.mErrors.addOneError(tError);
            flag = false;
        }
        try {
            if (null == mStatement) {
                CError tError = new CError();
                tError.moduleName = "LCAudVidRecordDB";
                tError.functionName = "closeData";
                tError.errorMessage = "语句已经关闭了！";
                this.mErrors.addOneError(tError);
                flag = false;
            } else {
                mStatement.close();
                mStatement = null;
            }
        }
        catch (Exception ex3) {
            CError tError = new CError();
            tError.moduleName = "LCAudVidRecordDB";
            tError.functionName = "closeData";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex3);
            this.mErrors.addOneError(tError);
            flag = false;
        }
        return flag;
    }
}
