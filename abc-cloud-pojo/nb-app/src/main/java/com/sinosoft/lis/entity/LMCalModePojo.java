/**
 * Copyright (c) 2018 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.entity;

import com.sinosoft.cloud.cache.annotation.RedisPrimaryHKey;
import com.sinosoft.utility.Pojo;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;

import java.io.Serializable;

/**
 * <p>ClassName: LMCalModePojo </p>
 * <p>Copyright: Copyright (c) 2018</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2018-10-09
 */
public class LMCalModePojo implements Pojo,Serializable {
    // @Field
    /** 算法编码 */
	@RedisPrimaryHKey
    private String CalCode; 
    /** 险种编码 */
    private String RiskCode; 
    /** 算法类型 */
    private String Type; 
    /** 算法内容 */
    private String CalSQL; 
    /** 算法描述 */
    private String Remark; 
    /** 算法版本 */
    private int Version; 


    public static final int FIELDNUM = 6;    // 数据库表的字段个数
    public String getCalCode() {
        return CalCode;
    }
    public void setCalCode(String aCalCode) {
        CalCode = aCalCode;
    }
    public String getRiskCode() {
        return RiskCode;
    }
    public void setRiskCode(String aRiskCode) {
        RiskCode = aRiskCode;
    }
    public String getType() {
        return Type;
    }
    public void setType(String aType) {
        Type = aType;
    }
    public String getCalSQL() {
        return CalSQL;
    }
    public void setCalSQL(String aCalSQL) {
        CalSQL = aCalSQL;
    }
    public String getRemark() {
        return Remark;
    }
    public void setRemark(String aRemark) {
        Remark = aRemark;
    }
    public int getVersion() {
        return Version;
    }
    public void setVersion(int aVersion) {
        Version = aVersion;
    }
    public void setVersion(String aVersion) {
        if (aVersion != null && !aVersion.equals("")) {
            Integer tInteger = new Integer(aVersion);
            int i = tInteger.intValue();
            Version = i;
        }
    }


    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("CalCode") ) {
            return 0;
        }
        if( strFieldName.equals("RiskCode") ) {
            return 1;
        }
        if( strFieldName.equals("Type") ) {
            return 2;
        }
        if( strFieldName.equals("CalSQL") ) {
            return 3;
        }
        if( strFieldName.equals("Remark") ) {
            return 4;
        }
        if( strFieldName.equals("Version") ) {
            return 5;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "CalCode";
                break;
            case 1:
                strFieldName = "RiskCode";
                break;
            case 2:
                strFieldName = "Type";
                break;
            case 3:
                strFieldName = "CalSQL";
                break;
            case 4:
                strFieldName = "Remark";
                break;
            case 5:
                strFieldName = "Version";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "CALCODE":
                return Schema.TYPE_STRING;
            case "RISKCODE":
                return Schema.TYPE_STRING;
            case "TYPE":
                return Schema.TYPE_STRING;
            case "CALSQL":
                return Schema.TYPE_STRING;
            case "REMARK":
                return Schema.TYPE_STRING;
            case "VERSION":
                return Schema.TYPE_INT;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_STRING;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_INT;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("CalCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CalCode));
        }
        if (FCode.equalsIgnoreCase("RiskCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RiskCode));
        }
        if (FCode.equalsIgnoreCase("Type")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Type));
        }
        if (FCode.equalsIgnoreCase("CalSQL")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CalSQL));
        }
        if (FCode.equalsIgnoreCase("Remark")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Remark));
        }
        if (FCode.equalsIgnoreCase("Version")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Version));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = String.valueOf(CalCode);
                break;
            case 1:
                strFieldValue = String.valueOf(RiskCode);
                break;
            case 2:
                strFieldValue = String.valueOf(Type);
                break;
            case 3:
                strFieldValue = String.valueOf(CalSQL);
                break;
            case 4:
                strFieldValue = String.valueOf(Remark);
                break;
            case 5:
                strFieldValue = String.valueOf(Version);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("CalCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                CalCode = FValue.trim();
            }
            else
                CalCode = null;
        }
        if (FCode.equalsIgnoreCase("RiskCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                RiskCode = FValue.trim();
            }
            else
                RiskCode = null;
        }
        if (FCode.equalsIgnoreCase("Type")) {
            if( FValue != null && !FValue.equals(""))
            {
                Type = FValue.trim();
            }
            else
                Type = null;
        }
        if (FCode.equalsIgnoreCase("CalSQL")) {
            if( FValue != null && !FValue.equals(""))
            {
                CalSQL = FValue.trim();
            }
            else
                CalSQL = null;
        }
        if (FCode.equalsIgnoreCase("Remark")) {
            if( FValue != null && !FValue.equals(""))
            {
                Remark = FValue.trim();
            }
            else
                Remark = null;
        }
        if (FCode.equalsIgnoreCase("Version")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                Version = i;
            }
        }
        return true;
    }


    public String toString() {
    return "LMCalModePojo [" +
            "CalCode="+CalCode +
            ", RiskCode="+RiskCode +
            ", Type="+Type +
            ", CalSQL="+CalSQL +
            ", Remark="+Remark +
            ", Version="+Version +"]";
    }
}
