/**
 * Copyright (c) 2018 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import com.sinosoft.lis.db.LAAuthorizeNewDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.*;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

/**
 * <p>ClassName: LAAuthorizeNewSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2018</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2018-08-22
 */
public class LAAuthorizeNewSchema implements Schema, Cloneable {
    // @Field
    /** 险种编码 */
    private String RiskCode;
    /** 代理人编码 */
    private String AgentCode;
    /** Authorizeflag */
    private String AuthoRizeFlag;
    /** 操作员 */
    private String Operator;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 修改日期 */
    private Date ModifyDate;
    /** 修改时间 */
    private String ModifyTime;
    /** Notice1 */
    private String Notice1;
    /** Notice2 */
    private String Notice2;
    /** Branchtype */
    private String BranchType;
    /** Branchtype2 */
    private String BranchType2;

    public static final int FIELDNUM = 12;    // 数据库表的字段个数

    private static String[] PK;                // 主键

    private FDate fDate = new FDate();        // 处理日期

    public CErrors mErrors;            // 错误信息

    // @Constructor
    public LAAuthorizeNewSchema() {
        mErrors = new CErrors();

        String[] pk = new String[2];
        pk[0] = "RiskCode";
        pk[1] = "AgentCode";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException {
        LAAuthorizeNewSchema cloned = (LAAuthorizeNewSchema)super.clone();
        cloned.fDate = (FDate) fDate.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK() {
        return PK;
    }

    public String getRiskCode() {
        return RiskCode;
    }
    public void setRiskCode(String aRiskCode) {
        RiskCode = aRiskCode;
    }
    public String getAgentCode() {
        return AgentCode;
    }
    public void setAgentCode(String aAgentCode) {
        AgentCode = aAgentCode;
    }
    public String getAuthoRizeFlag() {
        return AuthoRizeFlag;
    }
    public void setAuthoRizeFlag(String aAuthoRizeFlag) {
        AuthoRizeFlag = aAuthoRizeFlag;
    }
    public String getOperator() {
        return Operator;
    }
    public void setOperator(String aOperator) {
        Operator = aOperator;
    }
    public String getMakeDate() {
        if(MakeDate != null) {
            return fDate.getString(MakeDate);
        } else {
            return null;
        }
    }
    public void setMakeDate(Date aMakeDate) {
        MakeDate = aMakeDate;
    }
    public void setMakeDate(String aMakeDate) {
        if (aMakeDate != null && !aMakeDate.equals("")) {
            MakeDate = fDate.getDate(aMakeDate);
        } else
            MakeDate = null;
    }

    public String getMakeTime() {
        return MakeTime;
    }
    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }
    public String getModifyDate() {
        if(ModifyDate != null) {
            return fDate.getString(ModifyDate);
        } else {
            return null;
        }
    }
    public void setModifyDate(Date aModifyDate) {
        ModifyDate = aModifyDate;
    }
    public void setModifyDate(String aModifyDate) {
        if (aModifyDate != null && !aModifyDate.equals("")) {
            ModifyDate = fDate.getDate(aModifyDate);
        } else
            ModifyDate = null;
    }

    public String getModifyTime() {
        return ModifyTime;
    }
    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }
    public String getNotice1() {
        return Notice1;
    }
    public void setNotice1(String aNotice1) {
        Notice1 = aNotice1;
    }
    public String getNotice2() {
        return Notice2;
    }
    public void setNotice2(String aNotice2) {
        Notice2 = aNotice2;
    }
    public String getBranchType() {
        return BranchType;
    }
    public void setBranchType(String aBranchType) {
        BranchType = aBranchType;
    }
    public String getBranchType2() {
        return BranchType2;
    }
    public void setBranchType2(String aBranchType2) {
        BranchType2 = aBranchType2;
    }

    /**
    * 使用另外一个 LAAuthorizeNewSchema 对象给 Schema 赋值
    * @param: aLAAuthorizeNewSchema LAAuthorizeNewSchema
    **/
    public void setSchema(LAAuthorizeNewSchema aLAAuthorizeNewSchema) {
        this.RiskCode = aLAAuthorizeNewSchema.getRiskCode();
        this.AgentCode = aLAAuthorizeNewSchema.getAgentCode();
        this.AuthoRizeFlag = aLAAuthorizeNewSchema.getAuthoRizeFlag();
        this.Operator = aLAAuthorizeNewSchema.getOperator();
        this.MakeDate = fDate.getDate( aLAAuthorizeNewSchema.getMakeDate());
        this.MakeTime = aLAAuthorizeNewSchema.getMakeTime();
        this.ModifyDate = fDate.getDate( aLAAuthorizeNewSchema.getModifyDate());
        this.ModifyTime = aLAAuthorizeNewSchema.getModifyTime();
        this.Notice1 = aLAAuthorizeNewSchema.getNotice1();
        this.Notice2 = aLAAuthorizeNewSchema.getNotice2();
        this.BranchType = aLAAuthorizeNewSchema.getBranchType();
        this.BranchType2 = aLAAuthorizeNewSchema.getBranchType2();
    }

    /**
    * 使用 ResultSet 中的第 i 行给 Schema 赋值
    * @param: rs ResultSet
    * @param: i int
    * @return: boolean
    **/
    public boolean setSchema(ResultSet rs,int i) {
        try {
            //rs.absolute(i);        // 非滚动游标
            if( rs.getString("RiskCode") == null )
                this.RiskCode = null;
            else
                this.RiskCode = rs.getString("RiskCode").trim();

            if( rs.getString("AgentCode") == null )
                this.AgentCode = null;
            else
                this.AgentCode = rs.getString("AgentCode").trim();

            if( rs.getString("AuthoRizeFlag") == null )
                this.AuthoRizeFlag = null;
            else
                this.AuthoRizeFlag = rs.getString("AuthoRizeFlag").trim();

            if( rs.getString("Operator") == null )
                this.Operator = null;
            else
                this.Operator = rs.getString("Operator").trim();

            this.MakeDate = rs.getDate("MakeDate");
            if( rs.getString("MakeTime") == null )
                this.MakeTime = null;
            else
                this.MakeTime = rs.getString("MakeTime").trim();

            this.ModifyDate = rs.getDate("ModifyDate");
            if( rs.getString("ModifyTime") == null )
                this.ModifyTime = null;
            else
                this.ModifyTime = rs.getString("ModifyTime").trim();

            if( rs.getString("Notice1") == null )
                this.Notice1 = null;
            else
                this.Notice1 = rs.getString("Notice1").trim();

            if( rs.getString("Notice2") == null )
                this.Notice2 = null;
            else
                this.Notice2 = rs.getString("Notice2").trim();

            if( rs.getString("BranchType") == null )
                this.BranchType = null;
            else
                this.BranchType = rs.getString("BranchType").trim();

            if( rs.getString("BranchType2") == null )
                this.BranchType2 = null;
            else
                this.BranchType2 = rs.getString("BranchType2").trim();

        }
        catch(SQLException sqle) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAAuthorizeNewSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = ExceptionUtils.exceptionToString(sqle);
            this.mErrors .addOneError(tError);
            return false;
        }
        return true;
    }

    public LAAuthorizeNewSchema getSchema() {
        LAAuthorizeNewSchema aLAAuthorizeNewSchema = new LAAuthorizeNewSchema();
        aLAAuthorizeNewSchema.setSchema(this);
        return aLAAuthorizeNewSchema;
    }

    public LAAuthorizeNewDB getDB() {
        LAAuthorizeNewDB aDBOper = new LAAuthorizeNewDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
    * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLAAuthorizeNew描述/A>表字段
    * @return: String 返回打包后字符串
    **/
    public String encode() {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(RiskCode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AgentCode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AuthoRizeFlag)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ModifyTime)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Notice1)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Notice2)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(BranchType)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(BranchType2));
        return strReturn.toString();
    }

    /**
    * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLAAuthorizeNew>历史记账凭证主表信息</A>表字段
    * @param: strMessage String 包含一条纪录数据的字符串
    * @return: boolean
    **/
    public boolean decode(String strMessage) {
        try {
            RiskCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
            AgentCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
            AuthoRizeFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
            Notice1 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
            Notice2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
            BranchType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
            BranchType2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
        }
        catch(NumberFormatException ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAAuthorizeNewSchema";
            tError.functionName = "decode";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            return false;
        }
        return true;
    }

    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("RiskCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RiskCode));
        }
        if (FCode.equalsIgnoreCase("AgentCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentCode));
        }
        if (FCode.equalsIgnoreCase("AuthoRizeFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AuthoRizeFlag));
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (FCode.equalsIgnoreCase("Notice1")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Notice1));
        }
        if (FCode.equalsIgnoreCase("Notice2")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Notice2));
        }
        if (FCode.equalsIgnoreCase("BranchType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BranchType));
        }
        if (FCode.equalsIgnoreCase("BranchType2")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BranchType2));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(RiskCode);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(AgentCode);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(AuthoRizeFlag);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(Operator);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(MakeTime);
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(ModifyTime);
                break;
            case 8:
                strFieldValue = StrTool.GBKToUnicode(Notice1);
                break;
            case 9:
                strFieldValue = StrTool.GBKToUnicode(Notice2);
                break;
            case 10:
                strFieldValue = StrTool.GBKToUnicode(BranchType);
                break;
            case 11:
                strFieldValue = StrTool.GBKToUnicode(BranchType2);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("RiskCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                RiskCode = FValue.trim();
            }
            else
                RiskCode = null;
        }
        if (FCode.equalsIgnoreCase("AgentCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                AgentCode = FValue.trim();
            }
            else
                AgentCode = null;
        }
        if (FCode.equalsIgnoreCase("AuthoRizeFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                AuthoRizeFlag = FValue.trim();
            }
            else
                AuthoRizeFlag = null;
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            if( FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
                Operator = null;
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if(FValue != null && !FValue.equals("")) {
                MakeDate = fDate.getDate( FValue );
            }
            else
                MakeDate = null;
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
                MakeTime = null;
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if(FValue != null && !FValue.equals("")) {
                ModifyDate = fDate.getDate( FValue );
            }
            else
                ModifyDate = null;
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
                ModifyTime = null;
        }
        if (FCode.equalsIgnoreCase("Notice1")) {
            if( FValue != null && !FValue.equals(""))
            {
                Notice1 = FValue.trim();
            }
            else
                Notice1 = null;
        }
        if (FCode.equalsIgnoreCase("Notice2")) {
            if( FValue != null && !FValue.equals(""))
            {
                Notice2 = FValue.trim();
            }
            else
                Notice2 = null;
        }
        if (FCode.equalsIgnoreCase("BranchType")) {
            if( FValue != null && !FValue.equals(""))
            {
                BranchType = FValue.trim();
            }
            else
                BranchType = null;
        }
        if (FCode.equalsIgnoreCase("BranchType2")) {
            if( FValue != null && !FValue.equals(""))
            {
                BranchType2 = FValue.trim();
            }
            else
                BranchType2 = null;
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject) return true;
        if (otherObject == null) return false;
        if (getClass() != otherObject.getClass()) return false;
        LAAuthorizeNewSchema other = (LAAuthorizeNewSchema)otherObject;
        return
            RiskCode.equals(other.getRiskCode())
            && AgentCode.equals(other.getAgentCode())
            && AuthoRizeFlag.equals(other.getAuthoRizeFlag())
            && Operator.equals(other.getOperator())
            && fDate.getString(MakeDate).equals(other.getMakeDate())
            && MakeTime.equals(other.getMakeTime())
            && fDate.getString(ModifyDate).equals(other.getModifyDate())
            && ModifyTime.equals(other.getModifyTime())
            && Notice1.equals(other.getNotice1())
            && Notice2.equals(other.getNotice2())
            && BranchType.equals(other.getBranchType())
            && BranchType2.equals(other.getBranchType2());
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("RiskCode") ) {
            return 0;
        }
        if( strFieldName.equals("AgentCode") ) {
            return 1;
        }
        if( strFieldName.equals("AuthoRizeFlag") ) {
            return 2;
        }
        if( strFieldName.equals("Operator") ) {
            return 3;
        }
        if( strFieldName.equals("MakeDate") ) {
            return 4;
        }
        if( strFieldName.equals("MakeTime") ) {
            return 5;
        }
        if( strFieldName.equals("ModifyDate") ) {
            return 6;
        }
        if( strFieldName.equals("ModifyTime") ) {
            return 7;
        }
        if( strFieldName.equals("Notice1") ) {
            return 8;
        }
        if( strFieldName.equals("Notice2") ) {
            return 9;
        }
        if( strFieldName.equals("BranchType") ) {
            return 10;
        }
        if( strFieldName.equals("BranchType2") ) {
            return 11;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "RiskCode";
                break;
            case 1:
                strFieldName = "AgentCode";
                break;
            case 2:
                strFieldName = "AuthoRizeFlag";
                break;
            case 3:
                strFieldName = "Operator";
                break;
            case 4:
                strFieldName = "MakeDate";
                break;
            case 5:
                strFieldName = "MakeTime";
                break;
            case 6:
                strFieldName = "ModifyDate";
                break;
            case 7:
                strFieldName = "ModifyTime";
                break;
            case 8:
                strFieldName = "Notice1";
                break;
            case 9:
                strFieldName = "Notice2";
                break;
            case 10:
                strFieldName = "BranchType";
                break;
            case 11:
                strFieldName = "BranchType2";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "RISKCODE":
                return Schema.TYPE_STRING;
            case "AGENTCODE":
                return Schema.TYPE_STRING;
            case "AUTHORIZEFLAG":
                return Schema.TYPE_STRING;
            case "OPERATOR":
                return Schema.TYPE_STRING;
            case "MAKEDATE":
                return Schema.TYPE_DATE;
            case "MAKETIME":
                return Schema.TYPE_STRING;
            case "MODIFYDATE":
                return Schema.TYPE_DATE;
            case "MODIFYTIME":
                return Schema.TYPE_STRING;
            case "NOTICE1":
                return Schema.TYPE_STRING;
            case "NOTICE2":
                return Schema.TYPE_STRING;
            case "BRANCHTYPE":
                return Schema.TYPE_STRING;
            case "BRANCHTYPE2":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_STRING;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_DATE;
            case 5:
                return Schema.TYPE_STRING;
            case 6:
                return Schema.TYPE_DATE;
            case 7:
                return Schema.TYPE_STRING;
            case 8:
                return Schema.TYPE_STRING;
            case 9:
                return Schema.TYPE_STRING;
            case 10:
                return Schema.TYPE_STRING;
            case 11:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
}
