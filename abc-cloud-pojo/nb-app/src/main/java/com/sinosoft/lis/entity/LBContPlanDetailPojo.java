/**
 * Copyright (c) 2018 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.entity;

import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.Pojo;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;

import java.io.Serializable;

/**
 * <p>ClassName: LBContPlanDetailPojo </p>
 * <p>Copyright: Copyright (c) 2018</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2018-05-23
 */
public class LBContPlanDetailPojo implements Pojo,Serializable {
    // @Field
    /** 保单号码 */
    private String PolicyNo; 
    /** 投保单号 */
    private String PropNo; 
    /** 系统方案编码 */
    private String SysPlanCode; 
    /** 方案编码 */
    private String PlanCode; 
    /** 险种编码 */
    private String RiskCode; 
    /** 责任编码 */
    private String DutyCode; 
    /** 保额类型 */
    private String AmntType; 
    /** 固定保额 */
    private String FixedAmnt; 
    /** 月薪倍数 */
    private String SalaryMult; 
    /** 最高保额 */
    private String MaxAmnt; 
    /** 最低保额 */
    private String MinAmnt; 
    /** 期望保费类型 */
    private String ExceptPremType; 
    /** 期望保费/费率/折扣 */
    private String ExceptPrem; 
    /** 参考保费/费率 */
    private String StandValue; 
    /** 初始保费 */
    private String InitPrem; 
    /** 预期收益率 */
    private String ExceptYield; 
    /** 最终值 */
    private String FinalValue; 
    /** 变更类型 */
    private String ChangeType; 
    /** 主附共用标识 */
    private String RelaShareFlag; 
    /** 备注 */
    private String Remark; 
    /** 入机操作员 */
    private String MakeOperator; 
    /** 入机日期 */
    private String  MakeDate;
    /** 入机时间 */
    private String MakeTime; 
    /** 最后一次修改操作员 */
    private String ModifyOperator; 
    /** 最后一次修改日期 */
    private String  ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime; 


    public static final int FIELDNUM = 26;    // 数据库表的字段个数

    private FDate fDate = new FDate();        // 处理日期
    public String getPolicyNo() {
        return PolicyNo;
    }
    public void setPolicyNo(String aPolicyNo) {
        PolicyNo = aPolicyNo;
    }
    public String getPropNo() {
        return PropNo;
    }
    public void setPropNo(String aPropNo) {
        PropNo = aPropNo;
    }
    public String getSysPlanCode() {
        return SysPlanCode;
    }
    public void setSysPlanCode(String aSysPlanCode) {
        SysPlanCode = aSysPlanCode;
    }
    public String getPlanCode() {
        return PlanCode;
    }
    public void setPlanCode(String aPlanCode) {
        PlanCode = aPlanCode;
    }
    public String getRiskCode() {
        return RiskCode;
    }
    public void setRiskCode(String aRiskCode) {
        RiskCode = aRiskCode;
    }
    public String getDutyCode() {
        return DutyCode;
    }
    public void setDutyCode(String aDutyCode) {
        DutyCode = aDutyCode;
    }
    public String getAmntType() {
        return AmntType;
    }
    public void setAmntType(String aAmntType) {
        AmntType = aAmntType;
    }
    public String getFixedAmnt() {
        return FixedAmnt;
    }
    public void setFixedAmnt(String aFixedAmnt) {
        FixedAmnt = aFixedAmnt;
    }
    public String getSalaryMult() {
        return SalaryMult;
    }
    public void setSalaryMult(String aSalaryMult) {
        SalaryMult = aSalaryMult;
    }
    public String getMaxAmnt() {
        return MaxAmnt;
    }
    public void setMaxAmnt(String aMaxAmnt) {
        MaxAmnt = aMaxAmnt;
    }
    public String getMinAmnt() {
        return MinAmnt;
    }
    public void setMinAmnt(String aMinAmnt) {
        MinAmnt = aMinAmnt;
    }
    public String getExceptPremType() {
        return ExceptPremType;
    }
    public void setExceptPremType(String aExceptPremType) {
        ExceptPremType = aExceptPremType;
    }
    public String getExceptPrem() {
        return ExceptPrem;
    }
    public void setExceptPrem(String aExceptPrem) {
        ExceptPrem = aExceptPrem;
    }
    public String getStandValue() {
        return StandValue;
    }
    public void setStandValue(String aStandValue) {
        StandValue = aStandValue;
    }
    public String getInitPrem() {
        return InitPrem;
    }
    public void setInitPrem(String aInitPrem) {
        InitPrem = aInitPrem;
    }
    public String getExceptYield() {
        return ExceptYield;
    }
    public void setExceptYield(String aExceptYield) {
        ExceptYield = aExceptYield;
    }
    public String getFinalValue() {
        return FinalValue;
    }
    public void setFinalValue(String aFinalValue) {
        FinalValue = aFinalValue;
    }
    public String getChangeType() {
        return ChangeType;
    }
    public void setChangeType(String aChangeType) {
        ChangeType = aChangeType;
    }
    public String getRelaShareFlag() {
        return RelaShareFlag;
    }
    public void setRelaShareFlag(String aRelaShareFlag) {
        RelaShareFlag = aRelaShareFlag;
    }
    public String getRemark() {
        return Remark;
    }
    public void setRemark(String aRemark) {
        Remark = aRemark;
    }
    public String getMakeOperator() {
        return MakeOperator;
    }
    public void setMakeOperator(String aMakeOperator) {
        MakeOperator = aMakeOperator;
    }
    public String getMakeDate() {
        return MakeDate;
    }
    public void setMakeDate(String aMakeDate) {
        MakeDate = aMakeDate;
    }
    public String getMakeTime() {
        return MakeTime;
    }
    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }
    public String getModifyOperator() {
        return ModifyOperator;
    }
    public void setModifyOperator(String aModifyOperator) {
        ModifyOperator = aModifyOperator;
    }
    public String getModifyDate() {
        return ModifyDate;
    }
    public void setModifyDate(String aModifyDate) {
        ModifyDate = aModifyDate;
    }
    public String getModifyTime() {
        return ModifyTime;
    }
    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("PolicyNo") ) {
            return 0;
        }
        if( strFieldName.equals("PropNo") ) {
            return 1;
        }
        if( strFieldName.equals("SysPlanCode") ) {
            return 2;
        }
        if( strFieldName.equals("PlanCode") ) {
            return 3;
        }
        if( strFieldName.equals("RiskCode") ) {
            return 4;
        }
        if( strFieldName.equals("DutyCode") ) {
            return 5;
        }
        if( strFieldName.equals("AmntType") ) {
            return 6;
        }
        if( strFieldName.equals("FixedAmnt") ) {
            return 7;
        }
        if( strFieldName.equals("SalaryMult") ) {
            return 8;
        }
        if( strFieldName.equals("MaxAmnt") ) {
            return 9;
        }
        if( strFieldName.equals("MinAmnt") ) {
            return 10;
        }
        if( strFieldName.equals("ExceptPremType") ) {
            return 11;
        }
        if( strFieldName.equals("ExceptPrem") ) {
            return 12;
        }
        if( strFieldName.equals("StandValue") ) {
            return 13;
        }
        if( strFieldName.equals("InitPrem") ) {
            return 14;
        }
        if( strFieldName.equals("ExceptYield") ) {
            return 15;
        }
        if( strFieldName.equals("FinalValue") ) {
            return 16;
        }
        if( strFieldName.equals("ChangeType") ) {
            return 17;
        }
        if( strFieldName.equals("RelaShareFlag") ) {
            return 18;
        }
        if( strFieldName.equals("Remark") ) {
            return 19;
        }
        if( strFieldName.equals("MakeOperator") ) {
            return 20;
        }
        if( strFieldName.equals("MakeDate") ) {
            return 21;
        }
        if( strFieldName.equals("MakeTime") ) {
            return 22;
        }
        if( strFieldName.equals("ModifyOperator") ) {
            return 23;
        }
        if( strFieldName.equals("ModifyDate") ) {
            return 24;
        }
        if( strFieldName.equals("ModifyTime") ) {
            return 25;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "PolicyNo";
                break;
            case 1:
                strFieldName = "PropNo";
                break;
            case 2:
                strFieldName = "SysPlanCode";
                break;
            case 3:
                strFieldName = "PlanCode";
                break;
            case 4:
                strFieldName = "RiskCode";
                break;
            case 5:
                strFieldName = "DutyCode";
                break;
            case 6:
                strFieldName = "AmntType";
                break;
            case 7:
                strFieldName = "FixedAmnt";
                break;
            case 8:
                strFieldName = "SalaryMult";
                break;
            case 9:
                strFieldName = "MaxAmnt";
                break;
            case 10:
                strFieldName = "MinAmnt";
                break;
            case 11:
                strFieldName = "ExceptPremType";
                break;
            case 12:
                strFieldName = "ExceptPrem";
                break;
            case 13:
                strFieldName = "StandValue";
                break;
            case 14:
                strFieldName = "InitPrem";
                break;
            case 15:
                strFieldName = "ExceptYield";
                break;
            case 16:
                strFieldName = "FinalValue";
                break;
            case 17:
                strFieldName = "ChangeType";
                break;
            case 18:
                strFieldName = "RelaShareFlag";
                break;
            case 19:
                strFieldName = "Remark";
                break;
            case 20:
                strFieldName = "MakeOperator";
                break;
            case 21:
                strFieldName = "MakeDate";
                break;
            case 22:
                strFieldName = "MakeTime";
                break;
            case 23:
                strFieldName = "ModifyOperator";
                break;
            case 24:
                strFieldName = "ModifyDate";
                break;
            case 25:
                strFieldName = "ModifyTime";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "POLICYNO":
                return Schema.TYPE_STRING;
            case "PROPNO":
                return Schema.TYPE_STRING;
            case "SYSPLANCODE":
                return Schema.TYPE_STRING;
            case "PLANCODE":
                return Schema.TYPE_STRING;
            case "RISKCODE":
                return Schema.TYPE_STRING;
            case "DUTYCODE":
                return Schema.TYPE_STRING;
            case "AMNTTYPE":
                return Schema.TYPE_STRING;
            case "FIXEDAMNT":
                return Schema.TYPE_STRING;
            case "SALARYMULT":
                return Schema.TYPE_STRING;
            case "MAXAMNT":
                return Schema.TYPE_STRING;
            case "MINAMNT":
                return Schema.TYPE_STRING;
            case "EXCEPTPREMTYPE":
                return Schema.TYPE_STRING;
            case "EXCEPTPREM":
                return Schema.TYPE_STRING;
            case "STANDVALUE":
                return Schema.TYPE_STRING;
            case "INITPREM":
                return Schema.TYPE_STRING;
            case "EXCEPTYIELD":
                return Schema.TYPE_STRING;
            case "FINALVALUE":
                return Schema.TYPE_STRING;
            case "CHANGETYPE":
                return Schema.TYPE_STRING;
            case "RELASHAREFLAG":
                return Schema.TYPE_STRING;
            case "REMARK":
                return Schema.TYPE_STRING;
            case "MAKEOPERATOR":
                return Schema.TYPE_STRING;
            case "MAKEDATE":
                return Schema.TYPE_STRING;
            case "MAKETIME":
                return Schema.TYPE_STRING;
            case "MODIFYOPERATOR":
                return Schema.TYPE_STRING;
            case "MODIFYDATE":
                return Schema.TYPE_STRING;
            case "MODIFYTIME":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_STRING;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_STRING;
            case 6:
                return Schema.TYPE_STRING;
            case 7:
                return Schema.TYPE_STRING;
            case 8:
                return Schema.TYPE_STRING;
            case 9:
                return Schema.TYPE_STRING;
            case 10:
                return Schema.TYPE_STRING;
            case 11:
                return Schema.TYPE_STRING;
            case 12:
                return Schema.TYPE_STRING;
            case 13:
                return Schema.TYPE_STRING;
            case 14:
                return Schema.TYPE_STRING;
            case 15:
                return Schema.TYPE_STRING;
            case 16:
                return Schema.TYPE_STRING;
            case 17:
                return Schema.TYPE_STRING;
            case 18:
                return Schema.TYPE_STRING;
            case 19:
                return Schema.TYPE_STRING;
            case 20:
                return Schema.TYPE_STRING;
            case 21:
                return Schema.TYPE_STRING;
            case 22:
                return Schema.TYPE_STRING;
            case 23:
                return Schema.TYPE_STRING;
            case 24:
                return Schema.TYPE_STRING;
            case 25:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("PolicyNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PolicyNo));
        }
        if (FCode.equalsIgnoreCase("PropNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PropNo));
        }
        if (FCode.equalsIgnoreCase("SysPlanCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SysPlanCode));
        }
        if (FCode.equalsIgnoreCase("PlanCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PlanCode));
        }
        if (FCode.equalsIgnoreCase("RiskCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RiskCode));
        }
        if (FCode.equalsIgnoreCase("DutyCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DutyCode));
        }
        if (FCode.equalsIgnoreCase("AmntType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AmntType));
        }
        if (FCode.equalsIgnoreCase("FixedAmnt")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FixedAmnt));
        }
        if (FCode.equalsIgnoreCase("SalaryMult")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SalaryMult));
        }
        if (FCode.equalsIgnoreCase("MaxAmnt")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MaxAmnt));
        }
        if (FCode.equalsIgnoreCase("MinAmnt")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MinAmnt));
        }
        if (FCode.equalsIgnoreCase("ExceptPremType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ExceptPremType));
        }
        if (FCode.equalsIgnoreCase("ExceptPrem")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ExceptPrem));
        }
        if (FCode.equalsIgnoreCase("StandValue")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StandValue));
        }
        if (FCode.equalsIgnoreCase("InitPrem")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InitPrem));
        }
        if (FCode.equalsIgnoreCase("ExceptYield")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ExceptYield));
        }
        if (FCode.equalsIgnoreCase("FinalValue")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FinalValue));
        }
        if (FCode.equalsIgnoreCase("ChangeType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ChangeType));
        }
        if (FCode.equalsIgnoreCase("RelaShareFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RelaShareFlag));
        }
        if (FCode.equalsIgnoreCase("Remark")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Remark));
        }
        if (FCode.equalsIgnoreCase("MakeOperator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeOperator));
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeDate));
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equalsIgnoreCase("ModifyOperator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyOperator));
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyDate));
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = String.valueOf(PolicyNo);
                break;
            case 1:
                strFieldValue = String.valueOf(PropNo);
                break;
            case 2:
                strFieldValue = String.valueOf(SysPlanCode);
                break;
            case 3:
                strFieldValue = String.valueOf(PlanCode);
                break;
            case 4:
                strFieldValue = String.valueOf(RiskCode);
                break;
            case 5:
                strFieldValue = String.valueOf(DutyCode);
                break;
            case 6:
                strFieldValue = String.valueOf(AmntType);
                break;
            case 7:
                strFieldValue = String.valueOf(FixedAmnt);
                break;
            case 8:
                strFieldValue = String.valueOf(SalaryMult);
                break;
            case 9:
                strFieldValue = String.valueOf(MaxAmnt);
                break;
            case 10:
                strFieldValue = String.valueOf(MinAmnt);
                break;
            case 11:
                strFieldValue = String.valueOf(ExceptPremType);
                break;
            case 12:
                strFieldValue = String.valueOf(ExceptPrem);
                break;
            case 13:
                strFieldValue = String.valueOf(StandValue);
                break;
            case 14:
                strFieldValue = String.valueOf(InitPrem);
                break;
            case 15:
                strFieldValue = String.valueOf(ExceptYield);
                break;
            case 16:
                strFieldValue = String.valueOf(FinalValue);
                break;
            case 17:
                strFieldValue = String.valueOf(ChangeType);
                break;
            case 18:
                strFieldValue = String.valueOf(RelaShareFlag);
                break;
            case 19:
                strFieldValue = String.valueOf(Remark);
                break;
            case 20:
                strFieldValue = String.valueOf(MakeOperator);
                break;
            case 21:
                strFieldValue = String.valueOf(MakeDate);
                break;
            case 22:
                strFieldValue = String.valueOf(MakeTime);
                break;
            case 23:
                strFieldValue = String.valueOf(ModifyOperator);
                break;
            case 24:
                strFieldValue = String.valueOf(ModifyDate);
                break;
            case 25:
                strFieldValue = String.valueOf(ModifyTime);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("PolicyNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                PolicyNo = FValue.trim();
            }
            else
                PolicyNo = null;
        }
        if (FCode.equalsIgnoreCase("PropNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                PropNo = FValue.trim();
            }
            else
                PropNo = null;
        }
        if (FCode.equalsIgnoreCase("SysPlanCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                SysPlanCode = FValue.trim();
            }
            else
                SysPlanCode = null;
        }
        if (FCode.equalsIgnoreCase("PlanCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                PlanCode = FValue.trim();
            }
            else
                PlanCode = null;
        }
        if (FCode.equalsIgnoreCase("RiskCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                RiskCode = FValue.trim();
            }
            else
                RiskCode = null;
        }
        if (FCode.equalsIgnoreCase("DutyCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                DutyCode = FValue.trim();
            }
            else
                DutyCode = null;
        }
        if (FCode.equalsIgnoreCase("AmntType")) {
            if( FValue != null && !FValue.equals(""))
            {
                AmntType = FValue.trim();
            }
            else
                AmntType = null;
        }
        if (FCode.equalsIgnoreCase("FixedAmnt")) {
            if( FValue != null && !FValue.equals(""))
            {
                FixedAmnt = FValue.trim();
            }
            else
                FixedAmnt = null;
        }
        if (FCode.equalsIgnoreCase("SalaryMult")) {
            if( FValue != null && !FValue.equals(""))
            {
                SalaryMult = FValue.trim();
            }
            else
                SalaryMult = null;
        }
        if (FCode.equalsIgnoreCase("MaxAmnt")) {
            if( FValue != null && !FValue.equals(""))
            {
                MaxAmnt = FValue.trim();
            }
            else
                MaxAmnt = null;
        }
        if (FCode.equalsIgnoreCase("MinAmnt")) {
            if( FValue != null && !FValue.equals(""))
            {
                MinAmnt = FValue.trim();
            }
            else
                MinAmnt = null;
        }
        if (FCode.equalsIgnoreCase("ExceptPremType")) {
            if( FValue != null && !FValue.equals(""))
            {
                ExceptPremType = FValue.trim();
            }
            else
                ExceptPremType = null;
        }
        if (FCode.equalsIgnoreCase("ExceptPrem")) {
            if( FValue != null && !FValue.equals(""))
            {
                ExceptPrem = FValue.trim();
            }
            else
                ExceptPrem = null;
        }
        if (FCode.equalsIgnoreCase("StandValue")) {
            if( FValue != null && !FValue.equals(""))
            {
                StandValue = FValue.trim();
            }
            else
                StandValue = null;
        }
        if (FCode.equalsIgnoreCase("InitPrem")) {
            if( FValue != null && !FValue.equals(""))
            {
                InitPrem = FValue.trim();
            }
            else
                InitPrem = null;
        }
        if (FCode.equalsIgnoreCase("ExceptYield")) {
            if( FValue != null && !FValue.equals(""))
            {
                ExceptYield = FValue.trim();
            }
            else
                ExceptYield = null;
        }
        if (FCode.equalsIgnoreCase("FinalValue")) {
            if( FValue != null && !FValue.equals(""))
            {
                FinalValue = FValue.trim();
            }
            else
                FinalValue = null;
        }
        if (FCode.equalsIgnoreCase("ChangeType")) {
            if( FValue != null && !FValue.equals(""))
            {
                ChangeType = FValue.trim();
            }
            else
                ChangeType = null;
        }
        if (FCode.equalsIgnoreCase("RelaShareFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                RelaShareFlag = FValue.trim();
            }
            else
                RelaShareFlag = null;
        }
        if (FCode.equalsIgnoreCase("Remark")) {
            if( FValue != null && !FValue.equals(""))
            {
                Remark = FValue.trim();
            }
            else
                Remark = null;
        }
        if (FCode.equalsIgnoreCase("MakeOperator")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeOperator = FValue.trim();
            }
            else
                MakeOperator = null;
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeDate = FValue.trim();
            }
            else
                MakeDate = null;
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
                MakeTime = null;
        }
        if (FCode.equalsIgnoreCase("ModifyOperator")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyOperator = FValue.trim();
            }
            else
                ModifyOperator = null;
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyDate = FValue.trim();
            }
            else
                ModifyDate = null;
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
                ModifyTime = null;
        }
        return true;
    }


    public String toString() {
    return "LBContPlanDetailPojo [" +
            "PolicyNo="+PolicyNo +
            ", PropNo="+PropNo +
            ", SysPlanCode="+SysPlanCode +
            ", PlanCode="+PlanCode +
            ", RiskCode="+RiskCode +
            ", DutyCode="+DutyCode +
            ", AmntType="+AmntType +
            ", FixedAmnt="+FixedAmnt +
            ", SalaryMult="+SalaryMult +
            ", MaxAmnt="+MaxAmnt +
            ", MinAmnt="+MinAmnt +
            ", ExceptPremType="+ExceptPremType +
            ", ExceptPrem="+ExceptPrem +
            ", StandValue="+StandValue +
            ", InitPrem="+InitPrem +
            ", ExceptYield="+ExceptYield +
            ", FinalValue="+FinalValue +
            ", ChangeType="+ChangeType +
            ", RelaShareFlag="+RelaShareFlag +
            ", Remark="+Remark +
            ", MakeOperator="+MakeOperator +
            ", MakeDate="+MakeDate +
            ", MakeTime="+MakeTime +
            ", ModifyOperator="+ModifyOperator +
            ", ModifyDate="+ModifyDate +
            ", ModifyTime="+ModifyTime +"]";
    }
}
