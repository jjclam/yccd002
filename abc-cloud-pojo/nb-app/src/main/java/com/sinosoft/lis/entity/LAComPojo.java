/**
 * Copyright (c) 2017 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.entity;

import com.sinosoft.cloud.cache.annotation.RedisPrimaryHKey;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.Pojo;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;

import java.io.Serializable;

/**
 * <p>ClassName: LAComPojo </p>
 * <p>Copyright: Copyright (c) 2017</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2017-10-20
 */
public class LAComPojo implements Pojo,Serializable {
    // @Field
    /** 代理机构 */
    @RedisPrimaryHKey
    private String AgentCom;
    /** 管理机构 */
    private String ManageCom; 
    /** 地区类型 */
    private String AreaType; 
    /** 渠道类型 */
    private String ChannelType; 
    /** 上级代理机构 */
    private String UpAgentCom; 
    /** 机构名称 */
    private String Name; 
    /** 机构注册地址 */
    private String Address; 
    /** 机构邮编 */
    private String ZipCode; 
    /** 机构电话 */
    private String Phone; 
    /** 机构传真 */
    private String Fax; 
    /** Email */
    private String EMail; 
    /** 网址 */
    private String WebAddress; 
    /** 负责人 */
    private String LinkMan; 
    /** 密码 */
    private String Password; 
    /** 法人 */
    private String Corporation; 
    /** 银行编码 */
    private String BankCode; 
    /** 银行帐号 */
    private String BankAccNo; 
    /** 行业分类 */
    private String BusinessType; 
    /** 单位性质 */
    private String GrpNature; 
    /** 中介机构类别 */
    private String ACType; 
    /** 销售资格 */
    private String SellFlag; 
    /** 操作员代码 */
    private String Operator; 
    /** 入机日期 */
    private String  MakeDate;
    /** 入机时间 */
    private String MakeTime; 
    /** 最后一次修改日期 */
    private String  ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime; 
    /** 银行级别 */
    private String BankType; 
    /** 是否统计网点合格率 */
    private String CalFlag; 
    /** 工商执照编码 */
    private String BusiLicenseCode; 
    /** 保险公司id */
    private String InsureID; 
    /** 保险公司负责人 */
    private String InsurePrincipal; 
    /** 主营业务 */
    private String ChiefBusiness; 
    /** 营业地址 */
    private String BusiAddress; 
    /** 签署人 */
    private String SubscribeMan; 
    /** 签署人职务 */
    private String SubscribeManDuty; 
    /** 许可证号码 */
    private String LicenseNo; 
    /** 行政区划代码 */
    private String RegionalismCode; 
    /** 上报代码 */
    private String AppAgentCom; 
    /** 机构状态 */
    private String State; 
    /** 相关说明 */
    private String Noti; 
    /** 行业代码 */
    private String BusinessCode; 
    /** 许可证登记日期 */
    private String  LicenseStartDate;
    /** 许可证截至日期 */
    private String  LicenseEndDate;
    /** 展业类型 */
    private String BranchType; 
    /** 渠道 */
    private String BranchType2; 
    /** 资产 */
    private double Assets; 
    /** 营业收入 */
    private double Income; 
    /** 营业利润 */
    private double Profits; 
    /** 机构人数 */
    private int PersonnalSum; 
    /** 合同编码 */
    private String ProtocalNo; 
    /** 所属总行 */
    private String HeadOffice; 
    /** 成立日期 */
    private String  FoundDate;
    /** 停业日期 */
    private String  EndDate;
    /** 开户银行 */
    private String Bank; 
    /** 账户名称 */
    private String AccName; 
    /** 授权领取人姓名 */
    private String DraWer; 
    /** 授权领取人银行账号 */
    private String DraWerAccNo; 
    /** 上级管理机构 */
    private String RepManageCom; 
    /** 授权领取人银行编码 */
    private String DraWerAccCode; 
    /** 授权领取人银行名称 */
    private String DraWerAccName; 
    /** 中介机构渠道类别 */
    private String ChannelType2; 
    /** 区域类型 */
    private String AreaType2; 


    public static final int FIELDNUM = 62;    // 数据库表的字段个数

    private FDate fDate = new FDate();        // 处理日期
    public String getAgentCom() {
        return AgentCom;
    }
    public void setAgentCom(String aAgentCom) {
        AgentCom = aAgentCom;
    }
    public String getManageCom() {
        return ManageCom;
    }
    public void setManageCom(String aManageCom) {
        ManageCom = aManageCom;
    }
    public String getAreaType() {
        return AreaType;
    }
    public void setAreaType(String aAreaType) {
        AreaType = aAreaType;
    }
    public String getChannelType() {
        return ChannelType;
    }
    public void setChannelType(String aChannelType) {
        ChannelType = aChannelType;
    }
    public String getUpAgentCom() {
        return UpAgentCom;
    }
    public void setUpAgentCom(String aUpAgentCom) {
        UpAgentCom = aUpAgentCom;
    }
    public String getName() {
        return Name;
    }
    public void setName(String aName) {
        Name = aName;
    }
    public String getAddress() {
        return Address;
    }
    public void setAddress(String aAddress) {
        Address = aAddress;
    }
    public String getZipCode() {
        return ZipCode;
    }
    public void setZipCode(String aZipCode) {
        ZipCode = aZipCode;
    }
    public String getPhone() {
        return Phone;
    }
    public void setPhone(String aPhone) {
        Phone = aPhone;
    }
    public String getFax() {
        return Fax;
    }
    public void setFax(String aFax) {
        Fax = aFax;
    }
    public String getEMail() {
        return EMail;
    }
    public void setEMail(String aEMail) {
        EMail = aEMail;
    }
    public String getWebAddress() {
        return WebAddress;
    }
    public void setWebAddress(String aWebAddress) {
        WebAddress = aWebAddress;
    }
    public String getLinkMan() {
        return LinkMan;
    }
    public void setLinkMan(String aLinkMan) {
        LinkMan = aLinkMan;
    }
    public String getPassword() {
        return Password;
    }
    public void setPassword(String aPassword) {
        Password = aPassword;
    }
    public String getCorporation() {
        return Corporation;
    }
    public void setCorporation(String aCorporation) {
        Corporation = aCorporation;
    }
    public String getBankCode() {
        return BankCode;
    }
    public void setBankCode(String aBankCode) {
        BankCode = aBankCode;
    }
    public String getBankAccNo() {
        return BankAccNo;
    }
    public void setBankAccNo(String aBankAccNo) {
        BankAccNo = aBankAccNo;
    }
    public String getBusinessType() {
        return BusinessType;
    }
    public void setBusinessType(String aBusinessType) {
        BusinessType = aBusinessType;
    }
    public String getGrpNature() {
        return GrpNature;
    }
    public void setGrpNature(String aGrpNature) {
        GrpNature = aGrpNature;
    }
    public String getACType() {
        return ACType;
    }
    public void setACType(String aACType) {
        ACType = aACType;
    }
    public String getSellFlag() {
        return SellFlag;
    }
    public void setSellFlag(String aSellFlag) {
        SellFlag = aSellFlag;
    }
    public String getOperator() {
        return Operator;
    }
    public void setOperator(String aOperator) {
        Operator = aOperator;
    }
    public String getMakeDate() {
        return MakeDate;
    }
    public void setMakeDate(String aMakeDate) {
        MakeDate = aMakeDate;
    }
    public String getMakeTime() {
        return MakeTime;
    }
    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }
    public String getModifyDate() {
        return ModifyDate;
    }
    public void setModifyDate(String aModifyDate) {
        ModifyDate = aModifyDate;
    }
    public String getModifyTime() {
        return ModifyTime;
    }
    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }
    public String getBankType() {
        return BankType;
    }
    public void setBankType(String aBankType) {
        BankType = aBankType;
    }
    public String getCalFlag() {
        return CalFlag;
    }
    public void setCalFlag(String aCalFlag) {
        CalFlag = aCalFlag;
    }
    public String getBusiLicenseCode() {
        return BusiLicenseCode;
    }
    public void setBusiLicenseCode(String aBusiLicenseCode) {
        BusiLicenseCode = aBusiLicenseCode;
    }
    public String getInsureID() {
        return InsureID;
    }
    public void setInsureID(String aInsureID) {
        InsureID = aInsureID;
    }
    public String getInsurePrincipal() {
        return InsurePrincipal;
    }
    public void setInsurePrincipal(String aInsurePrincipal) {
        InsurePrincipal = aInsurePrincipal;
    }
    public String getChiefBusiness() {
        return ChiefBusiness;
    }
    public void setChiefBusiness(String aChiefBusiness) {
        ChiefBusiness = aChiefBusiness;
    }
    public String getBusiAddress() {
        return BusiAddress;
    }
    public void setBusiAddress(String aBusiAddress) {
        BusiAddress = aBusiAddress;
    }
    public String getSubscribeMan() {
        return SubscribeMan;
    }
    public void setSubscribeMan(String aSubscribeMan) {
        SubscribeMan = aSubscribeMan;
    }
    public String getSubscribeManDuty() {
        return SubscribeManDuty;
    }
    public void setSubscribeManDuty(String aSubscribeManDuty) {
        SubscribeManDuty = aSubscribeManDuty;
    }
    public String getLicenseNo() {
        return LicenseNo;
    }
    public void setLicenseNo(String aLicenseNo) {
        LicenseNo = aLicenseNo;
    }
    public String getRegionalismCode() {
        return RegionalismCode;
    }
    public void setRegionalismCode(String aRegionalismCode) {
        RegionalismCode = aRegionalismCode;
    }
    public String getAppAgentCom() {
        return AppAgentCom;
    }
    public void setAppAgentCom(String aAppAgentCom) {
        AppAgentCom = aAppAgentCom;
    }
    public String getState() {
        return State;
    }
    public void setState(String aState) {
        State = aState;
    }
    public String getNoti() {
        return Noti;
    }
    public void setNoti(String aNoti) {
        Noti = aNoti;
    }
    public String getBusinessCode() {
        return BusinessCode;
    }
    public void setBusinessCode(String aBusinessCode) {
        BusinessCode = aBusinessCode;
    }
    public String getLicenseStartDate() {
        return LicenseStartDate;
    }
    public void setLicenseStartDate(String aLicenseStartDate) {
        LicenseStartDate = aLicenseStartDate;
    }
    public String getLicenseEndDate() {
        return LicenseEndDate;
    }
    public void setLicenseEndDate(String aLicenseEndDate) {
        LicenseEndDate = aLicenseEndDate;
    }
    public String getBranchType() {
        return BranchType;
    }
    public void setBranchType(String aBranchType) {
        BranchType = aBranchType;
    }
    public String getBranchType2() {
        return BranchType2;
    }
    public void setBranchType2(String aBranchType2) {
        BranchType2 = aBranchType2;
    }
    public double getAssets() {
        return Assets;
    }
    public void setAssets(double aAssets) {
        Assets = aAssets;
    }
    public void setAssets(String aAssets) {
        if (aAssets != null && !aAssets.equals("")) {
            Double tDouble = new Double(aAssets);
            double d = tDouble.doubleValue();
            Assets = d;
        }
    }

    public double getIncome() {
        return Income;
    }
    public void setIncome(double aIncome) {
        Income = aIncome;
    }
    public void setIncome(String aIncome) {
        if (aIncome != null && !aIncome.equals("")) {
            Double tDouble = new Double(aIncome);
            double d = tDouble.doubleValue();
            Income = d;
        }
    }

    public double getProfits() {
        return Profits;
    }
    public void setProfits(double aProfits) {
        Profits = aProfits;
    }
    public void setProfits(String aProfits) {
        if (aProfits != null && !aProfits.equals("")) {
            Double tDouble = new Double(aProfits);
            double d = tDouble.doubleValue();
            Profits = d;
        }
    }

    public int getPersonnalSum() {
        return PersonnalSum;
    }
    public void setPersonnalSum(int aPersonnalSum) {
        PersonnalSum = aPersonnalSum;
    }
    public void setPersonnalSum(String aPersonnalSum) {
        if (aPersonnalSum != null && !aPersonnalSum.equals("")) {
            Integer tInteger = new Integer(aPersonnalSum);
            int i = tInteger.intValue();
            PersonnalSum = i;
        }
    }

    public String getProtocalNo() {
        return ProtocalNo;
    }
    public void setProtocalNo(String aProtocalNo) {
        ProtocalNo = aProtocalNo;
    }
    public String getHeadOffice() {
        return HeadOffice;
    }
    public void setHeadOffice(String aHeadOffice) {
        HeadOffice = aHeadOffice;
    }
    public String getFoundDate() {
        return FoundDate;
    }
    public void setFoundDate(String aFoundDate) {
        FoundDate = aFoundDate;
    }
    public String getEndDate() {
        return EndDate;
    }
    public void setEndDate(String aEndDate) {
        EndDate = aEndDate;
    }
    public String getBank() {
        return Bank;
    }
    public void setBank(String aBank) {
        Bank = aBank;
    }
    public String getAccName() {
        return AccName;
    }
    public void setAccName(String aAccName) {
        AccName = aAccName;
    }
    public String getDraWer() {
        return DraWer;
    }
    public void setDraWer(String aDraWer) {
        DraWer = aDraWer;
    }
    public String getDraWerAccNo() {
        return DraWerAccNo;
    }
    public void setDraWerAccNo(String aDraWerAccNo) {
        DraWerAccNo = aDraWerAccNo;
    }
    public String getRepManageCom() {
        return RepManageCom;
    }
    public void setRepManageCom(String aRepManageCom) {
        RepManageCom = aRepManageCom;
    }
    public String getDraWerAccCode() {
        return DraWerAccCode;
    }
    public void setDraWerAccCode(String aDraWerAccCode) {
        DraWerAccCode = aDraWerAccCode;
    }
    public String getDraWerAccName() {
        return DraWerAccName;
    }
    public void setDraWerAccName(String aDraWerAccName) {
        DraWerAccName = aDraWerAccName;
    }
    public String getChannelType2() {
        return ChannelType2;
    }
    public void setChannelType2(String aChannelType2) {
        ChannelType2 = aChannelType2;
    }
    public String getAreaType2() {
        return AreaType2;
    }
    public void setAreaType2(String aAreaType2) {
        AreaType2 = aAreaType2;
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("AgentCom") ) {
            return 0;
        }
        if( strFieldName.equals("ManageCom") ) {
            return 1;
        }
        if( strFieldName.equals("AreaType") ) {
            return 2;
        }
        if( strFieldName.equals("ChannelType") ) {
            return 3;
        }
        if( strFieldName.equals("UpAgentCom") ) {
            return 4;
        }
        if( strFieldName.equals("Name") ) {
            return 5;
        }
        if( strFieldName.equals("Address") ) {
            return 6;
        }
        if( strFieldName.equals("ZipCode") ) {
            return 7;
        }
        if( strFieldName.equals("Phone") ) {
            return 8;
        }
        if( strFieldName.equals("Fax") ) {
            return 9;
        }
        if( strFieldName.equals("EMail") ) {
            return 10;
        }
        if( strFieldName.equals("WebAddress") ) {
            return 11;
        }
        if( strFieldName.equals("LinkMan") ) {
            return 12;
        }
        if( strFieldName.equals("Password") ) {
            return 13;
        }
        if( strFieldName.equals("Corporation") ) {
            return 14;
        }
        if( strFieldName.equals("BankCode") ) {
            return 15;
        }
        if( strFieldName.equals("BankAccNo") ) {
            return 16;
        }
        if( strFieldName.equals("BusinessType") ) {
            return 17;
        }
        if( strFieldName.equals("GrpNature") ) {
            return 18;
        }
        if( strFieldName.equals("ACType") ) {
            return 19;
        }
        if( strFieldName.equals("SellFlag") ) {
            return 20;
        }
        if( strFieldName.equals("Operator") ) {
            return 21;
        }
        if( strFieldName.equals("MakeDate") ) {
            return 22;
        }
        if( strFieldName.equals("MakeTime") ) {
            return 23;
        }
        if( strFieldName.equals("ModifyDate") ) {
            return 24;
        }
        if( strFieldName.equals("ModifyTime") ) {
            return 25;
        }
        if( strFieldName.equals("BankType") ) {
            return 26;
        }
        if( strFieldName.equals("CalFlag") ) {
            return 27;
        }
        if( strFieldName.equals("BusiLicenseCode") ) {
            return 28;
        }
        if( strFieldName.equals("InsureID") ) {
            return 29;
        }
        if( strFieldName.equals("InsurePrincipal") ) {
            return 30;
        }
        if( strFieldName.equals("ChiefBusiness") ) {
            return 31;
        }
        if( strFieldName.equals("BusiAddress") ) {
            return 32;
        }
        if( strFieldName.equals("SubscribeMan") ) {
            return 33;
        }
        if( strFieldName.equals("SubscribeManDuty") ) {
            return 34;
        }
        if( strFieldName.equals("LicenseNo") ) {
            return 35;
        }
        if( strFieldName.equals("RegionalismCode") ) {
            return 36;
        }
        if( strFieldName.equals("AppAgentCom") ) {
            return 37;
        }
        if( strFieldName.equals("State") ) {
            return 38;
        }
        if( strFieldName.equals("Noti") ) {
            return 39;
        }
        if( strFieldName.equals("BusinessCode") ) {
            return 40;
        }
        if( strFieldName.equals("LicenseStartDate") ) {
            return 41;
        }
        if( strFieldName.equals("LicenseEndDate") ) {
            return 42;
        }
        if( strFieldName.equals("BranchType") ) {
            return 43;
        }
        if( strFieldName.equals("BranchType2") ) {
            return 44;
        }
        if( strFieldName.equals("Assets") ) {
            return 45;
        }
        if( strFieldName.equals("Income") ) {
            return 46;
        }
        if( strFieldName.equals("Profits") ) {
            return 47;
        }
        if( strFieldName.equals("PersonnalSum") ) {
            return 48;
        }
        if( strFieldName.equals("ProtocalNo") ) {
            return 49;
        }
        if( strFieldName.equals("HeadOffice") ) {
            return 50;
        }
        if( strFieldName.equals("FoundDate") ) {
            return 51;
        }
        if( strFieldName.equals("EndDate") ) {
            return 52;
        }
        if( strFieldName.equals("Bank") ) {
            return 53;
        }
        if( strFieldName.equals("AccName") ) {
            return 54;
        }
        if( strFieldName.equals("DraWer") ) {
            return 55;
        }
        if( strFieldName.equals("DraWerAccNo") ) {
            return 56;
        }
        if( strFieldName.equals("RepManageCom") ) {
            return 57;
        }
        if( strFieldName.equals("DraWerAccCode") ) {
            return 58;
        }
        if( strFieldName.equals("DraWerAccName") ) {
            return 59;
        }
        if( strFieldName.equals("ChannelType2") ) {
            return 60;
        }
        if( strFieldName.equals("AreaType2") ) {
            return 61;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "AgentCom";
                break;
            case 1:
                strFieldName = "ManageCom";
                break;
            case 2:
                strFieldName = "AreaType";
                break;
            case 3:
                strFieldName = "ChannelType";
                break;
            case 4:
                strFieldName = "UpAgentCom";
                break;
            case 5:
                strFieldName = "Name";
                break;
            case 6:
                strFieldName = "Address";
                break;
            case 7:
                strFieldName = "ZipCode";
                break;
            case 8:
                strFieldName = "Phone";
                break;
            case 9:
                strFieldName = "Fax";
                break;
            case 10:
                strFieldName = "EMail";
                break;
            case 11:
                strFieldName = "WebAddress";
                break;
            case 12:
                strFieldName = "LinkMan";
                break;
            case 13:
                strFieldName = "Password";
                break;
            case 14:
                strFieldName = "Corporation";
                break;
            case 15:
                strFieldName = "BankCode";
                break;
            case 16:
                strFieldName = "BankAccNo";
                break;
            case 17:
                strFieldName = "BusinessType";
                break;
            case 18:
                strFieldName = "GrpNature";
                break;
            case 19:
                strFieldName = "ACType";
                break;
            case 20:
                strFieldName = "SellFlag";
                break;
            case 21:
                strFieldName = "Operator";
                break;
            case 22:
                strFieldName = "MakeDate";
                break;
            case 23:
                strFieldName = "MakeTime";
                break;
            case 24:
                strFieldName = "ModifyDate";
                break;
            case 25:
                strFieldName = "ModifyTime";
                break;
            case 26:
                strFieldName = "BankType";
                break;
            case 27:
                strFieldName = "CalFlag";
                break;
            case 28:
                strFieldName = "BusiLicenseCode";
                break;
            case 29:
                strFieldName = "InsureID";
                break;
            case 30:
                strFieldName = "InsurePrincipal";
                break;
            case 31:
                strFieldName = "ChiefBusiness";
                break;
            case 32:
                strFieldName = "BusiAddress";
                break;
            case 33:
                strFieldName = "SubscribeMan";
                break;
            case 34:
                strFieldName = "SubscribeManDuty";
                break;
            case 35:
                strFieldName = "LicenseNo";
                break;
            case 36:
                strFieldName = "RegionalismCode";
                break;
            case 37:
                strFieldName = "AppAgentCom";
                break;
            case 38:
                strFieldName = "State";
                break;
            case 39:
                strFieldName = "Noti";
                break;
            case 40:
                strFieldName = "BusinessCode";
                break;
            case 41:
                strFieldName = "LicenseStartDate";
                break;
            case 42:
                strFieldName = "LicenseEndDate";
                break;
            case 43:
                strFieldName = "BranchType";
                break;
            case 44:
                strFieldName = "BranchType2";
                break;
            case 45:
                strFieldName = "Assets";
                break;
            case 46:
                strFieldName = "Income";
                break;
            case 47:
                strFieldName = "Profits";
                break;
            case 48:
                strFieldName = "PersonnalSum";
                break;
            case 49:
                strFieldName = "ProtocalNo";
                break;
            case 50:
                strFieldName = "HeadOffice";
                break;
            case 51:
                strFieldName = "FoundDate";
                break;
            case 52:
                strFieldName = "EndDate";
                break;
            case 53:
                strFieldName = "Bank";
                break;
            case 54:
                strFieldName = "AccName";
                break;
            case 55:
                strFieldName = "DraWer";
                break;
            case 56:
                strFieldName = "DraWerAccNo";
                break;
            case 57:
                strFieldName = "RepManageCom";
                break;
            case 58:
                strFieldName = "DraWerAccCode";
                break;
            case 59:
                strFieldName = "DraWerAccName";
                break;
            case 60:
                strFieldName = "ChannelType2";
                break;
            case 61:
                strFieldName = "AreaType2";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "AGENTCOM":
                return Schema.TYPE_STRING;
            case "MANAGECOM":
                return Schema.TYPE_STRING;
            case "AREATYPE":
                return Schema.TYPE_STRING;
            case "CHANNELTYPE":
                return Schema.TYPE_STRING;
            case "UPAGENTCOM":
                return Schema.TYPE_STRING;
            case "NAME":
                return Schema.TYPE_STRING;
            case "ADDRESS":
                return Schema.TYPE_STRING;
            case "ZIPCODE":
                return Schema.TYPE_STRING;
            case "PHONE":
                return Schema.TYPE_STRING;
            case "FAX":
                return Schema.TYPE_STRING;
            case "EMAIL":
                return Schema.TYPE_STRING;
            case "WEBADDRESS":
                return Schema.TYPE_STRING;
            case "LINKMAN":
                return Schema.TYPE_STRING;
            case "PASSWORD":
                return Schema.TYPE_STRING;
            case "CORPORATION":
                return Schema.TYPE_STRING;
            case "BANKCODE":
                return Schema.TYPE_STRING;
            case "BANKACCNO":
                return Schema.TYPE_STRING;
            case "BUSINESSTYPE":
                return Schema.TYPE_STRING;
            case "GRPNATURE":
                return Schema.TYPE_STRING;
            case "ACTYPE":
                return Schema.TYPE_STRING;
            case "SELLFLAG":
                return Schema.TYPE_STRING;
            case "OPERATOR":
                return Schema.TYPE_STRING;
            case "MAKEDATE":
                return Schema.TYPE_STRING;
            case "MAKETIME":
                return Schema.TYPE_STRING;
            case "MODIFYDATE":
                return Schema.TYPE_STRING;
            case "MODIFYTIME":
                return Schema.TYPE_STRING;
            case "BANKTYPE":
                return Schema.TYPE_STRING;
            case "CALFLAG":
                return Schema.TYPE_STRING;
            case "BUSILICENSECODE":
                return Schema.TYPE_STRING;
            case "INSUREID":
                return Schema.TYPE_STRING;
            case "INSUREPRINCIPAL":
                return Schema.TYPE_STRING;
            case "CHIEFBUSINESS":
                return Schema.TYPE_STRING;
            case "BUSIADDRESS":
                return Schema.TYPE_STRING;
            case "SUBSCRIBEMAN":
                return Schema.TYPE_STRING;
            case "SUBSCRIBEMANDUTY":
                return Schema.TYPE_STRING;
            case "LICENSENO":
                return Schema.TYPE_STRING;
            case "REGIONALISMCODE":
                return Schema.TYPE_STRING;
            case "APPAGENTCOM":
                return Schema.TYPE_STRING;
            case "STATE":
                return Schema.TYPE_STRING;
            case "NOTI":
                return Schema.TYPE_STRING;
            case "BUSINESSCODE":
                return Schema.TYPE_STRING;
            case "LICENSESTARTDATE":
                return Schema.TYPE_STRING;
            case "LICENSEENDDATE":
                return Schema.TYPE_STRING;
            case "BRANCHTYPE":
                return Schema.TYPE_STRING;
            case "BRANCHTYPE2":
                return Schema.TYPE_STRING;
            case "ASSETS":
                return Schema.TYPE_DOUBLE;
            case "INCOME":
                return Schema.TYPE_DOUBLE;
            case "PROFITS":
                return Schema.TYPE_DOUBLE;
            case "PERSONNALSUM":
                return Schema.TYPE_INT;
            case "PROTOCALNO":
                return Schema.TYPE_STRING;
            case "HEADOFFICE":
                return Schema.TYPE_STRING;
            case "FOUNDDATE":
                return Schema.TYPE_STRING;
            case "ENDDATE":
                return Schema.TYPE_STRING;
            case "BANK":
                return Schema.TYPE_STRING;
            case "ACCNAME":
                return Schema.TYPE_STRING;
            case "DRAWER":
                return Schema.TYPE_STRING;
            case "DRAWERACCNO":
                return Schema.TYPE_STRING;
            case "REPMANAGECOM":
                return Schema.TYPE_STRING;
            case "DRAWERACCCODE":
                return Schema.TYPE_STRING;
            case "DRAWERACCNAME":
                return Schema.TYPE_STRING;
            case "CHANNELTYPE2":
                return Schema.TYPE_STRING;
            case "AREATYPE2":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_STRING;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_STRING;
            case 6:
                return Schema.TYPE_STRING;
            case 7:
                return Schema.TYPE_STRING;
            case 8:
                return Schema.TYPE_STRING;
            case 9:
                return Schema.TYPE_STRING;
            case 10:
                return Schema.TYPE_STRING;
            case 11:
                return Schema.TYPE_STRING;
            case 12:
                return Schema.TYPE_STRING;
            case 13:
                return Schema.TYPE_STRING;
            case 14:
                return Schema.TYPE_STRING;
            case 15:
                return Schema.TYPE_STRING;
            case 16:
                return Schema.TYPE_STRING;
            case 17:
                return Schema.TYPE_STRING;
            case 18:
                return Schema.TYPE_STRING;
            case 19:
                return Schema.TYPE_STRING;
            case 20:
                return Schema.TYPE_STRING;
            case 21:
                return Schema.TYPE_STRING;
            case 22:
                return Schema.TYPE_STRING;
            case 23:
                return Schema.TYPE_STRING;
            case 24:
                return Schema.TYPE_STRING;
            case 25:
                return Schema.TYPE_STRING;
            case 26:
                return Schema.TYPE_STRING;
            case 27:
                return Schema.TYPE_STRING;
            case 28:
                return Schema.TYPE_STRING;
            case 29:
                return Schema.TYPE_STRING;
            case 30:
                return Schema.TYPE_STRING;
            case 31:
                return Schema.TYPE_STRING;
            case 32:
                return Schema.TYPE_STRING;
            case 33:
                return Schema.TYPE_STRING;
            case 34:
                return Schema.TYPE_STRING;
            case 35:
                return Schema.TYPE_STRING;
            case 36:
                return Schema.TYPE_STRING;
            case 37:
                return Schema.TYPE_STRING;
            case 38:
                return Schema.TYPE_STRING;
            case 39:
                return Schema.TYPE_STRING;
            case 40:
                return Schema.TYPE_STRING;
            case 41:
                return Schema.TYPE_STRING;
            case 42:
                return Schema.TYPE_STRING;
            case 43:
                return Schema.TYPE_STRING;
            case 44:
                return Schema.TYPE_STRING;
            case 45:
                return Schema.TYPE_DOUBLE;
            case 46:
                return Schema.TYPE_DOUBLE;
            case 47:
                return Schema.TYPE_DOUBLE;
            case 48:
                return Schema.TYPE_INT;
            case 49:
                return Schema.TYPE_STRING;
            case 50:
                return Schema.TYPE_STRING;
            case 51:
                return Schema.TYPE_STRING;
            case 52:
                return Schema.TYPE_STRING;
            case 53:
                return Schema.TYPE_STRING;
            case 54:
                return Schema.TYPE_STRING;
            case 55:
                return Schema.TYPE_STRING;
            case 56:
                return Schema.TYPE_STRING;
            case 57:
                return Schema.TYPE_STRING;
            case 58:
                return Schema.TYPE_STRING;
            case 59:
                return Schema.TYPE_STRING;
            case 60:
                return Schema.TYPE_STRING;
            case 61:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("AgentCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentCom));
        }
        if (FCode.equalsIgnoreCase("ManageCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
        }
        if (FCode.equalsIgnoreCase("AreaType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AreaType));
        }
        if (FCode.equalsIgnoreCase("ChannelType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ChannelType));
        }
        if (FCode.equalsIgnoreCase("UpAgentCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(UpAgentCom));
        }
        if (FCode.equalsIgnoreCase("Name")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Name));
        }
        if (FCode.equalsIgnoreCase("Address")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Address));
        }
        if (FCode.equalsIgnoreCase("ZipCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ZipCode));
        }
        if (FCode.equalsIgnoreCase("Phone")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Phone));
        }
        if (FCode.equalsIgnoreCase("Fax")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Fax));
        }
        if (FCode.equalsIgnoreCase("EMail")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(EMail));
        }
        if (FCode.equalsIgnoreCase("WebAddress")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(WebAddress));
        }
        if (FCode.equalsIgnoreCase("LinkMan")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(LinkMan));
        }
        if (FCode.equalsIgnoreCase("Password")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Password));
        }
        if (FCode.equalsIgnoreCase("Corporation")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Corporation));
        }
        if (FCode.equalsIgnoreCase("BankCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BankCode));
        }
        if (FCode.equalsIgnoreCase("BankAccNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BankAccNo));
        }
        if (FCode.equalsIgnoreCase("BusinessType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BusinessType));
        }
        if (FCode.equalsIgnoreCase("GrpNature")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GrpNature));
        }
        if (FCode.equalsIgnoreCase("ACType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ACType));
        }
        if (FCode.equalsIgnoreCase("SellFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SellFlag));
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeDate));
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyDate));
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (FCode.equalsIgnoreCase("BankType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BankType));
        }
        if (FCode.equalsIgnoreCase("CalFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CalFlag));
        }
        if (FCode.equalsIgnoreCase("BusiLicenseCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BusiLicenseCode));
        }
        if (FCode.equalsIgnoreCase("InsureID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InsureID));
        }
        if (FCode.equalsIgnoreCase("InsurePrincipal")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InsurePrincipal));
        }
        if (FCode.equalsIgnoreCase("ChiefBusiness")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ChiefBusiness));
        }
        if (FCode.equalsIgnoreCase("BusiAddress")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BusiAddress));
        }
        if (FCode.equalsIgnoreCase("SubscribeMan")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SubscribeMan));
        }
        if (FCode.equalsIgnoreCase("SubscribeManDuty")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SubscribeManDuty));
        }
        if (FCode.equalsIgnoreCase("LicenseNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(LicenseNo));
        }
        if (FCode.equalsIgnoreCase("RegionalismCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RegionalismCode));
        }
        if (FCode.equalsIgnoreCase("AppAgentCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AppAgentCom));
        }
        if (FCode.equalsIgnoreCase("State")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(State));
        }
        if (FCode.equalsIgnoreCase("Noti")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Noti));
        }
        if (FCode.equalsIgnoreCase("BusinessCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BusinessCode));
        }
        if (FCode.equalsIgnoreCase("LicenseStartDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(LicenseStartDate));
        }
        if (FCode.equalsIgnoreCase("LicenseEndDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(LicenseEndDate));
        }
        if (FCode.equalsIgnoreCase("BranchType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BranchType));
        }
        if (FCode.equalsIgnoreCase("BranchType2")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BranchType2));
        }
        if (FCode.equalsIgnoreCase("Assets")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Assets));
        }
        if (FCode.equalsIgnoreCase("Income")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Income));
        }
        if (FCode.equalsIgnoreCase("Profits")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Profits));
        }
        if (FCode.equalsIgnoreCase("PersonnalSum")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PersonnalSum));
        }
        if (FCode.equalsIgnoreCase("ProtocalNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ProtocalNo));
        }
        if (FCode.equalsIgnoreCase("HeadOffice")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(HeadOffice));
        }
        if (FCode.equalsIgnoreCase("FoundDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FoundDate));
        }
        if (FCode.equalsIgnoreCase("EndDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(EndDate));
        }
        if (FCode.equalsIgnoreCase("Bank")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Bank));
        }
        if (FCode.equalsIgnoreCase("AccName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AccName));
        }
        if (FCode.equalsIgnoreCase("DraWer")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DraWer));
        }
        if (FCode.equalsIgnoreCase("DraWerAccNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DraWerAccNo));
        }
        if (FCode.equalsIgnoreCase("RepManageCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RepManageCom));
        }
        if (FCode.equalsIgnoreCase("DraWerAccCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DraWerAccCode));
        }
        if (FCode.equalsIgnoreCase("DraWerAccName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DraWerAccName));
        }
        if (FCode.equalsIgnoreCase("ChannelType2")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ChannelType2));
        }
        if (FCode.equalsIgnoreCase("AreaType2")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AreaType2));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = String.valueOf(AgentCom);
                break;
            case 1:
                strFieldValue = String.valueOf(ManageCom);
                break;
            case 2:
                strFieldValue = String.valueOf(AreaType);
                break;
            case 3:
                strFieldValue = String.valueOf(ChannelType);
                break;
            case 4:
                strFieldValue = String.valueOf(UpAgentCom);
                break;
            case 5:
                strFieldValue = String.valueOf(Name);
                break;
            case 6:
                strFieldValue = String.valueOf(Address);
                break;
            case 7:
                strFieldValue = String.valueOf(ZipCode);
                break;
            case 8:
                strFieldValue = String.valueOf(Phone);
                break;
            case 9:
                strFieldValue = String.valueOf(Fax);
                break;
            case 10:
                strFieldValue = String.valueOf(EMail);
                break;
            case 11:
                strFieldValue = String.valueOf(WebAddress);
                break;
            case 12:
                strFieldValue = String.valueOf(LinkMan);
                break;
            case 13:
                strFieldValue = String.valueOf(Password);
                break;
            case 14:
                strFieldValue = String.valueOf(Corporation);
                break;
            case 15:
                strFieldValue = String.valueOf(BankCode);
                break;
            case 16:
                strFieldValue = String.valueOf(BankAccNo);
                break;
            case 17:
                strFieldValue = String.valueOf(BusinessType);
                break;
            case 18:
                strFieldValue = String.valueOf(GrpNature);
                break;
            case 19:
                strFieldValue = String.valueOf(ACType);
                break;
            case 20:
                strFieldValue = String.valueOf(SellFlag);
                break;
            case 21:
                strFieldValue = String.valueOf(Operator);
                break;
            case 22:
                strFieldValue = String.valueOf(MakeDate);
                break;
            case 23:
                strFieldValue = String.valueOf(MakeTime);
                break;
            case 24:
                strFieldValue = String.valueOf(ModifyDate);
                break;
            case 25:
                strFieldValue = String.valueOf(ModifyTime);
                break;
            case 26:
                strFieldValue = String.valueOf(BankType);
                break;
            case 27:
                strFieldValue = String.valueOf(CalFlag);
                break;
            case 28:
                strFieldValue = String.valueOf(BusiLicenseCode);
                break;
            case 29:
                strFieldValue = String.valueOf(InsureID);
                break;
            case 30:
                strFieldValue = String.valueOf(InsurePrincipal);
                break;
            case 31:
                strFieldValue = String.valueOf(ChiefBusiness);
                break;
            case 32:
                strFieldValue = String.valueOf(BusiAddress);
                break;
            case 33:
                strFieldValue = String.valueOf(SubscribeMan);
                break;
            case 34:
                strFieldValue = String.valueOf(SubscribeManDuty);
                break;
            case 35:
                strFieldValue = String.valueOf(LicenseNo);
                break;
            case 36:
                strFieldValue = String.valueOf(RegionalismCode);
                break;
            case 37:
                strFieldValue = String.valueOf(AppAgentCom);
                break;
            case 38:
                strFieldValue = String.valueOf(State);
                break;
            case 39:
                strFieldValue = String.valueOf(Noti);
                break;
            case 40:
                strFieldValue = String.valueOf(BusinessCode);
                break;
            case 41:
                strFieldValue = String.valueOf(LicenseStartDate);
                break;
            case 42:
                strFieldValue = String.valueOf(LicenseEndDate);
                break;
            case 43:
                strFieldValue = String.valueOf(BranchType);
                break;
            case 44:
                strFieldValue = String.valueOf(BranchType2);
                break;
            case 45:
                strFieldValue = String.valueOf(Assets);
                break;
            case 46:
                strFieldValue = String.valueOf(Income);
                break;
            case 47:
                strFieldValue = String.valueOf(Profits);
                break;
            case 48:
                strFieldValue = String.valueOf(PersonnalSum);
                break;
            case 49:
                strFieldValue = String.valueOf(ProtocalNo);
                break;
            case 50:
                strFieldValue = String.valueOf(HeadOffice);
                break;
            case 51:
                strFieldValue = String.valueOf(FoundDate);
                break;
            case 52:
                strFieldValue = String.valueOf(EndDate);
                break;
            case 53:
                strFieldValue = String.valueOf(Bank);
                break;
            case 54:
                strFieldValue = String.valueOf(AccName);
                break;
            case 55:
                strFieldValue = String.valueOf(DraWer);
                break;
            case 56:
                strFieldValue = String.valueOf(DraWerAccNo);
                break;
            case 57:
                strFieldValue = String.valueOf(RepManageCom);
                break;
            case 58:
                strFieldValue = String.valueOf(DraWerAccCode);
                break;
            case 59:
                strFieldValue = String.valueOf(DraWerAccName);
                break;
            case 60:
                strFieldValue = String.valueOf(ChannelType2);
                break;
            case 61:
                strFieldValue = String.valueOf(AreaType2);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("AgentCom")) {
            if( FValue != null && !FValue.equals(""))
            {
                AgentCom = FValue.trim();
            }
            else
                AgentCom = null;
        }
        if (FCode.equalsIgnoreCase("ManageCom")) {
            if( FValue != null && !FValue.equals(""))
            {
                ManageCom = FValue.trim();
            }
            else
                ManageCom = null;
        }
        if (FCode.equalsIgnoreCase("AreaType")) {
            if( FValue != null && !FValue.equals(""))
            {
                AreaType = FValue.trim();
            }
            else
                AreaType = null;
        }
        if (FCode.equalsIgnoreCase("ChannelType")) {
            if( FValue != null && !FValue.equals(""))
            {
                ChannelType = FValue.trim();
            }
            else
                ChannelType = null;
        }
        if (FCode.equalsIgnoreCase("UpAgentCom")) {
            if( FValue != null && !FValue.equals(""))
            {
                UpAgentCom = FValue.trim();
            }
            else
                UpAgentCom = null;
        }
        if (FCode.equalsIgnoreCase("Name")) {
            if( FValue != null && !FValue.equals(""))
            {
                Name = FValue.trim();
            }
            else
                Name = null;
        }
        if (FCode.equalsIgnoreCase("Address")) {
            if( FValue != null && !FValue.equals(""))
            {
                Address = FValue.trim();
            }
            else
                Address = null;
        }
        if (FCode.equalsIgnoreCase("ZipCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                ZipCode = FValue.trim();
            }
            else
                ZipCode = null;
        }
        if (FCode.equalsIgnoreCase("Phone")) {
            if( FValue != null && !FValue.equals(""))
            {
                Phone = FValue.trim();
            }
            else
                Phone = null;
        }
        if (FCode.equalsIgnoreCase("Fax")) {
            if( FValue != null && !FValue.equals(""))
            {
                Fax = FValue.trim();
            }
            else
                Fax = null;
        }
        if (FCode.equalsIgnoreCase("EMail")) {
            if( FValue != null && !FValue.equals(""))
            {
                EMail = FValue.trim();
            }
            else
                EMail = null;
        }
        if (FCode.equalsIgnoreCase("WebAddress")) {
            if( FValue != null && !FValue.equals(""))
            {
                WebAddress = FValue.trim();
            }
            else
                WebAddress = null;
        }
        if (FCode.equalsIgnoreCase("LinkMan")) {
            if( FValue != null && !FValue.equals(""))
            {
                LinkMan = FValue.trim();
            }
            else
                LinkMan = null;
        }
        if (FCode.equalsIgnoreCase("Password")) {
            if( FValue != null && !FValue.equals(""))
            {
                Password = FValue.trim();
            }
            else
                Password = null;
        }
        if (FCode.equalsIgnoreCase("Corporation")) {
            if( FValue != null && !FValue.equals(""))
            {
                Corporation = FValue.trim();
            }
            else
                Corporation = null;
        }
        if (FCode.equalsIgnoreCase("BankCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                BankCode = FValue.trim();
            }
            else
                BankCode = null;
        }
        if (FCode.equalsIgnoreCase("BankAccNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                BankAccNo = FValue.trim();
            }
            else
                BankAccNo = null;
        }
        if (FCode.equalsIgnoreCase("BusinessType")) {
            if( FValue != null && !FValue.equals(""))
            {
                BusinessType = FValue.trim();
            }
            else
                BusinessType = null;
        }
        if (FCode.equalsIgnoreCase("GrpNature")) {
            if( FValue != null && !FValue.equals(""))
            {
                GrpNature = FValue.trim();
            }
            else
                GrpNature = null;
        }
        if (FCode.equalsIgnoreCase("ACType")) {
            if( FValue != null && !FValue.equals(""))
            {
                ACType = FValue.trim();
            }
            else
                ACType = null;
        }
        if (FCode.equalsIgnoreCase("SellFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                SellFlag = FValue.trim();
            }
            else
                SellFlag = null;
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            if( FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
                Operator = null;
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeDate = FValue.trim();
            }
            else
                MakeDate = null;
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
                MakeTime = null;
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyDate = FValue.trim();
            }
            else
                ModifyDate = null;
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
                ModifyTime = null;
        }
        if (FCode.equalsIgnoreCase("BankType")) {
            if( FValue != null && !FValue.equals(""))
            {
                BankType = FValue.trim();
            }
            else
                BankType = null;
        }
        if (FCode.equalsIgnoreCase("CalFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                CalFlag = FValue.trim();
            }
            else
                CalFlag = null;
        }
        if (FCode.equalsIgnoreCase("BusiLicenseCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                BusiLicenseCode = FValue.trim();
            }
            else
                BusiLicenseCode = null;
        }
        if (FCode.equalsIgnoreCase("InsureID")) {
            if( FValue != null && !FValue.equals(""))
            {
                InsureID = FValue.trim();
            }
            else
                InsureID = null;
        }
        if (FCode.equalsIgnoreCase("InsurePrincipal")) {
            if( FValue != null && !FValue.equals(""))
            {
                InsurePrincipal = FValue.trim();
            }
            else
                InsurePrincipal = null;
        }
        if (FCode.equalsIgnoreCase("ChiefBusiness")) {
            if( FValue != null && !FValue.equals(""))
            {
                ChiefBusiness = FValue.trim();
            }
            else
                ChiefBusiness = null;
        }
        if (FCode.equalsIgnoreCase("BusiAddress")) {
            if( FValue != null && !FValue.equals(""))
            {
                BusiAddress = FValue.trim();
            }
            else
                BusiAddress = null;
        }
        if (FCode.equalsIgnoreCase("SubscribeMan")) {
            if( FValue != null && !FValue.equals(""))
            {
                SubscribeMan = FValue.trim();
            }
            else
                SubscribeMan = null;
        }
        if (FCode.equalsIgnoreCase("SubscribeManDuty")) {
            if( FValue != null && !FValue.equals(""))
            {
                SubscribeManDuty = FValue.trim();
            }
            else
                SubscribeManDuty = null;
        }
        if (FCode.equalsIgnoreCase("LicenseNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                LicenseNo = FValue.trim();
            }
            else
                LicenseNo = null;
        }
        if (FCode.equalsIgnoreCase("RegionalismCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                RegionalismCode = FValue.trim();
            }
            else
                RegionalismCode = null;
        }
        if (FCode.equalsIgnoreCase("AppAgentCom")) {
            if( FValue != null && !FValue.equals(""))
            {
                AppAgentCom = FValue.trim();
            }
            else
                AppAgentCom = null;
        }
        if (FCode.equalsIgnoreCase("State")) {
            if( FValue != null && !FValue.equals(""))
            {
                State = FValue.trim();
            }
            else
                State = null;
        }
        if (FCode.equalsIgnoreCase("Noti")) {
            if( FValue != null && !FValue.equals(""))
            {
                Noti = FValue.trim();
            }
            else
                Noti = null;
        }
        if (FCode.equalsIgnoreCase("BusinessCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                BusinessCode = FValue.trim();
            }
            else
                BusinessCode = null;
        }
        if (FCode.equalsIgnoreCase("LicenseStartDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                LicenseStartDate = FValue.trim();
            }
            else
                LicenseStartDate = null;
        }
        if (FCode.equalsIgnoreCase("LicenseEndDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                LicenseEndDate = FValue.trim();
            }
            else
                LicenseEndDate = null;
        }
        if (FCode.equalsIgnoreCase("BranchType")) {
            if( FValue != null && !FValue.equals(""))
            {
                BranchType = FValue.trim();
            }
            else
                BranchType = null;
        }
        if (FCode.equalsIgnoreCase("BranchType2")) {
            if( FValue != null && !FValue.equals(""))
            {
                BranchType2 = FValue.trim();
            }
            else
                BranchType2 = null;
        }
        if (FCode.equalsIgnoreCase("Assets")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                Assets = d;
            }
        }
        if (FCode.equalsIgnoreCase("Income")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                Income = d;
            }
        }
        if (FCode.equalsIgnoreCase("Profits")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                Profits = d;
            }
        }
        if (FCode.equalsIgnoreCase("PersonnalSum")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                PersonnalSum = i;
            }
        }
        if (FCode.equalsIgnoreCase("ProtocalNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                ProtocalNo = FValue.trim();
            }
            else
                ProtocalNo = null;
        }
        if (FCode.equalsIgnoreCase("HeadOffice")) {
            if( FValue != null && !FValue.equals(""))
            {
                HeadOffice = FValue.trim();
            }
            else
                HeadOffice = null;
        }
        if (FCode.equalsIgnoreCase("FoundDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                FoundDate = FValue.trim();
            }
            else
                FoundDate = null;
        }
        if (FCode.equalsIgnoreCase("EndDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                EndDate = FValue.trim();
            }
            else
                EndDate = null;
        }
        if (FCode.equalsIgnoreCase("Bank")) {
            if( FValue != null && !FValue.equals(""))
            {
                Bank = FValue.trim();
            }
            else
                Bank = null;
        }
        if (FCode.equalsIgnoreCase("AccName")) {
            if( FValue != null && !FValue.equals(""))
            {
                AccName = FValue.trim();
            }
            else
                AccName = null;
        }
        if (FCode.equalsIgnoreCase("DraWer")) {
            if( FValue != null && !FValue.equals(""))
            {
                DraWer = FValue.trim();
            }
            else
                DraWer = null;
        }
        if (FCode.equalsIgnoreCase("DraWerAccNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                DraWerAccNo = FValue.trim();
            }
            else
                DraWerAccNo = null;
        }
        if (FCode.equalsIgnoreCase("RepManageCom")) {
            if( FValue != null && !FValue.equals(""))
            {
                RepManageCom = FValue.trim();
            }
            else
                RepManageCom = null;
        }
        if (FCode.equalsIgnoreCase("DraWerAccCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                DraWerAccCode = FValue.trim();
            }
            else
                DraWerAccCode = null;
        }
        if (FCode.equalsIgnoreCase("DraWerAccName")) {
            if( FValue != null && !FValue.equals(""))
            {
                DraWerAccName = FValue.trim();
            }
            else
                DraWerAccName = null;
        }
        if (FCode.equalsIgnoreCase("ChannelType2")) {
            if( FValue != null && !FValue.equals(""))
            {
                ChannelType2 = FValue.trim();
            }
            else
                ChannelType2 = null;
        }
        if (FCode.equalsIgnoreCase("AreaType2")) {
            if( FValue != null && !FValue.equals(""))
            {
                AreaType2 = FValue.trim();
            }
            else
                AreaType2 = null;
        }
        return true;
    }


    public String toString() {
    return "LAComPojo [" +
            "AgentCom="+AgentCom +
            ", ManageCom="+ManageCom +
            ", AreaType="+AreaType +
            ", ChannelType="+ChannelType +
            ", UpAgentCom="+UpAgentCom +
            ", Name="+Name +
            ", Address="+Address +
            ", ZipCode="+ZipCode +
            ", Phone="+Phone +
            ", Fax="+Fax +
            ", EMail="+EMail +
            ", WebAddress="+WebAddress +
            ", LinkMan="+LinkMan +
            ", Password="+Password +
            ", Corporation="+Corporation +
            ", BankCode="+BankCode +
            ", BankAccNo="+BankAccNo +
            ", BusinessType="+BusinessType +
            ", GrpNature="+GrpNature +
            ", ACType="+ACType +
            ", SellFlag="+SellFlag +
            ", Operator="+Operator +
            ", MakeDate="+MakeDate +
            ", MakeTime="+MakeTime +
            ", ModifyDate="+ModifyDate +
            ", ModifyTime="+ModifyTime +
            ", BankType="+BankType +
            ", CalFlag="+CalFlag +
            ", BusiLicenseCode="+BusiLicenseCode +
            ", InsureID="+InsureID +
            ", InsurePrincipal="+InsurePrincipal +
            ", ChiefBusiness="+ChiefBusiness +
            ", BusiAddress="+BusiAddress +
            ", SubscribeMan="+SubscribeMan +
            ", SubscribeManDuty="+SubscribeManDuty +
            ", LicenseNo="+LicenseNo +
            ", RegionalismCode="+RegionalismCode +
            ", AppAgentCom="+AppAgentCom +
            ", State="+State +
            ", Noti="+Noti +
            ", BusinessCode="+BusinessCode +
            ", LicenseStartDate="+LicenseStartDate +
            ", LicenseEndDate="+LicenseEndDate +
            ", BranchType="+BranchType +
            ", BranchType2="+BranchType2 +
            ", Assets="+Assets +
            ", Income="+Income +
            ", Profits="+Profits +
            ", PersonnalSum="+PersonnalSum +
            ", ProtocalNo="+ProtocalNo +
            ", HeadOffice="+HeadOffice +
            ", FoundDate="+FoundDate +
            ", EndDate="+EndDate +
            ", Bank="+Bank +
            ", AccName="+AccName +
            ", DraWer="+DraWer +
            ", DraWerAccNo="+DraWerAccNo +
            ", RepManageCom="+RepManageCom +
            ", DraWerAccCode="+DraWerAccCode +
            ", DraWerAccName="+DraWerAccName +
            ", ChannelType2="+ChannelType2 +
            ", AreaType2="+AreaType2 +"]";
    }
}
