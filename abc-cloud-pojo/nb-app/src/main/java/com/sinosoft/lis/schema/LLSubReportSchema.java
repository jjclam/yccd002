/**
 * Copyright (c) 2017 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import com.sinosoft.lis.db.LLSubReportDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.*;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

/**
 * <p>ClassName: LLSubReportSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2017</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2017-10-21
 */
public class LLSubReportSchema implements Schema, Cloneable {
    // @Field
    /** 分报案号 */
    private String SubRptNo;
    /** 关联客户号码 */
    private String CustomerNo;
    /** 关联客户的名称 */
    private String CustomerName;
    /** 关联客户类型 */
    private String CustomerType;
    /** 事件主题 */
    private String AccSubject;
    /** 事故类型 */
    private String AccidentType;
    /** 出险日期 */
    private Date AccDate;
    /** 终止日期 */
    private Date AccEndDate;
    /** 事故描述 */
    private String AccDesc;
    /** 事故者状况 */
    private String CustSituation;
    /** 事故地点 */
    private String AccPlace;
    /** 医院代码 */
    private String HospitalCode;
    /** 医院名称 */
    private String HospitalName;
    /** 入院日期 */
    private Date InHospitalDate;
    /** 出院日期 */
    private Date OutHospitalDate;
    /** 备注 */
    private String Remark;
    /** 重大事件标志 */
    private String SeriousGrade;
    /** 调查报告标志 */
    private String SurveyFlag;
    /** 操作员 */
    private String Operator;
    /** 管理机构 */
    private String MngCom;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 最后一次修改日期 */
    private Date ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime;
    /** 首诊日 */
    private Date FirstDiaDate;
    /** 医院级别 */
    private String HosGrade;
    /** 所在地 */
    private String LocalPlace;
    /** 医院联系电话 */
    private String HosTel;
    /** 死亡日期 */
    private Date DieDate;
    /** 事件原因 */
    private String AccCause;
    /** Vip标志 */
    private String VIPFlag;
    /** 出险细节 */
    private String AccidentDetail;
    /** 治疗情况 */
    private String CureDesc;
    /** 发起呈报标志 */
    private String SubmitFlag;
    /** 提起慰问标志 */
    private String CondoleFlag;
    /** 死亡标志 */
    private String DieFlag;
    /** 出险结果1 */
    private String AccResult1;
    /** 出险结果2 */
    private String AccResult2;
    /** 案件号 */
    private String CaseNo;
    /** 客户序号 */
    private int SeqNo;

    public static final int FIELDNUM = 40;    // 数据库表的字段个数

    private static String[] PK;                // 主键

    private FDate fDate = new FDate();        // 处理日期

    public CErrors mErrors;            // 错误信息

    // @Constructor
    public LLSubReportSchema() {
        mErrors = new CErrors();

        String[] pk = new String[2];
        pk[0] = "SubRptNo";
        pk[1] = "CustomerNo";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException {
        LLSubReportSchema cloned = (LLSubReportSchema)super.clone();
        cloned.fDate = (FDate) fDate.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK() {
        return PK;
    }

    public String getSubRptNo() {
        return SubRptNo;
    }
    public void setSubRptNo(String aSubRptNo) {
        SubRptNo = aSubRptNo;
    }
    public String getCustomerNo() {
        return CustomerNo;
    }
    public void setCustomerNo(String aCustomerNo) {
        CustomerNo = aCustomerNo;
    }
    public String getCustomerName() {
        return CustomerName;
    }
    public void setCustomerName(String aCustomerName) {
        CustomerName = aCustomerName;
    }
    public String getCustomerType() {
        return CustomerType;
    }
    public void setCustomerType(String aCustomerType) {
        CustomerType = aCustomerType;
    }
    public String getAccSubject() {
        return AccSubject;
    }
    public void setAccSubject(String aAccSubject) {
        AccSubject = aAccSubject;
    }
    public String getAccidentType() {
        return AccidentType;
    }
    public void setAccidentType(String aAccidentType) {
        AccidentType = aAccidentType;
    }
    public String getAccDate() {
        if(AccDate != null) {
            return fDate.getString(AccDate);
        } else {
            return null;
        }
    }
    public void setAccDate(Date aAccDate) {
        AccDate = aAccDate;
    }
    public void setAccDate(String aAccDate) {
        if (aAccDate != null && !aAccDate.equals("")) {
            AccDate = fDate.getDate(aAccDate);
        } else
            AccDate = null;
    }

    public String getAccEndDate() {
        if(AccEndDate != null) {
            return fDate.getString(AccEndDate);
        } else {
            return null;
        }
    }
    public void setAccEndDate(Date aAccEndDate) {
        AccEndDate = aAccEndDate;
    }
    public void setAccEndDate(String aAccEndDate) {
        if (aAccEndDate != null && !aAccEndDate.equals("")) {
            AccEndDate = fDate.getDate(aAccEndDate);
        } else
            AccEndDate = null;
    }

    public String getAccDesc() {
        return AccDesc;
    }
    public void setAccDesc(String aAccDesc) {
        AccDesc = aAccDesc;
    }
    public String getCustSituation() {
        return CustSituation;
    }
    public void setCustSituation(String aCustSituation) {
        CustSituation = aCustSituation;
    }
    public String getAccPlace() {
        return AccPlace;
    }
    public void setAccPlace(String aAccPlace) {
        AccPlace = aAccPlace;
    }
    public String getHospitalCode() {
        return HospitalCode;
    }
    public void setHospitalCode(String aHospitalCode) {
        HospitalCode = aHospitalCode;
    }
    public String getHospitalName() {
        return HospitalName;
    }
    public void setHospitalName(String aHospitalName) {
        HospitalName = aHospitalName;
    }
    public String getInHospitalDate() {
        if(InHospitalDate != null) {
            return fDate.getString(InHospitalDate);
        } else {
            return null;
        }
    }
    public void setInHospitalDate(Date aInHospitalDate) {
        InHospitalDate = aInHospitalDate;
    }
    public void setInHospitalDate(String aInHospitalDate) {
        if (aInHospitalDate != null && !aInHospitalDate.equals("")) {
            InHospitalDate = fDate.getDate(aInHospitalDate);
        } else
            InHospitalDate = null;
    }

    public String getOutHospitalDate() {
        if(OutHospitalDate != null) {
            return fDate.getString(OutHospitalDate);
        } else {
            return null;
        }
    }
    public void setOutHospitalDate(Date aOutHospitalDate) {
        OutHospitalDate = aOutHospitalDate;
    }
    public void setOutHospitalDate(String aOutHospitalDate) {
        if (aOutHospitalDate != null && !aOutHospitalDate.equals("")) {
            OutHospitalDate = fDate.getDate(aOutHospitalDate);
        } else
            OutHospitalDate = null;
    }

    public String getRemark() {
        return Remark;
    }
    public void setRemark(String aRemark) {
        Remark = aRemark;
    }
    public String getSeriousGrade() {
        return SeriousGrade;
    }
    public void setSeriousGrade(String aSeriousGrade) {
        SeriousGrade = aSeriousGrade;
    }
    public String getSurveyFlag() {
        return SurveyFlag;
    }
    public void setSurveyFlag(String aSurveyFlag) {
        SurveyFlag = aSurveyFlag;
    }
    public String getOperator() {
        return Operator;
    }
    public void setOperator(String aOperator) {
        Operator = aOperator;
    }
    public String getMngCom() {
        return MngCom;
    }
    public void setMngCom(String aMngCom) {
        MngCom = aMngCom;
    }
    public String getMakeDate() {
        if(MakeDate != null) {
            return fDate.getString(MakeDate);
        } else {
            return null;
        }
    }
    public void setMakeDate(Date aMakeDate) {
        MakeDate = aMakeDate;
    }
    public void setMakeDate(String aMakeDate) {
        if (aMakeDate != null && !aMakeDate.equals("")) {
            MakeDate = fDate.getDate(aMakeDate);
        } else
            MakeDate = null;
    }

    public String getMakeTime() {
        return MakeTime;
    }
    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }
    public String getModifyDate() {
        if(ModifyDate != null) {
            return fDate.getString(ModifyDate);
        } else {
            return null;
        }
    }
    public void setModifyDate(Date aModifyDate) {
        ModifyDate = aModifyDate;
    }
    public void setModifyDate(String aModifyDate) {
        if (aModifyDate != null && !aModifyDate.equals("")) {
            ModifyDate = fDate.getDate(aModifyDate);
        } else
            ModifyDate = null;
    }

    public String getModifyTime() {
        return ModifyTime;
    }
    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }
    public String getFirstDiaDate() {
        if(FirstDiaDate != null) {
            return fDate.getString(FirstDiaDate);
        } else {
            return null;
        }
    }
    public void setFirstDiaDate(Date aFirstDiaDate) {
        FirstDiaDate = aFirstDiaDate;
    }
    public void setFirstDiaDate(String aFirstDiaDate) {
        if (aFirstDiaDate != null && !aFirstDiaDate.equals("")) {
            FirstDiaDate = fDate.getDate(aFirstDiaDate);
        } else
            FirstDiaDate = null;
    }

    public String getHosGrade() {
        return HosGrade;
    }
    public void setHosGrade(String aHosGrade) {
        HosGrade = aHosGrade;
    }
    public String getLocalPlace() {
        return LocalPlace;
    }
    public void setLocalPlace(String aLocalPlace) {
        LocalPlace = aLocalPlace;
    }
    public String getHosTel() {
        return HosTel;
    }
    public void setHosTel(String aHosTel) {
        HosTel = aHosTel;
    }
    public String getDieDate() {
        if(DieDate != null) {
            return fDate.getString(DieDate);
        } else {
            return null;
        }
    }
    public void setDieDate(Date aDieDate) {
        DieDate = aDieDate;
    }
    public void setDieDate(String aDieDate) {
        if (aDieDate != null && !aDieDate.equals("")) {
            DieDate = fDate.getDate(aDieDate);
        } else
            DieDate = null;
    }

    public String getAccCause() {
        return AccCause;
    }
    public void setAccCause(String aAccCause) {
        AccCause = aAccCause;
    }
    public String getVIPFlag() {
        return VIPFlag;
    }
    public void setVIPFlag(String aVIPFlag) {
        VIPFlag = aVIPFlag;
    }
    public String getAccidentDetail() {
        return AccidentDetail;
    }
    public void setAccidentDetail(String aAccidentDetail) {
        AccidentDetail = aAccidentDetail;
    }
    public String getCureDesc() {
        return CureDesc;
    }
    public void setCureDesc(String aCureDesc) {
        CureDesc = aCureDesc;
    }
    public String getSubmitFlag() {
        return SubmitFlag;
    }
    public void setSubmitFlag(String aSubmitFlag) {
        SubmitFlag = aSubmitFlag;
    }
    public String getCondoleFlag() {
        return CondoleFlag;
    }
    public void setCondoleFlag(String aCondoleFlag) {
        CondoleFlag = aCondoleFlag;
    }
    public String getDieFlag() {
        return DieFlag;
    }
    public void setDieFlag(String aDieFlag) {
        DieFlag = aDieFlag;
    }
    public String getAccResult1() {
        return AccResult1;
    }
    public void setAccResult1(String aAccResult1) {
        AccResult1 = aAccResult1;
    }
    public String getAccResult2() {
        return AccResult2;
    }
    public void setAccResult2(String aAccResult2) {
        AccResult2 = aAccResult2;
    }
    public String getCaseNo() {
        return CaseNo;
    }
    public void setCaseNo(String aCaseNo) {
        CaseNo = aCaseNo;
    }
    public int getSeqNo() {
        return SeqNo;
    }
    public void setSeqNo(int aSeqNo) {
        SeqNo = aSeqNo;
    }
    public void setSeqNo(String aSeqNo) {
        if (aSeqNo != null && !aSeqNo.equals("")) {
            Integer tInteger = new Integer(aSeqNo);
            int i = tInteger.intValue();
            SeqNo = i;
        }
    }


    /**
    * 使用另外一个 LLSubReportSchema 对象给 Schema 赋值
    * @param: aLLSubReportSchema LLSubReportSchema
    **/
    public void setSchema(LLSubReportSchema aLLSubReportSchema) {
        this.SubRptNo = aLLSubReportSchema.getSubRptNo();
        this.CustomerNo = aLLSubReportSchema.getCustomerNo();
        this.CustomerName = aLLSubReportSchema.getCustomerName();
        this.CustomerType = aLLSubReportSchema.getCustomerType();
        this.AccSubject = aLLSubReportSchema.getAccSubject();
        this.AccidentType = aLLSubReportSchema.getAccidentType();
        this.AccDate = fDate.getDate( aLLSubReportSchema.getAccDate());
        this.AccEndDate = fDate.getDate( aLLSubReportSchema.getAccEndDate());
        this.AccDesc = aLLSubReportSchema.getAccDesc();
        this.CustSituation = aLLSubReportSchema.getCustSituation();
        this.AccPlace = aLLSubReportSchema.getAccPlace();
        this.HospitalCode = aLLSubReportSchema.getHospitalCode();
        this.HospitalName = aLLSubReportSchema.getHospitalName();
        this.InHospitalDate = fDate.getDate( aLLSubReportSchema.getInHospitalDate());
        this.OutHospitalDate = fDate.getDate( aLLSubReportSchema.getOutHospitalDate());
        this.Remark = aLLSubReportSchema.getRemark();
        this.SeriousGrade = aLLSubReportSchema.getSeriousGrade();
        this.SurveyFlag = aLLSubReportSchema.getSurveyFlag();
        this.Operator = aLLSubReportSchema.getOperator();
        this.MngCom = aLLSubReportSchema.getMngCom();
        this.MakeDate = fDate.getDate( aLLSubReportSchema.getMakeDate());
        this.MakeTime = aLLSubReportSchema.getMakeTime();
        this.ModifyDate = fDate.getDate( aLLSubReportSchema.getModifyDate());
        this.ModifyTime = aLLSubReportSchema.getModifyTime();
        this.FirstDiaDate = fDate.getDate( aLLSubReportSchema.getFirstDiaDate());
        this.HosGrade = aLLSubReportSchema.getHosGrade();
        this.LocalPlace = aLLSubReportSchema.getLocalPlace();
        this.HosTel = aLLSubReportSchema.getHosTel();
        this.DieDate = fDate.getDate( aLLSubReportSchema.getDieDate());
        this.AccCause = aLLSubReportSchema.getAccCause();
        this.VIPFlag = aLLSubReportSchema.getVIPFlag();
        this.AccidentDetail = aLLSubReportSchema.getAccidentDetail();
        this.CureDesc = aLLSubReportSchema.getCureDesc();
        this.SubmitFlag = aLLSubReportSchema.getSubmitFlag();
        this.CondoleFlag = aLLSubReportSchema.getCondoleFlag();
        this.DieFlag = aLLSubReportSchema.getDieFlag();
        this.AccResult1 = aLLSubReportSchema.getAccResult1();
        this.AccResult2 = aLLSubReportSchema.getAccResult2();
        this.CaseNo = aLLSubReportSchema.getCaseNo();
        this.SeqNo = aLLSubReportSchema.getSeqNo();
    }

    /**
    * 使用 ResultSet 中的第 i 行给 Schema 赋值
    * @param: rs ResultSet
    * @param: i int
    * @return: boolean
    **/
    public boolean setSchema(ResultSet rs,int i) {
        try {
            //rs.absolute(i);        // 非滚动游标
            if( rs.getString("SubRptNo") == null )
                this.SubRptNo = null;
            else
                this.SubRptNo = rs.getString("SubRptNo").trim();

            if( rs.getString("CustomerNo") == null )
                this.CustomerNo = null;
            else
                this.CustomerNo = rs.getString("CustomerNo").trim();

            if( rs.getString("CustomerName") == null )
                this.CustomerName = null;
            else
                this.CustomerName = rs.getString("CustomerName").trim();

            if( rs.getString("CustomerType") == null )
                this.CustomerType = null;
            else
                this.CustomerType = rs.getString("CustomerType").trim();

            if( rs.getString("AccSubject") == null )
                this.AccSubject = null;
            else
                this.AccSubject = rs.getString("AccSubject").trim();

            if( rs.getString("AccidentType") == null )
                this.AccidentType = null;
            else
                this.AccidentType = rs.getString("AccidentType").trim();

            this.AccDate = rs.getDate("AccDate");
            this.AccEndDate = rs.getDate("AccEndDate");
            if( rs.getString("AccDesc") == null )
                this.AccDesc = null;
            else
                this.AccDesc = rs.getString("AccDesc").trim();

            if( rs.getString("CustSituation") == null )
                this.CustSituation = null;
            else
                this.CustSituation = rs.getString("CustSituation").trim();

            if( rs.getString("AccPlace") == null )
                this.AccPlace = null;
            else
                this.AccPlace = rs.getString("AccPlace").trim();

            if( rs.getString("HospitalCode") == null )
                this.HospitalCode = null;
            else
                this.HospitalCode = rs.getString("HospitalCode").trim();

            if( rs.getString("HospitalName") == null )
                this.HospitalName = null;
            else
                this.HospitalName = rs.getString("HospitalName").trim();

            this.InHospitalDate = rs.getDate("InHospitalDate");
            this.OutHospitalDate = rs.getDate("OutHospitalDate");
            if( rs.getString("Remark") == null )
                this.Remark = null;
            else
                this.Remark = rs.getString("Remark").trim();

            if( rs.getString("SeriousGrade") == null )
                this.SeriousGrade = null;
            else
                this.SeriousGrade = rs.getString("SeriousGrade").trim();

            if( rs.getString("SurveyFlag") == null )
                this.SurveyFlag = null;
            else
                this.SurveyFlag = rs.getString("SurveyFlag").trim();

            if( rs.getString("Operator") == null )
                this.Operator = null;
            else
                this.Operator = rs.getString("Operator").trim();

            if( rs.getString("MngCom") == null )
                this.MngCom = null;
            else
                this.MngCom = rs.getString("MngCom").trim();

            this.MakeDate = rs.getDate("MakeDate");
            if( rs.getString("MakeTime") == null )
                this.MakeTime = null;
            else
                this.MakeTime = rs.getString("MakeTime").trim();

            this.ModifyDate = rs.getDate("ModifyDate");
            if( rs.getString("ModifyTime") == null )
                this.ModifyTime = null;
            else
                this.ModifyTime = rs.getString("ModifyTime").trim();

            this.FirstDiaDate = rs.getDate("FirstDiaDate");
            if( rs.getString("HosGrade") == null )
                this.HosGrade = null;
            else
                this.HosGrade = rs.getString("HosGrade").trim();

            if( rs.getString("LocalPlace") == null )
                this.LocalPlace = null;
            else
                this.LocalPlace = rs.getString("LocalPlace").trim();

            if( rs.getString("HosTel") == null )
                this.HosTel = null;
            else
                this.HosTel = rs.getString("HosTel").trim();

            this.DieDate = rs.getDate("DieDate");
            if( rs.getString("AccCause") == null )
                this.AccCause = null;
            else
                this.AccCause = rs.getString("AccCause").trim();

            if( rs.getString("VIPFlag") == null )
                this.VIPFlag = null;
            else
                this.VIPFlag = rs.getString("VIPFlag").trim();

            if( rs.getString("AccidentDetail") == null )
                this.AccidentDetail = null;
            else
                this.AccidentDetail = rs.getString("AccidentDetail").trim();

            if( rs.getString("CureDesc") == null )
                this.CureDesc = null;
            else
                this.CureDesc = rs.getString("CureDesc").trim();

            if( rs.getString("SubmitFlag") == null )
                this.SubmitFlag = null;
            else
                this.SubmitFlag = rs.getString("SubmitFlag").trim();

            if( rs.getString("CondoleFlag") == null )
                this.CondoleFlag = null;
            else
                this.CondoleFlag = rs.getString("CondoleFlag").trim();

            if( rs.getString("DieFlag") == null )
                this.DieFlag = null;
            else
                this.DieFlag = rs.getString("DieFlag").trim();

            if( rs.getString("AccResult1") == null )
                this.AccResult1 = null;
            else
                this.AccResult1 = rs.getString("AccResult1").trim();

            if( rs.getString("AccResult2") == null )
                this.AccResult2 = null;
            else
                this.AccResult2 = rs.getString("AccResult2").trim();

            if( rs.getString("CaseNo") == null )
                this.CaseNo = null;
            else
                this.CaseNo = rs.getString("CaseNo").trim();

            this.SeqNo = rs.getInt("SeqNo");
        }
        catch(SQLException sqle) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LLSubReportSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = ExceptionUtils.exceptionToString(sqle);
            this.mErrors .addOneError(tError);
            return false;
        }
        return true;
    }

    public LLSubReportSchema getSchema() {
        LLSubReportSchema aLLSubReportSchema = new LLSubReportSchema();
        aLLSubReportSchema.setSchema(this);
        return aLLSubReportSchema;
    }

    public LLSubReportDB getDB() {
        LLSubReportDB aDBOper = new LLSubReportDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
    * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLLSubReport描述/A>表字段
    * @return: String 返回打包后字符串
    **/
    public String encode() {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(SubRptNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(CustomerNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(CustomerName)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(CustomerType)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AccSubject)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AccidentType)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( AccDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( AccEndDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AccDesc)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(CustSituation)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AccPlace)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(HospitalCode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(HospitalName)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( InHospitalDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( OutHospitalDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Remark)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(SeriousGrade)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(SurveyFlag)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MngCom)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ModifyTime)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( FirstDiaDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(HosGrade)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(LocalPlace)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(HosTel)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( DieDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AccCause)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(VIPFlag)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AccidentDetail)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(CureDesc)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(SubmitFlag)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(CondoleFlag)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(DieFlag)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AccResult1)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AccResult2)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(CaseNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(SeqNo));
        return strReturn.toString();
    }

    /**
    * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLLSubReport>历史记账凭证主表信息</A>表字段
    * @param: strMessage String 包含一条纪录数据的字符串
    * @return: boolean
    **/
    public boolean decode(String strMessage) {
        try {
            SubRptNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
            CustomerNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
            CustomerName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
            CustomerType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
            AccSubject = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
            AccidentType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
            AccDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER));
            AccEndDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER));
            AccDesc = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
            CustSituation = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
            AccPlace = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
            HospitalCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
            HospitalName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
            InHospitalDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER));
            OutHospitalDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15, SysConst.PACKAGESPILTER));
            Remark = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16, SysConst.PACKAGESPILTER );
            SeriousGrade = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17, SysConst.PACKAGESPILTER );
            SurveyFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18, SysConst.PACKAGESPILTER );
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19, SysConst.PACKAGESPILTER );
            MngCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20, SysConst.PACKAGESPILTER );
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 21, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 22, SysConst.PACKAGESPILTER );
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 23, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 24, SysConst.PACKAGESPILTER );
            FirstDiaDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 25, SysConst.PACKAGESPILTER));
            HosGrade = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 26, SysConst.PACKAGESPILTER );
            LocalPlace = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 27, SysConst.PACKAGESPILTER );
            HosTel = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 28, SysConst.PACKAGESPILTER );
            DieDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 29, SysConst.PACKAGESPILTER));
            AccCause = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 30, SysConst.PACKAGESPILTER );
            VIPFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 31, SysConst.PACKAGESPILTER );
            AccidentDetail = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 32, SysConst.PACKAGESPILTER );
            CureDesc = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 33, SysConst.PACKAGESPILTER );
            SubmitFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 34, SysConst.PACKAGESPILTER );
            CondoleFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 35, SysConst.PACKAGESPILTER );
            DieFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 36, SysConst.PACKAGESPILTER );
            AccResult1 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 37, SysConst.PACKAGESPILTER );
            AccResult2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 38, SysConst.PACKAGESPILTER );
            CaseNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 39, SysConst.PACKAGESPILTER );
            SeqNo = new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,40, SysConst.PACKAGESPILTER))).intValue();
        }
        catch(NumberFormatException ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LLSubReportSchema";
            tError.functionName = "decode";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            return false;
        }
        return true;
    }

    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("SubRptNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SubRptNo));
        }
        if (FCode.equalsIgnoreCase("CustomerNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CustomerNo));
        }
        if (FCode.equalsIgnoreCase("CustomerName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CustomerName));
        }
        if (FCode.equalsIgnoreCase("CustomerType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CustomerType));
        }
        if (FCode.equalsIgnoreCase("AccSubject")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AccSubject));
        }
        if (FCode.equalsIgnoreCase("AccidentType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AccidentType));
        }
        if (FCode.equalsIgnoreCase("AccDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getAccDate()));
        }
        if (FCode.equalsIgnoreCase("AccEndDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getAccEndDate()));
        }
        if (FCode.equalsIgnoreCase("AccDesc")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AccDesc));
        }
        if (FCode.equalsIgnoreCase("CustSituation")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CustSituation));
        }
        if (FCode.equalsIgnoreCase("AccPlace")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AccPlace));
        }
        if (FCode.equalsIgnoreCase("HospitalCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(HospitalCode));
        }
        if (FCode.equalsIgnoreCase("HospitalName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(HospitalName));
        }
        if (FCode.equalsIgnoreCase("InHospitalDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getInHospitalDate()));
        }
        if (FCode.equalsIgnoreCase("OutHospitalDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getOutHospitalDate()));
        }
        if (FCode.equalsIgnoreCase("Remark")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Remark));
        }
        if (FCode.equalsIgnoreCase("SeriousGrade")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SeriousGrade));
        }
        if (FCode.equalsIgnoreCase("SurveyFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SurveyFlag));
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equalsIgnoreCase("MngCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MngCom));
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (FCode.equalsIgnoreCase("FirstDiaDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getFirstDiaDate()));
        }
        if (FCode.equalsIgnoreCase("HosGrade")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(HosGrade));
        }
        if (FCode.equalsIgnoreCase("LocalPlace")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(LocalPlace));
        }
        if (FCode.equalsIgnoreCase("HosTel")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(HosTel));
        }
        if (FCode.equalsIgnoreCase("DieDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getDieDate()));
        }
        if (FCode.equalsIgnoreCase("AccCause")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AccCause));
        }
        if (FCode.equalsIgnoreCase("VIPFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(VIPFlag));
        }
        if (FCode.equalsIgnoreCase("AccidentDetail")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AccidentDetail));
        }
        if (FCode.equalsIgnoreCase("CureDesc")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CureDesc));
        }
        if (FCode.equalsIgnoreCase("SubmitFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SubmitFlag));
        }
        if (FCode.equalsIgnoreCase("CondoleFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CondoleFlag));
        }
        if (FCode.equalsIgnoreCase("DieFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DieFlag));
        }
        if (FCode.equalsIgnoreCase("AccResult1")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AccResult1));
        }
        if (FCode.equalsIgnoreCase("AccResult2")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AccResult2));
        }
        if (FCode.equalsIgnoreCase("CaseNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CaseNo));
        }
        if (FCode.equalsIgnoreCase("SeqNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SeqNo));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(SubRptNo);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(CustomerNo);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(CustomerName);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(CustomerType);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(AccSubject);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(AccidentType);
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getAccDate()));
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getAccEndDate()));
                break;
            case 8:
                strFieldValue = StrTool.GBKToUnicode(AccDesc);
                break;
            case 9:
                strFieldValue = StrTool.GBKToUnicode(CustSituation);
                break;
            case 10:
                strFieldValue = StrTool.GBKToUnicode(AccPlace);
                break;
            case 11:
                strFieldValue = StrTool.GBKToUnicode(HospitalCode);
                break;
            case 12:
                strFieldValue = StrTool.GBKToUnicode(HospitalName);
                break;
            case 13:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getInHospitalDate()));
                break;
            case 14:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getOutHospitalDate()));
                break;
            case 15:
                strFieldValue = StrTool.GBKToUnicode(Remark);
                break;
            case 16:
                strFieldValue = StrTool.GBKToUnicode(SeriousGrade);
                break;
            case 17:
                strFieldValue = StrTool.GBKToUnicode(SurveyFlag);
                break;
            case 18:
                strFieldValue = StrTool.GBKToUnicode(Operator);
                break;
            case 19:
                strFieldValue = StrTool.GBKToUnicode(MngCom);
                break;
            case 20:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
                break;
            case 21:
                strFieldValue = StrTool.GBKToUnicode(MakeTime);
                break;
            case 22:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
                break;
            case 23:
                strFieldValue = StrTool.GBKToUnicode(ModifyTime);
                break;
            case 24:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getFirstDiaDate()));
                break;
            case 25:
                strFieldValue = StrTool.GBKToUnicode(HosGrade);
                break;
            case 26:
                strFieldValue = StrTool.GBKToUnicode(LocalPlace);
                break;
            case 27:
                strFieldValue = StrTool.GBKToUnicode(HosTel);
                break;
            case 28:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getDieDate()));
                break;
            case 29:
                strFieldValue = StrTool.GBKToUnicode(AccCause);
                break;
            case 30:
                strFieldValue = StrTool.GBKToUnicode(VIPFlag);
                break;
            case 31:
                strFieldValue = StrTool.GBKToUnicode(AccidentDetail);
                break;
            case 32:
                strFieldValue = StrTool.GBKToUnicode(CureDesc);
                break;
            case 33:
                strFieldValue = StrTool.GBKToUnicode(SubmitFlag);
                break;
            case 34:
                strFieldValue = StrTool.GBKToUnicode(CondoleFlag);
                break;
            case 35:
                strFieldValue = StrTool.GBKToUnicode(DieFlag);
                break;
            case 36:
                strFieldValue = StrTool.GBKToUnicode(AccResult1);
                break;
            case 37:
                strFieldValue = StrTool.GBKToUnicode(AccResult2);
                break;
            case 38:
                strFieldValue = StrTool.GBKToUnicode(CaseNo);
                break;
            case 39:
                strFieldValue = String.valueOf(SeqNo);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("SubRptNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                SubRptNo = FValue.trim();
            }
            else
                SubRptNo = null;
        }
        if (FCode.equalsIgnoreCase("CustomerNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                CustomerNo = FValue.trim();
            }
            else
                CustomerNo = null;
        }
        if (FCode.equalsIgnoreCase("CustomerName")) {
            if( FValue != null && !FValue.equals(""))
            {
                CustomerName = FValue.trim();
            }
            else
                CustomerName = null;
        }
        if (FCode.equalsIgnoreCase("CustomerType")) {
            if( FValue != null && !FValue.equals(""))
            {
                CustomerType = FValue.trim();
            }
            else
                CustomerType = null;
        }
        if (FCode.equalsIgnoreCase("AccSubject")) {
            if( FValue != null && !FValue.equals(""))
            {
                AccSubject = FValue.trim();
            }
            else
                AccSubject = null;
        }
        if (FCode.equalsIgnoreCase("AccidentType")) {
            if( FValue != null && !FValue.equals(""))
            {
                AccidentType = FValue.trim();
            }
            else
                AccidentType = null;
        }
        if (FCode.equalsIgnoreCase("AccDate")) {
            if(FValue != null && !FValue.equals("")) {
                AccDate = fDate.getDate( FValue );
            }
            else
                AccDate = null;
        }
        if (FCode.equalsIgnoreCase("AccEndDate")) {
            if(FValue != null && !FValue.equals("")) {
                AccEndDate = fDate.getDate( FValue );
            }
            else
                AccEndDate = null;
        }
        if (FCode.equalsIgnoreCase("AccDesc")) {
            if( FValue != null && !FValue.equals(""))
            {
                AccDesc = FValue.trim();
            }
            else
                AccDesc = null;
        }
        if (FCode.equalsIgnoreCase("CustSituation")) {
            if( FValue != null && !FValue.equals(""))
            {
                CustSituation = FValue.trim();
            }
            else
                CustSituation = null;
        }
        if (FCode.equalsIgnoreCase("AccPlace")) {
            if( FValue != null && !FValue.equals(""))
            {
                AccPlace = FValue.trim();
            }
            else
                AccPlace = null;
        }
        if (FCode.equalsIgnoreCase("HospitalCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                HospitalCode = FValue.trim();
            }
            else
                HospitalCode = null;
        }
        if (FCode.equalsIgnoreCase("HospitalName")) {
            if( FValue != null && !FValue.equals(""))
            {
                HospitalName = FValue.trim();
            }
            else
                HospitalName = null;
        }
        if (FCode.equalsIgnoreCase("InHospitalDate")) {
            if(FValue != null && !FValue.equals("")) {
                InHospitalDate = fDate.getDate( FValue );
            }
            else
                InHospitalDate = null;
        }
        if (FCode.equalsIgnoreCase("OutHospitalDate")) {
            if(FValue != null && !FValue.equals("")) {
                OutHospitalDate = fDate.getDate( FValue );
            }
            else
                OutHospitalDate = null;
        }
        if (FCode.equalsIgnoreCase("Remark")) {
            if( FValue != null && !FValue.equals(""))
            {
                Remark = FValue.trim();
            }
            else
                Remark = null;
        }
        if (FCode.equalsIgnoreCase("SeriousGrade")) {
            if( FValue != null && !FValue.equals(""))
            {
                SeriousGrade = FValue.trim();
            }
            else
                SeriousGrade = null;
        }
        if (FCode.equalsIgnoreCase("SurveyFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                SurveyFlag = FValue.trim();
            }
            else
                SurveyFlag = null;
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            if( FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
                Operator = null;
        }
        if (FCode.equalsIgnoreCase("MngCom")) {
            if( FValue != null && !FValue.equals(""))
            {
                MngCom = FValue.trim();
            }
            else
                MngCom = null;
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if(FValue != null && !FValue.equals("")) {
                MakeDate = fDate.getDate( FValue );
            }
            else
                MakeDate = null;
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
                MakeTime = null;
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if(FValue != null && !FValue.equals("")) {
                ModifyDate = fDate.getDate( FValue );
            }
            else
                ModifyDate = null;
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
                ModifyTime = null;
        }
        if (FCode.equalsIgnoreCase("FirstDiaDate")) {
            if(FValue != null && !FValue.equals("")) {
                FirstDiaDate = fDate.getDate( FValue );
            }
            else
                FirstDiaDate = null;
        }
        if (FCode.equalsIgnoreCase("HosGrade")) {
            if( FValue != null && !FValue.equals(""))
            {
                HosGrade = FValue.trim();
            }
            else
                HosGrade = null;
        }
        if (FCode.equalsIgnoreCase("LocalPlace")) {
            if( FValue != null && !FValue.equals(""))
            {
                LocalPlace = FValue.trim();
            }
            else
                LocalPlace = null;
        }
        if (FCode.equalsIgnoreCase("HosTel")) {
            if( FValue != null && !FValue.equals(""))
            {
                HosTel = FValue.trim();
            }
            else
                HosTel = null;
        }
        if (FCode.equalsIgnoreCase("DieDate")) {
            if(FValue != null && !FValue.equals("")) {
                DieDate = fDate.getDate( FValue );
            }
            else
                DieDate = null;
        }
        if (FCode.equalsIgnoreCase("AccCause")) {
            if( FValue != null && !FValue.equals(""))
            {
                AccCause = FValue.trim();
            }
            else
                AccCause = null;
        }
        if (FCode.equalsIgnoreCase("VIPFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                VIPFlag = FValue.trim();
            }
            else
                VIPFlag = null;
        }
        if (FCode.equalsIgnoreCase("AccidentDetail")) {
            if( FValue != null && !FValue.equals(""))
            {
                AccidentDetail = FValue.trim();
            }
            else
                AccidentDetail = null;
        }
        if (FCode.equalsIgnoreCase("CureDesc")) {
            if( FValue != null && !FValue.equals(""))
            {
                CureDesc = FValue.trim();
            }
            else
                CureDesc = null;
        }
        if (FCode.equalsIgnoreCase("SubmitFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                SubmitFlag = FValue.trim();
            }
            else
                SubmitFlag = null;
        }
        if (FCode.equalsIgnoreCase("CondoleFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                CondoleFlag = FValue.trim();
            }
            else
                CondoleFlag = null;
        }
        if (FCode.equalsIgnoreCase("DieFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                DieFlag = FValue.trim();
            }
            else
                DieFlag = null;
        }
        if (FCode.equalsIgnoreCase("AccResult1")) {
            if( FValue != null && !FValue.equals(""))
            {
                AccResult1 = FValue.trim();
            }
            else
                AccResult1 = null;
        }
        if (FCode.equalsIgnoreCase("AccResult2")) {
            if( FValue != null && !FValue.equals(""))
            {
                AccResult2 = FValue.trim();
            }
            else
                AccResult2 = null;
        }
        if (FCode.equalsIgnoreCase("CaseNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                CaseNo = FValue.trim();
            }
            else
                CaseNo = null;
        }
        if (FCode.equalsIgnoreCase("SeqNo")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                SeqNo = i;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject) return true;
        if (otherObject == null) return false;
        if (getClass() != otherObject.getClass()) return false;
        LLSubReportSchema other = (LLSubReportSchema)otherObject;
        return
            SubRptNo.equals(other.getSubRptNo())
            && CustomerNo.equals(other.getCustomerNo())
            && CustomerName.equals(other.getCustomerName())
            && CustomerType.equals(other.getCustomerType())
            && AccSubject.equals(other.getAccSubject())
            && AccidentType.equals(other.getAccidentType())
            && fDate.getString(AccDate).equals(other.getAccDate())
            && fDate.getString(AccEndDate).equals(other.getAccEndDate())
            && AccDesc.equals(other.getAccDesc())
            && CustSituation.equals(other.getCustSituation())
            && AccPlace.equals(other.getAccPlace())
            && HospitalCode.equals(other.getHospitalCode())
            && HospitalName.equals(other.getHospitalName())
            && fDate.getString(InHospitalDate).equals(other.getInHospitalDate())
            && fDate.getString(OutHospitalDate).equals(other.getOutHospitalDate())
            && Remark.equals(other.getRemark())
            && SeriousGrade.equals(other.getSeriousGrade())
            && SurveyFlag.equals(other.getSurveyFlag())
            && Operator.equals(other.getOperator())
            && MngCom.equals(other.getMngCom())
            && fDate.getString(MakeDate).equals(other.getMakeDate())
            && MakeTime.equals(other.getMakeTime())
            && fDate.getString(ModifyDate).equals(other.getModifyDate())
            && ModifyTime.equals(other.getModifyTime())
            && fDate.getString(FirstDiaDate).equals(other.getFirstDiaDate())
            && HosGrade.equals(other.getHosGrade())
            && LocalPlace.equals(other.getLocalPlace())
            && HosTel.equals(other.getHosTel())
            && fDate.getString(DieDate).equals(other.getDieDate())
            && AccCause.equals(other.getAccCause())
            && VIPFlag.equals(other.getVIPFlag())
            && AccidentDetail.equals(other.getAccidentDetail())
            && CureDesc.equals(other.getCureDesc())
            && SubmitFlag.equals(other.getSubmitFlag())
            && CondoleFlag.equals(other.getCondoleFlag())
            && DieFlag.equals(other.getDieFlag())
            && AccResult1.equals(other.getAccResult1())
            && AccResult2.equals(other.getAccResult2())
            && CaseNo.equals(other.getCaseNo())
            && SeqNo == other.getSeqNo();
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("SubRptNo") ) {
            return 0;
        }
        if( strFieldName.equals("CustomerNo") ) {
            return 1;
        }
        if( strFieldName.equals("CustomerName") ) {
            return 2;
        }
        if( strFieldName.equals("CustomerType") ) {
            return 3;
        }
        if( strFieldName.equals("AccSubject") ) {
            return 4;
        }
        if( strFieldName.equals("AccidentType") ) {
            return 5;
        }
        if( strFieldName.equals("AccDate") ) {
            return 6;
        }
        if( strFieldName.equals("AccEndDate") ) {
            return 7;
        }
        if( strFieldName.equals("AccDesc") ) {
            return 8;
        }
        if( strFieldName.equals("CustSituation") ) {
            return 9;
        }
        if( strFieldName.equals("AccPlace") ) {
            return 10;
        }
        if( strFieldName.equals("HospitalCode") ) {
            return 11;
        }
        if( strFieldName.equals("HospitalName") ) {
            return 12;
        }
        if( strFieldName.equals("InHospitalDate") ) {
            return 13;
        }
        if( strFieldName.equals("OutHospitalDate") ) {
            return 14;
        }
        if( strFieldName.equals("Remark") ) {
            return 15;
        }
        if( strFieldName.equals("SeriousGrade") ) {
            return 16;
        }
        if( strFieldName.equals("SurveyFlag") ) {
            return 17;
        }
        if( strFieldName.equals("Operator") ) {
            return 18;
        }
        if( strFieldName.equals("MngCom") ) {
            return 19;
        }
        if( strFieldName.equals("MakeDate") ) {
            return 20;
        }
        if( strFieldName.equals("MakeTime") ) {
            return 21;
        }
        if( strFieldName.equals("ModifyDate") ) {
            return 22;
        }
        if( strFieldName.equals("ModifyTime") ) {
            return 23;
        }
        if( strFieldName.equals("FirstDiaDate") ) {
            return 24;
        }
        if( strFieldName.equals("HosGrade") ) {
            return 25;
        }
        if( strFieldName.equals("LocalPlace") ) {
            return 26;
        }
        if( strFieldName.equals("HosTel") ) {
            return 27;
        }
        if( strFieldName.equals("DieDate") ) {
            return 28;
        }
        if( strFieldName.equals("AccCause") ) {
            return 29;
        }
        if( strFieldName.equals("VIPFlag") ) {
            return 30;
        }
        if( strFieldName.equals("AccidentDetail") ) {
            return 31;
        }
        if( strFieldName.equals("CureDesc") ) {
            return 32;
        }
        if( strFieldName.equals("SubmitFlag") ) {
            return 33;
        }
        if( strFieldName.equals("CondoleFlag") ) {
            return 34;
        }
        if( strFieldName.equals("DieFlag") ) {
            return 35;
        }
        if( strFieldName.equals("AccResult1") ) {
            return 36;
        }
        if( strFieldName.equals("AccResult2") ) {
            return 37;
        }
        if( strFieldName.equals("CaseNo") ) {
            return 38;
        }
        if( strFieldName.equals("SeqNo") ) {
            return 39;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "SubRptNo";
                break;
            case 1:
                strFieldName = "CustomerNo";
                break;
            case 2:
                strFieldName = "CustomerName";
                break;
            case 3:
                strFieldName = "CustomerType";
                break;
            case 4:
                strFieldName = "AccSubject";
                break;
            case 5:
                strFieldName = "AccidentType";
                break;
            case 6:
                strFieldName = "AccDate";
                break;
            case 7:
                strFieldName = "AccEndDate";
                break;
            case 8:
                strFieldName = "AccDesc";
                break;
            case 9:
                strFieldName = "CustSituation";
                break;
            case 10:
                strFieldName = "AccPlace";
                break;
            case 11:
                strFieldName = "HospitalCode";
                break;
            case 12:
                strFieldName = "HospitalName";
                break;
            case 13:
                strFieldName = "InHospitalDate";
                break;
            case 14:
                strFieldName = "OutHospitalDate";
                break;
            case 15:
                strFieldName = "Remark";
                break;
            case 16:
                strFieldName = "SeriousGrade";
                break;
            case 17:
                strFieldName = "SurveyFlag";
                break;
            case 18:
                strFieldName = "Operator";
                break;
            case 19:
                strFieldName = "MngCom";
                break;
            case 20:
                strFieldName = "MakeDate";
                break;
            case 21:
                strFieldName = "MakeTime";
                break;
            case 22:
                strFieldName = "ModifyDate";
                break;
            case 23:
                strFieldName = "ModifyTime";
                break;
            case 24:
                strFieldName = "FirstDiaDate";
                break;
            case 25:
                strFieldName = "HosGrade";
                break;
            case 26:
                strFieldName = "LocalPlace";
                break;
            case 27:
                strFieldName = "HosTel";
                break;
            case 28:
                strFieldName = "DieDate";
                break;
            case 29:
                strFieldName = "AccCause";
                break;
            case 30:
                strFieldName = "VIPFlag";
                break;
            case 31:
                strFieldName = "AccidentDetail";
                break;
            case 32:
                strFieldName = "CureDesc";
                break;
            case 33:
                strFieldName = "SubmitFlag";
                break;
            case 34:
                strFieldName = "CondoleFlag";
                break;
            case 35:
                strFieldName = "DieFlag";
                break;
            case 36:
                strFieldName = "AccResult1";
                break;
            case 37:
                strFieldName = "AccResult2";
                break;
            case 38:
                strFieldName = "CaseNo";
                break;
            case 39:
                strFieldName = "SeqNo";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "SUBRPTNO":
                return Schema.TYPE_STRING;
            case "CUSTOMERNO":
                return Schema.TYPE_STRING;
            case "CUSTOMERNAME":
                return Schema.TYPE_STRING;
            case "CUSTOMERTYPE":
                return Schema.TYPE_STRING;
            case "ACCSUBJECT":
                return Schema.TYPE_STRING;
            case "ACCIDENTTYPE":
                return Schema.TYPE_STRING;
            case "ACCDATE":
                return Schema.TYPE_DATE;
            case "ACCENDDATE":
                return Schema.TYPE_DATE;
            case "ACCDESC":
                return Schema.TYPE_STRING;
            case "CUSTSITUATION":
                return Schema.TYPE_STRING;
            case "ACCPLACE":
                return Schema.TYPE_STRING;
            case "HOSPITALCODE":
                return Schema.TYPE_STRING;
            case "HOSPITALNAME":
                return Schema.TYPE_STRING;
            case "INHOSPITALDATE":
                return Schema.TYPE_DATE;
            case "OUTHOSPITALDATE":
                return Schema.TYPE_DATE;
            case "REMARK":
                return Schema.TYPE_STRING;
            case "SERIOUSGRADE":
                return Schema.TYPE_STRING;
            case "SURVEYFLAG":
                return Schema.TYPE_STRING;
            case "OPERATOR":
                return Schema.TYPE_STRING;
            case "MNGCOM":
                return Schema.TYPE_STRING;
            case "MAKEDATE":
                return Schema.TYPE_DATE;
            case "MAKETIME":
                return Schema.TYPE_STRING;
            case "MODIFYDATE":
                return Schema.TYPE_DATE;
            case "MODIFYTIME":
                return Schema.TYPE_STRING;
            case "FIRSTDIADATE":
                return Schema.TYPE_DATE;
            case "HOSGRADE":
                return Schema.TYPE_STRING;
            case "LOCALPLACE":
                return Schema.TYPE_STRING;
            case "HOSTEL":
                return Schema.TYPE_STRING;
            case "DIEDATE":
                return Schema.TYPE_DATE;
            case "ACCCAUSE":
                return Schema.TYPE_STRING;
            case "VIPFLAG":
                return Schema.TYPE_STRING;
            case "ACCIDENTDETAIL":
                return Schema.TYPE_STRING;
            case "CUREDESC":
                return Schema.TYPE_STRING;
            case "SUBMITFLAG":
                return Schema.TYPE_STRING;
            case "CONDOLEFLAG":
                return Schema.TYPE_STRING;
            case "DIEFLAG":
                return Schema.TYPE_STRING;
            case "ACCRESULT1":
                return Schema.TYPE_STRING;
            case "ACCRESULT2":
                return Schema.TYPE_STRING;
            case "CASENO":
                return Schema.TYPE_STRING;
            case "SEQNO":
                return Schema.TYPE_INT;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_STRING;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_STRING;
            case 6:
                return Schema.TYPE_DATE;
            case 7:
                return Schema.TYPE_DATE;
            case 8:
                return Schema.TYPE_STRING;
            case 9:
                return Schema.TYPE_STRING;
            case 10:
                return Schema.TYPE_STRING;
            case 11:
                return Schema.TYPE_STRING;
            case 12:
                return Schema.TYPE_STRING;
            case 13:
                return Schema.TYPE_DATE;
            case 14:
                return Schema.TYPE_DATE;
            case 15:
                return Schema.TYPE_STRING;
            case 16:
                return Schema.TYPE_STRING;
            case 17:
                return Schema.TYPE_STRING;
            case 18:
                return Schema.TYPE_STRING;
            case 19:
                return Schema.TYPE_STRING;
            case 20:
                return Schema.TYPE_DATE;
            case 21:
                return Schema.TYPE_STRING;
            case 22:
                return Schema.TYPE_DATE;
            case 23:
                return Schema.TYPE_STRING;
            case 24:
                return Schema.TYPE_DATE;
            case 25:
                return Schema.TYPE_STRING;
            case 26:
                return Schema.TYPE_STRING;
            case 27:
                return Schema.TYPE_STRING;
            case 28:
                return Schema.TYPE_DATE;
            case 29:
                return Schema.TYPE_STRING;
            case 30:
                return Schema.TYPE_STRING;
            case 31:
                return Schema.TYPE_STRING;
            case 32:
                return Schema.TYPE_STRING;
            case 33:
                return Schema.TYPE_STRING;
            case 34:
                return Schema.TYPE_STRING;
            case 35:
                return Schema.TYPE_STRING;
            case 36:
                return Schema.TYPE_STRING;
            case 37:
                return Schema.TYPE_STRING;
            case 38:
                return Schema.TYPE_STRING;
            case 39:
                return Schema.TYPE_INT;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
}
