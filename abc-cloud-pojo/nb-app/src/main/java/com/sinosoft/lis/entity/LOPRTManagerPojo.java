/**
 * Copyright (c) 2017 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.entity;

import com.sinosoft.cloud.cache.annotation.RedisIndexHKey;
import com.sinosoft.cloud.cache.annotation.RedisPrimaryHKey;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.Pojo;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;

import java.io.Serializable;

/**
 * <p>ClassName: LOPRTManagerPojo </p>
 * <p>Copyright: Copyright (c) 2017</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2017-10-17
 */
public class LOPRTManagerPojo implements Pojo,Serializable {
    // @Field
    /** Id */
    private long PRTManagerID; 
    /** Shardingid */
    private String ShardingID; 
    /** 印刷流水号 */
    @RedisPrimaryHKey
    private String PrtSeq; 
    /** 对应其它号码 */
    @RedisIndexHKey
    private String OtherNo; 
    /** 其它号码类型 */
    private String OtherNoType; 
    /** 单据类型 */
    private String Code; 
    /** 管理机构 */
    private String ManageCom; 
    /** 代理人编码 */
    private String AgentCode; 
    /** 发起机构 */
    private String ReqCom; 
    /** 发起人 */
    private String ReqOperator; 
    /** 执行机构 */
    private String ExeCom; 
    /** 执行人 */
    private String ExeOperator; 
    /** 打印方式 */
    private String PrtType; 
    /** 打印状态 */
    private String StateFlag; 
    /** 入机日期 */
    private String  MakeDate;
    /** 入机时间 */
    private String MakeTime; 
    /** 完成日期 */
    private String  DoneDate;
    /** 完成时间 */
    private String DoneTime; 
    /** 备用属性字段1 */
    private String StandbyFlag1; 
    /** 备用属性字段2 */
    private String StandbyFlag2; 
    /** 旧印刷流水号 */
    private String OldPrtSeq; 
    /** 补打标志 */
    private String PatchFlag; 
    /** 备用属性字段3 */
    private String StandbyFlag3; 
    /** 备用属性字段4 */
    private String StandbyFlag4; 
    /** 预计催办日期 */
    private String  ForMakeDate;
    /** 预计催办时间 */
    private String ForMakeTime; 
    /** 实际催办日期 */
    private String  ActMakeDate;
    /** 实际催办时间 */
    private String ActMakeTime; 
    /** 备注 */
    private String Remark; 
    /** 备用属性字段5 */
    private String StandbyFlag5; 
    /** 备用属性字段6 */
    private String StandbyFlag6; 
    /** 备用属性字段7 */
    private String StandbyFlag7; 


    public static final int FIELDNUM = 32;    // 数据库表的字段个数

    private FDate fDate = new FDate();        // 处理日期
    public long getPRTManagerID() {
        return PRTManagerID;
    }
    public void setPRTManagerID(long aPRTManagerID) {
        PRTManagerID = aPRTManagerID;
    }
    public void setPRTManagerID(String aPRTManagerID) {
        if (aPRTManagerID != null && !aPRTManagerID.equals("")) {
            PRTManagerID = new Long(aPRTManagerID).longValue();
        }
    }

    public String getShardingID() {
        return ShardingID;
    }
    public void setShardingID(String aShardingID) {
        ShardingID = aShardingID;
    }
    public String getPrtSeq() {
        return PrtSeq;
    }
    public void setPrtSeq(String aPrtSeq) {
        PrtSeq = aPrtSeq;
    }
    public String getOtherNo() {
        return OtherNo;
    }
    public void setOtherNo(String aOtherNo) {
        OtherNo = aOtherNo;
    }
    public String getOtherNoType() {
        return OtherNoType;
    }
    public void setOtherNoType(String aOtherNoType) {
        OtherNoType = aOtherNoType;
    }
    public String getCode() {
        return Code;
    }
    public void setCode(String aCode) {
        Code = aCode;
    }
    public String getManageCom() {
        return ManageCom;
    }
    public void setManageCom(String aManageCom) {
        ManageCom = aManageCom;
    }
    public String getAgentCode() {
        return AgentCode;
    }
    public void setAgentCode(String aAgentCode) {
        AgentCode = aAgentCode;
    }
    public String getReqCom() {
        return ReqCom;
    }
    public void setReqCom(String aReqCom) {
        ReqCom = aReqCom;
    }
    public String getReqOperator() {
        return ReqOperator;
    }
    public void setReqOperator(String aReqOperator) {
        ReqOperator = aReqOperator;
    }
    public String getExeCom() {
        return ExeCom;
    }
    public void setExeCom(String aExeCom) {
        ExeCom = aExeCom;
    }
    public String getExeOperator() {
        return ExeOperator;
    }
    public void setExeOperator(String aExeOperator) {
        ExeOperator = aExeOperator;
    }
    public String getPrtType() {
        return PrtType;
    }
    public void setPrtType(String aPrtType) {
        PrtType = aPrtType;
    }
    public String getStateFlag() {
        return StateFlag;
    }
    public void setStateFlag(String aStateFlag) {
        StateFlag = aStateFlag;
    }
    public String getMakeDate() {
        return MakeDate;
    }
    public void setMakeDate(String aMakeDate) {
        MakeDate = aMakeDate;
    }
    public String getMakeTime() {
        return MakeTime;
    }
    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }
    public String getDoneDate() {
        return DoneDate;
    }
    public void setDoneDate(String aDoneDate) {
        DoneDate = aDoneDate;
    }
    public String getDoneTime() {
        return DoneTime;
    }
    public void setDoneTime(String aDoneTime) {
        DoneTime = aDoneTime;
    }
    public String getStandbyFlag1() {
        return StandbyFlag1;
    }
    public void setStandbyFlag1(String aStandbyFlag1) {
        StandbyFlag1 = aStandbyFlag1;
    }
    public String getStandbyFlag2() {
        return StandbyFlag2;
    }
    public void setStandbyFlag2(String aStandbyFlag2) {
        StandbyFlag2 = aStandbyFlag2;
    }
    public String getOldPrtSeq() {
        return OldPrtSeq;
    }
    public void setOldPrtSeq(String aOldPrtSeq) {
        OldPrtSeq = aOldPrtSeq;
    }
    public String getPatchFlag() {
        return PatchFlag;
    }
    public void setPatchFlag(String aPatchFlag) {
        PatchFlag = aPatchFlag;
    }
    public String getStandbyFlag3() {
        return StandbyFlag3;
    }
    public void setStandbyFlag3(String aStandbyFlag3) {
        StandbyFlag3 = aStandbyFlag3;
    }
    public String getStandbyFlag4() {
        return StandbyFlag4;
    }
    public void setStandbyFlag4(String aStandbyFlag4) {
        StandbyFlag4 = aStandbyFlag4;
    }
    public String getForMakeDate() {
        return ForMakeDate;
    }
    public void setForMakeDate(String aForMakeDate) {
        ForMakeDate = aForMakeDate;
    }
    public String getForMakeTime() {
        return ForMakeTime;
    }
    public void setForMakeTime(String aForMakeTime) {
        ForMakeTime = aForMakeTime;
    }
    public String getActMakeDate() {
        return ActMakeDate;
    }
    public void setActMakeDate(String aActMakeDate) {
        ActMakeDate = aActMakeDate;
    }
    public String getActMakeTime() {
        return ActMakeTime;
    }
    public void setActMakeTime(String aActMakeTime) {
        ActMakeTime = aActMakeTime;
    }
    public String getRemark() {
        return Remark;
    }
    public void setRemark(String aRemark) {
        Remark = aRemark;
    }
    public String getStandbyFlag5() {
        return StandbyFlag5;
    }
    public void setStandbyFlag5(String aStandbyFlag5) {
        StandbyFlag5 = aStandbyFlag5;
    }
    public String getStandbyFlag6() {
        return StandbyFlag6;
    }
    public void setStandbyFlag6(String aStandbyFlag6) {
        StandbyFlag6 = aStandbyFlag6;
    }
    public String getStandbyFlag7() {
        return StandbyFlag7;
    }
    public void setStandbyFlag7(String aStandbyFlag7) {
        StandbyFlag7 = aStandbyFlag7;
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("PRTManagerID") ) {
            return 0;
        }
        if( strFieldName.equals("ShardingID") ) {
            return 1;
        }
        if( strFieldName.equals("PrtSeq") ) {
            return 2;
        }
        if( strFieldName.equals("OtherNo") ) {
            return 3;
        }
        if( strFieldName.equals("OtherNoType") ) {
            return 4;
        }
        if( strFieldName.equals("Code") ) {
            return 5;
        }
        if( strFieldName.equals("ManageCom") ) {
            return 6;
        }
        if( strFieldName.equals("AgentCode") ) {
            return 7;
        }
        if( strFieldName.equals("ReqCom") ) {
            return 8;
        }
        if( strFieldName.equals("ReqOperator") ) {
            return 9;
        }
        if( strFieldName.equals("ExeCom") ) {
            return 10;
        }
        if( strFieldName.equals("ExeOperator") ) {
            return 11;
        }
        if( strFieldName.equals("PrtType") ) {
            return 12;
        }
        if( strFieldName.equals("StateFlag") ) {
            return 13;
        }
        if( strFieldName.equals("MakeDate") ) {
            return 14;
        }
        if( strFieldName.equals("MakeTime") ) {
            return 15;
        }
        if( strFieldName.equals("DoneDate") ) {
            return 16;
        }
        if( strFieldName.equals("DoneTime") ) {
            return 17;
        }
        if( strFieldName.equals("StandbyFlag1") ) {
            return 18;
        }
        if( strFieldName.equals("StandbyFlag2") ) {
            return 19;
        }
        if( strFieldName.equals("OldPrtSeq") ) {
            return 20;
        }
        if( strFieldName.equals("PatchFlag") ) {
            return 21;
        }
        if( strFieldName.equals("StandbyFlag3") ) {
            return 22;
        }
        if( strFieldName.equals("StandbyFlag4") ) {
            return 23;
        }
        if( strFieldName.equals("ForMakeDate") ) {
            return 24;
        }
        if( strFieldName.equals("ForMakeTime") ) {
            return 25;
        }
        if( strFieldName.equals("ActMakeDate") ) {
            return 26;
        }
        if( strFieldName.equals("ActMakeTime") ) {
            return 27;
        }
        if( strFieldName.equals("Remark") ) {
            return 28;
        }
        if( strFieldName.equals("StandbyFlag5") ) {
            return 29;
        }
        if( strFieldName.equals("StandbyFlag6") ) {
            return 30;
        }
        if( strFieldName.equals("StandbyFlag7") ) {
            return 31;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "PRTManagerID";
                break;
            case 1:
                strFieldName = "ShardingID";
                break;
            case 2:
                strFieldName = "PrtSeq";
                break;
            case 3:
                strFieldName = "OtherNo";
                break;
            case 4:
                strFieldName = "OtherNoType";
                break;
            case 5:
                strFieldName = "Code";
                break;
            case 6:
                strFieldName = "ManageCom";
                break;
            case 7:
                strFieldName = "AgentCode";
                break;
            case 8:
                strFieldName = "ReqCom";
                break;
            case 9:
                strFieldName = "ReqOperator";
                break;
            case 10:
                strFieldName = "ExeCom";
                break;
            case 11:
                strFieldName = "ExeOperator";
                break;
            case 12:
                strFieldName = "PrtType";
                break;
            case 13:
                strFieldName = "StateFlag";
                break;
            case 14:
                strFieldName = "MakeDate";
                break;
            case 15:
                strFieldName = "MakeTime";
                break;
            case 16:
                strFieldName = "DoneDate";
                break;
            case 17:
                strFieldName = "DoneTime";
                break;
            case 18:
                strFieldName = "StandbyFlag1";
                break;
            case 19:
                strFieldName = "StandbyFlag2";
                break;
            case 20:
                strFieldName = "OldPrtSeq";
                break;
            case 21:
                strFieldName = "PatchFlag";
                break;
            case 22:
                strFieldName = "StandbyFlag3";
                break;
            case 23:
                strFieldName = "StandbyFlag4";
                break;
            case 24:
                strFieldName = "ForMakeDate";
                break;
            case 25:
                strFieldName = "ForMakeTime";
                break;
            case 26:
                strFieldName = "ActMakeDate";
                break;
            case 27:
                strFieldName = "ActMakeTime";
                break;
            case 28:
                strFieldName = "Remark";
                break;
            case 29:
                strFieldName = "StandbyFlag5";
                break;
            case 30:
                strFieldName = "StandbyFlag6";
                break;
            case 31:
                strFieldName = "StandbyFlag7";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "PRTMANAGERID":
                return Schema.TYPE_LONG;
            case "SHARDINGID":
                return Schema.TYPE_STRING;
            case "PRTSEQ":
                return Schema.TYPE_STRING;
            case "OTHERNO":
                return Schema.TYPE_STRING;
            case "OTHERNOTYPE":
                return Schema.TYPE_STRING;
            case "CODE":
                return Schema.TYPE_STRING;
            case "MANAGECOM":
                return Schema.TYPE_STRING;
            case "AGENTCODE":
                return Schema.TYPE_STRING;
            case "REQCOM":
                return Schema.TYPE_STRING;
            case "REQOPERATOR":
                return Schema.TYPE_STRING;
            case "EXECOM":
                return Schema.TYPE_STRING;
            case "EXEOPERATOR":
                return Schema.TYPE_STRING;
            case "PRTTYPE":
                return Schema.TYPE_STRING;
            case "STATEFLAG":
                return Schema.TYPE_STRING;
            case "MAKEDATE":
                return Schema.TYPE_STRING;
            case "MAKETIME":
                return Schema.TYPE_STRING;
            case "DONEDATE":
                return Schema.TYPE_STRING;
            case "DONETIME":
                return Schema.TYPE_STRING;
            case "STANDBYFLAG1":
                return Schema.TYPE_STRING;
            case "STANDBYFLAG2":
                return Schema.TYPE_STRING;
            case "OLDPRTSEQ":
                return Schema.TYPE_STRING;
            case "PATCHFLAG":
                return Schema.TYPE_STRING;
            case "STANDBYFLAG3":
                return Schema.TYPE_STRING;
            case "STANDBYFLAG4":
                return Schema.TYPE_STRING;
            case "FORMAKEDATE":
                return Schema.TYPE_STRING;
            case "FORMAKETIME":
                return Schema.TYPE_STRING;
            case "ACTMAKEDATE":
                return Schema.TYPE_STRING;
            case "ACTMAKETIME":
                return Schema.TYPE_STRING;
            case "REMARK":
                return Schema.TYPE_STRING;
            case "STANDBYFLAG5":
                return Schema.TYPE_STRING;
            case "STANDBYFLAG6":
                return Schema.TYPE_STRING;
            case "STANDBYFLAG7":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_LONG;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_STRING;
            case 6:
                return Schema.TYPE_STRING;
            case 7:
                return Schema.TYPE_STRING;
            case 8:
                return Schema.TYPE_STRING;
            case 9:
                return Schema.TYPE_STRING;
            case 10:
                return Schema.TYPE_STRING;
            case 11:
                return Schema.TYPE_STRING;
            case 12:
                return Schema.TYPE_STRING;
            case 13:
                return Schema.TYPE_STRING;
            case 14:
                return Schema.TYPE_STRING;
            case 15:
                return Schema.TYPE_STRING;
            case 16:
                return Schema.TYPE_STRING;
            case 17:
                return Schema.TYPE_STRING;
            case 18:
                return Schema.TYPE_STRING;
            case 19:
                return Schema.TYPE_STRING;
            case 20:
                return Schema.TYPE_STRING;
            case 21:
                return Schema.TYPE_STRING;
            case 22:
                return Schema.TYPE_STRING;
            case 23:
                return Schema.TYPE_STRING;
            case 24:
                return Schema.TYPE_STRING;
            case 25:
                return Schema.TYPE_STRING;
            case 26:
                return Schema.TYPE_STRING;
            case 27:
                return Schema.TYPE_STRING;
            case 28:
                return Schema.TYPE_STRING;
            case 29:
                return Schema.TYPE_STRING;
            case 30:
                return Schema.TYPE_STRING;
            case 31:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("PRTManagerID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PRTManagerID));
        }
        if (FCode.equalsIgnoreCase("ShardingID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ShardingID));
        }
        if (FCode.equalsIgnoreCase("PrtSeq")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PrtSeq));
        }
        if (FCode.equalsIgnoreCase("OtherNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OtherNo));
        }
        if (FCode.equalsIgnoreCase("OtherNoType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OtherNoType));
        }
        if (FCode.equalsIgnoreCase("Code")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Code));
        }
        if (FCode.equalsIgnoreCase("ManageCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
        }
        if (FCode.equalsIgnoreCase("AgentCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentCode));
        }
        if (FCode.equalsIgnoreCase("ReqCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ReqCom));
        }
        if (FCode.equalsIgnoreCase("ReqOperator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ReqOperator));
        }
        if (FCode.equalsIgnoreCase("ExeCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ExeCom));
        }
        if (FCode.equalsIgnoreCase("ExeOperator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ExeOperator));
        }
        if (FCode.equalsIgnoreCase("PrtType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PrtType));
        }
        if (FCode.equalsIgnoreCase("StateFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StateFlag));
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeDate));
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equalsIgnoreCase("DoneDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DoneDate));
        }
        if (FCode.equalsIgnoreCase("DoneTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DoneTime));
        }
        if (FCode.equalsIgnoreCase("StandbyFlag1")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StandbyFlag1));
        }
        if (FCode.equalsIgnoreCase("StandbyFlag2")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StandbyFlag2));
        }
        if (FCode.equalsIgnoreCase("OldPrtSeq")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OldPrtSeq));
        }
        if (FCode.equalsIgnoreCase("PatchFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PatchFlag));
        }
        if (FCode.equalsIgnoreCase("StandbyFlag3")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StandbyFlag3));
        }
        if (FCode.equalsIgnoreCase("StandbyFlag4")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StandbyFlag4));
        }
        if (FCode.equalsIgnoreCase("ForMakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ForMakeDate));
        }
        if (FCode.equalsIgnoreCase("ForMakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ForMakeTime));
        }
        if (FCode.equalsIgnoreCase("ActMakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ActMakeDate));
        }
        if (FCode.equalsIgnoreCase("ActMakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ActMakeTime));
        }
        if (FCode.equalsIgnoreCase("Remark")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Remark));
        }
        if (FCode.equalsIgnoreCase("StandbyFlag5")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StandbyFlag5));
        }
        if (FCode.equalsIgnoreCase("StandbyFlag6")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StandbyFlag6));
        }
        if (FCode.equalsIgnoreCase("StandbyFlag7")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StandbyFlag7));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = String.valueOf(PRTManagerID);
                break;
            case 1:
                strFieldValue = String.valueOf(ShardingID);
                break;
            case 2:
                strFieldValue = String.valueOf(PrtSeq);
                break;
            case 3:
                strFieldValue = String.valueOf(OtherNo);
                break;
            case 4:
                strFieldValue = String.valueOf(OtherNoType);
                break;
            case 5:
                strFieldValue = String.valueOf(Code);
                break;
            case 6:
                strFieldValue = String.valueOf(ManageCom);
                break;
            case 7:
                strFieldValue = String.valueOf(AgentCode);
                break;
            case 8:
                strFieldValue = String.valueOf(ReqCom);
                break;
            case 9:
                strFieldValue = String.valueOf(ReqOperator);
                break;
            case 10:
                strFieldValue = String.valueOf(ExeCom);
                break;
            case 11:
                strFieldValue = String.valueOf(ExeOperator);
                break;
            case 12:
                strFieldValue = String.valueOf(PrtType);
                break;
            case 13:
                strFieldValue = String.valueOf(StateFlag);
                break;
            case 14:
                strFieldValue = String.valueOf(MakeDate);
                break;
            case 15:
                strFieldValue = String.valueOf(MakeTime);
                break;
            case 16:
                strFieldValue = String.valueOf(DoneDate);
                break;
            case 17:
                strFieldValue = String.valueOf(DoneTime);
                break;
            case 18:
                strFieldValue = String.valueOf(StandbyFlag1);
                break;
            case 19:
                strFieldValue = String.valueOf(StandbyFlag2);
                break;
            case 20:
                strFieldValue = String.valueOf(OldPrtSeq);
                break;
            case 21:
                strFieldValue = String.valueOf(PatchFlag);
                break;
            case 22:
                strFieldValue = String.valueOf(StandbyFlag3);
                break;
            case 23:
                strFieldValue = String.valueOf(StandbyFlag4);
                break;
            case 24:
                strFieldValue = String.valueOf(ForMakeDate);
                break;
            case 25:
                strFieldValue = String.valueOf(ForMakeTime);
                break;
            case 26:
                strFieldValue = String.valueOf(ActMakeDate);
                break;
            case 27:
                strFieldValue = String.valueOf(ActMakeTime);
                break;
            case 28:
                strFieldValue = String.valueOf(Remark);
                break;
            case 29:
                strFieldValue = String.valueOf(StandbyFlag5);
                break;
            case 30:
                strFieldValue = String.valueOf(StandbyFlag6);
                break;
            case 31:
                strFieldValue = String.valueOf(StandbyFlag7);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("PRTManagerID")) {
            if( FValue != null && !FValue.equals("")) {
                PRTManagerID = new Long(FValue).longValue();
            }
        }
        if (FCode.equalsIgnoreCase("ShardingID")) {
            if( FValue != null && !FValue.equals(""))
            {
                ShardingID = FValue.trim();
            }
            else
                ShardingID = null;
        }
        if (FCode.equalsIgnoreCase("PrtSeq")) {
            if( FValue != null && !FValue.equals(""))
            {
                PrtSeq = FValue.trim();
            }
            else
                PrtSeq = null;
        }
        if (FCode.equalsIgnoreCase("OtherNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                OtherNo = FValue.trim();
            }
            else
                OtherNo = null;
        }
        if (FCode.equalsIgnoreCase("OtherNoType")) {
            if( FValue != null && !FValue.equals(""))
            {
                OtherNoType = FValue.trim();
            }
            else
                OtherNoType = null;
        }
        if (FCode.equalsIgnoreCase("Code")) {
            if( FValue != null && !FValue.equals(""))
            {
                Code = FValue.trim();
            }
            else
                Code = null;
        }
        if (FCode.equalsIgnoreCase("ManageCom")) {
            if( FValue != null && !FValue.equals(""))
            {
                ManageCom = FValue.trim();
            }
            else
                ManageCom = null;
        }
        if (FCode.equalsIgnoreCase("AgentCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                AgentCode = FValue.trim();
            }
            else
                AgentCode = null;
        }
        if (FCode.equalsIgnoreCase("ReqCom")) {
            if( FValue != null && !FValue.equals(""))
            {
                ReqCom = FValue.trim();
            }
            else
                ReqCom = null;
        }
        if (FCode.equalsIgnoreCase("ReqOperator")) {
            if( FValue != null && !FValue.equals(""))
            {
                ReqOperator = FValue.trim();
            }
            else
                ReqOperator = null;
        }
        if (FCode.equalsIgnoreCase("ExeCom")) {
            if( FValue != null && !FValue.equals(""))
            {
                ExeCom = FValue.trim();
            }
            else
                ExeCom = null;
        }
        if (FCode.equalsIgnoreCase("ExeOperator")) {
            if( FValue != null && !FValue.equals(""))
            {
                ExeOperator = FValue.trim();
            }
            else
                ExeOperator = null;
        }
        if (FCode.equalsIgnoreCase("PrtType")) {
            if( FValue != null && !FValue.equals(""))
            {
                PrtType = FValue.trim();
            }
            else
                PrtType = null;
        }
        if (FCode.equalsIgnoreCase("StateFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                StateFlag = FValue.trim();
            }
            else
                StateFlag = null;
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeDate = FValue.trim();
            }
            else
                MakeDate = null;
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
                MakeTime = null;
        }
        if (FCode.equalsIgnoreCase("DoneDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                DoneDate = FValue.trim();
            }
            else
                DoneDate = null;
        }
        if (FCode.equalsIgnoreCase("DoneTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                DoneTime = FValue.trim();
            }
            else
                DoneTime = null;
        }
        if (FCode.equalsIgnoreCase("StandbyFlag1")) {
            if( FValue != null && !FValue.equals(""))
            {
                StandbyFlag1 = FValue.trim();
            }
            else
                StandbyFlag1 = null;
        }
        if (FCode.equalsIgnoreCase("StandbyFlag2")) {
            if( FValue != null && !FValue.equals(""))
            {
                StandbyFlag2 = FValue.trim();
            }
            else
                StandbyFlag2 = null;
        }
        if (FCode.equalsIgnoreCase("OldPrtSeq")) {
            if( FValue != null && !FValue.equals(""))
            {
                OldPrtSeq = FValue.trim();
            }
            else
                OldPrtSeq = null;
        }
        if (FCode.equalsIgnoreCase("PatchFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                PatchFlag = FValue.trim();
            }
            else
                PatchFlag = null;
        }
        if (FCode.equalsIgnoreCase("StandbyFlag3")) {
            if( FValue != null && !FValue.equals(""))
            {
                StandbyFlag3 = FValue.trim();
            }
            else
                StandbyFlag3 = null;
        }
        if (FCode.equalsIgnoreCase("StandbyFlag4")) {
            if( FValue != null && !FValue.equals(""))
            {
                StandbyFlag4 = FValue.trim();
            }
            else
                StandbyFlag4 = null;
        }
        if (FCode.equalsIgnoreCase("ForMakeDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                ForMakeDate = FValue.trim();
            }
            else
                ForMakeDate = null;
        }
        if (FCode.equalsIgnoreCase("ForMakeTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                ForMakeTime = FValue.trim();
            }
            else
                ForMakeTime = null;
        }
        if (FCode.equalsIgnoreCase("ActMakeDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                ActMakeDate = FValue.trim();
            }
            else
                ActMakeDate = null;
        }
        if (FCode.equalsIgnoreCase("ActMakeTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                ActMakeTime = FValue.trim();
            }
            else
                ActMakeTime = null;
        }
        if (FCode.equalsIgnoreCase("Remark")) {
            if( FValue != null && !FValue.equals(""))
            {
                Remark = FValue.trim();
            }
            else
                Remark = null;
        }
        if (FCode.equalsIgnoreCase("StandbyFlag5")) {
            if( FValue != null && !FValue.equals(""))
            {
                StandbyFlag5 = FValue.trim();
            }
            else
                StandbyFlag5 = null;
        }
        if (FCode.equalsIgnoreCase("StandbyFlag6")) {
            if( FValue != null && !FValue.equals(""))
            {
                StandbyFlag6 = FValue.trim();
            }
            else
                StandbyFlag6 = null;
        }
        if (FCode.equalsIgnoreCase("StandbyFlag7")) {
            if( FValue != null && !FValue.equals(""))
            {
                StandbyFlag7 = FValue.trim();
            }
            else
                StandbyFlag7 = null;
        }
        return true;
    }


    public String toString() {
    return "LOPRTManagerPojo [" +
            "PRTManagerID="+PRTManagerID +
            ", ShardingID="+ShardingID +
            ", PrtSeq="+PrtSeq +
            ", OtherNo="+OtherNo +
            ", OtherNoType="+OtherNoType +
            ", Code="+Code +
            ", ManageCom="+ManageCom +
            ", AgentCode="+AgentCode +
            ", ReqCom="+ReqCom +
            ", ReqOperator="+ReqOperator +
            ", ExeCom="+ExeCom +
            ", ExeOperator="+ExeOperator +
            ", PrtType="+PrtType +
            ", StateFlag="+StateFlag +
            ", MakeDate="+MakeDate +
            ", MakeTime="+MakeTime +
            ", DoneDate="+DoneDate +
            ", DoneTime="+DoneTime +
            ", StandbyFlag1="+StandbyFlag1 +
            ", StandbyFlag2="+StandbyFlag2 +
            ", OldPrtSeq="+OldPrtSeq +
            ", PatchFlag="+PatchFlag +
            ", StandbyFlag3="+StandbyFlag3 +
            ", StandbyFlag4="+StandbyFlag4 +
            ", ForMakeDate="+ForMakeDate +
            ", ForMakeTime="+ForMakeTime +
            ", ActMakeDate="+ActMakeDate +
            ", ActMakeTime="+ActMakeTime +
            ", Remark="+Remark +
            ", StandbyFlag5="+StandbyFlag5 +
            ", StandbyFlag6="+StandbyFlag6 +
            ", StandbyFlag7="+StandbyFlag7 +"]";
    }
}
