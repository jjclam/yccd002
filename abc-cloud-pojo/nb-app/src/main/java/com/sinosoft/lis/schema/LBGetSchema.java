/**
 * Copyright (c) 2017 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import com.sinosoft.lis.db.LBGetDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.*;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

/**
 * <p>ClassName: LBGetSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2017</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2017-10-21
 */
public class LBGetSchema implements Schema, Cloneable {
    // @Field
    /** Id */
    private long GetID;
    /** Shardingid */
    private String ShardingID;
    /** 批单号 */
    private String EdorNo;
    /** 集体合同号码 */
    private String GrpContNo;
    /** 合同号码 */
    private String ContNo;
    /** 保单险种号码 */
    private String PolNo;
    /** 责任编码 */
    private String DutyCode;
    /** 给付责任编码 */
    private String GetDutyCode;
    /** 给付责任类型 */
    private String GetDutyKind;
    /** 被保人客户号码 */
    private String InsuredNo;
    /** 领取方式 */
    private String GetMode;
    /** 起付限 */
    private double GetLimit;
    /** 赔付比例 */
    private double GetRate;
    /** 催付标记 */
    private String UrgeGetFlag;
    /** 生存意外给付标志 */
    private String LiveGetType;
    /** 递增率 */
    private double AddRate;
    /** 默认申请标志 */
    private String CanGet;
    /** 是否和账户相关 */
    private String NeedAcc;
    /** 是否是账户结清后才能申请 */
    private String NeedCancelAcc;
    /** 标准给付金额 */
    private double StandMoney;
    /** 实际给付金额 */
    private double ActuGet;
    /** 已领金额 */
    private double SumMoney;
    /** 领取间隔 */
    private int GetIntv;
    /** 领至日期 */
    private Date GettoDate;
    /** 起领日期 */
    private Date GetStartDate;
    /** 止领日期 */
    private Date GetEndDate;
    /** 结算日期 */
    private Date BalaDate;
    /** 状态 */
    private String State;
    /** 管理机构 */
    private String ManageCom;
    /** 操作员 */
    private String Operator;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 最后一次修改时间 */
    private String ModifyTime;
    /** 最后一次修改日期 */
    private Date ModifyDate;
    /** 止领标志 */
    private String GetEndState;

    public static final int FIELDNUM = 35;    // 数据库表的字段个数

    private static String[] PK;                // 主键

    private FDate fDate = new FDate();        // 处理日期

    public CErrors mErrors;            // 错误信息

    // @Constructor
    public LBGetSchema() {
        mErrors = new CErrors();

        String[] pk = new String[1];
        pk[0] = "GetID";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException {
        LBGetSchema cloned = (LBGetSchema)super.clone();
        cloned.fDate = (FDate) fDate.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK() {
        return PK;
    }

    public long getGetID() {
        return GetID;
    }
    public void setGetID(long aGetID) {
        GetID = aGetID;
    }
    public void setGetID(String aGetID) {
        if (aGetID != null && !aGetID.equals("")) {
            GetID = new Long(aGetID).longValue();
        }
    }

    public String getShardingID() {
        return ShardingID;
    }
    public void setShardingID(String aShardingID) {
        ShardingID = aShardingID;
    }
    public String getEdorNo() {
        return EdorNo;
    }
    public void setEdorNo(String aEdorNo) {
        EdorNo = aEdorNo;
    }
    public String getGrpContNo() {
        return GrpContNo;
    }
    public void setGrpContNo(String aGrpContNo) {
        GrpContNo = aGrpContNo;
    }
    public String getContNo() {
        return ContNo;
    }
    public void setContNo(String aContNo) {
        ContNo = aContNo;
    }
    public String getPolNo() {
        return PolNo;
    }
    public void setPolNo(String aPolNo) {
        PolNo = aPolNo;
    }
    public String getDutyCode() {
        return DutyCode;
    }
    public void setDutyCode(String aDutyCode) {
        DutyCode = aDutyCode;
    }
    public String getGetDutyCode() {
        return GetDutyCode;
    }
    public void setGetDutyCode(String aGetDutyCode) {
        GetDutyCode = aGetDutyCode;
    }
    public String getGetDutyKind() {
        return GetDutyKind;
    }
    public void setGetDutyKind(String aGetDutyKind) {
        GetDutyKind = aGetDutyKind;
    }
    public String getInsuredNo() {
        return InsuredNo;
    }
    public void setInsuredNo(String aInsuredNo) {
        InsuredNo = aInsuredNo;
    }
    public String getGetMode() {
        return GetMode;
    }
    public void setGetMode(String aGetMode) {
        GetMode = aGetMode;
    }
    public double getGetLimit() {
        return GetLimit;
    }
    public void setGetLimit(double aGetLimit) {
        GetLimit = aGetLimit;
    }
    public void setGetLimit(String aGetLimit) {
        if (aGetLimit != null && !aGetLimit.equals("")) {
            Double tDouble = new Double(aGetLimit);
            double d = tDouble.doubleValue();
            GetLimit = d;
        }
    }

    public double getGetRate() {
        return GetRate;
    }
    public void setGetRate(double aGetRate) {
        GetRate = aGetRate;
    }
    public void setGetRate(String aGetRate) {
        if (aGetRate != null && !aGetRate.equals("")) {
            Double tDouble = new Double(aGetRate);
            double d = tDouble.doubleValue();
            GetRate = d;
        }
    }

    public String getUrgeGetFlag() {
        return UrgeGetFlag;
    }
    public void setUrgeGetFlag(String aUrgeGetFlag) {
        UrgeGetFlag = aUrgeGetFlag;
    }
    public String getLiveGetType() {
        return LiveGetType;
    }
    public void setLiveGetType(String aLiveGetType) {
        LiveGetType = aLiveGetType;
    }
    public double getAddRate() {
        return AddRate;
    }
    public void setAddRate(double aAddRate) {
        AddRate = aAddRate;
    }
    public void setAddRate(String aAddRate) {
        if (aAddRate != null && !aAddRate.equals("")) {
            Double tDouble = new Double(aAddRate);
            double d = tDouble.doubleValue();
            AddRate = d;
        }
    }

    public String getCanGet() {
        return CanGet;
    }
    public void setCanGet(String aCanGet) {
        CanGet = aCanGet;
    }
    public String getNeedAcc() {
        return NeedAcc;
    }
    public void setNeedAcc(String aNeedAcc) {
        NeedAcc = aNeedAcc;
    }
    public String getNeedCancelAcc() {
        return NeedCancelAcc;
    }
    public void setNeedCancelAcc(String aNeedCancelAcc) {
        NeedCancelAcc = aNeedCancelAcc;
    }
    public double getStandMoney() {
        return StandMoney;
    }
    public void setStandMoney(double aStandMoney) {
        StandMoney = aStandMoney;
    }
    public void setStandMoney(String aStandMoney) {
        if (aStandMoney != null && !aStandMoney.equals("")) {
            Double tDouble = new Double(aStandMoney);
            double d = tDouble.doubleValue();
            StandMoney = d;
        }
    }

    public double getActuGet() {
        return ActuGet;
    }
    public void setActuGet(double aActuGet) {
        ActuGet = aActuGet;
    }
    public void setActuGet(String aActuGet) {
        if (aActuGet != null && !aActuGet.equals("")) {
            Double tDouble = new Double(aActuGet);
            double d = tDouble.doubleValue();
            ActuGet = d;
        }
    }

    public double getSumMoney() {
        return SumMoney;
    }
    public void setSumMoney(double aSumMoney) {
        SumMoney = aSumMoney;
    }
    public void setSumMoney(String aSumMoney) {
        if (aSumMoney != null && !aSumMoney.equals("")) {
            Double tDouble = new Double(aSumMoney);
            double d = tDouble.doubleValue();
            SumMoney = d;
        }
    }

    public int getGetIntv() {
        return GetIntv;
    }
    public void setGetIntv(int aGetIntv) {
        GetIntv = aGetIntv;
    }
    public void setGetIntv(String aGetIntv) {
        if (aGetIntv != null && !aGetIntv.equals("")) {
            Integer tInteger = new Integer(aGetIntv);
            int i = tInteger.intValue();
            GetIntv = i;
        }
    }

    public String getGettoDate() {
        if(GettoDate != null) {
            return fDate.getString(GettoDate);
        } else {
            return null;
        }
    }
    public void setGettoDate(Date aGettoDate) {
        GettoDate = aGettoDate;
    }
    public void setGettoDate(String aGettoDate) {
        if (aGettoDate != null && !aGettoDate.equals("")) {
            GettoDate = fDate.getDate(aGettoDate);
        } else
            GettoDate = null;
    }

    public String getGetStartDate() {
        if(GetStartDate != null) {
            return fDate.getString(GetStartDate);
        } else {
            return null;
        }
    }
    public void setGetStartDate(Date aGetStartDate) {
        GetStartDate = aGetStartDate;
    }
    public void setGetStartDate(String aGetStartDate) {
        if (aGetStartDate != null && !aGetStartDate.equals("")) {
            GetStartDate = fDate.getDate(aGetStartDate);
        } else
            GetStartDate = null;
    }

    public String getGetEndDate() {
        if(GetEndDate != null) {
            return fDate.getString(GetEndDate);
        } else {
            return null;
        }
    }
    public void setGetEndDate(Date aGetEndDate) {
        GetEndDate = aGetEndDate;
    }
    public void setGetEndDate(String aGetEndDate) {
        if (aGetEndDate != null && !aGetEndDate.equals("")) {
            GetEndDate = fDate.getDate(aGetEndDate);
        } else
            GetEndDate = null;
    }

    public String getBalaDate() {
        if(BalaDate != null) {
            return fDate.getString(BalaDate);
        } else {
            return null;
        }
    }
    public void setBalaDate(Date aBalaDate) {
        BalaDate = aBalaDate;
    }
    public void setBalaDate(String aBalaDate) {
        if (aBalaDate != null && !aBalaDate.equals("")) {
            BalaDate = fDate.getDate(aBalaDate);
        } else
            BalaDate = null;
    }

    public String getState() {
        return State;
    }
    public void setState(String aState) {
        State = aState;
    }
    public String getManageCom() {
        return ManageCom;
    }
    public void setManageCom(String aManageCom) {
        ManageCom = aManageCom;
    }
    public String getOperator() {
        return Operator;
    }
    public void setOperator(String aOperator) {
        Operator = aOperator;
    }
    public String getMakeDate() {
        if(MakeDate != null) {
            return fDate.getString(MakeDate);
        } else {
            return null;
        }
    }
    public void setMakeDate(Date aMakeDate) {
        MakeDate = aMakeDate;
    }
    public void setMakeDate(String aMakeDate) {
        if (aMakeDate != null && !aMakeDate.equals("")) {
            MakeDate = fDate.getDate(aMakeDate);
        } else
            MakeDate = null;
    }

    public String getMakeTime() {
        return MakeTime;
    }
    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }
    public String getModifyTime() {
        return ModifyTime;
    }
    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }
    public String getModifyDate() {
        if(ModifyDate != null) {
            return fDate.getString(ModifyDate);
        } else {
            return null;
        }
    }
    public void setModifyDate(Date aModifyDate) {
        ModifyDate = aModifyDate;
    }
    public void setModifyDate(String aModifyDate) {
        if (aModifyDate != null && !aModifyDate.equals("")) {
            ModifyDate = fDate.getDate(aModifyDate);
        } else
            ModifyDate = null;
    }

    public String getGetEndState() {
        return GetEndState;
    }
    public void setGetEndState(String aGetEndState) {
        GetEndState = aGetEndState;
    }

    /**
    * 使用另外一个 LBGetSchema 对象给 Schema 赋值
    * @param: aLBGetSchema LBGetSchema
    **/
    public void setSchema(LBGetSchema aLBGetSchema) {
        this.GetID = aLBGetSchema.getGetID();
        this.ShardingID = aLBGetSchema.getShardingID();
        this.EdorNo = aLBGetSchema.getEdorNo();
        this.GrpContNo = aLBGetSchema.getGrpContNo();
        this.ContNo = aLBGetSchema.getContNo();
        this.PolNo = aLBGetSchema.getPolNo();
        this.DutyCode = aLBGetSchema.getDutyCode();
        this.GetDutyCode = aLBGetSchema.getGetDutyCode();
        this.GetDutyKind = aLBGetSchema.getGetDutyKind();
        this.InsuredNo = aLBGetSchema.getInsuredNo();
        this.GetMode = aLBGetSchema.getGetMode();
        this.GetLimit = aLBGetSchema.getGetLimit();
        this.GetRate = aLBGetSchema.getGetRate();
        this.UrgeGetFlag = aLBGetSchema.getUrgeGetFlag();
        this.LiveGetType = aLBGetSchema.getLiveGetType();
        this.AddRate = aLBGetSchema.getAddRate();
        this.CanGet = aLBGetSchema.getCanGet();
        this.NeedAcc = aLBGetSchema.getNeedAcc();
        this.NeedCancelAcc = aLBGetSchema.getNeedCancelAcc();
        this.StandMoney = aLBGetSchema.getStandMoney();
        this.ActuGet = aLBGetSchema.getActuGet();
        this.SumMoney = aLBGetSchema.getSumMoney();
        this.GetIntv = aLBGetSchema.getGetIntv();
        this.GettoDate = fDate.getDate( aLBGetSchema.getGettoDate());
        this.GetStartDate = fDate.getDate( aLBGetSchema.getGetStartDate());
        this.GetEndDate = fDate.getDate( aLBGetSchema.getGetEndDate());
        this.BalaDate = fDate.getDate( aLBGetSchema.getBalaDate());
        this.State = aLBGetSchema.getState();
        this.ManageCom = aLBGetSchema.getManageCom();
        this.Operator = aLBGetSchema.getOperator();
        this.MakeDate = fDate.getDate( aLBGetSchema.getMakeDate());
        this.MakeTime = aLBGetSchema.getMakeTime();
        this.ModifyTime = aLBGetSchema.getModifyTime();
        this.ModifyDate = fDate.getDate( aLBGetSchema.getModifyDate());
        this.GetEndState = aLBGetSchema.getGetEndState();
    }

    /**
    * 使用 ResultSet 中的第 i 行给 Schema 赋值
    * @param: rs ResultSet
    * @param: i int
    * @return: boolean
    **/
    public boolean setSchema(ResultSet rs,int i) {
        try {
            //rs.absolute(i);        // 非滚动游标
            this.GetID = rs.getLong("GetID");
            if( rs.getString("ShardingID") == null )
                this.ShardingID = null;
            else
                this.ShardingID = rs.getString("ShardingID").trim();

            if( rs.getString("EdorNo") == null )
                this.EdorNo = null;
            else
                this.EdorNo = rs.getString("EdorNo").trim();

            if( rs.getString("GrpContNo") == null )
                this.GrpContNo = null;
            else
                this.GrpContNo = rs.getString("GrpContNo").trim();

            if( rs.getString("ContNo") == null )
                this.ContNo = null;
            else
                this.ContNo = rs.getString("ContNo").trim();

            if( rs.getString("PolNo") == null )
                this.PolNo = null;
            else
                this.PolNo = rs.getString("PolNo").trim();

            if( rs.getString("DutyCode") == null )
                this.DutyCode = null;
            else
                this.DutyCode = rs.getString("DutyCode").trim();

            if( rs.getString("GetDutyCode") == null )
                this.GetDutyCode = null;
            else
                this.GetDutyCode = rs.getString("GetDutyCode").trim();

            if( rs.getString("GetDutyKind") == null )
                this.GetDutyKind = null;
            else
                this.GetDutyKind = rs.getString("GetDutyKind").trim();

            if( rs.getString("InsuredNo") == null )
                this.InsuredNo = null;
            else
                this.InsuredNo = rs.getString("InsuredNo").trim();

            if( rs.getString("GetMode") == null )
                this.GetMode = null;
            else
                this.GetMode = rs.getString("GetMode").trim();

            this.GetLimit = rs.getDouble("GetLimit");
            this.GetRate = rs.getDouble("GetRate");
            if( rs.getString("UrgeGetFlag") == null )
                this.UrgeGetFlag = null;
            else
                this.UrgeGetFlag = rs.getString("UrgeGetFlag").trim();

            if( rs.getString("LiveGetType") == null )
                this.LiveGetType = null;
            else
                this.LiveGetType = rs.getString("LiveGetType").trim();

            this.AddRate = rs.getDouble("AddRate");
            if( rs.getString("CanGet") == null )
                this.CanGet = null;
            else
                this.CanGet = rs.getString("CanGet").trim();

            if( rs.getString("NeedAcc") == null )
                this.NeedAcc = null;
            else
                this.NeedAcc = rs.getString("NeedAcc").trim();

            if( rs.getString("NeedCancelAcc") == null )
                this.NeedCancelAcc = null;
            else
                this.NeedCancelAcc = rs.getString("NeedCancelAcc").trim();

            this.StandMoney = rs.getDouble("StandMoney");
            this.ActuGet = rs.getDouble("ActuGet");
            this.SumMoney = rs.getDouble("SumMoney");
            this.GetIntv = rs.getInt("GetIntv");
            this.GettoDate = rs.getDate("GettoDate");
            this.GetStartDate = rs.getDate("GetStartDate");
            this.GetEndDate = rs.getDate("GetEndDate");
            this.BalaDate = rs.getDate("BalaDate");
            if( rs.getString("State") == null )
                this.State = null;
            else
                this.State = rs.getString("State").trim();

            if( rs.getString("ManageCom") == null )
                this.ManageCom = null;
            else
                this.ManageCom = rs.getString("ManageCom").trim();

            if( rs.getString("Operator") == null )
                this.Operator = null;
            else
                this.Operator = rs.getString("Operator").trim();

            this.MakeDate = rs.getDate("MakeDate");
            if( rs.getString("MakeTime") == null )
                this.MakeTime = null;
            else
                this.MakeTime = rs.getString("MakeTime").trim();

            if( rs.getString("ModifyTime") == null )
                this.ModifyTime = null;
            else
                this.ModifyTime = rs.getString("ModifyTime").trim();

            this.ModifyDate = rs.getDate("ModifyDate");
            if( rs.getString("GetEndState") == null )
                this.GetEndState = null;
            else
                this.GetEndState = rs.getString("GetEndState").trim();

        }
        catch(SQLException sqle) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LBGetSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = ExceptionUtils.exceptionToString(sqle);
            this.mErrors .addOneError(tError);
            return false;
        }
        return true;
    }

    public LBGetSchema getSchema() {
        LBGetSchema aLBGetSchema = new LBGetSchema();
        aLBGetSchema.setSchema(this);
        return aLBGetSchema;
    }

    public LBGetDB getDB() {
        LBGetDB aDBOper = new LBGetDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
    * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLBGet描述/A>表字段
    * @return: String 返回打包后字符串
    **/
    public String encode() {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(ChgData.chgData(GetID));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ShardingID)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(EdorNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(GrpContNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ContNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(PolNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(DutyCode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(GetDutyCode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(GetDutyKind)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(InsuredNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(GetMode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(GetLimit));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(GetRate));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(UrgeGetFlag)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(LiveGetType)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(AddRate));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(CanGet)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(NeedAcc)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(NeedCancelAcc)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(StandMoney));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(ActuGet));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(SumMoney));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(GetIntv));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( GettoDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( GetStartDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( GetEndDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( BalaDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(State)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ManageCom)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ModifyTime)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(GetEndState));
        return strReturn.toString();
    }

    /**
    * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLBGet>历史记账凭证主表信息</A>表字段
    * @param: strMessage String 包含一条纪录数据的字符串
    * @return: boolean
    **/
    public boolean decode(String strMessage) {
        try {
            GetID = new Long(ChgData.chgNumericStr(StrTool.getStr(strMessage,1, SysConst.PACKAGESPILTER))).longValue();
            ShardingID = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
            EdorNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
            GrpContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
            ContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
            PolNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
            DutyCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
            GetDutyCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
            GetDutyKind = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
            InsuredNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
            GetMode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
            GetLimit = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,12, SysConst.PACKAGESPILTER))).doubleValue();
            GetRate = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,13, SysConst.PACKAGESPILTER))).doubleValue();
            UrgeGetFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER );
            LiveGetType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15, SysConst.PACKAGESPILTER );
            AddRate = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,16, SysConst.PACKAGESPILTER))).doubleValue();
            CanGet = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17, SysConst.PACKAGESPILTER );
            NeedAcc = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18, SysConst.PACKAGESPILTER );
            NeedCancelAcc = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19, SysConst.PACKAGESPILTER );
            StandMoney = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,20, SysConst.PACKAGESPILTER))).doubleValue();
            ActuGet = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,21, SysConst.PACKAGESPILTER))).doubleValue();
            SumMoney = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,22, SysConst.PACKAGESPILTER))).doubleValue();
            GetIntv = new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,23, SysConst.PACKAGESPILTER))).intValue();
            GettoDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 24, SysConst.PACKAGESPILTER));
            GetStartDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 25, SysConst.PACKAGESPILTER));
            GetEndDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 26, SysConst.PACKAGESPILTER));
            BalaDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 27, SysConst.PACKAGESPILTER));
            State = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 28, SysConst.PACKAGESPILTER );
            ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 29, SysConst.PACKAGESPILTER );
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 30, SysConst.PACKAGESPILTER );
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 31, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 32, SysConst.PACKAGESPILTER );
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 33, SysConst.PACKAGESPILTER );
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 34, SysConst.PACKAGESPILTER));
            GetEndState = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 35, SysConst.PACKAGESPILTER );
        }
        catch(NumberFormatException ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LBGetSchema";
            tError.functionName = "decode";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            return false;
        }
        return true;
    }

    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("GetID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GetID));
        }
        if (FCode.equalsIgnoreCase("ShardingID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ShardingID));
        }
        if (FCode.equalsIgnoreCase("EdorNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(EdorNo));
        }
        if (FCode.equalsIgnoreCase("GrpContNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GrpContNo));
        }
        if (FCode.equalsIgnoreCase("ContNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ContNo));
        }
        if (FCode.equalsIgnoreCase("PolNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PolNo));
        }
        if (FCode.equalsIgnoreCase("DutyCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DutyCode));
        }
        if (FCode.equalsIgnoreCase("GetDutyCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GetDutyCode));
        }
        if (FCode.equalsIgnoreCase("GetDutyKind")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GetDutyKind));
        }
        if (FCode.equalsIgnoreCase("InsuredNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InsuredNo));
        }
        if (FCode.equalsIgnoreCase("GetMode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GetMode));
        }
        if (FCode.equalsIgnoreCase("GetLimit")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GetLimit));
        }
        if (FCode.equalsIgnoreCase("GetRate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GetRate));
        }
        if (FCode.equalsIgnoreCase("UrgeGetFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(UrgeGetFlag));
        }
        if (FCode.equalsIgnoreCase("LiveGetType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(LiveGetType));
        }
        if (FCode.equalsIgnoreCase("AddRate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AddRate));
        }
        if (FCode.equalsIgnoreCase("CanGet")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CanGet));
        }
        if (FCode.equalsIgnoreCase("NeedAcc")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(NeedAcc));
        }
        if (FCode.equalsIgnoreCase("NeedCancelAcc")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(NeedCancelAcc));
        }
        if (FCode.equalsIgnoreCase("StandMoney")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StandMoney));
        }
        if (FCode.equalsIgnoreCase("ActuGet")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ActuGet));
        }
        if (FCode.equalsIgnoreCase("SumMoney")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SumMoney));
        }
        if (FCode.equalsIgnoreCase("GetIntv")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GetIntv));
        }
        if (FCode.equalsIgnoreCase("GettoDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getGettoDate()));
        }
        if (FCode.equalsIgnoreCase("GetStartDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getGetStartDate()));
        }
        if (FCode.equalsIgnoreCase("GetEndDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getGetEndDate()));
        }
        if (FCode.equalsIgnoreCase("BalaDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getBalaDate()));
        }
        if (FCode.equalsIgnoreCase("State")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(State));
        }
        if (FCode.equalsIgnoreCase("ManageCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
        }
        if (FCode.equalsIgnoreCase("GetEndState")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GetEndState));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = String.valueOf(GetID);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(ShardingID);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(EdorNo);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(GrpContNo);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(ContNo);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(PolNo);
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(DutyCode);
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(GetDutyCode);
                break;
            case 8:
                strFieldValue = StrTool.GBKToUnicode(GetDutyKind);
                break;
            case 9:
                strFieldValue = StrTool.GBKToUnicode(InsuredNo);
                break;
            case 10:
                strFieldValue = StrTool.GBKToUnicode(GetMode);
                break;
            case 11:
                strFieldValue = String.valueOf(GetLimit);
                break;
            case 12:
                strFieldValue = String.valueOf(GetRate);
                break;
            case 13:
                strFieldValue = StrTool.GBKToUnicode(UrgeGetFlag);
                break;
            case 14:
                strFieldValue = StrTool.GBKToUnicode(LiveGetType);
                break;
            case 15:
                strFieldValue = String.valueOf(AddRate);
                break;
            case 16:
                strFieldValue = StrTool.GBKToUnicode(CanGet);
                break;
            case 17:
                strFieldValue = StrTool.GBKToUnicode(NeedAcc);
                break;
            case 18:
                strFieldValue = StrTool.GBKToUnicode(NeedCancelAcc);
                break;
            case 19:
                strFieldValue = String.valueOf(StandMoney);
                break;
            case 20:
                strFieldValue = String.valueOf(ActuGet);
                break;
            case 21:
                strFieldValue = String.valueOf(SumMoney);
                break;
            case 22:
                strFieldValue = String.valueOf(GetIntv);
                break;
            case 23:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getGettoDate()));
                break;
            case 24:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getGetStartDate()));
                break;
            case 25:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getGetEndDate()));
                break;
            case 26:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getBalaDate()));
                break;
            case 27:
                strFieldValue = StrTool.GBKToUnicode(State);
                break;
            case 28:
                strFieldValue = StrTool.GBKToUnicode(ManageCom);
                break;
            case 29:
                strFieldValue = StrTool.GBKToUnicode(Operator);
                break;
            case 30:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
                break;
            case 31:
                strFieldValue = StrTool.GBKToUnicode(MakeTime);
                break;
            case 32:
                strFieldValue = StrTool.GBKToUnicode(ModifyTime);
                break;
            case 33:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
                break;
            case 34:
                strFieldValue = StrTool.GBKToUnicode(GetEndState);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("GetID")) {
            if( FValue != null && !FValue.equals("")) {
                GetID = new Long(FValue).longValue();
            }
        }
        if (FCode.equalsIgnoreCase("ShardingID")) {
            if( FValue != null && !FValue.equals(""))
            {
                ShardingID = FValue.trim();
            }
            else
                ShardingID = null;
        }
        if (FCode.equalsIgnoreCase("EdorNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                EdorNo = FValue.trim();
            }
            else
                EdorNo = null;
        }
        if (FCode.equalsIgnoreCase("GrpContNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                GrpContNo = FValue.trim();
            }
            else
                GrpContNo = null;
        }
        if (FCode.equalsIgnoreCase("ContNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                ContNo = FValue.trim();
            }
            else
                ContNo = null;
        }
        if (FCode.equalsIgnoreCase("PolNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                PolNo = FValue.trim();
            }
            else
                PolNo = null;
        }
        if (FCode.equalsIgnoreCase("DutyCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                DutyCode = FValue.trim();
            }
            else
                DutyCode = null;
        }
        if (FCode.equalsIgnoreCase("GetDutyCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                GetDutyCode = FValue.trim();
            }
            else
                GetDutyCode = null;
        }
        if (FCode.equalsIgnoreCase("GetDutyKind")) {
            if( FValue != null && !FValue.equals(""))
            {
                GetDutyKind = FValue.trim();
            }
            else
                GetDutyKind = null;
        }
        if (FCode.equalsIgnoreCase("InsuredNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                InsuredNo = FValue.trim();
            }
            else
                InsuredNo = null;
        }
        if (FCode.equalsIgnoreCase("GetMode")) {
            if( FValue != null && !FValue.equals(""))
            {
                GetMode = FValue.trim();
            }
            else
                GetMode = null;
        }
        if (FCode.equalsIgnoreCase("GetLimit")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                GetLimit = d;
            }
        }
        if (FCode.equalsIgnoreCase("GetRate")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                GetRate = d;
            }
        }
        if (FCode.equalsIgnoreCase("UrgeGetFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                UrgeGetFlag = FValue.trim();
            }
            else
                UrgeGetFlag = null;
        }
        if (FCode.equalsIgnoreCase("LiveGetType")) {
            if( FValue != null && !FValue.equals(""))
            {
                LiveGetType = FValue.trim();
            }
            else
                LiveGetType = null;
        }
        if (FCode.equalsIgnoreCase("AddRate")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                AddRate = d;
            }
        }
        if (FCode.equalsIgnoreCase("CanGet")) {
            if( FValue != null && !FValue.equals(""))
            {
                CanGet = FValue.trim();
            }
            else
                CanGet = null;
        }
        if (FCode.equalsIgnoreCase("NeedAcc")) {
            if( FValue != null && !FValue.equals(""))
            {
                NeedAcc = FValue.trim();
            }
            else
                NeedAcc = null;
        }
        if (FCode.equalsIgnoreCase("NeedCancelAcc")) {
            if( FValue != null && !FValue.equals(""))
            {
                NeedCancelAcc = FValue.trim();
            }
            else
                NeedCancelAcc = null;
        }
        if (FCode.equalsIgnoreCase("StandMoney")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                StandMoney = d;
            }
        }
        if (FCode.equalsIgnoreCase("ActuGet")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                ActuGet = d;
            }
        }
        if (FCode.equalsIgnoreCase("SumMoney")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                SumMoney = d;
            }
        }
        if (FCode.equalsIgnoreCase("GetIntv")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                GetIntv = i;
            }
        }
        if (FCode.equalsIgnoreCase("GettoDate")) {
            if(FValue != null && !FValue.equals("")) {
                GettoDate = fDate.getDate( FValue );
            }
            else
                GettoDate = null;
        }
        if (FCode.equalsIgnoreCase("GetStartDate")) {
            if(FValue != null && !FValue.equals("")) {
                GetStartDate = fDate.getDate( FValue );
            }
            else
                GetStartDate = null;
        }
        if (FCode.equalsIgnoreCase("GetEndDate")) {
            if(FValue != null && !FValue.equals("")) {
                GetEndDate = fDate.getDate( FValue );
            }
            else
                GetEndDate = null;
        }
        if (FCode.equalsIgnoreCase("BalaDate")) {
            if(FValue != null && !FValue.equals("")) {
                BalaDate = fDate.getDate( FValue );
            }
            else
                BalaDate = null;
        }
        if (FCode.equalsIgnoreCase("State")) {
            if( FValue != null && !FValue.equals(""))
            {
                State = FValue.trim();
            }
            else
                State = null;
        }
        if (FCode.equalsIgnoreCase("ManageCom")) {
            if( FValue != null && !FValue.equals(""))
            {
                ManageCom = FValue.trim();
            }
            else
                ManageCom = null;
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            if( FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
                Operator = null;
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if(FValue != null && !FValue.equals("")) {
                MakeDate = fDate.getDate( FValue );
            }
            else
                MakeDate = null;
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
                MakeTime = null;
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
                ModifyTime = null;
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if(FValue != null && !FValue.equals("")) {
                ModifyDate = fDate.getDate( FValue );
            }
            else
                ModifyDate = null;
        }
        if (FCode.equalsIgnoreCase("GetEndState")) {
            if( FValue != null && !FValue.equals(""))
            {
                GetEndState = FValue.trim();
            }
            else
                GetEndState = null;
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject) return true;
        if (otherObject == null) return false;
        if (getClass() != otherObject.getClass()) return false;
        LBGetSchema other = (LBGetSchema)otherObject;
        return
            GetID == other.getGetID()
            && ShardingID.equals(other.getShardingID())
            && EdorNo.equals(other.getEdorNo())
            && GrpContNo.equals(other.getGrpContNo())
            && ContNo.equals(other.getContNo())
            && PolNo.equals(other.getPolNo())
            && DutyCode.equals(other.getDutyCode())
            && GetDutyCode.equals(other.getGetDutyCode())
            && GetDutyKind.equals(other.getGetDutyKind())
            && InsuredNo.equals(other.getInsuredNo())
            && GetMode.equals(other.getGetMode())
            && GetLimit == other.getGetLimit()
            && GetRate == other.getGetRate()
            && UrgeGetFlag.equals(other.getUrgeGetFlag())
            && LiveGetType.equals(other.getLiveGetType())
            && AddRate == other.getAddRate()
            && CanGet.equals(other.getCanGet())
            && NeedAcc.equals(other.getNeedAcc())
            && NeedCancelAcc.equals(other.getNeedCancelAcc())
            && StandMoney == other.getStandMoney()
            && ActuGet == other.getActuGet()
            && SumMoney == other.getSumMoney()
            && GetIntv == other.getGetIntv()
            && fDate.getString(GettoDate).equals(other.getGettoDate())
            && fDate.getString(GetStartDate).equals(other.getGetStartDate())
            && fDate.getString(GetEndDate).equals(other.getGetEndDate())
            && fDate.getString(BalaDate).equals(other.getBalaDate())
            && State.equals(other.getState())
            && ManageCom.equals(other.getManageCom())
            && Operator.equals(other.getOperator())
            && fDate.getString(MakeDate).equals(other.getMakeDate())
            && MakeTime.equals(other.getMakeTime())
            && ModifyTime.equals(other.getModifyTime())
            && fDate.getString(ModifyDate).equals(other.getModifyDate())
            && GetEndState.equals(other.getGetEndState());
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("GetID") ) {
            return 0;
        }
        if( strFieldName.equals("ShardingID") ) {
            return 1;
        }
        if( strFieldName.equals("EdorNo") ) {
            return 2;
        }
        if( strFieldName.equals("GrpContNo") ) {
            return 3;
        }
        if( strFieldName.equals("ContNo") ) {
            return 4;
        }
        if( strFieldName.equals("PolNo") ) {
            return 5;
        }
        if( strFieldName.equals("DutyCode") ) {
            return 6;
        }
        if( strFieldName.equals("GetDutyCode") ) {
            return 7;
        }
        if( strFieldName.equals("GetDutyKind") ) {
            return 8;
        }
        if( strFieldName.equals("InsuredNo") ) {
            return 9;
        }
        if( strFieldName.equals("GetMode") ) {
            return 10;
        }
        if( strFieldName.equals("GetLimit") ) {
            return 11;
        }
        if( strFieldName.equals("GetRate") ) {
            return 12;
        }
        if( strFieldName.equals("UrgeGetFlag") ) {
            return 13;
        }
        if( strFieldName.equals("LiveGetType") ) {
            return 14;
        }
        if( strFieldName.equals("AddRate") ) {
            return 15;
        }
        if( strFieldName.equals("CanGet") ) {
            return 16;
        }
        if( strFieldName.equals("NeedAcc") ) {
            return 17;
        }
        if( strFieldName.equals("NeedCancelAcc") ) {
            return 18;
        }
        if( strFieldName.equals("StandMoney") ) {
            return 19;
        }
        if( strFieldName.equals("ActuGet") ) {
            return 20;
        }
        if( strFieldName.equals("SumMoney") ) {
            return 21;
        }
        if( strFieldName.equals("GetIntv") ) {
            return 22;
        }
        if( strFieldName.equals("GettoDate") ) {
            return 23;
        }
        if( strFieldName.equals("GetStartDate") ) {
            return 24;
        }
        if( strFieldName.equals("GetEndDate") ) {
            return 25;
        }
        if( strFieldName.equals("BalaDate") ) {
            return 26;
        }
        if( strFieldName.equals("State") ) {
            return 27;
        }
        if( strFieldName.equals("ManageCom") ) {
            return 28;
        }
        if( strFieldName.equals("Operator") ) {
            return 29;
        }
        if( strFieldName.equals("MakeDate") ) {
            return 30;
        }
        if( strFieldName.equals("MakeTime") ) {
            return 31;
        }
        if( strFieldName.equals("ModifyTime") ) {
            return 32;
        }
        if( strFieldName.equals("ModifyDate") ) {
            return 33;
        }
        if( strFieldName.equals("GetEndState") ) {
            return 34;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "GetID";
                break;
            case 1:
                strFieldName = "ShardingID";
                break;
            case 2:
                strFieldName = "EdorNo";
                break;
            case 3:
                strFieldName = "GrpContNo";
                break;
            case 4:
                strFieldName = "ContNo";
                break;
            case 5:
                strFieldName = "PolNo";
                break;
            case 6:
                strFieldName = "DutyCode";
                break;
            case 7:
                strFieldName = "GetDutyCode";
                break;
            case 8:
                strFieldName = "GetDutyKind";
                break;
            case 9:
                strFieldName = "InsuredNo";
                break;
            case 10:
                strFieldName = "GetMode";
                break;
            case 11:
                strFieldName = "GetLimit";
                break;
            case 12:
                strFieldName = "GetRate";
                break;
            case 13:
                strFieldName = "UrgeGetFlag";
                break;
            case 14:
                strFieldName = "LiveGetType";
                break;
            case 15:
                strFieldName = "AddRate";
                break;
            case 16:
                strFieldName = "CanGet";
                break;
            case 17:
                strFieldName = "NeedAcc";
                break;
            case 18:
                strFieldName = "NeedCancelAcc";
                break;
            case 19:
                strFieldName = "StandMoney";
                break;
            case 20:
                strFieldName = "ActuGet";
                break;
            case 21:
                strFieldName = "SumMoney";
                break;
            case 22:
                strFieldName = "GetIntv";
                break;
            case 23:
                strFieldName = "GettoDate";
                break;
            case 24:
                strFieldName = "GetStartDate";
                break;
            case 25:
                strFieldName = "GetEndDate";
                break;
            case 26:
                strFieldName = "BalaDate";
                break;
            case 27:
                strFieldName = "State";
                break;
            case 28:
                strFieldName = "ManageCom";
                break;
            case 29:
                strFieldName = "Operator";
                break;
            case 30:
                strFieldName = "MakeDate";
                break;
            case 31:
                strFieldName = "MakeTime";
                break;
            case 32:
                strFieldName = "ModifyTime";
                break;
            case 33:
                strFieldName = "ModifyDate";
                break;
            case 34:
                strFieldName = "GetEndState";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "GETID":
                return Schema.TYPE_LONG;
            case "SHARDINGID":
                return Schema.TYPE_STRING;
            case "EDORNO":
                return Schema.TYPE_STRING;
            case "GRPCONTNO":
                return Schema.TYPE_STRING;
            case "CONTNO":
                return Schema.TYPE_STRING;
            case "POLNO":
                return Schema.TYPE_STRING;
            case "DUTYCODE":
                return Schema.TYPE_STRING;
            case "GETDUTYCODE":
                return Schema.TYPE_STRING;
            case "GETDUTYKIND":
                return Schema.TYPE_STRING;
            case "INSUREDNO":
                return Schema.TYPE_STRING;
            case "GETMODE":
                return Schema.TYPE_STRING;
            case "GETLIMIT":
                return Schema.TYPE_DOUBLE;
            case "GETRATE":
                return Schema.TYPE_DOUBLE;
            case "URGEGETFLAG":
                return Schema.TYPE_STRING;
            case "LIVEGETTYPE":
                return Schema.TYPE_STRING;
            case "ADDRATE":
                return Schema.TYPE_DOUBLE;
            case "CANGET":
                return Schema.TYPE_STRING;
            case "NEEDACC":
                return Schema.TYPE_STRING;
            case "NEEDCANCELACC":
                return Schema.TYPE_STRING;
            case "STANDMONEY":
                return Schema.TYPE_DOUBLE;
            case "ACTUGET":
                return Schema.TYPE_DOUBLE;
            case "SUMMONEY":
                return Schema.TYPE_DOUBLE;
            case "GETINTV":
                return Schema.TYPE_INT;
            case "GETTODATE":
                return Schema.TYPE_DATE;
            case "GETSTARTDATE":
                return Schema.TYPE_DATE;
            case "GETENDDATE":
                return Schema.TYPE_DATE;
            case "BALADATE":
                return Schema.TYPE_DATE;
            case "STATE":
                return Schema.TYPE_STRING;
            case "MANAGECOM":
                return Schema.TYPE_STRING;
            case "OPERATOR":
                return Schema.TYPE_STRING;
            case "MAKEDATE":
                return Schema.TYPE_DATE;
            case "MAKETIME":
                return Schema.TYPE_STRING;
            case "MODIFYTIME":
                return Schema.TYPE_STRING;
            case "MODIFYDATE":
                return Schema.TYPE_DATE;
            case "GETENDSTATE":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_LONG;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_STRING;
            case 6:
                return Schema.TYPE_STRING;
            case 7:
                return Schema.TYPE_STRING;
            case 8:
                return Schema.TYPE_STRING;
            case 9:
                return Schema.TYPE_STRING;
            case 10:
                return Schema.TYPE_STRING;
            case 11:
                return Schema.TYPE_DOUBLE;
            case 12:
                return Schema.TYPE_DOUBLE;
            case 13:
                return Schema.TYPE_STRING;
            case 14:
                return Schema.TYPE_STRING;
            case 15:
                return Schema.TYPE_DOUBLE;
            case 16:
                return Schema.TYPE_STRING;
            case 17:
                return Schema.TYPE_STRING;
            case 18:
                return Schema.TYPE_STRING;
            case 19:
                return Schema.TYPE_DOUBLE;
            case 20:
                return Schema.TYPE_DOUBLE;
            case 21:
                return Schema.TYPE_DOUBLE;
            case 22:
                return Schema.TYPE_INT;
            case 23:
                return Schema.TYPE_DATE;
            case 24:
                return Schema.TYPE_DATE;
            case 25:
                return Schema.TYPE_DATE;
            case 26:
                return Schema.TYPE_DATE;
            case 27:
                return Schema.TYPE_STRING;
            case 28:
                return Schema.TYPE_STRING;
            case 29:
                return Schema.TYPE_STRING;
            case 30:
                return Schema.TYPE_DATE;
            case 31:
                return Schema.TYPE_STRING;
            case 32:
                return Schema.TYPE_STRING;
            case 33:
                return Schema.TYPE_DATE;
            case 34:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
}
