/**
 * Copyright (c) 2017 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.entity;

import com.sinosoft.utility.Pojo;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;

import java.io.Serializable;

/**
 * <p>ClassName: LMRiskRnewPojo </p>
 * <p>Copyright: Copyright (c) 2017</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2017-10-17
 */
public class LMRiskRnewPojo implements Pojo,Serializable {
    // @Field
    /** 险种编码 */
    private String RiskCode; 
    /** 险种版本 */
    private String RiskVer; 
    /** 险种名称 */
    private String RiskName; 
    /** 续保类型 */
    private String RnewType; 
    /** 宽限期 */
    private int GracePeriod; 
    /** 宽限期单位 */
    private String GracePeriodUnit; 
    /** 宽限日期计算方式 */
    private String GraceDateCalMode; 
    /** 续保最大年龄 */
    private int MaxAge; 
    /** 续保限制 */
    private String RnewLmt; 
    /** 原保单终止日算法 */
    private int EndDateCalMode; 
    /** 续保确认日算法 */
    private int ComfirmDateCalMode; 
    /** 保证续保标记 */
    private String AssuRnewFlag; 
    /** 保证续保要求的续保次数 */
    private int RnewTimes; 


    public static final int FIELDNUM = 13;    // 数据库表的字段个数
    public String getRiskCode() {
        return RiskCode;
    }
    public void setRiskCode(String aRiskCode) {
        RiskCode = aRiskCode;
    }
    public String getRiskVer() {
        return RiskVer;
    }
    public void setRiskVer(String aRiskVer) {
        RiskVer = aRiskVer;
    }
    public String getRiskName() {
        return RiskName;
    }
    public void setRiskName(String aRiskName) {
        RiskName = aRiskName;
    }
    public String getRnewType() {
        return RnewType;
    }
    public void setRnewType(String aRnewType) {
        RnewType = aRnewType;
    }
    public int getGracePeriod() {
        return GracePeriod;
    }
    public void setGracePeriod(int aGracePeriod) {
        GracePeriod = aGracePeriod;
    }
    public void setGracePeriod(String aGracePeriod) {
        if (aGracePeriod != null && !aGracePeriod.equals("")) {
            Integer tInteger = new Integer(aGracePeriod);
            int i = tInteger.intValue();
            GracePeriod = i;
        }
    }

    public String getGracePeriodUnit() {
        return GracePeriodUnit;
    }
    public void setGracePeriodUnit(String aGracePeriodUnit) {
        GracePeriodUnit = aGracePeriodUnit;
    }
    public String getGraceDateCalMode() {
        return GraceDateCalMode;
    }
    public void setGraceDateCalMode(String aGraceDateCalMode) {
        GraceDateCalMode = aGraceDateCalMode;
    }
    public int getMaxAge() {
        return MaxAge;
    }
    public void setMaxAge(int aMaxAge) {
        MaxAge = aMaxAge;
    }
    public void setMaxAge(String aMaxAge) {
        if (aMaxAge != null && !aMaxAge.equals("")) {
            Integer tInteger = new Integer(aMaxAge);
            int i = tInteger.intValue();
            MaxAge = i;
        }
    }

    public String getRnewLmt() {
        return RnewLmt;
    }
    public void setRnewLmt(String aRnewLmt) {
        RnewLmt = aRnewLmt;
    }
    public int getEndDateCalMode() {
        return EndDateCalMode;
    }
    public void setEndDateCalMode(int aEndDateCalMode) {
        EndDateCalMode = aEndDateCalMode;
    }
    public void setEndDateCalMode(String aEndDateCalMode) {
        if (aEndDateCalMode != null && !aEndDateCalMode.equals("")) {
            Integer tInteger = new Integer(aEndDateCalMode);
            int i = tInteger.intValue();
            EndDateCalMode = i;
        }
    }

    public int getComfirmDateCalMode() {
        return ComfirmDateCalMode;
    }
    public void setComfirmDateCalMode(int aComfirmDateCalMode) {
        ComfirmDateCalMode = aComfirmDateCalMode;
    }
    public void setComfirmDateCalMode(String aComfirmDateCalMode) {
        if (aComfirmDateCalMode != null && !aComfirmDateCalMode.equals("")) {
            Integer tInteger = new Integer(aComfirmDateCalMode);
            int i = tInteger.intValue();
            ComfirmDateCalMode = i;
        }
    }

    public String getAssuRnewFlag() {
        return AssuRnewFlag;
    }
    public void setAssuRnewFlag(String aAssuRnewFlag) {
        AssuRnewFlag = aAssuRnewFlag;
    }
    public int getRnewTimes() {
        return RnewTimes;
    }
    public void setRnewTimes(int aRnewTimes) {
        RnewTimes = aRnewTimes;
    }
    public void setRnewTimes(String aRnewTimes) {
        if (aRnewTimes != null && !aRnewTimes.equals("")) {
            Integer tInteger = new Integer(aRnewTimes);
            int i = tInteger.intValue();
            RnewTimes = i;
        }
    }


    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("RiskCode") ) {
            return 0;
        }
        if( strFieldName.equals("RiskVer") ) {
            return 1;
        }
        if( strFieldName.equals("RiskName") ) {
            return 2;
        }
        if( strFieldName.equals("RnewType") ) {
            return 3;
        }
        if( strFieldName.equals("GracePeriod") ) {
            return 4;
        }
        if( strFieldName.equals("GracePeriodUnit") ) {
            return 5;
        }
        if( strFieldName.equals("GraceDateCalMode") ) {
            return 6;
        }
        if( strFieldName.equals("MaxAge") ) {
            return 7;
        }
        if( strFieldName.equals("RnewLmt") ) {
            return 8;
        }
        if( strFieldName.equals("EndDateCalMode") ) {
            return 9;
        }
        if( strFieldName.equals("ComfirmDateCalMode") ) {
            return 10;
        }
        if( strFieldName.equals("AssuRnewFlag") ) {
            return 11;
        }
        if( strFieldName.equals("RnewTimes") ) {
            return 12;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "RiskCode";
                break;
            case 1:
                strFieldName = "RiskVer";
                break;
            case 2:
                strFieldName = "RiskName";
                break;
            case 3:
                strFieldName = "RnewType";
                break;
            case 4:
                strFieldName = "GracePeriod";
                break;
            case 5:
                strFieldName = "GracePeriodUnit";
                break;
            case 6:
                strFieldName = "GraceDateCalMode";
                break;
            case 7:
                strFieldName = "MaxAge";
                break;
            case 8:
                strFieldName = "RnewLmt";
                break;
            case 9:
                strFieldName = "EndDateCalMode";
                break;
            case 10:
                strFieldName = "ComfirmDateCalMode";
                break;
            case 11:
                strFieldName = "AssuRnewFlag";
                break;
            case 12:
                strFieldName = "RnewTimes";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "RISKCODE":
                return Schema.TYPE_STRING;
            case "RISKVER":
                return Schema.TYPE_STRING;
            case "RISKNAME":
                return Schema.TYPE_STRING;
            case "RNEWTYPE":
                return Schema.TYPE_STRING;
            case "GRACEPERIOD":
                return Schema.TYPE_INT;
            case "GRACEPERIODUNIT":
                return Schema.TYPE_STRING;
            case "GRACEDATECALMODE":
                return Schema.TYPE_STRING;
            case "MAXAGE":
                return Schema.TYPE_INT;
            case "RNEWLMT":
                return Schema.TYPE_STRING;
            case "ENDDATECALMODE":
                return Schema.TYPE_INT;
            case "COMFIRMDATECALMODE":
                return Schema.TYPE_INT;
            case "ASSURNEWFLAG":
                return Schema.TYPE_STRING;
            case "RNEWTIMES":
                return Schema.TYPE_INT;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_STRING;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_INT;
            case 5:
                return Schema.TYPE_STRING;
            case 6:
                return Schema.TYPE_STRING;
            case 7:
                return Schema.TYPE_INT;
            case 8:
                return Schema.TYPE_STRING;
            case 9:
                return Schema.TYPE_INT;
            case 10:
                return Schema.TYPE_INT;
            case 11:
                return Schema.TYPE_STRING;
            case 12:
                return Schema.TYPE_INT;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("RiskCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RiskCode));
        }
        if (FCode.equalsIgnoreCase("RiskVer")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RiskVer));
        }
        if (FCode.equalsIgnoreCase("RiskName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RiskName));
        }
        if (FCode.equalsIgnoreCase("RnewType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RnewType));
        }
        if (FCode.equalsIgnoreCase("GracePeriod")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GracePeriod));
        }
        if (FCode.equalsIgnoreCase("GracePeriodUnit")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GracePeriodUnit));
        }
        if (FCode.equalsIgnoreCase("GraceDateCalMode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GraceDateCalMode));
        }
        if (FCode.equalsIgnoreCase("MaxAge")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MaxAge));
        }
        if (FCode.equalsIgnoreCase("RnewLmt")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RnewLmt));
        }
        if (FCode.equalsIgnoreCase("EndDateCalMode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(EndDateCalMode));
        }
        if (FCode.equalsIgnoreCase("ComfirmDateCalMode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ComfirmDateCalMode));
        }
        if (FCode.equalsIgnoreCase("AssuRnewFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AssuRnewFlag));
        }
        if (FCode.equalsIgnoreCase("RnewTimes")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RnewTimes));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = String.valueOf(RiskCode);
                break;
            case 1:
                strFieldValue = String.valueOf(RiskVer);
                break;
            case 2:
                strFieldValue = String.valueOf(RiskName);
                break;
            case 3:
                strFieldValue = String.valueOf(RnewType);
                break;
            case 4:
                strFieldValue = String.valueOf(GracePeriod);
                break;
            case 5:
                strFieldValue = String.valueOf(GracePeriodUnit);
                break;
            case 6:
                strFieldValue = String.valueOf(GraceDateCalMode);
                break;
            case 7:
                strFieldValue = String.valueOf(MaxAge);
                break;
            case 8:
                strFieldValue = String.valueOf(RnewLmt);
                break;
            case 9:
                strFieldValue = String.valueOf(EndDateCalMode);
                break;
            case 10:
                strFieldValue = String.valueOf(ComfirmDateCalMode);
                break;
            case 11:
                strFieldValue = String.valueOf(AssuRnewFlag);
                break;
            case 12:
                strFieldValue = String.valueOf(RnewTimes);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("RiskCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                RiskCode = FValue.trim();
            }
            else
                RiskCode = null;
        }
        if (FCode.equalsIgnoreCase("RiskVer")) {
            if( FValue != null && !FValue.equals(""))
            {
                RiskVer = FValue.trim();
            }
            else
                RiskVer = null;
        }
        if (FCode.equalsIgnoreCase("RiskName")) {
            if( FValue != null && !FValue.equals(""))
            {
                RiskName = FValue.trim();
            }
            else
                RiskName = null;
        }
        if (FCode.equalsIgnoreCase("RnewType")) {
            if( FValue != null && !FValue.equals(""))
            {
                RnewType = FValue.trim();
            }
            else
                RnewType = null;
        }
        if (FCode.equalsIgnoreCase("GracePeriod")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                GracePeriod = i;
            }
        }
        if (FCode.equalsIgnoreCase("GracePeriodUnit")) {
            if( FValue != null && !FValue.equals(""))
            {
                GracePeriodUnit = FValue.trim();
            }
            else
                GracePeriodUnit = null;
        }
        if (FCode.equalsIgnoreCase("GraceDateCalMode")) {
            if( FValue != null && !FValue.equals(""))
            {
                GraceDateCalMode = FValue.trim();
            }
            else
                GraceDateCalMode = null;
        }
        if (FCode.equalsIgnoreCase("MaxAge")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                MaxAge = i;
            }
        }
        if (FCode.equalsIgnoreCase("RnewLmt")) {
            if( FValue != null && !FValue.equals(""))
            {
                RnewLmt = FValue.trim();
            }
            else
                RnewLmt = null;
        }
        if (FCode.equalsIgnoreCase("EndDateCalMode")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                EndDateCalMode = i;
            }
        }
        if (FCode.equalsIgnoreCase("ComfirmDateCalMode")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                ComfirmDateCalMode = i;
            }
        }
        if (FCode.equalsIgnoreCase("AssuRnewFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                AssuRnewFlag = FValue.trim();
            }
            else
                AssuRnewFlag = null;
        }
        if (FCode.equalsIgnoreCase("RnewTimes")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                RnewTimes = i;
            }
        }
        return true;
    }


    public String toString() {
    return "LMRiskRnewPojo [" +
            "RiskCode="+RiskCode +
            ", RiskVer="+RiskVer +
            ", RiskName="+RiskName +
            ", RnewType="+RnewType +
            ", GracePeriod="+GracePeriod +
            ", GracePeriodUnit="+GracePeriodUnit +
            ", GraceDateCalMode="+GraceDateCalMode +
            ", MaxAge="+MaxAge +
            ", RnewLmt="+RnewLmt +
            ", EndDateCalMode="+EndDateCalMode +
            ", ComfirmDateCalMode="+ComfirmDateCalMode +
            ", AssuRnewFlag="+AssuRnewFlag +
            ", RnewTimes="+RnewTimes +"]";
    }
}
