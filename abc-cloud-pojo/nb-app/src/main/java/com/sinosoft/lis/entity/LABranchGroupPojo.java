/**
 * Copyright (c) 2017 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.entity;

import com.sinosoft.cloud.cache.annotation.RedisPrimaryHKey;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.Pojo;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;

import java.io.Serializable;

/**
 * <p>ClassName: LABranchGroupPojo </p>
 * <p>Copyright: Copyright (c) 2017</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2017-10-17
 */
public class LABranchGroupPojo implements Pojo,Serializable {
    // @Field
    /** 展业机构代码 */
    @RedisPrimaryHKey
    private String AgentGroup; 
    /** 展业机构名称 */
    private String Name; 
    /** 管理机构 */
    private String ManageCom; 
    /** 上级展业机构代码 */
    private String UpBranch; 
    /** 展业机构外部编码 */
    private String BranchAttr; 
    /** 展业机构序列编码 */
    private String BranchSeries; 
    /** 展业类型 */
    private String BranchType; 
    /** 展业机构级别 */
    private String BranchLevel; 
    /** 展业机构管理人员 */
    private String BranchManager; 
    /** 展业机构地址编码 */
    private String BranchAddressCode; 
    /** 展业机构地址 */
    private String BranchAddress; 
    /** 展业机构电话 */
    private String BranchPhone; 
    /** 展业机构传真 */
    private String BranchFax; 
    /** 展业机构邮编 */
    private String BranchZipcode; 
    /** 成立日期 */
    private String  FoundDate;
    /** 停业日期 */
    private String  EndDate;
    /** 停业 */
    private String EndFlag; 
    /** 是否有独立的营销职场 */
    private String FieldFlag; 
    /** 标志 */
    private String State; 
    /** 展业机构管理人员姓名 */
    private String BranchManagerName; 
    /** 展业机构的上下级属性 */
    private String UpBranchAttr; 
    /** 展业机构工作类型 */
    private String BranchJobType; 
    /** 操作员 */
    private String Operator; 
    /** 入机日期 */
    private String  MakeDate;
    /** 入机时间 */
    private String MakeTime; 
    /** 最后一次修改日期 */
    private String  ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime; 
    /** 渠道 */
    private String BranchType2; 
    /** 调整日期 */
    private String  AStartDate;
    /** 参与分配否的开关 */
    private String BranchArea; 
    /** 是否参与考核标记 */
    private String AssessFlag; 
    /** 考核渠道 */
    private String AssessBranchType; 
    /** 电销中心 */
    private String DXSaleOrg; 


    public static final int FIELDNUM = 33;    // 数据库表的字段个数

    private FDate fDate = new FDate();        // 处理日期
    public String getAgentGroup() {
        return AgentGroup;
    }
    public void setAgentGroup(String aAgentGroup) {
        AgentGroup = aAgentGroup;
    }
    public String getName() {
        return Name;
    }
    public void setName(String aName) {
        Name = aName;
    }
    public String getManageCom() {
        return ManageCom;
    }
    public void setManageCom(String aManageCom) {
        ManageCom = aManageCom;
    }
    public String getUpBranch() {
        return UpBranch;
    }
    public void setUpBranch(String aUpBranch) {
        UpBranch = aUpBranch;
    }
    public String getBranchAttr() {
        return BranchAttr;
    }
    public void setBranchAttr(String aBranchAttr) {
        BranchAttr = aBranchAttr;
    }
    public String getBranchSeries() {
        return BranchSeries;
    }
    public void setBranchSeries(String aBranchSeries) {
        BranchSeries = aBranchSeries;
    }
    public String getBranchType() {
        return BranchType;
    }
    public void setBranchType(String aBranchType) {
        BranchType = aBranchType;
    }
    public String getBranchLevel() {
        return BranchLevel;
    }
    public void setBranchLevel(String aBranchLevel) {
        BranchLevel = aBranchLevel;
    }
    public String getBranchManager() {
        return BranchManager;
    }
    public void setBranchManager(String aBranchManager) {
        BranchManager = aBranchManager;
    }
    public String getBranchAddressCode() {
        return BranchAddressCode;
    }
    public void setBranchAddressCode(String aBranchAddressCode) {
        BranchAddressCode = aBranchAddressCode;
    }
    public String getBranchAddress() {
        return BranchAddress;
    }
    public void setBranchAddress(String aBranchAddress) {
        BranchAddress = aBranchAddress;
    }
    public String getBranchPhone() {
        return BranchPhone;
    }
    public void setBranchPhone(String aBranchPhone) {
        BranchPhone = aBranchPhone;
    }
    public String getBranchFax() {
        return BranchFax;
    }
    public void setBranchFax(String aBranchFax) {
        BranchFax = aBranchFax;
    }
    public String getBranchZipcode() {
        return BranchZipcode;
    }
    public void setBranchZipcode(String aBranchZipcode) {
        BranchZipcode = aBranchZipcode;
    }
    public String getFoundDate() {
        return FoundDate;
    }
    public void setFoundDate(String aFoundDate) {
        FoundDate = aFoundDate;
    }
    public String getEndDate() {
        return EndDate;
    }
    public void setEndDate(String aEndDate) {
        EndDate = aEndDate;
    }
    public String getEndFlag() {
        return EndFlag;
    }
    public void setEndFlag(String aEndFlag) {
        EndFlag = aEndFlag;
    }
    public String getFieldFlag() {
        return FieldFlag;
    }
    public void setFieldFlag(String aFieldFlag) {
        FieldFlag = aFieldFlag;
    }
    public String getState() {
        return State;
    }
    public void setState(String aState) {
        State = aState;
    }
    public String getBranchManagerName() {
        return BranchManagerName;
    }
    public void setBranchManagerName(String aBranchManagerName) {
        BranchManagerName = aBranchManagerName;
    }
    public String getUpBranchAttr() {
        return UpBranchAttr;
    }
    public void setUpBranchAttr(String aUpBranchAttr) {
        UpBranchAttr = aUpBranchAttr;
    }
    public String getBranchJobType() {
        return BranchJobType;
    }
    public void setBranchJobType(String aBranchJobType) {
        BranchJobType = aBranchJobType;
    }
    public String getOperator() {
        return Operator;
    }
    public void setOperator(String aOperator) {
        Operator = aOperator;
    }
    public String getMakeDate() {
        return MakeDate;
    }
    public void setMakeDate(String aMakeDate) {
        MakeDate = aMakeDate;
    }
    public String getMakeTime() {
        return MakeTime;
    }
    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }
    public String getModifyDate() {
        return ModifyDate;
    }
    public void setModifyDate(String aModifyDate) {
        ModifyDate = aModifyDate;
    }
    public String getModifyTime() {
        return ModifyTime;
    }
    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }
    public String getBranchType2() {
        return BranchType2;
    }
    public void setBranchType2(String aBranchType2) {
        BranchType2 = aBranchType2;
    }
    public String getAStartDate() {
        return AStartDate;
    }
    public void setAStartDate(String aAStartDate) {
        AStartDate = aAStartDate;
    }
    public String getBranchArea() {
        return BranchArea;
    }
    public void setBranchArea(String aBranchArea) {
        BranchArea = aBranchArea;
    }
    public String getAssessFlag() {
        return AssessFlag;
    }
    public void setAssessFlag(String aAssessFlag) {
        AssessFlag = aAssessFlag;
    }
    public String getAssessBranchType() {
        return AssessBranchType;
    }
    public void setAssessBranchType(String aAssessBranchType) {
        AssessBranchType = aAssessBranchType;
    }
    public String getDXSaleOrg() {
        return DXSaleOrg;
    }
    public void setDXSaleOrg(String aDXSaleOrg) {
        DXSaleOrg = aDXSaleOrg;
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("AgentGroup") ) {
            return 0;
        }
        if( strFieldName.equals("Name") ) {
            return 1;
        }
        if( strFieldName.equals("ManageCom") ) {
            return 2;
        }
        if( strFieldName.equals("UpBranch") ) {
            return 3;
        }
        if( strFieldName.equals("BranchAttr") ) {
            return 4;
        }
        if( strFieldName.equals("BranchSeries") ) {
            return 5;
        }
        if( strFieldName.equals("BranchType") ) {
            return 6;
        }
        if( strFieldName.equals("BranchLevel") ) {
            return 7;
        }
        if( strFieldName.equals("BranchManager") ) {
            return 8;
        }
        if( strFieldName.equals("BranchAddressCode") ) {
            return 9;
        }
        if( strFieldName.equals("BranchAddress") ) {
            return 10;
        }
        if( strFieldName.equals("BranchPhone") ) {
            return 11;
        }
        if( strFieldName.equals("BranchFax") ) {
            return 12;
        }
        if( strFieldName.equals("BranchZipcode") ) {
            return 13;
        }
        if( strFieldName.equals("FoundDate") ) {
            return 14;
        }
        if( strFieldName.equals("EndDate") ) {
            return 15;
        }
        if( strFieldName.equals("EndFlag") ) {
            return 16;
        }
        if( strFieldName.equals("FieldFlag") ) {
            return 17;
        }
        if( strFieldName.equals("State") ) {
            return 18;
        }
        if( strFieldName.equals("BranchManagerName") ) {
            return 19;
        }
        if( strFieldName.equals("UpBranchAttr") ) {
            return 20;
        }
        if( strFieldName.equals("BranchJobType") ) {
            return 21;
        }
        if( strFieldName.equals("Operator") ) {
            return 22;
        }
        if( strFieldName.equals("MakeDate") ) {
            return 23;
        }
        if( strFieldName.equals("MakeTime") ) {
            return 24;
        }
        if( strFieldName.equals("ModifyDate") ) {
            return 25;
        }
        if( strFieldName.equals("ModifyTime") ) {
            return 26;
        }
        if( strFieldName.equals("BranchType2") ) {
            return 27;
        }
        if( strFieldName.equals("AStartDate") ) {
            return 28;
        }
        if( strFieldName.equals("BranchArea") ) {
            return 29;
        }
        if( strFieldName.equals("AssessFlag") ) {
            return 30;
        }
        if( strFieldName.equals("AssessBranchType") ) {
            return 31;
        }
        if( strFieldName.equals("DXSaleOrg") ) {
            return 32;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "AgentGroup";
                break;
            case 1:
                strFieldName = "Name";
                break;
            case 2:
                strFieldName = "ManageCom";
                break;
            case 3:
                strFieldName = "UpBranch";
                break;
            case 4:
                strFieldName = "BranchAttr";
                break;
            case 5:
                strFieldName = "BranchSeries";
                break;
            case 6:
                strFieldName = "BranchType";
                break;
            case 7:
                strFieldName = "BranchLevel";
                break;
            case 8:
                strFieldName = "BranchManager";
                break;
            case 9:
                strFieldName = "BranchAddressCode";
                break;
            case 10:
                strFieldName = "BranchAddress";
                break;
            case 11:
                strFieldName = "BranchPhone";
                break;
            case 12:
                strFieldName = "BranchFax";
                break;
            case 13:
                strFieldName = "BranchZipcode";
                break;
            case 14:
                strFieldName = "FoundDate";
                break;
            case 15:
                strFieldName = "EndDate";
                break;
            case 16:
                strFieldName = "EndFlag";
                break;
            case 17:
                strFieldName = "FieldFlag";
                break;
            case 18:
                strFieldName = "State";
                break;
            case 19:
                strFieldName = "BranchManagerName";
                break;
            case 20:
                strFieldName = "UpBranchAttr";
                break;
            case 21:
                strFieldName = "BranchJobType";
                break;
            case 22:
                strFieldName = "Operator";
                break;
            case 23:
                strFieldName = "MakeDate";
                break;
            case 24:
                strFieldName = "MakeTime";
                break;
            case 25:
                strFieldName = "ModifyDate";
                break;
            case 26:
                strFieldName = "ModifyTime";
                break;
            case 27:
                strFieldName = "BranchType2";
                break;
            case 28:
                strFieldName = "AStartDate";
                break;
            case 29:
                strFieldName = "BranchArea";
                break;
            case 30:
                strFieldName = "AssessFlag";
                break;
            case 31:
                strFieldName = "AssessBranchType";
                break;
            case 32:
                strFieldName = "DXSaleOrg";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "AGENTGROUP":
                return Schema.TYPE_STRING;
            case "NAME":
                return Schema.TYPE_STRING;
            case "MANAGECOM":
                return Schema.TYPE_STRING;
            case "UPBRANCH":
                return Schema.TYPE_STRING;
            case "BRANCHATTR":
                return Schema.TYPE_STRING;
            case "BRANCHSERIES":
                return Schema.TYPE_STRING;
            case "BRANCHTYPE":
                return Schema.TYPE_STRING;
            case "BRANCHLEVEL":
                return Schema.TYPE_STRING;
            case "BRANCHMANAGER":
                return Schema.TYPE_STRING;
            case "BRANCHADDRESSCODE":
                return Schema.TYPE_STRING;
            case "BRANCHADDRESS":
                return Schema.TYPE_STRING;
            case "BRANCHPHONE":
                return Schema.TYPE_STRING;
            case "BRANCHFAX":
                return Schema.TYPE_STRING;
            case "BRANCHZIPCODE":
                return Schema.TYPE_STRING;
            case "FOUNDDATE":
                return Schema.TYPE_STRING;
            case "ENDDATE":
                return Schema.TYPE_STRING;
            case "ENDFLAG":
                return Schema.TYPE_STRING;
            case "FIELDFLAG":
                return Schema.TYPE_STRING;
            case "STATE":
                return Schema.TYPE_STRING;
            case "BRANCHMANAGERNAME":
                return Schema.TYPE_STRING;
            case "UPBRANCHATTR":
                return Schema.TYPE_STRING;
            case "BRANCHJOBTYPE":
                return Schema.TYPE_STRING;
            case "OPERATOR":
                return Schema.TYPE_STRING;
            case "MAKEDATE":
                return Schema.TYPE_STRING;
            case "MAKETIME":
                return Schema.TYPE_STRING;
            case "MODIFYDATE":
                return Schema.TYPE_STRING;
            case "MODIFYTIME":
                return Schema.TYPE_STRING;
            case "BRANCHTYPE2":
                return Schema.TYPE_STRING;
            case "ASTARTDATE":
                return Schema.TYPE_STRING;
            case "BRANCHAREA":
                return Schema.TYPE_STRING;
            case "ASSESSFLAG":
                return Schema.TYPE_STRING;
            case "ASSESSBRANCHTYPE":
                return Schema.TYPE_STRING;
            case "DXSALEORG":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_STRING;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_STRING;
            case 6:
                return Schema.TYPE_STRING;
            case 7:
                return Schema.TYPE_STRING;
            case 8:
                return Schema.TYPE_STRING;
            case 9:
                return Schema.TYPE_STRING;
            case 10:
                return Schema.TYPE_STRING;
            case 11:
                return Schema.TYPE_STRING;
            case 12:
                return Schema.TYPE_STRING;
            case 13:
                return Schema.TYPE_STRING;
            case 14:
                return Schema.TYPE_STRING;
            case 15:
                return Schema.TYPE_STRING;
            case 16:
                return Schema.TYPE_STRING;
            case 17:
                return Schema.TYPE_STRING;
            case 18:
                return Schema.TYPE_STRING;
            case 19:
                return Schema.TYPE_STRING;
            case 20:
                return Schema.TYPE_STRING;
            case 21:
                return Schema.TYPE_STRING;
            case 22:
                return Schema.TYPE_STRING;
            case 23:
                return Schema.TYPE_STRING;
            case 24:
                return Schema.TYPE_STRING;
            case 25:
                return Schema.TYPE_STRING;
            case 26:
                return Schema.TYPE_STRING;
            case 27:
                return Schema.TYPE_STRING;
            case 28:
                return Schema.TYPE_STRING;
            case 29:
                return Schema.TYPE_STRING;
            case 30:
                return Schema.TYPE_STRING;
            case 31:
                return Schema.TYPE_STRING;
            case 32:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("AgentGroup")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentGroup));
        }
        if (FCode.equalsIgnoreCase("Name")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Name));
        }
        if (FCode.equalsIgnoreCase("ManageCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
        }
        if (FCode.equalsIgnoreCase("UpBranch")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(UpBranch));
        }
        if (FCode.equalsIgnoreCase("BranchAttr")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BranchAttr));
        }
        if (FCode.equalsIgnoreCase("BranchSeries")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BranchSeries));
        }
        if (FCode.equalsIgnoreCase("BranchType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BranchType));
        }
        if (FCode.equalsIgnoreCase("BranchLevel")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BranchLevel));
        }
        if (FCode.equalsIgnoreCase("BranchManager")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BranchManager));
        }
        if (FCode.equalsIgnoreCase("BranchAddressCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BranchAddressCode));
        }
        if (FCode.equalsIgnoreCase("BranchAddress")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BranchAddress));
        }
        if (FCode.equalsIgnoreCase("BranchPhone")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BranchPhone));
        }
        if (FCode.equalsIgnoreCase("BranchFax")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BranchFax));
        }
        if (FCode.equalsIgnoreCase("BranchZipcode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BranchZipcode));
        }
        if (FCode.equalsIgnoreCase("FoundDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FoundDate));
        }
        if (FCode.equalsIgnoreCase("EndDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(EndDate));
        }
        if (FCode.equalsIgnoreCase("EndFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(EndFlag));
        }
        if (FCode.equalsIgnoreCase("FieldFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FieldFlag));
        }
        if (FCode.equalsIgnoreCase("State")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(State));
        }
        if (FCode.equalsIgnoreCase("BranchManagerName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BranchManagerName));
        }
        if (FCode.equalsIgnoreCase("UpBranchAttr")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(UpBranchAttr));
        }
        if (FCode.equalsIgnoreCase("BranchJobType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BranchJobType));
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeDate));
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyDate));
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (FCode.equalsIgnoreCase("BranchType2")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BranchType2));
        }
        if (FCode.equalsIgnoreCase("AStartDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AStartDate));
        }
        if (FCode.equalsIgnoreCase("BranchArea")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BranchArea));
        }
        if (FCode.equalsIgnoreCase("AssessFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AssessFlag));
        }
        if (FCode.equalsIgnoreCase("AssessBranchType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AssessBranchType));
        }
        if (FCode.equalsIgnoreCase("DXSaleOrg")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DXSaleOrg));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = String.valueOf(AgentGroup);
                break;
            case 1:
                strFieldValue = String.valueOf(Name);
                break;
            case 2:
                strFieldValue = String.valueOf(ManageCom);
                break;
            case 3:
                strFieldValue = String.valueOf(UpBranch);
                break;
            case 4:
                strFieldValue = String.valueOf(BranchAttr);
                break;
            case 5:
                strFieldValue = String.valueOf(BranchSeries);
                break;
            case 6:
                strFieldValue = String.valueOf(BranchType);
                break;
            case 7:
                strFieldValue = String.valueOf(BranchLevel);
                break;
            case 8:
                strFieldValue = String.valueOf(BranchManager);
                break;
            case 9:
                strFieldValue = String.valueOf(BranchAddressCode);
                break;
            case 10:
                strFieldValue = String.valueOf(BranchAddress);
                break;
            case 11:
                strFieldValue = String.valueOf(BranchPhone);
                break;
            case 12:
                strFieldValue = String.valueOf(BranchFax);
                break;
            case 13:
                strFieldValue = String.valueOf(BranchZipcode);
                break;
            case 14:
                strFieldValue = String.valueOf(FoundDate);
                break;
            case 15:
                strFieldValue = String.valueOf(EndDate);
                break;
            case 16:
                strFieldValue = String.valueOf(EndFlag);
                break;
            case 17:
                strFieldValue = String.valueOf(FieldFlag);
                break;
            case 18:
                strFieldValue = String.valueOf(State);
                break;
            case 19:
                strFieldValue = String.valueOf(BranchManagerName);
                break;
            case 20:
                strFieldValue = String.valueOf(UpBranchAttr);
                break;
            case 21:
                strFieldValue = String.valueOf(BranchJobType);
                break;
            case 22:
                strFieldValue = String.valueOf(Operator);
                break;
            case 23:
                strFieldValue = String.valueOf(MakeDate);
                break;
            case 24:
                strFieldValue = String.valueOf(MakeTime);
                break;
            case 25:
                strFieldValue = String.valueOf(ModifyDate);
                break;
            case 26:
                strFieldValue = String.valueOf(ModifyTime);
                break;
            case 27:
                strFieldValue = String.valueOf(BranchType2);
                break;
            case 28:
                strFieldValue = String.valueOf(AStartDate);
                break;
            case 29:
                strFieldValue = String.valueOf(BranchArea);
                break;
            case 30:
                strFieldValue = String.valueOf(AssessFlag);
                break;
            case 31:
                strFieldValue = String.valueOf(AssessBranchType);
                break;
            case 32:
                strFieldValue = String.valueOf(DXSaleOrg);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("AgentGroup")) {
            if( FValue != null && !FValue.equals(""))
            {
                AgentGroup = FValue.trim();
            }
            else
                AgentGroup = null;
        }
        if (FCode.equalsIgnoreCase("Name")) {
            if( FValue != null && !FValue.equals(""))
            {
                Name = FValue.trim();
            }
            else
                Name = null;
        }
        if (FCode.equalsIgnoreCase("ManageCom")) {
            if( FValue != null && !FValue.equals(""))
            {
                ManageCom = FValue.trim();
            }
            else
                ManageCom = null;
        }
        if (FCode.equalsIgnoreCase("UpBranch")) {
            if( FValue != null && !FValue.equals(""))
            {
                UpBranch = FValue.trim();
            }
            else
                UpBranch = null;
        }
        if (FCode.equalsIgnoreCase("BranchAttr")) {
            if( FValue != null && !FValue.equals(""))
            {
                BranchAttr = FValue.trim();
            }
            else
                BranchAttr = null;
        }
        if (FCode.equalsIgnoreCase("BranchSeries")) {
            if( FValue != null && !FValue.equals(""))
            {
                BranchSeries = FValue.trim();
            }
            else
                BranchSeries = null;
        }
        if (FCode.equalsIgnoreCase("BranchType")) {
            if( FValue != null && !FValue.equals(""))
            {
                BranchType = FValue.trim();
            }
            else
                BranchType = null;
        }
        if (FCode.equalsIgnoreCase("BranchLevel")) {
            if( FValue != null && !FValue.equals(""))
            {
                BranchLevel = FValue.trim();
            }
            else
                BranchLevel = null;
        }
        if (FCode.equalsIgnoreCase("BranchManager")) {
            if( FValue != null && !FValue.equals(""))
            {
                BranchManager = FValue.trim();
            }
            else
                BranchManager = null;
        }
        if (FCode.equalsIgnoreCase("BranchAddressCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                BranchAddressCode = FValue.trim();
            }
            else
                BranchAddressCode = null;
        }
        if (FCode.equalsIgnoreCase("BranchAddress")) {
            if( FValue != null && !FValue.equals(""))
            {
                BranchAddress = FValue.trim();
            }
            else
                BranchAddress = null;
        }
        if (FCode.equalsIgnoreCase("BranchPhone")) {
            if( FValue != null && !FValue.equals(""))
            {
                BranchPhone = FValue.trim();
            }
            else
                BranchPhone = null;
        }
        if (FCode.equalsIgnoreCase("BranchFax")) {
            if( FValue != null && !FValue.equals(""))
            {
                BranchFax = FValue.trim();
            }
            else
                BranchFax = null;
        }
        if (FCode.equalsIgnoreCase("BranchZipcode")) {
            if( FValue != null && !FValue.equals(""))
            {
                BranchZipcode = FValue.trim();
            }
            else
                BranchZipcode = null;
        }
        if (FCode.equalsIgnoreCase("FoundDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                FoundDate = FValue.trim();
            }
            else
                FoundDate = null;
        }
        if (FCode.equalsIgnoreCase("EndDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                EndDate = FValue.trim();
            }
            else
                EndDate = null;
        }
        if (FCode.equalsIgnoreCase("EndFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                EndFlag = FValue.trim();
            }
            else
                EndFlag = null;
        }
        if (FCode.equalsIgnoreCase("FieldFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                FieldFlag = FValue.trim();
            }
            else
                FieldFlag = null;
        }
        if (FCode.equalsIgnoreCase("State")) {
            if( FValue != null && !FValue.equals(""))
            {
                State = FValue.trim();
            }
            else
                State = null;
        }
        if (FCode.equalsIgnoreCase("BranchManagerName")) {
            if( FValue != null && !FValue.equals(""))
            {
                BranchManagerName = FValue.trim();
            }
            else
                BranchManagerName = null;
        }
        if (FCode.equalsIgnoreCase("UpBranchAttr")) {
            if( FValue != null && !FValue.equals(""))
            {
                UpBranchAttr = FValue.trim();
            }
            else
                UpBranchAttr = null;
        }
        if (FCode.equalsIgnoreCase("BranchJobType")) {
            if( FValue != null && !FValue.equals(""))
            {
                BranchJobType = FValue.trim();
            }
            else
                BranchJobType = null;
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            if( FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
                Operator = null;
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeDate = FValue.trim();
            }
            else
                MakeDate = null;
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
                MakeTime = null;
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyDate = FValue.trim();
            }
            else
                ModifyDate = null;
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
                ModifyTime = null;
        }
        if (FCode.equalsIgnoreCase("BranchType2")) {
            if( FValue != null && !FValue.equals(""))
            {
                BranchType2 = FValue.trim();
            }
            else
                BranchType2 = null;
        }
        if (FCode.equalsIgnoreCase("AStartDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                AStartDate = FValue.trim();
            }
            else
                AStartDate = null;
        }
        if (FCode.equalsIgnoreCase("BranchArea")) {
            if( FValue != null && !FValue.equals(""))
            {
                BranchArea = FValue.trim();
            }
            else
                BranchArea = null;
        }
        if (FCode.equalsIgnoreCase("AssessFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                AssessFlag = FValue.trim();
            }
            else
                AssessFlag = null;
        }
        if (FCode.equalsIgnoreCase("AssessBranchType")) {
            if( FValue != null && !FValue.equals(""))
            {
                AssessBranchType = FValue.trim();
            }
            else
                AssessBranchType = null;
        }
        if (FCode.equalsIgnoreCase("DXSaleOrg")) {
            if( FValue != null && !FValue.equals(""))
            {
                DXSaleOrg = FValue.trim();
            }
            else
                DXSaleOrg = null;
        }
        return true;
    }


    public String toString() {
    return "LABranchGroupPojo [" +
            "AgentGroup="+AgentGroup +
            ", Name="+Name +
            ", ManageCom="+ManageCom +
            ", UpBranch="+UpBranch +
            ", BranchAttr="+BranchAttr +
            ", BranchSeries="+BranchSeries +
            ", BranchType="+BranchType +
            ", BranchLevel="+BranchLevel +
            ", BranchManager="+BranchManager +
            ", BranchAddressCode="+BranchAddressCode +
            ", BranchAddress="+BranchAddress +
            ", BranchPhone="+BranchPhone +
            ", BranchFax="+BranchFax +
            ", BranchZipcode="+BranchZipcode +
            ", FoundDate="+FoundDate +
            ", EndDate="+EndDate +
            ", EndFlag="+EndFlag +
            ", FieldFlag="+FieldFlag +
            ", State="+State +
            ", BranchManagerName="+BranchManagerName +
            ", UpBranchAttr="+UpBranchAttr +
            ", BranchJobType="+BranchJobType +
            ", Operator="+Operator +
            ", MakeDate="+MakeDate +
            ", MakeTime="+MakeTime +
            ", ModifyDate="+ModifyDate +
            ", ModifyTime="+ModifyTime +
            ", BranchType2="+BranchType2 +
            ", AStartDate="+AStartDate +
            ", BranchArea="+BranchArea +
            ", AssessFlag="+AssessFlag +
            ", AssessBranchType="+AssessBranchType +
            ", DXSaleOrg="+DXSaleOrg +"]";
    }
}
