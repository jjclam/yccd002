/**
 * Copyright (c) 2018 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.db;

import com.sinosoft.lis.schema.LCPolicyInfoSchema;
import com.sinosoft.lis.vschema.LCPolicyInfoSet;
import com.sinosoft.utility.*;

import java.sql.*;

/**
 * <p>ClassName: LCPolicyInfoDB </p>
 * <p>Description: DB层数据库操作类文件 </p>
 * <p>Copyright: Copyright (c) 2018</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2018-07-09
 */
public class LCPolicyInfoDB extends LCPolicyInfoSchema {
    // @Field
    private Connection con;
    private DBOper db;
    /**
     * flag = true: 传入Connection
     * flag = false: 不传入Connection
     **/
    private boolean mflag = false;

    public CErrors mErrors = new CErrors();        // 错误信息

    /**
     * 为批量操作而准备的语句和游标对象
     */
    private ResultSet mResultSet = null;
    private Statement mStatement = null;
    // @Constructor
    public LCPolicyInfoDB( Connection tConnection ) {
        con = tConnection;
        db = new DBOper( con, "LCPolicyInfo" );
        mflag = true;
    }

    public LCPolicyInfoDB() {
        con = null;
        db = new DBOper( "LCPolicyInfo" );
        mflag = false;
    }

    // @Method
    public boolean deleteSQL() {
        LCPolicyInfoSchema tSchema = this.getSchema();
        if (db.deleteSQL(tSchema)) {
            return true;
        } else {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LCPolicyInfoDB";
            tError.functionName = "deleteSQL";
            tError.errorMessage = "操作失败!";
            this.mErrors .addOneError(tError);
            return false;
        }
    }

    public int getCount() {
        LCPolicyInfoSchema tSchema = this.getSchema();

        int tCount = db.getCount(tSchema);
        if (tCount < 0) {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LCPolicyInfoDB";
            tError.functionName = "getCount";
            tError.errorMessage = "操作失败!";
            this.mErrors .addOneError(tError);

            return -1;
        }

        return tCount;
    }

    public boolean delete() {
        PreparedStatement pstmt = null;

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try {
            pstmt = con.prepareStatement("DELETE FROM LCPolicyInfo WHERE  1=1  AND ContNo = ? AND InsuredNo = ?");
            if(this.getContNo() == null || this.getContNo().equals("null")) {
                pstmt.setNull(1, 12);
            } else {
                pstmt.setString(1, this.getContNo());
            }
            if(this.getInsuredNo() == null || this.getInsuredNo().equals("null")) {
                pstmt.setNull(2, 12);
            } else {
                pstmt.setString(2, this.getInsuredNo());
            }
            pstmt.executeUpdate();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LCPolicyInfoDB";
            tError.functionName = "delete()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);
            ex.printStackTrace();

            try {
                pstmt.close();
            } catch (Exception e){}

            if(!mflag) {
                try {
                    con.close();
                } catch (Exception e){}
            }

            return false;
        }

        if(!mflag) {
            try {
                con.close();
            } catch (Exception e){}
        }

        return true;
    }

    public boolean update() {
        PreparedStatement pstmt = null;

        SQLString sqlObj = new SQLString("LCPolicyInfo");
        sqlObj.setSQL(2, this);
        String sql = sqlObj.getSQL();

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try {
            pstmt = con.prepareStatement("UPDATE LCPolicyInfo SET  ContNo = ? , InsuredNo = ? , ContplanCode = ? , ContplanName = ? , BasePrem = ? , AddPrem = ? , ActiveDate = ? , InsuYear = ? , InsuYearFlag = ? , Cvaliintv = ? , CvaliintvFlag = ? , SisinSuredNo = ? , TraveLcountry = ? , AgentIp = ? , FlightNo = ? , RescuecardNo = ? , HandlerName = ? , HandlerPhone = ? , SignDate = ? , SignTime = ? , agentcom = ? , RiskType = ? , OrderType = ? , JYContNo = ? , JYCertNo = ? , JYStartDate = ? , JYEndDate = ? , LoanAmount = ? , LendCom = ? , LoanerNature = ? , LoanerNatureName = ? , LendTerm = ? , CountryCode = ? , GasCompany = ? , GasUserAddress = ? , PayoutPro = ? , School = ? , ChargeDate = ? , Mark = ? WHERE  1=1  AND ContNo = ? AND InsuredNo = ?");
            if(this.getContNo() == null || this.getContNo().equals("null")) {
                pstmt.setNull(1, 12);
            } else {
                pstmt.setString(1, this.getContNo());
            }
            if(this.getInsuredNo() == null || this.getInsuredNo().equals("null")) {
                pstmt.setNull(2, 12);
            } else {
                pstmt.setString(2, this.getInsuredNo());
            }
            if(this.getContplanCode() == null || this.getContplanCode().equals("null")) {
                pstmt.setNull(3, 12);
            } else {
                pstmt.setString(3, this.getContplanCode());
            }
            if(this.getContplanName() == null || this.getContplanName().equals("null")) {
                pstmt.setNull(4, 12);
            } else {
                pstmt.setString(4, this.getContplanName());
            }
            pstmt.setDouble(5, this.getBasePrem());
            pstmt.setDouble(6, this.getAddPrem());
            if(this.getActiveDate() == null || this.getActiveDate().equals("null")) {
                pstmt.setNull(7, 93);
            } else {
                pstmt.setDate(7, Date.valueOf(this.getActiveDate()));
            }
            pstmt.setInt(8, this.getInsuYear());
            if(this.getInsuYearFlag() == null || this.getInsuYearFlag().equals("null")) {
                pstmt.setNull(9, 12);
            } else {
                pstmt.setString(9, this.getInsuYearFlag());
            }
            pstmt.setInt(10, this.getCvaliintv());
            if(this.getCvaliintvFlag() == null || this.getCvaliintvFlag().equals("null")) {
                pstmt.setNull(11, 12);
            } else {
                pstmt.setString(11, this.getCvaliintvFlag());
            }
            if(this.getSisinSuredNo() == null || this.getSisinSuredNo().equals("null")) {
                pstmt.setNull(12, 12);
            } else {
                pstmt.setString(12, this.getSisinSuredNo());
            }
            if(this.getTraveLcountry() == null || this.getTraveLcountry().equals("null")) {
                pstmt.setNull(13, 12);
            } else {
                pstmt.setString(13, this.getTraveLcountry());
            }
            if(this.getAgentIp() == null || this.getAgentIp().equals("null")) {
                pstmt.setNull(14, 12);
            } else {
                pstmt.setString(14, this.getAgentIp());
            }
            if(this.getFlightNo() == null || this.getFlightNo().equals("null")) {
                pstmt.setNull(15, 12);
            } else {
                pstmt.setString(15, this.getFlightNo());
            }
            if(this.getRescuecardNo() == null || this.getRescuecardNo().equals("null")) {
                pstmt.setNull(16, 12);
            } else {
                pstmt.setString(16, this.getRescuecardNo());
            }
            if(this.getHandlerName() == null || this.getHandlerName().equals("null")) {
                pstmt.setNull(17, 12);
            } else {
                pstmt.setString(17, this.getHandlerName());
            }
            if(this.getHandlerPhone() == null || this.getHandlerPhone().equals("null")) {
                pstmt.setNull(18, 12);
            } else {
                pstmt.setString(18, this.getHandlerPhone());
            }
            if(this.getSignDate() == null || this.getSignDate().equals("null")) {
                pstmt.setNull(19, 93);
            } else {
                pstmt.setDate(19, Date.valueOf(this.getSignDate()));
            }
            if(this.getSignTime() == null || this.getSignTime().equals("null")) {
                pstmt.setNull(20, 12);
            } else {
                pstmt.setString(20, this.getSignTime());
            }
            if(this.getAgentcom() == null || this.getAgentcom().equals("null")) {
                pstmt.setNull(21, 12);
            } else {
                pstmt.setString(21, this.getAgentcom());
            }
            if(this.getRiskType() == null || this.getRiskType().equals("null")) {
                pstmt.setNull(22, 12);
            } else {
                pstmt.setString(22, this.getRiskType());
            }
            if(this.getOrderType() == null || this.getOrderType().equals("null")) {
                pstmt.setNull(23, 12);
            } else {
                pstmt.setString(23, this.getOrderType());
            }
            if(this.getJYContNo() == null || this.getJYContNo().equals("null")) {
                pstmt.setNull(24, 12);
            } else {
                pstmt.setString(24, this.getJYContNo());
            }
            if(this.getJYCertNo() == null || this.getJYCertNo().equals("null")) {
                pstmt.setNull(25, 12);
            } else {
                pstmt.setString(25, this.getJYCertNo());
            }
            if(this.getJYStartDate() == null || this.getJYStartDate().equals("null")) {
                pstmt.setNull(26, 12);
            } else {
                pstmt.setString(26, this.getJYStartDate());
            }
            if(this.getJYEndDate() == null || this.getJYEndDate().equals("null")) {
                pstmt.setNull(27, 12);
            } else {
                pstmt.setString(27, this.getJYEndDate());
            }
            if(this.getLoanAmount() == null || this.getLoanAmount().equals("null")) {
                pstmt.setNull(28, 12);
            } else {
                pstmt.setString(28, this.getLoanAmount());
            }
            if(this.getLendCom() == null || this.getLendCom().equals("null")) {
                pstmt.setNull(29, 12);
            } else {
                pstmt.setString(29, this.getLendCom());
            }
            if(this.getLoanerNature() == null || this.getLoanerNature().equals("null")) {
                pstmt.setNull(30, 12);
            } else {
                pstmt.setString(30, this.getLoanerNature());
            }
            if(this.getLoanerNatureName() == null || this.getLoanerNatureName().equals("null")) {
                pstmt.setNull(31, 12);
            } else {
                pstmt.setString(31, this.getLoanerNatureName());
            }
            if(this.getLendTerm() == null || this.getLendTerm().equals("null")) {
                pstmt.setNull(32, 12);
            } else {
                pstmt.setString(32, this.getLendTerm());
            }
            if(this.getCountryCode() == null || this.getCountryCode().equals("null")) {
                pstmt.setNull(33, 12);
            } else {
                pstmt.setString(33, this.getCountryCode());
            }
            if(this.getGasCompany() == null || this.getGasCompany().equals("null")) {
                pstmt.setNull(34, 12);
            } else {
                pstmt.setString(34, this.getGasCompany());
            }
            if(this.getGasUserAddress() == null || this.getGasUserAddress().equals("null")) {
                pstmt.setNull(35, 12);
            } else {
                pstmt.setString(35, this.getGasUserAddress());
            }
            if(this.getPayoutPro() == null || this.getPayoutPro().equals("null")) {
                pstmt.setNull(36, 12);
            } else {
                pstmt.setString(36, this.getPayoutPro());
            }
            if(this.getSchool() == null || this.getSchool().equals("null")) {
                pstmt.setNull(37, 12);
            } else {
                pstmt.setString(37, this.getSchool());
            }
            if(this.getChargeDate() == null || this.getChargeDate().equals("null")) {
                pstmt.setNull(38, 12);
            } else {
                pstmt.setString(38, this.getChargeDate());
            }
            if(this.getMark() == null || this.getMark().equals("null")) {
                pstmt.setNull(39, 12);
            } else {
                pstmt.setString(39, this.getMark());
            }
            // set where condition
            if(this.getContNo() == null || this.getContNo().equals("null")) {
                pstmt.setNull(40, 12);
            } else {
                pstmt.setString(40, this.getContNo());
            }
            if(this.getInsuredNo() == null || this.getInsuredNo().equals("null")) {
                pstmt.setNull(41, 12);
            } else {
                pstmt.setString(41, this.getInsuredNo());
            }
            pstmt.executeUpdate();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LCPolicyInfoDB";
            tError.functionName = "update()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);
            ex.printStackTrace();

            try {
                pstmt.close();
            } catch (Exception e){}

            if( !mflag ) {
                try {
                    con.close();
                } catch (Exception e){}
            }

            return false;
        }

        if(!mflag) {
            try {
                con.close();
            } catch (Exception e){}
        }

        return true;
    }

    public boolean insert() {
        PreparedStatement pstmt = null;

        SQLString sqlObj = new SQLString("LCPolicyInfo");
        sqlObj.setSQL(1, this);
        String sql = sqlObj.getSQL();

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try {
            pstmt = con.prepareStatement("INSERT INTO LCPolicyInfo VALUES( ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ?)");
            if(this.getContNo() == null || this.getContNo().equals("null")) {
                pstmt.setNull(1, 12);
            } else {
                pstmt.setString(1, this.getContNo());
            }
            if(this.getInsuredNo() == null || this.getInsuredNo().equals("null")) {
                pstmt.setNull(2, 12);
            } else {
                pstmt.setString(2, this.getInsuredNo());
            }
            if(this.getContplanCode() == null || this.getContplanCode().equals("null")) {
                pstmt.setNull(3, 12);
            } else {
                pstmt.setString(3, this.getContplanCode());
            }
            if(this.getContplanName() == null || this.getContplanName().equals("null")) {
                pstmt.setNull(4, 12);
            } else {
                pstmt.setString(4, this.getContplanName());
            }
            pstmt.setDouble(5, this.getBasePrem());
            pstmt.setDouble(6, this.getAddPrem());
            if(this.getActiveDate() == null || this.getActiveDate().equals("null")) {
                pstmt.setNull(7, 93);
            } else {
                pstmt.setDate(7, Date.valueOf(this.getActiveDate()));
            }
            pstmt.setInt(8, this.getInsuYear());
            if(this.getInsuYearFlag() == null || this.getInsuYearFlag().equals("null")) {
                pstmt.setNull(9, 12);
            } else {
                pstmt.setString(9, this.getInsuYearFlag());
            }
            pstmt.setInt(10, this.getCvaliintv());
            if(this.getCvaliintvFlag() == null || this.getCvaliintvFlag().equals("null")) {
                pstmt.setNull(11, 12);
            } else {
                pstmt.setString(11, this.getCvaliintvFlag());
            }
            if(this.getSisinSuredNo() == null || this.getSisinSuredNo().equals("null")) {
                pstmt.setNull(12, 12);
            } else {
                pstmt.setString(12, this.getSisinSuredNo());
            }
            if(this.getTraveLcountry() == null || this.getTraveLcountry().equals("null")) {
                pstmt.setNull(13, 12);
            } else {
                pstmt.setString(13, this.getTraveLcountry());
            }
            if(this.getAgentIp() == null || this.getAgentIp().equals("null")) {
                pstmt.setNull(14, 12);
            } else {
                pstmt.setString(14, this.getAgentIp());
            }
            if(this.getFlightNo() == null || this.getFlightNo().equals("null")) {
                pstmt.setNull(15, 12);
            } else {
                pstmt.setString(15, this.getFlightNo());
            }
            if(this.getRescuecardNo() == null || this.getRescuecardNo().equals("null")) {
                pstmt.setNull(16, 12);
            } else {
                pstmt.setString(16, this.getRescuecardNo());
            }
            if(this.getHandlerName() == null || this.getHandlerName().equals("null")) {
                pstmt.setNull(17, 12);
            } else {
                pstmt.setString(17, this.getHandlerName());
            }
            if(this.getHandlerPhone() == null || this.getHandlerPhone().equals("null")) {
                pstmt.setNull(18, 12);
            } else {
                pstmt.setString(18, this.getHandlerPhone());
            }
            if(this.getSignDate() == null || this.getSignDate().equals("null")) {
                pstmt.setNull(19, 93);
            } else {
                pstmt.setDate(19, Date.valueOf(this.getSignDate()));
            }
            if(this.getSignTime() == null || this.getSignTime().equals("null")) {
                pstmt.setNull(20, 12);
            } else {
                pstmt.setString(20, this.getSignTime());
            }
            if(this.getAgentcom() == null || this.getAgentcom().equals("null")) {
                pstmt.setNull(21, 12);
            } else {
                pstmt.setString(21, this.getAgentcom());
            }
            if(this.getRiskType() == null || this.getRiskType().equals("null")) {
                pstmt.setNull(22, 12);
            } else {
                pstmt.setString(22, this.getRiskType());
            }
            if(this.getOrderType() == null || this.getOrderType().equals("null")) {
                pstmt.setNull(23, 12);
            } else {
                pstmt.setString(23, this.getOrderType());
            }
            if(this.getJYContNo() == null || this.getJYContNo().equals("null")) {
                pstmt.setNull(24, 12);
            } else {
                pstmt.setString(24, this.getJYContNo());
            }
            if(this.getJYCertNo() == null || this.getJYCertNo().equals("null")) {
                pstmt.setNull(25, 12);
            } else {
                pstmt.setString(25, this.getJYCertNo());
            }
            if(this.getJYStartDate() == null || this.getJYStartDate().equals("null")) {
                pstmt.setNull(26, 12);
            } else {
                pstmt.setString(26, this.getJYStartDate());
            }
            if(this.getJYEndDate() == null || this.getJYEndDate().equals("null")) {
                pstmt.setNull(27, 12);
            } else {
                pstmt.setString(27, this.getJYEndDate());
            }
            if(this.getLoanAmount() == null || this.getLoanAmount().equals("null")) {
                pstmt.setNull(28, 12);
            } else {
                pstmt.setString(28, this.getLoanAmount());
            }
            if(this.getLendCom() == null || this.getLendCom().equals("null")) {
                pstmt.setNull(29, 12);
            } else {
                pstmt.setString(29, this.getLendCom());
            }
            if(this.getLoanerNature() == null || this.getLoanerNature().equals("null")) {
                pstmt.setNull(30, 12);
            } else {
                pstmt.setString(30, this.getLoanerNature());
            }
            if(this.getLoanerNatureName() == null || this.getLoanerNatureName().equals("null")) {
                pstmt.setNull(31, 12);
            } else {
                pstmt.setString(31, this.getLoanerNatureName());
            }
            if(this.getLendTerm() == null || this.getLendTerm().equals("null")) {
                pstmt.setNull(32, 12);
            } else {
                pstmt.setString(32, this.getLendTerm());
            }
            if(this.getCountryCode() == null || this.getCountryCode().equals("null")) {
                pstmt.setNull(33, 12);
            } else {
                pstmt.setString(33, this.getCountryCode());
            }
            if(this.getGasCompany() == null || this.getGasCompany().equals("null")) {
                pstmt.setNull(34, 12);
            } else {
                pstmt.setString(34, this.getGasCompany());
            }
            if(this.getGasUserAddress() == null || this.getGasUserAddress().equals("null")) {
                pstmt.setNull(35, 12);
            } else {
                pstmt.setString(35, this.getGasUserAddress());
            }
            if(this.getPayoutPro() == null || this.getPayoutPro().equals("null")) {
                pstmt.setNull(36, 12);
            } else {
                pstmt.setString(36, this.getPayoutPro());
            }
            if(this.getSchool() == null || this.getSchool().equals("null")) {
                pstmt.setNull(37, 12);
            } else {
                pstmt.setString(37, this.getSchool());
            }
            if(this.getChargeDate() == null || this.getChargeDate().equals("null")) {
                pstmt.setNull(38, 12);
            } else {
                pstmt.setString(38, this.getChargeDate());
            }
            if(this.getMark() == null || this.getMark().equals("null")) {
                pstmt.setNull(39, 12);
            } else {
                pstmt.setString(39, this.getMark());
            }
            // execute sql
            pstmt.executeUpdate();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LCPolicyInfoDB";
            tError.functionName = "insert()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors.addOneError(tError);
            ex.printStackTrace();

            try {
                pstmt.close();
            } catch (Exception e){}

            if( !mflag ) {
                try {
                    con.close();
                } catch (Exception e){}
            }

            return false;
        }

        if(!mflag) {
            try {
                con.close();
            } catch (Exception e){}
        }

        return true;
    }

    public boolean getInfo() {
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        if(!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            pstmt = con.prepareStatement("SELECT * FROM LCPolicyInfo WHERE  1=1  AND ContNo = ? AND InsuredNo = ?",
                    ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
            if(this.getContNo() == null || this.getContNo().equals("null")) {
                pstmt.setNull(1, 12);
            } else {
                pstmt.setString(1, this.getContNo());
            }
            if(this.getInsuredNo() == null || this.getInsuredNo().equals("null")) {
                pstmt.setNull(2, 12);
            } else {
                pstmt.setString(2, this.getInsuredNo());
            }
            rs = pstmt.executeQuery();
            int i = 0;
            while (rs.next()) {
                i++;
                if (!this.setSchema(rs,i)) {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "LCPolicyInfoDB";
                    tError.functionName = "getInfo";
                    tError.errorMessage = "取数失败!";
                    this.mErrors .addOneError(tError);

                    try{ rs.close(); } catch( Exception ex ) {}
                    try{ pstmt.close(); } catch( Exception ex1 ) {}

                    if (!mflag) {
                        try {
                            con.close();
                        }
                        catch(Exception et){}
                    }
                    return false;
                }
                break;
            }
            try{ rs.close(); } catch( Exception ex2 ) {}
            try{ pstmt.close(); } catch( Exception ex3 ) {}

            if( i == 0 ) {
                if (!mflag) {
                    try {
                        con.close();
                    }
                    catch(Exception et){}
                }
                return false;
            }
        }
        catch(Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LCPolicyInfoDB";
            tError.functionName = "getInfo";
            tError.errorMessage = ExceptionUtils.exceptionToString(e);
            this.mErrors .addOneError(tError);

            try{ rs.close(); } catch( Exception ex ) {}
            try{ pstmt.close(); } catch( Exception ex1 ) {}

            if (!mflag) {
                try {
                    con.close();
                }
                catch(Exception et){}
            }
            return false;
        }
        // 断开数据库连接
        if (!mflag) {
            try {
                con.close();
            }
            catch(Exception e){}
        }

        return true;
    }

    public LCPolicyInfoSet query() {
        Statement stmt = null;
        ResultSet rs = null;
        LCPolicyInfoSet aLCPolicyInfoSet = new LCPolicyInfoSet();

        if (!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            stmt = con.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
            SQLString sqlObj = new SQLString("LCPolicyInfo");
            LCPolicyInfoSchema aSchema = this.getSchema();
            sqlObj.setSQL(5, aSchema);
            String sql = sqlObj.getSQL();

            rs = stmt.executeQuery(sql);
            int i = 0;
            while (rs.next()) {
                i++;
                if (i>10000) {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "LCPolicyInfoDB";
                    tError.functionName = "query";
                    tError.errorMessage = "SQL语句返回结果集太多，请换用其他方式!";
                    this.mErrors .addOneError(tError);
                    break;
                }
                LCPolicyInfoSchema s1 = new LCPolicyInfoSchema();
                s1.setSchema(rs,i);
                aLCPolicyInfoSet.add(s1);
            }
            try{ rs.close(); } catch( Exception ex ) {}
            try{ stmt.close(); } catch( Exception ex1 ) {}
        }
        catch(Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LCPolicyInfoDB";
            tError.functionName = "query";
            tError.errorMessage = ExceptionUtils.exceptionToString(e);
            this.mErrors.addOneError(tError);
            e.printStackTrace();

            try { rs.close(); } catch (Exception ex2) { ex2.printStackTrace(); }
            try { stmt.close(); } catch(Exception ex3) { ex3.printStackTrace(); }

            if (!mflag) {
                try {
                    con.close();
                }
                catch(Exception et){et.printStackTrace();}
            }
        }

        if (!mflag) {
            try {
                con.close();
            }
            catch(Exception e){e.printStackTrace();}
        }

        return aLCPolicyInfoSet;
    }

    public LCPolicyInfoSet executeQuery(String sql)  {
        Statement stmt = null;
        ResultSet rs = null;
        LCPolicyInfoSet aLCPolicyInfoSet = new LCPolicyInfoSet();

        if (!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            stmt = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);

            rs = stmt.executeQuery(StrTool.GBKToUnicode(sql));
            int i = 0;
            while (rs.next()) {
                i++;
                if (i>10000) {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "LCPolicyInfoDB";
                    tError.functionName = "executeQuery";
                    tError.errorMessage = "SQL语句返回结果集太多，请换用其他方式!";
                    this.mErrors .addOneError(tError);
                    break;
                }
                LCPolicyInfoSchema s1 = new LCPolicyInfoSchema();
                if (!s1.setSchema(rs,i)) {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "LCPolicyInfoDB";
                    tError.functionName = "executeQuery";
                    tError.errorMessage = "sql语句有误，请查看表名及字段名信息!";
                    this.mErrors .addOneError(tError);
                }
                aLCPolicyInfoSet.add(s1);
            }
            try{ rs.close(); } catch( Exception ex ) {}
            try{ stmt.close(); } catch( Exception ex1 ) {}
        }
        catch(Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LCPolicyInfoDB";
            tError.functionName = "executeQuery";
            tError.errorMessage = ExceptionUtils.exceptionToString(e);
            this.mErrors .addOneError(tError);

            try{ rs.close(); } catch( Exception ex2 ) {}
            try{ stmt.close(); } catch( Exception ex3 ) {}

            if (!mflag) {
                try {
                    con.close();
                }
                catch(Exception et){}
            }
        }

        if (!mflag) {
            try {
                con.close();
            }
            catch(Exception e){}
        }

        return aLCPolicyInfoSet;
    }

    public LCPolicyInfoSet query(int nStart, int nCount) {
        Statement stmt = null;
        ResultSet rs = null;
        LCPolicyInfoSet aLCPolicyInfoSet = new LCPolicyInfoSet();

        if (!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            stmt = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);
            SQLString sqlObj = new SQLString("LCPolicyInfo");
            LCPolicyInfoSchema aSchema = this.getSchema();
            sqlObj.setSQL(5,aSchema);
            String sql = sqlObj.getSQL();

            rs = stmt.executeQuery(sql);
            int i = 0;
            while (rs.next()) {
                i++;

                if( i < nStart ) {
                    continue;
                }

                if( i >= nStart + nCount ) {
                    break;
                }

                LCPolicyInfoSchema s1 = new LCPolicyInfoSchema();
                s1.setSchema(rs,i);
                aLCPolicyInfoSet.add(s1);
            }
            try{ rs.close(); } catch( Exception ex ) {}
            try{ stmt.close(); } catch( Exception ex1 ) {}
        }
        catch(Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LCPolicyInfoDB";
            tError.functionName = "query";
            tError.errorMessage = ExceptionUtils.exceptionToString(e);
            this.mErrors .addOneError(tError);

            try{ rs.close(); } catch( Exception ex2 ) {}
            try{ stmt.close(); } catch( Exception ex3 ) {}

            if (!mflag) {
                try {
                    con.close();
                }
                catch(Exception et){}
            }
        }

        if (!mflag) {
            try {
                con.close();
            }
            catch(Exception e){}
        }

        return aLCPolicyInfoSet;
    }

    public LCPolicyInfoSet executeQuery(String sql, int nStart, int nCount) {
        Statement stmt = null;
        ResultSet rs = null;
        LCPolicyInfoSet aLCPolicyInfoSet = new LCPolicyInfoSet();

        if (!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            stmt = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);

            rs = stmt.executeQuery(StrTool.GBKToUnicode(sql));
            int i = 0;
            while (rs.next()) {
                i++;

                if( i < nStart ) {
                    continue;
                }

                if( i >= nStart + nCount ) {
                    break;
                }

                LCPolicyInfoSchema s1 = new LCPolicyInfoSchema();
                if (!s1.setSchema(rs,i)) {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "LCPolicyInfoDB";
                    tError.functionName = "executeQuery";
                    tError.errorMessage = "sql语句有误，请查看表名及字段名信息!";
                    this.mErrors .addOneError(tError);
                }
                aLCPolicyInfoSet.add(s1);
            }
            try{ rs.close(); } catch( Exception ex ) {}
            try{ stmt.close(); } catch( Exception ex1 ) {}
        }
        catch(Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LCPolicyInfoDB";
            tError.functionName = "executeQuery";
            tError.errorMessage = ExceptionUtils.exceptionToString(e);
            this.mErrors .addOneError(tError);

            try{ rs.close(); } catch( Exception ex2 ) {}
            try{ stmt.close(); } catch( Exception ex3 ) {}

            if (!mflag) {
                try {
                    con.close();
                }
                catch(Exception et){}
            }
        }

        if (!mflag) {
            try {
                con.close();
            }
            catch(Exception e){}
        }

        return aLCPolicyInfoSet;
    }

    public boolean update(String strWherePart) {
        Statement stmt = null;

        if (!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            stmt = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);
            SQLString sqlObj = new SQLString("LCPolicyInfo");
            LCPolicyInfoSchema aSchema = this.getSchema();
            sqlObj.setSQL(2,aSchema);
            String sql = "update LCPolicyInfo " + sqlObj.getUpdPart() + " where " + strWherePart;

            int operCount = stmt.executeUpdate(sql);
            if (operCount == 0) {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "LCPolicyInfoDB";
                tError.functionName = "update";
                tError.errorMessage = "更新数据失败!";
                this.mErrors .addOneError(tError);

                if (!mflag) {
                    try {
                        con.close();
                    }
                    catch(Exception et){}
                }
                return false;
            }
        }
        catch(Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LCPolicyInfoDB";
            tError.functionName = "update";
            tError.errorMessage = ExceptionUtils.exceptionToString(e);
            this.mErrors .addOneError(tError);

            try{ stmt.close(); } catch( Exception ex1 ) {}

            if (!mflag) {
                try {
                    con.close();
                }
                catch(Exception et){}
            }
            return false;
        }
        // 断开数据库连接
        if (!mflag) {
            try {
                con.close();
            }
            catch(Exception e){}
        }

        return true;
    }

    /**
     * 准备数据查询条件
     * @param strSQL String
     * @return boolean
     */
    public boolean prepareData(String strSQL) {
        if (mResultSet != null) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LCPolicyInfoDB";
            tError.functionName = "prepareData";
            tError.errorMessage = "数据集非空，程序在准备数据集之后，没有关闭！";
            this.mErrors.addOneError(tError);
            return false;
        }

        if (!mflag) {
            con = DBConnPool.getConnection();
        }
        try {
            mStatement = con.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
            mResultSet = mStatement.executeQuery(StrTool.GBKToUnicode(strSQL));
        }
        catch (Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LCPolicyInfoDB";
            tError.functionName = "prepareData";
            tError.errorMessage = ExceptionUtils.exceptionToString(e);
            this.mErrors.addOneError(tError);
            try {
                mResultSet.close();
            }
            catch (Exception ex2) {}
            try {
                mStatement.close();
            }
            catch (Exception ex3) {}
            if (!mflag) {
                try {
                    con.close();
                }
                catch (Exception et) {}
            }
            return false;
        }

        if (!mflag) {
            try {
                con.close();
            }
            catch (Exception e) {}
        }
        return true;
    }

    /**
     * 获取数据集
     * @return boolean
     */
    public boolean hasMoreData() {
        boolean flag = true;
        if (null == mResultSet) {
            CError tError = new CError();
            tError.moduleName = "LCPolicyInfoDB";
            tError.functionName = "hasMoreData";
            tError.errorMessage = "数据集为空，请先准备数据集！";
            this.mErrors.addOneError(tError);
            return false;
        }
        try {
            flag = mResultSet.next();
        }
        catch (Exception ex) {
            CError tError = new CError();
            tError.moduleName = "LCPolicyInfoDB";
            tError.functionName = "hasMoreData";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors.addOneError(tError);
            try {
                mResultSet.close();
                mResultSet = null;
            }
            catch (Exception ex2) {}
            try {
                mStatement.close();
                mStatement = null;
            }
            catch (Exception ex3) {}
            if (!mflag) {
                try {
                    con.close();
                }
                catch (Exception et) {}
            }
            return false;
        }
        return flag;
    }
    /**
     * 获取定量数据
     * @return LCPolicyInfoSet
     */
    public LCPolicyInfoSet getData() {
        int tCount = 0;
        LCPolicyInfoSet tLCPolicyInfoSet = new LCPolicyInfoSet();
        LCPolicyInfoSchema tLCPolicyInfoSchema = null;
        if (null == mResultSet) {
            CError tError = new CError();
            tError.moduleName = "LCPolicyInfoDB";
            tError.functionName = "getData";
            tError.errorMessage = "数据集为空，请先准备数据集！";
            this.mErrors.addOneError(tError);
            return null;
        }
        try {
            tCount = 1;
            tLCPolicyInfoSchema = new LCPolicyInfoSchema();
            tLCPolicyInfoSchema.setSchema(mResultSet, 1);
            tLCPolicyInfoSet.add(tLCPolicyInfoSchema);
            //注意mResultSet.next()的作用
            while (tCount++ < SysConst.FETCHCOUNT) {
                if (mResultSet.next()) {
                    tLCPolicyInfoSchema = new LCPolicyInfoSchema();
                    tLCPolicyInfoSchema.setSchema(mResultSet, 1);
                    tLCPolicyInfoSet.add(tLCPolicyInfoSchema);
                }
            }
        }
        catch (Exception ex) {
            CError tError = new CError();
            tError.moduleName = "LCPolicyInfoDB";
            tError.functionName = "getData";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors.addOneError(tError);
            try {
                mResultSet.close();
                mResultSet = null;
            }
            catch (Exception ex2) {}
            try {
                mStatement.close();
                mStatement = null;
            }
            catch (Exception ex3) {}
            if (!mflag) {
                try {
                    con.close();
                }
                catch (Exception et) {}
            }
            return null;
        }
        return tLCPolicyInfoSet;
    }
    /**
     * 关闭数据集
     * @return boolean
     */
    public boolean closeData() {
        boolean flag = true;
        try {
            if (null == mResultSet) {
                CError tError = new CError();
                tError.moduleName = "LCPolicyInfoDB";
                tError.functionName = "closeData";
                tError.errorMessage = "数据集已经关闭了！";
                this.mErrors.addOneError(tError);
                flag = false;
            } else {
                mResultSet.close();
                mResultSet = null;
            }
        }
        catch (Exception ex2) {
            CError tError = new CError();
            tError.moduleName = "LCPolicyInfoDB";
            tError.functionName = "closeData";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex2);
            this.mErrors.addOneError(tError);
            flag = false;
        }
        try {
            if (null == mStatement) {
                CError tError = new CError();
                tError.moduleName = "LCPolicyInfoDB";
                tError.functionName = "closeData";
                tError.errorMessage = "语句已经关闭了！";
                this.mErrors.addOneError(tError);
                flag = false;
            } else {
                mStatement.close();
                mStatement = null;
            }
        }
        catch (Exception ex3) {
            CError tError = new CError();
            tError.moduleName = "LCPolicyInfoDB";
            tError.functionName = "closeData";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex3);
            this.mErrors.addOneError(tError);
            flag = false;
        }
        return flag;
    }
}
