/**
 * Copyright (c) 2017 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.entity;

import com.sinosoft.cloud.cache.annotation.RedisIndexHKey;
import com.sinosoft.cloud.cache.annotation.RedisPrimaryHKey;
import com.sinosoft.utility.Pojo;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;

import java.io.Serializable;

/**
 * <p>ClassName: LDAddressPojo </p>
 * <p>Copyright: Copyright (c) 2017</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2017-10-17
 */
public class LDAddressPojo implements Pojo,Serializable {
    // @Field
    /** 地域类型 */
    @RedisPrimaryHKey
    @RedisIndexHKey
    private String PlaceType; 
    /** 地域代码 */
    @RedisPrimaryHKey
    private String PlaceCode; 
    /** 地域名称 */
    private String PlaceName; 
    /** 上级地域代码 */
    private String UpPlaceName; 


    public static final int FIELDNUM = 4;    // 数据库表的字段个数
    public String getPlaceType() {
        return PlaceType;
    }
    public void setPlaceType(String aPlaceType) {
        PlaceType = aPlaceType;
    }
    public String getPlaceCode() {
        return PlaceCode;
    }
    public void setPlaceCode(String aPlaceCode) {
        PlaceCode = aPlaceCode;
    }
    public String getPlaceName() {
        return PlaceName;
    }
    public void setPlaceName(String aPlaceName) {
        PlaceName = aPlaceName;
    }
    public String getUpPlaceName() {
        return UpPlaceName;
    }
    public void setUpPlaceName(String aUpPlaceName) {
        UpPlaceName = aUpPlaceName;
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("PlaceType") ) {
            return 0;
        }
        if( strFieldName.equals("PlaceCode") ) {
            return 1;
        }
        if( strFieldName.equals("PlaceName") ) {
            return 2;
        }
        if( strFieldName.equals("UpPlaceName") ) {
            return 3;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "PlaceType";
                break;
            case 1:
                strFieldName = "PlaceCode";
                break;
            case 2:
                strFieldName = "PlaceName";
                break;
            case 3:
                strFieldName = "UpPlaceName";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "PLACETYPE":
                return Schema.TYPE_STRING;
            case "PLACECODE":
                return Schema.TYPE_STRING;
            case "PLACENAME":
                return Schema.TYPE_STRING;
            case "UPPLACENAME":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_STRING;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("PlaceType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PlaceType));
        }
        if (FCode.equalsIgnoreCase("PlaceCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PlaceCode));
        }
        if (FCode.equalsIgnoreCase("PlaceName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PlaceName));
        }
        if (FCode.equalsIgnoreCase("UpPlaceName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(UpPlaceName));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = String.valueOf(PlaceType);
                break;
            case 1:
                strFieldValue = String.valueOf(PlaceCode);
                break;
            case 2:
                strFieldValue = String.valueOf(PlaceName);
                break;
            case 3:
                strFieldValue = String.valueOf(UpPlaceName);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("PlaceType")) {
            if( FValue != null && !FValue.equals(""))
            {
                PlaceType = FValue.trim();
            }
            else
                PlaceType = null;
        }
        if (FCode.equalsIgnoreCase("PlaceCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                PlaceCode = FValue.trim();
            }
            else
                PlaceCode = null;
        }
        if (FCode.equalsIgnoreCase("PlaceName")) {
            if( FValue != null && !FValue.equals(""))
            {
                PlaceName = FValue.trim();
            }
            else
                PlaceName = null;
        }
        if (FCode.equalsIgnoreCase("UpPlaceName")) {
            if( FValue != null && !FValue.equals(""))
            {
                UpPlaceName = FValue.trim();
            }
            else
                UpPlaceName = null;
        }
        return true;
    }


    public String toString() {
    return "LDAddressPojo [" +
            "PlaceType="+PlaceType +
            ", PlaceCode="+PlaceCode +
            ", PlaceName="+PlaceName +
            ", UpPlaceName="+UpPlaceName +"]";
    }
}
