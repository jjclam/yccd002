/**
 * Copyright (c) 2019 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.entity;

import com.sinosoft.utility.Pojo;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;

import java.io.Serializable;

/**
 * <p>ClassName: LMUWPojo </p>
 * <p>Copyright: Copyright (c) 2019</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2019-09-10
 */
public class LMUWPojo implements  Pojo,Serializable {
    // @Field
    /** 核保编码 */
    private String UWCode;
    /** 险种编码 */
    private String RiskCode;
    /** 险种版本 */
    private String RiskVer;
    /** 险种名称 */
    private String RiskName;
    /** 关联保单类型 */
    private String RelaPolType;
    /** 核保类型 */
    private String UWType;
    /** 核保顺序号 */
    private int UWOrder;
    /** 算法 */
    private String CalCode;
    /** 其他算法 */
    private String OthCalCode;
    /** 备注 */
    private String Remark;
    /** 核保级别 */
    private String UWGrade;
    /** 核保结果控制 */
    private String UWResult;
    /** 核保通过标记 */
    private String PassFlag;
    /** 销售渠道 */
    private String SaleChnl;
    /** 销售子渠道 */
    private String SellType;
    /** 核保层级 */
    private String UWHierarchy;
    /** 核保规则分类 */
    private String UWStyle;
    /** 校验类型 */
    private String CheckFlag;


    public static final int FIELDNUM = 18;    // 数据库表的字段个数
    public String getUWCode() {
        return UWCode;
    }
    public void setUWCode(String aUWCode) {
        UWCode = aUWCode;
    }
    public String getRiskCode() {
        return RiskCode;
    }
    public void setRiskCode(String aRiskCode) {
        RiskCode = aRiskCode;
    }
    public String getRiskVer() {
        return RiskVer;
    }
    public void setRiskVer(String aRiskVer) {
        RiskVer = aRiskVer;
    }
    public String getRiskName() {
        return RiskName;
    }
    public void setRiskName(String aRiskName) {
        RiskName = aRiskName;
    }
    public String getRelaPolType() {
        return RelaPolType;
    }
    public void setRelaPolType(String aRelaPolType) {
        RelaPolType = aRelaPolType;
    }
    public String getUWType() {
        return UWType;
    }
    public void setUWType(String aUWType) {
        UWType = aUWType;
    }
    public int getUWOrder() {
        return UWOrder;
    }
    public void setUWOrder(int aUWOrder) {
        UWOrder = aUWOrder;
    }
    public void setUWOrder(String aUWOrder) {
        if (aUWOrder != null && !aUWOrder.equals("")) {
            Integer tInteger = new Integer(aUWOrder);
            int i = tInteger.intValue();
            UWOrder = i;
        }
    }

    public String getCalCode() {
        return CalCode;
    }
    public void setCalCode(String aCalCode) {
        CalCode = aCalCode;
    }
    public String getOthCalCode() {
        return OthCalCode;
    }
    public void setOthCalCode(String aOthCalCode) {
        OthCalCode = aOthCalCode;
    }
    public String getRemark() {
        return Remark;
    }
    public void setRemark(String aRemark) {
        Remark = aRemark;
    }
    public String getUWGrade() {
        return UWGrade;
    }
    public void setUWGrade(String aUWGrade) {
        UWGrade = aUWGrade;
    }
    public String getUWResult() {
        return UWResult;
    }
    public void setUWResult(String aUWResult) {
        UWResult = aUWResult;
    }
    public String getPassFlag() {
        return PassFlag;
    }
    public void setPassFlag(String aPassFlag) {
        PassFlag = aPassFlag;
    }
    public String getSaleChnl() {
        return SaleChnl;
    }
    public void setSaleChnl(String aSaleChnl) {
        SaleChnl = aSaleChnl;
    }
    public String getSellType() {
        return SellType;
    }
    public void setSellType(String aSellType) {
        SellType = aSellType;
    }
    public String getUWHierarchy() {
        return UWHierarchy;
    }
    public void setUWHierarchy(String aUWHierarchy) {
        UWHierarchy = aUWHierarchy;
    }
    public String getUWStyle() {
        return UWStyle;
    }
    public void setUWStyle(String aUWStyle) {
        UWStyle = aUWStyle;
    }
    public String getCheckFlag() {
        return CheckFlag;
    }
    public void setCheckFlag(String aCheckFlag) {
        CheckFlag = aCheckFlag;
    }

    /**
     * 取得Schema拥有字段的数量
     * @return: int
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("UWCode") ) {
            return 0;
        }
        if( strFieldName.equals("RiskCode") ) {
            return 1;
        }
        if( strFieldName.equals("RiskVer") ) {
            return 2;
        }
        if( strFieldName.equals("RiskName") ) {
            return 3;
        }
        if( strFieldName.equals("RelaPolType") ) {
            return 4;
        }
        if( strFieldName.equals("UWType") ) {
            return 5;
        }
        if( strFieldName.equals("UWOrder") ) {
            return 6;
        }
        if( strFieldName.equals("CalCode") ) {
            return 7;
        }
        if( strFieldName.equals("OthCalCode") ) {
            return 8;
        }
        if( strFieldName.equals("Remark") ) {
            return 9;
        }
        if( strFieldName.equals("UWGrade") ) {
            return 10;
        }
        if( strFieldName.equals("UWResult") ) {
            return 11;
        }
        if( strFieldName.equals("PassFlag") ) {
            return 12;
        }
        if( strFieldName.equals("SaleChnl") ) {
            return 13;
        }
        if( strFieldName.equals("SellType") ) {
            return 14;
        }
        if( strFieldName.equals("UWHierarchy") ) {
            return 15;
        }
        if( strFieldName.equals("UWStyle") ) {
            return 16;
        }
        if( strFieldName.equals("CheckFlag") ) {
            return 17;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     * @param: nFieldIndex int
     * @return: String
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "UWCode";
                break;
            case 1:
                strFieldName = "RiskCode";
                break;
            case 2:
                strFieldName = "RiskVer";
                break;
            case 3:
                strFieldName = "RiskName";
                break;
            case 4:
                strFieldName = "RelaPolType";
                break;
            case 5:
                strFieldName = "UWType";
                break;
            case 6:
                strFieldName = "UWOrder";
                break;
            case 7:
                strFieldName = "CalCode";
                break;
            case 8:
                strFieldName = "OthCalCode";
                break;
            case 9:
                strFieldName = "Remark";
                break;
            case 10:
                strFieldName = "UWGrade";
                break;
            case 11:
                strFieldName = "UWResult";
                break;
            case 12:
                strFieldName = "PassFlag";
                break;
            case 13:
                strFieldName = "SaleChnl";
                break;
            case 14:
                strFieldName = "SellType";
                break;
            case 15:
                strFieldName = "UWHierarchy";
                break;
            case 16:
                strFieldName = "UWStyle";
                break;
            case 17:
                strFieldName = "CheckFlag";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "UWCODE":
                return Schema.TYPE_STRING;
            case "RISKCODE":
                return Schema.TYPE_STRING;
            case "RISKVER":
                return Schema.TYPE_STRING;
            case "RISKNAME":
                return Schema.TYPE_STRING;
            case "RELAPOLTYPE":
                return Schema.TYPE_STRING;
            case "UWTYPE":
                return Schema.TYPE_STRING;
            case "UWORDER":
                return Schema.TYPE_INT;
            case "CALCODE":
                return Schema.TYPE_STRING;
            case "OTHCALCODE":
                return Schema.TYPE_STRING;
            case "REMARK":
                return Schema.TYPE_STRING;
            case "UWGRADE":
                return Schema.TYPE_STRING;
            case "UWRESULT":
                return Schema.TYPE_STRING;
            case "PASSFLAG":
                return Schema.TYPE_STRING;
            case "SALECHNL":
                return Schema.TYPE_STRING;
            case "SELLTYPE":
                return Schema.TYPE_STRING;
            case "UWHIERARCHY":
                return Schema.TYPE_STRING;
            case "UWSTYLE":
                return Schema.TYPE_STRING;
            case "CHECKFLAG":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: nFieldIndex int
     * @return: int
     **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_STRING;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_STRING;
            case 6:
                return Schema.TYPE_INT;
            case 7:
                return Schema.TYPE_STRING;
            case 8:
                return Schema.TYPE_STRING;
            case 9:
                return Schema.TYPE_STRING;
            case 10:
                return Schema.TYPE_STRING;
            case 11:
                return Schema.TYPE_STRING;
            case 12:
                return Schema.TYPE_STRING;
            case 13:
                return Schema.TYPE_STRING;
            case 14:
                return Schema.TYPE_STRING;
            case 15:
                return Schema.TYPE_STRING;
            case 16:
                return Schema.TYPE_STRING;
            case 17:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode String 希望取得的字段名
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("UWCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(UWCode));
        }
        if (FCode.equalsIgnoreCase("RiskCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RiskCode));
        }
        if (FCode.equalsIgnoreCase("RiskVer")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RiskVer));
        }
        if (FCode.equalsIgnoreCase("RiskName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RiskName));
        }
        if (FCode.equalsIgnoreCase("RelaPolType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RelaPolType));
        }
        if (FCode.equalsIgnoreCase("UWType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(UWType));
        }
        if (FCode.equalsIgnoreCase("UWOrder")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(UWOrder));
        }
        if (FCode.equalsIgnoreCase("CalCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CalCode));
        }
        if (FCode.equalsIgnoreCase("OthCalCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OthCalCode));
        }
        if (FCode.equalsIgnoreCase("Remark")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Remark));
        }
        if (FCode.equalsIgnoreCase("UWGrade")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(UWGrade));
        }
        if (FCode.equalsIgnoreCase("UWResult")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(UWResult));
        }
        if (FCode.equalsIgnoreCase("PassFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PassFlag));
        }
        if (FCode.equalsIgnoreCase("SaleChnl")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SaleChnl));
        }
        if (FCode.equalsIgnoreCase("SellType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SellType));
        }
        if (FCode.equalsIgnoreCase("UWHierarchy")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(UWHierarchy));
        }
        if (FCode.equalsIgnoreCase("UWStyle")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(UWStyle));
        }
        if (FCode.equalsIgnoreCase("CheckFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CheckFlag));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex int 指定的字段索引值
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = String.valueOf(UWCode);
                break;
            case 1:
                strFieldValue = String.valueOf(RiskCode);
                break;
            case 2:
                strFieldValue = String.valueOf(RiskVer);
                break;
            case 3:
                strFieldValue = String.valueOf(RiskName);
                break;
            case 4:
                strFieldValue = String.valueOf(RelaPolType);
                break;
            case 5:
                strFieldValue = String.valueOf(UWType);
                break;
            case 6:
                strFieldValue = String.valueOf(UWOrder);
                break;
            case 7:
                strFieldValue = String.valueOf(CalCode);
                break;
            case 8:
                strFieldValue = String.valueOf(OthCalCode);
                break;
            case 9:
                strFieldValue = String.valueOf(Remark);
                break;
            case 10:
                strFieldValue = String.valueOf(UWGrade);
                break;
            case 11:
                strFieldValue = String.valueOf(UWResult);
                break;
            case 12:
                strFieldValue = String.valueOf(PassFlag);
                break;
            case 13:
                strFieldValue = String.valueOf(SaleChnl);
                break;
            case 14:
                strFieldValue = String.valueOf(SellType);
                break;
            case 15:
                strFieldValue = String.valueOf(UWHierarchy);
                break;
            case 16:
                strFieldValue = String.valueOf(UWStyle);
                break;
            case 17:
                strFieldValue = String.valueOf(CheckFlag);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode String 需要赋值的对象
     * @param: FValue String 要赋的值
     * @return: boolean
     **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("UWCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                UWCode = FValue.trim();
            }
            else
                UWCode = null;
        }
        if (FCode.equalsIgnoreCase("RiskCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                RiskCode = FValue.trim();
            }
            else
                RiskCode = null;
        }
        if (FCode.equalsIgnoreCase("RiskVer")) {
            if( FValue != null && !FValue.equals(""))
            {
                RiskVer = FValue.trim();
            }
            else
                RiskVer = null;
        }
        if (FCode.equalsIgnoreCase("RiskName")) {
            if( FValue != null && !FValue.equals(""))
            {
                RiskName = FValue.trim();
            }
            else
                RiskName = null;
        }
        if (FCode.equalsIgnoreCase("RelaPolType")) {
            if( FValue != null && !FValue.equals(""))
            {
                RelaPolType = FValue.trim();
            }
            else
                RelaPolType = null;
        }
        if (FCode.equalsIgnoreCase("UWType")) {
            if( FValue != null && !FValue.equals(""))
            {
                UWType = FValue.trim();
            }
            else
                UWType = null;
        }
        if (FCode.equalsIgnoreCase("UWOrder")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                UWOrder = i;
            }
        }
        if (FCode.equalsIgnoreCase("CalCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                CalCode = FValue.trim();
            }
            else
                CalCode = null;
        }
        if (FCode.equalsIgnoreCase("OthCalCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                OthCalCode = FValue.trim();
            }
            else
                OthCalCode = null;
        }
        if (FCode.equalsIgnoreCase("Remark")) {
            if( FValue != null && !FValue.equals(""))
            {
                Remark = FValue.trim();
            }
            else
                Remark = null;
        }
        if (FCode.equalsIgnoreCase("UWGrade")) {
            if( FValue != null && !FValue.equals(""))
            {
                UWGrade = FValue.trim();
            }
            else
                UWGrade = null;
        }
        if (FCode.equalsIgnoreCase("UWResult")) {
            if( FValue != null && !FValue.equals(""))
            {
                UWResult = FValue.trim();
            }
            else
                UWResult = null;
        }
        if (FCode.equalsIgnoreCase("PassFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                PassFlag = FValue.trim();
            }
            else
                PassFlag = null;
        }
        if (FCode.equalsIgnoreCase("SaleChnl")) {
            if( FValue != null && !FValue.equals(""))
            {
                SaleChnl = FValue.trim();
            }
            else
                SaleChnl = null;
        }
        if (FCode.equalsIgnoreCase("SellType")) {
            if( FValue != null && !FValue.equals(""))
            {
                SellType = FValue.trim();
            }
            else
                SellType = null;
        }
        if (FCode.equalsIgnoreCase("UWHierarchy")) {
            if( FValue != null && !FValue.equals(""))
            {
                UWHierarchy = FValue.trim();
            }
            else
                UWHierarchy = null;
        }
        if (FCode.equalsIgnoreCase("UWStyle")) {
            if( FValue != null && !FValue.equals(""))
            {
                UWStyle = FValue.trim();
            }
            else
                UWStyle = null;
        }
        if (FCode.equalsIgnoreCase("CheckFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                CheckFlag = FValue.trim();
            }
            else
                CheckFlag = null;
        }
        return true;
    }


    public String toString() {
        return "LMUWPojo [" +
                "UWCode="+UWCode +
                ", RiskCode="+RiskCode +
                ", RiskVer="+RiskVer +
                ", RiskName="+RiskName +
                ", RelaPolType="+RelaPolType +
                ", UWType="+UWType +
                ", UWOrder="+UWOrder +
                ", CalCode="+CalCode +
                ", OthCalCode="+OthCalCode +
                ", Remark="+Remark +
                ", UWGrade="+UWGrade +
                ", UWResult="+UWResult +
                ", PassFlag="+PassFlag +
                ", SaleChnl="+SaleChnl +
                ", SellType="+SellType +
                ", UWHierarchy="+UWHierarchy +
                ", UWStyle="+UWStyle +
                ", CheckFlag="+CheckFlag +"]";
    }
}
