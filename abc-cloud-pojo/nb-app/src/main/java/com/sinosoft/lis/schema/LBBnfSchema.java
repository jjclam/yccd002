/**
 * Copyright (c) 2017 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import com.sinosoft.lis.db.LBBnfDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.*;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

/**
 * <p>ClassName: LBBnfSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2017</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2017-10-21
 */
public class LBBnfSchema implements Schema, Cloneable {
    // @Field
    /** Id */
    private long BnfID;
    /** Shardingid */
    private String ShardingID;
    /** 批单号 */
    private String EdorNo;
    /** 合同号码 */
    private String ContNo;
    /** 保单号码 */
    private String PolNo;
    /** 被保人客户号码 */
    private String InsuredNo;
    /** 受益人类别 */
    private String BnfType;
    /** 受益人序号 */
    private int BnfNo;
    /** 受益人级别 */
    private String BnfGrade;
    /** 与被保人关系 */
    private String RelationToInsured;
    /** 受益份额 */
    private double BnfLot;
    /** 客户号码 */
    private String CustomerNo;
    /** 客户姓名 */
    private String Name;
    /** 客户性别 */
    private String Sex;
    /** 客户出生日期 */
    private Date Birthday;
    /** 证件类型 */
    private String IDType;
    /** 证件号码 */
    private String IDNo;
    /** 操作员 */
    private String Operator;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 最后一次修改日期 */
    private Date ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime;
    /** 联系电话 */
    private String Tel;
    /** 联系地址 */
    private String Address;
    /** 邮编 */
    private String ZipCode;
    /** 证件有效期 */
    private String IdValiDate;
    /** 银行编码 */
    private String BankCode;
    /** 银行帐号 */
    private String BankAccNo;
    /** 银行帐户名 */
    private String AccName;

    public static final int FIELDNUM = 29;    // 数据库表的字段个数

    private static String[] PK;                // 主键

    private FDate fDate = new FDate();        // 处理日期

    public CErrors mErrors;            // 错误信息

    // @Constructor
    public LBBnfSchema() {
        mErrors = new CErrors();

        String[] pk = new String[1];
        pk[0] = "BnfID";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException {
        LBBnfSchema cloned = (LBBnfSchema)super.clone();
        cloned.fDate = (FDate) fDate.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK() {
        return PK;
    }

    public long getBnfID() {
        return BnfID;
    }
    public void setBnfID(long aBnfID) {
        BnfID = aBnfID;
    }
    public void setBnfID(String aBnfID) {
        if (aBnfID != null && !aBnfID.equals("")) {
            BnfID = new Long(aBnfID).longValue();
        }
    }

    public String getShardingID() {
        return ShardingID;
    }
    public void setShardingID(String aShardingID) {
        ShardingID = aShardingID;
    }
    public String getEdorNo() {
        return EdorNo;
    }
    public void setEdorNo(String aEdorNo) {
        EdorNo = aEdorNo;
    }
    public String getContNo() {
        return ContNo;
    }
    public void setContNo(String aContNo) {
        ContNo = aContNo;
    }
    public String getPolNo() {
        return PolNo;
    }
    public void setPolNo(String aPolNo) {
        PolNo = aPolNo;
    }
    public String getInsuredNo() {
        return InsuredNo;
    }
    public void setInsuredNo(String aInsuredNo) {
        InsuredNo = aInsuredNo;
    }
    public String getBnfType() {
        return BnfType;
    }
    public void setBnfType(String aBnfType) {
        BnfType = aBnfType;
    }
    public int getBnfNo() {
        return BnfNo;
    }
    public void setBnfNo(int aBnfNo) {
        BnfNo = aBnfNo;
    }
    public void setBnfNo(String aBnfNo) {
        if (aBnfNo != null && !aBnfNo.equals("")) {
            Integer tInteger = new Integer(aBnfNo);
            int i = tInteger.intValue();
            BnfNo = i;
        }
    }

    public String getBnfGrade() {
        return BnfGrade;
    }
    public void setBnfGrade(String aBnfGrade) {
        BnfGrade = aBnfGrade;
    }
    public String getRelationToInsured() {
        return RelationToInsured;
    }
    public void setRelationToInsured(String aRelationToInsured) {
        RelationToInsured = aRelationToInsured;
    }
    public double getBnfLot() {
        return BnfLot;
    }
    public void setBnfLot(double aBnfLot) {
        BnfLot = aBnfLot;
    }
    public void setBnfLot(String aBnfLot) {
        if (aBnfLot != null && !aBnfLot.equals("")) {
            Double tDouble = new Double(aBnfLot);
            double d = tDouble.doubleValue();
            BnfLot = d;
        }
    }

    public String getCustomerNo() {
        return CustomerNo;
    }
    public void setCustomerNo(String aCustomerNo) {
        CustomerNo = aCustomerNo;
    }
    public String getName() {
        return Name;
    }
    public void setName(String aName) {
        Name = aName;
    }
    public String getSex() {
        return Sex;
    }
    public void setSex(String aSex) {
        Sex = aSex;
    }
    public String getBirthday() {
        if(Birthday != null) {
            return fDate.getString(Birthday);
        } else {
            return null;
        }
    }
    public void setBirthday(Date aBirthday) {
        Birthday = aBirthday;
    }
    public void setBirthday(String aBirthday) {
        if (aBirthday != null && !aBirthday.equals("")) {
            Birthday = fDate.getDate(aBirthday);
        } else
            Birthday = null;
    }

    public String getIDType() {
        return IDType;
    }
    public void setIDType(String aIDType) {
        IDType = aIDType;
    }
    public String getIDNo() {
        return IDNo;
    }
    public void setIDNo(String aIDNo) {
        IDNo = aIDNo;
    }
    public String getOperator() {
        return Operator;
    }
    public void setOperator(String aOperator) {
        Operator = aOperator;
    }
    public String getMakeDate() {
        if(MakeDate != null) {
            return fDate.getString(MakeDate);
        } else {
            return null;
        }
    }
    public void setMakeDate(Date aMakeDate) {
        MakeDate = aMakeDate;
    }
    public void setMakeDate(String aMakeDate) {
        if (aMakeDate != null && !aMakeDate.equals("")) {
            MakeDate = fDate.getDate(aMakeDate);
        } else
            MakeDate = null;
    }

    public String getMakeTime() {
        return MakeTime;
    }
    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }
    public String getModifyDate() {
        if(ModifyDate != null) {
            return fDate.getString(ModifyDate);
        } else {
            return null;
        }
    }
    public void setModifyDate(Date aModifyDate) {
        ModifyDate = aModifyDate;
    }
    public void setModifyDate(String aModifyDate) {
        if (aModifyDate != null && !aModifyDate.equals("")) {
            ModifyDate = fDate.getDate(aModifyDate);
        } else
            ModifyDate = null;
    }

    public String getModifyTime() {
        return ModifyTime;
    }
    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }
    public String getTel() {
        return Tel;
    }
    public void setTel(String aTel) {
        Tel = aTel;
    }
    public String getAddress() {
        return Address;
    }
    public void setAddress(String aAddress) {
        Address = aAddress;
    }
    public String getZipCode() {
        return ZipCode;
    }
    public void setZipCode(String aZipCode) {
        ZipCode = aZipCode;
    }
    public String getIdValiDate() {
        return IdValiDate;
    }
    public void setIdValiDate(String aIdValiDate) {
        IdValiDate = aIdValiDate;
    }
    public String getBankCode() {
        return BankCode;
    }
    public void setBankCode(String aBankCode) {
        BankCode = aBankCode;
    }
    public String getBankAccNo() {
        return BankAccNo;
    }
    public void setBankAccNo(String aBankAccNo) {
        BankAccNo = aBankAccNo;
    }
    public String getAccName() {
        return AccName;
    }
    public void setAccName(String aAccName) {
        AccName = aAccName;
    }

    /**
    * 使用另外一个 LBBnfSchema 对象给 Schema 赋值
    * @param: aLBBnfSchema LBBnfSchema
    **/
    public void setSchema(LBBnfSchema aLBBnfSchema) {
        this.BnfID = aLBBnfSchema.getBnfID();
        this.ShardingID = aLBBnfSchema.getShardingID();
        this.EdorNo = aLBBnfSchema.getEdorNo();
        this.ContNo = aLBBnfSchema.getContNo();
        this.PolNo = aLBBnfSchema.getPolNo();
        this.InsuredNo = aLBBnfSchema.getInsuredNo();
        this.BnfType = aLBBnfSchema.getBnfType();
        this.BnfNo = aLBBnfSchema.getBnfNo();
        this.BnfGrade = aLBBnfSchema.getBnfGrade();
        this.RelationToInsured = aLBBnfSchema.getRelationToInsured();
        this.BnfLot = aLBBnfSchema.getBnfLot();
        this.CustomerNo = aLBBnfSchema.getCustomerNo();
        this.Name = aLBBnfSchema.getName();
        this.Sex = aLBBnfSchema.getSex();
        this.Birthday = fDate.getDate( aLBBnfSchema.getBirthday());
        this.IDType = aLBBnfSchema.getIDType();
        this.IDNo = aLBBnfSchema.getIDNo();
        this.Operator = aLBBnfSchema.getOperator();
        this.MakeDate = fDate.getDate( aLBBnfSchema.getMakeDate());
        this.MakeTime = aLBBnfSchema.getMakeTime();
        this.ModifyDate = fDate.getDate( aLBBnfSchema.getModifyDate());
        this.ModifyTime = aLBBnfSchema.getModifyTime();
        this.Tel = aLBBnfSchema.getTel();
        this.Address = aLBBnfSchema.getAddress();
        this.ZipCode = aLBBnfSchema.getZipCode();
        this.IdValiDate = aLBBnfSchema.getIdValiDate();
        this.BankCode = aLBBnfSchema.getBankCode();
        this.BankAccNo = aLBBnfSchema.getBankAccNo();
        this.AccName = aLBBnfSchema.getAccName();
    }

    /**
    * 使用 ResultSet 中的第 i 行给 Schema 赋值
    * @param: rs ResultSet
    * @param: i int
    * @return: boolean
    **/
    public boolean setSchema(ResultSet rs,int i) {
        try {
            //rs.absolute(i);        // 非滚动游标
            this.BnfID = rs.getLong("BnfID");
            if( rs.getString("ShardingID") == null )
                this.ShardingID = null;
            else
                this.ShardingID = rs.getString("ShardingID").trim();

            if( rs.getString("EdorNo") == null )
                this.EdorNo = null;
            else
                this.EdorNo = rs.getString("EdorNo").trim();

            if( rs.getString("ContNo") == null )
                this.ContNo = null;
            else
                this.ContNo = rs.getString("ContNo").trim();

            if( rs.getString("PolNo") == null )
                this.PolNo = null;
            else
                this.PolNo = rs.getString("PolNo").trim();

            if( rs.getString("InsuredNo") == null )
                this.InsuredNo = null;
            else
                this.InsuredNo = rs.getString("InsuredNo").trim();

            if( rs.getString("BnfType") == null )
                this.BnfType = null;
            else
                this.BnfType = rs.getString("BnfType").trim();

            this.BnfNo = rs.getInt("BnfNo");
            if( rs.getString("BnfGrade") == null )
                this.BnfGrade = null;
            else
                this.BnfGrade = rs.getString("BnfGrade").trim();

            if( rs.getString("RelationToInsured") == null )
                this.RelationToInsured = null;
            else
                this.RelationToInsured = rs.getString("RelationToInsured").trim();

            this.BnfLot = rs.getDouble("BnfLot");
            if( rs.getString("CustomerNo") == null )
                this.CustomerNo = null;
            else
                this.CustomerNo = rs.getString("CustomerNo").trim();

            if( rs.getString("Name") == null )
                this.Name = null;
            else
                this.Name = rs.getString("Name").trim();

            if( rs.getString("Sex") == null )
                this.Sex = null;
            else
                this.Sex = rs.getString("Sex").trim();

            this.Birthday = rs.getDate("Birthday");
            if( rs.getString("IDType") == null )
                this.IDType = null;
            else
                this.IDType = rs.getString("IDType").trim();

            if( rs.getString("IDNo") == null )
                this.IDNo = null;
            else
                this.IDNo = rs.getString("IDNo").trim();

            if( rs.getString("Operator") == null )
                this.Operator = null;
            else
                this.Operator = rs.getString("Operator").trim();

            this.MakeDate = rs.getDate("MakeDate");
            if( rs.getString("MakeTime") == null )
                this.MakeTime = null;
            else
                this.MakeTime = rs.getString("MakeTime").trim();

            this.ModifyDate = rs.getDate("ModifyDate");
            if( rs.getString("ModifyTime") == null )
                this.ModifyTime = null;
            else
                this.ModifyTime = rs.getString("ModifyTime").trim();

            if( rs.getString("Tel") == null )
                this.Tel = null;
            else
                this.Tel = rs.getString("Tel").trim();

            if( rs.getString("Address") == null )
                this.Address = null;
            else
                this.Address = rs.getString("Address").trim();

            if( rs.getString("ZipCode") == null )
                this.ZipCode = null;
            else
                this.ZipCode = rs.getString("ZipCode").trim();

            if( rs.getString("IdValiDate") == null )
                this.IdValiDate = null;
            else
                this.IdValiDate = rs.getString("IdValiDate").trim();

            if( rs.getString("BankCode") == null )
                this.BankCode = null;
            else
                this.BankCode = rs.getString("BankCode").trim();

            if( rs.getString("BankAccNo") == null )
                this.BankAccNo = null;
            else
                this.BankAccNo = rs.getString("BankAccNo").trim();

            if( rs.getString("AccName") == null )
                this.AccName = null;
            else
                this.AccName = rs.getString("AccName").trim();

        }
        catch(SQLException sqle) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LBBnfSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = ExceptionUtils.exceptionToString(sqle);
            this.mErrors .addOneError(tError);
            return false;
        }
        return true;
    }

    public LBBnfSchema getSchema() {
        LBBnfSchema aLBBnfSchema = new LBBnfSchema();
        aLBBnfSchema.setSchema(this);
        return aLBBnfSchema;
    }

    public LBBnfDB getDB() {
        LBBnfDB aDBOper = new LBBnfDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
    * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLBBnf描述/A>表字段
    * @return: String 返回打包后字符串
    **/
    public String encode() {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(ChgData.chgData(BnfID));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ShardingID)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(EdorNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ContNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(PolNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(InsuredNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(BnfType)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(BnfNo));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(BnfGrade)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(RelationToInsured)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(BnfLot));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(CustomerNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Name)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Sex)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( Birthday ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(IDType)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(IDNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ModifyTime)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Tel)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Address)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ZipCode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(IdValiDate)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(BankCode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(BankAccNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AccName));
        return strReturn.toString();
    }

    /**
    * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLBBnf>历史记账凭证主表信息</A>表字段
    * @param: strMessage String 包含一条纪录数据的字符串
    * @return: boolean
    **/
    public boolean decode(String strMessage) {
        try {
            BnfID = new Long(ChgData.chgNumericStr(StrTool.getStr(strMessage,1, SysConst.PACKAGESPILTER))).longValue();
            ShardingID = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
            EdorNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
            ContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
            PolNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
            InsuredNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
            BnfType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
            BnfNo = new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,8, SysConst.PACKAGESPILTER))).intValue();
            BnfGrade = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
            RelationToInsured = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
            BnfLot = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,11, SysConst.PACKAGESPILTER))).doubleValue();
            CustomerNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
            Name = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
            Sex = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER );
            Birthday = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15, SysConst.PACKAGESPILTER));
            IDType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16, SysConst.PACKAGESPILTER );
            IDNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17, SysConst.PACKAGESPILTER );
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18, SysConst.PACKAGESPILTER );
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20, SysConst.PACKAGESPILTER );
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 21, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 22, SysConst.PACKAGESPILTER );
            Tel = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 23, SysConst.PACKAGESPILTER );
            Address = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 24, SysConst.PACKAGESPILTER );
            ZipCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 25, SysConst.PACKAGESPILTER );
            IdValiDate = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 26, SysConst.PACKAGESPILTER );
            BankCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 27, SysConst.PACKAGESPILTER );
            BankAccNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 28, SysConst.PACKAGESPILTER );
            AccName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 29, SysConst.PACKAGESPILTER );
        }
        catch(NumberFormatException ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LBBnfSchema";
            tError.functionName = "decode";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            return false;
        }
        return true;
    }

    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("BnfID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BnfID));
        }
        if (FCode.equalsIgnoreCase("ShardingID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ShardingID));
        }
        if (FCode.equalsIgnoreCase("EdorNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(EdorNo));
        }
        if (FCode.equalsIgnoreCase("ContNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ContNo));
        }
        if (FCode.equalsIgnoreCase("PolNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PolNo));
        }
        if (FCode.equalsIgnoreCase("InsuredNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InsuredNo));
        }
        if (FCode.equalsIgnoreCase("BnfType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BnfType));
        }
        if (FCode.equalsIgnoreCase("BnfNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BnfNo));
        }
        if (FCode.equalsIgnoreCase("BnfGrade")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BnfGrade));
        }
        if (FCode.equalsIgnoreCase("RelationToInsured")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RelationToInsured));
        }
        if (FCode.equalsIgnoreCase("BnfLot")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BnfLot));
        }
        if (FCode.equalsIgnoreCase("CustomerNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CustomerNo));
        }
        if (FCode.equalsIgnoreCase("Name")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Name));
        }
        if (FCode.equalsIgnoreCase("Sex")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Sex));
        }
        if (FCode.equalsIgnoreCase("Birthday")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getBirthday()));
        }
        if (FCode.equalsIgnoreCase("IDType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(IDType));
        }
        if (FCode.equalsIgnoreCase("IDNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(IDNo));
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (FCode.equalsIgnoreCase("Tel")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Tel));
        }
        if (FCode.equalsIgnoreCase("Address")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Address));
        }
        if (FCode.equalsIgnoreCase("ZipCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ZipCode));
        }
        if (FCode.equalsIgnoreCase("IdValiDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(IdValiDate));
        }
        if (FCode.equalsIgnoreCase("BankCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BankCode));
        }
        if (FCode.equalsIgnoreCase("BankAccNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BankAccNo));
        }
        if (FCode.equalsIgnoreCase("AccName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AccName));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = String.valueOf(BnfID);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(ShardingID);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(EdorNo);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(ContNo);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(PolNo);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(InsuredNo);
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(BnfType);
                break;
            case 7:
                strFieldValue = String.valueOf(BnfNo);
                break;
            case 8:
                strFieldValue = StrTool.GBKToUnicode(BnfGrade);
                break;
            case 9:
                strFieldValue = StrTool.GBKToUnicode(RelationToInsured);
                break;
            case 10:
                strFieldValue = String.valueOf(BnfLot);
                break;
            case 11:
                strFieldValue = StrTool.GBKToUnicode(CustomerNo);
                break;
            case 12:
                strFieldValue = StrTool.GBKToUnicode(Name);
                break;
            case 13:
                strFieldValue = StrTool.GBKToUnicode(Sex);
                break;
            case 14:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getBirthday()));
                break;
            case 15:
                strFieldValue = StrTool.GBKToUnicode(IDType);
                break;
            case 16:
                strFieldValue = StrTool.GBKToUnicode(IDNo);
                break;
            case 17:
                strFieldValue = StrTool.GBKToUnicode(Operator);
                break;
            case 18:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
                break;
            case 19:
                strFieldValue = StrTool.GBKToUnicode(MakeTime);
                break;
            case 20:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
                break;
            case 21:
                strFieldValue = StrTool.GBKToUnicode(ModifyTime);
                break;
            case 22:
                strFieldValue = StrTool.GBKToUnicode(Tel);
                break;
            case 23:
                strFieldValue = StrTool.GBKToUnicode(Address);
                break;
            case 24:
                strFieldValue = StrTool.GBKToUnicode(ZipCode);
                break;
            case 25:
                strFieldValue = StrTool.GBKToUnicode(IdValiDate);
                break;
            case 26:
                strFieldValue = StrTool.GBKToUnicode(BankCode);
                break;
            case 27:
                strFieldValue = StrTool.GBKToUnicode(BankAccNo);
                break;
            case 28:
                strFieldValue = StrTool.GBKToUnicode(AccName);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("BnfID")) {
            if( FValue != null && !FValue.equals("")) {
                BnfID = new Long(FValue).longValue();
            }
        }
        if (FCode.equalsIgnoreCase("ShardingID")) {
            if( FValue != null && !FValue.equals(""))
            {
                ShardingID = FValue.trim();
            }
            else
                ShardingID = null;
        }
        if (FCode.equalsIgnoreCase("EdorNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                EdorNo = FValue.trim();
            }
            else
                EdorNo = null;
        }
        if (FCode.equalsIgnoreCase("ContNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                ContNo = FValue.trim();
            }
            else
                ContNo = null;
        }
        if (FCode.equalsIgnoreCase("PolNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                PolNo = FValue.trim();
            }
            else
                PolNo = null;
        }
        if (FCode.equalsIgnoreCase("InsuredNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                InsuredNo = FValue.trim();
            }
            else
                InsuredNo = null;
        }
        if (FCode.equalsIgnoreCase("BnfType")) {
            if( FValue != null && !FValue.equals(""))
            {
                BnfType = FValue.trim();
            }
            else
                BnfType = null;
        }
        if (FCode.equalsIgnoreCase("BnfNo")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                BnfNo = i;
            }
        }
        if (FCode.equalsIgnoreCase("BnfGrade")) {
            if( FValue != null && !FValue.equals(""))
            {
                BnfGrade = FValue.trim();
            }
            else
                BnfGrade = null;
        }
        if (FCode.equalsIgnoreCase("RelationToInsured")) {
            if( FValue != null && !FValue.equals(""))
            {
                RelationToInsured = FValue.trim();
            }
            else
                RelationToInsured = null;
        }
        if (FCode.equalsIgnoreCase("BnfLot")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                BnfLot = d;
            }
        }
        if (FCode.equalsIgnoreCase("CustomerNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                CustomerNo = FValue.trim();
            }
            else
                CustomerNo = null;
        }
        if (FCode.equalsIgnoreCase("Name")) {
            if( FValue != null && !FValue.equals(""))
            {
                Name = FValue.trim();
            }
            else
                Name = null;
        }
        if (FCode.equalsIgnoreCase("Sex")) {
            if( FValue != null && !FValue.equals(""))
            {
                Sex = FValue.trim();
            }
            else
                Sex = null;
        }
        if (FCode.equalsIgnoreCase("Birthday")) {
            if(FValue != null && !FValue.equals("")) {
                Birthday = fDate.getDate( FValue );
            }
            else
                Birthday = null;
        }
        if (FCode.equalsIgnoreCase("IDType")) {
            if( FValue != null && !FValue.equals(""))
            {
                IDType = FValue.trim();
            }
            else
                IDType = null;
        }
        if (FCode.equalsIgnoreCase("IDNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                IDNo = FValue.trim();
            }
            else
                IDNo = null;
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            if( FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
                Operator = null;
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if(FValue != null && !FValue.equals("")) {
                MakeDate = fDate.getDate( FValue );
            }
            else
                MakeDate = null;
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
                MakeTime = null;
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if(FValue != null && !FValue.equals("")) {
                ModifyDate = fDate.getDate( FValue );
            }
            else
                ModifyDate = null;
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
                ModifyTime = null;
        }
        if (FCode.equalsIgnoreCase("Tel")) {
            if( FValue != null && !FValue.equals(""))
            {
                Tel = FValue.trim();
            }
            else
                Tel = null;
        }
        if (FCode.equalsIgnoreCase("Address")) {
            if( FValue != null && !FValue.equals(""))
            {
                Address = FValue.trim();
            }
            else
                Address = null;
        }
        if (FCode.equalsIgnoreCase("ZipCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                ZipCode = FValue.trim();
            }
            else
                ZipCode = null;
        }
        if (FCode.equalsIgnoreCase("IdValiDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                IdValiDate = FValue.trim();
            }
            else
                IdValiDate = null;
        }
        if (FCode.equalsIgnoreCase("BankCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                BankCode = FValue.trim();
            }
            else
                BankCode = null;
        }
        if (FCode.equalsIgnoreCase("BankAccNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                BankAccNo = FValue.trim();
            }
            else
                BankAccNo = null;
        }
        if (FCode.equalsIgnoreCase("AccName")) {
            if( FValue != null && !FValue.equals(""))
            {
                AccName = FValue.trim();
            }
            else
                AccName = null;
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject) return true;
        if (otherObject == null) return false;
        if (getClass() != otherObject.getClass()) return false;
        LBBnfSchema other = (LBBnfSchema)otherObject;
        return
            BnfID == other.getBnfID()
            && ShardingID.equals(other.getShardingID())
            && EdorNo.equals(other.getEdorNo())
            && ContNo.equals(other.getContNo())
            && PolNo.equals(other.getPolNo())
            && InsuredNo.equals(other.getInsuredNo())
            && BnfType.equals(other.getBnfType())
            && BnfNo == other.getBnfNo()
            && BnfGrade.equals(other.getBnfGrade())
            && RelationToInsured.equals(other.getRelationToInsured())
            && BnfLot == other.getBnfLot()
            && CustomerNo.equals(other.getCustomerNo())
            && Name.equals(other.getName())
            && Sex.equals(other.getSex())
            && fDate.getString(Birthday).equals(other.getBirthday())
            && IDType.equals(other.getIDType())
            && IDNo.equals(other.getIDNo())
            && Operator.equals(other.getOperator())
            && fDate.getString(MakeDate).equals(other.getMakeDate())
            && MakeTime.equals(other.getMakeTime())
            && fDate.getString(ModifyDate).equals(other.getModifyDate())
            && ModifyTime.equals(other.getModifyTime())
            && Tel.equals(other.getTel())
            && Address.equals(other.getAddress())
            && ZipCode.equals(other.getZipCode())
            && IdValiDate.equals(other.getIdValiDate())
            && BankCode.equals(other.getBankCode())
            && BankAccNo.equals(other.getBankAccNo())
            && AccName.equals(other.getAccName());
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("BnfID") ) {
            return 0;
        }
        if( strFieldName.equals("ShardingID") ) {
            return 1;
        }
        if( strFieldName.equals("EdorNo") ) {
            return 2;
        }
        if( strFieldName.equals("ContNo") ) {
            return 3;
        }
        if( strFieldName.equals("PolNo") ) {
            return 4;
        }
        if( strFieldName.equals("InsuredNo") ) {
            return 5;
        }
        if( strFieldName.equals("BnfType") ) {
            return 6;
        }
        if( strFieldName.equals("BnfNo") ) {
            return 7;
        }
        if( strFieldName.equals("BnfGrade") ) {
            return 8;
        }
        if( strFieldName.equals("RelationToInsured") ) {
            return 9;
        }
        if( strFieldName.equals("BnfLot") ) {
            return 10;
        }
        if( strFieldName.equals("CustomerNo") ) {
            return 11;
        }
        if( strFieldName.equals("Name") ) {
            return 12;
        }
        if( strFieldName.equals("Sex") ) {
            return 13;
        }
        if( strFieldName.equals("Birthday") ) {
            return 14;
        }
        if( strFieldName.equals("IDType") ) {
            return 15;
        }
        if( strFieldName.equals("IDNo") ) {
            return 16;
        }
        if( strFieldName.equals("Operator") ) {
            return 17;
        }
        if( strFieldName.equals("MakeDate") ) {
            return 18;
        }
        if( strFieldName.equals("MakeTime") ) {
            return 19;
        }
        if( strFieldName.equals("ModifyDate") ) {
            return 20;
        }
        if( strFieldName.equals("ModifyTime") ) {
            return 21;
        }
        if( strFieldName.equals("Tel") ) {
            return 22;
        }
        if( strFieldName.equals("Address") ) {
            return 23;
        }
        if( strFieldName.equals("ZipCode") ) {
            return 24;
        }
        if( strFieldName.equals("IdValiDate") ) {
            return 25;
        }
        if( strFieldName.equals("BankCode") ) {
            return 26;
        }
        if( strFieldName.equals("BankAccNo") ) {
            return 27;
        }
        if( strFieldName.equals("AccName") ) {
            return 28;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "BnfID";
                break;
            case 1:
                strFieldName = "ShardingID";
                break;
            case 2:
                strFieldName = "EdorNo";
                break;
            case 3:
                strFieldName = "ContNo";
                break;
            case 4:
                strFieldName = "PolNo";
                break;
            case 5:
                strFieldName = "InsuredNo";
                break;
            case 6:
                strFieldName = "BnfType";
                break;
            case 7:
                strFieldName = "BnfNo";
                break;
            case 8:
                strFieldName = "BnfGrade";
                break;
            case 9:
                strFieldName = "RelationToInsured";
                break;
            case 10:
                strFieldName = "BnfLot";
                break;
            case 11:
                strFieldName = "CustomerNo";
                break;
            case 12:
                strFieldName = "Name";
                break;
            case 13:
                strFieldName = "Sex";
                break;
            case 14:
                strFieldName = "Birthday";
                break;
            case 15:
                strFieldName = "IDType";
                break;
            case 16:
                strFieldName = "IDNo";
                break;
            case 17:
                strFieldName = "Operator";
                break;
            case 18:
                strFieldName = "MakeDate";
                break;
            case 19:
                strFieldName = "MakeTime";
                break;
            case 20:
                strFieldName = "ModifyDate";
                break;
            case 21:
                strFieldName = "ModifyTime";
                break;
            case 22:
                strFieldName = "Tel";
                break;
            case 23:
                strFieldName = "Address";
                break;
            case 24:
                strFieldName = "ZipCode";
                break;
            case 25:
                strFieldName = "IdValiDate";
                break;
            case 26:
                strFieldName = "BankCode";
                break;
            case 27:
                strFieldName = "BankAccNo";
                break;
            case 28:
                strFieldName = "AccName";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "BNFID":
                return Schema.TYPE_LONG;
            case "SHARDINGID":
                return Schema.TYPE_STRING;
            case "EDORNO":
                return Schema.TYPE_STRING;
            case "CONTNO":
                return Schema.TYPE_STRING;
            case "POLNO":
                return Schema.TYPE_STRING;
            case "INSUREDNO":
                return Schema.TYPE_STRING;
            case "BNFTYPE":
                return Schema.TYPE_STRING;
            case "BNFNO":
                return Schema.TYPE_INT;
            case "BNFGRADE":
                return Schema.TYPE_STRING;
            case "RELATIONTOINSURED":
                return Schema.TYPE_STRING;
            case "BNFLOT":
                return Schema.TYPE_DOUBLE;
            case "CUSTOMERNO":
                return Schema.TYPE_STRING;
            case "NAME":
                return Schema.TYPE_STRING;
            case "SEX":
                return Schema.TYPE_STRING;
            case "BIRTHDAY":
                return Schema.TYPE_DATE;
            case "IDTYPE":
                return Schema.TYPE_STRING;
            case "IDNO":
                return Schema.TYPE_STRING;
            case "OPERATOR":
                return Schema.TYPE_STRING;
            case "MAKEDATE":
                return Schema.TYPE_DATE;
            case "MAKETIME":
                return Schema.TYPE_STRING;
            case "MODIFYDATE":
                return Schema.TYPE_DATE;
            case "MODIFYTIME":
                return Schema.TYPE_STRING;
            case "TEL":
                return Schema.TYPE_STRING;
            case "ADDRESS":
                return Schema.TYPE_STRING;
            case "ZIPCODE":
                return Schema.TYPE_STRING;
            case "IDVALIDATE":
                return Schema.TYPE_STRING;
            case "BANKCODE":
                return Schema.TYPE_STRING;
            case "BANKACCNO":
                return Schema.TYPE_STRING;
            case "ACCNAME":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_LONG;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_STRING;
            case 6:
                return Schema.TYPE_STRING;
            case 7:
                return Schema.TYPE_INT;
            case 8:
                return Schema.TYPE_STRING;
            case 9:
                return Schema.TYPE_STRING;
            case 10:
                return Schema.TYPE_DOUBLE;
            case 11:
                return Schema.TYPE_STRING;
            case 12:
                return Schema.TYPE_STRING;
            case 13:
                return Schema.TYPE_STRING;
            case 14:
                return Schema.TYPE_DATE;
            case 15:
                return Schema.TYPE_STRING;
            case 16:
                return Schema.TYPE_STRING;
            case 17:
                return Schema.TYPE_STRING;
            case 18:
                return Schema.TYPE_DATE;
            case 19:
                return Schema.TYPE_STRING;
            case 20:
                return Schema.TYPE_DATE;
            case 21:
                return Schema.TYPE_STRING;
            case 22:
                return Schema.TYPE_STRING;
            case 23:
                return Schema.TYPE_STRING;
            case 24:
                return Schema.TYPE_STRING;
            case 25:
                return Schema.TYPE_STRING;
            case 26:
                return Schema.TYPE_STRING;
            case 27:
                return Schema.TYPE_STRING;
            case 28:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
}
