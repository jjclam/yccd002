/**
 * Copyright (c) 2018 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import com.sinosoft.lis.db.LCPolicyInfoDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.*;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

/**
 * <p>ClassName: LCPolicyInfoSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2018</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2018-07-09
 */
public class LCPolicyInfoSchema implements Schema, Cloneable {
    // @Field
    /** 个人保单号 */
    private String ContNo;
    /** 个人客户号 */
    private String InsuredNo;
    /** 保险计划编码 */
    private String ContplanCode;
    /** 套餐名称 */
    private String ContplanName;
    /** 基本保费 */
    private double BasePrem;
    /** 加费 */
    private double AddPrem;
    /** 激活日期 */
    private Date ActiveDate;
    /** 保险期间 */
    private int InsuYear;
    /** 保险期间单位 */
    private String InsuYearFlag;
    /** 生效间隔 */
    private int Cvaliintv;
    /** 生效间隔单位 */
    private String CvaliintvFlag;
    /** 被保人中介平台客户号 */
    private String SisinSuredNo;
    /** 出访国家 */
    private String TraveLcountry;
    /** 接口网点ip地址 */
    private String AgentIp;
    /** 航班号 */
    private String FlightNo;
    /** 境外救援卡号 */
    private String RescuecardNo;
    /** 经办人 */
    private String HandlerName;
    /** 经办人电话 */
    private String HandlerPhone;
    /** 签单日期 */
    private Date SignDate;
    /** 签单时间 */
    private String SignTime;
    /** 网点地址编码 */
    private String agentcom;
    /** 险类 */
    private String RiskType;
    /** 出单方式 */
    private String OrderType;
    /** 借款合同编号 */
    private String JYContNo;
    /** 借款凭证编号 */
    private String JYCertNo;
    /** 借款（起始）日期 */
    private String JYStartDate;
    /** 借款（终止）到期 */
    private String JYEndDate;
    /** 借意险贷款金额 */
    private String LoanAmount;
    /** 借意险贷款发放机构 */
    private String LendCom;
    /** 借款人性质 */
    private String LoanerNature;
    /** 借款人性质名称 */
    private String LoanerNatureName;
    /** 借意险借款期限（yd套餐） */
    private String LendTerm;
    /** 境外救援出访国家代码 */
    private String CountryCode;
    /** 燃气公司 */
    private String GasCompany;
    /** 燃气用户地址 */
    private String GasUserAddress;
    /** 学平险约定赔付比例 */
    private String PayoutPro;
    /** 学平险学校名称 */
    private String School;
    /** 学平险保费收费确认时间 */
    private String ChargeDate;
    /** 溜冰标记为1 */
    private String Mark;

    public static final int FIELDNUM = 39;    // 数据库表的字段个数

    private static String[] PK;                // 主键

    private FDate fDate = new FDate();        // 处理日期

    public CErrors mErrors;            // 错误信息

    // @Constructor
    public LCPolicyInfoSchema() {
        mErrors = new CErrors();

        String[] pk = new String[2];
        pk[0] = "ContNo";
        pk[1] = "InsuredNo";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException {
        LCPolicyInfoSchema cloned = (LCPolicyInfoSchema)super.clone();
        cloned.fDate = (FDate) fDate.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK() {
        return PK;
    }

    public String getContNo() {
        return ContNo;
    }
    public void setContNo(String aContNo) {
        ContNo = aContNo;
    }
    public String getInsuredNo() {
        return InsuredNo;
    }
    public void setInsuredNo(String aInsuredNo) {
        InsuredNo = aInsuredNo;
    }
    public String getContplanCode() {
        return ContplanCode;
    }
    public void setContplanCode(String aContplanCode) {
        ContplanCode = aContplanCode;
    }
    public String getContplanName() {
        return ContplanName;
    }
    public void setContplanName(String aContplanName) {
        ContplanName = aContplanName;
    }
    public double getBasePrem() {
        return BasePrem;
    }
    public void setBasePrem(double aBasePrem) {
        BasePrem = aBasePrem;
    }
    public void setBasePrem(String aBasePrem) {
        if (aBasePrem != null && !aBasePrem.equals("")) {
            Double tDouble = new Double(aBasePrem);
            double d = tDouble.doubleValue();
            BasePrem = d;
        }
    }

    public double getAddPrem() {
        return AddPrem;
    }
    public void setAddPrem(double aAddPrem) {
        AddPrem = aAddPrem;
    }
    public void setAddPrem(String aAddPrem) {
        if (aAddPrem != null && !aAddPrem.equals("")) {
            Double tDouble = new Double(aAddPrem);
            double d = tDouble.doubleValue();
            AddPrem = d;
        }
    }

    public String getActiveDate() {
        if(ActiveDate != null) {
            return fDate.getString(ActiveDate);
        } else {
            return null;
        }
    }
    public void setActiveDate(Date aActiveDate) {
        ActiveDate = aActiveDate;
    }
    public void setActiveDate(String aActiveDate) {
        if (aActiveDate != null && !aActiveDate.equals("")) {
            ActiveDate = fDate.getDate(aActiveDate);
        } else
            ActiveDate = null;
    }

    public int getInsuYear() {
        return InsuYear;
    }
    public void setInsuYear(int aInsuYear) {
        InsuYear = aInsuYear;
    }
    public void setInsuYear(String aInsuYear) {
        if (aInsuYear != null && !aInsuYear.equals("")) {
            Integer tInteger = new Integer(aInsuYear);
            int i = tInteger.intValue();
            InsuYear = i;
        }
    }

    public String getInsuYearFlag() {
        return InsuYearFlag;
    }
    public void setInsuYearFlag(String aInsuYearFlag) {
        InsuYearFlag = aInsuYearFlag;
    }
    public int getCvaliintv() {
        return Cvaliintv;
    }
    public void setCvaliintv(int aCvaliintv) {
        Cvaliintv = aCvaliintv;
    }
    public void setCvaliintv(String aCvaliintv) {
        if (aCvaliintv != null && !aCvaliintv.equals("")) {
            Integer tInteger = new Integer(aCvaliintv);
            int i = tInteger.intValue();
            Cvaliintv = i;
        }
    }

    public String getCvaliintvFlag() {
        return CvaliintvFlag;
    }
    public void setCvaliintvFlag(String aCvaliintvFlag) {
        CvaliintvFlag = aCvaliintvFlag;
    }
    public String getSisinSuredNo() {
        return SisinSuredNo;
    }
    public void setSisinSuredNo(String aSisinSuredNo) {
        SisinSuredNo = aSisinSuredNo;
    }
    public String getTraveLcountry() {
        return TraveLcountry;
    }
    public void setTraveLcountry(String aTraveLcountry) {
        TraveLcountry = aTraveLcountry;
    }
    public String getAgentIp() {
        return AgentIp;
    }
    public void setAgentIp(String aAgentIp) {
        AgentIp = aAgentIp;
    }
    public String getFlightNo() {
        return FlightNo;
    }
    public void setFlightNo(String aFlightNo) {
        FlightNo = aFlightNo;
    }
    public String getRescuecardNo() {
        return RescuecardNo;
    }
    public void setRescuecardNo(String aRescuecardNo) {
        RescuecardNo = aRescuecardNo;
    }
    public String getHandlerName() {
        return HandlerName;
    }
    public void setHandlerName(String aHandlerName) {
        HandlerName = aHandlerName;
    }
    public String getHandlerPhone() {
        return HandlerPhone;
    }
    public void setHandlerPhone(String aHandlerPhone) {
        HandlerPhone = aHandlerPhone;
    }
    public String getSignDate() {
        if(SignDate != null) {
            return fDate.getString(SignDate);
        } else {
            return null;
        }
    }
    public void setSignDate(Date aSignDate) {
        SignDate = aSignDate;
    }
    public void setSignDate(String aSignDate) {
        if (aSignDate != null && !aSignDate.equals("")) {
            SignDate = fDate.getDate(aSignDate);
        } else
            SignDate = null;
    }

    public String getSignTime() {
        return SignTime;
    }
    public void setSignTime(String aSignTime) {
        SignTime = aSignTime;
    }
    public String getAgentcom() {
        return agentcom;
    }
    public void setAgentcom(String aagentcom) {
        agentcom = aagentcom;
    }
    public String getRiskType() {
        return RiskType;
    }
    public void setRiskType(String aRiskType) {
        RiskType = aRiskType;
    }
    public String getOrderType() {
        return OrderType;
    }
    public void setOrderType(String aOrderType) {
        OrderType = aOrderType;
    }
    public String getJYContNo() {
        return JYContNo;
    }
    public void setJYContNo(String aJYContNo) {
        JYContNo = aJYContNo;
    }
    public String getJYCertNo() {
        return JYCertNo;
    }
    public void setJYCertNo(String aJYCertNo) {
        JYCertNo = aJYCertNo;
    }
    public String getJYStartDate() {
        return JYStartDate;
    }
    public void setJYStartDate(String aJYStartDate) {
        JYStartDate = aJYStartDate;
    }
    public String getJYEndDate() {
        return JYEndDate;
    }
    public void setJYEndDate(String aJYEndDate) {
        JYEndDate = aJYEndDate;
    }
    public String getLoanAmount() {
        return LoanAmount;
    }
    public void setLoanAmount(String aLoanAmount) {
        LoanAmount = aLoanAmount;
    }
    public String getLendCom() {
        return LendCom;
    }
    public void setLendCom(String aLendCom) {
        LendCom = aLendCom;
    }
    public String getLoanerNature() {
        return LoanerNature;
    }
    public void setLoanerNature(String aLoanerNature) {
        LoanerNature = aLoanerNature;
    }
    public String getLoanerNatureName() {
        return LoanerNatureName;
    }
    public void setLoanerNatureName(String aLoanerNatureName) {
        LoanerNatureName = aLoanerNatureName;
    }
    public String getLendTerm() {
        return LendTerm;
    }
    public void setLendTerm(String aLendTerm) {
        LendTerm = aLendTerm;
    }
    public String getCountryCode() {
        return CountryCode;
    }
    public void setCountryCode(String aCountryCode) {
        CountryCode = aCountryCode;
    }
    public String getGasCompany() {
        return GasCompany;
    }
    public void setGasCompany(String aGasCompany) {
        GasCompany = aGasCompany;
    }
    public String getGasUserAddress() {
        return GasUserAddress;
    }
    public void setGasUserAddress(String aGasUserAddress) {
        GasUserAddress = aGasUserAddress;
    }
    public String getPayoutPro() {
        return PayoutPro;
    }
    public void setPayoutPro(String aPayoutPro) {
        PayoutPro = aPayoutPro;
    }
    public String getSchool() {
        return School;
    }
    public void setSchool(String aSchool) {
        School = aSchool;
    }
    public String getChargeDate() {
        return ChargeDate;
    }
    public void setChargeDate(String aChargeDate) {
        ChargeDate = aChargeDate;
    }
    public String getMark() {
        return Mark;
    }
    public void setMark(String aMark) {
        Mark = aMark;
    }

    /**
     * 使用另外一个 LCPolicyInfoSchema 对象给 Schema 赋值
     * @param: aLCPolicyInfoSchema LCPolicyInfoSchema
     **/
    public void setSchema(LCPolicyInfoSchema aLCPolicyInfoSchema) {
        this.ContNo = aLCPolicyInfoSchema.getContNo();
        this.InsuredNo = aLCPolicyInfoSchema.getInsuredNo();
        this.ContplanCode = aLCPolicyInfoSchema.getContplanCode();
        this.ContplanName = aLCPolicyInfoSchema.getContplanName();
        this.BasePrem = aLCPolicyInfoSchema.getBasePrem();
        this.AddPrem = aLCPolicyInfoSchema.getAddPrem();
        this.ActiveDate = fDate.getDate( aLCPolicyInfoSchema.getActiveDate());
        this.InsuYear = aLCPolicyInfoSchema.getInsuYear();
        this.InsuYearFlag = aLCPolicyInfoSchema.getInsuYearFlag();
        this.Cvaliintv = aLCPolicyInfoSchema.getCvaliintv();
        this.CvaliintvFlag = aLCPolicyInfoSchema.getCvaliintvFlag();
        this.SisinSuredNo = aLCPolicyInfoSchema.getSisinSuredNo();
        this.TraveLcountry = aLCPolicyInfoSchema.getTraveLcountry();
        this.AgentIp = aLCPolicyInfoSchema.getAgentIp();
        this.FlightNo = aLCPolicyInfoSchema.getFlightNo();
        this.RescuecardNo = aLCPolicyInfoSchema.getRescuecardNo();
        this.HandlerName = aLCPolicyInfoSchema.getHandlerName();
        this.HandlerPhone = aLCPolicyInfoSchema.getHandlerPhone();
        this.SignDate = fDate.getDate( aLCPolicyInfoSchema.getSignDate());
        this.SignTime = aLCPolicyInfoSchema.getSignTime();
        this.agentcom = aLCPolicyInfoSchema.getAgentcom();
        this.RiskType = aLCPolicyInfoSchema.getRiskType();
        this.OrderType = aLCPolicyInfoSchema.getOrderType();
        this.JYContNo = aLCPolicyInfoSchema.getJYContNo();
        this.JYCertNo = aLCPolicyInfoSchema.getJYCertNo();
        this.JYStartDate = aLCPolicyInfoSchema.getJYStartDate();
        this.JYEndDate = aLCPolicyInfoSchema.getJYEndDate();
        this.LoanAmount = aLCPolicyInfoSchema.getLoanAmount();
        this.LendCom = aLCPolicyInfoSchema.getLendCom();
        this.LoanerNature = aLCPolicyInfoSchema.getLoanerNature();
        this.LoanerNatureName = aLCPolicyInfoSchema.getLoanerNatureName();
        this.LendTerm = aLCPolicyInfoSchema.getLendTerm();
        this.CountryCode = aLCPolicyInfoSchema.getCountryCode();
        this.GasCompany = aLCPolicyInfoSchema.getGasCompany();
        this.GasUserAddress = aLCPolicyInfoSchema.getGasUserAddress();
        this.PayoutPro = aLCPolicyInfoSchema.getPayoutPro();
        this.School = aLCPolicyInfoSchema.getSchool();
        this.ChargeDate = aLCPolicyInfoSchema.getChargeDate();
        this.Mark = aLCPolicyInfoSchema.getMark();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: rs ResultSet
     * @param: i int
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs,int i) {
        try {
            //rs.absolute(i);        // 非滚动游标
            if( rs.getString("ContNo") == null )
                this.ContNo = null;
            else
                this.ContNo = rs.getString("ContNo").trim();

            if( rs.getString("InsuredNo") == null )
                this.InsuredNo = null;
            else
                this.InsuredNo = rs.getString("InsuredNo").trim();

            if( rs.getString("ContplanCode") == null )
                this.ContplanCode = null;
            else
                this.ContplanCode = rs.getString("ContplanCode").trim();

            if( rs.getString("ContplanName") == null )
                this.ContplanName = null;
            else
                this.ContplanName = rs.getString("ContplanName").trim();

            this.BasePrem = rs.getDouble("BasePrem");
            this.AddPrem = rs.getDouble("AddPrem");
            this.ActiveDate = rs.getDate("ActiveDate");
            this.InsuYear = rs.getInt("InsuYear");
            if( rs.getString("InsuYearFlag") == null )
                this.InsuYearFlag = null;
            else
                this.InsuYearFlag = rs.getString("InsuYearFlag").trim();

            this.Cvaliintv = rs.getInt("Cvaliintv");
            if( rs.getString("CvaliintvFlag") == null )
                this.CvaliintvFlag = null;
            else
                this.CvaliintvFlag = rs.getString("CvaliintvFlag").trim();

            if( rs.getString("SisinSuredNo") == null )
                this.SisinSuredNo = null;
            else
                this.SisinSuredNo = rs.getString("SisinSuredNo").trim();

            if( rs.getString("TraveLcountry") == null )
                this.TraveLcountry = null;
            else
                this.TraveLcountry = rs.getString("TraveLcountry").trim();

            if( rs.getString("AgentIp") == null )
                this.AgentIp = null;
            else
                this.AgentIp = rs.getString("AgentIp").trim();

            if( rs.getString("FlightNo") == null )
                this.FlightNo = null;
            else
                this.FlightNo = rs.getString("FlightNo").trim();

            if( rs.getString("RescuecardNo") == null )
                this.RescuecardNo = null;
            else
                this.RescuecardNo = rs.getString("RescuecardNo").trim();

            if( rs.getString("HandlerName") == null )
                this.HandlerName = null;
            else
                this.HandlerName = rs.getString("HandlerName").trim();

            if( rs.getString("HandlerPhone") == null )
                this.HandlerPhone = null;
            else
                this.HandlerPhone = rs.getString("HandlerPhone").trim();

            this.SignDate = rs.getDate("SignDate");
            if( rs.getString("SignTime") == null )
                this.SignTime = null;
            else
                this.SignTime = rs.getString("SignTime").trim();

            if( rs.getString("agentcom") == null )
                this.agentcom = null;
            else
                this.agentcom = rs.getString("agentcom").trim();

            if( rs.getString("RiskType") == null )
                this.RiskType = null;
            else
                this.RiskType = rs.getString("RiskType").trim();

            if( rs.getString("OrderType") == null )
                this.OrderType = null;
            else
                this.OrderType = rs.getString("OrderType").trim();

            if( rs.getString("JYContNo") == null )
                this.JYContNo = null;
            else
                this.JYContNo = rs.getString("JYContNo").trim();

            if( rs.getString("JYCertNo") == null )
                this.JYCertNo = null;
            else
                this.JYCertNo = rs.getString("JYCertNo").trim();

            if( rs.getString("JYStartDate") == null )
                this.JYStartDate = null;
            else
                this.JYStartDate = rs.getString("JYStartDate").trim();

            if( rs.getString("JYEndDate") == null )
                this.JYEndDate = null;
            else
                this.JYEndDate = rs.getString("JYEndDate").trim();

            if( rs.getString("LoanAmount") == null )
                this.LoanAmount = null;
            else
                this.LoanAmount = rs.getString("LoanAmount").trim();

            if( rs.getString("LendCom") == null )
                this.LendCom = null;
            else
                this.LendCom = rs.getString("LendCom").trim();

            if( rs.getString("LoanerNature") == null )
                this.LoanerNature = null;
            else
                this.LoanerNature = rs.getString("LoanerNature").trim();

            if( rs.getString("LoanerNatureName") == null )
                this.LoanerNatureName = null;
            else
                this.LoanerNatureName = rs.getString("LoanerNatureName").trim();

            if( rs.getString("LendTerm") == null )
                this.LendTerm = null;
            else
                this.LendTerm = rs.getString("LendTerm").trim();

            if( rs.getString("CountryCode") == null )
                this.CountryCode = null;
            else
                this.CountryCode = rs.getString("CountryCode").trim();

            if( rs.getString("GasCompany") == null )
                this.GasCompany = null;
            else
                this.GasCompany = rs.getString("GasCompany").trim();

            if( rs.getString("GasUserAddress") == null )
                this.GasUserAddress = null;
            else
                this.GasUserAddress = rs.getString("GasUserAddress").trim();

            if( rs.getString("PayoutPro") == null )
                this.PayoutPro = null;
            else
                this.PayoutPro = rs.getString("PayoutPro").trim();

            if( rs.getString("School") == null )
                this.School = null;
            else
                this.School = rs.getString("School").trim();

            if( rs.getString("ChargeDate") == null )
                this.ChargeDate = null;
            else
                this.ChargeDate = rs.getString("ChargeDate").trim();

            if( rs.getString("Mark") == null )
                this.Mark = null;
            else
                this.Mark = rs.getString("Mark").trim();

        }
        catch(SQLException sqle) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LCPolicyInfoSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = ExceptionUtils.exceptionToString(sqle);
            this.mErrors .addOneError(tError);
            return false;
        }
        return true;
    }

    public LCPolicyInfoSchema getSchema() {
        LCPolicyInfoSchema aLCPolicyInfoSchema = new LCPolicyInfoSchema();
        aLCPolicyInfoSchema.setSchema(this);
        return aLCPolicyInfoSchema;
    }

    public LCPolicyInfoDB getDB() {
        LCPolicyInfoDB aDBOper = new LCPolicyInfoDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLCPolicyInfo描述/A>表字段
     * @return: String 返回打包后字符串
     **/
    public String encode() {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(ContNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(InsuredNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ContplanCode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ContplanName)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(BasePrem));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(AddPrem));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( ActiveDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(InsuYear));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(InsuYearFlag)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(Cvaliintv));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(CvaliintvFlag)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(SisinSuredNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(TraveLcountry)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AgentIp)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(FlightNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(RescuecardNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(HandlerName)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(HandlerPhone)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( SignDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(SignTime)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(agentcom)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(RiskType)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(OrderType)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(JYContNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(JYCertNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(JYStartDate)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(JYEndDate)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(LoanAmount)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(LendCom)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(LoanerNature)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(LoanerNatureName)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(LendTerm)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(CountryCode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(GasCompany)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(GasUserAddress)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(PayoutPro)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(School)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ChargeDate)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Mark));
        return strReturn.toString();
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLCPolicyInfo>历史记账凭证主表信息</A>表字段
     * @param: strMessage String 包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage) {
        try {
            ContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
            InsuredNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
            ContplanCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
            ContplanName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
            BasePrem = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,5, SysConst.PACKAGESPILTER))).doubleValue();
            AddPrem = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,6, SysConst.PACKAGESPILTER))).doubleValue();
            ActiveDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER));
            InsuYear = new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,8, SysConst.PACKAGESPILTER))).intValue();
            InsuYearFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
            Cvaliintv = new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,10, SysConst.PACKAGESPILTER))).intValue();
            CvaliintvFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
            SisinSuredNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
            TraveLcountry = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
            AgentIp = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER );
            FlightNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15, SysConst.PACKAGESPILTER );
            RescuecardNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16, SysConst.PACKAGESPILTER );
            HandlerName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17, SysConst.PACKAGESPILTER );
            HandlerPhone = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18, SysConst.PACKAGESPILTER );
            SignDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19, SysConst.PACKAGESPILTER));
            SignTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20, SysConst.PACKAGESPILTER );
            agentcom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 21, SysConst.PACKAGESPILTER );
            RiskType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 22, SysConst.PACKAGESPILTER );
            OrderType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 23, SysConst.PACKAGESPILTER );
            JYContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 24, SysConst.PACKAGESPILTER );
            JYCertNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 25, SysConst.PACKAGESPILTER );
            JYStartDate = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 26, SysConst.PACKAGESPILTER );
            JYEndDate = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 27, SysConst.PACKAGESPILTER );
            LoanAmount = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 28, SysConst.PACKAGESPILTER );
            LendCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 29, SysConst.PACKAGESPILTER );
            LoanerNature = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 30, SysConst.PACKAGESPILTER );
            LoanerNatureName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 31, SysConst.PACKAGESPILTER );
            LendTerm = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 32, SysConst.PACKAGESPILTER );
            CountryCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 33, SysConst.PACKAGESPILTER );
            GasCompany = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 34, SysConst.PACKAGESPILTER );
            GasUserAddress = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 35, SysConst.PACKAGESPILTER );
            PayoutPro = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 36, SysConst.PACKAGESPILTER );
            School = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 37, SysConst.PACKAGESPILTER );
            ChargeDate = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 38, SysConst.PACKAGESPILTER );
            Mark = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 39, SysConst.PACKAGESPILTER );
        }
        catch(NumberFormatException ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LCPolicyInfoSchema";
            tError.functionName = "decode";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode String 希望取得的字段名
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("ContNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ContNo));
        }
        if (FCode.equalsIgnoreCase("InsuredNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InsuredNo));
        }
        if (FCode.equalsIgnoreCase("ContplanCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ContplanCode));
        }
        if (FCode.equalsIgnoreCase("ContplanName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ContplanName));
        }
        if (FCode.equalsIgnoreCase("BasePrem")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BasePrem));
        }
        if (FCode.equalsIgnoreCase("AddPrem")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AddPrem));
        }
        if (FCode.equalsIgnoreCase("ActiveDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getActiveDate()));
        }
        if (FCode.equalsIgnoreCase("InsuYear")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InsuYear));
        }
        if (FCode.equalsIgnoreCase("InsuYearFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InsuYearFlag));
        }
        if (FCode.equalsIgnoreCase("Cvaliintv")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Cvaliintv));
        }
        if (FCode.equalsIgnoreCase("CvaliintvFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CvaliintvFlag));
        }
        if (FCode.equalsIgnoreCase("SisinSuredNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SisinSuredNo));
        }
        if (FCode.equalsIgnoreCase("TraveLcountry")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(TraveLcountry));
        }
        if (FCode.equalsIgnoreCase("AgentIp")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentIp));
        }
        if (FCode.equalsIgnoreCase("FlightNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FlightNo));
        }
        if (FCode.equalsIgnoreCase("RescuecardNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RescuecardNo));
        }
        if (FCode.equalsIgnoreCase("HandlerName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(HandlerName));
        }
        if (FCode.equalsIgnoreCase("HandlerPhone")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(HandlerPhone));
        }
        if (FCode.equalsIgnoreCase("SignDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getSignDate()));
        }
        if (FCode.equalsIgnoreCase("SignTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SignTime));
        }
        if (FCode.equalsIgnoreCase("agentcom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(agentcom));
        }
        if (FCode.equalsIgnoreCase("RiskType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RiskType));
        }
        if (FCode.equalsIgnoreCase("OrderType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OrderType));
        }
        if (FCode.equalsIgnoreCase("JYContNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(JYContNo));
        }
        if (FCode.equalsIgnoreCase("JYCertNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(JYCertNo));
        }
        if (FCode.equalsIgnoreCase("JYStartDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(JYStartDate));
        }
        if (FCode.equalsIgnoreCase("JYEndDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(JYEndDate));
        }
        if (FCode.equalsIgnoreCase("LoanAmount")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(LoanAmount));
        }
        if (FCode.equalsIgnoreCase("LendCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(LendCom));
        }
        if (FCode.equalsIgnoreCase("LoanerNature")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(LoanerNature));
        }
        if (FCode.equalsIgnoreCase("LoanerNatureName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(LoanerNatureName));
        }
        if (FCode.equalsIgnoreCase("LendTerm")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(LendTerm));
        }
        if (FCode.equalsIgnoreCase("CountryCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CountryCode));
        }
        if (FCode.equalsIgnoreCase("GasCompany")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GasCompany));
        }
        if (FCode.equalsIgnoreCase("GasUserAddress")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GasUserAddress));
        }
        if (FCode.equalsIgnoreCase("PayoutPro")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PayoutPro));
        }
        if (FCode.equalsIgnoreCase("School")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(School));
        }
        if (FCode.equalsIgnoreCase("ChargeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ChargeDate));
        }
        if (FCode.equalsIgnoreCase("Mark")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Mark));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex int 指定的字段索引值
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(ContNo);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(InsuredNo);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(ContplanCode);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(ContplanName);
                break;
            case 4:
                strFieldValue = String.valueOf(BasePrem);
                break;
            case 5:
                strFieldValue = String.valueOf(AddPrem);
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getActiveDate()));
                break;
            case 7:
                strFieldValue = String.valueOf(InsuYear);
                break;
            case 8:
                strFieldValue = StrTool.GBKToUnicode(InsuYearFlag);
                break;
            case 9:
                strFieldValue = String.valueOf(Cvaliintv);
                break;
            case 10:
                strFieldValue = StrTool.GBKToUnicode(CvaliintvFlag);
                break;
            case 11:
                strFieldValue = StrTool.GBKToUnicode(SisinSuredNo);
                break;
            case 12:
                strFieldValue = StrTool.GBKToUnicode(TraveLcountry);
                break;
            case 13:
                strFieldValue = StrTool.GBKToUnicode(AgentIp);
                break;
            case 14:
                strFieldValue = StrTool.GBKToUnicode(FlightNo);
                break;
            case 15:
                strFieldValue = StrTool.GBKToUnicode(RescuecardNo);
                break;
            case 16:
                strFieldValue = StrTool.GBKToUnicode(HandlerName);
                break;
            case 17:
                strFieldValue = StrTool.GBKToUnicode(HandlerPhone);
                break;
            case 18:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getSignDate()));
                break;
            case 19:
                strFieldValue = StrTool.GBKToUnicode(SignTime);
                break;
            case 20:
                strFieldValue = StrTool.GBKToUnicode(agentcom);
                break;
            case 21:
                strFieldValue = StrTool.GBKToUnicode(RiskType);
                break;
            case 22:
                strFieldValue = StrTool.GBKToUnicode(OrderType);
                break;
            case 23:
                strFieldValue = StrTool.GBKToUnicode(JYContNo);
                break;
            case 24:
                strFieldValue = StrTool.GBKToUnicode(JYCertNo);
                break;
            case 25:
                strFieldValue = StrTool.GBKToUnicode(JYStartDate);
                break;
            case 26:
                strFieldValue = StrTool.GBKToUnicode(JYEndDate);
                break;
            case 27:
                strFieldValue = StrTool.GBKToUnicode(LoanAmount);
                break;
            case 28:
                strFieldValue = StrTool.GBKToUnicode(LendCom);
                break;
            case 29:
                strFieldValue = StrTool.GBKToUnicode(LoanerNature);
                break;
            case 30:
                strFieldValue = StrTool.GBKToUnicode(LoanerNatureName);
                break;
            case 31:
                strFieldValue = StrTool.GBKToUnicode(LendTerm);
                break;
            case 32:
                strFieldValue = StrTool.GBKToUnicode(CountryCode);
                break;
            case 33:
                strFieldValue = StrTool.GBKToUnicode(GasCompany);
                break;
            case 34:
                strFieldValue = StrTool.GBKToUnicode(GasUserAddress);
                break;
            case 35:
                strFieldValue = StrTool.GBKToUnicode(PayoutPro);
                break;
            case 36:
                strFieldValue = StrTool.GBKToUnicode(School);
                break;
            case 37:
                strFieldValue = StrTool.GBKToUnicode(ChargeDate);
                break;
            case 38:
                strFieldValue = StrTool.GBKToUnicode(Mark);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode String 需要赋值的对象
     * @param: FValue String 要赋的值
     * @return: boolean
     **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("ContNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                ContNo = FValue.trim();
            }
            else
                ContNo = null;
        }
        if (FCode.equalsIgnoreCase("InsuredNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                InsuredNo = FValue.trim();
            }
            else
                InsuredNo = null;
        }
        if (FCode.equalsIgnoreCase("ContplanCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                ContplanCode = FValue.trim();
            }
            else
                ContplanCode = null;
        }
        if (FCode.equalsIgnoreCase("ContplanName")) {
            if( FValue != null && !FValue.equals(""))
            {
                ContplanName = FValue.trim();
            }
            else
                ContplanName = null;
        }
        if (FCode.equalsIgnoreCase("BasePrem")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                BasePrem = d;
            }
        }
        if (FCode.equalsIgnoreCase("AddPrem")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                AddPrem = d;
            }
        }
        if (FCode.equalsIgnoreCase("ActiveDate")) {
            if(FValue != null && !FValue.equals("")) {
                ActiveDate = fDate.getDate( FValue );
            }
            else
                ActiveDate = null;
        }
        if (FCode.equalsIgnoreCase("InsuYear")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                InsuYear = i;
            }
        }
        if (FCode.equalsIgnoreCase("InsuYearFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                InsuYearFlag = FValue.trim();
            }
            else
                InsuYearFlag = null;
        }
        if (FCode.equalsIgnoreCase("Cvaliintv")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                Cvaliintv = i;
            }
        }
        if (FCode.equalsIgnoreCase("CvaliintvFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                CvaliintvFlag = FValue.trim();
            }
            else
                CvaliintvFlag = null;
        }
        if (FCode.equalsIgnoreCase("SisinSuredNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                SisinSuredNo = FValue.trim();
            }
            else
                SisinSuredNo = null;
        }
        if (FCode.equalsIgnoreCase("TraveLcountry")) {
            if( FValue != null && !FValue.equals(""))
            {
                TraveLcountry = FValue.trim();
            }
            else
                TraveLcountry = null;
        }
        if (FCode.equalsIgnoreCase("AgentIp")) {
            if( FValue != null && !FValue.equals(""))
            {
                AgentIp = FValue.trim();
            }
            else
                AgentIp = null;
        }
        if (FCode.equalsIgnoreCase("FlightNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                FlightNo = FValue.trim();
            }
            else
                FlightNo = null;
        }
        if (FCode.equalsIgnoreCase("RescuecardNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                RescuecardNo = FValue.trim();
            }
            else
                RescuecardNo = null;
        }
        if (FCode.equalsIgnoreCase("HandlerName")) {
            if( FValue != null && !FValue.equals(""))
            {
                HandlerName = FValue.trim();
            }
            else
                HandlerName = null;
        }
        if (FCode.equalsIgnoreCase("HandlerPhone")) {
            if( FValue != null && !FValue.equals(""))
            {
                HandlerPhone = FValue.trim();
            }
            else
                HandlerPhone = null;
        }
        if (FCode.equalsIgnoreCase("SignDate")) {
            if(FValue != null && !FValue.equals("")) {
                SignDate = fDate.getDate( FValue );
            }
            else
                SignDate = null;
        }
        if (FCode.equalsIgnoreCase("SignTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                SignTime = FValue.trim();
            }
            else
                SignTime = null;
        }
        if (FCode.equalsIgnoreCase("agentcom")) {
            if( FValue != null && !FValue.equals(""))
            {
                agentcom = FValue.trim();
            }
            else
                agentcom = null;
        }
        if (FCode.equalsIgnoreCase("RiskType")) {
            if( FValue != null && !FValue.equals(""))
            {
                RiskType = FValue.trim();
            }
            else
                RiskType = null;
        }
        if (FCode.equalsIgnoreCase("OrderType")) {
            if( FValue != null && !FValue.equals(""))
            {
                OrderType = FValue.trim();
            }
            else
                OrderType = null;
        }
        if (FCode.equalsIgnoreCase("JYContNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                JYContNo = FValue.trim();
            }
            else
                JYContNo = null;
        }
        if (FCode.equalsIgnoreCase("JYCertNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                JYCertNo = FValue.trim();
            }
            else
                JYCertNo = null;
        }
        if (FCode.equalsIgnoreCase("JYStartDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                JYStartDate = FValue.trim();
            }
            else
                JYStartDate = null;
        }
        if (FCode.equalsIgnoreCase("JYEndDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                JYEndDate = FValue.trim();
            }
            else
                JYEndDate = null;
        }
        if (FCode.equalsIgnoreCase("LoanAmount")) {
            if( FValue != null && !FValue.equals(""))
            {
                LoanAmount = FValue.trim();
            }
            else
                LoanAmount = null;
        }
        if (FCode.equalsIgnoreCase("LendCom")) {
            if( FValue != null && !FValue.equals(""))
            {
                LendCom = FValue.trim();
            }
            else
                LendCom = null;
        }
        if (FCode.equalsIgnoreCase("LoanerNature")) {
            if( FValue != null && !FValue.equals(""))
            {
                LoanerNature = FValue.trim();
            }
            else
                LoanerNature = null;
        }
        if (FCode.equalsIgnoreCase("LoanerNatureName")) {
            if( FValue != null && !FValue.equals(""))
            {
                LoanerNatureName = FValue.trim();
            }
            else
                LoanerNatureName = null;
        }
        if (FCode.equalsIgnoreCase("LendTerm")) {
            if( FValue != null && !FValue.equals(""))
            {
                LendTerm = FValue.trim();
            }
            else
                LendTerm = null;
        }
        if (FCode.equalsIgnoreCase("CountryCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                CountryCode = FValue.trim();
            }
            else
                CountryCode = null;
        }
        if (FCode.equalsIgnoreCase("GasCompany")) {
            if( FValue != null && !FValue.equals(""))
            {
                GasCompany = FValue.trim();
            }
            else
                GasCompany = null;
        }
        if (FCode.equalsIgnoreCase("GasUserAddress")) {
            if( FValue != null && !FValue.equals(""))
            {
                GasUserAddress = FValue.trim();
            }
            else
                GasUserAddress = null;
        }
        if (FCode.equalsIgnoreCase("PayoutPro")) {
            if( FValue != null && !FValue.equals(""))
            {
                PayoutPro = FValue.trim();
            }
            else
                PayoutPro = null;
        }
        if (FCode.equalsIgnoreCase("School")) {
            if( FValue != null && !FValue.equals(""))
            {
                School = FValue.trim();
            }
            else
                School = null;
        }
        if (FCode.equalsIgnoreCase("ChargeDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                ChargeDate = FValue.trim();
            }
            else
                ChargeDate = null;
        }
        if (FCode.equalsIgnoreCase("Mark")) {
            if( FValue != null && !FValue.equals(""))
            {
                Mark = FValue.trim();
            }
            else
                Mark = null;
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject) return true;
        if (otherObject == null) return false;
        if (getClass() != otherObject.getClass()) return false;
        LCPolicyInfoSchema other = (LCPolicyInfoSchema)otherObject;
        return
                ContNo.equals(other.getContNo())
                        && InsuredNo.equals(other.getInsuredNo())
                        && ContplanCode.equals(other.getContplanCode())
                        && ContplanName.equals(other.getContplanName())
                        && BasePrem == other.getBasePrem()
                        && AddPrem == other.getAddPrem()
                        && fDate.getString(ActiveDate).equals(other.getActiveDate())
                        && InsuYear == other.getInsuYear()
                        && InsuYearFlag.equals(other.getInsuYearFlag())
                        && Cvaliintv == other.getCvaliintv()
                        && CvaliintvFlag.equals(other.getCvaliintvFlag())
                        && SisinSuredNo.equals(other.getSisinSuredNo())
                        && TraveLcountry.equals(other.getTraveLcountry())
                        && AgentIp.equals(other.getAgentIp())
                        && FlightNo.equals(other.getFlightNo())
                        && RescuecardNo.equals(other.getRescuecardNo())
                        && HandlerName.equals(other.getHandlerName())
                        && HandlerPhone.equals(other.getHandlerPhone())
                        && fDate.getString(SignDate).equals(other.getSignDate())
                        && SignTime.equals(other.getSignTime())
                        && agentcom.equals(other.getAgentcom())
                        && RiskType.equals(other.getRiskType())
                        && OrderType.equals(other.getOrderType())
                        && JYContNo.equals(other.getJYContNo())
                        && JYCertNo.equals(other.getJYCertNo())
                        && JYStartDate.equals(other.getJYStartDate())
                        && JYEndDate.equals(other.getJYEndDate())
                        && LoanAmount.equals(other.getLoanAmount())
                        && LendCom.equals(other.getLendCom())
                        && LoanerNature.equals(other.getLoanerNature())
                        && LoanerNatureName.equals(other.getLoanerNatureName())
                        && LendTerm.equals(other.getLendTerm())
                        && CountryCode.equals(other.getCountryCode())
                        && GasCompany.equals(other.getGasCompany())
                        && GasUserAddress.equals(other.getGasUserAddress())
                        && PayoutPro.equals(other.getPayoutPro())
                        && School.equals(other.getSchool())
                        && ChargeDate.equals(other.getChargeDate())
                        && Mark.equals(other.getMark());
    }

    /**
     * 取得Schema拥有字段的数量
     * @return: int
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("ContNo") ) {
            return 0;
        }
        if( strFieldName.equals("InsuredNo") ) {
            return 1;
        }
        if( strFieldName.equals("ContplanCode") ) {
            return 2;
        }
        if( strFieldName.equals("ContplanName") ) {
            return 3;
        }
        if( strFieldName.equals("BasePrem") ) {
            return 4;
        }
        if( strFieldName.equals("AddPrem") ) {
            return 5;
        }
        if( strFieldName.equals("ActiveDate") ) {
            return 6;
        }
        if( strFieldName.equals("InsuYear") ) {
            return 7;
        }
        if( strFieldName.equals("InsuYearFlag") ) {
            return 8;
        }
        if( strFieldName.equals("Cvaliintv") ) {
            return 9;
        }
        if( strFieldName.equals("CvaliintvFlag") ) {
            return 10;
        }
        if( strFieldName.equals("SisinSuredNo") ) {
            return 11;
        }
        if( strFieldName.equals("TraveLcountry") ) {
            return 12;
        }
        if( strFieldName.equals("AgentIp") ) {
            return 13;
        }
        if( strFieldName.equals("FlightNo") ) {
            return 14;
        }
        if( strFieldName.equals("RescuecardNo") ) {
            return 15;
        }
        if( strFieldName.equals("HandlerName") ) {
            return 16;
        }
        if( strFieldName.equals("HandlerPhone") ) {
            return 17;
        }
        if( strFieldName.equals("SignDate") ) {
            return 18;
        }
        if( strFieldName.equals("SignTime") ) {
            return 19;
        }
        if( strFieldName.equals("agentcom") ) {
            return 20;
        }
        if( strFieldName.equals("RiskType") ) {
            return 21;
        }
        if( strFieldName.equals("OrderType") ) {
            return 22;
        }
        if( strFieldName.equals("JYContNo") ) {
            return 23;
        }
        if( strFieldName.equals("JYCertNo") ) {
            return 24;
        }
        if( strFieldName.equals("JYStartDate") ) {
            return 25;
        }
        if( strFieldName.equals("JYEndDate") ) {
            return 26;
        }
        if( strFieldName.equals("LoanAmount") ) {
            return 27;
        }
        if( strFieldName.equals("LendCom") ) {
            return 28;
        }
        if( strFieldName.equals("LoanerNature") ) {
            return 29;
        }
        if( strFieldName.equals("LoanerNatureName") ) {
            return 30;
        }
        if( strFieldName.equals("LendTerm") ) {
            return 31;
        }
        if( strFieldName.equals("CountryCode") ) {
            return 32;
        }
        if( strFieldName.equals("GasCompany") ) {
            return 33;
        }
        if( strFieldName.equals("GasUserAddress") ) {
            return 34;
        }
        if( strFieldName.equals("PayoutPro") ) {
            return 35;
        }
        if( strFieldName.equals("School") ) {
            return 36;
        }
        if( strFieldName.equals("ChargeDate") ) {
            return 37;
        }
        if( strFieldName.equals("Mark") ) {
            return 38;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     * @param: nFieldIndex int
     * @return: String
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "ContNo";
                break;
            case 1:
                strFieldName = "InsuredNo";
                break;
            case 2:
                strFieldName = "ContplanCode";
                break;
            case 3:
                strFieldName = "ContplanName";
                break;
            case 4:
                strFieldName = "BasePrem";
                break;
            case 5:
                strFieldName = "AddPrem";
                break;
            case 6:
                strFieldName = "ActiveDate";
                break;
            case 7:
                strFieldName = "InsuYear";
                break;
            case 8:
                strFieldName = "InsuYearFlag";
                break;
            case 9:
                strFieldName = "Cvaliintv";
                break;
            case 10:
                strFieldName = "CvaliintvFlag";
                break;
            case 11:
                strFieldName = "SisinSuredNo";
                break;
            case 12:
                strFieldName = "TraveLcountry";
                break;
            case 13:
                strFieldName = "AgentIp";
                break;
            case 14:
                strFieldName = "FlightNo";
                break;
            case 15:
                strFieldName = "RescuecardNo";
                break;
            case 16:
                strFieldName = "HandlerName";
                break;
            case 17:
                strFieldName = "HandlerPhone";
                break;
            case 18:
                strFieldName = "SignDate";
                break;
            case 19:
                strFieldName = "SignTime";
                break;
            case 20:
                strFieldName = "agentcom";
                break;
            case 21:
                strFieldName = "RiskType";
                break;
            case 22:
                strFieldName = "OrderType";
                break;
            case 23:
                strFieldName = "JYContNo";
                break;
            case 24:
                strFieldName = "JYCertNo";
                break;
            case 25:
                strFieldName = "JYStartDate";
                break;
            case 26:
                strFieldName = "JYEndDate";
                break;
            case 27:
                strFieldName = "LoanAmount";
                break;
            case 28:
                strFieldName = "LendCom";
                break;
            case 29:
                strFieldName = "LoanerNature";
                break;
            case 30:
                strFieldName = "LoanerNatureName";
                break;
            case 31:
                strFieldName = "LendTerm";
                break;
            case 32:
                strFieldName = "CountryCode";
                break;
            case 33:
                strFieldName = "GasCompany";
                break;
            case 34:
                strFieldName = "GasUserAddress";
                break;
            case 35:
                strFieldName = "PayoutPro";
                break;
            case 36:
                strFieldName = "School";
                break;
            case 37:
                strFieldName = "ChargeDate";
                break;
            case 38:
                strFieldName = "Mark";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "CONTNO":
                return Schema.TYPE_STRING;
            case "INSUREDNO":
                return Schema.TYPE_STRING;
            case "CONTPLANCODE":
                return Schema.TYPE_STRING;
            case "CONTPLANNAME":
                return Schema.TYPE_STRING;
            case "BASEPREM":
                return Schema.TYPE_DOUBLE;
            case "ADDPREM":
                return Schema.TYPE_DOUBLE;
            case "ACTIVEDATE":
                return Schema.TYPE_DATE;
            case "INSUYEAR":
                return Schema.TYPE_INT;
            case "INSUYEARFLAG":
                return Schema.TYPE_STRING;
            case "CVALIINTV":
                return Schema.TYPE_INT;
            case "CVALIINTVFLAG":
                return Schema.TYPE_STRING;
            case "SISINSUREDNO":
                return Schema.TYPE_STRING;
            case "TRAVELCOUNTRY":
                return Schema.TYPE_STRING;
            case "AGENTIP":
                return Schema.TYPE_STRING;
            case "FLIGHTNO":
                return Schema.TYPE_STRING;
            case "RESCUECARDNO":
                return Schema.TYPE_STRING;
            case "HANDLERNAME":
                return Schema.TYPE_STRING;
            case "HANDLERPHONE":
                return Schema.TYPE_STRING;
            case "SIGNDATE":
                return Schema.TYPE_DATE;
            case "SIGNTIME":
                return Schema.TYPE_STRING;
            case "AGENTCOM":
                return Schema.TYPE_STRING;
            case "RISKTYPE":
                return Schema.TYPE_STRING;
            case "ORDERTYPE":
                return Schema.TYPE_STRING;
            case "JYCONTNO":
                return Schema.TYPE_STRING;
            case "JYCERTNO":
                return Schema.TYPE_STRING;
            case "JYSTARTDATE":
                return Schema.TYPE_STRING;
            case "JYENDDATE":
                return Schema.TYPE_STRING;
            case "LOANAMOUNT":
                return Schema.TYPE_STRING;
            case "LENDCOM":
                return Schema.TYPE_STRING;
            case "LOANERNATURE":
                return Schema.TYPE_STRING;
            case "LOANERNATURENAME":
                return Schema.TYPE_STRING;
            case "LENDTERM":
                return Schema.TYPE_STRING;
            case "COUNTRYCODE":
                return Schema.TYPE_STRING;
            case "GASCOMPANY":
                return Schema.TYPE_STRING;
            case "GASUSERADDRESS":
                return Schema.TYPE_STRING;
            case "PAYOUTPRO":
                return Schema.TYPE_STRING;
            case "SCHOOL":
                return Schema.TYPE_STRING;
            case "CHARGEDATE":
                return Schema.TYPE_STRING;
            case "MARK":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: nFieldIndex int
     * @return: int
     **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_STRING;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_DOUBLE;
            case 5:
                return Schema.TYPE_DOUBLE;
            case 6:
                return Schema.TYPE_DATE;
            case 7:
                return Schema.TYPE_INT;
            case 8:
                return Schema.TYPE_STRING;
            case 9:
                return Schema.TYPE_INT;
            case 10:
                return Schema.TYPE_STRING;
            case 11:
                return Schema.TYPE_STRING;
            case 12:
                return Schema.TYPE_STRING;
            case 13:
                return Schema.TYPE_STRING;
            case 14:
                return Schema.TYPE_STRING;
            case 15:
                return Schema.TYPE_STRING;
            case 16:
                return Schema.TYPE_STRING;
            case 17:
                return Schema.TYPE_STRING;
            case 18:
                return Schema.TYPE_DATE;
            case 19:
                return Schema.TYPE_STRING;
            case 20:
                return Schema.TYPE_STRING;
            case 21:
                return Schema.TYPE_STRING;
            case 22:
                return Schema.TYPE_STRING;
            case 23:
                return Schema.TYPE_STRING;
            case 24:
                return Schema.TYPE_STRING;
            case 25:
                return Schema.TYPE_STRING;
            case 26:
                return Schema.TYPE_STRING;
            case 27:
                return Schema.TYPE_STRING;
            case 28:
                return Schema.TYPE_STRING;
            case 29:
                return Schema.TYPE_STRING;
            case 30:
                return Schema.TYPE_STRING;
            case 31:
                return Schema.TYPE_STRING;
            case 32:
                return Schema.TYPE_STRING;
            case 33:
                return Schema.TYPE_STRING;
            case 34:
                return Schema.TYPE_STRING;
            case 35:
                return Schema.TYPE_STRING;
            case 36:
                return Schema.TYPE_STRING;
            case 37:
                return Schema.TYPE_STRING;
            case 38:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
}
