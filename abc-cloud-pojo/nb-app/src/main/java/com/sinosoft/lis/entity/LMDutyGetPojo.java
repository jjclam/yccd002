/**
 * Copyright (c) 2017 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.entity;

import com.sinosoft.cloud.cache.annotation.RedisPrimaryHKey;
import com.sinosoft.utility.Pojo;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;

import java.io.Serializable;

/**
 * <p>ClassName: LMDutyGetPojo </p>
 * <p>Copyright: Copyright (c) 2017</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2017-10-17
 */
public class LMDutyGetPojo implements Pojo,Serializable {
    // @Field
    /** 给付代码 */
    @RedisPrimaryHKey
    private String GetDutyCode; 
    /** 给付名称 */
    private String GetDutyName; 
    /** 给付类型 */
    private String Type; 
    /** 给付间隔 */
    private int GetIntv; 
    /** 默认值 */
    private double DefaultVal; 
    /** 算法 */
    private String CalCode; 
    /** 反算算法 */
    private String CnterCalCode; 
    /** 其他算法 */
    private String OthCalCode; 
    /** 起领期间 */
    private int GetYear; 
    /** 起领期间单位 */
    private String GetYearFlag; 
    /** 起领日期计算参照 */
    private String StartDateCalRef; 
    /** 起领日期计算方式 */
    private String StartDateCalMode; 
    /** 起领期间上限 */
    private int MinGetStartPeriod; 
    /** 止领期间 */
    private int GetEndPeriod; 
    /** 止领期间单位 */
    private String GetEndUnit; 
    /** 止领日期计算参照 */
    private String EndDateCalRef; 
    /** 止领日期计算方式 */
    private String EndDateCalMode; 
    /** 止领期间下限 */
    private int MaxGetEndPeriod; 
    /** 递增标记 */
    private String AddFlag; 
    /** 性别关联标记 */
    private String SexRelaFlag; 
    /** 单位投保关联标记 */
    private String UnitAppRelaFlag; 
    /** 算入保额标记 */
    private String AddAmntFlag; 
    /** 现金领取标记 */
    private String DiscntFlag; 
    /** 利率标记 */
    private String InterestFlag; 
    /** 多被保人分享标记 */
    private String ShareFlag; 
    /** 录入标记 */
    private String InpFlag; 
    /** 受益人标记 */
    private String BnfFlag; 
    /** 催付标记 */
    private String UrgeGetFlag; 
    /** 被保人死亡后有效标记 */
    private String DeadValiFlag; 
    /** 给付初始化标记 */
    private String GetInitFlag; 
    /** 起付限 */
    private double GetLimit; 
    /** 赔付限额 */
    private double MaxGet; 
    /** 赔付比例 */
    private double GetRate; 
    /** 是否和账户相关 */
    private String NeedAcc; 
    /** 默认申请标志 */
    private String CanGet; 
    /** 是否是账户结清后才能申请 */
    private String NeedCancelAcc; 
    /** 给付分类1 */
    private String GetType1; 
    /** 是否允许零值标记 */
    private String ZeroFlag; 
    /** 给付分类2 */
    private String GetType2; 


    public static final int FIELDNUM = 39;    // 数据库表的字段个数
    public String getGetDutyCode() {
        return GetDutyCode;
    }
    public void setGetDutyCode(String aGetDutyCode) {
        GetDutyCode = aGetDutyCode;
    }
    public String getGetDutyName() {
        return GetDutyName;
    }
    public void setGetDutyName(String aGetDutyName) {
        GetDutyName = aGetDutyName;
    }
    public String getType() {
        return Type;
    }
    public void setType(String aType) {
        Type = aType;
    }
    public int getGetIntv() {
        return GetIntv;
    }
    public void setGetIntv(int aGetIntv) {
        GetIntv = aGetIntv;
    }
    public void setGetIntv(String aGetIntv) {
        if (aGetIntv != null && !aGetIntv.equals("")) {
            Integer tInteger = new Integer(aGetIntv);
            int i = tInteger.intValue();
            GetIntv = i;
        }
    }

    public double getDefaultVal() {
        return DefaultVal;
    }
    public void setDefaultVal(double aDefaultVal) {
        DefaultVal = aDefaultVal;
    }
    public void setDefaultVal(String aDefaultVal) {
        if (aDefaultVal != null && !aDefaultVal.equals("")) {
            Double tDouble = new Double(aDefaultVal);
            double d = tDouble.doubleValue();
            DefaultVal = d;
        }
    }

    public String getCalCode() {
        return CalCode;
    }
    public void setCalCode(String aCalCode) {
        CalCode = aCalCode;
    }
    public String getCnterCalCode() {
        return CnterCalCode;
    }
    public void setCnterCalCode(String aCnterCalCode) {
        CnterCalCode = aCnterCalCode;
    }
    public String getOthCalCode() {
        return OthCalCode;
    }
    public void setOthCalCode(String aOthCalCode) {
        OthCalCode = aOthCalCode;
    }
    public int getGetYear() {
        return GetYear;
    }
    public void setGetYear(int aGetYear) {
        GetYear = aGetYear;
    }
    public void setGetYear(String aGetYear) {
        if (aGetYear != null && !aGetYear.equals("")) {
            Integer tInteger = new Integer(aGetYear);
            int i = tInteger.intValue();
            GetYear = i;
        }
    }

    public String getGetYearFlag() {
        return GetYearFlag;
    }
    public void setGetYearFlag(String aGetYearFlag) {
        GetYearFlag = aGetYearFlag;
    }
    public String getStartDateCalRef() {
        return StartDateCalRef;
    }
    public void setStartDateCalRef(String aStartDateCalRef) {
        StartDateCalRef = aStartDateCalRef;
    }
    public String getStartDateCalMode() {
        return StartDateCalMode;
    }
    public void setStartDateCalMode(String aStartDateCalMode) {
        StartDateCalMode = aStartDateCalMode;
    }
    public int getMinGetStartPeriod() {
        return MinGetStartPeriod;
    }
    public void setMinGetStartPeriod(int aMinGetStartPeriod) {
        MinGetStartPeriod = aMinGetStartPeriod;
    }
    public void setMinGetStartPeriod(String aMinGetStartPeriod) {
        if (aMinGetStartPeriod != null && !aMinGetStartPeriod.equals("")) {
            Integer tInteger = new Integer(aMinGetStartPeriod);
            int i = tInteger.intValue();
            MinGetStartPeriod = i;
        }
    }

    public int getGetEndPeriod() {
        return GetEndPeriod;
    }
    public void setGetEndPeriod(int aGetEndPeriod) {
        GetEndPeriod = aGetEndPeriod;
    }
    public void setGetEndPeriod(String aGetEndPeriod) {
        if (aGetEndPeriod != null && !aGetEndPeriod.equals("")) {
            Integer tInteger = new Integer(aGetEndPeriod);
            int i = tInteger.intValue();
            GetEndPeriod = i;
        }
    }

    public String getGetEndUnit() {
        return GetEndUnit;
    }
    public void setGetEndUnit(String aGetEndUnit) {
        GetEndUnit = aGetEndUnit;
    }
    public String getEndDateCalRef() {
        return EndDateCalRef;
    }
    public void setEndDateCalRef(String aEndDateCalRef) {
        EndDateCalRef = aEndDateCalRef;
    }
    public String getEndDateCalMode() {
        return EndDateCalMode;
    }
    public void setEndDateCalMode(String aEndDateCalMode) {
        EndDateCalMode = aEndDateCalMode;
    }
    public int getMaxGetEndPeriod() {
        return MaxGetEndPeriod;
    }
    public void setMaxGetEndPeriod(int aMaxGetEndPeriod) {
        MaxGetEndPeriod = aMaxGetEndPeriod;
    }
    public void setMaxGetEndPeriod(String aMaxGetEndPeriod) {
        if (aMaxGetEndPeriod != null && !aMaxGetEndPeriod.equals("")) {
            Integer tInteger = new Integer(aMaxGetEndPeriod);
            int i = tInteger.intValue();
            MaxGetEndPeriod = i;
        }
    }

    public String getAddFlag() {
        return AddFlag;
    }
    public void setAddFlag(String aAddFlag) {
        AddFlag = aAddFlag;
    }
    public String getSexRelaFlag() {
        return SexRelaFlag;
    }
    public void setSexRelaFlag(String aSexRelaFlag) {
        SexRelaFlag = aSexRelaFlag;
    }
    public String getUnitAppRelaFlag() {
        return UnitAppRelaFlag;
    }
    public void setUnitAppRelaFlag(String aUnitAppRelaFlag) {
        UnitAppRelaFlag = aUnitAppRelaFlag;
    }
    public String getAddAmntFlag() {
        return AddAmntFlag;
    }
    public void setAddAmntFlag(String aAddAmntFlag) {
        AddAmntFlag = aAddAmntFlag;
    }
    public String getDiscntFlag() {
        return DiscntFlag;
    }
    public void setDiscntFlag(String aDiscntFlag) {
        DiscntFlag = aDiscntFlag;
    }
    public String getInterestFlag() {
        return InterestFlag;
    }
    public void setInterestFlag(String aInterestFlag) {
        InterestFlag = aInterestFlag;
    }
    public String getShareFlag() {
        return ShareFlag;
    }
    public void setShareFlag(String aShareFlag) {
        ShareFlag = aShareFlag;
    }
    public String getInpFlag() {
        return InpFlag;
    }
    public void setInpFlag(String aInpFlag) {
        InpFlag = aInpFlag;
    }
    public String getBnfFlag() {
        return BnfFlag;
    }
    public void setBnfFlag(String aBnfFlag) {
        BnfFlag = aBnfFlag;
    }
    public String getUrgeGetFlag() {
        return UrgeGetFlag;
    }
    public void setUrgeGetFlag(String aUrgeGetFlag) {
        UrgeGetFlag = aUrgeGetFlag;
    }
    public String getDeadValiFlag() {
        return DeadValiFlag;
    }
    public void setDeadValiFlag(String aDeadValiFlag) {
        DeadValiFlag = aDeadValiFlag;
    }
    public String getGetInitFlag() {
        return GetInitFlag;
    }
    public void setGetInitFlag(String aGetInitFlag) {
        GetInitFlag = aGetInitFlag;
    }
    public double getGetLimit() {
        return GetLimit;
    }
    public void setGetLimit(double aGetLimit) {
        GetLimit = aGetLimit;
    }
    public void setGetLimit(String aGetLimit) {
        if (aGetLimit != null && !aGetLimit.equals("")) {
            Double tDouble = new Double(aGetLimit);
            double d = tDouble.doubleValue();
            GetLimit = d;
        }
    }

    public double getMaxGet() {
        return MaxGet;
    }
    public void setMaxGet(double aMaxGet) {
        MaxGet = aMaxGet;
    }
    public void setMaxGet(String aMaxGet) {
        if (aMaxGet != null && !aMaxGet.equals("")) {
            Double tDouble = new Double(aMaxGet);
            double d = tDouble.doubleValue();
            MaxGet = d;
        }
    }

    public double getGetRate() {
        return GetRate;
    }
    public void setGetRate(double aGetRate) {
        GetRate = aGetRate;
    }
    public void setGetRate(String aGetRate) {
        if (aGetRate != null && !aGetRate.equals("")) {
            Double tDouble = new Double(aGetRate);
            double d = tDouble.doubleValue();
            GetRate = d;
        }
    }

    public String getNeedAcc() {
        return NeedAcc;
    }
    public void setNeedAcc(String aNeedAcc) {
        NeedAcc = aNeedAcc;
    }
    public String getCanGet() {
        return CanGet;
    }
    public void setCanGet(String aCanGet) {
        CanGet = aCanGet;
    }
    public String getNeedCancelAcc() {
        return NeedCancelAcc;
    }
    public void setNeedCancelAcc(String aNeedCancelAcc) {
        NeedCancelAcc = aNeedCancelAcc;
    }
    public String getGetType1() {
        return GetType1;
    }
    public void setGetType1(String aGetType1) {
        GetType1 = aGetType1;
    }
    public String getZeroFlag() {
        return ZeroFlag;
    }
    public void setZeroFlag(String aZeroFlag) {
        ZeroFlag = aZeroFlag;
    }
    public String getGetType2() {
        return GetType2;
    }
    public void setGetType2(String aGetType2) {
        GetType2 = aGetType2;
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("GetDutyCode") ) {
            return 0;
        }
        if( strFieldName.equals("GetDutyName") ) {
            return 1;
        }
        if( strFieldName.equals("Type") ) {
            return 2;
        }
        if( strFieldName.equals("GetIntv") ) {
            return 3;
        }
        if( strFieldName.equals("DefaultVal") ) {
            return 4;
        }
        if( strFieldName.equals("CalCode") ) {
            return 5;
        }
        if( strFieldName.equals("CnterCalCode") ) {
            return 6;
        }
        if( strFieldName.equals("OthCalCode") ) {
            return 7;
        }
        if( strFieldName.equals("GetYear") ) {
            return 8;
        }
        if( strFieldName.equals("GetYearFlag") ) {
            return 9;
        }
        if( strFieldName.equals("StartDateCalRef") ) {
            return 10;
        }
        if( strFieldName.equals("StartDateCalMode") ) {
            return 11;
        }
        if( strFieldName.equals("MinGetStartPeriod") ) {
            return 12;
        }
        if( strFieldName.equals("GetEndPeriod") ) {
            return 13;
        }
        if( strFieldName.equals("GetEndUnit") ) {
            return 14;
        }
        if( strFieldName.equals("EndDateCalRef") ) {
            return 15;
        }
        if( strFieldName.equals("EndDateCalMode") ) {
            return 16;
        }
        if( strFieldName.equals("MaxGetEndPeriod") ) {
            return 17;
        }
        if( strFieldName.equals("AddFlag") ) {
            return 18;
        }
        if( strFieldName.equals("SexRelaFlag") ) {
            return 19;
        }
        if( strFieldName.equals("UnitAppRelaFlag") ) {
            return 20;
        }
        if( strFieldName.equals("AddAmntFlag") ) {
            return 21;
        }
        if( strFieldName.equals("DiscntFlag") ) {
            return 22;
        }
        if( strFieldName.equals("InterestFlag") ) {
            return 23;
        }
        if( strFieldName.equals("ShareFlag") ) {
            return 24;
        }
        if( strFieldName.equals("InpFlag") ) {
            return 25;
        }
        if( strFieldName.equals("BnfFlag") ) {
            return 26;
        }
        if( strFieldName.equals("UrgeGetFlag") ) {
            return 27;
        }
        if( strFieldName.equals("DeadValiFlag") ) {
            return 28;
        }
        if( strFieldName.equals("GetInitFlag") ) {
            return 29;
        }
        if( strFieldName.equals("GetLimit") ) {
            return 30;
        }
        if( strFieldName.equals("MaxGet") ) {
            return 31;
        }
        if( strFieldName.equals("GetRate") ) {
            return 32;
        }
        if( strFieldName.equals("NeedAcc") ) {
            return 33;
        }
        if( strFieldName.equals("CanGet") ) {
            return 34;
        }
        if( strFieldName.equals("NeedCancelAcc") ) {
            return 35;
        }
        if( strFieldName.equals("GetType1") ) {
            return 36;
        }
        if( strFieldName.equals("ZeroFlag") ) {
            return 37;
        }
        if( strFieldName.equals("GetType2") ) {
            return 38;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "GetDutyCode";
                break;
            case 1:
                strFieldName = "GetDutyName";
                break;
            case 2:
                strFieldName = "Type";
                break;
            case 3:
                strFieldName = "GetIntv";
                break;
            case 4:
                strFieldName = "DefaultVal";
                break;
            case 5:
                strFieldName = "CalCode";
                break;
            case 6:
                strFieldName = "CnterCalCode";
                break;
            case 7:
                strFieldName = "OthCalCode";
                break;
            case 8:
                strFieldName = "GetYear";
                break;
            case 9:
                strFieldName = "GetYearFlag";
                break;
            case 10:
                strFieldName = "StartDateCalRef";
                break;
            case 11:
                strFieldName = "StartDateCalMode";
                break;
            case 12:
                strFieldName = "MinGetStartPeriod";
                break;
            case 13:
                strFieldName = "GetEndPeriod";
                break;
            case 14:
                strFieldName = "GetEndUnit";
                break;
            case 15:
                strFieldName = "EndDateCalRef";
                break;
            case 16:
                strFieldName = "EndDateCalMode";
                break;
            case 17:
                strFieldName = "MaxGetEndPeriod";
                break;
            case 18:
                strFieldName = "AddFlag";
                break;
            case 19:
                strFieldName = "SexRelaFlag";
                break;
            case 20:
                strFieldName = "UnitAppRelaFlag";
                break;
            case 21:
                strFieldName = "AddAmntFlag";
                break;
            case 22:
                strFieldName = "DiscntFlag";
                break;
            case 23:
                strFieldName = "InterestFlag";
                break;
            case 24:
                strFieldName = "ShareFlag";
                break;
            case 25:
                strFieldName = "InpFlag";
                break;
            case 26:
                strFieldName = "BnfFlag";
                break;
            case 27:
                strFieldName = "UrgeGetFlag";
                break;
            case 28:
                strFieldName = "DeadValiFlag";
                break;
            case 29:
                strFieldName = "GetInitFlag";
                break;
            case 30:
                strFieldName = "GetLimit";
                break;
            case 31:
                strFieldName = "MaxGet";
                break;
            case 32:
                strFieldName = "GetRate";
                break;
            case 33:
                strFieldName = "NeedAcc";
                break;
            case 34:
                strFieldName = "CanGet";
                break;
            case 35:
                strFieldName = "NeedCancelAcc";
                break;
            case 36:
                strFieldName = "GetType1";
                break;
            case 37:
                strFieldName = "ZeroFlag";
                break;
            case 38:
                strFieldName = "GetType2";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "GETDUTYCODE":
                return Schema.TYPE_STRING;
            case "GETDUTYNAME":
                return Schema.TYPE_STRING;
            case "TYPE":
                return Schema.TYPE_STRING;
            case "GETINTV":
                return Schema.TYPE_INT;
            case "DEFAULTVAL":
                return Schema.TYPE_DOUBLE;
            case "CALCODE":
                return Schema.TYPE_STRING;
            case "CNTERCALCODE":
                return Schema.TYPE_STRING;
            case "OTHCALCODE":
                return Schema.TYPE_STRING;
            case "GETYEAR":
                return Schema.TYPE_INT;
            case "GETYEARFLAG":
                return Schema.TYPE_STRING;
            case "STARTDATECALREF":
                return Schema.TYPE_STRING;
            case "STARTDATECALMODE":
                return Schema.TYPE_STRING;
            case "MINGETSTARTPERIOD":
                return Schema.TYPE_INT;
            case "GETENDPERIOD":
                return Schema.TYPE_INT;
            case "GETENDUNIT":
                return Schema.TYPE_STRING;
            case "ENDDATECALREF":
                return Schema.TYPE_STRING;
            case "ENDDATECALMODE":
                return Schema.TYPE_STRING;
            case "MAXGETENDPERIOD":
                return Schema.TYPE_INT;
            case "ADDFLAG":
                return Schema.TYPE_STRING;
            case "SEXRELAFLAG":
                return Schema.TYPE_STRING;
            case "UNITAPPRELAFLAG":
                return Schema.TYPE_STRING;
            case "ADDAMNTFLAG":
                return Schema.TYPE_STRING;
            case "DISCNTFLAG":
                return Schema.TYPE_STRING;
            case "INTERESTFLAG":
                return Schema.TYPE_STRING;
            case "SHAREFLAG":
                return Schema.TYPE_STRING;
            case "INPFLAG":
                return Schema.TYPE_STRING;
            case "BNFFLAG":
                return Schema.TYPE_STRING;
            case "URGEGETFLAG":
                return Schema.TYPE_STRING;
            case "DEADVALIFLAG":
                return Schema.TYPE_STRING;
            case "GETINITFLAG":
                return Schema.TYPE_STRING;
            case "GETLIMIT":
                return Schema.TYPE_DOUBLE;
            case "MAXGET":
                return Schema.TYPE_DOUBLE;
            case "GETRATE":
                return Schema.TYPE_DOUBLE;
            case "NEEDACC":
                return Schema.TYPE_STRING;
            case "CANGET":
                return Schema.TYPE_STRING;
            case "NEEDCANCELACC":
                return Schema.TYPE_STRING;
            case "GETTYPE1":
                return Schema.TYPE_STRING;
            case "ZEROFLAG":
                return Schema.TYPE_STRING;
            case "GETTYPE2":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_STRING;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_INT;
            case 4:
                return Schema.TYPE_DOUBLE;
            case 5:
                return Schema.TYPE_STRING;
            case 6:
                return Schema.TYPE_STRING;
            case 7:
                return Schema.TYPE_STRING;
            case 8:
                return Schema.TYPE_INT;
            case 9:
                return Schema.TYPE_STRING;
            case 10:
                return Schema.TYPE_STRING;
            case 11:
                return Schema.TYPE_STRING;
            case 12:
                return Schema.TYPE_INT;
            case 13:
                return Schema.TYPE_INT;
            case 14:
                return Schema.TYPE_STRING;
            case 15:
                return Schema.TYPE_STRING;
            case 16:
                return Schema.TYPE_STRING;
            case 17:
                return Schema.TYPE_INT;
            case 18:
                return Schema.TYPE_STRING;
            case 19:
                return Schema.TYPE_STRING;
            case 20:
                return Schema.TYPE_STRING;
            case 21:
                return Schema.TYPE_STRING;
            case 22:
                return Schema.TYPE_STRING;
            case 23:
                return Schema.TYPE_STRING;
            case 24:
                return Schema.TYPE_STRING;
            case 25:
                return Schema.TYPE_STRING;
            case 26:
                return Schema.TYPE_STRING;
            case 27:
                return Schema.TYPE_STRING;
            case 28:
                return Schema.TYPE_STRING;
            case 29:
                return Schema.TYPE_STRING;
            case 30:
                return Schema.TYPE_DOUBLE;
            case 31:
                return Schema.TYPE_DOUBLE;
            case 32:
                return Schema.TYPE_DOUBLE;
            case 33:
                return Schema.TYPE_STRING;
            case 34:
                return Schema.TYPE_STRING;
            case 35:
                return Schema.TYPE_STRING;
            case 36:
                return Schema.TYPE_STRING;
            case 37:
                return Schema.TYPE_STRING;
            case 38:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("GetDutyCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GetDutyCode));
        }
        if (FCode.equalsIgnoreCase("GetDutyName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GetDutyName));
        }
        if (FCode.equalsIgnoreCase("Type")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Type));
        }
        if (FCode.equalsIgnoreCase("GetIntv")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GetIntv));
        }
        if (FCode.equalsIgnoreCase("DefaultVal")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DefaultVal));
        }
        if (FCode.equalsIgnoreCase("CalCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CalCode));
        }
        if (FCode.equalsIgnoreCase("CnterCalCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CnterCalCode));
        }
        if (FCode.equalsIgnoreCase("OthCalCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OthCalCode));
        }
        if (FCode.equalsIgnoreCase("GetYear")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GetYear));
        }
        if (FCode.equalsIgnoreCase("GetYearFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GetYearFlag));
        }
        if (FCode.equalsIgnoreCase("StartDateCalRef")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StartDateCalRef));
        }
        if (FCode.equalsIgnoreCase("StartDateCalMode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StartDateCalMode));
        }
        if (FCode.equalsIgnoreCase("MinGetStartPeriod")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MinGetStartPeriod));
        }
        if (FCode.equalsIgnoreCase("GetEndPeriod")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GetEndPeriod));
        }
        if (FCode.equalsIgnoreCase("GetEndUnit")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GetEndUnit));
        }
        if (FCode.equalsIgnoreCase("EndDateCalRef")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(EndDateCalRef));
        }
        if (FCode.equalsIgnoreCase("EndDateCalMode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(EndDateCalMode));
        }
        if (FCode.equalsIgnoreCase("MaxGetEndPeriod")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MaxGetEndPeriod));
        }
        if (FCode.equalsIgnoreCase("AddFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AddFlag));
        }
        if (FCode.equalsIgnoreCase("SexRelaFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SexRelaFlag));
        }
        if (FCode.equalsIgnoreCase("UnitAppRelaFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(UnitAppRelaFlag));
        }
        if (FCode.equalsIgnoreCase("AddAmntFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AddAmntFlag));
        }
        if (FCode.equalsIgnoreCase("DiscntFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DiscntFlag));
        }
        if (FCode.equalsIgnoreCase("InterestFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InterestFlag));
        }
        if (FCode.equalsIgnoreCase("ShareFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ShareFlag));
        }
        if (FCode.equalsIgnoreCase("InpFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InpFlag));
        }
        if (FCode.equalsIgnoreCase("BnfFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BnfFlag));
        }
        if (FCode.equalsIgnoreCase("UrgeGetFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(UrgeGetFlag));
        }
        if (FCode.equalsIgnoreCase("DeadValiFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DeadValiFlag));
        }
        if (FCode.equalsIgnoreCase("GetInitFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GetInitFlag));
        }
        if (FCode.equalsIgnoreCase("GetLimit")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GetLimit));
        }
        if (FCode.equalsIgnoreCase("MaxGet")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MaxGet));
        }
        if (FCode.equalsIgnoreCase("GetRate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GetRate));
        }
        if (FCode.equalsIgnoreCase("NeedAcc")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(NeedAcc));
        }
        if (FCode.equalsIgnoreCase("CanGet")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CanGet));
        }
        if (FCode.equalsIgnoreCase("NeedCancelAcc")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(NeedCancelAcc));
        }
        if (FCode.equalsIgnoreCase("GetType1")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GetType1));
        }
        if (FCode.equalsIgnoreCase("ZeroFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ZeroFlag));
        }
        if (FCode.equalsIgnoreCase("GetType2")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GetType2));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = String.valueOf(GetDutyCode);
                break;
            case 1:
                strFieldValue = String.valueOf(GetDutyName);
                break;
            case 2:
                strFieldValue = String.valueOf(Type);
                break;
            case 3:
                strFieldValue = String.valueOf(GetIntv);
                break;
            case 4:
                strFieldValue = String.valueOf(DefaultVal);
                break;
            case 5:
                strFieldValue = String.valueOf(CalCode);
                break;
            case 6:
                strFieldValue = String.valueOf(CnterCalCode);
                break;
            case 7:
                strFieldValue = String.valueOf(OthCalCode);
                break;
            case 8:
                strFieldValue = String.valueOf(GetYear);
                break;
            case 9:
                strFieldValue = String.valueOf(GetYearFlag);
                break;
            case 10:
                strFieldValue = String.valueOf(StartDateCalRef);
                break;
            case 11:
                strFieldValue = String.valueOf(StartDateCalMode);
                break;
            case 12:
                strFieldValue = String.valueOf(MinGetStartPeriod);
                break;
            case 13:
                strFieldValue = String.valueOf(GetEndPeriod);
                break;
            case 14:
                strFieldValue = String.valueOf(GetEndUnit);
                break;
            case 15:
                strFieldValue = String.valueOf(EndDateCalRef);
                break;
            case 16:
                strFieldValue = String.valueOf(EndDateCalMode);
                break;
            case 17:
                strFieldValue = String.valueOf(MaxGetEndPeriod);
                break;
            case 18:
                strFieldValue = String.valueOf(AddFlag);
                break;
            case 19:
                strFieldValue = String.valueOf(SexRelaFlag);
                break;
            case 20:
                strFieldValue = String.valueOf(UnitAppRelaFlag);
                break;
            case 21:
                strFieldValue = String.valueOf(AddAmntFlag);
                break;
            case 22:
                strFieldValue = String.valueOf(DiscntFlag);
                break;
            case 23:
                strFieldValue = String.valueOf(InterestFlag);
                break;
            case 24:
                strFieldValue = String.valueOf(ShareFlag);
                break;
            case 25:
                strFieldValue = String.valueOf(InpFlag);
                break;
            case 26:
                strFieldValue = String.valueOf(BnfFlag);
                break;
            case 27:
                strFieldValue = String.valueOf(UrgeGetFlag);
                break;
            case 28:
                strFieldValue = String.valueOf(DeadValiFlag);
                break;
            case 29:
                strFieldValue = String.valueOf(GetInitFlag);
                break;
            case 30:
                strFieldValue = String.valueOf(GetLimit);
                break;
            case 31:
                strFieldValue = String.valueOf(MaxGet);
                break;
            case 32:
                strFieldValue = String.valueOf(GetRate);
                break;
            case 33:
                strFieldValue = String.valueOf(NeedAcc);
                break;
            case 34:
                strFieldValue = String.valueOf(CanGet);
                break;
            case 35:
                strFieldValue = String.valueOf(NeedCancelAcc);
                break;
            case 36:
                strFieldValue = String.valueOf(GetType1);
                break;
            case 37:
                strFieldValue = String.valueOf(ZeroFlag);
                break;
            case 38:
                strFieldValue = String.valueOf(GetType2);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("GetDutyCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                GetDutyCode = FValue.trim();
            }
            else
                GetDutyCode = null;
        }
        if (FCode.equalsIgnoreCase("GetDutyName")) {
            if( FValue != null && !FValue.equals(""))
            {
                GetDutyName = FValue.trim();
            }
            else
                GetDutyName = null;
        }
        if (FCode.equalsIgnoreCase("Type")) {
            if( FValue != null && !FValue.equals(""))
            {
                Type = FValue.trim();
            }
            else
                Type = null;
        }
        if (FCode.equalsIgnoreCase("GetIntv")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                GetIntv = i;
            }
        }
        if (FCode.equalsIgnoreCase("DefaultVal")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                DefaultVal = d;
            }
        }
        if (FCode.equalsIgnoreCase("CalCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                CalCode = FValue.trim();
            }
            else
                CalCode = null;
        }
        if (FCode.equalsIgnoreCase("CnterCalCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                CnterCalCode = FValue.trim();
            }
            else
                CnterCalCode = null;
        }
        if (FCode.equalsIgnoreCase("OthCalCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                OthCalCode = FValue.trim();
            }
            else
                OthCalCode = null;
        }
        if (FCode.equalsIgnoreCase("GetYear")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                GetYear = i;
            }
        }
        if (FCode.equalsIgnoreCase("GetYearFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                GetYearFlag = FValue.trim();
            }
            else
                GetYearFlag = null;
        }
        if (FCode.equalsIgnoreCase("StartDateCalRef")) {
            if( FValue != null && !FValue.equals(""))
            {
                StartDateCalRef = FValue.trim();
            }
            else
                StartDateCalRef = null;
        }
        if (FCode.equalsIgnoreCase("StartDateCalMode")) {
            if( FValue != null && !FValue.equals(""))
            {
                StartDateCalMode = FValue.trim();
            }
            else
                StartDateCalMode = null;
        }
        if (FCode.equalsIgnoreCase("MinGetStartPeriod")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                MinGetStartPeriod = i;
            }
        }
        if (FCode.equalsIgnoreCase("GetEndPeriod")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                GetEndPeriod = i;
            }
        }
        if (FCode.equalsIgnoreCase("GetEndUnit")) {
            if( FValue != null && !FValue.equals(""))
            {
                GetEndUnit = FValue.trim();
            }
            else
                GetEndUnit = null;
        }
        if (FCode.equalsIgnoreCase("EndDateCalRef")) {
            if( FValue != null && !FValue.equals(""))
            {
                EndDateCalRef = FValue.trim();
            }
            else
                EndDateCalRef = null;
        }
        if (FCode.equalsIgnoreCase("EndDateCalMode")) {
            if( FValue != null && !FValue.equals(""))
            {
                EndDateCalMode = FValue.trim();
            }
            else
                EndDateCalMode = null;
        }
        if (FCode.equalsIgnoreCase("MaxGetEndPeriod")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                MaxGetEndPeriod = i;
            }
        }
        if (FCode.equalsIgnoreCase("AddFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                AddFlag = FValue.trim();
            }
            else
                AddFlag = null;
        }
        if (FCode.equalsIgnoreCase("SexRelaFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                SexRelaFlag = FValue.trim();
            }
            else
                SexRelaFlag = null;
        }
        if (FCode.equalsIgnoreCase("UnitAppRelaFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                UnitAppRelaFlag = FValue.trim();
            }
            else
                UnitAppRelaFlag = null;
        }
        if (FCode.equalsIgnoreCase("AddAmntFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                AddAmntFlag = FValue.trim();
            }
            else
                AddAmntFlag = null;
        }
        if (FCode.equalsIgnoreCase("DiscntFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                DiscntFlag = FValue.trim();
            }
            else
                DiscntFlag = null;
        }
        if (FCode.equalsIgnoreCase("InterestFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                InterestFlag = FValue.trim();
            }
            else
                InterestFlag = null;
        }
        if (FCode.equalsIgnoreCase("ShareFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                ShareFlag = FValue.trim();
            }
            else
                ShareFlag = null;
        }
        if (FCode.equalsIgnoreCase("InpFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                InpFlag = FValue.trim();
            }
            else
                InpFlag = null;
        }
        if (FCode.equalsIgnoreCase("BnfFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                BnfFlag = FValue.trim();
            }
            else
                BnfFlag = null;
        }
        if (FCode.equalsIgnoreCase("UrgeGetFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                UrgeGetFlag = FValue.trim();
            }
            else
                UrgeGetFlag = null;
        }
        if (FCode.equalsIgnoreCase("DeadValiFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                DeadValiFlag = FValue.trim();
            }
            else
                DeadValiFlag = null;
        }
        if (FCode.equalsIgnoreCase("GetInitFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                GetInitFlag = FValue.trim();
            }
            else
                GetInitFlag = null;
        }
        if (FCode.equalsIgnoreCase("GetLimit")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                GetLimit = d;
            }
        }
        if (FCode.equalsIgnoreCase("MaxGet")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                MaxGet = d;
            }
        }
        if (FCode.equalsIgnoreCase("GetRate")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                GetRate = d;
            }
        }
        if (FCode.equalsIgnoreCase("NeedAcc")) {
            if( FValue != null && !FValue.equals(""))
            {
                NeedAcc = FValue.trim();
            }
            else
                NeedAcc = null;
        }
        if (FCode.equalsIgnoreCase("CanGet")) {
            if( FValue != null && !FValue.equals(""))
            {
                CanGet = FValue.trim();
            }
            else
                CanGet = null;
        }
        if (FCode.equalsIgnoreCase("NeedCancelAcc")) {
            if( FValue != null && !FValue.equals(""))
            {
                NeedCancelAcc = FValue.trim();
            }
            else
                NeedCancelAcc = null;
        }
        if (FCode.equalsIgnoreCase("GetType1")) {
            if( FValue != null && !FValue.equals(""))
            {
                GetType1 = FValue.trim();
            }
            else
                GetType1 = null;
        }
        if (FCode.equalsIgnoreCase("ZeroFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                ZeroFlag = FValue.trim();
            }
            else
                ZeroFlag = null;
        }
        if (FCode.equalsIgnoreCase("GetType2")) {
            if( FValue != null && !FValue.equals(""))
            {
                GetType2 = FValue.trim();
            }
            else
                GetType2 = null;
        }
        return true;
    }


    public String toString() {
    return "LMDutyGetPojo [" +
            "GetDutyCode="+GetDutyCode +
            ", GetDutyName="+GetDutyName +
            ", Type="+Type +
            ", GetIntv="+GetIntv +
            ", DefaultVal="+DefaultVal +
            ", CalCode="+CalCode +
            ", CnterCalCode="+CnterCalCode +
            ", OthCalCode="+OthCalCode +
            ", GetYear="+GetYear +
            ", GetYearFlag="+GetYearFlag +
            ", StartDateCalRef="+StartDateCalRef +
            ", StartDateCalMode="+StartDateCalMode +
            ", MinGetStartPeriod="+MinGetStartPeriod +
            ", GetEndPeriod="+GetEndPeriod +
            ", GetEndUnit="+GetEndUnit +
            ", EndDateCalRef="+EndDateCalRef +
            ", EndDateCalMode="+EndDateCalMode +
            ", MaxGetEndPeriod="+MaxGetEndPeriod +
            ", AddFlag="+AddFlag +
            ", SexRelaFlag="+SexRelaFlag +
            ", UnitAppRelaFlag="+UnitAppRelaFlag +
            ", AddAmntFlag="+AddAmntFlag +
            ", DiscntFlag="+DiscntFlag +
            ", InterestFlag="+InterestFlag +
            ", ShareFlag="+ShareFlag +
            ", InpFlag="+InpFlag +
            ", BnfFlag="+BnfFlag +
            ", UrgeGetFlag="+UrgeGetFlag +
            ", DeadValiFlag="+DeadValiFlag +
            ", GetInitFlag="+GetInitFlag +
            ", GetLimit="+GetLimit +
            ", MaxGet="+MaxGet +
            ", GetRate="+GetRate +
            ", NeedAcc="+NeedAcc +
            ", CanGet="+CanGet +
            ", NeedCancelAcc="+NeedCancelAcc +
            ", GetType1="+GetType1 +
            ", ZeroFlag="+ZeroFlag +
            ", GetType2="+GetType2 +"]";
    }
}
