/**
 * Copyright (c) 2018 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.entity;

import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.Pojo;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;

import java.io.Serializable;

/**
 * <p>ClassName: LJAGetOtherPojo </p>
 * <p>Copyright: Copyright (c) 2018</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2018-09-07
 */
public class LJAGetOtherPojo implements Pojo,Serializable {
    // @Field
    /** 实付号码 */
    private String ActuGetNo;
    /** 其它退费收据号码 */
    private String OtherNo;
    /** 其它退费收据类型 */
    private String OtherNoType;
    /** 交费方式 */
    private String PayMode;
    /** 退费金额 */
    private double GetMoney;
    /** 退费日期 */
    private String  GetDate;
    /** 到帐日期 */
    private String  EnterAccDate;
    /** 确认日期 */
    private String  ConfDate;
    /** 管理机构 */
    private String ManageCom;
    /** 代理机构 */
    private String AgentCom;
    /** 代理机构内部分类 */
    private String AgentType;
    /** 投保人名称 */
    private String APPntName;
    /** 代理人组别 */
    private String AgentGroup;
    /** 代理人编码 */
    private String AgentCode;
    /** 补退费业务类型 */
    private String FeeOperationType;
    /** 补退费财务类型 */
    private String FeeFinaType;
    /** 流水号 */
    private String SerialNo;
    /** 操作员 */
    private String Operator;
    /** 入机时间 */
    private String MakeTime;
    /** 入机日期 */
    private String  MakeDate;
    /** 状态 */
    private String State;
    /** 给付通知书号码 */
    private String GetNoticeNo;
    /** 最后一次修改日期 */
    private String  ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime;


    public static final int FIELDNUM = 24;    // 数据库表的字段个数

    private FDate fDate = new FDate();        // 处理日期
    public String getActuGetNo() {
        return ActuGetNo;
    }
    public void setActuGetNo(String aActuGetNo) {
        ActuGetNo = aActuGetNo;
    }
    public String getOtherNo() {
        return OtherNo;
    }
    public void setOtherNo(String aOtherNo) {
        OtherNo = aOtherNo;
    }
    public String getOtherNoType() {
        return OtherNoType;
    }
    public void setOtherNoType(String aOtherNoType) {
        OtherNoType = aOtherNoType;
    }
    public String getPayMode() {
        return PayMode;
    }
    public void setPayMode(String aPayMode) {
        PayMode = aPayMode;
    }
    public double getGetMoney() {
        return GetMoney;
    }
    public void setGetMoney(double aGetMoney) {
        GetMoney = aGetMoney;
    }
    public void setGetMoney(String aGetMoney) {
        if (aGetMoney != null && !aGetMoney.equals("")) {
            Double tDouble = new Double(aGetMoney);
            double d = tDouble.doubleValue();
            GetMoney = d;
        }
    }

    public String getGetDate() {
        return GetDate;
    }
    public void setGetDate(String aGetDate) {
        GetDate = aGetDate;
    }
    public String getEnterAccDate() {
        return EnterAccDate;
    }
    public void setEnterAccDate(String aEnterAccDate) {
        EnterAccDate = aEnterAccDate;
    }
    public String getConfDate() {
        return ConfDate;
    }
    public void setConfDate(String aConfDate) {
        ConfDate = aConfDate;
    }
    public String getManageCom() {
        return ManageCom;
    }
    public void setManageCom(String aManageCom) {
        ManageCom = aManageCom;
    }
    public String getAgentCom() {
        return AgentCom;
    }
    public void setAgentCom(String aAgentCom) {
        AgentCom = aAgentCom;
    }
    public String getAgentType() {
        return AgentType;
    }
    public void setAgentType(String aAgentType) {
        AgentType = aAgentType;
    }
    public String getAPPntName() {
        return APPntName;
    }
    public void setAPPntName(String aAPPntName) {
        APPntName = aAPPntName;
    }
    public String getAgentGroup() {
        return AgentGroup;
    }
    public void setAgentGroup(String aAgentGroup) {
        AgentGroup = aAgentGroup;
    }
    public String getAgentCode() {
        return AgentCode;
    }
    public void setAgentCode(String aAgentCode) {
        AgentCode = aAgentCode;
    }
    public String getFeeOperationType() {
        return FeeOperationType;
    }
    public void setFeeOperationType(String aFeeOperationType) {
        FeeOperationType = aFeeOperationType;
    }
    public String getFeeFinaType() {
        return FeeFinaType;
    }
    public void setFeeFinaType(String aFeeFinaType) {
        FeeFinaType = aFeeFinaType;
    }
    public String getSerialNo() {
        return SerialNo;
    }
    public void setSerialNo(String aSerialNo) {
        SerialNo = aSerialNo;
    }
    public String getOperator() {
        return Operator;
    }
    public void setOperator(String aOperator) {
        Operator = aOperator;
    }
    public String getMakeTime() {
        return MakeTime;
    }
    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }
    public String getMakeDate() {
        return MakeDate;
    }
    public void setMakeDate(String aMakeDate) {
        MakeDate = aMakeDate;
    }
    public String getState() {
        return State;
    }
    public void setState(String aState) {
        State = aState;
    }
    public String getGetNoticeNo() {
        return GetNoticeNo;
    }
    public void setGetNoticeNo(String aGetNoticeNo) {
        GetNoticeNo = aGetNoticeNo;
    }
    public String getModifyDate() {
        return ModifyDate;
    }
    public void setModifyDate(String aModifyDate) {
        ModifyDate = aModifyDate;
    }
    public String getModifyTime() {
        return ModifyTime;
    }
    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }

    /**
     * 取得Schema拥有字段的数量
     * @return: int
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("ActuGetNo") ) {
            return 0;
        }
        if( strFieldName.equals("OtherNo") ) {
            return 1;
        }
        if( strFieldName.equals("OtherNoType") ) {
            return 2;
        }
        if( strFieldName.equals("PayMode") ) {
            return 3;
        }
        if( strFieldName.equals("GetMoney") ) {
            return 4;
        }
        if( strFieldName.equals("GetDate") ) {
            return 5;
        }
        if( strFieldName.equals("EnterAccDate") ) {
            return 6;
        }
        if( strFieldName.equals("ConfDate") ) {
            return 7;
        }
        if( strFieldName.equals("ManageCom") ) {
            return 8;
        }
        if( strFieldName.equals("AgentCom") ) {
            return 9;
        }
        if( strFieldName.equals("AgentType") ) {
            return 10;
        }
        if( strFieldName.equals("APPntName") ) {
            return 11;
        }
        if( strFieldName.equals("AgentGroup") ) {
            return 12;
        }
        if( strFieldName.equals("AgentCode") ) {
            return 13;
        }
        if( strFieldName.equals("FeeOperationType") ) {
            return 14;
        }
        if( strFieldName.equals("FeeFinaType") ) {
            return 15;
        }
        if( strFieldName.equals("SerialNo") ) {
            return 16;
        }
        if( strFieldName.equals("Operator") ) {
            return 17;
        }
        if( strFieldName.equals("MakeTime") ) {
            return 18;
        }
        if( strFieldName.equals("MakeDate") ) {
            return 19;
        }
        if( strFieldName.equals("State") ) {
            return 20;
        }
        if( strFieldName.equals("GetNoticeNo") ) {
            return 21;
        }
        if( strFieldName.equals("ModifyDate") ) {
            return 22;
        }
        if( strFieldName.equals("ModifyTime") ) {
            return 23;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     * @param: nFieldIndex int
     * @return: String
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "ActuGetNo";
                break;
            case 1:
                strFieldName = "OtherNo";
                break;
            case 2:
                strFieldName = "OtherNoType";
                break;
            case 3:
                strFieldName = "PayMode";
                break;
            case 4:
                strFieldName = "GetMoney";
                break;
            case 5:
                strFieldName = "GetDate";
                break;
            case 6:
                strFieldName = "EnterAccDate";
                break;
            case 7:
                strFieldName = "ConfDate";
                break;
            case 8:
                strFieldName = "ManageCom";
                break;
            case 9:
                strFieldName = "AgentCom";
                break;
            case 10:
                strFieldName = "AgentType";
                break;
            case 11:
                strFieldName = "APPntName";
                break;
            case 12:
                strFieldName = "AgentGroup";
                break;
            case 13:
                strFieldName = "AgentCode";
                break;
            case 14:
                strFieldName = "FeeOperationType";
                break;
            case 15:
                strFieldName = "FeeFinaType";
                break;
            case 16:
                strFieldName = "SerialNo";
                break;
            case 17:
                strFieldName = "Operator";
                break;
            case 18:
                strFieldName = "MakeTime";
                break;
            case 19:
                strFieldName = "MakeDate";
                break;
            case 20:
                strFieldName = "State";
                break;
            case 21:
                strFieldName = "GetNoticeNo";
                break;
            case 22:
                strFieldName = "ModifyDate";
                break;
            case 23:
                strFieldName = "ModifyTime";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "ACTUGETNO":
                return Schema.TYPE_STRING;
            case "OTHERNO":
                return Schema.TYPE_STRING;
            case "OTHERNOTYPE":
                return Schema.TYPE_STRING;
            case "PAYMODE":
                return Schema.TYPE_STRING;
            case "GETMONEY":
                return Schema.TYPE_DOUBLE;
            case "GETDATE":
                return Schema.TYPE_STRING;
            case "ENTERACCDATE":
                return Schema.TYPE_STRING;
            case "CONFDATE":
                return Schema.TYPE_STRING;
            case "MANAGECOM":
                return Schema.TYPE_STRING;
            case "AGENTCOM":
                return Schema.TYPE_STRING;
            case "AGENTTYPE":
                return Schema.TYPE_STRING;
            case "APPNTNAME":
                return Schema.TYPE_STRING;
            case "AGENTGROUP":
                return Schema.TYPE_STRING;
            case "AGENTCODE":
                return Schema.TYPE_STRING;
            case "FEEOPERATIONTYPE":
                return Schema.TYPE_STRING;
            case "FEEFINATYPE":
                return Schema.TYPE_STRING;
            case "SERIALNO":
                return Schema.TYPE_STRING;
            case "OPERATOR":
                return Schema.TYPE_STRING;
            case "MAKETIME":
                return Schema.TYPE_STRING;
            case "MAKEDATE":
                return Schema.TYPE_STRING;
            case "STATE":
                return Schema.TYPE_STRING;
            case "GETNOTICENO":
                return Schema.TYPE_STRING;
            case "MODIFYDATE":
                return Schema.TYPE_STRING;
            case "MODIFYTIME":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: nFieldIndex int
     * @return: int
     **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_STRING;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_DOUBLE;
            case 5:
                return Schema.TYPE_STRING;
            case 6:
                return Schema.TYPE_STRING;
            case 7:
                return Schema.TYPE_STRING;
            case 8:
                return Schema.TYPE_STRING;
            case 9:
                return Schema.TYPE_STRING;
            case 10:
                return Schema.TYPE_STRING;
            case 11:
                return Schema.TYPE_STRING;
            case 12:
                return Schema.TYPE_STRING;
            case 13:
                return Schema.TYPE_STRING;
            case 14:
                return Schema.TYPE_STRING;
            case 15:
                return Schema.TYPE_STRING;
            case 16:
                return Schema.TYPE_STRING;
            case 17:
                return Schema.TYPE_STRING;
            case 18:
                return Schema.TYPE_STRING;
            case 19:
                return Schema.TYPE_STRING;
            case 20:
                return Schema.TYPE_STRING;
            case 21:
                return Schema.TYPE_STRING;
            case 22:
                return Schema.TYPE_STRING;
            case 23:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode String 希望取得的字段名
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("ActuGetNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ActuGetNo));
        }
        if (FCode.equalsIgnoreCase("OtherNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OtherNo));
        }
        if (FCode.equalsIgnoreCase("OtherNoType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OtherNoType));
        }
        if (FCode.equalsIgnoreCase("PayMode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PayMode));
        }
        if (FCode.equalsIgnoreCase("GetMoney")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GetMoney));
        }
        if (FCode.equalsIgnoreCase("GetDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GetDate));
        }
        if (FCode.equalsIgnoreCase("EnterAccDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(EnterAccDate));
        }
        if (FCode.equalsIgnoreCase("ConfDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ConfDate));
        }
        if (FCode.equalsIgnoreCase("ManageCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
        }
        if (FCode.equalsIgnoreCase("AgentCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentCom));
        }
        if (FCode.equalsIgnoreCase("AgentType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentType));
        }
        if (FCode.equalsIgnoreCase("APPntName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(APPntName));
        }
        if (FCode.equalsIgnoreCase("AgentGroup")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentGroup));
        }
        if (FCode.equalsIgnoreCase("AgentCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentCode));
        }
        if (FCode.equalsIgnoreCase("FeeOperationType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FeeOperationType));
        }
        if (FCode.equalsIgnoreCase("FeeFinaType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FeeFinaType));
        }
        if (FCode.equalsIgnoreCase("SerialNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SerialNo));
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeDate));
        }
        if (FCode.equalsIgnoreCase("State")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(State));
        }
        if (FCode.equalsIgnoreCase("GetNoticeNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GetNoticeNo));
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyDate));
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex int 指定的字段索引值
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = String.valueOf(ActuGetNo);
                break;
            case 1:
                strFieldValue = String.valueOf(OtherNo);
                break;
            case 2:
                strFieldValue = String.valueOf(OtherNoType);
                break;
            case 3:
                strFieldValue = String.valueOf(PayMode);
                break;
            case 4:
                strFieldValue = String.valueOf(GetMoney);
                break;
            case 5:
                strFieldValue = String.valueOf(GetDate);
                break;
            case 6:
                strFieldValue = String.valueOf(EnterAccDate);
                break;
            case 7:
                strFieldValue = String.valueOf(ConfDate);
                break;
            case 8:
                strFieldValue = String.valueOf(ManageCom);
                break;
            case 9:
                strFieldValue = String.valueOf(AgentCom);
                break;
            case 10:
                strFieldValue = String.valueOf(AgentType);
                break;
            case 11:
                strFieldValue = String.valueOf(APPntName);
                break;
            case 12:
                strFieldValue = String.valueOf(AgentGroup);
                break;
            case 13:
                strFieldValue = String.valueOf(AgentCode);
                break;
            case 14:
                strFieldValue = String.valueOf(FeeOperationType);
                break;
            case 15:
                strFieldValue = String.valueOf(FeeFinaType);
                break;
            case 16:
                strFieldValue = String.valueOf(SerialNo);
                break;
            case 17:
                strFieldValue = String.valueOf(Operator);
                break;
            case 18:
                strFieldValue = String.valueOf(MakeTime);
                break;
            case 19:
                strFieldValue = String.valueOf(MakeDate);
                break;
            case 20:
                strFieldValue = String.valueOf(State);
                break;
            case 21:
                strFieldValue = String.valueOf(GetNoticeNo);
                break;
            case 22:
                strFieldValue = String.valueOf(ModifyDate);
                break;
            case 23:
                strFieldValue = String.valueOf(ModifyTime);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode String 需要赋值的对象
     * @param: FValue String 要赋的值
     * @return: boolean
     **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("ActuGetNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                ActuGetNo = FValue.trim();
            }
            else
                ActuGetNo = null;
        }
        if (FCode.equalsIgnoreCase("OtherNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                OtherNo = FValue.trim();
            }
            else
                OtherNo = null;
        }
        if (FCode.equalsIgnoreCase("OtherNoType")) {
            if( FValue != null && !FValue.equals(""))
            {
                OtherNoType = FValue.trim();
            }
            else
                OtherNoType = null;
        }
        if (FCode.equalsIgnoreCase("PayMode")) {
            if( FValue != null && !FValue.equals(""))
            {
                PayMode = FValue.trim();
            }
            else
                PayMode = null;
        }
        if (FCode.equalsIgnoreCase("GetMoney")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                GetMoney = d;
            }
        }
        if (FCode.equalsIgnoreCase("GetDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                GetDate = FValue.trim();
            }
            else
                GetDate = null;
        }
        if (FCode.equalsIgnoreCase("EnterAccDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                EnterAccDate = FValue.trim();
            }
            else
                EnterAccDate = null;
        }
        if (FCode.equalsIgnoreCase("ConfDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                ConfDate = FValue.trim();
            }
            else
                ConfDate = null;
        }
        if (FCode.equalsIgnoreCase("ManageCom")) {
            if( FValue != null && !FValue.equals(""))
            {
                ManageCom = FValue.trim();
            }
            else
                ManageCom = null;
        }
        if (FCode.equalsIgnoreCase("AgentCom")) {
            if( FValue != null && !FValue.equals(""))
            {
                AgentCom = FValue.trim();
            }
            else
                AgentCom = null;
        }
        if (FCode.equalsIgnoreCase("AgentType")) {
            if( FValue != null && !FValue.equals(""))
            {
                AgentType = FValue.trim();
            }
            else
                AgentType = null;
        }
        if (FCode.equalsIgnoreCase("APPntName")) {
            if( FValue != null && !FValue.equals(""))
            {
                APPntName = FValue.trim();
            }
            else
                APPntName = null;
        }
        if (FCode.equalsIgnoreCase("AgentGroup")) {
            if( FValue != null && !FValue.equals(""))
            {
                AgentGroup = FValue.trim();
            }
            else
                AgentGroup = null;
        }
        if (FCode.equalsIgnoreCase("AgentCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                AgentCode = FValue.trim();
            }
            else
                AgentCode = null;
        }
        if (FCode.equalsIgnoreCase("FeeOperationType")) {
            if( FValue != null && !FValue.equals(""))
            {
                FeeOperationType = FValue.trim();
            }
            else
                FeeOperationType = null;
        }
        if (FCode.equalsIgnoreCase("FeeFinaType")) {
            if( FValue != null && !FValue.equals(""))
            {
                FeeFinaType = FValue.trim();
            }
            else
                FeeFinaType = null;
        }
        if (FCode.equalsIgnoreCase("SerialNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                SerialNo = FValue.trim();
            }
            else
                SerialNo = null;
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            if( FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
                Operator = null;
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
                MakeTime = null;
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeDate = FValue.trim();
            }
            else
                MakeDate = null;
        }
        if (FCode.equalsIgnoreCase("State")) {
            if( FValue != null && !FValue.equals(""))
            {
                State = FValue.trim();
            }
            else
                State = null;
        }
        if (FCode.equalsIgnoreCase("GetNoticeNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                GetNoticeNo = FValue.trim();
            }
            else
                GetNoticeNo = null;
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyDate = FValue.trim();
            }
            else
                ModifyDate = null;
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
                ModifyTime = null;
        }
        return true;
    }


    public String toString() {
        return "LJAGetOtherPojo [" +
                "ActuGetNo="+ActuGetNo +
                ", OtherNo="+OtherNo +
                ", OtherNoType="+OtherNoType +
                ", PayMode="+PayMode +
                ", GetMoney="+GetMoney +
                ", GetDate="+GetDate +
                ", EnterAccDate="+EnterAccDate +
                ", ConfDate="+ConfDate +
                ", ManageCom="+ManageCom +
                ", AgentCom="+AgentCom +
                ", AgentType="+AgentType +
                ", APPntName="+APPntName +
                ", AgentGroup="+AgentGroup +
                ", AgentCode="+AgentCode +
                ", FeeOperationType="+FeeOperationType +
                ", FeeFinaType="+FeeFinaType +
                ", SerialNo="+SerialNo +
                ", Operator="+Operator +
                ", MakeTime="+MakeTime +
                ", MakeDate="+MakeDate +
                ", State="+State +
                ", GetNoticeNo="+GetNoticeNo +
                ", ModifyDate="+ModifyDate +
                ", ModifyTime="+ModifyTime +"]";
    }
}
