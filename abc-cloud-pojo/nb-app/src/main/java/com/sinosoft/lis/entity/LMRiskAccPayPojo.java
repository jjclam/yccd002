/**
 * Copyright (c) 2017 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.entity;

import com.sinosoft.cloud.cache.annotation.RedisIndexHKey;
import com.sinosoft.cloud.cache.annotation.RedisPrimaryHKey;
import com.sinosoft.utility.Pojo;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;

import java.io.Serializable;

/**
 * <p>ClassName: LMRiskAccPayPojo </p>
 * <p>Copyright: Copyright (c) 2017</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2017-10-19
 */
public class LMRiskAccPayPojo implements Pojo,Serializable {
    // @Field
    /** 险种编码 */
    @RedisPrimaryHKey
    private String RiskCode;
    /** 险种版本 */
    private String RiskVer; 
    /** 保险帐户号码 */
    private String InsuAccNo; 
    /** 默认比例 */
    private double DefaultRate; 
    /** 是否需要录入 */
    private String NeedInput; 
    /** 账户产生位置 */
    private String AccCreatePos; 
    /** 转入账户时的算法编码(现金) */
    private String CalCodeMoney; 
    /** 转入账户时的算法编码(股份) */
    private String CalCodeUnit; 
    /** 账户转入计算标志 */
    private String CalFlag; 
    /** 缴费编码 */
    @RedisIndexHKey
    @RedisPrimaryHKey
    private String PayPlanCode;
    /** 缴费名称 */
    private String PayPlanName; 
    /** 账户交费转入位置 */
    private String PayNeedToAcc; 
    /** 险种帐户交费名 */
    private String RiskAccPayName; 
    /** Ascription */
    private String Ascription; 
    /** 投资比例下限 */
    private double InvestMinRate; 
    /** 投资比例上限 */
    private double InvestMaxRate; 
    /** 投资比例 */
    private double InvestRate; 


    public static final int FIELDNUM = 17;    // 数据库表的字段个数
    public String getRiskCode() {
        return RiskCode;
    }
    public void setRiskCode(String aRiskCode) {
        RiskCode = aRiskCode;
    }
    public String getRiskVer() {
        return RiskVer;
    }
    public void setRiskVer(String aRiskVer) {
        RiskVer = aRiskVer;
    }
    public String getInsuAccNo() {
        return InsuAccNo;
    }
    public void setInsuAccNo(String aInsuAccNo) {
        InsuAccNo = aInsuAccNo;
    }
    public double getDefaultRate() {
        return DefaultRate;
    }
    public void setDefaultRate(double aDefaultRate) {
        DefaultRate = aDefaultRate;
    }
    public void setDefaultRate(String aDefaultRate) {
        if (aDefaultRate != null && !aDefaultRate.equals("")) {
            Double tDouble = new Double(aDefaultRate);
            double d = tDouble.doubleValue();
            DefaultRate = d;
        }
    }

    public String getNeedInput() {
        return NeedInput;
    }
    public void setNeedInput(String aNeedInput) {
        NeedInput = aNeedInput;
    }
    public String getAccCreatePos() {
        return AccCreatePos;
    }
    public void setAccCreatePos(String aAccCreatePos) {
        AccCreatePos = aAccCreatePos;
    }
    public String getCalCodeMoney() {
        return CalCodeMoney;
    }
    public void setCalCodeMoney(String aCalCodeMoney) {
        CalCodeMoney = aCalCodeMoney;
    }
    public String getCalCodeUnit() {
        return CalCodeUnit;
    }
    public void setCalCodeUnit(String aCalCodeUnit) {
        CalCodeUnit = aCalCodeUnit;
    }
    public String getCalFlag() {
        return CalFlag;
    }
    public void setCalFlag(String aCalFlag) {
        CalFlag = aCalFlag;
    }
    public String getPayPlanCode() {
        return PayPlanCode;
    }
    public void setPayPlanCode(String aPayPlanCode) {
        PayPlanCode = aPayPlanCode;
    }
    public String getPayPlanName() {
        return PayPlanName;
    }
    public void setPayPlanName(String aPayPlanName) {
        PayPlanName = aPayPlanName;
    }
    public String getPayNeedToAcc() {
        return PayNeedToAcc;
    }
    public void setPayNeedToAcc(String aPayNeedToAcc) {
        PayNeedToAcc = aPayNeedToAcc;
    }
    public String getRiskAccPayName() {
        return RiskAccPayName;
    }
    public void setRiskAccPayName(String aRiskAccPayName) {
        RiskAccPayName = aRiskAccPayName;
    }
    public String getAscription() {
        return Ascription;
    }
    public void setAscription(String aAscription) {
        Ascription = aAscription;
    }
    public double getInvestMinRate() {
        return InvestMinRate;
    }
    public void setInvestMinRate(double aInvestMinRate) {
        InvestMinRate = aInvestMinRate;
    }
    public void setInvestMinRate(String aInvestMinRate) {
        if (aInvestMinRate != null && !aInvestMinRate.equals("")) {
            Double tDouble = new Double(aInvestMinRate);
            double d = tDouble.doubleValue();
            InvestMinRate = d;
        }
    }

    public double getInvestMaxRate() {
        return InvestMaxRate;
    }
    public void setInvestMaxRate(double aInvestMaxRate) {
        InvestMaxRate = aInvestMaxRate;
    }
    public void setInvestMaxRate(String aInvestMaxRate) {
        if (aInvestMaxRate != null && !aInvestMaxRate.equals("")) {
            Double tDouble = new Double(aInvestMaxRate);
            double d = tDouble.doubleValue();
            InvestMaxRate = d;
        }
    }

    public double getInvestRate() {
        return InvestRate;
    }
    public void setInvestRate(double aInvestRate) {
        InvestRate = aInvestRate;
    }
    public void setInvestRate(String aInvestRate) {
        if (aInvestRate != null && !aInvestRate.equals("")) {
            Double tDouble = new Double(aInvestRate);
            double d = tDouble.doubleValue();
            InvestRate = d;
        }
    }


    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("RiskCode") ) {
            return 0;
        }
        if( strFieldName.equals("RiskVer") ) {
            return 1;
        }
        if( strFieldName.equals("InsuAccNo") ) {
            return 2;
        }
        if( strFieldName.equals("DefaultRate") ) {
            return 3;
        }
        if( strFieldName.equals("NeedInput") ) {
            return 4;
        }
        if( strFieldName.equals("AccCreatePos") ) {
            return 5;
        }
        if( strFieldName.equals("CalCodeMoney") ) {
            return 6;
        }
        if( strFieldName.equals("CalCodeUnit") ) {
            return 7;
        }
        if( strFieldName.equals("CalFlag") ) {
            return 8;
        }
        if( strFieldName.equals("PayPlanCode") ) {
            return 9;
        }
        if( strFieldName.equals("PayPlanName") ) {
            return 10;
        }
        if( strFieldName.equals("PayNeedToAcc") ) {
            return 11;
        }
        if( strFieldName.equals("RiskAccPayName") ) {
            return 12;
        }
        if( strFieldName.equals("Ascription") ) {
            return 13;
        }
        if( strFieldName.equals("InvestMinRate") ) {
            return 14;
        }
        if( strFieldName.equals("InvestMaxRate") ) {
            return 15;
        }
        if( strFieldName.equals("InvestRate") ) {
            return 16;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "RiskCode";
                break;
            case 1:
                strFieldName = "RiskVer";
                break;
            case 2:
                strFieldName = "InsuAccNo";
                break;
            case 3:
                strFieldName = "DefaultRate";
                break;
            case 4:
                strFieldName = "NeedInput";
                break;
            case 5:
                strFieldName = "AccCreatePos";
                break;
            case 6:
                strFieldName = "CalCodeMoney";
                break;
            case 7:
                strFieldName = "CalCodeUnit";
                break;
            case 8:
                strFieldName = "CalFlag";
                break;
            case 9:
                strFieldName = "PayPlanCode";
                break;
            case 10:
                strFieldName = "PayPlanName";
                break;
            case 11:
                strFieldName = "PayNeedToAcc";
                break;
            case 12:
                strFieldName = "RiskAccPayName";
                break;
            case 13:
                strFieldName = "Ascription";
                break;
            case 14:
                strFieldName = "InvestMinRate";
                break;
            case 15:
                strFieldName = "InvestMaxRate";
                break;
            case 16:
                strFieldName = "InvestRate";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "RISKCODE":
                return Schema.TYPE_STRING;
            case "RISKVER":
                return Schema.TYPE_STRING;
            case "INSUACCNO":
                return Schema.TYPE_STRING;
            case "DEFAULTRATE":
                return Schema.TYPE_DOUBLE;
            case "NEEDINPUT":
                return Schema.TYPE_STRING;
            case "ACCCREATEPOS":
                return Schema.TYPE_STRING;
            case "CALCODEMONEY":
                return Schema.TYPE_STRING;
            case "CALCODEUNIT":
                return Schema.TYPE_STRING;
            case "CALFLAG":
                return Schema.TYPE_STRING;
            case "PAYPLANCODE":
                return Schema.TYPE_STRING;
            case "PAYPLANNAME":
                return Schema.TYPE_STRING;
            case "PAYNEEDTOACC":
                return Schema.TYPE_STRING;
            case "RISKACCPAYNAME":
                return Schema.TYPE_STRING;
            case "ASCRIPTION":
                return Schema.TYPE_STRING;
            case "INVESTMINRATE":
                return Schema.TYPE_DOUBLE;
            case "INVESTMAXRATE":
                return Schema.TYPE_DOUBLE;
            case "INVESTRATE":
                return Schema.TYPE_DOUBLE;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_STRING;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_DOUBLE;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_STRING;
            case 6:
                return Schema.TYPE_STRING;
            case 7:
                return Schema.TYPE_STRING;
            case 8:
                return Schema.TYPE_STRING;
            case 9:
                return Schema.TYPE_STRING;
            case 10:
                return Schema.TYPE_STRING;
            case 11:
                return Schema.TYPE_STRING;
            case 12:
                return Schema.TYPE_STRING;
            case 13:
                return Schema.TYPE_STRING;
            case 14:
                return Schema.TYPE_DOUBLE;
            case 15:
                return Schema.TYPE_DOUBLE;
            case 16:
                return Schema.TYPE_DOUBLE;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("RiskCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RiskCode));
        }
        if (FCode.equalsIgnoreCase("RiskVer")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RiskVer));
        }
        if (FCode.equalsIgnoreCase("InsuAccNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InsuAccNo));
        }
        if (FCode.equalsIgnoreCase("DefaultRate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DefaultRate));
        }
        if (FCode.equalsIgnoreCase("NeedInput")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(NeedInput));
        }
        if (FCode.equalsIgnoreCase("AccCreatePos")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AccCreatePos));
        }
        if (FCode.equalsIgnoreCase("CalCodeMoney")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CalCodeMoney));
        }
        if (FCode.equalsIgnoreCase("CalCodeUnit")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CalCodeUnit));
        }
        if (FCode.equalsIgnoreCase("CalFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CalFlag));
        }
        if (FCode.equalsIgnoreCase("PayPlanCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PayPlanCode));
        }
        if (FCode.equalsIgnoreCase("PayPlanName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PayPlanName));
        }
        if (FCode.equalsIgnoreCase("PayNeedToAcc")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PayNeedToAcc));
        }
        if (FCode.equalsIgnoreCase("RiskAccPayName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RiskAccPayName));
        }
        if (FCode.equalsIgnoreCase("Ascription")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Ascription));
        }
        if (FCode.equalsIgnoreCase("InvestMinRate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InvestMinRate));
        }
        if (FCode.equalsIgnoreCase("InvestMaxRate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InvestMaxRate));
        }
        if (FCode.equalsIgnoreCase("InvestRate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InvestRate));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = String.valueOf(RiskCode);
                break;
            case 1:
                strFieldValue = String.valueOf(RiskVer);
                break;
            case 2:
                strFieldValue = String.valueOf(InsuAccNo);
                break;
            case 3:
                strFieldValue = String.valueOf(DefaultRate);
                break;
            case 4:
                strFieldValue = String.valueOf(NeedInput);
                break;
            case 5:
                strFieldValue = String.valueOf(AccCreatePos);
                break;
            case 6:
                strFieldValue = String.valueOf(CalCodeMoney);
                break;
            case 7:
                strFieldValue = String.valueOf(CalCodeUnit);
                break;
            case 8:
                strFieldValue = String.valueOf(CalFlag);
                break;
            case 9:
                strFieldValue = String.valueOf(PayPlanCode);
                break;
            case 10:
                strFieldValue = String.valueOf(PayPlanName);
                break;
            case 11:
                strFieldValue = String.valueOf(PayNeedToAcc);
                break;
            case 12:
                strFieldValue = String.valueOf(RiskAccPayName);
                break;
            case 13:
                strFieldValue = String.valueOf(Ascription);
                break;
            case 14:
                strFieldValue = String.valueOf(InvestMinRate);
                break;
            case 15:
                strFieldValue = String.valueOf(InvestMaxRate);
                break;
            case 16:
                strFieldValue = String.valueOf(InvestRate);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("RiskCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                RiskCode = FValue.trim();
            }
            else
                RiskCode = null;
        }
        if (FCode.equalsIgnoreCase("RiskVer")) {
            if( FValue != null && !FValue.equals(""))
            {
                RiskVer = FValue.trim();
            }
            else
                RiskVer = null;
        }
        if (FCode.equalsIgnoreCase("InsuAccNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                InsuAccNo = FValue.trim();
            }
            else
                InsuAccNo = null;
        }
        if (FCode.equalsIgnoreCase("DefaultRate")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                DefaultRate = d;
            }
        }
        if (FCode.equalsIgnoreCase("NeedInput")) {
            if( FValue != null && !FValue.equals(""))
            {
                NeedInput = FValue.trim();
            }
            else
                NeedInput = null;
        }
        if (FCode.equalsIgnoreCase("AccCreatePos")) {
            if( FValue != null && !FValue.equals(""))
            {
                AccCreatePos = FValue.trim();
            }
            else
                AccCreatePos = null;
        }
        if (FCode.equalsIgnoreCase("CalCodeMoney")) {
            if( FValue != null && !FValue.equals(""))
            {
                CalCodeMoney = FValue.trim();
            }
            else
                CalCodeMoney = null;
        }
        if (FCode.equalsIgnoreCase("CalCodeUnit")) {
            if( FValue != null && !FValue.equals(""))
            {
                CalCodeUnit = FValue.trim();
            }
            else
                CalCodeUnit = null;
        }
        if (FCode.equalsIgnoreCase("CalFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                CalFlag = FValue.trim();
            }
            else
                CalFlag = null;
        }
        if (FCode.equalsIgnoreCase("PayPlanCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                PayPlanCode = FValue.trim();
            }
            else
                PayPlanCode = null;
        }
        if (FCode.equalsIgnoreCase("PayPlanName")) {
            if( FValue != null && !FValue.equals(""))
            {
                PayPlanName = FValue.trim();
            }
            else
                PayPlanName = null;
        }
        if (FCode.equalsIgnoreCase("PayNeedToAcc")) {
            if( FValue != null && !FValue.equals(""))
            {
                PayNeedToAcc = FValue.trim();
            }
            else
                PayNeedToAcc = null;
        }
        if (FCode.equalsIgnoreCase("RiskAccPayName")) {
            if( FValue != null && !FValue.equals(""))
            {
                RiskAccPayName = FValue.trim();
            }
            else
                RiskAccPayName = null;
        }
        if (FCode.equalsIgnoreCase("Ascription")) {
            if( FValue != null && !FValue.equals(""))
            {
                Ascription = FValue.trim();
            }
            else
                Ascription = null;
        }
        if (FCode.equalsIgnoreCase("InvestMinRate")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                InvestMinRate = d;
            }
        }
        if (FCode.equalsIgnoreCase("InvestMaxRate")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                InvestMaxRate = d;
            }
        }
        if (FCode.equalsIgnoreCase("InvestRate")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                InvestRate = d;
            }
        }
        return true;
    }


    public String toString() {
    return "LMRiskAccPayPojo [" +
            "RiskCode="+RiskCode +
            ", RiskVer="+RiskVer +
            ", InsuAccNo="+InsuAccNo +
            ", DefaultRate="+DefaultRate +
            ", NeedInput="+NeedInput +
            ", AccCreatePos="+AccCreatePos +
            ", CalCodeMoney="+CalCodeMoney +
            ", CalCodeUnit="+CalCodeUnit +
            ", CalFlag="+CalFlag +
            ", PayPlanCode="+PayPlanCode +
            ", PayPlanName="+PayPlanName +
            ", PayNeedToAcc="+PayNeedToAcc +
            ", RiskAccPayName="+RiskAccPayName +
            ", Ascription="+Ascription +
            ", InvestMinRate="+InvestMinRate +
            ", InvestMaxRate="+InvestMaxRate +
            ", InvestRate="+InvestRate +"]";
    }
}
