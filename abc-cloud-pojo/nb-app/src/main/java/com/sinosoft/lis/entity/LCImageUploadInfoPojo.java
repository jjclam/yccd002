/**
 * Copyright (c) 2019 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.entity;

import java.io.*;
import java.util.Date;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.StrTool;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * <p>ClassName: LCImageUploadInfoPojo </p>
 * <p>Copyright: Copyright (c) 2019</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2019-10-12
 */
public class LCImageUploadInfoPojo implements  Pojo,Serializable {
    // @Field
    /** 请求流水号 */
    private String TransNo; 
    /** 保单号 */
    private String ContNo; 
    /** 业务类型编码 */
    private String AppCode; 
    /** 图片数据类型 */
    private String DataType; 
    /** 上传路径 */
    private String UploadUrl; 
    /** 上传状态 */
    private String UploadState; 
    /** 结果描述 */
    private String ResultRemark; 
    /** 上传次数 */
    private String UploadCount; 
    /** 备用字段1 */
    private String StandByfFag1; 
    /** 备用字段2 */
    private String StandByfFag2; 
    /** 备用字段3 */
    private String StandByfFag3; 
    /** 备用字段4 */
    private String StandByfFag4; 
    /** 备用字段5 */
    private String StandByfFag5; 
    /** 入机日期 */
    private String  MakeDate;
    /** 入机时间 */
    private String MakeTime; 
    /** 修改日期 */
    private String  ModifyDate;
    /** 修改时间 */
    private String ModifyTime; 


    public static final int FIELDNUM = 17;    // 数据库表的字段个数

    private FDate fDate = new FDate();        // 处理日期
    public String getTransNo() {
        return TransNo;
    }
    public void setTransNo(String aTransNo) {
        TransNo = aTransNo;
    }
    public String getContNo() {
        return ContNo;
    }
    public void setContNo(String aContNo) {
        ContNo = aContNo;
    }
    public String getAppCode() {
        return AppCode;
    }
    public void setAppCode(String aAppCode) {
        AppCode = aAppCode;
    }
    public String getDataType() {
        return DataType;
    }
    public void setDataType(String aDataType) {
        DataType = aDataType;
    }
    public String getUploadUrl() {
        return UploadUrl;
    }
    public void setUploadUrl(String aUploadUrl) {
        UploadUrl = aUploadUrl;
    }
    public String getUploadState() {
        return UploadState;
    }
    public void setUploadState(String aUploadState) {
        UploadState = aUploadState;
    }
    public String getResultRemark() {
        return ResultRemark;
    }
    public void setResultRemark(String aResultRemark) {
        ResultRemark = aResultRemark;
    }
    public String getUploadCount() {
        return UploadCount;
    }
    public void setUploadCount(String aUploadCount) {
        UploadCount = aUploadCount;
    }
    public String getStandByfFag1() {
        return StandByfFag1;
    }
    public void setStandByfFag1(String aStandByfFag1) {
        StandByfFag1 = aStandByfFag1;
    }
    public String getStandByfFag2() {
        return StandByfFag2;
    }
    public void setStandByfFag2(String aStandByfFag2) {
        StandByfFag2 = aStandByfFag2;
    }
    public String getStandByfFag3() {
        return StandByfFag3;
    }
    public void setStandByfFag3(String aStandByfFag3) {
        StandByfFag3 = aStandByfFag3;
    }
    public String getStandByfFag4() {
        return StandByfFag4;
    }
    public void setStandByfFag4(String aStandByfFag4) {
        StandByfFag4 = aStandByfFag4;
    }
    public String getStandByfFag5() {
        return StandByfFag5;
    }
    public void setStandByfFag5(String aStandByfFag5) {
        StandByfFag5 = aStandByfFag5;
    }
    public String getMakeDate() {
        return MakeDate;
    }
    public void setMakeDate(String aMakeDate) {
        MakeDate = aMakeDate;
    }
    public String getMakeTime() {
        return MakeTime;
    }
    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }
    public String getModifyDate() {
        return ModifyDate;
    }
    public void setModifyDate(String aModifyDate) {
        ModifyDate = aModifyDate;
    }
    public String getModifyTime() {
        return ModifyTime;
    }
    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("TransNo") ) {
            return 0;
        }
        if( strFieldName.equals("ContNo") ) {
            return 1;
        }
        if( strFieldName.equals("AppCode") ) {
            return 2;
        }
        if( strFieldName.equals("DataType") ) {
            return 3;
        }
        if( strFieldName.equals("UploadUrl") ) {
            return 4;
        }
        if( strFieldName.equals("UploadState") ) {
            return 5;
        }
        if( strFieldName.equals("ResultRemark") ) {
            return 6;
        }
        if( strFieldName.equals("UploadCount") ) {
            return 7;
        }
        if( strFieldName.equals("StandByfFag1") ) {
            return 8;
        }
        if( strFieldName.equals("StandByfFag2") ) {
            return 9;
        }
        if( strFieldName.equals("StandByfFag3") ) {
            return 10;
        }
        if( strFieldName.equals("StandByfFag4") ) {
            return 11;
        }
        if( strFieldName.equals("StandByfFag5") ) {
            return 12;
        }
        if( strFieldName.equals("MakeDate") ) {
            return 13;
        }
        if( strFieldName.equals("MakeTime") ) {
            return 14;
        }
        if( strFieldName.equals("ModifyDate") ) {
            return 15;
        }
        if( strFieldName.equals("ModifyTime") ) {
            return 16;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "TransNo";
                break;
            case 1:
                strFieldName = "ContNo";
                break;
            case 2:
                strFieldName = "AppCode";
                break;
            case 3:
                strFieldName = "DataType";
                break;
            case 4:
                strFieldName = "UploadUrl";
                break;
            case 5:
                strFieldName = "UploadState";
                break;
            case 6:
                strFieldName = "ResultRemark";
                break;
            case 7:
                strFieldName = "UploadCount";
                break;
            case 8:
                strFieldName = "StandByfFag1";
                break;
            case 9:
                strFieldName = "StandByfFag2";
                break;
            case 10:
                strFieldName = "StandByfFag3";
                break;
            case 11:
                strFieldName = "StandByfFag4";
                break;
            case 12:
                strFieldName = "StandByfFag5";
                break;
            case 13:
                strFieldName = "MakeDate";
                break;
            case 14:
                strFieldName = "MakeTime";
                break;
            case 15:
                strFieldName = "ModifyDate";
                break;
            case 16:
                strFieldName = "ModifyTime";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "TRANSNO":
                return Schema.TYPE_STRING;
            case "CONTNO":
                return Schema.TYPE_STRING;
            case "APPCODE":
                return Schema.TYPE_STRING;
            case "DATATYPE":
                return Schema.TYPE_STRING;
            case "UPLOADURL":
                return Schema.TYPE_STRING;
            case "UPLOADSTATE":
                return Schema.TYPE_STRING;
            case "RESULTREMARK":
                return Schema.TYPE_STRING;
            case "UPLOADCOUNT":
                return Schema.TYPE_STRING;
            case "STANDBYFFAG1":
                return Schema.TYPE_STRING;
            case "STANDBYFFAG2":
                return Schema.TYPE_STRING;
            case "STANDBYFFAG3":
                return Schema.TYPE_STRING;
            case "STANDBYFFAG4":
                return Schema.TYPE_STRING;
            case "STANDBYFFAG5":
                return Schema.TYPE_STRING;
            case "MAKEDATE":
                return Schema.TYPE_STRING;
            case "MAKETIME":
                return Schema.TYPE_STRING;
            case "MODIFYDATE":
                return Schema.TYPE_STRING;
            case "MODIFYTIME":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_STRING;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_STRING;
            case 6:
                return Schema.TYPE_STRING;
            case 7:
                return Schema.TYPE_STRING;
            case 8:
                return Schema.TYPE_STRING;
            case 9:
                return Schema.TYPE_STRING;
            case 10:
                return Schema.TYPE_STRING;
            case 11:
                return Schema.TYPE_STRING;
            case 12:
                return Schema.TYPE_STRING;
            case 13:
                return Schema.TYPE_STRING;
            case 14:
                return Schema.TYPE_STRING;
            case 15:
                return Schema.TYPE_STRING;
            case 16:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("TransNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(TransNo));
        }
        if (FCode.equalsIgnoreCase("ContNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ContNo));
        }
        if (FCode.equalsIgnoreCase("AppCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AppCode));
        }
        if (FCode.equalsIgnoreCase("DataType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DataType));
        }
        if (FCode.equalsIgnoreCase("UploadUrl")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(UploadUrl));
        }
        if (FCode.equalsIgnoreCase("UploadState")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(UploadState));
        }
        if (FCode.equalsIgnoreCase("ResultRemark")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ResultRemark));
        }
        if (FCode.equalsIgnoreCase("UploadCount")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(UploadCount));
        }
        if (FCode.equalsIgnoreCase("StandByfFag1")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StandByfFag1));
        }
        if (FCode.equalsIgnoreCase("StandByfFag2")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StandByfFag2));
        }
        if (FCode.equalsIgnoreCase("StandByfFag3")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StandByfFag3));
        }
        if (FCode.equalsIgnoreCase("StandByfFag4")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StandByfFag4));
        }
        if (FCode.equalsIgnoreCase("StandByfFag5")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StandByfFag5));
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeDate));
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyDate));
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = String.valueOf(TransNo);
                break;
            case 1:
                strFieldValue = String.valueOf(ContNo);
                break;
            case 2:
                strFieldValue = String.valueOf(AppCode);
                break;
            case 3:
                strFieldValue = String.valueOf(DataType);
                break;
            case 4:
                strFieldValue = String.valueOf(UploadUrl);
                break;
            case 5:
                strFieldValue = String.valueOf(UploadState);
                break;
            case 6:
                strFieldValue = String.valueOf(ResultRemark);
                break;
            case 7:
                strFieldValue = String.valueOf(UploadCount);
                break;
            case 8:
                strFieldValue = String.valueOf(StandByfFag1);
                break;
            case 9:
                strFieldValue = String.valueOf(StandByfFag2);
                break;
            case 10:
                strFieldValue = String.valueOf(StandByfFag3);
                break;
            case 11:
                strFieldValue = String.valueOf(StandByfFag4);
                break;
            case 12:
                strFieldValue = String.valueOf(StandByfFag5);
                break;
            case 13:
                strFieldValue = String.valueOf(MakeDate);
                break;
            case 14:
                strFieldValue = String.valueOf(MakeTime);
                break;
            case 15:
                strFieldValue = String.valueOf(ModifyDate);
                break;
            case 16:
                strFieldValue = String.valueOf(ModifyTime);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("TransNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                TransNo = FValue.trim();
            }
            else
                TransNo = null;
        }
        if (FCode.equalsIgnoreCase("ContNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                ContNo = FValue.trim();
            }
            else
                ContNo = null;
        }
        if (FCode.equalsIgnoreCase("AppCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                AppCode = FValue.trim();
            }
            else
                AppCode = null;
        }
        if (FCode.equalsIgnoreCase("DataType")) {
            if( FValue != null && !FValue.equals(""))
            {
                DataType = FValue.trim();
            }
            else
                DataType = null;
        }
        if (FCode.equalsIgnoreCase("UploadUrl")) {
            if( FValue != null && !FValue.equals(""))
            {
                UploadUrl = FValue.trim();
            }
            else
                UploadUrl = null;
        }
        if (FCode.equalsIgnoreCase("UploadState")) {
            if( FValue != null && !FValue.equals(""))
            {
                UploadState = FValue.trim();
            }
            else
                UploadState = null;
        }
        if (FCode.equalsIgnoreCase("ResultRemark")) {
            if( FValue != null && !FValue.equals(""))
            {
                ResultRemark = FValue.trim();
            }
            else
                ResultRemark = null;
        }
        if (FCode.equalsIgnoreCase("UploadCount")) {
            if( FValue != null && !FValue.equals(""))
            {
                UploadCount = FValue.trim();
            }
            else
                UploadCount = null;
        }
        if (FCode.equalsIgnoreCase("StandByfFag1")) {
            if( FValue != null && !FValue.equals(""))
            {
                StandByfFag1 = FValue.trim();
            }
            else
                StandByfFag1 = null;
        }
        if (FCode.equalsIgnoreCase("StandByfFag2")) {
            if( FValue != null && !FValue.equals(""))
            {
                StandByfFag2 = FValue.trim();
            }
            else
                StandByfFag2 = null;
        }
        if (FCode.equalsIgnoreCase("StandByfFag3")) {
            if( FValue != null && !FValue.equals(""))
            {
                StandByfFag3 = FValue.trim();
            }
            else
                StandByfFag3 = null;
        }
        if (FCode.equalsIgnoreCase("StandByfFag4")) {
            if( FValue != null && !FValue.equals(""))
            {
                StandByfFag4 = FValue.trim();
            }
            else
                StandByfFag4 = null;
        }
        if (FCode.equalsIgnoreCase("StandByfFag5")) {
            if( FValue != null && !FValue.equals(""))
            {
                StandByfFag5 = FValue.trim();
            }
            else
                StandByfFag5 = null;
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeDate = FValue.trim();
            }
            else
                MakeDate = null;
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
                MakeTime = null;
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyDate = FValue.trim();
            }
            else
                ModifyDate = null;
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
                ModifyTime = null;
        }
        return true;
    }


    public String toString() {
    return "LCImageUploadInfoPojo [" +
            "TransNo="+TransNo +
            ", ContNo="+ContNo +
            ", AppCode="+AppCode +
            ", DataType="+DataType +
            ", UploadUrl="+UploadUrl +
            ", UploadState="+UploadState +
            ", ResultRemark="+ResultRemark +
            ", UploadCount="+UploadCount +
            ", StandByfFag1="+StandByfFag1 +
            ", StandByfFag2="+StandByfFag2 +
            ", StandByfFag3="+StandByfFag3 +
            ", StandByfFag4="+StandByfFag4 +
            ", StandByfFag5="+StandByfFag5 +
            ", MakeDate="+MakeDate +
            ", MakeTime="+MakeTime +
            ", ModifyDate="+ModifyDate +
            ", ModifyTime="+ModifyTime +"]";
    }
}
