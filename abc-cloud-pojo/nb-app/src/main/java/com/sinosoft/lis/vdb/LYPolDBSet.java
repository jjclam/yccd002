/**
 * Copyright (c) 2017 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.vdb;

import com.sinosoft.lis.vschema.LYPolSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.DBConnPool;
import com.sinosoft.utility.DBOper;
import com.sinosoft.utility.ExceptionUtils;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;

/**
 * <p>ClassName: LYPolDBSet </p>
 * <p>Description: DB层多记录数据库操作类文件 </p>
 * <p>Copyright: Copyright (c) 2017</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2017-10-21
 */
public class LYPolDBSet extends LYPolSet {
    // @Field
    private Connection con;
    private DBOper db;
    /**
    * flag = true: 传入Connection
    * flag = false: 不传入Connection
    **/
    private boolean mflag = false;

    // @Constructor
    public LYPolDBSet(Connection tConnection) {
        con = tConnection;
        db = new DBOper(con,"LYPol");
        mflag = true;
    }

    public LYPolDBSet() {
        db = new DBOper( "LYPol" );
    }
    // @Method
    public boolean deleteSQL() {
        if (db.deleteSQL(this)) {
            return true;
        } else {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LYPolDBSet";
            tError.functionName = "deleteSQL";
            tError.errorMessage = "操作失败!";
            this.mErrors .addOneError(tError);
            return false;
        }
    }

    /**
     * 删除操作
     * 删除条件：主键
     * @return boolean
     */
    public boolean delete() {
        PreparedStatement pstmt = null;

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try {
            int tCount = this.size();
            pstmt = con.prepareStatement("DELETE FROM LYPol WHERE  1=1  AND YPolID = ?");
            for (int i = 1; i <= tCount; i++) {
            pstmt.setLong(1, this.get(i).getYPolID());
                pstmt.addBatch();
            }
            pstmt.executeBatch();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            ex.printStackTrace();
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LYPolDBSet";
            tError.functionName = "delete()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            try {
                pstmt.close();
            } catch (Exception e){
                e.printStackTrace();
            }

            if (!mflag) {
                try {
                    con.close();
                } catch (Exception e){e.printStackTrace();}
            }

            return false;
        }

        if( !mflag ) {
            try {
                con.close();
            } catch (Exception e){e.printStackTrace();}
        }

        return true;
    }

    /**
     * 更新操作
     * 更新条件：主键
     * @return boolean
     */
    public boolean update() {
        PreparedStatement pstmt = null;

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try {
            int tCount = this.size();
            pstmt = con.prepareStatement("UPDATE LYPol SET  YPolID = ? , VerifyAppID = ? , ShardingID = ? , ContNo = ? , ApplyDate = ? , ApplyEndDate = ? , State = ? , RiskCode = ? , ManageCom = ? , AgentBankCode = ? , AgentCom = ? , BankAgent = ? , AgentName = ? , AgentPhone = ? , AgentCode = ? , AgentGroup = ? , SaleChnl = ? , Prem = ? , PayIntv = ? , PayYears = ? , StandbyFlag1 = ? , StandbyFlag2 = ? , StandbyFlag3 = ? , Operator = ? , MakeDate = ? , MakeTime = ? , ModifyDate = ? , ModifyTime = ? , Prem1 = ? , Prem2 = ? , Prem3 = ? , Amnt1 = ? , Amnt2 = ? , Amnt3 = ? , Amnt = ? , Mult = ? , ProdSetCode = ? , ProdSetMult = ? , PayEndYearFlag = ? , PayEndYear = ? , InsuYearFlag = ? , InsuYear = ? , GetYearFlag = ? , GetYear = ? WHERE  1=1  AND YPolID = ?");
            for (int i = 1; i <= tCount; i++) {
            pstmt.setLong(1, this.get(i).getYPolID());
            pstmt.setLong(2, this.get(i).getVerifyAppID());
            if(this.get(i).getShardingID() == null || this.get(i).getShardingID().equals("null")) {
            	pstmt.setString(3,null);
            } else {
            	pstmt.setString(3, this.get(i).getShardingID());
            }
            if(this.get(i).getContNo() == null || this.get(i).getContNo().equals("null")) {
            	pstmt.setString(4,null);
            } else {
            	pstmt.setString(4, this.get(i).getContNo());
            }
            if(this.get(i).getApplyDate() == null || this.get(i).getApplyDate().equals("null")) {
                pstmt.setDate(5,null);
            } else {
                pstmt.setDate(5, Date.valueOf(this.get(i).getApplyDate()));
            }
            if(this.get(i).getApplyEndDate() == null || this.get(i).getApplyEndDate().equals("null")) {
                pstmt.setDate(6,null);
            } else {
                pstmt.setDate(6, Date.valueOf(this.get(i).getApplyEndDate()));
            }
            if(this.get(i).getState() == null || this.get(i).getState().equals("null")) {
            	pstmt.setString(7,null);
            } else {
            	pstmt.setString(7, this.get(i).getState());
            }
            if(this.get(i).getRiskCode() == null || this.get(i).getRiskCode().equals("null")) {
            	pstmt.setString(8,null);
            } else {
            	pstmt.setString(8, this.get(i).getRiskCode());
            }
            if(this.get(i).getManageCom() == null || this.get(i).getManageCom().equals("null")) {
            	pstmt.setString(9,null);
            } else {
            	pstmt.setString(9, this.get(i).getManageCom());
            }
            if(this.get(i).getAgentBankCode() == null || this.get(i).getAgentBankCode().equals("null")) {
            	pstmt.setString(10,null);
            } else {
            	pstmt.setString(10, this.get(i).getAgentBankCode());
            }
            if(this.get(i).getAgentCom() == null || this.get(i).getAgentCom().equals("null")) {
            	pstmt.setString(11,null);
            } else {
            	pstmt.setString(11, this.get(i).getAgentCom());
            }
            if(this.get(i).getBankAgent() == null || this.get(i).getBankAgent().equals("null")) {
            	pstmt.setString(12,null);
            } else {
            	pstmt.setString(12, this.get(i).getBankAgent());
            }
            if(this.get(i).getAgentName() == null || this.get(i).getAgentName().equals("null")) {
            	pstmt.setString(13,null);
            } else {
            	pstmt.setString(13, this.get(i).getAgentName());
            }
            if(this.get(i).getAgentPhone() == null || this.get(i).getAgentPhone().equals("null")) {
            	pstmt.setString(14,null);
            } else {
            	pstmt.setString(14, this.get(i).getAgentPhone());
            }
            if(this.get(i).getAgentCode() == null || this.get(i).getAgentCode().equals("null")) {
            	pstmt.setString(15,null);
            } else {
            	pstmt.setString(15, this.get(i).getAgentCode());
            }
            if(this.get(i).getAgentGroup() == null || this.get(i).getAgentGroup().equals("null")) {
            	pstmt.setString(16,null);
            } else {
            	pstmt.setString(16, this.get(i).getAgentGroup());
            }
            if(this.get(i).getSaleChnl() == null || this.get(i).getSaleChnl().equals("null")) {
            	pstmt.setString(17,null);
            } else {
            	pstmt.setString(17, this.get(i).getSaleChnl());
            }
            pstmt.setDouble(18, this.get(i).getPrem());
            pstmt.setDouble(19, this.get(i).getPayIntv());
            pstmt.setDouble(20, this.get(i).getPayYears());
            if(this.get(i).getStandbyFlag1() == null || this.get(i).getStandbyFlag1().equals("null")) {
            	pstmt.setString(21,null);
            } else {
            	pstmt.setString(21, this.get(i).getStandbyFlag1());
            }
            if(this.get(i).getStandbyFlag2() == null || this.get(i).getStandbyFlag2().equals("null")) {
            	pstmt.setString(22,null);
            } else {
            	pstmt.setString(22, this.get(i).getStandbyFlag2());
            }
            if(this.get(i).getStandbyFlag3() == null || this.get(i).getStandbyFlag3().equals("null")) {
            	pstmt.setString(23,null);
            } else {
            	pstmt.setString(23, this.get(i).getStandbyFlag3());
            }
            if(this.get(i).getOperator() == null || this.get(i).getOperator().equals("null")) {
            	pstmt.setString(24,null);
            } else {
            	pstmt.setString(24, this.get(i).getOperator());
            }
            if(this.get(i).getMakeDate() == null || this.get(i).getMakeDate().equals("null")) {
                pstmt.setDate(25,null);
            } else {
                pstmt.setDate(25, Date.valueOf(this.get(i).getMakeDate()));
            }
            if(this.get(i).getMakeTime() == null || this.get(i).getMakeTime().equals("null")) {
            	pstmt.setString(26,null);
            } else {
            	pstmt.setString(26, this.get(i).getMakeTime());
            }
            if(this.get(i).getModifyDate() == null || this.get(i).getModifyDate().equals("null")) {
                pstmt.setDate(27,null);
            } else {
                pstmt.setDate(27, Date.valueOf(this.get(i).getModifyDate()));
            }
            if(this.get(i).getModifyTime() == null || this.get(i).getModifyTime().equals("null")) {
            	pstmt.setString(28,null);
            } else {
            	pstmt.setString(28, this.get(i).getModifyTime());
            }
            pstmt.setDouble(29, this.get(i).getPrem1());
            pstmt.setDouble(30, this.get(i).getPrem2());
            pstmt.setDouble(31, this.get(i).getPrem3());
            pstmt.setDouble(32, this.get(i).getAmnt1());
            pstmt.setDouble(33, this.get(i).getAmnt2());
            pstmt.setDouble(34, this.get(i).getAmnt3());
            pstmt.setDouble(35, this.get(i).getAmnt());
            pstmt.setDouble(36, this.get(i).getMult());
            if(this.get(i).getProdSetCode() == null || this.get(i).getProdSetCode().equals("null")) {
            	pstmt.setString(37,null);
            } else {
            	pstmt.setString(37, this.get(i).getProdSetCode());
            }
            pstmt.setDouble(38, this.get(i).getProdSetMult());
            if(this.get(i).getPayEndYearFlag() == null || this.get(i).getPayEndYearFlag().equals("null")) {
            	pstmt.setString(39,null);
            } else {
            	pstmt.setString(39, this.get(i).getPayEndYearFlag());
            }
            pstmt.setInt(40, this.get(i).getPayEndYear());
            if(this.get(i).getInsuYearFlag() == null || this.get(i).getInsuYearFlag().equals("null")) {
            	pstmt.setString(41,null);
            } else {
            	pstmt.setString(41, this.get(i).getInsuYearFlag());
            }
            pstmt.setInt(42, this.get(i).getInsuYear());
            if(this.get(i).getGetYearFlag() == null || this.get(i).getGetYearFlag().equals("null")) {
            	pstmt.setString(43,null);
            } else {
            	pstmt.setString(43, this.get(i).getGetYearFlag());
            }
            pstmt.setInt(44, this.get(i).getGetYear());
            // set where condition
            pstmt.setLong(45, this.get(i).getYPolID());
                pstmt.addBatch();
            }
            pstmt.executeBatch();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            ex.printStackTrace();
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LYPolDBSet";
            tError.functionName = "update()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            try {
                pstmt.close();
            } catch (Exception e){e.printStackTrace();}

            if( !mflag ) {
                try {
                    con.close();
                } catch (Exception e){e.printStackTrace();}
            }

            return false;
        }

        if( !mflag ) {
            try {
                con.close();
            } catch (Exception e){e.printStackTrace();}
        }

        return true;
    }

    /**
     * 新增操作
     * @return boolean
     */
    public boolean insert() {
        PreparedStatement pstmt = null;

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try
        {
            int tCount = this.size();
            pstmt = con.prepareStatement("INSERT INTO LYPol VALUES( ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ?)");
            for (int i = 1; i <= tCount; i++) {
            pstmt.setLong(1, this.get(i).getYPolID());
            pstmt.setLong(2, this.get(i).getVerifyAppID());
            if(this.get(i).getShardingID() == null || this.get(i).getShardingID().equals("null")) {
            	pstmt.setString(3,null);
            } else {
            	pstmt.setString(3, this.get(i).getShardingID());
            }
            if(this.get(i).getContNo() == null || this.get(i).getContNo().equals("null")) {
            	pstmt.setString(4,null);
            } else {
            	pstmt.setString(4, this.get(i).getContNo());
            }
            if(this.get(i).getApplyDate() == null || this.get(i).getApplyDate().equals("null")) {
                pstmt.setDate(5,null);
            } else {
                pstmt.setDate(5, Date.valueOf(this.get(i).getApplyDate()));
            }
            if(this.get(i).getApplyEndDate() == null || this.get(i).getApplyEndDate().equals("null")) {
                pstmt.setDate(6,null);
            } else {
                pstmt.setDate(6, Date.valueOf(this.get(i).getApplyEndDate()));
            }
            if(this.get(i).getState() == null || this.get(i).getState().equals("null")) {
            	pstmt.setString(7,null);
            } else {
            	pstmt.setString(7, this.get(i).getState());
            }
            if(this.get(i).getRiskCode() == null || this.get(i).getRiskCode().equals("null")) {
            	pstmt.setString(8,null);
            } else {
            	pstmt.setString(8, this.get(i).getRiskCode());
            }
            if(this.get(i).getManageCom() == null || this.get(i).getManageCom().equals("null")) {
            	pstmt.setString(9,null);
            } else {
            	pstmt.setString(9, this.get(i).getManageCom());
            }
            if(this.get(i).getAgentBankCode() == null || this.get(i).getAgentBankCode().equals("null")) {
            	pstmt.setString(10,null);
            } else {
            	pstmt.setString(10, this.get(i).getAgentBankCode());
            }
            if(this.get(i).getAgentCom() == null || this.get(i).getAgentCom().equals("null")) {
            	pstmt.setString(11,null);
            } else {
            	pstmt.setString(11, this.get(i).getAgentCom());
            }
            if(this.get(i).getBankAgent() == null || this.get(i).getBankAgent().equals("null")) {
            	pstmt.setString(12,null);
            } else {
            	pstmt.setString(12, this.get(i).getBankAgent());
            }
            if(this.get(i).getAgentName() == null || this.get(i).getAgentName().equals("null")) {
            	pstmt.setString(13,null);
            } else {
            	pstmt.setString(13, this.get(i).getAgentName());
            }
            if(this.get(i).getAgentPhone() == null || this.get(i).getAgentPhone().equals("null")) {
            	pstmt.setString(14,null);
            } else {
            	pstmt.setString(14, this.get(i).getAgentPhone());
            }
            if(this.get(i).getAgentCode() == null || this.get(i).getAgentCode().equals("null")) {
            	pstmt.setString(15,null);
            } else {
            	pstmt.setString(15, this.get(i).getAgentCode());
            }
            if(this.get(i).getAgentGroup() == null || this.get(i).getAgentGroup().equals("null")) {
            	pstmt.setString(16,null);
            } else {
            	pstmt.setString(16, this.get(i).getAgentGroup());
            }
            if(this.get(i).getSaleChnl() == null || this.get(i).getSaleChnl().equals("null")) {
            	pstmt.setString(17,null);
            } else {
            	pstmt.setString(17, this.get(i).getSaleChnl());
            }
            pstmt.setDouble(18, this.get(i).getPrem());
            pstmt.setDouble(19, this.get(i).getPayIntv());
            pstmt.setDouble(20, this.get(i).getPayYears());
            if(this.get(i).getStandbyFlag1() == null || this.get(i).getStandbyFlag1().equals("null")) {
            	pstmt.setString(21,null);
            } else {
            	pstmt.setString(21, this.get(i).getStandbyFlag1());
            }
            if(this.get(i).getStandbyFlag2() == null || this.get(i).getStandbyFlag2().equals("null")) {
            	pstmt.setString(22,null);
            } else {
            	pstmt.setString(22, this.get(i).getStandbyFlag2());
            }
            if(this.get(i).getStandbyFlag3() == null || this.get(i).getStandbyFlag3().equals("null")) {
            	pstmt.setString(23,null);
            } else {
            	pstmt.setString(23, this.get(i).getStandbyFlag3());
            }
            if(this.get(i).getOperator() == null || this.get(i).getOperator().equals("null")) {
            	pstmt.setString(24,null);
            } else {
            	pstmt.setString(24, this.get(i).getOperator());
            }
            if(this.get(i).getMakeDate() == null || this.get(i).getMakeDate().equals("null")) {
                pstmt.setDate(25,null);
            } else {
                pstmt.setDate(25, Date.valueOf(this.get(i).getMakeDate()));
            }
            if(this.get(i).getMakeTime() == null || this.get(i).getMakeTime().equals("null")) {
            	pstmt.setString(26,null);
            } else {
            	pstmt.setString(26, this.get(i).getMakeTime());
            }
            if(this.get(i).getModifyDate() == null || this.get(i).getModifyDate().equals("null")) {
                pstmt.setDate(27,null);
            } else {
                pstmt.setDate(27, Date.valueOf(this.get(i).getModifyDate()));
            }
            if(this.get(i).getModifyTime() == null || this.get(i).getModifyTime().equals("null")) {
            	pstmt.setString(28,null);
            } else {
            	pstmt.setString(28, this.get(i).getModifyTime());
            }
            pstmt.setDouble(29, this.get(i).getPrem1());
            pstmt.setDouble(30, this.get(i).getPrem2());
            pstmt.setDouble(31, this.get(i).getPrem3());
            pstmt.setDouble(32, this.get(i).getAmnt1());
            pstmt.setDouble(33, this.get(i).getAmnt2());
            pstmt.setDouble(34, this.get(i).getAmnt3());
            pstmt.setDouble(35, this.get(i).getAmnt());
            pstmt.setDouble(36, this.get(i).getMult());
            if(this.get(i).getProdSetCode() == null || this.get(i).getProdSetCode().equals("null")) {
            	pstmt.setString(37,null);
            } else {
            	pstmt.setString(37, this.get(i).getProdSetCode());
            }
            pstmt.setDouble(38, this.get(i).getProdSetMult());
            if(this.get(i).getPayEndYearFlag() == null || this.get(i).getPayEndYearFlag().equals("null")) {
            	pstmt.setString(39,null);
            } else {
            	pstmt.setString(39, this.get(i).getPayEndYearFlag());
            }
            pstmt.setInt(40, this.get(i).getPayEndYear());
            if(this.get(i).getInsuYearFlag() == null || this.get(i).getInsuYearFlag().equals("null")) {
            	pstmt.setString(41,null);
            } else {
            	pstmt.setString(41, this.get(i).getInsuYearFlag());
            }
            pstmt.setInt(42, this.get(i).getInsuYear());
            if(this.get(i).getGetYearFlag() == null || this.get(i).getGetYearFlag().equals("null")) {
            	pstmt.setString(43,null);
            } else {
            	pstmt.setString(43, this.get(i).getGetYearFlag());
            }
            pstmt.setInt(44, this.get(i).getGetYear());
                pstmt.addBatch();
            }
            pstmt.executeBatch();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            ex.printStackTrace();
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LYPolDBSet";
            tError.functionName = "insert()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            try {
                pstmt.close();
            } catch (Exception e){e.printStackTrace();}

            if( !mflag ) {
                try {
                    con.close();
                } catch (Exception e){e.printStackTrace();}
            }

            return false;
        }

        if( !mflag ) {
            try {
                con.close();
            } catch (Exception e){e.printStackTrace();}
        }

        return true;
    }
}
