/**
 * Copyright (c) 2018 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.entity;

import com.sinosoft.cloud.cache.annotation.RedisPrimaryHKey;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.Pojo;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;

import java.io.Serializable;

/**
 * <p>ClassName: T_CUST_MANAGERPojo </p>
 * <p>Copyright: Copyright (c) 2018</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2018-11-22
 */

public class T_CUST_MANAGERPojo implements Pojo,Serializable {
    // @Field
    /** 客户经理id */
    @RedisPrimaryHKey
    private long CUST_MANAGER_ID; 
    /** 客户经理编码 */
    private String CUST_MANAGER_CODE; 
    /** 客户经理级别 */
    private String CUST_MANAGER_LEVEL; 
    /** 有效状态 */
    private String ENABLE_STATUS; 
    /** 用户id */
    private long USER_ID; 
    /** 备注 */
    private String REMARK; 
    /** 插入操作员 */
    private String INSERT_OPER; 
    /** 插入委托人 */
    private String INSERT_CONSIGNOR; 
    /** 插入时间 */
    private String  INSERT_TIME;
    /** 更新操作员 */
    private String UPDATE_OPER; 
    /** 更新委托人 */
    private String UPDATE_CONSIGNOR; 
    /** 更新时间 */
    private String  UPDATE_TIME;
    /** 客户经理名称 */
    private String CUST_NAME; 
    /** Mngorg_id */
    private long MNGORG_ID; 


    public static final int FIELDNUM = 14;    // 数据库表的字段个数

    private FDate fDate = new FDate();        // 处理日期
    public long getCUST_MANAGER_ID() {
        return CUST_MANAGER_ID;
    }
    public void setCUST_MANAGER_ID(long aCUST_MANAGER_ID) {
        CUST_MANAGER_ID = aCUST_MANAGER_ID;
    }
    public void setCUST_MANAGER_ID(String aCUST_MANAGER_ID) {
        if (aCUST_MANAGER_ID != null && !aCUST_MANAGER_ID.equals("")) {
            CUST_MANAGER_ID = new Long(aCUST_MANAGER_ID).longValue();
        }
    }

    public String getCUST_MANAGER_CODE() {
        return CUST_MANAGER_CODE;
    }
    public void setCUST_MANAGER_CODE(String aCUST_MANAGER_CODE) {
        CUST_MANAGER_CODE = aCUST_MANAGER_CODE;
    }
    public String getCUST_MANAGER_LEVEL() {
        return CUST_MANAGER_LEVEL;
    }
    public void setCUST_MANAGER_LEVEL(String aCUST_MANAGER_LEVEL) {
        CUST_MANAGER_LEVEL = aCUST_MANAGER_LEVEL;
    }
    public String getENABLE_STATUS() {
        return ENABLE_STATUS;
    }
    public void setENABLE_STATUS(String aENABLE_STATUS) {
        ENABLE_STATUS = aENABLE_STATUS;
    }
    public long getUSER_ID() {
        return USER_ID;
    }
    public void setUSER_ID(long aUSER_ID) {
        USER_ID = aUSER_ID;
    }
    public void setUSER_ID(String aUSER_ID) {
        if (aUSER_ID != null && !aUSER_ID.equals("")) {
            USER_ID = new Long(aUSER_ID).longValue();
        }
    }

    public String getREMARK() {
        return REMARK;
    }
    public void setREMARK(String aREMARK) {
        REMARK = aREMARK;
    }
    public String getINSERT_OPER() {
        return INSERT_OPER;
    }
    public void setINSERT_OPER(String aINSERT_OPER) {
        INSERT_OPER = aINSERT_OPER;
    }
    public String getINSERT_CONSIGNOR() {
        return INSERT_CONSIGNOR;
    }
    public void setINSERT_CONSIGNOR(String aINSERT_CONSIGNOR) {
        INSERT_CONSIGNOR = aINSERT_CONSIGNOR;
    }
    public String getINSERT_TIME() {
        return INSERT_TIME;
    }
    public void setINSERT_TIME(String aINSERT_TIME) {
        INSERT_TIME = aINSERT_TIME;
    }
    public String getUPDATE_OPER() {
        return UPDATE_OPER;
    }
    public void setUPDATE_OPER(String aUPDATE_OPER) {
        UPDATE_OPER = aUPDATE_OPER;
    }
    public String getUPDATE_CONSIGNOR() {
        return UPDATE_CONSIGNOR;
    }
    public void setUPDATE_CONSIGNOR(String aUPDATE_CONSIGNOR) {
        UPDATE_CONSIGNOR = aUPDATE_CONSIGNOR;
    }
    public String getUPDATE_TIME() {
        return UPDATE_TIME;
    }
    public void setUPDATE_TIME(String aUPDATE_TIME) {
        UPDATE_TIME = aUPDATE_TIME;
    }
    public String getCUST_NAME() {
        return CUST_NAME;
    }
    public void setCUST_NAME(String aCUST_NAME) {
        CUST_NAME = aCUST_NAME;
    }
    public long getMNGORG_ID() {
        return MNGORG_ID;
    }
    public void setMNGORG_ID(long aMNGORG_ID) {
        MNGORG_ID = aMNGORG_ID;
    }
    public void setMNGORG_ID(String aMNGORG_ID) {
        if (aMNGORG_ID != null && !aMNGORG_ID.equals("")) {
            MNGORG_ID = new Long(aMNGORG_ID).longValue();
        }
    }


    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("CUST_MANAGER_ID") ) {
            return 0;
        }
        if( strFieldName.equals("CUST_MANAGER_CODE") ) {
            return 1;
        }
        if( strFieldName.equals("CUST_MANAGER_LEVEL") ) {
            return 2;
        }
        if( strFieldName.equals("ENABLE_STATUS") ) {
            return 3;
        }
        if( strFieldName.equals("USER_ID") ) {
            return 4;
        }
        if( strFieldName.equals("REMARK") ) {
            return 5;
        }
        if( strFieldName.equals("INSERT_OPER") ) {
            return 6;
        }
        if( strFieldName.equals("INSERT_CONSIGNOR") ) {
            return 7;
        }
        if( strFieldName.equals("INSERT_TIME") ) {
            return 8;
        }
        if( strFieldName.equals("UPDATE_OPER") ) {
            return 9;
        }
        if( strFieldName.equals("UPDATE_CONSIGNOR") ) {
            return 10;
        }
        if( strFieldName.equals("UPDATE_TIME") ) {
            return 11;
        }
        if( strFieldName.equals("CUST_NAME") ) {
            return 12;
        }
        if( strFieldName.equals("MNGORG_ID") ) {
            return 13;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "CUST_MANAGER_ID";
                break;
            case 1:
                strFieldName = "CUST_MANAGER_CODE";
                break;
            case 2:
                strFieldName = "CUST_MANAGER_LEVEL";
                break;
            case 3:
                strFieldName = "ENABLE_STATUS";
                break;
            case 4:
                strFieldName = "USER_ID";
                break;
            case 5:
                strFieldName = "REMARK";
                break;
            case 6:
                strFieldName = "INSERT_OPER";
                break;
            case 7:
                strFieldName = "INSERT_CONSIGNOR";
                break;
            case 8:
                strFieldName = "INSERT_TIME";
                break;
            case 9:
                strFieldName = "UPDATE_OPER";
                break;
            case 10:
                strFieldName = "UPDATE_CONSIGNOR";
                break;
            case 11:
                strFieldName = "UPDATE_TIME";
                break;
            case 12:
                strFieldName = "CUST_NAME";
                break;
            case 13:
                strFieldName = "MNGORG_ID";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "CUST_MANAGER_ID":
                return Schema.TYPE_LONG;
            case "CUST_MANAGER_CODE":
                return Schema.TYPE_STRING;
            case "CUST_MANAGER_LEVEL":
                return Schema.TYPE_STRING;
            case "ENABLE_STATUS":
                return Schema.TYPE_STRING;
            case "USER_ID":
                return Schema.TYPE_LONG;
            case "REMARK":
                return Schema.TYPE_STRING;
            case "INSERT_OPER":
                return Schema.TYPE_STRING;
            case "INSERT_CONSIGNOR":
                return Schema.TYPE_STRING;
            case "INSERT_TIME":
                return Schema.TYPE_STRING;
            case "UPDATE_OPER":
                return Schema.TYPE_STRING;
            case "UPDATE_CONSIGNOR":
                return Schema.TYPE_STRING;
            case "UPDATE_TIME":
                return Schema.TYPE_STRING;
            case "CUST_NAME":
                return Schema.TYPE_STRING;
            case "MNGORG_ID":
                return Schema.TYPE_LONG;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_LONG;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_LONG;
            case 5:
                return Schema.TYPE_STRING;
            case 6:
                return Schema.TYPE_STRING;
            case 7:
                return Schema.TYPE_STRING;
            case 8:
                return Schema.TYPE_STRING;
            case 9:
                return Schema.TYPE_STRING;
            case 10:
                return Schema.TYPE_STRING;
            case 11:
                return Schema.TYPE_STRING;
            case 12:
                return Schema.TYPE_STRING;
            case 13:
                return Schema.TYPE_LONG;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("CUST_MANAGER_ID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CUST_MANAGER_ID));
        }
        if (FCode.equalsIgnoreCase("CUST_MANAGER_CODE")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CUST_MANAGER_CODE));
        }
        if (FCode.equalsIgnoreCase("CUST_MANAGER_LEVEL")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CUST_MANAGER_LEVEL));
        }
        if (FCode.equalsIgnoreCase("ENABLE_STATUS")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ENABLE_STATUS));
        }
        if (FCode.equalsIgnoreCase("USER_ID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(USER_ID));
        }
        if (FCode.equalsIgnoreCase("REMARK")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(REMARK));
        }
        if (FCode.equalsIgnoreCase("INSERT_OPER")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(INSERT_OPER));
        }
        if (FCode.equalsIgnoreCase("INSERT_CONSIGNOR")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(INSERT_CONSIGNOR));
        }
        if (FCode.equalsIgnoreCase("INSERT_TIME")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(INSERT_TIME));
        }
        if (FCode.equalsIgnoreCase("UPDATE_OPER")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(UPDATE_OPER));
        }
        if (FCode.equalsIgnoreCase("UPDATE_CONSIGNOR")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(UPDATE_CONSIGNOR));
        }
        if (FCode.equalsIgnoreCase("UPDATE_TIME")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(UPDATE_TIME));
        }
        if (FCode.equalsIgnoreCase("CUST_NAME")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CUST_NAME));
        }
        if (FCode.equalsIgnoreCase("MNGORG_ID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MNGORG_ID));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = String.valueOf(CUST_MANAGER_ID);
                break;
            case 1:
                strFieldValue = String.valueOf(CUST_MANAGER_CODE);
                break;
            case 2:
                strFieldValue = String.valueOf(CUST_MANAGER_LEVEL);
                break;
            case 3:
                strFieldValue = String.valueOf(ENABLE_STATUS);
                break;
            case 4:
                strFieldValue = String.valueOf(USER_ID);
                break;
            case 5:
                strFieldValue = String.valueOf(REMARK);
                break;
            case 6:
                strFieldValue = String.valueOf(INSERT_OPER);
                break;
            case 7:
                strFieldValue = String.valueOf(INSERT_CONSIGNOR);
                break;
            case 8:
                strFieldValue = String.valueOf(INSERT_TIME);
                break;
            case 9:
                strFieldValue = String.valueOf(UPDATE_OPER);
                break;
            case 10:
                strFieldValue = String.valueOf(UPDATE_CONSIGNOR);
                break;
            case 11:
                strFieldValue = String.valueOf(UPDATE_TIME);
                break;
            case 12:
                strFieldValue = String.valueOf(CUST_NAME);
                break;
            case 13:
                strFieldValue = String.valueOf(MNGORG_ID);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("CUST_MANAGER_ID")) {
            if( FValue != null && !FValue.equals("")) {
                CUST_MANAGER_ID = new Long(FValue).longValue();
            }
        }
        if (FCode.equalsIgnoreCase("CUST_MANAGER_CODE")) {
            if( FValue != null && !FValue.equals(""))
            {
                CUST_MANAGER_CODE = FValue.trim();
            }
            else
                CUST_MANAGER_CODE = null;
        }
        if (FCode.equalsIgnoreCase("CUST_MANAGER_LEVEL")) {
            if( FValue != null && !FValue.equals(""))
            {
                CUST_MANAGER_LEVEL = FValue.trim();
            }
            else
                CUST_MANAGER_LEVEL = null;
        }
        if (FCode.equalsIgnoreCase("ENABLE_STATUS")) {
            if( FValue != null && !FValue.equals(""))
            {
                ENABLE_STATUS = FValue.trim();
            }
            else
                ENABLE_STATUS = null;
        }
        if (FCode.equalsIgnoreCase("USER_ID")) {
            if( FValue != null && !FValue.equals("")) {
                USER_ID = new Long(FValue).longValue();
            }
        }
        if (FCode.equalsIgnoreCase("REMARK")) {
            if( FValue != null && !FValue.equals(""))
            {
                REMARK = FValue.trim();
            }
            else
                REMARK = null;
        }
        if (FCode.equalsIgnoreCase("INSERT_OPER")) {
            if( FValue != null && !FValue.equals(""))
            {
                INSERT_OPER = FValue.trim();
            }
            else
                INSERT_OPER = null;
        }
        if (FCode.equalsIgnoreCase("INSERT_CONSIGNOR")) {
            if( FValue != null && !FValue.equals(""))
            {
                INSERT_CONSIGNOR = FValue.trim();
            }
            else
                INSERT_CONSIGNOR = null;
        }
        if (FCode.equalsIgnoreCase("INSERT_TIME")) {
            if( FValue != null && !FValue.equals(""))
            {
                INSERT_TIME = FValue.trim();
            }
            else
                INSERT_TIME = null;
        }
        if (FCode.equalsIgnoreCase("UPDATE_OPER")) {
            if( FValue != null && !FValue.equals(""))
            {
                UPDATE_OPER = FValue.trim();
            }
            else
                UPDATE_OPER = null;
        }
        if (FCode.equalsIgnoreCase("UPDATE_CONSIGNOR")) {
            if( FValue != null && !FValue.equals(""))
            {
                UPDATE_CONSIGNOR = FValue.trim();
            }
            else
                UPDATE_CONSIGNOR = null;
        }
        if (FCode.equalsIgnoreCase("UPDATE_TIME")) {
            if( FValue != null && !FValue.equals(""))
            {
                UPDATE_TIME = FValue.trim();
            }
            else
                UPDATE_TIME = null;
        }
        if (FCode.equalsIgnoreCase("CUST_NAME")) {
            if( FValue != null && !FValue.equals(""))
            {
                CUST_NAME = FValue.trim();
            }
            else
                CUST_NAME = null;
        }
        if (FCode.equalsIgnoreCase("MNGORG_ID")) {
            if( FValue != null && !FValue.equals("")) {
                MNGORG_ID = new Long(FValue).longValue();
            }
        }
        return true;
    }


    public String toString() {
    return "T_CUST_MANAGERPojo [" +
            "CUST_MANAGER_ID="+CUST_MANAGER_ID +
            ", CUST_MANAGER_CODE="+CUST_MANAGER_CODE +
            ", CUST_MANAGER_LEVEL="+CUST_MANAGER_LEVEL +
            ", ENABLE_STATUS="+ENABLE_STATUS +
            ", USER_ID="+USER_ID +
            ", REMARK="+REMARK +
            ", INSERT_OPER="+INSERT_OPER +
            ", INSERT_CONSIGNOR="+INSERT_CONSIGNOR +
            ", INSERT_TIME="+INSERT_TIME +
            ", UPDATE_OPER="+UPDATE_OPER +
            ", UPDATE_CONSIGNOR="+UPDATE_CONSIGNOR +
            ", UPDATE_TIME="+UPDATE_TIME +
            ", CUST_NAME="+CUST_NAME +
            ", MNGORG_ID="+MNGORG_ID +"]";
    }
}
