/**
 * Copyright (c) 2017 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.entity;

import com.sinosoft.utility.Pojo;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;

import java.io.Serializable;

/**
 * <p>ClassName: LDMaxNoPojo </p>
 * <p>Copyright: Copyright (c) 2017</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2017-10-17
 */
public class LDMaxNoPojo implements Pojo,Serializable {
    // @Field
    /** 号码类型 */
    private String NoType; 
    /** 号码限制条件 */
    private String NoLimit; 
    /** 当前最大值 */
    private int MaxNo; 


    public static final int FIELDNUM = 3;    // 数据库表的字段个数
    public String getNoType() {
        return NoType;
    }
    public void setNoType(String aNoType) {
        NoType = aNoType;
    }
    public String getNoLimit() {
        return NoLimit;
    }
    public void setNoLimit(String aNoLimit) {
        NoLimit = aNoLimit;
    }
    public int getMaxNo() {
        return MaxNo;
    }
    public void setMaxNo(int aMaxNo) {
        MaxNo = aMaxNo;
    }
    public void setMaxNo(String aMaxNo) {
        if (aMaxNo != null && !aMaxNo.equals("")) {
            Integer tInteger = new Integer(aMaxNo);
            int i = tInteger.intValue();
            MaxNo = i;
        }
    }


    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("NoType") ) {
            return 0;
        }
        if( strFieldName.equals("NoLimit") ) {
            return 1;
        }
        if( strFieldName.equals("MaxNo") ) {
            return 2;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "NoType";
                break;
            case 1:
                strFieldName = "NoLimit";
                break;
            case 2:
                strFieldName = "MaxNo";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "NOTYPE":
                return Schema.TYPE_STRING;
            case "NOLIMIT":
                return Schema.TYPE_STRING;
            case "MAXNO":
                return Schema.TYPE_INT;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_STRING;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_INT;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("NoType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(NoType));
        }
        if (FCode.equalsIgnoreCase("NoLimit")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(NoLimit));
        }
        if (FCode.equalsIgnoreCase("MaxNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MaxNo));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = String.valueOf(NoType);
                break;
            case 1:
                strFieldValue = String.valueOf(NoLimit);
                break;
            case 2:
                strFieldValue = String.valueOf(MaxNo);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("NoType")) {
            if( FValue != null && !FValue.equals(""))
            {
                NoType = FValue.trim();
            }
            else
                NoType = null;
        }
        if (FCode.equalsIgnoreCase("NoLimit")) {
            if( FValue != null && !FValue.equals(""))
            {
                NoLimit = FValue.trim();
            }
            else
                NoLimit = null;
        }
        if (FCode.equalsIgnoreCase("MaxNo")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                MaxNo = i;
            }
        }
        return true;
    }


    public String toString() {
    return "LDMaxNoPojo [" +
            "NoType="+NoType +
            ", NoLimit="+NoLimit +
            ", MaxNo="+MaxNo +"]";
    }
}
