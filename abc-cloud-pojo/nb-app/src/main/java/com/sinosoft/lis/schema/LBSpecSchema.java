/**
 * Copyright (c) 2017 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import com.sinosoft.lis.db.LBSpecDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.*;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

/**
 * <p>ClassName: LBSpecSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2017</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2017-10-21
 */
public class LBSpecSchema implements Schema, Cloneable {
    // @Field
    /** Id */
    private long SpecID;
    /** Fk_lccont */
    private long ContID;
    /** Shardingid */
    private String ShardingID;
    /** 批单号 */
    private String EdorNo;
    /** 集体合同号码 */
    private String GrpContNo;
    /** 合同号码 */
    private String ContNo;
    /** 保单号码 */
    private String PolNo;
    /** 投保单号码 */
    private String ProposalNo;
    /** 打印流水号 */
    private String PrtSeq;
    /** 流水号 */
    private String SerialNo;
    /** 批单号码 */
    private String EndorsementNo;
    /** 特约类型 */
    private String SpecType;
    /** 特约编码 */
    private String SpecCode;
    /** 特约内容 */
    private String SpecContent;
    /** 打印标记 */
    private String PrtFlag;
    /** 备份类型 */
    private String BackupType;
    /** 操作员 */
    private String Operator;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 最后一次修改日期 */
    private Date ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime;

    public static final int FIELDNUM = 21;    // 数据库表的字段个数

    private static String[] PK;                // 主键

    private FDate fDate = new FDate();        // 处理日期

    public CErrors mErrors;            // 错误信息

    // @Constructor
    public LBSpecSchema() {
        mErrors = new CErrors();

        String[] pk = new String[1];
        pk[0] = "SpecID";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException {
        LBSpecSchema cloned = (LBSpecSchema)super.clone();
        cloned.fDate = (FDate) fDate.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK() {
        return PK;
    }

    public long getSpecID() {
        return SpecID;
    }
    public void setSpecID(long aSpecID) {
        SpecID = aSpecID;
    }
    public void setSpecID(String aSpecID) {
        if (aSpecID != null && !aSpecID.equals("")) {
            SpecID = new Long(aSpecID).longValue();
        }
    }

    public long getContID() {
        return ContID;
    }
    public void setContID(long aContID) {
        ContID = aContID;
    }
    public void setContID(String aContID) {
        if (aContID != null && !aContID.equals("")) {
            ContID = new Long(aContID).longValue();
        }
    }

    public String getShardingID() {
        return ShardingID;
    }
    public void setShardingID(String aShardingID) {
        ShardingID = aShardingID;
    }
    public String getEdorNo() {
        return EdorNo;
    }
    public void setEdorNo(String aEdorNo) {
        EdorNo = aEdorNo;
    }
    public String getGrpContNo() {
        return GrpContNo;
    }
    public void setGrpContNo(String aGrpContNo) {
        GrpContNo = aGrpContNo;
    }
    public String getContNo() {
        return ContNo;
    }
    public void setContNo(String aContNo) {
        ContNo = aContNo;
    }
    public String getPolNo() {
        return PolNo;
    }
    public void setPolNo(String aPolNo) {
        PolNo = aPolNo;
    }
    public String getProposalNo() {
        return ProposalNo;
    }
    public void setProposalNo(String aProposalNo) {
        ProposalNo = aProposalNo;
    }
    public String getPrtSeq() {
        return PrtSeq;
    }
    public void setPrtSeq(String aPrtSeq) {
        PrtSeq = aPrtSeq;
    }
    public String getSerialNo() {
        return SerialNo;
    }
    public void setSerialNo(String aSerialNo) {
        SerialNo = aSerialNo;
    }
    public String getEndorsementNo() {
        return EndorsementNo;
    }
    public void setEndorsementNo(String aEndorsementNo) {
        EndorsementNo = aEndorsementNo;
    }
    public String getSpecType() {
        return SpecType;
    }
    public void setSpecType(String aSpecType) {
        SpecType = aSpecType;
    }
    public String getSpecCode() {
        return SpecCode;
    }
    public void setSpecCode(String aSpecCode) {
        SpecCode = aSpecCode;
    }
    public String getSpecContent() {
        return SpecContent;
    }
    public void setSpecContent(String aSpecContent) {
        SpecContent = aSpecContent;
    }
    public String getPrtFlag() {
        return PrtFlag;
    }
    public void setPrtFlag(String aPrtFlag) {
        PrtFlag = aPrtFlag;
    }
    public String getBackupType() {
        return BackupType;
    }
    public void setBackupType(String aBackupType) {
        BackupType = aBackupType;
    }
    public String getOperator() {
        return Operator;
    }
    public void setOperator(String aOperator) {
        Operator = aOperator;
    }
    public String getMakeDate() {
        if(MakeDate != null) {
            return fDate.getString(MakeDate);
        } else {
            return null;
        }
    }
    public void setMakeDate(Date aMakeDate) {
        MakeDate = aMakeDate;
    }
    public void setMakeDate(String aMakeDate) {
        if (aMakeDate != null && !aMakeDate.equals("")) {
            MakeDate = fDate.getDate(aMakeDate);
        } else
            MakeDate = null;
    }

    public String getMakeTime() {
        return MakeTime;
    }
    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }
    public String getModifyDate() {
        if(ModifyDate != null) {
            return fDate.getString(ModifyDate);
        } else {
            return null;
        }
    }
    public void setModifyDate(Date aModifyDate) {
        ModifyDate = aModifyDate;
    }
    public void setModifyDate(String aModifyDate) {
        if (aModifyDate != null && !aModifyDate.equals("")) {
            ModifyDate = fDate.getDate(aModifyDate);
        } else
            ModifyDate = null;
    }

    public String getModifyTime() {
        return ModifyTime;
    }
    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }

    /**
    * 使用另外一个 LBSpecSchema 对象给 Schema 赋值
    * @param: aLBSpecSchema LBSpecSchema
    **/
    public void setSchema(LBSpecSchema aLBSpecSchema) {
        this.SpecID = aLBSpecSchema.getSpecID();
        this.ContID = aLBSpecSchema.getContID();
        this.ShardingID = aLBSpecSchema.getShardingID();
        this.EdorNo = aLBSpecSchema.getEdorNo();
        this.GrpContNo = aLBSpecSchema.getGrpContNo();
        this.ContNo = aLBSpecSchema.getContNo();
        this.PolNo = aLBSpecSchema.getPolNo();
        this.ProposalNo = aLBSpecSchema.getProposalNo();
        this.PrtSeq = aLBSpecSchema.getPrtSeq();
        this.SerialNo = aLBSpecSchema.getSerialNo();
        this.EndorsementNo = aLBSpecSchema.getEndorsementNo();
        this.SpecType = aLBSpecSchema.getSpecType();
        this.SpecCode = aLBSpecSchema.getSpecCode();
        this.SpecContent = aLBSpecSchema.getSpecContent();
        this.PrtFlag = aLBSpecSchema.getPrtFlag();
        this.BackupType = aLBSpecSchema.getBackupType();
        this.Operator = aLBSpecSchema.getOperator();
        this.MakeDate = fDate.getDate( aLBSpecSchema.getMakeDate());
        this.MakeTime = aLBSpecSchema.getMakeTime();
        this.ModifyDate = fDate.getDate( aLBSpecSchema.getModifyDate());
        this.ModifyTime = aLBSpecSchema.getModifyTime();
    }

    /**
    * 使用 ResultSet 中的第 i 行给 Schema 赋值
    * @param: rs ResultSet
    * @param: i int
    * @return: boolean
    **/
    public boolean setSchema(ResultSet rs,int i) {
        try {
            //rs.absolute(i);        // 非滚动游标
            this.SpecID = rs.getLong("SpecID");
            this.ContID = rs.getLong("ContID");
            if( rs.getString("ShardingID") == null )
                this.ShardingID = null;
            else
                this.ShardingID = rs.getString("ShardingID").trim();

            if( rs.getString("EdorNo") == null )
                this.EdorNo = null;
            else
                this.EdorNo = rs.getString("EdorNo").trim();

            if( rs.getString("GrpContNo") == null )
                this.GrpContNo = null;
            else
                this.GrpContNo = rs.getString("GrpContNo").trim();

            if( rs.getString("ContNo") == null )
                this.ContNo = null;
            else
                this.ContNo = rs.getString("ContNo").trim();

            if( rs.getString("PolNo") == null )
                this.PolNo = null;
            else
                this.PolNo = rs.getString("PolNo").trim();

            if( rs.getString("ProposalNo") == null )
                this.ProposalNo = null;
            else
                this.ProposalNo = rs.getString("ProposalNo").trim();

            if( rs.getString("PrtSeq") == null )
                this.PrtSeq = null;
            else
                this.PrtSeq = rs.getString("PrtSeq").trim();

            if( rs.getString("SerialNo") == null )
                this.SerialNo = null;
            else
                this.SerialNo = rs.getString("SerialNo").trim();

            if( rs.getString("EndorsementNo") == null )
                this.EndorsementNo = null;
            else
                this.EndorsementNo = rs.getString("EndorsementNo").trim();

            if( rs.getString("SpecType") == null )
                this.SpecType = null;
            else
                this.SpecType = rs.getString("SpecType").trim();

            if( rs.getString("SpecCode") == null )
                this.SpecCode = null;
            else
                this.SpecCode = rs.getString("SpecCode").trim();

            if( rs.getString("SpecContent") == null )
                this.SpecContent = null;
            else
                this.SpecContent = rs.getString("SpecContent").trim();

            if( rs.getString("PrtFlag") == null )
                this.PrtFlag = null;
            else
                this.PrtFlag = rs.getString("PrtFlag").trim();

            if( rs.getString("BackupType") == null )
                this.BackupType = null;
            else
                this.BackupType = rs.getString("BackupType").trim();

            if( rs.getString("Operator") == null )
                this.Operator = null;
            else
                this.Operator = rs.getString("Operator").trim();

            this.MakeDate = rs.getDate("MakeDate");
            if( rs.getString("MakeTime") == null )
                this.MakeTime = null;
            else
                this.MakeTime = rs.getString("MakeTime").trim();

            this.ModifyDate = rs.getDate("ModifyDate");
            if( rs.getString("ModifyTime") == null )
                this.ModifyTime = null;
            else
                this.ModifyTime = rs.getString("ModifyTime").trim();

        }
        catch(SQLException sqle) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LBSpecSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = ExceptionUtils.exceptionToString(sqle);
            this.mErrors .addOneError(tError);
            return false;
        }
        return true;
    }

    public LBSpecSchema getSchema() {
        LBSpecSchema aLBSpecSchema = new LBSpecSchema();
        aLBSpecSchema.setSchema(this);
        return aLBSpecSchema;
    }

    public LBSpecDB getDB() {
        LBSpecDB aDBOper = new LBSpecDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
    * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLBSpec描述/A>表字段
    * @return: String 返回打包后字符串
    **/
    public String encode() {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(ChgData.chgData(SpecID));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(ContID));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ShardingID)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(EdorNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(GrpContNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ContNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(PolNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ProposalNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(PrtSeq)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(SerialNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(EndorsementNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(SpecType)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(SpecCode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(SpecContent)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(PrtFlag)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(BackupType)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ModifyTime));
        return strReturn.toString();
    }

    /**
    * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLBSpec>历史记账凭证主表信息</A>表字段
    * @param: strMessage String 包含一条纪录数据的字符串
    * @return: boolean
    **/
    public boolean decode(String strMessage) {
        try {
            SpecID = new Long(ChgData.chgNumericStr(StrTool.getStr(strMessage,1, SysConst.PACKAGESPILTER))).longValue();
            ContID = new Long(ChgData.chgNumericStr(StrTool.getStr(strMessage,2, SysConst.PACKAGESPILTER))).longValue();
            ShardingID = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
            EdorNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
            GrpContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
            ContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
            PolNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
            ProposalNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
            PrtSeq = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
            SerialNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
            EndorsementNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
            SpecType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
            SpecCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
            SpecContent = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER );
            PrtFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15, SysConst.PACKAGESPILTER );
            BackupType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16, SysConst.PACKAGESPILTER );
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17, SysConst.PACKAGESPILTER );
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19, SysConst.PACKAGESPILTER );
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 21, SysConst.PACKAGESPILTER );
        }
        catch(NumberFormatException ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LBSpecSchema";
            tError.functionName = "decode";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            return false;
        }
        return true;
    }

    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("SpecID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SpecID));
        }
        if (FCode.equalsIgnoreCase("ContID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ContID));
        }
        if (FCode.equalsIgnoreCase("ShardingID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ShardingID));
        }
        if (FCode.equalsIgnoreCase("EdorNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(EdorNo));
        }
        if (FCode.equalsIgnoreCase("GrpContNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GrpContNo));
        }
        if (FCode.equalsIgnoreCase("ContNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ContNo));
        }
        if (FCode.equalsIgnoreCase("PolNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PolNo));
        }
        if (FCode.equalsIgnoreCase("ProposalNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ProposalNo));
        }
        if (FCode.equalsIgnoreCase("PrtSeq")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PrtSeq));
        }
        if (FCode.equalsIgnoreCase("SerialNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SerialNo));
        }
        if (FCode.equalsIgnoreCase("EndorsementNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(EndorsementNo));
        }
        if (FCode.equalsIgnoreCase("SpecType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SpecType));
        }
        if (FCode.equalsIgnoreCase("SpecCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SpecCode));
        }
        if (FCode.equalsIgnoreCase("SpecContent")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SpecContent));
        }
        if (FCode.equalsIgnoreCase("PrtFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PrtFlag));
        }
        if (FCode.equalsIgnoreCase("BackupType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BackupType));
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = String.valueOf(SpecID);
                break;
            case 1:
                strFieldValue = String.valueOf(ContID);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(ShardingID);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(EdorNo);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(GrpContNo);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(ContNo);
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(PolNo);
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(ProposalNo);
                break;
            case 8:
                strFieldValue = StrTool.GBKToUnicode(PrtSeq);
                break;
            case 9:
                strFieldValue = StrTool.GBKToUnicode(SerialNo);
                break;
            case 10:
                strFieldValue = StrTool.GBKToUnicode(EndorsementNo);
                break;
            case 11:
                strFieldValue = StrTool.GBKToUnicode(SpecType);
                break;
            case 12:
                strFieldValue = StrTool.GBKToUnicode(SpecCode);
                break;
            case 13:
                strFieldValue = StrTool.GBKToUnicode(SpecContent);
                break;
            case 14:
                strFieldValue = StrTool.GBKToUnicode(PrtFlag);
                break;
            case 15:
                strFieldValue = StrTool.GBKToUnicode(BackupType);
                break;
            case 16:
                strFieldValue = StrTool.GBKToUnicode(Operator);
                break;
            case 17:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
                break;
            case 18:
                strFieldValue = StrTool.GBKToUnicode(MakeTime);
                break;
            case 19:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
                break;
            case 20:
                strFieldValue = StrTool.GBKToUnicode(ModifyTime);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("SpecID")) {
            if( FValue != null && !FValue.equals("")) {
                SpecID = new Long(FValue).longValue();
            }
        }
        if (FCode.equalsIgnoreCase("ContID")) {
            if( FValue != null && !FValue.equals("")) {
                ContID = new Long(FValue).longValue();
            }
        }
        if (FCode.equalsIgnoreCase("ShardingID")) {
            if( FValue != null && !FValue.equals(""))
            {
                ShardingID = FValue.trim();
            }
            else
                ShardingID = null;
        }
        if (FCode.equalsIgnoreCase("EdorNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                EdorNo = FValue.trim();
            }
            else
                EdorNo = null;
        }
        if (FCode.equalsIgnoreCase("GrpContNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                GrpContNo = FValue.trim();
            }
            else
                GrpContNo = null;
        }
        if (FCode.equalsIgnoreCase("ContNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                ContNo = FValue.trim();
            }
            else
                ContNo = null;
        }
        if (FCode.equalsIgnoreCase("PolNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                PolNo = FValue.trim();
            }
            else
                PolNo = null;
        }
        if (FCode.equalsIgnoreCase("ProposalNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                ProposalNo = FValue.trim();
            }
            else
                ProposalNo = null;
        }
        if (FCode.equalsIgnoreCase("PrtSeq")) {
            if( FValue != null && !FValue.equals(""))
            {
                PrtSeq = FValue.trim();
            }
            else
                PrtSeq = null;
        }
        if (FCode.equalsIgnoreCase("SerialNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                SerialNo = FValue.trim();
            }
            else
                SerialNo = null;
        }
        if (FCode.equalsIgnoreCase("EndorsementNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                EndorsementNo = FValue.trim();
            }
            else
                EndorsementNo = null;
        }
        if (FCode.equalsIgnoreCase("SpecType")) {
            if( FValue != null && !FValue.equals(""))
            {
                SpecType = FValue.trim();
            }
            else
                SpecType = null;
        }
        if (FCode.equalsIgnoreCase("SpecCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                SpecCode = FValue.trim();
            }
            else
                SpecCode = null;
        }
        if (FCode.equalsIgnoreCase("SpecContent")) {
            if( FValue != null && !FValue.equals(""))
            {
                SpecContent = FValue.trim();
            }
            else
                SpecContent = null;
        }
        if (FCode.equalsIgnoreCase("PrtFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                PrtFlag = FValue.trim();
            }
            else
                PrtFlag = null;
        }
        if (FCode.equalsIgnoreCase("BackupType")) {
            if( FValue != null && !FValue.equals(""))
            {
                BackupType = FValue.trim();
            }
            else
                BackupType = null;
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            if( FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
                Operator = null;
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if(FValue != null && !FValue.equals("")) {
                MakeDate = fDate.getDate( FValue );
            }
            else
                MakeDate = null;
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
                MakeTime = null;
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if(FValue != null && !FValue.equals("")) {
                ModifyDate = fDate.getDate( FValue );
            }
            else
                ModifyDate = null;
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
                ModifyTime = null;
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject) return true;
        if (otherObject == null) return false;
        if (getClass() != otherObject.getClass()) return false;
        LBSpecSchema other = (LBSpecSchema)otherObject;
        return
            SpecID == other.getSpecID()
            && ContID == other.getContID()
            && ShardingID.equals(other.getShardingID())
            && EdorNo.equals(other.getEdorNo())
            && GrpContNo.equals(other.getGrpContNo())
            && ContNo.equals(other.getContNo())
            && PolNo.equals(other.getPolNo())
            && ProposalNo.equals(other.getProposalNo())
            && PrtSeq.equals(other.getPrtSeq())
            && SerialNo.equals(other.getSerialNo())
            && EndorsementNo.equals(other.getEndorsementNo())
            && SpecType.equals(other.getSpecType())
            && SpecCode.equals(other.getSpecCode())
            && SpecContent.equals(other.getSpecContent())
            && PrtFlag.equals(other.getPrtFlag())
            && BackupType.equals(other.getBackupType())
            && Operator.equals(other.getOperator())
            && fDate.getString(MakeDate).equals(other.getMakeDate())
            && MakeTime.equals(other.getMakeTime())
            && fDate.getString(ModifyDate).equals(other.getModifyDate())
            && ModifyTime.equals(other.getModifyTime());
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("SpecID") ) {
            return 0;
        }
        if( strFieldName.equals("ContID") ) {
            return 1;
        }
        if( strFieldName.equals("ShardingID") ) {
            return 2;
        }
        if( strFieldName.equals("EdorNo") ) {
            return 3;
        }
        if( strFieldName.equals("GrpContNo") ) {
            return 4;
        }
        if( strFieldName.equals("ContNo") ) {
            return 5;
        }
        if( strFieldName.equals("PolNo") ) {
            return 6;
        }
        if( strFieldName.equals("ProposalNo") ) {
            return 7;
        }
        if( strFieldName.equals("PrtSeq") ) {
            return 8;
        }
        if( strFieldName.equals("SerialNo") ) {
            return 9;
        }
        if( strFieldName.equals("EndorsementNo") ) {
            return 10;
        }
        if( strFieldName.equals("SpecType") ) {
            return 11;
        }
        if( strFieldName.equals("SpecCode") ) {
            return 12;
        }
        if( strFieldName.equals("SpecContent") ) {
            return 13;
        }
        if( strFieldName.equals("PrtFlag") ) {
            return 14;
        }
        if( strFieldName.equals("BackupType") ) {
            return 15;
        }
        if( strFieldName.equals("Operator") ) {
            return 16;
        }
        if( strFieldName.equals("MakeDate") ) {
            return 17;
        }
        if( strFieldName.equals("MakeTime") ) {
            return 18;
        }
        if( strFieldName.equals("ModifyDate") ) {
            return 19;
        }
        if( strFieldName.equals("ModifyTime") ) {
            return 20;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "SpecID";
                break;
            case 1:
                strFieldName = "ContID";
                break;
            case 2:
                strFieldName = "ShardingID";
                break;
            case 3:
                strFieldName = "EdorNo";
                break;
            case 4:
                strFieldName = "GrpContNo";
                break;
            case 5:
                strFieldName = "ContNo";
                break;
            case 6:
                strFieldName = "PolNo";
                break;
            case 7:
                strFieldName = "ProposalNo";
                break;
            case 8:
                strFieldName = "PrtSeq";
                break;
            case 9:
                strFieldName = "SerialNo";
                break;
            case 10:
                strFieldName = "EndorsementNo";
                break;
            case 11:
                strFieldName = "SpecType";
                break;
            case 12:
                strFieldName = "SpecCode";
                break;
            case 13:
                strFieldName = "SpecContent";
                break;
            case 14:
                strFieldName = "PrtFlag";
                break;
            case 15:
                strFieldName = "BackupType";
                break;
            case 16:
                strFieldName = "Operator";
                break;
            case 17:
                strFieldName = "MakeDate";
                break;
            case 18:
                strFieldName = "MakeTime";
                break;
            case 19:
                strFieldName = "ModifyDate";
                break;
            case 20:
                strFieldName = "ModifyTime";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "SPECID":
                return Schema.TYPE_LONG;
            case "CONTID":
                return Schema.TYPE_LONG;
            case "SHARDINGID":
                return Schema.TYPE_STRING;
            case "EDORNO":
                return Schema.TYPE_STRING;
            case "GRPCONTNO":
                return Schema.TYPE_STRING;
            case "CONTNO":
                return Schema.TYPE_STRING;
            case "POLNO":
                return Schema.TYPE_STRING;
            case "PROPOSALNO":
                return Schema.TYPE_STRING;
            case "PRTSEQ":
                return Schema.TYPE_STRING;
            case "SERIALNO":
                return Schema.TYPE_STRING;
            case "ENDORSEMENTNO":
                return Schema.TYPE_STRING;
            case "SPECTYPE":
                return Schema.TYPE_STRING;
            case "SPECCODE":
                return Schema.TYPE_STRING;
            case "SPECCONTENT":
                return Schema.TYPE_STRING;
            case "PRTFLAG":
                return Schema.TYPE_STRING;
            case "BACKUPTYPE":
                return Schema.TYPE_STRING;
            case "OPERATOR":
                return Schema.TYPE_STRING;
            case "MAKEDATE":
                return Schema.TYPE_DATE;
            case "MAKETIME":
                return Schema.TYPE_STRING;
            case "MODIFYDATE":
                return Schema.TYPE_DATE;
            case "MODIFYTIME":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_LONG;
            case 1:
                return Schema.TYPE_LONG;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_STRING;
            case 6:
                return Schema.TYPE_STRING;
            case 7:
                return Schema.TYPE_STRING;
            case 8:
                return Schema.TYPE_STRING;
            case 9:
                return Schema.TYPE_STRING;
            case 10:
                return Schema.TYPE_STRING;
            case 11:
                return Schema.TYPE_STRING;
            case 12:
                return Schema.TYPE_STRING;
            case 13:
                return Schema.TYPE_STRING;
            case 14:
                return Schema.TYPE_STRING;
            case 15:
                return Schema.TYPE_STRING;
            case 16:
                return Schema.TYPE_STRING;
            case 17:
                return Schema.TYPE_DATE;
            case 18:
                return Schema.TYPE_STRING;
            case 19:
                return Schema.TYPE_DATE;
            case 20:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
}
