package com.sinosoft.lis.entity;

import java.io.Serializable;

/**
 * Created by zhangfei on 2018/11/1.
 *
 * 存储移动展业的拓展信息
 */
public class CaluParamInfoPojo implements Serializable{
    private String riskCode;
    private String caluParamCode;
    private String caluParamValue;

    public CaluParamInfoPojo(String riskCode, String caluParamCode, String caluParamValue) {
        this.riskCode = riskCode;
        this.caluParamCode = caluParamCode;
        this.caluParamValue = caluParamValue;
    }

    public CaluParamInfoPojo() {
    }

    public String getRiskCode() {
        return riskCode;
    }

    public void setRiskCode(String riskCode) {
        this.riskCode = riskCode;
    }

    public String getCaluParamCode() {
        return caluParamCode;
    }

    public void setCaluParamCode(String caluParamCode) {
        this.caluParamCode = caluParamCode;
    }

    public String getCaluParamValue() {
        return caluParamValue;
    }

    public void setCaluParamValue(String caluParamValue) {
        this.caluParamValue = caluParamValue;
    }

    @Override
    public String toString() {
        return "CaluParamInfoPojo{" +
                "riskCode='" + riskCode + '\'' +
                ", caluParamCode='" + caluParamCode + '\'' +
                ", caluParamValue='" + caluParamValue + '\'' +
                '}';
    }
}
