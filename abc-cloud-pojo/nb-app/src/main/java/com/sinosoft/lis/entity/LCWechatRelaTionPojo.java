/**
 * Copyright (c) 2019 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.entity;

import com.sinosoft.cloud.cache.annotation.RedisPrimaryHKey;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.Pojo;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;

import java.io.Serializable;

/**
 * <p>ClassName: LCWechatRelaTionPojo </p>
 * <p>Copyright: Copyright (c) 2019</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2019-03-14
 */
public class LCWechatRelaTionPojo implements  Pojo,Serializable {
    @RedisPrimaryHKey
    // @Field
    /** 保单号 */
    private String Contno; 
    /** 先前保单号 */
    private String BeforeContno; 
    /** 初始保单号 */
    private String BeginContno; 
    /** 承保日期 */
    private String  SignDate;
    /** 生效日期 */
    private String  CValiDate;
    /** 终止日期 */
    private String  FirstDate;
    /** 保险费收费确认日期 */
    private String  ConfMakeDate;
    /** 交费方式 */
    private String PayMode; 
    /** 交费帐号 */
    private String BankaccNo; 
    /** 交费账户户名 */
    private String AccName; 
    /** 入机日期 */
    private String  ModifyDate;
    /** 入机时间 */
    private String ModifyTime; 
    /** 初始日期 */
    private String  MakeDate;
    /** 初始时间 */
    private String MakeTime; 


    public static final int FIELDNUM = 14;    // 数据库表的字段个数

    private FDate fDate = new FDate();        // 处理日期
    public String getContno() {
        return Contno;
    }
    public void setContno(String aContno) {
        Contno = aContno;
    }
    public String getBeforeContno() {
        return BeforeContno;
    }
    public void setBeforeContno(String aBeforeContno) {
        BeforeContno = aBeforeContno;
    }
    public String getBeginContno() {
        return BeginContno;
    }
    public void setBeginContno(String aBeginContno) {
        BeginContno = aBeginContno;
    }
    public String getSignDate() {
        return SignDate;
    }
    public void setSignDate(String aSignDate) {
        SignDate = aSignDate;
    }
    public String getCValiDate() {
        return CValiDate;
    }
    public void setCValiDate(String aCValiDate) {
        CValiDate = aCValiDate;
    }
    public String getFirstDate() {
        return FirstDate;
    }
    public void setFirstDate(String aFirstDate) {
        FirstDate = aFirstDate;
    }
    public String getConfMakeDate() {
        return ConfMakeDate;
    }
    public void setConfMakeDate(String aConfMakeDate) {
        ConfMakeDate = aConfMakeDate;
    }
    public String getPayMode() {
        return PayMode;
    }
    public void setPayMode(String aPayMode) {
        PayMode = aPayMode;
    }
    public String getBankaccNo() {
        return BankaccNo;
    }
    public void setBankaccNo(String aBankaccNo) {
        BankaccNo = aBankaccNo;
    }
    public String getAccName() {
        return AccName;
    }
    public void setAccName(String aAccName) {
        AccName = aAccName;
    }
    public String getModifyDate() {
        return ModifyDate;
    }
    public void setModifyDate(String aModifyDate) {
        ModifyDate = aModifyDate;
    }
    public String getModifyTime() {
        return ModifyTime;
    }
    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }
    public String getMakeDate() {
        return MakeDate;
    }
    public void setMakeDate(String aMakeDate) {
        MakeDate = aMakeDate;
    }
    public String getMakeTime() {
        return MakeTime;
    }
    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("Contno") ) {
            return 0;
        }
        if( strFieldName.equals("BeforeContno") ) {
            return 1;
        }
        if( strFieldName.equals("BeginContno") ) {
            return 2;
        }
        if( strFieldName.equals("SignDate") ) {
            return 3;
        }
        if( strFieldName.equals("CValiDate") ) {
            return 4;
        }
        if( strFieldName.equals("FirstDate") ) {
            return 5;
        }
        if( strFieldName.equals("ConfMakeDate") ) {
            return 6;
        }
        if( strFieldName.equals("PayMode") ) {
            return 7;
        }
        if( strFieldName.equals("BankaccNo") ) {
            return 8;
        }
        if( strFieldName.equals("AccName") ) {
            return 9;
        }
        if( strFieldName.equals("ModifyDate") ) {
            return 10;
        }
        if( strFieldName.equals("ModifyTime") ) {
            return 11;
        }
        if( strFieldName.equals("MakeDate") ) {
            return 12;
        }
        if( strFieldName.equals("MakeTime") ) {
            return 13;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "Contno";
                break;
            case 1:
                strFieldName = "BeforeContno";
                break;
            case 2:
                strFieldName = "BeginContno";
                break;
            case 3:
                strFieldName = "SignDate";
                break;
            case 4:
                strFieldName = "CValiDate";
                break;
            case 5:
                strFieldName = "FirstDate";
                break;
            case 6:
                strFieldName = "ConfMakeDate";
                break;
            case 7:
                strFieldName = "PayMode";
                break;
            case 8:
                strFieldName = "BankaccNo";
                break;
            case 9:
                strFieldName = "AccName";
                break;
            case 10:
                strFieldName = "ModifyDate";
                break;
            case 11:
                strFieldName = "ModifyTime";
                break;
            case 12:
                strFieldName = "MakeDate";
                break;
            case 13:
                strFieldName = "MakeTime";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "CONTNO":
                return Schema.TYPE_STRING;
            case "BEFORECONTNO":
                return Schema.TYPE_STRING;
            case "BEGINCONTNO":
                return Schema.TYPE_STRING;
            case "SIGNDATE":
                return Schema.TYPE_STRING;
            case "CVALIDATE":
                return Schema.TYPE_STRING;
            case "FIRSTDATE":
                return Schema.TYPE_STRING;
            case "CONFMAKEDATE":
                return Schema.TYPE_STRING;
            case "PAYMODE":
                return Schema.TYPE_STRING;
            case "BANKACCNO":
                return Schema.TYPE_STRING;
            case "ACCNAME":
                return Schema.TYPE_STRING;
            case "MODIFYDATE":
                return Schema.TYPE_STRING;
            case "MODIFYTIME":
                return Schema.TYPE_STRING;
            case "MAKEDATE":
                return Schema.TYPE_STRING;
            case "MAKETIME":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_STRING;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_STRING;
            case 6:
                return Schema.TYPE_STRING;
            case 7:
                return Schema.TYPE_STRING;
            case 8:
                return Schema.TYPE_STRING;
            case 9:
                return Schema.TYPE_STRING;
            case 10:
                return Schema.TYPE_STRING;
            case 11:
                return Schema.TYPE_STRING;
            case 12:
                return Schema.TYPE_STRING;
            case 13:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("Contno")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Contno));
        }
        if (FCode.equalsIgnoreCase("BeforeContno")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BeforeContno));
        }
        if (FCode.equalsIgnoreCase("BeginContno")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BeginContno));
        }
        if (FCode.equalsIgnoreCase("SignDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SignDate));
        }
        if (FCode.equalsIgnoreCase("CValiDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CValiDate));
        }
        if (FCode.equalsIgnoreCase("FirstDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FirstDate));
        }
        if (FCode.equalsIgnoreCase("ConfMakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ConfMakeDate));
        }
        if (FCode.equalsIgnoreCase("PayMode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PayMode));
        }
        if (FCode.equalsIgnoreCase("BankaccNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BankaccNo));
        }
        if (FCode.equalsIgnoreCase("AccName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AccName));
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyDate));
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeDate));
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = String.valueOf(Contno);
                break;
            case 1:
                strFieldValue = String.valueOf(BeforeContno);
                break;
            case 2:
                strFieldValue = String.valueOf(BeginContno);
                break;
            case 3:
                strFieldValue = String.valueOf(SignDate);
                break;
            case 4:
                strFieldValue = String.valueOf(CValiDate);
                break;
            case 5:
                strFieldValue = String.valueOf(FirstDate);
                break;
            case 6:
                strFieldValue = String.valueOf(ConfMakeDate);
                break;
            case 7:
                strFieldValue = String.valueOf(PayMode);
                break;
            case 8:
                strFieldValue = String.valueOf(BankaccNo);
                break;
            case 9:
                strFieldValue = String.valueOf(AccName);
                break;
            case 10:
                strFieldValue = String.valueOf(ModifyDate);
                break;
            case 11:
                strFieldValue = String.valueOf(ModifyTime);
                break;
            case 12:
                strFieldValue = String.valueOf(MakeDate);
                break;
            case 13:
                strFieldValue = String.valueOf(MakeTime);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("Contno")) {
            if( FValue != null && !FValue.equals(""))
            {
                Contno = FValue.trim();
            }
            else
                Contno = null;
        }
        if (FCode.equalsIgnoreCase("BeforeContno")) {
            if( FValue != null && !FValue.equals(""))
            {
                BeforeContno = FValue.trim();
            }
            else
                BeforeContno = null;
        }
        if (FCode.equalsIgnoreCase("BeginContno")) {
            if( FValue != null && !FValue.equals(""))
            {
                BeginContno = FValue.trim();
            }
            else
                BeginContno = null;
        }
        if (FCode.equalsIgnoreCase("SignDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                SignDate = FValue.trim();
            }
            else
                SignDate = null;
        }
        if (FCode.equalsIgnoreCase("CValiDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                CValiDate = FValue.trim();
            }
            else
                CValiDate = null;
        }
        if (FCode.equalsIgnoreCase("FirstDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                FirstDate = FValue.trim();
            }
            else
                FirstDate = null;
        }
        if (FCode.equalsIgnoreCase("ConfMakeDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                ConfMakeDate = FValue.trim();
            }
            else
                ConfMakeDate = null;
        }
        if (FCode.equalsIgnoreCase("PayMode")) {
            if( FValue != null && !FValue.equals(""))
            {
                PayMode = FValue.trim();
            }
            else
                PayMode = null;
        }
        if (FCode.equalsIgnoreCase("BankaccNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                BankaccNo = FValue.trim();
            }
            else
                BankaccNo = null;
        }
        if (FCode.equalsIgnoreCase("AccName")) {
            if( FValue != null && !FValue.equals(""))
            {
                AccName = FValue.trim();
            }
            else
                AccName = null;
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyDate = FValue.trim();
            }
            else
                ModifyDate = null;
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
                ModifyTime = null;
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeDate = FValue.trim();
            }
            else
                MakeDate = null;
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
                MakeTime = null;
        }
        return true;
    }


    public String toString() {
    return "LCWechatRelaTionPojo [" +
            "Contno="+Contno +
            ", BeforeContno="+BeforeContno +
            ", BeginContno="+BeginContno +
            ", SignDate="+SignDate +
            ", CValiDate="+CValiDate +
            ", FirstDate="+FirstDate +
            ", ConfMakeDate="+ConfMakeDate +
            ", PayMode="+PayMode +
            ", BankaccNo="+BankaccNo +
            ", AccName="+AccName +
            ", ModifyDate="+ModifyDate +
            ", ModifyTime="+ModifyTime +
            ", MakeDate="+MakeDate +
            ", MakeTime="+MakeTime +"]";
    }
}
