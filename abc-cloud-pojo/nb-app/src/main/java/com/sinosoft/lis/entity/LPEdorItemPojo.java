/**
 * Copyright (c) 2017 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.entity;

import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.Pojo;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;

import java.io.Serializable;

/**
 * <p>ClassName: LPEdorItemPojo </p>
 * <p>Copyright: Copyright (c) 2017</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2017-10-17
 */
public class LPEdorItemPojo implements Pojo,Serializable {
    // @Field
    /** 保全受理号 */
    private String EdorAcceptNo; 
    /** 批单号 */
    private String EdorNo; 
    /** 批改申请号 */
    private String EdorAppNo; 
    /** 批改类型 */
    private String EdorType; 
    /** 批改类型显示级别 */
    private String DisplayType; 
    /** 集体合同号码 */
    private String GrpContNo; 
    /** 合同号码 */
    private String ContNo; 
    /** 被保人客户号码 */
    private String InsuredNo; 
    /** 保单险种号码 */
    private String PolNo; 
    /** 管理机构 */
    private String ManageCom; 
    /** 批改生效日期 */
    private String  EdorValiDate;
    /** 批改申请日期 */
    private String  EdorAppDate;
    /** 批改状态 */
    private String EdorState; 
    /** 核保状态 */
    private String UWFlag; 
    /** 核保人 */
    private String UWOperator; 
    /** 核保完成日期 */
    private String  UWDate;
    /** 核保完成时间 */
    private String UWTime; 
    /** 变动的保费 */
    private double ChgPrem; 
    /** 变动的保额 */
    private double ChgAmnt; 
    /** 补/退费金额 */
    private double GetMoney; 
    /** 补/退费利息 */
    private double GetInterest; 
    /** 操作员 */
    private String Operator; 
    /** 入机日期 */
    private String  MakeDate;
    /** 入机时间 */
    private String MakeTime; 
    /** 最后一次修改日期 */
    private String  ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime; 
    /** 保全申请原因 */
    private String AppReason; 
    /** 保全变更原因编码 */
    private String EdorReasonCode; 
    /** 保全变更原因 */
    private String EdorReason; 
    /** 复核状态 */
    private String ApproveFlag; 
    /** 复核人 */
    private String ApproveOperator; 
    /** 复核日期 */
    private String  ApproveDate;
    /** 复核时间 */
    private String ApproveTime; 
    /** 备用属性字段1 */
    private String StandbyFlag1; 
    /** 备用属性字段2 */
    private String StandbyFlag2; 
    /** 备用属性字段3 */
    private String StandbyFlag3; 
    /** 保全计算类型 */
    private String EdorTypeCal; 
    /** 备用属性字段4 */
    private String StandbyFlag4; 
    /** 备用属性字段5 */
    private String StandbyFlag5; 
    /** 备用属性字段6 */
    private String StandbyFlag6; 
    /** 保全变更具体原因 */
    private String EdorReasonRemark; 
    /** Oa编号 */
    private String OACode; 


    public static final int FIELDNUM = 42;    // 数据库表的字段个数

    private FDate fDate = new FDate();        // 处理日期
    public String getEdorAcceptNo() {
        return EdorAcceptNo;
    }
    public void setEdorAcceptNo(String aEdorAcceptNo) {
        EdorAcceptNo = aEdorAcceptNo;
    }
    public String getEdorNo() {
        return EdorNo;
    }
    public void setEdorNo(String aEdorNo) {
        EdorNo = aEdorNo;
    }
    public String getEdorAppNo() {
        return EdorAppNo;
    }
    public void setEdorAppNo(String aEdorAppNo) {
        EdorAppNo = aEdorAppNo;
    }
    public String getEdorType() {
        return EdorType;
    }
    public void setEdorType(String aEdorType) {
        EdorType = aEdorType;
    }
    public String getDisplayType() {
        return DisplayType;
    }
    public void setDisplayType(String aDisplayType) {
        DisplayType = aDisplayType;
    }
    public String getGrpContNo() {
        return GrpContNo;
    }
    public void setGrpContNo(String aGrpContNo) {
        GrpContNo = aGrpContNo;
    }
    public String getContNo() {
        return ContNo;
    }
    public void setContNo(String aContNo) {
        ContNo = aContNo;
    }
    public String getInsuredNo() {
        return InsuredNo;
    }
    public void setInsuredNo(String aInsuredNo) {
        InsuredNo = aInsuredNo;
    }
    public String getPolNo() {
        return PolNo;
    }
    public void setPolNo(String aPolNo) {
        PolNo = aPolNo;
    }
    public String getManageCom() {
        return ManageCom;
    }
    public void setManageCom(String aManageCom) {
        ManageCom = aManageCom;
    }
    public String getEdorValiDate() {
        return EdorValiDate;
    }
    public void setEdorValiDate(String aEdorValiDate) {
        EdorValiDate = aEdorValiDate;
    }
    public String getEdorAppDate() {
        return EdorAppDate;
    }
    public void setEdorAppDate(String aEdorAppDate) {
        EdorAppDate = aEdorAppDate;
    }
    public String getEdorState() {
        return EdorState;
    }
    public void setEdorState(String aEdorState) {
        EdorState = aEdorState;
    }
    public String getUWFlag() {
        return UWFlag;
    }
    public void setUWFlag(String aUWFlag) {
        UWFlag = aUWFlag;
    }
    public String getUWOperator() {
        return UWOperator;
    }
    public void setUWOperator(String aUWOperator) {
        UWOperator = aUWOperator;
    }
    public String getUWDate() {
        return UWDate;
    }
    public void setUWDate(String aUWDate) {
        UWDate = aUWDate;
    }
    public String getUWTime() {
        return UWTime;
    }
    public void setUWTime(String aUWTime) {
        UWTime = aUWTime;
    }
    public double getChgPrem() {
        return ChgPrem;
    }
    public void setChgPrem(double aChgPrem) {
        ChgPrem = aChgPrem;
    }
    public void setChgPrem(String aChgPrem) {
        if (aChgPrem != null && !aChgPrem.equals("")) {
            Double tDouble = new Double(aChgPrem);
            double d = tDouble.doubleValue();
            ChgPrem = d;
        }
    }

    public double getChgAmnt() {
        return ChgAmnt;
    }
    public void setChgAmnt(double aChgAmnt) {
        ChgAmnt = aChgAmnt;
    }
    public void setChgAmnt(String aChgAmnt) {
        if (aChgAmnt != null && !aChgAmnt.equals("")) {
            Double tDouble = new Double(aChgAmnt);
            double d = tDouble.doubleValue();
            ChgAmnt = d;
        }
    }

    public double getGetMoney() {
        return GetMoney;
    }
    public void setGetMoney(double aGetMoney) {
        GetMoney = aGetMoney;
    }
    public void setGetMoney(String aGetMoney) {
        if (aGetMoney != null && !aGetMoney.equals("")) {
            Double tDouble = new Double(aGetMoney);
            double d = tDouble.doubleValue();
            GetMoney = d;
        }
    }

    public double getGetInterest() {
        return GetInterest;
    }
    public void setGetInterest(double aGetInterest) {
        GetInterest = aGetInterest;
    }
    public void setGetInterest(String aGetInterest) {
        if (aGetInterest != null && !aGetInterest.equals("")) {
            Double tDouble = new Double(aGetInterest);
            double d = tDouble.doubleValue();
            GetInterest = d;
        }
    }

    public String getOperator() {
        return Operator;
    }
    public void setOperator(String aOperator) {
        Operator = aOperator;
    }
    public String getMakeDate() {
        return MakeDate;
    }
    public void setMakeDate(String aMakeDate) {
        MakeDate = aMakeDate;
    }
    public String getMakeTime() {
        return MakeTime;
    }
    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }
    public String getModifyDate() {
        return ModifyDate;
    }
    public void setModifyDate(String aModifyDate) {
        ModifyDate = aModifyDate;
    }
    public String getModifyTime() {
        return ModifyTime;
    }
    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }
    public String getAppReason() {
        return AppReason;
    }
    public void setAppReason(String aAppReason) {
        AppReason = aAppReason;
    }
    public String getEdorReasonCode() {
        return EdorReasonCode;
    }
    public void setEdorReasonCode(String aEdorReasonCode) {
        EdorReasonCode = aEdorReasonCode;
    }
    public String getEdorReason() {
        return EdorReason;
    }
    public void setEdorReason(String aEdorReason) {
        EdorReason = aEdorReason;
    }
    public String getApproveFlag() {
        return ApproveFlag;
    }
    public void setApproveFlag(String aApproveFlag) {
        ApproveFlag = aApproveFlag;
    }
    public String getApproveOperator() {
        return ApproveOperator;
    }
    public void setApproveOperator(String aApproveOperator) {
        ApproveOperator = aApproveOperator;
    }
    public String getApproveDate() {
        return ApproveDate;
    }
    public void setApproveDate(String aApproveDate) {
        ApproveDate = aApproveDate;
    }
    public String getApproveTime() {
        return ApproveTime;
    }
    public void setApproveTime(String aApproveTime) {
        ApproveTime = aApproveTime;
    }
    public String getStandbyFlag1() {
        return StandbyFlag1;
    }
    public void setStandbyFlag1(String aStandbyFlag1) {
        StandbyFlag1 = aStandbyFlag1;
    }
    public String getStandbyFlag2() {
        return StandbyFlag2;
    }
    public void setStandbyFlag2(String aStandbyFlag2) {
        StandbyFlag2 = aStandbyFlag2;
    }
    public String getStandbyFlag3() {
        return StandbyFlag3;
    }
    public void setStandbyFlag3(String aStandbyFlag3) {
        StandbyFlag3 = aStandbyFlag3;
    }
    public String getEdorTypeCal() {
        return EdorTypeCal;
    }
    public void setEdorTypeCal(String aEdorTypeCal) {
        EdorTypeCal = aEdorTypeCal;
    }
    public String getStandbyFlag4() {
        return StandbyFlag4;
    }
    public void setStandbyFlag4(String aStandbyFlag4) {
        StandbyFlag4 = aStandbyFlag4;
    }
    public String getStandbyFlag5() {
        return StandbyFlag5;
    }
    public void setStandbyFlag5(String aStandbyFlag5) {
        StandbyFlag5 = aStandbyFlag5;
    }
    public String getStandbyFlag6() {
        return StandbyFlag6;
    }
    public void setStandbyFlag6(String aStandbyFlag6) {
        StandbyFlag6 = aStandbyFlag6;
    }
    public String getEdorReasonRemark() {
        return EdorReasonRemark;
    }
    public void setEdorReasonRemark(String aEdorReasonRemark) {
        EdorReasonRemark = aEdorReasonRemark;
    }
    public String getOACode() {
        return OACode;
    }
    public void setOACode(String aOACode) {
        OACode = aOACode;
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("EdorAcceptNo") ) {
            return 0;
        }
        if( strFieldName.equals("EdorNo") ) {
            return 1;
        }
        if( strFieldName.equals("EdorAppNo") ) {
            return 2;
        }
        if( strFieldName.equals("EdorType") ) {
            return 3;
        }
        if( strFieldName.equals("DisplayType") ) {
            return 4;
        }
        if( strFieldName.equals("GrpContNo") ) {
            return 5;
        }
        if( strFieldName.equals("ContNo") ) {
            return 6;
        }
        if( strFieldName.equals("InsuredNo") ) {
            return 7;
        }
        if( strFieldName.equals("PolNo") ) {
            return 8;
        }
        if( strFieldName.equals("ManageCom") ) {
            return 9;
        }
        if( strFieldName.equals("EdorValiDate") ) {
            return 10;
        }
        if( strFieldName.equals("EdorAppDate") ) {
            return 11;
        }
        if( strFieldName.equals("EdorState") ) {
            return 12;
        }
        if( strFieldName.equals("UWFlag") ) {
            return 13;
        }
        if( strFieldName.equals("UWOperator") ) {
            return 14;
        }
        if( strFieldName.equals("UWDate") ) {
            return 15;
        }
        if( strFieldName.equals("UWTime") ) {
            return 16;
        }
        if( strFieldName.equals("ChgPrem") ) {
            return 17;
        }
        if( strFieldName.equals("ChgAmnt") ) {
            return 18;
        }
        if( strFieldName.equals("GetMoney") ) {
            return 19;
        }
        if( strFieldName.equals("GetInterest") ) {
            return 20;
        }
        if( strFieldName.equals("Operator") ) {
            return 21;
        }
        if( strFieldName.equals("MakeDate") ) {
            return 22;
        }
        if( strFieldName.equals("MakeTime") ) {
            return 23;
        }
        if( strFieldName.equals("ModifyDate") ) {
            return 24;
        }
        if( strFieldName.equals("ModifyTime") ) {
            return 25;
        }
        if( strFieldName.equals("AppReason") ) {
            return 26;
        }
        if( strFieldName.equals("EdorReasonCode") ) {
            return 27;
        }
        if( strFieldName.equals("EdorReason") ) {
            return 28;
        }
        if( strFieldName.equals("ApproveFlag") ) {
            return 29;
        }
        if( strFieldName.equals("ApproveOperator") ) {
            return 30;
        }
        if( strFieldName.equals("ApproveDate") ) {
            return 31;
        }
        if( strFieldName.equals("ApproveTime") ) {
            return 32;
        }
        if( strFieldName.equals("StandbyFlag1") ) {
            return 33;
        }
        if( strFieldName.equals("StandbyFlag2") ) {
            return 34;
        }
        if( strFieldName.equals("StandbyFlag3") ) {
            return 35;
        }
        if( strFieldName.equals("EdorTypeCal") ) {
            return 36;
        }
        if( strFieldName.equals("StandbyFlag4") ) {
            return 37;
        }
        if( strFieldName.equals("StandbyFlag5") ) {
            return 38;
        }
        if( strFieldName.equals("StandbyFlag6") ) {
            return 39;
        }
        if( strFieldName.equals("EdorReasonRemark") ) {
            return 40;
        }
        if( strFieldName.equals("OACode") ) {
            return 41;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "EdorAcceptNo";
                break;
            case 1:
                strFieldName = "EdorNo";
                break;
            case 2:
                strFieldName = "EdorAppNo";
                break;
            case 3:
                strFieldName = "EdorType";
                break;
            case 4:
                strFieldName = "DisplayType";
                break;
            case 5:
                strFieldName = "GrpContNo";
                break;
            case 6:
                strFieldName = "ContNo";
                break;
            case 7:
                strFieldName = "InsuredNo";
                break;
            case 8:
                strFieldName = "PolNo";
                break;
            case 9:
                strFieldName = "ManageCom";
                break;
            case 10:
                strFieldName = "EdorValiDate";
                break;
            case 11:
                strFieldName = "EdorAppDate";
                break;
            case 12:
                strFieldName = "EdorState";
                break;
            case 13:
                strFieldName = "UWFlag";
                break;
            case 14:
                strFieldName = "UWOperator";
                break;
            case 15:
                strFieldName = "UWDate";
                break;
            case 16:
                strFieldName = "UWTime";
                break;
            case 17:
                strFieldName = "ChgPrem";
                break;
            case 18:
                strFieldName = "ChgAmnt";
                break;
            case 19:
                strFieldName = "GetMoney";
                break;
            case 20:
                strFieldName = "GetInterest";
                break;
            case 21:
                strFieldName = "Operator";
                break;
            case 22:
                strFieldName = "MakeDate";
                break;
            case 23:
                strFieldName = "MakeTime";
                break;
            case 24:
                strFieldName = "ModifyDate";
                break;
            case 25:
                strFieldName = "ModifyTime";
                break;
            case 26:
                strFieldName = "AppReason";
                break;
            case 27:
                strFieldName = "EdorReasonCode";
                break;
            case 28:
                strFieldName = "EdorReason";
                break;
            case 29:
                strFieldName = "ApproveFlag";
                break;
            case 30:
                strFieldName = "ApproveOperator";
                break;
            case 31:
                strFieldName = "ApproveDate";
                break;
            case 32:
                strFieldName = "ApproveTime";
                break;
            case 33:
                strFieldName = "StandbyFlag1";
                break;
            case 34:
                strFieldName = "StandbyFlag2";
                break;
            case 35:
                strFieldName = "StandbyFlag3";
                break;
            case 36:
                strFieldName = "EdorTypeCal";
                break;
            case 37:
                strFieldName = "StandbyFlag4";
                break;
            case 38:
                strFieldName = "StandbyFlag5";
                break;
            case 39:
                strFieldName = "StandbyFlag6";
                break;
            case 40:
                strFieldName = "EdorReasonRemark";
                break;
            case 41:
                strFieldName = "OACode";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "EDORACCEPTNO":
                return Schema.TYPE_STRING;
            case "EDORNO":
                return Schema.TYPE_STRING;
            case "EDORAPPNO":
                return Schema.TYPE_STRING;
            case "EDORTYPE":
                return Schema.TYPE_STRING;
            case "DISPLAYTYPE":
                return Schema.TYPE_STRING;
            case "GRPCONTNO":
                return Schema.TYPE_STRING;
            case "CONTNO":
                return Schema.TYPE_STRING;
            case "INSUREDNO":
                return Schema.TYPE_STRING;
            case "POLNO":
                return Schema.TYPE_STRING;
            case "MANAGECOM":
                return Schema.TYPE_STRING;
            case "EDORVALIDATE":
                return Schema.TYPE_STRING;
            case "EDORAPPDATE":
                return Schema.TYPE_STRING;
            case "EDORSTATE":
                return Schema.TYPE_STRING;
            case "UWFLAG":
                return Schema.TYPE_STRING;
            case "UWOPERATOR":
                return Schema.TYPE_STRING;
            case "UWDATE":
                return Schema.TYPE_STRING;
            case "UWTIME":
                return Schema.TYPE_STRING;
            case "CHGPREM":
                return Schema.TYPE_DOUBLE;
            case "CHGAMNT":
                return Schema.TYPE_DOUBLE;
            case "GETMONEY":
                return Schema.TYPE_DOUBLE;
            case "GETINTEREST":
                return Schema.TYPE_DOUBLE;
            case "OPERATOR":
                return Schema.TYPE_STRING;
            case "MAKEDATE":
                return Schema.TYPE_STRING;
            case "MAKETIME":
                return Schema.TYPE_STRING;
            case "MODIFYDATE":
                return Schema.TYPE_STRING;
            case "MODIFYTIME":
                return Schema.TYPE_STRING;
            case "APPREASON":
                return Schema.TYPE_STRING;
            case "EDORREASONCODE":
                return Schema.TYPE_STRING;
            case "EDORREASON":
                return Schema.TYPE_STRING;
            case "APPROVEFLAG":
                return Schema.TYPE_STRING;
            case "APPROVEOPERATOR":
                return Schema.TYPE_STRING;
            case "APPROVEDATE":
                return Schema.TYPE_STRING;
            case "APPROVETIME":
                return Schema.TYPE_STRING;
            case "STANDBYFLAG1":
                return Schema.TYPE_STRING;
            case "STANDBYFLAG2":
                return Schema.TYPE_STRING;
            case "STANDBYFLAG3":
                return Schema.TYPE_STRING;
            case "EDORTYPECAL":
                return Schema.TYPE_STRING;
            case "STANDBYFLAG4":
                return Schema.TYPE_STRING;
            case "STANDBYFLAG5":
                return Schema.TYPE_STRING;
            case "STANDBYFLAG6":
                return Schema.TYPE_STRING;
            case "EDORREASONREMARK":
                return Schema.TYPE_STRING;
            case "OACODE":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_STRING;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_STRING;
            case 6:
                return Schema.TYPE_STRING;
            case 7:
                return Schema.TYPE_STRING;
            case 8:
                return Schema.TYPE_STRING;
            case 9:
                return Schema.TYPE_STRING;
            case 10:
                return Schema.TYPE_STRING;
            case 11:
                return Schema.TYPE_STRING;
            case 12:
                return Schema.TYPE_STRING;
            case 13:
                return Schema.TYPE_STRING;
            case 14:
                return Schema.TYPE_STRING;
            case 15:
                return Schema.TYPE_STRING;
            case 16:
                return Schema.TYPE_STRING;
            case 17:
                return Schema.TYPE_DOUBLE;
            case 18:
                return Schema.TYPE_DOUBLE;
            case 19:
                return Schema.TYPE_DOUBLE;
            case 20:
                return Schema.TYPE_DOUBLE;
            case 21:
                return Schema.TYPE_STRING;
            case 22:
                return Schema.TYPE_STRING;
            case 23:
                return Schema.TYPE_STRING;
            case 24:
                return Schema.TYPE_STRING;
            case 25:
                return Schema.TYPE_STRING;
            case 26:
                return Schema.TYPE_STRING;
            case 27:
                return Schema.TYPE_STRING;
            case 28:
                return Schema.TYPE_STRING;
            case 29:
                return Schema.TYPE_STRING;
            case 30:
                return Schema.TYPE_STRING;
            case 31:
                return Schema.TYPE_STRING;
            case 32:
                return Schema.TYPE_STRING;
            case 33:
                return Schema.TYPE_STRING;
            case 34:
                return Schema.TYPE_STRING;
            case 35:
                return Schema.TYPE_STRING;
            case 36:
                return Schema.TYPE_STRING;
            case 37:
                return Schema.TYPE_STRING;
            case 38:
                return Schema.TYPE_STRING;
            case 39:
                return Schema.TYPE_STRING;
            case 40:
                return Schema.TYPE_STRING;
            case 41:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("EdorAcceptNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(EdorAcceptNo));
        }
        if (FCode.equalsIgnoreCase("EdorNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(EdorNo));
        }
        if (FCode.equalsIgnoreCase("EdorAppNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(EdorAppNo));
        }
        if (FCode.equalsIgnoreCase("EdorType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(EdorType));
        }
        if (FCode.equalsIgnoreCase("DisplayType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DisplayType));
        }
        if (FCode.equalsIgnoreCase("GrpContNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GrpContNo));
        }
        if (FCode.equalsIgnoreCase("ContNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ContNo));
        }
        if (FCode.equalsIgnoreCase("InsuredNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InsuredNo));
        }
        if (FCode.equalsIgnoreCase("PolNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PolNo));
        }
        if (FCode.equalsIgnoreCase("ManageCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
        }
        if (FCode.equalsIgnoreCase("EdorValiDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(EdorValiDate));
        }
        if (FCode.equalsIgnoreCase("EdorAppDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(EdorAppDate));
        }
        if (FCode.equalsIgnoreCase("EdorState")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(EdorState));
        }
        if (FCode.equalsIgnoreCase("UWFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(UWFlag));
        }
        if (FCode.equalsIgnoreCase("UWOperator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(UWOperator));
        }
        if (FCode.equalsIgnoreCase("UWDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(UWDate));
        }
        if (FCode.equalsIgnoreCase("UWTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(UWTime));
        }
        if (FCode.equalsIgnoreCase("ChgPrem")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ChgPrem));
        }
        if (FCode.equalsIgnoreCase("ChgAmnt")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ChgAmnt));
        }
        if (FCode.equalsIgnoreCase("GetMoney")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GetMoney));
        }
        if (FCode.equalsIgnoreCase("GetInterest")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GetInterest));
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeDate));
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyDate));
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (FCode.equalsIgnoreCase("AppReason")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AppReason));
        }
        if (FCode.equalsIgnoreCase("EdorReasonCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(EdorReasonCode));
        }
        if (FCode.equalsIgnoreCase("EdorReason")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(EdorReason));
        }
        if (FCode.equalsIgnoreCase("ApproveFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ApproveFlag));
        }
        if (FCode.equalsIgnoreCase("ApproveOperator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ApproveOperator));
        }
        if (FCode.equalsIgnoreCase("ApproveDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ApproveDate));
        }
        if (FCode.equalsIgnoreCase("ApproveTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ApproveTime));
        }
        if (FCode.equalsIgnoreCase("StandbyFlag1")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StandbyFlag1));
        }
        if (FCode.equalsIgnoreCase("StandbyFlag2")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StandbyFlag2));
        }
        if (FCode.equalsIgnoreCase("StandbyFlag3")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StandbyFlag3));
        }
        if (FCode.equalsIgnoreCase("EdorTypeCal")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(EdorTypeCal));
        }
        if (FCode.equalsIgnoreCase("StandbyFlag4")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StandbyFlag4));
        }
        if (FCode.equalsIgnoreCase("StandbyFlag5")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StandbyFlag5));
        }
        if (FCode.equalsIgnoreCase("StandbyFlag6")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StandbyFlag6));
        }
        if (FCode.equalsIgnoreCase("EdorReasonRemark")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(EdorReasonRemark));
        }
        if (FCode.equalsIgnoreCase("OACode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OACode));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = String.valueOf(EdorAcceptNo);
                break;
            case 1:
                strFieldValue = String.valueOf(EdorNo);
                break;
            case 2:
                strFieldValue = String.valueOf(EdorAppNo);
                break;
            case 3:
                strFieldValue = String.valueOf(EdorType);
                break;
            case 4:
                strFieldValue = String.valueOf(DisplayType);
                break;
            case 5:
                strFieldValue = String.valueOf(GrpContNo);
                break;
            case 6:
                strFieldValue = String.valueOf(ContNo);
                break;
            case 7:
                strFieldValue = String.valueOf(InsuredNo);
                break;
            case 8:
                strFieldValue = String.valueOf(PolNo);
                break;
            case 9:
                strFieldValue = String.valueOf(ManageCom);
                break;
            case 10:
                strFieldValue = String.valueOf(EdorValiDate);
                break;
            case 11:
                strFieldValue = String.valueOf(EdorAppDate);
                break;
            case 12:
                strFieldValue = String.valueOf(EdorState);
                break;
            case 13:
                strFieldValue = String.valueOf(UWFlag);
                break;
            case 14:
                strFieldValue = String.valueOf(UWOperator);
                break;
            case 15:
                strFieldValue = String.valueOf(UWDate);
                break;
            case 16:
                strFieldValue = String.valueOf(UWTime);
                break;
            case 17:
                strFieldValue = String.valueOf(ChgPrem);
                break;
            case 18:
                strFieldValue = String.valueOf(ChgAmnt);
                break;
            case 19:
                strFieldValue = String.valueOf(GetMoney);
                break;
            case 20:
                strFieldValue = String.valueOf(GetInterest);
                break;
            case 21:
                strFieldValue = String.valueOf(Operator);
                break;
            case 22:
                strFieldValue = String.valueOf(MakeDate);
                break;
            case 23:
                strFieldValue = String.valueOf(MakeTime);
                break;
            case 24:
                strFieldValue = String.valueOf(ModifyDate);
                break;
            case 25:
                strFieldValue = String.valueOf(ModifyTime);
                break;
            case 26:
                strFieldValue = String.valueOf(AppReason);
                break;
            case 27:
                strFieldValue = String.valueOf(EdorReasonCode);
                break;
            case 28:
                strFieldValue = String.valueOf(EdorReason);
                break;
            case 29:
                strFieldValue = String.valueOf(ApproveFlag);
                break;
            case 30:
                strFieldValue = String.valueOf(ApproveOperator);
                break;
            case 31:
                strFieldValue = String.valueOf(ApproveDate);
                break;
            case 32:
                strFieldValue = String.valueOf(ApproveTime);
                break;
            case 33:
                strFieldValue = String.valueOf(StandbyFlag1);
                break;
            case 34:
                strFieldValue = String.valueOf(StandbyFlag2);
                break;
            case 35:
                strFieldValue = String.valueOf(StandbyFlag3);
                break;
            case 36:
                strFieldValue = String.valueOf(EdorTypeCal);
                break;
            case 37:
                strFieldValue = String.valueOf(StandbyFlag4);
                break;
            case 38:
                strFieldValue = String.valueOf(StandbyFlag5);
                break;
            case 39:
                strFieldValue = String.valueOf(StandbyFlag6);
                break;
            case 40:
                strFieldValue = String.valueOf(EdorReasonRemark);
                break;
            case 41:
                strFieldValue = String.valueOf(OACode);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("EdorAcceptNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                EdorAcceptNo = FValue.trim();
            }
            else
                EdorAcceptNo = null;
        }
        if (FCode.equalsIgnoreCase("EdorNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                EdorNo = FValue.trim();
            }
            else
                EdorNo = null;
        }
        if (FCode.equalsIgnoreCase("EdorAppNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                EdorAppNo = FValue.trim();
            }
            else
                EdorAppNo = null;
        }
        if (FCode.equalsIgnoreCase("EdorType")) {
            if( FValue != null && !FValue.equals(""))
            {
                EdorType = FValue.trim();
            }
            else
                EdorType = null;
        }
        if (FCode.equalsIgnoreCase("DisplayType")) {
            if( FValue != null && !FValue.equals(""))
            {
                DisplayType = FValue.trim();
            }
            else
                DisplayType = null;
        }
        if (FCode.equalsIgnoreCase("GrpContNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                GrpContNo = FValue.trim();
            }
            else
                GrpContNo = null;
        }
        if (FCode.equalsIgnoreCase("ContNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                ContNo = FValue.trim();
            }
            else
                ContNo = null;
        }
        if (FCode.equalsIgnoreCase("InsuredNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                InsuredNo = FValue.trim();
            }
            else
                InsuredNo = null;
        }
        if (FCode.equalsIgnoreCase("PolNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                PolNo = FValue.trim();
            }
            else
                PolNo = null;
        }
        if (FCode.equalsIgnoreCase("ManageCom")) {
            if( FValue != null && !FValue.equals(""))
            {
                ManageCom = FValue.trim();
            }
            else
                ManageCom = null;
        }
        if (FCode.equalsIgnoreCase("EdorValiDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                EdorValiDate = FValue.trim();
            }
            else
                EdorValiDate = null;
        }
        if (FCode.equalsIgnoreCase("EdorAppDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                EdorAppDate = FValue.trim();
            }
            else
                EdorAppDate = null;
        }
        if (FCode.equalsIgnoreCase("EdorState")) {
            if( FValue != null && !FValue.equals(""))
            {
                EdorState = FValue.trim();
            }
            else
                EdorState = null;
        }
        if (FCode.equalsIgnoreCase("UWFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                UWFlag = FValue.trim();
            }
            else
                UWFlag = null;
        }
        if (FCode.equalsIgnoreCase("UWOperator")) {
            if( FValue != null && !FValue.equals(""))
            {
                UWOperator = FValue.trim();
            }
            else
                UWOperator = null;
        }
        if (FCode.equalsIgnoreCase("UWDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                UWDate = FValue.trim();
            }
            else
                UWDate = null;
        }
        if (FCode.equalsIgnoreCase("UWTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                UWTime = FValue.trim();
            }
            else
                UWTime = null;
        }
        if (FCode.equalsIgnoreCase("ChgPrem")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                ChgPrem = d;
            }
        }
        if (FCode.equalsIgnoreCase("ChgAmnt")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                ChgAmnt = d;
            }
        }
        if (FCode.equalsIgnoreCase("GetMoney")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                GetMoney = d;
            }
        }
        if (FCode.equalsIgnoreCase("GetInterest")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                GetInterest = d;
            }
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            if( FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
                Operator = null;
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeDate = FValue.trim();
            }
            else
                MakeDate = null;
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
                MakeTime = null;
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyDate = FValue.trim();
            }
            else
                ModifyDate = null;
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
                ModifyTime = null;
        }
        if (FCode.equalsIgnoreCase("AppReason")) {
            if( FValue != null && !FValue.equals(""))
            {
                AppReason = FValue.trim();
            }
            else
                AppReason = null;
        }
        if (FCode.equalsIgnoreCase("EdorReasonCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                EdorReasonCode = FValue.trim();
            }
            else
                EdorReasonCode = null;
        }
        if (FCode.equalsIgnoreCase("EdorReason")) {
            if( FValue != null && !FValue.equals(""))
            {
                EdorReason = FValue.trim();
            }
            else
                EdorReason = null;
        }
        if (FCode.equalsIgnoreCase("ApproveFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                ApproveFlag = FValue.trim();
            }
            else
                ApproveFlag = null;
        }
        if (FCode.equalsIgnoreCase("ApproveOperator")) {
            if( FValue != null && !FValue.equals(""))
            {
                ApproveOperator = FValue.trim();
            }
            else
                ApproveOperator = null;
        }
        if (FCode.equalsIgnoreCase("ApproveDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                ApproveDate = FValue.trim();
            }
            else
                ApproveDate = null;
        }
        if (FCode.equalsIgnoreCase("ApproveTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                ApproveTime = FValue.trim();
            }
            else
                ApproveTime = null;
        }
        if (FCode.equalsIgnoreCase("StandbyFlag1")) {
            if( FValue != null && !FValue.equals(""))
            {
                StandbyFlag1 = FValue.trim();
            }
            else
                StandbyFlag1 = null;
        }
        if (FCode.equalsIgnoreCase("StandbyFlag2")) {
            if( FValue != null && !FValue.equals(""))
            {
                StandbyFlag2 = FValue.trim();
            }
            else
                StandbyFlag2 = null;
        }
        if (FCode.equalsIgnoreCase("StandbyFlag3")) {
            if( FValue != null && !FValue.equals(""))
            {
                StandbyFlag3 = FValue.trim();
            }
            else
                StandbyFlag3 = null;
        }
        if (FCode.equalsIgnoreCase("EdorTypeCal")) {
            if( FValue != null && !FValue.equals(""))
            {
                EdorTypeCal = FValue.trim();
            }
            else
                EdorTypeCal = null;
        }
        if (FCode.equalsIgnoreCase("StandbyFlag4")) {
            if( FValue != null && !FValue.equals(""))
            {
                StandbyFlag4 = FValue.trim();
            }
            else
                StandbyFlag4 = null;
        }
        if (FCode.equalsIgnoreCase("StandbyFlag5")) {
            if( FValue != null && !FValue.equals(""))
            {
                StandbyFlag5 = FValue.trim();
            }
            else
                StandbyFlag5 = null;
        }
        if (FCode.equalsIgnoreCase("StandbyFlag6")) {
            if( FValue != null && !FValue.equals(""))
            {
                StandbyFlag6 = FValue.trim();
            }
            else
                StandbyFlag6 = null;
        }
        if (FCode.equalsIgnoreCase("EdorReasonRemark")) {
            if( FValue != null && !FValue.equals(""))
            {
                EdorReasonRemark = FValue.trim();
            }
            else
                EdorReasonRemark = null;
        }
        if (FCode.equalsIgnoreCase("OACode")) {
            if( FValue != null && !FValue.equals(""))
            {
                OACode = FValue.trim();
            }
            else
                OACode = null;
        }
        return true;
    }


    public String toString() {
    return "LPEdorItemPojo [" +
            "EdorAcceptNo="+EdorAcceptNo +
            ", EdorNo="+EdorNo +
            ", EdorAppNo="+EdorAppNo +
            ", EdorType="+EdorType +
            ", DisplayType="+DisplayType +
            ", GrpContNo="+GrpContNo +
            ", ContNo="+ContNo +
            ", InsuredNo="+InsuredNo +
            ", PolNo="+PolNo +
            ", ManageCom="+ManageCom +
            ", EdorValiDate="+EdorValiDate +
            ", EdorAppDate="+EdorAppDate +
            ", EdorState="+EdorState +
            ", UWFlag="+UWFlag +
            ", UWOperator="+UWOperator +
            ", UWDate="+UWDate +
            ", UWTime="+UWTime +
            ", ChgPrem="+ChgPrem +
            ", ChgAmnt="+ChgAmnt +
            ", GetMoney="+GetMoney +
            ", GetInterest="+GetInterest +
            ", Operator="+Operator +
            ", MakeDate="+MakeDate +
            ", MakeTime="+MakeTime +
            ", ModifyDate="+ModifyDate +
            ", ModifyTime="+ModifyTime +
            ", AppReason="+AppReason +
            ", EdorReasonCode="+EdorReasonCode +
            ", EdorReason="+EdorReason +
            ", ApproveFlag="+ApproveFlag +
            ", ApproveOperator="+ApproveOperator +
            ", ApproveDate="+ApproveDate +
            ", ApproveTime="+ApproveTime +
            ", StandbyFlag1="+StandbyFlag1 +
            ", StandbyFlag2="+StandbyFlag2 +
            ", StandbyFlag3="+StandbyFlag3 +
            ", EdorTypeCal="+EdorTypeCal +
            ", StandbyFlag4="+StandbyFlag4 +
            ", StandbyFlag5="+StandbyFlag5 +
            ", StandbyFlag6="+StandbyFlag6 +
            ", EdorReasonRemark="+EdorReasonRemark +
            ", OACode="+OACode +"]";
    }
}
