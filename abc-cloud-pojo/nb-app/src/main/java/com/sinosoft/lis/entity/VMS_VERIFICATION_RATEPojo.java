/**
 * Copyright (c) 2017 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.entity;

import com.sinosoft.cloud.cache.annotation.RedisPrimaryHKey;
import com.sinosoft.utility.Pojo;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;

import java.io.Serializable;

/**
 * <p>ClassName: VMS_VERIFICATION_RATEPojo </p>
 * <p>Copyright: Copyright (c) 2017</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2017-10-17
 */
public class VMS_VERIFICATION_RATEPojo implements Pojo,Serializable {
    // @Field
    /** 序列号 */
    private String SERIALNO; 
    /** 险种 */
    @RedisPrimaryHKey
    private String RISKCODE; 
    /** 机构 */
    private String MANAGECOM; 
    /** 税率 */
    private double RATE; 
    /** 备用字段1 */
    private String STANDBYFLAG1; 
    /** 创建日期 */
    private String MAKEDATE; 
    /** 创建时间 */
    private String MAKETIME; 


    public static final int FIELDNUM = 7;    // 数据库表的字段个数
    public String getSERIALNO() {
        return SERIALNO;
    }
    public void setSERIALNO(String aSERIALNO) {
        SERIALNO = aSERIALNO;
    }
    public String getRISKCODE() {
        return RISKCODE;
    }
    public void setRISKCODE(String aRISKCODE) {
        RISKCODE = aRISKCODE;
    }
    public String getMANAGECOM() {
        return MANAGECOM;
    }
    public void setMANAGECOM(String aMANAGECOM) {
        MANAGECOM = aMANAGECOM;
    }
    public double getRATE() {
        return RATE;
    }
    public void setRATE(double aRATE) {
        RATE = aRATE;
    }
    public void setRATE(String aRATE) {
        if (aRATE != null && !aRATE.equals("")) {
            Double tDouble = new Double(aRATE);
            double d = tDouble.doubleValue();
            RATE = d;
        }
    }

    public String getSTANDBYFLAG1() {
        return STANDBYFLAG1;
    }
    public void setSTANDBYFLAG1(String aSTANDBYFLAG1) {
        STANDBYFLAG1 = aSTANDBYFLAG1;
    }
    public String getMAKEDATE() {
        return MAKEDATE;
    }
    public void setMAKEDATE(String aMAKEDATE) {
        MAKEDATE = aMAKEDATE;
    }
    public String getMAKETIME() {
        return MAKETIME;
    }
    public void setMAKETIME(String aMAKETIME) {
        MAKETIME = aMAKETIME;
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("SERIALNO") ) {
            return 0;
        }
        if( strFieldName.equals("RISKCODE") ) {
            return 1;
        }
        if( strFieldName.equals("MANAGECOM") ) {
            return 2;
        }
        if( strFieldName.equals("RATE") ) {
            return 3;
        }
        if( strFieldName.equals("STANDBYFLAG1") ) {
            return 4;
        }
        if( strFieldName.equals("MAKEDATE") ) {
            return 5;
        }
        if( strFieldName.equals("MAKETIME") ) {
            return 6;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "SERIALNO";
                break;
            case 1:
                strFieldName = "RISKCODE";
                break;
            case 2:
                strFieldName = "MANAGECOM";
                break;
            case 3:
                strFieldName = "RATE";
                break;
            case 4:
                strFieldName = "STANDBYFLAG1";
                break;
            case 5:
                strFieldName = "MAKEDATE";
                break;
            case 6:
                strFieldName = "MAKETIME";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "SERIALNO":
                return Schema.TYPE_STRING;
            case "RISKCODE":
                return Schema.TYPE_STRING;
            case "MANAGECOM":
                return Schema.TYPE_STRING;
            case "RATE":
                return Schema.TYPE_DOUBLE;
            case "STANDBYFLAG1":
                return Schema.TYPE_STRING;
            case "MAKEDATE":
                return Schema.TYPE_STRING;
            case "MAKETIME":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_STRING;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_DOUBLE;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_STRING;
            case 6:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("SERIALNO")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SERIALNO));
        }
        if (FCode.equalsIgnoreCase("RISKCODE")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RISKCODE));
        }
        if (FCode.equalsIgnoreCase("MANAGECOM")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MANAGECOM));
        }
        if (FCode.equalsIgnoreCase("RATE")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RATE));
        }
        if (FCode.equalsIgnoreCase("STANDBYFLAG1")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(STANDBYFLAG1));
        }
        if (FCode.equalsIgnoreCase("MAKEDATE")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MAKEDATE));
        }
        if (FCode.equalsIgnoreCase("MAKETIME")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MAKETIME));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = String.valueOf(SERIALNO);
                break;
            case 1:
                strFieldValue = String.valueOf(RISKCODE);
                break;
            case 2:
                strFieldValue = String.valueOf(MANAGECOM);
                break;
            case 3:
                strFieldValue = String.valueOf(RATE);
                break;
            case 4:
                strFieldValue = String.valueOf(STANDBYFLAG1);
                break;
            case 5:
                strFieldValue = String.valueOf(MAKEDATE);
                break;
            case 6:
                strFieldValue = String.valueOf(MAKETIME);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("SERIALNO")) {
            if( FValue != null && !FValue.equals(""))
            {
                SERIALNO = FValue.trim();
            }
            else
                SERIALNO = null;
        }
        if (FCode.equalsIgnoreCase("RISKCODE")) {
            if( FValue != null && !FValue.equals(""))
            {
                RISKCODE = FValue.trim();
            }
            else
                RISKCODE = null;
        }
        if (FCode.equalsIgnoreCase("MANAGECOM")) {
            if( FValue != null && !FValue.equals(""))
            {
                MANAGECOM = FValue.trim();
            }
            else
                MANAGECOM = null;
        }
        if (FCode.equalsIgnoreCase("RATE")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                RATE = d;
            }
        }
        if (FCode.equalsIgnoreCase("STANDBYFLAG1")) {
            if( FValue != null && !FValue.equals(""))
            {
                STANDBYFLAG1 = FValue.trim();
            }
            else
                STANDBYFLAG1 = null;
        }
        if (FCode.equalsIgnoreCase("MAKEDATE")) {
            if( FValue != null && !FValue.equals(""))
            {
                MAKEDATE = FValue.trim();
            }
            else
                MAKEDATE = null;
        }
        if (FCode.equalsIgnoreCase("MAKETIME")) {
            if( FValue != null && !FValue.equals(""))
            {
                MAKETIME = FValue.trim();
            }
            else
                MAKETIME = null;
        }
        return true;
    }


    public String toString() {
    return "VMS_VERIFICATION_RATEPojo [" +
            "SERIALNO="+SERIALNO +
            ", RISKCODE="+RISKCODE +
            ", MANAGECOM="+MANAGECOM +
            ", RATE="+RATE +
            ", STANDBYFLAG1="+STANDBYFLAG1 +
            ", MAKEDATE="+MAKEDATE +
            ", MAKETIME="+MAKETIME +"]";
    }
}
