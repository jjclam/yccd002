/**
 * Copyright (c) 2020 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.vdb;

import java.sql.*;
import com.sinosoft.lis.schema.surrendValueSchema;
import com.sinosoft.lis.vschema.surrendValueSet;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;

/**
 * <p>ClassName: surrendValueDBSet </p>
 * <p>Description: DB层多记录数据库操作类文件 </p>
 * <p>Copyright: Copyright (c) 2020</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2020-01-14
 */
public class surrendValueDBSet extends surrendValueSet {
    // @Field
    private Connection con;
    private DBOper db;
    /**
    * flag = true: 传入Connection
    * flag = false: 不传入Connection
    **/
    private boolean mflag = false;

    // @Constructor
    public surrendValueDBSet(Connection tConnection) {
        con = tConnection;
        db = new DBOper(con,"surrendValue");
        mflag = true;
    }

    public surrendValueDBSet() {
        db = new DBOper( "surrendValue" );
    }
    // @Method
    public boolean deleteSQL() {
        if (db.deleteSQL(this)) {
            return true;
        } else {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "surrendValueDBSet";
            tError.functionName = "deleteSQL";
            tError.errorMessage = "操作失败!";
            this.mErrors .addOneError(tError);
            return false;
        }
    }

    /**
     * 删除操作
     * 删除条件：主键
     * @return boolean
     */
    public boolean delete() {
        PreparedStatement pstmt = null;

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try {
            int tCount = this.size();
            pstmt = con.prepareStatement("DELETE FROM surrendValue WHERE  1=1 ");
            for (int i = 1; i <= tCount; i++) {
                pstmt.addBatch();
            }
            pstmt.executeBatch();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            ex.printStackTrace();
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "surrendValueDBSet";
            tError.functionName = "delete()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            try {
                pstmt.close();
            } catch (Exception e){
                e.printStackTrace();
            }

            if (!mflag) {
                try {
                    con.close();
                } catch (Exception e){e.printStackTrace();}
            }

            return false;
        }

        if( !mflag ) {
            try {
                con.close();
            } catch (Exception e){e.printStackTrace();}
        }

        return true;
    }

    /**
     * 更新操作
     * 更新条件：主键
     * @return boolean
     */
    public boolean update() {
        PreparedStatement pstmt = null;

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try {
            int tCount = this.size();
            pstmt = con.prepareStatement("UPDATE surrendValue SET  reqMsgId = ? , instSerialNo = ? , sellType = ? , endorseNo = ? , endorseCreateTime = ? , endorseTime = ? , endorseReason = ? , endorseReasonDesc = ? , endorseFee = ? , contNo = ? , policyNo = ? , productCode = ? , riskCode = ? , surrenderReason = ? , hesitationType = ? , inAccount = ? , outAccount = ? , payTime = ? , fee = ? , payFlowId = ? , discountFee = ? WHERE  1=1 ");
            for (int i = 1; i <= tCount; i++) {
            if(this.get(i).getReqMsgId() == null || this.get(i).getReqMsgId().equals("null")) {
            	pstmt.setString(1,null);
            } else {
            	pstmt.setString(1, this.get(i).getReqMsgId());
            }
            if(this.get(i).getInstSerialNo() == null || this.get(i).getInstSerialNo().equals("null")) {
            	pstmt.setString(2,null);
            } else {
            	pstmt.setString(2, this.get(i).getInstSerialNo());
            }
            if(this.get(i).getSellType() == null || this.get(i).getSellType().equals("null")) {
            	pstmt.setString(3,null);
            } else {
            	pstmt.setString(3, this.get(i).getSellType());
            }
            if(this.get(i).getEndorseNo() == null || this.get(i).getEndorseNo().equals("null")) {
            	pstmt.setString(4,null);
            } else {
            	pstmt.setString(4, this.get(i).getEndorseNo());
            }
            if(this.get(i).getEndorseCreateTime() == null || this.get(i).getEndorseCreateTime().equals("null")) {
                pstmt.setDate(5,null);
            } else {
                pstmt.setDate(5, Date.valueOf(this.get(i).getEndorseCreateTime()));
            }
            if(this.get(i).getEndorseTime() == null || this.get(i).getEndorseTime().equals("null")) {
                pstmt.setDate(6,null);
            } else {
                pstmt.setDate(6, Date.valueOf(this.get(i).getEndorseTime()));
            }
            if(this.get(i).getEndorseReason() == null || this.get(i).getEndorseReason().equals("null")) {
            	pstmt.setString(7,null);
            } else {
            	pstmt.setString(7, this.get(i).getEndorseReason());
            }
            if(this.get(i).getEndorseReasonDesc() == null || this.get(i).getEndorseReasonDesc().equals("null")) {
            	pstmt.setString(8,null);
            } else {
            	pstmt.setString(8, this.get(i).getEndorseReasonDesc());
            }
            if(this.get(i).getEndorseFee() == null || this.get(i).getEndorseFee().equals("null")) {
            	pstmt.setString(9,null);
            } else {
            	pstmt.setString(9, this.get(i).getEndorseFee());
            }
            if(this.get(i).getContNo() == null || this.get(i).getContNo().equals("null")) {
            	pstmt.setString(10,null);
            } else {
            	pstmt.setString(10, this.get(i).getContNo());
            }
            if(this.get(i).getPolicyNo() == null || this.get(i).getPolicyNo().equals("null")) {
            	pstmt.setString(11,null);
            } else {
            	pstmt.setString(11, this.get(i).getPolicyNo());
            }
            if(this.get(i).getProductCode() == null || this.get(i).getProductCode().equals("null")) {
            	pstmt.setString(12,null);
            } else {
            	pstmt.setString(12, this.get(i).getProductCode());
            }
            if(this.get(i).getRiskCode() == null || this.get(i).getRiskCode().equals("null")) {
            	pstmt.setString(13,null);
            } else {
            	pstmt.setString(13, this.get(i).getRiskCode());
            }
            if(this.get(i).getSurrenderReason() == null || this.get(i).getSurrenderReason().equals("null")) {
            	pstmt.setString(14,null);
            } else {
            	pstmt.setString(14, this.get(i).getSurrenderReason());
            }
            if(this.get(i).getHesitationType() == null || this.get(i).getHesitationType().equals("null")) {
            	pstmt.setString(15,null);
            } else {
            	pstmt.setString(15, this.get(i).getHesitationType());
            }
            if(this.get(i).getInAccount() == null || this.get(i).getInAccount().equals("null")) {
            	pstmt.setString(16,null);
            } else {
            	pstmt.setString(16, this.get(i).getInAccount());
            }
            if(this.get(i).getOutAccount() == null || this.get(i).getOutAccount().equals("null")) {
            	pstmt.setString(17,null);
            } else {
            	pstmt.setString(17, this.get(i).getOutAccount());
            }
            if(this.get(i).getPayTime() == null || this.get(i).getPayTime().equals("null")) {
                pstmt.setDate(18,null);
            } else {
                pstmt.setDate(18, Date.valueOf(this.get(i).getPayTime()));
            }
            if(this.get(i).getFee() == null || this.get(i).getFee().equals("null")) {
            	pstmt.setString(19,null);
            } else {
            	pstmt.setString(19, this.get(i).getFee());
            }
            if(this.get(i).getPayFlowId() == null || this.get(i).getPayFlowId().equals("null")) {
            	pstmt.setString(20,null);
            } else {
            	pstmt.setString(20, this.get(i).getPayFlowId());
            }
            if(this.get(i).getDiscountFee() == null || this.get(i).getDiscountFee().equals("null")) {
            	pstmt.setString(21,null);
            } else {
            	pstmt.setString(21, this.get(i).getDiscountFee());
            }
            // set where condition
                pstmt.addBatch();
            }
            pstmt.executeBatch();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            ex.printStackTrace();
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "surrendValueDBSet";
            tError.functionName = "update()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            try {
                pstmt.close();
            } catch (Exception e){e.printStackTrace();}

            if( !mflag ) {
                try {
                    con.close();
                } catch (Exception e){e.printStackTrace();}
            }

            return false;
        }

        if( !mflag ) {
            try {
                con.close();
            } catch (Exception e){e.printStackTrace();}
        }

        return true;
    }

    /**
     * 新增操作
     * @return boolean
     */
    public boolean insert() {
        PreparedStatement pstmt = null;

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try
        {
            int tCount = this.size();
            pstmt = con.prepareStatement("INSERT INTO surrendValue VALUES( ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ?)");
            for (int i = 1; i <= tCount; i++) {
            if(this.get(i).getReqMsgId() == null || this.get(i).getReqMsgId().equals("null")) {
            	pstmt.setString(1,null);
            } else {
            	pstmt.setString(1, this.get(i).getReqMsgId());
            }
            if(this.get(i).getInstSerialNo() == null || this.get(i).getInstSerialNo().equals("null")) {
            	pstmt.setString(2,null);
            } else {
            	pstmt.setString(2, this.get(i).getInstSerialNo());
            }
            if(this.get(i).getSellType() == null || this.get(i).getSellType().equals("null")) {
            	pstmt.setString(3,null);
            } else {
            	pstmt.setString(3, this.get(i).getSellType());
            }
            if(this.get(i).getEndorseNo() == null || this.get(i).getEndorseNo().equals("null")) {
            	pstmt.setString(4,null);
            } else {
            	pstmt.setString(4, this.get(i).getEndorseNo());
            }
            if(this.get(i).getEndorseCreateTime() == null || this.get(i).getEndorseCreateTime().equals("null")) {
                pstmt.setDate(5,null);
            } else {
                pstmt.setDate(5, Date.valueOf(this.get(i).getEndorseCreateTime()));
            }
            if(this.get(i).getEndorseTime() == null || this.get(i).getEndorseTime().equals("null")) {
                pstmt.setDate(6,null);
            } else {
                pstmt.setDate(6, Date.valueOf(this.get(i).getEndorseTime()));
            }
            if(this.get(i).getEndorseReason() == null || this.get(i).getEndorseReason().equals("null")) {
            	pstmt.setString(7,null);
            } else {
            	pstmt.setString(7, this.get(i).getEndorseReason());
            }
            if(this.get(i).getEndorseReasonDesc() == null || this.get(i).getEndorseReasonDesc().equals("null")) {
            	pstmt.setString(8,null);
            } else {
            	pstmt.setString(8, this.get(i).getEndorseReasonDesc());
            }
            if(this.get(i).getEndorseFee() == null || this.get(i).getEndorseFee().equals("null")) {
            	pstmt.setString(9,null);
            } else {
            	pstmt.setString(9, this.get(i).getEndorseFee());
            }
            if(this.get(i).getContNo() == null || this.get(i).getContNo().equals("null")) {
            	pstmt.setString(10,null);
            } else {
            	pstmt.setString(10, this.get(i).getContNo());
            }
            if(this.get(i).getPolicyNo() == null || this.get(i).getPolicyNo().equals("null")) {
            	pstmt.setString(11,null);
            } else {
            	pstmt.setString(11, this.get(i).getPolicyNo());
            }
            if(this.get(i).getProductCode() == null || this.get(i).getProductCode().equals("null")) {
            	pstmt.setString(12,null);
            } else {
            	pstmt.setString(12, this.get(i).getProductCode());
            }
            if(this.get(i).getRiskCode() == null || this.get(i).getRiskCode().equals("null")) {
            	pstmt.setString(13,null);
            } else {
            	pstmt.setString(13, this.get(i).getRiskCode());
            }
            if(this.get(i).getSurrenderReason() == null || this.get(i).getSurrenderReason().equals("null")) {
            	pstmt.setString(14,null);
            } else {
            	pstmt.setString(14, this.get(i).getSurrenderReason());
            }
            if(this.get(i).getHesitationType() == null || this.get(i).getHesitationType().equals("null")) {
            	pstmt.setString(15,null);
            } else {
            	pstmt.setString(15, this.get(i).getHesitationType());
            }
            if(this.get(i).getInAccount() == null || this.get(i).getInAccount().equals("null")) {
            	pstmt.setString(16,null);
            } else {
            	pstmt.setString(16, this.get(i).getInAccount());
            }
            if(this.get(i).getOutAccount() == null || this.get(i).getOutAccount().equals("null")) {
            	pstmt.setString(17,null);
            } else {
            	pstmt.setString(17, this.get(i).getOutAccount());
            }
            if(this.get(i).getPayTime() == null || this.get(i).getPayTime().equals("null")) {
                pstmt.setDate(18,null);
            } else {
                pstmt.setDate(18, Date.valueOf(this.get(i).getPayTime()));
            }
            if(this.get(i).getFee() == null || this.get(i).getFee().equals("null")) {
            	pstmt.setString(19,null);
            } else {
            	pstmt.setString(19, this.get(i).getFee());
            }
            if(this.get(i).getPayFlowId() == null || this.get(i).getPayFlowId().equals("null")) {
            	pstmt.setString(20,null);
            } else {
            	pstmt.setString(20, this.get(i).getPayFlowId());
            }
            if(this.get(i).getDiscountFee() == null || this.get(i).getDiscountFee().equals("null")) {
            	pstmt.setString(21,null);
            } else {
            	pstmt.setString(21, this.get(i).getDiscountFee());
            }
                pstmt.addBatch();
            }
            pstmt.executeBatch();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            ex.printStackTrace();
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "surrendValueDBSet";
            tError.functionName = "insert()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            try {
                pstmt.close();
            } catch (Exception e){e.printStackTrace();}

            if( !mflag ) {
                try {
                    con.close();
                } catch (Exception e){e.printStackTrace();}
            }

            return false;
        }

        if( !mflag ) {
            try {
                con.close();
            } catch (Exception e){e.printStackTrace();}
        }

        return true;
    }
}
