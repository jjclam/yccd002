/**
 * Copyright (c) 2018 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.db;

import com.sinosoft.lis.schema.LBGrpContSchema;
import com.sinosoft.lis.vschema.LBGrpContSet;
import com.sinosoft.utility.*;

import java.sql.*;

/**
 * <p>ClassName: LBGrpContDB </p>
 * <p>Description: DB层数据库操作类文件 </p>
 * <p>Copyright: Copyright (c) 2018</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2018-05-23
 */
public class LBGrpContDB extends LBGrpContSchema {
    // @Field
    private Connection con;
    private DBOper db;
    /**
    * flag = true: 传入Connection
    * flag = false: 不传入Connection
    **/
    private boolean mflag = false;

    public CErrors mErrors = new CErrors();        // 错误信息

    /**
     * 为批量操作而准备的语句和游标对象
     */
    private ResultSet mResultSet = null;
    private Statement mStatement = null;
    // @Constructor
    public LBGrpContDB( Connection tConnection ) {
        con = tConnection;
        db = new DBOper( con, "LBGrpCont" );
        mflag = true;
    }

    public LBGrpContDB() {
        con = null;
        db = new DBOper( "LBGrpCont" );
        mflag = false;
    }

    // @Method
    public boolean deleteSQL() {
        LBGrpContSchema tSchema = this.getSchema();
        if (db.deleteSQL(tSchema)) {
             return true;
        } else {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LBGrpContDB";
            tError.functionName = "deleteSQL";
            tError.errorMessage = "操作失败!";
            this.mErrors .addOneError(tError);
            return false;
        }
    }

    public int getCount() {
        LBGrpContSchema tSchema = this.getSchema();

        int tCount = db.getCount(tSchema);
        if (tCount < 0) {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LBGrpContDB";
            tError.functionName = "getCount";
            tError.errorMessage = "操作失败!";
            this.mErrors .addOneError(tError);

            return -1;
        }

        return tCount;
    }

    public boolean delete() {
        PreparedStatement pstmt = null;

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try {
            pstmt = con.prepareStatement("DELETE FROM LBGrpCont WHERE  1=1  AND GrpContNo = ?");
            if(this.getGrpContNo() == null || this.getGrpContNo().equals("null")) {
            	pstmt.setNull(1, 12);
            } else {
            	pstmt.setString(1, this.getGrpContNo());
            }
            pstmt.executeUpdate();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LBGrpContDB";
            tError.functionName = "delete()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);
            ex.printStackTrace();

            try {
                pstmt.close();
            } catch (Exception e){}

            if(!mflag) {
                try {
                    con.close();
                } catch (Exception e){}
            }

            return false;
        }

        if(!mflag) {
            try {
                con.close();
            } catch (Exception e){}
        }

        return true;
    }

    public boolean update() {
        PreparedStatement pstmt = null;

        SQLString sqlObj = new SQLString("LBGrpCont");
        sqlObj.setSQL(2, this);
        String sql = sqlObj.getSQL();

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try {
            pstmt = con.prepareStatement("UPDATE LBGrpCont SET  GrpContNo = ? , ProposalGrpContNo = ? , PrtNo = ? , SaleChnl = ? , SalesWay = ? , SalesWaySub = ? , BizNature = ? , PolIssuPlat = ? , ContType = ? , AgentCom = ? , AgentCode = ? , AgentGroup = ? , AgentCode1 = ? , AppntNo = ? , Peoples2 = ? , GrpName = ? , DisputedFlag = ? , OutPayFlag = ? , Lang = ? , Currency = ? , LostTimes = ? , PrintCount = ? , LastEdorDate = ? , SpecFlag = ? , SignCom = ? , SignDate = ? , SignTime = ? , CValiDate = ? , PayIntv = ? , ManageFeeRate = ? , ExpPeoples = ? , ExpPremium = ? , ExpAmnt = ? , Peoples = ? , Mult = ? , Prem = ? , Amnt = ? , SumPrem = ? , SumPay = ? , Dif = ? , StandbyFlag1 = ? , StandbyFlag2 = ? , StandbyFlag3 = ? , InputOperator = ? , InputDate = ? , InputTime = ? , ApproveFlag = ? , ApproveCode = ? , ApproveDate = ? , ApproveTime = ? , UWOperator = ? , UWFlag = ? , UWDate = ? , UWTime = ? , AppFlag = ? , State = ? , PolApplyDate = ? , CustomGetPolDate = ? , GetPolDate = ? , ManageCom = ? , ComCode = ? , Operator = ? , MakeDate = ? , MakeTime = ? , ModifyOperator = ? , ModifyDate = ? , ModifyTime = ? , EnterKind = ? , AmntGrade = ? , FirstTrialOperator = ? , FirstTrialDate = ? , FirstTrialTime = ? , ReceiveOperator = ? , ReceiveDate = ? , ReceiveTime = ? , MarketType = ? , ReportNo = ? , EdorCalType = ? , RenewFlag = ? , RenewContNo = ? , EndDate = ? , Coinsurance = ? , CardTypeCode = ? , RelationToappnt = ? , NewReinsureFlag = ? , InsureRate = ? , AgcAgentCode = ? , AgcAgentName = ? , AvailiaDateFlag = ? , ComBusType = ? , PrintFlag = ? , SpecAcceptFlag = ? , PolicyType = ? , AchvAccruFlag = ? , Remark = ? , ValDateType = ? , SecLevelOther = ? , TBType = ? , SlipForm = ? WHERE  1=1  AND GrpContNo = ?");
            if(this.getGrpContNo() == null || this.getGrpContNo().equals("null")) {
            	pstmt.setNull(1, 12);
            } else {
            	pstmt.setString(1, this.getGrpContNo());
            }
            if(this.getProposalGrpContNo() == null || this.getProposalGrpContNo().equals("null")) {
            	pstmt.setNull(2, 12);
            } else {
            	pstmt.setString(2, this.getProposalGrpContNo());
            }
            if(this.getPrtNo() == null || this.getPrtNo().equals("null")) {
            	pstmt.setNull(3, 12);
            } else {
            	pstmt.setString(3, this.getPrtNo());
            }
            if(this.getSaleChnl() == null || this.getSaleChnl().equals("null")) {
            	pstmt.setNull(4, 12);
            } else {
            	pstmt.setString(4, this.getSaleChnl());
            }
            if(this.getSalesWay() == null || this.getSalesWay().equals("null")) {
            	pstmt.setNull(5, 12);
            } else {
            	pstmt.setString(5, this.getSalesWay());
            }
            if(this.getSalesWaySub() == null || this.getSalesWaySub().equals("null")) {
            	pstmt.setNull(6, 12);
            } else {
            	pstmt.setString(6, this.getSalesWaySub());
            }
            if(this.getBizNature() == null || this.getBizNature().equals("null")) {
            	pstmt.setNull(7, 12);
            } else {
            	pstmt.setString(7, this.getBizNature());
            }
            if(this.getPolIssuPlat() == null || this.getPolIssuPlat().equals("null")) {
            	pstmt.setNull(8, 12);
            } else {
            	pstmt.setString(8, this.getPolIssuPlat());
            }
            if(this.getContType() == null || this.getContType().equals("null")) {
            	pstmt.setNull(9, 12);
            } else {
            	pstmt.setString(9, this.getContType());
            }
            if(this.getAgentCom() == null || this.getAgentCom().equals("null")) {
            	pstmt.setNull(10, 12);
            } else {
            	pstmt.setString(10, this.getAgentCom());
            }
            if(this.getAgentCode() == null || this.getAgentCode().equals("null")) {
            	pstmt.setNull(11, 12);
            } else {
            	pstmt.setString(11, this.getAgentCode());
            }
            if(this.getAgentGroup() == null || this.getAgentGroup().equals("null")) {
            	pstmt.setNull(12, 12);
            } else {
            	pstmt.setString(12, this.getAgentGroup());
            }
            if(this.getAgentCode1() == null || this.getAgentCode1().equals("null")) {
            	pstmt.setNull(13, 12);
            } else {
            	pstmt.setString(13, this.getAgentCode1());
            }
            if(this.getAppntNo() == null || this.getAppntNo().equals("null")) {
            	pstmt.setNull(14, 12);
            } else {
            	pstmt.setString(14, this.getAppntNo());
            }
            pstmt.setInt(15, this.getPeoples2());
            if(this.getGrpName() == null || this.getGrpName().equals("null")) {
            	pstmt.setNull(16, 12);
            } else {
            	pstmt.setString(16, this.getGrpName());
            }
            if(this.getDisputedFlag() == null || this.getDisputedFlag().equals("null")) {
            	pstmt.setNull(17, 12);
            } else {
            	pstmt.setString(17, this.getDisputedFlag());
            }
            if(this.getOutPayFlag() == null || this.getOutPayFlag().equals("null")) {
            	pstmt.setNull(18, 12);
            } else {
            	pstmt.setString(18, this.getOutPayFlag());
            }
            if(this.getLang() == null || this.getLang().equals("null")) {
            	pstmt.setNull(19, 12);
            } else {
            	pstmt.setString(19, this.getLang());
            }
            if(this.getCurrency() == null || this.getCurrency().equals("null")) {
            	pstmt.setNull(20, 12);
            } else {
            	pstmt.setString(20, this.getCurrency());
            }
            pstmt.setInt(21, this.getLostTimes());
            pstmt.setInt(22, this.getPrintCount());
            if(this.getLastEdorDate() == null || this.getLastEdorDate().equals("null")) {
            	pstmt.setNull(23, 93);
            } else {
            	pstmt.setDate(23, Date.valueOf(this.getLastEdorDate()));
            }
            if(this.getSpecFlag() == null || this.getSpecFlag().equals("null")) {
            	pstmt.setNull(24, 12);
            } else {
            	pstmt.setString(24, this.getSpecFlag());
            }
            if(this.getSignCom() == null || this.getSignCom().equals("null")) {
            	pstmt.setNull(25, 12);
            } else {
            	pstmt.setString(25, this.getSignCom());
            }
            if(this.getSignDate() == null || this.getSignDate().equals("null")) {
            	pstmt.setNull(26, 93);
            } else {
            	pstmt.setDate(26, Date.valueOf(this.getSignDate()));
            }
            if(this.getSignTime() == null || this.getSignTime().equals("null")) {
            	pstmt.setNull(27, 12);
            } else {
            	pstmt.setString(27, this.getSignTime());
            }
            if(this.getCValiDate() == null || this.getCValiDate().equals("null")) {
            	pstmt.setNull(28, 93);
            } else {
            	pstmt.setDate(28, Date.valueOf(this.getCValiDate()));
            }
            pstmt.setInt(29, this.getPayIntv());
            pstmt.setDouble(30, this.getManageFeeRate());
            pstmt.setInt(31, this.getExpPeoples());
            pstmt.setDouble(32, this.getExpPremium());
            pstmt.setDouble(33, this.getExpAmnt());
            pstmt.setInt(34, this.getPeoples());
            pstmt.setDouble(35, this.getMult());
            pstmt.setDouble(36, this.getPrem());
            pstmt.setDouble(37, this.getAmnt());
            pstmt.setDouble(38, this.getSumPrem());
            pstmt.setDouble(39, this.getSumPay());
            pstmt.setDouble(40, this.getDif());
            if(this.getStandbyFlag1() == null || this.getStandbyFlag1().equals("null")) {
            	pstmt.setNull(41, 12);
            } else {
            	pstmt.setString(41, this.getStandbyFlag1());
            }
            if(this.getStandbyFlag2() == null || this.getStandbyFlag2().equals("null")) {
            	pstmt.setNull(42, 12);
            } else {
            	pstmt.setString(42, this.getStandbyFlag2());
            }
            if(this.getStandbyFlag3() == null || this.getStandbyFlag3().equals("null")) {
            	pstmt.setNull(43, 12);
            } else {
            	pstmt.setString(43, this.getStandbyFlag3());
            }
            if(this.getInputOperator() == null || this.getInputOperator().equals("null")) {
            	pstmt.setNull(44, 12);
            } else {
            	pstmt.setString(44, this.getInputOperator());
            }
            if(this.getInputDate() == null || this.getInputDate().equals("null")) {
            	pstmt.setNull(45, 93);
            } else {
            	pstmt.setDate(45, Date.valueOf(this.getInputDate()));
            }
            if(this.getInputTime() == null || this.getInputTime().equals("null")) {
            	pstmt.setNull(46, 12);
            } else {
            	pstmt.setString(46, this.getInputTime());
            }
            if(this.getApproveFlag() == null || this.getApproveFlag().equals("null")) {
            	pstmt.setNull(47, 12);
            } else {
            	pstmt.setString(47, this.getApproveFlag());
            }
            if(this.getApproveCode() == null || this.getApproveCode().equals("null")) {
            	pstmt.setNull(48, 12);
            } else {
            	pstmt.setString(48, this.getApproveCode());
            }
            if(this.getApproveDate() == null || this.getApproveDate().equals("null")) {
            	pstmt.setNull(49, 93);
            } else {
            	pstmt.setDate(49, Date.valueOf(this.getApproveDate()));
            }
            if(this.getApproveTime() == null || this.getApproveTime().equals("null")) {
            	pstmt.setNull(50, 12);
            } else {
            	pstmt.setString(50, this.getApproveTime());
            }
            if(this.getUWOperator() == null || this.getUWOperator().equals("null")) {
            	pstmt.setNull(51, 12);
            } else {
            	pstmt.setString(51, this.getUWOperator());
            }
            if(this.getUWFlag() == null || this.getUWFlag().equals("null")) {
            	pstmt.setNull(52, 12);
            } else {
            	pstmt.setString(52, this.getUWFlag());
            }
            if(this.getUWDate() == null || this.getUWDate().equals("null")) {
            	pstmt.setNull(53, 93);
            } else {
            	pstmt.setDate(53, Date.valueOf(this.getUWDate()));
            }
            if(this.getUWTime() == null || this.getUWTime().equals("null")) {
            	pstmt.setNull(54, 12);
            } else {
            	pstmt.setString(54, this.getUWTime());
            }
            if(this.getAppFlag() == null || this.getAppFlag().equals("null")) {
            	pstmt.setNull(55, 12);
            } else {
            	pstmt.setString(55, this.getAppFlag());
            }
            if(this.getState() == null || this.getState().equals("null")) {
            	pstmt.setNull(56, 12);
            } else {
            	pstmt.setString(56, this.getState());
            }
            if(this.getPolApplyDate() == null || this.getPolApplyDate().equals("null")) {
            	pstmt.setNull(57, 93);
            } else {
            	pstmt.setDate(57, Date.valueOf(this.getPolApplyDate()));
            }
            if(this.getCustomGetPolDate() == null || this.getCustomGetPolDate().equals("null")) {
            	pstmt.setNull(58, 93);
            } else {
            	pstmt.setDate(58, Date.valueOf(this.getCustomGetPolDate()));
            }
            if(this.getGetPolDate() == null || this.getGetPolDate().equals("null")) {
            	pstmt.setNull(59, 93);
            } else {
            	pstmt.setDate(59, Date.valueOf(this.getGetPolDate()));
            }
            if(this.getManageCom() == null || this.getManageCom().equals("null")) {
            	pstmt.setNull(60, 12);
            } else {
            	pstmt.setString(60, this.getManageCom());
            }
            if(this.getComCode() == null || this.getComCode().equals("null")) {
            	pstmt.setNull(61, 12);
            } else {
            	pstmt.setString(61, this.getComCode());
            }
            if(this.getOperator() == null || this.getOperator().equals("null")) {
            	pstmt.setNull(62, 12);
            } else {
            	pstmt.setString(62, this.getOperator());
            }
            if(this.getMakeDate() == null || this.getMakeDate().equals("null")) {
            	pstmt.setNull(63, 93);
            } else {
            	pstmt.setDate(63, Date.valueOf(this.getMakeDate()));
            }
            if(this.getMakeTime() == null || this.getMakeTime().equals("null")) {
            	pstmt.setNull(64, 12);
            } else {
            	pstmt.setString(64, this.getMakeTime());
            }
            if(this.getModifyOperator() == null || this.getModifyOperator().equals("null")) {
            	pstmt.setNull(65, 12);
            } else {
            	pstmt.setString(65, this.getModifyOperator());
            }
            if(this.getModifyDate() == null || this.getModifyDate().equals("null")) {
            	pstmt.setNull(66, 93);
            } else {
            	pstmt.setDate(66, Date.valueOf(this.getModifyDate()));
            }
            if(this.getModifyTime() == null || this.getModifyTime().equals("null")) {
            	pstmt.setNull(67, 12);
            } else {
            	pstmt.setString(67, this.getModifyTime());
            }
            if(this.getEnterKind() == null || this.getEnterKind().equals("null")) {
            	pstmt.setNull(68, 12);
            } else {
            	pstmt.setString(68, this.getEnterKind());
            }
            if(this.getAmntGrade() == null || this.getAmntGrade().equals("null")) {
            	pstmt.setNull(69, 12);
            } else {
            	pstmt.setString(69, this.getAmntGrade());
            }
            if(this.getFirstTrialOperator() == null || this.getFirstTrialOperator().equals("null")) {
            	pstmt.setNull(70, 12);
            } else {
            	pstmt.setString(70, this.getFirstTrialOperator());
            }
            if(this.getFirstTrialDate() == null || this.getFirstTrialDate().equals("null")) {
            	pstmt.setNull(71, 93);
            } else {
            	pstmt.setDate(71, Date.valueOf(this.getFirstTrialDate()));
            }
            if(this.getFirstTrialTime() == null || this.getFirstTrialTime().equals("null")) {
            	pstmt.setNull(72, 12);
            } else {
            	pstmt.setString(72, this.getFirstTrialTime());
            }
            if(this.getReceiveOperator() == null || this.getReceiveOperator().equals("null")) {
            	pstmt.setNull(73, 12);
            } else {
            	pstmt.setString(73, this.getReceiveOperator());
            }
            if(this.getReceiveDate() == null || this.getReceiveDate().equals("null")) {
            	pstmt.setNull(74, 93);
            } else {
            	pstmt.setDate(74, Date.valueOf(this.getReceiveDate()));
            }
            if(this.getReceiveTime() == null || this.getReceiveTime().equals("null")) {
            	pstmt.setNull(75, 12);
            } else {
            	pstmt.setString(75, this.getReceiveTime());
            }
            if(this.getMarketType() == null || this.getMarketType().equals("null")) {
            	pstmt.setNull(76, 12);
            } else {
            	pstmt.setString(76, this.getMarketType());
            }
            if(this.getReportNo() == null || this.getReportNo().equals("null")) {
            	pstmt.setNull(77, 12);
            } else {
            	pstmt.setString(77, this.getReportNo());
            }
            if(this.getEdorCalType() == null || this.getEdorCalType().equals("null")) {
            	pstmt.setNull(78, 12);
            } else {
            	pstmt.setString(78, this.getEdorCalType());
            }
            if(this.getRenewFlag() == null || this.getRenewFlag().equals("null")) {
            	pstmt.setNull(79, 12);
            } else {
            	pstmt.setString(79, this.getRenewFlag());
            }
            if(this.getRenewContNo() == null || this.getRenewContNo().equals("null")) {
            	pstmt.setNull(80, 12);
            } else {
            	pstmt.setString(80, this.getRenewContNo());
            }
            if(this.getEndDate() == null || this.getEndDate().equals("null")) {
            	pstmt.setNull(81, 93);
            } else {
            	pstmt.setDate(81, Date.valueOf(this.getEndDate()));
            }
            if(this.getCoinsurance() == null || this.getCoinsurance().equals("null")) {
            	pstmt.setNull(82, 12);
            } else {
            	pstmt.setString(82, this.getCoinsurance());
            }
            if(this.getCardTypeCode() == null || this.getCardTypeCode().equals("null")) {
            	pstmt.setNull(83, 12);
            } else {
            	pstmt.setString(83, this.getCardTypeCode());
            }
            if(this.getRelationToappnt() == null || this.getRelationToappnt().equals("null")) {
            	pstmt.setNull(84, 12);
            } else {
            	pstmt.setString(84, this.getRelationToappnt());
            }
            if(this.getNewReinsureFlag() == null || this.getNewReinsureFlag().equals("null")) {
            	pstmt.setNull(85, 12);
            } else {
            	pstmt.setString(85, this.getNewReinsureFlag());
            }
            pstmt.setLong(86, this.getInsureRate());
            if(this.getAgcAgentCode() == null || this.getAgcAgentCode().equals("null")) {
            	pstmt.setNull(87, 12);
            } else {
            	pstmt.setString(87, this.getAgcAgentCode());
            }
            if(this.getAgcAgentName() == null || this.getAgcAgentName().equals("null")) {
            	pstmt.setNull(88, 12);
            } else {
            	pstmt.setString(88, this.getAgcAgentName());
            }
            if(this.getAvailiaDateFlag() == null || this.getAvailiaDateFlag().equals("null")) {
            	pstmt.setNull(89, 12);
            } else {
            	pstmt.setString(89, this.getAvailiaDateFlag());
            }
            if(this.getComBusType() == null || this.getComBusType().equals("null")) {
            	pstmt.setNull(90, 12);
            } else {
            	pstmt.setString(90, this.getComBusType());
            }
            if(this.getPrintFlag() == null || this.getPrintFlag().equals("null")) {
            	pstmt.setNull(91, 12);
            } else {
            	pstmt.setString(91, this.getPrintFlag());
            }
            if(this.getSpecAcceptFlag() == null || this.getSpecAcceptFlag().equals("null")) {
            	pstmt.setNull(92, 12);
            } else {
            	pstmt.setString(92, this.getSpecAcceptFlag());
            }
            if(this.getPolicyType() == null || this.getPolicyType().equals("null")) {
            	pstmt.setNull(93, 12);
            } else {
            	pstmt.setString(93, this.getPolicyType());
            }
            if(this.getAchvAccruFlag() == null || this.getAchvAccruFlag().equals("null")) {
            	pstmt.setNull(94, 12);
            } else {
            	pstmt.setString(94, this.getAchvAccruFlag());
            }
            if(this.getRemark() == null || this.getRemark().equals("null")) {
            	pstmt.setNull(95, 12);
            } else {
            	pstmt.setString(95, this.getRemark());
            }
            if(this.getValDateType() == null || this.getValDateType().equals("null")) {
            	pstmt.setNull(96, 12);
            } else {
            	pstmt.setString(96, this.getValDateType());
            }
            if(this.getSecLevelOther() == null || this.getSecLevelOther().equals("null")) {
            	pstmt.setNull(97, 12);
            } else {
            	pstmt.setString(97, this.getSecLevelOther());
            }
            if(this.getTBType() == null || this.getTBType().equals("null")) {
            	pstmt.setNull(98, 12);
            } else {
            	pstmt.setString(98, this.getTBType());
            }
            if(this.getSlipForm() == null || this.getSlipForm().equals("null")) {
            	pstmt.setNull(99, 12);
            } else {
            	pstmt.setString(99, this.getSlipForm());
            }
            // set where condition
            if(this.getGrpContNo() == null || this.getGrpContNo().equals("null")) {
            	pstmt.setNull(100, 12);
            } else {
            	pstmt.setString(100, this.getGrpContNo());
            }
            pstmt.executeUpdate();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LBGrpContDB";
            tError.functionName = "update()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);
            ex.printStackTrace();

            try {
                pstmt.close();
            } catch (Exception e){}

            if( !mflag ) {
                try {
                    con.close();
                } catch (Exception e){}
            }

            return false;
        }

        if(!mflag) {
            try {
                con.close();
            } catch (Exception e){}
        }

        return true;
    }

    public boolean insert() {
        PreparedStatement pstmt = null;

        SQLString sqlObj = new SQLString("LBGrpCont");
        sqlObj.setSQL(1, this);
        String sql = sqlObj.getSQL();

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try {
            pstmt = con.prepareStatement("INSERT INTO LBGrpCont VALUES( ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ?)");
            if(this.getGrpContNo() == null || this.getGrpContNo().equals("null")) {
            	pstmt.setNull(1, 12);
            } else {
            	pstmt.setString(1, this.getGrpContNo());
            }
            if(this.getProposalGrpContNo() == null || this.getProposalGrpContNo().equals("null")) {
            	pstmt.setNull(2, 12);
            } else {
            	pstmt.setString(2, this.getProposalGrpContNo());
            }
            if(this.getPrtNo() == null || this.getPrtNo().equals("null")) {
            	pstmt.setNull(3, 12);
            } else {
            	pstmt.setString(3, this.getPrtNo());
            }
            if(this.getSaleChnl() == null || this.getSaleChnl().equals("null")) {
            	pstmt.setNull(4, 12);
            } else {
            	pstmt.setString(4, this.getSaleChnl());
            }
            if(this.getSalesWay() == null || this.getSalesWay().equals("null")) {
            	pstmt.setNull(5, 12);
            } else {
            	pstmt.setString(5, this.getSalesWay());
            }
            if(this.getSalesWaySub() == null || this.getSalesWaySub().equals("null")) {
            	pstmt.setNull(6, 12);
            } else {
            	pstmt.setString(6, this.getSalesWaySub());
            }
            if(this.getBizNature() == null || this.getBizNature().equals("null")) {
            	pstmt.setNull(7, 12);
            } else {
            	pstmt.setString(7, this.getBizNature());
            }
            if(this.getPolIssuPlat() == null || this.getPolIssuPlat().equals("null")) {
            	pstmt.setNull(8, 12);
            } else {
            	pstmt.setString(8, this.getPolIssuPlat());
            }
            if(this.getContType() == null || this.getContType().equals("null")) {
            	pstmt.setNull(9, 12);
            } else {
            	pstmt.setString(9, this.getContType());
            }
            if(this.getAgentCom() == null || this.getAgentCom().equals("null")) {
            	pstmt.setNull(10, 12);
            } else {
            	pstmt.setString(10, this.getAgentCom());
            }
            if(this.getAgentCode() == null || this.getAgentCode().equals("null")) {
            	pstmt.setNull(11, 12);
            } else {
            	pstmt.setString(11, this.getAgentCode());
            }
            if(this.getAgentGroup() == null || this.getAgentGroup().equals("null")) {
            	pstmt.setNull(12, 12);
            } else {
            	pstmt.setString(12, this.getAgentGroup());
            }
            if(this.getAgentCode1() == null || this.getAgentCode1().equals("null")) {
            	pstmt.setNull(13, 12);
            } else {
            	pstmt.setString(13, this.getAgentCode1());
            }
            if(this.getAppntNo() == null || this.getAppntNo().equals("null")) {
            	pstmt.setNull(14, 12);
            } else {
            	pstmt.setString(14, this.getAppntNo());
            }
            pstmt.setInt(15, this.getPeoples2());
            if(this.getGrpName() == null || this.getGrpName().equals("null")) {
            	pstmt.setNull(16, 12);
            } else {
            	pstmt.setString(16, this.getGrpName());
            }
            if(this.getDisputedFlag() == null || this.getDisputedFlag().equals("null")) {
            	pstmt.setNull(17, 12);
            } else {
            	pstmt.setString(17, this.getDisputedFlag());
            }
            if(this.getOutPayFlag() == null || this.getOutPayFlag().equals("null")) {
            	pstmt.setNull(18, 12);
            } else {
            	pstmt.setString(18, this.getOutPayFlag());
            }
            if(this.getLang() == null || this.getLang().equals("null")) {
            	pstmt.setNull(19, 12);
            } else {
            	pstmt.setString(19, this.getLang());
            }
            if(this.getCurrency() == null || this.getCurrency().equals("null")) {
            	pstmt.setNull(20, 12);
            } else {
            	pstmt.setString(20, this.getCurrency());
            }
            pstmt.setInt(21, this.getLostTimes());
            pstmt.setInt(22, this.getPrintCount());
            if(this.getLastEdorDate() == null || this.getLastEdorDate().equals("null")) {
            	pstmt.setNull(23, 93);
            } else {
            	pstmt.setDate(23, Date.valueOf(this.getLastEdorDate()));
            }
            if(this.getSpecFlag() == null || this.getSpecFlag().equals("null")) {
            	pstmt.setNull(24, 12);
            } else {
            	pstmt.setString(24, this.getSpecFlag());
            }
            if(this.getSignCom() == null || this.getSignCom().equals("null")) {
            	pstmt.setNull(25, 12);
            } else {
            	pstmt.setString(25, this.getSignCom());
            }
            if(this.getSignDate() == null || this.getSignDate().equals("null")) {
            	pstmt.setNull(26, 93);
            } else {
            	pstmt.setDate(26, Date.valueOf(this.getSignDate()));
            }
            if(this.getSignTime() == null || this.getSignTime().equals("null")) {
            	pstmt.setNull(27, 12);
            } else {
            	pstmt.setString(27, this.getSignTime());
            }
            if(this.getCValiDate() == null || this.getCValiDate().equals("null")) {
            	pstmt.setNull(28, 93);
            } else {
            	pstmt.setDate(28, Date.valueOf(this.getCValiDate()));
            }
            pstmt.setInt(29, this.getPayIntv());
            pstmt.setDouble(30, this.getManageFeeRate());
            pstmt.setInt(31, this.getExpPeoples());
            pstmt.setDouble(32, this.getExpPremium());
            pstmt.setDouble(33, this.getExpAmnt());
            pstmt.setInt(34, this.getPeoples());
            pstmt.setDouble(35, this.getMult());
            pstmt.setDouble(36, this.getPrem());
            pstmt.setDouble(37, this.getAmnt());
            pstmt.setDouble(38, this.getSumPrem());
            pstmt.setDouble(39, this.getSumPay());
            pstmt.setDouble(40, this.getDif());
            if(this.getStandbyFlag1() == null || this.getStandbyFlag1().equals("null")) {
            	pstmt.setNull(41, 12);
            } else {
            	pstmt.setString(41, this.getStandbyFlag1());
            }
            if(this.getStandbyFlag2() == null || this.getStandbyFlag2().equals("null")) {
            	pstmt.setNull(42, 12);
            } else {
            	pstmt.setString(42, this.getStandbyFlag2());
            }
            if(this.getStandbyFlag3() == null || this.getStandbyFlag3().equals("null")) {
            	pstmt.setNull(43, 12);
            } else {
            	pstmt.setString(43, this.getStandbyFlag3());
            }
            if(this.getInputOperator() == null || this.getInputOperator().equals("null")) {
            	pstmt.setNull(44, 12);
            } else {
            	pstmt.setString(44, this.getInputOperator());
            }
            if(this.getInputDate() == null || this.getInputDate().equals("null")) {
            	pstmt.setNull(45, 93);
            } else {
            	pstmt.setDate(45, Date.valueOf(this.getInputDate()));
            }
            if(this.getInputTime() == null || this.getInputTime().equals("null")) {
            	pstmt.setNull(46, 12);
            } else {
            	pstmt.setString(46, this.getInputTime());
            }
            if(this.getApproveFlag() == null || this.getApproveFlag().equals("null")) {
            	pstmt.setNull(47, 12);
            } else {
            	pstmt.setString(47, this.getApproveFlag());
            }
            if(this.getApproveCode() == null || this.getApproveCode().equals("null")) {
            	pstmt.setNull(48, 12);
            } else {
            	pstmt.setString(48, this.getApproveCode());
            }
            if(this.getApproveDate() == null || this.getApproveDate().equals("null")) {
            	pstmt.setNull(49, 93);
            } else {
            	pstmt.setDate(49, Date.valueOf(this.getApproveDate()));
            }
            if(this.getApproveTime() == null || this.getApproveTime().equals("null")) {
            	pstmt.setNull(50, 12);
            } else {
            	pstmt.setString(50, this.getApproveTime());
            }
            if(this.getUWOperator() == null || this.getUWOperator().equals("null")) {
            	pstmt.setNull(51, 12);
            } else {
            	pstmt.setString(51, this.getUWOperator());
            }
            if(this.getUWFlag() == null || this.getUWFlag().equals("null")) {
            	pstmt.setNull(52, 12);
            } else {
            	pstmt.setString(52, this.getUWFlag());
            }
            if(this.getUWDate() == null || this.getUWDate().equals("null")) {
            	pstmt.setNull(53, 93);
            } else {
            	pstmt.setDate(53, Date.valueOf(this.getUWDate()));
            }
            if(this.getUWTime() == null || this.getUWTime().equals("null")) {
            	pstmt.setNull(54, 12);
            } else {
            	pstmt.setString(54, this.getUWTime());
            }
            if(this.getAppFlag() == null || this.getAppFlag().equals("null")) {
            	pstmt.setNull(55, 12);
            } else {
            	pstmt.setString(55, this.getAppFlag());
            }
            if(this.getState() == null || this.getState().equals("null")) {
            	pstmt.setNull(56, 12);
            } else {
            	pstmt.setString(56, this.getState());
            }
            if(this.getPolApplyDate() == null || this.getPolApplyDate().equals("null")) {
            	pstmt.setNull(57, 93);
            } else {
            	pstmt.setDate(57, Date.valueOf(this.getPolApplyDate()));
            }
            if(this.getCustomGetPolDate() == null || this.getCustomGetPolDate().equals("null")) {
            	pstmt.setNull(58, 93);
            } else {
            	pstmt.setDate(58, Date.valueOf(this.getCustomGetPolDate()));
            }
            if(this.getGetPolDate() == null || this.getGetPolDate().equals("null")) {
            	pstmt.setNull(59, 93);
            } else {
            	pstmt.setDate(59, Date.valueOf(this.getGetPolDate()));
            }
            if(this.getManageCom() == null || this.getManageCom().equals("null")) {
            	pstmt.setNull(60, 12);
            } else {
            	pstmt.setString(60, this.getManageCom());
            }
            if(this.getComCode() == null || this.getComCode().equals("null")) {
            	pstmt.setNull(61, 12);
            } else {
            	pstmt.setString(61, this.getComCode());
            }
            if(this.getOperator() == null || this.getOperator().equals("null")) {
            	pstmt.setNull(62, 12);
            } else {
            	pstmt.setString(62, this.getOperator());
            }
            if(this.getMakeDate() == null || this.getMakeDate().equals("null")) {
            	pstmt.setNull(63, 93);
            } else {
            	pstmt.setDate(63, Date.valueOf(this.getMakeDate()));
            }
            if(this.getMakeTime() == null || this.getMakeTime().equals("null")) {
            	pstmt.setNull(64, 12);
            } else {
            	pstmt.setString(64, this.getMakeTime());
            }
            if(this.getModifyOperator() == null || this.getModifyOperator().equals("null")) {
            	pstmt.setNull(65, 12);
            } else {
            	pstmt.setString(65, this.getModifyOperator());
            }
            if(this.getModifyDate() == null || this.getModifyDate().equals("null")) {
            	pstmt.setNull(66, 93);
            } else {
            	pstmt.setDate(66, Date.valueOf(this.getModifyDate()));
            }
            if(this.getModifyTime() == null || this.getModifyTime().equals("null")) {
            	pstmt.setNull(67, 12);
            } else {
            	pstmt.setString(67, this.getModifyTime());
            }
            if(this.getEnterKind() == null || this.getEnterKind().equals("null")) {
            	pstmt.setNull(68, 12);
            } else {
            	pstmt.setString(68, this.getEnterKind());
            }
            if(this.getAmntGrade() == null || this.getAmntGrade().equals("null")) {
            	pstmt.setNull(69, 12);
            } else {
            	pstmt.setString(69, this.getAmntGrade());
            }
            if(this.getFirstTrialOperator() == null || this.getFirstTrialOperator().equals("null")) {
            	pstmt.setNull(70, 12);
            } else {
            	pstmt.setString(70, this.getFirstTrialOperator());
            }
            if(this.getFirstTrialDate() == null || this.getFirstTrialDate().equals("null")) {
            	pstmt.setNull(71, 93);
            } else {
            	pstmt.setDate(71, Date.valueOf(this.getFirstTrialDate()));
            }
            if(this.getFirstTrialTime() == null || this.getFirstTrialTime().equals("null")) {
            	pstmt.setNull(72, 12);
            } else {
            	pstmt.setString(72, this.getFirstTrialTime());
            }
            if(this.getReceiveOperator() == null || this.getReceiveOperator().equals("null")) {
            	pstmt.setNull(73, 12);
            } else {
            	pstmt.setString(73, this.getReceiveOperator());
            }
            if(this.getReceiveDate() == null || this.getReceiveDate().equals("null")) {
            	pstmt.setNull(74, 93);
            } else {
            	pstmt.setDate(74, Date.valueOf(this.getReceiveDate()));
            }
            if(this.getReceiveTime() == null || this.getReceiveTime().equals("null")) {
            	pstmt.setNull(75, 12);
            } else {
            	pstmt.setString(75, this.getReceiveTime());
            }
            if(this.getMarketType() == null || this.getMarketType().equals("null")) {
            	pstmt.setNull(76, 12);
            } else {
            	pstmt.setString(76, this.getMarketType());
            }
            if(this.getReportNo() == null || this.getReportNo().equals("null")) {
            	pstmt.setNull(77, 12);
            } else {
            	pstmt.setString(77, this.getReportNo());
            }
            if(this.getEdorCalType() == null || this.getEdorCalType().equals("null")) {
            	pstmt.setNull(78, 12);
            } else {
            	pstmt.setString(78, this.getEdorCalType());
            }
            if(this.getRenewFlag() == null || this.getRenewFlag().equals("null")) {
            	pstmt.setNull(79, 12);
            } else {
            	pstmt.setString(79, this.getRenewFlag());
            }
            if(this.getRenewContNo() == null || this.getRenewContNo().equals("null")) {
            	pstmt.setNull(80, 12);
            } else {
            	pstmt.setString(80, this.getRenewContNo());
            }
            if(this.getEndDate() == null || this.getEndDate().equals("null")) {
            	pstmt.setNull(81, 93);
            } else {
            	pstmt.setDate(81, Date.valueOf(this.getEndDate()));
            }
            if(this.getCoinsurance() == null || this.getCoinsurance().equals("null")) {
            	pstmt.setNull(82, 12);
            } else {
            	pstmt.setString(82, this.getCoinsurance());
            }
            if(this.getCardTypeCode() == null || this.getCardTypeCode().equals("null")) {
            	pstmt.setNull(83, 12);
            } else {
            	pstmt.setString(83, this.getCardTypeCode());
            }
            if(this.getRelationToappnt() == null || this.getRelationToappnt().equals("null")) {
            	pstmt.setNull(84, 12);
            } else {
            	pstmt.setString(84, this.getRelationToappnt());
            }
            if(this.getNewReinsureFlag() == null || this.getNewReinsureFlag().equals("null")) {
            	pstmt.setNull(85, 12);
            } else {
            	pstmt.setString(85, this.getNewReinsureFlag());
            }
            pstmt.setLong(86, this.getInsureRate());
            if(this.getAgcAgentCode() == null || this.getAgcAgentCode().equals("null")) {
            	pstmt.setNull(87, 12);
            } else {
            	pstmt.setString(87, this.getAgcAgentCode());
            }
            if(this.getAgcAgentName() == null || this.getAgcAgentName().equals("null")) {
            	pstmt.setNull(88, 12);
            } else {
            	pstmt.setString(88, this.getAgcAgentName());
            }
            if(this.getAvailiaDateFlag() == null || this.getAvailiaDateFlag().equals("null")) {
            	pstmt.setNull(89, 12);
            } else {
            	pstmt.setString(89, this.getAvailiaDateFlag());
            }
            if(this.getComBusType() == null || this.getComBusType().equals("null")) {
            	pstmt.setNull(90, 12);
            } else {
            	pstmt.setString(90, this.getComBusType());
            }
            if(this.getPrintFlag() == null || this.getPrintFlag().equals("null")) {
            	pstmt.setNull(91, 12);
            } else {
            	pstmt.setString(91, this.getPrintFlag());
            }
            if(this.getSpecAcceptFlag() == null || this.getSpecAcceptFlag().equals("null")) {
            	pstmt.setNull(92, 12);
            } else {
            	pstmt.setString(92, this.getSpecAcceptFlag());
            }
            if(this.getPolicyType() == null || this.getPolicyType().equals("null")) {
            	pstmt.setNull(93, 12);
            } else {
            	pstmt.setString(93, this.getPolicyType());
            }
            if(this.getAchvAccruFlag() == null || this.getAchvAccruFlag().equals("null")) {
            	pstmt.setNull(94, 12);
            } else {
            	pstmt.setString(94, this.getAchvAccruFlag());
            }
            if(this.getRemark() == null || this.getRemark().equals("null")) {
            	pstmt.setNull(95, 12);
            } else {
            	pstmt.setString(95, this.getRemark());
            }
            if(this.getValDateType() == null || this.getValDateType().equals("null")) {
            	pstmt.setNull(96, 12);
            } else {
            	pstmt.setString(96, this.getValDateType());
            }
            if(this.getSecLevelOther() == null || this.getSecLevelOther().equals("null")) {
            	pstmt.setNull(97, 12);
            } else {
            	pstmt.setString(97, this.getSecLevelOther());
            }
            if(this.getTBType() == null || this.getTBType().equals("null")) {
            	pstmt.setNull(98, 12);
            } else {
            	pstmt.setString(98, this.getTBType());
            }
            if(this.getSlipForm() == null || this.getSlipForm().equals("null")) {
            	pstmt.setNull(99, 12);
            } else {
            	pstmt.setString(99, this.getSlipForm());
            }
            // execute sql
            pstmt.executeUpdate();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LBGrpContDB";
            tError.functionName = "insert()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors.addOneError(tError);
            ex.printStackTrace();

            try {
                pstmt.close();
            } catch (Exception e){}

            if( !mflag ) {
                try {
                    con.close();
                } catch (Exception e){}
            }

            return false;
        }

        if(!mflag) {
            try {
                con.close();
            } catch (Exception e){}
        }

        return true;
    }

    public boolean getInfo() {
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        if(!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            pstmt = con.prepareStatement("SELECT * FROM LBGrpCont WHERE  1=1  AND GrpContNo = ?", 
                ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
            if(this.getGrpContNo() == null || this.getGrpContNo().equals("null")) {
            	pstmt.setNull(1, 12);
            } else {
            	pstmt.setString(1, this.getGrpContNo());
            }
            rs = pstmt.executeQuery();
            int i = 0;
            while (rs.next()) {
                i++;
                if (!this.setSchema(rs,i)) {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "LBGrpContDB";
                    tError.functionName = "getInfo";
                    tError.errorMessage = "取数失败!";
                    this.mErrors .addOneError(tError);

                    try{ rs.close(); } catch( Exception ex ) {}
                    try{ pstmt.close(); } catch( Exception ex1 ) {}

                    if (!mflag) {
                        try {
                            con.close();
                        }
                        catch(Exception et){}
                    }
                    return false;
                }
                break;
            }
            try{ rs.close(); } catch( Exception ex2 ) {}
            try{ pstmt.close(); } catch( Exception ex3 ) {}

            if( i == 0 ) {
                if (!mflag) {
                    try {
                        con.close();
                    }
                    catch(Exception et){}
                }
                return false;
            }
        }
        catch(Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LBGrpContDB";
            tError.functionName = "getInfo";
            tError.errorMessage = ExceptionUtils.exceptionToString(e);
            this.mErrors .addOneError(tError);

            try{ rs.close(); } catch( Exception ex ) {}
            try{ pstmt.close(); } catch( Exception ex1 ) {}

            if (!mflag) {
                try {
                    con.close();
                }
                catch(Exception et){}
            }
            return false;
        }
        // 断开数据库连接
        if (!mflag) {
            try {
                con.close();
            }
            catch(Exception e){}
        }

        return true;
    }

    public LBGrpContSet query() {
        Statement stmt = null;
        ResultSet rs = null;
        LBGrpContSet aLBGrpContSet = new LBGrpContSet();

        if (!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            stmt = con.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
            SQLString sqlObj = new SQLString("LBGrpCont");
            LBGrpContSchema aSchema = this.getSchema();
            sqlObj.setSQL(5, aSchema);
            String sql = sqlObj.getSQL();

            rs = stmt.executeQuery(sql);
            int i = 0;
            while (rs.next()) {
                i++;
                if (i>10000) {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "LBGrpContDB";
                    tError.functionName = "query";
                    tError.errorMessage = "SQL语句返回结果集太多，请换用其他方式!";
                    this.mErrors .addOneError(tError);
                        break;
                }
                LBGrpContSchema s1 = new LBGrpContSchema();
                s1.setSchema(rs,i);
                aLBGrpContSet.add(s1);
            }
            try{ rs.close(); } catch( Exception ex ) {}
            try{ stmt.close(); } catch( Exception ex1 ) {}
        }
        catch(Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LBGrpContDB";
            tError.functionName = "query";
            tError.errorMessage = ExceptionUtils.exceptionToString(e);
            this.mErrors.addOneError(tError);
            e.printStackTrace();

            try { rs.close(); } catch (Exception ex2) { ex2.printStackTrace(); }
            try { stmt.close(); } catch(Exception ex3) { ex3.printStackTrace(); }

            if (!mflag) {
                try {
                    con.close();
                }
                catch(Exception et){et.printStackTrace();}
            }
        }

        if (!mflag) {
            try {
                con.close();
            }
            catch(Exception e){e.printStackTrace();}
        }

        return aLBGrpContSet;
    }

    public LBGrpContSet executeQuery(String sql)  {
        Statement stmt = null;
        ResultSet rs = null;
        LBGrpContSet aLBGrpContSet = new LBGrpContSet();

        if (!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            stmt = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);

            rs = stmt.executeQuery(StrTool.GBKToUnicode(sql));
            int i = 0;
            while (rs.next()) {
                i++;
                if (i>10000) {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "LBGrpContDB";
                    tError.functionName = "executeQuery";
                    tError.errorMessage = "SQL语句返回结果集太多，请换用其他方式!";
                    this.mErrors .addOneError(tError);
                        break;
                }
                LBGrpContSchema s1 = new LBGrpContSchema();
                if (!s1.setSchema(rs,i)) {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "LBGrpContDB";
                    tError.functionName = "executeQuery";
                    tError.errorMessage = "sql语句有误，请查看表名及字段名信息!";
                    this.mErrors .addOneError(tError);
                }
                aLBGrpContSet.add(s1);
            }
            try{ rs.close(); } catch( Exception ex ) {}
            try{ stmt.close(); } catch( Exception ex1 ) {}
        }
        catch(Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LBGrpContDB";
            tError.functionName = "executeQuery";
            tError.errorMessage = ExceptionUtils.exceptionToString(e);
            this.mErrors .addOneError(tError);

            try{ rs.close(); } catch( Exception ex2 ) {}
            try{ stmt.close(); } catch( Exception ex3 ) {}

            if (!mflag) {
                try {
                    con.close();
                }
                catch(Exception et){}
            }
        }

        if (!mflag) {
            try {
                con.close();
            }
            catch(Exception e){}
        }

        return aLBGrpContSet;
    }

    public LBGrpContSet query(int nStart, int nCount) {
        Statement stmt = null;
        ResultSet rs = null;
        LBGrpContSet aLBGrpContSet = new LBGrpContSet();

        if (!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            stmt = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);
            SQLString sqlObj = new SQLString("LBGrpCont");
            LBGrpContSchema aSchema = this.getSchema();
            sqlObj.setSQL(5,aSchema);
            String sql = sqlObj.getSQL();

            rs = stmt.executeQuery(sql);
            int i = 0;
            while (rs.next()) {
                i++;

                if( i < nStart ) {
                    continue;
                }

                if( i >= nStart + nCount ) {
                    break;
                }

                LBGrpContSchema s1 = new LBGrpContSchema();
                s1.setSchema(rs,i);
                aLBGrpContSet.add(s1);
            }
            try{ rs.close(); } catch( Exception ex ) {}
            try{ stmt.close(); } catch( Exception ex1 ) {}
        }
        catch(Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LBGrpContDB";
            tError.functionName = "query";
            tError.errorMessage = ExceptionUtils.exceptionToString(e);
            this.mErrors .addOneError(tError);

            try{ rs.close(); } catch( Exception ex2 ) {}
            try{ stmt.close(); } catch( Exception ex3 ) {}

            if (!mflag) {
                try {
                    con.close();
                }
                catch(Exception et){}
            }
        }

        if (!mflag) {
            try {
                con.close();
            }
            catch(Exception e){}
        }

        return aLBGrpContSet;
    }

    public LBGrpContSet executeQuery(String sql, int nStart, int nCount) {
        Statement stmt = null;
        ResultSet rs = null;
        LBGrpContSet aLBGrpContSet = new LBGrpContSet();

        if (!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            stmt = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);

            rs = stmt.executeQuery(StrTool.GBKToUnicode(sql));
            int i = 0;
            while (rs.next()) {
                i++;

                if( i < nStart ) {
                    continue;
                }

                if( i >= nStart + nCount ) {
                    break;
                }

                LBGrpContSchema s1 = new LBGrpContSchema();
                if (!s1.setSchema(rs,i)) {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "LBGrpContDB";
                    tError.functionName = "executeQuery";
                    tError.errorMessage = "sql语句有误，请查看表名及字段名信息!";
                    this.mErrors .addOneError(tError);
                }
                aLBGrpContSet.add(s1);
            }
            try{ rs.close(); } catch( Exception ex ) {}
            try{ stmt.close(); } catch( Exception ex1 ) {}
        }
        catch(Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LBGrpContDB";
            tError.functionName = "executeQuery";
            tError.errorMessage = ExceptionUtils.exceptionToString(e);
            this.mErrors .addOneError(tError);

            try{ rs.close(); } catch( Exception ex2 ) {}
            try{ stmt.close(); } catch( Exception ex3 ) {}

            if (!mflag) {
                try {
                    con.close();
                }
                catch(Exception et){}
            }
        }

        if (!mflag) {
            try {
                con.close();
            }
            catch(Exception e){}
        }

        return aLBGrpContSet;
    }

    public boolean update(String strWherePart) {
        Statement stmt = null;

        if (!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            stmt = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);
            SQLString sqlObj = new SQLString("LBGrpCont");
            LBGrpContSchema aSchema = this.getSchema();
            sqlObj.setSQL(2,aSchema);
            String sql = "update LBGrpCont " + sqlObj.getUpdPart() + " where " + strWherePart;

            int operCount = stmt.executeUpdate(sql);
            if (operCount == 0) {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "LBGrpContDB";
                tError.functionName = "update";
                tError.errorMessage = "更新数据失败!";
                this.mErrors .addOneError(tError);

                if (!mflag) {
                    try {
                        con.close();
                    }
                    catch(Exception et){}
                }
                return false;
            }
        }
        catch(Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LBGrpContDB";
            tError.functionName = "update";
            tError.errorMessage = ExceptionUtils.exceptionToString(e);
            this.mErrors .addOneError(tError);

            try{ stmt.close(); } catch( Exception ex1 ) {}

            if (!mflag) {
                try {
                    con.close();
                }
                catch(Exception et){}
            }
            return false;
        }
        // 断开数据库连接
        if (!mflag) {
            try {
                con.close();
            }
            catch(Exception e){}
        }

        return true;
    }

    /**
     * 准备数据查询条件
     * @param strSQL String
     * @return boolean
     */
    public boolean prepareData(String strSQL) {
        if (mResultSet != null) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LBGrpContDB";
            tError.functionName = "prepareData";
            tError.errorMessage = "数据集非空，程序在准备数据集之后，没有关闭！";
            this.mErrors.addOneError(tError);
            return false;
        }

        if (!mflag) {
            con = DBConnPool.getConnection();
        }
        try {
            mStatement = con.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
            mResultSet = mStatement.executeQuery(StrTool.GBKToUnicode(strSQL));
        }
        catch (Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LBGrpContDB";
            tError.functionName = "prepareData";
            tError.errorMessage = ExceptionUtils.exceptionToString(e);
            this.mErrors.addOneError(tError);
            try {
                mResultSet.close();
            }
            catch (Exception ex2) {}
            try {
                mStatement.close();
            }
            catch (Exception ex3) {}
            if (!mflag) {
                try {
                    con.close();
                }
                catch (Exception et) {}
            }
            return false;
        }

        if (!mflag) {
            try {
                con.close();
            }
            catch (Exception e) {}
        }
        return true;
    }

    /**
     * 获取数据集
     * @return boolean
     */
    public boolean hasMoreData() {
        boolean flag = true;
        if (null == mResultSet) {
            CError tError = new CError();
            tError.moduleName = "LBGrpContDB";
            tError.functionName = "hasMoreData";
            tError.errorMessage = "数据集为空，请先准备数据集！";
            this.mErrors.addOneError(tError);
            return false;
        }
        try {
            flag = mResultSet.next();
        }
        catch (Exception ex) {
            CError tError = new CError();
            tError.moduleName = "LBGrpContDB";
            tError.functionName = "hasMoreData";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors.addOneError(tError);
            try {
                mResultSet.close();
                mResultSet = null;
            }
            catch (Exception ex2) {}
            try {
                mStatement.close();
                mStatement = null;
            }
            catch (Exception ex3) {}
            if (!mflag) {
                try {
                    con.close();
                }
                catch (Exception et) {}
            }
            return false;
        }
        return flag;
    }
    /**
     * 获取定量数据
     * @return LBGrpContSet
     */
    public LBGrpContSet getData() {
        int tCount = 0;
        LBGrpContSet tLBGrpContSet = new LBGrpContSet();
        LBGrpContSchema tLBGrpContSchema = null;
        if (null == mResultSet) {
            CError tError = new CError();
            tError.moduleName = "LBGrpContDB";
            tError.functionName = "getData";
            tError.errorMessage = "数据集为空，请先准备数据集！";
            this.mErrors.addOneError(tError);
            return null;
        }
        try {
            tCount = 1;
            tLBGrpContSchema = new LBGrpContSchema();
            tLBGrpContSchema.setSchema(mResultSet, 1);
            tLBGrpContSet.add(tLBGrpContSchema);
            //注意mResultSet.next()的作用
            while (tCount++ < SysConst.FETCHCOUNT) {
                if (mResultSet.next()) {
                    tLBGrpContSchema = new LBGrpContSchema();
                    tLBGrpContSchema.setSchema(mResultSet, 1);
                    tLBGrpContSet.add(tLBGrpContSchema);
                }
            }
        }
        catch (Exception ex) {
            CError tError = new CError();
            tError.moduleName = "LBGrpContDB";
            tError.functionName = "getData";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors.addOneError(tError);
            try {
                mResultSet.close();
                mResultSet = null;
            }
            catch (Exception ex2) {}
            try {
                mStatement.close();
                mStatement = null;
            }
            catch (Exception ex3) {}
            if (!mflag) {
                try {
                    con.close();
                }
                catch (Exception et) {}
            }
            return null;
        }
        return tLBGrpContSet;
    }
    /**
     * 关闭数据集
     * @return boolean
     */
    public boolean closeData() {
        boolean flag = true;
        try {
            if (null == mResultSet) {
                CError tError = new CError();
                tError.moduleName = "LBGrpContDB";
                tError.functionName = "closeData";
                tError.errorMessage = "数据集已经关闭了！";
                this.mErrors.addOneError(tError);
                flag = false;
            } else {
                mResultSet.close();
                mResultSet = null;
            }
        }
        catch (Exception ex2) {
            CError tError = new CError();
            tError.moduleName = "LBGrpContDB";
            tError.functionName = "closeData";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex2);
            this.mErrors.addOneError(tError);
            flag = false;
        }
        try {
            if (null == mStatement) {
                CError tError = new CError();
                tError.moduleName = "LBGrpContDB";
                tError.functionName = "closeData";
                tError.errorMessage = "语句已经关闭了！";
                this.mErrors.addOneError(tError);
                flag = false;
            } else {
                mStatement.close();
                mStatement = null;
            }
        }
        catch (Exception ex3) {
            CError tError = new CError();
            tError.moduleName = "LBGrpContDB";
            tError.functionName = "closeData";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex3);
            this.mErrors.addOneError(tError);
            flag = false;
        }
        return flag;
    }
}
