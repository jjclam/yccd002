/**
 * Copyright (c) 2019 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LCAddressDB;

/**
 * <p>ClassName: LCAddressSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2019</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2019-09-05
 */
public class LCAddressSchema implements Schema, Cloneable {
    // @Field
    /** Id */
    private long AddressID;
    /** Fk_ldperson */
    private long PersonID;
    /** Shardingid */
    private String ShardingID;
    /** 客户号码 */
    private String CustomerNo;
    /** 地址号码 */
    private int AddressNo;
    /** 通讯地址 */
    private String PostalAddress;
    /** 通讯邮编 */
    private String ZipCode;
    /** 通讯电话 */
    private String Phone;
    /** 通讯传真 */
    private String Fax;
    /** 家庭地址 */
    private String HomeAddress;
    /** 家庭邮编 */
    private String HomeZipCode;
    /** 家庭电话 */
    private String HomePhone;
    /** 家庭传真 */
    private String HomeFax;
    /** 单位地址 */
    private String CompanyAddress;
    /** 单位邮编 */
    private String CompanyZipCode;
    /** 单位电话 */
    private String CompanyPhone;
    /** 单位传真 */
    private String CompanyFax;
    /** 手机 */
    private String Mobile;
    /** 手机中文标示 */
    private String MobileChs;
    /** E_mail */
    private String EMail;
    /** 传呼 */
    private String BP;
    /** 手机2 */
    private String Mobile2;
    /** 手机中文标示2 */
    private String MobileChs2;
    /** E_mail2 */
    private String EMail2;
    /** 传呼2 */
    private String BP2;
    /** 操作员 */
    private String Operator;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 最后一次修改日期 */
    private Date ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime;
    /** 单位名称 */
    private String GrpName;
    /** 省 */
    private String Province;
    /** 市 */
    private String City;
    /** 区/县 */
    private String County;
    /** 固定电话区号 */
    private String ZoneCode;
    /** 常住地址村/社区（楼、号） */
    private String StoreNo;
    /** 通讯地址村/社区（楼、号） */
    private String StoreNo2;
    /** 是否首选地址 */
    private String PreferredAddress;
    /** 是否首选电话 */
    private String PreferredPhoneNum;
    /** 固定电话国际区号 */
    private String PhoneInterCode;
    /** 固定电话国内区号 */
    private String PhoneDomesticCode;
    /** 移动电话国际区号 */
    private String MobileInterCode;
    /** 详细地址 */
    private String Address;

    public static final int FIELDNUM = 43;    // 数据库表的字段个数

    private static String[] PK;                // 主键

    private FDate fDate = new FDate();        // 处理日期

    public CErrors mErrors;            // 错误信息

    // @Constructor
    public LCAddressSchema() {
        mErrors = new CErrors();

        String[] pk = new String[3];
        pk[0] = "AddressID";
        pk[1] = "CustomerNo";
        pk[2] = "AddressNo";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException {
        LCAddressSchema cloned = (LCAddressSchema)super.clone();
        cloned.fDate = (FDate) fDate.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK() {
        return PK;
    }

    public long getAddressID() {
        return AddressID;
    }
    public void setAddressID(long aAddressID) {
        AddressID = aAddressID;
    }
    public void setAddressID(String aAddressID) {
        if (aAddressID != null && !aAddressID.equals("")) {
            AddressID = new Long(aAddressID).longValue();
        }
    }

    public long getPersonID() {
        return PersonID;
    }
    public void setPersonID(long aPersonID) {
        PersonID = aPersonID;
    }
    public void setPersonID(String aPersonID) {
        if (aPersonID != null && !aPersonID.equals("")) {
            PersonID = new Long(aPersonID).longValue();
        }
    }

    public String getShardingID() {
        return ShardingID;
    }
    public void setShardingID(String aShardingID) {
        ShardingID = aShardingID;
    }
    public String getCustomerNo() {
        return CustomerNo;
    }
    public void setCustomerNo(String aCustomerNo) {
        CustomerNo = aCustomerNo;
    }
    public int getAddressNo() {
        return AddressNo;
    }
    public void setAddressNo(int aAddressNo) {
        AddressNo = aAddressNo;
    }
    public void setAddressNo(String aAddressNo) {
        if (aAddressNo != null && !aAddressNo.equals("")) {
            Integer tInteger = new Integer(aAddressNo);
            int i = tInteger.intValue();
            AddressNo = i;
        }
    }

    public String getPostalAddress() {
        return PostalAddress;
    }
    public void setPostalAddress(String aPostalAddress) {
        PostalAddress = aPostalAddress;
    }
    public String getZipCode() {
        return ZipCode;
    }
    public void setZipCode(String aZipCode) {
        ZipCode = aZipCode;
    }
    public String getPhone() {
        return Phone;
    }
    public void setPhone(String aPhone) {
        Phone = aPhone;
    }
    public String getFax() {
        return Fax;
    }
    public void setFax(String aFax) {
        Fax = aFax;
    }
    public String getHomeAddress() {
        return HomeAddress;
    }
    public void setHomeAddress(String aHomeAddress) {
        HomeAddress = aHomeAddress;
    }
    public String getHomeZipCode() {
        return HomeZipCode;
    }
    public void setHomeZipCode(String aHomeZipCode) {
        HomeZipCode = aHomeZipCode;
    }
    public String getHomePhone() {
        return HomePhone;
    }
    public void setHomePhone(String aHomePhone) {
        HomePhone = aHomePhone;
    }
    public String getHomeFax() {
        return HomeFax;
    }
    public void setHomeFax(String aHomeFax) {
        HomeFax = aHomeFax;
    }
    public String getCompanyAddress() {
        return CompanyAddress;
    }
    public void setCompanyAddress(String aCompanyAddress) {
        CompanyAddress = aCompanyAddress;
    }
    public String getCompanyZipCode() {
        return CompanyZipCode;
    }
    public void setCompanyZipCode(String aCompanyZipCode) {
        CompanyZipCode = aCompanyZipCode;
    }
    public String getCompanyPhone() {
        return CompanyPhone;
    }
    public void setCompanyPhone(String aCompanyPhone) {
        CompanyPhone = aCompanyPhone;
    }
    public String getCompanyFax() {
        return CompanyFax;
    }
    public void setCompanyFax(String aCompanyFax) {
        CompanyFax = aCompanyFax;
    }
    public String getMobile() {
        return Mobile;
    }
    public void setMobile(String aMobile) {
        Mobile = aMobile;
    }
    public String getMobileChs() {
        return MobileChs;
    }
    public void setMobileChs(String aMobileChs) {
        MobileChs = aMobileChs;
    }
    public String getEMail() {
        return EMail;
    }
    public void setEMail(String aEMail) {
        EMail = aEMail;
    }
    public String getBP() {
        return BP;
    }
    public void setBP(String aBP) {
        BP = aBP;
    }
    public String getMobile2() {
        return Mobile2;
    }
    public void setMobile2(String aMobile2) {
        Mobile2 = aMobile2;
    }
    public String getMobileChs2() {
        return MobileChs2;
    }
    public void setMobileChs2(String aMobileChs2) {
        MobileChs2 = aMobileChs2;
    }
    public String getEMail2() {
        return EMail2;
    }
    public void setEMail2(String aEMail2) {
        EMail2 = aEMail2;
    }
    public String getBP2() {
        return BP2;
    }
    public void setBP2(String aBP2) {
        BP2 = aBP2;
    }
    public String getOperator() {
        return Operator;
    }
    public void setOperator(String aOperator) {
        Operator = aOperator;
    }
    public String getMakeDate() {
        if(MakeDate != null) {
            return fDate.getString(MakeDate);
        } else {
            return null;
        }
    }
    public void setMakeDate(Date aMakeDate) {
        MakeDate = aMakeDate;
    }
    public void setMakeDate(String aMakeDate) {
        if (aMakeDate != null && !aMakeDate.equals("")) {
            MakeDate = fDate.getDate(aMakeDate);
        } else
            MakeDate = null;
    }

    public String getMakeTime() {
        return MakeTime;
    }
    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }
    public String getModifyDate() {
        if(ModifyDate != null) {
            return fDate.getString(ModifyDate);
        } else {
            return null;
        }
    }
    public void setModifyDate(Date aModifyDate) {
        ModifyDate = aModifyDate;
    }
    public void setModifyDate(String aModifyDate) {
        if (aModifyDate != null && !aModifyDate.equals("")) {
            ModifyDate = fDate.getDate(aModifyDate);
        } else
            ModifyDate = null;
    }

    public String getModifyTime() {
        return ModifyTime;
    }
    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }
    public String getGrpName() {
        return GrpName;
    }
    public void setGrpName(String aGrpName) {
        GrpName = aGrpName;
    }
    public String getProvince() {
        return Province;
    }
    public void setProvince(String aProvince) {
        Province = aProvince;
    }
    public String getCity() {
        return City;
    }
    public void setCity(String aCity) {
        City = aCity;
    }
    public String getCounty() {
        return County;
    }
    public void setCounty(String aCounty) {
        County = aCounty;
    }
    public String getZoneCode() {
        return ZoneCode;
    }
    public void setZoneCode(String aZoneCode) {
        ZoneCode = aZoneCode;
    }
    public String getStoreNo() {
        return StoreNo;
    }
    public void setStoreNo(String aStoreNo) {
        StoreNo = aStoreNo;
    }
    public String getStoreNo2() {
        return StoreNo2;
    }
    public void setStoreNo2(String aStoreNo2) {
        StoreNo2 = aStoreNo2;
    }
    public String getPreferredAddress() {
        return PreferredAddress;
    }
    public void setPreferredAddress(String aPreferredAddress) {
        PreferredAddress = aPreferredAddress;
    }
    public String getPreferredPhoneNum() {
        return PreferredPhoneNum;
    }
    public void setPreferredPhoneNum(String aPreferredPhoneNum) {
        PreferredPhoneNum = aPreferredPhoneNum;
    }
    public String getPhoneInterCode() {
        return PhoneInterCode;
    }
    public void setPhoneInterCode(String aPhoneInterCode) {
        PhoneInterCode = aPhoneInterCode;
    }
    public String getPhoneDomesticCode() {
        return PhoneDomesticCode;
    }
    public void setPhoneDomesticCode(String aPhoneDomesticCode) {
        PhoneDomesticCode = aPhoneDomesticCode;
    }
    public String getMobileInterCode() {
        return MobileInterCode;
    }
    public void setMobileInterCode(String aMobileInterCode) {
        MobileInterCode = aMobileInterCode;
    }
    public String getAddress() {
        return Address;
    }
    public void setAddress(String aAddress) {
        Address = aAddress;
    }

    /**
    * 使用另外一个 LCAddressSchema 对象给 Schema 赋值
    * @param: aLCAddressSchema LCAddressSchema
    **/
    public void setSchema(LCAddressSchema aLCAddressSchema) {
        this.AddressID = aLCAddressSchema.getAddressID();
        this.PersonID = aLCAddressSchema.getPersonID();
        this.ShardingID = aLCAddressSchema.getShardingID();
        this.CustomerNo = aLCAddressSchema.getCustomerNo();
        this.AddressNo = aLCAddressSchema.getAddressNo();
        this.PostalAddress = aLCAddressSchema.getPostalAddress();
        this.ZipCode = aLCAddressSchema.getZipCode();
        this.Phone = aLCAddressSchema.getPhone();
        this.Fax = aLCAddressSchema.getFax();
        this.HomeAddress = aLCAddressSchema.getHomeAddress();
        this.HomeZipCode = aLCAddressSchema.getHomeZipCode();
        this.HomePhone = aLCAddressSchema.getHomePhone();
        this.HomeFax = aLCAddressSchema.getHomeFax();
        this.CompanyAddress = aLCAddressSchema.getCompanyAddress();
        this.CompanyZipCode = aLCAddressSchema.getCompanyZipCode();
        this.CompanyPhone = aLCAddressSchema.getCompanyPhone();
        this.CompanyFax = aLCAddressSchema.getCompanyFax();
        this.Mobile = aLCAddressSchema.getMobile();
        this.MobileChs = aLCAddressSchema.getMobileChs();
        this.EMail = aLCAddressSchema.getEMail();
        this.BP = aLCAddressSchema.getBP();
        this.Mobile2 = aLCAddressSchema.getMobile2();
        this.MobileChs2 = aLCAddressSchema.getMobileChs2();
        this.EMail2 = aLCAddressSchema.getEMail2();
        this.BP2 = aLCAddressSchema.getBP2();
        this.Operator = aLCAddressSchema.getOperator();
        this.MakeDate = fDate.getDate( aLCAddressSchema.getMakeDate());
        this.MakeTime = aLCAddressSchema.getMakeTime();
        this.ModifyDate = fDate.getDate( aLCAddressSchema.getModifyDate());
        this.ModifyTime = aLCAddressSchema.getModifyTime();
        this.GrpName = aLCAddressSchema.getGrpName();
        this.Province = aLCAddressSchema.getProvince();
        this.City = aLCAddressSchema.getCity();
        this.County = aLCAddressSchema.getCounty();
        this.ZoneCode = aLCAddressSchema.getZoneCode();
        this.StoreNo = aLCAddressSchema.getStoreNo();
        this.StoreNo2 = aLCAddressSchema.getStoreNo2();
        this.PreferredAddress = aLCAddressSchema.getPreferredAddress();
        this.PreferredPhoneNum = aLCAddressSchema.getPreferredPhoneNum();
        this.PhoneInterCode = aLCAddressSchema.getPhoneInterCode();
        this.PhoneDomesticCode = aLCAddressSchema.getPhoneDomesticCode();
        this.MobileInterCode = aLCAddressSchema.getMobileInterCode();
        this.Address = aLCAddressSchema.getAddress();
    }

    /**
    * 使用 ResultSet 中的第 i 行给 Schema 赋值
    * @param: rs ResultSet
    * @param: i int
    * @return: boolean
    **/
    public boolean setSchema(ResultSet rs,int i) {
        try {
            //rs.absolute(i);        // 非滚动游标
            this.AddressID = rs.getLong("AddressID");
            this.PersonID = rs.getLong("PersonID");
            if( rs.getString("ShardingID") == null )
                this.ShardingID = null;
            else
                this.ShardingID = rs.getString("ShardingID").trim();

            if( rs.getString("CustomerNo") == null )
                this.CustomerNo = null;
            else
                this.CustomerNo = rs.getString("CustomerNo").trim();

            this.AddressNo = rs.getInt("AddressNo");
            if( rs.getString("PostalAddress") == null )
                this.PostalAddress = null;
            else
                this.PostalAddress = rs.getString("PostalAddress").trim();

            if( rs.getString("ZipCode") == null )
                this.ZipCode = null;
            else
                this.ZipCode = rs.getString("ZipCode").trim();

            if( rs.getString("Phone") == null )
                this.Phone = null;
            else
                this.Phone = rs.getString("Phone").trim();

            if( rs.getString("Fax") == null )
                this.Fax = null;
            else
                this.Fax = rs.getString("Fax").trim();

            if( rs.getString("HomeAddress") == null )
                this.HomeAddress = null;
            else
                this.HomeAddress = rs.getString("HomeAddress").trim();

            if( rs.getString("HomeZipCode") == null )
                this.HomeZipCode = null;
            else
                this.HomeZipCode = rs.getString("HomeZipCode").trim();

            if( rs.getString("HomePhone") == null )
                this.HomePhone = null;
            else
                this.HomePhone = rs.getString("HomePhone").trim();

            if( rs.getString("HomeFax") == null )
                this.HomeFax = null;
            else
                this.HomeFax = rs.getString("HomeFax").trim();

            if( rs.getString("CompanyAddress") == null )
                this.CompanyAddress = null;
            else
                this.CompanyAddress = rs.getString("CompanyAddress").trim();

            if( rs.getString("CompanyZipCode") == null )
                this.CompanyZipCode = null;
            else
                this.CompanyZipCode = rs.getString("CompanyZipCode").trim();

            if( rs.getString("CompanyPhone") == null )
                this.CompanyPhone = null;
            else
                this.CompanyPhone = rs.getString("CompanyPhone").trim();

            if( rs.getString("CompanyFax") == null )
                this.CompanyFax = null;
            else
                this.CompanyFax = rs.getString("CompanyFax").trim();

            if( rs.getString("Mobile") == null )
                this.Mobile = null;
            else
                this.Mobile = rs.getString("Mobile").trim();

            if( rs.getString("MobileChs") == null )
                this.MobileChs = null;
            else
                this.MobileChs = rs.getString("MobileChs").trim();

            if( rs.getString("EMail") == null )
                this.EMail = null;
            else
                this.EMail = rs.getString("EMail").trim();

            if( rs.getString("BP") == null )
                this.BP = null;
            else
                this.BP = rs.getString("BP").trim();

            if( rs.getString("Mobile2") == null )
                this.Mobile2 = null;
            else
                this.Mobile2 = rs.getString("Mobile2").trim();

            if( rs.getString("MobileChs2") == null )
                this.MobileChs2 = null;
            else
                this.MobileChs2 = rs.getString("MobileChs2").trim();

            if( rs.getString("EMail2") == null )
                this.EMail2 = null;
            else
                this.EMail2 = rs.getString("EMail2").trim();

            if( rs.getString("BP2") == null )
                this.BP2 = null;
            else
                this.BP2 = rs.getString("BP2").trim();

            if( rs.getString("Operator") == null )
                this.Operator = null;
            else
                this.Operator = rs.getString("Operator").trim();

            this.MakeDate = rs.getDate("MakeDate");
            if( rs.getString("MakeTime") == null )
                this.MakeTime = null;
            else
                this.MakeTime = rs.getString("MakeTime").trim();

            this.ModifyDate = rs.getDate("ModifyDate");
            if( rs.getString("ModifyTime") == null )
                this.ModifyTime = null;
            else
                this.ModifyTime = rs.getString("ModifyTime").trim();

            if( rs.getString("GrpName") == null )
                this.GrpName = null;
            else
                this.GrpName = rs.getString("GrpName").trim();

            if( rs.getString("Province") == null )
                this.Province = null;
            else
                this.Province = rs.getString("Province").trim();

            if( rs.getString("City") == null )
                this.City = null;
            else
                this.City = rs.getString("City").trim();

            if( rs.getString("County") == null )
                this.County = null;
            else
                this.County = rs.getString("County").trim();

            if( rs.getString("ZoneCode") == null )
                this.ZoneCode = null;
            else
                this.ZoneCode = rs.getString("ZoneCode").trim();

            if( rs.getString("StoreNo") == null )
                this.StoreNo = null;
            else
                this.StoreNo = rs.getString("StoreNo").trim();

            if( rs.getString("StoreNo2") == null )
                this.StoreNo2 = null;
            else
                this.StoreNo2 = rs.getString("StoreNo2").trim();

            if( rs.getString("PreferredAddress") == null )
                this.PreferredAddress = null;
            else
                this.PreferredAddress = rs.getString("PreferredAddress").trim();

            if( rs.getString("PreferredPhoneNum") == null )
                this.PreferredPhoneNum = null;
            else
                this.PreferredPhoneNum = rs.getString("PreferredPhoneNum").trim();

            if( rs.getString("PhoneInterCode") == null )
                this.PhoneInterCode = null;
            else
                this.PhoneInterCode = rs.getString("PhoneInterCode").trim();

            if( rs.getString("PhoneDomesticCode") == null )
                this.PhoneDomesticCode = null;
            else
                this.PhoneDomesticCode = rs.getString("PhoneDomesticCode").trim();

            if( rs.getString("MobileInterCode") == null )
                this.MobileInterCode = null;
            else
                this.MobileInterCode = rs.getString("MobileInterCode").trim();

            if( rs.getString("Address") == null )
                this.Address = null;
            else
                this.Address = rs.getString("Address").trim();

        }
        catch(SQLException sqle) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LCAddressSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = ExceptionUtils.exceptionToString(sqle);
            this.mErrors .addOneError(tError);
            return false;
        }
        return true;
    }

    public LCAddressSchema getSchema() {
        LCAddressSchema aLCAddressSchema = new LCAddressSchema();
        aLCAddressSchema.setSchema(this);
        return aLCAddressSchema;
    }

    public LCAddressDB getDB() {
        LCAddressDB aDBOper = new LCAddressDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
    * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLCAddress描述/A>表字段
    * @return: String 返回打包后字符串
    **/
    public String encode() {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(ChgData.chgData(AddressID));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(PersonID));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ShardingID)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(CustomerNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(AddressNo));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(PostalAddress)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ZipCode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Phone)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Fax)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(HomeAddress)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(HomeZipCode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(HomePhone)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(HomeFax)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(CompanyAddress)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(CompanyZipCode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(CompanyPhone)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(CompanyFax)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Mobile)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MobileChs)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(EMail)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(BP)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Mobile2)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MobileChs2)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(EMail2)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(BP2)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ModifyTime)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(GrpName)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Province)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(City)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(County)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ZoneCode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StoreNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StoreNo2)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(PreferredAddress)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(PreferredPhoneNum)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(PhoneInterCode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(PhoneDomesticCode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MobileInterCode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Address));
        return strReturn.toString();
    }

    /**
    * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLCAddress>历史记账凭证主表信息</A>表字段
    * @param: strMessage String 包含一条纪录数据的字符串
    * @return: boolean
    **/
    public boolean decode(String strMessage) {
        try {
            AddressID = new Long(ChgData.chgNumericStr(StrTool.getStr(strMessage,1,SysConst.PACKAGESPILTER))).longValue();
            PersonID = new Long(ChgData.chgNumericStr(StrTool.getStr(strMessage,2,SysConst.PACKAGESPILTER))).longValue();
            ShardingID = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
            CustomerNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
            AddressNo = new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,5,SysConst.PACKAGESPILTER))).intValue();
            PostalAddress = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
            ZipCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
            Phone = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
            Fax = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
            HomeAddress = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
            HomeZipCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
            HomePhone = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
            HomeFax = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
            CompanyAddress = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER );
            CompanyZipCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15, SysConst.PACKAGESPILTER );
            CompanyPhone = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16, SysConst.PACKAGESPILTER );
            CompanyFax = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17, SysConst.PACKAGESPILTER );
            Mobile = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18, SysConst.PACKAGESPILTER );
            MobileChs = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19, SysConst.PACKAGESPILTER );
            EMail = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20, SysConst.PACKAGESPILTER );
            BP = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 21, SysConst.PACKAGESPILTER );
            Mobile2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 22, SysConst.PACKAGESPILTER );
            MobileChs2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 23, SysConst.PACKAGESPILTER );
            EMail2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 24, SysConst.PACKAGESPILTER );
            BP2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 25, SysConst.PACKAGESPILTER );
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 26, SysConst.PACKAGESPILTER );
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 27,SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 28, SysConst.PACKAGESPILTER );
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 29,SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 30, SysConst.PACKAGESPILTER );
            GrpName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 31, SysConst.PACKAGESPILTER );
            Province = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 32, SysConst.PACKAGESPILTER );
            City = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 33, SysConst.PACKAGESPILTER );
            County = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 34, SysConst.PACKAGESPILTER );
            ZoneCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 35, SysConst.PACKAGESPILTER );
            StoreNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 36, SysConst.PACKAGESPILTER );
            StoreNo2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 37, SysConst.PACKAGESPILTER );
            PreferredAddress = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 38, SysConst.PACKAGESPILTER );
            PreferredPhoneNum = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 39, SysConst.PACKAGESPILTER );
            PhoneInterCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 40, SysConst.PACKAGESPILTER );
            PhoneDomesticCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 41, SysConst.PACKAGESPILTER );
            MobileInterCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 42, SysConst.PACKAGESPILTER );
            Address = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 43, SysConst.PACKAGESPILTER );
        }
        catch(NumberFormatException ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LCAddressSchema";
            tError.functionName = "decode";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            return false;
        }
        return true;
    }

    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("AddressID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AddressID));
        }
        if (FCode.equalsIgnoreCase("PersonID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PersonID));
        }
        if (FCode.equalsIgnoreCase("ShardingID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ShardingID));
        }
        if (FCode.equalsIgnoreCase("CustomerNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CustomerNo));
        }
        if (FCode.equalsIgnoreCase("AddressNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AddressNo));
        }
        if (FCode.equalsIgnoreCase("PostalAddress")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PostalAddress));
        }
        if (FCode.equalsIgnoreCase("ZipCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ZipCode));
        }
        if (FCode.equalsIgnoreCase("Phone")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Phone));
        }
        if (FCode.equalsIgnoreCase("Fax")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Fax));
        }
        if (FCode.equalsIgnoreCase("HomeAddress")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(HomeAddress));
        }
        if (FCode.equalsIgnoreCase("HomeZipCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(HomeZipCode));
        }
        if (FCode.equalsIgnoreCase("HomePhone")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(HomePhone));
        }
        if (FCode.equalsIgnoreCase("HomeFax")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(HomeFax));
        }
        if (FCode.equalsIgnoreCase("CompanyAddress")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CompanyAddress));
        }
        if (FCode.equalsIgnoreCase("CompanyZipCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CompanyZipCode));
        }
        if (FCode.equalsIgnoreCase("CompanyPhone")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CompanyPhone));
        }
        if (FCode.equalsIgnoreCase("CompanyFax")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CompanyFax));
        }
        if (FCode.equalsIgnoreCase("Mobile")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Mobile));
        }
        if (FCode.equalsIgnoreCase("MobileChs")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MobileChs));
        }
        if (FCode.equalsIgnoreCase("EMail")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(EMail));
        }
        if (FCode.equalsIgnoreCase("BP")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BP));
        }
        if (FCode.equalsIgnoreCase("Mobile2")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Mobile2));
        }
        if (FCode.equalsIgnoreCase("MobileChs2")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MobileChs2));
        }
        if (FCode.equalsIgnoreCase("EMail2")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(EMail2));
        }
        if (FCode.equalsIgnoreCase("BP2")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BP2));
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (FCode.equalsIgnoreCase("GrpName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GrpName));
        }
        if (FCode.equalsIgnoreCase("Province")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Province));
        }
        if (FCode.equalsIgnoreCase("City")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(City));
        }
        if (FCode.equalsIgnoreCase("County")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(County));
        }
        if (FCode.equalsIgnoreCase("ZoneCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ZoneCode));
        }
        if (FCode.equalsIgnoreCase("StoreNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StoreNo));
        }
        if (FCode.equalsIgnoreCase("StoreNo2")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StoreNo2));
        }
        if (FCode.equalsIgnoreCase("PreferredAddress")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PreferredAddress));
        }
        if (FCode.equalsIgnoreCase("PreferredPhoneNum")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PreferredPhoneNum));
        }
        if (FCode.equalsIgnoreCase("PhoneInterCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PhoneInterCode));
        }
        if (FCode.equalsIgnoreCase("PhoneDomesticCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PhoneDomesticCode));
        }
        if (FCode.equalsIgnoreCase("MobileInterCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MobileInterCode));
        }
        if (FCode.equalsIgnoreCase("Address")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Address));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = String.valueOf(AddressID);
                break;
            case 1:
                strFieldValue = String.valueOf(PersonID);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(ShardingID);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(CustomerNo);
                break;
            case 4:
                strFieldValue = String.valueOf(AddressNo);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(PostalAddress);
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(ZipCode);
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(Phone);
                break;
            case 8:
                strFieldValue = StrTool.GBKToUnicode(Fax);
                break;
            case 9:
                strFieldValue = StrTool.GBKToUnicode(HomeAddress);
                break;
            case 10:
                strFieldValue = StrTool.GBKToUnicode(HomeZipCode);
                break;
            case 11:
                strFieldValue = StrTool.GBKToUnicode(HomePhone);
                break;
            case 12:
                strFieldValue = StrTool.GBKToUnicode(HomeFax);
                break;
            case 13:
                strFieldValue = StrTool.GBKToUnicode(CompanyAddress);
                break;
            case 14:
                strFieldValue = StrTool.GBKToUnicode(CompanyZipCode);
                break;
            case 15:
                strFieldValue = StrTool.GBKToUnicode(CompanyPhone);
                break;
            case 16:
                strFieldValue = StrTool.GBKToUnicode(CompanyFax);
                break;
            case 17:
                strFieldValue = StrTool.GBKToUnicode(Mobile);
                break;
            case 18:
                strFieldValue = StrTool.GBKToUnicode(MobileChs);
                break;
            case 19:
                strFieldValue = StrTool.GBKToUnicode(EMail);
                break;
            case 20:
                strFieldValue = StrTool.GBKToUnicode(BP);
                break;
            case 21:
                strFieldValue = StrTool.GBKToUnicode(Mobile2);
                break;
            case 22:
                strFieldValue = StrTool.GBKToUnicode(MobileChs2);
                break;
            case 23:
                strFieldValue = StrTool.GBKToUnicode(EMail2);
                break;
            case 24:
                strFieldValue = StrTool.GBKToUnicode(BP2);
                break;
            case 25:
                strFieldValue = StrTool.GBKToUnicode(Operator);
                break;
            case 26:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
                break;
            case 27:
                strFieldValue = StrTool.GBKToUnicode(MakeTime);
                break;
            case 28:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
                break;
            case 29:
                strFieldValue = StrTool.GBKToUnicode(ModifyTime);
                break;
            case 30:
                strFieldValue = StrTool.GBKToUnicode(GrpName);
                break;
            case 31:
                strFieldValue = StrTool.GBKToUnicode(Province);
                break;
            case 32:
                strFieldValue = StrTool.GBKToUnicode(City);
                break;
            case 33:
                strFieldValue = StrTool.GBKToUnicode(County);
                break;
            case 34:
                strFieldValue = StrTool.GBKToUnicode(ZoneCode);
                break;
            case 35:
                strFieldValue = StrTool.GBKToUnicode(StoreNo);
                break;
            case 36:
                strFieldValue = StrTool.GBKToUnicode(StoreNo2);
                break;
            case 37:
                strFieldValue = StrTool.GBKToUnicode(PreferredAddress);
                break;
            case 38:
                strFieldValue = StrTool.GBKToUnicode(PreferredPhoneNum);
                break;
            case 39:
                strFieldValue = StrTool.GBKToUnicode(PhoneInterCode);
                break;
            case 40:
                strFieldValue = StrTool.GBKToUnicode(PhoneDomesticCode);
                break;
            case 41:
                strFieldValue = StrTool.GBKToUnicode(MobileInterCode);
                break;
            case 42:
                strFieldValue = StrTool.GBKToUnicode(Address);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("AddressID")) {
            if( FValue != null && !FValue.equals("")) {
                AddressID = new Long(FValue).longValue();
            }
        }
        if (FCode.equalsIgnoreCase("PersonID")) {
            if( FValue != null && !FValue.equals("")) {
                PersonID = new Long(FValue).longValue();
            }
        }
        if (FCode.equalsIgnoreCase("ShardingID")) {
            if( FValue != null && !FValue.equals(""))
            {
                ShardingID = FValue.trim();
            }
            else
                ShardingID = null;
        }
        if (FCode.equalsIgnoreCase("CustomerNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                CustomerNo = FValue.trim();
            }
            else
                CustomerNo = null;
        }
        if (FCode.equalsIgnoreCase("AddressNo")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                AddressNo = i;
            }
        }
        if (FCode.equalsIgnoreCase("PostalAddress")) {
            if( FValue != null && !FValue.equals(""))
            {
                PostalAddress = FValue.trim();
            }
            else
                PostalAddress = null;
        }
        if (FCode.equalsIgnoreCase("ZipCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                ZipCode = FValue.trim();
            }
            else
                ZipCode = null;
        }
        if (FCode.equalsIgnoreCase("Phone")) {
            if( FValue != null && !FValue.equals(""))
            {
                Phone = FValue.trim();
            }
            else
                Phone = null;
        }
        if (FCode.equalsIgnoreCase("Fax")) {
            if( FValue != null && !FValue.equals(""))
            {
                Fax = FValue.trim();
            }
            else
                Fax = null;
        }
        if (FCode.equalsIgnoreCase("HomeAddress")) {
            if( FValue != null && !FValue.equals(""))
            {
                HomeAddress = FValue.trim();
            }
            else
                HomeAddress = null;
        }
        if (FCode.equalsIgnoreCase("HomeZipCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                HomeZipCode = FValue.trim();
            }
            else
                HomeZipCode = null;
        }
        if (FCode.equalsIgnoreCase("HomePhone")) {
            if( FValue != null && !FValue.equals(""))
            {
                HomePhone = FValue.trim();
            }
            else
                HomePhone = null;
        }
        if (FCode.equalsIgnoreCase("HomeFax")) {
            if( FValue != null && !FValue.equals(""))
            {
                HomeFax = FValue.trim();
            }
            else
                HomeFax = null;
        }
        if (FCode.equalsIgnoreCase("CompanyAddress")) {
            if( FValue != null && !FValue.equals(""))
            {
                CompanyAddress = FValue.trim();
            }
            else
                CompanyAddress = null;
        }
        if (FCode.equalsIgnoreCase("CompanyZipCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                CompanyZipCode = FValue.trim();
            }
            else
                CompanyZipCode = null;
        }
        if (FCode.equalsIgnoreCase("CompanyPhone")) {
            if( FValue != null && !FValue.equals(""))
            {
                CompanyPhone = FValue.trim();
            }
            else
                CompanyPhone = null;
        }
        if (FCode.equalsIgnoreCase("CompanyFax")) {
            if( FValue != null && !FValue.equals(""))
            {
                CompanyFax = FValue.trim();
            }
            else
                CompanyFax = null;
        }
        if (FCode.equalsIgnoreCase("Mobile")) {
            if( FValue != null && !FValue.equals(""))
            {
                Mobile = FValue.trim();
            }
            else
                Mobile = null;
        }
        if (FCode.equalsIgnoreCase("MobileChs")) {
            if( FValue != null && !FValue.equals(""))
            {
                MobileChs = FValue.trim();
            }
            else
                MobileChs = null;
        }
        if (FCode.equalsIgnoreCase("EMail")) {
            if( FValue != null && !FValue.equals(""))
            {
                EMail = FValue.trim();
            }
            else
                EMail = null;
        }
        if (FCode.equalsIgnoreCase("BP")) {
            if( FValue != null && !FValue.equals(""))
            {
                BP = FValue.trim();
            }
            else
                BP = null;
        }
        if (FCode.equalsIgnoreCase("Mobile2")) {
            if( FValue != null && !FValue.equals(""))
            {
                Mobile2 = FValue.trim();
            }
            else
                Mobile2 = null;
        }
        if (FCode.equalsIgnoreCase("MobileChs2")) {
            if( FValue != null && !FValue.equals(""))
            {
                MobileChs2 = FValue.trim();
            }
            else
                MobileChs2 = null;
        }
        if (FCode.equalsIgnoreCase("EMail2")) {
            if( FValue != null && !FValue.equals(""))
            {
                EMail2 = FValue.trim();
            }
            else
                EMail2 = null;
        }
        if (FCode.equalsIgnoreCase("BP2")) {
            if( FValue != null && !FValue.equals(""))
            {
                BP2 = FValue.trim();
            }
            else
                BP2 = null;
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            if( FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
                Operator = null;
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if(FValue != null && !FValue.equals("")) {
                MakeDate = fDate.getDate( FValue );
            }
            else
                MakeDate = null;
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
                MakeTime = null;
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if(FValue != null && !FValue.equals("")) {
                ModifyDate = fDate.getDate( FValue );
            }
            else
                ModifyDate = null;
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
                ModifyTime = null;
        }
        if (FCode.equalsIgnoreCase("GrpName")) {
            if( FValue != null && !FValue.equals(""))
            {
                GrpName = FValue.trim();
            }
            else
                GrpName = null;
        }
        if (FCode.equalsIgnoreCase("Province")) {
            if( FValue != null && !FValue.equals(""))
            {
                Province = FValue.trim();
            }
            else
                Province = null;
        }
        if (FCode.equalsIgnoreCase("City")) {
            if( FValue != null && !FValue.equals(""))
            {
                City = FValue.trim();
            }
            else
                City = null;
        }
        if (FCode.equalsIgnoreCase("County")) {
            if( FValue != null && !FValue.equals(""))
            {
                County = FValue.trim();
            }
            else
                County = null;
        }
        if (FCode.equalsIgnoreCase("ZoneCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                ZoneCode = FValue.trim();
            }
            else
                ZoneCode = null;
        }
        if (FCode.equalsIgnoreCase("StoreNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                StoreNo = FValue.trim();
            }
            else
                StoreNo = null;
        }
        if (FCode.equalsIgnoreCase("StoreNo2")) {
            if( FValue != null && !FValue.equals(""))
            {
                StoreNo2 = FValue.trim();
            }
            else
                StoreNo2 = null;
        }
        if (FCode.equalsIgnoreCase("PreferredAddress")) {
            if( FValue != null && !FValue.equals(""))
            {
                PreferredAddress = FValue.trim();
            }
            else
                PreferredAddress = null;
        }
        if (FCode.equalsIgnoreCase("PreferredPhoneNum")) {
            if( FValue != null && !FValue.equals(""))
            {
                PreferredPhoneNum = FValue.trim();
            }
            else
                PreferredPhoneNum = null;
        }
        if (FCode.equalsIgnoreCase("PhoneInterCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                PhoneInterCode = FValue.trim();
            }
            else
                PhoneInterCode = null;
        }
        if (FCode.equalsIgnoreCase("PhoneDomesticCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                PhoneDomesticCode = FValue.trim();
            }
            else
                PhoneDomesticCode = null;
        }
        if (FCode.equalsIgnoreCase("MobileInterCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                MobileInterCode = FValue.trim();
            }
            else
                MobileInterCode = null;
        }
        if (FCode.equalsIgnoreCase("Address")) {
            if( FValue != null && !FValue.equals(""))
            {
                Address = FValue.trim();
            }
            else
                Address = null;
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject) return true;
        if (otherObject == null) return false;
        if (getClass() != otherObject.getClass()) return false;
        LCAddressSchema other = (LCAddressSchema)otherObject;
        return
            AddressID == other.getAddressID()
            && PersonID == other.getPersonID()
            && ShardingID.equals(other.getShardingID())
            && CustomerNo.equals(other.getCustomerNo())
            && AddressNo == other.getAddressNo()
            && PostalAddress.equals(other.getPostalAddress())
            && ZipCode.equals(other.getZipCode())
            && Phone.equals(other.getPhone())
            && Fax.equals(other.getFax())
            && HomeAddress.equals(other.getHomeAddress())
            && HomeZipCode.equals(other.getHomeZipCode())
            && HomePhone.equals(other.getHomePhone())
            && HomeFax.equals(other.getHomeFax())
            && CompanyAddress.equals(other.getCompanyAddress())
            && CompanyZipCode.equals(other.getCompanyZipCode())
            && CompanyPhone.equals(other.getCompanyPhone())
            && CompanyFax.equals(other.getCompanyFax())
            && Mobile.equals(other.getMobile())
            && MobileChs.equals(other.getMobileChs())
            && EMail.equals(other.getEMail())
            && BP.equals(other.getBP())
            && Mobile2.equals(other.getMobile2())
            && MobileChs2.equals(other.getMobileChs2())
            && EMail2.equals(other.getEMail2())
            && BP2.equals(other.getBP2())
            && Operator.equals(other.getOperator())
            && fDate.getString(MakeDate).equals(other.getMakeDate())
            && MakeTime.equals(other.getMakeTime())
            && fDate.getString(ModifyDate).equals(other.getModifyDate())
            && ModifyTime.equals(other.getModifyTime())
            && GrpName.equals(other.getGrpName())
            && Province.equals(other.getProvince())
            && City.equals(other.getCity())
            && County.equals(other.getCounty())
            && ZoneCode.equals(other.getZoneCode())
            && StoreNo.equals(other.getStoreNo())
            && StoreNo2.equals(other.getStoreNo2())
            && PreferredAddress.equals(other.getPreferredAddress())
            && PreferredPhoneNum.equals(other.getPreferredPhoneNum())
            && PhoneInterCode.equals(other.getPhoneInterCode())
            && PhoneDomesticCode.equals(other.getPhoneDomesticCode())
            && MobileInterCode.equals(other.getMobileInterCode())
            && Address.equals(other.getAddress());
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("AddressID") ) {
            return 0;
        }
        if( strFieldName.equals("PersonID") ) {
            return 1;
        }
        if( strFieldName.equals("ShardingID") ) {
            return 2;
        }
        if( strFieldName.equals("CustomerNo") ) {
            return 3;
        }
        if( strFieldName.equals("AddressNo") ) {
            return 4;
        }
        if( strFieldName.equals("PostalAddress") ) {
            return 5;
        }
        if( strFieldName.equals("ZipCode") ) {
            return 6;
        }
        if( strFieldName.equals("Phone") ) {
            return 7;
        }
        if( strFieldName.equals("Fax") ) {
            return 8;
        }
        if( strFieldName.equals("HomeAddress") ) {
            return 9;
        }
        if( strFieldName.equals("HomeZipCode") ) {
            return 10;
        }
        if( strFieldName.equals("HomePhone") ) {
            return 11;
        }
        if( strFieldName.equals("HomeFax") ) {
            return 12;
        }
        if( strFieldName.equals("CompanyAddress") ) {
            return 13;
        }
        if( strFieldName.equals("CompanyZipCode") ) {
            return 14;
        }
        if( strFieldName.equals("CompanyPhone") ) {
            return 15;
        }
        if( strFieldName.equals("CompanyFax") ) {
            return 16;
        }
        if( strFieldName.equals("Mobile") ) {
            return 17;
        }
        if( strFieldName.equals("MobileChs") ) {
            return 18;
        }
        if( strFieldName.equals("EMail") ) {
            return 19;
        }
        if( strFieldName.equals("BP") ) {
            return 20;
        }
        if( strFieldName.equals("Mobile2") ) {
            return 21;
        }
        if( strFieldName.equals("MobileChs2") ) {
            return 22;
        }
        if( strFieldName.equals("EMail2") ) {
            return 23;
        }
        if( strFieldName.equals("BP2") ) {
            return 24;
        }
        if( strFieldName.equals("Operator") ) {
            return 25;
        }
        if( strFieldName.equals("MakeDate") ) {
            return 26;
        }
        if( strFieldName.equals("MakeTime") ) {
            return 27;
        }
        if( strFieldName.equals("ModifyDate") ) {
            return 28;
        }
        if( strFieldName.equals("ModifyTime") ) {
            return 29;
        }
        if( strFieldName.equals("GrpName") ) {
            return 30;
        }
        if( strFieldName.equals("Province") ) {
            return 31;
        }
        if( strFieldName.equals("City") ) {
            return 32;
        }
        if( strFieldName.equals("County") ) {
            return 33;
        }
        if( strFieldName.equals("ZoneCode") ) {
            return 34;
        }
        if( strFieldName.equals("StoreNo") ) {
            return 35;
        }
        if( strFieldName.equals("StoreNo2") ) {
            return 36;
        }
        if( strFieldName.equals("PreferredAddress") ) {
            return 37;
        }
        if( strFieldName.equals("PreferredPhoneNum") ) {
            return 38;
        }
        if( strFieldName.equals("PhoneInterCode") ) {
            return 39;
        }
        if( strFieldName.equals("PhoneDomesticCode") ) {
            return 40;
        }
        if( strFieldName.equals("MobileInterCode") ) {
            return 41;
        }
        if( strFieldName.equals("Address") ) {
            return 42;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "AddressID";
                break;
            case 1:
                strFieldName = "PersonID";
                break;
            case 2:
                strFieldName = "ShardingID";
                break;
            case 3:
                strFieldName = "CustomerNo";
                break;
            case 4:
                strFieldName = "AddressNo";
                break;
            case 5:
                strFieldName = "PostalAddress";
                break;
            case 6:
                strFieldName = "ZipCode";
                break;
            case 7:
                strFieldName = "Phone";
                break;
            case 8:
                strFieldName = "Fax";
                break;
            case 9:
                strFieldName = "HomeAddress";
                break;
            case 10:
                strFieldName = "HomeZipCode";
                break;
            case 11:
                strFieldName = "HomePhone";
                break;
            case 12:
                strFieldName = "HomeFax";
                break;
            case 13:
                strFieldName = "CompanyAddress";
                break;
            case 14:
                strFieldName = "CompanyZipCode";
                break;
            case 15:
                strFieldName = "CompanyPhone";
                break;
            case 16:
                strFieldName = "CompanyFax";
                break;
            case 17:
                strFieldName = "Mobile";
                break;
            case 18:
                strFieldName = "MobileChs";
                break;
            case 19:
                strFieldName = "EMail";
                break;
            case 20:
                strFieldName = "BP";
                break;
            case 21:
                strFieldName = "Mobile2";
                break;
            case 22:
                strFieldName = "MobileChs2";
                break;
            case 23:
                strFieldName = "EMail2";
                break;
            case 24:
                strFieldName = "BP2";
                break;
            case 25:
                strFieldName = "Operator";
                break;
            case 26:
                strFieldName = "MakeDate";
                break;
            case 27:
                strFieldName = "MakeTime";
                break;
            case 28:
                strFieldName = "ModifyDate";
                break;
            case 29:
                strFieldName = "ModifyTime";
                break;
            case 30:
                strFieldName = "GrpName";
                break;
            case 31:
                strFieldName = "Province";
                break;
            case 32:
                strFieldName = "City";
                break;
            case 33:
                strFieldName = "County";
                break;
            case 34:
                strFieldName = "ZoneCode";
                break;
            case 35:
                strFieldName = "StoreNo";
                break;
            case 36:
                strFieldName = "StoreNo2";
                break;
            case 37:
                strFieldName = "PreferredAddress";
                break;
            case 38:
                strFieldName = "PreferredPhoneNum";
                break;
            case 39:
                strFieldName = "PhoneInterCode";
                break;
            case 40:
                strFieldName = "PhoneDomesticCode";
                break;
            case 41:
                strFieldName = "MobileInterCode";
                break;
            case 42:
                strFieldName = "Address";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "ADDRESSID":
                return Schema.TYPE_LONG;
            case "PERSONID":
                return Schema.TYPE_LONG;
            case "SHARDINGID":
                return Schema.TYPE_STRING;
            case "CUSTOMERNO":
                return Schema.TYPE_STRING;
            case "ADDRESSNO":
                return Schema.TYPE_INT;
            case "POSTALADDRESS":
                return Schema.TYPE_STRING;
            case "ZIPCODE":
                return Schema.TYPE_STRING;
            case "PHONE":
                return Schema.TYPE_STRING;
            case "FAX":
                return Schema.TYPE_STRING;
            case "HOMEADDRESS":
                return Schema.TYPE_STRING;
            case "HOMEZIPCODE":
                return Schema.TYPE_STRING;
            case "HOMEPHONE":
                return Schema.TYPE_STRING;
            case "HOMEFAX":
                return Schema.TYPE_STRING;
            case "COMPANYADDRESS":
                return Schema.TYPE_STRING;
            case "COMPANYZIPCODE":
                return Schema.TYPE_STRING;
            case "COMPANYPHONE":
                return Schema.TYPE_STRING;
            case "COMPANYFAX":
                return Schema.TYPE_STRING;
            case "MOBILE":
                return Schema.TYPE_STRING;
            case "MOBILECHS":
                return Schema.TYPE_STRING;
            case "EMAIL":
                return Schema.TYPE_STRING;
            case "BP":
                return Schema.TYPE_STRING;
            case "MOBILE2":
                return Schema.TYPE_STRING;
            case "MOBILECHS2":
                return Schema.TYPE_STRING;
            case "EMAIL2":
                return Schema.TYPE_STRING;
            case "BP2":
                return Schema.TYPE_STRING;
            case "OPERATOR":
                return Schema.TYPE_STRING;
            case "MAKEDATE":
                return Schema.TYPE_DATE;
            case "MAKETIME":
                return Schema.TYPE_STRING;
            case "MODIFYDATE":
                return Schema.TYPE_DATE;
            case "MODIFYTIME":
                return Schema.TYPE_STRING;
            case "GRPNAME":
                return Schema.TYPE_STRING;
            case "PROVINCE":
                return Schema.TYPE_STRING;
            case "CITY":
                return Schema.TYPE_STRING;
            case "COUNTY":
                return Schema.TYPE_STRING;
            case "ZONECODE":
                return Schema.TYPE_STRING;
            case "STORENO":
                return Schema.TYPE_STRING;
            case "STORENO2":
                return Schema.TYPE_STRING;
            case "PREFERREDADDRESS":
                return Schema.TYPE_STRING;
            case "PREFERREDPHONENUM":
                return Schema.TYPE_STRING;
            case "PHONEINTERCODE":
                return Schema.TYPE_STRING;
            case "PHONEDOMESTICCODE":
                return Schema.TYPE_STRING;
            case "MOBILEINTERCODE":
                return Schema.TYPE_STRING;
            case "ADDRESS":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_LONG;
            case 1:
                return Schema.TYPE_LONG;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_INT;
            case 5:
                return Schema.TYPE_STRING;
            case 6:
                return Schema.TYPE_STRING;
            case 7:
                return Schema.TYPE_STRING;
            case 8:
                return Schema.TYPE_STRING;
            case 9:
                return Schema.TYPE_STRING;
            case 10:
                return Schema.TYPE_STRING;
            case 11:
                return Schema.TYPE_STRING;
            case 12:
                return Schema.TYPE_STRING;
            case 13:
                return Schema.TYPE_STRING;
            case 14:
                return Schema.TYPE_STRING;
            case 15:
                return Schema.TYPE_STRING;
            case 16:
                return Schema.TYPE_STRING;
            case 17:
                return Schema.TYPE_STRING;
            case 18:
                return Schema.TYPE_STRING;
            case 19:
                return Schema.TYPE_STRING;
            case 20:
                return Schema.TYPE_STRING;
            case 21:
                return Schema.TYPE_STRING;
            case 22:
                return Schema.TYPE_STRING;
            case 23:
                return Schema.TYPE_STRING;
            case 24:
                return Schema.TYPE_STRING;
            case 25:
                return Schema.TYPE_STRING;
            case 26:
                return Schema.TYPE_DATE;
            case 27:
                return Schema.TYPE_STRING;
            case 28:
                return Schema.TYPE_DATE;
            case 29:
                return Schema.TYPE_STRING;
            case 30:
                return Schema.TYPE_STRING;
            case 31:
                return Schema.TYPE_STRING;
            case 32:
                return Schema.TYPE_STRING;
            case 33:
                return Schema.TYPE_STRING;
            case 34:
                return Schema.TYPE_STRING;
            case 35:
                return Schema.TYPE_STRING;
            case 36:
                return Schema.TYPE_STRING;
            case 37:
                return Schema.TYPE_STRING;
            case 38:
                return Schema.TYPE_STRING;
            case 39:
                return Schema.TYPE_STRING;
            case 40:
                return Schema.TYPE_STRING;
            case 41:
                return Schema.TYPE_STRING;
            case 42:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
}
