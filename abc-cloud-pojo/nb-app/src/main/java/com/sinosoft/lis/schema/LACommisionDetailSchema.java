/**
 * Copyright (c) 2017 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import com.sinosoft.lis.db.LACommisionDetailDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.*;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

/**
 * <p>ClassName: LACommisionDetailSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2017</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2017-10-21
 */
public class LACommisionDetailSchema implements Schema, Cloneable {
    // @Field
    /** Id */
    private long CommisionDetailID;
    /** Shardingid */
    private String ShardingID;
    /** 集体合同号码 */
    private String GrpContNo;
    /** 代理人编码 */
    private String AgentCode;
    /** 业务百分比 */
    private double BusiRate;
    /** 操作员 */
    private String Operator;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 最后一次修改日期 */
    private Date ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime;
    /** 代理人组别 */
    private String AgentGroup;
    /** 保单类型 */
    private String PolType;
    /** 服务起期 */
    private Date StartSerDate;
    /** 服务止期 */
    private Date EndSerDate;
    /** Agentflag */
    private String AgentFlag;
    /** Salesrate */
    private double SalesRate;
    /** 与投保人关系 */
    private String RelationShip;

    public static final int FIELDNUM = 17;    // 数据库表的字段个数

    private static String[] PK;                // 主键

    private FDate fDate = new FDate();        // 处理日期

    public CErrors mErrors;            // 错误信息

    // @Constructor
    public LACommisionDetailSchema() {
        mErrors = new CErrors();

        String[] pk = new String[1];
        pk[0] = "CommisionDetailID";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException {
        LACommisionDetailSchema cloned = (LACommisionDetailSchema)super.clone();
        cloned.fDate = (FDate) fDate.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK() {
        return PK;
    }

    public long getCommisionDetailID() {
        return CommisionDetailID;
    }
    public void setCommisionDetailID(long aCommisionDetailID) {
        CommisionDetailID = aCommisionDetailID;
    }
    public void setCommisionDetailID(String aCommisionDetailID) {
        if (aCommisionDetailID != null && !aCommisionDetailID.equals("")) {
            CommisionDetailID = new Long(aCommisionDetailID).longValue();
        }
    }

    public String getShardingID() {
        return ShardingID;
    }
    public void setShardingID(String aShardingID) {
        ShardingID = aShardingID;
    }
    public String getGrpContNo() {
        return GrpContNo;
    }
    public void setGrpContNo(String aGrpContNo) {
        GrpContNo = aGrpContNo;
    }
    public String getAgentCode() {
        return AgentCode;
    }
    public void setAgentCode(String aAgentCode) {
        AgentCode = aAgentCode;
    }
    public double getBusiRate() {
        return BusiRate;
    }
    public void setBusiRate(double aBusiRate) {
        BusiRate = aBusiRate;
    }
    public void setBusiRate(String aBusiRate) {
        if (aBusiRate != null && !aBusiRate.equals("")) {
            Double tDouble = new Double(aBusiRate);
            double d = tDouble.doubleValue();
            BusiRate = d;
        }
    }

    public String getOperator() {
        return Operator;
    }
    public void setOperator(String aOperator) {
        Operator = aOperator;
    }
    public String getMakeDate() {
        if(MakeDate != null) {
            return fDate.getString(MakeDate);
        } else {
            return null;
        }
    }
    public void setMakeDate(Date aMakeDate) {
        MakeDate = aMakeDate;
    }
    public void setMakeDate(String aMakeDate) {
        if (aMakeDate != null && !aMakeDate.equals("")) {
            MakeDate = fDate.getDate(aMakeDate);
        } else
            MakeDate = null;
    }

    public String getMakeTime() {
        return MakeTime;
    }
    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }
    public String getModifyDate() {
        if(ModifyDate != null) {
            return fDate.getString(ModifyDate);
        } else {
            return null;
        }
    }
    public void setModifyDate(Date aModifyDate) {
        ModifyDate = aModifyDate;
    }
    public void setModifyDate(String aModifyDate) {
        if (aModifyDate != null && !aModifyDate.equals("")) {
            ModifyDate = fDate.getDate(aModifyDate);
        } else
            ModifyDate = null;
    }

    public String getModifyTime() {
        return ModifyTime;
    }
    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }
    public String getAgentGroup() {
        return AgentGroup;
    }
    public void setAgentGroup(String aAgentGroup) {
        AgentGroup = aAgentGroup;
    }
    public String getPolType() {
        return PolType;
    }
    public void setPolType(String aPolType) {
        PolType = aPolType;
    }
    public String getStartSerDate() {
        if(StartSerDate != null) {
            return fDate.getString(StartSerDate);
        } else {
            return null;
        }
    }
    public void setStartSerDate(Date aStartSerDate) {
        StartSerDate = aStartSerDate;
    }
    public void setStartSerDate(String aStartSerDate) {
        if (aStartSerDate != null && !aStartSerDate.equals("")) {
            StartSerDate = fDate.getDate(aStartSerDate);
        } else
            StartSerDate = null;
    }

    public String getEndSerDate() {
        if(EndSerDate != null) {
            return fDate.getString(EndSerDate);
        } else {
            return null;
        }
    }
    public void setEndSerDate(Date aEndSerDate) {
        EndSerDate = aEndSerDate;
    }
    public void setEndSerDate(String aEndSerDate) {
        if (aEndSerDate != null && !aEndSerDate.equals("")) {
            EndSerDate = fDate.getDate(aEndSerDate);
        } else
            EndSerDate = null;
    }

    public String getAgentFlag() {
        return AgentFlag;
    }
    public void setAgentFlag(String aAgentFlag) {
        AgentFlag = aAgentFlag;
    }
    public double getSalesRate() {
        return SalesRate;
    }
    public void setSalesRate(double aSalesRate) {
        SalesRate = aSalesRate;
    }
    public void setSalesRate(String aSalesRate) {
        if (aSalesRate != null && !aSalesRate.equals("")) {
            Double tDouble = new Double(aSalesRate);
            double d = tDouble.doubleValue();
            SalesRate = d;
        }
    }

    public String getRelationShip() {
        return RelationShip;
    }
    public void setRelationShip(String aRelationShip) {
        RelationShip = aRelationShip;
    }

    /**
    * 使用另外一个 LACommisionDetailSchema 对象给 Schema 赋值
    * @param: aLACommisionDetailSchema LACommisionDetailSchema
    **/
    public void setSchema(LACommisionDetailSchema aLACommisionDetailSchema) {
        this.CommisionDetailID = aLACommisionDetailSchema.getCommisionDetailID();
        this.ShardingID = aLACommisionDetailSchema.getShardingID();
        this.GrpContNo = aLACommisionDetailSchema.getGrpContNo();
        this.AgentCode = aLACommisionDetailSchema.getAgentCode();
        this.BusiRate = aLACommisionDetailSchema.getBusiRate();
        this.Operator = aLACommisionDetailSchema.getOperator();
        this.MakeDate = fDate.getDate( aLACommisionDetailSchema.getMakeDate());
        this.MakeTime = aLACommisionDetailSchema.getMakeTime();
        this.ModifyDate = fDate.getDate( aLACommisionDetailSchema.getModifyDate());
        this.ModifyTime = aLACommisionDetailSchema.getModifyTime();
        this.AgentGroup = aLACommisionDetailSchema.getAgentGroup();
        this.PolType = aLACommisionDetailSchema.getPolType();
        this.StartSerDate = fDate.getDate( aLACommisionDetailSchema.getStartSerDate());
        this.EndSerDate = fDate.getDate( aLACommisionDetailSchema.getEndSerDate());
        this.AgentFlag = aLACommisionDetailSchema.getAgentFlag();
        this.SalesRate = aLACommisionDetailSchema.getSalesRate();
        this.RelationShip = aLACommisionDetailSchema.getRelationShip();
    }

    /**
    * 使用 ResultSet 中的第 i 行给 Schema 赋值
    * @param: rs ResultSet
    * @param: i int
    * @return: boolean
    **/
    public boolean setSchema(ResultSet rs,int i) {
        try {
            //rs.absolute(i);        // 非滚动游标
            this.CommisionDetailID = rs.getLong("CommisionDetailID");
            if( rs.getString("ShardingID") == null )
                this.ShardingID = null;
            else
                this.ShardingID = rs.getString("ShardingID").trim();

            if( rs.getString("GrpContNo") == null )
                this.GrpContNo = null;
            else
                this.GrpContNo = rs.getString("GrpContNo").trim();

            if( rs.getString("AgentCode") == null )
                this.AgentCode = null;
            else
                this.AgentCode = rs.getString("AgentCode").trim();

            this.BusiRate = rs.getDouble("BusiRate");
            if( rs.getString("Operator") == null )
                this.Operator = null;
            else
                this.Operator = rs.getString("Operator").trim();

            this.MakeDate = rs.getDate("MakeDate");
            if( rs.getString("MakeTime") == null )
                this.MakeTime = null;
            else
                this.MakeTime = rs.getString("MakeTime").trim();

            this.ModifyDate = rs.getDate("ModifyDate");
            if( rs.getString("ModifyTime") == null )
                this.ModifyTime = null;
            else
                this.ModifyTime = rs.getString("ModifyTime").trim();

            if( rs.getString("AgentGroup") == null )
                this.AgentGroup = null;
            else
                this.AgentGroup = rs.getString("AgentGroup").trim();

            if( rs.getString("PolType") == null )
                this.PolType = null;
            else
                this.PolType = rs.getString("PolType").trim();

            this.StartSerDate = rs.getDate("StartSerDate");
            this.EndSerDate = rs.getDate("EndSerDate");
            if( rs.getString("AgentFlag") == null )
                this.AgentFlag = null;
            else
                this.AgentFlag = rs.getString("AgentFlag").trim();

            this.SalesRate = rs.getDouble("SalesRate");
            if( rs.getString("RelationShip") == null )
                this.RelationShip = null;
            else
                this.RelationShip = rs.getString("RelationShip").trim();

        }
        catch(SQLException sqle) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LACommisionDetailSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = ExceptionUtils.exceptionToString(sqle);
            this.mErrors .addOneError(tError);
            return false;
        }
        return true;
    }

    public LACommisionDetailSchema getSchema() {
        LACommisionDetailSchema aLACommisionDetailSchema = new LACommisionDetailSchema();
        aLACommisionDetailSchema.setSchema(this);
        return aLACommisionDetailSchema;
    }

    public LACommisionDetailDB getDB() {
        LACommisionDetailDB aDBOper = new LACommisionDetailDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
    * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLACommisionDetail描述/A>表字段
    * @return: String 返回打包后字符串
    **/
    public String encode() {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(ChgData.chgData(CommisionDetailID));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ShardingID)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(GrpContNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AgentCode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(BusiRate));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ModifyTime)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AgentGroup)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(PolType)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( StartSerDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( EndSerDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AgentFlag)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(SalesRate));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(RelationShip));
        return strReturn.toString();
    }

    /**
    * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLACommisionDetail>历史记账凭证主表信息</A>表字段
    * @param: strMessage String 包含一条纪录数据的字符串
    * @return: boolean
    **/
    public boolean decode(String strMessage) {
        try {
            CommisionDetailID = new Long(ChgData.chgNumericStr(StrTool.getStr(strMessage,1, SysConst.PACKAGESPILTER))).longValue();
            ShardingID = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
            GrpContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
            AgentCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
            BusiRate = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,5, SysConst.PACKAGESPILTER))).doubleValue();
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
            AgentGroup = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
            PolType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
            StartSerDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER));
            EndSerDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER));
            AgentFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15, SysConst.PACKAGESPILTER );
            SalesRate = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,16, SysConst.PACKAGESPILTER))).doubleValue();
            RelationShip = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17, SysConst.PACKAGESPILTER );
        }
        catch(NumberFormatException ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LACommisionDetailSchema";
            tError.functionName = "decode";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            return false;
        }
        return true;
    }

    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("CommisionDetailID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CommisionDetailID));
        }
        if (FCode.equalsIgnoreCase("ShardingID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ShardingID));
        }
        if (FCode.equalsIgnoreCase("GrpContNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GrpContNo));
        }
        if (FCode.equalsIgnoreCase("AgentCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentCode));
        }
        if (FCode.equalsIgnoreCase("BusiRate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BusiRate));
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (FCode.equalsIgnoreCase("AgentGroup")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentGroup));
        }
        if (FCode.equalsIgnoreCase("PolType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PolType));
        }
        if (FCode.equalsIgnoreCase("StartSerDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getStartSerDate()));
        }
        if (FCode.equalsIgnoreCase("EndSerDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getEndSerDate()));
        }
        if (FCode.equalsIgnoreCase("AgentFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentFlag));
        }
        if (FCode.equalsIgnoreCase("SalesRate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SalesRate));
        }
        if (FCode.equalsIgnoreCase("RelationShip")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RelationShip));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = String.valueOf(CommisionDetailID);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(ShardingID);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(GrpContNo);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(AgentCode);
                break;
            case 4:
                strFieldValue = String.valueOf(BusiRate);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(Operator);
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(MakeTime);
                break;
            case 8:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
                break;
            case 9:
                strFieldValue = StrTool.GBKToUnicode(ModifyTime);
                break;
            case 10:
                strFieldValue = StrTool.GBKToUnicode(AgentGroup);
                break;
            case 11:
                strFieldValue = StrTool.GBKToUnicode(PolType);
                break;
            case 12:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getStartSerDate()));
                break;
            case 13:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getEndSerDate()));
                break;
            case 14:
                strFieldValue = StrTool.GBKToUnicode(AgentFlag);
                break;
            case 15:
                strFieldValue = String.valueOf(SalesRate);
                break;
            case 16:
                strFieldValue = StrTool.GBKToUnicode(RelationShip);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("CommisionDetailID")) {
            if( FValue != null && !FValue.equals("")) {
                CommisionDetailID = new Long(FValue).longValue();
            }
        }
        if (FCode.equalsIgnoreCase("ShardingID")) {
            if( FValue != null && !FValue.equals(""))
            {
                ShardingID = FValue.trim();
            }
            else
                ShardingID = null;
        }
        if (FCode.equalsIgnoreCase("GrpContNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                GrpContNo = FValue.trim();
            }
            else
                GrpContNo = null;
        }
        if (FCode.equalsIgnoreCase("AgentCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                AgentCode = FValue.trim();
            }
            else
                AgentCode = null;
        }
        if (FCode.equalsIgnoreCase("BusiRate")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                BusiRate = d;
            }
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            if( FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
                Operator = null;
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if(FValue != null && !FValue.equals("")) {
                MakeDate = fDate.getDate( FValue );
            }
            else
                MakeDate = null;
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
                MakeTime = null;
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if(FValue != null && !FValue.equals("")) {
                ModifyDate = fDate.getDate( FValue );
            }
            else
                ModifyDate = null;
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
                ModifyTime = null;
        }
        if (FCode.equalsIgnoreCase("AgentGroup")) {
            if( FValue != null && !FValue.equals(""))
            {
                AgentGroup = FValue.trim();
            }
            else
                AgentGroup = null;
        }
        if (FCode.equalsIgnoreCase("PolType")) {
            if( FValue != null && !FValue.equals(""))
            {
                PolType = FValue.trim();
            }
            else
                PolType = null;
        }
        if (FCode.equalsIgnoreCase("StartSerDate")) {
            if(FValue != null && !FValue.equals("")) {
                StartSerDate = fDate.getDate( FValue );
            }
            else
                StartSerDate = null;
        }
        if (FCode.equalsIgnoreCase("EndSerDate")) {
            if(FValue != null && !FValue.equals("")) {
                EndSerDate = fDate.getDate( FValue );
            }
            else
                EndSerDate = null;
        }
        if (FCode.equalsIgnoreCase("AgentFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                AgentFlag = FValue.trim();
            }
            else
                AgentFlag = null;
        }
        if (FCode.equalsIgnoreCase("SalesRate")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                SalesRate = d;
            }
        }
        if (FCode.equalsIgnoreCase("RelationShip")) {
            if( FValue != null && !FValue.equals(""))
            {
                RelationShip = FValue.trim();
            }
            else
                RelationShip = null;
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject) return true;
        if (otherObject == null) return false;
        if (getClass() != otherObject.getClass()) return false;
        LACommisionDetailSchema other = (LACommisionDetailSchema)otherObject;
        return
            CommisionDetailID == other.getCommisionDetailID()
            && ShardingID.equals(other.getShardingID())
            && GrpContNo.equals(other.getGrpContNo())
            && AgentCode.equals(other.getAgentCode())
            && BusiRate == other.getBusiRate()
            && Operator.equals(other.getOperator())
            && fDate.getString(MakeDate).equals(other.getMakeDate())
            && MakeTime.equals(other.getMakeTime())
            && fDate.getString(ModifyDate).equals(other.getModifyDate())
            && ModifyTime.equals(other.getModifyTime())
            && AgentGroup.equals(other.getAgentGroup())
            && PolType.equals(other.getPolType())
            && fDate.getString(StartSerDate).equals(other.getStartSerDate())
            && fDate.getString(EndSerDate).equals(other.getEndSerDate())
            && AgentFlag.equals(other.getAgentFlag())
            && SalesRate == other.getSalesRate()
            && RelationShip.equals(other.getRelationShip());
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("CommisionDetailID") ) {
            return 0;
        }
        if( strFieldName.equals("ShardingID") ) {
            return 1;
        }
        if( strFieldName.equals("GrpContNo") ) {
            return 2;
        }
        if( strFieldName.equals("AgentCode") ) {
            return 3;
        }
        if( strFieldName.equals("BusiRate") ) {
            return 4;
        }
        if( strFieldName.equals("Operator") ) {
            return 5;
        }
        if( strFieldName.equals("MakeDate") ) {
            return 6;
        }
        if( strFieldName.equals("MakeTime") ) {
            return 7;
        }
        if( strFieldName.equals("ModifyDate") ) {
            return 8;
        }
        if( strFieldName.equals("ModifyTime") ) {
            return 9;
        }
        if( strFieldName.equals("AgentGroup") ) {
            return 10;
        }
        if( strFieldName.equals("PolType") ) {
            return 11;
        }
        if( strFieldName.equals("StartSerDate") ) {
            return 12;
        }
        if( strFieldName.equals("EndSerDate") ) {
            return 13;
        }
        if( strFieldName.equals("AgentFlag") ) {
            return 14;
        }
        if( strFieldName.equals("SalesRate") ) {
            return 15;
        }
        if( strFieldName.equals("RelationShip") ) {
            return 16;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "CommisionDetailID";
                break;
            case 1:
                strFieldName = "ShardingID";
                break;
            case 2:
                strFieldName = "GrpContNo";
                break;
            case 3:
                strFieldName = "AgentCode";
                break;
            case 4:
                strFieldName = "BusiRate";
                break;
            case 5:
                strFieldName = "Operator";
                break;
            case 6:
                strFieldName = "MakeDate";
                break;
            case 7:
                strFieldName = "MakeTime";
                break;
            case 8:
                strFieldName = "ModifyDate";
                break;
            case 9:
                strFieldName = "ModifyTime";
                break;
            case 10:
                strFieldName = "AgentGroup";
                break;
            case 11:
                strFieldName = "PolType";
                break;
            case 12:
                strFieldName = "StartSerDate";
                break;
            case 13:
                strFieldName = "EndSerDate";
                break;
            case 14:
                strFieldName = "AgentFlag";
                break;
            case 15:
                strFieldName = "SalesRate";
                break;
            case 16:
                strFieldName = "RelationShip";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "COMMISIONDETAILID":
                return Schema.TYPE_LONG;
            case "SHARDINGID":
                return Schema.TYPE_STRING;
            case "GRPCONTNO":
                return Schema.TYPE_STRING;
            case "AGENTCODE":
                return Schema.TYPE_STRING;
            case "BUSIRATE":
                return Schema.TYPE_DOUBLE;
            case "OPERATOR":
                return Schema.TYPE_STRING;
            case "MAKEDATE":
                return Schema.TYPE_DATE;
            case "MAKETIME":
                return Schema.TYPE_STRING;
            case "MODIFYDATE":
                return Schema.TYPE_DATE;
            case "MODIFYTIME":
                return Schema.TYPE_STRING;
            case "AGENTGROUP":
                return Schema.TYPE_STRING;
            case "POLTYPE":
                return Schema.TYPE_STRING;
            case "STARTSERDATE":
                return Schema.TYPE_DATE;
            case "ENDSERDATE":
                return Schema.TYPE_DATE;
            case "AGENTFLAG":
                return Schema.TYPE_STRING;
            case "SALESRATE":
                return Schema.TYPE_DOUBLE;
            case "RELATIONSHIP":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_LONG;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_DOUBLE;
            case 5:
                return Schema.TYPE_STRING;
            case 6:
                return Schema.TYPE_DATE;
            case 7:
                return Schema.TYPE_STRING;
            case 8:
                return Schema.TYPE_DATE;
            case 9:
                return Schema.TYPE_STRING;
            case 10:
                return Schema.TYPE_STRING;
            case 11:
                return Schema.TYPE_STRING;
            case 12:
                return Schema.TYPE_DATE;
            case 13:
                return Schema.TYPE_DATE;
            case 14:
                return Schema.TYPE_STRING;
            case 15:
                return Schema.TYPE_DOUBLE;
            case 16:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
}
