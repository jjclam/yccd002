/**
 * Copyright (c) 2018 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.entity;

import com.sinosoft.cloud.cache.annotation.RedisPrimaryHKey;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.Pojo;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;

import java.io.Serializable;

/**
 * <p>ClassName: LMWhiteListPojo </p>
 * <p>Copyright: Copyright (c) 2018</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2018-08-22
 */
public class LMWhiteListPojo implements Pojo,Serializable {
    // @Field
    /** 序列号 */
    @RedisPrimaryHKey
    private String SeqNo; 
    /** 客户号 */
    private String CustomerNo; 
    /** 姓名 */
    private String Name; 
    /** 性别 */
    private String Sex; 
    /** 证件类型 */
    private String IDType; 
    /** 证件号码 */
    private String IDNo; 
    /** 出生日期 */
    private String  BirthDay;
    /** 累计保额 */
    private double Amnt; 
    /** 白名单产品类型标记 */
    private String WFWFlag; 
    /** 保额类型 */
    private String AmntType; 
    /** 申请人 */
    private String ApplyOpertor; 
    /** 申请日期 */
    private String  ApplyDate;
    /** 申请时间 */
    private String ApplyTime; 
    /** 申请状态 */
    private String ApplyState; 
    /** 回退人 */
    private String BackOpertor; 
    /** 审批人一 */
    private String ApproveOne; 
    /** 审批一日期 */
    private String  ApproveOneDate;
    /** 审批一时间 */
    private String ApproveOneTime; 
    /** 审批一结论 */
    private String AOVerdict; 
    /** 审批一意见 */
    private String AOSuggestion; 
    /** 审批人二 */
    private String ApproveTwo; 
    /** 审批二日期 */
    private String  ApproveTwoDate;
    /** 审批二时间 */
    private String ApproveTwoTime; 
    /** 审批二结论 */
    private String ATVerdict; 
    /** 审批二意见 */
    private String ATSuggestion; 
    /** 修改日期 */
    private String  ModifyDate;
    /** 修改时间 */
    private String ModifyTime; 
    /** 申请险种 */
    private String RiskCode; 
    /** 机构编码 */
    private String ManageCom; 
    /** 客户状态 */
    private String UserState; 
    /** 证件有效期 */
    private String IdValiDate; 
    /** 备用字段 */
    private String StandByFlag1; 
    /** 申请账号 */
    private String DiCardNo; 
    /** 账号类型 */
    private String DiType; 


    public static final int FIELDNUM = 34;    // 数据库表的字段个数

    private FDate fDate = new FDate();        // 处理日期
    public String getSeqNo() {
        return SeqNo;
    }
    public void setSeqNo(String aSeqNo) {
        SeqNo = aSeqNo;
    }
    public String getCustomerNo() {
        return CustomerNo;
    }
    public void setCustomerNo(String aCustomerNo) {
        CustomerNo = aCustomerNo;
    }
    public String getName() {
        return Name;
    }
    public void setName(String aName) {
        Name = aName;
    }
    public String getSex() {
        return Sex;
    }
    public void setSex(String aSex) {
        Sex = aSex;
    }
    public String getIDType() {
        return IDType;
    }
    public void setIDType(String aIDType) {
        IDType = aIDType;
    }
    public String getIDNo() {
        return IDNo;
    }
    public void setIDNo(String aIDNo) {
        IDNo = aIDNo;
    }
    public String getBirthDay() {
        return BirthDay;
    }
    public void setBirthDay(String aBirthDay) {
        BirthDay = aBirthDay;
    }
    public double getAmnt() {
        return Amnt;
    }
    public void setAmnt(double aAmnt) {
        Amnt = aAmnt;
    }
    public void setAmnt(String aAmnt) {
        if (aAmnt != null && !aAmnt.equals("")) {
            Double tDouble = new Double(aAmnt);
            double d = tDouble.doubleValue();
            Amnt = d;
        }
    }

    public String getWFWFlag() {
        return WFWFlag;
    }
    public void setWFWFlag(String aWFWFlag) {
        WFWFlag = aWFWFlag;
    }
    public String getAmntType() {
        return AmntType;
    }
    public void setAmntType(String aAmntType) {
        AmntType = aAmntType;
    }
    public String getApplyOpertor() {
        return ApplyOpertor;
    }
    public void setApplyOpertor(String aApplyOpertor) {
        ApplyOpertor = aApplyOpertor;
    }
    public String getApplyDate() {
        return ApplyDate;
    }
    public void setApplyDate(String aApplyDate) {
        ApplyDate = aApplyDate;
    }
    public String getApplyTime() {
        return ApplyTime;
    }
    public void setApplyTime(String aApplyTime) {
        ApplyTime = aApplyTime;
    }
    public String getApplyState() {
        return ApplyState;
    }
    public void setApplyState(String aApplyState) {
        ApplyState = aApplyState;
    }
    public String getBackOpertor() {
        return BackOpertor;
    }
    public void setBackOpertor(String aBackOpertor) {
        BackOpertor = aBackOpertor;
    }
    public String getApproveOne() {
        return ApproveOne;
    }
    public void setApproveOne(String aApproveOne) {
        ApproveOne = aApproveOne;
    }
    public String getApproveOneDate() {
        return ApproveOneDate;
    }
    public void setApproveOneDate(String aApproveOneDate) {
        ApproveOneDate = aApproveOneDate;
    }
    public String getApproveOneTime() {
        return ApproveOneTime;
    }
    public void setApproveOneTime(String aApproveOneTime) {
        ApproveOneTime = aApproveOneTime;
    }
    public String getAOVerdict() {
        return AOVerdict;
    }
    public void setAOVerdict(String aAOVerdict) {
        AOVerdict = aAOVerdict;
    }
    public String getAOSuggestion() {
        return AOSuggestion;
    }
    public void setAOSuggestion(String aAOSuggestion) {
        AOSuggestion = aAOSuggestion;
    }
    public String getApproveTwo() {
        return ApproveTwo;
    }
    public void setApproveTwo(String aApproveTwo) {
        ApproveTwo = aApproveTwo;
    }
    public String getApproveTwoDate() {
        return ApproveTwoDate;
    }
    public void setApproveTwoDate(String aApproveTwoDate) {
        ApproveTwoDate = aApproveTwoDate;
    }
    public String getApproveTwoTime() {
        return ApproveTwoTime;
    }
    public void setApproveTwoTime(String aApproveTwoTime) {
        ApproveTwoTime = aApproveTwoTime;
    }
    public String getATVerdict() {
        return ATVerdict;
    }
    public void setATVerdict(String aATVerdict) {
        ATVerdict = aATVerdict;
    }
    public String getATSuggestion() {
        return ATSuggestion;
    }
    public void setATSuggestion(String aATSuggestion) {
        ATSuggestion = aATSuggestion;
    }
    public String getModifyDate() {
        return ModifyDate;
    }
    public void setModifyDate(String aModifyDate) {
        ModifyDate = aModifyDate;
    }
    public String getModifyTime() {
        return ModifyTime;
    }
    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }
    public String getRiskCode() {
        return RiskCode;
    }
    public void setRiskCode(String aRiskCode) {
        RiskCode = aRiskCode;
    }
    public String getManageCom() {
        return ManageCom;
    }
    public void setManageCom(String aManageCom) {
        ManageCom = aManageCom;
    }
    public String getUserState() {
        return UserState;
    }
    public void setUserState(String aUserState) {
        UserState = aUserState;
    }
    public String getIdValiDate() {
        return IdValiDate;
    }
    public void setIdValiDate(String aIdValiDate) {
        IdValiDate = aIdValiDate;
    }
    public String getStandByFlag1() {
        return StandByFlag1;
    }
    public void setStandByFlag1(String aStandByFlag1) {
        StandByFlag1 = aStandByFlag1;
    }
    public String getDiCardNo() {
        return DiCardNo;
    }
    public void setDiCardNo(String aDiCardNo) {
        DiCardNo = aDiCardNo;
    }
    public String getDiType() {
        return DiType;
    }
    public void setDiType(String aDiType) {
        DiType = aDiType;
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("SeqNo") ) {
            return 0;
        }
        if( strFieldName.equals("CustomerNo") ) {
            return 1;
        }
        if( strFieldName.equals("Name") ) {
            return 2;
        }
        if( strFieldName.equals("Sex") ) {
            return 3;
        }
        if( strFieldName.equals("IDType") ) {
            return 4;
        }
        if( strFieldName.equals("IDNo") ) {
            return 5;
        }
        if( strFieldName.equals("BirthDay") ) {
            return 6;
        }
        if( strFieldName.equals("Amnt") ) {
            return 7;
        }
        if( strFieldName.equals("WFWFlag") ) {
            return 8;
        }
        if( strFieldName.equals("AmntType") ) {
            return 9;
        }
        if( strFieldName.equals("ApplyOpertor") ) {
            return 10;
        }
        if( strFieldName.equals("ApplyDate") ) {
            return 11;
        }
        if( strFieldName.equals("ApplyTime") ) {
            return 12;
        }
        if( strFieldName.equals("ApplyState") ) {
            return 13;
        }
        if( strFieldName.equals("BackOpertor") ) {
            return 14;
        }
        if( strFieldName.equals("ApproveOne") ) {
            return 15;
        }
        if( strFieldName.equals("ApproveOneDate") ) {
            return 16;
        }
        if( strFieldName.equals("ApproveOneTime") ) {
            return 17;
        }
        if( strFieldName.equals("AOVerdict") ) {
            return 18;
        }
        if( strFieldName.equals("AOSuggestion") ) {
            return 19;
        }
        if( strFieldName.equals("ApproveTwo") ) {
            return 20;
        }
        if( strFieldName.equals("ApproveTwoDate") ) {
            return 21;
        }
        if( strFieldName.equals("ApproveTwoTime") ) {
            return 22;
        }
        if( strFieldName.equals("ATVerdict") ) {
            return 23;
        }
        if( strFieldName.equals("ATSuggestion") ) {
            return 24;
        }
        if( strFieldName.equals("ModifyDate") ) {
            return 25;
        }
        if( strFieldName.equals("ModifyTime") ) {
            return 26;
        }
        if( strFieldName.equals("RiskCode") ) {
            return 27;
        }
        if( strFieldName.equals("ManageCom") ) {
            return 28;
        }
        if( strFieldName.equals("UserState") ) {
            return 29;
        }
        if( strFieldName.equals("IdValiDate") ) {
            return 30;
        }
        if( strFieldName.equals("StandByFlag1") ) {
            return 31;
        }
        if( strFieldName.equals("DiCardNo") ) {
            return 32;
        }
        if( strFieldName.equals("DiType") ) {
            return 33;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "SeqNo";
                break;
            case 1:
                strFieldName = "CustomerNo";
                break;
            case 2:
                strFieldName = "Name";
                break;
            case 3:
                strFieldName = "Sex";
                break;
            case 4:
                strFieldName = "IDType";
                break;
            case 5:
                strFieldName = "IDNo";
                break;
            case 6:
                strFieldName = "BirthDay";
                break;
            case 7:
                strFieldName = "Amnt";
                break;
            case 8:
                strFieldName = "WFWFlag";
                break;
            case 9:
                strFieldName = "AmntType";
                break;
            case 10:
                strFieldName = "ApplyOpertor";
                break;
            case 11:
                strFieldName = "ApplyDate";
                break;
            case 12:
                strFieldName = "ApplyTime";
                break;
            case 13:
                strFieldName = "ApplyState";
                break;
            case 14:
                strFieldName = "BackOpertor";
                break;
            case 15:
                strFieldName = "ApproveOne";
                break;
            case 16:
                strFieldName = "ApproveOneDate";
                break;
            case 17:
                strFieldName = "ApproveOneTime";
                break;
            case 18:
                strFieldName = "AOVerdict";
                break;
            case 19:
                strFieldName = "AOSuggestion";
                break;
            case 20:
                strFieldName = "ApproveTwo";
                break;
            case 21:
                strFieldName = "ApproveTwoDate";
                break;
            case 22:
                strFieldName = "ApproveTwoTime";
                break;
            case 23:
                strFieldName = "ATVerdict";
                break;
            case 24:
                strFieldName = "ATSuggestion";
                break;
            case 25:
                strFieldName = "ModifyDate";
                break;
            case 26:
                strFieldName = "ModifyTime";
                break;
            case 27:
                strFieldName = "RiskCode";
                break;
            case 28:
                strFieldName = "ManageCom";
                break;
            case 29:
                strFieldName = "UserState";
                break;
            case 30:
                strFieldName = "IdValiDate";
                break;
            case 31:
                strFieldName = "StandByFlag1";
                break;
            case 32:
                strFieldName = "DiCardNo";
                break;
            case 33:
                strFieldName = "DiType";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "SEQNO":
                return Schema.TYPE_STRING;
            case "CUSTOMERNO":
                return Schema.TYPE_STRING;
            case "NAME":
                return Schema.TYPE_STRING;
            case "SEX":
                return Schema.TYPE_STRING;
            case "IDTYPE":
                return Schema.TYPE_STRING;
            case "IDNO":
                return Schema.TYPE_STRING;
            case "BIRTHDAY":
                return Schema.TYPE_STRING;
            case "AMNT":
                return Schema.TYPE_DOUBLE;
            case "WFWFLAG":
                return Schema.TYPE_STRING;
            case "AMNTTYPE":
                return Schema.TYPE_STRING;
            case "APPLYOPERTOR":
                return Schema.TYPE_STRING;
            case "APPLYDATE":
                return Schema.TYPE_STRING;
            case "APPLYTIME":
                return Schema.TYPE_STRING;
            case "APPLYSTATE":
                return Schema.TYPE_STRING;
            case "BACKOPERTOR":
                return Schema.TYPE_STRING;
            case "APPROVEONE":
                return Schema.TYPE_STRING;
            case "APPROVEONEDATE":
                return Schema.TYPE_STRING;
            case "APPROVEONETIME":
                return Schema.TYPE_STRING;
            case "AOVERDICT":
                return Schema.TYPE_STRING;
            case "AOSUGGESTION":
                return Schema.TYPE_STRING;
            case "APPROVETWO":
                return Schema.TYPE_STRING;
            case "APPROVETWODATE":
                return Schema.TYPE_STRING;
            case "APPROVETWOTIME":
                return Schema.TYPE_STRING;
            case "ATVERDICT":
                return Schema.TYPE_STRING;
            case "ATSUGGESTION":
                return Schema.TYPE_STRING;
            case "MODIFYDATE":
                return Schema.TYPE_STRING;
            case "MODIFYTIME":
                return Schema.TYPE_STRING;
            case "RISKCODE":
                return Schema.TYPE_STRING;
            case "MANAGECOM":
                return Schema.TYPE_STRING;
            case "USERSTATE":
                return Schema.TYPE_STRING;
            case "IDVALIDATE":
                return Schema.TYPE_STRING;
            case "STANDBYFLAG1":
                return Schema.TYPE_STRING;
            case "DICARDNO":
                return Schema.TYPE_STRING;
            case "DITYPE":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_STRING;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_STRING;
            case 6:
                return Schema.TYPE_STRING;
            case 7:
                return Schema.TYPE_DOUBLE;
            case 8:
                return Schema.TYPE_STRING;
            case 9:
                return Schema.TYPE_STRING;
            case 10:
                return Schema.TYPE_STRING;
            case 11:
                return Schema.TYPE_STRING;
            case 12:
                return Schema.TYPE_STRING;
            case 13:
                return Schema.TYPE_STRING;
            case 14:
                return Schema.TYPE_STRING;
            case 15:
                return Schema.TYPE_STRING;
            case 16:
                return Schema.TYPE_STRING;
            case 17:
                return Schema.TYPE_STRING;
            case 18:
                return Schema.TYPE_STRING;
            case 19:
                return Schema.TYPE_STRING;
            case 20:
                return Schema.TYPE_STRING;
            case 21:
                return Schema.TYPE_STRING;
            case 22:
                return Schema.TYPE_STRING;
            case 23:
                return Schema.TYPE_STRING;
            case 24:
                return Schema.TYPE_STRING;
            case 25:
                return Schema.TYPE_STRING;
            case 26:
                return Schema.TYPE_STRING;
            case 27:
                return Schema.TYPE_STRING;
            case 28:
                return Schema.TYPE_STRING;
            case 29:
                return Schema.TYPE_STRING;
            case 30:
                return Schema.TYPE_STRING;
            case 31:
                return Schema.TYPE_STRING;
            case 32:
                return Schema.TYPE_STRING;
            case 33:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("SeqNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SeqNo));
        }
        if (FCode.equalsIgnoreCase("CustomerNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CustomerNo));
        }
        if (FCode.equalsIgnoreCase("Name")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Name));
        }
        if (FCode.equalsIgnoreCase("Sex")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Sex));
        }
        if (FCode.equalsIgnoreCase("IDType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(IDType));
        }
        if (FCode.equalsIgnoreCase("IDNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(IDNo));
        }
        if (FCode.equalsIgnoreCase("BirthDay")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BirthDay));
        }
        if (FCode.equalsIgnoreCase("Amnt")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Amnt));
        }
        if (FCode.equalsIgnoreCase("WFWFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(WFWFlag));
        }
        if (FCode.equalsIgnoreCase("AmntType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AmntType));
        }
        if (FCode.equalsIgnoreCase("ApplyOpertor")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ApplyOpertor));
        }
        if (FCode.equalsIgnoreCase("ApplyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ApplyDate));
        }
        if (FCode.equalsIgnoreCase("ApplyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ApplyTime));
        }
        if (FCode.equalsIgnoreCase("ApplyState")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ApplyState));
        }
        if (FCode.equalsIgnoreCase("BackOpertor")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BackOpertor));
        }
        if (FCode.equalsIgnoreCase("ApproveOne")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ApproveOne));
        }
        if (FCode.equalsIgnoreCase("ApproveOneDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ApproveOneDate));
        }
        if (FCode.equalsIgnoreCase("ApproveOneTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ApproveOneTime));
        }
        if (FCode.equalsIgnoreCase("AOVerdict")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AOVerdict));
        }
        if (FCode.equalsIgnoreCase("AOSuggestion")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AOSuggestion));
        }
        if (FCode.equalsIgnoreCase("ApproveTwo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ApproveTwo));
        }
        if (FCode.equalsIgnoreCase("ApproveTwoDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ApproveTwoDate));
        }
        if (FCode.equalsIgnoreCase("ApproveTwoTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ApproveTwoTime));
        }
        if (FCode.equalsIgnoreCase("ATVerdict")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ATVerdict));
        }
        if (FCode.equalsIgnoreCase("ATSuggestion")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ATSuggestion));
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyDate));
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (FCode.equalsIgnoreCase("RiskCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RiskCode));
        }
        if (FCode.equalsIgnoreCase("ManageCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
        }
        if (FCode.equalsIgnoreCase("UserState")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(UserState));
        }
        if (FCode.equalsIgnoreCase("IdValiDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(IdValiDate));
        }
        if (FCode.equalsIgnoreCase("StandByFlag1")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StandByFlag1));
        }
        if (FCode.equalsIgnoreCase("DiCardNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DiCardNo));
        }
        if (FCode.equalsIgnoreCase("DiType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DiType));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = String.valueOf(SeqNo);
                break;
            case 1:
                strFieldValue = String.valueOf(CustomerNo);
                break;
            case 2:
                strFieldValue = String.valueOf(Name);
                break;
            case 3:
                strFieldValue = String.valueOf(Sex);
                break;
            case 4:
                strFieldValue = String.valueOf(IDType);
                break;
            case 5:
                strFieldValue = String.valueOf(IDNo);
                break;
            case 6:
                strFieldValue = String.valueOf(BirthDay);
                break;
            case 7:
                strFieldValue = String.valueOf(Amnt);
                break;
            case 8:
                strFieldValue = String.valueOf(WFWFlag);
                break;
            case 9:
                strFieldValue = String.valueOf(AmntType);
                break;
            case 10:
                strFieldValue = String.valueOf(ApplyOpertor);
                break;
            case 11:
                strFieldValue = String.valueOf(ApplyDate);
                break;
            case 12:
                strFieldValue = String.valueOf(ApplyTime);
                break;
            case 13:
                strFieldValue = String.valueOf(ApplyState);
                break;
            case 14:
                strFieldValue = String.valueOf(BackOpertor);
                break;
            case 15:
                strFieldValue = String.valueOf(ApproveOne);
                break;
            case 16:
                strFieldValue = String.valueOf(ApproveOneDate);
                break;
            case 17:
                strFieldValue = String.valueOf(ApproveOneTime);
                break;
            case 18:
                strFieldValue = String.valueOf(AOVerdict);
                break;
            case 19:
                strFieldValue = String.valueOf(AOSuggestion);
                break;
            case 20:
                strFieldValue = String.valueOf(ApproveTwo);
                break;
            case 21:
                strFieldValue = String.valueOf(ApproveTwoDate);
                break;
            case 22:
                strFieldValue = String.valueOf(ApproveTwoTime);
                break;
            case 23:
                strFieldValue = String.valueOf(ATVerdict);
                break;
            case 24:
                strFieldValue = String.valueOf(ATSuggestion);
                break;
            case 25:
                strFieldValue = String.valueOf(ModifyDate);
                break;
            case 26:
                strFieldValue = String.valueOf(ModifyTime);
                break;
            case 27:
                strFieldValue = String.valueOf(RiskCode);
                break;
            case 28:
                strFieldValue = String.valueOf(ManageCom);
                break;
            case 29:
                strFieldValue = String.valueOf(UserState);
                break;
            case 30:
                strFieldValue = String.valueOf(IdValiDate);
                break;
            case 31:
                strFieldValue = String.valueOf(StandByFlag1);
                break;
            case 32:
                strFieldValue = String.valueOf(DiCardNo);
                break;
            case 33:
                strFieldValue = String.valueOf(DiType);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("SeqNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                SeqNo = FValue.trim();
            }
            else
                SeqNo = null;
        }
        if (FCode.equalsIgnoreCase("CustomerNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                CustomerNo = FValue.trim();
            }
            else
                CustomerNo = null;
        }
        if (FCode.equalsIgnoreCase("Name")) {
            if( FValue != null && !FValue.equals(""))
            {
                Name = FValue.trim();
            }
            else
                Name = null;
        }
        if (FCode.equalsIgnoreCase("Sex")) {
            if( FValue != null && !FValue.equals(""))
            {
                Sex = FValue.trim();
            }
            else
                Sex = null;
        }
        if (FCode.equalsIgnoreCase("IDType")) {
            if( FValue != null && !FValue.equals(""))
            {
                IDType = FValue.trim();
            }
            else
                IDType = null;
        }
        if (FCode.equalsIgnoreCase("IDNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                IDNo = FValue.trim();
            }
            else
                IDNo = null;
        }
        if (FCode.equalsIgnoreCase("BirthDay")) {
            if( FValue != null && !FValue.equals(""))
            {
                BirthDay = FValue.trim();
            }
            else
                BirthDay = null;
        }
        if (FCode.equalsIgnoreCase("Amnt")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                Amnt = d;
            }
        }
        if (FCode.equalsIgnoreCase("WFWFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                WFWFlag = FValue.trim();
            }
            else
                WFWFlag = null;
        }
        if (FCode.equalsIgnoreCase("AmntType")) {
            if( FValue != null && !FValue.equals(""))
            {
                AmntType = FValue.trim();
            }
            else
                AmntType = null;
        }
        if (FCode.equalsIgnoreCase("ApplyOpertor")) {
            if( FValue != null && !FValue.equals(""))
            {
                ApplyOpertor = FValue.trim();
            }
            else
                ApplyOpertor = null;
        }
        if (FCode.equalsIgnoreCase("ApplyDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                ApplyDate = FValue.trim();
            }
            else
                ApplyDate = null;
        }
        if (FCode.equalsIgnoreCase("ApplyTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                ApplyTime = FValue.trim();
            }
            else
                ApplyTime = null;
        }
        if (FCode.equalsIgnoreCase("ApplyState")) {
            if( FValue != null && !FValue.equals(""))
            {
                ApplyState = FValue.trim();
            }
            else
                ApplyState = null;
        }
        if (FCode.equalsIgnoreCase("BackOpertor")) {
            if( FValue != null && !FValue.equals(""))
            {
                BackOpertor = FValue.trim();
            }
            else
                BackOpertor = null;
        }
        if (FCode.equalsIgnoreCase("ApproveOne")) {
            if( FValue != null && !FValue.equals(""))
            {
                ApproveOne = FValue.trim();
            }
            else
                ApproveOne = null;
        }
        if (FCode.equalsIgnoreCase("ApproveOneDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                ApproveOneDate = FValue.trim();
            }
            else
                ApproveOneDate = null;
        }
        if (FCode.equalsIgnoreCase("ApproveOneTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                ApproveOneTime = FValue.trim();
            }
            else
                ApproveOneTime = null;
        }
        if (FCode.equalsIgnoreCase("AOVerdict")) {
            if( FValue != null && !FValue.equals(""))
            {
                AOVerdict = FValue.trim();
            }
            else
                AOVerdict = null;
        }
        if (FCode.equalsIgnoreCase("AOSuggestion")) {
            if( FValue != null && !FValue.equals(""))
            {
                AOSuggestion = FValue.trim();
            }
            else
                AOSuggestion = null;
        }
        if (FCode.equalsIgnoreCase("ApproveTwo")) {
            if( FValue != null && !FValue.equals(""))
            {
                ApproveTwo = FValue.trim();
            }
            else
                ApproveTwo = null;
        }
        if (FCode.equalsIgnoreCase("ApproveTwoDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                ApproveTwoDate = FValue.trim();
            }
            else
                ApproveTwoDate = null;
        }
        if (FCode.equalsIgnoreCase("ApproveTwoTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                ApproveTwoTime = FValue.trim();
            }
            else
                ApproveTwoTime = null;
        }
        if (FCode.equalsIgnoreCase("ATVerdict")) {
            if( FValue != null && !FValue.equals(""))
            {
                ATVerdict = FValue.trim();
            }
            else
                ATVerdict = null;
        }
        if (FCode.equalsIgnoreCase("ATSuggestion")) {
            if( FValue != null && !FValue.equals(""))
            {
                ATSuggestion = FValue.trim();
            }
            else
                ATSuggestion = null;
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyDate = FValue.trim();
            }
            else
                ModifyDate = null;
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
                ModifyTime = null;
        }
        if (FCode.equalsIgnoreCase("RiskCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                RiskCode = FValue.trim();
            }
            else
                RiskCode = null;
        }
        if (FCode.equalsIgnoreCase("ManageCom")) {
            if( FValue != null && !FValue.equals(""))
            {
                ManageCom = FValue.trim();
            }
            else
                ManageCom = null;
        }
        if (FCode.equalsIgnoreCase("UserState")) {
            if( FValue != null && !FValue.equals(""))
            {
                UserState = FValue.trim();
            }
            else
                UserState = null;
        }
        if (FCode.equalsIgnoreCase("IdValiDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                IdValiDate = FValue.trim();
            }
            else
                IdValiDate = null;
        }
        if (FCode.equalsIgnoreCase("StandByFlag1")) {
            if( FValue != null && !FValue.equals(""))
            {
                StandByFlag1 = FValue.trim();
            }
            else
                StandByFlag1 = null;
        }
        if (FCode.equalsIgnoreCase("DiCardNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                DiCardNo = FValue.trim();
            }
            else
                DiCardNo = null;
        }
        if (FCode.equalsIgnoreCase("DiType")) {
            if( FValue != null && !FValue.equals(""))
            {
                DiType = FValue.trim();
            }
            else
                DiType = null;
        }
        return true;
    }


    public String toString() {
    return "LMWhiteListPojo [" +
            "SeqNo="+SeqNo +
            ", CustomerNo="+CustomerNo +
            ", Name="+Name +
            ", Sex="+Sex +
            ", IDType="+IDType +
            ", IDNo="+IDNo +
            ", BirthDay="+BirthDay +
            ", Amnt="+Amnt +
            ", WFWFlag="+WFWFlag +
            ", AmntType="+AmntType +
            ", ApplyOpertor="+ApplyOpertor +
            ", ApplyDate="+ApplyDate +
            ", ApplyTime="+ApplyTime +
            ", ApplyState="+ApplyState +
            ", BackOpertor="+BackOpertor +
            ", ApproveOne="+ApproveOne +
            ", ApproveOneDate="+ApproveOneDate +
            ", ApproveOneTime="+ApproveOneTime +
            ", AOVerdict="+AOVerdict +
            ", AOSuggestion="+AOSuggestion +
            ", ApproveTwo="+ApproveTwo +
            ", ApproveTwoDate="+ApproveTwoDate +
            ", ApproveTwoTime="+ApproveTwoTime +
            ", ATVerdict="+ATVerdict +
            ", ATSuggestion="+ATSuggestion +
            ", ModifyDate="+ModifyDate +
            ", ModifyTime="+ModifyTime +
            ", RiskCode="+RiskCode +
            ", ManageCom="+ManageCom +
            ", UserState="+UserState +
            ", IdValiDate="+IdValiDate +
            ", StandByFlag1="+StandByFlag1 +
            ", DiCardNo="+DiCardNo +
            ", DiType="+DiType +"]";
    }
}
