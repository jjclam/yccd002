/**
 * Copyright (c) 2017 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.entity;

import com.sinosoft.cloud.cache.annotation.RedisIndexHKey;
import com.sinosoft.cloud.cache.annotation.RedisPrimaryHKey;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.Pojo;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;

import java.io.Serializable;

/**
 * <p>ClassName: LAAgent1Pojo </p>
 * <p>Copyright: Copyright (c) 2017</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2017-10-17
 */
public class LAAgent1Pojo implements Pojo,Serializable {
    // @Field
    /** Managecom */
    private String ManageCom; 
    /** Agentcom */
    private String AgentCom; 
    /** Outagentcode */
    @RedisIndexHKey
    private String OutAgentCode; 
    /** Agentname */
    private String AgentName; 
    /** Idtype */
    private String IDType; 
    /** Idno */
    private String IDNo; 
    /** Qualtype */
    private String QualType; 
    /** Qualno */
    private String QualNo; 
    /** Qualstartdate */
    private String  QualStartDate;
    /** Qualenddate */
    private String  QualEndDate;
    /** Agentstate */
    private String AgentState; 
    /** Branchtype */
    private String BranchType; 
    /** Operator */
    private String Operator; 
    /** Makedate */
    private String  MakeDate;
    /** Maketime */
    private String MakeTime; 
    /** Modifydate */
    private String  ModifyDate;
    /** Modifytime */
    private String ModifyTime; 
    /** State1 */
    private String State1; 
    /** Phone */
    private String Phone; 
    /** Mobile */
    private String Mobile; 
    /** Serialno */
    @RedisPrimaryHKey
    private String SerialNo; 


    public static final int FIELDNUM = 21;    // 数据库表的字段个数

    private FDate fDate = new FDate();        // 处理日期
    public String getManageCom() {
        return ManageCom;
    }
    public void setManageCom(String aManageCom) {
        ManageCom = aManageCom;
    }
    public String getAgentCom() {
        return AgentCom;
    }
    public void setAgentCom(String aAgentCom) {
        AgentCom = aAgentCom;
    }
    public String getOutAgentCode() {
        return OutAgentCode;
    }
    public void setOutAgentCode(String aOutAgentCode) {
        OutAgentCode = aOutAgentCode;
    }
    public String getAgentName() {
        return AgentName;
    }
    public void setAgentName(String aAgentName) {
        AgentName = aAgentName;
    }
    public String getIDType() {
        return IDType;
    }
    public void setIDType(String aIDType) {
        IDType = aIDType;
    }
    public String getIDNo() {
        return IDNo;
    }
    public void setIDNo(String aIDNo) {
        IDNo = aIDNo;
    }
    public String getQualType() {
        return QualType;
    }
    public void setQualType(String aQualType) {
        QualType = aQualType;
    }
    public String getQualNo() {
        return QualNo;
    }
    public void setQualNo(String aQualNo) {
        QualNo = aQualNo;
    }
    public String getQualStartDate() {
        return QualStartDate;
    }
    public void setQualStartDate(String aQualStartDate) {
        QualStartDate = aQualStartDate;
    }
    public String getQualEndDate() {
        return QualEndDate;
    }
    public void setQualEndDate(String aQualEndDate) {
        QualEndDate = aQualEndDate;
    }
    public String getAgentState() {
        return AgentState;
    }
    public void setAgentState(String aAgentState) {
        AgentState = aAgentState;
    }
    public String getBranchType() {
        return BranchType;
    }
    public void setBranchType(String aBranchType) {
        BranchType = aBranchType;
    }
    public String getOperator() {
        return Operator;
    }
    public void setOperator(String aOperator) {
        Operator = aOperator;
    }
    public String getMakeDate() {
        return MakeDate;
    }
    public void setMakeDate(String aMakeDate) {
        MakeDate = aMakeDate;
    }
    public String getMakeTime() {
        return MakeTime;
    }
    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }
    public String getModifyDate() {
        return ModifyDate;
    }
    public void setModifyDate(String aModifyDate) {
        ModifyDate = aModifyDate;
    }
    public String getModifyTime() {
        return ModifyTime;
    }
    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }
    public String getState1() {
        return State1;
    }
    public void setState1(String aState1) {
        State1 = aState1;
    }
    public String getPhone() {
        return Phone;
    }
    public void setPhone(String aPhone) {
        Phone = aPhone;
    }
    public String getMobile() {
        return Mobile;
    }
    public void setMobile(String aMobile) {
        Mobile = aMobile;
    }
    public String getSerialNo() {
        return SerialNo;
    }
    public void setSerialNo(String aSerialNo) {
        SerialNo = aSerialNo;
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("ManageCom") ) {
            return 0;
        }
        if( strFieldName.equals("AgentCom") ) {
            return 1;
        }
        if( strFieldName.equals("OutAgentCode") ) {
            return 2;
        }
        if( strFieldName.equals("AgentName") ) {
            return 3;
        }
        if( strFieldName.equals("IDType") ) {
            return 4;
        }
        if( strFieldName.equals("IDNo") ) {
            return 5;
        }
        if( strFieldName.equals("QualType") ) {
            return 6;
        }
        if( strFieldName.equals("QualNo") ) {
            return 7;
        }
        if( strFieldName.equals("QualStartDate") ) {
            return 8;
        }
        if( strFieldName.equals("QualEndDate") ) {
            return 9;
        }
        if( strFieldName.equals("AgentState") ) {
            return 10;
        }
        if( strFieldName.equals("BranchType") ) {
            return 11;
        }
        if( strFieldName.equals("Operator") ) {
            return 12;
        }
        if( strFieldName.equals("MakeDate") ) {
            return 13;
        }
        if( strFieldName.equals("MakeTime") ) {
            return 14;
        }
        if( strFieldName.equals("ModifyDate") ) {
            return 15;
        }
        if( strFieldName.equals("ModifyTime") ) {
            return 16;
        }
        if( strFieldName.equals("State1") ) {
            return 17;
        }
        if( strFieldName.equals("Phone") ) {
            return 18;
        }
        if( strFieldName.equals("Mobile") ) {
            return 19;
        }
        if( strFieldName.equals("SerialNo") ) {
            return 20;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "ManageCom";
                break;
            case 1:
                strFieldName = "AgentCom";
                break;
            case 2:
                strFieldName = "OutAgentCode";
                break;
            case 3:
                strFieldName = "AgentName";
                break;
            case 4:
                strFieldName = "IDType";
                break;
            case 5:
                strFieldName = "IDNo";
                break;
            case 6:
                strFieldName = "QualType";
                break;
            case 7:
                strFieldName = "QualNo";
                break;
            case 8:
                strFieldName = "QualStartDate";
                break;
            case 9:
                strFieldName = "QualEndDate";
                break;
            case 10:
                strFieldName = "AgentState";
                break;
            case 11:
                strFieldName = "BranchType";
                break;
            case 12:
                strFieldName = "Operator";
                break;
            case 13:
                strFieldName = "MakeDate";
                break;
            case 14:
                strFieldName = "MakeTime";
                break;
            case 15:
                strFieldName = "ModifyDate";
                break;
            case 16:
                strFieldName = "ModifyTime";
                break;
            case 17:
                strFieldName = "State1";
                break;
            case 18:
                strFieldName = "Phone";
                break;
            case 19:
                strFieldName = "Mobile";
                break;
            case 20:
                strFieldName = "SerialNo";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "MANAGECOM":
                return Schema.TYPE_STRING;
            case "AGENTCOM":
                return Schema.TYPE_STRING;
            case "OUTAGENTCODE":
                return Schema.TYPE_STRING;
            case "AGENTNAME":
                return Schema.TYPE_STRING;
            case "IDTYPE":
                return Schema.TYPE_STRING;
            case "IDNO":
                return Schema.TYPE_STRING;
            case "QUALTYPE":
                return Schema.TYPE_STRING;
            case "QUALNO":
                return Schema.TYPE_STRING;
            case "QUALSTARTDATE":
                return Schema.TYPE_STRING;
            case "QUALENDDATE":
                return Schema.TYPE_STRING;
            case "AGENTSTATE":
                return Schema.TYPE_STRING;
            case "BRANCHTYPE":
                return Schema.TYPE_STRING;
            case "OPERATOR":
                return Schema.TYPE_STRING;
            case "MAKEDATE":
                return Schema.TYPE_STRING;
            case "MAKETIME":
                return Schema.TYPE_STRING;
            case "MODIFYDATE":
                return Schema.TYPE_STRING;
            case "MODIFYTIME":
                return Schema.TYPE_STRING;
            case "STATE1":
                return Schema.TYPE_STRING;
            case "PHONE":
                return Schema.TYPE_STRING;
            case "MOBILE":
                return Schema.TYPE_STRING;
            case "SERIALNO":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_STRING;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_STRING;
            case 6:
                return Schema.TYPE_STRING;
            case 7:
                return Schema.TYPE_STRING;
            case 8:
                return Schema.TYPE_STRING;
            case 9:
                return Schema.TYPE_STRING;
            case 10:
                return Schema.TYPE_STRING;
            case 11:
                return Schema.TYPE_STRING;
            case 12:
                return Schema.TYPE_STRING;
            case 13:
                return Schema.TYPE_STRING;
            case 14:
                return Schema.TYPE_STRING;
            case 15:
                return Schema.TYPE_STRING;
            case 16:
                return Schema.TYPE_STRING;
            case 17:
                return Schema.TYPE_STRING;
            case 18:
                return Schema.TYPE_STRING;
            case 19:
                return Schema.TYPE_STRING;
            case 20:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("ManageCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
        }
        if (FCode.equalsIgnoreCase("AgentCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentCom));
        }
        if (FCode.equalsIgnoreCase("OutAgentCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OutAgentCode));
        }
        if (FCode.equalsIgnoreCase("AgentName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentName));
        }
        if (FCode.equalsIgnoreCase("IDType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(IDType));
        }
        if (FCode.equalsIgnoreCase("IDNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(IDNo));
        }
        if (FCode.equalsIgnoreCase("QualType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(QualType));
        }
        if (FCode.equalsIgnoreCase("QualNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(QualNo));
        }
        if (FCode.equalsIgnoreCase("QualStartDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(QualStartDate));
        }
        if (FCode.equalsIgnoreCase("QualEndDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(QualEndDate));
        }
        if (FCode.equalsIgnoreCase("AgentState")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentState));
        }
        if (FCode.equalsIgnoreCase("BranchType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BranchType));
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeDate));
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyDate));
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (FCode.equalsIgnoreCase("State1")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(State1));
        }
        if (FCode.equalsIgnoreCase("Phone")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Phone));
        }
        if (FCode.equalsIgnoreCase("Mobile")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Mobile));
        }
        if (FCode.equalsIgnoreCase("SerialNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SerialNo));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = String.valueOf(ManageCom);
                break;
            case 1:
                strFieldValue = String.valueOf(AgentCom);
                break;
            case 2:
                strFieldValue = String.valueOf(OutAgentCode);
                break;
            case 3:
                strFieldValue = String.valueOf(AgentName);
                break;
            case 4:
                strFieldValue = String.valueOf(IDType);
                break;
            case 5:
                strFieldValue = String.valueOf(IDNo);
                break;
            case 6:
                strFieldValue = String.valueOf(QualType);
                break;
            case 7:
                strFieldValue = String.valueOf(QualNo);
                break;
            case 8:
                strFieldValue = String.valueOf(QualStartDate);
                break;
            case 9:
                strFieldValue = String.valueOf(QualEndDate);
                break;
            case 10:
                strFieldValue = String.valueOf(AgentState);
                break;
            case 11:
                strFieldValue = String.valueOf(BranchType);
                break;
            case 12:
                strFieldValue = String.valueOf(Operator);
                break;
            case 13:
                strFieldValue = String.valueOf(MakeDate);
                break;
            case 14:
                strFieldValue = String.valueOf(MakeTime);
                break;
            case 15:
                strFieldValue = String.valueOf(ModifyDate);
                break;
            case 16:
                strFieldValue = String.valueOf(ModifyTime);
                break;
            case 17:
                strFieldValue = String.valueOf(State1);
                break;
            case 18:
                strFieldValue = String.valueOf(Phone);
                break;
            case 19:
                strFieldValue = String.valueOf(Mobile);
                break;
            case 20:
                strFieldValue = String.valueOf(SerialNo);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("ManageCom")) {
            if( FValue != null && !FValue.equals(""))
            {
                ManageCom = FValue.trim();
            }
            else
                ManageCom = null;
        }
        if (FCode.equalsIgnoreCase("AgentCom")) {
            if( FValue != null && !FValue.equals(""))
            {
                AgentCom = FValue.trim();
            }
            else
                AgentCom = null;
        }
        if (FCode.equalsIgnoreCase("OutAgentCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                OutAgentCode = FValue.trim();
            }
            else
                OutAgentCode = null;
        }
        if (FCode.equalsIgnoreCase("AgentName")) {
            if( FValue != null && !FValue.equals(""))
            {
                AgentName = FValue.trim();
            }
            else
                AgentName = null;
        }
        if (FCode.equalsIgnoreCase("IDType")) {
            if( FValue != null && !FValue.equals(""))
            {
                IDType = FValue.trim();
            }
            else
                IDType = null;
        }
        if (FCode.equalsIgnoreCase("IDNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                IDNo = FValue.trim();
            }
            else
                IDNo = null;
        }
        if (FCode.equalsIgnoreCase("QualType")) {
            if( FValue != null && !FValue.equals(""))
            {
                QualType = FValue.trim();
            }
            else
                QualType = null;
        }
        if (FCode.equalsIgnoreCase("QualNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                QualNo = FValue.trim();
            }
            else
                QualNo = null;
        }
        if (FCode.equalsIgnoreCase("QualStartDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                QualStartDate = FValue.trim();
            }
            else
                QualStartDate = null;
        }
        if (FCode.equalsIgnoreCase("QualEndDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                QualEndDate = FValue.trim();
            }
            else
                QualEndDate = null;
        }
        if (FCode.equalsIgnoreCase("AgentState")) {
            if( FValue != null && !FValue.equals(""))
            {
                AgentState = FValue.trim();
            }
            else
                AgentState = null;
        }
        if (FCode.equalsIgnoreCase("BranchType")) {
            if( FValue != null && !FValue.equals(""))
            {
                BranchType = FValue.trim();
            }
            else
                BranchType = null;
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            if( FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
                Operator = null;
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeDate = FValue.trim();
            }
            else
                MakeDate = null;
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
                MakeTime = null;
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyDate = FValue.trim();
            }
            else
                ModifyDate = null;
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
                ModifyTime = null;
        }
        if (FCode.equalsIgnoreCase("State1")) {
            if( FValue != null && !FValue.equals(""))
            {
                State1 = FValue.trim();
            }
            else
                State1 = null;
        }
        if (FCode.equalsIgnoreCase("Phone")) {
            if( FValue != null && !FValue.equals(""))
            {
                Phone = FValue.trim();
            }
            else
                Phone = null;
        }
        if (FCode.equalsIgnoreCase("Mobile")) {
            if( FValue != null && !FValue.equals(""))
            {
                Mobile = FValue.trim();
            }
            else
                Mobile = null;
        }
        if (FCode.equalsIgnoreCase("SerialNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                SerialNo = FValue.trim();
            }
            else
                SerialNo = null;
        }
        return true;
    }


    public String toString() {
    return "LAAgent1Pojo [" +
            "ManageCom="+ManageCom +
            ", AgentCom="+AgentCom +
            ", OutAgentCode="+OutAgentCode +
            ", AgentName="+AgentName +
            ", IDType="+IDType +
            ", IDNo="+IDNo +
            ", QualType="+QualType +
            ", QualNo="+QualNo +
            ", QualStartDate="+QualStartDate +
            ", QualEndDate="+QualEndDate +
            ", AgentState="+AgentState +
            ", BranchType="+BranchType +
            ", Operator="+Operator +
            ", MakeDate="+MakeDate +
            ", MakeTime="+MakeTime +
            ", ModifyDate="+ModifyDate +
            ", ModifyTime="+ModifyTime +
            ", State1="+State1 +
            ", Phone="+Phone +
            ", Mobile="+Mobile +
            ", SerialNo="+SerialNo +"]";
    }
}
