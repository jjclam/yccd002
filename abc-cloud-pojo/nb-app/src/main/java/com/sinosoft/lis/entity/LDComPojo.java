/**
 * Copyright (c) 2019 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.entity;

import com.sinosoft.cloud.cache.annotation.RedisPrimaryHKey;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.Pojo;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;

import java.io.Serializable;

/**
 * <p>ClassName: LDComPojo </p>
 * <p>Copyright: Copyright (c) 2019</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2019-03-21
 */
public class LDComPojo implements  Pojo,Serializable {
    // @Field
    /** 机构编码 */
    @RedisPrimaryHKey
    private String ComCode; 
    /** 对外公布的机构代码 */
    private String OutComCode; 
    /** 机构名称 */
    private String Name; 
    /** 短名称 */
    private String ShortName; 
    /** 机构地址 */
    private String Address; 
    /** 机构邮编 */
    private String ZipCode; 
    /** 机构电话 */
    private String Phone; 
    /** 机构传真 */
    private String Fax; 
    /** Email */
    private String EMail; 
    /** 网址 */
    private String WebAddress; 
    /** 主管人姓名 */
    private String SatrapName; 
    /** 对应保监办代码 */
    private String InsuMonitorCode; 
    /** 保险公司id */
    private String InsureID; 
    /** 标识码 */
    private String SignID; 
    /** 行政区划代码 */
    private String RegionalismCode; 
    /** 公司性质 */
    private String ComNature; 
    /** 校验码 */
    private String ValidCode; 
    /** 标志 */
    private String Sign; 
    /** 机构所在市规模 */
    private String ComCitySize; 
    /** 服务机构名称 */
    private String ServiceName; 
    /** 服务机构编码 */
    private String ServiceNo; 
    /** 服务电话 */
    private String ServicePhone; 
    /** 服务投递地址 */
    private String ServicePostAddress; 
    /** 机构级别 */
    private String COMGRADE; 
    /** 机构地区类型 */
    private String COMAREATYPE; 
    /** 上级机构代码 */
    private String UPCOMCODE; 
    /** Isdirunder */
    private String ISDIRUNDER; 
    /** Comareatype1 */
    private String COMAREATYPE1; 
    /** Province */
    private String PROVINCE; 
    /** City */
    private String CITY; 
    /** County */
    private String COUNTY; 
    /** Findb */
    private String FINDB; 
    /** Letterservicename */
    private String LETTERSERVICENAME; 
    /** Comstate */
    private String COMSTATE; 
    /** Compreparedate */
    private String  COMPREPAREDATE;
    /** Supmanagedate */
    private String  SUPMANAGEDATE;
    /** Supapprovaldate */
    private String  SUPAPPROVALDATE;
    /** Theopeningdate */
    private String  THEOPENINGDATE;
    /** Comreplycanceldate */
    private String  COMREPLYCANCELDATE;
    /** Supreplycanceldate */
    private String  SUPREPLYCANCELDATE;
    /** Buspapercanceldate */
    private String  BUSPAPERCANCELDATE;


    public static final int FIELDNUM = 41;    // 数据库表的字段个数

    private FDate fDate = new FDate();        // 处理日期
    public String getComCode() {
        return ComCode;
    }
    public void setComCode(String aComCode) {
        ComCode = aComCode;
    }
    public String getOutComCode() {
        return OutComCode;
    }
    public void setOutComCode(String aOutComCode) {
        OutComCode = aOutComCode;
    }
    public String getName() {
        return Name;
    }
    public void setName(String aName) {
        Name = aName;
    }
    public String getShortName() {
        return ShortName;
    }
    public void setShortName(String aShortName) {
        ShortName = aShortName;
    }
    public String getAddress() {
        return Address;
    }
    public void setAddress(String aAddress) {
        Address = aAddress;
    }
    public String getZipCode() {
        return ZipCode;
    }
    public void setZipCode(String aZipCode) {
        ZipCode = aZipCode;
    }
    public String getPhone() {
        return Phone;
    }
    public void setPhone(String aPhone) {
        Phone = aPhone;
    }
    public String getFax() {
        return Fax;
    }
    public void setFax(String aFax) {
        Fax = aFax;
    }
    public String getEMail() {
        return EMail;
    }
    public void setEMail(String aEMail) {
        EMail = aEMail;
    }
    public String getWebAddress() {
        return WebAddress;
    }
    public void setWebAddress(String aWebAddress) {
        WebAddress = aWebAddress;
    }
    public String getSatrapName() {
        return SatrapName;
    }
    public void setSatrapName(String aSatrapName) {
        SatrapName = aSatrapName;
    }
    public String getInsuMonitorCode() {
        return InsuMonitorCode;
    }
    public void setInsuMonitorCode(String aInsuMonitorCode) {
        InsuMonitorCode = aInsuMonitorCode;
    }
    public String getInsureID() {
        return InsureID;
    }
    public void setInsureID(String aInsureID) {
        InsureID = aInsureID;
    }
    public String getSignID() {
        return SignID;
    }
    public void setSignID(String aSignID) {
        SignID = aSignID;
    }
    public String getRegionalismCode() {
        return RegionalismCode;
    }
    public void setRegionalismCode(String aRegionalismCode) {
        RegionalismCode = aRegionalismCode;
    }
    public String getComNature() {
        return ComNature;
    }
    public void setComNature(String aComNature) {
        ComNature = aComNature;
    }
    public String getValidCode() {
        return ValidCode;
    }
    public void setValidCode(String aValidCode) {
        ValidCode = aValidCode;
    }
    public String getSign() {
        return Sign;
    }
    public void setSign(String aSign) {
        Sign = aSign;
    }
    public String getComCitySize() {
        return ComCitySize;
    }
    public void setComCitySize(String aComCitySize) {
        ComCitySize = aComCitySize;
    }
    public String getServiceName() {
        return ServiceName;
    }
    public void setServiceName(String aServiceName) {
        ServiceName = aServiceName;
    }
    public String getServiceNo() {
        return ServiceNo;
    }
    public void setServiceNo(String aServiceNo) {
        ServiceNo = aServiceNo;
    }
    public String getServicePhone() {
        return ServicePhone;
    }
    public void setServicePhone(String aServicePhone) {
        ServicePhone = aServicePhone;
    }
    public String getServicePostAddress() {
        return ServicePostAddress;
    }
    public void setServicePostAddress(String aServicePostAddress) {
        ServicePostAddress = aServicePostAddress;
    }
    public String getCOMGRADE() {
        return COMGRADE;
    }
    public void setCOMGRADE(String aCOMGRADE) {
        COMGRADE = aCOMGRADE;
    }
    public String getCOMAREATYPE() {
        return COMAREATYPE;
    }
    public void setCOMAREATYPE(String aCOMAREATYPE) {
        COMAREATYPE = aCOMAREATYPE;
    }
    public String getUPCOMCODE() {
        return UPCOMCODE;
    }
    public void setUPCOMCODE(String aUPCOMCODE) {
        UPCOMCODE = aUPCOMCODE;
    }
    public String getISDIRUNDER() {
        return ISDIRUNDER;
    }
    public void setISDIRUNDER(String aISDIRUNDER) {
        ISDIRUNDER = aISDIRUNDER;
    }
    public String getCOMAREATYPE1() {
        return COMAREATYPE1;
    }
    public void setCOMAREATYPE1(String aCOMAREATYPE1) {
        COMAREATYPE1 = aCOMAREATYPE1;
    }
    public String getPROVINCE() {
        return PROVINCE;
    }
    public void setPROVINCE(String aPROVINCE) {
        PROVINCE = aPROVINCE;
    }
    public String getCITY() {
        return CITY;
    }
    public void setCITY(String aCITY) {
        CITY = aCITY;
    }
    public String getCOUNTY() {
        return COUNTY;
    }
    public void setCOUNTY(String aCOUNTY) {
        COUNTY = aCOUNTY;
    }
    public String getFINDB() {
        return FINDB;
    }
    public void setFINDB(String aFINDB) {
        FINDB = aFINDB;
    }
    public String getLETTERSERVICENAME() {
        return LETTERSERVICENAME;
    }
    public void setLETTERSERVICENAME(String aLETTERSERVICENAME) {
        LETTERSERVICENAME = aLETTERSERVICENAME;
    }
    public String getCOMSTATE() {
        return COMSTATE;
    }
    public void setCOMSTATE(String aCOMSTATE) {
        COMSTATE = aCOMSTATE;
    }
    public String getCOMPREPAREDATE() {
        return COMPREPAREDATE;
    }
    public void setCOMPREPAREDATE(String aCOMPREPAREDATE) {
        COMPREPAREDATE = aCOMPREPAREDATE;
    }
    public String getSUPMANAGEDATE() {
        return SUPMANAGEDATE;
    }
    public void setSUPMANAGEDATE(String aSUPMANAGEDATE) {
        SUPMANAGEDATE = aSUPMANAGEDATE;
    }
    public String getSUPAPPROVALDATE() {
        return SUPAPPROVALDATE;
    }
    public void setSUPAPPROVALDATE(String aSUPAPPROVALDATE) {
        SUPAPPROVALDATE = aSUPAPPROVALDATE;
    }
    public String getTHEOPENINGDATE() {
        return THEOPENINGDATE;
    }
    public void setTHEOPENINGDATE(String aTHEOPENINGDATE) {
        THEOPENINGDATE = aTHEOPENINGDATE;
    }
    public String getCOMREPLYCANCELDATE() {
        return COMREPLYCANCELDATE;
    }
    public void setCOMREPLYCANCELDATE(String aCOMREPLYCANCELDATE) {
        COMREPLYCANCELDATE = aCOMREPLYCANCELDATE;
    }
    public String getSUPREPLYCANCELDATE() {
        return SUPREPLYCANCELDATE;
    }
    public void setSUPREPLYCANCELDATE(String aSUPREPLYCANCELDATE) {
        SUPREPLYCANCELDATE = aSUPREPLYCANCELDATE;
    }
    public String getBUSPAPERCANCELDATE() {
        return BUSPAPERCANCELDATE;
    }
    public void setBUSPAPERCANCELDATE(String aBUSPAPERCANCELDATE) {
        BUSPAPERCANCELDATE = aBUSPAPERCANCELDATE;
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("ComCode") ) {
            return 0;
        }
        if( strFieldName.equals("OutComCode") ) {
            return 1;
        }
        if( strFieldName.equals("Name") ) {
            return 2;
        }
        if( strFieldName.equals("ShortName") ) {
            return 3;
        }
        if( strFieldName.equals("Address") ) {
            return 4;
        }
        if( strFieldName.equals("ZipCode") ) {
            return 5;
        }
        if( strFieldName.equals("Phone") ) {
            return 6;
        }
        if( strFieldName.equals("Fax") ) {
            return 7;
        }
        if( strFieldName.equals("EMail") ) {
            return 8;
        }
        if( strFieldName.equals("WebAddress") ) {
            return 9;
        }
        if( strFieldName.equals("SatrapName") ) {
            return 10;
        }
        if( strFieldName.equals("InsuMonitorCode") ) {
            return 11;
        }
        if( strFieldName.equals("InsureID") ) {
            return 12;
        }
        if( strFieldName.equals("SignID") ) {
            return 13;
        }
        if( strFieldName.equals("RegionalismCode") ) {
            return 14;
        }
        if( strFieldName.equals("ComNature") ) {
            return 15;
        }
        if( strFieldName.equals("ValidCode") ) {
            return 16;
        }
        if( strFieldName.equals("Sign") ) {
            return 17;
        }
        if( strFieldName.equals("ComCitySize") ) {
            return 18;
        }
        if( strFieldName.equals("ServiceName") ) {
            return 19;
        }
        if( strFieldName.equals("ServiceNo") ) {
            return 20;
        }
        if( strFieldName.equals("ServicePhone") ) {
            return 21;
        }
        if( strFieldName.equals("ServicePostAddress") ) {
            return 22;
        }
        if( strFieldName.equals("COMGRADE") ) {
            return 23;
        }
        if( strFieldName.equals("COMAREATYPE") ) {
            return 24;
        }
        if( strFieldName.equals("UPCOMCODE") ) {
            return 25;
        }
        if( strFieldName.equals("ISDIRUNDER") ) {
            return 26;
        }
        if( strFieldName.equals("COMAREATYPE1") ) {
            return 27;
        }
        if( strFieldName.equals("PROVINCE") ) {
            return 28;
        }
        if( strFieldName.equals("CITY") ) {
            return 29;
        }
        if( strFieldName.equals("COUNTY") ) {
            return 30;
        }
        if( strFieldName.equals("FINDB") ) {
            return 31;
        }
        if( strFieldName.equals("LETTERSERVICENAME") ) {
            return 32;
        }
        if( strFieldName.equals("COMSTATE") ) {
            return 33;
        }
        if( strFieldName.equals("COMPREPAREDATE") ) {
            return 34;
        }
        if( strFieldName.equals("SUPMANAGEDATE") ) {
            return 35;
        }
        if( strFieldName.equals("SUPAPPROVALDATE") ) {
            return 36;
        }
        if( strFieldName.equals("THEOPENINGDATE") ) {
            return 37;
        }
        if( strFieldName.equals("COMREPLYCANCELDATE") ) {
            return 38;
        }
        if( strFieldName.equals("SUPREPLYCANCELDATE") ) {
            return 39;
        }
        if( strFieldName.equals("BUSPAPERCANCELDATE") ) {
            return 40;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "ComCode";
                break;
            case 1:
                strFieldName = "OutComCode";
                break;
            case 2:
                strFieldName = "Name";
                break;
            case 3:
                strFieldName = "ShortName";
                break;
            case 4:
                strFieldName = "Address";
                break;
            case 5:
                strFieldName = "ZipCode";
                break;
            case 6:
                strFieldName = "Phone";
                break;
            case 7:
                strFieldName = "Fax";
                break;
            case 8:
                strFieldName = "EMail";
                break;
            case 9:
                strFieldName = "WebAddress";
                break;
            case 10:
                strFieldName = "SatrapName";
                break;
            case 11:
                strFieldName = "InsuMonitorCode";
                break;
            case 12:
                strFieldName = "InsureID";
                break;
            case 13:
                strFieldName = "SignID";
                break;
            case 14:
                strFieldName = "RegionalismCode";
                break;
            case 15:
                strFieldName = "ComNature";
                break;
            case 16:
                strFieldName = "ValidCode";
                break;
            case 17:
                strFieldName = "Sign";
                break;
            case 18:
                strFieldName = "ComCitySize";
                break;
            case 19:
                strFieldName = "ServiceName";
                break;
            case 20:
                strFieldName = "ServiceNo";
                break;
            case 21:
                strFieldName = "ServicePhone";
                break;
            case 22:
                strFieldName = "ServicePostAddress";
                break;
            case 23:
                strFieldName = "COMGRADE";
                break;
            case 24:
                strFieldName = "COMAREATYPE";
                break;
            case 25:
                strFieldName = "UPCOMCODE";
                break;
            case 26:
                strFieldName = "ISDIRUNDER";
                break;
            case 27:
                strFieldName = "COMAREATYPE1";
                break;
            case 28:
                strFieldName = "PROVINCE";
                break;
            case 29:
                strFieldName = "CITY";
                break;
            case 30:
                strFieldName = "COUNTY";
                break;
            case 31:
                strFieldName = "FINDB";
                break;
            case 32:
                strFieldName = "LETTERSERVICENAME";
                break;
            case 33:
                strFieldName = "COMSTATE";
                break;
            case 34:
                strFieldName = "COMPREPAREDATE";
                break;
            case 35:
                strFieldName = "SUPMANAGEDATE";
                break;
            case 36:
                strFieldName = "SUPAPPROVALDATE";
                break;
            case 37:
                strFieldName = "THEOPENINGDATE";
                break;
            case 38:
                strFieldName = "COMREPLYCANCELDATE";
                break;
            case 39:
                strFieldName = "SUPREPLYCANCELDATE";
                break;
            case 40:
                strFieldName = "BUSPAPERCANCELDATE";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "COMCODE":
                return Schema.TYPE_STRING;
            case "OUTCOMCODE":
                return Schema.TYPE_STRING;
            case "NAME":
                return Schema.TYPE_STRING;
            case "SHORTNAME":
                return Schema.TYPE_STRING;
            case "ADDRESS":
                return Schema.TYPE_STRING;
            case "ZIPCODE":
                return Schema.TYPE_STRING;
            case "PHONE":
                return Schema.TYPE_STRING;
            case "FAX":
                return Schema.TYPE_STRING;
            case "EMAIL":
                return Schema.TYPE_STRING;
            case "WEBADDRESS":
                return Schema.TYPE_STRING;
            case "SATRAPNAME":
                return Schema.TYPE_STRING;
            case "INSUMONITORCODE":
                return Schema.TYPE_STRING;
            case "INSUREID":
                return Schema.TYPE_STRING;
            case "SIGNID":
                return Schema.TYPE_STRING;
            case "REGIONALISMCODE":
                return Schema.TYPE_STRING;
            case "COMNATURE":
                return Schema.TYPE_STRING;
            case "VALIDCODE":
                return Schema.TYPE_STRING;
            case "SIGN":
                return Schema.TYPE_STRING;
            case "COMCITYSIZE":
                return Schema.TYPE_STRING;
            case "SERVICENAME":
                return Schema.TYPE_STRING;
            case "SERVICENO":
                return Schema.TYPE_STRING;
            case "SERVICEPHONE":
                return Schema.TYPE_STRING;
            case "SERVICEPOSTADDRESS":
                return Schema.TYPE_STRING;
            case "COMGRADE":
                return Schema.TYPE_STRING;
            case "COMAREATYPE":
                return Schema.TYPE_STRING;
            case "UPCOMCODE":
                return Schema.TYPE_STRING;
            case "ISDIRUNDER":
                return Schema.TYPE_STRING;
            case "COMAREATYPE1":
                return Schema.TYPE_STRING;
            case "PROVINCE":
                return Schema.TYPE_STRING;
            case "CITY":
                return Schema.TYPE_STRING;
            case "COUNTY":
                return Schema.TYPE_STRING;
            case "FINDB":
                return Schema.TYPE_STRING;
            case "LETTERSERVICENAME":
                return Schema.TYPE_STRING;
            case "COMSTATE":
                return Schema.TYPE_STRING;
            case "COMPREPAREDATE":
                return Schema.TYPE_STRING;
            case "SUPMANAGEDATE":
                return Schema.TYPE_STRING;
            case "SUPAPPROVALDATE":
                return Schema.TYPE_STRING;
            case "THEOPENINGDATE":
                return Schema.TYPE_STRING;
            case "COMREPLYCANCELDATE":
                return Schema.TYPE_STRING;
            case "SUPREPLYCANCELDATE":
                return Schema.TYPE_STRING;
            case "BUSPAPERCANCELDATE":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_STRING;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_STRING;
            case 6:
                return Schema.TYPE_STRING;
            case 7:
                return Schema.TYPE_STRING;
            case 8:
                return Schema.TYPE_STRING;
            case 9:
                return Schema.TYPE_STRING;
            case 10:
                return Schema.TYPE_STRING;
            case 11:
                return Schema.TYPE_STRING;
            case 12:
                return Schema.TYPE_STRING;
            case 13:
                return Schema.TYPE_STRING;
            case 14:
                return Schema.TYPE_STRING;
            case 15:
                return Schema.TYPE_STRING;
            case 16:
                return Schema.TYPE_STRING;
            case 17:
                return Schema.TYPE_STRING;
            case 18:
                return Schema.TYPE_STRING;
            case 19:
                return Schema.TYPE_STRING;
            case 20:
                return Schema.TYPE_STRING;
            case 21:
                return Schema.TYPE_STRING;
            case 22:
                return Schema.TYPE_STRING;
            case 23:
                return Schema.TYPE_STRING;
            case 24:
                return Schema.TYPE_STRING;
            case 25:
                return Schema.TYPE_STRING;
            case 26:
                return Schema.TYPE_STRING;
            case 27:
                return Schema.TYPE_STRING;
            case 28:
                return Schema.TYPE_STRING;
            case 29:
                return Schema.TYPE_STRING;
            case 30:
                return Schema.TYPE_STRING;
            case 31:
                return Schema.TYPE_STRING;
            case 32:
                return Schema.TYPE_STRING;
            case 33:
                return Schema.TYPE_STRING;
            case 34:
                return Schema.TYPE_STRING;
            case 35:
                return Schema.TYPE_STRING;
            case 36:
                return Schema.TYPE_STRING;
            case 37:
                return Schema.TYPE_STRING;
            case 38:
                return Schema.TYPE_STRING;
            case 39:
                return Schema.TYPE_STRING;
            case 40:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("ComCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ComCode));
        }
        if (FCode.equalsIgnoreCase("OutComCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OutComCode));
        }
        if (FCode.equalsIgnoreCase("Name")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Name));
        }
        if (FCode.equalsIgnoreCase("ShortName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ShortName));
        }
        if (FCode.equalsIgnoreCase("Address")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Address));
        }
        if (FCode.equalsIgnoreCase("ZipCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ZipCode));
        }
        if (FCode.equalsIgnoreCase("Phone")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Phone));
        }
        if (FCode.equalsIgnoreCase("Fax")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Fax));
        }
        if (FCode.equalsIgnoreCase("EMail")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(EMail));
        }
        if (FCode.equalsIgnoreCase("WebAddress")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(WebAddress));
        }
        if (FCode.equalsIgnoreCase("SatrapName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SatrapName));
        }
        if (FCode.equalsIgnoreCase("InsuMonitorCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InsuMonitorCode));
        }
        if (FCode.equalsIgnoreCase("InsureID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InsureID));
        }
        if (FCode.equalsIgnoreCase("SignID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SignID));
        }
        if (FCode.equalsIgnoreCase("RegionalismCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RegionalismCode));
        }
        if (FCode.equalsIgnoreCase("ComNature")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ComNature));
        }
        if (FCode.equalsIgnoreCase("ValidCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ValidCode));
        }
        if (FCode.equalsIgnoreCase("Sign")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Sign));
        }
        if (FCode.equalsIgnoreCase("ComCitySize")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ComCitySize));
        }
        if (FCode.equalsIgnoreCase("ServiceName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ServiceName));
        }
        if (FCode.equalsIgnoreCase("ServiceNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ServiceNo));
        }
        if (FCode.equalsIgnoreCase("ServicePhone")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ServicePhone));
        }
        if (FCode.equalsIgnoreCase("ServicePostAddress")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ServicePostAddress));
        }
        if (FCode.equalsIgnoreCase("COMGRADE")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(COMGRADE));
        }
        if (FCode.equalsIgnoreCase("COMAREATYPE")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(COMAREATYPE));
        }
        if (FCode.equalsIgnoreCase("UPCOMCODE")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(UPCOMCODE));
        }
        if (FCode.equalsIgnoreCase("ISDIRUNDER")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ISDIRUNDER));
        }
        if (FCode.equalsIgnoreCase("COMAREATYPE1")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(COMAREATYPE1));
        }
        if (FCode.equalsIgnoreCase("PROVINCE")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PROVINCE));
        }
        if (FCode.equalsIgnoreCase("CITY")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CITY));
        }
        if (FCode.equalsIgnoreCase("COUNTY")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(COUNTY));
        }
        if (FCode.equalsIgnoreCase("FINDB")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FINDB));
        }
        if (FCode.equalsIgnoreCase("LETTERSERVICENAME")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(LETTERSERVICENAME));
        }
        if (FCode.equalsIgnoreCase("COMSTATE")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(COMSTATE));
        }
        if (FCode.equalsIgnoreCase("COMPREPAREDATE")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(COMPREPAREDATE));
        }
        if (FCode.equalsIgnoreCase("SUPMANAGEDATE")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SUPMANAGEDATE));
        }
        if (FCode.equalsIgnoreCase("SUPAPPROVALDATE")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SUPAPPROVALDATE));
        }
        if (FCode.equalsIgnoreCase("THEOPENINGDATE")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(THEOPENINGDATE));
        }
        if (FCode.equalsIgnoreCase("COMREPLYCANCELDATE")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(COMREPLYCANCELDATE));
        }
        if (FCode.equalsIgnoreCase("SUPREPLYCANCELDATE")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SUPREPLYCANCELDATE));
        }
        if (FCode.equalsIgnoreCase("BUSPAPERCANCELDATE")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BUSPAPERCANCELDATE));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = String.valueOf(ComCode);
                break;
            case 1:
                strFieldValue = String.valueOf(OutComCode);
                break;
            case 2:
                strFieldValue = String.valueOf(Name);
                break;
            case 3:
                strFieldValue = String.valueOf(ShortName);
                break;
            case 4:
                strFieldValue = String.valueOf(Address);
                break;
            case 5:
                strFieldValue = String.valueOf(ZipCode);
                break;
            case 6:
                strFieldValue = String.valueOf(Phone);
                break;
            case 7:
                strFieldValue = String.valueOf(Fax);
                break;
            case 8:
                strFieldValue = String.valueOf(EMail);
                break;
            case 9:
                strFieldValue = String.valueOf(WebAddress);
                break;
            case 10:
                strFieldValue = String.valueOf(SatrapName);
                break;
            case 11:
                strFieldValue = String.valueOf(InsuMonitorCode);
                break;
            case 12:
                strFieldValue = String.valueOf(InsureID);
                break;
            case 13:
                strFieldValue = String.valueOf(SignID);
                break;
            case 14:
                strFieldValue = String.valueOf(RegionalismCode);
                break;
            case 15:
                strFieldValue = String.valueOf(ComNature);
                break;
            case 16:
                strFieldValue = String.valueOf(ValidCode);
                break;
            case 17:
                strFieldValue = String.valueOf(Sign);
                break;
            case 18:
                strFieldValue = String.valueOf(ComCitySize);
                break;
            case 19:
                strFieldValue = String.valueOf(ServiceName);
                break;
            case 20:
                strFieldValue = String.valueOf(ServiceNo);
                break;
            case 21:
                strFieldValue = String.valueOf(ServicePhone);
                break;
            case 22:
                strFieldValue = String.valueOf(ServicePostAddress);
                break;
            case 23:
                strFieldValue = String.valueOf(COMGRADE);
                break;
            case 24:
                strFieldValue = String.valueOf(COMAREATYPE);
                break;
            case 25:
                strFieldValue = String.valueOf(UPCOMCODE);
                break;
            case 26:
                strFieldValue = String.valueOf(ISDIRUNDER);
                break;
            case 27:
                strFieldValue = String.valueOf(COMAREATYPE1);
                break;
            case 28:
                strFieldValue = String.valueOf(PROVINCE);
                break;
            case 29:
                strFieldValue = String.valueOf(CITY);
                break;
            case 30:
                strFieldValue = String.valueOf(COUNTY);
                break;
            case 31:
                strFieldValue = String.valueOf(FINDB);
                break;
            case 32:
                strFieldValue = String.valueOf(LETTERSERVICENAME);
                break;
            case 33:
                strFieldValue = String.valueOf(COMSTATE);
                break;
            case 34:
                strFieldValue = String.valueOf(COMPREPAREDATE);
                break;
            case 35:
                strFieldValue = String.valueOf(SUPMANAGEDATE);
                break;
            case 36:
                strFieldValue = String.valueOf(SUPAPPROVALDATE);
                break;
            case 37:
                strFieldValue = String.valueOf(THEOPENINGDATE);
                break;
            case 38:
                strFieldValue = String.valueOf(COMREPLYCANCELDATE);
                break;
            case 39:
                strFieldValue = String.valueOf(SUPREPLYCANCELDATE);
                break;
            case 40:
                strFieldValue = String.valueOf(BUSPAPERCANCELDATE);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("ComCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                ComCode = FValue.trim();
            }
            else
                ComCode = null;
        }
        if (FCode.equalsIgnoreCase("OutComCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                OutComCode = FValue.trim();
            }
            else
                OutComCode = null;
        }
        if (FCode.equalsIgnoreCase("Name")) {
            if( FValue != null && !FValue.equals(""))
            {
                Name = FValue.trim();
            }
            else
                Name = null;
        }
        if (FCode.equalsIgnoreCase("ShortName")) {
            if( FValue != null && !FValue.equals(""))
            {
                ShortName = FValue.trim();
            }
            else
                ShortName = null;
        }
        if (FCode.equalsIgnoreCase("Address")) {
            if( FValue != null && !FValue.equals(""))
            {
                Address = FValue.trim();
            }
            else
                Address = null;
        }
        if (FCode.equalsIgnoreCase("ZipCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                ZipCode = FValue.trim();
            }
            else
                ZipCode = null;
        }
        if (FCode.equalsIgnoreCase("Phone")) {
            if( FValue != null && !FValue.equals(""))
            {
                Phone = FValue.trim();
            }
            else
                Phone = null;
        }
        if (FCode.equalsIgnoreCase("Fax")) {
            if( FValue != null && !FValue.equals(""))
            {
                Fax = FValue.trim();
            }
            else
                Fax = null;
        }
        if (FCode.equalsIgnoreCase("EMail")) {
            if( FValue != null && !FValue.equals(""))
            {
                EMail = FValue.trim();
            }
            else
                EMail = null;
        }
        if (FCode.equalsIgnoreCase("WebAddress")) {
            if( FValue != null && !FValue.equals(""))
            {
                WebAddress = FValue.trim();
            }
            else
                WebAddress = null;
        }
        if (FCode.equalsIgnoreCase("SatrapName")) {
            if( FValue != null && !FValue.equals(""))
            {
                SatrapName = FValue.trim();
            }
            else
                SatrapName = null;
        }
        if (FCode.equalsIgnoreCase("InsuMonitorCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                InsuMonitorCode = FValue.trim();
            }
            else
                InsuMonitorCode = null;
        }
        if (FCode.equalsIgnoreCase("InsureID")) {
            if( FValue != null && !FValue.equals(""))
            {
                InsureID = FValue.trim();
            }
            else
                InsureID = null;
        }
        if (FCode.equalsIgnoreCase("SignID")) {
            if( FValue != null && !FValue.equals(""))
            {
                SignID = FValue.trim();
            }
            else
                SignID = null;
        }
        if (FCode.equalsIgnoreCase("RegionalismCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                RegionalismCode = FValue.trim();
            }
            else
                RegionalismCode = null;
        }
        if (FCode.equalsIgnoreCase("ComNature")) {
            if( FValue != null && !FValue.equals(""))
            {
                ComNature = FValue.trim();
            }
            else
                ComNature = null;
        }
        if (FCode.equalsIgnoreCase("ValidCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                ValidCode = FValue.trim();
            }
            else
                ValidCode = null;
        }
        if (FCode.equalsIgnoreCase("Sign")) {
            if( FValue != null && !FValue.equals(""))
            {
                Sign = FValue.trim();
            }
            else
                Sign = null;
        }
        if (FCode.equalsIgnoreCase("ComCitySize")) {
            if( FValue != null && !FValue.equals(""))
            {
                ComCitySize = FValue.trim();
            }
            else
                ComCitySize = null;
        }
        if (FCode.equalsIgnoreCase("ServiceName")) {
            if( FValue != null && !FValue.equals(""))
            {
                ServiceName = FValue.trim();
            }
            else
                ServiceName = null;
        }
        if (FCode.equalsIgnoreCase("ServiceNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                ServiceNo = FValue.trim();
            }
            else
                ServiceNo = null;
        }
        if (FCode.equalsIgnoreCase("ServicePhone")) {
            if( FValue != null && !FValue.equals(""))
            {
                ServicePhone = FValue.trim();
            }
            else
                ServicePhone = null;
        }
        if (FCode.equalsIgnoreCase("ServicePostAddress")) {
            if( FValue != null && !FValue.equals(""))
            {
                ServicePostAddress = FValue.trim();
            }
            else
                ServicePostAddress = null;
        }
        if (FCode.equalsIgnoreCase("COMGRADE")) {
            if( FValue != null && !FValue.equals(""))
            {
                COMGRADE = FValue.trim();
            }
            else
                COMGRADE = null;
        }
        if (FCode.equalsIgnoreCase("COMAREATYPE")) {
            if( FValue != null && !FValue.equals(""))
            {
                COMAREATYPE = FValue.trim();
            }
            else
                COMAREATYPE = null;
        }
        if (FCode.equalsIgnoreCase("UPCOMCODE")) {
            if( FValue != null && !FValue.equals(""))
            {
                UPCOMCODE = FValue.trim();
            }
            else
                UPCOMCODE = null;
        }
        if (FCode.equalsIgnoreCase("ISDIRUNDER")) {
            if( FValue != null && !FValue.equals(""))
            {
                ISDIRUNDER = FValue.trim();
            }
            else
                ISDIRUNDER = null;
        }
        if (FCode.equalsIgnoreCase("COMAREATYPE1")) {
            if( FValue != null && !FValue.equals(""))
            {
                COMAREATYPE1 = FValue.trim();
            }
            else
                COMAREATYPE1 = null;
        }
        if (FCode.equalsIgnoreCase("PROVINCE")) {
            if( FValue != null && !FValue.equals(""))
            {
                PROVINCE = FValue.trim();
            }
            else
                PROVINCE = null;
        }
        if (FCode.equalsIgnoreCase("CITY")) {
            if( FValue != null && !FValue.equals(""))
            {
                CITY = FValue.trim();
            }
            else
                CITY = null;
        }
        if (FCode.equalsIgnoreCase("COUNTY")) {
            if( FValue != null && !FValue.equals(""))
            {
                COUNTY = FValue.trim();
            }
            else
                COUNTY = null;
        }
        if (FCode.equalsIgnoreCase("FINDB")) {
            if( FValue != null && !FValue.equals(""))
            {
                FINDB = FValue.trim();
            }
            else
                FINDB = null;
        }
        if (FCode.equalsIgnoreCase("LETTERSERVICENAME")) {
            if( FValue != null && !FValue.equals(""))
            {
                LETTERSERVICENAME = FValue.trim();
            }
            else
                LETTERSERVICENAME = null;
        }
        if (FCode.equalsIgnoreCase("COMSTATE")) {
            if( FValue != null && !FValue.equals(""))
            {
                COMSTATE = FValue.trim();
            }
            else
                COMSTATE = null;
        }
        if (FCode.equalsIgnoreCase("COMPREPAREDATE")) {
            if( FValue != null && !FValue.equals(""))
            {
                COMPREPAREDATE = FValue.trim();
            }
            else
                COMPREPAREDATE = null;
        }
        if (FCode.equalsIgnoreCase("SUPMANAGEDATE")) {
            if( FValue != null && !FValue.equals(""))
            {
                SUPMANAGEDATE = FValue.trim();
            }
            else
                SUPMANAGEDATE = null;
        }
        if (FCode.equalsIgnoreCase("SUPAPPROVALDATE")) {
            if( FValue != null && !FValue.equals(""))
            {
                SUPAPPROVALDATE = FValue.trim();
            }
            else
                SUPAPPROVALDATE = null;
        }
        if (FCode.equalsIgnoreCase("THEOPENINGDATE")) {
            if( FValue != null && !FValue.equals(""))
            {
                THEOPENINGDATE = FValue.trim();
            }
            else
                THEOPENINGDATE = null;
        }
        if (FCode.equalsIgnoreCase("COMREPLYCANCELDATE")) {
            if( FValue != null && !FValue.equals(""))
            {
                COMREPLYCANCELDATE = FValue.trim();
            }
            else
                COMREPLYCANCELDATE = null;
        }
        if (FCode.equalsIgnoreCase("SUPREPLYCANCELDATE")) {
            if( FValue != null && !FValue.equals(""))
            {
                SUPREPLYCANCELDATE = FValue.trim();
            }
            else
                SUPREPLYCANCELDATE = null;
        }
        if (FCode.equalsIgnoreCase("BUSPAPERCANCELDATE")) {
            if( FValue != null && !FValue.equals(""))
            {
                BUSPAPERCANCELDATE = FValue.trim();
            }
            else
                BUSPAPERCANCELDATE = null;
        }
        return true;
    }


    public String toString() {
    return "LDComPojo [" +
            "ComCode="+ComCode +
            ", OutComCode="+OutComCode +
            ", Name="+Name +
            ", ShortName="+ShortName +
            ", Address="+Address +
            ", ZipCode="+ZipCode +
            ", Phone="+Phone +
            ", Fax="+Fax +
            ", EMail="+EMail +
            ", WebAddress="+WebAddress +
            ", SatrapName="+SatrapName +
            ", InsuMonitorCode="+InsuMonitorCode +
            ", InsureID="+InsureID +
            ", SignID="+SignID +
            ", RegionalismCode="+RegionalismCode +
            ", ComNature="+ComNature +
            ", ValidCode="+ValidCode +
            ", Sign="+Sign +
            ", ComCitySize="+ComCitySize +
            ", ServiceName="+ServiceName +
            ", ServiceNo="+ServiceNo +
            ", ServicePhone="+ServicePhone +
            ", ServicePostAddress="+ServicePostAddress +
            ", COMGRADE="+COMGRADE +
            ", COMAREATYPE="+COMAREATYPE +
            ", UPCOMCODE="+UPCOMCODE +
            ", ISDIRUNDER="+ISDIRUNDER +
            ", COMAREATYPE1="+COMAREATYPE1 +
            ", PROVINCE="+PROVINCE +
            ", CITY="+CITY +
            ", COUNTY="+COUNTY +
            ", FINDB="+FINDB +
            ", LETTERSERVICENAME="+LETTERSERVICENAME +
            ", COMSTATE="+COMSTATE +
            ", COMPREPAREDATE="+COMPREPAREDATE +
            ", SUPMANAGEDATE="+SUPMANAGEDATE +
            ", SUPAPPROVALDATE="+SUPAPPROVALDATE +
            ", THEOPENINGDATE="+THEOPENINGDATE +
            ", COMREPLYCANCELDATE="+COMREPLYCANCELDATE +
            ", SUPREPLYCANCELDATE="+SUPREPLYCANCELDATE +
            ", BUSPAPERCANCELDATE="+BUSPAPERCANCELDATE +"]";
    }
}
