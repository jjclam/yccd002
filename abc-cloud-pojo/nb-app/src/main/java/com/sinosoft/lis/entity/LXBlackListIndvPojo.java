/**
 * Copyright (c) 2017 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.entity;

import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.Pojo;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;

import java.io.Serializable;

/**
 * <p>ClassName: LXBlackListIndvPojo </p>
 * <p>Copyright: Copyright (c) 2017</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2017-10-17
 */
public class LXBlackListIndvPojo implements Pojo,Serializable {
    // @Field
    /** 黑名单id */
    private String BlacklistID; 
    /** 姓名 */
    private String Name; 
    /** 曾用名 */
    private String Usedname; 
    /** 性别 */
    private String Gender; 
    /** 籍贯(出生地) */
    private String Birthplace; 
    /** 国籍 */
    private String Nationality; 
    /** 证件类型 */
    private String IDType; 
    /** 证件号码 */
    private String IDNo; 
    /** 民族 */
    private String Nation; 
    /** 职业 */
    private String Occupation; 
    /** 职位 */
    private String Title; 
    /** 地址 */
    private String Address; 
    /** 其他信息 */
    private String OtherInfo; 
    /** 参考号码 */
    private String ReferNo; 
    /** 备注 */
    private String Remark; 
    /** 备用字段1 */
    private String StandbyString1; 
    /** 备用字段2 */
    private String StandbyString2; 
    /** 备用字段3 */
    private String StandbyString3; 
    /** 备用字段4 */
    private String StandbyString4; 
    /** 备用字段5 */
    private String StandbyString5; 
    /** 入机日期 */
    private String  MakeDate;
    /** 入机时间 */
    private String MakeTime; 
    /** 入机操作员 */
    private String MakeOperator; 
    /** 最后修改日期 */
    private String  ModifyDate;
    /** 最后修改时间 */
    private String ModifyTime; 
    /** 最后修改人 */
    private String ModifyOperator; 
    /** Middlename */
    private String MiddleName; 
    /** Surname */
    private String SurName; 
    /** 关联人id */
    private String RelevanceId; 
    /** 关联关系 */
    private String RelevanceType; 
    /** 名单性质 */
    private String ListProperty; 
    /** 描述1 */
    private String Description1Name; 
    /** 描述2 */
    private String Description2Name; 
    /** 描述3 */
    private String Description3Name; 
    /** 描述4 */
    private String ProFileNotes; 
    /** 描述5 */
    private String ReferenceName; 
    /** 个人/实体 */
    private String RecordType; 
    /** 出生日期 */
    private String DateOfBirth; 
    /** 生日下限 */
    private String MinBirthday; 
    /** 生日上限 */
    private String MaxBirthday; 
    /** 列入名单日期 */
    private String ListedDate; 
    /** 本国语言类别 */
    private String ScriptLanguageId; 
    /** Firstname */
    private String FirstName; 


    public static final int FIELDNUM = 43;    // 数据库表的字段个数

    private FDate fDate = new FDate();        // 处理日期
    public String getBlacklistID() {
        return BlacklistID;
    }
    public void setBlacklistID(String aBlacklistID) {
        BlacklistID = aBlacklistID;
    }
    public String getName() {
        return Name;
    }
    public void setName(String aName) {
        Name = aName;
    }
    public String getUsedname() {
        return Usedname;
    }
    public void setUsedname(String aUsedname) {
        Usedname = aUsedname;
    }
    public String getGender() {
        return Gender;
    }
    public void setGender(String aGender) {
        Gender = aGender;
    }
    public String getBirthplace() {
        return Birthplace;
    }
    public void setBirthplace(String aBirthplace) {
        Birthplace = aBirthplace;
    }
    public String getNationality() {
        return Nationality;
    }
    public void setNationality(String aNationality) {
        Nationality = aNationality;
    }
    public String getIDType() {
        return IDType;
    }
    public void setIDType(String aIDType) {
        IDType = aIDType;
    }
    public String getIDNo() {
        return IDNo;
    }
    public void setIDNo(String aIDNo) {
        IDNo = aIDNo;
    }
    public String getNation() {
        return Nation;
    }
    public void setNation(String aNation) {
        Nation = aNation;
    }
    public String getOccupation() {
        return Occupation;
    }
    public void setOccupation(String aOccupation) {
        Occupation = aOccupation;
    }
    public String getTitle() {
        return Title;
    }
    public void setTitle(String aTitle) {
        Title = aTitle;
    }
    public String getAddress() {
        return Address;
    }
    public void setAddress(String aAddress) {
        Address = aAddress;
    }
    public String getOtherInfo() {
        return OtherInfo;
    }
    public void setOtherInfo(String aOtherInfo) {
        OtherInfo = aOtherInfo;
    }
    public String getReferNo() {
        return ReferNo;
    }
    public void setReferNo(String aReferNo) {
        ReferNo = aReferNo;
    }
    public String getRemark() {
        return Remark;
    }
    public void setRemark(String aRemark) {
        Remark = aRemark;
    }
    public String getStandbyString1() {
        return StandbyString1;
    }
    public void setStandbyString1(String aStandbyString1) {
        StandbyString1 = aStandbyString1;
    }
    public String getStandbyString2() {
        return StandbyString2;
    }
    public void setStandbyString2(String aStandbyString2) {
        StandbyString2 = aStandbyString2;
    }
    public String getStandbyString3() {
        return StandbyString3;
    }
    public void setStandbyString3(String aStandbyString3) {
        StandbyString3 = aStandbyString3;
    }
    public String getStandbyString4() {
        return StandbyString4;
    }
    public void setStandbyString4(String aStandbyString4) {
        StandbyString4 = aStandbyString4;
    }
    public String getStandbyString5() {
        return StandbyString5;
    }
    public void setStandbyString5(String aStandbyString5) {
        StandbyString5 = aStandbyString5;
    }
    public String getMakeDate() {
        return MakeDate;
    }
    public void setMakeDate(String aMakeDate) {
        MakeDate = aMakeDate;
    }
    public String getMakeTime() {
        return MakeTime;
    }
    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }
    public String getMakeOperator() {
        return MakeOperator;
    }
    public void setMakeOperator(String aMakeOperator) {
        MakeOperator = aMakeOperator;
    }
    public String getModifyDate() {
        return ModifyDate;
    }
    public void setModifyDate(String aModifyDate) {
        ModifyDate = aModifyDate;
    }
    public String getModifyTime() {
        return ModifyTime;
    }
    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }
    public String getModifyOperator() {
        return ModifyOperator;
    }
    public void setModifyOperator(String aModifyOperator) {
        ModifyOperator = aModifyOperator;
    }
    public String getMiddleName() {
        return MiddleName;
    }
    public void setMiddleName(String aMiddleName) {
        MiddleName = aMiddleName;
    }
    public String getSurName() {
        return SurName;
    }
    public void setSurName(String aSurName) {
        SurName = aSurName;
    }
    public String getRelevanceId() {
        return RelevanceId;
    }
    public void setRelevanceId(String aRelevanceId) {
        RelevanceId = aRelevanceId;
    }
    public String getRelevanceType() {
        return RelevanceType;
    }
    public void setRelevanceType(String aRelevanceType) {
        RelevanceType = aRelevanceType;
    }
    public String getListProperty() {
        return ListProperty;
    }
    public void setListProperty(String aListProperty) {
        ListProperty = aListProperty;
    }
    public String getDescription1Name() {
        return Description1Name;
    }
    public void setDescription1Name(String aDescription1Name) {
        Description1Name = aDescription1Name;
    }
    public String getDescription2Name() {
        return Description2Name;
    }
    public void setDescription2Name(String aDescription2Name) {
        Description2Name = aDescription2Name;
    }
    public String getDescription3Name() {
        return Description3Name;
    }
    public void setDescription3Name(String aDescription3Name) {
        Description3Name = aDescription3Name;
    }
    public String getProFileNotes() {
        return ProFileNotes;
    }
    public void setProFileNotes(String aProFileNotes) {
        ProFileNotes = aProFileNotes;
    }
    public String getReferenceName() {
        return ReferenceName;
    }
    public void setReferenceName(String aReferenceName) {
        ReferenceName = aReferenceName;
    }
    public String getRecordType() {
        return RecordType;
    }
    public void setRecordType(String aRecordType) {
        RecordType = aRecordType;
    }
    public String getDateOfBirth() {
        return DateOfBirth;
    }
    public void setDateOfBirth(String aDateOfBirth) {
        DateOfBirth = aDateOfBirth;
    }
    public String getMinBirthday() {
        return MinBirthday;
    }
    public void setMinBirthday(String aMinBirthday) {
        MinBirthday = aMinBirthday;
    }
    public String getMaxBirthday() {
        return MaxBirthday;
    }
    public void setMaxBirthday(String aMaxBirthday) {
        MaxBirthday = aMaxBirthday;
    }
    public String getListedDate() {
        return ListedDate;
    }
    public void setListedDate(String aListedDate) {
        ListedDate = aListedDate;
    }
    public String getScriptLanguageId() {
        return ScriptLanguageId;
    }
    public void setScriptLanguageId(String aScriptLanguageId) {
        ScriptLanguageId = aScriptLanguageId;
    }
    public String getFirstName() {
        return FirstName;
    }
    public void setFirstName(String aFirstName) {
        FirstName = aFirstName;
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("BlacklistID") ) {
            return 0;
        }
        if( strFieldName.equals("Name") ) {
            return 1;
        }
        if( strFieldName.equals("Usedname") ) {
            return 2;
        }
        if( strFieldName.equals("Gender") ) {
            return 3;
        }
        if( strFieldName.equals("Birthplace") ) {
            return 4;
        }
        if( strFieldName.equals("Nationality") ) {
            return 5;
        }
        if( strFieldName.equals("IDType") ) {
            return 6;
        }
        if( strFieldName.equals("IDNo") ) {
            return 7;
        }
        if( strFieldName.equals("Nation") ) {
            return 8;
        }
        if( strFieldName.equals("Occupation") ) {
            return 9;
        }
        if( strFieldName.equals("Title") ) {
            return 10;
        }
        if( strFieldName.equals("Address") ) {
            return 11;
        }
        if( strFieldName.equals("OtherInfo") ) {
            return 12;
        }
        if( strFieldName.equals("ReferNo") ) {
            return 13;
        }
        if( strFieldName.equals("Remark") ) {
            return 14;
        }
        if( strFieldName.equals("StandbyString1") ) {
            return 15;
        }
        if( strFieldName.equals("StandbyString2") ) {
            return 16;
        }
        if( strFieldName.equals("StandbyString3") ) {
            return 17;
        }
        if( strFieldName.equals("StandbyString4") ) {
            return 18;
        }
        if( strFieldName.equals("StandbyString5") ) {
            return 19;
        }
        if( strFieldName.equals("MakeDate") ) {
            return 20;
        }
        if( strFieldName.equals("MakeTime") ) {
            return 21;
        }
        if( strFieldName.equals("MakeOperator") ) {
            return 22;
        }
        if( strFieldName.equals("ModifyDate") ) {
            return 23;
        }
        if( strFieldName.equals("ModifyTime") ) {
            return 24;
        }
        if( strFieldName.equals("ModifyOperator") ) {
            return 25;
        }
        if( strFieldName.equals("MiddleName") ) {
            return 26;
        }
        if( strFieldName.equals("SurName") ) {
            return 27;
        }
        if( strFieldName.equals("RelevanceId") ) {
            return 28;
        }
        if( strFieldName.equals("RelevanceType") ) {
            return 29;
        }
        if( strFieldName.equals("ListProperty") ) {
            return 30;
        }
        if( strFieldName.equals("Description1Name") ) {
            return 31;
        }
        if( strFieldName.equals("Description2Name") ) {
            return 32;
        }
        if( strFieldName.equals("Description3Name") ) {
            return 33;
        }
        if( strFieldName.equals("ProFileNotes") ) {
            return 34;
        }
        if( strFieldName.equals("ReferenceName") ) {
            return 35;
        }
        if( strFieldName.equals("RecordType") ) {
            return 36;
        }
        if( strFieldName.equals("DateOfBirth") ) {
            return 37;
        }
        if( strFieldName.equals("MinBirthday") ) {
            return 38;
        }
        if( strFieldName.equals("MaxBirthday") ) {
            return 39;
        }
        if( strFieldName.equals("ListedDate") ) {
            return 40;
        }
        if( strFieldName.equals("ScriptLanguageId") ) {
            return 41;
        }
        if( strFieldName.equals("FirstName") ) {
            return 42;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "BlacklistID";
                break;
            case 1:
                strFieldName = "Name";
                break;
            case 2:
                strFieldName = "Usedname";
                break;
            case 3:
                strFieldName = "Gender";
                break;
            case 4:
                strFieldName = "Birthplace";
                break;
            case 5:
                strFieldName = "Nationality";
                break;
            case 6:
                strFieldName = "IDType";
                break;
            case 7:
                strFieldName = "IDNo";
                break;
            case 8:
                strFieldName = "Nation";
                break;
            case 9:
                strFieldName = "Occupation";
                break;
            case 10:
                strFieldName = "Title";
                break;
            case 11:
                strFieldName = "Address";
                break;
            case 12:
                strFieldName = "OtherInfo";
                break;
            case 13:
                strFieldName = "ReferNo";
                break;
            case 14:
                strFieldName = "Remark";
                break;
            case 15:
                strFieldName = "StandbyString1";
                break;
            case 16:
                strFieldName = "StandbyString2";
                break;
            case 17:
                strFieldName = "StandbyString3";
                break;
            case 18:
                strFieldName = "StandbyString4";
                break;
            case 19:
                strFieldName = "StandbyString5";
                break;
            case 20:
                strFieldName = "MakeDate";
                break;
            case 21:
                strFieldName = "MakeTime";
                break;
            case 22:
                strFieldName = "MakeOperator";
                break;
            case 23:
                strFieldName = "ModifyDate";
                break;
            case 24:
                strFieldName = "ModifyTime";
                break;
            case 25:
                strFieldName = "ModifyOperator";
                break;
            case 26:
                strFieldName = "MiddleName";
                break;
            case 27:
                strFieldName = "SurName";
                break;
            case 28:
                strFieldName = "RelevanceId";
                break;
            case 29:
                strFieldName = "RelevanceType";
                break;
            case 30:
                strFieldName = "ListProperty";
                break;
            case 31:
                strFieldName = "Description1Name";
                break;
            case 32:
                strFieldName = "Description2Name";
                break;
            case 33:
                strFieldName = "Description3Name";
                break;
            case 34:
                strFieldName = "ProFileNotes";
                break;
            case 35:
                strFieldName = "ReferenceName";
                break;
            case 36:
                strFieldName = "RecordType";
                break;
            case 37:
                strFieldName = "DateOfBirth";
                break;
            case 38:
                strFieldName = "MinBirthday";
                break;
            case 39:
                strFieldName = "MaxBirthday";
                break;
            case 40:
                strFieldName = "ListedDate";
                break;
            case 41:
                strFieldName = "ScriptLanguageId";
                break;
            case 42:
                strFieldName = "FirstName";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "BLACKLISTID":
                return Schema.TYPE_STRING;
            case "NAME":
                return Schema.TYPE_STRING;
            case "USEDNAME":
                return Schema.TYPE_STRING;
            case "GENDER":
                return Schema.TYPE_STRING;
            case "BIRTHPLACE":
                return Schema.TYPE_STRING;
            case "NATIONALITY":
                return Schema.TYPE_STRING;
            case "IDTYPE":
                return Schema.TYPE_STRING;
            case "IDNO":
                return Schema.TYPE_STRING;
            case "NATION":
                return Schema.TYPE_STRING;
            case "OCCUPATION":
                return Schema.TYPE_STRING;
            case "TITLE":
                return Schema.TYPE_STRING;
            case "ADDRESS":
                return Schema.TYPE_STRING;
            case "OTHERINFO":
                return Schema.TYPE_STRING;
            case "REFERNO":
                return Schema.TYPE_STRING;
            case "REMARK":
                return Schema.TYPE_STRING;
            case "STANDBYSTRING1":
                return Schema.TYPE_STRING;
            case "STANDBYSTRING2":
                return Schema.TYPE_STRING;
            case "STANDBYSTRING3":
                return Schema.TYPE_STRING;
            case "STANDBYSTRING4":
                return Schema.TYPE_STRING;
            case "STANDBYSTRING5":
                return Schema.TYPE_STRING;
            case "MAKEDATE":
                return Schema.TYPE_STRING;
            case "MAKETIME":
                return Schema.TYPE_STRING;
            case "MAKEOPERATOR":
                return Schema.TYPE_STRING;
            case "MODIFYDATE":
                return Schema.TYPE_STRING;
            case "MODIFYTIME":
                return Schema.TYPE_STRING;
            case "MODIFYOPERATOR":
                return Schema.TYPE_STRING;
            case "MIDDLENAME":
                return Schema.TYPE_STRING;
            case "SURNAME":
                return Schema.TYPE_STRING;
            case "RELEVANCEID":
                return Schema.TYPE_STRING;
            case "RELEVANCETYPE":
                return Schema.TYPE_STRING;
            case "LISTPROPERTY":
                return Schema.TYPE_STRING;
            case "DESCRIPTION1NAME":
                return Schema.TYPE_STRING;
            case "DESCRIPTION2NAME":
                return Schema.TYPE_STRING;
            case "DESCRIPTION3NAME":
                return Schema.TYPE_STRING;
            case "PROFILENOTES":
                return Schema.TYPE_STRING;
            case "REFERENCENAME":
                return Schema.TYPE_STRING;
            case "RECORDTYPE":
                return Schema.TYPE_STRING;
            case "DATEOFBIRTH":
                return Schema.TYPE_STRING;
            case "MINBIRTHDAY":
                return Schema.TYPE_STRING;
            case "MAXBIRTHDAY":
                return Schema.TYPE_STRING;
            case "LISTEDDATE":
                return Schema.TYPE_STRING;
            case "SCRIPTLANGUAGEID":
                return Schema.TYPE_STRING;
            case "FIRSTNAME":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_STRING;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_STRING;
            case 6:
                return Schema.TYPE_STRING;
            case 7:
                return Schema.TYPE_STRING;
            case 8:
                return Schema.TYPE_STRING;
            case 9:
                return Schema.TYPE_STRING;
            case 10:
                return Schema.TYPE_STRING;
            case 11:
                return Schema.TYPE_STRING;
            case 12:
                return Schema.TYPE_STRING;
            case 13:
                return Schema.TYPE_STRING;
            case 14:
                return Schema.TYPE_STRING;
            case 15:
                return Schema.TYPE_STRING;
            case 16:
                return Schema.TYPE_STRING;
            case 17:
                return Schema.TYPE_STRING;
            case 18:
                return Schema.TYPE_STRING;
            case 19:
                return Schema.TYPE_STRING;
            case 20:
                return Schema.TYPE_STRING;
            case 21:
                return Schema.TYPE_STRING;
            case 22:
                return Schema.TYPE_STRING;
            case 23:
                return Schema.TYPE_STRING;
            case 24:
                return Schema.TYPE_STRING;
            case 25:
                return Schema.TYPE_STRING;
            case 26:
                return Schema.TYPE_STRING;
            case 27:
                return Schema.TYPE_STRING;
            case 28:
                return Schema.TYPE_STRING;
            case 29:
                return Schema.TYPE_STRING;
            case 30:
                return Schema.TYPE_STRING;
            case 31:
                return Schema.TYPE_STRING;
            case 32:
                return Schema.TYPE_STRING;
            case 33:
                return Schema.TYPE_STRING;
            case 34:
                return Schema.TYPE_STRING;
            case 35:
                return Schema.TYPE_STRING;
            case 36:
                return Schema.TYPE_STRING;
            case 37:
                return Schema.TYPE_STRING;
            case 38:
                return Schema.TYPE_STRING;
            case 39:
                return Schema.TYPE_STRING;
            case 40:
                return Schema.TYPE_STRING;
            case 41:
                return Schema.TYPE_STRING;
            case 42:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("BlacklistID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BlacklistID));
        }
        if (FCode.equalsIgnoreCase("Name")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Name));
        }
        if (FCode.equalsIgnoreCase("Usedname")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Usedname));
        }
        if (FCode.equalsIgnoreCase("Gender")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Gender));
        }
        if (FCode.equalsIgnoreCase("Birthplace")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Birthplace));
        }
        if (FCode.equalsIgnoreCase("Nationality")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Nationality));
        }
        if (FCode.equalsIgnoreCase("IDType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(IDType));
        }
        if (FCode.equalsIgnoreCase("IDNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(IDNo));
        }
        if (FCode.equalsIgnoreCase("Nation")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Nation));
        }
        if (FCode.equalsIgnoreCase("Occupation")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Occupation));
        }
        if (FCode.equalsIgnoreCase("Title")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Title));
        }
        if (FCode.equalsIgnoreCase("Address")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Address));
        }
        if (FCode.equalsIgnoreCase("OtherInfo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OtherInfo));
        }
        if (FCode.equalsIgnoreCase("ReferNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ReferNo));
        }
        if (FCode.equalsIgnoreCase("Remark")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Remark));
        }
        if (FCode.equalsIgnoreCase("StandbyString1")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StandbyString1));
        }
        if (FCode.equalsIgnoreCase("StandbyString2")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StandbyString2));
        }
        if (FCode.equalsIgnoreCase("StandbyString3")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StandbyString3));
        }
        if (FCode.equalsIgnoreCase("StandbyString4")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StandbyString4));
        }
        if (FCode.equalsIgnoreCase("StandbyString5")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StandbyString5));
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeDate));
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equalsIgnoreCase("MakeOperator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeOperator));
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyDate));
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (FCode.equalsIgnoreCase("ModifyOperator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyOperator));
        }
        if (FCode.equalsIgnoreCase("MiddleName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MiddleName));
        }
        if (FCode.equalsIgnoreCase("SurName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SurName));
        }
        if (FCode.equalsIgnoreCase("RelevanceId")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RelevanceId));
        }
        if (FCode.equalsIgnoreCase("RelevanceType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RelevanceType));
        }
        if (FCode.equalsIgnoreCase("ListProperty")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ListProperty));
        }
        if (FCode.equalsIgnoreCase("Description1Name")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Description1Name));
        }
        if (FCode.equalsIgnoreCase("Description2Name")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Description2Name));
        }
        if (FCode.equalsIgnoreCase("Description3Name")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Description3Name));
        }
        if (FCode.equalsIgnoreCase("ProFileNotes")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ProFileNotes));
        }
        if (FCode.equalsIgnoreCase("ReferenceName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ReferenceName));
        }
        if (FCode.equalsIgnoreCase("RecordType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RecordType));
        }
        if (FCode.equalsIgnoreCase("DateOfBirth")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DateOfBirth));
        }
        if (FCode.equalsIgnoreCase("MinBirthday")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MinBirthday));
        }
        if (FCode.equalsIgnoreCase("MaxBirthday")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MaxBirthday));
        }
        if (FCode.equalsIgnoreCase("ListedDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ListedDate));
        }
        if (FCode.equalsIgnoreCase("ScriptLanguageId")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ScriptLanguageId));
        }
        if (FCode.equalsIgnoreCase("FirstName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FirstName));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = String.valueOf(BlacklistID);
                break;
            case 1:
                strFieldValue = String.valueOf(Name);
                break;
            case 2:
                strFieldValue = String.valueOf(Usedname);
                break;
            case 3:
                strFieldValue = String.valueOf(Gender);
                break;
            case 4:
                strFieldValue = String.valueOf(Birthplace);
                break;
            case 5:
                strFieldValue = String.valueOf(Nationality);
                break;
            case 6:
                strFieldValue = String.valueOf(IDType);
                break;
            case 7:
                strFieldValue = String.valueOf(IDNo);
                break;
            case 8:
                strFieldValue = String.valueOf(Nation);
                break;
            case 9:
                strFieldValue = String.valueOf(Occupation);
                break;
            case 10:
                strFieldValue = String.valueOf(Title);
                break;
            case 11:
                strFieldValue = String.valueOf(Address);
                break;
            case 12:
                strFieldValue = String.valueOf(OtherInfo);
                break;
            case 13:
                strFieldValue = String.valueOf(ReferNo);
                break;
            case 14:
                strFieldValue = String.valueOf(Remark);
                break;
            case 15:
                strFieldValue = String.valueOf(StandbyString1);
                break;
            case 16:
                strFieldValue = String.valueOf(StandbyString2);
                break;
            case 17:
                strFieldValue = String.valueOf(StandbyString3);
                break;
            case 18:
                strFieldValue = String.valueOf(StandbyString4);
                break;
            case 19:
                strFieldValue = String.valueOf(StandbyString5);
                break;
            case 20:
                strFieldValue = String.valueOf(MakeDate);
                break;
            case 21:
                strFieldValue = String.valueOf(MakeTime);
                break;
            case 22:
                strFieldValue = String.valueOf(MakeOperator);
                break;
            case 23:
                strFieldValue = String.valueOf(ModifyDate);
                break;
            case 24:
                strFieldValue = String.valueOf(ModifyTime);
                break;
            case 25:
                strFieldValue = String.valueOf(ModifyOperator);
                break;
            case 26:
                strFieldValue = String.valueOf(MiddleName);
                break;
            case 27:
                strFieldValue = String.valueOf(SurName);
                break;
            case 28:
                strFieldValue = String.valueOf(RelevanceId);
                break;
            case 29:
                strFieldValue = String.valueOf(RelevanceType);
                break;
            case 30:
                strFieldValue = String.valueOf(ListProperty);
                break;
            case 31:
                strFieldValue = String.valueOf(Description1Name);
                break;
            case 32:
                strFieldValue = String.valueOf(Description2Name);
                break;
            case 33:
                strFieldValue = String.valueOf(Description3Name);
                break;
            case 34:
                strFieldValue = String.valueOf(ProFileNotes);
                break;
            case 35:
                strFieldValue = String.valueOf(ReferenceName);
                break;
            case 36:
                strFieldValue = String.valueOf(RecordType);
                break;
            case 37:
                strFieldValue = String.valueOf(DateOfBirth);
                break;
            case 38:
                strFieldValue = String.valueOf(MinBirthday);
                break;
            case 39:
                strFieldValue = String.valueOf(MaxBirthday);
                break;
            case 40:
                strFieldValue = String.valueOf(ListedDate);
                break;
            case 41:
                strFieldValue = String.valueOf(ScriptLanguageId);
                break;
            case 42:
                strFieldValue = String.valueOf(FirstName);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("BlacklistID")) {
            if( FValue != null && !FValue.equals(""))
            {
                BlacklistID = FValue.trim();
            }
            else
                BlacklistID = null;
        }
        if (FCode.equalsIgnoreCase("Name")) {
            if( FValue != null && !FValue.equals(""))
            {
                Name = FValue.trim();
            }
            else
                Name = null;
        }
        if (FCode.equalsIgnoreCase("Usedname")) {
            if( FValue != null && !FValue.equals(""))
            {
                Usedname = FValue.trim();
            }
            else
                Usedname = null;
        }
        if (FCode.equalsIgnoreCase("Gender")) {
            if( FValue != null && !FValue.equals(""))
            {
                Gender = FValue.trim();
            }
            else
                Gender = null;
        }
        if (FCode.equalsIgnoreCase("Birthplace")) {
            if( FValue != null && !FValue.equals(""))
            {
                Birthplace = FValue.trim();
            }
            else
                Birthplace = null;
        }
        if (FCode.equalsIgnoreCase("Nationality")) {
            if( FValue != null && !FValue.equals(""))
            {
                Nationality = FValue.trim();
            }
            else
                Nationality = null;
        }
        if (FCode.equalsIgnoreCase("IDType")) {
            if( FValue != null && !FValue.equals(""))
            {
                IDType = FValue.trim();
            }
            else
                IDType = null;
        }
        if (FCode.equalsIgnoreCase("IDNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                IDNo = FValue.trim();
            }
            else
                IDNo = null;
        }
        if (FCode.equalsIgnoreCase("Nation")) {
            if( FValue != null && !FValue.equals(""))
            {
                Nation = FValue.trim();
            }
            else
                Nation = null;
        }
        if (FCode.equalsIgnoreCase("Occupation")) {
            if( FValue != null && !FValue.equals(""))
            {
                Occupation = FValue.trim();
            }
            else
                Occupation = null;
        }
        if (FCode.equalsIgnoreCase("Title")) {
            if( FValue != null && !FValue.equals(""))
            {
                Title = FValue.trim();
            }
            else
                Title = null;
        }
        if (FCode.equalsIgnoreCase("Address")) {
            if( FValue != null && !FValue.equals(""))
            {
                Address = FValue.trim();
            }
            else
                Address = null;
        }
        if (FCode.equalsIgnoreCase("OtherInfo")) {
            if( FValue != null && !FValue.equals(""))
            {
                OtherInfo = FValue.trim();
            }
            else
                OtherInfo = null;
        }
        if (FCode.equalsIgnoreCase("ReferNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                ReferNo = FValue.trim();
            }
            else
                ReferNo = null;
        }
        if (FCode.equalsIgnoreCase("Remark")) {
            if( FValue != null && !FValue.equals(""))
            {
                Remark = FValue.trim();
            }
            else
                Remark = null;
        }
        if (FCode.equalsIgnoreCase("StandbyString1")) {
            if( FValue != null && !FValue.equals(""))
            {
                StandbyString1 = FValue.trim();
            }
            else
                StandbyString1 = null;
        }
        if (FCode.equalsIgnoreCase("StandbyString2")) {
            if( FValue != null && !FValue.equals(""))
            {
                StandbyString2 = FValue.trim();
            }
            else
                StandbyString2 = null;
        }
        if (FCode.equalsIgnoreCase("StandbyString3")) {
            if( FValue != null && !FValue.equals(""))
            {
                StandbyString3 = FValue.trim();
            }
            else
                StandbyString3 = null;
        }
        if (FCode.equalsIgnoreCase("StandbyString4")) {
            if( FValue != null && !FValue.equals(""))
            {
                StandbyString4 = FValue.trim();
            }
            else
                StandbyString4 = null;
        }
        if (FCode.equalsIgnoreCase("StandbyString5")) {
            if( FValue != null && !FValue.equals(""))
            {
                StandbyString5 = FValue.trim();
            }
            else
                StandbyString5 = null;
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeDate = FValue.trim();
            }
            else
                MakeDate = null;
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
                MakeTime = null;
        }
        if (FCode.equalsIgnoreCase("MakeOperator")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeOperator = FValue.trim();
            }
            else
                MakeOperator = null;
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyDate = FValue.trim();
            }
            else
                ModifyDate = null;
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
                ModifyTime = null;
        }
        if (FCode.equalsIgnoreCase("ModifyOperator")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyOperator = FValue.trim();
            }
            else
                ModifyOperator = null;
        }
        if (FCode.equalsIgnoreCase("MiddleName")) {
            if( FValue != null && !FValue.equals(""))
            {
                MiddleName = FValue.trim();
            }
            else
                MiddleName = null;
        }
        if (FCode.equalsIgnoreCase("SurName")) {
            if( FValue != null && !FValue.equals(""))
            {
                SurName = FValue.trim();
            }
            else
                SurName = null;
        }
        if (FCode.equalsIgnoreCase("RelevanceId")) {
            if( FValue != null && !FValue.equals(""))
            {
                RelevanceId = FValue.trim();
            }
            else
                RelevanceId = null;
        }
        if (FCode.equalsIgnoreCase("RelevanceType")) {
            if( FValue != null && !FValue.equals(""))
            {
                RelevanceType = FValue.trim();
            }
            else
                RelevanceType = null;
        }
        if (FCode.equalsIgnoreCase("ListProperty")) {
            if( FValue != null && !FValue.equals(""))
            {
                ListProperty = FValue.trim();
            }
            else
                ListProperty = null;
        }
        if (FCode.equalsIgnoreCase("Description1Name")) {
            if( FValue != null && !FValue.equals(""))
            {
                Description1Name = FValue.trim();
            }
            else
                Description1Name = null;
        }
        if (FCode.equalsIgnoreCase("Description2Name")) {
            if( FValue != null && !FValue.equals(""))
            {
                Description2Name = FValue.trim();
            }
            else
                Description2Name = null;
        }
        if (FCode.equalsIgnoreCase("Description3Name")) {
            if( FValue != null && !FValue.equals(""))
            {
                Description3Name = FValue.trim();
            }
            else
                Description3Name = null;
        }
        if (FCode.equalsIgnoreCase("ProFileNotes")) {
            if( FValue != null && !FValue.equals(""))
            {
                ProFileNotes = FValue.trim();
            }
            else
                ProFileNotes = null;
        }
        if (FCode.equalsIgnoreCase("ReferenceName")) {
            if( FValue != null && !FValue.equals(""))
            {
                ReferenceName = FValue.trim();
            }
            else
                ReferenceName = null;
        }
        if (FCode.equalsIgnoreCase("RecordType")) {
            if( FValue != null && !FValue.equals(""))
            {
                RecordType = FValue.trim();
            }
            else
                RecordType = null;
        }
        if (FCode.equalsIgnoreCase("DateOfBirth")) {
            if( FValue != null && !FValue.equals(""))
            {
                DateOfBirth = FValue.trim();
            }
            else
                DateOfBirth = null;
        }
        if (FCode.equalsIgnoreCase("MinBirthday")) {
            if( FValue != null && !FValue.equals(""))
            {
                MinBirthday = FValue.trim();
            }
            else
                MinBirthday = null;
        }
        if (FCode.equalsIgnoreCase("MaxBirthday")) {
            if( FValue != null && !FValue.equals(""))
            {
                MaxBirthday = FValue.trim();
            }
            else
                MaxBirthday = null;
        }
        if (FCode.equalsIgnoreCase("ListedDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                ListedDate = FValue.trim();
            }
            else
                ListedDate = null;
        }
        if (FCode.equalsIgnoreCase("ScriptLanguageId")) {
            if( FValue != null && !FValue.equals(""))
            {
                ScriptLanguageId = FValue.trim();
            }
            else
                ScriptLanguageId = null;
        }
        if (FCode.equalsIgnoreCase("FirstName")) {
            if( FValue != null && !FValue.equals(""))
            {
                FirstName = FValue.trim();
            }
            else
                FirstName = null;
        }
        return true;
    }


    public String toString() {
    return "LXBlackListIndvPojo [" +
            "BlacklistID="+BlacklistID +
            ", Name="+Name +
            ", Usedname="+Usedname +
            ", Gender="+Gender +
            ", Birthplace="+Birthplace +
            ", Nationality="+Nationality +
            ", IDType="+IDType +
            ", IDNo="+IDNo +
            ", Nation="+Nation +
            ", Occupation="+Occupation +
            ", Title="+Title +
            ", Address="+Address +
            ", OtherInfo="+OtherInfo +
            ", ReferNo="+ReferNo +
            ", Remark="+Remark +
            ", StandbyString1="+StandbyString1 +
            ", StandbyString2="+StandbyString2 +
            ", StandbyString3="+StandbyString3 +
            ", StandbyString4="+StandbyString4 +
            ", StandbyString5="+StandbyString5 +
            ", MakeDate="+MakeDate +
            ", MakeTime="+MakeTime +
            ", MakeOperator="+MakeOperator +
            ", ModifyDate="+ModifyDate +
            ", ModifyTime="+ModifyTime +
            ", ModifyOperator="+ModifyOperator +
            ", MiddleName="+MiddleName +
            ", SurName="+SurName +
            ", RelevanceId="+RelevanceId +
            ", RelevanceType="+RelevanceType +
            ", ListProperty="+ListProperty +
            ", Description1Name="+Description1Name +
            ", Description2Name="+Description2Name +
            ", Description3Name="+Description3Name +
            ", ProFileNotes="+ProFileNotes +
            ", ReferenceName="+ReferenceName +
            ", RecordType="+RecordType +
            ", DateOfBirth="+DateOfBirth +
            ", MinBirthday="+MinBirthday +
            ", MaxBirthday="+MaxBirthday +
            ", ListedDate="+ListedDate +
            ", ScriptLanguageId="+ScriptLanguageId +
            ", FirstName="+FirstName +"]";
    }
}
