/**
 * Copyright (c) 2017 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.entity;

import com.sinosoft.utility.Pojo;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;

import java.io.Serializable;

/**
 * <p>ClassName: VMS_OUTPUT_CUSTOMERPojo </p>
 * <p>Copyright: Copyright (c) 2017</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2017-10-17
 */
public class VMS_OUTPUT_CUSTOMERPojo implements Pojo,Serializable {
    // @Field
    /** 基准日 */
    private String VIC_INDATE; 
    /** 客户号 */
    private String VIC_CUSTOMER_ID; 
    /** 客户纳税人中文名称 */
    private String VIC_CUSTOMER_CNAME; 
    /** 客户纳税人识别号 */
    private String VIC_CUSTOMER_TAXNO; 
    /** 客户开户账号 */
    private String VIC_CUSTOMER_ACCOUNT; 
    /** 客户开户银行中文名称 */
    private String VIC_CUSTOMER_CBANK; 
    /** 客户电话 */
    private String VIC_CUSTOMER_PHONE; 
    /** 客户邮箱地址 */
    private String VIC_CUSTOMER_EMAIL; 
    /** 客户地址 */
    private String VIC_CUSTOMER_ADDRESS; 
    /** 客户纳税人类别 */
    private String VIC_TAXPAYER_TYPE; 
    /** 发票类型 */
    private String VIC_FAPIAO_TYPE; 
    /** 客户类型 */
    private String VIC_CUSTOMER_TYPE; 
    /** 客户是否打票 */
    private String VIC_CUSTOMER_FAPIAO_FLAG; 
    /** 客户国籍 */
    private String VIC_CUSTOMER_NATIONALITY; 
    /** 数据来源 */
    private String VIC_DATA_SOURCE; 
    /** 联系人名称 */
    private String VIC_LINK_NAME; 
    /** 联系人电话 */
    private String VIC_LINK_PHONE; 
    /** 联系人地址 */
    private String VIC_LINK_ADDRESS; 
    /** 客户邮编 */
    private String VIC_CUSTOMER_ZIP_CODE; 
    /** 上传处理标记 */
    private String VIC_CUSTOMER_UPLOAD_FLAG; 


    public static final int FIELDNUM = 20;    // 数据库表的字段个数
    public String getVIC_INDATE() {
        return VIC_INDATE;
    }
    public void setVIC_INDATE(String aVIC_INDATE) {
        VIC_INDATE = aVIC_INDATE;
    }
    public String getVIC_CUSTOMER_ID() {
        return VIC_CUSTOMER_ID;
    }
    public void setVIC_CUSTOMER_ID(String aVIC_CUSTOMER_ID) {
        VIC_CUSTOMER_ID = aVIC_CUSTOMER_ID;
    }
    public String getVIC_CUSTOMER_CNAME() {
        return VIC_CUSTOMER_CNAME;
    }
    public void setVIC_CUSTOMER_CNAME(String aVIC_CUSTOMER_CNAME) {
        VIC_CUSTOMER_CNAME = aVIC_CUSTOMER_CNAME;
    }
    public String getVIC_CUSTOMER_TAXNO() {
        return VIC_CUSTOMER_TAXNO;
    }
    public void setVIC_CUSTOMER_TAXNO(String aVIC_CUSTOMER_TAXNO) {
        VIC_CUSTOMER_TAXNO = aVIC_CUSTOMER_TAXNO;
    }
    public String getVIC_CUSTOMER_ACCOUNT() {
        return VIC_CUSTOMER_ACCOUNT;
    }
    public void setVIC_CUSTOMER_ACCOUNT(String aVIC_CUSTOMER_ACCOUNT) {
        VIC_CUSTOMER_ACCOUNT = aVIC_CUSTOMER_ACCOUNT;
    }
    public String getVIC_CUSTOMER_CBANK() {
        return VIC_CUSTOMER_CBANK;
    }
    public void setVIC_CUSTOMER_CBANK(String aVIC_CUSTOMER_CBANK) {
        VIC_CUSTOMER_CBANK = aVIC_CUSTOMER_CBANK;
    }
    public String getVIC_CUSTOMER_PHONE() {
        return VIC_CUSTOMER_PHONE;
    }
    public void setVIC_CUSTOMER_PHONE(String aVIC_CUSTOMER_PHONE) {
        VIC_CUSTOMER_PHONE = aVIC_CUSTOMER_PHONE;
    }
    public String getVIC_CUSTOMER_EMAIL() {
        return VIC_CUSTOMER_EMAIL;
    }
    public void setVIC_CUSTOMER_EMAIL(String aVIC_CUSTOMER_EMAIL) {
        VIC_CUSTOMER_EMAIL = aVIC_CUSTOMER_EMAIL;
    }
    public String getVIC_CUSTOMER_ADDRESS() {
        return VIC_CUSTOMER_ADDRESS;
    }
    public void setVIC_CUSTOMER_ADDRESS(String aVIC_CUSTOMER_ADDRESS) {
        VIC_CUSTOMER_ADDRESS = aVIC_CUSTOMER_ADDRESS;
    }
    public String getVIC_TAXPAYER_TYPE() {
        return VIC_TAXPAYER_TYPE;
    }
    public void setVIC_TAXPAYER_TYPE(String aVIC_TAXPAYER_TYPE) {
        VIC_TAXPAYER_TYPE = aVIC_TAXPAYER_TYPE;
    }
    public String getVIC_FAPIAO_TYPE() {
        return VIC_FAPIAO_TYPE;
    }
    public void setVIC_FAPIAO_TYPE(String aVIC_FAPIAO_TYPE) {
        VIC_FAPIAO_TYPE = aVIC_FAPIAO_TYPE;
    }
    public String getVIC_CUSTOMER_TYPE() {
        return VIC_CUSTOMER_TYPE;
    }
    public void setVIC_CUSTOMER_TYPE(String aVIC_CUSTOMER_TYPE) {
        VIC_CUSTOMER_TYPE = aVIC_CUSTOMER_TYPE;
    }
    public String getVIC_CUSTOMER_FAPIAO_FLAG() {
        return VIC_CUSTOMER_FAPIAO_FLAG;
    }
    public void setVIC_CUSTOMER_FAPIAO_FLAG(String aVIC_CUSTOMER_FAPIAO_FLAG) {
        VIC_CUSTOMER_FAPIAO_FLAG = aVIC_CUSTOMER_FAPIAO_FLAG;
    }
    public String getVIC_CUSTOMER_NATIONALITY() {
        return VIC_CUSTOMER_NATIONALITY;
    }
    public void setVIC_CUSTOMER_NATIONALITY(String aVIC_CUSTOMER_NATIONALITY) {
        VIC_CUSTOMER_NATIONALITY = aVIC_CUSTOMER_NATIONALITY;
    }
    public String getVIC_DATA_SOURCE() {
        return VIC_DATA_SOURCE;
    }
    public void setVIC_DATA_SOURCE(String aVIC_DATA_SOURCE) {
        VIC_DATA_SOURCE = aVIC_DATA_SOURCE;
    }
    public String getVIC_LINK_NAME() {
        return VIC_LINK_NAME;
    }
    public void setVIC_LINK_NAME(String aVIC_LINK_NAME) {
        VIC_LINK_NAME = aVIC_LINK_NAME;
    }
    public String getVIC_LINK_PHONE() {
        return VIC_LINK_PHONE;
    }
    public void setVIC_LINK_PHONE(String aVIC_LINK_PHONE) {
        VIC_LINK_PHONE = aVIC_LINK_PHONE;
    }
    public String getVIC_LINK_ADDRESS() {
        return VIC_LINK_ADDRESS;
    }
    public void setVIC_LINK_ADDRESS(String aVIC_LINK_ADDRESS) {
        VIC_LINK_ADDRESS = aVIC_LINK_ADDRESS;
    }
    public String getVIC_CUSTOMER_ZIP_CODE() {
        return VIC_CUSTOMER_ZIP_CODE;
    }
    public void setVIC_CUSTOMER_ZIP_CODE(String aVIC_CUSTOMER_ZIP_CODE) {
        VIC_CUSTOMER_ZIP_CODE = aVIC_CUSTOMER_ZIP_CODE;
    }
    public String getVIC_CUSTOMER_UPLOAD_FLAG() {
        return VIC_CUSTOMER_UPLOAD_FLAG;
    }
    public void setVIC_CUSTOMER_UPLOAD_FLAG(String aVIC_CUSTOMER_UPLOAD_FLAG) {
        VIC_CUSTOMER_UPLOAD_FLAG = aVIC_CUSTOMER_UPLOAD_FLAG;
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("VIC_INDATE") ) {
            return 0;
        }
        if( strFieldName.equals("VIC_CUSTOMER_ID") ) {
            return 1;
        }
        if( strFieldName.equals("VIC_CUSTOMER_CNAME") ) {
            return 2;
        }
        if( strFieldName.equals("VIC_CUSTOMER_TAXNO") ) {
            return 3;
        }
        if( strFieldName.equals("VIC_CUSTOMER_ACCOUNT") ) {
            return 4;
        }
        if( strFieldName.equals("VIC_CUSTOMER_CBANK") ) {
            return 5;
        }
        if( strFieldName.equals("VIC_CUSTOMER_PHONE") ) {
            return 6;
        }
        if( strFieldName.equals("VIC_CUSTOMER_EMAIL") ) {
            return 7;
        }
        if( strFieldName.equals("VIC_CUSTOMER_ADDRESS") ) {
            return 8;
        }
        if( strFieldName.equals("VIC_TAXPAYER_TYPE") ) {
            return 9;
        }
        if( strFieldName.equals("VIC_FAPIAO_TYPE") ) {
            return 10;
        }
        if( strFieldName.equals("VIC_CUSTOMER_TYPE") ) {
            return 11;
        }
        if( strFieldName.equals("VIC_CUSTOMER_FAPIAO_FLAG") ) {
            return 12;
        }
        if( strFieldName.equals("VIC_CUSTOMER_NATIONALITY") ) {
            return 13;
        }
        if( strFieldName.equals("VIC_DATA_SOURCE") ) {
            return 14;
        }
        if( strFieldName.equals("VIC_LINK_NAME") ) {
            return 15;
        }
        if( strFieldName.equals("VIC_LINK_PHONE") ) {
            return 16;
        }
        if( strFieldName.equals("VIC_LINK_ADDRESS") ) {
            return 17;
        }
        if( strFieldName.equals("VIC_CUSTOMER_ZIP_CODE") ) {
            return 18;
        }
        if( strFieldName.equals("VIC_CUSTOMER_UPLOAD_FLAG") ) {
            return 19;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "VIC_INDATE";
                break;
            case 1:
                strFieldName = "VIC_CUSTOMER_ID";
                break;
            case 2:
                strFieldName = "VIC_CUSTOMER_CNAME";
                break;
            case 3:
                strFieldName = "VIC_CUSTOMER_TAXNO";
                break;
            case 4:
                strFieldName = "VIC_CUSTOMER_ACCOUNT";
                break;
            case 5:
                strFieldName = "VIC_CUSTOMER_CBANK";
                break;
            case 6:
                strFieldName = "VIC_CUSTOMER_PHONE";
                break;
            case 7:
                strFieldName = "VIC_CUSTOMER_EMAIL";
                break;
            case 8:
                strFieldName = "VIC_CUSTOMER_ADDRESS";
                break;
            case 9:
                strFieldName = "VIC_TAXPAYER_TYPE";
                break;
            case 10:
                strFieldName = "VIC_FAPIAO_TYPE";
                break;
            case 11:
                strFieldName = "VIC_CUSTOMER_TYPE";
                break;
            case 12:
                strFieldName = "VIC_CUSTOMER_FAPIAO_FLAG";
                break;
            case 13:
                strFieldName = "VIC_CUSTOMER_NATIONALITY";
                break;
            case 14:
                strFieldName = "VIC_DATA_SOURCE";
                break;
            case 15:
                strFieldName = "VIC_LINK_NAME";
                break;
            case 16:
                strFieldName = "VIC_LINK_PHONE";
                break;
            case 17:
                strFieldName = "VIC_LINK_ADDRESS";
                break;
            case 18:
                strFieldName = "VIC_CUSTOMER_ZIP_CODE";
                break;
            case 19:
                strFieldName = "VIC_CUSTOMER_UPLOAD_FLAG";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "VIC_INDATE":
                return Schema.TYPE_STRING;
            case "VIC_CUSTOMER_ID":
                return Schema.TYPE_STRING;
            case "VIC_CUSTOMER_CNAME":
                return Schema.TYPE_STRING;
            case "VIC_CUSTOMER_TAXNO":
                return Schema.TYPE_STRING;
            case "VIC_CUSTOMER_ACCOUNT":
                return Schema.TYPE_STRING;
            case "VIC_CUSTOMER_CBANK":
                return Schema.TYPE_STRING;
            case "VIC_CUSTOMER_PHONE":
                return Schema.TYPE_STRING;
            case "VIC_CUSTOMER_EMAIL":
                return Schema.TYPE_STRING;
            case "VIC_CUSTOMER_ADDRESS":
                return Schema.TYPE_STRING;
            case "VIC_TAXPAYER_TYPE":
                return Schema.TYPE_STRING;
            case "VIC_FAPIAO_TYPE":
                return Schema.TYPE_STRING;
            case "VIC_CUSTOMER_TYPE":
                return Schema.TYPE_STRING;
            case "VIC_CUSTOMER_FAPIAO_FLAG":
                return Schema.TYPE_STRING;
            case "VIC_CUSTOMER_NATIONALITY":
                return Schema.TYPE_STRING;
            case "VIC_DATA_SOURCE":
                return Schema.TYPE_STRING;
            case "VIC_LINK_NAME":
                return Schema.TYPE_STRING;
            case "VIC_LINK_PHONE":
                return Schema.TYPE_STRING;
            case "VIC_LINK_ADDRESS":
                return Schema.TYPE_STRING;
            case "VIC_CUSTOMER_ZIP_CODE":
                return Schema.TYPE_STRING;
            case "VIC_CUSTOMER_UPLOAD_FLAG":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_STRING;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_STRING;
            case 6:
                return Schema.TYPE_STRING;
            case 7:
                return Schema.TYPE_STRING;
            case 8:
                return Schema.TYPE_STRING;
            case 9:
                return Schema.TYPE_STRING;
            case 10:
                return Schema.TYPE_STRING;
            case 11:
                return Schema.TYPE_STRING;
            case 12:
                return Schema.TYPE_STRING;
            case 13:
                return Schema.TYPE_STRING;
            case 14:
                return Schema.TYPE_STRING;
            case 15:
                return Schema.TYPE_STRING;
            case 16:
                return Schema.TYPE_STRING;
            case 17:
                return Schema.TYPE_STRING;
            case 18:
                return Schema.TYPE_STRING;
            case 19:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("VIC_INDATE")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(VIC_INDATE));
        }
        if (FCode.equalsIgnoreCase("VIC_CUSTOMER_ID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(VIC_CUSTOMER_ID));
        }
        if (FCode.equalsIgnoreCase("VIC_CUSTOMER_CNAME")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(VIC_CUSTOMER_CNAME));
        }
        if (FCode.equalsIgnoreCase("VIC_CUSTOMER_TAXNO")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(VIC_CUSTOMER_TAXNO));
        }
        if (FCode.equalsIgnoreCase("VIC_CUSTOMER_ACCOUNT")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(VIC_CUSTOMER_ACCOUNT));
        }
        if (FCode.equalsIgnoreCase("VIC_CUSTOMER_CBANK")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(VIC_CUSTOMER_CBANK));
        }
        if (FCode.equalsIgnoreCase("VIC_CUSTOMER_PHONE")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(VIC_CUSTOMER_PHONE));
        }
        if (FCode.equalsIgnoreCase("VIC_CUSTOMER_EMAIL")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(VIC_CUSTOMER_EMAIL));
        }
        if (FCode.equalsIgnoreCase("VIC_CUSTOMER_ADDRESS")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(VIC_CUSTOMER_ADDRESS));
        }
        if (FCode.equalsIgnoreCase("VIC_TAXPAYER_TYPE")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(VIC_TAXPAYER_TYPE));
        }
        if (FCode.equalsIgnoreCase("VIC_FAPIAO_TYPE")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(VIC_FAPIAO_TYPE));
        }
        if (FCode.equalsIgnoreCase("VIC_CUSTOMER_TYPE")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(VIC_CUSTOMER_TYPE));
        }
        if (FCode.equalsIgnoreCase("VIC_CUSTOMER_FAPIAO_FLAG")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(VIC_CUSTOMER_FAPIAO_FLAG));
        }
        if (FCode.equalsIgnoreCase("VIC_CUSTOMER_NATIONALITY")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(VIC_CUSTOMER_NATIONALITY));
        }
        if (FCode.equalsIgnoreCase("VIC_DATA_SOURCE")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(VIC_DATA_SOURCE));
        }
        if (FCode.equalsIgnoreCase("VIC_LINK_NAME")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(VIC_LINK_NAME));
        }
        if (FCode.equalsIgnoreCase("VIC_LINK_PHONE")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(VIC_LINK_PHONE));
        }
        if (FCode.equalsIgnoreCase("VIC_LINK_ADDRESS")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(VIC_LINK_ADDRESS));
        }
        if (FCode.equalsIgnoreCase("VIC_CUSTOMER_ZIP_CODE")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(VIC_CUSTOMER_ZIP_CODE));
        }
        if (FCode.equalsIgnoreCase("VIC_CUSTOMER_UPLOAD_FLAG")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(VIC_CUSTOMER_UPLOAD_FLAG));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = String.valueOf(VIC_INDATE);
                break;
            case 1:
                strFieldValue = String.valueOf(VIC_CUSTOMER_ID);
                break;
            case 2:
                strFieldValue = String.valueOf(VIC_CUSTOMER_CNAME);
                break;
            case 3:
                strFieldValue = String.valueOf(VIC_CUSTOMER_TAXNO);
                break;
            case 4:
                strFieldValue = String.valueOf(VIC_CUSTOMER_ACCOUNT);
                break;
            case 5:
                strFieldValue = String.valueOf(VIC_CUSTOMER_CBANK);
                break;
            case 6:
                strFieldValue = String.valueOf(VIC_CUSTOMER_PHONE);
                break;
            case 7:
                strFieldValue = String.valueOf(VIC_CUSTOMER_EMAIL);
                break;
            case 8:
                strFieldValue = String.valueOf(VIC_CUSTOMER_ADDRESS);
                break;
            case 9:
                strFieldValue = String.valueOf(VIC_TAXPAYER_TYPE);
                break;
            case 10:
                strFieldValue = String.valueOf(VIC_FAPIAO_TYPE);
                break;
            case 11:
                strFieldValue = String.valueOf(VIC_CUSTOMER_TYPE);
                break;
            case 12:
                strFieldValue = String.valueOf(VIC_CUSTOMER_FAPIAO_FLAG);
                break;
            case 13:
                strFieldValue = String.valueOf(VIC_CUSTOMER_NATIONALITY);
                break;
            case 14:
                strFieldValue = String.valueOf(VIC_DATA_SOURCE);
                break;
            case 15:
                strFieldValue = String.valueOf(VIC_LINK_NAME);
                break;
            case 16:
                strFieldValue = String.valueOf(VIC_LINK_PHONE);
                break;
            case 17:
                strFieldValue = String.valueOf(VIC_LINK_ADDRESS);
                break;
            case 18:
                strFieldValue = String.valueOf(VIC_CUSTOMER_ZIP_CODE);
                break;
            case 19:
                strFieldValue = String.valueOf(VIC_CUSTOMER_UPLOAD_FLAG);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("VIC_INDATE")) {
            if( FValue != null && !FValue.equals(""))
            {
                VIC_INDATE = FValue.trim();
            }
            else
                VIC_INDATE = null;
        }
        if (FCode.equalsIgnoreCase("VIC_CUSTOMER_ID")) {
            if( FValue != null && !FValue.equals(""))
            {
                VIC_CUSTOMER_ID = FValue.trim();
            }
            else
                VIC_CUSTOMER_ID = null;
        }
        if (FCode.equalsIgnoreCase("VIC_CUSTOMER_CNAME")) {
            if( FValue != null && !FValue.equals(""))
            {
                VIC_CUSTOMER_CNAME = FValue.trim();
            }
            else
                VIC_CUSTOMER_CNAME = null;
        }
        if (FCode.equalsIgnoreCase("VIC_CUSTOMER_TAXNO")) {
            if( FValue != null && !FValue.equals(""))
            {
                VIC_CUSTOMER_TAXNO = FValue.trim();
            }
            else
                VIC_CUSTOMER_TAXNO = null;
        }
        if (FCode.equalsIgnoreCase("VIC_CUSTOMER_ACCOUNT")) {
            if( FValue != null && !FValue.equals(""))
            {
                VIC_CUSTOMER_ACCOUNT = FValue.trim();
            }
            else
                VIC_CUSTOMER_ACCOUNT = null;
        }
        if (FCode.equalsIgnoreCase("VIC_CUSTOMER_CBANK")) {
            if( FValue != null && !FValue.equals(""))
            {
                VIC_CUSTOMER_CBANK = FValue.trim();
            }
            else
                VIC_CUSTOMER_CBANK = null;
        }
        if (FCode.equalsIgnoreCase("VIC_CUSTOMER_PHONE")) {
            if( FValue != null && !FValue.equals(""))
            {
                VIC_CUSTOMER_PHONE = FValue.trim();
            }
            else
                VIC_CUSTOMER_PHONE = null;
        }
        if (FCode.equalsIgnoreCase("VIC_CUSTOMER_EMAIL")) {
            if( FValue != null && !FValue.equals(""))
            {
                VIC_CUSTOMER_EMAIL = FValue.trim();
            }
            else
                VIC_CUSTOMER_EMAIL = null;
        }
        if (FCode.equalsIgnoreCase("VIC_CUSTOMER_ADDRESS")) {
            if( FValue != null && !FValue.equals(""))
            {
                VIC_CUSTOMER_ADDRESS = FValue.trim();
            }
            else
                VIC_CUSTOMER_ADDRESS = null;
        }
        if (FCode.equalsIgnoreCase("VIC_TAXPAYER_TYPE")) {
            if( FValue != null && !FValue.equals(""))
            {
                VIC_TAXPAYER_TYPE = FValue.trim();
            }
            else
                VIC_TAXPAYER_TYPE = null;
        }
        if (FCode.equalsIgnoreCase("VIC_FAPIAO_TYPE")) {
            if( FValue != null && !FValue.equals(""))
            {
                VIC_FAPIAO_TYPE = FValue.trim();
            }
            else
                VIC_FAPIAO_TYPE = null;
        }
        if (FCode.equalsIgnoreCase("VIC_CUSTOMER_TYPE")) {
            if( FValue != null && !FValue.equals(""))
            {
                VIC_CUSTOMER_TYPE = FValue.trim();
            }
            else
                VIC_CUSTOMER_TYPE = null;
        }
        if (FCode.equalsIgnoreCase("VIC_CUSTOMER_FAPIAO_FLAG")) {
            if( FValue != null && !FValue.equals(""))
            {
                VIC_CUSTOMER_FAPIAO_FLAG = FValue.trim();
            }
            else
                VIC_CUSTOMER_FAPIAO_FLAG = null;
        }
        if (FCode.equalsIgnoreCase("VIC_CUSTOMER_NATIONALITY")) {
            if( FValue != null && !FValue.equals(""))
            {
                VIC_CUSTOMER_NATIONALITY = FValue.trim();
            }
            else
                VIC_CUSTOMER_NATIONALITY = null;
        }
        if (FCode.equalsIgnoreCase("VIC_DATA_SOURCE")) {
            if( FValue != null && !FValue.equals(""))
            {
                VIC_DATA_SOURCE = FValue.trim();
            }
            else
                VIC_DATA_SOURCE = null;
        }
        if (FCode.equalsIgnoreCase("VIC_LINK_NAME")) {
            if( FValue != null && !FValue.equals(""))
            {
                VIC_LINK_NAME = FValue.trim();
            }
            else
                VIC_LINK_NAME = null;
        }
        if (FCode.equalsIgnoreCase("VIC_LINK_PHONE")) {
            if( FValue != null && !FValue.equals(""))
            {
                VIC_LINK_PHONE = FValue.trim();
            }
            else
                VIC_LINK_PHONE = null;
        }
        if (FCode.equalsIgnoreCase("VIC_LINK_ADDRESS")) {
            if( FValue != null && !FValue.equals(""))
            {
                VIC_LINK_ADDRESS = FValue.trim();
            }
            else
                VIC_LINK_ADDRESS = null;
        }
        if (FCode.equalsIgnoreCase("VIC_CUSTOMER_ZIP_CODE")) {
            if( FValue != null && !FValue.equals(""))
            {
                VIC_CUSTOMER_ZIP_CODE = FValue.trim();
            }
            else
                VIC_CUSTOMER_ZIP_CODE = null;
        }
        if (FCode.equalsIgnoreCase("VIC_CUSTOMER_UPLOAD_FLAG")) {
            if( FValue != null && !FValue.equals(""))
            {
                VIC_CUSTOMER_UPLOAD_FLAG = FValue.trim();
            }
            else
                VIC_CUSTOMER_UPLOAD_FLAG = null;
        }
        return true;
    }


    public String toString() {
    return "VMS_OUTPUT_CUSTOMERPojo [" +
            "VIC_INDATE="+VIC_INDATE +
            ", VIC_CUSTOMER_ID="+VIC_CUSTOMER_ID +
            ", VIC_CUSTOMER_CNAME="+VIC_CUSTOMER_CNAME +
            ", VIC_CUSTOMER_TAXNO="+VIC_CUSTOMER_TAXNO +
            ", VIC_CUSTOMER_ACCOUNT="+VIC_CUSTOMER_ACCOUNT +
            ", VIC_CUSTOMER_CBANK="+VIC_CUSTOMER_CBANK +
            ", VIC_CUSTOMER_PHONE="+VIC_CUSTOMER_PHONE +
            ", VIC_CUSTOMER_EMAIL="+VIC_CUSTOMER_EMAIL +
            ", VIC_CUSTOMER_ADDRESS="+VIC_CUSTOMER_ADDRESS +
            ", VIC_TAXPAYER_TYPE="+VIC_TAXPAYER_TYPE +
            ", VIC_FAPIAO_TYPE="+VIC_FAPIAO_TYPE +
            ", VIC_CUSTOMER_TYPE="+VIC_CUSTOMER_TYPE +
            ", VIC_CUSTOMER_FAPIAO_FLAG="+VIC_CUSTOMER_FAPIAO_FLAG +
            ", VIC_CUSTOMER_NATIONALITY="+VIC_CUSTOMER_NATIONALITY +
            ", VIC_DATA_SOURCE="+VIC_DATA_SOURCE +
            ", VIC_LINK_NAME="+VIC_LINK_NAME +
            ", VIC_LINK_PHONE="+VIC_LINK_PHONE +
            ", VIC_LINK_ADDRESS="+VIC_LINK_ADDRESS +
            ", VIC_CUSTOMER_ZIP_CODE="+VIC_CUSTOMER_ZIP_CODE +
            ", VIC_CUSTOMER_UPLOAD_FLAG="+VIC_CUSTOMER_UPLOAD_FLAG +"]";
    }
}
