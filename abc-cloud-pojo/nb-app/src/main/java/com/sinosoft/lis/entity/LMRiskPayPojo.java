/**
 * Copyright (c) 2017 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.entity;

import com.sinosoft.utility.Pojo;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;

import java.io.Serializable;

/**
 * <p>ClassName: LMRiskPayPojo </p>
 * <p>Copyright: Copyright (c) 2017</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2017-10-17
 */
public class LMRiskPayPojo implements Pojo,Serializable {
    // @Field
    /** 险种编码 */
    private String RiskCode; 
    /** 险种版本 */
    private String RiskVer; 
    /** 险种名称 */
    private String RiskName; 
    /** 催缴标记 */
    private String UrgePayFlag; 
    /** 手续费类型 */
    private String ChargeType; 
    /** 分解缴费间隔 */
    private String CutPayIntv; 
    /** 免交涉及加费 */
    private String PayAvoidType; 
    /** 手续费与保费关系 */
    private String ChargeAndPrem; 
    /** 结算日类型 */
    private String BalaDateType; 
    /** 免交标记 */
    private String PayAvoidFlag; 
    /** 缴费与复效关系 */
    private String PayAndRevEffe; 
    /** 缴费宽限期 */
    private int GracePeriod; 
    /** 宽限期单位 */
    private String GracePeriodUnit; 
    /** 宽限日期计算方式 */
    private String GraceDateCalMode; 


    public static final int FIELDNUM = 14;    // 数据库表的字段个数
    public String getRiskCode() {
        return RiskCode;
    }
    public void setRiskCode(String aRiskCode) {
        RiskCode = aRiskCode;
    }
    public String getRiskVer() {
        return RiskVer;
    }
    public void setRiskVer(String aRiskVer) {
        RiskVer = aRiskVer;
    }
    public String getRiskName() {
        return RiskName;
    }
    public void setRiskName(String aRiskName) {
        RiskName = aRiskName;
    }
    public String getUrgePayFlag() {
        return UrgePayFlag;
    }
    public void setUrgePayFlag(String aUrgePayFlag) {
        UrgePayFlag = aUrgePayFlag;
    }
    public String getChargeType() {
        return ChargeType;
    }
    public void setChargeType(String aChargeType) {
        ChargeType = aChargeType;
    }
    public String getCutPayIntv() {
        return CutPayIntv;
    }
    public void setCutPayIntv(String aCutPayIntv) {
        CutPayIntv = aCutPayIntv;
    }
    public String getPayAvoidType() {
        return PayAvoidType;
    }
    public void setPayAvoidType(String aPayAvoidType) {
        PayAvoidType = aPayAvoidType;
    }
    public String getChargeAndPrem() {
        return ChargeAndPrem;
    }
    public void setChargeAndPrem(String aChargeAndPrem) {
        ChargeAndPrem = aChargeAndPrem;
    }
    public String getBalaDateType() {
        return BalaDateType;
    }
    public void setBalaDateType(String aBalaDateType) {
        BalaDateType = aBalaDateType;
    }
    public String getPayAvoidFlag() {
        return PayAvoidFlag;
    }
    public void setPayAvoidFlag(String aPayAvoidFlag) {
        PayAvoidFlag = aPayAvoidFlag;
    }
    public String getPayAndRevEffe() {
        return PayAndRevEffe;
    }
    public void setPayAndRevEffe(String aPayAndRevEffe) {
        PayAndRevEffe = aPayAndRevEffe;
    }
    public int getGracePeriod() {
        return GracePeriod;
    }
    public void setGracePeriod(int aGracePeriod) {
        GracePeriod = aGracePeriod;
    }
    public void setGracePeriod(String aGracePeriod) {
        if (aGracePeriod != null && !aGracePeriod.equals("")) {
            Integer tInteger = new Integer(aGracePeriod);
            int i = tInteger.intValue();
            GracePeriod = i;
        }
    }

    public String getGracePeriodUnit() {
        return GracePeriodUnit;
    }
    public void setGracePeriodUnit(String aGracePeriodUnit) {
        GracePeriodUnit = aGracePeriodUnit;
    }
    public String getGraceDateCalMode() {
        return GraceDateCalMode;
    }
    public void setGraceDateCalMode(String aGraceDateCalMode) {
        GraceDateCalMode = aGraceDateCalMode;
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("RiskCode") ) {
            return 0;
        }
        if( strFieldName.equals("RiskVer") ) {
            return 1;
        }
        if( strFieldName.equals("RiskName") ) {
            return 2;
        }
        if( strFieldName.equals("UrgePayFlag") ) {
            return 3;
        }
        if( strFieldName.equals("ChargeType") ) {
            return 4;
        }
        if( strFieldName.equals("CutPayIntv") ) {
            return 5;
        }
        if( strFieldName.equals("PayAvoidType") ) {
            return 6;
        }
        if( strFieldName.equals("ChargeAndPrem") ) {
            return 7;
        }
        if( strFieldName.equals("BalaDateType") ) {
            return 8;
        }
        if( strFieldName.equals("PayAvoidFlag") ) {
            return 9;
        }
        if( strFieldName.equals("PayAndRevEffe") ) {
            return 10;
        }
        if( strFieldName.equals("GracePeriod") ) {
            return 11;
        }
        if( strFieldName.equals("GracePeriodUnit") ) {
            return 12;
        }
        if( strFieldName.equals("GraceDateCalMode") ) {
            return 13;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "RiskCode";
                break;
            case 1:
                strFieldName = "RiskVer";
                break;
            case 2:
                strFieldName = "RiskName";
                break;
            case 3:
                strFieldName = "UrgePayFlag";
                break;
            case 4:
                strFieldName = "ChargeType";
                break;
            case 5:
                strFieldName = "CutPayIntv";
                break;
            case 6:
                strFieldName = "PayAvoidType";
                break;
            case 7:
                strFieldName = "ChargeAndPrem";
                break;
            case 8:
                strFieldName = "BalaDateType";
                break;
            case 9:
                strFieldName = "PayAvoidFlag";
                break;
            case 10:
                strFieldName = "PayAndRevEffe";
                break;
            case 11:
                strFieldName = "GracePeriod";
                break;
            case 12:
                strFieldName = "GracePeriodUnit";
                break;
            case 13:
                strFieldName = "GraceDateCalMode";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "RISKCODE":
                return Schema.TYPE_STRING;
            case "RISKVER":
                return Schema.TYPE_STRING;
            case "RISKNAME":
                return Schema.TYPE_STRING;
            case "URGEPAYFLAG":
                return Schema.TYPE_STRING;
            case "CHARGETYPE":
                return Schema.TYPE_STRING;
            case "CUTPAYINTV":
                return Schema.TYPE_STRING;
            case "PAYAVOIDTYPE":
                return Schema.TYPE_STRING;
            case "CHARGEANDPREM":
                return Schema.TYPE_STRING;
            case "BALADATETYPE":
                return Schema.TYPE_STRING;
            case "PAYAVOIDFLAG":
                return Schema.TYPE_STRING;
            case "PAYANDREVEFFE":
                return Schema.TYPE_STRING;
            case "GRACEPERIOD":
                return Schema.TYPE_INT;
            case "GRACEPERIODUNIT":
                return Schema.TYPE_STRING;
            case "GRACEDATECALMODE":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_STRING;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_STRING;
            case 6:
                return Schema.TYPE_STRING;
            case 7:
                return Schema.TYPE_STRING;
            case 8:
                return Schema.TYPE_STRING;
            case 9:
                return Schema.TYPE_STRING;
            case 10:
                return Schema.TYPE_STRING;
            case 11:
                return Schema.TYPE_INT;
            case 12:
                return Schema.TYPE_STRING;
            case 13:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("RiskCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RiskCode));
        }
        if (FCode.equalsIgnoreCase("RiskVer")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RiskVer));
        }
        if (FCode.equalsIgnoreCase("RiskName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RiskName));
        }
        if (FCode.equalsIgnoreCase("UrgePayFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(UrgePayFlag));
        }
        if (FCode.equalsIgnoreCase("ChargeType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ChargeType));
        }
        if (FCode.equalsIgnoreCase("CutPayIntv")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CutPayIntv));
        }
        if (FCode.equalsIgnoreCase("PayAvoidType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PayAvoidType));
        }
        if (FCode.equalsIgnoreCase("ChargeAndPrem")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ChargeAndPrem));
        }
        if (FCode.equalsIgnoreCase("BalaDateType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BalaDateType));
        }
        if (FCode.equalsIgnoreCase("PayAvoidFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PayAvoidFlag));
        }
        if (FCode.equalsIgnoreCase("PayAndRevEffe")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PayAndRevEffe));
        }
        if (FCode.equalsIgnoreCase("GracePeriod")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GracePeriod));
        }
        if (FCode.equalsIgnoreCase("GracePeriodUnit")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GracePeriodUnit));
        }
        if (FCode.equalsIgnoreCase("GraceDateCalMode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GraceDateCalMode));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = String.valueOf(RiskCode);
                break;
            case 1:
                strFieldValue = String.valueOf(RiskVer);
                break;
            case 2:
                strFieldValue = String.valueOf(RiskName);
                break;
            case 3:
                strFieldValue = String.valueOf(UrgePayFlag);
                break;
            case 4:
                strFieldValue = String.valueOf(ChargeType);
                break;
            case 5:
                strFieldValue = String.valueOf(CutPayIntv);
                break;
            case 6:
                strFieldValue = String.valueOf(PayAvoidType);
                break;
            case 7:
                strFieldValue = String.valueOf(ChargeAndPrem);
                break;
            case 8:
                strFieldValue = String.valueOf(BalaDateType);
                break;
            case 9:
                strFieldValue = String.valueOf(PayAvoidFlag);
                break;
            case 10:
                strFieldValue = String.valueOf(PayAndRevEffe);
                break;
            case 11:
                strFieldValue = String.valueOf(GracePeriod);
                break;
            case 12:
                strFieldValue = String.valueOf(GracePeriodUnit);
                break;
            case 13:
                strFieldValue = String.valueOf(GraceDateCalMode);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("RiskCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                RiskCode = FValue.trim();
            }
            else
                RiskCode = null;
        }
        if (FCode.equalsIgnoreCase("RiskVer")) {
            if( FValue != null && !FValue.equals(""))
            {
                RiskVer = FValue.trim();
            }
            else
                RiskVer = null;
        }
        if (FCode.equalsIgnoreCase("RiskName")) {
            if( FValue != null && !FValue.equals(""))
            {
                RiskName = FValue.trim();
            }
            else
                RiskName = null;
        }
        if (FCode.equalsIgnoreCase("UrgePayFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                UrgePayFlag = FValue.trim();
            }
            else
                UrgePayFlag = null;
        }
        if (FCode.equalsIgnoreCase("ChargeType")) {
            if( FValue != null && !FValue.equals(""))
            {
                ChargeType = FValue.trim();
            }
            else
                ChargeType = null;
        }
        if (FCode.equalsIgnoreCase("CutPayIntv")) {
            if( FValue != null && !FValue.equals(""))
            {
                CutPayIntv = FValue.trim();
            }
            else
                CutPayIntv = null;
        }
        if (FCode.equalsIgnoreCase("PayAvoidType")) {
            if( FValue != null && !FValue.equals(""))
            {
                PayAvoidType = FValue.trim();
            }
            else
                PayAvoidType = null;
        }
        if (FCode.equalsIgnoreCase("ChargeAndPrem")) {
            if( FValue != null && !FValue.equals(""))
            {
                ChargeAndPrem = FValue.trim();
            }
            else
                ChargeAndPrem = null;
        }
        if (FCode.equalsIgnoreCase("BalaDateType")) {
            if( FValue != null && !FValue.equals(""))
            {
                BalaDateType = FValue.trim();
            }
            else
                BalaDateType = null;
        }
        if (FCode.equalsIgnoreCase("PayAvoidFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                PayAvoidFlag = FValue.trim();
            }
            else
                PayAvoidFlag = null;
        }
        if (FCode.equalsIgnoreCase("PayAndRevEffe")) {
            if( FValue != null && !FValue.equals(""))
            {
                PayAndRevEffe = FValue.trim();
            }
            else
                PayAndRevEffe = null;
        }
        if (FCode.equalsIgnoreCase("GracePeriod")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                GracePeriod = i;
            }
        }
        if (FCode.equalsIgnoreCase("GracePeriodUnit")) {
            if( FValue != null && !FValue.equals(""))
            {
                GracePeriodUnit = FValue.trim();
            }
            else
                GracePeriodUnit = null;
        }
        if (FCode.equalsIgnoreCase("GraceDateCalMode")) {
            if( FValue != null && !FValue.equals(""))
            {
                GraceDateCalMode = FValue.trim();
            }
            else
                GraceDateCalMode = null;
        }
        return true;
    }


    public String toString() {
    return "LMRiskPayPojo [" +
            "RiskCode="+RiskCode +
            ", RiskVer="+RiskVer +
            ", RiskName="+RiskName +
            ", UrgePayFlag="+UrgePayFlag +
            ", ChargeType="+ChargeType +
            ", CutPayIntv="+CutPayIntv +
            ", PayAvoidType="+PayAvoidType +
            ", ChargeAndPrem="+ChargeAndPrem +
            ", BalaDateType="+BalaDateType +
            ", PayAvoidFlag="+PayAvoidFlag +
            ", PayAndRevEffe="+PayAndRevEffe +
            ", GracePeriod="+GracePeriod +
            ", GracePeriodUnit="+GracePeriodUnit +
            ", GraceDateCalMode="+GraceDateCalMode +"]";
    }
}
