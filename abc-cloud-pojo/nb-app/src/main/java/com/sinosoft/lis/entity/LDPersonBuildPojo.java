/**
 * Copyright (c) 2017 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.entity;

import com.sinosoft.cloud.cache.annotation.RedisPrimaryHKey;
import com.sinosoft.utility.Pojo;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;

import java.io.Serializable;

/**
 * <p>ClassName: LDPersonBuildPojo </p>
 * <p>Copyright: Copyright (c) 2017</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2017-11-23
 */
public class LDPersonBuildPojo implements Pojo,Serializable {
    // @Field
    /** 最小年龄 */
    @RedisPrimaryHKey
    private int MinAge; 
    /** 最大年龄 */
    @RedisPrimaryHKey
    private int MaxAge; 
    /** 年龄标志 */
    @RedisPrimaryHKey
    private String AgeFlag; 
    /** 性别 */
    @RedisPrimaryHKey
    private String Sex; 
    /** 最小身高 */
    private double MinStature; 
    /** 最大身高 */
    private double MaxStature; 
    /** 最小体重 */
    private double MinAvoirdupois; 
    /** 最大体重 */
    private double MaxAvoirdupois; 
    /** 最小bmi */
    private double MinBMI; 
    /** 最大bmi */
    private double MaxBMI; 


    public static final int FIELDNUM = 10;    // 数据库表的字段个数
    public int getMinAge() {
        return MinAge;
    }
    public void setMinAge(int aMinAge) {
        MinAge = aMinAge;
    }
    public void setMinAge(String aMinAge) {
        if (aMinAge != null && !aMinAge.equals("")) {
            Integer tInteger = new Integer(aMinAge);
            int i = tInteger.intValue();
            MinAge = i;
        }
    }

    public int getMaxAge() {
        return MaxAge;
    }
    public void setMaxAge(int aMaxAge) {
        MaxAge = aMaxAge;
    }
    public void setMaxAge(String aMaxAge) {
        if (aMaxAge != null && !aMaxAge.equals("")) {
            Integer tInteger = new Integer(aMaxAge);
            int i = tInteger.intValue();
            MaxAge = i;
        }
    }

    public String getAgeFlag() {
        return AgeFlag;
    }
    public void setAgeFlag(String aAgeFlag) {
        AgeFlag = aAgeFlag;
    }
    public String getSex() {
        return Sex;
    }
    public void setSex(String aSex) {
        Sex = aSex;
    }
    public double getMinStature() {
        return MinStature;
    }
    public void setMinStature(double aMinStature) {
        MinStature = aMinStature;
    }
    public void setMinStature(String aMinStature) {
        if (aMinStature != null && !aMinStature.equals("")) {
            Double tDouble = new Double(aMinStature);
            double d = tDouble.doubleValue();
            MinStature = d;
        }
    }

    public double getMaxStature() {
        return MaxStature;
    }
    public void setMaxStature(double aMaxStature) {
        MaxStature = aMaxStature;
    }
    public void setMaxStature(String aMaxStature) {
        if (aMaxStature != null && !aMaxStature.equals("")) {
            Double tDouble = new Double(aMaxStature);
            double d = tDouble.doubleValue();
            MaxStature = d;
        }
    }

    public double getMinAvoirdupois() {
        return MinAvoirdupois;
    }
    public void setMinAvoirdupois(double aMinAvoirdupois) {
        MinAvoirdupois = aMinAvoirdupois;
    }
    public void setMinAvoirdupois(String aMinAvoirdupois) {
        if (aMinAvoirdupois != null && !aMinAvoirdupois.equals("")) {
            Double tDouble = new Double(aMinAvoirdupois);
            double d = tDouble.doubleValue();
            MinAvoirdupois = d;
        }
    }

    public double getMaxAvoirdupois() {
        return MaxAvoirdupois;
    }
    public void setMaxAvoirdupois(double aMaxAvoirdupois) {
        MaxAvoirdupois = aMaxAvoirdupois;
    }
    public void setMaxAvoirdupois(String aMaxAvoirdupois) {
        if (aMaxAvoirdupois != null && !aMaxAvoirdupois.equals("")) {
            Double tDouble = new Double(aMaxAvoirdupois);
            double d = tDouble.doubleValue();
            MaxAvoirdupois = d;
        }
    }

    public double getMinBMI() {
        return MinBMI;
    }
    public void setMinBMI(double aMinBMI) {
        MinBMI = aMinBMI;
    }
    public void setMinBMI(String aMinBMI) {
        if (aMinBMI != null && !aMinBMI.equals("")) {
            Double tDouble = new Double(aMinBMI);
            double d = tDouble.doubleValue();
            MinBMI = d;
        }
    }

    public double getMaxBMI() {
        return MaxBMI;
    }
    public void setMaxBMI(double aMaxBMI) {
        MaxBMI = aMaxBMI;
    }
    public void setMaxBMI(String aMaxBMI) {
        if (aMaxBMI != null && !aMaxBMI.equals("")) {
            Double tDouble = new Double(aMaxBMI);
            double d = tDouble.doubleValue();
            MaxBMI = d;
        }
    }


    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("MinAge") ) {
            return 0;
        }
        if( strFieldName.equals("MaxAge") ) {
            return 1;
        }
        if( strFieldName.equals("AgeFlag") ) {
            return 2;
        }
        if( strFieldName.equals("Sex") ) {
            return 3;
        }
        if( strFieldName.equals("MinStature") ) {
            return 4;
        }
        if( strFieldName.equals("MaxStature") ) {
            return 5;
        }
        if( strFieldName.equals("MinAvoirdupois") ) {
            return 6;
        }
        if( strFieldName.equals("MaxAvoirdupois") ) {
            return 7;
        }
        if( strFieldName.equals("MinBMI") ) {
            return 8;
        }
        if( strFieldName.equals("MaxBMI") ) {
            return 9;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "MinAge";
                break;
            case 1:
                strFieldName = "MaxAge";
                break;
            case 2:
                strFieldName = "AgeFlag";
                break;
            case 3:
                strFieldName = "Sex";
                break;
            case 4:
                strFieldName = "MinStature";
                break;
            case 5:
                strFieldName = "MaxStature";
                break;
            case 6:
                strFieldName = "MinAvoirdupois";
                break;
            case 7:
                strFieldName = "MaxAvoirdupois";
                break;
            case 8:
                strFieldName = "MinBMI";
                break;
            case 9:
                strFieldName = "MaxBMI";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "MINAGE":
                return Schema.TYPE_INT;
            case "MAXAGE":
                return Schema.TYPE_INT;
            case "AGEFLAG":
                return Schema.TYPE_STRING;
            case "SEX":
                return Schema.TYPE_STRING;
            case "MINSTATURE":
                return Schema.TYPE_DOUBLE;
            case "MAXSTATURE":
                return Schema.TYPE_DOUBLE;
            case "MINAVOIRDUPOIS":
                return Schema.TYPE_DOUBLE;
            case "MAXAVOIRDUPOIS":
                return Schema.TYPE_DOUBLE;
            case "MINBMI":
                return Schema.TYPE_DOUBLE;
            case "MAXBMI":
                return Schema.TYPE_DOUBLE;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_INT;
            case 1:
                return Schema.TYPE_INT;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_DOUBLE;
            case 5:
                return Schema.TYPE_DOUBLE;
            case 6:
                return Schema.TYPE_DOUBLE;
            case 7:
                return Schema.TYPE_DOUBLE;
            case 8:
                return Schema.TYPE_DOUBLE;
            case 9:
                return Schema.TYPE_DOUBLE;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("MinAge")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MinAge));
        }
        if (FCode.equalsIgnoreCase("MaxAge")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MaxAge));
        }
        if (FCode.equalsIgnoreCase("AgeFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgeFlag));
        }
        if (FCode.equalsIgnoreCase("Sex")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Sex));
        }
        if (FCode.equalsIgnoreCase("MinStature")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MinStature));
        }
        if (FCode.equalsIgnoreCase("MaxStature")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MaxStature));
        }
        if (FCode.equalsIgnoreCase("MinAvoirdupois")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MinAvoirdupois));
        }
        if (FCode.equalsIgnoreCase("MaxAvoirdupois")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MaxAvoirdupois));
        }
        if (FCode.equalsIgnoreCase("MinBMI")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MinBMI));
        }
        if (FCode.equalsIgnoreCase("MaxBMI")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MaxBMI));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = String.valueOf(MinAge);
                break;
            case 1:
                strFieldValue = String.valueOf(MaxAge);
                break;
            case 2:
                strFieldValue = String.valueOf(AgeFlag);
                break;
            case 3:
                strFieldValue = String.valueOf(Sex);
                break;
            case 4:
                strFieldValue = String.valueOf(MinStature);
                break;
            case 5:
                strFieldValue = String.valueOf(MaxStature);
                break;
            case 6:
                strFieldValue = String.valueOf(MinAvoirdupois);
                break;
            case 7:
                strFieldValue = String.valueOf(MaxAvoirdupois);
                break;
            case 8:
                strFieldValue = String.valueOf(MinBMI);
                break;
            case 9:
                strFieldValue = String.valueOf(MaxBMI);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("MinAge")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                MinAge = i;
            }
        }
        if (FCode.equalsIgnoreCase("MaxAge")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                MaxAge = i;
            }
        }
        if (FCode.equalsIgnoreCase("AgeFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                AgeFlag = FValue.trim();
            }
            else
                AgeFlag = null;
        }
        if (FCode.equalsIgnoreCase("Sex")) {
            if( FValue != null && !FValue.equals(""))
            {
                Sex = FValue.trim();
            }
            else
                Sex = null;
        }
        if (FCode.equalsIgnoreCase("MinStature")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                MinStature = d;
            }
        }
        if (FCode.equalsIgnoreCase("MaxStature")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                MaxStature = d;
            }
        }
        if (FCode.equalsIgnoreCase("MinAvoirdupois")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                MinAvoirdupois = d;
            }
        }
        if (FCode.equalsIgnoreCase("MaxAvoirdupois")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                MaxAvoirdupois = d;
            }
        }
        if (FCode.equalsIgnoreCase("MinBMI")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                MinBMI = d;
            }
        }
        if (FCode.equalsIgnoreCase("MaxBMI")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                MaxBMI = d;
            }
        }
        return true;
    }


    public String toString() {
    return "LDPersonBuildPojo [" +
            "MinAge="+MinAge +
            ", MaxAge="+MaxAge +
            ", AgeFlag="+AgeFlag +
            ", Sex="+Sex +
            ", MinStature="+MinStature +
            ", MaxStature="+MaxStature +
            ", MinAvoirdupois="+MinAvoirdupois +
            ", MaxAvoirdupois="+MaxAvoirdupois +
            ", MinBMI="+MinBMI +
            ", MaxBMI="+MaxBMI +"]";
    }
}
