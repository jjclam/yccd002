/**
 * Copyright (c) 2019 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.entity;

import java.io.*;
import java.util.Date;

import com.sinosoft.cloud.cache.annotation.RedisPrimaryHKey;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.StrTool;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * <p>ClassName: LMRiskAppPojo </p>
 * <p>Copyright: Copyright (c) 2019</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2019-10-16
 */
public class LMRiskAppPojo implements  Pojo,Serializable {

    // @Field
    /** 险种编码 */
    @RedisPrimaryHKey
    private String RiskCode; 
    /** 险种版本 */
    private String RiskVer; 
    /** 险种名称 */
    private String RiskName; 
    /** 险类编码 */
    private String KindCode; 
    /** 险种分类 */
    private String RiskType; 
    /** 险种分类1 */
    private String RiskType1; 
    /** 险种分类2 */
    private String RiskType2; 
    /** 险种性质 */
    private String RiskProp; 
    /** 险种类别 */
    private String RiskPeriod; 
    /** 险种细分 */
    private String RiskTypeDetail; 
    /** 险种标记 */
    private String RiskFlag; 
    /** 保单类型 */
    private String PolType; 
    /** 投资标记 */
    private String InvestFlag; 
    /** 分红标记 */
    private String BonusFlag; 
    /** 红利领取方式 */
    private String BonusMode; 
    /** 有无名单标记 */
    private String ListFlag; 
    /** 主附险标记 */
    private String SubRiskFlag; 
    /** 计算精确位 */
    private int CalDigital; 
    /** 计算取舍方法 */
    private String CalChoMode; 
    /** 风险保额倍数 */
    private int RiskAmntMult; 
    /** 保险期限标志 */
    private String InsuPeriodFlag; 
    /** 保险期间上限 */
    private int MaxEndPeriod; 
    /** 满期截至年龄 */
    private int AgeLmt; 
    /** 签单日算法 */
    private int SignDateCalMode; 
    /** 协议险标记 */
    private String ProtocolFlag; 
    /** 协议险给付金改变标记 */
    private String GetChgFlag; 
    /** 协议缴费标记 */
    private String ProtocolPayFlag; 
    /** 保障计划标记 */
    private String EnsuPlanFlag; 
    /** 保障计划调整标记 */
    private String EnsuPlanAdjFlag; 
    /** 开办日期 */
    private String  StartDate;
    /** 停办日期 */
    private String  EndDate;
    /** 最小投保人年龄 */
    private int MinAppntAge; 
    /** 最大投保人年龄 */
    private int MaxAppntAge; 
    /** 最大被保人年龄 */
    private int MaxInsuredAge; 
    /** 最小被保人年龄 */
    private int MinInsuredAge; 
    /** 投保使用利率 */
    private double AppInterest; 
    /** 投保使用费率 */
    private double AppPremRate; 
    /** 多被保人标记 */
    private String InsuredFlag; 
    /** 共保标记 */
    private String ShareFlag; 
    /** 受益人标记 */
    private String BnfFlag; 
    /** 暂缴费标记 */
    private String TempPayFlag; 
    /** 录入缴费项标记 */
    private String InpPayPlan; 
    /** 告知标记 */
    private String ImpartFlag; 
    /** 保险经历标记 */
    private String InsuExpeFlag; 
    /** 提供借款标记 */
    private String LoanFlag; 
    /** 抵押标记 */
    private String MortagageFlag; 
    /** 备注 */
    private String IDifReturnFlag; 
    /** 减额缴清标记 */
    private String CutAmntStopPay; 
    /** 分保率 */
    private double RinsRate; 
    /** 销售标记 */
    private String SaleFlag; 
    /** 磁盘文件投保标记 */
    private String FileAppFlag; 
    /** 管理部门 */
    private String MngCom; 
    /** 自动垫缴标志 */
    private String AutoPayFlag; 
    /** 是否打印医院列表标记 */
    private String NeedPrintHospital; 
    /** 是否打印伤残给付表标记 */
    private String NeedPrintGet; 
    /** 险种分类3 */
    private String RiskType3; 
    /** 险种分类4 */
    private String RiskType4; 
    /** 险种分类5 */
    private String RiskType5; 
    /** 签单后不需要打印 */
    private String NotPrintPol; 
    /** 录单时是否需要设置保单送达日期 */
    private String NeedGetPolDate; 
    /** 是否从暂缴费表中读取银行的账号和户名 */
    private String NeedReReadBank; 
    /** 特殊险种标记 */
    private String SpecFlag; 
    /** 利差返还标记 */
    private String InterestDifFlag; 
    /** 财务险种分类 */
    private String RiskTypeAcc; 
    /** 财务险种类别 */
    private String RiskPeriodAcc; 
    /** 险种分类7 */
    private String RiskType7; 
    /** 险种分类6 */
    private String RiskType6; 
    /** 健康委托产品分类 */
    private String HealthType; 
    /** 取消提前给付特约标志 */
    private String CANCLEFOREGETSPECFLAG; 
    /** 险种分类8 */
    private String RiskType8; 
    /** 签单日算法2 */
    private int SIGNDATECALMODE2; 
    /** 签单日算法3 */
    private int SIGNDATECALMODE3; 
    /** 险种分类9 */
    private String RISKSTYLE; 


    public static final int FIELDNUM = 73;    // 数据库表的字段个数

    private FDate fDate = new FDate();        // 处理日期
    public String getRiskCode() {
        return RiskCode;
    }
    public void setRiskCode(String aRiskCode) {
        RiskCode = aRiskCode;
    }
    public String getRiskVer() {
        return RiskVer;
    }
    public void setRiskVer(String aRiskVer) {
        RiskVer = aRiskVer;
    }
    public String getRiskName() {
        return RiskName;
    }
    public void setRiskName(String aRiskName) {
        RiskName = aRiskName;
    }
    public String getKindCode() {
        return KindCode;
    }
    public void setKindCode(String aKindCode) {
        KindCode = aKindCode;
    }
    public String getRiskType() {
        return RiskType;
    }
    public void setRiskType(String aRiskType) {
        RiskType = aRiskType;
    }
    public String getRiskType1() {
        return RiskType1;
    }
    public void setRiskType1(String aRiskType1) {
        RiskType1 = aRiskType1;
    }
    public String getRiskType2() {
        return RiskType2;
    }
    public void setRiskType2(String aRiskType2) {
        RiskType2 = aRiskType2;
    }
    public String getRiskProp() {
        return RiskProp;
    }
    public void setRiskProp(String aRiskProp) {
        RiskProp = aRiskProp;
    }
    public String getRiskPeriod() {
        return RiskPeriod;
    }
    public void setRiskPeriod(String aRiskPeriod) {
        RiskPeriod = aRiskPeriod;
    }
    public String getRiskTypeDetail() {
        return RiskTypeDetail;
    }
    public void setRiskTypeDetail(String aRiskTypeDetail) {
        RiskTypeDetail = aRiskTypeDetail;
    }
    public String getRiskFlag() {
        return RiskFlag;
    }
    public void setRiskFlag(String aRiskFlag) {
        RiskFlag = aRiskFlag;
    }
    public String getPolType() {
        return PolType;
    }
    public void setPolType(String aPolType) {
        PolType = aPolType;
    }
    public String getInvestFlag() {
        return InvestFlag;
    }
    public void setInvestFlag(String aInvestFlag) {
        InvestFlag = aInvestFlag;
    }
    public String getBonusFlag() {
        return BonusFlag;
    }
    public void setBonusFlag(String aBonusFlag) {
        BonusFlag = aBonusFlag;
    }
    public String getBonusMode() {
        return BonusMode;
    }
    public void setBonusMode(String aBonusMode) {
        BonusMode = aBonusMode;
    }
    public String getListFlag() {
        return ListFlag;
    }
    public void setListFlag(String aListFlag) {
        ListFlag = aListFlag;
    }
    public String getSubRiskFlag() {
        return SubRiskFlag;
    }
    public void setSubRiskFlag(String aSubRiskFlag) {
        SubRiskFlag = aSubRiskFlag;
    }
    public int getCalDigital() {
        return CalDigital;
    }
    public void setCalDigital(int aCalDigital) {
        CalDigital = aCalDigital;
    }
    public void setCalDigital(String aCalDigital) {
        if (aCalDigital != null && !aCalDigital.equals("")) {
            Integer tInteger = new Integer(aCalDigital);
            int i = tInteger.intValue();
            CalDigital = i;
        }
    }

    public String getCalChoMode() {
        return CalChoMode;
    }
    public void setCalChoMode(String aCalChoMode) {
        CalChoMode = aCalChoMode;
    }
    public int getRiskAmntMult() {
        return RiskAmntMult;
    }
    public void setRiskAmntMult(int aRiskAmntMult) {
        RiskAmntMult = aRiskAmntMult;
    }
    public void setRiskAmntMult(String aRiskAmntMult) {
        if (aRiskAmntMult != null && !aRiskAmntMult.equals("")) {
            Integer tInteger = new Integer(aRiskAmntMult);
            int i = tInteger.intValue();
            RiskAmntMult = i;
        }
    }

    public String getInsuPeriodFlag() {
        return InsuPeriodFlag;
    }
    public void setInsuPeriodFlag(String aInsuPeriodFlag) {
        InsuPeriodFlag = aInsuPeriodFlag;
    }
    public int getMaxEndPeriod() {
        return MaxEndPeriod;
    }
    public void setMaxEndPeriod(int aMaxEndPeriod) {
        MaxEndPeriod = aMaxEndPeriod;
    }
    public void setMaxEndPeriod(String aMaxEndPeriod) {
        if (aMaxEndPeriod != null && !aMaxEndPeriod.equals("")) {
            Integer tInteger = new Integer(aMaxEndPeriod);
            int i = tInteger.intValue();
            MaxEndPeriod = i;
        }
    }

    public int getAgeLmt() {
        return AgeLmt;
    }
    public void setAgeLmt(int aAgeLmt) {
        AgeLmt = aAgeLmt;
    }
    public void setAgeLmt(String aAgeLmt) {
        if (aAgeLmt != null && !aAgeLmt.equals("")) {
            Integer tInteger = new Integer(aAgeLmt);
            int i = tInteger.intValue();
            AgeLmt = i;
        }
    }

    public int getSignDateCalMode() {
        return SignDateCalMode;
    }
    public void setSignDateCalMode(int aSignDateCalMode) {
        SignDateCalMode = aSignDateCalMode;
    }
    public void setSignDateCalMode(String aSignDateCalMode) {
        if (aSignDateCalMode != null && !aSignDateCalMode.equals("")) {
            Integer tInteger = new Integer(aSignDateCalMode);
            int i = tInteger.intValue();
            SignDateCalMode = i;
        }
    }

    public String getProtocolFlag() {
        return ProtocolFlag;
    }
    public void setProtocolFlag(String aProtocolFlag) {
        ProtocolFlag = aProtocolFlag;
    }
    public String getGetChgFlag() {
        return GetChgFlag;
    }
    public void setGetChgFlag(String aGetChgFlag) {
        GetChgFlag = aGetChgFlag;
    }
    public String getProtocolPayFlag() {
        return ProtocolPayFlag;
    }
    public void setProtocolPayFlag(String aProtocolPayFlag) {
        ProtocolPayFlag = aProtocolPayFlag;
    }
    public String getEnsuPlanFlag() {
        return EnsuPlanFlag;
    }
    public void setEnsuPlanFlag(String aEnsuPlanFlag) {
        EnsuPlanFlag = aEnsuPlanFlag;
    }
    public String getEnsuPlanAdjFlag() {
        return EnsuPlanAdjFlag;
    }
    public void setEnsuPlanAdjFlag(String aEnsuPlanAdjFlag) {
        EnsuPlanAdjFlag = aEnsuPlanAdjFlag;
    }
    public String getStartDate() {
        return StartDate;
    }
    public void setStartDate(String aStartDate) {
        StartDate = aStartDate;
    }
    public String getEndDate() {
        return EndDate;
    }
    public void setEndDate(String aEndDate) {
        EndDate = aEndDate;
    }
    public int getMinAppntAge() {
        return MinAppntAge;
    }
    public void setMinAppntAge(int aMinAppntAge) {
        MinAppntAge = aMinAppntAge;
    }
    public void setMinAppntAge(String aMinAppntAge) {
        if (aMinAppntAge != null && !aMinAppntAge.equals("")) {
            Integer tInteger = new Integer(aMinAppntAge);
            int i = tInteger.intValue();
            MinAppntAge = i;
        }
    }

    public int getMaxAppntAge() {
        return MaxAppntAge;
    }
    public void setMaxAppntAge(int aMaxAppntAge) {
        MaxAppntAge = aMaxAppntAge;
    }
    public void setMaxAppntAge(String aMaxAppntAge) {
        if (aMaxAppntAge != null && !aMaxAppntAge.equals("")) {
            Integer tInteger = new Integer(aMaxAppntAge);
            int i = tInteger.intValue();
            MaxAppntAge = i;
        }
    }

    public int getMaxInsuredAge() {
        return MaxInsuredAge;
    }
    public void setMaxInsuredAge(int aMaxInsuredAge) {
        MaxInsuredAge = aMaxInsuredAge;
    }
    public void setMaxInsuredAge(String aMaxInsuredAge) {
        if (aMaxInsuredAge != null && !aMaxInsuredAge.equals("")) {
            Integer tInteger = new Integer(aMaxInsuredAge);
            int i = tInteger.intValue();
            MaxInsuredAge = i;
        }
    }

    public int getMinInsuredAge() {
        return MinInsuredAge;
    }
    public void setMinInsuredAge(int aMinInsuredAge) {
        MinInsuredAge = aMinInsuredAge;
    }
    public void setMinInsuredAge(String aMinInsuredAge) {
        if (aMinInsuredAge != null && !aMinInsuredAge.equals("")) {
            Integer tInteger = new Integer(aMinInsuredAge);
            int i = tInteger.intValue();
            MinInsuredAge = i;
        }
    }

    public double getAppInterest() {
        return AppInterest;
    }
    public void setAppInterest(double aAppInterest) {
        AppInterest = aAppInterest;
    }
    public void setAppInterest(String aAppInterest) {
        if (aAppInterest != null && !aAppInterest.equals("")) {
            Double tDouble = new Double(aAppInterest);
            double d = tDouble.doubleValue();
            AppInterest = d;
        }
    }

    public double getAppPremRate() {
        return AppPremRate;
    }
    public void setAppPremRate(double aAppPremRate) {
        AppPremRate = aAppPremRate;
    }
    public void setAppPremRate(String aAppPremRate) {
        if (aAppPremRate != null && !aAppPremRate.equals("")) {
            Double tDouble = new Double(aAppPremRate);
            double d = tDouble.doubleValue();
            AppPremRate = d;
        }
    }

    public String getInsuredFlag() {
        return InsuredFlag;
    }
    public void setInsuredFlag(String aInsuredFlag) {
        InsuredFlag = aInsuredFlag;
    }
    public String getShareFlag() {
        return ShareFlag;
    }
    public void setShareFlag(String aShareFlag) {
        ShareFlag = aShareFlag;
    }
    public String getBnfFlag() {
        return BnfFlag;
    }
    public void setBnfFlag(String aBnfFlag) {
        BnfFlag = aBnfFlag;
    }
    public String getTempPayFlag() {
        return TempPayFlag;
    }
    public void setTempPayFlag(String aTempPayFlag) {
        TempPayFlag = aTempPayFlag;
    }
    public String getInpPayPlan() {
        return InpPayPlan;
    }
    public void setInpPayPlan(String aInpPayPlan) {
        InpPayPlan = aInpPayPlan;
    }
    public String getImpartFlag() {
        return ImpartFlag;
    }
    public void setImpartFlag(String aImpartFlag) {
        ImpartFlag = aImpartFlag;
    }
    public String getInsuExpeFlag() {
        return InsuExpeFlag;
    }
    public void setInsuExpeFlag(String aInsuExpeFlag) {
        InsuExpeFlag = aInsuExpeFlag;
    }
    public String getLoanFlag() {
        return LoanFlag;
    }
    public void setLoanFlag(String aLoanFlag) {
        LoanFlag = aLoanFlag;
    }
    public String getMortagageFlag() {
        return MortagageFlag;
    }
    public void setMortagageFlag(String aMortagageFlag) {
        MortagageFlag = aMortagageFlag;
    }
    public String getIDifReturnFlag() {
        return IDifReturnFlag;
    }
    public void setIDifReturnFlag(String aIDifReturnFlag) {
        IDifReturnFlag = aIDifReturnFlag;
    }
    public String getCutAmntStopPay() {
        return CutAmntStopPay;
    }
    public void setCutAmntStopPay(String aCutAmntStopPay) {
        CutAmntStopPay = aCutAmntStopPay;
    }
    public double getRinsRate() {
        return RinsRate;
    }
    public void setRinsRate(double aRinsRate) {
        RinsRate = aRinsRate;
    }
    public void setRinsRate(String aRinsRate) {
        if (aRinsRate != null && !aRinsRate.equals("")) {
            Double tDouble = new Double(aRinsRate);
            double d = tDouble.doubleValue();
            RinsRate = d;
        }
    }

    public String getSaleFlag() {
        return SaleFlag;
    }
    public void setSaleFlag(String aSaleFlag) {
        SaleFlag = aSaleFlag;
    }
    public String getFileAppFlag() {
        return FileAppFlag;
    }
    public void setFileAppFlag(String aFileAppFlag) {
        FileAppFlag = aFileAppFlag;
    }
    public String getMngCom() {
        return MngCom;
    }
    public void setMngCom(String aMngCom) {
        MngCom = aMngCom;
    }
    public String getAutoPayFlag() {
        return AutoPayFlag;
    }
    public void setAutoPayFlag(String aAutoPayFlag) {
        AutoPayFlag = aAutoPayFlag;
    }
    public String getNeedPrintHospital() {
        return NeedPrintHospital;
    }
    public void setNeedPrintHospital(String aNeedPrintHospital) {
        NeedPrintHospital = aNeedPrintHospital;
    }
    public String getNeedPrintGet() {
        return NeedPrintGet;
    }
    public void setNeedPrintGet(String aNeedPrintGet) {
        NeedPrintGet = aNeedPrintGet;
    }
    public String getRiskType3() {
        return RiskType3;
    }
    public void setRiskType3(String aRiskType3) {
        RiskType3 = aRiskType3;
    }
    public String getRiskType4() {
        return RiskType4;
    }
    public void setRiskType4(String aRiskType4) {
        RiskType4 = aRiskType4;
    }
    public String getRiskType5() {
        return RiskType5;
    }
    public void setRiskType5(String aRiskType5) {
        RiskType5 = aRiskType5;
    }
    public String getNotPrintPol() {
        return NotPrintPol;
    }
    public void setNotPrintPol(String aNotPrintPol) {
        NotPrintPol = aNotPrintPol;
    }
    public String getNeedGetPolDate() {
        return NeedGetPolDate;
    }
    public void setNeedGetPolDate(String aNeedGetPolDate) {
        NeedGetPolDate = aNeedGetPolDate;
    }
    public String getNeedReReadBank() {
        return NeedReReadBank;
    }
    public void setNeedReReadBank(String aNeedReReadBank) {
        NeedReReadBank = aNeedReReadBank;
    }
    public String getSpecFlag() {
        return SpecFlag;
    }
    public void setSpecFlag(String aSpecFlag) {
        SpecFlag = aSpecFlag;
    }
    public String getInterestDifFlag() {
        return InterestDifFlag;
    }
    public void setInterestDifFlag(String aInterestDifFlag) {
        InterestDifFlag = aInterestDifFlag;
    }
    public String getRiskTypeAcc() {
        return RiskTypeAcc;
    }
    public void setRiskTypeAcc(String aRiskTypeAcc) {
        RiskTypeAcc = aRiskTypeAcc;
    }
    public String getRiskPeriodAcc() {
        return RiskPeriodAcc;
    }
    public void setRiskPeriodAcc(String aRiskPeriodAcc) {
        RiskPeriodAcc = aRiskPeriodAcc;
    }
    public String getRiskType7() {
        return RiskType7;
    }
    public void setRiskType7(String aRiskType7) {
        RiskType7 = aRiskType7;
    }
    public String getRiskType6() {
        return RiskType6;
    }
    public void setRiskType6(String aRiskType6) {
        RiskType6 = aRiskType6;
    }
    public String getHealthType() {
        return HealthType;
    }
    public void setHealthType(String aHealthType) {
        HealthType = aHealthType;
    }
    public String getCANCLEFOREGETSPECFLAG() {
        return CANCLEFOREGETSPECFLAG;
    }
    public void setCANCLEFOREGETSPECFLAG(String aCANCLEFOREGETSPECFLAG) {
        CANCLEFOREGETSPECFLAG = aCANCLEFOREGETSPECFLAG;
    }
    public String getRiskType8() {
        return RiskType8;
    }
    public void setRiskType8(String aRiskType8) {
        RiskType8 = aRiskType8;
    }
    public int getSIGNDATECALMODE2() {
        return SIGNDATECALMODE2;
    }
    public void setSIGNDATECALMODE2(int aSIGNDATECALMODE2) {
        SIGNDATECALMODE2 = aSIGNDATECALMODE2;
    }
    public void setSIGNDATECALMODE2(String aSIGNDATECALMODE2) {
        if (aSIGNDATECALMODE2 != null && !aSIGNDATECALMODE2.equals("")) {
            Integer tInteger = new Integer(aSIGNDATECALMODE2);
            int i = tInteger.intValue();
            SIGNDATECALMODE2 = i;
        }
    }

    public int getSIGNDATECALMODE3() {
        return SIGNDATECALMODE3;
    }
    public void setSIGNDATECALMODE3(int aSIGNDATECALMODE3) {
        SIGNDATECALMODE3 = aSIGNDATECALMODE3;
    }
    public void setSIGNDATECALMODE3(String aSIGNDATECALMODE3) {
        if (aSIGNDATECALMODE3 != null && !aSIGNDATECALMODE3.equals("")) {
            Integer tInteger = new Integer(aSIGNDATECALMODE3);
            int i = tInteger.intValue();
            SIGNDATECALMODE3 = i;
        }
    }

    public String getRISKSTYLE() {
        return RISKSTYLE;
    }
    public void setRISKSTYLE(String aRISKSTYLE) {
        RISKSTYLE = aRISKSTYLE;
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("RiskCode") ) {
            return 0;
        }
        if( strFieldName.equals("RiskVer") ) {
            return 1;
        }
        if( strFieldName.equals("RiskName") ) {
            return 2;
        }
        if( strFieldName.equals("KindCode") ) {
            return 3;
        }
        if( strFieldName.equals("RiskType") ) {
            return 4;
        }
        if( strFieldName.equals("RiskType1") ) {
            return 5;
        }
        if( strFieldName.equals("RiskType2") ) {
            return 6;
        }
        if( strFieldName.equals("RiskProp") ) {
            return 7;
        }
        if( strFieldName.equals("RiskPeriod") ) {
            return 8;
        }
        if( strFieldName.equals("RiskTypeDetail") ) {
            return 9;
        }
        if( strFieldName.equals("RiskFlag") ) {
            return 10;
        }
        if( strFieldName.equals("PolType") ) {
            return 11;
        }
        if( strFieldName.equals("InvestFlag") ) {
            return 12;
        }
        if( strFieldName.equals("BonusFlag") ) {
            return 13;
        }
        if( strFieldName.equals("BonusMode") ) {
            return 14;
        }
        if( strFieldName.equals("ListFlag") ) {
            return 15;
        }
        if( strFieldName.equals("SubRiskFlag") ) {
            return 16;
        }
        if( strFieldName.equals("CalDigital") ) {
            return 17;
        }
        if( strFieldName.equals("CalChoMode") ) {
            return 18;
        }
        if( strFieldName.equals("RiskAmntMult") ) {
            return 19;
        }
        if( strFieldName.equals("InsuPeriodFlag") ) {
            return 20;
        }
        if( strFieldName.equals("MaxEndPeriod") ) {
            return 21;
        }
        if( strFieldName.equals("AgeLmt") ) {
            return 22;
        }
        if( strFieldName.equals("SignDateCalMode") ) {
            return 23;
        }
        if( strFieldName.equals("ProtocolFlag") ) {
            return 24;
        }
        if( strFieldName.equals("GetChgFlag") ) {
            return 25;
        }
        if( strFieldName.equals("ProtocolPayFlag") ) {
            return 26;
        }
        if( strFieldName.equals("EnsuPlanFlag") ) {
            return 27;
        }
        if( strFieldName.equals("EnsuPlanAdjFlag") ) {
            return 28;
        }
        if( strFieldName.equals("StartDate") ) {
            return 29;
        }
        if( strFieldName.equals("EndDate") ) {
            return 30;
        }
        if( strFieldName.equals("MinAppntAge") ) {
            return 31;
        }
        if( strFieldName.equals("MaxAppntAge") ) {
            return 32;
        }
        if( strFieldName.equals("MaxInsuredAge") ) {
            return 33;
        }
        if( strFieldName.equals("MinInsuredAge") ) {
            return 34;
        }
        if( strFieldName.equals("AppInterest") ) {
            return 35;
        }
        if( strFieldName.equals("AppPremRate") ) {
            return 36;
        }
        if( strFieldName.equals("InsuredFlag") ) {
            return 37;
        }
        if( strFieldName.equals("ShareFlag") ) {
            return 38;
        }
        if( strFieldName.equals("BnfFlag") ) {
            return 39;
        }
        if( strFieldName.equals("TempPayFlag") ) {
            return 40;
        }
        if( strFieldName.equals("InpPayPlan") ) {
            return 41;
        }
        if( strFieldName.equals("ImpartFlag") ) {
            return 42;
        }
        if( strFieldName.equals("InsuExpeFlag") ) {
            return 43;
        }
        if( strFieldName.equals("LoanFlag") ) {
            return 44;
        }
        if( strFieldName.equals("MortagageFlag") ) {
            return 45;
        }
        if( strFieldName.equals("IDifReturnFlag") ) {
            return 46;
        }
        if( strFieldName.equals("CutAmntStopPay") ) {
            return 47;
        }
        if( strFieldName.equals("RinsRate") ) {
            return 48;
        }
        if( strFieldName.equals("SaleFlag") ) {
            return 49;
        }
        if( strFieldName.equals("FileAppFlag") ) {
            return 50;
        }
        if( strFieldName.equals("MngCom") ) {
            return 51;
        }
        if( strFieldName.equals("AutoPayFlag") ) {
            return 52;
        }
        if( strFieldName.equals("NeedPrintHospital") ) {
            return 53;
        }
        if( strFieldName.equals("NeedPrintGet") ) {
            return 54;
        }
        if( strFieldName.equals("RiskType3") ) {
            return 55;
        }
        if( strFieldName.equals("RiskType4") ) {
            return 56;
        }
        if( strFieldName.equals("RiskType5") ) {
            return 57;
        }
        if( strFieldName.equals("NotPrintPol") ) {
            return 58;
        }
        if( strFieldName.equals("NeedGetPolDate") ) {
            return 59;
        }
        if( strFieldName.equals("NeedReReadBank") ) {
            return 60;
        }
        if( strFieldName.equals("SpecFlag") ) {
            return 61;
        }
        if( strFieldName.equals("InterestDifFlag") ) {
            return 62;
        }
        if( strFieldName.equals("RiskTypeAcc") ) {
            return 63;
        }
        if( strFieldName.equals("RiskPeriodAcc") ) {
            return 64;
        }
        if( strFieldName.equals("RiskType7") ) {
            return 65;
        }
        if( strFieldName.equals("RiskType6") ) {
            return 66;
        }
        if( strFieldName.equals("HealthType") ) {
            return 67;
        }
        if( strFieldName.equals("CANCLEFOREGETSPECFLAG") ) {
            return 68;
        }
        if( strFieldName.equals("RiskType8") ) {
            return 69;
        }
        if( strFieldName.equals("SIGNDATECALMODE2") ) {
            return 70;
        }
        if( strFieldName.equals("SIGNDATECALMODE3") ) {
            return 71;
        }
        if( strFieldName.equals("RISKSTYLE") ) {
            return 72;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "RiskCode";
                break;
            case 1:
                strFieldName = "RiskVer";
                break;
            case 2:
                strFieldName = "RiskName";
                break;
            case 3:
                strFieldName = "KindCode";
                break;
            case 4:
                strFieldName = "RiskType";
                break;
            case 5:
                strFieldName = "RiskType1";
                break;
            case 6:
                strFieldName = "RiskType2";
                break;
            case 7:
                strFieldName = "RiskProp";
                break;
            case 8:
                strFieldName = "RiskPeriod";
                break;
            case 9:
                strFieldName = "RiskTypeDetail";
                break;
            case 10:
                strFieldName = "RiskFlag";
                break;
            case 11:
                strFieldName = "PolType";
                break;
            case 12:
                strFieldName = "InvestFlag";
                break;
            case 13:
                strFieldName = "BonusFlag";
                break;
            case 14:
                strFieldName = "BonusMode";
                break;
            case 15:
                strFieldName = "ListFlag";
                break;
            case 16:
                strFieldName = "SubRiskFlag";
                break;
            case 17:
                strFieldName = "CalDigital";
                break;
            case 18:
                strFieldName = "CalChoMode";
                break;
            case 19:
                strFieldName = "RiskAmntMult";
                break;
            case 20:
                strFieldName = "InsuPeriodFlag";
                break;
            case 21:
                strFieldName = "MaxEndPeriod";
                break;
            case 22:
                strFieldName = "AgeLmt";
                break;
            case 23:
                strFieldName = "SignDateCalMode";
                break;
            case 24:
                strFieldName = "ProtocolFlag";
                break;
            case 25:
                strFieldName = "GetChgFlag";
                break;
            case 26:
                strFieldName = "ProtocolPayFlag";
                break;
            case 27:
                strFieldName = "EnsuPlanFlag";
                break;
            case 28:
                strFieldName = "EnsuPlanAdjFlag";
                break;
            case 29:
                strFieldName = "StartDate";
                break;
            case 30:
                strFieldName = "EndDate";
                break;
            case 31:
                strFieldName = "MinAppntAge";
                break;
            case 32:
                strFieldName = "MaxAppntAge";
                break;
            case 33:
                strFieldName = "MaxInsuredAge";
                break;
            case 34:
                strFieldName = "MinInsuredAge";
                break;
            case 35:
                strFieldName = "AppInterest";
                break;
            case 36:
                strFieldName = "AppPremRate";
                break;
            case 37:
                strFieldName = "InsuredFlag";
                break;
            case 38:
                strFieldName = "ShareFlag";
                break;
            case 39:
                strFieldName = "BnfFlag";
                break;
            case 40:
                strFieldName = "TempPayFlag";
                break;
            case 41:
                strFieldName = "InpPayPlan";
                break;
            case 42:
                strFieldName = "ImpartFlag";
                break;
            case 43:
                strFieldName = "InsuExpeFlag";
                break;
            case 44:
                strFieldName = "LoanFlag";
                break;
            case 45:
                strFieldName = "MortagageFlag";
                break;
            case 46:
                strFieldName = "IDifReturnFlag";
                break;
            case 47:
                strFieldName = "CutAmntStopPay";
                break;
            case 48:
                strFieldName = "RinsRate";
                break;
            case 49:
                strFieldName = "SaleFlag";
                break;
            case 50:
                strFieldName = "FileAppFlag";
                break;
            case 51:
                strFieldName = "MngCom";
                break;
            case 52:
                strFieldName = "AutoPayFlag";
                break;
            case 53:
                strFieldName = "NeedPrintHospital";
                break;
            case 54:
                strFieldName = "NeedPrintGet";
                break;
            case 55:
                strFieldName = "RiskType3";
                break;
            case 56:
                strFieldName = "RiskType4";
                break;
            case 57:
                strFieldName = "RiskType5";
                break;
            case 58:
                strFieldName = "NotPrintPol";
                break;
            case 59:
                strFieldName = "NeedGetPolDate";
                break;
            case 60:
                strFieldName = "NeedReReadBank";
                break;
            case 61:
                strFieldName = "SpecFlag";
                break;
            case 62:
                strFieldName = "InterestDifFlag";
                break;
            case 63:
                strFieldName = "RiskTypeAcc";
                break;
            case 64:
                strFieldName = "RiskPeriodAcc";
                break;
            case 65:
                strFieldName = "RiskType7";
                break;
            case 66:
                strFieldName = "RiskType6";
                break;
            case 67:
                strFieldName = "HealthType";
                break;
            case 68:
                strFieldName = "CANCLEFOREGETSPECFLAG";
                break;
            case 69:
                strFieldName = "RiskType8";
                break;
            case 70:
                strFieldName = "SIGNDATECALMODE2";
                break;
            case 71:
                strFieldName = "SIGNDATECALMODE3";
                break;
            case 72:
                strFieldName = "RISKSTYLE";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "RISKCODE":
                return Schema.TYPE_STRING;
            case "RISKVER":
                return Schema.TYPE_STRING;
            case "RISKNAME":
                return Schema.TYPE_STRING;
            case "KINDCODE":
                return Schema.TYPE_STRING;
            case "RISKTYPE":
                return Schema.TYPE_STRING;
            case "RISKTYPE1":
                return Schema.TYPE_STRING;
            case "RISKTYPE2":
                return Schema.TYPE_STRING;
            case "RISKPROP":
                return Schema.TYPE_STRING;
            case "RISKPERIOD":
                return Schema.TYPE_STRING;
            case "RISKTYPEDETAIL":
                return Schema.TYPE_STRING;
            case "RISKFLAG":
                return Schema.TYPE_STRING;
            case "POLTYPE":
                return Schema.TYPE_STRING;
            case "INVESTFLAG":
                return Schema.TYPE_STRING;
            case "BONUSFLAG":
                return Schema.TYPE_STRING;
            case "BONUSMODE":
                return Schema.TYPE_STRING;
            case "LISTFLAG":
                return Schema.TYPE_STRING;
            case "SUBRISKFLAG":
                return Schema.TYPE_STRING;
            case "CALDIGITAL":
                return Schema.TYPE_INT;
            case "CALCHOMODE":
                return Schema.TYPE_STRING;
            case "RISKAMNTMULT":
                return Schema.TYPE_INT;
            case "INSUPERIODFLAG":
                return Schema.TYPE_STRING;
            case "MAXENDPERIOD":
                return Schema.TYPE_INT;
            case "AGELMT":
                return Schema.TYPE_INT;
            case "SIGNDATECALMODE":
                return Schema.TYPE_INT;
            case "PROTOCOLFLAG":
                return Schema.TYPE_STRING;
            case "GETCHGFLAG":
                return Schema.TYPE_STRING;
            case "PROTOCOLPAYFLAG":
                return Schema.TYPE_STRING;
            case "ENSUPLANFLAG":
                return Schema.TYPE_STRING;
            case "ENSUPLANADJFLAG":
                return Schema.TYPE_STRING;
            case "STARTDATE":
                return Schema.TYPE_STRING;
            case "ENDDATE":
                return Schema.TYPE_STRING;
            case "MINAPPNTAGE":
                return Schema.TYPE_INT;
            case "MAXAPPNTAGE":
                return Schema.TYPE_INT;
            case "MAXINSUREDAGE":
                return Schema.TYPE_INT;
            case "MININSUREDAGE":
                return Schema.TYPE_INT;
            case "APPINTEREST":
                return Schema.TYPE_DOUBLE;
            case "APPPREMRATE":
                return Schema.TYPE_DOUBLE;
            case "INSUREDFLAG":
                return Schema.TYPE_STRING;
            case "SHAREFLAG":
                return Schema.TYPE_STRING;
            case "BNFFLAG":
                return Schema.TYPE_STRING;
            case "TEMPPAYFLAG":
                return Schema.TYPE_STRING;
            case "INPPAYPLAN":
                return Schema.TYPE_STRING;
            case "IMPARTFLAG":
                return Schema.TYPE_STRING;
            case "INSUEXPEFLAG":
                return Schema.TYPE_STRING;
            case "LOANFLAG":
                return Schema.TYPE_STRING;
            case "MORTAGAGEFLAG":
                return Schema.TYPE_STRING;
            case "IDIFRETURNFLAG":
                return Schema.TYPE_STRING;
            case "CUTAMNTSTOPPAY":
                return Schema.TYPE_STRING;
            case "RINSRATE":
                return Schema.TYPE_DOUBLE;
            case "SALEFLAG":
                return Schema.TYPE_STRING;
            case "FILEAPPFLAG":
                return Schema.TYPE_STRING;
            case "MNGCOM":
                return Schema.TYPE_STRING;
            case "AUTOPAYFLAG":
                return Schema.TYPE_STRING;
            case "NEEDPRINTHOSPITAL":
                return Schema.TYPE_STRING;
            case "NEEDPRINTGET":
                return Schema.TYPE_STRING;
            case "RISKTYPE3":
                return Schema.TYPE_STRING;
            case "RISKTYPE4":
                return Schema.TYPE_STRING;
            case "RISKTYPE5":
                return Schema.TYPE_STRING;
            case "NOTPRINTPOL":
                return Schema.TYPE_STRING;
            case "NEEDGETPOLDATE":
                return Schema.TYPE_STRING;
            case "NEEDREREADBANK":
                return Schema.TYPE_STRING;
            case "SPECFLAG":
                return Schema.TYPE_STRING;
            case "INTERESTDIFFLAG":
                return Schema.TYPE_STRING;
            case "RISKTYPEACC":
                return Schema.TYPE_STRING;
            case "RISKPERIODACC":
                return Schema.TYPE_STRING;
            case "RISKTYPE7":
                return Schema.TYPE_STRING;
            case "RISKTYPE6":
                return Schema.TYPE_STRING;
            case "HEALTHTYPE":
                return Schema.TYPE_STRING;
            case "CANCLEFOREGETSPECFLAG":
                return Schema.TYPE_STRING;
            case "RISKTYPE8":
                return Schema.TYPE_STRING;
            case "SIGNDATECALMODE2":
                return Schema.TYPE_INT;
            case "SIGNDATECALMODE3":
                return Schema.TYPE_INT;
            case "RISKSTYLE":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_STRING;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_STRING;
            case 6:
                return Schema.TYPE_STRING;
            case 7:
                return Schema.TYPE_STRING;
            case 8:
                return Schema.TYPE_STRING;
            case 9:
                return Schema.TYPE_STRING;
            case 10:
                return Schema.TYPE_STRING;
            case 11:
                return Schema.TYPE_STRING;
            case 12:
                return Schema.TYPE_STRING;
            case 13:
                return Schema.TYPE_STRING;
            case 14:
                return Schema.TYPE_STRING;
            case 15:
                return Schema.TYPE_STRING;
            case 16:
                return Schema.TYPE_STRING;
            case 17:
                return Schema.TYPE_INT;
            case 18:
                return Schema.TYPE_STRING;
            case 19:
                return Schema.TYPE_INT;
            case 20:
                return Schema.TYPE_STRING;
            case 21:
                return Schema.TYPE_INT;
            case 22:
                return Schema.TYPE_INT;
            case 23:
                return Schema.TYPE_INT;
            case 24:
                return Schema.TYPE_STRING;
            case 25:
                return Schema.TYPE_STRING;
            case 26:
                return Schema.TYPE_STRING;
            case 27:
                return Schema.TYPE_STRING;
            case 28:
                return Schema.TYPE_STRING;
            case 29:
                return Schema.TYPE_STRING;
            case 30:
                return Schema.TYPE_STRING;
            case 31:
                return Schema.TYPE_INT;
            case 32:
                return Schema.TYPE_INT;
            case 33:
                return Schema.TYPE_INT;
            case 34:
                return Schema.TYPE_INT;
            case 35:
                return Schema.TYPE_DOUBLE;
            case 36:
                return Schema.TYPE_DOUBLE;
            case 37:
                return Schema.TYPE_STRING;
            case 38:
                return Schema.TYPE_STRING;
            case 39:
                return Schema.TYPE_STRING;
            case 40:
                return Schema.TYPE_STRING;
            case 41:
                return Schema.TYPE_STRING;
            case 42:
                return Schema.TYPE_STRING;
            case 43:
                return Schema.TYPE_STRING;
            case 44:
                return Schema.TYPE_STRING;
            case 45:
                return Schema.TYPE_STRING;
            case 46:
                return Schema.TYPE_STRING;
            case 47:
                return Schema.TYPE_STRING;
            case 48:
                return Schema.TYPE_DOUBLE;
            case 49:
                return Schema.TYPE_STRING;
            case 50:
                return Schema.TYPE_STRING;
            case 51:
                return Schema.TYPE_STRING;
            case 52:
                return Schema.TYPE_STRING;
            case 53:
                return Schema.TYPE_STRING;
            case 54:
                return Schema.TYPE_STRING;
            case 55:
                return Schema.TYPE_STRING;
            case 56:
                return Schema.TYPE_STRING;
            case 57:
                return Schema.TYPE_STRING;
            case 58:
                return Schema.TYPE_STRING;
            case 59:
                return Schema.TYPE_STRING;
            case 60:
                return Schema.TYPE_STRING;
            case 61:
                return Schema.TYPE_STRING;
            case 62:
                return Schema.TYPE_STRING;
            case 63:
                return Schema.TYPE_STRING;
            case 64:
                return Schema.TYPE_STRING;
            case 65:
                return Schema.TYPE_STRING;
            case 66:
                return Schema.TYPE_STRING;
            case 67:
                return Schema.TYPE_STRING;
            case 68:
                return Schema.TYPE_STRING;
            case 69:
                return Schema.TYPE_STRING;
            case 70:
                return Schema.TYPE_INT;
            case 71:
                return Schema.TYPE_INT;
            case 72:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("RiskCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RiskCode));
        }
        if (FCode.equalsIgnoreCase("RiskVer")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RiskVer));
        }
        if (FCode.equalsIgnoreCase("RiskName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RiskName));
        }
        if (FCode.equalsIgnoreCase("KindCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(KindCode));
        }
        if (FCode.equalsIgnoreCase("RiskType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RiskType));
        }
        if (FCode.equalsIgnoreCase("RiskType1")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RiskType1));
        }
        if (FCode.equalsIgnoreCase("RiskType2")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RiskType2));
        }
        if (FCode.equalsIgnoreCase("RiskProp")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RiskProp));
        }
        if (FCode.equalsIgnoreCase("RiskPeriod")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RiskPeriod));
        }
        if (FCode.equalsIgnoreCase("RiskTypeDetail")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RiskTypeDetail));
        }
        if (FCode.equalsIgnoreCase("RiskFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RiskFlag));
        }
        if (FCode.equalsIgnoreCase("PolType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PolType));
        }
        if (FCode.equalsIgnoreCase("InvestFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InvestFlag));
        }
        if (FCode.equalsIgnoreCase("BonusFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BonusFlag));
        }
        if (FCode.equalsIgnoreCase("BonusMode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BonusMode));
        }
        if (FCode.equalsIgnoreCase("ListFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ListFlag));
        }
        if (FCode.equalsIgnoreCase("SubRiskFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SubRiskFlag));
        }
        if (FCode.equalsIgnoreCase("CalDigital")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CalDigital));
        }
        if (FCode.equalsIgnoreCase("CalChoMode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CalChoMode));
        }
        if (FCode.equalsIgnoreCase("RiskAmntMult")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RiskAmntMult));
        }
        if (FCode.equalsIgnoreCase("InsuPeriodFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InsuPeriodFlag));
        }
        if (FCode.equalsIgnoreCase("MaxEndPeriod")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MaxEndPeriod));
        }
        if (FCode.equalsIgnoreCase("AgeLmt")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgeLmt));
        }
        if (FCode.equalsIgnoreCase("SignDateCalMode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SignDateCalMode));
        }
        if (FCode.equalsIgnoreCase("ProtocolFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ProtocolFlag));
        }
        if (FCode.equalsIgnoreCase("GetChgFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GetChgFlag));
        }
        if (FCode.equalsIgnoreCase("ProtocolPayFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ProtocolPayFlag));
        }
        if (FCode.equalsIgnoreCase("EnsuPlanFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(EnsuPlanFlag));
        }
        if (FCode.equalsIgnoreCase("EnsuPlanAdjFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(EnsuPlanAdjFlag));
        }
        if (FCode.equalsIgnoreCase("StartDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StartDate));
        }
        if (FCode.equalsIgnoreCase("EndDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(EndDate));
        }
        if (FCode.equalsIgnoreCase("MinAppntAge")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MinAppntAge));
        }
        if (FCode.equalsIgnoreCase("MaxAppntAge")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MaxAppntAge));
        }
        if (FCode.equalsIgnoreCase("MaxInsuredAge")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MaxInsuredAge));
        }
        if (FCode.equalsIgnoreCase("MinInsuredAge")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MinInsuredAge));
        }
        if (FCode.equalsIgnoreCase("AppInterest")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AppInterest));
        }
        if (FCode.equalsIgnoreCase("AppPremRate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AppPremRate));
        }
        if (FCode.equalsIgnoreCase("InsuredFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InsuredFlag));
        }
        if (FCode.equalsIgnoreCase("ShareFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ShareFlag));
        }
        if (FCode.equalsIgnoreCase("BnfFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BnfFlag));
        }
        if (FCode.equalsIgnoreCase("TempPayFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(TempPayFlag));
        }
        if (FCode.equalsIgnoreCase("InpPayPlan")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InpPayPlan));
        }
        if (FCode.equalsIgnoreCase("ImpartFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ImpartFlag));
        }
        if (FCode.equalsIgnoreCase("InsuExpeFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InsuExpeFlag));
        }
        if (FCode.equalsIgnoreCase("LoanFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(LoanFlag));
        }
        if (FCode.equalsIgnoreCase("MortagageFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MortagageFlag));
        }
        if (FCode.equalsIgnoreCase("IDifReturnFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(IDifReturnFlag));
        }
        if (FCode.equalsIgnoreCase("CutAmntStopPay")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CutAmntStopPay));
        }
        if (FCode.equalsIgnoreCase("RinsRate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RinsRate));
        }
        if (FCode.equalsIgnoreCase("SaleFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SaleFlag));
        }
        if (FCode.equalsIgnoreCase("FileAppFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FileAppFlag));
        }
        if (FCode.equalsIgnoreCase("MngCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MngCom));
        }
        if (FCode.equalsIgnoreCase("AutoPayFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AutoPayFlag));
        }
        if (FCode.equalsIgnoreCase("NeedPrintHospital")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(NeedPrintHospital));
        }
        if (FCode.equalsIgnoreCase("NeedPrintGet")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(NeedPrintGet));
        }
        if (FCode.equalsIgnoreCase("RiskType3")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RiskType3));
        }
        if (FCode.equalsIgnoreCase("RiskType4")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RiskType4));
        }
        if (FCode.equalsIgnoreCase("RiskType5")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RiskType5));
        }
        if (FCode.equalsIgnoreCase("NotPrintPol")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(NotPrintPol));
        }
        if (FCode.equalsIgnoreCase("NeedGetPolDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(NeedGetPolDate));
        }
        if (FCode.equalsIgnoreCase("NeedReReadBank")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(NeedReReadBank));
        }
        if (FCode.equalsIgnoreCase("SpecFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SpecFlag));
        }
        if (FCode.equalsIgnoreCase("InterestDifFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InterestDifFlag));
        }
        if (FCode.equalsIgnoreCase("RiskTypeAcc")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RiskTypeAcc));
        }
        if (FCode.equalsIgnoreCase("RiskPeriodAcc")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RiskPeriodAcc));
        }
        if (FCode.equalsIgnoreCase("RiskType7")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RiskType7));
        }
        if (FCode.equalsIgnoreCase("RiskType6")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RiskType6));
        }
        if (FCode.equalsIgnoreCase("HealthType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(HealthType));
        }
        if (FCode.equalsIgnoreCase("CANCLEFOREGETSPECFLAG")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CANCLEFOREGETSPECFLAG));
        }
        if (FCode.equalsIgnoreCase("RiskType8")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RiskType8));
        }
        if (FCode.equalsIgnoreCase("SIGNDATECALMODE2")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SIGNDATECALMODE2));
        }
        if (FCode.equalsIgnoreCase("SIGNDATECALMODE3")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SIGNDATECALMODE3));
        }
        if (FCode.equalsIgnoreCase("RISKSTYLE")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RISKSTYLE));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = String.valueOf(RiskCode);
                break;
            case 1:
                strFieldValue = String.valueOf(RiskVer);
                break;
            case 2:
                strFieldValue = String.valueOf(RiskName);
                break;
            case 3:
                strFieldValue = String.valueOf(KindCode);
                break;
            case 4:
                strFieldValue = String.valueOf(RiskType);
                break;
            case 5:
                strFieldValue = String.valueOf(RiskType1);
                break;
            case 6:
                strFieldValue = String.valueOf(RiskType2);
                break;
            case 7:
                strFieldValue = String.valueOf(RiskProp);
                break;
            case 8:
                strFieldValue = String.valueOf(RiskPeriod);
                break;
            case 9:
                strFieldValue = String.valueOf(RiskTypeDetail);
                break;
            case 10:
                strFieldValue = String.valueOf(RiskFlag);
                break;
            case 11:
                strFieldValue = String.valueOf(PolType);
                break;
            case 12:
                strFieldValue = String.valueOf(InvestFlag);
                break;
            case 13:
                strFieldValue = String.valueOf(BonusFlag);
                break;
            case 14:
                strFieldValue = String.valueOf(BonusMode);
                break;
            case 15:
                strFieldValue = String.valueOf(ListFlag);
                break;
            case 16:
                strFieldValue = String.valueOf(SubRiskFlag);
                break;
            case 17:
                strFieldValue = String.valueOf(CalDigital);
                break;
            case 18:
                strFieldValue = String.valueOf(CalChoMode);
                break;
            case 19:
                strFieldValue = String.valueOf(RiskAmntMult);
                break;
            case 20:
                strFieldValue = String.valueOf(InsuPeriodFlag);
                break;
            case 21:
                strFieldValue = String.valueOf(MaxEndPeriod);
                break;
            case 22:
                strFieldValue = String.valueOf(AgeLmt);
                break;
            case 23:
                strFieldValue = String.valueOf(SignDateCalMode);
                break;
            case 24:
                strFieldValue = String.valueOf(ProtocolFlag);
                break;
            case 25:
                strFieldValue = String.valueOf(GetChgFlag);
                break;
            case 26:
                strFieldValue = String.valueOf(ProtocolPayFlag);
                break;
            case 27:
                strFieldValue = String.valueOf(EnsuPlanFlag);
                break;
            case 28:
                strFieldValue = String.valueOf(EnsuPlanAdjFlag);
                break;
            case 29:
                strFieldValue = String.valueOf(StartDate);
                break;
            case 30:
                strFieldValue = String.valueOf(EndDate);
                break;
            case 31:
                strFieldValue = String.valueOf(MinAppntAge);
                break;
            case 32:
                strFieldValue = String.valueOf(MaxAppntAge);
                break;
            case 33:
                strFieldValue = String.valueOf(MaxInsuredAge);
                break;
            case 34:
                strFieldValue = String.valueOf(MinInsuredAge);
                break;
            case 35:
                strFieldValue = String.valueOf(AppInterest);
                break;
            case 36:
                strFieldValue = String.valueOf(AppPremRate);
                break;
            case 37:
                strFieldValue = String.valueOf(InsuredFlag);
                break;
            case 38:
                strFieldValue = String.valueOf(ShareFlag);
                break;
            case 39:
                strFieldValue = String.valueOf(BnfFlag);
                break;
            case 40:
                strFieldValue = String.valueOf(TempPayFlag);
                break;
            case 41:
                strFieldValue = String.valueOf(InpPayPlan);
                break;
            case 42:
                strFieldValue = String.valueOf(ImpartFlag);
                break;
            case 43:
                strFieldValue = String.valueOf(InsuExpeFlag);
                break;
            case 44:
                strFieldValue = String.valueOf(LoanFlag);
                break;
            case 45:
                strFieldValue = String.valueOf(MortagageFlag);
                break;
            case 46:
                strFieldValue = String.valueOf(IDifReturnFlag);
                break;
            case 47:
                strFieldValue = String.valueOf(CutAmntStopPay);
                break;
            case 48:
                strFieldValue = String.valueOf(RinsRate);
                break;
            case 49:
                strFieldValue = String.valueOf(SaleFlag);
                break;
            case 50:
                strFieldValue = String.valueOf(FileAppFlag);
                break;
            case 51:
                strFieldValue = String.valueOf(MngCom);
                break;
            case 52:
                strFieldValue = String.valueOf(AutoPayFlag);
                break;
            case 53:
                strFieldValue = String.valueOf(NeedPrintHospital);
                break;
            case 54:
                strFieldValue = String.valueOf(NeedPrintGet);
                break;
            case 55:
                strFieldValue = String.valueOf(RiskType3);
                break;
            case 56:
                strFieldValue = String.valueOf(RiskType4);
                break;
            case 57:
                strFieldValue = String.valueOf(RiskType5);
                break;
            case 58:
                strFieldValue = String.valueOf(NotPrintPol);
                break;
            case 59:
                strFieldValue = String.valueOf(NeedGetPolDate);
                break;
            case 60:
                strFieldValue = String.valueOf(NeedReReadBank);
                break;
            case 61:
                strFieldValue = String.valueOf(SpecFlag);
                break;
            case 62:
                strFieldValue = String.valueOf(InterestDifFlag);
                break;
            case 63:
                strFieldValue = String.valueOf(RiskTypeAcc);
                break;
            case 64:
                strFieldValue = String.valueOf(RiskPeriodAcc);
                break;
            case 65:
                strFieldValue = String.valueOf(RiskType7);
                break;
            case 66:
                strFieldValue = String.valueOf(RiskType6);
                break;
            case 67:
                strFieldValue = String.valueOf(HealthType);
                break;
            case 68:
                strFieldValue = String.valueOf(CANCLEFOREGETSPECFLAG);
                break;
            case 69:
                strFieldValue = String.valueOf(RiskType8);
                break;
            case 70:
                strFieldValue = String.valueOf(SIGNDATECALMODE2);
                break;
            case 71:
                strFieldValue = String.valueOf(SIGNDATECALMODE3);
                break;
            case 72:
                strFieldValue = String.valueOf(RISKSTYLE);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("RiskCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                RiskCode = FValue.trim();
            }
            else
                RiskCode = null;
        }
        if (FCode.equalsIgnoreCase("RiskVer")) {
            if( FValue != null && !FValue.equals(""))
            {
                RiskVer = FValue.trim();
            }
            else
                RiskVer = null;
        }
        if (FCode.equalsIgnoreCase("RiskName")) {
            if( FValue != null && !FValue.equals(""))
            {
                RiskName = FValue.trim();
            }
            else
                RiskName = null;
        }
        if (FCode.equalsIgnoreCase("KindCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                KindCode = FValue.trim();
            }
            else
                KindCode = null;
        }
        if (FCode.equalsIgnoreCase("RiskType")) {
            if( FValue != null && !FValue.equals(""))
            {
                RiskType = FValue.trim();
            }
            else
                RiskType = null;
        }
        if (FCode.equalsIgnoreCase("RiskType1")) {
            if( FValue != null && !FValue.equals(""))
            {
                RiskType1 = FValue.trim();
            }
            else
                RiskType1 = null;
        }
        if (FCode.equalsIgnoreCase("RiskType2")) {
            if( FValue != null && !FValue.equals(""))
            {
                RiskType2 = FValue.trim();
            }
            else
                RiskType2 = null;
        }
        if (FCode.equalsIgnoreCase("RiskProp")) {
            if( FValue != null && !FValue.equals(""))
            {
                RiskProp = FValue.trim();
            }
            else
                RiskProp = null;
        }
        if (FCode.equalsIgnoreCase("RiskPeriod")) {
            if( FValue != null && !FValue.equals(""))
            {
                RiskPeriod = FValue.trim();
            }
            else
                RiskPeriod = null;
        }
        if (FCode.equalsIgnoreCase("RiskTypeDetail")) {
            if( FValue != null && !FValue.equals(""))
            {
                RiskTypeDetail = FValue.trim();
            }
            else
                RiskTypeDetail = null;
        }
        if (FCode.equalsIgnoreCase("RiskFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                RiskFlag = FValue.trim();
            }
            else
                RiskFlag = null;
        }
        if (FCode.equalsIgnoreCase("PolType")) {
            if( FValue != null && !FValue.equals(""))
            {
                PolType = FValue.trim();
            }
            else
                PolType = null;
        }
        if (FCode.equalsIgnoreCase("InvestFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                InvestFlag = FValue.trim();
            }
            else
                InvestFlag = null;
        }
        if (FCode.equalsIgnoreCase("BonusFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                BonusFlag = FValue.trim();
            }
            else
                BonusFlag = null;
        }
        if (FCode.equalsIgnoreCase("BonusMode")) {
            if( FValue != null && !FValue.equals(""))
            {
                BonusMode = FValue.trim();
            }
            else
                BonusMode = null;
        }
        if (FCode.equalsIgnoreCase("ListFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                ListFlag = FValue.trim();
            }
            else
                ListFlag = null;
        }
        if (FCode.equalsIgnoreCase("SubRiskFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                SubRiskFlag = FValue.trim();
            }
            else
                SubRiskFlag = null;
        }
        if (FCode.equalsIgnoreCase("CalDigital")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                CalDigital = i;
            }
        }
        if (FCode.equalsIgnoreCase("CalChoMode")) {
            if( FValue != null && !FValue.equals(""))
            {
                CalChoMode = FValue.trim();
            }
            else
                CalChoMode = null;
        }
        if (FCode.equalsIgnoreCase("RiskAmntMult")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                RiskAmntMult = i;
            }
        }
        if (FCode.equalsIgnoreCase("InsuPeriodFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                InsuPeriodFlag = FValue.trim();
            }
            else
                InsuPeriodFlag = null;
        }
        if (FCode.equalsIgnoreCase("MaxEndPeriod")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                MaxEndPeriod = i;
            }
        }
        if (FCode.equalsIgnoreCase("AgeLmt")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                AgeLmt = i;
            }
        }
        if (FCode.equalsIgnoreCase("SignDateCalMode")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                SignDateCalMode = i;
            }
        }
        if (FCode.equalsIgnoreCase("ProtocolFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                ProtocolFlag = FValue.trim();
            }
            else
                ProtocolFlag = null;
        }
        if (FCode.equalsIgnoreCase("GetChgFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                GetChgFlag = FValue.trim();
            }
            else
                GetChgFlag = null;
        }
        if (FCode.equalsIgnoreCase("ProtocolPayFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                ProtocolPayFlag = FValue.trim();
            }
            else
                ProtocolPayFlag = null;
        }
        if (FCode.equalsIgnoreCase("EnsuPlanFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                EnsuPlanFlag = FValue.trim();
            }
            else
                EnsuPlanFlag = null;
        }
        if (FCode.equalsIgnoreCase("EnsuPlanAdjFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                EnsuPlanAdjFlag = FValue.trim();
            }
            else
                EnsuPlanAdjFlag = null;
        }
        if (FCode.equalsIgnoreCase("StartDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                StartDate = FValue.trim();
            }
            else
                StartDate = null;
        }
        if (FCode.equalsIgnoreCase("EndDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                EndDate = FValue.trim();
            }
            else
                EndDate = null;
        }
        if (FCode.equalsIgnoreCase("MinAppntAge")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                MinAppntAge = i;
            }
        }
        if (FCode.equalsIgnoreCase("MaxAppntAge")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                MaxAppntAge = i;
            }
        }
        if (FCode.equalsIgnoreCase("MaxInsuredAge")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                MaxInsuredAge = i;
            }
        }
        if (FCode.equalsIgnoreCase("MinInsuredAge")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                MinInsuredAge = i;
            }
        }
        if (FCode.equalsIgnoreCase("AppInterest")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                AppInterest = d;
            }
        }
        if (FCode.equalsIgnoreCase("AppPremRate")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                AppPremRate = d;
            }
        }
        if (FCode.equalsIgnoreCase("InsuredFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                InsuredFlag = FValue.trim();
            }
            else
                InsuredFlag = null;
        }
        if (FCode.equalsIgnoreCase("ShareFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                ShareFlag = FValue.trim();
            }
            else
                ShareFlag = null;
        }
        if (FCode.equalsIgnoreCase("BnfFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                BnfFlag = FValue.trim();
            }
            else
                BnfFlag = null;
        }
        if (FCode.equalsIgnoreCase("TempPayFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                TempPayFlag = FValue.trim();
            }
            else
                TempPayFlag = null;
        }
        if (FCode.equalsIgnoreCase("InpPayPlan")) {
            if( FValue != null && !FValue.equals(""))
            {
                InpPayPlan = FValue.trim();
            }
            else
                InpPayPlan = null;
        }
        if (FCode.equalsIgnoreCase("ImpartFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                ImpartFlag = FValue.trim();
            }
            else
                ImpartFlag = null;
        }
        if (FCode.equalsIgnoreCase("InsuExpeFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                InsuExpeFlag = FValue.trim();
            }
            else
                InsuExpeFlag = null;
        }
        if (FCode.equalsIgnoreCase("LoanFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                LoanFlag = FValue.trim();
            }
            else
                LoanFlag = null;
        }
        if (FCode.equalsIgnoreCase("MortagageFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                MortagageFlag = FValue.trim();
            }
            else
                MortagageFlag = null;
        }
        if (FCode.equalsIgnoreCase("IDifReturnFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                IDifReturnFlag = FValue.trim();
            }
            else
                IDifReturnFlag = null;
        }
        if (FCode.equalsIgnoreCase("CutAmntStopPay")) {
            if( FValue != null && !FValue.equals(""))
            {
                CutAmntStopPay = FValue.trim();
            }
            else
                CutAmntStopPay = null;
        }
        if (FCode.equalsIgnoreCase("RinsRate")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                RinsRate = d;
            }
        }
        if (FCode.equalsIgnoreCase("SaleFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                SaleFlag = FValue.trim();
            }
            else
                SaleFlag = null;
        }
        if (FCode.equalsIgnoreCase("FileAppFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                FileAppFlag = FValue.trim();
            }
            else
                FileAppFlag = null;
        }
        if (FCode.equalsIgnoreCase("MngCom")) {
            if( FValue != null && !FValue.equals(""))
            {
                MngCom = FValue.trim();
            }
            else
                MngCom = null;
        }
        if (FCode.equalsIgnoreCase("AutoPayFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                AutoPayFlag = FValue.trim();
            }
            else
                AutoPayFlag = null;
        }
        if (FCode.equalsIgnoreCase("NeedPrintHospital")) {
            if( FValue != null && !FValue.equals(""))
            {
                NeedPrintHospital = FValue.trim();
            }
            else
                NeedPrintHospital = null;
        }
        if (FCode.equalsIgnoreCase("NeedPrintGet")) {
            if( FValue != null && !FValue.equals(""))
            {
                NeedPrintGet = FValue.trim();
            }
            else
                NeedPrintGet = null;
        }
        if (FCode.equalsIgnoreCase("RiskType3")) {
            if( FValue != null && !FValue.equals(""))
            {
                RiskType3 = FValue.trim();
            }
            else
                RiskType3 = null;
        }
        if (FCode.equalsIgnoreCase("RiskType4")) {
            if( FValue != null && !FValue.equals(""))
            {
                RiskType4 = FValue.trim();
            }
            else
                RiskType4 = null;
        }
        if (FCode.equalsIgnoreCase("RiskType5")) {
            if( FValue != null && !FValue.equals(""))
            {
                RiskType5 = FValue.trim();
            }
            else
                RiskType5 = null;
        }
        if (FCode.equalsIgnoreCase("NotPrintPol")) {
            if( FValue != null && !FValue.equals(""))
            {
                NotPrintPol = FValue.trim();
            }
            else
                NotPrintPol = null;
        }
        if (FCode.equalsIgnoreCase("NeedGetPolDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                NeedGetPolDate = FValue.trim();
            }
            else
                NeedGetPolDate = null;
        }
        if (FCode.equalsIgnoreCase("NeedReReadBank")) {
            if( FValue != null && !FValue.equals(""))
            {
                NeedReReadBank = FValue.trim();
            }
            else
                NeedReReadBank = null;
        }
        if (FCode.equalsIgnoreCase("SpecFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                SpecFlag = FValue.trim();
            }
            else
                SpecFlag = null;
        }
        if (FCode.equalsIgnoreCase("InterestDifFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                InterestDifFlag = FValue.trim();
            }
            else
                InterestDifFlag = null;
        }
        if (FCode.equalsIgnoreCase("RiskTypeAcc")) {
            if( FValue != null && !FValue.equals(""))
            {
                RiskTypeAcc = FValue.trim();
            }
            else
                RiskTypeAcc = null;
        }
        if (FCode.equalsIgnoreCase("RiskPeriodAcc")) {
            if( FValue != null && !FValue.equals(""))
            {
                RiskPeriodAcc = FValue.trim();
            }
            else
                RiskPeriodAcc = null;
        }
        if (FCode.equalsIgnoreCase("RiskType7")) {
            if( FValue != null && !FValue.equals(""))
            {
                RiskType7 = FValue.trim();
            }
            else
                RiskType7 = null;
        }
        if (FCode.equalsIgnoreCase("RiskType6")) {
            if( FValue != null && !FValue.equals(""))
            {
                RiskType6 = FValue.trim();
            }
            else
                RiskType6 = null;
        }
        if (FCode.equalsIgnoreCase("HealthType")) {
            if( FValue != null && !FValue.equals(""))
            {
                HealthType = FValue.trim();
            }
            else
                HealthType = null;
        }
        if (FCode.equalsIgnoreCase("CANCLEFOREGETSPECFLAG")) {
            if( FValue != null && !FValue.equals(""))
            {
                CANCLEFOREGETSPECFLAG = FValue.trim();
            }
            else
                CANCLEFOREGETSPECFLAG = null;
        }
        if (FCode.equalsIgnoreCase("RiskType8")) {
            if( FValue != null && !FValue.equals(""))
            {
                RiskType8 = FValue.trim();
            }
            else
                RiskType8 = null;
        }
        if (FCode.equalsIgnoreCase("SIGNDATECALMODE2")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                SIGNDATECALMODE2 = i;
            }
        }
        if (FCode.equalsIgnoreCase("SIGNDATECALMODE3")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                SIGNDATECALMODE3 = i;
            }
        }
        if (FCode.equalsIgnoreCase("RISKSTYLE")) {
            if( FValue != null && !FValue.equals(""))
            {
                RISKSTYLE = FValue.trim();
            }
            else
                RISKSTYLE = null;
        }
        return true;
    }


    public String toString() {
    return "LMRiskAppPojo [" +
            "RiskCode="+RiskCode +
            ", RiskVer="+RiskVer +
            ", RiskName="+RiskName +
            ", KindCode="+KindCode +
            ", RiskType="+RiskType +
            ", RiskType1="+RiskType1 +
            ", RiskType2="+RiskType2 +
            ", RiskProp="+RiskProp +
            ", RiskPeriod="+RiskPeriod +
            ", RiskTypeDetail="+RiskTypeDetail +
            ", RiskFlag="+RiskFlag +
            ", PolType="+PolType +
            ", InvestFlag="+InvestFlag +
            ", BonusFlag="+BonusFlag +
            ", BonusMode="+BonusMode +
            ", ListFlag="+ListFlag +
            ", SubRiskFlag="+SubRiskFlag +
            ", CalDigital="+CalDigital +
            ", CalChoMode="+CalChoMode +
            ", RiskAmntMult="+RiskAmntMult +
            ", InsuPeriodFlag="+InsuPeriodFlag +
            ", MaxEndPeriod="+MaxEndPeriod +
            ", AgeLmt="+AgeLmt +
            ", SignDateCalMode="+SignDateCalMode +
            ", ProtocolFlag="+ProtocolFlag +
            ", GetChgFlag="+GetChgFlag +
            ", ProtocolPayFlag="+ProtocolPayFlag +
            ", EnsuPlanFlag="+EnsuPlanFlag +
            ", EnsuPlanAdjFlag="+EnsuPlanAdjFlag +
            ", StartDate="+StartDate +
            ", EndDate="+EndDate +
            ", MinAppntAge="+MinAppntAge +
            ", MaxAppntAge="+MaxAppntAge +
            ", MaxInsuredAge="+MaxInsuredAge +
            ", MinInsuredAge="+MinInsuredAge +
            ", AppInterest="+AppInterest +
            ", AppPremRate="+AppPremRate +
            ", InsuredFlag="+InsuredFlag +
            ", ShareFlag="+ShareFlag +
            ", BnfFlag="+BnfFlag +
            ", TempPayFlag="+TempPayFlag +
            ", InpPayPlan="+InpPayPlan +
            ", ImpartFlag="+ImpartFlag +
            ", InsuExpeFlag="+InsuExpeFlag +
            ", LoanFlag="+LoanFlag +
            ", MortagageFlag="+MortagageFlag +
            ", IDifReturnFlag="+IDifReturnFlag +
            ", CutAmntStopPay="+CutAmntStopPay +
            ", RinsRate="+RinsRate +
            ", SaleFlag="+SaleFlag +
            ", FileAppFlag="+FileAppFlag +
            ", MngCom="+MngCom +
            ", AutoPayFlag="+AutoPayFlag +
            ", NeedPrintHospital="+NeedPrintHospital +
            ", NeedPrintGet="+NeedPrintGet +
            ", RiskType3="+RiskType3 +
            ", RiskType4="+RiskType4 +
            ", RiskType5="+RiskType5 +
            ", NotPrintPol="+NotPrintPol +
            ", NeedGetPolDate="+NeedGetPolDate +
            ", NeedReReadBank="+NeedReReadBank +
            ", SpecFlag="+SpecFlag +
            ", InterestDifFlag="+InterestDifFlag +
            ", RiskTypeAcc="+RiskTypeAcc +
            ", RiskPeriodAcc="+RiskPeriodAcc +
            ", RiskType7="+RiskType7 +
            ", RiskType6="+RiskType6 +
            ", HealthType="+HealthType +
            ", CANCLEFOREGETSPECFLAG="+CANCLEFOREGETSPECFLAG +
            ", RiskType8="+RiskType8 +
            ", SIGNDATECALMODE2="+SIGNDATECALMODE2 +
            ", SIGNDATECALMODE3="+SIGNDATECALMODE3 +
            ", RISKSTYLE="+RISKSTYLE +"]";
    }
}
