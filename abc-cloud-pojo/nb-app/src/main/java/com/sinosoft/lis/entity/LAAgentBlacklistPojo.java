/**
 * Copyright (c) 2017 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.entity;

import com.sinosoft.cloud.cache.annotation.RedisPrimaryHKey;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.Pojo;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;

import java.io.Serializable;

/**
 * <p>ClassName: LAAgentBlacklistPojo </p>
 * <p>Copyright: Copyright (c) 2017</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2017-10-17
 */
public class LAAgentBlacklistPojo implements Pojo,Serializable {
    // @Field
    /** 代理人黑名单编码 */
    @RedisPrimaryHKey
    private String BlackListCode; 
    /** 姓名 */
    private String Name; 
    /** 性别 */
    private String Sex; 
    /** 出生日期 */
    private String  Birthday;
    /** 籍贯 */
    private String NativePlace; 
    /** 民族 */
    private String Nationality; 
    /** 婚姻状况 */
    private String Marriage; 
    /** 信用等级 */
    private String CreditGrade; 
    /** 家庭地址编码 */
    private String HomeAddressCode; 
    /** 家庭地址 */
    private String HomeAddress; 
    /** 通讯地址 */
    private String PostalAddress; 
    /** 邮政编码 */
    private String ZipCode; 
    /** 电话 */
    private String Phone; 
    /** 传呼 */
    private String BP; 
    /** 手机 */
    private String Mobile; 
    /** E_mail */
    private String EMail; 
    /** 结婚日期 */
    private String  MarriageDate;
    /** 身份证号码 */
    private String IDNo; 
    /** 来源地 */
    private String Source; 
    /** 血型 */
    private String BloodType; 
    /** 政治面貌 */
    private String PolityVisage; 
    /** 学历 */
    private String Degree; 
    /** 毕业院校 */
    private String GraduateSchool; 
    /** 专业 */
    private String Speciality; 
    /** 职称 */
    private String PostTitle; 
    /** 外语水平 */
    private String ForeignLevel; 
    /** 从业年限 */
    private int WorkAge; 
    /** 原工作单位 */
    private String OldCom; 
    /** 原职业 */
    private String OldOccupation; 
    /** 工作职务 */
    private String HeadShip; 
    /** 工种/行业 */
    private String Business; 
    /** 计入黑名单原因 */
    private String BlacklistReason; 
    /** 原所属保险公司 */
    private String InsurerCompany; 
    /** 原所属展业机构 */
    private String AgentName; 
    /** 操作员代码 */
    private String Operator; 
    /** 入机日期 */
    private String  MakeDate;
    /** 入机时间 */
    private String MakeTime; 
    /** 最后一次修改日期 */
    private String  ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime; 
    /** 证件号码类型 */
    private String IDNoType; 


    public static final int FIELDNUM = 40;    // 数据库表的字段个数

    private FDate fDate = new FDate();        // 处理日期
    public String getBlackListCode() {
        return BlackListCode;
    }
    public void setBlackListCode(String aBlackListCode) {
        BlackListCode = aBlackListCode;
    }
    public String getName() {
        return Name;
    }
    public void setName(String aName) {
        Name = aName;
    }
    public String getSex() {
        return Sex;
    }
    public void setSex(String aSex) {
        Sex = aSex;
    }
    public String getBirthday() {
        return Birthday;
    }
    public void setBirthday(String aBirthday) {
        Birthday = aBirthday;
    }
    public String getNativePlace() {
        return NativePlace;
    }
    public void setNativePlace(String aNativePlace) {
        NativePlace = aNativePlace;
    }
    public String getNationality() {
        return Nationality;
    }
    public void setNationality(String aNationality) {
        Nationality = aNationality;
    }
    public String getMarriage() {
        return Marriage;
    }
    public void setMarriage(String aMarriage) {
        Marriage = aMarriage;
    }
    public String getCreditGrade() {
        return CreditGrade;
    }
    public void setCreditGrade(String aCreditGrade) {
        CreditGrade = aCreditGrade;
    }
    public String getHomeAddressCode() {
        return HomeAddressCode;
    }
    public void setHomeAddressCode(String aHomeAddressCode) {
        HomeAddressCode = aHomeAddressCode;
    }
    public String getHomeAddress() {
        return HomeAddress;
    }
    public void setHomeAddress(String aHomeAddress) {
        HomeAddress = aHomeAddress;
    }
    public String getPostalAddress() {
        return PostalAddress;
    }
    public void setPostalAddress(String aPostalAddress) {
        PostalAddress = aPostalAddress;
    }
    public String getZipCode() {
        return ZipCode;
    }
    public void setZipCode(String aZipCode) {
        ZipCode = aZipCode;
    }
    public String getPhone() {
        return Phone;
    }
    public void setPhone(String aPhone) {
        Phone = aPhone;
    }
    public String getBP() {
        return BP;
    }
    public void setBP(String aBP) {
        BP = aBP;
    }
    public String getMobile() {
        return Mobile;
    }
    public void setMobile(String aMobile) {
        Mobile = aMobile;
    }
    public String getEMail() {
        return EMail;
    }
    public void setEMail(String aEMail) {
        EMail = aEMail;
    }
    public String getMarriageDate() {
        return MarriageDate;
    }
    public void setMarriageDate(String aMarriageDate) {
        MarriageDate = aMarriageDate;
    }
    public String getIDNo() {
        return IDNo;
    }
    public void setIDNo(String aIDNo) {
        IDNo = aIDNo;
    }
    public String getSource() {
        return Source;
    }
    public void setSource(String aSource) {
        Source = aSource;
    }
    public String getBloodType() {
        return BloodType;
    }
    public void setBloodType(String aBloodType) {
        BloodType = aBloodType;
    }
    public String getPolityVisage() {
        return PolityVisage;
    }
    public void setPolityVisage(String aPolityVisage) {
        PolityVisage = aPolityVisage;
    }
    public String getDegree() {
        return Degree;
    }
    public void setDegree(String aDegree) {
        Degree = aDegree;
    }
    public String getGraduateSchool() {
        return GraduateSchool;
    }
    public void setGraduateSchool(String aGraduateSchool) {
        GraduateSchool = aGraduateSchool;
    }
    public String getSpeciality() {
        return Speciality;
    }
    public void setSpeciality(String aSpeciality) {
        Speciality = aSpeciality;
    }
    public String getPostTitle() {
        return PostTitle;
    }
    public void setPostTitle(String aPostTitle) {
        PostTitle = aPostTitle;
    }
    public String getForeignLevel() {
        return ForeignLevel;
    }
    public void setForeignLevel(String aForeignLevel) {
        ForeignLevel = aForeignLevel;
    }
    public int getWorkAge() {
        return WorkAge;
    }
    public void setWorkAge(int aWorkAge) {
        WorkAge = aWorkAge;
    }
    public void setWorkAge(String aWorkAge) {
        if (aWorkAge != null && !aWorkAge.equals("")) {
            Integer tInteger = new Integer(aWorkAge);
            int i = tInteger.intValue();
            WorkAge = i;
        }
    }

    public String getOldCom() {
        return OldCom;
    }
    public void setOldCom(String aOldCom) {
        OldCom = aOldCom;
    }
    public String getOldOccupation() {
        return OldOccupation;
    }
    public void setOldOccupation(String aOldOccupation) {
        OldOccupation = aOldOccupation;
    }
    public String getHeadShip() {
        return HeadShip;
    }
    public void setHeadShip(String aHeadShip) {
        HeadShip = aHeadShip;
    }
    public String getBusiness() {
        return Business;
    }
    public void setBusiness(String aBusiness) {
        Business = aBusiness;
    }
    public String getBlacklistReason() {
        return BlacklistReason;
    }
    public void setBlacklistReason(String aBlacklistReason) {
        BlacklistReason = aBlacklistReason;
    }
    public String getInsurerCompany() {
        return InsurerCompany;
    }
    public void setInsurerCompany(String aInsurerCompany) {
        InsurerCompany = aInsurerCompany;
    }
    public String getAgentName() {
        return AgentName;
    }
    public void setAgentName(String aAgentName) {
        AgentName = aAgentName;
    }
    public String getOperator() {
        return Operator;
    }
    public void setOperator(String aOperator) {
        Operator = aOperator;
    }
    public String getMakeDate() {
        return MakeDate;
    }
    public void setMakeDate(String aMakeDate) {
        MakeDate = aMakeDate;
    }
    public String getMakeTime() {
        return MakeTime;
    }
    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }
    public String getModifyDate() {
        return ModifyDate;
    }
    public void setModifyDate(String aModifyDate) {
        ModifyDate = aModifyDate;
    }
    public String getModifyTime() {
        return ModifyTime;
    }
    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }
    public String getIDNoType() {
        return IDNoType;
    }
    public void setIDNoType(String aIDNoType) {
        IDNoType = aIDNoType;
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("BlackListCode") ) {
            return 0;
        }
        if( strFieldName.equals("Name") ) {
            return 1;
        }
        if( strFieldName.equals("Sex") ) {
            return 2;
        }
        if( strFieldName.equals("Birthday") ) {
            return 3;
        }
        if( strFieldName.equals("NativePlace") ) {
            return 4;
        }
        if( strFieldName.equals("Nationality") ) {
            return 5;
        }
        if( strFieldName.equals("Marriage") ) {
            return 6;
        }
        if( strFieldName.equals("CreditGrade") ) {
            return 7;
        }
        if( strFieldName.equals("HomeAddressCode") ) {
            return 8;
        }
        if( strFieldName.equals("HomeAddress") ) {
            return 9;
        }
        if( strFieldName.equals("PostalAddress") ) {
            return 10;
        }
        if( strFieldName.equals("ZipCode") ) {
            return 11;
        }
        if( strFieldName.equals("Phone") ) {
            return 12;
        }
        if( strFieldName.equals("BP") ) {
            return 13;
        }
        if( strFieldName.equals("Mobile") ) {
            return 14;
        }
        if( strFieldName.equals("EMail") ) {
            return 15;
        }
        if( strFieldName.equals("MarriageDate") ) {
            return 16;
        }
        if( strFieldName.equals("IDNo") ) {
            return 17;
        }
        if( strFieldName.equals("Source") ) {
            return 18;
        }
        if( strFieldName.equals("BloodType") ) {
            return 19;
        }
        if( strFieldName.equals("PolityVisage") ) {
            return 20;
        }
        if( strFieldName.equals("Degree") ) {
            return 21;
        }
        if( strFieldName.equals("GraduateSchool") ) {
            return 22;
        }
        if( strFieldName.equals("Speciality") ) {
            return 23;
        }
        if( strFieldName.equals("PostTitle") ) {
            return 24;
        }
        if( strFieldName.equals("ForeignLevel") ) {
            return 25;
        }
        if( strFieldName.equals("WorkAge") ) {
            return 26;
        }
        if( strFieldName.equals("OldCom") ) {
            return 27;
        }
        if( strFieldName.equals("OldOccupation") ) {
            return 28;
        }
        if( strFieldName.equals("HeadShip") ) {
            return 29;
        }
        if( strFieldName.equals("Business") ) {
            return 30;
        }
        if( strFieldName.equals("BlacklistReason") ) {
            return 31;
        }
        if( strFieldName.equals("InsurerCompany") ) {
            return 32;
        }
        if( strFieldName.equals("AgentName") ) {
            return 33;
        }
        if( strFieldName.equals("Operator") ) {
            return 34;
        }
        if( strFieldName.equals("MakeDate") ) {
            return 35;
        }
        if( strFieldName.equals("MakeTime") ) {
            return 36;
        }
        if( strFieldName.equals("ModifyDate") ) {
            return 37;
        }
        if( strFieldName.equals("ModifyTime") ) {
            return 38;
        }
        if( strFieldName.equals("IDNoType") ) {
            return 39;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "BlackListCode";
                break;
            case 1:
                strFieldName = "Name";
                break;
            case 2:
                strFieldName = "Sex";
                break;
            case 3:
                strFieldName = "Birthday";
                break;
            case 4:
                strFieldName = "NativePlace";
                break;
            case 5:
                strFieldName = "Nationality";
                break;
            case 6:
                strFieldName = "Marriage";
                break;
            case 7:
                strFieldName = "CreditGrade";
                break;
            case 8:
                strFieldName = "HomeAddressCode";
                break;
            case 9:
                strFieldName = "HomeAddress";
                break;
            case 10:
                strFieldName = "PostalAddress";
                break;
            case 11:
                strFieldName = "ZipCode";
                break;
            case 12:
                strFieldName = "Phone";
                break;
            case 13:
                strFieldName = "BP";
                break;
            case 14:
                strFieldName = "Mobile";
                break;
            case 15:
                strFieldName = "EMail";
                break;
            case 16:
                strFieldName = "MarriageDate";
                break;
            case 17:
                strFieldName = "IDNo";
                break;
            case 18:
                strFieldName = "Source";
                break;
            case 19:
                strFieldName = "BloodType";
                break;
            case 20:
                strFieldName = "PolityVisage";
                break;
            case 21:
                strFieldName = "Degree";
                break;
            case 22:
                strFieldName = "GraduateSchool";
                break;
            case 23:
                strFieldName = "Speciality";
                break;
            case 24:
                strFieldName = "PostTitle";
                break;
            case 25:
                strFieldName = "ForeignLevel";
                break;
            case 26:
                strFieldName = "WorkAge";
                break;
            case 27:
                strFieldName = "OldCom";
                break;
            case 28:
                strFieldName = "OldOccupation";
                break;
            case 29:
                strFieldName = "HeadShip";
                break;
            case 30:
                strFieldName = "Business";
                break;
            case 31:
                strFieldName = "BlacklistReason";
                break;
            case 32:
                strFieldName = "InsurerCompany";
                break;
            case 33:
                strFieldName = "AgentName";
                break;
            case 34:
                strFieldName = "Operator";
                break;
            case 35:
                strFieldName = "MakeDate";
                break;
            case 36:
                strFieldName = "MakeTime";
                break;
            case 37:
                strFieldName = "ModifyDate";
                break;
            case 38:
                strFieldName = "ModifyTime";
                break;
            case 39:
                strFieldName = "IDNoType";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "BLACKLISTCODE":
                return Schema.TYPE_STRING;
            case "NAME":
                return Schema.TYPE_STRING;
            case "SEX":
                return Schema.TYPE_STRING;
            case "BIRTHDAY":
                return Schema.TYPE_STRING;
            case "NATIVEPLACE":
                return Schema.TYPE_STRING;
            case "NATIONALITY":
                return Schema.TYPE_STRING;
            case "MARRIAGE":
                return Schema.TYPE_STRING;
            case "CREDITGRADE":
                return Schema.TYPE_STRING;
            case "HOMEADDRESSCODE":
                return Schema.TYPE_STRING;
            case "HOMEADDRESS":
                return Schema.TYPE_STRING;
            case "POSTALADDRESS":
                return Schema.TYPE_STRING;
            case "ZIPCODE":
                return Schema.TYPE_STRING;
            case "PHONE":
                return Schema.TYPE_STRING;
            case "BP":
                return Schema.TYPE_STRING;
            case "MOBILE":
                return Schema.TYPE_STRING;
            case "EMAIL":
                return Schema.TYPE_STRING;
            case "MARRIAGEDATE":
                return Schema.TYPE_STRING;
            case "IDNO":
                return Schema.TYPE_STRING;
            case "SOURCE":
                return Schema.TYPE_STRING;
            case "BLOODTYPE":
                return Schema.TYPE_STRING;
            case "POLITYVISAGE":
                return Schema.TYPE_STRING;
            case "DEGREE":
                return Schema.TYPE_STRING;
            case "GRADUATESCHOOL":
                return Schema.TYPE_STRING;
            case "SPECIALITY":
                return Schema.TYPE_STRING;
            case "POSTTITLE":
                return Schema.TYPE_STRING;
            case "FOREIGNLEVEL":
                return Schema.TYPE_STRING;
            case "WORKAGE":
                return Schema.TYPE_INT;
            case "OLDCOM":
                return Schema.TYPE_STRING;
            case "OLDOCCUPATION":
                return Schema.TYPE_STRING;
            case "HEADSHIP":
                return Schema.TYPE_STRING;
            case "BUSINESS":
                return Schema.TYPE_STRING;
            case "BLACKLISTREASON":
                return Schema.TYPE_STRING;
            case "INSURERCOMPANY":
                return Schema.TYPE_STRING;
            case "AGENTNAME":
                return Schema.TYPE_STRING;
            case "OPERATOR":
                return Schema.TYPE_STRING;
            case "MAKEDATE":
                return Schema.TYPE_STRING;
            case "MAKETIME":
                return Schema.TYPE_STRING;
            case "MODIFYDATE":
                return Schema.TYPE_STRING;
            case "MODIFYTIME":
                return Schema.TYPE_STRING;
            case "IDNOTYPE":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_STRING;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_STRING;
            case 6:
                return Schema.TYPE_STRING;
            case 7:
                return Schema.TYPE_STRING;
            case 8:
                return Schema.TYPE_STRING;
            case 9:
                return Schema.TYPE_STRING;
            case 10:
                return Schema.TYPE_STRING;
            case 11:
                return Schema.TYPE_STRING;
            case 12:
                return Schema.TYPE_STRING;
            case 13:
                return Schema.TYPE_STRING;
            case 14:
                return Schema.TYPE_STRING;
            case 15:
                return Schema.TYPE_STRING;
            case 16:
                return Schema.TYPE_STRING;
            case 17:
                return Schema.TYPE_STRING;
            case 18:
                return Schema.TYPE_STRING;
            case 19:
                return Schema.TYPE_STRING;
            case 20:
                return Schema.TYPE_STRING;
            case 21:
                return Schema.TYPE_STRING;
            case 22:
                return Schema.TYPE_STRING;
            case 23:
                return Schema.TYPE_STRING;
            case 24:
                return Schema.TYPE_STRING;
            case 25:
                return Schema.TYPE_STRING;
            case 26:
                return Schema.TYPE_INT;
            case 27:
                return Schema.TYPE_STRING;
            case 28:
                return Schema.TYPE_STRING;
            case 29:
                return Schema.TYPE_STRING;
            case 30:
                return Schema.TYPE_STRING;
            case 31:
                return Schema.TYPE_STRING;
            case 32:
                return Schema.TYPE_STRING;
            case 33:
                return Schema.TYPE_STRING;
            case 34:
                return Schema.TYPE_STRING;
            case 35:
                return Schema.TYPE_STRING;
            case 36:
                return Schema.TYPE_STRING;
            case 37:
                return Schema.TYPE_STRING;
            case 38:
                return Schema.TYPE_STRING;
            case 39:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("BlackListCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BlackListCode));
        }
        if (FCode.equalsIgnoreCase("Name")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Name));
        }
        if (FCode.equalsIgnoreCase("Sex")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Sex));
        }
        if (FCode.equalsIgnoreCase("Birthday")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Birthday));
        }
        if (FCode.equalsIgnoreCase("NativePlace")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(NativePlace));
        }
        if (FCode.equalsIgnoreCase("Nationality")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Nationality));
        }
        if (FCode.equalsIgnoreCase("Marriage")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Marriage));
        }
        if (FCode.equalsIgnoreCase("CreditGrade")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CreditGrade));
        }
        if (FCode.equalsIgnoreCase("HomeAddressCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(HomeAddressCode));
        }
        if (FCode.equalsIgnoreCase("HomeAddress")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(HomeAddress));
        }
        if (FCode.equalsIgnoreCase("PostalAddress")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PostalAddress));
        }
        if (FCode.equalsIgnoreCase("ZipCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ZipCode));
        }
        if (FCode.equalsIgnoreCase("Phone")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Phone));
        }
        if (FCode.equalsIgnoreCase("BP")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BP));
        }
        if (FCode.equalsIgnoreCase("Mobile")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Mobile));
        }
        if (FCode.equalsIgnoreCase("EMail")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(EMail));
        }
        if (FCode.equalsIgnoreCase("MarriageDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MarriageDate));
        }
        if (FCode.equalsIgnoreCase("IDNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(IDNo));
        }
        if (FCode.equalsIgnoreCase("Source")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Source));
        }
        if (FCode.equalsIgnoreCase("BloodType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BloodType));
        }
        if (FCode.equalsIgnoreCase("PolityVisage")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PolityVisage));
        }
        if (FCode.equalsIgnoreCase("Degree")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Degree));
        }
        if (FCode.equalsIgnoreCase("GraduateSchool")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GraduateSchool));
        }
        if (FCode.equalsIgnoreCase("Speciality")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Speciality));
        }
        if (FCode.equalsIgnoreCase("PostTitle")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PostTitle));
        }
        if (FCode.equalsIgnoreCase("ForeignLevel")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ForeignLevel));
        }
        if (FCode.equalsIgnoreCase("WorkAge")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(WorkAge));
        }
        if (FCode.equalsIgnoreCase("OldCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OldCom));
        }
        if (FCode.equalsIgnoreCase("OldOccupation")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OldOccupation));
        }
        if (FCode.equalsIgnoreCase("HeadShip")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(HeadShip));
        }
        if (FCode.equalsIgnoreCase("Business")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Business));
        }
        if (FCode.equalsIgnoreCase("BlacklistReason")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BlacklistReason));
        }
        if (FCode.equalsIgnoreCase("InsurerCompany")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InsurerCompany));
        }
        if (FCode.equalsIgnoreCase("AgentName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentName));
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeDate));
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyDate));
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (FCode.equalsIgnoreCase("IDNoType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(IDNoType));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = String.valueOf(BlackListCode);
                break;
            case 1:
                strFieldValue = String.valueOf(Name);
                break;
            case 2:
                strFieldValue = String.valueOf(Sex);
                break;
            case 3:
                strFieldValue = String.valueOf(Birthday);
                break;
            case 4:
                strFieldValue = String.valueOf(NativePlace);
                break;
            case 5:
                strFieldValue = String.valueOf(Nationality);
                break;
            case 6:
                strFieldValue = String.valueOf(Marriage);
                break;
            case 7:
                strFieldValue = String.valueOf(CreditGrade);
                break;
            case 8:
                strFieldValue = String.valueOf(HomeAddressCode);
                break;
            case 9:
                strFieldValue = String.valueOf(HomeAddress);
                break;
            case 10:
                strFieldValue = String.valueOf(PostalAddress);
                break;
            case 11:
                strFieldValue = String.valueOf(ZipCode);
                break;
            case 12:
                strFieldValue = String.valueOf(Phone);
                break;
            case 13:
                strFieldValue = String.valueOf(BP);
                break;
            case 14:
                strFieldValue = String.valueOf(Mobile);
                break;
            case 15:
                strFieldValue = String.valueOf(EMail);
                break;
            case 16:
                strFieldValue = String.valueOf(MarriageDate);
                break;
            case 17:
                strFieldValue = String.valueOf(IDNo);
                break;
            case 18:
                strFieldValue = String.valueOf(Source);
                break;
            case 19:
                strFieldValue = String.valueOf(BloodType);
                break;
            case 20:
                strFieldValue = String.valueOf(PolityVisage);
                break;
            case 21:
                strFieldValue = String.valueOf(Degree);
                break;
            case 22:
                strFieldValue = String.valueOf(GraduateSchool);
                break;
            case 23:
                strFieldValue = String.valueOf(Speciality);
                break;
            case 24:
                strFieldValue = String.valueOf(PostTitle);
                break;
            case 25:
                strFieldValue = String.valueOf(ForeignLevel);
                break;
            case 26:
                strFieldValue = String.valueOf(WorkAge);
                break;
            case 27:
                strFieldValue = String.valueOf(OldCom);
                break;
            case 28:
                strFieldValue = String.valueOf(OldOccupation);
                break;
            case 29:
                strFieldValue = String.valueOf(HeadShip);
                break;
            case 30:
                strFieldValue = String.valueOf(Business);
                break;
            case 31:
                strFieldValue = String.valueOf(BlacklistReason);
                break;
            case 32:
                strFieldValue = String.valueOf(InsurerCompany);
                break;
            case 33:
                strFieldValue = String.valueOf(AgentName);
                break;
            case 34:
                strFieldValue = String.valueOf(Operator);
                break;
            case 35:
                strFieldValue = String.valueOf(MakeDate);
                break;
            case 36:
                strFieldValue = String.valueOf(MakeTime);
                break;
            case 37:
                strFieldValue = String.valueOf(ModifyDate);
                break;
            case 38:
                strFieldValue = String.valueOf(ModifyTime);
                break;
            case 39:
                strFieldValue = String.valueOf(IDNoType);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("BlackListCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                BlackListCode = FValue.trim();
            }
            else
                BlackListCode = null;
        }
        if (FCode.equalsIgnoreCase("Name")) {
            if( FValue != null && !FValue.equals(""))
            {
                Name = FValue.trim();
            }
            else
                Name = null;
        }
        if (FCode.equalsIgnoreCase("Sex")) {
            if( FValue != null && !FValue.equals(""))
            {
                Sex = FValue.trim();
            }
            else
                Sex = null;
        }
        if (FCode.equalsIgnoreCase("Birthday")) {
            if( FValue != null && !FValue.equals(""))
            {
                Birthday = FValue.trim();
            }
            else
                Birthday = null;
        }
        if (FCode.equalsIgnoreCase("NativePlace")) {
            if( FValue != null && !FValue.equals(""))
            {
                NativePlace = FValue.trim();
            }
            else
                NativePlace = null;
        }
        if (FCode.equalsIgnoreCase("Nationality")) {
            if( FValue != null && !FValue.equals(""))
            {
                Nationality = FValue.trim();
            }
            else
                Nationality = null;
        }
        if (FCode.equalsIgnoreCase("Marriage")) {
            if( FValue != null && !FValue.equals(""))
            {
                Marriage = FValue.trim();
            }
            else
                Marriage = null;
        }
        if (FCode.equalsIgnoreCase("CreditGrade")) {
            if( FValue != null && !FValue.equals(""))
            {
                CreditGrade = FValue.trim();
            }
            else
                CreditGrade = null;
        }
        if (FCode.equalsIgnoreCase("HomeAddressCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                HomeAddressCode = FValue.trim();
            }
            else
                HomeAddressCode = null;
        }
        if (FCode.equalsIgnoreCase("HomeAddress")) {
            if( FValue != null && !FValue.equals(""))
            {
                HomeAddress = FValue.trim();
            }
            else
                HomeAddress = null;
        }
        if (FCode.equalsIgnoreCase("PostalAddress")) {
            if( FValue != null && !FValue.equals(""))
            {
                PostalAddress = FValue.trim();
            }
            else
                PostalAddress = null;
        }
        if (FCode.equalsIgnoreCase("ZipCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                ZipCode = FValue.trim();
            }
            else
                ZipCode = null;
        }
        if (FCode.equalsIgnoreCase("Phone")) {
            if( FValue != null && !FValue.equals(""))
            {
                Phone = FValue.trim();
            }
            else
                Phone = null;
        }
        if (FCode.equalsIgnoreCase("BP")) {
            if( FValue != null && !FValue.equals(""))
            {
                BP = FValue.trim();
            }
            else
                BP = null;
        }
        if (FCode.equalsIgnoreCase("Mobile")) {
            if( FValue != null && !FValue.equals(""))
            {
                Mobile = FValue.trim();
            }
            else
                Mobile = null;
        }
        if (FCode.equalsIgnoreCase("EMail")) {
            if( FValue != null && !FValue.equals(""))
            {
                EMail = FValue.trim();
            }
            else
                EMail = null;
        }
        if (FCode.equalsIgnoreCase("MarriageDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                MarriageDate = FValue.trim();
            }
            else
                MarriageDate = null;
        }
        if (FCode.equalsIgnoreCase("IDNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                IDNo = FValue.trim();
            }
            else
                IDNo = null;
        }
        if (FCode.equalsIgnoreCase("Source")) {
            if( FValue != null && !FValue.equals(""))
            {
                Source = FValue.trim();
            }
            else
                Source = null;
        }
        if (FCode.equalsIgnoreCase("BloodType")) {
            if( FValue != null && !FValue.equals(""))
            {
                BloodType = FValue.trim();
            }
            else
                BloodType = null;
        }
        if (FCode.equalsIgnoreCase("PolityVisage")) {
            if( FValue != null && !FValue.equals(""))
            {
                PolityVisage = FValue.trim();
            }
            else
                PolityVisage = null;
        }
        if (FCode.equalsIgnoreCase("Degree")) {
            if( FValue != null && !FValue.equals(""))
            {
                Degree = FValue.trim();
            }
            else
                Degree = null;
        }
        if (FCode.equalsIgnoreCase("GraduateSchool")) {
            if( FValue != null && !FValue.equals(""))
            {
                GraduateSchool = FValue.trim();
            }
            else
                GraduateSchool = null;
        }
        if (FCode.equalsIgnoreCase("Speciality")) {
            if( FValue != null && !FValue.equals(""))
            {
                Speciality = FValue.trim();
            }
            else
                Speciality = null;
        }
        if (FCode.equalsIgnoreCase("PostTitle")) {
            if( FValue != null && !FValue.equals(""))
            {
                PostTitle = FValue.trim();
            }
            else
                PostTitle = null;
        }
        if (FCode.equalsIgnoreCase("ForeignLevel")) {
            if( FValue != null && !FValue.equals(""))
            {
                ForeignLevel = FValue.trim();
            }
            else
                ForeignLevel = null;
        }
        if (FCode.equalsIgnoreCase("WorkAge")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                WorkAge = i;
            }
        }
        if (FCode.equalsIgnoreCase("OldCom")) {
            if( FValue != null && !FValue.equals(""))
            {
                OldCom = FValue.trim();
            }
            else
                OldCom = null;
        }
        if (FCode.equalsIgnoreCase("OldOccupation")) {
            if( FValue != null && !FValue.equals(""))
            {
                OldOccupation = FValue.trim();
            }
            else
                OldOccupation = null;
        }
        if (FCode.equalsIgnoreCase("HeadShip")) {
            if( FValue != null && !FValue.equals(""))
            {
                HeadShip = FValue.trim();
            }
            else
                HeadShip = null;
        }
        if (FCode.equalsIgnoreCase("Business")) {
            if( FValue != null && !FValue.equals(""))
            {
                Business = FValue.trim();
            }
            else
                Business = null;
        }
        if (FCode.equalsIgnoreCase("BlacklistReason")) {
            if( FValue != null && !FValue.equals(""))
            {
                BlacklistReason = FValue.trim();
            }
            else
                BlacklistReason = null;
        }
        if (FCode.equalsIgnoreCase("InsurerCompany")) {
            if( FValue != null && !FValue.equals(""))
            {
                InsurerCompany = FValue.trim();
            }
            else
                InsurerCompany = null;
        }
        if (FCode.equalsIgnoreCase("AgentName")) {
            if( FValue != null && !FValue.equals(""))
            {
                AgentName = FValue.trim();
            }
            else
                AgentName = null;
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            if( FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
                Operator = null;
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeDate = FValue.trim();
            }
            else
                MakeDate = null;
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
                MakeTime = null;
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyDate = FValue.trim();
            }
            else
                ModifyDate = null;
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
                ModifyTime = null;
        }
        if (FCode.equalsIgnoreCase("IDNoType")) {
            if( FValue != null && !FValue.equals(""))
            {
                IDNoType = FValue.trim();
            }
            else
                IDNoType = null;
        }
        return true;
    }


    public String toString() {
    return "LAAgentBlacklistPojo [" +
            "BlackListCode="+BlackListCode +
            ", Name="+Name +
            ", Sex="+Sex +
            ", Birthday="+Birthday +
            ", NativePlace="+NativePlace +
            ", Nationality="+Nationality +
            ", Marriage="+Marriage +
            ", CreditGrade="+CreditGrade +
            ", HomeAddressCode="+HomeAddressCode +
            ", HomeAddress="+HomeAddress +
            ", PostalAddress="+PostalAddress +
            ", ZipCode="+ZipCode +
            ", Phone="+Phone +
            ", BP="+BP +
            ", Mobile="+Mobile +
            ", EMail="+EMail +
            ", MarriageDate="+MarriageDate +
            ", IDNo="+IDNo +
            ", Source="+Source +
            ", BloodType="+BloodType +
            ", PolityVisage="+PolityVisage +
            ", Degree="+Degree +
            ", GraduateSchool="+GraduateSchool +
            ", Speciality="+Speciality +
            ", PostTitle="+PostTitle +
            ", ForeignLevel="+ForeignLevel +
            ", WorkAge="+WorkAge +
            ", OldCom="+OldCom +
            ", OldOccupation="+OldOccupation +
            ", HeadShip="+HeadShip +
            ", Business="+Business +
            ", BlacklistReason="+BlacklistReason +
            ", InsurerCompany="+InsurerCompany +
            ", AgentName="+AgentName +
            ", Operator="+Operator +
            ", MakeDate="+MakeDate +
            ", MakeTime="+MakeTime +
            ", ModifyDate="+ModifyDate +
            ", ModifyTime="+ModifyTime +
            ", IDNoType="+IDNoType +"]";
    }
}
