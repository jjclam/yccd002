/**
 * Copyright (c) 2017 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.entity;

import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.Pojo;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;

import java.io.Serializable;

/**
 * <p>ClassName: LCPremToAccPojo </p>
 * <p>Copyright: Copyright (c) 2017</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2017-10-17
 */
public class LCPremToAccPojo implements Pojo,Serializable {
    // @Field
    /** Id */
    private long PremToAccID; 
    /** Fk_lcprem */
    private long PremID; 
    /** Fk_lcinsureacc */
    private long InsureAccID; 
    /** Shardingid */
    private String ShardingID; 
    /** 保单险种号码 */
    private String PolNo; 
    /** 责任编码 */
    private String DutyCode; 
    /** 交费计划编码 */
    private String PayPlanCode; 
    /** 保险帐户号码 */
    private String InsuAccNo; 
    /** 保费项到该帐户比例 */
    private double Rate; 
    /** 是否需要产生新账户 */
    private String NewFlag; 
    /** 转入账户时的算法编码(现金) */
    private String CalCodeMoney; 
    /** 转入账户时的算法编码(股份) */
    private String CalCodeUnit; 
    /** 账户计算标志 */
    private String CalFlag; 
    /** 操作员 */
    private String Operator; 
    /** 入机日期 */
    private String  MakeDate;
    /** 入机时间 */
    private String MakeTime; 
    /** 最后一次修改日期 */
    private String  ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime; 


    public static final int FIELDNUM = 18;    // 数据库表的字段个数

    private FDate fDate = new FDate();        // 处理日期
    public long getPremToAccID() {
        return PremToAccID;
    }
    public void setPremToAccID(long aPremToAccID) {
        PremToAccID = aPremToAccID;
    }
    public void setPremToAccID(String aPremToAccID) {
        if (aPremToAccID != null && !aPremToAccID.equals("")) {
            PremToAccID = new Long(aPremToAccID).longValue();
        }
    }

    public long getPremID() {
        return PremID;
    }
    public void setPremID(long aPremID) {
        PremID = aPremID;
    }
    public void setPremID(String aPremID) {
        if (aPremID != null && !aPremID.equals("")) {
            PremID = new Long(aPremID).longValue();
        }
    }

    public long getInsureAccID() {
        return InsureAccID;
    }
    public void setInsureAccID(long aInsureAccID) {
        InsureAccID = aInsureAccID;
    }
    public void setInsureAccID(String aInsureAccID) {
        if (aInsureAccID != null && !aInsureAccID.equals("")) {
            InsureAccID = new Long(aInsureAccID).longValue();
        }
    }

    public String getShardingID() {
        return ShardingID;
    }
    public void setShardingID(String aShardingID) {
        ShardingID = aShardingID;
    }
    public String getPolNo() {
        return PolNo;
    }
    public void setPolNo(String aPolNo) {
        PolNo = aPolNo;
    }
    public String getDutyCode() {
        return DutyCode;
    }
    public void setDutyCode(String aDutyCode) {
        DutyCode = aDutyCode;
    }
    public String getPayPlanCode() {
        return PayPlanCode;
    }
    public void setPayPlanCode(String aPayPlanCode) {
        PayPlanCode = aPayPlanCode;
    }
    public String getInsuAccNo() {
        return InsuAccNo;
    }
    public void setInsuAccNo(String aInsuAccNo) {
        InsuAccNo = aInsuAccNo;
    }
    public double getRate() {
        return Rate;
    }
    public void setRate(double aRate) {
        Rate = aRate;
    }
    public void setRate(String aRate) {
        if (aRate != null && !aRate.equals("")) {
            Double tDouble = new Double(aRate);
            double d = tDouble.doubleValue();
            Rate = d;
        }
    }

    public String getNewFlag() {
        return NewFlag;
    }
    public void setNewFlag(String aNewFlag) {
        NewFlag = aNewFlag;
    }
    public String getCalCodeMoney() {
        return CalCodeMoney;
    }
    public void setCalCodeMoney(String aCalCodeMoney) {
        CalCodeMoney = aCalCodeMoney;
    }
    public String getCalCodeUnit() {
        return CalCodeUnit;
    }
    public void setCalCodeUnit(String aCalCodeUnit) {
        CalCodeUnit = aCalCodeUnit;
    }
    public String getCalFlag() {
        return CalFlag;
    }
    public void setCalFlag(String aCalFlag) {
        CalFlag = aCalFlag;
    }
    public String getOperator() {
        return Operator;
    }
    public void setOperator(String aOperator) {
        Operator = aOperator;
    }
    public String getMakeDate() {
        return MakeDate;
    }
    public void setMakeDate(String aMakeDate) {
        MakeDate = aMakeDate;
    }
    public String getMakeTime() {
        return MakeTime;
    }
    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }
    public String getModifyDate() {
        return ModifyDate;
    }
    public void setModifyDate(String aModifyDate) {
        ModifyDate = aModifyDate;
    }
    public String getModifyTime() {
        return ModifyTime;
    }
    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("PremToAccID") ) {
            return 0;
        }
        if( strFieldName.equals("PremID") ) {
            return 1;
        }
        if( strFieldName.equals("InsureAccID") ) {
            return 2;
        }
        if( strFieldName.equals("ShardingID") ) {
            return 3;
        }
        if( strFieldName.equals("PolNo") ) {
            return 4;
        }
        if( strFieldName.equals("DutyCode") ) {
            return 5;
        }
        if( strFieldName.equals("PayPlanCode") ) {
            return 6;
        }
        if( strFieldName.equals("InsuAccNo") ) {
            return 7;
        }
        if( strFieldName.equals("Rate") ) {
            return 8;
        }
        if( strFieldName.equals("NewFlag") ) {
            return 9;
        }
        if( strFieldName.equals("CalCodeMoney") ) {
            return 10;
        }
        if( strFieldName.equals("CalCodeUnit") ) {
            return 11;
        }
        if( strFieldName.equals("CalFlag") ) {
            return 12;
        }
        if( strFieldName.equals("Operator") ) {
            return 13;
        }
        if( strFieldName.equals("MakeDate") ) {
            return 14;
        }
        if( strFieldName.equals("MakeTime") ) {
            return 15;
        }
        if( strFieldName.equals("ModifyDate") ) {
            return 16;
        }
        if( strFieldName.equals("ModifyTime") ) {
            return 17;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "PremToAccID";
                break;
            case 1:
                strFieldName = "PremID";
                break;
            case 2:
                strFieldName = "InsureAccID";
                break;
            case 3:
                strFieldName = "ShardingID";
                break;
            case 4:
                strFieldName = "PolNo";
                break;
            case 5:
                strFieldName = "DutyCode";
                break;
            case 6:
                strFieldName = "PayPlanCode";
                break;
            case 7:
                strFieldName = "InsuAccNo";
                break;
            case 8:
                strFieldName = "Rate";
                break;
            case 9:
                strFieldName = "NewFlag";
                break;
            case 10:
                strFieldName = "CalCodeMoney";
                break;
            case 11:
                strFieldName = "CalCodeUnit";
                break;
            case 12:
                strFieldName = "CalFlag";
                break;
            case 13:
                strFieldName = "Operator";
                break;
            case 14:
                strFieldName = "MakeDate";
                break;
            case 15:
                strFieldName = "MakeTime";
                break;
            case 16:
                strFieldName = "ModifyDate";
                break;
            case 17:
                strFieldName = "ModifyTime";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "PREMTOACCID":
                return Schema.TYPE_LONG;
            case "PREMID":
                return Schema.TYPE_LONG;
            case "INSUREACCID":
                return Schema.TYPE_LONG;
            case "SHARDINGID":
                return Schema.TYPE_STRING;
            case "POLNO":
                return Schema.TYPE_STRING;
            case "DUTYCODE":
                return Schema.TYPE_STRING;
            case "PAYPLANCODE":
                return Schema.TYPE_STRING;
            case "INSUACCNO":
                return Schema.TYPE_STRING;
            case "RATE":
                return Schema.TYPE_DOUBLE;
            case "NEWFLAG":
                return Schema.TYPE_STRING;
            case "CALCODEMONEY":
                return Schema.TYPE_STRING;
            case "CALCODEUNIT":
                return Schema.TYPE_STRING;
            case "CALFLAG":
                return Schema.TYPE_STRING;
            case "OPERATOR":
                return Schema.TYPE_STRING;
            case "MAKEDATE":
                return Schema.TYPE_STRING;
            case "MAKETIME":
                return Schema.TYPE_STRING;
            case "MODIFYDATE":
                return Schema.TYPE_STRING;
            case "MODIFYTIME":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_LONG;
            case 1:
                return Schema.TYPE_LONG;
            case 2:
                return Schema.TYPE_LONG;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_STRING;
            case 6:
                return Schema.TYPE_STRING;
            case 7:
                return Schema.TYPE_STRING;
            case 8:
                return Schema.TYPE_DOUBLE;
            case 9:
                return Schema.TYPE_STRING;
            case 10:
                return Schema.TYPE_STRING;
            case 11:
                return Schema.TYPE_STRING;
            case 12:
                return Schema.TYPE_STRING;
            case 13:
                return Schema.TYPE_STRING;
            case 14:
                return Schema.TYPE_STRING;
            case 15:
                return Schema.TYPE_STRING;
            case 16:
                return Schema.TYPE_STRING;
            case 17:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("PremToAccID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PremToAccID));
        }
        if (FCode.equalsIgnoreCase("PremID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PremID));
        }
        if (FCode.equalsIgnoreCase("InsureAccID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InsureAccID));
        }
        if (FCode.equalsIgnoreCase("ShardingID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ShardingID));
        }
        if (FCode.equalsIgnoreCase("PolNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PolNo));
        }
        if (FCode.equalsIgnoreCase("DutyCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DutyCode));
        }
        if (FCode.equalsIgnoreCase("PayPlanCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PayPlanCode));
        }
        if (FCode.equalsIgnoreCase("InsuAccNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InsuAccNo));
        }
        if (FCode.equalsIgnoreCase("Rate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Rate));
        }
        if (FCode.equalsIgnoreCase("NewFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(NewFlag));
        }
        if (FCode.equalsIgnoreCase("CalCodeMoney")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CalCodeMoney));
        }
        if (FCode.equalsIgnoreCase("CalCodeUnit")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CalCodeUnit));
        }
        if (FCode.equalsIgnoreCase("CalFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CalFlag));
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeDate));
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyDate));
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = String.valueOf(PremToAccID);
                break;
            case 1:
                strFieldValue = String.valueOf(PremID);
                break;
            case 2:
                strFieldValue = String.valueOf(InsureAccID);
                break;
            case 3:
                strFieldValue = String.valueOf(ShardingID);
                break;
            case 4:
                strFieldValue = String.valueOf(PolNo);
                break;
            case 5:
                strFieldValue = String.valueOf(DutyCode);
                break;
            case 6:
                strFieldValue = String.valueOf(PayPlanCode);
                break;
            case 7:
                strFieldValue = String.valueOf(InsuAccNo);
                break;
            case 8:
                strFieldValue = String.valueOf(Rate);
                break;
            case 9:
                strFieldValue = String.valueOf(NewFlag);
                break;
            case 10:
                strFieldValue = String.valueOf(CalCodeMoney);
                break;
            case 11:
                strFieldValue = String.valueOf(CalCodeUnit);
                break;
            case 12:
                strFieldValue = String.valueOf(CalFlag);
                break;
            case 13:
                strFieldValue = String.valueOf(Operator);
                break;
            case 14:
                strFieldValue = String.valueOf(MakeDate);
                break;
            case 15:
                strFieldValue = String.valueOf(MakeTime);
                break;
            case 16:
                strFieldValue = String.valueOf(ModifyDate);
                break;
            case 17:
                strFieldValue = String.valueOf(ModifyTime);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("PremToAccID")) {
            if( FValue != null && !FValue.equals("")) {
                PremToAccID = new Long(FValue).longValue();
            }
        }
        if (FCode.equalsIgnoreCase("PremID")) {
            if( FValue != null && !FValue.equals("")) {
                PremID = new Long(FValue).longValue();
            }
        }
        if (FCode.equalsIgnoreCase("InsureAccID")) {
            if( FValue != null && !FValue.equals("")) {
                InsureAccID = new Long(FValue).longValue();
            }
        }
        if (FCode.equalsIgnoreCase("ShardingID")) {
            if( FValue != null && !FValue.equals(""))
            {
                ShardingID = FValue.trim();
            }
            else
                ShardingID = null;
        }
        if (FCode.equalsIgnoreCase("PolNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                PolNo = FValue.trim();
            }
            else
                PolNo = null;
        }
        if (FCode.equalsIgnoreCase("DutyCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                DutyCode = FValue.trim();
            }
            else
                DutyCode = null;
        }
        if (FCode.equalsIgnoreCase("PayPlanCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                PayPlanCode = FValue.trim();
            }
            else
                PayPlanCode = null;
        }
        if (FCode.equalsIgnoreCase("InsuAccNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                InsuAccNo = FValue.trim();
            }
            else
                InsuAccNo = null;
        }
        if (FCode.equalsIgnoreCase("Rate")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                Rate = d;
            }
        }
        if (FCode.equalsIgnoreCase("NewFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                NewFlag = FValue.trim();
            }
            else
                NewFlag = null;
        }
        if (FCode.equalsIgnoreCase("CalCodeMoney")) {
            if( FValue != null && !FValue.equals(""))
            {
                CalCodeMoney = FValue.trim();
            }
            else
                CalCodeMoney = null;
        }
        if (FCode.equalsIgnoreCase("CalCodeUnit")) {
            if( FValue != null && !FValue.equals(""))
            {
                CalCodeUnit = FValue.trim();
            }
            else
                CalCodeUnit = null;
        }
        if (FCode.equalsIgnoreCase("CalFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                CalFlag = FValue.trim();
            }
            else
                CalFlag = null;
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            if( FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
                Operator = null;
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeDate = FValue.trim();
            }
            else
                MakeDate = null;
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
                MakeTime = null;
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyDate = FValue.trim();
            }
            else
                ModifyDate = null;
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
                ModifyTime = null;
        }
        return true;
    }


    public String toString() {
    return "LCPremToAccPojo [" +
            "PremToAccID="+PremToAccID +
            ", PremID="+PremID +
            ", InsureAccID="+InsureAccID +
            ", ShardingID="+ShardingID +
            ", PolNo="+PolNo +
            ", DutyCode="+DutyCode +
            ", PayPlanCode="+PayPlanCode +
            ", InsuAccNo="+InsuAccNo +
            ", Rate="+Rate +
            ", NewFlag="+NewFlag +
            ", CalCodeMoney="+CalCodeMoney +
            ", CalCodeUnit="+CalCodeUnit +
            ", CalFlag="+CalFlag +
            ", Operator="+Operator +
            ", MakeDate="+MakeDate +
            ", MakeTime="+MakeTime +
            ", ModifyDate="+ModifyDate +
            ", ModifyTime="+ModifyTime +"]";
    }
}
