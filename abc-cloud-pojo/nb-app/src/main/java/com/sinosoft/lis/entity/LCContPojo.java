/**
 * Copyright (c) 2020 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.entity;

import java.io.*;
import java.util.Date;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.StrTool;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * <p>ClassName: LCContPojo </p>
 * <p>Copyright: Copyright (c) 2020</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2020-03-20
 */
public class LCContPojo implements  Pojo,Serializable {
    // @Field
    /** Id */
    private long ContID; 
    /** Shardingid */
    private String ShardingID; 
    /** 集体合同号码 */
    private String GrpContNo; 
    /** 合同号码 */
    private String ContNo; 
    /** 总单投保单号码 */
    private String ProposalContNo; 
    /** 印刷号码 */
    private String PrtNo; 
    /** 总单类型 */
    private String ContType; 
    /** 家庭单类型 */
    private String FamilyType; 
    /** 家庭保障号 */
    private String FamilyID; 
    /** 保单类型标记 */
    private String PolType; 
    /** 卡单标志 */
    private String CardFlag; 
    /** 管理机构 */
    private String ManageCom; 
    /** 处理机构 */
    private String ExecuteCom; 
    /** 代理机构 */
    private String AgentCom; 
    /** 代理人编码 */
    private String AgentCode; 
    /** 代理人组别 */
    private String AgentGroup; 
    /** 联合代理人代码 */
    private String AgentCode1; 
    /** 代理机构内部分类 */
    private String AgentType; 
    /** 销售渠道 */
    private String SaleChnl; 
    /** 经办人 */
    private String Handler; 
    /** 保单口令 */
    private String Password; 
    /** 投保人客户号码 */
    private String AppntNo; 
    /** 投保人名称 */
    private String AppntName; 
    /** 投保人性别 */
    private String AppntSex; 
    /** 投保人出生日期 */
    private String  AppntBirthday;
    /** 投保人证件类型 */
    private String AppntIDType; 
    /** 投保人证件号码 */
    private String AppntIDNo; 
    /** 被保人客户号 */
    private String InsuredNo; 
    /** 被保人名称 */
    private String InsuredName; 
    /** 被保人性别 */
    private String InsuredSex; 
    /** 被保人出生日期 */
    private String  InsuredBirthday;
    /** 证件类型 */
    private String InsuredIDType; 
    /** 证件号码 */
    private String InsuredIDNo; 
    /** 交费间隔 */
    private int PayIntv; 
    /** 交费方式 */
    private String PayMode; 
    /** 交费位置 */
    private String PayLocation; 
    /** 合同争议处理方式 */
    private String DisputedFlag; 
    /** 溢交处理方式 */
    private String OutPayFlag; 
    /** 保单送达方式 */
    private String GetPolMode; 
    /** 签单机构 */
    private String SignCom; 
    /** 签单日期 */
    private String  SignDate;
    /** 签单时间 */
    private String SignTime; 
    /** 银行委托书号码 */
    private String ConsignNo; 
    /** 银行编码 */
    private String BankCode; 
    /** 银行帐号 */
    private String BankAccNo; 
    /** 银行帐户名 */
    private String AccName; 
    /** 保单打印次数 */
    private int PrintCount; 
    /** 遗失补发次数 */
    private int LostTimes; 
    /** 语种标记 */
    private String Lang; 
    /** 币别 */
    private String Currency; 
    /** 备注 */
    private String Remark; 
    /** 人数 */
    private int Peoples; 
    /** 份数 */
    private double Mult; 
    /** 保费 */
    private double Prem; 
    /** 保额 */
    private double Amnt; 
    /** 累计保费 */
    private double SumPrem; 
    /** 余额 */
    private double Dif; 
    /** 交至日期 */
    private String  PaytoDate;
    /** 首期交费日期 */
    private String  FirstPayDate;
    /** 保单生效日期 */
    private String  CValiDate;
    /** 录单人 */
    private String InputOperator; 
    /** 录单完成日期 */
    private String  InputDate;
    /** 录单完成时间 */
    private String InputTime; 
    /** 复核状态 */
    private String ApproveFlag; 
    /** 复核人编码 */
    private String ApproveCode; 
    /** 复核日期 */
    private String  ApproveDate;
    /** 复核时间 */
    private String ApproveTime; 
    /** 核保状态 */
    private String UWFlag; 
    /** 核保人 */
    private String UWOperator; 
    /** 核保完成日期 */
    private String  UWDate;
    /** 核保完成时间 */
    private String UWTime; 
    /** 投保单/保单标志 */
    private String AppFlag; 
    /** 投保单申请日期 */
    private String  PolApplyDate;
    /** 保单送达日期 */
    private String  GetPolDate;
    /** 保单送达时间 */
    private String GetPolTime; 
    /** 保单回执客户签收日期 */
    private String  CustomGetPolDate;
    /** 状态 */
    private String State; 
    /** 操作员 */
    private String Operator; 
    /** 入机日期 */
    private String  MakeDate;
    /** 入机时间 */
    private String MakeTime; 
    /** 最后一次修改日期 */
    private String  ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime; 
    /** 初审人 */
    private String FirstTrialOperator; 
    /** 初审日期 */
    private String  FirstTrialDate;
    /** 初审时间 */
    private String FirstTrialTime; 
    /** 收单人 */
    private String ReceiveOperator; 
    /** 收单日期 */
    private String  ReceiveDate;
    /** 收单时间 */
    private String ReceiveTime; 
    /** 暂收据号 */
    private String TempFeeNo; 
    /** 销售方式 */
    private String SellType; 
    /** 强制人工核保标志 */
    private String ForceUWFlag; 
    /** 强制人工核保原因 */
    private String ForceUWReason; 
    /** 首期银行编码 */
    private String NewBankCode; 
    /** 首期银行帐号 */
    private String NewBankAccNo; 
    /** 首期银行帐户名 */
    private String NewAccName; 
    /** 首期交费方式 */
    private String NewPayMode; 
    /** 银代银行代码 */
    private String AgentBankCode; 
    /** 银代柜员 */
    private String BankAgent; 
    /** 银行柜员姓名 */
    private String BankAgentName; 
    /** 银行柜员电话 */
    private String BankAgentTel; 
    /** 组合险代码 */
    private String ProdSetCode; 
    /** 中介平台保单号码 */
    private String PolicyNo; 
    /** 单证印刷号码 */
    private String BillPressNo; 
    /** 单证类型码 */
    private String CardTypeCode; 
    /** 回访日期 */
    private String  VisitDate;
    /** 回访时间 */
    private String VisitTime; 
    /** 销售机构 */
    private String SaleCom; 
    /** 打印标志 */
    private String PrintFlag; 
    /** 发票标志 */
    private String InvoicePrtFlag; 
    /** 临分标记 */
    private String NewReinsureFlag; 
    /** 续期交费提示 */
    private String RenewPayFlag; 
    /** Appntfirstname */
    private String AppntFirstName; 
    /** Appntlastname */
    private String AppntLastName; 
    /** Insuredfirstname */
    private String InsuredFirstName; 
    /** Insuredlastname */
    private String InsuredLastName; 
    /** 授权标记 */
    private String AuthorFlag; 
    /** 绿色通道 */
    private int GreenChnl; 
    /** 出单方式 */
    private String TBType; 
    /** 电子签名 */
    private String EAuto; 
    /** 保单形式 */
    private String SlipForm; 
    /** 自动垫交标志 */
    private String AutoPayFlag; 
    /** 续保标志 */
    private int RnewFlag; 
    /** 家庭保单号码 */
    private String FamilyContNo; 
    /** 商业因素标准体承保标志 */
    private String BussFlag; 
    /** 初审员签名 */
    private String SignName; 
    /** 合同成立日期 */
    private String  OrganizeDate;
    /** 合同成立时间 */
    private String OrganizeTime; 
    /** 首期自动发盘标志 */
    private String NewAutoSendBankFlag; 
    /** 综拓专员编码 */
    private String AgentCodeOper; 
    /** 综拓助理编码 */
    private String AgentCodeAssi; 
    /** 延迟送达  因代码 */
    private String DelayReasonCode; 
    /** 延迟送达原因 */
    private String DelayReasonDesc; 
    /** 续期缴费提示 */
    private String XQremindflag; 
    /** 组织机构代码 */
    private String OrganComCode; 
    /** 开户行所在省 */
    private String BankProivnce; 
    /** 首期开户行所在省 */
    private String NewBankProivnce; 
    /** 仲裁机构 */
    private String ArbitrationCom; 
    /** 印刷单证号 */
    private String OtherPrtno; 
    /** 指定转账日期 */
    private String  DestAccountDate;
    /** 缴费次数 */
    private String PrePayCount; 
    /** 首期开户行所在城市 */
    private String NewBankCity; 
    /** 被保人转账银行 */
    private String InsuredBankCode; 
    /** 被保险人银行账户号 */
    private String InsuredBankAccNo; 
    /** 被保人转账银行账户名 */
    private String InsuredAccName; 
    /** 被保人转账银行所在省 */
    private String InsuredBankProvince; 
    /** 被保人转账银行所在城市 */
    private String InsuredBankCity; 
    /** 首期卡折标志 */
    private String NewAccType; 
    /** 续期卡折标志 */
    private String AccType; 
    /** 续期开户城市 */
    private String BankCity; 
    /** 预算金额 */
    private double EstimateMoney; 
    /** 被保人卡折类型 */
    private String InsuredAccType; 
    /** 是否允许逾越高保额收费 */
    private String IsAllowedCharges; 
    /** 退保保全受理号 */
    private String WTEdorAcceptNo; 
    /** 双录标识 */
    private String Recording; 
    /** 销售渠道编码 */
    private String SaleChannels; 
    /** 代理机构编码 */
    private String ZJAgentCom; 
    /** 代理机构名称 */
    private String ZJAgentComName; 
    /** 是否重录 */
    private String RecordFlag; 
    /** 双主险编码 */
    private String PlanCode; 
    /** 回执单证 */
    private String ReceiptNo; 
    /** 保单补发 */
    private String ReissueCont; 
    /** 职域代码 */
    private String DomainCode; 
    /** 职域名称 */
    private String DomainName; 
    /** 银行推荐人编码 */
    private String ReferralNo; 
    /** 第三方订单号 */
    private String ThirdPartyOrderId; 
    /** 用户id */
    private String UserId; 
    /** 结算号码 */
    private String SettlementNumber; 
    /** 结算日期 */
    private String  SettlementDate;
    /** 结算状态 */
    private String SettlementStatus; 
    /** 产品名称 */
    private String ProductCode; 
    /** 保单终止日期 */
    private String  EndDate;


    public static final int FIELDNUM = 171;    // 数据库表的字段个数

    private FDate fDate = new FDate();        // 处理日期
    public long getContID() {
        return ContID;
    }
    public void setContID(long aContID) {
        ContID = aContID;
    }
    public void setContID(String aContID) {
        if (aContID != null && !aContID.equals("")) {
            ContID = new Long(aContID).longValue();
        }
    }

    public String getShardingID() {
        return ShardingID;
    }
    public void setShardingID(String aShardingID) {
        ShardingID = aShardingID;
    }
    public String getGrpContNo() {
        return GrpContNo;
    }
    public void setGrpContNo(String aGrpContNo) {
        GrpContNo = aGrpContNo;
    }
    public String getContNo() {
        return ContNo;
    }
    public void setContNo(String aContNo) {
        ContNo = aContNo;
    }
    public String getProposalContNo() {
        return ProposalContNo;
    }
    public void setProposalContNo(String aProposalContNo) {
        ProposalContNo = aProposalContNo;
    }
    public String getPrtNo() {
        return PrtNo;
    }
    public void setPrtNo(String aPrtNo) {
        PrtNo = aPrtNo;
    }
    public String getContType() {
        return ContType;
    }
    public void setContType(String aContType) {
        ContType = aContType;
    }
    public String getFamilyType() {
        return FamilyType;
    }
    public void setFamilyType(String aFamilyType) {
        FamilyType = aFamilyType;
    }
    public String getFamilyID() {
        return FamilyID;
    }
    public void setFamilyID(String aFamilyID) {
        FamilyID = aFamilyID;
    }
    public String getPolType() {
        return PolType;
    }
    public void setPolType(String aPolType) {
        PolType = aPolType;
    }
    public String getCardFlag() {
        return CardFlag;
    }
    public void setCardFlag(String aCardFlag) {
        CardFlag = aCardFlag;
    }
    public String getManageCom() {
        return ManageCom;
    }
    public void setManageCom(String aManageCom) {
        ManageCom = aManageCom;
    }
    public String getExecuteCom() {
        return ExecuteCom;
    }
    public void setExecuteCom(String aExecuteCom) {
        ExecuteCom = aExecuteCom;
    }
    public String getAgentCom() {
        return AgentCom;
    }
    public void setAgentCom(String aAgentCom) {
        AgentCom = aAgentCom;
    }
    public String getAgentCode() {
        return AgentCode;
    }
    public void setAgentCode(String aAgentCode) {
        AgentCode = aAgentCode;
    }
    public String getAgentGroup() {
        return AgentGroup;
    }
    public void setAgentGroup(String aAgentGroup) {
        AgentGroup = aAgentGroup;
    }
    public String getAgentCode1() {
        return AgentCode1;
    }
    public void setAgentCode1(String aAgentCode1) {
        AgentCode1 = aAgentCode1;
    }
    public String getAgentType() {
        return AgentType;
    }
    public void setAgentType(String aAgentType) {
        AgentType = aAgentType;
    }
    public String getSaleChnl() {
        return SaleChnl;
    }
    public void setSaleChnl(String aSaleChnl) {
        SaleChnl = aSaleChnl;
    }
    public String getHandler() {
        return Handler;
    }
    public void setHandler(String aHandler) {
        Handler = aHandler;
    }
    public String getPassword() {
        return Password;
    }
    public void setPassword(String aPassword) {
        Password = aPassword;
    }
    public String getAppntNo() {
        return AppntNo;
    }
    public void setAppntNo(String aAppntNo) {
        AppntNo = aAppntNo;
    }
    public String getAppntName() {
        return AppntName;
    }
    public void setAppntName(String aAppntName) {
        AppntName = aAppntName;
    }
    public String getAppntSex() {
        return AppntSex;
    }
    public void setAppntSex(String aAppntSex) {
        AppntSex = aAppntSex;
    }
    public String getAppntBirthday() {
        return AppntBirthday;
    }
    public void setAppntBirthday(String aAppntBirthday) {
        AppntBirthday = aAppntBirthday;
    }
    public String getAppntIDType() {
        return AppntIDType;
    }
    public void setAppntIDType(String aAppntIDType) {
        AppntIDType = aAppntIDType;
    }
    public String getAppntIDNo() {
        return AppntIDNo;
    }
    public void setAppntIDNo(String aAppntIDNo) {
        AppntIDNo = aAppntIDNo;
    }
    public String getInsuredNo() {
        return InsuredNo;
    }
    public void setInsuredNo(String aInsuredNo) {
        InsuredNo = aInsuredNo;
    }
    public String getInsuredName() {
        return InsuredName;
    }
    public void setInsuredName(String aInsuredName) {
        InsuredName = aInsuredName;
    }
    public String getInsuredSex() {
        return InsuredSex;
    }
    public void setInsuredSex(String aInsuredSex) {
        InsuredSex = aInsuredSex;
    }
    public String getInsuredBirthday() {
        return InsuredBirthday;
    }
    public void setInsuredBirthday(String aInsuredBirthday) {
        InsuredBirthday = aInsuredBirthday;
    }
    public String getInsuredIDType() {
        return InsuredIDType;
    }
    public void setInsuredIDType(String aInsuredIDType) {
        InsuredIDType = aInsuredIDType;
    }
    public String getInsuredIDNo() {
        return InsuredIDNo;
    }
    public void setInsuredIDNo(String aInsuredIDNo) {
        InsuredIDNo = aInsuredIDNo;
    }
    public int getPayIntv() {
        return PayIntv;
    }
    public void setPayIntv(int aPayIntv) {
        PayIntv = aPayIntv;
    }
    public void setPayIntv(String aPayIntv) {
        if (aPayIntv != null && !aPayIntv.equals("")) {
            Integer tInteger = new Integer(aPayIntv);
            int i = tInteger.intValue();
            PayIntv = i;
        }
    }

    public String getPayMode() {
        return PayMode;
    }
    public void setPayMode(String aPayMode) {
        PayMode = aPayMode;
    }
    public String getPayLocation() {
        return PayLocation;
    }
    public void setPayLocation(String aPayLocation) {
        PayLocation = aPayLocation;
    }
    public String getDisputedFlag() {
        return DisputedFlag;
    }
    public void setDisputedFlag(String aDisputedFlag) {
        DisputedFlag = aDisputedFlag;
    }
    public String getOutPayFlag() {
        return OutPayFlag;
    }
    public void setOutPayFlag(String aOutPayFlag) {
        OutPayFlag = aOutPayFlag;
    }
    public String getGetPolMode() {
        return GetPolMode;
    }
    public void setGetPolMode(String aGetPolMode) {
        GetPolMode = aGetPolMode;
    }
    public String getSignCom() {
        return SignCom;
    }
    public void setSignCom(String aSignCom) {
        SignCom = aSignCom;
    }
    public String getSignDate() {
        return SignDate;
    }
    public void setSignDate(String aSignDate) {
        SignDate = aSignDate;
    }
    public String getSignTime() {
        return SignTime;
    }
    public void setSignTime(String aSignTime) {
        SignTime = aSignTime;
    }
    public String getConsignNo() {
        return ConsignNo;
    }
    public void setConsignNo(String aConsignNo) {
        ConsignNo = aConsignNo;
    }
    public String getBankCode() {
        return BankCode;
    }
    public void setBankCode(String aBankCode) {
        BankCode = aBankCode;
    }
    public String getBankAccNo() {
        return BankAccNo;
    }
    public void setBankAccNo(String aBankAccNo) {
        BankAccNo = aBankAccNo;
    }
    public String getAccName() {
        return AccName;
    }
    public void setAccName(String aAccName) {
        AccName = aAccName;
    }
    public int getPrintCount() {
        return PrintCount;
    }
    public void setPrintCount(int aPrintCount) {
        PrintCount = aPrintCount;
    }
    public void setPrintCount(String aPrintCount) {
        if (aPrintCount != null && !aPrintCount.equals("")) {
            Integer tInteger = new Integer(aPrintCount);
            int i = tInteger.intValue();
            PrintCount = i;
        }
    }

    public int getLostTimes() {
        return LostTimes;
    }
    public void setLostTimes(int aLostTimes) {
        LostTimes = aLostTimes;
    }
    public void setLostTimes(String aLostTimes) {
        if (aLostTimes != null && !aLostTimes.equals("")) {
            Integer tInteger = new Integer(aLostTimes);
            int i = tInteger.intValue();
            LostTimes = i;
        }
    }

    public String getLang() {
        return Lang;
    }
    public void setLang(String aLang) {
        Lang = aLang;
    }
    public String getCurrency() {
        return Currency;
    }
    public void setCurrency(String aCurrency) {
        Currency = aCurrency;
    }
    public String getRemark() {
        return Remark;
    }
    public void setRemark(String aRemark) {
        Remark = aRemark;
    }
    public int getPeoples() {
        return Peoples;
    }
    public void setPeoples(int aPeoples) {
        Peoples = aPeoples;
    }
    public void setPeoples(String aPeoples) {
        if (aPeoples != null && !aPeoples.equals("")) {
            Integer tInteger = new Integer(aPeoples);
            int i = tInteger.intValue();
            Peoples = i;
        }
    }

    public double getMult() {
        return Mult;
    }
    public void setMult(double aMult) {
        Mult = aMult;
    }
    public void setMult(String aMult) {
        if (aMult != null && !aMult.equals("")) {
            Double tDouble = new Double(aMult);
            double d = tDouble.doubleValue();
            Mult = d;
        }
    }

    public double getPrem() {
        return Prem;
    }
    public void setPrem(double aPrem) {
        Prem = aPrem;
    }
    public void setPrem(String aPrem) {
        if (aPrem != null && !aPrem.equals("")) {
            Double tDouble = new Double(aPrem);
            double d = tDouble.doubleValue();
            Prem = d;
        }
    }

    public double getAmnt() {
        return Amnt;
    }
    public void setAmnt(double aAmnt) {
        Amnt = aAmnt;
    }
    public void setAmnt(String aAmnt) {
        if (aAmnt != null && !aAmnt.equals("")) {
            Double tDouble = new Double(aAmnt);
            double d = tDouble.doubleValue();
            Amnt = d;
        }
    }

    public double getSumPrem() {
        return SumPrem;
    }
    public void setSumPrem(double aSumPrem) {
        SumPrem = aSumPrem;
    }
    public void setSumPrem(String aSumPrem) {
        if (aSumPrem != null && !aSumPrem.equals("")) {
            Double tDouble = new Double(aSumPrem);
            double d = tDouble.doubleValue();
            SumPrem = d;
        }
    }

    public double getDif() {
        return Dif;
    }
    public void setDif(double aDif) {
        Dif = aDif;
    }
    public void setDif(String aDif) {
        if (aDif != null && !aDif.equals("")) {
            Double tDouble = new Double(aDif);
            double d = tDouble.doubleValue();
            Dif = d;
        }
    }

    public String getPaytoDate() {
        return PaytoDate;
    }
    public void setPaytoDate(String aPaytoDate) {
        PaytoDate = aPaytoDate;
    }
    public String getFirstPayDate() {
        return FirstPayDate;
    }
    public void setFirstPayDate(String aFirstPayDate) {
        FirstPayDate = aFirstPayDate;
    }
    public String getCValiDate() {
        return CValiDate;
    }
    public void setCValiDate(String aCValiDate) {
        CValiDate = aCValiDate;
    }
    public String getInputOperator() {
        return InputOperator;
    }
    public void setInputOperator(String aInputOperator) {
        InputOperator = aInputOperator;
    }
    public String getInputDate() {
        return InputDate;
    }
    public void setInputDate(String aInputDate) {
        InputDate = aInputDate;
    }
    public String getInputTime() {
        return InputTime;
    }
    public void setInputTime(String aInputTime) {
        InputTime = aInputTime;
    }
    public String getApproveFlag() {
        return ApproveFlag;
    }
    public void setApproveFlag(String aApproveFlag) {
        ApproveFlag = aApproveFlag;
    }
    public String getApproveCode() {
        return ApproveCode;
    }
    public void setApproveCode(String aApproveCode) {
        ApproveCode = aApproveCode;
    }
    public String getApproveDate() {
        return ApproveDate;
    }
    public void setApproveDate(String aApproveDate) {
        ApproveDate = aApproveDate;
    }
    public String getApproveTime() {
        return ApproveTime;
    }
    public void setApproveTime(String aApproveTime) {
        ApproveTime = aApproveTime;
    }
    public String getUWFlag() {
        return UWFlag;
    }
    public void setUWFlag(String aUWFlag) {
        UWFlag = aUWFlag;
    }
    public String getUWOperator() {
        return UWOperator;
    }
    public void setUWOperator(String aUWOperator) {
        UWOperator = aUWOperator;
    }
    public String getUWDate() {
        return UWDate;
    }
    public void setUWDate(String aUWDate) {
        UWDate = aUWDate;
    }
    public String getUWTime() {
        return UWTime;
    }
    public void setUWTime(String aUWTime) {
        UWTime = aUWTime;
    }
    public String getAppFlag() {
        return AppFlag;
    }
    public void setAppFlag(String aAppFlag) {
        AppFlag = aAppFlag;
    }
    public String getPolApplyDate() {
        return PolApplyDate;
    }
    public void setPolApplyDate(String aPolApplyDate) {
        PolApplyDate = aPolApplyDate;
    }
    public String getGetPolDate() {
        return GetPolDate;
    }
    public void setGetPolDate(String aGetPolDate) {
        GetPolDate = aGetPolDate;
    }
    public String getGetPolTime() {
        return GetPolTime;
    }
    public void setGetPolTime(String aGetPolTime) {
        GetPolTime = aGetPolTime;
    }
    public String getCustomGetPolDate() {
        return CustomGetPolDate;
    }
    public void setCustomGetPolDate(String aCustomGetPolDate) {
        CustomGetPolDate = aCustomGetPolDate;
    }
    public String getState() {
        return State;
    }
    public void setState(String aState) {
        State = aState;
    }
    public String getOperator() {
        return Operator;
    }
    public void setOperator(String aOperator) {
        Operator = aOperator;
    }
    public String getMakeDate() {
        return MakeDate;
    }
    public void setMakeDate(String aMakeDate) {
        MakeDate = aMakeDate;
    }
    public String getMakeTime() {
        return MakeTime;
    }
    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }
    public String getModifyDate() {
        return ModifyDate;
    }
    public void setModifyDate(String aModifyDate) {
        ModifyDate = aModifyDate;
    }
    public String getModifyTime() {
        return ModifyTime;
    }
    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }
    public String getFirstTrialOperator() {
        return FirstTrialOperator;
    }
    public void setFirstTrialOperator(String aFirstTrialOperator) {
        FirstTrialOperator = aFirstTrialOperator;
    }
    public String getFirstTrialDate() {
        return FirstTrialDate;
    }
    public void setFirstTrialDate(String aFirstTrialDate) {
        FirstTrialDate = aFirstTrialDate;
    }
    public String getFirstTrialTime() {
        return FirstTrialTime;
    }
    public void setFirstTrialTime(String aFirstTrialTime) {
        FirstTrialTime = aFirstTrialTime;
    }
    public String getReceiveOperator() {
        return ReceiveOperator;
    }
    public void setReceiveOperator(String aReceiveOperator) {
        ReceiveOperator = aReceiveOperator;
    }
    public String getReceiveDate() {
        return ReceiveDate;
    }
    public void setReceiveDate(String aReceiveDate) {
        ReceiveDate = aReceiveDate;
    }
    public String getReceiveTime() {
        return ReceiveTime;
    }
    public void setReceiveTime(String aReceiveTime) {
        ReceiveTime = aReceiveTime;
    }
    public String getTempFeeNo() {
        return TempFeeNo;
    }
    public void setTempFeeNo(String aTempFeeNo) {
        TempFeeNo = aTempFeeNo;
    }
    public String getSellType() {
        return SellType;
    }
    public void setSellType(String aSellType) {
        SellType = aSellType;
    }
    public String getForceUWFlag() {
        return ForceUWFlag;
    }
    public void setForceUWFlag(String aForceUWFlag) {
        ForceUWFlag = aForceUWFlag;
    }
    public String getForceUWReason() {
        return ForceUWReason;
    }
    public void setForceUWReason(String aForceUWReason) {
        ForceUWReason = aForceUWReason;
    }
    public String getNewBankCode() {
        return NewBankCode;
    }
    public void setNewBankCode(String aNewBankCode) {
        NewBankCode = aNewBankCode;
    }
    public String getNewBankAccNo() {
        return NewBankAccNo;
    }
    public void setNewBankAccNo(String aNewBankAccNo) {
        NewBankAccNo = aNewBankAccNo;
    }
    public String getNewAccName() {
        return NewAccName;
    }
    public void setNewAccName(String aNewAccName) {
        NewAccName = aNewAccName;
    }
    public String getNewPayMode() {
        return NewPayMode;
    }
    public void setNewPayMode(String aNewPayMode) {
        NewPayMode = aNewPayMode;
    }
    public String getAgentBankCode() {
        return AgentBankCode;
    }
    public void setAgentBankCode(String aAgentBankCode) {
        AgentBankCode = aAgentBankCode;
    }
    public String getBankAgent() {
        return BankAgent;
    }
    public void setBankAgent(String aBankAgent) {
        BankAgent = aBankAgent;
    }
    public String getBankAgentName() {
        return BankAgentName;
    }
    public void setBankAgentName(String aBankAgentName) {
        BankAgentName = aBankAgentName;
    }
    public String getBankAgentTel() {
        return BankAgentTel;
    }
    public void setBankAgentTel(String aBankAgentTel) {
        BankAgentTel = aBankAgentTel;
    }
    public String getProdSetCode() {
        return ProdSetCode;
    }
    public void setProdSetCode(String aProdSetCode) {
        ProdSetCode = aProdSetCode;
    }
    public String getPolicyNo() {
        return PolicyNo;
    }
    public void setPolicyNo(String aPolicyNo) {
        PolicyNo = aPolicyNo;
    }
    public String getBillPressNo() {
        return BillPressNo;
    }
    public void setBillPressNo(String aBillPressNo) {
        BillPressNo = aBillPressNo;
    }
    public String getCardTypeCode() {
        return CardTypeCode;
    }
    public void setCardTypeCode(String aCardTypeCode) {
        CardTypeCode = aCardTypeCode;
    }
    public String getVisitDate() {
        return VisitDate;
    }
    public void setVisitDate(String aVisitDate) {
        VisitDate = aVisitDate;
    }
    public String getVisitTime() {
        return VisitTime;
    }
    public void setVisitTime(String aVisitTime) {
        VisitTime = aVisitTime;
    }
    public String getSaleCom() {
        return SaleCom;
    }
    public void setSaleCom(String aSaleCom) {
        SaleCom = aSaleCom;
    }
    public String getPrintFlag() {
        return PrintFlag;
    }
    public void setPrintFlag(String aPrintFlag) {
        PrintFlag = aPrintFlag;
    }
    public String getInvoicePrtFlag() {
        return InvoicePrtFlag;
    }
    public void setInvoicePrtFlag(String aInvoicePrtFlag) {
        InvoicePrtFlag = aInvoicePrtFlag;
    }
    public String getNewReinsureFlag() {
        return NewReinsureFlag;
    }
    public void setNewReinsureFlag(String aNewReinsureFlag) {
        NewReinsureFlag = aNewReinsureFlag;
    }
    public String getRenewPayFlag() {
        return RenewPayFlag;
    }
    public void setRenewPayFlag(String aRenewPayFlag) {
        RenewPayFlag = aRenewPayFlag;
    }
    public String getAppntFirstName() {
        return AppntFirstName;
    }
    public void setAppntFirstName(String aAppntFirstName) {
        AppntFirstName = aAppntFirstName;
    }
    public String getAppntLastName() {
        return AppntLastName;
    }
    public void setAppntLastName(String aAppntLastName) {
        AppntLastName = aAppntLastName;
    }
    public String getInsuredFirstName() {
        return InsuredFirstName;
    }
    public void setInsuredFirstName(String aInsuredFirstName) {
        InsuredFirstName = aInsuredFirstName;
    }
    public String getInsuredLastName() {
        return InsuredLastName;
    }
    public void setInsuredLastName(String aInsuredLastName) {
        InsuredLastName = aInsuredLastName;
    }
    public String getAuthorFlag() {
        return AuthorFlag;
    }
    public void setAuthorFlag(String aAuthorFlag) {
        AuthorFlag = aAuthorFlag;
    }
    public int getGreenChnl() {
        return GreenChnl;
    }
    public void setGreenChnl(int aGreenChnl) {
        GreenChnl = aGreenChnl;
    }
    public void setGreenChnl(String aGreenChnl) {
        if (aGreenChnl != null && !aGreenChnl.equals("")) {
            Integer tInteger = new Integer(aGreenChnl);
            int i = tInteger.intValue();
            GreenChnl = i;
        }
    }

    public String getTBType() {
        return TBType;
    }
    public void setTBType(String aTBType) {
        TBType = aTBType;
    }
    public String getEAuto() {
        return EAuto;
    }
    public void setEAuto(String aEAuto) {
        EAuto = aEAuto;
    }
    public String getSlipForm() {
        return SlipForm;
    }
    public void setSlipForm(String aSlipForm) {
        SlipForm = aSlipForm;
    }
    public String getAutoPayFlag() {
        return AutoPayFlag;
    }
    public void setAutoPayFlag(String aAutoPayFlag) {
        AutoPayFlag = aAutoPayFlag;
    }
    public int getRnewFlag() {
        return RnewFlag;
    }
    public void setRnewFlag(int aRnewFlag) {
        RnewFlag = aRnewFlag;
    }
    public void setRnewFlag(String aRnewFlag) {
        if (aRnewFlag != null && !aRnewFlag.equals("")) {
            Integer tInteger = new Integer(aRnewFlag);
            int i = tInteger.intValue();
            RnewFlag = i;
        }
    }

    public String getFamilyContNo() {
        return FamilyContNo;
    }
    public void setFamilyContNo(String aFamilyContNo) {
        FamilyContNo = aFamilyContNo;
    }
    public String getBussFlag() {
        return BussFlag;
    }
    public void setBussFlag(String aBussFlag) {
        BussFlag = aBussFlag;
    }
    public String getSignName() {
        return SignName;
    }
    public void setSignName(String aSignName) {
        SignName = aSignName;
    }
    public String getOrganizeDate() {
        return OrganizeDate;
    }
    public void setOrganizeDate(String aOrganizeDate) {
        OrganizeDate = aOrganizeDate;
    }
    public String getOrganizeTime() {
        return OrganizeTime;
    }
    public void setOrganizeTime(String aOrganizeTime) {
        OrganizeTime = aOrganizeTime;
    }
    public String getNewAutoSendBankFlag() {
        return NewAutoSendBankFlag;
    }
    public void setNewAutoSendBankFlag(String aNewAutoSendBankFlag) {
        NewAutoSendBankFlag = aNewAutoSendBankFlag;
    }
    public String getAgentCodeOper() {
        return AgentCodeOper;
    }
    public void setAgentCodeOper(String aAgentCodeOper) {
        AgentCodeOper = aAgentCodeOper;
    }
    public String getAgentCodeAssi() {
        return AgentCodeAssi;
    }
    public void setAgentCodeAssi(String aAgentCodeAssi) {
        AgentCodeAssi = aAgentCodeAssi;
    }
    public String getDelayReasonCode() {
        return DelayReasonCode;
    }
    public void setDelayReasonCode(String aDelayReasonCode) {
        DelayReasonCode = aDelayReasonCode;
    }
    public String getDelayReasonDesc() {
        return DelayReasonDesc;
    }
    public void setDelayReasonDesc(String aDelayReasonDesc) {
        DelayReasonDesc = aDelayReasonDesc;
    }
    public String getXQremindflag() {
        return XQremindflag;
    }
    public void setXQremindflag(String aXQremindflag) {
        XQremindflag = aXQremindflag;
    }
    public String getOrganComCode() {
        return OrganComCode;
    }
    public void setOrganComCode(String aOrganComCode) {
        OrganComCode = aOrganComCode;
    }
    public String getBankProivnce() {
        return BankProivnce;
    }
    public void setBankProivnce(String aBankProivnce) {
        BankProivnce = aBankProivnce;
    }
    public String getNewBankProivnce() {
        return NewBankProivnce;
    }
    public void setNewBankProivnce(String aNewBankProivnce) {
        NewBankProivnce = aNewBankProivnce;
    }
    public String getArbitrationCom() {
        return ArbitrationCom;
    }
    public void setArbitrationCom(String aArbitrationCom) {
        ArbitrationCom = aArbitrationCom;
    }
    public String getOtherPrtno() {
        return OtherPrtno;
    }
    public void setOtherPrtno(String aOtherPrtno) {
        OtherPrtno = aOtherPrtno;
    }
    public String getDestAccountDate() {
        return DestAccountDate;
    }
    public void setDestAccountDate(String aDestAccountDate) {
        DestAccountDate = aDestAccountDate;
    }
    public String getPrePayCount() {
        return PrePayCount;
    }
    public void setPrePayCount(String aPrePayCount) {
        PrePayCount = aPrePayCount;
    }
    public String getNewBankCity() {
        return NewBankCity;
    }
    public void setNewBankCity(String aNewBankCity) {
        NewBankCity = aNewBankCity;
    }
    public String getInsuredBankCode() {
        return InsuredBankCode;
    }
    public void setInsuredBankCode(String aInsuredBankCode) {
        InsuredBankCode = aInsuredBankCode;
    }
    public String getInsuredBankAccNo() {
        return InsuredBankAccNo;
    }
    public void setInsuredBankAccNo(String aInsuredBankAccNo) {
        InsuredBankAccNo = aInsuredBankAccNo;
    }
    public String getInsuredAccName() {
        return InsuredAccName;
    }
    public void setInsuredAccName(String aInsuredAccName) {
        InsuredAccName = aInsuredAccName;
    }
    public String getInsuredBankProvince() {
        return InsuredBankProvince;
    }
    public void setInsuredBankProvince(String aInsuredBankProvince) {
        InsuredBankProvince = aInsuredBankProvince;
    }
    public String getInsuredBankCity() {
        return InsuredBankCity;
    }
    public void setInsuredBankCity(String aInsuredBankCity) {
        InsuredBankCity = aInsuredBankCity;
    }
    public String getNewAccType() {
        return NewAccType;
    }
    public void setNewAccType(String aNewAccType) {
        NewAccType = aNewAccType;
    }
    public String getAccType() {
        return AccType;
    }
    public void setAccType(String aAccType) {
        AccType = aAccType;
    }
    public String getBankCity() {
        return BankCity;
    }
    public void setBankCity(String aBankCity) {
        BankCity = aBankCity;
    }
    public double getEstimateMoney() {
        return EstimateMoney;
    }
    public void setEstimateMoney(double aEstimateMoney) {
        EstimateMoney = aEstimateMoney;
    }
    public void setEstimateMoney(String aEstimateMoney) {
        if (aEstimateMoney != null && !aEstimateMoney.equals("")) {
            Double tDouble = new Double(aEstimateMoney);
            double d = tDouble.doubleValue();
            EstimateMoney = d;
        }
    }

    public String getInsuredAccType() {
        return InsuredAccType;
    }
    public void setInsuredAccType(String aInsuredAccType) {
        InsuredAccType = aInsuredAccType;
    }
    public String getIsAllowedCharges() {
        return IsAllowedCharges;
    }
    public void setIsAllowedCharges(String aIsAllowedCharges) {
        IsAllowedCharges = aIsAllowedCharges;
    }
    public String getWTEdorAcceptNo() {
        return WTEdorAcceptNo;
    }
    public void setWTEdorAcceptNo(String aWTEdorAcceptNo) {
        WTEdorAcceptNo = aWTEdorAcceptNo;
    }
    public String getRecording() {
        return Recording;
    }
    public void setRecording(String aRecording) {
        Recording = aRecording;
    }
    public String getSaleChannels() {
        return SaleChannels;
    }
    public void setSaleChannels(String aSaleChannels) {
        SaleChannels = aSaleChannels;
    }
    public String getZJAgentCom() {
        return ZJAgentCom;
    }
    public void setZJAgentCom(String aZJAgentCom) {
        ZJAgentCom = aZJAgentCom;
    }
    public String getZJAgentComName() {
        return ZJAgentComName;
    }
    public void setZJAgentComName(String aZJAgentComName) {
        ZJAgentComName = aZJAgentComName;
    }
    public String getRecordFlag() {
        return RecordFlag;
    }
    public void setRecordFlag(String aRecordFlag) {
        RecordFlag = aRecordFlag;
    }
    public String getPlanCode() {
        return PlanCode;
    }
    public void setPlanCode(String aPlanCode) {
        PlanCode = aPlanCode;
    }
    public String getReceiptNo() {
        return ReceiptNo;
    }
    public void setReceiptNo(String aReceiptNo) {
        ReceiptNo = aReceiptNo;
    }
    public String getReissueCont() {
        return ReissueCont;
    }
    public void setReissueCont(String aReissueCont) {
        ReissueCont = aReissueCont;
    }
    public String getDomainCode() {
        return DomainCode;
    }
    public void setDomainCode(String aDomainCode) {
        DomainCode = aDomainCode;
    }
    public String getDomainName() {
        return DomainName;
    }
    public void setDomainName(String aDomainName) {
        DomainName = aDomainName;
    }
    public String getReferralNo() {
        return ReferralNo;
    }
    public void setReferralNo(String aReferralNo) {
        ReferralNo = aReferralNo;
    }
    public String getThirdPartyOrderId() {
        return ThirdPartyOrderId;
    }
    public void setThirdPartyOrderId(String aThirdPartyOrderId) {
        ThirdPartyOrderId = aThirdPartyOrderId;
    }
    public String getUserId() {
        return UserId;
    }
    public void setUserId(String aUserId) {
        UserId = aUserId;
    }
    public String getSettlementNumber() {
        return SettlementNumber;
    }
    public void setSettlementNumber(String aSettlementNumber) {
        SettlementNumber = aSettlementNumber;
    }
    public String getSettlementDate() {
        return SettlementDate;
    }
    public void setSettlementDate(String aSettlementDate) {
        SettlementDate = aSettlementDate;
    }
    public String getSettlementStatus() {
        return SettlementStatus;
    }
    public void setSettlementStatus(String aSettlementStatus) {
        SettlementStatus = aSettlementStatus;
    }
    public String getProductCode() {
        return ProductCode;
    }
    public void setProductCode(String aProductCode) {
        ProductCode = aProductCode;
    }
    public String getEndDate() {
        return EndDate;
    }
    public void setEndDate(String aEndDate) {
        EndDate = aEndDate;
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("ContID") ) {
            return 0;
        }
        if( strFieldName.equals("ShardingID") ) {
            return 1;
        }
        if( strFieldName.equals("GrpContNo") ) {
            return 2;
        }
        if( strFieldName.equals("ContNo") ) {
            return 3;
        }
        if( strFieldName.equals("ProposalContNo") ) {
            return 4;
        }
        if( strFieldName.equals("PrtNo") ) {
            return 5;
        }
        if( strFieldName.equals("ContType") ) {
            return 6;
        }
        if( strFieldName.equals("FamilyType") ) {
            return 7;
        }
        if( strFieldName.equals("FamilyID") ) {
            return 8;
        }
        if( strFieldName.equals("PolType") ) {
            return 9;
        }
        if( strFieldName.equals("CardFlag") ) {
            return 10;
        }
        if( strFieldName.equals("ManageCom") ) {
            return 11;
        }
        if( strFieldName.equals("ExecuteCom") ) {
            return 12;
        }
        if( strFieldName.equals("AgentCom") ) {
            return 13;
        }
        if( strFieldName.equals("AgentCode") ) {
            return 14;
        }
        if( strFieldName.equals("AgentGroup") ) {
            return 15;
        }
        if( strFieldName.equals("AgentCode1") ) {
            return 16;
        }
        if( strFieldName.equals("AgentType") ) {
            return 17;
        }
        if( strFieldName.equals("SaleChnl") ) {
            return 18;
        }
        if( strFieldName.equals("Handler") ) {
            return 19;
        }
        if( strFieldName.equals("Password") ) {
            return 20;
        }
        if( strFieldName.equals("AppntNo") ) {
            return 21;
        }
        if( strFieldName.equals("AppntName") ) {
            return 22;
        }
        if( strFieldName.equals("AppntSex") ) {
            return 23;
        }
        if( strFieldName.equals("AppntBirthday") ) {
            return 24;
        }
        if( strFieldName.equals("AppntIDType") ) {
            return 25;
        }
        if( strFieldName.equals("AppntIDNo") ) {
            return 26;
        }
        if( strFieldName.equals("InsuredNo") ) {
            return 27;
        }
        if( strFieldName.equals("InsuredName") ) {
            return 28;
        }
        if( strFieldName.equals("InsuredSex") ) {
            return 29;
        }
        if( strFieldName.equals("InsuredBirthday") ) {
            return 30;
        }
        if( strFieldName.equals("InsuredIDType") ) {
            return 31;
        }
        if( strFieldName.equals("InsuredIDNo") ) {
            return 32;
        }
        if( strFieldName.equals("PayIntv") ) {
            return 33;
        }
        if( strFieldName.equals("PayMode") ) {
            return 34;
        }
        if( strFieldName.equals("PayLocation") ) {
            return 35;
        }
        if( strFieldName.equals("DisputedFlag") ) {
            return 36;
        }
        if( strFieldName.equals("OutPayFlag") ) {
            return 37;
        }
        if( strFieldName.equals("GetPolMode") ) {
            return 38;
        }
        if( strFieldName.equals("SignCom") ) {
            return 39;
        }
        if( strFieldName.equals("SignDate") ) {
            return 40;
        }
        if( strFieldName.equals("SignTime") ) {
            return 41;
        }
        if( strFieldName.equals("ConsignNo") ) {
            return 42;
        }
        if( strFieldName.equals("BankCode") ) {
            return 43;
        }
        if( strFieldName.equals("BankAccNo") ) {
            return 44;
        }
        if( strFieldName.equals("AccName") ) {
            return 45;
        }
        if( strFieldName.equals("PrintCount") ) {
            return 46;
        }
        if( strFieldName.equals("LostTimes") ) {
            return 47;
        }
        if( strFieldName.equals("Lang") ) {
            return 48;
        }
        if( strFieldName.equals("Currency") ) {
            return 49;
        }
        if( strFieldName.equals("Remark") ) {
            return 50;
        }
        if( strFieldName.equals("Peoples") ) {
            return 51;
        }
        if( strFieldName.equals("Mult") ) {
            return 52;
        }
        if( strFieldName.equals("Prem") ) {
            return 53;
        }
        if( strFieldName.equals("Amnt") ) {
            return 54;
        }
        if( strFieldName.equals("SumPrem") ) {
            return 55;
        }
        if( strFieldName.equals("Dif") ) {
            return 56;
        }
        if( strFieldName.equals("PaytoDate") ) {
            return 57;
        }
        if( strFieldName.equals("FirstPayDate") ) {
            return 58;
        }
        if( strFieldName.equals("CValiDate") ) {
            return 59;
        }
        if( strFieldName.equals("InputOperator") ) {
            return 60;
        }
        if( strFieldName.equals("InputDate") ) {
            return 61;
        }
        if( strFieldName.equals("InputTime") ) {
            return 62;
        }
        if( strFieldName.equals("ApproveFlag") ) {
            return 63;
        }
        if( strFieldName.equals("ApproveCode") ) {
            return 64;
        }
        if( strFieldName.equals("ApproveDate") ) {
            return 65;
        }
        if( strFieldName.equals("ApproveTime") ) {
            return 66;
        }
        if( strFieldName.equals("UWFlag") ) {
            return 67;
        }
        if( strFieldName.equals("UWOperator") ) {
            return 68;
        }
        if( strFieldName.equals("UWDate") ) {
            return 69;
        }
        if( strFieldName.equals("UWTime") ) {
            return 70;
        }
        if( strFieldName.equals("AppFlag") ) {
            return 71;
        }
        if( strFieldName.equals("PolApplyDate") ) {
            return 72;
        }
        if( strFieldName.equals("GetPolDate") ) {
            return 73;
        }
        if( strFieldName.equals("GetPolTime") ) {
            return 74;
        }
        if( strFieldName.equals("CustomGetPolDate") ) {
            return 75;
        }
        if( strFieldName.equals("State") ) {
            return 76;
        }
        if( strFieldName.equals("Operator") ) {
            return 77;
        }
        if( strFieldName.equals("MakeDate") ) {
            return 78;
        }
        if( strFieldName.equals("MakeTime") ) {
            return 79;
        }
        if( strFieldName.equals("ModifyDate") ) {
            return 80;
        }
        if( strFieldName.equals("ModifyTime") ) {
            return 81;
        }
        if( strFieldName.equals("FirstTrialOperator") ) {
            return 82;
        }
        if( strFieldName.equals("FirstTrialDate") ) {
            return 83;
        }
        if( strFieldName.equals("FirstTrialTime") ) {
            return 84;
        }
        if( strFieldName.equals("ReceiveOperator") ) {
            return 85;
        }
        if( strFieldName.equals("ReceiveDate") ) {
            return 86;
        }
        if( strFieldName.equals("ReceiveTime") ) {
            return 87;
        }
        if( strFieldName.equals("TempFeeNo") ) {
            return 88;
        }
        if( strFieldName.equals("SellType") ) {
            return 89;
        }
        if( strFieldName.equals("ForceUWFlag") ) {
            return 90;
        }
        if( strFieldName.equals("ForceUWReason") ) {
            return 91;
        }
        if( strFieldName.equals("NewBankCode") ) {
            return 92;
        }
        if( strFieldName.equals("NewBankAccNo") ) {
            return 93;
        }
        if( strFieldName.equals("NewAccName") ) {
            return 94;
        }
        if( strFieldName.equals("NewPayMode") ) {
            return 95;
        }
        if( strFieldName.equals("AgentBankCode") ) {
            return 96;
        }
        if( strFieldName.equals("BankAgent") ) {
            return 97;
        }
        if( strFieldName.equals("BankAgentName") ) {
            return 98;
        }
        if( strFieldName.equals("BankAgentTel") ) {
            return 99;
        }
        if( strFieldName.equals("ProdSetCode") ) {
            return 100;
        }
        if( strFieldName.equals("PolicyNo") ) {
            return 101;
        }
        if( strFieldName.equals("BillPressNo") ) {
            return 102;
        }
        if( strFieldName.equals("CardTypeCode") ) {
            return 103;
        }
        if( strFieldName.equals("VisitDate") ) {
            return 104;
        }
        if( strFieldName.equals("VisitTime") ) {
            return 105;
        }
        if( strFieldName.equals("SaleCom") ) {
            return 106;
        }
        if( strFieldName.equals("PrintFlag") ) {
            return 107;
        }
        if( strFieldName.equals("InvoicePrtFlag") ) {
            return 108;
        }
        if( strFieldName.equals("NewReinsureFlag") ) {
            return 109;
        }
        if( strFieldName.equals("RenewPayFlag") ) {
            return 110;
        }
        if( strFieldName.equals("AppntFirstName") ) {
            return 111;
        }
        if( strFieldName.equals("AppntLastName") ) {
            return 112;
        }
        if( strFieldName.equals("InsuredFirstName") ) {
            return 113;
        }
        if( strFieldName.equals("InsuredLastName") ) {
            return 114;
        }
        if( strFieldName.equals("AuthorFlag") ) {
            return 115;
        }
        if( strFieldName.equals("GreenChnl") ) {
            return 116;
        }
        if( strFieldName.equals("TBType") ) {
            return 117;
        }
        if( strFieldName.equals("EAuto") ) {
            return 118;
        }
        if( strFieldName.equals("SlipForm") ) {
            return 119;
        }
        if( strFieldName.equals("AutoPayFlag") ) {
            return 120;
        }
        if( strFieldName.equals("RnewFlag") ) {
            return 121;
        }
        if( strFieldName.equals("FamilyContNo") ) {
            return 122;
        }
        if( strFieldName.equals("BussFlag") ) {
            return 123;
        }
        if( strFieldName.equals("SignName") ) {
            return 124;
        }
        if( strFieldName.equals("OrganizeDate") ) {
            return 125;
        }
        if( strFieldName.equals("OrganizeTime") ) {
            return 126;
        }
        if( strFieldName.equals("NewAutoSendBankFlag") ) {
            return 127;
        }
        if( strFieldName.equals("AgentCodeOper") ) {
            return 128;
        }
        if( strFieldName.equals("AgentCodeAssi") ) {
            return 129;
        }
        if( strFieldName.equals("DelayReasonCode") ) {
            return 130;
        }
        if( strFieldName.equals("DelayReasonDesc") ) {
            return 131;
        }
        if( strFieldName.equals("XQremindflag") ) {
            return 132;
        }
        if( strFieldName.equals("OrganComCode") ) {
            return 133;
        }
        if( strFieldName.equals("BankProivnce") ) {
            return 134;
        }
        if( strFieldName.equals("NewBankProivnce") ) {
            return 135;
        }
        if( strFieldName.equals("ArbitrationCom") ) {
            return 136;
        }
        if( strFieldName.equals("OtherPrtno") ) {
            return 137;
        }
        if( strFieldName.equals("DestAccountDate") ) {
            return 138;
        }
        if( strFieldName.equals("PrePayCount") ) {
            return 139;
        }
        if( strFieldName.equals("NewBankCity") ) {
            return 140;
        }
        if( strFieldName.equals("InsuredBankCode") ) {
            return 141;
        }
        if( strFieldName.equals("InsuredBankAccNo") ) {
            return 142;
        }
        if( strFieldName.equals("InsuredAccName") ) {
            return 143;
        }
        if( strFieldName.equals("InsuredBankProvince") ) {
            return 144;
        }
        if( strFieldName.equals("InsuredBankCity") ) {
            return 145;
        }
        if( strFieldName.equals("NewAccType") ) {
            return 146;
        }
        if( strFieldName.equals("AccType") ) {
            return 147;
        }
        if( strFieldName.equals("BankCity") ) {
            return 148;
        }
        if( strFieldName.equals("EstimateMoney") ) {
            return 149;
        }
        if( strFieldName.equals("InsuredAccType") ) {
            return 150;
        }
        if( strFieldName.equals("IsAllowedCharges") ) {
            return 151;
        }
        if( strFieldName.equals("WTEdorAcceptNo") ) {
            return 152;
        }
        if( strFieldName.equals("Recording") ) {
            return 153;
        }
        if( strFieldName.equals("SaleChannels") ) {
            return 154;
        }
        if( strFieldName.equals("ZJAgentCom") ) {
            return 155;
        }
        if( strFieldName.equals("ZJAgentComName") ) {
            return 156;
        }
        if( strFieldName.equals("RecordFlag") ) {
            return 157;
        }
        if( strFieldName.equals("PlanCode") ) {
            return 158;
        }
        if( strFieldName.equals("ReceiptNo") ) {
            return 159;
        }
        if( strFieldName.equals("ReissueCont") ) {
            return 160;
        }
        if( strFieldName.equals("DomainCode") ) {
            return 161;
        }
        if( strFieldName.equals("DomainName") ) {
            return 162;
        }
        if( strFieldName.equals("ReferralNo") ) {
            return 163;
        }
        if( strFieldName.equals("ThirdPartyOrderId") ) {
            return 164;
        }
        if( strFieldName.equals("UserId") ) {
            return 165;
        }
        if( strFieldName.equals("SettlementNumber") ) {
            return 166;
        }
        if( strFieldName.equals("SettlementDate") ) {
            return 167;
        }
        if( strFieldName.equals("SettlementStatus") ) {
            return 168;
        }
        if( strFieldName.equals("ProductCode") ) {
            return 169;
        }
        if( strFieldName.equals("EndDate") ) {
            return 170;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "ContID";
                break;
            case 1:
                strFieldName = "ShardingID";
                break;
            case 2:
                strFieldName = "GrpContNo";
                break;
            case 3:
                strFieldName = "ContNo";
                break;
            case 4:
                strFieldName = "ProposalContNo";
                break;
            case 5:
                strFieldName = "PrtNo";
                break;
            case 6:
                strFieldName = "ContType";
                break;
            case 7:
                strFieldName = "FamilyType";
                break;
            case 8:
                strFieldName = "FamilyID";
                break;
            case 9:
                strFieldName = "PolType";
                break;
            case 10:
                strFieldName = "CardFlag";
                break;
            case 11:
                strFieldName = "ManageCom";
                break;
            case 12:
                strFieldName = "ExecuteCom";
                break;
            case 13:
                strFieldName = "AgentCom";
                break;
            case 14:
                strFieldName = "AgentCode";
                break;
            case 15:
                strFieldName = "AgentGroup";
                break;
            case 16:
                strFieldName = "AgentCode1";
                break;
            case 17:
                strFieldName = "AgentType";
                break;
            case 18:
                strFieldName = "SaleChnl";
                break;
            case 19:
                strFieldName = "Handler";
                break;
            case 20:
                strFieldName = "Password";
                break;
            case 21:
                strFieldName = "AppntNo";
                break;
            case 22:
                strFieldName = "AppntName";
                break;
            case 23:
                strFieldName = "AppntSex";
                break;
            case 24:
                strFieldName = "AppntBirthday";
                break;
            case 25:
                strFieldName = "AppntIDType";
                break;
            case 26:
                strFieldName = "AppntIDNo";
                break;
            case 27:
                strFieldName = "InsuredNo";
                break;
            case 28:
                strFieldName = "InsuredName";
                break;
            case 29:
                strFieldName = "InsuredSex";
                break;
            case 30:
                strFieldName = "InsuredBirthday";
                break;
            case 31:
                strFieldName = "InsuredIDType";
                break;
            case 32:
                strFieldName = "InsuredIDNo";
                break;
            case 33:
                strFieldName = "PayIntv";
                break;
            case 34:
                strFieldName = "PayMode";
                break;
            case 35:
                strFieldName = "PayLocation";
                break;
            case 36:
                strFieldName = "DisputedFlag";
                break;
            case 37:
                strFieldName = "OutPayFlag";
                break;
            case 38:
                strFieldName = "GetPolMode";
                break;
            case 39:
                strFieldName = "SignCom";
                break;
            case 40:
                strFieldName = "SignDate";
                break;
            case 41:
                strFieldName = "SignTime";
                break;
            case 42:
                strFieldName = "ConsignNo";
                break;
            case 43:
                strFieldName = "BankCode";
                break;
            case 44:
                strFieldName = "BankAccNo";
                break;
            case 45:
                strFieldName = "AccName";
                break;
            case 46:
                strFieldName = "PrintCount";
                break;
            case 47:
                strFieldName = "LostTimes";
                break;
            case 48:
                strFieldName = "Lang";
                break;
            case 49:
                strFieldName = "Currency";
                break;
            case 50:
                strFieldName = "Remark";
                break;
            case 51:
                strFieldName = "Peoples";
                break;
            case 52:
                strFieldName = "Mult";
                break;
            case 53:
                strFieldName = "Prem";
                break;
            case 54:
                strFieldName = "Amnt";
                break;
            case 55:
                strFieldName = "SumPrem";
                break;
            case 56:
                strFieldName = "Dif";
                break;
            case 57:
                strFieldName = "PaytoDate";
                break;
            case 58:
                strFieldName = "FirstPayDate";
                break;
            case 59:
                strFieldName = "CValiDate";
                break;
            case 60:
                strFieldName = "InputOperator";
                break;
            case 61:
                strFieldName = "InputDate";
                break;
            case 62:
                strFieldName = "InputTime";
                break;
            case 63:
                strFieldName = "ApproveFlag";
                break;
            case 64:
                strFieldName = "ApproveCode";
                break;
            case 65:
                strFieldName = "ApproveDate";
                break;
            case 66:
                strFieldName = "ApproveTime";
                break;
            case 67:
                strFieldName = "UWFlag";
                break;
            case 68:
                strFieldName = "UWOperator";
                break;
            case 69:
                strFieldName = "UWDate";
                break;
            case 70:
                strFieldName = "UWTime";
                break;
            case 71:
                strFieldName = "AppFlag";
                break;
            case 72:
                strFieldName = "PolApplyDate";
                break;
            case 73:
                strFieldName = "GetPolDate";
                break;
            case 74:
                strFieldName = "GetPolTime";
                break;
            case 75:
                strFieldName = "CustomGetPolDate";
                break;
            case 76:
                strFieldName = "State";
                break;
            case 77:
                strFieldName = "Operator";
                break;
            case 78:
                strFieldName = "MakeDate";
                break;
            case 79:
                strFieldName = "MakeTime";
                break;
            case 80:
                strFieldName = "ModifyDate";
                break;
            case 81:
                strFieldName = "ModifyTime";
                break;
            case 82:
                strFieldName = "FirstTrialOperator";
                break;
            case 83:
                strFieldName = "FirstTrialDate";
                break;
            case 84:
                strFieldName = "FirstTrialTime";
                break;
            case 85:
                strFieldName = "ReceiveOperator";
                break;
            case 86:
                strFieldName = "ReceiveDate";
                break;
            case 87:
                strFieldName = "ReceiveTime";
                break;
            case 88:
                strFieldName = "TempFeeNo";
                break;
            case 89:
                strFieldName = "SellType";
                break;
            case 90:
                strFieldName = "ForceUWFlag";
                break;
            case 91:
                strFieldName = "ForceUWReason";
                break;
            case 92:
                strFieldName = "NewBankCode";
                break;
            case 93:
                strFieldName = "NewBankAccNo";
                break;
            case 94:
                strFieldName = "NewAccName";
                break;
            case 95:
                strFieldName = "NewPayMode";
                break;
            case 96:
                strFieldName = "AgentBankCode";
                break;
            case 97:
                strFieldName = "BankAgent";
                break;
            case 98:
                strFieldName = "BankAgentName";
                break;
            case 99:
                strFieldName = "BankAgentTel";
                break;
            case 100:
                strFieldName = "ProdSetCode";
                break;
            case 101:
                strFieldName = "PolicyNo";
                break;
            case 102:
                strFieldName = "BillPressNo";
                break;
            case 103:
                strFieldName = "CardTypeCode";
                break;
            case 104:
                strFieldName = "VisitDate";
                break;
            case 105:
                strFieldName = "VisitTime";
                break;
            case 106:
                strFieldName = "SaleCom";
                break;
            case 107:
                strFieldName = "PrintFlag";
                break;
            case 108:
                strFieldName = "InvoicePrtFlag";
                break;
            case 109:
                strFieldName = "NewReinsureFlag";
                break;
            case 110:
                strFieldName = "RenewPayFlag";
                break;
            case 111:
                strFieldName = "AppntFirstName";
                break;
            case 112:
                strFieldName = "AppntLastName";
                break;
            case 113:
                strFieldName = "InsuredFirstName";
                break;
            case 114:
                strFieldName = "InsuredLastName";
                break;
            case 115:
                strFieldName = "AuthorFlag";
                break;
            case 116:
                strFieldName = "GreenChnl";
                break;
            case 117:
                strFieldName = "TBType";
                break;
            case 118:
                strFieldName = "EAuto";
                break;
            case 119:
                strFieldName = "SlipForm";
                break;
            case 120:
                strFieldName = "AutoPayFlag";
                break;
            case 121:
                strFieldName = "RnewFlag";
                break;
            case 122:
                strFieldName = "FamilyContNo";
                break;
            case 123:
                strFieldName = "BussFlag";
                break;
            case 124:
                strFieldName = "SignName";
                break;
            case 125:
                strFieldName = "OrganizeDate";
                break;
            case 126:
                strFieldName = "OrganizeTime";
                break;
            case 127:
                strFieldName = "NewAutoSendBankFlag";
                break;
            case 128:
                strFieldName = "AgentCodeOper";
                break;
            case 129:
                strFieldName = "AgentCodeAssi";
                break;
            case 130:
                strFieldName = "DelayReasonCode";
                break;
            case 131:
                strFieldName = "DelayReasonDesc";
                break;
            case 132:
                strFieldName = "XQremindflag";
                break;
            case 133:
                strFieldName = "OrganComCode";
                break;
            case 134:
                strFieldName = "BankProivnce";
                break;
            case 135:
                strFieldName = "NewBankProivnce";
                break;
            case 136:
                strFieldName = "ArbitrationCom";
                break;
            case 137:
                strFieldName = "OtherPrtno";
                break;
            case 138:
                strFieldName = "DestAccountDate";
                break;
            case 139:
                strFieldName = "PrePayCount";
                break;
            case 140:
                strFieldName = "NewBankCity";
                break;
            case 141:
                strFieldName = "InsuredBankCode";
                break;
            case 142:
                strFieldName = "InsuredBankAccNo";
                break;
            case 143:
                strFieldName = "InsuredAccName";
                break;
            case 144:
                strFieldName = "InsuredBankProvince";
                break;
            case 145:
                strFieldName = "InsuredBankCity";
                break;
            case 146:
                strFieldName = "NewAccType";
                break;
            case 147:
                strFieldName = "AccType";
                break;
            case 148:
                strFieldName = "BankCity";
                break;
            case 149:
                strFieldName = "EstimateMoney";
                break;
            case 150:
                strFieldName = "InsuredAccType";
                break;
            case 151:
                strFieldName = "IsAllowedCharges";
                break;
            case 152:
                strFieldName = "WTEdorAcceptNo";
                break;
            case 153:
                strFieldName = "Recording";
                break;
            case 154:
                strFieldName = "SaleChannels";
                break;
            case 155:
                strFieldName = "ZJAgentCom";
                break;
            case 156:
                strFieldName = "ZJAgentComName";
                break;
            case 157:
                strFieldName = "RecordFlag";
                break;
            case 158:
                strFieldName = "PlanCode";
                break;
            case 159:
                strFieldName = "ReceiptNo";
                break;
            case 160:
                strFieldName = "ReissueCont";
                break;
            case 161:
                strFieldName = "DomainCode";
                break;
            case 162:
                strFieldName = "DomainName";
                break;
            case 163:
                strFieldName = "ReferralNo";
                break;
            case 164:
                strFieldName = "ThirdPartyOrderId";
                break;
            case 165:
                strFieldName = "UserId";
                break;
            case 166:
                strFieldName = "SettlementNumber";
                break;
            case 167:
                strFieldName = "SettlementDate";
                break;
            case 168:
                strFieldName = "SettlementStatus";
                break;
            case 169:
                strFieldName = "ProductCode";
                break;
            case 170:
                strFieldName = "EndDate";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "CONTID":
                return Schema.TYPE_LONG;
            case "SHARDINGID":
                return Schema.TYPE_STRING;
            case "GRPCONTNO":
                return Schema.TYPE_STRING;
            case "CONTNO":
                return Schema.TYPE_STRING;
            case "PROPOSALCONTNO":
                return Schema.TYPE_STRING;
            case "PRTNO":
                return Schema.TYPE_STRING;
            case "CONTTYPE":
                return Schema.TYPE_STRING;
            case "FAMILYTYPE":
                return Schema.TYPE_STRING;
            case "FAMILYID":
                return Schema.TYPE_STRING;
            case "POLTYPE":
                return Schema.TYPE_STRING;
            case "CARDFLAG":
                return Schema.TYPE_STRING;
            case "MANAGECOM":
                return Schema.TYPE_STRING;
            case "EXECUTECOM":
                return Schema.TYPE_STRING;
            case "AGENTCOM":
                return Schema.TYPE_STRING;
            case "AGENTCODE":
                return Schema.TYPE_STRING;
            case "AGENTGROUP":
                return Schema.TYPE_STRING;
            case "AGENTCODE1":
                return Schema.TYPE_STRING;
            case "AGENTTYPE":
                return Schema.TYPE_STRING;
            case "SALECHNL":
                return Schema.TYPE_STRING;
            case "HANDLER":
                return Schema.TYPE_STRING;
            case "PASSWORD":
                return Schema.TYPE_STRING;
            case "APPNTNO":
                return Schema.TYPE_STRING;
            case "APPNTNAME":
                return Schema.TYPE_STRING;
            case "APPNTSEX":
                return Schema.TYPE_STRING;
            case "APPNTBIRTHDAY":
                return Schema.TYPE_STRING;
            case "APPNTIDTYPE":
                return Schema.TYPE_STRING;
            case "APPNTIDNO":
                return Schema.TYPE_STRING;
            case "INSUREDNO":
                return Schema.TYPE_STRING;
            case "INSUREDNAME":
                return Schema.TYPE_STRING;
            case "INSUREDSEX":
                return Schema.TYPE_STRING;
            case "INSUREDBIRTHDAY":
                return Schema.TYPE_STRING;
            case "INSUREDIDTYPE":
                return Schema.TYPE_STRING;
            case "INSUREDIDNO":
                return Schema.TYPE_STRING;
            case "PAYINTV":
                return Schema.TYPE_INT;
            case "PAYMODE":
                return Schema.TYPE_STRING;
            case "PAYLOCATION":
                return Schema.TYPE_STRING;
            case "DISPUTEDFLAG":
                return Schema.TYPE_STRING;
            case "OUTPAYFLAG":
                return Schema.TYPE_STRING;
            case "GETPOLMODE":
                return Schema.TYPE_STRING;
            case "SIGNCOM":
                return Schema.TYPE_STRING;
            case "SIGNDATE":
                return Schema.TYPE_STRING;
            case "SIGNTIME":
                return Schema.TYPE_STRING;
            case "CONSIGNNO":
                return Schema.TYPE_STRING;
            case "BANKCODE":
                return Schema.TYPE_STRING;
            case "BANKACCNO":
                return Schema.TYPE_STRING;
            case "ACCNAME":
                return Schema.TYPE_STRING;
            case "PRINTCOUNT":
                return Schema.TYPE_INT;
            case "LOSTTIMES":
                return Schema.TYPE_INT;
            case "LANG":
                return Schema.TYPE_STRING;
            case "CURRENCY":
                return Schema.TYPE_STRING;
            case "REMARK":
                return Schema.TYPE_STRING;
            case "PEOPLES":
                return Schema.TYPE_INT;
            case "MULT":
                return Schema.TYPE_DOUBLE;
            case "PREM":
                return Schema.TYPE_DOUBLE;
            case "AMNT":
                return Schema.TYPE_DOUBLE;
            case "SUMPREM":
                return Schema.TYPE_DOUBLE;
            case "DIF":
                return Schema.TYPE_DOUBLE;
            case "PAYTODATE":
                return Schema.TYPE_STRING;
            case "FIRSTPAYDATE":
                return Schema.TYPE_STRING;
            case "CVALIDATE":
                return Schema.TYPE_STRING;
            case "INPUTOPERATOR":
                return Schema.TYPE_STRING;
            case "INPUTDATE":
                return Schema.TYPE_STRING;
            case "INPUTTIME":
                return Schema.TYPE_STRING;
            case "APPROVEFLAG":
                return Schema.TYPE_STRING;
            case "APPROVECODE":
                return Schema.TYPE_STRING;
            case "APPROVEDATE":
                return Schema.TYPE_STRING;
            case "APPROVETIME":
                return Schema.TYPE_STRING;
            case "UWFLAG":
                return Schema.TYPE_STRING;
            case "UWOPERATOR":
                return Schema.TYPE_STRING;
            case "UWDATE":
                return Schema.TYPE_STRING;
            case "UWTIME":
                return Schema.TYPE_STRING;
            case "APPFLAG":
                return Schema.TYPE_STRING;
            case "POLAPPLYDATE":
                return Schema.TYPE_STRING;
            case "GETPOLDATE":
                return Schema.TYPE_STRING;
            case "GETPOLTIME":
                return Schema.TYPE_STRING;
            case "CUSTOMGETPOLDATE":
                return Schema.TYPE_STRING;
            case "STATE":
                return Schema.TYPE_STRING;
            case "OPERATOR":
                return Schema.TYPE_STRING;
            case "MAKEDATE":
                return Schema.TYPE_STRING;
            case "MAKETIME":
                return Schema.TYPE_STRING;
            case "MODIFYDATE":
                return Schema.TYPE_STRING;
            case "MODIFYTIME":
                return Schema.TYPE_STRING;
            case "FIRSTTRIALOPERATOR":
                return Schema.TYPE_STRING;
            case "FIRSTTRIALDATE":
                return Schema.TYPE_STRING;
            case "FIRSTTRIALTIME":
                return Schema.TYPE_STRING;
            case "RECEIVEOPERATOR":
                return Schema.TYPE_STRING;
            case "RECEIVEDATE":
                return Schema.TYPE_STRING;
            case "RECEIVETIME":
                return Schema.TYPE_STRING;
            case "TEMPFEENO":
                return Schema.TYPE_STRING;
            case "SELLTYPE":
                return Schema.TYPE_STRING;
            case "FORCEUWFLAG":
                return Schema.TYPE_STRING;
            case "FORCEUWREASON":
                return Schema.TYPE_STRING;
            case "NEWBANKCODE":
                return Schema.TYPE_STRING;
            case "NEWBANKACCNO":
                return Schema.TYPE_STRING;
            case "NEWACCNAME":
                return Schema.TYPE_STRING;
            case "NEWPAYMODE":
                return Schema.TYPE_STRING;
            case "AGENTBANKCODE":
                return Schema.TYPE_STRING;
            case "BANKAGENT":
                return Schema.TYPE_STRING;
            case "BANKAGENTNAME":
                return Schema.TYPE_STRING;
            case "BANKAGENTTEL":
                return Schema.TYPE_STRING;
            case "PRODSETCODE":
                return Schema.TYPE_STRING;
            case "POLICYNO":
                return Schema.TYPE_STRING;
            case "BILLPRESSNO":
                return Schema.TYPE_STRING;
            case "CARDTYPECODE":
                return Schema.TYPE_STRING;
            case "VISITDATE":
                return Schema.TYPE_STRING;
            case "VISITTIME":
                return Schema.TYPE_STRING;
            case "SALECOM":
                return Schema.TYPE_STRING;
            case "PRINTFLAG":
                return Schema.TYPE_STRING;
            case "INVOICEPRTFLAG":
                return Schema.TYPE_STRING;
            case "NEWREINSUREFLAG":
                return Schema.TYPE_STRING;
            case "RENEWPAYFLAG":
                return Schema.TYPE_STRING;
            case "APPNTFIRSTNAME":
                return Schema.TYPE_STRING;
            case "APPNTLASTNAME":
                return Schema.TYPE_STRING;
            case "INSUREDFIRSTNAME":
                return Schema.TYPE_STRING;
            case "INSUREDLASTNAME":
                return Schema.TYPE_STRING;
            case "AUTHORFLAG":
                return Schema.TYPE_STRING;
            case "GREENCHNL":
                return Schema.TYPE_INT;
            case "TBTYPE":
                return Schema.TYPE_STRING;
            case "EAUTO":
                return Schema.TYPE_STRING;
            case "SLIPFORM":
                return Schema.TYPE_STRING;
            case "AUTOPAYFLAG":
                return Schema.TYPE_STRING;
            case "RNEWFLAG":
                return Schema.TYPE_INT;
            case "FAMILYCONTNO":
                return Schema.TYPE_STRING;
            case "BUSSFLAG":
                return Schema.TYPE_STRING;
            case "SIGNNAME":
                return Schema.TYPE_STRING;
            case "ORGANIZEDATE":
                return Schema.TYPE_STRING;
            case "ORGANIZETIME":
                return Schema.TYPE_STRING;
            case "NEWAUTOSENDBANKFLAG":
                return Schema.TYPE_STRING;
            case "AGENTCODEOPER":
                return Schema.TYPE_STRING;
            case "AGENTCODEASSI":
                return Schema.TYPE_STRING;
            case "DELAYREASONCODE":
                return Schema.TYPE_STRING;
            case "DELAYREASONDESC":
                return Schema.TYPE_STRING;
            case "XQREMINDFLAG":
                return Schema.TYPE_STRING;
            case "ORGANCOMCODE":
                return Schema.TYPE_STRING;
            case "BANKPROIVNCE":
                return Schema.TYPE_STRING;
            case "NEWBANKPROIVNCE":
                return Schema.TYPE_STRING;
            case "ARBITRATIONCOM":
                return Schema.TYPE_STRING;
            case "OTHERPRTNO":
                return Schema.TYPE_STRING;
            case "DESTACCOUNTDATE":
                return Schema.TYPE_STRING;
            case "PREPAYCOUNT":
                return Schema.TYPE_STRING;
            case "NEWBANKCITY":
                return Schema.TYPE_STRING;
            case "INSUREDBANKCODE":
                return Schema.TYPE_STRING;
            case "INSUREDBANKACCNO":
                return Schema.TYPE_STRING;
            case "INSUREDACCNAME":
                return Schema.TYPE_STRING;
            case "INSUREDBANKPROVINCE":
                return Schema.TYPE_STRING;
            case "INSUREDBANKCITY":
                return Schema.TYPE_STRING;
            case "NEWACCTYPE":
                return Schema.TYPE_STRING;
            case "ACCTYPE":
                return Schema.TYPE_STRING;
            case "BANKCITY":
                return Schema.TYPE_STRING;
            case "ESTIMATEMONEY":
                return Schema.TYPE_DOUBLE;
            case "INSUREDACCTYPE":
                return Schema.TYPE_STRING;
            case "ISALLOWEDCHARGES":
                return Schema.TYPE_STRING;
            case "WTEDORACCEPTNO":
                return Schema.TYPE_STRING;
            case "RECORDING":
                return Schema.TYPE_STRING;
            case "SALECHANNELS":
                return Schema.TYPE_STRING;
            case "ZJAGENTCOM":
                return Schema.TYPE_STRING;
            case "ZJAGENTCOMNAME":
                return Schema.TYPE_STRING;
            case "RECORDFLAG":
                return Schema.TYPE_STRING;
            case "PLANCODE":
                return Schema.TYPE_STRING;
            case "RECEIPTNO":
                return Schema.TYPE_STRING;
            case "REISSUECONT":
                return Schema.TYPE_STRING;
            case "DOMAINCODE":
                return Schema.TYPE_STRING;
            case "DOMAINNAME":
                return Schema.TYPE_STRING;
            case "REFERRALNO":
                return Schema.TYPE_STRING;
            case "THIRDPARTYORDERID":
                return Schema.TYPE_STRING;
            case "USERID":
                return Schema.TYPE_STRING;
            case "SETTLEMENTNUMBER":
                return Schema.TYPE_STRING;
            case "SETTLEMENTDATE":
                return Schema.TYPE_STRING;
            case "SETTLEMENTSTATUS":
                return Schema.TYPE_STRING;
            case "PRODUCTCODE":
                return Schema.TYPE_STRING;
            case "ENDDATE":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_LONG;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_STRING;
            case 6:
                return Schema.TYPE_STRING;
            case 7:
                return Schema.TYPE_STRING;
            case 8:
                return Schema.TYPE_STRING;
            case 9:
                return Schema.TYPE_STRING;
            case 10:
                return Schema.TYPE_STRING;
            case 11:
                return Schema.TYPE_STRING;
            case 12:
                return Schema.TYPE_STRING;
            case 13:
                return Schema.TYPE_STRING;
            case 14:
                return Schema.TYPE_STRING;
            case 15:
                return Schema.TYPE_STRING;
            case 16:
                return Schema.TYPE_STRING;
            case 17:
                return Schema.TYPE_STRING;
            case 18:
                return Schema.TYPE_STRING;
            case 19:
                return Schema.TYPE_STRING;
            case 20:
                return Schema.TYPE_STRING;
            case 21:
                return Schema.TYPE_STRING;
            case 22:
                return Schema.TYPE_STRING;
            case 23:
                return Schema.TYPE_STRING;
            case 24:
                return Schema.TYPE_STRING;
            case 25:
                return Schema.TYPE_STRING;
            case 26:
                return Schema.TYPE_STRING;
            case 27:
                return Schema.TYPE_STRING;
            case 28:
                return Schema.TYPE_STRING;
            case 29:
                return Schema.TYPE_STRING;
            case 30:
                return Schema.TYPE_STRING;
            case 31:
                return Schema.TYPE_STRING;
            case 32:
                return Schema.TYPE_STRING;
            case 33:
                return Schema.TYPE_INT;
            case 34:
                return Schema.TYPE_STRING;
            case 35:
                return Schema.TYPE_STRING;
            case 36:
                return Schema.TYPE_STRING;
            case 37:
                return Schema.TYPE_STRING;
            case 38:
                return Schema.TYPE_STRING;
            case 39:
                return Schema.TYPE_STRING;
            case 40:
                return Schema.TYPE_STRING;
            case 41:
                return Schema.TYPE_STRING;
            case 42:
                return Schema.TYPE_STRING;
            case 43:
                return Schema.TYPE_STRING;
            case 44:
                return Schema.TYPE_STRING;
            case 45:
                return Schema.TYPE_STRING;
            case 46:
                return Schema.TYPE_INT;
            case 47:
                return Schema.TYPE_INT;
            case 48:
                return Schema.TYPE_STRING;
            case 49:
                return Schema.TYPE_STRING;
            case 50:
                return Schema.TYPE_STRING;
            case 51:
                return Schema.TYPE_INT;
            case 52:
                return Schema.TYPE_DOUBLE;
            case 53:
                return Schema.TYPE_DOUBLE;
            case 54:
                return Schema.TYPE_DOUBLE;
            case 55:
                return Schema.TYPE_DOUBLE;
            case 56:
                return Schema.TYPE_DOUBLE;
            case 57:
                return Schema.TYPE_STRING;
            case 58:
                return Schema.TYPE_STRING;
            case 59:
                return Schema.TYPE_STRING;
            case 60:
                return Schema.TYPE_STRING;
            case 61:
                return Schema.TYPE_STRING;
            case 62:
                return Schema.TYPE_STRING;
            case 63:
                return Schema.TYPE_STRING;
            case 64:
                return Schema.TYPE_STRING;
            case 65:
                return Schema.TYPE_STRING;
            case 66:
                return Schema.TYPE_STRING;
            case 67:
                return Schema.TYPE_STRING;
            case 68:
                return Schema.TYPE_STRING;
            case 69:
                return Schema.TYPE_STRING;
            case 70:
                return Schema.TYPE_STRING;
            case 71:
                return Schema.TYPE_STRING;
            case 72:
                return Schema.TYPE_STRING;
            case 73:
                return Schema.TYPE_STRING;
            case 74:
                return Schema.TYPE_STRING;
            case 75:
                return Schema.TYPE_STRING;
            case 76:
                return Schema.TYPE_STRING;
            case 77:
                return Schema.TYPE_STRING;
            case 78:
                return Schema.TYPE_STRING;
            case 79:
                return Schema.TYPE_STRING;
            case 80:
                return Schema.TYPE_STRING;
            case 81:
                return Schema.TYPE_STRING;
            case 82:
                return Schema.TYPE_STRING;
            case 83:
                return Schema.TYPE_STRING;
            case 84:
                return Schema.TYPE_STRING;
            case 85:
                return Schema.TYPE_STRING;
            case 86:
                return Schema.TYPE_STRING;
            case 87:
                return Schema.TYPE_STRING;
            case 88:
                return Schema.TYPE_STRING;
            case 89:
                return Schema.TYPE_STRING;
            case 90:
                return Schema.TYPE_STRING;
            case 91:
                return Schema.TYPE_STRING;
            case 92:
                return Schema.TYPE_STRING;
            case 93:
                return Schema.TYPE_STRING;
            case 94:
                return Schema.TYPE_STRING;
            case 95:
                return Schema.TYPE_STRING;
            case 96:
                return Schema.TYPE_STRING;
            case 97:
                return Schema.TYPE_STRING;
            case 98:
                return Schema.TYPE_STRING;
            case 99:
                return Schema.TYPE_STRING;
            case 100:
                return Schema.TYPE_STRING;
            case 101:
                return Schema.TYPE_STRING;
            case 102:
                return Schema.TYPE_STRING;
            case 103:
                return Schema.TYPE_STRING;
            case 104:
                return Schema.TYPE_STRING;
            case 105:
                return Schema.TYPE_STRING;
            case 106:
                return Schema.TYPE_STRING;
            case 107:
                return Schema.TYPE_STRING;
            case 108:
                return Schema.TYPE_STRING;
            case 109:
                return Schema.TYPE_STRING;
            case 110:
                return Schema.TYPE_STRING;
            case 111:
                return Schema.TYPE_STRING;
            case 112:
                return Schema.TYPE_STRING;
            case 113:
                return Schema.TYPE_STRING;
            case 114:
                return Schema.TYPE_STRING;
            case 115:
                return Schema.TYPE_STRING;
            case 116:
                return Schema.TYPE_INT;
            case 117:
                return Schema.TYPE_STRING;
            case 118:
                return Schema.TYPE_STRING;
            case 119:
                return Schema.TYPE_STRING;
            case 120:
                return Schema.TYPE_STRING;
            case 121:
                return Schema.TYPE_INT;
            case 122:
                return Schema.TYPE_STRING;
            case 123:
                return Schema.TYPE_STRING;
            case 124:
                return Schema.TYPE_STRING;
            case 125:
                return Schema.TYPE_STRING;
            case 126:
                return Schema.TYPE_STRING;
            case 127:
                return Schema.TYPE_STRING;
            case 128:
                return Schema.TYPE_STRING;
            case 129:
                return Schema.TYPE_STRING;
            case 130:
                return Schema.TYPE_STRING;
            case 131:
                return Schema.TYPE_STRING;
            case 132:
                return Schema.TYPE_STRING;
            case 133:
                return Schema.TYPE_STRING;
            case 134:
                return Schema.TYPE_STRING;
            case 135:
                return Schema.TYPE_STRING;
            case 136:
                return Schema.TYPE_STRING;
            case 137:
                return Schema.TYPE_STRING;
            case 138:
                return Schema.TYPE_STRING;
            case 139:
                return Schema.TYPE_STRING;
            case 140:
                return Schema.TYPE_STRING;
            case 141:
                return Schema.TYPE_STRING;
            case 142:
                return Schema.TYPE_STRING;
            case 143:
                return Schema.TYPE_STRING;
            case 144:
                return Schema.TYPE_STRING;
            case 145:
                return Schema.TYPE_STRING;
            case 146:
                return Schema.TYPE_STRING;
            case 147:
                return Schema.TYPE_STRING;
            case 148:
                return Schema.TYPE_STRING;
            case 149:
                return Schema.TYPE_DOUBLE;
            case 150:
                return Schema.TYPE_STRING;
            case 151:
                return Schema.TYPE_STRING;
            case 152:
                return Schema.TYPE_STRING;
            case 153:
                return Schema.TYPE_STRING;
            case 154:
                return Schema.TYPE_STRING;
            case 155:
                return Schema.TYPE_STRING;
            case 156:
                return Schema.TYPE_STRING;
            case 157:
                return Schema.TYPE_STRING;
            case 158:
                return Schema.TYPE_STRING;
            case 159:
                return Schema.TYPE_STRING;
            case 160:
                return Schema.TYPE_STRING;
            case 161:
                return Schema.TYPE_STRING;
            case 162:
                return Schema.TYPE_STRING;
            case 163:
                return Schema.TYPE_STRING;
            case 164:
                return Schema.TYPE_STRING;
            case 165:
                return Schema.TYPE_STRING;
            case 166:
                return Schema.TYPE_STRING;
            case 167:
                return Schema.TYPE_STRING;
            case 168:
                return Schema.TYPE_STRING;
            case 169:
                return Schema.TYPE_STRING;
            case 170:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("ContID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ContID));
        }
        if (FCode.equalsIgnoreCase("ShardingID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ShardingID));
        }
        if (FCode.equalsIgnoreCase("GrpContNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GrpContNo));
        }
        if (FCode.equalsIgnoreCase("ContNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ContNo));
        }
        if (FCode.equalsIgnoreCase("ProposalContNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ProposalContNo));
        }
        if (FCode.equalsIgnoreCase("PrtNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PrtNo));
        }
        if (FCode.equalsIgnoreCase("ContType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ContType));
        }
        if (FCode.equalsIgnoreCase("FamilyType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FamilyType));
        }
        if (FCode.equalsIgnoreCase("FamilyID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FamilyID));
        }
        if (FCode.equalsIgnoreCase("PolType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PolType));
        }
        if (FCode.equalsIgnoreCase("CardFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CardFlag));
        }
        if (FCode.equalsIgnoreCase("ManageCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
        }
        if (FCode.equalsIgnoreCase("ExecuteCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ExecuteCom));
        }
        if (FCode.equalsIgnoreCase("AgentCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentCom));
        }
        if (FCode.equalsIgnoreCase("AgentCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentCode));
        }
        if (FCode.equalsIgnoreCase("AgentGroup")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentGroup));
        }
        if (FCode.equalsIgnoreCase("AgentCode1")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentCode1));
        }
        if (FCode.equalsIgnoreCase("AgentType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentType));
        }
        if (FCode.equalsIgnoreCase("SaleChnl")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SaleChnl));
        }
        if (FCode.equalsIgnoreCase("Handler")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Handler));
        }
        if (FCode.equalsIgnoreCase("Password")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Password));
        }
        if (FCode.equalsIgnoreCase("AppntNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AppntNo));
        }
        if (FCode.equalsIgnoreCase("AppntName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AppntName));
        }
        if (FCode.equalsIgnoreCase("AppntSex")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AppntSex));
        }
        if (FCode.equalsIgnoreCase("AppntBirthday")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AppntBirthday));
        }
        if (FCode.equalsIgnoreCase("AppntIDType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AppntIDType));
        }
        if (FCode.equalsIgnoreCase("AppntIDNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AppntIDNo));
        }
        if (FCode.equalsIgnoreCase("InsuredNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InsuredNo));
        }
        if (FCode.equalsIgnoreCase("InsuredName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InsuredName));
        }
        if (FCode.equalsIgnoreCase("InsuredSex")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InsuredSex));
        }
        if (FCode.equalsIgnoreCase("InsuredBirthday")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InsuredBirthday));
        }
        if (FCode.equalsIgnoreCase("InsuredIDType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InsuredIDType));
        }
        if (FCode.equalsIgnoreCase("InsuredIDNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InsuredIDNo));
        }
        if (FCode.equalsIgnoreCase("PayIntv")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PayIntv));
        }
        if (FCode.equalsIgnoreCase("PayMode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PayMode));
        }
        if (FCode.equalsIgnoreCase("PayLocation")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PayLocation));
        }
        if (FCode.equalsIgnoreCase("DisputedFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DisputedFlag));
        }
        if (FCode.equalsIgnoreCase("OutPayFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OutPayFlag));
        }
        if (FCode.equalsIgnoreCase("GetPolMode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GetPolMode));
        }
        if (FCode.equalsIgnoreCase("SignCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SignCom));
        }
        if (FCode.equalsIgnoreCase("SignDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SignDate));
        }
        if (FCode.equalsIgnoreCase("SignTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SignTime));
        }
        if (FCode.equalsIgnoreCase("ConsignNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ConsignNo));
        }
        if (FCode.equalsIgnoreCase("BankCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BankCode));
        }
        if (FCode.equalsIgnoreCase("BankAccNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BankAccNo));
        }
        if (FCode.equalsIgnoreCase("AccName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AccName));
        }
        if (FCode.equalsIgnoreCase("PrintCount")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PrintCount));
        }
        if (FCode.equalsIgnoreCase("LostTimes")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(LostTimes));
        }
        if (FCode.equalsIgnoreCase("Lang")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Lang));
        }
        if (FCode.equalsIgnoreCase("Currency")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Currency));
        }
        if (FCode.equalsIgnoreCase("Remark")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Remark));
        }
        if (FCode.equalsIgnoreCase("Peoples")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Peoples));
        }
        if (FCode.equalsIgnoreCase("Mult")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Mult));
        }
        if (FCode.equalsIgnoreCase("Prem")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Prem));
        }
        if (FCode.equalsIgnoreCase("Amnt")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Amnt));
        }
        if (FCode.equalsIgnoreCase("SumPrem")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SumPrem));
        }
        if (FCode.equalsIgnoreCase("Dif")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Dif));
        }
        if (FCode.equalsIgnoreCase("PaytoDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PaytoDate));
        }
        if (FCode.equalsIgnoreCase("FirstPayDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FirstPayDate));
        }
        if (FCode.equalsIgnoreCase("CValiDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CValiDate));
        }
        if (FCode.equalsIgnoreCase("InputOperator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InputOperator));
        }
        if (FCode.equalsIgnoreCase("InputDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InputDate));
        }
        if (FCode.equalsIgnoreCase("InputTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InputTime));
        }
        if (FCode.equalsIgnoreCase("ApproveFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ApproveFlag));
        }
        if (FCode.equalsIgnoreCase("ApproveCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ApproveCode));
        }
        if (FCode.equalsIgnoreCase("ApproveDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ApproveDate));
        }
        if (FCode.equalsIgnoreCase("ApproveTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ApproveTime));
        }
        if (FCode.equalsIgnoreCase("UWFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(UWFlag));
        }
        if (FCode.equalsIgnoreCase("UWOperator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(UWOperator));
        }
        if (FCode.equalsIgnoreCase("UWDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(UWDate));
        }
        if (FCode.equalsIgnoreCase("UWTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(UWTime));
        }
        if (FCode.equalsIgnoreCase("AppFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AppFlag));
        }
        if (FCode.equalsIgnoreCase("PolApplyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PolApplyDate));
        }
        if (FCode.equalsIgnoreCase("GetPolDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GetPolDate));
        }
        if (FCode.equalsIgnoreCase("GetPolTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GetPolTime));
        }
        if (FCode.equalsIgnoreCase("CustomGetPolDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CustomGetPolDate));
        }
        if (FCode.equalsIgnoreCase("State")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(State));
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeDate));
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyDate));
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (FCode.equalsIgnoreCase("FirstTrialOperator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FirstTrialOperator));
        }
        if (FCode.equalsIgnoreCase("FirstTrialDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FirstTrialDate));
        }
        if (FCode.equalsIgnoreCase("FirstTrialTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FirstTrialTime));
        }
        if (FCode.equalsIgnoreCase("ReceiveOperator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ReceiveOperator));
        }
        if (FCode.equalsIgnoreCase("ReceiveDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ReceiveDate));
        }
        if (FCode.equalsIgnoreCase("ReceiveTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ReceiveTime));
        }
        if (FCode.equalsIgnoreCase("TempFeeNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(TempFeeNo));
        }
        if (FCode.equalsIgnoreCase("SellType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SellType));
        }
        if (FCode.equalsIgnoreCase("ForceUWFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ForceUWFlag));
        }
        if (FCode.equalsIgnoreCase("ForceUWReason")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ForceUWReason));
        }
        if (FCode.equalsIgnoreCase("NewBankCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(NewBankCode));
        }
        if (FCode.equalsIgnoreCase("NewBankAccNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(NewBankAccNo));
        }
        if (FCode.equalsIgnoreCase("NewAccName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(NewAccName));
        }
        if (FCode.equalsIgnoreCase("NewPayMode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(NewPayMode));
        }
        if (FCode.equalsIgnoreCase("AgentBankCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentBankCode));
        }
        if (FCode.equalsIgnoreCase("BankAgent")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BankAgent));
        }
        if (FCode.equalsIgnoreCase("BankAgentName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BankAgentName));
        }
        if (FCode.equalsIgnoreCase("BankAgentTel")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BankAgentTel));
        }
        if (FCode.equalsIgnoreCase("ProdSetCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ProdSetCode));
        }
        if (FCode.equalsIgnoreCase("PolicyNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PolicyNo));
        }
        if (FCode.equalsIgnoreCase("BillPressNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BillPressNo));
        }
        if (FCode.equalsIgnoreCase("CardTypeCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CardTypeCode));
        }
        if (FCode.equalsIgnoreCase("VisitDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(VisitDate));
        }
        if (FCode.equalsIgnoreCase("VisitTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(VisitTime));
        }
        if (FCode.equalsIgnoreCase("SaleCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SaleCom));
        }
        if (FCode.equalsIgnoreCase("PrintFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PrintFlag));
        }
        if (FCode.equalsIgnoreCase("InvoicePrtFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InvoicePrtFlag));
        }
        if (FCode.equalsIgnoreCase("NewReinsureFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(NewReinsureFlag));
        }
        if (FCode.equalsIgnoreCase("RenewPayFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RenewPayFlag));
        }
        if (FCode.equalsIgnoreCase("AppntFirstName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AppntFirstName));
        }
        if (FCode.equalsIgnoreCase("AppntLastName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AppntLastName));
        }
        if (FCode.equalsIgnoreCase("InsuredFirstName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InsuredFirstName));
        }
        if (FCode.equalsIgnoreCase("InsuredLastName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InsuredLastName));
        }
        if (FCode.equalsIgnoreCase("AuthorFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AuthorFlag));
        }
        if (FCode.equalsIgnoreCase("GreenChnl")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GreenChnl));
        }
        if (FCode.equalsIgnoreCase("TBType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(TBType));
        }
        if (FCode.equalsIgnoreCase("EAuto")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(EAuto));
        }
        if (FCode.equalsIgnoreCase("SlipForm")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SlipForm));
        }
        if (FCode.equalsIgnoreCase("AutoPayFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AutoPayFlag));
        }
        if (FCode.equalsIgnoreCase("RnewFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RnewFlag));
        }
        if (FCode.equalsIgnoreCase("FamilyContNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FamilyContNo));
        }
        if (FCode.equalsIgnoreCase("BussFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BussFlag));
        }
        if (FCode.equalsIgnoreCase("SignName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SignName));
        }
        if (FCode.equalsIgnoreCase("OrganizeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OrganizeDate));
        }
        if (FCode.equalsIgnoreCase("OrganizeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OrganizeTime));
        }
        if (FCode.equalsIgnoreCase("NewAutoSendBankFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(NewAutoSendBankFlag));
        }
        if (FCode.equalsIgnoreCase("AgentCodeOper")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentCodeOper));
        }
        if (FCode.equalsIgnoreCase("AgentCodeAssi")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentCodeAssi));
        }
        if (FCode.equalsIgnoreCase("DelayReasonCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DelayReasonCode));
        }
        if (FCode.equalsIgnoreCase("DelayReasonDesc")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DelayReasonDesc));
        }
        if (FCode.equalsIgnoreCase("XQremindflag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(XQremindflag));
        }
        if (FCode.equalsIgnoreCase("OrganComCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OrganComCode));
        }
        if (FCode.equalsIgnoreCase("BankProivnce")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BankProivnce));
        }
        if (FCode.equalsIgnoreCase("NewBankProivnce")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(NewBankProivnce));
        }
        if (FCode.equalsIgnoreCase("ArbitrationCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ArbitrationCom));
        }
        if (FCode.equalsIgnoreCase("OtherPrtno")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OtherPrtno));
        }
        if (FCode.equalsIgnoreCase("DestAccountDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DestAccountDate));
        }
        if (FCode.equalsIgnoreCase("PrePayCount")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PrePayCount));
        }
        if (FCode.equalsIgnoreCase("NewBankCity")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(NewBankCity));
        }
        if (FCode.equalsIgnoreCase("InsuredBankCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InsuredBankCode));
        }
        if (FCode.equalsIgnoreCase("InsuredBankAccNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InsuredBankAccNo));
        }
        if (FCode.equalsIgnoreCase("InsuredAccName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InsuredAccName));
        }
        if (FCode.equalsIgnoreCase("InsuredBankProvince")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InsuredBankProvince));
        }
        if (FCode.equalsIgnoreCase("InsuredBankCity")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InsuredBankCity));
        }
        if (FCode.equalsIgnoreCase("NewAccType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(NewAccType));
        }
        if (FCode.equalsIgnoreCase("AccType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AccType));
        }
        if (FCode.equalsIgnoreCase("BankCity")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BankCity));
        }
        if (FCode.equalsIgnoreCase("EstimateMoney")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(EstimateMoney));
        }
        if (FCode.equalsIgnoreCase("InsuredAccType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InsuredAccType));
        }
        if (FCode.equalsIgnoreCase("IsAllowedCharges")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(IsAllowedCharges));
        }
        if (FCode.equalsIgnoreCase("WTEdorAcceptNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(WTEdorAcceptNo));
        }
        if (FCode.equalsIgnoreCase("Recording")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Recording));
        }
        if (FCode.equalsIgnoreCase("SaleChannels")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SaleChannels));
        }
        if (FCode.equalsIgnoreCase("ZJAgentCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ZJAgentCom));
        }
        if (FCode.equalsIgnoreCase("ZJAgentComName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ZJAgentComName));
        }
        if (FCode.equalsIgnoreCase("RecordFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RecordFlag));
        }
        if (FCode.equalsIgnoreCase("PlanCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PlanCode));
        }
        if (FCode.equalsIgnoreCase("ReceiptNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ReceiptNo));
        }
        if (FCode.equalsIgnoreCase("ReissueCont")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ReissueCont));
        }
        if (FCode.equalsIgnoreCase("DomainCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DomainCode));
        }
        if (FCode.equalsIgnoreCase("DomainName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DomainName));
        }
        if (FCode.equalsIgnoreCase("ReferralNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ReferralNo));
        }
        if (FCode.equalsIgnoreCase("ThirdPartyOrderId")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ThirdPartyOrderId));
        }
        if (FCode.equalsIgnoreCase("UserId")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(UserId));
        }
        if (FCode.equalsIgnoreCase("SettlementNumber")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SettlementNumber));
        }
        if (FCode.equalsIgnoreCase("SettlementDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SettlementDate));
        }
        if (FCode.equalsIgnoreCase("SettlementStatus")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SettlementStatus));
        }
        if (FCode.equalsIgnoreCase("ProductCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ProductCode));
        }
        if (FCode.equalsIgnoreCase("EndDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(EndDate));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = String.valueOf(ContID);
                break;
            case 1:
                strFieldValue = String.valueOf(ShardingID);
                break;
            case 2:
                strFieldValue = String.valueOf(GrpContNo);
                break;
            case 3:
                strFieldValue = String.valueOf(ContNo);
                break;
            case 4:
                strFieldValue = String.valueOf(ProposalContNo);
                break;
            case 5:
                strFieldValue = String.valueOf(PrtNo);
                break;
            case 6:
                strFieldValue = String.valueOf(ContType);
                break;
            case 7:
                strFieldValue = String.valueOf(FamilyType);
                break;
            case 8:
                strFieldValue = String.valueOf(FamilyID);
                break;
            case 9:
                strFieldValue = String.valueOf(PolType);
                break;
            case 10:
                strFieldValue = String.valueOf(CardFlag);
                break;
            case 11:
                strFieldValue = String.valueOf(ManageCom);
                break;
            case 12:
                strFieldValue = String.valueOf(ExecuteCom);
                break;
            case 13:
                strFieldValue = String.valueOf(AgentCom);
                break;
            case 14:
                strFieldValue = String.valueOf(AgentCode);
                break;
            case 15:
                strFieldValue = String.valueOf(AgentGroup);
                break;
            case 16:
                strFieldValue = String.valueOf(AgentCode1);
                break;
            case 17:
                strFieldValue = String.valueOf(AgentType);
                break;
            case 18:
                strFieldValue = String.valueOf(SaleChnl);
                break;
            case 19:
                strFieldValue = String.valueOf(Handler);
                break;
            case 20:
                strFieldValue = String.valueOf(Password);
                break;
            case 21:
                strFieldValue = String.valueOf(AppntNo);
                break;
            case 22:
                strFieldValue = String.valueOf(AppntName);
                break;
            case 23:
                strFieldValue = String.valueOf(AppntSex);
                break;
            case 24:
                strFieldValue = String.valueOf(AppntBirthday);
                break;
            case 25:
                strFieldValue = String.valueOf(AppntIDType);
                break;
            case 26:
                strFieldValue = String.valueOf(AppntIDNo);
                break;
            case 27:
                strFieldValue = String.valueOf(InsuredNo);
                break;
            case 28:
                strFieldValue = String.valueOf(InsuredName);
                break;
            case 29:
                strFieldValue = String.valueOf(InsuredSex);
                break;
            case 30:
                strFieldValue = String.valueOf(InsuredBirthday);
                break;
            case 31:
                strFieldValue = String.valueOf(InsuredIDType);
                break;
            case 32:
                strFieldValue = String.valueOf(InsuredIDNo);
                break;
            case 33:
                strFieldValue = String.valueOf(PayIntv);
                break;
            case 34:
                strFieldValue = String.valueOf(PayMode);
                break;
            case 35:
                strFieldValue = String.valueOf(PayLocation);
                break;
            case 36:
                strFieldValue = String.valueOf(DisputedFlag);
                break;
            case 37:
                strFieldValue = String.valueOf(OutPayFlag);
                break;
            case 38:
                strFieldValue = String.valueOf(GetPolMode);
                break;
            case 39:
                strFieldValue = String.valueOf(SignCom);
                break;
            case 40:
                strFieldValue = String.valueOf(SignDate);
                break;
            case 41:
                strFieldValue = String.valueOf(SignTime);
                break;
            case 42:
                strFieldValue = String.valueOf(ConsignNo);
                break;
            case 43:
                strFieldValue = String.valueOf(BankCode);
                break;
            case 44:
                strFieldValue = String.valueOf(BankAccNo);
                break;
            case 45:
                strFieldValue = String.valueOf(AccName);
                break;
            case 46:
                strFieldValue = String.valueOf(PrintCount);
                break;
            case 47:
                strFieldValue = String.valueOf(LostTimes);
                break;
            case 48:
                strFieldValue = String.valueOf(Lang);
                break;
            case 49:
                strFieldValue = String.valueOf(Currency);
                break;
            case 50:
                strFieldValue = String.valueOf(Remark);
                break;
            case 51:
                strFieldValue = String.valueOf(Peoples);
                break;
            case 52:
                strFieldValue = String.valueOf(Mult);
                break;
            case 53:
                strFieldValue = String.valueOf(Prem);
                break;
            case 54:
                strFieldValue = String.valueOf(Amnt);
                break;
            case 55:
                strFieldValue = String.valueOf(SumPrem);
                break;
            case 56:
                strFieldValue = String.valueOf(Dif);
                break;
            case 57:
                strFieldValue = String.valueOf(PaytoDate);
                break;
            case 58:
                strFieldValue = String.valueOf(FirstPayDate);
                break;
            case 59:
                strFieldValue = String.valueOf(CValiDate);
                break;
            case 60:
                strFieldValue = String.valueOf(InputOperator);
                break;
            case 61:
                strFieldValue = String.valueOf(InputDate);
                break;
            case 62:
                strFieldValue = String.valueOf(InputTime);
                break;
            case 63:
                strFieldValue = String.valueOf(ApproveFlag);
                break;
            case 64:
                strFieldValue = String.valueOf(ApproveCode);
                break;
            case 65:
                strFieldValue = String.valueOf(ApproveDate);
                break;
            case 66:
                strFieldValue = String.valueOf(ApproveTime);
                break;
            case 67:
                strFieldValue = String.valueOf(UWFlag);
                break;
            case 68:
                strFieldValue = String.valueOf(UWOperator);
                break;
            case 69:
                strFieldValue = String.valueOf(UWDate);
                break;
            case 70:
                strFieldValue = String.valueOf(UWTime);
                break;
            case 71:
                strFieldValue = String.valueOf(AppFlag);
                break;
            case 72:
                strFieldValue = String.valueOf(PolApplyDate);
                break;
            case 73:
                strFieldValue = String.valueOf(GetPolDate);
                break;
            case 74:
                strFieldValue = String.valueOf(GetPolTime);
                break;
            case 75:
                strFieldValue = String.valueOf(CustomGetPolDate);
                break;
            case 76:
                strFieldValue = String.valueOf(State);
                break;
            case 77:
                strFieldValue = String.valueOf(Operator);
                break;
            case 78:
                strFieldValue = String.valueOf(MakeDate);
                break;
            case 79:
                strFieldValue = String.valueOf(MakeTime);
                break;
            case 80:
                strFieldValue = String.valueOf(ModifyDate);
                break;
            case 81:
                strFieldValue = String.valueOf(ModifyTime);
                break;
            case 82:
                strFieldValue = String.valueOf(FirstTrialOperator);
                break;
            case 83:
                strFieldValue = String.valueOf(FirstTrialDate);
                break;
            case 84:
                strFieldValue = String.valueOf(FirstTrialTime);
                break;
            case 85:
                strFieldValue = String.valueOf(ReceiveOperator);
                break;
            case 86:
                strFieldValue = String.valueOf(ReceiveDate);
                break;
            case 87:
                strFieldValue = String.valueOf(ReceiveTime);
                break;
            case 88:
                strFieldValue = String.valueOf(TempFeeNo);
                break;
            case 89:
                strFieldValue = String.valueOf(SellType);
                break;
            case 90:
                strFieldValue = String.valueOf(ForceUWFlag);
                break;
            case 91:
                strFieldValue = String.valueOf(ForceUWReason);
                break;
            case 92:
                strFieldValue = String.valueOf(NewBankCode);
                break;
            case 93:
                strFieldValue = String.valueOf(NewBankAccNo);
                break;
            case 94:
                strFieldValue = String.valueOf(NewAccName);
                break;
            case 95:
                strFieldValue = String.valueOf(NewPayMode);
                break;
            case 96:
                strFieldValue = String.valueOf(AgentBankCode);
                break;
            case 97:
                strFieldValue = String.valueOf(BankAgent);
                break;
            case 98:
                strFieldValue = String.valueOf(BankAgentName);
                break;
            case 99:
                strFieldValue = String.valueOf(BankAgentTel);
                break;
            case 100:
                strFieldValue = String.valueOf(ProdSetCode);
                break;
            case 101:
                strFieldValue = String.valueOf(PolicyNo);
                break;
            case 102:
                strFieldValue = String.valueOf(BillPressNo);
                break;
            case 103:
                strFieldValue = String.valueOf(CardTypeCode);
                break;
            case 104:
                strFieldValue = String.valueOf(VisitDate);
                break;
            case 105:
                strFieldValue = String.valueOf(VisitTime);
                break;
            case 106:
                strFieldValue = String.valueOf(SaleCom);
                break;
            case 107:
                strFieldValue = String.valueOf(PrintFlag);
                break;
            case 108:
                strFieldValue = String.valueOf(InvoicePrtFlag);
                break;
            case 109:
                strFieldValue = String.valueOf(NewReinsureFlag);
                break;
            case 110:
                strFieldValue = String.valueOf(RenewPayFlag);
                break;
            case 111:
                strFieldValue = String.valueOf(AppntFirstName);
                break;
            case 112:
                strFieldValue = String.valueOf(AppntLastName);
                break;
            case 113:
                strFieldValue = String.valueOf(InsuredFirstName);
                break;
            case 114:
                strFieldValue = String.valueOf(InsuredLastName);
                break;
            case 115:
                strFieldValue = String.valueOf(AuthorFlag);
                break;
            case 116:
                strFieldValue = String.valueOf(GreenChnl);
                break;
            case 117:
                strFieldValue = String.valueOf(TBType);
                break;
            case 118:
                strFieldValue = String.valueOf(EAuto);
                break;
            case 119:
                strFieldValue = String.valueOf(SlipForm);
                break;
            case 120:
                strFieldValue = String.valueOf(AutoPayFlag);
                break;
            case 121:
                strFieldValue = String.valueOf(RnewFlag);
                break;
            case 122:
                strFieldValue = String.valueOf(FamilyContNo);
                break;
            case 123:
                strFieldValue = String.valueOf(BussFlag);
                break;
            case 124:
                strFieldValue = String.valueOf(SignName);
                break;
            case 125:
                strFieldValue = String.valueOf(OrganizeDate);
                break;
            case 126:
                strFieldValue = String.valueOf(OrganizeTime);
                break;
            case 127:
                strFieldValue = String.valueOf(NewAutoSendBankFlag);
                break;
            case 128:
                strFieldValue = String.valueOf(AgentCodeOper);
                break;
            case 129:
                strFieldValue = String.valueOf(AgentCodeAssi);
                break;
            case 130:
                strFieldValue = String.valueOf(DelayReasonCode);
                break;
            case 131:
                strFieldValue = String.valueOf(DelayReasonDesc);
                break;
            case 132:
                strFieldValue = String.valueOf(XQremindflag);
                break;
            case 133:
                strFieldValue = String.valueOf(OrganComCode);
                break;
            case 134:
                strFieldValue = String.valueOf(BankProivnce);
                break;
            case 135:
                strFieldValue = String.valueOf(NewBankProivnce);
                break;
            case 136:
                strFieldValue = String.valueOf(ArbitrationCom);
                break;
            case 137:
                strFieldValue = String.valueOf(OtherPrtno);
                break;
            case 138:
                strFieldValue = String.valueOf(DestAccountDate);
                break;
            case 139:
                strFieldValue = String.valueOf(PrePayCount);
                break;
            case 140:
                strFieldValue = String.valueOf(NewBankCity);
                break;
            case 141:
                strFieldValue = String.valueOf(InsuredBankCode);
                break;
            case 142:
                strFieldValue = String.valueOf(InsuredBankAccNo);
                break;
            case 143:
                strFieldValue = String.valueOf(InsuredAccName);
                break;
            case 144:
                strFieldValue = String.valueOf(InsuredBankProvince);
                break;
            case 145:
                strFieldValue = String.valueOf(InsuredBankCity);
                break;
            case 146:
                strFieldValue = String.valueOf(NewAccType);
                break;
            case 147:
                strFieldValue = String.valueOf(AccType);
                break;
            case 148:
                strFieldValue = String.valueOf(BankCity);
                break;
            case 149:
                strFieldValue = String.valueOf(EstimateMoney);
                break;
            case 150:
                strFieldValue = String.valueOf(InsuredAccType);
                break;
            case 151:
                strFieldValue = String.valueOf(IsAllowedCharges);
                break;
            case 152:
                strFieldValue = String.valueOf(WTEdorAcceptNo);
                break;
            case 153:
                strFieldValue = String.valueOf(Recording);
                break;
            case 154:
                strFieldValue = String.valueOf(SaleChannels);
                break;
            case 155:
                strFieldValue = String.valueOf(ZJAgentCom);
                break;
            case 156:
                strFieldValue = String.valueOf(ZJAgentComName);
                break;
            case 157:
                strFieldValue = String.valueOf(RecordFlag);
                break;
            case 158:
                strFieldValue = String.valueOf(PlanCode);
                break;
            case 159:
                strFieldValue = String.valueOf(ReceiptNo);
                break;
            case 160:
                strFieldValue = String.valueOf(ReissueCont);
                break;
            case 161:
                strFieldValue = String.valueOf(DomainCode);
                break;
            case 162:
                strFieldValue = String.valueOf(DomainName);
                break;
            case 163:
                strFieldValue = String.valueOf(ReferralNo);
                break;
            case 164:
                strFieldValue = String.valueOf(ThirdPartyOrderId);
                break;
            case 165:
                strFieldValue = String.valueOf(UserId);
                break;
            case 166:
                strFieldValue = String.valueOf(SettlementNumber);
                break;
            case 167:
                strFieldValue = String.valueOf(SettlementDate);
                break;
            case 168:
                strFieldValue = String.valueOf(SettlementStatus);
                break;
            case 169:
                strFieldValue = String.valueOf(ProductCode);
                break;
            case 170:
                strFieldValue = String.valueOf(EndDate);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("ContID")) {
            if( FValue != null && !FValue.equals("")) {
                ContID = new Long(FValue).longValue();
            }
        }
        if (FCode.equalsIgnoreCase("ShardingID")) {
            if( FValue != null && !FValue.equals(""))
            {
                ShardingID = FValue.trim();
            }
            else
                ShardingID = null;
        }
        if (FCode.equalsIgnoreCase("GrpContNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                GrpContNo = FValue.trim();
            }
            else
                GrpContNo = null;
        }
        if (FCode.equalsIgnoreCase("ContNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                ContNo = FValue.trim();
            }
            else
                ContNo = null;
        }
        if (FCode.equalsIgnoreCase("ProposalContNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                ProposalContNo = FValue.trim();
            }
            else
                ProposalContNo = null;
        }
        if (FCode.equalsIgnoreCase("PrtNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                PrtNo = FValue.trim();
            }
            else
                PrtNo = null;
        }
        if (FCode.equalsIgnoreCase("ContType")) {
            if( FValue != null && !FValue.equals(""))
            {
                ContType = FValue.trim();
            }
            else
                ContType = null;
        }
        if (FCode.equalsIgnoreCase("FamilyType")) {
            if( FValue != null && !FValue.equals(""))
            {
                FamilyType = FValue.trim();
            }
            else
                FamilyType = null;
        }
        if (FCode.equalsIgnoreCase("FamilyID")) {
            if( FValue != null && !FValue.equals(""))
            {
                FamilyID = FValue.trim();
            }
            else
                FamilyID = null;
        }
        if (FCode.equalsIgnoreCase("PolType")) {
            if( FValue != null && !FValue.equals(""))
            {
                PolType = FValue.trim();
            }
            else
                PolType = null;
        }
        if (FCode.equalsIgnoreCase("CardFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                CardFlag = FValue.trim();
            }
            else
                CardFlag = null;
        }
        if (FCode.equalsIgnoreCase("ManageCom")) {
            if( FValue != null && !FValue.equals(""))
            {
                ManageCom = FValue.trim();
            }
            else
                ManageCom = null;
        }
        if (FCode.equalsIgnoreCase("ExecuteCom")) {
            if( FValue != null && !FValue.equals(""))
            {
                ExecuteCom = FValue.trim();
            }
            else
                ExecuteCom = null;
        }
        if (FCode.equalsIgnoreCase("AgentCom")) {
            if( FValue != null && !FValue.equals(""))
            {
                AgentCom = FValue.trim();
            }
            else
                AgentCom = null;
        }
        if (FCode.equalsIgnoreCase("AgentCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                AgentCode = FValue.trim();
            }
            else
                AgentCode = null;
        }
        if (FCode.equalsIgnoreCase("AgentGroup")) {
            if( FValue != null && !FValue.equals(""))
            {
                AgentGroup = FValue.trim();
            }
            else
                AgentGroup = null;
        }
        if (FCode.equalsIgnoreCase("AgentCode1")) {
            if( FValue != null && !FValue.equals(""))
            {
                AgentCode1 = FValue.trim();
            }
            else
                AgentCode1 = null;
        }
        if (FCode.equalsIgnoreCase("AgentType")) {
            if( FValue != null && !FValue.equals(""))
            {
                AgentType = FValue.trim();
            }
            else
                AgentType = null;
        }
        if (FCode.equalsIgnoreCase("SaleChnl")) {
            if( FValue != null && !FValue.equals(""))
            {
                SaleChnl = FValue.trim();
            }
            else
                SaleChnl = null;
        }
        if (FCode.equalsIgnoreCase("Handler")) {
            if( FValue != null && !FValue.equals(""))
            {
                Handler = FValue.trim();
            }
            else
                Handler = null;
        }
        if (FCode.equalsIgnoreCase("Password")) {
            if( FValue != null && !FValue.equals(""))
            {
                Password = FValue.trim();
            }
            else
                Password = null;
        }
        if (FCode.equalsIgnoreCase("AppntNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                AppntNo = FValue.trim();
            }
            else
                AppntNo = null;
        }
        if (FCode.equalsIgnoreCase("AppntName")) {
            if( FValue != null && !FValue.equals(""))
            {
                AppntName = FValue.trim();
            }
            else
                AppntName = null;
        }
        if (FCode.equalsIgnoreCase("AppntSex")) {
            if( FValue != null && !FValue.equals(""))
            {
                AppntSex = FValue.trim();
            }
            else
                AppntSex = null;
        }
        if (FCode.equalsIgnoreCase("AppntBirthday")) {
            if( FValue != null && !FValue.equals(""))
            {
                AppntBirthday = FValue.trim();
            }
            else
                AppntBirthday = null;
        }
        if (FCode.equalsIgnoreCase("AppntIDType")) {
            if( FValue != null && !FValue.equals(""))
            {
                AppntIDType = FValue.trim();
            }
            else
                AppntIDType = null;
        }
        if (FCode.equalsIgnoreCase("AppntIDNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                AppntIDNo = FValue.trim();
            }
            else
                AppntIDNo = null;
        }
        if (FCode.equalsIgnoreCase("InsuredNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                InsuredNo = FValue.trim();
            }
            else
                InsuredNo = null;
        }
        if (FCode.equalsIgnoreCase("InsuredName")) {
            if( FValue != null && !FValue.equals(""))
            {
                InsuredName = FValue.trim();
            }
            else
                InsuredName = null;
        }
        if (FCode.equalsIgnoreCase("InsuredSex")) {
            if( FValue != null && !FValue.equals(""))
            {
                InsuredSex = FValue.trim();
            }
            else
                InsuredSex = null;
        }
        if (FCode.equalsIgnoreCase("InsuredBirthday")) {
            if( FValue != null && !FValue.equals(""))
            {
                InsuredBirthday = FValue.trim();
            }
            else
                InsuredBirthday = null;
        }
        if (FCode.equalsIgnoreCase("InsuredIDType")) {
            if( FValue != null && !FValue.equals(""))
            {
                InsuredIDType = FValue.trim();
            }
            else
                InsuredIDType = null;
        }
        if (FCode.equalsIgnoreCase("InsuredIDNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                InsuredIDNo = FValue.trim();
            }
            else
                InsuredIDNo = null;
        }
        if (FCode.equalsIgnoreCase("PayIntv")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                PayIntv = i;
            }
        }
        if (FCode.equalsIgnoreCase("PayMode")) {
            if( FValue != null && !FValue.equals(""))
            {
                PayMode = FValue.trim();
            }
            else
                PayMode = null;
        }
        if (FCode.equalsIgnoreCase("PayLocation")) {
            if( FValue != null && !FValue.equals(""))
            {
                PayLocation = FValue.trim();
            }
            else
                PayLocation = null;
        }
        if (FCode.equalsIgnoreCase("DisputedFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                DisputedFlag = FValue.trim();
            }
            else
                DisputedFlag = null;
        }
        if (FCode.equalsIgnoreCase("OutPayFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                OutPayFlag = FValue.trim();
            }
            else
                OutPayFlag = null;
        }
        if (FCode.equalsIgnoreCase("GetPolMode")) {
            if( FValue != null && !FValue.equals(""))
            {
                GetPolMode = FValue.trim();
            }
            else
                GetPolMode = null;
        }
        if (FCode.equalsIgnoreCase("SignCom")) {
            if( FValue != null && !FValue.equals(""))
            {
                SignCom = FValue.trim();
            }
            else
                SignCom = null;
        }
        if (FCode.equalsIgnoreCase("SignDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                SignDate = FValue.trim();
            }
            else
                SignDate = null;
        }
        if (FCode.equalsIgnoreCase("SignTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                SignTime = FValue.trim();
            }
            else
                SignTime = null;
        }
        if (FCode.equalsIgnoreCase("ConsignNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                ConsignNo = FValue.trim();
            }
            else
                ConsignNo = null;
        }
        if (FCode.equalsIgnoreCase("BankCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                BankCode = FValue.trim();
            }
            else
                BankCode = null;
        }
        if (FCode.equalsIgnoreCase("BankAccNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                BankAccNo = FValue.trim();
            }
            else
                BankAccNo = null;
        }
        if (FCode.equalsIgnoreCase("AccName")) {
            if( FValue != null && !FValue.equals(""))
            {
                AccName = FValue.trim();
            }
            else
                AccName = null;
        }
        if (FCode.equalsIgnoreCase("PrintCount")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                PrintCount = i;
            }
        }
        if (FCode.equalsIgnoreCase("LostTimes")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                LostTimes = i;
            }
        }
        if (FCode.equalsIgnoreCase("Lang")) {
            if( FValue != null && !FValue.equals(""))
            {
                Lang = FValue.trim();
            }
            else
                Lang = null;
        }
        if (FCode.equalsIgnoreCase("Currency")) {
            if( FValue != null && !FValue.equals(""))
            {
                Currency = FValue.trim();
            }
            else
                Currency = null;
        }
        if (FCode.equalsIgnoreCase("Remark")) {
            if( FValue != null && !FValue.equals(""))
            {
                Remark = FValue.trim();
            }
            else
                Remark = null;
        }
        if (FCode.equalsIgnoreCase("Peoples")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                Peoples = i;
            }
        }
        if (FCode.equalsIgnoreCase("Mult")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                Mult = d;
            }
        }
        if (FCode.equalsIgnoreCase("Prem")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                Prem = d;
            }
        }
        if (FCode.equalsIgnoreCase("Amnt")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                Amnt = d;
            }
        }
        if (FCode.equalsIgnoreCase("SumPrem")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                SumPrem = d;
            }
        }
        if (FCode.equalsIgnoreCase("Dif")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                Dif = d;
            }
        }
        if (FCode.equalsIgnoreCase("PaytoDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                PaytoDate = FValue.trim();
            }
            else
                PaytoDate = null;
        }
        if (FCode.equalsIgnoreCase("FirstPayDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                FirstPayDate = FValue.trim();
            }
            else
                FirstPayDate = null;
        }
        if (FCode.equalsIgnoreCase("CValiDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                CValiDate = FValue.trim();
            }
            else
                CValiDate = null;
        }
        if (FCode.equalsIgnoreCase("InputOperator")) {
            if( FValue != null && !FValue.equals(""))
            {
                InputOperator = FValue.trim();
            }
            else
                InputOperator = null;
        }
        if (FCode.equalsIgnoreCase("InputDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                InputDate = FValue.trim();
            }
            else
                InputDate = null;
        }
        if (FCode.equalsIgnoreCase("InputTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                InputTime = FValue.trim();
            }
            else
                InputTime = null;
        }
        if (FCode.equalsIgnoreCase("ApproveFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                ApproveFlag = FValue.trim();
            }
            else
                ApproveFlag = null;
        }
        if (FCode.equalsIgnoreCase("ApproveCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                ApproveCode = FValue.trim();
            }
            else
                ApproveCode = null;
        }
        if (FCode.equalsIgnoreCase("ApproveDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                ApproveDate = FValue.trim();
            }
            else
                ApproveDate = null;
        }
        if (FCode.equalsIgnoreCase("ApproveTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                ApproveTime = FValue.trim();
            }
            else
                ApproveTime = null;
        }
        if (FCode.equalsIgnoreCase("UWFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                UWFlag = FValue.trim();
            }
            else
                UWFlag = null;
        }
        if (FCode.equalsIgnoreCase("UWOperator")) {
            if( FValue != null && !FValue.equals(""))
            {
                UWOperator = FValue.trim();
            }
            else
                UWOperator = null;
        }
        if (FCode.equalsIgnoreCase("UWDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                UWDate = FValue.trim();
            }
            else
                UWDate = null;
        }
        if (FCode.equalsIgnoreCase("UWTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                UWTime = FValue.trim();
            }
            else
                UWTime = null;
        }
        if (FCode.equalsIgnoreCase("AppFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                AppFlag = FValue.trim();
            }
            else
                AppFlag = null;
        }
        if (FCode.equalsIgnoreCase("PolApplyDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                PolApplyDate = FValue.trim();
            }
            else
                PolApplyDate = null;
        }
        if (FCode.equalsIgnoreCase("GetPolDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                GetPolDate = FValue.trim();
            }
            else
                GetPolDate = null;
        }
        if (FCode.equalsIgnoreCase("GetPolTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                GetPolTime = FValue.trim();
            }
            else
                GetPolTime = null;
        }
        if (FCode.equalsIgnoreCase("CustomGetPolDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                CustomGetPolDate = FValue.trim();
            }
            else
                CustomGetPolDate = null;
        }
        if (FCode.equalsIgnoreCase("State")) {
            if( FValue != null && !FValue.equals(""))
            {
                State = FValue.trim();
            }
            else
                State = null;
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            if( FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
                Operator = null;
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeDate = FValue.trim();
            }
            else
                MakeDate = null;
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
                MakeTime = null;
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyDate = FValue.trim();
            }
            else
                ModifyDate = null;
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
                ModifyTime = null;
        }
        if (FCode.equalsIgnoreCase("FirstTrialOperator")) {
            if( FValue != null && !FValue.equals(""))
            {
                FirstTrialOperator = FValue.trim();
            }
            else
                FirstTrialOperator = null;
        }
        if (FCode.equalsIgnoreCase("FirstTrialDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                FirstTrialDate = FValue.trim();
            }
            else
                FirstTrialDate = null;
        }
        if (FCode.equalsIgnoreCase("FirstTrialTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                FirstTrialTime = FValue.trim();
            }
            else
                FirstTrialTime = null;
        }
        if (FCode.equalsIgnoreCase("ReceiveOperator")) {
            if( FValue != null && !FValue.equals(""))
            {
                ReceiveOperator = FValue.trim();
            }
            else
                ReceiveOperator = null;
        }
        if (FCode.equalsIgnoreCase("ReceiveDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                ReceiveDate = FValue.trim();
            }
            else
                ReceiveDate = null;
        }
        if (FCode.equalsIgnoreCase("ReceiveTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                ReceiveTime = FValue.trim();
            }
            else
                ReceiveTime = null;
        }
        if (FCode.equalsIgnoreCase("TempFeeNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                TempFeeNo = FValue.trim();
            }
            else
                TempFeeNo = null;
        }
        if (FCode.equalsIgnoreCase("SellType")) {
            if( FValue != null && !FValue.equals(""))
            {
                SellType = FValue.trim();
            }
            else
                SellType = null;
        }
        if (FCode.equalsIgnoreCase("ForceUWFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                ForceUWFlag = FValue.trim();
            }
            else
                ForceUWFlag = null;
        }
        if (FCode.equalsIgnoreCase("ForceUWReason")) {
            if( FValue != null && !FValue.equals(""))
            {
                ForceUWReason = FValue.trim();
            }
            else
                ForceUWReason = null;
        }
        if (FCode.equalsIgnoreCase("NewBankCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                NewBankCode = FValue.trim();
            }
            else
                NewBankCode = null;
        }
        if (FCode.equalsIgnoreCase("NewBankAccNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                NewBankAccNo = FValue.trim();
            }
            else
                NewBankAccNo = null;
        }
        if (FCode.equalsIgnoreCase("NewAccName")) {
            if( FValue != null && !FValue.equals(""))
            {
                NewAccName = FValue.trim();
            }
            else
                NewAccName = null;
        }
        if (FCode.equalsIgnoreCase("NewPayMode")) {
            if( FValue != null && !FValue.equals(""))
            {
                NewPayMode = FValue.trim();
            }
            else
                NewPayMode = null;
        }
        if (FCode.equalsIgnoreCase("AgentBankCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                AgentBankCode = FValue.trim();
            }
            else
                AgentBankCode = null;
        }
        if (FCode.equalsIgnoreCase("BankAgent")) {
            if( FValue != null && !FValue.equals(""))
            {
                BankAgent = FValue.trim();
            }
            else
                BankAgent = null;
        }
        if (FCode.equalsIgnoreCase("BankAgentName")) {
            if( FValue != null && !FValue.equals(""))
            {
                BankAgentName = FValue.trim();
            }
            else
                BankAgentName = null;
        }
        if (FCode.equalsIgnoreCase("BankAgentTel")) {
            if( FValue != null && !FValue.equals(""))
            {
                BankAgentTel = FValue.trim();
            }
            else
                BankAgentTel = null;
        }
        if (FCode.equalsIgnoreCase("ProdSetCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                ProdSetCode = FValue.trim();
            }
            else
                ProdSetCode = null;
        }
        if (FCode.equalsIgnoreCase("PolicyNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                PolicyNo = FValue.trim();
            }
            else
                PolicyNo = null;
        }
        if (FCode.equalsIgnoreCase("BillPressNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                BillPressNo = FValue.trim();
            }
            else
                BillPressNo = null;
        }
        if (FCode.equalsIgnoreCase("CardTypeCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                CardTypeCode = FValue.trim();
            }
            else
                CardTypeCode = null;
        }
        if (FCode.equalsIgnoreCase("VisitDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                VisitDate = FValue.trim();
            }
            else
                VisitDate = null;
        }
        if (FCode.equalsIgnoreCase("VisitTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                VisitTime = FValue.trim();
            }
            else
                VisitTime = null;
        }
        if (FCode.equalsIgnoreCase("SaleCom")) {
            if( FValue != null && !FValue.equals(""))
            {
                SaleCom = FValue.trim();
            }
            else
                SaleCom = null;
        }
        if (FCode.equalsIgnoreCase("PrintFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                PrintFlag = FValue.trim();
            }
            else
                PrintFlag = null;
        }
        if (FCode.equalsIgnoreCase("InvoicePrtFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                InvoicePrtFlag = FValue.trim();
            }
            else
                InvoicePrtFlag = null;
        }
        if (FCode.equalsIgnoreCase("NewReinsureFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                NewReinsureFlag = FValue.trim();
            }
            else
                NewReinsureFlag = null;
        }
        if (FCode.equalsIgnoreCase("RenewPayFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                RenewPayFlag = FValue.trim();
            }
            else
                RenewPayFlag = null;
        }
        if (FCode.equalsIgnoreCase("AppntFirstName")) {
            if( FValue != null && !FValue.equals(""))
            {
                AppntFirstName = FValue.trim();
            }
            else
                AppntFirstName = null;
        }
        if (FCode.equalsIgnoreCase("AppntLastName")) {
            if( FValue != null && !FValue.equals(""))
            {
                AppntLastName = FValue.trim();
            }
            else
                AppntLastName = null;
        }
        if (FCode.equalsIgnoreCase("InsuredFirstName")) {
            if( FValue != null && !FValue.equals(""))
            {
                InsuredFirstName = FValue.trim();
            }
            else
                InsuredFirstName = null;
        }
        if (FCode.equalsIgnoreCase("InsuredLastName")) {
            if( FValue != null && !FValue.equals(""))
            {
                InsuredLastName = FValue.trim();
            }
            else
                InsuredLastName = null;
        }
        if (FCode.equalsIgnoreCase("AuthorFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                AuthorFlag = FValue.trim();
            }
            else
                AuthorFlag = null;
        }
        if (FCode.equalsIgnoreCase("GreenChnl")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                GreenChnl = i;
            }
        }
        if (FCode.equalsIgnoreCase("TBType")) {
            if( FValue != null && !FValue.equals(""))
            {
                TBType = FValue.trim();
            }
            else
                TBType = null;
        }
        if (FCode.equalsIgnoreCase("EAuto")) {
            if( FValue != null && !FValue.equals(""))
            {
                EAuto = FValue.trim();
            }
            else
                EAuto = null;
        }
        if (FCode.equalsIgnoreCase("SlipForm")) {
            if( FValue != null && !FValue.equals(""))
            {
                SlipForm = FValue.trim();
            }
            else
                SlipForm = null;
        }
        if (FCode.equalsIgnoreCase("AutoPayFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                AutoPayFlag = FValue.trim();
            }
            else
                AutoPayFlag = null;
        }
        if (FCode.equalsIgnoreCase("RnewFlag")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                RnewFlag = i;
            }
        }
        if (FCode.equalsIgnoreCase("FamilyContNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                FamilyContNo = FValue.trim();
            }
            else
                FamilyContNo = null;
        }
        if (FCode.equalsIgnoreCase("BussFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                BussFlag = FValue.trim();
            }
            else
                BussFlag = null;
        }
        if (FCode.equalsIgnoreCase("SignName")) {
            if( FValue != null && !FValue.equals(""))
            {
                SignName = FValue.trim();
            }
            else
                SignName = null;
        }
        if (FCode.equalsIgnoreCase("OrganizeDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                OrganizeDate = FValue.trim();
            }
            else
                OrganizeDate = null;
        }
        if (FCode.equalsIgnoreCase("OrganizeTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                OrganizeTime = FValue.trim();
            }
            else
                OrganizeTime = null;
        }
        if (FCode.equalsIgnoreCase("NewAutoSendBankFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                NewAutoSendBankFlag = FValue.trim();
            }
            else
                NewAutoSendBankFlag = null;
        }
        if (FCode.equalsIgnoreCase("AgentCodeOper")) {
            if( FValue != null && !FValue.equals(""))
            {
                AgentCodeOper = FValue.trim();
            }
            else
                AgentCodeOper = null;
        }
        if (FCode.equalsIgnoreCase("AgentCodeAssi")) {
            if( FValue != null && !FValue.equals(""))
            {
                AgentCodeAssi = FValue.trim();
            }
            else
                AgentCodeAssi = null;
        }
        if (FCode.equalsIgnoreCase("DelayReasonCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                DelayReasonCode = FValue.trim();
            }
            else
                DelayReasonCode = null;
        }
        if (FCode.equalsIgnoreCase("DelayReasonDesc")) {
            if( FValue != null && !FValue.equals(""))
            {
                DelayReasonDesc = FValue.trim();
            }
            else
                DelayReasonDesc = null;
        }
        if (FCode.equalsIgnoreCase("XQremindflag")) {
            if( FValue != null && !FValue.equals(""))
            {
                XQremindflag = FValue.trim();
            }
            else
                XQremindflag = null;
        }
        if (FCode.equalsIgnoreCase("OrganComCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                OrganComCode = FValue.trim();
            }
            else
                OrganComCode = null;
        }
        if (FCode.equalsIgnoreCase("BankProivnce")) {
            if( FValue != null && !FValue.equals(""))
            {
                BankProivnce = FValue.trim();
            }
            else
                BankProivnce = null;
        }
        if (FCode.equalsIgnoreCase("NewBankProivnce")) {
            if( FValue != null && !FValue.equals(""))
            {
                NewBankProivnce = FValue.trim();
            }
            else
                NewBankProivnce = null;
        }
        if (FCode.equalsIgnoreCase("ArbitrationCom")) {
            if( FValue != null && !FValue.equals(""))
            {
                ArbitrationCom = FValue.trim();
            }
            else
                ArbitrationCom = null;
        }
        if (FCode.equalsIgnoreCase("OtherPrtno")) {
            if( FValue != null && !FValue.equals(""))
            {
                OtherPrtno = FValue.trim();
            }
            else
                OtherPrtno = null;
        }
        if (FCode.equalsIgnoreCase("DestAccountDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                DestAccountDate = FValue.trim();
            }
            else
                DestAccountDate = null;
        }
        if (FCode.equalsIgnoreCase("PrePayCount")) {
            if( FValue != null && !FValue.equals(""))
            {
                PrePayCount = FValue.trim();
            }
            else
                PrePayCount = null;
        }
        if (FCode.equalsIgnoreCase("NewBankCity")) {
            if( FValue != null && !FValue.equals(""))
            {
                NewBankCity = FValue.trim();
            }
            else
                NewBankCity = null;
        }
        if (FCode.equalsIgnoreCase("InsuredBankCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                InsuredBankCode = FValue.trim();
            }
            else
                InsuredBankCode = null;
        }
        if (FCode.equalsIgnoreCase("InsuredBankAccNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                InsuredBankAccNo = FValue.trim();
            }
            else
                InsuredBankAccNo = null;
        }
        if (FCode.equalsIgnoreCase("InsuredAccName")) {
            if( FValue != null && !FValue.equals(""))
            {
                InsuredAccName = FValue.trim();
            }
            else
                InsuredAccName = null;
        }
        if (FCode.equalsIgnoreCase("InsuredBankProvince")) {
            if( FValue != null && !FValue.equals(""))
            {
                InsuredBankProvince = FValue.trim();
            }
            else
                InsuredBankProvince = null;
        }
        if (FCode.equalsIgnoreCase("InsuredBankCity")) {
            if( FValue != null && !FValue.equals(""))
            {
                InsuredBankCity = FValue.trim();
            }
            else
                InsuredBankCity = null;
        }
        if (FCode.equalsIgnoreCase("NewAccType")) {
            if( FValue != null && !FValue.equals(""))
            {
                NewAccType = FValue.trim();
            }
            else
                NewAccType = null;
        }
        if (FCode.equalsIgnoreCase("AccType")) {
            if( FValue != null && !FValue.equals(""))
            {
                AccType = FValue.trim();
            }
            else
                AccType = null;
        }
        if (FCode.equalsIgnoreCase("BankCity")) {
            if( FValue != null && !FValue.equals(""))
            {
                BankCity = FValue.trim();
            }
            else
                BankCity = null;
        }
        if (FCode.equalsIgnoreCase("EstimateMoney")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                EstimateMoney = d;
            }
        }
        if (FCode.equalsIgnoreCase("InsuredAccType")) {
            if( FValue != null && !FValue.equals(""))
            {
                InsuredAccType = FValue.trim();
            }
            else
                InsuredAccType = null;
        }
        if (FCode.equalsIgnoreCase("IsAllowedCharges")) {
            if( FValue != null && !FValue.equals(""))
            {
                IsAllowedCharges = FValue.trim();
            }
            else
                IsAllowedCharges = null;
        }
        if (FCode.equalsIgnoreCase("WTEdorAcceptNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                WTEdorAcceptNo = FValue.trim();
            }
            else
                WTEdorAcceptNo = null;
        }
        if (FCode.equalsIgnoreCase("Recording")) {
            if( FValue != null && !FValue.equals(""))
            {
                Recording = FValue.trim();
            }
            else
                Recording = null;
        }
        if (FCode.equalsIgnoreCase("SaleChannels")) {
            if( FValue != null && !FValue.equals(""))
            {
                SaleChannels = FValue.trim();
            }
            else
                SaleChannels = null;
        }
        if (FCode.equalsIgnoreCase("ZJAgentCom")) {
            if( FValue != null && !FValue.equals(""))
            {
                ZJAgentCom = FValue.trim();
            }
            else
                ZJAgentCom = null;
        }
        if (FCode.equalsIgnoreCase("ZJAgentComName")) {
            if( FValue != null && !FValue.equals(""))
            {
                ZJAgentComName = FValue.trim();
            }
            else
                ZJAgentComName = null;
        }
        if (FCode.equalsIgnoreCase("RecordFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                RecordFlag = FValue.trim();
            }
            else
                RecordFlag = null;
        }
        if (FCode.equalsIgnoreCase("PlanCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                PlanCode = FValue.trim();
            }
            else
                PlanCode = null;
        }
        if (FCode.equalsIgnoreCase("ReceiptNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                ReceiptNo = FValue.trim();
            }
            else
                ReceiptNo = null;
        }
        if (FCode.equalsIgnoreCase("ReissueCont")) {
            if( FValue != null && !FValue.equals(""))
            {
                ReissueCont = FValue.trim();
            }
            else
                ReissueCont = null;
        }
        if (FCode.equalsIgnoreCase("DomainCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                DomainCode = FValue.trim();
            }
            else
                DomainCode = null;
        }
        if (FCode.equalsIgnoreCase("DomainName")) {
            if( FValue != null && !FValue.equals(""))
            {
                DomainName = FValue.trim();
            }
            else
                DomainName = null;
        }
        if (FCode.equalsIgnoreCase("ReferralNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                ReferralNo = FValue.trim();
            }
            else
                ReferralNo = null;
        }
        if (FCode.equalsIgnoreCase("ThirdPartyOrderId")) {
            if( FValue != null && !FValue.equals(""))
            {
                ThirdPartyOrderId = FValue.trim();
            }
            else
                ThirdPartyOrderId = null;
        }
        if (FCode.equalsIgnoreCase("UserId")) {
            if( FValue != null && !FValue.equals(""))
            {
                UserId = FValue.trim();
            }
            else
                UserId = null;
        }
        if (FCode.equalsIgnoreCase("SettlementNumber")) {
            if( FValue != null && !FValue.equals(""))
            {
                SettlementNumber = FValue.trim();
            }
            else
                SettlementNumber = null;
        }
        if (FCode.equalsIgnoreCase("SettlementDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                SettlementDate = FValue.trim();
            }
            else
                SettlementDate = null;
        }
        if (FCode.equalsIgnoreCase("SettlementStatus")) {
            if( FValue != null && !FValue.equals(""))
            {
                SettlementStatus = FValue.trim();
            }
            else
                SettlementStatus = null;
        }
        if (FCode.equalsIgnoreCase("ProductCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                ProductCode = FValue.trim();
            }
            else
                ProductCode = null;
        }
        if (FCode.equalsIgnoreCase("EndDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                EndDate = FValue.trim();
            }
            else
                EndDate = null;
        }
        return true;
    }


    public String toString() {
    return "LCContPojo [" +
            "ContID="+ContID +
            ", ShardingID="+ShardingID +
            ", GrpContNo="+GrpContNo +
            ", ContNo="+ContNo +
            ", ProposalContNo="+ProposalContNo +
            ", PrtNo="+PrtNo +
            ", ContType="+ContType +
            ", FamilyType="+FamilyType +
            ", FamilyID="+FamilyID +
            ", PolType="+PolType +
            ", CardFlag="+CardFlag +
            ", ManageCom="+ManageCom +
            ", ExecuteCom="+ExecuteCom +
            ", AgentCom="+AgentCom +
            ", AgentCode="+AgentCode +
            ", AgentGroup="+AgentGroup +
            ", AgentCode1="+AgentCode1 +
            ", AgentType="+AgentType +
            ", SaleChnl="+SaleChnl +
            ", Handler="+Handler +
            ", Password="+Password +
            ", AppntNo="+AppntNo +
            ", AppntName="+AppntName +
            ", AppntSex="+AppntSex +
            ", AppntBirthday="+AppntBirthday +
            ", AppntIDType="+AppntIDType +
            ", AppntIDNo="+AppntIDNo +
            ", InsuredNo="+InsuredNo +
            ", InsuredName="+InsuredName +
            ", InsuredSex="+InsuredSex +
            ", InsuredBirthday="+InsuredBirthday +
            ", InsuredIDType="+InsuredIDType +
            ", InsuredIDNo="+InsuredIDNo +
            ", PayIntv="+PayIntv +
            ", PayMode="+PayMode +
            ", PayLocation="+PayLocation +
            ", DisputedFlag="+DisputedFlag +
            ", OutPayFlag="+OutPayFlag +
            ", GetPolMode="+GetPolMode +
            ", SignCom="+SignCom +
            ", SignDate="+SignDate +
            ", SignTime="+SignTime +
            ", ConsignNo="+ConsignNo +
            ", BankCode="+BankCode +
            ", BankAccNo="+BankAccNo +
            ", AccName="+AccName +
            ", PrintCount="+PrintCount +
            ", LostTimes="+LostTimes +
            ", Lang="+Lang +
            ", Currency="+Currency +
            ", Remark="+Remark +
            ", Peoples="+Peoples +
            ", Mult="+Mult +
            ", Prem="+Prem +
            ", Amnt="+Amnt +
            ", SumPrem="+SumPrem +
            ", Dif="+Dif +
            ", PaytoDate="+PaytoDate +
            ", FirstPayDate="+FirstPayDate +
            ", CValiDate="+CValiDate +
            ", InputOperator="+InputOperator +
            ", InputDate="+InputDate +
            ", InputTime="+InputTime +
            ", ApproveFlag="+ApproveFlag +
            ", ApproveCode="+ApproveCode +
            ", ApproveDate="+ApproveDate +
            ", ApproveTime="+ApproveTime +
            ", UWFlag="+UWFlag +
            ", UWOperator="+UWOperator +
            ", UWDate="+UWDate +
            ", UWTime="+UWTime +
            ", AppFlag="+AppFlag +
            ", PolApplyDate="+PolApplyDate +
            ", GetPolDate="+GetPolDate +
            ", GetPolTime="+GetPolTime +
            ", CustomGetPolDate="+CustomGetPolDate +
            ", State="+State +
            ", Operator="+Operator +
            ", MakeDate="+MakeDate +
            ", MakeTime="+MakeTime +
            ", ModifyDate="+ModifyDate +
            ", ModifyTime="+ModifyTime +
            ", FirstTrialOperator="+FirstTrialOperator +
            ", FirstTrialDate="+FirstTrialDate +
            ", FirstTrialTime="+FirstTrialTime +
            ", ReceiveOperator="+ReceiveOperator +
            ", ReceiveDate="+ReceiveDate +
            ", ReceiveTime="+ReceiveTime +
            ", TempFeeNo="+TempFeeNo +
            ", SellType="+SellType +
            ", ForceUWFlag="+ForceUWFlag +
            ", ForceUWReason="+ForceUWReason +
            ", NewBankCode="+NewBankCode +
            ", NewBankAccNo="+NewBankAccNo +
            ", NewAccName="+NewAccName +
            ", NewPayMode="+NewPayMode +
            ", AgentBankCode="+AgentBankCode +
            ", BankAgent="+BankAgent +
            ", BankAgentName="+BankAgentName +
            ", BankAgentTel="+BankAgentTel +
            ", ProdSetCode="+ProdSetCode +
            ", PolicyNo="+PolicyNo +
            ", BillPressNo="+BillPressNo +
            ", CardTypeCode="+CardTypeCode +
            ", VisitDate="+VisitDate +
            ", VisitTime="+VisitTime +
            ", SaleCom="+SaleCom +
            ", PrintFlag="+PrintFlag +
            ", InvoicePrtFlag="+InvoicePrtFlag +
            ", NewReinsureFlag="+NewReinsureFlag +
            ", RenewPayFlag="+RenewPayFlag +
            ", AppntFirstName="+AppntFirstName +
            ", AppntLastName="+AppntLastName +
            ", InsuredFirstName="+InsuredFirstName +
            ", InsuredLastName="+InsuredLastName +
            ", AuthorFlag="+AuthorFlag +
            ", GreenChnl="+GreenChnl +
            ", TBType="+TBType +
            ", EAuto="+EAuto +
            ", SlipForm="+SlipForm +
            ", AutoPayFlag="+AutoPayFlag +
            ", RnewFlag="+RnewFlag +
            ", FamilyContNo="+FamilyContNo +
            ", BussFlag="+BussFlag +
            ", SignName="+SignName +
            ", OrganizeDate="+OrganizeDate +
            ", OrganizeTime="+OrganizeTime +
            ", NewAutoSendBankFlag="+NewAutoSendBankFlag +
            ", AgentCodeOper="+AgentCodeOper +
            ", AgentCodeAssi="+AgentCodeAssi +
            ", DelayReasonCode="+DelayReasonCode +
            ", DelayReasonDesc="+DelayReasonDesc +
            ", XQremindflag="+XQremindflag +
            ", OrganComCode="+OrganComCode +
            ", BankProivnce="+BankProivnce +
            ", NewBankProivnce="+NewBankProivnce +
            ", ArbitrationCom="+ArbitrationCom +
            ", OtherPrtno="+OtherPrtno +
            ", DestAccountDate="+DestAccountDate +
            ", PrePayCount="+PrePayCount +
            ", NewBankCity="+NewBankCity +
            ", InsuredBankCode="+InsuredBankCode +
            ", InsuredBankAccNo="+InsuredBankAccNo +
            ", InsuredAccName="+InsuredAccName +
            ", InsuredBankProvince="+InsuredBankProvince +
            ", InsuredBankCity="+InsuredBankCity +
            ", NewAccType="+NewAccType +
            ", AccType="+AccType +
            ", BankCity="+BankCity +
            ", EstimateMoney="+EstimateMoney +
            ", InsuredAccType="+InsuredAccType +
            ", IsAllowedCharges="+IsAllowedCharges +
            ", WTEdorAcceptNo="+WTEdorAcceptNo +
            ", Recording="+Recording +
            ", SaleChannels="+SaleChannels +
            ", ZJAgentCom="+ZJAgentCom +
            ", ZJAgentComName="+ZJAgentComName +
            ", RecordFlag="+RecordFlag +
            ", PlanCode="+PlanCode +
            ", ReceiptNo="+ReceiptNo +
            ", ReissueCont="+ReissueCont +
            ", DomainCode="+DomainCode +
            ", DomainName="+DomainName +
            ", ReferralNo="+ReferralNo +
            ", ThirdPartyOrderId="+ThirdPartyOrderId +
            ", UserId="+UserId +
            ", SettlementNumber="+SettlementNumber +
            ", SettlementDate="+SettlementDate +
            ", SettlementStatus="+SettlementStatus +
            ", ProductCode="+ProductCode +
            ", EndDate="+EndDate +"]";
    }
}
