/**
 * Copyright (c) 2017 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.entity;

import com.sinosoft.utility.Pojo;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;

import java.io.Serializable;

/**
 * <p>ClassName: LMCheckFieldPojo </p>
 * <p>Copyright: Copyright (c) 2017</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2017-10-17
 */
public class LMCheckFieldPojo implements Pojo,Serializable {
    // @Field
    /** 险种编码 */
    private String RiskCode; 
    /** 险种版本 */
    private String RiskVer; 
    /** 控制字段名称 */
    private String FieldName; 
    /** 序号 */
    private String SerialNo; 
    /** 算法 */
    private String CalCode; 
    /** 页面位置 */
    private String PageLocation; 
    /** 事件位置 */
    private String Location; 
    /** 提示信息 */
    private String Msg; 
    /** 提示标记 */
    private String MsgFlag; 
    /** 修改变量标记 */
    private String UpdFlag; 
    /** 有效结果标记 */
    private String ValiFlag; 
    /** 返回值有效标记 */
    private String ReturnValiFlag; 


    public static final int FIELDNUM = 12;    // 数据库表的字段个数
    public String getRiskCode() {
        return RiskCode;
    }
    public void setRiskCode(String aRiskCode) {
        RiskCode = aRiskCode;
    }
    public String getRiskVer() {
        return RiskVer;
    }
    public void setRiskVer(String aRiskVer) {
        RiskVer = aRiskVer;
    }
    public String getFieldName() {
        return FieldName;
    }
    public void setFieldName(String aFieldName) {
        FieldName = aFieldName;
    }
    public String getSerialNo() {
        return SerialNo;
    }
    public void setSerialNo(String aSerialNo) {
        SerialNo = aSerialNo;
    }
    public String getCalCode() {
        return CalCode;
    }
    public void setCalCode(String aCalCode) {
        CalCode = aCalCode;
    }
    public String getPageLocation() {
        return PageLocation;
    }
    public void setPageLocation(String aPageLocation) {
        PageLocation = aPageLocation;
    }
    public String getLocation() {
        return Location;
    }
    public void setLocation(String aLocation) {
        Location = aLocation;
    }
    public String getMsg() {
        return Msg;
    }
    public void setMsg(String aMsg) {
        Msg = aMsg;
    }
    public String getMsgFlag() {
        return MsgFlag;
    }
    public void setMsgFlag(String aMsgFlag) {
        MsgFlag = aMsgFlag;
    }
    public String getUpdFlag() {
        return UpdFlag;
    }
    public void setUpdFlag(String aUpdFlag) {
        UpdFlag = aUpdFlag;
    }
    public String getValiFlag() {
        return ValiFlag;
    }
    public void setValiFlag(String aValiFlag) {
        ValiFlag = aValiFlag;
    }
    public String getReturnValiFlag() {
        return ReturnValiFlag;
    }
    public void setReturnValiFlag(String aReturnValiFlag) {
        ReturnValiFlag = aReturnValiFlag;
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("RiskCode") ) {
            return 0;
        }
        if( strFieldName.equals("RiskVer") ) {
            return 1;
        }
        if( strFieldName.equals("FieldName") ) {
            return 2;
        }
        if( strFieldName.equals("SerialNo") ) {
            return 3;
        }
        if( strFieldName.equals("CalCode") ) {
            return 4;
        }
        if( strFieldName.equals("PageLocation") ) {
            return 5;
        }
        if( strFieldName.equals("Location") ) {
            return 6;
        }
        if( strFieldName.equals("Msg") ) {
            return 7;
        }
        if( strFieldName.equals("MsgFlag") ) {
            return 8;
        }
        if( strFieldName.equals("UpdFlag") ) {
            return 9;
        }
        if( strFieldName.equals("ValiFlag") ) {
            return 10;
        }
        if( strFieldName.equals("ReturnValiFlag") ) {
            return 11;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "RiskCode";
                break;
            case 1:
                strFieldName = "RiskVer";
                break;
            case 2:
                strFieldName = "FieldName";
                break;
            case 3:
                strFieldName = "SerialNo";
                break;
            case 4:
                strFieldName = "CalCode";
                break;
            case 5:
                strFieldName = "PageLocation";
                break;
            case 6:
                strFieldName = "Location";
                break;
            case 7:
                strFieldName = "Msg";
                break;
            case 8:
                strFieldName = "MsgFlag";
                break;
            case 9:
                strFieldName = "UpdFlag";
                break;
            case 10:
                strFieldName = "ValiFlag";
                break;
            case 11:
                strFieldName = "ReturnValiFlag";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "RISKCODE":
                return Schema.TYPE_STRING;
            case "RISKVER":
                return Schema.TYPE_STRING;
            case "FIELDNAME":
                return Schema.TYPE_STRING;
            case "SERIALNO":
                return Schema.TYPE_STRING;
            case "CALCODE":
                return Schema.TYPE_STRING;
            case "PAGELOCATION":
                return Schema.TYPE_STRING;
            case "LOCATION":
                return Schema.TYPE_STRING;
            case "MSG":
                return Schema.TYPE_STRING;
            case "MSGFLAG":
                return Schema.TYPE_STRING;
            case "UPDFLAG":
                return Schema.TYPE_STRING;
            case "VALIFLAG":
                return Schema.TYPE_STRING;
            case "RETURNVALIFLAG":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_STRING;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_STRING;
            case 6:
                return Schema.TYPE_STRING;
            case 7:
                return Schema.TYPE_STRING;
            case 8:
                return Schema.TYPE_STRING;
            case 9:
                return Schema.TYPE_STRING;
            case 10:
                return Schema.TYPE_STRING;
            case 11:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("RiskCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RiskCode));
        }
        if (FCode.equalsIgnoreCase("RiskVer")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RiskVer));
        }
        if (FCode.equalsIgnoreCase("FieldName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FieldName));
        }
        if (FCode.equalsIgnoreCase("SerialNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SerialNo));
        }
        if (FCode.equalsIgnoreCase("CalCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CalCode));
        }
        if (FCode.equalsIgnoreCase("PageLocation")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PageLocation));
        }
        if (FCode.equalsIgnoreCase("Location")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Location));
        }
        if (FCode.equalsIgnoreCase("Msg")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Msg));
        }
        if (FCode.equalsIgnoreCase("MsgFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MsgFlag));
        }
        if (FCode.equalsIgnoreCase("UpdFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(UpdFlag));
        }
        if (FCode.equalsIgnoreCase("ValiFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ValiFlag));
        }
        if (FCode.equalsIgnoreCase("ReturnValiFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ReturnValiFlag));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = String.valueOf(RiskCode);
                break;
            case 1:
                strFieldValue = String.valueOf(RiskVer);
                break;
            case 2:
                strFieldValue = String.valueOf(FieldName);
                break;
            case 3:
                strFieldValue = String.valueOf(SerialNo);
                break;
            case 4:
                strFieldValue = String.valueOf(CalCode);
                break;
            case 5:
                strFieldValue = String.valueOf(PageLocation);
                break;
            case 6:
                strFieldValue = String.valueOf(Location);
                break;
            case 7:
                strFieldValue = String.valueOf(Msg);
                break;
            case 8:
                strFieldValue = String.valueOf(MsgFlag);
                break;
            case 9:
                strFieldValue = String.valueOf(UpdFlag);
                break;
            case 10:
                strFieldValue = String.valueOf(ValiFlag);
                break;
            case 11:
                strFieldValue = String.valueOf(ReturnValiFlag);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("RiskCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                RiskCode = FValue.trim();
            }
            else
                RiskCode = null;
        }
        if (FCode.equalsIgnoreCase("RiskVer")) {
            if( FValue != null && !FValue.equals(""))
            {
                RiskVer = FValue.trim();
            }
            else
                RiskVer = null;
        }
        if (FCode.equalsIgnoreCase("FieldName")) {
            if( FValue != null && !FValue.equals(""))
            {
                FieldName = FValue.trim();
            }
            else
                FieldName = null;
        }
        if (FCode.equalsIgnoreCase("SerialNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                SerialNo = FValue.trim();
            }
            else
                SerialNo = null;
        }
        if (FCode.equalsIgnoreCase("CalCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                CalCode = FValue.trim();
            }
            else
                CalCode = null;
        }
        if (FCode.equalsIgnoreCase("PageLocation")) {
            if( FValue != null && !FValue.equals(""))
            {
                PageLocation = FValue.trim();
            }
            else
                PageLocation = null;
        }
        if (FCode.equalsIgnoreCase("Location")) {
            if( FValue != null && !FValue.equals(""))
            {
                Location = FValue.trim();
            }
            else
                Location = null;
        }
        if (FCode.equalsIgnoreCase("Msg")) {
            if( FValue != null && !FValue.equals(""))
            {
                Msg = FValue.trim();
            }
            else
                Msg = null;
        }
        if (FCode.equalsIgnoreCase("MsgFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                MsgFlag = FValue.trim();
            }
            else
                MsgFlag = null;
        }
        if (FCode.equalsIgnoreCase("UpdFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                UpdFlag = FValue.trim();
            }
            else
                UpdFlag = null;
        }
        if (FCode.equalsIgnoreCase("ValiFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                ValiFlag = FValue.trim();
            }
            else
                ValiFlag = null;
        }
        if (FCode.equalsIgnoreCase("ReturnValiFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                ReturnValiFlag = FValue.trim();
            }
            else
                ReturnValiFlag = null;
        }
        return true;
    }


    public String toString() {
    return "LMCheckFieldPojo [" +
            "RiskCode="+RiskCode +
            ", RiskVer="+RiskVer +
            ", FieldName="+FieldName +
            ", SerialNo="+SerialNo +
            ", CalCode="+CalCode +
            ", PageLocation="+PageLocation +
            ", Location="+Location +
            ", Msg="+Msg +
            ", MsgFlag="+MsgFlag +
            ", UpdFlag="+UpdFlag +
            ", ValiFlag="+ValiFlag +
            ", ReturnValiFlag="+ReturnValiFlag +"]";
    }
}
