package com.sinosoft.lis.entity;

import java.io.Serializable;

public class ChnlInfoExtendPojo implements Serializable{

    private String chnlInfoExtendCode;
    private String chnlInfoExtendValue;

    public ChnlInfoExtendPojo(String chnlInfoExtendCode, String chnlInfoExtendValue) {
        this.chnlInfoExtendCode = chnlInfoExtendCode;
        this.chnlInfoExtendValue = chnlInfoExtendValue;
    }

    public ChnlInfoExtendPojo() {
    }

    public String getChnlInfoExtendCode() {
        return chnlInfoExtendCode;
    }

    public void setChnlInfoExtendCode(String chnlInfoExtendCode) {
        this.chnlInfoExtendCode = chnlInfoExtendCode;
    }

    public String getChnlInfoExtendValue() {
        return chnlInfoExtendValue;
    }

    public void setChnlInfoExtendValue(String chnlInfoExtendValue) {
        this.chnlInfoExtendValue = chnlInfoExtendValue;
    }

    @Override
    public String toString() {
        return "ChnlInfoExtendPojo{" +
                "chnlInfoExtendCode='" + chnlInfoExtendCode + '\'' +
                ", chnlInfoExtendValue='" + chnlInfoExtendValue + '\'' +
                '}';
    }
}
