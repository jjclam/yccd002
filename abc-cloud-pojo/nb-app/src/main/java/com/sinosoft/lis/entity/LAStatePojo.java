/**
 * Copyright (c) 2018 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.entity;

import com.sinosoft.cloud.cache.annotation.RedisPrimaryHKey;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.Pojo;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;

import java.io.Serializable;

/**
 * <p>ClassName: LAStatePojo </p>
 * <p>Copyright: Copyright (c) 2018</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2018-03-27
 */
public class LAStatePojo implements Pojo,Serializable {
    // @Field
    /** Managecom */
    private String ManageCom; 
    /** Branchtype */
    @RedisPrimaryHKey
    private String BranchType; 
    /** Branchtype2 */
    private String BranchType2; 
    /** Startdate */
    @RedisPrimaryHKey
    private String StartDate; 
    /** Enddate */
    @RedisPrimaryHKey
    private String EndDate; 
    /** Statetype */
    @RedisPrimaryHKey
    private String StateType; 
    /** Statevalue */
    private String StateValue; 
    /** Statevalue1 */
    private String StateValue1; 
    /** Statevalue2 */
    private String StateValue2; 
    /** Objecttype */
    private String ObjectType; 
    /** Objectid */
    @RedisPrimaryHKey
    private String ObjectId; 
    /** Objectlevel */
    private String ObjectLevel; 
    /** Operator */
    private String Operator; 
    /** Makedate */
    private String  MakeDate;
    /** Maketime */
    private String MakeTime; 
    /** Modifydate */
    private String  ModifyDate;
    /** Modifytime */
    private String ModifyTime; 
    /** Branchattr */
    private String BranchAttr; 
    /** Gradedate */
    private String  GradeDate;


    public static final int FIELDNUM = 19;    // 数据库表的字段个数

    private FDate fDate = new FDate();        // 处理日期
    public String getManageCom() {
        return ManageCom;
    }
    public void setManageCom(String aManageCom) {
        ManageCom = aManageCom;
    }
    public String getBranchType() {
        return BranchType;
    }
    public void setBranchType(String aBranchType) {
        BranchType = aBranchType;
    }
    public String getBranchType2() {
        return BranchType2;
    }
    public void setBranchType2(String aBranchType2) {
        BranchType2 = aBranchType2;
    }
    public String getStartDate() {
        return StartDate;
    }
    public void setStartDate(String aStartDate) {
        StartDate = aStartDate;
    }
    public String getEndDate() {
        return EndDate;
    }
    public void setEndDate(String aEndDate) {
        EndDate = aEndDate;
    }
    public String getStateType() {
        return StateType;
    }
    public void setStateType(String aStateType) {
        StateType = aStateType;
    }
    public String getStateValue() {
        return StateValue;
    }
    public void setStateValue(String aStateValue) {
        StateValue = aStateValue;
    }
    public String getStateValue1() {
        return StateValue1;
    }
    public void setStateValue1(String aStateValue1) {
        StateValue1 = aStateValue1;
    }
    public String getStateValue2() {
        return StateValue2;
    }
    public void setStateValue2(String aStateValue2) {
        StateValue2 = aStateValue2;
    }
    public String getObjectType() {
        return ObjectType;
    }
    public void setObjectType(String aObjectType) {
        ObjectType = aObjectType;
    }
    public String getObjectId() {
        return ObjectId;
    }
    public void setObjectId(String aObjectId) {
        ObjectId = aObjectId;
    }
    public String getObjectLevel() {
        return ObjectLevel;
    }
    public void setObjectLevel(String aObjectLevel) {
        ObjectLevel = aObjectLevel;
    }
    public String getOperator() {
        return Operator;
    }
    public void setOperator(String aOperator) {
        Operator = aOperator;
    }
    public String getMakeDate() {
        return MakeDate;
    }
    public void setMakeDate(String aMakeDate) {
        MakeDate = aMakeDate;
    }
    public String getMakeTime() {
        return MakeTime;
    }
    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }
    public String getModifyDate() {
        return ModifyDate;
    }
    public void setModifyDate(String aModifyDate) {
        ModifyDate = aModifyDate;
    }
    public String getModifyTime() {
        return ModifyTime;
    }
    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }
    public String getBranchAttr() {
        return BranchAttr;
    }
    public void setBranchAttr(String aBranchAttr) {
        BranchAttr = aBranchAttr;
    }
    public String getGradeDate() {
        return GradeDate;
    }
    public void setGradeDate(String aGradeDate) {
        GradeDate = aGradeDate;
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("ManageCom") ) {
            return 0;
        }
        if( strFieldName.equals("BranchType") ) {
            return 1;
        }
        if( strFieldName.equals("BranchType2") ) {
            return 2;
        }
        if( strFieldName.equals("StartDate") ) {
            return 3;
        }
        if( strFieldName.equals("EndDate") ) {
            return 4;
        }
        if( strFieldName.equals("StateType") ) {
            return 5;
        }
        if( strFieldName.equals("StateValue") ) {
            return 6;
        }
        if( strFieldName.equals("StateValue1") ) {
            return 7;
        }
        if( strFieldName.equals("StateValue2") ) {
            return 8;
        }
        if( strFieldName.equals("ObjectType") ) {
            return 9;
        }
        if( strFieldName.equals("ObjectId") ) {
            return 10;
        }
        if( strFieldName.equals("ObjectLevel") ) {
            return 11;
        }
        if( strFieldName.equals("Operator") ) {
            return 12;
        }
        if( strFieldName.equals("MakeDate") ) {
            return 13;
        }
        if( strFieldName.equals("MakeTime") ) {
            return 14;
        }
        if( strFieldName.equals("ModifyDate") ) {
            return 15;
        }
        if( strFieldName.equals("ModifyTime") ) {
            return 16;
        }
        if( strFieldName.equals("BranchAttr") ) {
            return 17;
        }
        if( strFieldName.equals("GradeDate") ) {
            return 18;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "ManageCom";
                break;
            case 1:
                strFieldName = "BranchType";
                break;
            case 2:
                strFieldName = "BranchType2";
                break;
            case 3:
                strFieldName = "StartDate";
                break;
            case 4:
                strFieldName = "EndDate";
                break;
            case 5:
                strFieldName = "StateType";
                break;
            case 6:
                strFieldName = "StateValue";
                break;
            case 7:
                strFieldName = "StateValue1";
                break;
            case 8:
                strFieldName = "StateValue2";
                break;
            case 9:
                strFieldName = "ObjectType";
                break;
            case 10:
                strFieldName = "ObjectId";
                break;
            case 11:
                strFieldName = "ObjectLevel";
                break;
            case 12:
                strFieldName = "Operator";
                break;
            case 13:
                strFieldName = "MakeDate";
                break;
            case 14:
                strFieldName = "MakeTime";
                break;
            case 15:
                strFieldName = "ModifyDate";
                break;
            case 16:
                strFieldName = "ModifyTime";
                break;
            case 17:
                strFieldName = "BranchAttr";
                break;
            case 18:
                strFieldName = "GradeDate";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "MANAGECOM":
                return Schema.TYPE_STRING;
            case "BRANCHTYPE":
                return Schema.TYPE_STRING;
            case "BRANCHTYPE2":
                return Schema.TYPE_STRING;
            case "STARTDATE":
                return Schema.TYPE_STRING;
            case "ENDDATE":
                return Schema.TYPE_STRING;
            case "STATETYPE":
                return Schema.TYPE_STRING;
            case "STATEVALUE":
                return Schema.TYPE_STRING;
            case "STATEVALUE1":
                return Schema.TYPE_STRING;
            case "STATEVALUE2":
                return Schema.TYPE_STRING;
            case "OBJECTTYPE":
                return Schema.TYPE_STRING;
            case "OBJECTID":
                return Schema.TYPE_STRING;
            case "OBJECTLEVEL":
                return Schema.TYPE_STRING;
            case "OPERATOR":
                return Schema.TYPE_STRING;
            case "MAKEDATE":
                return Schema.TYPE_STRING;
            case "MAKETIME":
                return Schema.TYPE_STRING;
            case "MODIFYDATE":
                return Schema.TYPE_STRING;
            case "MODIFYTIME":
                return Schema.TYPE_STRING;
            case "BRANCHATTR":
                return Schema.TYPE_STRING;
            case "GRADEDATE":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_STRING;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_STRING;
            case 6:
                return Schema.TYPE_STRING;
            case 7:
                return Schema.TYPE_STRING;
            case 8:
                return Schema.TYPE_STRING;
            case 9:
                return Schema.TYPE_STRING;
            case 10:
                return Schema.TYPE_STRING;
            case 11:
                return Schema.TYPE_STRING;
            case 12:
                return Schema.TYPE_STRING;
            case 13:
                return Schema.TYPE_STRING;
            case 14:
                return Schema.TYPE_STRING;
            case 15:
                return Schema.TYPE_STRING;
            case 16:
                return Schema.TYPE_STRING;
            case 17:
                return Schema.TYPE_STRING;
            case 18:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("ManageCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
        }
        if (FCode.equalsIgnoreCase("BranchType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BranchType));
        }
        if (FCode.equalsIgnoreCase("BranchType2")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BranchType2));
        }
        if (FCode.equalsIgnoreCase("StartDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StartDate));
        }
        if (FCode.equalsIgnoreCase("EndDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(EndDate));
        }
        if (FCode.equalsIgnoreCase("StateType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StateType));
        }
        if (FCode.equalsIgnoreCase("StateValue")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StateValue));
        }
        if (FCode.equalsIgnoreCase("StateValue1")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StateValue1));
        }
        if (FCode.equalsIgnoreCase("StateValue2")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StateValue2));
        }
        if (FCode.equalsIgnoreCase("ObjectType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ObjectType));
        }
        if (FCode.equalsIgnoreCase("ObjectId")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ObjectId));
        }
        if (FCode.equalsIgnoreCase("ObjectLevel")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ObjectLevel));
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeDate));
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyDate));
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (FCode.equalsIgnoreCase("BranchAttr")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BranchAttr));
        }
        if (FCode.equalsIgnoreCase("GradeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GradeDate));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = String.valueOf(ManageCom);
                break;
            case 1:
                strFieldValue = String.valueOf(BranchType);
                break;
            case 2:
                strFieldValue = String.valueOf(BranchType2);
                break;
            case 3:
                strFieldValue = String.valueOf(StartDate);
                break;
            case 4:
                strFieldValue = String.valueOf(EndDate);
                break;
            case 5:
                strFieldValue = String.valueOf(StateType);
                break;
            case 6:
                strFieldValue = String.valueOf(StateValue);
                break;
            case 7:
                strFieldValue = String.valueOf(StateValue1);
                break;
            case 8:
                strFieldValue = String.valueOf(StateValue2);
                break;
            case 9:
                strFieldValue = String.valueOf(ObjectType);
                break;
            case 10:
                strFieldValue = String.valueOf(ObjectId);
                break;
            case 11:
                strFieldValue = String.valueOf(ObjectLevel);
                break;
            case 12:
                strFieldValue = String.valueOf(Operator);
                break;
            case 13:
                strFieldValue = String.valueOf(MakeDate);
                break;
            case 14:
                strFieldValue = String.valueOf(MakeTime);
                break;
            case 15:
                strFieldValue = String.valueOf(ModifyDate);
                break;
            case 16:
                strFieldValue = String.valueOf(ModifyTime);
                break;
            case 17:
                strFieldValue = String.valueOf(BranchAttr);
                break;
            case 18:
                strFieldValue = String.valueOf(GradeDate);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("ManageCom")) {
            if( FValue != null && !FValue.equals(""))
            {
                ManageCom = FValue.trim();
            }
            else
                ManageCom = null;
        }
        if (FCode.equalsIgnoreCase("BranchType")) {
            if( FValue != null && !FValue.equals(""))
            {
                BranchType = FValue.trim();
            }
            else
                BranchType = null;
        }
        if (FCode.equalsIgnoreCase("BranchType2")) {
            if( FValue != null && !FValue.equals(""))
            {
                BranchType2 = FValue.trim();
            }
            else
                BranchType2 = null;
        }
        if (FCode.equalsIgnoreCase("StartDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                StartDate = FValue.trim();
            }
            else
                StartDate = null;
        }
        if (FCode.equalsIgnoreCase("EndDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                EndDate = FValue.trim();
            }
            else
                EndDate = null;
        }
        if (FCode.equalsIgnoreCase("StateType")) {
            if( FValue != null && !FValue.equals(""))
            {
                StateType = FValue.trim();
            }
            else
                StateType = null;
        }
        if (FCode.equalsIgnoreCase("StateValue")) {
            if( FValue != null && !FValue.equals(""))
            {
                StateValue = FValue.trim();
            }
            else
                StateValue = null;
        }
        if (FCode.equalsIgnoreCase("StateValue1")) {
            if( FValue != null && !FValue.equals(""))
            {
                StateValue1 = FValue.trim();
            }
            else
                StateValue1 = null;
        }
        if (FCode.equalsIgnoreCase("StateValue2")) {
            if( FValue != null && !FValue.equals(""))
            {
                StateValue2 = FValue.trim();
            }
            else
                StateValue2 = null;
        }
        if (FCode.equalsIgnoreCase("ObjectType")) {
            if( FValue != null && !FValue.equals(""))
            {
                ObjectType = FValue.trim();
            }
            else
                ObjectType = null;
        }
        if (FCode.equalsIgnoreCase("ObjectId")) {
            if( FValue != null && !FValue.equals(""))
            {
                ObjectId = FValue.trim();
            }
            else
                ObjectId = null;
        }
        if (FCode.equalsIgnoreCase("ObjectLevel")) {
            if( FValue != null && !FValue.equals(""))
            {
                ObjectLevel = FValue.trim();
            }
            else
                ObjectLevel = null;
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            if( FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
                Operator = null;
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeDate = FValue.trim();
            }
            else
                MakeDate = null;
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
                MakeTime = null;
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyDate = FValue.trim();
            }
            else
                ModifyDate = null;
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
                ModifyTime = null;
        }
        if (FCode.equalsIgnoreCase("BranchAttr")) {
            if( FValue != null && !FValue.equals(""))
            {
                BranchAttr = FValue.trim();
            }
            else
                BranchAttr = null;
        }
        if (FCode.equalsIgnoreCase("GradeDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                GradeDate = FValue.trim();
            }
            else
                GradeDate = null;
        }
        return true;
    }


    public String toString() {
    return "LAStatePojo [" +
            "ManageCom="+ManageCom +
            ", BranchType="+BranchType +
            ", BranchType2="+BranchType2 +
            ", StartDate="+StartDate +
            ", EndDate="+EndDate +
            ", StateType="+StateType +
            ", StateValue="+StateValue +
            ", StateValue1="+StateValue1 +
            ", StateValue2="+StateValue2 +
            ", ObjectType="+ObjectType +
            ", ObjectId="+ObjectId +
            ", ObjectLevel="+ObjectLevel +
            ", Operator="+Operator +
            ", MakeDate="+MakeDate +
            ", MakeTime="+MakeTime +
            ", ModifyDate="+ModifyDate +
            ", ModifyTime="+ModifyTime +
            ", BranchAttr="+BranchAttr +
            ", GradeDate="+GradeDate +"]";
    }
}
