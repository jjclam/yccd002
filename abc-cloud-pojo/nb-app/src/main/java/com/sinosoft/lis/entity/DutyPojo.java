package com.sinosoft.lis.entity;

import com.sinosoft.utility.Pojo;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;

import java.io.Serializable;

public class DutyPojo implements Pojo, Serializable {
    private String riskCode;

    private String riskVersion;

    private String dutyCode;

    private String dutyName;

    private String amnt;

    private String prem;

    private String floatRate;

    private String valiDate;

    private String endDate;

    private String payIntv;

    private String payEndYearFlag;

    private String payEndYear;

    private String insuYearFlag;

    private String insuYear;

    private String rnewFlag;

    private String autoPayFlag;
    private String calMode;
    public static final int FIELDNUM = 17;    // 数据库表的字段个数

    @Override
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("riskCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(riskCode));
        }
        if (FCode.equalsIgnoreCase("riskVersion")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(riskVersion));
        }
        if (FCode.equalsIgnoreCase("dutyCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(dutyCode));
        }
        if (FCode.equalsIgnoreCase("dutyName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(dutyName));
        }
        if (FCode.equalsIgnoreCase("amnt")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(amnt));
        }
        if (FCode.equalsIgnoreCase("prem")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(prem));
        }
        if (FCode.equalsIgnoreCase("floatRate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(floatRate));
        }
        if (FCode.equalsIgnoreCase("valiDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(valiDate));
        }

        if (FCode.equalsIgnoreCase("endDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(endDate));
        }
        if (FCode.equalsIgnoreCase("payIntv")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(payIntv));
        }
        if (FCode.equalsIgnoreCase("payEndYearFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(payEndYearFlag));
        }
        if (FCode.equalsIgnoreCase("insuYear")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(insuYear));
        }
        if (FCode.equalsIgnoreCase("rnewFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(rnewFlag));
        }
        if (FCode.equalsIgnoreCase("autoPayFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(autoPayFlag));
        }
        if (FCode.equalsIgnoreCase("calMode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(calMode));
        }

        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }

    @Override
    public String getV(int nIndex) {
        String strFieldValue = "";
        switch (nIndex) {
            case 0:
                strFieldValue = String.valueOf(riskCode);
                break;
            case 1:
                strFieldValue = String.valueOf(riskVersion);
                break;
            case 2:
                strFieldValue = String.valueOf(dutyCode);
                break;
            case 3:
                strFieldValue = String.valueOf(dutyName);
                break;
            case 4:
                strFieldValue = String.valueOf(amnt);
                break;
            case 5:
                strFieldValue = String.valueOf(prem);
                break;
            case 6:
                strFieldValue = String.valueOf(floatRate);
                break;
            case 7:
                strFieldValue = String.valueOf(valiDate);
                break;
            case 8:
                strFieldValue = String.valueOf(endDate);
                break;
            case 9:
                strFieldValue = String.valueOf(payIntv);
                break;
            case 10:
                strFieldValue = String.valueOf(payEndYearFlag);
                break;
            case 11:
                strFieldValue = String.valueOf(payEndYear);
                break;
            case 12:
                strFieldValue = String.valueOf(insuYearFlag);
                break;
            case 13:
                strFieldValue = String.valueOf(insuYear);
                break;
            case 14:
                strFieldValue = String.valueOf(rnewFlag);
                break;
            case 15:
                strFieldValue = String.valueOf(autoPayFlag);
                break;
            case 16:
                strFieldValue = String.valueOf(calMode);
                break;
            default:
                strFieldValue = "";
        }
        if (strFieldValue.equals("")) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    @Override
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "riskCode":
                return Schema.TYPE_STRING;
            case "riskVersion":
                return Schema.TYPE_STRING;
            case "dutyCode":
                return Schema.TYPE_STRING;
            case "dutyName":
                return Schema.TYPE_STRING;
            case "amnt":
                return Schema.TYPE_STRING;
            case "prem":
                return Schema.TYPE_STRING;
            case "floatRate":
                return Schema.TYPE_STRING;
            case "valiDate":
                return Schema.TYPE_STRING;
            case "endDate":
                return Schema.TYPE_STRING;
            case "payIntv":
                return Schema.TYPE_STRING;
            case "payEndYearFlag":
                return Schema.TYPE_STRING;
            case "payEndYear":
                return Schema.TYPE_STRING;
            case "insuYearFlag":
                return Schema.TYPE_STRING;
            case "insuYear":
                return Schema.TYPE_STRING;
            case "rnewFlag":
                return Schema.TYPE_STRING;
            case "autoPayFlag":
                return Schema.TYPE_STRING;
            case "calMode":
                return Schema.TYPE_STRING;


            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    @Override
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_STRING;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_STRING;
            case 6:
                return Schema.TYPE_STRING;
            case 7:
                return Schema.TYPE_STRING;
            case 8:
                return Schema.TYPE_STRING;
            case 9:
                return Schema.TYPE_STRING;
            case 10:
                return Schema.TYPE_STRING;
            case 11:
                return Schema.TYPE_STRING;
            case 12:
                return Schema.TYPE_STRING;
            case 13:
                return Schema.TYPE_STRING;
            case 14:
                return Schema.TYPE_STRING;
            case 15:
                return Schema.TYPE_STRING;
            case 16:
                return Schema.TYPE_STRING;

            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    @Override
    public int getFieldCount() {
        return FIELDNUM;
    }

    @Override
    public int getFieldIndex(String strFieldName) {
        if (strFieldName.equals("riskCode")) {
            return 0;
        }
        if (strFieldName.equals("riskVersion")) {
            return 1;
        }
        if (strFieldName.equals("dutyCode")) {
            return 2;
        }
        if (strFieldName.equals("dutyName")) {
            return 3;
        }
        if (strFieldName.equals("amnt")) {
            return 4;
        }
        if (strFieldName.equals("prem")) {
            return 5;
        }
        if (strFieldName.equals("floatRate")) {
            return 6;
        }
        if (strFieldName.equals("valiDate")) {
            return 7;
        }
        if (strFieldName.equals("endDate")) {
            return 8;
        }
        if (strFieldName.equals("payIntv")) {
            return 9;
        }
        if (strFieldName.equals("payEndYearFlag")) {
            return 10;
        }
        if (strFieldName.equals("payEndYear")) {
            return 11;
        }
        if (strFieldName.equals("insuYearFlag")) {
            return 12;
        }
        if (strFieldName.equals("insuYear")) {
            return 13;
        }
        if (strFieldName.equals("rnewFlag")) {
            return 14;
        }
        if (strFieldName.equals("autoPayFlag")) {
            return 15;
        }
        if (strFieldName.equals("calMode")) {
            return 16;
        }
        return -1;
    }



    @Override
    public String getFieldName(int nFieldIndex) {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "riskCode";
                break;
            case 1:
                strFieldName = "riskVersion";
                break;
            case 2:
                strFieldName = "dutyCode";
                break;
            case 3:
                strFieldName = "dutyName";
                break;
            case 4:
                strFieldName = "amnt";
                break;
            case 5:
                strFieldName = "prem";
                break;
            case 6:
                strFieldName = "floatRate";
                break;
            case 7:
                strFieldName = "valiDate";
                break;
            case 8:
                strFieldName = "endDate";
                break;
            case 9:
                strFieldName = "payIntv";
                break;
            case 10:
                strFieldName = "payEndYearFlag";
                break;
            case 11:
                strFieldName = "payEndYear";
                break;
            case 12:
                strFieldName = "insuYearFlag";
                break;
            case 13:
                strFieldName = "insuYear";
                break;
            case 14:
                strFieldName = "rnewFlag";
                break;
            case 15:
                strFieldName = "autoPayFlag";
                break;
            case 16:
                strFieldName = "calMode";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    @Override
    public boolean setV(String FCode, String FValue) {
        if (StrTool.cTrim(FCode).equals("")){
            return false;
         }
        if (FCode.equalsIgnoreCase("riskCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                riskCode = FValue.trim();
            }
            else
                riskCode = null;
        }
        if (FCode.equalsIgnoreCase("riskVersion")) {
            if( FValue != null && !FValue.equals(""))
            {
                riskVersion = FValue.trim();
            }
            else
                riskVersion = null;
        }
        if (FCode.equalsIgnoreCase("dutyCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                dutyCode = FValue.trim();
            }
            else
                dutyCode = null;
        }
        if (FCode.equalsIgnoreCase("dutyName")) {
            if( FValue != null && !FValue.equals(""))
            {
                dutyName = FValue.trim();
            }
            else
                dutyName = null;
        }
        if (FCode.equalsIgnoreCase("amnt")) {
            if( FValue != null && !FValue.equals(""))
            {
                amnt = FValue.trim();
            }
            else
                amnt = null;
        }
        if (FCode.equalsIgnoreCase("prem")) {
            if( FValue != null && !FValue.equals(""))
            {
                prem = FValue.trim();
            }
            else
                prem = null;
        }
        if (FCode.equalsIgnoreCase("floatRate")) {
            if( FValue != null && !FValue.equals(""))
            {
                floatRate = FValue.trim();
            }
            else
                floatRate = null;
        }
        if (FCode.equalsIgnoreCase("valiDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                valiDate = FValue.trim();
            }
            else
                valiDate = null;
        }
        if (FCode.equalsIgnoreCase("endDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                endDate = FValue.trim();
            }
            else
                endDate = null;
        }
        if (FCode.equalsIgnoreCase("payIntv")) {
            if( FValue != null && !FValue.equals(""))
            {
                payIntv = FValue.trim();
            }
            else
                payIntv = null;
        }
        if (FCode.equalsIgnoreCase("payEndYearFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                payEndYearFlag = FValue.trim();
            }
            else
                payEndYearFlag = null;
        }
        if (FCode.equalsIgnoreCase("payEndYear")) {
            if( FValue != null && !FValue.equals(""))
            {
                payEndYear = FValue.trim();
            }
            else
                payEndYear = null;
        }
        if (FCode.equalsIgnoreCase("insuYearFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                insuYearFlag = FValue.trim();
            }
            else
                insuYearFlag = null;
        }
        if (FCode.equalsIgnoreCase("insuYear")) {
            if( FValue != null && !FValue.equals(""))
            {
                insuYear = FValue.trim();
            }
            else
                insuYear = null;
        }
        if (FCode.equalsIgnoreCase("rnewFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                rnewFlag = FValue.trim();
            }
            else
                rnewFlag = null;
        }
        if (FCode.equalsIgnoreCase("autoPayFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                autoPayFlag = FValue.trim();
            }
            else
                autoPayFlag = null;
        }
        if (FCode.equalsIgnoreCase("calMode")) {
            if( FValue != null && !FValue.equals(""))
            {
                calMode = FValue.trim();
            }
            else
                calMode = null;
        }

         return true;
    }

    public String getCalMode() {
        return calMode;
    }

    public void setCalMode(String calMode) {
        this.calMode = calMode;
    }

    public String getRiskCode() {
        return riskCode;
    }

    public void setRiskCode(String riskCode) {
        this.riskCode = riskCode;
    }

    public String getRiskVersion() {
        return riskVersion;
    }

    public void setRiskVersion(String riskVersion) {
        this.riskVersion = riskVersion;
    }

    public String getDutyCode() {
        return dutyCode;
    }

    public void setDutyCode(String dutyCode) {
        this.dutyCode = dutyCode;
    }

    public String getDutyName() {
        return dutyName;
    }

    public void setDutyName(String dutyName) {
        this.dutyName = dutyName;
    }

    public String getAmnt() {
        return amnt;
    }

    public void setAmnt(String amnt) {
        this.amnt = amnt;
    }

    public String getPrem() {
        return prem;
    }

    public void setPrem(String prem) {
        this.prem = prem;
    }

    public String getFloatRate() {
        return floatRate;
    }

    public void setFloatRate(String floatRate) {
        this.floatRate = floatRate;
    }

    public String getValiDate() {
        return valiDate;
    }

    public void setValiDate(String valiDate) {
        this.valiDate = valiDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getPayIntv() {
        return payIntv;
    }

    public void setPayIntv(String payIntv) {
        this.payIntv = payIntv;
    }

    public String getPayEndYearFlag() {
        return payEndYearFlag;
    }

    public void setPayEndYearFlag(String payEndYearFlag) {
        this.payEndYearFlag = payEndYearFlag;
    }

    public String getPayEndYear() {
        return payEndYear;
    }

    public void setPayEndYear(String payEndYear) {
        this.payEndYear = payEndYear;
    }

    public String getInsuYearFlag() {
        return insuYearFlag;
    }

    public void setInsuYearFlag(String insuYearFlag) {
        this.insuYearFlag = insuYearFlag;
    }

    public String getInsuYear() {
        return insuYear;
    }

    public void setInsuYear(String insuYear) {
        this.insuYear = insuYear;
    }

    public String getRnewFlag() {
        return rnewFlag;
    }

    public void setRnewFlag(String rnewFlag) {
        this.rnewFlag = rnewFlag;
    }

    public String getAutoPayFlag() {
        return autoPayFlag;
    }

    public void setAutoPayFlag(String autoPayFlag) {
        this.autoPayFlag = autoPayFlag;
    }


}
