/**
 * Copyright (c) 2019 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.vdb;

import com.sinosoft.lis.vschema.LCWechatRelaTionSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.DBConnPool;
import com.sinosoft.utility.DBOper;
import com.sinosoft.utility.ExceptionUtils;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;

/**
 * <p>ClassName: LCWechatRelaTionDBSet </p>
 * <p>Description: DB层多记录数据库操作类文件 </p>
 * <p>Copyright: Copyright (c) 2019</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2019-03-14
 */
public class LCWechatRelaTionDBSet extends LCWechatRelaTionSet {
    // @Field
    private Connection con;
    private DBOper db;
    /**
    * flag = true: 传入Connection
    * flag = false: 不传入Connection
    **/
    private boolean mflag = false;

    // @Constructor
    public LCWechatRelaTionDBSet(Connection tConnection) {
        con = tConnection;
        db = new DBOper(con,"LCWechatRelaTion");
        mflag = true;
    }

    public LCWechatRelaTionDBSet() {
        db = new DBOper( "LCWechatRelaTion" );
    }
    // @Method
    public boolean deleteSQL() {
        if (db.deleteSQL(this)) {
            return true;
        } else {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LCWechatRelaTionDBSet";
            tError.functionName = "deleteSQL";
            tError.errorMessage = "操作失败!";
            this.mErrors .addOneError(tError);
            return false;
        }
    }

    /**
     * 删除操作
     * 删除条件：主键
     * @return boolean
     */
    public boolean delete() {
        PreparedStatement pstmt = null;

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try {
            int tCount = this.size();
            pstmt = con.prepareStatement("DELETE FROM LCWechatRelaTion WHERE  1=1  AND Contno = ?");
            for (int i = 1; i <= tCount; i++) {
            if(this.get(i).getContno() == null || this.get(i).getContno().equals("null")) {
            	pstmt.setString(1,null);
            } else {
            	pstmt.setString(1, this.get(i).getContno());
            }
                pstmt.addBatch();
            }
            pstmt.executeBatch();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            ex.printStackTrace();
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LCWechatRelaTionDBSet";
            tError.functionName = "delete()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            try {
                pstmt.close();
            } catch (Exception e){
                e.printStackTrace();
            }

            if (!mflag) {
                try {
                    con.close();
                } catch (Exception e){e.printStackTrace();}
            }

            return false;
        }

        if( !mflag ) {
            try {
                con.close();
            } catch (Exception e){e.printStackTrace();}
        }

        return true;
    }

    /**
     * 更新操作
     * 更新条件：主键
     * @return boolean
     */
    public boolean update() {
        PreparedStatement pstmt = null;

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try {
            int tCount = this.size();
            pstmt = con.prepareStatement("UPDATE LCWechatRelaTion SET  Contno = ? , BeforeContno = ? , BeginContno = ? , SignDate = ? , CValiDate = ? , FirstDate = ? , ConfMakeDate = ? , PayMode = ? , BankaccNo = ? , AccName = ? , ModifyDate = ? , ModifyTime = ? , MakeDate = ? , MakeTime = ? WHERE  1=1  AND Contno = ?");
            for (int i = 1; i <= tCount; i++) {
            if(this.get(i).getContno() == null || this.get(i).getContno().equals("null")) {
            	pstmt.setString(1,null);
            } else {
            	pstmt.setString(1, this.get(i).getContno());
            }
            if(this.get(i).getBeforeContno() == null || this.get(i).getBeforeContno().equals("null")) {
            	pstmt.setString(2,null);
            } else {
            	pstmt.setString(2, this.get(i).getBeforeContno());
            }
            if(this.get(i).getBeginContno() == null || this.get(i).getBeginContno().equals("null")) {
            	pstmt.setString(3,null);
            } else {
            	pstmt.setString(3, this.get(i).getBeginContno());
            }
            if(this.get(i).getSignDate() == null || this.get(i).getSignDate().equals("null")) {
                pstmt.setDate(4,null);
            } else {
                pstmt.setDate(4, Date.valueOf(this.get(i).getSignDate()));
            }
            if(this.get(i).getCValiDate() == null || this.get(i).getCValiDate().equals("null")) {
                pstmt.setDate(5,null);
            } else {
                pstmt.setDate(5, Date.valueOf(this.get(i).getCValiDate()));
            }
            if(this.get(i).getFirstDate() == null || this.get(i).getFirstDate().equals("null")) {
                pstmt.setDate(6,null);
            } else {
                pstmt.setDate(6, Date.valueOf(this.get(i).getFirstDate()));
            }
            if(this.get(i).getConfMakeDate() == null || this.get(i).getConfMakeDate().equals("null")) {
                pstmt.setDate(7,null);
            } else {
                pstmt.setDate(7, Date.valueOf(this.get(i).getConfMakeDate()));
            }
            if(this.get(i).getPayMode() == null || this.get(i).getPayMode().equals("null")) {
            	pstmt.setString(8,null);
            } else {
            	pstmt.setString(8, this.get(i).getPayMode());
            }
            if(this.get(i).getBankaccNo() == null || this.get(i).getBankaccNo().equals("null")) {
            	pstmt.setString(9,null);
            } else {
            	pstmt.setString(9, this.get(i).getBankaccNo());
            }
            if(this.get(i).getAccName() == null || this.get(i).getAccName().equals("null")) {
            	pstmt.setString(10,null);
            } else {
            	pstmt.setString(10, this.get(i).getAccName());
            }
            if(this.get(i).getModifyDate() == null || this.get(i).getModifyDate().equals("null")) {
                pstmt.setDate(11,null);
            } else {
                pstmt.setDate(11, Date.valueOf(this.get(i).getModifyDate()));
            }
            if(this.get(i).getModifyTime() == null || this.get(i).getModifyTime().equals("null")) {
            	pstmt.setString(12,null);
            } else {
            	pstmt.setString(12, this.get(i).getModifyTime());
            }
            if(this.get(i).getMakeDate() == null || this.get(i).getMakeDate().equals("null")) {
                pstmt.setDate(13,null);
            } else {
                pstmt.setDate(13, Date.valueOf(this.get(i).getMakeDate()));
            }
            if(this.get(i).getMakeTime() == null || this.get(i).getMakeTime().equals("null")) {
            	pstmt.setString(14,null);
            } else {
            	pstmt.setString(14, this.get(i).getMakeTime());
            }
            // set where condition
            if(this.get(i).getContno() == null || this.get(i).getContno().equals("null")) {
            	pstmt.setString(15,null);
            } else {
            	pstmt.setString(15, this.get(i).getContno());
            }
                pstmt.addBatch();
            }
            pstmt.executeBatch();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            ex.printStackTrace();
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LCWechatRelaTionDBSet";
            tError.functionName = "update()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            try {
                pstmt.close();
            } catch (Exception e){e.printStackTrace();}

            if( !mflag ) {
                try {
                    con.close();
                } catch (Exception e){e.printStackTrace();}
            }

            return false;
        }

        if( !mflag ) {
            try {
                con.close();
            } catch (Exception e){e.printStackTrace();}
        }

        return true;
    }

    /**
     * 新增操作
     * @return boolean
     */
    public boolean insert() {
        PreparedStatement pstmt = null;

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try
        {
            int tCount = this.size();
            pstmt = con.prepareStatement("INSERT INTO LCWechatRelaTion VALUES( ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ?)");
            for (int i = 1; i <= tCount; i++) {
            if(this.get(i).getContno() == null || this.get(i).getContno().equals("null")) {
            	pstmt.setString(1,null);
            } else {
            	pstmt.setString(1, this.get(i).getContno());
            }
            if(this.get(i).getBeforeContno() == null || this.get(i).getBeforeContno().equals("null")) {
            	pstmt.setString(2,null);
            } else {
            	pstmt.setString(2, this.get(i).getBeforeContno());
            }
            if(this.get(i).getBeginContno() == null || this.get(i).getBeginContno().equals("null")) {
            	pstmt.setString(3,null);
            } else {
            	pstmt.setString(3, this.get(i).getBeginContno());
            }
            if(this.get(i).getSignDate() == null || this.get(i).getSignDate().equals("null")) {
                pstmt.setDate(4,null);
            } else {
                pstmt.setDate(4, Date.valueOf(this.get(i).getSignDate()));
            }
            if(this.get(i).getCValiDate() == null || this.get(i).getCValiDate().equals("null")) {
                pstmt.setDate(5,null);
            } else {
                pstmt.setDate(5, Date.valueOf(this.get(i).getCValiDate()));
            }
            if(this.get(i).getFirstDate() == null || this.get(i).getFirstDate().equals("null")) {
                pstmt.setDate(6,null);
            } else {
                pstmt.setDate(6, Date.valueOf(this.get(i).getFirstDate()));
            }
            if(this.get(i).getConfMakeDate() == null || this.get(i).getConfMakeDate().equals("null")) {
                pstmt.setDate(7,null);
            } else {
                pstmt.setDate(7, Date.valueOf(this.get(i).getConfMakeDate()));
            }
            if(this.get(i).getPayMode() == null || this.get(i).getPayMode().equals("null")) {
            	pstmt.setString(8,null);
            } else {
            	pstmt.setString(8, this.get(i).getPayMode());
            }
            if(this.get(i).getBankaccNo() == null || this.get(i).getBankaccNo().equals("null")) {
            	pstmt.setString(9,null);
            } else {
            	pstmt.setString(9, this.get(i).getBankaccNo());
            }
            if(this.get(i).getAccName() == null || this.get(i).getAccName().equals("null")) {
            	pstmt.setString(10,null);
            } else {
            	pstmt.setString(10, this.get(i).getAccName());
            }
            if(this.get(i).getModifyDate() == null || this.get(i).getModifyDate().equals("null")) {
                pstmt.setDate(11,null);
            } else {
                pstmt.setDate(11, Date.valueOf(this.get(i).getModifyDate()));
            }
            if(this.get(i).getModifyTime() == null || this.get(i).getModifyTime().equals("null")) {
            	pstmt.setString(12,null);
            } else {
            	pstmt.setString(12, this.get(i).getModifyTime());
            }
            if(this.get(i).getMakeDate() == null || this.get(i).getMakeDate().equals("null")) {
                pstmt.setDate(13,null);
            } else {
                pstmt.setDate(13, Date.valueOf(this.get(i).getMakeDate()));
            }
            if(this.get(i).getMakeTime() == null || this.get(i).getMakeTime().equals("null")) {
            	pstmt.setString(14,null);
            } else {
            	pstmt.setString(14, this.get(i).getMakeTime());
            }
                pstmt.addBatch();
            }
            pstmt.executeBatch();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            ex.printStackTrace();
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LCWechatRelaTionDBSet";
            tError.functionName = "insert()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            try {
                pstmt.close();
            } catch (Exception e){e.printStackTrace();}

            if( !mflag ) {
                try {
                    con.close();
                } catch (Exception e){e.printStackTrace();}
            }

            return false;
        }

        if( !mflag ) {
            try {
                con.close();
            } catch (Exception e){e.printStackTrace();}
        }

        return true;
    }
}
