package com.sinosoft.lis.entity;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Administrator on 2017/10/19.
 */
public class CancelTheDealPojo implements Serializable {

    private ArrayList<LCAppntPojo> lcAppntPojos;
    private ArrayList<LBAppntPojo> lbAppntPojos;
    private ArrayList<LCBnfPojo> lcBnfPojos;
    private ArrayList<LBBnfPojo> lbBnfPojos;
    private ArrayList<LCInsuredPojo> lcInsuredPojos;
    private ArrayList<LBInsuredPojo> lbInsuredPojos;
    private ArrayList<LCGetPojo> lcGetPojos;
    private ArrayList<LBGetPojo> lbGetPojos;
    private ArrayList<LCPremPojo> lcPremPojos;
    private ArrayList<LBPremPojo> lbPremPojos;
    private ArrayList<LCDutyPojo> lcDutyPojos;
    private ArrayList<LBDutyPojo> lbDutyPojos;
    private ArrayList<LCCustomerImpartParamsPojo> lcCustomerImpartParamsPojos;
    private ArrayList<LBCustomerImpartParamsPojo> lbCustomerImpartParamsPojos;
    private ArrayList<LCCustomerImpartPojo> lcCustomerImpartPojos;
    private ArrayList<LBCustomerImpartPojo> lbCustomerImpartPojos;
    private ArrayList<LCInsureAccClassPojo> lcInsureAccClassPojos;
    private ArrayList<LBInsureAccClassPojo> lbInsureAccClassPojos;
    private ArrayList<LCInsureAccPojo> lcInsureAccPojos;
    private ArrayList<LBInsureAccPojo> lbInsureAccPojos;
    private ArrayList<LCInsureAccClassFeePojo> lcInsureAccClassFeePojos;
    private ArrayList<LBInsureAccClassFeePojo> lbInsureAccClassFeePojos;
    private ArrayList<LCInsureAccFeePojo> lcInsureAccFeePojos;
    private ArrayList<LBInsureAccFeePojo> lbInsureAccFeePojos;
    private ArrayList<LCInsureAccFeeTracePojo> lcInsureAccFeeTracePojos;
    private ArrayList<LBInsureAccFeeTracePojo> lbInsureAccFeeTracePojos;
    private ArrayList<LCInsureAccTracePojo> lcInsureAccTracePojos;
    private ArrayList<LBInsureAccTracePojo> lbInsureAccTracePojos;
    private ArrayList<LACommisionDetailPojo> laCommisionDetailPojos;
    private ArrayList<LACommisionDetailBPojo> laCommisionDetailBPojos;
    private ArrayList<LCPolPojo> lcPolPojos;
    private ArrayList<LBPolPojo> lbPolPojos;
    private ArrayList<LCContPojo> lcContPojo;
    private ArrayList<LBContPojo> lbContPojo;
    private ArrayList<LKTransStatusPojo> lkTransStatusPojos;
    private ArrayList<LCAudVidRecordPojo> lcAudVidRecordPojos;
    private ArrayList<LBAudVidRecordPojo> lbAudVidRecordPojos;
    private ArrayList<VMS_OUTPUT_CUSTOMERPojo> vms_output_customerPojos;
    //5020万能险新增pojo
    private ArrayList<LCGetToAccPojo> lcGetToAccPojos;
    private ArrayList<LCPremToAccPojo> lcPremToAccPojos;

    //团险业务数据
    private ArrayList<LCGrpContPojo>lcGrpContPojos;
    private ArrayList<LCGrpPolPojo>lcGrpPolPojos;
    private ArrayList<LCGrpAppntPojo>lcGrpAppntPojos;
    private ArrayList<LCGrpAddressPojo>lcGrpAddressPojos;
    private ArrayList<LCAgentToContPojo>lcAgentToContPojos;
    private ArrayList<LCAgentComInfoPojo>lcAgentComInfoPojos;
    private ArrayList<LCPolicyInfoPojo>lcPolicyInfoPojos;
    private ArrayList<LCContPlanPojo>lcContPlanPojos;
    private ArrayList<LCDutySubPojo>lcDutySubPojos;
    private ArrayList<LCContPlanDetailPojo>lcContPlanDetailPojos;
    private ArrayList<LCContPlanDetailSubPojo>lcContPlanDetailSubPojos;

    private ArrayList<LBGrpContPojo>lbGrpContPojos;
    private ArrayList<LBGrpPolPojo>lbGrpPolPojos;
    private ArrayList<LBGrpAppntPojo>lbGrpAppntPojos;
    private ArrayList<LBGrpAddressPojo>lbGrpAddressPojos;
    private ArrayList<LBAgentToContPojo>lbAgentToContPojos;
    private ArrayList<LBAgentComInfoPojo>lbAgentComInfoPojos;
    private ArrayList<LBPolicyInfoPojo>lbPolicyInfoPojos;
    private ArrayList<LBContPlanPojo>lbContPlanPojos;
    private ArrayList<LBDutySubPojo>lbDutySubPojos;
    private ArrayList<LBContPlanDetailPojo>lbContPlanDetailPojos;
    private ArrayList<LBContPlanDetailSubPojo>lbContPlanDetailSubPojos;
    private ArrayList<LCAIPContRegisterPojo>lcaipContRegisterPojos;
    private ArrayList<LBAIPContRegisterPojo>lbaipContRegisterPojos;

    public ArrayList<LCAIPContRegisterPojo> getLcaipContRegisterPojos() {
        return lcaipContRegisterPojos;
    }

    public void setLcaipContRegisterPojos(ArrayList<LCAIPContRegisterPojo> lcaipContRegisterPojos) {
        this.lcaipContRegisterPojos = lcaipContRegisterPojos;
    }

    public ArrayList<LBAIPContRegisterPojo> getLbaipContRegisterPojos() {
        return lbaipContRegisterPojos;
    }

    public void setLbaipContRegisterPojos(ArrayList<LBAIPContRegisterPojo> lbaipContRegisterPojos) {
        this.lbaipContRegisterPojos = lbaipContRegisterPojos;
    }

    public ArrayList<LBGrpContPojo> getLbGrpContPojos() {
        return lbGrpContPojos;
    }

    public void setLbGrpContPojos(ArrayList<LBGrpContPojo> lbGrpContPojos) {
        this.lbGrpContPojos = lbGrpContPojos;
    }

    public ArrayList<LBGrpPolPojo> getLbGrpPolPojos() {
        return lbGrpPolPojos;
    }

    public void setLbGrpPolPojos(ArrayList<LBGrpPolPojo> lbGrpPolPojos) {
        this.lbGrpPolPojos = lbGrpPolPojos;
    }

    public ArrayList<LBGrpAppntPojo> getLbGrpAppntPojos() {
        return lbGrpAppntPojos;
    }

    public void setLbGrpAppntPojos(ArrayList<LBGrpAppntPojo> lbGrpAppntPojos) {
        this.lbGrpAppntPojos = lbGrpAppntPojos;
    }

    public ArrayList<LBGrpAddressPojo> getLbGrpAddressPojos() {
        return lbGrpAddressPojos;
    }

    public void setLbGrpAddressPojos(ArrayList<LBGrpAddressPojo> lbGrpAddressPojos) {
        this.lbGrpAddressPojos = lbGrpAddressPojos;
    }

    public ArrayList<LBAgentToContPojo> getLbAgentToContPojos() {
        return lbAgentToContPojos;
    }

    public void setLbAgentToContPojos(ArrayList<LBAgentToContPojo> lbAgentToContPojos) {
        this.lbAgentToContPojos = lbAgentToContPojos;
    }

    public ArrayList<LBAgentComInfoPojo> getLbAgentComInfoPojos() {
        return lbAgentComInfoPojos;
    }

    public void setLbAgentComInfoPojos(ArrayList<LBAgentComInfoPojo> lbAgentComInfoPojos) {
        this.lbAgentComInfoPojos = lbAgentComInfoPojos;
    }

    public ArrayList<LBPolicyInfoPojo> getLbPolicyInfoPojos() {
        return lbPolicyInfoPojos;
    }

    public void setLbPolicyInfoPojos(ArrayList<LBPolicyInfoPojo> lbPolicyInfoPojos) {
        this.lbPolicyInfoPojos = lbPolicyInfoPojos;
    }

    public ArrayList<LBContPlanPojo> getLbContPlanPojos() {
        return lbContPlanPojos;
    }

    public void setLbContPlanPojos(ArrayList<LBContPlanPojo> lbContPlanPojos) {
        this.lbContPlanPojos = lbContPlanPojos;
    }

    public ArrayList<LBDutySubPojo> getLbDutySubPojos() {
        return lbDutySubPojos;
    }

    public void setLbDutySubPojos(ArrayList<LBDutySubPojo> lbDutySubPojos) {
        this.lbDutySubPojos = lbDutySubPojos;
    }

    public ArrayList<LBContPlanDetailPojo> getLbContPlanDetailPojos() {
        return lbContPlanDetailPojos;
    }

    public void setLbContPlanDetailPojos(ArrayList<LBContPlanDetailPojo> lbContPlanDetailPojos) {
        this.lbContPlanDetailPojos = lbContPlanDetailPojos;
    }

    public ArrayList<LBContPlanDetailSubPojo> getLbContPlanDetailSubPojos() {
        return lbContPlanDetailSubPojos;
    }

    public void setLbContPlanDetailSubPojos(ArrayList<LBContPlanDetailSubPojo> lbContPlanDetailSubPojos) {
        this.lbContPlanDetailSubPojos = lbContPlanDetailSubPojos;
    }

    public ArrayList<LCGrpContPojo> getLcGrpContPojos() {
        return lcGrpContPojos;
    }

    public void setLcGrpContPojos(ArrayList<LCGrpContPojo> lcGrpContPojos) {
        this.lcGrpContPojos = lcGrpContPojos;
    }

    public ArrayList<LCGrpPolPojo> getLcGrpPolPojos() {
        return lcGrpPolPojos;
    }

    public void setLcGrpPolPojos(ArrayList<LCGrpPolPojo> lcGrpPolPojos) {
        this.lcGrpPolPojos = lcGrpPolPojos;
    }

    public ArrayList<LCGrpAppntPojo> getLcGrpAppntPojos() {
        return lcGrpAppntPojos;
    }

    public void setLcGrpAppntPojos(ArrayList<LCGrpAppntPojo> lcGrpAppntPojos) {
        this.lcGrpAppntPojos = lcGrpAppntPojos;
    }

    public ArrayList<LCGrpAddressPojo> getLcGrpAddressPojos() {
        return lcGrpAddressPojos;
    }

    public void setLcGrpAddressPojos(ArrayList<LCGrpAddressPojo> lcGrpAddressPojos) {
        this.lcGrpAddressPojos = lcGrpAddressPojos;
    }

    public ArrayList<LCAgentToContPojo> getLcAgentToContPojos() {
        return lcAgentToContPojos;
    }

    public void setLcAgentToContPojos(ArrayList<LCAgentToContPojo> lcAgentToContPojos) {
        this.lcAgentToContPojos = lcAgentToContPojos;
    }

    public ArrayList<LCAgentComInfoPojo> getLcAgentComInfoPojos() {
        return lcAgentComInfoPojos;
    }

    public void setLcAgentComInfoPojos(ArrayList<LCAgentComInfoPojo> lcAgentComInfoPojos) {
        this.lcAgentComInfoPojos = lcAgentComInfoPojos;
    }

    public ArrayList<LCPolicyInfoPojo> getLcPolicyInfoPojos() {
        return lcPolicyInfoPojos;
    }

    public void setLcPolicyInfoPojos(ArrayList<LCPolicyInfoPojo> lcPolicyInfoPojos) {
        this.lcPolicyInfoPojos = lcPolicyInfoPojos;
    }

    public ArrayList<LCContPlanPojo> getLcContPlanPojos() {
        return lcContPlanPojos;
    }

    public void setLcContPlanPojos(ArrayList<LCContPlanPojo> lcContPlanPojos) {
        this.lcContPlanPojos = lcContPlanPojos;
    }

    public ArrayList<LCDutySubPojo> getLcDutySubPojos() {
        return lcDutySubPojos;
    }

    public void setLcDutySubPojos(ArrayList<LCDutySubPojo> lcDutySubPojos) {
        this.lcDutySubPojos = lcDutySubPojos;
    }

    public ArrayList<LCContPlanDetailPojo> getLcContPlanDetailPojos() {
        return lcContPlanDetailPojos;
    }

    public void setLcContPlanDetailPojos(ArrayList<LCContPlanDetailPojo> lcContPlanDetailPojos) {
        this.lcContPlanDetailPojos = lcContPlanDetailPojos;
    }

    public ArrayList<LCContPlanDetailSubPojo> getLcContPlanDetailSubPojos() {
        return lcContPlanDetailSubPojos;
    }

    public void setLcContPlanDetailSubPojos(ArrayList<LCContPlanDetailSubPojo> lcContPlanDetailSubPojos) {
        this.lcContPlanDetailSubPojos = lcContPlanDetailSubPojos;
    }

    public CancelTheDealPojo() {
    }

    public ArrayList<LCGetToAccPojo> getLcGetToAccPojos() {
        return lcGetToAccPojos;
    }

    public void setLcGetToAccPojos(ArrayList<LCGetToAccPojo> lcGetToAccPojos) {
        this.lcGetToAccPojos = lcGetToAccPojos;
    }

    public ArrayList<LCPremToAccPojo> getLcPremToAccPojos() {
        return lcPremToAccPojos;
    }

    public void setLcPremToAccPojos(ArrayList<LCPremToAccPojo> lcPremToAccPojos) {
        this.lcPremToAccPojos = lcPremToAccPojos;
    }

    public ArrayList<LCAudVidRecordPojo> getLcAudVidRecordPojos() {
        return lcAudVidRecordPojos;
    }

    public void setLcAudVidRecordPojos(ArrayList<LCAudVidRecordPojo> lcAudVidRecordPojos) {
        this.lcAudVidRecordPojos = lcAudVidRecordPojos;
    }

    public ArrayList<LBAudVidRecordPojo> getLbAudVidRecordPojos() {
        return lbAudVidRecordPojos;
    }

    public void setLbAudVidRecordPojos(ArrayList<LBAudVidRecordPojo> lbAudVidRecordPojos) {
        this.lbAudVidRecordPojos = lbAudVidRecordPojos;
    }

    public ArrayList<LCAppntPojo> getLcAppntPojos() {
        return lcAppntPojos;
    }

    public void setLcAppntPojos(ArrayList<LCAppntPojo> lcAppntPojos) {
        this.lcAppntPojos = lcAppntPojos;
    }

    public ArrayList<LBAppntPojo> getLbAppntPojos() {
        return lbAppntPojos;
    }

    public void setLbAppntPojos(ArrayList<LBAppntPojo> lbAppntPojos) {
        this.lbAppntPojos = lbAppntPojos;
    }

    public ArrayList<LCBnfPojo> getLcBnfPojos() {
        return lcBnfPojos;
    }

    public void setLcBnfPojos(ArrayList<LCBnfPojo> lcBnfPojos) {
        this.lcBnfPojos = lcBnfPojos;
    }

    public ArrayList<LBBnfPojo> getLbBnfPojos() {
        return lbBnfPojos;
    }

    public void setLbBnfPojos(ArrayList<LBBnfPojo> lbBnfPojos) {
        this.lbBnfPojos = lbBnfPojos;
    }

    public ArrayList<LCInsuredPojo> getLcInsuredPojos() {
        return lcInsuredPojos;
    }

    public void setLcInsuredPojos(ArrayList<LCInsuredPojo> lcInsuredPojos) {
        this.lcInsuredPojos = lcInsuredPojos;
    }

    public ArrayList<LBInsuredPojo> getLbInsuredPojos() {
        return lbInsuredPojos;
    }

    public void setLbInsuredPojos(ArrayList<LBInsuredPojo> lbInsuredPojos) {
        this.lbInsuredPojos = lbInsuredPojos;
    }

    public ArrayList<LCGetPojo> getLcGetPojos() {
        return lcGetPojos;
    }

    public void setLcGetPojos(ArrayList<LCGetPojo> lcGetPojos) {
        this.lcGetPojos = lcGetPojos;
    }

    public ArrayList<LBGetPojo> getLbGetPojos() {
        return lbGetPojos;
    }

    public void setLbGetPojos(ArrayList<LBGetPojo> lbGetPojos) {
        this.lbGetPojos = lbGetPojos;
    }

    public ArrayList<LCPremPojo> getLcPremPojos() {
        return lcPremPojos;
    }

    public void setLcPremPojos(ArrayList<LCPremPojo> lcPremPojos) {
        this.lcPremPojos = lcPremPojos;
    }

    public ArrayList<LBPremPojo> getLbPremPojos() {
        return lbPremPojos;
    }

    public void setLbPremPojos(ArrayList<LBPremPojo> lbPremPojos) {
        this.lbPremPojos = lbPremPojos;
    }

    public ArrayList<LCDutyPojo> getLcDutyPojos() {
        return lcDutyPojos;
    }

    public void setLcDutyPojos(ArrayList<LCDutyPojo> lcDutyPojos) {
        this.lcDutyPojos = lcDutyPojos;
    }

    public ArrayList<LBDutyPojo> getLbDutyPojos() {
        return lbDutyPojos;
    }

    public void setLbDutyPojos(ArrayList<LBDutyPojo> lbDutyPojos) {
        this.lbDutyPojos = lbDutyPojos;
    }

    public ArrayList<LCCustomerImpartParamsPojo> getLcCustomerImpartParamsPojos() {
        return lcCustomerImpartParamsPojos;
    }

    public void setLcCustomerImpartParamsPojos(ArrayList<LCCustomerImpartParamsPojo> lcCustomerImpartParamsPojos) {
        this.lcCustomerImpartParamsPojos = lcCustomerImpartParamsPojos;
    }

    public ArrayList<LBCustomerImpartParamsPojo> getLbCustomerImpartParamsPojos() {
        return lbCustomerImpartParamsPojos;
    }

    public void setLbCustomerImpartParamsPojos(ArrayList<LBCustomerImpartParamsPojo> lbCustomerImpartParamsPojos) {
        this.lbCustomerImpartParamsPojos = lbCustomerImpartParamsPojos;
    }

    public ArrayList<LCCustomerImpartPojo> getLcCustomerImpartPojos() {
        return lcCustomerImpartPojos;
    }

    public void setLcCustomerImpartPojos(ArrayList<LCCustomerImpartPojo> lcCustomerImpartPojos) {
        this.lcCustomerImpartPojos = lcCustomerImpartPojos;
    }

    public ArrayList<LBCustomerImpartPojo> getLbCustomerImpartPojos() {
        return lbCustomerImpartPojos;
    }

    public void setLbCustomerImpartPojos(ArrayList<LBCustomerImpartPojo> lbCustomerImpartPojos) {
        this.lbCustomerImpartPojos = lbCustomerImpartPojos;
    }

    public ArrayList<LCInsureAccClassPojo> getLcInsureAccClassPojos() {
        return lcInsureAccClassPojos;
    }

    public void setLcInsureAccClassPojos(ArrayList<LCInsureAccClassPojo> lcInsureAccClassPojos) {
        this.lcInsureAccClassPojos = lcInsureAccClassPojos;
    }

    public ArrayList<LBInsureAccClassPojo> getLbInsureAccClassPojos() {
        return lbInsureAccClassPojos;
    }

    public void setLbInsureAccClassPojos(ArrayList<LBInsureAccClassPojo> lbInsureAccClassPojos) {
        this.lbInsureAccClassPojos = lbInsureAccClassPojos;
    }

    public ArrayList<LCInsureAccPojo> getLcInsureAccPojos() {
        return lcInsureAccPojos;
    }

    public void setLcInsureAccPojos(ArrayList<LCInsureAccPojo> lcInsureAccPojos) {
        this.lcInsureAccPojos = lcInsureAccPojos;
    }

    public ArrayList<LBInsureAccPojo> getLbInsureAccPojos() {
        return lbInsureAccPojos;
    }

    public void setLbInsureAccPojos(ArrayList<LBInsureAccPojo> lbInsureAccPojos) {
        this.lbInsureAccPojos = lbInsureAccPojos;
    }

    public ArrayList<LCInsureAccClassFeePojo> getLcInsureAccClassFeePojos() {
        return lcInsureAccClassFeePojos;
    }

    public void setLcInsureAccClassFeePojos(ArrayList<LCInsureAccClassFeePojo> lcInsureAccClassFeePojos) {
        this.lcInsureAccClassFeePojos = lcInsureAccClassFeePojos;
    }

    public ArrayList<LBInsureAccClassFeePojo> getLbInsureAccClassFeePojos() {
        return lbInsureAccClassFeePojos;
    }

    public void setLbInsureAccClassFeePojos(ArrayList<LBInsureAccClassFeePojo> lbInsureAccClassFeePojos) {
        this.lbInsureAccClassFeePojos = lbInsureAccClassFeePojos;
    }

    public ArrayList<LCInsureAccFeePojo> getLcInsureAccFeePojos() {
        return lcInsureAccFeePojos;
    }

    public void setLcInsureAccFeePojos(ArrayList<LCInsureAccFeePojo> lcInsureAccFeePojos) {
        this.lcInsureAccFeePojos = lcInsureAccFeePojos;
    }

    public ArrayList<LBInsureAccFeePojo> getLbInsureAccFeePojos() {
        return lbInsureAccFeePojos;
    }

    public void setLbInsureAccFeePojos(ArrayList<LBInsureAccFeePojo> lbInsureAccFeePojos) {
        this.lbInsureAccFeePojos = lbInsureAccFeePojos;
    }

    public ArrayList<LCInsureAccFeeTracePojo> getLcInsureAccFeeTracePojos() {
        return lcInsureAccFeeTracePojos;
    }

    public void setLcInsureAccFeeTracePojos(ArrayList<LCInsureAccFeeTracePojo> lcInsureAccFeeTracePojos) {
        this.lcInsureAccFeeTracePojos = lcInsureAccFeeTracePojos;
    }

    public ArrayList<LBInsureAccFeeTracePojo> getLbInsureAccFeeTracePojos() {
        return lbInsureAccFeeTracePojos;
    }

    public void setLbInsureAccFeeTracePojos(ArrayList<LBInsureAccFeeTracePojo> lbInsureAccFeeTracePojos) {
        this.lbInsureAccFeeTracePojos = lbInsureAccFeeTracePojos;
    }

    public ArrayList<LCInsureAccTracePojo> getLcInsureAccTracePojos() {
        return lcInsureAccTracePojos;
    }

    public void setLcInsureAccTracePojos(ArrayList<LCInsureAccTracePojo> lcInsureAccTracePojos) {
        this.lcInsureAccTracePojos = lcInsureAccTracePojos;
    }

    public ArrayList<LBInsureAccTracePojo> getLbInsureAccTracePojos() {
        return lbInsureAccTracePojos;
    }

    public void setLbInsureAccTracePojos(ArrayList<LBInsureAccTracePojo> lbInsureAccTracePojos) {
        this.lbInsureAccTracePojos = lbInsureAccTracePojos;
    }

    public ArrayList<LACommisionDetailPojo> getLaCommisionDetailPojos() {
        return laCommisionDetailPojos;
    }

    public void setLaCommisionDetailPojos(ArrayList<LACommisionDetailPojo> laCommisionDetailPojos) {
        this.laCommisionDetailPojos = laCommisionDetailPojos;
    }

    public ArrayList<LACommisionDetailBPojo> getLaCommisionDetailBPojos() {
        return laCommisionDetailBPojos;
    }

    public void setLaCommisionDetailBPojos(ArrayList<LACommisionDetailBPojo> laCommisionDetailBPojos) {
        this.laCommisionDetailBPojos = laCommisionDetailBPojos;
    }

    public ArrayList<LCPolPojo> getLcPolPojos() {
        return lcPolPojos;
    }

    public void setLcPolPojos(ArrayList<LCPolPojo> lcPolPojos) {
        this.lcPolPojos = lcPolPojos;
    }

    public ArrayList<LBPolPojo> getLbPolPojos() {
        return lbPolPojos;
    }

    public void setLbPolPojos(ArrayList<LBPolPojo> lbPolPojos) {
        this.lbPolPojos = lbPolPojos;
    }

    public ArrayList<LCContPojo> getLcContPojo() {
        return lcContPojo;
    }

    public void setLcContPojo(ArrayList<LCContPojo> lcContPojo) {
        this.lcContPojo = lcContPojo;
    }

    public ArrayList<LBContPojo> getLbContPojo() {
        return lbContPojo;
    }

    public void setLbContPojo(ArrayList<LBContPojo> lbContPojo) {
        this.lbContPojo = lbContPojo;
    }

    public ArrayList<LKTransStatusPojo> getLkTransStatusPojos() {
        return lkTransStatusPojos;
    }

    public void setLkTransStatusPojos(ArrayList<LKTransStatusPojo> lkTransStatusPojos) {
        this.lkTransStatusPojos = lkTransStatusPojos;
    }

    public ArrayList<VMS_OUTPUT_CUSTOMERPojo> getVms_output_customerPojos() {
        return vms_output_customerPojos;
    }

    public void setVms_output_customerPojos(ArrayList<VMS_OUTPUT_CUSTOMERPojo> vms_output_customerPojos) {
        this.vms_output_customerPojos = vms_output_customerPojos;
    }

    @Override
    public String toString() {
        return "CancelTheDealPojo{" +
                "lcAppntPojos=" + lcAppntPojos +
                ", lbAppntPojos=" + lbAppntPojos +
                ", lcBnfPojos=" + lcBnfPojos +
                ", lbBnfPojos=" + lbBnfPojos +
                ", lcInsuredPojos=" + lcInsuredPojos +
                ", lbInsuredPojos=" + lbInsuredPojos +
                ", lcGetPojos=" + lcGetPojos +
                ", lbGetPojos=" + lbGetPojos +
                ", lcPremPojos=" + lcPremPojos +
                ", lbPremPojos=" + lbPremPojos +
                ", lcDutyPojos=" + lcDutyPojos +
                ", lbDutyPojos=" + lbDutyPojos +
                ", lcCustomerImpartParamsPojos=" + lcCustomerImpartParamsPojos +
                ", lbCustomerImpartParamsPojos=" + lbCustomerImpartParamsPojos +
                ", lcCustomerImpartPojos=" + lcCustomerImpartPojos +
                ", lbCustomerImpartPojos=" + lbCustomerImpartPojos +
                ", lcInsureAccClassPojos=" + lcInsureAccClassPojos +
                ", lbInsureAccClassPojos=" + lbInsureAccClassPojos +
                ", lcInsureAccPojos=" + lcInsureAccPojos +
                ", lbInsureAccPojos=" + lbInsureAccPojos +
                ", lcInsureAccClassFeePojos=" + lcInsureAccClassFeePojos +
                ", lbInsureAccClassFeePojos=" + lbInsureAccClassFeePojos +
                ", lcInsureAccFeePojos=" + lcInsureAccFeePojos +
                ", lbInsureAccFeePojos=" + lbInsureAccFeePojos +
                ", lcInsureAccFeeTracePojos=" + lcInsureAccFeeTracePojos +
                ", lbInsureAccFeeTracePojos=" + lbInsureAccFeeTracePojos +
                ", lcInsureAccTracePojos=" + lcInsureAccTracePojos +
                ", lbInsureAccTracePojos=" + lbInsureAccTracePojos +
                ", laCommisionDetailPojos=" + laCommisionDetailPojos +
                ", laCommisionDetailBPojos=" + laCommisionDetailBPojos +
                ", lcPolPojos=" + lcPolPojos +
                ", lbPolPojos=" + lbPolPojos +
                ", lcContPojo=" + lcContPojo +
                ", lbContPojo=" + lbContPojo +
                ", lkTransStatusPojos=" + lkTransStatusPojos +
                ", lcAudVidRecordPojos=" + lcAudVidRecordPojos +
                ", lbAudVidRecordPojos=" + lbAudVidRecordPojos +
                ", vms_output_customerPojos=" + vms_output_customerPojos +
                ", lcGetToAccPojos=" + lcGetToAccPojos +
                ", lcPremToAccPojos=" + lcPremToAccPojos +
                ", lcGrpContPojos=" + lcGrpContPojos +
                ", lcGrpPolPojos=" + lcGrpPolPojos +
                ", lcGrpAppntPojos=" + lcGrpAppntPojos +
                ", lcGrpAddressPojos=" + lcGrpAddressPojos +
                ", lcAgentToContPojos=" + lcAgentToContPojos +
                ", lcAgentComInfoPojos=" + lcAgentComInfoPojos +
                ", lcPolicyInfoPojos=" + lcPolicyInfoPojos +
                ", lcContPlanPojos=" + lcContPlanPojos +
                ", lcDutySubPojos=" + lcDutySubPojos +
                ", lcContPlanDetailPojos=" + lcContPlanDetailPojos +
                ", lcContPlanDetailSubPojos=" + lcContPlanDetailSubPojos +
                ", lbGrpContPojos=" + lbGrpContPojos +
                ", lbGrpPolPojos=" + lbGrpPolPojos +
                ", lbGrpAppntPojos=" + lbGrpAppntPojos +
                ", lbGrpAddressPojos=" + lbGrpAddressPojos +
                ", lbAgentToContPojos=" + lbAgentToContPojos +
                ", lbAgentComInfoPojos=" + lbAgentComInfoPojos +
                ", lbPolicyInfoPojos=" + lbPolicyInfoPojos +
                ", lbContPlanPojos=" + lbContPlanPojos +
                ", lbDutySubPojos=" + lbDutySubPojos +
                ", lbContPlanDetailPojos=" + lbContPlanDetailPojos +
                ", lbContPlanDetailSubPojos=" + lbContPlanDetailSubPojos +
                ", lcaipContRegisterPojos=" + lcaipContRegisterPojos +
                ", lbaipContRegisterPojos=" + lbaipContRegisterPojos +
                '}';
    }
}
