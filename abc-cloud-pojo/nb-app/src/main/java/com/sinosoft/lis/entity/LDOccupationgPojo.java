/**
 * Copyright (c) 2018 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.entity;

import com.sinosoft.cloud.cache.annotation.RedisPrimaryHKey;
import com.sinosoft.utility.Pojo;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;

import java.io.Serializable;

/**
 * <p>ClassName: LDOccupationgPojo </p>
 * <p>Copyright: Copyright (c) 2018</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2018-08-09
 */
public class LDOccupationgPojo implements Pojo,Serializable {
    // @Field
    /** 工种代码 */
	@RedisPrimaryHKey
    private String OccupationCode; 
    /** 工种名称 */
    private String OccupationName; 
    /** 职业类别 */
    private String OccupationType; 
    /** 行业代码 */
    private String WorkType; 
    /** 行业名称 */
    private String WorkName; 
    /** 医疗险加费比例 */
    private int MedRate; 
    /** 意外风险 */
    private String SuddRisk; 
    /** 重疾风险 */
    private String DiseaRisk; 
    /** 住院风险 */
    private String HosipRisk; 
    /** 系统编码 */
	@RedisPrimaryHKey
    private String System; 


    public static final int FIELDNUM = 10;    // 数据库表的字段个数
    public String getOccupationCode() {
        return OccupationCode;
    }
    public void setOccupationCode(String aOccupationCode) {
        OccupationCode = aOccupationCode;
    }
    public String getOccupationName() {
        return OccupationName;
    }
    public void setOccupationName(String aOccupationName) {
        OccupationName = aOccupationName;
    }
    public String getOccupationType() {
        return OccupationType;
    }
    public void setOccupationType(String aOccupationType) {
        OccupationType = aOccupationType;
    }
    public String getWorkType() {
        return WorkType;
    }
    public void setWorkType(String aWorkType) {
        WorkType = aWorkType;
    }
    public String getWorkName() {
        return WorkName;
    }
    public void setWorkName(String aWorkName) {
        WorkName = aWorkName;
    }
    public int getMedRate() {
        return MedRate;
    }
    public void setMedRate(int aMedRate) {
        MedRate = aMedRate;
    }
    public void setMedRate(String aMedRate) {
        if (aMedRate != null && !aMedRate.equals("")) {
            Integer tInteger = new Integer(aMedRate);
            int i = tInteger.intValue();
            MedRate = i;
        }
    }

    public String getSuddRisk() {
        return SuddRisk;
    }
    public void setSuddRisk(String aSuddRisk) {
        SuddRisk = aSuddRisk;
    }
    public String getDiseaRisk() {
        return DiseaRisk;
    }
    public void setDiseaRisk(String aDiseaRisk) {
        DiseaRisk = aDiseaRisk;
    }
    public String getHosipRisk() {
        return HosipRisk;
    }
    public void setHosipRisk(String aHosipRisk) {
        HosipRisk = aHosipRisk;
    }
    public String getSystem() {
        return System;
    }
    public void setSystem(String aSystem) {
        System = aSystem;
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("OccupationCode") ) {
            return 0;
        }
        if( strFieldName.equals("OccupationName") ) {
            return 1;
        }
        if( strFieldName.equals("OccupationType") ) {
            return 2;
        }
        if( strFieldName.equals("WorkType") ) {
            return 3;
        }
        if( strFieldName.equals("WorkName") ) {
            return 4;
        }
        if( strFieldName.equals("MedRate") ) {
            return 5;
        }
        if( strFieldName.equals("SuddRisk") ) {
            return 6;
        }
        if( strFieldName.equals("DiseaRisk") ) {
            return 7;
        }
        if( strFieldName.equals("HosipRisk") ) {
            return 8;
        }
        if( strFieldName.equals("System") ) {
            return 9;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "OccupationCode";
                break;
            case 1:
                strFieldName = "OccupationName";
                break;
            case 2:
                strFieldName = "OccupationType";
                break;
            case 3:
                strFieldName = "WorkType";
                break;
            case 4:
                strFieldName = "WorkName";
                break;
            case 5:
                strFieldName = "MedRate";
                break;
            case 6:
                strFieldName = "SuddRisk";
                break;
            case 7:
                strFieldName = "DiseaRisk";
                break;
            case 8:
                strFieldName = "HosipRisk";
                break;
            case 9:
                strFieldName = "System";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "OCCUPATIONCODE":
                return Schema.TYPE_STRING;
            case "OCCUPATIONNAME":
                return Schema.TYPE_STRING;
            case "OCCUPATIONTYPE":
                return Schema.TYPE_STRING;
            case "WORKTYPE":
                return Schema.TYPE_STRING;
            case "WORKNAME":
                return Schema.TYPE_STRING;
            case "MEDRATE":
                return Schema.TYPE_INT;
            case "SUDDRISK":
                return Schema.TYPE_STRING;
            case "DISEARISK":
                return Schema.TYPE_STRING;
            case "HOSIPRISK":
                return Schema.TYPE_STRING;
            case "SYSTEM":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_STRING;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_INT;
            case 6:
                return Schema.TYPE_STRING;
            case 7:
                return Schema.TYPE_STRING;
            case 8:
                return Schema.TYPE_STRING;
            case 9:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("OccupationCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OccupationCode));
        }
        if (FCode.equalsIgnoreCase("OccupationName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OccupationName));
        }
        if (FCode.equalsIgnoreCase("OccupationType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OccupationType));
        }
        if (FCode.equalsIgnoreCase("WorkType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(WorkType));
        }
        if (FCode.equalsIgnoreCase("WorkName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(WorkName));
        }
        if (FCode.equalsIgnoreCase("MedRate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MedRate));
        }
        if (FCode.equalsIgnoreCase("SuddRisk")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SuddRisk));
        }
        if (FCode.equalsIgnoreCase("DiseaRisk")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DiseaRisk));
        }
        if (FCode.equalsIgnoreCase("HosipRisk")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(HosipRisk));
        }
        if (FCode.equalsIgnoreCase("System")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(System));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = String.valueOf(OccupationCode);
                break;
            case 1:
                strFieldValue = String.valueOf(OccupationName);
                break;
            case 2:
                strFieldValue = String.valueOf(OccupationType);
                break;
            case 3:
                strFieldValue = String.valueOf(WorkType);
                break;
            case 4:
                strFieldValue = String.valueOf(WorkName);
                break;
            case 5:
                strFieldValue = String.valueOf(MedRate);
                break;
            case 6:
                strFieldValue = String.valueOf(SuddRisk);
                break;
            case 7:
                strFieldValue = String.valueOf(DiseaRisk);
                break;
            case 8:
                strFieldValue = String.valueOf(HosipRisk);
                break;
            case 9:
                strFieldValue = String.valueOf(System);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("OccupationCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                OccupationCode = FValue.trim();
            }
            else
                OccupationCode = null;
        }
        if (FCode.equalsIgnoreCase("OccupationName")) {
            if( FValue != null && !FValue.equals(""))
            {
                OccupationName = FValue.trim();
            }
            else
                OccupationName = null;
        }
        if (FCode.equalsIgnoreCase("OccupationType")) {
            if( FValue != null && !FValue.equals(""))
            {
                OccupationType = FValue.trim();
            }
            else
                OccupationType = null;
        }
        if (FCode.equalsIgnoreCase("WorkType")) {
            if( FValue != null && !FValue.equals(""))
            {
                WorkType = FValue.trim();
            }
            else
                WorkType = null;
        }
        if (FCode.equalsIgnoreCase("WorkName")) {
            if( FValue != null && !FValue.equals(""))
            {
                WorkName = FValue.trim();
            }
            else
                WorkName = null;
        }
        if (FCode.equalsIgnoreCase("MedRate")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                MedRate = i;
            }
        }
        if (FCode.equalsIgnoreCase("SuddRisk")) {
            if( FValue != null && !FValue.equals(""))
            {
                SuddRisk = FValue.trim();
            }
            else
                SuddRisk = null;
        }
        if (FCode.equalsIgnoreCase("DiseaRisk")) {
            if( FValue != null && !FValue.equals(""))
            {
                DiseaRisk = FValue.trim();
            }
            else
                DiseaRisk = null;
        }
        if (FCode.equalsIgnoreCase("HosipRisk")) {
            if( FValue != null && !FValue.equals(""))
            {
                HosipRisk = FValue.trim();
            }
            else
                HosipRisk = null;
        }
        if (FCode.equalsIgnoreCase("System")) {
            if( FValue != null && !FValue.equals(""))
            {
                System = FValue.trim();
            }
            else
                System = null;
        }
        return true;
    }


    public String toString() {
    return "LDOccupationgPojo [" +
            "OccupationCode="+OccupationCode +
            ", OccupationName="+OccupationName +
            ", OccupationType="+OccupationType +
            ", WorkType="+WorkType +
            ", WorkName="+WorkName +
            ", MedRate="+MedRate +
            ", SuddRisk="+SuddRisk +
            ", DiseaRisk="+DiseaRisk +
            ", HosipRisk="+HosipRisk +
            ", System="+System +"]";
    }
}
