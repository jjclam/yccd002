/**
 * Copyright (c) 2017 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.vdb;

import com.sinosoft.lis.vschema.VMS_OUTPUT_CUSTOMERSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.DBConnPool;
import com.sinosoft.utility.DBOper;
import com.sinosoft.utility.ExceptionUtils;

import java.sql.Connection;
import java.sql.PreparedStatement;

/**
 * <p>ClassName: VMS_OUTPUT_CUSTOMERDBSet </p>
 * <p>Description: DB层多记录数据库操作类文件 </p>
 * <p>Copyright: Copyright (c) 2017</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2017-10-21
 */
public class VMS_OUTPUT_CUSTOMERDBSet extends VMS_OUTPUT_CUSTOMERSet {
    // @Field
    private Connection con;
    private DBOper db;
    /**
    * flag = true: 传入Connection
    * flag = false: 不传入Connection
    **/
    private boolean mflag = false;

    // @Constructor
    public VMS_OUTPUT_CUSTOMERDBSet(Connection tConnection) {
        con = tConnection;
        db = new DBOper(con,"VMS_OUTPUT_CUSTOMER");
        mflag = true;
    }

    public VMS_OUTPUT_CUSTOMERDBSet() {
        db = new DBOper( "VMS_OUTPUT_CUSTOMER" );
    }
    // @Method
    public boolean deleteSQL() {
        if (db.deleteSQL(this)) {
            return true;
        } else {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "VMS_OUTPUT_CUSTOMERDBSet";
            tError.functionName = "deleteSQL";
            tError.errorMessage = "操作失败!";
            this.mErrors .addOneError(tError);
            return false;
        }
    }

    /**
     * 删除操作
     * 删除条件：主键
     * @return boolean
     */
    public boolean delete() {
        PreparedStatement pstmt = null;

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try {
            int tCount = this.size();
            pstmt = con.prepareStatement("DELETE FROM VMS_OUTPUT_CUSTOMER WHERE  1=1  AND VIC_INDATE = ? AND VIC_CUSTOMER_ID = ?");
            for (int i = 1; i <= tCount; i++) {
            if(this.get(i).getVIC_INDATE() == null || this.get(i).getVIC_INDATE().equals("null")) {
            	pstmt.setString(1,null);
            } else {
            	pstmt.setString(1, this.get(i).getVIC_INDATE());
            }
            if(this.get(i).getVIC_CUSTOMER_ID() == null || this.get(i).getVIC_CUSTOMER_ID().equals("null")) {
            	pstmt.setString(2,null);
            } else {
            	pstmt.setString(2, this.get(i).getVIC_CUSTOMER_ID());
            }
                pstmt.addBatch();
            }
            pstmt.executeBatch();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            ex.printStackTrace();
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "VMS_OUTPUT_CUSTOMERDBSet";
            tError.functionName = "delete()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            try {
                pstmt.close();
            } catch (Exception e){
                e.printStackTrace();
            }

            if (!mflag) {
                try {
                    con.close();
                } catch (Exception e){e.printStackTrace();}
            }

            return false;
        }

        if( !mflag ) {
            try {
                con.close();
            } catch (Exception e){e.printStackTrace();}
        }

        return true;
    }

    /**
     * 更新操作
     * 更新条件：主键
     * @return boolean
     */
    public boolean update() {
        PreparedStatement pstmt = null;

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try {
            int tCount = this.size();
            pstmt = con.prepareStatement("UPDATE VMS_OUTPUT_CUSTOMER SET  VIC_INDATE = ? , VIC_CUSTOMER_ID = ? , VIC_CUSTOMER_CNAME = ? , VIC_CUSTOMER_TAXNO = ? , VIC_CUSTOMER_ACCOUNT = ? , VIC_CUSTOMER_CBANK = ? , VIC_CUSTOMER_PHONE = ? , VIC_CUSTOMER_EMAIL = ? , VIC_CUSTOMER_ADDRESS = ? , VIC_TAXPAYER_TYPE = ? , VIC_FAPIAO_TYPE = ? , VIC_CUSTOMER_TYPE = ? , VIC_CUSTOMER_FAPIAO_FLAG = ? , VIC_CUSTOMER_NATIONALITY = ? , VIC_DATA_SOURCE = ? , VIC_LINK_NAME = ? , VIC_LINK_PHONE = ? , VIC_LINK_ADDRESS = ? , VIC_CUSTOMER_ZIP_CODE = ? , VIC_CUSTOMER_UPLOAD_FLAG = ? WHERE  1=1  AND VIC_INDATE = ? AND VIC_CUSTOMER_ID = ?");
            for (int i = 1; i <= tCount; i++) {
            if(this.get(i).getVIC_INDATE() == null || this.get(i).getVIC_INDATE().equals("null")) {
            	pstmt.setString(1,null);
            } else {
            	pstmt.setString(1, this.get(i).getVIC_INDATE());
            }
            if(this.get(i).getVIC_CUSTOMER_ID() == null || this.get(i).getVIC_CUSTOMER_ID().equals("null")) {
            	pstmt.setString(2,null);
            } else {
            	pstmt.setString(2, this.get(i).getVIC_CUSTOMER_ID());
            }
            if(this.get(i).getVIC_CUSTOMER_CNAME() == null || this.get(i).getVIC_CUSTOMER_CNAME().equals("null")) {
            	pstmt.setString(3,null);
            } else {
            	pstmt.setString(3, this.get(i).getVIC_CUSTOMER_CNAME());
            }
            if(this.get(i).getVIC_CUSTOMER_TAXNO() == null || this.get(i).getVIC_CUSTOMER_TAXNO().equals("null")) {
            	pstmt.setString(4,null);
            } else {
            	pstmt.setString(4, this.get(i).getVIC_CUSTOMER_TAXNO());
            }
            if(this.get(i).getVIC_CUSTOMER_ACCOUNT() == null || this.get(i).getVIC_CUSTOMER_ACCOUNT().equals("null")) {
            	pstmt.setString(5,null);
            } else {
            	pstmt.setString(5, this.get(i).getVIC_CUSTOMER_ACCOUNT());
            }
            if(this.get(i).getVIC_CUSTOMER_CBANK() == null || this.get(i).getVIC_CUSTOMER_CBANK().equals("null")) {
            	pstmt.setString(6,null);
            } else {
            	pstmt.setString(6, this.get(i).getVIC_CUSTOMER_CBANK());
            }
            if(this.get(i).getVIC_CUSTOMER_PHONE() == null || this.get(i).getVIC_CUSTOMER_PHONE().equals("null")) {
            	pstmt.setString(7,null);
            } else {
            	pstmt.setString(7, this.get(i).getVIC_CUSTOMER_PHONE());
            }
            if(this.get(i).getVIC_CUSTOMER_EMAIL() == null || this.get(i).getVIC_CUSTOMER_EMAIL().equals("null")) {
            	pstmt.setString(8,null);
            } else {
            	pstmt.setString(8, this.get(i).getVIC_CUSTOMER_EMAIL());
            }
            if(this.get(i).getVIC_CUSTOMER_ADDRESS() == null || this.get(i).getVIC_CUSTOMER_ADDRESS().equals("null")) {
            	pstmt.setString(9,null);
            } else {
            	pstmt.setString(9, this.get(i).getVIC_CUSTOMER_ADDRESS());
            }
            if(this.get(i).getVIC_TAXPAYER_TYPE() == null || this.get(i).getVIC_TAXPAYER_TYPE().equals("null")) {
            	pstmt.setString(10,null);
            } else {
            	pstmt.setString(10, this.get(i).getVIC_TAXPAYER_TYPE());
            }
            if(this.get(i).getVIC_FAPIAO_TYPE() == null || this.get(i).getVIC_FAPIAO_TYPE().equals("null")) {
            	pstmt.setString(11,null);
            } else {
            	pstmt.setString(11, this.get(i).getVIC_FAPIAO_TYPE());
            }
            if(this.get(i).getVIC_CUSTOMER_TYPE() == null || this.get(i).getVIC_CUSTOMER_TYPE().equals("null")) {
            	pstmt.setString(12,null);
            } else {
            	pstmt.setString(12, this.get(i).getVIC_CUSTOMER_TYPE());
            }
            if(this.get(i).getVIC_CUSTOMER_FAPIAO_FLAG() == null || this.get(i).getVIC_CUSTOMER_FAPIAO_FLAG().equals("null")) {
            	pstmt.setString(13,null);
            } else {
            	pstmt.setString(13, this.get(i).getVIC_CUSTOMER_FAPIAO_FLAG());
            }
            if(this.get(i).getVIC_CUSTOMER_NATIONALITY() == null || this.get(i).getVIC_CUSTOMER_NATIONALITY().equals("null")) {
            	pstmt.setString(14,null);
            } else {
            	pstmt.setString(14, this.get(i).getVIC_CUSTOMER_NATIONALITY());
            }
            if(this.get(i).getVIC_DATA_SOURCE() == null || this.get(i).getVIC_DATA_SOURCE().equals("null")) {
            	pstmt.setString(15,null);
            } else {
            	pstmt.setString(15, this.get(i).getVIC_DATA_SOURCE());
            }
            if(this.get(i).getVIC_LINK_NAME() == null || this.get(i).getVIC_LINK_NAME().equals("null")) {
            	pstmt.setString(16,null);
            } else {
            	pstmt.setString(16, this.get(i).getVIC_LINK_NAME());
            }
            if(this.get(i).getVIC_LINK_PHONE() == null || this.get(i).getVIC_LINK_PHONE().equals("null")) {
            	pstmt.setString(17,null);
            } else {
            	pstmt.setString(17, this.get(i).getVIC_LINK_PHONE());
            }
            if(this.get(i).getVIC_LINK_ADDRESS() == null || this.get(i).getVIC_LINK_ADDRESS().equals("null")) {
            	pstmt.setString(18,null);
            } else {
            	pstmt.setString(18, this.get(i).getVIC_LINK_ADDRESS());
            }
            if(this.get(i).getVIC_CUSTOMER_ZIP_CODE() == null || this.get(i).getVIC_CUSTOMER_ZIP_CODE().equals("null")) {
            	pstmt.setString(19,null);
            } else {
            	pstmt.setString(19, this.get(i).getVIC_CUSTOMER_ZIP_CODE());
            }
            if(this.get(i).getVIC_CUSTOMER_UPLOAD_FLAG() == null || this.get(i).getVIC_CUSTOMER_UPLOAD_FLAG().equals("null")) {
            	pstmt.setString(20,null);
            } else {
            	pstmt.setString(20, this.get(i).getVIC_CUSTOMER_UPLOAD_FLAG());
            }
            // set where condition
            if(this.get(i).getVIC_INDATE() == null || this.get(i).getVIC_INDATE().equals("null")) {
            	pstmt.setString(21,null);
            } else {
            	pstmt.setString(21, this.get(i).getVIC_INDATE());
            }
            if(this.get(i).getVIC_CUSTOMER_ID() == null || this.get(i).getVIC_CUSTOMER_ID().equals("null")) {
            	pstmt.setString(22,null);
            } else {
            	pstmt.setString(22, this.get(i).getVIC_CUSTOMER_ID());
            }
                pstmt.addBatch();
            }
            pstmt.executeBatch();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            ex.printStackTrace();
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "VMS_OUTPUT_CUSTOMERDBSet";
            tError.functionName = "update()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            try {
                pstmt.close();
            } catch (Exception e){e.printStackTrace();}

            if( !mflag ) {
                try {
                    con.close();
                } catch (Exception e){e.printStackTrace();}
            }

            return false;
        }

        if( !mflag ) {
            try {
                con.close();
            } catch (Exception e){e.printStackTrace();}
        }

        return true;
    }

    /**
     * 新增操作
     * @return boolean
     */
    public boolean insert() {
        PreparedStatement pstmt = null;

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try
        {
            int tCount = this.size();
            pstmt = con.prepareStatement("INSERT INTO VMS_OUTPUT_CUSTOMER VALUES( ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ?)");
            for (int i = 1; i <= tCount; i++) {
            if(this.get(i).getVIC_INDATE() == null || this.get(i).getVIC_INDATE().equals("null")) {
            	pstmt.setString(1,null);
            } else {
            	pstmt.setString(1, this.get(i).getVIC_INDATE());
            }
            if(this.get(i).getVIC_CUSTOMER_ID() == null || this.get(i).getVIC_CUSTOMER_ID().equals("null")) {
            	pstmt.setString(2,null);
            } else {
            	pstmt.setString(2, this.get(i).getVIC_CUSTOMER_ID());
            }
            if(this.get(i).getVIC_CUSTOMER_CNAME() == null || this.get(i).getVIC_CUSTOMER_CNAME().equals("null")) {
            	pstmt.setString(3,null);
            } else {
            	pstmt.setString(3, this.get(i).getVIC_CUSTOMER_CNAME());
            }
            if(this.get(i).getVIC_CUSTOMER_TAXNO() == null || this.get(i).getVIC_CUSTOMER_TAXNO().equals("null")) {
            	pstmt.setString(4,null);
            } else {
            	pstmt.setString(4, this.get(i).getVIC_CUSTOMER_TAXNO());
            }
            if(this.get(i).getVIC_CUSTOMER_ACCOUNT() == null || this.get(i).getVIC_CUSTOMER_ACCOUNT().equals("null")) {
            	pstmt.setString(5,null);
            } else {
            	pstmt.setString(5, this.get(i).getVIC_CUSTOMER_ACCOUNT());
            }
            if(this.get(i).getVIC_CUSTOMER_CBANK() == null || this.get(i).getVIC_CUSTOMER_CBANK().equals("null")) {
            	pstmt.setString(6,null);
            } else {
            	pstmt.setString(6, this.get(i).getVIC_CUSTOMER_CBANK());
            }
            if(this.get(i).getVIC_CUSTOMER_PHONE() == null || this.get(i).getVIC_CUSTOMER_PHONE().equals("null")) {
            	pstmt.setString(7,null);
            } else {
            	pstmt.setString(7, this.get(i).getVIC_CUSTOMER_PHONE());
            }
            if(this.get(i).getVIC_CUSTOMER_EMAIL() == null || this.get(i).getVIC_CUSTOMER_EMAIL().equals("null")) {
            	pstmt.setString(8,null);
            } else {
            	pstmt.setString(8, this.get(i).getVIC_CUSTOMER_EMAIL());
            }
            if(this.get(i).getVIC_CUSTOMER_ADDRESS() == null || this.get(i).getVIC_CUSTOMER_ADDRESS().equals("null")) {
            	pstmt.setString(9,null);
            } else {
            	pstmt.setString(9, this.get(i).getVIC_CUSTOMER_ADDRESS());
            }
            if(this.get(i).getVIC_TAXPAYER_TYPE() == null || this.get(i).getVIC_TAXPAYER_TYPE().equals("null")) {
            	pstmt.setString(10,null);
            } else {
            	pstmt.setString(10, this.get(i).getVIC_TAXPAYER_TYPE());
            }
            if(this.get(i).getVIC_FAPIAO_TYPE() == null || this.get(i).getVIC_FAPIAO_TYPE().equals("null")) {
            	pstmt.setString(11,null);
            } else {
            	pstmt.setString(11, this.get(i).getVIC_FAPIAO_TYPE());
            }
            if(this.get(i).getVIC_CUSTOMER_TYPE() == null || this.get(i).getVIC_CUSTOMER_TYPE().equals("null")) {
            	pstmt.setString(12,null);
            } else {
            	pstmt.setString(12, this.get(i).getVIC_CUSTOMER_TYPE());
            }
            if(this.get(i).getVIC_CUSTOMER_FAPIAO_FLAG() == null || this.get(i).getVIC_CUSTOMER_FAPIAO_FLAG().equals("null")) {
            	pstmt.setString(13,null);
            } else {
            	pstmt.setString(13, this.get(i).getVIC_CUSTOMER_FAPIAO_FLAG());
            }
            if(this.get(i).getVIC_CUSTOMER_NATIONALITY() == null || this.get(i).getVIC_CUSTOMER_NATIONALITY().equals("null")) {
            	pstmt.setString(14,null);
            } else {
            	pstmt.setString(14, this.get(i).getVIC_CUSTOMER_NATIONALITY());
            }
            if(this.get(i).getVIC_DATA_SOURCE() == null || this.get(i).getVIC_DATA_SOURCE().equals("null")) {
            	pstmt.setString(15,null);
            } else {
            	pstmt.setString(15, this.get(i).getVIC_DATA_SOURCE());
            }
            if(this.get(i).getVIC_LINK_NAME() == null || this.get(i).getVIC_LINK_NAME().equals("null")) {
            	pstmt.setString(16,null);
            } else {
            	pstmt.setString(16, this.get(i).getVIC_LINK_NAME());
            }
            if(this.get(i).getVIC_LINK_PHONE() == null || this.get(i).getVIC_LINK_PHONE().equals("null")) {
            	pstmt.setString(17,null);
            } else {
            	pstmt.setString(17, this.get(i).getVIC_LINK_PHONE());
            }
            if(this.get(i).getVIC_LINK_ADDRESS() == null || this.get(i).getVIC_LINK_ADDRESS().equals("null")) {
            	pstmt.setString(18,null);
            } else {
            	pstmt.setString(18, this.get(i).getVIC_LINK_ADDRESS());
            }
            if(this.get(i).getVIC_CUSTOMER_ZIP_CODE() == null || this.get(i).getVIC_CUSTOMER_ZIP_CODE().equals("null")) {
            	pstmt.setString(19,null);
            } else {
            	pstmt.setString(19, this.get(i).getVIC_CUSTOMER_ZIP_CODE());
            }
            if(this.get(i).getVIC_CUSTOMER_UPLOAD_FLAG() == null || this.get(i).getVIC_CUSTOMER_UPLOAD_FLAG().equals("null")) {
            	pstmt.setString(20,null);
            } else {
            	pstmt.setString(20, this.get(i).getVIC_CUSTOMER_UPLOAD_FLAG());
            }
                pstmt.addBatch();
            }
            pstmt.executeBatch();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            ex.printStackTrace();
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "VMS_OUTPUT_CUSTOMERDBSet";
            tError.functionName = "insert()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            try {
                pstmt.close();
            } catch (Exception e){e.printStackTrace();}

            if( !mflag ) {
                try {
                    con.close();
                } catch (Exception e){e.printStackTrace();}
            }

            return false;
        }

        if( !mflag ) {
            try {
                con.close();
            } catch (Exception e){e.printStackTrace();}
        }

        return true;
    }
}
