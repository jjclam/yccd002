/**
 * Copyright (c) 2019 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.vdb;

import java.sql.*;
import com.sinosoft.lis.schema.LCRiskManagementInfoSchema;
import com.sinosoft.lis.vschema.LCRiskManagementInfoSet;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;

/**
 * <p>ClassName: LCRiskManagementInfoDBSet </p>
 * <p>Description: DB层多记录数据库操作类文件 </p>
 * <p>Copyright: Copyright (c) 2019</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2019-11-14
 */
public class LCRiskManagementInfoDBSet extends LCRiskManagementInfoSet {
    // @Field
    private Connection con;
    private DBOper db;
    /**
    * flag = true: 传入Connection
    * flag = false: 不传入Connection
    **/
    private boolean mflag = false;

    // @Constructor
    public LCRiskManagementInfoDBSet(Connection tConnection) {
        con = tConnection;
        db = new DBOper(con,"LCRiskManagementInfo");
        mflag = true;
    }

    public LCRiskManagementInfoDBSet() {
        db = new DBOper( "LCRiskManagementInfo" );
    }
    // @Method
    public boolean deleteSQL() {
        if (db.deleteSQL(this)) {
            return true;
        } else {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LCRiskManagementInfoDBSet";
            tError.functionName = "deleteSQL";
            tError.errorMessage = "操作失败!";
            this.mErrors .addOneError(tError);
            return false;
        }
    }

    /**
     * 删除操作
     * 删除条件：主键
     * @return boolean
     */
    public boolean delete() {
        PreparedStatement pstmt = null;

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try {
            int tCount = this.size();
            pstmt = con.prepareStatement("DELETE FROM LCRiskManagementInfo WHERE  1=1  AND SerialNo = ?");
            for (int i = 1; i <= tCount; i++) {
            if(this.get(i).getSerialNo() == null || this.get(i).getSerialNo().equals("null")) {
            	pstmt.setString(1,null);
            } else {
            	pstmt.setString(1, this.get(i).getSerialNo());
            }
                pstmt.addBatch();
            }
            pstmt.executeBatch();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            ex.printStackTrace();
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LCRiskManagementInfoDBSet";
            tError.functionName = "delete()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            try {
                pstmt.close();
            } catch (Exception e){
                e.printStackTrace();
            }

            if (!mflag) {
                try {
                    con.close();
                } catch (Exception e){e.printStackTrace();}
            }

            return false;
        }

        if( !mflag ) {
            try {
                con.close();
            } catch (Exception e){e.printStackTrace();}
        }

        return true;
    }

    /**
     * 更新操作
     * 更新条件：主键
     * @return boolean
     */
    public boolean update() {
        PreparedStatement pstmt = null;

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try {
            int tCount = this.size();
            pstmt = con.prepareStatement("UPDATE LCRiskManagementInfo SET  SerialNo = ? , AccNo = ? , ContNo = ? , PrtNo = ? , CaseNo = ? , RiskCode = ? , InsuredName = ? , InsuredIDType = ? , InsuredIDNo = ? , CompanyCode = ? , Business = ? , RiskType = ? , CustomerAllowed = ? , MultiCompany = ? , MajorDiseasePayment = ? , Disability = ? , Dense = ? , AccumulativeMoney = ? , PageQueryCode = ? , TagDate = ? , DisplayPage = ? , AbnormalCheck = ? , AbnormalPayment = ? , ChronicDiseasePayment = ? , PayMoney = ? , DayMoney = ? , CountOneYear = ? , PaymentCountOneYear = ? , Paymented = ? , PaymentCount = ? , DayMoneySum = ? , MajorDiseaseMoney = ? , PayCount = ? , PaymentDayCount = ? , Paymented2 = ? , ReceiptCode = ? , HospitalCode = ? , Operator = ? , MakeDate = ? , MakeTime = ? , ModifyDate = ? , ModifyTime = ? , InsurerUuid = ? WHERE  1=1  AND SerialNo = ?");
            for (int i = 1; i <= tCount; i++) {
            if(this.get(i).getSerialNo() == null || this.get(i).getSerialNo().equals("null")) {
            	pstmt.setString(1,null);
            } else {
            	pstmt.setString(1, this.get(i).getSerialNo());
            }
            if(this.get(i).getAccNo() == null || this.get(i).getAccNo().equals("null")) {
            	pstmt.setString(2,null);
            } else {
            	pstmt.setString(2, this.get(i).getAccNo());
            }
            if(this.get(i).getContNo() == null || this.get(i).getContNo().equals("null")) {
            	pstmt.setString(3,null);
            } else {
            	pstmt.setString(3, this.get(i).getContNo());
            }
            if(this.get(i).getPrtNo() == null || this.get(i).getPrtNo().equals("null")) {
            	pstmt.setString(4,null);
            } else {
            	pstmt.setString(4, this.get(i).getPrtNo());
            }
            if(this.get(i).getCaseNo() == null || this.get(i).getCaseNo().equals("null")) {
            	pstmt.setString(5,null);
            } else {
            	pstmt.setString(5, this.get(i).getCaseNo());
            }
            if(this.get(i).getRiskCode() == null || this.get(i).getRiskCode().equals("null")) {
            	pstmt.setString(6,null);
            } else {
            	pstmt.setString(6, this.get(i).getRiskCode());
            }
            if(this.get(i).getInsuredName() == null || this.get(i).getInsuredName().equals("null")) {
            	pstmt.setString(7,null);
            } else {
            	pstmt.setString(7, this.get(i).getInsuredName());
            }
            if(this.get(i).getInsuredIDType() == null || this.get(i).getInsuredIDType().equals("null")) {
            	pstmt.setString(8,null);
            } else {
            	pstmt.setString(8, this.get(i).getInsuredIDType());
            }
            if(this.get(i).getInsuredIDNo() == null || this.get(i).getInsuredIDNo().equals("null")) {
            	pstmt.setString(9,null);
            } else {
            	pstmt.setString(9, this.get(i).getInsuredIDNo());
            }
            if(this.get(i).getCompanyCode() == null || this.get(i).getCompanyCode().equals("null")) {
            	pstmt.setString(10,null);
            } else {
            	pstmt.setString(10, this.get(i).getCompanyCode());
            }
            if(this.get(i).getBusiness() == null || this.get(i).getBusiness().equals("null")) {
            	pstmt.setString(11,null);
            } else {
            	pstmt.setString(11, this.get(i).getBusiness());
            }
            if(this.get(i).getRiskType() == null || this.get(i).getRiskType().equals("null")) {
            	pstmt.setString(12,null);
            } else {
            	pstmt.setString(12, this.get(i).getRiskType());
            }
            if(this.get(i).getCustomerAllowed() == null || this.get(i).getCustomerAllowed().equals("null")) {
            	pstmt.setString(13,null);
            } else {
            	pstmt.setString(13, this.get(i).getCustomerAllowed());
            }
            if(this.get(i).getMultiCompany() == null || this.get(i).getMultiCompany().equals("null")) {
            	pstmt.setString(14,null);
            } else {
            	pstmt.setString(14, this.get(i).getMultiCompany());
            }
            if(this.get(i).getMajorDiseasePayment() == null || this.get(i).getMajorDiseasePayment().equals("null")) {
            	pstmt.setString(15,null);
            } else {
            	pstmt.setString(15, this.get(i).getMajorDiseasePayment());
            }
            if(this.get(i).getDisability() == null || this.get(i).getDisability().equals("null")) {
            	pstmt.setString(16,null);
            } else {
            	pstmt.setString(16, this.get(i).getDisability());
            }
            if(this.get(i).getDense() == null || this.get(i).getDense().equals("null")) {
            	pstmt.setString(17,null);
            } else {
            	pstmt.setString(17, this.get(i).getDense());
            }
            if(this.get(i).getAccumulativeMoney() == null || this.get(i).getAccumulativeMoney().equals("null")) {
            	pstmt.setString(18,null);
            } else {
            	pstmt.setString(18, this.get(i).getAccumulativeMoney());
            }
            if(this.get(i).getPageQueryCode() == null || this.get(i).getPageQueryCode().equals("null")) {
            	pstmt.setString(19,null);
            } else {
            	pstmt.setString(19, this.get(i).getPageQueryCode());
            }
            if(this.get(i).getTagDate() == null || this.get(i).getTagDate().equals("null")) {
            	pstmt.setString(20,null);
            } else {
            	pstmt.setString(20, this.get(i).getTagDate());
            }
            if(this.get(i).getDisplayPage() == null || this.get(i).getDisplayPage().equals("null")) {
            	pstmt.setString(21,null);
            } else {
            	pstmt.setString(21, this.get(i).getDisplayPage());
            }
            if(this.get(i).getAbnormalCheck() == null || this.get(i).getAbnormalCheck().equals("null")) {
            	pstmt.setString(22,null);
            } else {
            	pstmt.setString(22, this.get(i).getAbnormalCheck());
            }
            if(this.get(i).getAbnormalPayment() == null || this.get(i).getAbnormalPayment().equals("null")) {
            	pstmt.setString(23,null);
            } else {
            	pstmt.setString(23, this.get(i).getAbnormalPayment());
            }
            if(this.get(i).getChronicDiseasePayment() == null || this.get(i).getChronicDiseasePayment().equals("null")) {
            	pstmt.setString(24,null);
            } else {
            	pstmt.setString(24, this.get(i).getChronicDiseasePayment());
            }
            if(this.get(i).getPayMoney() == null || this.get(i).getPayMoney().equals("null")) {
            	pstmt.setString(25,null);
            } else {
            	pstmt.setString(25, this.get(i).getPayMoney());
            }
            if(this.get(i).getDayMoney() == null || this.get(i).getDayMoney().equals("null")) {
            	pstmt.setString(26,null);
            } else {
            	pstmt.setString(26, this.get(i).getDayMoney());
            }
            if(this.get(i).getCountOneYear() == null || this.get(i).getCountOneYear().equals("null")) {
            	pstmt.setString(27,null);
            } else {
            	pstmt.setString(27, this.get(i).getCountOneYear());
            }
            if(this.get(i).getPaymentCountOneYear() == null || this.get(i).getPaymentCountOneYear().equals("null")) {
            	pstmt.setString(28,null);
            } else {
            	pstmt.setString(28, this.get(i).getPaymentCountOneYear());
            }
            if(this.get(i).getPaymented() == null || this.get(i).getPaymented().equals("null")) {
            	pstmt.setString(29,null);
            } else {
            	pstmt.setString(29, this.get(i).getPaymented());
            }
            if(this.get(i).getPaymentCount() == null || this.get(i).getPaymentCount().equals("null")) {
            	pstmt.setString(30,null);
            } else {
            	pstmt.setString(30, this.get(i).getPaymentCount());
            }
            if(this.get(i).getDayMoneySum() == null || this.get(i).getDayMoneySum().equals("null")) {
            	pstmt.setString(31,null);
            } else {
            	pstmt.setString(31, this.get(i).getDayMoneySum());
            }
            if(this.get(i).getMajorDiseaseMoney() == null || this.get(i).getMajorDiseaseMoney().equals("null")) {
            	pstmt.setString(32,null);
            } else {
            	pstmt.setString(32, this.get(i).getMajorDiseaseMoney());
            }
            if(this.get(i).getPayCount() == null || this.get(i).getPayCount().equals("null")) {
            	pstmt.setString(33,null);
            } else {
            	pstmt.setString(33, this.get(i).getPayCount());
            }
            if(this.get(i).getPaymentDayCount() == null || this.get(i).getPaymentDayCount().equals("null")) {
            	pstmt.setString(34,null);
            } else {
            	pstmt.setString(34, this.get(i).getPaymentDayCount());
            }
            if(this.get(i).getPaymented2() == null || this.get(i).getPaymented2().equals("null")) {
            	pstmt.setString(35,null);
            } else {
            	pstmt.setString(35, this.get(i).getPaymented2());
            }
            if(this.get(i).getReceiptCode() == null || this.get(i).getReceiptCode().equals("null")) {
            	pstmt.setString(36,null);
            } else {
            	pstmt.setString(36, this.get(i).getReceiptCode());
            }
            if(this.get(i).getHospitalCode() == null || this.get(i).getHospitalCode().equals("null")) {
            	pstmt.setString(37,null);
            } else {
            	pstmt.setString(37, this.get(i).getHospitalCode());
            }
            if(this.get(i).getOperator() == null || this.get(i).getOperator().equals("null")) {
            	pstmt.setString(38,null);
            } else {
            	pstmt.setString(38, this.get(i).getOperator());
            }
            if(this.get(i).getMakeDate() == null || this.get(i).getMakeDate().equals("null")) {
                pstmt.setDate(39,null);
            } else {
                pstmt.setDate(39, Date.valueOf(this.get(i).getMakeDate()));
            }
            if(this.get(i).getMakeTime() == null || this.get(i).getMakeTime().equals("null")) {
            	pstmt.setString(40,null);
            } else {
            	pstmt.setString(40, this.get(i).getMakeTime());
            }
            if(this.get(i).getModifyDate() == null || this.get(i).getModifyDate().equals("null")) {
                pstmt.setDate(41,null);
            } else {
                pstmt.setDate(41, Date.valueOf(this.get(i).getModifyDate()));
            }
            if(this.get(i).getModifyTime() == null || this.get(i).getModifyTime().equals("null")) {
            	pstmt.setString(42,null);
            } else {
            	pstmt.setString(42, this.get(i).getModifyTime());
            }
            if(this.get(i).getInsurerUuid() == null || this.get(i).getInsurerUuid().equals("null")) {
            	pstmt.setString(43,null);
            } else {
            	pstmt.setString(43, this.get(i).getInsurerUuid());
            }
            // set where condition
            if(this.get(i).getSerialNo() == null || this.get(i).getSerialNo().equals("null")) {
            	pstmt.setString(44,null);
            } else {
            	pstmt.setString(44, this.get(i).getSerialNo());
            }
                pstmt.addBatch();
            }
            pstmt.executeBatch();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            ex.printStackTrace();
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LCRiskManagementInfoDBSet";
            tError.functionName = "update()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            try {
                pstmt.close();
            } catch (Exception e){e.printStackTrace();}

            if( !mflag ) {
                try {
                    con.close();
                } catch (Exception e){e.printStackTrace();}
            }

            return false;
        }

        if( !mflag ) {
            try {
                con.close();
            } catch (Exception e){e.printStackTrace();}
        }

        return true;
    }

    /**
     * 新增操作
     * @return boolean
     */
    public boolean insert() {
        PreparedStatement pstmt = null;

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try
        {
            int tCount = this.size();
            pstmt = con.prepareStatement("INSERT INTO LCRiskManagementInfo VALUES( ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ?)");
            for (int i = 1; i <= tCount; i++) {
            if(this.get(i).getSerialNo() == null || this.get(i).getSerialNo().equals("null")) {
            	pstmt.setString(1,null);
            } else {
            	pstmt.setString(1, this.get(i).getSerialNo());
            }
            if(this.get(i).getAccNo() == null || this.get(i).getAccNo().equals("null")) {
            	pstmt.setString(2,null);
            } else {
            	pstmt.setString(2, this.get(i).getAccNo());
            }
            if(this.get(i).getContNo() == null || this.get(i).getContNo().equals("null")) {
            	pstmt.setString(3,null);
            } else {
            	pstmt.setString(3, this.get(i).getContNo());
            }
            if(this.get(i).getPrtNo() == null || this.get(i).getPrtNo().equals("null")) {
            	pstmt.setString(4,null);
            } else {
            	pstmt.setString(4, this.get(i).getPrtNo());
            }
            if(this.get(i).getCaseNo() == null || this.get(i).getCaseNo().equals("null")) {
            	pstmt.setString(5,null);
            } else {
            	pstmt.setString(5, this.get(i).getCaseNo());
            }
            if(this.get(i).getRiskCode() == null || this.get(i).getRiskCode().equals("null")) {
            	pstmt.setString(6,null);
            } else {
            	pstmt.setString(6, this.get(i).getRiskCode());
            }
            if(this.get(i).getInsuredName() == null || this.get(i).getInsuredName().equals("null")) {
            	pstmt.setString(7,null);
            } else {
            	pstmt.setString(7, this.get(i).getInsuredName());
            }
            if(this.get(i).getInsuredIDType() == null || this.get(i).getInsuredIDType().equals("null")) {
            	pstmt.setString(8,null);
            } else {
            	pstmt.setString(8, this.get(i).getInsuredIDType());
            }
            if(this.get(i).getInsuredIDNo() == null || this.get(i).getInsuredIDNo().equals("null")) {
            	pstmt.setString(9,null);
            } else {
            	pstmt.setString(9, this.get(i).getInsuredIDNo());
            }
            if(this.get(i).getCompanyCode() == null || this.get(i).getCompanyCode().equals("null")) {
            	pstmt.setString(10,null);
            } else {
            	pstmt.setString(10, this.get(i).getCompanyCode());
            }
            if(this.get(i).getBusiness() == null || this.get(i).getBusiness().equals("null")) {
            	pstmt.setString(11,null);
            } else {
            	pstmt.setString(11, this.get(i).getBusiness());
            }
            if(this.get(i).getRiskType() == null || this.get(i).getRiskType().equals("null")) {
            	pstmt.setString(12,null);
            } else {
            	pstmt.setString(12, this.get(i).getRiskType());
            }
            if(this.get(i).getCustomerAllowed() == null || this.get(i).getCustomerAllowed().equals("null")) {
            	pstmt.setString(13,null);
            } else {
            	pstmt.setString(13, this.get(i).getCustomerAllowed());
            }
            if(this.get(i).getMultiCompany() == null || this.get(i).getMultiCompany().equals("null")) {
            	pstmt.setString(14,null);
            } else {
            	pstmt.setString(14, this.get(i).getMultiCompany());
            }
            if(this.get(i).getMajorDiseasePayment() == null || this.get(i).getMajorDiseasePayment().equals("null")) {
            	pstmt.setString(15,null);
            } else {
            	pstmt.setString(15, this.get(i).getMajorDiseasePayment());
            }
            if(this.get(i).getDisability() == null || this.get(i).getDisability().equals("null")) {
            	pstmt.setString(16,null);
            } else {
            	pstmt.setString(16, this.get(i).getDisability());
            }
            if(this.get(i).getDense() == null || this.get(i).getDense().equals("null")) {
            	pstmt.setString(17,null);
            } else {
            	pstmt.setString(17, this.get(i).getDense());
            }
            if(this.get(i).getAccumulativeMoney() == null || this.get(i).getAccumulativeMoney().equals("null")) {
            	pstmt.setString(18,null);
            } else {
            	pstmt.setString(18, this.get(i).getAccumulativeMoney());
            }
            if(this.get(i).getPageQueryCode() == null || this.get(i).getPageQueryCode().equals("null")) {
            	pstmt.setString(19,null);
            } else {
            	pstmt.setString(19, this.get(i).getPageQueryCode());
            }
            if(this.get(i).getTagDate() == null || this.get(i).getTagDate().equals("null")) {
            	pstmt.setString(20,null);
            } else {
            	pstmt.setString(20, this.get(i).getTagDate());
            }
            if(this.get(i).getDisplayPage() == null || this.get(i).getDisplayPage().equals("null")) {
            	pstmt.setString(21,null);
            } else {
            	pstmt.setString(21, this.get(i).getDisplayPage());
            }
            if(this.get(i).getAbnormalCheck() == null || this.get(i).getAbnormalCheck().equals("null")) {
            	pstmt.setString(22,null);
            } else {
            	pstmt.setString(22, this.get(i).getAbnormalCheck());
            }
            if(this.get(i).getAbnormalPayment() == null || this.get(i).getAbnormalPayment().equals("null")) {
            	pstmt.setString(23,null);
            } else {
            	pstmt.setString(23, this.get(i).getAbnormalPayment());
            }
            if(this.get(i).getChronicDiseasePayment() == null || this.get(i).getChronicDiseasePayment().equals("null")) {
            	pstmt.setString(24,null);
            } else {
            	pstmt.setString(24, this.get(i).getChronicDiseasePayment());
            }
            if(this.get(i).getPayMoney() == null || this.get(i).getPayMoney().equals("null")) {
            	pstmt.setString(25,null);
            } else {
            	pstmt.setString(25, this.get(i).getPayMoney());
            }
            if(this.get(i).getDayMoney() == null || this.get(i).getDayMoney().equals("null")) {
            	pstmt.setString(26,null);
            } else {
            	pstmt.setString(26, this.get(i).getDayMoney());
            }
            if(this.get(i).getCountOneYear() == null || this.get(i).getCountOneYear().equals("null")) {
            	pstmt.setString(27,null);
            } else {
            	pstmt.setString(27, this.get(i).getCountOneYear());
            }
            if(this.get(i).getPaymentCountOneYear() == null || this.get(i).getPaymentCountOneYear().equals("null")) {
            	pstmt.setString(28,null);
            } else {
            	pstmt.setString(28, this.get(i).getPaymentCountOneYear());
            }
            if(this.get(i).getPaymented() == null || this.get(i).getPaymented().equals("null")) {
            	pstmt.setString(29,null);
            } else {
            	pstmt.setString(29, this.get(i).getPaymented());
            }
            if(this.get(i).getPaymentCount() == null || this.get(i).getPaymentCount().equals("null")) {
            	pstmt.setString(30,null);
            } else {
            	pstmt.setString(30, this.get(i).getPaymentCount());
            }
            if(this.get(i).getDayMoneySum() == null || this.get(i).getDayMoneySum().equals("null")) {
            	pstmt.setString(31,null);
            } else {
            	pstmt.setString(31, this.get(i).getDayMoneySum());
            }
            if(this.get(i).getMajorDiseaseMoney() == null || this.get(i).getMajorDiseaseMoney().equals("null")) {
            	pstmt.setString(32,null);
            } else {
            	pstmt.setString(32, this.get(i).getMajorDiseaseMoney());
            }
            if(this.get(i).getPayCount() == null || this.get(i).getPayCount().equals("null")) {
            	pstmt.setString(33,null);
            } else {
            	pstmt.setString(33, this.get(i).getPayCount());
            }
            if(this.get(i).getPaymentDayCount() == null || this.get(i).getPaymentDayCount().equals("null")) {
            	pstmt.setString(34,null);
            } else {
            	pstmt.setString(34, this.get(i).getPaymentDayCount());
            }
            if(this.get(i).getPaymented2() == null || this.get(i).getPaymented2().equals("null")) {
            	pstmt.setString(35,null);
            } else {
            	pstmt.setString(35, this.get(i).getPaymented2());
            }
            if(this.get(i).getReceiptCode() == null || this.get(i).getReceiptCode().equals("null")) {
            	pstmt.setString(36,null);
            } else {
            	pstmt.setString(36, this.get(i).getReceiptCode());
            }
            if(this.get(i).getHospitalCode() == null || this.get(i).getHospitalCode().equals("null")) {
            	pstmt.setString(37,null);
            } else {
            	pstmt.setString(37, this.get(i).getHospitalCode());
            }
            if(this.get(i).getOperator() == null || this.get(i).getOperator().equals("null")) {
            	pstmt.setString(38,null);
            } else {
            	pstmt.setString(38, this.get(i).getOperator());
            }
            if(this.get(i).getMakeDate() == null || this.get(i).getMakeDate().equals("null")) {
                pstmt.setDate(39,null);
            } else {
                pstmt.setDate(39, Date.valueOf(this.get(i).getMakeDate()));
            }
            if(this.get(i).getMakeTime() == null || this.get(i).getMakeTime().equals("null")) {
            	pstmt.setString(40,null);
            } else {
            	pstmt.setString(40, this.get(i).getMakeTime());
            }
            if(this.get(i).getModifyDate() == null || this.get(i).getModifyDate().equals("null")) {
                pstmt.setDate(41,null);
            } else {
                pstmt.setDate(41, Date.valueOf(this.get(i).getModifyDate()));
            }
            if(this.get(i).getModifyTime() == null || this.get(i).getModifyTime().equals("null")) {
            	pstmt.setString(42,null);
            } else {
            	pstmt.setString(42, this.get(i).getModifyTime());
            }
            if(this.get(i).getInsurerUuid() == null || this.get(i).getInsurerUuid().equals("null")) {
            	pstmt.setString(43,null);
            } else {
            	pstmt.setString(43, this.get(i).getInsurerUuid());
            }
                pstmt.addBatch();
            }
            pstmt.executeBatch();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            ex.printStackTrace();
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LCRiskManagementInfoDBSet";
            tError.functionName = "insert()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            try {
                pstmt.close();
            } catch (Exception e){e.printStackTrace();}

            if( !mflag ) {
                try {
                    con.close();
                } catch (Exception e){e.printStackTrace();}
            }

            return false;
        }

        if( !mflag ) {
            try {
                con.close();
            } catch (Exception e){e.printStackTrace();}
        }

        return true;
    }
}
