/**
 * Copyright (c) 2017 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.entity;

import com.sinosoft.utility.Pojo;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;

import java.io.Serializable;

/**
 * <p>ClassName: VMS_OUTPUT_TRANSPojo </p>
 * <p>Copyright: Copyright (c) 2017</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2017-10-17
 */
public class VMS_OUTPUT_TRANSPojo implements Pojo,Serializable {
    // @Field
    /** 交易id */
    private String BUSINESS_ID; 
    /** 团体保单号 */
    private String GRPCONTNO; 
    /** 团体保险号 */
    private String GRPPOLNO; 
    /** 保单号 */
    private String CHERNUM; 
    /** 险种号 */
    private String POLNO; 
    /** 投保单号 */
    private String TTMPRCNO; 
    /** 客户号 */
    private String COWNNUM; 
    /** 交易币种 */
    private String ORIGCURR; 
    /** 税率 */
    private double TAX_RATE; 
    /** 价税合计金额（原币） */
    private double ORIG_AMT; 
    /** 收入金额（原币） */
    private double ORIG_INCOME; 
    /** 税额（原币） */
    private double ORIG_TAX_AMT; 
    /** 价税合计金额（本币） */
    private double AMT_CNY; 
    /** 收入金额（本币） */
    private double INCOME_CNY; 
    /** 税额(本币） */
    private double TAX_AMT_CNY; 
    /** 收付标识 */
    private String DC_FLAG; 
    /** 犹豫期 */
    private String HESITAGE; 
    /** 序列号 */
    private String SERIALNO; 
    /** 交易日期 */
    private String TRA_DT; 
    /** 交易类型 */
    private String TRA_TYP; 
    /** 交易描述 */
    private String BATCTRCDE; 
    /** 交易发生机构 */
    private String TRA_BRA; 
    /** 发票类型 */
    private String INVTYP; 
    /** 费用类型 */
    private String FEETYP; 
    /** 收入类型 */
    private String INCOMETYP; 
    /** 交费频率 */
    private String BILLFREQ; 
    /** 保单年度 */
    private String POLYEAR; 
    /** 承保日期 */
    private String HISSDTE; 
    /** 数据来源 */
    private String DSOURCE; 
    /** 会计科目代码 */
    private String GL_CD_ID; 
    /** 会计科目名称 */
    private String GL_CD_GRD; 
    /** 产品代码 */
    private String PRODUCT_COD; 
    /** 发票号 */
    private String INVONO; 
    /** Otherno */
    private String OTHERNO; 
    /** 险别 */
    private String INS_COD; 
    /** 交费起始日期 */
    private String INSTFROM; 
    /** 交费终止日期 */
    private String INSTTO; 
    /** 保单生效日期 */
    private String OCCDATE; 
    /** 期数 */
    private String PREMTERM; 
    /** 上传处理标记 */
    private String UPLOADFLAG; 
    /** 财务关联号 */
    private String BUSSNO; 
    /** 备用字段1 */
    private String STANDBYFLAG1; 
    /** 备用字段2 */
    private String STANDBYFLAG2; 
    /** 是否打发票标记 */
    private String FPFLAG; 
    /** 创建日期 */
    private String MAKEDATE; 
    /** 创建时间 */
    private String MAKETIME; 
    /** 修改日期 */
    private String MODIFYDATE; 
    /** 修改时间 */
    private String MODIFYTIME; 
    /** 产品名称 */
    private String PRODUCT_NAME; 
    /** 渠道 */
    private String SALECHNL; 


    public static final int FIELDNUM = 50;    // 数据库表的字段个数
    public String getBUSINESS_ID() {
        return BUSINESS_ID;
    }
    public void setBUSINESS_ID(String aBUSINESS_ID) {
        BUSINESS_ID = aBUSINESS_ID;
    }
    public String getGRPCONTNO() {
        return GRPCONTNO;
    }
    public void setGRPCONTNO(String aGRPCONTNO) {
        GRPCONTNO = aGRPCONTNO;
    }
    public String getGRPPOLNO() {
        return GRPPOLNO;
    }
    public void setGRPPOLNO(String aGRPPOLNO) {
        GRPPOLNO = aGRPPOLNO;
    }
    public String getCHERNUM() {
        return CHERNUM;
    }
    public void setCHERNUM(String aCHERNUM) {
        CHERNUM = aCHERNUM;
    }
    public String getPOLNO() {
        return POLNO;
    }
    public void setPOLNO(String aPOLNO) {
        POLNO = aPOLNO;
    }
    public String getTTMPRCNO() {
        return TTMPRCNO;
    }
    public void setTTMPRCNO(String aTTMPRCNO) {
        TTMPRCNO = aTTMPRCNO;
    }
    public String getCOWNNUM() {
        return COWNNUM;
    }
    public void setCOWNNUM(String aCOWNNUM) {
        COWNNUM = aCOWNNUM;
    }
    public String getORIGCURR() {
        return ORIGCURR;
    }
    public void setORIGCURR(String aORIGCURR) {
        ORIGCURR = aORIGCURR;
    }
    public double getTAX_RATE() {
        return TAX_RATE;
    }
    public void setTAX_RATE(double aTAX_RATE) {
        TAX_RATE = aTAX_RATE;
    }
    public void setTAX_RATE(String aTAX_RATE) {
        if (aTAX_RATE != null && !aTAX_RATE.equals("")) {
            Double tDouble = new Double(aTAX_RATE);
            double d = tDouble.doubleValue();
            TAX_RATE = d;
        }
    }

    public double getORIG_AMT() {
        return ORIG_AMT;
    }
    public void setORIG_AMT(double aORIG_AMT) {
        ORIG_AMT = aORIG_AMT;
    }
    public void setORIG_AMT(String aORIG_AMT) {
        if (aORIG_AMT != null && !aORIG_AMT.equals("")) {
            Double tDouble = new Double(aORIG_AMT);
            double d = tDouble.doubleValue();
            ORIG_AMT = d;
        }
    }

    public double getORIG_INCOME() {
        return ORIG_INCOME;
    }
    public void setORIG_INCOME(double aORIG_INCOME) {
        ORIG_INCOME = aORIG_INCOME;
    }
    public void setORIG_INCOME(String aORIG_INCOME) {
        if (aORIG_INCOME != null && !aORIG_INCOME.equals("")) {
            Double tDouble = new Double(aORIG_INCOME);
            double d = tDouble.doubleValue();
            ORIG_INCOME = d;
        }
    }

    public double getORIG_TAX_AMT() {
        return ORIG_TAX_AMT;
    }
    public void setORIG_TAX_AMT(double aORIG_TAX_AMT) {
        ORIG_TAX_AMT = aORIG_TAX_AMT;
    }
    public void setORIG_TAX_AMT(String aORIG_TAX_AMT) {
        if (aORIG_TAX_AMT != null && !aORIG_TAX_AMT.equals("")) {
            Double tDouble = new Double(aORIG_TAX_AMT);
            double d = tDouble.doubleValue();
            ORIG_TAX_AMT = d;
        }
    }

    public double getAMT_CNY() {
        return AMT_CNY;
    }
    public void setAMT_CNY(double aAMT_CNY) {
        AMT_CNY = aAMT_CNY;
    }
    public void setAMT_CNY(String aAMT_CNY) {
        if (aAMT_CNY != null && !aAMT_CNY.equals("")) {
            Double tDouble = new Double(aAMT_CNY);
            double d = tDouble.doubleValue();
            AMT_CNY = d;
        }
    }

    public double getINCOME_CNY() {
        return INCOME_CNY;
    }
    public void setINCOME_CNY(double aINCOME_CNY) {
        INCOME_CNY = aINCOME_CNY;
    }
    public void setINCOME_CNY(String aINCOME_CNY) {
        if (aINCOME_CNY != null && !aINCOME_CNY.equals("")) {
            Double tDouble = new Double(aINCOME_CNY);
            double d = tDouble.doubleValue();
            INCOME_CNY = d;
        }
    }

    public double getTAX_AMT_CNY() {
        return TAX_AMT_CNY;
    }
    public void setTAX_AMT_CNY(double aTAX_AMT_CNY) {
        TAX_AMT_CNY = aTAX_AMT_CNY;
    }
    public void setTAX_AMT_CNY(String aTAX_AMT_CNY) {
        if (aTAX_AMT_CNY != null && !aTAX_AMT_CNY.equals("")) {
            Double tDouble = new Double(aTAX_AMT_CNY);
            double d = tDouble.doubleValue();
            TAX_AMT_CNY = d;
        }
    }

    public String getDC_FLAG() {
        return DC_FLAG;
    }
    public void setDC_FLAG(String aDC_FLAG) {
        DC_FLAG = aDC_FLAG;
    }
    public String getHESITAGE() {
        return HESITAGE;
    }
    public void setHESITAGE(String aHESITAGE) {
        HESITAGE = aHESITAGE;
    }
    public String getSERIALNO() {
        return SERIALNO;
    }
    public void setSERIALNO(String aSERIALNO) {
        SERIALNO = aSERIALNO;
    }
    public String getTRA_DT() {
        return TRA_DT;
    }
    public void setTRA_DT(String aTRA_DT) {
        TRA_DT = aTRA_DT;
    }
    public String getTRA_TYP() {
        return TRA_TYP;
    }
    public void setTRA_TYP(String aTRA_TYP) {
        TRA_TYP = aTRA_TYP;
    }
    public String getBATCTRCDE() {
        return BATCTRCDE;
    }
    public void setBATCTRCDE(String aBATCTRCDE) {
        BATCTRCDE = aBATCTRCDE;
    }
    public String getTRA_BRA() {
        return TRA_BRA;
    }
    public void setTRA_BRA(String aTRA_BRA) {
        TRA_BRA = aTRA_BRA;
    }
    public String getINVTYP() {
        return INVTYP;
    }
    public void setINVTYP(String aINVTYP) {
        INVTYP = aINVTYP;
    }
    public String getFEETYP() {
        return FEETYP;
    }
    public void setFEETYP(String aFEETYP) {
        FEETYP = aFEETYP;
    }
    public String getINCOMETYP() {
        return INCOMETYP;
    }
    public void setINCOMETYP(String aINCOMETYP) {
        INCOMETYP = aINCOMETYP;
    }
    public String getBILLFREQ() {
        return BILLFREQ;
    }
    public void setBILLFREQ(String aBILLFREQ) {
        BILLFREQ = aBILLFREQ;
    }
    public String getPOLYEAR() {
        return POLYEAR;
    }
    public void setPOLYEAR(String aPOLYEAR) {
        POLYEAR = aPOLYEAR;
    }
    public String getHISSDTE() {
        return HISSDTE;
    }
    public void setHISSDTE(String aHISSDTE) {
        HISSDTE = aHISSDTE;
    }
    public String getDSOURCE() {
        return DSOURCE;
    }
    public void setDSOURCE(String aDSOURCE) {
        DSOURCE = aDSOURCE;
    }
    public String getGL_CD_ID() {
        return GL_CD_ID;
    }
    public void setGL_CD_ID(String aGL_CD_ID) {
        GL_CD_ID = aGL_CD_ID;
    }
    public String getGL_CD_GRD() {
        return GL_CD_GRD;
    }
    public void setGL_CD_GRD(String aGL_CD_GRD) {
        GL_CD_GRD = aGL_CD_GRD;
    }
    public String getPRODUCT_COD() {
        return PRODUCT_COD;
    }
    public void setPRODUCT_COD(String aPRODUCT_COD) {
        PRODUCT_COD = aPRODUCT_COD;
    }
    public String getINVONO() {
        return INVONO;
    }
    public void setINVONO(String aINVONO) {
        INVONO = aINVONO;
    }
    public String getOTHERNO() {
        return OTHERNO;
    }
    public void setOTHERNO(String aOTHERNO) {
        OTHERNO = aOTHERNO;
    }
    public String getINS_COD() {
        return INS_COD;
    }
    public void setINS_COD(String aINS_COD) {
        INS_COD = aINS_COD;
    }
    public String getINSTFROM() {
        return INSTFROM;
    }
    public void setINSTFROM(String aINSTFROM) {
        INSTFROM = aINSTFROM;
    }
    public String getINSTTO() {
        return INSTTO;
    }
    public void setINSTTO(String aINSTTO) {
        INSTTO = aINSTTO;
    }
    public String getOCCDATE() {
        return OCCDATE;
    }
    public void setOCCDATE(String aOCCDATE) {
        OCCDATE = aOCCDATE;
    }
    public String getPREMTERM() {
        return PREMTERM;
    }
    public void setPREMTERM(String aPREMTERM) {
        PREMTERM = aPREMTERM;
    }
    public String getUPLOADFLAG() {
        return UPLOADFLAG;
    }
    public void setUPLOADFLAG(String aUPLOADFLAG) {
        UPLOADFLAG = aUPLOADFLAG;
    }
    public String getBUSSNO() {
        return BUSSNO;
    }
    public void setBUSSNO(String aBUSSNO) {
        BUSSNO = aBUSSNO;
    }
    public String getSTANDBYFLAG1() {
        return STANDBYFLAG1;
    }
    public void setSTANDBYFLAG1(String aSTANDBYFLAG1) {
        STANDBYFLAG1 = aSTANDBYFLAG1;
    }
    public String getSTANDBYFLAG2() {
        return STANDBYFLAG2;
    }
    public void setSTANDBYFLAG2(String aSTANDBYFLAG2) {
        STANDBYFLAG2 = aSTANDBYFLAG2;
    }
    public String getFPFLAG() {
        return FPFLAG;
    }
    public void setFPFLAG(String aFPFLAG) {
        FPFLAG = aFPFLAG;
    }
    public String getMAKEDATE() {
        return MAKEDATE;
    }
    public void setMAKEDATE(String aMAKEDATE) {
        MAKEDATE = aMAKEDATE;
    }
    public String getMAKETIME() {
        return MAKETIME;
    }
    public void setMAKETIME(String aMAKETIME) {
        MAKETIME = aMAKETIME;
    }
    public String getMODIFYDATE() {
        return MODIFYDATE;
    }
    public void setMODIFYDATE(String aMODIFYDATE) {
        MODIFYDATE = aMODIFYDATE;
    }
    public String getMODIFYTIME() {
        return MODIFYTIME;
    }
    public void setMODIFYTIME(String aMODIFYTIME) {
        MODIFYTIME = aMODIFYTIME;
    }
    public String getPRODUCT_NAME() {
        return PRODUCT_NAME;
    }
    public void setPRODUCT_NAME(String aPRODUCT_NAME) {
        PRODUCT_NAME = aPRODUCT_NAME;
    }
    public String getSALECHNL() {
        return SALECHNL;
    }
    public void setSALECHNL(String aSALECHNL) {
        SALECHNL = aSALECHNL;
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("BUSINESS_ID") ) {
            return 0;
        }
        if( strFieldName.equals("GRPCONTNO") ) {
            return 1;
        }
        if( strFieldName.equals("GRPPOLNO") ) {
            return 2;
        }
        if( strFieldName.equals("CHERNUM") ) {
            return 3;
        }
        if( strFieldName.equals("POLNO") ) {
            return 4;
        }
        if( strFieldName.equals("TTMPRCNO") ) {
            return 5;
        }
        if( strFieldName.equals("COWNNUM") ) {
            return 6;
        }
        if( strFieldName.equals("ORIGCURR") ) {
            return 7;
        }
        if( strFieldName.equals("TAX_RATE") ) {
            return 8;
        }
        if( strFieldName.equals("ORIG_AMT") ) {
            return 9;
        }
        if( strFieldName.equals("ORIG_INCOME") ) {
            return 10;
        }
        if( strFieldName.equals("ORIG_TAX_AMT") ) {
            return 11;
        }
        if( strFieldName.equals("AMT_CNY") ) {
            return 12;
        }
        if( strFieldName.equals("INCOME_CNY") ) {
            return 13;
        }
        if( strFieldName.equals("TAX_AMT_CNY") ) {
            return 14;
        }
        if( strFieldName.equals("DC_FLAG") ) {
            return 15;
        }
        if( strFieldName.equals("HESITAGE") ) {
            return 16;
        }
        if( strFieldName.equals("SERIALNO") ) {
            return 17;
        }
        if( strFieldName.equals("TRA_DT") ) {
            return 18;
        }
        if( strFieldName.equals("TRA_TYP") ) {
            return 19;
        }
        if( strFieldName.equals("BATCTRCDE") ) {
            return 20;
        }
        if( strFieldName.equals("TRA_BRA") ) {
            return 21;
        }
        if( strFieldName.equals("INVTYP") ) {
            return 22;
        }
        if( strFieldName.equals("FEETYP") ) {
            return 23;
        }
        if( strFieldName.equals("INCOMETYP") ) {
            return 24;
        }
        if( strFieldName.equals("BILLFREQ") ) {
            return 25;
        }
        if( strFieldName.equals("POLYEAR") ) {
            return 26;
        }
        if( strFieldName.equals("HISSDTE") ) {
            return 27;
        }
        if( strFieldName.equals("DSOURCE") ) {
            return 28;
        }
        if( strFieldName.equals("GL_CD_ID") ) {
            return 29;
        }
        if( strFieldName.equals("GL_CD_GRD") ) {
            return 30;
        }
        if( strFieldName.equals("PRODUCT_COD") ) {
            return 31;
        }
        if( strFieldName.equals("INVONO") ) {
            return 32;
        }
        if( strFieldName.equals("OTHERNO") ) {
            return 33;
        }
        if( strFieldName.equals("INS_COD") ) {
            return 34;
        }
        if( strFieldName.equals("INSTFROM") ) {
            return 35;
        }
        if( strFieldName.equals("INSTTO") ) {
            return 36;
        }
        if( strFieldName.equals("OCCDATE") ) {
            return 37;
        }
        if( strFieldName.equals("PREMTERM") ) {
            return 38;
        }
        if( strFieldName.equals("UPLOADFLAG") ) {
            return 39;
        }
        if( strFieldName.equals("BUSSNO") ) {
            return 40;
        }
        if( strFieldName.equals("STANDBYFLAG1") ) {
            return 41;
        }
        if( strFieldName.equals("STANDBYFLAG2") ) {
            return 42;
        }
        if( strFieldName.equals("FPFLAG") ) {
            return 43;
        }
        if( strFieldName.equals("MAKEDATE") ) {
            return 44;
        }
        if( strFieldName.equals("MAKETIME") ) {
            return 45;
        }
        if( strFieldName.equals("MODIFYDATE") ) {
            return 46;
        }
        if( strFieldName.equals("MODIFYTIME") ) {
            return 47;
        }
        if( strFieldName.equals("PRODUCT_NAME") ) {
            return 48;
        }
        if( strFieldName.equals("SALECHNL") ) {
            return 49;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "BUSINESS_ID";
                break;
            case 1:
                strFieldName = "GRPCONTNO";
                break;
            case 2:
                strFieldName = "GRPPOLNO";
                break;
            case 3:
                strFieldName = "CHERNUM";
                break;
            case 4:
                strFieldName = "POLNO";
                break;
            case 5:
                strFieldName = "TTMPRCNO";
                break;
            case 6:
                strFieldName = "COWNNUM";
                break;
            case 7:
                strFieldName = "ORIGCURR";
                break;
            case 8:
                strFieldName = "TAX_RATE";
                break;
            case 9:
                strFieldName = "ORIG_AMT";
                break;
            case 10:
                strFieldName = "ORIG_INCOME";
                break;
            case 11:
                strFieldName = "ORIG_TAX_AMT";
                break;
            case 12:
                strFieldName = "AMT_CNY";
                break;
            case 13:
                strFieldName = "INCOME_CNY";
                break;
            case 14:
                strFieldName = "TAX_AMT_CNY";
                break;
            case 15:
                strFieldName = "DC_FLAG";
                break;
            case 16:
                strFieldName = "HESITAGE";
                break;
            case 17:
                strFieldName = "SERIALNO";
                break;
            case 18:
                strFieldName = "TRA_DT";
                break;
            case 19:
                strFieldName = "TRA_TYP";
                break;
            case 20:
                strFieldName = "BATCTRCDE";
                break;
            case 21:
                strFieldName = "TRA_BRA";
                break;
            case 22:
                strFieldName = "INVTYP";
                break;
            case 23:
                strFieldName = "FEETYP";
                break;
            case 24:
                strFieldName = "INCOMETYP";
                break;
            case 25:
                strFieldName = "BILLFREQ";
                break;
            case 26:
                strFieldName = "POLYEAR";
                break;
            case 27:
                strFieldName = "HISSDTE";
                break;
            case 28:
                strFieldName = "DSOURCE";
                break;
            case 29:
                strFieldName = "GL_CD_ID";
                break;
            case 30:
                strFieldName = "GL_CD_GRD";
                break;
            case 31:
                strFieldName = "PRODUCT_COD";
                break;
            case 32:
                strFieldName = "INVONO";
                break;
            case 33:
                strFieldName = "OTHERNO";
                break;
            case 34:
                strFieldName = "INS_COD";
                break;
            case 35:
                strFieldName = "INSTFROM";
                break;
            case 36:
                strFieldName = "INSTTO";
                break;
            case 37:
                strFieldName = "OCCDATE";
                break;
            case 38:
                strFieldName = "PREMTERM";
                break;
            case 39:
                strFieldName = "UPLOADFLAG";
                break;
            case 40:
                strFieldName = "BUSSNO";
                break;
            case 41:
                strFieldName = "STANDBYFLAG1";
                break;
            case 42:
                strFieldName = "STANDBYFLAG2";
                break;
            case 43:
                strFieldName = "FPFLAG";
                break;
            case 44:
                strFieldName = "MAKEDATE";
                break;
            case 45:
                strFieldName = "MAKETIME";
                break;
            case 46:
                strFieldName = "MODIFYDATE";
                break;
            case 47:
                strFieldName = "MODIFYTIME";
                break;
            case 48:
                strFieldName = "PRODUCT_NAME";
                break;
            case 49:
                strFieldName = "SALECHNL";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "BUSINESS_ID":
                return Schema.TYPE_STRING;
            case "GRPCONTNO":
                return Schema.TYPE_STRING;
            case "GRPPOLNO":
                return Schema.TYPE_STRING;
            case "CHERNUM":
                return Schema.TYPE_STRING;
            case "POLNO":
                return Schema.TYPE_STRING;
            case "TTMPRCNO":
                return Schema.TYPE_STRING;
            case "COWNNUM":
                return Schema.TYPE_STRING;
            case "ORIGCURR":
                return Schema.TYPE_STRING;
            case "TAX_RATE":
                return Schema.TYPE_DOUBLE;
            case "ORIG_AMT":
                return Schema.TYPE_DOUBLE;
            case "ORIG_INCOME":
                return Schema.TYPE_DOUBLE;
            case "ORIG_TAX_AMT":
                return Schema.TYPE_DOUBLE;
            case "AMT_CNY":
                return Schema.TYPE_DOUBLE;
            case "INCOME_CNY":
                return Schema.TYPE_DOUBLE;
            case "TAX_AMT_CNY":
                return Schema.TYPE_DOUBLE;
            case "DC_FLAG":
                return Schema.TYPE_STRING;
            case "HESITAGE":
                return Schema.TYPE_STRING;
            case "SERIALNO":
                return Schema.TYPE_STRING;
            case "TRA_DT":
                return Schema.TYPE_STRING;
            case "TRA_TYP":
                return Schema.TYPE_STRING;
            case "BATCTRCDE":
                return Schema.TYPE_STRING;
            case "TRA_BRA":
                return Schema.TYPE_STRING;
            case "INVTYP":
                return Schema.TYPE_STRING;
            case "FEETYP":
                return Schema.TYPE_STRING;
            case "INCOMETYP":
                return Schema.TYPE_STRING;
            case "BILLFREQ":
                return Schema.TYPE_STRING;
            case "POLYEAR":
                return Schema.TYPE_STRING;
            case "HISSDTE":
                return Schema.TYPE_STRING;
            case "DSOURCE":
                return Schema.TYPE_STRING;
            case "GL_CD_ID":
                return Schema.TYPE_STRING;
            case "GL_CD_GRD":
                return Schema.TYPE_STRING;
            case "PRODUCT_COD":
                return Schema.TYPE_STRING;
            case "INVONO":
                return Schema.TYPE_STRING;
            case "OTHERNO":
                return Schema.TYPE_STRING;
            case "INS_COD":
                return Schema.TYPE_STRING;
            case "INSTFROM":
                return Schema.TYPE_STRING;
            case "INSTTO":
                return Schema.TYPE_STRING;
            case "OCCDATE":
                return Schema.TYPE_STRING;
            case "PREMTERM":
                return Schema.TYPE_STRING;
            case "UPLOADFLAG":
                return Schema.TYPE_STRING;
            case "BUSSNO":
                return Schema.TYPE_STRING;
            case "STANDBYFLAG1":
                return Schema.TYPE_STRING;
            case "STANDBYFLAG2":
                return Schema.TYPE_STRING;
            case "FPFLAG":
                return Schema.TYPE_STRING;
            case "MAKEDATE":
                return Schema.TYPE_STRING;
            case "MAKETIME":
                return Schema.TYPE_STRING;
            case "MODIFYDATE":
                return Schema.TYPE_STRING;
            case "MODIFYTIME":
                return Schema.TYPE_STRING;
            case "PRODUCT_NAME":
                return Schema.TYPE_STRING;
            case "SALECHNL":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_STRING;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_STRING;
            case 6:
                return Schema.TYPE_STRING;
            case 7:
                return Schema.TYPE_STRING;
            case 8:
                return Schema.TYPE_DOUBLE;
            case 9:
                return Schema.TYPE_DOUBLE;
            case 10:
                return Schema.TYPE_DOUBLE;
            case 11:
                return Schema.TYPE_DOUBLE;
            case 12:
                return Schema.TYPE_DOUBLE;
            case 13:
                return Schema.TYPE_DOUBLE;
            case 14:
                return Schema.TYPE_DOUBLE;
            case 15:
                return Schema.TYPE_STRING;
            case 16:
                return Schema.TYPE_STRING;
            case 17:
                return Schema.TYPE_STRING;
            case 18:
                return Schema.TYPE_STRING;
            case 19:
                return Schema.TYPE_STRING;
            case 20:
                return Schema.TYPE_STRING;
            case 21:
                return Schema.TYPE_STRING;
            case 22:
                return Schema.TYPE_STRING;
            case 23:
                return Schema.TYPE_STRING;
            case 24:
                return Schema.TYPE_STRING;
            case 25:
                return Schema.TYPE_STRING;
            case 26:
                return Schema.TYPE_STRING;
            case 27:
                return Schema.TYPE_STRING;
            case 28:
                return Schema.TYPE_STRING;
            case 29:
                return Schema.TYPE_STRING;
            case 30:
                return Schema.TYPE_STRING;
            case 31:
                return Schema.TYPE_STRING;
            case 32:
                return Schema.TYPE_STRING;
            case 33:
                return Schema.TYPE_STRING;
            case 34:
                return Schema.TYPE_STRING;
            case 35:
                return Schema.TYPE_STRING;
            case 36:
                return Schema.TYPE_STRING;
            case 37:
                return Schema.TYPE_STRING;
            case 38:
                return Schema.TYPE_STRING;
            case 39:
                return Schema.TYPE_STRING;
            case 40:
                return Schema.TYPE_STRING;
            case 41:
                return Schema.TYPE_STRING;
            case 42:
                return Schema.TYPE_STRING;
            case 43:
                return Schema.TYPE_STRING;
            case 44:
                return Schema.TYPE_STRING;
            case 45:
                return Schema.TYPE_STRING;
            case 46:
                return Schema.TYPE_STRING;
            case 47:
                return Schema.TYPE_STRING;
            case 48:
                return Schema.TYPE_STRING;
            case 49:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("BUSINESS_ID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BUSINESS_ID));
        }
        if (FCode.equalsIgnoreCase("GRPCONTNO")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GRPCONTNO));
        }
        if (FCode.equalsIgnoreCase("GRPPOLNO")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GRPPOLNO));
        }
        if (FCode.equalsIgnoreCase("CHERNUM")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CHERNUM));
        }
        if (FCode.equalsIgnoreCase("POLNO")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(POLNO));
        }
        if (FCode.equalsIgnoreCase("TTMPRCNO")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(TTMPRCNO));
        }
        if (FCode.equalsIgnoreCase("COWNNUM")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(COWNNUM));
        }
        if (FCode.equalsIgnoreCase("ORIGCURR")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ORIGCURR));
        }
        if (FCode.equalsIgnoreCase("TAX_RATE")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(TAX_RATE));
        }
        if (FCode.equalsIgnoreCase("ORIG_AMT")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ORIG_AMT));
        }
        if (FCode.equalsIgnoreCase("ORIG_INCOME")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ORIG_INCOME));
        }
        if (FCode.equalsIgnoreCase("ORIG_TAX_AMT")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ORIG_TAX_AMT));
        }
        if (FCode.equalsIgnoreCase("AMT_CNY")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AMT_CNY));
        }
        if (FCode.equalsIgnoreCase("INCOME_CNY")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(INCOME_CNY));
        }
        if (FCode.equalsIgnoreCase("TAX_AMT_CNY")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(TAX_AMT_CNY));
        }
        if (FCode.equalsIgnoreCase("DC_FLAG")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DC_FLAG));
        }
        if (FCode.equalsIgnoreCase("HESITAGE")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(HESITAGE));
        }
        if (FCode.equalsIgnoreCase("SERIALNO")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SERIALNO));
        }
        if (FCode.equalsIgnoreCase("TRA_DT")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(TRA_DT));
        }
        if (FCode.equalsIgnoreCase("TRA_TYP")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(TRA_TYP));
        }
        if (FCode.equalsIgnoreCase("BATCTRCDE")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BATCTRCDE));
        }
        if (FCode.equalsIgnoreCase("TRA_BRA")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(TRA_BRA));
        }
        if (FCode.equalsIgnoreCase("INVTYP")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(INVTYP));
        }
        if (FCode.equalsIgnoreCase("FEETYP")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FEETYP));
        }
        if (FCode.equalsIgnoreCase("INCOMETYP")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(INCOMETYP));
        }
        if (FCode.equalsIgnoreCase("BILLFREQ")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BILLFREQ));
        }
        if (FCode.equalsIgnoreCase("POLYEAR")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(POLYEAR));
        }
        if (FCode.equalsIgnoreCase("HISSDTE")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(HISSDTE));
        }
        if (FCode.equalsIgnoreCase("DSOURCE")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DSOURCE));
        }
        if (FCode.equalsIgnoreCase("GL_CD_ID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GL_CD_ID));
        }
        if (FCode.equalsIgnoreCase("GL_CD_GRD")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GL_CD_GRD));
        }
        if (FCode.equalsIgnoreCase("PRODUCT_COD")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PRODUCT_COD));
        }
        if (FCode.equalsIgnoreCase("INVONO")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(INVONO));
        }
        if (FCode.equalsIgnoreCase("OTHERNO")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OTHERNO));
        }
        if (FCode.equalsIgnoreCase("INS_COD")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(INS_COD));
        }
        if (FCode.equalsIgnoreCase("INSTFROM")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(INSTFROM));
        }
        if (FCode.equalsIgnoreCase("INSTTO")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(INSTTO));
        }
        if (FCode.equalsIgnoreCase("OCCDATE")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OCCDATE));
        }
        if (FCode.equalsIgnoreCase("PREMTERM")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PREMTERM));
        }
        if (FCode.equalsIgnoreCase("UPLOADFLAG")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(UPLOADFLAG));
        }
        if (FCode.equalsIgnoreCase("BUSSNO")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BUSSNO));
        }
        if (FCode.equalsIgnoreCase("STANDBYFLAG1")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(STANDBYFLAG1));
        }
        if (FCode.equalsIgnoreCase("STANDBYFLAG2")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(STANDBYFLAG2));
        }
        if (FCode.equalsIgnoreCase("FPFLAG")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FPFLAG));
        }
        if (FCode.equalsIgnoreCase("MAKEDATE")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MAKEDATE));
        }
        if (FCode.equalsIgnoreCase("MAKETIME")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MAKETIME));
        }
        if (FCode.equalsIgnoreCase("MODIFYDATE")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MODIFYDATE));
        }
        if (FCode.equalsIgnoreCase("MODIFYTIME")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MODIFYTIME));
        }
        if (FCode.equalsIgnoreCase("PRODUCT_NAME")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PRODUCT_NAME));
        }
        if (FCode.equalsIgnoreCase("SALECHNL")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SALECHNL));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = String.valueOf(BUSINESS_ID);
                break;
            case 1:
                strFieldValue = String.valueOf(GRPCONTNO);
                break;
            case 2:
                strFieldValue = String.valueOf(GRPPOLNO);
                break;
            case 3:
                strFieldValue = String.valueOf(CHERNUM);
                break;
            case 4:
                strFieldValue = String.valueOf(POLNO);
                break;
            case 5:
                strFieldValue = String.valueOf(TTMPRCNO);
                break;
            case 6:
                strFieldValue = String.valueOf(COWNNUM);
                break;
            case 7:
                strFieldValue = String.valueOf(ORIGCURR);
                break;
            case 8:
                strFieldValue = String.valueOf(TAX_RATE);
                break;
            case 9:
                strFieldValue = String.valueOf(ORIG_AMT);
                break;
            case 10:
                strFieldValue = String.valueOf(ORIG_INCOME);
                break;
            case 11:
                strFieldValue = String.valueOf(ORIG_TAX_AMT);
                break;
            case 12:
                strFieldValue = String.valueOf(AMT_CNY);
                break;
            case 13:
                strFieldValue = String.valueOf(INCOME_CNY);
                break;
            case 14:
                strFieldValue = String.valueOf(TAX_AMT_CNY);
                break;
            case 15:
                strFieldValue = String.valueOf(DC_FLAG);
                break;
            case 16:
                strFieldValue = String.valueOf(HESITAGE);
                break;
            case 17:
                strFieldValue = String.valueOf(SERIALNO);
                break;
            case 18:
                strFieldValue = String.valueOf(TRA_DT);
                break;
            case 19:
                strFieldValue = String.valueOf(TRA_TYP);
                break;
            case 20:
                strFieldValue = String.valueOf(BATCTRCDE);
                break;
            case 21:
                strFieldValue = String.valueOf(TRA_BRA);
                break;
            case 22:
                strFieldValue = String.valueOf(INVTYP);
                break;
            case 23:
                strFieldValue = String.valueOf(FEETYP);
                break;
            case 24:
                strFieldValue = String.valueOf(INCOMETYP);
                break;
            case 25:
                strFieldValue = String.valueOf(BILLFREQ);
                break;
            case 26:
                strFieldValue = String.valueOf(POLYEAR);
                break;
            case 27:
                strFieldValue = String.valueOf(HISSDTE);
                break;
            case 28:
                strFieldValue = String.valueOf(DSOURCE);
                break;
            case 29:
                strFieldValue = String.valueOf(GL_CD_ID);
                break;
            case 30:
                strFieldValue = String.valueOf(GL_CD_GRD);
                break;
            case 31:
                strFieldValue = String.valueOf(PRODUCT_COD);
                break;
            case 32:
                strFieldValue = String.valueOf(INVONO);
                break;
            case 33:
                strFieldValue = String.valueOf(OTHERNO);
                break;
            case 34:
                strFieldValue = String.valueOf(INS_COD);
                break;
            case 35:
                strFieldValue = String.valueOf(INSTFROM);
                break;
            case 36:
                strFieldValue = String.valueOf(INSTTO);
                break;
            case 37:
                strFieldValue = String.valueOf(OCCDATE);
                break;
            case 38:
                strFieldValue = String.valueOf(PREMTERM);
                break;
            case 39:
                strFieldValue = String.valueOf(UPLOADFLAG);
                break;
            case 40:
                strFieldValue = String.valueOf(BUSSNO);
                break;
            case 41:
                strFieldValue = String.valueOf(STANDBYFLAG1);
                break;
            case 42:
                strFieldValue = String.valueOf(STANDBYFLAG2);
                break;
            case 43:
                strFieldValue = String.valueOf(FPFLAG);
                break;
            case 44:
                strFieldValue = String.valueOf(MAKEDATE);
                break;
            case 45:
                strFieldValue = String.valueOf(MAKETIME);
                break;
            case 46:
                strFieldValue = String.valueOf(MODIFYDATE);
                break;
            case 47:
                strFieldValue = String.valueOf(MODIFYTIME);
                break;
            case 48:
                strFieldValue = String.valueOf(PRODUCT_NAME);
                break;
            case 49:
                strFieldValue = String.valueOf(SALECHNL);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("BUSINESS_ID")) {
            if( FValue != null && !FValue.equals(""))
            {
                BUSINESS_ID = FValue.trim();
            }
            else
                BUSINESS_ID = null;
        }
        if (FCode.equalsIgnoreCase("GRPCONTNO")) {
            if( FValue != null && !FValue.equals(""))
            {
                GRPCONTNO = FValue.trim();
            }
            else
                GRPCONTNO = null;
        }
        if (FCode.equalsIgnoreCase("GRPPOLNO")) {
            if( FValue != null && !FValue.equals(""))
            {
                GRPPOLNO = FValue.trim();
            }
            else
                GRPPOLNO = null;
        }
        if (FCode.equalsIgnoreCase("CHERNUM")) {
            if( FValue != null && !FValue.equals(""))
            {
                CHERNUM = FValue.trim();
            }
            else
                CHERNUM = null;
        }
        if (FCode.equalsIgnoreCase("POLNO")) {
            if( FValue != null && !FValue.equals(""))
            {
                POLNO = FValue.trim();
            }
            else
                POLNO = null;
        }
        if (FCode.equalsIgnoreCase("TTMPRCNO")) {
            if( FValue != null && !FValue.equals(""))
            {
                TTMPRCNO = FValue.trim();
            }
            else
                TTMPRCNO = null;
        }
        if (FCode.equalsIgnoreCase("COWNNUM")) {
            if( FValue != null && !FValue.equals(""))
            {
                COWNNUM = FValue.trim();
            }
            else
                COWNNUM = null;
        }
        if (FCode.equalsIgnoreCase("ORIGCURR")) {
            if( FValue != null && !FValue.equals(""))
            {
                ORIGCURR = FValue.trim();
            }
            else
                ORIGCURR = null;
        }
        if (FCode.equalsIgnoreCase("TAX_RATE")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                TAX_RATE = d;
            }
        }
        if (FCode.equalsIgnoreCase("ORIG_AMT")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                ORIG_AMT = d;
            }
        }
        if (FCode.equalsIgnoreCase("ORIG_INCOME")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                ORIG_INCOME = d;
            }
        }
        if (FCode.equalsIgnoreCase("ORIG_TAX_AMT")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                ORIG_TAX_AMT = d;
            }
        }
        if (FCode.equalsIgnoreCase("AMT_CNY")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                AMT_CNY = d;
            }
        }
        if (FCode.equalsIgnoreCase("INCOME_CNY")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                INCOME_CNY = d;
            }
        }
        if (FCode.equalsIgnoreCase("TAX_AMT_CNY")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                TAX_AMT_CNY = d;
            }
        }
        if (FCode.equalsIgnoreCase("DC_FLAG")) {
            if( FValue != null && !FValue.equals(""))
            {
                DC_FLAG = FValue.trim();
            }
            else
                DC_FLAG = null;
        }
        if (FCode.equalsIgnoreCase("HESITAGE")) {
            if( FValue != null && !FValue.equals(""))
            {
                HESITAGE = FValue.trim();
            }
            else
                HESITAGE = null;
        }
        if (FCode.equalsIgnoreCase("SERIALNO")) {
            if( FValue != null && !FValue.equals(""))
            {
                SERIALNO = FValue.trim();
            }
            else
                SERIALNO = null;
        }
        if (FCode.equalsIgnoreCase("TRA_DT")) {
            if( FValue != null && !FValue.equals(""))
            {
                TRA_DT = FValue.trim();
            }
            else
                TRA_DT = null;
        }
        if (FCode.equalsIgnoreCase("TRA_TYP")) {
            if( FValue != null && !FValue.equals(""))
            {
                TRA_TYP = FValue.trim();
            }
            else
                TRA_TYP = null;
        }
        if (FCode.equalsIgnoreCase("BATCTRCDE")) {
            if( FValue != null && !FValue.equals(""))
            {
                BATCTRCDE = FValue.trim();
            }
            else
                BATCTRCDE = null;
        }
        if (FCode.equalsIgnoreCase("TRA_BRA")) {
            if( FValue != null && !FValue.equals(""))
            {
                TRA_BRA = FValue.trim();
            }
            else
                TRA_BRA = null;
        }
        if (FCode.equalsIgnoreCase("INVTYP")) {
            if( FValue != null && !FValue.equals(""))
            {
                INVTYP = FValue.trim();
            }
            else
                INVTYP = null;
        }
        if (FCode.equalsIgnoreCase("FEETYP")) {
            if( FValue != null && !FValue.equals(""))
            {
                FEETYP = FValue.trim();
            }
            else
                FEETYP = null;
        }
        if (FCode.equalsIgnoreCase("INCOMETYP")) {
            if( FValue != null && !FValue.equals(""))
            {
                INCOMETYP = FValue.trim();
            }
            else
                INCOMETYP = null;
        }
        if (FCode.equalsIgnoreCase("BILLFREQ")) {
            if( FValue != null && !FValue.equals(""))
            {
                BILLFREQ = FValue.trim();
            }
            else
                BILLFREQ = null;
        }
        if (FCode.equalsIgnoreCase("POLYEAR")) {
            if( FValue != null && !FValue.equals(""))
            {
                POLYEAR = FValue.trim();
            }
            else
                POLYEAR = null;
        }
        if (FCode.equalsIgnoreCase("HISSDTE")) {
            if( FValue != null && !FValue.equals(""))
            {
                HISSDTE = FValue.trim();
            }
            else
                HISSDTE = null;
        }
        if (FCode.equalsIgnoreCase("DSOURCE")) {
            if( FValue != null && !FValue.equals(""))
            {
                DSOURCE = FValue.trim();
            }
            else
                DSOURCE = null;
        }
        if (FCode.equalsIgnoreCase("GL_CD_ID")) {
            if( FValue != null && !FValue.equals(""))
            {
                GL_CD_ID = FValue.trim();
            }
            else
                GL_CD_ID = null;
        }
        if (FCode.equalsIgnoreCase("GL_CD_GRD")) {
            if( FValue != null && !FValue.equals(""))
            {
                GL_CD_GRD = FValue.trim();
            }
            else
                GL_CD_GRD = null;
        }
        if (FCode.equalsIgnoreCase("PRODUCT_COD")) {
            if( FValue != null && !FValue.equals(""))
            {
                PRODUCT_COD = FValue.trim();
            }
            else
                PRODUCT_COD = null;
        }
        if (FCode.equalsIgnoreCase("INVONO")) {
            if( FValue != null && !FValue.equals(""))
            {
                INVONO = FValue.trim();
            }
            else
                INVONO = null;
        }
        if (FCode.equalsIgnoreCase("OTHERNO")) {
            if( FValue != null && !FValue.equals(""))
            {
                OTHERNO = FValue.trim();
            }
            else
                OTHERNO = null;
        }
        if (FCode.equalsIgnoreCase("INS_COD")) {
            if( FValue != null && !FValue.equals(""))
            {
                INS_COD = FValue.trim();
            }
            else
                INS_COD = null;
        }
        if (FCode.equalsIgnoreCase("INSTFROM")) {
            if( FValue != null && !FValue.equals(""))
            {
                INSTFROM = FValue.trim();
            }
            else
                INSTFROM = null;
        }
        if (FCode.equalsIgnoreCase("INSTTO")) {
            if( FValue != null && !FValue.equals(""))
            {
                INSTTO = FValue.trim();
            }
            else
                INSTTO = null;
        }
        if (FCode.equalsIgnoreCase("OCCDATE")) {
            if( FValue != null && !FValue.equals(""))
            {
                OCCDATE = FValue.trim();
            }
            else
                OCCDATE = null;
        }
        if (FCode.equalsIgnoreCase("PREMTERM")) {
            if( FValue != null && !FValue.equals(""))
            {
                PREMTERM = FValue.trim();
            }
            else
                PREMTERM = null;
        }
        if (FCode.equalsIgnoreCase("UPLOADFLAG")) {
            if( FValue != null && !FValue.equals(""))
            {
                UPLOADFLAG = FValue.trim();
            }
            else
                UPLOADFLAG = null;
        }
        if (FCode.equalsIgnoreCase("BUSSNO")) {
            if( FValue != null && !FValue.equals(""))
            {
                BUSSNO = FValue.trim();
            }
            else
                BUSSNO = null;
        }
        if (FCode.equalsIgnoreCase("STANDBYFLAG1")) {
            if( FValue != null && !FValue.equals(""))
            {
                STANDBYFLAG1 = FValue.trim();
            }
            else
                STANDBYFLAG1 = null;
        }
        if (FCode.equalsIgnoreCase("STANDBYFLAG2")) {
            if( FValue != null && !FValue.equals(""))
            {
                STANDBYFLAG2 = FValue.trim();
            }
            else
                STANDBYFLAG2 = null;
        }
        if (FCode.equalsIgnoreCase("FPFLAG")) {
            if( FValue != null && !FValue.equals(""))
            {
                FPFLAG = FValue.trim();
            }
            else
                FPFLAG = null;
        }
        if (FCode.equalsIgnoreCase("MAKEDATE")) {
            if( FValue != null && !FValue.equals(""))
            {
                MAKEDATE = FValue.trim();
            }
            else
                MAKEDATE = null;
        }
        if (FCode.equalsIgnoreCase("MAKETIME")) {
            if( FValue != null && !FValue.equals(""))
            {
                MAKETIME = FValue.trim();
            }
            else
                MAKETIME = null;
        }
        if (FCode.equalsIgnoreCase("MODIFYDATE")) {
            if( FValue != null && !FValue.equals(""))
            {
                MODIFYDATE = FValue.trim();
            }
            else
                MODIFYDATE = null;
        }
        if (FCode.equalsIgnoreCase("MODIFYTIME")) {
            if( FValue != null && !FValue.equals(""))
            {
                MODIFYTIME = FValue.trim();
            }
            else
                MODIFYTIME = null;
        }
        if (FCode.equalsIgnoreCase("PRODUCT_NAME")) {
            if( FValue != null && !FValue.equals(""))
            {
                PRODUCT_NAME = FValue.trim();
            }
            else
                PRODUCT_NAME = null;
        }
        if (FCode.equalsIgnoreCase("SALECHNL")) {
            if( FValue != null && !FValue.equals(""))
            {
                SALECHNL = FValue.trim();
            }
            else
                SALECHNL = null;
        }
        return true;
    }


    public String toString() {
    return "VMS_OUTPUT_TRANSPojo [" +
            "BUSINESS_ID="+BUSINESS_ID +
            ", GRPCONTNO="+GRPCONTNO +
            ", GRPPOLNO="+GRPPOLNO +
            ", CHERNUM="+CHERNUM +
            ", POLNO="+POLNO +
            ", TTMPRCNO="+TTMPRCNO +
            ", COWNNUM="+COWNNUM +
            ", ORIGCURR="+ORIGCURR +
            ", TAX_RATE="+TAX_RATE +
            ", ORIG_AMT="+ORIG_AMT +
            ", ORIG_INCOME="+ORIG_INCOME +
            ", ORIG_TAX_AMT="+ORIG_TAX_AMT +
            ", AMT_CNY="+AMT_CNY +
            ", INCOME_CNY="+INCOME_CNY +
            ", TAX_AMT_CNY="+TAX_AMT_CNY +
            ", DC_FLAG="+DC_FLAG +
            ", HESITAGE="+HESITAGE +
            ", SERIALNO="+SERIALNO +
            ", TRA_DT="+TRA_DT +
            ", TRA_TYP="+TRA_TYP +
            ", BATCTRCDE="+BATCTRCDE +
            ", TRA_BRA="+TRA_BRA +
            ", INVTYP="+INVTYP +
            ", FEETYP="+FEETYP +
            ", INCOMETYP="+INCOMETYP +
            ", BILLFREQ="+BILLFREQ +
            ", POLYEAR="+POLYEAR +
            ", HISSDTE="+HISSDTE +
            ", DSOURCE="+DSOURCE +
            ", GL_CD_ID="+GL_CD_ID +
            ", GL_CD_GRD="+GL_CD_GRD +
            ", PRODUCT_COD="+PRODUCT_COD +
            ", INVONO="+INVONO +
            ", OTHERNO="+OTHERNO +
            ", INS_COD="+INS_COD +
            ", INSTFROM="+INSTFROM +
            ", INSTTO="+INSTTO +
            ", OCCDATE="+OCCDATE +
            ", PREMTERM="+PREMTERM +
            ", UPLOADFLAG="+UPLOADFLAG +
            ", BUSSNO="+BUSSNO +
            ", STANDBYFLAG1="+STANDBYFLAG1 +
            ", STANDBYFLAG2="+STANDBYFLAG2 +
            ", FPFLAG="+FPFLAG +
            ", MAKEDATE="+MAKEDATE +
            ", MAKETIME="+MAKETIME +
            ", MODIFYDATE="+MODIFYDATE +
            ", MODIFYTIME="+MODIFYTIME +
            ", PRODUCT_NAME="+PRODUCT_NAME +
            ", SALECHNL="+SALECHNL +"]";
    }
}
