/**
 * Copyright (c) 2017 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.entity;

import com.sinosoft.utility.Pojo;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;

import java.io.Serializable;

/**
 * <p>ClassName: LXIDTypeOrNoPojo </p>
 * <p>Copyright: Copyright (c) 2017</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2017-10-17
 */
public class LXIDTypeOrNoPojo implements Pojo,Serializable {
    // @Field
    /** 黑名单id */
    private String BlackListID; 
    /** 证件号码 */
    private String IDNO; 
    /** 证件类型 */
    private String IDType; 
    /** 证件编码（核心） */
    private String IDCode; 
    /** 证件名称（核心） */
    private String IDName; 
    /** 创建者操作员代码 */
    private String Operator; 
    /** 入机日期 */
    private String MakeDate; 
    /** 入机时间 */
    private String MakeTime; 
    /** 修改日期 */
    private String ModifityDate; 
    /** 修改时间 */
    private String ModifyTime; 
    /** Stringbyfalg1 */
    private String Stringbyfalg1; 
    /** Stringbyfalg2 */
    private String Stringbyfalg2; 


    public static final int FIELDNUM = 12;    // 数据库表的字段个数
    public String getBlackListID() {
        return BlackListID;
    }
    public void setBlackListID(String aBlackListID) {
        BlackListID = aBlackListID;
    }
    public String getIDNO() {
        return IDNO;
    }
    public void setIDNO(String aIDNO) {
        IDNO = aIDNO;
    }
    public String getIDType() {
        return IDType;
    }
    public void setIDType(String aIDType) {
        IDType = aIDType;
    }
    public String getIDCode() {
        return IDCode;
    }
    public void setIDCode(String aIDCode) {
        IDCode = aIDCode;
    }
    public String getIDName() {
        return IDName;
    }
    public void setIDName(String aIDName) {
        IDName = aIDName;
    }
    public String getOperator() {
        return Operator;
    }
    public void setOperator(String aOperator) {
        Operator = aOperator;
    }
    public String getMakeDate() {
        return MakeDate;
    }
    public void setMakeDate(String aMakeDate) {
        MakeDate = aMakeDate;
    }
    public String getMakeTime() {
        return MakeTime;
    }
    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }
    public String getModifityDate() {
        return ModifityDate;
    }
    public void setModifityDate(String aModifityDate) {
        ModifityDate = aModifityDate;
    }
    public String getModifyTime() {
        return ModifyTime;
    }
    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }
    public String getStringbyfalg1() {
        return Stringbyfalg1;
    }
    public void setStringbyfalg1(String aStringbyfalg1) {
        Stringbyfalg1 = aStringbyfalg1;
    }
    public String getStringbyfalg2() {
        return Stringbyfalg2;
    }
    public void setStringbyfalg2(String aStringbyfalg2) {
        Stringbyfalg2 = aStringbyfalg2;
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("BlackListID") ) {
            return 0;
        }
        if( strFieldName.equals("IDNO") ) {
            return 1;
        }
        if( strFieldName.equals("IDType") ) {
            return 2;
        }
        if( strFieldName.equals("IDCode") ) {
            return 3;
        }
        if( strFieldName.equals("IDName") ) {
            return 4;
        }
        if( strFieldName.equals("Operator") ) {
            return 5;
        }
        if( strFieldName.equals("MakeDate") ) {
            return 6;
        }
        if( strFieldName.equals("MakeTime") ) {
            return 7;
        }
        if( strFieldName.equals("ModifityDate") ) {
            return 8;
        }
        if( strFieldName.equals("ModifyTime") ) {
            return 9;
        }
        if( strFieldName.equals("Stringbyfalg1") ) {
            return 10;
        }
        if( strFieldName.equals("Stringbyfalg2") ) {
            return 11;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "BlackListID";
                break;
            case 1:
                strFieldName = "IDNO";
                break;
            case 2:
                strFieldName = "IDType";
                break;
            case 3:
                strFieldName = "IDCode";
                break;
            case 4:
                strFieldName = "IDName";
                break;
            case 5:
                strFieldName = "Operator";
                break;
            case 6:
                strFieldName = "MakeDate";
                break;
            case 7:
                strFieldName = "MakeTime";
                break;
            case 8:
                strFieldName = "ModifityDate";
                break;
            case 9:
                strFieldName = "ModifyTime";
                break;
            case 10:
                strFieldName = "Stringbyfalg1";
                break;
            case 11:
                strFieldName = "Stringbyfalg2";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "BLACKLISTID":
                return Schema.TYPE_STRING;
            case "IDNO":
                return Schema.TYPE_STRING;
            case "IDTYPE":
                return Schema.TYPE_STRING;
            case "IDCODE":
                return Schema.TYPE_STRING;
            case "IDNAME":
                return Schema.TYPE_STRING;
            case "OPERATOR":
                return Schema.TYPE_STRING;
            case "MAKEDATE":
                return Schema.TYPE_STRING;
            case "MAKETIME":
                return Schema.TYPE_STRING;
            case "MODIFITYDATE":
                return Schema.TYPE_STRING;
            case "MODIFYTIME":
                return Schema.TYPE_STRING;
            case "STRINGBYFALG1":
                return Schema.TYPE_STRING;
            case "STRINGBYFALG2":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_STRING;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_STRING;
            case 6:
                return Schema.TYPE_STRING;
            case 7:
                return Schema.TYPE_STRING;
            case 8:
                return Schema.TYPE_STRING;
            case 9:
                return Schema.TYPE_STRING;
            case 10:
                return Schema.TYPE_STRING;
            case 11:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("BlackListID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BlackListID));
        }
        if (FCode.equalsIgnoreCase("IDNO")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(IDNO));
        }
        if (FCode.equalsIgnoreCase("IDType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(IDType));
        }
        if (FCode.equalsIgnoreCase("IDCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(IDCode));
        }
        if (FCode.equalsIgnoreCase("IDName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(IDName));
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeDate));
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equalsIgnoreCase("ModifityDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifityDate));
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (FCode.equalsIgnoreCase("Stringbyfalg1")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Stringbyfalg1));
        }
        if (FCode.equalsIgnoreCase("Stringbyfalg2")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Stringbyfalg2));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = String.valueOf(BlackListID);
                break;
            case 1:
                strFieldValue = String.valueOf(IDNO);
                break;
            case 2:
                strFieldValue = String.valueOf(IDType);
                break;
            case 3:
                strFieldValue = String.valueOf(IDCode);
                break;
            case 4:
                strFieldValue = String.valueOf(IDName);
                break;
            case 5:
                strFieldValue = String.valueOf(Operator);
                break;
            case 6:
                strFieldValue = String.valueOf(MakeDate);
                break;
            case 7:
                strFieldValue = String.valueOf(MakeTime);
                break;
            case 8:
                strFieldValue = String.valueOf(ModifityDate);
                break;
            case 9:
                strFieldValue = String.valueOf(ModifyTime);
                break;
            case 10:
                strFieldValue = String.valueOf(Stringbyfalg1);
                break;
            case 11:
                strFieldValue = String.valueOf(Stringbyfalg2);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("BlackListID")) {
            if( FValue != null && !FValue.equals(""))
            {
                BlackListID = FValue.trim();
            }
            else
                BlackListID = null;
        }
        if (FCode.equalsIgnoreCase("IDNO")) {
            if( FValue != null && !FValue.equals(""))
            {
                IDNO = FValue.trim();
            }
            else
                IDNO = null;
        }
        if (FCode.equalsIgnoreCase("IDType")) {
            if( FValue != null && !FValue.equals(""))
            {
                IDType = FValue.trim();
            }
            else
                IDType = null;
        }
        if (FCode.equalsIgnoreCase("IDCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                IDCode = FValue.trim();
            }
            else
                IDCode = null;
        }
        if (FCode.equalsIgnoreCase("IDName")) {
            if( FValue != null && !FValue.equals(""))
            {
                IDName = FValue.trim();
            }
            else
                IDName = null;
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            if( FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
                Operator = null;
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeDate = FValue.trim();
            }
            else
                MakeDate = null;
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
                MakeTime = null;
        }
        if (FCode.equalsIgnoreCase("ModifityDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifityDate = FValue.trim();
            }
            else
                ModifityDate = null;
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
                ModifyTime = null;
        }
        if (FCode.equalsIgnoreCase("Stringbyfalg1")) {
            if( FValue != null && !FValue.equals(""))
            {
                Stringbyfalg1 = FValue.trim();
            }
            else
                Stringbyfalg1 = null;
        }
        if (FCode.equalsIgnoreCase("Stringbyfalg2")) {
            if( FValue != null && !FValue.equals(""))
            {
                Stringbyfalg2 = FValue.trim();
            }
            else
                Stringbyfalg2 = null;
        }
        return true;
    }


    public String toString() {
    return "LXIDTypeOrNoPojo [" +
            "BlackListID="+BlackListID +
            ", IDNO="+IDNO +
            ", IDType="+IDType +
            ", IDCode="+IDCode +
            ", IDName="+IDName +
            ", Operator="+Operator +
            ", MakeDate="+MakeDate +
            ", MakeTime="+MakeTime +
            ", ModifityDate="+ModifityDate +
            ", ModifyTime="+ModifyTime +
            ", Stringbyfalg1="+Stringbyfalg1 +
            ", Stringbyfalg2="+Stringbyfalg2 +"]";
    }
}
