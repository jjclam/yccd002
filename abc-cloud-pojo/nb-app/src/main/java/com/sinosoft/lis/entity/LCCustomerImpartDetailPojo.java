/**
 * Copyright (c) 2017 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.entity;

import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.Pojo;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;

import java.io.Serializable;

/**
 * <p>ClassName: LCCustomerImpartDetailPojo </p>
 * <p>Copyright: Copyright (c) 2017</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2017-10-17
 */
public class LCCustomerImpartDetailPojo implements Pojo,Serializable {
    // @Field
    /** Id */
    private long CustomerImpartDetailID; 
    /** Id2 */
    private long CustomerImpartID; 
    /** Shardingid */
    private String ShardingID; 
    /** 集体合同号码 */
    private String GrpContNo; 
    /** 合同号码 */
    private String ContNo; 
    /** 总单投保单号码 */
    private String ProposalContNo; 
    /** 印刷号码 */
    private String PrtNo; 
    /** 告知编码 */
    private String ImpartCode; 
    /** 告知版别 */
    private String ImpartVer; 
    /** 内部流水号 */
    private String SubSerialNo; 
    /** 告知内容 */
    private String ImpartDetailContent; 
    /** 疾病内容 */
    private String DiseaseContent; 
    /** 开始时间 */
    private String  StartDate;
    /** 结束时间 */
    private String  EndDate;
    /** 证明人 */
    private String Prover; 
    /** 目前状况 */
    private String CurrCondition; 
    /** 能否证明 */
    private String IsProved; 
    /** 客户号码 */
    private String CustomerNo; 
    /** 客户号码类型 */
    private String CustomerNoType; 
    /** 是否参与核保核赔标志 */
    private String UWClaimFlg; 
    /** 打印标志 */
    private String PrtFlag; 
    /** 操作员 */
    private String Operator; 
    /** 入机日期 */
    private String  MakeDate;
    /** 入机时间 */
    private String MakeTime; 
    /** 最后一次修改日期 */
    private String  ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime; 
    /** 批次号 */
    private int PatchNo; 


    public static final int FIELDNUM = 27;    // 数据库表的字段个数

    private FDate fDate = new FDate();        // 处理日期
    public long getCustomerImpartDetailID() {
        return CustomerImpartDetailID;
    }
    public void setCustomerImpartDetailID(long aCustomerImpartDetailID) {
        CustomerImpartDetailID = aCustomerImpartDetailID;
    }
    public void setCustomerImpartDetailID(String aCustomerImpartDetailID) {
        if (aCustomerImpartDetailID != null && !aCustomerImpartDetailID.equals("")) {
            CustomerImpartDetailID = new Long(aCustomerImpartDetailID).longValue();
        }
    }

    public long getCustomerImpartID() {
        return CustomerImpartID;
    }
    public void setCustomerImpartID(long aCustomerImpartID) {
        CustomerImpartID = aCustomerImpartID;
    }
    public void setCustomerImpartID(String aCustomerImpartID) {
        if (aCustomerImpartID != null && !aCustomerImpartID.equals("")) {
            CustomerImpartID = new Long(aCustomerImpartID).longValue();
        }
    }

    public String getShardingID() {
        return ShardingID;
    }
    public void setShardingID(String aShardingID) {
        ShardingID = aShardingID;
    }
    public String getGrpContNo() {
        return GrpContNo;
    }
    public void setGrpContNo(String aGrpContNo) {
        GrpContNo = aGrpContNo;
    }
    public String getContNo() {
        return ContNo;
    }
    public void setContNo(String aContNo) {
        ContNo = aContNo;
    }
    public String getProposalContNo() {
        return ProposalContNo;
    }
    public void setProposalContNo(String aProposalContNo) {
        ProposalContNo = aProposalContNo;
    }
    public String getPrtNo() {
        return PrtNo;
    }
    public void setPrtNo(String aPrtNo) {
        PrtNo = aPrtNo;
    }
    public String getImpartCode() {
        return ImpartCode;
    }
    public void setImpartCode(String aImpartCode) {
        ImpartCode = aImpartCode;
    }
    public String getImpartVer() {
        return ImpartVer;
    }
    public void setImpartVer(String aImpartVer) {
        ImpartVer = aImpartVer;
    }
    public String getSubSerialNo() {
        return SubSerialNo;
    }
    public void setSubSerialNo(String aSubSerialNo) {
        SubSerialNo = aSubSerialNo;
    }
    public String getImpartDetailContent() {
        return ImpartDetailContent;
    }
    public void setImpartDetailContent(String aImpartDetailContent) {
        ImpartDetailContent = aImpartDetailContent;
    }
    public String getDiseaseContent() {
        return DiseaseContent;
    }
    public void setDiseaseContent(String aDiseaseContent) {
        DiseaseContent = aDiseaseContent;
    }
    public String getStartDate() {
        return StartDate;
    }
    public void setStartDate(String aStartDate) {
        StartDate = aStartDate;
    }
    public String getEndDate() {
        return EndDate;
    }
    public void setEndDate(String aEndDate) {
        EndDate = aEndDate;
    }
    public String getProver() {
        return Prover;
    }
    public void setProver(String aProver) {
        Prover = aProver;
    }
    public String getCurrCondition() {
        return CurrCondition;
    }
    public void setCurrCondition(String aCurrCondition) {
        CurrCondition = aCurrCondition;
    }
    public String getIsProved() {
        return IsProved;
    }
    public void setIsProved(String aIsProved) {
        IsProved = aIsProved;
    }
    public String getCustomerNo() {
        return CustomerNo;
    }
    public void setCustomerNo(String aCustomerNo) {
        CustomerNo = aCustomerNo;
    }
    public String getCustomerNoType() {
        return CustomerNoType;
    }
    public void setCustomerNoType(String aCustomerNoType) {
        CustomerNoType = aCustomerNoType;
    }
    public String getUWClaimFlg() {
        return UWClaimFlg;
    }
    public void setUWClaimFlg(String aUWClaimFlg) {
        UWClaimFlg = aUWClaimFlg;
    }
    public String getPrtFlag() {
        return PrtFlag;
    }
    public void setPrtFlag(String aPrtFlag) {
        PrtFlag = aPrtFlag;
    }
    public String getOperator() {
        return Operator;
    }
    public void setOperator(String aOperator) {
        Operator = aOperator;
    }
    public String getMakeDate() {
        return MakeDate;
    }
    public void setMakeDate(String aMakeDate) {
        MakeDate = aMakeDate;
    }
    public String getMakeTime() {
        return MakeTime;
    }
    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }
    public String getModifyDate() {
        return ModifyDate;
    }
    public void setModifyDate(String aModifyDate) {
        ModifyDate = aModifyDate;
    }
    public String getModifyTime() {
        return ModifyTime;
    }
    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }
    public int getPatchNo() {
        return PatchNo;
    }
    public void setPatchNo(int aPatchNo) {
        PatchNo = aPatchNo;
    }
    public void setPatchNo(String aPatchNo) {
        if (aPatchNo != null && !aPatchNo.equals("")) {
            Integer tInteger = new Integer(aPatchNo);
            int i = tInteger.intValue();
            PatchNo = i;
        }
    }


    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("CustomerImpartDetailID") ) {
            return 0;
        }
        if( strFieldName.equals("CustomerImpartID") ) {
            return 1;
        }
        if( strFieldName.equals("ShardingID") ) {
            return 2;
        }
        if( strFieldName.equals("GrpContNo") ) {
            return 3;
        }
        if( strFieldName.equals("ContNo") ) {
            return 4;
        }
        if( strFieldName.equals("ProposalContNo") ) {
            return 5;
        }
        if( strFieldName.equals("PrtNo") ) {
            return 6;
        }
        if( strFieldName.equals("ImpartCode") ) {
            return 7;
        }
        if( strFieldName.equals("ImpartVer") ) {
            return 8;
        }
        if( strFieldName.equals("SubSerialNo") ) {
            return 9;
        }
        if( strFieldName.equals("ImpartDetailContent") ) {
            return 10;
        }
        if( strFieldName.equals("DiseaseContent") ) {
            return 11;
        }
        if( strFieldName.equals("StartDate") ) {
            return 12;
        }
        if( strFieldName.equals("EndDate") ) {
            return 13;
        }
        if( strFieldName.equals("Prover") ) {
            return 14;
        }
        if( strFieldName.equals("CurrCondition") ) {
            return 15;
        }
        if( strFieldName.equals("IsProved") ) {
            return 16;
        }
        if( strFieldName.equals("CustomerNo") ) {
            return 17;
        }
        if( strFieldName.equals("CustomerNoType") ) {
            return 18;
        }
        if( strFieldName.equals("UWClaimFlg") ) {
            return 19;
        }
        if( strFieldName.equals("PrtFlag") ) {
            return 20;
        }
        if( strFieldName.equals("Operator") ) {
            return 21;
        }
        if( strFieldName.equals("MakeDate") ) {
            return 22;
        }
        if( strFieldName.equals("MakeTime") ) {
            return 23;
        }
        if( strFieldName.equals("ModifyDate") ) {
            return 24;
        }
        if( strFieldName.equals("ModifyTime") ) {
            return 25;
        }
        if( strFieldName.equals("PatchNo") ) {
            return 26;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "CustomerImpartDetailID";
                break;
            case 1:
                strFieldName = "CustomerImpartID";
                break;
            case 2:
                strFieldName = "ShardingID";
                break;
            case 3:
                strFieldName = "GrpContNo";
                break;
            case 4:
                strFieldName = "ContNo";
                break;
            case 5:
                strFieldName = "ProposalContNo";
                break;
            case 6:
                strFieldName = "PrtNo";
                break;
            case 7:
                strFieldName = "ImpartCode";
                break;
            case 8:
                strFieldName = "ImpartVer";
                break;
            case 9:
                strFieldName = "SubSerialNo";
                break;
            case 10:
                strFieldName = "ImpartDetailContent";
                break;
            case 11:
                strFieldName = "DiseaseContent";
                break;
            case 12:
                strFieldName = "StartDate";
                break;
            case 13:
                strFieldName = "EndDate";
                break;
            case 14:
                strFieldName = "Prover";
                break;
            case 15:
                strFieldName = "CurrCondition";
                break;
            case 16:
                strFieldName = "IsProved";
                break;
            case 17:
                strFieldName = "CustomerNo";
                break;
            case 18:
                strFieldName = "CustomerNoType";
                break;
            case 19:
                strFieldName = "UWClaimFlg";
                break;
            case 20:
                strFieldName = "PrtFlag";
                break;
            case 21:
                strFieldName = "Operator";
                break;
            case 22:
                strFieldName = "MakeDate";
                break;
            case 23:
                strFieldName = "MakeTime";
                break;
            case 24:
                strFieldName = "ModifyDate";
                break;
            case 25:
                strFieldName = "ModifyTime";
                break;
            case 26:
                strFieldName = "PatchNo";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "CUSTOMERIMPARTDETAILID":
                return Schema.TYPE_LONG;
            case "CUSTOMERIMPARTID":
                return Schema.TYPE_LONG;
            case "SHARDINGID":
                return Schema.TYPE_STRING;
            case "GRPCONTNO":
                return Schema.TYPE_STRING;
            case "CONTNO":
                return Schema.TYPE_STRING;
            case "PROPOSALCONTNO":
                return Schema.TYPE_STRING;
            case "PRTNO":
                return Schema.TYPE_STRING;
            case "IMPARTCODE":
                return Schema.TYPE_STRING;
            case "IMPARTVER":
                return Schema.TYPE_STRING;
            case "SUBSERIALNO":
                return Schema.TYPE_STRING;
            case "IMPARTDETAILCONTENT":
                return Schema.TYPE_STRING;
            case "DISEASECONTENT":
                return Schema.TYPE_STRING;
            case "STARTDATE":
                return Schema.TYPE_STRING;
            case "ENDDATE":
                return Schema.TYPE_STRING;
            case "PROVER":
                return Schema.TYPE_STRING;
            case "CURRCONDITION":
                return Schema.TYPE_STRING;
            case "ISPROVED":
                return Schema.TYPE_STRING;
            case "CUSTOMERNO":
                return Schema.TYPE_STRING;
            case "CUSTOMERNOTYPE":
                return Schema.TYPE_STRING;
            case "UWCLAIMFLG":
                return Schema.TYPE_STRING;
            case "PRTFLAG":
                return Schema.TYPE_STRING;
            case "OPERATOR":
                return Schema.TYPE_STRING;
            case "MAKEDATE":
                return Schema.TYPE_STRING;
            case "MAKETIME":
                return Schema.TYPE_STRING;
            case "MODIFYDATE":
                return Schema.TYPE_STRING;
            case "MODIFYTIME":
                return Schema.TYPE_STRING;
            case "PATCHNO":
                return Schema.TYPE_INT;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_LONG;
            case 1:
                return Schema.TYPE_LONG;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_STRING;
            case 6:
                return Schema.TYPE_STRING;
            case 7:
                return Schema.TYPE_STRING;
            case 8:
                return Schema.TYPE_STRING;
            case 9:
                return Schema.TYPE_STRING;
            case 10:
                return Schema.TYPE_STRING;
            case 11:
                return Schema.TYPE_STRING;
            case 12:
                return Schema.TYPE_STRING;
            case 13:
                return Schema.TYPE_STRING;
            case 14:
                return Schema.TYPE_STRING;
            case 15:
                return Schema.TYPE_STRING;
            case 16:
                return Schema.TYPE_STRING;
            case 17:
                return Schema.TYPE_STRING;
            case 18:
                return Schema.TYPE_STRING;
            case 19:
                return Schema.TYPE_STRING;
            case 20:
                return Schema.TYPE_STRING;
            case 21:
                return Schema.TYPE_STRING;
            case 22:
                return Schema.TYPE_STRING;
            case 23:
                return Schema.TYPE_STRING;
            case 24:
                return Schema.TYPE_STRING;
            case 25:
                return Schema.TYPE_STRING;
            case 26:
                return Schema.TYPE_INT;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("CustomerImpartDetailID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CustomerImpartDetailID));
        }
        if (FCode.equalsIgnoreCase("CustomerImpartID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CustomerImpartID));
        }
        if (FCode.equalsIgnoreCase("ShardingID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ShardingID));
        }
        if (FCode.equalsIgnoreCase("GrpContNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GrpContNo));
        }
        if (FCode.equalsIgnoreCase("ContNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ContNo));
        }
        if (FCode.equalsIgnoreCase("ProposalContNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ProposalContNo));
        }
        if (FCode.equalsIgnoreCase("PrtNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PrtNo));
        }
        if (FCode.equalsIgnoreCase("ImpartCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ImpartCode));
        }
        if (FCode.equalsIgnoreCase("ImpartVer")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ImpartVer));
        }
        if (FCode.equalsIgnoreCase("SubSerialNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SubSerialNo));
        }
        if (FCode.equalsIgnoreCase("ImpartDetailContent")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ImpartDetailContent));
        }
        if (FCode.equalsIgnoreCase("DiseaseContent")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DiseaseContent));
        }
        if (FCode.equalsIgnoreCase("StartDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StartDate));
        }
        if (FCode.equalsIgnoreCase("EndDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(EndDate));
        }
        if (FCode.equalsIgnoreCase("Prover")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Prover));
        }
        if (FCode.equalsIgnoreCase("CurrCondition")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CurrCondition));
        }
        if (FCode.equalsIgnoreCase("IsProved")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(IsProved));
        }
        if (FCode.equalsIgnoreCase("CustomerNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CustomerNo));
        }
        if (FCode.equalsIgnoreCase("CustomerNoType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CustomerNoType));
        }
        if (FCode.equalsIgnoreCase("UWClaimFlg")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(UWClaimFlg));
        }
        if (FCode.equalsIgnoreCase("PrtFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PrtFlag));
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeDate));
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyDate));
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (FCode.equalsIgnoreCase("PatchNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PatchNo));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = String.valueOf(CustomerImpartDetailID);
                break;
            case 1:
                strFieldValue = String.valueOf(CustomerImpartID);
                break;
            case 2:
                strFieldValue = String.valueOf(ShardingID);
                break;
            case 3:
                strFieldValue = String.valueOf(GrpContNo);
                break;
            case 4:
                strFieldValue = String.valueOf(ContNo);
                break;
            case 5:
                strFieldValue = String.valueOf(ProposalContNo);
                break;
            case 6:
                strFieldValue = String.valueOf(PrtNo);
                break;
            case 7:
                strFieldValue = String.valueOf(ImpartCode);
                break;
            case 8:
                strFieldValue = String.valueOf(ImpartVer);
                break;
            case 9:
                strFieldValue = String.valueOf(SubSerialNo);
                break;
            case 10:
                strFieldValue = String.valueOf(ImpartDetailContent);
                break;
            case 11:
                strFieldValue = String.valueOf(DiseaseContent);
                break;
            case 12:
                strFieldValue = String.valueOf(StartDate);
                break;
            case 13:
                strFieldValue = String.valueOf(EndDate);
                break;
            case 14:
                strFieldValue = String.valueOf(Prover);
                break;
            case 15:
                strFieldValue = String.valueOf(CurrCondition);
                break;
            case 16:
                strFieldValue = String.valueOf(IsProved);
                break;
            case 17:
                strFieldValue = String.valueOf(CustomerNo);
                break;
            case 18:
                strFieldValue = String.valueOf(CustomerNoType);
                break;
            case 19:
                strFieldValue = String.valueOf(UWClaimFlg);
                break;
            case 20:
                strFieldValue = String.valueOf(PrtFlag);
                break;
            case 21:
                strFieldValue = String.valueOf(Operator);
                break;
            case 22:
                strFieldValue = String.valueOf(MakeDate);
                break;
            case 23:
                strFieldValue = String.valueOf(MakeTime);
                break;
            case 24:
                strFieldValue = String.valueOf(ModifyDate);
                break;
            case 25:
                strFieldValue = String.valueOf(ModifyTime);
                break;
            case 26:
                strFieldValue = String.valueOf(PatchNo);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("CustomerImpartDetailID")) {
            if( FValue != null && !FValue.equals("")) {
                CustomerImpartDetailID = new Long(FValue).longValue();
            }
        }
        if (FCode.equalsIgnoreCase("CustomerImpartID")) {
            if( FValue != null && !FValue.equals("")) {
                CustomerImpartID = new Long(FValue).longValue();
            }
        }
        if (FCode.equalsIgnoreCase("ShardingID")) {
            if( FValue != null && !FValue.equals(""))
            {
                ShardingID = FValue.trim();
            }
            else
                ShardingID = null;
        }
        if (FCode.equalsIgnoreCase("GrpContNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                GrpContNo = FValue.trim();
            }
            else
                GrpContNo = null;
        }
        if (FCode.equalsIgnoreCase("ContNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                ContNo = FValue.trim();
            }
            else
                ContNo = null;
        }
        if (FCode.equalsIgnoreCase("ProposalContNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                ProposalContNo = FValue.trim();
            }
            else
                ProposalContNo = null;
        }
        if (FCode.equalsIgnoreCase("PrtNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                PrtNo = FValue.trim();
            }
            else
                PrtNo = null;
        }
        if (FCode.equalsIgnoreCase("ImpartCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                ImpartCode = FValue.trim();
            }
            else
                ImpartCode = null;
        }
        if (FCode.equalsIgnoreCase("ImpartVer")) {
            if( FValue != null && !FValue.equals(""))
            {
                ImpartVer = FValue.trim();
            }
            else
                ImpartVer = null;
        }
        if (FCode.equalsIgnoreCase("SubSerialNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                SubSerialNo = FValue.trim();
            }
            else
                SubSerialNo = null;
        }
        if (FCode.equalsIgnoreCase("ImpartDetailContent")) {
            if( FValue != null && !FValue.equals(""))
            {
                ImpartDetailContent = FValue.trim();
            }
            else
                ImpartDetailContent = null;
        }
        if (FCode.equalsIgnoreCase("DiseaseContent")) {
            if( FValue != null && !FValue.equals(""))
            {
                DiseaseContent = FValue.trim();
            }
            else
                DiseaseContent = null;
        }
        if (FCode.equalsIgnoreCase("StartDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                StartDate = FValue.trim();
            }
            else
                StartDate = null;
        }
        if (FCode.equalsIgnoreCase("EndDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                EndDate = FValue.trim();
            }
            else
                EndDate = null;
        }
        if (FCode.equalsIgnoreCase("Prover")) {
            if( FValue != null && !FValue.equals(""))
            {
                Prover = FValue.trim();
            }
            else
                Prover = null;
        }
        if (FCode.equalsIgnoreCase("CurrCondition")) {
            if( FValue != null && !FValue.equals(""))
            {
                CurrCondition = FValue.trim();
            }
            else
                CurrCondition = null;
        }
        if (FCode.equalsIgnoreCase("IsProved")) {
            if( FValue != null && !FValue.equals(""))
            {
                IsProved = FValue.trim();
            }
            else
                IsProved = null;
        }
        if (FCode.equalsIgnoreCase("CustomerNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                CustomerNo = FValue.trim();
            }
            else
                CustomerNo = null;
        }
        if (FCode.equalsIgnoreCase("CustomerNoType")) {
            if( FValue != null && !FValue.equals(""))
            {
                CustomerNoType = FValue.trim();
            }
            else
                CustomerNoType = null;
        }
        if (FCode.equalsIgnoreCase("UWClaimFlg")) {
            if( FValue != null && !FValue.equals(""))
            {
                UWClaimFlg = FValue.trim();
            }
            else
                UWClaimFlg = null;
        }
        if (FCode.equalsIgnoreCase("PrtFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                PrtFlag = FValue.trim();
            }
            else
                PrtFlag = null;
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            if( FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
                Operator = null;
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeDate = FValue.trim();
            }
            else
                MakeDate = null;
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
                MakeTime = null;
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyDate = FValue.trim();
            }
            else
                ModifyDate = null;
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
                ModifyTime = null;
        }
        if (FCode.equalsIgnoreCase("PatchNo")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                PatchNo = i;
            }
        }
        return true;
    }


    public String toString() {
    return "LCCustomerImpartDetailPojo [" +
            "CustomerImpartDetailID="+CustomerImpartDetailID +
            ", CustomerImpartID="+CustomerImpartID +
            ", ShardingID="+ShardingID +
            ", GrpContNo="+GrpContNo +
            ", ContNo="+ContNo +
            ", ProposalContNo="+ProposalContNo +
            ", PrtNo="+PrtNo +
            ", ImpartCode="+ImpartCode +
            ", ImpartVer="+ImpartVer +
            ", SubSerialNo="+SubSerialNo +
            ", ImpartDetailContent="+ImpartDetailContent +
            ", DiseaseContent="+DiseaseContent +
            ", StartDate="+StartDate +
            ", EndDate="+EndDate +
            ", Prover="+Prover +
            ", CurrCondition="+CurrCondition +
            ", IsProved="+IsProved +
            ", CustomerNo="+CustomerNo +
            ", CustomerNoType="+CustomerNoType +
            ", UWClaimFlg="+UWClaimFlg +
            ", PrtFlag="+PrtFlag +
            ", Operator="+Operator +
            ", MakeDate="+MakeDate +
            ", MakeTime="+MakeTime +
            ", ModifyDate="+ModifyDate +
            ", ModifyTime="+ModifyTime +
            ", PatchNo="+PatchNo +"]";
    }
}
