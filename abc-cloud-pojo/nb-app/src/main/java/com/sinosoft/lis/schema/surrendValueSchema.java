/**
 * Copyright (c) 2020 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.surrendValueDB;

/**
 * <p>ClassName: surrendValueSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2020</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2020-01-14
 */
public class surrendValueSchema implements Schema, Cloneable {
    // @Field
    /** 请求报文id */
    private String reqMsgId;
    /** 机构流水号 */
    private String instSerialNo;
    /** 渠道编码 */
    private String sellType;
    /** 渠道退保订单号 */
    private String endorseNo;
    /** 渠道退保申请时间 */
    private Date endorseCreateTime;
    /** 渠道退保时间 */
    private Date endorseTime;
    /** 退保原因类型 */
    private String endorseReason;
    /** 退保原因描述 */
    private String endorseReasonDesc;
    /** 退保金额 */
    private String endorseFee;
    /** 保单号 */
    private String contNo;
    /** 渠道保单号 */
    private String policyNo;
    /** 渠道产品编号 */
    private String productCode;
    /** 产品编号 */
    private String riskCode;
    /** 退保性质 */
    private String surrenderReason;
    /** 退保犹豫期类型 */
    private String hesitationType;
    /** 收入账户 */
    private String inAccount;
    /** 支出账号 */
    private String outAccount;
    /** 支付发生时间 */
    private Date payTime;
    /** 发生金额 */
    private String fee;
    /** 支付流水号 */
    private String payFlowId;
    /** 优惠金额 */
    private String discountFee;

    public static final int FIELDNUM = 21;    // 数据库表的字段个数

    private static String[] PK;                // 主键

    private FDate fDate = new FDate();        // 处理日期

    public CErrors mErrors;            // 错误信息

    // @Constructor
    public surrendValueSchema() {
        mErrors = new CErrors();

        String[] pk = new String[0];

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException {
        surrendValueSchema cloned = (surrendValueSchema)super.clone();
        cloned.fDate = (FDate) fDate.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK() {
        return PK;
    }

    public String getReqMsgId() {
        return reqMsgId;
    }
    public void setReqMsgId(String areqMsgId) {
        reqMsgId = areqMsgId;
    }
    public String getInstSerialNo() {
        return instSerialNo;
    }
    public void setInstSerialNo(String ainstSerialNo) {
        instSerialNo = ainstSerialNo;
    }
    public String getSellType() {
        return sellType;
    }
    public void setSellType(String asellType) {
        sellType = asellType;
    }
    public String getEndorseNo() {
        return endorseNo;
    }
    public void setEndorseNo(String aendorseNo) {
        endorseNo = aendorseNo;
    }
    public String getEndorseCreateTime() {
        if(endorseCreateTime != null) {
            return fDate.getString(endorseCreateTime);
        } else {
            return null;
        }
    }
    public void setEndorseCreateTime(Date aendorseCreateTime) {
        endorseCreateTime = aendorseCreateTime;
    }
    public void setEndorseCreateTime(String aendorseCreateTime) {
        if (aendorseCreateTime != null && !aendorseCreateTime.equals("")) {
            endorseCreateTime = fDate.getDate(aendorseCreateTime);
        } else
            endorseCreateTime = null;
    }

    public String getEndorseTime() {
        if(endorseTime != null) {
            return fDate.getString(endorseTime);
        } else {
            return null;
        }
    }
    public void setEndorseTime(Date aendorseTime) {
        endorseTime = aendorseTime;
    }
    public void setEndorseTime(String aendorseTime) {
        if (aendorseTime != null && !aendorseTime.equals("")) {
            endorseTime = fDate.getDate(aendorseTime);
        } else
            endorseTime = null;
    }

    public String getEndorseReason() {
        return endorseReason;
    }
    public void setEndorseReason(String aendorseReason) {
        endorseReason = aendorseReason;
    }
    public String getEndorseReasonDesc() {
        return endorseReasonDesc;
    }
    public void setEndorseReasonDesc(String aendorseReasonDesc) {
        endorseReasonDesc = aendorseReasonDesc;
    }
    public String getEndorseFee() {
        return endorseFee;
    }
    public void setEndorseFee(String aendorseFee) {
        endorseFee = aendorseFee;
    }
    public String getContNo() {
        return contNo;
    }
    public void setContNo(String acontNo) {
        contNo = acontNo;
    }
    public String getPolicyNo() {
        return policyNo;
    }
    public void setPolicyNo(String apolicyNo) {
        policyNo = apolicyNo;
    }
    public String getProductCode() {
        return productCode;
    }
    public void setProductCode(String aproductCode) {
        productCode = aproductCode;
    }
    public String getRiskCode() {
        return riskCode;
    }
    public void setRiskCode(String ariskCode) {
        riskCode = ariskCode;
    }
    public String getSurrenderReason() {
        return surrenderReason;
    }
    public void setSurrenderReason(String asurrenderReason) {
        surrenderReason = asurrenderReason;
    }
    public String getHesitationType() {
        return hesitationType;
    }
    public void setHesitationType(String ahesitationType) {
        hesitationType = ahesitationType;
    }
    public String getInAccount() {
        return inAccount;
    }
    public void setInAccount(String ainAccount) {
        inAccount = ainAccount;
    }
    public String getOutAccount() {
        return outAccount;
    }
    public void setOutAccount(String aoutAccount) {
        outAccount = aoutAccount;
    }
    public String getPayTime() {
        if(payTime != null) {
            return fDate.getString(payTime);
        } else {
            return null;
        }
    }
    public void setPayTime(Date apayTime) {
        payTime = apayTime;
    }
    public void setPayTime(String apayTime) {
        if (apayTime != null && !apayTime.equals("")) {
            payTime = fDate.getDate(apayTime);
        } else
            payTime = null;
    }

    public String getFee() {
        return fee;
    }
    public void setFee(String afee) {
        fee = afee;
    }
    public String getPayFlowId() {
        return payFlowId;
    }
    public void setPayFlowId(String apayFlowId) {
        payFlowId = apayFlowId;
    }
    public String getDiscountFee() {
        return discountFee;
    }
    public void setDiscountFee(String adiscountFee) {
        discountFee = adiscountFee;
    }

    /**
    * 使用另外一个 surrendValueSchema 对象给 Schema 赋值
    * @param: asurrendValueSchema surrendValueSchema
    **/
    public void setSchema(surrendValueSchema asurrendValueSchema) {
        this.reqMsgId = asurrendValueSchema.getReqMsgId();
        this.instSerialNo = asurrendValueSchema.getInstSerialNo();
        this.sellType = asurrendValueSchema.getSellType();
        this.endorseNo = asurrendValueSchema.getEndorseNo();
        this.endorseCreateTime = fDate.getDate( asurrendValueSchema.getEndorseCreateTime());
        this.endorseTime = fDate.getDate( asurrendValueSchema.getEndorseTime());
        this.endorseReason = asurrendValueSchema.getEndorseReason();
        this.endorseReasonDesc = asurrendValueSchema.getEndorseReasonDesc();
        this.endorseFee = asurrendValueSchema.getEndorseFee();
        this.contNo = asurrendValueSchema.getContNo();
        this.policyNo = asurrendValueSchema.getPolicyNo();
        this.productCode = asurrendValueSchema.getProductCode();
        this.riskCode = asurrendValueSchema.getRiskCode();
        this.surrenderReason = asurrendValueSchema.getSurrenderReason();
        this.hesitationType = asurrendValueSchema.getHesitationType();
        this.inAccount = asurrendValueSchema.getInAccount();
        this.outAccount = asurrendValueSchema.getOutAccount();
        this.payTime = fDate.getDate( asurrendValueSchema.getPayTime());
        this.fee = asurrendValueSchema.getFee();
        this.payFlowId = asurrendValueSchema.getPayFlowId();
        this.discountFee = asurrendValueSchema.getDiscountFee();
    }

    /**
    * 使用 ResultSet 中的第 i 行给 Schema 赋值
    * @param: rs ResultSet
    * @param: i int
    * @return: boolean
    **/
    public boolean setSchema(ResultSet rs,int i) {
        try {
            //rs.absolute(i);        // 非滚动游标
            if( rs.getString("reqMsgId") == null )
                this.reqMsgId = null;
            else
                this.reqMsgId = rs.getString("reqMsgId").trim();

            if( rs.getString("instSerialNo") == null )
                this.instSerialNo = null;
            else
                this.instSerialNo = rs.getString("instSerialNo").trim();

            if( rs.getString("sellType") == null )
                this.sellType = null;
            else
                this.sellType = rs.getString("sellType").trim();

            if( rs.getString("endorseNo") == null )
                this.endorseNo = null;
            else
                this.endorseNo = rs.getString("endorseNo").trim();

            this.endorseCreateTime = rs.getDate("endorseCreateTime");
            this.endorseTime = rs.getDate("endorseTime");
            if( rs.getString("endorseReason") == null )
                this.endorseReason = null;
            else
                this.endorseReason = rs.getString("endorseReason").trim();

            if( rs.getString("endorseReasonDesc") == null )
                this.endorseReasonDesc = null;
            else
                this.endorseReasonDesc = rs.getString("endorseReasonDesc").trim();

            if( rs.getString("endorseFee") == null )
                this.endorseFee = null;
            else
                this.endorseFee = rs.getString("endorseFee").trim();

            if( rs.getString("contNo") == null )
                this.contNo = null;
            else
                this.contNo = rs.getString("contNo").trim();

            if( rs.getString("policyNo") == null )
                this.policyNo = null;
            else
                this.policyNo = rs.getString("policyNo").trim();

            if( rs.getString("productCode") == null )
                this.productCode = null;
            else
                this.productCode = rs.getString("productCode").trim();

            if( rs.getString("riskCode") == null )
                this.riskCode = null;
            else
                this.riskCode = rs.getString("riskCode").trim();

            if( rs.getString("surrenderReason") == null )
                this.surrenderReason = null;
            else
                this.surrenderReason = rs.getString("surrenderReason").trim();

            if( rs.getString("hesitationType") == null )
                this.hesitationType = null;
            else
                this.hesitationType = rs.getString("hesitationType").trim();

            if( rs.getString("inAccount") == null )
                this.inAccount = null;
            else
                this.inAccount = rs.getString("inAccount").trim();

            if( rs.getString("outAccount") == null )
                this.outAccount = null;
            else
                this.outAccount = rs.getString("outAccount").trim();

            this.payTime = rs.getDate("payTime");
            if( rs.getString("fee") == null )
                this.fee = null;
            else
                this.fee = rs.getString("fee").trim();

            if( rs.getString("payFlowId") == null )
                this.payFlowId = null;
            else
                this.payFlowId = rs.getString("payFlowId").trim();

            if( rs.getString("discountFee") == null )
                this.discountFee = null;
            else
                this.discountFee = rs.getString("discountFee").trim();

        }
        catch(SQLException sqle) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "surrendValueSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = ExceptionUtils.exceptionToString(sqle);
            this.mErrors .addOneError(tError);
            return false;
        }
        return true;
    }

    public surrendValueSchema getSchema() {
        surrendValueSchema asurrendValueSchema = new surrendValueSchema();
        asurrendValueSchema.setSchema(this);
        return asurrendValueSchema;
    }

    public surrendValueDB getDB() {
        surrendValueDB aDBOper = new surrendValueDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
    * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpsurrendValue描述/A>表字段
    * @return: String 返回打包后字符串
    **/
    public String encode() {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(reqMsgId)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(instSerialNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(sellType)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(endorseNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( endorseCreateTime ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( endorseTime ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(endorseReason)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(endorseReasonDesc)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(endorseFee)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(contNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(policyNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(productCode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(riskCode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(surrenderReason)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(hesitationType)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(inAccount)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(outAccount)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( payTime ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fee)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(payFlowId)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(discountFee));
        return strReturn.toString();
    }

    /**
    * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpsurrendValue>历史记账凭证主表信息</A>表字段
    * @param: strMessage String 包含一条纪录数据的字符串
    * @return: boolean
    **/
    public boolean decode(String strMessage) {
        try {
            reqMsgId = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
            instSerialNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
            sellType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
            endorseNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
            endorseCreateTime = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,SysConst.PACKAGESPILTER));
            endorseTime = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,SysConst.PACKAGESPILTER));
            endorseReason = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
            endorseReasonDesc = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
            endorseFee = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
            contNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
            policyNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
            productCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
            riskCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
            surrenderReason = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER );
            hesitationType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15, SysConst.PACKAGESPILTER );
            inAccount = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16, SysConst.PACKAGESPILTER );
            outAccount = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17, SysConst.PACKAGESPILTER );
            payTime = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18,SysConst.PACKAGESPILTER));
            fee = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19, SysConst.PACKAGESPILTER );
            payFlowId = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20, SysConst.PACKAGESPILTER );
            discountFee = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 21, SysConst.PACKAGESPILTER );
        }
        catch(NumberFormatException ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "surrendValueSchema";
            tError.functionName = "decode";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            return false;
        }
        return true;
    }

    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("reqMsgId")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(reqMsgId));
        }
        if (FCode.equalsIgnoreCase("instSerialNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(instSerialNo));
        }
        if (FCode.equalsIgnoreCase("sellType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(sellType));
        }
        if (FCode.equalsIgnoreCase("endorseNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(endorseNo));
        }
        if (FCode.equalsIgnoreCase("endorseCreateTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getEndorseCreateTime()));
        }
        if (FCode.equalsIgnoreCase("endorseTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getEndorseTime()));
        }
        if (FCode.equalsIgnoreCase("endorseReason")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(endorseReason));
        }
        if (FCode.equalsIgnoreCase("endorseReasonDesc")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(endorseReasonDesc));
        }
        if (FCode.equalsIgnoreCase("endorseFee")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(endorseFee));
        }
        if (FCode.equalsIgnoreCase("contNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(contNo));
        }
        if (FCode.equalsIgnoreCase("policyNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(policyNo));
        }
        if (FCode.equalsIgnoreCase("productCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(productCode));
        }
        if (FCode.equalsIgnoreCase("riskCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(riskCode));
        }
        if (FCode.equalsIgnoreCase("surrenderReason")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(surrenderReason));
        }
        if (FCode.equalsIgnoreCase("hesitationType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(hesitationType));
        }
        if (FCode.equalsIgnoreCase("inAccount")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(inAccount));
        }
        if (FCode.equalsIgnoreCase("outAccount")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(outAccount));
        }
        if (FCode.equalsIgnoreCase("payTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getPayTime()));
        }
        if (FCode.equalsIgnoreCase("fee")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(fee));
        }
        if (FCode.equalsIgnoreCase("payFlowId")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(payFlowId));
        }
        if (FCode.equalsIgnoreCase("discountFee")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(discountFee));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(reqMsgId);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(instSerialNo);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(sellType);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(endorseNo);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getEndorseCreateTime()));
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getEndorseTime()));
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(endorseReason);
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(endorseReasonDesc);
                break;
            case 8:
                strFieldValue = StrTool.GBKToUnicode(endorseFee);
                break;
            case 9:
                strFieldValue = StrTool.GBKToUnicode(contNo);
                break;
            case 10:
                strFieldValue = StrTool.GBKToUnicode(policyNo);
                break;
            case 11:
                strFieldValue = StrTool.GBKToUnicode(productCode);
                break;
            case 12:
                strFieldValue = StrTool.GBKToUnicode(riskCode);
                break;
            case 13:
                strFieldValue = StrTool.GBKToUnicode(surrenderReason);
                break;
            case 14:
                strFieldValue = StrTool.GBKToUnicode(hesitationType);
                break;
            case 15:
                strFieldValue = StrTool.GBKToUnicode(inAccount);
                break;
            case 16:
                strFieldValue = StrTool.GBKToUnicode(outAccount);
                break;
            case 17:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getPayTime()));
                break;
            case 18:
                strFieldValue = StrTool.GBKToUnicode(fee);
                break;
            case 19:
                strFieldValue = StrTool.GBKToUnicode(payFlowId);
                break;
            case 20:
                strFieldValue = StrTool.GBKToUnicode(discountFee);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("reqMsgId")) {
            if( FValue != null && !FValue.equals(""))
            {
                reqMsgId = FValue.trim();
            }
            else
                reqMsgId = null;
        }
        if (FCode.equalsIgnoreCase("instSerialNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                instSerialNo = FValue.trim();
            }
            else
                instSerialNo = null;
        }
        if (FCode.equalsIgnoreCase("sellType")) {
            if( FValue != null && !FValue.equals(""))
            {
                sellType = FValue.trim();
            }
            else
                sellType = null;
        }
        if (FCode.equalsIgnoreCase("endorseNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                endorseNo = FValue.trim();
            }
            else
                endorseNo = null;
        }
        if (FCode.equalsIgnoreCase("endorseCreateTime")) {
            if(FValue != null && !FValue.equals("")) {
                endorseCreateTime = fDate.getDate( FValue );
            }
            else
                endorseCreateTime = null;
        }
        if (FCode.equalsIgnoreCase("endorseTime")) {
            if(FValue != null && !FValue.equals("")) {
                endorseTime = fDate.getDate( FValue );
            }
            else
                endorseTime = null;
        }
        if (FCode.equalsIgnoreCase("endorseReason")) {
            if( FValue != null && !FValue.equals(""))
            {
                endorseReason = FValue.trim();
            }
            else
                endorseReason = null;
        }
        if (FCode.equalsIgnoreCase("endorseReasonDesc")) {
            if( FValue != null && !FValue.equals(""))
            {
                endorseReasonDesc = FValue.trim();
            }
            else
                endorseReasonDesc = null;
        }
        if (FCode.equalsIgnoreCase("endorseFee")) {
            if( FValue != null && !FValue.equals(""))
            {
                endorseFee = FValue.trim();
            }
            else
                endorseFee = null;
        }
        if (FCode.equalsIgnoreCase("contNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                contNo = FValue.trim();
            }
            else
                contNo = null;
        }
        if (FCode.equalsIgnoreCase("policyNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                policyNo = FValue.trim();
            }
            else
                policyNo = null;
        }
        if (FCode.equalsIgnoreCase("productCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                productCode = FValue.trim();
            }
            else
                productCode = null;
        }
        if (FCode.equalsIgnoreCase("riskCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                riskCode = FValue.trim();
            }
            else
                riskCode = null;
        }
        if (FCode.equalsIgnoreCase("surrenderReason")) {
            if( FValue != null && !FValue.equals(""))
            {
                surrenderReason = FValue.trim();
            }
            else
                surrenderReason = null;
        }
        if (FCode.equalsIgnoreCase("hesitationType")) {
            if( FValue != null && !FValue.equals(""))
            {
                hesitationType = FValue.trim();
            }
            else
                hesitationType = null;
        }
        if (FCode.equalsIgnoreCase("inAccount")) {
            if( FValue != null && !FValue.equals(""))
            {
                inAccount = FValue.trim();
            }
            else
                inAccount = null;
        }
        if (FCode.equalsIgnoreCase("outAccount")) {
            if( FValue != null && !FValue.equals(""))
            {
                outAccount = FValue.trim();
            }
            else
                outAccount = null;
        }
        if (FCode.equalsIgnoreCase("payTime")) {
            if(FValue != null && !FValue.equals("")) {
                payTime = fDate.getDate( FValue );
            }
            else
                payTime = null;
        }
        if (FCode.equalsIgnoreCase("fee")) {
            if( FValue != null && !FValue.equals(""))
            {
                fee = FValue.trim();
            }
            else
                fee = null;
        }
        if (FCode.equalsIgnoreCase("payFlowId")) {
            if( FValue != null && !FValue.equals(""))
            {
                payFlowId = FValue.trim();
            }
            else
                payFlowId = null;
        }
        if (FCode.equalsIgnoreCase("discountFee")) {
            if( FValue != null && !FValue.equals(""))
            {
                discountFee = FValue.trim();
            }
            else
                discountFee = null;
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject) return true;
        if (otherObject == null) return false;
        if (getClass() != otherObject.getClass()) return false;
        surrendValueSchema other = (surrendValueSchema)otherObject;
        return
            reqMsgId.equals(other.getReqMsgId())
            && instSerialNo.equals(other.getInstSerialNo())
            && sellType.equals(other.getSellType())
            && endorseNo.equals(other.getEndorseNo())
            && fDate.getString(endorseCreateTime).equals(other.getEndorseCreateTime())
            && fDate.getString(endorseTime).equals(other.getEndorseTime())
            && endorseReason.equals(other.getEndorseReason())
            && endorseReasonDesc.equals(other.getEndorseReasonDesc())
            && endorseFee.equals(other.getEndorseFee())
            && contNo.equals(other.getContNo())
            && policyNo.equals(other.getPolicyNo())
            && productCode.equals(other.getProductCode())
            && riskCode.equals(other.getRiskCode())
            && surrenderReason.equals(other.getSurrenderReason())
            && hesitationType.equals(other.getHesitationType())
            && inAccount.equals(other.getInAccount())
            && outAccount.equals(other.getOutAccount())
            && fDate.getString(payTime).equals(other.getPayTime())
            && fee.equals(other.getFee())
            && payFlowId.equals(other.getPayFlowId())
            && discountFee.equals(other.getDiscountFee());
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("reqMsgId") ) {
            return 0;
        }
        if( strFieldName.equals("instSerialNo") ) {
            return 1;
        }
        if( strFieldName.equals("sellType") ) {
            return 2;
        }
        if( strFieldName.equals("endorseNo") ) {
            return 3;
        }
        if( strFieldName.equals("endorseCreateTime") ) {
            return 4;
        }
        if( strFieldName.equals("endorseTime") ) {
            return 5;
        }
        if( strFieldName.equals("endorseReason") ) {
            return 6;
        }
        if( strFieldName.equals("endorseReasonDesc") ) {
            return 7;
        }
        if( strFieldName.equals("endorseFee") ) {
            return 8;
        }
        if( strFieldName.equals("contNo") ) {
            return 9;
        }
        if( strFieldName.equals("policyNo") ) {
            return 10;
        }
        if( strFieldName.equals("productCode") ) {
            return 11;
        }
        if( strFieldName.equals("riskCode") ) {
            return 12;
        }
        if( strFieldName.equals("surrenderReason") ) {
            return 13;
        }
        if( strFieldName.equals("hesitationType") ) {
            return 14;
        }
        if( strFieldName.equals("inAccount") ) {
            return 15;
        }
        if( strFieldName.equals("outAccount") ) {
            return 16;
        }
        if( strFieldName.equals("payTime") ) {
            return 17;
        }
        if( strFieldName.equals("fee") ) {
            return 18;
        }
        if( strFieldName.equals("payFlowId") ) {
            return 19;
        }
        if( strFieldName.equals("discountFee") ) {
            return 20;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "reqMsgId";
                break;
            case 1:
                strFieldName = "instSerialNo";
                break;
            case 2:
                strFieldName = "sellType";
                break;
            case 3:
                strFieldName = "endorseNo";
                break;
            case 4:
                strFieldName = "endorseCreateTime";
                break;
            case 5:
                strFieldName = "endorseTime";
                break;
            case 6:
                strFieldName = "endorseReason";
                break;
            case 7:
                strFieldName = "endorseReasonDesc";
                break;
            case 8:
                strFieldName = "endorseFee";
                break;
            case 9:
                strFieldName = "contNo";
                break;
            case 10:
                strFieldName = "policyNo";
                break;
            case 11:
                strFieldName = "productCode";
                break;
            case 12:
                strFieldName = "riskCode";
                break;
            case 13:
                strFieldName = "surrenderReason";
                break;
            case 14:
                strFieldName = "hesitationType";
                break;
            case 15:
                strFieldName = "inAccount";
                break;
            case 16:
                strFieldName = "outAccount";
                break;
            case 17:
                strFieldName = "payTime";
                break;
            case 18:
                strFieldName = "fee";
                break;
            case 19:
                strFieldName = "payFlowId";
                break;
            case 20:
                strFieldName = "discountFee";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "REQMSGID":
                return Schema.TYPE_STRING;
            case "INSTSERIALNO":
                return Schema.TYPE_STRING;
            case "SELLTYPE":
                return Schema.TYPE_STRING;
            case "ENDORSENO":
                return Schema.TYPE_STRING;
            case "ENDORSECREATETIME":
                return Schema.TYPE_DATE;
            case "ENDORSETIME":
                return Schema.TYPE_DATE;
            case "ENDORSEREASON":
                return Schema.TYPE_STRING;
            case "ENDORSEREASONDESC":
                return Schema.TYPE_STRING;
            case "ENDORSEFEE":
                return Schema.TYPE_STRING;
            case "CONTNO":
                return Schema.TYPE_STRING;
            case "POLICYNO":
                return Schema.TYPE_STRING;
            case "PRODUCTCODE":
                return Schema.TYPE_STRING;
            case "RISKCODE":
                return Schema.TYPE_STRING;
            case "SURRENDERREASON":
                return Schema.TYPE_STRING;
            case "HESITATIONTYPE":
                return Schema.TYPE_STRING;
            case "INACCOUNT":
                return Schema.TYPE_STRING;
            case "OUTACCOUNT":
                return Schema.TYPE_STRING;
            case "PAYTIME":
                return Schema.TYPE_DATE;
            case "FEE":
                return Schema.TYPE_STRING;
            case "PAYFLOWID":
                return Schema.TYPE_STRING;
            case "DISCOUNTFEE":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_STRING;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_DATE;
            case 5:
                return Schema.TYPE_DATE;
            case 6:
                return Schema.TYPE_STRING;
            case 7:
                return Schema.TYPE_STRING;
            case 8:
                return Schema.TYPE_STRING;
            case 9:
                return Schema.TYPE_STRING;
            case 10:
                return Schema.TYPE_STRING;
            case 11:
                return Schema.TYPE_STRING;
            case 12:
                return Schema.TYPE_STRING;
            case 13:
                return Schema.TYPE_STRING;
            case 14:
                return Schema.TYPE_STRING;
            case 15:
                return Schema.TYPE_STRING;
            case 16:
                return Schema.TYPE_STRING;
            case 17:
                return Schema.TYPE_DATE;
            case 18:
                return Schema.TYPE_STRING;
            case 19:
                return Schema.TYPE_STRING;
            case 20:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
}
