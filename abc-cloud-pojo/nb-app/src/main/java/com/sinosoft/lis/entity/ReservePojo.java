package com.sinosoft.lis.entity;

import java.io.Serializable;

/**
 * @Author:WangShuliang
 * @Description:
 * @Date:Created in 18:29 2018/3/29
 * @Modified by:
 */
public class ReservePojo implements Serializable {
    private  String  riskCode;
    private  String  policyValue;
    private  String  AccountValue;

    public String getRiskCode() {
        return riskCode;
    }

    public void setRiskCode(String riskCode) {
        this.riskCode = riskCode;
    }

    public String getPolicyValue() {
        return policyValue;
    }

    public void setPolicyValue(String policyValue) {
        this.policyValue = policyValue;
    }

    public String getAccountValue() {
        return AccountValue;
    }

    public void setAccountValue(String accountValue) {
        AccountValue = accountValue;
    }

    @Override
    public String toString() {
        return "ReservePojo{" +
                "riskCode='" + riskCode + '\'' +
                ", policyValue='" + policyValue + '\'' +
                ", AccountValue='" + AccountValue + '\'' +
                '}';
    }
}