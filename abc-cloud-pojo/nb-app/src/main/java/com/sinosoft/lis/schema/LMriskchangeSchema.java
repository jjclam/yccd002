/**
 * Copyright (c) 2019 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LMriskchangeDB;

/**
 * <p>ClassName: LMriskchangeSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2019</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2019-01-18
 */
public class LMriskchangeSchema implements Schema, Cloneable {
    // @Field
    /** 录入险种编码 */
    private String InputRiskCode;
    /** 险种编码 */
    private String RiskCode;
    /** 返回险种编码 */
    private String BackRiskCode;
    /** 组合险编码 */
    private String ProdSetCode;
    /** 销售渠道 */
    private String SellType;
    /** 是否拆分 */
    private String QFlag;
    /** 备用字段1 */
    private String Bak1;
    /** 备用字段2 */
    private String Bak2;
    /** 备用字段3 */
    private String Bak3;
    /** 备用字段4 */
    private String Bak4;
    /** 备用字段5 */
    private String Bak5;

    public static final int FIELDNUM = 11;    // 数据库表的字段个数

    private static String[] PK;                // 主键

    public CErrors mErrors;            // 错误信息

    // @Constructor
    public LMriskchangeSchema() {
        mErrors = new CErrors();

        String[] pk = new String[2];
        pk[0] = "InputRiskCode";
        pk[1] = "BackRiskCode";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException {
        LMriskchangeSchema cloned = (LMriskchangeSchema)super.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK() {
        return PK;
    }

    public String getInputRiskCode() {
        return InputRiskCode;
    }
    public void setInputRiskCode(String aInputRiskCode) {
        InputRiskCode = aInputRiskCode;
    }
    public String getRiskCode() {
        return RiskCode;
    }
    public void setRiskCode(String aRiskCode) {
        RiskCode = aRiskCode;
    }
    public String getBackRiskCode() {
        return BackRiskCode;
    }
    public void setBackRiskCode(String aBackRiskCode) {
        BackRiskCode = aBackRiskCode;
    }
    public String getProdSetCode() {
        return ProdSetCode;
    }
    public void setProdSetCode(String aProdSetCode) {
        ProdSetCode = aProdSetCode;
    }
    public String getSellType() {
        return SellType;
    }
    public void setSellType(String aSellType) {
        SellType = aSellType;
    }
    public String getQFlag() {
        return QFlag;
    }
    public void setQFlag(String aQFlag) {
        QFlag = aQFlag;
    }
    public String getBak1() {
        return Bak1;
    }
    public void setBak1(String aBak1) {
        Bak1 = aBak1;
    }
    public String getBak2() {
        return Bak2;
    }
    public void setBak2(String aBak2) {
        Bak2 = aBak2;
    }
    public String getBak3() {
        return Bak3;
    }
    public void setBak3(String aBak3) {
        Bak3 = aBak3;
    }
    public String getBak4() {
        return Bak4;
    }
    public void setBak4(String aBak4) {
        Bak4 = aBak4;
    }
    public String getBak5() {
        return Bak5;
    }
    public void setBak5(String aBak5) {
        Bak5 = aBak5;
    }

    /**
    * 使用另外一个 LMriskchangeSchema 对象给 Schema 赋值
    * @param: aLMriskchangeSchema LMriskchangeSchema
    **/
    public void setSchema(LMriskchangeSchema aLMriskchangeSchema) {
        this.InputRiskCode = aLMriskchangeSchema.getInputRiskCode();
        this.RiskCode = aLMriskchangeSchema.getRiskCode();
        this.BackRiskCode = aLMriskchangeSchema.getBackRiskCode();
        this.ProdSetCode = aLMriskchangeSchema.getProdSetCode();
        this.SellType = aLMriskchangeSchema.getSellType();
        this.QFlag = aLMriskchangeSchema.getQFlag();
        this.Bak1 = aLMriskchangeSchema.getBak1();
        this.Bak2 = aLMriskchangeSchema.getBak2();
        this.Bak3 = aLMriskchangeSchema.getBak3();
        this.Bak4 = aLMriskchangeSchema.getBak4();
        this.Bak5 = aLMriskchangeSchema.getBak5();
    }

    /**
    * 使用 ResultSet 中的第 i 行给 Schema 赋值
    * @param: rs ResultSet
    * @param: i int
    * @return: boolean
    **/
    public boolean setSchema(ResultSet rs,int i) {
        try {
            //rs.absolute(i);        // 非滚动游标
            if( rs.getString("InputRiskCode") == null )
                this.InputRiskCode = null;
            else
                this.InputRiskCode = rs.getString("InputRiskCode").trim();

            if( rs.getString("RiskCode") == null )
                this.RiskCode = null;
            else
                this.RiskCode = rs.getString("RiskCode").trim();

            if( rs.getString("BackRiskCode") == null )
                this.BackRiskCode = null;
            else
                this.BackRiskCode = rs.getString("BackRiskCode").trim();

            if( rs.getString("ProdSetCode") == null )
                this.ProdSetCode = null;
            else
                this.ProdSetCode = rs.getString("ProdSetCode").trim();

            if( rs.getString("SellType") == null )
                this.SellType = null;
            else
                this.SellType = rs.getString("SellType").trim();

            if( rs.getString("QFlag") == null )
                this.QFlag = null;
            else
                this.QFlag = rs.getString("QFlag").trim();

            if( rs.getString("Bak1") == null )
                this.Bak1 = null;
            else
                this.Bak1 = rs.getString("Bak1").trim();

            if( rs.getString("Bak2") == null )
                this.Bak2 = null;
            else
                this.Bak2 = rs.getString("Bak2").trim();

            if( rs.getString("Bak3") == null )
                this.Bak3 = null;
            else
                this.Bak3 = rs.getString("Bak3").trim();

            if( rs.getString("Bak4") == null )
                this.Bak4 = null;
            else
                this.Bak4 = rs.getString("Bak4").trim();

            if( rs.getString("Bak5") == null )
                this.Bak5 = null;
            else
                this.Bak5 = rs.getString("Bak5").trim();

        }
        catch(SQLException sqle) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LMriskchangeSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = ExceptionUtils.exceptionToString(sqle);
            this.mErrors .addOneError(tError);
            return false;
        }
        return true;
    }

    public LMriskchangeSchema getSchema() {
        LMriskchangeSchema aLMriskchangeSchema = new LMriskchangeSchema();
        aLMriskchangeSchema.setSchema(this);
        return aLMriskchangeSchema;
    }

    public LMriskchangeDB getDB() {
        LMriskchangeDB aDBOper = new LMriskchangeDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
    * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLMriskchange描述/A>表字段
    * @return: String 返回打包后字符串
    **/
    public String encode() {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(InputRiskCode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(RiskCode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(BackRiskCode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ProdSetCode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(SellType)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(QFlag)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Bak1)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Bak2)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Bak3)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Bak4)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Bak5));
        return strReturn.toString();
    }

    /**
    * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLMriskchange>历史记账凭证主表信息</A>表字段
    * @param: strMessage String 包含一条纪录数据的字符串
    * @return: boolean
    **/
    public boolean decode(String strMessage) {
        try {
            InputRiskCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
            RiskCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
            BackRiskCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
            ProdSetCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
            SellType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
            QFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
            Bak1 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
            Bak2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
            Bak3 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
            Bak4 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
            Bak5 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
        }
        catch(NumberFormatException ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LMriskchangeSchema";
            tError.functionName = "decode";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            return false;
        }
        return true;
    }

    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("InputRiskCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InputRiskCode));
        }
        if (FCode.equalsIgnoreCase("RiskCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RiskCode));
        }
        if (FCode.equalsIgnoreCase("BackRiskCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BackRiskCode));
        }
        if (FCode.equalsIgnoreCase("ProdSetCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ProdSetCode));
        }
        if (FCode.equalsIgnoreCase("SellType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SellType));
        }
        if (FCode.equalsIgnoreCase("QFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(QFlag));
        }
        if (FCode.equalsIgnoreCase("Bak1")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Bak1));
        }
        if (FCode.equalsIgnoreCase("Bak2")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Bak2));
        }
        if (FCode.equalsIgnoreCase("Bak3")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Bak3));
        }
        if (FCode.equalsIgnoreCase("Bak4")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Bak4));
        }
        if (FCode.equalsIgnoreCase("Bak5")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Bak5));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(InputRiskCode);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(RiskCode);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(BackRiskCode);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(ProdSetCode);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(SellType);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(QFlag);
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(Bak1);
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(Bak2);
                break;
            case 8:
                strFieldValue = StrTool.GBKToUnicode(Bak3);
                break;
            case 9:
                strFieldValue = StrTool.GBKToUnicode(Bak4);
                break;
            case 10:
                strFieldValue = StrTool.GBKToUnicode(Bak5);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("InputRiskCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                InputRiskCode = FValue.trim();
            }
            else
                InputRiskCode = null;
        }
        if (FCode.equalsIgnoreCase("RiskCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                RiskCode = FValue.trim();
            }
            else
                RiskCode = null;
        }
        if (FCode.equalsIgnoreCase("BackRiskCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                BackRiskCode = FValue.trim();
            }
            else
                BackRiskCode = null;
        }
        if (FCode.equalsIgnoreCase("ProdSetCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                ProdSetCode = FValue.trim();
            }
            else
                ProdSetCode = null;
        }
        if (FCode.equalsIgnoreCase("SellType")) {
            if( FValue != null && !FValue.equals(""))
            {
                SellType = FValue.trim();
            }
            else
                SellType = null;
        }
        if (FCode.equalsIgnoreCase("QFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                QFlag = FValue.trim();
            }
            else
                QFlag = null;
        }
        if (FCode.equalsIgnoreCase("Bak1")) {
            if( FValue != null && !FValue.equals(""))
            {
                Bak1 = FValue.trim();
            }
            else
                Bak1 = null;
        }
        if (FCode.equalsIgnoreCase("Bak2")) {
            if( FValue != null && !FValue.equals(""))
            {
                Bak2 = FValue.trim();
            }
            else
                Bak2 = null;
        }
        if (FCode.equalsIgnoreCase("Bak3")) {
            if( FValue != null && !FValue.equals(""))
            {
                Bak3 = FValue.trim();
            }
            else
                Bak3 = null;
        }
        if (FCode.equalsIgnoreCase("Bak4")) {
            if( FValue != null && !FValue.equals(""))
            {
                Bak4 = FValue.trim();
            }
            else
                Bak4 = null;
        }
        if (FCode.equalsIgnoreCase("Bak5")) {
            if( FValue != null && !FValue.equals(""))
            {
                Bak5 = FValue.trim();
            }
            else
                Bak5 = null;
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject) return true;
        if (otherObject == null) return false;
        if (getClass() != otherObject.getClass()) return false;
        LMriskchangeSchema other = (LMriskchangeSchema)otherObject;
        return
            InputRiskCode.equals(other.getInputRiskCode())
            && RiskCode.equals(other.getRiskCode())
            && BackRiskCode.equals(other.getBackRiskCode())
            && ProdSetCode.equals(other.getProdSetCode())
            && SellType.equals(other.getSellType())
            && QFlag.equals(other.getQFlag())
            && Bak1.equals(other.getBak1())
            && Bak2.equals(other.getBak2())
            && Bak3.equals(other.getBak3())
            && Bak4.equals(other.getBak4())
            && Bak5.equals(other.getBak5());
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("InputRiskCode") ) {
            return 0;
        }
        if( strFieldName.equals("RiskCode") ) {
            return 1;
        }
        if( strFieldName.equals("BackRiskCode") ) {
            return 2;
        }
        if( strFieldName.equals("ProdSetCode") ) {
            return 3;
        }
        if( strFieldName.equals("SellType") ) {
            return 4;
        }
        if( strFieldName.equals("QFlag") ) {
            return 5;
        }
        if( strFieldName.equals("Bak1") ) {
            return 6;
        }
        if( strFieldName.equals("Bak2") ) {
            return 7;
        }
        if( strFieldName.equals("Bak3") ) {
            return 8;
        }
        if( strFieldName.equals("Bak4") ) {
            return 9;
        }
        if( strFieldName.equals("Bak5") ) {
            return 10;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "InputRiskCode";
                break;
            case 1:
                strFieldName = "RiskCode";
                break;
            case 2:
                strFieldName = "BackRiskCode";
                break;
            case 3:
                strFieldName = "ProdSetCode";
                break;
            case 4:
                strFieldName = "SellType";
                break;
            case 5:
                strFieldName = "QFlag";
                break;
            case 6:
                strFieldName = "Bak1";
                break;
            case 7:
                strFieldName = "Bak2";
                break;
            case 8:
                strFieldName = "Bak3";
                break;
            case 9:
                strFieldName = "Bak4";
                break;
            case 10:
                strFieldName = "Bak5";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "INPUTRISKCODE":
                return Schema.TYPE_STRING;
            case "RISKCODE":
                return Schema.TYPE_STRING;
            case "BACKRISKCODE":
                return Schema.TYPE_STRING;
            case "PRODSETCODE":
                return Schema.TYPE_STRING;
            case "SELLTYPE":
                return Schema.TYPE_STRING;
            case "QFLAG":
                return Schema.TYPE_STRING;
            case "BAK1":
                return Schema.TYPE_STRING;
            case "BAK2":
                return Schema.TYPE_STRING;
            case "BAK3":
                return Schema.TYPE_STRING;
            case "BAK4":
                return Schema.TYPE_STRING;
            case "BAK5":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_STRING;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_STRING;
            case 6:
                return Schema.TYPE_STRING;
            case 7:
                return Schema.TYPE_STRING;
            case 8:
                return Schema.TYPE_STRING;
            case 9:
                return Schema.TYPE_STRING;
            case 10:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    public String toString() {
    return "LMriskchangeSchema {" +
            "InputRiskCode="+InputRiskCode +
            ", RiskCode="+RiskCode +
            ", BackRiskCode="+BackRiskCode +
            ", ProdSetCode="+ProdSetCode +
            ", SellType="+SellType +
            ", QFlag="+QFlag +
            ", Bak1="+Bak1 +
            ", Bak2="+Bak2 +
            ", Bak3="+Bak3 +
            ", Bak4="+Bak4 +
            ", Bak5="+Bak5 +"}";
    }
}
