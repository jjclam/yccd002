/**
 * Copyright (c) 2019 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.entity;

import java.io.*;
import java.util.Date;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.StrTool;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * <p>ClassName: LCAppntPojo </p>
 * <p>Copyright: Copyright (c) 2019</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2019-09-07
 */
public class LCAppntPojo implements  Pojo,Serializable {
    // @Field
    /** Id */
    private long AppntID; 
    /** Fk_lccont */
    private long ContID; 
    /** Fk_ldperson */
    private long PersonID; 
    /** Shardingid */
    private String ShardingID; 
    /** 集体合同号码 */
    private String GrpContNo; 
    /** 合同号码 */
    private String ContNo; 
    /** 印刷号码 */
    private String PrtNo; 
    /** 投保人客户号码 */
    private String AppntNo; 
    /** 投保人级别 */
    private String AppntGrade; 
    /** 投保人名称 */
    private String AppntName; 
    /** 投保人性别 */
    private String AppntSex; 
    /** 投保人出生日期 */
    private String  AppntBirthday;
    /** 投保人类型 */
    private String AppntType; 
    /** 客户地址号码 */
    private String AddressNo; 
    /** 证件类型 */
    private String IDType; 
    /** 证件号码 */
    private String IDNo; 
    /** 国籍 */
    private String NativePlace; 
    /** 民族 */
    private String Nationality; 
    /** 户口所在地 */
    private String RgtAddress; 
    /** 婚姻状况 */
    private String Marriage; 
    /** 结婚日期 */
    private String  MarriageDate;
    /** 健康状况 */
    private String Health; 
    /** 身高 */
    private double Stature; 
    /** 体重 */
    private double Avoirdupois; 
    /** 学历 */
    private String Degree; 
    /** 信用等级 */
    private String CreditGrade; 
    /** 银行编码 */
    private String BankCode; 
    /** 银行帐号 */
    private String BankAccNo; 
    /** 银行帐户名 */
    private String AccName; 
    /** 入司日期 */
    private String  JoinCompanyDate;
    /** 参加工作日期 */
    private String  StartWorkDate;
    /** 职位 */
    private String Position; 
    /** 工资 */
    private double Salary; 
    /** 职业类别 */
    private String OccupationType; 
    /** 职业代码 */
    private String OccupationCode; 
    /** 职业（工种） */
    private String WorkType; 
    /** 兼职（工种） */
    private String PluralityType; 
    /** 是否吸烟标志 */
    private String SmokeFlag; 
    /** 操作员 */
    private String Operator; 
    /** 管理机构 */
    private String ManageCom; 
    /** 入机日期 */
    private String  MakeDate;
    /** 入机时间 */
    private String MakeTime; 
    /** 最后一次修改日期 */
    private String  ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime; 
    /** 身体指标 */
    private double BMI; 
    /** 驾照 */
    private String License; 
    /** 驾照类型 */
    private String LicenseType; 
    /** 与被保人关系 */
    private String RelatToInsu; 
    /** 职业描述 */
    private String OccupationDesb; 
    /** 证件有效期 */
    private String IdValiDate; 
    /** 是否有摩托车驾照 */
    private String HaveMotorcycleLicence; 
    /** 兼职 */
    private String PartTimeJob; 
    /** Firstname */
    private String FirstName; 
    /** Lastname */
    private String LastName; 
    /** 客户级别 */
    private String CUSLevel; 
    /** 居民类型 */
    private String AppRgtTpye; 
    /** 美国纳税人识别号 */
    private String TINNO; 
    /** 是否为美国纳税义务的个人 */
    private String TINFlag; 
    /** 是否有社保标志 */
    private String SocialInsuFlag; 
    /** 英文姓 */
    private String LastNameEn; 
    /** 英文名 */
    private String FirstNameEn; 
    /** 英文姓名 */
    private String NameEn; 
    /** 拼音姓 */
    private String LastNamePY; 
    /** 拼音名 */
    private String FirstNamePY; 
    /** 语言 */
    private String Language; 
    /** 特殊人群标识 */
    private String SpecialPopFlag; 
    /** 确认栏填写标识 */
    private String AffirmInputFlag; 
    /** 年收入 */
    private double YearIncome; 
    /** 收入来源 */
    private String IncomeSource; 
    /** 家庭年收入 */
    private double FamilyYearSalary; 
    /** Dentype */
    private String DenType; 
    /** Budgetprem */
    private String BudgetPrem; 
    /** Taxresidenttype */
    private String TAXRESIDENTTYPE; 
    /** Idstdate */
    private String IDSTDATE; 


    public static final int FIELDNUM = 74;    // 数据库表的字段个数

    private FDate fDate = new FDate();        // 处理日期
    public long getAppntID() {
        return AppntID;
    }
    public void setAppntID(long aAppntID) {
        AppntID = aAppntID;
    }
    public void setAppntID(String aAppntID) {
        if (aAppntID != null && !aAppntID.equals("")) {
            AppntID = new Long(aAppntID).longValue();
        }
    }

    public long getContID() {
        return ContID;
    }
    public void setContID(long aContID) {
        ContID = aContID;
    }
    public void setContID(String aContID) {
        if (aContID != null && !aContID.equals("")) {
            ContID = new Long(aContID).longValue();
        }
    }

    public long getPersonID() {
        return PersonID;
    }
    public void setPersonID(long aPersonID) {
        PersonID = aPersonID;
    }
    public void setPersonID(String aPersonID) {
        if (aPersonID != null && !aPersonID.equals("")) {
            PersonID = new Long(aPersonID).longValue();
        }
    }

    public String getShardingID() {
        return ShardingID;
    }
    public void setShardingID(String aShardingID) {
        ShardingID = aShardingID;
    }
    public String getGrpContNo() {
        return GrpContNo;
    }
    public void setGrpContNo(String aGrpContNo) {
        GrpContNo = aGrpContNo;
    }
    public String getContNo() {
        return ContNo;
    }
    public void setContNo(String aContNo) {
        ContNo = aContNo;
    }
    public String getPrtNo() {
        return PrtNo;
    }
    public void setPrtNo(String aPrtNo) {
        PrtNo = aPrtNo;
    }
    public String getAppntNo() {
        return AppntNo;
    }
    public void setAppntNo(String aAppntNo) {
        AppntNo = aAppntNo;
    }
    public String getAppntGrade() {
        return AppntGrade;
    }
    public void setAppntGrade(String aAppntGrade) {
        AppntGrade = aAppntGrade;
    }
    public String getAppntName() {
        return AppntName;
    }
    public void setAppntName(String aAppntName) {
        AppntName = aAppntName;
    }
    public String getAppntSex() {
        return AppntSex;
    }
    public void setAppntSex(String aAppntSex) {
        AppntSex = aAppntSex;
    }
    public String getAppntBirthday() {
        return AppntBirthday;
    }
    public void setAppntBirthday(String aAppntBirthday) {
        AppntBirthday = aAppntBirthday;
    }
    public String getAppntType() {
        return AppntType;
    }
    public void setAppntType(String aAppntType) {
        AppntType = aAppntType;
    }
    public String getAddressNo() {
        return AddressNo;
    }
    public void setAddressNo(String aAddressNo) {
        AddressNo = aAddressNo;
    }
    public String getIDType() {
        return IDType;
    }
    public void setIDType(String aIDType) {
        IDType = aIDType;
    }
    public String getIDNo() {
        return IDNo;
    }
    public void setIDNo(String aIDNo) {
        IDNo = aIDNo;
    }
    public String getNativePlace() {
        return NativePlace;
    }
    public void setNativePlace(String aNativePlace) {
        NativePlace = aNativePlace;
    }
    public String getNationality() {
        return Nationality;
    }
    public void setNationality(String aNationality) {
        Nationality = aNationality;
    }
    public String getRgtAddress() {
        return RgtAddress;
    }
    public void setRgtAddress(String aRgtAddress) {
        RgtAddress = aRgtAddress;
    }
    public String getMarriage() {
        return Marriage;
    }
    public void setMarriage(String aMarriage) {
        Marriage = aMarriage;
    }
    public String getMarriageDate() {
        return MarriageDate;
    }
    public void setMarriageDate(String aMarriageDate) {
        MarriageDate = aMarriageDate;
    }
    public String getHealth() {
        return Health;
    }
    public void setHealth(String aHealth) {
        Health = aHealth;
    }
    public double getStature() {
        return Stature;
    }
    public void setStature(double aStature) {
        Stature = aStature;
    }
    public void setStature(String aStature) {
        if (aStature != null && !aStature.equals("")) {
            Double tDouble = new Double(aStature);
            double d = tDouble.doubleValue();
            Stature = d;
        }
    }

    public double getAvoirdupois() {
        return Avoirdupois;
    }
    public void setAvoirdupois(double aAvoirdupois) {
        Avoirdupois = aAvoirdupois;
    }
    public void setAvoirdupois(String aAvoirdupois) {
        if (aAvoirdupois != null && !aAvoirdupois.equals("")) {
            Double tDouble = new Double(aAvoirdupois);
            double d = tDouble.doubleValue();
            Avoirdupois = d;
        }
    }

    public String getDegree() {
        return Degree;
    }
    public void setDegree(String aDegree) {
        Degree = aDegree;
    }
    public String getCreditGrade() {
        return CreditGrade;
    }
    public void setCreditGrade(String aCreditGrade) {
        CreditGrade = aCreditGrade;
    }
    public String getBankCode() {
        return BankCode;
    }
    public void setBankCode(String aBankCode) {
        BankCode = aBankCode;
    }
    public String getBankAccNo() {
        return BankAccNo;
    }
    public void setBankAccNo(String aBankAccNo) {
        BankAccNo = aBankAccNo;
    }
    public String getAccName() {
        return AccName;
    }
    public void setAccName(String aAccName) {
        AccName = aAccName;
    }
    public String getJoinCompanyDate() {
        return JoinCompanyDate;
    }
    public void setJoinCompanyDate(String aJoinCompanyDate) {
        JoinCompanyDate = aJoinCompanyDate;
    }
    public String getStartWorkDate() {
        return StartWorkDate;
    }
    public void setStartWorkDate(String aStartWorkDate) {
        StartWorkDate = aStartWorkDate;
    }
    public String getPosition() {
        return Position;
    }
    public void setPosition(String aPosition) {
        Position = aPosition;
    }
    public double getSalary() {
        return Salary;
    }
    public void setSalary(double aSalary) {
        Salary = aSalary;
    }
    public void setSalary(String aSalary) {
        if (aSalary != null && !aSalary.equals("")) {
            Double tDouble = new Double(aSalary);
            double d = tDouble.doubleValue();
            Salary = d;
        }
    }

    public String getOccupationType() {
        return OccupationType;
    }
    public void setOccupationType(String aOccupationType) {
        OccupationType = aOccupationType;
    }
    public String getOccupationCode() {
        return OccupationCode;
    }
    public void setOccupationCode(String aOccupationCode) {
        OccupationCode = aOccupationCode;
    }
    public String getWorkType() {
        return WorkType;
    }
    public void setWorkType(String aWorkType) {
        WorkType = aWorkType;
    }
    public String getPluralityType() {
        return PluralityType;
    }
    public void setPluralityType(String aPluralityType) {
        PluralityType = aPluralityType;
    }
    public String getSmokeFlag() {
        return SmokeFlag;
    }
    public void setSmokeFlag(String aSmokeFlag) {
        SmokeFlag = aSmokeFlag;
    }
    public String getOperator() {
        return Operator;
    }
    public void setOperator(String aOperator) {
        Operator = aOperator;
    }
    public String getManageCom() {
        return ManageCom;
    }
    public void setManageCom(String aManageCom) {
        ManageCom = aManageCom;
    }
    public String getMakeDate() {
        return MakeDate;
    }
    public void setMakeDate(String aMakeDate) {
        MakeDate = aMakeDate;
    }
    public String getMakeTime() {
        return MakeTime;
    }
    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }
    public String getModifyDate() {
        return ModifyDate;
    }
    public void setModifyDate(String aModifyDate) {
        ModifyDate = aModifyDate;
    }
    public String getModifyTime() {
        return ModifyTime;
    }
    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }
    public double getBMI() {
        return BMI;
    }
    public void setBMI(double aBMI) {
        BMI = aBMI;
    }
    public void setBMI(String aBMI) {
        if (aBMI != null && !aBMI.equals("")) {
            Double tDouble = new Double(aBMI);
            double d = tDouble.doubleValue();
            BMI = d;
        }
    }

    public String getLicense() {
        return License;
    }
    public void setLicense(String aLicense) {
        License = aLicense;
    }
    public String getLicenseType() {
        return LicenseType;
    }
    public void setLicenseType(String aLicenseType) {
        LicenseType = aLicenseType;
    }
    public String getRelatToInsu() {
        return RelatToInsu;
    }
    public void setRelatToInsu(String aRelatToInsu) {
        RelatToInsu = aRelatToInsu;
    }
    public String getOccupationDesb() {
        return OccupationDesb;
    }
    public void setOccupationDesb(String aOccupationDesb) {
        OccupationDesb = aOccupationDesb;
    }
    public String getIdValiDate() {
        return IdValiDate;
    }
    public void setIdValiDate(String aIdValiDate) {
        IdValiDate = aIdValiDate;
    }
    public String getHaveMotorcycleLicence() {
        return HaveMotorcycleLicence;
    }
    public void setHaveMotorcycleLicence(String aHaveMotorcycleLicence) {
        HaveMotorcycleLicence = aHaveMotorcycleLicence;
    }
    public String getPartTimeJob() {
        return PartTimeJob;
    }
    public void setPartTimeJob(String aPartTimeJob) {
        PartTimeJob = aPartTimeJob;
    }
    public String getFirstName() {
        return FirstName;
    }
    public void setFirstName(String aFirstName) {
        FirstName = aFirstName;
    }
    public String getLastName() {
        return LastName;
    }
    public void setLastName(String aLastName) {
        LastName = aLastName;
    }
    public String getCUSLevel() {
        return CUSLevel;
    }
    public void setCUSLevel(String aCUSLevel) {
        CUSLevel = aCUSLevel;
    }
    public String getAppRgtTpye() {
        return AppRgtTpye;
    }
    public void setAppRgtTpye(String aAppRgtTpye) {
        AppRgtTpye = aAppRgtTpye;
    }
    public String getTINNO() {
        return TINNO;
    }
    public void setTINNO(String aTINNO) {
        TINNO = aTINNO;
    }
    public String getTINFlag() {
        return TINFlag;
    }
    public void setTINFlag(String aTINFlag) {
        TINFlag = aTINFlag;
    }
    public String getSocialInsuFlag() {
        return SocialInsuFlag;
    }
    public void setSocialInsuFlag(String aSocialInsuFlag) {
        SocialInsuFlag = aSocialInsuFlag;
    }
    public String getLastNameEn() {
        return LastNameEn;
    }
    public void setLastNameEn(String aLastNameEn) {
        LastNameEn = aLastNameEn;
    }
    public String getFirstNameEn() {
        return FirstNameEn;
    }
    public void setFirstNameEn(String aFirstNameEn) {
        FirstNameEn = aFirstNameEn;
    }
    public String getNameEn() {
        return NameEn;
    }
    public void setNameEn(String aNameEn) {
        NameEn = aNameEn;
    }
    public String getLastNamePY() {
        return LastNamePY;
    }
    public void setLastNamePY(String aLastNamePY) {
        LastNamePY = aLastNamePY;
    }
    public String getFirstNamePY() {
        return FirstNamePY;
    }
    public void setFirstNamePY(String aFirstNamePY) {
        FirstNamePY = aFirstNamePY;
    }
    public String getLanguage() {
        return Language;
    }
    public void setLanguage(String aLanguage) {
        Language = aLanguage;
    }
    public String getSpecialPopFlag() {
        return SpecialPopFlag;
    }
    public void setSpecialPopFlag(String aSpecialPopFlag) {
        SpecialPopFlag = aSpecialPopFlag;
    }
    public String getAffirmInputFlag() {
        return AffirmInputFlag;
    }
    public void setAffirmInputFlag(String aAffirmInputFlag) {
        AffirmInputFlag = aAffirmInputFlag;
    }
    public double getYearIncome() {
        return YearIncome;
    }
    public void setYearIncome(double aYearIncome) {
        YearIncome = aYearIncome;
    }
    public void setYearIncome(String aYearIncome) {
        if (aYearIncome != null && !aYearIncome.equals("")) {
            Double tDouble = new Double(aYearIncome);
            double d = tDouble.doubleValue();
            YearIncome = d;
        }
    }

    public String getIncomeSource() {
        return IncomeSource;
    }
    public void setIncomeSource(String aIncomeSource) {
        IncomeSource = aIncomeSource;
    }
    public double getFamilyYearSalary() {
        return FamilyYearSalary;
    }
    public void setFamilyYearSalary(double aFamilyYearSalary) {
        FamilyYearSalary = aFamilyYearSalary;
    }
    public void setFamilyYearSalary(String aFamilyYearSalary) {
        if (aFamilyYearSalary != null && !aFamilyYearSalary.equals("")) {
            Double tDouble = new Double(aFamilyYearSalary);
            double d = tDouble.doubleValue();
            FamilyYearSalary = d;
        }
    }

    public String getDenType() {
        return DenType;
    }
    public void setDenType(String aDenType) {
        DenType = aDenType;
    }
    public String getBudgetPrem() {
        return BudgetPrem;
    }
    public void setBudgetPrem(String aBudgetPrem) {
        BudgetPrem = aBudgetPrem;
    }
    public String getTAXRESIDENTTYPE() {
        return TAXRESIDENTTYPE;
    }
    public void setTAXRESIDENTTYPE(String aTAXRESIDENTTYPE) {
        TAXRESIDENTTYPE = aTAXRESIDENTTYPE;
    }
    public String getIDSTDATE() {
        return IDSTDATE;
    }
    public void setIDSTDATE(String aIDSTDATE) {
        IDSTDATE = aIDSTDATE;
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("AppntID") ) {
            return 0;
        }
        if( strFieldName.equals("ContID") ) {
            return 1;
        }
        if( strFieldName.equals("PersonID") ) {
            return 2;
        }
        if( strFieldName.equals("ShardingID") ) {
            return 3;
        }
        if( strFieldName.equals("GrpContNo") ) {
            return 4;
        }
        if( strFieldName.equals("ContNo") ) {
            return 5;
        }
        if( strFieldName.equals("PrtNo") ) {
            return 6;
        }
        if( strFieldName.equals("AppntNo") ) {
            return 7;
        }
        if( strFieldName.equals("AppntGrade") ) {
            return 8;
        }
        if( strFieldName.equals("AppntName") ) {
            return 9;
        }
        if( strFieldName.equals("AppntSex") ) {
            return 10;
        }
        if( strFieldName.equals("AppntBirthday") ) {
            return 11;
        }
        if( strFieldName.equals("AppntType") ) {
            return 12;
        }
        if( strFieldName.equals("AddressNo") ) {
            return 13;
        }
        if( strFieldName.equals("IDType") ) {
            return 14;
        }
        if( strFieldName.equals("IDNo") ) {
            return 15;
        }
        if( strFieldName.equals("NativePlace") ) {
            return 16;
        }
        if( strFieldName.equals("Nationality") ) {
            return 17;
        }
        if( strFieldName.equals("RgtAddress") ) {
            return 18;
        }
        if( strFieldName.equals("Marriage") ) {
            return 19;
        }
        if( strFieldName.equals("MarriageDate") ) {
            return 20;
        }
        if( strFieldName.equals("Health") ) {
            return 21;
        }
        if( strFieldName.equals("Stature") ) {
            return 22;
        }
        if( strFieldName.equals("Avoirdupois") ) {
            return 23;
        }
        if( strFieldName.equals("Degree") ) {
            return 24;
        }
        if( strFieldName.equals("CreditGrade") ) {
            return 25;
        }
        if( strFieldName.equals("BankCode") ) {
            return 26;
        }
        if( strFieldName.equals("BankAccNo") ) {
            return 27;
        }
        if( strFieldName.equals("AccName") ) {
            return 28;
        }
        if( strFieldName.equals("JoinCompanyDate") ) {
            return 29;
        }
        if( strFieldName.equals("StartWorkDate") ) {
            return 30;
        }
        if( strFieldName.equals("Position") ) {
            return 31;
        }
        if( strFieldName.equals("Salary") ) {
            return 32;
        }
        if( strFieldName.equals("OccupationType") ) {
            return 33;
        }
        if( strFieldName.equals("OccupationCode") ) {
            return 34;
        }
        if( strFieldName.equals("WorkType") ) {
            return 35;
        }
        if( strFieldName.equals("PluralityType") ) {
            return 36;
        }
        if( strFieldName.equals("SmokeFlag") ) {
            return 37;
        }
        if( strFieldName.equals("Operator") ) {
            return 38;
        }
        if( strFieldName.equals("ManageCom") ) {
            return 39;
        }
        if( strFieldName.equals("MakeDate") ) {
            return 40;
        }
        if( strFieldName.equals("MakeTime") ) {
            return 41;
        }
        if( strFieldName.equals("ModifyDate") ) {
            return 42;
        }
        if( strFieldName.equals("ModifyTime") ) {
            return 43;
        }
        if( strFieldName.equals("BMI") ) {
            return 44;
        }
        if( strFieldName.equals("License") ) {
            return 45;
        }
        if( strFieldName.equals("LicenseType") ) {
            return 46;
        }
        if( strFieldName.equals("RelatToInsu") ) {
            return 47;
        }
        if( strFieldName.equals("OccupationDesb") ) {
            return 48;
        }
        if( strFieldName.equals("IdValiDate") ) {
            return 49;
        }
        if( strFieldName.equals("HaveMotorcycleLicence") ) {
            return 50;
        }
        if( strFieldName.equals("PartTimeJob") ) {
            return 51;
        }
        if( strFieldName.equals("FirstName") ) {
            return 52;
        }
        if( strFieldName.equals("LastName") ) {
            return 53;
        }
        if( strFieldName.equals("CUSLevel") ) {
            return 54;
        }
        if( strFieldName.equals("AppRgtTpye") ) {
            return 55;
        }
        if( strFieldName.equals("TINNO") ) {
            return 56;
        }
        if( strFieldName.equals("TINFlag") ) {
            return 57;
        }
        if( strFieldName.equals("SocialInsuFlag") ) {
            return 58;
        }
        if( strFieldName.equals("LastNameEn") ) {
            return 59;
        }
        if( strFieldName.equals("FirstNameEn") ) {
            return 60;
        }
        if( strFieldName.equals("NameEn") ) {
            return 61;
        }
        if( strFieldName.equals("LastNamePY") ) {
            return 62;
        }
        if( strFieldName.equals("FirstNamePY") ) {
            return 63;
        }
        if( strFieldName.equals("Language") ) {
            return 64;
        }
        if( strFieldName.equals("SpecialPopFlag") ) {
            return 65;
        }
        if( strFieldName.equals("AffirmInputFlag") ) {
            return 66;
        }
        if( strFieldName.equals("YearIncome") ) {
            return 67;
        }
        if( strFieldName.equals("IncomeSource") ) {
            return 68;
        }
        if( strFieldName.equals("FamilyYearSalary") ) {
            return 69;
        }
        if( strFieldName.equals("DenType") ) {
            return 70;
        }
        if( strFieldName.equals("BudgetPrem") ) {
            return 71;
        }
        if( strFieldName.equals("TAXRESIDENTTYPE") ) {
            return 72;
        }
        if( strFieldName.equals("IDSTDATE") ) {
            return 73;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "AppntID";
                break;
            case 1:
                strFieldName = "ContID";
                break;
            case 2:
                strFieldName = "PersonID";
                break;
            case 3:
                strFieldName = "ShardingID";
                break;
            case 4:
                strFieldName = "GrpContNo";
                break;
            case 5:
                strFieldName = "ContNo";
                break;
            case 6:
                strFieldName = "PrtNo";
                break;
            case 7:
                strFieldName = "AppntNo";
                break;
            case 8:
                strFieldName = "AppntGrade";
                break;
            case 9:
                strFieldName = "AppntName";
                break;
            case 10:
                strFieldName = "AppntSex";
                break;
            case 11:
                strFieldName = "AppntBirthday";
                break;
            case 12:
                strFieldName = "AppntType";
                break;
            case 13:
                strFieldName = "AddressNo";
                break;
            case 14:
                strFieldName = "IDType";
                break;
            case 15:
                strFieldName = "IDNo";
                break;
            case 16:
                strFieldName = "NativePlace";
                break;
            case 17:
                strFieldName = "Nationality";
                break;
            case 18:
                strFieldName = "RgtAddress";
                break;
            case 19:
                strFieldName = "Marriage";
                break;
            case 20:
                strFieldName = "MarriageDate";
                break;
            case 21:
                strFieldName = "Health";
                break;
            case 22:
                strFieldName = "Stature";
                break;
            case 23:
                strFieldName = "Avoirdupois";
                break;
            case 24:
                strFieldName = "Degree";
                break;
            case 25:
                strFieldName = "CreditGrade";
                break;
            case 26:
                strFieldName = "BankCode";
                break;
            case 27:
                strFieldName = "BankAccNo";
                break;
            case 28:
                strFieldName = "AccName";
                break;
            case 29:
                strFieldName = "JoinCompanyDate";
                break;
            case 30:
                strFieldName = "StartWorkDate";
                break;
            case 31:
                strFieldName = "Position";
                break;
            case 32:
                strFieldName = "Salary";
                break;
            case 33:
                strFieldName = "OccupationType";
                break;
            case 34:
                strFieldName = "OccupationCode";
                break;
            case 35:
                strFieldName = "WorkType";
                break;
            case 36:
                strFieldName = "PluralityType";
                break;
            case 37:
                strFieldName = "SmokeFlag";
                break;
            case 38:
                strFieldName = "Operator";
                break;
            case 39:
                strFieldName = "ManageCom";
                break;
            case 40:
                strFieldName = "MakeDate";
                break;
            case 41:
                strFieldName = "MakeTime";
                break;
            case 42:
                strFieldName = "ModifyDate";
                break;
            case 43:
                strFieldName = "ModifyTime";
                break;
            case 44:
                strFieldName = "BMI";
                break;
            case 45:
                strFieldName = "License";
                break;
            case 46:
                strFieldName = "LicenseType";
                break;
            case 47:
                strFieldName = "RelatToInsu";
                break;
            case 48:
                strFieldName = "OccupationDesb";
                break;
            case 49:
                strFieldName = "IdValiDate";
                break;
            case 50:
                strFieldName = "HaveMotorcycleLicence";
                break;
            case 51:
                strFieldName = "PartTimeJob";
                break;
            case 52:
                strFieldName = "FirstName";
                break;
            case 53:
                strFieldName = "LastName";
                break;
            case 54:
                strFieldName = "CUSLevel";
                break;
            case 55:
                strFieldName = "AppRgtTpye";
                break;
            case 56:
                strFieldName = "TINNO";
                break;
            case 57:
                strFieldName = "TINFlag";
                break;
            case 58:
                strFieldName = "SocialInsuFlag";
                break;
            case 59:
                strFieldName = "LastNameEn";
                break;
            case 60:
                strFieldName = "FirstNameEn";
                break;
            case 61:
                strFieldName = "NameEn";
                break;
            case 62:
                strFieldName = "LastNamePY";
                break;
            case 63:
                strFieldName = "FirstNamePY";
                break;
            case 64:
                strFieldName = "Language";
                break;
            case 65:
                strFieldName = "SpecialPopFlag";
                break;
            case 66:
                strFieldName = "AffirmInputFlag";
                break;
            case 67:
                strFieldName = "YearIncome";
                break;
            case 68:
                strFieldName = "IncomeSource";
                break;
            case 69:
                strFieldName = "FamilyYearSalary";
                break;
            case 70:
                strFieldName = "DenType";
                break;
            case 71:
                strFieldName = "BudgetPrem";
                break;
            case 72:
                strFieldName = "TAXRESIDENTTYPE";
                break;
            case 73:
                strFieldName = "IDSTDATE";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "APPNTID":
                return Schema.TYPE_LONG;
            case "CONTID":
                return Schema.TYPE_LONG;
            case "PERSONID":
                return Schema.TYPE_LONG;
            case "SHARDINGID":
                return Schema.TYPE_STRING;
            case "GRPCONTNO":
                return Schema.TYPE_STRING;
            case "CONTNO":
                return Schema.TYPE_STRING;
            case "PRTNO":
                return Schema.TYPE_STRING;
            case "APPNTNO":
                return Schema.TYPE_STRING;
            case "APPNTGRADE":
                return Schema.TYPE_STRING;
            case "APPNTNAME":
                return Schema.TYPE_STRING;
            case "APPNTSEX":
                return Schema.TYPE_STRING;
            case "APPNTBIRTHDAY":
                return Schema.TYPE_STRING;
            case "APPNTTYPE":
                return Schema.TYPE_STRING;
            case "ADDRESSNO":
                return Schema.TYPE_STRING;
            case "IDTYPE":
                return Schema.TYPE_STRING;
            case "IDNO":
                return Schema.TYPE_STRING;
            case "NATIVEPLACE":
                return Schema.TYPE_STRING;
            case "NATIONALITY":
                return Schema.TYPE_STRING;
            case "RGTADDRESS":
                return Schema.TYPE_STRING;
            case "MARRIAGE":
                return Schema.TYPE_STRING;
            case "MARRIAGEDATE":
                return Schema.TYPE_STRING;
            case "HEALTH":
                return Schema.TYPE_STRING;
            case "STATURE":
                return Schema.TYPE_DOUBLE;
            case "AVOIRDUPOIS":
                return Schema.TYPE_DOUBLE;
            case "DEGREE":
                return Schema.TYPE_STRING;
            case "CREDITGRADE":
                return Schema.TYPE_STRING;
            case "BANKCODE":
                return Schema.TYPE_STRING;
            case "BANKACCNO":
                return Schema.TYPE_STRING;
            case "ACCNAME":
                return Schema.TYPE_STRING;
            case "JOINCOMPANYDATE":
                return Schema.TYPE_STRING;
            case "STARTWORKDATE":
                return Schema.TYPE_STRING;
            case "POSITION":
                return Schema.TYPE_STRING;
            case "SALARY":
                return Schema.TYPE_DOUBLE;
            case "OCCUPATIONTYPE":
                return Schema.TYPE_STRING;
            case "OCCUPATIONCODE":
                return Schema.TYPE_STRING;
            case "WORKTYPE":
                return Schema.TYPE_STRING;
            case "PLURALITYTYPE":
                return Schema.TYPE_STRING;
            case "SMOKEFLAG":
                return Schema.TYPE_STRING;
            case "OPERATOR":
                return Schema.TYPE_STRING;
            case "MANAGECOM":
                return Schema.TYPE_STRING;
            case "MAKEDATE":
                return Schema.TYPE_STRING;
            case "MAKETIME":
                return Schema.TYPE_STRING;
            case "MODIFYDATE":
                return Schema.TYPE_STRING;
            case "MODIFYTIME":
                return Schema.TYPE_STRING;
            case "BMI":
                return Schema.TYPE_DOUBLE;
            case "LICENSE":
                return Schema.TYPE_STRING;
            case "LICENSETYPE":
                return Schema.TYPE_STRING;
            case "RELATTOINSU":
                return Schema.TYPE_STRING;
            case "OCCUPATIONDESB":
                return Schema.TYPE_STRING;
            case "IDVALIDATE":
                return Schema.TYPE_STRING;
            case "HAVEMOTORCYCLELICENCE":
                return Schema.TYPE_STRING;
            case "PARTTIMEJOB":
                return Schema.TYPE_STRING;
            case "FIRSTNAME":
                return Schema.TYPE_STRING;
            case "LASTNAME":
                return Schema.TYPE_STRING;
            case "CUSLEVEL":
                return Schema.TYPE_STRING;
            case "APPRGTTPYE":
                return Schema.TYPE_STRING;
            case "TINNO":
                return Schema.TYPE_STRING;
            case "TINFLAG":
                return Schema.TYPE_STRING;
            case "SOCIALINSUFLAG":
                return Schema.TYPE_STRING;
            case "LASTNAMEEN":
                return Schema.TYPE_STRING;
            case "FIRSTNAMEEN":
                return Schema.TYPE_STRING;
            case "NAMEEN":
                return Schema.TYPE_STRING;
            case "LASTNAMEPY":
                return Schema.TYPE_STRING;
            case "FIRSTNAMEPY":
                return Schema.TYPE_STRING;
            case "LANGUAGE":
                return Schema.TYPE_STRING;
            case "SPECIALPOPFLAG":
                return Schema.TYPE_STRING;
            case "AFFIRMINPUTFLAG":
                return Schema.TYPE_STRING;
            case "YEARINCOME":
                return Schema.TYPE_DOUBLE;
            case "INCOMESOURCE":
                return Schema.TYPE_STRING;
            case "FAMILYYEARSALARY":
                return Schema.TYPE_DOUBLE;
            case "DENTYPE":
                return Schema.TYPE_STRING;
            case "BUDGETPREM":
                return Schema.TYPE_STRING;
            case "TAXRESIDENTTYPE":
                return Schema.TYPE_STRING;
            case "IDSTDATE":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_LONG;
            case 1:
                return Schema.TYPE_LONG;
            case 2:
                return Schema.TYPE_LONG;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_STRING;
            case 6:
                return Schema.TYPE_STRING;
            case 7:
                return Schema.TYPE_STRING;
            case 8:
                return Schema.TYPE_STRING;
            case 9:
                return Schema.TYPE_STRING;
            case 10:
                return Schema.TYPE_STRING;
            case 11:
                return Schema.TYPE_STRING;
            case 12:
                return Schema.TYPE_STRING;
            case 13:
                return Schema.TYPE_STRING;
            case 14:
                return Schema.TYPE_STRING;
            case 15:
                return Schema.TYPE_STRING;
            case 16:
                return Schema.TYPE_STRING;
            case 17:
                return Schema.TYPE_STRING;
            case 18:
                return Schema.TYPE_STRING;
            case 19:
                return Schema.TYPE_STRING;
            case 20:
                return Schema.TYPE_STRING;
            case 21:
                return Schema.TYPE_STRING;
            case 22:
                return Schema.TYPE_DOUBLE;
            case 23:
                return Schema.TYPE_DOUBLE;
            case 24:
                return Schema.TYPE_STRING;
            case 25:
                return Schema.TYPE_STRING;
            case 26:
                return Schema.TYPE_STRING;
            case 27:
                return Schema.TYPE_STRING;
            case 28:
                return Schema.TYPE_STRING;
            case 29:
                return Schema.TYPE_STRING;
            case 30:
                return Schema.TYPE_STRING;
            case 31:
                return Schema.TYPE_STRING;
            case 32:
                return Schema.TYPE_DOUBLE;
            case 33:
                return Schema.TYPE_STRING;
            case 34:
                return Schema.TYPE_STRING;
            case 35:
                return Schema.TYPE_STRING;
            case 36:
                return Schema.TYPE_STRING;
            case 37:
                return Schema.TYPE_STRING;
            case 38:
                return Schema.TYPE_STRING;
            case 39:
                return Schema.TYPE_STRING;
            case 40:
                return Schema.TYPE_STRING;
            case 41:
                return Schema.TYPE_STRING;
            case 42:
                return Schema.TYPE_STRING;
            case 43:
                return Schema.TYPE_STRING;
            case 44:
                return Schema.TYPE_DOUBLE;
            case 45:
                return Schema.TYPE_STRING;
            case 46:
                return Schema.TYPE_STRING;
            case 47:
                return Schema.TYPE_STRING;
            case 48:
                return Schema.TYPE_STRING;
            case 49:
                return Schema.TYPE_STRING;
            case 50:
                return Schema.TYPE_STRING;
            case 51:
                return Schema.TYPE_STRING;
            case 52:
                return Schema.TYPE_STRING;
            case 53:
                return Schema.TYPE_STRING;
            case 54:
                return Schema.TYPE_STRING;
            case 55:
                return Schema.TYPE_STRING;
            case 56:
                return Schema.TYPE_STRING;
            case 57:
                return Schema.TYPE_STRING;
            case 58:
                return Schema.TYPE_STRING;
            case 59:
                return Schema.TYPE_STRING;
            case 60:
                return Schema.TYPE_STRING;
            case 61:
                return Schema.TYPE_STRING;
            case 62:
                return Schema.TYPE_STRING;
            case 63:
                return Schema.TYPE_STRING;
            case 64:
                return Schema.TYPE_STRING;
            case 65:
                return Schema.TYPE_STRING;
            case 66:
                return Schema.TYPE_STRING;
            case 67:
                return Schema.TYPE_DOUBLE;
            case 68:
                return Schema.TYPE_STRING;
            case 69:
                return Schema.TYPE_DOUBLE;
            case 70:
                return Schema.TYPE_STRING;
            case 71:
                return Schema.TYPE_STRING;
            case 72:
                return Schema.TYPE_STRING;
            case 73:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("AppntID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AppntID));
        }
        if (FCode.equalsIgnoreCase("ContID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ContID));
        }
        if (FCode.equalsIgnoreCase("PersonID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PersonID));
        }
        if (FCode.equalsIgnoreCase("ShardingID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ShardingID));
        }
        if (FCode.equalsIgnoreCase("GrpContNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GrpContNo));
        }
        if (FCode.equalsIgnoreCase("ContNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ContNo));
        }
        if (FCode.equalsIgnoreCase("PrtNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PrtNo));
        }
        if (FCode.equalsIgnoreCase("AppntNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AppntNo));
        }
        if (FCode.equalsIgnoreCase("AppntGrade")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AppntGrade));
        }
        if (FCode.equalsIgnoreCase("AppntName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AppntName));
        }
        if (FCode.equalsIgnoreCase("AppntSex")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AppntSex));
        }
        if (FCode.equalsIgnoreCase("AppntBirthday")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AppntBirthday));
        }
        if (FCode.equalsIgnoreCase("AppntType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AppntType));
        }
        if (FCode.equalsIgnoreCase("AddressNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AddressNo));
        }
        if (FCode.equalsIgnoreCase("IDType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(IDType));
        }
        if (FCode.equalsIgnoreCase("IDNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(IDNo));
        }
        if (FCode.equalsIgnoreCase("NativePlace")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(NativePlace));
        }
        if (FCode.equalsIgnoreCase("Nationality")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Nationality));
        }
        if (FCode.equalsIgnoreCase("RgtAddress")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RgtAddress));
        }
        if (FCode.equalsIgnoreCase("Marriage")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Marriage));
        }
        if (FCode.equalsIgnoreCase("MarriageDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MarriageDate));
        }
        if (FCode.equalsIgnoreCase("Health")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Health));
        }
        if (FCode.equalsIgnoreCase("Stature")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Stature));
        }
        if (FCode.equalsIgnoreCase("Avoirdupois")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Avoirdupois));
        }
        if (FCode.equalsIgnoreCase("Degree")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Degree));
        }
        if (FCode.equalsIgnoreCase("CreditGrade")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CreditGrade));
        }
        if (FCode.equalsIgnoreCase("BankCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BankCode));
        }
        if (FCode.equalsIgnoreCase("BankAccNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BankAccNo));
        }
        if (FCode.equalsIgnoreCase("AccName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AccName));
        }
        if (FCode.equalsIgnoreCase("JoinCompanyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(JoinCompanyDate));
        }
        if (FCode.equalsIgnoreCase("StartWorkDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StartWorkDate));
        }
        if (FCode.equalsIgnoreCase("Position")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Position));
        }
        if (FCode.equalsIgnoreCase("Salary")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Salary));
        }
        if (FCode.equalsIgnoreCase("OccupationType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OccupationType));
        }
        if (FCode.equalsIgnoreCase("OccupationCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OccupationCode));
        }
        if (FCode.equalsIgnoreCase("WorkType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(WorkType));
        }
        if (FCode.equalsIgnoreCase("PluralityType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PluralityType));
        }
        if (FCode.equalsIgnoreCase("SmokeFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SmokeFlag));
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equalsIgnoreCase("ManageCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeDate));
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyDate));
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (FCode.equalsIgnoreCase("BMI")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BMI));
        }
        if (FCode.equalsIgnoreCase("License")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(License));
        }
        if (FCode.equalsIgnoreCase("LicenseType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(LicenseType));
        }
        if (FCode.equalsIgnoreCase("RelatToInsu")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RelatToInsu));
        }
        if (FCode.equalsIgnoreCase("OccupationDesb")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OccupationDesb));
        }
        if (FCode.equalsIgnoreCase("IdValiDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(IdValiDate));
        }
        if (FCode.equalsIgnoreCase("HaveMotorcycleLicence")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(HaveMotorcycleLicence));
        }
        if (FCode.equalsIgnoreCase("PartTimeJob")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PartTimeJob));
        }
        if (FCode.equalsIgnoreCase("FirstName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FirstName));
        }
        if (FCode.equalsIgnoreCase("LastName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(LastName));
        }
        if (FCode.equalsIgnoreCase("CUSLevel")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CUSLevel));
        }
        if (FCode.equalsIgnoreCase("AppRgtTpye")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AppRgtTpye));
        }
        if (FCode.equalsIgnoreCase("TINNO")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(TINNO));
        }
        if (FCode.equalsIgnoreCase("TINFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(TINFlag));
        }
        if (FCode.equalsIgnoreCase("SocialInsuFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SocialInsuFlag));
        }
        if (FCode.equalsIgnoreCase("LastNameEn")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(LastNameEn));
        }
        if (FCode.equalsIgnoreCase("FirstNameEn")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FirstNameEn));
        }
        if (FCode.equalsIgnoreCase("NameEn")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(NameEn));
        }
        if (FCode.equalsIgnoreCase("LastNamePY")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(LastNamePY));
        }
        if (FCode.equalsIgnoreCase("FirstNamePY")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FirstNamePY));
        }
        if (FCode.equalsIgnoreCase("Language")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Language));
        }
        if (FCode.equalsIgnoreCase("SpecialPopFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SpecialPopFlag));
        }
        if (FCode.equalsIgnoreCase("AffirmInputFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AffirmInputFlag));
        }
        if (FCode.equalsIgnoreCase("YearIncome")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(YearIncome));
        }
        if (FCode.equalsIgnoreCase("IncomeSource")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(IncomeSource));
        }
        if (FCode.equalsIgnoreCase("FamilyYearSalary")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FamilyYearSalary));
        }
        if (FCode.equalsIgnoreCase("DenType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DenType));
        }
        if (FCode.equalsIgnoreCase("BudgetPrem")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BudgetPrem));
        }
        if (FCode.equalsIgnoreCase("TAXRESIDENTTYPE")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(TAXRESIDENTTYPE));
        }
        if (FCode.equalsIgnoreCase("IDSTDATE")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(IDSTDATE));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = String.valueOf(AppntID);
                break;
            case 1:
                strFieldValue = String.valueOf(ContID);
                break;
            case 2:
                strFieldValue = String.valueOf(PersonID);
                break;
            case 3:
                strFieldValue = String.valueOf(ShardingID);
                break;
            case 4:
                strFieldValue = String.valueOf(GrpContNo);
                break;
            case 5:
                strFieldValue = String.valueOf(ContNo);
                break;
            case 6:
                strFieldValue = String.valueOf(PrtNo);
                break;
            case 7:
                strFieldValue = String.valueOf(AppntNo);
                break;
            case 8:
                strFieldValue = String.valueOf(AppntGrade);
                break;
            case 9:
                strFieldValue = String.valueOf(AppntName);
                break;
            case 10:
                strFieldValue = String.valueOf(AppntSex);
                break;
            case 11:
                strFieldValue = String.valueOf(AppntBirthday);
                break;
            case 12:
                strFieldValue = String.valueOf(AppntType);
                break;
            case 13:
                strFieldValue = String.valueOf(AddressNo);
                break;
            case 14:
                strFieldValue = String.valueOf(IDType);
                break;
            case 15:
                strFieldValue = String.valueOf(IDNo);
                break;
            case 16:
                strFieldValue = String.valueOf(NativePlace);
                break;
            case 17:
                strFieldValue = String.valueOf(Nationality);
                break;
            case 18:
                strFieldValue = String.valueOf(RgtAddress);
                break;
            case 19:
                strFieldValue = String.valueOf(Marriage);
                break;
            case 20:
                strFieldValue = String.valueOf(MarriageDate);
                break;
            case 21:
                strFieldValue = String.valueOf(Health);
                break;
            case 22:
                strFieldValue = String.valueOf(Stature);
                break;
            case 23:
                strFieldValue = String.valueOf(Avoirdupois);
                break;
            case 24:
                strFieldValue = String.valueOf(Degree);
                break;
            case 25:
                strFieldValue = String.valueOf(CreditGrade);
                break;
            case 26:
                strFieldValue = String.valueOf(BankCode);
                break;
            case 27:
                strFieldValue = String.valueOf(BankAccNo);
                break;
            case 28:
                strFieldValue = String.valueOf(AccName);
                break;
            case 29:
                strFieldValue = String.valueOf(JoinCompanyDate);
                break;
            case 30:
                strFieldValue = String.valueOf(StartWorkDate);
                break;
            case 31:
                strFieldValue = String.valueOf(Position);
                break;
            case 32:
                strFieldValue = String.valueOf(Salary);
                break;
            case 33:
                strFieldValue = String.valueOf(OccupationType);
                break;
            case 34:
                strFieldValue = String.valueOf(OccupationCode);
                break;
            case 35:
                strFieldValue = String.valueOf(WorkType);
                break;
            case 36:
                strFieldValue = String.valueOf(PluralityType);
                break;
            case 37:
                strFieldValue = String.valueOf(SmokeFlag);
                break;
            case 38:
                strFieldValue = String.valueOf(Operator);
                break;
            case 39:
                strFieldValue = String.valueOf(ManageCom);
                break;
            case 40:
                strFieldValue = String.valueOf(MakeDate);
                break;
            case 41:
                strFieldValue = String.valueOf(MakeTime);
                break;
            case 42:
                strFieldValue = String.valueOf(ModifyDate);
                break;
            case 43:
                strFieldValue = String.valueOf(ModifyTime);
                break;
            case 44:
                strFieldValue = String.valueOf(BMI);
                break;
            case 45:
                strFieldValue = String.valueOf(License);
                break;
            case 46:
                strFieldValue = String.valueOf(LicenseType);
                break;
            case 47:
                strFieldValue = String.valueOf(RelatToInsu);
                break;
            case 48:
                strFieldValue = String.valueOf(OccupationDesb);
                break;
            case 49:
                strFieldValue = String.valueOf(IdValiDate);
                break;
            case 50:
                strFieldValue = String.valueOf(HaveMotorcycleLicence);
                break;
            case 51:
                strFieldValue = String.valueOf(PartTimeJob);
                break;
            case 52:
                strFieldValue = String.valueOf(FirstName);
                break;
            case 53:
                strFieldValue = String.valueOf(LastName);
                break;
            case 54:
                strFieldValue = String.valueOf(CUSLevel);
                break;
            case 55:
                strFieldValue = String.valueOf(AppRgtTpye);
                break;
            case 56:
                strFieldValue = String.valueOf(TINNO);
                break;
            case 57:
                strFieldValue = String.valueOf(TINFlag);
                break;
            case 58:
                strFieldValue = String.valueOf(SocialInsuFlag);
                break;
            case 59:
                strFieldValue = String.valueOf(LastNameEn);
                break;
            case 60:
                strFieldValue = String.valueOf(FirstNameEn);
                break;
            case 61:
                strFieldValue = String.valueOf(NameEn);
                break;
            case 62:
                strFieldValue = String.valueOf(LastNamePY);
                break;
            case 63:
                strFieldValue = String.valueOf(FirstNamePY);
                break;
            case 64:
                strFieldValue = String.valueOf(Language);
                break;
            case 65:
                strFieldValue = String.valueOf(SpecialPopFlag);
                break;
            case 66:
                strFieldValue = String.valueOf(AffirmInputFlag);
                break;
            case 67:
                strFieldValue = String.valueOf(YearIncome);
                break;
            case 68:
                strFieldValue = String.valueOf(IncomeSource);
                break;
            case 69:
                strFieldValue = String.valueOf(FamilyYearSalary);
                break;
            case 70:
                strFieldValue = String.valueOf(DenType);
                break;
            case 71:
                strFieldValue = String.valueOf(BudgetPrem);
                break;
            case 72:
                strFieldValue = String.valueOf(TAXRESIDENTTYPE);
                break;
            case 73:
                strFieldValue = String.valueOf(IDSTDATE);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("AppntID")) {
            if( FValue != null && !FValue.equals("")) {
                AppntID = new Long(FValue).longValue();
            }
        }
        if (FCode.equalsIgnoreCase("ContID")) {
            if( FValue != null && !FValue.equals("")) {
                ContID = new Long(FValue).longValue();
            }
        }
        if (FCode.equalsIgnoreCase("PersonID")) {
            if( FValue != null && !FValue.equals("")) {
                PersonID = new Long(FValue).longValue();
            }
        }
        if (FCode.equalsIgnoreCase("ShardingID")) {
            if( FValue != null && !FValue.equals(""))
            {
                ShardingID = FValue.trim();
            }
            else
                ShardingID = null;
        }
        if (FCode.equalsIgnoreCase("GrpContNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                GrpContNo = FValue.trim();
            }
            else
                GrpContNo = null;
        }
        if (FCode.equalsIgnoreCase("ContNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                ContNo = FValue.trim();
            }
            else
                ContNo = null;
        }
        if (FCode.equalsIgnoreCase("PrtNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                PrtNo = FValue.trim();
            }
            else
                PrtNo = null;
        }
        if (FCode.equalsIgnoreCase("AppntNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                AppntNo = FValue.trim();
            }
            else
                AppntNo = null;
        }
        if (FCode.equalsIgnoreCase("AppntGrade")) {
            if( FValue != null && !FValue.equals(""))
            {
                AppntGrade = FValue.trim();
            }
            else
                AppntGrade = null;
        }
        if (FCode.equalsIgnoreCase("AppntName")) {
            if( FValue != null && !FValue.equals(""))
            {
                AppntName = FValue.trim();
            }
            else
                AppntName = null;
        }
        if (FCode.equalsIgnoreCase("AppntSex")) {
            if( FValue != null && !FValue.equals(""))
            {
                AppntSex = FValue.trim();
            }
            else
                AppntSex = null;
        }
        if (FCode.equalsIgnoreCase("AppntBirthday")) {
            if( FValue != null && !FValue.equals(""))
            {
                AppntBirthday = FValue.trim();
            }
            else
                AppntBirthday = null;
        }
        if (FCode.equalsIgnoreCase("AppntType")) {
            if( FValue != null && !FValue.equals(""))
            {
                AppntType = FValue.trim();
            }
            else
                AppntType = null;
        }
        if (FCode.equalsIgnoreCase("AddressNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                AddressNo = FValue.trim();
            }
            else
                AddressNo = null;
        }
        if (FCode.equalsIgnoreCase("IDType")) {
            if( FValue != null && !FValue.equals(""))
            {
                IDType = FValue.trim();
            }
            else
                IDType = null;
        }
        if (FCode.equalsIgnoreCase("IDNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                IDNo = FValue.trim();
            }
            else
                IDNo = null;
        }
        if (FCode.equalsIgnoreCase("NativePlace")) {
            if( FValue != null && !FValue.equals(""))
            {
                NativePlace = FValue.trim();
            }
            else
                NativePlace = null;
        }
        if (FCode.equalsIgnoreCase("Nationality")) {
            if( FValue != null && !FValue.equals(""))
            {
                Nationality = FValue.trim();
            }
            else
                Nationality = null;
        }
        if (FCode.equalsIgnoreCase("RgtAddress")) {
            if( FValue != null && !FValue.equals(""))
            {
                RgtAddress = FValue.trim();
            }
            else
                RgtAddress = null;
        }
        if (FCode.equalsIgnoreCase("Marriage")) {
            if( FValue != null && !FValue.equals(""))
            {
                Marriage = FValue.trim();
            }
            else
                Marriage = null;
        }
        if (FCode.equalsIgnoreCase("MarriageDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                MarriageDate = FValue.trim();
            }
            else
                MarriageDate = null;
        }
        if (FCode.equalsIgnoreCase("Health")) {
            if( FValue != null && !FValue.equals(""))
            {
                Health = FValue.trim();
            }
            else
                Health = null;
        }
        if (FCode.equalsIgnoreCase("Stature")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                Stature = d;
            }
        }
        if (FCode.equalsIgnoreCase("Avoirdupois")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                Avoirdupois = d;
            }
        }
        if (FCode.equalsIgnoreCase("Degree")) {
            if( FValue != null && !FValue.equals(""))
            {
                Degree = FValue.trim();
            }
            else
                Degree = null;
        }
        if (FCode.equalsIgnoreCase("CreditGrade")) {
            if( FValue != null && !FValue.equals(""))
            {
                CreditGrade = FValue.trim();
            }
            else
                CreditGrade = null;
        }
        if (FCode.equalsIgnoreCase("BankCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                BankCode = FValue.trim();
            }
            else
                BankCode = null;
        }
        if (FCode.equalsIgnoreCase("BankAccNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                BankAccNo = FValue.trim();
            }
            else
                BankAccNo = null;
        }
        if (FCode.equalsIgnoreCase("AccName")) {
            if( FValue != null && !FValue.equals(""))
            {
                AccName = FValue.trim();
            }
            else
                AccName = null;
        }
        if (FCode.equalsIgnoreCase("JoinCompanyDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                JoinCompanyDate = FValue.trim();
            }
            else
                JoinCompanyDate = null;
        }
        if (FCode.equalsIgnoreCase("StartWorkDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                StartWorkDate = FValue.trim();
            }
            else
                StartWorkDate = null;
        }
        if (FCode.equalsIgnoreCase("Position")) {
            if( FValue != null && !FValue.equals(""))
            {
                Position = FValue.trim();
            }
            else
                Position = null;
        }
        if (FCode.equalsIgnoreCase("Salary")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                Salary = d;
            }
        }
        if (FCode.equalsIgnoreCase("OccupationType")) {
            if( FValue != null && !FValue.equals(""))
            {
                OccupationType = FValue.trim();
            }
            else
                OccupationType = null;
        }
        if (FCode.equalsIgnoreCase("OccupationCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                OccupationCode = FValue.trim();
            }
            else
                OccupationCode = null;
        }
        if (FCode.equalsIgnoreCase("WorkType")) {
            if( FValue != null && !FValue.equals(""))
            {
                WorkType = FValue.trim();
            }
            else
                WorkType = null;
        }
        if (FCode.equalsIgnoreCase("PluralityType")) {
            if( FValue != null && !FValue.equals(""))
            {
                PluralityType = FValue.trim();
            }
            else
                PluralityType = null;
        }
        if (FCode.equalsIgnoreCase("SmokeFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                SmokeFlag = FValue.trim();
            }
            else
                SmokeFlag = null;
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            if( FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
                Operator = null;
        }
        if (FCode.equalsIgnoreCase("ManageCom")) {
            if( FValue != null && !FValue.equals(""))
            {
                ManageCom = FValue.trim();
            }
            else
                ManageCom = null;
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeDate = FValue.trim();
            }
            else
                MakeDate = null;
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
                MakeTime = null;
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyDate = FValue.trim();
            }
            else
                ModifyDate = null;
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
                ModifyTime = null;
        }
        if (FCode.equalsIgnoreCase("BMI")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                BMI = d;
            }
        }
        if (FCode.equalsIgnoreCase("License")) {
            if( FValue != null && !FValue.equals(""))
            {
                License = FValue.trim();
            }
            else
                License = null;
        }
        if (FCode.equalsIgnoreCase("LicenseType")) {
            if( FValue != null && !FValue.equals(""))
            {
                LicenseType = FValue.trim();
            }
            else
                LicenseType = null;
        }
        if (FCode.equalsIgnoreCase("RelatToInsu")) {
            if( FValue != null && !FValue.equals(""))
            {
                RelatToInsu = FValue.trim();
            }
            else
                RelatToInsu = null;
        }
        if (FCode.equalsIgnoreCase("OccupationDesb")) {
            if( FValue != null && !FValue.equals(""))
            {
                OccupationDesb = FValue.trim();
            }
            else
                OccupationDesb = null;
        }
        if (FCode.equalsIgnoreCase("IdValiDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                IdValiDate = FValue.trim();
            }
            else
                IdValiDate = null;
        }
        if (FCode.equalsIgnoreCase("HaveMotorcycleLicence")) {
            if( FValue != null && !FValue.equals(""))
            {
                HaveMotorcycleLicence = FValue.trim();
            }
            else
                HaveMotorcycleLicence = null;
        }
        if (FCode.equalsIgnoreCase("PartTimeJob")) {
            if( FValue != null && !FValue.equals(""))
            {
                PartTimeJob = FValue.trim();
            }
            else
                PartTimeJob = null;
        }
        if (FCode.equalsIgnoreCase("FirstName")) {
            if( FValue != null && !FValue.equals(""))
            {
                FirstName = FValue.trim();
            }
            else
                FirstName = null;
        }
        if (FCode.equalsIgnoreCase("LastName")) {
            if( FValue != null && !FValue.equals(""))
            {
                LastName = FValue.trim();
            }
            else
                LastName = null;
        }
        if (FCode.equalsIgnoreCase("CUSLevel")) {
            if( FValue != null && !FValue.equals(""))
            {
                CUSLevel = FValue.trim();
            }
            else
                CUSLevel = null;
        }
        if (FCode.equalsIgnoreCase("AppRgtTpye")) {
            if( FValue != null && !FValue.equals(""))
            {
                AppRgtTpye = FValue.trim();
            }
            else
                AppRgtTpye = null;
        }
        if (FCode.equalsIgnoreCase("TINNO")) {
            if( FValue != null && !FValue.equals(""))
            {
                TINNO = FValue.trim();
            }
            else
                TINNO = null;
        }
        if (FCode.equalsIgnoreCase("TINFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                TINFlag = FValue.trim();
            }
            else
                TINFlag = null;
        }
        if (FCode.equalsIgnoreCase("SocialInsuFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                SocialInsuFlag = FValue.trim();
            }
            else
                SocialInsuFlag = null;
        }
        if (FCode.equalsIgnoreCase("LastNameEn")) {
            if( FValue != null && !FValue.equals(""))
            {
                LastNameEn = FValue.trim();
            }
            else
                LastNameEn = null;
        }
        if (FCode.equalsIgnoreCase("FirstNameEn")) {
            if( FValue != null && !FValue.equals(""))
            {
                FirstNameEn = FValue.trim();
            }
            else
                FirstNameEn = null;
        }
        if (FCode.equalsIgnoreCase("NameEn")) {
            if( FValue != null && !FValue.equals(""))
            {
                NameEn = FValue.trim();
            }
            else
                NameEn = null;
        }
        if (FCode.equalsIgnoreCase("LastNamePY")) {
            if( FValue != null && !FValue.equals(""))
            {
                LastNamePY = FValue.trim();
            }
            else
                LastNamePY = null;
        }
        if (FCode.equalsIgnoreCase("FirstNamePY")) {
            if( FValue != null && !FValue.equals(""))
            {
                FirstNamePY = FValue.trim();
            }
            else
                FirstNamePY = null;
        }
        if (FCode.equalsIgnoreCase("Language")) {
            if( FValue != null && !FValue.equals(""))
            {
                Language = FValue.trim();
            }
            else
                Language = null;
        }
        if (FCode.equalsIgnoreCase("SpecialPopFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                SpecialPopFlag = FValue.trim();
            }
            else
                SpecialPopFlag = null;
        }
        if (FCode.equalsIgnoreCase("AffirmInputFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                AffirmInputFlag = FValue.trim();
            }
            else
                AffirmInputFlag = null;
        }
        if (FCode.equalsIgnoreCase("YearIncome")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                YearIncome = d;
            }
        }
        if (FCode.equalsIgnoreCase("IncomeSource")) {
            if( FValue != null && !FValue.equals(""))
            {
                IncomeSource = FValue.trim();
            }
            else
                IncomeSource = null;
        }
        if (FCode.equalsIgnoreCase("FamilyYearSalary")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                FamilyYearSalary = d;
            }
        }
        if (FCode.equalsIgnoreCase("DenType")) {
            if( FValue != null && !FValue.equals(""))
            {
                DenType = FValue.trim();
            }
            else
                DenType = null;
        }
        if (FCode.equalsIgnoreCase("BudgetPrem")) {
            if( FValue != null && !FValue.equals(""))
            {
                BudgetPrem = FValue.trim();
            }
            else
                BudgetPrem = null;
        }
        if (FCode.equalsIgnoreCase("TAXRESIDENTTYPE")) {
            if( FValue != null && !FValue.equals(""))
            {
                TAXRESIDENTTYPE = FValue.trim();
            }
            else
                TAXRESIDENTTYPE = null;
        }
        if (FCode.equalsIgnoreCase("IDSTDATE")) {
            if( FValue != null && !FValue.equals(""))
            {
                IDSTDATE = FValue.trim();
            }
            else
                IDSTDATE = null;
        }
        return true;
    }


    public String toString() {
    return "LCAppntPojo [" +
            "AppntID="+AppntID +
            ", ContID="+ContID +
            ", PersonID="+PersonID +
            ", ShardingID="+ShardingID +
            ", GrpContNo="+GrpContNo +
            ", ContNo="+ContNo +
            ", PrtNo="+PrtNo +
            ", AppntNo="+AppntNo +
            ", AppntGrade="+AppntGrade +
            ", AppntName="+AppntName +
            ", AppntSex="+AppntSex +
            ", AppntBirthday="+AppntBirthday +
            ", AppntType="+AppntType +
            ", AddressNo="+AddressNo +
            ", IDType="+IDType +
            ", IDNo="+IDNo +
            ", NativePlace="+NativePlace +
            ", Nationality="+Nationality +
            ", RgtAddress="+RgtAddress +
            ", Marriage="+Marriage +
            ", MarriageDate="+MarriageDate +
            ", Health="+Health +
            ", Stature="+Stature +
            ", Avoirdupois="+Avoirdupois +
            ", Degree="+Degree +
            ", CreditGrade="+CreditGrade +
            ", BankCode="+BankCode +
            ", BankAccNo="+BankAccNo +
            ", AccName="+AccName +
            ", JoinCompanyDate="+JoinCompanyDate +
            ", StartWorkDate="+StartWorkDate +
            ", Position="+Position +
            ", Salary="+Salary +
            ", OccupationType="+OccupationType +
            ", OccupationCode="+OccupationCode +
            ", WorkType="+WorkType +
            ", PluralityType="+PluralityType +
            ", SmokeFlag="+SmokeFlag +
            ", Operator="+Operator +
            ", ManageCom="+ManageCom +
            ", MakeDate="+MakeDate +
            ", MakeTime="+MakeTime +
            ", ModifyDate="+ModifyDate +
            ", ModifyTime="+ModifyTime +
            ", BMI="+BMI +
            ", License="+License +
            ", LicenseType="+LicenseType +
            ", RelatToInsu="+RelatToInsu +
            ", OccupationDesb="+OccupationDesb +
            ", IdValiDate="+IdValiDate +
            ", HaveMotorcycleLicence="+HaveMotorcycleLicence +
            ", PartTimeJob="+PartTimeJob +
            ", FirstName="+FirstName +
            ", LastName="+LastName +
            ", CUSLevel="+CUSLevel +
            ", AppRgtTpye="+AppRgtTpye +
            ", TINNO="+TINNO +
            ", TINFlag="+TINFlag +
            ", SocialInsuFlag="+SocialInsuFlag +
            ", LastNameEn="+LastNameEn +
            ", FirstNameEn="+FirstNameEn +
            ", NameEn="+NameEn +
            ", LastNamePY="+LastNamePY +
            ", FirstNamePY="+FirstNamePY +
            ", Language="+Language +
            ", SpecialPopFlag="+SpecialPopFlag +
            ", AffirmInputFlag="+AffirmInputFlag +
            ", YearIncome="+YearIncome +
            ", IncomeSource="+IncomeSource +
            ", FamilyYearSalary="+FamilyYearSalary +
            ", DenType="+DenType +
            ", BudgetPrem="+BudgetPrem +
            ", TAXRESIDENTTYPE="+TAXRESIDENTTYPE +
            ", IDSTDATE="+IDSTDATE +"]";
    }
}
