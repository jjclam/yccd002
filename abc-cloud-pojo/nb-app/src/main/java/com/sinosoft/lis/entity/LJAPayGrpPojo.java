/**
 * Copyright (c) 2018 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.entity;

import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.Pojo;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;

import java.io.Serializable;

/**
 * <p>ClassName: LJAPayGrpPojo </p>
 * <p>Copyright: Copyright (c) 2018</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2018-06-19
 */
public class LJAPayGrpPojo implements Pojo,Serializable {
    // @Field
    /** 集体保单号码 */
    private String GrpPolNo; 
    /** 第几次交费 */
    private int PayCount; 
    /** 集体合同号码 */
    private String GrpContNo; 
    /** 管理机构 */
    private String ManageCom; 
    /** 代理机构 */
    private String AgentCom; 
    /** 代理机构内部分类 */
    private String AgentType; 
    /** 险种编码 */
    private String RiskCode; 
    /** 代理人编码 */
    private String AgentCode; 
    /** 代理人组别 */
    private String AgentGroup; 
    /** 续保收费标记 */
    private String PayTypeFlag; 
    /** 投保人客户号码 */
    private String AppntNo; 
    /** 交费收据号码 */
    private String PayNo; 
    /** 批单号码 */
    private String EndorsementNo; 
    /** 总应交金额 */
    private double SumDuePayMoney; 
    /** 总实交金额 */
    private double SumActuPayMoney; 
    /** 交费间隔 */
    private int PayIntv; 
    /** 交费日期 */
    private String  PayDate;
    /** 交费类型 */
    private String PayType; 
    /** 到帐日期 */
    private String  EnterAccDate;
    /** 确认日期 */
    private String  ConfDate;
    /** 原交至日期 */
    private String  LastPayToDate;
    /** 现交至日期 */
    private String  CurPayToDate;
    /** 复核人编码 */
    private String ApproveCode; 
    /** 复核日期 */
    private String  ApproveDate;
    /** 复核时间 */
    private String ApproveTime; 
    /** 流水号 */
    private String SerialNo; 
    /** 通知书号码 */
    private String GetNoticeNo; 
    /** 操作员 */
    private String Operator; 
    /** 入机日期 */
    private String  MakeDate;
    /** 入机时间 */
    private String MakeTime; 
    /** 最后一次修改日期 */
    private String  ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime; 
    /** 实收管理费 */
    private double FeeMoney; 
    /** 业务状态 */
    private String OperState; 
    /** 币别 */
    private String Currency; 
    /** 业务类型 */
    private String BussType; 
    /** 公司代码 */
    private String ComCode; 
    /** 最后一次修改操作员 */
    private String ModifyOperator; 
    /** 净费用 */
    private double NetAmount; 
    /** 税额 */
    private double TaxAmount; 
    /** 税率 */
    private double Tax; 
    /** 保单年度 */
    private String PolicyYear; 
    /** 交费次序 */
    private String OrderPay; 


    public static final int FIELDNUM = 43;    // 数据库表的字段个数

    private FDate fDate = new FDate();        // 处理日期
    public String getGrpPolNo() {
        return GrpPolNo;
    }
    public void setGrpPolNo(String aGrpPolNo) {
        GrpPolNo = aGrpPolNo;
    }
    public int getPayCount() {
        return PayCount;
    }
    public void setPayCount(int aPayCount) {
        PayCount = aPayCount;
    }
    public void setPayCount(String aPayCount) {
        if (aPayCount != null && !aPayCount.equals("")) {
            Integer tInteger = new Integer(aPayCount);
            int i = tInteger.intValue();
            PayCount = i;
        }
    }

    public String getGrpContNo() {
        return GrpContNo;
    }
    public void setGrpContNo(String aGrpContNo) {
        GrpContNo = aGrpContNo;
    }
    public String getManageCom() {
        return ManageCom;
    }
    public void setManageCom(String aManageCom) {
        ManageCom = aManageCom;
    }
    public String getAgentCom() {
        return AgentCom;
    }
    public void setAgentCom(String aAgentCom) {
        AgentCom = aAgentCom;
    }
    public String getAgentType() {
        return AgentType;
    }
    public void setAgentType(String aAgentType) {
        AgentType = aAgentType;
    }
    public String getRiskCode() {
        return RiskCode;
    }
    public void setRiskCode(String aRiskCode) {
        RiskCode = aRiskCode;
    }
    public String getAgentCode() {
        return AgentCode;
    }
    public void setAgentCode(String aAgentCode) {
        AgentCode = aAgentCode;
    }
    public String getAgentGroup() {
        return AgentGroup;
    }
    public void setAgentGroup(String aAgentGroup) {
        AgentGroup = aAgentGroup;
    }
    public String getPayTypeFlag() {
        return PayTypeFlag;
    }
    public void setPayTypeFlag(String aPayTypeFlag) {
        PayTypeFlag = aPayTypeFlag;
    }
    public String getAppntNo() {
        return AppntNo;
    }
    public void setAppntNo(String aAppntNo) {
        AppntNo = aAppntNo;
    }
    public String getPayNo() {
        return PayNo;
    }
    public void setPayNo(String aPayNo) {
        PayNo = aPayNo;
    }
    public String getEndorsementNo() {
        return EndorsementNo;
    }
    public void setEndorsementNo(String aEndorsementNo) {
        EndorsementNo = aEndorsementNo;
    }
    public double getSumDuePayMoney() {
        return SumDuePayMoney;
    }
    public void setSumDuePayMoney(double aSumDuePayMoney) {
        SumDuePayMoney = aSumDuePayMoney;
    }
    public void setSumDuePayMoney(String aSumDuePayMoney) {
        if (aSumDuePayMoney != null && !aSumDuePayMoney.equals("")) {
            Double tDouble = new Double(aSumDuePayMoney);
            double d = tDouble.doubleValue();
            SumDuePayMoney = d;
        }
    }

    public double getSumActuPayMoney() {
        return SumActuPayMoney;
    }
    public void setSumActuPayMoney(double aSumActuPayMoney) {
        SumActuPayMoney = aSumActuPayMoney;
    }
    public void setSumActuPayMoney(String aSumActuPayMoney) {
        if (aSumActuPayMoney != null && !aSumActuPayMoney.equals("")) {
            Double tDouble = new Double(aSumActuPayMoney);
            double d = tDouble.doubleValue();
            SumActuPayMoney = d;
        }
    }

    public int getPayIntv() {
        return PayIntv;
    }
    public void setPayIntv(int aPayIntv) {
        PayIntv = aPayIntv;
    }
    public void setPayIntv(String aPayIntv) {
        if (aPayIntv != null && !aPayIntv.equals("")) {
            Integer tInteger = new Integer(aPayIntv);
            int i = tInteger.intValue();
            PayIntv = i;
        }
    }

    public String getPayDate() {
        return PayDate;
    }
    public void setPayDate(String aPayDate) {
        PayDate = aPayDate;
    }
    public String getPayType() {
        return PayType;
    }
    public void setPayType(String aPayType) {
        PayType = aPayType;
    }
    public String getEnterAccDate() {
        return EnterAccDate;
    }
    public void setEnterAccDate(String aEnterAccDate) {
        EnterAccDate = aEnterAccDate;
    }
    public String getConfDate() {
        return ConfDate;
    }
    public void setConfDate(String aConfDate) {
        ConfDate = aConfDate;
    }
    public String getLastPayToDate() {
        return LastPayToDate;
    }
    public void setLastPayToDate(String aLastPayToDate) {
        LastPayToDate = aLastPayToDate;
    }
    public String getCurPayToDate() {
        return CurPayToDate;
    }
    public void setCurPayToDate(String aCurPayToDate) {
        CurPayToDate = aCurPayToDate;
    }
    public String getApproveCode() {
        return ApproveCode;
    }
    public void setApproveCode(String aApproveCode) {
        ApproveCode = aApproveCode;
    }
    public String getApproveDate() {
        return ApproveDate;
    }
    public void setApproveDate(String aApproveDate) {
        ApproveDate = aApproveDate;
    }
    public String getApproveTime() {
        return ApproveTime;
    }
    public void setApproveTime(String aApproveTime) {
        ApproveTime = aApproveTime;
    }
    public String getSerialNo() {
        return SerialNo;
    }
    public void setSerialNo(String aSerialNo) {
        SerialNo = aSerialNo;
    }
    public String getGetNoticeNo() {
        return GetNoticeNo;
    }
    public void setGetNoticeNo(String aGetNoticeNo) {
        GetNoticeNo = aGetNoticeNo;
    }
    public String getOperator() {
        return Operator;
    }
    public void setOperator(String aOperator) {
        Operator = aOperator;
    }
    public String getMakeDate() {
        return MakeDate;
    }
    public void setMakeDate(String aMakeDate) {
        MakeDate = aMakeDate;
    }
    public String getMakeTime() {
        return MakeTime;
    }
    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }
    public String getModifyDate() {
        return ModifyDate;
    }
    public void setModifyDate(String aModifyDate) {
        ModifyDate = aModifyDate;
    }
    public String getModifyTime() {
        return ModifyTime;
    }
    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }
    public double getFeeMoney() {
        return FeeMoney;
    }
    public void setFeeMoney(double aFeeMoney) {
        FeeMoney = aFeeMoney;
    }
    public void setFeeMoney(String aFeeMoney) {
        if (aFeeMoney != null && !aFeeMoney.equals("")) {
            Double tDouble = new Double(aFeeMoney);
            double d = tDouble.doubleValue();
            FeeMoney = d;
        }
    }

    public String getOperState() {
        return OperState;
    }
    public void setOperState(String aOperState) {
        OperState = aOperState;
    }
    public String getCurrency() {
        return Currency;
    }
    public void setCurrency(String aCurrency) {
        Currency = aCurrency;
    }
    public String getBussType() {
        return BussType;
    }
    public void setBussType(String aBussType) {
        BussType = aBussType;
    }
    public String getComCode() {
        return ComCode;
    }
    public void setComCode(String aComCode) {
        ComCode = aComCode;
    }
    public String getModifyOperator() {
        return ModifyOperator;
    }
    public void setModifyOperator(String aModifyOperator) {
        ModifyOperator = aModifyOperator;
    }
    public double getNetAmount() {
        return NetAmount;
    }
    public void setNetAmount(double aNetAmount) {
        NetAmount = aNetAmount;
    }
    public void setNetAmount(String aNetAmount) {
        if (aNetAmount != null && !aNetAmount.equals("")) {
            Double tDouble = new Double(aNetAmount);
            double d = tDouble.doubleValue();
            NetAmount = d;
        }
    }

    public double getTaxAmount() {
        return TaxAmount;
    }
    public void setTaxAmount(double aTaxAmount) {
        TaxAmount = aTaxAmount;
    }
    public void setTaxAmount(String aTaxAmount) {
        if (aTaxAmount != null && !aTaxAmount.equals("")) {
            Double tDouble = new Double(aTaxAmount);
            double d = tDouble.doubleValue();
            TaxAmount = d;
        }
    }

    public double getTax() {
        return Tax;
    }
    public void setTax(double aTax) {
        Tax = aTax;
    }
    public void setTax(String aTax) {
        if (aTax != null && !aTax.equals("")) {
            Double tDouble = new Double(aTax);
            double d = tDouble.doubleValue();
            Tax = d;
        }
    }

    public String getPolicyYear() {
        return PolicyYear;
    }
    public void setPolicyYear(String aPolicyYear) {
        PolicyYear = aPolicyYear;
    }
    public String getOrderPay() {
        return OrderPay;
    }
    public void setOrderPay(String aOrderPay) {
        OrderPay = aOrderPay;
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("GrpPolNo") ) {
            return 0;
        }
        if( strFieldName.equals("PayCount") ) {
            return 1;
        }
        if( strFieldName.equals("GrpContNo") ) {
            return 2;
        }
        if( strFieldName.equals("ManageCom") ) {
            return 3;
        }
        if( strFieldName.equals("AgentCom") ) {
            return 4;
        }
        if( strFieldName.equals("AgentType") ) {
            return 5;
        }
        if( strFieldName.equals("RiskCode") ) {
            return 6;
        }
        if( strFieldName.equals("AgentCode") ) {
            return 7;
        }
        if( strFieldName.equals("AgentGroup") ) {
            return 8;
        }
        if( strFieldName.equals("PayTypeFlag") ) {
            return 9;
        }
        if( strFieldName.equals("AppntNo") ) {
            return 10;
        }
        if( strFieldName.equals("PayNo") ) {
            return 11;
        }
        if( strFieldName.equals("EndorsementNo") ) {
            return 12;
        }
        if( strFieldName.equals("SumDuePayMoney") ) {
            return 13;
        }
        if( strFieldName.equals("SumActuPayMoney") ) {
            return 14;
        }
        if( strFieldName.equals("PayIntv") ) {
            return 15;
        }
        if( strFieldName.equals("PayDate") ) {
            return 16;
        }
        if( strFieldName.equals("PayType") ) {
            return 17;
        }
        if( strFieldName.equals("EnterAccDate") ) {
            return 18;
        }
        if( strFieldName.equals("ConfDate") ) {
            return 19;
        }
        if( strFieldName.equals("LastPayToDate") ) {
            return 20;
        }
        if( strFieldName.equals("CurPayToDate") ) {
            return 21;
        }
        if( strFieldName.equals("ApproveCode") ) {
            return 22;
        }
        if( strFieldName.equals("ApproveDate") ) {
            return 23;
        }
        if( strFieldName.equals("ApproveTime") ) {
            return 24;
        }
        if( strFieldName.equals("SerialNo") ) {
            return 25;
        }
        if( strFieldName.equals("GetNoticeNo") ) {
            return 26;
        }
        if( strFieldName.equals("Operator") ) {
            return 27;
        }
        if( strFieldName.equals("MakeDate") ) {
            return 28;
        }
        if( strFieldName.equals("MakeTime") ) {
            return 29;
        }
        if( strFieldName.equals("ModifyDate") ) {
            return 30;
        }
        if( strFieldName.equals("ModifyTime") ) {
            return 31;
        }
        if( strFieldName.equals("FeeMoney") ) {
            return 32;
        }
        if( strFieldName.equals("OperState") ) {
            return 33;
        }
        if( strFieldName.equals("Currency") ) {
            return 34;
        }
        if( strFieldName.equals("BussType") ) {
            return 35;
        }
        if( strFieldName.equals("ComCode") ) {
            return 36;
        }
        if( strFieldName.equals("ModifyOperator") ) {
            return 37;
        }
        if( strFieldName.equals("NetAmount") ) {
            return 38;
        }
        if( strFieldName.equals("TaxAmount") ) {
            return 39;
        }
        if( strFieldName.equals("Tax") ) {
            return 40;
        }
        if( strFieldName.equals("PolicyYear") ) {
            return 41;
        }
        if( strFieldName.equals("OrderPay") ) {
            return 42;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "GrpPolNo";
                break;
            case 1:
                strFieldName = "PayCount";
                break;
            case 2:
                strFieldName = "GrpContNo";
                break;
            case 3:
                strFieldName = "ManageCom";
                break;
            case 4:
                strFieldName = "AgentCom";
                break;
            case 5:
                strFieldName = "AgentType";
                break;
            case 6:
                strFieldName = "RiskCode";
                break;
            case 7:
                strFieldName = "AgentCode";
                break;
            case 8:
                strFieldName = "AgentGroup";
                break;
            case 9:
                strFieldName = "PayTypeFlag";
                break;
            case 10:
                strFieldName = "AppntNo";
                break;
            case 11:
                strFieldName = "PayNo";
                break;
            case 12:
                strFieldName = "EndorsementNo";
                break;
            case 13:
                strFieldName = "SumDuePayMoney";
                break;
            case 14:
                strFieldName = "SumActuPayMoney";
                break;
            case 15:
                strFieldName = "PayIntv";
                break;
            case 16:
                strFieldName = "PayDate";
                break;
            case 17:
                strFieldName = "PayType";
                break;
            case 18:
                strFieldName = "EnterAccDate";
                break;
            case 19:
                strFieldName = "ConfDate";
                break;
            case 20:
                strFieldName = "LastPayToDate";
                break;
            case 21:
                strFieldName = "CurPayToDate";
                break;
            case 22:
                strFieldName = "ApproveCode";
                break;
            case 23:
                strFieldName = "ApproveDate";
                break;
            case 24:
                strFieldName = "ApproveTime";
                break;
            case 25:
                strFieldName = "SerialNo";
                break;
            case 26:
                strFieldName = "GetNoticeNo";
                break;
            case 27:
                strFieldName = "Operator";
                break;
            case 28:
                strFieldName = "MakeDate";
                break;
            case 29:
                strFieldName = "MakeTime";
                break;
            case 30:
                strFieldName = "ModifyDate";
                break;
            case 31:
                strFieldName = "ModifyTime";
                break;
            case 32:
                strFieldName = "FeeMoney";
                break;
            case 33:
                strFieldName = "OperState";
                break;
            case 34:
                strFieldName = "Currency";
                break;
            case 35:
                strFieldName = "BussType";
                break;
            case 36:
                strFieldName = "ComCode";
                break;
            case 37:
                strFieldName = "ModifyOperator";
                break;
            case 38:
                strFieldName = "NetAmount";
                break;
            case 39:
                strFieldName = "TaxAmount";
                break;
            case 40:
                strFieldName = "Tax";
                break;
            case 41:
                strFieldName = "PolicyYear";
                break;
            case 42:
                strFieldName = "OrderPay";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "GRPPOLNO":
                return Schema.TYPE_STRING;
            case "PAYCOUNT":
                return Schema.TYPE_INT;
            case "GRPCONTNO":
                return Schema.TYPE_STRING;
            case "MANAGECOM":
                return Schema.TYPE_STRING;
            case "AGENTCOM":
                return Schema.TYPE_STRING;
            case "AGENTTYPE":
                return Schema.TYPE_STRING;
            case "RISKCODE":
                return Schema.TYPE_STRING;
            case "AGENTCODE":
                return Schema.TYPE_STRING;
            case "AGENTGROUP":
                return Schema.TYPE_STRING;
            case "PAYTYPEFLAG":
                return Schema.TYPE_STRING;
            case "APPNTNO":
                return Schema.TYPE_STRING;
            case "PAYNO":
                return Schema.TYPE_STRING;
            case "ENDORSEMENTNO":
                return Schema.TYPE_STRING;
            case "SUMDUEPAYMONEY":
                return Schema.TYPE_DOUBLE;
            case "SUMACTUPAYMONEY":
                return Schema.TYPE_DOUBLE;
            case "PAYINTV":
                return Schema.TYPE_INT;
            case "PAYDATE":
                return Schema.TYPE_STRING;
            case "PAYTYPE":
                return Schema.TYPE_STRING;
            case "ENTERACCDATE":
                return Schema.TYPE_STRING;
            case "CONFDATE":
                return Schema.TYPE_STRING;
            case "LASTPAYTODATE":
                return Schema.TYPE_STRING;
            case "CURPAYTODATE":
                return Schema.TYPE_STRING;
            case "APPROVECODE":
                return Schema.TYPE_STRING;
            case "APPROVEDATE":
                return Schema.TYPE_STRING;
            case "APPROVETIME":
                return Schema.TYPE_STRING;
            case "SERIALNO":
                return Schema.TYPE_STRING;
            case "GETNOTICENO":
                return Schema.TYPE_STRING;
            case "OPERATOR":
                return Schema.TYPE_STRING;
            case "MAKEDATE":
                return Schema.TYPE_STRING;
            case "MAKETIME":
                return Schema.TYPE_STRING;
            case "MODIFYDATE":
                return Schema.TYPE_STRING;
            case "MODIFYTIME":
                return Schema.TYPE_STRING;
            case "FEEMONEY":
                return Schema.TYPE_DOUBLE;
            case "OPERSTATE":
                return Schema.TYPE_STRING;
            case "CURRENCY":
                return Schema.TYPE_STRING;
            case "BUSSTYPE":
                return Schema.TYPE_STRING;
            case "COMCODE":
                return Schema.TYPE_STRING;
            case "MODIFYOPERATOR":
                return Schema.TYPE_STRING;
            case "NETAMOUNT":
                return Schema.TYPE_DOUBLE;
            case "TAXAMOUNT":
                return Schema.TYPE_DOUBLE;
            case "TAX":
                return Schema.TYPE_DOUBLE;
            case "POLICYYEAR":
                return Schema.TYPE_STRING;
            case "ORDERPAY":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_STRING;
            case 1:
                return Schema.TYPE_INT;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_STRING;
            case 6:
                return Schema.TYPE_STRING;
            case 7:
                return Schema.TYPE_STRING;
            case 8:
                return Schema.TYPE_STRING;
            case 9:
                return Schema.TYPE_STRING;
            case 10:
                return Schema.TYPE_STRING;
            case 11:
                return Schema.TYPE_STRING;
            case 12:
                return Schema.TYPE_STRING;
            case 13:
                return Schema.TYPE_DOUBLE;
            case 14:
                return Schema.TYPE_DOUBLE;
            case 15:
                return Schema.TYPE_INT;
            case 16:
                return Schema.TYPE_STRING;
            case 17:
                return Schema.TYPE_STRING;
            case 18:
                return Schema.TYPE_STRING;
            case 19:
                return Schema.TYPE_STRING;
            case 20:
                return Schema.TYPE_STRING;
            case 21:
                return Schema.TYPE_STRING;
            case 22:
                return Schema.TYPE_STRING;
            case 23:
                return Schema.TYPE_STRING;
            case 24:
                return Schema.TYPE_STRING;
            case 25:
                return Schema.TYPE_STRING;
            case 26:
                return Schema.TYPE_STRING;
            case 27:
                return Schema.TYPE_STRING;
            case 28:
                return Schema.TYPE_STRING;
            case 29:
                return Schema.TYPE_STRING;
            case 30:
                return Schema.TYPE_STRING;
            case 31:
                return Schema.TYPE_STRING;
            case 32:
                return Schema.TYPE_DOUBLE;
            case 33:
                return Schema.TYPE_STRING;
            case 34:
                return Schema.TYPE_STRING;
            case 35:
                return Schema.TYPE_STRING;
            case 36:
                return Schema.TYPE_STRING;
            case 37:
                return Schema.TYPE_STRING;
            case 38:
                return Schema.TYPE_DOUBLE;
            case 39:
                return Schema.TYPE_DOUBLE;
            case 40:
                return Schema.TYPE_DOUBLE;
            case 41:
                return Schema.TYPE_STRING;
            case 42:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("GrpPolNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GrpPolNo));
        }
        if (FCode.equalsIgnoreCase("PayCount")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PayCount));
        }
        if (FCode.equalsIgnoreCase("GrpContNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GrpContNo));
        }
        if (FCode.equalsIgnoreCase("ManageCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
        }
        if (FCode.equalsIgnoreCase("AgentCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentCom));
        }
        if (FCode.equalsIgnoreCase("AgentType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentType));
        }
        if (FCode.equalsIgnoreCase("RiskCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RiskCode));
        }
        if (FCode.equalsIgnoreCase("AgentCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentCode));
        }
        if (FCode.equalsIgnoreCase("AgentGroup")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentGroup));
        }
        if (FCode.equalsIgnoreCase("PayTypeFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PayTypeFlag));
        }
        if (FCode.equalsIgnoreCase("AppntNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AppntNo));
        }
        if (FCode.equalsIgnoreCase("PayNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PayNo));
        }
        if (FCode.equalsIgnoreCase("EndorsementNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(EndorsementNo));
        }
        if (FCode.equalsIgnoreCase("SumDuePayMoney")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SumDuePayMoney));
        }
        if (FCode.equalsIgnoreCase("SumActuPayMoney")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SumActuPayMoney));
        }
        if (FCode.equalsIgnoreCase("PayIntv")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PayIntv));
        }
        if (FCode.equalsIgnoreCase("PayDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PayDate));
        }
        if (FCode.equalsIgnoreCase("PayType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PayType));
        }
        if (FCode.equalsIgnoreCase("EnterAccDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(EnterAccDate));
        }
        if (FCode.equalsIgnoreCase("ConfDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ConfDate));
        }
        if (FCode.equalsIgnoreCase("LastPayToDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(LastPayToDate));
        }
        if (FCode.equalsIgnoreCase("CurPayToDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CurPayToDate));
        }
        if (FCode.equalsIgnoreCase("ApproveCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ApproveCode));
        }
        if (FCode.equalsIgnoreCase("ApproveDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ApproveDate));
        }
        if (FCode.equalsIgnoreCase("ApproveTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ApproveTime));
        }
        if (FCode.equalsIgnoreCase("SerialNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SerialNo));
        }
        if (FCode.equalsIgnoreCase("GetNoticeNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GetNoticeNo));
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeDate));
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyDate));
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (FCode.equalsIgnoreCase("FeeMoney")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FeeMoney));
        }
        if (FCode.equalsIgnoreCase("OperState")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OperState));
        }
        if (FCode.equalsIgnoreCase("Currency")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Currency));
        }
        if (FCode.equalsIgnoreCase("BussType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BussType));
        }
        if (FCode.equalsIgnoreCase("ComCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ComCode));
        }
        if (FCode.equalsIgnoreCase("ModifyOperator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyOperator));
        }
        if (FCode.equalsIgnoreCase("NetAmount")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(NetAmount));
        }
        if (FCode.equalsIgnoreCase("TaxAmount")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(TaxAmount));
        }
        if (FCode.equalsIgnoreCase("Tax")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Tax));
        }
        if (FCode.equalsIgnoreCase("PolicyYear")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PolicyYear));
        }
        if (FCode.equalsIgnoreCase("OrderPay")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OrderPay));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = String.valueOf(GrpPolNo);
                break;
            case 1:
                strFieldValue = String.valueOf(PayCount);
                break;
            case 2:
                strFieldValue = String.valueOf(GrpContNo);
                break;
            case 3:
                strFieldValue = String.valueOf(ManageCom);
                break;
            case 4:
                strFieldValue = String.valueOf(AgentCom);
                break;
            case 5:
                strFieldValue = String.valueOf(AgentType);
                break;
            case 6:
                strFieldValue = String.valueOf(RiskCode);
                break;
            case 7:
                strFieldValue = String.valueOf(AgentCode);
                break;
            case 8:
                strFieldValue = String.valueOf(AgentGroup);
                break;
            case 9:
                strFieldValue = String.valueOf(PayTypeFlag);
                break;
            case 10:
                strFieldValue = String.valueOf(AppntNo);
                break;
            case 11:
                strFieldValue = String.valueOf(PayNo);
                break;
            case 12:
                strFieldValue = String.valueOf(EndorsementNo);
                break;
            case 13:
                strFieldValue = String.valueOf(SumDuePayMoney);
                break;
            case 14:
                strFieldValue = String.valueOf(SumActuPayMoney);
                break;
            case 15:
                strFieldValue = String.valueOf(PayIntv);
                break;
            case 16:
                strFieldValue = String.valueOf(PayDate);
                break;
            case 17:
                strFieldValue = String.valueOf(PayType);
                break;
            case 18:
                strFieldValue = String.valueOf(EnterAccDate);
                break;
            case 19:
                strFieldValue = String.valueOf(ConfDate);
                break;
            case 20:
                strFieldValue = String.valueOf(LastPayToDate);
                break;
            case 21:
                strFieldValue = String.valueOf(CurPayToDate);
                break;
            case 22:
                strFieldValue = String.valueOf(ApproveCode);
                break;
            case 23:
                strFieldValue = String.valueOf(ApproveDate);
                break;
            case 24:
                strFieldValue = String.valueOf(ApproveTime);
                break;
            case 25:
                strFieldValue = String.valueOf(SerialNo);
                break;
            case 26:
                strFieldValue = String.valueOf(GetNoticeNo);
                break;
            case 27:
                strFieldValue = String.valueOf(Operator);
                break;
            case 28:
                strFieldValue = String.valueOf(MakeDate);
                break;
            case 29:
                strFieldValue = String.valueOf(MakeTime);
                break;
            case 30:
                strFieldValue = String.valueOf(ModifyDate);
                break;
            case 31:
                strFieldValue = String.valueOf(ModifyTime);
                break;
            case 32:
                strFieldValue = String.valueOf(FeeMoney);
                break;
            case 33:
                strFieldValue = String.valueOf(OperState);
                break;
            case 34:
                strFieldValue = String.valueOf(Currency);
                break;
            case 35:
                strFieldValue = String.valueOf(BussType);
                break;
            case 36:
                strFieldValue = String.valueOf(ComCode);
                break;
            case 37:
                strFieldValue = String.valueOf(ModifyOperator);
                break;
            case 38:
                strFieldValue = String.valueOf(NetAmount);
                break;
            case 39:
                strFieldValue = String.valueOf(TaxAmount);
                break;
            case 40:
                strFieldValue = String.valueOf(Tax);
                break;
            case 41:
                strFieldValue = String.valueOf(PolicyYear);
                break;
            case 42:
                strFieldValue = String.valueOf(OrderPay);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("GrpPolNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                GrpPolNo = FValue.trim();
            }
            else
                GrpPolNo = null;
        }
        if (FCode.equalsIgnoreCase("PayCount")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                PayCount = i;
            }
        }
        if (FCode.equalsIgnoreCase("GrpContNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                GrpContNo = FValue.trim();
            }
            else
                GrpContNo = null;
        }
        if (FCode.equalsIgnoreCase("ManageCom")) {
            if( FValue != null && !FValue.equals(""))
            {
                ManageCom = FValue.trim();
            }
            else
                ManageCom = null;
        }
        if (FCode.equalsIgnoreCase("AgentCom")) {
            if( FValue != null && !FValue.equals(""))
            {
                AgentCom = FValue.trim();
            }
            else
                AgentCom = null;
        }
        if (FCode.equalsIgnoreCase("AgentType")) {
            if( FValue != null && !FValue.equals(""))
            {
                AgentType = FValue.trim();
            }
            else
                AgentType = null;
        }
        if (FCode.equalsIgnoreCase("RiskCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                RiskCode = FValue.trim();
            }
            else
                RiskCode = null;
        }
        if (FCode.equalsIgnoreCase("AgentCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                AgentCode = FValue.trim();
            }
            else
                AgentCode = null;
        }
        if (FCode.equalsIgnoreCase("AgentGroup")) {
            if( FValue != null && !FValue.equals(""))
            {
                AgentGroup = FValue.trim();
            }
            else
                AgentGroup = null;
        }
        if (FCode.equalsIgnoreCase("PayTypeFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                PayTypeFlag = FValue.trim();
            }
            else
                PayTypeFlag = null;
        }
        if (FCode.equalsIgnoreCase("AppntNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                AppntNo = FValue.trim();
            }
            else
                AppntNo = null;
        }
        if (FCode.equalsIgnoreCase("PayNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                PayNo = FValue.trim();
            }
            else
                PayNo = null;
        }
        if (FCode.equalsIgnoreCase("EndorsementNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                EndorsementNo = FValue.trim();
            }
            else
                EndorsementNo = null;
        }
        if (FCode.equalsIgnoreCase("SumDuePayMoney")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                SumDuePayMoney = d;
            }
        }
        if (FCode.equalsIgnoreCase("SumActuPayMoney")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                SumActuPayMoney = d;
            }
        }
        if (FCode.equalsIgnoreCase("PayIntv")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                PayIntv = i;
            }
        }
        if (FCode.equalsIgnoreCase("PayDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                PayDate = FValue.trim();
            }
            else
                PayDate = null;
        }
        if (FCode.equalsIgnoreCase("PayType")) {
            if( FValue != null && !FValue.equals(""))
            {
                PayType = FValue.trim();
            }
            else
                PayType = null;
        }
        if (FCode.equalsIgnoreCase("EnterAccDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                EnterAccDate = FValue.trim();
            }
            else
                EnterAccDate = null;
        }
        if (FCode.equalsIgnoreCase("ConfDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                ConfDate = FValue.trim();
            }
            else
                ConfDate = null;
        }
        if (FCode.equalsIgnoreCase("LastPayToDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                LastPayToDate = FValue.trim();
            }
            else
                LastPayToDate = null;
        }
        if (FCode.equalsIgnoreCase("CurPayToDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                CurPayToDate = FValue.trim();
            }
            else
                CurPayToDate = null;
        }
        if (FCode.equalsIgnoreCase("ApproveCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                ApproveCode = FValue.trim();
            }
            else
                ApproveCode = null;
        }
        if (FCode.equalsIgnoreCase("ApproveDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                ApproveDate = FValue.trim();
            }
            else
                ApproveDate = null;
        }
        if (FCode.equalsIgnoreCase("ApproveTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                ApproveTime = FValue.trim();
            }
            else
                ApproveTime = null;
        }
        if (FCode.equalsIgnoreCase("SerialNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                SerialNo = FValue.trim();
            }
            else
                SerialNo = null;
        }
        if (FCode.equalsIgnoreCase("GetNoticeNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                GetNoticeNo = FValue.trim();
            }
            else
                GetNoticeNo = null;
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            if( FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
                Operator = null;
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeDate = FValue.trim();
            }
            else
                MakeDate = null;
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
                MakeTime = null;
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyDate = FValue.trim();
            }
            else
                ModifyDate = null;
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
                ModifyTime = null;
        }
        if (FCode.equalsIgnoreCase("FeeMoney")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                FeeMoney = d;
            }
        }
        if (FCode.equalsIgnoreCase("OperState")) {
            if( FValue != null && !FValue.equals(""))
            {
                OperState = FValue.trim();
            }
            else
                OperState = null;
        }
        if (FCode.equalsIgnoreCase("Currency")) {
            if( FValue != null && !FValue.equals(""))
            {
                Currency = FValue.trim();
            }
            else
                Currency = null;
        }
        if (FCode.equalsIgnoreCase("BussType")) {
            if( FValue != null && !FValue.equals(""))
            {
                BussType = FValue.trim();
            }
            else
                BussType = null;
        }
        if (FCode.equalsIgnoreCase("ComCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                ComCode = FValue.trim();
            }
            else
                ComCode = null;
        }
        if (FCode.equalsIgnoreCase("ModifyOperator")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyOperator = FValue.trim();
            }
            else
                ModifyOperator = null;
        }
        if (FCode.equalsIgnoreCase("NetAmount")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                NetAmount = d;
            }
        }
        if (FCode.equalsIgnoreCase("TaxAmount")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                TaxAmount = d;
            }
        }
        if (FCode.equalsIgnoreCase("Tax")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                Tax = d;
            }
        }
        if (FCode.equalsIgnoreCase("PolicyYear")) {
            if( FValue != null && !FValue.equals(""))
            {
                PolicyYear = FValue.trim();
            }
            else
                PolicyYear = null;
        }
        if (FCode.equalsIgnoreCase("OrderPay")) {
            if( FValue != null && !FValue.equals(""))
            {
                OrderPay = FValue.trim();
            }
            else
                OrderPay = null;
        }
        return true;
    }


    public String toString() {
    return "LJAPayGrpPojo [" +
            "GrpPolNo="+GrpPolNo +
            ", PayCount="+PayCount +
            ", GrpContNo="+GrpContNo +
            ", ManageCom="+ManageCom +
            ", AgentCom="+AgentCom +
            ", AgentType="+AgentType +
            ", RiskCode="+RiskCode +
            ", AgentCode="+AgentCode +
            ", AgentGroup="+AgentGroup +
            ", PayTypeFlag="+PayTypeFlag +
            ", AppntNo="+AppntNo +
            ", PayNo="+PayNo +
            ", EndorsementNo="+EndorsementNo +
            ", SumDuePayMoney="+SumDuePayMoney +
            ", SumActuPayMoney="+SumActuPayMoney +
            ", PayIntv="+PayIntv +
            ", PayDate="+PayDate +
            ", PayType="+PayType +
            ", EnterAccDate="+EnterAccDate +
            ", ConfDate="+ConfDate +
            ", LastPayToDate="+LastPayToDate +
            ", CurPayToDate="+CurPayToDate +
            ", ApproveCode="+ApproveCode +
            ", ApproveDate="+ApproveDate +
            ", ApproveTime="+ApproveTime +
            ", SerialNo="+SerialNo +
            ", GetNoticeNo="+GetNoticeNo +
            ", Operator="+Operator +
            ", MakeDate="+MakeDate +
            ", MakeTime="+MakeTime +
            ", ModifyDate="+ModifyDate +
            ", ModifyTime="+ModifyTime +
            ", FeeMoney="+FeeMoney +
            ", OperState="+OperState +
            ", Currency="+Currency +
            ", BussType="+BussType +
            ", ComCode="+ComCode +
            ", ModifyOperator="+ModifyOperator +
            ", NetAmount="+NetAmount +
            ", TaxAmount="+TaxAmount +
            ", Tax="+Tax +
            ", PolicyYear="+PolicyYear +
            ", OrderPay="+OrderPay +"]";
    }
}
