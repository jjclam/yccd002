package com.sinosoft.lis.entity;

/**
 * Created by 张旭东 on 2017/10/16.
 */


import java.io.Serializable;
import java.util.ArrayList;

public class LCAllDataCLPojo implements Serializable {
    private ArrayList<LKTransTracksPojo> lkTransTracksPojos;
    private ArrayList<LCPolPojo> lcPolPojos;
    private ArrayList<LCContPojo> lcContPojos;
    private ArrayList<LCContStatePojo> lcContStatePojos;
    private ArrayList<LCPremPojo> lcPremPojos;
    private ArrayList<LCDutyPojo> lcDutyPojos;
    private ArrayList<LCAddressPojo> lcAddressPojos;
    private ArrayList<LCInsuredPojo> lcInsuredPojos;
    private ArrayList<LCBnfPojo> lcBnfPojos;
    private ArrayList<LDPersonPojo> ldPersonPojos;
    private ArrayList<LCGetPojo> lcGetPojos;
    private ArrayList<LCAppntPojo> lcAppntPojos;
    private ArrayList<LCGetToAccPojo> lcGetToAccPojos;
    private ArrayList<LCPremToAccPojo> lcPremToAccPojos;
    private ArrayList<LCInsureAccPojo> lcInsureAccPojos;
    private ArrayList<LCInsureAccTracePojo> lcInsureAccTracePojos;
    private ArrayList<LCInsureAccClassPojo> lcInsureAccClassPojos;
    private ArrayList<LCInsureAccFeePojo> lcInsureAccFeePojos;
    private ArrayList<LCInsureAccFeeTracePojo> lcInsureAccFeeTracePojos;
    private ArrayList<LCInsureAccClassFeePojo> lcInsureAccClassFeePojos;
    private ArrayList<LJAPayPojo> ljAPayPojos;
    private ArrayList<LJAPayPersonPojo> ljAPayPersonPojos;
    private ArrayList<LJSPayPojo> ljSPayPojos;
    private ArrayList<LJSPayPersonPojo> ljSPayPersonPojos;
    private ArrayList<LCCustomerImpartPojo> lcCusTomerImpartPojos;
    private ArrayList<LCCUWMasterPojo> lCCUWMasterPojos;
    private ArrayList<LCCUWSubPojo> lCCUWSubPojos;
    private ArrayList<LCUWMasterPojo> lCUWMasterPojos;
    private ArrayList<LCAppntLinkManInfoPojo> lcAppntLinkManInfoPojos;
    private ArrayList<LCUWErrorPojo> lCUWErrorPojos;
    private ArrayList<LCCUWErrorPojo> lCCUWErrorPojos;
    private ArrayList<LCUWSubPojo> lCUWSubPojos;
    private ArrayList<LCCustomerImpartParamsPojo> lCCustomerImpartParamsPojos;
    private ArrayList<LJTempFeePojo> lJTempFeePojos;
    private ArrayList<LJTempFeeClassPojo> lJTempFeeClassPojos;
    private ArrayList<LCCustomerImpartDetailPojo> lcCustomerImpartDetailPojos;
    private ArrayList<LCSpecPojo> lCSpecPojos;
    private ArrayList<LACommisionDetailPojo> laCommisionDetailPojos;
    private ArrayList<LCAccountPojo> lcAccountPojos;
    private ArrayList<LKTransStatusPojo> lkTransStatusPojos;
    private ArrayList<VMS_OUTPUT_TRANSPojo> vms_output_transPojos;
    private ArrayList<VMS_OUTPUT_TRANS_TOTALPojo> vms_output_trans_totalPojos;
    private ArrayList<VMS_OUTPUT_CUSTOMERPojo> vms_output_customerPojos;
    //    private ArrayList<LCGettoaccPojo> lcGettoaccPojos;
//    private ArrayList<LCPremtoaccPojo> lcGettoaccPojos;
//    private ArrayList<LJApaypersonPojo> lcGettoaccPojos;
    private ArrayList<LCAudVidRecordPojo> lcAudVidRecordPojos;
    private ArrayList<LCProdSetPojo> lcProdSetPojos;
    private ArrayList<LASignAgentPojo> laSignAgentPojos;
    private ArrayList<LKTransInfoPojo> lkTransInfoPojos;
    private ArrayList<LVInsureAccPojo> lvInsureAccPojos;
    private ArrayList<LVInsureAccTracePojo> lvInsureAccTracePojos;

    private ArrayList<LCGrpContPojo> lcGrpContPojos;
    private ArrayList<LCGrpPolPojo> lcGrpPolPojos;
    private ArrayList<LCGrpAppntPojo> lcGrpAppntPojos;
    private ArrayList<LCGrpAddressPojo> lcGrpAddressPojos;
    private ArrayList<LCAgentToContPojo> lcAgentToContPojos;
    private ArrayList<LCAgentComInfoPojo> lcAgentComInfoPojos;
    private ArrayList<LCPolicyInfoPojo> lcPolicyInfoPojos;
    private ArrayList<LCContPlanPojo> lcContPlanPojos;
    private ArrayList<LCDutySubPojo> lcDutySubPojos;
    private ArrayList<LCContPlanDetailPojo> lcContPlanDetailPojos;
    private ArrayList<LCContPlanDetailSubPojo> lcContPlanDetailSubPojos;
    private ArrayList<LDGrpPojo> ldGrpPojos;
    private ArrayList<LCApiPojo> lcApiPojos;
    private ArrayList<LCAIPMULTIYEARCONTPojo> lcaipmultiyearcontPojos;
    private ArrayList<LJAPayGrpPojo> ljaPayGrpPojos;
    private ArrayList<LCAIPContRegisterPojo> lcaipContRegisterPojos;
    private LJAGetPojo ljaGetPojo;
    private LJAGetOtherPojo ljaGetOtherPojo;
    private ArrayList<LCPadImagePojo> lcPadImagePojos;
    private LCWechatRelaTionPojo lcWechatRelaTionPojo;
    public LCAllDataCLPojo() {
    }

    public ArrayList<LKTransTracksPojo> getLkTransTracksPojos() {
        return lkTransTracksPojos;
    }

    public void setLkTransTracksPojos(ArrayList<LKTransTracksPojo> lkTransTracksPojos) {
        this.lkTransTracksPojos = lkTransTracksPojos;
    }

    public LCWechatRelaTionPojo getLcWechatRelaTionPojo() {
        return lcWechatRelaTionPojo;
    }

    public void setLcWechatRelaTionPojo(LCWechatRelaTionPojo lcWechatRelaTionPojo) {
        this.lcWechatRelaTionPojo = lcWechatRelaTionPojo;
    }

    public ArrayList<LVInsureAccPojo> getLvInsureAccPojos() {
        return lvInsureAccPojos;
    }

    public void setLvInsureAccPojos(ArrayList<LVInsureAccPojo> lvInsureAccPojos) {
        this.lvInsureAccPojos = lvInsureAccPojos;
    }

    public ArrayList<LVInsureAccTracePojo> getLvInsureAccTracePojos() {
        return lvInsureAccTracePojos;
    }

    public void setLvInsureAccTracePojos(ArrayList<LVInsureAccTracePojo> lvInsureAccTracePojos) {
        this.lvInsureAccTracePojos = lvInsureAccTracePojos;
    }

    public ArrayList<LCAIPContRegisterPojo> getLcaipContRegisterPojos() {
        return lcaipContRegisterPojos;
    }

    public void setLcaipContRegisterPojos(ArrayList<LCAIPContRegisterPojo> lcaipContRegisterPojos) {
        this.lcaipContRegisterPojos = lcaipContRegisterPojos;
    }

    public ArrayList<LJAPayGrpPojo> getLjaPayGrpPojos() {
        return ljaPayGrpPojos;
    }

    public void setLjaPayGrpPojos(ArrayList<LJAPayGrpPojo> ljaPayGrpPojos) {
        this.ljaPayGrpPojos = ljaPayGrpPojos;
    }

    public ArrayList<LCAIPMULTIYEARCONTPojo> getLcaipmultiyearcontPojos() {
        return lcaipmultiyearcontPojos;
    }

    public void setLcaipmultiyearcontPojos(ArrayList<LCAIPMULTIYEARCONTPojo> lcaipmultiyearcontPojos) {
        this.lcaipmultiyearcontPojos = lcaipmultiyearcontPojos;
    }

    public ArrayList<LCApiPojo> getLcApiPojos() {
        return lcApiPojos;
    }

    public void setLcApiPojos(ArrayList<LCApiPojo> lcApiPojos) {
        this.lcApiPojos = lcApiPojos;
    }


    public ArrayList<LDGrpPojo> getLdGrpPojos() {
        return ldGrpPojos;
    }

    public void setLdGrpPojos(ArrayList<LDGrpPojo> ldGrpPojos) {
        this.ldGrpPojos = ldGrpPojos;
    }

    public ArrayList<LCGrpPolPojo> getLcGrpPolPojos() {
        return lcGrpPolPojos;
    }

    public void setLcGrpPolPojos(ArrayList<LCGrpPolPojo> lcGrpPolPojos) {
        this.lcGrpPolPojos = lcGrpPolPojos;
    }

    public ArrayList<LCGrpAddressPojo> getLcGrpAddressPojos() {
        return lcGrpAddressPojos;
    }

    public void setLcGrpAddressPojos(ArrayList<LCGrpAddressPojo> lcGrpAddressPojos) {
        this.lcGrpAddressPojos = lcGrpAddressPojos;
    }

    public ArrayList<LCPolicyInfoPojo> getLcPolicyInfoPojos() {
        return lcPolicyInfoPojos;
    }

    public void setLcPolicyInfoPojos(ArrayList<LCPolicyInfoPojo> lcPolicyInfoPojos) {
        this.lcPolicyInfoPojos = lcPolicyInfoPojos;
    }

    public ArrayList<LCContPlanPojo> getLcContPlanPojos() {
        return lcContPlanPojos;
    }

    public void setLcContPlanPojos(ArrayList<LCContPlanPojo> lcContPlanPojos) {
        this.lcContPlanPojos = lcContPlanPojos;
    }

    public ArrayList<LCDutySubPojo> getLcDutySubPojos() {
        return lcDutySubPojos;
    }

    public void setLcDutySubPojos(ArrayList<LCDutySubPojo> lcDutySubPojos) {
        this.lcDutySubPojos = lcDutySubPojos;
    }

    public ArrayList<LCContPlanDetailPojo> getLcContPlanDetailPojos() {
        return lcContPlanDetailPojos;
    }

    public void setLcContPlanDetailPojos(ArrayList<LCContPlanDetailPojo> lcContPlanDetailPojos) {
        this.lcContPlanDetailPojos = lcContPlanDetailPojos;
    }

    public ArrayList<LCContPlanDetailSubPojo> getLcContPlanDetailSubPojos() {
        return lcContPlanDetailSubPojos;
    }

    public void setLcContPlanDetailSubPojos(ArrayList<LCContPlanDetailSubPojo> lcContPlanDetailSubPojos) {
        this.lcContPlanDetailSubPojos = lcContPlanDetailSubPojos;
    }

    public ArrayList<LCGrpContPojo> getLcGrpContPojos() {
        return lcGrpContPojos;
    }

    public void setLcGrpContPojos(ArrayList<LCGrpContPojo> lcGrpContPojos) {
        this.lcGrpContPojos = lcGrpContPojos;
    }

    public ArrayList<LCGrpAppntPojo> getLcGrpAppntPojos() {
        return lcGrpAppntPojos;
    }

    public void setLcGrpAppntPojos(ArrayList<LCGrpAppntPojo> lcGrpAppntPojos) {
        this.lcGrpAppntPojos = lcGrpAppntPojos;
    }

    public ArrayList<LCAgentToContPojo> getLcAgentToContPojos() {
        return lcAgentToContPojos;
    }

    public void setLcAgentToContPojos(ArrayList<LCAgentToContPojo> lcAgentToContPojos) {
        this.lcAgentToContPojos = lcAgentToContPojos;
    }

    public ArrayList<LCAgentComInfoPojo> getLcAgentComInfoPojos() {
        return lcAgentComInfoPojos;
    }

    public void setLcAgentComInfoPojos(ArrayList<LCAgentComInfoPojo> lcAgentComInfoPojos) {
        this.lcAgentComInfoPojos = lcAgentComInfoPojos;
    }

    public ArrayList<LCSpecPojo> getlCSpecPojos() {
        return this.lCSpecPojos;
    }

    public void setlCSpecPojos(ArrayList<LCSpecPojo> lCSpecPojos) {
        this.lCSpecPojos = lCSpecPojos;
    }

    public ArrayList<LCUWErrorPojo> getlCUWErrorPojos() {
        return this.lCUWErrorPojos;
    }

    public void setlCUWErrorPojos(ArrayList<LCUWErrorPojo> lCUWErrorPojos) {
        this.lCUWErrorPojos = lCUWErrorPojos;
    }

    public ArrayList<LCCUWErrorPojo> getlCCUWErrorPojos() {
        return this.lCCUWErrorPojos;
    }

    public void setlCCUWErrorPojos(ArrayList<LCCUWErrorPojo> lCCUWErrorPojos) {
        this.lCCUWErrorPojos = lCCUWErrorPojos;
    }

    public ArrayList<LCAppntLinkManInfoPojo> getLcAppntLinkManInfoPojos() {
        return this.lcAppntLinkManInfoPojos;
    }

    public void setLcAppntLinkManInfoPojos(ArrayList<LCAppntLinkManInfoPojo> lcAppntLinkManInfoPojos) {
        this.lcAppntLinkManInfoPojos = lcAppntLinkManInfoPojos;
    }

    public ArrayList<LCCustomerImpartDetailPojo> getLcCustomerImpartDetailPojos() {
        return this.lcCustomerImpartDetailPojos;
    }

    public void setLcCustomerImpartDetailPojos(ArrayList<LCCustomerImpartDetailPojo> lcCustomerImpartDetailPojos) {
        this.lcCustomerImpartDetailPojos = lcCustomerImpartDetailPojos;
    }

    public ArrayList<LCGetToAccPojo> getLcGetToAccPojos() {
        return this.lcGetToAccPojos;
    }

    public void setLcGetToAccPojos(ArrayList<LCGetToAccPojo> lcGetToAccPojos) {
        this.lcGetToAccPojos = lcGetToAccPojos;
    }

    public ArrayList<LCPolPojo> getLcPolPojos() {
        return this.lcPolPojos;
    }

    public void setLcPolPojos(ArrayList<LCPolPojo> lcPolPojos) {
        this.lcPolPojos = lcPolPojos;
    }

    public ArrayList<LCContPojo> getLcContPojos() {
        return this.lcContPojos;
    }

    public void setLcContPojos(ArrayList<LCContPojo> lcContPojos) {
        this.lcContPojos = lcContPojos;
    }

    public ArrayList<LCContStatePojo> getLcContStatePojos() {
        return this.lcContStatePojos;
    }

    public void setLcContStatePojos(ArrayList<LCContStatePojo> lcContStatePojos) {
        this.lcContStatePojos = lcContStatePojos;
    }

    public ArrayList<LCPremPojo> getLcPremPojos() {
        return this.lcPremPojos;
    }

    public void setLcPremPojos(ArrayList<LCPremPojo> lcPremPojos) {
        this.lcPremPojos = lcPremPojos;
    }

    public ArrayList<LCDutyPojo> getLcDutyPojos() {
        return this.lcDutyPojos;
    }

    public void setLcDutyPojos(ArrayList<LCDutyPojo> lcDutyPojos) {
        this.lcDutyPojos = lcDutyPojos;
    }

    public ArrayList<LCAddressPojo> getLcAddressPojos() {
        return this.lcAddressPojos;
    }

    public void setLcAddressPojos(ArrayList<LCAddressPojo> lcAddressPojos) {
        this.lcAddressPojos = lcAddressPojos;
    }

    public ArrayList<LCInsuredPojo> getLcInsuredPojos() {
        return this.lcInsuredPojos;
    }

    public void setLcInsuredPojos(ArrayList<LCInsuredPojo> lcInsuredPojos) {
        this.lcInsuredPojos = lcInsuredPojos;
    }

    public ArrayList<LCBnfPojo> getLcBnfPojos() {
        return this.lcBnfPojos;
    }

    public void setLcBnfPojos(ArrayList<LCBnfPojo> lcBnfPojos) {
        this.lcBnfPojos = lcBnfPojos;
    }

    public ArrayList<LDPersonPojo> getLdPersonPojos() {
        return this.ldPersonPojos;
    }

    public void setLdPersonPojos(ArrayList<LDPersonPojo> ldPersonPojos) {
        this.ldPersonPojos = ldPersonPojos;
    }

    public ArrayList<LCGetPojo> getLcGetPojos() {
        return this.lcGetPojos;
    }

    public void setLcGetPojos(ArrayList<LCGetPojo> lcGetPojos) {
        this.lcGetPojos = lcGetPojos;
    }

    public ArrayList<LCAppntPojo> getLcAppntPojos() {
        return this.lcAppntPojos;
    }

    public void setLcAppntPojos(ArrayList<LCAppntPojo> lcAppntPojos) {
        this.lcAppntPojos = lcAppntPojos;
    }

    public ArrayList<LCPremToAccPojo> getLcPremToAccPojos() {
        return this.lcPremToAccPojos;
    }

    public void setLcPremToAccPojos(ArrayList<LCPremToAccPojo> lcPremToAccPojos) {
        this.lcPremToAccPojos = lcPremToAccPojos;
    }

    public ArrayList<LCInsureAccPojo> getLcInsureAccPojos() {
        return this.lcInsureAccPojos;
    }

    public void setLcInsureAccPojos(ArrayList<LCInsureAccPojo> lcInsureAccPojos) {
        this.lcInsureAccPojos = lcInsureAccPojos;
    }

    public ArrayList<LCInsureAccTracePojo> getLcInsureAccTracePojos() {
        return this.lcInsureAccTracePojos;
    }

    public void setLcInsureAccTracePojos(ArrayList<LCInsureAccTracePojo> lcInsureAccTracePojos) {
        this.lcInsureAccTracePojos = lcInsureAccTracePojos;
    }

    public ArrayList<LCInsureAccClassPojo> getLcInsureAccClassPojos() {
        return this.lcInsureAccClassPojos;
    }

    public void setLcInsureAccClassPojos(ArrayList<LCInsureAccClassPojo> lcInsureAccClassPojos) {
        this.lcInsureAccClassPojos = lcInsureAccClassPojos;
    }

    public ArrayList<LCInsureAccFeePojo> getLcInsureAccFeePojos() {
        return this.lcInsureAccFeePojos;
    }

    public void setLcInsureAccFeePojos(ArrayList<LCInsureAccFeePojo> lcInsureAccFeePojos) {
        this.lcInsureAccFeePojos = lcInsureAccFeePojos;
    }

    public ArrayList<LCInsureAccFeeTracePojo> getLcInsureAccFeeTracePojos() {
        return this.lcInsureAccFeeTracePojos;
    }

    public void setLcInsureAccFeeTracePojos(ArrayList<LCInsureAccFeeTracePojo> lcInsureAccFeeTracePojos) {
        this.lcInsureAccFeeTracePojos = lcInsureAccFeeTracePojos;
    }

    public ArrayList<LCInsureAccClassFeePojo> getLcInsureAccClassFeePojos() {
        return this.lcInsureAccClassFeePojos;
    }

    public void setLcInsureAccClassFeePojos(ArrayList<LCInsureAccClassFeePojo> lcInsureAccClassFeePojos) {
        this.lcInsureAccClassFeePojos = lcInsureAccClassFeePojos;
    }

    public ArrayList<LJAPayPojo> getLjAPayPojos() {
        return this.ljAPayPojos;
    }

    public void setLjAPayPojos(ArrayList<LJAPayPojo> ljAPayPojos) {
        this.ljAPayPojos = ljAPayPojos;
    }

    public ArrayList<LJAPayPersonPojo> getLjAPayPersonPojos() {
        return this.ljAPayPersonPojos;
    }

    public void setLjAPayPersonPojos(ArrayList<LJAPayPersonPojo> ljAPayPersonPojos) {
        this.ljAPayPersonPojos = ljAPayPersonPojos;
    }

    public ArrayList<LJSPayPojo> getLjSPayPojos() {
        return this.ljSPayPojos;
    }

    public void setLjSPayPojos(ArrayList<LJSPayPojo> ljSPayPojos) {
        this.ljSPayPojos = ljSPayPojos;
    }

    public ArrayList<LJSPayPersonPojo> getLjSPayPersonPojos() {
        return this.ljSPayPersonPojos;
    }

    public void setLjSPayPersonPojos(ArrayList<LJSPayPersonPojo> ljSPayPersonPojos) {
        this.ljSPayPersonPojos = ljSPayPersonPojos;
    }

    public ArrayList<LCCustomerImpartPojo> getLcCusTomerImpartPojos() {
        return this.lcCusTomerImpartPojos;
    }

    public void setLcCusTomerImpartPojos(ArrayList<LCCustomerImpartPojo> lcCusTomerImpartPojos) {
        this.lcCusTomerImpartPojos = lcCusTomerImpartPojos;
    }

    public ArrayList<LCCUWMasterPojo> getlCCUWMasterPojos() {
        return this.lCCUWMasterPojos;
    }

    public void setlCCUWMasterPojos(ArrayList<LCCUWMasterPojo> lCCUWMasterPojos) {
        this.lCCUWMasterPojos = lCCUWMasterPojos;
    }

    public ArrayList<LCCUWSubPojo> getlCCUWSubPojos() {
        return this.lCCUWSubPojos;
    }

    public void setlCCUWSubPojos(ArrayList<LCCUWSubPojo> lCCUWSubPojos) {
        this.lCCUWSubPojos = lCCUWSubPojos;
    }

    public ArrayList<LCUWMasterPojo> getlCUWMasterPojos() {
        return this.lCUWMasterPojos;
    }

    public void setlCUWMasterPojos(ArrayList<LCUWMasterPojo> lCUWMasterPojos) {
        this.lCUWMasterPojos = lCUWMasterPojos;
    }

    public ArrayList<LCUWSubPojo> getlCUWSubPojos() {
        return this.lCUWSubPojos;
    }

    public void setlCUWSubPojos(ArrayList<LCUWSubPojo> lCUWSubPojos) {
        this.lCUWSubPojos = lCUWSubPojos;
    }

    public ArrayList<LCCustomerImpartParamsPojo> getlCCustomerImpartParamsPojos() {
        return this.lCCustomerImpartParamsPojos;
    }

    public void setlCCustomerImpartParamsPojos(ArrayList<LCCustomerImpartParamsPojo> lCCustomerImpartParamsPojos) {
        this.lCCustomerImpartParamsPojos = lCCustomerImpartParamsPojos;
    }

    public ArrayList<LACommisionDetailPojo> getLaCommisionDetailPojos() {
        return laCommisionDetailPojos;
    }

    public void setLaCommisionDetailPojos(ArrayList<LACommisionDetailPojo> laCommisionDetailPojos) {
        this.laCommisionDetailPojos = laCommisionDetailPojos;
    }

    public ArrayList<LCAccountPojo> getLcAccountPojos() {
        return lcAccountPojos;
    }

    public void setLcAccountPojos(ArrayList<LCAccountPojo> lcAccountPojos) {
        this.lcAccountPojos = lcAccountPojos;
    }

    public ArrayList<LKTransStatusPojo> getLkTransStatusPojos() {
        return lkTransStatusPojos;
    }

    public void setLkTransStatusPojos(ArrayList<LKTransStatusPojo> lkTransStatusPojos) {
        this.lkTransStatusPojos = lkTransStatusPojos;
    }

    public ArrayList<VMS_OUTPUT_TRANSPojo> getVms_output_transPojos() {
        return vms_output_transPojos;
    }

    public void setVms_output_transPojos(ArrayList<VMS_OUTPUT_TRANSPojo> vms_output_transPojos) {
        this.vms_output_transPojos = vms_output_transPojos;
    }

    public ArrayList<VMS_OUTPUT_TRANS_TOTALPojo> getVms_output_trans_totalPojos() {
        return vms_output_trans_totalPojos;
    }

    public void setVms_output_trans_totalPojos(ArrayList<VMS_OUTPUT_TRANS_TOTALPojo> vms_output_trans_totalPojos) {
        this.vms_output_trans_totalPojos = vms_output_trans_totalPojos;
    }

    public ArrayList<VMS_OUTPUT_CUSTOMERPojo> getVms_output_customerPojos() {
        return vms_output_customerPojos;
    }

    public void setVms_output_customerPojos(ArrayList<VMS_OUTPUT_CUSTOMERPojo> vms_output_customerPojos) {
        this.vms_output_customerPojos = vms_output_customerPojos;
    }

    public ArrayList<LCAudVidRecordPojo> getLcAudVidRecordPojos() {
        return lcAudVidRecordPojos;
    }

    public void setLcAudVidRecordPojos(ArrayList<LCAudVidRecordPojo> lcAudVidRecordPojos) {
        this.lcAudVidRecordPojos = lcAudVidRecordPojos;
    }

    public ArrayList<LCProdSetPojo> getLcProdSetPojos() {
        return lcProdSetPojos;
    }

    public void setLcProdSetPojos(ArrayList<LCProdSetPojo> lcProdSetPojos) {
        this.lcProdSetPojos = lcProdSetPojos;
    }

    public ArrayList<LASignAgentPojo> getLaSignAgentPojos() {
        return laSignAgentPojos;
    }

    public void setLaSignAgentPojos(ArrayList<LASignAgentPojo> laSignAgentPojos) {
        this.laSignAgentPojos = laSignAgentPojos;
    }

    public ArrayList<LKTransInfoPojo> getLkTransInfoPojos() {
        return lkTransInfoPojos;
    }

    public void setLkTransInfoPojos(ArrayList<LKTransInfoPojo> lkTransInfoPojos) {
        this.lkTransInfoPojos = lkTransInfoPojos;
    }

    public ArrayList<LJTempFeePojo> getlJTempFeePojos() {
        return lJTempFeePojos;
    }

    public void setlJTempFeePojos(ArrayList<LJTempFeePojo> lJTempFeePojos) {
        this.lJTempFeePojos = lJTempFeePojos;
    }

    public ArrayList<LJTempFeeClassPojo> getlJTempFeeClassPojos() {
        return lJTempFeeClassPojos;
    }

    public void setlJTempFeeClassPojos(ArrayList<LJTempFeeClassPojo> lJTempFeeClassPojos) {
        this.lJTempFeeClassPojos = lJTempFeeClassPojos;
    }

    public LJAGetPojo getLjaGetPojo() {
        return ljaGetPojo;
    }

    public void setLjaGetPojo(LJAGetPojo ljaGetPojo) {
        this.ljaGetPojo = ljaGetPojo;
    }

    public LJAGetOtherPojo getLjaGetOtherPojo() {
        return ljaGetOtherPojo;
    }

    public void setLjaGetOtherPojo(LJAGetOtherPojo ljaGetOtherPojo) {
        this.ljaGetOtherPojo = ljaGetOtherPojo;
    }

    public ArrayList<LCPadImagePojo> getLcPadImagePojos() {
        return lcPadImagePojos;
    }

    public void setLcPadImagePojos(ArrayList<LCPadImagePojo> lcPadImagePojos) {
        this.lcPadImagePojos = lcPadImagePojos;
    }

    @Override
    public String toString() {
        return "LCAllDataCLPojo{" +
                "lkTransTracksPojos=" + lkTransTracksPojos +
                ", lcPolPojos=" + lcPolPojos +
                ", lcContPojos=" + lcContPojos +
                ", lcContStatePojos=" + lcContStatePojos +
                ", lcPremPojos=" + lcPremPojos +
                ", lcDutyPojos=" + lcDutyPojos +
                ", lcAddressPojos=" + lcAddressPojos +
                ", lcInsuredPojos=" + lcInsuredPojos +
                ", lcBnfPojos=" + lcBnfPojos +
                ", ldPersonPojos=" + ldPersonPojos +
                ", lcGetPojos=" + lcGetPojos +
                ", lcAppntPojos=" + lcAppntPojos +
                ", lcGetToAccPojos=" + lcGetToAccPojos +
                ", lcPremToAccPojos=" + lcPremToAccPojos +
                ", lcInsureAccPojos=" + lcInsureAccPojos +
                ", lcInsureAccTracePojos=" + lcInsureAccTracePojos +
                ", lcInsureAccClassPojos=" + lcInsureAccClassPojos +
                ", lcInsureAccFeePojos=" + lcInsureAccFeePojos +
                ", lcInsureAccFeeTracePojos=" + lcInsureAccFeeTracePojos +
                ", lcInsureAccClassFeePojos=" + lcInsureAccClassFeePojos +
                ", ljAPayPojos=" + ljAPayPojos +
                ", ljAPayPersonPojos=" + ljAPayPersonPojos +
                ", ljSPayPojos=" + ljSPayPojos +
                ", ljSPayPersonPojos=" + ljSPayPersonPojos +
                ", lcCusTomerImpartPojos=" + lcCusTomerImpartPojos +
                ", lCCUWMasterPojos=" + lCCUWMasterPojos +
                ", lCCUWSubPojos=" + lCCUWSubPojos +
                ", lCUWMasterPojos=" + lCUWMasterPojos +
                ", lcAppntLinkManInfoPojos=" + lcAppntLinkManInfoPojos +
                ", lCUWErrorPojos=" + lCUWErrorPojos +
                ", lCCUWErrorPojos=" + lCCUWErrorPojos +
                ", lCUWSubPojos=" + lCUWSubPojos +
                ", lCCustomerImpartParamsPojos=" + lCCustomerImpartParamsPojos +
                ", lJTempFeePojos=" + lJTempFeePojos +
                ", lJTempFeeClassPojos=" + lJTempFeeClassPojos +
                ", lcCustomerImpartDetailPojos=" + lcCustomerImpartDetailPojos +
                ", lCSpecPojos=" + lCSpecPojos +
                ", laCommisionDetailPojos=" + laCommisionDetailPojos +
                ", lcAccountPojos=" + lcAccountPojos +
                ", lkTransStatusPojos=" + lkTransStatusPojos +
                ", vms_output_transPojos=" + vms_output_transPojos +
                ", vms_output_trans_totalPojos=" + vms_output_trans_totalPojos +
                ", vms_output_customerPojos=" + vms_output_customerPojos +
                ", lcAudVidRecordPojos=" + lcAudVidRecordPojos +
                ", lcProdSetPojos=" + lcProdSetPojos +
                ", laSignAgentPojos=" + laSignAgentPojos +
                ", lkTransInfoPojos=" + lkTransInfoPojos +
                ", lvInsureAccPojos=" + lvInsureAccPojos +
                ", lvInsureAccTracePojos=" + lvInsureAccTracePojos +
                ", lcGrpContPojos=" + lcGrpContPojos +
                ", lcGrpPolPojos=" + lcGrpPolPojos +
                ", lcGrpAppntPojos=" + lcGrpAppntPojos +
                ", lcGrpAddressPojos=" + lcGrpAddressPojos +
                ", lcAgentToContPojos=" + lcAgentToContPojos +
                ", lcAgentComInfoPojos=" + lcAgentComInfoPojos +
                ", lcPolicyInfoPojos=" + lcPolicyInfoPojos +
                ", lcContPlanPojos=" + lcContPlanPojos +
                ", lcDutySubPojos=" + lcDutySubPojos +
                ", lcContPlanDetailPojos=" + lcContPlanDetailPojos +
                ", lcContPlanDetailSubPojos=" + lcContPlanDetailSubPojos +
                ", ldGrpPojos=" + ldGrpPojos +
                ", lcApiPojos=" + lcApiPojos +
                ", lcaipmultiyearcontPojos=" + lcaipmultiyearcontPojos +
                ", ljaPayGrpPojos=" + ljaPayGrpPojos +
                ", lcaipContRegisterPojos=" + lcaipContRegisterPojos +
                ", ljaGetPojo=" + ljaGetPojo +
                ", ljaGetOtherPojo=" + ljaGetOtherPojo +
                ", lcPadImagePojos=" + lcPadImagePojos +
                ", lcWechatRelaTionPojo=" + lcWechatRelaTionPojo +
                '}';
    }
}
