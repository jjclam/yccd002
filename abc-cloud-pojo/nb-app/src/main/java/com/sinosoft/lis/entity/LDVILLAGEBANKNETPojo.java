/**
 * Copyright (c) 2019 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.entity;

import java.io.*;
import java.util.Date;

import com.sinosoft.cloud.cache.annotation.RedisPrimaryHKey;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.StrTool;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * <p>ClassName: LDVILLAGEBANKNETPojo </p>
 * <p>Copyright: Copyright (c) 2019</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2019-01-09
 */
public class LDVILLAGEBANKNETPojo implements  Pojo,Serializable {
    // @Field
    /** Bank_brchnet_code */
    @RedisPrimaryHKey
    private String BANK_BRCHNET_CODE; 
    /** Bank_brchnet_name */
    private String BANK_BRCHNET_NAME; 
    /** Bank_inside_code */
    private String BANK_INSIDE_CODE; 
    /** Agentcode */
    private String AGENTCODE; 
    /** Agency_brchnet_code */
    private String AGENCY_BRCHNET_CODE; 
    /** Agencycode */
    private String AGENCYCODE; 
    /** Makedate */
    private String  MAKEDATE;
    /** Maketime */
    private String MAKETIME; 
    /** Modifydate */
    private String  MODIFYDATE;
    /** Modifytime */
    private String MODIFYTIME; 


    public static final int FIELDNUM = 10;    // 数据库表的字段个数

    private FDate fDate = new FDate();        // 处理日期
    public String getBANK_BRCHNET_CODE() {
        return BANK_BRCHNET_CODE;
    }
    public void setBANK_BRCHNET_CODE(String aBANK_BRCHNET_CODE) {
        BANK_BRCHNET_CODE = aBANK_BRCHNET_CODE;
    }
    public String getBANK_BRCHNET_NAME() {
        return BANK_BRCHNET_NAME;
    }
    public void setBANK_BRCHNET_NAME(String aBANK_BRCHNET_NAME) {
        BANK_BRCHNET_NAME = aBANK_BRCHNET_NAME;
    }
    public String getBANK_INSIDE_CODE() {
        return BANK_INSIDE_CODE;
    }
    public void setBANK_INSIDE_CODE(String aBANK_INSIDE_CODE) {
        BANK_INSIDE_CODE = aBANK_INSIDE_CODE;
    }
    public String getAGENTCODE() {
        return AGENTCODE;
    }
    public void setAGENTCODE(String aAGENTCODE) {
        AGENTCODE = aAGENTCODE;
    }
    public String getAGENCY_BRCHNET_CODE() {
        return AGENCY_BRCHNET_CODE;
    }
    public void setAGENCY_BRCHNET_CODE(String aAGENCY_BRCHNET_CODE) {
        AGENCY_BRCHNET_CODE = aAGENCY_BRCHNET_CODE;
    }
    public String getAGENCYCODE() {
        return AGENCYCODE;
    }
    public void setAGENCYCODE(String aAGENCYCODE) {
        AGENCYCODE = aAGENCYCODE;
    }
    public String getMAKEDATE() {
        return MAKEDATE;
    }
    public void setMAKEDATE(String aMAKEDATE) {
        MAKEDATE = aMAKEDATE;
    }
    public String getMAKETIME() {
        return MAKETIME;
    }
    public void setMAKETIME(String aMAKETIME) {
        MAKETIME = aMAKETIME;
    }
    public String getMODIFYDATE() {
        return MODIFYDATE;
    }
    public void setMODIFYDATE(String aMODIFYDATE) {
        MODIFYDATE = aMODIFYDATE;
    }
    public String getMODIFYTIME() {
        return MODIFYTIME;
    }
    public void setMODIFYTIME(String aMODIFYTIME) {
        MODIFYTIME = aMODIFYTIME;
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("BANK_BRCHNET_CODE") ) {
            return 0;
        }
        if( strFieldName.equals("BANK_BRCHNET_NAME") ) {
            return 1;
        }
        if( strFieldName.equals("BANK_INSIDE_CODE") ) {
            return 2;
        }
        if( strFieldName.equals("AGENTCODE") ) {
            return 3;
        }
        if( strFieldName.equals("AGENCY_BRCHNET_CODE") ) {
            return 4;
        }
        if( strFieldName.equals("AGENCYCODE") ) {
            return 5;
        }
        if( strFieldName.equals("MAKEDATE") ) {
            return 6;
        }
        if( strFieldName.equals("MAKETIME") ) {
            return 7;
        }
        if( strFieldName.equals("MODIFYDATE") ) {
            return 8;
        }
        if( strFieldName.equals("MODIFYTIME") ) {
            return 9;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "BANK_BRCHNET_CODE";
                break;
            case 1:
                strFieldName = "BANK_BRCHNET_NAME";
                break;
            case 2:
                strFieldName = "BANK_INSIDE_CODE";
                break;
            case 3:
                strFieldName = "AGENTCODE";
                break;
            case 4:
                strFieldName = "AGENCY_BRCHNET_CODE";
                break;
            case 5:
                strFieldName = "AGENCYCODE";
                break;
            case 6:
                strFieldName = "MAKEDATE";
                break;
            case 7:
                strFieldName = "MAKETIME";
                break;
            case 8:
                strFieldName = "MODIFYDATE";
                break;
            case 9:
                strFieldName = "MODIFYTIME";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "BANK_BRCHNET_CODE":
                return Schema.TYPE_STRING;
            case "BANK_BRCHNET_NAME":
                return Schema.TYPE_STRING;
            case "BANK_INSIDE_CODE":
                return Schema.TYPE_STRING;
            case "AGENTCODE":
                return Schema.TYPE_STRING;
            case "AGENCY_BRCHNET_CODE":
                return Schema.TYPE_STRING;
            case "AGENCYCODE":
                return Schema.TYPE_STRING;
            case "MAKEDATE":
                return Schema.TYPE_STRING;
            case "MAKETIME":
                return Schema.TYPE_STRING;
            case "MODIFYDATE":
                return Schema.TYPE_STRING;
            case "MODIFYTIME":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_STRING;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_STRING;
            case 6:
                return Schema.TYPE_STRING;
            case 7:
                return Schema.TYPE_STRING;
            case 8:
                return Schema.TYPE_STRING;
            case 9:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("BANK_BRCHNET_CODE")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BANK_BRCHNET_CODE));
        }
        if (FCode.equalsIgnoreCase("BANK_BRCHNET_NAME")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BANK_BRCHNET_NAME));
        }
        if (FCode.equalsIgnoreCase("BANK_INSIDE_CODE")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BANK_INSIDE_CODE));
        }
        if (FCode.equalsIgnoreCase("AGENTCODE")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AGENTCODE));
        }
        if (FCode.equalsIgnoreCase("AGENCY_BRCHNET_CODE")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AGENCY_BRCHNET_CODE));
        }
        if (FCode.equalsIgnoreCase("AGENCYCODE")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AGENCYCODE));
        }
        if (FCode.equalsIgnoreCase("MAKEDATE")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MAKEDATE));
        }
        if (FCode.equalsIgnoreCase("MAKETIME")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MAKETIME));
        }
        if (FCode.equalsIgnoreCase("MODIFYDATE")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MODIFYDATE));
        }
        if (FCode.equalsIgnoreCase("MODIFYTIME")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MODIFYTIME));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = String.valueOf(BANK_BRCHNET_CODE);
                break;
            case 1:
                strFieldValue = String.valueOf(BANK_BRCHNET_NAME);
                break;
            case 2:
                strFieldValue = String.valueOf(BANK_INSIDE_CODE);
                break;
            case 3:
                strFieldValue = String.valueOf(AGENTCODE);
                break;
            case 4:
                strFieldValue = String.valueOf(AGENCY_BRCHNET_CODE);
                break;
            case 5:
                strFieldValue = String.valueOf(AGENCYCODE);
                break;
            case 6:
                strFieldValue = String.valueOf(MAKEDATE);
                break;
            case 7:
                strFieldValue = String.valueOf(MAKETIME);
                break;
            case 8:
                strFieldValue = String.valueOf(MODIFYDATE);
                break;
            case 9:
                strFieldValue = String.valueOf(MODIFYTIME);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("BANK_BRCHNET_CODE")) {
            if( FValue != null && !FValue.equals(""))
            {
                BANK_BRCHNET_CODE = FValue.trim();
            }
            else
                BANK_BRCHNET_CODE = null;
        }
        if (FCode.equalsIgnoreCase("BANK_BRCHNET_NAME")) {
            if( FValue != null && !FValue.equals(""))
            {
                BANK_BRCHNET_NAME = FValue.trim();
            }
            else
                BANK_BRCHNET_NAME = null;
        }
        if (FCode.equalsIgnoreCase("BANK_INSIDE_CODE")) {
            if( FValue != null && !FValue.equals(""))
            {
                BANK_INSIDE_CODE = FValue.trim();
            }
            else
                BANK_INSIDE_CODE = null;
        }
        if (FCode.equalsIgnoreCase("AGENTCODE")) {
            if( FValue != null && !FValue.equals(""))
            {
                AGENTCODE = FValue.trim();
            }
            else
                AGENTCODE = null;
        }
        if (FCode.equalsIgnoreCase("AGENCY_BRCHNET_CODE")) {
            if( FValue != null && !FValue.equals(""))
            {
                AGENCY_BRCHNET_CODE = FValue.trim();
            }
            else
                AGENCY_BRCHNET_CODE = null;
        }
        if (FCode.equalsIgnoreCase("AGENCYCODE")) {
            if( FValue != null && !FValue.equals(""))
            {
                AGENCYCODE = FValue.trim();
            }
            else
                AGENCYCODE = null;
        }
        if (FCode.equalsIgnoreCase("MAKEDATE")) {
            if( FValue != null && !FValue.equals(""))
            {
                MAKEDATE = FValue.trim();
            }
            else
                MAKEDATE = null;
        }
        if (FCode.equalsIgnoreCase("MAKETIME")) {
            if( FValue != null && !FValue.equals(""))
            {
                MAKETIME = FValue.trim();
            }
            else
                MAKETIME = null;
        }
        if (FCode.equalsIgnoreCase("MODIFYDATE")) {
            if( FValue != null && !FValue.equals(""))
            {
                MODIFYDATE = FValue.trim();
            }
            else
                MODIFYDATE = null;
        }
        if (FCode.equalsIgnoreCase("MODIFYTIME")) {
            if( FValue != null && !FValue.equals(""))
            {
                MODIFYTIME = FValue.trim();
            }
            else
                MODIFYTIME = null;
        }
        return true;
    }


    public String toString() {
    return "LDVILLAGEBANKNETPojo [" +
            "BANK_BRCHNET_CODE="+BANK_BRCHNET_CODE +
            ", BANK_BRCHNET_NAME="+BANK_BRCHNET_NAME +
            ", BANK_INSIDE_CODE="+BANK_INSIDE_CODE +
            ", AGENTCODE="+AGENTCODE +
            ", AGENCY_BRCHNET_CODE="+AGENCY_BRCHNET_CODE +
            ", AGENCYCODE="+AGENCYCODE +
            ", MAKEDATE="+MAKEDATE +
            ", MAKETIME="+MAKETIME +
            ", MODIFYDATE="+MODIFYDATE +
            ", MODIFYTIME="+MODIFYTIME +"]";
    }
}
