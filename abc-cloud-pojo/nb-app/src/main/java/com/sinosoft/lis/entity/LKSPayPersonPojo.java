/**
 * Copyright (c) 2017 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.entity;

import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.Pojo;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;

import java.io.Serializable;

/**
 * <p>ClassName: LKSPayPersonPojo </p>
 * <p>Copyright: Copyright (c) 2017</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2017-10-23
 */
public class LKSPayPersonPojo implements Pojo,Serializable {
    // @Field
    /** Id */
    private long SPayPersonID; 
    /** Shardingid */
    private String ShardingID; 
    /** 保单号码 */
    private String PolNo; 
    /** 第几次交费 */
    private int PayCount; 
    /** 集体合同号码 */
    private String GrpContNo; 
    /** 集体保单号码 */
    private String GrpPolNo; 
    /** 合同号码 */
    private String ContNo; 
    /** 管理机构 */
    private String ManageCom; 
    /** 代理机构 */
    private String AgentCom; 
    /** 代理机构内部分类 */
    private String AgentType; 
    /** 险种编码 */
    private String RiskCode; 
    /** 代理人编码 */
    private String AgentCode; 
    /** 代理人组别 */
    private String AgentGroup; 
    /** 续保收费标记 */
    private String PayTypeFlag; 
    /** 投保人客户号码 */
    private String AppntNo; 
    /** 通知书号码 */
    private String GetNoticeNo; 
    /** 交费目的分类 */
    private String PayAimClass; 
    /** 责任编码 */
    private String DutyCode; 
    /** 交费计划编码 */
    private String PayPlanCode; 
    /** 总应交金额 */
    private double SumDuePayMoney; 
    /** 总实交金额 */
    private double SumActuPayMoney; 
    /** 交费间隔 */
    private int PayIntv; 
    /** 交费日期 */
    private String  PayDate;
    /** 交费类型 */
    private String PayType; 
    /** 原交至日期 */
    private String  LastPayToDate;
    /** 现交至日期 */
    private String  CurPayToDate;
    /** 转入保险帐户状态 */
    private String InInsuAccState; 
    /** 银行编码 */
    private String BankCode; 
    /** 银行帐号 */
    private String BankAccNo; 
    /** 银行在途标志 */
    private String BankOnTheWayFlag; 
    /** 银行转帐成功标记 */
    private String BankSuccFlag; 
    /** 复核人编码 */
    private String ApproveCode; 
    /** 复核日期 */
    private String  ApproveDate;
    /** 复核时间 */
    private String ApproveTime; 
    /** 流水号 */
    private String SerialNo; 
    /** 录入标志 */
    private String InputFlag; 
    /** 操作员 */
    private String Operator; 
    /** 入机日期 */
    private String  MakeDate;
    /** 入机时间 */
    private String MakeTime; 
    /** 最后一次修改日期 */
    private String  ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime; 
    /** 批单号 */
    private String EdorNo; 
    /** 主险保单年度 */
    private int MainPolYear; 
    /** 其他号码 */
    private String OtherNo; 
    /** 其他号码类型 */
    private String OtherNoType; 
    /** 印刷号码 */
    private String PrtNo; 
    /** 销售渠道 */
    private String SaleChnl; 
    /** 收费机构 */
    private String ChargeCom; 
    /** 投保人姓名 */
    private String APPntName; 
    /** 险类编码 */
    private String KindCode; 
    /** 缴至日期 */
    private String  PaytoDate;
    /** 终交日期 */
    private String  PayEndDate;
    /** 签单日期 */
    private String  SignDate;
    /** 生效日期 */
    private String  CValiDate;
    /** 终交年龄年期 */
    private int PayEndYear; 
    /** 缴费金额 */
    private double PayMoney; 


    public static final int FIELDNUM = 56;    // 数据库表的字段个数

    private FDate fDate = new FDate();        // 处理日期
    public long getSPayPersonID() {
        return SPayPersonID;
    }
    public void setSPayPersonID(long aSPayPersonID) {
        SPayPersonID = aSPayPersonID;
    }
    public void setSPayPersonID(String aSPayPersonID) {
        if (aSPayPersonID != null && !aSPayPersonID.equals("")) {
            SPayPersonID = new Long(aSPayPersonID).longValue();
        }
    }

    public String getShardingID() {
        return ShardingID;
    }
    public void setShardingID(String aShardingID) {
        ShardingID = aShardingID;
    }
    public String getPolNo() {
        return PolNo;
    }
    public void setPolNo(String aPolNo) {
        PolNo = aPolNo;
    }
    public int getPayCount() {
        return PayCount;
    }
    public void setPayCount(int aPayCount) {
        PayCount = aPayCount;
    }
    public void setPayCount(String aPayCount) {
        if (aPayCount != null && !aPayCount.equals("")) {
            Integer tInteger = new Integer(aPayCount);
            int i = tInteger.intValue();
            PayCount = i;
        }
    }

    public String getGrpContNo() {
        return GrpContNo;
    }
    public void setGrpContNo(String aGrpContNo) {
        GrpContNo = aGrpContNo;
    }
    public String getGrpPolNo() {
        return GrpPolNo;
    }
    public void setGrpPolNo(String aGrpPolNo) {
        GrpPolNo = aGrpPolNo;
    }
    public String getContNo() {
        return ContNo;
    }
    public void setContNo(String aContNo) {
        ContNo = aContNo;
    }
    public String getManageCom() {
        return ManageCom;
    }
    public void setManageCom(String aManageCom) {
        ManageCom = aManageCom;
    }
    public String getAgentCom() {
        return AgentCom;
    }
    public void setAgentCom(String aAgentCom) {
        AgentCom = aAgentCom;
    }
    public String getAgentType() {
        return AgentType;
    }
    public void setAgentType(String aAgentType) {
        AgentType = aAgentType;
    }
    public String getRiskCode() {
        return RiskCode;
    }
    public void setRiskCode(String aRiskCode) {
        RiskCode = aRiskCode;
    }
    public String getAgentCode() {
        return AgentCode;
    }
    public void setAgentCode(String aAgentCode) {
        AgentCode = aAgentCode;
    }
    public String getAgentGroup() {
        return AgentGroup;
    }
    public void setAgentGroup(String aAgentGroup) {
        AgentGroup = aAgentGroup;
    }
    public String getPayTypeFlag() {
        return PayTypeFlag;
    }
    public void setPayTypeFlag(String aPayTypeFlag) {
        PayTypeFlag = aPayTypeFlag;
    }
    public String getAppntNo() {
        return AppntNo;
    }
    public void setAppntNo(String aAppntNo) {
        AppntNo = aAppntNo;
    }
    public String getGetNoticeNo() {
        return GetNoticeNo;
    }
    public void setGetNoticeNo(String aGetNoticeNo) {
        GetNoticeNo = aGetNoticeNo;
    }
    public String getPayAimClass() {
        return PayAimClass;
    }
    public void setPayAimClass(String aPayAimClass) {
        PayAimClass = aPayAimClass;
    }
    public String getDutyCode() {
        return DutyCode;
    }
    public void setDutyCode(String aDutyCode) {
        DutyCode = aDutyCode;
    }
    public String getPayPlanCode() {
        return PayPlanCode;
    }
    public void setPayPlanCode(String aPayPlanCode) {
        PayPlanCode = aPayPlanCode;
    }
    public double getSumDuePayMoney() {
        return SumDuePayMoney;
    }
    public void setSumDuePayMoney(double aSumDuePayMoney) {
        SumDuePayMoney = aSumDuePayMoney;
    }
    public void setSumDuePayMoney(String aSumDuePayMoney) {
        if (aSumDuePayMoney != null && !aSumDuePayMoney.equals("")) {
            Double tDouble = new Double(aSumDuePayMoney);
            double d = tDouble.doubleValue();
            SumDuePayMoney = d;
        }
    }

    public double getSumActuPayMoney() {
        return SumActuPayMoney;
    }
    public void setSumActuPayMoney(double aSumActuPayMoney) {
        SumActuPayMoney = aSumActuPayMoney;
    }
    public void setSumActuPayMoney(String aSumActuPayMoney) {
        if (aSumActuPayMoney != null && !aSumActuPayMoney.equals("")) {
            Double tDouble = new Double(aSumActuPayMoney);
            double d = tDouble.doubleValue();
            SumActuPayMoney = d;
        }
    }

    public int getPayIntv() {
        return PayIntv;
    }
    public void setPayIntv(int aPayIntv) {
        PayIntv = aPayIntv;
    }
    public void setPayIntv(String aPayIntv) {
        if (aPayIntv != null && !aPayIntv.equals("")) {
            Integer tInteger = new Integer(aPayIntv);
            int i = tInteger.intValue();
            PayIntv = i;
        }
    }

    public String getPayDate() {
        return PayDate;
    }
    public void setPayDate(String aPayDate) {
        PayDate = aPayDate;
    }
    public String getPayType() {
        return PayType;
    }
    public void setPayType(String aPayType) {
        PayType = aPayType;
    }
    public String getLastPayToDate() {
        return LastPayToDate;
    }
    public void setLastPayToDate(String aLastPayToDate) {
        LastPayToDate = aLastPayToDate;
    }
    public String getCurPayToDate() {
        return CurPayToDate;
    }
    public void setCurPayToDate(String aCurPayToDate) {
        CurPayToDate = aCurPayToDate;
    }
    public String getInInsuAccState() {
        return InInsuAccState;
    }
    public void setInInsuAccState(String aInInsuAccState) {
        InInsuAccState = aInInsuAccState;
    }
    public String getBankCode() {
        return BankCode;
    }
    public void setBankCode(String aBankCode) {
        BankCode = aBankCode;
    }
    public String getBankAccNo() {
        return BankAccNo;
    }
    public void setBankAccNo(String aBankAccNo) {
        BankAccNo = aBankAccNo;
    }
    public String getBankOnTheWayFlag() {
        return BankOnTheWayFlag;
    }
    public void setBankOnTheWayFlag(String aBankOnTheWayFlag) {
        BankOnTheWayFlag = aBankOnTheWayFlag;
    }
    public String getBankSuccFlag() {
        return BankSuccFlag;
    }
    public void setBankSuccFlag(String aBankSuccFlag) {
        BankSuccFlag = aBankSuccFlag;
    }
    public String getApproveCode() {
        return ApproveCode;
    }
    public void setApproveCode(String aApproveCode) {
        ApproveCode = aApproveCode;
    }
    public String getApproveDate() {
        return ApproveDate;
    }
    public void setApproveDate(String aApproveDate) {
        ApproveDate = aApproveDate;
    }
    public String getApproveTime() {
        return ApproveTime;
    }
    public void setApproveTime(String aApproveTime) {
        ApproveTime = aApproveTime;
    }
    public String getSerialNo() {
        return SerialNo;
    }
    public void setSerialNo(String aSerialNo) {
        SerialNo = aSerialNo;
    }
    public String getInputFlag() {
        return InputFlag;
    }
    public void setInputFlag(String aInputFlag) {
        InputFlag = aInputFlag;
    }
    public String getOperator() {
        return Operator;
    }
    public void setOperator(String aOperator) {
        Operator = aOperator;
    }
    public String getMakeDate() {
        return MakeDate;
    }
    public void setMakeDate(String aMakeDate) {
        MakeDate = aMakeDate;
    }
    public String getMakeTime() {
        return MakeTime;
    }
    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }
    public String getModifyDate() {
        return ModifyDate;
    }
    public void setModifyDate(String aModifyDate) {
        ModifyDate = aModifyDate;
    }
    public String getModifyTime() {
        return ModifyTime;
    }
    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }
    public String getEdorNo() {
        return EdorNo;
    }
    public void setEdorNo(String aEdorNo) {
        EdorNo = aEdorNo;
    }
    public int getMainPolYear() {
        return MainPolYear;
    }
    public void setMainPolYear(int aMainPolYear) {
        MainPolYear = aMainPolYear;
    }
    public void setMainPolYear(String aMainPolYear) {
        if (aMainPolYear != null && !aMainPolYear.equals("")) {
            Integer tInteger = new Integer(aMainPolYear);
            int i = tInteger.intValue();
            MainPolYear = i;
        }
    }

    public String getOtherNo() {
        return OtherNo;
    }
    public void setOtherNo(String aOtherNo) {
        OtherNo = aOtherNo;
    }
    public String getOtherNoType() {
        return OtherNoType;
    }
    public void setOtherNoType(String aOtherNoType) {
        OtherNoType = aOtherNoType;
    }
    public String getPrtNo() {
        return PrtNo;
    }
    public void setPrtNo(String aPrtNo) {
        PrtNo = aPrtNo;
    }
    public String getSaleChnl() {
        return SaleChnl;
    }
    public void setSaleChnl(String aSaleChnl) {
        SaleChnl = aSaleChnl;
    }
    public String getChargeCom() {
        return ChargeCom;
    }
    public void setChargeCom(String aChargeCom) {
        ChargeCom = aChargeCom;
    }
    public String getAPPntName() {
        return APPntName;
    }
    public void setAPPntName(String aAPPntName) {
        APPntName = aAPPntName;
    }
    public String getKindCode() {
        return KindCode;
    }
    public void setKindCode(String aKindCode) {
        KindCode = aKindCode;
    }
    public String getPaytoDate() {
        return PaytoDate;
    }
    public void setPaytoDate(String aPaytoDate) {
        PaytoDate = aPaytoDate;
    }
    public String getPayEndDate() {
        return PayEndDate;
    }
    public void setPayEndDate(String aPayEndDate) {
        PayEndDate = aPayEndDate;
    }
    public String getSignDate() {
        return SignDate;
    }
    public void setSignDate(String aSignDate) {
        SignDate = aSignDate;
    }
    public String getCValiDate() {
        return CValiDate;
    }
    public void setCValiDate(String aCValiDate) {
        CValiDate = aCValiDate;
    }
    public int getPayEndYear() {
        return PayEndYear;
    }
    public void setPayEndYear(int aPayEndYear) {
        PayEndYear = aPayEndYear;
    }
    public void setPayEndYear(String aPayEndYear) {
        if (aPayEndYear != null && !aPayEndYear.equals("")) {
            Integer tInteger = new Integer(aPayEndYear);
            int i = tInteger.intValue();
            PayEndYear = i;
        }
    }

    public double getPayMoney() {
        return PayMoney;
    }
    public void setPayMoney(double aPayMoney) {
        PayMoney = aPayMoney;
    }
    public void setPayMoney(String aPayMoney) {
        if (aPayMoney != null && !aPayMoney.equals("")) {
            Double tDouble = new Double(aPayMoney);
            double d = tDouble.doubleValue();
            PayMoney = d;
        }
    }


    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("SPayPersonID") ) {
            return 0;
        }
        if( strFieldName.equals("ShardingID") ) {
            return 1;
        }
        if( strFieldName.equals("PolNo") ) {
            return 2;
        }
        if( strFieldName.equals("PayCount") ) {
            return 3;
        }
        if( strFieldName.equals("GrpContNo") ) {
            return 4;
        }
        if( strFieldName.equals("GrpPolNo") ) {
            return 5;
        }
        if( strFieldName.equals("ContNo") ) {
            return 6;
        }
        if( strFieldName.equals("ManageCom") ) {
            return 7;
        }
        if( strFieldName.equals("AgentCom") ) {
            return 8;
        }
        if( strFieldName.equals("AgentType") ) {
            return 9;
        }
        if( strFieldName.equals("RiskCode") ) {
            return 10;
        }
        if( strFieldName.equals("AgentCode") ) {
            return 11;
        }
        if( strFieldName.equals("AgentGroup") ) {
            return 12;
        }
        if( strFieldName.equals("PayTypeFlag") ) {
            return 13;
        }
        if( strFieldName.equals("AppntNo") ) {
            return 14;
        }
        if( strFieldName.equals("GetNoticeNo") ) {
            return 15;
        }
        if( strFieldName.equals("PayAimClass") ) {
            return 16;
        }
        if( strFieldName.equals("DutyCode") ) {
            return 17;
        }
        if( strFieldName.equals("PayPlanCode") ) {
            return 18;
        }
        if( strFieldName.equals("SumDuePayMoney") ) {
            return 19;
        }
        if( strFieldName.equals("SumActuPayMoney") ) {
            return 20;
        }
        if( strFieldName.equals("PayIntv") ) {
            return 21;
        }
        if( strFieldName.equals("PayDate") ) {
            return 22;
        }
        if( strFieldName.equals("PayType") ) {
            return 23;
        }
        if( strFieldName.equals("LastPayToDate") ) {
            return 24;
        }
        if( strFieldName.equals("CurPayToDate") ) {
            return 25;
        }
        if( strFieldName.equals("InInsuAccState") ) {
            return 26;
        }
        if( strFieldName.equals("BankCode") ) {
            return 27;
        }
        if( strFieldName.equals("BankAccNo") ) {
            return 28;
        }
        if( strFieldName.equals("BankOnTheWayFlag") ) {
            return 29;
        }
        if( strFieldName.equals("BankSuccFlag") ) {
            return 30;
        }
        if( strFieldName.equals("ApproveCode") ) {
            return 31;
        }
        if( strFieldName.equals("ApproveDate") ) {
            return 32;
        }
        if( strFieldName.equals("ApproveTime") ) {
            return 33;
        }
        if( strFieldName.equals("SerialNo") ) {
            return 34;
        }
        if( strFieldName.equals("InputFlag") ) {
            return 35;
        }
        if( strFieldName.equals("Operator") ) {
            return 36;
        }
        if( strFieldName.equals("MakeDate") ) {
            return 37;
        }
        if( strFieldName.equals("MakeTime") ) {
            return 38;
        }
        if( strFieldName.equals("ModifyDate") ) {
            return 39;
        }
        if( strFieldName.equals("ModifyTime") ) {
            return 40;
        }
        if( strFieldName.equals("EdorNo") ) {
            return 41;
        }
        if( strFieldName.equals("MainPolYear") ) {
            return 42;
        }
        if( strFieldName.equals("OtherNo") ) {
            return 43;
        }
        if( strFieldName.equals("OtherNoType") ) {
            return 44;
        }
        if( strFieldName.equals("PrtNo") ) {
            return 45;
        }
        if( strFieldName.equals("SaleChnl") ) {
            return 46;
        }
        if( strFieldName.equals("ChargeCom") ) {
            return 47;
        }
        if( strFieldName.equals("APPntName") ) {
            return 48;
        }
        if( strFieldName.equals("KindCode") ) {
            return 49;
        }
        if( strFieldName.equals("PaytoDate") ) {
            return 50;
        }
        if( strFieldName.equals("PayEndDate") ) {
            return 51;
        }
        if( strFieldName.equals("SignDate") ) {
            return 52;
        }
        if( strFieldName.equals("CValiDate") ) {
            return 53;
        }
        if( strFieldName.equals("PayEndYear") ) {
            return 54;
        }
        if( strFieldName.equals("PayMoney") ) {
            return 55;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "SPayPersonID";
                break;
            case 1:
                strFieldName = "ShardingID";
                break;
            case 2:
                strFieldName = "PolNo";
                break;
            case 3:
                strFieldName = "PayCount";
                break;
            case 4:
                strFieldName = "GrpContNo";
                break;
            case 5:
                strFieldName = "GrpPolNo";
                break;
            case 6:
                strFieldName = "ContNo";
                break;
            case 7:
                strFieldName = "ManageCom";
                break;
            case 8:
                strFieldName = "AgentCom";
                break;
            case 9:
                strFieldName = "AgentType";
                break;
            case 10:
                strFieldName = "RiskCode";
                break;
            case 11:
                strFieldName = "AgentCode";
                break;
            case 12:
                strFieldName = "AgentGroup";
                break;
            case 13:
                strFieldName = "PayTypeFlag";
                break;
            case 14:
                strFieldName = "AppntNo";
                break;
            case 15:
                strFieldName = "GetNoticeNo";
                break;
            case 16:
                strFieldName = "PayAimClass";
                break;
            case 17:
                strFieldName = "DutyCode";
                break;
            case 18:
                strFieldName = "PayPlanCode";
                break;
            case 19:
                strFieldName = "SumDuePayMoney";
                break;
            case 20:
                strFieldName = "SumActuPayMoney";
                break;
            case 21:
                strFieldName = "PayIntv";
                break;
            case 22:
                strFieldName = "PayDate";
                break;
            case 23:
                strFieldName = "PayType";
                break;
            case 24:
                strFieldName = "LastPayToDate";
                break;
            case 25:
                strFieldName = "CurPayToDate";
                break;
            case 26:
                strFieldName = "InInsuAccState";
                break;
            case 27:
                strFieldName = "BankCode";
                break;
            case 28:
                strFieldName = "BankAccNo";
                break;
            case 29:
                strFieldName = "BankOnTheWayFlag";
                break;
            case 30:
                strFieldName = "BankSuccFlag";
                break;
            case 31:
                strFieldName = "ApproveCode";
                break;
            case 32:
                strFieldName = "ApproveDate";
                break;
            case 33:
                strFieldName = "ApproveTime";
                break;
            case 34:
                strFieldName = "SerialNo";
                break;
            case 35:
                strFieldName = "InputFlag";
                break;
            case 36:
                strFieldName = "Operator";
                break;
            case 37:
                strFieldName = "MakeDate";
                break;
            case 38:
                strFieldName = "MakeTime";
                break;
            case 39:
                strFieldName = "ModifyDate";
                break;
            case 40:
                strFieldName = "ModifyTime";
                break;
            case 41:
                strFieldName = "EdorNo";
                break;
            case 42:
                strFieldName = "MainPolYear";
                break;
            case 43:
                strFieldName = "OtherNo";
                break;
            case 44:
                strFieldName = "OtherNoType";
                break;
            case 45:
                strFieldName = "PrtNo";
                break;
            case 46:
                strFieldName = "SaleChnl";
                break;
            case 47:
                strFieldName = "ChargeCom";
                break;
            case 48:
                strFieldName = "APPntName";
                break;
            case 49:
                strFieldName = "KindCode";
                break;
            case 50:
                strFieldName = "PaytoDate";
                break;
            case 51:
                strFieldName = "PayEndDate";
                break;
            case 52:
                strFieldName = "SignDate";
                break;
            case 53:
                strFieldName = "CValiDate";
                break;
            case 54:
                strFieldName = "PayEndYear";
                break;
            case 55:
                strFieldName = "PayMoney";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "SPAYPERSONID":
                return Schema.TYPE_LONG;
            case "SHARDINGID":
                return Schema.TYPE_STRING;
            case "POLNO":
                return Schema.TYPE_STRING;
            case "PAYCOUNT":
                return Schema.TYPE_INT;
            case "GRPCONTNO":
                return Schema.TYPE_STRING;
            case "GRPPOLNO":
                return Schema.TYPE_STRING;
            case "CONTNO":
                return Schema.TYPE_STRING;
            case "MANAGECOM":
                return Schema.TYPE_STRING;
            case "AGENTCOM":
                return Schema.TYPE_STRING;
            case "AGENTTYPE":
                return Schema.TYPE_STRING;
            case "RISKCODE":
                return Schema.TYPE_STRING;
            case "AGENTCODE":
                return Schema.TYPE_STRING;
            case "AGENTGROUP":
                return Schema.TYPE_STRING;
            case "PAYTYPEFLAG":
                return Schema.TYPE_STRING;
            case "APPNTNO":
                return Schema.TYPE_STRING;
            case "GETNOTICENO":
                return Schema.TYPE_STRING;
            case "PAYAIMCLASS":
                return Schema.TYPE_STRING;
            case "DUTYCODE":
                return Schema.TYPE_STRING;
            case "PAYPLANCODE":
                return Schema.TYPE_STRING;
            case "SUMDUEPAYMONEY":
                return Schema.TYPE_DOUBLE;
            case "SUMACTUPAYMONEY":
                return Schema.TYPE_DOUBLE;
            case "PAYINTV":
                return Schema.TYPE_INT;
            case "PAYDATE":
                return Schema.TYPE_STRING;
            case "PAYTYPE":
                return Schema.TYPE_STRING;
            case "LASTPAYTODATE":
                return Schema.TYPE_STRING;
            case "CURPAYTODATE":
                return Schema.TYPE_STRING;
            case "ININSUACCSTATE":
                return Schema.TYPE_STRING;
            case "BANKCODE":
                return Schema.TYPE_STRING;
            case "BANKACCNO":
                return Schema.TYPE_STRING;
            case "BANKONTHEWAYFLAG":
                return Schema.TYPE_STRING;
            case "BANKSUCCFLAG":
                return Schema.TYPE_STRING;
            case "APPROVECODE":
                return Schema.TYPE_STRING;
            case "APPROVEDATE":
                return Schema.TYPE_STRING;
            case "APPROVETIME":
                return Schema.TYPE_STRING;
            case "SERIALNO":
                return Schema.TYPE_STRING;
            case "INPUTFLAG":
                return Schema.TYPE_STRING;
            case "OPERATOR":
                return Schema.TYPE_STRING;
            case "MAKEDATE":
                return Schema.TYPE_STRING;
            case "MAKETIME":
                return Schema.TYPE_STRING;
            case "MODIFYDATE":
                return Schema.TYPE_STRING;
            case "MODIFYTIME":
                return Schema.TYPE_STRING;
            case "EDORNO":
                return Schema.TYPE_STRING;
            case "MAINPOLYEAR":
                return Schema.TYPE_INT;
            case "OTHERNO":
                return Schema.TYPE_STRING;
            case "OTHERNOTYPE":
                return Schema.TYPE_STRING;
            case "PRTNO":
                return Schema.TYPE_STRING;
            case "SALECHNL":
                return Schema.TYPE_STRING;
            case "CHARGECOM":
                return Schema.TYPE_STRING;
            case "APPNTNAME":
                return Schema.TYPE_STRING;
            case "KINDCODE":
                return Schema.TYPE_STRING;
            case "PAYTODATE":
                return Schema.TYPE_STRING;
            case "PAYENDDATE":
                return Schema.TYPE_STRING;
            case "SIGNDATE":
                return Schema.TYPE_STRING;
            case "CVALIDATE":
                return Schema.TYPE_STRING;
            case "PAYENDYEAR":
                return Schema.TYPE_INT;
            case "PAYMONEY":
                return Schema.TYPE_DOUBLE;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_LONG;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_INT;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_STRING;
            case 6:
                return Schema.TYPE_STRING;
            case 7:
                return Schema.TYPE_STRING;
            case 8:
                return Schema.TYPE_STRING;
            case 9:
                return Schema.TYPE_STRING;
            case 10:
                return Schema.TYPE_STRING;
            case 11:
                return Schema.TYPE_STRING;
            case 12:
                return Schema.TYPE_STRING;
            case 13:
                return Schema.TYPE_STRING;
            case 14:
                return Schema.TYPE_STRING;
            case 15:
                return Schema.TYPE_STRING;
            case 16:
                return Schema.TYPE_STRING;
            case 17:
                return Schema.TYPE_STRING;
            case 18:
                return Schema.TYPE_STRING;
            case 19:
                return Schema.TYPE_DOUBLE;
            case 20:
                return Schema.TYPE_DOUBLE;
            case 21:
                return Schema.TYPE_INT;
            case 22:
                return Schema.TYPE_STRING;
            case 23:
                return Schema.TYPE_STRING;
            case 24:
                return Schema.TYPE_STRING;
            case 25:
                return Schema.TYPE_STRING;
            case 26:
                return Schema.TYPE_STRING;
            case 27:
                return Schema.TYPE_STRING;
            case 28:
                return Schema.TYPE_STRING;
            case 29:
                return Schema.TYPE_STRING;
            case 30:
                return Schema.TYPE_STRING;
            case 31:
                return Schema.TYPE_STRING;
            case 32:
                return Schema.TYPE_STRING;
            case 33:
                return Schema.TYPE_STRING;
            case 34:
                return Schema.TYPE_STRING;
            case 35:
                return Schema.TYPE_STRING;
            case 36:
                return Schema.TYPE_STRING;
            case 37:
                return Schema.TYPE_STRING;
            case 38:
                return Schema.TYPE_STRING;
            case 39:
                return Schema.TYPE_STRING;
            case 40:
                return Schema.TYPE_STRING;
            case 41:
                return Schema.TYPE_STRING;
            case 42:
                return Schema.TYPE_INT;
            case 43:
                return Schema.TYPE_STRING;
            case 44:
                return Schema.TYPE_STRING;
            case 45:
                return Schema.TYPE_STRING;
            case 46:
                return Schema.TYPE_STRING;
            case 47:
                return Schema.TYPE_STRING;
            case 48:
                return Schema.TYPE_STRING;
            case 49:
                return Schema.TYPE_STRING;
            case 50:
                return Schema.TYPE_STRING;
            case 51:
                return Schema.TYPE_STRING;
            case 52:
                return Schema.TYPE_STRING;
            case 53:
                return Schema.TYPE_STRING;
            case 54:
                return Schema.TYPE_INT;
            case 55:
                return Schema.TYPE_DOUBLE;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("SPayPersonID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SPayPersonID));
        }
        if (FCode.equalsIgnoreCase("ShardingID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ShardingID));
        }
        if (FCode.equalsIgnoreCase("PolNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PolNo));
        }
        if (FCode.equalsIgnoreCase("PayCount")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PayCount));
        }
        if (FCode.equalsIgnoreCase("GrpContNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GrpContNo));
        }
        if (FCode.equalsIgnoreCase("GrpPolNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GrpPolNo));
        }
        if (FCode.equalsIgnoreCase("ContNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ContNo));
        }
        if (FCode.equalsIgnoreCase("ManageCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
        }
        if (FCode.equalsIgnoreCase("AgentCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentCom));
        }
        if (FCode.equalsIgnoreCase("AgentType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentType));
        }
        if (FCode.equalsIgnoreCase("RiskCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RiskCode));
        }
        if (FCode.equalsIgnoreCase("AgentCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentCode));
        }
        if (FCode.equalsIgnoreCase("AgentGroup")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentGroup));
        }
        if (FCode.equalsIgnoreCase("PayTypeFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PayTypeFlag));
        }
        if (FCode.equalsIgnoreCase("AppntNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AppntNo));
        }
        if (FCode.equalsIgnoreCase("GetNoticeNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GetNoticeNo));
        }
        if (FCode.equalsIgnoreCase("PayAimClass")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PayAimClass));
        }
        if (FCode.equalsIgnoreCase("DutyCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DutyCode));
        }
        if (FCode.equalsIgnoreCase("PayPlanCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PayPlanCode));
        }
        if (FCode.equalsIgnoreCase("SumDuePayMoney")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SumDuePayMoney));
        }
        if (FCode.equalsIgnoreCase("SumActuPayMoney")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SumActuPayMoney));
        }
        if (FCode.equalsIgnoreCase("PayIntv")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PayIntv));
        }
        if (FCode.equalsIgnoreCase("PayDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PayDate));
        }
        if (FCode.equalsIgnoreCase("PayType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PayType));
        }
        if (FCode.equalsIgnoreCase("LastPayToDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(LastPayToDate));
        }
        if (FCode.equalsIgnoreCase("CurPayToDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CurPayToDate));
        }
        if (FCode.equalsIgnoreCase("InInsuAccState")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InInsuAccState));
        }
        if (FCode.equalsIgnoreCase("BankCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BankCode));
        }
        if (FCode.equalsIgnoreCase("BankAccNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BankAccNo));
        }
        if (FCode.equalsIgnoreCase("BankOnTheWayFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BankOnTheWayFlag));
        }
        if (FCode.equalsIgnoreCase("BankSuccFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BankSuccFlag));
        }
        if (FCode.equalsIgnoreCase("ApproveCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ApproveCode));
        }
        if (FCode.equalsIgnoreCase("ApproveDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ApproveDate));
        }
        if (FCode.equalsIgnoreCase("ApproveTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ApproveTime));
        }
        if (FCode.equalsIgnoreCase("SerialNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SerialNo));
        }
        if (FCode.equalsIgnoreCase("InputFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InputFlag));
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeDate));
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyDate));
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (FCode.equalsIgnoreCase("EdorNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(EdorNo));
        }
        if (FCode.equalsIgnoreCase("MainPolYear")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MainPolYear));
        }
        if (FCode.equalsIgnoreCase("OtherNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OtherNo));
        }
        if (FCode.equalsIgnoreCase("OtherNoType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OtherNoType));
        }
        if (FCode.equalsIgnoreCase("PrtNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PrtNo));
        }
        if (FCode.equalsIgnoreCase("SaleChnl")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SaleChnl));
        }
        if (FCode.equalsIgnoreCase("ChargeCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ChargeCom));
        }
        if (FCode.equalsIgnoreCase("APPntName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(APPntName));
        }
        if (FCode.equalsIgnoreCase("KindCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(KindCode));
        }
        if (FCode.equalsIgnoreCase("PaytoDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PaytoDate));
        }
        if (FCode.equalsIgnoreCase("PayEndDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PayEndDate));
        }
        if (FCode.equalsIgnoreCase("SignDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SignDate));
        }
        if (FCode.equalsIgnoreCase("CValiDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CValiDate));
        }
        if (FCode.equalsIgnoreCase("PayEndYear")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PayEndYear));
        }
        if (FCode.equalsIgnoreCase("PayMoney")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PayMoney));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = String.valueOf(SPayPersonID);
                break;
            case 1:
                strFieldValue = String.valueOf(ShardingID);
                break;
            case 2:
                strFieldValue = String.valueOf(PolNo);
                break;
            case 3:
                strFieldValue = String.valueOf(PayCount);
                break;
            case 4:
                strFieldValue = String.valueOf(GrpContNo);
                break;
            case 5:
                strFieldValue = String.valueOf(GrpPolNo);
                break;
            case 6:
                strFieldValue = String.valueOf(ContNo);
                break;
            case 7:
                strFieldValue = String.valueOf(ManageCom);
                break;
            case 8:
                strFieldValue = String.valueOf(AgentCom);
                break;
            case 9:
                strFieldValue = String.valueOf(AgentType);
                break;
            case 10:
                strFieldValue = String.valueOf(RiskCode);
                break;
            case 11:
                strFieldValue = String.valueOf(AgentCode);
                break;
            case 12:
                strFieldValue = String.valueOf(AgentGroup);
                break;
            case 13:
                strFieldValue = String.valueOf(PayTypeFlag);
                break;
            case 14:
                strFieldValue = String.valueOf(AppntNo);
                break;
            case 15:
                strFieldValue = String.valueOf(GetNoticeNo);
                break;
            case 16:
                strFieldValue = String.valueOf(PayAimClass);
                break;
            case 17:
                strFieldValue = String.valueOf(DutyCode);
                break;
            case 18:
                strFieldValue = String.valueOf(PayPlanCode);
                break;
            case 19:
                strFieldValue = String.valueOf(SumDuePayMoney);
                break;
            case 20:
                strFieldValue = String.valueOf(SumActuPayMoney);
                break;
            case 21:
                strFieldValue = String.valueOf(PayIntv);
                break;
            case 22:
                strFieldValue = String.valueOf(PayDate);
                break;
            case 23:
                strFieldValue = String.valueOf(PayType);
                break;
            case 24:
                strFieldValue = String.valueOf(LastPayToDate);
                break;
            case 25:
                strFieldValue = String.valueOf(CurPayToDate);
                break;
            case 26:
                strFieldValue = String.valueOf(InInsuAccState);
                break;
            case 27:
                strFieldValue = String.valueOf(BankCode);
                break;
            case 28:
                strFieldValue = String.valueOf(BankAccNo);
                break;
            case 29:
                strFieldValue = String.valueOf(BankOnTheWayFlag);
                break;
            case 30:
                strFieldValue = String.valueOf(BankSuccFlag);
                break;
            case 31:
                strFieldValue = String.valueOf(ApproveCode);
                break;
            case 32:
                strFieldValue = String.valueOf(ApproveDate);
                break;
            case 33:
                strFieldValue = String.valueOf(ApproveTime);
                break;
            case 34:
                strFieldValue = String.valueOf(SerialNo);
                break;
            case 35:
                strFieldValue = String.valueOf(InputFlag);
                break;
            case 36:
                strFieldValue = String.valueOf(Operator);
                break;
            case 37:
                strFieldValue = String.valueOf(MakeDate);
                break;
            case 38:
                strFieldValue = String.valueOf(MakeTime);
                break;
            case 39:
                strFieldValue = String.valueOf(ModifyDate);
                break;
            case 40:
                strFieldValue = String.valueOf(ModifyTime);
                break;
            case 41:
                strFieldValue = String.valueOf(EdorNo);
                break;
            case 42:
                strFieldValue = String.valueOf(MainPolYear);
                break;
            case 43:
                strFieldValue = String.valueOf(OtherNo);
                break;
            case 44:
                strFieldValue = String.valueOf(OtherNoType);
                break;
            case 45:
                strFieldValue = String.valueOf(PrtNo);
                break;
            case 46:
                strFieldValue = String.valueOf(SaleChnl);
                break;
            case 47:
                strFieldValue = String.valueOf(ChargeCom);
                break;
            case 48:
                strFieldValue = String.valueOf(APPntName);
                break;
            case 49:
                strFieldValue = String.valueOf(KindCode);
                break;
            case 50:
                strFieldValue = String.valueOf(PaytoDate);
                break;
            case 51:
                strFieldValue = String.valueOf(PayEndDate);
                break;
            case 52:
                strFieldValue = String.valueOf(SignDate);
                break;
            case 53:
                strFieldValue = String.valueOf(CValiDate);
                break;
            case 54:
                strFieldValue = String.valueOf(PayEndYear);
                break;
            case 55:
                strFieldValue = String.valueOf(PayMoney);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("SPayPersonID")) {
            if( FValue != null && !FValue.equals("")) {
                SPayPersonID = new Long(FValue).longValue();
            }
        }
        if (FCode.equalsIgnoreCase("ShardingID")) {
            if( FValue != null && !FValue.equals(""))
            {
                ShardingID = FValue.trim();
            }
            else
                ShardingID = null;
        }
        if (FCode.equalsIgnoreCase("PolNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                PolNo = FValue.trim();
            }
            else
                PolNo = null;
        }
        if (FCode.equalsIgnoreCase("PayCount")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                PayCount = i;
            }
        }
        if (FCode.equalsIgnoreCase("GrpContNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                GrpContNo = FValue.trim();
            }
            else
                GrpContNo = null;
        }
        if (FCode.equalsIgnoreCase("GrpPolNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                GrpPolNo = FValue.trim();
            }
            else
                GrpPolNo = null;
        }
        if (FCode.equalsIgnoreCase("ContNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                ContNo = FValue.trim();
            }
            else
                ContNo = null;
        }
        if (FCode.equalsIgnoreCase("ManageCom")) {
            if( FValue != null && !FValue.equals(""))
            {
                ManageCom = FValue.trim();
            }
            else
                ManageCom = null;
        }
        if (FCode.equalsIgnoreCase("AgentCom")) {
            if( FValue != null && !FValue.equals(""))
            {
                AgentCom = FValue.trim();
            }
            else
                AgentCom = null;
        }
        if (FCode.equalsIgnoreCase("AgentType")) {
            if( FValue != null && !FValue.equals(""))
            {
                AgentType = FValue.trim();
            }
            else
                AgentType = null;
        }
        if (FCode.equalsIgnoreCase("RiskCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                RiskCode = FValue.trim();
            }
            else
                RiskCode = null;
        }
        if (FCode.equalsIgnoreCase("AgentCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                AgentCode = FValue.trim();
            }
            else
                AgentCode = null;
        }
        if (FCode.equalsIgnoreCase("AgentGroup")) {
            if( FValue != null && !FValue.equals(""))
            {
                AgentGroup = FValue.trim();
            }
            else
                AgentGroup = null;
        }
        if (FCode.equalsIgnoreCase("PayTypeFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                PayTypeFlag = FValue.trim();
            }
            else
                PayTypeFlag = null;
        }
        if (FCode.equalsIgnoreCase("AppntNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                AppntNo = FValue.trim();
            }
            else
                AppntNo = null;
        }
        if (FCode.equalsIgnoreCase("GetNoticeNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                GetNoticeNo = FValue.trim();
            }
            else
                GetNoticeNo = null;
        }
        if (FCode.equalsIgnoreCase("PayAimClass")) {
            if( FValue != null && !FValue.equals(""))
            {
                PayAimClass = FValue.trim();
            }
            else
                PayAimClass = null;
        }
        if (FCode.equalsIgnoreCase("DutyCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                DutyCode = FValue.trim();
            }
            else
                DutyCode = null;
        }
        if (FCode.equalsIgnoreCase("PayPlanCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                PayPlanCode = FValue.trim();
            }
            else
                PayPlanCode = null;
        }
        if (FCode.equalsIgnoreCase("SumDuePayMoney")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                SumDuePayMoney = d;
            }
        }
        if (FCode.equalsIgnoreCase("SumActuPayMoney")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                SumActuPayMoney = d;
            }
        }
        if (FCode.equalsIgnoreCase("PayIntv")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                PayIntv = i;
            }
        }
        if (FCode.equalsIgnoreCase("PayDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                PayDate = FValue.trim();
            }
            else
                PayDate = null;
        }
        if (FCode.equalsIgnoreCase("PayType")) {
            if( FValue != null && !FValue.equals(""))
            {
                PayType = FValue.trim();
            }
            else
                PayType = null;
        }
        if (FCode.equalsIgnoreCase("LastPayToDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                LastPayToDate = FValue.trim();
            }
            else
                LastPayToDate = null;
        }
        if (FCode.equalsIgnoreCase("CurPayToDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                CurPayToDate = FValue.trim();
            }
            else
                CurPayToDate = null;
        }
        if (FCode.equalsIgnoreCase("InInsuAccState")) {
            if( FValue != null && !FValue.equals(""))
            {
                InInsuAccState = FValue.trim();
            }
            else
                InInsuAccState = null;
        }
        if (FCode.equalsIgnoreCase("BankCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                BankCode = FValue.trim();
            }
            else
                BankCode = null;
        }
        if (FCode.equalsIgnoreCase("BankAccNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                BankAccNo = FValue.trim();
            }
            else
                BankAccNo = null;
        }
        if (FCode.equalsIgnoreCase("BankOnTheWayFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                BankOnTheWayFlag = FValue.trim();
            }
            else
                BankOnTheWayFlag = null;
        }
        if (FCode.equalsIgnoreCase("BankSuccFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                BankSuccFlag = FValue.trim();
            }
            else
                BankSuccFlag = null;
        }
        if (FCode.equalsIgnoreCase("ApproveCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                ApproveCode = FValue.trim();
            }
            else
                ApproveCode = null;
        }
        if (FCode.equalsIgnoreCase("ApproveDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                ApproveDate = FValue.trim();
            }
            else
                ApproveDate = null;
        }
        if (FCode.equalsIgnoreCase("ApproveTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                ApproveTime = FValue.trim();
            }
            else
                ApproveTime = null;
        }
        if (FCode.equalsIgnoreCase("SerialNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                SerialNo = FValue.trim();
            }
            else
                SerialNo = null;
        }
        if (FCode.equalsIgnoreCase("InputFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                InputFlag = FValue.trim();
            }
            else
                InputFlag = null;
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            if( FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
                Operator = null;
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeDate = FValue.trim();
            }
            else
                MakeDate = null;
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
                MakeTime = null;
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyDate = FValue.trim();
            }
            else
                ModifyDate = null;
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
                ModifyTime = null;
        }
        if (FCode.equalsIgnoreCase("EdorNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                EdorNo = FValue.trim();
            }
            else
                EdorNo = null;
        }
        if (FCode.equalsIgnoreCase("MainPolYear")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                MainPolYear = i;
            }
        }
        if (FCode.equalsIgnoreCase("OtherNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                OtherNo = FValue.trim();
            }
            else
                OtherNo = null;
        }
        if (FCode.equalsIgnoreCase("OtherNoType")) {
            if( FValue != null && !FValue.equals(""))
            {
                OtherNoType = FValue.trim();
            }
            else
                OtherNoType = null;
        }
        if (FCode.equalsIgnoreCase("PrtNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                PrtNo = FValue.trim();
            }
            else
                PrtNo = null;
        }
        if (FCode.equalsIgnoreCase("SaleChnl")) {
            if( FValue != null && !FValue.equals(""))
            {
                SaleChnl = FValue.trim();
            }
            else
                SaleChnl = null;
        }
        if (FCode.equalsIgnoreCase("ChargeCom")) {
            if( FValue != null && !FValue.equals(""))
            {
                ChargeCom = FValue.trim();
            }
            else
                ChargeCom = null;
        }
        if (FCode.equalsIgnoreCase("APPntName")) {
            if( FValue != null && !FValue.equals(""))
            {
                APPntName = FValue.trim();
            }
            else
                APPntName = null;
        }
        if (FCode.equalsIgnoreCase("KindCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                KindCode = FValue.trim();
            }
            else
                KindCode = null;
        }
        if (FCode.equalsIgnoreCase("PaytoDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                PaytoDate = FValue.trim();
            }
            else
                PaytoDate = null;
        }
        if (FCode.equalsIgnoreCase("PayEndDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                PayEndDate = FValue.trim();
            }
            else
                PayEndDate = null;
        }
        if (FCode.equalsIgnoreCase("SignDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                SignDate = FValue.trim();
            }
            else
                SignDate = null;
        }
        if (FCode.equalsIgnoreCase("CValiDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                CValiDate = FValue.trim();
            }
            else
                CValiDate = null;
        }
        if (FCode.equalsIgnoreCase("PayEndYear")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                PayEndYear = i;
            }
        }
        if (FCode.equalsIgnoreCase("PayMoney")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                PayMoney = d;
            }
        }
        return true;
    }


    public String toString() {
    return "LKSPayPersonPojo [" +
            "SPayPersonID="+SPayPersonID +
            ", ShardingID="+ShardingID +
            ", PolNo="+PolNo +
            ", PayCount="+PayCount +
            ", GrpContNo="+GrpContNo +
            ", GrpPolNo="+GrpPolNo +
            ", ContNo="+ContNo +
            ", ManageCom="+ManageCom +
            ", AgentCom="+AgentCom +
            ", AgentType="+AgentType +
            ", RiskCode="+RiskCode +
            ", AgentCode="+AgentCode +
            ", AgentGroup="+AgentGroup +
            ", PayTypeFlag="+PayTypeFlag +
            ", AppntNo="+AppntNo +
            ", GetNoticeNo="+GetNoticeNo +
            ", PayAimClass="+PayAimClass +
            ", DutyCode="+DutyCode +
            ", PayPlanCode="+PayPlanCode +
            ", SumDuePayMoney="+SumDuePayMoney +
            ", SumActuPayMoney="+SumActuPayMoney +
            ", PayIntv="+PayIntv +
            ", PayDate="+PayDate +
            ", PayType="+PayType +
            ", LastPayToDate="+LastPayToDate +
            ", CurPayToDate="+CurPayToDate +
            ", InInsuAccState="+InInsuAccState +
            ", BankCode="+BankCode +
            ", BankAccNo="+BankAccNo +
            ", BankOnTheWayFlag="+BankOnTheWayFlag +
            ", BankSuccFlag="+BankSuccFlag +
            ", ApproveCode="+ApproveCode +
            ", ApproveDate="+ApproveDate +
            ", ApproveTime="+ApproveTime +
            ", SerialNo="+SerialNo +
            ", InputFlag="+InputFlag +
            ", Operator="+Operator +
            ", MakeDate="+MakeDate +
            ", MakeTime="+MakeTime +
            ", ModifyDate="+ModifyDate +
            ", ModifyTime="+ModifyTime +
            ", EdorNo="+EdorNo +
            ", MainPolYear="+MainPolYear +
            ", OtherNo="+OtherNo +
            ", OtherNoType="+OtherNoType +
            ", PrtNo="+PrtNo +
            ", SaleChnl="+SaleChnl +
            ", ChargeCom="+ChargeCom +
            ", APPntName="+APPntName +
            ", KindCode="+KindCode +
            ", PaytoDate="+PaytoDate +
            ", PayEndDate="+PayEndDate +
            ", SignDate="+SignDate +
            ", CValiDate="+CValiDate +
            ", PayEndYear="+PayEndYear +
            ", PayMoney="+PayMoney +"]";
    }
}
