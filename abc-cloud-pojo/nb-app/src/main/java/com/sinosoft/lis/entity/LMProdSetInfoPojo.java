/**
 * Copyright (c) 2017 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.entity;

import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.Pojo;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;

import java.io.Serializable;

/**
 * <p>ClassName: LMProdSetInfoPojo </p>
 * <p>Copyright: Copyright (c) 2017</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2017-10-17
 */
public class LMProdSetInfoPojo implements Pojo,Serializable {
    // @Field
    /** 产品组合编码 */
    private String ProdSetCode; 
    /** 产品组合名称 */
    private String ProdSetName; 
    /** 产品组合简称 */
    private String ProdSetShortName; 
    /** 产品组合英文名称 */
    private String ProdSetEnName; 
    /** 产品组合英文简称 */
    private String ProdSetEnShortName; 
    /** 销售渠道 */
    private String SaleChnl; 
    /** 产品组合停售日期 */
    private String  EndDate;
    /** 操作员 */
    private String Operator; 
    /** 入机日期 */
    private String  MakeDate;
    /** 入机时间 */
    private String MakeTime; 
    /** 修改日期 */
    private String  ModifyDate;
    /** 修改时间 */
    private String ModifyTime; 
    /** Standbyflag1 */
    private String STANDBYFLAG1; 
    /** Standbyflag2 */
    private String STANDBYFLAG2; 
    /** Standbyflag3 */
    private String  STANDBYFLAG3;
    /** Standbyflag4 */
    private String  STANDBYFLAG4;
    /** Standbyflag5 */
    private double STANDBYFLAG5; 
    /** Standbyflag6 */
    private double STANDBYFLAG6; 
    /** 产品组合启售日期 */
    private String  StartDate;
    /** 序号 */
    private String ProdSetID; 


    public static final int FIELDNUM = 20;    // 数据库表的字段个数

    private FDate fDate = new FDate();        // 处理日期
    public String getProdSetCode() {
        return ProdSetCode;
    }
    public void setProdSetCode(String aProdSetCode) {
        ProdSetCode = aProdSetCode;
    }
    public String getProdSetName() {
        return ProdSetName;
    }
    public void setProdSetName(String aProdSetName) {
        ProdSetName = aProdSetName;
    }
    public String getProdSetShortName() {
        return ProdSetShortName;
    }
    public void setProdSetShortName(String aProdSetShortName) {
        ProdSetShortName = aProdSetShortName;
    }
    public String getProdSetEnName() {
        return ProdSetEnName;
    }
    public void setProdSetEnName(String aProdSetEnName) {
        ProdSetEnName = aProdSetEnName;
    }
    public String getProdSetEnShortName() {
        return ProdSetEnShortName;
    }
    public void setProdSetEnShortName(String aProdSetEnShortName) {
        ProdSetEnShortName = aProdSetEnShortName;
    }
    public String getSaleChnl() {
        return SaleChnl;
    }
    public void setSaleChnl(String aSaleChnl) {
        SaleChnl = aSaleChnl;
    }
    public String getEndDate() {
        return EndDate;
    }
    public void setEndDate(String aEndDate) {
        EndDate = aEndDate;
    }
    public String getOperator() {
        return Operator;
    }
    public void setOperator(String aOperator) {
        Operator = aOperator;
    }
    public String getMakeDate() {
        return MakeDate;
    }
    public void setMakeDate(String aMakeDate) {
        MakeDate = aMakeDate;
    }
    public String getMakeTime() {
        return MakeTime;
    }
    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }
    public String getModifyDate() {
        return ModifyDate;
    }
    public void setModifyDate(String aModifyDate) {
        ModifyDate = aModifyDate;
    }
    public String getModifyTime() {
        return ModifyTime;
    }
    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }
    public String getSTANDBYFLAG1() {
        return STANDBYFLAG1;
    }
    public void setSTANDBYFLAG1(String aSTANDBYFLAG1) {
        STANDBYFLAG1 = aSTANDBYFLAG1;
    }
    public String getSTANDBYFLAG2() {
        return STANDBYFLAG2;
    }
    public void setSTANDBYFLAG2(String aSTANDBYFLAG2) {
        STANDBYFLAG2 = aSTANDBYFLAG2;
    }
    public String getSTANDBYFLAG3() {
        return STANDBYFLAG3;
    }
    public void setSTANDBYFLAG3(String aSTANDBYFLAG3) {
        STANDBYFLAG3 = aSTANDBYFLAG3;
    }
    public String getSTANDBYFLAG4() {
        return STANDBYFLAG4;
    }
    public void setSTANDBYFLAG4(String aSTANDBYFLAG4) {
        STANDBYFLAG4 = aSTANDBYFLAG4;
    }
    public double getSTANDBYFLAG5() {
        return STANDBYFLAG5;
    }
    public void setSTANDBYFLAG5(double aSTANDBYFLAG5) {
        STANDBYFLAG5 = aSTANDBYFLAG5;
    }
    public void setSTANDBYFLAG5(String aSTANDBYFLAG5) {
        if (aSTANDBYFLAG5 != null && !aSTANDBYFLAG5.equals("")) {
            Double tDouble = new Double(aSTANDBYFLAG5);
            double d = tDouble.doubleValue();
            STANDBYFLAG5 = d;
        }
    }

    public double getSTANDBYFLAG6() {
        return STANDBYFLAG6;
    }
    public void setSTANDBYFLAG6(double aSTANDBYFLAG6) {
        STANDBYFLAG6 = aSTANDBYFLAG6;
    }
    public void setSTANDBYFLAG6(String aSTANDBYFLAG6) {
        if (aSTANDBYFLAG6 != null && !aSTANDBYFLAG6.equals("")) {
            Double tDouble = new Double(aSTANDBYFLAG6);
            double d = tDouble.doubleValue();
            STANDBYFLAG6 = d;
        }
    }

    public String getStartDate() {
        return StartDate;
    }
    public void setStartDate(String aStartDate) {
        StartDate = aStartDate;
    }
    public String getProdSetID() {
        return ProdSetID;
    }
    public void setProdSetID(String aProdSetID) {
        ProdSetID = aProdSetID;
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("ProdSetCode") ) {
            return 0;
        }
        if( strFieldName.equals("ProdSetName") ) {
            return 1;
        }
        if( strFieldName.equals("ProdSetShortName") ) {
            return 2;
        }
        if( strFieldName.equals("ProdSetEnName") ) {
            return 3;
        }
        if( strFieldName.equals("ProdSetEnShortName") ) {
            return 4;
        }
        if( strFieldName.equals("SaleChnl") ) {
            return 5;
        }
        if( strFieldName.equals("EndDate") ) {
            return 6;
        }
        if( strFieldName.equals("Operator") ) {
            return 7;
        }
        if( strFieldName.equals("MakeDate") ) {
            return 8;
        }
        if( strFieldName.equals("MakeTime") ) {
            return 9;
        }
        if( strFieldName.equals("ModifyDate") ) {
            return 10;
        }
        if( strFieldName.equals("ModifyTime") ) {
            return 11;
        }
        if( strFieldName.equals("STANDBYFLAG1") ) {
            return 12;
        }
        if( strFieldName.equals("STANDBYFLAG2") ) {
            return 13;
        }
        if( strFieldName.equals("STANDBYFLAG3") ) {
            return 14;
        }
        if( strFieldName.equals("STANDBYFLAG4") ) {
            return 15;
        }
        if( strFieldName.equals("STANDBYFLAG5") ) {
            return 16;
        }
        if( strFieldName.equals("STANDBYFLAG6") ) {
            return 17;
        }
        if( strFieldName.equals("StartDate") ) {
            return 18;
        }
        if( strFieldName.equals("ProdSetID") ) {
            return 19;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "ProdSetCode";
                break;
            case 1:
                strFieldName = "ProdSetName";
                break;
            case 2:
                strFieldName = "ProdSetShortName";
                break;
            case 3:
                strFieldName = "ProdSetEnName";
                break;
            case 4:
                strFieldName = "ProdSetEnShortName";
                break;
            case 5:
                strFieldName = "SaleChnl";
                break;
            case 6:
                strFieldName = "EndDate";
                break;
            case 7:
                strFieldName = "Operator";
                break;
            case 8:
                strFieldName = "MakeDate";
                break;
            case 9:
                strFieldName = "MakeTime";
                break;
            case 10:
                strFieldName = "ModifyDate";
                break;
            case 11:
                strFieldName = "ModifyTime";
                break;
            case 12:
                strFieldName = "STANDBYFLAG1";
                break;
            case 13:
                strFieldName = "STANDBYFLAG2";
                break;
            case 14:
                strFieldName = "STANDBYFLAG3";
                break;
            case 15:
                strFieldName = "STANDBYFLAG4";
                break;
            case 16:
                strFieldName = "STANDBYFLAG5";
                break;
            case 17:
                strFieldName = "STANDBYFLAG6";
                break;
            case 18:
                strFieldName = "StartDate";
                break;
            case 19:
                strFieldName = "ProdSetID";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "PRODSETCODE":
                return Schema.TYPE_STRING;
            case "PRODSETNAME":
                return Schema.TYPE_STRING;
            case "PRODSETSHORTNAME":
                return Schema.TYPE_STRING;
            case "PRODSETENNAME":
                return Schema.TYPE_STRING;
            case "PRODSETENSHORTNAME":
                return Schema.TYPE_STRING;
            case "SALECHNL":
                return Schema.TYPE_STRING;
            case "ENDDATE":
                return Schema.TYPE_STRING;
            case "OPERATOR":
                return Schema.TYPE_STRING;
            case "MAKEDATE":
                return Schema.TYPE_STRING;
            case "MAKETIME":
                return Schema.TYPE_STRING;
            case "MODIFYDATE":
                return Schema.TYPE_STRING;
            case "MODIFYTIME":
                return Schema.TYPE_STRING;
            case "STANDBYFLAG1":
                return Schema.TYPE_STRING;
            case "STANDBYFLAG2":
                return Schema.TYPE_STRING;
            case "STANDBYFLAG3":
                return Schema.TYPE_STRING;
            case "STANDBYFLAG4":
                return Schema.TYPE_STRING;
            case "STANDBYFLAG5":
                return Schema.TYPE_DOUBLE;
            case "STANDBYFLAG6":
                return Schema.TYPE_DOUBLE;
            case "STARTDATE":
                return Schema.TYPE_STRING;
            case "PRODSETID":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_STRING;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_STRING;
            case 6:
                return Schema.TYPE_STRING;
            case 7:
                return Schema.TYPE_STRING;
            case 8:
                return Schema.TYPE_STRING;
            case 9:
                return Schema.TYPE_STRING;
            case 10:
                return Schema.TYPE_STRING;
            case 11:
                return Schema.TYPE_STRING;
            case 12:
                return Schema.TYPE_STRING;
            case 13:
                return Schema.TYPE_STRING;
            case 14:
                return Schema.TYPE_STRING;
            case 15:
                return Schema.TYPE_STRING;
            case 16:
                return Schema.TYPE_DOUBLE;
            case 17:
                return Schema.TYPE_DOUBLE;
            case 18:
                return Schema.TYPE_STRING;
            case 19:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("ProdSetCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ProdSetCode));
        }
        if (FCode.equalsIgnoreCase("ProdSetName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ProdSetName));
        }
        if (FCode.equalsIgnoreCase("ProdSetShortName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ProdSetShortName));
        }
        if (FCode.equalsIgnoreCase("ProdSetEnName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ProdSetEnName));
        }
        if (FCode.equalsIgnoreCase("ProdSetEnShortName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ProdSetEnShortName));
        }
        if (FCode.equalsIgnoreCase("SaleChnl")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SaleChnl));
        }
        if (FCode.equalsIgnoreCase("EndDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(EndDate));
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeDate));
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyDate));
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (FCode.equalsIgnoreCase("STANDBYFLAG1")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(STANDBYFLAG1));
        }
        if (FCode.equalsIgnoreCase("STANDBYFLAG2")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(STANDBYFLAG2));
        }
        if (FCode.equalsIgnoreCase("STANDBYFLAG3")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(STANDBYFLAG3));
        }
        if (FCode.equalsIgnoreCase("STANDBYFLAG4")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(STANDBYFLAG4));
        }
        if (FCode.equalsIgnoreCase("STANDBYFLAG5")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(STANDBYFLAG5));
        }
        if (FCode.equalsIgnoreCase("STANDBYFLAG6")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(STANDBYFLAG6));
        }
        if (FCode.equalsIgnoreCase("StartDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StartDate));
        }
        if (FCode.equalsIgnoreCase("ProdSetID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ProdSetID));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = String.valueOf(ProdSetCode);
                break;
            case 1:
                strFieldValue = String.valueOf(ProdSetName);
                break;
            case 2:
                strFieldValue = String.valueOf(ProdSetShortName);
                break;
            case 3:
                strFieldValue = String.valueOf(ProdSetEnName);
                break;
            case 4:
                strFieldValue = String.valueOf(ProdSetEnShortName);
                break;
            case 5:
                strFieldValue = String.valueOf(SaleChnl);
                break;
            case 6:
                strFieldValue = String.valueOf(EndDate);
                break;
            case 7:
                strFieldValue = String.valueOf(Operator);
                break;
            case 8:
                strFieldValue = String.valueOf(MakeDate);
                break;
            case 9:
                strFieldValue = String.valueOf(MakeTime);
                break;
            case 10:
                strFieldValue = String.valueOf(ModifyDate);
                break;
            case 11:
                strFieldValue = String.valueOf(ModifyTime);
                break;
            case 12:
                strFieldValue = String.valueOf(STANDBYFLAG1);
                break;
            case 13:
                strFieldValue = String.valueOf(STANDBYFLAG2);
                break;
            case 14:
                strFieldValue = String.valueOf(STANDBYFLAG3);
                break;
            case 15:
                strFieldValue = String.valueOf(STANDBYFLAG4);
                break;
            case 16:
                strFieldValue = String.valueOf(STANDBYFLAG5);
                break;
            case 17:
                strFieldValue = String.valueOf(STANDBYFLAG6);
                break;
            case 18:
                strFieldValue = String.valueOf(StartDate);
                break;
            case 19:
                strFieldValue = String.valueOf(ProdSetID);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("ProdSetCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                ProdSetCode = FValue.trim();
            }
            else
                ProdSetCode = null;
        }
        if (FCode.equalsIgnoreCase("ProdSetName")) {
            if( FValue != null && !FValue.equals(""))
            {
                ProdSetName = FValue.trim();
            }
            else
                ProdSetName = null;
        }
        if (FCode.equalsIgnoreCase("ProdSetShortName")) {
            if( FValue != null && !FValue.equals(""))
            {
                ProdSetShortName = FValue.trim();
            }
            else
                ProdSetShortName = null;
        }
        if (FCode.equalsIgnoreCase("ProdSetEnName")) {
            if( FValue != null && !FValue.equals(""))
            {
                ProdSetEnName = FValue.trim();
            }
            else
                ProdSetEnName = null;
        }
        if (FCode.equalsIgnoreCase("ProdSetEnShortName")) {
            if( FValue != null && !FValue.equals(""))
            {
                ProdSetEnShortName = FValue.trim();
            }
            else
                ProdSetEnShortName = null;
        }
        if (FCode.equalsIgnoreCase("SaleChnl")) {
            if( FValue != null && !FValue.equals(""))
            {
                SaleChnl = FValue.trim();
            }
            else
                SaleChnl = null;
        }
        if (FCode.equalsIgnoreCase("EndDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                EndDate = FValue.trim();
            }
            else
                EndDate = null;
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            if( FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
                Operator = null;
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeDate = FValue.trim();
            }
            else
                MakeDate = null;
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
                MakeTime = null;
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyDate = FValue.trim();
            }
            else
                ModifyDate = null;
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
                ModifyTime = null;
        }
        if (FCode.equalsIgnoreCase("STANDBYFLAG1")) {
            if( FValue != null && !FValue.equals(""))
            {
                STANDBYFLAG1 = FValue.trim();
            }
            else
                STANDBYFLAG1 = null;
        }
        if (FCode.equalsIgnoreCase("STANDBYFLAG2")) {
            if( FValue != null && !FValue.equals(""))
            {
                STANDBYFLAG2 = FValue.trim();
            }
            else
                STANDBYFLAG2 = null;
        }
        if (FCode.equalsIgnoreCase("STANDBYFLAG3")) {
            if( FValue != null && !FValue.equals(""))
            {
                STANDBYFLAG3 = FValue.trim();
            }
            else
                STANDBYFLAG3 = null;
        }
        if (FCode.equalsIgnoreCase("STANDBYFLAG4")) {
            if( FValue != null && !FValue.equals(""))
            {
                STANDBYFLAG4 = FValue.trim();
            }
            else
                STANDBYFLAG4 = null;
        }
        if (FCode.equalsIgnoreCase("STANDBYFLAG5")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                STANDBYFLAG5 = d;
            }
        }
        if (FCode.equalsIgnoreCase("STANDBYFLAG6")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                STANDBYFLAG6 = d;
            }
        }
        if (FCode.equalsIgnoreCase("StartDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                StartDate = FValue.trim();
            }
            else
                StartDate = null;
        }
        if (FCode.equalsIgnoreCase("ProdSetID")) {
            if( FValue != null && !FValue.equals(""))
            {
                ProdSetID = FValue.trim();
            }
            else
                ProdSetID = null;
        }
        return true;
    }


    public String toString() {
    return "LMProdSetInfoPojo [" +
            "ProdSetCode="+ProdSetCode +
            ", ProdSetName="+ProdSetName +
            ", ProdSetShortName="+ProdSetShortName +
            ", ProdSetEnName="+ProdSetEnName +
            ", ProdSetEnShortName="+ProdSetEnShortName +
            ", SaleChnl="+SaleChnl +
            ", EndDate="+EndDate +
            ", Operator="+Operator +
            ", MakeDate="+MakeDate +
            ", MakeTime="+MakeTime +
            ", ModifyDate="+ModifyDate +
            ", ModifyTime="+ModifyTime +
            ", STANDBYFLAG1="+STANDBYFLAG1 +
            ", STANDBYFLAG2="+STANDBYFLAG2 +
            ", STANDBYFLAG3="+STANDBYFLAG3 +
            ", STANDBYFLAG4="+STANDBYFLAG4 +
            ", STANDBYFLAG5="+STANDBYFLAG5 +
            ", STANDBYFLAG6="+STANDBYFLAG6 +
            ", StartDate="+StartDate +
            ", ProdSetID="+ProdSetID +"]";
    }
}
