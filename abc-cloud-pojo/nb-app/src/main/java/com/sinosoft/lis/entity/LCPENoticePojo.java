/**
 * Copyright (c) 2017 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.entity;

import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.Pojo;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;

import java.io.Serializable;

/**
 * <p>ClassName: LCPENoticePojo </p>
 * <p>Copyright: Copyright (c) 2017</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2017-10-17
 */
public class LCPENoticePojo implements Pojo,Serializable {
    // @Field
    /** 集体合同号码 */
    private String GrpContNo; 
    /** 合同号码 */
    private String ContNo; 
    /** 总单投保单号码 */
    private String ProposalContNo; 
    /** 打印流水号 */
    private String PrtSeq; 
    /** 投保人姓名 */
    private String AppName; 
    /** 险种名称 */
    private String RiskName; 
    /** 体检日期 */
    private String  PEDate;
    /** 体检客户号码 */
    private String CustomerNo; 
    /** 体检客户姓名 */
    private String Name; 
    /** 体检地点 */
    private String PEAddress; 
    /** 体检前条件 */
    private String PEBeforeCond; 
    /** 打印标记 */
    private String PrintFlag; 
    /** 管理机构 */
    private String ManageCom; 
    /** 代理人姓名 */
    private String AgentName; 
    /** 代理人编码 */
    private String AgentCode; 
    /** 操作员 */
    private String Operator; 
    /** 入机日期 */
    private String  MakeDate;
    /** 入机时间 */
    private String MakeTime; 
    /** 最后一次修改日期 */
    private String  ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime; 
    /** 备注 */
    private String Remark; 
    /** 体检结论 */
    private String PEResult; 
    /** 团单体检总表打印流水号 */
    private String GrpPrtSeq; 
    /** 体检医师 */
    private String PEDoctor; 
    /** 复查时间 */
    private String  REPEDate;
    /** 体检原因 */
    private String PEReason; 
    /** 是否阳性标记 */
    private String MasculineFlag; 
    /** 体检医院代码 */
    private String HospitalCode; 
    /** 是否在保单中打印 */
    private String NeedPrint; 


    public static final int FIELDNUM = 29;    // 数据库表的字段个数

    private FDate fDate = new FDate();        // 处理日期
    public String getGrpContNo() {
        return GrpContNo;
    }
    public void setGrpContNo(String aGrpContNo) {
        GrpContNo = aGrpContNo;
    }
    public String getContNo() {
        return ContNo;
    }
    public void setContNo(String aContNo) {
        ContNo = aContNo;
    }
    public String getProposalContNo() {
        return ProposalContNo;
    }
    public void setProposalContNo(String aProposalContNo) {
        ProposalContNo = aProposalContNo;
    }
    public String getPrtSeq() {
        return PrtSeq;
    }
    public void setPrtSeq(String aPrtSeq) {
        PrtSeq = aPrtSeq;
    }
    public String getAppName() {
        return AppName;
    }
    public void setAppName(String aAppName) {
        AppName = aAppName;
    }
    public String getRiskName() {
        return RiskName;
    }
    public void setRiskName(String aRiskName) {
        RiskName = aRiskName;
    }
    public String getPEDate() {
        return PEDate;
    }
    public void setPEDate(String aPEDate) {
        PEDate = aPEDate;
    }
    public String getCustomerNo() {
        return CustomerNo;
    }
    public void setCustomerNo(String aCustomerNo) {
        CustomerNo = aCustomerNo;
    }
    public String getName() {
        return Name;
    }
    public void setName(String aName) {
        Name = aName;
    }
    public String getPEAddress() {
        return PEAddress;
    }
    public void setPEAddress(String aPEAddress) {
        PEAddress = aPEAddress;
    }
    public String getPEBeforeCond() {
        return PEBeforeCond;
    }
    public void setPEBeforeCond(String aPEBeforeCond) {
        PEBeforeCond = aPEBeforeCond;
    }
    public String getPrintFlag() {
        return PrintFlag;
    }
    public void setPrintFlag(String aPrintFlag) {
        PrintFlag = aPrintFlag;
    }
    public String getManageCom() {
        return ManageCom;
    }
    public void setManageCom(String aManageCom) {
        ManageCom = aManageCom;
    }
    public String getAgentName() {
        return AgentName;
    }
    public void setAgentName(String aAgentName) {
        AgentName = aAgentName;
    }
    public String getAgentCode() {
        return AgentCode;
    }
    public void setAgentCode(String aAgentCode) {
        AgentCode = aAgentCode;
    }
    public String getOperator() {
        return Operator;
    }
    public void setOperator(String aOperator) {
        Operator = aOperator;
    }
    public String getMakeDate() {
        return MakeDate;
    }
    public void setMakeDate(String aMakeDate) {
        MakeDate = aMakeDate;
    }
    public String getMakeTime() {
        return MakeTime;
    }
    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }
    public String getModifyDate() {
        return ModifyDate;
    }
    public void setModifyDate(String aModifyDate) {
        ModifyDate = aModifyDate;
    }
    public String getModifyTime() {
        return ModifyTime;
    }
    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }
    public String getRemark() {
        return Remark;
    }
    public void setRemark(String aRemark) {
        Remark = aRemark;
    }
    public String getPEResult() {
        return PEResult;
    }
    public void setPEResult(String aPEResult) {
        PEResult = aPEResult;
    }
    public String getGrpPrtSeq() {
        return GrpPrtSeq;
    }
    public void setGrpPrtSeq(String aGrpPrtSeq) {
        GrpPrtSeq = aGrpPrtSeq;
    }
    public String getPEDoctor() {
        return PEDoctor;
    }
    public void setPEDoctor(String aPEDoctor) {
        PEDoctor = aPEDoctor;
    }
    public String getREPEDate() {
        return REPEDate;
    }
    public void setREPEDate(String aREPEDate) {
        REPEDate = aREPEDate;
    }
    public String getPEReason() {
        return PEReason;
    }
    public void setPEReason(String aPEReason) {
        PEReason = aPEReason;
    }
    public String getMasculineFlag() {
        return MasculineFlag;
    }
    public void setMasculineFlag(String aMasculineFlag) {
        MasculineFlag = aMasculineFlag;
    }
    public String getHospitalCode() {
        return HospitalCode;
    }
    public void setHospitalCode(String aHospitalCode) {
        HospitalCode = aHospitalCode;
    }
    public String getNeedPrint() {
        return NeedPrint;
    }
    public void setNeedPrint(String aNeedPrint) {
        NeedPrint = aNeedPrint;
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("GrpContNo") ) {
            return 0;
        }
        if( strFieldName.equals("ContNo") ) {
            return 1;
        }
        if( strFieldName.equals("ProposalContNo") ) {
            return 2;
        }
        if( strFieldName.equals("PrtSeq") ) {
            return 3;
        }
        if( strFieldName.equals("AppName") ) {
            return 4;
        }
        if( strFieldName.equals("RiskName") ) {
            return 5;
        }
        if( strFieldName.equals("PEDate") ) {
            return 6;
        }
        if( strFieldName.equals("CustomerNo") ) {
            return 7;
        }
        if( strFieldName.equals("Name") ) {
            return 8;
        }
        if( strFieldName.equals("PEAddress") ) {
            return 9;
        }
        if( strFieldName.equals("PEBeforeCond") ) {
            return 10;
        }
        if( strFieldName.equals("PrintFlag") ) {
            return 11;
        }
        if( strFieldName.equals("ManageCom") ) {
            return 12;
        }
        if( strFieldName.equals("AgentName") ) {
            return 13;
        }
        if( strFieldName.equals("AgentCode") ) {
            return 14;
        }
        if( strFieldName.equals("Operator") ) {
            return 15;
        }
        if( strFieldName.equals("MakeDate") ) {
            return 16;
        }
        if( strFieldName.equals("MakeTime") ) {
            return 17;
        }
        if( strFieldName.equals("ModifyDate") ) {
            return 18;
        }
        if( strFieldName.equals("ModifyTime") ) {
            return 19;
        }
        if( strFieldName.equals("Remark") ) {
            return 20;
        }
        if( strFieldName.equals("PEResult") ) {
            return 21;
        }
        if( strFieldName.equals("GrpPrtSeq") ) {
            return 22;
        }
        if( strFieldName.equals("PEDoctor") ) {
            return 23;
        }
        if( strFieldName.equals("REPEDate") ) {
            return 24;
        }
        if( strFieldName.equals("PEReason") ) {
            return 25;
        }
        if( strFieldName.equals("MasculineFlag") ) {
            return 26;
        }
        if( strFieldName.equals("HospitalCode") ) {
            return 27;
        }
        if( strFieldName.equals("NeedPrint") ) {
            return 28;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "GrpContNo";
                break;
            case 1:
                strFieldName = "ContNo";
                break;
            case 2:
                strFieldName = "ProposalContNo";
                break;
            case 3:
                strFieldName = "PrtSeq";
                break;
            case 4:
                strFieldName = "AppName";
                break;
            case 5:
                strFieldName = "RiskName";
                break;
            case 6:
                strFieldName = "PEDate";
                break;
            case 7:
                strFieldName = "CustomerNo";
                break;
            case 8:
                strFieldName = "Name";
                break;
            case 9:
                strFieldName = "PEAddress";
                break;
            case 10:
                strFieldName = "PEBeforeCond";
                break;
            case 11:
                strFieldName = "PrintFlag";
                break;
            case 12:
                strFieldName = "ManageCom";
                break;
            case 13:
                strFieldName = "AgentName";
                break;
            case 14:
                strFieldName = "AgentCode";
                break;
            case 15:
                strFieldName = "Operator";
                break;
            case 16:
                strFieldName = "MakeDate";
                break;
            case 17:
                strFieldName = "MakeTime";
                break;
            case 18:
                strFieldName = "ModifyDate";
                break;
            case 19:
                strFieldName = "ModifyTime";
                break;
            case 20:
                strFieldName = "Remark";
                break;
            case 21:
                strFieldName = "PEResult";
                break;
            case 22:
                strFieldName = "GrpPrtSeq";
                break;
            case 23:
                strFieldName = "PEDoctor";
                break;
            case 24:
                strFieldName = "REPEDate";
                break;
            case 25:
                strFieldName = "PEReason";
                break;
            case 26:
                strFieldName = "MasculineFlag";
                break;
            case 27:
                strFieldName = "HospitalCode";
                break;
            case 28:
                strFieldName = "NeedPrint";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "GRPCONTNO":
                return Schema.TYPE_STRING;
            case "CONTNO":
                return Schema.TYPE_STRING;
            case "PROPOSALCONTNO":
                return Schema.TYPE_STRING;
            case "PRTSEQ":
                return Schema.TYPE_STRING;
            case "APPNAME":
                return Schema.TYPE_STRING;
            case "RISKNAME":
                return Schema.TYPE_STRING;
            case "PEDATE":
                return Schema.TYPE_STRING;
            case "CUSTOMERNO":
                return Schema.TYPE_STRING;
            case "NAME":
                return Schema.TYPE_STRING;
            case "PEADDRESS":
                return Schema.TYPE_STRING;
            case "PEBEFORECOND":
                return Schema.TYPE_STRING;
            case "PRINTFLAG":
                return Schema.TYPE_STRING;
            case "MANAGECOM":
                return Schema.TYPE_STRING;
            case "AGENTNAME":
                return Schema.TYPE_STRING;
            case "AGENTCODE":
                return Schema.TYPE_STRING;
            case "OPERATOR":
                return Schema.TYPE_STRING;
            case "MAKEDATE":
                return Schema.TYPE_STRING;
            case "MAKETIME":
                return Schema.TYPE_STRING;
            case "MODIFYDATE":
                return Schema.TYPE_STRING;
            case "MODIFYTIME":
                return Schema.TYPE_STRING;
            case "REMARK":
                return Schema.TYPE_STRING;
            case "PERESULT":
                return Schema.TYPE_STRING;
            case "GRPPRTSEQ":
                return Schema.TYPE_STRING;
            case "PEDOCTOR":
                return Schema.TYPE_STRING;
            case "REPEDATE":
                return Schema.TYPE_STRING;
            case "PEREASON":
                return Schema.TYPE_STRING;
            case "MASCULINEFLAG":
                return Schema.TYPE_STRING;
            case "HOSPITALCODE":
                return Schema.TYPE_STRING;
            case "NEEDPRINT":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_STRING;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_STRING;
            case 6:
                return Schema.TYPE_STRING;
            case 7:
                return Schema.TYPE_STRING;
            case 8:
                return Schema.TYPE_STRING;
            case 9:
                return Schema.TYPE_STRING;
            case 10:
                return Schema.TYPE_STRING;
            case 11:
                return Schema.TYPE_STRING;
            case 12:
                return Schema.TYPE_STRING;
            case 13:
                return Schema.TYPE_STRING;
            case 14:
                return Schema.TYPE_STRING;
            case 15:
                return Schema.TYPE_STRING;
            case 16:
                return Schema.TYPE_STRING;
            case 17:
                return Schema.TYPE_STRING;
            case 18:
                return Schema.TYPE_STRING;
            case 19:
                return Schema.TYPE_STRING;
            case 20:
                return Schema.TYPE_STRING;
            case 21:
                return Schema.TYPE_STRING;
            case 22:
                return Schema.TYPE_STRING;
            case 23:
                return Schema.TYPE_STRING;
            case 24:
                return Schema.TYPE_STRING;
            case 25:
                return Schema.TYPE_STRING;
            case 26:
                return Schema.TYPE_STRING;
            case 27:
                return Schema.TYPE_STRING;
            case 28:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("GrpContNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GrpContNo));
        }
        if (FCode.equalsIgnoreCase("ContNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ContNo));
        }
        if (FCode.equalsIgnoreCase("ProposalContNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ProposalContNo));
        }
        if (FCode.equalsIgnoreCase("PrtSeq")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PrtSeq));
        }
        if (FCode.equalsIgnoreCase("AppName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AppName));
        }
        if (FCode.equalsIgnoreCase("RiskName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RiskName));
        }
        if (FCode.equalsIgnoreCase("PEDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PEDate));
        }
        if (FCode.equalsIgnoreCase("CustomerNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CustomerNo));
        }
        if (FCode.equalsIgnoreCase("Name")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Name));
        }
        if (FCode.equalsIgnoreCase("PEAddress")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PEAddress));
        }
        if (FCode.equalsIgnoreCase("PEBeforeCond")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PEBeforeCond));
        }
        if (FCode.equalsIgnoreCase("PrintFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PrintFlag));
        }
        if (FCode.equalsIgnoreCase("ManageCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
        }
        if (FCode.equalsIgnoreCase("AgentName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentName));
        }
        if (FCode.equalsIgnoreCase("AgentCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentCode));
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeDate));
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyDate));
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (FCode.equalsIgnoreCase("Remark")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Remark));
        }
        if (FCode.equalsIgnoreCase("PEResult")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PEResult));
        }
        if (FCode.equalsIgnoreCase("GrpPrtSeq")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GrpPrtSeq));
        }
        if (FCode.equalsIgnoreCase("PEDoctor")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PEDoctor));
        }
        if (FCode.equalsIgnoreCase("REPEDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(REPEDate));
        }
        if (FCode.equalsIgnoreCase("PEReason")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PEReason));
        }
        if (FCode.equalsIgnoreCase("MasculineFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MasculineFlag));
        }
        if (FCode.equalsIgnoreCase("HospitalCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(HospitalCode));
        }
        if (FCode.equalsIgnoreCase("NeedPrint")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(NeedPrint));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = String.valueOf(GrpContNo);
                break;
            case 1:
                strFieldValue = String.valueOf(ContNo);
                break;
            case 2:
                strFieldValue = String.valueOf(ProposalContNo);
                break;
            case 3:
                strFieldValue = String.valueOf(PrtSeq);
                break;
            case 4:
                strFieldValue = String.valueOf(AppName);
                break;
            case 5:
                strFieldValue = String.valueOf(RiskName);
                break;
            case 6:
                strFieldValue = String.valueOf(PEDate);
                break;
            case 7:
                strFieldValue = String.valueOf(CustomerNo);
                break;
            case 8:
                strFieldValue = String.valueOf(Name);
                break;
            case 9:
                strFieldValue = String.valueOf(PEAddress);
                break;
            case 10:
                strFieldValue = String.valueOf(PEBeforeCond);
                break;
            case 11:
                strFieldValue = String.valueOf(PrintFlag);
                break;
            case 12:
                strFieldValue = String.valueOf(ManageCom);
                break;
            case 13:
                strFieldValue = String.valueOf(AgentName);
                break;
            case 14:
                strFieldValue = String.valueOf(AgentCode);
                break;
            case 15:
                strFieldValue = String.valueOf(Operator);
                break;
            case 16:
                strFieldValue = String.valueOf(MakeDate);
                break;
            case 17:
                strFieldValue = String.valueOf(MakeTime);
                break;
            case 18:
                strFieldValue = String.valueOf(ModifyDate);
                break;
            case 19:
                strFieldValue = String.valueOf(ModifyTime);
                break;
            case 20:
                strFieldValue = String.valueOf(Remark);
                break;
            case 21:
                strFieldValue = String.valueOf(PEResult);
                break;
            case 22:
                strFieldValue = String.valueOf(GrpPrtSeq);
                break;
            case 23:
                strFieldValue = String.valueOf(PEDoctor);
                break;
            case 24:
                strFieldValue = String.valueOf(REPEDate);
                break;
            case 25:
                strFieldValue = String.valueOf(PEReason);
                break;
            case 26:
                strFieldValue = String.valueOf(MasculineFlag);
                break;
            case 27:
                strFieldValue = String.valueOf(HospitalCode);
                break;
            case 28:
                strFieldValue = String.valueOf(NeedPrint);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("GrpContNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                GrpContNo = FValue.trim();
            }
            else
                GrpContNo = null;
        }
        if (FCode.equalsIgnoreCase("ContNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                ContNo = FValue.trim();
            }
            else
                ContNo = null;
        }
        if (FCode.equalsIgnoreCase("ProposalContNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                ProposalContNo = FValue.trim();
            }
            else
                ProposalContNo = null;
        }
        if (FCode.equalsIgnoreCase("PrtSeq")) {
            if( FValue != null && !FValue.equals(""))
            {
                PrtSeq = FValue.trim();
            }
            else
                PrtSeq = null;
        }
        if (FCode.equalsIgnoreCase("AppName")) {
            if( FValue != null && !FValue.equals(""))
            {
                AppName = FValue.trim();
            }
            else
                AppName = null;
        }
        if (FCode.equalsIgnoreCase("RiskName")) {
            if( FValue != null && !FValue.equals(""))
            {
                RiskName = FValue.trim();
            }
            else
                RiskName = null;
        }
        if (FCode.equalsIgnoreCase("PEDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                PEDate = FValue.trim();
            }
            else
                PEDate = null;
        }
        if (FCode.equalsIgnoreCase("CustomerNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                CustomerNo = FValue.trim();
            }
            else
                CustomerNo = null;
        }
        if (FCode.equalsIgnoreCase("Name")) {
            if( FValue != null && !FValue.equals(""))
            {
                Name = FValue.trim();
            }
            else
                Name = null;
        }
        if (FCode.equalsIgnoreCase("PEAddress")) {
            if( FValue != null && !FValue.equals(""))
            {
                PEAddress = FValue.trim();
            }
            else
                PEAddress = null;
        }
        if (FCode.equalsIgnoreCase("PEBeforeCond")) {
            if( FValue != null && !FValue.equals(""))
            {
                PEBeforeCond = FValue.trim();
            }
            else
                PEBeforeCond = null;
        }
        if (FCode.equalsIgnoreCase("PrintFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                PrintFlag = FValue.trim();
            }
            else
                PrintFlag = null;
        }
        if (FCode.equalsIgnoreCase("ManageCom")) {
            if( FValue != null && !FValue.equals(""))
            {
                ManageCom = FValue.trim();
            }
            else
                ManageCom = null;
        }
        if (FCode.equalsIgnoreCase("AgentName")) {
            if( FValue != null && !FValue.equals(""))
            {
                AgentName = FValue.trim();
            }
            else
                AgentName = null;
        }
        if (FCode.equalsIgnoreCase("AgentCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                AgentCode = FValue.trim();
            }
            else
                AgentCode = null;
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            if( FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
                Operator = null;
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeDate = FValue.trim();
            }
            else
                MakeDate = null;
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
                MakeTime = null;
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyDate = FValue.trim();
            }
            else
                ModifyDate = null;
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
                ModifyTime = null;
        }
        if (FCode.equalsIgnoreCase("Remark")) {
            if( FValue != null && !FValue.equals(""))
            {
                Remark = FValue.trim();
            }
            else
                Remark = null;
        }
        if (FCode.equalsIgnoreCase("PEResult")) {
            if( FValue != null && !FValue.equals(""))
            {
                PEResult = FValue.trim();
            }
            else
                PEResult = null;
        }
        if (FCode.equalsIgnoreCase("GrpPrtSeq")) {
            if( FValue != null && !FValue.equals(""))
            {
                GrpPrtSeq = FValue.trim();
            }
            else
                GrpPrtSeq = null;
        }
        if (FCode.equalsIgnoreCase("PEDoctor")) {
            if( FValue != null && !FValue.equals(""))
            {
                PEDoctor = FValue.trim();
            }
            else
                PEDoctor = null;
        }
        if (FCode.equalsIgnoreCase("REPEDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                REPEDate = FValue.trim();
            }
            else
                REPEDate = null;
        }
        if (FCode.equalsIgnoreCase("PEReason")) {
            if( FValue != null && !FValue.equals(""))
            {
                PEReason = FValue.trim();
            }
            else
                PEReason = null;
        }
        if (FCode.equalsIgnoreCase("MasculineFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                MasculineFlag = FValue.trim();
            }
            else
                MasculineFlag = null;
        }
        if (FCode.equalsIgnoreCase("HospitalCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                HospitalCode = FValue.trim();
            }
            else
                HospitalCode = null;
        }
        if (FCode.equalsIgnoreCase("NeedPrint")) {
            if( FValue != null && !FValue.equals(""))
            {
                NeedPrint = FValue.trim();
            }
            else
                NeedPrint = null;
        }
        return true;
    }


    public String toString() {
    return "LCPENoticePojo [" +
            "GrpContNo="+GrpContNo +
            ", ContNo="+ContNo +
            ", ProposalContNo="+ProposalContNo +
            ", PrtSeq="+PrtSeq +
            ", AppName="+AppName +
            ", RiskName="+RiskName +
            ", PEDate="+PEDate +
            ", CustomerNo="+CustomerNo +
            ", Name="+Name +
            ", PEAddress="+PEAddress +
            ", PEBeforeCond="+PEBeforeCond +
            ", PrintFlag="+PrintFlag +
            ", ManageCom="+ManageCom +
            ", AgentName="+AgentName +
            ", AgentCode="+AgentCode +
            ", Operator="+Operator +
            ", MakeDate="+MakeDate +
            ", MakeTime="+MakeTime +
            ", ModifyDate="+ModifyDate +
            ", ModifyTime="+ModifyTime +
            ", Remark="+Remark +
            ", PEResult="+PEResult +
            ", GrpPrtSeq="+GrpPrtSeq +
            ", PEDoctor="+PEDoctor +
            ", REPEDate="+REPEDate +
            ", PEReason="+PEReason +
            ", MasculineFlag="+MasculineFlag +
            ", HospitalCode="+HospitalCode +
            ", NeedPrint="+NeedPrint +"]";
    }
}
