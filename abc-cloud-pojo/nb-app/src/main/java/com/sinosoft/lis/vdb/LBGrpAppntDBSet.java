/**
 * Copyright (c) 2018 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.vdb;

import com.sinosoft.lis.vschema.LBGrpAppntSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.DBConnPool;
import com.sinosoft.utility.DBOper;
import com.sinosoft.utility.ExceptionUtils;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;

/**
 * <p>ClassName: LBGrpAppntDBSet </p>
 * <p>Description: DB层多记录数据库操作类文件 </p>
 * <p>Copyright: Copyright (c) 2018</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2018-05-23
 */
public class LBGrpAppntDBSet extends LBGrpAppntSet {
    // @Field
    private Connection con;
    private DBOper db;
    /**
    * flag = true: 传入Connection
    * flag = false: 不传入Connection
    **/
    private boolean mflag = false;

    // @Constructor
    public LBGrpAppntDBSet(Connection tConnection) {
        con = tConnection;
        db = new DBOper(con,"LBGrpAppnt");
        mflag = true;
    }

    public LBGrpAppntDBSet() {
        db = new DBOper( "LBGrpAppnt" );
    }
    // @Method
    public boolean deleteSQL() {
        if (db.deleteSQL(this)) {
            return true;
        } else {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LBGrpAppntDBSet";
            tError.functionName = "deleteSQL";
            tError.errorMessage = "操作失败!";
            this.mErrors .addOneError(tError);
            return false;
        }
    }

    /**
     * 删除操作
     * 删除条件：主键
     * @return boolean
     */
    public boolean delete() {
        PreparedStatement pstmt = null;

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try {
            int tCount = this.size();
            pstmt = con.prepareStatement("DELETE FROM LBGrpAppnt WHERE  1=1  AND GrpContNo = ?");
            for (int i = 1; i <= tCount; i++) {
            if(this.get(i).getGrpContNo() == null || this.get(i).getGrpContNo().equals("null")) {
            	pstmt.setString(1,null);
            } else {
            	pstmt.setString(1, this.get(i).getGrpContNo());
            }
                pstmt.addBatch();
            }
            pstmt.executeBatch();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            ex.printStackTrace();
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LBGrpAppntDBSet";
            tError.functionName = "delete()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            try {
                pstmt.close();
            } catch (Exception e){
                e.printStackTrace();
            }

            if (!mflag) {
                try {
                    con.close();
                } catch (Exception e){e.printStackTrace();}
            }

            return false;
        }

        if( !mflag ) {
            try {
                con.close();
            } catch (Exception e){e.printStackTrace();}
        }

        return true;
    }

    /**
     * 更新操作
     * 更新条件：主键
     * @return boolean
     */
    public boolean update() {
        PreparedStatement pstmt = null;

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try {
            int tCount = this.size();
            pstmt = con.prepareStatement("UPDATE LBGrpAppnt SET  GrpContNo = ? , CustomerNo = ? , PrtNo = ? , AddressNo = ? , AppntGrade = ? , Name = ? , PostalAddress = ? , ZipCode = ? , Phone = ? , Password = ? , State = ? , AppntType = ? , RelationToInsured = ? , Operator = ? , MakeDate = ? , MakeTime = ? , ModifyDate = ? , ModifyTime = ? , BusiCategory = ? , GrpNature = ? , SumNumPeople = ? , MainBusiness = ? , Corporation = ? , CorIDType = ? , CorID = ? , CorIDExpiryDate = ? , OnJobNumber = ? , RetireNumber = ? , OtherNumber = ? , RgtCapital = ? , TotalAssets = ? , NetProfitRate = ? , Satrap = ? , ActuCtrl = ? , License = ? , SocialInsuCode = ? , OrganizationCode = ? , TaxCode = ? , Fax = ? , EMail = ? , FoundDate = ? , Remark = ? , Segment1 = ? , Segment2 = ? , Segment3 = ? , ManageCom = ? , ComCode = ? , ModifyOperator = ? , Sex = ? , NativePlace = ? , Occupation = ? , OccupationCode = ? , TaxPayerType = ? , FixedPlan = ? , AddTaxFlag = ? , AddTaxDate = ? , BankAccTaxNo = ? , BankTaxName = ? , BankTaxCode = ? , GrpAppIDType = ? , GrpAppIDExpDate = ? , CtrPeople = ? , Peoples3 = ? , RelaPeoples = ? , RelaMatePeoples = ? , RelaYoungPeoples = ? , RelaOtherPeoples = ? WHERE  1=1  AND GrpContNo = ?");
            for (int i = 1; i <= tCount; i++) {
            if(this.get(i).getGrpContNo() == null || this.get(i).getGrpContNo().equals("null")) {
            	pstmt.setString(1,null);
            } else {
            	pstmt.setString(1, this.get(i).getGrpContNo());
            }
            if(this.get(i).getCustomerNo() == null || this.get(i).getCustomerNo().equals("null")) {
            	pstmt.setString(2,null);
            } else {
            	pstmt.setString(2, this.get(i).getCustomerNo());
            }
            if(this.get(i).getPrtNo() == null || this.get(i).getPrtNo().equals("null")) {
            	pstmt.setString(3,null);
            } else {
            	pstmt.setString(3, this.get(i).getPrtNo());
            }
            if(this.get(i).getAddressNo() == null || this.get(i).getAddressNo().equals("null")) {
            	pstmt.setString(4,null);
            } else {
            	pstmt.setString(4, this.get(i).getAddressNo());
            }
            if(this.get(i).getAppntGrade() == null || this.get(i).getAppntGrade().equals("null")) {
            	pstmt.setString(5,null);
            } else {
            	pstmt.setString(5, this.get(i).getAppntGrade());
            }
            if(this.get(i).getName() == null || this.get(i).getName().equals("null")) {
            	pstmt.setString(6,null);
            } else {
            	pstmt.setString(6, this.get(i).getName());
            }
            if(this.get(i).getPostalAddress() == null || this.get(i).getPostalAddress().equals("null")) {
            	pstmt.setString(7,null);
            } else {
            	pstmt.setString(7, this.get(i).getPostalAddress());
            }
            if(this.get(i).getZipCode() == null || this.get(i).getZipCode().equals("null")) {
            	pstmt.setString(8,null);
            } else {
            	pstmt.setString(8, this.get(i).getZipCode());
            }
            if(this.get(i).getPhone() == null || this.get(i).getPhone().equals("null")) {
            	pstmt.setString(9,null);
            } else {
            	pstmt.setString(9, this.get(i).getPhone());
            }
            if(this.get(i).getPassword() == null || this.get(i).getPassword().equals("null")) {
            	pstmt.setString(10,null);
            } else {
            	pstmt.setString(10, this.get(i).getPassword());
            }
            if(this.get(i).getState() == null || this.get(i).getState().equals("null")) {
            	pstmt.setString(11,null);
            } else {
            	pstmt.setString(11, this.get(i).getState());
            }
            if(this.get(i).getAppntType() == null || this.get(i).getAppntType().equals("null")) {
            	pstmt.setString(12,null);
            } else {
            	pstmt.setString(12, this.get(i).getAppntType());
            }
            if(this.get(i).getRelationToInsured() == null || this.get(i).getRelationToInsured().equals("null")) {
            	pstmt.setString(13,null);
            } else {
            	pstmt.setString(13, this.get(i).getRelationToInsured());
            }
            if(this.get(i).getOperator() == null || this.get(i).getOperator().equals("null")) {
            	pstmt.setString(14,null);
            } else {
            	pstmt.setString(14, this.get(i).getOperator());
            }
            if(this.get(i).getMakeDate() == null || this.get(i).getMakeDate().equals("null")) {
                pstmt.setDate(15,null);
            } else {
                pstmt.setDate(15, Date.valueOf(this.get(i).getMakeDate()));
            }
            if(this.get(i).getMakeTime() == null || this.get(i).getMakeTime().equals("null")) {
            	pstmt.setString(16,null);
            } else {
            	pstmt.setString(16, this.get(i).getMakeTime());
            }
            if(this.get(i).getModifyDate() == null || this.get(i).getModifyDate().equals("null")) {
                pstmt.setDate(17,null);
            } else {
                pstmt.setDate(17, Date.valueOf(this.get(i).getModifyDate()));
            }
            if(this.get(i).getModifyTime() == null || this.get(i).getModifyTime().equals("null")) {
            	pstmt.setString(18,null);
            } else {
            	pstmt.setString(18, this.get(i).getModifyTime());
            }
            if(this.get(i).getBusiCategory() == null || this.get(i).getBusiCategory().equals("null")) {
            	pstmt.setString(19,null);
            } else {
            	pstmt.setString(19, this.get(i).getBusiCategory());
            }
            if(this.get(i).getGrpNature() == null || this.get(i).getGrpNature().equals("null")) {
            	pstmt.setString(20,null);
            } else {
            	pstmt.setString(20, this.get(i).getGrpNature());
            }
            pstmt.setInt(21, this.get(i).getSumNumPeople());
            if(this.get(i).getMainBusiness() == null || this.get(i).getMainBusiness().equals("null")) {
            	pstmt.setString(22,null);
            } else {
            	pstmt.setString(22, this.get(i).getMainBusiness());
            }
            if(this.get(i).getCorporation() == null || this.get(i).getCorporation().equals("null")) {
            	pstmt.setString(23,null);
            } else {
            	pstmt.setString(23, this.get(i).getCorporation());
            }
            if(this.get(i).getCorIDType() == null || this.get(i).getCorIDType().equals("null")) {
            	pstmt.setString(24,null);
            } else {
            	pstmt.setString(24, this.get(i).getCorIDType());
            }
            if(this.get(i).getCorID() == null || this.get(i).getCorID().equals("null")) {
            	pstmt.setString(25,null);
            } else {
            	pstmt.setString(25, this.get(i).getCorID());
            }
            if(this.get(i).getCorIDExpiryDate() == null || this.get(i).getCorIDExpiryDate().equals("null")) {
                pstmt.setDate(26,null);
            } else {
                pstmt.setDate(26, Date.valueOf(this.get(i).getCorIDExpiryDate()));
            }
            pstmt.setInt(27, this.get(i).getOnJobNumber());
            pstmt.setInt(28, this.get(i).getRetireNumber());
            pstmt.setInt(29, this.get(i).getOtherNumber());
            pstmt.setDouble(30, this.get(i).getRgtCapital());
            pstmt.setDouble(31, this.get(i).getTotalAssets());
            pstmt.setDouble(32, this.get(i).getNetProfitRate());
            if(this.get(i).getSatrap() == null || this.get(i).getSatrap().equals("null")) {
            	pstmt.setString(33,null);
            } else {
            	pstmt.setString(33, this.get(i).getSatrap());
            }
            if(this.get(i).getActuCtrl() == null || this.get(i).getActuCtrl().equals("null")) {
            	pstmt.setString(34,null);
            } else {
            	pstmt.setString(34, this.get(i).getActuCtrl());
            }
            if(this.get(i).getLicense() == null || this.get(i).getLicense().equals("null")) {
            	pstmt.setString(35,null);
            } else {
            	pstmt.setString(35, this.get(i).getLicense());
            }
            if(this.get(i).getSocialInsuCode() == null || this.get(i).getSocialInsuCode().equals("null")) {
            	pstmt.setString(36,null);
            } else {
            	pstmt.setString(36, this.get(i).getSocialInsuCode());
            }
            if(this.get(i).getOrganizationCode() == null || this.get(i).getOrganizationCode().equals("null")) {
            	pstmt.setString(37,null);
            } else {
            	pstmt.setString(37, this.get(i).getOrganizationCode());
            }
            if(this.get(i).getTaxCode() == null || this.get(i).getTaxCode().equals("null")) {
            	pstmt.setString(38,null);
            } else {
            	pstmt.setString(38, this.get(i).getTaxCode());
            }
            if(this.get(i).getFax() == null || this.get(i).getFax().equals("null")) {
            	pstmt.setString(39,null);
            } else {
            	pstmt.setString(39, this.get(i).getFax());
            }
            if(this.get(i).getEMail() == null || this.get(i).getEMail().equals("null")) {
            	pstmt.setString(40,null);
            } else {
            	pstmt.setString(40, this.get(i).getEMail());
            }
            if(this.get(i).getFoundDate() == null || this.get(i).getFoundDate().equals("null")) {
                pstmt.setDate(41,null);
            } else {
                pstmt.setDate(41, Date.valueOf(this.get(i).getFoundDate()));
            }
            if(this.get(i).getRemark() == null || this.get(i).getRemark().equals("null")) {
            	pstmt.setString(42,null);
            } else {
            	pstmt.setString(42, this.get(i).getRemark());
            }
            if(this.get(i).getSegment1() == null || this.get(i).getSegment1().equals("null")) {
            	pstmt.setString(43,null);
            } else {
            	pstmt.setString(43, this.get(i).getSegment1());
            }
            if(this.get(i).getSegment2() == null || this.get(i).getSegment2().equals("null")) {
            	pstmt.setString(44,null);
            } else {
            	pstmt.setString(44, this.get(i).getSegment2());
            }
            if(this.get(i).getSegment3() == null || this.get(i).getSegment3().equals("null")) {
            	pstmt.setString(45,null);
            } else {
            	pstmt.setString(45, this.get(i).getSegment3());
            }
            if(this.get(i).getManageCom() == null || this.get(i).getManageCom().equals("null")) {
            	pstmt.setString(46,null);
            } else {
            	pstmt.setString(46, this.get(i).getManageCom());
            }
            if(this.get(i).getComCode() == null || this.get(i).getComCode().equals("null")) {
            	pstmt.setString(47,null);
            } else {
            	pstmt.setString(47, this.get(i).getComCode());
            }
            if(this.get(i).getModifyOperator() == null || this.get(i).getModifyOperator().equals("null")) {
            	pstmt.setString(48,null);
            } else {
            	pstmt.setString(48, this.get(i).getModifyOperator());
            }
            if(this.get(i).getSex() == null || this.get(i).getSex().equals("null")) {
            	pstmt.setString(49,null);
            } else {
            	pstmt.setString(49, this.get(i).getSex());
            }
            if(this.get(i).getNativePlace() == null || this.get(i).getNativePlace().equals("null")) {
            	pstmt.setString(50,null);
            } else {
            	pstmt.setString(50, this.get(i).getNativePlace());
            }
            if(this.get(i).getOccupation() == null || this.get(i).getOccupation().equals("null")) {
            	pstmt.setString(51,null);
            } else {
            	pstmt.setString(51, this.get(i).getOccupation());
            }
            if(this.get(i).getOccupationCode() == null || this.get(i).getOccupationCode().equals("null")) {
            	pstmt.setString(52,null);
            } else {
            	pstmt.setString(52, this.get(i).getOccupationCode());
            }
            if(this.get(i).getTaxPayerType() == null || this.get(i).getTaxPayerType().equals("null")) {
            	pstmt.setString(53,null);
            } else {
            	pstmt.setString(53, this.get(i).getTaxPayerType());
            }
            if(this.get(i).getFixedPlan() == null || this.get(i).getFixedPlan().equals("null")) {
            	pstmt.setString(54,null);
            } else {
            	pstmt.setString(54, this.get(i).getFixedPlan());
            }
            if(this.get(i).getAddTaxFlag() == null || this.get(i).getAddTaxFlag().equals("null")) {
            	pstmt.setString(55,null);
            } else {
            	pstmt.setString(55, this.get(i).getAddTaxFlag());
            }
            if(this.get(i).getAddTaxDate() == null || this.get(i).getAddTaxDate().equals("null")) {
                pstmt.setDate(56,null);
            } else {
                pstmt.setDate(56, Date.valueOf(this.get(i).getAddTaxDate()));
            }
            if(this.get(i).getBankAccTaxNo() == null || this.get(i).getBankAccTaxNo().equals("null")) {
            	pstmt.setString(57,null);
            } else {
            	pstmt.setString(57, this.get(i).getBankAccTaxNo());
            }
            if(this.get(i).getBankTaxName() == null || this.get(i).getBankTaxName().equals("null")) {
            	pstmt.setString(58,null);
            } else {
            	pstmt.setString(58, this.get(i).getBankTaxName());
            }
            if(this.get(i).getBankTaxCode() == null || this.get(i).getBankTaxCode().equals("null")) {
            	pstmt.setString(59,null);
            } else {
            	pstmt.setString(59, this.get(i).getBankTaxCode());
            }
            if(this.get(i).getGrpAppIDType() == null || this.get(i).getGrpAppIDType().equals("null")) {
            	pstmt.setString(60,null);
            } else {
            	pstmt.setString(60, this.get(i).getGrpAppIDType());
            }
            if(this.get(i).getGrpAppIDExpDate() == null || this.get(i).getGrpAppIDExpDate().equals("null")) {
                pstmt.setDate(61,null);
            } else {
                pstmt.setDate(61, Date.valueOf(this.get(i).getGrpAppIDExpDate()));
            }
            if(this.get(i).getCtrPeople() == null || this.get(i).getCtrPeople().equals("null")) {
            	pstmt.setString(62,null);
            } else {
            	pstmt.setString(62, this.get(i).getCtrPeople());
            }
            pstmt.setInt(63, this.get(i).getPeoples3());
            pstmt.setInt(64, this.get(i).getRelaPeoples());
            pstmt.setInt(65, this.get(i).getRelaMatePeoples());
            pstmt.setInt(66, this.get(i).getRelaYoungPeoples());
            pstmt.setInt(67, this.get(i).getRelaOtherPeoples());
            // set where condition
            if(this.get(i).getGrpContNo() == null || this.get(i).getGrpContNo().equals("null")) {
            	pstmt.setString(68,null);
            } else {
            	pstmt.setString(68, this.get(i).getGrpContNo());
            }
                pstmt.addBatch();
            }
            pstmt.executeBatch();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            ex.printStackTrace();
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LBGrpAppntDBSet";
            tError.functionName = "update()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            try {
                pstmt.close();
            } catch (Exception e){e.printStackTrace();}

            if( !mflag ) {
                try {
                    con.close();
                } catch (Exception e){e.printStackTrace();}
            }

            return false;
        }

        if( !mflag ) {
            try {
                con.close();
            } catch (Exception e){e.printStackTrace();}
        }

        return true;
    }

    /**
     * 新增操作
     * @return boolean
     */
    public boolean insert() {
        PreparedStatement pstmt = null;

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try
        {
            int tCount = this.size();
            pstmt = con.prepareStatement("INSERT INTO LBGrpAppnt VALUES( ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ?)");
            for (int i = 1; i <= tCount; i++) {
            if(this.get(i).getGrpContNo() == null || this.get(i).getGrpContNo().equals("null")) {
            	pstmt.setString(1,null);
            } else {
            	pstmt.setString(1, this.get(i).getGrpContNo());
            }
            if(this.get(i).getCustomerNo() == null || this.get(i).getCustomerNo().equals("null")) {
            	pstmt.setString(2,null);
            } else {
            	pstmt.setString(2, this.get(i).getCustomerNo());
            }
            if(this.get(i).getPrtNo() == null || this.get(i).getPrtNo().equals("null")) {
            	pstmt.setString(3,null);
            } else {
            	pstmt.setString(3, this.get(i).getPrtNo());
            }
            if(this.get(i).getAddressNo() == null || this.get(i).getAddressNo().equals("null")) {
            	pstmt.setString(4,null);
            } else {
            	pstmt.setString(4, this.get(i).getAddressNo());
            }
            if(this.get(i).getAppntGrade() == null || this.get(i).getAppntGrade().equals("null")) {
            	pstmt.setString(5,null);
            } else {
            	pstmt.setString(5, this.get(i).getAppntGrade());
            }
            if(this.get(i).getName() == null || this.get(i).getName().equals("null")) {
            	pstmt.setString(6,null);
            } else {
            	pstmt.setString(6, this.get(i).getName());
            }
            if(this.get(i).getPostalAddress() == null || this.get(i).getPostalAddress().equals("null")) {
            	pstmt.setString(7,null);
            } else {
            	pstmt.setString(7, this.get(i).getPostalAddress());
            }
            if(this.get(i).getZipCode() == null || this.get(i).getZipCode().equals("null")) {
            	pstmt.setString(8,null);
            } else {
            	pstmt.setString(8, this.get(i).getZipCode());
            }
            if(this.get(i).getPhone() == null || this.get(i).getPhone().equals("null")) {
            	pstmt.setString(9,null);
            } else {
            	pstmt.setString(9, this.get(i).getPhone());
            }
            if(this.get(i).getPassword() == null || this.get(i).getPassword().equals("null")) {
            	pstmt.setString(10,null);
            } else {
            	pstmt.setString(10, this.get(i).getPassword());
            }
            if(this.get(i).getState() == null || this.get(i).getState().equals("null")) {
            	pstmt.setString(11,null);
            } else {
            	pstmt.setString(11, this.get(i).getState());
            }
            if(this.get(i).getAppntType() == null || this.get(i).getAppntType().equals("null")) {
            	pstmt.setString(12,null);
            } else {
            	pstmt.setString(12, this.get(i).getAppntType());
            }
            if(this.get(i).getRelationToInsured() == null || this.get(i).getRelationToInsured().equals("null")) {
            	pstmt.setString(13,null);
            } else {
            	pstmt.setString(13, this.get(i).getRelationToInsured());
            }
            if(this.get(i).getOperator() == null || this.get(i).getOperator().equals("null")) {
            	pstmt.setString(14,null);
            } else {
            	pstmt.setString(14, this.get(i).getOperator());
            }
            if(this.get(i).getMakeDate() == null || this.get(i).getMakeDate().equals("null")) {
                pstmt.setDate(15,null);
            } else {
                pstmt.setDate(15, Date.valueOf(this.get(i).getMakeDate()));
            }
            if(this.get(i).getMakeTime() == null || this.get(i).getMakeTime().equals("null")) {
            	pstmt.setString(16,null);
            } else {
            	pstmt.setString(16, this.get(i).getMakeTime());
            }
            if(this.get(i).getModifyDate() == null || this.get(i).getModifyDate().equals("null")) {
                pstmt.setDate(17,null);
            } else {
                pstmt.setDate(17, Date.valueOf(this.get(i).getModifyDate()));
            }
            if(this.get(i).getModifyTime() == null || this.get(i).getModifyTime().equals("null")) {
            	pstmt.setString(18,null);
            } else {
            	pstmt.setString(18, this.get(i).getModifyTime());
            }
            if(this.get(i).getBusiCategory() == null || this.get(i).getBusiCategory().equals("null")) {
            	pstmt.setString(19,null);
            } else {
            	pstmt.setString(19, this.get(i).getBusiCategory());
            }
            if(this.get(i).getGrpNature() == null || this.get(i).getGrpNature().equals("null")) {
            	pstmt.setString(20,null);
            } else {
            	pstmt.setString(20, this.get(i).getGrpNature());
            }
            pstmt.setInt(21, this.get(i).getSumNumPeople());
            if(this.get(i).getMainBusiness() == null || this.get(i).getMainBusiness().equals("null")) {
            	pstmt.setString(22,null);
            } else {
            	pstmt.setString(22, this.get(i).getMainBusiness());
            }
            if(this.get(i).getCorporation() == null || this.get(i).getCorporation().equals("null")) {
            	pstmt.setString(23,null);
            } else {
            	pstmt.setString(23, this.get(i).getCorporation());
            }
            if(this.get(i).getCorIDType() == null || this.get(i).getCorIDType().equals("null")) {
            	pstmt.setString(24,null);
            } else {
            	pstmt.setString(24, this.get(i).getCorIDType());
            }
            if(this.get(i).getCorID() == null || this.get(i).getCorID().equals("null")) {
            	pstmt.setString(25,null);
            } else {
            	pstmt.setString(25, this.get(i).getCorID());
            }
            if(this.get(i).getCorIDExpiryDate() == null || this.get(i).getCorIDExpiryDate().equals("null")) {
                pstmt.setDate(26,null);
            } else {
                pstmt.setDate(26, Date.valueOf(this.get(i).getCorIDExpiryDate()));
            }
            pstmt.setInt(27, this.get(i).getOnJobNumber());
            pstmt.setInt(28, this.get(i).getRetireNumber());
            pstmt.setInt(29, this.get(i).getOtherNumber());
            pstmt.setDouble(30, this.get(i).getRgtCapital());
            pstmt.setDouble(31, this.get(i).getTotalAssets());
            pstmt.setDouble(32, this.get(i).getNetProfitRate());
            if(this.get(i).getSatrap() == null || this.get(i).getSatrap().equals("null")) {
            	pstmt.setString(33,null);
            } else {
            	pstmt.setString(33, this.get(i).getSatrap());
            }
            if(this.get(i).getActuCtrl() == null || this.get(i).getActuCtrl().equals("null")) {
            	pstmt.setString(34,null);
            } else {
            	pstmt.setString(34, this.get(i).getActuCtrl());
            }
            if(this.get(i).getLicense() == null || this.get(i).getLicense().equals("null")) {
            	pstmt.setString(35,null);
            } else {
            	pstmt.setString(35, this.get(i).getLicense());
            }
            if(this.get(i).getSocialInsuCode() == null || this.get(i).getSocialInsuCode().equals("null")) {
            	pstmt.setString(36,null);
            } else {
            	pstmt.setString(36, this.get(i).getSocialInsuCode());
            }
            if(this.get(i).getOrganizationCode() == null || this.get(i).getOrganizationCode().equals("null")) {
            	pstmt.setString(37,null);
            } else {
            	pstmt.setString(37, this.get(i).getOrganizationCode());
            }
            if(this.get(i).getTaxCode() == null || this.get(i).getTaxCode().equals("null")) {
            	pstmt.setString(38,null);
            } else {
            	pstmt.setString(38, this.get(i).getTaxCode());
            }
            if(this.get(i).getFax() == null || this.get(i).getFax().equals("null")) {
            	pstmt.setString(39,null);
            } else {
            	pstmt.setString(39, this.get(i).getFax());
            }
            if(this.get(i).getEMail() == null || this.get(i).getEMail().equals("null")) {
            	pstmt.setString(40,null);
            } else {
            	pstmt.setString(40, this.get(i).getEMail());
            }
            if(this.get(i).getFoundDate() == null || this.get(i).getFoundDate().equals("null")) {
                pstmt.setDate(41,null);
            } else {
                pstmt.setDate(41, Date.valueOf(this.get(i).getFoundDate()));
            }
            if(this.get(i).getRemark() == null || this.get(i).getRemark().equals("null")) {
            	pstmt.setString(42,null);
            } else {
            	pstmt.setString(42, this.get(i).getRemark());
            }
            if(this.get(i).getSegment1() == null || this.get(i).getSegment1().equals("null")) {
            	pstmt.setString(43,null);
            } else {
            	pstmt.setString(43, this.get(i).getSegment1());
            }
            if(this.get(i).getSegment2() == null || this.get(i).getSegment2().equals("null")) {
            	pstmt.setString(44,null);
            } else {
            	pstmt.setString(44, this.get(i).getSegment2());
            }
            if(this.get(i).getSegment3() == null || this.get(i).getSegment3().equals("null")) {
            	pstmt.setString(45,null);
            } else {
            	pstmt.setString(45, this.get(i).getSegment3());
            }
            if(this.get(i).getManageCom() == null || this.get(i).getManageCom().equals("null")) {
            	pstmt.setString(46,null);
            } else {
            	pstmt.setString(46, this.get(i).getManageCom());
            }
            if(this.get(i).getComCode() == null || this.get(i).getComCode().equals("null")) {
            	pstmt.setString(47,null);
            } else {
            	pstmt.setString(47, this.get(i).getComCode());
            }
            if(this.get(i).getModifyOperator() == null || this.get(i).getModifyOperator().equals("null")) {
            	pstmt.setString(48,null);
            } else {
            	pstmt.setString(48, this.get(i).getModifyOperator());
            }
            if(this.get(i).getSex() == null || this.get(i).getSex().equals("null")) {
            	pstmt.setString(49,null);
            } else {
            	pstmt.setString(49, this.get(i).getSex());
            }
            if(this.get(i).getNativePlace() == null || this.get(i).getNativePlace().equals("null")) {
            	pstmt.setString(50,null);
            } else {
            	pstmt.setString(50, this.get(i).getNativePlace());
            }
            if(this.get(i).getOccupation() == null || this.get(i).getOccupation().equals("null")) {
            	pstmt.setString(51,null);
            } else {
            	pstmt.setString(51, this.get(i).getOccupation());
            }
            if(this.get(i).getOccupationCode() == null || this.get(i).getOccupationCode().equals("null")) {
            	pstmt.setString(52,null);
            } else {
            	pstmt.setString(52, this.get(i).getOccupationCode());
            }
            if(this.get(i).getTaxPayerType() == null || this.get(i).getTaxPayerType().equals("null")) {
            	pstmt.setString(53,null);
            } else {
            	pstmt.setString(53, this.get(i).getTaxPayerType());
            }
            if(this.get(i).getFixedPlan() == null || this.get(i).getFixedPlan().equals("null")) {
            	pstmt.setString(54,null);
            } else {
            	pstmt.setString(54, this.get(i).getFixedPlan());
            }
            if(this.get(i).getAddTaxFlag() == null || this.get(i).getAddTaxFlag().equals("null")) {
            	pstmt.setString(55,null);
            } else {
            	pstmt.setString(55, this.get(i).getAddTaxFlag());
            }
            if(this.get(i).getAddTaxDate() == null || this.get(i).getAddTaxDate().equals("null")) {
                pstmt.setDate(56,null);
            } else {
                pstmt.setDate(56, Date.valueOf(this.get(i).getAddTaxDate()));
            }
            if(this.get(i).getBankAccTaxNo() == null || this.get(i).getBankAccTaxNo().equals("null")) {
            	pstmt.setString(57,null);
            } else {
            	pstmt.setString(57, this.get(i).getBankAccTaxNo());
            }
            if(this.get(i).getBankTaxName() == null || this.get(i).getBankTaxName().equals("null")) {
            	pstmt.setString(58,null);
            } else {
            	pstmt.setString(58, this.get(i).getBankTaxName());
            }
            if(this.get(i).getBankTaxCode() == null || this.get(i).getBankTaxCode().equals("null")) {
            	pstmt.setString(59,null);
            } else {
            	pstmt.setString(59, this.get(i).getBankTaxCode());
            }
            if(this.get(i).getGrpAppIDType() == null || this.get(i).getGrpAppIDType().equals("null")) {
            	pstmt.setString(60,null);
            } else {
            	pstmt.setString(60, this.get(i).getGrpAppIDType());
            }
            if(this.get(i).getGrpAppIDExpDate() == null || this.get(i).getGrpAppIDExpDate().equals("null")) {
                pstmt.setDate(61,null);
            } else {
                pstmt.setDate(61, Date.valueOf(this.get(i).getGrpAppIDExpDate()));
            }
            if(this.get(i).getCtrPeople() == null || this.get(i).getCtrPeople().equals("null")) {
            	pstmt.setString(62,null);
            } else {
            	pstmt.setString(62, this.get(i).getCtrPeople());
            }
            pstmt.setInt(63, this.get(i).getPeoples3());
            pstmt.setInt(64, this.get(i).getRelaPeoples());
            pstmt.setInt(65, this.get(i).getRelaMatePeoples());
            pstmt.setInt(66, this.get(i).getRelaYoungPeoples());
            pstmt.setInt(67, this.get(i).getRelaOtherPeoples());
                pstmt.addBatch();
            }
            pstmt.executeBatch();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            ex.printStackTrace();
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LBGrpAppntDBSet";
            tError.functionName = "insert()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            try {
                pstmt.close();
            } catch (Exception e){e.printStackTrace();}

            if( !mflag ) {
                try {
                    con.close();
                } catch (Exception e){e.printStackTrace();}
            }

            return false;
        }

        if( !mflag ) {
            try {
                con.close();
            } catch (Exception e){e.printStackTrace();}
        }

        return true;
    }
}
