/**
 * Copyright (c) 2017 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.entity;

import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.Pojo;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;

import java.io.Serializable;

/**
 * <p>ClassName: LMInsuAccValuePojo </p>
 * <p>Copyright: Copyright (c) 2017</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2017-10-17
 */
public class LMInsuAccValuePojo implements Pojo,Serializable {
    // @Field
    /** 险种编码 */
    private String RiskCode; 
    /** 保险帐户号码 */
    private String InsuAccNo; 
    /** 计价日期 */
    private String  ValueDate;
    /** 价格应公布日期 */
    private String  SRateDate;
    /** 价格实际公布日期 */
    private String  ARateDate;
    /** 买入单位价格 */
    private double RateIn; 
    /** 卖出单位价格 */
    private double RateOut; 
    /** 赎回比例 */
    private double RedeemRate; 
    /** 赎回金额 */
    private double RedeemMoney; 
    /** 操作员 */
    private String Operator; 
    /** 入机日期 */
    private String  MakeDate;
    /** 入机时间 */
    private String MakeTime; 


    public static final int FIELDNUM = 12;    // 数据库表的字段个数

    private FDate fDate = new FDate();        // 处理日期
    public String getRiskCode() {
        return RiskCode;
    }
    public void setRiskCode(String aRiskCode) {
        RiskCode = aRiskCode;
    }
    public String getInsuAccNo() {
        return InsuAccNo;
    }
    public void setInsuAccNo(String aInsuAccNo) {
        InsuAccNo = aInsuAccNo;
    }
    public String getValueDate() {
        return ValueDate;
    }
    public void setValueDate(String aValueDate) {
        ValueDate = aValueDate;
    }
    public String getSRateDate() {
        return SRateDate;
    }
    public void setSRateDate(String aSRateDate) {
        SRateDate = aSRateDate;
    }
    public String getARateDate() {
        return ARateDate;
    }
    public void setARateDate(String aARateDate) {
        ARateDate = aARateDate;
    }
    public double getRateIn() {
        return RateIn;
    }
    public void setRateIn(double aRateIn) {
        RateIn = aRateIn;
    }
    public void setRateIn(String aRateIn) {
        if (aRateIn != null && !aRateIn.equals("")) {
            Double tDouble = new Double(aRateIn);
            double d = tDouble.doubleValue();
            RateIn = d;
        }
    }

    public double getRateOut() {
        return RateOut;
    }
    public void setRateOut(double aRateOut) {
        RateOut = aRateOut;
    }
    public void setRateOut(String aRateOut) {
        if (aRateOut != null && !aRateOut.equals("")) {
            Double tDouble = new Double(aRateOut);
            double d = tDouble.doubleValue();
            RateOut = d;
        }
    }

    public double getRedeemRate() {
        return RedeemRate;
    }
    public void setRedeemRate(double aRedeemRate) {
        RedeemRate = aRedeemRate;
    }
    public void setRedeemRate(String aRedeemRate) {
        if (aRedeemRate != null && !aRedeemRate.equals("")) {
            Double tDouble = new Double(aRedeemRate);
            double d = tDouble.doubleValue();
            RedeemRate = d;
        }
    }

    public double getRedeemMoney() {
        return RedeemMoney;
    }
    public void setRedeemMoney(double aRedeemMoney) {
        RedeemMoney = aRedeemMoney;
    }
    public void setRedeemMoney(String aRedeemMoney) {
        if (aRedeemMoney != null && !aRedeemMoney.equals("")) {
            Double tDouble = new Double(aRedeemMoney);
            double d = tDouble.doubleValue();
            RedeemMoney = d;
        }
    }

    public String getOperator() {
        return Operator;
    }
    public void setOperator(String aOperator) {
        Operator = aOperator;
    }
    public String getMakeDate() {
        return MakeDate;
    }
    public void setMakeDate(String aMakeDate) {
        MakeDate = aMakeDate;
    }
    public String getMakeTime() {
        return MakeTime;
    }
    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("RiskCode") ) {
            return 0;
        }
        if( strFieldName.equals("InsuAccNo") ) {
            return 1;
        }
        if( strFieldName.equals("ValueDate") ) {
            return 2;
        }
        if( strFieldName.equals("SRateDate") ) {
            return 3;
        }
        if( strFieldName.equals("ARateDate") ) {
            return 4;
        }
        if( strFieldName.equals("RateIn") ) {
            return 5;
        }
        if( strFieldName.equals("RateOut") ) {
            return 6;
        }
        if( strFieldName.equals("RedeemRate") ) {
            return 7;
        }
        if( strFieldName.equals("RedeemMoney") ) {
            return 8;
        }
        if( strFieldName.equals("Operator") ) {
            return 9;
        }
        if( strFieldName.equals("MakeDate") ) {
            return 10;
        }
        if( strFieldName.equals("MakeTime") ) {
            return 11;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "RiskCode";
                break;
            case 1:
                strFieldName = "InsuAccNo";
                break;
            case 2:
                strFieldName = "ValueDate";
                break;
            case 3:
                strFieldName = "SRateDate";
                break;
            case 4:
                strFieldName = "ARateDate";
                break;
            case 5:
                strFieldName = "RateIn";
                break;
            case 6:
                strFieldName = "RateOut";
                break;
            case 7:
                strFieldName = "RedeemRate";
                break;
            case 8:
                strFieldName = "RedeemMoney";
                break;
            case 9:
                strFieldName = "Operator";
                break;
            case 10:
                strFieldName = "MakeDate";
                break;
            case 11:
                strFieldName = "MakeTime";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "RISKCODE":
                return Schema.TYPE_STRING;
            case "INSUACCNO":
                return Schema.TYPE_STRING;
            case "VALUEDATE":
                return Schema.TYPE_STRING;
            case "SRATEDATE":
                return Schema.TYPE_STRING;
            case "ARATEDATE":
                return Schema.TYPE_STRING;
            case "RATEIN":
                return Schema.TYPE_DOUBLE;
            case "RATEOUT":
                return Schema.TYPE_DOUBLE;
            case "REDEEMRATE":
                return Schema.TYPE_DOUBLE;
            case "REDEEMMONEY":
                return Schema.TYPE_DOUBLE;
            case "OPERATOR":
                return Schema.TYPE_STRING;
            case "MAKEDATE":
                return Schema.TYPE_STRING;
            case "MAKETIME":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_STRING;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_DOUBLE;
            case 6:
                return Schema.TYPE_DOUBLE;
            case 7:
                return Schema.TYPE_DOUBLE;
            case 8:
                return Schema.TYPE_DOUBLE;
            case 9:
                return Schema.TYPE_STRING;
            case 10:
                return Schema.TYPE_STRING;
            case 11:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("RiskCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RiskCode));
        }
        if (FCode.equalsIgnoreCase("InsuAccNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InsuAccNo));
        }
        if (FCode.equalsIgnoreCase("ValueDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ValueDate));
        }
        if (FCode.equalsIgnoreCase("SRateDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SRateDate));
        }
        if (FCode.equalsIgnoreCase("ARateDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ARateDate));
        }
        if (FCode.equalsIgnoreCase("RateIn")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RateIn));
        }
        if (FCode.equalsIgnoreCase("RateOut")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RateOut));
        }
        if (FCode.equalsIgnoreCase("RedeemRate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RedeemRate));
        }
        if (FCode.equalsIgnoreCase("RedeemMoney")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RedeemMoney));
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeDate));
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = String.valueOf(RiskCode);
                break;
            case 1:
                strFieldValue = String.valueOf(InsuAccNo);
                break;
            case 2:
                strFieldValue = String.valueOf(ValueDate);
                break;
            case 3:
                strFieldValue = String.valueOf(SRateDate);
                break;
            case 4:
                strFieldValue = String.valueOf(ARateDate);
                break;
            case 5:
                strFieldValue = String.valueOf(RateIn);
                break;
            case 6:
                strFieldValue = String.valueOf(RateOut);
                break;
            case 7:
                strFieldValue = String.valueOf(RedeemRate);
                break;
            case 8:
                strFieldValue = String.valueOf(RedeemMoney);
                break;
            case 9:
                strFieldValue = String.valueOf(Operator);
                break;
            case 10:
                strFieldValue = String.valueOf(MakeDate);
                break;
            case 11:
                strFieldValue = String.valueOf(MakeTime);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("RiskCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                RiskCode = FValue.trim();
            }
            else
                RiskCode = null;
        }
        if (FCode.equalsIgnoreCase("InsuAccNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                InsuAccNo = FValue.trim();
            }
            else
                InsuAccNo = null;
        }
        if (FCode.equalsIgnoreCase("ValueDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                ValueDate = FValue.trim();
            }
            else
                ValueDate = null;
        }
        if (FCode.equalsIgnoreCase("SRateDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                SRateDate = FValue.trim();
            }
            else
                SRateDate = null;
        }
        if (FCode.equalsIgnoreCase("ARateDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                ARateDate = FValue.trim();
            }
            else
                ARateDate = null;
        }
        if (FCode.equalsIgnoreCase("RateIn")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                RateIn = d;
            }
        }
        if (FCode.equalsIgnoreCase("RateOut")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                RateOut = d;
            }
        }
        if (FCode.equalsIgnoreCase("RedeemRate")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                RedeemRate = d;
            }
        }
        if (FCode.equalsIgnoreCase("RedeemMoney")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                RedeemMoney = d;
            }
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            if( FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
                Operator = null;
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeDate = FValue.trim();
            }
            else
                MakeDate = null;
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
                MakeTime = null;
        }
        return true;
    }


    public String toString() {
    return "LMInsuAccValuePojo [" +
            "RiskCode="+RiskCode +
            ", InsuAccNo="+InsuAccNo +
            ", ValueDate="+ValueDate +
            ", SRateDate="+SRateDate +
            ", ARateDate="+ARateDate +
            ", RateIn="+RateIn +
            ", RateOut="+RateOut +
            ", RedeemRate="+RedeemRate +
            ", RedeemMoney="+RedeemMoney +
            ", Operator="+Operator +
            ", MakeDate="+MakeDate +
            ", MakeTime="+MakeTime +"]";
    }
}
