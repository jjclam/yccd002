/**
 * Copyright (c) 2017 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.entity;

import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.Pojo;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;

import java.io.Serializable;

/**
 * <p>ClassName: LACommisionDetailBPojo </p>
 * <p>Copyright: Copyright (c) 2017</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2017-10-17
 */
public class LACommisionDetailBPojo implements Pojo,Serializable {
    // @Field
    /** Id */
    private long CommisionDetailID; 
    /** Shardingid */
    private String ShardingID; 
    /** 转储号 */
    private String EdorNo; 
    /** 转储类型 */
    private String EdorType; 
    /** 集体合同号码 */
    private String GrpContNo; 
    /** 代理人编码 */
    private String AgentCode; 
    /** 业务百分比 */
    private double BusiRate; 
    /** 操作员 */
    private String Operator; 
    /** 入机日期 */
    private String  MakeDate;
    /** 入机时间 */
    private String MakeTime; 
    /** 最后一次修改日期 */
    private String  ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime; 
    /** 代理人组别 */
    private String AgentGroup; 
    /** 保单类型 */
    private String PolType; 
    /** 服务起期 */
    private String  StartSerDate;
    /** 服务止期 */
    private String  EndSerDate;
    /** 原入机日期 */
    private String  MakeDate2;
    /** 原入机时间 */
    private String MakeTime2; 
    /** 代理人标志 */
    private String AgentFlag; 
    /** Salesrate */
    private double SalesRate; 
    /** 与投保人关系 */
    private String RelationShip; 


    public static final int FIELDNUM = 21;    // 数据库表的字段个数

    private FDate fDate = new FDate();        // 处理日期
    public long getCommisionDetailID() {
        return CommisionDetailID;
    }
    public void setCommisionDetailID(long aCommisionDetailID) {
        CommisionDetailID = aCommisionDetailID;
    }
    public void setCommisionDetailID(String aCommisionDetailID) {
        if (aCommisionDetailID != null && !aCommisionDetailID.equals("")) {
            CommisionDetailID = new Long(aCommisionDetailID).longValue();
        }
    }

    public String getShardingID() {
        return ShardingID;
    }
    public void setShardingID(String aShardingID) {
        ShardingID = aShardingID;
    }
    public String getEdorNo() {
        return EdorNo;
    }
    public void setEdorNo(String aEdorNo) {
        EdorNo = aEdorNo;
    }
    public String getEdorType() {
        return EdorType;
    }
    public void setEdorType(String aEdorType) {
        EdorType = aEdorType;
    }
    public String getGrpContNo() {
        return GrpContNo;
    }
    public void setGrpContNo(String aGrpContNo) {
        GrpContNo = aGrpContNo;
    }
    public String getAgentCode() {
        return AgentCode;
    }
    public void setAgentCode(String aAgentCode) {
        AgentCode = aAgentCode;
    }
    public double getBusiRate() {
        return BusiRate;
    }
    public void setBusiRate(double aBusiRate) {
        BusiRate = aBusiRate;
    }
    public void setBusiRate(String aBusiRate) {
        if (aBusiRate != null && !aBusiRate.equals("")) {
            Double tDouble = new Double(aBusiRate);
            double d = tDouble.doubleValue();
            BusiRate = d;
        }
    }

    public String getOperator() {
        return Operator;
    }
    public void setOperator(String aOperator) {
        Operator = aOperator;
    }
    public String getMakeDate() {
        return MakeDate;
    }
    public void setMakeDate(String aMakeDate) {
        MakeDate = aMakeDate;
    }
    public String getMakeTime() {
        return MakeTime;
    }
    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }
    public String getModifyDate() {
        return ModifyDate;
    }
    public void setModifyDate(String aModifyDate) {
        ModifyDate = aModifyDate;
    }
    public String getModifyTime() {
        return ModifyTime;
    }
    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }
    public String getAgentGroup() {
        return AgentGroup;
    }
    public void setAgentGroup(String aAgentGroup) {
        AgentGroup = aAgentGroup;
    }
    public String getPolType() {
        return PolType;
    }
    public void setPolType(String aPolType) {
        PolType = aPolType;
    }
    public String getStartSerDate() {
        return StartSerDate;
    }
    public void setStartSerDate(String aStartSerDate) {
        StartSerDate = aStartSerDate;
    }
    public String getEndSerDate() {
        return EndSerDate;
    }
    public void setEndSerDate(String aEndSerDate) {
        EndSerDate = aEndSerDate;
    }
    public String getMakeDate2() {
        return MakeDate2;
    }
    public void setMakeDate2(String aMakeDate2) {
        MakeDate2 = aMakeDate2;
    }
    public String getMakeTime2() {
        return MakeTime2;
    }
    public void setMakeTime2(String aMakeTime2) {
        MakeTime2 = aMakeTime2;
    }
    public String getAgentFlag() {
        return AgentFlag;
    }
    public void setAgentFlag(String aAgentFlag) {
        AgentFlag = aAgentFlag;
    }
    public double getSalesRate() {
        return SalesRate;
    }
    public void setSalesRate(double aSalesRate) {
        SalesRate = aSalesRate;
    }
    public void setSalesRate(String aSalesRate) {
        if (aSalesRate != null && !aSalesRate.equals("")) {
            Double tDouble = new Double(aSalesRate);
            double d = tDouble.doubleValue();
            SalesRate = d;
        }
    }

    public String getRelationShip() {
        return RelationShip;
    }
    public void setRelationShip(String aRelationShip) {
        RelationShip = aRelationShip;
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("CommisionDetailID") ) {
            return 0;
        }
        if( strFieldName.equals("ShardingID") ) {
            return 1;
        }
        if( strFieldName.equals("EdorNo") ) {
            return 2;
        }
        if( strFieldName.equals("EdorType") ) {
            return 3;
        }
        if( strFieldName.equals("GrpContNo") ) {
            return 4;
        }
        if( strFieldName.equals("AgentCode") ) {
            return 5;
        }
        if( strFieldName.equals("BusiRate") ) {
            return 6;
        }
        if( strFieldName.equals("Operator") ) {
            return 7;
        }
        if( strFieldName.equals("MakeDate") ) {
            return 8;
        }
        if( strFieldName.equals("MakeTime") ) {
            return 9;
        }
        if( strFieldName.equals("ModifyDate") ) {
            return 10;
        }
        if( strFieldName.equals("ModifyTime") ) {
            return 11;
        }
        if( strFieldName.equals("AgentGroup") ) {
            return 12;
        }
        if( strFieldName.equals("PolType") ) {
            return 13;
        }
        if( strFieldName.equals("StartSerDate") ) {
            return 14;
        }
        if( strFieldName.equals("EndSerDate") ) {
            return 15;
        }
        if( strFieldName.equals("MakeDate2") ) {
            return 16;
        }
        if( strFieldName.equals("MakeTime2") ) {
            return 17;
        }
        if( strFieldName.equals("AgentFlag") ) {
            return 18;
        }
        if( strFieldName.equals("SalesRate") ) {
            return 19;
        }
        if( strFieldName.equals("RelationShip") ) {
            return 20;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "CommisionDetailID";
                break;
            case 1:
                strFieldName = "ShardingID";
                break;
            case 2:
                strFieldName = "EdorNo";
                break;
            case 3:
                strFieldName = "EdorType";
                break;
            case 4:
                strFieldName = "GrpContNo";
                break;
            case 5:
                strFieldName = "AgentCode";
                break;
            case 6:
                strFieldName = "BusiRate";
                break;
            case 7:
                strFieldName = "Operator";
                break;
            case 8:
                strFieldName = "MakeDate";
                break;
            case 9:
                strFieldName = "MakeTime";
                break;
            case 10:
                strFieldName = "ModifyDate";
                break;
            case 11:
                strFieldName = "ModifyTime";
                break;
            case 12:
                strFieldName = "AgentGroup";
                break;
            case 13:
                strFieldName = "PolType";
                break;
            case 14:
                strFieldName = "StartSerDate";
                break;
            case 15:
                strFieldName = "EndSerDate";
                break;
            case 16:
                strFieldName = "MakeDate2";
                break;
            case 17:
                strFieldName = "MakeTime2";
                break;
            case 18:
                strFieldName = "AgentFlag";
                break;
            case 19:
                strFieldName = "SalesRate";
                break;
            case 20:
                strFieldName = "RelationShip";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "COMMISIONDETAILID":
                return Schema.TYPE_LONG;
            case "SHARDINGID":
                return Schema.TYPE_STRING;
            case "EDORNO":
                return Schema.TYPE_STRING;
            case "EDORTYPE":
                return Schema.TYPE_STRING;
            case "GRPCONTNO":
                return Schema.TYPE_STRING;
            case "AGENTCODE":
                return Schema.TYPE_STRING;
            case "BUSIRATE":
                return Schema.TYPE_DOUBLE;
            case "OPERATOR":
                return Schema.TYPE_STRING;
            case "MAKEDATE":
                return Schema.TYPE_STRING;
            case "MAKETIME":
                return Schema.TYPE_STRING;
            case "MODIFYDATE":
                return Schema.TYPE_STRING;
            case "MODIFYTIME":
                return Schema.TYPE_STRING;
            case "AGENTGROUP":
                return Schema.TYPE_STRING;
            case "POLTYPE":
                return Schema.TYPE_STRING;
            case "STARTSERDATE":
                return Schema.TYPE_STRING;
            case "ENDSERDATE":
                return Schema.TYPE_STRING;
            case "MAKEDATE2":
                return Schema.TYPE_STRING;
            case "MAKETIME2":
                return Schema.TYPE_STRING;
            case "AGENTFLAG":
                return Schema.TYPE_STRING;
            case "SALESRATE":
                return Schema.TYPE_DOUBLE;
            case "RELATIONSHIP":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_LONG;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_STRING;
            case 6:
                return Schema.TYPE_DOUBLE;
            case 7:
                return Schema.TYPE_STRING;
            case 8:
                return Schema.TYPE_STRING;
            case 9:
                return Schema.TYPE_STRING;
            case 10:
                return Schema.TYPE_STRING;
            case 11:
                return Schema.TYPE_STRING;
            case 12:
                return Schema.TYPE_STRING;
            case 13:
                return Schema.TYPE_STRING;
            case 14:
                return Schema.TYPE_STRING;
            case 15:
                return Schema.TYPE_STRING;
            case 16:
                return Schema.TYPE_STRING;
            case 17:
                return Schema.TYPE_STRING;
            case 18:
                return Schema.TYPE_STRING;
            case 19:
                return Schema.TYPE_DOUBLE;
            case 20:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("CommisionDetailID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CommisionDetailID));
        }
        if (FCode.equalsIgnoreCase("ShardingID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ShardingID));
        }
        if (FCode.equalsIgnoreCase("EdorNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(EdorNo));
        }
        if (FCode.equalsIgnoreCase("EdorType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(EdorType));
        }
        if (FCode.equalsIgnoreCase("GrpContNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GrpContNo));
        }
        if (FCode.equalsIgnoreCase("AgentCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentCode));
        }
        if (FCode.equalsIgnoreCase("BusiRate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BusiRate));
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeDate));
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyDate));
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (FCode.equalsIgnoreCase("AgentGroup")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentGroup));
        }
        if (FCode.equalsIgnoreCase("PolType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PolType));
        }
        if (FCode.equalsIgnoreCase("StartSerDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StartSerDate));
        }
        if (FCode.equalsIgnoreCase("EndSerDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(EndSerDate));
        }
        if (FCode.equalsIgnoreCase("MakeDate2")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeDate2));
        }
        if (FCode.equalsIgnoreCase("MakeTime2")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime2));
        }
        if (FCode.equalsIgnoreCase("AgentFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentFlag));
        }
        if (FCode.equalsIgnoreCase("SalesRate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SalesRate));
        }
        if (FCode.equalsIgnoreCase("RelationShip")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RelationShip));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = String.valueOf(CommisionDetailID);
                break;
            case 1:
                strFieldValue = String.valueOf(ShardingID);
                break;
            case 2:
                strFieldValue = String.valueOf(EdorNo);
                break;
            case 3:
                strFieldValue = String.valueOf(EdorType);
                break;
            case 4:
                strFieldValue = String.valueOf(GrpContNo);
                break;
            case 5:
                strFieldValue = String.valueOf(AgentCode);
                break;
            case 6:
                strFieldValue = String.valueOf(BusiRate);
                break;
            case 7:
                strFieldValue = String.valueOf(Operator);
                break;
            case 8:
                strFieldValue = String.valueOf(MakeDate);
                break;
            case 9:
                strFieldValue = String.valueOf(MakeTime);
                break;
            case 10:
                strFieldValue = String.valueOf(ModifyDate);
                break;
            case 11:
                strFieldValue = String.valueOf(ModifyTime);
                break;
            case 12:
                strFieldValue = String.valueOf(AgentGroup);
                break;
            case 13:
                strFieldValue = String.valueOf(PolType);
                break;
            case 14:
                strFieldValue = String.valueOf(StartSerDate);
                break;
            case 15:
                strFieldValue = String.valueOf(EndSerDate);
                break;
            case 16:
                strFieldValue = String.valueOf(MakeDate2);
                break;
            case 17:
                strFieldValue = String.valueOf(MakeTime2);
                break;
            case 18:
                strFieldValue = String.valueOf(AgentFlag);
                break;
            case 19:
                strFieldValue = String.valueOf(SalesRate);
                break;
            case 20:
                strFieldValue = String.valueOf(RelationShip);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("CommisionDetailID")) {
            if( FValue != null && !FValue.equals("")) {
                CommisionDetailID = new Long(FValue).longValue();
            }
        }
        if (FCode.equalsIgnoreCase("ShardingID")) {
            if( FValue != null && !FValue.equals(""))
            {
                ShardingID = FValue.trim();
            }
            else
                ShardingID = null;
        }
        if (FCode.equalsIgnoreCase("EdorNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                EdorNo = FValue.trim();
            }
            else
                EdorNo = null;
        }
        if (FCode.equalsIgnoreCase("EdorType")) {
            if( FValue != null && !FValue.equals(""))
            {
                EdorType = FValue.trim();
            }
            else
                EdorType = null;
        }
        if (FCode.equalsIgnoreCase("GrpContNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                GrpContNo = FValue.trim();
            }
            else
                GrpContNo = null;
        }
        if (FCode.equalsIgnoreCase("AgentCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                AgentCode = FValue.trim();
            }
            else
                AgentCode = null;
        }
        if (FCode.equalsIgnoreCase("BusiRate")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                BusiRate = d;
            }
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            if( FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
                Operator = null;
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeDate = FValue.trim();
            }
            else
                MakeDate = null;
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
                MakeTime = null;
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyDate = FValue.trim();
            }
            else
                ModifyDate = null;
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
                ModifyTime = null;
        }
        if (FCode.equalsIgnoreCase("AgentGroup")) {
            if( FValue != null && !FValue.equals(""))
            {
                AgentGroup = FValue.trim();
            }
            else
                AgentGroup = null;
        }
        if (FCode.equalsIgnoreCase("PolType")) {
            if( FValue != null && !FValue.equals(""))
            {
                PolType = FValue.trim();
            }
            else
                PolType = null;
        }
        if (FCode.equalsIgnoreCase("StartSerDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                StartSerDate = FValue.trim();
            }
            else
                StartSerDate = null;
        }
        if (FCode.equalsIgnoreCase("EndSerDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                EndSerDate = FValue.trim();
            }
            else
                EndSerDate = null;
        }
        if (FCode.equalsIgnoreCase("MakeDate2")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeDate2 = FValue.trim();
            }
            else
                MakeDate2 = null;
        }
        if (FCode.equalsIgnoreCase("MakeTime2")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeTime2 = FValue.trim();
            }
            else
                MakeTime2 = null;
        }
        if (FCode.equalsIgnoreCase("AgentFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                AgentFlag = FValue.trim();
            }
            else
                AgentFlag = null;
        }
        if (FCode.equalsIgnoreCase("SalesRate")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                SalesRate = d;
            }
        }
        if (FCode.equalsIgnoreCase("RelationShip")) {
            if( FValue != null && !FValue.equals(""))
            {
                RelationShip = FValue.trim();
            }
            else
                RelationShip = null;
        }
        return true;
    }


    public String toString() {
    return "LACommisionDetailBPojo [" +
            "CommisionDetailID="+CommisionDetailID +
            ", ShardingID="+ShardingID +
            ", EdorNo="+EdorNo +
            ", EdorType="+EdorType +
            ", GrpContNo="+GrpContNo +
            ", AgentCode="+AgentCode +
            ", BusiRate="+BusiRate +
            ", Operator="+Operator +
            ", MakeDate="+MakeDate +
            ", MakeTime="+MakeTime +
            ", ModifyDate="+ModifyDate +
            ", ModifyTime="+ModifyTime +
            ", AgentGroup="+AgentGroup +
            ", PolType="+PolType +
            ", StartSerDate="+StartSerDate +
            ", EndSerDate="+EndSerDate +
            ", MakeDate2="+MakeDate2 +
            ", MakeTime2="+MakeTime2 +
            ", AgentFlag="+AgentFlag +
            ", SalesRate="+SalesRate +
            ", RelationShip="+RelationShip +"]";
    }
}
