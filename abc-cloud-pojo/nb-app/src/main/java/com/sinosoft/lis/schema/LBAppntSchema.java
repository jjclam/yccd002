/**
 * Copyright (c) 2017 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import com.sinosoft.lis.db.LBAppntDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.*;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

/**
 * <p>ClassName: LBAppntSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2017</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2017-10-21
 */
public class LBAppntSchema implements Schema, Cloneable {
    // @Field
    /** Id */
    private long AppntID;
    /** Shardingid */
    private String ShardingID;
    /** 批单号 */
    private String EdorNo;
    /** 集体合同号码 */
    private String GrpContNo;
    /** 合同号码 */
    private String ContNo;
    /** 印刷号码 */
    private String PrtNo;
    /** 投保人客户号码 */
    private String AppntNo;
    /** 投保人级别 */
    private String AppntGrade;
    /** 投保人名称 */
    private String AppntName;
    /** 投保人性别 */
    private String AppntSex;
    /** 投保人出生日期 */
    private Date AppntBirthday;
    /** 投保人类型 */
    private String AppntType;
    /** 客户地址号码 */
    private String AddressNo;
    /** 证件类型 */
    private String IDType;
    /** 证件号码 */
    private String IDNo;
    /** 国籍 */
    private String NativePlace;
    /** 民族 */
    private String Nationality;
    /** 户口所在地 */
    private String RgtAddress;
    /** 婚姻状况 */
    private String Marriage;
    /** 结婚日期 */
    private Date MarriageDate;
    /** 健康状况 */
    private String Health;
    /** 身高 */
    private double Stature;
    /** 体重 */
    private double Avoirdupois;
    /** 学历 */
    private String Degree;
    /** 信用等级 */
    private String CreditGrade;
    /** 银行编码 */
    private String BankCode;
    /** 银行帐号 */
    private String BankAccNo;
    /** 银行帐户名 */
    private String AccName;
    /** 入司日期 */
    private Date JoinCompanyDate;
    /** 参加工作日期 */
    private Date StartWorkDate;
    /** 职位 */
    private String Position;
    /** 工资 */
    private double Salary;
    /** 职业类别 */
    private String OccupationType;
    /** 职业代码 */
    private String OccupationCode;
    /** 职业（工种） */
    private String WorkType;
    /** 兼职（工种） */
    private String PluralityType;
    /** 是否吸烟标志 */
    private String SmokeFlag;
    /** 操作员 */
    private String Operator;
    /** 管理机构 */
    private String ManageCom;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 最后一次修改日期 */
    private Date ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime;
    /** 身体指标 */
    private double BMI;
    /** 驾照 */
    private String License;
    /** 驾照类型 */
    private String LicenseType;
    /** 与被保人关系 */
    private String RelatToInsu;
    /** 职业描述 */
    private String OccupationDesb;
    /** 证件有效期 */
    private String IdValiDate;
    /** 是否有摩托车驾照 */
    private String HaveMotorcycleLicence;
    /** 兼职 */
    private String PartTimeJob;
    /** Firstname */
    private String FirstName;
    /** Lastname */
    private String LastName;
    /** 客户级别 */
    private String CUSLevel;
    /** 居民类型 */
    private String AppRgtTpye;
    /** 美国纳税人识别号 */
    private String TINNO;
    /** 是否为美国纳税义务的个人 */
    private String TINFlag;

    public static final int FIELDNUM = 57;    // 数据库表的字段个数

    private static String[] PK;                // 主键

    private FDate fDate = new FDate();        // 处理日期

    public CErrors mErrors;            // 错误信息

    // @Constructor
    public LBAppntSchema() {
        mErrors = new CErrors();

        String[] pk = new String[1];
        pk[0] = "AppntID";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException {
        LBAppntSchema cloned = (LBAppntSchema)super.clone();
        cloned.fDate = (FDate) fDate.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK() {
        return PK;
    }

    public long getAppntID() {
        return AppntID;
    }
    public void setAppntID(long aAppntID) {
        AppntID = aAppntID;
    }
    public void setAppntID(String aAppntID) {
        if (aAppntID != null && !aAppntID.equals("")) {
            AppntID = new Long(aAppntID).longValue();
        }
    }

    public String getShardingID() {
        return ShardingID;
    }
    public void setShardingID(String aShardingID) {
        ShardingID = aShardingID;
    }
    public String getEdorNo() {
        return EdorNo;
    }
    public void setEdorNo(String aEdorNo) {
        EdorNo = aEdorNo;
    }
    public String getGrpContNo() {
        return GrpContNo;
    }
    public void setGrpContNo(String aGrpContNo) {
        GrpContNo = aGrpContNo;
    }
    public String getContNo() {
        return ContNo;
    }
    public void setContNo(String aContNo) {
        ContNo = aContNo;
    }
    public String getPrtNo() {
        return PrtNo;
    }
    public void setPrtNo(String aPrtNo) {
        PrtNo = aPrtNo;
    }
    public String getAppntNo() {
        return AppntNo;
    }
    public void setAppntNo(String aAppntNo) {
        AppntNo = aAppntNo;
    }
    public String getAppntGrade() {
        return AppntGrade;
    }
    public void setAppntGrade(String aAppntGrade) {
        AppntGrade = aAppntGrade;
    }
    public String getAppntName() {
        return AppntName;
    }
    public void setAppntName(String aAppntName) {
        AppntName = aAppntName;
    }
    public String getAppntSex() {
        return AppntSex;
    }
    public void setAppntSex(String aAppntSex) {
        AppntSex = aAppntSex;
    }
    public String getAppntBirthday() {
        if(AppntBirthday != null) {
            return fDate.getString(AppntBirthday);
        } else {
            return null;
        }
    }
    public void setAppntBirthday(Date aAppntBirthday) {
        AppntBirthday = aAppntBirthday;
    }
    public void setAppntBirthday(String aAppntBirthday) {
        if (aAppntBirthday != null && !aAppntBirthday.equals("")) {
            AppntBirthday = fDate.getDate(aAppntBirthday);
        } else
            AppntBirthday = null;
    }

    public String getAppntType() {
        return AppntType;
    }
    public void setAppntType(String aAppntType) {
        AppntType = aAppntType;
    }
    public String getAddressNo() {
        return AddressNo;
    }
    public void setAddressNo(String aAddressNo) {
        AddressNo = aAddressNo;
    }
    public String getIDType() {
        return IDType;
    }
    public void setIDType(String aIDType) {
        IDType = aIDType;
    }
    public String getIDNo() {
        return IDNo;
    }
    public void setIDNo(String aIDNo) {
        IDNo = aIDNo;
    }
    public String getNativePlace() {
        return NativePlace;
    }
    public void setNativePlace(String aNativePlace) {
        NativePlace = aNativePlace;
    }
    public String getNationality() {
        return Nationality;
    }
    public void setNationality(String aNationality) {
        Nationality = aNationality;
    }
    public String getRgtAddress() {
        return RgtAddress;
    }
    public void setRgtAddress(String aRgtAddress) {
        RgtAddress = aRgtAddress;
    }
    public String getMarriage() {
        return Marriage;
    }
    public void setMarriage(String aMarriage) {
        Marriage = aMarriage;
    }
    public String getMarriageDate() {
        if(MarriageDate != null) {
            return fDate.getString(MarriageDate);
        } else {
            return null;
        }
    }
    public void setMarriageDate(Date aMarriageDate) {
        MarriageDate = aMarriageDate;
    }
    public void setMarriageDate(String aMarriageDate) {
        if (aMarriageDate != null && !aMarriageDate.equals("")) {
            MarriageDate = fDate.getDate(aMarriageDate);
        } else
            MarriageDate = null;
    }

    public String getHealth() {
        return Health;
    }
    public void setHealth(String aHealth) {
        Health = aHealth;
    }
    public double getStature() {
        return Stature;
    }
    public void setStature(double aStature) {
        Stature = aStature;
    }
    public void setStature(String aStature) {
        if (aStature != null && !aStature.equals("")) {
            Double tDouble = new Double(aStature);
            double d = tDouble.doubleValue();
            Stature = d;
        }
    }

    public double getAvoirdupois() {
        return Avoirdupois;
    }
    public void setAvoirdupois(double aAvoirdupois) {
        Avoirdupois = aAvoirdupois;
    }
    public void setAvoirdupois(String aAvoirdupois) {
        if (aAvoirdupois != null && !aAvoirdupois.equals("")) {
            Double tDouble = new Double(aAvoirdupois);
            double d = tDouble.doubleValue();
            Avoirdupois = d;
        }
    }

    public String getDegree() {
        return Degree;
    }
    public void setDegree(String aDegree) {
        Degree = aDegree;
    }
    public String getCreditGrade() {
        return CreditGrade;
    }
    public void setCreditGrade(String aCreditGrade) {
        CreditGrade = aCreditGrade;
    }
    public String getBankCode() {
        return BankCode;
    }
    public void setBankCode(String aBankCode) {
        BankCode = aBankCode;
    }
    public String getBankAccNo() {
        return BankAccNo;
    }
    public void setBankAccNo(String aBankAccNo) {
        BankAccNo = aBankAccNo;
    }
    public String getAccName() {
        return AccName;
    }
    public void setAccName(String aAccName) {
        AccName = aAccName;
    }
    public String getJoinCompanyDate() {
        if(JoinCompanyDate != null) {
            return fDate.getString(JoinCompanyDate);
        } else {
            return null;
        }
    }
    public void setJoinCompanyDate(Date aJoinCompanyDate) {
        JoinCompanyDate = aJoinCompanyDate;
    }
    public void setJoinCompanyDate(String aJoinCompanyDate) {
        if (aJoinCompanyDate != null && !aJoinCompanyDate.equals("")) {
            JoinCompanyDate = fDate.getDate(aJoinCompanyDate);
        } else
            JoinCompanyDate = null;
    }

    public String getStartWorkDate() {
        if(StartWorkDate != null) {
            return fDate.getString(StartWorkDate);
        } else {
            return null;
        }
    }
    public void setStartWorkDate(Date aStartWorkDate) {
        StartWorkDate = aStartWorkDate;
    }
    public void setStartWorkDate(String aStartWorkDate) {
        if (aStartWorkDate != null && !aStartWorkDate.equals("")) {
            StartWorkDate = fDate.getDate(aStartWorkDate);
        } else
            StartWorkDate = null;
    }

    public String getPosition() {
        return Position;
    }
    public void setPosition(String aPosition) {
        Position = aPosition;
    }
    public double getSalary() {
        return Salary;
    }
    public void setSalary(double aSalary) {
        Salary = aSalary;
    }
    public void setSalary(String aSalary) {
        if (aSalary != null && !aSalary.equals("")) {
            Double tDouble = new Double(aSalary);
            double d = tDouble.doubleValue();
            Salary = d;
        }
    }

    public String getOccupationType() {
        return OccupationType;
    }
    public void setOccupationType(String aOccupationType) {
        OccupationType = aOccupationType;
    }
    public String getOccupationCode() {
        return OccupationCode;
    }
    public void setOccupationCode(String aOccupationCode) {
        OccupationCode = aOccupationCode;
    }
    public String getWorkType() {
        return WorkType;
    }
    public void setWorkType(String aWorkType) {
        WorkType = aWorkType;
    }
    public String getPluralityType() {
        return PluralityType;
    }
    public void setPluralityType(String aPluralityType) {
        PluralityType = aPluralityType;
    }
    public String getSmokeFlag() {
        return SmokeFlag;
    }
    public void setSmokeFlag(String aSmokeFlag) {
        SmokeFlag = aSmokeFlag;
    }
    public String getOperator() {
        return Operator;
    }
    public void setOperator(String aOperator) {
        Operator = aOperator;
    }
    public String getManageCom() {
        return ManageCom;
    }
    public void setManageCom(String aManageCom) {
        ManageCom = aManageCom;
    }
    public String getMakeDate() {
        if(MakeDate != null) {
            return fDate.getString(MakeDate);
        } else {
            return null;
        }
    }
    public void setMakeDate(Date aMakeDate) {
        MakeDate = aMakeDate;
    }
    public void setMakeDate(String aMakeDate) {
        if (aMakeDate != null && !aMakeDate.equals("")) {
            MakeDate = fDate.getDate(aMakeDate);
        } else
            MakeDate = null;
    }

    public String getMakeTime() {
        return MakeTime;
    }
    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }
    public String getModifyDate() {
        if(ModifyDate != null) {
            return fDate.getString(ModifyDate);
        } else {
            return null;
        }
    }
    public void setModifyDate(Date aModifyDate) {
        ModifyDate = aModifyDate;
    }
    public void setModifyDate(String aModifyDate) {
        if (aModifyDate != null && !aModifyDate.equals("")) {
            ModifyDate = fDate.getDate(aModifyDate);
        } else
            ModifyDate = null;
    }

    public String getModifyTime() {
        return ModifyTime;
    }
    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }
    public double getBMI() {
        return BMI;
    }
    public void setBMI(double aBMI) {
        BMI = aBMI;
    }
    public void setBMI(String aBMI) {
        if (aBMI != null && !aBMI.equals("")) {
            Double tDouble = new Double(aBMI);
            double d = tDouble.doubleValue();
            BMI = d;
        }
    }

    public String getLicense() {
        return License;
    }
    public void setLicense(String aLicense) {
        License = aLicense;
    }
    public String getLicenseType() {
        return LicenseType;
    }
    public void setLicenseType(String aLicenseType) {
        LicenseType = aLicenseType;
    }
    public String getRelatToInsu() {
        return RelatToInsu;
    }
    public void setRelatToInsu(String aRelatToInsu) {
        RelatToInsu = aRelatToInsu;
    }
    public String getOccupationDesb() {
        return OccupationDesb;
    }
    public void setOccupationDesb(String aOccupationDesb) {
        OccupationDesb = aOccupationDesb;
    }
    public String getIdValiDate() {
        return IdValiDate;
    }
    public void setIdValiDate(String aIdValiDate) {
        IdValiDate = aIdValiDate;
    }
    public String getHaveMotorcycleLicence() {
        return HaveMotorcycleLicence;
    }
    public void setHaveMotorcycleLicence(String aHaveMotorcycleLicence) {
        HaveMotorcycleLicence = aHaveMotorcycleLicence;
    }
    public String getPartTimeJob() {
        return PartTimeJob;
    }
    public void setPartTimeJob(String aPartTimeJob) {
        PartTimeJob = aPartTimeJob;
    }
    public String getFirstName() {
        return FirstName;
    }
    public void setFirstName(String aFirstName) {
        FirstName = aFirstName;
    }
    public String getLastName() {
        return LastName;
    }
    public void setLastName(String aLastName) {
        LastName = aLastName;
    }
    public String getCUSLevel() {
        return CUSLevel;
    }
    public void setCUSLevel(String aCUSLevel) {
        CUSLevel = aCUSLevel;
    }
    public String getAppRgtTpye() {
        return AppRgtTpye;
    }
    public void setAppRgtTpye(String aAppRgtTpye) {
        AppRgtTpye = aAppRgtTpye;
    }
    public String getTINNO() {
        return TINNO;
    }
    public void setTINNO(String aTINNO) {
        TINNO = aTINNO;
    }
    public String getTINFlag() {
        return TINFlag;
    }
    public void setTINFlag(String aTINFlag) {
        TINFlag = aTINFlag;
    }

    /**
    * 使用另外一个 LBAppntSchema 对象给 Schema 赋值
    * @param: aLBAppntSchema LBAppntSchema
    **/
    public void setSchema(LBAppntSchema aLBAppntSchema) {
        this.AppntID = aLBAppntSchema.getAppntID();
        this.ShardingID = aLBAppntSchema.getShardingID();
        this.EdorNo = aLBAppntSchema.getEdorNo();
        this.GrpContNo = aLBAppntSchema.getGrpContNo();
        this.ContNo = aLBAppntSchema.getContNo();
        this.PrtNo = aLBAppntSchema.getPrtNo();
        this.AppntNo = aLBAppntSchema.getAppntNo();
        this.AppntGrade = aLBAppntSchema.getAppntGrade();
        this.AppntName = aLBAppntSchema.getAppntName();
        this.AppntSex = aLBAppntSchema.getAppntSex();
        this.AppntBirthday = fDate.getDate( aLBAppntSchema.getAppntBirthday());
        this.AppntType = aLBAppntSchema.getAppntType();
        this.AddressNo = aLBAppntSchema.getAddressNo();
        this.IDType = aLBAppntSchema.getIDType();
        this.IDNo = aLBAppntSchema.getIDNo();
        this.NativePlace = aLBAppntSchema.getNativePlace();
        this.Nationality = aLBAppntSchema.getNationality();
        this.RgtAddress = aLBAppntSchema.getRgtAddress();
        this.Marriage = aLBAppntSchema.getMarriage();
        this.MarriageDate = fDate.getDate( aLBAppntSchema.getMarriageDate());
        this.Health = aLBAppntSchema.getHealth();
        this.Stature = aLBAppntSchema.getStature();
        this.Avoirdupois = aLBAppntSchema.getAvoirdupois();
        this.Degree = aLBAppntSchema.getDegree();
        this.CreditGrade = aLBAppntSchema.getCreditGrade();
        this.BankCode = aLBAppntSchema.getBankCode();
        this.BankAccNo = aLBAppntSchema.getBankAccNo();
        this.AccName = aLBAppntSchema.getAccName();
        this.JoinCompanyDate = fDate.getDate( aLBAppntSchema.getJoinCompanyDate());
        this.StartWorkDate = fDate.getDate( aLBAppntSchema.getStartWorkDate());
        this.Position = aLBAppntSchema.getPosition();
        this.Salary = aLBAppntSchema.getSalary();
        this.OccupationType = aLBAppntSchema.getOccupationType();
        this.OccupationCode = aLBAppntSchema.getOccupationCode();
        this.WorkType = aLBAppntSchema.getWorkType();
        this.PluralityType = aLBAppntSchema.getPluralityType();
        this.SmokeFlag = aLBAppntSchema.getSmokeFlag();
        this.Operator = aLBAppntSchema.getOperator();
        this.ManageCom = aLBAppntSchema.getManageCom();
        this.MakeDate = fDate.getDate( aLBAppntSchema.getMakeDate());
        this.MakeTime = aLBAppntSchema.getMakeTime();
        this.ModifyDate = fDate.getDate( aLBAppntSchema.getModifyDate());
        this.ModifyTime = aLBAppntSchema.getModifyTime();
        this.BMI = aLBAppntSchema.getBMI();
        this.License = aLBAppntSchema.getLicense();
        this.LicenseType = aLBAppntSchema.getLicenseType();
        this.RelatToInsu = aLBAppntSchema.getRelatToInsu();
        this.OccupationDesb = aLBAppntSchema.getOccupationDesb();
        this.IdValiDate = aLBAppntSchema.getIdValiDate();
        this.HaveMotorcycleLicence = aLBAppntSchema.getHaveMotorcycleLicence();
        this.PartTimeJob = aLBAppntSchema.getPartTimeJob();
        this.FirstName = aLBAppntSchema.getFirstName();
        this.LastName = aLBAppntSchema.getLastName();
        this.CUSLevel = aLBAppntSchema.getCUSLevel();
        this.AppRgtTpye = aLBAppntSchema.getAppRgtTpye();
        this.TINNO = aLBAppntSchema.getTINNO();
        this.TINFlag = aLBAppntSchema.getTINFlag();
    }

    /**
    * 使用 ResultSet 中的第 i 行给 Schema 赋值
    * @param: rs ResultSet
    * @param: i int
    * @return: boolean
    **/
    public boolean setSchema(ResultSet rs,int i) {
        try {
            //rs.absolute(i);        // 非滚动游标
            this.AppntID = rs.getLong("AppntID");
            if( rs.getString("ShardingID") == null )
                this.ShardingID = null;
            else
                this.ShardingID = rs.getString("ShardingID").trim();

            if( rs.getString("EdorNo") == null )
                this.EdorNo = null;
            else
                this.EdorNo = rs.getString("EdorNo").trim();

            if( rs.getString("GrpContNo") == null )
                this.GrpContNo = null;
            else
                this.GrpContNo = rs.getString("GrpContNo").trim();

            if( rs.getString("ContNo") == null )
                this.ContNo = null;
            else
                this.ContNo = rs.getString("ContNo").trim();

            if( rs.getString("PrtNo") == null )
                this.PrtNo = null;
            else
                this.PrtNo = rs.getString("PrtNo").trim();

            if( rs.getString("AppntNo") == null )
                this.AppntNo = null;
            else
                this.AppntNo = rs.getString("AppntNo").trim();

            if( rs.getString("AppntGrade") == null )
                this.AppntGrade = null;
            else
                this.AppntGrade = rs.getString("AppntGrade").trim();

            if( rs.getString("AppntName") == null )
                this.AppntName = null;
            else
                this.AppntName = rs.getString("AppntName").trim();

            if( rs.getString("AppntSex") == null )
                this.AppntSex = null;
            else
                this.AppntSex = rs.getString("AppntSex").trim();

            this.AppntBirthday = rs.getDate("AppntBirthday");
            if( rs.getString("AppntType") == null )
                this.AppntType = null;
            else
                this.AppntType = rs.getString("AppntType").trim();

            if( rs.getString("AddressNo") == null )
                this.AddressNo = null;
            else
                this.AddressNo = rs.getString("AddressNo").trim();

            if( rs.getString("IDType") == null )
                this.IDType = null;
            else
                this.IDType = rs.getString("IDType").trim();

            if( rs.getString("IDNo") == null )
                this.IDNo = null;
            else
                this.IDNo = rs.getString("IDNo").trim();

            if( rs.getString("NativePlace") == null )
                this.NativePlace = null;
            else
                this.NativePlace = rs.getString("NativePlace").trim();

            if( rs.getString("Nationality") == null )
                this.Nationality = null;
            else
                this.Nationality = rs.getString("Nationality").trim();

            if( rs.getString("RgtAddress") == null )
                this.RgtAddress = null;
            else
                this.RgtAddress = rs.getString("RgtAddress").trim();

            if( rs.getString("Marriage") == null )
                this.Marriage = null;
            else
                this.Marriage = rs.getString("Marriage").trim();

            this.MarriageDate = rs.getDate("MarriageDate");
            if( rs.getString("Health") == null )
                this.Health = null;
            else
                this.Health = rs.getString("Health").trim();

            this.Stature = rs.getDouble("Stature");
            this.Avoirdupois = rs.getDouble("Avoirdupois");
            if( rs.getString("Degree") == null )
                this.Degree = null;
            else
                this.Degree = rs.getString("Degree").trim();

            if( rs.getString("CreditGrade") == null )
                this.CreditGrade = null;
            else
                this.CreditGrade = rs.getString("CreditGrade").trim();

            if( rs.getString("BankCode") == null )
                this.BankCode = null;
            else
                this.BankCode = rs.getString("BankCode").trim();

            if( rs.getString("BankAccNo") == null )
                this.BankAccNo = null;
            else
                this.BankAccNo = rs.getString("BankAccNo").trim();

            if( rs.getString("AccName") == null )
                this.AccName = null;
            else
                this.AccName = rs.getString("AccName").trim();

            this.JoinCompanyDate = rs.getDate("JoinCompanyDate");
            this.StartWorkDate = rs.getDate("StartWorkDate");
            if( rs.getString("Position") == null )
                this.Position = null;
            else
                this.Position = rs.getString("Position").trim();

            this.Salary = rs.getDouble("Salary");
            if( rs.getString("OccupationType") == null )
                this.OccupationType = null;
            else
                this.OccupationType = rs.getString("OccupationType").trim();

            if( rs.getString("OccupationCode") == null )
                this.OccupationCode = null;
            else
                this.OccupationCode = rs.getString("OccupationCode").trim();

            if( rs.getString("WorkType") == null )
                this.WorkType = null;
            else
                this.WorkType = rs.getString("WorkType").trim();

            if( rs.getString("PluralityType") == null )
                this.PluralityType = null;
            else
                this.PluralityType = rs.getString("PluralityType").trim();

            if( rs.getString("SmokeFlag") == null )
                this.SmokeFlag = null;
            else
                this.SmokeFlag = rs.getString("SmokeFlag").trim();

            if( rs.getString("Operator") == null )
                this.Operator = null;
            else
                this.Operator = rs.getString("Operator").trim();

            if( rs.getString("ManageCom") == null )
                this.ManageCom = null;
            else
                this.ManageCom = rs.getString("ManageCom").trim();

            this.MakeDate = rs.getDate("MakeDate");
            if( rs.getString("MakeTime") == null )
                this.MakeTime = null;
            else
                this.MakeTime = rs.getString("MakeTime").trim();

            this.ModifyDate = rs.getDate("ModifyDate");
            if( rs.getString("ModifyTime") == null )
                this.ModifyTime = null;
            else
                this.ModifyTime = rs.getString("ModifyTime").trim();

            this.BMI = rs.getDouble("BMI");
            if( rs.getString("License") == null )
                this.License = null;
            else
                this.License = rs.getString("License").trim();

            if( rs.getString("LicenseType") == null )
                this.LicenseType = null;
            else
                this.LicenseType = rs.getString("LicenseType").trim();

            if( rs.getString("RelatToInsu") == null )
                this.RelatToInsu = null;
            else
                this.RelatToInsu = rs.getString("RelatToInsu").trim();

            if( rs.getString("OccupationDesb") == null )
                this.OccupationDesb = null;
            else
                this.OccupationDesb = rs.getString("OccupationDesb").trim();

            if( rs.getString("IdValiDate") == null )
                this.IdValiDate = null;
            else
                this.IdValiDate = rs.getString("IdValiDate").trim();

            if( rs.getString("HaveMotorcycleLicence") == null )
                this.HaveMotorcycleLicence = null;
            else
                this.HaveMotorcycleLicence = rs.getString("HaveMotorcycleLicence").trim();

            if( rs.getString("PartTimeJob") == null )
                this.PartTimeJob = null;
            else
                this.PartTimeJob = rs.getString("PartTimeJob").trim();

            if( rs.getString("FirstName") == null )
                this.FirstName = null;
            else
                this.FirstName = rs.getString("FirstName").trim();

            if( rs.getString("LastName") == null )
                this.LastName = null;
            else
                this.LastName = rs.getString("LastName").trim();

            if( rs.getString("CUSLevel") == null )
                this.CUSLevel = null;
            else
                this.CUSLevel = rs.getString("CUSLevel").trim();

            if( rs.getString("AppRgtTpye") == null )
                this.AppRgtTpye = null;
            else
                this.AppRgtTpye = rs.getString("AppRgtTpye").trim();

            if( rs.getString("TINNO") == null )
                this.TINNO = null;
            else
                this.TINNO = rs.getString("TINNO").trim();

            if( rs.getString("TINFlag") == null )
                this.TINFlag = null;
            else
                this.TINFlag = rs.getString("TINFlag").trim();

        }
        catch(SQLException sqle) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LBAppntSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = ExceptionUtils.exceptionToString(sqle);
            this.mErrors .addOneError(tError);
            return false;
        }
        return true;
    }

    public LBAppntSchema getSchema() {
        LBAppntSchema aLBAppntSchema = new LBAppntSchema();
        aLBAppntSchema.setSchema(this);
        return aLBAppntSchema;
    }

    public LBAppntDB getDB() {
        LBAppntDB aDBOper = new LBAppntDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
    * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLBAppnt描述/A>表字段
    * @return: String 返回打包后字符串
    **/
    public String encode() {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(ChgData.chgData(AppntID));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ShardingID)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(EdorNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(GrpContNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ContNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(PrtNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AppntNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AppntGrade)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AppntName)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AppntSex)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( AppntBirthday ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AppntType)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AddressNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(IDType)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(IDNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(NativePlace)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Nationality)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(RgtAddress)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Marriage)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( MarriageDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Health)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(Stature));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(Avoirdupois));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Degree)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(CreditGrade)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(BankCode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(BankAccNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AccName)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( JoinCompanyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( StartWorkDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Position)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(Salary));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(OccupationType)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(OccupationCode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(WorkType)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(PluralityType)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(SmokeFlag)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ManageCom)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ModifyTime)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(BMI));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(License)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(LicenseType)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(RelatToInsu)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(OccupationDesb)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(IdValiDate)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(HaveMotorcycleLicence)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(PartTimeJob)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(FirstName)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(LastName)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(CUSLevel)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AppRgtTpye)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(TINNO)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(TINFlag));
        return strReturn.toString();
    }

    /**
    * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLBAppnt>历史记账凭证主表信息</A>表字段
    * @param: strMessage String 包含一条纪录数据的字符串
    * @return: boolean
    **/
    public boolean decode(String strMessage) {
        try {
            AppntID = new Long(ChgData.chgNumericStr(StrTool.getStr(strMessage,1, SysConst.PACKAGESPILTER))).longValue();
            ShardingID = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
            EdorNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
            GrpContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
            ContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
            PrtNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
            AppntNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
            AppntGrade = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
            AppntName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
            AppntSex = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
            AppntBirthday = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER));
            AppntType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
            AddressNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
            IDType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER );
            IDNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15, SysConst.PACKAGESPILTER );
            NativePlace = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16, SysConst.PACKAGESPILTER );
            Nationality = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17, SysConst.PACKAGESPILTER );
            RgtAddress = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18, SysConst.PACKAGESPILTER );
            Marriage = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19, SysConst.PACKAGESPILTER );
            MarriageDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20, SysConst.PACKAGESPILTER));
            Health = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 21, SysConst.PACKAGESPILTER );
            Stature = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,22, SysConst.PACKAGESPILTER))).doubleValue();
            Avoirdupois = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,23, SysConst.PACKAGESPILTER))).doubleValue();
            Degree = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 24, SysConst.PACKAGESPILTER );
            CreditGrade = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 25, SysConst.PACKAGESPILTER );
            BankCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 26, SysConst.PACKAGESPILTER );
            BankAccNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 27, SysConst.PACKAGESPILTER );
            AccName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 28, SysConst.PACKAGESPILTER );
            JoinCompanyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 29, SysConst.PACKAGESPILTER));
            StartWorkDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 30, SysConst.PACKAGESPILTER));
            Position = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 31, SysConst.PACKAGESPILTER );
            Salary = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,32, SysConst.PACKAGESPILTER))).doubleValue();
            OccupationType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 33, SysConst.PACKAGESPILTER );
            OccupationCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 34, SysConst.PACKAGESPILTER );
            WorkType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 35, SysConst.PACKAGESPILTER );
            PluralityType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 36, SysConst.PACKAGESPILTER );
            SmokeFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 37, SysConst.PACKAGESPILTER );
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 38, SysConst.PACKAGESPILTER );
            ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 39, SysConst.PACKAGESPILTER );
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 40, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 41, SysConst.PACKAGESPILTER );
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 42, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 43, SysConst.PACKAGESPILTER );
            BMI = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,44, SysConst.PACKAGESPILTER))).doubleValue();
            License = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 45, SysConst.PACKAGESPILTER );
            LicenseType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 46, SysConst.PACKAGESPILTER );
            RelatToInsu = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 47, SysConst.PACKAGESPILTER );
            OccupationDesb = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 48, SysConst.PACKAGESPILTER );
            IdValiDate = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 49, SysConst.PACKAGESPILTER );
            HaveMotorcycleLicence = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 50, SysConst.PACKAGESPILTER );
            PartTimeJob = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 51, SysConst.PACKAGESPILTER );
            FirstName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 52, SysConst.PACKAGESPILTER );
            LastName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 53, SysConst.PACKAGESPILTER );
            CUSLevel = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 54, SysConst.PACKAGESPILTER );
            AppRgtTpye = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 55, SysConst.PACKAGESPILTER );
            TINNO = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 56, SysConst.PACKAGESPILTER );
            TINFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 57, SysConst.PACKAGESPILTER );
        }
        catch(NumberFormatException ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LBAppntSchema";
            tError.functionName = "decode";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            return false;
        }
        return true;
    }

    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("AppntID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AppntID));
        }
        if (FCode.equalsIgnoreCase("ShardingID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ShardingID));
        }
        if (FCode.equalsIgnoreCase("EdorNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(EdorNo));
        }
        if (FCode.equalsIgnoreCase("GrpContNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GrpContNo));
        }
        if (FCode.equalsIgnoreCase("ContNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ContNo));
        }
        if (FCode.equalsIgnoreCase("PrtNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PrtNo));
        }
        if (FCode.equalsIgnoreCase("AppntNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AppntNo));
        }
        if (FCode.equalsIgnoreCase("AppntGrade")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AppntGrade));
        }
        if (FCode.equalsIgnoreCase("AppntName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AppntName));
        }
        if (FCode.equalsIgnoreCase("AppntSex")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AppntSex));
        }
        if (FCode.equalsIgnoreCase("AppntBirthday")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getAppntBirthday()));
        }
        if (FCode.equalsIgnoreCase("AppntType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AppntType));
        }
        if (FCode.equalsIgnoreCase("AddressNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AddressNo));
        }
        if (FCode.equalsIgnoreCase("IDType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(IDType));
        }
        if (FCode.equalsIgnoreCase("IDNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(IDNo));
        }
        if (FCode.equalsIgnoreCase("NativePlace")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(NativePlace));
        }
        if (FCode.equalsIgnoreCase("Nationality")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Nationality));
        }
        if (FCode.equalsIgnoreCase("RgtAddress")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RgtAddress));
        }
        if (FCode.equalsIgnoreCase("Marriage")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Marriage));
        }
        if (FCode.equalsIgnoreCase("MarriageDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMarriageDate()));
        }
        if (FCode.equalsIgnoreCase("Health")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Health));
        }
        if (FCode.equalsIgnoreCase("Stature")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Stature));
        }
        if (FCode.equalsIgnoreCase("Avoirdupois")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Avoirdupois));
        }
        if (FCode.equalsIgnoreCase("Degree")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Degree));
        }
        if (FCode.equalsIgnoreCase("CreditGrade")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CreditGrade));
        }
        if (FCode.equalsIgnoreCase("BankCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BankCode));
        }
        if (FCode.equalsIgnoreCase("BankAccNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BankAccNo));
        }
        if (FCode.equalsIgnoreCase("AccName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AccName));
        }
        if (FCode.equalsIgnoreCase("JoinCompanyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getJoinCompanyDate()));
        }
        if (FCode.equalsIgnoreCase("StartWorkDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getStartWorkDate()));
        }
        if (FCode.equalsIgnoreCase("Position")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Position));
        }
        if (FCode.equalsIgnoreCase("Salary")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Salary));
        }
        if (FCode.equalsIgnoreCase("OccupationType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OccupationType));
        }
        if (FCode.equalsIgnoreCase("OccupationCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OccupationCode));
        }
        if (FCode.equalsIgnoreCase("WorkType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(WorkType));
        }
        if (FCode.equalsIgnoreCase("PluralityType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PluralityType));
        }
        if (FCode.equalsIgnoreCase("SmokeFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SmokeFlag));
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equalsIgnoreCase("ManageCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (FCode.equalsIgnoreCase("BMI")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BMI));
        }
        if (FCode.equalsIgnoreCase("License")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(License));
        }
        if (FCode.equalsIgnoreCase("LicenseType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(LicenseType));
        }
        if (FCode.equalsIgnoreCase("RelatToInsu")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RelatToInsu));
        }
        if (FCode.equalsIgnoreCase("OccupationDesb")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OccupationDesb));
        }
        if (FCode.equalsIgnoreCase("IdValiDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(IdValiDate));
        }
        if (FCode.equalsIgnoreCase("HaveMotorcycleLicence")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(HaveMotorcycleLicence));
        }
        if (FCode.equalsIgnoreCase("PartTimeJob")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PartTimeJob));
        }
        if (FCode.equalsIgnoreCase("FirstName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FirstName));
        }
        if (FCode.equalsIgnoreCase("LastName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(LastName));
        }
        if (FCode.equalsIgnoreCase("CUSLevel")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CUSLevel));
        }
        if (FCode.equalsIgnoreCase("AppRgtTpye")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AppRgtTpye));
        }
        if (FCode.equalsIgnoreCase("TINNO")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(TINNO));
        }
        if (FCode.equalsIgnoreCase("TINFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(TINFlag));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = String.valueOf(AppntID);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(ShardingID);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(EdorNo);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(GrpContNo);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(ContNo);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(PrtNo);
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(AppntNo);
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(AppntGrade);
                break;
            case 8:
                strFieldValue = StrTool.GBKToUnicode(AppntName);
                break;
            case 9:
                strFieldValue = StrTool.GBKToUnicode(AppntSex);
                break;
            case 10:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getAppntBirthday()));
                break;
            case 11:
                strFieldValue = StrTool.GBKToUnicode(AppntType);
                break;
            case 12:
                strFieldValue = StrTool.GBKToUnicode(AddressNo);
                break;
            case 13:
                strFieldValue = StrTool.GBKToUnicode(IDType);
                break;
            case 14:
                strFieldValue = StrTool.GBKToUnicode(IDNo);
                break;
            case 15:
                strFieldValue = StrTool.GBKToUnicode(NativePlace);
                break;
            case 16:
                strFieldValue = StrTool.GBKToUnicode(Nationality);
                break;
            case 17:
                strFieldValue = StrTool.GBKToUnicode(RgtAddress);
                break;
            case 18:
                strFieldValue = StrTool.GBKToUnicode(Marriage);
                break;
            case 19:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMarriageDate()));
                break;
            case 20:
                strFieldValue = StrTool.GBKToUnicode(Health);
                break;
            case 21:
                strFieldValue = String.valueOf(Stature);
                break;
            case 22:
                strFieldValue = String.valueOf(Avoirdupois);
                break;
            case 23:
                strFieldValue = StrTool.GBKToUnicode(Degree);
                break;
            case 24:
                strFieldValue = StrTool.GBKToUnicode(CreditGrade);
                break;
            case 25:
                strFieldValue = StrTool.GBKToUnicode(BankCode);
                break;
            case 26:
                strFieldValue = StrTool.GBKToUnicode(BankAccNo);
                break;
            case 27:
                strFieldValue = StrTool.GBKToUnicode(AccName);
                break;
            case 28:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getJoinCompanyDate()));
                break;
            case 29:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getStartWorkDate()));
                break;
            case 30:
                strFieldValue = StrTool.GBKToUnicode(Position);
                break;
            case 31:
                strFieldValue = String.valueOf(Salary);
                break;
            case 32:
                strFieldValue = StrTool.GBKToUnicode(OccupationType);
                break;
            case 33:
                strFieldValue = StrTool.GBKToUnicode(OccupationCode);
                break;
            case 34:
                strFieldValue = StrTool.GBKToUnicode(WorkType);
                break;
            case 35:
                strFieldValue = StrTool.GBKToUnicode(PluralityType);
                break;
            case 36:
                strFieldValue = StrTool.GBKToUnicode(SmokeFlag);
                break;
            case 37:
                strFieldValue = StrTool.GBKToUnicode(Operator);
                break;
            case 38:
                strFieldValue = StrTool.GBKToUnicode(ManageCom);
                break;
            case 39:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
                break;
            case 40:
                strFieldValue = StrTool.GBKToUnicode(MakeTime);
                break;
            case 41:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
                break;
            case 42:
                strFieldValue = StrTool.GBKToUnicode(ModifyTime);
                break;
            case 43:
                strFieldValue = String.valueOf(BMI);
                break;
            case 44:
                strFieldValue = StrTool.GBKToUnicode(License);
                break;
            case 45:
                strFieldValue = StrTool.GBKToUnicode(LicenseType);
                break;
            case 46:
                strFieldValue = StrTool.GBKToUnicode(RelatToInsu);
                break;
            case 47:
                strFieldValue = StrTool.GBKToUnicode(OccupationDesb);
                break;
            case 48:
                strFieldValue = StrTool.GBKToUnicode(IdValiDate);
                break;
            case 49:
                strFieldValue = StrTool.GBKToUnicode(HaveMotorcycleLicence);
                break;
            case 50:
                strFieldValue = StrTool.GBKToUnicode(PartTimeJob);
                break;
            case 51:
                strFieldValue = StrTool.GBKToUnicode(FirstName);
                break;
            case 52:
                strFieldValue = StrTool.GBKToUnicode(LastName);
                break;
            case 53:
                strFieldValue = StrTool.GBKToUnicode(CUSLevel);
                break;
            case 54:
                strFieldValue = StrTool.GBKToUnicode(AppRgtTpye);
                break;
            case 55:
                strFieldValue = StrTool.GBKToUnicode(TINNO);
                break;
            case 56:
                strFieldValue = StrTool.GBKToUnicode(TINFlag);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("AppntID")) {
            if( FValue != null && !FValue.equals("")) {
                AppntID = new Long(FValue).longValue();
            }
        }
        if (FCode.equalsIgnoreCase("ShardingID")) {
            if( FValue != null && !FValue.equals(""))
            {
                ShardingID = FValue.trim();
            }
            else
                ShardingID = null;
        }
        if (FCode.equalsIgnoreCase("EdorNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                EdorNo = FValue.trim();
            }
            else
                EdorNo = null;
        }
        if (FCode.equalsIgnoreCase("GrpContNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                GrpContNo = FValue.trim();
            }
            else
                GrpContNo = null;
        }
        if (FCode.equalsIgnoreCase("ContNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                ContNo = FValue.trim();
            }
            else
                ContNo = null;
        }
        if (FCode.equalsIgnoreCase("PrtNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                PrtNo = FValue.trim();
            }
            else
                PrtNo = null;
        }
        if (FCode.equalsIgnoreCase("AppntNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                AppntNo = FValue.trim();
            }
            else
                AppntNo = null;
        }
        if (FCode.equalsIgnoreCase("AppntGrade")) {
            if( FValue != null && !FValue.equals(""))
            {
                AppntGrade = FValue.trim();
            }
            else
                AppntGrade = null;
        }
        if (FCode.equalsIgnoreCase("AppntName")) {
            if( FValue != null && !FValue.equals(""))
            {
                AppntName = FValue.trim();
            }
            else
                AppntName = null;
        }
        if (FCode.equalsIgnoreCase("AppntSex")) {
            if( FValue != null && !FValue.equals(""))
            {
                AppntSex = FValue.trim();
            }
            else
                AppntSex = null;
        }
        if (FCode.equalsIgnoreCase("AppntBirthday")) {
            if(FValue != null && !FValue.equals("")) {
                AppntBirthday = fDate.getDate( FValue );
            }
            else
                AppntBirthday = null;
        }
        if (FCode.equalsIgnoreCase("AppntType")) {
            if( FValue != null && !FValue.equals(""))
            {
                AppntType = FValue.trim();
            }
            else
                AppntType = null;
        }
        if (FCode.equalsIgnoreCase("AddressNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                AddressNo = FValue.trim();
            }
            else
                AddressNo = null;
        }
        if (FCode.equalsIgnoreCase("IDType")) {
            if( FValue != null && !FValue.equals(""))
            {
                IDType = FValue.trim();
            }
            else
                IDType = null;
        }
        if (FCode.equalsIgnoreCase("IDNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                IDNo = FValue.trim();
            }
            else
                IDNo = null;
        }
        if (FCode.equalsIgnoreCase("NativePlace")) {
            if( FValue != null && !FValue.equals(""))
            {
                NativePlace = FValue.trim();
            }
            else
                NativePlace = null;
        }
        if (FCode.equalsIgnoreCase("Nationality")) {
            if( FValue != null && !FValue.equals(""))
            {
                Nationality = FValue.trim();
            }
            else
                Nationality = null;
        }
        if (FCode.equalsIgnoreCase("RgtAddress")) {
            if( FValue != null && !FValue.equals(""))
            {
                RgtAddress = FValue.trim();
            }
            else
                RgtAddress = null;
        }
        if (FCode.equalsIgnoreCase("Marriage")) {
            if( FValue != null && !FValue.equals(""))
            {
                Marriage = FValue.trim();
            }
            else
                Marriage = null;
        }
        if (FCode.equalsIgnoreCase("MarriageDate")) {
            if(FValue != null && !FValue.equals("")) {
                MarriageDate = fDate.getDate( FValue );
            }
            else
                MarriageDate = null;
        }
        if (FCode.equalsIgnoreCase("Health")) {
            if( FValue != null && !FValue.equals(""))
            {
                Health = FValue.trim();
            }
            else
                Health = null;
        }
        if (FCode.equalsIgnoreCase("Stature")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                Stature = d;
            }
        }
        if (FCode.equalsIgnoreCase("Avoirdupois")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                Avoirdupois = d;
            }
        }
        if (FCode.equalsIgnoreCase("Degree")) {
            if( FValue != null && !FValue.equals(""))
            {
                Degree = FValue.trim();
            }
            else
                Degree = null;
        }
        if (FCode.equalsIgnoreCase("CreditGrade")) {
            if( FValue != null && !FValue.equals(""))
            {
                CreditGrade = FValue.trim();
            }
            else
                CreditGrade = null;
        }
        if (FCode.equalsIgnoreCase("BankCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                BankCode = FValue.trim();
            }
            else
                BankCode = null;
        }
        if (FCode.equalsIgnoreCase("BankAccNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                BankAccNo = FValue.trim();
            }
            else
                BankAccNo = null;
        }
        if (FCode.equalsIgnoreCase("AccName")) {
            if( FValue != null && !FValue.equals(""))
            {
                AccName = FValue.trim();
            }
            else
                AccName = null;
        }
        if (FCode.equalsIgnoreCase("JoinCompanyDate")) {
            if(FValue != null && !FValue.equals("")) {
                JoinCompanyDate = fDate.getDate( FValue );
            }
            else
                JoinCompanyDate = null;
        }
        if (FCode.equalsIgnoreCase("StartWorkDate")) {
            if(FValue != null && !FValue.equals("")) {
                StartWorkDate = fDate.getDate( FValue );
            }
            else
                StartWorkDate = null;
        }
        if (FCode.equalsIgnoreCase("Position")) {
            if( FValue != null && !FValue.equals(""))
            {
                Position = FValue.trim();
            }
            else
                Position = null;
        }
        if (FCode.equalsIgnoreCase("Salary")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                Salary = d;
            }
        }
        if (FCode.equalsIgnoreCase("OccupationType")) {
            if( FValue != null && !FValue.equals(""))
            {
                OccupationType = FValue.trim();
            }
            else
                OccupationType = null;
        }
        if (FCode.equalsIgnoreCase("OccupationCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                OccupationCode = FValue.trim();
            }
            else
                OccupationCode = null;
        }
        if (FCode.equalsIgnoreCase("WorkType")) {
            if( FValue != null && !FValue.equals(""))
            {
                WorkType = FValue.trim();
            }
            else
                WorkType = null;
        }
        if (FCode.equalsIgnoreCase("PluralityType")) {
            if( FValue != null && !FValue.equals(""))
            {
                PluralityType = FValue.trim();
            }
            else
                PluralityType = null;
        }
        if (FCode.equalsIgnoreCase("SmokeFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                SmokeFlag = FValue.trim();
            }
            else
                SmokeFlag = null;
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            if( FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
                Operator = null;
        }
        if (FCode.equalsIgnoreCase("ManageCom")) {
            if( FValue != null && !FValue.equals(""))
            {
                ManageCom = FValue.trim();
            }
            else
                ManageCom = null;
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if(FValue != null && !FValue.equals("")) {
                MakeDate = fDate.getDate( FValue );
            }
            else
                MakeDate = null;
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
                MakeTime = null;
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if(FValue != null && !FValue.equals("")) {
                ModifyDate = fDate.getDate( FValue );
            }
            else
                ModifyDate = null;
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
                ModifyTime = null;
        }
        if (FCode.equalsIgnoreCase("BMI")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                BMI = d;
            }
        }
        if (FCode.equalsIgnoreCase("License")) {
            if( FValue != null && !FValue.equals(""))
            {
                License = FValue.trim();
            }
            else
                License = null;
        }
        if (FCode.equalsIgnoreCase("LicenseType")) {
            if( FValue != null && !FValue.equals(""))
            {
                LicenseType = FValue.trim();
            }
            else
                LicenseType = null;
        }
        if (FCode.equalsIgnoreCase("RelatToInsu")) {
            if( FValue != null && !FValue.equals(""))
            {
                RelatToInsu = FValue.trim();
            }
            else
                RelatToInsu = null;
        }
        if (FCode.equalsIgnoreCase("OccupationDesb")) {
            if( FValue != null && !FValue.equals(""))
            {
                OccupationDesb = FValue.trim();
            }
            else
                OccupationDesb = null;
        }
        if (FCode.equalsIgnoreCase("IdValiDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                IdValiDate = FValue.trim();
            }
            else
                IdValiDate = null;
        }
        if (FCode.equalsIgnoreCase("HaveMotorcycleLicence")) {
            if( FValue != null && !FValue.equals(""))
            {
                HaveMotorcycleLicence = FValue.trim();
            }
            else
                HaveMotorcycleLicence = null;
        }
        if (FCode.equalsIgnoreCase("PartTimeJob")) {
            if( FValue != null && !FValue.equals(""))
            {
                PartTimeJob = FValue.trim();
            }
            else
                PartTimeJob = null;
        }
        if (FCode.equalsIgnoreCase("FirstName")) {
            if( FValue != null && !FValue.equals(""))
            {
                FirstName = FValue.trim();
            }
            else
                FirstName = null;
        }
        if (FCode.equalsIgnoreCase("LastName")) {
            if( FValue != null && !FValue.equals(""))
            {
                LastName = FValue.trim();
            }
            else
                LastName = null;
        }
        if (FCode.equalsIgnoreCase("CUSLevel")) {
            if( FValue != null && !FValue.equals(""))
            {
                CUSLevel = FValue.trim();
            }
            else
                CUSLevel = null;
        }
        if (FCode.equalsIgnoreCase("AppRgtTpye")) {
            if( FValue != null && !FValue.equals(""))
            {
                AppRgtTpye = FValue.trim();
            }
            else
                AppRgtTpye = null;
        }
        if (FCode.equalsIgnoreCase("TINNO")) {
            if( FValue != null && !FValue.equals(""))
            {
                TINNO = FValue.trim();
            }
            else
                TINNO = null;
        }
        if (FCode.equalsIgnoreCase("TINFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                TINFlag = FValue.trim();
            }
            else
                TINFlag = null;
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject) return true;
        if (otherObject == null) return false;
        if (getClass() != otherObject.getClass()) return false;
        LBAppntSchema other = (LBAppntSchema)otherObject;
        return
            AppntID == other.getAppntID()
            && ShardingID.equals(other.getShardingID())
            && EdorNo.equals(other.getEdorNo())
            && GrpContNo.equals(other.getGrpContNo())
            && ContNo.equals(other.getContNo())
            && PrtNo.equals(other.getPrtNo())
            && AppntNo.equals(other.getAppntNo())
            && AppntGrade.equals(other.getAppntGrade())
            && AppntName.equals(other.getAppntName())
            && AppntSex.equals(other.getAppntSex())
            && fDate.getString(AppntBirthday).equals(other.getAppntBirthday())
            && AppntType.equals(other.getAppntType())
            && AddressNo.equals(other.getAddressNo())
            && IDType.equals(other.getIDType())
            && IDNo.equals(other.getIDNo())
            && NativePlace.equals(other.getNativePlace())
            && Nationality.equals(other.getNationality())
            && RgtAddress.equals(other.getRgtAddress())
            && Marriage.equals(other.getMarriage())
            && fDate.getString(MarriageDate).equals(other.getMarriageDate())
            && Health.equals(other.getHealth())
            && Stature == other.getStature()
            && Avoirdupois == other.getAvoirdupois()
            && Degree.equals(other.getDegree())
            && CreditGrade.equals(other.getCreditGrade())
            && BankCode.equals(other.getBankCode())
            && BankAccNo.equals(other.getBankAccNo())
            && AccName.equals(other.getAccName())
            && fDate.getString(JoinCompanyDate).equals(other.getJoinCompanyDate())
            && fDate.getString(StartWorkDate).equals(other.getStartWorkDate())
            && Position.equals(other.getPosition())
            && Salary == other.getSalary()
            && OccupationType.equals(other.getOccupationType())
            && OccupationCode.equals(other.getOccupationCode())
            && WorkType.equals(other.getWorkType())
            && PluralityType.equals(other.getPluralityType())
            && SmokeFlag.equals(other.getSmokeFlag())
            && Operator.equals(other.getOperator())
            && ManageCom.equals(other.getManageCom())
            && fDate.getString(MakeDate).equals(other.getMakeDate())
            && MakeTime.equals(other.getMakeTime())
            && fDate.getString(ModifyDate).equals(other.getModifyDate())
            && ModifyTime.equals(other.getModifyTime())
            && BMI == other.getBMI()
            && License.equals(other.getLicense())
            && LicenseType.equals(other.getLicenseType())
            && RelatToInsu.equals(other.getRelatToInsu())
            && OccupationDesb.equals(other.getOccupationDesb())
            && IdValiDate.equals(other.getIdValiDate())
            && HaveMotorcycleLicence.equals(other.getHaveMotorcycleLicence())
            && PartTimeJob.equals(other.getPartTimeJob())
            && FirstName.equals(other.getFirstName())
            && LastName.equals(other.getLastName())
            && CUSLevel.equals(other.getCUSLevel())
            && AppRgtTpye.equals(other.getAppRgtTpye())
            && TINNO.equals(other.getTINNO())
            && TINFlag.equals(other.getTINFlag());
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("AppntID") ) {
            return 0;
        }
        if( strFieldName.equals("ShardingID") ) {
            return 1;
        }
        if( strFieldName.equals("EdorNo") ) {
            return 2;
        }
        if( strFieldName.equals("GrpContNo") ) {
            return 3;
        }
        if( strFieldName.equals("ContNo") ) {
            return 4;
        }
        if( strFieldName.equals("PrtNo") ) {
            return 5;
        }
        if( strFieldName.equals("AppntNo") ) {
            return 6;
        }
        if( strFieldName.equals("AppntGrade") ) {
            return 7;
        }
        if( strFieldName.equals("AppntName") ) {
            return 8;
        }
        if( strFieldName.equals("AppntSex") ) {
            return 9;
        }
        if( strFieldName.equals("AppntBirthday") ) {
            return 10;
        }
        if( strFieldName.equals("AppntType") ) {
            return 11;
        }
        if( strFieldName.equals("AddressNo") ) {
            return 12;
        }
        if( strFieldName.equals("IDType") ) {
            return 13;
        }
        if( strFieldName.equals("IDNo") ) {
            return 14;
        }
        if( strFieldName.equals("NativePlace") ) {
            return 15;
        }
        if( strFieldName.equals("Nationality") ) {
            return 16;
        }
        if( strFieldName.equals("RgtAddress") ) {
            return 17;
        }
        if( strFieldName.equals("Marriage") ) {
            return 18;
        }
        if( strFieldName.equals("MarriageDate") ) {
            return 19;
        }
        if( strFieldName.equals("Health") ) {
            return 20;
        }
        if( strFieldName.equals("Stature") ) {
            return 21;
        }
        if( strFieldName.equals("Avoirdupois") ) {
            return 22;
        }
        if( strFieldName.equals("Degree") ) {
            return 23;
        }
        if( strFieldName.equals("CreditGrade") ) {
            return 24;
        }
        if( strFieldName.equals("BankCode") ) {
            return 25;
        }
        if( strFieldName.equals("BankAccNo") ) {
            return 26;
        }
        if( strFieldName.equals("AccName") ) {
            return 27;
        }
        if( strFieldName.equals("JoinCompanyDate") ) {
            return 28;
        }
        if( strFieldName.equals("StartWorkDate") ) {
            return 29;
        }
        if( strFieldName.equals("Position") ) {
            return 30;
        }
        if( strFieldName.equals("Salary") ) {
            return 31;
        }
        if( strFieldName.equals("OccupationType") ) {
            return 32;
        }
        if( strFieldName.equals("OccupationCode") ) {
            return 33;
        }
        if( strFieldName.equals("WorkType") ) {
            return 34;
        }
        if( strFieldName.equals("PluralityType") ) {
            return 35;
        }
        if( strFieldName.equals("SmokeFlag") ) {
            return 36;
        }
        if( strFieldName.equals("Operator") ) {
            return 37;
        }
        if( strFieldName.equals("ManageCom") ) {
            return 38;
        }
        if( strFieldName.equals("MakeDate") ) {
            return 39;
        }
        if( strFieldName.equals("MakeTime") ) {
            return 40;
        }
        if( strFieldName.equals("ModifyDate") ) {
            return 41;
        }
        if( strFieldName.equals("ModifyTime") ) {
            return 42;
        }
        if( strFieldName.equals("BMI") ) {
            return 43;
        }
        if( strFieldName.equals("License") ) {
            return 44;
        }
        if( strFieldName.equals("LicenseType") ) {
            return 45;
        }
        if( strFieldName.equals("RelatToInsu") ) {
            return 46;
        }
        if( strFieldName.equals("OccupationDesb") ) {
            return 47;
        }
        if( strFieldName.equals("IdValiDate") ) {
            return 48;
        }
        if( strFieldName.equals("HaveMotorcycleLicence") ) {
            return 49;
        }
        if( strFieldName.equals("PartTimeJob") ) {
            return 50;
        }
        if( strFieldName.equals("FirstName") ) {
            return 51;
        }
        if( strFieldName.equals("LastName") ) {
            return 52;
        }
        if( strFieldName.equals("CUSLevel") ) {
            return 53;
        }
        if( strFieldName.equals("AppRgtTpye") ) {
            return 54;
        }
        if( strFieldName.equals("TINNO") ) {
            return 55;
        }
        if( strFieldName.equals("TINFlag") ) {
            return 56;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "AppntID";
                break;
            case 1:
                strFieldName = "ShardingID";
                break;
            case 2:
                strFieldName = "EdorNo";
                break;
            case 3:
                strFieldName = "GrpContNo";
                break;
            case 4:
                strFieldName = "ContNo";
                break;
            case 5:
                strFieldName = "PrtNo";
                break;
            case 6:
                strFieldName = "AppntNo";
                break;
            case 7:
                strFieldName = "AppntGrade";
                break;
            case 8:
                strFieldName = "AppntName";
                break;
            case 9:
                strFieldName = "AppntSex";
                break;
            case 10:
                strFieldName = "AppntBirthday";
                break;
            case 11:
                strFieldName = "AppntType";
                break;
            case 12:
                strFieldName = "AddressNo";
                break;
            case 13:
                strFieldName = "IDType";
                break;
            case 14:
                strFieldName = "IDNo";
                break;
            case 15:
                strFieldName = "NativePlace";
                break;
            case 16:
                strFieldName = "Nationality";
                break;
            case 17:
                strFieldName = "RgtAddress";
                break;
            case 18:
                strFieldName = "Marriage";
                break;
            case 19:
                strFieldName = "MarriageDate";
                break;
            case 20:
                strFieldName = "Health";
                break;
            case 21:
                strFieldName = "Stature";
                break;
            case 22:
                strFieldName = "Avoirdupois";
                break;
            case 23:
                strFieldName = "Degree";
                break;
            case 24:
                strFieldName = "CreditGrade";
                break;
            case 25:
                strFieldName = "BankCode";
                break;
            case 26:
                strFieldName = "BankAccNo";
                break;
            case 27:
                strFieldName = "AccName";
                break;
            case 28:
                strFieldName = "JoinCompanyDate";
                break;
            case 29:
                strFieldName = "StartWorkDate";
                break;
            case 30:
                strFieldName = "Position";
                break;
            case 31:
                strFieldName = "Salary";
                break;
            case 32:
                strFieldName = "OccupationType";
                break;
            case 33:
                strFieldName = "OccupationCode";
                break;
            case 34:
                strFieldName = "WorkType";
                break;
            case 35:
                strFieldName = "PluralityType";
                break;
            case 36:
                strFieldName = "SmokeFlag";
                break;
            case 37:
                strFieldName = "Operator";
                break;
            case 38:
                strFieldName = "ManageCom";
                break;
            case 39:
                strFieldName = "MakeDate";
                break;
            case 40:
                strFieldName = "MakeTime";
                break;
            case 41:
                strFieldName = "ModifyDate";
                break;
            case 42:
                strFieldName = "ModifyTime";
                break;
            case 43:
                strFieldName = "BMI";
                break;
            case 44:
                strFieldName = "License";
                break;
            case 45:
                strFieldName = "LicenseType";
                break;
            case 46:
                strFieldName = "RelatToInsu";
                break;
            case 47:
                strFieldName = "OccupationDesb";
                break;
            case 48:
                strFieldName = "IdValiDate";
                break;
            case 49:
                strFieldName = "HaveMotorcycleLicence";
                break;
            case 50:
                strFieldName = "PartTimeJob";
                break;
            case 51:
                strFieldName = "FirstName";
                break;
            case 52:
                strFieldName = "LastName";
                break;
            case 53:
                strFieldName = "CUSLevel";
                break;
            case 54:
                strFieldName = "AppRgtTpye";
                break;
            case 55:
                strFieldName = "TINNO";
                break;
            case 56:
                strFieldName = "TINFlag";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "APPNTID":
                return Schema.TYPE_LONG;
            case "SHARDINGID":
                return Schema.TYPE_STRING;
            case "EDORNO":
                return Schema.TYPE_STRING;
            case "GRPCONTNO":
                return Schema.TYPE_STRING;
            case "CONTNO":
                return Schema.TYPE_STRING;
            case "PRTNO":
                return Schema.TYPE_STRING;
            case "APPNTNO":
                return Schema.TYPE_STRING;
            case "APPNTGRADE":
                return Schema.TYPE_STRING;
            case "APPNTNAME":
                return Schema.TYPE_STRING;
            case "APPNTSEX":
                return Schema.TYPE_STRING;
            case "APPNTBIRTHDAY":
                return Schema.TYPE_DATE;
            case "APPNTTYPE":
                return Schema.TYPE_STRING;
            case "ADDRESSNO":
                return Schema.TYPE_STRING;
            case "IDTYPE":
                return Schema.TYPE_STRING;
            case "IDNO":
                return Schema.TYPE_STRING;
            case "NATIVEPLACE":
                return Schema.TYPE_STRING;
            case "NATIONALITY":
                return Schema.TYPE_STRING;
            case "RGTADDRESS":
                return Schema.TYPE_STRING;
            case "MARRIAGE":
                return Schema.TYPE_STRING;
            case "MARRIAGEDATE":
                return Schema.TYPE_DATE;
            case "HEALTH":
                return Schema.TYPE_STRING;
            case "STATURE":
                return Schema.TYPE_DOUBLE;
            case "AVOIRDUPOIS":
                return Schema.TYPE_DOUBLE;
            case "DEGREE":
                return Schema.TYPE_STRING;
            case "CREDITGRADE":
                return Schema.TYPE_STRING;
            case "BANKCODE":
                return Schema.TYPE_STRING;
            case "BANKACCNO":
                return Schema.TYPE_STRING;
            case "ACCNAME":
                return Schema.TYPE_STRING;
            case "JOINCOMPANYDATE":
                return Schema.TYPE_DATE;
            case "STARTWORKDATE":
                return Schema.TYPE_DATE;
            case "POSITION":
                return Schema.TYPE_STRING;
            case "SALARY":
                return Schema.TYPE_DOUBLE;
            case "OCCUPATIONTYPE":
                return Schema.TYPE_STRING;
            case "OCCUPATIONCODE":
                return Schema.TYPE_STRING;
            case "WORKTYPE":
                return Schema.TYPE_STRING;
            case "PLURALITYTYPE":
                return Schema.TYPE_STRING;
            case "SMOKEFLAG":
                return Schema.TYPE_STRING;
            case "OPERATOR":
                return Schema.TYPE_STRING;
            case "MANAGECOM":
                return Schema.TYPE_STRING;
            case "MAKEDATE":
                return Schema.TYPE_DATE;
            case "MAKETIME":
                return Schema.TYPE_STRING;
            case "MODIFYDATE":
                return Schema.TYPE_DATE;
            case "MODIFYTIME":
                return Schema.TYPE_STRING;
            case "BMI":
                return Schema.TYPE_DOUBLE;
            case "LICENSE":
                return Schema.TYPE_STRING;
            case "LICENSETYPE":
                return Schema.TYPE_STRING;
            case "RELATTOINSU":
                return Schema.TYPE_STRING;
            case "OCCUPATIONDESB":
                return Schema.TYPE_STRING;
            case "IDVALIDATE":
                return Schema.TYPE_STRING;
            case "HAVEMOTORCYCLELICENCE":
                return Schema.TYPE_STRING;
            case "PARTTIMEJOB":
                return Schema.TYPE_STRING;
            case "FIRSTNAME":
                return Schema.TYPE_STRING;
            case "LASTNAME":
                return Schema.TYPE_STRING;
            case "CUSLEVEL":
                return Schema.TYPE_STRING;
            case "APPRGTTPYE":
                return Schema.TYPE_STRING;
            case "TINNO":
                return Schema.TYPE_STRING;
            case "TINFLAG":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_LONG;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_STRING;
            case 6:
                return Schema.TYPE_STRING;
            case 7:
                return Schema.TYPE_STRING;
            case 8:
                return Schema.TYPE_STRING;
            case 9:
                return Schema.TYPE_STRING;
            case 10:
                return Schema.TYPE_DATE;
            case 11:
                return Schema.TYPE_STRING;
            case 12:
                return Schema.TYPE_STRING;
            case 13:
                return Schema.TYPE_STRING;
            case 14:
                return Schema.TYPE_STRING;
            case 15:
                return Schema.TYPE_STRING;
            case 16:
                return Schema.TYPE_STRING;
            case 17:
                return Schema.TYPE_STRING;
            case 18:
                return Schema.TYPE_STRING;
            case 19:
                return Schema.TYPE_DATE;
            case 20:
                return Schema.TYPE_STRING;
            case 21:
                return Schema.TYPE_DOUBLE;
            case 22:
                return Schema.TYPE_DOUBLE;
            case 23:
                return Schema.TYPE_STRING;
            case 24:
                return Schema.TYPE_STRING;
            case 25:
                return Schema.TYPE_STRING;
            case 26:
                return Schema.TYPE_STRING;
            case 27:
                return Schema.TYPE_STRING;
            case 28:
                return Schema.TYPE_DATE;
            case 29:
                return Schema.TYPE_DATE;
            case 30:
                return Schema.TYPE_STRING;
            case 31:
                return Schema.TYPE_DOUBLE;
            case 32:
                return Schema.TYPE_STRING;
            case 33:
                return Schema.TYPE_STRING;
            case 34:
                return Schema.TYPE_STRING;
            case 35:
                return Schema.TYPE_STRING;
            case 36:
                return Schema.TYPE_STRING;
            case 37:
                return Schema.TYPE_STRING;
            case 38:
                return Schema.TYPE_STRING;
            case 39:
                return Schema.TYPE_DATE;
            case 40:
                return Schema.TYPE_STRING;
            case 41:
                return Schema.TYPE_DATE;
            case 42:
                return Schema.TYPE_STRING;
            case 43:
                return Schema.TYPE_DOUBLE;
            case 44:
                return Schema.TYPE_STRING;
            case 45:
                return Schema.TYPE_STRING;
            case 46:
                return Schema.TYPE_STRING;
            case 47:
                return Schema.TYPE_STRING;
            case 48:
                return Schema.TYPE_STRING;
            case 49:
                return Schema.TYPE_STRING;
            case 50:
                return Schema.TYPE_STRING;
            case 51:
                return Schema.TYPE_STRING;
            case 52:
                return Schema.TYPE_STRING;
            case 53:
                return Schema.TYPE_STRING;
            case 54:
                return Schema.TYPE_STRING;
            case 55:
                return Schema.TYPE_STRING;
            case 56:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
}
