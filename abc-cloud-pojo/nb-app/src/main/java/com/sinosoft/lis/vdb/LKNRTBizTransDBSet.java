/**
 * Copyright (c) 2018 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.vdb;

import com.sinosoft.lis.vschema.LKNRTBizTransSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.DBConnPool;
import com.sinosoft.utility.DBOper;
import com.sinosoft.utility.ExceptionUtils;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;

/**
 * <p>ClassName: LKNRTBizTransDBSet </p>
 * <p>Description: DB层多记录数据库操作类文件 </p>
 * <p>Copyright: Copyright (c) 2018</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2018-03-07
 */
public class LKNRTBizTransDBSet extends LKNRTBizTransSet {
    // @Field
    private Connection con;
    private DBOper db;
    /**
    * flag = true: 传入Connection
    * flag = false: 不传入Connection
    **/
    private boolean mflag = false;

    // @Constructor
    public LKNRTBizTransDBSet(Connection tConnection) {
        con = tConnection;
        db = new DBOper(con,"LKNRTBizTrans");
        mflag = true;
    }

    public LKNRTBizTransDBSet() {
        db = new DBOper( "LKNRTBizTrans" );
    }
    // @Method
    public boolean deleteSQL() {
        if (db.deleteSQL(this)) {
            return true;
        } else {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LKNRTBizTransDBSet";
            tError.functionName = "deleteSQL";
            tError.errorMessage = "操作失败!";
            this.mErrors .addOneError(tError);
            return false;
        }
    }

    /**
     * 删除操作
     * 删除条件：主键
     * @return boolean
     */
    public boolean delete() {
        PreparedStatement pstmt = null;

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try {
            int tCount = this.size();
            pstmt = con.prepareStatement("DELETE FROM LKNRTBizTrans WHERE  1=1  AND LKNRTBizID = ? AND TransCode = ? AND BankCode = ?");
            for (int i = 1; i <= tCount; i++) {
            pstmt.setLong(1, this.get(i).getLKNRTBizID());
            if(this.get(i).getTransCode() == null || this.get(i).getTransCode().equals("null")) {
            	pstmt.setString(2,null);
            } else {
            	pstmt.setString(2, this.get(i).getTransCode());
            }
            if(this.get(i).getBankCode() == null || this.get(i).getBankCode().equals("null")) {
            	pstmt.setString(3,null);
            } else {
            	pstmt.setString(3, this.get(i).getBankCode());
            }
                pstmt.addBatch();
            }
            pstmt.executeBatch();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            ex.printStackTrace();
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LKNRTBizTransDBSet";
            tError.functionName = "delete()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            try {
                pstmt.close();
            } catch (Exception e){
                e.printStackTrace();
            }

            if (!mflag) {
                try {
                    con.close();
                } catch (Exception e){e.printStackTrace();}
            }

            return false;
        }

        if( !mflag ) {
            try {
                con.close();
            } catch (Exception e){e.printStackTrace();}
        }

        return true;
    }

    /**
     * 更新操作
     * 更新条件：主键
     * @return boolean
     */
    public boolean update() {
        PreparedStatement pstmt = null;

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try {
            int tCount = this.size();
            pstmt = con.prepareStatement("UPDATE LKNRTBizTrans SET  LKNRTBizID = ? , ShardingID = ? , TransCode = ? , BankCode = ? , TranDate = ? , ZoneNo = ? , BankNode = ? , TellerNo = ? , TransNo = ? , ProposalNo = ? , BanksaleChnl = ? , UwType = ? , AppntName = ? , AppntidType = ? , AppntidNo = ? , AppntaccNo = ? , BizStatus = ? , UwStatus = ? , RespDate = ? , RespTime = ? , MakeDate = ? , MakeTime = ? , ModifyDate = ? , ModifyTime = ? , Bak1 = ? , Bak2 = ? WHERE  1=1  AND LKNRTBizID = ? AND TransCode = ? AND BankCode = ?");
            for (int i = 1; i <= tCount; i++) {
            pstmt.setLong(1, this.get(i).getLKNRTBizID());
            if(this.get(i).getShardingID() == null || this.get(i).getShardingID().equals("null")) {
            	pstmt.setString(2,null);
            } else {
            	pstmt.setString(2, this.get(i).getShardingID());
            }
            if(this.get(i).getTransCode() == null || this.get(i).getTransCode().equals("null")) {
            	pstmt.setString(3,null);
            } else {
            	pstmt.setString(3, this.get(i).getTransCode());
            }
            if(this.get(i).getBankCode() == null || this.get(i).getBankCode().equals("null")) {
            	pstmt.setString(4,null);
            } else {
            	pstmt.setString(4, this.get(i).getBankCode());
            }
            if(this.get(i).getTranDate() == null || this.get(i).getTranDate().equals("null")) {
                pstmt.setDate(5,null);
            } else {
                pstmt.setDate(5, Date.valueOf(this.get(i).getTranDate()));
            }
            if(this.get(i).getZoneNo() == null || this.get(i).getZoneNo().equals("null")) {
            	pstmt.setString(6,null);
            } else {
            	pstmt.setString(6, this.get(i).getZoneNo());
            }
            if(this.get(i).getBankNode() == null || this.get(i).getBankNode().equals("null")) {
            	pstmt.setString(7,null);
            } else {
            	pstmt.setString(7, this.get(i).getBankNode());
            }
            if(this.get(i).getTellerNo() == null || this.get(i).getTellerNo().equals("null")) {
            	pstmt.setString(8,null);
            } else {
            	pstmt.setString(8, this.get(i).getTellerNo());
            }
            if(this.get(i).getTransNo() == null || this.get(i).getTransNo().equals("null")) {
            	pstmt.setString(9,null);
            } else {
            	pstmt.setString(9, this.get(i).getTransNo());
            }
            if(this.get(i).getProposalNo() == null || this.get(i).getProposalNo().equals("null")) {
            	pstmt.setString(10,null);
            } else {
            	pstmt.setString(10, this.get(i).getProposalNo());
            }
            if(this.get(i).getBanksaleChnl() == null || this.get(i).getBanksaleChnl().equals("null")) {
            	pstmt.setString(11,null);
            } else {
            	pstmt.setString(11, this.get(i).getBanksaleChnl());
            }
            if(this.get(i).getUwType() == null || this.get(i).getUwType().equals("null")) {
            	pstmt.setString(12,null);
            } else {
            	pstmt.setString(12, this.get(i).getUwType());
            }
            if(this.get(i).getAppntName() == null || this.get(i).getAppntName().equals("null")) {
            	pstmt.setString(13,null);
            } else {
            	pstmt.setString(13, this.get(i).getAppntName());
            }
            if(this.get(i).getAppntidType() == null || this.get(i).getAppntidType().equals("null")) {
            	pstmt.setString(14,null);
            } else {
            	pstmt.setString(14, this.get(i).getAppntidType());
            }
            if(this.get(i).getAppntidNo() == null || this.get(i).getAppntidNo().equals("null")) {
            	pstmt.setString(15,null);
            } else {
            	pstmt.setString(15, this.get(i).getAppntidNo());
            }
            if(this.get(i).getAppntaccNo() == null || this.get(i).getAppntaccNo().equals("null")) {
            	pstmt.setString(16,null);
            } else {
            	pstmt.setString(16, this.get(i).getAppntaccNo());
            }
            if(this.get(i).getBizStatus() == null || this.get(i).getBizStatus().equals("null")) {
            	pstmt.setString(17,null);
            } else {
            	pstmt.setString(17, this.get(i).getBizStatus());
            }
            if(this.get(i).getUwStatus() == null || this.get(i).getUwStatus().equals("null")) {
            	pstmt.setString(18,null);
            } else {
            	pstmt.setString(18, this.get(i).getUwStatus());
            }
            if(this.get(i).getRespDate() == null || this.get(i).getRespDate().equals("null")) {
                pstmt.setDate(19,null);
            } else {
                pstmt.setDate(19, Date.valueOf(this.get(i).getRespDate()));
            }
            if(this.get(i).getRespTime() == null || this.get(i).getRespTime().equals("null")) {
            	pstmt.setString(20,null);
            } else {
            	pstmt.setString(20, this.get(i).getRespTime());
            }
            if(this.get(i).getMakeDate() == null || this.get(i).getMakeDate().equals("null")) {
                pstmt.setDate(21,null);
            } else {
                pstmt.setDate(21, Date.valueOf(this.get(i).getMakeDate()));
            }
            if(this.get(i).getMakeTime() == null || this.get(i).getMakeTime().equals("null")) {
            	pstmt.setString(22,null);
            } else {
            	pstmt.setString(22, this.get(i).getMakeTime());
            }
            if(this.get(i).getModifyDate() == null || this.get(i).getModifyDate().equals("null")) {
                pstmt.setDate(23,null);
            } else {
                pstmt.setDate(23, Date.valueOf(this.get(i).getModifyDate()));
            }
            if(this.get(i).getModifyTime() == null || this.get(i).getModifyTime().equals("null")) {
            	pstmt.setString(24,null);
            } else {
            	pstmt.setString(24, this.get(i).getModifyTime());
            }
            if(this.get(i).getBak1() == null || this.get(i).getBak1().equals("null")) {
            	pstmt.setString(25,null);
            } else {
            	pstmt.setString(25, this.get(i).getBak1());
            }
            if(this.get(i).getBak2() == null || this.get(i).getBak2().equals("null")) {
            	pstmt.setString(26,null);
            } else {
            	pstmt.setString(26, this.get(i).getBak2());
            }
            // set where condition
            pstmt.setLong(27, this.get(i).getLKNRTBizID());
            if(this.get(i).getTransCode() == null || this.get(i).getTransCode().equals("null")) {
            	pstmt.setString(28,null);
            } else {
            	pstmt.setString(28, this.get(i).getTransCode());
            }
            if(this.get(i).getBankCode() == null || this.get(i).getBankCode().equals("null")) {
            	pstmt.setString(29,null);
            } else {
            	pstmt.setString(29, this.get(i).getBankCode());
            }
                pstmt.addBatch();
            }
            pstmt.executeBatch();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            ex.printStackTrace();
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LKNRTBizTransDBSet";
            tError.functionName = "update()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            try {
                pstmt.close();
            } catch (Exception e){e.printStackTrace();}

            if( !mflag ) {
                try {
                    con.close();
                } catch (Exception e){e.printStackTrace();}
            }

            return false;
        }

        if( !mflag ) {
            try {
                con.close();
            } catch (Exception e){e.printStackTrace();}
        }

        return true;
    }

    /**
     * 新增操作
     * @return boolean
     */
    public boolean insert() {
        PreparedStatement pstmt = null;

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try
        {
            int tCount = this.size();
            pstmt = con.prepareStatement("INSERT INTO LKNRTBizTrans VALUES( ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ?)");
            for (int i = 1; i <= tCount; i++) {
            pstmt.setLong(1, this.get(i).getLKNRTBizID());
            if(this.get(i).getShardingID() == null || this.get(i).getShardingID().equals("null")) {
            	pstmt.setString(2,null);
            } else {
            	pstmt.setString(2, this.get(i).getShardingID());
            }
            if(this.get(i).getTransCode() == null || this.get(i).getTransCode().equals("null")) {
            	pstmt.setString(3,null);
            } else {
            	pstmt.setString(3, this.get(i).getTransCode());
            }
            if(this.get(i).getBankCode() == null || this.get(i).getBankCode().equals("null")) {
            	pstmt.setString(4,null);
            } else {
            	pstmt.setString(4, this.get(i).getBankCode());
            }
            if(this.get(i).getTranDate() == null || this.get(i).getTranDate().equals("null")) {
                pstmt.setDate(5,null);
            } else {
                pstmt.setDate(5, Date.valueOf(this.get(i).getTranDate()));
            }
            if(this.get(i).getZoneNo() == null || this.get(i).getZoneNo().equals("null")) {
            	pstmt.setString(6,null);
            } else {
            	pstmt.setString(6, this.get(i).getZoneNo());
            }
            if(this.get(i).getBankNode() == null || this.get(i).getBankNode().equals("null")) {
            	pstmt.setString(7,null);
            } else {
            	pstmt.setString(7, this.get(i).getBankNode());
            }
            if(this.get(i).getTellerNo() == null || this.get(i).getTellerNo().equals("null")) {
            	pstmt.setString(8,null);
            } else {
            	pstmt.setString(8, this.get(i).getTellerNo());
            }
            if(this.get(i).getTransNo() == null || this.get(i).getTransNo().equals("null")) {
            	pstmt.setString(9,null);
            } else {
            	pstmt.setString(9, this.get(i).getTransNo());
            }
            if(this.get(i).getProposalNo() == null || this.get(i).getProposalNo().equals("null")) {
            	pstmt.setString(10,null);
            } else {
            	pstmt.setString(10, this.get(i).getProposalNo());
            }
            if(this.get(i).getBanksaleChnl() == null || this.get(i).getBanksaleChnl().equals("null")) {
            	pstmt.setString(11,null);
            } else {
            	pstmt.setString(11, this.get(i).getBanksaleChnl());
            }
            if(this.get(i).getUwType() == null || this.get(i).getUwType().equals("null")) {
            	pstmt.setString(12,null);
            } else {
            	pstmt.setString(12, this.get(i).getUwType());
            }
            if(this.get(i).getAppntName() == null || this.get(i).getAppntName().equals("null")) {
            	pstmt.setString(13,null);
            } else {
            	pstmt.setString(13, this.get(i).getAppntName());
            }
            if(this.get(i).getAppntidType() == null || this.get(i).getAppntidType().equals("null")) {
            	pstmt.setString(14,null);
            } else {
            	pstmt.setString(14, this.get(i).getAppntidType());
            }
            if(this.get(i).getAppntidNo() == null || this.get(i).getAppntidNo().equals("null")) {
            	pstmt.setString(15,null);
            } else {
            	pstmt.setString(15, this.get(i).getAppntidNo());
            }
            if(this.get(i).getAppntaccNo() == null || this.get(i).getAppntaccNo().equals("null")) {
            	pstmt.setString(16,null);
            } else {
            	pstmt.setString(16, this.get(i).getAppntaccNo());
            }
            if(this.get(i).getBizStatus() == null || this.get(i).getBizStatus().equals("null")) {
            	pstmt.setString(17,null);
            } else {
            	pstmt.setString(17, this.get(i).getBizStatus());
            }
            if(this.get(i).getUwStatus() == null || this.get(i).getUwStatus().equals("null")) {
            	pstmt.setString(18,null);
            } else {
            	pstmt.setString(18, this.get(i).getUwStatus());
            }
            if(this.get(i).getRespDate() == null || this.get(i).getRespDate().equals("null")) {
                pstmt.setDate(19,null);
            } else {
                pstmt.setDate(19, Date.valueOf(this.get(i).getRespDate()));
            }
            if(this.get(i).getRespTime() == null || this.get(i).getRespTime().equals("null")) {
            	pstmt.setString(20,null);
            } else {
            	pstmt.setString(20, this.get(i).getRespTime());
            }
            if(this.get(i).getMakeDate() == null || this.get(i).getMakeDate().equals("null")) {
                pstmt.setDate(21,null);
            } else {
                pstmt.setDate(21, Date.valueOf(this.get(i).getMakeDate()));
            }
            if(this.get(i).getMakeTime() == null || this.get(i).getMakeTime().equals("null")) {
            	pstmt.setString(22,null);
            } else {
            	pstmt.setString(22, this.get(i).getMakeTime());
            }
            if(this.get(i).getModifyDate() == null || this.get(i).getModifyDate().equals("null")) {
                pstmt.setDate(23,null);
            } else {
                pstmt.setDate(23, Date.valueOf(this.get(i).getModifyDate()));
            }
            if(this.get(i).getModifyTime() == null || this.get(i).getModifyTime().equals("null")) {
            	pstmt.setString(24,null);
            } else {
            	pstmt.setString(24, this.get(i).getModifyTime());
            }
            if(this.get(i).getBak1() == null || this.get(i).getBak1().equals("null")) {
            	pstmt.setString(25,null);
            } else {
            	pstmt.setString(25, this.get(i).getBak1());
            }
            if(this.get(i).getBak2() == null || this.get(i).getBak2().equals("null")) {
            	pstmt.setString(26,null);
            } else {
            	pstmt.setString(26, this.get(i).getBak2());
            }
                pstmt.addBatch();
            }
            pstmt.executeBatch();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            ex.printStackTrace();
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LKNRTBizTransDBSet";
            tError.functionName = "insert()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            try {
                pstmt.close();
            } catch (Exception e){e.printStackTrace();}

            if( !mflag ) {
                try {
                    con.close();
                } catch (Exception e){e.printStackTrace();}
            }

            return false;
        }

        if( !mflag ) {
            try {
                con.close();
            } catch (Exception e){e.printStackTrace();}
        }

        return true;
    }
}
