/**
 * Copyright (c) 2018 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import com.sinosoft.lis.db.LKTransTracksDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.*;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

/**
 * <p>ClassName: LKTransTracksSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2018</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2018-09-17
 */
public class LKTransTracksSchema implements Schema, Cloneable {
    // @Field
    /** 物理主键 */
    private long TransID;
    /** 分表id */
    private String ShadingID;
    /** 交易流水号 */
    private String TransNo;
    /** 交易渠道 */
    private String AccessChnl;
    /** 交易子渠道 */
    private String AccessChnlSub;
    /** 交易编码 */
    private String TransCode;
    /** 原交易编码 */
    private String TransCodeOri;
    /** 银行代码 */
    private String BankCode;
    /** 银行地区代码 */
    private String BankBranch;
    /** 银行网点代码 */
    private String BankNode;
    /** 保单号 */
    private String ContNo;
    /** 投保单号 */
    private String ProposalNo;
    /** 交易金额 */
    private double TransMony;
    /** 交易日期 */
    private Date TransDate;
    /** 交易时间 */
    private String TransTime;
    /** 团/个标志 */
    private String ContType;
    /** 投保结论 */
    private String PHConclusion;
    /** 核保结论 */
    private String UWConclusion;
    /** 出单方式 */
    private String SellsWay;
    /** 性能 */
    private String Perform;
    /** 对账状态编码 */
    private String BalanceStateCode;
    /** 对账结果编码 */
    private String BalanceResult;
    /** 对账结果描述 */
    private String BalanceResultDesc;
    /** 备用字段1 */
    private String Temp1;
    /** 备用字段2 */
    private String Temp2;
    /** 备用字段3 */
    private String Temp3;
    /** 备用字段4 */
    private String Temp4;
    /** 备用字段5 */
    private String Temp5;
    /** 缴费方式 */
    private String PayMode;

    public static final int FIELDNUM = 29;    // 数据库表的字段个数

    private static String[] PK;                // 主键

    private FDate fDate = new FDate();        // 处理日期

    public CErrors mErrors;            // 错误信息

    // @Constructor
    public LKTransTracksSchema() {
        mErrors = new CErrors();

        String[] pk = new String[1];
        pk[0] = "TransID";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException {
        LKTransTracksSchema cloned = (LKTransTracksSchema)super.clone();
        cloned.fDate = (FDate) fDate.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK() {
        return PK;
    }

    public long getTransID() {
        return TransID;
    }
    public void setTransID(long aTransID) {
        TransID = aTransID;
    }
    public void setTransID(String aTransID) {
        if (aTransID != null && !aTransID.equals("")) {
            TransID = new Long(aTransID).longValue();
        }
    }

    public String getShadingID() {
        return ShadingID;
    }
    public void setShadingID(String aShadingID) {
        ShadingID = aShadingID;
    }
    public String getTransNo() {
        return TransNo;
    }
    public void setTransNo(String aTransNo) {
        TransNo = aTransNo;
    }
    public String getAccessChnl() {
        return AccessChnl;
    }
    public void setAccessChnl(String aAccessChnl) {
        AccessChnl = aAccessChnl;
    }
    public String getAccessChnlSub() {
        return AccessChnlSub;
    }
    public void setAccessChnlSub(String aAccessChnlSub) {
        AccessChnlSub = aAccessChnlSub;
    }
    public String getTransCode() {
        return TransCode;
    }
    public void setTransCode(String aTransCode) {
        TransCode = aTransCode;
    }
    public String getTransCodeOri() {
        return TransCodeOri;
    }
    public void setTransCodeOri(String aTransCodeOri) {
        TransCodeOri = aTransCodeOri;
    }
    public String getBankCode() {
        return BankCode;
    }
    public void setBankCode(String aBankCode) {
        BankCode = aBankCode;
    }
    public String getBankBranch() {
        return BankBranch;
    }
    public void setBankBranch(String aBankBranch) {
        BankBranch = aBankBranch;
    }
    public String getBankNode() {
        return BankNode;
    }
    public void setBankNode(String aBankNode) {
        BankNode = aBankNode;
    }
    public String getContNo() {
        return ContNo;
    }
    public void setContNo(String aContNo) {
        ContNo = aContNo;
    }
    public String getProposalNo() {
        return ProposalNo;
    }
    public void setProposalNo(String aProposalNo) {
        ProposalNo = aProposalNo;
    }
    public double getTransMony() {
        return TransMony;
    }
    public void setTransMony(double aTransMony) {
        TransMony = aTransMony;
    }
    public void setTransMony(String aTransMony) {
        if (aTransMony != null && !aTransMony.equals("")) {
            Double tDouble = new Double(aTransMony);
            double d = tDouble.doubleValue();
            TransMony = d;
        }
    }

    public String getTransDate() {
        if(TransDate != null) {
            return fDate.getString(TransDate);
        } else {
            return null;
        }
    }
    public void setTransDate(Date aTransDate) {
        TransDate = aTransDate;
    }
    public void setTransDate(String aTransDate) {
        if (aTransDate != null && !aTransDate.equals("")) {
            TransDate = fDate.getDate(aTransDate);
        } else
            TransDate = null;
    }

    public String getTransTime() {
        return TransTime;
    }
    public void setTransTime(String aTransTime) {
        TransTime = aTransTime;
    }
    public String getContType() {
        return ContType;
    }
    public void setContType(String aContType) {
        ContType = aContType;
    }
    public String getPHConclusion() {
        return PHConclusion;
    }
    public void setPHConclusion(String aPHConclusion) {
        PHConclusion = aPHConclusion;
    }
    public String getUWConclusion() {
        return UWConclusion;
    }
    public void setUWConclusion(String aUWConclusion) {
        UWConclusion = aUWConclusion;
    }
    public String getSellsWay() {
        return SellsWay;
    }
    public void setSellsWay(String aSellsWay) {
        SellsWay = aSellsWay;
    }
    public String getPerform() {
        return Perform;
    }
    public void setPerform(String aPerform) {
        Perform = aPerform;
    }
    public String getBalanceStateCode() {
        return BalanceStateCode;
    }
    public void setBalanceStateCode(String aBalanceStateCode) {
        BalanceStateCode = aBalanceStateCode;
    }
    public String getBalanceResult() {
        return BalanceResult;
    }
    public void setBalanceResult(String aBalanceResult) {
        BalanceResult = aBalanceResult;
    }
    public String getBalanceResultDesc() {
        return BalanceResultDesc;
    }
    public void setBalanceResultDesc(String aBalanceResultDesc) {
        BalanceResultDesc = aBalanceResultDesc;
    }
    public String getTemp1() {
        return Temp1;
    }
    public void setTemp1(String aTemp1) {
        Temp1 = aTemp1;
    }
    public String getTemp2() {
        return Temp2;
    }
    public void setTemp2(String aTemp2) {
        Temp2 = aTemp2;
    }
    public String getTemp3() {
        return Temp3;
    }
    public void setTemp3(String aTemp3) {
        Temp3 = aTemp3;
    }
    public String getTemp4() {
        return Temp4;
    }
    public void setTemp4(String aTemp4) {
        Temp4 = aTemp4;
    }
    public String getTemp5() {
        return Temp5;
    }
    public void setTemp5(String aTemp5) {
        Temp5 = aTemp5;
    }
    public String getPayMode() {
        return PayMode;
    }
    public void setPayMode(String aPayMode) {
        PayMode = aPayMode;
    }

    /**
    * 使用另外一个 LKTransTracksSchema 对象给 Schema 赋值
    * @param: aLKTransTracksSchema LKTransTracksSchema
    **/
    public void setSchema(LKTransTracksSchema aLKTransTracksSchema) {
        this.TransID = aLKTransTracksSchema.getTransID();
        this.ShadingID = aLKTransTracksSchema.getShadingID();
        this.TransNo = aLKTransTracksSchema.getTransNo();
        this.AccessChnl = aLKTransTracksSchema.getAccessChnl();
        this.AccessChnlSub = aLKTransTracksSchema.getAccessChnlSub();
        this.TransCode = aLKTransTracksSchema.getTransCode();
        this.TransCodeOri = aLKTransTracksSchema.getTransCodeOri();
        this.BankCode = aLKTransTracksSchema.getBankCode();
        this.BankBranch = aLKTransTracksSchema.getBankBranch();
        this.BankNode = aLKTransTracksSchema.getBankNode();
        this.ContNo = aLKTransTracksSchema.getContNo();
        this.ProposalNo = aLKTransTracksSchema.getProposalNo();
        this.TransMony = aLKTransTracksSchema.getTransMony();
        this.TransDate = fDate.getDate( aLKTransTracksSchema.getTransDate());
        this.TransTime = aLKTransTracksSchema.getTransTime();
        this.ContType = aLKTransTracksSchema.getContType();
        this.PHConclusion = aLKTransTracksSchema.getPHConclusion();
        this.UWConclusion = aLKTransTracksSchema.getUWConclusion();
        this.SellsWay = aLKTransTracksSchema.getSellsWay();
        this.Perform = aLKTransTracksSchema.getPerform();
        this.BalanceStateCode = aLKTransTracksSchema.getBalanceStateCode();
        this.BalanceResult = aLKTransTracksSchema.getBalanceResult();
        this.BalanceResultDesc = aLKTransTracksSchema.getBalanceResultDesc();
        this.Temp1 = aLKTransTracksSchema.getTemp1();
        this.Temp2 = aLKTransTracksSchema.getTemp2();
        this.Temp3 = aLKTransTracksSchema.getTemp3();
        this.Temp4 = aLKTransTracksSchema.getTemp4();
        this.Temp5 = aLKTransTracksSchema.getTemp5();
        this.PayMode = aLKTransTracksSchema.getPayMode();
    }

    /**
    * 使用 ResultSet 中的第 i 行给 Schema 赋值
    * @param: rs ResultSet
    * @param: i int
    * @return: boolean
    **/
    public boolean setSchema(ResultSet rs,int i) {
        try {
            //rs.absolute(i);        // 非滚动游标
            this.TransID = rs.getLong("TransID");
            if( rs.getString("ShadingID") == null )
                this.ShadingID = null;
            else
                this.ShadingID = rs.getString("ShadingID").trim();

            if( rs.getString("TransNo") == null )
                this.TransNo = null;
            else
                this.TransNo = rs.getString("TransNo").trim();

            if( rs.getString("AccessChnl") == null )
                this.AccessChnl = null;
            else
                this.AccessChnl = rs.getString("AccessChnl").trim();

            if( rs.getString("AccessChnlSub") == null )
                this.AccessChnlSub = null;
            else
                this.AccessChnlSub = rs.getString("AccessChnlSub").trim();

            if( rs.getString("TransCode") == null )
                this.TransCode = null;
            else
                this.TransCode = rs.getString("TransCode").trim();

            if( rs.getString("TransCodeOri") == null )
                this.TransCodeOri = null;
            else
                this.TransCodeOri = rs.getString("TransCodeOri").trim();

            if( rs.getString("BankCode") == null )
                this.BankCode = null;
            else
                this.BankCode = rs.getString("BankCode").trim();

            if( rs.getString("BankBranch") == null )
                this.BankBranch = null;
            else
                this.BankBranch = rs.getString("BankBranch").trim();

            if( rs.getString("BankNode") == null )
                this.BankNode = null;
            else
                this.BankNode = rs.getString("BankNode").trim();

            if( rs.getString("ContNo") == null )
                this.ContNo = null;
            else
                this.ContNo = rs.getString("ContNo").trim();

            if( rs.getString("ProposalNo") == null )
                this.ProposalNo = null;
            else
                this.ProposalNo = rs.getString("ProposalNo").trim();

            this.TransMony = rs.getDouble("TransMony");
            this.TransDate = rs.getDate("TransDate");
            if( rs.getString("TransTime") == null )
                this.TransTime = null;
            else
                this.TransTime = rs.getString("TransTime").trim();

            if( rs.getString("ContType") == null )
                this.ContType = null;
            else
                this.ContType = rs.getString("ContType").trim();

            if( rs.getString("PHConclusion") == null )
                this.PHConclusion = null;
            else
                this.PHConclusion = rs.getString("PHConclusion").trim();

            if( rs.getString("UWConclusion") == null )
                this.UWConclusion = null;
            else
                this.UWConclusion = rs.getString("UWConclusion").trim();

            if( rs.getString("SellsWay") == null )
                this.SellsWay = null;
            else
                this.SellsWay = rs.getString("SellsWay").trim();

            if( rs.getString("Perform") == null )
                this.Perform = null;
            else
                this.Perform = rs.getString("Perform").trim();

            if( rs.getString("BalanceStateCode") == null )
                this.BalanceStateCode = null;
            else
                this.BalanceStateCode = rs.getString("BalanceStateCode").trim();

            if( rs.getString("BalanceResult") == null )
                this.BalanceResult = null;
            else
                this.BalanceResult = rs.getString("BalanceResult").trim();

            if( rs.getString("BalanceResultDesc") == null )
                this.BalanceResultDesc = null;
            else
                this.BalanceResultDesc = rs.getString("BalanceResultDesc").trim();

            if( rs.getString("Temp1") == null )
                this.Temp1 = null;
            else
                this.Temp1 = rs.getString("Temp1").trim();

            if( rs.getString("Temp2") == null )
                this.Temp2 = null;
            else
                this.Temp2 = rs.getString("Temp2").trim();

            if( rs.getString("Temp3") == null )
                this.Temp3 = null;
            else
                this.Temp3 = rs.getString("Temp3").trim();

            if( rs.getString("Temp4") == null )
                this.Temp4 = null;
            else
                this.Temp4 = rs.getString("Temp4").trim();

            if( rs.getString("Temp5") == null )
                this.Temp5 = null;
            else
                this.Temp5 = rs.getString("Temp5").trim();

            if( rs.getString("PayMode") == null )
                this.PayMode = null;
            else
                this.PayMode = rs.getString("PayMode").trim();

        }
        catch(SQLException sqle) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LKTransTracksSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = ExceptionUtils.exceptionToString(sqle);
            this.mErrors .addOneError(tError);
            return false;
        }
        return true;
    }

    public LKTransTracksSchema getSchema() {
        LKTransTracksSchema aLKTransTracksSchema = new LKTransTracksSchema();
        aLKTransTracksSchema.setSchema(this);
        return aLKTransTracksSchema;
    }

    public LKTransTracksDB getDB() {
        LKTransTracksDB aDBOper = new LKTransTracksDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
    * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLKTransTracks描述/A>表字段
    * @return: String 返回打包后字符串
    **/
    public String encode() {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(ChgData.chgData(TransID));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ShadingID)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(TransNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AccessChnl)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AccessChnlSub)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(TransCode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(TransCodeOri)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(BankCode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(BankBranch)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(BankNode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ContNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ProposalNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(TransMony));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( TransDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(TransTime)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ContType)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(PHConclusion)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(UWConclusion)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(SellsWay)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Perform)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(BalanceStateCode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(BalanceResult)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(BalanceResultDesc)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Temp1)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Temp2)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Temp3)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Temp4)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Temp5)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(PayMode));
        return strReturn.toString();
    }

    /**
    * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLKTransTracks>历史记账凭证主表信息</A>表字段
    * @param: strMessage String 包含一条纪录数据的字符串
    * @return: boolean
    **/
    public boolean decode(String strMessage) {
        try {
            TransID = new Long(ChgData.chgNumericStr(StrTool.getStr(strMessage,1, SysConst.PACKAGESPILTER))).longValue();
            ShadingID = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
            TransNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
            AccessChnl = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
            AccessChnlSub = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
            TransCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
            TransCodeOri = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
            BankCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
            BankBranch = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
            BankNode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
            ContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
            ProposalNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
            TransMony = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,13, SysConst.PACKAGESPILTER))).doubleValue();
            TransDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER));
            TransTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15, SysConst.PACKAGESPILTER );
            ContType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16, SysConst.PACKAGESPILTER );
            PHConclusion = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17, SysConst.PACKAGESPILTER );
            UWConclusion = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18, SysConst.PACKAGESPILTER );
            SellsWay = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19, SysConst.PACKAGESPILTER );
            Perform = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20, SysConst.PACKAGESPILTER );
            BalanceStateCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 21, SysConst.PACKAGESPILTER );
            BalanceResult = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 22, SysConst.PACKAGESPILTER );
            BalanceResultDesc = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 23, SysConst.PACKAGESPILTER );
            Temp1 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 24, SysConst.PACKAGESPILTER );
            Temp2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 25, SysConst.PACKAGESPILTER );
            Temp3 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 26, SysConst.PACKAGESPILTER );
            Temp4 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 27, SysConst.PACKAGESPILTER );
            Temp5 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 28, SysConst.PACKAGESPILTER );
            PayMode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 29, SysConst.PACKAGESPILTER );
        }
        catch(NumberFormatException ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LKTransTracksSchema";
            tError.functionName = "decode";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            return false;
        }
        return true;
    }

    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("TransID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(TransID));
        }
        if (FCode.equalsIgnoreCase("ShadingID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ShadingID));
        }
        if (FCode.equalsIgnoreCase("TransNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(TransNo));
        }
        if (FCode.equalsIgnoreCase("AccessChnl")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AccessChnl));
        }
        if (FCode.equalsIgnoreCase("AccessChnlSub")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AccessChnlSub));
        }
        if (FCode.equalsIgnoreCase("TransCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(TransCode));
        }
        if (FCode.equalsIgnoreCase("TransCodeOri")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(TransCodeOri));
        }
        if (FCode.equalsIgnoreCase("BankCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BankCode));
        }
        if (FCode.equalsIgnoreCase("BankBranch")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BankBranch));
        }
        if (FCode.equalsIgnoreCase("BankNode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BankNode));
        }
        if (FCode.equalsIgnoreCase("ContNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ContNo));
        }
        if (FCode.equalsIgnoreCase("ProposalNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ProposalNo));
        }
        if (FCode.equalsIgnoreCase("TransMony")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(TransMony));
        }
        if (FCode.equalsIgnoreCase("TransDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getTransDate()));
        }
        if (FCode.equalsIgnoreCase("TransTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(TransTime));
        }
        if (FCode.equalsIgnoreCase("ContType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ContType));
        }
        if (FCode.equalsIgnoreCase("PHConclusion")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PHConclusion));
        }
        if (FCode.equalsIgnoreCase("UWConclusion")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(UWConclusion));
        }
        if (FCode.equalsIgnoreCase("SellsWay")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SellsWay));
        }
        if (FCode.equalsIgnoreCase("Perform")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Perform));
        }
        if (FCode.equalsIgnoreCase("BalanceStateCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BalanceStateCode));
        }
        if (FCode.equalsIgnoreCase("BalanceResult")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BalanceResult));
        }
        if (FCode.equalsIgnoreCase("BalanceResultDesc")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BalanceResultDesc));
        }
        if (FCode.equalsIgnoreCase("Temp1")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Temp1));
        }
        if (FCode.equalsIgnoreCase("Temp2")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Temp2));
        }
        if (FCode.equalsIgnoreCase("Temp3")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Temp3));
        }
        if (FCode.equalsIgnoreCase("Temp4")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Temp4));
        }
        if (FCode.equalsIgnoreCase("Temp5")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Temp5));
        }
        if (FCode.equalsIgnoreCase("PayMode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PayMode));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = String.valueOf(TransID);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(ShadingID);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(TransNo);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(AccessChnl);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(AccessChnlSub);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(TransCode);
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(TransCodeOri);
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(BankCode);
                break;
            case 8:
                strFieldValue = StrTool.GBKToUnicode(BankBranch);
                break;
            case 9:
                strFieldValue = StrTool.GBKToUnicode(BankNode);
                break;
            case 10:
                strFieldValue = StrTool.GBKToUnicode(ContNo);
                break;
            case 11:
                strFieldValue = StrTool.GBKToUnicode(ProposalNo);
                break;
            case 12:
                strFieldValue = String.valueOf(TransMony);
                break;
            case 13:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getTransDate()));
                break;
            case 14:
                strFieldValue = StrTool.GBKToUnicode(TransTime);
                break;
            case 15:
                strFieldValue = StrTool.GBKToUnicode(ContType);
                break;
            case 16:
                strFieldValue = StrTool.GBKToUnicode(PHConclusion);
                break;
            case 17:
                strFieldValue = StrTool.GBKToUnicode(UWConclusion);
                break;
            case 18:
                strFieldValue = StrTool.GBKToUnicode(SellsWay);
                break;
            case 19:
                strFieldValue = StrTool.GBKToUnicode(Perform);
                break;
            case 20:
                strFieldValue = StrTool.GBKToUnicode(BalanceStateCode);
                break;
            case 21:
                strFieldValue = StrTool.GBKToUnicode(BalanceResult);
                break;
            case 22:
                strFieldValue = StrTool.GBKToUnicode(BalanceResultDesc);
                break;
            case 23:
                strFieldValue = StrTool.GBKToUnicode(Temp1);
                break;
            case 24:
                strFieldValue = StrTool.GBKToUnicode(Temp2);
                break;
            case 25:
                strFieldValue = StrTool.GBKToUnicode(Temp3);
                break;
            case 26:
                strFieldValue = StrTool.GBKToUnicode(Temp4);
                break;
            case 27:
                strFieldValue = StrTool.GBKToUnicode(Temp5);
                break;
            case 28:
                strFieldValue = StrTool.GBKToUnicode(PayMode);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("TransID")) {
            if( FValue != null && !FValue.equals("")) {
                TransID = new Long(FValue).longValue();
            }
        }
        if (FCode.equalsIgnoreCase("ShadingID")) {
            if( FValue != null && !FValue.equals(""))
            {
                ShadingID = FValue.trim();
            }
            else
                ShadingID = null;
        }
        if (FCode.equalsIgnoreCase("TransNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                TransNo = FValue.trim();
            }
            else
                TransNo = null;
        }
        if (FCode.equalsIgnoreCase("AccessChnl")) {
            if( FValue != null && !FValue.equals(""))
            {
                AccessChnl = FValue.trim();
            }
            else
                AccessChnl = null;
        }
        if (FCode.equalsIgnoreCase("AccessChnlSub")) {
            if( FValue != null && !FValue.equals(""))
            {
                AccessChnlSub = FValue.trim();
            }
            else
                AccessChnlSub = null;
        }
        if (FCode.equalsIgnoreCase("TransCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                TransCode = FValue.trim();
            }
            else
                TransCode = null;
        }
        if (FCode.equalsIgnoreCase("TransCodeOri")) {
            if( FValue != null && !FValue.equals(""))
            {
                TransCodeOri = FValue.trim();
            }
            else
                TransCodeOri = null;
        }
        if (FCode.equalsIgnoreCase("BankCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                BankCode = FValue.trim();
            }
            else
                BankCode = null;
        }
        if (FCode.equalsIgnoreCase("BankBranch")) {
            if( FValue != null && !FValue.equals(""))
            {
                BankBranch = FValue.trim();
            }
            else
                BankBranch = null;
        }
        if (FCode.equalsIgnoreCase("BankNode")) {
            if( FValue != null && !FValue.equals(""))
            {
                BankNode = FValue.trim();
            }
            else
                BankNode = null;
        }
        if (FCode.equalsIgnoreCase("ContNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                ContNo = FValue.trim();
            }
            else
                ContNo = null;
        }
        if (FCode.equalsIgnoreCase("ProposalNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                ProposalNo = FValue.trim();
            }
            else
                ProposalNo = null;
        }
        if (FCode.equalsIgnoreCase("TransMony")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                TransMony = d;
            }
        }
        if (FCode.equalsIgnoreCase("TransDate")) {
            if(FValue != null && !FValue.equals("")) {
                TransDate = fDate.getDate( FValue );
            }
            else
                TransDate = null;
        }
        if (FCode.equalsIgnoreCase("TransTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                TransTime = FValue.trim();
            }
            else
                TransTime = null;
        }
        if (FCode.equalsIgnoreCase("ContType")) {
            if( FValue != null && !FValue.equals(""))
            {
                ContType = FValue.trim();
            }
            else
                ContType = null;
        }
        if (FCode.equalsIgnoreCase("PHConclusion")) {
            if( FValue != null && !FValue.equals(""))
            {
                PHConclusion = FValue.trim();
            }
            else
                PHConclusion = null;
        }
        if (FCode.equalsIgnoreCase("UWConclusion")) {
            if( FValue != null && !FValue.equals(""))
            {
                UWConclusion = FValue.trim();
            }
            else
                UWConclusion = null;
        }
        if (FCode.equalsIgnoreCase("SellsWay")) {
            if( FValue != null && !FValue.equals(""))
            {
                SellsWay = FValue.trim();
            }
            else
                SellsWay = null;
        }
        if (FCode.equalsIgnoreCase("Perform")) {
            if( FValue != null && !FValue.equals(""))
            {
                Perform = FValue.trim();
            }
            else
                Perform = null;
        }
        if (FCode.equalsIgnoreCase("BalanceStateCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                BalanceStateCode = FValue.trim();
            }
            else
                BalanceStateCode = null;
        }
        if (FCode.equalsIgnoreCase("BalanceResult")) {
            if( FValue != null && !FValue.equals(""))
            {
                BalanceResult = FValue.trim();
            }
            else
                BalanceResult = null;
        }
        if (FCode.equalsIgnoreCase("BalanceResultDesc")) {
            if( FValue != null && !FValue.equals(""))
            {
                BalanceResultDesc = FValue.trim();
            }
            else
                BalanceResultDesc = null;
        }
        if (FCode.equalsIgnoreCase("Temp1")) {
            if( FValue != null && !FValue.equals(""))
            {
                Temp1 = FValue.trim();
            }
            else
                Temp1 = null;
        }
        if (FCode.equalsIgnoreCase("Temp2")) {
            if( FValue != null && !FValue.equals(""))
            {
                Temp2 = FValue.trim();
            }
            else
                Temp2 = null;
        }
        if (FCode.equalsIgnoreCase("Temp3")) {
            if( FValue != null && !FValue.equals(""))
            {
                Temp3 = FValue.trim();
            }
            else
                Temp3 = null;
        }
        if (FCode.equalsIgnoreCase("Temp4")) {
            if( FValue != null && !FValue.equals(""))
            {
                Temp4 = FValue.trim();
            }
            else
                Temp4 = null;
        }
        if (FCode.equalsIgnoreCase("Temp5")) {
            if( FValue != null && !FValue.equals(""))
            {
                Temp5 = FValue.trim();
            }
            else
                Temp5 = null;
        }
        if (FCode.equalsIgnoreCase("PayMode")) {
            if( FValue != null && !FValue.equals(""))
            {
                PayMode = FValue.trim();
            }
            else
                PayMode = null;
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject) return true;
        if (otherObject == null) return false;
        if (getClass() != otherObject.getClass()) return false;
        LKTransTracksSchema other = (LKTransTracksSchema)otherObject;
        return
            TransID == other.getTransID()
            && ShadingID.equals(other.getShadingID())
            && TransNo.equals(other.getTransNo())
            && AccessChnl.equals(other.getAccessChnl())
            && AccessChnlSub.equals(other.getAccessChnlSub())
            && TransCode.equals(other.getTransCode())
            && TransCodeOri.equals(other.getTransCodeOri())
            && BankCode.equals(other.getBankCode())
            && BankBranch.equals(other.getBankBranch())
            && BankNode.equals(other.getBankNode())
            && ContNo.equals(other.getContNo())
            && ProposalNo.equals(other.getProposalNo())
            && TransMony == other.getTransMony()
            && fDate.getString(TransDate).equals(other.getTransDate())
            && TransTime.equals(other.getTransTime())
            && ContType.equals(other.getContType())
            && PHConclusion.equals(other.getPHConclusion())
            && UWConclusion.equals(other.getUWConclusion())
            && SellsWay.equals(other.getSellsWay())
            && Perform.equals(other.getPerform())
            && BalanceStateCode.equals(other.getBalanceStateCode())
            && BalanceResult.equals(other.getBalanceResult())
            && BalanceResultDesc.equals(other.getBalanceResultDesc())
            && Temp1.equals(other.getTemp1())
            && Temp2.equals(other.getTemp2())
            && Temp3.equals(other.getTemp3())
            && Temp4.equals(other.getTemp4())
            && Temp5.equals(other.getTemp5())
            && PayMode.equals(other.getPayMode());
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("TransID") ) {
            return 0;
        }
        if( strFieldName.equals("ShadingID") ) {
            return 1;
        }
        if( strFieldName.equals("TransNo") ) {
            return 2;
        }
        if( strFieldName.equals("AccessChnl") ) {
            return 3;
        }
        if( strFieldName.equals("AccessChnlSub") ) {
            return 4;
        }
        if( strFieldName.equals("TransCode") ) {
            return 5;
        }
        if( strFieldName.equals("TransCodeOri") ) {
            return 6;
        }
        if( strFieldName.equals("BankCode") ) {
            return 7;
        }
        if( strFieldName.equals("BankBranch") ) {
            return 8;
        }
        if( strFieldName.equals("BankNode") ) {
            return 9;
        }
        if( strFieldName.equals("ContNo") ) {
            return 10;
        }
        if( strFieldName.equals("ProposalNo") ) {
            return 11;
        }
        if( strFieldName.equals("TransMony") ) {
            return 12;
        }
        if( strFieldName.equals("TransDate") ) {
            return 13;
        }
        if( strFieldName.equals("TransTime") ) {
            return 14;
        }
        if( strFieldName.equals("ContType") ) {
            return 15;
        }
        if( strFieldName.equals("PHConclusion") ) {
            return 16;
        }
        if( strFieldName.equals("UWConclusion") ) {
            return 17;
        }
        if( strFieldName.equals("SellsWay") ) {
            return 18;
        }
        if( strFieldName.equals("Perform") ) {
            return 19;
        }
        if( strFieldName.equals("BalanceStateCode") ) {
            return 20;
        }
        if( strFieldName.equals("BalanceResult") ) {
            return 21;
        }
        if( strFieldName.equals("BalanceResultDesc") ) {
            return 22;
        }
        if( strFieldName.equals("Temp1") ) {
            return 23;
        }
        if( strFieldName.equals("Temp2") ) {
            return 24;
        }
        if( strFieldName.equals("Temp3") ) {
            return 25;
        }
        if( strFieldName.equals("Temp4") ) {
            return 26;
        }
        if( strFieldName.equals("Temp5") ) {
            return 27;
        }
        if( strFieldName.equals("PayMode") ) {
            return 28;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "TransID";
                break;
            case 1:
                strFieldName = "ShadingID";
                break;
            case 2:
                strFieldName = "TransNo";
                break;
            case 3:
                strFieldName = "AccessChnl";
                break;
            case 4:
                strFieldName = "AccessChnlSub";
                break;
            case 5:
                strFieldName = "TransCode";
                break;
            case 6:
                strFieldName = "TransCodeOri";
                break;
            case 7:
                strFieldName = "BankCode";
                break;
            case 8:
                strFieldName = "BankBranch";
                break;
            case 9:
                strFieldName = "BankNode";
                break;
            case 10:
                strFieldName = "ContNo";
                break;
            case 11:
                strFieldName = "ProposalNo";
                break;
            case 12:
                strFieldName = "TransMony";
                break;
            case 13:
                strFieldName = "TransDate";
                break;
            case 14:
                strFieldName = "TransTime";
                break;
            case 15:
                strFieldName = "ContType";
                break;
            case 16:
                strFieldName = "PHConclusion";
                break;
            case 17:
                strFieldName = "UWConclusion";
                break;
            case 18:
                strFieldName = "SellsWay";
                break;
            case 19:
                strFieldName = "Perform";
                break;
            case 20:
                strFieldName = "BalanceStateCode";
                break;
            case 21:
                strFieldName = "BalanceResult";
                break;
            case 22:
                strFieldName = "BalanceResultDesc";
                break;
            case 23:
                strFieldName = "Temp1";
                break;
            case 24:
                strFieldName = "Temp2";
                break;
            case 25:
                strFieldName = "Temp3";
                break;
            case 26:
                strFieldName = "Temp4";
                break;
            case 27:
                strFieldName = "Temp5";
                break;
            case 28:
                strFieldName = "PayMode";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "TRANSID":
                return Schema.TYPE_LONG;
            case "SHADINGID":
                return Schema.TYPE_STRING;
            case "TRANSNO":
                return Schema.TYPE_STRING;
            case "ACCESSCHNL":
                return Schema.TYPE_STRING;
            case "ACCESSCHNLSUB":
                return Schema.TYPE_STRING;
            case "TRANSCODE":
                return Schema.TYPE_STRING;
            case "TRANSCODEORI":
                return Schema.TYPE_STRING;
            case "BANKCODE":
                return Schema.TYPE_STRING;
            case "BANKBRANCH":
                return Schema.TYPE_STRING;
            case "BANKNODE":
                return Schema.TYPE_STRING;
            case "CONTNO":
                return Schema.TYPE_STRING;
            case "PROPOSALNO":
                return Schema.TYPE_STRING;
            case "TRANSMONY":
                return Schema.TYPE_DOUBLE;
            case "TRANSDATE":
                return Schema.TYPE_DATE;
            case "TRANSTIME":
                return Schema.TYPE_STRING;
            case "CONTTYPE":
                return Schema.TYPE_STRING;
            case "PHCONCLUSION":
                return Schema.TYPE_STRING;
            case "UWCONCLUSION":
                return Schema.TYPE_STRING;
            case "SELLSWAY":
                return Schema.TYPE_STRING;
            case "PERFORM":
                return Schema.TYPE_STRING;
            case "BALANCESTATECODE":
                return Schema.TYPE_STRING;
            case "BALANCERESULT":
                return Schema.TYPE_STRING;
            case "BALANCERESULTDESC":
                return Schema.TYPE_STRING;
            case "TEMP1":
                return Schema.TYPE_STRING;
            case "TEMP2":
                return Schema.TYPE_STRING;
            case "TEMP3":
                return Schema.TYPE_STRING;
            case "TEMP4":
                return Schema.TYPE_STRING;
            case "TEMP5":
                return Schema.TYPE_STRING;
            case "PAYMODE":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_LONG;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_STRING;
            case 6:
                return Schema.TYPE_STRING;
            case 7:
                return Schema.TYPE_STRING;
            case 8:
                return Schema.TYPE_STRING;
            case 9:
                return Schema.TYPE_STRING;
            case 10:
                return Schema.TYPE_STRING;
            case 11:
                return Schema.TYPE_STRING;
            case 12:
                return Schema.TYPE_DOUBLE;
            case 13:
                return Schema.TYPE_DATE;
            case 14:
                return Schema.TYPE_STRING;
            case 15:
                return Schema.TYPE_STRING;
            case 16:
                return Schema.TYPE_STRING;
            case 17:
                return Schema.TYPE_STRING;
            case 18:
                return Schema.TYPE_STRING;
            case 19:
                return Schema.TYPE_STRING;
            case 20:
                return Schema.TYPE_STRING;
            case 21:
                return Schema.TYPE_STRING;
            case 22:
                return Schema.TYPE_STRING;
            case 23:
                return Schema.TYPE_STRING;
            case 24:
                return Schema.TYPE_STRING;
            case 25:
                return Schema.TYPE_STRING;
            case 26:
                return Schema.TYPE_STRING;
            case 27:
                return Schema.TYPE_STRING;
            case 28:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
}
