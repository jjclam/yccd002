/**
 * Copyright (c) 2017 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.entity;

import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.Pojo;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;

import java.io.Serializable;

/**
 * <p>ClassName: LCSpecPojo </p>
 * <p>Copyright: Copyright (c) 2017</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2017-10-17
 */
public class LCSpecPojo implements Pojo,Serializable {
    // @Field
    /** Id */
    private long SpecID; 
    /** Fk_lccont */
    private long ContID; 
    /** Shardingid */
    private String ShardingID; 
    /** 集体合同号码 */
    private String GrpContNo; 
    /** 合同号码 */
    private String ContNo; 
    /** 保单号码 */
    private String PolNo; 
    /** 投保单号码 */
    private String ProposalNo; 
    /** 打印流水号 */
    private String PrtSeq; 
    /** 流水号 */
    private String SerialNo; 
    /** 批单号码 */
    private String EndorsementNo; 
    /** 特约类型 */
    private String SpecType; 
    /** 特约编码 */
    private String SpecCode; 
    /** 特约内容 */
    private String SpecContent; 
    /** 打印标记 */
    private String PrtFlag; 
    /** 备份类型 */
    private String BackupType; 
    /** 操作员 */
    private String Operator; 
    /** 入机日期 */
    private String  MakeDate;
    /** 入机时间 */
    private String MakeTime; 
    /** 最后一次修改日期 */
    private String  ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime; 


    public static final int FIELDNUM = 20;    // 数据库表的字段个数

    private FDate fDate = new FDate();        // 处理日期
    public long getSpecID() {
        return SpecID;
    }
    public void setSpecID(long aSpecID) {
        SpecID = aSpecID;
    }
    public void setSpecID(String aSpecID) {
        if (aSpecID != null && !aSpecID.equals("")) {
            SpecID = new Long(aSpecID).longValue();
        }
    }

    public long getContID() {
        return ContID;
    }
    public void setContID(long aContID) {
        ContID = aContID;
    }
    public void setContID(String aContID) {
        if (aContID != null && !aContID.equals("")) {
            ContID = new Long(aContID).longValue();
        }
    }

    public String getShardingID() {
        return ShardingID;
    }
    public void setShardingID(String aShardingID) {
        ShardingID = aShardingID;
    }
    public String getGrpContNo() {
        return GrpContNo;
    }
    public void setGrpContNo(String aGrpContNo) {
        GrpContNo = aGrpContNo;
    }
    public String getContNo() {
        return ContNo;
    }
    public void setContNo(String aContNo) {
        ContNo = aContNo;
    }
    public String getPolNo() {
        return PolNo;
    }
    public void setPolNo(String aPolNo) {
        PolNo = aPolNo;
    }
    public String getProposalNo() {
        return ProposalNo;
    }
    public void setProposalNo(String aProposalNo) {
        ProposalNo = aProposalNo;
    }
    public String getPrtSeq() {
        return PrtSeq;
    }
    public void setPrtSeq(String aPrtSeq) {
        PrtSeq = aPrtSeq;
    }
    public String getSerialNo() {
        return SerialNo;
    }
    public void setSerialNo(String aSerialNo) {
        SerialNo = aSerialNo;
    }
    public String getEndorsementNo() {
        return EndorsementNo;
    }
    public void setEndorsementNo(String aEndorsementNo) {
        EndorsementNo = aEndorsementNo;
    }
    public String getSpecType() {
        return SpecType;
    }
    public void setSpecType(String aSpecType) {
        SpecType = aSpecType;
    }
    public String getSpecCode() {
        return SpecCode;
    }
    public void setSpecCode(String aSpecCode) {
        SpecCode = aSpecCode;
    }
    public String getSpecContent() {
        return SpecContent;
    }
    public void setSpecContent(String aSpecContent) {
        SpecContent = aSpecContent;
    }
    public String getPrtFlag() {
        return PrtFlag;
    }
    public void setPrtFlag(String aPrtFlag) {
        PrtFlag = aPrtFlag;
    }
    public String getBackupType() {
        return BackupType;
    }
    public void setBackupType(String aBackupType) {
        BackupType = aBackupType;
    }
    public String getOperator() {
        return Operator;
    }
    public void setOperator(String aOperator) {
        Operator = aOperator;
    }
    public String getMakeDate() {
        return MakeDate;
    }
    public void setMakeDate(String aMakeDate) {
        MakeDate = aMakeDate;
    }
    public String getMakeTime() {
        return MakeTime;
    }
    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }
    public String getModifyDate() {
        return ModifyDate;
    }
    public void setModifyDate(String aModifyDate) {
        ModifyDate = aModifyDate;
    }
    public String getModifyTime() {
        return ModifyTime;
    }
    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("SpecID") ) {
            return 0;
        }
        if( strFieldName.equals("ContID") ) {
            return 1;
        }
        if( strFieldName.equals("ShardingID") ) {
            return 2;
        }
        if( strFieldName.equals("GrpContNo") ) {
            return 3;
        }
        if( strFieldName.equals("ContNo") ) {
            return 4;
        }
        if( strFieldName.equals("PolNo") ) {
            return 5;
        }
        if( strFieldName.equals("ProposalNo") ) {
            return 6;
        }
        if( strFieldName.equals("PrtSeq") ) {
            return 7;
        }
        if( strFieldName.equals("SerialNo") ) {
            return 8;
        }
        if( strFieldName.equals("EndorsementNo") ) {
            return 9;
        }
        if( strFieldName.equals("SpecType") ) {
            return 10;
        }
        if( strFieldName.equals("SpecCode") ) {
            return 11;
        }
        if( strFieldName.equals("SpecContent") ) {
            return 12;
        }
        if( strFieldName.equals("PrtFlag") ) {
            return 13;
        }
        if( strFieldName.equals("BackupType") ) {
            return 14;
        }
        if( strFieldName.equals("Operator") ) {
            return 15;
        }
        if( strFieldName.equals("MakeDate") ) {
            return 16;
        }
        if( strFieldName.equals("MakeTime") ) {
            return 17;
        }
        if( strFieldName.equals("ModifyDate") ) {
            return 18;
        }
        if( strFieldName.equals("ModifyTime") ) {
            return 19;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "SpecID";
                break;
            case 1:
                strFieldName = "ContID";
                break;
            case 2:
                strFieldName = "ShardingID";
                break;
            case 3:
                strFieldName = "GrpContNo";
                break;
            case 4:
                strFieldName = "ContNo";
                break;
            case 5:
                strFieldName = "PolNo";
                break;
            case 6:
                strFieldName = "ProposalNo";
                break;
            case 7:
                strFieldName = "PrtSeq";
                break;
            case 8:
                strFieldName = "SerialNo";
                break;
            case 9:
                strFieldName = "EndorsementNo";
                break;
            case 10:
                strFieldName = "SpecType";
                break;
            case 11:
                strFieldName = "SpecCode";
                break;
            case 12:
                strFieldName = "SpecContent";
                break;
            case 13:
                strFieldName = "PrtFlag";
                break;
            case 14:
                strFieldName = "BackupType";
                break;
            case 15:
                strFieldName = "Operator";
                break;
            case 16:
                strFieldName = "MakeDate";
                break;
            case 17:
                strFieldName = "MakeTime";
                break;
            case 18:
                strFieldName = "ModifyDate";
                break;
            case 19:
                strFieldName = "ModifyTime";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "SPECID":
                return Schema.TYPE_LONG;
            case "CONTID":
                return Schema.TYPE_LONG;
            case "SHARDINGID":
                return Schema.TYPE_STRING;
            case "GRPCONTNO":
                return Schema.TYPE_STRING;
            case "CONTNO":
                return Schema.TYPE_STRING;
            case "POLNO":
                return Schema.TYPE_STRING;
            case "PROPOSALNO":
                return Schema.TYPE_STRING;
            case "PRTSEQ":
                return Schema.TYPE_STRING;
            case "SERIALNO":
                return Schema.TYPE_STRING;
            case "ENDORSEMENTNO":
                return Schema.TYPE_STRING;
            case "SPECTYPE":
                return Schema.TYPE_STRING;
            case "SPECCODE":
                return Schema.TYPE_STRING;
            case "SPECCONTENT":
                return Schema.TYPE_STRING;
            case "PRTFLAG":
                return Schema.TYPE_STRING;
            case "BACKUPTYPE":
                return Schema.TYPE_STRING;
            case "OPERATOR":
                return Schema.TYPE_STRING;
            case "MAKEDATE":
                return Schema.TYPE_STRING;
            case "MAKETIME":
                return Schema.TYPE_STRING;
            case "MODIFYDATE":
                return Schema.TYPE_STRING;
            case "MODIFYTIME":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_LONG;
            case 1:
                return Schema.TYPE_LONG;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_STRING;
            case 6:
                return Schema.TYPE_STRING;
            case 7:
                return Schema.TYPE_STRING;
            case 8:
                return Schema.TYPE_STRING;
            case 9:
                return Schema.TYPE_STRING;
            case 10:
                return Schema.TYPE_STRING;
            case 11:
                return Schema.TYPE_STRING;
            case 12:
                return Schema.TYPE_STRING;
            case 13:
                return Schema.TYPE_STRING;
            case 14:
                return Schema.TYPE_STRING;
            case 15:
                return Schema.TYPE_STRING;
            case 16:
                return Schema.TYPE_STRING;
            case 17:
                return Schema.TYPE_STRING;
            case 18:
                return Schema.TYPE_STRING;
            case 19:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("SpecID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SpecID));
        }
        if (FCode.equalsIgnoreCase("ContID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ContID));
        }
        if (FCode.equalsIgnoreCase("ShardingID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ShardingID));
        }
        if (FCode.equalsIgnoreCase("GrpContNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GrpContNo));
        }
        if (FCode.equalsIgnoreCase("ContNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ContNo));
        }
        if (FCode.equalsIgnoreCase("PolNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PolNo));
        }
        if (FCode.equalsIgnoreCase("ProposalNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ProposalNo));
        }
        if (FCode.equalsIgnoreCase("PrtSeq")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PrtSeq));
        }
        if (FCode.equalsIgnoreCase("SerialNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SerialNo));
        }
        if (FCode.equalsIgnoreCase("EndorsementNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(EndorsementNo));
        }
        if (FCode.equalsIgnoreCase("SpecType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SpecType));
        }
        if (FCode.equalsIgnoreCase("SpecCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SpecCode));
        }
        if (FCode.equalsIgnoreCase("SpecContent")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SpecContent));
        }
        if (FCode.equalsIgnoreCase("PrtFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PrtFlag));
        }
        if (FCode.equalsIgnoreCase("BackupType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BackupType));
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeDate));
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyDate));
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = String.valueOf(SpecID);
                break;
            case 1:
                strFieldValue = String.valueOf(ContID);
                break;
            case 2:
                strFieldValue = String.valueOf(ShardingID);
                break;
            case 3:
                strFieldValue = String.valueOf(GrpContNo);
                break;
            case 4:
                strFieldValue = String.valueOf(ContNo);
                break;
            case 5:
                strFieldValue = String.valueOf(PolNo);
                break;
            case 6:
                strFieldValue = String.valueOf(ProposalNo);
                break;
            case 7:
                strFieldValue = String.valueOf(PrtSeq);
                break;
            case 8:
                strFieldValue = String.valueOf(SerialNo);
                break;
            case 9:
                strFieldValue = String.valueOf(EndorsementNo);
                break;
            case 10:
                strFieldValue = String.valueOf(SpecType);
                break;
            case 11:
                strFieldValue = String.valueOf(SpecCode);
                break;
            case 12:
                strFieldValue = String.valueOf(SpecContent);
                break;
            case 13:
                strFieldValue = String.valueOf(PrtFlag);
                break;
            case 14:
                strFieldValue = String.valueOf(BackupType);
                break;
            case 15:
                strFieldValue = String.valueOf(Operator);
                break;
            case 16:
                strFieldValue = String.valueOf(MakeDate);
                break;
            case 17:
                strFieldValue = String.valueOf(MakeTime);
                break;
            case 18:
                strFieldValue = String.valueOf(ModifyDate);
                break;
            case 19:
                strFieldValue = String.valueOf(ModifyTime);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("SpecID")) {
            if( FValue != null && !FValue.equals("")) {
                SpecID = new Long(FValue).longValue();
            }
        }
        if (FCode.equalsIgnoreCase("ContID")) {
            if( FValue != null && !FValue.equals("")) {
                ContID = new Long(FValue).longValue();
            }
        }
        if (FCode.equalsIgnoreCase("ShardingID")) {
            if( FValue != null && !FValue.equals(""))
            {
                ShardingID = FValue.trim();
            }
            else
                ShardingID = null;
        }
        if (FCode.equalsIgnoreCase("GrpContNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                GrpContNo = FValue.trim();
            }
            else
                GrpContNo = null;
        }
        if (FCode.equalsIgnoreCase("ContNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                ContNo = FValue.trim();
            }
            else
                ContNo = null;
        }
        if (FCode.equalsIgnoreCase("PolNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                PolNo = FValue.trim();
            }
            else
                PolNo = null;
        }
        if (FCode.equalsIgnoreCase("ProposalNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                ProposalNo = FValue.trim();
            }
            else
                ProposalNo = null;
        }
        if (FCode.equalsIgnoreCase("PrtSeq")) {
            if( FValue != null && !FValue.equals(""))
            {
                PrtSeq = FValue.trim();
            }
            else
                PrtSeq = null;
        }
        if (FCode.equalsIgnoreCase("SerialNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                SerialNo = FValue.trim();
            }
            else
                SerialNo = null;
        }
        if (FCode.equalsIgnoreCase("EndorsementNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                EndorsementNo = FValue.trim();
            }
            else
                EndorsementNo = null;
        }
        if (FCode.equalsIgnoreCase("SpecType")) {
            if( FValue != null && !FValue.equals(""))
            {
                SpecType = FValue.trim();
            }
            else
                SpecType = null;
        }
        if (FCode.equalsIgnoreCase("SpecCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                SpecCode = FValue.trim();
            }
            else
                SpecCode = null;
        }
        if (FCode.equalsIgnoreCase("SpecContent")) {
            if( FValue != null && !FValue.equals(""))
            {
                SpecContent = FValue.trim();
            }
            else
                SpecContent = null;
        }
        if (FCode.equalsIgnoreCase("PrtFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                PrtFlag = FValue.trim();
            }
            else
                PrtFlag = null;
        }
        if (FCode.equalsIgnoreCase("BackupType")) {
            if( FValue != null && !FValue.equals(""))
            {
                BackupType = FValue.trim();
            }
            else
                BackupType = null;
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            if( FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
                Operator = null;
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeDate = FValue.trim();
            }
            else
                MakeDate = null;
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
                MakeTime = null;
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyDate = FValue.trim();
            }
            else
                ModifyDate = null;
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
                ModifyTime = null;
        }
        return true;
    }


    public String toString() {
    return "LCSpecPojo [" +
            "SpecID="+SpecID +
            ", ContID="+ContID +
            ", ShardingID="+ShardingID +
            ", GrpContNo="+GrpContNo +
            ", ContNo="+ContNo +
            ", PolNo="+PolNo +
            ", ProposalNo="+ProposalNo +
            ", PrtSeq="+PrtSeq +
            ", SerialNo="+SerialNo +
            ", EndorsementNo="+EndorsementNo +
            ", SpecType="+SpecType +
            ", SpecCode="+SpecCode +
            ", SpecContent="+SpecContent +
            ", PrtFlag="+PrtFlag +
            ", BackupType="+BackupType +
            ", Operator="+Operator +
            ", MakeDate="+MakeDate +
            ", MakeTime="+MakeTime +
            ", ModifyDate="+ModifyDate +
            ", ModifyTime="+ModifyTime +"]";
    }
}
