/**
 * Copyright (c) 2017 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.entity;

import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.Pojo;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;

import java.io.Serializable;

/**
 * <p>ClassName: LJAPayPojo </p>
 * <p>Copyright: Copyright (c) 2017</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2017-10-17
 */
public class LJAPayPojo implements Pojo,Serializable {
    // @Field
    /** Id */
    private long APayID; 
    /** Shardingid */
    private String ShardingID; 
    /** 交费收据号码 */
    private String PayNo; 
    /** 应收/实收编号 */
    private String IncomeNo; 
    /** 应收/实收编号类型 */
    private String IncomeType; 
    /** 投保人客户号码 */
    private String AppntNo; 
    /** 总实交金额 */
    private double SumActuPayMoney; 
    /** 交费日期 */
    private String  PayDate;
    /** 到帐日期 */
    private String  EnterAccDate;
    /** 确认日期 */
    private String  ConfDate;
    /** 复核人编码 */
    private String ApproveCode; 
    /** 复核日期 */
    private String  ApproveDate;
    /** 流水号 */
    private String SerialNo; 
    /** 操作员 */
    private String Operator; 
    /** 入机日期 */
    private String  MakeDate;
    /** 入机时间 */
    private String MakeTime; 
    /** 通知书号码 */
    private String GetNoticeNo; 
    /** 最后一次修改日期 */
    private String  ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime; 
    /** 管理机构 */
    private String ManageCom; 
    /** 代理机构 */
    private String AgentCom; 
    /** 代理机构内部分类 */
    private String AgentType; 
    /** 银行编码 */
    private String BankCode; 
    /** 银行帐号 */
    private String BankAccNo; 
    /** 险种编码 */
    private String RiskCode; 
    /** 代理人编码 */
    private String AgentCode; 
    /** 代理人组别 */
    private String AgentGroup; 
    /** 帐户名 */
    private String AccName; 
    /** 最早收费日期 */
    private String  StartPayDate;
    /** 续保收费标记 */
    private String PayTypeFlag; 
    /** 其它号码 */
    private String OtherNo; 
    /** 其它号码类型 */
    private String OtherNoType; 
    /** 定期结算标志 */
    private String BalanceOnTime; 


    public static final int FIELDNUM = 33;    // 数据库表的字段个数

    private FDate fDate = new FDate();        // 处理日期
    public long getAPayID() {
        return APayID;
    }
    public void setAPayID(long aAPayID) {
        APayID = aAPayID;
    }
    public void setAPayID(String aAPayID) {
        if (aAPayID != null && !aAPayID.equals("")) {
            APayID = new Long(aAPayID).longValue();
        }
    }

    public String getShardingID() {
        return ShardingID;
    }
    public void setShardingID(String aShardingID) {
        ShardingID = aShardingID;
    }
    public String getPayNo() {
        return PayNo;
    }
    public void setPayNo(String aPayNo) {
        PayNo = aPayNo;
    }
    public String getIncomeNo() {
        return IncomeNo;
    }
    public void setIncomeNo(String aIncomeNo) {
        IncomeNo = aIncomeNo;
    }
    public String getIncomeType() {
        return IncomeType;
    }
    public void setIncomeType(String aIncomeType) {
        IncomeType = aIncomeType;
    }
    public String getAppntNo() {
        return AppntNo;
    }
    public void setAppntNo(String aAppntNo) {
        AppntNo = aAppntNo;
    }
    public double getSumActuPayMoney() {
        return SumActuPayMoney;
    }
    public void setSumActuPayMoney(double aSumActuPayMoney) {
        SumActuPayMoney = aSumActuPayMoney;
    }
    public void setSumActuPayMoney(String aSumActuPayMoney) {
        if (aSumActuPayMoney != null && !aSumActuPayMoney.equals("")) {
            Double tDouble = new Double(aSumActuPayMoney);
            double d = tDouble.doubleValue();
            SumActuPayMoney = d;
        }
    }

    public String getPayDate() {
        return PayDate;
    }
    public void setPayDate(String aPayDate) {
        PayDate = aPayDate;
    }
    public String getEnterAccDate() {
        return EnterAccDate;
    }
    public void setEnterAccDate(String aEnterAccDate) {
        EnterAccDate = aEnterAccDate;
    }
    public String getConfDate() {
        return ConfDate;
    }
    public void setConfDate(String aConfDate) {
        ConfDate = aConfDate;
    }
    public String getApproveCode() {
        return ApproveCode;
    }
    public void setApproveCode(String aApproveCode) {
        ApproveCode = aApproveCode;
    }
    public String getApproveDate() {
        return ApproveDate;
    }
    public void setApproveDate(String aApproveDate) {
        ApproveDate = aApproveDate;
    }
    public String getSerialNo() {
        return SerialNo;
    }
    public void setSerialNo(String aSerialNo) {
        SerialNo = aSerialNo;
    }
    public String getOperator() {
        return Operator;
    }
    public void setOperator(String aOperator) {
        Operator = aOperator;
    }
    public String getMakeDate() {
        return MakeDate;
    }
    public void setMakeDate(String aMakeDate) {
        MakeDate = aMakeDate;
    }
    public String getMakeTime() {
        return MakeTime;
    }
    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }
    public String getGetNoticeNo() {
        return GetNoticeNo;
    }
    public void setGetNoticeNo(String aGetNoticeNo) {
        GetNoticeNo = aGetNoticeNo;
    }
    public String getModifyDate() {
        return ModifyDate;
    }
    public void setModifyDate(String aModifyDate) {
        ModifyDate = aModifyDate;
    }
    public String getModifyTime() {
        return ModifyTime;
    }
    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }
    public String getManageCom() {
        return ManageCom;
    }
    public void setManageCom(String aManageCom) {
        ManageCom = aManageCom;
    }
    public String getAgentCom() {
        return AgentCom;
    }
    public void setAgentCom(String aAgentCom) {
        AgentCom = aAgentCom;
    }
    public String getAgentType() {
        return AgentType;
    }
    public void setAgentType(String aAgentType) {
        AgentType = aAgentType;
    }
    public String getBankCode() {
        return BankCode;
    }
    public void setBankCode(String aBankCode) {
        BankCode = aBankCode;
    }
    public String getBankAccNo() {
        return BankAccNo;
    }
    public void setBankAccNo(String aBankAccNo) {
        BankAccNo = aBankAccNo;
    }
    public String getRiskCode() {
        return RiskCode;
    }
    public void setRiskCode(String aRiskCode) {
        RiskCode = aRiskCode;
    }
    public String getAgentCode() {
        return AgentCode;
    }
    public void setAgentCode(String aAgentCode) {
        AgentCode = aAgentCode;
    }
    public String getAgentGroup() {
        return AgentGroup;
    }
    public void setAgentGroup(String aAgentGroup) {
        AgentGroup = aAgentGroup;
    }
    public String getAccName() {
        return AccName;
    }
    public void setAccName(String aAccName) {
        AccName = aAccName;
    }
    public String getStartPayDate() {
        return StartPayDate;
    }
    public void setStartPayDate(String aStartPayDate) {
        StartPayDate = aStartPayDate;
    }
    public String getPayTypeFlag() {
        return PayTypeFlag;
    }
    public void setPayTypeFlag(String aPayTypeFlag) {
        PayTypeFlag = aPayTypeFlag;
    }
    public String getOtherNo() {
        return OtherNo;
    }
    public void setOtherNo(String aOtherNo) {
        OtherNo = aOtherNo;
    }
    public String getOtherNoType() {
        return OtherNoType;
    }
    public void setOtherNoType(String aOtherNoType) {
        OtherNoType = aOtherNoType;
    }
    public String getBalanceOnTime() {
        return BalanceOnTime;
    }
    public void setBalanceOnTime(String aBalanceOnTime) {
        BalanceOnTime = aBalanceOnTime;
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("APayID") ) {
            return 0;
        }
        if( strFieldName.equals("ShardingID") ) {
            return 1;
        }
        if( strFieldName.equals("PayNo") ) {
            return 2;
        }
        if( strFieldName.equals("IncomeNo") ) {
            return 3;
        }
        if( strFieldName.equals("IncomeType") ) {
            return 4;
        }
        if( strFieldName.equals("AppntNo") ) {
            return 5;
        }
        if( strFieldName.equals("SumActuPayMoney") ) {
            return 6;
        }
        if( strFieldName.equals("PayDate") ) {
            return 7;
        }
        if( strFieldName.equals("EnterAccDate") ) {
            return 8;
        }
        if( strFieldName.equals("ConfDate") ) {
            return 9;
        }
        if( strFieldName.equals("ApproveCode") ) {
            return 10;
        }
        if( strFieldName.equals("ApproveDate") ) {
            return 11;
        }
        if( strFieldName.equals("SerialNo") ) {
            return 12;
        }
        if( strFieldName.equals("Operator") ) {
            return 13;
        }
        if( strFieldName.equals("MakeDate") ) {
            return 14;
        }
        if( strFieldName.equals("MakeTime") ) {
            return 15;
        }
        if( strFieldName.equals("GetNoticeNo") ) {
            return 16;
        }
        if( strFieldName.equals("ModifyDate") ) {
            return 17;
        }
        if( strFieldName.equals("ModifyTime") ) {
            return 18;
        }
        if( strFieldName.equals("ManageCom") ) {
            return 19;
        }
        if( strFieldName.equals("AgentCom") ) {
            return 20;
        }
        if( strFieldName.equals("AgentType") ) {
            return 21;
        }
        if( strFieldName.equals("BankCode") ) {
            return 22;
        }
        if( strFieldName.equals("BankAccNo") ) {
            return 23;
        }
        if( strFieldName.equals("RiskCode") ) {
            return 24;
        }
        if( strFieldName.equals("AgentCode") ) {
            return 25;
        }
        if( strFieldName.equals("AgentGroup") ) {
            return 26;
        }
        if( strFieldName.equals("AccName") ) {
            return 27;
        }
        if( strFieldName.equals("StartPayDate") ) {
            return 28;
        }
        if( strFieldName.equals("PayTypeFlag") ) {
            return 29;
        }
        if( strFieldName.equals("OtherNo") ) {
            return 30;
        }
        if( strFieldName.equals("OtherNoType") ) {
            return 31;
        }
        if( strFieldName.equals("BalanceOnTime") ) {
            return 32;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "APayID";
                break;
            case 1:
                strFieldName = "ShardingID";
                break;
            case 2:
                strFieldName = "PayNo";
                break;
            case 3:
                strFieldName = "IncomeNo";
                break;
            case 4:
                strFieldName = "IncomeType";
                break;
            case 5:
                strFieldName = "AppntNo";
                break;
            case 6:
                strFieldName = "SumActuPayMoney";
                break;
            case 7:
                strFieldName = "PayDate";
                break;
            case 8:
                strFieldName = "EnterAccDate";
                break;
            case 9:
                strFieldName = "ConfDate";
                break;
            case 10:
                strFieldName = "ApproveCode";
                break;
            case 11:
                strFieldName = "ApproveDate";
                break;
            case 12:
                strFieldName = "SerialNo";
                break;
            case 13:
                strFieldName = "Operator";
                break;
            case 14:
                strFieldName = "MakeDate";
                break;
            case 15:
                strFieldName = "MakeTime";
                break;
            case 16:
                strFieldName = "GetNoticeNo";
                break;
            case 17:
                strFieldName = "ModifyDate";
                break;
            case 18:
                strFieldName = "ModifyTime";
                break;
            case 19:
                strFieldName = "ManageCom";
                break;
            case 20:
                strFieldName = "AgentCom";
                break;
            case 21:
                strFieldName = "AgentType";
                break;
            case 22:
                strFieldName = "BankCode";
                break;
            case 23:
                strFieldName = "BankAccNo";
                break;
            case 24:
                strFieldName = "RiskCode";
                break;
            case 25:
                strFieldName = "AgentCode";
                break;
            case 26:
                strFieldName = "AgentGroup";
                break;
            case 27:
                strFieldName = "AccName";
                break;
            case 28:
                strFieldName = "StartPayDate";
                break;
            case 29:
                strFieldName = "PayTypeFlag";
                break;
            case 30:
                strFieldName = "OtherNo";
                break;
            case 31:
                strFieldName = "OtherNoType";
                break;
            case 32:
                strFieldName = "BalanceOnTime";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "APAYID":
                return Schema.TYPE_LONG;
            case "SHARDINGID":
                return Schema.TYPE_STRING;
            case "PAYNO":
                return Schema.TYPE_STRING;
            case "INCOMENO":
                return Schema.TYPE_STRING;
            case "INCOMETYPE":
                return Schema.TYPE_STRING;
            case "APPNTNO":
                return Schema.TYPE_STRING;
            case "SUMACTUPAYMONEY":
                return Schema.TYPE_DOUBLE;
            case "PAYDATE":
                return Schema.TYPE_STRING;
            case "ENTERACCDATE":
                return Schema.TYPE_STRING;
            case "CONFDATE":
                return Schema.TYPE_STRING;
            case "APPROVECODE":
                return Schema.TYPE_STRING;
            case "APPROVEDATE":
                return Schema.TYPE_STRING;
            case "SERIALNO":
                return Schema.TYPE_STRING;
            case "OPERATOR":
                return Schema.TYPE_STRING;
            case "MAKEDATE":
                return Schema.TYPE_STRING;
            case "MAKETIME":
                return Schema.TYPE_STRING;
            case "GETNOTICENO":
                return Schema.TYPE_STRING;
            case "MODIFYDATE":
                return Schema.TYPE_STRING;
            case "MODIFYTIME":
                return Schema.TYPE_STRING;
            case "MANAGECOM":
                return Schema.TYPE_STRING;
            case "AGENTCOM":
                return Schema.TYPE_STRING;
            case "AGENTTYPE":
                return Schema.TYPE_STRING;
            case "BANKCODE":
                return Schema.TYPE_STRING;
            case "BANKACCNO":
                return Schema.TYPE_STRING;
            case "RISKCODE":
                return Schema.TYPE_STRING;
            case "AGENTCODE":
                return Schema.TYPE_STRING;
            case "AGENTGROUP":
                return Schema.TYPE_STRING;
            case "ACCNAME":
                return Schema.TYPE_STRING;
            case "STARTPAYDATE":
                return Schema.TYPE_STRING;
            case "PAYTYPEFLAG":
                return Schema.TYPE_STRING;
            case "OTHERNO":
                return Schema.TYPE_STRING;
            case "OTHERNOTYPE":
                return Schema.TYPE_STRING;
            case "BALANCEONTIME":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_LONG;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_STRING;
            case 6:
                return Schema.TYPE_DOUBLE;
            case 7:
                return Schema.TYPE_STRING;
            case 8:
                return Schema.TYPE_STRING;
            case 9:
                return Schema.TYPE_STRING;
            case 10:
                return Schema.TYPE_STRING;
            case 11:
                return Schema.TYPE_STRING;
            case 12:
                return Schema.TYPE_STRING;
            case 13:
                return Schema.TYPE_STRING;
            case 14:
                return Schema.TYPE_STRING;
            case 15:
                return Schema.TYPE_STRING;
            case 16:
                return Schema.TYPE_STRING;
            case 17:
                return Schema.TYPE_STRING;
            case 18:
                return Schema.TYPE_STRING;
            case 19:
                return Schema.TYPE_STRING;
            case 20:
                return Schema.TYPE_STRING;
            case 21:
                return Schema.TYPE_STRING;
            case 22:
                return Schema.TYPE_STRING;
            case 23:
                return Schema.TYPE_STRING;
            case 24:
                return Schema.TYPE_STRING;
            case 25:
                return Schema.TYPE_STRING;
            case 26:
                return Schema.TYPE_STRING;
            case 27:
                return Schema.TYPE_STRING;
            case 28:
                return Schema.TYPE_STRING;
            case 29:
                return Schema.TYPE_STRING;
            case 30:
                return Schema.TYPE_STRING;
            case 31:
                return Schema.TYPE_STRING;
            case 32:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("APayID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(APayID));
        }
        if (FCode.equalsIgnoreCase("ShardingID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ShardingID));
        }
        if (FCode.equalsIgnoreCase("PayNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PayNo));
        }
        if (FCode.equalsIgnoreCase("IncomeNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(IncomeNo));
        }
        if (FCode.equalsIgnoreCase("IncomeType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(IncomeType));
        }
        if (FCode.equalsIgnoreCase("AppntNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AppntNo));
        }
        if (FCode.equalsIgnoreCase("SumActuPayMoney")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SumActuPayMoney));
        }
        if (FCode.equalsIgnoreCase("PayDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PayDate));
        }
        if (FCode.equalsIgnoreCase("EnterAccDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(EnterAccDate));
        }
        if (FCode.equalsIgnoreCase("ConfDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ConfDate));
        }
        if (FCode.equalsIgnoreCase("ApproveCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ApproveCode));
        }
        if (FCode.equalsIgnoreCase("ApproveDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ApproveDate));
        }
        if (FCode.equalsIgnoreCase("SerialNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SerialNo));
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeDate));
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equalsIgnoreCase("GetNoticeNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GetNoticeNo));
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyDate));
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (FCode.equalsIgnoreCase("ManageCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
        }
        if (FCode.equalsIgnoreCase("AgentCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentCom));
        }
        if (FCode.equalsIgnoreCase("AgentType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentType));
        }
        if (FCode.equalsIgnoreCase("BankCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BankCode));
        }
        if (FCode.equalsIgnoreCase("BankAccNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BankAccNo));
        }
        if (FCode.equalsIgnoreCase("RiskCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RiskCode));
        }
        if (FCode.equalsIgnoreCase("AgentCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentCode));
        }
        if (FCode.equalsIgnoreCase("AgentGroup")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentGroup));
        }
        if (FCode.equalsIgnoreCase("AccName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AccName));
        }
        if (FCode.equalsIgnoreCase("StartPayDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StartPayDate));
        }
        if (FCode.equalsIgnoreCase("PayTypeFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PayTypeFlag));
        }
        if (FCode.equalsIgnoreCase("OtherNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OtherNo));
        }
        if (FCode.equalsIgnoreCase("OtherNoType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OtherNoType));
        }
        if (FCode.equalsIgnoreCase("BalanceOnTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BalanceOnTime));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = String.valueOf(APayID);
                break;
            case 1:
                strFieldValue = String.valueOf(ShardingID);
                break;
            case 2:
                strFieldValue = String.valueOf(PayNo);
                break;
            case 3:
                strFieldValue = String.valueOf(IncomeNo);
                break;
            case 4:
                strFieldValue = String.valueOf(IncomeType);
                break;
            case 5:
                strFieldValue = String.valueOf(AppntNo);
                break;
            case 6:
                strFieldValue = String.valueOf(SumActuPayMoney);
                break;
            case 7:
                strFieldValue = String.valueOf(PayDate);
                break;
            case 8:
                strFieldValue = String.valueOf(EnterAccDate);
                break;
            case 9:
                strFieldValue = String.valueOf(ConfDate);
                break;
            case 10:
                strFieldValue = String.valueOf(ApproveCode);
                break;
            case 11:
                strFieldValue = String.valueOf(ApproveDate);
                break;
            case 12:
                strFieldValue = String.valueOf(SerialNo);
                break;
            case 13:
                strFieldValue = String.valueOf(Operator);
                break;
            case 14:
                strFieldValue = String.valueOf(MakeDate);
                break;
            case 15:
                strFieldValue = String.valueOf(MakeTime);
                break;
            case 16:
                strFieldValue = String.valueOf(GetNoticeNo);
                break;
            case 17:
                strFieldValue = String.valueOf(ModifyDate);
                break;
            case 18:
                strFieldValue = String.valueOf(ModifyTime);
                break;
            case 19:
                strFieldValue = String.valueOf(ManageCom);
                break;
            case 20:
                strFieldValue = String.valueOf(AgentCom);
                break;
            case 21:
                strFieldValue = String.valueOf(AgentType);
                break;
            case 22:
                strFieldValue = String.valueOf(BankCode);
                break;
            case 23:
                strFieldValue = String.valueOf(BankAccNo);
                break;
            case 24:
                strFieldValue = String.valueOf(RiskCode);
                break;
            case 25:
                strFieldValue = String.valueOf(AgentCode);
                break;
            case 26:
                strFieldValue = String.valueOf(AgentGroup);
                break;
            case 27:
                strFieldValue = String.valueOf(AccName);
                break;
            case 28:
                strFieldValue = String.valueOf(StartPayDate);
                break;
            case 29:
                strFieldValue = String.valueOf(PayTypeFlag);
                break;
            case 30:
                strFieldValue = String.valueOf(OtherNo);
                break;
            case 31:
                strFieldValue = String.valueOf(OtherNoType);
                break;
            case 32:
                strFieldValue = String.valueOf(BalanceOnTime);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("APayID")) {
            if( FValue != null && !FValue.equals("")) {
                APayID = new Long(FValue).longValue();
            }
        }
        if (FCode.equalsIgnoreCase("ShardingID")) {
            if( FValue != null && !FValue.equals(""))
            {
                ShardingID = FValue.trim();
            }
            else
                ShardingID = null;
        }
        if (FCode.equalsIgnoreCase("PayNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                PayNo = FValue.trim();
            }
            else
                PayNo = null;
        }
        if (FCode.equalsIgnoreCase("IncomeNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                IncomeNo = FValue.trim();
            }
            else
                IncomeNo = null;
        }
        if (FCode.equalsIgnoreCase("IncomeType")) {
            if( FValue != null && !FValue.equals(""))
            {
                IncomeType = FValue.trim();
            }
            else
                IncomeType = null;
        }
        if (FCode.equalsIgnoreCase("AppntNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                AppntNo = FValue.trim();
            }
            else
                AppntNo = null;
        }
        if (FCode.equalsIgnoreCase("SumActuPayMoney")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                SumActuPayMoney = d;
            }
        }
        if (FCode.equalsIgnoreCase("PayDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                PayDate = FValue.trim();
            }
            else
                PayDate = null;
        }
        if (FCode.equalsIgnoreCase("EnterAccDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                EnterAccDate = FValue.trim();
            }
            else
                EnterAccDate = null;
        }
        if (FCode.equalsIgnoreCase("ConfDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                ConfDate = FValue.trim();
            }
            else
                ConfDate = null;
        }
        if (FCode.equalsIgnoreCase("ApproveCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                ApproveCode = FValue.trim();
            }
            else
                ApproveCode = null;
        }
        if (FCode.equalsIgnoreCase("ApproveDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                ApproveDate = FValue.trim();
            }
            else
                ApproveDate = null;
        }
        if (FCode.equalsIgnoreCase("SerialNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                SerialNo = FValue.trim();
            }
            else
                SerialNo = null;
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            if( FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
                Operator = null;
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeDate = FValue.trim();
            }
            else
                MakeDate = null;
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
                MakeTime = null;
        }
        if (FCode.equalsIgnoreCase("GetNoticeNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                GetNoticeNo = FValue.trim();
            }
            else
                GetNoticeNo = null;
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyDate = FValue.trim();
            }
            else
                ModifyDate = null;
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
                ModifyTime = null;
        }
        if (FCode.equalsIgnoreCase("ManageCom")) {
            if( FValue != null && !FValue.equals(""))
            {
                ManageCom = FValue.trim();
            }
            else
                ManageCom = null;
        }
        if (FCode.equalsIgnoreCase("AgentCom")) {
            if( FValue != null && !FValue.equals(""))
            {
                AgentCom = FValue.trim();
            }
            else
                AgentCom = null;
        }
        if (FCode.equalsIgnoreCase("AgentType")) {
            if( FValue != null && !FValue.equals(""))
            {
                AgentType = FValue.trim();
            }
            else
                AgentType = null;
        }
        if (FCode.equalsIgnoreCase("BankCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                BankCode = FValue.trim();
            }
            else
                BankCode = null;
        }
        if (FCode.equalsIgnoreCase("BankAccNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                BankAccNo = FValue.trim();
            }
            else
                BankAccNo = null;
        }
        if (FCode.equalsIgnoreCase("RiskCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                RiskCode = FValue.trim();
            }
            else
                RiskCode = null;
        }
        if (FCode.equalsIgnoreCase("AgentCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                AgentCode = FValue.trim();
            }
            else
                AgentCode = null;
        }
        if (FCode.equalsIgnoreCase("AgentGroup")) {
            if( FValue != null && !FValue.equals(""))
            {
                AgentGroup = FValue.trim();
            }
            else
                AgentGroup = null;
        }
        if (FCode.equalsIgnoreCase("AccName")) {
            if( FValue != null && !FValue.equals(""))
            {
                AccName = FValue.trim();
            }
            else
                AccName = null;
        }
        if (FCode.equalsIgnoreCase("StartPayDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                StartPayDate = FValue.trim();
            }
            else
                StartPayDate = null;
        }
        if (FCode.equalsIgnoreCase("PayTypeFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                PayTypeFlag = FValue.trim();
            }
            else
                PayTypeFlag = null;
        }
        if (FCode.equalsIgnoreCase("OtherNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                OtherNo = FValue.trim();
            }
            else
                OtherNo = null;
        }
        if (FCode.equalsIgnoreCase("OtherNoType")) {
            if( FValue != null && !FValue.equals(""))
            {
                OtherNoType = FValue.trim();
            }
            else
                OtherNoType = null;
        }
        if (FCode.equalsIgnoreCase("BalanceOnTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                BalanceOnTime = FValue.trim();
            }
            else
                BalanceOnTime = null;
        }
        return true;
    }


    public String toString() {
    return "LJAPayPojo [" +
            "APayID="+APayID +
            ", ShardingID="+ShardingID +
            ", PayNo="+PayNo +
            ", IncomeNo="+IncomeNo +
            ", IncomeType="+IncomeType +
            ", AppntNo="+AppntNo +
            ", SumActuPayMoney="+SumActuPayMoney +
            ", PayDate="+PayDate +
            ", EnterAccDate="+EnterAccDate +
            ", ConfDate="+ConfDate +
            ", ApproveCode="+ApproveCode +
            ", ApproveDate="+ApproveDate +
            ", SerialNo="+SerialNo +
            ", Operator="+Operator +
            ", MakeDate="+MakeDate +
            ", MakeTime="+MakeTime +
            ", GetNoticeNo="+GetNoticeNo +
            ", ModifyDate="+ModifyDate +
            ", ModifyTime="+ModifyTime +
            ", ManageCom="+ManageCom +
            ", AgentCom="+AgentCom +
            ", AgentType="+AgentType +
            ", BankCode="+BankCode +
            ", BankAccNo="+BankAccNo +
            ", RiskCode="+RiskCode +
            ", AgentCode="+AgentCode +
            ", AgentGroup="+AgentGroup +
            ", AccName="+AccName +
            ", StartPayDate="+StartPayDate +
            ", PayTypeFlag="+PayTypeFlag +
            ", OtherNo="+OtherNo +
            ", OtherNoType="+OtherNoType +
            ", BalanceOnTime="+BalanceOnTime +"]";
    }
}
